// ---------------------------------------------------------------------------
// This file has been generated partially by the Web Dynpro Code Generator.
// MODIFY CODE ONLY IN SECTIONS ENCLOSED BY @@begin AND @@end.
// ALL OTHER CHANGES WILL BE LOST IF THE FILE IS REGENERATED.
// ---------------------------------------------------------------------------
package com.manulirubber.mdm.fittings;

// 
// IMPORTANT NOTE: 
// _ALL_ IMPORT STATEMENTS MUST BE PLACED IN THE FOLLOWING SECTION ENCLOSED
// BY @@begin imports AND @@end. FURTHERMORE, THIS SECTION MUST ALWAYS CONTAIN
// AT LEAST ONE IMPORT STATEMENT (E.G. THAT FOR IPrivateMDM_EPFC_FITTINGSView).
// OTHERWISE, USING THE ECLIPSE FUNCTION "Organize Imports" FOLLOWED BY
// A WEB DYNPRO CODE GENERATION (E.G. PROJECT BUILD) WILL RESULT IN THE LOSS
// OF IMPORT STATEMENTS.
//
//@@begin imports
import java.util.Map;

import com.manulirubber.mdm.fittings.wdp.IPrivateMDM_EPFC_FITTINGSView;
import com.sap.tc.webdynpro.clientserver.event.api.WDPortalEventing;
import com.sap.tc.webdynpro.services.session.api.IWDWindow;
import java.util.HashMap;
import java.util.Map;
//@@end

//@@begin documentation
//@@end

public class MDM_EPFC_FITTINGSView
{
  /**
   * Logging location.
   */
  private static final com.sap.tc.logging.Location logger = 
    com.sap.tc.logging.Location.getLocation(MDM_EPFC_FITTINGSView.class);

  static 
  {
    //@@begin id
		String id = "$Id$";
    //@@end
    com.sap.tc.logging.Location.getLocation("ID.com.sap.tc.webdynpro").infoT(id);
  }

  /**
   * Private access to the generated Web Dynpro counterpart 
   * for this controller class.  </p>
   *
   * Use <code>wdThis</code> to gain typed access to the context,
   * to trigger navigation via outbound plugs, to get and enable/disable
   * actions, fire declared events, and access used controllers and/or 
   * component usages.
   *
   * @see com.manulirubber.mdm.fittings.wdp.IPrivateMDM_EPFC_FITTINGSView for more details
   */
  private final IPrivateMDM_EPFC_FITTINGSView wdThis;

  /**
   * Root node of this controller's context. </p>
   *
   * Provides typed access not only to the elements of the root node 
   * but also to all nodes in the context (methods node<i>XYZ</i>()) 
   * and their currently selected element (methods current<i>XYZ</i>Element()). 
   * It also facilitates the creation of new elements for all nodes 
   * (methods create<i>XYZ</i>Element()). </p>
   *
   * @see com.manulirubber.mdm.fittings.wdp.IPrivateMDM_EPFC_FITTINGSView.IContextNode for more details.
   */
  private final IPrivateMDM_EPFC_FITTINGSView.IContextNode wdContext;

  /**
   * A shortcut for <code>wdThis.wdGetAPI()</code>. </p>
   * 
   * Represents the generic API of the generic Web Dynpro counterpart 
   * for this controller. </p>
   */
  private final com.sap.tc.webdynpro.progmodel.api.IWDViewController wdControllerAPI;
  
  /**
   * A shortcut for <code>wdThis.wdGetAPI().getComponent()</code>. </p>
   * 
   * Represents the generic API of the Web Dynpro component this controller 
   * belongs to. Can be used to access the message manager, the window manager,
   * to add/remove event handlers and so on. </p>
   */
  private final com.sap.tc.webdynpro.progmodel.api.IWDComponent wdComponentAPI;
  
  public MDM_EPFC_FITTINGSView(IPrivateMDM_EPFC_FITTINGSView wdThis)
  {
    this.wdThis = wdThis;
    this.wdContext = wdThis.wdGetContext();
    this.wdControllerAPI = wdThis.wdGetAPI();
    this.wdComponentAPI = wdThis.wdGetAPI().getComponent();
  }

  //@@begin javadoc:wdDoInit()
	/** Hook method called to initialize controller. */
  //@@end
  public void wdDoInit()
  {
    //@@begin wdDoInit()
	namespace = "urn:com.sap.pct.mdm.appl.masteriviews";
	eventname = "selectID";
	WDPortalEventing.subscribe(namespace, eventname, wdThis.wdGetActionCatchValueAction());
    
    //@@end
  }

  //@@begin javadoc:wdDoExit()
	/** Hook method called to clean up controller. */
  //@@end
  public void wdDoExit()
  {
    //@@begin wdDoExit()
    //@@end
  }

  //@@begin javadoc:wdDoModifyView
	/**
	 * Hook method called to modify a view just before rendering.
	 * This method conceptually belongs to the view itself, not to the
	 * controller (cf. MVC pattern).
	 * It is made static to discourage a way of programming that
	 * routinely stores references to UI elements in instance fields
	 * for access by the view controller's event handlers, and so on.
	 * The Web Dynpro programming model recommends that UI elements can
	 * only be accessed by code executed within the call to this hook method.
	 *
	 * @param wdThis Generated private interface of the view's controller, as
	 *        provided by Web Dynpro. Provides access to the view controller's
	 *        outgoing controller usages, etc.
	 * @param wdContext Generated interface of the view's context, as provided
	 *        by Web Dynpro. Provides access to the view's data.
	 * @param view The view's generic API, as provided by Web Dynpro.
	 *        Provides access to UI elements.
	 * @param firstTime Indicates whether the hook is called for the first time
	 *        during the lifetime of the view.
	 */
  //@@end
  public static void wdDoModifyView(IPrivateMDM_EPFC_FITTINGSView wdThis, IPrivateMDM_EPFC_FITTINGSView.IContextNode wdContext, com.sap.tc.webdynpro.progmodel.api.IWDView view, boolean firstTime)
  {
    //@@begin wdDoModifyView
    //@@end
  }

  //@@begin javadoc:print()
	/** Declared method. */
  //@@end
  public void print( boolean ISO )
  {
    //@@begin print()
		Map params =
			getParams(wdContext.currentContextElement().getCatchedValue());
		if (params == null)
			return;
		boolean allProducts =
			wdContext.currentContextElement().getAllProducts();
		String id = (String) params.get("ID");
		MDMFittingsDataSheet fittings =
			new MDMFittingsDataSheet(allProducts, id, ISO);
		String appParams = fittings.generatePDF();
		if (appParams == null
			|| appParams.equals("empty")
			|| appParams.equals("errorLogin")) {
			return;
		} else {
//			*** PRODUZIONE ***
//			String printUrl =
//				"http://www.manuli-hydraulics.com:8000/fittings/pdf/" + appParams;
//				*** PRODUZIONE ***

//				***  SVILUPPO  ***
				String printUrl =
					"http://www.manuli-hydraulics.com:8001/fittings/pdf/" + appParams;
//				***  SVILUPPO  ***
			fittings.removeFiles();
			IWDWindow window =
				wdComponentAPI.getWindowManager().createExternalWindow(
					printUrl,
					"Fittings",
					false);
			window.open();
			return;
		}

    //@@end
  }

  //@@begin javadoc:getParams()
  /** Declared method. */
  //@@end
  public java.util.Map getParams( java.lang.String params )
  {
    //@@begin getParams()
	Map map = new HashMap();
	String xmlData = null;
	try
	{
		String paramList[] = params.split("&");
		for(int i = 0; i < paramList.length; i++)
		{
			String parameter[] = paramList[i].split("=", 2);
			if(parameter.length == 2)
				map.put(parameter[0], parameter[1]);
		}

	}
	catch(Exception e)
	{
		return null;
	}
	return map;
    //@@end
  }

  //@@begin javadoc:onActionActionPrintISO(ServerEvent)
	/** Declared validating event handler. */
  //@@end
  public void onActionActionPrintISO(com.sap.tc.webdynpro.progmodel.api.IWDCustomEvent wdEvent )
  {
    //@@begin onActionActionPrintISO(ServerEvent)
		print(true);

    //@@end
  }

  //@@begin javadoc:onActionActionPrintAmerican(ServerEvent)
	/** Declared validating event handler. */
  //@@end
  public void onActionActionPrintAmerican(com.sap.tc.webdynpro.progmodel.api.IWDCustomEvent wdEvent )
  {
    //@@begin onActionActionPrintAmerican(ServerEvent)
		print(false);

    //@@end
  }

  //@@begin javadoc:onActionActionCatchValue(ServerEvent)
  /** Declared validating event handler. */
  //@@end
  public void onActionActionCatchValue(com.sap.tc.webdynpro.progmodel.api.IWDCustomEvent wdEvent, java.lang.String dataObject )
  {
    //@@begin onActionActionCatchValue(ServerEvent)
	wdContext.currentContextElement().setCatchedValue(dataObject);

    //@@end
  }

  /*
   * The following code section can be used for any Java code that is 
   * not to be visible to other controllers/views or that contains constructs
   * currently not supported directly by Web Dynpro (such as inner classes or
   * member variables etc.). </p>
   *
   * Note: The content of this section is in no way managed/controlled
   * by the Web Dynpro Designtime or the Web Dynpro Runtime. 
   */
  //@@begin others
  private String namespace;
  private String eventname;
  
  //@@end
}
