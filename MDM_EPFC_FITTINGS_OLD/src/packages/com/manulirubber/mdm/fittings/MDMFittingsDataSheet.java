/*
 * Created on 22-mag-2013
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.manulirubber.mdm.fittings;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.sap.tc.webdynpro.basesrvc.util.IOUtil;

// Referenced classes of package com.manuli:
//			  MDMDataAccess, PDFGenerator, ProdFittings, ProdFittingsRec

public class MDMFittingsDataSheet
{

	public MDMFittingsDataSheet(boolean allProducts, String id, boolean isISO)
	{
		this.allProducts = allProducts;
		this.id = id;
		this.isISO = isISO;
	}

	public String generatePDF()
	{
		String xmlData = null;
		String test = null;
		data = new MDMDataAccess(allProducts, id);
		if(!data.login())
			return new String("errorLogin");
		ProdFittings header = data.getProdFittings();
		List items = data.getProdFittingsRec(header, isISO);
		data.logout();
		if(header == null || items == null || items.isEmpty())
			return new String("empty");
		try
		{
			xmlData = parseResult(allProducts, header, items);
			PDFGenerator pdf = new PDFGenerator(xmlData, pdfTemplateUrl);
			return writePDF(pdf.generate());
		}
		catch(IOException ioe)
		{
			return null;
		}
	}

	private String parseResult(boolean allBranding, ProdFittings header, List list)
	{
		Document docOut = null;
		Element nodeRoot = null;
		Element node = null;
		Element nodeRow = null;
		ProdFittingsRec rec = null;
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		factory.setNamespaceAware(true);
		factory.setValidating(true);
		try
		{
			builder = factory.newDocumentBuilder();
			docOut = builder.newDocument();
		}
		catch(Exception e)
		{
			return null;
		}
		sort(list);
		nodeRoot = (Element)docOut.appendChild(docOut.createElement("fittings"));
		node = (Element)nodeRoot.appendChild(docOut.createElement("header"));
		node.appendChild(docOut.createElement("type_image")).appendChild(docOut.createTextNode(header.getTypeImage()));
		node.appendChild(docOut.createElement("catalogue_title")).appendChild(docOut.createTextNode(header.getCatalogueTitle()));
		node.appendChild(docOut.createElement("catalogue_subtitle")).appendChild(docOut.createTextNode(header.getCatalogueSubtitle()));
		node.appendChild(docOut.createElement("catalogue_image")).appendChild(docOut.createTextNode(header.getCatalogueImage()));
		node.appendChild(docOut.createElement("printable_name")).appendChild(docOut.createTextNode(header.getPrintableName()));
		node.appendChild(docOut.createElement("family_description")).appendChild(docOut.createTextNode(header.getFamilyDescription()));
		node = (Element)nodeRoot.appendChild(docOut.createElement("label"));
		for(int j = 0; j < header.getColLabels1().size(); j++)
		{
			String labelTitle = "column" + Integer.toString(j + 1) + "_text1";
			node.appendChild(docOut.createElement(labelTitle)).appendChild(docOut.createTextNode((String)header.getColLabels1().get(j)));
			labelTitle = "column" + Integer.toString(j + 1) + "_text2";
			node.appendChild(docOut.createElement(labelTitle)).appendChild(docOut.createTextNode((String)header.getColLabels2().get(j)));
		}

		node.appendChild(docOut.createElement("dimensions")).appendChild(docOut.createTextNode(header.getDimHeadLabel()));
		for(int j = 0; j < header.getDimLabels().size(); j++)
		{
			String labelTitle = "label" + Integer.toString(j + 1);
			node.appendChild(docOut.createElement(labelTitle)).appendChild(docOut.createTextNode((String)header.getDimLabels().get(j)));
		}

		node = (Element)nodeRoot.appendChild(docOut.createElement("lines"));
		for(Iterator itr = list.iterator(); itr.hasNext();)
		{
			rec = (ProdFittingsRec)itr.next();
			nodeRow = (Element)node.appendChild(docOut.createElement("row"));
			nodeRow.appendChild(docOut.createElement("part_number")).appendChild(docOut.createTextNode(rec.getName()));
			nodeRow.appendChild(docOut.createElement("hose_dn")).appendChild(docOut.createTextNode(rec.getDN()));
			nodeRow.appendChild(docOut.createElement("hose_dash")).appendChild(docOut.createTextNode(rec.getDash()));
			nodeRow.appendChild(docOut.createElement("hose_inch")).appendChild(docOut.createTextNode(rec.getInch()));
			for(int j = 0; j < rec.getColValues().size(); j++)
			{
				String labelTitle = "column" + Integer.toString(j + 1);
				nodeRow.appendChild(docOut.createElement(labelTitle)).appendChild(docOut.createTextNode((String)rec.getColValues().get(j)));
			}
			
			DecimalFormat format = new DecimalFormat("##.##");

			for(int j = 0; j < rec.getDimValues().size(); j++)
			{			
				String dimVal = (String)rec.getDimValues().get(j).toString().replaceAll(",", ".");
				double dimValues = Double.parseDouble(dimVal);
				String labelTitle = "dimension" + Integer.toString(j + 1);
				nodeRow.appendChild(docOut.createElement(labelTitle)).appendChild(docOut.createTextNode(format.format(dimValues)));
			}
		}

		nodeRoot.appendChild(docOut.createElement("footer")).appendChild(docOut.createTextNode(header.getNote1()));
		return domToString(docOut);
	}

	private String writePDF(InputStream is)
		throws IOException
	{
		String filename = null;
		String prefix = "PDF_" + (new SimpleDateFormat("yyyyMMdd_")).format(new Date());
		File tmpFile = File.createTempFile(prefix, ".pdf", new File(tempUrl));
		OutputStream out = null;
		try
		{
			out = new BufferedOutputStream(new FileOutputStream(tmpFile));
			IOUtil.write(is, out);
			filename = tmpFile.getName();
		}
		catch(IOException ioe)
		{
			String s = null;
			return s;
		}
		finally
		{
			if(out != null)
			{
				out.flush();
				out.close();
			}
		}
		return filename;
	}

	public void removeFiles()
	{
		File dir = new File(tempUrl);
		if(!dir.exists() || !dir.isDirectory())
			return;
		String info[] = dir.list();
		for(int i = 0; i < info.length; i++)
		{
			File file = new File(tempUrl + File.separator + info[i]);
			if(file.isFile())
			{
				String split[] = info[i].split("_");
				if(split.length >= 3 && split[1] != null)
				{
					String nowTimestamp = (new SimpleDateFormat("yyyyMMdd")).format(new Date());
					if(split[1].compareTo(nowTimestamp) < 0)
						file.delete();
				}
			}
		}

	}

	private void writeXML(String data)
		throws IOException
	{
		File tmpFile = File.createTempFile("PDF", ".xml", new File(tempUrl));
		byte bytes[] = data.getBytes("UTF-8");
		OutputStream out = null;
		try
		{
			out = new BufferedOutputStream(new FileOutputStream(tmpFile));
			out.write(bytes, 0, bytes.length);
			out.close();
		}
		catch(IOException ioe)
		{
			return;
		}
		finally
		{
			if(out != null)
			{
				out.flush();
				out.close();
			}
		}
	}

	private void sort(List list){
		Collections.sort(list, new Comparator() {
			public int compare(Object o1, Object o2){
				ProdFittingsRec p1 = (ProdFittingsRec)o1;
				ProdFittingsRec p2 = (ProdFittingsRec)o2;
				return p1.compareTo(p2);
			}});
	}

	private String domToString(Document doc)
	{
		StringWriter out = new StringWriter();
		try
		{
			Transformer transformer = TransformerFactory.newInstance().newTransformer();
			transformer.transform(new DOMSource(doc), new StreamResult(out));
		}
		catch(Exception e)
		{
			return null;
		}
		return out.toString();
	}

	private boolean allProducts;
	private String id;
	private boolean isISO;
	private MDMDataAccess data;
//  *** PRODUZIONE ***
//	private static String pdfTemplateUrl = "E://MDMPortal//reports//FittingsDataSheet.xdp";
//	private static String tempUrl = "E://MDMPortal//reports//output//fittings//";
//	*** PRODUZIONE ***

//	***  SVILUPPO  ***
	private static String pdfTemplateUrl = "F://MDMPortal//reports//FittingsDataSheet.xdp";
	private static String tempUrl = "F://MDMPortal//reports//output//fittings//";
//	***  SVILUPPO  ***

}