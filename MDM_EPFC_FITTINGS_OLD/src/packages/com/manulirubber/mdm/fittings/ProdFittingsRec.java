/*
 * Created on 22-mag-2013
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.manulirubber.mdm.fittings;

import a2i.core.Measurement;
import a2i.core.Value;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class ProdFittingsRec
{

	public ProdFittingsRec(boolean isISO)
	{
		dFormat = null;
		if(isISO)
			dFormat = new DecimalFormat("0.0");
		else
			dFormat = new DecimalFormat("0.00");
		dimValue = new ArrayList();
		colValue = new ArrayList();
	}

	public ArrayList getDimValues()
	{
		return dimValue;
	}

	public ArrayList getColValues()
	{
		return colValue;
	}

	public String getDash()
	{
		return dash;
	}

	public String getDN()
	{
		return dN;
	}

	public String getInch()
	{
		return inch;
	}

	public String getName()
	{
		return name;
	}

	public String getNominalSize()
	{
		return nominalSize;
	}

	private boolean isColEmpty(java.util.Map hm, String col){
		
		// colonna valida se esiste un valore per almeno una riga
		return (((Integer)hm.get(col)).intValue()==0 ? true : false);

	}

	public boolean setDimValue(java.util.Map colMap, a2i.common.A2iResultSet rs, int row, String col) throws a2i.core.StringException{

		// controlla se gia inserito il numero massimo di colonne (max 7)
		if(dimValue.size() >= 7) return false;
		
		// lettura valore
		Value value = rs.GetValueAt(row, col);
		
		// non considera il campo se per tutte le righe, la colonna è vuota
		if (isColEmpty(colMap, col)) return false;
		
		//if(value.IsNull())
		//	return false;
		double doubleVal = value.GetMeasurement().GetValue();
		//if(doubleVal == 0.0D){
		//	return false;
		//} else{
		dimValue.add(dFormat.format(doubleVal));
		return true;
		//}
	}
	
	public static boolean isDimValueValid(Value value){

		// valori null o zero non sono validi
		if(value.IsNull()) return false;
		double doubleVal = value.GetMeasurement().GetValue();
		if(doubleVal == 0.0D) return false;
		return true;

	}

	public boolean setThread(Value value)
	{
		if(colValue.size() >= 2)
			return false;
		if(value.IsNull())
		{
			return false;
		} else
		{
			colValue.add(value.GetStringValue());
			return true;
		}
	}

	public boolean setPipeMm(Value value)
	{
		DecimalFormat dFormatPipe = new DecimalFormat("0.##");
		if(colValue.size() >= 2)
			return false;
		if(value.IsNull())
			return false;
		double doubleVal = value.GetMeasurement().GetValue();
		if(doubleVal == 0.0D)
		{
			return false;
		} else
		{
			colValue.add(dFormatPipe.format(doubleVal));
			return true;
		}
	}

	public boolean setFlangeInch(Value value)
	{
		if(colValue.size() >= 2)
			return false;
		if(value.IsNull())
		{
			return false;
		} else
		{
			colValue.add(value.GetStringValue());
			return true;
		}
	}

	public boolean setWorkPressBar(Value value)
	{
		if(colValue.size() >= 2)
			return false;
		if(value.IsNull())
		{
			return false;
		} else
		{
			colValue.add(value.GetStringValue());
			return true;
		}
	}

	public boolean setWorkPressPSI(Value value)
	{
		if(colValue.size() >= 2)
			return false;
		if(value.IsNull())
		{
			return false;
		} else
		{
			colValue.add(value.GetStringValue());
			return true;
		}
	}

	public boolean setBolt(Value value)
	{
		if(colValue.size() >= 2)
			return false;
		if(value.IsNull())
		{
			return false;
		} else
		{
			colValue.add(value.GetStringValue().replaceFirst("mm", "").trim());
			return true;
		}
	}

	public void setName(Value value)
	{
		if(value.IsNull())
			name = null;
		else
			name = value.GetStringValue();
	}

	public void setNominalSize(Value value)
	{
		nominalSize = dash = inch = dN = null;
		if(value.IsNull())
			return;
		nominalSize = value.GetStringValue();
		String result[] = nominalSize.split(",");
		dN = result.length <= 0 || result[0] == null ? "" : result[0];
		dash = result.length <= 1 || result[1] == null ? "" : result[1];
		inch = result.length <= 3 || result[3] == null ? "" : result[3];
		if(value.IsNull())
			nominalSize = null;
		else
			nominalSize = value.GetStringValue();
	}

	public int compareTo(ProdFittingsRec p)
	{
		int compareResult = compareToDN(p);
		return compareResult == 0 ? compareToPartNumber(p) : compareResult;
	}

	public int compareToPartNumber(ProdFittingsRec p)
	{
		return getName().compareTo(p.getName());
	}

	public int compareToDN(ProdFittingsRec p)
	{
		try
		{
			int thisValue = Integer.parseInt(getDN());
			int otherValue = Integer.parseInt(p.getDN());
			if(thisValue == otherValue)
				return 0;
			return thisValue <= otherValue ? -1 : 1;
		}
		catch(NumberFormatException numberformatexception)
		{
			return 0;
		}
	}

	private DecimalFormat dFormat;
	private String name;
	private String nominalSize;
	private String dN;
	private String dash;
	private String inch;
	private String thread;
	private String pipeMm;
	private String flangeInch;
	private ArrayList dimValue;
	private ArrayList colValue;
	private String aMm;
	private String aInch;
	private String bMm;
	private String bInch;
	private String cMm;
	private String cInch;
	private String dMm;
	private String dInch;
	private String eMm;
	private String eInch;
	private String fMm;
	private String fInch;
	private String fDiamMm;
	private String fDiamInch;
	private String hMm;
	private String hInch;
	private String h1Mm;
	private String h1Inch;
	private String h2Mm;
	private String h2Inch;
	private String kMm;
	private String kInch;
	private String lMm;
	private String lInch;
	private String sMm;
	private String sInch;
	private String vMm;
	private String vInch;
	private String zMm;
	private String zInch;
	private String boltMm;
	private String workingPressBar;
	private String workingPressPsi;
}
