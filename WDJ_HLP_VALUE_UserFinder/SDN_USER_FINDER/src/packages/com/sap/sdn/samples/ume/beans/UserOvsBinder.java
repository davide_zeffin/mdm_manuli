/*
 * Created on 28.07.2005
 *
 */
package com.sap.sdn.samples.ume.beans;

import java.text.Collator;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Locale;

import com.sap.security.api.IUser;
import com.sap.tc.webdynpro.progmodel.api.IWDAttributeInfo;
import com.sap.tc.webdynpro.progmodel.api.IWDNodeElement;
import com.sap.tc.webdynpro.progmodel.api.IWDNodeInfo;
import com.sap.tc.webdynpro.progmodel.api.IWDOVSNotificationListener;
import com.sap.tc.webdynpro.progmodel.api.WDValueServices;
import com.sap.tc.webdynpro.services.sal.localization.api.WDResourceHandler;

/**
 * @author Valery_Silaev
 *
 */
public class UserOvsBinder 
{
	private UserOvsBinder() {}
	public static void bind(final IWDNodeInfo node)
	{
		bind(node, "smartKey");
	}
	
	public static void bind(final IWDNodeInfo node, final String attribute)
	{
		if ( null == node )
			throw new IllegalArgumentException( "Cannot bind OVS extension to null node" );
		final IWDAttributeInfo attrInfo = node.getAttribute(attribute);
		if ( null == attrInfo )
			throw new IllegalArgumentException( "Unnable to find target attribute: " + attribute + ", node" + node );
			
		bind(  attrInfo );
	}
	
	public static void bind(final IWDAttributeInfo attribute)
	{
		WDValueServices.addOVSExtension
		(
			SearchUsers.class.getName(), 
			new IWDAttributeInfo[]{ attribute },
			new SearchUsers( new UserComparator() ), 
			NOTIFICATION_LISTENER
		);				
	}
	
	final public static IWDOVSNotificationListener NOTIFICATION_LISTENER = new IWDOVSNotificationListener()
	{	
	  public void onQuery(final Object query) {}
  	
	  public void applyResult(final IWDNodeElement target, final Object result)
	  {
		  final SearchUsers.Result myResult = (SearchUsers.Result)result;
		  final IUserInfo info = (IUserInfo)target.model();
		  info.apply( myResult.user() );
		  
		  for (final Iterator i = target.node().getNodeInfo().iterateAttributes(); i.hasNext(); )
		  {
		  	final IWDAttributeInfo attr = (IWDAttributeInfo)i.next();
		  	if ( !attr.isReadOnly() )
		  		target.changed( attr.getName() );
		  }
	  }
  	
	  public void applyInputValues(final IWDNodeElement target, final Object query)
	  {
		  final SearchUsers myQuery = (SearchUsers)query;
		  myQuery.invalidate();
		  final IUserInfo info = (IUserInfo)target.model();
		  final IUser current = info.user();
		  if ( null != current )
		  {
			  myQuery.setDisplayNameFilter( current.getDisplayName() );
			  myQuery.setUniqueNameFilter( current.getUniqueName() );
			  myQuery.setEmailFilter( current.getEmail() );
		  }
		  else
		  {
		  	myQuery.setDisplayNameFilter( null );
			myQuery.setUniqueNameFilter( null );
			myQuery.setEmailFilter( null );
		  }
	  }
	};
	
	public static class UserComparator implements Comparator
	{
		final private Collator _collator;
		
		public UserComparator() { this( WDResourceHandler.getCurrentSessionLocale() ); }
		public UserComparator(final Locale locale) { _collator = Collator.getInstance(locale);	}
		
		public int compare(final Object o1, final Object o2)
		{
			return compare( (IUser)o1, (IUser)o2 );
		}
		
		public int compare(final IUser u1, final IUser u2)
		{
			final String dname1 = u1.getDisplayName();
			final String dname2 = u2.getDisplayName();
			return _collator.compare( dname1, dname2 );		
		}
	}
}
