/*
 * Created on 05.08.2005
 *
 */
package com.sap.sdn.samples.ume.beans;

import java.util.Collections;

import com.sap.security.api.IUser;
import com.sap.security.api.IUserFactory;
import com.sap.security.api.UMException;
import com.sap.security.api.UMFactory;

import com.sap.tc.webdynpro.progmodel.api.IWDNode;
import com.sap.tc.webdynpro.progmodel.api.IWDNodeCollectionSupplier;
import com.sap.tc.webdynpro.progmodel.api.IWDNodeElement;
import com.sap.tc.webdynpro.services.exceptions.WDNonFatalException;

/**
 * @author Valery_Silaev
 *
 */
public class UserNodeSupplier implements IWDNodeCollectionSupplier
{
	final private String _uidAttr;
	
	public UserNodeSupplier(final String uidAttr) { _uidAttr = uidAttr; }
	
	public void supplyElements(final IWDNode node, final IWDNodeElement parentElement)
	{
		final IUserInfo wrapee = new UserInfo();
		final IUserInfo wraper = new IUserInfo()
		{
			public String getDescription() { return wrapee.getDescription(); }
			public String getSmartKey() { return wrapee.getSmartKey(); }
			public IUser user()	{ return wrapee.user(); }

			public void apply(final IUser user) 
			{ 
				wrapee.apply( user );
				syncUid();
			}
			
			public void setSmartKey(final String value) 
			{
				try { wrapee.setSmartKey( value ); }
				finally { syncUid(); }
			}
			
			private void syncUid()
			{
				final IUser user = user();
				parentElement.setAttributeValue
				( 
					_uidAttr, null == user ? null : user.getUniqueID()
				);
				parentElement.changed( _uidAttr );
			}
		};
		
		node.bind( Collections.singleton( wraper ) );		
		final String currentUID = (String)parentElement.getAttributeValue( _uidAttr );
		if ( null != currentUID )
		{
			try
			{
				final IUser currentUser = _Users.getUser( currentUID );
				// Update wrapee, not wraper -- avoid changing attribute
				wrapee.apply( currentUser );
			}
			catch (final UMException ex)
			{
				node.getContext()
					.getController().getComponent()
						.getMessageManager()
							.reportException
							(
								new WDNonFatalException(ex), false
							); 
			}
		}
	}

	final private static IUserFactory _Users = UMFactory.getUserFactory();
}
