/*
 * Created on 28.07.2005
 *
 */
package com.sap.sdn.samples.ume.beans;

import com.sap.security.api.IUser;
import com.sap.tc.cmi.exception.CMIException;

/**
 * @author Valery_Silaev
 *
 */
public interface IUserFinder 
{
	abstract public IUser find(final String mask) throws CMIException;
}
