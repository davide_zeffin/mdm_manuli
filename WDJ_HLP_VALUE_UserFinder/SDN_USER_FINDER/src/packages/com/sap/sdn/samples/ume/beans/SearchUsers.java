/*
 * Created on 17.06.2005
 *
 */
package com.sap.sdn.samples.ume.beans;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.Locale;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.sap.sdn.samples.cmi.CMIInfo;
import com.sap.sdn.samples.cmi.CMIBean;

import com.sap.security.api.ISearchAttribute;
import com.sap.security.api.ISearchResult;
import com.sap.security.api.IUser;
import com.sap.security.api.IUserFactory;
import com.sap.security.api.IUserSearchFilter;
import com.sap.security.api.PrincipalIterator;
import com.sap.security.api.UMException;
import com.sap.security.api.UMFactory;

import com.sap.tc.cmi.exception.CMIException;

import com.sap.tc.cmi.metadata.ICMIModelClassInfo;
import com.sap.tc.cmi.metadata.ICMIModelObjectCollectionInfo;

import com.sap.tc.cmi.model.ICMIQuery;

import com.sap.tc.webdynpro.basesrvc.util.StringUtil;
/**
 * @author Valery_Silaev
 *
 */
public class SearchUsers
	extends
		CMIBean 
	implements 
		ICMIQuery 
{
	private Collection _result = null;
	
	private String _displayNameFilter = "*";
	private String _uniqueNameFilter  = "*";
	private String _emailFilter       = "*";
	
	final private Comparator _resultComparator;
	
	SearchUsers()
	{
		this(null);
	}
	
	SearchUsers(final Comparator resultComparator)
	{
		_resultComparator = resultComparator;
	}
	
	public String getDisplayNameFilter() { return _displayNameFilter; }
	public void setDisplayNameFilter(final String value) 
	{ 
		_displayNameFilter = StringUtil.isEmpty(value) ? "*" : value; 
	}
	
	public String getUniqueNameFilter() { return _uniqueNameFilter; }
	public void setUniqueNameFilter(final String value) 
	{ 
		_uniqueNameFilter = StringUtil.isEmpty(value) ? "*" : value;
	}
	
	public String getEmailFilter() { return _emailFilter; }
	public void setEmailFilter(final String value) 
	{ 
		_emailFilter = StringUtil.isEmpty(value) ? "*" : value;
	}

	public Object getAttributeValue(final String name)
	{
		if ( "displayNameFilter".equals(name) ) return getDisplayNameFilter();
		if ( "uniqueNameFilter".equals(name) ) return getUniqueNameFilter();
		if ( "emailFilter".equals(name) ) return getEmailFilter();
		return super.getAttributeValue(name);
	}
	
	public void setAttributeValue(final String name, final Object value)
	{
		if ( "displayNameFilter".equals(name) ) 
			setDisplayNameFilter((String)value);
		else if ( "uniqueNameFilter".equals(name) ) 
			setUniqueNameFilter((String)value);
		else if ( "emailFilter".equals(name) ) 
			setEmailFilter((String)value);
		else 
			super.setAttributeValue(name, value);			
	}


	public ICMIModelClassInfo associatedInputParameterInfo() 
	{
		return associatedModelClassInfo();
	}

	public ICMIModelObjectCollectionInfo associatedResultInfo() 
	{
		return RESULT_INFO;
	}

	public long countOf() { return -1; }

	public void invalidate() { _result = null; }

	public void execute() throws CMIException 
	{
		try
		{
			final IUserFactory users = UMFactory.getUserFactory();
			final IUserSearchFilter filter = users.getUserSearchFilter();

			final String[] names = names( _displayNameFilter );

			if ( !skipFilter(names[0]) ) 
				filter.setFirstName( names[0], ISearchAttribute.LIKE_OPERATOR, false);
			if ( !skipFilter(names[1]) ) 
				filter.setLastName( names[1], ISearchAttribute.LIKE_OPERATOR, false);
				
			if ( !skipFilter(_uniqueNameFilter) )
				filter.setUniqueName( _uniqueNameFilter, ISearchAttribute.LIKE_OPERATOR, false );
				
			if ( !skipFilter( _emailFilter ) )
				filter.setEmail( _emailFilter, ISearchAttribute.LIKE_OPERATOR, false );
				
			final ISearchResult sresult = users.searchUsers(filter);
			if ( sresult.getState() != ISearchResult.SEARCH_RESULT_OK )
			{
				throw new CMIException("Error searching users");
			}
			
			final ArrayList output = new ArrayList( sresult.size() );
			for ( final Iterator i = new PrincipalIterator(sresult); i.hasNext(); ) 
			{ 
				output.add
				( 
					new Result( (IUser)i.next() )
				); 
			}
			if ( null != _resultComparator )
			{
				Collections.sort
				( 
					output,
					new Comparator()
					{
						public int compare(final Object o1, final Object o2)
						{
							return _resultComparator.compare
							(
								((Result)o1).user(),
								((Result)o2).user()
							);
						}
					} 
				);
			}
			_result = output;
		}
		catch (final UMException ex)
		{
			throw new CMIException(ex);
		}
	}

	public Object getInputParameter() 
	{
		return this;
	}

	public Collection getResult() 
	{
		return _result;
	}

	public boolean isDirty() 
	{
		return null == _result;
	}

	public ICMIModelClassInfo associatedModelClassInfo()
	{
		return INPUT_INFO; 
	}
		
	private static boolean skipFilter(final String mask)
	{
		return ( StringUtil.isEmpty(mask) || "*".equals(mask) ); 
	}
	
	final private static Pattern LF_NAME = Pattern.compile("^\\s*(\\S+)\\s*,\\s*(\\S+)\\s*$");
	final private static Pattern FL_NAME = Pattern.compile("^\\s*(\\S+)\\s+(\\S+)\\s*$");
	
	private static String[] names(final String fullNameMask)
	{
		if ( skipFilter(fullNameMask) )
			return new String[]{null, null};
		
		Matcher matcher;
		
		matcher = LF_NAME.matcher( fullNameMask );
		if ( matcher.matches() )
			return new String[]{ matcher.group(2), matcher.group(1) };	
		
		matcher = FL_NAME.matcher( fullNameMask );
		if ( matcher.matches() )
			return new String[]{ matcher.group(1), matcher.group(2) };
			
		return new String[]{null,fullNameMask};	
	}
	
	
	public static class Result extends CMIBean
	{
		final private IUser _user;
		
		Result(final IUser user)
		{
			_user = user;
		}
	
		public String getDisplayName() { return _user.getDisplayName(); }
	
		public String getEmail() { return _user.getEmail(); }
	
		public IUser user() { return _user; }
		
		public Object getAttributeValue(final String name)
		{
			if ( "email".equals(name) ) return getEmail();
			if ( "displayName".equals(name) ) return getDisplayName();
			return super.getAttributeValue(name);
		}
	
		public void setAttributeValue(final String name, final Object value)
		{
			if ( "email".equals(name) || "displayName".equals(name) ) 
				throw new IllegalArgumentException("Property " + name + " is read-only");
			super.setAttributeValue(name, value);			
		}

		public ICMIModelClassInfo associatedModelClassInfo()
		{
			return RESULT_ELEMENT_INFO;
		}
	}
	
	final private static CMIInfo.GenericClassInfo INPUT_INFO 
		= new CMIInfo.GenericClassInfo( "SearchUsers", Locale.ENGLISH )
	{
		{
			property("uniqueNameFilter",  
			         "com.sap.sdn.samples.ume.models.uf.types.UniqueNameFilter", 
			         false );
			property("emailFilter",       
                     "com.sap.sdn.samples.ume.models.uf.types.EmailFilter", 
                      false );
			property("displayNameFilter", 
                     "com.sap.sdn.samples.ume.models.uf.types.DisplayNameFilter", 
                     false );			
		}
	};
	
	final private static ICMIModelClassInfo RESULT_ELEMENT_INFO 
		= new CMIInfo.GenericClassInfo( INPUT_INFO.getName() + "_Result", Locale.ENGLISH ) 
	{
		{
			property("displayName", 
                     "com.sap.sdn.samples.ume.models.uf.types.DisplayName", 
                     true );				
			property("email",       
                     "com.sap.sdn.samples.ume.models.uf.types.Email", 
                     true );
		}
	};
	
	final private static ICMIModelObjectCollectionInfo RESULT_INFO = new CMIInfo.BasicCollectionInfo
	(
		RESULT_ELEMENT_INFO.getName() + "_Collection", RESULT_ELEMENT_INFO 
	);
	
}
