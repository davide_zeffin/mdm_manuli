/*
 * Created on 28.07.2005
 *
 */
package com.sap.sdn.samples.ume.beans;

import java.util.AbstractCollection;
import java.util.Collection;
import java.util.Iterator;
import java.util.regex.Pattern;

import com.sap.security.api.IUser;
import com.sap.tc.cmi.exception.CMIException;
import com.sap.tc.webdynpro.basesrvc.util.StringUtil;

/**
 * @author Valery_Silaev
 *
 */
public class UserFinder 
{
	private UserFinder() {}
	
	public static IUser find(final String keyMask) throws CMIException
	{
		if ( StringUtil.isEmpty(keyMask) )
			throw new CMIException("No search criteria defined");
			  
		final IUserFinder finder = FULL_NAME.matcher( keyMask ).matches() 
			? STARTEGY_RELAXED2STRICT : STRATEGY_STRICT2RELAXED;
			
		return finder.find( keyMask ); 
	}
	
	final public static IUserFinder NOT_FOUND = new IUserFinder()
	{
		public IUser find(final String mask) throws CMIException
		{
			throw new CMIException("No user found");
		}
	};
	
	final public static IUserFinder MULTIPLE_RESULTS = new IUserFinder()
	{
		public IUser find(final String mask) throws CMIException
		{
			throw new CMIException("Multiple users found, please refine search criteria");
		}
	};
	
	abstract public static class ChainedFinder implements IUserFinder
	{
		final private IUserFinder _onNone;
		final private IUserFinder _onMultiple;
		
		public ChainedFinder(final IUserFinder onNone, final IUserFinder onMultiple)
		{
			_onNone     = onNone;
			_onMultiple = onMultiple;
		}
		
		abstract protected Collection _find(final String mask) throws CMIException;
		
		public IUser find(final String mask) throws CMIException
		{
			final Collection result = _find( mask );
			
			if ( null == result || result.isEmpty() )
				return _onNone.find( mask );
				
			if ( result.size() > 1 )
				return _onMultiple.find( mask );
				
			return (IUser)result.iterator().next();
		}
	}
	
	public static class LookupByAttribute extends ChainedFinder
	{
		final String _attr;
		public LookupByAttribute(final String attr, final IUserFinder onNone, final IUserFinder onMultiple) 
		{ 
			super( onNone, onMultiple );
			_attr = attr; 
		}
		
		protected Collection _find(final String mask) throws CMIException
		{
			final SearchUsers query = new SearchUsers(); // without sorting
			query.setAttributeValue( _attr, mask );
			query.execute();
			
			final Collection coriginal = query.getResult();
			return new AbstractCollection()
			{
				public int size() { return coriginal.size(); }
				public Iterator iterator() 
				{
					final Iterator ioriginal = coriginal.iterator();
					return new Iterator()
					{
						public boolean hasNext() { return ioriginal.hasNext(); }
						public Object next() 
						{
							final SearchUsers.Result entry = (SearchUsers.Result)ioriginal.next();  
							return entry.user(); 
						}
						public void remove() { throw new UnsupportedOperationException(); }
					};
				}
			};
		}					
	}
	
	final public static IUserFinder STRATEGY_STRICT2RELAXED;
	final public static IUserFinder STARTEGY_RELAXED2STRICT;
	
	static
	{
		STRATEGY_STRICT2RELAXED = new LookupByAttribute
		(
			"uniqueNameFilter",
			new LookupByAttribute
			(
				"emailFilter",
				new LookupByAttribute
				(
					"displayNameFilter",
					NOT_FOUND,
					MULTIPLE_RESULTS
				),
				MULTIPLE_RESULTS
			),
			MULTIPLE_RESULTS
		);
		
		STARTEGY_RELAXED2STRICT = new LookupByAttribute
		(
			"displayNameFilter",
			NOT_FOUND,
			new LookupByAttribute
			(
				"emailFilter",
				NOT_FOUND,
				new LookupByAttribute
				(
					"uniqueNameFilter",
					NOT_FOUND,
					MULTIPLE_RESULTS
				)
			)
		);
	}
	
	final private static Pattern FULL_NAME 
		= Pattern.compile("^\\s*\\S+\\s*(,|\\s)\\s*(\\S+)\\s*$");
		
	
}
