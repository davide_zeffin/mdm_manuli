/*
 * Created on 28.07.2005
 */
package com.sap.sdn.samples.ume.beans;

import com.sap.security.api.IUser;

/**
 * @author Valery_Silaev
 *
 */
public interface IUserInfo 
{
	public abstract void apply(final IUser user);
	public abstract IUser user();
	public abstract String getSmartKey();
	public abstract void setSmartKey(final String value);
	public abstract String getDescription();
}