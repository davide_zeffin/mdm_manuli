/*
 * Created on 09.06.2005
 *
 */
package com.sap.sdn.samples.cmi;

import java.util.Locale;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.LinkedHashMap;
import java.util.Collections;

import com.sap.dictionary.runtime.IDataType;
import com.sap.dictionary.runtime.IStructure;

import com.sap.tc.cmi.metadata.CMISetting;
import com.sap.tc.cmi.metadata.CMISettingDefinition;

import com.sap.tc.cmi.metadata.ICMIAbstractInfo;
import com.sap.tc.cmi.metadata.ICMIModelClassInfo;
import com.sap.tc.cmi.metadata.ICMIModelClassPropertyInfo;
import com.sap.tc.cmi.metadata.ICMIModelInfo;
import com.sap.tc.cmi.metadata.ICMIModelObjectCollectionInfo;
import com.sap.tc.cmi.metadata.ICMIRelationRoleInfo;

import com.sap.tc.webdynpro.progmodel.api.IWDAttributeInfo;
import com.sap.tc.webdynpro.progmodel.api.IWDNodeInfo;

/**
 * @author Valery_Silaev
 */
public class CMIInfo 
{
	private CMIInfo() {}
	
	public static class GenericClassInfo extends AbstractClassInfo
	{	
		final private Locale _locale;
		 		
		protected GenericClassInfo(final String name, final Locale locale) 
		{ 
			super(name);
			_locale = locale;
		}
		
		final protected void property(final String name, final String type, final boolean isReadOnly)
		{
			final IDataType dataType = DictionaryTypes.getType
			( 
				type, _locale, this.getClass().getClassLoader() 
			);
			super.property( name, dataType, isReadOnly );
		}
		
		public GenericClassInfo clone(final Locale locale)
		{
			final GenericClassInfo result = new GenericClassInfo( getName(), _locale );
			for (final Iterator i = iteratePropertyInfos(); i.hasNext(); )
			{
				final ICMIModelClassPropertyInfo prop = (ICMIModelClassPropertyInfo)i.next();
				final IDataType type = prop.getDataType();
				result.property( prop.getName(), type.getQualifiedName(), prop.isReadOnly() );
			}
			return result;
		}
	}		
	
	public static class ClassInfoByPrototype extends AbstractClassInfo
	{
		public ClassInfoByPrototype(final String name, final IWDNodeInfo prototype)
		{	
			super(name);
			for (final Iterator i = prototype.iterateAttributes(); i.hasNext(); )
			{
				final IWDAttributeInfo attr = (IWDAttributeInfo)i.next();
				property( attr.getName(), attr.getDataType(), attr.isReadOnly() );
			}
		}
	}
	
	public static abstract class AbstractClassInfo extends BasicInfo implements ICMIModelClassInfo
	{	
		final private Map _properties = new LinkedHashMap();
				
		protected AbstractClassInfo(final String name) 
		{ 
			super(name);
		}
		
		final protected void property(final String name, final IDataType type, final boolean isReadOnly)
		{
			_properties.put
			( 
				name,
				new BasicPropertyInfo
				(
					name,
					type,
					isReadOnly
				)
			);
		}
				
		public ICMIModelInfo getModelInfo() { return null; }

		public Collection getPropertyInfos() 
			{ return Collections.unmodifiableCollection( _properties.values() ); }

		public Iterator iteratePropertyInfos() 
			{ return getPropertyInfos().iterator(); }

		public ICMIModelClassPropertyInfo getPropertyInfo(final String name) 
			{ return (ICMIModelClassPropertyInfo)_properties.get( name ); }

		public IStructure getStructureType() { return null; }
  
		public Collection getSourceRoleInfos() { return Collections.EMPTY_LIST; }
		public Iterator iterateSourceRoleInfos() { return Collections.EMPTY_LIST.iterator(); }
		public ICMIRelationRoleInfo getTargetRoleInfo(final String targetRoleName) { return null; }
		public ICMIModelClassInfo getRelatedModelClassInfo(final String targetRoleName) { return null; }
		
		public boolean isGeneric() { return true; }
		
		class BasicPropertyInfo extends BasicInfo implements ICMIModelClassPropertyInfo
		{
			final private IDataType _dataType;
			final private boolean   _isReadOnly;
			
			BasicPropertyInfo(final String name, final IDataType type, final boolean isReadOnly) 
			{ 
				super(name); 
				_dataType   = type;
				_isReadOnly = isReadOnly;
			}

			public ICMIModelClassInfo getModelClassInfo() { return AbstractClassInfo.this; }
			public IDataType getDataType() { return _dataType; }
			public boolean isReadOnly() { return _isReadOnly; }
		}
	}

	public static class BasicCollectionInfo
		extends
			CMIInfo.BasicInfo 
		implements 
			ICMIModelObjectCollectionInfo
			
	{
		final private ICMIModelClassInfo _elementInfo;
		public BasicCollectionInfo(final String name, final ICMIModelClassInfo elementInfo)
		{ 
			super(name); 
			_elementInfo = elementInfo;
		}
		
		public ICMIModelClassInfo getElementModelClassInfo()
		{
			return _elementInfo;		
		}
	}	

	
	public abstract static class BasicInfo implements ICMIAbstractInfo
	{
		final private String _name;
		
		protected BasicInfo(final String name) { _name = name; }
		
		public String getName() { return _name; }
				
		public CMISetting getSetting(final CMISettingDefinition settingDef) 
			{ throw new UnsupportedOperationException(); }
		public void setSetting(final CMISettingDefinition settingDef, final String value) 
			{throw new UnsupportedOperationException();}
		
		public Map getSettings() 
			{ return Collections.EMPTY_MAP; }

		public boolean supports(String option) 
			{ return false; }

		public Collection supportedOptions()
			{ return Collections.EMPTY_SET; }

		public void addSupportedOption(final String option)
			{ throw new UnsupportedOperationException();}		
	}
}
