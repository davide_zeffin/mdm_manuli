/*
 * Created on 28.07.2005
 *
 */
package com.sap.sdn.samples.ume.beans;

import java.text.MessageFormat;

import com.sap.security.api.IUser;
import com.sap.tc.cmi.exception.CMIException;
import com.sap.tc.webdynpro.basesrvc.util.StringUtil;
import com.sap.tc.webdynpro.services.exceptions.WDNonFatalRuntimeException;

/**
 * @author Valery_Silaev
 *
 */
public class UserInfo implements IUserInfo
{
	private IUser   _user;
	private String  _description;
	
	public UserInfo() {}
	public UserInfo(final IUser user) { apply( user ); }
	
	public void apply(final IUser user) 
	{
		_user = user;
		if ( null == _user )
		{
			_description = null;
			return;
		}
		
		final String email = _user.getEmail();
		if ( !StringUtil.isEmpty(email) )
		{
			final String name = _user.getDisplayName();
			
			if ( StringUtil.isEmpty( name ) )
				_description = email;
			else
				_description = new MessageFormat("{0} '<'{1}'>'")
					.format( new Object[]{ name, email } );
		}
		else
			_description = null;
	}
	
	public IUser user() { return _user; }
	
	public String getSmartKey() 
	{ 
		return null == _user ? null : _user.getDisplayName(); 
	}
	
	public void setSmartKey(final String value)
	{
		if ( StringUtil.isEmpty(value) )
		{
			apply(null);
			return;
		}
		
		try
		{
			apply
			(
				UserFinder.find( value )
			);
		}
		catch (final CMIException ex)
		{
			apply( null );
			throw new WDNonFatalRuntimeException(ex);
		}
	}
	
	public String getDescription()
	{
		return _description;
	}
	
}
