// ---------------------------------------------------------------------------
// This file has been generated partially by the Web Dynpro Code Generator.
// MODIFY CODE ONLY IN SECTIONS ENCLOSED BY @@begin AND @@end.
// ALL OTHER CHANGES WILL BE LOST IF THE FILE IS REGENERATED.
// ---------------------------------------------------------------------------
package it.alcantara.crsap.view;

// 
// IMPORTANT NOTE: 
// _ALL_ IMPORT STATEMENTS MUST BE PLACED IN THE FOLLOWING SECTION ENCLOSED
// BY @@begin imports AND @@end. FURTHERMORE, THIS SECTION MUST ALWAYS CONTAIN
// AT LEAST ONE IMPORT STATEMENT (E.G. THAT FOR IPrivateChangeRequestSAPView).
// OTHERWISE, USING THE ECLIPSE FUNCTION "Organize Imports" FOLLOWED BY
// A WEB DYNPRO CODE GENERATION (E.G. PROJECT BUILD) WILL RESULT IN THE LOSS
// OF IMPORT STATEMENTS.
//
//@@begin imports
import it.alcantara.crsap.view.wdp.IPrivateChangeRequestSAPView;
import it.alcantara.crsap.view.wdp.IPrivateChangeRequestSAPView.IAccettazioneListElement;
import it.alcantara.crsap.view.wdp.IPrivateChangeRequestSAPView.IAccettazioneListNode;
import it.alcantara.crsap.view.wdp.IPrivateChangeRequestSAPView.IApprovalListElement;
import it.alcantara.crsap.view.wdp.IPrivateChangeRequestSAPView.IApprovalListNode;
import it.alcantara.crsap.view.wdp.IPrivateChangeRequestSAPView.ICRSapUpdateOutputElement;
import it.alcantara.crsap.view.wdp.IPrivateChangeRequestSAPView.IContextNode;
import it.alcantara.crsap.view.wdp.IPrivateChangeRequestSAPView.IControllerlLoadImgElement;
import it.alcantara.crsap.view.wdp.IPrivateChangeRequestSAPView.IITListElement;
import it.alcantara.crsap.view.wdp.IPrivateChangeRequestSAPView.IITListNode;
import it.alcantara.crsap.view.wdp.IPrivateChangeRequestSAPView.ILocalUserListElement;
import it.alcantara.crsap.view.wdp.IPrivateChangeRequestSAPView.ILocalUserListNode;
import it.alcantara.crsap.view.wdp.IPrivateChangeRequestSAPView.IPrioritySelectionElement;
import it.alcantara.crsap.view.wdp.IPrivateChangeRequestSAPView.IPrioritySelectionNode;
import it.alcantara.crsap.view.wdp.IPrivateChangeRequestSAPView.IReaderCommentsElement;
import it.alcantara.crsap.view.wdp.IPrivateChangeRequestSAPView.IReaderCommentsNode;
import it.alcantara.crsap.view.wdp.IPrivateChangeRequestSAPView.IReaderListElement;
import it.alcantara.crsap.view.wdp.IPrivateChangeRequestSAPView.IReaderListNode;
import it.alcantara.crsap.view.wdp.IPrivateChangeRequestSAPView.IRoleValuesElement;
import it.alcantara.crsap.view.wdp.IPrivateChangeRequestSAPView.IRoleValuesNode;
import it.alcantara.crsap.view.wdp.IPrivateChangeRequestSAPView.ISapModuleSelectionElement;
import it.alcantara.crsap.view.wdp.IPrivateChangeRequestSAPView.ISapModuleSelectionNode;
import it.alcantara.crsap.view.wdp.IPrivateChangeRequestSAPView.IUserSearchResultElement;
import it.alcantara.crsap.view.wdp.IPrivateChangeRequestSAPView.IUserSearchResultNode;
import it.alcantara.crsap.view.wdp.IPrivateChangeRequestSAPView.IViewNodeElement;
import it.alcantara.crsap.wdp.IPublicChangeRequestSAP;
import it.alcantara.crsap.wdp.IPublicUpdateCRSAPCust;
import it.alcantara.model.addattach.Zsattachment;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

import com.sap.security.api.ISearchAttribute;
import com.sap.security.api.ISearchResult;
import com.sap.security.api.IUser;
import com.sap.security.api.IUserAccount;
import com.sap.security.api.IUserFactory;
import com.sap.security.api.IUserSearchFilter;
import com.sap.security.api.UMFactory;
import com.sap.tc.webdynpro.progmodel.api.IWDAttributeInfo;
import com.sap.tc.webdynpro.progmodel.api.IWDMessageManager;
import com.sap.tc.webdynpro.progmodel.api.IWDModifiableBinaryType;
import com.sap.tc.webdynpro.progmodel.api.IWDNode;
import com.sap.tc.webdynpro.progmodel.api.WDVisibility;
import com.sap.tc.webdynpro.services.sal.datatransport.api.IWDResource;
import com.sap.tc.webdynpro.services.sal.url.api.IWDCachedWebResource;
import com.sap.tc.webdynpro.services.sal.url.api.WDWebResource;
import com.sap.tc.webdynpro.services.sal.url.api.WDWebResourceType;
//@@end

//@@begin documentation
//@@end

public class ChangeRequestSAPView
{
  /**
   * Logging location.
   */
  private static final com.sap.tc.logging.Location logger = 
    com.sap.tc.logging.Location.getLocation(ChangeRequestSAPView.class);

  static 
  {
    //@@begin id
		String id = "$Id$";
    //@@end
    com.sap.tc.logging.Location.getLocation("ID.com.sap.tc.webdynpro").infoT(id);
  }

  /**
   * Private access to the generated Web Dynpro counterpart 
   * for this controller class.  </p>
   *
   * Use <code>wdThis</code> to gain typed access to the context,
   * to trigger navigation via outbound plugs, to get and enable/disable
   * actions, fire declared events, and access used controllers and/or 
   * component usages.
   *
   * @see it.alcantara.crsap.view.wdp.IPrivateChangeRequestSAPView for more details
   */
  private final IPrivateChangeRequestSAPView wdThis;

  /**
   * Root node of this controller's context. </p>
   *
   * Provides typed access not only to the elements of the root node 
   * but also to all nodes in the context (methods node<i>XYZ</i>()) 
   * and their currently selected element (methods current<i>XYZ</i>Element()). 
   * It also facilitates the creation of new elements for all nodes 
   * (methods create<i>XYZ</i>Element()). </p>
   *
   * @see it.alcantara.crsap.view.wdp.IPrivateChangeRequestSAPView.IContextNode for more details.
   */
  private final IPrivateChangeRequestSAPView.IContextNode wdContext;

  /**
   * A shortcut for <code>wdThis.wdGetAPI()</code>. </p>
   * 
   * Represents the generic API of the generic Web Dynpro counterpart 
   * for this controller. </p>
   */
  private final com.sap.tc.webdynpro.progmodel.api.IWDViewController wdControllerAPI;
  
  /**
   * A shortcut for <code>wdThis.wdGetAPI().getComponent()</code>. </p>
   * 
   * Represents the generic API of the Web Dynpro component this controller 
   * belongs to. Can be used to access the message manager, the window manager,
   * to add/remove event handlers and so on. </p>
   */
  private final com.sap.tc.webdynpro.progmodel.api.IWDComponent wdComponentAPI;
  
  public ChangeRequestSAPView(IPrivateChangeRequestSAPView wdThis)
  {
    this.wdThis = wdThis;
    this.wdContext = wdThis.wdGetContext();
    this.wdControllerAPI = wdThis.wdGetAPI();
    this.wdComponentAPI = wdThis.wdGetAPI().getComponent();
  }

  //@@begin javadoc:wdDoInit()
	/** Hook method called to initialize controller. */
  //@@end
  public void wdDoInit()
  {
    //@@begin wdDoInit()

		messageManager = wdComponentAPI.getMessageManager();
		IPublicChangeRequestSAP controller = wdThis.wdGetChangeRequestSAPController();

		/*************************
		** Insert Combo Box
		** Values
		** The Combo Values must 
		** be retrieved by KM Init
		** File. 
		** TO BE IMP
		*************************/
		Collection collection = retrieveRoleValue();
		Iterator iter = collection.iterator();
		IRoleValuesNode roleValuesNode = wdContext.nodeRoleValues();

		while (iter.hasNext()) {
			// Populate RoleValues Node Context
			String roleName = (String)iter.next();
			IRoleValuesElement element = roleValuesNode.createRoleValuesElement();
			element.setRoleValue(roleName);
			roleValuesNode.addElement(element);
		}

		currentStatusCod = wdContext.currentCRAttributesElement().getCRStatus();

		/********************
		** To Load Image
		*********************/
		//mx -> Commento parte non più usata inerente il caricamento degli attachment
		//IWDAttributeInfo attInfo = wdContext.nodeLocalLoadImg().getNodeInfo().getAttribute("fData");
		//((IWDModifiableBinaryType)attInfo.getModifiableSimpleType()).getMimeType();

    //@@end
  }

  //@@begin javadoc:wdDoExit()
	/** Hook method called to clean up controller. */
  //@@end
  public void wdDoExit()
  {
    //@@begin wdDoExit()
    //@@end
  }

  //@@begin javadoc:wdDoModifyView
	/**
	 * Hook method called to modify a view just before rendering.
	 * This method conceptually belongs to the view itself, not to the
	 * controller (cf. MVC pattern).
	 * It is made static to discourage a way of programming that
	 * routinely stores references to UI elements in instance fields
	 * for access by the view controller's event handlers, and so on.
	 * The Web Dynpro programming model recommends that UI elements can
	 * only be accessed by code executed within the call to this hook method.
	 *
	 * @param wdThis Generated private interface of the view's controller, as
	 *        provided by Web Dynpro. Provides access to the view controller's
	 *        outgoing controller usages, etc.
	 * @param wdContext Generated interface of the view's context, as provided
	 *        by Web Dynpro. Provides access to the view's data.
	 * @param view The view's generic API, as provided by Web Dynpro.
	 *        Provides access to UI elements.
	 * @param firstTime Indicates whether the hook is called for the first time
	 *        during the lifetime of the view.
	 */
  //@@end
  public static void wdDoModifyView(IPrivateChangeRequestSAPView wdThis, IPrivateChangeRequestSAPView.IContextNode wdContext, com.sap.tc.webdynpro.progmodel.api.IWDView view, boolean firstTime)
  {
    //@@begin wdDoModifyView

		IViewNodeElement viewNodeElement = wdContext.currentViewNodeElement();

		short CRStatus = wdContext.currentCRAttributesElement().getCRStatus();
		IWDNode rootNode = wdContext.wdGetAPI().getRootNode();

		IPublicChangeRequestSAP controller = wdThis.wdGetChangeRequestSAPController();
		IWDNode node = null;

		/******************************
		** Init Context View Fields
		** only the first Time.
		******************************/
		if (!firstTime)
			return;

		currentStatusCod = CRStatus;

		// Only if the current status = 2 <=> Revisione
		setReaderCommentEnableField(controller.getCurrentUserLogonID(), wdContext, currentStatusCod);
		
		//mx -> Commento parte non più usata inerente il caricamento degli attachment
//		byte[] data = wdContext.currentCRAttributesElement().getFDataImg();
//
//		if (data != null && data.length > 0) {
//			wdContext.currentLocalLoadImgElement().setFData(data);
//
//			WDWebResourceType type =
//				((IWDModifiableBinaryType)wdContext
//					.nodeLocalLoadImg()
//					.getNodeInfo()
//					.getAttribute("fData")
//					.getModifiableSimpleType())
//					.getMimeType();
//
//			byte[] file = wdContext.currentLocalLoadImgElement().getFData();
//			wdContext.currentCRAttributesElement().setFDataImg(file);
//
//			if (file != null) {
//				IWDCachedWebResource cachedResource = WDWebResource.getWebResource(file, type);
//				try {
//					String imgUrl = cachedResource.getURL();
//					wdContext.currentLocalLoadImgElement().setResourceUrl(imgUrl);
//				} catch (Exception e) {
//					messageManager.reportException(" Exception in Load Image = " + e, false);
//				}
//			}
//
//		}

		switch (CRStatus) {
			case 0 :
				/***************************************
				** INIT OF GP PROCESS:
				** Set Responsabile SIOR;
				** Load UserList default Values;
				** Load Priority and Sap Module fron KM
				****************************************/
				wdContext.currentCRAttributesElement().setCRStatus((short)0);
				wdContext.currentCRAttributesElement().setResponsabileSIOR(controller.getCurrentUserLogonID());

				viewNodeElement.setCRStatusStr("Inserimento CR");
				controller.loadUserListDefaultValues();
				loadKMValues(wdThis, wdContext);
				setStatusInserimentoCRView(wdContext);
				break;
			case 1 :
				/***************************************
				** The init state = 0, the next state = 2 
				** or 7. So state = 1 is impossible.
				** TO BE DONE Change all status???
				***************************************/
				messageManager.reportException(" PROCESSING ERROR: " + " INVALID STATUS = [" + CRStatus + "]", false);
				break;
			case 2 :
				viewNodeElement.setCRStatusStr("Revisione CR");
				//setReaderCommentEnableField(controller.getCurrentUserLogonID(), wdContext);
				node = rootNode.getChildNode("ReaderList", IWDNode.LEAD_SELECTION);
				controller.populateLocalUserList(node, "ReaderID");
				setRoleLeadSelection(wdThis, wdContext, REVISORI);

				setStatusRevisioneCRView(wdContext);
				break;
			case 3 :
				viewNodeElement.setCRStatusStr("Approvazione CR");
				node = rootNode.getChildNode("ApprovalList", IWDNode.LEAD_SELECTION);
				controller.populateLocalUserList(node, "ApprovalID");
				setRoleLeadSelection(wdThis, wdContext, APPROVATORI);
				setStatusApprovazioneCRView(wdContext);
				break;
			case 4 :
				viewNodeElement.setCRStatusStr("Accettazione CR");
				node = rootNode.getChildNode("AccettazioneList", IWDNode.LEAD_SELECTION);
				controller.populateLocalUserList(node, "AccettazioneID");
				setRoleLeadSelection(wdThis, wdContext, ACCETTATORI);
				setStatusAccettazioneCRCRView(wdContext);
				break;
			case 5 :
				viewNodeElement.setCRStatusStr("Avanzamento in Test CR");
				node = rootNode.getChildNode("ITList", IWDNode.LEAD_SELECTION);
				controller.populateLocalUserList(node, "ITID");
				setRoleLeadSelection(wdThis, wdContext, GRUPPOIT);
				setStatusInTestCRView(wdContext);
				break;
			case 6 :
				viewNodeElement.setCRStatusStr("Avanzamento in Prod CR");
				node = rootNode.getChildNode("ITList", IWDNode.LEAD_SELECTION);
				controller.populateLocalUserList(node, "ITID");
				setRoleLeadSelection(wdThis, wdContext, GRUPPOIT);
				setStatusInProdCRView(wdContext);
				break;
			case 7 :
				viewNodeElement.setCRStatusStr("Update CR SAP REQUEST");

				/***************************************
				** Test 12.11.2009.
				** Not call controller.loadUserListDefaultValues();
				** Because during this method
				** All User Context are cleaned.
				***************************************/
				//controller.loadUserListDefaultValues();
				loadKMValues(wdThis, wdContext);
				setStatusUpdateCRView(wdContext);
				break;
			default :
				messageManager.reportException(" PROCESSING ERROR: " + " INVALID STATUS = [" + CRStatus + "]", false);
		}

    //@@end
  }

  //@@begin javadoc:onActionApprovaGPStep(ServerEvent)
	/** Declared validating event handler. */
  //@@end
  public void onActionApprovaGPStep(com.sap.tc.webdynpro.progmodel.api.IWDCustomEvent wdEvent )
  {
    //@@begin onActionApprovaGPStep(ServerEvent)

		String actionMsg = wdThis.wdGetChangeRequestSAPController().getActionPerformedMsg(currentStatusCod, true);

		//wdThis.wdGetChangeRequestSAPController().UpdateActionPerformed(actionMsg);
		wdContext.currentResultStateElement().setResultStateValue(RESULT_STATE_APPROVATO);

		// Call update CRSAP BackEnd Archive 
		// Before complete Guided Procedure Step.
		IPublicUpdateCRSAPCust updateController = wdThis.wdGetUpdateCRSAPCustController();
		short rfc_result = updateController.executeZcrsap_Update_Input();
		ICRSapUpdateOutputElement crSapUpdateOutputElement = wdContext.currentCRSapUpdateOutputElement();

		if (rfc_result == 0) {
			// No Exception in Calling RFC Update Funzion
			// Check return Code From RFC Function
			if (crSapUpdateOutputElement != null) {
				int returnCode = crSapUpdateOutputElement.getReturn_Code();

				if (returnCode != 0) {
					String errorMsg = crSapUpdateOutputElement.getReturn_Msg();
					messageManager.reportException(
						"Errore nella Funzione di Aggiornamento Archivio. " + "Errore = " + errorMsg,
						false);
					return;
				} else {
					messageManager.reportSuccess("Aggiornamento Archivio Avvenuto con Successo");
				}
			} else {
				messageManager.reportException("Nessun Ritorno nella Funzione di Aggiornamento", false);
				return;
			}
		} else {
			return;
		}

		wdThis.wdGetChangeRequestSAPController().UpdateActionPerformed(actionMsg);
		// Call CR SAP Archive Update to ActionPerformed Update
		updateController.executeZcrsap_Update_Input();
		wdThis.wdGetChangeRequestSAPController().complete();

    //@@end
  }

  //@@begin javadoc:onActionRifiutaGPStep(ServerEvent)
	/** Declared validating event handler. */
  //@@end
  public void onActionRifiutaGPStep(com.sap.tc.webdynpro.progmodel.api.IWDCustomEvent wdEvent )
  {
    //@@begin onActionRifiutaGPStep(ServerEvent)

		//wdContext.currentCRAttributesElement().setCRStatus((short)7);

		String actionMsg = wdThis.wdGetChangeRequestSAPController().getActionPerformedMsg(currentStatusCod, false);

		//wdThis.wdGetChangeRequestSAPController().UpdateActionPerformed(actionMsg);

		wdContext.currentResultStateElement().setResultStateValue(RESULT_STATE_RIFIUTATO);

		// Call update CRSAP BackEnd Archive 
		// Before complete Guided Procedure Step.
		IPublicUpdateCRSAPCust updateController = wdThis.wdGetUpdateCRSAPCustController();
		short rfc_result = updateController.executeZcrsap_Update_Input();
		ICRSapUpdateOutputElement crSapUpdateOutputElement = wdContext.currentCRSapUpdateOutputElement();

		if (rfc_result == 0) {
			// No Exception in Calling RFC Update Funzion
			// Check return Code From RFC Function
			if (crSapUpdateOutputElement != null) {
				int returnCode = crSapUpdateOutputElement.getReturn_Code();

				if (returnCode != 0) {
					String errorMsg = crSapUpdateOutputElement.getReturn_Msg();
					messageManager.reportException(
						"Errore nella Funzione di Aggiornamento Archivio. " + "Errore = " + errorMsg,
						false);
					return;
				} else {
					messageManager.reportSuccess("Aggiornamento Archivio Avvenuto con Successo");
				}
			} else {
				messageManager.reportException("Nessun Ritorno nella Funzione di Aggiornamento", false);
				return;
			}
		} else {
			return;
		}

		wdThis.wdGetChangeRequestSAPController().UpdateActionPerformed(actionMsg);
		updateController.executeZcrsap_Update_Input();
		wdThis.wdGetChangeRequestSAPController().complete();

    //@@end
  }

  //@@begin javadoc:onActionstartGuidedProcedure(ServerEvent)
	/** Declared validating event handler. */
  //@@end
  public void onActionstartGuidedProcedure(com.sap.tc.webdynpro.progmodel.api.IWDCustomEvent wdEvent )
  {
    //@@begin onActionstartGuidedProcedure(ServerEvent)

		String msg = wdThis.wdGetChangeRequestSAPController().getActionPerformedMsg(currentStatusCod, true);
		//wdThis.wdGetChangeRequestSAPController().UpdateActionPerformed(msg);

		populateCommentsTable();

		short archive_result = createSapCrArchive();

		if (archive_result == 0) {
			byte[] crsapID = wdContext.currentOutputElement().getReq_Id();
			wdContext.currentResultStateElement().setCrsapID(crsapID);
			wdThis.wdGetChangeRequestSAPController().UpdateActionPerformed(msg);
		} else {
			return;
		}
		/************************
		** Check Error from
		** Archive Function.
		
		byte[] crsapID = wdContext.currentOutputElement().getReq_Id();
		messageManager.reportSuccess("crsapId = [" + crsapID + "]");
		
		if (crsapID == null || crsapID.length != 16 )
		{
			messageManager.reportException("Impossibile salvare la persistenza dei dati", false);
			return;
		}
		else
		{
			wdContext.currentResultStateElement().setCrsapID(crsapID);
		}
		************************/
		int result = wdThis.wdGetChangeRequestSAPController().startGuidedProcedure();

		switch (result) {
			case 0 :
				messageManager.reportSuccess("Operazione Conclusa con successo");

				/************************************
				** Update SAP Archive with 
				** Comment Action.
				************************************/
				short rfc_result = wdThis.wdGetUpdateCRSAPCustController().executeZcrsap_Update_Input();
				ICRSapUpdateOutputElement crSapUpdateOutputElement = wdContext.currentCRSapUpdateOutputElement();

				if (rfc_result == 0) {
					if (crSapUpdateOutputElement != null) {
						int returnCode = crSapUpdateOutputElement.getReturn_Code();

						if (returnCode != 0) {
							String errorMsg = crSapUpdateOutputElement.getReturn_Msg();
							messageManager.reportException(
								"Errore nella Funzione di Aggiornamento Archivio. " + "Errore = " + errorMsg,
								false);
							return;
						} else {
							//mx -> Se è andato tutto bene procedo al salvataggio degli attachments
							saveAttachments();
							messageManager.reportSuccess("Aggiornamento Archivio Avvenuto con Successo");
						}
					} else {
						messageManager.reportException("Nessun Ritorno nella Funzione di Aggiornamento", false);
						return;
					}
				} else {
					return;
				}

				wdContext.currentViewNodeElement().setStartGPButtonVisibility(WDVisibility.NONE);
				break;
			case 1 :
				break;
			default :
				}

    //@@end
  }

  //@@begin javadoc:onActionaddUser(ServerEvent)
	/** Declared validating event handler. */
  //@@end
  public void onActionaddUser(com.sap.tc.webdynpro.progmodel.api.IWDCustomEvent wdEvent )
  {
    //@@begin onActionaddUser(ServerEvent)
		wdContext.currentViewNodeElement().setSearchReaderUserVisibility(WDVisibility.VISIBLE);

		// Clean Previous Search Criteria and Result
		wdContext.nodeUserSearchResult().bind((Collection) null);
		wdContext.currentUserSearchElement().setSearchReader("");

    //@@end
  }

  //@@begin javadoc:onActionselectionRoleGroup(ServerEvent)
	/*************************************** 
	** onActionselectionRoleGroup. 
	** Populate LocalUserList Context Node
	** with Values from UserList selected
	***************************************/
  //@@end
  public void onActionselectionRoleGroup(com.sap.tc.webdynpro.progmodel.api.IWDCustomEvent wdEvent )
  {
    //@@begin onActionselectionRoleGroup(ServerEvent)

		IPublicChangeRequestSAP controller = wdThis.wdGetChangeRequestSAPController();

		// Clean Local List Table.
		wdContext.currentViewNodeElement().setSearchReaderUserVisibility(WDVisibility.NONE);
		wdContext.nodeLocalUserList().bind((Collection) null);

		// Retrieve the selected value from combo
		IRoleValuesElement valuesElement = wdContext.currentRoleValuesElement();

		if (valuesElement == null) {
			messageManager.reportException("Invalid Combo Selection", false);
			return;
		}

		String roleValue = valuesElement.getRoleValue();

		if (roleValue.equals(REVISORI)) {
			IWDNode node = wdContext.nodeReaderList();
			controller.populateLocalUserList(node, "ReaderID");
			//populateLocalListFromReader();
		} else if (roleValue.equals(APPROVATORI)) {
			IWDNode node = wdContext.nodeApprovalList();
			controller.populateLocalUserList(node, "ApprovalID");
			//populateLocalListFromApprovatori();
		} else if (roleValue.equals(ACCETTATORI)) {
			IWDNode node = wdContext.nodeAccettazioneList();
			controller.populateLocalUserList(node, "AccettazioneID");
			//populateLocalListFromAccettatori();
		} else if (roleValue.equals(GRUPPOIT)) {
			IWDNode node = wdContext.nodeITList();
			controller.populateLocalUserList(node, "ITID");
			//populateLocalListFromIT();
		} else {
			messageManager.reportException("PROCESSING ERROR: Invalid Role Value", false);
			return;
		}

    //@@end
  }

  //@@begin javadoc:onActionsearchPortalUsers(ServerEvent)
	/** Declared validating event handler. */
  //@@end
  public void onActionsearchPortalUsers(com.sap.tc.webdynpro.progmodel.api.IWDCustomEvent wdEvent )
  {
    //@@begin onActionsearchPortalUsers(ServerEvent)

		String searchValue = wdContext.currentUserSearchElement().getSearchReader();

		wdContext.nodeUserSearchResult().bind((Collection) null);

		try {
			IUserFactory userFactory = UMFactory.getUserFactory();
			IUserSearchFilter userFilter = userFactory.getUserSearchFilter();
			userFilter.setLastName(searchValue, ISearchAttribute.LIKE_OPERATOR, false);

			ISearchResult searchResult = userFactory.searchUsers(userFilter);

			if (searchResult.getState() == ISearchResult.SEARCH_RESULT_OK) {
				while (searchResult.hasNext()) {
					String uniqueID = (String)searchResult.next();
					messageManager.reportSuccess("uniqueID = [" + uniqueID + "]");
					IUser user = userFactory.getUser(uniqueID);
					//user.getEmail();
					IUserAccount[] userAccounts = user.getUserAccounts();

					if (userAccounts != null) {
						IUserSearchResultElement userSearchResultElement =
							wdContext.nodeUserSearchResult().createUserSearchResultElement();

						messageManager.reportSuccess("Account LoginUid = [" + userAccounts[0].getLogonUid() + "]");
						userSearchResultElement.setUserID(userAccounts[0].getLogonUid());
						wdContext.nodeUserSearchResult().addElement(userSearchResultElement);
					}

				}
			} else {
				messageManager.reportException("Exception Seaching users ....", false);
			}
		} catch (Exception e) {
			messageManager.reportException("Exception Seaching users. " + "Exception = " + e, false);
		}

    //@@end
  }

  //@@begin javadoc:onActionaddUserToList(ServerEvent)
	/**************************************** 
	** onActionaddUserToList Definition 
	** From SearchResult UserAccount Table
	** Insert into LocalUserTable and User 
	** Table ( depending on Role Selected)
	** all Selected User.
	****************************************/
  //@@end
  public void onActionaddUserToList(com.sap.tc.webdynpro.progmodel.api.IWDCustomEvent wdEvent )
  {
    //@@begin onActionaddUserToList(ServerEvent)

		IUserSearchResultNode resultNode = wdContext.nodeUserSearchResult();

		int size = resultNode.size();
		boolean selectedItem = false;

		try {
			for (int k = 0; k < size; k++) {
				// Check if the current Element
				// is selected
				if (resultNode.isMultiSelected(k)) {
					selectedItem = true;
					String logonID = resultNode.getUserSearchResultElementAt(k).getUserID();

					// Add Element to LocalUserList
					ILocalUserListElement localUserElement = wdContext.nodeLocalUserList().createLocalUserListElement();

					localUserElement.setLocalUserID(logonID);
					wdContext.nodeLocalUserList().addElement(localUserElement);
					// Add Element to UserList f(Role Selected)
					addElementToUserList(logonID);
				}
			}

			if (!selectedItem) {
				messageManager.reportWarning("Select an User before add ....");
			}

		} catch (Exception e) {
			messageManager.reportException(e.getMessage(), false);
		}

    //@@end
  }

  //@@begin javadoc:onActionremoveUserFromLocalUserList(ServerEvent)
	/*************************************** 
	** Declared validating event handler. 
	** Remove an user from LocalUserList. 
	** Basing on Selected Role remove the same
	** user(s) from the corrisponding user table
	*****************************************/
  //@@end
  public void onActionremoveUserFromLocalUserList(com.sap.tc.webdynpro.progmodel.api.IWDCustomEvent wdEvent )
  {
    //@@begin onActionremoveUserFromLocalUserList(ServerEvent)

		// Check Role Selected

		IRoleValuesElement roleElement = wdContext.currentRoleValuesElement();

		if (roleElement == null) {
			messageManager.reportException("PROCESSING ERROR: NO ROLE SELECTED", false);
			return;
		}

		// Retrieve Selected Users of LocalUserTable List
		ILocalUserListNode localUserList = wdContext.nodeLocalUserList();
		int localUserListSize = localUserList.size();
		Collection selectedUsers = new ArrayList();
		ILocalUserListElement localUserListElement = null;

		for (int k = localUserListSize - 1; k >= 0; k--) {
			if (localUserList.isMultiSelected(k)) {
				localUserListElement = localUserList.getLocalUserListElementAt(k);

				String logonID = localUserListElement.getLocalUserID();
				selectedUsers.add(logonID);

				localUserList.removeElement(localUserListElement);
			}

		}

		String roleValue = roleElement.getRoleValue();

		if (roleValue.equals(REVISORI)) {
			removeSelectedUserFromReader(selectedUsers);
		} else if (roleValue.equals(APPROVATORI)) {
			removeSelectedUserFromApprovatori(selectedUsers);
		} else if (roleValue.equals(ACCETTATORI)) {
			removeSelectedUserFromAccettatori(selectedUsers);
		} else if (roleValue.equals(GRUPPOIT)) {
			removeSelectedUserFromIT(selectedUsers);
		} else {
			messageManager.reportException("PROCESSING ERROR: Invalid Role Value", false);
			return;
		}

    //@@end
  }

  //@@begin javadoc:onActionsendToReader(ServerEvent)
	/****************************************** 
	** The following Method is called in the 
	** Update State. 
	** The complte method must be called in order
	** to enter in reader process step.
	******************************************/
  //@@end
  public void onActionsendToReader(com.sap.tc.webdynpro.progmodel.api.IWDCustomEvent wdEvent )
  {
    //@@begin onActionsendToReader(ServerEvent)

		/**********************
		** Update Action Table
		***********************/
		String msg = wdThis.wdGetChangeRequestSAPController().getActionPerformedMsg(currentStatusCod, true);
		wdThis.wdGetChangeRequestSAPController().UpdateActionPerformed(msg);

		/***************************
		** Before Attach File and
		** Complete GP STEP 
		** Call RFC Function for
		** Archive Update
		***************************/
		IPublicUpdateCRSAPCust updateController = wdThis.wdGetUpdateCRSAPCustController();
		wdContext.currentResultStateElement().setResultStateValue(RESULT_STATE_APPROVATO);
		short rfc_result = updateController.executeZcrsap_Update_Input();
		ICRSapUpdateOutputElement crSapUpdateOutputElement = wdContext.currentCRSapUpdateOutputElement();

		if (rfc_result == 0) {
			// No Exception in Calling RFC Update Function
			// Check return Code From RFC Function
			if (crSapUpdateOutputElement != null) {
				int returnCode = crSapUpdateOutputElement.getReturn_Code();

				if (returnCode != 0) {
					String errorMsg = crSapUpdateOutputElement.getReturn_Msg();
					messageManager.reportException(
						"Errore nella Funzione di Aggiornamento Archivio. " + "Errore = " + errorMsg,
						false);
					return;
				} else {
					//mx -> Se è andato tutto bene, posso procedere al salvataggio degli attachments
					saveAttachments();
					messageManager.reportSuccess("Aggiornamento Archivio Avvenuto con Successo");
				}
			} else {
				messageManager.reportException("Nessun Ritorno nella Funzione di Aggiornamento", false);
				return;
			}
		} else {
			return;
		}
		
		wdThis.wdGetChangeRequestSAPController().complete();

    //@@end
  }

  //@@begin javadoc:onActionsapModuleSelection(ServerEvent)
	/** Declared validating event handler. */
  //@@end
  public void onActionsapModuleSelection(com.sap.tc.webdynpro.progmodel.api.IWDCustomEvent wdEvent )
  {
    //@@begin onActionsapModuleSelection(ServerEvent)

		String sapModule = wdContext.currentSapModuleSelectionElement().getSapModule();

		//mx -> Recupero istanza di Calendar esplicitando TimeZone e Locale (sbagliato mese, non ora...)
		//Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT+1:00"), Locale.ITALY);
		Calendar calendar = Calendar.getInstance();
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		//mx -> Months are zero - indexed...
		int month = calendar.get(Calendar.MONTH) + 1;
		int year = calendar.get(Calendar.YEAR);
		//int hour = calendar.get(Calendar.HOUR);
		//int min = calendar.get(Calendar.MINUTE);
		//int sec = calendar.get(Calendar.SECOND);

		//mx -> Recupero progressivo
		String progressivo = wdThis.wdGetChangeRequestSAPController().getNextCounter();

		//mx -> codiceCR non deve più contenere ora-min-sec, ma il progressivo;
		String codiceCR = sapModule + "-" + day + "-" + month + "-" + year + "-" + progressivo;
		//+ "-" + hour + "-" + min + "-" + sec;

		wdContext.currentCRAttributesElement().setCodiceCR(codiceCR);

    //@@end
  }

  //@@begin javadoc:onActionprioritySelection(ServerEvent)
	/** Declared validating event handler. */
  //@@end
  public void onActionprioritySelection(com.sap.tc.webdynpro.progmodel.api.IWDCustomEvent wdEvent )
  {
    //@@begin onActionprioritySelection(ServerEvent)

		String priority = wdContext.currentPrioritySelectionElement().getPriority();

		wdContext.currentCRAttributesElement().setPriorita(priority);

    //@@end
  }

  //@@begin javadoc:onActionShowImage(ServerEvent)
	/** Declared validating event handler. */
  //@@end
  public void onActionShowImage(com.sap.tc.webdynpro.progmodel.api.IWDCustomEvent wdEvent )
  {
    //@@begin onActionShowImage(ServerEvent)

		//byte[] file = wdContext.currentLocalLoadImgElement().getFData();
		//String fileName = wdContext.currentLocalLoadImgElement().getFName();
		IWDResource file = wdContext.currentLocalLoadImgElement().getImgData();
		if (file != null) {
			try {
				IControllerlLoadImgElement controllerElement =
					wdContext.nodeControllerlLoadImg().createControllerlLoadImgElement();

				//controllerElement.setFData(file);
				//controllerElement.setFName(fileName);
				controllerElement.setFName(file.getResourceName());
				//IWDResource resource = WDResourceFactory.createResource( file, fileName, WDWebResourceType.GIF_IMAGE);
				//controllerElement.setImgData( resource);
				controllerElement.setImgData(file);
				controllerElement.setFData(file.read(false).toString().getBytes());

				wdContext.nodeControllerlLoadImg().addElement(controllerElement);

				//wdContext.currentControllerlLoadImgElement().setFData(file);
				//wdContext.currentControllerlLoadImgElement().setFName(fileName);

				//messageManager.reportSuccess("Il File " + fileName + " è stato caricato con successo");
				messageManager.reportSuccess("Il File " + file.getResourceName() + " è stato caricato con successo");
			} catch (Exception e) {
				messageManager.reportException(" Exception in ShowImage = " + e, false);
			}

			//wdContext.currentViewNodeElement().setShowImageVisible(WDVisibility.VISIBLE);

		}

    //@@end
  }

  /*
   * The following code section can be used for any Java code that is 
   * not to be visible to other controllers/views or that contains constructs
   * currently not supported directly by Web Dynpro (such as inner classes or
   * member variables etc.). </p>
   *
   * Note: The content of this section is in no way managed/controlled
   * by the Web Dynpro Designtime or the Web Dynpro Runtime. 
   */
  //@@begin others

	private static IWDMessageManager messageManager;
	private static short currentStatusCod;

	private short createSapCrArchive() {
		return wdThis.wdGetCRSAPArchiveCustController().executeZcrsap_Create_Input();
	}

	/***********************************************
	** setReaderCommentEnableField
	** Input: CurrentUserID.
	** Set Comment Field = true <=>
	** ReaderComments.User = CurrentUser.
	***********************************************/
	private static void setReaderCommentEnableField(
		String currentUserID,
		IPrivateChangeRequestSAPView.IContextNode wdContext,
		int status) {
		IReaderCommentsNode commentsNode = wdContext.nodeReaderComments();

		if (commentsNode != null && commentsNode.size() > 0) {
			for (int k = 0; k < commentsNode.size(); k++) {
				IReaderCommentsElement commentsElement = commentsNode.getReaderCommentsElementAt(k);

				if ((commentsElement.getUser()).equals(currentUserID) && status == 2)
					commentsElement.setEnableField(true);
				else
					commentsElement.setEnableField(false);

			}
		}
	}

	/***********************************************
	** Remove All Selected Users of LocalUSer Table
	** from UserReaderTable 
	***********************************************/
	private void removeSelectedUserFromReader(Collection selectedUsers) {
		IReaderListNode readerNode = wdContext.nodeReaderList();

		int size = readerNode.size();
		int deletedElements = 0;

		for (int k = size - 1; k >= 0; k--) {
			IReaderListElement element = readerNode.getReaderListElementAt(k);

			String value = element.getReaderID();

			if (selectedUsers.contains(value)) {
				readerNode.removeElement(element);
				deletedElements++;
			}
		}

		if (deletedElements != selectedUsers.size()) {
			messageManager.reportException(
				"PROCESSING ERROR: readerNodeSize = [" + size + "] elements to delete = [" + selectedUsers.size() + "]",
				false);
		}
	}

	/***********************************************
	** Remove All Selected Users of LocalUSer Table
	** from UserApprovatoriTable 
	***********************************************/
	private void removeSelectedUserFromApprovatori(Collection selectedUsers) {
		IApprovalListNode approvalNode = wdContext.nodeApprovalList();

		int size = approvalNode.size();
		int deletedElements = 0;

		for (int k = size - 1; k >= 0; k--) {
			IApprovalListElement element = approvalNode.getApprovalListElementAt(k);

			String value = element.getApprovalID();

			if (selectedUsers.contains(value)) {
				approvalNode.removeElement(element);
				deletedElements++;
			}
		}

		if (deletedElements != selectedUsers.size()) {
			messageManager.reportException(
				"PROCESSING ERROR: approvalNodeSize = [" + size + "] elements to delete = [" + selectedUsers.size() + "]",
				false);
		}

	}

	/***********************************************
	** Remove All Selected Users of LocalUSer Table
	** from UserAccettatoriTable 
	***********************************************/
	private void removeSelectedUserFromAccettatori(Collection selectedUsers) {
		IAccettazioneListNode accettazioneNode = wdContext.nodeAccettazioneList();

		int size = accettazioneNode.size();
		int deletedElements = 0;

		for (int k = size - 1; k >= 0; k--) {
			IAccettazioneListElement element = accettazioneNode.getAccettazioneListElementAt(k);

			String value = element.getAccettazioneID();

			if (selectedUsers.contains(value)) {
				accettazioneNode.removeElement(element);
				deletedElements++;
			}
		}

		if (deletedElements != selectedUsers.size()) {
			messageManager.reportException(
				"PROCESSING ERROR: accettazioneNodeSize = ["
					+ size
					+ "] elements to delete = ["
					+ selectedUsers.size()
					+ "]",
				false);
		}
	}

	/***********************************************
	** Remove All Selected Users of LocalUSer Table
	** from UserITTable 
	***********************************************/
	private void removeSelectedUserFromIT(Collection selectedUsers) {
		IITListNode itNode = wdContext.nodeITList();

		int size = itNode.size();
		int deletedElements = 0;

		for (int k = size - 1; k >= 0; k--) {
			IITListElement element = itNode.getITListElementAt(k);

			String value = element.getITID();

			if (selectedUsers.contains(value)) {
				itNode.removeElement(element);
				deletedElements++;
			}
		}

		if (deletedElements != selectedUsers.size()) {
			messageManager.reportException(
				"PROCESSING ERROR: itNodeSize = [" + size + "] elements to delete = [" + selectedUsers.size() + "]",
				false);
		}

	}

	/************************************
	** Input: logonID of a Account User
	** Add a new Element with logonID 
	** Value to a Context Node depending
	** on Role Selected.
	** If non Role is Selected then Thows
	** an Exception.
	************************************/
	private void addElementToUserList(String logonID) throws Exception {
		IRoleValuesElement roleElement = wdContext.currentRoleValuesElement();

		if (roleElement == null) {
			throw new Exception("addElementToUserList: PROCESSING ERROR: " + "NO ROLE ELEMENT SELECETED");
		}

		String roleValue = roleElement.getRoleValue();

		if (roleValue.equals(REVISORI)) {
			IReaderListElement readerElement = wdContext.nodeReaderList().createReaderListElement();

			readerElement.setReaderID(logonID);
			wdContext.nodeReaderList().addElement(readerElement);
		} else if (roleValue.equals(APPROVATORI)) {
			IApprovalListElement approvalListElement = wdContext.nodeApprovalList().createApprovalListElement();

			approvalListElement.setApprovalID(logonID);
			wdContext.nodeApprovalList().addElement(approvalListElement);
		} else if (roleValue.equals(ACCETTATORI)) {
			IAccettazioneListElement accettazioneListElement =
				wdContext.nodeAccettazioneList().createAccettazioneListElement();

			accettazioneListElement.setAccettazioneID(logonID);
			wdContext.nodeAccettazioneList().addElement(accettazioneListElement);
		} else if (roleValue.equals(GRUPPOIT)) {
			IITListElement itListElement = wdContext.nodeITList().createITListElement();

			itListElement.setITID(logonID);
			wdContext.nodeITList().addElement(itListElement);
		} else {
			throw new Exception("addElementToUserList: " + "PROCESSING ERROR: Invalid Role Value");
		}

	}

	/**********************
	** Retrieve Role Values
	** Approvatore,
	** Reader (Revisore);
	** IT;
	** Accettazione;
	**********************/
	private Collection retrieveRoleValue() {
		ArrayList arrayList = new ArrayList();
		/*
		arrayList.add("Revisori");
		arrayList.add("Approvatori");
		arrayList.add("Accettatori");
		*/

		// Try to delete a gruop IT from selection!!!!
		//arrayList.add(GRUPPOIT);
		arrayList.add("01_Richiedenti");
		arrayList.add("02_Approvatori");
		arrayList.add("03_ApprovatoriFinali");

		return arrayList;
	}

	static private void loadKMValues(
		IPrivateChangeRequestSAPView wdThis,
		IPrivateChangeRequestSAPView.IContextNode wdContext) {
		IPublicChangeRequestSAP controller = wdThis.wdGetChangeRequestSAPController();

		HashSet sapModules = controller.getSapModules();
		HashSet priorities = controller.getPriority();

		/*********************************
		** Clean Previous Values
		*********************************/
		wdContext.nodeSapModuleSelection().bind((Collection) null);
		wdContext.nodePrioritySelection().bind((Collection) null);

		/*********************************
		** Load Values from KM
		*********************************/
		loadContextSapModules(wdThis, wdContext, sapModules);
		loadContextPriority(wdThis, wdContext, priorities);
	}

	private static void loadContextSapModules(
		IPrivateChangeRequestSAPView wdThis,
		IContextNode wdContext,
		HashSet sapModules) {
		ISapModuleSelectionNode moduleSelectionNode = wdContext.nodeSapModuleSelection();

		Iterator iter = sapModules.iterator();

		while (iter.hasNext()) {
			String moduleType = (String)iter.next();

			ISapModuleSelectionElement moduleSelectionElement = moduleSelectionNode.createSapModuleSelectionElement();

			moduleSelectionElement.setSapModule(moduleType);
			moduleSelectionNode.addElement(moduleSelectionElement);
		}

	}

	private static void loadContextPriority(
		IPrivateChangeRequestSAPView wdThis,
		IContextNode wdContext,
		HashSet priorities) {
		IPrioritySelectionNode prioritySelectionNode = wdContext.nodePrioritySelection();

		Iterator iter = priorities.iterator();

		while (iter.hasNext()) {
			String priority = (String)iter.next();

			IPrioritySelectionElement prioritySelectionElement = prioritySelectionNode.createPrioritySelectionElement();

			prioritySelectionElement.setPriority(priority);
			prioritySelectionNode.addElement(prioritySelectionElement);
		}

	}

	/***************************************
	** Set Combo RoleName as LeadSelection
	** and load corrisponding values
	** to LocalUserList Table
	***************************************/
	static private void setRoleLeadSelection(
		IPrivateChangeRequestSAPView wdThis,
		IPrivateChangeRequestSAPView.IContextNode wdContext,
		String roleName) {

		IRoleValuesNode roleValuesNode = wdContext.nodeRoleValues();
		int size = roleValuesNode.size();
		int index = -1;

		for (int k = 0; k < size; k++) {
			IRoleValuesElement element = roleValuesNode.getRoleValuesElementAt(k);

			if (element.getRoleValue().equals(roleName)) {
				index = k;
				break;
			}
		}

		/*
		if (index < 0 )
		{
			messageManager.reportException("PROCESSING ERROR: UNABLE TO FIND " +
				"CORRECT ROLE NAME", false);
			return;
		}
		*/
		if (index >= 0)
			roleValuesNode.setLeadSelection(index);

		// Find Context Node to populate LocalUserList
		IPublicChangeRequestSAP controller = wdThis.wdGetChangeRequestSAPController();

		if (roleName.equals(REVISORI)) {
			IWDNode node = wdContext.nodeReaderList();
			//node.bind((Collection)null);
			controller.populateLocalUserList(node, "ReaderID");

		} else if (roleName.equals(APPROVATORI)) {
			IWDNode node = wdContext.nodeApprovalList();
			//node.bind((Collection)null);
			controller.populateLocalUserList(node, "ApprovalID");
		} else if (roleName.equals(ACCETTATORI)) {
			IWDNode node = wdContext.nodeAccettazioneList();
			//node.bind((Collection)null);
			controller.populateLocalUserList(node, "AccettazioneID");
		} else if (roleName.equals(GRUPPOIT)) {
			IWDNode node = wdContext.nodeITList();
			//node.bind((Collection)null);
			controller.populateLocalUserList(node, "ITID");
		}

	}

	/**********************************
	** Poppualte CommentsTable with a row
	** for every Reader User selectect
	** by user.
	** The selected Reader User are
	** inserted into nodeReaderList
	** context Node.
	**********************************/
	private void populateCommentsTable() {
		IReaderListNode readerNode = wdContext.nodeReaderList();
		IReaderListElement readerListElement;
		String readerID;

		IReaderCommentsNode commentNode = wdContext.nodeReaderComments();
		commentNode.bind((Collection) null);

		IReaderCommentsElement commentElement;

		for (int k = 0; k < readerNode.size(); k++) {
			readerListElement = readerNode.getReaderListElementAt(k);
			readerID = readerListElement.getReaderID();

			commentElement = commentNode.createReaderCommentsElement();
			commentElement.setUser(readerID);
			commentNode.addElement(commentElement);
		}
	}

	private void saveAttachments() {
		//mx -> Salvataggio allegati (sse c'è almeno un allegato caricato)
		wdComponentAPI.getMessageManager().reportSuccess("INIZIO");
		if (wdContext.nodeControllerlLoadImg().size() > 0) {
			//mx -> Recupero da UpdateCRSAPCust sapId
			byte[] crSapId = wdThis.wdGetUpdateCRSAPCustController().wdGetContext().currentZcrsap_Update_InputElement().getCrsap_Id();
			wdComponentAPI.getMessageManager().reportSuccess("CRSAPID: " + crSapId.toString());
			//mx -> Per ogni riga di ControllerLoadImg salvo documento associato (chiamo funzione che popola contesto del custom controller)
			for (int i = 0; i < wdContext.nodeControllerlLoadImg().size(); i++) {
				byte[] source = wdContext.nodeControllerlLoadImg().getControllerlLoadImgElementAt(i).getFData();
				String fileName = wdContext.nodeControllerlLoadImg().getControllerlLoadImgElementAt(i).getFName();
				String fileType = wdContext.nodeControllerlLoadImg().getControllerlLoadImgElementAt(i).getImgData().getResourceType().getHtmlMime();
				wdComponentAPI.getMessageManager().reportSuccess("Tipo " + i + ": " + fileType);
				wdThis.wdGetCRSAPAttachmentController().addAttachment(source, fileName, fileType, crSapId);
			}
			//mx -> Alla fine eseguo chiamata RFC
			wdThis.wdGetCRSAPAttachmentController().executeZcrsap_Add_Attachment_Input();
		}
	}

	/****************************************
	** Elenco dei Campi da Settare
	** 1. Visibilità Analisi Costi
	** 2. ReadOnly Analisi Costi
	** 3. Ricerca Utenti approvatori
	** 4. Ricerca utenti revisori
	****************************************/
	static private void setStatusInserimentoCRView(IPrivateChangeRequestSAPView.IContextNode wdContext) {
		IViewNodeElement viewNodeElement = wdContext.currentViewNodeElement();

		// Analisi Costi Properties
		viewNodeElement.setAnalisiCostiVisibility(WDVisibility.NONE);
		viewNodeElement.setAnalisiCostiEnable(false);
		viewNodeElement.setUserGroupsEnable(true);

		viewNodeElement.setSearchReaderUserVisibility(WDVisibility.NONE);
		viewNodeElement.setCRAttributesEnable(true);
		viewNodeElement.setActionButtonGroupVisibility(WDVisibility.NONE);
		viewNodeElement.setStartGPButtonVisibility(WDVisibility.VISIBLE);
		viewNodeElement.setSendToReaderVisibility(WDVisibility.NONE);
		viewNodeElement.setRoleSelectVisibility(WDVisibility.VISIBLE);
		viewNodeElement.setRoleSelectedVisibility(WDVisibility.NONE);
		viewNodeElement.setSAPModuleSelectionGroupVisibility(WDVisibility.VISIBLE);
		viewNodeElement.setRifiutaButtonVisibility(WDVisibility.VISIBLE);
		viewNodeElement.setReaderCommentVisibility(WDVisibility.NONE);
		viewNodeElement.setLaodImageVisible(WDVisibility.VISIBLE);
		viewNodeElement.setShowImageVisible(WDVisibility.NONE);
		viewNodeElement.setSoluzioneImplementataVisibility(WDVisibility.NONE);
		viewNodeElement.setAccettaButtontext("Accetta");
	}

	static private void setStatusRevisioneCRView(IPrivateChangeRequestSAPView.IContextNode wdContext) {
		IViewNodeElement viewNodeElement = wdContext.currentViewNodeElement();

		// Analisi Costi Properties
		viewNodeElement.setAnalisiCostiVisibility(WDVisibility.VISIBLE);
		viewNodeElement.setAnalisiCostiEnable(true);
		viewNodeElement.setUserGroupsEnable(false);

		viewNodeElement.setSearchReaderUserVisibility(WDVisibility.NONE);
		viewNodeElement.setCRAttributesEnable(false);

		viewNodeElement.setActionButtonGroupVisibility(WDVisibility.VISIBLE);
		viewNodeElement.setStartGPButtonVisibility(WDVisibility.NONE);
		viewNodeElement.setSendToReaderVisibility(WDVisibility.NONE);
		viewNodeElement.setRoleSelectVisibility(WDVisibility.NONE);
		viewNodeElement.setSAPModuleSelectionGroupVisibility(WDVisibility.NONE);
		viewNodeElement.setRoleSelectedVisibility(WDVisibility.VISIBLE);
		viewNodeElement.setRifiutaButtonVisibility(WDVisibility.VISIBLE);
		viewNodeElement.setReaderCommentVisibility(WDVisibility.VISIBLE);
		viewNodeElement.setLaodImageVisible(WDVisibility.NONE);
		viewNodeElement.setSoluzioneImplementataVisibility(WDVisibility.NONE);
		viewNodeElement.setAccettaButtontext("Accetta");

		if (wdContext.currentCRAttributesElement().getFDataImg() != null
			&& wdContext.currentCRAttributesElement().getFDataImg().length > 0) {
			wdContext.currentViewNodeElement().setShowImageVisible(WDVisibility.VISIBLE);
		}

	}

	static private void setStatusApprovazioneCRView(IPrivateChangeRequestSAPView.IContextNode wdContext) {
		IViewNodeElement viewNodeElement = wdContext.currentViewNodeElement();

		// Analisi Costi Properties
		viewNodeElement.setAnalisiCostiVisibility(WDVisibility.VISIBLE);
		viewNodeElement.setAnalisiCostiEnable(false);
		viewNodeElement.setUserGroupsEnable(false);

		viewNodeElement.setSearchReaderUserVisibility(WDVisibility.NONE);
		viewNodeElement.setCRAttributesEnable(false);

		viewNodeElement.setActionButtonGroupVisibility(WDVisibility.VISIBLE);
		viewNodeElement.setStartGPButtonVisibility(WDVisibility.NONE);
		viewNodeElement.setSendToReaderVisibility(WDVisibility.NONE);
		viewNodeElement.setRoleSelectVisibility(WDVisibility.NONE);
		viewNodeElement.setSAPModuleSelectionGroupVisibility(WDVisibility.NONE);
		viewNodeElement.setRoleSelectedVisibility(WDVisibility.VISIBLE);
		viewNodeElement.setRifiutaButtonVisibility(WDVisibility.VISIBLE);
		viewNodeElement.setReaderCommentVisibility(WDVisibility.VISIBLE);
		viewNodeElement.setLaodImageVisible(WDVisibility.NONE);
		viewNodeElement.setSoluzioneImplementataVisibility(WDVisibility.NONE);
		viewNodeElement.setAccettaButtontext("Accetta");

		if (wdContext.currentCRAttributesElement().getFDataImg() != null
			&& wdContext.currentCRAttributesElement().getFDataImg().length > 0) {
			wdContext.currentViewNodeElement().setShowImageVisible(WDVisibility.VISIBLE);
		}
	}

	static private void setStatusAccettazioneCRCRView(IPrivateChangeRequestSAPView.IContextNode wdContext) {
		IViewNodeElement viewNodeElement = wdContext.currentViewNodeElement();

		// Analisi Costi Properties
		viewNodeElement.setAnalisiCostiVisibility(WDVisibility.VISIBLE);
		viewNodeElement.setAnalisiCostiEnable(false);
		viewNodeElement.setUserGroupsEnable(false);

		viewNodeElement.setActionButtonGroupVisibility(WDVisibility.VISIBLE);
		viewNodeElement.setStartGPButtonVisibility(WDVisibility.NONE);

		viewNodeElement.setSearchReaderUserVisibility(WDVisibility.NONE);
		viewNodeElement.setCRAttributesEnable(false);
		viewNodeElement.setSendToReaderVisibility(WDVisibility.NONE);
		viewNodeElement.setRoleSelectVisibility(WDVisibility.NONE);
		viewNodeElement.setSAPModuleSelectionGroupVisibility(WDVisibility.NONE);
		viewNodeElement.setRoleSelectedVisibility(WDVisibility.VISIBLE);
		viewNodeElement.setRifiutaButtonVisibility(WDVisibility.VISIBLE);
		viewNodeElement.setReaderCommentVisibility(WDVisibility.VISIBLE);
		viewNodeElement.setSoluzioneImplementataVisibility(WDVisibility.NONE);
		viewNodeElement.setLaodImageVisible(WDVisibility.NONE);
		viewNodeElement.setAccettaButtontext("Accetta");

		/*
		if ( wdContext.currentCRAttributesElement().getFDataImg() != null &&
					wdContext.currentCRAttributesElement().getFDataImg().length > 0  )
		{
			wdContext.currentViewNodeElement().setShowImageVisible(WDVisibility.VISIBLE);
		}
		
		*/

	}

	static private void setStatusInTestCRView(IPrivateChangeRequestSAPView.IContextNode wdContext) {
		IViewNodeElement viewNodeElement = wdContext.currentViewNodeElement();

		// Analisi Costi Properties
		viewNodeElement.setAnalisiCostiVisibility(WDVisibility.VISIBLE);
		viewNodeElement.setAnalisiCostiEnable(false);
		viewNodeElement.setUserGroupsEnable(false);

		viewNodeElement.setActionButtonGroupVisibility(WDVisibility.VISIBLE);
		viewNodeElement.setStartGPButtonVisibility(WDVisibility.NONE);
		viewNodeElement.setSendToReaderVisibility(WDVisibility.NONE);

		viewNodeElement.setSearchReaderUserVisibility(WDVisibility.NONE);
		viewNodeElement.setCRAttributesEnable(false);
		viewNodeElement.setRoleSelectVisibility(WDVisibility.NONE);
		viewNodeElement.setSAPModuleSelectionGroupVisibility(WDVisibility.NONE);
		viewNodeElement.setRoleSelectedVisibility(WDVisibility.VISIBLE);
		viewNodeElement.setRifiutaButtonVisibility(WDVisibility.NONE);
		viewNodeElement.setReaderCommentVisibility(WDVisibility.VISIBLE);
		viewNodeElement.setLaodImageVisible(WDVisibility.NONE);
		viewNodeElement.setSoluzioneImplementataVisibility(WDVisibility.NONE);
		viewNodeElement.setAccettaButtontext("CR In Test");

		if (wdContext.currentCRAttributesElement().getFDataImg() != null
			&& wdContext.currentCRAttributesElement().getFDataImg().length > 0) {
			wdContext.currentViewNodeElement().setShowImageVisible(WDVisibility.VISIBLE);
		}

	}

	static private void setStatusInProdCRView(IPrivateChangeRequestSAPView.IContextNode wdContext) {
		IViewNodeElement viewNodeElement = wdContext.currentViewNodeElement();

		// Analisi Costi Properties
		viewNodeElement.setAnalisiCostiVisibility(WDVisibility.VISIBLE);
		viewNodeElement.setAnalisiCostiEnable(false);
		viewNodeElement.setUserGroupsEnable(false);

		viewNodeElement.setActionButtonGroupVisibility(WDVisibility.VISIBLE);
		viewNodeElement.setStartGPButtonVisibility(WDVisibility.NONE);

		viewNodeElement.setSearchReaderUserVisibility(WDVisibility.NONE);
		viewNodeElement.setCRAttributesEnable(false);
		viewNodeElement.setSendToReaderVisibility(WDVisibility.NONE);
		viewNodeElement.setRoleSelectVisibility(WDVisibility.NONE);
		viewNodeElement.setSAPModuleSelectionGroupVisibility(WDVisibility.NONE);
		viewNodeElement.setRoleSelectedVisibility(WDVisibility.VISIBLE);
		viewNodeElement.setRifiutaButtonVisibility(WDVisibility.NONE);
		viewNodeElement.setReaderCommentVisibility(WDVisibility.VISIBLE);
		viewNodeElement.setLaodImageVisible(WDVisibility.NONE);
		viewNodeElement.setSoluzioneImplementataVisibility(WDVisibility.VISIBLE);
		//viewNodeElement.setS
		viewNodeElement.setAccettaButtontext("CR In Prod");

		if (wdContext.currentCRAttributesElement().getFDataImg() != null
			&& wdContext.currentCRAttributesElement().getFDataImg().length > 0) {
			wdContext.currentViewNodeElement().setShowImageVisible(WDVisibility.VISIBLE);
		}

	}

	static private void setStatusUpdateCRView(IPrivateChangeRequestSAPView.IContextNode wdContext) {
		IViewNodeElement viewNodeElement = wdContext.currentViewNodeElement();

		// Analisi Costi Properties
		viewNodeElement.setAnalisiCostiVisibility(WDVisibility.NONE);
		viewNodeElement.setAnalisiCostiEnable(false);

		viewNodeElement.setSearchReaderUserVisibility(WDVisibility.NONE);
		viewNodeElement.setCRAttributesEnable(true);
		viewNodeElement.setUserGroupsEnable(true);

		viewNodeElement.setActionButtonGroupVisibility(WDVisibility.NONE);
		viewNodeElement.setStartGPButtonVisibility(WDVisibility.NONE);
		viewNodeElement.setSendToReaderVisibility(WDVisibility.VISIBLE);
		viewNodeElement.setRoleSelectVisibility(WDVisibility.VISIBLE);
		viewNodeElement.setSAPModuleSelectionGroupVisibility(WDVisibility.NONE);
		viewNodeElement.setReaderCommentVisibility(WDVisibility.NONE);
		viewNodeElement.setRoleSelectedVisibility(WDVisibility.NONE);
		viewNodeElement.setLaodImageVisible(WDVisibility.VISIBLE);
		viewNodeElement.setSoluzioneImplementataVisibility(WDVisibility.NONE);

		if (wdContext.currentCRAttributesElement().getFDataImg() != null
			&& wdContext.currentCRAttributesElement().getFDataImg().length > 0) {
			wdContext.currentViewNodeElement().setShowImageVisible(WDVisibility.VISIBLE);
		}

	}

	// Possible Result State and Result State Desc of CO.
	private static final String RESULT_STATE_DA_RIVEDERE = "DaRivedere";
	private static final String RESULT_STATE_DA_RIVEDERE_DESC = "Da Rivedere";
	private static final String RESULT_STATE_APPROVATO = "Approvato";
	private static final String RESULT_STATE_APPROVATO_DESC = "Approvato";
	private static final String RESULT_STATE_RIFIUTATO = "Rifiutato";
	private static final String RESULT_STATE_RIFIUTATO_DESC = "Rifiutato";

	// Role Definition
	/***************************************
	** ORIGINAL NAMES DEFINITION
	** arrayList.add("Revisori");
	** arrayList.add("Approvatori");
	** arrayList.add("Accettatori");
	***************************************/
	private static final String REVISORI = "01_Richiedenti";
	private static final String APPROVATORI = "02_Approvatori";
	private static final String ACCETTATORI = "03_ApprovatoriFinali";
	private static final String GRUPPOIT = "Gruppo IT";

  //@@end
}

// ---- content of obsolete user coding area(s) ----
//@@begin obsolete:javadoc:onActionLoadImage(ServerEvent)
//  /** Declared validating event handler. */
//@@end
//@@begin obsolete:onActionLoadImage(ServerEvent)
//    
//    
//	byte fileData[] = wdContext.currentTmpLoadImgElement().getFData();
//	String fileName = wdContext.currentTmpLoadImgElement().getFName();
//	
//	if ( fileData != null && fileData.length > 0 )
//	{
//		messageManager.reportSuccess("Load Iamge fileData Size = [" 
//			+ fileData.length + "]");
//	}
//	else
//	{
//		messageManager.reportSuccess("Unable to Laod File");
//	}
//    
//@@end
//@@begin obsolete:javadoc:onActionrifiuta(ServerEvent)
//  /** Declared validating event handler. */
//@@end
//@@begin obsolete:onActionrifiuta(ServerEvent)
//@@end
//@@begin obsolete:javadoc:onActionapprova(ServerEvent)
//  /** Declared validating event handler. */
//@@end
//@@begin obsolete:onActionapprova(ServerEvent)
//    
//@@end
