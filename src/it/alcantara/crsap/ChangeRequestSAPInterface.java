// ---------------------------------------------------------------------------
// This file has been generated partially by the Web Dynpro Code Generator.
// MODIFY CODE ONLY IN SECTIONS ENCLOSED BY @@begin AND @@end.
// ALL OTHER CHANGES WILL BE LOST IF THE FILE IS REGENERATED.
// ---------------------------------------------------------------------------
package it.alcantara.crsap;

// 
// IMPORTANT NOTE: 
// _ALL_ IMPORT STATEMENTS MUST BE PLACED IN THE FOLLOWING SECTION ENCLOSED
// BY @@begin imports AND @@end. FURTHERMORE, THIS SECTION MUST ALWAYS CONTAIN
// AT LEAST ONE IMPORT STATEMENT (E.G. THAT FOR IPrivateChangeRequestSAPInterface).
// OTHERWISE, USING THE ECLIPSE FUNCTION "Organize Imports" FOLLOWED BY
// A WEB DYNPRO CODE GENERATION (E.G. PROJECT BUILD) WILL RESULT IN THE LOSS
// OF IMPORT STATEMENTS.
//
//@@begin imports
import it.alcantara.crsap.wdp.IPrivateChangeRequestSAPInterface;
import it.alcantara.crsap.wdp.IPublicChangeRequestSAPInterface.IReaderListElement;
import it.alcantara.crsap.wdp.IPublicChangeRequestSAPInterface.IReaderListNode;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import com.sap.caf.eu.gp.co.api.GPCallableObjectFactory;
import com.sap.caf.eu.gp.co.api.GPWebDynproResourceAccessor;
import com.sap.caf.eu.gp.co.api.IGPCOResultStateInfo;
import com.sap.caf.eu.gp.co.api.IGPExecutionContext;
import com.sap.caf.eu.gp.co.api.IGPTechnicalDescription;
import com.sap.caf.eu.gp.structure.api.IGPAttributeInfo;
import com.sap.caf.eu.gp.structure.api.IGPStructure;
import com.sap.caf.eu.gp.structure.api.IGPStructureInfo;
import com.sap.dictionary.runtime.IDataType;
import com.sap.tc.webdynpro.progmodel.api.IWDAttributeInfo;
import com.sap.tc.webdynpro.progmodel.api.IWDMessageManager;
import com.sap.tc.webdynpro.progmodel.api.IWDNode;
import com.sap.tc.webdynpro.progmodel.api.IWDNodeElement;
import com.sap.tc.webdynpro.progmodel.api.IWDNodeInfo;
import com.sap.tc.webdynpro.progmodel.api.IWDTextAccessor;
//@@end

//@@begin documentation
//@@end

public class ChangeRequestSAPInterface
{
  /**
   * Logging location.
   */
  private static final com.sap.tc.logging.Location logger = 
    com.sap.tc.logging.Location.getLocation(ChangeRequestSAPInterface.class);

  static 
  {
    //@@begin id
		String id = "$Id$";
    //@@end
    com.sap.tc.logging.Location.getLocation("ID.com.sap.tc.webdynpro").infoT(id);
  }

  /**
   * Private access to the generated Web Dynpro counterpart 
   * for this controller class.  </p>
   *
   * Use <code>wdThis</code> to gain typed access to the context,
   * to trigger navigation via outbound plugs, to get and enable/disable
   * actions, fire declared events, and access used controllers and/or 
   * component usages.
   *
   * @see it.alcantara.crsap.wdp.IPrivateChangeRequestSAPInterface for more details
   */
  private final IPrivateChangeRequestSAPInterface wdThis;

  /**
   * Root node of this controller's context. </p>
   *
   * Provides typed access not only to the elements of the root node 
   * but also to all nodes in the context (methods node<i>XYZ</i>()) 
   * and their currently selected element (methods current<i>XYZ</i>Element()). 
   * It also facilitates the creation of new elements for all nodes 
   * (methods create<i>XYZ</i>Element()). </p>
   *
   * @see it.alcantara.crsap.wdp.IPrivateChangeRequestSAPInterface.IContextNode for more details.
   */
  private final IPrivateChangeRequestSAPInterface.IContextNode wdContext;

  /**
   * A shortcut for <code>wdThis.wdGetAPI()</code>. </p>
   * 
   * Represents the generic API of the generic Web Dynpro counterpart 
   * for this controller. </p>
   */
  private final com.sap.tc.webdynpro.progmodel.api.IWDController wdControllerAPI;
  
  /**
   * A shortcut for <code>wdThis.wdGetAPI().getComponent()</code>. </p>
   * 
   * Represents the generic API of the Web Dynpro component this controller 
   * belongs to. Can be used to access the message manager, the window manager,
   * to add/remove event handlers and so on. </p>
   */
  private final com.sap.tc.webdynpro.progmodel.api.IWDComponent wdComponentAPI;
  
  public ChangeRequestSAPInterface(IPrivateChangeRequestSAPInterface wdThis)
  {
    this.wdThis = wdThis;
    this.wdContext = wdThis.wdGetContext();
    this.wdControllerAPI = wdThis.wdGetAPI();
    this.wdComponentAPI = wdThis.wdGetAPI().getComponent();
  }

  //@@begin javadoc:wdDoInit()
	/** Hook method called to initialize controller. */
  //@@end
  public void wdDoInit()
  {
    //@@begin wdDoInit()
	
    //@@end
  }

  //@@begin javadoc:wdDoExit()
	/** Hook method called to clean up controller. */
  //@@end
  public void wdDoExit()
  {
    //@@begin wdDoExit()
    //@@end
  }

  //@@begin javadoc:getDescription()
	/** Declared method. */
  //@@end
  public com.sap.caf.eu.gp.co.api.IGPTechnicalDescription getDescription( java.util.Locale locale )
  {
    //@@begin getDescription()

		IWDMessageManager manager = wdComponentAPI.getMessageManager();

		IGPTechnicalDescription technicalDescription;
		IWDTextAccessor textAccessor = wdComponentAPI.getTextAccessor();
		GPWebDynproResourceAccessor resourceAccessor =
			new GPWebDynproResourceAccessor(textAccessor);

		technicalDescription =
			GPCallableObjectFactory.createTechnicalDescription(
				"CO_CHANGE_REQUEST_SAP",
				"Form Change Request SAP",
				resourceAccessor,
				locale);

		// Define Input and Output Structure
		IGPStructureInfo input = technicalDescription.getInputStructureInfo();
		IGPStructureInfo output = technicalDescription.getOutputStructureInfo();

		try 
		{
			/**************************************
			** Define Input Parameters
			**************************************/
			
			// Insert Attributre crsapID
			// for CRSAP R3 Archive.
			IGPAttributeInfo crsapIDAttributeInfo = 
				input.addAttribute("crsapIDInput", IGPAttributeInfo.BASE_BASE64BINARY);
			
			crsapIDAttributeInfo.setMultiplicity(IGPAttributeInfo.MULITIPLICITY_0_1);

			// Context Attributes
			IGPStructureInfo structureInputInfo =
				input.addStructure(GP_INPUT_STRUCTURE);

			structureInputInfo.setMultiplicity(
				IGPAttributeInfo.MULITIPLICITY_1_1);
			
			IWDNodeInfo crAttributesNodeInfo =
				wdContext.nodeCRAttributes().getNodeInfo();

			defineIGPAttributes(structureInputInfo, crAttributesNodeInfo);
			
			// Input Action Permormed
			IGPStructureInfo actionInputInfo =
				input.addStructure(GP_INPUT_ACTION_PERFORMED);
			
			actionInputInfo.setMultiplicity(
				IGPAttributeInfo.MULITIPLICITY_0_N);
			
			IWDNodeInfo actionNodeInfo =
				wdContext.nodeActionPerformed().getNodeInfo();
				
			defineIGPAttributes(actionInputInfo, actionNodeInfo);

			// Input Reader Comments
			IGPStructureInfo readerCommentsInputInfo =
				input.addStructure(GP_INPUT_READER_COMMENTS);

			readerCommentsInputInfo.setMultiplicity(
				IGPAttributeInfo.MULITIPLICITY_0_N);

			IWDNodeInfo readerCommentsNodeInfo =
				wdContext.nodeReaderComments().getNodeInfo();
	
			defineIGPAttributes(readerCommentsInputInfo, readerCommentsNodeInfo);

			// List of Reader Users
			IGPStructureInfo readerUsersInputInfo =
				input.addStructure(GP_INPUT_READER_LIST);

			readerUsersInputInfo.setMultiplicity(
				IGPAttributeInfo.MULITIPLICITY_1_1);

			defineUserList(readerUsersInputInfo, READER_LIST_ATTRIBUTE_NAME);

			// List of Approval Users
			IGPStructureInfo approvalUsersInputInfo =
				input.addStructure(GP_INPUT_APPROVAL_LIST);

			approvalUsersInputInfo.setMultiplicity(
				IGPAttributeInfo.MULITIPLICITY_1_1);

			defineUserList(approvalUsersInputInfo, APPROVAL_LIST_ATTRIBUTE_NAME);
			
			// List of IT Users
			IGPStructureInfo itUsersInputInfo =
				input.addStructure(GP_INPUT_IT_LIST);

			itUsersInputInfo.setMultiplicity(
				IGPAttributeInfo.MULITIPLICITY_1_1);

			defineUserList(itUsersInputInfo, IT_LIST_ATTRIBUTE_NAME);
			
			// List of Accettatore Users
			IGPStructureInfo accettatoreUsersInputInfo =
				input.addStructure(GP_INPUT_ACCETTATORE_LIST);

			accettatoreUsersInputInfo.setMultiplicity(
				IGPAttributeInfo.MULITIPLICITY_1_1);

			defineUserList(accettatoreUsersInputInfo, ACCETTATORE_LIST_ATTRIBUTE_NAME);
			
			/**************************************
			** Define Output Parameters
			**************************************/
			IGPStructureInfo structureOutputInfo =
				output.addStructure(GP_OUTPUT_STRUCTURE);

			structureOutputInfo.setMultiplicity(
				IGPAttributeInfo.MULITIPLICITY_1_1);

			IWDNodeInfo crOutputAttributesNodeInfo =
				wdContext.nodeCRAttributes().getNodeInfo();

			defineIGPAttributes(structureOutputInfo, crOutputAttributesNodeInfo);
			
			// Output Action Permormed
			IGPStructureInfo actionOutputInfo =
				output.addStructure(GP_OUTPUT_ACTION_PERFORMED);
			
			actionOutputInfo.setMultiplicity(
				IGPAttributeInfo.MULITIPLICITY_0_N);
			
			IWDNodeInfo actionOutputNodeInfo =
				wdContext.nodeActionPerformed().getNodeInfo();
				
			defineIGPAttributes(actionOutputInfo, actionNodeInfo);

			// Output Reader Comments
			IGPStructureInfo readerCommentsOutputInfo =
				output.addStructure(GP_OUTPUT_READER_COMMENTS);

			readerCommentsOutputInfo.setMultiplicity(
				IGPAttributeInfo.MULITIPLICITY_0_N);

			IWDNodeInfo readerOutputCommentsNodeInfo =
				wdContext.nodeReaderComments().getNodeInfo();
	
			defineIGPAttributes(readerCommentsOutputInfo, 
				readerOutputCommentsNodeInfo);

			// List of Reader Users
			IGPStructureInfo readerUsersOutputInfo =
				output.addStructure(GP_OUTPUT_READER_LIST);

			readerUsersOutputInfo.setMultiplicity(
				IGPAttributeInfo.MULITIPLICITY_1_1);

			defineUserList(readerUsersOutputInfo, READER_LIST_ATTRIBUTE_NAME);
			
			// List of Approval Users
			IGPStructureInfo approvalUsersOutputInfo =
				output.addStructure(GP_OUTPUT_APPROVAL_LIST);

			approvalUsersOutputInfo.setMultiplicity(
				IGPAttributeInfo.MULITIPLICITY_1_1);

			defineUserList(approvalUsersOutputInfo, APPROVAL_LIST_ATTRIBUTE_NAME);

			// List of Accept Users
			IGPStructureInfo acceptUsersOutputInfo =
				output.addStructure(GP_OUTPUT_ACCETTATORE_LIST);

			acceptUsersOutputInfo .setMultiplicity(
				IGPAttributeInfo.MULITIPLICITY_1_1);

			defineUserList(acceptUsersOutputInfo, ACCETTATORE_LIST_ATTRIBUTE_NAME);
			
			/**************************************
			** Define Result Set Parameters
			**************************************/
			IGPCOResultStateInfo resultSet =
				technicalDescription.addResultState(RESULT_STATE_APPROVATO);
			resultSet.setDescriptionKey(RESULT_STATE_APPROVATO_DESC);

			resultSet =
				technicalDescription.addResultState(RESULT_STATE_DA_RIVEDERE);
			resultSet.setDescriptionKey(RESULT_STATE_DA_RIVEDERE_DESC);

			resultSet =
				technicalDescription.addResultState(RESULT_STATE_RIFIUTATO);
			resultSet.setDescriptionKey(RESULT_STATE_RIFIUTATO_DESC);

		} 
		catch (Exception e) 
		{
			manager.reportException(
				"Exception in getDescription Method = " + e,
				false);
			printStackTrace(e);

			String stackTraceStr = new String();

			StackTraceElement[] stackTraceElements = e.getStackTrace();

			for (int j = 0; j < stackTraceElements.length; j++) {
				stackTraceStr = stackTraceStr + stackTraceElements[j];
			}

			manager.reportException(
				"Exception adding attribute, exception = " + e,
				false);

			manager.reportException(
				"Exception adding attribute, stackTrace = " + stackTraceStr,
				false);
		}

		return technicalDescription;

    //@@end
  }

  //@@begin javadoc:execute()
	/***************************** 
	** Declared method. 
	** Update Interface Context
	** from Input GP Structure
	*****************************/
  //@@end
  public void execute( com.sap.caf.eu.gp.co.api.IGPExecutionContext executionContext )
  {
    //@@begin execute()

		IWDMessageManager manager = wdComponentAPI.getMessageManager();

		/*****************************
		** Save executionContext to
		** complete method.
		** Retrieve ProcessID and save it
		** in Current Context
		*****************************/
		this.executionContext = executionContext;
		String processID = executionContext.getProcessId();
		wdContext.currentExecutionContextElement().setProcessID(processID);

		/**********************************
		** Retrieve Input Parameters
		**********************************/
		IGPStructure input = executionContext.getInputStructure();
		
		IGPStructureInfo inputStructureInfo = input.getStructureInfo();
		printStructureInfo(inputStructureInfo);

		try 
		{
			// Retrieve crsapID Attribute 
			byte[] crsapID = input.getAttributeAsByteArray("crsapIDInput");
			wdContext.currentResultStateElement().setCrsapID(crsapID);
			//manager.reportSuccess("Interface Controller retrieve crsapID = [" + crsapID + "]");
			
			// Retrieve CR Attributes and Update Context
			IGPStructure igpStructure = input.getStructure(GP_INPUT_STRUCTURE);
			setContextAttributes(igpStructure, 
				wdContext.nodeCRAttributes(), 
				wdContext.currentCRAttributesElement());
			
			// Retrieve ActionPerformed and Update Context
			Collection igpActionStructures = input.getStructures(GP_INPUT_ACTION_PERFORMED);
			setContextFromCollection(igpActionStructures, wdContext.nodeActionPerformed());


			// Retrieve Reader Comments and Update Context
			Collection igpReaderCommentsStructures = input.getStructures(GP_INPUT_READER_COMMENTS);
			setContextFromCollection(igpReaderCommentsStructures, wdContext.nodeReaderComments());
			/*			
			// Retrieve Reader List and Update Context
			Collection igpReaderListStructures = input.getStructures(GP_INPUT_READER_LIST);
			setContextFromCollection(igpReaderListStructures, wdContext.nodeReaderList());
			
			// Retrieve Approval List and Update Context
			Collection igpApprovalListStructures = input.getStructures(GP_INPUT_APPROVAL_LIST);
			setContextFromCollection(igpApprovalListStructures, wdContext.nodeApprovalList());

			// Retrieve Accettazione List and Update Context
			Collection igpAccettazioneListStructures = input.getStructures(GP_INPUT_ACCETTATORE_LIST);
			setContextFromCollection(igpAccettazioneListStructures, wdContext.nodeAccettazioneList());
			
			// Retrieve IT List and Update Context
			Collection igpItListStructures = input.getStructures(GP_INPUT_IT_LIST);
			setContextFromCollection(igpItListStructures, wdContext.nodeITList());
	*/


			// Retrieve Reader List user and Update Context
			IGPStructure igpReaderListStructure =
				input.getStructure(GP_INPUT_READER_LIST);

			setContextUserList(igpReaderListStructure, READER_LIST_ATTRIBUTE_NAME, 
				READER_LIST_CONTEXT_NODE_NAME, READER_LIST_ATTRIBUTE_NAME);
		 	
			// Retrieve Approval List user and Update Context	
			IGPStructure igpApprovalListStructure =
				input.getStructure(GP_INPUT_APPROVAL_LIST);

			setContextUserList(igpApprovalListStructure, APPROVAL_LIST_ATTRIBUTE_NAME, 
				APPROVAL_LIST_CONTEXT_NODE_NAME, APPROVAL_LIST_ATTRIBUTE_NAME);

			// Retrieve Accettazione List user and Update Context
			IGPStructure igpAccettatoreListStructure =
				input.getStructure(GP_INPUT_ACCETTATORE_LIST);

			setContextUserList(igpAccettatoreListStructure, ACCETTATORE_LIST_ATTRIBUTE_NAME, 
				ACCETTATORE_CONTEXT_NODE_NAME, ACCETTATORE_LIST_ATTRIBUTE_NAME);

			// Retrieve IT List user and Update Context				
			IGPStructure igITListStructure =
				input.getStructure(GP_INPUT_IT_LIST);

			setContextUserList(igITListStructure, IT_LIST_ATTRIBUTE_NAME, 
				IT_LIST_CONTEXT_NODE_NAME, IT_LIST_ATTRIBUTE_NAME);

		} 
		catch (Exception exception) 
		{
			manager.reportException(
				"Error in execute Method = " + exception,
				false);
			printStackTrace(exception);
		}

    //@@end
  }

  //@@begin javadoc:complete()
	/** Declared method. */
  //@@end
  public void complete( )
  {
    //@@begin complete()

		IWDMessageManager manager = wdComponentAPI.getMessageManager();

		if (executionContext == null) {
			manager.reportException(
				"ChangeRequestSAPInterface executionContext null!!",
				true);
			return;
		}

		try 
		{
			IGPStructure outputStructure =
				executionContext.getOutputStructure();

			if (outputStructure == null) 
			{
				manager.reportException(
					"EmployeeValInterface outputStructure null!!",
					true);
				throw new Exception("EmployeeValInterface outputStructure null");
			}

			/*************************************
			** Set Output Structure
			*************************************/
			IGPStructure igpStructure =
				outputStructure.addStructure(GP_OUTPUT_STRUCTURE);

			setOutputGPStructure(igpStructure, 
								wdContext.nodeCRAttributes(), 
								wdContext.currentCRAttributesElement());
			
			// Set Output Action Performed Output
			setOutputFromCollection(wdContext.nodeActionPerformed(), 
									outputStructure, 
									GP_OUTPUT_ACTION_PERFORMED);
			
			// Set Output Reader Comments
			setOutputFromCollection(wdContext.nodeReaderComments(), 
									outputStructure, 
									GP_OUTPUT_READER_COMMENTS);

			
			// Set Output Reader List
			IGPStructure readUserStructure =
				outputStructure.addStructure(GP_OUTPUT_READER_LIST);

			setOutputGPUserList(readUserStructure,READER_LIST_ATTRIBUTE_NAME,
				READER_LIST_CONTEXT_NODE_NAME,READER_LIST_ATTRIBUTE_NAME);
			
			// Set Output Approvatore List
			IGPStructure approvatoreUserStructure = 
				outputStructure.addStructure(GP_OUTPUT_APPROVAL_LIST);
			
			setOutputGPUserList(approvatoreUserStructure,APPROVAL_LIST_ATTRIBUTE_NAME,
				APPROVAL_LIST_CONTEXT_NODE_NAME,APPROVAL_LIST_ATTRIBUTE_NAME);
	
			// Set Output Accettatore List
			IGPStructure accettatoreUserStructure = 
				outputStructure.addStructure(GP_OUTPUT_ACCETTATORE_LIST);
			
			setOutputGPUserList(accettatoreUserStructure,ACCETTATORE_LIST_ATTRIBUTE_NAME,
				ACCETTATORE_CONTEXT_NODE_NAME,ACCETTATORE_LIST_ATTRIBUTE_NAME);
					
			/*************************************
			** Set Result State
			*************************************/
			short status = wdContext.currentCRAttributesElement().getCRStatus();
			//manager.reportSuccess("Output Status = " + status);

			/*************************************
			** If Status == 7 => 
			** The current action is refused
			** => ResultState = RESULT_STATE_RIFIUTATO
			*************************************/
			if ( status == 7 )
			{
				executionContext.setResultState(RESULT_STATE_RIFIUTATO);
			}
			else
			{
				executionContext.setResultState(RESULT_STATE_APPROVATO);
			}
			
			executionContext.processingComplete();
			//executionContext.setResultState(RESULT_STATE_APPROVATO);

		} 
		catch (Exception exception) 
		{
			String stackTrace = new String();
			StackTraceElement[] stackTraceElements = exception.getStackTrace();

			for (int j = 0; j < stackTraceElements.length; j++) 
			{
				stackTrace = stackTrace + stackTraceElements[j].toString();
			}

			manager.reportException(
				"Exception in complete Method = [" + exception + "]",
				false);

		}

    //@@end
  }

  /*
   * The following code section can be used for any Java code that is 
   * not to be visible to other controllers/views or that contains constructs
   * currently not supported directly by Web Dynpro (such as inner classes or
   * member variables etc.). </p>
   *
   * Note: The content of this section is in no way managed/controlled
   * by the Web Dynpro Designtime or the Web Dynpro Runtime. 
   */
  //@@begin others

	/*******************************
	** Private Fields Definition
	*******************************/
	
	// Input, Output Structure Name for Context Attributes
	private static final String GP_INPUT_STRUCTURE = "InputCRAttributes";
	private static final String GP_OUTPUT_STRUCTURE = "OutputCRAttributes";

	// Context for Action history
	private static final String GP_INPUT_ACTION_PERFORMED = "InputActionPerformed";
	private static final String GP_OUTPUT_ACTION_PERFORMED= "OutputActionPerformed";
	
	// Context for Reader Comments
	private static final String GP_INPUT_READER_COMMENTS = "InputReaderComments";
	private static final String GP_OUTPUT_READER_COMMENTS = "OutputReaderComments";

	//	Input, Output Structure Name for Reader List Attributes
	private static final String GP_INPUT_READER_LIST = "InputReaderList";
	private static final String GP_OUTPUT_READER_LIST = "OuputReaderList";

	//	Input, Output Structure Name for Approval List Attributes
	private static final String GP_INPUT_APPROVAL_LIST = "InputApprovalList";
	private static final String GP_OUTPUT_APPROVAL_LIST = "OuputApprovalList";

	// Input Structure Name for UfficioIT List Attribute
	private static final String GP_INPUT_IT_LIST = "InputITList";
	
	//	Input Structure Name for Accettatore Attribute
	private static final String GP_INPUT_ACCETTATORE_LIST = "InputAccettatoreList";
	private static final String GP_OUTPUT_ACCETTATORE_LIST = "OutputAccettatoreList";

	// Attribute Name for Reader List Attribute
	private static final String READER_LIST_ATTRIBUTE_NAME = "ReaderID";
	private static final String APPROVAL_LIST_ATTRIBUTE_NAME = "ApprovalID";
	private static final String IT_LIST_ATTRIBUTE_NAME = "ITID";
	private static final String ACCETTATORE_LIST_ATTRIBUTE_NAME = "AccettatoreID";

	// Context Node Name
	private static final String READER_LIST_CONTEXT_NODE_NAME= "ReaderList";
	private static final String APPROVAL_LIST_CONTEXT_NODE_NAME = "ApprovalList";
	private static final String IT_LIST_CONTEXT_NODE_NAME = "ITList";
	private static final String ACCETTATORE_CONTEXT_NODE_NAME = "AccettazioneList";
	
	
	// Possible Result State and Result State Desc of CO.
	private static final String RESULT_STATE_DA_RIVEDERE = "DaRivedere";
	private static final String RESULT_STATE_DA_RIVEDERE_DESC = "Da Rivedere";
	private static final String RESULT_STATE_APPROVATO = "Approvato";
	private static final String RESULT_STATE_APPROVATO_DESC = "Approvato";
	private static final String RESULT_STATE_RIFIUTATO = "Rifiutato";
	private static final String RESULT_STATE_RIFIUTATO_DESC = "Rifiutato";

	private IGPExecutionContext executionContext = null;


	/**********************************
	** Set GP Output Parameters from
	** GPAttributes of Context Node
	**********************************/
	private void setOutputGPStructure(IGPStructure igpStructure, IWDNode node, IWDNodeElement nodeElement)
		throws Exception 
	{
		IWDMessageManager manager = wdComponentAPI.getMessageManager();

		IWDNodeInfo nodeInfo = node.getNodeInfo();
		Iterator iter = nodeInfo.iterateAttributes();

		while (iter.hasNext()) 
		{
			IWDAttributeInfo iwdAttributeInfo = (IWDAttributeInfo) iter.next();
			String attributeName = iwdAttributeInfo.getName();
			IDataType idataType = iwdAttributeInfo.getDataType();
			String dataTypeName = idataType.getName();

			//manager.reportSuccess("DataType = [" + dataTypeName + "]");
			//manager.reportSuccess("Attribute Name Context = [" + attributeName + "]");		

			Object object =
				nodeElement.getAttributeValue(attributeName);

			if (dataTypeName.equals("com.sap.dictionary.string")) 
			{
				igpStructure.setAttributeValue(attributeName, (String) object);
			} 
			else if (dataTypeName.equals("com.sap.dictionary.date")) 
			{
				igpStructure.setAttributeValue(
					attributeName,
					(java.sql.Date) object);
			} 
			else if (dataTypeName.equals("com.sap.dictionary.short")) 
			{
				//manager.reportSuccess("Set IGPOutputStructure Attribute = " + 
				//	attributeName + "With Value = " + (Short) object);
				igpStructure.setAttributeValue(attributeName, (Short) object);
			}			
			else if (dataTypeName.equals("com.sap.dictionary.float")) 
			{
				igpStructure.setAttributeValue(attributeName, (Float) object);				 
			} 
			else if (dataTypeName.equals("com.sap.dictionary.integer")) 
			{ 
				//manager.reportSuccess("Set IGPOutputStructure Attribute = " + 
				//					attributeName + "With Value = " + (Integer) object);
				
				igpStructure.setAttributeValue(attributeName, (Integer) object);
			} 
			else if (dataTypeName.equals("com.sap.dictionary.boolean")) 
			{ 
				//manager.reportSuccess("Set IGPOutputStructure Attribute = " + 
				//					attributeName + "With Value = " + (Integer) object);
				
				igpStructure.setAttributeValue(attributeName, (Boolean) object);
			} 
			else 
			{
				if ( !attributeName.equals("FDataImg"))
				{
					manager.reportException(
						"Exception Unknown Data Type....",
						false);
					throw new Exception("Data Type = " + object.getClass().toString() + 
								"is not managed by method SetOutputGPStructure");
				}
			}

		}

	}

	private void defineUserList(IGPStructureInfo usersInputInfo, String attributeName)
	{
		IWDMessageManager manager = wdComponentAPI.getMessageManager();

		try 
		{
			IGPAttributeInfo attributeInfo =
				usersInputInfo.addAttribute(
					attributeName,
					IGPAttributeInfo.BASE_STRING);

			attributeInfo.setMultiplicity(IGPAttributeInfo.MULITIPLICITY_1_N);
		} 
		catch (Exception e) 
		{

			manager.reportException(
				"Exception adding attribute, exception = " + e,
				false);

			printStackTrace(e);
		}
	}
	
	/*******************************************
	** Define Input Output Parameters
	** for CO.
	** The parameters are defined 
	** from CRAttributes of Interface Context 
	*******************************************/
	private void defineIGPAttributes(IGPStructureInfo igpStructureInfo, IWDNodeInfo nodeInfo) 
	{
		IWDMessageManager manager = wdComponentAPI.getMessageManager();
		//manager.reportSuccess("nodeInfo = [" + nodeInfo.getName() + "]");
		
		Iterator iter = nodeInfo.iterateAttributes();

		while (iter.hasNext()) 
		{
			IWDAttributeInfo iwdAttributeInfo = (IWDAttributeInfo) iter.next();
			String attributeName = iwdAttributeInfo.getName();
			IDataType idataType = iwdAttributeInfo.getDataType();
			String dataTypeName = idataType.getName();

			//manager.reportSuccess("DataType = [" + dataTypeName + "] " +
			//	"and AttributeName = [" + attributeName + "]");

			try 
			{
				IGPAttributeInfo attributeInfo = null;

				if (dataTypeName.equals("com.sap.dictionary.string")) 
				{
					//manager.reportSuccess(" Add Attribute " + attributeName + " to IGPStucture " +
					//	igpStructureInfo.getTechName());
						
					attributeInfo = 
						igpStructureInfo.addAttribute(
							attributeName,
							IGPAttributeInfo.BASE_STRING);
				} 
				else if (dataTypeName.equals("com.sap.dictionary.date")) 
				{
					attributeInfo =
						igpStructureInfo.addAttribute(
							attributeName,
							IGPAttributeInfo.BASE_DATE);
				} else if (dataTypeName.equals("com.sap.dictionary.short")) {
					attributeInfo =
						igpStructureInfo.addAttribute(
							attributeName,
							IGPAttributeInfo.BASE_SIGNED_SHORT);
				} else if (dataTypeName.equals("com.sap.dictionary.float")) {
					attributeInfo =
						igpStructureInfo.addAttribute(
							attributeName,
							IGPAttributeInfo.BASE_FLOAT);
				} 
				else if (dataTypeName.equals("com.sap.dictionary.integer")) 
				{
					attributeInfo =
						igpStructureInfo.addAttribute(
							attributeName,
							IGPAttributeInfo.BASE_SIGNED_INT);
				}
				else if (dataTypeName.equals("com.sap.dictionary.boolean")) 
				{
					attributeInfo =
						igpStructureInfo.addAttribute(
							attributeName,
							IGPAttributeInfo.BASE_BOOLEAN);
				}
				else if (dataTypeName.equals("com.sap.dictionary.binary")) 
				{
					attributeInfo =
						igpStructureInfo.addAttribute(
							attributeName,
							IGPAttributeInfo.BASE_BASE64BINARY);
				} 
				else 
				{
					if ( !attributeName.equals("FDataImg"))
					{
						manager.reportException(
							"Exception Unknown Data Type....",
							false);
						throw new Exception();
					}
				}

				attributeInfo.setMultiplicity(
					IGPAttributeInfo.MULITIPLICITY_0_1);
			} 
			catch (Exception e) 
			{
				String stackTraceStr = new String();

				StackTraceElement[] stackTraceElements = e.getStackTrace();

				for (int j = 0; j < stackTraceElements.length; j++) 
				{
					stackTraceStr = stackTraceStr + stackTraceElements[j];
				}

				manager.reportException(
					"Exception adding attribute, exception = " + e,
					false);

				manager.reportException(
					"Exception adding attribute, stackTrace = " + stackTraceStr,
					false);
			}
		}

	}

	/*******************************************
	** Every Attribute of CRAttributes Node
	** must be set from Input Structure
	********************************************/
	private void setContextAttributes(IGPStructure igpStructure, IWDNode node, IWDNodeElement nodeElement)
	{
		IWDMessageManager manager = wdComponentAPI.getMessageManager();

		IWDNodeInfo  nodeInfo = node.getNodeInfo();
		Iterator iter = nodeInfo.iterateAttributes();

		while (iter.hasNext()) {
			IWDAttributeInfo iwdAttributeInfo = (IWDAttributeInfo) iter.next();
			String attributeName = iwdAttributeInfo.getName();
			IDataType idataType = iwdAttributeInfo.getDataType();
			String dataTypeName = idataType.getName();
			String attributeValueString = null;
			Date attributeValueDate = null;
			short attributeValueShort;
			
			//manager.reportSuccess("DataType = [" + dataTypeName + "]");
			//manager.reportSuccess("Attribute Name Context = [" + attributeName + "]");		

			try 
			{
				IGPAttributeInfo attributeInfo = null;

				// Check Data Type to create Attribute.
				if (dataTypeName.equals("com.sap.dictionary.string")) {
					attributeValueString =
						igpStructure.getAttributeAsString(attributeName);
					//manager.reportSuccess("attributeValueString = [" + attributeValueString + "]");
					nodeElement.setAttributeValue(
						attributeName,
						attributeValueString);
				} 
				else if (dataTypeName.equals("com.sap.dictionary.date")) 
				{
					java.sql.Date sql_date = null;
					attributeValueDate =
						igpStructure.getAttributeAsDate(attributeName);
						
					if ( attributeValueDate != null )
					{
						sql_date =
							new java.sql.Date(attributeValueDate.getTime());
					}
					//manager.reportSuccess("attributeValueDate = [" + attributeValueDate + "]");
					nodeElement.setAttributeValue(
						attributeName,
						sql_date);
				} 
				else if (dataTypeName.equals("com.sap.dictionary.short")) {
					attributeValueShort =
						igpStructure.getAttributeAsShort(attributeName);
					Short short_value = new Short(attributeValueShort);
					//manager.reportSuccess("attributeValueString = [" + short_value + "]");
					nodeElement.setAttributeValue(
						attributeName,
						short_value);
				}
				else if (dataTypeName.equals("com.sap.dictionary.float")) 
				{
					//attributeValueFloat; 
					Float float_value;
					double value = 
						igpStructure.getAttributeAsDouble(attributeName);
					float_value = new Float(value);	
					//manager.reportSuccess("attributeValue = [" + float_value + "]");
					nodeElement.setAttributeValue(
						attributeName,
						float_value);
				} 
				else if (dataTypeName.equals("com.sap.dictionary.integer")) 
				{ 
					Integer int_value;
					int value = 
						igpStructure.getAttributeAsInt(attributeName);
					int_value = new Integer(value);	
					//manager.reportSuccess("attributeValue = [" + int_value + "]");
					nodeElement.setAttributeValue(
						attributeName,
						int_value);
				}
				else if (dataTypeName.equals("com.sap.dictionary.boolean")) 
				{ 
					Boolean boolean_value;
					boolean value = 
						igpStructure.getAttributeAsBoolean(attributeName);
					boolean_value = new Boolean(value);	
					nodeElement.setAttributeValue(
						attributeName,
						boolean_value);
				}
				else if (dataTypeName.equals("com.sap.dictionary.binary")) 
				{ 
					byte[] value = 
						igpStructure.getAttributeAsByteArray(attributeName);
					//boolean_value = new Boolean(value);	
					nodeElement.setAttributeValue(
						attributeName,
						value);
				} 
				else 
				{
					if ( !attributeName.equals("FDataImg"))
					{
						manager.reportException(
							"Exception Unknown Data Type....",
							false);
						throw new Exception();
					}
				}

			} 
			catch (Exception e) 
			{
				manager.reportException("Exception = " + e, false);
				printStackTrace(e);
			}
		}

	}
	
	private void setOutputGPUserList(IGPStructure igpStrucure, String igpAttributeName, 
		String nodeName, String attributeName)
	{
		IWDMessageManager manager = wdComponentAPI.getMessageManager();

		//manager.reportSuccess(" Method setOutputGPUserList igpAttributeName = " +
		//	igpAttributeName + " nodeName = " + nodeName + "attributeName = " +  attributeName);
		
		IWDNode node = 
				wdContext.wdGetAPI().getRootNode().
					getChildNode(nodeName,IWDNode.LEAD_SELECTION);
				
		try
		{
			int size = node.size();
			ArrayList arrayList = new ArrayList();
			
			for ( int k = 0; k < size; k ++ )
			{
				IWDNodeElement element =  node.getElementAt(k);
				String attributeValue = (String)element.getAttributeValue(attributeName);
				//manager.reportSuccess("Add AttributeValue = [" + attributeValue + "] to output user structure" );
				arrayList.add(attributeValue);				
			}
			
			igpStrucure.setAttributeValues(igpAttributeName, arrayList);
		}
		catch(Exception e)
		{
			manager.reportException("Exception in setOutputGPUserList Method " +
				"nodeName = " + nodeName + "AttributeName = " + attributeName + 
				" Exception = " + e, false);
			printStackTrace(e);
			
		}

	}

	/*******************************************
	** Set Generic context User Node from
	** igpStructure: GP Input Parameter Structure
	** attributeName: Nome of GP Input Parameter Structure
	** nodeName: Name of Context Node
	** nodeAttributeName: Name of Node Context Attribute to create
	*********************************************/
	private void setContextUserList(IGPStructure igpStructure, String attributeName, 
										String nodeName, String nodeAttributeName)
		throws Exception
	{
		IWDMessageManager manager = wdComponentAPI.getMessageManager();
		
		IWDNode node = 
			wdContext.wdGetAPI().getRootNode().
				getChildNode(nodeName,IWDNode.LEAD_SELECTION);
				
		Collection collection =
					igpStructure.getAttributeValues(
					attributeName);
		
		if (collection != null)
		{
			Iterator iter = collection.iterator();
			String attributeValue = null;
		
			while (iter.hasNext()) 
			{
				attributeValue = (String) iter.next();
			
				IWDNodeElement element = node.createElement();
				element.setAttributeValue(nodeAttributeName, attributeValue);
				node.addElement(element);			
			}
		}		
		
	}

	/*******************************************
	** Set Context Attributes Revisore ID
	** from Input Parameter
	*********************************************/
	private void setContextReaderList(IGPStructure igpReaderListStructure)
		throws Exception 
	{
		IReaderListNode revisoriListNode = wdContext.nodeReaderList();

		Collection collection =
			igpReaderListStructure.getAttributeValues(
				READER_LIST_ATTRIBUTE_NAME);

		Iterator iter = collection.iterator();
		String readerID = null;

		while (iter.hasNext()) {
			readerID = (String) iter.next();

			IReaderListElement revisoriListElement =
				revisoriListNode.createReaderListElement();

			revisoriListElement.setReaderID(readerID);
			revisoriListNode.addElement(revisoriListElement);
		}

	}

	private void setOutputFromCollection(IWDNode node, 
										IGPStructure outputStructure,
										String outputStructName) throws Exception
	{
		IWDMessageManager manager = wdComponentAPI.getMessageManager();
		int size = node.size();
		
		//manager.reportSuccess("setOutputFromCollection Context node = [" + 
		//	node.getNodeInfo().getName() + "] with size = [" + size + "]");
		
		for ( int k = 0; k < size; k++)
		{
			IGPStructure igpStructure =
				outputStructure.addStructure(outputStructName);
			
			IWDNodeElement nodeElement = node.getElementAt(k);
			setOutputGPStructure(igpStructure, node, nodeElement);
		}
	}

	private void setContextFromCollection(Collection igpStructures, IWDNode node)
	{
		IWDMessageManager manager = wdComponentAPI.getMessageManager();
		Iterator iterator = igpStructures.iterator();
		IWDNodeInfo nodeInfo = node.getNodeInfo();
		//manager.reportSuccess("setContextFromCollection IGPStructure Size = [" 
		//	+ igpStructures.size() + "]");
		
		while( iterator.hasNext() )
		{
			IWDNodeElement nodeElement =  node.createElement();
			IGPStructure igpStructure = (IGPStructure)iterator.next();
			//manager.reportSuccess("setContextFromCollection IGPStrucure Name = [" 
			//	+ igpStructure.getStructureInfo().getTechName() + "]");
			setContextAttributes(igpStructure, node, nodeElement);
			node.addElement(nodeElement);
		}
	}
	
	private void printStructureInfo(IGPStructureInfo inputStructureInfo)
	{
		IWDMessageManager manager = wdComponentAPI.getMessageManager();

		Collection collection = inputStructureInfo.getStructures();
		Iterator iter = collection.iterator();
		
		while ( iter.hasNext() )
		{
			IGPStructureInfo structureInfo = (IGPStructureInfo)iter.next();
			String technicalName = structureInfo.getTechName();
			String label = structureInfo.getLabel().toString();
			
			//manager.reportSuccess("technicalName = [" + technicalName + 
			//	"] and label = [" + label + "]");
		}
		
	}

	private void printStackTrace(Exception exception) {
		IWDMessageManager manager = wdComponentAPI.getMessageManager();
		String stackTraceStr = new String();

		StackTraceElement[] stackTraceElements = exception.getStackTrace();

		for (int j = 0; j < stackTraceElements.length; j++) {
			stackTraceStr = stackTraceStr + stackTraceElements[j];
		}

		manager.reportException(
			"Exception adding attribute, exception = " + exception,
			false);

		manager.reportException(
			"Exception adding attribute, stackTrace = " + stackTraceStr,
			false);

	}

  //@@end
}
