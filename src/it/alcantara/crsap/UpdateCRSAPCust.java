// ---------------------------------------------------------------------------
// This file has been generated partially by the Web Dynpro Code Generator.
// MODIFY CODE ONLY IN SECTIONS ENCLOSED BY @@begin AND @@end.
// ALL OTHER CHANGES WILL BE LOST IF THE FILE IS REGENERATED.
// ---------------------------------------------------------------------------
package it.alcantara.crsap;

// 
// IMPORTANT NOTE: 
// _ALL_ IMPORT STATEMENTS MUST BE PLACED IN THE FOLLOWING SECTION ENCLOSED
// BY @@begin imports AND @@end. FURTHERMORE, THIS SECTION MUST ALWAYS CONTAIN
// AT LEAST ONE IMPORT STATEMENT (E.G. THAT FOR IPrivateUpdateCRSAPCust).
// OTHERWISE, USING THE ECLIPSE FUNCTION "Organize Imports" FOLLOWED BY
// A WEB DYNPRO CODE GENERATION (E.G. PROJECT BUILD) WILL RESULT IN THE LOSS
// OF IMPORT STATEMENTS.
//
//@@begin imports
import it.alcantara.crsap.wdp.IPrivateUpdateCRSAPCust;
import it.alcantara.crsap.wdp.IPublicUpdateCRSAPCust.IActionPerformedElement;
import it.alcantara.crsap.wdp.IPublicUpdateCRSAPCust.IActionPerformedNode;
import it.alcantara.crsap.wdp.IPublicUpdateCRSAPCust.ICRAttributesElement;
import it.alcantara.crsap.wdp.IPublicUpdateCRSAPCust.IReaderCommentsElement;
import it.alcantara.crsap.wdp.IPublicUpdateCRSAPCust.IReaderCommentsNode;
import it.alcantara.crsap.wdp.IPublicUpdateCRSAPCust.IZcrsap_Update_InputElement;
import it.alcantara.model.update.Zcrsap_History;
import it.alcantara.model.update.Zcrsap_Update_Input;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

import com.sap.tc.webdynpro.modelimpl.dynamicrfc.WDDynamicRFCExecuteException;
import com.sap.tc.webdynpro.progmodel.api.IWDMessageManager;
import com.sap.tc.webdynpro.services.sal.um.api.WDUMException;
//@@end

//@@begin documentation
//@@end

public class UpdateCRSAPCust
{
  /**
   * Logging location.
   */
  private static final com.sap.tc.logging.Location logger = 
    com.sap.tc.logging.Location.getLocation(UpdateCRSAPCust.class);

  static 
  {
    //@@begin id
    String id = "$Id$";
    //@@end
    com.sap.tc.logging.Location.getLocation("ID.com.sap.tc.webdynpro").infoT(id);
  }

  /**
   * Private access to the generated Web Dynpro counterpart 
   * for this controller class.  </p>
   *
   * Use <code>wdThis</code> to gain typed access to the context,
   * to trigger navigation via outbound plugs, to get and enable/disable
   * actions, fire declared events, and access used controllers and/or 
   * component usages.
   *
   * @see it.alcantara.crsap.wdp.IPrivateUpdateCRSAPCust for more details
   */
  private final IPrivateUpdateCRSAPCust wdThis;

  /**
   * Root node of this controller's context. </p>
   *
   * Provides typed access not only to the elements of the root node 
   * but also to all nodes in the context (methods node<i>XYZ</i>()) 
   * and their currently selected element (methods current<i>XYZ</i>Element()). 
   * It also facilitates the creation of new elements for all nodes 
   * (methods create<i>XYZ</i>Element()). </p>
   *
   * @see it.alcantara.crsap.wdp.IPrivateUpdateCRSAPCust.IContextNode for more details.
   */
  private final IPrivateUpdateCRSAPCust.IContextNode wdContext;

  /**
   * A shortcut for <code>wdThis.wdGetAPI()</code>. </p>
   * 
   * Represents the generic API of the generic Web Dynpro counterpart 
   * for this controller. </p>
   */
  private final com.sap.tc.webdynpro.progmodel.api.IWDController wdControllerAPI;
  
  /**
   * A shortcut for <code>wdThis.wdGetAPI().getComponent()</code>. </p>
   * 
   * Represents the generic API of the Web Dynpro component this controller 
   * belongs to. Can be used to access the message manager, the window manager,
   * to add/remove event handlers and so on. </p>
   */
  private final com.sap.tc.webdynpro.progmodel.api.IWDComponent wdComponentAPI;
  
  public UpdateCRSAPCust(IPrivateUpdateCRSAPCust wdThis)
  {
    this.wdThis = wdThis;
    this.wdContext = wdThis.wdGetContext();
    this.wdControllerAPI = wdThis.wdGetAPI();
    this.wdComponentAPI = wdThis.wdGetAPI().getComponent();
  }

  //@@begin javadoc:wdDoInit()
  /** Hook method called to initialize controller. */
  //@@end
  public void wdDoInit()
  {
    //@@begin wdDoInit()
    //$$begin Service Controller(-596333341)
    wdContext.nodeZcrsap_Update_Input().bind(new Zcrsap_Update_Input());
    //$$end
    //@@end
  }

  //@@begin javadoc:wdDoExit()
  /** Hook method called to clean up controller. */
  //@@end
  public void wdDoExit()
  {
    //@@begin wdDoExit()
    //@@end
  }

  //@@begin javadoc:executeZcrsap_Update_Input()
  /** Declared method. */
  //@@end
  public short executeZcrsap_Update_Input( )
  {
    //@@begin executeZcrsap_Update_Input()
    //$$begin Service Controller(1361912597)
    IWDMessageManager manager = wdComponentAPI.getMessageManager();
	//manager.reportSuccess("Call executeZcrsap_Update_Input");
	short success = 0;
	short error = 1;
	
	wdContext.nodeZcrsap_Update_Input().bind((Collection)null);
	wdContext.nodeZcrsap_Update_Input().bind(new Zcrsap_Update_Input());
	
	
    try
    {
		// Retrieve Attribute to Update
		retrieveAttribute();
		retrieveHistoryAction();
      	wdContext.currentZcrsap_Update_InputElement().modelObject().execute();
      	wdContext.nodeOutput().invalidate();
      	
      	if ( wdContext.currentOutputElement() != null )
      	{
      		//manager.reportSuccess("Retrun Code = [" + wdContext.currentOutputElement().getReturn_Code() + "]");
      	}
      	else
      	{
			//manager.reportSuccess("Current Output Element is null");
      	}
      	
    }
    catch(WDDynamicRFCExecuteException e)
    {
      manager.reportException(" WDDynamicRFCExecuteException = " + e.getMessage(), false);
      return error;
    }
	catch(NullPointerException e)
	{
	  manager.reportException("NullPointerException = " + e.getMessage(), false);
	  return error;
	}
	catch(ParseException e)
	{
	  manager.reportException("ParseException = " + e.getMessage(), false);
	  return error;
	}
	catch(WDUMException e)
	{
	  manager.reportException("WDUMException = " + e.getMessage(), false);
	  return error;
	}
	
	return success;
    //$$end
    //@@end
  }

  /*
   * The following code section can be used for any Java code that is 
   * not to be visible to other controllers/views or that contains constructs
   * currently not supported directly by Web Dynpro (such as inner classes or
   * member variables etc.). </p>
   *
   * Note: The content of this section is in no way managed/controlled
   * by the Web Dynpro Designtime or the Web Dynpro Runtime. 
   */
  //@@begin others
  
	//Possible Result State and Result State Desc of CO.
 	private static final String RESULT_STATE_DA_RIVEDERE = "DaRivedere";
 	private static final String RESULT_STATE_DA_RIVEDERE_DESC = "Da Rivedere";
 	private static final String RESULT_STATE_APPROVATO = "Approvato";
 	private static final String RESULT_STATE_APPROVATO_DESC = "Approvato";
 	private static final String RESULT_STATE_RIFIUTATO = "Rifiutato";
 	private static final String RESULT_STATE_RIFIUTATO_DESC = "Rifiutato";
  
  
	private void retrieveAttribute()
  		throws NullPointerException, WDUMException
  	{	
		IWDMessageManager manager = wdComponentAPI.getMessageManager();
		String currentLogonID = 
			wdThis.wdGetChangeRequestSAPController().getCurrentUserLogonID();
	  	
	  	ICRAttributesElement attributesElement = 
	  		wdContext.currentCRAttributesElement();
	  		
		short newStatus = attributesElement.getCRStatus();
		String analisiCosti = attributesElement.getAnalisiCosti();
		String soluzioneImplementata = attributesElement.getSoluzioneImplementata();
		byte[] crsapID = wdContext.currentResultStateElement().getCrsapID();
		String resultStateValue = wdContext.currentResultStateElement().getResultStateValue();
		String currentUserComment = retrieveCurrentUserComment(currentLogonID);
		
		//manager.reportSuccess("CRSapUpdateController retrieve newStatus = [" + newStatus + "]" );
		//manager.reportSuccess("CRSapUpdateController retrieve analisiCosti = [" + analisiCosti + "]" );
		//manager.reportSuccess("CRSapUpdateController retrieve crsapID = [" + crsapID + "]" );
		
		IZcrsap_Update_InputElement inputElement = 
			wdContext.currentZcrsap_Update_InputElement();
			
		inputElement.setAnalisi_Costi(analisiCosti);
		inputElement.setNew_Status(newStatus);
		inputElement.setCrsap_Id(crsapID);
		inputElement.setComment(currentUserComment);
		inputElement.setUser_Id(currentLogonID);
		inputElement.setSoluzione_Imp(soluzioneImplementata);
		
		if (resultStateValue != null && resultStateValue.equals(RESULT_STATE_APPROVATO))
			inputElement.setAction("A");
		else
			inputElement.setAction("R");

		
	}
  
  /*****************************
  ** Retrieve Action Values
  ** From Context and fill
  ** History_Action Update RFC
  ** Input Table. 
  *****************************/
  private void retrieveHistoryAction()
  	throws NullPointerException, ParseException
  {
	IWDMessageManager manager = wdComponentAPI.getMessageManager();
	
  	IActionPerformedNode actionPerformedNode = 
  		wdContext.nodeActionPerformed();
  	
	Zcrsap_Update_Input updateInputElement = 
		wdContext.currentZcrsap_Update_InputElement().modelObject();	
		
  	for ( int k = 0; k < actionPerformedNode.size(); k++ )
  	{
		IActionPerformedElement actionPerformedElement = 
			actionPerformedNode.getActionPerformedElementAt(k);
		
		String action = actionPerformedElement.getAction();
		
		DateFormat df = new SimpleDateFormat("EEE MMM dd HH:mm:ss ZZZZ yyyy");
		String dateString = actionPerformedElement.getActionDate();
		Date date = df.parse(dateString);
		String owner = actionPerformedElement.getActionOwner();
		
		Zcrsap_History crsapHistory = new Zcrsap_History();
		crsapHistory.setAction_Desc(action);
		crsapHistory.setUser_Id(owner);
		crsapHistory.setData( new java.sql.Date(date.getTime()));
		
		updateInputElement.addHistory_Action(crsapHistory);	
		
  	}
  		
  }
  
  private String retrieveCurrentUserComment(String currentUserLogonId)
  {
  	IReaderCommentsNode commentsNode = wdContext.nodeReaderComments();
  	
  	for ( int k = 0; k < commentsNode.size(); k++)
  	{
  		IReaderCommentsElement commentsElement = 
  			commentsNode.getReaderCommentsElementAt(k);
  		
  		if (commentsElement.getUser().equals(currentUserLogonId))
  		{
  			return commentsElement.getComment();
  		}
  	}
  	
  	return null;
  }
  
  //@@end
}
