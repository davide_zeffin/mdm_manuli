// ---------------------------------------------------------------------------
// This file has been generated partially by the Web Dynpro Code Generator.
// MODIFY CODE ONLY IN SECTIONS ENCLOSED BY @@begin AND @@end.
// ALL OTHER CHANGES WILL BE LOST IF THE FILE IS REGENERATED.
// ---------------------------------------------------------------------------
package it.alcantara.crsap.model;

//@@begin imports
import java.lang.String;
//@@end

//@@begin documentation
/**
 * Title:        Web Dynpro Version 6.30
 * Description:  "CRSAPArchive" RFC Adapter Model implementation
 * Copyright:    Copyright (c) 2001 - 2003. All rights reserved.
 * Company:      SAP AG
 * @author       File created by Web Dynpro code generator
 */
//@@end

public class CRSAPArchive extends com.sap.tc.webdynpro.modelimpl.dynamicrfc.DynamicRFCModel
{

  /**
   * Log version during <clinit>
   */
  static {
    //@@begin id
    String id = "$Id$";
    //@@end
    com.sap.tc.logging.Location.getLocation("ID.com.sap.tc.webdynpro").infoT(id);
  }

  private static final Object[][] WD_STATIC_TYPE_INFO = {
    { "it.alcantara.crsap.model.Zcrsap_Create_Output", "ZCRSAP_CREATE",
      "output", new String[][] {
        { "Req_Id", "it.alcantara.crsap.model.types.Guid_16" },
      } },
    { "it.alcantara.crsap.model.Zcrsap_History", "ZCRSAP_HISTORY",
      "structure", 
      "it.alcantara.crsap.model.types.Zcrsap_History" },
    { "it.alcantara.crsap.model.Zcrsap_Attachment", "ZCRSAP_ATTACHMENT",
      "structure", 
      "it.alcantara.crsap.model.types.Zcrsap_Attachment" },
    { "it.alcantara.crsap.model.Zstr_User_Id", "ZSTR_USER_ID",
      "structure", 
      "it.alcantara.crsap.model.types.Zstr_User_Id" },
    { "it.alcantara.crsap.model.Zcrsap_Create_Input", "ZCRSAP_CREATE",
      "input", new String[][] {
      } },
    { "it.alcantara.crsap.model.Zcrsap_Attributes", "ZCRSAP_ATTRIBUTES",
      "structure", 
      "it.alcantara.crsap.model.types.Zcrsap_Attributes" },
  };
  
  private static final com.sap.aii.proxy.framework.core.BaseProxyDescriptor staticDescriptor = 
    com.sap.aii.proxy.framework.core.BaseProxyDescriptorFactory.createNewBaseProxyDescriptor(
      "urn:sap-com:document:sap:rfc:functions:CRSAPArchive.PortType",
      new java.lang.Object[][][] {
        {
          { "ZCRSAP_CREATE",
            "zcrsap_Create",
            "ZCRSAP_CREATE" },
          { "urn:sap-com:document:sap:rfc:functions:ZCRSAP_CREATE.Input",
            "it.alcantara.crsap.model.Zcrsap_Create_Input" },
          { "urn:sap-com:document:sap:rfc:functions:ZCRSAP_CREATE.Output",
            "it.alcantara.crsap.model.Zcrsap_Create_Output" },
          { "urn:sap-com:document:sap:rfc:functions:WDDynamicRFC_Fault.Exception",
            "com.sap.tc.webdynpro.modelimpl.dynamicrfc.WDDynamicRFC_Fault_Exception",
            "com.sap.tc.webdynpro.modelimpl.dynamicrfc.WDDynamicRFC_Fault" }
        },
      },
      com.sap.aii.proxy.framework.core.FactoryConstants.CONNECTION_TYPE_JCO,
      it.alcantara.crsap.model.CRSAPArchive.class);

  public static final com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType DEFAULT_MODELSCOPE =
        com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType.APPLICATION_SCOPE;
  public static final String DEFAULT_SYSTEMNAME = "ECC_ESS_MODELDATA";
  public static final String DEFAULT_RFCMETADATA_SYSTEMNAME = "ECC_ESS_METADATA";
  public static final String LOGICAL_DICTIONARY_NAME = "it.alcantara.crsap.model.types.CRSAPArchive";
  public static final String LOGICAL_DICTIONARY_DEFAULT_SYSTEM_NAME = "ECC_ESS_METADATA";
  private static final com.sap.aii.proxy.framework.core.GenerationInfo staticGenerationInfo = new com.sap.aii.proxy.framework.core.GenerationInfo("2.0", 1057227900576L) ;
  static{
    com.sap.tc.webdynpro.progmodel.model.api.WDModelFactory.registerDefaultScopeForModel(it.alcantara.crsap.model.CRSAPArchive.class, DEFAULT_MODELSCOPE);
  }

  /**
   * Class local cache for model metadata
   */
  private static final com.sap.tc.webdynpro.modelimpl.dynamicrfc.DynamicRFCModel.MetadataCache wdModelMetadataCache =
  	new com.sap.tc.webdynpro.modelimpl.dynamicrfc.DynamicRFCModel.MetadataCache(CRSAPArchive.class);

  /**
   * Called by superclass to retrieve the model's metadata cache.
   */  
  protected com.sap.tc.webdynpro.modelimpl.dynamicrfc.DynamicRFCModel.MetadataCache wdGetInstanceMetadataCache() {
    return wdModelMetadataCache;
  }

  /**
   * Called to retrieve the model's metadata cache.
   */  
  static com.sap.tc.webdynpro.modelimpl.dynamicrfc.DynamicRFCModel.MetadataCache wdGetStaticMetadataCache() {
    return wdModelMetadataCache;
  }
  
  /**
   * Constructor
   */
  public CRSAPArchive() {
    this(DEFAULT_MODELSCOPE, null);
  }

  /**
   * Constructor
   * @param instanceId
   */
  public CRSAPArchive( String instanceId) {
    this(DEFAULT_MODELSCOPE, instanceId);
  }

  /**
   * Constructor
   * @param scope
   */
  public CRSAPArchive( com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType scope) {
    this(scope, null);
  }

  /**
   * Constructor
   * @param scope
   * @param instanceId
   */
  public CRSAPArchive( com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType scope, String instanceId) {
    super(WD_STATIC_TYPE_INFO, staticDescriptor, staticGenerationInfo, DEFAULT_SYSTEMNAME, LOGICAL_DICTIONARY_NAME, scope, instanceId, DEFAULT_RFCMETADATA_SYSTEMNAME, LOGICAL_DICTIONARY_DEFAULT_SYSTEM_NAME);
  }

  public it.alcantara.crsap.model.Zcrsap_Create_Output zcrsap_Create(it.alcantara.crsap.model.Zcrsap_Create_Input input)
    throws com.sap.aii.proxy.framework.core.SystemFaultException,
           com.sap.aii.proxy.framework.core.ApplicationFaultException,
           com.sap.tc.webdynpro.modelimpl.dynamicrfc.WDDynamicRFC_Fault_Exception,
           com.sap.tc.webdynpro.modelimpl.dynamicrfc.WDDynamicRFCExecuteException {
    com.sap.aii.proxy.framework.core.BaseType result = null;
    prepareExecute();
    try {
      result = send$(input, "urn:sap-com:document:sap:rfc:functions", "CRSAPArchive.PortType", "ZCRSAP_CREATE", new it.alcantara.crsap.model.Zcrsap_Create_Output());
    } catch (com.sap.aii.proxy.framework.core.ApplicationFaultException e){
      if (e instanceof com.sap.tc.webdynpro.modelimpl.dynamicrfc.WDDynamicRFC_Fault_Exception)
      	{ throw (com.sap.tc.webdynpro.modelimpl.dynamicrfc.WDDynamicRFC_Fault_Exception)e;}
      throw createExceptionWrongExceptionType$(e);
    }
    if (result == null || (result instanceof it.alcantara.crsap.model.Zcrsap_Create_Output)) {
      return (it.alcantara.crsap.model.Zcrsap_Create_Output) result;
    } else { 
      throw createExceptionWrongType$(result);
    }
  }

}
