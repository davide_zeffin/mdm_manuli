// ---------------------------------------------------------------------------
// This file has been generated partially by the Web Dynpro Code Generator.
// MODIFY CODE ONLY IN SECTIONS ENCLOSED BY @@begin AND @@end.
// ALL OTHER CHANGES WILL BE LOST IF THE FILE IS REGENERATED.
// ---------------------------------------------------------------------------
package it.alcantara.crsap.model;

// 
// IMPORTANT NOTE: 
// _ALL_ IMPORT STATEMENTS MUST BE PLACED IN THE FOLLOWING SECTION ENCLOSED
// BY @@begin imports AND @@end. FURTHERMORE, THIS SECTION MUST ALWAYS CONTAIN
// AT LEAST ONE IMPORT STATEMENT (E.G. THAT FOR java.lang.String).
// OTHERWISE, USING THE ECLIPSE FUNCTION "Organize Imports" FOLLOWED BY
// A WEB DYNPRO CODE GENERATION (E.G. PROJECT BUILD) WILL RESULT IN THE LOSS
// OF IMPORT STATEMENTS.
//
//@@begin imports
import java.lang.String;
//@@end

//@@begin documentation
/**
 * Title:        Web Dynpro Version 6.30
 * Description:  "Zcrsap_Attributes" RFC Adapter ModelClass implementation
 * Copyright:    Copyright (c) 2001 - 2002. All rights reserved.
 * Company:      SAP AG
 * @author       File created by Web Dynpro code generator
 */
//@@end

public class Zcrsap_Attributes extends com.sap.tc.webdynpro.modelimpl.dynamicrfc.DynamicRFCModelClass implements java.io.Serializable
{
  /** Logging location. */
  private static final com.sap.tc.logging.Location logger = com.sap.tc.logging.Location.getLocation(Zcrsap_Attributes.class);
  static {
    //@@begin id
    String id = "$Id$";
    //@@end
    com.sap.tc.logging.Location.getLocation("ID.com.sap.tc.webdynpro").infoT(id);
  }

  private static final long serialVersionUID = 858420244L ;
  private static final com.sap.aii.proxy.framework.core.GenerationInfo staticGenerationInfo = new com.sap.aii.proxy.framework.core.GenerationInfo("2.0", 1057090423493L) ;

  private static final Class _associatedModelClazz = it.alcantara.crsap.model.CRSAPArchive.class;
  private static final com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType DEFAULT_SCOPE =
    it.alcantara.crsap.model.CRSAPArchive.DEFAULT_MODELSCOPE;

  /**  Simple Constructor  */
  public Zcrsap_Attributes () {
    super( descriptor( com.sap.tc.webdynpro.modelimpl.dynamicrfc.RFCMetadataRepository.getSingleton(it.alcantara.crsap.model.CRSAPArchive.DEFAULT_RFCMETADATA_SYSTEMNAME),
                       it.alcantara.crsap.model.CRSAPArchive.wdGetStaticMetadataCache().getBaseTypeDescriptorMap(),
                       Zcrsap_Attributes.class,
                       "ZCRSAP_ATTRIBUTES",
                       PROXYTYPE_STRUCTURE, 
                       "Zcrsap_Attributes", 
                       "it.alcantara.crsap.model" ), 
            staticGenerationInfo);
  }

  /** Constructor for specific scope
   *  use this Constructor, if this instance should belong to a model instance
   *  of a different scope than the default model scope.
   *  The default model scope can be found in it.alcantara.crsap.model.CRSAPArchive.DEFAULT_MODELSCOPE
   * 
   *  @param scope the WDModelScopeType representing in which scope this instance is to be maintained
   */
  public Zcrsap_Attributes (com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType scope) {
    this();
    maintainInScope(scope);
  }

  /** Constructor for specific modelInstanceId
   *  use this Constructor, if this instance should belong to a different model instance
   *  than the default model instance.
   *  The default modelInstance has no id (null). 
   * 
   *  @param modelInstanceId a String representing the model instance this modelClass instance should belong to
   */
  public Zcrsap_Attributes (String modelInstanceId) {
    this();
    maintainInInstanceId(modelInstanceId);
  }

  /** Constructor for specific scope and modelInstanceId
   *  use this Constructor, of this instance should belong to a differnt scope
   *  and a different model instance than the default model scope and model instance.
   *  The default model scope can be found in it.alcantara.crsap.model.CRSAPArchive.DEFAULT_MODELSCOPE
   *  The default modelInstance has no id (null). 
   * 
   *  @param scope the WDModelScopeType representing in which scope this instance is to be maintained
   *  @param modelInstanceId a String representing the model instance this modelClass instance should belong to
   */
  public Zcrsap_Attributes (com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType scope, String modelInstanceId) {
    this();
    maintainInScopeAndInstanceId(scope, modelInstanceId);
  }

  /** Constructor for specific model Instance
   *  use this Constructor, if this instance should belong to a specific instance
   *  of the model it.alcantara.crsap.model.CRSAPArchive 
   *  passed as a parameter to this constructor.
   * 
   *  @param modelInstance the DynamicRFCModel referring to the modelInstance
   */
  public Zcrsap_Attributes ( it.alcantara.crsap.model.CRSAPArchive modelInstance) {
    this();
    maintainInModel(modelInstance);
  }

  /**
   * Returns the modelInstance associated with this ModelClass 
   */
  public CRSAPArchive modelInstance() {
    return (CRSAPArchive)associatedModel();
  }

  protected com.sap.tc.webdynpro.modelimpl.dynamicrfc.DynamicRFCModel.MetadataCache wdGetMetadataCache() {
    return it.alcantara.crsap.model.CRSAPArchive.wdGetStaticMetadataCache();	
  }
  protected Class _associatedModelClazz() {
    return _associatedModelClazz;
  }
  protected com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType _defaultScope(){
    return DEFAULT_SCOPE;
  }

  /* ***************************************************************************
   *  ModelAttribute -> Alternativasvil
   * **************************************************************************/
  /** getter for ModelAttribute -> Alternativasvil 
   *  @return value of ModelAttribute Alternativasvil */
  public java.lang.String getAlternativasvil() {
    return super.getAttributeValueAsString("Alternativasvil");
  }
 
  /** setter for ModelAttribute -> Alternativasvil 
   *  @param value new value for ModelAttribute Alternativasvil */
  public void setAlternativasvil(java.lang.String value) {
    super.setAttributeValueAsString("Alternativasvil", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Analisicosti
   * **************************************************************************/
  /** getter for ModelAttribute -> Analisicosti 
   *  @return value of ModelAttribute Analisicosti */
  public java.lang.String getAnalisicosti() {
    return super.getAttributeValueAsString("Analisicosti");
  }
 
  /** setter for ModelAttribute -> Analisicosti 
   *  @param value new value for ModelAttribute Analisicosti */
  public void setAnalisicosti(java.lang.String value) {
    super.setAttributeValueAsString("Analisicosti", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Codicecr
   * **************************************************************************/
  /** getter for ModelAttribute -> Codicecr 
   *  @return value of ModelAttribute Codicecr */
  public java.lang.String getCodicecr() {
    return super.getAttributeValueAsString("Codicecr");
  }
 
  /** setter for ModelAttribute -> Codicecr 
   *  @param value new value for ModelAttribute Codicecr */
  public void setCodicecr(java.lang.String value) {
    super.setAttributeValueAsString("Codicecr", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Costototale
   * **************************************************************************/
  /** getter for ModelAttribute -> Costototale 
   *  @return value of ModelAttribute Costototale */
  public double getCostototale() {
    return super.getAttributeValueAsDouble("Costototale");
  }
 
  /** setter for ModelAttribute -> Costototale 
   *  @param value new value for ModelAttribute Costototale */
  public void setCostototale(double value) {
    super.setAttributeValueAsDouble("Costototale", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Crstatus
   * **************************************************************************/
  /** getter for ModelAttribute -> Crstatus 
   *  @return value of ModelAttribute Crstatus */
  public short getCrstatus() {
    return super.getAttributeValueAsShort("Crstatus");
  }
 
  /** setter for ModelAttribute -> Crstatus 
   *  @param value new value for ModelAttribute Crstatus */
  public void setCrstatus(short value) {
    super.setAttributeValueAsShort("Crstatus", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Datarichiesta
   * **************************************************************************/
  /** getter for ModelAttribute -> Datarichiesta 
   *  @return value of ModelAttribute Datarichiesta */
  public java.sql.Date getDatarichiesta() {
    return super.getAttributeValueAsDate("Datarichiesta");
  }
 
  /** setter for ModelAttribute -> Datarichiesta 
   *  @param value new value for ModelAttribute Datarichiesta */
  public void setDatarichiesta(java.sql.Date value) {
    super.setAttributeValueAsDate("Datarichiesta", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Descrizione
   * **************************************************************************/
  /** getter for ModelAttribute -> Descrizione 
   *  @return value of ModelAttribute Descrizione */
  public java.lang.String getDescrizione() {
    return super.getAttributeValueAsString("Descrizione");
  }
 
  /** setter for ModelAttribute -> Descrizione 
   *  @param value new value for ModelAttribute Descrizione */
  public void setDescrizione(java.lang.String value) {
    super.setAttributeValueAsString("Descrizione", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Impatto
   * **************************************************************************/
  /** getter for ModelAttribute -> Impatto 
   *  @return value of ModelAttribute Impatto */
  public java.lang.String getImpatto() {
    return super.getAttributeValueAsString("Impatto");
  }
 
  /** setter for ModelAttribute -> Impatto 
   *  @param value new value for ModelAttribute Impatto */
  public void setImpatto(java.lang.String value) {
    super.setAttributeValueAsString("Impatto", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Priorita
   * **************************************************************************/
  /** getter for ModelAttribute -> Priorita 
   *  @return value of ModelAttribute Priorita */
  public java.lang.String getPriorita() {
    return super.getAttributeValueAsString("Priorita");
  }
 
  /** setter for ModelAttribute -> Priorita 
   *  @param value new value for ModelAttribute Priorita */
  public void setPriorita(java.lang.String value) {
    super.setAttributeValueAsString("Priorita", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Responsabilesior
   * **************************************************************************/
  /** getter for ModelAttribute -> Responsabilesior 
   *  @return value of ModelAttribute Responsabilesior */
  public java.lang.String getResponsabilesior() {
    return super.getAttributeValueAsString("Responsabilesior");
  }
 
  /** setter for ModelAttribute -> Responsabilesior 
   *  @param value new value for ModelAttribute Responsabilesior */
  public void setResponsabilesior(java.lang.String value) {
    super.setAttributeValueAsString("Responsabilesior", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Richiedente
   * **************************************************************************/
  /** getter for ModelAttribute -> Richiedente 
   *  @return value of ModelAttribute Richiedente */
  public java.lang.String getRichiedente() {
    return super.getAttributeValueAsString("Richiedente");
  }
 
  /** setter for ModelAttribute -> Richiedente 
   *  @param value new value for ModelAttribute Richiedente */
  public void setRichiedente(java.lang.String value) {
    super.setAttributeValueAsString("Richiedente", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Rif_Ticket
   * **************************************************************************/
  /** getter for ModelAttribute -> Rif_Ticket 
   *  @return value of ModelAttribute Rif_Ticket */
  public java.lang.String getRif_Ticket() {
    return super.getAttributeValueAsString("Rif_Ticket");
  }
 
  /** setter for ModelAttribute -> Rif_Ticket 
   *  @param value new value for ModelAttribute Rif_Ticket */
  public void setRif_Ticket(java.lang.String value) {
    super.setAttributeValueAsString("Rif_Ticket", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Risorseassegnate
   * **************************************************************************/
  /** getter for ModelAttribute -> Risorseassegnate 
   *  @return value of ModelAttribute Risorseassegnate */
  public java.lang.String getRisorseassegnate() {
    return super.getAttributeValueAsString("Risorseassegnate");
  }
 
  /** setter for ModelAttribute -> Risorseassegnate 
   *  @param value new value for ModelAttribute Risorseassegnate */
  public void setRisorseassegnate(java.lang.String value) {
    super.setAttributeValueAsString("Risorseassegnate", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Societa_Cons
   * **************************************************************************/
  /** getter for ModelAttribute -> Societa_Cons 
   *  @return value of ModelAttribute Societa_Cons */
  public java.lang.String getSocieta_Cons() {
    return super.getAttributeValueAsString("Societa_Cons");
  }
 
  /** setter for ModelAttribute -> Societa_Cons 
   *  @param value new value for ModelAttribute Societa_Cons */
  public void setSocieta_Cons(java.lang.String value) {
    super.setAttributeValueAsString("Societa_Cons", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Sol_Proposta
   * **************************************************************************/
  /** getter for ModelAttribute -> Sol_Proposta 
   *  @return value of ModelAttribute Sol_Proposta */
  public java.lang.String getSol_Proposta() {
    return super.getAttributeValueAsString("Sol_Proposta");
  }
 
  /** setter for ModelAttribute -> Sol_Proposta 
   *  @param value new value for ModelAttribute Sol_Proposta */
  public void setSol_Proposta(java.lang.String value) {
    super.setAttributeValueAsString("Sol_Proposta", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Soluzioneimp
   * **************************************************************************/
  /** getter for ModelAttribute -> Soluzioneimp 
   *  @return value of ModelAttribute Soluzioneimp */
  public java.lang.String getSoluzioneimp() {
    return super.getAttributeValueAsString("Soluzioneimp");
  }
 
  /** setter for ModelAttribute -> Soluzioneimp 
   *  @param value new value for ModelAttribute Soluzioneimp */
  public void setSoluzioneimp(java.lang.String value) {
    super.setAttributeValueAsString("Soluzioneimp", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Titolo
   * **************************************************************************/
  /** getter for ModelAttribute -> Titolo 
   *  @return value of ModelAttribute Titolo */
  public java.lang.String getTitolo() {
    return super.getAttributeValueAsString("Titolo");
  }
 
  /** setter for ModelAttribute -> Titolo 
   *  @param value new value for ModelAttribute Titolo */
  public void setTitolo(java.lang.String value) {
    super.setAttributeValueAsString("Titolo", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Tot_Gior_Svil
   * **************************************************************************/
  /** getter for ModelAttribute -> Tot_Gior_Svil 
   *  @return value of ModelAttribute Tot_Gior_Svil */
  public int getTot_Gior_Svil() {
    return super.getAttributeValueAsInt("Tot_Gior_Svil");
  }
 
  /** setter for ModelAttribute -> Tot_Gior_Svil 
   *  @param value new value for ModelAttribute Tot_Gior_Svil */
  public void setTot_Gior_Svil(int value) {
    super.setAttributeValueAsInt("Tot_Gior_Svil", value);
  }
   
  /*
   * The following code section can be used for any Java code that is 
   * not to be visible to other controllers/views or that contains constructs
   * currently not supported directly by Web Dynpro (such as inner classes or
   * member variables etc.). </p>
   *
   * Note: The content of this section is in no way managed/controlled
   * by the Web Dynpro Designtime or the Web Dynpro Runtime. 
   */
  //@@begin others
  //@@end
  
  public static class Zcrsap_Attributes_List extends com.sap.tc.webdynpro.modelimpl.dynamicrfc.DynamicRFCList implements java.io.Serializable , java.util.List   {

    protected Class _associatedModelClazz() { return _associatedModelClazz; }
	protected com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType _defaultScope(){ return DEFAULT_SCOPE; }

    private static final long serialVersionUID = 60422512L ;
    
    public Zcrsap_Attributes_List () {
      super(createElementProperties(it.alcantara.crsap.model.Zcrsap_Attributes.class, new it.alcantara.crsap.model.Zcrsap_Attributes() ));
    }

    /** Constructor for specific scope
     *  use this Constructor, if this List should belong to a model instance
     *  of a different scope than the default model scope.
     *  The default model scope can be found in it.alcantara.crsap.model.CRSAPArchive.DEFAULT_MODELSCOPE
     * 
     *  @param scope the WDModelScopeType representing in which scope this List is to be maintained
     */
    public Zcrsap_Attributes_List (com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType scope) {
      this();
      maintainInScope(scope);
    }
  
    /** Constructor for specific modelInstanceId
     *  use this Constructor, if this List should belong to a different model instance
     *  than the default model instance.
     *  The default modelInstance has no id (null). 
     * 
     *  @param modelInstanceId a String representing the model instance this List should belong to
     */
    public Zcrsap_Attributes_List (String modelInstanceId) {
      this();
      maintainInInstanceId(modelInstanceId);
    }
  
    /** Constructor for specific scope and modelInstanceId
     *  use this Constructor, of this List should belong to a differnt scope
     *  and a different model instance than the default model scope and model instance.
     *  The default model scope can be found in it.alcantara.crsap.model.CRSAPArchive.DEFAULT_MODELSCOPE
     *  The default modelInstance has no id (null). 
     * 
     *  @param scope the WDModelScopeType representing in which scope this List is to be maintained
     *  @param modelInstanceId a String representing the model instance this List should belong to
     */
    public Zcrsap_Attributes_List (com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType scope, String modelInstanceId) {
      this();
      maintainInScopeAndInstanceId(scope, modelInstanceId);
    }
  
    /** Constructor for specific model Instance
     *  use this Constructor, if this instance should belong to a specific instance
     *  of the model it.alcantara.crsap.model.CRSAPArchive 
     *  passed as a parameter to this constructor.
     * 
     *  @param modelInstance the DynamicRFCModel referring to the modelInstance
     */
    public Zcrsap_Attributes_List ( it.alcantara.crsap.model.CRSAPArchive modelInstance) {
      this();
      maintainInModel(modelInstance);
    }

    public it.alcantara.crsap.model.Zcrsap_Attributes[] toArrayZcrsap_Attributes() {
      return (it.alcantara.crsap.model.Zcrsap_Attributes[])super.toArray(new it.alcantara.crsap.model.Zcrsap_Attributes[] {});
    }
    
    public int lastIndexOfZcrsap_Attributes(it.alcantara.crsap.model.Zcrsap_Attributes item) {
      return super.lastIndexOf(item);
    }
    
    public int indexOfZcrsap_Attributes(it.alcantara.crsap.model.Zcrsap_Attributes item) {
      return super.indexOf(item);
    }
    
    public Zcrsap_Attributes_List subListZcrsap_Attributes(int fromIndex, int toIndex) {
      return (Zcrsap_Attributes_List)super.subList(fromIndex, toIndex);
    }
    
    public void addAllZcrsap_Attributes(Zcrsap_Attributes_List item) {
      super.addAll(item);
    }
    
    public void addZcrsap_Attributes(it.alcantara.crsap.model.Zcrsap_Attributes item) {
      super.add(item);
    }
    
    public boolean removeZcrsap_Attributes(it.alcantara.crsap.model.Zcrsap_Attributes item) {
      return super.remove(item);
    }
    
    public it.alcantara.crsap.model.Zcrsap_Attributes getZcrsap_Attributes(int index) {
      return (it.alcantara.crsap.model.Zcrsap_Attributes)super.get(index);
    }
    
    public boolean containsAllZcrsap_Attributes(Zcrsap_Attributes_List item) {
      return super.containsAll(item);
    }
    
    public void addZcrsap_Attributes(int index, it.alcantara.crsap.model.Zcrsap_Attributes item) {
      super.add(index, item);
    }
    
    public boolean containsZcrsap_Attributes(it.alcantara.crsap.model.Zcrsap_Attributes item) {
      return super.contains(item);
    }
        
    public void addAllZcrsap_Attributes(int index, Zcrsap_Attributes_List item) {
      super.addAll(index, item);
    }
    
    public it.alcantara.crsap.model.Zcrsap_Attributes setZcrsap_Attributes(int index, it.alcantara.crsap.model.Zcrsap_Attributes item) {
      return (it.alcantara.crsap.model.Zcrsap_Attributes)super.set(index, item);
    }
    
    public it.alcantara.crsap.model.Zcrsap_Attributes removeZcrsap_Attributes(int index) {
      return (it.alcantara.crsap.model.Zcrsap_Attributes)super.remove(index);
    }
  }
  
}
