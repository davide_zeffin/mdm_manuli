// ---------------------------------------------------------------------------
// This file has been generated partially by the Web Dynpro Code Generator.
// MODIFY CODE ONLY IN SECTIONS ENCLOSED BY @@begin AND @@end.
// ALL OTHER CHANGES WILL BE LOST IF THE FILE IS REGENERATED.
// ---------------------------------------------------------------------------
package it.alcantara.model.getattach;

// 
// IMPORTANT NOTE: 
// _ALL_ IMPORT STATEMENTS MUST BE PLACED IN THE FOLLOWING SECTION ENCLOSED
// BY @@begin imports AND @@end. FURTHERMORE, THIS SECTION MUST ALWAYS CONTAIN
// AT LEAST ONE IMPORT STATEMENT (E.G. THAT FOR java.lang.String).
// OTHERWISE, USING THE ECLIPSE FUNCTION "Organize Imports" FOLLOWED BY
// A WEB DYNPRO CODE GENERATION (E.G. PROJECT BUILD) WILL RESULT IN THE LOSS
// OF IMPORT STATEMENTS.
//
//@@begin imports
import java.lang.String;
//@@end

//@@begin documentation
/**
 * Title:        Web Dynpro Version 6.30
 * Description:  "Bapiret2" RFC Adapter ModelClass implementation
 * Copyright:    Copyright (c) 2001 - 2002. All rights reserved.
 * Company:      SAP AG
 * @author       File created by Web Dynpro code generator
 */
//@@end

public class Bapiret2 extends com.sap.tc.webdynpro.modelimpl.dynamicrfc.DynamicRFCModelClass implements java.io.Serializable
{
  /** Logging location. */
  private static final com.sap.tc.logging.Location logger = com.sap.tc.logging.Location.getLocation(Bapiret2.class);
  static {
    //@@begin id
    String id = "$Id$";
    //@@end
    com.sap.tc.logging.Location.getLocation("ID.com.sap.tc.webdynpro").infoT(id);
  }

  private static final long serialVersionUID = 858420244L ;
  private static final com.sap.aii.proxy.framework.core.GenerationInfo staticGenerationInfo = new com.sap.aii.proxy.framework.core.GenerationInfo("2.0", 1057090423493L) ;

  private static final Class _associatedModelClazz = it.alcantara.model.getattach.CRSAPGetAttach.class;
  private static final com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType DEFAULT_SCOPE =
    it.alcantara.model.getattach.CRSAPGetAttach.DEFAULT_MODELSCOPE;

  /**  Simple Constructor  */
  public Bapiret2 () {
    super( descriptor( com.sap.tc.webdynpro.modelimpl.dynamicrfc.RFCMetadataRepository.getSingleton(it.alcantara.model.getattach.CRSAPGetAttach.DEFAULT_RFCMETADATA_SYSTEMNAME),
                       it.alcantara.model.getattach.CRSAPGetAttach.wdGetStaticMetadataCache().getBaseTypeDescriptorMap(),
                       Bapiret2.class,
                       "BAPIRET2",
                       PROXYTYPE_STRUCTURE, 
                       "Bapiret2", 
                       "it.alcantara.model.getattach" ), 
            staticGenerationInfo);
  }

  /** Constructor for specific scope
   *  use this Constructor, if this instance should belong to a model instance
   *  of a different scope than the default model scope.
   *  The default model scope can be found in it.alcantara.model.getattach.CRSAPGetAttach.DEFAULT_MODELSCOPE
   * 
   *  @param scope the WDModelScopeType representing in which scope this instance is to be maintained
   */
  public Bapiret2 (com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType scope) {
    this();
    maintainInScope(scope);
  }

  /** Constructor for specific modelInstanceId
   *  use this Constructor, if this instance should belong to a different model instance
   *  than the default model instance.
   *  The default modelInstance has no id (null). 
   * 
   *  @param modelInstanceId a String representing the model instance this modelClass instance should belong to
   */
  public Bapiret2 (String modelInstanceId) {
    this();
    maintainInInstanceId(modelInstanceId);
  }

  /** Constructor for specific scope and modelInstanceId
   *  use this Constructor, of this instance should belong to a differnt scope
   *  and a different model instance than the default model scope and model instance.
   *  The default model scope can be found in it.alcantara.model.getattach.CRSAPGetAttach.DEFAULT_MODELSCOPE
   *  The default modelInstance has no id (null). 
   * 
   *  @param scope the WDModelScopeType representing in which scope this instance is to be maintained
   *  @param modelInstanceId a String representing the model instance this modelClass instance should belong to
   */
  public Bapiret2 (com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType scope, String modelInstanceId) {
    this();
    maintainInScopeAndInstanceId(scope, modelInstanceId);
  }

  /** Constructor for specific model Instance
   *  use this Constructor, if this instance should belong to a specific instance
   *  of the model it.alcantara.model.getattach.CRSAPGetAttach 
   *  passed as a parameter to this constructor.
   * 
   *  @param modelInstance the DynamicRFCModel referring to the modelInstance
   */
  public Bapiret2 ( it.alcantara.model.getattach.CRSAPGetAttach modelInstance) {
    this();
    maintainInModel(modelInstance);
  }

  /**
   * Returns the modelInstance associated with this ModelClass 
   */
  public CRSAPGetAttach modelInstance() {
    return (CRSAPGetAttach)associatedModel();
  }

  protected com.sap.tc.webdynpro.modelimpl.dynamicrfc.DynamicRFCModel.MetadataCache wdGetMetadataCache() {
    return it.alcantara.model.getattach.CRSAPGetAttach.wdGetStaticMetadataCache();	
  }
  protected Class _associatedModelClazz() {
    return _associatedModelClazz;
  }
  protected com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType _defaultScope(){
    return DEFAULT_SCOPE;
  }

  /* ***************************************************************************
   *  ModelAttribute -> Field
   * **************************************************************************/
  /** getter for ModelAttribute -> Field 
   *  @return value of ModelAttribute Field */
  public java.lang.String getField() {
    return super.getAttributeValueAsString("Field");
  }
 
  /** setter for ModelAttribute -> Field 
   *  @param value new value for ModelAttribute Field */
  public void setField(java.lang.String value) {
    super.setAttributeValueAsString("Field", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Id
   * **************************************************************************/
  /** getter for ModelAttribute -> Id 
   *  @return value of ModelAttribute Id */
  public java.lang.String getId() {
    return super.getAttributeValueAsString("Id");
  }
 
  /** setter for ModelAttribute -> Id 
   *  @param value new value for ModelAttribute Id */
  public void setId(java.lang.String value) {
    super.setAttributeValueAsString("Id", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Log_Msg_No
   * **************************************************************************/
  /** getter for ModelAttribute -> Log_Msg_No 
   *  @return value of ModelAttribute Log_Msg_No */
  public java.lang.String getLog_Msg_No() {
    return super.getAttributeValueAsString("Log_Msg_No");
  }
 
  /** setter for ModelAttribute -> Log_Msg_No 
   *  @param value new value for ModelAttribute Log_Msg_No */
  public void setLog_Msg_No(java.lang.String value) {
    super.setAttributeValueAsString("Log_Msg_No", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Log_No
   * **************************************************************************/
  /** getter for ModelAttribute -> Log_No 
   *  @return value of ModelAttribute Log_No */
  public java.lang.String getLog_No() {
    return super.getAttributeValueAsString("Log_No");
  }
 
  /** setter for ModelAttribute -> Log_No 
   *  @param value new value for ModelAttribute Log_No */
  public void setLog_No(java.lang.String value) {
    super.setAttributeValueAsString("Log_No", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Message
   * **************************************************************************/
  /** getter for ModelAttribute -> Message 
   *  @return value of ModelAttribute Message */
  public java.lang.String getMessage() {
    return super.getAttributeValueAsString("Message");
  }
 
  /** setter for ModelAttribute -> Message 
   *  @param value new value for ModelAttribute Message */
  public void setMessage(java.lang.String value) {
    super.setAttributeValueAsString("Message", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Message_V1
   * **************************************************************************/
  /** getter for ModelAttribute -> Message_V1 
   *  @return value of ModelAttribute Message_V1 */
  public java.lang.String getMessage_V1() {
    return super.getAttributeValueAsString("Message_V1");
  }
 
  /** setter for ModelAttribute -> Message_V1 
   *  @param value new value for ModelAttribute Message_V1 */
  public void setMessage_V1(java.lang.String value) {
    super.setAttributeValueAsString("Message_V1", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Message_V2
   * **************************************************************************/
  /** getter for ModelAttribute -> Message_V2 
   *  @return value of ModelAttribute Message_V2 */
  public java.lang.String getMessage_V2() {
    return super.getAttributeValueAsString("Message_V2");
  }
 
  /** setter for ModelAttribute -> Message_V2 
   *  @param value new value for ModelAttribute Message_V2 */
  public void setMessage_V2(java.lang.String value) {
    super.setAttributeValueAsString("Message_V2", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Message_V3
   * **************************************************************************/
  /** getter for ModelAttribute -> Message_V3 
   *  @return value of ModelAttribute Message_V3 */
  public java.lang.String getMessage_V3() {
    return super.getAttributeValueAsString("Message_V3");
  }
 
  /** setter for ModelAttribute -> Message_V3 
   *  @param value new value for ModelAttribute Message_V3 */
  public void setMessage_V3(java.lang.String value) {
    super.setAttributeValueAsString("Message_V3", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Message_V4
   * **************************************************************************/
  /** getter for ModelAttribute -> Message_V4 
   *  @return value of ModelAttribute Message_V4 */
  public java.lang.String getMessage_V4() {
    return super.getAttributeValueAsString("Message_V4");
  }
 
  /** setter for ModelAttribute -> Message_V4 
   *  @param value new value for ModelAttribute Message_V4 */
  public void setMessage_V4(java.lang.String value) {
    super.setAttributeValueAsString("Message_V4", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Number
   * **************************************************************************/
  /** getter for ModelAttribute -> Number 
   *  @return value of ModelAttribute Number */
  public java.lang.String getNumber() {
    return super.getAttributeValueAsString("Number");
  }
 
  /** setter for ModelAttribute -> Number 
   *  @param value new value for ModelAttribute Number */
  public void setNumber(java.lang.String value) {
    super.setAttributeValueAsString("Number", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Parameter
   * **************************************************************************/
  /** getter for ModelAttribute -> Parameter 
   *  @return value of ModelAttribute Parameter */
  public java.lang.String getParameter() {
    return super.getAttributeValueAsString("Parameter");
  }
 
  /** setter for ModelAttribute -> Parameter 
   *  @param value new value for ModelAttribute Parameter */
  public void setParameter(java.lang.String value) {
    super.setAttributeValueAsString("Parameter", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Row
   * **************************************************************************/
  /** getter for ModelAttribute -> Row 
   *  @return value of ModelAttribute Row */
  public int getRow() {
    return super.getAttributeValueAsInt("Row");
  }
 
  /** setter for ModelAttribute -> Row 
   *  @param value new value for ModelAttribute Row */
  public void setRow(int value) {
    super.setAttributeValueAsInt("Row", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> System
   * **************************************************************************/
  /** getter for ModelAttribute -> System 
   *  @return value of ModelAttribute System */
  public java.lang.String getSystem() {
    return super.getAttributeValueAsString("System");
  }
 
  /** setter for ModelAttribute -> System 
   *  @param value new value for ModelAttribute System */
  public void setSystem(java.lang.String value) {
    super.setAttributeValueAsString("System", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Type
   * **************************************************************************/
  /** getter for ModelAttribute -> Type 
   *  @return value of ModelAttribute Type */
  public java.lang.String getType() {
    return super.getAttributeValueAsString("Type");
  }
 
  /** setter for ModelAttribute -> Type 
   *  @param value new value for ModelAttribute Type */
  public void setType(java.lang.String value) {
    super.setAttributeValueAsString("Type", value);
  }
   
  /*
   * The following code section can be used for any Java code that is 
   * not to be visible to other controllers/views or that contains constructs
   * currently not supported directly by Web Dynpro (such as inner classes or
   * member variables etc.). </p>
   *
   * Note: The content of this section is in no way managed/controlled
   * by the Web Dynpro Designtime or the Web Dynpro Runtime. 
   */
  //@@begin others
  //@@end
  
  public static class Bapiret2_List extends com.sap.tc.webdynpro.modelimpl.dynamicrfc.DynamicRFCList implements java.io.Serializable , java.util.List   {

    protected Class _associatedModelClazz() { return _associatedModelClazz; }
	protected com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType _defaultScope(){ return DEFAULT_SCOPE; }

    private static final long serialVersionUID = 60422512L ;
    
    public Bapiret2_List () {
      super(createElementProperties(it.alcantara.model.getattach.Bapiret2.class, new it.alcantara.model.getattach.Bapiret2() ));
    }

    /** Constructor for specific scope
     *  use this Constructor, if this List should belong to a model instance
     *  of a different scope than the default model scope.
     *  The default model scope can be found in it.alcantara.model.getattach.CRSAPGetAttach.DEFAULT_MODELSCOPE
     * 
     *  @param scope the WDModelScopeType representing in which scope this List is to be maintained
     */
    public Bapiret2_List (com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType scope) {
      this();
      maintainInScope(scope);
    }
  
    /** Constructor for specific modelInstanceId
     *  use this Constructor, if this List should belong to a different model instance
     *  than the default model instance.
     *  The default modelInstance has no id (null). 
     * 
     *  @param modelInstanceId a String representing the model instance this List should belong to
     */
    public Bapiret2_List (String modelInstanceId) {
      this();
      maintainInInstanceId(modelInstanceId);
    }
  
    /** Constructor for specific scope and modelInstanceId
     *  use this Constructor, of this List should belong to a differnt scope
     *  and a different model instance than the default model scope and model instance.
     *  The default model scope can be found in it.alcantara.model.getattach.CRSAPGetAttach.DEFAULT_MODELSCOPE
     *  The default modelInstance has no id (null). 
     * 
     *  @param scope the WDModelScopeType representing in which scope this List is to be maintained
     *  @param modelInstanceId a String representing the model instance this List should belong to
     */
    public Bapiret2_List (com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType scope, String modelInstanceId) {
      this();
      maintainInScopeAndInstanceId(scope, modelInstanceId);
    }
  
    /** Constructor for specific model Instance
     *  use this Constructor, if this instance should belong to a specific instance
     *  of the model it.alcantara.model.getattach.CRSAPGetAttach 
     *  passed as a parameter to this constructor.
     * 
     *  @param modelInstance the DynamicRFCModel referring to the modelInstance
     */
    public Bapiret2_List ( it.alcantara.model.getattach.CRSAPGetAttach modelInstance) {
      this();
      maintainInModel(modelInstance);
    }

    public it.alcantara.model.getattach.Bapiret2[] toArrayBapiret2() {
      return (it.alcantara.model.getattach.Bapiret2[])super.toArray(new it.alcantara.model.getattach.Bapiret2[] {});
    }
    
    public int lastIndexOfBapiret2(it.alcantara.model.getattach.Bapiret2 item) {
      return super.lastIndexOf(item);
    }
    
    public int indexOfBapiret2(it.alcantara.model.getattach.Bapiret2 item) {
      return super.indexOf(item);
    }
    
    public Bapiret2_List subListBapiret2(int fromIndex, int toIndex) {
      return (Bapiret2_List)super.subList(fromIndex, toIndex);
    }
    
    public void addAllBapiret2(Bapiret2_List item) {
      super.addAll(item);
    }
    
    public void addBapiret2(it.alcantara.model.getattach.Bapiret2 item) {
      super.add(item);
    }
    
    public boolean removeBapiret2(it.alcantara.model.getattach.Bapiret2 item) {
      return super.remove(item);
    }
    
    public it.alcantara.model.getattach.Bapiret2 getBapiret2(int index) {
      return (it.alcantara.model.getattach.Bapiret2)super.get(index);
    }
    
    public boolean containsAllBapiret2(Bapiret2_List item) {
      return super.containsAll(item);
    }
    
    public void addBapiret2(int index, it.alcantara.model.getattach.Bapiret2 item) {
      super.add(index, item);
    }
    
    public boolean containsBapiret2(it.alcantara.model.getattach.Bapiret2 item) {
      return super.contains(item);
    }
        
    public void addAllBapiret2(int index, Bapiret2_List item) {
      super.addAll(index, item);
    }
    
    public it.alcantara.model.getattach.Bapiret2 setBapiret2(int index, it.alcantara.model.getattach.Bapiret2 item) {
      return (it.alcantara.model.getattach.Bapiret2)super.set(index, item);
    }
    
    public it.alcantara.model.getattach.Bapiret2 removeBapiret2(int index) {
      return (it.alcantara.model.getattach.Bapiret2)super.remove(index);
    }
  }
  
}
