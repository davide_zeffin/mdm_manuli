// ---------------------------------------------------------------------------
// This file has been generated partially by the Web Dynpro Code Generator.
// MODIFY CODE ONLY IN SECTIONS ENCLOSED BY @@begin AND @@end.
// ALL OTHER CHANGES WILL BE LOST IF THE FILE IS REGENERATED.
// ---------------------------------------------------------------------------
package it.alcantara.model.counter;

//@@begin imports
import java.lang.String;
//@@end

//@@begin documentation
/**
 * Title:        Web Dynpro Version 6.30
 * Description:  "CRSAPCounter" RFC Adapter Model implementation
 * Copyright:    Copyright (c) 2001 - 2003. All rights reserved.
 * Company:      SAP AG
 * @author       File created by Web Dynpro code generator
 */
//@@end

public class CRSAPCounter extends com.sap.tc.webdynpro.modelimpl.dynamicrfc.DynamicRFCModel
{

  /**
   * Log version during <clinit>
   */
  static {
    //@@begin id
    String id = "$Id$";
    //@@end
    com.sap.tc.logging.Location.getLocation("ID.com.sap.tc.webdynpro").infoT(id);
  }

  private static final Object[][] WD_STATIC_TYPE_INFO = {
    { "it.alcantara.model.counter.Zcrsap_Counter_Get_Next_Input", "ZCRSAP_COUNTER_GET_NEXT",
      "input", new String[][] {
      } },
    { "it.alcantara.model.counter.Zcrsap_Counter_Get_Next_Output", "ZCRSAP_COUNTER_GET_NEXT",
      "output", new String[][] {
        { "E_Zcrsap_Cnt", "it.alcantara.model.counter.types.Zcrsap_Cnt" },
      } },
  };
  
  private static final com.sap.aii.proxy.framework.core.BaseProxyDescriptor staticDescriptor = 
    com.sap.aii.proxy.framework.core.BaseProxyDescriptorFactory.createNewBaseProxyDescriptor(
      "urn:sap-com:document:sap:rfc:functions:CRSAPCounter.PortType",
      new java.lang.Object[][][] {
        {
          { "ZCRSAP_COUNTER_GET_NEXT",
            "zcrsap_Counter_Get_Next",
            "ZCRSAP_COUNTER_GET_NEXT" },
          { "urn:sap-com:document:sap:rfc:functions:ZCRSAP_COUNTER_GET_NEXT.Input",
            "it.alcantara.model.counter.Zcrsap_Counter_Get_Next_Input" },
          { "urn:sap-com:document:sap:rfc:functions:ZCRSAP_COUNTER_GET_NEXT.Output",
            "it.alcantara.model.counter.Zcrsap_Counter_Get_Next_Output" },
          { "urn:sap-com:document:sap:rfc:functions:WDDynamicRFC_Fault.Exception",
            "com.sap.tc.webdynpro.modelimpl.dynamicrfc.WDDynamicRFC_Fault_Exception",
            "com.sap.tc.webdynpro.modelimpl.dynamicrfc.WDDynamicRFC_Fault" }
        },
      },
      com.sap.aii.proxy.framework.core.FactoryConstants.CONNECTION_TYPE_JCO,
      it.alcantara.model.counter.CRSAPCounter.class);

  public static final com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType DEFAULT_MODELSCOPE =
        com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType.APPLICATION_SCOPE;
  public static final String DEFAULT_SYSTEMNAME = "ECC_ESS_MODELDATA";
  public static final String DEFAULT_RFCMETADATA_SYSTEMNAME = "ECC_ESS_METADATA";
  public static final String LOGICAL_DICTIONARY_NAME = "it.alcantara.model.counter.types.CRSAPCounter";
  public static final String LOGICAL_DICTIONARY_DEFAULT_SYSTEM_NAME = "ECC_ESS_METADATA";
  private static final com.sap.aii.proxy.framework.core.GenerationInfo staticGenerationInfo = new com.sap.aii.proxy.framework.core.GenerationInfo("2.0", 1057227900576L) ;
  static{
    com.sap.tc.webdynpro.progmodel.model.api.WDModelFactory.registerDefaultScopeForModel(it.alcantara.model.counter.CRSAPCounter.class, DEFAULT_MODELSCOPE);
  }

  /**
   * Class local cache for model metadata
   */
  private static final com.sap.tc.webdynpro.modelimpl.dynamicrfc.DynamicRFCModel.MetadataCache wdModelMetadataCache =
  	new com.sap.tc.webdynpro.modelimpl.dynamicrfc.DynamicRFCModel.MetadataCache(CRSAPCounter.class);

  /**
   * Called by superclass to retrieve the model's metadata cache.
   */  
  protected com.sap.tc.webdynpro.modelimpl.dynamicrfc.DynamicRFCModel.MetadataCache wdGetInstanceMetadataCache() {
    return wdModelMetadataCache;
  }

  /**
   * Called to retrieve the model's metadata cache.
   */  
  static com.sap.tc.webdynpro.modelimpl.dynamicrfc.DynamicRFCModel.MetadataCache wdGetStaticMetadataCache() {
    return wdModelMetadataCache;
  }
  
  /**
   * Constructor
   */
  public CRSAPCounter() {
    this(DEFAULT_MODELSCOPE, null);
  }

  /**
   * Constructor
   * @param instanceId
   */
  public CRSAPCounter( String instanceId) {
    this(DEFAULT_MODELSCOPE, instanceId);
  }

  /**
   * Constructor
   * @param scope
   */
  public CRSAPCounter( com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType scope) {
    this(scope, null);
  }

  /**
   * Constructor
   * @param scope
   * @param instanceId
   */
  public CRSAPCounter( com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType scope, String instanceId) {
    super(WD_STATIC_TYPE_INFO, staticDescriptor, staticGenerationInfo, DEFAULT_SYSTEMNAME, LOGICAL_DICTIONARY_NAME, scope, instanceId, DEFAULT_RFCMETADATA_SYSTEMNAME, LOGICAL_DICTIONARY_DEFAULT_SYSTEM_NAME);
  }

  public it.alcantara.model.counter.Zcrsap_Counter_Get_Next_Output zcrsap_Counter_Get_Next(it.alcantara.model.counter.Zcrsap_Counter_Get_Next_Input input)
    throws com.sap.aii.proxy.framework.core.SystemFaultException,
           com.sap.aii.proxy.framework.core.ApplicationFaultException,
           com.sap.tc.webdynpro.modelimpl.dynamicrfc.WDDynamicRFC_Fault_Exception,
           com.sap.tc.webdynpro.modelimpl.dynamicrfc.WDDynamicRFCExecuteException {
    com.sap.aii.proxy.framework.core.BaseType result = null;
    prepareExecute();
    try {
      result = send$(input, "urn:sap-com:document:sap:rfc:functions", "CRSAPCounter.PortType", "ZCRSAP_COUNTER_GET_NEXT", new it.alcantara.model.counter.Zcrsap_Counter_Get_Next_Output());
    } catch (com.sap.aii.proxy.framework.core.ApplicationFaultException e){
      if (e instanceof com.sap.tc.webdynpro.modelimpl.dynamicrfc.WDDynamicRFC_Fault_Exception)
      	{ throw (com.sap.tc.webdynpro.modelimpl.dynamicrfc.WDDynamicRFC_Fault_Exception)e;}
      throw createExceptionWrongExceptionType$(e);
    }
    if (result == null || (result instanceof it.alcantara.model.counter.Zcrsap_Counter_Get_Next_Output)) {
      return (it.alcantara.model.counter.Zcrsap_Counter_Get_Next_Output) result;
    } else { 
      throw createExceptionWrongType$(result);
    }
  }

}
