/*
 * Created on 15-set-2011
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package it.sdb;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;

import com.sap.aii.mapping.api.AbstractTransformation;
import com.sap.aii.mapping.api.InputAttachments;
import com.sap.aii.mapping.api.InputHeader;
import com.sap.aii.mapping.api.InputParameters;
import com.sap.aii.mapping.api.InputPayload;
import com.sap.aii.mapping.api.OutputAttachments;
import com.sap.aii.mapping.api.OutputHeader;
import com.sap.aii.mapping.api.OutputParameters;
import com.sap.aii.mapping.api.OutputPayload;
import com.sap.aii.mapping.api.StreamTransformationException;
import com.sap.aii.mapping.api.TransformationInput;
import com.sap.aii.mapping.api.TransformationOutput;

public class JavaMapping extends AbstractTransformation {

	/*
	 * Each JAVA Mapping using the 7.1 API will implement the method
	 * transform(TransformationInput arg0, TransformationOutput arg1)
	 * as opposed to execute(inputStream in, outputStream out) Method
	 * in earlier version.
	 */

	public void transform(TransformationInput arg0, TransformationOutput arg1)
			throws StreamTransformationException {

		/*
		 * An info message is added to trace. An instance of trace of object is
		 * obtained by calling the getTrace method of class AbstractTransformation
		 */

		getTrace().addInfo("JAVA Mapping RemoveBodyTag is Initiated");

		/*
		 * Input payload is obtained by using
		 * arg0.getInputPayload().getInputStream()
		 */

		String inputPayload = convertInputStreamToString(arg0.getInputPayload()
				.getInputStream());
		String outputPayload = "";

		String inParam = arg0.getInputParameters().getString("TAG_NAME");
		getTrace().addInfo("Input Parameter: " + inParam);

		String startTag = "<" + inParam + ">";
		String endTag = "</" + inParam + ">";

		inputPayload = inputPayload.replaceAll(startTag, "");
		inputPayload = inputPayload.replaceAll(endTag, "");

		outputPayload = inputPayload;

		try {

			/*
			 * Output payload is returned using the TransformationOutput class
			 * arg1.getOutputPayload().getOutputStream()
			 */

			arg1.getOutputPayload().getOutputStream().write(
					outputPayload.getBytes("UTF-8"));
		} catch (Exception exception1) {
		}
	}

	public String convertInputStreamToString(InputStream in) {
		StringBuffer sb = new StringBuffer();
		try {
			InputStreamReader isr = new InputStreamReader(in);
			Reader reader = new BufferedReader(isr);
			int ch;
			while ((ch = in.read()) > -1) {
				sb.append((char) ch);
			}
			reader.close();
		} catch (Exception exception) {
		}
		return sb.toString();
	}

	/*
	 * ****************************************************************************
	 * The below Classes and Main method are implemented to help in the stand alone
	 * testing of the mapping program. You can delete the below when exporting and
	 * creating the imported archive in SAP PI.
	 * ****************************************************************************
	 */

	//Implementation of the main method is for the stand alone testing of the mapping program
	public static void main(String args[]) throws StreamTransformationException {
		RemoveBodyTag object = new RemoveBodyTag();
		try {
			InputStream in = new FileInputStream(
					new File(
							"<directory path>\\input.xml"));
			OutputStream out = new FileOutputStream(
					new File(
							"<directory path>\\output.xml"));

			InputPayloadImpl payloadInObj = new InputPayloadImpl(in);
			TransformationInputImpl transformInObj = new TransformationInputImpl(
					payloadInObj);

			OutPayloadImpl payloadOutObj = new OutPayloadImpl(out);
			TransformationOutputImpl transformOutObj = new TransformationOutputImpl(
					payloadOutObj);

			object.transform(transformInObj, transformOutObj);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

/* The below classes (InputPayloadImpl, TransformationInputImpl, OutPayloadImpl,
 * TransformationOutputImpl)are used to assist in the stand alone test
 */

class InputPayloadImpl extends InputPayload {
	InputStream in;

	public InputPayloadImpl(InputStream in) {
		this.in = in;
	}

	@Override
	public InputStream getInputStream() {
		return in;
	}
}

class TransformationInputImpl extends TransformationInput {

	InputPayload payload;

	public TransformationInputImpl(InputPayload payload) {
		this.payload = payload;
	}

	@Override
	public InputAttachments getInputAttachments() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public InputHeader getInputHeader() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public InputParameters getInputParameters() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public InputPayload getInputPayload() {
		return payload;
	}

}

class OutPayloadImpl extends OutputPayload {
	OutputStream ou;

	public OutPayloadImpl(OutputStream ou) {
		this.ou = ou;

	}

	@Override
	public OutputStream getOutputStream() {
		// TODO Auto-generated method stub
		return ou;
	}
}

class TransformationOutputImpl extends TransformationOutput {

	OutputPayload payload;

	public TransformationOutputImpl(OutputPayload payload) {
		this.payload = payload;
	}

	@Override
	public void copyInputAttachments() {
		// TODO Auto-generated method stub

	}

	@Override
	public OutputAttachments getOutputAttachments() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public OutputHeader getOutputHeader() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public OutputParameters getOutputParameters() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public OutputPayload getOutputPayload() {
		// TODO Auto-generated method stub
		return payload;
	}

}
