


import java.io.FileOutputStream;
import java.io.IOException;

import a2i.common.A2iResultSet;
import a2i.common.CatalogData;
import a2i.common.ResultSetDefinition;
import a2i.core.StringException;
import a2i.search.FreeFormParameter;
import a2i.search.FreeFormParameterField;
import a2i.search.FreeFormTableParameter;
import a2i.search.Search;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfWriter;

/**
 * Examples for free form searches via the MDM Java API.
 * 
 * <br>To run these examples, create the following Classpath Variable in your Developer Studio to link to your local MDM API JAR:</p>
 *Go to <em>Window -> Preferences -> Java -> Classpath Variables</em> and create this variable:<br>
 *<em>SAP_MDM_MDM4J</em> - Path to the folder containing MDM4J.jar<br>
 *<br>
 *Of course you need also an up-and-running MDM Server with a loaded Repository.<br>
 *The connection details are directly maintained in the source code.<br>
 *
 *<br><br>
 * 
 *This code is copyrighted by SAP AG, Dietmar-Hopp-Allee 16, 69190 Walldorf, Germany.<br><br>
 *It has been written to serve educational purposes only. You may reuse, modify and
 *redistribute it as long as the following rules are obeyed: <br><br>
 *<strong>
 *SAP AG assumes no responsibility for errors or omissions in this sample code.<br>
 *It is provided "as is" without a warranty of any kind, either express or implied,
 *including, but not limited to, the implied warranties of merchantability, fitness for a particular purpose,
 *or non-infringement.<br>
 *SAP AG shall not be liable for damages of any kind including without limitation direct, special,
 *indirect, or consequential damages that may result from the use of this code.<br>
 *SAP AG does not warrant the accuracy or completeness of information, text, graphics, links or
 *other items contained within these materials. SAP AG reserves the right to modify, replace or
 *supplement this documentation and coding without notice.
 *</strong>
 *
 * @author Andreas Seifried (D035740), Copyright SAP AG, 200
 */
public class FreeFormSearchExamples {
	
	CatalogData catalog;
	
	public void login() {
		
		catalog = new CatalogData();
		
		catalog.Login("10.194.72.24",  2345, "Admin", "admin", "English [US]");

	}
	
	public void logout() {
		
		try {
			catalog.Logout();
		} catch (StringException e) {
			System.out.println("Logout Failed!");
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Search on one single field for one single value
	 */
	public void searchSingleFieldSimple() {
		
		//FIND(Name, "Contractor") on table Products
		// case sensitive substring search in the field "Name"
		
		System.out.println();
		System.out.println("Performing searchSingleFieldSimple()");
		System.out.println("FIND(CatHier.Name, \"TRACTOR/1\") on table Products");
		System.out.println("Expected Hits: 18");
		System.out.println("=========================");
		
		ResultSetDefinition rsd = new ResultSetDefinition("prod_hoses_rec");
		rsd.AddField("category");
		rsd.AddField("Name");
		rsd.AddField("description");

		Search search = new Search("prod_hoses_rec");

		FreeFormTableParameter fftpNames = search.GetParameters().NewFreeFormTableParameter("prod_hoses_rec");
				
		FreeFormParameterField ffpfName = fftpNames.GetFields().New("category");
		FreeFormParameterField ffpfPart = fftpNames.GetFields().New("Name");
		FreeFormParameterField ffpfDesc = fftpNames.GetFields().New("description");
		ffpfName.GetFreeForm().NewString("ADLER/2", FreeFormParameter.SubstringSearchType);

		A2iResultSet result;
		try {
			result = catalog.GetResultSet(search, rsd, "category", true, 0);
			
			System.out.println("Found " + result.GetRecordCount() + " records.");
			for(int n=0; n<result.GetRecordCount(); n++) {
//				System.out.println(result.GetValueAt(n, "CatHier").GetStringValue());
				System.out.println(result.GetValueAt(n, "category").GetStringValue() + "\t\t" + 
				                   result.GetValueAt(n, "Name").GetStringValue() + "\t\t" + 
								   result.GetValueAt(n, "description").GetStringValue());
			}


		} catch (StringException e) {
			System.out.println("Search Failed!");
			e.printStackTrace();
		}

		// step 1: creation of a document-object
		Document document = new Document();
		try {
		 // step 2:
		 // we create a writer that listens to the document
		 // and directs a PDF-stream to a file
		 PdfWriter.getInstance(document,
		   new FileOutputStream("HelloWorld.pdf"));
		 // step 3: we open the document
		 document.open();
		 // step 4: we add a paragraph to the document
		 document.add(new Paragraph("Hello World"));
		} catch (DocumentException de) {
		 System.err.println(de.getMessage());
		} catch (IOException ioe) {
		 System.err.println(ioe.getMessage());
		}
		// step 5: we close the document
		document.close();

	}

	/**
	 * Search on one single field for 3 values, AND combination
	 */	
	public void searchSingleFieldAnd() {
		
		//FIND(Name, "Contractor") AND FIND(Name, "Blade") AND FIND(Name, "Standard") on table Products
		
		System.out.println();
		System.out.println("Performing searchSingleFieldAnd()");
		System.out.println("FIND(Name, \"Description\") AND FIND(Name, \"Blade\") AND FIND(Name, \"Standard\") on table Products");
		System.out.println("Expected Hits: 5");
		System.out.println("=========================");
		
		ResultSetDefinition rsd = new ResultSetDefinition("Products");
		rsd.AddField("Name");
		
		Search search = new Search("Products");

		FreeFormTableParameter fftpNames = search.GetParameters().NewFreeFormTableParameter("Products");
		
		FreeFormParameterField ffpfName = fftpNames.GetFields().New("Name", FreeFormParameterField.SEARCH_OPERATOR_AND);
		ffpfName.GetFreeForm().NewString("Description", FreeFormParameter.SubstringSearchType);
		ffpfName.GetFreeForm().NewString("Blade", FreeFormParameter.SubstringSearchType);
		ffpfName.GetFreeForm().NewString("Standard", FreeFormParameter.SubstringSearchType);

		A2iResultSet result;
		try {
			result = catalog.GetResultSet(search, rsd, "Name", true, 0);

			System.out.println("Found " + result.GetRecordCount() + " records.");
//			for(int n=0; n<result.GetRecordCount(); n++) {
//				System.out.println(result.GetValueAt(n, "Name").GetStringValue());
//			}


		} catch (StringException e) {
			System.out.println("Search Failed!");
			e.printStackTrace();
		}
	}

	/**
	 * Search on one single field for 3 values, OR combination
	 */	
	public void searchSingleFieldOr() {
		
		//FIND(Name, "Contractor") OR FIND(Name, "Blade") OR FIND(Name, "Standard") on table Products
		
		System.out.println();
		System.out.println("Performing searchSingleFieldOr()");
		System.out.println("FIND(Name, \"Description\") OR FIND(Name, \"Blade\") OR FIND(Name, \"Standard\") on table Products");
		System.out.println("Expected Hits: 48");
		System.out.println("=========================");
		
		ResultSetDefinition rsd = new ResultSetDefinition("Products");
		rsd.AddField("Name");
		
		Search search = new Search("Products");

		FreeFormTableParameter fftpNames = search.GetParameters().NewFreeFormTableParameter("Products");
		
		FreeFormParameterField ffpfName = fftpNames.GetFields().New("Name", FreeFormParameterField.SEARCH_OPERATOR_OR);
		ffpfName.GetFreeForm().NewString("Description", FreeFormParameter.SubstringSearchType);
		ffpfName.GetFreeForm().NewString("Blade", FreeFormParameter.SubstringSearchType);
		ffpfName.GetFreeForm().NewString("Standard", FreeFormParameter.SubstringSearchType);

		A2iResultSet result;
		try {
			result = catalog.GetResultSet(search, rsd, "Name", true, 0);

			System.out.println("Found " + result.GetRecordCount() + " records.");
//			for(int n=0; n<result.GetRecordCount(); n++) {
//				System.out.println(result.GetValueAt(n, "Name").GetStringValue());
//			}


		} catch (StringException e) {
			System.out.println("Search Failed!");
			e.printStackTrace();
		}
	}
	
	/**
	 * Search on one single field for 3 values, nested OR / AND combination
	 */		
	public void searchSingleFieldOrAnd() {
		
		//(FIND(Name, "Contractor") OR FIND(Name, "Blade")) AND FIND(Name, "Standard") on table Products
		
		System.out.println();
		System.out.println("Performing searchSingleFieldOrAnd()");
		System.out.println("(FIND(Name, \"Description\") OR FIND(Name, \"Blade\")) AND FIND(Name, \"Standard\") on table Products");
		System.out.println("Expected Hits: 11");
		System.out.println("=========================");
		
		ResultSetDefinition rsd = new ResultSetDefinition("Products");
		rsd.AddField("Name");
		
		Search search = new Search("Products");

		FreeFormTableParameter fftpNames = search.GetParameters().NewFreeFormTableParameter("Products");
		
		FreeFormParameterField ffpfName1 = fftpNames.GetFields().New("Name", FreeFormParameterField.SEARCH_OPERATOR_OR);
		ffpfName1.GetFreeForm().NewString("Description", FreeFormParameter.SubstringSearchType);
		ffpfName1.GetFreeForm().NewString("Blade", FreeFormParameter.SubstringSearchType);
		
		FreeFormParameterField ffpfName2 = fftpNames.GetFields().New("Name");
		ffpfName2.GetFreeForm().NewString("Standard", FreeFormParameter.SubstringSearchType);

		A2iResultSet result;
		try {
			result = catalog.GetResultSet(search, rsd, "Name", true, 0);

			System.out.println("Found " + result.GetRecordCount() + " records.");
//			for(int n=0; n<result.GetRecordCount(); n++) {
//				System.out.println(result.GetValueAt(n, "Name").GetStringValue());
//			}


		} catch (StringException e) {
			System.out.println("Search Failed!");
			e.printStackTrace();
		}
	}


	/**
	 * Search on two fields for single values, AND combination
	 */	
	public void searchTwoFieldsAnd() {
		
		//FIND(Manufacturer, "Estwing") AND FIND(Name, "Polyurethane") on table Products
		
		System.out.println();
		System.out.println("Performing searchTwoFieldsAnd()");
		System.out.println("FIND(Manufacturer, \"Estwing\") AND FIND(Name, \"Polyurethane\") on table Products");
		System.out.println("Expected Hits: 3");
		System.out.println("=========================");
		
		ResultSetDefinition rsd = new ResultSetDefinition("Products");
		rsd.AddField("Name");
		rsd.AddField("Manufacturer");
		
		Search search = new Search("Products");

		FreeFormTableParameter fftpProducts = search.GetParameters().NewFreeFormTableParameter("Products");
		
		FreeFormParameterField ffpfManu = fftpProducts.GetFields().New("Manufacturer");
		ffpfManu.GetFreeForm().NewString("Estwing", FreeFormParameter.SubstringSearchType);

		FreeFormParameterField ffpfName = fftpProducts.GetFields().New("Name");
		ffpfName.GetFreeForm().NewString("Polyurethane", FreeFormParameter.SubstringSearchType);
		
		A2iResultSet result;
		try {
			result = catalog.GetResultSet(search, rsd, "Name", true, 0);

			System.out.println("Found " + result.GetRecordCount() + " records.");
//			for(int n=0; n<result.GetRecordCount(); n++) {
//				System.out.println(result.GetValueAt(n, "Name").GetStringValue() + "\t\t" + result.GetValueAt(n, "Manufacturer").GetStringValue());
//			}


		} catch (StringException e) {
			System.out.println("Search Failed!");
			e.printStackTrace();
		}
	}

	/**
	 * Search on two fields for single values, OR combination
	 */	
	public void searchTwoFieldsOr() {
		
		//FIND(Manufacturer, "Estwing") OR FIND(Name, "Polyurethane") on table Products
		
		System.out.println();
		System.out.println("Performing searchTwoFieldsOr()");
		System.out.println("FIND(Manufacturer, \"Estwing\") OR FIND(Name, \"Polyurethane\") on table Products");
		System.out.println("Expected Hits: 67");
		System.out.println("=========================");
		
		ResultSetDefinition rsd = new ResultSetDefinition("Products");
		rsd.AddField("Name");
		rsd.AddField("Manufacturer");
		
		Search search = new Search("Products");

		search.SetSearchType(Search.GlobalOrSearchCombinationType);
		
		FreeFormTableParameter fftpProducts = search.GetParameters().NewFreeFormTableParameter("Products");
		
		FreeFormParameterField ffpfManu = fftpProducts.GetFields().New("Manufacturer");
		ffpfManu.GetFreeForm().NewString("Estwing", FreeFormParameter.SubstringSearchType);

		FreeFormParameterField ffpfName = fftpProducts.GetFields().New("Name");
		ffpfName.GetFreeForm().NewString("Polyurethane", FreeFormParameter.SubstringSearchType);
		
		search.SetSearchType(Search.GlobalOrSearchCombinationType);
		
		A2iResultSet result;
		try {
			result = catalog.GetResultSet(search, rsd, "Name", true, 0);

			System.out.println("Found " + result.GetRecordCount() + " records.");
//			for(int n=0; n<result.GetRecordCount(); n++) {
//				System.out.println(result.GetValueAt(n, "Name").GetStringValue() + "\t\t" + result.GetValueAt(n, "Manufacturer").GetStringValue());
//			}


		} catch (StringException e) {
			System.out.println("Search Failed!");
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		
		FreeFormSearchExamples ffse = new FreeFormSearchExamples();
		
		ffse.login();
		
		ffse.searchSingleFieldSimple();
		
//		ffse.searchSingleFieldAnd();
//		
//		ffse.searchSingleFieldOr();
//		
//		ffse.searchSingleFieldOrAnd();
//		
//		ffse.searchTwoFieldsAnd();
//
//		ffse.searchTwoFieldsOr();		
//		
		ffse.logout();
		
		
	}
}
