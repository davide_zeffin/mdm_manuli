import com.sap.aii.mapping.api.StreamTransformation;
import com.sap.aii.mapping.api.*;
import java.io.*;
import java.util.*;
import java.util.zip.*;
import java.util.Map;

/**
 * @author Administrator
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class MapPDF implements StreamTransformation {
	static final String ENTRYNAME = "ServeltShouldProcessThis.xml";
	static final int BUFFER = 1024 * 1000;
	String ourSourceFileName;
	private Map param;

	public void setParameter(Map map) {
		param = map;
		if (param == null) {
			param = new HashMap();
		}
	}
	public static void main(String args[]) {
		try {
			FileInputStream fin = new FileInputStream(args[0]);
			FileOutputStream fout = new FileOutputStream(args[1]);
			MapPDF mapping = new MapPDF();
			mapping.execute(fin, fout);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void execute(InputStream inputstream, OutputStream outputstream) {
		DynamicConfiguration conf =
			(DynamicConfiguration) param.get("DynamicConfiguration");
		DynamicConfigurationKey KEY_FILENAME =
			DynamicConfigurationKey.create(
				"http://sap.com/xi/XI/System/File",
				"FileName");
		ourSourceFileName = conf.get(KEY_FILENAME);
		int len = 0;
		byte buf[] = new byte[BUFFER];
		try {
			BufferedInputStream bis = new BufferedInputStream(inputstream);
			ZipInputStream zis = new ZipInputStream(bis);
			ZipEntry objZipEntry = null;
			bis.mark(0);
			//	  ZipInputStream zis = new ZipInputStream(inputstream);
			ZipOutputStream zos = new ZipOutputStream(outputstream);
			if ((objZipEntry = zis.getNextEntry()) != null) {
				bis.reset();
				while ((len = bis.read(buf)) > 0)
					outputstream.write(buf, 0, len);
			} else {
				//	  DOM, SAX or NORMAL PARSING POSSILBE before ZIPPING - NOT IMPLEMENTED
				bis.reset();
				if (ourSourceFileName != null) {
					objZipEntry = new ZipEntry(ourSourceFileName);
				} else {
					objZipEntry = new ZipEntry(ENTRYNAME);
				}
				zos.putNextEntry(objZipEntry);
				while ((len = bis.read(buf)) > 0) {
					zos.write(buf, 0, len);
				}
				zis.close();
				zos.close();
			}
		} catch (Exception e) {
			 //Write the error to a log file on Server Directory – Not Implemented}
		}

	}
}