/*
 * Created on Dec 11, 2007
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.comp;

import java.util.Vector;

/**
 * @author alexeii
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public interface IOVSCallBack {
	/* returns array of vectors: 
	 * 	(SIGN,OPTION,LOW,HIGH)
	 */
	public Vector[] addSelections(String node, String fld);
	/* returns array of vectors: 
	 * 	(IWDNodeElement,fieldName)
	 *  that is used to get additional data from help line selected
	 */
	public Vector[] applyResults(String node, String fld);
	/* returns vector of key-value pairs in Object[] array
	 * v = { [key,value], [key,value],...},
	 * where key is a name of input node attribute, value is a value that goes there
	 */
	public Vector applyInputValues();
}
