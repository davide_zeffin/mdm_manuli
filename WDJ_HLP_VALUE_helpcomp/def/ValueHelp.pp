<?xml version="1.0" encoding="UTF-8"?>
<public-part
		 xmlns="http://xml.sap.com/2002/11/PublicPart"
		 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		 xsi:schemaLocation="http://xml.sap.com/2002/11/PublicPart ppdef.xsd"
		 version="1.0.3"
		 xmlns:IDX="urn:sap.com:PublicPart:1.0">
	<name>ValueHelp</name>
	<purpose>compilation</purpose>
	<caption></caption>
	<entities>
		<entity>
			<name>ValueHelp</name>
			<package>com/sap/comp</package>
			<caption>ValueHelp</caption>
			<description>ValueHelp</description>
			<entity-type>Web Dynpro Component</entity-type>
			<entity-sub-type>Source</entity-sub-type>
		</entity>
		<entity>
			<name>IOVSCallBack</name>
			<package>com/sap/comp</package>
			<caption>IOVSCallBack</caption>
			<description>IOVSCallBack</description>
			<entity-type>Java Class</entity-type>
			<entity-sub-type>Class</entity-sub-type>
		</entity>
		<entity>
			<name>OVSCallbackAbstract</name>
			<package>com/sap/comp</package>
			<caption>OVSCallbackAbstract</caption>
			<description>OVSCallbackAbstract</description>
			<entity-type>Java Class</entity-type>
			<entity-sub-type>Class</entity-sub-type>
		</entity>
	</entities>
</public-part>