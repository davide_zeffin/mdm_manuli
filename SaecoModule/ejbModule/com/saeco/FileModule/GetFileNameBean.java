package com.saeco.FileModule;
// Classes for EJB
import javax.ejb.CreateException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
// Classes for Module development & Trace
//import com.sap.aii.af.mp.module.Module;
import com.sap.aii.af.mp.module.ModuleContext;
import com.sap.aii.af.mp.module.ModuleData;
import com.sap.aii.af.mp.module.ModuleException;
import com.sap.aii.af.ra.ms.api.Message;
import com.sap.aii.af.service.trace.Trace;
import java.util.Hashtable;
// XML parsing and transformation classes
import javax.xml.parsers.*;
import org.w3c.dom.*;
import java.io.InputStream;
import java.io.ByteArrayOutputStream;
import com.sap.aii.af.ra.ms.api.XMLPayload;
import javax.xml.transform.*;
import javax.xml.transform.Source;
import javax.xml.transform.Result;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

/**
 * @ejbLocal <{com.saeco.FileModule.GetFileNameLocal}>
 * @ejbLocalHome <{com.saeco.FileModule.GetFileNameLocalHome}>
 * @stateless 
 * @transactionType Container
 */
public class GetFileNameBean implements SessionBean {
	public static final String VERSION_ID =
		"$Id://tc/aii/30_REL/src/_adapters/_sample/java/com/sap/aii/af/sample/module/GetFileNameBean.java#1 $";

	private static final Trace TRACE = new Trace(VERSION_ID);
	private SessionContext myContext;

	public void ejbRemove() {
	}
	public void ejbActivate() {
	}
	public void ejbPassivate() {
	}
	public void setSessionContext(SessionContext context) {
		myContext = context;
	}
	public void ejbCreate() throws CreateException {
	}
	public ModuleData process(
		ModuleContext moduleContext,
		ModuleData inputModuleData)
		throws ModuleException {
		Object obj = null;
		Message msg = null;
		String fileName = null;
		try {
			obj = inputModuleData.getPrincipalData();
			msg = (Message) obj;
			Hashtable mp =
				(Hashtable) inputModuleData.getSupplementalData(
					"module.parameters");
			if (mp != null)
				fileName = (String) mp.get("FileName");
		} catch (Exception e) {
			ModuleException me = new ModuleException(e);
			throw me;
		}
		try {
			XMLPayload xmlpayload = msg.getDocument();
			DocumentBuilderFactory factory;
			factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document document =
				builder.parse((InputStream) xmlpayload.getInputStream());
			Element rootNode = document.getDocumentElement();
			if (rootNode != null) {
				Element childElement = document.createElement("FileName");
				childElement.appendChild(document.createTextNode(fileName));
				rootNode.appendChild(childElement);
			}
			// Transforming the DOM object to Stream object.
			TransformerFactory tfactory = TransformerFactory.newInstance();
			Transformer transformer = tfactory.newTransformer();
			Source src = new DOMSource(document);
			ByteArrayOutputStream myBytes = new ByteArrayOutputStream();
			Result dest = new StreamResult(myBytes);
			transformer.transform(src, dest);
			byte[] docContent = myBytes.toByteArray();
			if (docContent != null) {
				xmlpayload.setContent(docContent);
				inputModuleData.setPrincipalData(msg);
			}
		} catch (Exception e) {
			ModuleException me = new ModuleException(e);
			throw me;
		}
		return inputModuleData;
	}
}