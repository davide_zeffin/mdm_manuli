package com.saeco.FileModule;
import javax.ejb.EJBLocalHome;

import javax.ejb.CreateException;
public interface GetFileNameLocalHome extends EJBLocalHome {

	/**
	 * Create Method.
	 */
	public GetFileNameLocal create() throws CreateException;

}

