package dom.ihsr.jco_rfc_call;
 
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;

import com.sap.mw.jco.JCO;
import com.sap.security.core.server.destinations.api.DestinationService;
import com.sap.security.core.server.destinations.api.RFCDestination;
import com.sapportals.portal.prt.component.AbstractPortalComponent;
import com.sapportals.portal.prt.component.IPortalComponentRequest;
import com.sapportals.portal.prt.component.IPortalComponentResponse;

public class JCORFCCall extends AbstractPortalComponent
{
    public void doContent(IPortalComponentRequest request, IPortalComponentResponse response)
    {
    	try {
    	
		Context ctx = new InitialContext();
		DestinationService dstService = (DestinationService) ctx.lookup(DestinationService.JNDI_KEY);

		RFCDestination dst = (RFCDestination) dstService.getDestination("RFC", "SAP_ECC");
		Properties jcoProperties = dst.getJCoProperties();
		JCO.Client client = JCO.createClient(jcoProperties);

		JCO.Function functionCfgApiGetConfigInfo;
		JCO.Repository mRepository = new JCO.Repository("WASJCORep", client);
		functionCfgApiGetConfigInfo = mRepository.getFunctionTemplate("ZISH_GET_CWS_CONTEXT").getFunction();
		JCO.ParameterList im = functionCfgApiGetConfigInfo.getImportParameterList();
		JCO.ParameterList ex = functionCfgApiGetConfigInfo.getExportParameterList();
		JCO.ParameterList tbl = functionCfgApiGetConfigInfo.getTableParameterList();

		//No import parameters
		//im.setValue("X", "IGNORE_BUFFER");
		client.execute(functionCfgApiGetConfigInfo);



		//lista parametri
//		im.setValue(messageType, "MESSAGE_TYPE");
//		im.setValue("X", "IGNORE_BUFFER");
//		client.execute(functionCfgApiGetConfigInfo);

		// process export document
//		retVal = ex.getString("NUMBER");
			  //elaborazione export parameters

		String params = ex.getString("URL");
		params.concat("?applicationID=SAP");
		params.concat("&command=START_APPLICATION");
		params.concat("&clientID="+ex.getString("ADDRSTR"));
		params.concat("&Ward.Id="+ex.getString("REPARTO"));
		params.concat("&Patient.Id.MRN="+ex.getString("PATNR"));
		params.concat("&Encounter.Id.VisitNumber="+ex.getString("SERVICE"));
		params.concat("&Observation.Id.Placer_Order_Number="+ex.getString("ORDER_PLACER"));	
			  
		params = ex.getString("URL") + "applicationID=SAP&command=START_APPLICATION&clientID=" + ex.getString("ADDRSTR") +
		         "&Ward.Id=" + ex.getString("REPARTO") + "&Patient.Id.MRN=" + ex.getString("PATNR") + "&Encounter.Id.VisitNumber=" +
		         ex.getString("SERVICE") + "&Observation.Id.Placer_Order_Number=" + ex.getString("ORDER_PLACER");
			  
		client.disconnect();
		response.write(params);

	}
	catch(Exception e){
//		return null;
	}
//	return null;
    }
    }