/*
 * Created on 31.07.2007 by dbeisert
 * Copyright Holcim 2007
 */
package de.beisertbtc.wd.util.cmi;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import com.sap.tc.cmi.metadata.ICMIModelClassInfo;

/**
 * An implementation of a CMIBean that can dynamically
 * can change its Metadata (information about fields and types) and
 * stores its attribute values in a <code>java.util.Map</code>.
 * . The Metadata is
 * provided by the <code>CMIInfo.GenericClassInfo</code>.
 * Used by the OVSBuilder for the Input and the Result.
 * 
 * @author dbeisert
 * @see com.holcim.hgrs.tco.basic.ui.wd.ovs.OVSBuilder
 * @see CMIInfo.GenericClassInfo
 */
public class GenericCMIBean extends CMIBean{

	CMIInfo.GenericClassInfo _genericClassInfo = null;
	Map _propertyValues = new HashMap();
	/**
	 * Initialize the bean with a initialized <code>ModelClassInfo</code>.
	 * 
	 * @param pClassInfo
	 */
	public GenericCMIBean(CMIInfo.GenericClassInfo pClassInfo) {
		super();
		_genericClassInfo = pClassInfo;
	}
	
	/**
	 * Initialize the bean with the essential information.
	 * The <code>ModelClassInfo</code> is initialized but empty and should be
	 * modified later on by the client.<br>
	 * <p>
	 * Example:
	 * <pre>
	 * GenericCMIBean bean = new GenericCMIBean("MyBean",WDRessourceHandler.getCurrentSessionLocale());
	 * bean.getGenericClassInfo().addProperty("name","Name", String.class);
	 * ..
	 * </pre>
	 * 
	 * @param name
	 * @param loc
	 */
	public GenericCMIBean(String name, Locale loc) {
		super();
		_genericClassInfo = new CMIInfo.GenericClassInfo(name,loc);
	}

	public ICMIModelClassInfo associatedModelClassInfo() {
		return _genericClassInfo;
	}

	/**
	 * @return
	 */
	public CMIInfo.GenericClassInfo getGenericClassInfo() {
		return _genericClassInfo;
	}

	/**
	 * returns a generic value of an attribute.
	 * <p>
	 * It is <b>not</b> checked if the property is defined in the ModelClassInfo
	 * and that is intended. The reason is: to give clients more flexibility
	 * to add data to the bean that must not be displayed
	 * but needs to be kept in the context of the bean.<br>
	 * For Example the domain object that was retrieved by the
	 * DAO query could be stored in an attribute.
	 * 
	 * @param name  - the attribute name
	 * @return the attribute value
	 */
	public Object getAttributeValue(String name) {
//		if(_genericClassInfo.getPropertyInfo(name)==null)
//			throw new IllegalArgumentException("No attribute with name " + name + " is defined");
		return _propertyValues.get(name);
	}

	/**
	 * sets a generic value of an attribute.
	 * <p>
	 * It is <b>not</b> checked if the property is defined in the ModelClassInfo
	 * and that is intended. The reason is: to give clients more flexibility
	 * to add data to the bean that must not be displayed
	 * but needs to be kept in the context of the bean.<br>
	 * For Example the domain object that was retrieved by the
	 * DAO query could be stored in an attribute.
	 * 
	 * @param name  - the attribute name
	 * @param value - the value
	 */
	public void setAttributeValue(String name, Object value) {
//		if(_genericClassInfo.getPropertyInfo(name)==null)
//			throw new IllegalArgumentException("No attribute with name " + name + " is defined.");
		
		_propertyValues.put(name, value);
	}
	
	

}
