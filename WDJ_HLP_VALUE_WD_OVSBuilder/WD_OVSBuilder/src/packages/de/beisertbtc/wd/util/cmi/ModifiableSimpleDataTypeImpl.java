/*
 * Created on 20.08.2007 by dbeisert
 * Copyright Holcim 2007
 */
package de.beisertbtc.wd.util.cmi;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import com.sap.dictionary.runtime.DdCheckException;
import com.sap.dictionary.runtime.IDataProvider;
import com.sap.dictionary.runtime.ISimpleTypeModifiable;
import com.sap.dictionary.types.services.IBackendConversionRule;
import com.sap.typeservices.ISimpleValueServices;
import com.sap.typeservices.ITextServices;

/**
 * Workaround class that overrides getQualifiedName() of
 * the datatype to distinguish a datatype from another
 * when using SVServices. It is the result of trial and
 * error until the OVS really worked. The reason was
 * that we have country and siteType in the OVS that have both
 * ValueSets. Somehow the OVS always assigned the same ValueSet
 * to both inputfields. The reason is probably that
 * the OVS caches the ValueSet in a map with the qualified name
 * of the datatype
 * as key.
 * Used by DataTypes.createDataType().
 * 
 * 
 * @author dbeisert
 */
public class ModifiableSimpleDataTypeImpl implements ISimpleTypeModifiable{

			ISimpleTypeModifiable _dt = null;
		String _name = null;
		
		public ModifiableSimpleDataTypeImpl(ISimpleTypeModifiable dt, String name){
			_dt = dt;
			_name = name;
		}
		/**
		 * @param arg0
		 * @throws com.sap.dictionary.runtime.DdCheckException
		 */
		public void checkValid(Object arg0) throws DdCheckException {
			_dt.checkValid(arg0);
		}

		/**
		 * @return
		 */
		public ISimpleTypeModifiable cloneType() {
			return _dt.cloneType();
		}

		/**
		 * @param arg0
		 * @return
		 */
		public ISimpleTypeModifiable cloneType(Locale arg0) {
			return _dt.cloneType(arg0);
		}

//		public boolean equals(Object obj) {
//			return _dt.equals(obj);
//		}

		/**
		 * @param arg0
		 * @return
		 */
		public String format(boolean arg0) {
			return _dt.format(arg0);
		}

		/**
		 * @param arg0
		 * @return
		 */
		public String format(byte arg0) {
			return _dt.format(arg0);
		}

		/**
		 * @param arg0
		 * @return
		 */
		public String format(double arg0) {
			return _dt.format(arg0);
		}

		/**
		 * @param arg0
		 * @return
		 */
		public String format(float arg0) {
			return _dt.format(arg0);
		}

		/**
		 * @param arg0
		 * @return
		 */
		public String format(int arg0) {
			return _dt.format(arg0);
		}

		/**
		 * @param arg0
		 * @return
		 */
		public String format(Boolean arg0) {
			return _dt.format(arg0);
		}

		/**
		 * @param arg0
		 * @return
		 */
		public String format(Byte arg0) {
			return _dt.format(arg0);
		}

		/**
		 * @param arg0
		 * @return
		 */
		public String format(Double arg0) {
			return _dt.format(arg0);
		}

		/**
		 * @param arg0
		 * @return
		 */
		public String format(Float arg0) {
			return _dt.format(arg0);
		}

		/**
		 * @param arg0
		 * @return
		 */
		public String format(Integer arg0) {
			return _dt.format(arg0);
		}

		/**
		 * @param arg0
		 * @return
		 */
		public String format(Long arg0) {
			return _dt.format(arg0);
		}

		/**
		 * @param arg0
		 * @return
		 */
		public String format(Object arg0) {
			return _dt.format(arg0);
		}

		/**
		 * @param arg0
		 * @return
		 */
		public String format(Short arg0) {
			return _dt.format(arg0);
		}

		/**
		 * @param arg0
		 * @return
		 */
		public String format(String arg0) {
			return _dt.format(arg0);
		}

		/**
		 * @param arg0
		 * @return
		 */
		public String format(BigDecimal arg0) {
			return _dt.format(arg0);
		}

		/**
		 * @param arg0
		 * @return
		 */
		public String format(BigInteger arg0) {
			return _dt.format(arg0);
		}

		/**
		 * @param arg0
		 * @return
		 */
		public String format(java.sql.Date arg0) {
			return _dt.format(arg0);
		}

		/**
		 * @param arg0
		 * @return
		 */
		public String format(Time arg0) {
			return _dt.format(arg0);
		}

		/**
		 * @param arg0
		 * @return
		 */
		public String format(Timestamp arg0) {
			return _dt.format(arg0);
		}

		/**
		 * @param arg0
		 * @return
		 */
		public String format(Date arg0) {
			return _dt.format(arg0);
		}

		/**
		 * @param arg0
		 * @return
		 */
		public String format(long arg0) {
			return _dt.format(arg0);
		}

		/**
		 * @param arg0
		 * @return
		 */
		public String format(short arg0) {
			return _dt.format(arg0);
		}

		/**
		 * @return
		 */
		public String formatOfDefaultValue() {
			return _dt.formatOfDefaultValue();
		}

		/**
		 * @return
		 */
		public Class getAssociatedClass() {
			return _dt.getAssociatedClass();
		}

		/**
		 * @return
		 */
		public Class getAttributeClass() {
			return _dt.getAttributeClass();
		}

		/**
		 * @return
		 */
		public IBackendConversionRule getBackendConversionRule() {
			return _dt.getBackendConversionRule();
		}

		/**
		 * @return
		 */
		public String getBackendName() {
			return _dt.getBackendName();
		}

		/**
		 * @return
		 */
		public String getBuiltInType() {
			return _dt.getBuiltInType();
		}

		/**
		 * @return
		 */
		public Object getBuiltInTypeEnum() {
			return _dt.getBuiltInTypeEnum();
		}

		/**
		 * @return
		 */
		public String getColumnLabel() {
			return _dt.getColumnLabel();
		}

		/**
		 * @return
		 */
		public Object getDefaultValue() {
			return _dt.getDefaultValue();
		}

		/**
		 * @return
		 */
		public String getDescription() {
			return _dt.getDescription();
		}

		/**
		 * @return
		 */
		public Set getEnumeration() {
			return _dt.getEnumeration();
		}

		/**
		 * @return
		 */
		public Map getEnumerationTexts() {
			return _dt.getEnumerationTexts();
		}

		/**
		 * @return
		 */
		public String getFieldLabel() {
			return _dt.getFieldLabel();
		}

		/**
		 * @return
		 */
		public String getFormat() {
			return _dt.getFormat();
		}

		/**
		 * @return
		 */
		public String getFormatTemplate() {
			return _dt.getFormatTemplate();
		}

		/**
		 * @return
		 */
		public int getFractionDigits() {
			return _dt.getFractionDigits();
		}

		/**
		 * @return
		 */
		public int getLength() {
			return _dt.getLength();
		}

		/**
		 * @return
		 */
		public Locale getLocale() {
			return _dt.getLocale();
		}

		/**
		 * @return
		 */
		public String getLocalName() {
			return _dt.getLocalName();
		}

		/**
		 * @return
		 */
		public Object getMaxExclusive() {
			return _dt.getMaxExclusive();
		}

		/**
		 * @return
		 */
		public int getMaxExternalLength() {
			return _dt.getMaxExternalLength();
		}

		/**
		 * @return
		 */
		public Object getMaxInclusive() {
			return _dt.getMaxInclusive();
		}

		/**
		 * @return
		 */
		public int getMaxLength() {
			return _dt.getMaxLength();
		}

		/**
		 * @return
		 */
		public Object getMinExclusive() {
			return _dt.getMinExclusive();
		}

		/**
		 * @return
		 */
		public Object getMinInclusive() {
			return _dt.getMinInclusive();
		}

		/**
		 * @return
		 */
		public int getMinLength() {
			return _dt.getMinLength();
		}

		/**
		 * @return
		 */
		public String getName() {
			return _name;
			//return _dt.getName();
		}

		/**
		 * @return
		 */
		public String getPattern() {
			return _dt.getPattern();
		}

		/**
		 * @return
		 */
		public String getPrefix() {
			return _dt.getPrefix();
		}

		/**
		 * @return
		 */
		public IDataProvider getProvider() {
			return _dt.getProvider();
		}

		/**
		 * @return
		 */
		public String getQualifiedName() {
			return _name;
		}

		/**
		 * @return
		 */
		public String getQuickInfo() {
			return _dt.getQuickInfo();
		}

		/**
		 * @return
		 */
		public boolean getReadOnly() {
			return _dt.getReadOnly();
		}

		/**
		 * @return
		 */
		public ISimpleValueServices getSVServices() {
			return _dt.getSVServices();
		}

		/**
		 * @return
		 */
		public String getTextLocale() {
			return _dt.getTextLocale();
		}

		/**
		 * @return
		 */
		public ITextServices getTextServices() {
			return _dt.getTextServices();
		}

		/**
		 * @return
		 */
		public int getTotalDigits() {
			return _dt.getTotalDigits();
		}

		/**
		 * @return
		 */
		public boolean getTranslateFlag() {
			return _dt.getTranslateFlag();
		}

		/**
		 * @return
		 */
		public boolean getUppercase() {
			return _dt.getUppercase();
		}

//		public int hashCode() {
//			return _dt.hashCode();
//		}

		/**
		 * @return
		 */
		public boolean hasSVService() {
			return _dt.hasSVService();
		}

		/**
		 * @return
		 */
		public boolean isModifyAllowed() {
			return _dt.isModifyAllowed();
		}

		/**
		 * @return
		 */
		public boolean isNumeric() {
			return _dt.isNumeric();
		}

		/**
		 * @return
		 */
		public boolean isSimpleType() {
			return _dt.isSimpleType();
		}

		/**
		 * @return
		 */
		public boolean isStructure() {
			return _dt.isStructure();
		}

		/**
		 * @param arg0
		 * @return
		 */
		public boolean isValid(boolean arg0) {
			return _dt.isValid(arg0);
		}

		/**
		 * @param arg0
		 * @return
		 */
		public boolean isValid(double arg0) {
			return _dt.isValid(arg0);
		}

		/**
		 * @param arg0
		 * @return
		 */
		public boolean isValid(int arg0) {
			return _dt.isValid(arg0);
		}

		/**
		 * @param arg0
		 * @return
		 */
		public boolean isValid(Boolean arg0) {
			return _dt.isValid(arg0);
		}

		/**
		 * @param arg0
		 * @return
		 */
		public boolean isValid(Double arg0) {
			return _dt.isValid(arg0);
		}

		/**
		 * @param arg0
		 * @return
		 */
		public boolean isValid(Integer arg0) {
			return _dt.isValid(arg0);
		}

		/**
		 * @param arg0
		 * @return
		 */
		public boolean isValid(Object arg0) {
			return _dt.isValid(arg0);
		}

		/**
		 * @param arg0
		 * @return
		 */
		public boolean isValid(String arg0) {
			return _dt.isValid(arg0);
		}

		/**
		 * @param arg0
		 * @return
		 */
		public boolean isValid(BigDecimal arg0) {
			return _dt.isValid(arg0);
		}

		/**
		 * @param arg0
		 * @return
		 */
		public boolean isValid(java.sql.Date arg0) {
			return _dt.isValid(arg0);
		}

		/**
		 * @param arg0
		 * @return
		 */
		public boolean isValid(Time arg0) {
			return _dt.isValid(arg0);
		}

		/**
		 * @param arg0
		 * @return
		 * @throws java.text.ParseException
		 */
		public Object parse(String arg0) throws ParseException {
			return _dt.parse(arg0);
		}

		/**
		 * @param arg0
		 * @return
		 * @throws java.text.ParseException
		 */
		public BigInteger parseBigInteger(String arg0) throws ParseException {
			return _dt.parseBigInteger(arg0);
		}

		/**
		 * @param arg0
		 * @return
		 * @throws java.text.ParseException
		 */
		public boolean parseBoolean(String arg0) throws ParseException {
			return _dt.parseBoolean(arg0);
		}

		/**
		 * @param arg0
		 * @return
		 * @throws java.text.ParseException
		 */
		public Boolean parseBooleanObject(String arg0) throws ParseException {
			return _dt.parseBooleanObject(arg0);
		}

		/**
		 * @param arg0
		 * @return
		 * @throws java.text.ParseException
		 */
		public byte parseByte(String arg0) throws ParseException {
			return _dt.parseByte(arg0);
		}

		/**
		 * @param arg0
		 * @return
		 * @throws java.text.ParseException
		 */
		public Byte parseByteObject(String arg0) throws ParseException {
			return _dt.parseByteObject(arg0);
		}

		/**
		 * @param arg0
		 * @return
		 * @throws java.text.ParseException
		 */
		public java.sql.Date parseDate(String arg0) throws ParseException {
			return _dt.parseDate(arg0);
		}

		/**
		 * @param arg0
		 * @return
		 * @throws java.text.ParseException
		 */
		public BigDecimal parseDecimal(String arg0) throws ParseException {
			return _dt.parseDecimal(arg0);
		}

		/**
		 * @param arg0
		 * @return
		 * @throws java.text.ParseException
		 */
		public double parseDouble(String arg0) throws ParseException {
			return _dt.parseDouble(arg0);
		}

		/**
		 * @param arg0
		 * @return
		 * @throws java.text.ParseException
		 */
		public Double parseDoubleObject(String arg0) throws ParseException {
			return _dt.parseDoubleObject(arg0);
		}

		/**
		 * @param arg0
		 * @return
		 * @throws java.text.ParseException
		 */
		public float parseFloat(String arg0) throws ParseException {
			return _dt.parseFloat(arg0);
		}

		/**
		 * @param arg0
		 * @return
		 * @throws java.text.ParseException
		 */
		public Float parseFloatObject(String arg0) throws ParseException {
			return _dt.parseFloatObject(arg0);
		}

		/**
		 * @param arg0
		 * @return
		 * @throws java.text.ParseException
		 */
		public int parseInt(String arg0) throws ParseException {
			return _dt.parseInt(arg0);
		}

		/**
		 * @param arg0
		 * @return
		 * @throws java.text.ParseException
		 */
		public Integer parseInteger(String arg0) throws ParseException {
			return _dt.parseInteger(arg0);
		}

		/**
		 * @param arg0
		 * @return
		 * @throws java.text.ParseException
		 */
		public long parseLong(String arg0) throws ParseException {
			return _dt.parseLong(arg0);
		}

		/**
		 * @param arg0
		 * @return
		 * @throws java.text.ParseException
		 */
		public Long parseLongObject(String arg0) throws ParseException {
			return _dt.parseLongObject(arg0);
		}

		/**
		 * @param arg0
		 * @return
		 * @throws java.text.ParseException
		 */
		public short parseShort(String arg0) throws ParseException {
			return _dt.parseShort(arg0);
		}

		/**
		 * @param arg0
		 * @return
		 * @throws java.text.ParseException
		 */
		public Short parseShortObject(String arg0) throws ParseException {
			return _dt.parseShortObject(arg0);
		}

		/**
		 * @param arg0
		 * @return
		 * @throws java.text.ParseException
		 */
		public String parseString(String arg0) throws ParseException {
			return _dt.parseString(arg0);
		}

		/**
		 * @param arg0
		 * @return
		 * @throws java.text.ParseException
		 */
		public Time parseTime(String arg0) throws ParseException {
			return _dt.parseTime(arg0);
		}

		/**
		 * @param arg0
		 * @return
		 * @throws java.text.ParseException
		 */
		public Timestamp parseTimestamp(String arg0) throws ParseException {
			return _dt.parseTimestamp(arg0);
		}

		/**
		 * @param arg0
		 * @return
		 * @throws java.text.ParseException
		 */
		public Date parseUtilDate(String arg0) throws ParseException {
			return _dt.parseUtilDate(arg0);
		}

		/**
		 * @return
		 */
		public int resolveLength() {
			return _dt.resolveLength();
		}

		/**
		 * @param arg0
		 */
		public void setColumnLabel(String arg0) {
			_dt.setColumnLabel(arg0);
		}

		/**
		 * @param arg0
		 */
		public void setDefaultValue(Object arg0) {
			_dt.setDefaultValue(arg0);
		}

		/**
		 * @param arg0
		 */
		public void setDescription(String arg0) {
			_dt.setDescription(arg0);
		}

		/**
		 * @param arg0
		 */
		public void setEnumeration(Set arg0) {
			_dt.setEnumeration(arg0);
		}

		/**
		 * @param arg0
		 */
		public void setEnumerationTexts(Map arg0) {
			_dt.setEnumerationTexts(arg0);
		}

		/**
		 * @param arg0
		 */
		public void setFieldLabel(String arg0) {
			_dt.setFieldLabel(arg0);
		}

		/**
		 * @param arg0
		 */
		public void setFormat(String arg0) {
			_dt.setFormat(arg0);
		}

		/**
		 * @param arg0
		 */
		public void setFractionDigits(int arg0) {
			_dt.setFractionDigits(arg0);
		}

		/**
		 * @param arg0
		 */
		public void setLength(int arg0) {
			_dt.setLength(arg0);
		}

		/**
		 * @param arg0
		 */
		public void setLocale(Locale arg0) {
			_dt.setLocale(arg0);
		}

		/**
		 * @param arg0
		 */
		public void setMaxExclusive(Object arg0) {
			_dt.setMaxExclusive(arg0);
		}

		/**
		 * @param arg0
		 */
		public void setMaxExternalLength(int arg0) {
			_dt.setMaxExternalLength(arg0);
		}

		/**
		 * @param arg0
		 */
		public void setMaxInclusive(Object arg0) {
			_dt.setMaxInclusive(arg0);
		}

		/**
		 * @param arg0
		 */
		public void setMaxLength(int arg0) {
			_dt.setMaxLength(arg0);
		}

		/**
		 * @param arg0
		 */
		public void setMinExclusive(Object arg0) {
			_dt.setMinExclusive(arg0);
		}

		/**
		 * @param arg0
		 */
		public void setMinInclusive(Object arg0) {
			_dt.setMinInclusive(arg0);
		}

		/**
		 * @param arg0
		 */
		public void setMinLength(int arg0) {
			_dt.setMinLength(arg0);
		}

		/**
		 * @param arg0
		 */
		public void setPattern(String arg0) {
			_dt.setPattern(arg0);
		}

		/**
		 * @param arg0
		 */
		public void setQuickInfo(String arg0) {
			_dt.setQuickInfo(arg0);
		}

		/**
		 * @param arg0
		 */
		public void setReadOnly(boolean arg0) {
			_dt.setReadOnly(arg0);
		}

		/**
		 * @param arg0
		 */
		public void setSVServices(ISimpleValueServices arg0) {
			_dt.setSVServices(arg0);
		}

		/**
		 * @param arg0
		 */
		public void setTextLocale(String arg0) {
			_dt.setTextLocale(arg0);
		}

		/**
		 * @param arg0
		 */
		public void setTextServices(ITextServices arg0) {
			_dt.setTextServices(arg0);
		}

		/**
		 * @param arg0
		 */
		public void setTotalDigits(int arg0) {
			_dt.setTotalDigits(arg0);
		}

		/**
		 * @param arg0
		 */
		public void setUppercase(boolean arg0) {
			_dt.setUppercase(arg0);
		}

		public String toString() {
			return _dt.toString();
		}

		/**
		 * @param arg0
		 * @return
		 */
		public String toString(Object arg0) {
			return _dt.toString(arg0);
		}

		/**
		 * @param arg0
		 * @return
		 * @throws java.text.ParseException
		 */
		public Object valueOf(String arg0) throws ParseException {
			return _dt.valueOf(arg0);
		}

}
