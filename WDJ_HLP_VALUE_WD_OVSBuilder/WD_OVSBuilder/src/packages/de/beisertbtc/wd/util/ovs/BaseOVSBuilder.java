/*
 * Created on 12.10.2007 by dbeisert
 * Copyright Holcim 2007
 */
package de.beisertbtc.wd.util.ovs;

import java.util.Locale;

/**
 * This class is meant to be subclassed. It combines the
 * IOVSBehaviour and the OVSBuilder in one class. If you want to separate this
 * you must subclass OVSBuilder and implements IOVSBehaviour yourself.
 * 
 * @author dbeisert
 */
public abstract class BaseOVSBuilder extends OVSBuilder implements IOVSBehaviour{

	

	/**
	 * Default constructor that is needed.
	 * @param pName the unique identifier, this can be free text as you like. When more than one OVS is added to an Attribute this serves as key.
	 * @param loc The current Locale of the logged on user. Use <code>WDResourceHandler.getCurrentSessionLocale()</code>
	 */
	public BaseOVSBuilder(String pName, Locale loc) {
		super(pName, loc);
		setOvsBehaviour(this);
	}

}
