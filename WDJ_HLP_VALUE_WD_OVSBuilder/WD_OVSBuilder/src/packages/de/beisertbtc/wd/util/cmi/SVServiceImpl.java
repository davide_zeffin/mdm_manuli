/*
 * Created on 14.08.2007 by dbeisert
 * Copyright Holcim 2007
 */
package de.beisertbtc.wd.util.cmi;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import com.sap.typeservices.IModifiableSimpleValueSet;
import com.sap.typeservices.ISimpleValueServices;
import com.sap.typeservices.ISimpleValueSet;
import com.sap.typeservices.SimpleValueSetException;

/**
 * A simple implementation of a Simple ValueService.
 * It is needed in the CMI layer because when cloning
 * or creating Datatypes they are first without ValueServices.
 * This serves as a simple implementation and is used by the DictionaryTypes.getModifiableType
 * 
 * 
 * @author dbeisert
 */
public class SVServiceImpl implements ISimpleValueServices{

	private ModifiableSimpleValueSet valueSet = new ModifiableSimpleValueSet();
	private Locale locale = null;
	public SVServiceImpl(Locale loc){
		locale = loc;
	}
	public ISimpleValueSet getValues(Locale arg0) {
		return valueSet;
	}

	public ISimpleValueSet getValues() {
		return valueSet;
	}

	public boolean validate(String key) {
		return validate((Object)key);
	}

	public boolean validate(Object key) {
		try {
			valueSet.checkValue(key);
			return true;
		} catch (SimpleValueSetException e) {
			return false;
		}
		
	}

	public Locale getLocale() {
		return locale;
	}

	public IModifiableSimpleValueSet getModifiableSimpleValueSet() {
		return valueSet;
	}
	
	
	public class ModifiableSimpleValueSet implements IModifiableSimpleValueSet{

		LinkedHashMap _map = new LinkedHashMap();
		public void setLocale(Locale arg0) {
			locale = arg0;
		}

		public String put(Object arg0, String arg1) {
			return (String)_map.put(arg0,arg1);
		}

		public String put(Object arg0, String arg1, Locale arg2) {
			return put(arg0,arg1);
		}

		public String removeKey(Object key) {
			return (String)_map.remove(key);
		}

		public void clear() {
			_map.clear();
		}

		public void sort(boolean arg0, boolean arg1, boolean arg2) {
		
		}

		public void sort(boolean arg0, boolean arg1, boolean arg2, Locale arg3) {
		
		}

		public void sort(Comparator arg0) {
		
		}

		public Locale getLocale() {
			return locale;
		}

		public int size() {
			return _map.size();
		}

		public Set entrySet() {
			return _map.entrySet();
		}

		public Set keySet() {
			return _map.keySet();
		}

		public Collection texts() {
			return _map.values();
		}

		public Collection texts(Locale arg0) {
			return texts();
		}

		public String getText(Object arg0) {
			return (String)_map.get(arg0);
		}

		public String getText(Object arg0, Locale arg1) {
			return getText(arg0);
		}

		public Object getKey(String value) {
			Set keys = keySet();
			for (Iterator iter = keys.iterator(); iter.hasNext();) {
				Object key = iter.next();
				Object val = _map.get(key);
				if(val.equals(value))return key;
			
			}
			return null;
		}

		public Object getKey(String val, Locale arg1) {
			return getKey(val);
		}

		public String getText(int i) {
			Object key = getKey(i);
			return getText(key);
		}

		public String getText(int i, Locale arg1) {
			return getText(i);
		}

		public Object getKey(int i) {
			List array = new ArrayList(keySet());
			return array.get(i);
		
		}

		public boolean containsText(String value) {
			return _map.containsValue(value);
		}

		public boolean containsText(String value, Locale arg1) {
			return containsText(value);
		}

		public boolean containsKey(Object key) {
			return _map.containsKey(key);
		}

		public boolean isEmpty() {
			return _map.isEmpty();
		}

		public void checkValue(Object key) throws SimpleValueSetException {
			if(!_map.containsKey(key)) throw new SimpleValueSetException(key + " is not contained in valueset" );
		}

		public boolean hasText(Object value, Locale arg1) {
			return containsText((String)value);
		}

	}
	/**
	 * Initialize the ValueSet with the values of an existing valueset.
	 * Used by the DictionaryTypes.getModifiableType.
	 * 
	 * @param svservices
	 */
	public void populate(ISimpleValueServices svservices) {
		IModifiableSimpleValueSet svs = svservices.getModifiableSimpleValueSet();
		Set keys = svs.keySet();
		for (Iterator iter = keys.iterator(); iter.hasNext();) {
			Object key = (Object) iter.next();
			valueSet.put(key,svs.getText(key,locale));
		}
	}
	/**
	 * Initialize the ValueSet with the values of a map.
	 *  
	 * @param map
	 */
	public void populate(Map map) {
		
		Set keys = map.keySet();
		for (Iterator iter = keys.iterator(); iter.hasNext();) {
			Object key = (Object) iter.next();
			valueSet.put(key, (String)map.get(key));
		}
	}

}
