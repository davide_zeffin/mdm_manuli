/*
 * Created on 09.06.2005
 *
 */
package de.beisertbtc.wd.util.cmi;

import java.util.Locale;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.LinkedHashMap;
import java.util.Collections;
import java.util.Set;
import java.util.SortedMap;

import com.sap.dictionary.runtime.IDataType;
import com.sap.dictionary.runtime.ISimpleType;
import com.sap.dictionary.runtime.ISimpleTypeModifiable;
import com.sap.dictionary.runtime.IStructure;

import com.sap.tc.cmi.metadata.CMISetting;
import com.sap.tc.cmi.metadata.CMISettingDefinition;

import com.sap.tc.cmi.metadata.ICMIAbstractInfo;
import com.sap.tc.cmi.metadata.ICMIModelClassInfo;
import com.sap.tc.cmi.metadata.ICMIModelClassPropertyInfo;
import com.sap.tc.cmi.metadata.ICMIModelInfo;
import com.sap.tc.cmi.metadata.ICMIModelObjectCollectionInfo;
import com.sap.tc.cmi.metadata.ICMIRelationRoleInfo;

import com.sap.tc.webdynpro.progmodel.api.IWDAttributeInfo;
import com.sap.tc.webdynpro.progmodel.api.IWDNodeInfo;
import com.sap.typeservices.IModifiableSimpleValueSet;
import com.sap.typeservices.ISimpleValueServices;

/**
 * Contains Implementation of <code>ICMIModelClassInfo</code> as Inner Classes.
 * It acts as a namespace for the different implementations but the class itself has
 * no functionality only its inner classes.
 * 
 * @author dbeisert
 */
public class CMIInfo 
{
	private CMIInfo() {}
	
	/**
	 * 
	 * Implementation of <code>ICMIModelClassInfo</code>  that can be modified
	 * very simply by using the <code>property(..)</code> method.
	 * 
	 * @author dbeisert
	 */
	public static class GenericClassInfo extends AbstractClassInfo
	{	
		public GenericClassInfo(final String name, final Locale locale) 
		{ 
			super(name,locale);
		} 
		
		public GenericClassInfo clone(final Locale locale)
		{
			final GenericClassInfo result = new GenericClassInfo( getName(), _locale );
			for (final Iterator i = iteratePropertyInfos(); i.hasNext(); )
			{
				final ICMIModelClassPropertyInfo prop = (ICMIModelClassPropertyInfo)i.next();
				final IDataType type = prop.getDataType();
				result.addProperty( prop.getName(), type.getQualifiedName(), prop.isReadOnly() );
			}
			return result;
		}
	}		
	
	/**
	 * 
	 * Implementation of <code>ICMIModelClassInfo</code> that creates the property information
	 * based on attributes of the <code>IWDNode</code> passed to it.
	 * 
	 * @author dbeisert
	 */
	public static class ClassInfoByPrototype extends AbstractClassInfo
	{
		
		public ClassInfoByPrototype(final String name, final Locale locale , final IWDNodeInfo prototype)
		{	
			super(name,locale);
			for (final Iterator i = prototype.iterateAttributes(); i.hasNext(); )
			{
				final IWDAttributeInfo attr = (IWDAttributeInfo)i.next();
				addProperty( attr.getName(), attr.getModifiableSimpleType(), attr.isReadOnly() );
			}
		}
	}
	
	/**
	 * 
	 * Base class for most implementations of <code>ICMIModelClassInfo</code> 
	 * in <code>CMIInfo</code>.
	 * 
	 * @author dbeisert
	 */
	public static abstract class AbstractClassInfo extends BasicInfo implements ICMIModelClassInfo
	{	
		final private Map _properties = new LinkedHashMap();
				
		protected AbstractClassInfo(final String name, final Locale loc) 
		{ 
			super(name);
			_locale = loc;
		}
		
		final public void addProperty(final String name, final ISimpleTypeModifiable type, final boolean isReadOnly)
		{
			_properties.put
			( 
				name,
				new BasicPropertyInfo
				(
					name,
					type,
					isReadOnly
				)
			);
		}
				
		public ICMIModelInfo getModelInfo() { return null; }

		public Collection getPropertyInfos() 
			{ return Collections.unmodifiableCollection( _properties.values() ); }

		public Iterator iteratePropertyInfos() 
			{ return getPropertyInfos().iterator(); }

		public ICMIModelClassPropertyInfo getPropertyInfo(final String name) 
			{ return (ICMIModelClassPropertyInfo)_properties.get( name ); }

		public IStructure getStructureType() { return null; }
  
		public Collection getSourceRoleInfos() { return Collections.EMPTY_LIST; }
		public Iterator iterateSourceRoleInfos() { return Collections.EMPTY_LIST.iterator(); }
		public ICMIRelationRoleInfo getTargetRoleInfo(final String targetRoleName) { return null; }
		public ICMIModelClassInfo getRelatedModelClassInfo(final String targetRoleName) { return null; }
		
		public boolean isGeneric() { return true; }
		
		class BasicPropertyInfo extends BasicInfo implements ICMIModelClassPropertyInfo
		{
			final private ISimpleTypeModifiable _dataType;
			final private boolean   _isReadOnly;
			
			BasicPropertyInfo(final String name, final ISimpleTypeModifiable type, final boolean isReadOnly) 
			{ 
				super(name); 
				_dataType   = type;
				_isReadOnly = isReadOnly;
			}

			public ICMIModelClassInfo getModelClassInfo() { return AbstractClassInfo.this; }
			public IDataType getDataType() { return _dataType; }
			public boolean isReadOnly() { return _isReadOnly; }
		}

		protected final Locale _locale;

		public final void addProperty(final String name, final String type, final boolean isReadOnly) {
			final ISimpleTypeModifiable dataType = DictionaryTypes.createModifiableDataType
			( 
				name, name, type, _locale, this.getClass().getClassLoader(), null 
			);
			this.addProperty( name, dataType, isReadOnly );
		}

		public final void addProperty(final String name, String label, final Class type, final boolean isReadOnly) {
			String typeName = DictionaryTypes.getTypeName(type);
			final ISimpleTypeModifiable dataType = DictionaryTypes.createModifiableDataType
			( 
				name,label, typeName, _locale, this.getClass().getClassLoader(),null
			);
			dataType.setFieldLabel(label);
			dataType.setColumnLabel(label);
			this.addProperty( name, dataType, isReadOnly );
		}
		
		/**
		 * add a property information including the length attribute of the datatype
		 * @param name
		 * @param label
		 * @param type
		 * @param length
		 */
		public final void addProperty(final String name, String label, final Class type, final int length) {
			String typeName = DictionaryTypes.getTypeName(type);
			final ISimpleTypeModifiable dataType = DictionaryTypes.createModifiableDataType
			( 
				name,label,typeName, _locale, this.getClass().getClassLoader(),null 
			);
			dataType.setFieldLabel(label);
			dataType.setColumnLabel(label);
			dataType.setLength(length);
			this.addProperty( name, dataType, true );
		}
		
		/**
		 * Convenience method to add a property with valueset.
		 * 
		 * @param name
		 * @param label
		 * @param type
		 * @param mapWithValues - key=Integer,String etc., Value=String(Label)
		 */
		public final void addProperty(final String name, String label, final Class type, Map mapWithValues) {
			String typeName = DictionaryTypes.getTypeName(type);
			final ISimpleTypeModifiable dataType = DictionaryTypes.createModifiableDataType
			( 
				typeName, label,type, _locale, this.getClass().getClassLoader(), mapWithValues 
			);
			dataType.setColumnLabel(label);
			
			//create SV Services on demand
			ISimpleValueServices svservices = dataType.getSVServices();
			if(svservices == null){
				svservices = new SVServiceImpl(_locale);
				dataType.setSVServices(svservices);
			}
			IModifiableSimpleValueSet svs = svservices.getModifiableSimpleValueSet();
			
			Set keys = mapWithValues.keySet();
			for (Iterator iter = keys.iterator(); iter.hasNext();) {
				Object key =  iter.next();
				String val = (String)mapWithValues.get(key);
				svs.put(key,val);
				
			}
			
			this.addProperty( name, dataType, false );
		}
		
		public ISimpleTypeModifiable getModifiableDataTypeOfProperty(String propertyName){
			BasicPropertyInfo prop = (BasicPropertyInfo)_properties.get(propertyName);
			return prop._dataType;
		}
		
		public Object removeProperty(String name){
			return _properties.remove(name);
		}

		public final void addProperty(final String name, String label, final Class type) {
			addProperty(name,label,type, false);
		}
	}

	public static class BasicCollectionInfo
		extends
			CMIInfo.BasicInfo 
		implements 
			ICMIModelObjectCollectionInfo
			
	{
		final private ICMIModelClassInfo _elementInfo;
		public BasicCollectionInfo(final String name, final ICMIModelClassInfo elementInfo)
		{ 
			super(name); 
			_elementInfo = elementInfo;
		}
		
		public ICMIModelClassInfo getElementModelClassInfo()
		{
			return _elementInfo;		
		}
	}	

	
	public abstract static class BasicInfo implements ICMIAbstractInfo
	{
		final private String _name;
		
		protected BasicInfo(final String name) { _name = name; }
		
		public String getName() { return _name; }
				
		public CMISetting getSetting(final CMISettingDefinition settingDef) 
			{ throw new UnsupportedOperationException(); }
		public void setSetting(final CMISettingDefinition settingDef, final String value) 
			{throw new UnsupportedOperationException();}
		
		public Map getSettings() 
			{ return Collections.EMPTY_MAP; }

		public boolean supports(String option) 
			{ return false; }

		public Collection supportedOptions()
			{ return Collections.EMPTY_SET; }

		public void addSupportedOption(final String option)
			{ throw new UnsupportedOperationException();}		
	}
}
