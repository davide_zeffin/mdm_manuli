/*
 * Created on 05.07.2005
 *
 */
package de.beisertbtc.wd.util.cmi;

import java.util.Collection;

import com.sap.tc.cmi.metadata.ICMIModelClassInfo;

import com.sap.tc.cmi.model.ICMIModel;
import com.sap.tc.cmi.model.ICMIModelClass;
import com.sap.tc.cmi.model.ICMIGenericModelClass;
/**
 * Base implementation of <code>ICMIGenericModelClass</code>.
 * Typically you rather use GenericCMIBean that is a full blown
 * implementation and easier to use.
 * 
 * @author dbeisert
 * @see GenericCMIBean
 * @see CMIInfo
 */
abstract public class CMIBean implements ICMIGenericModelClass 
{
	public Object getAttributeValue(final String name)
	{
		throw new IllegalArgumentException("Unknown attribute: " + name);			
	}
	
	public void setAttributeValue(final String name, final Object value)
	{
		throw new IllegalArgumentException("Unknown attribute: " + name);			
	}
	
	public ICMIModelClass getRelatedModelObject(final String targetRoleName) { return illegal(targetRoleName); }
	public void setRelatedModelObject(final String targetRoleName, final ICMIModelClass o) { illegal(targetRoleName); }
	public Collection getRelatedModelObjects(final String targetRoleName) { illegal(targetRoleName); return null; }
	public void setRelatedModelObjects(final String targetRoleName, final Collection col) { illegal(targetRoleName); }
	public boolean addRelatedModelObject(final String targetRoleName, final ICMIModelClass o){ illegal(targetRoleName); return false; }
	public boolean removeRelatedModelObject(final String targetRoleName, final ICMIModelClass o) { illegal(targetRoleName); return false; }

	public ICMIModel associatedModel() { return null; }	

	final private static ICMIModelClass illegal(final String relation)
	{
		throw new IllegalArgumentException("No relation with name " + relation);
	}

}
