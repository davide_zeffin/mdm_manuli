/*
 * Created on 31.07.2007 by dbeisert
 * Copyright Holcim 2007
 */
package de.beisertbtc.wd.util.ovs;

import java.util.Collection;
import java.util.List;

import com.sap.tc.cmi.model.ICMIGenericModelClass;
import com.sap.tc.webdynpro.progmodel.api.IWDNodeElement;

/**
 * Interface needed for the <code>OVSBuilder</code> that encapsulates
 * the logic of an <code>IWDOVSNotificationListener</code> and a <code>ICMIQuery</code>.
 * Clients need to implement this interface to
 * be able to use the <code>OVSBuilder</code>. 
 * 
 * @author dbeisert
 * @see OVSBuilder
 */
public interface IOVSBehaviour {
	/**
	 * this method is called when the OVS is opened. It is used
	 * to initialize the fields with default values.
	 * @param inputElement
	 * @param inputBean
	 * @see OVSBuilder
	 */
	public void applyInputValues(IWDNodeElement inputElement, ICMIGenericModelClass inputBean);
	 /**
	  * executed when the user presses Search. It can return
	  * any Collection, the method transformResult is used
	  * to convert each object in the collection to a ICMI object
	  * that can be displayed in the result list.
	  * 
	  * @param inputBean	The CMIBean that holds the input values
	  * @return a Collection of any objects.
	  * @see OVSBuilder
	  */
	 public Collection search(ICMIGenericModelClass inputBean);
	 /**
	  * after the search is executed this method is used
	  * to transform each search result object into a CMIBean
	  * that the OVS can display.
	  * 
	  * @param resultObject
	  * @param resultBean
	  * @see OVSBuilder
	  */
	 public void transformResult(Object resultObject, ICMIGenericModelClass resultBean);
	 /**
	  * executed when the user selects an entry in the result list. You
	  * have the possibility to take values from the selected result object and put 
	  * in the IWDNodeElement where the OVS is bound to.
	  * 
	  * @param inputElement the NodeElement where the OVS belongs to.
	  * @param resultBean The selected CMIBean in the list.
	  * @see OVSBuilder
	  */
	 public void applyResult(IWDNodeElement inputElement, ICMIGenericModelClass resultBean);
}
