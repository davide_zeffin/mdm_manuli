/*
 * Created on 06.06.2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package de.beisertbtc.wd.util.filter;

/**
 * Filter a list, needed by the CollectionUtil
 * @author dbe
 */
public interface IFilter {

	/**
	 * this method needs to be implemented to choose an entry in the list that stays in the list
	 * @param obj
	 * @return false, if you do not want to keep that entry in the filtered list
	 */
	boolean keepInList(Object obj);
}
