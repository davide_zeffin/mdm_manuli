/*
 * Created on 31.07.2007 by dbeisert
 * Copyright Holcim 2007
 */
package de.beisertbtc.wd.util.cmi;

import java.util.Collection;

import com.sap.tc.cmi.exception.CMIException;
import com.sap.tc.cmi.metadata.ICMIModelClassInfo;
import com.sap.tc.cmi.metadata.ICMIModelObjectCollectionInfo;
import com.sap.tc.cmi.model.ICMIGenericModelClass;
import com.sap.tc.cmi.model.ICMIModel;
import com.sap.tc.cmi.model.ICMIQuery;

/**
 * Base implementation ICMIQuery, so that most methods
 * are already implemented.
 * 
 * @author dbeisert
 */
public abstract class AbstractCMIQuery implements ICMIQuery{

	ICMIGenericModelClass _input = null;
	protected Collection _result;
	ICMIModelObjectCollectionInfo _resultInfo = null;
	
	public AbstractCMIQuery(ICMIGenericModelClass pInput, ICMIModelObjectCollectionInfo pResultInfo){
		_input = pInput;
		_resultInfo = pResultInfo;
	}
	

	public abstract void execute() throws CMIException ;

	public long countOf() {
		if(getResult()==null)return 0;
		return getResult().size();
	}

	public boolean isDirty() {
		// TODO Auto-generated method stub
		return false;
	}

	public Object getInputParameter() {
		return _input;
	}

	public Collection getResult() {
		return _result;
	}

	public ICMIModelClassInfo associatedInputParameterInfo() {
		return _input.associatedModelClassInfo();
	}

	public ICMIModelObjectCollectionInfo associatedResultInfo() {
		return _resultInfo;
	}

	public ICMIModel associatedModel() {
		return null;
	}

	public ICMIModelClassInfo associatedModelClassInfo() {
		return associatedInputParameterInfo();
	}

	/**
	 * @param pCollection
	 */
	public void setResult(Collection pCollection) {
		_result = pCollection;
	}

}
