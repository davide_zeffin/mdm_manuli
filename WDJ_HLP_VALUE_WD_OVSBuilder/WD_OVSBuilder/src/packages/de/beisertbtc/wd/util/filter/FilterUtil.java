/*
 * Created on 25.02.2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package de.beisertbtc.wd.util.filter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * 
 * Utility functions for List, Collections, Enumerations, Iterators etc
 * @author DBE
  
 */
public abstract class FilterUtil {

	/**
	 * create a new List with the values filtered out
	 * @param list
	 * @param filter
	 * @return
	 */
	public static List filterList(List list, IFilter filter)
	{
		Object element = null;
		List result = new ArrayList();
		if(list == null){
			return list;
		}
		for (Iterator iter = list.iterator(); iter.hasNext();) {
			element = iter.next();	
			if(filter.keepInList(element))
			{
				result.add(element);
			}
		}  
		return result;
	}

}
