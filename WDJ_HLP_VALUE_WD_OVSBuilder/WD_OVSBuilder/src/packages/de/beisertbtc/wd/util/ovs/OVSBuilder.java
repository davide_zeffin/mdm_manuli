/*
 * Created on 31.07.2007 by dbeisert
 * Copyright Holcim 2007
 */
package de.beisertbtc.wd.util.ovs;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import de.beisertbtc.wd.util.cmi.AbstractCMIQuery;
import de.beisertbtc.wd.util.cmi.CMIBean;
import de.beisertbtc.wd.util.cmi.CMIInfo; 
import de.beisertbtc.wd.util.cmi.GenericCMIBean;
import com.sap.tc.cmi.exception.CMIException;
import com.sap.tc.cmi.metadata.ICMIModelClassInfo;
import com.sap.tc.cmi.metadata.ICMIModelObjectCollectionInfo;
import com.sap.tc.cmi.model.ICMIGenericModelClass;
import com.sap.tc.webdynpro.progmodel.api.IWDAttributeInfo;
import com.sap.tc.webdynpro.progmodel.api.IWDNode;
import com.sap.tc.webdynpro.progmodel.api.IWDNodeElement;
import com.sap.tc.webdynpro.progmodel.api.IWDOVSNotificationListener;
import com.sap.tc.webdynpro.progmodel.api.WDValueServices;

/**
 * Utility class that makes it easier to implement an
 * OVS Search.
 * <p>
 * Example:<br>
 * <pre>
 * //just an example of an dao
 * final SearchDAO someDao = DAOMananager.getManager().getSearchDAO();
 * OVSBuilder ovs = new OVSBuilder("SomeUniqueNameOfOVS", WDResourceHandler.getCurrentSessionLocale());
 * // add string field
 * ovs.getInputInfo().addProperty("name","a Title",String.class);
 * // add a checkbox
 * ovs.getInputInfo().addProperty("onlyMales","show only males?", Boolean.class);
 * //add a field with a valueset
 * ovs.getInput().addProperty("years","In Year", Integer.class, 4);
 * 
 * ovs.getResultInfo().addProperty("customerId","Kundennr", Long.class);
 * ovs.getResultInfo().addProperty("customerName","Name", String.class);
 * ovs.getResultInfo().addProperty("male","Male",Boolean.class);
 * 
 * ovs.setOVSBehaviour(
 * 	new OVSBehaviour(){
 * 		public void applyInputValues(IWDNodeElement inputElement, ICMIGenericModelClass inp){
 * 			inp.setAttributeValue("name",inputElement.getAttributeValue("name"));
 * 			inp.setAttributeValue("years",new Integer(2007));
 * 			inp.setAttributeValue("male",Boolean.FALSE);
 * 		}
 * 		public List search(IWDNodeElement inputElement, ICMIGenericModelClass inp){
 * 			List customers = someDao.findCustomers(
 * 				(String)inp.getAttributeValue("name"),
 * 				(Boolean)inp.getAttributeValue("onlyMales"),
 * 				(Integer)inp.getAttributeValue("years")
 * 			);
 * 			return customers;
 * 		}
 * 		public void transformResult(Object resultObject, ResultBean bean){
 * 			Customer cust = (Customer)resultObject;
 * 			bean.setAttributeValue("name",cust.getName());
 * 			bean.setAttributeValue("customerId",cust.getId());
 * 			bean.setAttributeValue("male",cust.getGender() == Customer.MALE?Boolean.TRUE:Boolean.FALSE);
 * 		}
 * 
 * 		public void applyResult(IWDNodeElement inputElement, ResultBean bean){
 * 			inputElement.setAttributeValue("customerId", bean.getValueLong("customerId"));
 * 			//maybe just to update the UI, set the name also
 * 			inputElement.setAttributeValue("customerName", bean.getValueString("customerName");
 * 		}  		
 *  }
 * );
 * //finally add the OVS help to a field
 * ovs.bindOVS(wdContext.nodeOrders(),"customerId");
 * 
 * 
 * </pre>
 * <p>
 * It can be advisable to ovveride this OVSBuilder to perform
 * a specific search function only. For example an CustomerOVSBuilder that only
 * looks for Customers. 
 * <p>
 * You can see the OVS in action in the <code>OVSTestComp->OVSTestCompView</code>.
 * 
 * @author dbeisert
 * @see com.holcim.hgrs.tco.basic.ui.wd.ovs.comp.test.OVSTestCompView
 */
public class OVSBuilder {

	private String _name;
	private GenericCMIBean _input = null;
	//private CMIInfo.GenericClassInfo _inputInfo;
	private CMIInfo.GenericClassInfo _resultInfo;
	//private ICMIModelObjectCollectionInfo _resultCollectionInfo;
	private IOVSBehaviour _ovsBehaviour;
	
	
	public OVSBuilder(String pName, Locale loc) {
		super();
		this._name = pName;
		_input = new GenericCMIBean("input",loc);
		_resultInfo = new CMIInfo.GenericClassInfo("result",loc);
	}
	
	/**
	 * Implementation of the OVSNotificationListener
	 * that uses the OVSBehaviour
	 * OVSBuilder
	 * 
	 * @author dbeisert
	 */
	private class OVSNotificationListener implements IWDOVSNotificationListener{
		
		
		public void applyInputValues(IWDNodeElement inputElement, Object inputBean) {
			_ovsBehaviour.applyInputValues(inputElement,_input);

		}
		public void applyResult(IWDNodeElement inputElement, Object resultBean) {
			_ovsBehaviour.applyResult(inputElement,(ICMIGenericModelClass)resultBean);
		}
		public void onQuery(Object arg0) {
		}
		/**
		 * @param _query
		 * @param attribute
		 */
		public OVSNotificationListener() {
		}

	}
	
	/**
	 * 
	 * Uses the OVSBehaviour to execute the query
	 * 
	 * @author dbeisert
	 */
	private class OVSQuery extends AbstractCMIQuery{
		/**
		 * @param pInput
		 * @param pResultInfo
		 */
		public OVSQuery(ICMIModelObjectCollectionInfo resultCollectionInfo) {
			super(_input, resultCollectionInfo);
		}

		public void execute() throws CMIException {
			Collection result = _ovsBehaviour.search(_input);
			List ovsResult = new ArrayList();
			
			for (Iterator iter = result.iterator(); iter.hasNext();) {
				Object element =  iter.next();
				GenericCMIBean resultBean = new GenericCMIBean(_resultInfo);				
				_ovsBehaviour.transformResult(element,resultBean);
				ovsResult.add(resultBean);
			}
			setResult(ovsResult);
		}

	}
	
	
	/**
	 * Binds the OVS to the IWDNodeAttribute.
	 * Before this can be called the inputInfo and resultInfo must be
	 * properly initialized and the IOVSBehavior must be set.
	 * 
	 * @param node
	 * @param attribute
	 */
	public void bindOVS(IWDNode node, String attribute){
			IWDAttributeInfo attr = node.getNodeInfo().getAttribute(attribute);
//			node.getContext().getController().getComponent().getMessageManager().repo
			//these have to be initialized before the OVSQuery, or OVSListener are used
			
			//CMIInfo.GenericClassInfo inputInfo = _input.getGenericClassInfo();
			 ICMIModelObjectCollectionInfo resultCollectionInfo = new CMIInfo.BasicCollectionInfo("result",_resultInfo);
			
			OVSQuery query = new OVSQuery(resultCollectionInfo);
			OVSNotificationListener listener = new OVSNotificationListener();
			
			WDValueServices.addOVSExtension(_name,
				new IWDAttributeInfo[]{attr},
				query, 
				listener
			
			);
		}
	/**
	 * @return
	 */
	public ICMIGenericModelClass getInput() {
		return _input;
	}
	/**
	 * @return
	 */
	public CMIInfo.GenericClassInfo getInputInfo() {
		if(_input == null)return null;
		return _input.getGenericClassInfo();
	}

	/**
	 * @return
	 */
	public String getName() {
		return _name;
	}

	/**
	 * @return
	 */
	public CMIInfo.GenericClassInfo getResultInfo() {
		return _resultInfo;
	}

	/**
	 * @param pBehaviour
	 */
	public void setOvsBehaviour(IOVSBehaviour pBehaviour) {
		_ovsBehaviour = pBehaviour;
	}

}
