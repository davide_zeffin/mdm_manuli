/*
 * Created on 12.10.2007 by dbeisert
 * Copyright Holcim 2007
 */
package de.beisertbtc.wd.example.ovsbuilder;

import java.math.BigDecimal;

/**
 * 
 * 
 * @author dbeisert
 */
public class Customer {

	/**
	 * @param pInteger
	 * @param pString
	 * @param pString2
	 * @param pDecimals
	 */
	public Customer(Integer pInteger, String pString, String pString2, BigDecimal pDecimal) {
		id = pInteger;
		lastName = pString;
		firstName = pString2;
		moneyToSpent = pDecimal;
	}



	public final static String GENDER_MALE = "MALE";
	public final static String GENDER_FEMALE = "FEMALE";
	
	Integer id;
	String lastName;
	String firstName;
	//Male or female, this will be dropdown in the UI
	String gender;
	BigDecimal moneyToSpent = new BigDecimal(0);
	
	public Customer() {
		super();
	}

	

	/**
	 * @return
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @return
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @return
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @return
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @return
	 */
	public BigDecimal getMoneyToSpent() {
		return moneyToSpent;
	}

	/**
	 * @param pString
	 */
	public void setFirstName(String pString) {
		firstName = pString;
	}

	/**
	 * @param pString
	 */
	public void setGender(String pString) {
		gender = pString;
	}

	/**
	 * @param pInteger
	 */
	public void setId(Integer pInteger) {
		id = pInteger;
	}

	/**
	 * @param pString
	 */
	public void setLastName(String pString) {
		lastName = pString;
	}

	/**
	 * @param pDecimal
	 */
	public void setMoneyToSpent(BigDecimal pDecimal) {
		moneyToSpent = pDecimal;
	}

}
