/*
 * Created on 22.10.2007 by dbeisert
 * Copyright Holcim 2007
 */
package de.beisertbtc.wd.example.ovsbuilder;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import com.sap.tc.cmi.model.ICMIGenericModelClass;
import com.sap.tc.webdynpro.progmodel.api.IWDNodeElement;
import com.sap.tc.webdynpro.services.sal.localization.api.WDResourceHandler;
import de.beisertbtc.wd.util.ovs.BaseOVSBuilder;

/**
 * Sample implementation of a BaseOVSBuilder. 
 * 
 * @author dbeisert
 */
public abstract class CustomerOVSBuilder extends BaseOVSBuilder{

public CustomerOVSBuilder() {
	//assign a unique name to the OVS, and the current locale
	super("CustomerOVS",WDResourceHandler.getCurrentSessionLocale());
	
	//1. Define input screen
	// addProperty( <id of field>, <displayed label>, <datatype> [, <optional Valueset>] )
	getInputInfo().addProperty("id","Customer ID",Integer.class);
	getInputInfo().addProperty("lastName","Last Name",String.class);
	getInputInfo().addProperty("firstName","First Name",String.class);
	// here is an example of an input field that has a valueset, I have implemented this with a Map
	Map map = new LinkedHashMap();
	map.put(Customer.GENDER_MALE,"Male");
	map.put(Customer.GENDER_FEMALE,"Female");
	getInputInfo().addProperty("gender","Gender",String.class,map);
	
	getInputInfo().addProperty("money","Money",BigDecimal.class);
	
	//2. Define Result List
	// addProperty( <id of field>, <displayed label>, <datatype> [, <optional Valueset>] )
	getResultInfo().addProperty("id","Customer ID",Integer.class);
	getResultInfo().addProperty("lastName","Last Name",String.class);
	getResultInfo().addProperty("firstName","First Name",String.class);
	getResultInfo().addProperty("gender","Gender",String.class,map);
	getResultInfo().addProperty("money","Money",BigDecimal.class);
	
}

/**
 * The user opens the OVS. Here you can prefill values from the nodeElement from to which the OVS is bound to.
 */
public void applyInputValues(IWDNodeElement inputElement, ICMIGenericModelClass inputBean) {
			
}

/**
 * Extract the parameters from the input screen and execute any search that returns a Collection
 */
public Collection search(ICMIGenericModelClass inputBean) {
	Integer id = (Integer)inputBean.getAttributeValue("id");
	String lastName = (String)inputBean.getAttributeValue("lastName");
	String firstName = (String)inputBean.getAttributeValue("firstName");
	String gender = (String)inputBean.getAttributeValue("gender");
	BigDecimal money  = (BigDecimal)inputBean.getAttributeValue("money");
	MyDummyDAO dao = new MyDummyDAO();
	return dao.findCustomers(id,lastName,firstName,gender,money);
}

/**
 * The Collection returned by search is iterated and for each element in the Collection
 * this method is called that enables you to convert your Object from the query to
 * a generic CMI Bean that the OVS expects.
 */
public void transformResult(Object resultObject, ICMIGenericModelClass resultBean) {
	Customer c = (Customer)resultObject;
	
	resultBean.setAttributeValue("id",c.getId());
	resultBean.setAttributeValue("lastName",c.getLastName());
	resultBean.setAttributeValue("firstName",c.getFirstName());
	resultBean.setAttributeValue("gender",c.getGender());
	resultBean.setAttributeValue("money",c.getMoneyToSpent());
}

/**
 * The user clicked on an entry in the result list. Here you can transport the selected element to your nodeElement.
 */
public abstract void applyResult(IWDNodeElement inputElement, ICMIGenericModelClass resultBean);
}
