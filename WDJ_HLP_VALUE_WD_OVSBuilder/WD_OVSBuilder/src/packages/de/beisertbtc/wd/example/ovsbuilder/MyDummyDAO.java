/*
 * Created on 12.10.2007 by dbeisert
 * Copyright Holcim 2007
 */
package de.beisertbtc.wd.example.ovsbuilder;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import de.beisertbtc.wd.util.filter.FilterUtil;
import de.beisertbtc.wd.util.filter.IFilter;

/**
 * Usually this would server customers from the db, in this way we do it just simple
 * with an existing list of dummy <code>Customers</code>.
 * 
 * @author dbeisert
 */
public class MyDummyDAO {

	List allCustomers = new ArrayList();
	
	/**
	 * initialize the customer list.
	 *
	 */
	public MyDummyDAO() {
		super();
		String[] lastNames = {"Schmidt","Brandt","Kohl","Schroeder","Merkel"};
		String[] firstNames = {"Helmut","Willi","Helmut","Gerhardt","Angie"};
		String[] money = {"1","10.5","1000000","100000","20000"};
		String[] genders = {Customer.GENDER_MALE,Customer.GENDER_MALE,Customer.GENDER_MALE,Customer.GENDER_MALE,Customer.GENDER_FEMALE};
		
		for (int i = 0; i < lastNames.length; i++) {
			Customer c = new Customer(new Integer(i+1),lastNames[i],firstNames[i],new BigDecimal(money[i]));
			c.setGender(genders[i]);
			allCustomers.add(c);
		}
	}
	
	public List findCustomers(final Integer id, final String lastName, final String firstName, final String gender, final BigDecimal minimumMoney){
		List result = FilterUtil.filterList(allCustomers,
			new IFilter(){
				public boolean keepInList(Object obj) {
					Customer c = (Customer)obj;
					boolean bId = true;
					boolean bLName = true;
					boolean bFName = true;
					boolean bGender = true;
					boolean bMoney = true;
					if(id !=null && c.id !=null){
						bId =(id.intValue() == c.id.intValue());
					}
					if(lastName!=null && c.lastName !=null){
						bLName = (c.lastName.startsWith(lastName)) ;
					}
					if(firstName!=null && c.firstName !=null){
						bFName = (c.firstName.startsWith(firstName));
					}
					if(gender!=null && c.gender !=null){
						bGender = (c.gender.equals(gender));
					}
					if(minimumMoney!=null && c.moneyToSpent !=null){
						bMoney = (c.moneyToSpent.compareTo(minimumMoney)>-1);
					}
					return bId && bLName && bFName && bGender && bMoney;
				}

			}
		);
		
		return result;
	}

}
