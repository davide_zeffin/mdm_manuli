// ---------------------------------------------------------------------------
// This file has been generated partially by the Web Dynpro Code Generator.
// MODIFY CODE ONLY IN SECTIONS ENCLOSED BY @@begin AND @@end.
// ALL OTHER CHANGES WILL BE LOST IF THE FILE IS REGENERATED.
// ---------------------------------------------------------------------------
package com.sap.mdm.blog.epfc;

// 
// IMPORTANT NOTE: 
// _ALL_ IMPORT STATEMENTS MUST BE PLACED IN THE FOLLOWING SECTION ENCLOSED
// BY @@begin imports AND @@end. FURTHERMORE, THIS SECTION MUST ALWAYS CONTAIN
// AT LEAST ONE IMPORT STATEMENT (E.G. THAT FOR IPrivateSTATIC_MDM_EPFC_CompView).
// OTHERWISE, USING THE ECLIPSE FUNCTION "Organize Imports" FOLLOWED BY
// A WEB DYNPRO CODE GENERATION (E.G. PROJECT BUILD) WILL RESULT IN THE LOSS
// OF IMPORT STATEMENTS.
//
//@@begin imports
import com.sap.mdm.blog.epfc.wdp.IPrivateSTATIC_MDM_EPFC_CompView;
import com.sap.tc.webdynpro.clientserver.event.api.WDPortalEventing;
import com.sap.tc.webdynpro.clientserver.navigation.api.WDPortalNavigation;
import com.sap.tc.webdynpro.clientserver.navigation.api.WDPortalNavigationHistoryMode;
import com.sap.tc.webdynpro.clientserver.navigation.api.WDPortalNavigationMode;
//@@end

//@@begin documentation
//@@end

public class STATIC_MDM_EPFC_CompView
{
  /**
   * Logging location.
   */
  private static final com.sap.tc.logging.Location logger = 
    com.sap.tc.logging.Location.getLocation(STATIC_MDM_EPFC_CompView.class);

  static 
  {
    //@@begin id
		String id = "$Id$";
    //@@end
    com.sap.tc.logging.Location.getLocation("ID.com.sap.tc.webdynpro").infoT(id);
  }

  /**
   * Private access to the generated Web Dynpro counterpart 
   * for this controller class.  </p>
   *
   * Use <code>wdThis</code> to gain typed access to the context,
   * to trigger navigation via outbound plugs, to get and enable/disable
   * actions, fire declared events, and access used controllers and/or 
   * component usages.
   *
   * @see com.sap.mdm.blog.epfc.wdp.IPrivateSTATIC_MDM_EPFC_CompView for more details
   */
  private final IPrivateSTATIC_MDM_EPFC_CompView wdThis;

  /**
   * Root node of this controller's context. </p>
   *
   * Provides typed access not only to the elements of the root node 
   * but also to all nodes in the context (methods node<i>XYZ</i>()) 
   * and their currently selected element (methods current<i>XYZ</i>Element()). 
   * It also facilitates the creation of new elements for all nodes 
   * (methods create<i>XYZ</i>Element()). </p>
   *
   * @see com.sap.mdm.blog.epfc.wdp.IPrivateSTATIC_MDM_EPFC_CompView.IContextNode for more details.
   */
  private final IPrivateSTATIC_MDM_EPFC_CompView.IContextNode wdContext;

  /**
   * A shortcut for <code>wdThis.wdGetAPI()</code>. </p>
   * 
   * Represents the generic API of the generic Web Dynpro counterpart 
   * for this controller. </p>
   */
  private final com.sap.tc.webdynpro.progmodel.api.IWDViewController wdControllerAPI;
  
  /**
   * A shortcut for <code>wdThis.wdGetAPI().getComponent()</code>. </p>
   * 
   * Represents the generic API of the Web Dynpro component this controller 
   * belongs to. Can be used to access the message manager, the window manager,
   * to add/remove event handlers and so on. </p>
   */
  private final com.sap.tc.webdynpro.progmodel.api.IWDComponent wdComponentAPI;
  
  public STATIC_MDM_EPFC_CompView(IPrivateSTATIC_MDM_EPFC_CompView wdThis)
  {
    this.wdThis = wdThis;
    this.wdContext = wdThis.wdGetContext();
    this.wdControllerAPI = wdThis.wdGetAPI();
    this.wdComponentAPI = wdThis.wdGetAPI().getComponent();
  }

  //@@begin javadoc:wdDoInit()
	/** Hook method called to initialize controller. */
  //@@end
  public void wdDoInit()
  {
    //@@begin wdDoInit()
		namespace = "urn:com.sap.pct.mdm.appl.masteriviews";
		eventname = "selectRecord";
		WDPortalEventing.subscribe(
			namespace,
			eventname,
			wdThis.wdGetCatchValueAction());
		wdContext.currentContextElement().setStd("01");
		wdContext.currentContextElement().setAlt("02");

    //@@end
  }

  //@@begin javadoc:wdDoExit()
	/** Hook method called to clean up controller. */
  //@@end
  public void wdDoExit()
  {
    //@@begin wdDoExit()
    //@@end
  }

  //@@begin javadoc:wdDoModifyView
	/**
	 * Hook method called to modify a view just before rendering.
	 * This method conceptually belongs to the view itself, not to the
	 * controller (cf. MVC pattern).
	 * It is made static to discourage a way of programming that
	 * routinely stores references to UI elements in instance fields
	 * for access by the view controller's event handlers, and so on.
	 * The Web Dynpro programming model recommends that UI elements can
	 * only be accessed by code executed within the call to this hook method.
	 *
	 * @param wdThis Generated private interface of the view's controller, as
	 *        provided by Web Dynpro. Provides access to the view controller's
	 *        outgoing controller usages, etc.
	 * @param wdContext Generated interface of the view's context, as provided
	 *        by Web Dynpro. Provides access to the view's data.
	 * @param view The view's generic API, as provided by Web Dynpro.
	 *        Provides access to UI elements.
	 * @param firstTime Indicates whether the hook is called for the first time
	 *        during the lifetime of the view.
	 */
  //@@end
  public static void wdDoModifyView(IPrivateSTATIC_MDM_EPFC_CompView wdThis, IPrivateSTATIC_MDM_EPFC_CompView.IContextNode wdContext, com.sap.tc.webdynpro.progmodel.api.IWDView view, boolean firstTime)
  {
    //@@begin wdDoModifyView
    //@@end
  }

  //@@begin javadoc:onActionCatchValue(ServerEvent)
	/** Declared validating event handler. */
  //@@end
  public void onActionCatchValue(com.sap.tc.webdynpro.progmodel.api.IWDCustomEvent wdEvent, java.lang.String dataObject )
  {
    //@@begin onActionCatchValue(ServerEvent)

		int marked = dataObject.indexOf("=");
		String value = dataObject.substring(marked + 1);
		wdContext.currentContextElement().setCatchedValue(value);

    //@@end
  }

  //@@begin javadoc:onActionPrint(ServerEvent)
	/** Declared validating event handler. */
  //@@end
  public void onActionPrint(com.sap.tc.webdynpro.progmodel.api.IWDCustomEvent wdEvent )
  {
    //@@begin onActionPrint(ServerEvent)

		String appParams =
			"stampa=1&table="
				+ wdContext.currentContextElement().getCatchedValue()
				+ "&catalogo="
				+ wdContext.currentContextElement().getFlag_catalogo()
				+ "&category="
				+ wdContext.currentContextElement().getFlag_famiglie()
				+ "&crimpings_std="
				+ wdContext.currentContextElement().getStd()
				+ "&crimpings_alt="
				+ wdContext.currentContextElement().getAlt();

		WDPortalNavigation.navigateAbsolute(
			"ROLES:portal_content/com.manulirubber.manuli_0/com.manulirubber.catalogue.iviews/com.manulirubber.staticstampa",
			WDPortalNavigationMode.SHOW_EXTERNAL,
			WDPortalNavigationHistoryMode.ALLOW_DUPLICATIONS,
			appParams);

    //@@end
  }

  //@@begin javadoc:onActionPrintMarcatura(ServerEvent)
	/** Declared validating event handler. */
  //@@end
  public void onActionPrintMarcatura(com.sap.tc.webdynpro.progmodel.api.IWDCustomEvent wdEvent )
  {
    //@@begin onActionPrintMarcatura(ServerEvent)

		String appParams =
			"stampa=3&table="
				+ wdContext.currentContextElement().getCatchedValue()
				+ "&catalogo="
				+ wdContext.currentContextElement().getFlag_catalogo()
				+ "&category="
				+ wdContext.currentContextElement().getFlag_famiglie()
				+ "&crimpings_std="
				+ wdContext.currentContextElement().getStd()
				+ "&crimpings_alt="
				+ wdContext.currentContextElement().getAlt();

		WDPortalNavigation.navigateAbsolute(
			"ROLES://portal_content/com.manulirubber.manuli_0/com.manulirubber.catalogue.iviews/com.manulirubber.staticstampa",
			WDPortalNavigationMode.SHOW_EXTERNAL,
			WDPortalNavigationHistoryMode.ALLOW_DUPLICATIONS,
			appParams);

    //@@end
  }

  //@@begin javadoc:onActionPrintCatalog2(ServerEvent)
	/** Declared validating event handler. */
  //@@end
  public void onActionPrintCatalog2(com.sap.tc.webdynpro.progmodel.api.IWDCustomEvent wdEvent )
  {
    //@@begin onActionPrintCatalog2(ServerEvent)

		String appParams =
			"stampa=2&table="
				+ wdContext.currentContextElement().getCatchedValue()
				+ "&catalogo="
				+ wdContext.currentContextElement().getFlag_catalogo()
				+ "&category="
				+ wdContext.currentContextElement().getFlag_famiglie()
				+ "&crimpings_std="
				+ wdContext.currentContextElement().getStd()
				+ "&crimpings_alt="
				+ wdContext.currentContextElement().getAlt();

		WDPortalNavigation.navigateAbsolute(
			"ROLES://portal_content/com.manulirubber.manuli_0/com.manulirubber.catalogue.iviews/com.manulirubber.staticstampa",
			WDPortalNavigationMode.SHOW_EXTERNAL,
			WDPortalNavigationHistoryMode.ALLOW_DUPLICATIONS,
			appParams);

    //@@end
  }

  /*
   * The following code section can be used for any Java code that is 
   * not to be visible to other controllers/views or that contains constructs
   * currently not supported directly by Web Dynpro (such as inner classes or
   * member variables etc.). </p>
   *
   * Note: The content of this section is in no way managed/controlled
   * by the Web Dynpro Designtime or the Web Dynpro Runtime. 
   */
  //@@begin others
	private String namespace;
	private String eventname;
  //@@end
}
