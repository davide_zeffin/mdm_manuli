/****************************************************************************
*****************************************************************************
****
****  For this to compile and run, make sure sapjco.jar is in your classpath
****
*****************************************************************************
****************************************************************************/
import java.io.PrintWriter;
import java.io.FileWriter;

import com.sap.mw.jco.*;

public class SapListener extends JCO.Server {
	private String      logFile = "SapListener.log";
	private PrintWriter out     = null;
    

	public SapListener( String sapGwHost, String sapGwService, String progId, IRepository repository ) {
		super( sapGwHost, sapGwService, progId, repository );
		this.setProperty( "jco.server.unicode", "0" );

		try {
			out = new PrintWriter( new FileWriter( logFile ), true );
		} catch( Exception ex ) {
			out = null;
			ex.printStackTrace();
		}

		this.writeLogMessage( "SAP Listener Started" );
	}

	private void writeLogMessage( String msg ) {
		try {
			out.println( msg );
			out.flush();
		} catch( Exception ex ) {
			ex.printStackTrace();
		}
	}

	protected void handleRequest( JCO.Function function ) {
		String functionName = "IDOC_INBOUND_ASYNCHRONOUS";

		String      idocDataFile = null;
		PrintWriter dataOut      = null;

		JCO.Table controlRecord = null;
		JCO.Table dataRecord    = null;
		System.out.println( "Nr. recs" + controlRecord.getNumRows() );

		if( function.getName().equalsIgnoreCase( functionName ) ) {

			controlRecord = function.getTableParameterList().getTable( 0 );
			dataRecord    = function.getTableParameterList().getTable( 1 );

			if( controlRecord.getNumRows() >= 1 ) {

				this.writeLogMessage( "TABNAM  - " + controlRecord.getString( 0  ) );
				this.writeLogMessage( "IDOCTYP - " + controlRecord.getString( 9  ) );
				this.writeLogMessage( "MESTYP  - " + controlRecord.getString( 11 ) );

				while( controlRecord.nextRow() ) {
					this.writeLogMessage( "TABNAM  - " + controlRecord.getString( 0  ) );
					this.writeLogMessage( "IDOCTYP - " + controlRecord.getString( 9  ) );
					this.writeLogMessage( "MESTYP  - " + controlRecord.getString( 11 ) );
				}
			}
			if( dataRecord.getNumRows() >= 1 ) {

				this.writeLogMessage( "SEGNAM  - " + dataRecord.getString( 0  ) );
				idocDataFile = dataRecord.getString( 0 ) + ".txt";
				try {
					dataOut = new PrintWriter( new FileWriter( idocDataFile ), true );
					dataOut.println( dataRecord.getString( 6 ) );
					dataOut.flush();
					dataOut.close();
				} catch( Exception ex ) {
					ex.printStackTrace();
				}

				while( dataRecord.nextRow() ) {
					this.writeLogMessage( "SEGNAM - " + dataRecord.getString( 0  ) );
					idocDataFile = dataRecord.getString( 0 ) + ".txt";
					try {
						dataOut = new PrintWriter( new FileWriter( idocDataFile ), true );
						dataOut.println( dataRecord.getString( 6 ) );
						dataOut.flush();
						dataOut.close();
					} catch( Exception ex ) {
						ex.printStackTrace();
					}
				}
			}
		}
	}

	public static void main( String [] argv ) {
		JCO.addClientPool( "POOL", 3, "100", "dimartij", "johann74", "EN", "r3svil.tods.com", "01" );
		IRepository rep = JCO.createRepository( "REP", "POOL" );

		SapListener listener = new SapListener( "r3svil.tods.com", "sapgw01", "JCOSERVER01", rep );
		listener.start();
	}

}
