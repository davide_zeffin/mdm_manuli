import com.sap.mw.jco.IRepository;
import com.sap.mw.jco.JCO;

/*
 * Created on 18-lug-2009
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */

/**
 * @author Administrator
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class FirstExample {

	static MyFirstServer serverConnections[] = new MyFirstServer[3];

	/**

	 *  Start the server

	 */

	public static void startServers()  {

	  JCO.addClientPool("POOL",  3, "100", "dimartij" ,"rodamajo" , "EN", 

									 "abap_system" ,"00");

	   IRepository repository = JCO.createRepository("REP", "POOL");

	   for(int i = 0; i < serverConnections.length; i++) {

		   // Server listens for incoming requests from system 1

		   // (Change gateway host, service, and program ID according to your needs)

		   serverConnections [i] = new MyFirstServer 

							 ("xitest",  //gateway host, often the same as host

							  "sapgw00", //gateway service, generally sapgw+<SYSNR>

							  "JCOSERVER01", // corresponds to program ID defined in SM59

							  repository);

		   serverConnections [i].start();

	}
	}
	public static void stopServers()  {

	   for(int i = 0; i < serverConnections.length; i++) {

		   serverConnections [i].stop();

	}
	}
	public static void main(String[] args)    {

			  startServers() ;

	}

} 
