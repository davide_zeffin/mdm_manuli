import com.sap.mw.jco.IRepository;
import com.sap.mw.jco.JCO;

/*
 * Created on 18-lug-2009
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */

/**
 * @author Administrator
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class MyFirstServer extends JCO.Server {

	/**

	 *  Create an instance of my own server

	 *  @param gwhost (gateway host)

	 *  @param gwserv (gateway service number)

	 *  @param progid (program id)

	 *  @param repository (repository used by the server to lookup the 

	   definitions of an inc)

	 */
public  boolean isUnicode;
 
	public MyFirstServer(String gwhost, String gwserv,
						 String progid, IRepository repository) {
							

			   super(gwhost,gwserv,progid,repository);
//			   jco.server.unicode = 1;
		   this.setProperty("jco.server.unicode", isUnicode?"1":"0");
	} 

 

	/**

	 *  Overrides the default method.

	 */

	protected void handleRequest(JCO.Function function) {

		JCO.ParameterList input  = function.getImportParameterList();

		JCO.ParameterList output = function.getExportParameterList();

		JCO.ParameterList tables = function.getTableParameterList();

        

		System.out.println("handleRequest(" + function.getName() + ")");

		if (function.getName().equals("STFC_CONNECTION")) {

			System.out.println(">>> request STFC_CONNECTION: " + input.getString("REQUTEXT"));

			output.setValue(input.getString("REQUTEXT"),"ECHOTEXT");

			output.setValue("This is a response from MyFirstServer","RESPTEXT");

		}

	}

}

