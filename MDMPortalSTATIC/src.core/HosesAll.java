/*
 * Created on 24-mag-2017
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */

/**
 * @author Administrator
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class HosesAll {

	/**
	 * @param i
	 */

	public HosesAll(int i) {

		// TODO Auto-generated constructor stub

		// TODO Auto-generated constructor stub
		this.Name = new String[i];
		this.catalogue_subtitle = new String[i];
		this.imagePath = new String[i];
		this.key_performance = new String[i];
		this.applications_fluids = new String[i];
		this.main_application = new String[i];
		this.min_cont_serv_temp = new String[i];
		this.max_cont_serv_temp = new String[i];
		this.cont_serv_temp_far = new String[i];
		this.cont_serv_temp_cel = new String[i];
		this.max_operating_temp = new String[i];
		this.min_external_temp = new String[i];
		this.recommended_fluids = new String[i];
		this.refrigerants = new String[i];
		this.lubrificants = new String[i];
		this.tube = new String[i];
		this.reinforcement = new String[i];
		this.cover = new String[i];
		this.applicable_specs = new String[i];
		this.type_approvals = new String[i];
		this.other_technical_notes = new String[i];
		this.standard_Packaging = new String[i];
		this.catalogue_small_exclamation = new String[i];
		this.catalogue_big_exclamation = new String[i];
		this.catalogue_bottom_exclamation = new String[i];
		this.version = new String[i];
		this.version_date = new String[i];

		// TODO Auto-generated constructor stub
		// TODO Auto-generated constructor stub
	}
	/**
	 * @param i
	 */

	private String[] Name;
	private String[] catalogue_subtitle;
	private String[] imagePath;
	private String[] key_performance;
	private String[] applications_fluids;
	private String[] main_application;
	private String[] min_cont_serv_temp;
	private String[] max_cont_serv_temp;
	private String[] cont_serv_temp_far;
	private String[] cont_serv_temp_cel;
	private String[] max_operating_temp;
	private String[] min_external_temp;
	private String[] recommended_fluids;
	private String[] refrigerants;
	private String[] lubrificants;
	private String[] tube;
	private String[] reinforcement;
	private String[] cover;
	private String[] applicable_specs;
	private String[] type_approvals;
	private String[] other_technical_notes;
	private String[] standard_Packaging;
	private String[] catalogue_small_exclamation;
	private String[] catalogue_big_exclamation;
	private String[] catalogue_bottom_exclamation;
	private String[] version;
	private String[] version_date;

	/**
	 * @return
	 */
	public String[] getApplicable_specs() {
		return applicable_specs;
	}

	/**
	 * @return
	 */
	public String[] getApplications_fluids() {
		return applications_fluids;
	}

	/**
	 * @return
	 */
	public String[] getCatalogue_big_exclamation() {
		return catalogue_big_exclamation;
	}

	/**
	 * @return
	 */
	public String[] getCatalogue_bottom_exclamation() {
		return catalogue_bottom_exclamation;
	}

	/**
	 * @return
	 */
	public String[] getCatalogue_small_exclamation() {
		return catalogue_small_exclamation;
	}

	/**
	 * @return
	 */
	public String[] getCatalogue_subtitle() {
		return catalogue_subtitle;
	}

	/**
	 * @return
	 */
	public String[] getCont_serv_temp_cel() {
		return cont_serv_temp_cel;
	}

	/**
	 * @return
	 */
	public String[] getCont_serv_temp_far() {
		return cont_serv_temp_far;
	}

	/**
	 * @return
	 */
	public String[] getCover() {
		return cover;
	}

	/**
	 * @return
	 */
	public String[] getImagePath() {
		return imagePath;
	}

	/**
	 * @return
	 */
	public String[] getKey_performance() {
		return key_performance;
	}

	/**
	 * @return
	 */
	public String[] getLubrificants() {
		return lubrificants;
	}

	/**
	 * @return
	 */
	public String[] getMain_application() {
		return main_application;
	}

	/**
	 * @return
	 */
	public String[] getMax_cont_serv_temp() {
		return max_cont_serv_temp;
	}

	/**
	 * @return
	 */
	public String[] getMax_operating_temp() {
		return max_operating_temp;
	}

	/**
	 * @return
	 */
	public String[] getMin_cont_serv_temp() {
		return min_cont_serv_temp;
	}

	/**
	 * @return
	 */
	public String[] getMin_external_temp() {
		return min_external_temp;
	}

	/**
	 * @return
	 */
	public String[] getName() {
		return Name;
	}
	
	public String getNamePos(int i){
		String[] xName = this.getName();
		int idx = 0;
		while(i!=idx){
		 idx ++;
		}
		return xName[idx];
	}
	
	/**
	 * @return
	 */
	public String[] getOther_technical_notes() {
		return other_technical_notes;
	}

	/**
	 * @return
	 */
	public String[] getRecommended_fluids() {
		return recommended_fluids;
	}

	/**
	 * @return
	 */
	public String[] getRefrigerants() {
		return refrigerants;
	}

	/**
	 * @return
	 */
	public String[] getReinforcement() {
		return reinforcement;
	}

	/**
	 * @return
	 */
	public String[] getStandard_Packaging() {
		return standard_Packaging;
	}

	/**
	 * @return
	 */
	public String[] getTube() {
		return tube;
	}

	/**
	 * @return
	 */
	public String[] getType_approvals() {
		return type_approvals;
	}

	/**
	 * @return
	 */
	public String[] getVersion() {
		return version;
	}

	/**
	 * @return
	 */
	public String[] getVersion_date() {
		return version_date;
	}

	/**
	 * @param strings
	 */
	public void setApplicable_specs(String[] strings) {
		applicable_specs = strings;
	}

	/**
	 * @param strings
	 */
	public void setApplications_fluids(String[] strings) {
		applications_fluids = strings;
	}

	/**
	 * @param strings
	 */
	public void setCatalogue_big_exclamation(String[] strings) {
		catalogue_big_exclamation = strings;
	}

	/**
	 * @param strings
	 */
	public void setCatalogue_bottom_exclamation(String[] strings) {
		catalogue_bottom_exclamation = strings;
	}

	/**
	 * @param strings
	 */
	public void setCatalogue_small_exclamation(String[] strings) {
		catalogue_small_exclamation = strings;
	}

	/**
	 * @param strings
	 */
	public void setCatalogue_subtitle(String[] strings) {
		catalogue_subtitle = strings;
	}

	/**
	 * @param strings
	 */
	public void setCont_serv_temp_cel(String[] strings) {
		cont_serv_temp_cel = strings;
	}

	/**
	 * @param strings
	 */
	public void setCont_serv_temp_far(String[] strings) {
		cont_serv_temp_far = strings;
	}

	/**
	 * @param strings
	 */
	public void setCover(String[] strings) {
		cover = strings;
	}

	/**
	 * @param strings
	 */
	public void setImagePath(String[] strings) {
		imagePath = strings;
	}

	/**
	 * @param strings
	 */
	public void setKey_performance(String[] strings) {
		key_performance = strings;
	}

	/**
	 * @param strings
	 */
	public void setLubrificants(String[] strings) {
		lubrificants = strings;
	}

	/**
	 * @param strings
	 */
	public void setMain_application(String[] strings) {
		main_application = strings;
	}

	/**
	 * @param strings
	 */
	public void setMax_cont_serv_temp(String[] strings) {
		max_cont_serv_temp = strings;
	}

	/**
	 * @param strings
	 */
	public void setMax_operating_temp(String[] strings) {
		max_operating_temp = strings;
	}

	/**
	 * @param strings
	 */
	public void setMin_cont_serv_temp(String[] strings) {
		min_cont_serv_temp = strings;
	}

	/**
	 * @param strings
	 */
	public void setMin_external_temp(String[] strings) {
		min_external_temp = strings;
	}

	/**
	 * @param strings
	 */
	public void setName(String[] strings) {
		Name = strings;
	}

	/**
	 * @param strings
	 */
	public void setOther_technical_notes(String[] strings) {
		other_technical_notes = strings;
	}

	/**
	 * @param strings
	 */
	public void setRecommended_fluids(String[] strings) {
		recommended_fluids = strings;
	}

	/**
	 * @param strings
	 */
	public void setRefrigerants(String[] strings) {
		refrigerants = strings;
	}

	/**
	 * @param strings
	 */
	public void setReinforcement(String[] strings) {
		reinforcement = strings;
	}

	/**
	 * @param strings
	 */
	public void setStandard_Packaging(String[] strings) {
		standard_Packaging = strings;
	}

	/**
	 * @param strings
	 */
	public void setTube(String[] strings) {
		tube = strings;
	}

	/**
	 * @param strings
	 */
	public void setType_approvals(String[] strings) {
		type_approvals = strings;
	}

	/**
	 * @param strings
	 */
	public void setVersion(String[] strings) {
		version = strings;
	}

	/**
	 * @param strings
	 */
	public void setVersion_date(String[] strings) {
		version_date = strings;
	}

	public void setAllHoses(
		String Name,
		String catalogue_subtitle,
		String imagePath,
		String key_performance,
		String applications_fluids,
		String main_application,
		String min_cont_serv_temp,
		String max_cont_serv_temp,
		String cont_serv_temp_far,
		String cont_serv_temp_cel,
		String max_operating_temp,
		String min_external_temp,
		String recommended_fluids,
		String refrigerants,
		String lubrificants,
		String tube,
		String reinforcement,
		String cover,
		String applicable_specs,
		String type_approvals,
		String other_technical_notes,
		String standard_Packaging,
		String catalogue_small_exclamation,
		String catalogue_big_exclamation,
		String catalogue_bottom_exclamation,
		String version,
		String version_date,
		int position_record) {
		int i = position_record;
		
		this.Name[i] = Name;
		this.catalogue_subtitle[i] = catalogue_subtitle;
		this.imagePath[i] = imagePath;
		this.key_performance[i] = key_performance;
		this.applications_fluids[i] = applications_fluids;
		this.main_application[i] = main_application;
		this.min_cont_serv_temp[i] = min_cont_serv_temp;
		this.max_cont_serv_temp[i] = max_cont_serv_temp;
		this.cont_serv_temp_far[i] = cont_serv_temp_far;
		this.cont_serv_temp_cel[i] = cont_serv_temp_cel;
		this.max_operating_temp[i] = max_operating_temp;
		this.min_external_temp[i] = min_external_temp;
		this.recommended_fluids[i] = recommended_fluids;
		this.refrigerants[i] = refrigerants;
		this.lubrificants[i] = lubrificants;
		this.tube[i] = tube;
		this.reinforcement[i] = reinforcement;
		this.cover[i] = cover;
		this.applicable_specs[i] = applicable_specs;
		this.type_approvals[i] = type_approvals;
		this.other_technical_notes[i] = other_technical_notes;
		this.standard_Packaging[i] = standard_Packaging;
		this.catalogue_small_exclamation[i] = catalogue_small_exclamation;
		this.catalogue_big_exclamation[i] = catalogue_big_exclamation;
		this.catalogue_bottom_exclamation[i] = catalogue_bottom_exclamation;
		this.version[i] = version;
		this.version_date[i] = version_date;
	}

}
