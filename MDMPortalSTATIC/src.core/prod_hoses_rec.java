/*
 * Created on 19-dic-2008
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
/**
 * @author Administrator
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class prod_hoses_rec {

	/**
	 * @param fld01String
	 * @param fld02Float
	 * @param fld03String
	 * @param fld04String
	 * @param fld05Float
	 * @param fld06Float
	 * @param fld07Float
	 * @param fld08Float
	 * @param fld09Float
	 * @param fld10Float
	 * @param fld11Float
	 * @param fld12Float
	 * @param fld13Float
	 * @param fld14Float
	 * @param fld15Float
	 * @param fld16Float
	 * @param fld17String
	 * @param fld18String
	 * @param fld19String
	 * @param fld20Float
	 * @param fld21Float
	 * @param fld22Float
	 * @param fld23Float
	 * @param fld24Float
	 * @param fld25Float
	 * @param fld26Float
	 * @param fld27Float
	 * @param fld28Float
	 * @param fld29Float
	 * @param fld30Float
	 * @param fld31Float
	 * @param fld32Float
	 * @param fld33Float
	 */
	public prod_hoses_rec(  String field01, float  field02, String field03, String field04, float field05, 
							float  field06, float  field07, float  field08, float  field09, float field10,
							float  field11, float  field12, float  field13, float  field14, float field15,
							float  field16, String field17, String field18, String field19, float field20,
							float  field21, float  field22, float  field23, float  field24, float field25,
							float  field26, float  field27, float  field28, float  field29, float field30,
							float  field31, float  field32, float  field33, String field34, String field35,
							String field36, String field37, String field38, String field39, String field40, 
							String field41, String field42, String field43) {
								
		this.field01 = field01; 
		this.field02 = field02;
		this.field03 = field03;
		this.field04 = field04;
		this.field05 = field05;
		this.field06 = field06;
		this.field07 = field07;
		this.field08 = field08;
		this.field09 = field09;
		this.field10 = field10;
		this.field11 = field11;
		this.field12 = field12;
		this.field13 = field13;
		this.field14 = field14;
		this.field15 = field15;
		this.field16 = field16;
		this.field17 = field17;
		this.field18 = field18;
		this.field19 = field19;
		this.field20 = field20;
		this.field21 = field21;
		this.field22 = field22;
		this.field23 = field23;
		this.field24 = field24;
		this.field25 = field25;
		this.field26 = field26;
		this.field27 = field27;
		this.field28 = field28;
		this.field29 = field29;
		this.field30 = field30;
		this.field31 = field31;
		this.field32 = field32;
		this.field33 = field33;
		this.field34 = field34;
		this.field35 = field35;
		this.field36 = field36;
		this.field37 = field37;
		this.field38 = field38;
		this.field39 = field39;
		this.field40 = field40;
		this.field41 = field41;
		this.field42 = field42;
		this.field43 = field43;
		// TODO Auto-generated constructor stub
	}
	private String field01;
	private float  field02;
	private String field03;
	private String field04;
	private float field05;
	private float field06;
	private float field07;
	private float field08;
	private float field09;
	private float field10;
	private float field11;
	private float field12;
	private float field13;
	private float field14;
	private float field15;
	private float field16;
	private String field17;
	private String field18;
	private String field19;
	private float  field20;
	private float  field21;
	private float  field22;
	private float  field23;
	private float  field24;
	private float  field25;
	private float  field26;
	private float  field27;
	private float  field28;
	private float  field29;
	private float  field30;
	private float  field31;
	private float  field32;
	private float  field33;
	private String  field34;
	private String  field35;
	private String  field36;
	private String  field37;
	private String  field38;
	private	String	field39;
	private	String	field40;
	private	String	field41;
	private	String	field42;
	private	String	field43;

	/**
	 * @return
	 */
	public String getField01() {
		return field01;
	}

	/**
	 * @return
	 */
	public float getField02() {
		return field02;
	}

	/**
	 * @return
	 */
	public String getField03() {
		return field03;
	}

	/**
	 * @return
	 */
	public String getField04() {
		return field04;
	}

	/**
	 * @return
	 */
	public float getField05() {
		return field05;
	}

	/**
	 * @return
	 */
	public float getField06() {
		return field06;
	}

	/**
	 * @return
	 */
	public float getField07() {
		return field07;
	}

	/**
	 * @return
	 */
	public float getField08() {
		return field08;
	}

	/**
	 * @return
	 */
	public float getField09() {
		return field09;
	}

	/**
	 * @return
	 */
	public float getField10() {
		return field10;
	}

	/**
	 * @return
	 */
	public float getField11() {
		return field11;
	}

	/**
	 * @return
	 */
	public float getField12() {
		return field12;
	}

	/**
	 * @return
	 */
	public float getField13() {
		return field13;
	}

	/**
	 * @return
	 */
	public float getField14() {
		return field14;
	}

	/**
	 * @return
	 */
	public float getField15() {
		return field15;
	}

	/**
	 * @return
	 */
	public float getField16() {
		return field16;
	}

	/**
	 * @return
	 */
	public String getField17() {
		return field17;
	}
	/**
	 * @return
	 */
	public String getField18() {
		return field18;
	}
	/**
	 * @return
	 */
	public String getField19() {
		return field19;
	}
	/**
	 * @return
	 */
	public float getField20() {
		return field20;
	}
	/**
	 * @return
	 */
	public float getField21() {
		return field21;
	}
	/**
	 * @return
	 */
	public float getField22() {
		return field22;
	}
	/**
	 * @return
	 */
	public float getField23() {
		return field23;
	}
	/**
	 * @return
	 */
	public float getField24() {
		return field24;
	}
	/**
	 * @return
	 */
	public float getField25() {
		return field25;
	}
	/**
	 * @return
	 */
	public float getField26() {
		return field26;
	}
	/**
	 * @return
	 */
	public float getField27() {
		return field27;
	}
	/**
	 * @return
	 */
	public float getField28() {
		return field28;
	}
	/**
	 * @return
	 */
	public float getField29() {
		return field29;
	}
	/**
	 * @return
	 */
	public float getField30() {
		return field30;
	}
	/**
	 * @return
	 */
	public float getField31() {
		return field31;
	}
	/**
	 * @return
	 */
	public float getField32() {
		return field32;
	}
	/**
	 * @return
	 */
	public float getField33() {
		return field33;
	}

	public String getField34() {
			return field34;
		}

	public String getField35() {
				return field35;
			}
	public String getField36() {
				return field36;
			}
	public String getField37() {
				return field37;
			}
	public String getField38() {
				return field38;
			}
	public String getField39() {
				return field39;
			}
	public String getField40() {
					return field40;
				}
	public String getField41() {
					return field41;
				}
	public String getField42() {
					return field42;
				}
	public String getField43() {
					return field43;
				}



	/**
	 * @param string
	 */
	public void setField01(String string) {
		field01 = string;
	}

	/**
	 * @param string
	 */
	public void setField02(float string) {
		field02 = string;
	}

	/**
	 * @param string
	 */
	public void setField03(String string) {
		field03 = string;
	}

	/**
	 * @param string
	 */
	public void setField04(String string) {
		field04 = string;
	}

	/**
	 * @param string
	 */
	public void setField05(float string) {
		field05 = string;
	}

	/**
	 * @param string
	 */
	public void setField06(float string) {
		field06 = string;
	}

	/**
	 * @param string
	 */
	public void setField07(float string) {
		field07 = string;
	}

	/**
	 * @param string
	 */
	public void setField08(float string) {
		field08 = string;
	}

	/**
	 * @param string
	 */
	public void setField09(float string) {
		field09 = string;
	}

	/**
	 * @param string
	 */
	public void setField10(float string) {
		field10 = string;
	}

	/**
	 * @param string
	 */
	public void setField11(float string) {
		field11 = string;
	}

	/**
	 * @param string
	 */
	public void setField12(float string) {
		field12 = string;
	}

	/**
	 * @param string
	 */
	public void setField13(float string) {
		field13 = string;
	}

	/**
	 * @param string
	 */
	public void setField14(float string) {
		field14 = string;
	}

	/**
	 * @param string
	 */
	public void setField15(float string) {
		field15 = string;
	}

	/**
	 * @param string
	 */
	public void setField16(float string) {
		field16 = string;
	}

	/**
	 * @param string
	 */
	public void setField17(String string) {
		field17 = string;
	}

	/**
	 * @param string
	 */
	public void setField18(String string) {
		field18 = string;
	}

	/**
	 * @param string
	 */
	public void setField19(String string) {
		field19 = string;
	}

	/**
	 * @param string
	 */
	public void setField20(float string) {
		field20 = string;
	}

	/**
	 * @param string
	 */
	public void setField21(float string) {
		field21 = string;
	}

	/**
	 * @param string
	 */
	public void setField22(float string) {
		field22 = string;
	}

	/**
	 * @param string
	 */
	public void setField23(float string) {
		field23 = string;
	}

	/**
	 * @param string
	 */
	public void setField24(float string) {
		field24 = string;
	}

	/**
	 * @param string
	 */
	public void setField25(float string) {
		field25 = string;
	}

	/**
	 * @param string
	 */
	public void setField26(float string) {
		field26 = string;
	}

	/**
	 * @param string
	 */
	public void setField27(float string) {
		field27 = string;
	}

	/**
	 * @param string
	 */
	public void setField28(float string) {
		field28 = string;
	}

	/**
	 * @param string
	 */
	public void setField29(float string) {
		field29 = string;
	}

	/**
	 * @param string
	 */
	public void setField30(float string) {
		field30 = string;
	}

	/**
	 * @param string
	 */
	public void setField31(float string) {
		field31 = string;
	}

	/**
	 * @param string
	 */
	public void setField32(float string) {
		field32 = string;
	}

	/**
	 * @param string
	 */
	public void setField33(float string) {
		field33 = string;
	}
	public void setField34(String string) {
			field34 = string;
		}
	public void setField35(String string) {
			field35 = string;
		}
	public void setField36(String string) {
			field36 = string;
		}
	public void setField37(String string) {
			field37 = string;
		}
	public void setField38(String string) {
			field38 = string;
		}
	public void setField39(String string) {
			field39 = string;
		}

	public int compareTo(prod_hoses_rec p){
	  if (getField01() == p.getField01())
		return 0;
	  else if (getField01().compareTo(p.getField01()) > 0)
		return 1;
	  else
		return -1;
	} 
	public int orderByLine(prod_hoses_rec p){
	  if (getField01() == p.getField01())
		return 0;
	  else if (getField01().compareTo(p.getField01()) > 0)
		return 1;
	  else
		return -1;
	} 
	public int orderByDN(prod_hoses_rec p){
	  if (getField02() == p.getField02())
		return 0;
	  else if (getField02() > p.getField02())
		return 1;
	  else
		return -1;
	}
}
