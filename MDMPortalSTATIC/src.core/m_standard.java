public class m_standard {

	public m_standard(
		String field01,
		String field02,
		String field03,
		String field04,
		String field05,
		String field06,
		String field07)
		 {

		this.field01 = field01;
		this.field02 = field02;
		this.field03 = field03;
		this.field04 = field04;
		this.field05 = field05;
		this.field06 = field06;
		this.field07 = field07;

		// TODO Auto-generated constructor stub
	}
	private String field01;
	private String field02;
	private String field03;
	private String field04;
	private String field05;
	private String field06;
	private String field07;

	public String getField01() {
		return field01;
	}
	public String getField02() {
		return field02;
	}
	public String getField03() {
		return field03;
	}
	public String getField04() {
		return field04;
	}
	public String getField05() {
		return field05;
	}
	public String getField06() {
		return field06;
	}
	public String getField07() {
		return field07;
	}

	public void setField01(String string) {
		field01 = string;
	}
	public void setField02(String string) {
		field02 = string;
	}
	public void setField03(String string) {
		field03 = string;
	}
	public void setField04(String string) {
		field04 = string;
	}
	public void setField05(String string) {
		field05 = string;
	}
	public void setField06(String string) {
		field06 = string;
	}
	public void setField07(String string) {
		field07 = string;
	}

	public int sortByFamilyStandard(m_standard p){
	  if (getField01() == p.getField01())
		return 0;
		else if (getField01().compareTo(p.getField01()) > 0)
		return 1;
	  else
		return -1;
	} 
}
