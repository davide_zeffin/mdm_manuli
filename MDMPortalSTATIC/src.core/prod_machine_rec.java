public class prod_machine_rec {

	public prod_machine_rec(
		String field00,
		String field01,
		String field02,
		String field03,
		String field04,
		String field05,
		String field06) {

		this.field00 = field00;
		this.field01 = field01;
		this.field02 = field02;
		this.field03 = field03;
		this.field04 = field04;
		this.field05 = field05;
		this.field06 = field06;

		// TODO Auto-generated constructor stub
	}
	private String field00;
	private String field01;
	private String field02;
	private String field03;
	private String field04;
	private String field05;
	private String field06;

	public String getField00() {
		return field00;
	}
	public String getField01() {
		return field01;
	}
	public String getField02() {
		return field02;
	}
	public String getField03() {
		return field03;
	}
	public String getField04() {
		return field04;
	}
	public String getField05() {
		return field05;
	}
	public String getField06() {
		return field06;
	}

	public void setField00(String string) {
		field00 = string;
	}
	public void setField01(String string) {
		field01 = string;
	}
	public void setField02(String string) {
		field02 = string;
	}
	public void setField03(String string) {
		field03 = string;
	}
	public void setField04(String string) {
		field04 = string;
	}
	public void setField05(String string) {
		field05 = string;
	}
	public void setField06(String string) {
		field06 = string;
	}
	public int sortByNameMachine(prod_machine_rec p){
	  if (getField01() == p.getField01())
		return 0;
		else if (getField01().compareTo(p.getField01()) > 0)
		return 1;
	  else
		return -1;
	} 
	public int sortByVoltagesMachine(prod_machine_rec p){
	  if (getField00() == p.getField00())
		return 0;
		else if (getField00().compareTo(p.getField00()) > 0)
		return 1;
	  else
		return -1;
	} 
	
}
