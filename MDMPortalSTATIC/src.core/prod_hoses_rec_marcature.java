/*
 * Created on 19-dic-2008
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
/**
 * @author Administrator
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class prod_hoses_rec_marcature {

	/**
	 * @param fld34String
	 * @param fld37String
	 * @param imagePath
	 * @param fld38String
	 * @param fld41String
	 * @param fld42String
	 * @param fld40String
	 * @param fld01String
	 * @param fld02Float
	 * @param fld03String
	 * @param fld04String
	 * @param fld43String
	 */

	public prod_hoses_rec_marcature(String  field01, String  field02, String  field03, String field04,  String field05, 
									String  field06, String  field07, String  field08, float  field09,  String field10,
									String  field11, String  field12) {
								
		this.field01 = field01; 
		this.field02 = field02;
		this.field03 = field03;
		this.field04 = field04;
		this.field05 = field05;
		this.field06 = field06;
		this.field07 = field07;
		this.field08 = field08;
		this.field09 = field09;
		this.field10 = field10;
		this.field11 = field11;
		this.field12 = field12;
		
		// TODO Auto-generated constructor stub
	}
	private String  field01;
	private String  field02;
	private String  field03;
	private String  field04;
	private String  field05;
	private String  field06;
	private String  field07;
	private String  field08;
	private float   field09;
	private String  field10;
	private String  field11;
	private String  field12;
	/**
	 * @return
	 */
	public String getField01() {
		return field01;
	}

	/**
	 * @return
	 */
	public String getField02() {
		return field02;
	}

	/**
	 * @return
	 */
	public String getField03() {
		return field03;
	}

	/**
	 * @return
	 */
	public String getField04() {
		return field04;
	}

	/**
	 * @return
	 */
	public String getField05() {
		return field05;
	}

	/**
	 * @return
	 */
	public String getField06() {
		return field06;
	}

	/**
	 * @return
	 */
	public String getField07() {
		return field07;
	}

	/**
	 * @return
	 */
	public String getField08() {
		return field08;
	}

	/**
	 * @return
	 */
	public float getField09() {
		return field09;
	}

	/**
	 * @return
	 */
	public String getField10() {
		return field10;
	}

	/**
	 * @return
	 */
	public String getField11() {
		return field11;
	}

	/**
	 * @return
	 */
	public String getField12() {
		return field12;
	}






	/**
	 * @param string
	 */
	public void setField01(String string) {
		field01 = string;
	}

	/**
	 * @param string
	 */
	public void setField02(String string) {
		field02 = string;
	}

	/**
	 * @param string
	 */
	public void setField03(String string) {
		field03 = string;
	}

	/**
	 * @param string
	 */
	public void setField04(String string) {
		field04 = string;
	}

	/**
	 * @param string
	 */
	public void setField05(String string) {
		field05 = string;
	}

	/**
	 * @param string
	 */
	public void setField06(String string) {
		field06 = string;
	}

	/**
	 * @param string
	 */
	public void setField07(String string) {
		field07 = string;
	}

	/**
	 * @param string
	 */
	public void setField08(String string) {
		field08 = string;
	}

	/**
	 * @param string
	 */
	public void setField09(float string) {
		field09 = string;
	}

	/**
	 * @param string
	 */
	public void setField10(String string) {
		field10 = string;
	}

	/**
	 * @param string
	 */
	public void setField11(String string) {
		field11 = string;
	}

	/**
	 * @param string
	 */
	public void setField12(String string) {
		field12 = string;
	}
/*
	public int compareTo(prod_hoses_rec_marcature p){
	  if (getField01() == p.getField01())
		return 0;
	  else if (getField01().compareTo(p.getField01()) > 0)
		return 1;
	  else
		return -1;
	} 
	*/
	public int orderByLine(prod_hoses_rec_marcature p){
	  if (getField01() == p.getField01() && getField08() == p.getField08())
		return 0;
	  else if (getField01().compareTo(p.getField01()) > 0 && getField08().compareTo(p.getField08()) > 0)
		return 1;
	  else
		return -1;
	} 

	public int orderByDN(prod_hoses_rec_marcature p){
		String fld3 = Float.toString(getField09() + 10000);  
		String fld4 = Float.toString(p.getField09() + 10000);  
		String fld1 = getField01() + fld3 + getField08();
		String fld2 = p.getField01() + fld4 + p.getField08();
	  if (fld1 == fld2)
		return 0;
	  else if (fld1.compareTo(fld2) > 0 )
		return 1;
	  else
		return -1;
	}
}
