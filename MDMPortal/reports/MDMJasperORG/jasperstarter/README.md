
JasperStarter - Running JasperReports from command line
--------------------------------------------------------

JasperStarter is an opensource command line launcher and batch compiler for
[JasperReports][].

It has the following features:

  * Run any JasperReport that needs a jdbc datasource or empty datasource
  * Use with any database for which a jdbc driver is available
  * Execute reports that need runtime parameters. The following parameter types
    are supported:
    * string, int, double, date, image (see usage)
  * Print directly to system default or given printer
  * Optionally show printer dialog to choose printer
  * Optionally Show printpreview
  * Export to file in the following formats:
    * pdf, rtf, xls, xlsx, docx, odt, ods, pptx, csv, html, xhtml, xml, jrprint
  * Export multiple formats in one commanding call
  * Compile, Print and export in one commanding call
  * View, print or export previously filled reports (use jrprint file as input)
  * Can compile a whole directory of .jrxml files.
  * Integrate in non Java applications (for example PHP, Python)
  * Binary executable on Windows
  * Includes JasperReports so this is the only tool you need to install

Requirements

  * Java 1.6 or higher.
  * A JDBC 2.1 driver for your database


### Quickstart

  * Download JasperStarter from [Sourceforge][]
  * Extract the distribution archive to any directory on your system.
  * Add the _./bin_ directoy of your installation to your searchpath.

  * or just invoke _setup.exe_ on Windows

  * Put your jdbc drivers in the _./jdbc_ directory of your installation or
    use _\--jdbc-dir_ to point to a different directory.

Invoke JasperStarter with _\-h_ to get an overview:

    $ jasperstarter -h

Invoke JasperStarter with _pr \-h_ to get help on the process command:

    $ jasperstarter pr -h

Example with reportparameters:

    $ jasperstarter pr -t mysql -u myuser -f pdf -H myhost -n mydb -i report.jasper \
    -o report -p secret -P CustomerNo=string:10 StartFrom=date:2012-10-01

Example with hsql using database type generic:

    $ jasperstarter pr -t generic -f pdf -i report.jasper -o report -u sa \
    --db-driver org.hsqldb.jdbcDriver \
    --db-url jdbc:hsqldb:hsql://localhost

For more information take a look in the docs directory of the distibution
archive or read the [Usage][] page online.


### Release Notes

  * JasperStarter now has commands. Every command can have different arguments
    and options. Users of previous versions just must add **pr** to their
    existing command line.
  * New command: cp - compile, can compile one file or all .jrxml in a
    directory.
  * New input file types for command pr allowed:
    * jrxml    - compiles implicit
    * jrprint  - print, view or export previously filled reports.
  * New output type: jrprint. This makes \--keep obsolete.
  * New parameter -w writes compiled file to imput dir if jrxml is
    processed.
  * Parameter -t defaults to "none" and can therefore be omited if no
    database is needed.
  * Input file is read once. No temporary files needed anymore.
  * Setup checks for previous versions and creates menuitems for uninstall
    and help.
  * Setup is available in English, German, French, Polish, Romanian, Ukrainian.

See CHANGES file for a history of changes.


#### Known Bugs

For upcoming issues see [Issues][]


### Feedback

Feedback is always welcome! If you have any questions or proposals, don't
hesitate to write to our [discussion][] forum.
If you found a bug or you are missing a feature, log into our [Issuetracker][]
and create a bug or feature request.

If you like the software you can write a [review][] :-)


### Developement

The sourcecode is available at [bitbucket.org/cenote/jasperstarter][], the
project website is hosted at [Sourceforge][].

JasperStarter is build with [Maven][]. To get a distribution package run:

    $ mvn package -P release

or if you build from the current default branch you better use:

    $ mvn package -P release,snapshot

**Attention! You cannot execute** `target/jasperstarter.jar`
**without having it\'s dependencies in** `../lib` ! See **dev** profile below!

If you want to build the Windows setup.exe, you need to have _nsis_ in your
search path (works on linux too, you can find a compiled release in the 
sourceforge download folder _build-tools_ for your convenience)
an add the **windows-setup** profile to your build:

    $ mvn package -P release,windows-setup

or

    $ mvn package -P release,windows-setup,snapshot

While developing you may want to have a quicker build. The **dev** profile
excludes some long running reports and the compressed archives. Instead it puts
the build result into _target/jasperstarter-dev-bin_.

    $ mvn package -P dev

Now you can execute JasperStarter without IDE:

    $ target/jasperstarter-dev-bin/bin/jasperstarter

or

    $ java -jar target/jasperstarter-dev-bin/lib/jasperstarter.jar

To run JasperStarter from within your IDE add _\--jdbc-dir jdbc_ to the argument
list of your run configuration. Otherwise you will get an error:

    Error, (...)/JasperStarter/target/classes/jdbc is not a directory!

Put your jdbc drivers in the _./jdbc_ directory of the project to invoke
JasperStarter from within your IDE to call up a database based report.


### License

Copyright 2012 Cenote GmbH.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

[JasperReports]:http://community.jaspersoft.com/project/jasperreports-library
[Maven]:http://maven.apache.org/
[Sourceforge]:http://sourceforge.net/projects/jasperstarter/
[bitbucket.org/cenote/jasperstarter]:http://bitbucket.org/cenote/jasperstarter
[review]:http://sourceforge.net/projects/jasperstarter/reviews
[discussion]:http://sourceforge.net/p/jasperstarter/discussion/
[Issuetracker]:https://cenote-issues.atlassian.net/browse/JAS
[Usage]:http://jasperstarter.sourceforge.net/usage.html
[Issues]:https://cenote-issues.atlassian.net/browse/JAS
