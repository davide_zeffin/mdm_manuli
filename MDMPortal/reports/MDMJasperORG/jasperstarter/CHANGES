
JasperStarter - Running JasperReports from command line
========================================================

Changes
--------

 1.0.1  [JAS-18] - Unable to save output into Excel format

 1.0.0
        JasperStarter now has commands: pr - process, lp - list printers.
        New command: cp - compile, can compile one file or all .jrxml in a
        directory.
        New input file types for command pr allowed:
          jrxml    - compiles implicit
          jrprint  - print, view or export previously filled reports.
        New output type: jrprint. This makes --keep obsolete.
        New parameter -w writes compiled file to imput dir if jrxml is
        processed.
        Parameter -t defaults to "none" and can therefore be omited if no
        database is needed.
        Input file is read once. No temporary files needed anymore.
        Setup checks for previous versions and creates menuitems for uninstall
        and help.
        Setup is available in English, Chinese (Simplified), Czech, French,
        Hungarian, German, Polish, Romanian, Thai, Ukrainian.
        [JAS-2] - runtime parameter value cannot contain equal sign
        Contains JasperReports 5.0.1
        German translation for Site/docs
        [JAS-4] - java.lang.Integer cannot be cast to java.lang.String
        [JAS-8] - java.lang.String cannot be cast to java.lang.Integer
        [JAS-9] - Exception in thread "main" java.lang.IllegalArgumentException:
                  URI has an authority component

 0.10.0 New report parameter types: double, image (see usage).
        New supported export formats: xls, xlsx, csv, ods, pptx, xhtml, xml.
        Windows setup available.
        --version shows included JasperReports version.
        Fixed some minor bugs.

V 0.9.1 Bugfix release fixed problems with --jdbc-dir option.

V 0.9.0 First public release
        Switched from Commons CLI to argparse4j.
        Project documentation in generated site.
        README uses markdown syntay, renamed to README.md.
        Applied Apache License 2.0 to the software.
        JasperStarter now starts via executable files in ./bin.
        Windows binary jasperstarter.exe is generated with launch4j.

V 0.8.0 Switched to maven.

V 0.7.1 Fixed issue: duplicated option -n

V 0.7.0 new option --set-report-name to temporary change the reportname when
        printing. This is useful if you want to change the printjob name for
        printing to a pdf printer like cups-pfd which uses the document name as
        part of the pdf name by default.

V 0.6.0 new options --printer-name --with-print-dialog --list-printers
        printername matches .toLowercase().startWith() and spaces can be escaped
        by the underline character _.
        print dialog and viewer appear in system look an feel.

V 0.5.0 support for postgres, oracle and generic jdbc
        password is no longer a required option except for oracle
        jrprint file is stored in system temp dir and deleted after processing
        new options --jdbc-dir, --debug, --keep-jrprint
        file extension .jasper is added to input if omitted
        output can be omitted or can be file or directory

V 0.4.0 jdbc drivers are loaded from jdbc dir
        new parameter: db-type: none, mysql (none provides JREmptyDataSource()
           for a non database report)
        support for barcode4j

V 0.3.1 Bugfix: removed jasperreports-javaflow
        added barbecue barcode lib

V 0.3.0 Print preview
        nicer help message
        package renamed
 
V 0.2.0 Print support added
        Added exportformats html, odt
        Added report parameter type date.
        New parameter db-name - database name

V 0.1.0 First working version
        Supports export to PDF, DOCX, RTF.
        Simple report parameters of type string and int.
