import java.awt.event.ActionEvent;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.PasswordAuthentication;
import java.net.URL;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.swing.JFrame;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRPrintPage;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import a2i.cache.CatalogCache;
import a2i.common.A2iResultSet;
import a2i.common.CatalogData;
import a2i.common.ResultSetDefinition;
import a2i.core.Measurement;
import a2i.core.StringException;
import a2i.generated.MeasurementManager;
import a2i.search.FreeFormParameter;
import a2i.search.FreeFormParameterField;
import a2i.search.FreeFormTableParameter;
import a2i.search.Search;

import com.sapportals.portal.prt.component.AbstractPortalComponent;
import com.sapportals.portal.prt.component.IPortalComponentRequest;
import com.sapportals.portal.prt.component.IPortalComponentResponse;

public class MDMJasper extends AbstractPortalComponent {
	private static Map parameters = new HashMap();
	private static Map parameters_00 = new HashMap();
	private static Map parameters_01 = new HashMap();
	private static Map parameters_02 = new HashMap();
	private static CatalogData catalogData;
	private static CatalogCache catalogCache;
	private static MeasurementManager measurementManager;
	private static String JASPER_FILENAME = "ManuliCatalogue";
	private static String JASPER_FILENAME_00 = "ManuliMachineFamilies";
	private static String JASPER_FILENAME_01 = "ManuliMachineCatalogue";
	private static String JASPER_FILENAME_02 = "ManuliMachineReference";
	//  PRODUZIONE
	//	private static String JASPER_REPORT_FOLDER = "E://MDMPortal//reports//";
	//	private static String CACHE_DIRECTORY = "E:\\MDMPortal\\reports\\";
	//	private static String CACHE_SERVER = "manepprd.sapman.dc";
	//	private static String CACHE_USER = "Admin";
	//	private static String CACHE_PASSWORD = "mdm prod";
	//	SVILUPPO
	private static String JASPER_REPORT_FOLDER = "F://MDMPortal//reports//";
	private static String CACHE_DIRECTORY = "F:\\MDMPortal\\reports\\";
	private static String CACHE_SERVER = "manepsvi.sapman.dc";
	private static String CACHE_USER = "Admin";
	private static String CACHE_PASSWORD = "admin";

	private static int CACHE_PORT = 2345;
	private static String STR1 = ", ;";
	private static String STR2 = ",";
	private static boolean getRecords = false;
	private static String strWatermark = null;
	/*****************************************************************/
	/***           variabili per stampa Catalogue                  ***/
	/*****************************************************************/
	private static String Name;
	private static String catalogue_subtitle;
	private static String imagePath;
	private static String key_performance;
	private static String applications_fluids;
	private static String main_application;
	private static String min_cont_serv_temp;
	private static String max_cont_serv_temp;
	private static String cont_serv_temp_far;
	private static String cont_serv_temp_cel;
	private static String max_operating_temp;
	private static String min_external_temp;
	private static String recommended_fluids;
	private static String refrigerants;
	private static String lubrificants;
	private static String tube;
	private static String reinforcement;
	private static String cover;
	private static String applicable_specs;
	private static String type_approvals;
	private static String other_technical_notes;
	private static String standard_Packaging;
	private static String catalogue_small_exclamation;
	private static String catalogue_big_exclamation;
	private static String catalogue_bottom_exclamation;
	private static String version;
	private static String version_date;

	/*****************************************************************/
	/***           variabili per stampa crimping                   ***/
	/*****************************************************************/
	private static String family;
	private static String release_number;
	private static String date;
	private static String crimp_version;
	private static String general;
	private static String skive_tool;
	private static String crimp_tool;
	private static String remarks;
	private static String title = "";
	private static String titleCol1 = "Internal Skive";
	private static String titleCol2 = "External Skive";
	private static String titleCol3 = "Crimp.Diam.";

	/*****************************************************************/
	/***    variabili per stampa Catalogue Extended	               ***/
	/*****************************************************************/
	private static String hose_inside_diameter_min_mm;
	private static String hose_inside_diameter_min_inches;
	private static String hose_inside_diameter_max_mm;
	private static String hose_inside_diameter_max_inches;
	private static String reinforcement_outside_diameter_min_mm;
	private static String reinforcement_outside_diameter_min_inches;
	private static String reinforcement_outside_diameter_max_mm;
	private static String reinforcement_outside_diameter_max_inches;
	private static String hose_outside_diameter_min_mm;
	private static String hose_outside_diameter_min_inches;
	private static String hose_outside_diameter_max_mm;
	private static String hose_outside_diameter_max_inches;
	private static String hose_max_wall_thickness_differences_mm;
	private static String hose_max_wall_thickness_differences_inches;

	/*****************************************************************/
	/***    variabili per stampa Marcature      	               ***/
	/*****************************************************************/
	private static String brand_pack;
	private static String brand_customer;
	private static String marking_technology;
	private static String brand_color;
	private static String brand_logo;
	private static String brand_notes;
	private static String brand_data_type;
	private static String department;
	private static String brand;

	private static String paramTableHeader = "prod_hoses";
	private static String paramTableItems;
	private static String paramValue;
	private static String paramFamily;
	private static String paramPartNumber;
	private static String paramReferences;
	private static String familyReferences;
	private static String paramCatalog = "false";
	private static String paramCategory = "false";
	private static String paramBrand = "";
	private static String paramType = "1";
	private static String paramStandard = "01";
	private static String paramAlternative = "02";
	private static String paramTableName = "";
	private static String paramTableFamily = "";
	private static String paramCrimpVersion = "";
	private static String paramVersion = "";
	private static String paramCrimpingFlag = "";
	private static String paramHoseFamilyFlag = "";
	private static String paramRestrictedFlag = "";
	private static String paramTitleBooklet = "";
	private static String paramTitleDatasheet = "";
	private static String paramDatePubblication = "";
	private static String dateRelease = "";
	private static String flag_PI = "";
	
	private static boolean fld25Boolean = true;
	private static boolean fld26Boolean = true;
	private static boolean fld52Boolean = true;

	private static String fld01String;
	private static String fld02String;
	private static String fld03String;
	private static String fld04String;
	private static String fld05String;
	private static String fld06String;
	private static String fld07String;
	private static String fld08String;
	private static String fld09String;
	private static String fld10String;
	private static String fld11String;
	private static String fld12String;
	private static String fld13String;
	private static String fld14String;
	private static String fld15String;
	private static String fld16String;
	private static String fld17String;
	private static String fld18String;
	private static String fld19String;
	private static String fld20String;
	private static String fld21String;
	private static String fld22String;
	private static String fld23String;
	private static String fld24String;
	private static String fld25String;
	private static String fld26String;
	private static String fld27String;
	private static String fld28String;
	private static String fld29String;
	private static String fld30String;
	private static String fld31String;
	private static String fld32String;
	private static String fld33String;
	private static String fld34String;
	private static String fld35String;
	private static String fld36String;
	private static String fld37String;
	private static String fld38String;
	private static String fld39String;
	private static String fld40String;
	private static String fld41String;
	private static String fld42String;
	private static String fld43String;
	private static String fld44String;
	private static String fld45String;
	private static String fld46String;
	private static String fld47String;
	private static String fld48String;
	private static String fld49String;
	private static String fld50String;
	private static String fld51String;
	private static String fld52String;
	private static String fld53String;
	private static String fld54String;
	private static String fld55String;
	private static String fld56String;
	private static String fld57String;
	private static String fld58String;

	private static String fld59String;
	private static String fld60String;
	private static String fld61String;
	private static String fld62String;
	private static String fld63String;
	private static String fld64String;
	private static String fld65String;
	private static String fld66String;

	private static String fldS01String;
	private static String fldS02String;
	private static String fldS03String;
	private static String fldS04String;
	private static String fldS05String;
	private static String fldS06String;
	private static String fldS07String;

	private static float fldS03Float;
	private static float fldS05Float;

	private static String fldM01String;
	private static String fldM02String;
	private static String fldM03String;
	private static String fldM04String;
	private static String fldM05String;
	private static String fldM06String;

	private static float fld02Float;
	private static float fld05Float;
	private static float fld06Float;
	private static float fld07Float;
	private static float fld08Float;
	private static float fld09Float;
	private static float fld10Float;
	private static float fld11Float;
	private static float fld12Float;
	private static float fld13Float = 0;
	private static float fld14Float = 0;
	private static float fld15Float = 0;
	private static float fld16Float = 0;
	private static float fld17Float = 0;
	private static float fld18Float = 0;
	private static float fld19Float = 0;
	private static float fld20Float = 0;
	private static float fld21Float;
	private static float fld22Float;
	private static float fld23Float;
	private static float fld24Float;
	private static float fld25Float;
	private static float fld26Float;
	private static float fld27Float;
	private static float fld28Float;
	private static float fld29Float;
	private static float fld30Float;
	private static float fld31Float;
	private static float fld32Float;
	private static float fld33Float;
	private static float fld34Float;
	private static float fld35Float;
	private static float fld36Float;
	private static float fld37Float;
	private static float fld38Float;
	private static float fld39Float;
	private static float fld40Float;
	private static float fld41Float;
	private static float fld42Float;
	private static float fld43Float;
	private static float fld44Float;
	private static float fld45Float;
	private static float fld46Float;
	private static float fld47Float;
	private static float fld48Float;

	private static String head1;
	private static String head2;
	private static String col1;
	private static String col2;
	private static String col3;
	private static String col4;

	private static String footer_left;
	private static String footer_central;
	private static String footer_right;
	static String addrow = "";
	private Vector recsMachine = new Vector();
	private Vector recsMachineNew = new Vector();
	private Vector recsMachineRec = new Vector();
	private Vector recsMachineRecNew = new Vector();
	private Vector recsOption = new Vector();
	private Vector recsOptionNew = new Vector();
	private Vector recsDieSets = new Vector();
	private Vector recsDieSetsNew = new Vector();
	private Vector recsAvailAcc = new Vector();
	private Vector recsAvailAccNew = new Vector();
	private Vector recsStandard = new Vector();
	private Vector recsStandardNew = new Vector();
	private Vector recsSelection = new Vector();
	private Vector recs = new Vector();
	private Vector recsNew = new Vector();
	private Vector recsApp = new Vector();
	private Vector recsTit = new Vector();
	private String DynamicParameter;

	public void doContent(
		IPortalComponentRequest request,
		IPortalComponentResponse response) {
		catalogData = new CatalogData();
		//		inizializzazione variabili		
		recsMachine = new Vector();
		recsMachineNew = new Vector();
		recsMachineRec = new Vector();
		recsMachineRecNew = new Vector();
		recsOption = new Vector();
		recsOptionNew = new Vector();
		recsDieSets = new Vector();
		recsDieSetsNew = new Vector();
		recsAvailAcc = new Vector();
		recsAvailAccNew = new Vector();
		recsStandard = new Vector();
		recsStandardNew = new Vector();
		recsSelection = new Vector();
		recs = new Vector();
		recsNew = new Vector();
		recsApp = new Vector();
		recsTit = new Vector();
		DynamicParameter = null;
		parameters = new HashMap();
		parameters_00 = new HashMap();
		parameters_01 = new HashMap();
		parameters_02 = new HashMap();

		mdmlogin(request, response);
		//DynamicParameter =-stampa=1&table=prod_hoses_rec&Name=ADLER/2&brand_pack=0906&IS_NOT_NULL(Tree)=&catalogo=false&category=false-

		try {
			DynamicParameter = request.getParameter("DynamicParameter");
			int indexType = DynamicParameter.indexOf("stampa=");
			int indexTable = DynamicParameter.indexOf("table=");
			paramType =
				DynamicParameter.substring(indexType + 7, indexTable - 1);
			/*****************************************************************/
			/***                           MACHINE                         ***/
			/*****************************************************************/
			if (paramType.equals("10") || paramType.equals("11")) {
				//DynamicParameter = stampa=11&table=prod_machine_rec&Name=MS 480&IS_NOT_NULL(Tree)=&catalog=true&family=true&sendPI=false
								response.write("DynamicParameter = " + DynamicParameter);

				int indexItems = DynamicParameter.indexOf("table=");
				int indexValue = DynamicParameter.indexOf("Name=");
				int indexPartNumber = DynamicParameter.indexOf("PartNumber=");
				int indexNull = DynamicParameter.indexOf("IS_NOT_NULL(Tree)=");
				int indexCatalog = DynamicParameter.indexOf("catalog=");
				int indexFamily = DynamicParameter.indexOf("family=");
				int indexReferences = DynamicParameter.indexOf("references=");
				int indexFlagPI = DynamicParameter.indexOf("sendPI=");
				paramTableItems =
					DynamicParameter.substring(indexItems + 6, indexValue - 1);
				paramValue =
					DynamicParameter.substring(
						indexValue + 5,
						indexPartNumber - 1);
				paramPartNumber =
					DynamicParameter.substring(
						indexPartNumber + 11,
						indexNull - 1);
				paramCatalog =
					DynamicParameter.substring(
						indexCatalog + 8,
						indexFamily - 1);
				paramFamily =
					DynamicParameter.substring(
						indexFamily + 7,
						indexReferences - 1);
				paramReferences =
					DynamicParameter.substring(indexReferences + 11);
//				flag_PI = 
//					DynamicParameter.substring(indexFlagPI);

				mdmGetMachine(request, response);
				mdmGetSortItems(request, response);
				mdmJSaddValues(request, response);
				
				if (recsMachineRec.size() == 0) {
					mdmGetMessage(request, response);
				} else {
					mdmJSprintValues(request, response);
					
				};
				mdmlogout(request, response);
				/*****************************************************************/
				/***                        CRIMPING                           ***/
				/*****************************************************************/
			} else if (paramType.equals("8") || paramType.equals("9")) {
				//DynamicParameter = -stampa=8&table=Crimp_Rec&Family=ADLER/1&Crimp_Version=STD2, Standard 2&version=1.0&crimping=true&hosefamily=true-

				int indexFamily = DynamicParameter.indexOf("Family=");
				int indexFlagPI = DynamicParameter.indexOf("sendPI=");
				int indexCrimpVersion =
					DynamicParameter.indexOf("Crimp_Version=");
				int indexVersion = DynamicParameter.indexOf("version=");
				int indexCrimpingFlag = DynamicParameter.indexOf("crimping=");
				int indexHoseFamilyFlag =
					DynamicParameter.indexOf("hosefamily=");
				int indexRestrictedFlag =
					DynamicParameter.indexOf("restricted=");

				int indexTitleBooklet =
					DynamicParameter.indexOf("title_booklet=");
				int indexTitleDatasheet =
					DynamicParameter.indexOf("title_datasheet=");
				int indexDatePubblication =
					DynamicParameter.indexOf("date_pubblication=");

				paramTableName =
					DynamicParameter.substring(indexTable + 6, indexFamily - 1);
				paramTableFamily =
					DynamicParameter.substring(
						indexFamily + 7,
						indexCrimpVersion - 1);
				paramCrimpVersion =
					DynamicParameter.substring(
						indexCrimpVersion + 14,
						indexCrimpingFlag - 1);
				int end = paramCrimpVersion.indexOf(",");
				paramCrimpVersion = paramCrimpVersion.substring(0, end);
				paramCrimpingFlag =
					DynamicParameter.substring(
						indexCrimpingFlag + 9,
						indexHoseFamilyFlag - 1);
				paramVersion =
					DynamicParameter.substring(
						indexVersion + 8,
						indexCrimpingFlag - 1);
				paramHoseFamilyFlag =
					DynamicParameter.substring(
						indexHoseFamilyFlag + 11,
						indexRestrictedFlag - 1);
				paramRestrictedFlag =
					DynamicParameter.substring(
						indexRestrictedFlag + 11,
						indexTitleBooklet - 1);

				paramTitleBooklet =
					DynamicParameter.substring(
						indexTitleBooklet + 14,
						indexTitleDatasheet - 1);
				paramTitleDatasheet =
					DynamicParameter.substring(
						indexTitleDatasheet + 16,
						indexDatePubblication - 1);
				paramDatePubblication =
					DynamicParameter.substring(indexDatePubblication + 18);
				flag_PI = 
					DynamicParameter.substring(indexFlagPI);
				mdmGetCrimpRec(request, response);
				mdmGetSortItems(request, response);
				mdmJSaddValues(request, response);
				if (recsNew.size() == 0) {
					mdmGetMessage(request, response);
				} else {
					mdmJSprintValues(request, response);
					
				};
				//mdmGetDataRecOutput(request, response);
				mdmlogout(request, response);

				/*****************************************************************/
				/***                        PROD HOSES                         ***/
				/*****************************************************************/
			} else {
				int indexItems = DynamicParameter.indexOf("table=");
				int indexValue = DynamicParameter.indexOf("Name=");
				int indexBrand = DynamicParameter.indexOf("brand_pack=");
				int indexNull = DynamicParameter.indexOf("IS_NOT_NULL(Tree)=");
				int indexCatalog = DynamicParameter.indexOf("catalogo=");
				int indexCategory = DynamicParameter.indexOf("category=");
				int indexStandard = DynamicParameter.indexOf("crimpings_std=");
				int indexAlternative =
					DynamicParameter.indexOf("crimpings_alt=");
				//paramTableHeader.substring(0,11);
				paramTableItems =
					DynamicParameter.substring(indexItems + 6, indexValue - 1);
				paramValue =
					DynamicParameter.substring(indexValue + 5, indexBrand - 1);
				paramBrand =
					DynamicParameter.substring(indexBrand + 11, indexNull - 1);
				paramCatalog =
					DynamicParameter.substring(
						indexCatalog + 9,
						indexCategory - 1);
				paramCategory =
					DynamicParameter.substring(
						indexCategory + 9,
						indexStandard - 1);
				paramStandard =
					DynamicParameter.substring(
						indexStandard + 14,
						indexAlternative - 1);
				paramAlternative =
					DynamicParameter.substring(indexAlternative + 14);

				mdmGetProdHoses(request, response);
				mdmGetProdHosesRec(request, response);
				mdmGetSortItems(request, response);
				mdmJSaddValues(request, response);
				if (recsNew.size() == 0) {
					mdmGetMessage(request, response);
				} else {
					mdmJSprintValues(request, response);
					//mdmGetDataOutput(request, response);
				};
				mdmlogout(request, response);
			}
		} catch (Exception e) {
			response.write("Please, select a printable item" + e.toString());
			// + e.getMessage());
		}

	}
	/*****************************************************************/
	/***                   Login MDM Server                        ***/
	/*****************************************************************/
	public void mdmlogin(
		IPortalComponentRequest request,
		IPortalComponentResponse response) {

		try {
			
			catalogData.Login(
				CACHE_SERVER,
				CACHE_PORT,
				CACHE_USER,
				CACHE_PASSWORD,
				"English [US]");
			measurementManager = catalogData.GetMeasurements();
		} catch (Exception e) {
			response.write("Error login: " + e.getMessage());
		}
	}
	/*****************************************************************/
	/***                  Logout MDM Serve                         ***/
	/*****************************************************************/
	public void mdmlogout(
		IPortalComponentRequest request,
		IPortalComponentResponse response) {

		try {
			catalogData.Logout();
		} catch (Exception e) {
			response.write("Error Logout: " + e.getMessage());
		}

	}
	/*****************************************************************/
	/***            open a new frame i.e window                    ***/
	/*****************************************************************/
	public void actionPerformed(ActionEvent e) {
		JFrame newFrame = new JFrame("New Window");
		newFrame.pack();
		newFrame.setVisible(true);
	}
	/*****************************************************************/
	/***             Retrieve data from prod_machine               ***/
	/*****************************************************************/
	public void mdmGetMachine(
		IPortalComponentRequest request,
		IPortalComponentResponse response) {
		response.write("<BR>prod_machine[" + "-</BR>");

		/*** create a ResultSetDefinition on the machines table ***/
		ResultSetDefinition rsd_prod_machine;
		rsd_prod_machine = new ResultSetDefinition("prod_machine");
		rsd_prod_machine.AddField("Name");
		rsd_prod_machine.AddField("printable_name");
		rsd_prod_machine.AddField("Phase");
		rsd_prod_machine.AddField("sbtitle");
		rsd_prod_machine.AddField("advantages");
		rsd_prod_machine.AddField("technical_features");
		rsd_prod_machine.AddField("main_application");
		rsd_prod_machine.AddField("application");
		rsd_prod_machine.AddField("sector");
		rsd_prod_machine.AddField("Modules");
		rsd_prod_machine.AddField("Drawing");
		rsd_prod_machine.AddField("Voltages");
		rsd_prod_machine.AddField("Capacity");
		rsd_prod_machine.AddField("crimp_range");
		rsd_prod_machine.AddField("Crimp_force_t");
		rsd_prod_machine.AddField("Opening_with_dies");
		rsd_prod_machine.AddField("Complete_opening");
		rsd_prod_machine.AddField("Masterdie_length_mm");
		rsd_prod_machine.AddField("Masterdie_length_inch");
		rsd_prod_machine.AddField("Motor_KW");
		rsd_prod_machine.AddField("Noise_level");
		rsd_prod_machine.AddField("Motor_protection_class");
		rsd_prod_machine.AddField("Dimensions");
		rsd_prod_machine.AddField("Net_Weight_kg");
		rsd_prod_machine.AddField("Net_Weight_lbs");
		rsd_prod_machine.AddField("Gross_Weight_kg");
		rsd_prod_machine.AddField("Gross_weight_lbs");
		rsd_prod_machine.AddField("Packaging_dimensions");
		rsd_prod_machine.AddField("Tooling");
		rsd_prod_machine.AddField("safety_remarks");
		rsd_prod_machine.AddField("Notes");
		rsd_prod_machine.AddField("Standard");
		rsd_prod_machine.AddField("Options");
		rsd_prod_machine.AddField("Diesets_available");
		rsd_prod_machine.AddField("Hose_type_1");
		rsd_prod_machine.AddField("Ref_hoses1");
		rsd_prod_machine.AddField("Max_hoses_size_1");
		rsd_prod_machine.AddField("Fittings_Type1");
		rsd_prod_machine.AddField("Hose_type_2");
		rsd_prod_machine.AddField("Ref_hoses2");
		rsd_prod_machine.AddField("Max_hoses_size_2");
		rsd_prod_machine.AddField("Fittings_Type2");
		rsd_prod_machine.AddField("Hose_type_3");
		rsd_prod_machine.AddField("Ref_hoses3");
		rsd_prod_machine.AddField("Max_hoses_size_3");
		rsd_prod_machine.AddField("Fittings_Type3");
		rsd_prod_machine.AddField("Hose_type_4");
		rsd_prod_machine.AddField("Ref_hoses4");
		rsd_prod_machine.AddField("Max_hoses_size_4");
		rsd_prod_machine.AddField("Fittings_Type4");

		rsd_prod_machine.AddField("Hose_type_5");
		rsd_prod_machine.AddField("Ref_hoses5");
		rsd_prod_machine.AddField("Max_hoses_size_5");
		rsd_prod_machine.AddField("Fittings_Type5");
		rsd_prod_machine.AddField("Hose_type_6");
		rsd_prod_machine.AddField("Ref_hoses6");
		rsd_prod_machine.AddField("Max_hoses_size_6");
		rsd_prod_machine.AddField("Fittings_Type6");

		rsd_prod_machine.AddField("Datasheet_release_No");
		rsd_prod_machine.AddField("Datasheet_release_Date");
		rsd_prod_machine.AddField("tree");
		rsd_prod_machine.AddField("datecreation");
		rsd_prod_machine.AddField("created_from");
		rsd_prod_machine.AddField("date_last_change");
		rsd_prod_machine.AddField("author_change");
		rsd_prod_machine.AddField("Available_accessories");
		/*** create an empty Search object ***/
		Search search_machine = new Search(rsd_prod_machine.GetTable());
		/*** Filters ***/
		FreeFormTableParameter fftp_H =
			search_machine.GetParameters().NewFreeFormTableParameter(
				rsd_prod_machine.GetTable());
		//		response.write("<BR>paramCatalog: " + paramCatalog + "</BR>");

		FreeFormParameterField ffpf_H_Key02 = fftp_H.GetFields().New("tree");
		ffpf_H_Key02.GetFreeForm().NewString(
			"Yes",
			FreeFormParameter.NotNullSearchType);

		if (paramCatalog.equals("false")) {
			FreeFormParameterField ffpf_H_Key01 =
				fftp_H.GetFields().New("Name");
			ffpf_H_Key01.GetFreeForm().NewString(
				paramValue,
				FreeFormParameter.EqualToSearchType);
		}

		/*** Printout Fields ***/
		FreeFormParameterField ffpf_H_Field00 = fftp_H.GetFields().New("Name");
		FreeFormParameterField ffpf_H_Field01 =
			fftp_H.GetFields().New("printable_name");
		FreeFormParameterField ffpf_H_Field02 = fftp_H.GetFields().New("Phase");
		FreeFormParameterField ffpf_H_Field03 =
			fftp_H.GetFields().New("sbtitle");
		FreeFormParameterField ffpf_H_Field04 =
			fftp_H.GetFields().New("advantages");
		FreeFormParameterField ffpf_H_Field05 =
			fftp_H.GetFields().New("technical_features");
		FreeFormParameterField ffpf_H_Field06 =
			fftp_H.GetFields().New("main_application");
		FreeFormParameterField ffpf_H_Field07 =
			fftp_H.GetFields().New("application");
		FreeFormParameterField ffpf_H_Field08 =
			fftp_H.GetFields().New("sector");
		FreeFormParameterField ffpf_H_Field09 =
			fftp_H.GetFields().New("Modules");
		FreeFormParameterField ffpf_H_Field10 =
			fftp_H.GetFields().New("Drawing");
		FreeFormParameterField ffpf_H_Field11 =
			fftp_H.GetFields().New("Voltages");
		FreeFormParameterField ffpf_H_Field12 =
			fftp_H.GetFields().New("Capacity");
		FreeFormParameterField ffpf_H_Field13 =
			fftp_H.GetFields().New("crimp_range");
		FreeFormParameterField ffpf_H_Field14 =
			fftp_H.GetFields().New("Crimp_force_t");
		FreeFormParameterField ffpf_H_Field15 =
			fftp_H.GetFields().New("Opening_with_dies");
		FreeFormParameterField ffpf_H_Field16 =
			fftp_H.GetFields().New("Complete_opening");
		FreeFormParameterField ffpf_H_Field17 =
			fftp_H.GetFields().New("Masterdie_length_mm");
		FreeFormParameterField ffpf_H_Field18 =
			fftp_H.GetFields().New("Masterdie_length_inch");
		FreeFormParameterField ffpf_H_Field19 =
			fftp_H.GetFields().New("Motor_KW");
		FreeFormParameterField ffpf_H_Field20 =
			fftp_H.GetFields().New("Noise_level");
		FreeFormParameterField ffpf_H_Field21 =
			fftp_H.GetFields().New("Motor_protection_class");
		FreeFormParameterField ffpf_H_Field22 =
			fftp_H.GetFields().New("Dimensions");
		FreeFormParameterField ffpf_H_Field23 =
			fftp_H.GetFields().New("Net_Weight_kg");
		FreeFormParameterField ffpf_H_Field24 =
			fftp_H.GetFields().New("Net_Weight_lbs");
		FreeFormParameterField ffpf_H_Field25 =
			fftp_H.GetFields().New("Gross_Weight_kg");
		FreeFormParameterField ffpf_H_Field26 =
			fftp_H.GetFields().New("Gross_weight_lbs");
		FreeFormParameterField ffpf_H_Field27 =
			fftp_H.GetFields().New("Packaging_dimensions");
		FreeFormParameterField ffpf_H_Field28 =
			fftp_H.GetFields().New("Tooling");
		FreeFormParameterField ffpf_H_Field29 =
			fftp_H.GetFields().New("safety_remarks");
		FreeFormParameterField ffpf_H_Field30 = fftp_H.GetFields().New("Notes");
		FreeFormParameterField ffpf_H_Field31 =
			fftp_H.GetFields().New("Standard");
		FreeFormParameterField ffpf_H_Field32 =
			fftp_H.GetFields().New("Options");
		FreeFormParameterField ffpf_H_Field33 =
			fftp_H.GetFields().New("Diesets_available");
		FreeFormParameterField ffpf_H_Field34 =
			fftp_H.GetFields().New("Hose_type_1");
		FreeFormParameterField ffpf_H_Field35 =
			fftp_H.GetFields().New("Ref_hoses1");
		FreeFormParameterField ffpf_H_Field36 =
			fftp_H.GetFields().New("Max_hoses_size_1");
		FreeFormParameterField ffpf_H_Field37 =
			fftp_H.GetFields().New("Fittings_Type1");
		FreeFormParameterField ffpf_H_Field38 =
			fftp_H.GetFields().New("Hose_type_2");
		FreeFormParameterField ffpf_H_Field39 =
			fftp_H.GetFields().New("Ref_hoses2");
		FreeFormParameterField ffpf_H_Field40 =
			fftp_H.GetFields().New("Max_hoses_size_2");
		FreeFormParameterField ffpf_H_Field41 =
			fftp_H.GetFields().New("Fittings_Type2");
		FreeFormParameterField ffpf_H_Field42 =
			fftp_H.GetFields().New("Hose_type_3");
		FreeFormParameterField ffpf_H_Field43 =
			fftp_H.GetFields().New("Ref_hoses3");
		FreeFormParameterField ffpf_H_Field44 =
			fftp_H.GetFields().New("Max_hoses_size_3");
		FreeFormParameterField ffpf_H_Field45 =
			fftp_H.GetFields().New("Fittings_Type3");
		FreeFormParameterField ffpf_H_Field46 =
			fftp_H.GetFields().New("Hose_type_4");
		FreeFormParameterField ffpf_H_Field47 =
			fftp_H.GetFields().New("Ref_hoses4");
		FreeFormParameterField ffpf_H_Field48 =
			fftp_H.GetFields().New("Max_hoses_size_4");
		FreeFormParameterField ffpf_H_Field49 =
			fftp_H.GetFields().New("Fittings_Type4");
		FreeFormParameterField ffpf_H_Field50 =
			fftp_H.GetFields().New("Datasheet_release_No");
		FreeFormParameterField ffpf_H_Field51 =
			fftp_H.GetFields().New("Datasheet_release_Date");
		FreeFormParameterField ffpf_H_Field52 = fftp_H.GetFields().New("tree");
		FreeFormParameterField ffpf_H_Field53 =
			fftp_H.GetFields().New("datecreation");
		FreeFormParameterField ffpf_H_Field54 =
			fftp_H.GetFields().New("created_from");
		FreeFormParameterField ffpf_H_Field55 =
			fftp_H.GetFields().New("date_last_change");
		FreeFormParameterField ffpf_H_Field56 =
			fftp_H.GetFields().New("author_change");
		FreeFormParameterField ffpf_H_Field57 =
			fftp_H.GetFields().New("Available_accessories");

		FreeFormParameterField ffpf_H_Field58 =
			fftp_H.GetFields().New("Hose_type_5");
		FreeFormParameterField ffpf_H_Field59 =
			fftp_H.GetFields().New("Ref_hoses5");
		FreeFormParameterField ffpf_H_Field60 =
			fftp_H.GetFields().New("Max_hoses_size_5");
		FreeFormParameterField ffpf_H_Field61 =
			fftp_H.GetFields().New("Fittings_Type5");
		FreeFormParameterField ffpf_H_Field62 =
			fftp_H.GetFields().New("Hose_type_6");
		FreeFormParameterField ffpf_H_Field63 =
			fftp_H.GetFields().New("Ref_hoses6");
		FreeFormParameterField ffpf_H_Field64 =
			fftp_H.GetFields().New("Max_hoses_size_6");
		FreeFormParameterField ffpf_H_Field65 =
			fftp_H.GetFields().New("Fittings_Type6");
		try {
			A2iResultSet rs_H =
				catalogData.GetResultSet(
					search_machine,
					rsd_prod_machine,
					null,
					true,
					0);
			//			response.write(
			//				"<BR>prod_machine recordCount: "
			//					+ rs_H.GetRecordCount()
			//					+ "</BR>");
			for (int i = 0; i < rs_H.GetRecordCount(); i++) {
				fld01String = null;
				fld02String = null;
				fld03String = null;
				fld04String = null;
				fld05String = null;
				fld06String = null;
				fld07String = null;
				fld08String = null;
				fld09String = null;
				fld10String = null;
				fld11String = null;
				fld12String = null;
				fld13String = null;
				fld14String = null;
				fld15String = null;
				fld16String = null;
				fld17String = null;
				fld18String = null;
				fld19String = null;
				fld20String = null;
				fld21String = null;
				fld22String = null;
				fld23String = null;
				fld24String = null;
				fld25String = null;
				fld26String = null;
				fld27String = null;
				fld28String = null;
				fld29String = null;
				fld30String = null;
				fld31String = null;
				fld32String = null;
				fld33String = null;
				fld34String = null;
				fld35String = null;
				fld36String = null;
				fld37String = null;
				fld38String = null;
				fld39String = null;
				fld40String = null;
				fld41String = null;
				fld42String = null;
				fld43String = null;
				fld44String = null;
				fld45String = null;
				fld46String = null;
				fld47String = null;
				fld48String = null;
				fld49String = null;
				fld50String = null;
				fld51String = null;
				fld52String = null;
				fld53String = null;
				fld54String = null;
				fld55String = null;
				fld56String = null;
				fld57String = null;
				fld58String = null;
				fld59String = null;
				fld60String = null;
				fld61String = null;
				fld62String = null;
				fld63String = null;
				fld64String = null;
				fld65String = null;
				fld66String = null;

				// H_Field00		String
				if (!rs_H.GetValueAt(i, "Name").IsNull()) {
					fld01String = rs_H.GetValueAt(i, "Name").GetStringValue();
					mdmGetMachineRec(request, response, fld01String);
				}

				if (getRecords == true) {

					// H_Field01		String
					if (!rs_H.GetValueAt(i, "printable_name").IsNull()) {
						fld02String =
							rs_H
								.GetValueAt(i, "printable_name")
								.GetStringValue();
					}
					// H_Field02		String
					if (!rs_H.GetValueAt(i, "Phase").IsNull()) {
						fld03String =
							rs_H.GetValueAt(i, "Phase").GetStringValue();
					}
					// H_Field03		String
					if (!rs_H.GetValueAt(i, "sbtitle").IsNull()) {
						fld04String =
							rs_H.GetValueAt(i, "sbtitle").GetStringValue();
					}
					// H_Field04		String
					if (!rs_H.GetValueAt(i, "advantages").IsNull()) {
						fld05String =
							rs_H.GetValueAt(i, "advantages").GetStringValue();
					}
					// H_Field05		String
					if (!rs_H.GetValueAt(i, "technical_features").IsNull()) {
						fld06String =
							rs_H
								.GetValueAt(i, "technical_features")
								.GetStringValue();
					}
					// H_Field06		String
					if (!rs_H.GetValueAt(i, "main_application").IsNull()) {
						fld07String =
							rs_H
								.GetValueAt(i, "main_application")
								.GetStringValue();
					}
					// H_Field07		String
					if (!rs_H.GetValueAt(i, "application").IsNull()) {
						fld08String =
							rs_H
								.GetValueAt(i, "application")
								.TranslateToString();
					}
					// H_Field08		String
					if (!rs_H.GetValueAt(i, "sector").IsNull()) {
						fld09String =
							rs_H.GetValueAt(i, "sector").GetStringValue();
					}
					// H_Field09		String
					if (!rs_H.GetValueAt(i, "Modules").IsNull()) {
						fld10String =
							rs_H.GetValueAt(i, "Modules").TranslateToString();
					}
					// H_Field10		Image
					if (!rs_H.GetValueAt(i, "Drawing").IsNull()) {
						CatalogCache catalogCache = new CatalogCache();
						catalogCache.Init(
							CACHE_SERVER,
							CACHE_PORT,
							CACHE_USER,
							CACHE_PASSWORD,
							CACHE_DIRECTORY,
							"English [US]");

						Object id = rs_H.GetValueAt(i, "Drawing").GetData();
						int idx = id.hashCode();
						fld11String =
							CACHE_DIRECTORY
								+ catalogCache.GetImagePath(
									"Images",
									"Thumbnail",
									idx);
						catalogCache.Shutdown();
					}
					// H_Field11		String
					if (!rs_H.GetValueAt(i, "Voltages").IsNull()) {
						fld12String =
							rs_H.GetValueAt(i, "Voltages").TranslateToString();
					}
					// H_Field12		String
					if (!rs_H.GetValueAt(i, "Capacity").IsNull()) {
						fld13String =
							rs_H.GetValueAt(i, "Capacity").TranslateToString();
					}
					// H_Field13		String
					if (!rs_H.GetValueAt(i, "crimp_range").IsNull()) {
						fld14String =
							rs_H.GetValueAt(i, "crimp_range").GetStringValue();
					}
					// H_Field14		Measurement
					if (rs_H.GetValueAt(i, "Crimp_force_t").IsNull() != true) {
						Measurement fldMeas15 =
							rs_H
								.GetValueAt(i, "Crimp_force_t")
								.GetMeasurement();
						fld15String =
							measurementManager.GetString(
								fldMeas15.GetValue(),
								0,
								0,
								rs_H
									.GetFieldAt("Crimp_force_t")
									.GetDecimalPlaces(),
								false,
								-1);
						fld15String = fld15String.replaceAll(",", ".");
						fld15Float = Float.valueOf(fld15String).floatValue();
					}
					// H_Field15		String
					if (!rs_H.GetValueAt(i, "Opening_with_dies").IsNull()) {
						fld16String =
							rs_H
								.GetValueAt(i, "Opening_with_dies")
								.GetStringValue();
					}
					// H_Field16		String
					if (!rs_H.GetValueAt(i, "Complete_opening").IsNull()) {
						fld17String =
							rs_H
								.GetValueAt(i, "Complete_opening")
								.GetStringValue();
					}
					// H_Field17		Measurement
					if (rs_H.GetValueAt(i, "Masterdie_length_mm").IsNull()
						!= true) {
						Measurement fldMeas18 =
							rs_H
								.GetValueAt(i, "Masterdie_length_mm")
								.GetMeasurement();
						fld18String =
							measurementManager.GetString(
								fldMeas18.GetValue(),
								fldMeas18.GetUnitID(),
								rs_H
									.GetFieldAt("Masterdie_length_mm")
									.GetDimensionID(),
								rs_H
									.GetFieldAt("Masterdie_length_mm")
									.GetDecimalPlaces(),
								false,
								-1);
						fld18String = fld18String.replaceAll(",", ".");
						//fld18Float = Float.valueOf(fld18String).floatValue();
					}
					// H_Field18		Measurement
					if (rs_H.GetValueAt(i, "Masterdie_length_inch").IsNull()
						!= true) {
						fld19String =
							rs_H
								.GetValueAt(i, "Masterdie_length_inch")
								.GetStringValue();

						//					Measurement fldMeas19 =
						//						rs_H
						//							.GetValueAt(i, "Masterdie_length_inch")
						//							.GetMeasurement();
						//					fld19String =
						//						measurementManager.GetString(
						//							fldMeas19.GetValue(),
						//							fldMeas19.GetUnitID(),
						//							rs_H
						//								.GetFieldAt("Masterdie_length_inch")
						//								.GetDimensionID(),
						//							rs_H
						//								.GetFieldAt("Masterdie_length_inch")
						//								.GetDecimalPlaces(),
						//							false,
						//							-1);
						//					fld19String = fld19String.replaceAll(",", ".");
						//fld19Float = Float.valueOf(fld19String).floatValue();
					}
					// H_Field19		Measurement
					if (rs_H.GetValueAt(i, "Motor_KW").IsNull() != true) {
						Measurement fldMeas20 =
							rs_H.GetValueAt(i, "Motor_KW").GetMeasurement();
						fld20String =
							measurementManager.GetString(
								fldMeas20.GetValue(),
								0,
								0,
								rs_H.GetFieldAt("Motor_KW").GetDecimalPlaces(),
								false,
								-1);
						fld20String = fld20String.replaceAll(",", ".");
						fld20Float = Float.valueOf(fld20String).floatValue();
					}
					// H_Field20		String
					if (!rs_H.GetValueAt(i, "Noise_level").IsNull()) {
						fld21String =
							rs_H.GetValueAt(i, "Noise_level").GetStringValue();
					}
					// H_Field21		String
					if (!rs_H
						.GetValueAt(i, "Motor_protection_class")
						.IsNull()) {
						fld22String =
							rs_H
								.GetValueAt(i, "Motor_protection_class")
								.GetStringValue();
					}
					// H_Field22		String
					if (!rs_H.GetValueAt(i, "Dimensions").IsNull()) {
						fld23String =
							rs_H.GetValueAt(i, "Dimensions").GetStringValue();
					}
					// H_Field23		Measurement
					if (rs_H.GetValueAt(i, "Net_Weight_kg").IsNull() != true) {
						Measurement fldMeas24 =
							rs_H
								.GetValueAt(i, "Net_Weight_kg")
								.GetMeasurement();
						fld24String =
							measurementManager.GetString(
								fldMeas24.GetValue(),
								fldMeas24.GetUnitID(),
								rs_H
									.GetFieldAt("Net_Weight_kg")
									.GetDimensionID(),
								rs_H
									.GetFieldAt("Net_Weight_kg")
									.GetDecimalPlaces(),
								false,
								-1);
						fld24String = fld24String.replaceAll(",", ".");
						//fld24Float = Float.valueOf(fld24String).floatValue();
					}
					// H_Field24		Measurement
					if (rs_H.GetValueAt(i, "Net_Weight_lbs").IsNull()
						!= true) {
						Measurement fldMeas25 =
							rs_H
								.GetValueAt(i, "Net_Weight_lbs")
								.GetMeasurement();
						fld25String =
							measurementManager.GetString(
								fldMeas25.GetValue(),
								fldMeas25.GetUnitID(),
								rs_H
									.GetFieldAt("Net_Weight_lbs")
									.GetDimensionID(),
								rs_H
									.GetFieldAt("Net_Weight_lbs")
									.GetDecimalPlaces(),
								false,
								-1);
						fld25String = fld25String.replaceAll(",", ".");
						//fld25Float = Float.valueOf(fld25String).floatValue();
					}
					// H_Field25		Measurement
					if (rs_H.GetValueAt(i, "Gross_Weight_kg").IsNull()
						!= true) {
						Measurement fldMeas26 =
							rs_H
								.GetValueAt(i, "Gross_Weight_kg")
								.GetMeasurement();
						fld26String =
							measurementManager.GetString(
								fldMeas26.GetValue(),
								fldMeas26.GetUnitID(),
								rs_H
									.GetFieldAt("Gross_Weight_kg")
									.GetDimensionID(),
								rs_H
									.GetFieldAt("Gross_Weight_kg")
									.GetDecimalPlaces(),
								false,
								-1);
						fld26String = fld26String.replaceAll(",", ".");
						//fld26Float = Float.valueOf(fld26String).floatValue();
					}
					// H_Field26		Measurement
					if (rs_H.GetValueAt(i, "Gross_weight_lbs").IsNull()
						!= true) {
						Measurement fldMeas27 =
							rs_H
								.GetValueAt(i, "Gross_weight_lbs")
								.GetMeasurement();
						fld27String =
							measurementManager.GetString(
								fldMeas27.GetValue(),
								fldMeas27.GetUnitID(),
								rs_H
									.GetFieldAt("Gross_weight_lbs")
									.GetDimensionID(),
								rs_H
									.GetFieldAt("Gross_weight_lbs")
									.GetDecimalPlaces(),
								false,
								-1);
						fld27String = fld27String.replaceAll(",", ".");
						//fld27Float = Float.valueOf(fld27String).floatValue();
					}
					// H_Field27		String
					if (!rs_H.GetValueAt(i, "Packaging_dimensions").IsNull()) {
						fld28String =
							rs_H
								.GetValueAt(i, "Packaging_dimensions")
								.GetStringValue();
					}
					// H_Field28		String
					if (!rs_H.GetValueAt(i, "Tooling").IsNull()) {
						fld29String =
							rs_H.GetValueAt(i, "Tooling").GetStringValue();
					}
					// H_Field29		String
					if (!rs_H.GetValueAt(i, "safety_remarks").IsNull()) {
						fld30String =
							rs_H
								.GetValueAt(i, "safety_remarks")
								.GetStringValue();
					}
					// H_Field30		String
					if (!rs_H.GetValueAt(i, "Notes").IsNull()) {
						fld31String =
							rs_H.GetValueAt(i, "Notes").GetStringValue();
					}
					// H_Field31		String
					if (!rs_H.GetValueAt(i, "Standard").IsNull()) {
						fld32String =
							rs_H.GetValueAt(i, "Standard").TranslateToString();
						mdmGetStandard(
							request,
							response,
							fld32String,
							fld01String);
					}
					// H_Field32		String
					if (!rs_H.GetValueAt(i, "Options").IsNull()) {
						fld33String =
							rs_H.GetValueAt(i, "Options").TranslateToString();
						mdmGetOptions(
							request,
							response,
							fld33String,
							fld01String);
					}
					// H_Field33		String
					if (!rs_H.GetValueAt(i, "Diesets_available").IsNull()) {
						fld34String =
							rs_H
								.GetValueAt(i, "Diesets_available")
								.TranslateToString();
						mdmGetMachaccRec(
							request,
							response,
							fld34String,
							fld01String);
					}
					// H_Field34		String
					if (!rs_H.GetValueAt(i, "Hose_type_1").IsNull()) {
						fld35String =
							rs_H
								.GetValueAt(i, "Hose_type_1")
								.TranslateToString();
					}
					// H_Field35		String
					if (!rs_H.GetValueAt(i, "Ref_hoses1").IsNull()) {
						fld36String =
							rs_H
								.GetValueAt(i, "Ref_hoses1")
								.TranslateToString();
					}
					// H_Field36		String
					if (!rs_H.GetValueAt(i, "Max_hoses_size_1").IsNull()) {
						fld37String =
							rs_H
								.GetValueAt(i, "Max_hoses_size_1")
								.GetStringValue();
					}
					// H_Field37		String
					if (!rs_H.GetValueAt(i, "Fittings_Type1").IsNull()) {
						fld38String =
							rs_H
								.GetValueAt(i, "Fittings_Type1")
								.TranslateToString();
					}
					// H_Field38		String
					if (!rs_H.GetValueAt(i, "Hose_type_2").IsNull()) {
						fld39String =
							rs_H
								.GetValueAt(i, "Hose_type_2")
								.TranslateToString();
					}
					// H_Field39		String
					if (!rs_H.GetValueAt(i, "Ref_hoses2").IsNull()) {
						fld40String =
							rs_H
								.GetValueAt(i, "Ref_hoses2")
								.TranslateToString();
					}
					// H_Field40		String
					if (!rs_H.GetValueAt(i, "Max_hoses_size_2").IsNull()) {
						fld41String =
							rs_H
								.GetValueAt(i, "Max_hoses_size_2")
								.GetStringValue();
					}
					// H_Field41		String
					if (!rs_H.GetValueAt(i, "Fittings_Type2").IsNull()) {
						fld42String =
							rs_H
								.GetValueAt(i, "Fittings_Type2")
								.TranslateToString();
					}
					// H_Field42		String
					if (!rs_H.GetValueAt(i, "Hose_type_3").IsNull()) {
						fld43String =
							rs_H
								.GetValueAt(i, "Hose_type_3")
								.TranslateToString();
					}
					// H_Field43		String
					if (!rs_H.GetValueAt(i, "Ref_hoses3").IsNull()) {
						fld44String =
							rs_H
								.GetValueAt(i, "Ref_hoses3")
								.TranslateToString();
					}
					// H_Field44		String
					if (!rs_H.GetValueAt(i, "Max_hoses_size_3").IsNull()) {
						fld45String =
							rs_H
								.GetValueAt(i, "Max_hoses_size_3")
								.GetStringValue();
					}
					// H_Field45		String
					if (!rs_H.GetValueAt(i, "Fittings_Type3").IsNull()) {
						fld46String =
							rs_H
								.GetValueAt(i, "Fittings_Type3")
								.TranslateToString();
					}
					// H_Field46		String
					if (!rs_H.GetValueAt(i, "Hose_type_4").IsNull()) {
						fld47String =
							rs_H
								.GetValueAt(i, "Hose_type_4")
								.TranslateToString();
					}
					// H_Field47		String
					if (!rs_H.GetValueAt(i, "Ref_hoses4").IsNull()) {
						fld48String =
							rs_H
								.GetValueAt(i, "Ref_hoses4")
								.TranslateToString();
					}
					// H_Field48		String
					if (!rs_H.GetValueAt(i, "Max_hoses_size_4").IsNull()) {
						fld49String =
							rs_H
								.GetValueAt(i, "Max_hoses_size_4")
								.GetStringValue();
					}
					// H_Field49		String
					if (!rs_H.GetValueAt(i, "Fittings_Type4").IsNull()) {
						fld50String =
							rs_H
								.GetValueAt(i, "Fittings_Type4")
								.TranslateToString();
					}
					// H_Field50		String
					if (!rs_H.GetValueAt(i, "Datasheet_release_No").IsNull()) {
						fld51String =
							rs_H
								.GetValueAt(i, "Datasheet_release_No")
								.GetStringValue();
					}
					// H_Field51		DateTime
					if (rs_H.GetValueAt(i, "Datasheet_release_Date").IsNull()
						!= true) {
						String v_dateTime =
							rs_H
								.GetValueAt(i, "Datasheet_release_Date")
								.GetSystemTime()
								.ConvertSystemTimeToString();
						fld52String =
							v_dateTime.substring(8, 10)
								+ "/"
								+ v_dateTime.substring(5, 7)
								+ "/"
								+ v_dateTime.substring(0, 4);
					}
					// H_Field52		String
					if (!rs_H.GetValueAt(i, "tree").IsNull()) {
						fld53String =
							rs_H.GetValueAt(i, "tree").GetStringValue();
					}
					// H_Field53		DateTime
					if (rs_H.GetValueAt(i, "datecreation").IsNull() != true) {
						String v_dateTime =
							rs_H
								.GetValueAt(i, "datecreation")
								.GetSystemTime()
								.ConvertSystemTimeToString();
						fld54String =
							v_dateTime.substring(8, 10)
								+ "/"
								+ v_dateTime.substring(5, 7)
								+ "/"
								+ v_dateTime.substring(0, 4);
					}
					// H_Field54		String
					if (!rs_H.GetValueAt(i, "created_from").IsNull()) {
						fld55String =
							rs_H.GetValueAt(i, "created_from").GetStringValue();
					}
					// H_Field55		DateTime
					if (rs_H.GetValueAt(i, "date_last_change").IsNull()
						!= true) {
						String v_dateTime =
							rs_H
								.GetValueAt(i, "date_last_change")
								.GetSystemTime()
								.ConvertSystemTimeToString();
						fld56String =
							v_dateTime.substring(8, 10)
								+ "/"
								+ v_dateTime.substring(5, 7)
								+ "/"
								+ v_dateTime.substring(0, 4);
					}
					// H_Field56		String
					if (!rs_H.GetValueAt(i, "author_change").IsNull()) {
						fld57String =
							rs_H
								.GetValueAt(i, "author_change")
								.GetStringValue();
					}
					// H_Field57		String
					if (!rs_H
						.GetValueAt(i, "Available_accessories")
						.IsNull()) {
						fld58String =
							rs_H
								.GetValueAt(i, "Available_accessories")
								.TranslateToString();
						mdmGetMachaccNewRec(
							request,
							response,
							fld58String,
							fld01String);
					}

					// H_Field46		String
					if (!rs_H.GetValueAt(i, "Hose_type_5").IsNull()) {
						fld59String =
							rs_H
								.GetValueAt(i, "Hose_type_5")
								.TranslateToString();
					}
					// H_Field47		String
					if (!rs_H.GetValueAt(i, "Ref_hoses5").IsNull()) {
						fld60String =
							rs_H
								.GetValueAt(i, "Ref_hoses5")
								.TranslateToString();
					}
					// H_Field48		String
					if (!rs_H.GetValueAt(i, "Max_hoses_size_5").IsNull()) {
						fld61String =
							rs_H
								.GetValueAt(i, "Max_hoses_size_5")
								.GetStringValue();
					}
					// H_Field49		String
					if (!rs_H.GetValueAt(i, "Fittings_Type5").IsNull()) {
						fld62String =
							rs_H
								.GetValueAt(i, "Fittings_Type5")
								.TranslateToString();
					}
					// H_Field50		String
					// H_Field46		String
					if (!rs_H.GetValueAt(i, "Hose_type_6").IsNull()) {
						fld63String =
							rs_H
								.GetValueAt(i, "Hose_type_6")
								.TranslateToString();
					}
					// H_Field47		String
					if (!rs_H.GetValueAt(i, "Ref_hoses6").IsNull()) {
						fld64String =
							rs_H
								.GetValueAt(i, "Ref_hoses6")
								.TranslateToString();
					}
					// H_Field48		String
					if (!rs_H.GetValueAt(i, "Max_hoses_size_6").IsNull()) {
						fld65String =
							rs_H
								.GetValueAt(i, "Max_hoses_size_6")
								.GetStringValue();
					}
					// H_Field49		String
					if (!rs_H.GetValueAt(i, "Fittings_Type6").IsNull()) {
						fld66String =
							rs_H
								.GetValueAt(i, "Fittings_Type6")
								.TranslateToString();
					}
					// H_Field50		String

					mdmGetSelection(
						request,
						response,
						fld01String,
						fld35String,
						fld36String,
						fld37String,
						fld38String,
						fld39String,
						fld40String,
						fld41String,
						fld42String,
						fld43String,
						fld44String,
						fld45String,
						fld46String,
						fld47String,
						fld48String,
						fld49String,
						fld50String,
						fld59String,
						fld60String,
						fld61String,
						fld62String,
						fld63String,
						fld64String,
						fld65String,
						fld66String);

					recsMachine.add(
						new prod_machine(
							fld01String,
							fld02String,
							fld03String,
							fld04String,
							fld05String,
							fld06String,
							fld07String,
							fld08String,
							fld09String,
							fld10String,
							fld11String,
							fld12String,
							fld13String,
							fld14String,
							fld15String,
							fld16String,
							fld17String,
							fld18String,
							fld19String,
							fld20String,
							fld21String,
							fld22String,
							fld23String,
							fld24String,
							fld25String,
							fld26String,
							fld27String,
							fld28String,
							fld29String,
							fld30String,
							fld31String,
							fld32String,
							fld33String,
							fld34String,
							fld35String,
							fld36String,
							fld37String,
							fld38String,
							fld39String,
							fld40String,
							fld41String,
							fld42String,
							fld43String,
							fld44String,
							fld45String,
							fld46String,
							fld47String,
							fld48String,
							fld49String,
							fld50String,
							fld51String,
							fld52String,
							fld53String,
							fld54String,
							fld55String,
							fld56String,
							fld57String,
							fld58String));
				}

			}
		} catch (StringException e) {
			response.write(
				"<BR> e.printStackTrace: "
					+ e.getCause()
					+ e.getMessage()
					+ "</BR>");
			e.printStackTrace();
		}

	}
	/*****************************************************************/
	/***             Retrieve data from M_Standard                 ***/
	/*****************************************************************/
	public void mdmGetStandard(
		IPortalComponentRequest request,
		IPortalComponentResponse response,
		String standard,
		String family) {
		int k = 0;
		String[] arrayStandardRec = new String[7];
		String newStandard = standard.replaceAll(", ;", ",");
		String[] arrayStandard = newStandard.split(",");
		//		response.write(
		//			"<BR>arrayStandard.length: " + arrayStandard.length + "</BR>");
		try {
			for (int j = 0; j < arrayStandard.length - 1; j++) {

				//				response.write(
				//					"<BR>arrayStandard[j]: -" + arrayStandard[j] + "-</BR>");
				//				response.write("<BR>M_Standard" + "-</BR>");

				/*** create a ResultSetDefinition on the M_Standard table ***/
				ResultSetDefinition rsd_m_standard;
				rsd_m_standard = new ResultSetDefinition("M_Standard");
				rsd_m_standard.AddField("Name");
				rsd_m_standard.AddField("Description");
				rsd_m_standard.AddField("Logo");

				/*** create an empty Search object ***/
				Search search_standard = new Search(rsd_m_standard.GetTable());
				/*** Filters ***/
				FreeFormTableParameter fftp_S =
					search_standard.GetParameters().NewFreeFormTableParameter(
						rsd_m_standard.GetTable());

				FreeFormParameterField ffpf_S_Key01 =
					fftp_S.GetFields().New("Name");
				ffpf_S_Key01.GetFreeForm().NewString(
					arrayStandard[j],
					FreeFormParameter.EqualToSearchType);

				/*** Printout Fields ***/
				FreeFormParameterField ffpf_S_Field00 =
					fftp_S.GetFields().New("Name");
				FreeFormParameterField ffpf_S_Field01 =
					fftp_S.GetFields().New("Description");
				FreeFormParameterField ffpf_S_Field02 =
					fftp_S.GetFields().New("Logo");

				A2iResultSet rs_S =
					catalogData.GetResultSet(
						search_standard,
						rsd_m_standard,
						null,
						true,
						0);
				//				response.write(
				//					"<BR>M_Standard recordCount: "
				//						+ rs_S.GetRecordCount()
				//						+ "</BR>");
				for (int i = 0; i < rs_S.GetRecordCount(); i++) {
					fldS01String = null;
					fldS02String = null;
					fldS03String = null;
					// S_Field00	String
					if (!rs_S.GetValueAt(i, "Name").IsNull()) {
						fldS01String =
							rs_S.GetValueAt(i, "Name").TranslateToString();
					}
					// S_Field01		String
					if (!rs_S.GetValueAt(i, "Description").IsNull()) {
						fldS02String =
							rs_S
								.GetValueAt(i, "Description")
								.TranslateToString();
					}
					// S_Field02		Image
					if (!rs_S.GetValueAt(i, "Logo").IsNull()) {
						CatalogCache catalogCache = new CatalogCache();
						catalogCache.Init(
							CACHE_SERVER,
							CACHE_PORT,
							CACHE_USER,
							CACHE_PASSWORD,
							CACHE_DIRECTORY,
							"English [US]");

						Object id = rs_S.GetValueAt(i, "Logo").GetData();
						int idx = id.hashCode();
						fldS03String =
							CACHE_DIRECTORY
								+ catalogCache.GetImagePath(
									"Images",
									"Original",
									idx);
						catalogCache.Shutdown();
					}

				}

				arrayStandardRec[0] = family;
				switch (k) {
					case 0 :
						arrayStandardRec[1] = fldS03String;
						arrayStandardRec[2] = fldS01String;
						break;
					case 1 :
						arrayStandardRec[3] = fldS03String;
						arrayStandardRec[4] = fldS01String;
						break;
					case 2 :
						arrayStandardRec[5] = fldS03String;
						arrayStandardRec[6] = fldS01String;
						break;
					default :
						}
				++k;
				if (k > 2) {
					recsStandard.add(
						new m_standard(
							arrayStandardRec[0],
							arrayStandardRec[1],
							arrayStandardRec[2],
							arrayStandardRec[3],
							arrayStandardRec[4],
							arrayStandardRec[5],
							arrayStandardRec[6]));
					k = 0;
					arrayStandardRec[0] = null;
					arrayStandardRec[1] = null;
					arrayStandardRec[2] = null;
					arrayStandardRec[3] = null;
					arrayStandardRec[4] = null;
					arrayStandardRec[5] = null;
					arrayStandardRec[6] = null;
				}

			}
			if (k != 0) {
				recsStandard.add(
					new m_standard(
						arrayStandardRec[0],
						arrayStandardRec[1],
						arrayStandardRec[2],
						arrayStandardRec[3],
						arrayStandardRec[4],
						arrayStandardRec[5],
						arrayStandardRec[6]));
				k = 0;
				arrayStandardRec[0] = null;
				arrayStandardRec[1] = null;
				arrayStandardRec[2] = null;
				arrayStandardRec[3] = null;
				arrayStandardRec[4] = null;
				arrayStandardRec[5] = null;
				arrayStandardRec[6] = null;
			}
		} catch (StringException e) {
			response.write(
				"<BR> e.printStackTrace: " + e.getMessage() + "</BR>");
			e.printStackTrace();
		}

	}
	/*****************************************************************/
	/***             Retrieve data from prod_machacc_rec           ***/
	/*****************************************************************/
	public void mdmGetOptions(
		IPortalComponentRequest request,
		IPortalComponentResponse response,
		String options,
		String family) {
		int k = 0;
		String[] arrayOptionsRec = new String[8];
		String newOptions = options.replaceAll(", ", ",");
		String[] arrayOptions = newOptions.split(";");
		try {
			for (int j = 0; j < arrayOptions.length; j++) {

				//				response.write(
				//					"<BR>arrayOptions["
				//						+ j
				//						+ "]: -"
				//						+ arrayOptions[j]
				//						+ "-</BR>");
				//				response.write("<BR>prod_machacc_rec" + "-</BR>");

				/*** create a ResultSetDefinition on the prod_machacc_rec table ***/
				ResultSetDefinition rsd_prod_machacc_rec;
				rsd_prod_machacc_rec =
					new ResultSetDefinition("prod_machacc_rec");
				rsd_prod_machacc_rec.AddField("Part_number");
				rsd_prod_machacc_rec.AddField("Description");
				rsd_prod_machacc_rec.AddField("Logo");

				/*** create an empty Search object ***/
				Search search_options =
					new Search(rsd_prod_machacc_rec.GetTable());
				/*** Filters ***/
				FreeFormTableParameter fftp_S =
					search_options.GetParameters().NewFreeFormTableParameter(
						rsd_prod_machacc_rec.GetTable());
				int partNumberIndex = arrayOptions[j].indexOf(",");
				String partNumber =
					arrayOptions[j].substring(0, partNumberIndex);
				FreeFormParameterField ffpf_S_Key01 =
					fftp_S.GetFields().New("Part_number");
				ffpf_S_Key01.GetFreeForm().NewString(
					partNumber,
					FreeFormParameter.EqualToSearchType);
				FreeFormParameterField ffpf_S_Key02 =
					fftp_S.GetFields().New("Printable");
				ffpf_S_Key02.GetFreeForm().NewString(
					"YES",
					FreeFormParameter.EqualToSearchType);
				FreeFormParameterField ffpf_S_Key03 =
					fftp_S.GetFields().New("Phase_flag");
				ffpf_S_Key03.GetFreeForm().NewString(
					"APPROVED",
					FreeFormParameter.EqualToSearchType);
				FreeFormParameterField ffpf_S_Key04 =
					fftp_S.GetFields().New("Public");
				ffpf_S_Key04.GetFreeForm().NewString(
					"YES",
					FreeFormParameter.EqualToSearchType);

				/*** Printout Fields ***/
				FreeFormParameterField ffpf_S_Field00 =
					fftp_S.GetFields().New("Part_number");
				FreeFormParameterField ffpf_S_Field01 =
					fftp_S.GetFields().New("Description");
				FreeFormParameterField ffpf_S_Field02 =
					fftp_S.GetFields().New("Logo");

				A2iResultSet rs_S =
					catalogData.GetResultSet(
						search_options,
						rsd_prod_machacc_rec,
						null,
						true,
						0);
				//				response.write(
				//					"<BR>prod_machacc_rec recordCount: "
				//						+ rs_S.GetRecordCount()
				//						+ "</BR>");
				for (int i = 0; i < rs_S.GetRecordCount(); i++) {
					fldS01String = null;
					fldS02String = null;
					fldS03String = null;
					// S_Field00	String
					if (!rs_S.GetValueAt(i, "Part_number").IsNull()) {
						fldS01String =
							rs_S
								.GetValueAt(i, "Part_number")
								.TranslateToString();
					}
					// S_Field01		String
					if (!rs_S.GetValueAt(i, "Description").IsNull()) {
						fldS02String =
							rs_S
								.GetValueAt(i, "Description")
								.TranslateToString();
					}
					// S_Field02		Image
					if (!rs_S.GetValueAt(i, "Logo").IsNull()) {
						CatalogCache catalogCache = new CatalogCache();
						catalogCache.Init(
							CACHE_SERVER,
							CACHE_PORT,
							CACHE_USER,
							CACHE_PASSWORD,
							CACHE_DIRECTORY,
							"English [US]");

						Object id = rs_S.GetValueAt(i, "Logo").GetData();
						int idx = id.hashCode();
						fldS03String =
							CACHE_DIRECTORY
								+ catalogCache.GetImagePath(
									"Images",
									"Thumbnail",
									idx);
						catalogCache.Shutdown();
					}

				}

				arrayOptionsRec[0] = family;
				arrayOptionsRec[1] = family;

				switch (k) {
					case 0 :
						arrayOptionsRec[2] = fldS03String;
						arrayOptionsRec[3] = fldS02String;
						break;
					case 1 :
						arrayOptionsRec[4] = fldS03String;
						arrayOptionsRec[5] = fldS02String;
						break;
					case 2 :
						arrayOptionsRec[6] = fldS03String;
						arrayOptionsRec[7] = fldS02String;
						break;
					default :
						}
				++k;
				if (k > 2) {
					recsOption.add(
						new prod_machacc_rec(
							arrayOptionsRec[0],
							arrayOptionsRec[1],
							arrayOptionsRec[2],
							arrayOptionsRec[3],
							arrayOptionsRec[4],
							arrayOptionsRec[5],
							arrayOptionsRec[6],
							arrayOptionsRec[7]));
					k = 0;
					arrayOptionsRec[0] = null;
					arrayOptionsRec[1] = null;
					arrayOptionsRec[2] = null;
					arrayOptionsRec[3] = null;
					arrayOptionsRec[4] = null;
					arrayOptionsRec[5] = null;
					arrayOptionsRec[6] = null;
					arrayOptionsRec[7] = null;
				}

			}
			if (k != 0) {
				recsOption.add(
					new prod_machacc_rec(
						arrayOptionsRec[0],
						arrayOptionsRec[1],
						arrayOptionsRec[2],
						arrayOptionsRec[3],
						arrayOptionsRec[4],
						arrayOptionsRec[5],
						arrayOptionsRec[6],
						arrayOptionsRec[7]));
				k = 0;
				arrayOptionsRec[0] = null;
				arrayOptionsRec[1] = null;
				arrayOptionsRec[2] = null;
				arrayOptionsRec[3] = null;
				arrayOptionsRec[4] = null;
				arrayOptionsRec[5] = null;
				arrayOptionsRec[6] = null;
				arrayOptionsRec[7] = null;
			}

		} catch (StringException e) {
			response.write(
				"<BR> e.printStackTrace: " + e.getMessage() + "</BR>");
			e.printStackTrace();
		}

	}

	/*****************************************************************/
	/***             Retrieve data from M_Voltages                 ***/
	/*****************************************************************/

	public static String mdmGetVoltages(
		IPortalComponentRequest request,
		IPortalComponentResponse response,
		String voltages) {

		String sort = null;

		try {

			/*** create a ResultSetDefinition on the prod_machine_rec table ***/
			ResultSetDefinition rsd_m_voltages;
			rsd_m_voltages = new ResultSetDefinition("M_Voltages");
			rsd_m_voltages.AddField("Voltages");
			rsd_m_voltages.AddField("V_Sort");

			/*** create an empty Search object ***/
			Search search_voltages = new Search(rsd_m_voltages.GetTable());
			/*** Filters ***/
			FreeFormTableParameter fftp_V =
				search_voltages.GetParameters().NewFreeFormTableParameter(
					rsd_m_voltages.GetTable());
			FreeFormParameterField ffpf_V_Key01 =
				fftp_V.GetFields().New("Voltages");
			ffpf_V_Key01.GetFreeForm().NewString(
				voltages,
				FreeFormParameter.EqualToSearchType);

			/*** Printout Fields ***/
			FreeFormParameterField ffpf_V_Field00 =
				fftp_V.GetFields().New("Voltages");
			FreeFormParameterField ffpf_V_Field01 =
				fftp_V.GetFields().New("V_Sort");

			A2iResultSet rs_M =
				catalogData.GetResultSet(
					search_voltages,
					rsd_m_voltages,
					null,
					true,
					0);
			if (!rs_M.GetValueAt(0, "V_Sort").IsNull()) {
				sort = rs_M.GetValueAt(0, "V_Sort").TranslateToString();
			}
		} catch (StringException e) {
			response.write(
				"<BR> e.printStackTrace: " + e.getMessage() + "</BR>");
			e.printStackTrace();
		}
		sort = ("0000000000" + sort).substring(sort.length());
		return sort;
	}
	/*****************************************************************/
	/***             Retrieve data from prod_machine               ***/
	/*****************************************************************/
	public void mdmGetSelection(
		IPortalComponentRequest request,
		IPortalComponentResponse response,
		String family,
		String fld35String,
		String fld36String,
		String fld37String,
		String fld38String,
		String fld39String,
		String fld40String,
		String fld41String,
		String fld42String,
		String fld43String,
		String fld44String,
		String fld45String,
		String fld46String,
		String fld47String,
		String fld48String,
		String fld49String,
		String fld50String,
		String fld59String,
		String fld60String,
		String fld61String,
		String fld62String,
		String fld63String,
		String fld64String,
		String fld65String,
		String fld66String) {

		String[] arraySelection = new String[5];
		for (int j = 0; j < 6; j++) {
			arraySelection[0] = family;
			switch (j) {
				case 0 :
					arraySelection[1] = fld35String;
					arraySelection[2] = fld36String;
					arraySelection[3] = fld37String;
					arraySelection[4] = fld38String;
					break;
				case 1 :
					arraySelection[1] = fld39String;
					arraySelection[2] = fld40String;
					arraySelection[3] = fld41String;
					arraySelection[4] = fld42String;
					break;
				case 2 :
					arraySelection[1] = fld43String;
					arraySelection[2] = fld44String;
					arraySelection[3] = fld45String;
					arraySelection[4] = fld46String;
					break;
				case 3 :
					arraySelection[1] = fld47String;
					arraySelection[2] = fld48String;
					arraySelection[3] = fld49String;
					arraySelection[4] = fld50String;
					break;
				case 4 :
					arraySelection[1] = fld59String;
					arraySelection[2] = fld60String;
					arraySelection[3] = fld61String;
					arraySelection[4] = fld62String;
					break;
				case 5 :
					arraySelection[1] = fld63String;
					arraySelection[2] = fld64String;
					arraySelection[3] = fld65String;
					arraySelection[4] = fld66String;
					break;
				default :
					//					response.write("<BR>switch default: " + j + "</BR>");
			}
			//			response.write(
			//				"<BR>arraySelection[1]: " + arraySelection[1] + "</BR>");
			//			response.write(
			//				"<BR>arraySelection[2]: " + arraySelection[2] + "</BR>");
			//			response.write(
			//				"<BR>arraySelection[3]: " + arraySelection[3] + "</BR>");
			//			response.write(
			//				"<BR>arraySelection[4]: " + arraySelection[4] + "</BR>");

			if (arraySelection[1] != null
				|| arraySelection[2] != null
				|| arraySelection[3] != null
				|| arraySelection[4] != null) {
				recsSelection.add(
					new prod_machine_selection(
						arraySelection[0],
						arraySelection[1],
						arraySelection[2],
						arraySelection[3],
						arraySelection[4]));
				arraySelection[0] = null;
				arraySelection[1] = null;
				arraySelection[2] = null;
				arraySelection[3] = null;
				arraySelection[4] = null;
			}

		}
	}
	/*****************************************************************/
	/***             Retrieve data from prod_machine_rec           ***/
	/*****************************************************************/
	public void mdmGetMachineRec(
		IPortalComponentRequest request,
		IPortalComponentResponse response,
		String family) {
		getRecords = false;
		try {
			//			response.write("<BR>prod_machine_rec[" + "-</BR>");

			/*** create a ResultSetDefinition on the prod_machine_rec table ***/
			ResultSetDefinition rsd_prod_machine_rec;
			rsd_prod_machine_rec = new ResultSetDefinition("prod_machine_rec");
			rsd_prod_machine_rec.AddField("Name");
			rsd_prod_machine_rec.AddField("Description");
			rsd_prod_machine_rec.AddField("New_logo");
			rsd_prod_machine_rec.AddField("Voltages");
			rsd_prod_machine_rec.AddField("Number_phase");

			/*** create an empty Search object ***/
			Search search_options = new Search(rsd_prod_machine_rec.GetTable());
			/*** Filters ***/
			FreeFormTableParameter fftp_M =
				search_options.GetParameters().NewFreeFormTableParameter(
					rsd_prod_machine_rec.GetTable());
			FreeFormParameterField ffpf_M_Key01 =
				fftp_M.GetFields().New("category");
			ffpf_M_Key01.GetFreeForm().NewString(
				family,
				FreeFormParameter.EqualToSearchType);
			//			FreeFormParameterField ffpf_M_Key02 =
			//				fftp_M.GetFields().New("Public");
			//			ffpf_M_Key02.GetFreeForm().NewString(
			//				"YES",
			//				FreeFormParameter.EqualToSearchType);
			FreeFormParameterField ffpf_M_Key02 =
				fftp_M.GetFields().New("Printable");
			ffpf_M_Key02.GetFreeForm().NewString(
				"YES",
				FreeFormParameter.EqualToSearchType);
			FreeFormParameterField ffpf_M_Key03 =
				fftp_M.GetFields().New("phase_flag");
			ffpf_M_Key03.GetFreeForm().NewString(
				"APPROVED",
				FreeFormParameter.EqualToSearchType);

			/*** Printout Fields ***/
			FreeFormParameterField ffpf_S_Field00 =
				fftp_M.GetFields().New("Name");
			FreeFormParameterField ffpf_S_Field01 =
				fftp_M.GetFields().New("Description");
			FreeFormParameterField ffpf_S_Field02 =
				fftp_M.GetFields().New("New_logo");
			FreeFormParameterField ffpf_S_Field03 =
				fftp_M.GetFields().New("Voltages");
			FreeFormParameterField ffpf_S_Field04 =
				fftp_M.GetFields().New("Number_phase");

			A2iResultSet rs_M =
				catalogData.GetResultSet(
					search_options,
					rsd_prod_machine_rec,
					null,
					true,
					0);
			//						response.write(
			//							"<BR>prod_machine_rec recordCount: "
			//								+ rs_M.GetRecordCount()
			//								+ "</BR>");
			for (int i = 0; i < rs_M.GetRecordCount(); i++) {
				fldM01String = null;
				fldM02String = null;
				fldM03String = null;
				fldM04String = null;
				fldM05String = null;
				// S_Field00	String
				if (!rs_M.GetValueAt(i, "Name").IsNull()) {
					fldM01String =
						rs_M.GetValueAt(i, "Name").TranslateToString();
				}
				// S_Field01		String
				if (!rs_M.GetValueAt(i, "Description").IsNull()) {
					fldM02String =
						rs_M.GetValueAt(i, "Description").TranslateToString();
				}
				// S_Field02		Image
				if (!rs_M.GetValueAt(i, "New_logo").IsNull()) {
					CatalogCache catalogCache = new CatalogCache();
					catalogCache.Init(
						CACHE_SERVER,
						CACHE_PORT,
						CACHE_USER,
						CACHE_PASSWORD,
						CACHE_DIRECTORY,
						"English [US]");

					Object id = rs_M.GetValueAt(i, "New_logo").GetData();
					int idx = id.hashCode();
					fldM03String =
						CACHE_DIRECTORY
							+ catalogCache.GetImagePath(
								"Images",
								"Thumbnail",
								idx);
					catalogCache.Shutdown();
				}
				if (!rs_M.GetValueAt(i, "Voltages").IsNull()) {
					fldM04String =
						rs_M.GetValueAt(i, "Voltages").TranslateToString();
					fldM06String =
						mdmGetVoltages(request, response, fldM04String);
				}

				if (rs_M.GetValueAt(i, "Number_phase").IsNull() != true) {
					Measurement fldMeas05 =
						rs_M.GetValueAt(i, "Number_phase").GetMeasurement();
					fldM05String =
						measurementManager.GetString(
							fldMeas05.GetValue(),
							fldMeas05.GetUnitID(),
							rs_M.GetFieldAt("Number_phase").GetDimensionID(),
							rs_M.GetFieldAt("Number_phase").GetDecimalPlaces(),
							false,
							-1);
					fldM05String = fldM05String.replaceAll(",", ".");
				}

				if (paramReferences.equals("true") && paramType.equals("11")) {
					String sortField = family + fldM06String;
					recsMachineRec.add(
						new prod_machine_rec(
							sortField,
							family,
							fldM01String,
							fldM02String,
							fldM03String,
							fldM04String,
							fldM05String));
					getRecords = true;
				} else if (
					paramReferences.equals("false")
						&& paramType.equals("11")) {
					if (paramPartNumber.equals(fldM01String)) {
						String sortField = family;
						recsMachineRec.add(
							new prod_machine_rec(
								sortField,
								family,
								fldM01String,
								fldM02String,
								fldM03String,
								fldM04String,
								fldM05String));
						getRecords = true;
					}
				} else {
					String sortField = family + fldM06String;
					recsMachineRec.add(
						new prod_machine_rec(
							sortField,
							family,
							fldM01String,
							fldM02String,
							fldM03String,
							fldM04String,
							fldM05String));
					getRecords = true;
				}
			}
		} catch (StringException e) {
			response.write(
				"<BR> e.printStackTrace: " + e.getMessage() + "</BR>");
			e.printStackTrace();
		}

	}
	/*****************************************************************/
	/***             Retrieve data from prod_machacc_rec           ***/
	/*****************************************************************/
	public void mdmGetMachaccRec(
		IPortalComponentRequest request,
		IPortalComponentResponse response,
		String options,
		String family) {
		int k = 0;
		String[] arrayOptionsRec = new String[8];
		String newOptions = options.replaceAll(", ", ",");
		String[] arrayOptions = newOptions.split(";");
		try {
			for (int j = 0; j < arrayOptions.length; j++) {
				//				response.write(
				//					"<BR>arrayDieSets["
				//						+ j
				//						+ "]: -"
				//						+ arrayOptions[j]
				//						+ "-</BR>");

				//				response.write("<BR>prod_machacc_rec[" + "-</BR>");
				/*** create a ResultSetDefinition on the prod_machacc_rec table ***/
				ResultSetDefinition rsd_prod_machacc_rec;
				rsd_prod_machacc_rec =
					new ResultSetDefinition("prod_machacc_rec");
				rsd_prod_machacc_rec.AddField("Part_number");
				rsd_prod_machacc_rec.AddField("Description");
				rsd_prod_machacc_rec.AddField("Dieset_number");
				rsd_prod_machacc_rec.AddField("Crimp_range");
				rsd_prod_machacc_rec.AddField("Dimension_mm");
				rsd_prod_machacc_rec.AddField("Dimension_inch");

				/*** create an empty Search object ***/
				Search search_options =
					new Search(rsd_prod_machacc_rec.GetTable());
				/*** Filters ***/
				FreeFormTableParameter fftp_S =
					search_options.GetParameters().NewFreeFormTableParameter(
						rsd_prod_machacc_rec.GetTable());
				int partNumberIndex = arrayOptions[j].indexOf(",");
				String partNumber =
					arrayOptions[j].substring(0, partNumberIndex);
				//				response.write(
				//					"<BR>partNumber: -->" + partNumber + "<-- </BR>");
				FreeFormParameterField ffpf_S_Key01 =
					fftp_S.GetFields().New("Part_number");
				ffpf_S_Key01.GetFreeForm().NewString(
					partNumber,
					FreeFormParameter.EqualToSearchType);
				FreeFormParameterField ffpf_S_Key02 =
					fftp_S.GetFields().New("Printable");
				ffpf_S_Key02.GetFreeForm().NewString(
					"YES",
					FreeFormParameter.EqualToSearchType);
				FreeFormParameterField ffpf_S_Key03 =
					fftp_S.GetFields().New("Phase_flag");
				ffpf_S_Key03.GetFreeForm().NewString(
					"APPROVED",
					FreeFormParameter.EqualToSearchType);
				FreeFormParameterField ffpf_S_Key04 =
					fftp_S.GetFields().New("Public");
				ffpf_S_Key04.GetFreeForm().NewString(
					"YES",
					FreeFormParameter.EqualToSearchType);

				/*** Printout Fields ***/
				FreeFormParameterField ffpf_S_Field00 =
					fftp_S.GetFields().New("Part_number");
				FreeFormParameterField ffpf_S_Field01 =
					fftp_S.GetFields().New("Description");
				FreeFormParameterField ffpf_S_Field02 =
					fftp_S.GetFields().New("Dieset_number");
				FreeFormParameterField ffpf_S_Field03 =
					fftp_S.GetFields().New("Crimp_range");
				FreeFormParameterField ffpf_S_Field04 =
					fftp_S.GetFields().New("Dimension_mm");
				FreeFormParameterField ffpf_S_Field05 =
					fftp_S.GetFields().New("Dimension_inch");

				A2iResultSet rs_S =
					catalogData.GetResultSet(
						search_options,
						rsd_prod_machacc_rec,
						null,
						true,
						0);
				//				response.write(
				//					"<BR>prod_machacc_rec recordCount: "
				//						+ rs_S.GetRecordCount()
				//						+ "</BR>");
				for (int i = 0; i < rs_S.GetRecordCount(); i++) {
					fldS01String = null;
					fldS02String = null;
					fldS03String = null;
					fldS04String = null;
					fldS05String = null;
					fldS06String = null;
					// S_Field00	String
					if (!rs_S.GetValueAt(i, "Part_number").IsNull()) {
						fldS01String =
							rs_S
								.GetValueAt(i, "Part_number")
								.TranslateToString();
					}
					// S_Field01		String
					if (!rs_S.GetValueAt(i, "Description").IsNull()) {
						fldS02String =
							rs_S
								.GetValueAt(i, "Description")
								.TranslateToString();
					}
					// S_Field02		Measurement
					if (rs_S.GetValueAt(i, "Dieset_number").IsNull() != true) {
						Measurement fldMeas03 =
							rs_S
								.GetValueAt(i, "Dieset_number")
								.GetMeasurement();
						//						fldS03String =
						//							measurementManager.GetString(
						//								fldMeas03.GetValue(),
						//								fldMeas03.GetUnitID(),
						//								rs_S
						//									.GetFieldAt("Dieset_number")
						//									.GetDimensionID(),
						//								rs_S
						//									.GetFieldAt("Dieset_number")
						//									.GetDecimalPlaces(),
						//								false,
						//								-1);
						//						fldS03String = fldS03String.replaceAll(",", ".");
						fldS03Float =
							Float
								.valueOf(
									measurementManager.GetString(
										fldMeas03.GetValue(),
										0,
										0,
										0,
										false,
										-1))
								.floatValue();
						DecimalFormat myFormatter = new DecimalFormat("000");
						String output = myFormatter.format(fldS03Float);
						fldS03String = output;
					}
					// S_Field03		String
					if (!rs_S.GetValueAt(i, "Crimp_range").IsNull()) {
						fldS04String =
							rs_S
								.GetValueAt(i, "Crimp_range")
								.TranslateToString();
					}
					// S_Field04		Measurement
					if (rs_S.GetValueAt(i, "Dimension_mm").IsNull() != true) {
						Measurement fldMeas05 =
							rs_S.GetValueAt(i, "Dimension_mm").GetMeasurement();
						fldS05Float =
							Float
								.valueOf(
									measurementManager.GetString(
										fldMeas05.GetValue(),
										0,
										0,
										0,
										false,
										-1))
								.floatValue();
						int idx = Float.toString(fldS05Float).indexOf(".");
						fldS05String =
							Float.toString(fldS05Float).substring(0, idx);
					}
					// S_Field05		Measurement
					if (rs_S.GetValueAt(i, "Dimension_inch").IsNull()
						!= true) {
						Measurement fldMeas06 =
							rs_S
								.GetValueAt(i, "Dimension_inch")
								.GetMeasurement();
						fldS06String =
							measurementManager.GetString(
								fldMeas06.GetValue(),
								0,
								0,
								rs_S
									.GetFieldAt("Dimension_inch")
									.GetDecimalPlaces(),
								false,
								-1);
						fldS06String = fldS06String.replaceAll(",", ".");
					}
					String sortField = family + fldS03String;
					recsDieSets.add(
						new prod_machacc_rec(
							sortField,
							family,
							fldS01String,
							fldS02String,
							fldS03String,
							fldS04String,
							fldS05String,
							fldS06String));
				}

			}
		} catch (StringException e) {
			response.write(
				"<BR> e.printStackTrace: " + e.getMessage() + "</BR>");
			e.printStackTrace();
		}

	}
	/*****************************************************************/
	/***             Retrieve data from prod_machaccnew_rec        ***/
	/*****************************************************************/
	public void mdmGetMachaccNewRec(
		IPortalComponentRequest request,
		IPortalComponentResponse response,
		String options,
		String family) {
		int k = 0;
		String[] arrayOptionsRec = new String[8];
		String newOptions = options.replaceAll(", ", ",");
		String[] arrayOptions = newOptions.split(";");
		try {
			for (int j = 0; j < arrayOptions.length; j++) {
				//				response.write(
				//					"<BR>arrayDieSets["
				//						+ j
				//						+ "]: -"
				//						+ arrayOptions[j]
				//						+ "-</BR>");

				//				response.write("<BR>prod_machacc_rec[" + "-</BR>");
				/*** create a ResultSetDefinition on the prod_machacc_rec table ***/
				ResultSetDefinition rsd_prod_machaccnew_rec;
				rsd_prod_machaccnew_rec =
					new ResultSetDefinition("prod_machacc_rec");
				rsd_prod_machaccnew_rec.AddField("Part_number");
				rsd_prod_machaccnew_rec.AddField("Description");
				rsd_prod_machaccnew_rec.AddField("Dieset_number");
				rsd_prod_machaccnew_rec.AddField("Crimp_range");
				rsd_prod_machaccnew_rec.AddField("Dimension_mm");
				rsd_prod_machaccnew_rec.AddField("Dimension_inch");

				/*** create an empty Search object ***/
				Search search_options =
					new Search(rsd_prod_machaccnew_rec.GetTable());
				/*** Filters ***/
				FreeFormTableParameter fftp_S =
					search_options.GetParameters().NewFreeFormTableParameter(
						rsd_prod_machaccnew_rec.GetTable());
				int partNumberIndex = arrayOptions[j].indexOf(",");
				String partNumber =
					arrayOptions[j].substring(0, partNumberIndex);
				//				response.write(
				//					"<BR>partNumber: -->" + partNumber + "<-- </BR>");
				FreeFormParameterField ffpf_S_Key01 =
					fftp_S.GetFields().New("Part_number");
				ffpf_S_Key01.GetFreeForm().NewString(
					partNumber,
					FreeFormParameter.EqualToSearchType);
				FreeFormParameterField ffpf_S_Key02 =
					fftp_S.GetFields().New("Printable");
				ffpf_S_Key02.GetFreeForm().NewString(
					"YES",
					FreeFormParameter.EqualToSearchType);
				FreeFormParameterField ffpf_S_Key03 =
					fftp_S.GetFields().New("Phase_flag");
				ffpf_S_Key03.GetFreeForm().NewString(
					"APPROVED",
					FreeFormParameter.EqualToSearchType);
				FreeFormParameterField ffpf_S_Key04 =
					fftp_S.GetFields().New("Public");
				ffpf_S_Key04.GetFreeForm().NewString(
					"YES",
					FreeFormParameter.EqualToSearchType);

				/*** Printout Fields ***/
				FreeFormParameterField ffpf_S_Field00 =
					fftp_S.GetFields().New("Part_number");
				FreeFormParameterField ffpf_S_Field01 =
					fftp_S.GetFields().New("Description");
				FreeFormParameterField ffpf_S_Field02 =
					fftp_S.GetFields().New("Dieset_number");
				FreeFormParameterField ffpf_S_Field03 =
					fftp_S.GetFields().New("Crimp_range");
				FreeFormParameterField ffpf_S_Field04 =
					fftp_S.GetFields().New("Dimension_mm");
				FreeFormParameterField ffpf_S_Field05 =
					fftp_S.GetFields().New("Dimension_inch");

				A2iResultSet rs_S =
					catalogData.GetResultSet(
						search_options,
						rsd_prod_machaccnew_rec,
						null,
						true,
						0);
				//				response.write(
				//					"<BR>prod_machacc_rec recordCount: "
				//						+ rs_S.GetRecordCount()
				//						+ "</BR>");
				for (int i = 0; i < rs_S.GetRecordCount(); i++) {
					fldS01String = null;
					fldS02String = null;
					fldS03String = null;
					fldS04String = null;
					fldS05String = null;
					fldS06String = null;
					// S_Field00	String
					if (!rs_S.GetValueAt(i, "Part_number").IsNull()) {
						fldS01String =
							rs_S
								.GetValueAt(i, "Part_number")
								.TranslateToString();
					}
					// S_Field01		String
					if (!rs_S.GetValueAt(i, "Description").IsNull()) {
						fldS02String =
							rs_S
								.GetValueAt(i, "Description")
								.TranslateToString();
					}
					// S_Field02		Measurement
					if (rs_S.GetValueAt(i, "Dieset_number").IsNull() != true) {
						Measurement fldMeas03 =
							rs_S
								.GetValueAt(i, "Dieset_number")
								.GetMeasurement();
						//						fldS03String =
						//							measurementManager.GetString(
						//								fldMeas03.GetValue(),
						//								fldMeas03.GetUnitID(),
						//								rs_S
						//									.GetFieldAt("Dieset_number")
						//									.GetDimensionID(),
						//								rs_S
						//									.GetFieldAt("Dieset_number")
						//									.GetDecimalPlaces(),
						//								false,
						//								-1);
						//						fldS03String = fldS03String.replaceAll(",", ".");
						fldS03Float =
							Float
								.valueOf(
									measurementManager.GetString(
										fldMeas03.GetValue(),
										0,
										0,
										0,
										false,
										-1))
								.floatValue();
						DecimalFormat myFormatter = new DecimalFormat("000");
						String output = myFormatter.format(fldS03Float);
						fldS03String = output;
					}
					// S_Field03		String
					if (!rs_S.GetValueAt(i, "Crimp_range").IsNull()) {
						fldS04String =
							rs_S
								.GetValueAt(i, "Crimp_range")
								.TranslateToString();
					}
					// S_Field04		Measurement
					if (rs_S.GetValueAt(i, "Dimension_mm").IsNull() != true) {
						Measurement fldMeas05 =
							rs_S.GetValueAt(i, "Dimension_mm").GetMeasurement();
						fldS05Float =
							Float
								.valueOf(
									measurementManager.GetString(
										fldMeas05.GetValue(),
										0,
										0,
										0,
										false,
										-1))
								.floatValue();
						int idx = Float.toString(fldS05Float).indexOf(".");
						fldS05String =
							Float.toString(fldS05Float).substring(0, idx);
					}
					// S_Field05		Measurement
					if (rs_S.GetValueAt(i, "Dimension_inch").IsNull()
						!= true) {
						Measurement fldMeas06 =
							rs_S
								.GetValueAt(i, "Dimension_inch")
								.GetMeasurement();
						fldS06String =
							measurementManager.GetString(
								fldMeas06.GetValue(),
								0,
								0,
								rs_S
									.GetFieldAt("Dimension_inch")
									.GetDecimalPlaces(),
								false,
								-1);
						fldS06String = fldS06String.replaceAll(",", ".");
					}
					String sortField = family + fldS03String;
					recsAvailAcc.add(
						new prod_machacc_rec(
							sortField,
							family,
							fldS01String,
							fldS02String,
							fldS03String,
							fldS04String,
							fldS05String,
							fldS06String));
				}

			}
		} catch (StringException e) {
			response.write(
				"<BR> e.printStackTrace: " + e.getMessage() + "</BR>");
			e.printStackTrace();
		}

	}

	//	-------------- Retrieve data from prod_hoses ------------------------------
	public void mdmGetProdHoses(
		IPortalComponentRequest request,
		IPortalComponentResponse response) {

		if (paramType.equals("1") || paramType.equals("2")) {

			// create a ResultSetDefinition on the prod_hoses table
			ResultSetDefinition rsd_prod_hoses;
			rsd_prod_hoses = new ResultSetDefinition(paramTableHeader);
			rsd_prod_hoses.AddField("Name");
			rsd_prod_hoses.AddField("catalogue_subtitle");
			rsd_prod_hoses.AddField("catalogue_image");
			rsd_prod_hoses.AddField("key_performance");

			rsd_prod_hoses.AddField("main_application");
			rsd_prod_hoses.AddField("min_cont_serv_temp");
			rsd_prod_hoses.AddField("max_cont_serv_temp");
			rsd_prod_hoses.AddField("max_operating_temp");
			rsd_prod_hoses.AddField("min_external_temp");
			rsd_prod_hoses.AddField("recommended_fluids");
			rsd_prod_hoses.AddField("refrigerants");
			rsd_prod_hoses.AddField("lubrificants");
			rsd_prod_hoses.AddField("tube");
			rsd_prod_hoses.AddField("reinforcement");
			rsd_prod_hoses.AddField("cover");
			rsd_prod_hoses.AddField("applicable_specs");
			rsd_prod_hoses.AddField("type_approvals");
			rsd_prod_hoses.AddField("other_technical_notes");
			rsd_prod_hoses.AddField("standard_Packaging");
			rsd_prod_hoses.AddField("catalogue_small_exclamation");
			rsd_prod_hoses.AddField("catalogue_big_exclamation");
			rsd_prod_hoses.AddField("tree");
			//			----          INSERT ML 30.01.2013
			rsd_prod_hoses.AddField("version");
			rsd_prod_hoses.AddField("Version_Date");
			rsd_prod_hoses.AddField("Applications_Fluids");
			rsd_prod_hoses.AddField("catalogue_bottom_exclamation");
			//			----          INSERT ML 30.01.2013
			// create an empty Search object
			Search search_prod_hoses = new Search(rsd_prod_hoses.GetTable());

			// Filters		
			FreeFormTableParameter fftp_H =
				search_prod_hoses.GetParameters().NewFreeFormTableParameter(
					rsd_prod_hoses.GetTable());

			FreeFormParameterField ffpf_H_Key01 =
				fftp_H.GetFields().New("Name");
			ffpf_H_Key01.GetFreeForm().NewString(
				paramValue,
				FreeFormParameter.EqualToSearchType);

			FreeFormParameterField ffpf_H_Key02 =
				fftp_H.GetFields().New("tree");
			ffpf_H_Key02.GetFreeForm().NewString(
				"Yes",
				FreeFormParameter.NotNullSearchType);

			// Printout Fields 
			FreeFormParameterField ffpf_H_Field00 =
				fftp_H.GetFields().New("Name");
			FreeFormParameterField ffpf_H_Field01 =
				fftp_H.GetFields().New("catalogue_subtitle");
			FreeFormParameterField ffpf_H_Field02 =
				fftp_H.GetFields().New("catalogue_image");
			FreeFormParameterField ffpf_H_Field03 =
				fftp_H.GetFields().New("key_performance");
			FreeFormParameterField ffpf_H_Field04 =
				fftp_H.GetFields().New("main_application");
			FreeFormParameterField ffpf_H_Field05 =
				fftp_H.GetFields().New("min_cont_serv_temp");
			FreeFormParameterField ffpf_H_Field06 =
				fftp_H.GetFields().New("max_cont_serv_temp");
			FreeFormParameterField ffpf_H_Field07 =
				fftp_H.GetFields().New("max_operating_temp");
			FreeFormParameterField ffpf_H_Field08 =
				fftp_H.GetFields().New("min_external_temp");
			FreeFormParameterField ffpf_H_Field09 =
				fftp_H.GetFields().New("recommended_fluids");
			FreeFormParameterField ffpf_H_Field10 =
				fftp_H.GetFields().New("refrigerants");
			FreeFormParameterField ffpf_H_Field11 =
				fftp_H.GetFields().New("lubrificants");
			FreeFormParameterField ffpf_H_Field12 =
				fftp_H.GetFields().New("tube");
			FreeFormParameterField ffpf_H_Field13 =
				fftp_H.GetFields().New("reinforcement");
			FreeFormParameterField ffpf_H_Field14 =
				fftp_H.GetFields().New("cover");
			FreeFormParameterField ffpf_H_Field15 =
				fftp_H.GetFields().New("applicable_specs");
			FreeFormParameterField ffpf_H_Field16 =
				fftp_H.GetFields().New("type_approvals");
			FreeFormParameterField ffpf_H_Field17 =
				fftp_H.GetFields().New("other_technical_notes");
			FreeFormParameterField ffpf_H_Field18 =
				fftp_H.GetFields().New("standard_Packaging");
			FreeFormParameterField ffpf_H_Field19 =
				fftp_H.GetFields().New("catalogue_small_exclamation");
			FreeFormParameterField ffpf_H_Field20 =
				fftp_H.GetFields().New("catalogue_big_exclamation");
			FreeFormParameterField ffpf_H_Field21 =
				fftp_H.GetFields().New("tree");
			//			----          INSERT ML 30.01.2013
			FreeFormParameterField ffpf_H_Field22 =
				fftp_H.GetFields().New("version");
			FreeFormParameterField ffpf_H_Field23 =
				fftp_H.GetFields().New("Version_Date");
			FreeFormParameterField ffpf_H_Field24 =
				fftp_H.GetFields().New("Applications_Fluids");
			FreeFormParameterField ffpf_H_Field25 =
				fftp_H.GetFields().New("catalogue_bottom_exclamation");

			 			
			//			----          INSERT ML 30.01.2013

			try {
				A2iResultSet rs_H =
					catalogData.GetResultSet(
						search_prod_hoses,
						rsd_prod_hoses,
						null,
						true,
						0);
						
						for (int i = 0; i < rs_H.GetRecordCount(); i++) {
											if (!rs_H.GetValueAt(i, "tree").IsNull()) {
												// H_Field00
												if (!rs_H.GetValueAt(i, "Name").IsNull()) {
													Name = rs_H.GetValueAt(i, "Name").GetStringValue();
												} else {
													Name = null;
												}
												// H_Field01
												if (!rs_H
													.GetValueAt(i, "catalogue_subtitle")
													.IsNull()) {
													catalogue_subtitle =
														rs_H
															.GetValueAt(i, "catalogue_subtitle")
															.GetStringValue();
												} else {
													catalogue_subtitle = null;
												}
												// H_Field02
												if (!rs_H.GetValueAt(i, "catalogue_image").IsNull()) {
													CatalogCache catalogCache = new CatalogCache();
													catalogCache.Init(
														CACHE_SERVER,
														CACHE_PORT,
														CACHE_USER,
														CACHE_PASSWORD,
														CACHE_DIRECTORY,
														"English [US]");

													Object id =
														rs_H.GetValueAt(i, "catalogue_image").GetData();
													int idx = id.hashCode();
													imagePath =
														catalogCache.GetImagePath(
															"Images",
															"Original",
															idx);
													catalogCache.Shutdown();
												} else {
													imagePath = null;
												}
												// H_Field03
												if (!rs_H.GetValueAt(i, "key_performance").IsNull()) {
													key_performance =
														rs_H
															.GetValueAt(i, "key_performance")
															.GetStringValue();
												} else {
													key_performance = null;
												}
												// H_Field04
												if (!rs_H.GetValueAt(i, "main_application").IsNull()) {
													main_application =
														rs_H
															.GetValueAt(i, "main_application")
															.GetStringValue();
												} else {
													main_application = null;
												}
												// H_Field05
												if (!rs_H
													.GetValueAt(i, "min_cont_serv_temp")
													.IsNull()) {
													min_cont_serv_temp =
														rs_H
															.GetValueAt(i, "min_cont_serv_temp")
															.GetStringValue();
												} else {
													min_cont_serv_temp = null;
												}
												// H_Field06
												if (!rs_H
													.GetValueAt(i, "max_cont_serv_temp")
													.IsNull()) {
													max_cont_serv_temp =
														rs_H
															.GetValueAt(i, "max_cont_serv_temp")
															.GetStringValue();
												} else {
													max_cont_serv_temp = null;
												}
												// H_Field07
												if (rs_H.GetValueAt(i, "max_operating_temp").IsNull()
													!= true) {
													max_operating_temp =
														rs_H
															.GetValueAt(i, "max_operating_temp")
															.GetStringValue();
												} else {
													max_operating_temp = null;
												}
												// H_Field08
												if (rs_H.GetValueAt(i, "min_external_temp").IsNull()
													!= true) {
													min_external_temp =
														rs_H
															.GetValueAt(i, "min_external_temp")
															.GetStringValue();
												} else {
													min_external_temp = null;
												}
												// H_Field09
												if (rs_H.GetValueAt(i, "recommended_fluids").IsNull()
													!= true) {
													recommended_fluids =
														rs_H
															.GetValueAt(i, "recommended_fluids")
															.GetStringValue();
												} else {
													recommended_fluids = null;
												}
												// H_Field10
												if (rs_H.GetValueAt(i, "refrigerants").IsNull()
													!= true) {
													refrigerants =
														rs_H
															.GetValueAt(i, "refrigerants")
															.GetStringValue();
												} else {
													refrigerants = null;
												}
												// H_Field11
												if (rs_H.GetValueAt(i, "lubrificants").IsNull()
													!= true) {
													lubrificants =
														rs_H
															.GetValueAt(i, "lubrificants")
															.GetStringValue();
												} else {
													lubrificants = null;
												}
												// H_Field12
												if (rs_H.GetValueAt(i, "tube").IsNull() != true) {
													tube = rs_H.GetValueAt(i, "tube").GetStringValue();
												} else {
													tube = null;
												}

												// H_Field13
												if (rs_H.GetValueAt(i, "reinforcement").IsNull()
													!= true) {
													reinforcement =
														rs_H
															.GetValueAt(i, "reinforcement")
															.GetStringValue();
												} else {
													reinforcement = null;
												}
												// H_Field14
												if (rs_H.GetValueAt(i, "cover").IsNull() != true) {
													cover =
														rs_H.GetValueAt(i, "cover").GetStringValue();
												} else {
													cover = null;
												}
												// H_Field15
												if (rs_H.GetValueAt(i, "applicable_specs").IsNull()
													!= true) {
													applicable_specs =
														rs_H
															.GetValueAt(i, "applicable_specs")
															.GetStringValue();
												} else {
													applicable_specs = null;
												}
												// H_Field16
												if (rs_H.GetValueAt(i, "type_approvals").IsNull()
													!= true) {
													type_approvals =
														rs_H
															.GetValueAt(i, "type_approvals")
															.GetStringValue();
												} else {
													type_approvals = null;
												}
												// H_Field17
												if (rs_H
													.GetValueAt(i, "other_technical_notes")
													.IsNull()
													!= true) {
													other_technical_notes =
														rs_H
															.GetValueAt(i, "other_technical_notes")
															.GetStringValue();
												} else {
													other_technical_notes = null;
												}
												// H_Field18
												if (rs_H.GetValueAt(i, "standard_Packaging").IsNull()
													!= true) {
													standard_Packaging =
														rs_H
															.GetValueAt(i, "standard_Packaging")
															.GetStringValue();
												} else {
													standard_Packaging = null;
												}
												// H_Field19
												if (rs_H
													.GetValueAt(i, "catalogue_small_exclamation")
													.IsNull()
													!= true) {
													catalogue_small_exclamation =
														rs_H
															.GetValueAt(
																i,
																"catalogue_small_exclamation")
															.GetStringValue();
												} else {
													catalogue_small_exclamation = null;
												}
												// H_Field20
												if (rs_H
													.GetValueAt(i, "catalogue_big_exclamation")
													.IsNull()
													!= true) {
													catalogue_big_exclamation =
														rs_H
															.GetValueAt(i, "catalogue_big_exclamation")
															.GetStringValue();
												} else {
													catalogue_big_exclamation = null;
												}
												//						----          INSERT ML 30.01.2013
												// H_Field22
												if (rs_H.GetValueAt(i, "version").IsNull() != true) {
													version =
														rs_H.GetValueAt(i, "version").GetStringValue();
												} else {
													version = "";
												}
												// H_Field23
												if (rs_H.GetValueAt(i, "Version_Date").IsNull()
													!= true) {
													String v_dateTime =
														rs_H
															.GetValueAt(i, "Version_Date")
															.GetSystemTime()
															.ConvertSystemTimeToString();
													version_date =
														v_dateTime.substring(8, 10)
															+ "/"
															+ v_dateTime.substring(5, 7)
															+ "/"
															+ v_dateTime.substring(0, 4);
												} else {
													version_date = "";
												}
												if (!rs_H
													.GetValueAt(i, "Applications_Fluids")
													.IsNull()) {
													applications_fluids =
														rs_H
															.GetValueAt(i, "Applications_Fluids")
															.GetStringValue();
												} else {
													applications_fluids = null;
												}
												// H_Field25
												if (rs_H
													.GetValueAt(i, "catalogue_bottom_exclamation")
													.IsNull()
													!= true) {
													catalogue_bottom_exclamation =
														rs_H
															.GetValueAt(
																i,
																"catalogue_bottom_exclamation")
															.GetStringValue();
												} else {
													catalogue_bottom_exclamation = null;
												}

												//						----          INSERT ML 30.01.2013
											}
										}
		}			
			 catch (StringException e) {
				e.printStackTrace();
			}
		}
	}
	//	-------------- Retrieve data from prod_hoses_rec --------------------------

	public void mdmGetProdHosesRec(
		IPortalComponentRequest request,
		IPortalComponentResponse response) {

		//		  create a ResultSetDefinition on the prod_hoses_rec table
		ResultSetDefinition rsd_prod_hoses_rec;
		rsd_prod_hoses_rec = new ResultSetDefinition(paramTableItems);
		rsd_prod_hoses_rec.AddField("Name");
		rsd_prod_hoses_rec.AddField("line");
		rsd_prod_hoses_rec.AddField("nominal_size");
		rsd_prod_hoses_rec.AddField("reinforcement_outside_diameter_mm");
		rsd_prod_hoses_rec.AddField("reinforcement_outside_diameter_inch");
		rsd_prod_hoses_rec.AddField("outside_diameter_mm");
		rsd_prod_hoses_rec.AddField("outside_diameter_inch");
		rsd_prod_hoses_rec.AddField("Max_working_pressure_bar");
		rsd_prod_hoses_rec.AddField("max_working_pressure_psi");
		rsd_prod_hoses_rec.AddField("burst_pressure_bar");
		rsd_prod_hoses_rec.AddField("burst_pressure_psi");
		rsd_prod_hoses_rec.AddField("min_bend_radius_mm");
		rsd_prod_hoses_rec.AddField("min_bend_radius_inch");
		rsd_prod_hoses_rec.AddField("weight_g_m");
		rsd_prod_hoses_rec.AddField("weight_lbs_ft");
		//		----          DELETE ML 30.01.2013
		//		rsd_prod_hoses_rec.AddField("standard_ferrule");
		//		rsd_prod_hoses_rec.AddField("standard_insert");
		//		----          DELETE ML 30.01.2013
		//		----          INSERT ML 30.01.2013
		rsd_prod_hoses_rec.AddField("Std_Ferrule1");
		rsd_prod_hoses_rec.AddField("Std_Insert1");
		rsd_prod_hoses_rec.AddField("Std_Ferrule2");
		rsd_prod_hoses_rec.AddField("Std_Insert2");
		rsd_prod_hoses_rec.AddField("Alt_Ferrule1");
		rsd_prod_hoses_rec.AddField("Alt_Insert1");
		rsd_prod_hoses_rec.AddField("Alt_Ferrule2");
		rsd_prod_hoses_rec.AddField("Alt_Insert2");
		//		----          INSERT ML 30.01.2013
		rsd_prod_hoses_rec.AddField("standard_mf3000");

		//		  variabili per stampa Catalogue Extended	
		rsd_prod_hoses_rec.AddField("hose_inside_diameter_min_mm");
		rsd_prod_hoses_rec.AddField("hose_inside_diameter_min_inches");
		rsd_prod_hoses_rec.AddField("hose_inside_diameter_max_mm");
		rsd_prod_hoses_rec.AddField("hose_inside_diameter_max_inches");
		rsd_prod_hoses_rec.AddField("reinforcement_outside_diameter_min_mm");
		rsd_prod_hoses_rec.AddField(
			"reinforcement_outside_diameter_min_inches");
		rsd_prod_hoses_rec.AddField("reinforcement_outside_diameter_max_mm");
		rsd_prod_hoses_rec.AddField(
			"reinforcement_outside_diameter_max_inches");
		rsd_prod_hoses_rec.AddField("hose_outside_diameter_min_mm");
		rsd_prod_hoses_rec.AddField("hose_outside_diameter_min_inches");
		rsd_prod_hoses_rec.AddField("hose_outside_diameter_max_mm");
		rsd_prod_hoses_rec.AddField("hose_outside_diameter_max_inches");
		rsd_prod_hoses_rec.AddField("hose_max_wall_thickness_difference_mm");
		rsd_prod_hoses_rec.AddField(
			"hose_max_wall_thickness_difference_inches");
		//		  variabili per stampa Catalogue Extended	

		//		  variabili per stampa Marcature
		rsd_prod_hoses_rec.AddField("category");
		rsd_prod_hoses_rec.AddField("brand_pack");
		rsd_prod_hoses_rec.AddField("brand_customer");
		rsd_prod_hoses_rec.AddField("marking_technology");
		rsd_prod_hoses_rec.AddField("brand_color");
		rsd_prod_hoses_rec.AddField("brand_logo");
		rsd_prod_hoses_rec.AddField("brand_notes");
		rsd_prod_hoses_rec.AddField("brand_date_type");
		rsd_prod_hoses_rec.AddField("brand_customer");
		rsd_prod_hoses_rec.AddField("department");
		rsd_prod_hoses_rec.AddField("brand");
		//		  variabili per stampa Marcature

		rsd_prod_hoses_rec.AddField("category");
		rsd_prod_hoses_rec.AddField("Printable");
		rsd_prod_hoses_rec.AddField("public");
		rsd_prod_hoses_rec.AddField("Phase");

		// create an empty Search object
		Search search = new Search(rsd_prod_hoses_rec.GetTable());
		FreeFormTableParameter fftpNames =
			search.GetParameters().NewFreeFormTableParameter(
				rsd_prod_hoses_rec.GetTable());

		// Filters		
		if (paramType.equals("3")) {
			FreeFormParameterField ffpfKey01 =
				fftpNames.GetFields().New("brand_pack");
			ffpfKey01.GetFreeForm().NewString(
				paramBrand,
				FreeFormParameter.EqualToSearchType);
			if (paramCategory.equals("false")) {
				FreeFormParameterField ffpfKey02 =
					fftpNames.GetFields().New("category");
				ffpfKey02.GetFreeForm().NewString(
					paramValue,
					FreeFormParameter.EqualToSearchType);
			}
			FreeFormParameterField ffpf_M_Key03 =
				fftpNames.GetFields().New("Printable");
			ffpf_M_Key03.GetFreeForm().NewString(
				"YES",
				FreeFormParameter.EqualToSearchType);
			FreeFormParameterField ffpf_M_Key04 =
				fftpNames.GetFields().New("phase_flag");
			ffpf_M_Key04.GetFreeForm().NewString(
				"APPROVED",
				FreeFormParameter.EqualToSearchType);
		} else {
			FreeFormParameterField ffpfKey01 =
				fftpNames.GetFields().New("category");
			ffpfKey01.GetFreeForm().NewString(
				paramValue,
				FreeFormParameter.EqualToSearchType);
			if (paramCatalog.equals("false")) {
				FreeFormParameterField ffpfKey03 =
					fftpNames.GetFields().New("catalogue");
				ffpfKey03.GetFreeForm().NewString(
					"YES",
					FreeFormParameter.SubstringSearchType);
			}
			FreeFormParameterField ffpf_M_Key03 =
				fftpNames.GetFields().New("Printable");
			ffpf_M_Key03.GetFreeForm().NewString(
				"YES",
				FreeFormParameter.EqualToSearchType);
			FreeFormParameterField ffpf_M_Key04 =
				fftpNames.GetFields().New("phase_flag");
			ffpf_M_Key04.GetFreeForm().NewString(
				"APPROVED",
				FreeFormParameter.EqualToSearchType);

		}

		//			FreeFormParameterField ffpfKey03 = fftpNames.GetFields().New("public");
		//			ffpfKey03.GetFreeForm().NewString("YES", FreeFormParameter.SubstringSearchType);
		FreeFormParameterField ffpfKey04 = fftpNames.GetFields().New("Phase");
		ffpfKey04.GetFreeForm().NewString(
			"BLOCKED",
			FreeFormParameter.NotEqualToSearchType);

		//		  Printout Fields 
		FreeFormParameterField ffpfField00 = fftpNames.GetFields().New("Name");
		FreeFormParameterField ffpfField01 = fftpNames.GetFields().New("line");
		FreeFormParameterField ffpfField02 =
			fftpNames.GetFields().New("nominal_size");
		FreeFormParameterField ffpfField03 =
			fftpNames.GetFields().New("reinforcement_outside_diameter_mm");
		FreeFormParameterField ffpfField04 =
			fftpNames.GetFields().New("reinforcement_outside_diameter_inch");
		FreeFormParameterField ffpfField05 =
			fftpNames.GetFields().New("outside_diameter_mm");
		FreeFormParameterField ffpfField06 =
			fftpNames.GetFields().New("outside_diameter_inch");
		FreeFormParameterField ffpfField07 =
			fftpNames.GetFields().New("Max_working_pressure_bar");
		FreeFormParameterField ffpfField08 =
			fftpNames.GetFields().New("max_working_pressure_psi");
		FreeFormParameterField ffpfField09 =
			fftpNames.GetFields().New("burst_pressure_bar");
		FreeFormParameterField ffpfField10 =
			fftpNames.GetFields().New("burst_pressure_psi");
		FreeFormParameterField ffpfField11 =
			fftpNames.GetFields().New("min_bend_radius_mm");
		FreeFormParameterField ffpfField12 =
			fftpNames.GetFields().New("min_bend_radius_inch");
		FreeFormParameterField ffpfField13 =
			fftpNames.GetFields().New("weight_g_m");
		FreeFormParameterField ffpfField14 =
			fftpNames.GetFields().New("weight_lbs_ft");
		//			----          DELETE ML 30.01.2013
		//		FreeFormParameterField ffpfField15 =
		//			fftpNames.GetFields().New("standard_ferrule");
		//		FreeFormParameterField ffpfField16 =
		//			fftpNames.GetFields().New("standard_insert");
		//			----          DELETE ML 30.01.2013
		//			----          INSERT ML 30.01.2013
		FreeFormParameterField ffpfField15 =
			fftpNames.GetFields().New("Std_Ferrule1");
		FreeFormParameterField ffpfField16 =
			fftpNames.GetFields().New("Std_Insert1");
		FreeFormParameterField ffpfField55 =
			fftpNames.GetFields().New("Std_Ferrule2");
		FreeFormParameterField ffpfField56 =
			fftpNames.GetFields().New("Std_Insert2");

		FreeFormParameterField ffpfField65 =
			fftpNames.GetFields().New("Alt_Ferrule1");
		FreeFormParameterField ffpfField66 =
			fftpNames.GetFields().New("Alt_Insert1");
		FreeFormParameterField ffpfField75 =
			fftpNames.GetFields().New("Alt_Ferrule2");
		FreeFormParameterField ffpfField76 =
			fftpNames.GetFields().New("Alt_Insert2");
		//			----          INSERT ML 30.01.2013

		FreeFormParameterField ffpfField17 =
			fftpNames.GetFields().New("standard_mf3000");

		//		  variabili per stampa Catalogue Extended	
		FreeFormParameterField ffpfField18 =
			fftpNames.GetFields().New("hose_inside_diameter_min_mm");
		FreeFormParameterField ffpfField19 =
			fftpNames.GetFields().New("hose_inside_diameter_min_inches");
		FreeFormParameterField ffpfField20 =
			fftpNames.GetFields().New("hose_inside_diameter_max_mm");
		FreeFormParameterField ffpfField21 =
			fftpNames.GetFields().New("hose_inside_diameter_max_inches");
		FreeFormParameterField ffpfField22 =
			fftpNames.GetFields().New("reinforcement_outside_diameter_min_mm");
		FreeFormParameterField ffpfField23 =
			fftpNames.GetFields().New(
				"reinforcement_outside_diameter_min_inches");
		FreeFormParameterField ffpfField24 =
			fftpNames.GetFields().New("reinforcement_outside_diameter_max_mm");
		FreeFormParameterField ffpfField25 =
			fftpNames.GetFields().New(
				"reinforcement_outside_diameter_max_inches");
		FreeFormParameterField ffpfField26 =
			fftpNames.GetFields().New("hose_outside_diameter_min_mm");
		FreeFormParameterField ffpfField27 =
			fftpNames.GetFields().New("hose_outside_diameter_min_inches");
		FreeFormParameterField ffpfField28 =
			fftpNames.GetFields().New("hose_outside_diameter_max_mm");
		FreeFormParameterField ffpfField29 =
			fftpNames.GetFields().New("hose_outside_diameter_max_inches");
		FreeFormParameterField ffpfField30 =
			fftpNames.GetFields().New("hose_max_wall_thickness_difference_mm");
		FreeFormParameterField ffpfField31 =
			fftpNames.GetFields().New(
				"hose_max_wall_thickness_difference_inches");
		//		  variabili per stampa Catalogue Extended	

		//		  variabili per stampa Marcature
		FreeFormParameterField ffpfField32 =
			fftpNames.GetFields().New("category");
		FreeFormParameterField ffpfField33 =
			fftpNames.GetFields().New("brand_pack");
		FreeFormParameterField ffpfField34 =
			fftpNames.GetFields().New("brand_customer");
		FreeFormParameterField ffpfField35 =
			fftpNames.GetFields().New("marking_technology");
		FreeFormParameterField ffpfField36 =
			fftpNames.GetFields().New("brand_color");
		FreeFormParameterField ffpfField37 =
			fftpNames.GetFields().New("brand_logo");
		FreeFormParameterField ffpfField38 =
			fftpNames.GetFields().New("brand_notes");
		FreeFormParameterField ffpfField39 =
			fftpNames.GetFields().New("brand_date_type");
		FreeFormParameterField ffpfField40 =
			fftpNames.GetFields().New("brand_customer");
		FreeFormParameterField ffpfField41 =
			fftpNames.GetFields().New("department");
		FreeFormParameterField ffpfField42 = fftpNames.GetFields().New("brand");
		//		  variabili per stampa Marcature

		// Ordinamento records
		A2iResultSet rs;
		try {
			Object fields = null;
			Object locale = null;
			if (paramType.equals("3")) {
				rs =
					catalogData.GetResultSet(
						search,
						rsd_prod_hoses_rec,
						ffpfField32.GetName(),
						true,
						0);

			} else {
				rs =
					catalogData.GetResultSet(
						search,
						rsd_prod_hoses_rec,
						ffpfField01.GetName(),
						true,
						0);

			}
			//			Vector recs = new Vector();

			for (int i = 0; i < rs.GetRecordCount(); i++) {
				// Field01  PART.REF.
				fld01String = rs.GetValueAt(i, "line").GetStringValue();

				int idx1 =
					rs.GetValueAt(i, "nominal_size").GetStringValue().indexOf(
						",",
						0);
				int idx2 =
					rs.GetValueAt(i, "nominal_size").GetStringValue().indexOf(
						",",
						idx1 + 1);
				int idx3 =
					rs.GetValueAt(i, "nominal_size").GetStringValue().indexOf(
						",",
						idx2 + 1);
				int idx4 =
					rs.GetValueAt(i, "nominal_size").GetStringValue().length();

				// Field02 HOSE SIZE DN
				fld02String =
					rs
						.GetValueAt(i, "nominal_size")
						.GetStringValue()
						.substring(
						0,
						idx1);
				fld02Float = Integer.parseInt(fld02String);
				// Field03 HOSE SIZE dash
				fld03String =
					rs
						.GetValueAt(i, "nominal_size")
						.GetStringValue()
						.substring(
						idx1 + 1,
						idx2);
				// Field04 HOSE SIZE mm
				//fld04String = rs.GetValueAt(i, "nominal_size").GetStringValue().substring(idx2 + 1, idx3);
				// Field04 HOSE SIZE inch
				fld04String =
					rs
						.GetValueAt(i, "nominal_size")
						.GetStringValue()
						.substring(
						idx3 + 1,
						idx4);

				// Field05
				if (rs
					.GetValueAt(i, "reinforcement_outside_diameter_mm")
					.IsNull()
					!= true) {
					Measurement fldMeas05 =
						rs
							.GetValueAt(i, "reinforcement_outside_diameter_mm")
							.GetMeasurement();
					fld05String =
						measurementManager.GetString(
							fldMeas05.GetValue(),
							0,
							0,
							rs
								.GetFieldAt("reinforcement_outside_diameter_mm")
								.GetDecimalPlaces(),
							false,
							-1);
					fld05String = fld05String.replaceAll(",", ".");
					fld05Float = Float.valueOf(fld05String).floatValue();
				} else {
					fld05Float = 0;
					fld05String = "";
				}

				// Field06
				if (rs
					.GetValueAt(i, "reinforcement_outside_diameter_inch")
					.IsNull()
					!= true) {
					Measurement fldMeas06 =
						rs
							.GetValueAt(
								i,
								"reinforcement_outside_diameter_inch")
							.GetMeasurement();
					fld06String =
						measurementManager.GetString(
							fldMeas06.GetValue(),
							0,
							0,
							rs
								.GetFieldAt("reinforcement_outside_diameter_inch")
								.GetDecimalPlaces(),
							false,
							-1);
					fld06String = fld06String.replaceAll(",", ".");
					fld06Float = Float.valueOf(fld06String).floatValue();
				} else {
					fld06Float = 0;
					fld06String = "";

				}
				// Field07
				if (rs.GetValueAt(i, "outside_diameter_mm").IsNull() != true) {
					Measurement fldMeas07 =
						rs
							.GetValueAt(i, "outside_diameter_mm")
							.GetMeasurement();
					fld07String =
						measurementManager.GetString(
							fldMeas07.GetValue(),
							0,
							0,
							rs
								.GetFieldAt("outside_diameter_mm")
								.GetDecimalPlaces(),
							false,
							-1);
					fld07String = fld07String.replaceAll(",", ".");
					fld07Float = Float.valueOf(fld07String).floatValue();
				} else {
					fld07Float = 0;
					fld07String = "";

				}
				// Field08
				if (rs.GetValueAt(i, "outside_diameter_inch").IsNull()
					!= true) {
					Measurement fldMeas08 =
						rs
							.GetValueAt(i, "outside_diameter_inch")
							.GetMeasurement();
					fld08String =
						measurementManager.GetString(
							fldMeas08.GetValue(),
							0,
							0,
							rs
								.GetFieldAt("outside_diameter_inch")
								.GetDecimalPlaces(),
							false,
							-1);
					fld08String = fld08String.replaceAll(",", ".");
					fld08Float = Float.valueOf(fld08String).floatValue();
				} else {
					fld08Float = 0;
					fld08String = "";

				}
				// Field09
				if (rs.GetValueAt(i, "Max_working_pressure_bar").IsNull()
					!= true) {
					Measurement fldMeas09 =
						rs
							.GetValueAt(i, "Max_working_pressure_bar")
							.GetMeasurement();
					fld09Float =
						Float
							.valueOf(
								measurementManager.GetString(
									fldMeas09.GetValue(),
									0,
									0,
									0,
									false,
									-1))
							.floatValue();
					fld09String = Float.toString(fld09Float);
				} else {
					fld09Float = 0;
					fld09String = "";

				}
				// Field10
				if (rs.GetValueAt(i, "max_working_pressure_psi").IsNull()
					!= true) {
					Measurement fldMeas10 =
						rs
							.GetValueAt(i, "max_working_pressure_psi")
							.GetMeasurement();
					fld10Float =
						Float
							.valueOf(
								measurementManager.GetString(
									fldMeas10.GetValue(),
									0,
									0,
									0,
									false,
									-1))
							.floatValue();
					fld10String = Float.toString(fld10Float);
				} else {
					fld10Float = 0;
					fld10String = "";

				}
				// Field11
				if (rs.GetValueAt(i, "burst_pressure_bar").IsNull() != true) {
					Measurement fldMeas11 =
						rs.GetValueAt(i, "burst_pressure_bar").GetMeasurement();
					fld11Float =
						Float
							.valueOf(
								measurementManager.GetString(
									fldMeas11.GetValue(),
									0,
									0,
									0,
									false,
									-1))
							.floatValue();
					fld11String = Float.toString(fld11Float);
				} else {
					fld11Float = 0;
					fld11String = "";

				}
				// Field12
				if (rs.GetValueAt(i, "burst_pressure_psi").IsNull() != true) {
					Measurement fldMeas12 =
						rs.GetValueAt(i, "burst_pressure_psi").GetMeasurement();
					fld12Float =
						Float
							.valueOf(
								measurementManager.GetString(
									fldMeas12.GetValue(),
									0,
									0,
									0,
									false,
									-1))
							.floatValue();
					fld12String = Float.toString(fld12Float);
				} else {
					fld12Float = 0;
					fld12String = "";

				}
				// Field13
				if (rs.GetValueAt(i, "min_bend_radius_mm").IsNull() != true) {
					Measurement fldMeas13 =
						rs.GetValueAt(i, "min_bend_radius_mm").GetMeasurement();
					fld13Float =
						Float
							.valueOf(
								measurementManager.GetString(
									fldMeas13.GetValue(),
									0,
									0,
									0,
									false,
									-1))
							.floatValue();
					fld13String = Float.toString(fld13Float);
				} else {
					fld13Float = 0;
					fld13String = "";

				}
				// Field14
				if (rs.GetValueAt(i, "min_bend_radius_inch").IsNull()
					!= true) {
					Measurement fldMeas14 =
						rs
							.GetValueAt(i, "min_bend_radius_inch")
							.GetMeasurement();
					fld14String =
						measurementManager.GetString(
							fldMeas14.GetValue(),
							0,
							0,
							rs
								.GetFieldAt("min_bend_radius_inch")
								.GetDecimalPlaces(),
							false,
							-1);
					fld14String = fld14String.replaceAll(",", ".");
					fld14Float = Float.valueOf(fld14String).floatValue();

					//				String fldMeas12String =	measurementManager.GetString(fldMeas12.GetValue(), fldMeas12.GetUnitID(),
					//																 rs.GetFieldAt("min_bend_radius_inch").GetDimensionID(),
					//																 rs.GetFieldAt("min_bend_radius_inch").GetDecimalPlaces(),
					//																 false, -1);
				} else {
					fld14Float = 0;
					fld14String = "";

				}
				// Field15
				if (rs.GetValueAt(i, "weight_g_m").IsNull() != true) {
					fld15Float = rs.GetValueAt(i, "weight_g_m").GetFloatValue();
					fld15String = Float.toString(fld15Float);

				} else {
					fld15Float = 0;
					fld15String = "";

				}
				// Field16
				if (rs.GetValueAt(i, "weight_lbs_ft").IsNull() != true) {
					fld16Float =
						rs.GetValueAt(i, "weight_lbs_ft").GetFloatValue();
					fld16String = Float.toString(fld16Float);
				} else {
					fld16Float = 0;
					fld16String = "";

				}
				//----          INSERT ML 30.01.2013
				if (paramStandard.equals("01")) { // Standard 1
					head1 = "Std1";
					// Field17
					if (rs.GetValueAt(i, "Std_Ferrule1").IsNull() != true) {
						col1 =
							"+"
								+ rs
									.GetValueAt(i, "Std_Ferrule1")
									.GetStringValue();
					} else {
						col1 = "";
					}
					// Field18
					if (rs.GetValueAt(i, "Std_Insert1").IsNull() != true) {
						col2 = rs.GetValueAt(i, "Std_Insert1").GetStringValue();
						int index = col2.indexOf(",");
						col2 = col2.substring(0, index);
					} else {
						col2 = "";
					}
					fld17String = col2 + col1;
					if (fld17String.length() > 0
						&& fld17String.charAt(0) == '+') {
						fld17String = fld17String.substring(1);
					}
				} else if (paramStandard.equals("02")) { // Standard 2
					head1 = "Std2";
					// Field17
					if (rs.GetValueAt(i, "Std_Ferrule2").IsNull() != true) {
						col1 =
							"+"
								+ rs
									.GetValueAt(i, "Std_Ferrule2")
									.GetStringValue();
					} else {
						col1 = "";
					}
					// Field18
					if (rs.GetValueAt(i, "Std_Insert2").IsNull() != true) {
						col2 = rs.GetValueAt(i, "Std_Insert2").GetStringValue();
						int index = col2.indexOf(",");
						col2 = col2.substring(0, index);
					} else {
						col2 = "";
					}
					fld17String = col2 + col1;
					if (fld17String.length() > 0
						&& fld17String.charAt(0) == '+') {
						fld17String = fld17String.substring(1);
					}
				} else if (paramStandard.equals("03")) { // Alternative 1
					head1 = "Alt1";
					// Field17
					if (rs.GetValueAt(i, "Alt_Ferrule1").IsNull() != true) {
						col1 =
							"+"
								+ rs
									.GetValueAt(i, "Alt_Ferrule1")
									.GetStringValue();
					} else {
						col1 = "";
					}
					// Field18
					if (rs.GetValueAt(i, "Alt_Insert1").IsNull() != true) {
						col2 = rs.GetValueAt(i, "Alt_Insert1").GetStringValue();
						int index = col2.indexOf(",");
						col2 = col2.substring(0, index);
					} else {
						col2 = "";
					}
					fld17String = col2 + col1;
					if (fld17String.length() > 0
						&& fld17String.charAt(0) == '+') {
						fld17String = fld17String.substring(1);
					}
				} else if (paramStandard.equals("04")) { // Alternative 2
					head1 = "Alt2";
					// Field17
					if (rs.GetValueAt(i, "Alt_Ferrule2").IsNull() != true) {
						col1 =
							"+"
								+ rs
									.GetValueAt(i, "Alt_Ferrule2")
									.GetStringValue();
					} else {
						col1 = "";
					}
					// Field18
					if (rs.GetValueAt(i, "Alt_Insert2").IsNull() != true) {
						col2 = rs.GetValueAt(i, "Alt_Insert2").GetStringValue();
						int index = col2.indexOf(",");
						col2 = col2.substring(0, index);
					} else {
						col2 = "";
					}
					fld17String = col2 + col1;
					if (fld17String.length() > 0
						&& fld17String.charAt(0) == '+') {
						fld17String = fld17String.substring(1);
					}
				} else {
					col1 = paramStandard;
					col2 = paramStandard;
					fld17String = col2 + col1;
					if (fld17String.length() > 0
						&& fld17String.charAt(0) == '+') {
						fld17String = fld17String.substring(1);
					}
				}

				if (paramAlternative.equals("01")) { // Standard 1
					head2 = "Std1";
					// Field17
					if (rs.GetValueAt(i, "Std_Ferrule1").IsNull() != true) {
						col3 =
							"+"
								+ rs
									.GetValueAt(i, "Std_Ferrule1")
									.GetStringValue();
					} else {
						col3 = "";
					}
					// Field18
					if (rs.GetValueAt(i, "Std_Insert1").IsNull() != true) {
						col4 = rs.GetValueAt(i, "Std_Insert1").GetStringValue();
						int index = col4.indexOf(",");
						col4 = col4.substring(0, index);
					} else {
						col4 = "";
					}
					fld18String = col4 + col3;
					if (fld18String.length() > 0
						&& fld18String.charAt(0) == '+') {
						fld18String = fld18String.substring(1);
					}
				} else if (paramAlternative.equals("02")) { // Standard 2
					head2 = "Std2";
					// Field17
					if (rs.GetValueAt(i, "Std_Ferrule2").IsNull() != true) {
						col3 =
							"+"
								+ rs
									.GetValueAt(i, "Std_Ferrule2")
									.GetStringValue();
					} else {
						col3 = "";
					}
					// Field18
					if (rs.GetValueAt(i, "Std_Insert2").IsNull() != true) {
						col4 = rs.GetValueAt(i, "Std_Insert2").GetStringValue();
						int index = col4.indexOf(",");
						col4 = col4.substring(0, index);
					} else {
						col4 = "";
					}
					fld18String = col4 + col3;
					if (fld18String.length() > 0
						&& fld18String.charAt(0) == '+') {
						fld18String = fld18String.substring(1);
					}
				} else if (paramAlternative.equals("03")) { // Alternative 1
					head2 = "Alt1";
					// Field17
					if (rs.GetValueAt(i, "Alt_Ferrule1").IsNull() != true) {
						col3 =
							"+"
								+ rs
									.GetValueAt(i, "Alt_Ferrule1")
									.GetStringValue();
					} else {
						col3 = "";
					}
					// Field18
					if (rs.GetValueAt(i, "Alt_Insert1").IsNull() != true) {
						col4 = rs.GetValueAt(i, "Alt_Insert1").GetStringValue();
						int index = col4.indexOf(",");
						col4 = col4.substring(0, index);
					} else {
						col4 = "";
					}
					fld18String = col4 + col3;
					if (fld18String.length() > 0
						&& fld18String.charAt(0) == '+') {
						fld18String = fld18String.substring(1);
					}
				} else if (paramAlternative.equals("04")) { // Alternative 2
					head2 = "Alt2";
					// Field17
					if (rs.GetValueAt(i, "Alt_Ferrule2").IsNull() != true) {
						col3 =
							"+"
								+ rs
									.GetValueAt(i, "Alt_Ferrule2")
									.GetStringValue();
					} else {
						col3 = "";
					}
					// Field18
					if (rs.GetValueAt(i, "Alt_Insert2").IsNull() != true) {
						col4 = rs.GetValueAt(i, "Alt_Insert2").GetStringValue();
						int index = col4.indexOf(",");
						col4 = col4.substring(0, index);
					} else {
						col4 = "";
					}
					fld18String = col4 + col3;
					if (fld18String.length() > 0
						&& fld18String.charAt(0) == '+') {
						fld18String = fld18String.substring(1);
					}
				} else {
					col3 = paramAlternative;
					col4 = paramAlternative;
					fld18String = col4 + col3;
					if (fld18String.length() > 0
						&& fld18String.charAt(0) == '+') {
						fld18String = fld18String.substring(1);
					}
				}

				//----          INSERT ML 30.01.2013

				//----          DELETE ML 30.01.2013
				//				// Field17
				//				if (rs.GetValueAt(i, "standard_ferrule").IsNull() != true) {
				//					fld17String =
				//						rs.GetValueAt(i, "standard_ferrule").GetStringValue();
				//
				//				} else {
				//					fld17String = null;
				//
				//				}
				//				// Field18
				//				if (rs.GetValueAt(i, "standard_insert").IsNull() != true) {
				//					fld18String =
				//						rs.GetValueAt(i, "standard_insert").GetStringValue();
				//					int index = fld18String.indexOf(",");
				//					fld18String = fld18String.substring(0, index);
				//				} else {
				//					fld18String = null;
				//
				//				}
				//----          DELETE ML 30.01.2013

				// Field19
				if (rs.GetValueAt(i, "standard_mf3000").IsNull() != true) {
					fld19String =
						rs.GetValueAt(i, "standard_mf3000").GetStringValue();
				} else {
					fld19String = null;

				}
				//				variabili per stampa Catalogue Extended	
				// Field20
				if (rs.GetValueAt(i, "hose_inside_diameter_min_mm").IsNull()
					!= true) {
					Measurement fldMeas20 =
						rs
							.GetValueAt(i, "hose_inside_diameter_min_mm")
							.GetMeasurement();
					String fld20String =
						measurementManager.GetString(
							fldMeas20.GetValue(),
							0,
							0,
							rs
								.GetFieldAt("hose_inside_diameter_min_mm")
								.GetDecimalPlaces(),
							false,
							-1);
					fld20String = fld20String.replaceAll(",", ".");
					fld20Float = Float.valueOf(fld20String).floatValue();
				} else {
					fld20Float = 0;

				}
				// Field21
				if (rs
					.GetValueAt(i, "hose_inside_diameter_min_inches")
					.IsNull()
					!= true) {
					Measurement fldMeas21 =
						rs
							.GetValueAt(i, "hose_inside_diameter_min_inches")
							.GetMeasurement();
					String fld21String =
						measurementManager.GetString(
							fldMeas21.GetValue(),
							0,
							0,
							rs
								.GetFieldAt("hose_inside_diameter_min_inches")
								.GetDecimalPlaces(),
							false,
							-1);
					fld21String = fld21String.replaceAll(",", ".");
					fld21Float = Float.valueOf(fld21String).floatValue();
				} else {
					fld21Float = 0;

				}
				// Field22
				if (rs.GetValueAt(i, "hose_inside_diameter_max_mm").IsNull()
					!= true) {
					Measurement fldMeas22 =
						rs
							.GetValueAt(i, "hose_inside_diameter_max_mm")
							.GetMeasurement();
					String fld22String =
						measurementManager.GetString(
							fldMeas22.GetValue(),
							0,
							0,
							rs
								.GetFieldAt("hose_inside_diameter_max_mm")
								.GetDecimalPlaces(),
							false,
							-1);
					fld22String = fld22String.replaceAll(",", ".");
					fld22Float = Float.valueOf(fld22String).floatValue();
				} else {
					fld22Float = 0;

				}
				// Field23
				if (rs
					.GetValueAt(i, "hose_inside_diameter_max_inches")
					.IsNull()
					!= true) {
					Measurement fldMeas23 =
						rs
							.GetValueAt(i, "hose_inside_diameter_max_inches")
							.GetMeasurement();
					String fld23String =
						measurementManager.GetString(
							fldMeas23.GetValue(),
							0,
							0,
							rs
								.GetFieldAt("hose_inside_diameter_max_inches")
								.GetDecimalPlaces(),
							false,
							-1);
					fld23String = fld23String.replaceAll(",", ".");
					fld23Float = Float.valueOf(fld23String).floatValue();
				} else {
					fld23Float = 0;

				}
				// Field24
				if (rs
					.GetValueAt(i, "reinforcement_outside_diameter_min_mm")
					.IsNull()
					!= true) {
					Measurement fldMeas24 =
						rs
							.GetValueAt(
								i,
								"reinforcement_outside_diameter_min_mm")
							.GetMeasurement();
					String fld24String =
						measurementManager.GetString(
							fldMeas24.GetValue(),
							0,
							0,
							rs
								.GetFieldAt("reinforcement_outside_diameter_min_mm")
								.GetDecimalPlaces(),
							false,
							-1);
					fld24String = fld24String.replaceAll(",", ".");
					fld24Float = Float.valueOf(fld24String).floatValue();
				} else {
					fld24Float = 0;

				}
				// Field25
				if (rs
					.GetValueAt(i, "reinforcement_outside_diameter_min_inches")
					.IsNull()
					!= true) {
					Measurement fldMeas25 =
						rs
							.GetValueAt(
								i,
								"reinforcement_outside_diameter_min_inches")
							.GetMeasurement();
					String fld25String =
						measurementManager.GetString(
							fldMeas25.GetValue(),
							0,
							0,
							rs
								.GetFieldAt("reinforcement_outside_diameter_min_inches")
								.GetDecimalPlaces(),
							false,
							-1);
					fld25String = fld25String.replaceAll(",", ".");
					fld25Float = Float.valueOf(fld25String).floatValue();
				} else {
					fld25Float = 0;

				}
				// Field26
				if (rs
					.GetValueAt(i, "reinforcement_outside_diameter_max_mm")
					.IsNull()
					!= true) {
					Measurement fldMeas26 =
						rs
							.GetValueAt(
								i,
								"reinforcement_outside_diameter_max_mm")
							.GetMeasurement();
					String fld26String =
						measurementManager.GetString(
							fldMeas26.GetValue(),
							0,
							0,
							rs
								.GetFieldAt("reinforcement_outside_diameter_max_mm")
								.GetDecimalPlaces(),
							false,
							-1);
					fld26String = fld26String.replaceAll(",", ".");
					fld26Float = Float.valueOf(fld26String).floatValue();
				} else {
					fld26Float = 0;

				}
				// Field27
				if (rs
					.GetValueAt(i, "reinforcement_outside_diameter_max_inches")
					.IsNull()
					!= true) {
					Measurement fldMeas27 =
						rs
							.GetValueAt(
								i,
								"reinforcement_outside_diameter_max_inches")
							.GetMeasurement();
					String fld27String =
						measurementManager.GetString(
							fldMeas27.GetValue(),
							0,
							0,
							rs
								.GetFieldAt("reinforcement_outside_diameter_max_inches")
								.GetDecimalPlaces(),
							false,
							-1);
					fld27String = fld27String.replaceAll(",", ".");
					fld27Float = Float.valueOf(fld27String).floatValue();
				} else {
					fld27Float = 0;

				}
				// Field28
				if (rs.GetValueAt(i, "hose_outside_diameter_min_mm").IsNull()
					!= true) {
					Measurement fldMeas28 =
						rs
							.GetValueAt(i, "hose_outside_diameter_min_mm")
							.GetMeasurement();
					String fld28String =
						measurementManager.GetString(
							fldMeas28.GetValue(),
							0,
							0,
							rs
								.GetFieldAt("hose_outside_diameter_min_mm")
								.GetDecimalPlaces(),
							false,
							-1);
					fld28String = fld28String.replaceAll(",", ".");
					fld28Float = Float.valueOf(fld28String).floatValue();
				} else {
					fld28Float = 0;

				}
				// Field29
				if (rs
					.GetValueAt(i, "hose_outside_diameter_min_inches")
					.IsNull()
					!= true) {
					Measurement fldMeas29 =
						rs
							.GetValueAt(i, "hose_outside_diameter_min_inches")
							.GetMeasurement();
					String fld29String =
						measurementManager.GetString(
							fldMeas29.GetValue(),
							0,
							0,
							rs
								.GetFieldAt("hose_outside_diameter_min_inches")
								.GetDecimalPlaces(),
							false,
							-1);
					fld29String = fld29String.replaceAll(",", ".");
					fld29Float = Float.valueOf(fld29String).floatValue();
				} else {
					fld29Float = 0;

				}
				// Field30
				if (rs.GetValueAt(i, "hose_outside_diameter_max_mm").IsNull()
					!= true) {
					Measurement fldMeas30 =
						rs
							.GetValueAt(i, "hose_outside_diameter_max_mm")
							.GetMeasurement();
					String fld30String =
						measurementManager.GetString(
							fldMeas30.GetValue(),
							0,
							0,
							rs
								.GetFieldAt("hose_outside_diameter_max_mm")
								.GetDecimalPlaces(),
							false,
							-1);
					fld30String = fld30String.replaceAll(",", ".");
					fld30Float = Float.valueOf(fld30String).floatValue();
				} else {
					fld30Float = 0;

				}
				// Field31
				if (rs
					.GetValueAt(i, "hose_outside_diameter_max_inches")
					.IsNull()
					!= true) {
					Measurement fldMeas31 =
						rs
							.GetValueAt(i, "hose_outside_diameter_max_inches")
							.GetMeasurement();
					String fld31String =
						measurementManager.GetString(
							fldMeas31.GetValue(),
							0,
							0,
							rs
								.GetFieldAt("hose_outside_diameter_max_inches")
								.GetDecimalPlaces(),
							false,
							-1);
					fld31String = fld31String.replaceAll(",", ".");
					fld31Float = Float.valueOf(fld31String).floatValue();
				} else {
					fld31Float = 0;

				}
				// Field32
				if (rs
					.GetValueAt(i, "hose_max_wall_thickness_difference_mm")
					.IsNull()
					!= true) {
					Measurement fldMeas32 =
						rs
							.GetValueAt(
								i,
								"hose_max_wall_thickness_difference_mm")
							.GetMeasurement();
					String fld32String =
						measurementManager.GetString(
							fldMeas32.GetValue(),
							0,
							0,
							rs
								.GetFieldAt("hose_max_wall_thickness_difference_mm")
								.GetDecimalPlaces(),
							false,
							-1);
					fld32String = fld32String.replaceAll(",", ".");
					fld32Float = Float.valueOf(fld32String).floatValue();
				} else {
					fld32Float = 0;

				}
				// Field33
				if (rs
					.GetValueAt(i, "hose_max_wall_thickness_difference_inches")
					.IsNull()
					!= true) {
					Measurement fldMeas33 =
						rs
							.GetValueAt(
								i,
								"hose_max_wall_thickness_difference_inches")
							.GetMeasurement();
					String fld33String =
						measurementManager.GetString(
							fldMeas33.GetValue(),
							0,
							0,
							rs
								.GetFieldAt("hose_max_wall_thickness_difference_inches")
								.GetDecimalPlaces(),
							false,
							-1);
					fld33String = fld33String.replaceAll(",", ".");
					fld33Float = Float.valueOf(fld33String).floatValue();
				} else {
					fld33Float = 0;

				}

				//			variabili per stampa Catalogue Extended	

				//			variabili per stampa Catalogue Marcature	
				if (paramType.equals("3")) {

					// Field34
					if (rs.GetValueAt(i, "category").IsNull() != true) {
						fld34String =
							rs.GetValueAt(i, "category").GetStringValue();
					} else {
						fld34String = null;

					}
					// Field35
					if (rs.GetValueAt(i, "brand_pack").IsNull() != true) {
						fld35String =
							rs.GetValueAt(i, "brand_pack").GetStringValue();
					} else {
						fld35String = null;

					}
					// Field36
					if (rs.GetValueAt(i, "brand_customer").IsNull() != true) {
						fld36String =
							rs.GetValueAt(i, "brand_customer").GetStringValue();
					} else {
						fld36String = null;

					}
					// Field37
					if (rs.GetValueAt(i, "marking_technology").IsNull()
						!= true) {
						fld37String =
							rs
								.GetValueAt(i, "marking_technology")
								.GetStringValue();
					} else {
						fld37String = null;

					}
					// Field38
					if (rs.GetValueAt(i, "brand_color").IsNull() != true) {
						fld38String =
							rs.GetValueAt(i, "brand_color").GetStringValue();
					} else {
						fld38String = null;

					}
					// Field39
					if (paramType.equals("3")
						&& !rs.GetValueAt(i, "brand_logo").IsNull()) {
						CatalogCache catalogCache = new CatalogCache();
						catalogCache.Init(
							CACHE_SERVER,
							CACHE_PORT,
							CACHE_USER,
							CACHE_PASSWORD,
							CACHE_DIRECTORY,
							"English [US]");
						Object id = rs.GetValueAt(i, "brand_logo").GetData();
						int idx = id.hashCode();
						imagePath =
							CACHE_DIRECTORY
								+ "\\"
								+ catalogCache.GetImagePath(
									"Images",
									"Original",
									idx);
						catalogCache.Shutdown();
					} else {
						imagePath = null;

					}
					// Field40
					if (rs.GetValueAt(i, "brand_notes").IsNull() != true) {
						fld40String =
							rs.GetValueAt(i, "brand_notes").GetStringValue();
					} else {
						fld40String = null;

					}
					// Field41
					if (rs.GetValueAt(i, "brand_date_type").IsNull() != true) {
						fld41String =
							rs
								.GetValueAt(i, "brand_date_type")
								.GetStringValue();
					} else {
						fld41String = null;

					}
					// Field42
					if (rs.GetValueAt(i, "department").IsNull() != true) {
						fld42String =
							rs.GetValueAt(i, "department").GetStringValue();
					} else {
						fld42String = null;

					}
					// Field43
					if (rs.GetValueAt(i, "brand").IsNull() != true) {
						fld43String =
							rs.GetValueAt(i, "brand").GetStringValue();
					} else {
						fld43String = null;

					}
				}

				if (fld17String.length() > 12
					&& fld17String.indexOf("+") != -1) {
					fld17String = fld17String.replaceAll("\\+", "+ \n");
				}
				if (fld18String.length() > 12
					&& fld18String.indexOf("+") != -1) {
					fld18String = fld18String.replaceAll("\\+", "+ \n");
				}
				if (paramType.equals("3")) {
					recs.add(new prod_hoses_rec_marcature(fld34String,
					// marcatura
					fld37String, // Marking Technology
					imagePath, // Brand Logo
					fld38String, // Brand Color
					fld41String, // Brand Date type
					fld42String, // Department
					fld40String, // Brand Note
					fld01String, // Line
					fld02Float, // DN
					fld03String, // Dash
					fld04String, // inch
					fld43String)); // Brand

				} else {
					recs.add(
						new prod_hoses_rec(
							fld01String,
							fld02Float,
							fld03String,
							fld04String,
							fld05Float,
							fld06Float,
							fld07Float,
							fld08Float,
							fld09Float,
							fld10Float,
							fld11Float,
							fld12Float,
							fld13Float,
							fld14Float,
							fld15Float,
							fld16Float,
							fld17String,
							fld18String,
							fld19String,
							fld20Float,
							fld21Float,
							fld22Float,
							fld23Float,
							fld24Float,
							fld25Float,
							fld26Float,
							fld27Float,
							fld28Float,
							fld29Float,
							fld30Float,
							fld31Float,
							fld32Float,
							fld33Float));

				}

			}

		} catch (StringException e) {
			response.write(
				"<BR> Exception -" + e.getMessage() + e.getCause() + "</BR>");
			e.getStackTrace();
		} catch (IllegalArgumentException e) {
			e.getStackTrace();
			response.write(
				"<BR> Exception -" + e.getMessage() + e.getCause() + "</BR>");
		}
	}
	//	-------------- Retrieve data from prod_hoses_rec --------------------------

	public void mdmGetCrimpRec(
		IPortalComponentRequest request,
		IPortalComponentResponse response) {

		//		  create a ResultSetDefinition on the crimp_rec table
		ResultSetDefinition rsd_crimp_rec;
		rsd_crimp_rec = new ResultSetDefinition(paramTableName);
		rsd_crimp_rec.AddField("Family");
		rsd_crimp_rec.AddField("version");
		rsd_crimp_rec.AddField("version_date");
		rsd_crimp_rec.AddField("Crimp_Version");
		rsd_crimp_rec.AddField("Description");
		rsd_crimp_rec.AddField("Technical_Product");
		rsd_crimp_rec.AddField("DN");
		rsd_crimp_rec.AddField("Insert");
		rsd_crimp_rec.AddField("Ferrule");
		rsd_crimp_rec.AddField("skive_solution");
		rsd_crimp_rec.AddField("Int_Skive_mm");
		rsd_crimp_rec.AddField("Int_Skive_inch");
		rsd_crimp_rec.AddField("Ext_Skive_mm");
		rsd_crimp_rec.AddField("Ext_Skive_inch");
		rsd_crimp_rec.AddField("Crimping_Diameter_mm");
		rsd_crimp_rec.AddField("Crimping_Diameter_inch");
		rsd_crimp_rec.AddField("General");
		rsd_crimp_rec.AddField("Skive_Toll");
		rsd_crimp_rec.AddField("Crimp_Toll");
		rsd_crimp_rec.AddField("Remarks");
		rsd_crimp_rec.AddField("Released_by");
		rsd_crimp_rec.AddField("Notes");
		rsd_crimp_rec.AddField("sort_criteria");
		//          Internal Crimping fields
		rsd_crimp_rec.AddField("Ferrule_phase");
		rsd_crimp_rec.AddField("IntSkiveTolMax_inch");
		rsd_crimp_rec.AddField("IntSkiveTolMax_mm");
		rsd_crimp_rec.AddField("IntSkiveTolMin_inch");
		rsd_crimp_rec.AddField("IntSkiveTolMin_mm");
		rsd_crimp_rec.AddField("ExtSkiveTolMax_inch");
		rsd_crimp_rec.AddField("ExtSkiveTolMax_mm");
		rsd_crimp_rec.AddField("ExtSkiveTolMin_inch");
		rsd_crimp_rec.AddField("ExtSkiveTolMin_mm");
		rsd_crimp_rec.AddField("CrimpTolMax_inch");
		rsd_crimp_rec.AddField("CrimpTolMax_mm");
		rsd_crimp_rec.AddField("CrimpTolMin_inch");
		rsd_crimp_rec.AddField("CrimpTolMin_mm");
		rsd_crimp_rec.AddField("Rebounding_inch");
		rsd_crimp_rec.AddField("Rebounding_mm");
		rsd_crimp_rec.AddField("Bending_Radius_inch");
		rsd_crimp_rec.AddField("Bending_Radius_mm");
		rsd_crimp_rec.AddField("Temperature_C");
		rsd_crimp_rec.AddField("Temperature_K");
		rsd_crimp_rec.AddField("Duration_cycles");
		rsd_crimp_rec.AddField("Condition");
		rsd_crimp_rec.AddField("Safety_Factor");
		rsd_crimp_rec.AddField("Public");
		rsd_crimp_rec.AddField("Crimp_DIE");

		// create an empty Search object
		Search search = new Search(rsd_crimp_rec.GetTable());

		FreeFormTableParameter fftpNames =
			search.GetParameters().NewFreeFormTableParameter(
				rsd_crimp_rec.GetTable());
		// Filters		
		if (paramType.equals("8")) {
			if (paramHoseFamilyFlag.equals("false")
				&& paramCrimpingFlag.equals("false")) {

				FreeFormParameterField ffpfKey01 =
					fftpNames.GetFields().New("Family");
				ffpfKey01.GetFreeForm().NewString(
					paramTableFamily,
					FreeFormParameter.EqualToSearchType);
				FreeFormParameterField ffpfKey02 =
					fftpNames.GetFields().New("Crimp_Version");
				ffpfKey02.GetFreeForm().NewString(
					paramCrimpVersion,
					FreeFormParameter.EqualToSearchType);
				FreeFormParameterField ffpfKey03 =
					fftpNames.GetFields().New("version");
				ffpfKey03.GetFreeForm().NewString(
					paramVersion,
					FreeFormParameter.EqualToSearchType);
				if (paramRestrictedFlag.equals("Y")) {
					FreeFormParameterField ffpfKey04 =
						fftpNames.GetFields().New("Public");
					ffpfKey04.GetFreeForm().NewString(
						"YES",
						FreeFormParameter.SubstringSearchType);
				}
			} else if (
				paramHoseFamilyFlag.equals("true")
					&& paramCrimpingFlag.equals("false")) {

				FreeFormParameterField ffpfKey01 =
					fftpNames.GetFields().New("Family");
				ffpfKey01.GetFreeForm().NewString(
					paramTableFamily,
					FreeFormParameter.EqualToSearchType);
				FreeFormParameterField ffpfKey04 =
					fftpNames.GetFields().New("Public");
				ffpfKey04.GetFreeForm().NewString(
					"YES",
					FreeFormParameter.SubstringSearchType);

			} else if (paramCrimpingFlag.equals("true")) {

				FreeFormParameterField ffpfKey04 =
					fftpNames.GetFields().New("Public");
				ffpfKey04.GetFreeForm().NewString(
					"YES",
					FreeFormParameter.EqualToSearchType);
			}

		} else {
			FreeFormParameterField ffpfKey01 =
				fftpNames.GetFields().New("Crimp_Version");
			ffpfKey01.GetFreeForm().NewString(
				paramCrimpVersion,
				FreeFormParameter.EqualToSearchType);

			FreeFormParameterField ffpfKey02 =
				fftpNames.GetFields().New("Family");
			ffpfKey02.GetFreeForm().NewString(
				paramTableFamily,
				FreeFormParameter.EqualToSearchType);

			FreeFormParameterField ffpfKey03 =
				fftpNames.GetFields().New("version");
			ffpfKey03.GetFreeForm().NewString(
				paramVersion,
				FreeFormParameter.EqualToSearchType);
		}

		//		  Printout Fields 
		FreeFormParameterField ffpfField00 =
			fftpNames.GetFields().New("Family");
		FreeFormParameterField ffpfField01 =
			fftpNames.GetFields().New("version");
		FreeFormParameterField ffpfField02 =
			fftpNames.GetFields().New("version_date");
		FreeFormParameterField ffpfField03 =
			fftpNames.GetFields().New("Crimp_Version");
		FreeFormParameterField ffpfField04 =
			fftpNames.GetFields().New("Description");
		FreeFormParameterField ffpfField05 =
			fftpNames.GetFields().New("Technical_Product");
		FreeFormParameterField ffpfField06 = fftpNames.GetFields().New("DN");
		FreeFormParameterField ffpfField07 =
			fftpNames.GetFields().New("Insert");
		FreeFormParameterField ffpfField08 =
			fftpNames.GetFields().New("Ferrule");
		FreeFormParameterField ffpfField09 =
			fftpNames.GetFields().New("skive_solution");
		FreeFormParameterField ffpfField10 =
			fftpNames.GetFields().New("Int_Skive_mm");
		FreeFormParameterField ffpfField11 =
			fftpNames.GetFields().New("Int_Skive_inch");
		FreeFormParameterField ffpfField12 =
			fftpNames.GetFields().New("Ext_Skive_mm");
		FreeFormParameterField ffpfField13 =
			fftpNames.GetFields().New("Ext_Skive_inch");
		FreeFormParameterField ffpfField14 =
			fftpNames.GetFields().New("Crimping_Diameter_mm");
		FreeFormParameterField ffpfField15 =
			fftpNames.GetFields().New("Crimping_Diameter_inch");
		FreeFormParameterField ffpfField16 =
			fftpNames.GetFields().New("General");
		FreeFormParameterField ffpfField17 =
			fftpNames.GetFields().New("Skive_Toll");
		FreeFormParameterField ffpfField18 =
			fftpNames.GetFields().New("Crimp_Toll");
		FreeFormParameterField ffpfField19 =
			fftpNames.GetFields().New("Remarks");
		FreeFormParameterField ffpfField20 =
			fftpNames.GetFields().New("Released_by");
		FreeFormParameterField ffpfField21 = fftpNames.GetFields().New("Notes");
		FreeFormParameterField ffpfField22 =
			fftpNames.GetFields().New("sort_criteria");
		FreeFormParameterField ffpfField23 =
			fftpNames.GetFields().New("Ferrule_phase");
		FreeFormParameterField ffpfField24 =
			fftpNames.GetFields().New("IntSkiveTolMax_inch");
		FreeFormParameterField ffpfField25 =
			fftpNames.GetFields().New("IntSkiveTolMax_mm");
		FreeFormParameterField ffpfField26 =
			fftpNames.GetFields().New("IntSkiveTolMin_inch");
		FreeFormParameterField ffpfField27 =
			fftpNames.GetFields().New("IntSkiveTolMin_mm");
		FreeFormParameterField ffpfField28 =
			fftpNames.GetFields().New("ExtSkiveTolMax_inch");
		FreeFormParameterField ffpfField29 =
			fftpNames.GetFields().New("ExtSkiveTolMax_mm");
		FreeFormParameterField ffpfField30 =
			fftpNames.GetFields().New("ExtSkiveTolMin_inch");
		FreeFormParameterField ffpfField31 =
			fftpNames.GetFields().New("ExtSkiveTolMin_mm");
		FreeFormParameterField ffpfField32 =
			fftpNames.GetFields().New("CrimpTolMax_inch");
		FreeFormParameterField ffpfField33 =
			fftpNames.GetFields().New("CrimpTolMax_mm");
		FreeFormParameterField ffpfField34 =
			fftpNames.GetFields().New("CrimpTolMin_inch");
		FreeFormParameterField ffpfField35 =
			fftpNames.GetFields().New("CrimpTolMin_mm");
		FreeFormParameterField ffpfField36 =
			fftpNames.GetFields().New("Rebounding_inch");
		FreeFormParameterField ffpfField37 =
			fftpNames.GetFields().New("Rebounding_mm");
		FreeFormParameterField ffpfField38 =
			fftpNames.GetFields().New("Bending_Radius_inch");
		FreeFormParameterField ffpfField39 =
			fftpNames.GetFields().New("Bending_Radius_mm");
		FreeFormParameterField ffpfField40 =
			fftpNames.GetFields().New("Temperature_C");
		FreeFormParameterField ffpfField41 =
			fftpNames.GetFields().New("Temperature_K");
		FreeFormParameterField ffpfField42 =
			fftpNames.GetFields().New("Duration_cycles");
		FreeFormParameterField ffpfField43 =
			fftpNames.GetFields().New("Condition");
		FreeFormParameterField ffpfField44 =
			fftpNames.GetFields().New("Safety_Factor");
		FreeFormParameterField ffpfField45 =
			fftpNames.GetFields().New("sort_criteria");
		FreeFormParameterField ffpfField46 =
			fftpNames.GetFields().New("Public");
		FreeFormParameterField ffpfField47 =
			fftpNames.GetFields().New("Crimp_DIE");
		// Ordinamento records
		A2iResultSet rs;
		//			response.write("<BR>prima del TRY </BR>");

		try {
			Object fields = null;
			Object locale = null;
			rs = catalogData.GetResultSet(search, rsd_crimp_rec, null, true, 0);
			//				response.write("<BR>prima del FOR " + rs.GetRecordCount() + "</BR>");
			if (rs.GetRecordCount() == 0) {
				response.write("<BR>MESSAGE: no records found!!!</BR>");
			}

			for (int i = 0; i < rs.GetRecordCount(); i++) {
				fld01String = rs.GetValueAt(i, "Family").GetStringValue();
				if (rs.GetValueAt(i, "version").IsNull() != true) {
					fld02String = rs.GetValueAt(i, "version").GetStringValue();
				} else {
					fld02String = "";
				}
				if (rs.GetValueAt(i, "version_date").IsNull() != true) {
					String v_dateTime =
						rs
							.GetValueAt(i, "version_date")
							.GetSystemTime()
							.ConvertSystemTimeToString();
					fld03String =
						v_dateTime.substring(0, 4)
							+ "/"
							+ v_dateTime.substring(5, 7)
							+ "/"
							+ v_dateTime.substring(8, 10);
				} else {
					fld03String = "";
				}

				if (rs.GetValueAt(i, "Crimp_Version").IsNull() != true) {
					fld04String =
						rs.GetValueAt(i, "Crimp_Version").GetStringValue();
					int start = fld04String.indexOf(",");
					fld04String = fld04String.substring(start + 1);
				} else {
					fld04String = "";
				}

				if (rs.GetValueAt(i, "Description").IsNull() != true) {
					fld05String =
						rs.GetValueAt(i, "Description").GetStringValue();
				} else {
					fld05String = "";
				}
				if (rs.GetValueAt(i, "Technical_Product").IsNull() != true) {
					fld06String =
						rs.GetValueAt(i, "Technical_Product").GetStringValue();
				} else {
					fld06String = "";
				}
				int idx1 =
					rs.GetValueAt(i, "DN").GetStringValue().indexOf(",", 0);
				int idx2 =
					rs.GetValueAt(i, "DN").GetStringValue().indexOf(
						",",
						idx1 + 1);
				int idx3 =
					rs.GetValueAt(i, "DN").GetStringValue().indexOf(
						",",
						idx2 + 1);
				int idx4 = rs.GetValueAt(i, "DN").GetStringValue().length();

				// HOSE SIZE DN
				fld07String =
					rs.GetValueAt(i, "DN").GetStringValue().substring(0, idx1);
				fld07Float = Integer.parseInt(fld07String);

				DecimalFormat myFormatter = new DecimalFormat("000");
				String output = myFormatter.format(fld07Float);
				fld07String = output;

				fld08String =
					rs.GetValueAt(i, "DN").GetStringValue().substring(
						idx1 + 1,
						idx2);
				fld09String =
					rs.GetValueAt(i, "DN").GetStringValue().substring(
						idx3 + 1,
						idx4);

				if (rs.GetValueAt(i, "Insert").IsNull() != true) {
					fld10String = rs.GetValueAt(i, "Insert").GetStringValue();
					int start = fld10String.indexOf(",");
					fld10String = fld10String.substring(start + 1);
				} else {
					fld10String = "";
				}
				if (rs.GetValueAt(i, "Ferrule").IsNull() != true) {
					fld11String = rs.GetValueAt(i, "Ferrule").GetStringValue();
				} else {
					fld11String = "";
				}
				if (rs.GetValueAt(i, "skive_solution").IsNull() != true) {
					fld12String =
						rs.GetValueAt(i, "skive_solution").GetStringValue();
				} else {
					fld12String = "";
				}
				if (rs.GetValueAt(i, "Int_Skive_mm").IsNull() != true) {
					Measurement fldMeas13 =
						rs.GetValueAt(i, "Int_Skive_mm").GetMeasurement();
					String fld13String =
						measurementManager.GetString(
							fldMeas13.GetValue(),
							0,
							0,
							rs.GetFieldAt("Int_Skive_mm").GetDecimalPlaces(),
							false,
							-1);
					fld13String = fld13String.replaceAll(",", ".");
					fld13Float = Float.valueOf(fld13String).floatValue();
				} else {
					fld13Float = 0;
				}
				if (rs.GetValueAt(i, "Int_Skive_inch").IsNull() != true) {
					Measurement fldMeas14 =
						rs.GetValueAt(i, "Int_Skive_inch").GetMeasurement();
					String fld14String =
						measurementManager.GetString(
							fldMeas14.GetValue(),
							0,
							0,
							rs.GetFieldAt("Int_Skive_inch").GetDecimalPlaces(),
							false,
							-1);
					fld14String = fld14String.replaceAll(",", ".");
					fld14Float = Float.valueOf(fld14String).floatValue();
				} else {
					fld14Float = 0;
				}
				if (rs.GetValueAt(i, "Ext_Skive_mm").IsNull() != true) {
					Measurement fldMeas15 =
						rs.GetValueAt(i, "Ext_Skive_mm").GetMeasurement();
					String fld15String =
						measurementManager.GetString(
							fldMeas15.GetValue(),
							0,
							0,
							rs.GetFieldAt("Ext_Skive_mm").GetDecimalPlaces(),
							false,
							-1);
					fld15String = fld15String.replaceAll(",", ".");
					fld15Float = Float.valueOf(fld15String).floatValue();
				} else {
					fld15Float = 0;
				}
				if (rs.GetValueAt(i, "Ext_Skive_inch").IsNull() != true) {
					Measurement fldMeas16 =
						rs.GetValueAt(i, "Ext_Skive_inch").GetMeasurement();
					String fld16String =
						measurementManager.GetString(
							fldMeas16.GetValue(),
							0,
							0,
							rs.GetFieldAt("Ext_Skive_inch").GetDecimalPlaces(),
							false,
							-1);
					fld16String = fld16String.replaceAll(",", ".");
					fld16Float = Float.valueOf(fld16String).floatValue();
				} else {
					fld16Float = 0;
				}
				if (rs.GetValueAt(i, "Crimping_Diameter_mm").IsNull()
					!= true) {
					Measurement fldMeas17 =
						rs
							.GetValueAt(i, "Crimping_Diameter_mm")
							.GetMeasurement();
					String fld17String =
						measurementManager.GetString(
							fldMeas17.GetValue(),
							0,
							0,
							rs
								.GetFieldAt("Crimping_Diameter_mm")
								.GetDecimalPlaces(),
							false,
							-1);
					fld17String = fld17String.replaceAll(",", ".");
					fld17Float = Float.valueOf(fld17String).floatValue();
				} else {
					fld17Float = 0;
				}
				if (rs.GetValueAt(i, "Crimping_Diameter_inch").IsNull()
					!= true) {
					Measurement fldMeas18 =
						rs
							.GetValueAt(i, "Crimping_Diameter_inch")
							.GetMeasurement();
					String fld18String =
						measurementManager.GetString(
							fldMeas18.GetValue(),
							0,
							0,
							rs
								.GetFieldAt("Crimping_Diameter_inch")
								.GetDecimalPlaces(),
							false,
							-1);
					fld18String = fld18String.replaceAll(",", ".");
					fld18Float = Float.valueOf(fld18String).floatValue();
				} else {
					fld18Float = 0;
				}
				if (rs.GetValueAt(i, "General").IsNull() != true) {
					fld19String = rs.GetValueAt(i, "General").GetStringValue();
				} else {
					fld19String = "";
				}
				if (rs.GetValueAt(i, "Skive_Toll").IsNull() != true) {
					fld20String =
						rs.GetValueAt(i, "Skive_Toll").GetStringValue();
				} else {
					fld20String = "";
				}
				if (rs.GetValueAt(i, "Crimp_Toll").IsNull() != true) {
					fld21String =
						rs.GetValueAt(i, "Crimp_Toll").GetStringValue();
				} else {
					fld21String = "";
				}
				if (rs.GetValueAt(i, "Remarks").IsNull() != true) {
					fld22String = rs.GetValueAt(i, "Remarks").GetStringValue();
				} else {
					fld22String = "";
				}
				if (rs.GetValueAt(i, "Released_by").IsNull() != true) {
					fld23String =
						rs.GetValueAt(i, "Released_by").GetStringValue();
				} else {
					fld23String = "";
				}
				if (rs.GetValueAt(i, "Notes").IsNull() != true) {
					fld24String = rs.GetValueAt(i, "Notes").GetStringValue();
				} else {
					fld24String = "";
				}
				if (rs.GetValueAt(i, "sort_criteria").IsNull() != true) {
					fld28String =
						rs.GetValueAt(i, "sort_criteria").GetStringValue();
				} else {
					fld28String = "";
				}
				if (rs.GetValueAt(i, "Ferrule_phase").IsNull() != true) {
					fld29String =
						rs.GetValueAt(i, "Ferrule_phase").GetStringValue();
				} else {
					fld29String = "";
				}
				if (rs.GetValueAt(i, "IntSkiveTolMax_inch").IsNull() != true) {
					Measurement fldMeas30 =
						rs
							.GetValueAt(i, "IntSkiveTolMax_inch")
							.GetMeasurement();
					String fld30String =
						measurementManager.GetString(
							fldMeas30.GetValue(),
							0,
							0,
							rs
								.GetFieldAt("IntSkiveTolMax_inch")
								.GetDecimalPlaces(),
							false,
							-1);
					fld30String = fld30String.replaceAll(",", ".");
					fld30Float = Float.valueOf(fld30String).floatValue();
				} else {
					fld30Float = 0;
				}
				if (rs.GetValueAt(i, "IntSkiveTolMax_mm").IsNull() != true) {
					Measurement fldMeas31 =
						rs.GetValueAt(i, "IntSkiveTolMax_mm").GetMeasurement();
					String fld31String =
						measurementManager.GetString(
							fldMeas31.GetValue(),
							0,
							0,
							rs
								.GetFieldAt("IntSkiveTolMax_mm")
								.GetDecimalPlaces(),
							false,
							-1);
					fld31String = fld31String.replaceAll(",", ".");
					fld31Float = Float.valueOf(fld31String).floatValue();
				} else {
					fld31Float = 0;
				}
				if (rs.GetValueAt(i, "IntSkiveTolMin_inch").IsNull() != true) {
					Measurement fldMeas32 =
						rs
							.GetValueAt(i, "IntSkiveTolMin_inch")
							.GetMeasurement();
					String fld32String =
						measurementManager.GetString(
							fldMeas32.GetValue(),
							0,
							0,
							rs
								.GetFieldAt("IntSkiveTolMin_inch")
								.GetDecimalPlaces(),
							false,
							-1);
					fld32String = fld32String.replaceAll(",", ".");
					fld32Float = Float.valueOf(fld32String).floatValue();
				} else {
					fld32Float = 0;
				}

				if (rs.GetValueAt(i, "IntSkiveTolMin_mm").IsNull() != true) {
					Measurement fldMeas33 =
						rs.GetValueAt(i, "IntSkiveTolMin_mm").GetMeasurement();
					String fld33String =
						measurementManager.GetString(
							fldMeas33.GetValue(),
							0,
							0,
							rs
								.GetFieldAt("IntSkiveTolMin_mm")
								.GetDecimalPlaces(),
							false,
							-1);
					fld33String = fld33String.replaceAll(",", ".");
					fld33Float = Float.valueOf(fld33String).floatValue();
				} else {
					fld33Float = 0;
				}

				if (rs.GetValueAt(i, "ExtSkiveTolMax_inch").IsNull() != true) {
					Measurement fldMeas34 =
						rs
							.GetValueAt(i, "ExtSkiveTolMax_inch")
							.GetMeasurement();
					String fld34String =
						measurementManager.GetString(
							fldMeas34.GetValue(),
							0,
							0,
							rs
								.GetFieldAt("ExtSkiveTolMax_inch")
								.GetDecimalPlaces(),
							false,
							-1);
					fld34String = fld34String.replaceAll(",", ".");
					fld34Float = Float.valueOf(fld34String).floatValue();
				} else {
					fld34Float = 0;
				}
				if (rs.GetValueAt(i, "ExtSkiveTolMax_mm").IsNull() != true) {
					Measurement fldMeas35 =
						rs.GetValueAt(i, "ExtSkiveTolMax_mm").GetMeasurement();
					String fld35String =
						measurementManager.GetString(
							fldMeas35.GetValue(),
							0,
							0,
							rs
								.GetFieldAt("ExtSkiveTolMax_mm")
								.GetDecimalPlaces(),
							false,
							-1);
					fld35String = fld35String.replaceAll(",", ".");
					fld35Float = Float.valueOf(fld35String).floatValue();
				} else {
					fld35Float = 0;
				}
				if (rs.GetValueAt(i, "ExtSkiveTolMin_inch").IsNull() != true) {
					Measurement fldMeas36 =
						rs
							.GetValueAt(i, "ExtSkiveTolMin_inch")
							.GetMeasurement();
					String fld36String =
						measurementManager.GetString(
							fldMeas36.GetValue(),
							0,
							0,
							rs
								.GetFieldAt("ExtSkiveTolMin_inch")
								.GetDecimalPlaces(),
							false,
							-1);
					fld36String = fld36String.replaceAll(",", ".");
					fld36Float = Float.valueOf(fld36String).floatValue();
				} else {
					fld36Float = 0;
				}
				if (rs.GetValueAt(i, "ExtSkiveTolMin_mm").IsNull() != true) {
					Measurement fldMeas37 =
						rs.GetValueAt(i, "ExtSkiveTolMin_mm").GetMeasurement();
					String fld37String =
						measurementManager.GetString(
							fldMeas37.GetValue(),
							0,
							0,
							rs
								.GetFieldAt("ExtSkiveTolMin_mm")
								.GetDecimalPlaces(),
							false,
							-1);
					fld37String = fld37String.replaceAll(",", ".");
					fld37Float = Float.valueOf(fld37String).floatValue();
				} else {
					fld37Float = 0;
				}
				if (rs.GetValueAt(i, "CrimpTolMax_inch").IsNull() != true) {
					Measurement fldMeas38 =
						rs.GetValueAt(i, "CrimpTolMax_inch").GetMeasurement();
					String fld38String =
						measurementManager.GetString(
							fldMeas38.GetValue(),
							0,
							0,
							rs
								.GetFieldAt("CrimpTolMax_inch")
								.GetDecimalPlaces(),
							false,
							-1);
					fld38String = fld38String.replaceAll(",", ".");
					fld38Float = Float.valueOf(fld38String).floatValue();
				} else {
					fld38Float = 0;
				}
				if (rs.GetValueAt(i, "CrimpTolMax_mm").IsNull() != true) {
					Measurement fldMeas39 =
						rs.GetValueAt(i, "CrimpTolMax_mm").GetMeasurement();
					String fld39String =
						measurementManager.GetString(
							fldMeas39.GetValue(),
							0,
							0,
							rs.GetFieldAt("CrimpTolMax_mm").GetDecimalPlaces(),
							false,
							-1);
					fld39String = fld39String.replaceAll(",", ".");
					fld39Float = Float.valueOf(fld39String).floatValue();
				} else {
					fld39Float = 0;
				}
				if (rs.GetValueAt(i, "CrimpTolMin_inch").IsNull() != true) {
					Measurement fldMeas40 =
						rs.GetValueAt(i, "CrimpTolMin_inch").GetMeasurement();
					String fld40String =
						measurementManager.GetString(
							fldMeas40.GetValue(),
							0,
							0,
							rs
								.GetFieldAt("CrimpTolMin_inch")
								.GetDecimalPlaces(),
							false,
							-1);
					fld40String = fld40String.replaceAll(",", ".");
					fld40Float = Float.valueOf(fld40String).floatValue();
				} else {
					fld40Float = 0;
				}
				if (rs.GetValueAt(i, "CrimpTolMin_mm").IsNull() != true) {
					Measurement fldMeas41 =
						rs.GetValueAt(i, "CrimpTolMin_mm").GetMeasurement();
					String fld41String =
						measurementManager.GetString(
							fldMeas41.GetValue(),
							0,
							0,
							rs.GetFieldAt("CrimpTolMin_mm").GetDecimalPlaces(),
							false,
							-1);
					fld41String = fld41String.replaceAll(",", ".");
					fld41Float = Float.valueOf(fld41String).floatValue();
				} else {
					fld41Float = 0;
				}
				if (rs.GetValueAt(i, "Rebounding_inch").IsNull() != true) {
					Measurement fldMeas42 =
						rs.GetValueAt(i, "Rebounding_inch").GetMeasurement();
					String fld42String =
						measurementManager.GetString(
							fldMeas42.GetValue(),
							0,
							0,
							rs.GetFieldAt("Rebounding_inch").GetDecimalPlaces(),
							false,
							-1);
					fld42String = fld42String.replaceAll(",", ".");
					fld42Float = Float.valueOf(fld42String).floatValue();
				} else {
					fld42Float = 0;
				}
				if (rs.GetValueAt(i, "Rebounding_mm").IsNull() != true) {
					Measurement fldMeas43 =
						rs.GetValueAt(i, "Rebounding_mm").GetMeasurement();
					String fld43String =
						measurementManager.GetString(
							fldMeas43.GetValue(),
							0,
							0,
							rs.GetFieldAt("Rebounding_mm").GetDecimalPlaces(),
							false,
							-1);
					fld43String = fld43String.replaceAll(",", ".");
					fld43Float = Float.valueOf(fld43String).floatValue();
				} else {
					fld43Float = 0;
				}
				if (rs.GetValueAt(i, "Bending_Radius_inch").IsNull() != true) {
					Measurement fldMeas44 =
						rs
							.GetValueAt(i, "Bending_Radius_inch")
							.GetMeasurement();
					String fld44String =
						measurementManager.GetString(
							fldMeas44.GetValue(),
							0,
							0,
							rs
								.GetFieldAt("Bending_Radius_inch")
								.GetDecimalPlaces(),
							false,
							-1);
					fld44String = fld44String.replaceAll(",", ".");
					fld44Float = Float.valueOf(fld44String).floatValue();
				} else {
					fld44Float = 0;
				}
				if (rs.GetValueAt(i, "Bending_Radius_mm").IsNull() != true) {
					Measurement fldMeas45 =
						rs.GetValueAt(i, "Bending_Radius_mm").GetMeasurement();
					String fld45String =
						measurementManager.GetString(
							fldMeas45.GetValue(),
							0,
							0,
							rs
								.GetFieldAt("Bending_Radius_mm")
								.GetDecimalPlaces(),
							false,
							-1);
					fld45String = fld45String.replaceAll(",", ".");
					fld45Float = Float.valueOf(fld45String).floatValue();
				} else {
					fld45Float = 0;
				}
				if (rs.GetValueAt(i, "Temperature_C").IsNull() != true) {
					Measurement fldMeas46 =
						rs.GetValueAt(i, "Temperature_C").GetMeasurement();
					String fld46String =
						measurementManager.GetString(
							fldMeas46.GetValue(),
							0,
							0,
							rs.GetFieldAt("Temperature_C").GetDecimalPlaces(),
							false,
							-1);
					fld46String = fld46String.replaceAll(",", ".");
					fld46Float = Float.valueOf(fld46String).floatValue();
				} else {
					fld46Float = 0;
				}
				if (rs.GetValueAt(i, "Temperature_K").IsNull() != true) {
					Measurement fldMeas47 =
						rs.GetValueAt(i, "Temperature_K").GetMeasurement();
					String fld47String =
						measurementManager.GetString(
							fldMeas47.GetValue(),
							0,
							0,
							rs.GetFieldAt("Temperature_K").GetDecimalPlaces(),
							false,
							-1);
					fld47String = fld47String.replaceAll(",", ".");
					fld47Float = Float.valueOf(fld47String).floatValue();
				} else {
					fld47Float = 0;
				}
				if (rs.GetValueAt(i, "Duration_cycles").IsNull() != true) {
					fld48String =
						rs.GetValueAt(i, "Duration_cycles").GetStringValue();
				} else {
					fld48String = "";
				}
				if (rs.GetValueAt(i, "Condition").IsNull() != true) {
					fld49String =
						rs.GetValueAt(i, "Condition").GetStringValue();
				} else {
					fld49String = "";
				}
				if (rs.GetValueAt(i, "Safety_Factor").IsNull() != true) {
					fld50String =
						rs.GetValueAt(i, "Safety_Factor").GetStringValue();
				} else {
					fld50String = "";
				}
				if (rs.GetValueAt(i, "Public").IsNull() != true) {
					fld51String = rs.GetValueAt(i, "Public").GetStringValue();
				} else {
					fld51String = "";
				}
				if (rs.GetValueAt(i, "Crimp_DIE").IsNull() != true) {
					fld53String =
						rs.GetValueAt(i, "Crimp_DIE").GetStringValue();
				} else {
					fld53String = "";
				}

				fld27String = "";
				fld27String = fld01String + fld28String + fld07String;
				fld54String = fld01String + fld03String;
				recs.add(
					new crimp_rec(
						fld01String,
						fld02String,
						fld03String,
						fld04String,
						fld05String,
						fld06String,
						fld07Float,
						fld08String,
						fld09String,
						fld10String,
						fld11String,
						fld12String,
						fld13Float,
						fld14Float,
						fld15Float,
						fld16Float,
						fld17Float,
						fld18Float,
						fld19String,
						fld20String,
						fld21String,
						fld22String,
						fld23String,
						fld24String,
						fld25Boolean,
						fld26Boolean,
						fld27String,
						fld28String,
						fld29String,
						fld30Float,
						fld31Float,
						fld32Float,
						fld33Float,
						fld34Float,
						fld35Float,
						fld36Float,
						fld37Float,
						fld38Float,
						fld39Float,
						fld40Float,
						fld41Float,
						fld42Float,
						fld43Float,
						fld44Float,
						fld45Float,
						fld46Float,
						fld47Float,
						fld48String,
						fld49String,
						fld50String,
						fld51String,
						fld52Boolean,
						fld53String,
						fld54String));

				// Variabili di testata

				family = fld01String;
				release_number = fld02String;
				date = fld03String;
				crimp_version = fld04String;
				general = fld19String;
				skive_tool = fld20String;
				crimp_tool = fld21String;
				remarks = fld22String;

			}

		} catch (StringException e) {
			response.write("<BR> Exception -" + e.getStackTrace() + "</BR>");
			e.getStackTrace();
		}
	}

	//	-------------- Sort items  --------------------------------------------
	public void mdmGetSortItems(
		IPortalComponentRequest request,
		IPortalComponentResponse response) {

		String field1 = null;
		String field8 = null;
		int i;
		int n;
		if (paramType.equals("1") || paramType.equals("2")) {
			Collections.sort(recs, new lineOrder());
			for (i = 0; i < recs.size(); i++) {
				String fld01String =
					((prod_hoses_rec) recs.get(i)).getField01();
				if (field1 == null || !field1.equals(fld01String)) {
					recsNew.add(recs.get(i));
				}
				field1 = fld01String;
			}
			Collections.sort(recsNew, new sizeOrder());
		} else if (paramType.equals("8")) {
			// sort by version date
			for (i = 0; i < recs.size(); i++) {
				recsTit.add(recs.get(i));
			}
			Collections.sort(recsTit, new sortByVersionDate());
			String appField01 = "";
			String appField03 = "";
			for (i = 0; i < recsTit.size(); i++) {
				if (appField01.equals("")) {
					appField01 = ((crimp_rec) recsTit.get(i)).getField01();
					appField03 = ((crimp_rec) recsTit.get(i)).getField03();
				}
				if (appField01 == ((crimp_rec) recsTit.get(i)).getField01()) {
					((crimp_rec) recsTit.get(i)).setField54(appField03);
				} else {
					appField01 = ((crimp_rec) recsTit.get(i)).getField01();
					appField03 = ((crimp_rec) recsTit.get(i)).getField03();
					((crimp_rec) recsTit.get(i)).setField54(appField03);

				}
			}

			for (i = 0; i < recsTit.size(); i++) {
				recsNew.add(recs.get(i));
			}

			Collections.sort(recsNew, new sortByFamilyCrimpVersion());

			//----------------------  1st Loop --------------------------//
			// check columns 15 & 16 is empty
			String crimpVersion = null;
			String family = null;

			boolean removeColumns = true;
			int from = 0;
			int to = 0;
			for (i = 0; i < recsNew.size(); i++) {
				to = i;
				//					response.write(
				//						"<BR>"
				//							+ "1st Loop Crimp. Version."
				//							+ ((crimp_rec) recsNew.get(i)).getField04()
				//							+ " i: "
				//							+ i
				//							+ "</BR>");

				//					if (crimpVersion == null
				//						|| crimpVersion.equals(
				//							((crimp_rec) recsNew.get(i)).getField04())){

				if ((crimpVersion == null
					|| crimpVersion.equals(
						((crimp_rec) recsNew.get(i)).getField04()))
					&& (family == null
						|| family.equals(
							((crimp_rec) recsNew.get(i)).getField01()))) {

					if (((crimp_rec) recsNew.get(i)).getField15() == 0
						&& ((crimp_rec) recsNew.get(i)).getField16() == 0
						&& removeColumns == true) {
						removeColumns = true;
					} else {
						removeColumns = false;
					}
				} else {
					if (removeColumns == true) {
						// shift columns 17 & 18 to columns 15 & 16
						for (n = from; n < to; n++) {
							//								response.write(
							//									"<BR>"
							//										+ "1st Loop Crimp. Version."
							//										+ ((crimp_rec) recsNew.get(n)).getField04()
							//										+ " n: "
							//										+ n
							//										+ " from"
							//										+ from
							//										+ " to"
							//										+ to
							//										+ "</BR>");

							((crimp_rec) recsNew.get(n)).setField15(
								((crimp_rec) recsNew.get(n)).getField17());
							((crimp_rec) recsNew.get(n)).setField16(
								((crimp_rec) recsNew.get(n)).getField18());
							((crimp_rec) recsNew.get(n)).setField17(0);
							((crimp_rec) recsNew.get(n)).setField18(0);
							((crimp_rec) recsNew.get(n)).setField26(false);
						}
					}
					from = to;
					removeColumns = true;
					if (((crimp_rec) recsNew.get(i)).getField15() == 0
						&& ((crimp_rec) recsNew.get(i)).getField16() == 0
						&& removeColumns == true) {
						removeColumns = true;
					} else {
						removeColumns = false;
					}
				}
				crimpVersion = ((crimp_rec) recsNew.get(i)).getField04();
				family = ((crimp_rec) recsNew.get(i)).getField01();
			}
			if (removeColumns == true) {
				// rimuovere le colonne per 
				for (n = from; n <= to; n++) {
					//						response.write(
					//							"<BR>"
					//								+ "1st Loop last Crimp. Version."
					//								+ ((crimp_rec) recsNew.get(n)).getField04()
					//								+ " n: "
					//								+ n
					//								+ " from"
					//								+ from
					//								+ " to"
					//								+ to
					//								+ "</BR>");
					((crimp_rec) recsNew.get(n)).setField15(
						((crimp_rec) recsNew.get(n)).getField17());
					((crimp_rec) recsNew.get(n)).setField16(
						((crimp_rec) recsNew.get(n)).getField18());
					((crimp_rec) recsNew.get(n)).setField17(0);
					((crimp_rec) recsNew.get(n)).setField18(0);
					((crimp_rec) recsNew.get(n)).setField26(false);
				}
			}
			//----------------------  2nd Loop --------------------------//
			// check columns 13 & 14 is empty
			crimpVersion = null;
			family = null;
			removeColumns = true;
			from = 0;
			to = 0;
			for (i = 0; i < recsNew.size(); i++) {
				to = i;
				//					response.write(
				//						"<BR>"
				//							+ "2nd Loop Crimp. Version."
				//							+ ((crimp_rec) recsNew.get(i)).getField04()
				//							+ " i: "
				//							+ i
				//							+ "</BR>");
				//					if (crimpVersion == null
				//						|| crimpVersion.equals(
				//							((crimp_rec) recsNew.get(i)).getField04())){

				if ((crimpVersion == null
					|| crimpVersion.equals(
						((crimp_rec) recsNew.get(i)).getField04()))
					&& (family == null
						|| family.equals(
							((crimp_rec) recsNew.get(i)).getField01()))) {

					if (((crimp_rec) recsNew.get(i)).getField13() == 0
						&& ((crimp_rec) recsNew.get(i)).getField14() == 0
						&& removeColumns == true) {
						removeColumns = true;
					} else {
						removeColumns = false;
					}
				} else {
					if (removeColumns == true) {
						// shift columns 15 & 16 to columns 13 & 14
						// shift columns 17 & 18 to columns 15 & 16
						for (n = from; n < to; n++) {
							//								response.write(
							//									"<BR>"
							//										+ "2nd Loop Crimp. Version."
							//										+ ((crimp_rec) recsNew.get(n)).getField04()
							//										+ " n: "
							//										+ n
							//										+ " from"
							//										+ from
							//										+ " to"
							//										+ to
							//										+ "</BR>");
							((crimp_rec) recsNew.get(n)).setField13(
								((crimp_rec) recsNew.get(n)).getField15());
							((crimp_rec) recsNew.get(n)).setField14(
								((crimp_rec) recsNew.get(n)).getField16());
							((crimp_rec) recsNew.get(n)).setField15(
								((crimp_rec) recsNew.get(n)).getField17());
							((crimp_rec) recsNew.get(n)).setField16(
								((crimp_rec) recsNew.get(n)).getField18());
							((crimp_rec) recsNew.get(n)).setField17(0);
							((crimp_rec) recsNew.get(n)).setField18(0);
							((crimp_rec) recsNew.get(n)).setField25(false);
						}
					}
					from = to;
					removeColumns = true;
					if (((crimp_rec) recsNew.get(i)).getField13() == 0
						&& ((crimp_rec) recsNew.get(i)).getField14() == 0
						&& removeColumns == true) {
						removeColumns = true;
					} else {
						removeColumns = false;
					}
				}
				crimpVersion = ((crimp_rec) recsNew.get(i)).getField04();
				family = ((crimp_rec) recsNew.get(i)).getField01();
			}
			if (removeColumns == true) {
				// rimuovere le colonne per 
				for (n = from; n <= to; n++) {
					//						response.write(
					//							"<BR>"
					//								+ "2nd Loop last Crimp. Version."
					//								+ ((crimp_rec) recsNew.get(n)).getField04()
					//								+ " n: "
					//								+ n
					//								+ " from"
					//								+ from
					//								+ " to"
					//								+ to
					//								+ "</BR>");
					((crimp_rec) recsNew.get(n)).setField13(
						((crimp_rec) recsNew.get(n)).getField15());
					((crimp_rec) recsNew.get(n)).setField14(
						((crimp_rec) recsNew.get(n)).getField16());
					((crimp_rec) recsNew.get(n)).setField15(
						((crimp_rec) recsNew.get(n)).getField17());
					((crimp_rec) recsNew.get(n)).setField16(
						((crimp_rec) recsNew.get(n)).getField18());
					((crimp_rec) recsNew.get(n)).setField17(0);
					((crimp_rec) recsNew.get(n)).setField18(0);
					((crimp_rec) recsNew.get(n)).setField25(false);
				}
			}
			///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//----------------------  3nd Loop --------------------------//
			// check columns 13 & 14 is empty
			crimpVersion = null;
			family = null;
			removeColumns = true;
			from = 0;
			to = 0;
			for (i = 0; i < recsNew.size(); i++) {
				to = i;
				//				response.write(
				//					"<BR>"
				//						+ "3° Loop Crimp. Version."
				//						+ "- 25 -"
				//						+ ((crimp_rec) recsNew.get(i)).getField25()
				//						+ "- 26 -"
				//						+ ((crimp_rec) recsNew.get(i)).getField26()
				//						+ "- 52 -"
				//						+ ((crimp_rec) recsNew.get(i)).getField52()
				//						+ " i: "
				//						+ i
				//						+ " - crimpVersion "
				//						+ crimpVersion
				//						+ " - family "
				//						+ family
				//						+ " - removeColumns "
				//						+ removeColumns
				//						+ "</BR>");

				if ((crimpVersion == null
					|| crimpVersion.equals(
						((crimp_rec) recsNew.get(i)).getField04()))
					&& (family == null
						|| family.equals(
							((crimp_rec) recsNew.get(i)).getField01()))) {
					if (((crimp_rec) recsNew.get(i)).getField13() == 0
						&& ((crimp_rec) recsNew.get(i)).getField14() == 0
						&& removeColumns == true) {
						removeColumns = true;
					} else {
						removeColumns = false;
					}
				} else {
					if (removeColumns == true) {
						// shift columns 15 & 16 to columns 13 & 14
						// shift columns 17 & 18 to columns 15 & 16
						for (n = from; n < to; n++) {
							//							response.write(
							//								"<BR>"
							//									+ "3°  Loop Crimp. Version."
							//									+ ((crimp_rec) recsNew.get(n)).getField04()
							//									+ " n: "
							//									+ n
							//									+ " from"
							//									+ from
							//									+ " to"
							//									+ to
							//									+ "</BR>");
							 ((crimp_rec) recsNew.get(n)).setField52(false);
						}
					}
					from = to;
					removeColumns = true;
					if (((crimp_rec) recsNew.get(i)).getField13() == 0
						&& ((crimp_rec) recsNew.get(i)).getField14() == 0
						&& removeColumns == true) {
						removeColumns = true;
					} else {
						removeColumns = false;
					}
				}
				crimpVersion = ((crimp_rec) recsNew.get(i)).getField04();
				family = ((crimp_rec) recsNew.get(i)).getField01();
			}
			if (removeColumns == true) {
				// rimuovere le colonne per 
				for (n = from; n <= to; n++) {
					//					response.write(
					//						"<BR>"
					//							+ "3° Loop last Crimp. Version."
					//							+ ((crimp_rec) recsNew.get(n)).getField04()
					//							+ " n: "
					//							+ n
					//							+ " from"
					//							+ from
					//							+ " to"
					//							+ to
					//							+ "</BR>");
					 ((crimp_rec) recsNew.get(n)).setField52(false);
				}
			}
		} else if (paramType.equals("9")) {
			//			response.write("<BR>recs.size: " + recs.size() + "</BR>");
			for (i = 0; i < recs.size(); i++) {
				recsTit.add(recs.get(i));
			}
			Collections.sort(recsTit, new sortByVersionDate());
			String appField01 = "";
			String appField03 = "";
			for (i = 0; i < recsTit.size(); i++) {
				if (appField01.equals("")) {
					appField01 = ((crimp_rec) recsTit.get(i)).getField01();
					appField03 = ((crimp_rec) recsTit.get(i)).getField03();
				}
				if (appField01 == ((crimp_rec) recsTit.get(i)).getField01()) {
					((crimp_rec) recsTit.get(i)).setField54(appField03);
				} else {
					appField01 = ((crimp_rec) recsTit.get(i)).getField01();
					appField03 = ((crimp_rec) recsTit.get(i)).getField03();
					((crimp_rec) recsTit.get(i)).setField54(appField03);

				}
			}

			for (i = 0; i < recsTit.size(); i++) {
				recsNew.add(recs.get(i));
			}

			Collections.sort(recsNew, new sortByFamilyCrimpVersion());

		} else if (paramType.equals("10") || paramType.equals("11")) {
			/***    prod_machine   ***/
			//			response.write(
			//				"<BR>recsMachine.size: " + recsMachine.size() + "</BR>");
			for (i = 0; i < recsMachine.size(); i++) {
				recsNew.add(recsMachine.get(i));
			}
			Collections.sort(recsNew, new sortByFamilyMachine());
			//			if (paramType.equals("11")) {
			/***    prod_machine_rec   ***/
			//			response.write(
			//				"<BR>recsMachineRec.size: " + recsMachineRec.size() + "</BR>");
			for (i = 0; i < recsMachineRec.size(); i++) {
				recsMachineRecNew.add(recsMachineRec.get(i));
			}
			Collections.sort(recsMachineRecNew, new sortByVoltagesMachine());
			/***    DIE SETS   ***/
			//			response.write(
			//				"<BR>recsDieSets.size: " + recsDieSets.size() + "</BR>");
			for (i = 0; i < recsDieSets.size(); i++) {
				recsDieSetsNew.add(recsDieSets.get(i));
			}
			Collections.sort(recsDieSetsNew, new sortByNumberOptions());
			//			} else {
			/***    AVAILABLE ACCESSORIES   ***/
			//			response.write(
			//				"<BR>recsDieSets.size: " + recsDieSets.size() + "</BR>");
			for (i = 0; i < recsAvailAcc.size(); i++) {
				recsAvailAccNew.add(recsAvailAcc.get(i));
			}
			Collections.sort(recsAvailAccNew, new sortByNumberOptions());
			//			} else {
			/***    OPTIONS   ***/
			//			response.write(
			//				"<BR>recsOption.size: " + recsOption.size() + "</BR>");
			for (i = 0; i < recsOption.size(); i++) {
				recsOptionNew.add(recsOption.get(i));
			}
			Collections.sort(recsOptionNew, new sortByFamilyOptions());

			//			}
			/***    STANDARD   ***/
			//			response.write(
			//				"<BR>recsStandard.size: " + recsStandard.size() + "</BR>");
			for (i = 0; i < recsStandard.size(); i++) {
				recsStandardNew.add(recsStandard.get(i));
			}
			//Collections.sort(recsStandardNew, new sortByFamilyStandard());
		} else {
			Collections.sort(recs, new lineOrderMarc());
			for (i = 0; i < recs.size(); i++) {
				String fld08String =
					((prod_hoses_rec_marcature) recs.get(i)).getField08();
				if (field8 == null || !field8.equals(fld08String)) {
					recsNew.add(recs.get(i));
				}
				field8 = fld08String;
			}
			Collections.sort(recsNew, new sizeOrderMarc());

		}
	}
	//	-------------- add values to JasperReport -----------------------------
	public void mdmJSaddValues(
		IPortalComponentRequest request,
		IPortalComponentResponse response) {

		//			if (paramType.equals("1")) {
		//
		//				//			  TESTATA			
		//				parameters.put("home_dir", CACHE_DIRECTORY);
		//				parameters.put("printable_name", Name);
		//				parameters.put("catalogue_subtitle", catalogue_subtitle);
		//				if (imagePath != null) {
		//					parameters.put(
		//						"catalogue_image",
		//						CACHE_DIRECTORY + "\\" + imagePath);
		//				}
		//				parameters.put("head1", head1);
		//				parameters.put("head2", head2);
		//
		//				parameters.put("version", version);
		//				parameters.put("version_date", version_date);
		//
		//				//			  PIE' PAGINA CENTRALE			
		//				parameters.put("key_performance", key_performance);
		//				//			  PIE' PAGINA LATO SINISTRO	
		//				if (paramType.equals("1")) {
		//					addrow = "\n";
		//				} else {
		//					addrow = "";
		//
		//				}
		//
		//				int i = 100;
		//				footer_left = "footer_left" + Integer.toString(i);
		//				if (main_application != null) {
		//					parameters.put(footer_left, "MAIN APPLICATIONS");
		//					++i;
		//					footer_left = "footer_left" + Integer.toString(i);
		//
		//					parameters.put(footer_left, main_application + addrow);
		//					++i;
		//					footer_left = "footer_left" + Integer.toString(i);
		//				}
		//
		//				if (min_cont_serv_temp != null && max_cont_serv_temp != null) {
		//					int idx1 = min_cont_serv_temp.indexOf(",", 0);
		//					int idx2 = max_cont_serv_temp.indexOf(",", 0);
		//					cont_serv_temp_cel =
		//						min_cont_serv_temp.substring(0, idx1)
		//							+ " / "
		//							+ max_cont_serv_temp.substring(0, idx2);
		//					cont_serv_temp_far =
		//						min_cont_serv_temp.substring(idx1 + 2)
		//							+ " / "
		//							+ max_cont_serv_temp.substring(idx2 + 1);
		//
		//					parameters.put(
		//						footer_left,
		//						"CONTINUOUS SERVICE TEMPERATURE RANGE");
		//					++i;
		//					footer_left = "footer_left" + Integer.toString(i);
		//
		//					parameters.put(
		//						footer_left,
		//						cont_serv_temp_far
		//							+ "\n"
		//							+ cont_serv_temp_cel
		//							+ addrow);
		//					++i;
		//					footer_left = "footer_left" + Integer.toString(i);
		//				}
		//
		//				if (max_operating_temp != null) {
		//					parameters.put(footer_left, "MAX OPERATING TEMPERATURE");
		//					++i;
		//					footer_left = "footer_left" + Integer.toString(i);
		//
		//					parameters.put(footer_left, max_operating_temp + addrow);
		//					++i;
		//					footer_left = "footer_left" + Integer.toString(i);
		//				}
		//				if (min_external_temp != null) {
		//					parameters.put(footer_left, "MIN EXTERNAL TEMPERATURE");
		//					++i;
		//					footer_left = "footer_left" + Integer.toString(i);
		//
		//					parameters.put(footer_left, min_external_temp + addrow);
		//					++i;
		//					footer_left = "footer_left" + Integer.toString(i);
		//				}
		//				if (recommended_fluids != null) {
		//					parameters.put(footer_left, "RECOMMENDED FLUIDS");
		//					++i;
		//					footer_left = "footer_left" + Integer.toString(i);
		//
		//					parameters.put(footer_left, recommended_fluids + addrow);
		//					++i;
		//					footer_left = "footer_left" + Integer.toString(i);
		//				}
		//				if (refrigerants != null) {
		//					parameters.put(footer_left, "REFRIGERANTS");
		//					++i;
		//					footer_left = "footer_left" + Integer.toString(i);
		//
		//					parameters.put(footer_left, refrigerants + addrow);
		//					++i;
		//					footer_left = "footer_left" + Integer.toString(i);
		//				}
		//				if (lubrificants != null) {
		//					parameters.put(footer_left, "LUBRIFICANTS");
		//					++i;
		//					footer_left = "footer_left" + Integer.toString(i);
		//
		//					parameters.put(footer_left, lubrificants + addrow);
		//				}
		//
		//				//			  PIE' PAGINA LATO DESTRO	
		//				i = 100;
		//				footer_right = "footer_right" + Integer.toString(i);
		//				if (tube != null) {
		//					parameters.put(footer_right, "TUBE");
		//					++i;
		//					footer_right = "footer_right" + Integer.toString(i);
		//
		//					parameters.put(footer_right, tube + addrow);
		//					++i;
		//					footer_right = "footer_right" + Integer.toString(i);
		//				}
		//				if (reinforcement != null) {
		//					parameters.put(footer_right, "REINFORCEMENT");
		//					++i;
		//					footer_right = "footer_right" + Integer.toString(i);
		//
		//					parameters.put(footer_right, reinforcement + addrow);
		//					++i;
		//					footer_right = "footer_right" + Integer.toString(i);
		//				}
		//
		//				if (cover != null) {
		//					parameters.put(footer_right, "COVER");
		//					++i;
		//					footer_right = "footer_right" + Integer.toString(i);
		//
		//					parameters.put(footer_right, cover + addrow);
		//					++i;
		//					footer_right = "footer_right" + Integer.toString(i);
		//				}
		//				if (applicable_specs != null) {
		//					parameters.put(footer_right, "APPLICABLE SPECS");
		//					++i;
		//					footer_right = "footer_right" + Integer.toString(i);
		//
		//					parameters.put(footer_right, applicable_specs + addrow);
		//					++i;
		//					footer_right = "footer_right" + Integer.toString(i);
		//				}
		//				if (type_approvals != null) {
		//					parameters.put(footer_right, "TYPE APPROVALS");
		//					++i;
		//					footer_right = "footer_right" + Integer.toString(i);
		//
		//					parameters.put(footer_right, type_approvals + addrow);
		//					++i;
		//					footer_right = "footer_right" + Integer.toString(i);
		//				}
		//
		//				if (other_technical_notes != null) {
		//					parameters.put(footer_right, "OTHER TECHNICAL NOTES");
		//					++i;
		//					footer_right = "footer_right" + Integer.toString(i);
		//
		//					parameters.put(
		//						footer_right,
		//						other_technical_notes + addrow);
		//					++i;
		//					footer_right = "footer_right" + Integer.toString(i);
		//				}
		//				if (standard_Packaging != null) {
		//					parameters.put(footer_right, "STANDARD PACKAGING");
		//					++i;
		//					footer_right = "footer_right" + Integer.toString(i);
		//
		//					parameters.put(footer_right, standard_Packaging + addrow);
		//					++i;
		//					footer_right = "footer_right" + Integer.toString(i);
		//				}
		//				if (catalogue_small_exclamation != null) {
		//					parameters.put(
		//						"catalogue_small_exclamation_caption",
		//						"SMALL EXCLAMATION");
		//					parameters.put(
		//						"catalogue_small_exclamation",
		//						catalogue_small_exclamation);
		//				}
		//				if (catalogue_big_exclamation != null) {
		//					parameters.put(
		//						"catalogue_big_exclamation_caption",
		//						"BIG EXCLAMATION");
		//					parameters.put(
		//						"catalogue_big_exclamation",
		//						catalogue_big_exclamation);
		//				}
		//			} else 

		if (paramType.equals("1") || paramType.equals("2")) {

			//			  TESTATA			
			parameters.put("home_dir", CACHE_DIRECTORY);
			parameters.put("printable_name", Name);
			parameters.put("catalogue_subtitle", catalogue_subtitle);
			if (imagePath != null) {
				parameters.put(
					"catalogue_image",
					CACHE_DIRECTORY + "\\" + imagePath);
			}
			parameters.put("head1", head1);
			parameters.put("head2", head2);

			parameters.put("version", version);
			parameters.put("version_date", version_date);
			parameters.put("key_performance", key_performance);
			parameters.put("applications_fluids", applications_fluids);
			parameters.put(
				"catalogue_bottom_exclamation",
				catalogue_bottom_exclamation);

			//			  PIE' PAGINA LATO SINISTRO	
			if (paramType.equals("1")) {
				addrow = "\n";
				other_technical_notes = null;
				standard_Packaging = null;
			} else {
				addrow = "";

			}

			int i = 100;
			footer_left = "footer_left" + Integer.toString(i);
			if (min_cont_serv_temp != null && max_cont_serv_temp != null) {
				int idx1 = min_cont_serv_temp.indexOf(",", 0);
				int idx2 = max_cont_serv_temp.indexOf(",", 0);
				cont_serv_temp_cel =
					min_cont_serv_temp.substring(0, idx1)
						+ " / "
						+ max_cont_serv_temp.substring(0, idx2);
				cont_serv_temp_far =
					min_cont_serv_temp.substring(idx1 + 2)
						+ " / "
						+ max_cont_serv_temp.substring(idx2 + 1);

				parameters.put(
					footer_left,
					"CONTINUOUS SERVICE TEMPERATURE RANGE");
				++i;
				footer_left = "footer_left" + Integer.toString(i);

				parameters.put(
					footer_left,
					cont_serv_temp_far + "\n" + cont_serv_temp_cel + addrow);
				++i;
				footer_left = "footer_left" + Integer.toString(i);
			}

			if (max_operating_temp != null) {
				parameters.put(footer_left, "MAX OPERATING TEMPERATURE");
				++i;
				footer_left = "footer_left" + Integer.toString(i);

				parameters.put(footer_left, max_operating_temp + addrow);
				++i;
				footer_left = "footer_left" + Integer.toString(i);
			}
			if (min_external_temp != null) {
				parameters.put(footer_left, "MIN EXTERNAL TEMPERATURE");
				++i;
				footer_left = "footer_left" + Integer.toString(i);

				parameters.put(footer_left, min_external_temp + addrow);
				++i;
				footer_left = "footer_left" + Integer.toString(i);
			}
			//			  PIE' PAGINA CENTRALE

			i = 100;
			footer_central = "footer_central" + Integer.toString(i);
			if (tube != null) {
				parameters.put(footer_central, "TUBE");
				++i;
				footer_central = "footer_central" + Integer.toString(i);

				parameters.put(footer_central, tube + addrow);
				++i;
				footer_central = "footer_central" + Integer.toString(i);
			}
			if (reinforcement != null) {
				parameters.put(footer_central, "REINFORCEMENT");
				++i;
				footer_central = "footer_central" + Integer.toString(i);

				parameters.put(footer_central, reinforcement + addrow);
				++i;
				footer_central = "footer_central" + Integer.toString(i);
			}

			if (cover != null) {
				parameters.put(footer_central, "COVER");
				++i;
				footer_central = "footer_central" + Integer.toString(i);

				parameters.put(footer_central, cover + addrow);
				++i;
				footer_central = "footer_central" + Integer.toString(i);
			}

			//			  PIE' PAGINA LATO DESTRO	
			i = 100;
			footer_right = "footer_right" + Integer.toString(i);
			if (applicable_specs != null) {
				parameters.put(footer_right, "APPLICABLE SPECS");
				++i;
				footer_right = "footer_right" + Integer.toString(i);

				parameters.put(footer_right, applicable_specs + addrow);
				++i;
				footer_right = "footer_right" + Integer.toString(i);
			}
			if (type_approvals != null) {
				parameters.put(footer_right, "TYPE APPROVALS");
				++i;
				footer_right = "footer_right" + Integer.toString(i);

				parameters.put(footer_right, type_approvals + addrow);
				++i;
				footer_right = "footer_right" + Integer.toString(i);
			}

			if (other_technical_notes != null) {
				parameters.put(footer_right, "OTHER TECHNICAL NOTES");
				++i;
				footer_right = "footer_right" + Integer.toString(i);

				parameters.put(footer_right, other_technical_notes + addrow);
				++i;
				footer_right = "footer_right" + Integer.toString(i);
			}
			if (standard_Packaging != null) {
				parameters.put(footer_right, "STANDARD PACKAGING");
				++i;
				footer_right = "footer_right" + Integer.toString(i);

				parameters.put(footer_right, standard_Packaging + addrow);
				++i;
				footer_right = "footer_right" + Integer.toString(i);
			}

		} else if (paramType.equals("8") || paramType.equals("9")) {

			//				  TESTATA			
			parameters.put("home_dir", CACHE_DIRECTORY);
			parameters.put("family", family);
			parameters.put("release_number", release_number);
			parameters.put("date", date);
			parameters.put("crimp_version", crimp_version);
			parameters.put("general", general);
			parameters.put("skive_tool", skive_tool);
			parameters.put("crimp_tool", crimp_tool);
			parameters.put("remarks", remarks);
		} else if (paramType.equals("10") || paramType.equals("11")) {

			//				  TESTATA			
			parameters.put("home_dir", CACHE_DIRECTORY);
		} else {
			//				  TESTATA			
			parameters.put("home_dir", CACHE_DIRECTORY);
			parameters.put("brand_package", fld35String);
			parameters.put("customer", fld36String);
		}
	}
	/*****************************************************************/
	/***             Retrieve data from watermark                  ***/
	/*****************************************************************/
	public void mdmGetWatermark(
		IPortalComponentRequest request,
		IPortalComponentResponse response) {
		getRecords = false;
		try {
			//			response.write("<BR>prod_machine_rec[" + "-</BR>");

			/*** create a ResultSetDefinition on the prod_machine_rec table ***/
			ResultSetDefinition rsd_watermark;
			rsd_watermark = new ResultSetDefinition("Watermark");
			rsd_watermark.AddField("Watermark_text");

			/*** create an empty Search object ***/
			Search search_options = new Search(rsd_watermark.GetTable());
			/*** Filters ***/

			FreeFormTableParameter fftp_W =
				search_options.GetParameters().NewFreeFormTableParameter(
					rsd_watermark.GetTable());
			/*
									FreeFormParameterField ffpf_W_Key01 =
							fftp_W.GetFields().New("Watermark_text");
						ffpf_W_Key01.GetFreeForm().NewString(
							"is NULL",
							FreeFormParameter.BooleanParameterType);
			*/
			/*** Printout Fields ***/
			FreeFormParameterField ffpf_W_Field00 =
				fftp_W.GetFields().New("Watermark_text");

			A2iResultSet rs_W =
				catalogData.GetResultSet(
					search_options,
					rsd_watermark,
					null,
					true,
					0);
			//						response.write(
			//							"<BR>prod_machine_rec recordCount: "
			//								+ rs_M.GetRecordCount()
			//								+ "</BR>");
			for (int i = 0; i < rs_W.GetRecordCount(); i++) {
				// S_Field00	String
				if (!rs_W.GetValueAt(i, "Watermark_text").IsNull()) {

					strWatermark =
						rs_W.GetValueAt(i, "Watermark_text").GetStringValue();
					response.write(
						"<BR> Watermark_text: " + strWatermark + "</BR>");
				}
			}
		} catch (StringException e) {
			response.write(
				"<BR> e.printStackTrace: " + e.getMessage() + "</BR>");
			e.printStackTrace();
		}

	}
	//	-------------- Login MDM Server -------------------------------------------
	public void mdmJSprintValues(
		IPortalComponentRequest request,
		IPortalComponentResponse response) {

		mdmGetWatermark(request, response);
		JRBeanCollectionDataSource datasource =
			new JRBeanCollectionDataSource(recsNew);

		//rendering e generazione del file PDF
		JasperPrint jp = null;
		JasperPrint jp_00 = null;
		JasperPrint jp_01 = null;
		JasperPrint jp_02 = null;

		if (paramType.equals("1")) {
			JASPER_FILENAME = "ManuliCatalogue";
			parameters.put("watermark", strWatermark);
		} else if (paramType.equals("2")) {
			JASPER_FILENAME = "ManuliCatalogueExtended";
			parameters.put("watermark", strWatermark);
		} else if (paramType.equals("8")) {
			JASPER_FILENAME = "ManuliExtCrimping";
			parameters.put("watermark", strWatermark);
			parameters.put("titleBooklet", paramTitleBooklet);
			parameters.put("titleDatasheet", paramTitleDatasheet);
			String datePubblication =
				paramDatePubblication.substring(0, 4)
					+ "/"
					+ paramDatePubblication.substring(5, 7)
					+ "/"
					+ paramDatePubblication.substring(8, 10);
			parameters.put("datePubblication", datePubblication);

			// settiamo la data in inglese: 20 February 2014
			int month = Integer.parseInt(paramDatePubblication.substring(5, 7));
			String monthPubblication = "";
			switch (month) {
				case 1 :
					{
						monthPubblication = "JANUARY";
						break;
					}
				case 2 :
					{
						monthPubblication = "FEBRUARY";
						break;
					}
				case 3 :
					{
						monthPubblication = "MARCH";
						break;
					}
				case 4 :
					{
						monthPubblication = "APRIL";
						break;
					}
				case 5 :
					{
						monthPubblication = "MAY";
						break;
					}
				case 6 :
					{
						monthPubblication = "JUNE";
						break;
					}
				case 7 :
					{
						monthPubblication = "JULY";
						break;
					}
				case 8 :
					{
						monthPubblication = "AUGUST";
						break;
					}
				case 9 :
					{
						monthPubblication = "SEPTEMBER";
						break;
					}
				case 10 :
					{
						monthPubblication = "OCTOBER";
						break;
					}
				case 11 :
					{
						monthPubblication = "NOVEMBER";
						break;
					}
				case 12 :
					{
						monthPubblication = "DECEMBER";
						break;
					}
				default :
					{
						break;
					}
			};
			parameters.put("monthPubblication", monthPubblication);

		} else if (paramType.equals("9")) {
			JASPER_FILENAME = "ManuliIntCrimping";
			JRBeanCollectionDataSource datasource1 =
				new JRBeanCollectionDataSource(recsNew);
			JRBeanCollectionDataSource datasource2 =
				new JRBeanCollectionDataSource(recsNew);
			parameters.put("watermark", strWatermark);
			parameters.put("sourceDataSub1", datasource1);
			parameters.put("sourceDataSub2", datasource2);
			parameters.put("titleDatasheet", paramTitleDatasheet);
		} else if (
			(paramType.equals("10") || paramType.equals("11"))
				&& paramFamily.equals("true")) {

			JASPER_FILENAME_01 = "ManuliMachineCatalogue";
			JASPER_FILENAME_02 = "ManuliMachineReference";

			JRBeanCollectionDataSource datasource3 =
				new JRBeanCollectionDataSource(recsStandardNew);
			JRBeanCollectionDataSource datasource4 =
				new JRBeanCollectionDataSource(recsOptionNew);
			JRBeanCollectionDataSource datasource5 =
				new JRBeanCollectionDataSource(recsSelection);
			JRBeanCollectionDataSource datasource6 =
				new JRBeanCollectionDataSource(recsNew);
			JRBeanCollectionDataSource datasource1 =
				new JRBeanCollectionDataSource(recsMachineRecNew);
			JRBeanCollectionDataSource datasource2 =
				new JRBeanCollectionDataSource(recsDieSetsNew);
			JRBeanCollectionDataSource datasource7 =
				new JRBeanCollectionDataSource(recsAvailAccNew);

			parameters_00.put("watermark", strWatermark);
			parameters_00.put("home_dir", CACHE_DIRECTORY);

			parameters_01.put("watermark", strWatermark);
			parameters_01.put("home_dir", CACHE_DIRECTORY);
			parameters_01.put("SUBREPORT_DIR", CACHE_DIRECTORY);
			parameters_01.put("sourceDataSub3", datasource3);
			parameters_01.put("sourceDataSub4", datasource4);
			parameters_01.put("sourceDataSub5", datasource5);
			parameters_01.put("sourceDataSub6", datasource6);

			parameters_02.put("watermark", strWatermark);
			parameters_02.put("home_dir", CACHE_DIRECTORY);
			parameters_02.put("SUBREPORT_DIR", CACHE_DIRECTORY);
			parameters_02.put("sourceDataSub1", datasource1);
			parameters_02.put("sourceDataSub2", datasource2);
			parameters_02.put("sourceDataSub7", datasource7);

			try {
				jp_00 =
					JasperFillManager.fillReport(
						JASPER_REPORT_FOLDER + JASPER_FILENAME_01 + ".jasper",
						parameters_00,
						new JREmptyDataSource());
				jp_01 =
					JasperFillManager.fillReport(
						JASPER_REPORT_FOLDER + JASPER_FILENAME_01 + ".jasper",
						parameters_01,
						new JRBeanCollectionDataSource(recsNew));

				jp_02 =
					JasperFillManager.fillReport(
						JASPER_REPORT_FOLDER + JASPER_FILENAME_02 + ".jasper",
						parameters_02,
						new JRBeanCollectionDataSource(recsNew));
				jp_00.removePage(0);
				List pages_01 = jp_01.getPages();
				List pages_02 = jp_02.getPages();
				for (int j = 0; j < pages_01.size(); j++) {
					JRPrintPage object_01 = (JRPrintPage) pages_01.get(j);
					JRPrintPage object_02 = (JRPrintPage) pages_02.get(j);
					jp_00.addPage(object_01);
					jp_00.addPage(object_02);

				}
			} catch (JRException e3) {
				response.write(
					"Error Print Datasheet: " + e3.getMessage() + "<BR>");
				e3.printStackTrace();
			}
		} else if (paramType.equals("10")) {
			JASPER_FILENAME = "ManuliMachineCatalogue";
			JRBeanCollectionDataSource datasource3 =
				new JRBeanCollectionDataSource(recsStandardNew);
			JRBeanCollectionDataSource datasource4 =
				new JRBeanCollectionDataSource(recsOptionNew);
			JRBeanCollectionDataSource datasource5 =
				new JRBeanCollectionDataSource(recsSelection);
			JRBeanCollectionDataSource datasource6 =
				new JRBeanCollectionDataSource(recsNew);
			parameters.put("watermark", strWatermark);
			parameters.put("sourceDataSub3", datasource3);
			parameters.put("sourceDataSub4", datasource4);
			parameters.put("sourceDataSub5", datasource5);
			parameters.put("sourceDataSub6", datasource6);
			//			response.write("<BR>print values: " + "</BR>");

		} else if (paramType.equals("11")) {
			JASPER_FILENAME = "ManuliMachineReference";
			JRBeanCollectionDataSource datasource1 =
				new JRBeanCollectionDataSource(recsMachineRecNew);
			JRBeanCollectionDataSource datasource2 =
				new JRBeanCollectionDataSource(recsDieSetsNew);
			JRBeanCollectionDataSource datasource7 =
				new JRBeanCollectionDataSource(recsAvailAccNew);
			parameters.put("watermark", strWatermark);
			parameters.put("sourceDataSub1", datasource1);
			parameters.put("sourceDataSub2", datasource2);
			parameters.put("sourceDataSub7", datasource7);
		} else {
			JASPER_FILENAME = "ManuliCatalogueMarcature";
			parameters.put("watermark", strWatermark);

		}
		try {

			if ((paramType.equals("10") || paramType.equals("11"))
				&& paramFamily.equals("true")) {
				jp = jp_00;
			} else {
				jp =
					JasperFillManager.fillReport(
						JASPER_REPORT_FOLDER + JASPER_FILENAME + ".jasper",
						parameters,
						datasource);
			}
			//				  simpleDS);
			JRPdfExporter exporter = new JRPdfExporter();
			HttpServletResponse res = request.getServletResponse(true);
			//					set the mimetype

			exporter.setParameter(JRExporterParameter.JASPER_PRINT, jp);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, baos);
			exporter.exportReport();
			response.write("fillreport passed" + "<BR>");

			byte[] bytes = baos.toByteArray();
			//					res.addHeader("Cache-Control", "max-age=3600");
			//					res.setHeader("Content-Disposition",  "inline; filename=\"" + JASPER_REPORT_FOLDER + JASPER_FILENAME + ".pdf" +"\"");

			res.setHeader("CACHE-CONTROL", "PRIVATE");
			res.setHeader("Cache-Control", "maxage=3600");
			res.setHeader("Pragma", "public");
			res.setHeader("Accept-Ranges", "none");

			res.setContentType("application/pdf");
			res.setContentLength(bytes.length);
			try {
				String timeStamp =
					new SimpleDateFormat("yyyyMMddHHmmss").format(
						new java.util.Date());
				response.write("fileOuputStream passed" + "<BR>");

				FileOutputStream fileOuputStream =
					new FileOutputStream(
						JASPER_REPORT_FOLDER
							+ timeStamp
							+ JASPER_FILENAME
							+ ".pdf");
				fileOuputStream.write(bytes);
				fileOuputStream.close();
				ServletOutputStream ouputStream = res.getOutputStream();
				ouputStream.write(bytes, 0, bytes.length);
				ouputStream.flush();
				ouputStream.close();
				try {
					File file =
						new File(
							JASPER_REPORT_FOLDER
								+ timeStamp
								+ JASPER_FILENAME
								+ ".pdf");

					if (file.delete()) {
						System.out.println(file.getName() + " is deleted!");
					} else {
						System.out.println("Delete operation is failed.");
					}

				} catch (Exception e) {

					e.printStackTrace();

				}
			} catch (IOException e2) {
				response.write(
					"Error fileOuputStream " + e2.toString() + "<BR>");
				e2.printStackTrace();
			}
		} catch (JRException e3) {
			response.write("Error fillReport " + e3.toString() + "<BR>");
			e3.printStackTrace();
		}
		try {
			catalogData.Logout();
		} catch (Exception e) {
			response.write("Error Print Datasheet: " + e.toString() + "<BR>");
		}
	}

	//	-------------- Login MDM Server -------------------------------------------
	public void mdmGetDataRecOutput(
		IPortalComponentRequest request,
		IPortalComponentResponse response) {

		response.write("<BR>Family: " + paramTableFamily + "</BR>");
		response.write("<BR>DynamicParameter: " + DynamicParameter + "</BR>");
		response.write("<BR>paramCrimpVersion: " + paramCrimpVersion + "</BR>");
		response.write("<BR>paramCrimpingFlag: " + paramCrimpingFlag + "</BR>");
		response.write(
			"<BR>paramHoseFamilyFlag: " + paramHoseFamilyFlag + "</BR>");
		response.write("<BR>paramVersion: " + paramVersion + "</BR>");
		response.write(
			"<BR>paramRestrictedFlag: " + paramRestrictedFlag + "</BR>");
		response.write("<BR>recsNew.size: " + recsNew.size() + "</BR>");

		for (int i = 0; i < recsNew.size(); i++) {
			response
				.write(
					"<BR>rec: "
					+ i
					+ "--01="
					+ ((crimp_rec) recsNew.get(i)).getField01()
			/*
									+ "--02="
									+ ((crimp_rec) recsNew.get(i)).getField02()
			*/
			+"--03="
				+ ((crimp_rec) recsNew.get(i)).getField03()
				+ "--04="
				+ ((crimp_rec) recsNew.get(i)).getField04()
				+ "--54="
				+ ((crimp_rec) recsNew.get(i)).getField54()
			/*
									+ "--05="
									+ ((crimp_rec) recsNew.get(i)).getField05()
									+ "--06="
									+ ((crimp_rec) recsNew.get(i)).getField06()
									+ "--07="	
									+ ((crimp_rec) recsNew.get(i)).getField07()
									+ "--08="
									+ ((crimp_rec) recsNew.get(i)).getField08()
									+ "--09="
									+ ((crimp_rec) recsNew.get(i)).getField09()
									+ "--10="
									+ ((crimp_rec) recsNew.get(i)).getField10()
									+ "--11="
									+ ((crimp_rec) recsNew.get(i)).getField11()
									+ "--12="
									+ ((crimp_rec) recsNew.get(i)).getField12()
			*/
			+"--13="
				+ ((crimp_rec) recsNew.get(i)).getField13()
				+ "--14="
				+ ((crimp_rec) recsNew.get(i)).getField14()
				+ "--15="
				+ ((crimp_rec) recsNew.get(i)).getField15()
				+ "--16="
				+ ((crimp_rec) recsNew.get(i)).getField16()
				+ "--17="
				+ ((crimp_rec) recsNew.get(i)).getField17()
				+ "--18="
				+ ((crimp_rec) recsNew.get(i)).getField18()
			/*
									+ "--19="
									+ ((crimp_rec) recsNew.get(i)).getField19()
									+ "--20="
									+ ((crimp_rec) recsNew.get(i)).getField20()
									+ "--21="
									+ ((crimp_rec) recsNew.get(i)).getField21()
									+ "--22="
									+ ((crimp_rec) recsNew.get(i)).getField22()
									+ "--23="
									+ ((crimp_rec) recsNew.get(i)).getField23()
									+ "--24="
									+ ((crimp_rec) recsNew.get(i)).getField24()
			*/
			+"--25="
				+ ((crimp_rec) recsNew.get(i)).getField25()
				+ "--26="
				+ ((crimp_rec) recsNew.get(i)).getField26()
				+ "--52="
				+ ((crimp_rec) recsNew.get(i)).getField52()
			/*
									+ "--51="
									+ ((crimp_rec) recsNew.get(i)).getField51()
			*/
			+"</BR>");
		}
	}

	//	-------------- MDM Error Message -------------------------------------------
	public void mdmGetMessage(
		IPortalComponentRequest request,
		IPortalComponentResponse response) {

		response.write("<BR>Attention: ");
		response.write("<BR>Please select a printable item</BR>");
	}

	//  --------------- Call WebService -------------------------------------------
	public void contactPI(
		IPortalComponentRequest request,
		IPortalComponentResponse response,
		String type)
		throws Exception {

//		Authenticator.setDefault(new Authenticator() {
//			//		   This method is called when a password-protected URL is accessed
//			//		  @Override
//			protected PasswordAuthentication getPasswordAuthentication() {
//				return new PasswordAuthentication(
//					"GREENOR",
//					"Gora2017".toCharArray());
//			}
//		});
//		HttpURLConnection con =
//			(HttpURLConnection) new URL("http://manpisvi01.sapman.dc:50000/HttpAdapter/HttpMessageServlet?interfaceNamespace=urn:manulihydraulic.com:Portal:Adobe&interface=oa_Adobe_items&senderService=EMTCLNT400&senderParty=&receiverParty=&receiverService=&qos=EO")
//				.openConnection();
//		con.setRequestMethod("POST");
//		con.setRequestProperty("content-type", "text/xml");
//		con.setDoOutput(true);
		
		String s = "";
		int case_mdm = 0;
		if (type.equals("Machine")) {
			case_mdm = 1;
			
			Authenticator.setDefault(new Authenticator() {
						//		   This method is called when a password-protected URL is accessed
						//		  @Override
						protected PasswordAuthentication getPasswordAuthentication() {
							return new PasswordAuthentication(
								"GREENOR",
								"Gora2017".toCharArray());
						}
					});
			HttpURLConnection con =
						(HttpURLConnection) new URL("http://manpisvi01.sapman.dc:50000/HttpAdapter/HttpMessageServlet?interfaceNamespace=urn:manulihydraulic.com:Portal:Adobe&interface=oa_Adobe_Machines&senderService=EMTCLNT400&senderParty=&receiverParty=&receiverService=&qos=EO")
							.openConnection();
					con.setRequestMethod("POST");
					con.setRequestProperty("content-type", "text/xml");
					con.setDoOutput(true);
					s = setOutputMDM_Machine(request, response, s);
					con.getOutputStream().write(s.getBytes());
					byte[] b = new byte[con.getInputStream().available()];
					con.getInputStream().read(b);
					
		}
		if (type.equals("CrimpRec")) {
			Authenticator.setDefault(new Authenticator() {
						//		   This method is called when a password-protected URL is accessed
						//		  @Override
						protected PasswordAuthentication getPasswordAuthentication() {
							return new PasswordAuthentication(
								"GREENOR",
								"Gora2017".toCharArray());
						}
					});
			case_mdm = 2;
			HttpURLConnection	con =
			(HttpURLConnection) new URL("http://manpisvi01.sapman.dc:50000/HttpAdapter/HttpMessageServlet?interfaceNamespace=urn:manulihydraulic.com:Portal:Adobe&interface=oa_Adobe_CrimpRec&senderService=EMTCLNT400&senderParty=&receiverParty=&receiverService=&qos=EO")
							.openConnection();
					con.setRequestMethod("POST");
					con.setRequestProperty("content-type", "text/xml");
					con.setDoOutput(true);
					s = setOutputMDM_CrimpRec(request, response, s);
					con.getOutputStream().write(s.getBytes());
					byte[] b = new byte[con.getInputStream().available()];
					con.getInputStream().read(b);
					response.write(new String(b));
		}
		if (type.equals("ProdHoses")) {
			Authenticator.setDefault(new Authenticator() {
						//		   This method is called when a password-protected URL is accessed
						//		  @Override
						protected PasswordAuthentication getPasswordAuthentication() {
							return new PasswordAuthentication(
								"GREENOR",
								"Gora2017".toCharArray());
						}
					});
			case_mdm = 3;
			HttpURLConnection	con =
						(HttpURLConnection) new URL("http://manpisvi01.sapman.dc:50000/HttpAdapter/HttpMessageServlet?interfaceNamespace=urn:manulihydraulic.com:Portal:Adobe&interface=oa_Adobe_Prodhoses&senderService=EMTCLNT400&senderParty=&receiverParty=&receiverService=&qos=EO")
							.openConnection();
					con.setRequestMethod("POST");
					con.setRequestProperty("content-type", "text/xml");
					con.setDoOutput(true);
					s = setOutputMDM_ProdHoses(request, response, s);
					con.getOutputStream().write(s.getBytes());
					byte[] b = new byte[con.getInputStream().available()];
					con.getInputStream().read(b);
					response.write(new String(b));
		}
		if (type.equals("ProdHosesRec")) {
			Authenticator.setDefault(new Authenticator() {
						//		   This method is called when a password-protected URL is accessed
						//		  @Override
						protected PasswordAuthentication getPasswordAuthentication() {
							return new PasswordAuthentication(
								"GREENOR",
								"Gora2017".toCharArray());
						}
					});
			case_mdm = 4;
			HttpURLConnection	con =
						(HttpURLConnection) new URL("http://manpisvi01.sapman.dc:50000/HttpAdapter/HttpMessageServlet?interfaceNamespace=urn:manulihydraulic.com:Portal:Adobe&interface=oa_Adobe_Prodhoses_rec&senderService=EMTCLNT400&senderParty=&receiverParty=&receiverService=&qos=EO")
							.openConnection();
					con.setRequestMethod("POST");
					con.setRequestProperty("content-type", "text/xml");
					con.setDoOutput(true);
					s = setOutputMDM_ProdHosesRec(request, response, s);
					con.getOutputStream().write(s.getBytes());
					byte[] b = new byte[con.getInputStream().available()];
					con.getInputStream().read(b);
					response.write(new String(b));
		}
//		switch (case_mdm) {
//			case 1 :
//				setOutputMDM_Machine(request, response, s);
//				break;
//
//			case 2 :
//				setOutputMDM_CrimpRec(request, response, s);
//				break;
//
//			case 3 :
//				setOutputMDM_ProdHoses(request, response, s);
//				break;
//
//			case 4 :
//				setOutputMDM_ProdHosesRec(request, response, s);
//				break;
//
//			default :
//				break;
//		}

		//
		//"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:com:sap:netweaver:pi:monitoring\">" +
		//		  "   <soapenv:Header/>" +
		//		  "   <soapenv:Body>" +
		//		  "      <urn:Content>" +
		//		  "			<urn:xml>	" +
		//		  "			</urn:xml> 	" +
		//		  "      </urn:Content>" +
		//		  "   </soapenv:Body>" +
		//		  "</soapenv:Envelope>";
//		con.getOutputStream().write(s.getBytes());
//		byte[] b = new byte[con.getInputStream().available()];
//		con.getInputStream().read(b);
//		response.write(new String(b));
		//		  System.out.println(new String(b));
	}

	/**
	 * @param request
	 * @param response
	 * @param s
	 */
	public String setOutputMDM_ProdHosesRec(
		IPortalComponentRequest request,
		IPortalComponentResponse response,
		String s) {
		// TODO Auto-generated method stub
		String items = "";
		items = buildProdHosesRec(request, response, items);
		
		String Dyn_Par = convertXml(DynamicParameter);
		String paramTab_Items = convertXml(paramTableItems);
		String paraValue = convertXml(paramValue);
		String paraCat = convertXml(paramCatalog);
		String paraFam = convertXml(paramFamily);
		
		s =
		"<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
			+ "<ns0:Adobe_ProdHosesRec xmlns:ns0=\"urn:manulihydraulic.com:Portal:Adobe\">"
			+ "<Header>"
			+ "<paramTableItems>"
			+ paramTab_Items
			+ "</paramTableItems>"
			+ "<paramValue>"
			+ paraValue
			+ "</paramValue>"
			+ "<paramCatalog>"
			+ paraCat
			+ "</paramCatalog>"
			+ "<paramFamily>"
			+ paraFam
			+ "</paramFamily>"
			+ "<DynamicParameter>"
			+ Dyn_Par
			+ "</DynamicParameter>"
			+ "<recsMachine_size>"
			+ recsNew.size()
			+ "</recsMachine_size>"
			+ items
			+ "</Header>"
			+ "</ns0:Adobe_ProdHosesRec>";
			
//			response.write(s);
			
 		return s;
	}	
	/**
	 * @param request
	 * @param response
	 * @param items
	 */
	private String buildProdHosesRec(IPortalComponentRequest request, IPortalComponentResponse response, String items) {
		// TODO Auto-generated method stub
		ResultSetDefinition rsd_prod_hoses_rec;
				rsd_prod_hoses_rec = new ResultSetDefinition(paramTableItems);
				rsd_prod_hoses_rec.AddField("Name");
				rsd_prod_hoses_rec.AddField("line");
				rsd_prod_hoses_rec.AddField("nominal_size");
				rsd_prod_hoses_rec.AddField("reinforcement_outside_diameter_mm");
				rsd_prod_hoses_rec.AddField("reinforcement_outside_diameter_inch");
				rsd_prod_hoses_rec.AddField("outside_diameter_mm");
				rsd_prod_hoses_rec.AddField("outside_diameter_inch");
				rsd_prod_hoses_rec.AddField("Max_working_pressure_bar");
				rsd_prod_hoses_rec.AddField("max_working_pressure_psi");
				rsd_prod_hoses_rec.AddField("burst_pressure_bar");
				rsd_prod_hoses_rec.AddField("burst_pressure_psi");
				rsd_prod_hoses_rec.AddField("min_bend_radius_mm");
				rsd_prod_hoses_rec.AddField("min_bend_radius_inch");
				rsd_prod_hoses_rec.AddField("weight_g_m");
				rsd_prod_hoses_rec.AddField("weight_lbs_ft");
				//		----          DELETE ML 30.01.2013
				//		rsd_prod_hoses_rec.AddField("standard_ferrule");
				//		rsd_prod_hoses_rec.AddField("standard_insert");
				//		----          DELETE ML 30.01.2013
				//		----          INSERT ML 30.01.2013
				rsd_prod_hoses_rec.AddField("Std_Ferrule1");
				rsd_prod_hoses_rec.AddField("Std_Insert1");
				rsd_prod_hoses_rec.AddField("Std_Ferrule2");
				rsd_prod_hoses_rec.AddField("Std_Insert2");
				rsd_prod_hoses_rec.AddField("Alt_Ferrule1");
				rsd_prod_hoses_rec.AddField("Alt_Insert1");
				rsd_prod_hoses_rec.AddField("Alt_Ferrule2");
				rsd_prod_hoses_rec.AddField("Alt_Insert2");
				//		----          INSERT ML 30.01.2013
				rsd_prod_hoses_rec.AddField("standard_mf3000");

				//		  variabili per stampa Catalogue Extended	
				rsd_prod_hoses_rec.AddField("hose_inside_diameter_min_mm");
				rsd_prod_hoses_rec.AddField("hose_inside_diameter_min_inches");
				rsd_prod_hoses_rec.AddField("hose_inside_diameter_max_mm");
				rsd_prod_hoses_rec.AddField("hose_inside_diameter_max_inches");
				rsd_prod_hoses_rec.AddField("reinforcement_outside_diameter_min_mm");
				rsd_prod_hoses_rec.AddField(
					"reinforcement_outside_diameter_min_inches");
				rsd_prod_hoses_rec.AddField("reinforcement_outside_diameter_max_mm");
				rsd_prod_hoses_rec.AddField(
					"reinforcement_outside_diameter_max_inches");
				rsd_prod_hoses_rec.AddField("hose_outside_diameter_min_mm");
				rsd_prod_hoses_rec.AddField("hose_outside_diameter_min_inches");
				rsd_prod_hoses_rec.AddField("hose_outside_diameter_max_mm");
				rsd_prod_hoses_rec.AddField("hose_outside_diameter_max_inches");
				rsd_prod_hoses_rec.AddField("hose_max_wall_thickness_difference_mm");
				rsd_prod_hoses_rec.AddField(
					"hose_max_wall_thickness_difference_inches");
				//		  variabili per stampa Catalogue Extended	

				//		  variabili per stampa Marcature
				rsd_prod_hoses_rec.AddField("category");
				rsd_prod_hoses_rec.AddField("brand_pack");
				rsd_prod_hoses_rec.AddField("brand_customer");
				rsd_prod_hoses_rec.AddField("marking_technology");
				rsd_prod_hoses_rec.AddField("brand_color");
				rsd_prod_hoses_rec.AddField("brand_logo");
				rsd_prod_hoses_rec.AddField("brand_notes");
				rsd_prod_hoses_rec.AddField("brand_date_type");
				rsd_prod_hoses_rec.AddField("brand_customer");
				rsd_prod_hoses_rec.AddField("department");
				rsd_prod_hoses_rec.AddField("brand");
				//		  variabili per stampa Marcature

				rsd_prod_hoses_rec.AddField("category");
				rsd_prod_hoses_rec.AddField("Printable");
				rsd_prod_hoses_rec.AddField("public");
				rsd_prod_hoses_rec.AddField("Phase");

				// create an empty Search object
				Search search = new Search(rsd_prod_hoses_rec.GetTable());
				FreeFormTableParameter fftpNames =
					search.GetParameters().NewFreeFormTableParameter(
						rsd_prod_hoses_rec.GetTable());

				// Filters		
				if (paramType.equals("3")) {
					FreeFormParameterField ffpfKey01 =
						fftpNames.GetFields().New("brand_pack");
					ffpfKey01.GetFreeForm().NewString(
						paramBrand,
						FreeFormParameter.EqualToSearchType);
					if (paramCategory.equals("false")) {
						FreeFormParameterField ffpfKey02 =
							fftpNames.GetFields().New("category");
						ffpfKey02.GetFreeForm().NewString(
							paramValue,
							FreeFormParameter.EqualToSearchType);
					}
					FreeFormParameterField ffpf_M_Key03 =
						fftpNames.GetFields().New("Printable");
					ffpf_M_Key03.GetFreeForm().NewString(
						"YES",
						FreeFormParameter.EqualToSearchType);
					FreeFormParameterField ffpf_M_Key04 =
						fftpNames.GetFields().New("phase_flag");
					ffpf_M_Key04.GetFreeForm().NewString(
						"APPROVED",
						FreeFormParameter.EqualToSearchType);
				} else {
					FreeFormParameterField ffpfKey01 =
						fftpNames.GetFields().New("category");
					ffpfKey01.GetFreeForm().NewString(
						paramValue,
						FreeFormParameter.EqualToSearchType);
					if (paramCatalog.equals("false")) {
						FreeFormParameterField ffpfKey03 =
							fftpNames.GetFields().New("catalogue");
						ffpfKey03.GetFreeForm().NewString(
							"YES",
							FreeFormParameter.SubstringSearchType);
					}
					FreeFormParameterField ffpf_M_Key03 =
						fftpNames.GetFields().New("Printable");
					ffpf_M_Key03.GetFreeForm().NewString(
						"YES",
						FreeFormParameter.EqualToSearchType);
					FreeFormParameterField ffpf_M_Key04 =
						fftpNames.GetFields().New("phase_flag");
					ffpf_M_Key04.GetFreeForm().NewString(
						"APPROVED",
						FreeFormParameter.EqualToSearchType);

				}

				//			FreeFormParameterField ffpfKey03 = fftpNames.GetFields().New("public");
				//			ffpfKey03.GetFreeForm().NewString("YES", FreeFormParameter.SubstringSearchType);
				FreeFormParameterField ffpfKey04 = fftpNames.GetFields().New("Phase");
				ffpfKey04.GetFreeForm().NewString(
					"BLOCKED",
					FreeFormParameter.NotEqualToSearchType);

				//		  Printout Fields 
				FreeFormParameterField ffpfField00 = fftpNames.GetFields().New("Name");
				FreeFormParameterField ffpfField01 = fftpNames.GetFields().New("line");
				FreeFormParameterField ffpfField02 =
					fftpNames.GetFields().New("nominal_size");
				FreeFormParameterField ffpfField03 =
					fftpNames.GetFields().New("reinforcement_outside_diameter_mm");
				FreeFormParameterField ffpfField04 =
					fftpNames.GetFields().New("reinforcement_outside_diameter_inch");
				FreeFormParameterField ffpfField05 =
					fftpNames.GetFields().New("outside_diameter_mm");
				FreeFormParameterField ffpfField06 =
					fftpNames.GetFields().New("outside_diameter_inch");
				FreeFormParameterField ffpfField07 =
					fftpNames.GetFields().New("Max_working_pressure_bar");
				FreeFormParameterField ffpfField08 =
					fftpNames.GetFields().New("max_working_pressure_psi");
				FreeFormParameterField ffpfField09 =
					fftpNames.GetFields().New("burst_pressure_bar");
				FreeFormParameterField ffpfField10 =
					fftpNames.GetFields().New("burst_pressure_psi");
				FreeFormParameterField ffpfField11 =
					fftpNames.GetFields().New("min_bend_radius_mm");
				FreeFormParameterField ffpfField12 =
					fftpNames.GetFields().New("min_bend_radius_inch");
				FreeFormParameterField ffpfField13 =
					fftpNames.GetFields().New("weight_g_m");
				FreeFormParameterField ffpfField14 =
					fftpNames.GetFields().New("weight_lbs_ft");
				//			----          DELETE ML 30.01.2013
				//		FreeFormParameterField ffpfField15 =
				//			fftpNames.GetFields().New("standard_ferrule");
				//		FreeFormParameterField ffpfField16 =
				//			fftpNames.GetFields().New("standard_insert");
				//			----          DELETE ML 30.01.2013
				//			----          INSERT ML 30.01.2013
				FreeFormParameterField ffpfField15 =
					fftpNames.GetFields().New("Std_Ferrule1");
				FreeFormParameterField ffpfField16 =
					fftpNames.GetFields().New("Std_Insert1");
				FreeFormParameterField ffpfField55 =
					fftpNames.GetFields().New("Std_Ferrule2");
				FreeFormParameterField ffpfField56 =
					fftpNames.GetFields().New("Std_Insert2");

				FreeFormParameterField ffpfField65 =
					fftpNames.GetFields().New("Alt_Ferrule1");
				FreeFormParameterField ffpfField66 =
					fftpNames.GetFields().New("Alt_Insert1");
				FreeFormParameterField ffpfField75 =
					fftpNames.GetFields().New("Alt_Ferrule2");
				FreeFormParameterField ffpfField76 =
					fftpNames.GetFields().New("Alt_Insert2");
				//			----          INSERT ML 30.01.2013

				FreeFormParameterField ffpfField17 =
					fftpNames.GetFields().New("standard_mf3000");

				//		  variabili per stampa Catalogue Extended	
				FreeFormParameterField ffpfField18 =
					fftpNames.GetFields().New("hose_inside_diameter_min_mm");
				FreeFormParameterField ffpfField19 =
					fftpNames.GetFields().New("hose_inside_diameter_min_inches");
				FreeFormParameterField ffpfField20 =
					fftpNames.GetFields().New("hose_inside_diameter_max_mm");
				FreeFormParameterField ffpfField21 =
					fftpNames.GetFields().New("hose_inside_diameter_max_inches");
				FreeFormParameterField ffpfField22 =
					fftpNames.GetFields().New("reinforcement_outside_diameter_min_mm");
				FreeFormParameterField ffpfField23 =
					fftpNames.GetFields().New(
						"reinforcement_outside_diameter_min_inches");
				FreeFormParameterField ffpfField24 =
					fftpNames.GetFields().New("reinforcement_outside_diameter_max_mm");
				FreeFormParameterField ffpfField25 =
					fftpNames.GetFields().New(
						"reinforcement_outside_diameter_max_inches");
				FreeFormParameterField ffpfField26 =
					fftpNames.GetFields().New("hose_outside_diameter_min_mm");
				FreeFormParameterField ffpfField27 =
					fftpNames.GetFields().New("hose_outside_diameter_min_inches");
				FreeFormParameterField ffpfField28 =
					fftpNames.GetFields().New("hose_outside_diameter_max_mm");
				FreeFormParameterField ffpfField29 =
					fftpNames.GetFields().New("hose_outside_diameter_max_inches");
				FreeFormParameterField ffpfField30 =
					fftpNames.GetFields().New("hose_max_wall_thickness_difference_mm");
				FreeFormParameterField ffpfField31 =
					fftpNames.GetFields().New(
						"hose_max_wall_thickness_difference_inches");
				//		  variabili per stampa Catalogue Extended	

				//		  variabili per stampa Marcature
				FreeFormParameterField ffpfField32 =
					fftpNames.GetFields().New("category");
				FreeFormParameterField ffpfField33 =
					fftpNames.GetFields().New("brand_pack");
				FreeFormParameterField ffpfField34 =
					fftpNames.GetFields().New("brand_customer");
				FreeFormParameterField ffpfField35 =
					fftpNames.GetFields().New("marking_technology");
				FreeFormParameterField ffpfField36 =
					fftpNames.GetFields().New("brand_color");
				FreeFormParameterField ffpfField37 =
					fftpNames.GetFields().New("brand_logo");
				FreeFormParameterField ffpfField38 =
					fftpNames.GetFields().New("brand_notes");
				FreeFormParameterField ffpfField39 =
					fftpNames.GetFields().New("brand_date_type");
				FreeFormParameterField ffpfField40 =
					fftpNames.GetFields().New("brand_customer");
				FreeFormParameterField ffpfField41 =
					fftpNames.GetFields().New("department");
				FreeFormParameterField ffpfField42 = fftpNames.GetFields().New("brand");
				//		  variabili per stampa Marcature

				// Ordinamento records
				A2iResultSet rs;
				try {
					Object fields = null;
					Object locale = null;
					
					if (paramType.equals("3")) {
						rs =
							catalogData.GetResultSet(
								search,
								rsd_prod_hoses_rec,
								ffpfField32.GetName(),
								true,
								0);

					} else {
						rs =
							catalogData.GetResultSet(
								search,
								rsd_prod_hoses_rec,
								ffpfField01.GetName(),
								true,
								0);

					}
					String[] Name = new String[rs.GetRecordCount()];
														String[] line = new String[rs.GetRecordCount()];
														String[] nominal_size = new String[rs.GetRecordCount()];
														String[] nominal_size_dash = new String[rs.GetRecordCount()];
														String[] nominal_size_inch = new String[rs.GetRecordCount()];
														String[] reinforcement_outside_diameter_mm = new String[rs.GetRecordCount()];
														String[] reinforcement_outside_diameter_inch = new String[rs.GetRecordCount()];
														String[] outside_diameter_mm = new String[rs.GetRecordCount()];
														String[] outside_diameter_inch = new String[rs.GetRecordCount()];
														String[] Max_working_pressure_bar = new String[rs.GetRecordCount()];
														String[] max_working_pressure_psi = new String[rs.GetRecordCount()];
														String[] burst_pressure_bar = new String[rs.GetRecordCount()];
														String[] burst_pressure_psi = new String[rs.GetRecordCount()];
														String[] min_bend_radius_mm = new String[rs.GetRecordCount()];
														String[] min_bend_radius_inch = new String[rs.GetRecordCount()];
														String[] weight_g_m = new String[rs.GetRecordCount()];
														String[] weight_lbs_ft = new String[rs.GetRecordCount()];
														//		----          DELETE ML 30.01.2013
														//		String[] standard_ferrule = new String[rs.GetRecordCount()];
														//		String[] standard_insert = new String[rs.GetRecordCount()];
														//		----          DELETE ML 30.01.2013
														//		----          INSERT ML 30.01.2013
														String[] Std_Ferrule1 = new String[rs.GetRecordCount()];
														String[] Std_Insert1 = new String[rs.GetRecordCount()];
														String[] Std_Ferrule2 = new String[rs.GetRecordCount()];
														String[] Std_Insert2 = new String[rs.GetRecordCount()];
														String[] Alt_Ferrule1 = new String[rs.GetRecordCount()];
														String[] Alt_Insert1 = new String[rs.GetRecordCount()];
														String[] Alt_Ferrule2 = new String[rs.GetRecordCount()];
														String[] Alt_Insert2 = new String[rs.GetRecordCount()];
														//		----          INSERT ML 30.01.2013
														String[] standard_mf3000 = new String[rs.GetRecordCount()];

														//		  variabili per stampa Catalogue Extended	
														String[] hose_inside_diameter_min_mm = new String[rs.GetRecordCount()];
														String[] hose_inside_diameter_min_inches = new String[rs.GetRecordCount()];
														String[] hose_inside_diameter_max_mm = new String[rs.GetRecordCount()];
														String[] hose_inside_diameter_max_inches = new String[rs.GetRecordCount()];
														String[] reinforcement_outside_diameter_min_mm = new String[rs.GetRecordCount()];
														String[] reinforcement_outside_diameter_min_inches = new String[rs.GetRecordCount()];
														String[] reinforcement_outside_diameter_max_mm = new String[rs.GetRecordCount()];
														String[] reinforcement_outside_diameter_max_inches = new String[rs.GetRecordCount()];
														String[] hose_outside_diameter_min_mm = new String[rs.GetRecordCount()];
														String[] hose_outside_diameter_min_inches = new String[rs.GetRecordCount()];
														String[] hose_outside_diameter_max_mm = new String[rs.GetRecordCount()];
														String[] hose_outside_diameter_max_inches = new String[rs.GetRecordCount()];
														String[] hose_max_wall_thickness_difference_mm = new String[rs.GetRecordCount()];
														String[] hose_max_wall_thickness_difference_inches = new String[rs.GetRecordCount()];
														//		  variabili per stampa Catalogue Extended	

														//		  variabili per stampa Marcature
														String[] category = new String[rs.GetRecordCount()];
														String[] brand_pack = new String[rs.GetRecordCount()];
														String[] brand_customer = new String[rs.GetRecordCount()];
														String[] marking_technology = new String[rs.GetRecordCount()];
														String[] brand_color = new String[rs.GetRecordCount()];
														String[] brand_logo = new String[rs.GetRecordCount()];
														String[] brand_notes = new String[rs.GetRecordCount()];
														String[] brand_date_type = new String[rs.GetRecordCount()];
														String[] brand_customer_2 = new String[rs.GetRecordCount()];
														String[] department = new String[rs.GetRecordCount()];
														String[] brand = new String[rs.GetRecordCount()];
														//		  variabili per stampa Marcature

														String[] category_2 = new String[rs.GetRecordCount()];
														String[] Printable = new String[rs.GetRecordCount()];
														String[] Public = new String[rs.GetRecordCount()];
														String[] Phase = new String[rs.GetRecordCount()];
					//			Vector recs = new Vector();
					
					for (int i = 0; i < rs.GetRecordCount(); i++) {
						// Field01  PART.REF.
						line[i] = rs.GetValueAt(i, "line").GetStringValue();

						int idx1 =
							rs.GetValueAt(i, "nominal_size").GetStringValue().indexOf(
								",",
								0);
						int idx2 =
							rs.GetValueAt(i, "nominal_size").GetStringValue().indexOf(
								",",
								idx1 + 1);
						int idx3 =
							rs.GetValueAt(i, "nominal_size").GetStringValue().indexOf(
								",",
								idx2 + 1);
						int idx4 =
							rs.GetValueAt(i, "nominal_size").GetStringValue().length();

						// Field02 HOSE SIZE DN
						fld02String =
							rs
								.GetValueAt(i, "nominal_size")
								.GetStringValue()
								.substring(
								0,
								idx1);
						fld02Float = Integer.parseInt(fld02String);
						nominal_size[i] = fld02String;
						// Field03 HOSE SIZE dash
						fld03String =
							rs
								.GetValueAt(i, "nominal_size")
								.GetStringValue()
								.substring(
								idx1 + 1,
								idx2);
						nominal_size_dash[i] = fld03String;
						// Field04 HOSE SIZE mm
						//fld04String = rs.GetValueAt(i, "nominal_size").GetStringValue().substring(idx2 + 1, idx3);
						// Field04 HOSE SIZE inch
						fld04String =
							rs
								.GetValueAt(i, "nominal_size")
								.GetStringValue()
								.substring(
								idx3 + 1,
								idx4);
						nominal_size_inch[i] = fld04String;
						// Field05
						if (rs
							.GetValueAt(i, "reinforcement_outside_diameter_mm")
							.IsNull()
							!= true) {
							Measurement fldMeas05 =
								rs
									.GetValueAt(i, "reinforcement_outside_diameter_mm")
									.GetMeasurement();
							fld05String =
								measurementManager.GetString(
									fldMeas05.GetValue(),
									0,
									0,
									rs
										.GetFieldAt("reinforcement_outside_diameter_mm")
										.GetDecimalPlaces(),
									false,
									-1);
							reinforcement_outside_diameter_mm[i] = fld05String.replaceAll(",", ".");
							fld05Float = Float.valueOf(fld05String).floatValue();
						} else {
							fld05Float = 0;
							reinforcement_outside_diameter_mm[i] = "";
						}

						// Field06
						if (rs
							.GetValueAt(i, "reinforcement_outside_diameter_inch")
							.IsNull()
							!= true) {
							Measurement fldMeas06 =
								rs
									.GetValueAt(
										i,
										"reinforcement_outside_diameter_inch")
									.GetMeasurement();
							fld06String =
								measurementManager.GetString(
									fldMeas06.GetValue(),
									0,
									0,
									rs
										.GetFieldAt("reinforcement_outside_diameter_inch")
										.GetDecimalPlaces(),
									false,
									-1);
							reinforcement_outside_diameter_inch[i] = fld06String.replaceAll(",", ".");
							fld06Float = Float.valueOf(fld06String).floatValue();
						} else {
							fld06Float = 0;
							reinforcement_outside_diameter_inch[i] = "";

						}
						// Field07
						if (rs.GetValueAt(i, "outside_diameter_mm").IsNull() != true) {
							Measurement fldMeas07 =
								rs
									.GetValueAt(i, "outside_diameter_mm")
									.GetMeasurement();
							fld07String =
								measurementManager.GetString(
									fldMeas07.GetValue(),
									0,
									0,
									rs
										.GetFieldAt("outside_diameter_mm")
										.GetDecimalPlaces(),
									false,
									-1);
							outside_diameter_mm[i] = fld07String.replaceAll(",", ".");
							fld07Float = Float.valueOf(fld07String).floatValue();
						} else {
							fld07Float = 0;
							outside_diameter_mm[i] = "";

						}
						// Field08
						if (rs.GetValueAt(i, "outside_diameter_inch").IsNull()
							!= true) {
							Measurement fldMeas08 =
								rs
									.GetValueAt(i, "outside_diameter_inch")
									.GetMeasurement();
							fld08String =
								measurementManager.GetString(
									fldMeas08.GetValue(),
									0,
									0,
									rs
										.GetFieldAt("outside_diameter_inch")
										.GetDecimalPlaces(),
									false,
									-1);
								outside_diameter_inch[i] = fld08String.replaceAll(",", ".");
							fld08Float = Float.valueOf(fld08String).floatValue();
						} else {
							fld08Float = 0;
							outside_diameter_inch[i] = "";

						}
						// Field09
						if (rs.GetValueAt(i, "Max_working_pressure_bar").IsNull()
							!= true) {
							Measurement fldMeas09 =
								rs
									.GetValueAt(i, "Max_working_pressure_bar")
									.GetMeasurement();
							fld09Float =
								Float
									.valueOf(
										measurementManager.GetString(
											fldMeas09.GetValue(),
											0,
											0,
											0,
											false,
											-1))
									.floatValue();
								Max_working_pressure_bar[i] = Float.toString(fld09Float);
						} else {
							fld09Float = 0;
							Max_working_pressure_bar[i] = "";

						}
						// Field10
						if (rs.GetValueAt(i, "max_working_pressure_psi").IsNull()
							!= true) {
							Measurement fldMeas10 =
								rs
									.GetValueAt(i, "max_working_pressure_psi")
									.GetMeasurement();
							fld10Float =
								Float
									.valueOf(
										measurementManager.GetString(
											fldMeas10.GetValue(),
											0,
											0,
											0,
											false,
											-1))
									.floatValue();
								max_working_pressure_psi[i] = Float.toString(fld10Float);
						} else {
							fld10Float = 0;
							max_working_pressure_psi[i] = "";

						}
						// Field11
						if (rs.GetValueAt(i, "burst_pressure_bar").IsNull() != true) {
							Measurement fldMeas11 =
								rs.GetValueAt(i, "burst_pressure_bar").GetMeasurement();
							fld11Float =
								Float
									.valueOf(
										measurementManager.GetString(
											fldMeas11.GetValue(),
											0,
											0,
											0,
											false,
											-1))
									.floatValue();
							burst_pressure_bar[i] = Float.toString(fld11Float);
						} else {
							fld11Float = 0;
							burst_pressure_bar[i] = "";

						}
						// Field12
						if (rs.GetValueAt(i, "burst_pressure_psi").IsNull() != true) {
							Measurement fldMeas12 =
								rs.GetValueAt(i, "burst_pressure_psi").GetMeasurement();
							fld12Float =
								Float
									.valueOf(
										measurementManager.GetString(
											fldMeas12.GetValue(),
											0,
											0,
											0,
											false,
											-1))
									.floatValue();
							burst_pressure_psi[i] = Float.toString(fld12Float);
						} else {
							fld12Float = 0;
							burst_pressure_psi[i] = "";

						}
						// Field13
						if (rs.GetValueAt(i, "min_bend_radius_mm").IsNull() != true) {
							Measurement fldMeas13 =
								rs.GetValueAt(i, "min_bend_radius_mm").GetMeasurement();
							fld13Float =
								Float
									.valueOf(
										measurementManager.GetString(
											fldMeas13.GetValue(),
											0,
											0,
											0,
											false,
											-1))
									.floatValue();
							min_bend_radius_mm[i] = Float.toString(fld13Float);
						} else {
							fld13Float = 0;
							min_bend_radius_mm[i] = "";

						}
						// Field14
						if (rs.GetValueAt(i, "min_bend_radius_inch").IsNull()
							!= true) {
							Measurement fldMeas14 =
								rs
									.GetValueAt(i, "min_bend_radius_inch")
									.GetMeasurement();
							fld14String =
								measurementManager.GetString(
									fldMeas14.GetValue(),
									0,
									0,
									rs
										.GetFieldAt("min_bend_radius_inch")
										.GetDecimalPlaces(),
									false,
									-1);
							min_bend_radius_inch[i] = fld14String.replaceAll(",", ".");
							fld14Float = Float.valueOf(fld14String).floatValue();

							//				String fldMeas12String =	measurementManager.GetString(fldMeas12.GetValue(), fldMeas12.GetUnitID(),
							//																 rs.GetFieldAt("min_bend_radius_inch").GetDimensionID(),
							//																 rs.GetFieldAt("min_bend_radius_inch").GetDecimalPlaces(),
							//																 false, -1);
						} else {
							fld14Float = 0;
							min_bend_radius_inch[i] = "";

						}
						// Field15
						if (rs.GetValueAt(i, "weight_g_m").IsNull() != true) {
							fld15Float = rs.GetValueAt(i, "weight_g_m").GetFloatValue();
							weight_g_m[i] = Float.toString(fld15Float);

						} else {
							fld15Float = 0;
							weight_g_m[i] = "";

						}
						// Field16
						if (rs.GetValueAt(i, "weight_lbs_ft").IsNull() != true) {
							fld16Float =
								rs.GetValueAt(i, "weight_lbs_ft").GetFloatValue();
							weight_lbs_ft[i] = Float.toString(fld16Float);
						} else {
							fld16Float = 0;
							weight_lbs_ft[i] = "";

						}
						//----          INSERT ML 30.01.2013
						if (paramStandard.equals("01")) { // Standard 1
							head1 = "Std1";
							// Field17
							if (rs.GetValueAt(i, "Std_Ferrule1").IsNull() != true) {
								col1 =
									"+"
										+ rs
											.GetValueAt(i, "Std_Ferrule1")
											.GetStringValue();
							} else {
								col1 = "";
							}
							// Field18
							if (rs.GetValueAt(i, "Std_Insert1").IsNull() != true) {
								col2 = rs.GetValueAt(i, "Std_Insert1").GetStringValue();
								int index = col2.indexOf(",");
								col2 = col2.substring(0, index);
							} else {
								col2 = "";
							}
							fld17String = col2 + col1;
							if (fld17String.length() > 0
								&& fld17String.charAt(0) == '+') {
									Std_Ferrule1[i] = fld17String.substring(1);
							}
						} else if (paramStandard.equals("02")) { // Standard 2
							head1 = "Std2";
							// Field17
							if (rs.GetValueAt(i, "Std_Ferrule2").IsNull() != true) {
								col1 =
									"+"
										+ rs
											.GetValueAt(i, "Std_Ferrule2")
											.GetStringValue();
							} else {
								col1 = "";
							}
							// Field18
							if (rs.GetValueAt(i, "Std_Insert2").IsNull() != true) {
								col2 = rs.GetValueAt(i, "Std_Insert2").GetStringValue();
								int index = col2.indexOf(",");
								col2 = col2.substring(0, index);
							} else {
								col2 = "";
							}
							fld17String = col2 + col1;
							if (fld17String.length() > 0
								&& fld17String.charAt(0) == '+') {
									Std_Ferrule1[i] = fld17String.substring(1);
							}
						} else if (paramStandard.equals("03")) { // Alternative 1
							head1 = "Alt1";
							// Field17
							if (rs.GetValueAt(i, "Alt_Ferrule1").IsNull() != true) {
								col1 =
									"+"
										+ rs
											.GetValueAt(i, "Alt_Ferrule1")
											.GetStringValue();
							} else {
								col1 = "";
							}
							// Field18
							if (rs.GetValueAt(i, "Alt_Insert1").IsNull() != true) {
								col2 = rs.GetValueAt(i, "Alt_Insert1").GetStringValue();
								int index = col2.indexOf(",");
								col2 = col2.substring(0, index);
							} else {
								col2 = "";
							}
							fld17String = col2 + col1;
							if (fld17String.length() > 0
								&& fld17String.charAt(0) == '+') {
								fld17String = fld17String.substring(1);
							}
						} else if (paramStandard.equals("04")) { // Alternative 2
							head1 = "Alt2";
							// Field17
							if (rs.GetValueAt(i, "Alt_Ferrule2").IsNull() != true) {
								col1 =
									"+"
										+ rs
											.GetValueAt(i, "Alt_Ferrule2")
											.GetStringValue();
							} else {
								col1 = "";
							}
							// Field18
							if (rs.GetValueAt(i, "Alt_Insert2").IsNull() != true) {
								col2 = rs.GetValueAt(i, "Alt_Insert2").GetStringValue();
								int index = col2.indexOf(",");
								col2 = col2.substring(0, index);
							} else {
								col2 = "";
							}
							fld17String = col2 + col1;
							if (fld17String.length() > 0
								&& fld17String.charAt(0) == '+') {
								fld17String = fld17String.substring(1);
							}
						} else {
							col1 = paramStandard;
							col2 = paramStandard;
							fld17String = col2 + col1;
							if (fld17String.length() > 0
								&& fld17String.charAt(0) == '+') {
									Std_Ferrule1[i] = fld17String.substring(1);
							}
						}

						if (paramAlternative.equals("01")) { // Standard 1
							head2 = "Std1";
							// Field17
							if (rs.GetValueAt(i, "Std_Ferrule1").IsNull() != true) {
								col3 =
									"+"
										+ rs
											.GetValueAt(i, "Std_Ferrule1")
											.GetStringValue();
							} else {
								col3 = "";
							}
							// Field18
							if (rs.GetValueAt(i, "Std_Insert1").IsNull() != true) {
								col4 = rs.GetValueAt(i, "Std_Insert1").GetStringValue();
								int index = col4.indexOf(",");
								col4 = col4.substring(0, index);
							} else {
								col4 = "";
							}
							fld18String = col4 + col3;
							if (fld18String.length() > 0
								&& fld18String.charAt(0) == '+') {
									Std_Insert1[i] = fld18String.substring(1);
							}
						} else if (paramAlternative.equals("02")) { // Standard 2
							head2 = "Std2";
							// Field17
							if (rs.GetValueAt(i, "Std_Ferrule2").IsNull() != true) {
								col3 =
									"+"
										+ rs
											.GetValueAt(i, "Std_Ferrule2")
											.GetStringValue();
							} else {
								col3 = "";
							}
							// Field18
							if (rs.GetValueAt(i, "Std_Insert2").IsNull() != true) {
								col4 = rs.GetValueAt(i, "Std_Insert2").GetStringValue();
								int index = col4.indexOf(",");
								col4 = col4.substring(0, index);
							} else {
								col4 = "";
							}
							fld18String = col4 + col3;
							if (fld18String.length() > 0
								&& fld18String.charAt(0) == '+') {
									Std_Insert1[i] = fld18String.substring(1);
							}
						} else if (paramAlternative.equals("03")) { // Alternative 1
							head2 = "Alt1";
							// Field17
							if (rs.GetValueAt(i, "Alt_Ferrule1").IsNull() != true) {
								col3 =
									"+"
										+ rs
											.GetValueAt(i, "Alt_Ferrule1")
											.GetStringValue();
							} else {
								col3 = "";
							}
							// Field18
							if (rs.GetValueAt(i, "Alt_Insert1").IsNull() != true) {
								col4 = rs.GetValueAt(i, "Alt_Insert1").GetStringValue();
								int index = col4.indexOf(",");
								col4 = col4.substring(0, index);
							} else {
								col4 = "";
							}
							fld18String = col4 + col3;
							if (fld18String.length() > 0
								&& fld18String.charAt(0) == '+') {
									Std_Insert1[i] = fld18String.substring(1);
							}
						} else if (paramAlternative.equals("04")) { // Alternative 2
							head2 = "Alt2";
							// Field17
							if (rs.GetValueAt(i, "Alt_Ferrule2").IsNull() != true) {
								col3 =
									"+"
										+ rs
											.GetValueAt(i, "Alt_Ferrule2")
											.GetStringValue();
							} else {
								col3 = "";
							}
							// Field18
							if (rs.GetValueAt(i, "Alt_Insert2").IsNull() != true) {
								col4 = rs.GetValueAt(i, "Alt_Insert2").GetStringValue();
								int index = col4.indexOf(",");
								col4 = col4.substring(0, index);
							} else {
								col4 = "";
							}
							fld18String = col4 + col3;
							if (fld18String.length() > 0
								&& fld18String.charAt(0) == '+') {
									Std_Insert1[i] = fld18String.substring(1);
							}
						} else {
							col3 = paramAlternative;
							col4 = paramAlternative;
							fld18String = col4 + col3;
							if (fld18String.length() > 0
								&& fld18String.charAt(0) == '+') {
									Std_Insert1[i] = fld18String.substring(1);
							}
						}

						//----          INSERT ML 30.01.2013

						//----          DELETE ML 30.01.2013
						//				// Field17
						//				if (rs.GetValueAt(i, "standard_ferrule").IsNull() != true) {
						//					fld17String =
						//						rs.GetValueAt(i, "standard_ferrule").GetStringValue();
						//
						//				} else {
						//					fld17String = null;
						//
						//				}
						//				// Field18
						//				if (rs.GetValueAt(i, "standard_insert").IsNull() != true) {
						//					fld18String =
						//						rs.GetValueAt(i, "standard_insert").GetStringValue();
						//					int index = fld18String.indexOf(",");
						//					fld18String = fld18String.substring(0, index);
						//				} else {
						//					fld18String = null;
						//
						//				}
						//----          DELETE ML 30.01.2013

						// Field19
						if (rs.GetValueAt(i, "standard_mf3000").IsNull() != true) {
							standard_mf3000[i] =
								rs.GetValueAt(i, "standard_mf3000").GetStringValue();
						} else {
							standard_mf3000[i] = null;

						}
						//				variabili per stampa Catalogue Extended	
						// Field20
						if (rs.GetValueAt(i, "hose_inside_diameter_min_mm").IsNull()
							!= true) {
							Measurement fldMeas20 =
								rs
									.GetValueAt(i, "hose_inside_diameter_min_mm")
									.GetMeasurement();
							String fld20String =
								measurementManager.GetString(
									fldMeas20.GetValue(),
									0,
									0,
									rs
										.GetFieldAt("hose_inside_diameter_min_mm")
										.GetDecimalPlaces(),
									false,
									-1);
							hose_inside_diameter_min_mm[i] = fld20String.replaceAll(",", ".");
							fld20Float = Float.valueOf(fld20String).floatValue();
						} else {
							hose_inside_diameter_min_mm[i] = "0";

						}
						// Field21
						if (rs
							.GetValueAt(i, "hose_inside_diameter_min_inches")
							.IsNull()
							!= true) {
							Measurement fldMeas21 =
								rs
									.GetValueAt(i, "hose_inside_diameter_min_inches")
									.GetMeasurement();
							String fld21String =
								measurementManager.GetString(
									fldMeas21.GetValue(),
									0,
									0,
									rs
										.GetFieldAt("hose_inside_diameter_min_inches")
										.GetDecimalPlaces(),
									false,
									-1);
							hose_inside_diameter_min_inches[i] = fld21String.replaceAll(",", ".");
							fld21Float = Float.valueOf(fld21String).floatValue();
						} else {
							hose_inside_diameter_min_inches[i] = "0";

						}
						// Field22
						if (rs.GetValueAt(i, "hose_inside_diameter_max_mm").IsNull()
							!= true) {
							Measurement fldMeas22 =
								rs
									.GetValueAt(i, "hose_inside_diameter_max_mm")
									.GetMeasurement();
							String fld22String =
								measurementManager.GetString(
									fldMeas22.GetValue(),
									0,
									0,
									rs
										.GetFieldAt("hose_inside_diameter_max_mm")
										.GetDecimalPlaces(),
									false,
									-1);
							hose_inside_diameter_max_mm[i] = fld22String.replaceAll(",", ".");
							fld22Float = Float.valueOf(fld22String).floatValue();
						} else {
							hose_inside_diameter_max_mm[i] = "0";

						}
						// Field23
						if (rs
							.GetValueAt(i, "hose_inside_diameter_max_inches")
							.IsNull()
							!= true) {
							Measurement fldMeas23 =
								rs
									.GetValueAt(i, "hose_inside_diameter_max_inches")
									.GetMeasurement();
							String fld23String =
								measurementManager.GetString(
									fldMeas23.GetValue(),
									0,
									0,
									rs
										.GetFieldAt("hose_inside_diameter_max_inches")
										.GetDecimalPlaces(),
									false,
									-1);
							hose_inside_diameter_max_inches[i] = fld23String.replaceAll(",", ".");
							fld23Float = Float.valueOf(fld23String).floatValue();
						} else {
							hose_inside_diameter_max_inches[i] = "0";

						}
						// Field24
						if (rs
							.GetValueAt(i, "reinforcement_outside_diameter_min_mm")
							.IsNull()
							!= true) {
							Measurement fldMeas24 =
								rs
									.GetValueAt(
										i,
										"reinforcement_outside_diameter_min_mm")
									.GetMeasurement();
							String fld24String =
								measurementManager.GetString(
									fldMeas24.GetValue(),
									0,
									0,
									rs
										.GetFieldAt("reinforcement_outside_diameter_min_mm")
										.GetDecimalPlaces(),
									false,
									-1);
							reinforcement_outside_diameter_min_mm[i] = fld24String.replaceAll(",", ".");
							fld24Float = Float.valueOf(fld24String).floatValue();
						} else {
							reinforcement_outside_diameter_min_mm[i] = "0";

						}
						// Field25
						if (rs
							.GetValueAt(i, "reinforcement_outside_diameter_min_inches")
							.IsNull()
							!= true) {
							Measurement fldMeas25 =
								rs
									.GetValueAt(
										i,
										"reinforcement_outside_diameter_min_inches")
									.GetMeasurement();
							String fld25String =
								measurementManager.GetString(
									fldMeas25.GetValue(),
									0,
									0,
									rs
										.GetFieldAt("reinforcement_outside_diameter_min_inches")
										.GetDecimalPlaces(),
									false,
									-1);
								reinforcement_outside_diameter_min_inches[i] = fld25String.replaceAll(",", ".");
							fld25Float = Float.valueOf(fld25String).floatValue();
						} else {
							reinforcement_outside_diameter_min_inches[i] = "0";

						}
						// Field26
						if (rs
							.GetValueAt(i, "reinforcement_outside_diameter_max_mm")
							.IsNull()
							!= true) {
							Measurement fldMeas26 =
								rs
									.GetValueAt(
										i,
										"reinforcement_outside_diameter_max_mm")
									.GetMeasurement();
							String fld26String =
								measurementManager.GetString(
									fldMeas26.GetValue(),
									0,
									0,
									rs
										.GetFieldAt("reinforcement_outside_diameter_max_mm")
										.GetDecimalPlaces(),
									false,
									-1);
								reinforcement_outside_diameter_max_mm[i] = fld26String.replaceAll(",", ".");
							fld26Float = Float.valueOf(fld26String).floatValue();
						} else {
							reinforcement_outside_diameter_max_mm[i] = "0";

						}
						// Field27
						if (rs
							.GetValueAt(i, "reinforcement_outside_diameter_max_inches")
							.IsNull()
							!= true) {
							Measurement fldMeas27 =
								rs
									.GetValueAt(
										i,
										"reinforcement_outside_diameter_max_inches")
									.GetMeasurement();
							String fld27String =
								measurementManager.GetString(
									fldMeas27.GetValue(),
									0,
									0,
									rs
										.GetFieldAt("reinforcement_outside_diameter_max_inches")
										.GetDecimalPlaces(),
									false,
									-1);
							reinforcement_outside_diameter_max_inches[i] = fld27String.replaceAll(",", ".");
							fld27Float = Float.valueOf(fld27String).floatValue();
						} else {
							reinforcement_outside_diameter_max_inches[i] = "0";

						}
						// Field28
						if (rs.GetValueAt(i, "hose_outside_diameter_min_mm").IsNull()
							!= true) {
							Measurement fldMeas28 =
								rs
									.GetValueAt(i, "hose_outside_diameter_min_mm")
									.GetMeasurement();
							String fld28String =
								measurementManager.GetString(
									fldMeas28.GetValue(),
									0,
									0,
									rs
										.GetFieldAt("hose_outside_diameter_min_mm")
										.GetDecimalPlaces(),
									false,
									-1);
								hose_outside_diameter_min_mm[i] = fld28String.replaceAll(",", ".");
							fld28Float = Float.valueOf(fld28String).floatValue();
						} else {
							hose_outside_diameter_min_mm[i] = "0";

						}
						// Field29
						if (rs
							.GetValueAt(i, "hose_outside_diameter_min_inches")
							.IsNull()
							!= true) {
							Measurement fldMeas29 =
								rs
									.GetValueAt(i, "hose_outside_diameter_min_inches")
									.GetMeasurement();
							String fld29String =
								measurementManager.GetString(
									fldMeas29.GetValue(),
									0,
									0,
									rs
										.GetFieldAt("hose_outside_diameter_min_inches")
										.GetDecimalPlaces(),
									false,
									-1);
								hose_outside_diameter_min_inches[i] = fld29String.replaceAll(",", ".");
							fld29Float = Float.valueOf(fld29String).floatValue();
						} else {
							hose_outside_diameter_min_inches[i] = "0";

						}
						// Field30
						if (rs.GetValueAt(i, "hose_outside_diameter_max_mm").IsNull()
							!= true) {
							Measurement fldMeas30 =
								rs
									.GetValueAt(i, "hose_outside_diameter_max_mm")
									.GetMeasurement();
							String fld30String =
								measurementManager.GetString(
									fldMeas30.GetValue(),
									0,
									0,
									rs
										.GetFieldAt("hose_outside_diameter_max_mm")
										.GetDecimalPlaces(),
									false,
									-1);
								hose_outside_diameter_max_mm[i] = fld30String.replaceAll(",", ".");
							fld30Float = Float.valueOf(fld30String).floatValue();
						} else {
							hose_outside_diameter_max_mm[i] = "0";

						}
						// Field31
						if (rs
							.GetValueAt(i, "hose_outside_diameter_max_inches")
							.IsNull()
							!= true) {
							Measurement fldMeas31 =
								rs
									.GetValueAt(i, "hose_outside_diameter_max_inches")
									.GetMeasurement();
							String fld31String =
								measurementManager.GetString(
									fldMeas31.GetValue(),
									0,
									0,
									rs
										.GetFieldAt("hose_outside_diameter_max_inches")
										.GetDecimalPlaces(),
									false,
									-1);
							hose_outside_diameter_max_inches[i] = fld31String.replaceAll(",", ".");
							fld31Float = Float.valueOf(fld31String).floatValue();
						} else {
							hose_outside_diameter_max_inches[i] = "0";

						}
						// Field32
						if (rs
							.GetValueAt(i, "hose_max_wall_thickness_difference_mm")
							.IsNull()
							!= true) {
							Measurement fldMeas32 =
								rs
									.GetValueAt(
										i,
										"hose_max_wall_thickness_difference_mm")
									.GetMeasurement();
							String fld32String =
								measurementManager.GetString(
									fldMeas32.GetValue(),
									0,
									0,
									rs
										.GetFieldAt("hose_max_wall_thickness_difference_mm")
										.GetDecimalPlaces(),
									false,
									-1);
							hose_max_wall_thickness_difference_mm[i] = fld32String.replaceAll(",", ".");
							fld32Float = Float.valueOf(fld32String).floatValue();
						} else {
							hose_max_wall_thickness_difference_mm[i] = "0";

						}
						// Field33
						if (rs
							.GetValueAt(i, "hose_max_wall_thickness_difference_inches")
							.IsNull()
							!= true) {
							Measurement fldMeas33 =
								rs
									.GetValueAt(
										i,
										"hose_max_wall_thickness_difference_inches")
									.GetMeasurement();
							String fld33String =
								measurementManager.GetString(
									fldMeas33.GetValue(),
									0,
									0,
									rs
										.GetFieldAt("hose_max_wall_thickness_difference_inches")
										.GetDecimalPlaces(),
									false,
									-1);
								hose_max_wall_thickness_difference_inches[i] = fld33String.replaceAll(",", ".");
							fld33Float = Float.valueOf(fld33String).floatValue();
						} else {
							hose_max_wall_thickness_difference_inches[i] = "0";

						}

						//			variabili per stampa Catalogue Extended	

						//			variabili per stampa Catalogue Marcature	
						if (paramType.equals("3")) {

							// Field34
							if (rs.GetValueAt(i, "category").IsNull() != true) {
								category[i] =
									rs.GetValueAt(i, "category").GetStringValue();
							} else {
								category[i] = "";

							}
							// Field35
							if (rs.GetValueAt(i, "brand_pack").IsNull() != true) {
								brand_pack[i] =
									rs.GetValueAt(i, "brand_pack").GetStringValue();
							} else {
								brand_pack[i] = "";

							}
							// Field36
							if (rs.GetValueAt(i, "brand_customer").IsNull() != true) {
								brand_customer[i] =
									rs.GetValueAt(i, "brand_customer").GetStringValue();
							} else {
								brand_customer[i] = "";

							}
							// Field37
							if (rs.GetValueAt(i, "marking_technology").IsNull()
								!= true) {
									marking_technology[i] =
									rs
										.GetValueAt(i, "marking_technology")
										.GetStringValue();
							} else {
								marking_technology[i] = "";

							}
							// Field38
							if (rs.GetValueAt(i, "brand_color").IsNull() != true) {
								brand_color[i] =
									rs.GetValueAt(i, "brand_color").GetStringValue();
							} else {
								brand_color[i] = "";

							}
							// Field39
							if (paramType.equals("3")
								&& !rs.GetValueAt(i, "brand_logo").IsNull()) {
								CatalogCache catalogCache = new CatalogCache();
								catalogCache.Init(
									CACHE_SERVER,
									CACHE_PORT,
									CACHE_USER,
									CACHE_PASSWORD,
									CACHE_DIRECTORY,
									"English [US]");
								Object id = rs.GetValueAt(i, "brand_logo").GetData();
								int idx = id.hashCode();
								brand_logo [i] =
									CACHE_DIRECTORY
										+ "\\"
										+ catalogCache.GetImagePath(
											"Images",
											"Original",
											idx);
								catalogCache.Shutdown();
							} else {
								brand_logo[i] = "";

							}
							// Field40
							if (rs.GetValueAt(i, "brand_notes").IsNull() != true) {
								brand_notes[i] =
									rs.GetValueAt(i, "brand_notes").GetStringValue();
							} else {
								brand_notes[i] = "";

							}
							// Field41
							if (rs.GetValueAt(i, "brand_date_type").IsNull() != true) {
								brand_date_type[i] =
									rs
										.GetValueAt(i, "brand_date_type")
										.GetStringValue();
							} else {
								brand_date_type[i] = null;

							}
							// Field42
							if (rs.GetValueAt(i, "department").IsNull() != true) {
								department[i] =
									rs.GetValueAt(i, "department").GetStringValue();
							} else {
								department[i] = "";

							}
							// Field43
							if (rs.GetValueAt(i, "brand").IsNull() != true) {
								brand[i] =
									rs.GetValueAt(i, "brand").GetStringValue();
							} else {
								brand[i] = "";

							}
						}

//						if (fld17String.length() > 12
//							&& fld17String.indexOf("+") != -1) {
//							fld17String = fld17String.replaceAll("\\+", "+ \n");
//						}
//						if (fld18String.length() > 12
//							&& fld18String.indexOf("+") != -1) {
//							fld18String = fld18String.replaceAll("\\+", "+ \n");
//						}
						 

					}
							 Name = convertXML(Name);
							 line = convertXML(line);
							 nominal_size_dash = convertXML(nominal_size_dash);
							 nominal_size_inch = convertXML(nominal_size_inch);
							 reinforcement_outside_diameter_mm = convertXML(reinforcement_outside_diameter_mm);
							 reinforcement_outside_diameter_inch = convertXML(reinforcement_outside_diameter_inch);
							 outside_diameter_mm = convertXML(outside_diameter_mm);
							 outside_diameter_inch = convertXML(outside_diameter_inch);
							 Max_working_pressure_bar = convertXML(Max_working_pressure_bar);
							 max_working_pressure_psi = convertXML(max_working_pressure_psi);
							 burst_pressure_bar = convertXML(burst_pressure_bar);
							 burst_pressure_psi = convertXML(burst_pressure_psi);
							 min_bend_radius_mm = convertXML(min_bend_radius_mm);
							 min_bend_radius_inch = convertXML(min_bend_radius_inch);
							 weight_g_m = convertXML(weight_g_m);
							 weight_lbs_ft = convertXML(weight_lbs_ft);
							 Std_Ferrule1 = convertXML(Std_Ferrule1);
							 Std_Insert1 = convertXML(Std_Insert1);
							 standard_mf3000 = convertXML(standard_mf3000);
							 hose_inside_diameter_min_mm = convertXML(hose_inside_diameter_min_mm);
							 hose_inside_diameter_min_inches = convertXML(hose_inside_diameter_min_inches);
							 hose_inside_diameter_max_mm = convertXML(hose_inside_diameter_max_mm);
							 hose_inside_diameter_max_inches = convertXML(hose_inside_diameter_max_inches);
							 reinforcement_outside_diameter_min_mm = convertXML(reinforcement_outside_diameter_min_mm);
							 reinforcement_outside_diameter_min_inches = convertXML(reinforcement_outside_diameter_min_inches);
							 reinforcement_outside_diameter_max_mm = convertXML(reinforcement_outside_diameter_max_mm);
							 reinforcement_outside_diameter_max_inches = convertXML(reinforcement_outside_diameter_max_inches);
							 hose_outside_diameter_min_mm = convertXML(hose_outside_diameter_min_mm);
							 hose_outside_diameter_min_inches = convertXML(hose_outside_diameter_min_inches);
							 hose_outside_diameter_max_mm = convertXML(hose_outside_diameter_max_mm);
							 hose_outside_diameter_max_inches = convertXML(hose_outside_diameter_max_inches);
							 hose_max_wall_thickness_difference_mm = convertXML(hose_max_wall_thickness_difference_mm);
							 hose_max_wall_thickness_difference_inches = convertXML(hose_max_wall_thickness_difference_inches);
							 category = convertXML(category);
							 brand_pack = convertXML(brand_pack);
							 brand_customer = convertXML(brand_customer);
							 marking_technology = convertXML(marking_technology);
							 brand_color = convertXML(brand_color);
							 brand_logo = convertXML(brand_logo);
							 brand_notes = convertXML(brand_notes);
							 brand_date_type = convertXML(brand_date_type);
							 department = convertXML(department);
							 brand = convertXML(brand);
							 Printable = convertXML(Printable);
							 Public = convertXML(Public);
							 Phase = convertXML(Phase);
							 
					for(int i = 0; i < rs.GetRecordCount(); i++){
						items = items + "<items>" +
										"<Name>" + Name[i] + "</Name>" +
										"<line>" + line[i] + "</line>" +
										"<nominal_size_dash>" + nominal_size_dash[i] + "</nominal_size_dash>" +
										"<nominal_size_inch>" + nominal_size_inch[i] + "</nominal_size_inch>" +
										"<reinforcement_outside_diameter_mm>" + reinforcement_outside_diameter_mm[i] + "</reinforcement_outside_diameter_mm>" +
										"<reinforcement_outside_diameter_inch>" + reinforcement_outside_diameter_inch[i] + "</reinforcement_outside_diameter_inch>" +
										"<outside_diameter_mm>" + outside_diameter_mm[i] + "</outside_diameter_mm>" +
										"<outside_diameter_inch>" + outside_diameter_inch[i] + "</outside_diameter_inch>" +
										"<Max_working_pressure_bar>" + Max_working_pressure_bar[i] + "</Max_working_pressure_bar>" +
										"<max_working_pressure_psi>" + max_working_pressure_psi[i] + "</max_working_pressure_psi>" +
										"<burst_pressure_bar>" + burst_pressure_bar[i] + "</burst_pressure_bar>" +
										"<burst_pressure_psi>" + burst_pressure_psi[i] + "</burst_pressure_psi>" +
										"<min_bend_radius_mm>" + min_bend_radius_mm[i] + "</min_bend_radius_mm>" +
										"<min_bend_radius_inch>" + min_bend_radius_inch[i] + "</min_bend_radius_inch>" +
										"<weight_g_m>" + weight_g_m[i] + "</weight_g_m>" +
										"<weight_lbs_ft>" + weight_lbs_ft[i] + "</weight_lbs_ft>" +
										"<Std_Ferrule1>" + Std_Ferrule1[i] + "</Std_Ferrule1>" +
										"<Std_Insert1>" + Std_Insert1[i] + "</Std_Insert1>" +
										"<standard_mf3000>" + standard_mf3000[i] + "</standard_mf3000>" +
										"<hose_inside_diameter_min_mm>" + hose_inside_diameter_min_mm[i] + "</hose_inside_diameter_min_mm>" +
										"<hose_inside_diameter_min_inches>" + hose_inside_diameter_min_inches[i] + "</hose_inside_diameter_min_inches>" +
										"<hose_inside_diameter_max_mm>"  + hose_inside_diameter_max_mm[i] + "</hose_inside_diameter_max_mm>" +
										"<hose_inside_diameter_max_inches>" + hose_inside_diameter_max_inches[i] + "</hose_inside_diameter_max_inches>" +
										"<reinforcement_outside_diameter_min_mm>" + reinforcement_outside_diameter_min_mm[i] + "</reinforcement_outside_diameter_min_mm>" +
										"<reinforcement_outside_diameter_min_inches>" + reinforcement_outside_diameter_min_inches[i] + "</reinforcement_outside_diameter_min_inches>" +
										"<reinforcement_outside_diameter_max_mm>" + reinforcement_outside_diameter_max_mm[i] + "</reinforcement_outside_diameter_max_mm>" +
										"<reinforcement_outside_diameter_max_inches>" + reinforcement_outside_diameter_max_inches[i] + "</reinforcement_outside_diameter_max_inches>" +
										"<hose_outside_diameter_min_mm>" + hose_outside_diameter_min_mm[i] + "</hose_outside_diameter_min_mm>" +
										"<hose_outside_diameter_min_inches>" + hose_outside_diameter_min_inches[i] + "</hose_outside_diameter_min_inches>" +
										"<hose_outside_diameter_max_mm>" + hose_outside_diameter_max_mm[i] + "</hose_outside_diameter_max_mm>" +
										"<hose_outside_diameter_max_inches>" + hose_outside_diameter_max_inches[i] + "</hose_outside_diameter_max_inches>" +
										"<hose_max_wall_thickness_difference_mm>" + hose_max_wall_thickness_difference_mm[i] + "</hose_max_wall_thickness_difference_mm>" +
										"<hose_max_wall_thickness_difference_inches>" + hose_max_wall_thickness_difference_inches[i] + "</hose_max_wall_thickness_difference_inches>" +
										"<category>" + category[i] + "</category>" +
										"<brand_pack>" + brand_pack[i] + "</brand_pack>" +
										"<brand_customer>" + brand_customer[i] + "</brand_customer>" +
										"<marking_technology>" + marking_technology[i] + "</marking_technology>" +
										"<brand_color>" + brand_color[i] + "</brand_color>" +
										"<brand_logo>" + brand_logo[i] + "</brand_logo>" +
										"<brand_notes>" + brand_notes[i] + "</brand_notes>" +
										"<brand_date_type>" + brand_date_type[i] + "</brand_date_type>" +
										"<department>" + department[i] + "</department>" +
										"<brand>" + brand[i] + "</brand>" +
										"<Printable>" + Printable[i] + "</Printable>" +
										"<Public>" + Public[i] + "</Public>" +
										"<Phase>" + Phase[i] + "</Phase>" +
						"</items>";
					}

				} catch (StringException e) {
					response.write(
						"<BR> Exception -" + e.getMessage() + e.getCause() + "</BR>");
					e.getStackTrace();
				} catch (IllegalArgumentException e) {
					e.getStackTrace();
					response.write(
						"<BR> Exception -" + e.getMessage() + e.getCause() + "</BR>");
				}
				return items;
	}
	/**
	 * @param request
	 * @param response
	 * @param s
	 */
	public String setOutputMDM_Machine(
		IPortalComponentRequest request,
		IPortalComponentResponse response,
		String s) {
		// TODO Auto-generated method stub
		String items = "";
		items = buildMachines(request, response, items);
		//				mdmGetWatermark(request, response);
		//		// ////////////////////////////////////////////////////////////	
		
		String Dyn_Par = convertXml(DynamicParameter);//.replaceAll("&","&amp;");
		String paramTab_Items = convertXml(paramTableItems);
		String paraValue = convertXml(paramValue);
		String paraCat = convertXml(paramCatalog);
		String paraFam = convertXml(paramFamily);
			
		s = 
			"<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
				+ "<ns0:Adobe_Machines xmlns:ns0=\"urn:manulihydraulic.com:Portal:Adobe\">"
				+ "<Header>"
				+ "<paramTableItems>"
				+ paramTab_Items
				+ "</paramTableItems>"
				+ "<paramValue>"
				+ paraValue
				+ "</paramValue>"
				+ "<paramCatalog>"
				+ paraCat
				+ "</paramCatalog>"
				+ "<paramFamily>"
				+ paraFam
				+ "</paramFamily>"
				+ "<DynamicParameter>"
				+ Dyn_Par
				+ "</DynamicParameter>"
				+ "<recsMachine_size>"
				+ recsNew.size()
				+ "</recsMachine_size>"
				+ items
				+ "</Header>"
				+ "</ns0:Adobe_Machines>";
				return s;
	}
	/**
	 * @param request
	 * @param response
	 * @param items
	 */
	private String buildMachines(IPortalComponentRequest request, IPortalComponentResponse response, String items) {
		// TODO Auto-generated method stub
		/*** create a ResultSetDefinition on the machines table ***/
				ResultSetDefinition rsd_prod_machine;
				rsd_prod_machine = new ResultSetDefinition("prod_machine");
				rsd_prod_machine.AddField("Name");
				rsd_prod_machine.AddField("printable_name");
				rsd_prod_machine.AddField("Phase");
				rsd_prod_machine.AddField("sbtitle");
				rsd_prod_machine.AddField("advantages");
				rsd_prod_machine.AddField("technical_features");
				rsd_prod_machine.AddField("main_application");
				rsd_prod_machine.AddField("application");
				rsd_prod_machine.AddField("sector");
				rsd_prod_machine.AddField("Modules");
				rsd_prod_machine.AddField("Drawing");
				rsd_prod_machine.AddField("Voltages");
				rsd_prod_machine.AddField("Capacity");
				rsd_prod_machine.AddField("crimp_range");
				rsd_prod_machine.AddField("Crimp_force_t");
				rsd_prod_machine.AddField("Opening_with_dies");
				rsd_prod_machine.AddField("Complete_opening");
				rsd_prod_machine.AddField("Masterdie_length_mm");
				rsd_prod_machine.AddField("Masterdie_length_inch");
				rsd_prod_machine.AddField("Motor_KW");
				rsd_prod_machine.AddField("Noise_level");
				rsd_prod_machine.AddField("Motor_protection_class");
				rsd_prod_machine.AddField("Dimensions");
				rsd_prod_machine.AddField("Net_Weight_kg");
				rsd_prod_machine.AddField("Net_Weight_lbs");
				rsd_prod_machine.AddField("Gross_Weight_kg");
				rsd_prod_machine.AddField("Gross_weight_lbs");
				rsd_prod_machine.AddField("Packaging_dimensions");
				rsd_prod_machine.AddField("Tooling");
				rsd_prod_machine.AddField("safety_remarks");
				rsd_prod_machine.AddField("Notes");
				rsd_prod_machine.AddField("Standard");
				rsd_prod_machine.AddField("Options");
				rsd_prod_machine.AddField("Diesets_available");
				rsd_prod_machine.AddField("Hose_type_1");
				rsd_prod_machine.AddField("Ref_hoses1");
				rsd_prod_machine.AddField("Max_hoses_size_1");
				rsd_prod_machine.AddField("Fittings_Type1");
				rsd_prod_machine.AddField("Hose_type_2");
				rsd_prod_machine.AddField("Ref_hoses2");
				rsd_prod_machine.AddField("Max_hoses_size_2");
				rsd_prod_machine.AddField("Fittings_Type2");
				rsd_prod_machine.AddField("Hose_type_3");
				rsd_prod_machine.AddField("Ref_hoses3");
				rsd_prod_machine.AddField("Max_hoses_size_3");
				rsd_prod_machine.AddField("Fittings_Type3");
				rsd_prod_machine.AddField("Hose_type_4");
				rsd_prod_machine.AddField("Ref_hoses4");
				rsd_prod_machine.AddField("Max_hoses_size_4");
				rsd_prod_machine.AddField("Fittings_Type4");

				rsd_prod_machine.AddField("Hose_type_5");
				rsd_prod_machine.AddField("Ref_hoses5");
				rsd_prod_machine.AddField("Max_hoses_size_5");
				rsd_prod_machine.AddField("Fittings_Type5");
				rsd_prod_machine.AddField("Hose_type_6");
				rsd_prod_machine.AddField("Ref_hoses6");
				rsd_prod_machine.AddField("Max_hoses_size_6");
				rsd_prod_machine.AddField("Fittings_Type6");

				rsd_prod_machine.AddField("Datasheet_release_No");
				rsd_prod_machine.AddField("Datasheet_release_Date");
				rsd_prod_machine.AddField("tree");
				rsd_prod_machine.AddField("datecreation");
				rsd_prod_machine.AddField("created_from");
				rsd_prod_machine.AddField("date_last_change");
				rsd_prod_machine.AddField("author_change");
				rsd_prod_machine.AddField("Available_accessories");
				/*** create an empty Search object ***/
				Search search_machine = new Search(rsd_prod_machine.GetTable());
				/*** Filters ***/
				FreeFormTableParameter fftp_H =
					search_machine.GetParameters().NewFreeFormTableParameter(
						rsd_prod_machine.GetTable());
				//		response.write("<BR>paramCatalog: " + paramCatalog + "</BR>");

				FreeFormParameterField ffpf_H_Key02 = fftp_H.GetFields().New("tree");
				ffpf_H_Key02.GetFreeForm().NewString(
					"Yes",
					FreeFormParameter.NotNullSearchType);

				if (paramCatalog.equals("false")) {
					FreeFormParameterField ffpf_H_Key01 =
						fftp_H.GetFields().New("Name");
					ffpf_H_Key01.GetFreeForm().NewString(
						paramValue,
						FreeFormParameter.EqualToSearchType);
				}

				/*** Printout Fields ***/
				FreeFormParameterField ffpf_H_Field00 = fftp_H.GetFields().New("Name");
				FreeFormParameterField ffpf_H_Field01 =
					fftp_H.GetFields().New("printable_name");
				FreeFormParameterField ffpf_H_Field02 = fftp_H.GetFields().New("Phase");
				FreeFormParameterField ffpf_H_Field03 =
					fftp_H.GetFields().New("sbtitle");
				FreeFormParameterField ffpf_H_Field04 =
					fftp_H.GetFields().New("advantages");
				FreeFormParameterField ffpf_H_Field05 =
					fftp_H.GetFields().New("technical_features");
				FreeFormParameterField ffpf_H_Field06 =
					fftp_H.GetFields().New("main_application");
				FreeFormParameterField ffpf_H_Field07 =
					fftp_H.GetFields().New("application");
				FreeFormParameterField ffpf_H_Field08 =
					fftp_H.GetFields().New("sector");
				FreeFormParameterField ffpf_H_Field09 =
					fftp_H.GetFields().New("Modules");
				FreeFormParameterField ffpf_H_Field10 =
					fftp_H.GetFields().New("Drawing");
				FreeFormParameterField ffpf_H_Field11 =
					fftp_H.GetFields().New("Voltages");
				FreeFormParameterField ffpf_H_Field12 =
					fftp_H.GetFields().New("Capacity");
				FreeFormParameterField ffpf_H_Field13 =
					fftp_H.GetFields().New("crimp_range");
				FreeFormParameterField ffpf_H_Field14 =
					fftp_H.GetFields().New("Crimp_force_t");
				FreeFormParameterField ffpf_H_Field15 =
					fftp_H.GetFields().New("Opening_with_dies");
				FreeFormParameterField ffpf_H_Field16 =
					fftp_H.GetFields().New("Complete_opening");
				FreeFormParameterField ffpf_H_Field17 =
					fftp_H.GetFields().New("Masterdie_length_mm");
				FreeFormParameterField ffpf_H_Field18 =
					fftp_H.GetFields().New("Masterdie_length_inch");
				FreeFormParameterField ffpf_H_Field19 =
					fftp_H.GetFields().New("Motor_KW");
				FreeFormParameterField ffpf_H_Field20 =
					fftp_H.GetFields().New("Noise_level");
				FreeFormParameterField ffpf_H_Field21 =
					fftp_H.GetFields().New("Motor_protection_class");
				FreeFormParameterField ffpf_H_Field22 =
					fftp_H.GetFields().New("Dimensions");
				FreeFormParameterField ffpf_H_Field23 =
					fftp_H.GetFields().New("Net_Weight_kg");
				FreeFormParameterField ffpf_H_Field24 =
					fftp_H.GetFields().New("Net_Weight_lbs");
				FreeFormParameterField ffpf_H_Field25 =
					fftp_H.GetFields().New("Gross_Weight_kg");
				FreeFormParameterField ffpf_H_Field26 =
					fftp_H.GetFields().New("Gross_weight_lbs");
				FreeFormParameterField ffpf_H_Field27 =
					fftp_H.GetFields().New("Packaging_dimensions");
				FreeFormParameterField ffpf_H_Field28 =
					fftp_H.GetFields().New("Tooling");
				FreeFormParameterField ffpf_H_Field29 =
					fftp_H.GetFields().New("safety_remarks");
				FreeFormParameterField ffpf_H_Field30 = fftp_H.GetFields().New("Notes");
				FreeFormParameterField ffpf_H_Field31 =
					fftp_H.GetFields().New("Standard");
				FreeFormParameterField ffpf_H_Field32 =
					fftp_H.GetFields().New("Options");
				FreeFormParameterField ffpf_H_Field33 =
					fftp_H.GetFields().New("Diesets_available");
				FreeFormParameterField ffpf_H_Field34 =
					fftp_H.GetFields().New("Hose_type_1");
				FreeFormParameterField ffpf_H_Field35 =
					fftp_H.GetFields().New("Ref_hoses1");
				FreeFormParameterField ffpf_H_Field36 =
					fftp_H.GetFields().New("Max_hoses_size_1");
				FreeFormParameterField ffpf_H_Field37 =
					fftp_H.GetFields().New("Fittings_Type1");
				FreeFormParameterField ffpf_H_Field38 =
					fftp_H.GetFields().New("Hose_type_2");
				FreeFormParameterField ffpf_H_Field39 =
					fftp_H.GetFields().New("Ref_hoses2");
				FreeFormParameterField ffpf_H_Field40 =
					fftp_H.GetFields().New("Max_hoses_size_2");
				FreeFormParameterField ffpf_H_Field41 =
					fftp_H.GetFields().New("Fittings_Type2");
				FreeFormParameterField ffpf_H_Field42 =
					fftp_H.GetFields().New("Hose_type_3");
				FreeFormParameterField ffpf_H_Field43 =
					fftp_H.GetFields().New("Ref_hoses3");
				FreeFormParameterField ffpf_H_Field44 =
					fftp_H.GetFields().New("Max_hoses_size_3");
				FreeFormParameterField ffpf_H_Field45 =
					fftp_H.GetFields().New("Fittings_Type3");
				FreeFormParameterField ffpf_H_Field46 =
					fftp_H.GetFields().New("Hose_type_4");
				FreeFormParameterField ffpf_H_Field47 =
					fftp_H.GetFields().New("Ref_hoses4");
				FreeFormParameterField ffpf_H_Field48 =
					fftp_H.GetFields().New("Max_hoses_size_4");
				FreeFormParameterField ffpf_H_Field49 =
					fftp_H.GetFields().New("Fittings_Type4");
				FreeFormParameterField ffpf_H_Field50 =
					fftp_H.GetFields().New("Datasheet_release_No");
				FreeFormParameterField ffpf_H_Field51 =
					fftp_H.GetFields().New("Datasheet_release_Date");
				FreeFormParameterField ffpf_H_Field52 = fftp_H.GetFields().New("tree");
				FreeFormParameterField ffpf_H_Field53 =
					fftp_H.GetFields().New("datecreation");
				FreeFormParameterField ffpf_H_Field54 =
					fftp_H.GetFields().New("created_from");
				FreeFormParameterField ffpf_H_Field55 =
					fftp_H.GetFields().New("date_last_change");
				FreeFormParameterField ffpf_H_Field56 =
					fftp_H.GetFields().New("author_change");
				FreeFormParameterField ffpf_H_Field57 =
					fftp_H.GetFields().New("Available_accessories");

				FreeFormParameterField ffpf_H_Field58 =
					fftp_H.GetFields().New("Hose_type_5");
				FreeFormParameterField ffpf_H_Field59 =
					fftp_H.GetFields().New("Ref_hoses5");
				FreeFormParameterField ffpf_H_Field60 =
					fftp_H.GetFields().New("Max_hoses_size_5");
				FreeFormParameterField ffpf_H_Field61 =
					fftp_H.GetFields().New("Fittings_Type5");
				FreeFormParameterField ffpf_H_Field62 =
					fftp_H.GetFields().New("Hose_type_6");
				FreeFormParameterField ffpf_H_Field63 =
					fftp_H.GetFields().New("Ref_hoses6");
				FreeFormParameterField ffpf_H_Field64 =
					fftp_H.GetFields().New("Max_hoses_size_6");
				FreeFormParameterField ffpf_H_Field65 =
					fftp_H.GetFields().New("Fittings_Type6");
					
				
					
				try {
					A2iResultSet rs_H =
						catalogData.GetResultSet(
							search_machine,
							rsd_prod_machine,
							null,
							true,
							0);
					//			response.write(
					//				"<BR>prod_machine recordCount: "
					//					+ rs_H.GetRecordCount()
					//					+ "</BR>");
					String[] Name = new String[rs_H.GetRecordCount()];
					String[] printable_name = new String[rs_H.GetRecordCount()];
					String[] Phase = new String[rs_H.GetRecordCount()];
					String[] sbtitle = new String[rs_H.GetRecordCount()];
					String[] advantages = new String[rs_H.GetRecordCount()];
					String[] technical_features = new String[rs_H.GetRecordCount()];
					String[] main_application = new String[rs_H.GetRecordCount()];
					String[] application = new String[rs_H.GetRecordCount()];
					String[] sector = new String[rs_H.GetRecordCount()];
					String[] Modules = new String[rs_H.GetRecordCount()];
					String[] Drawing = new String[rs_H.GetRecordCount()];
					String[] Voltages = new String[rs_H.GetRecordCount()];
					String[] Capacity = new String[rs_H.GetRecordCount()];
					String[] crimp_range = new String[rs_H.GetRecordCount()];
					String[] Crimp_force_t = new String[rs_H.GetRecordCount()];
					String[] Opening_with_dies = new String[rs_H.GetRecordCount()];
					String[] Complete_opening = new String[rs_H.GetRecordCount()];
					String[] Masterdie_length_mm = new String[rs_H.GetRecordCount()];
					String[] Masterdie_length_inch = new String[rs_H.GetRecordCount()];
					String[] Motor_KW = new String[rs_H.GetRecordCount()];
					String[] Noise_level = new String[rs_H.GetRecordCount()];
					String[] Motor_protection_class = new String[rs_H.GetRecordCount()];
					String[] Dimensions = new String[rs_H.GetRecordCount()];
					String[] Net_Weight_kg = new String[rs_H.GetRecordCount()];
					String[] Net_Weight_lbs = new String[rs_H.GetRecordCount()];
					String[] Gross_Weight_kg = new String[rs_H.GetRecordCount()];
					String[] Gross_weight_lbs = new String[rs_H.GetRecordCount()];
					String[] Packaging_dimensions = new String[rs_H.GetRecordCount()];
					String[] Tooling = new String[rs_H.GetRecordCount()];
					String[] safety_remarks = new String[rs_H.GetRecordCount()];
					String[] Notes = new String[rs_H.GetRecordCount()];
					String[] Standard = new String[rs_H.GetRecordCount()];
					String[] Options = new String[rs_H.GetRecordCount()];
					String[] Diesets_available = new String[rs_H.GetRecordCount()];
					String[] Hose_type_1 = new String[rs_H.GetRecordCount()];
					String[] Ref_hoses1 = new String[rs_H.GetRecordCount()];
					String[] Max_hoses_size_1 = new String[rs_H.GetRecordCount()];
					String[] Fittings_Type1 = new String[rs_H.GetRecordCount()];
					String[] Hose_type_2 = new String[rs_H.GetRecordCount()];
					String[] Ref_hoses2 = new String[rs_H.GetRecordCount()];
					String[] Max_hoses_size_2 = new String[rs_H.GetRecordCount()];
					String[] Fittings_Type2 = new String[rs_H.GetRecordCount()];
					String[] Hose_type_3 = new String[rs_H.GetRecordCount()];
					String[] Ref_hoses3 = new String[rs_H.GetRecordCount()];
					String[] Max_hoses_size_3 = new String[rs_H.GetRecordCount()];
					String[] Fittings_Type3 = new String[rs_H.GetRecordCount()];
					String[] Hose_type_4 = new String[rs_H.GetRecordCount()];
					String[] Ref_hoses4 = new String[rs_H.GetRecordCount()];
					String[] Max_hoses_size_4 = new String[rs_H.GetRecordCount()];
					String[] Fittings_Type4 = new String[rs_H.GetRecordCount()];
					String[] Hose_type_5 = new String[rs_H.GetRecordCount()];
					String[] Ref_hoses5 = new String[rs_H.GetRecordCount()];
					String[] Max_hoses_size_5 = new String[rs_H.GetRecordCount()];
					String[] Fittings_Type5 = new String[rs_H.GetRecordCount()];
					String[] Hose_type_6 = new String[rs_H.GetRecordCount()];
					String[] Ref_hoses6 = new String[rs_H.GetRecordCount()];
					String[] Max_hoses_size_6 = new String[rs_H.GetRecordCount()];
					String[] Fittings_Type6 = new String[rs_H.GetRecordCount()];
					String[] Datasheet_release_No = new String[rs_H.GetRecordCount()];
					String[] Datasheet_release_Date = new String[rs_H.GetRecordCount()];
					String[] tree = new String[rs_H.GetRecordCount()];
					String[] datecreation = new String[rs_H.GetRecordCount()];
					String[] created_from = new String[rs_H.GetRecordCount()];
					String[] date_last_change = new String[rs_H.GetRecordCount()];
					String[] author_change = new String[rs_H.GetRecordCount()];
					String[] Available_accessories = new String[rs_H.GetRecordCount()];
					
					for (int i = 0; i < rs_H.GetRecordCount(); i++) {
						

						// H_Field00		String
						if (!rs_H.GetValueAt(i, "Name").IsNull()) {
							Name[i] = rs_H.GetValueAt(i, "Name").GetStringValue();
							mdmGetMachineRec(request, response, fld01String);
						}

						if (getRecords == true) {

							// H_Field01		String
							if (!rs_H.GetValueAt(i, "printable_name").IsNull()) {
								printable_name[i] =
									rs_H
										.GetValueAt(i, "printable_name")
										.GetStringValue();
							}
							// H_Field02		String
							if (!rs_H.GetValueAt(i, "Phase").IsNull()) {
								Phase[i] =
									rs_H.GetValueAt(i, "Phase").GetStringValue();
							}
							// H_Field03		String
							if (!rs_H.GetValueAt(i, "sbtitle").IsNull()) {
								sbtitle[i] =
									rs_H.GetValueAt(i, "sbtitle").GetStringValue();
							}
							// H_Field04		String
							if (!rs_H.GetValueAt(i, "advantages").IsNull()) {
								advantages[i] =
									rs_H.GetValueAt(i, "advantages").GetStringValue();
							}
							// H_Field05		String
							if (!rs_H.GetValueAt(i, "technical_features").IsNull()) {
								technical_features[i] =
									rs_H
										.GetValueAt(i, "technical_features")
										.GetStringValue();
							}
							// H_Field06		String
							if (!rs_H.GetValueAt(i, "main_application").IsNull()) {
								main_application[i] =
									rs_H
										.GetValueAt(i, "main_application")
										.GetStringValue();
							}
							// H_Field07		String
							if (!rs_H.GetValueAt(i, "application").IsNull()) {
								application[i] =
									rs_H
										.GetValueAt(i, "application")
										.TranslateToString();
							}
							// H_Field08		String
							if (!rs_H.GetValueAt(i, "sector").IsNull()) {
								sector[i] =
									rs_H.GetValueAt(i, "sector").GetStringValue();
							}
							// H_Field09		String
							if (!rs_H.GetValueAt(i, "Modules").IsNull()) {
								Modules[i] =
									rs_H.GetValueAt(i, "Modules").TranslateToString();
							}
							// H_Field10		Image
							if (!rs_H.GetValueAt(i, "Drawing").IsNull()) {
								CatalogCache catalogCache = new CatalogCache();
								catalogCache.Init(
									CACHE_SERVER,
									CACHE_PORT,
									CACHE_USER,
									CACHE_PASSWORD,
									CACHE_DIRECTORY,
									"English [US]");

								Object id = rs_H.GetValueAt(i, "Drawing").GetData();
								int idx = id.hashCode();
								Drawing[i] =
									CACHE_DIRECTORY
										+ catalogCache.GetImagePath(
											"Images",
											"Thumbnail",
											idx);
								catalogCache.Shutdown();
							}
							// H_Field11		String
							if (!rs_H.GetValueAt(i, "Voltages").IsNull()) {
								Voltages[i] =
									rs_H.GetValueAt(i, "Voltages").TranslateToString();
							}
							// H_Field12		String
							if (!rs_H.GetValueAt(i, "Capacity").IsNull()) {
								Capacity[i] =
									rs_H.GetValueAt(i, "Capacity").TranslateToString();
							}
							// H_Field13		String
							if (!rs_H.GetValueAt(i, "crimp_range").IsNull()) {
								crimp_range[i] =
									rs_H.GetValueAt(i, "crimp_range").GetStringValue();
							}
							// H_Field14		Measurement
							if (rs_H.GetValueAt(i, "Crimp_force_t").IsNull() != true) {
								Measurement fldMeas15 =
									rs_H
										.GetValueAt(i, "Crimp_force_t")
										.GetMeasurement();
								fld15String =
									measurementManager.GetString(
										fldMeas15.GetValue(),
										0,
										0,
										rs_H
											.GetFieldAt("Crimp_force_t")
											.GetDecimalPlaces(),
										false,
										-1);
								Crimp_force_t[i] = fld15String.replaceAll(",", ".");
								fld15Float = Float.valueOf(fld15String).floatValue();
							}
							// H_Field15		String
							if (!rs_H.GetValueAt(i, "Opening_with_dies").IsNull()) {
								Opening_with_dies[i] =
									rs_H
										.GetValueAt(i, "Opening_with_dies")
										.GetStringValue();
							}
							// H_Field16		String
							if (!rs_H.GetValueAt(i, "Complete_opening").IsNull()) {
								Complete_opening[i] =
									rs_H
										.GetValueAt(i, "Complete_opening")
										.GetStringValue();
							}
							// H_Field17		Measurement
							if (rs_H.GetValueAt(i, "Masterdie_length_mm").IsNull()
								!= true) {
								Measurement fldMeas18 =
									rs_H
										.GetValueAt(i, "Masterdie_length_mm")
										.GetMeasurement();
								fld18String =
									measurementManager.GetString(
										fldMeas18.GetValue(),
										fldMeas18.GetUnitID(),
										rs_H
											.GetFieldAt("Masterdie_length_mm")
											.GetDimensionID(),
										rs_H
											.GetFieldAt("Masterdie_length_mm")
											.GetDecimalPlaces(),
										false,
										-1);
									Masterdie_length_mm[i] = fld18String.replaceAll(",", ".");
								//fld18Float = Float.valueOf(fld18String).floatValue();
							}
							// H_Field18		Measurement
							if (rs_H.GetValueAt(i, "Masterdie_length_inch").IsNull()
								!= true) {
									Masterdie_length_inch[i] =
									rs_H
										.GetValueAt(i, "Masterdie_length_inch")
										.GetStringValue();

								//					Measurement fldMeas19 =
								//						rs_H
								//							.GetValueAt(i, "Masterdie_length_inch")
								//							.GetMeasurement();
								//					fld19String =
								//						measurementManager.GetString(
								//							fldMeas19.GetValue(),
								//							fldMeas19.GetUnitID(),
								//							rs_H
								//								.GetFieldAt("Masterdie_length_inch")
								//								.GetDimensionID(),
								//							rs_H
								//								.GetFieldAt("Masterdie_length_inch")
								//								.GetDecimalPlaces(),
								//							false,
								//							-1);
								//					fld19String = fld19String.replaceAll(",", ".");
								//fld19Float = Float.valueOf(fld19String).floatValue();
							}
							// H_Field19		Measurement
							if (rs_H.GetValueAt(i, "Motor_KW").IsNull() != true) {
								Measurement fldMeas20 =
									rs_H.GetValueAt(i, "Motor_KW").GetMeasurement();
								fld20String =
									measurementManager.GetString(
										fldMeas20.GetValue(),
										0,
										0,
										rs_H.GetFieldAt("Motor_KW").GetDecimalPlaces(),
										false,
										-1);
								Motor_KW[i] = fld20String.replaceAll(",", ".");
								fld20Float = Float.valueOf(fld20String).floatValue();
							}
							// H_Field20		String
							if (!rs_H.GetValueAt(i, "Noise_level").IsNull()) {
								Noise_level[i] =
									rs_H.GetValueAt(i, "Noise_level").GetStringValue();
							}
							// H_Field21		String
							if (!rs_H
								.GetValueAt(i, "Motor_protection_class")
								.IsNull()) {
									Motor_protection_class[i] =
									rs_H
										.GetValueAt(i, "Motor_protection_class")
										.GetStringValue();
							}
							// H_Field22		String
							if (!rs_H.GetValueAt(i, "Dimensions").IsNull()) {
								Dimensions[i] =
									rs_H.GetValueAt(i, "Dimensions").GetStringValue();
							}
							// H_Field23		Measurement
							if (rs_H.GetValueAt(i, "Net_Weight_kg").IsNull() != true) {
								Measurement fldMeas24 =
									rs_H
										.GetValueAt(i, "Net_Weight_kg")
										.GetMeasurement();
								fld24String =
									measurementManager.GetString(
										fldMeas24.GetValue(),
										fldMeas24.GetUnitID(),
										rs_H
											.GetFieldAt("Net_Weight_kg")
											.GetDimensionID(),
										rs_H
											.GetFieldAt("Net_Weight_kg")
											.GetDecimalPlaces(),
										false,
										-1);
								Net_Weight_kg[i] = fld24String.replaceAll(",", ".");
								//fld24Float = Float.valueOf(fld24String).floatValue();
							}
							// H_Field24		Measurement
							if (rs_H.GetValueAt(i, "Net_Weight_lbs").IsNull()
								!= true) {
								Measurement fldMeas25 =
									rs_H
										.GetValueAt(i, "Net_Weight_lbs")
										.GetMeasurement();
								fld25String =
									measurementManager.GetString(
										fldMeas25.GetValue(),
										fldMeas25.GetUnitID(),
										rs_H
											.GetFieldAt("Net_Weight_lbs")
											.GetDimensionID(),
										rs_H
											.GetFieldAt("Net_Weight_lbs")
											.GetDecimalPlaces(),
										false,
										-1);
									Net_Weight_lbs[i] = fld25String.replaceAll(",", ".");
								//fld25Float = Float.valueOf(fld25String).floatValue();
							}
							// H_Field25		Measurement
							if (rs_H.GetValueAt(i, "Gross_Weight_kg").IsNull()
								!= true) {
								Measurement fldMeas26 =
									rs_H
										.GetValueAt(i, "Gross_Weight_kg")
										.GetMeasurement();
								fld26String =
									measurementManager.GetString(
										fldMeas26.GetValue(),
										fldMeas26.GetUnitID(),
										rs_H
											.GetFieldAt("Gross_Weight_kg")
											.GetDimensionID(),
										rs_H
											.GetFieldAt("Gross_Weight_kg")
											.GetDecimalPlaces(),
										false,
										-1);
									Gross_Weight_kg[i] = fld26String.replaceAll(",", ".");
								//fld26Float = Float.valueOf(fld26String).floatValue();
							}
							// H_Field26		Measurement
							if (rs_H.GetValueAt(i, "Gross_weight_lbs").IsNull()
								!= true) {
								Measurement fldMeas27 =
									rs_H
										.GetValueAt(i, "Gross_weight_lbs")
										.GetMeasurement();
								fld27String =
									measurementManager.GetString(
										fldMeas27.GetValue(),
										fldMeas27.GetUnitID(),
										rs_H
											.GetFieldAt("Gross_weight_lbs")
											.GetDimensionID(),
										rs_H
											.GetFieldAt("Gross_weight_lbs")
											.GetDecimalPlaces(),
										false,
										-1);
									Gross_weight_lbs[i] = fld27String.replaceAll(",", ".");
								//fld27Float = Float.valueOf(fld27String).floatValue();
							}
							// H_Field27		String
							if (!rs_H.GetValueAt(i, "Packaging_dimensions").IsNull()) {
								Packaging_dimensions[i] =
									rs_H
										.GetValueAt(i, "Packaging_dimensions")
										.GetStringValue();
							}
							// H_Field28		String
							if (!rs_H.GetValueAt(i, "Tooling").IsNull()) {
								Tooling[i] =
									rs_H.GetValueAt(i, "Tooling").GetStringValue();
							}
							// H_Field29		String
							if (!rs_H.GetValueAt(i, "safety_remarks").IsNull()) {
								safety_remarks[i] =
									rs_H
										.GetValueAt(i, "safety_remarks")
										.GetStringValue();
							}
							// H_Field30		String
							if (!rs_H.GetValueAt(i, "Notes").IsNull()) {
								Notes[i] =
									rs_H.GetValueAt(i, "Notes").GetStringValue();
							}
							// H_Field31		String
							if (!rs_H.GetValueAt(i, "Standard").IsNull()) {
								fld32String =
									rs_H.GetValueAt(i, "Standard").TranslateToString();
								mdmGetStandard(
									request,
									response,
									fld32String,
									fld01String);
								Standard[i] = fld32String;	
							}
							// H_Field32		String
							if (!rs_H.GetValueAt(i, "Options").IsNull()) {
								fld33String =
									rs_H.GetValueAt(i, "Options").TranslateToString();
								mdmGetOptions(
									request,
									response,
									fld33String,
									fld01String);
								Options[i] = fld33String;
							}
							// H_Field33		String
							if (!rs_H.GetValueAt(i, "Diesets_available").IsNull()) {
								fld34String =
									rs_H
										.GetValueAt(i, "Diesets_available")
										.TranslateToString();
								mdmGetMachaccRec(
									request,
									response,
									fld34String,
									fld01String);
								Diesets_available[i] = fld34String;
							}
							// H_Field34		String
							if (!rs_H.GetValueAt(i, "Hose_type_1").IsNull()) {
								Hose_type_1[i] =
									rs_H
										.GetValueAt(i, "Hose_type_1")
										.TranslateToString();
							}
							// H_Field35		String
							if (!rs_H.GetValueAt(i, "Ref_hoses1").IsNull()) {
								Ref_hoses1[i] =
									rs_H
										.GetValueAt(i, "Ref_hoses1")
										.TranslateToString();
							}
							// H_Field36		String
							if (!rs_H.GetValueAt(i, "Max_hoses_size_1").IsNull()) {
								Max_hoses_size_1[i] =
									rs_H
										.GetValueAt(i, "Max_hoses_size_1")
										.GetStringValue();
							}
							// H_Field37		String
							if (!rs_H.GetValueAt(i, "Fittings_Type1").IsNull()) {
								Fittings_Type1[i] =
									rs_H
										.GetValueAt(i, "Fittings_Type1")
										.TranslateToString();
							}
							// H_Field38		String
							if (!rs_H.GetValueAt(i, "Hose_type_2").IsNull()) {
								Hose_type_2[i] =
									rs_H
										.GetValueAt(i, "Hose_type_2")
										.TranslateToString();
							}
							// H_Field39		String
							if (!rs_H.GetValueAt(i, "Ref_hoses2").IsNull()) {
								Ref_hoses2[i] =
									rs_H
										.GetValueAt(i, "Ref_hoses2")
										.TranslateToString();
							}
							// H_Field40		String
							if (!rs_H.GetValueAt(i, "Max_hoses_size_2").IsNull()) {
								Max_hoses_size_2[i] =
									rs_H
										.GetValueAt(i, "Max_hoses_size_2")
										.GetStringValue();
							}
							// H_Field41		String
							if (!rs_H.GetValueAt(i, "Fittings_Type2").IsNull()) {
								Fittings_Type2[i] =
									rs_H
										.GetValueAt(i, "Fittings_Type2")
										.TranslateToString();
							}
							// H_Field42		String
							if (!rs_H.GetValueAt(i, "Hose_type_3").IsNull()) {
								Hose_type_3[i] =
									rs_H
										.GetValueAt(i, "Hose_type_3")
										.TranslateToString();
							}
							// H_Field43		String
							if (!rs_H.GetValueAt(i, "Ref_hoses3").IsNull()) {
								Ref_hoses3[i] =
									rs_H
										.GetValueAt(i, "Ref_hoses3")
										.TranslateToString();
							}
							// H_Field44		String
							if (!rs_H.GetValueAt(i, "Max_hoses_size_3").IsNull()) {
								Max_hoses_size_3[i] =
									rs_H
										.GetValueAt(i, "Max_hoses_size_3")
										.GetStringValue();
							}
							// H_Field45		String
							if (!rs_H.GetValueAt(i, "Fittings_Type3").IsNull()) {
								Fittings_Type3[i] =
									rs_H
										.GetValueAt(i, "Fittings_Type3")
										.TranslateToString();
							}
							// H_Field46		String
							if (!rs_H.GetValueAt(i, "Hose_type_4").IsNull()) {
								Hose_type_4[i] =
									rs_H
										.GetValueAt(i, "Hose_type_4")
										.TranslateToString();
							}
							// H_Field47		String
							if (!rs_H.GetValueAt(i, "Ref_hoses4").IsNull()) {
								Ref_hoses4[i] =
									rs_H
										.GetValueAt(i, "Ref_hoses4")
										.TranslateToString();
							}
							// H_Field48		String
							if (!rs_H.GetValueAt(i, "Max_hoses_size_4").IsNull()) {
								Max_hoses_size_4[i] =
									rs_H
										.GetValueAt(i, "Max_hoses_size_4")
										.GetStringValue();
							}
							// H_Field49		String
							if (!rs_H.GetValueAt(i, "Fittings_Type4").IsNull()) {
								Fittings_Type4[i] =
									rs_H
										.GetValueAt(i, "Fittings_Type4")
										.TranslateToString();
							}
							// H_Field50		String
							if (!rs_H.GetValueAt(i, "Datasheet_release_No").IsNull()) {
								Datasheet_release_No[i] =
									rs_H
										.GetValueAt(i, "Datasheet_release_No")
										.GetStringValue();
							}
							// H_Field51		DateTime
							if (rs_H.GetValueAt(i, "Datasheet_release_Date").IsNull()
								!= true) {
								String v_dateTime =
									rs_H
										.GetValueAt(i, "Datasheet_release_Date")
										.GetSystemTime()
										.ConvertSystemTimeToString();
									Datasheet_release_Date[i] =
									v_dateTime.substring(8, 10)
										+ "/"
										+ v_dateTime.substring(5, 7)
										+ "/"
										+ v_dateTime.substring(0, 4);
							}
							// H_Field52		String
							if (!rs_H.GetValueAt(i, "tree").IsNull()) {
								tree[i] =
									rs_H.GetValueAt(i, "tree").GetStringValue();
							}
							// H_Field53		DateTime
							if (rs_H.GetValueAt(i, "datecreation").IsNull() != true) {
								String v_dateTime =
									rs_H
										.GetValueAt(i, "datecreation")
										.GetSystemTime()
										.ConvertSystemTimeToString();
								datecreation[i] =
									v_dateTime.substring(8, 10)
										+ "/"
										+ v_dateTime.substring(5, 7)
										+ "/"
										+ v_dateTime.substring(0, 4);
							}
							// H_Field54		String
							if (!rs_H.GetValueAt(i, "created_from").IsNull()) {
								created_from[i] =
									rs_H.GetValueAt(i, "created_from").GetStringValue();
							}
							// H_Field55		DateTime
							if (rs_H.GetValueAt(i, "date_last_change").IsNull()
								!= true) {
								String v_dateTime =
									rs_H
										.GetValueAt(i, "date_last_change")
										.GetSystemTime()
										.ConvertSystemTimeToString();
									date_last_change[i] =
									v_dateTime.substring(8, 10)
										+ "/"
										+ v_dateTime.substring(5, 7)
										+ "/"
										+ v_dateTime.substring(0, 4);
							}
							// H_Field56		String
							if (!rs_H.GetValueAt(i, "author_change").IsNull()) {
								author_change[i] =
									rs_H
										.GetValueAt(i, "author_change")
										.GetStringValue();
							}
							// H_Field57		String
							if (!rs_H
								.GetValueAt(i, "Available_accessories")
								.IsNull()) {
								fld58String =
									rs_H
										.GetValueAt(i, "Available_accessories")
										.TranslateToString();
								mdmGetMachaccNewRec(
									request,
									response,
									fld58String,
									fld01String);
								Available_accessories[i] = fld58String; 	
							}

							// H_Field46		String
							if (!rs_H.GetValueAt(i, "Hose_type_5").IsNull()) {
								Hose_type_5[i] =
									rs_H
										.GetValueAt(i, "Hose_type_5")
										.TranslateToString();
							}
							// H_Field47		String
							if (!rs_H.GetValueAt(i, "Ref_hoses5").IsNull()) {
								Ref_hoses5[i] =
									rs_H
										.GetValueAt(i, "Ref_hoses5")
										.TranslateToString();
							}
							// H_Field48		String
							if (!rs_H.GetValueAt(i, "Max_hoses_size_5").IsNull()) {
								Max_hoses_size_5[i] =
									rs_H
										.GetValueAt(i, "Max_hoses_size_5")
										.GetStringValue();
							}
							// H_Field49		String
							if (!rs_H.GetValueAt(i, "Fittings_Type5").IsNull()) {
								Fittings_Type5[i] =
									rs_H
										.GetValueAt(i, "Fittings_Type5")
										.TranslateToString();
							}
							// H_Field50		String
							// H_Field46		String
							if (!rs_H.GetValueAt(i, "Hose_type_6").IsNull()) {
								Hose_type_6[i] =
									rs_H
										.GetValueAt(i, "Hose_type_6")
										.TranslateToString();
							}
							// H_Field47		String
							if (!rs_H.GetValueAt(i, "Ref_hoses6").IsNull()) {
								Ref_hoses6[i] =
									rs_H
										.GetValueAt(i, "Ref_hoses6")
										.TranslateToString();
							}
							// H_Field48		String
							if (!rs_H.GetValueAt(i, "Max_hoses_size_6").IsNull()) {
								Max_hoses_size_6[i] =
									rs_H
										.GetValueAt(i, "Max_hoses_size_6")
										.GetStringValue();
							}
							// H_Field49		String
							if (!rs_H.GetValueAt(i, "Fittings_Type6").IsNull()) {
								Fittings_Type6[i] =
									rs_H
										.GetValueAt(i, "Fittings_Type6")
										.TranslateToString();
							}
							// H_Field50		String
							
						}

					}
							Name = convertXML(Name);
							printable_name = convertXML(printable_name);
							Phase = convertXML(Phase);
							sbtitle = convertXML(sbtitle);
							advantages = convertXML(advantages);
							technical_features = convertXML(technical_features);
							main_application = convertXML(main_application);
							application = convertXML(application);
							sector = convertXML(sector);
							Modules = convertXML(Modules);
							Drawing = convertXML(Drawing);
							Voltages = convertXML(Voltages);
							Capacity = convertXML(Capacity);
							crimp_range = convertXML(crimp_range);
							Crimp_force_t = convertXML(Crimp_force_t);
							Opening_with_dies = convertXML(Opening_with_dies);
							Complete_opening = convertXML(Complete_opening);
							Masterdie_length_mm = convertXML(Masterdie_length_mm);
							Masterdie_length_inch = convertXML(Masterdie_length_inch);
							Motor_KW = convertXML(Motor_KW);
							Noise_level = convertXML(Noise_level);
							Motor_protection_class = convertXML(Motor_protection_class);
							Dimensions = convertXML(Dimensions);
							Net_Weight_kg = convertXML(Net_Weight_kg);
							Net_Weight_lbs = convertXML(Net_Weight_lbs);
							Gross_Weight_kg = convertXML(Gross_Weight_kg);
							Gross_weight_lbs = convertXML(Gross_weight_lbs);
							Packaging_dimensions = convertXML(Packaging_dimensions);
							Tooling = convertXML(Tooling);
							safety_remarks = convertXML(safety_remarks);
							Standard = convertXML(Standard);
							Options = convertXML(Options);
							Diesets_available = convertXML(Diesets_available);
							Hose_type_1 = convertXML(Hose_type_1);
							Ref_hoses1 = convertXML(Ref_hoses1);
							Max_hoses_size_1 = convertXML(Max_hoses_size_1);
							Fittings_Type1 = convertXML(Fittings_Type1);
							Hose_type_2 = convertXML(Hose_type_2);
							Ref_hoses2 = convertXML(Ref_hoses2);
							Max_hoses_size_2 = convertXML(Max_hoses_size_2);
							Fittings_Type2 = convertXML(Fittings_Type2);
							Hose_type_3 = convertXML(Hose_type_3);
							Ref_hoses3 = convertXML(Ref_hoses3);
							Max_hoses_size_3 = convertXML(Max_hoses_size_3);
							Fittings_Type3 = convertXML(Fittings_Type3);
							Hose_type_4 = convertXML(Hose_type_4);
							Ref_hoses4 = convertXML(Ref_hoses4);
							Max_hoses_size_4 = convertXML(Max_hoses_size_4);
							Fittings_Type4 = convertXML(Fittings_Type4);
							Hose_type_5 = convertXML(Hose_type_5);
							Ref_hoses5 = convertXML(Ref_hoses5);
							Max_hoses_size_5 = convertXML(Max_hoses_size_5);
							Fittings_Type5 = convertXML(Fittings_Type5);
							Hose_type_6 = convertXML(Hose_type_6);
							Ref_hoses6 = convertXML(Ref_hoses6);
							Max_hoses_size_6 = convertXML(Max_hoses_size_6);
							Fittings_Type6 = convertXML(Fittings_Type6);
							Datasheet_release_No = convertXML(Datasheet_release_No);
							Datasheet_release_Date = convertXML(Datasheet_release_Date);
							tree = convertXML(tree);
							datecreation = convertXML(datecreation);
							created_from = convertXML(created_from);
							date_last_change = convertXML(date_last_change);
							author_change = convertXML(author_change);
							Available_accessories = convertXML(Available_accessories);
					for (int i = 0; i < rs_H.GetRecordCount(); i++){
			
										items = items + "<items>" +
													"<Name>" + Name[i] + "</Name>" +
													"<printable_name>" + printable_name[i] + "</printable_name>" +
													"<Phase>" + Phase[i] + "</Phase>" +
													"<sbtitle>" + sbtitle[i] + "</sbtitle>" +
													"<advantages>" + advantages[i] + "</advantages>" +
													"<technical_features>" + technical_features[i] + "</technical_features>" +
													"<main_application>" + main_application[i] + "</main_application>" +
													"<application>" + application[i] + "</application>" +
													"<sector>" + sector[i] + "</sector>" +
													"<Modules>" + Modules[i] + "</Modules>" +
													"<Drawing>" + Drawing[i] + "</Drawing>" +
													"<Voltages>" + Voltages[i] + "</Voltages>" +
													"<Capacity>" + Capacity[i] + "</Capacity>" +
													"<crimp_range>" + crimp_range[i] + "</crimp_range>" +
													"<Crimp_force_t>" + Crimp_force_t[i] + "</Crimp_force_t>" +
													"<Opening_with_dies>" + Opening_with_dies[i] + "</Opening_with_dies>" +
													"<Complete_opening>" + Complete_opening[i] + "</Complete_opening>" +
													"<Masterdie_length_mm>" + Masterdie_length_mm[i] + "</Masterdie_length_mm>" +
													"<Masterdie_length_inch>" + Masterdie_length_inch[i] + "</Masterdie_length_inch>" +
													"<Motor_KW>" + Motor_KW[i] + "</Motor_KW>" +
													"<Noise_level>" + Noise_level[i] + "</Noise_level>" +
													"<Motor_protection_class>" + Motor_protection_class[i] + "</Motor_protection_class>" +
													"<Dimensions>" + Dimensions[i] + "</Dimensions>" +
													"<Net_Weight_kg>" + Net_Weight_kg[i] + "</Net_Weight_kg>" +
													"<Net_Weight_lbs>" + Net_Weight_lbs[i] + "</Net_Weight_lbs>" +
													"<Gross_Weight_kg>" + Gross_Weight_kg[i] + "</Gross_Weight_kg>" +
													"<Gross_weight_lbs>" + Gross_weight_lbs[i] + "</Gross_weight_lbs>" +
													"<Packaging_dimensions>" + Packaging_dimensions[i] + "</Packaging_dimensions>" +
													"<Tooling>" + Tooling + "</Tooling>" +
													"<safety_remarks>" + safety_remarks[i] + "</safety_remarks>"+
													"<Notes>" + Notes[i] + "</Notes>" +
													"<Standard>" + Standard[i] + "</Standard>" +
													"<Options>" + Options[i] + "</Options>" +
													"<Diesets_available>" + Diesets_available[i] + "</Diesets_available>" +
													"<Hose_type_1>" + Hose_type_1[i] + "</Hose_type_1>" +
													"<Ref_hoses1>" + Ref_hoses1[i] + "</Ref_hoses1>" +
													"<Max_hoses_size_1>" + Max_hoses_size_1[i] + "</Max_hoses_size_1>" +
													"<Fittings_Type1>" + Fittings_Type1[i] + "</Fittings_Type1>" +
													"<Hose_type_2>" + Hose_type_2[i] + "</Hose_type_2>" + 
													"<Ref_hoses2>" + Ref_hoses2[i] + "</Ref_hoses2>" +
													"<Max_hoses_size_2>" + Max_hoses_size_2[i] + "</Max_hoses_size_2>" +
													"<Fittings_Type2>" + Fittings_Type2[i] + "</Fittings_Type2>" +
													"<Hose_type_3>" + Hose_type_3[i] + "</Hose_type_3>" +
													"<Ref_hoses3>" + Ref_hoses3[i] + "</Ref_hoses3>" +
													"<Max_hoses_size_3>" + Max_hoses_size_3[i] + "</Max_hoses_size_3>" +
													"<Fittings_Type3>" + Fittings_Type3[i] + "</Fittings_Type3>" +
													"<Hose_type_4>" + Hose_type_4[i] + "</Hose_type_4>" +
													"<Ref_hoses4>" + Ref_hoses4[i] + "</Ref_hoses4>" +
													"<Max_hoses_size_4>" + Max_hoses_size_4[i] + "</Max_hoses_size_4>" +
													"<Fittings_Type4>" + Fittings_Type4[i] + "</Fittings_Type4>" +
													"<Hose_type_5>" + Hose_type_5[i] + "</Hose_type_5>" +
													"<Ref_hoses5>" + Ref_hoses5[i] + "</Ref_hoses5>" +
													"<Max_hoses_size_5>" + Max_hoses_size_5[i] + "</Max_hoses_size_5>" +
													"<Fittings_Type5>" + Fittings_Type5[i] + "</Fittings_Type5>" +
													"<Hose_type_6>" + Hose_type_6[i] + "</Hose_type_6>" +
													"<Ref_hoses6>" + Ref_hoses6[i]+ "</Ref_hoses6>" +
													"<Max_hoses_size_6>" + Max_hoses_size_6[i] + "</Max_hoses_size_6>" +
													"<Fittings_Type6>" + Fittings_Type6[i] + "</Fittings_Type6>" +
													"<Datasheet_release_No>" + Datasheet_release_No[i] + "</Datasheet_release_No>" +
													"<Datasheet_release_Date>" + Datasheet_release_Date[i] + "</Datasheet_release_Date>" +
													"<tree>" + tree[i] + "</tree>" +
													"<datecreation>" + datecreation[i] + "</datecreation>" +
													"<created_from>" + created_from[i] + "</created_from>" +
													"<date_last_change>" + date_last_change[i] + "</date_last_change>" +
													"<author_change>" + author_change[i] + "</author_change>" +
													"<Available_accessories>" + Available_accessories[i] + "</Available_accessories>" +
									"</items>";
				
								}
				} catch (StringException e) {
					response.write(
						"<BR> e.printStackTrace: "
							+ e.getCause()
							+ e.getMessage()
							+ "</BR>");
					e.printStackTrace();
				}
				return items;
	}
	/**
	 * @param request
	 * @param response
	 * @param s
	 */
	public String setOutputMDM_CrimpRec(
		IPortalComponentRequest request,
		IPortalComponentResponse response,
		String s) {
		// TODO Auto-generated method stub
//		create a ResultSetDefinition on the crimp_rec table
	  ResultSetDefinition rsd_crimp_rec;
	  rsd_crimp_rec = new ResultSetDefinition(paramTableName);
	  rsd_crimp_rec.AddField("Family");
	  rsd_crimp_rec.AddField("version");
	  rsd_crimp_rec.AddField("version_date");
	  rsd_crimp_rec.AddField("Crimp_Version");
	  rsd_crimp_rec.AddField("Description");
	  rsd_crimp_rec.AddField("Technical_Product");
	  rsd_crimp_rec.AddField("DN");
	  rsd_crimp_rec.AddField("Insert");
	  rsd_crimp_rec.AddField("Ferrule");
	  rsd_crimp_rec.AddField("skive_solution");
	  rsd_crimp_rec.AddField("Int_Skive_mm");
	  rsd_crimp_rec.AddField("Int_Skive_inch");
	  rsd_crimp_rec.AddField("Ext_Skive_mm");
	  rsd_crimp_rec.AddField("Ext_Skive_inch");
	  rsd_crimp_rec.AddField("Crimping_Diameter_mm");
	  rsd_crimp_rec.AddField("Crimping_Diameter_inch");
	  rsd_crimp_rec.AddField("General");
	  rsd_crimp_rec.AddField("Skive_Toll");
	  rsd_crimp_rec.AddField("Crimp_Toll");
	  rsd_crimp_rec.AddField("Remarks");
	  rsd_crimp_rec.AddField("Released_by");
	  rsd_crimp_rec.AddField("Notes");
	  rsd_crimp_rec.AddField("sort_criteria");
	  //          Internal Crimping fields
	  rsd_crimp_rec.AddField("Ferrule_phase");
	  rsd_crimp_rec.AddField("IntSkiveTolMax_inch");
	  rsd_crimp_rec.AddField("IntSkiveTolMax_mm");
	  rsd_crimp_rec.AddField("IntSkiveTolMin_inch");
	  rsd_crimp_rec.AddField("IntSkiveTolMin_mm");
	  rsd_crimp_rec.AddField("ExtSkiveTolMax_inch");
	  rsd_crimp_rec.AddField("ExtSkiveTolMax_mm");
	  rsd_crimp_rec.AddField("ExtSkiveTolMin_inch");
	  rsd_crimp_rec.AddField("ExtSkiveTolMin_mm");
	  rsd_crimp_rec.AddField("CrimpTolMax_inch");
	  rsd_crimp_rec.AddField("CrimpTolMax_mm");
	  rsd_crimp_rec.AddField("CrimpTolMin_inch");
	  rsd_crimp_rec.AddField("CrimpTolMin_mm");
	  rsd_crimp_rec.AddField("Rebounding_inch");
	  rsd_crimp_rec.AddField("Rebounding_mm");
	  rsd_crimp_rec.AddField("Bending_Radius_inch");
	  rsd_crimp_rec.AddField("Bending_Radius_mm");
	  rsd_crimp_rec.AddField("Temperature_C");
	  rsd_crimp_rec.AddField("Temperature_K");
	  rsd_crimp_rec.AddField("Duration_cycles");
	  rsd_crimp_rec.AddField("Condition");
	  rsd_crimp_rec.AddField("Safety_Factor");
	  rsd_crimp_rec.AddField("Public");
	  rsd_crimp_rec.AddField("Crimp_DIE");

	  // create an empty Search object
	  Search search = new Search(rsd_crimp_rec.GetTable());

	  FreeFormTableParameter fftpNames =
		  search.GetParameters().NewFreeFormTableParameter(
			  rsd_crimp_rec.GetTable());
	  // Filters		
	  if (paramType.equals("8")) {
		  if (paramHoseFamilyFlag.equals("false")
			  && paramCrimpingFlag.equals("false")) {

			  FreeFormParameterField ffpfKey01 =
				  fftpNames.GetFields().New("Family");
			  ffpfKey01.GetFreeForm().NewString(
				  paramTableFamily,
				  FreeFormParameter.EqualToSearchType);
			  FreeFormParameterField ffpfKey02 =
				  fftpNames.GetFields().New("Crimp_Version");
			  ffpfKey02.GetFreeForm().NewString(
				  paramCrimpVersion,
				  FreeFormParameter.EqualToSearchType);
			  FreeFormParameterField ffpfKey03 =
				  fftpNames.GetFields().New("version");
			  ffpfKey03.GetFreeForm().NewString(
				  paramVersion,
				  FreeFormParameter.EqualToSearchType);
			  if (paramRestrictedFlag.equals("Y")) {
				  FreeFormParameterField ffpfKey04 =
					  fftpNames.GetFields().New("Public");
				  ffpfKey04.GetFreeForm().NewString(
					  "YES",
					  FreeFormParameter.SubstringSearchType);
			  }
		  } else if (
			  paramHoseFamilyFlag.equals("true")
				  && paramCrimpingFlag.equals("false")) {

			  FreeFormParameterField ffpfKey01 =
				  fftpNames.GetFields().New("Family");
			  ffpfKey01.GetFreeForm().NewString(
				  paramTableFamily,
				  FreeFormParameter.EqualToSearchType);
			  FreeFormParameterField ffpfKey04 =
				  fftpNames.GetFields().New("Public");
			  ffpfKey04.GetFreeForm().NewString(
				  "YES",
				  FreeFormParameter.SubstringSearchType);

		  } else if (paramCrimpingFlag.equals("true")) {

			  FreeFormParameterField ffpfKey04 =
				  fftpNames.GetFields().New("Public");
			  ffpfKey04.GetFreeForm().NewString(
				  "YES",
				  FreeFormParameter.EqualToSearchType);
		  }

	  } else {
		  FreeFormParameterField ffpfKey01 =
			  fftpNames.GetFields().New("Crimp_Version");
		  ffpfKey01.GetFreeForm().NewString(
			  paramCrimpVersion,
			  FreeFormParameter.EqualToSearchType);

		  FreeFormParameterField ffpfKey02 =
			  fftpNames.GetFields().New("Family");
		  ffpfKey02.GetFreeForm().NewString(
			  paramTableFamily,
			  FreeFormParameter.EqualToSearchType);

		  FreeFormParameterField ffpfKey03 =
			  fftpNames.GetFields().New("version");
		  ffpfKey03.GetFreeForm().NewString(
			  paramVersion,
			  FreeFormParameter.EqualToSearchType);
	  }

	  //		  Printout Fields 
	  FreeFormParameterField ffpfField00 =
		  fftpNames.GetFields().New("Family");
	  FreeFormParameterField ffpfField01 =
		  fftpNames.GetFields().New("version");
	  FreeFormParameterField ffpfField02 =
		  fftpNames.GetFields().New("version_date");
	  FreeFormParameterField ffpfField03 =
		  fftpNames.GetFields().New("Crimp_Version");
	  FreeFormParameterField ffpfField04 =
		  fftpNames.GetFields().New("Description");
	  FreeFormParameterField ffpfField05 =
		  fftpNames.GetFields().New("Technical_Product");
	  FreeFormParameterField ffpfField06 = fftpNames.GetFields().New("DN");
	  FreeFormParameterField ffpfField07 =
		  fftpNames.GetFields().New("Insert");
	  FreeFormParameterField ffpfField08 =
		  fftpNames.GetFields().New("Ferrule");
	  FreeFormParameterField ffpfField09 =
		  fftpNames.GetFields().New("skive_solution");
	  FreeFormParameterField ffpfField10 =
		  fftpNames.GetFields().New("Int_Skive_mm");
	  FreeFormParameterField ffpfField11 =
		  fftpNames.GetFields().New("Int_Skive_inch");
	  FreeFormParameterField ffpfField12 =
		  fftpNames.GetFields().New("Ext_Skive_mm");
	  FreeFormParameterField ffpfField13 =
		  fftpNames.GetFields().New("Ext_Skive_inch");
	  FreeFormParameterField ffpfField14 =
		  fftpNames.GetFields().New("Crimping_Diameter_mm");
	  FreeFormParameterField ffpfField15 =
		  fftpNames.GetFields().New("Crimping_Diameter_inch");
	  FreeFormParameterField ffpfField16 =
		  fftpNames.GetFields().New("General");
	  FreeFormParameterField ffpfField17 =
		  fftpNames.GetFields().New("Skive_Toll");
	  FreeFormParameterField ffpfField18 =
		  fftpNames.GetFields().New("Crimp_Toll");
	  FreeFormParameterField ffpfField19 =
		  fftpNames.GetFields().New("Remarks");
	  FreeFormParameterField ffpfField20 =
		  fftpNames.GetFields().New("Released_by");
	  FreeFormParameterField ffpfField21 = fftpNames.GetFields().New("Notes");
	  FreeFormParameterField ffpfField22 =
		  fftpNames.GetFields().New("sort_criteria");
	  FreeFormParameterField ffpfField23 =
		  fftpNames.GetFields().New("Ferrule_phase");
	  FreeFormParameterField ffpfField24 =
		  fftpNames.GetFields().New("IntSkiveTolMax_inch");
	  FreeFormParameterField ffpfField25 =
		  fftpNames.GetFields().New("IntSkiveTolMax_mm");
	  FreeFormParameterField ffpfField26 =
		  fftpNames.GetFields().New("IntSkiveTolMin_inch");
	  FreeFormParameterField ffpfField27 =
		  fftpNames.GetFields().New("IntSkiveTolMin_mm");
	  FreeFormParameterField ffpfField28 =
		  fftpNames.GetFields().New("ExtSkiveTolMax_inch");
	  FreeFormParameterField ffpfField29 =
		  fftpNames.GetFields().New("ExtSkiveTolMax_mm");
	  FreeFormParameterField ffpfField30 =
		  fftpNames.GetFields().New("ExtSkiveTolMin_inch");
	  FreeFormParameterField ffpfField31 =
		  fftpNames.GetFields().New("ExtSkiveTolMin_mm");
	  FreeFormParameterField ffpfField32 =
		  fftpNames.GetFields().New("CrimpTolMax_inch");
	  FreeFormParameterField ffpfField33 =
		  fftpNames.GetFields().New("CrimpTolMax_mm");
	  FreeFormParameterField ffpfField34 =
		  fftpNames.GetFields().New("CrimpTolMin_inch");
	  FreeFormParameterField ffpfField35 =
		  fftpNames.GetFields().New("CrimpTolMin_mm");
	  FreeFormParameterField ffpfField36 =
		  fftpNames.GetFields().New("Rebounding_inch");
	  FreeFormParameterField ffpfField37 =
		  fftpNames.GetFields().New("Rebounding_mm");
	  FreeFormParameterField ffpfField38 =
		  fftpNames.GetFields().New("Bending_Radius_inch");
	  FreeFormParameterField ffpfField39 =
		  fftpNames.GetFields().New("Bending_Radius_mm");
	  FreeFormParameterField ffpfField40 =
		  fftpNames.GetFields().New("Temperature_C");
	  FreeFormParameterField ffpfField41 =
		  fftpNames.GetFields().New("Temperature_K");
	  FreeFormParameterField ffpfField42 =
		  fftpNames.GetFields().New("Duration_cycles");
	  FreeFormParameterField ffpfField43 =
		  fftpNames.GetFields().New("Condition");
	  FreeFormParameterField ffpfField44 =
		  fftpNames.GetFields().New("Safety_Factor");
	  FreeFormParameterField ffpfField45 =
		  fftpNames.GetFields().New("sort_criteria");
	  FreeFormParameterField ffpfField46 =
		  fftpNames.GetFields().New("Public");
	  FreeFormParameterField ffpfField47 =
		  fftpNames.GetFields().New("Crimp_DIE");
	  // Ordinamento records
	  
	  //			Struttura Output PI CrimpRec
	  A2iResultSet rs = new A2iResultSet();	
	  String items = s;	
	  items = buildCrimpRec(rsd_crimp_rec, rs, search, request, response, items);
	  s =
	  "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
		  + "<ns0:Adobe_CrimpRec xmlns:ns0=\"urn:manulihydraulic.com:Portal:Adobe\">"
		  + "<Header>"
		  + "<DynamicParameter>" + DynamicParameter + "</DynamicParameter>" 
		  +  "<paramTableHeader>" + paramTableHeader  + "<paramTableHeader/>" 
		  + "<paramTableItems>" + paramTableItems + "<paramTableItems/>"
		  + "<paramType>" + paramType + "<paramType/>" 
		  + "<paramValue>" + paramValue + "<paramValue/>" 
		  + "<paramBrand>" + paramBrand + "<paramBrand/>" 
		  + "<paramCatalog>" + paramCatalog + "</paramCatalog>" 
		  + "<paramCategory>" + paramCategory + "</paramCategory>" 
		  + "<Name>" + Name + "</Name>" 
		  + "<catalogue_subtitle>" + catalogue_subtitle + "</catalogue_subtitle>" 
		  + "<catalogue_image>" + imagePath + "</catalogue_image>" 
		  + "<key_performance>" + key_performance + "<key_performance/>"
		  + "<main_application>" + main_application + "<main_application/>"
		  + "<min_cont_serv_temp>" + min_cont_serv_temp + "<min_cont_serv_temp/>"
		  + "<max_cont_serv_temp>" + max_cont_serv_temp + "<max_cont_serv_temp/>"
		  + "<max_operating_temp>" + max_operating_temp + "<max_operating_temp/>"
		  + "<min_external_temp>" + min_external_temp + "<min_external_temp/>"
		  + "<recommended_fluids>" + recommended_fluids + "<recommended_fluids/>"
		  + "<refrigerants>" + refrigerants + "<refrigerants/>"
		  + "<lubrificants>" + lubrificants + "<lubrificants/>"
		  + "<reinforcement>" + reinforcement + "<reinforcement/>"
		  + "<cover>" + cover + "<cover/>"
		  + "<applicable_specs>" + applicable_specs + "<applicable_specs/>"
		  + "<type_approvals>" + type_approvals + "<type_approvals/>"
		  + "<other_technical_notes>" + other_technical_notes + "<other_technical_notes/>"
		  + "<standard_Packaging>" + standard_Packaging + "<standard_Packaging/>"
		  + "<catalogue_small_exclamation>" + catalogue_small_exclamation + "<catalogue_small_exclamation/>"
		  + "<catalogue_big_exclamation>" + catalogue_big_exclamation + "<catalogue_big_exclamation/>"
		  + "<item_rec>" + recs.size() + "<item_rec/>"
		  + "<item_recnew>" + recsNew.size() + "<item_recnew/>"
		  + items
		  + "</Header>"
		  + "</ns0:Adobe_CrimpRec>";
		  return s;
	}
	/**
	 * @param rsd_crimp_rec
	 */
	private String buildCrimpRec(ResultSetDefinition rsd_crimp_rec, A2iResultSet rs, Search search, IPortalComponentRequest request,
	IPortalComponentResponse response, String items) {
		// TODO Auto-generated method stub
		
		try {	
				  Object fields = null;
				  Object locale = null;
				  rs = catalogData.GetResultSet(search, rsd_crimp_rec, null, true, 0);
				  //				response.write("<BR>prima del FOR " + rs.GetRecordCount() + "</BR>");
				  
				  if (rs.GetRecordCount() == 0) {
					  response.write("<BR>MESSAGE: no records found!!!</BR>");
				  }
					String[] Family = new String[rs.GetRecordCount()];	
					String[] version = new String[rs.GetRecordCount()];
					String[] version_date = new String[rs.GetRecordCount()];
					String[] Crimp_Version = new String[rs.GetRecordCount()];
					String[] Description = new String[rs.GetRecordCount()];
					String[] Technical_Product = new String[rs.GetRecordCount()];
					String[] DN = new String[rs.GetRecordCount()];
					String[] Insert = new String[rs.GetRecordCount()];
					String[] Ferrule = new String[rs.GetRecordCount()];
					String[] skive_solution = new String[rs.GetRecordCount()];
					String[] Int_Skive_mm = new String[rs.GetRecordCount()];
					String[] Int_Skive_inch = new String[rs.GetRecordCount()];
					String[] Ext_Skive_mm = new String[rs.GetRecordCount()];
					String[] Ext_Skive_inch = new String[rs.GetRecordCount()];
					String[] Crimping_Diameter_mm = new String[rs.GetRecordCount()];
					String[] Crimping_Diameter_inch = new String[rs.GetRecordCount()];
					String[] General = new String[rs.GetRecordCount()];
					String[] Skive_Toll = new String[rs.GetRecordCount()];
					String[] Crimp_Toll = new String[rs.GetRecordCount()];
					String[] Remarks = new String[rs.GetRecordCount()];
					String[] Released_by = new String[rs.GetRecordCount()];
					String[] Notes = new String[rs.GetRecordCount()];
					String[] sort_criteria = new String[rs.GetRecordCount()];
					String[] Ferrule_phase = new String[rs.GetRecordCount()];
					String[] IntSkiveTolMax_inch = new String[rs.GetRecordCount()];
					String[] IntSkiveTolMax_mm = new String[rs.GetRecordCount()];
					String[] IntSkiveTolMin_inch = new String[rs.GetRecordCount()];
					String[] IntSkiveTolMin_mm = new String[rs.GetRecordCount()];
					String[] ExtSkiveTolMax_inch = new String[rs.GetRecordCount()];
					String[] ExtSkiveTolMax_mm = new String[rs.GetRecordCount()];
					String[] ExtSkiveTolMin_inch = new String[rs.GetRecordCount()];
					String[] ExtSkiveTolMin_mm = new String[rs.GetRecordCount()];
					String[] CrimpTolMax_inch = new String[rs.GetRecordCount()];
					String[] CrimpTolMax_mm = new String[rs.GetRecordCount()];
					String[] CrimpTolMin_inch = new String[rs.GetRecordCount()];
					String[] CrimpTolMin_mm = new String[rs.GetRecordCount()];
					String[] Rebounding_inch = new String[rs.GetRecordCount()];
					String[] Rebounding_mm = new String[rs.GetRecordCount()];
					String[] Bending_Radius_inch = new String[rs.GetRecordCount()];
					String[] Bending_Radius_mm = new String[rs.GetRecordCount()];
					String[] Temperature_C = new String[rs.GetRecordCount()];
					String[] Temperature_K = new String[rs.GetRecordCount()];
					String[] Duration_cycles = new String[rs.GetRecordCount()];
					String[] Condition = new String[rs.GetRecordCount()];
					String[] Safety_Factor = new String[rs.GetRecordCount()];
					String[] Public = new String[rs.GetRecordCount()];
					String[] Crimp_DIE = new String[rs.GetRecordCount()];
			
				  for (int i = 0; i < rs.GetRecordCount(); i++) {
					
					  Family[i] = rs.GetValueAt(i, "Family").GetStringValue();
					  if (rs.GetValueAt(i, "version").IsNull() != true) {
						  version[i] = rs.GetValueAt(i, "version").GetStringValue();
					  } else {
						  version[i] = "";
					  }
					  if (rs.GetValueAt(i, "version_date").IsNull() != true) {
						  String v_dateTime =
							  rs
								  .GetValueAt(i, "version_date")
								  .GetSystemTime()
								  .ConvertSystemTimeToString();
						  version_date[i] =
							  v_dateTime.substring(0, 4)
								  + "/"
								  + v_dateTime.substring(5, 7)
								  + "/"
								  + v_dateTime.substring(8, 10);
					  } else {
							version_date[i] = "";
					  }

					  if (rs.GetValueAt(i, "Crimp_Version").IsNull() != true) {
						Crimp_Version[i] =
							  rs.GetValueAt(i, "Crimp_Version").GetStringValue();
						  int start = Crimp_Version[i].indexOf(",");
						Crimp_Version[i] = Crimp_Version[i].substring(start + 1);
					  } else {
						Crimp_Version[i] = "";
					  }

					  if (rs.GetValueAt(i, "Description").IsNull() != true) {
						Description[i] =
							  rs.GetValueAt(i, "Description").GetStringValue();
					  } else {
						Description[i] = "";
					  }
					  if (rs.GetValueAt(i, "Technical_Product").IsNull() != true) {
						Technical_Product[i] =
							  rs.GetValueAt(i, "Technical_Product").GetStringValue();
					  } else {
						Technical_Product[i] = "";
					  }
					  

					  // HOSE SIZE DN
						DN[i] =
						  rs.GetValueAt(i, "DN").GetStringValue();

					  if (rs.GetValueAt(i, "Insert").IsNull() != true) {
						  Insert[i] = rs.GetValueAt(i, "Insert").GetStringValue();
						  int start = fld10String.indexOf(",");
						  Insert[i] = Insert[i].substring(start + 1);
					  } else {
						  Insert[i] = "";
					  }
					  if (rs.GetValueAt(i, "Ferrule").IsNull() != true) {
						  Ferrule[i] = rs.GetValueAt(i, "Ferrule").GetStringValue();
					  } else {
						  Ferrule[i] = "";
					  }
					  if (rs.GetValueAt(i, "skive_solution").IsNull() != true) {
						skive_solution[i] =
							  rs.GetValueAt(i, "skive_solution").GetStringValue();
					  } else {
						skive_solution[i] = "";
					  }
					  if (rs.GetValueAt(i, "Int_Skive_mm").IsNull() != true) {
						  Measurement fldMeas13 =
							  rs.GetValueAt(i, "Int_Skive_mm").GetMeasurement();
						  String fld13String =
							  measurementManager.GetString(
								  fldMeas13.GetValue(),
								  0,
								  0,
								  rs.GetFieldAt("Int_Skive_mm").GetDecimalPlaces(),
								  false,
								  -1);
						  Int_Skive_mm[i] = fld13String.replaceAll(",", ".");
						  fld13Float = Float.valueOf(fld13String).floatValue();//da controllare
					  } else {
						Int_Skive_mm[i] = "0";
					  }
					  if (rs.GetValueAt(i, "Int_Skive_inch").IsNull() != true) {
						  Measurement fldMeas14 =
							  rs.GetValueAt(i, "Int_Skive_inch").GetMeasurement();
						  String fld14String =
							  measurementManager.GetString(
								  fldMeas14.GetValue(),
								  0,
								  0,
								  rs.GetFieldAt("Int_Skive_inch").GetDecimalPlaces(),
								  false,
								  -1);
						  Int_Skive_inch[i] = fld14String.replaceAll(",", ".");
						  fld14Float = Float.valueOf(fld14String).floatValue();
					  } else {
						Int_Skive_inch[i] = "0";
					  }
					  if (rs.GetValueAt(i, "Ext_Skive_mm").IsNull() != true) {
						  Measurement fldMeas15 =
							  rs.GetValueAt(i, "Ext_Skive_mm").GetMeasurement();
						  String fld15String =
							  measurementManager.GetString(
								  fldMeas15.GetValue(),
								  0,
								  0,
								  rs.GetFieldAt("Ext_Skive_mm").GetDecimalPlaces(),
								  false,
								  -1);
						  Ext_Skive_mm[i] = fld15String.replaceAll(",", ".");
						  fld15Float = Float.valueOf(fld15String).floatValue();
					  } else {
						Ext_Skive_mm[i] = "0";
					  }
					  if (rs.GetValueAt(i, "Ext_Skive_inch").IsNull() != true) {
						  Measurement fldMeas16 =
							  rs.GetValueAt(i, "Ext_Skive_inch").GetMeasurement();
						  String fld16String =
							  measurementManager.GetString(
								  fldMeas16.GetValue(),
								  0,
								  0,
								  rs.GetFieldAt("Ext_Skive_inch").GetDecimalPlaces(),
								  false,
								  -1);
						  Ext_Skive_inch[i] = fld16String.replaceAll(",", ".");
						  fld16Float = Float.valueOf(fld16String).floatValue();
					  } else {
						Ext_Skive_inch[i] = "0";
					  }
					  if (rs.GetValueAt(i, "Crimping_Diameter_mm").IsNull()
						  != true) {
						  Measurement fldMeas17 =
							  rs
								  .GetValueAt(i, "Crimping_Diameter_mm")
								  .GetMeasurement();
						  String fld17String =
							  measurementManager.GetString(
								  fldMeas17.GetValue(),
								  0,
								  0,
								  rs
									  .GetFieldAt("Crimping_Diameter_mm")
									  .GetDecimalPlaces(),
								  false,
								  -1);
						  Crimping_Diameter_mm[i] = fld17String.replaceAll(",", ".");
						  fld17Float = Float.valueOf(fld17String).floatValue();
					  } else {
						Crimping_Diameter_mm[i] = "0";
					  }
					  if (rs.GetValueAt(i, "Crimping_Diameter_inch").IsNull()
						  != true) {
						  Measurement fldMeas18 =
							  rs
								  .GetValueAt(i, "Crimping_Diameter_inch")
								  .GetMeasurement();
						  String fld18String =
							  measurementManager.GetString(
								  fldMeas18.GetValue(),
								  0,
								  0,
								  rs
									  .GetFieldAt("Crimping_Diameter_inch")
									  .GetDecimalPlaces(),
								  false,
								  -1);
							Crimping_Diameter_inch[i] = fld18String.replaceAll(",", ".");
						  fld18Float = Float.valueOf(fld18String).floatValue();
					  } else {
						Crimping_Diameter_inch[i] = "0";
					  }
					  if (rs.GetValueAt(i, "General").IsNull() != true) {
						General[i] = rs.GetValueAt(i, "General").GetStringValue();
					  } else {
						 General[i] = "";
					  }
					  if (rs.GetValueAt(i, "Skive_Toll").IsNull() != true) {
						Skive_Toll[i] =
							  rs.GetValueAt(i, "Skive_Toll").GetStringValue();
					  } else {
						Skive_Toll[i] = "";
					  }
					  if (rs.GetValueAt(i, "Crimp_Toll").IsNull() != true) {
						Crimp_Toll[i] =
							  rs.GetValueAt(i, "Crimp_Toll").GetStringValue();
					  } else {
						Crimp_Toll[i] = "";
					  }
					  if (rs.GetValueAt(i, "Remarks").IsNull() != true) {
						Remarks[i] = rs.GetValueAt(i, "Remarks").GetStringValue();
					  } else {
						Remarks[i] = "";
					  }
					  if (rs.GetValueAt(i, "Released_by").IsNull() != true) {
						Released_by[i] =
							  rs.GetValueAt(i, "Released_by").GetStringValue();
					  } else {
						Released_by[i] = "";
					  }
					  if (rs.GetValueAt(i, "Notes").IsNull() != true) {
						Notes[i] = rs.GetValueAt(i, "Notes").GetStringValue();
					  } else {
						Notes[i] = "";
					  }
					  if (rs.GetValueAt(i, "sort_criteria").IsNull() != true) {
						sort_criteria[i] =
							  rs.GetValueAt(i, "sort_criteria").GetStringValue();
					  } else {
						sort_criteria[i] = "";
					  }
					  if (rs.GetValueAt(i, "Ferrule_phase").IsNull() != true) {
						Ferrule_phase[i] =
							  rs.GetValueAt(i, "Ferrule_phase").GetStringValue();
					  } else {
						Ferrule_phase[i] = "";
					  }
					  if (rs.GetValueAt(i, "IntSkiveTolMax_inch").IsNull() != true) {
						  Measurement fldMeas30 =
							  rs
								  .GetValueAt(i, "IntSkiveTolMax_inch")
								  .GetMeasurement();
						  String fld30String =
							  measurementManager.GetString(
								  fldMeas30.GetValue(),
								  0,
								  0,
								  rs
									  .GetFieldAt("IntSkiveTolMax_inch")
									  .GetDecimalPlaces(),
								  false,
								  -1);
						IntSkiveTolMax_inch[i] = fld30String.replaceAll(",", ".");
						  fld30Float = Float.valueOf(fld30String).floatValue();
					  } else {
						IntSkiveTolMax_inch[i] = "0";
					  }
					  if (rs.GetValueAt(i, "IntSkiveTolMax_mm").IsNull() != true) {
						  Measurement fldMeas31 =
							  rs.GetValueAt(i, "IntSkiveTolMax_mm").GetMeasurement();
						  String fld31String =
							  measurementManager.GetString(
								  fldMeas31.GetValue(),
								  0,
								  0,
								  rs
									  .GetFieldAt("IntSkiveTolMax_mm")
									  .GetDecimalPlaces(),
								  false,
								  -1);
						IntSkiveTolMax_mm[i] = fld31String.replaceAll(",", ".");
						  fld31Float = Float.valueOf(fld31String).floatValue();
					  } else {
						IntSkiveTolMax_mm[i] = "0";
					  }
					  if (rs.GetValueAt(i, "IntSkiveTolMin_inch").IsNull() != true) {
						  Measurement fldMeas32 =
							  rs
								  .GetValueAt(i, "IntSkiveTolMin_inch")
								  .GetMeasurement();
						  String fld32String =
							  measurementManager.GetString(
								  fldMeas32.GetValue(),
								  0,
								  0,
								  rs
									  .GetFieldAt("IntSkiveTolMin_inch")
									  .GetDecimalPlaces(),
								  false,
								  -1);
						IntSkiveTolMin_inch[i] = fld32String.replaceAll(",", ".");
						  fld32Float = Float.valueOf(fld32String).floatValue();
					  } else {
						IntSkiveTolMin_inch[i] = "0";
					  }

					  if (rs.GetValueAt(i, "IntSkiveTolMin_mm").IsNull() != true) {
						  Measurement fldMeas33 =
							  rs.GetValueAt(i, "IntSkiveTolMin_mm").GetMeasurement();
						  String fld33String =
							  measurementManager.GetString(
								  fldMeas33.GetValue(),
								  0,
								  0,
								  rs
									  .GetFieldAt("IntSkiveTolMin_mm")
									  .GetDecimalPlaces(),
								  false,
								  -1);
						IntSkiveTolMin_mm[i] = fld33String.replaceAll(",", ".");
						  fld33Float = Float.valueOf(fld33String).floatValue();
					  } else {
						IntSkiveTolMin_mm[i] = "0";
					  }

					  if (rs.GetValueAt(i, "ExtSkiveTolMax_inch").IsNull() != true) {
						  Measurement fldMeas34 =
							  rs
								  .GetValueAt(i, "ExtSkiveTolMax_inch")
								  .GetMeasurement();
						  String fld34String =
							  measurementManager.GetString(
								  fldMeas34.GetValue(),
								  0,
								  0,
								  rs
									  .GetFieldAt("ExtSkiveTolMax_inch")
									  .GetDecimalPlaces(),
								  false,
								  -1);
						ExtSkiveTolMax_inch[i] = fld34String.replaceAll(",", ".");
						  fld34Float = Float.valueOf(fld34String).floatValue();
					  } else {
						ExtSkiveTolMax_inch[i] = "0";
					  }
					  if (rs.GetValueAt(i, "ExtSkiveTolMax_mm").IsNull() != true) {
						  Measurement fldMeas35 =
							  rs.GetValueAt(i, "ExtSkiveTolMax_mm").GetMeasurement();
						  String fld35String =
							  measurementManager.GetString(
								  fldMeas35.GetValue(),
								  0,
								  0,
								  rs
									  .GetFieldAt("ExtSkiveTolMax_mm")
									  .GetDecimalPlaces(),
								  false,
								  -1);
						ExtSkiveTolMax_mm[i] = fld35String.replaceAll(",", ".");
						  fld35Float = Float.valueOf(fld35String).floatValue();
					  } else {
						ExtSkiveTolMax_mm[i] = "0";
					  }
					  if (rs.GetValueAt(i, "ExtSkiveTolMin_inch").IsNull() != true) {
						  Measurement fldMeas36 =
							  rs
								  .GetValueAt(i, "ExtSkiveTolMin_inch")
								  .GetMeasurement();
						  String fld36String =
							  measurementManager.GetString(
								  fldMeas36.GetValue(),
								  0,
								  0,
								  rs
									  .GetFieldAt("ExtSkiveTolMin_inch")
									  .GetDecimalPlaces(),
								  false,
								  -1);
						ExtSkiveTolMin_inch[i] = fld36String.replaceAll(",", ".");
						  fld36Float = Float.valueOf(fld36String).floatValue();
					  } else {
						ExtSkiveTolMin_inch[i] = "0";
					  }
					  if (rs.GetValueAt(i, "ExtSkiveTolMin_mm").IsNull() != true) {
						  Measurement fldMeas37 =
							  rs.GetValueAt(i, "ExtSkiveTolMin_mm").GetMeasurement();
						  String fld37String =
							  measurementManager.GetString(
								  fldMeas37.GetValue(),
								  0,
								  0,
								  rs
									  .GetFieldAt("ExtSkiveTolMin_mm")
									  .GetDecimalPlaces(),
								  false,
								  -1);
						ExtSkiveTolMin_mm[i] = fld37String.replaceAll(",", ".");
						  fld37Float = Float.valueOf(fld37String).floatValue();
					  } else {
						ExtSkiveTolMin_mm[i] = "0";
					  }
					  if (rs.GetValueAt(i, "CrimpTolMax_inch").IsNull() != true) {
						  Measurement fldMeas38 =
							  rs.GetValueAt(i, "CrimpTolMax_inch").GetMeasurement();
						  String fld38String =
							  measurementManager.GetString(
								  fldMeas38.GetValue(),
								  0,
								  0,
								  rs
									  .GetFieldAt("CrimpTolMax_inch")
									  .GetDecimalPlaces(),
								  false,
								  -1);
						CrimpTolMax_inch[i] = fld38String.replaceAll(",", ".");
						  fld38Float = Float.valueOf(fld38String).floatValue();
					  } else {
						CrimpTolMax_inch[i] = "0";
					  }
					  if (rs.GetValueAt(i, "CrimpTolMax_mm").IsNull() != true) {
						  Measurement fldMeas39 =
							  rs.GetValueAt(i, "CrimpTolMax_mm").GetMeasurement();
						  String fld39String =
							  measurementManager.GetString(
								  fldMeas39.GetValue(),
								  0,
								  0,
								  rs.GetFieldAt("CrimpTolMax_mm").GetDecimalPlaces(),
								  false,
								  -1);
						CrimpTolMax_mm[i] = fld39String.replaceAll(",", ".");
						  fld39Float = Float.valueOf(fld39String).floatValue();
					  } else {
						CrimpTolMax_mm[i] = "0";
					  }
					  if (rs.GetValueAt(i, "CrimpTolMin_inch").IsNull() != true) {
						  Measurement fldMeas40 =
							  rs.GetValueAt(i, "CrimpTolMin_inch").GetMeasurement();
						  String fld40String =
							  measurementManager.GetString(
								  fldMeas40.GetValue(),
								  0,
								  0,
								  rs
									  .GetFieldAt("CrimpTolMin_inch")
									  .GetDecimalPlaces(),
								  false,
								  -1);
						CrimpTolMin_inch[i] = fld40String.replaceAll(",", ".");
						  fld40Float = Float.valueOf(fld40String).floatValue();
					  } else {
						CrimpTolMin_inch[i] = "0";
					  }
					  if (rs.GetValueAt(i, "CrimpTolMin_mm").IsNull() != true) {
						  Measurement fldMeas41 =
							  rs.GetValueAt(i, "CrimpTolMin_mm").GetMeasurement();
						  String fld41String =
							  measurementManager.GetString(
								  fldMeas41.GetValue(),
								  0,
								  0,
								  rs.GetFieldAt("CrimpTolMin_mm").GetDecimalPlaces(),
								  false,
								  -1);
						CrimpTolMin_mm[i] = fld41String.replaceAll(",", ".");
						  fld41Float = Float.valueOf(fld41String).floatValue();
					  } else {
						CrimpTolMin_mm[i] = "0";
					  }
					  if (rs.GetValueAt(i, "Rebounding_inch").IsNull() != true) {
						  Measurement fldMeas42 =
							  rs.GetValueAt(i, "Rebounding_inch").GetMeasurement();
						  String fld42String =
							  measurementManager.GetString(
								  fldMeas42.GetValue(),
								  0,
								  0,
								  rs.GetFieldAt("Rebounding_inch").GetDecimalPlaces(),
								  false,
								  -1);
						Rebounding_inch[i] = fld42String.replaceAll(",", ".");
						  fld42Float = Float.valueOf(fld42String).floatValue();
					  } else {
						Rebounding_inch[i] = "0";
					  }
					  if (rs.GetValueAt(i, "Rebounding_mm").IsNull() != true) {
						  Measurement fldMeas43 =
							  rs.GetValueAt(i, "Rebounding_mm").GetMeasurement();
						  String fld43String =
							  measurementManager.GetString(
								  fldMeas43.GetValue(),
								  0,
								  0,
								  rs.GetFieldAt("Rebounding_mm").GetDecimalPlaces(),
								  false,
								  -1);
						Rebounding_mm[i] = fld43String.replaceAll(",", ".");
						  fld43Float = Float.valueOf(fld43String).floatValue();
					  } else {
						Rebounding_mm[i] = "0";
					  }
					  if (rs.GetValueAt(i, "Bending_Radius_inch").IsNull() != true) {
						  Measurement fldMeas44 =
							  rs
								  .GetValueAt(i, "Bending_Radius_inch")
								  .GetMeasurement();
						  String fld44String =
							  measurementManager.GetString(
								  fldMeas44.GetValue(),
								  0,
								  0,
								  rs
									  .GetFieldAt("Bending_Radius_inch")
									  .GetDecimalPlaces(),
								  false,
								  -1);
						Bending_Radius_inch[i] = fld44String.replaceAll(",", ".");
						  fld44Float = Float.valueOf(fld44String).floatValue();
					  } else {
						Bending_Radius_inch[i] = "0";
					  }
					  if (rs.GetValueAt(i, "Bending_Radius_mm").IsNull() != true) {
						  Measurement fldMeas45 =
							  rs.GetValueAt(i, "Bending_Radius_mm").GetMeasurement();
						  String fld45String =
							  measurementManager.GetString(
								  fldMeas45.GetValue(),
								  0,
								  0,
								  rs
									  .GetFieldAt("Bending_Radius_mm")
									  .GetDecimalPlaces(),
								  false,
								  -1);
						Bending_Radius_mm[i] = fld45String.replaceAll(",", ".");
						  fld45Float = Float.valueOf(fld45String).floatValue();
					  } else {
						Bending_Radius_mm[i] = "0";
					  }
					  if (rs.GetValueAt(i, "Temperature_C").IsNull() != true) {
						  Measurement fldMeas46 =
							  rs.GetValueAt(i, "Temperature_C").GetMeasurement();
						  String fld46String =
							  measurementManager.GetString(
								  fldMeas46.GetValue(),
								  0,
								  0,
								  rs.GetFieldAt("Temperature_C").GetDecimalPlaces(),
								  false,
								  -1);
						Temperature_C[i] = fld46String.replaceAll(",", ".");
						  fld46Float = Float.valueOf(fld46String).floatValue();
					  } else {
						Temperature_C[i] = "0";
					  }
					  if (rs.GetValueAt(i, "Temperature_K").IsNull() != true) {
						  Measurement fldMeas47 =
							  rs.GetValueAt(i, "Temperature_K").GetMeasurement();
						  String fld47String =
							  measurementManager.GetString(
								  fldMeas47.GetValue(),
								  0,
								  0,
								  rs.GetFieldAt("Temperature_K").GetDecimalPlaces(),
								  false,
								  -1);
						Temperature_K[i] = fld47String.replaceAll(",", ".");
						  fld47Float = Float.valueOf(fld47String).floatValue();
					  } else {
						Temperature_K[i] = "0";
					  }
					  if (rs.GetValueAt(i, "Duration_cycles").IsNull() != true) {
						Duration_cycles[i] =
							  rs.GetValueAt(i, "Duration_cycles").GetStringValue();
					  } else {
						Duration_cycles[i] = "";
					  }
					  if (rs.GetValueAt(i, "Condition").IsNull() != true) {
						Condition[i] =
							  rs.GetValueAt(i, "Condition").GetStringValue();
					  } else {
						Condition[i] = "";
					  }
					  if (rs.GetValueAt(i, "Safety_Factor").IsNull() != true) {
						Safety_Factor[i] =
							  rs.GetValueAt(i, "Safety_Factor").GetStringValue();
					  } else {
						Safety_Factor[i] = "";
					  }
					  if (rs.GetValueAt(i, "Public").IsNull() != true) {
						Public[i] = rs.GetValueAt(i, "Public").GetStringValue();
					  } else {
						Public[i] = "";
					  }
					  if (rs.GetValueAt(i, "Crimp_DIE").IsNull() != true) {
						Crimp_DIE[i] =
							  rs.GetValueAt(i, "Crimp_DIE").GetStringValue();
					  } else {
						Crimp_DIE[i] = "";
					  }

					  
				  }
			for (int i = 0; i < rs.GetRecordCount(); i++){
			
					items = items + "<items>" +
								 "<Family>" + Family[i] + "</Family>" +
								 "<version>" + version[i] + "</version>" +
								 "<version_date>" + version_date[i] + "</version_date>" +
				"<Crimp_Version>" + Crimp_Version[i] + "</Crimp_Version>" +
				"<Description>" + Description[i] + "<Description>" +
				"<Technical_Product>" + Technical_Product[i] + "</Technical_Product>" +
				"<DN>" + DN[i] + "</DN>" +
				"<Insert>" + Insert[i] + "</Insert>" +
				"<Ferrule>" + Ferrule[i] + "</Ferrule>" +
				"<skive_solution>" + skive_solution[i] + "</skive_solution>" +
				"<Int_Skive_mm>" + Int_Skive_mm[i] + "</Int_Skive_mm>" +
				"<Int_Skive_inch>" + Int_Skive_inch[i] + "</Int_Skive_inch>" +
				"<Ext_Skive_mm>" + Ext_Skive_mm[i] + "</Ext_Skive_mm>" +
				"<Ext_Skive_inch>" + Ext_Skive_inch[i] + "</Ext_Skive_inch>" +
				"<Crimping_Diameter_mm>" + Crimping_Diameter_mm[i] + "</Crimping_Diameter_mm>" +
				"<Crimping_Diameter_inch>" + Crimping_Diameter_inch[i] + "</Crimping_Diameter_inch>" +
				"<General>" + General[i] + "</General>" +
				"<Skive_Toll>" + Skive_Toll[i] + "</Skive_Toll>" +
				"<Crimp_Toll>" + Crimp_Toll[i] + "</Crimp_Toll>" +
				"<Remarks>" + Remarks[i] + "</Remarks>" +
				"<Released_by>" + Released_by[i] + "</Released_by>" +
				"<Notes>" + Notes[i] + "</Notes>" +
				"<sort_criteria>" + sort_criteria[i] + "</sort_criteria>" +
				"<Ferrule_phase>" + Ferrule_phase[i] + "</Ferrule_phase>" +
				"<IntSkiveTolMax_inch>" + IntSkiveTolMax_inch[i] + "</IntSkiveTolMax_inch>" +
				"<IntSkiveTolMax_mm>" + IntSkiveTolMax_mm[i] + "</IntSkiveTolMax_mm>" +
				"<IntSkiveTolMin_inch>" + IntSkiveTolMin_inch[i] + "</IntSkiveTolMin_inch>" +
				"<IntSkiveTolMin_mm>" + IntSkiveTolMin_mm[i] + "</IntSkiveTolMin_mm>" +
				"<ExtSkiveTolMax_inch>" + ExtSkiveTolMax_inch[i] + "</ExtSkiveTolMax_inch>" +
				"<ExtSkiveTolMax_mm>" + ExtSkiveTolMax_mm[i] + "</ExtSkiveTolMax_mm>" +
				"<ExtSkiveTolMin_inch>" + ExtSkiveTolMin_inch[i] + "</ExtSkiveTolMin_inch>" +
				"<ExtSkiveTolMin_mm>" + ExtSkiveTolMin_mm[i] + "<ExtSkiveTolMin_mm>" +
				"<CrimpTolMax_inch>" + CrimpTolMax_inch[i] + "</CrimpTolMax_inch>" +
				"<CrimpTolMax_mm>" + CrimpTolMax_mm[i] + "</CrimpTolMax_mm>" +
				"<CrimpTolMin_inch>" + CrimpTolMin_inch[i] + "</CrimpTolMin_inch>" +
				"<CrimpTolMin_mm>" + CrimpTolMin_mm[i] + "</CrimpTolMin_mm>" +
				"<Rebounding_inch>" + Rebounding_inch[i] + "</Rebounding_inch>" +
				"<Rebounding_mm>" + Rebounding_mm[i] + "</Rebounding_mm>" +
				"<Bending_Radius_inch>" + Bending_Radius_inch[i] + "</Bending_Radius_inch>" +
				"<Bending_Radius_mm>" + Bending_Radius_mm[i] + "</Bending_Radius_mm>" +
				"<Temperature_C>" + Temperature_C[i] + "</Temperature_C>" +
				"<Temperature_K>" + Temperature_K[i] + "</Temperature_K>" +
				"<Duration_cycles>" + Duration_cycles[i] + "</Duration_cycles>" +
				"<Condition>" + Condition[i] + "</Condition>" +
				"</Safety_Factor>" + Safety_Factor[i] + "</Safety_Factor>" +
				"<Public>" + Public[i] + "</Public>" +
				"<Crimp_DIE>" + Crimp_DIE[i] + "</Crimp_DIE>" +
				"</items>";
				
			}

			  } catch (StringException e) {
				  response.write("<BR> Exception -" + e.getStackTrace() + "</BR>");
				  e.getStackTrace();
			  }
			 return items;
	}
	/**
	 * @Metodo per costruire output xml
	 */
	public String setOutputMDM_ProdHoses(
		IPortalComponentRequest request,
		IPortalComponentResponse response,
		String s) {
		String items = "";
		ResultSetDefinition rsd_prod_hoses;
		rsd_prod_hoses = new ResultSetDefinition(paramTableHeader);
		rsd_prod_hoses.AddField("Name");
		rsd_prod_hoses.AddField("catalogue_subtitle");
		rsd_prod_hoses.AddField("catalogue_image");
		rsd_prod_hoses.AddField("key_performance");
		rsd_prod_hoses.AddField("main_application");
		rsd_prod_hoses.AddField("min_cont_serv_temp");
		rsd_prod_hoses.AddField("max_cont_serv_temp");
		rsd_prod_hoses.AddField("max_operating_temp");
		rsd_prod_hoses.AddField("min_external_temp");
		rsd_prod_hoses.AddField("recommended_fluids");
		rsd_prod_hoses.AddField("refrigerants");
		rsd_prod_hoses.AddField("lubrificants");
		rsd_prod_hoses.AddField("tube");
		rsd_prod_hoses.AddField("reinforcement");
		rsd_prod_hoses.AddField("cover");
		rsd_prod_hoses.AddField("applicable_specs");
		rsd_prod_hoses.AddField("type_approvals");
		rsd_prod_hoses.AddField("other_technical_notes");
		rsd_prod_hoses.AddField("standard_Packaging");
		rsd_prod_hoses.AddField("catalogue_small_exclamation");
		rsd_prod_hoses.AddField("catalogue_big_exclamation");
		rsd_prod_hoses.AddField("tree");
		//			----          INSERT ML 30.01.2013
		rsd_prod_hoses.AddField("version");
		rsd_prod_hoses.AddField("Version_Date");
		rsd_prod_hoses.AddField("Applications_Fluids");
		rsd_prod_hoses.AddField("catalogue_bottom_exclamation");
					//			----          INSERT ML 30.01.2013
					// create an empty Search object
					Search search_prod_hoses = new Search(rsd_prod_hoses.GetTable());
					items = build_prodHoses(search_prod_hoses, rsd_prod_hoses, items);
					
			String Dyn_Par = convertXml(DynamicParameter);//.replaceAll("&","&amp;");
			String paramTab_Items = convertXml(paramTableItems);
			String paraValue = convertXml(paramValue);
			String paraCat = convertXml(paramCatalog);
			String paraFam = convertXml(paramFamily);
		//		Struttura Dati
		//		// ////////////////////////////////////////////////////////////		
		s =
//		"<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
//+ "<ns0:Adobe_Prodhoses xmlns:ns0=\"urn:manulihydraulic.com:Portal:Adobe\">"
//   + "<Header>"
//	  + "<paramTableItems/>"
//	  + "<paramValue/>"
//	  + "<paramCatalog/>"
//	  + "<paramFamily/>"
//	  + "<DynamicParameter/>"
//	  + "<recsMachine_size/>"
//	  + "<items>"
//		 + "<Name/>"
//		 + "<catalogue_subtitle/>"
//		 + "<catalogue_image/>"
//		 + "<key_performance/>"
//		 + "<main_application/>"
//		 + "<min_cont_serv_temp/>"
//		 + "<max_cont_serv_temp/>"
//		 + "<max_operating_temp/>"
//		 + "<min_external_temp/>"
//		 + "<recommended_fluids/>"
//		 + "<refrigerants/>"
//		 + "<lubrificants/>"
//		 + "<tube/>"
//		 + "<reinforcement/>"
//		 + "<cover/>"
//		 + "<applicable_specs/>"
//		 + "<type_approvals/>"
//		 + "<other_technical_notes/>"
//		 + "<standard_Packaging/>"
//		 + "<catalogue_small_exclamation/>"
//		 + "<catalogue_big_exclamation/>"
//		 + "<tree/>"
//		 + "<version/>"
//		 + "<Version_Date/>"
//		 + "<Applications_Fluids/>"
//		 + "<catalogue_bottom_exclamation/>"
//	  + "</items>"
//   + "</Header>"
//+ "</ns0:Adobe_Prodhoses>";

			"<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
				+ "<ns0:Adobe_Prodhoses xmlns:ns0=\"urn:manulihydraulic.com:Portal:Adobe\">"
				+ "<Header>"
				+ "<paramTableItems>" + paramTab_Items	+ "</paramTableItems>"
				+ "<paramValue>" + paraValue + "</paramValue>"
				+ "<paramCatalog>" + paraCat + "</paramCatalog>"
				+ "<paramFamily>" + paraFam + "</paramFamily>"
				+ "<DynamicParameter>" + Dyn_Par + "</DynamicParameter>"
				+ "<recsMachine_size>" + recsNew.size() + "</recsMachine_size>"
				+ items
				+ "</Header>"
				+ "</ns0:Adobe_Prodhoses>";
				
				//s = s.replaceAll("\n|\r", "");
//				response.write(s);
				return s;
	}

	/**
	 * @param DynamicParameter
	 * @return
	 */
	public String convertXml(String DynamicParameter) {
		// TODO Auto-generated method stub
		if(DynamicParameter.indexOf("\"") >= 0 ){
			DynamicParameter = DynamicParameter.replaceAll("\"" , "&quot;");
		}
		
		if(DynamicParameter.indexOf("'") >= 0 ){
			DynamicParameter = DynamicParameter.replaceAll("'" , "&apos");
		}
		
		if(DynamicParameter.indexOf("<") >= 0 ){
			DynamicParameter = DynamicParameter.replaceAll("<" , "&lt");
		}
		
		if(DynamicParameter.indexOf(">") >= 0){ 
			DynamicParameter = DynamicParameter.replaceAll(">" , "&gt");
		}
			
		if(DynamicParameter.indexOf("&") >= 0){ 
			DynamicParameter = DynamicParameter.replaceAll("&" , "&amp");	
		}
			
		
		return DynamicParameter;
	}
	/**
	 * @param search_prod_hoses
	 * @param rsd_prod_hoses
	 */
	public String build_prodHoses(Search search_prod_hoses, ResultSetDefinition rsd_prod_hoses, String items) {
		// TODO Auto-generated method stub
		try {
						A2iResultSet rs_H =
									catalogData.GetResultSet(
										search_prod_hoses,
										rsd_prod_hoses,
										null,
										true,
										0);
			String[] Name = new String[rs_H.GetRecordCount()];
	String[] catalogue_subtitle = new String[rs_H.GetRecordCount()];
	String[] catalogue_image = new String[rs_H.GetRecordCount()];
	String[] key_performance = new String[rs_H.GetRecordCount()];
	String[] main_application = new String[rs_H.GetRecordCount()];
	String[] min_cont_serv_temp = new String[rs_H.GetRecordCount()];
	String[] max_cont_serv_temp = new String[rs_H.GetRecordCount()];
	String[] max_operating_temp = new String[rs_H.GetRecordCount()];
	String[] min_external_temp = new String[rs_H.GetRecordCount()];
	String[] recommended_fluids = new String[rs_H.GetRecordCount()];
	String[] refrigerants = new String[rs_H.GetRecordCount()];
	String[] lubrificants = new String[rs_H.GetRecordCount()];
	String[] tube = new String[rs_H.GetRecordCount()];
	String[] reinforcement = new String[rs_H.GetRecordCount()];
	String[] cover = new String[rs_H.GetRecordCount()];
	String[] applicable_specs = new String[rs_H.GetRecordCount()];
	String[] type_approvals = new String[rs_H.GetRecordCount()];
	String[] other_technical_notes = new String[rs_H.GetRecordCount()];
	String[] standard_Packaging = new String[rs_H.GetRecordCount()];
	String[] catalogue_small_exclamation = new String[rs_H.GetRecordCount()];
	String[] catalogue_big_exclamation = new String[rs_H.GetRecordCount()];
	String[] tree = new String[rs_H.GetRecordCount()];
	String[] version = new String[rs_H.GetRecordCount()];
	String[] Version_Date = new String[rs_H.GetRecordCount()];
	String[] Applications_Fluids = new String[rs_H.GetRecordCount()];
	String[] catalogue_bottom_exclamation = new String[rs_H.GetRecordCount()];
				
	for (int i = 0; i < rs_H.GetRecordCount(); i++) {
		if (!rs_H.GetValueAt(i, "tree").IsNull()) {
			// H_Field00
			if (!rs_H.GetValueAt(i, "Name").IsNull()) {
				Name[i] = rs_H.GetValueAt(i, "Name").GetStringValue();
			} else {
				Name[i] = "";
			}
			// H_Field01
			if (!rs_H
				.GetValueAt(i, "catalogue_subtitle")
				.IsNull()) {
				catalogue_subtitle[i] =
					rs_H
						.GetValueAt(i, "catalogue_subtitle")
						.GetStringValue();
			} else {
				catalogue_subtitle[i] = "";
			}
			// H_Field02
			if (!rs_H.GetValueAt(i, "catalogue_image").IsNull()) {
				CatalogCache catalogCache = new CatalogCache();
				catalogCache.Init(
					CACHE_SERVER,
					CACHE_PORT,
					CACHE_USER,
					CACHE_PASSWORD,
					CACHE_DIRECTORY,
					"English [US]");

				Object id =
					rs_H.GetValueAt(i, "catalogue_image").GetData();
				int idx = id.hashCode();
				imagePath =
					catalogCache.GetImagePath(
						"Images",
						"Original",
						idx);
				catalogue_image[i] = imagePath;		
				catalogCache.Shutdown();
			} else {
				imagePath = null;
			}
			// H_Field03
			if (!rs_H.GetValueAt(i, "key_performance").IsNull()) {
				key_performance[i] =
					rs_H
						.GetValueAt(i, "key_performance")
						.GetStringValue();
			} else {
				key_performance[i] = "";
			}
			// H_Field04
			if (!rs_H.GetValueAt(i, "main_application").IsNull()) {
				main_application[i] =
					rs_H
						.GetValueAt(i, "main_application")
						.GetStringValue();
			} else {
				main_application[i] = "";
			}
			// H_Field05
			if (!rs_H
				.GetValueAt(i, "min_cont_serv_temp")
				.IsNull()) {
				min_cont_serv_temp[i] =
					rs_H
						.GetValueAt(i, "min_cont_serv_temp")
						.GetStringValue();
			} else {
				min_cont_serv_temp[i] = null;
			}
			// H_Field06
			if (!rs_H
				.GetValueAt(i, "max_cont_serv_temp")
				.IsNull()) {
				max_cont_serv_temp[i] =
					rs_H
						.GetValueAt(i, "max_cont_serv_temp")
						.GetStringValue();
			} else {
				max_cont_serv_temp[i] = "";
			}
			// H_Field07
			if (rs_H.GetValueAt(i, "max_operating_temp").IsNull()
				!= true) {
				max_operating_temp[i] =
					rs_H
						.GetValueAt(i, "max_operating_temp")
						.GetStringValue();
			} else {
				max_operating_temp[i] = "";
			}
			// H_Field08
			if (rs_H.GetValueAt(i, "min_external_temp").IsNull()
				!= true) {
				min_external_temp[i] =
					rs_H
						.GetValueAt(i, "min_external_temp")
						.GetStringValue();
			} else {
				min_external_temp[i] = "";
			}
			// H_Field09
			if (rs_H.GetValueAt(i, "recommended_fluids").IsNull()
				!= true) {
				recommended_fluids[i] =
					rs_H
						.GetValueAt(i, "recommended_fluids")
						.GetStringValue();
			} else {
				recommended_fluids[i] = "";
			}
			// H_Field10
			if (rs_H.GetValueAt(i, "refrigerants").IsNull()
				!= true) {
				refrigerants[i] =
					rs_H
						.GetValueAt(i, "refrigerants")
						.GetStringValue();
			} else {
				refrigerants[i] = "";
			}
			// H_Field11
			if (rs_H.GetValueAt(i, "lubrificants").IsNull()
				!= true) {
				lubrificants[i] =
					rs_H
						.GetValueAt(i, "lubrificants")
						.GetStringValue();
			} else {
				lubrificants[i] = "";
			}
			// H_Field12
			if (rs_H.GetValueAt(i, "tube").IsNull() != true) {
				tube[i] = rs_H.GetValueAt(i, "tube").GetStringValue();
			} else {
				tube[i] = "";
			}

			// H_Field13
			if (rs_H.GetValueAt(i, "reinforcement").IsNull()
				!= true) {
				reinforcement[i] =
					rs_H
						.GetValueAt(i, "reinforcement")
						.GetStringValue();
			} else {
				reinforcement[i] = "";
			}
			// H_Field14
			if (rs_H.GetValueAt(i, "cover").IsNull() != true) {
				cover[i] =
					rs_H.GetValueAt(i, "cover").GetStringValue();
			} else {
				cover[i] = "";
			}
			// H_Field15
			if (rs_H.GetValueAt(i, "applicable_specs").IsNull()
				!= true) {
				applicable_specs[i] =
					rs_H
						.GetValueAt(i, "applicable_specs")
						.GetStringValue();
			} else {
				applicable_specs[i] = "";
			}
			// H_Field16
			if (rs_H.GetValueAt(i, "type_approvals").IsNull()
				!= true) {
				type_approvals[i] =
					rs_H
						.GetValueAt(i, "type_approvals")
						.GetStringValue();
			} else {
				type_approvals[i] = "";
			}
			// H_Field17
			if (rs_H
				.GetValueAt(i, "other_technical_notes")
				.IsNull()
				!= true) {
				other_technical_notes[i] =
					rs_H
						.GetValueAt(i, "other_technical_notes")
						.GetStringValue();
			} else {
				other_technical_notes[i] = "";
			}
			// H_Field18
			if (rs_H.GetValueAt(i, "standard_Packaging").IsNull()
				!= true) {
				standard_Packaging[i] =
					rs_H
						.GetValueAt(i, "standard_Packaging")
						.GetStringValue();
			} else {
				standard_Packaging[i] = "";
			}
			// H_Field19
			if (rs_H
				.GetValueAt(i, "catalogue_small_exclamation")
				.IsNull()
				!= true) {
				catalogue_small_exclamation[i] =
					rs_H
						.GetValueAt(
							i,
							"catalogue_small_exclamation")
						.GetStringValue();
			} else {
				catalogue_small_exclamation[i] = "";
			}
			// H_Field20
			if (rs_H
				.GetValueAt(i, "catalogue_big_exclamation")
				.IsNull()
				!= true) {
				catalogue_big_exclamation[i] =
					rs_H
						.GetValueAt(i, "catalogue_big_exclamation")
						.GetStringValue();
			} else {
				catalogue_big_exclamation[i] = "";
			}
			//						----          INSERT ML 30.01.2013
			// H_Field22
			if (rs_H.GetValueAt(i, "version").IsNull() != true) {
				version[i] =
					rs_H.GetValueAt(i, "version").GetStringValue();
			} else {
				version[i] = "";
			}
			// H_Field23
			if (rs_H.GetValueAt(i, "Version_Date").IsNull()
				!= true) {
				String v_dateTime =
					rs_H
						.GetValueAt(i, "Version_Date")
						.GetSystemTime()
						.ConvertSystemTimeToString();
				version_date =
					v_dateTime.substring(8, 10)
						+ "/"
						+ v_dateTime.substring(5, 7)
						+ "/"
						+ v_dateTime.substring(0, 4);
			} else {
				version_date = "";
			}
			if (!rs_H
				.GetValueAt(i, "Applications_Fluids")
				.IsNull()) {
				Applications_Fluids[i] =
					rs_H
						.GetValueAt(i, "Applications_Fluids")
						.GetStringValue();
			} else {
				Applications_Fluids[i] = "";
			}
			// H_Field25
			if (rs_H
				.GetValueAt(i, "catalogue_bottom_exclamation")
				.IsNull()
				!= true) {
				catalogue_bottom_exclamation[i] =
					rs_H
						.GetValueAt(
							i,
							"catalogue_bottom_exclamation")
						.GetStringValue();
			} else {
				catalogue_bottom_exclamation[i] = "";
			}

			//						----          INSERT ML 30.01.2013
		}
	}
	Name = convertXML(Name);
					catalogue_subtitle = convertXML(catalogue_subtitle);
					catalogue_image = convertXML(catalogue_image);
					key_performance = convertXML(key_performance);
					main_application = convertXML(main_application);
					min_cont_serv_temp = convertXML(min_cont_serv_temp);
					max_cont_serv_temp = convertXML(max_cont_serv_temp);
					max_operating_temp = convertXML(max_operating_temp);
					min_external_temp = convertXML(min_external_temp);
					recommended_fluids = convertXML(recommended_fluids);
					refrigerants = convertXML(refrigerants);
					lubrificants = convertXML(lubrificants);
					tube = convertXML(tube);
					reinforcement = convertXML(reinforcement);
					cover = convertXML(cover);
					applicable_specs = convertXML(applicable_specs);
					type_approvals = convertXML(type_approvals);
					other_technical_notes = convertXML(other_technical_notes);
					standard_Packaging = convertXML(standard_Packaging);
					catalogue_small_exclamation = convertXML(catalogue_small_exclamation);
					catalogue_big_exclamation = convertXML(catalogue_big_exclamation);
					tree = convertXML(tree);
					version = convertXML(version);
					Version_Date = convertXML(Version_Date);
					Applications_Fluids = convertXML(Applications_Fluids);
					catalogue_bottom_exclamation = convertXML(catalogue_bottom_exclamation);
			for(int i = 0; i < rs_H.GetRecordCount(); i++){
				
				
				items = items + "<items>" +
					 "<Name>" + Name[i] + "</Name>" + 
					 "<catalogue_subtitle>" + catalogue_subtitle[i] + "</catalogue_subtitle>" +
					 "<catalogue_image>" + catalogue_image[i] + "</catalogue_image>"  +
					 "<key_performance>" + key_performance[i] + "</key_performance>"  +
					 "<main_application>" + main_application[i] + "</main_application>" +
					 "<min_cont_serv_temp>" + min_cont_serv_temp[i] + "</min_cont_serv_temp>" +  
					 "<max_cont_serv_temp>" + max_cont_serv_temp[i] + "</max_cont_serv_temp>" +
					 "<max_operating_temp>" + max_operating_temp[i] + "</max_operating_temp>" +
					 "<min_external_temp>" + min_external_temp[i] + "</min_external_temp>" +
					 "<recommended_fluids>" + recommended_fluids[i] + "</recommended_fluids>" + 
					 "<refrigerants>" + refrigerants[i] + "</refrigerants>" +
					 "<lubrificants>" + lubrificants[i] + "</lubrificants>" +
					 "<tube>" + tube[i] + "</tube>" +
					 "<reinforcement>" + reinforcement[i] + "</reinforcement>" +
					 "<cover>" + cover[i] + "</cover>" +
					 "<applicable_specs>" + applicable_specs[i] + "</applicable_specs>" +
					 "<type_approvals>" + type_approvals[i] + "</type_approvals>" +
					 "<other_technical_notes>" + other_technical_notes[i] + "</other_technical_notes>" +
					 "<standard_Packaging>" + standard_Packaging[i] + "</standard_Packaging>" +
					 "<catalogue_small_exclamation>" + catalogue_small_exclamation[i] + "</catalogue_small_exclamation>" +
					 "<catalogue_big_exclamation>" + catalogue_big_exclamation[i] + "</catalogue_big_exclamation>" +
					 "<tree>" + tree[i] + "</tree>" +
					 "<version>" + version[i] + "</version>" + 
					 "<Version_Date>" + Version_Date[i] + "</Version_Date>" +
					 "<Applications_Fluids>" + Applications_Fluids[i] + "</Applications_Fluids>" +
					 "<catalogue_bottom_exclamation>" + catalogue_bottom_exclamation[i] + "</catalogue_bottom_exclamation>" +
				"</items>";
			}
		}			
							catch (StringException e) {
								e.printStackTrace();
							}	
							return items;
									
	}
	/**
	 * @param catalogue_bottom_exclamation
	 * @return
	 */
	public String[] convertXML(String[] DynamicParameter) {
		// TODO Auto-generated method stub
		for(int i = 0; i < DynamicParameter.length; i++){
			DynamicParameter[i] = convertXml(DynamicParameter[i]);
		}
		
	
		return DynamicParameter;
	}
	//	-------------- Login MDM Server -------------------------------------------
	public void mdmGetDataOutput(
		IPortalComponentRequest request,
		IPortalComponentResponse response) {
		mdmGetWatermark(request, response);
		//		// ////////////////////////////////////////////////////////////		
		response.write("DynamicParameter = -" + DynamicParameter + "-<BR>");
		response.write("paramTableHeader = -" + paramTableHeader + "-<BR>");
		response.write("paramTableItems = -" + paramTableItems + "-<BR>");
		response.write("paramType = -" + paramType + "-<BR>");
		response.write("paramValue = -" + paramValue + "-<BR>");
		response.write("paramBrand = -" + paramBrand + "-<BR>");
		response.write("paramCatalog = -" + paramCatalog + "-<BR>");
		response.write("paramCategory = -" + paramCategory + "-<BR>");
		response.write("Name: " + Name + "-<BR>");

		response.write("<BR>" + "HEADER DATA" + "<BR>");
		response.write("catalogue_subtitle: " + catalogue_subtitle + "<BR>");
		response.write("catalogue_image: " + imagePath + "<BR>");
		response.write("key_performance: " + key_performance + "<BR>");
		response.write("main_application: " + main_application + "<BR>");
		response.write("min_cont_serv_temp: " + min_cont_serv_temp + "<BR>");
		response.write("max_cont_serv_temp: " + max_cont_serv_temp + "<BR>");
		response.write("max_operating_temp: " + max_operating_temp + "<BR>");
		response.write("min_external_temp: " + min_external_temp + "<BR>");
		response.write("recommended_fluids: " + recommended_fluids + "<BR>");
		response.write("refrigerants: " + refrigerants + "<BR>");
		response.write("lubrificants: " + lubrificants + "<BR>");
		response.write("reinforcement: " + reinforcement + "<BR>");
		response.write("cover: " + cover + "<BR>");
		response.write("applicable_specs: " + applicable_specs + "<BR>");
		response.write("type_approvals: " + type_approvals + "<BR>");
		response.write(
			"other_technical_notes: " + other_technical_notes + "<BR>");
		response.write("standard_Packaging: " + standard_Packaging + "<BR>");
		response.write(
			"catalogue_small_exclamation: "
				+ catalogue_small_exclamation
				+ "<BR>");
		response.write(
			"catalogue_big_exclamation: " + catalogue_big_exclamation + "<BR>");
		response.write("item rec   : " + recs.size() + "<BR>");
		response.write("item recnew: " + recsNew.size() + "<BR>");

		if (paramType.equals("1") || paramType.equals("2")) {
			for (int i = 0; i < recsNew.size(); i++) {
				response.write(
					"rec: "
						+ i
						+ " "
						+ ((prod_hoses_rec) recsNew.get(i)).getField01()
						+ "---"
						+ ((prod_hoses_rec) recsNew.get(i)).getField02()
						+ "---"
						+ ((prod_hoses_rec) recsNew.get(i)).getField03()
						+ "---"
						+ ((prod_hoses_rec) recsNew.get(i)).getField04()
						+ "---"
						+ ((prod_hoses_rec) recsNew.get(i)).getField05()
						+ "---"
						+ ((prod_hoses_rec) recsNew.get(i)).getField06()
						+ "---"
						+ ((prod_hoses_rec) recsNew.get(i)).getField07()
						+ "---"
						+ ((prod_hoses_rec) recsNew.get(i)).getField08()
						+ "---"
						+ ((prod_hoses_rec) recsNew.get(i)).getField09()
						+ "---"
						+ ((prod_hoses_rec) recsNew.get(i)).getField10()
						+ "---"
						+ ((prod_hoses_rec) recsNew.get(i)).getField11()
						+ "---"
						+ ((prod_hoses_rec) recsNew.get(i)).getField12()
						+ "---"
						+ ((prod_hoses_rec) recsNew.get(i)).getField13()
						+ "---"
						+ ((prod_hoses_rec) recsNew.get(i)).getField14()
						+ "---"
						+ ((prod_hoses_rec) recsNew.get(i)).getField15()
						+ "---"
						+ ((prod_hoses_rec) recsNew.get(i)).getField16()
						+ "---"
						+ ((prod_hoses_rec) recsNew.get(i)).getField17()
						+ "---"
						+ ((prod_hoses_rec) recsNew.get(i)).getField18()
						+ "---"
						+ ((prod_hoses_rec) recsNew.get(i)).getField19()
						+ "---"
						+ "<BR>");
			}
		} else {
			for (int i = 0; i < recsNew.size(); i++) {
				response
					.write(
						"rec: "
						+ i
						+ " "
						+ ((prod_hoses_rec_marcature) recsNew.get(i))
							.getField01()
						+ "---"
				//						+ ((prod_hoses_rec_marcature)recsNew.get(i)).getField02() + "---"
				//						+ ((prod_hoses_rec_marcature)recsNew.get(i)).getField03() + "---"
				//						+ ((prod_hoses_rec_marcature)recsNew.get(i)).getField04() + "---"
				//						+ ((prod_hoses_rec_marcature)recsNew.get(i)).getField05() + "---"
				//						+ ((prod_hoses_rec_marcature)recsNew.get(i)).getField06() + "---"
				//						+ ((prod_hoses_rec_marcature)recsNew.get(i)).getField07() + "---"
				+ ((prod_hoses_rec_marcature) recsNew.get(i)).getField08()
					+ "---"
					+ ((prod_hoses_rec_marcature) recsNew.get(i)).getField09()
					+ "---"
					+ ((prod_hoses_rec_marcature) recsNew.get(i)).getField10()
					+ "---"
					+ ((prod_hoses_rec_marcature) recsNew.get(i)).getField11()
					+ "---"
				//						+ ((prod_hoses_rec_marcature)recsNew.get(i)).getField12() + "---"
				+"<BR>");

			}

		}
	}
	public void mdmGetDataMachineOutput(
		IPortalComponentRequest request,
		IPortalComponentResponse response) {
		response.write("<BR>paramTableItems: " + paramTableItems + "</BR>");
		response.write("<BR>paramValue: " + paramValue + "</BR>");
		response.write("<BR>paramCatalog: " + paramCatalog + "</BR>");
		response.write("<BR>paramFamily: " + paramFamily + "</BR>");
		response.write("<BR>DynamicParameter: " + DynamicParameter + "</BR>");
		response.write("<BR>recsMachine.size: " + recsNew.size() + "</BR>");
		response.write(
			"<BR>-----------------------------  START  -------------------------</BR>");
		for (int i = 0; i < recsNew.size(); i++) {
			response.write("<BR>PROD_MACHINE       -   "
			//						+ i
			//						+ " 00="
			//						+ ((prod_machine_rec) recsMachineRecNew.get(i)).getField00()
			+" 01=" + ((prod_machine) recsNew.get(i)).getField01()
			//						+ " 02="
			//						+ ((prod_machine_rec) recsMachineRecNew.get(i)).getField02()
			//						+ " 03="
			//						+ ((prod_machine_rec) recsMachineRecNew.get(i)).getField03()
			//						+ " 04="
			//						+ ((prod_machine_rec) recsMachineRecNew.get(i)).getField04()
			//						+ " 05="
			//						+ ((prod_machine_rec) recsMachineRecNew.get(i)).getField05()
			//						+ " 06="
			//						+ ((prod_machine_rec) recsMachineRecNew.get(i)).getField06()
			+"</BR>");

			for (int j = 0; j < recsMachineRecNew.size(); j++) {
				if (((prod_machine_rec) recsMachineRecNew.get(j))
					.getField01()
					.equals(((prod_machine) recsNew.get(i)).getField01())) {
					response
						.write("<BR>----------------- AVAILABLE MACHINES   -   "
					//					+ i
					//					+ " 00="
					//					+ ((prod_machine_rec) recsMachineRecNew.get(i)).getField00()
					+" 01="
						+ ((prod_machine_rec) recsMachineRecNew.get(j))
							.getField01()
						+ " 02="
						+ ((prod_machine_rec) recsMachineRecNew.get(i))
							.getField02()
					//					+ " 03="
					//					+ ((prod_machine_rec) recsMachineRecNew.get(i)).getField03()
					//					+ " 04="
					//					+ ((prod_machine_rec) recsMachineRecNew.get(i)).getField04()
					//					+ " 05="
					//					+ ((prod_machine_rec) recsMachineRecNew.get(i)).getField05()
					//					+ " 06="
					//					+ ((prod_machine_rec) recsMachineRecNew.get(i)).getField06()
					+"</BR>");
				}
			}
			//			for (int j = 0; j < recsStandardNew.size(); j++) {
			//				if (((m_standard) recsStandardNew.get(j))
			//					.getField01()
			//					.equals(
			//						((prod_machine) recsNew.get(i))
			//							.getField01())) {
			//					response.write(
			//						"<BR>m_standard rec: "
			//							+ j
			//							+ " 01="
			//							+ ((m_standard) recsStandardNew.get(j)).getField01()
			//							+ " 02="
			//							+ ((m_standard) recsStandardNew.get(j)).getField02()
			//							+ " 03="
			//							+ ((m_standard) recsStandardNew.get(j)).getField03()
			//							+ " 04="
			//							+ ((m_standard) recsStandardNew.get(j)).getField04()
			//							+ " 05="
			//							+ ((m_standard) recsStandardNew.get(j)).getField05()
			//							+ " 06="
			//							+ ((m_standard) recsStandardNew.get(j)).getField06()
			//							+ " 07="
			//							+ ((m_standard) recsStandardNew.get(j)).getField07()
			//							+ "</BR>");
			//				}
			//			}
			//			for (int j = 0; j < recsOptionNew.size(); j++) {
			//				if (((prod_machacc_rec) recsOptionNew.get(j))
			//					.getField01()
			//					.equals(
			//						((prod_machine) recsNew.get(i))
			//							.getField01())) {
			//					response.write(
			//						"<BR>recsOptionNew rec : "
			//							+ j
			//							+ " 00="
			//							+ ((prod_machacc_rec) recsOptionNew.get(j))
			//								.getField00()
			//							+ " 01="
			//							+ ((prod_machacc_rec) recsOptionNew.get(j))
			//								.getField01()
			//							+ " 02="
			//							+ ((prod_machacc_rec) recsOptionNew.get(j))
			//								.getField02()
			//							+ " 03="
			//							+ ((prod_machacc_rec) recsOptionNew.get(j))
			//								.getField03()
			//							+ " 04="
			//							+ ((prod_machacc_rec) recsOptionNew.get(j))
			//								.getField04()
			//							+ " 05="
			//							+ ((prod_machacc_rec) recsOptionNew.get(j))
			//								.getField05()
			//							+ " 06="
			//							+ ((prod_machacc_rec) recsOptionNew.get(j))
			//								.getField06()
			//							+ " 07="
			//							+ ((prod_machacc_rec) recsOptionNew.get(j))
			//								.getField07()
			//							+ "</BR>");
			//				}
			//			}
			for (int j = 0; j < recsDieSetsNew.size(); j++) {
				if (((prod_machacc_rec) recsDieSetsNew.get(j))
					.getField01()
					.equals(((prod_machine) recsNew.get(i)).getField01())) {
					response
						.write("<BR>----------------- AVAILABLE DIE SETS   -   "
					//							+ j
					//							+ " 00="
					//							+ ((prod_machacc_rec) recsDieSetsNew.get(j))
					//								.getField00()
					+" 01="
						+ ((prod_machacc_rec) recsDieSetsNew.get(j)).getField01()
						+ " 02="
						+ ((prod_machacc_rec) recsDieSetsNew.get(j)).getField02()
					//							+ " 03="
					//							+ ((prod_machacc_rec) recsDieSetsNew.get(j))
					//								.getField03()
					//							+ " 04="
					//							+ ((prod_machacc_rec) recsDieSetsNew.get(j))
					//								.getField04()
					//							+ " 05="
					//							+ ((prod_machacc_rec) recsDieSetsNew.get(j))
					//								.getField05()
					//							+ " 06="
					//							+ ((prod_machacc_rec) recsDieSetsNew.get(j))
					//								.getField06()
					//							+ " 07="
					//							+ ((prod_machacc_rec) recsDieSetsNew.get(j))
					//								.getField07()
					+"</BR>");
				}
			}
			for (int j = 0; j < recsAvailAccNew.size(); j++) {
				if (((prod_machacc_rec) recsAvailAccNew.get(j))
					.getField01()
					.equals(((prod_machine) recsNew.get(i)).getField01())) {
					response
						.write("<BR>----------------- AVAILABLE ACCESSORIES   -   "
					//							+ j
					//							+ " 00="
					//							+ ((prod_machacc_rec) recsDieSetsNew.get(j))
					//								.getField00()
					+" 01="
						+ ((prod_machacc_rec) recsAvailAccNew.get(j))
							.getField01()
						+ " 02="
						+ ((prod_machacc_rec) recsAvailAccNew.get(j))
							.getField02()
					//							+ " 03="
					//							+ ((prod_machacc_rec) recsDieSetsNew.get(j))
					//								.getField03()
					//							+ " 04="
					//							+ ((prod_machacc_rec) recsDieSetsNew.get(j))
					//								.getField04()
					//							+ " 05="
					//							+ ((prod_machacc_rec) recsDieSetsNew.get(j))
					//								.getField05()
					//							+ " 06="
					//							+ ((prod_machacc_rec) recsDieSetsNew.get(j))
					//								.getField06()
					//							+ " 07="
					//							+ ((prod_machacc_rec) recsDieSetsNew.get(j))
					//								.getField07()
					+"</BR>");
				}
			}
			//			for (int j = 0; j < recsSelection.size(); j++) {
			//				if (((prod_machine_selection) recsSelection.get(j))
			//					.getField01()
			//					.equals(
			//						((prod_machine) recsNew.get(i))
			//							.getField01())) {
			//					response.write(
			//						"<BR>prod_machine_selection rec : "
			//							+ j
			//							+ " 01="
			//							+ ((prod_machine_selection) recsSelection.get(j))
			//								.getField01()
			//							+ " 02="
			//							+ ((prod_machine_selection) recsSelection.get(j))
			//								.getField02()
			//							+ " 03="
			//							+ ((prod_machine_selection) recsSelection.get(j))
			//								.getField03()
			//							+ " 04="
			//							+ ((prod_machine_selection) recsSelection.get(j))
			//								.getField04()
			//							+ " 05="
			//							+ ((prod_machine_selection) recsSelection.get(j))
			//								.getField05()
			//							+ "</BR>");
			//				}
			//			}

		}
	}

}
