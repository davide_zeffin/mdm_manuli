/*
 * Created on 30-giu-2014
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */

/**
 * @author Administrator
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
import javax.swing.*;
import java.awt.event.*;

public class ShowDialogBox {
	JFrame frame;
	public static void main(String[] args) {
		ShowDialogBox db = new ShowDialogBox();
	}

	public ShowDialogBox() {
		frame = new JFrame("Show Message Dialog");
		JButton button = new JButton("Click Me");
		button.addActionListener(new MyAction());
		frame.add(button);
		frame.setSize(400, 400);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public class MyAction implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			JOptionPane.showMessageDialog(frame, "Roseindia.net");
		}
	}
}