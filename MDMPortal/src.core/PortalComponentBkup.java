import javax.naming.Context;
import javax.naming.InitialContext;
//import java.util.Hashtable;

import a2i.common.A2iResultSet;
import a2i.common.CatalogData;
import a2i.common.ResultSetDefinition;
import a2i.search.Search;

import com.sapportals.connector.connection.IConnection;
import com.sapportals.connector.connection.IConnectionFactory;
import com.sapportals.connector.connection.IConnectionSpec;
import com.sapportals.connector.connection.INative;
//import com.sapportals.portal.pcd.gl.IPcdContext; 

import com.sapportals.portal.prt.component.*;

public class PortalComponentBkup extends AbstractPortalComponent
{
	
	public static void main (String []args){
	
	}
    public void doContent(IPortalComponentRequest request, IPortalComponentResponse response)
    {
		try {

			response.write("1.1 get the context :"); 
			Context ctx = new InitialContext();

			response.write("1.2 lookup in the JDNI to get the connection factory class");
			IConnectionFactory connectionFactory = (IConnectionFactory)ctx.lookup("deployedAdapters/MDMEFactory/shareable/MDMEFactory");

			response.write("1.3 Get Connection Spec to set connection properties");
			IConnectionSpec spec = connectionFactory.getConnectionSpec();

			response.write("1.4 Set Connection Properties");
			spec.setPropertyValue("UserName", "Admin");
			spec.setPropertyValue("Password", "admin");
			spec.setPropertyValue("Server", "10.194.72.24");
			spec.setPropertyValue("Port", "50000");
			spec.setPropertyValue("RepositoryLanguage", "English [EN]");
			response.write("1.4 Set Connection Properties");

			response.write("1.5 Get the Connection");
			IConnection connection = connectionFactory.getConnectionEx(spec);

//			Step 2 : obtain access to the native object

			response.write("2.1 Retrieve Native inteface");
			INative nativeInterface = connection.retrieveNative();

			response.write("2.2 Get the CatalogData the physical connection");
			CatalogData catalog = (CatalogData) nativeInterface.getNative(CatalogData.class.getName());

//			Step 3: Retrieve data from Catalog

			response.write("3.1 Create ResultSetDefinition for products table");
			ResultSetDefinition resultdefenition = new ResultSetDefinition("Categories");
			resultdefenition.AddField("Id");

			response.write("3.2 Create Search object for Categories table");
			Search search = new Search("Categories");

			response.write("3.3 Get Data from repository.");
			A2iResultSet rs = catalog.GetResultSet(search, resultdefenition, "Id", true, 0);

//			Once we have all the data in the result set, one can do manipulation on it and retrieve the data.

			response.write("Step 4: Close the connection");
			connection.close();



		}catch(Exception e){
			e.printStackTrace();
		}
	}

    }
