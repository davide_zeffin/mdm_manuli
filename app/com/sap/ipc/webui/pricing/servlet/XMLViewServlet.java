package com.sap.ipc.webui.pricing.servlet;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;
import java.util.Hashtable;
import java.util.Enumeration;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import javax.xml.transform.*;
import javax.xml.transform.stream.*;
import java.util.Properties;

public class XMLViewServlet extends HttpServlet {

	private static final String CONTENT_TYPE = "text/html";
	private static final String CHARSET_DEFAULT = "UTF-8";
	private static final String XMLAppliDefault = "CRM";
	private static final String DefaultId = "1";
	private static final String defaultTreeCSSStyleSheet = "CRM_tree_style.css";
	private static final String defaultShowCSSStyleSheet = "CRM_show_style.css";

	private static final String fileSeparator = System.getProperty("file.separator");


	// do have to be declared as member variables;
	StringWriter writer;
	NodeList nodeList;
	Document doc;


	int [] visi_List;  // Which tree line is visible /Welche Tree-Zeile ist sichtbar
	int [] level_List; // Which hierarchy level has node i /Welche Hierarchiestufe hat Knoten i

	String ServletName ;    //Servlet Name

	String charset;

	String servlet_path ;   //URI-path for Servlet-call
	String source_path;     // ="samples/analysis.xml";
	String styles_path;     //relative Path for the XSL-Files

	String styles;          //relative patch for the stylesheets /relativer URI_Pfad fuer die Stylesheets
	String uri_styles_path; //URI patch for the xsl files/URI_Pfad fuer die XSL-Files

	String ui;              //relative path for the UIrelativer URI_Pfad fuer die UI
	String uri_ui_path;     //URI path for the UI: e.g. html pages /URI_Pfad fuer UI: etwa HTML-Seiten

	String pictures;        //relative URI path/relativer URI-Pfad
	String pictures_path;   //URI path for the images/ URI-Pfad fuer die Bilder

	String xsl_style_tree;
	String xsl_style_show;

	String css_style_tree;
	String css_style_show;

	String returnaddress;


	/**Initialize global variables*/
	public void init(ServletConfig config) throws ServletException {
		ServletName = config.getServletName();
		source_path = config.getServletContext().getRealPath("/");
		int pos = source_path.length() - 1;
		if ( !source_path.substring(pos).equals("/") && !source_path.substring(pos).equals("\\") ) {
			source_path = source_path + System.getProperty("file.separator");
		}

		super.init(config);

		charset = getServletConfig().getInitParameter("charset");
		if ( charset == null ) {
			charset = CHARSET_DEFAULT;
		}

		styles_path = getServletConfig().getInitParameter("styles_path");
		if ( styles_path == null ) {
			//styles_path = "styles\\";
			styles_path = "styles" + fileSeparator;
		}

		styles = getServletConfig().getInitParameter("styles");
		if ( styles == null ) {
			// styles = "/styles/";
			styles = fileSeparator + "styles" + fileSeparator;
		}

		ui = getServletConfig().getInitParameter("ui");
		if ( ui == null ) {
			//ui = "/ui/";
			ui = fileSeparator + "ui" + fileSeparator;
		}

		pictures = getServletConfig().getInitParameter("pictures");
		if (pictures == null) {
			// pictures = "/mimes/images/tree/";
			pictures = fileSeparator + "mimes" + fileSeparator + "images" + fileSeparator + "tree" + fileSeparator;
		}
	}


	/**Process the HTTP Get request*/
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


		// Session-Context
		HttpSession thisSession = request.getSession(true);

		// URL-Parameter:
		// internal_call: parameter for the servlet control, if existing
		//then the servlet calls itself; contrary to beeing called from another object
		String internal_call = request.getParameter("internal_call");

		// change: parameter for the tree navigation: index for the line taht
		//should be collapsed or expanded
		String change   = request.getParameter("change");

		// counter: Parameter for response SHOW-HTML
		String counter  = request.getParameter("counter");

		// Parameter: documentId
		String documentId  = request.getParameter("documentId");
		if (documentId == null || documentId == "") documentId = DefaultId;

		// Parameter: itemId
		String itemId      = request.getParameter("itemId");
		if (itemId == null || itemId == "") itemId = DefaultId;

		// Parameter: unique identifier for the Document: xmlId = documentId + itemId
		String xmlId = documentId + itemId;;

		//Parameter: mark the last clicked line
		String detail = request.getParameter("detail");

		//Parameter: Servletinfo
		String info = request.getParameter("info");
		if ( (info== null) || (info=="") ) info = "false";
		thisSession.setAttribute(xmlId+".info",info);


		//Parameter: Return adress
		returnaddress = request.getParameter("returnAddress");

		// Response-Parameter
		String CONTENT_TYPE_CHARSET= new String(CONTENT_TYPE + "; charset=" + charset);
		response.setContentType(CONTENT_TYPE_CHARSET);
		response.setHeader("pragma", "no-cache");


		// Objects und Variables

		NodeList l_nodeList;
		Node node,attribute,parent,child;

		Integer I_length;
		Integer I_level_maxx;
		Integer I_visi;
		String  S_visi;
		Integer I_level;
		String  S_level;
		String l_s_lastDetail;


		int length;             // nodeList.getLength(); Number of tag elements of source document
		int l_visi_List[];      // local List of Visibility-Status of the tree lines
		int l_level_List[];     // local list of hierarchy level of a tree line
		int level_maxx;         // maximum depth of the table representation
		int i_next;             // which node to be displayed next in case the folder is closed
		int width_max = 1000;   // total width of hte table
		int width_td = 16;      // width of a column


		if (thisSession.getAttribute(xmlId+".XMLView.lastDetail") != null){
			l_s_lastDetail = (String)thisSession.getAttribute(xmlId+".XMLView.lastDetail");
		}
		else {
			l_s_lastDetail = new String();
		}

		if ((internal_call != null) && (counter != null) && (detail != null)){
			l_s_lastDetail = counter;
		}

		// construct frameset
		if(( internal_call == null ) && ( change == null ) && ( counter == null)) {


			// new session context, also in case the browser session is the old one
			String NewSession = (String)thisSession.getAttribute(xmlId+".XMLView.NewSession");

			if( NewSession != null ) {

				// thisSession.invalidate(); leads to difficulties with other servlets
				// clean up manually
				thisSession.removeAttribute(xmlId+".XMLView.XMLAppli");
				thisSession.removeAttribute(xmlId+".XMLView.XMLString");
				thisSession.removeAttribute(xmlId+".XMLView.XSLTree");
				thisSession.removeAttribute(xmlId+".XMLView.XSLShow");
				thisSession.removeAttribute(xmlId+".XMLView.CSSTree");
				thisSession.removeAttribute(xmlId+".XMLView.CSSShow");
				thisSession.removeAttribute(xmlId+".XMLView.treeNodeList");
				thisSession.removeAttribute(xmlId+".XMLView.sourceReader");
				thisSession.removeAttribute(xmlId+".XMLView.level_maxx");
				thisSession.removeAttribute(xmlId+".XMLView.lastDetail");

				I_length = (Integer)thisSession.getAttribute(xmlId+".XMLView.length");
				length = I_length.intValue();

				for( int j=0; j < length; j++ ) {
					// delete session values visi_List and therefore initialize
					S_visi = new String (xmlId+".XMLView.l_visi_" + j);
					thisSession.removeAttribute(S_visi);
				}

				for( int j=0; j < length; j++ ) {
					// delete session values visi_List and therefore initialize
					S_level = new String (xmlId+".XMLView.l_level_" + j);
					thisSession.removeAttribute(S_level);
				}

				thisSession.removeAttribute(xmlId+".XMLView.length");
				length = 0;

				NewSession = new String("true");
				thisSession.setAttribute(xmlId+".XMLView.NewSession",NewSession);
			}
			else {
				NewSession = new String("true");
				thisSession.setAttribute(xmlId+".XMLView.NewSession",NewSession);
			}

			PrintWriter out = response.getWriter();

			//one time initialization
			servlet_path = null;

			if ( request.getServletPath() != null ) {
				servlet_path = request.getServletPath();
			}

			if ( request.getContextPath() != null ) {
				pictures_path = request.getContextPath() + pictures;
				servlet_path  = request.getContextPath() +  servlet_path;
				uri_styles_path = request.getContextPath() + styles;
				uri_ui_path = request.getContextPath() + ui;
			}

			if ( !(( returnaddress == null )|| ( returnaddress.equals("") )) ) {
				out.println("<FRAMESET ROWS=\"*,100\" FRAMEBORDER=no BORDER=0 >");
			}

			out.println("<FRAMESET COLS=\"55%,45%\" FRAMEBORDER=yes >");
			out.println("<FRAME NAME=\"TREE\" SRC=" + servlet_path + "?documentId="+ documentId +"&itemId=" + itemId + "&internal_call=true&info=" + info +"&& FRAMEBORDER=yes >");
			out.println("<FRAME NAME=\"SHOW\" SRC=" + uri_ui_path + "DefaultShow.html FRAMEBORDER=yes >");
			out.println("</FRAMESET>");

			if ( !(( returnaddress == null )|| ( returnaddress.equals("") )) ) {
				out.println("<FRAME NAME=\"MENU\" SRC=" + uri_ui_path + "DefaultMenu.html?documentId="+ documentId +"&itemId=" + itemId + "&info=" + info + "&returnaddress=" + returnaddress + "%&& FRAMEBORDER=no SCROLLABLE=no>");
				out.println("</FRAMESET>");
			}
		}

		// build the frame tree or navigate within the frame tree
		if( ( internal_call != null ) && ( counter == null ) ) {

			PrintWriter out = response.getWriter();

			// A: new session

			String NewSession = (String)thisSession.getAttribute(xmlId+".XMLView.NewSession");
			
			if ( NewSession == null ) {
								NewSession = new String("true");
								thisSession.setAttribute(xmlId+".XMLView.NewSession",NewSession);
							}
			if( NewSession.equalsIgnoreCase("true") ) {
				// freshly to initialized objects per session

				// read context attributes
				// I. "XMLAppli"
				String XMLAppli = (String) getServletContext().getAttribute("XMLAppli");

				if( XMLAppli== null ) {
					XMLAppli = XMLAppliDefault;
					out.println("XMLAppli wasn't found in ServletContext, <BR>");
					out.println("the default value is used instead.<BR>");
					out.println("XMLAppli:" + XMLAppli + "<BR>");
				}

				// derive the css names from XMLAppli
				if ( XMLAppli != null ) {
					css_style_tree = XMLAppli + "_tree_style.css";
					css_style_show = XMLAppli + "_show_style.css";
				}
				else {
					css_style_tree = defaultTreeCSSStyleSheet;
					css_style_show = defaultShowCSSStyleSheet;
				}

				// derive xsl-stylesheet names + path from XMLAppli
				if(source_path == null) {
					out.println("Check Parameter: source_path  <BR>");
				}
				if(styles_path == null) {
					out.println("Check Parameter: styles_path  <BR>");
				}
				if( !(( source_path == null ) || ( styles_path == null )) ) {
					xsl_style_tree = source_path + styles_path  + XMLAppli + "_tree.xsl";
					xsl_style_show = source_path + styles_path  + XMLAppli + "_show.xsl";
				}

				// II. "XMLString" (The Source-XML document)
				String XMLString = (String) getServletContext().getAttribute("XMLString");
				if( XMLString == null ) {
					out.println("XMLAppli wasn't found in ServletContext. <BR>");
				}


				String l_info = (String)thisSession.getAttribute(xmlId+".info");
				if( l_info.equalsIgnoreCase("true")) {
					try {
						servlet_info("This is a new session", request, out, XMLString);
					}
					catch ( Exception err ) {
						out.println("Error in showing servlet_info:" + err.getMessage());
						err.printStackTrace();
					}
				}

				/* if( servlet_info ) {
					try {
						servlet_info("This is a new session", request, out, XMLString);
					}
					catch ( Exception err ) {
						out.println("Error in showing servlet_info:" + err.getMessage());
						err.printStackTrace();
					}
				} */

				// load into session context
				if (XMLAppli != null) {
					thisSession.setAttribute(xmlId+".XMLView.XMLAppli",XMLAppli);
				}
				if (XMLString  !=null) {
					thisSession.setAttribute(xmlId+".XMLView.XMLString",XMLString);
				}
				if (xsl_style_tree !=null) {
					thisSession.setAttribute(xmlId+".XMLView.XSLTree",xsl_style_tree);
				}
				if (xsl_style_show !=null) {
					thisSession.setAttribute(xmlId+".XMLView.XSLShow",xsl_style_show);
				}
				if (css_style_tree !=null) {
					thisSession.setAttribute(xmlId+".XMLView.CSSTree",css_style_tree);
				}
				if (css_style_show !=null) {
					thisSession.setAttribute(xmlId+".XMLView.CSSShow",css_style_show);
				}


				// XSLT-transformations
				// Part 1: create the tree xml file! (transformation only once per session)

				StringReader rsource = new StringReader(XMLString);
				String style = new String(xsl_style_tree);
				try {
					xslt_xml2xml(style, rsource, response);
				}
				catch ( TransformerException err ) {
					out.println("xslt_xml2xml: Error applying stylesheet: " + err.getMessage());
				}
				catch ( Exception err ) {
					out.println(err.getMessage());
					err.printStackTrace();
				}

				try {

					DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
					DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();

					StringReader sReader = new StringReader(new String(writer.toString()));
					InputSource inSource = new InputSource(sReader);

					doc = docBuilder.parse (inSource);

					if (doc == null) {
						out.println("Document couldn't be parsed correct. <BR> ");
					}
				}
				catch (SAXParseException err) {
					out.println ("** Parsing error"
						+ ", line " + err.getLineNumber()
						+ ", uri " + err.getSystemId());
					out.println("   " + err.getMessage());

					// print stack trace as below
				}
				catch (SAXException e) {
					Exception	x = e.getException ();
					((x == null) ? e : x).printStackTrace ();
				}
				catch (Throwable t) {
					t.printStackTrace ();
				} //try

				// definiation of the list of nodes
				nodeList  = doc.getElementsByTagName("*");
				length = nodeList.getLength();
				doc.getChildNodes();

				level_List = new int[length];

				// inistialization of visible list 
				for( int j=0; j< length;j++) {
					level_List[j] = 0;
				}

				// initialization hierarchy level
				parent = nodeList.item(0).getParentNode(); // #document-Object !

				int level = 0 ;
				int level_search = 0;
				int level_hist = 0;
					level_maxx = 0;

				boolean old_node = false;

				for (int i =0 ; i < length; i++) {

					//current node
					node = nodeList.item(i);

					// determination of hierarchy level
					if(!parent.equals(node.getParentNode())) {
						old_node = false;
						level_search = level;
						level_hist = level;

						// naviagate back through the tree: is there already a shared parent node
						for( int j= 0; j < level_search; j++) {
							if ( parent.equals(node.getParentNode())){
								level = level_hist;
								level_List[i] = level;
								old_node = true;
								break;
							}
							parent = parent.getParentNode();
							level_hist --;
						}
						// new hierarchy level
						if (!old_node) {
							parent = node.getParentNode();
							level++;
							level_List[i] = level;
						}
					}
					else {
						level_List[i] = level;
					}
					if ( level_List[i] > level_maxx ) {
						level_maxx = level_List[i];
					}
				} // end of nodeList i-loop

				visi_List = new int[length];

				for( int j=0; j< length;j++) {
					if ( level_List[j] == 1) {
						visi_List[j] = 1;
					}
					else {
						visi_List[j] = 2;
					}
				}

				visi_List[0] = 2; // root invisible

				// save values into the session context

				// constant per session: the DOM nodes of the TREE-xml files
				thisSession.setAttribute(xmlId+".XMLView.treeNodeList",nodeList);
				thisSession.setAttribute(xmlId+".XMLView.sourceReader",rsource);

				//constant per session: length of the source document (no nodes added)
				I_length = new Integer(length);
				thisSession.setAttribute(xmlId+".XMLView.length",I_length);

				// constant per session: max depth of tree hierarchy
				I_level_maxx = new Integer(level_maxx);
				thisSession.setAttribute(xmlId+".XMLView.level_maxx",I_level_maxx);

				// initialization of the arrays
				l_visi_List  = new int[length];
				l_level_List = new int[length];
				l_nodeList = nodeList;

				// constant per session: level_List (which hierarchy level has a tree line)
				for(int j=0; j < length; j++) {
					l_level_List[j] = level_List[j];
					I_level = new Integer(level_List[j]);
					S_level = new String (xmlId+".XMLView.l_level_" + j);
					thisSession.setAttribute(S_level,I_level);
				}

				// variable per session:  visi_List (Which tree lines are visible?)
				for(int j=0; j < length; j++) {
					l_visi_List[j] = visi_List[j];
					I_visi = new Integer(visi_List[j]);
					S_visi = new String (xmlId+".XMLView.l_visi_" + j);
					thisSession.setAttribute(S_visi,I_visi);
				}

				// manually
				NewSession = new String("false");
				thisSession.setAttribute(xmlId+".XMLView.NewSession",NewSession);

				//end of new session
			}
			else {

			//old session
			String l_info = (String)thisSession.getAttribute(xmlId+".info");

			if(l_info.equalsIgnoreCase("true")) {
				try {
					servlet_info("This is an old session", request, out, (String)thisSession.getAttribute(xmlId+".XMLView.XMLString"));
				}
				catch (Exception err) {
					out.println("Error in showing servlet_info:" + err.getMessage());
					err.printStackTrace();
				}
			}

			l_nodeList = (NodeList)thisSession.getAttribute(xmlId+".XMLView.treeNodeList");

			I_length = (Integer)thisSession.getAttribute(xmlId+".XMLView.length");
			length   = I_length.intValue();

			I_level_maxx = (Integer)thisSession.getAttribute(xmlId+".XMLView.level_maxx");
			level_maxx   = I_level_maxx.intValue();

			// initialization of arrays
			l_visi_List  = new int[length];
			l_level_List = new int[length];

			for(int j=0; j < length; j++) {
				// download of session values visi_List and initialization
				S_visi = new String (xmlId+".XMLView.l_visi_" + j);
				I_visi = (Integer)thisSession.getAttribute(S_visi);
				l_visi_List[j] = I_visi.intValue();
			}

			for(int j=0; j < length; j++) {
				// download of session values visi_List and initialization
				S_level = new String (xmlId+".XMLView.l_level_" + j);
				I_level = (Integer)thisSession.getAttribute(S_level);
				l_level_List[j] = I_level.intValue();
			}
		}


		// HTML-Document-Header
		out.println("<html>");
		out.println("<head>");
		out.println("<LINK REL=\"STYLESHEET\" TYPE=\"text/css\" HREF=\"" + uri_styles_path + css_style_tree + "\"></LINK>" );
		out.println("<title>" + ServletName + "</title>");
		out.println("</head>");
		// JavaScript Function: reset the background color of all lines
		out.println("<script language=\"JavaScript\">");
		out.println("<!--");
		if (l_s_lastDetail != null){
			out.println("       var id_clear = (\"KTH" + l_s_lastDetail + "\");");
		}
		else {
			out.println("       var id_clear = (\"KTH\" + 0);");
		}

		out.println("function SetAndClear(x)");
		out.println("{");
		out.println("      var id_set = (\"KTH\" + x);");
		out.println("      if(document.all[id_clear])");
		out.println("      {");
		out.println("         document.all[id_clear].style.backgroundColor = '';");
		out.println("      }");
		out.println("      document.all[id_set].style.backgroundColor = '#FFD700';");
		out.println("      id_clear = (\"KTH\" + x);");
		out.println("}");
		out.println("-->");
		out.println("</SCRIPT>");
		out.println("<body>");



		// update Tree 

		if (change != null) {

			int i_change = Integer.parseInt(change);

			int status_change = l_visi_List[i_change];

			//node was collapsed - going to be expanded
			if (status_change != 0) {
				l_visi_List[i_change] = 0;
				// Simple: show succeeding node
				for(int j=(i_change + 1); j < length; j++ ) {
					// nodes only changed if the tree is expanded the first time
					if(l_visi_List[j] == 2) {
						if(l_level_List[j] < ( l_level_List[i_change + 1 ])) {
							break;
						}
						if(l_level_List[j] > ( l_level_List[i_change] + 1)) {
							l_visi_List[j] = 2;
						}
						if(l_level_List[j] == ( l_level_List[i_change] + 1)) {
							l_visi_List[j] = 1;
						}
					}
				}
			}
			 //node was expanded - going to be collapsed
			 else {
				l_visi_List[i_change] = 1;
			}
		}
		// Session-Handling
		// save l_visi_List as session parameter
		// save of changed visi_List
		for(int j=0; j < length; j++) {
			I_visi = new Integer(l_visi_List[j]);
			S_visi = new String (xmlId+".XMLView.l_visi_" + j);
			thisSession.setAttribute(S_visi,I_visi);
		}

		out.println("<table id=\"tab\" style=\"background-color: \" width=\"" + width_max + "\">");
		for (int i =0 ; i < length; i++) {
			i_next = 0;
			//current node
			node = l_nodeList.item(i);

			// condition to Expand/collaps: show hierarchy level or not
				if(l_visi_List[i] != 2){
					int l_span;
					int l_width;
					l_span = I_level_maxx.intValue() - l_level_List[i] + 1;
					l_width = width_max - (l_level_List[i] * width_td);
					// construct tree line
					out.println("<tr ID=K" + i + " style=\"background-color: \">");

					// show hierarchy level indented from left margin
					for (int j =0; j < l_level_List[i]; j++) {
						out.println("<td width=\"" + width_td + "\"></td>");
					}

					// determine nodeNode or leafNode (!!path)
					if (node.hasChildNodes()) {
						out.println("<th id=\"KTH" + i + "\"colspan=\"" + l_span + "\" align=\"left\" width=\"" + l_width + "\"><nobr>");
						out.println("<a href=\"" + servlet_path + "?documentId="+ documentId +"&itemId=" + itemId + "&change="+ i + "&internal_call=true\">");
						// if expanded folder
						if (l_visi_List[i] == 0) {
							out.println("<img src=\"" + pictures_path + "folder_open.gif\" height=\"15\" width=\"" + width_td + "\" border=\"0\" ></img></a>");
						}
						// if collapsed folder
						if (l_visi_List[i] == 1) {
							out.println("<img src=\"" + pictures_path + "folder_close.gif\" height=\"15\" width=\"" + width_td + "\" border=\"0\"></img></a>");
							// search for next element at same hierarchy level of the collapsed folder
							for (int j = i+1; j < length; j++) {
								if (l_level_List[j] <= l_level_List[i]) {
									i_next = j;
									break;
								}
							
								// In case there is no other node at the same level
								// leave outer for - loop after this execution
								else if(j == length - 1) {
									i_next = length + 1;
								}
							}
						}
					}
					else {
						out.println("<th id=\"KTH" + i + "\" colspan=\"" + l_span + "\" align=\"left\" width=\"" + l_width + "\"><nobr><a href=\"" + servlet_path + "?documentId="+ documentId +"&itemId=" + itemId + "&internal_call=true&counter="+ i + "&detail=true\" target=\"SHOW\" onClick=\"SetAndClear(" + i + ");\"><img src=\"" + pictures_path + "doc.gif\" height=\"15\" width=\"" + width_td + "\" border=\"0\"></img></a>");
					}

					// read tree text
					String TreeText = null;
					String success = new String();
					String successTrue = "true";
					String successFalse = "false";

					for (int j =0 ; j < node.getAttributes().getLength(); j++) {
						attribute = node.getAttributes().item(j);
						if(attribute.getNodeName().equals("Success")) {
							success = (String)attribute.getNodeValue();
						}
						if(attribute.getNodeName().equals("TreeText")) {
							// out.println("<td>" + attribute.getNodeValue() + "</td>");
							TreeText = attribute.getNodeValue();
							TreeText.trim();
							if( TreeText.length() < 2) {
								TreeText = "There is no description available";
							}
						}
						else {
							TreeText = "There is no description available";
						}
					}

					if (success.equals(successTrue)) {
						//out.println("<td>Counter:" + i + "</td><td>Visi: " + l_visi_List[i] + "</td><td>Level: " + l_level_List[i] + "</td>");
						out.println("<img src=\"" + pictures_path + "s_s_ledg.gif\" height=\"15\" width=\"" + width_td + "\" border=\"0\" ></img>");

						out.println("<a href=\"" + servlet_path + "?documentId="+ documentId +"&itemId=" + itemId + "&internal_call=true&counter="+ i + "&detail=true\" target=\"SHOW\" onClick=\"SetAndClear(" + i + ");\">"  + TreeText + "</a></nobr>");
					}
					else if (success.equals(successFalse)) {
						//out.println("<td>Counter:" + i + "</td><td>Visi: " + l_visi_List[i] + "</td><td>Level: " + l_level_List[i] + "</td>");
						out.println("<img src=\"" + pictures_path + "s_s_ledr.gif\" height=\"15\" width=\"" + width_td + "\" border=\"0\" ></img>");

						out.println("<a href=\"" + servlet_path + "?documentId="+ documentId +"&itemId=" + itemId + "&internal_call=true&counter="+ i + "&detail=true\" target=\"SHOW\" onClick=\"SetAndClear(" + i + ");\">" + TreeText + "</a></nobr>");
					}
					else {
						//out.println("<td>Counter:" + i + "</td><td>Visi: " + l_visi_List[i] + "</td><td>Level: " + l_level_List[i] + "</td>");
						out.println("<img src=\"" + pictures_path + "icon_empty.gif\" height=\"15\" width=\"" + width_td + "\" border=\"0\" ></img>");

						out.println("<a href=\"" + servlet_path + "?documentId="+ documentId +"&itemId=" + itemId + "&internal_call=true&counter="+ i + "&detail=true\" target=\"SHOW\" onClick=\"SetAndClear(" + i + ");\">" + TreeText + "</a></nobr>");
					}

					// end of tree line
					out.println("</th></tr>");
				} // visibility condition for the hierarchy level
				if(i_next != 0) {
					i = i_next - 1;
				}
			} // nodeList i - loop

			// HTML-Document-Footer
			out.println("</table>");

			// ScrollIntoView
			if (change != null) {
				int i_change = Integer.parseInt(change);
				out.println("<SCRIPT>");
				// outprintln("scroll("+ i_change + ");" );
				out.println( "eval('document.all.K" + i_change + ".scrollIntoView()');");
				out.println("</SCRIPT>");
			}

			// remark table line after request
			if (l_s_lastDetail != null) {
				out.println("<SCRIPT>");
				out.println("if(document.all.KTH" + l_s_lastDetail.toString() + "){");
				out.println("document.all.KTH" + l_s_lastDetail.toString() + ".style.backgroundColor = '#FFD700';}");
				out.println("</SCRIPT>");
			}
			out.println("</body></html>");

		}// internall_call && counter == null

			// last detail view set/get
			if (detail != null && detail.toString().equals("true") && counter != null){
				thisSession.setAttribute(xmlId+".XMLView.lastDetail",counter);
			}
			else if (thisSession.getAttribute(xmlId+".XMLView.lastDetail") != null){
				l_s_lastDetail = (String)thisSession.getAttribute(xmlId+".XMLView.lastDetail");
			}
		// construct the SHOW-frame:HTML page with XSLT_XML2HTML transformations
		if((internal_call != null) && (counter !=null )) {

			// XSLT in Action
			// II. display Show-HTML-page for selected list entry

			String XMLString = (String) thisSession.getAttribute(xmlId+".XMLView.XMLString");
			if (XMLString == null || XMLString.equals("")) {
				 XMLString = (String) getServletContext().getAttribute("XMLString");
			}
			StringReader rsource = new StringReader(XMLString);
			String style = (String) thisSession.getAttribute(xmlId+".XMLView.XSLShow");

			try {
				xslt_xml2html(style, rsource, request, response);
			}
			catch (TransformerException err) {
				response.getOutputStream().println("xslt_xml2html: Error applying stylesheet: " + err.getMessage());
			}

		} // internal_call && counter!= null

	} //End doGet

	/**Process the HTTP Get request*/
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request,response);
	}

	/**Clean up resources*/
	public void destroy() {
	}

	//XSLT-Transformations

	/**
	* 1. xml2xml: Reduktion der Quell-XML-Datei in xml f�r TREE-Frame.
	*
	**/

	private void xslt_xml2xml(String style, StringReader source, HttpServletResponse res)
		throws TransformerException, java.io.IOException {
		if (style==null) {
			System.out.println("xslt_xml2xml: No style parameter supplied");
			return;
		}
		if (source==null) {
			System.out.println("xslt_xml2xml: No source parameter supplied");
			return;
		}
		try {

			Templates pss = tryCache(style);
			Transformer transformer = pss.newTransformer();
			Properties details = pss.getOutputProperties();
			writer = new StringWriter();
			transformer.transform(new StreamSource(source), new StreamResult(writer));
		}
		catch (Exception err) {
			System.out.println(err.getMessage());
			err.printStackTrace();
		}
	}

	//XSLT-Transformations

	/**
	* 2. xslt_xml2html: detail view of the source XML file in HTML
	*    for the SHOW-frame.
	*    parameter counter is passed in the http request!
	*   */
	private void xslt_xml2html(String style, StringReader source, HttpServletRequest req, HttpServletResponse res)
		throws TransformerException, java.io.IOException {

		ServletOutputStream out = res.getOutputStream();

		if (style==null) {
			out.println("xslt_xml2html: No style parameter supplied");
			return;
		}
		if (source==null) {
			out.println("xslt_xml2html: No source parameter supplied");
			return;
		}
		try {
			Templates pss = tryCache(style);
			Transformer transformer = pss.newTransformer();
			Properties details = pss.getOutputProperties();
			res.setContentType("text/html");

			Enumeration p = req.getParameterNames();
			while (p.hasMoreElements()) {
				String name = (String)p.nextElement();
				if (!(name.equals("style") || name.equals("source"))) {
					String value = req.getParameter(name);
					transformer.setParameter(name, value);
				}
			}

			transformer.transform(new StreamSource(source), new StreamResult(out));

		}
		catch (Exception err) {
			out.println(err.getMessage());
			err.printStackTrace();
		}
	}

	/*
	*  cache Stylesheets
	*/

	private synchronized Templates tryCache(String url)
		throws TransformerException,TransformerConfigurationException,java.io.IOException {
		String path = url;
		if (path==null) {
			throw new TransformerException("Stylesheet " + url + " not found");
		}

		Templates x = (Templates)cache.get(path);

		if (x==null) {
			TransformerFactory factory = TransformerFactory.newInstance();
			x = factory.newTemplates(new StreamSource(new File(path)));
			cache.put(path, x);
		}
		return x;
	}

	/**
	* delete Cache
	*/

	private synchronized void clearCache() {
		cache = new Hashtable();
	}

	private Hashtable cache = new Hashtable();

	/**
	* Informationen of Servlet
	*/

	private void servlet_info(String label, HttpServletRequest request, PrintWriter out, String XMLString)
		throws java.io.IOException {
		out.println("<BR><BR><HR><BIG>SERVLET_INFO</BIG><HR><BR><BR>");
		out.println("<table>");
		out.println("<tr><td> ServletName </td><td>" + ServletName + "</td></tr>");
		out.println("<tr><td> request.getSession().getId()</td><td>" + request.getSession().getId() + "</td></tr>");
		out.println("<tr><td> request.isRequestedSessionIdValid </td><td>" + request.isRequestedSessionIdValid() + "</td></tr>");
		out.println("<tr><td> servlet_path </td><td>" + request.getServletPath() + "</td></tr>");
		out.println("<tr><td> XMLString.length():</td><td>" + XMLString.length() + "</td></tr>");
		out.println("<tr><td> Realpath \"/ \" : </td><td>" + getServletContext().getRealPath("/") + "</td></tr>");
		out.println("<tr><td> request.URI: </td><td>" + request.getRequestURI() + "</td></tr>");
		out.println("<tr><td> request.getContextPath </td><td>" + request.getContextPath() + "</td></tr>");
		out.println("<tr><td> request.getPathInfo </td><td>" + request.getPathInfo() + "</td></tr>");
		out.println("<tr><td> request.getPathTranslated </td><td>" + request.getPathTranslated() + "</td></tr>");
		out.println("<tr><td> xsl_style_tree: </td><td>" + xsl_style_tree + "</td></tr>");
		out.println("<tr><td> xsl_style_show: </td><td>" + xsl_style_show + "</td></tr>");
		out.println("<tr><td> css_style_tree: </td><td>" + css_style_tree + "</td></tr>");
		out.println("<tr><td> css_style_show: </td><td>" + css_style_show + "</td></tr>");
		out.println("</table><BR><BR><HR><BR><BR>");

	}
}

