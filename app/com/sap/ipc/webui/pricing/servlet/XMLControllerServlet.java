package com.sap.ipc.webui.pricing.servlet;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.Hashtable;
import java.util.Enumeration;

public class XMLControllerServlet extends HttpServlet {

    public void init(ServletConfig config) throws ServletException {
      super.init(config);
    }

    public void service(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
    {

    HttpSession thisSession = req.getSession(true);

	String address  = req.getParameter("address");

	if(address == null)
	    address = "/Frameset.html";
 	String forward  = req.getParameter("forward");
	String XMLAppli = req.getParameter("appli");

	if(forward != null){

	    String source_path = super.getServletContext().getRealPath("/");

		//I. File in einen String verwanden
	    char buffer[] = new char[500000];
		int byte_length = 0 ;

		FileReader freader = new FileReader(source_path + forward);
		byte_length = freader.read(buffer);
		String XMLFString = new String(buffer,0,byte_length);


        //II.ContextAttribute "XMLAppli" fllen
		String XMLAppliDefault = new String("CRM");

	    if(XMLAppli == null)
			getServletContext().setAttribute("XMLAppli", XMLAppliDefault);
	    else
			getServletContext().setAttribute("XMLAppli", XMLAppli);


	   //III.ContextAttribute "XMLString" fllen
	   String XMLStringDefault = new String("Test my attributes");

	   if(XMLFString == null)
			getServletContext().setAttribute("XMLString", XMLStringDefault);
	   else
			getServletContext().setAttribute("XMLString", XMLFString);


       // thisSession.invalidate();
	   RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(address);

	   if(dispatcher != null)
		    dispatcher.forward(req,res);

       else{
	        res.setContentType("text/html");
		    ServletOutputStream out = res.getOutputStream();
		    out.println("<HTML><BODY>");
		    out.println("Dispatcher nicht gefunden<BR>");
			out.println("</BODY></HTML>");
       }

	   }
	   else{
		res.setContentType("text/html");
                ServletOutputStream out = res.getOutputStream();
		out.println("<HTML><BODY>Parameters:<BR>");
                out.println("?forward=data/analysis.xml&appli=CRM&address=/servlet/com.sap.ipc.webui.pricing.servlet.XMLViewServlet<BR>");
                out.println("</BODY></HTML>");
	   }
    }

    /**
    * getServletInfo<BR>
    * Required by Servlet interface
    */

    public String getServletInfo() {
        return "Calls XMLViewServlet to test this servlet";
    }
}
