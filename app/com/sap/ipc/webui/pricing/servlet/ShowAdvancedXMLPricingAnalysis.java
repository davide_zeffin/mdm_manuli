package com.sap.ipc.webui.pricing.servlet;

import javax.servlet.*;
import javax.servlet.http.*;
import java.net.URLEncoder;
import java.io.*;
import java.util.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

//import com.sap.spc.remote.client.ClientSupport;
//import com.sap.sxe.socket.client.ClientException;
//import com.sap.sxe.socket.client.ServerResponse;

//import com.sap.sxe.socket.shared.ErrorCodes;
//import com.sap.sxe.sys.SAPTimestamp;

//import com.sap.spc.remote.shared.command.*;

//import com.sap.spc.remote.client.object.IPCException;
import com.sap.ipc.webui.pricing.UIConstants;
import com.sap.isa.core.logging.IsaLocation;


public class ShowAdvancedXMLPricingAnalysis extends HttpServlet {
                         //           implements com.sap.spc.remote.shared.command.CommandConstants {
	protected static IsaLocation log = IsaLocation.getInstance(ShowAdvancedXMLPricingAnalysis.class.getName());

    private static final String CONTENT_TYPE_XML   = "text/xml";
    private static final String CONTENT_TYPE_HTML  = "text/html";
    private static final String CONTENT_TYPE_PLAIN = "text/plain";

	private static Calendar m_calendar = null;

	private String m_logFileName = "speservlet.html";
    private String m_sourcePath = null;

 //  private ClientSupport m_client     = null;
    private PrintWriter   m_outScreen  = null;
    private String        m_result     = "";

    private boolean       m_trace      = false;
    private boolean       m_forwarding = true;
    private boolean       m_save       = false;
    private boolean       m_info       = false;

    private String        m_servletSourcePath = "";
    private PrintWriter   m_logOut            = null;  // for writing the log file of the servlet

    /**Initialize global variables*/
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        m_sourcePath = 	config.getServletContext().getRealPath("/");
        if (m_sourcePath == null || m_sourcePath == "")
            m_logFileName = "./" + m_logFileName;
        else
            m_logFileName = m_sourcePath + m_logFileName;
    }

    /**Process the HTTP Post request*/
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession httpSession = getSession(request, response);
        response.setHeader("pragma", "no-cache");

        // get parameter
  //      String ipcServer      = request.getParameter("server");
  //      String ipcPort        = request.getParameter("port");
  //      String ipcSessionId   = request.getParameter("sessionId");
  //      String ipcDocumentId  = request.getParameter("documentId");
  //      String ipcItemId      = request.getParameter("itemId");
   //     String forwarding     = request.getParameter("forwarding");
   //     String application    = request.getParameter("application");
   //     String info           = request.getParameter("info");
        String language       = request.getParameter("language");
        String trace          = request.getParameter("trace");
 //       String save           = request.getParameter("save");
 //       String fileName       = request.getParameter("file");
        String ipcScenario    = request.getParameter("ipcScenario");
        String styleSheet     = request.getParameter("styleSheet");
        String returnAddress  = request.getParameter("returnAddress");
        //The document is not passed as a url parameter but along with the request
        //String xmldocument    = request.getParameter("xmldocument");
		String xmldocument    = (String)request.getAttribute("xmldocument");
   //      m_result = xmldocument;
  //      m_save       = checkFlag(save);
   //    m_forwarding = checkFlag(forwarding);
        m_trace      = checkFlag(trace);
  //      m_info       = checkFlag(info);
       m_result = (String)xmldocument;
        this.setTraceWriter(response, request);    // get output writer

        writeParametersToTrace(request);

        String address        = "";

 //     if (application == null || application.equals("")) {
 //         application = "CRM";
 //         writeTrace("application set to CRM <br>");
       // }

        if (language == null || language.equals("")) {
            language = "EN";
            writeTrace("language set to EN <br>");
        }
        
		
        try {
            // create client for IPC
  //          m_client = getClient(httpSession, ipcServer , ipcPort);

    //        attachToSession(ipcSessionId);

   //         m_result = getXMLPricingTrace(ipcDocumentId, ipcItemId);

            if (xmldocument.length() == 0) {
				Object e = null;
                throw new Exception();
            }

      /*   if (save != null && save.equals("true")) {

                String fileNameForSave = fileName;
                BufferedWriter writer = new BufferedWriter(new FileWriter(fileNameForSave, false));
                writer.write(m_result);
                writer.newLine();
                writer.close();
            } */

          if (xmldocument.indexOf("INFO") != -1) {
                writeTrace("Trace has no data");
            } 

            if (m_forwarding) {

                if (m_info)
                    address = "/servlet/com.sap.ipc.webui.pricing.servlet.XMLViewServlet?info=true";
                else
                    address = "/servlet/com.sap.ipc.webui.pricing.servlet.XMLViewServlet?";

                if (ipcScenario != null && !ipcScenario.equals(""))
                    address = address+"&ipcScenario="+ipcScenario;

                if (styleSheet != null && !styleSheet.equals(""))
                    address = address+"&styleSheet="+styleSheet;
       /*       if (ipcDocumentId != null && !ipcDocumentId.equals(""))
                    address = address+"&documentId="+ipcDocumentId;

                if (ipcItemId != null && !ipcItemId.equals(""))
                    address = address+"&itemId="+ipcItemId; */

                if (returnAddress != null && !returnAddress.equals(""))
                    address = address+"&returnAddress="+URLEncoder.encode(returnAddress);

                writeTrace("<p><h2>forward address</h2><br>");
                writeTrace("forwardAddress: " + address);

                super.getServletContext().setAttribute("XMLString", xmldocument);
              super.getServletContext().setAttribute("XMLAppli",  "CRM");

                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(address);
                if (dispatcher != null)
                    dispatcher.forward(request, response);

                m_logOut.close();
            }
            else {
                // write raw XML string
//                if (!this.isTracing()) {

                    m_logOut.close();

                    response.setContentType(CONTENT_TYPE_XML);
                    m_outScreen = response.getWriter();
                    m_outScreen.println(xmldocument);
                    m_outScreen.close();
//                }
            }
        }
        catch(Exception e) {

            if (isTracing()) {
                writeTrace("<p><b>IPCException is thrown</b><br>");
                e.printStackTrace(m_logOut);
                m_logOut.close();
            }

            if (xmldocument.indexOf("INFO") != -1)
                generateEmptyTraceInfo(response, xmldocument);
            else {
                response.setContentType(CONTENT_TYPE_HTML);
                request.setAttribute(UIConstants.Request.Parameter.IPC_EXCEPTION, e);
                address = "/ui/IPCErrorPage.jsp";
                RequestDispatcher dispatcher = request.getRequestDispatcher(address);
                if (dispatcher != null) {
                    dispatcher.forward(request, response);
                }
            }
        }
    /*  catch(NullPointerException e) {
            m_trace = true;
            setTraceWriter(response, request);
            writeTrace("<p><b>NullPointerException in post command GetXMLPricingTrace (probably a parameter problem):</b> <br><p>");
            e.printStackTrace(m_logOut);
            m_logOut.close();
        } */
    }

    /**Process the HTTP Get request*/
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    /**
     * @return the client that this session uses to communicate with a server.
     *         If their is no client the method will create one
     */
  /*  private ClientSupport getClient(HttpSession httpSession,
                                   String server,
                                   String port)
                throws IPCException {

        String encoding = "UnicodeLittle";
        String key = "CLIENT" + " " + server + " " + port;

        writeTrace("<p> try to get a client on server" + " " + server + " " + port + "<br>");

        ClientSupport client = (ClientSupport)httpSession.getAttribute(key);

        if (client == null) {
            if (server == null || port == null)
                throw new IPCException(new ClientException(ErrorCodes.RET_INTERNAL_FATAL,
                                          "no parameters for connecting to client: server = " + server + " port = " + port));
            try {
                client = new ClientSupport(server, Integer.parseInt(port), encoding, new Boolean("false"));
                writeTrace("created a new client on server"+ " " + server + " " + port + "<br>");
            }
            catch (ClientException e) {
                throw new IPCException(e);
            }
        }

        httpSession.setAttribute(key,client);
        return client;
    } */


  /*  private void attachToSession(String sessionId) {

        writeTrace("<p>try to attach to session " + " " + sessionId + "<br>");
        // attach to session
        m_client.setSessionId(sessionId);
        writeTrace("attachment to session was successfull<br>");
    }*/

  /*  private String getXMLPricingTrace(String documentID,
                                      String itemID)
            throws IPCException {

        writeTrace("<p>try to get XML trace for documentID=" + documentID + " and itemID=" + itemID + "<br>");

        try {
            if (m_client != null) {
                ServerResponse r = m_client.cmd(GET_XML_PRICING_TRACE, new String[]  {
                                                GetXMLPricingTrace.DOCUMENT_ID, documentID,
                                                GetXMLPricingTrace.ITEM_ID, itemID,
                                              });
                writeTrace("got an string<br>");
                return r.getParameterValue(GetXMLPricingTrace.XML_STRING);
            }
            else
                return null;
        }
        catch (ClientException e) {
            throw new IPCException(e);
        }
    }*/

    public HttpSession getSession(HttpServletRequest req, HttpServletResponse res) {
        HttpSession result = req.getSession(true);
        if (result != null) {
            return result;
        }
        else {
            getServletContext().log("Servlet session not found");
            try {
                res.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,"Servlet session not found. This is most probably caused by a timeout.");
            }
            catch(IOException e)
            {log.debug(e.getMessage(),e);}
            return null;
        }
    }

    public String getServletUrl() {
            return "/servlet/"+getClass().getName();
    }

    private void generateEmptyTraceInfo(HttpServletResponse response, String result) {
        int pos1;
        int pos2;
        try {
            m_outScreen = response.getWriter();
            response.setContentType(CONTENT_TYPE_HTML);

            m_outScreen.println("<HTML><BODY BGCOLOR=\"#DED5C4\" LEFTMARGIN=\"10\" TOPMARGIN=\"0\" MARGINWIDTH=\"0\" MARGINHEIGHT=\"0\" text=\"black\" link=\"#000000\" vlink=\"#000000\" alink=\"#000000\" >");
            m_outScreen.println("<center><p><hr><p><font size=\"+2\"><b> ");
            pos1 = result.indexOf("INFO1")+7;
            pos2 = result.indexOf("INFO2")-2 ;
            m_outScreen.println(result.substring( pos1 ,  pos2));
            m_outScreen.println("</b></font></p></center>");
            m_outScreen.println("<p><br>");

            pos1 = result.indexOf("INFO2")+7;
            pos2 = result.indexOf("/>")-1 ;
            m_outScreen.println(result.substring( pos1 , pos2 ));
            m_outScreen.println("</br></p><hr></body></html>");
        }
        catch(IOException e){
			log.debug(e.getMessage(),e);
        }
    }

    private boolean isTracing() {
        return m_trace;
    }

    private void setTraceWriter(HttpServletResponse response, HttpServletRequest request) {

        if (this.isTracing()) {
            try {
                if (m_logOut == null) {
                    FileOutputStream fos = new FileOutputStream(m_logFileName);
                    m_logOut = new PrintWriter(fos, true);
                }
                else {
                    FileOutputStream fos = new FileOutputStream(m_logFileName, true);
                    m_logOut = new PrintWriter(fos, true);
                }
            }
	    	catch(IOException e){
                System.out.println("Writing to file "+ m_logFileName +" failed. Log can not be written");
            }
        }

        if (m_calendar == null) {
            m_calendar = Calendar.getInstance();
        }
        m_calendar.setTime(new Date());


        SimpleDateFormat formatter = new SimpleDateFormat ("dd.MM.yyyy hh:mm:ss");
        Date currentTime_1 = new Date();
        String dateString = formatter.format(currentTime_1);

   //     SAPTimestamp timeStamp = new SAPTimestamp(m_calendar);
        writeTrace("<p>");
        writeTrace("<b>" + dateString + "   com.sap.ipc.webui.pricing.servlet.ShowAdvancedXMLPricingAnalysis</b> <p>");
    }

    private void writeTrace(String line) {
        if (this.isTracing())
            m_logOut.println(line);
   }

   private void writeParametersToTrace(HttpServletRequest request) {
        // write parameters of servlet to output file
        writeTrace("<p><b>Input parameters for servlet: </b></br>");
   //     writeTrace("server:      " + request.getParameter("server") + "<br>");
  //      writeTrace("port:        " + request.getParameter("port") + "<br>");
  //      writeTrace("sessionId:   " + request.getParameter("sessionId") + "<br>");
  //      writeTrace("documentId:  " + request.getParameter("documentId") + "<br>");
 //       writeTrace("itemId:      " + request.getParameter("itemId") + "<br>");
 //       writeTrace("language:    " + request.getParameter("language") + "<br>");
//        writeTrace("forwarding:  " + request.getParameter("forwarding") + "<br>");
//        writeTrace("application: " + request.getParameter("application") + "<br>");
//        writeTrace("save:        " + request.getParameter("save") + "<br>");
//        writeTrace("fileName:    " + request.getParameter("file") + "<br>");
//        writeTrace("info:        " + request.getParameter("info") + "<br>");
        writeTrace("ipcScenario: " + request.getParameter("ipcScenario") + "<br>");
        writeTrace("styleSheet:  " + request.getParameter("styleSheet") + "<br>");
        writeTrace("returnAddress:" + request.getParameter("returnAddress") + "<br>");
   }

   private boolean checkFlag(String switchString) {
        if (switchString == null ||
            switchString.equals("") ||
            switchString.equals(" ") ||
            switchString.equalsIgnoreCase("FALSE"))
            return false;
        else
            return true;
    }

    /**Clean up resources*/
    public void destroy() {
    }

}
