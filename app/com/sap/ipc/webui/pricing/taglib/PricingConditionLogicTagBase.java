/************************************************************************

	Copyright (c) 2001 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.ipc.webui.pricing.taglib;

import javax.servlet.jsp.JspException;

import com.sap.ipc.webui.pricing.model.*;

public abstract class PricingConditionLogicTagBase extends PricingConditionTagBase{

	// ----------------------------------------------------- Instance Variables

	/**
	 * indicates, if the check should be negated (NOT).
	 */
	private String _isNegated = null;

	/**
	 * the value that is used for comparison with the condition property.
	 */
	private String _value = null;

	// ------------------------------------------------------------- Properties

	/**
	 * Return if the check should be negated (NOT).
	 */
	public String getNot() {
		return _isNegated;
	}


	/**
	 * Set  if the check should be negated (NOT).
	 *
	 * @param notFlag the flag
	 */
	public void setNot(String notFlag) {
		_isNegated = notFlag;
	}

	/**
	 * Return the coparison value.
	 */
	public String getValue() {
		return _value;
	}


	/**
	 * Set the comparison value
	 *
	 * @param value the value
	 */
	public void setValue(String value) {
		_value = value;
	}

	// --------------------------------------------------------- Public Methods

	public int doStartTag() throws JspException {

		boolean isNegated = new Boolean(_isNegated).booleanValue();
		if (!isNegated && check(getPricingCondition(), getPricingConditionProperty(), getValue()) ||
			isNegated && !check(getPricingCondition(), getPricingConditionProperty(), getValue()) )
			return (EVAL_BODY_INCLUDE);
		else
			return (SKIP_BODY);

	}

	protected abstract boolean check(PricingCondition pricingCondition, Object propertyValue, String comparisonValue) throws JspException;

	public int doEndTag() throws JspException {

		return (EVAL_PAGE);

	}

}