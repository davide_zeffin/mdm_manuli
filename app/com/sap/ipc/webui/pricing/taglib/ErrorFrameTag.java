/************************************************************************

	Copyright (c) 2001 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.ipc.webui.pricing.taglib;

import java.util.Iterator;
import java.io.IOException;
import javax.servlet.jsp.tagext.TagSupport;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;

import org.apache.struts.util.MessageResources;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;

import com.sap.ipc.webui.pricing.UIConstants;


public class ErrorFrameTag extends TagSupport {

	/**
	* The message resources for this package.
	*/
	protected static MessageResources messages =
		MessageResources.getMessageResources(UIConstants.MESSAGE_RESOURCES);

	public int doStartTag() throws JspException{

		// get ActionErrors
	    ActionErrors errors = (ActionErrors)pageContext.getAttribute(Action.ERROR_KEY, PageContext.REQUEST_SCOPE);
		if (errors == null)
			return SKIP_BODY;
		return EVAL_BODY_INCLUDE;
    }

    public int doEndTag() throws JspException{
		return EVAL_PAGE;
	}

}


