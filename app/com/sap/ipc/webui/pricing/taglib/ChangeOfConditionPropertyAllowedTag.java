/************************************************************************

	Copyright (c) 2001 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.ipc.webui.pricing.taglib;

import javax.servlet.jsp.JspException;

import com.sap.ipc.webui.pricing.model.*;


public class ChangeOfConditionPropertyAllowedTag  extends PricingConditionLogicTagBase{


	// --------------------------------------------------------- Public Methods


	/**
	 * Check if changing of property is allowed.
	 *
	 * @exception JspException if a JSP exception has occurred
	 */
	protected boolean check(PricingCondition pricingCondition, Object propertyValue, String comparisonValue) throws JspException{
		String property = getProperty();
		if (pricingCondition == null)
			return true;
		if (property == null)
			return false;
		if (property.equals(PricingCondition.PropertyNames.CONDITION_TYPE_NAME))
			return true;
		if (pricingCondition.isChangeOfRatesAllowed()
			&& (property.equals(PricingCondition.PropertyNames.CONDITION_RATE_VALUE)
				|| property.equals(PricingCondition.PropertyNames.CONDITION_CURRENCY)))
			return true;
		if (pricingCondition.isChangeOfPricingUnitsAllowed()
			&& (property.equals(PricingCondition.PropertyNames.PRICING_UNIT_VALUE)
				|| property.equals(PricingCondition.PropertyNames.PRICING_UNIT_UNIT)))
			return true;
		if (pricingCondition.isChangeOfValuesAllowed()
			&& (property.equals(PricingCondition.PropertyNames.CONDITION_VALUE_VALUE)))
			return true;
		return false;
	}

}