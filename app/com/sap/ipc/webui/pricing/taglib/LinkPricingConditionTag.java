/************************************************************************

	Copyright (c) 2001 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.ipc.webui.pricing.taglib;

import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;

import org.apache.struts.util.ResponseUtils;

import com.sap.ipc.webui.pricing.UIConstants;
import com.sap.ipc.webui.pricing.model.*;

/**
 * @author
 * Generates a <a href=... tag with name containing the parameter "page" and the stepNumber and counter of the
 * pricingConditionForm currently available in the pageContext.
 *
 */
public class LinkPricingConditionTag extends PricingConditionTagBase {

	// ----------------------------------------------------- Instance Variables

	/**
	 * The context-relative URI.
	 */
	private String _page = null;
	private String _tabindex = null;

	// ------------------------------------------------------------- Properties

	/**
	 * Return the context-relative URI.
	 */
	public String getPage() {
		return _page;
	}

	/**
	 * Set the context-relative URI.
	 *
	 * @param page Set the context-relative URI
	 */
	public void setPage(String page) {
		_page = page;
	}


	// --------------------------------------------------------- Public Methods


	/**
	 * Render the beginning of the hyperlink.
	 *
	 * @exception JspException if a JSP exception has occurred
	 */
	public int doStartTag() throws JspException {

		// Generate the URL to be encoded
		HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();
		StringBuffer url = new StringBuffer(request.getContextPath());
		url.append(_page);

		PricingCondition pricingCondition = pricingCondition = getPricingCondition();

		if (_page.indexOf("?") < 0)
			url.append("?");
		else
			url.append("&");
		url.append(UIConstants.Request.Parameter.STEP_NO);
		url.append("=");
		url.append(ResponseUtils.filter(pricingCondition.getStepNo()));
		url.append("&");
		url.append(UIConstants.Request.Parameter.COUNTER);
		url.append("=");
		url.append(ResponseUtils.filter(pricingCondition.getCounter()));

		// Generate the hyperlink start element
		HttpServletResponse response = (HttpServletResponse) pageContext.getResponse();
		StringBuffer results = new StringBuffer("<a href=\"");
		results.append(response.encodeURL(ResponseUtils.filter(url.toString())));
		results.append("\"");
		if (_tabindex != null) {
			results.append(" tabindex=\"");
			results.append(_tabindex);
			results.append("\"");
		}
		results.append(">");

		// Print this element to our output writer
		JspWriter writer = pageContext.getOut();
		try {
			writer.print(results.toString());
		} catch (IOException e) {
			throw new JspException
			(messages.getMessage("error.jsp.application", e.toString()));
		}

		// Evaluate the body of this tag
		return (EVAL_BODY_INCLUDE);

	}



	/**
	 * Render the end of the hyperlink.
	 *
	 * @exception JspException if a JSP exception has occurred
	 */
	public int doEndTag() throws JspException {

		// Print the ending element to our output writer
		JspWriter writer = pageContext.getOut();
		try {
			writer.print("</a>");
		} catch (IOException e) {
			throw new JspException
				(messages.getMessage("error.jsp.application", e.toString()));
		}

		return (EVAL_PAGE);

	}


	/**
	 * Release any acquired resources.
	 */
	public void release() {

		super.release();
		_page = null;

	}

	/**
	 * @return
	 */
	public String getTabindex() {
		return _tabindex;
	}

	/**
	 * @param string
	 */
	public void setTabindex(String string) {
		_tabindex = string;
	}

}