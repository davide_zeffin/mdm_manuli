/************************************************************************

	Copyright (c) 2001 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.ipc.webui.pricing.taglib;

import java.util.Iterator;
import java.io.IOException;
import javax.servlet.jsp.tagext.BodyTagSupport;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;

import org.apache.struts.util.MessageResources;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;

import com.sap.ipc.webui.pricing.UIConstants;


public class ErrorIterateTag extends BodyTagSupport {

	/**
	* The message resources for this package.
	*/
	protected static MessageResources messages =
		MessageResources.getMessageResources(UIConstants.MESSAGE_RESOURCES);

    private Iterator _iterator;

	public int doStartTag() throws JspException{

		// get ActionErrors
	    ActionErrors errors = (ActionErrors)pageContext.getAttribute(Action.ERROR_KEY, PageContext.REQUEST_SCOPE);
		if (errors == null)
			return SKIP_BODY;

		_iterator = errors.get();
        // set the first value, if any
        if (_iterator != null && _iterator.hasNext()){
            Object currError = _iterator.next();
            pageContext.setAttribute(UIConstants.Page.Attribute.ERROR, currError);
		    return EVAL_BODY_TAG;
		}
        else{
		    return SKIP_BODY;
        }
    }

    public int doAfterBody() throws JspException{
        if (_iterator.hasNext()){
            Object currError = _iterator.next();
            pageContext.setAttribute(UIConstants.Page.Attribute.ERROR, currError);
		    return EVAL_BODY_TAG;
		}
        else{
		    return SKIP_BODY;
        }
	}

    public int doEndTag() throws JspException{
		if (getBodyContent() != null){
			try{
				getPreviousOut().print(getBodyContent().getString());
			} catch (IOException e) {
				throw new JspException
				(messages.getMessage("error.jsp.application", e.toString()));
			}
		}
		return EVAL_PAGE;
	}

    /**
     * Releases all instance variables.
     */
    public void release() {
        _iterator = null;
        super.release();
    }
}


