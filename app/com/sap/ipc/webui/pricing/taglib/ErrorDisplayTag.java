/************************************************************************

	Copyright (c) 2001 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.ipc.webui.pricing.taglib;

import java.io.IOException;
import javax.servlet.jsp.tagext.TagSupport;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.util.MessageResources;
import org.apache.struts.util.RequestUtils;
import org.apache.struts.action.ActionError;

import com.sap.ipc.webui.pricing.UIConstants;


public class ErrorDisplayTag extends TagSupport {

	/**
	* The message resources for this package.
	*/
	protected static MessageResources messages =
		MessageResources.getMessageResources(UIConstants.MESSAGE_RESOURCES);

	public int doStartTag() throws JspException {

		ActionError error = (ActionError)pageContext.getAttribute(UIConstants.Page.Attribute.ERROR);
		if (error == null)
			//no error there
			return (SKIP_BODY);

		HttpServletResponse response = (HttpServletResponse) pageContext.getResponse();

		StringBuffer results = new StringBuffer();
        String message = RequestUtils.message(pageContext, null, null, error.getKey(), error.getValues());
	    if (message != null){
			results.append(message);
	    }

		// print to output writer
		JspWriter writer = pageContext.getOut();
		try {
			writer.print(results.toString());
		} catch (IOException e) {
			throw new JspException
			(messages.getMessage("error.jsp.application", e.toString()));
		}

		// skip the body of this tag
		return (SKIP_BODY);

	}

	/**
	 * end tag.
	 *
	 * @exception JspException if a JSP exception has occurred
	 */
	public int doEndTag() throws JspException{
		return (EVAL_PAGE);
	}

}