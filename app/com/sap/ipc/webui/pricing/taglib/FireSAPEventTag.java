/************************************************************************

	Copyright (c) 2001 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.ipc.webui.pricing.taglib;

import java.util.Hashtable;
import java.util.Enumeration;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.struts.util.ResponseUtils;

import com.sap.ipc.webui.pricing.model.*;

public class FireSAPEventTag extends PricingConditionPanelTagBase{


   // ----------------------------------------------------- Instance Variables

	/**
	 * SAP event action.
	 */
	private String _action = null;

	// ------------------------------------------------------------- Properties

	/**
	 * Return SAP event action.
	 */
	public String getAction() {
		return _action;
	}

	/**
	 * Set the SAP event action.
	 *
	 * @param action Set SAP event action
	 */
	public void setAction(String action) {
		_action = action;
	}


	// --------------------------------------------------------- Public Methods


	/**
	 * Render the <SAPEVENT:xxx> tag
	 *
	 * @exception JspException if a JSP exception has occurred
	 */
	public int doStartTag() throws JspException {

		PricingConditionPanel pricingConditionPanel = getPricingConditionPanel();

		Hashtable paramData = pricingConditionPanel.getSAPEventStack().processEvent(_action);
		if (paramData == null)
			//no event there
			return (SKIP_BODY);

		HttpServletResponse response = (HttpServletResponse) pageContext.getResponse();

		StringBuffer results = new StringBuffer("<form name='SAPEVENT' METHOD='POST' action='SAPEVENT:");
		results.append(ResponseUtils.filter(_action));
		results.append("'>");
		//add parameters from pricingConditionPanel.processEvent()
		Enumeration enum = paramData.keys();
		for (int i = 0; enum.hasMoreElements(); i++) {
			String paramName = (String)enum.nextElement();
			String paramValue = (String)paramData.get(paramName);
			results.append("<input type='hidden' name='");
			results.append(ResponseUtils.filter(paramName));
			results.append("' value='");
			results.append(ResponseUtils.filter(paramValue));
			results.append("'>");
		}
		results.append("</form>");

		results.append("<SCRIPT LANGUAGE='Javascript'> document.SAPEVENT.submit();</SCRIPT>");

		// Print this element to our output writer
		JspWriter writer = pageContext.getOut();
		try {
			writer.print(results.toString());
		} catch (IOException e) {
			throw new JspException
			(messages.getMessage("error.jsp.application", e.toString()));
		}

		// skip the body of this tag
		return (SKIP_BODY);

	}



	/**
	 * end tag.
	 *
	 * @exception JspException if a JSP exception has occurred
	 */
	public int doEndTag() throws JspException {

		return (EVAL_PAGE);

	}


	/**
	 * Release any acquired resources.
	 */
	public void release() {

		super.release();
		_action = null;

	}

}