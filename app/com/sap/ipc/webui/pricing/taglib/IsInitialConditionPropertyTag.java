/************************************************************************

	Copyright (c) 2001 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.ipc.webui.pricing.taglib;

import com.sap.ipc.webui.pricing.model.PricingCondition;

import java.lang.reflect.InvocationTargetException;
import javax.servlet.jsp.JspException;

public class IsInitialConditionPropertyTag extends CheckConditionPropertyValueTag{

	protected boolean check(PricingCondition pricingCondition, Object propertyValue, String comparisonValue) throws JspException{
		//check all "supported" return types
		if (propertyValue instanceof Boolean){
		    return false;
		}
		else if (propertyValue instanceof String){
		    comparisonValue = "";
		}
		else if (propertyValue instanceof Character ){
		    comparisonValue = "";
		    propertyValue = propertyValue.toString().trim();
		}
        else throw new JspException
			(messages.getMessage("error.jsp.application", _property, propertyValue.getClass().getName()));//<<
		return super.check(pricingCondition, propertyValue, comparisonValue);
	}

}