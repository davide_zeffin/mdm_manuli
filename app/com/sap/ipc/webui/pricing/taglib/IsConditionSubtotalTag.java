/************************************************************************

	Copyright (c) 2001 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.ipc.webui.pricing.taglib;

import javax.servlet.jsp.JspException;

import com.sap.ipc.webui.pricing.model.*;


public class IsConditionSubtotalTag extends PricingConditionLogicTagBase{

	protected boolean check(PricingCondition pricingCondition, Object propertyValue, String comparisonValue){
		return (pricingCondition.getConditionTypeName().equals("")
			    && !(pricingCondition instanceof NewPricingCondition)); //HACK- TODO:use method pricingCondition.isSubtotal
	}

}