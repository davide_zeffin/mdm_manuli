/************************************************************************

	Copyright (c) 2001 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.ipc.webui.pricing.taglib;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.lang.reflect.InvocationTargetException;

import org.apache.struts.util.MessageResources;

import com.sap.ipc.webui.pricing.utils.PropertyUtils;
import com.sap.ipc.webui.pricing.model.*;
import com.sap.ipc.webui.pricing.actionform.PricingConditionForm;
import com.sap.ipc.webui.pricing.UIConstants;


public class PricingConditionTagBase extends TagSupport {


	// ----------------------------------------------------- Instance Variables

	/**
	* The message resources for this package.
	*/
	protected static MessageResources messages =
		MessageResources.getMessageResources(UIConstants.MESSAGE_RESOURCES);

	/**
	 * The attribute name.
	 */
	private String _name = com.sap.ipc.webui.pricing.UIConstants.Session.Attribute.PRICING_CONDITION;
    /**
     * name of the pricing condition property.
     */
    protected String _property = null;


	// ------------------------------------------------------------- Properties

	/**
	 * Return the attribute name.
	 */
	public String getName() {
		return _name;
	}

	/**
	 * Set the attribute name.
	 *
	 * @param name The new attribute name
	 */
	public void setName(String name) {
		_name = name;
	}

	/**
	 * Return the property name.
	 */
    public String getProperty() {
        return _property;
    }

	/**
	 * Set the attribute name.
	 *
	 * @param property The new property name
	 */
    public void setProperty(String property) {
        _property = property;
    }


	// --------------------------------------------------------- Public Methods

	/**
	 * Release any acquired resources.
	 */
	public void release() {

		super.release();
		_name = com.sap.ipc.webui.pricing.UIConstants.Session.Attribute.PRICING_CONDITION;

	}


	protected PricingCondition getPricingCondition() throws JspException{
		PricingCondition pricingCondition = null;
		try {
			pricingCondition = (PricingCondition) pageContext.findAttribute(_name);
			if (pricingCondition == null){
				// get PricingCondition from UIContext
				UIContext uIContext = (UIContext) pageContext.getSession().getAttribute(com.sap.ipc.webui.pricing.UIConstants.Session.Attribute.UI_CONTEXT);
				if (uIContext == null)
					throw new JspException
						(messages.getMessage("error.jsp.application"));
				pricingCondition = uIContext.getPricingCondition();
				if (uIContext != null && pricingCondition == null){
					PricingConditionForm pricingConditionForm = (PricingConditionForm) pageContext.findAttribute("pricingConditionForm"); //->Constants
					if (pricingConditionForm != null){
						PricingConditionPanel panel = uIContext.getPricingConditionPanel();
						pricingCondition = panel.findPricingCondition(pricingConditionForm.getStepNo(), pricingConditionForm.getCounter());
					}
				}
			}
		} catch (ClassCastException e) {
			pricingCondition = null;
		}
		if (pricingCondition == null)
			throw new JspException
				(messages.getMessage("error.jsp.application"));
		return pricingCondition;
	}

	protected Object getPricingConditionProperty() throws JspException{
		return getPricingConditionProperty(getPricingCondition());
	}

	protected Object getPricingConditionProperty(PricingCondition pricingCondition) throws JspException{
		if (_property == null)
			return null;
		Object value = null;
		try {
			value = PropertyUtils.getProperty(pricingCondition, _property);
		} catch (InvocationTargetException e) {
			Throwable t = e.getTargetException();
            if (t == null)
				t = e;
			//RequestUtils.saveException(pageContext, t);
            throw new JspException
				(messages.getMessage("error.jsp.application", _property, t.toString()));
		} catch (Throwable t) {
			//RequestUtils.saveException(pageContext, t);
            throw new JspException
			(messages.getMessage("error.jsp.application", _property, t.toString()));
		}
		return value;
	}

}