/************************************************************************

	Copyright (c) 2001 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.ipc.webui.pricing.taglib;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.lang.reflect.InvocationTargetException;

import org.apache.struts.util.MessageResources;

import com.sap.ipc.webui.pricing.utils.PropertyUtils;
import com.sap.ipc.webui.pricing.model.*;
import com.sap.ipc.webui.pricing.UIConstants;


public class PricingConditionPanelTagBase extends TagSupport {


	// ----------------------------------------------------- Instance Variables

	/**
	* The message resources for this package.
	*/
	protected static MessageResources messages =
		MessageResources.getMessageResources(UIConstants.MESSAGE_RESOURCES);

	/**
	 * The attribute name.
	 */
	private String _name = com.sap.ipc.webui.pricing.UIConstants.Session.Attribute.PRICING_CONDITION_PANEL;
    /**
     * name of the pricing condition property.
     */
    protected String _property = null;

	// ------------------------------------------------------------- Properties

	/**
	 * Return the attribute name.
	 */
	public String getName() {
		return _name;
	}


	/**
	 * Set the attribute name.
	 *
	 * @param name The new attribute name
	 */
	public void setName(String name) {
		_name = name;
	}

	/**
	 * Return the property name.
	 */
    public String getProperty() {
        return _property;
    }

	/**
	 * Set the attribute name.
	 *
	 * @param property The new property name
	 */
    public void setProperty(String property) {
        _property = property;
    }

	// --------------------------------------------------------- Public Methods

	/**
	 * Release any acquired resources.
	 */
	public void release() {

		super.release();
		_name = com.sap.ipc.webui.pricing.UIConstants.Session.Attribute.PRICING_CONDITION_PANEL;

	}


	protected PricingConditionPanel getPricingConditionPanel() throws JspException{
		PricingConditionPanel pricingConditionPanel = null;
		try {
			pricingConditionPanel = (PricingConditionPanel) pageContext.findAttribute(_name);
			if (pricingConditionPanel == null){
				// get PricingConditionPanel from UIContext
				UIContext uIContext = (UIContext) pageContext.getSession().getAttribute(com.sap.ipc.webui.pricing.UIConstants.Session.Attribute.UI_CONTEXT);
				if (uIContext == null)
					throw new JspException
						(messages.getMessage("error.jsp.application"));
				pricingConditionPanel = uIContext.getPricingConditionPanel();
			}
		} catch (ClassCastException e) {
			pricingConditionPanel = null;
		}
		if (pricingConditionPanel == null)
			throw new JspException
				(messages.getMessage("error.jsp.application"));
		return pricingConditionPanel;
	}

	protected Object getPricingConditionPanelProperty() throws JspException{
		return getPricingConditionPanelProperty(getPricingConditionPanel());
	}

	protected Object getPricingConditionPanelProperty(PricingConditionPanel pricingConditionPanel) throws JspException{
		if (_property == null)
			return null;
		Object value = null;
		try {
			value = PropertyUtils.getProperty(pricingConditionPanel, _property);
		} catch (InvocationTargetException e) {
			Throwable t = e.getTargetException();
            if (t == null)
				t = e;
			//RequestUtils.saveException(pageContext, t);
            throw new JspException
				(messages.getMessage("error.jsp.application", _property, t.toString()));
		} catch (Throwable t) {
			//RequestUtils.saveException(pageContext, t);
            throw new JspException
			(messages.getMessage("error.jsp.application", _property, t.toString()));
		}
		return value;
	}

}