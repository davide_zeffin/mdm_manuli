/************************************************************************

	Copyright (c) 2001 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.ipc.webui.pricing;

public final class UIConstants {

	public static final class Request{
		public static final class Parameter{
			public static final String    ACTION                    = "action";
			public static final String    STEP_NO                   = "stepNo";
			public static final String    COUNTER                   = "counter";
			public static final String    LANGUAGE                  = "language";
			public static final String    MODE                      = "mode";
			public static final String    CLIENT                    = "client"; //the mandt
			public static final String    SERVER                    = "server";
			public static final String    PORT                      = "port";
			public static final String    SESSION_ID                = "sessionId";
			public static final String    DOCUMENT_ID               = "documentId";
			public static final String    ITEM_ID                   = "itemId";
			public static final String    PRICING_ANALYSIS          = "pricingAnalysis";
			public static final String    GROUP_PROCESSING          = "groupProcessing";
			public static final String    IPC_EXCEPTION             = "ipcException";
			public static final String    GROUPING_SEPARATOR        = "GROUPING_SEPARATOR";
			public static final String    DECIMAL_SEPARATOR         = "DECIMAL_SEPARATOR";
			/**
			 * The calling application may pass the identifiers of an
			 * {@link #com.sap.spc.base.SPCItem} in form of an
			 * {@link #com.sap.spc.remote.client.object.IPCItemReference}.
			 */
			public static final String IPC_ITEM_REFERENCE = "ipcItemReference";
		}
	}

	public static final class Page{
		public static final class Attribute{
			public static final String    ERROR                     = "com.sap.ipc.webui.pricing.error";
		}
	}

	public static final class Session{
		public static final class Attribute{
			public static final String    UI_CONTEXT                = "com.sap.ipc.webui.pricing.UIContext";
			public static final String    PRICING_CONDITION_PANEL   = "com.sap.ipc.webui.pricing.pricingConditionPanel"; //page scope
			public static final String    PRICING_CONDITION         = "com.sap.ipc.webui.pricing.pricingCondition"; //page scope
		}
	}

	public static final class Mode{
		public static final String    DISPLAY            = "Display";
		public static final String    EDIT               = "Edit";
	}

	public static final class Switch{
		public static final String    ACTIVE             = "true";
		public static final String    INACTIVE           = "false";
	}

	public static final class Action{
		public static final String    DISPLAY             = "Display";
		public static final String    CREATE              = "Create";
		public static final String    DELETE              = "Delete";
		public static final String    EDIT                = "Edit";
	}

	public static final class SAPEventAction{
		public static final String    PRICING_DATA_CHANGED     = "PRC";
	}


	public static final String MESSAGE_RESOURCES = "properties.ApplicationResources";

}

