/*
 * Created on 01.07.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.ipc.webui.pricing.constants;

/**
 * @author 
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public interface IPCExceptionConstants {
    public static final String IPCEX = "temporary dummy";
    public static final String IPCEX1 = "ipc.exception.1";
    public static final String IPCEX2 = "ipc.exception.2";
    public static final String IPCEX3 = "ipc.exception.3";
    public static final String IPCEX4 = "ipc.exception.4";
    public static final String IPCEX5 = "ipc.exception.5";
    public static final String IPCEX6 = "ipc.exception.6";
    public static final String IPCEX7 = "ipc.exception.7";
    public static final String IPCEX8 = "ipc.exception.8";
    public static final String IPCEX9 = "ipc.exception.9";
    public static final String IPCEX10 = "ipc.exception.10";
    public static final String IPCEX11 = "ipc.exception.11";
    public static final String IPCEX12 = "ipc.exception.12";
    public static final String IPCEX13 = "ipc.exception.13";
    public static final String IPCEX14 = "ipc.exception.14";
    public static final String IPCEX15 = "ipc.exception.15";
    public static final String IPCEX16 = "ipc.exception.16";
    public static final String IPCEX17 = "ipc.exception.17";
    public static final String IPCEX18 = "ipc.exception.18"; //no UIContext object in user session
    public static final String IPCEX19 = "ipc.exception.19"; //user session not in request
    public static final String IPCEX20 = "ipc.exception.20"; //found UIContext object in user session when trying to create one
    public static final String IPCEX21 = "Required Parameter instancePage not passed";
    public static final String IPCEX22 = "Required Parameter instance not passed";
    public static final String IPCEX23 = "Required Parameter group not passed";
    public static final String IPCEX24 = "Required Parameter instancesPage not passed";
    public static final String IPCEX25 = "Required Parameter instances not passed";
    public static final String IPCEX26 = "Required Parameter instance not passed";
    public static final String IPCEX27 = "Error including instancesPage";
    public static final String IPCEX28 = "Error including dynamicProductPictureTemplate";
    public static final String IPCEX29 = "dynamicProductPictureTemplate not found";
    public static final String IPCEX30 = "Error including groupsPage";
    public static final String IPCEX31 = "groupsPage not found";
    public static final String IPCEX32 = "Include Parameter on Stack do not have the type CharacteristicGroupsUI.IncludeParams";
    public static final String IPCEX33 = "";
    public static final String IPCEX34 = "";
    public static final String IPCEX35 = "";
    public static final String IPCEX36 = "";
    public static final String IPCEX37 = "";
    public static final String IPCEX38 = "";
    public static final String IPCEX39 = "";
    public static final String IPCEX40 = "";
    public static final String IPCEX41 = "";
    public static final String IPCEX42 = "";
    public static final String IPCEX43 = "";
    public static final String IPCEX44 = "";
    public static final String IPCEX45 = "";
    public static final String IPCEX46 = "";
    public static final String IPCEX47 = "";
    public static final String IPCEX48 = "";
    public static final String IPCEX49 = "";
    public static final String IPCEX50 = "";
    public static final String IPCEX51 = "";
    public static final String IPCEX52 = "";
    public static final String IPCEX53 = "";
    public static final String IPCEX54 = "";
    public static final String IPCEX55 = "";
    public static final String IPCEX56 = "";
    public static final String IPCEX57 = "";
    public static final String IPCEX58 = "";
    public static final String IPCEX59 = "";
    public static final String IPCEX60 = "Parameter config is missing";
    public static final String IPCEX61 = "";
    public static final String IPCEX62 = "";
    public static final String IPCEX63 = "";
    public static final String IPCEX64 = "";
    public static final String IPCEX65 = "";
    public static final String IPCEX66 = "";
    public static final String IPCEX67 = "Encoding UTF-8 not supported";
	public static final String IPCEX68 = "Error including conflict handling shortcuts";
	public static final String IPCEX69 = "";
	public static final String IPCEX70 = "IPC-Error 70: Error while including page ";
    
}
