/*
 * Created on 03.08.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.ipc.webui.pricing.constants;

/**
 * @author 
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public interface IPCDebugMessages {
    
    public static final String MSG1 = "Removed UIContext from user session.";
    public static final String MSG2 = "Successfully created and added UIContext to user session.";

}
