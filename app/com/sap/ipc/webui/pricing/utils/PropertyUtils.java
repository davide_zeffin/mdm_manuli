/************************************************************************

	Copyright (c) 2001 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.ipc.webui.pricing.utils;

import java.lang.reflect.*;

/**
 * Facade for deprecated class org.apache.struts.util.PropertyUtils
 */
public class PropertyUtils {

    public static void copyProperties(Object dest, Object orig)
		throws  IllegalAccessException,
			    InvocationTargetException,
				NoSuchMethodException{
		org.apache.commons.beanutils.PropertyUtils.copyProperties(dest, orig);
	}

    public static Object getProperty(Object bean, String name)
		throws  IllegalAccessException,
			    InvocationTargetException,
				NoSuchMethodException
    {
        return org.apache.commons.beanutils.PropertyUtils.getProperty(bean, name);
    }

}