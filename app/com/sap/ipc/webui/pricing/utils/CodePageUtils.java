/************************************************************************

	Copyright (c) 2001 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.ipc.webui.pricing.utils;

import java.util.Locale;
import java.util.Hashtable;

public class CodePageUtils {

	private static Hashtable _isoLanguage2HtmlCharset = initIsoLanguage2HtmlCharset();

	private static Hashtable initIsoLanguage2HtmlCharset(){
		Hashtable map = new Hashtable();
		map.put("EN", "ISO8859_1");
		map.put("DE", "ISO8859_1");
		map.put("FR", "ISO8859_1");
		map.put("ES", "ISO8859_1");
		map.put("PT", "ISO8859_1");
		map.put("IT", "ISO8859_1");
		map.put("SI", "ISO8859_2");
		map.put("RU", "ISO8859_5");
		map.put("BG", "ISO8859_5");
		map.put("TR", "ISO8859_9");
		map.put("EL", "ISO8859_7");
		map.put("HE", "ISO8859_8");
		map.put("JA", "shift_jis");
		map.put("ZF", "Big5");
		map.put("ZH", "GB2312");
		map.put("KO", "EUC-KR");
		return map;
	};

	//TODO: delegate this to sxe.util.converter.converter
	public static String getHtmlCharsetForLocale(Locale locale){
		return (String)_isoLanguage2HtmlCharset.get(locale.getLanguage().toUpperCase());
	}

}