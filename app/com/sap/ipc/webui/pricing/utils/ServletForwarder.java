/************************************************************************

	Copyright (c) 2002 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.ipc.webui.pricing.utils;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.Hashtable;
import java.util.Enumeration;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sap.ipc.webui.pricing.model.*;


public class ServletForwarder {

	private HttpServletRequest  _request;
	private HttpServletResponse _response;
	private UIContext           _uIContext;

	private String              _contextPath;

	private String              _ipcServer;
	private String              _ipcPort;
	private String              _ipcSessionId;
	private String              _ipcDocumentId;
	private String              _ipcItemId;
	private String              _returnAddress;
	private Hashtable           _additionalParams;


    public ServletForwarder(HttpServletRequest  request,
							HttpServletResponse response,
							UIContext uIContext) {

		_request = request;
		_response = response;
		_uIContext = uIContext;

        _contextPath = request.getContextPath();
		//get all required request-parameters
		// server
//        _ipcServer        = ((com.sap.spc.remote.client.object.imp.DefaultIPCSession)uIContext.getIPCSession()).getClient().getHost();
//		// port
//		_ipcPort          = Integer.toString(((com.sap.spc.remote.client.object.imp.DefaultIPCSession)uIContext.getIPCSession()).getClient().getPort());
//		// serssionId
//        _ipcSessionId     = ((com.sap.spc.remote.client.object.imp.DefaultIPCSession)uIContext.getIPCSession()).getClient().getSessionId();
//		// documentId
        _ipcDocumentId    = uIContext.getDocumentId();
		// itemId
        _ipcItemId        = uIContext.getItemId();
		// returnAddress
        _returnAddress    = uIContext.getInitialAddress();
		// additional Parameters
		_additionalParams = new Hashtable();

    }

	public void setContextPath(String contextPath){
		_contextPath = contextPath;
	}
	
	public void setRequestVariable(String name,Object value) {
		if (_request!=null) {
			_request.setAttribute(name,value);
		}
	}

	public void setAdditionalParam(String name, String value){
		_additionalParams.put(name, value);
	}

	public void forward(String targetAddress) throws IOException,ServletException {

 		//forward to pricing analysis servlet
		String urlString = "";
		urlString = urlString+_contextPath+targetAddress;
		urlString = urlString+"?server="+_ipcServer;
		urlString = urlString+"&port="+_ipcPort;
		urlString = urlString+"&sessionId="+_ipcSessionId;
        urlString = urlString+"&documentId="+_ipcDocumentId;
        urlString = urlString+"&itemId="+_ipcItemId;
		Enumeration enum = _additionalParams.keys();
		while(enum.hasMoreElements()){
			String name = (String)enum.nextElement();
			String value = (String)_additionalParams.get(name);
	        urlString = urlString+"&"+name+"="+value;
		}
        urlString = urlString+"&returnAddress="+URLEncoder.encode(_returnAddress);
        //Since some variables are needed to be added to the request object
        //and these can not be passed as get parameters the sendRedirect part is commented and
        //requestDispatcher is used.
		RequestDispatcher dispatcher = _request.getRequestDispatcher(urlString);
        dispatcher.forward(_request, _response);
		//_response.sendRedirect(urlString);
	}
}