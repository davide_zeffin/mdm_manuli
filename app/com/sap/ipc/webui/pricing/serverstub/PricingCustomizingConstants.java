/*******************************************************************************

	Copyright (c) 2003 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works based
	upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any warranty
	about the software, its performance or its conformity to any specification.

*******************************************************************************/
package com.sap.ipc.webui.pricing.serverstub;


/**
 * customizing constants
 */
public final class PricingCustomizingConstants extends CustomizingConstants {

    public static final class CalculationRule {
        // calculation rules (KRECH)
        public static final char PERCENTAGE = 'A';
        public static final char FIXED_AMOUNT = 'B';
        public static final char QUANTITY_DEP = 'C';
        public static final char GROSS_WEIGHT_DEP = 'D';
        public static final char NET_WEIGHT_DEP = 'E';
        public static final char VOLUME_DEP = 'F';
        public static final char FORMULA = 'G';
        public static final char PERCENTAGE_IN_HUNDREDS = 'H';
		public static final char PERCENTAGE_TRAVEL_EXPENSES = 'I';
		public static final char POINTS = 'L';
        public static final char MONTHLY = 'M';
        public static final char YEARLY = 'N';
        public static final char DAILY = 'O';
        public static final char WEEKLY = 'P';
        public static final char MULTIDIMENSIONAL = 'T';
        public static final char PERCENTAGE_FINANCING = 'U';
        public static final char COMMODITY_FORMULA = 'Q';
		public static final char INITIAL = ' ';
        
        private static final String FIXED_AMOUNT_OR_PERCENTAGE = "ABHIU";
        private static final String FIXED_AMOUNT_OR_MULTIDIMENSIONAL = new String(new char[] {
                                                                                      FIXED_AMOUNT, MULTIDIMENSIONAL
                                                                                  });
        private static final String PERCENTAGES = "AHIU";
        private static final String FIXED_AMOUNT_OR_PERCENTAGE_OR_FORMULA = "ABGHIU";
        private static final String RELATIVE = "CDEF";
        private static final String WEIGHT_DEPENDENT = "DE";
        private static final String PHYSICAL_UNIT_DEPENDENT = "DEFL";
        private static final String TIME_DEP = "MNOP";

        public static final boolean isPercentage(char calculationRule) {
            return PERCENTAGES.indexOf( calculationRule) != -1;
        }

        public static final boolean isFixedAmountOrPercentage(char calculationRule) {
            return FIXED_AMOUNT_OR_PERCENTAGE.indexOf(calculationRule) != -1;
        }

        public static final boolean isFixedAmountOrMultidimensional(char calculationRule) {
            return FIXED_AMOUNT_OR_MULTIDIMENSIONAL.indexOf( calculationRule) != -1;
        }

        //used
        public static final boolean isPhysicalUnitDependent(char calculationRule) {
            return PHYSICAL_UNIT_DEPENDENT.indexOf( calculationRule) != -1;
        }

        public static boolean isWeightDependent(char calculationRule) {
            return WEIGHT_DEPENDENT.indexOf( calculationRule) != -1;
        }

        public static final boolean isFixedAmountOrPercentageOrFormula(char calculationRule) {
            return FIXED_AMOUNT_OR_PERCENTAGE_OR_FORMULA.indexOf( calculationRule) != -1;
        }

        //used
        public static final boolean isRelative(char calculationRule) {
            return RELATIVE.indexOf( calculationRule) != -1;
        }

        public static final boolean isTimeDependent(char calculationRule) {
            return TIME_DEP.indexOf( calculationRule) != -1;
        }
    }

    public static final class Category {
        // category (KNTYP)
        public static final char VPRS = 'G';
        public static final char SKONTO = 'E';
        public static final char FREIGHT = 'F';

        // ...
        public static final char VARIANTS = 'O';

        // ....
        public static final char DOWN_PAYMENT = 'e';
        //used
        public static final char FREE_GOODS_INCLUSIVE = 'f';

        // TODO:
        //public static final String  COST                             = "GST";
        public static final char INITIAL = ' ';
    }

}
