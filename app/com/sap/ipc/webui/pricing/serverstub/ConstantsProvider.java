/*******************************************************************************

	Copyright (c) 2003 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works based
	upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any warranty
	about the software, its performance or its conformity to any specification.

*******************************************************************************/
package com.sap.ipc.webui.pricing.serverstub;

import java.util.Map;

/**
 * customizing constants
 */
public final class ConstantsProvider {
    private static Map applicationNameMap = null;

    public static synchronized String getApplicationName(String alias) {
        return (String) applicationNameMap.get(alias);
    }
}
