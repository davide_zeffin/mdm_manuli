/************************************************************************

	Copyright (c) 2001 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.ipc.webui.pricing.test;

import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.ipc.webui.pricing.UIConstants;
import com.sap.ipc.webui.pricing.action.IPCBaseAction;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.ipc.IPCBOManager;
import com.sap.isa.core.Constants;
import com.sap.isa.core.InitAction;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.EComBaseAction;
import com.sap.spc.remote.client.ClientException;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCDocument;
import com.sap.spc.remote.client.object.IPCDocumentProperties;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.IPCItemProperties;
import com.sap.spc.remote.client.object.IPCSession;
import com.sap.spc.remote.client.object.OriginalRequestParameterConstants;
import com.sap.spc.remote.client.object.imp.DimensionalValue;
import com.sap.spc.remote.client.object.imp.IPCClientObjectFactory;
import com.sap.spc.remote.client.object.imp.tcp.TCPIPCItemReference;
import com.sap.spc.remote.client.object.imp.tcp.TcpDefaultIPCItem;


public final class TestPricingConditionPanel extends EComBaseAction {

	private static final IsaLocation log =
						   IsaLocation.getInstance(InitAction.class.getName());

	private IPCClientObjectFactory factory =
		IPCClientObjectFactory.getInstance();

	/*Initialize Pricing Panel
	  Parameters provided by request:
	  - server          IPC-Server
	  - port            Port of the IPC-Server
	  - sessionId       Id of the IPCSession to be connect to
	  - documentId      Id of the document to be accessed
	  - itemId          Id of the item to be accessed
	  - SAPEvent        "true", if changes should fire a "SAP Event" - default: "false"
	  - mode            switches between edit ("Edit") and display mode ("Display")
	*/

	// --------------------------------------------------------- Public Methods

	public ActionForward ecomPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack)
		throws IOException, ServletException, CommunicationException {


		// Extract attributes we will need
		// parameter server
		String server = request.getParameter(UIConstants.Request.Parameter.SERVER);
		// parameter port
		String port = request.getParameter(UIConstants.Request.Parameter.PORT);
		// parameter client
		String mandt = request.getParameter(UIConstants.Request.Parameter.CLIENT);
		// parameter mode
		String mode = request.getParameter(UIConstants.Request.Parameter.MODE);
		//xcm configuration
		String xcmConfiguration = request.getParameter(Constants.XCM_CONFIGURATION_RP);
		if (!IPCBaseAction.isSet(xcmConfiguration)) {
			xcmConfiguration = request.getParameter(Constants.XCM_SCENARIO_RP);
		}
		String decimalSeparator = request.getParameter(UIConstants.Request.Parameter.DECIMAL_SEPARATOR);
		String groupingSeparator = request.getParameter(UIConstants.Request.Parameter.GROUPING_SEPARATOR);
		
		//Dispatcher mode
		boolean isDispatcher=false;

		IPCBOManager ipcBoManager =
			(IPCBOManager) userSessionData.getBOM(IPCBOManager.NAME);
		if (ipcBoManager == null) {
			log.error("No IPC BOM in session data!");
			return (mapping.findForward("internalError"));
		}
		
		try{
		//create IPCClient and IPCSession
		    TCPIPCItemReference itemReference = new TCPIPCItemReference(server, port, isDispatcher, "UnicodeLittle", "", "", "");
			IPCClient client = IPCClientObjectFactory.getInstance().newIPCClient(mandt, "ISA", itemReference);

			// create document+item if
			IPCDocumentProperties docProp = getDocumentProperties(request);
			IPCDocument ipcDocument = client.createIPCDocument(docProp);

			if (ipcDocument == null)
				throw new IPCException(new ClientException("500", "Document could not be created"));
				
		    IPCItemProperties itemProperties = getItemProperties(request);
		    IPCItem ipcItem = TcpDefaultIPCItem.createItems(ipcDocument, new IPCItemProperties[]{itemProperties})[0];
			IPCSession ipcSession = client.getIPCSession();
			String serverHost = ((TCPIPCItemReference)client.getItemReference()).getHost();
			String serverPort = ((TCPIPCItemReference)client.getItemReference()).getPort();

			// Forward control to the showPricingConditionPanel action
				String forward = "/initializePricingConditionPanel.do?" + 
								  UIConstants.Request.Parameter.SERVER + "=" + serverHost + "&" +
								  UIConstants.Request.Parameter.PORT + "=" + serverPort + "&" +
			                      Constants.XCM_CONFIGURATION_RP + "=" + xcmConfiguration + "&" +
								  UIConstants.Request.Parameter.SESSION_ID + "=" + ipcSession.getSessionId() + "&" +
								  UIConstants.Request.Parameter.DOCUMENT_ID + "=" + ipcDocument.getId() + "&";
								  String item = request.getParameter("item");
								  if (item != null && item.equals("T")){
								     forward+= UIConstants.Request.Parameter.ITEM_ID + "=" + ipcItem.getItemId() + "&";
								  }
								  forward+= UIConstants.Request.Parameter.MODE + "=" + mode + "&" +
								  OriginalRequestParameterConstants.PERFORM_PRICING_ANALYSIS + "=" + (docProp.getPerformPricingAnalysis()?"Y":"N") + "&" + 
								  UIConstants.Request.Parameter.DECIMAL_SEPARATOR + "=" + decimalSeparator + "&" +
								  UIConstants.Request.Parameter.GROUPING_SEPARATOR + "=" + groupingSeparator;
			if (log.isDebugEnabled())
				servlet.log(" Forwarding to " + forward);
		                      
			return new ActionForward(forward, true);
		}catch(com.sap.spc.remote.client.object.IPCException e) {
			request.setAttribute(UIConstants.Request.Parameter.IPC_EXCEPTION, e);
			return (mapping.findForward("ipcFailure"));
		}
		
	}

	/*
	 * fill document properties from the request into an IPCDocumentProperties object
	 * @return a IPCDocumentProperties
	 */
	protected IPCDocumentProperties getDocumentProperties(HttpServletRequest request) {
		IPCDocumentProperties p = factory.newIPCDocumentProperties();

		p.setLanguage(request.getParameter(OriginalRequestParameterConstants.LANGUAGE));
		p.setCountry(request.getParameter(OriginalRequestParameterConstants.COUNTRY));
		p.setDepartureCountry(request.getParameter(OriginalRequestParameterConstants.DEPARTURE_COUNTRY));
		p.setDistributionChannel(request.getParameter(OriginalRequestParameterConstants.DISTRIBUTION_CHANNEL));
		p.setDocumentCurrency(request.getParameter(OriginalRequestParameterConstants.DOCUMENT_CURRENCY_UNIT));
		p.setLocalCurrency(request.getParameter(OriginalRequestParameterConstants.LOCAL_CURRENCY_UNIT));
		p.setSalesOrganisation(request.getParameter(OriginalRequestParameterConstants.SALES_ORGANISATION));
		p.setDistributionChannel(request.getParameter(OriginalRequestParameterConstants.DISTRIBUTION_CHANNEL));
		p.setPricingProcedure(request.getParameter(OriginalRequestParameterConstants.PROCEDURE_NAME));
		String performPricingAnanlysis = request.getParameter(OriginalRequestParameterConstants.PERFORM_PRICING_ANALYSIS);
		if (IPCBaseAction.isSet(performPricingAnanlysis)){
			p.setPerformPricingAnalysis(performPricingAnanlysis.equals("Y") ? true : false);
		}

		// pricing issue:
		// setting of further document attributes
		Enumeration parameterNames = request.getParameterNames();
		while (parameterNames.hasMoreElements()) {
			String nextParam = (String) parameterNames.nextElement();
			if (!nextParam.startsWith(OriginalRequestParameterConstants.GENERIC_DOC_ATTR))
				continue;
			String value = request.getParameter(nextParam);
			// get attribute value
			if (value == null)
				continue;
			// extract attribute name
			String name = null;
			name =
				nextParam.substring(
			OriginalRequestParameterConstants.GENERIC_DOC_ATTR.length(),
					nextParam.length());
			p.addCreationCommandParameter(name, value);
		}

		return p;
	}
	
	/*
	 * fill item properties from the request into an IPCItemProperties object
	 * @return a IPCItemProperties
	 */
	protected IPCItemProperties getItemProperties(HttpServletRequest request) {
		IPCItemProperties p = factory.newIPCItemProperties();
		//timestamps
		HashMap timestamps = new HashMap();

			// the request parameter "productIdERP" is used in ERP to submit the productId
			p.setProductId(request.getParameter(OriginalRequestParameterConstants.PRODUCT_ID_ERP));
//				IPCBaseAction.getParameterAttributeOrScenarioParameter(
//					request,
//					PRODUCT_ID,
//					""));
			String baseQuantityValue = request.getParameter(OriginalRequestParameterConstants.BASE_QUANTITY_VALUE);
			String baseQuantityUnit = request.getParameter(OriginalRequestParameterConstants.BASE_QUANTITY_UNIT);

			String salesQuantityValue = request.getParameter(OriginalRequestParameterConstants.SALES_QUANTITY_VALUE);
//				IPCBaseAction.getParameterAttributeOrScenarioParameter(
//					request,
//					SALES_QUANTITY_VALUE,
//					"");
			String salesQuantityUnit = request.getParameter(OriginalRequestParameterConstants.SALES_QUANTITY_UNIT);
//				IPCBaseAction.getParameterAttributeOrScenarioParameter(
//					request,
//					SALES_QUANTITY_UNIT,
//					"");


			if (IPCBaseAction.isSet(baseQuantityValue)
				&& IPCBaseAction.isSet(baseQuantityUnit)) {
				p.setBaseQuantity(
					new DimensionalValue(
						baseQuantityValue,
						baseQuantityUnit));
			}

			if (IPCBaseAction.isSet(salesQuantityValue)
				&& IPCBaseAction.isSet(salesQuantityUnit)) {
				p.setSalesQuantity(
					new DimensionalValue(
						salesQuantityValue,
						salesQuantityUnit));
			}
            
			String productGuid = request.getParameter(OriginalRequestParameterConstants.PRODUCT_GUID);
			if (IPCBaseAction.isSet(productGuid)){
				p.setProductGuid(productGuid);
			}
            
			String pricingDate = request.getParameter(OriginalRequestParameterConstants.PRICING_DATE);
			if (IPCBaseAction.isSet(pricingDate)) {
					p.setDate(pricingDate);
			}else {
				log.warn("Parameter " + OriginalRequestParameterConstants.PRICING_DATE + " not passed!"); 
			}
//				IPCBaseAction.getParameterAttributeOrScenarioParameter(
//					request,
//					PRICING_DATE,
//					""));
			String pricingRelevant = request.getParameter(OriginalRequestParameterConstants.PRICING_RELEVANT);
			if (IPCBaseAction.isSet(pricingRelevant)) {
				p.setPricingRelevant(pricingRelevant.equals("Y") ? Boolean.TRUE : Boolean.FALSE);
			}
			String performPricingAnanlysis = request.getParameter(OriginalRequestParameterConstants.PERFORM_PRICING_ANALYSIS);
			if (IPCBaseAction.isSet(performPricingAnanlysis)){
				p.setPerformPricingAnalysis(performPricingAnanlysis.equals("Y") ? true : false);
			}

			String kbLogsys = request.getParameter(OriginalRequestParameterConstants.KB_LOGSYS);
//				IPCBaseAction.getParameterAttributeOrScenarioParameter(
//					request,
//					KB_LOGSYS,
//					"");
			if (IPCBaseAction.isSet(kbLogsys)) {
				p.setKbLogSys(kbLogsys);
			}
			String kbName = request.getParameter(OriginalRequestParameterConstants.KB_NAME);
//				IPCBaseAction.getParameterAttributeOrScenarioParameter(
//					request,
//					KB_NAME,
//					"");
			if (IPCBaseAction.isSet(kbName)) {
				p.setKbName(kbName);
			}
			String kbVersion = request.getParameter(OriginalRequestParameterConstants.KB_VERSION);
//				IPCBaseAction.getParameterAttributeOrScenarioParameter(
//					request,
//					KB_VERSION,
//					"");
			if (IPCBaseAction.isSet(kbVersion)) {
				p.setKbVersion(kbVersion);
			}
			String kbProfile = request.getParameter(OriginalRequestParameterConstants.KB_PROFILE);
//				IPCBaseAction.getParameterAttributeOrScenarioParameter(
//					request,
//					KB_PROFILE,
//					"");
			if (IPCBaseAction.isSet(kbProfile)) {
				p.setKbProfile(kbProfile);
			}

			// pricing issue:
			// setting of header and item attributes AND
			// setting of further pricing/taxing attributes
			Enumeration parameterNames = request.getParameterNames();
//TODO:remove           Enumeration attributeNames = request.getAttributeNames();
			Map headerAttributes = new HashMap();
			Map itemAttributes = new HashMap();
			while (parameterNames.hasMoreElements()) {
				String nextParam = (String) parameterNames.nextElement();
				if (!nextParam.startsWith(OriginalRequestParameterConstants.PRC_HDR_ATTR)
					&& !nextParam.startsWith(OriginalRequestParameterConstants.PRC_ITM_ATTR)
					&& !nextParam.startsWith(OriginalRequestParameterConstants.GENERIC_ITM_ATTR)
				    && !nextParam.startsWith(OriginalRequestParameterConstants.PRC_TIMESTAMP))
					continue;
				String value = request.getParameter(nextParam);
				// get attribute value
				if (value == null)
					continue;
				// extract attribute name
				String name = null;
				// header
				if (nextParam.startsWith(OriginalRequestParameterConstants.PRC_HDR_ATTR)) {
					name =
						nextParam.substring(
					OriginalRequestParameterConstants.PRC_HDR_ATTR.length(),
							nextParam.length());
					headerAttributes.put(name, value);
					// item
				} else if (nextParam.startsWith(OriginalRequestParameterConstants.PRC_ITM_ATTR)) {
					name =
						nextParam.substring(
					OriginalRequestParameterConstants.PRC_ITM_ATTR.length(),
							nextParam.length());
					itemAttributes.put(name, value);
					// generic item attribute
				} else if (nextParam.startsWith(OriginalRequestParameterConstants.GENERIC_ITM_ATTR)) {
					name =
						nextParam.substring(
					OriginalRequestParameterConstants.GENERIC_ITM_ATTR.length(),
							nextParam.length());
					p.addCreationCommandParameter(name, value);
				} else if (nextParam.startsWith(OriginalRequestParameterConstants.PRC_TIMESTAMP)) {
					name =
					    nextParam.substring(
					        OriginalRequestParameterConstants.PRC_TIMESTAMP.length(),
					        nextParam.length());
					timestamps.put(name, value);
				}
			}
			if (headerAttributes.size() > 0)
				p.setHeaderAttributes(headerAttributes);
			if (itemAttributes.size() > 0)
				p.setItemAttributes(itemAttributes);
			if (timestamps.size() > 0) {
				p.setConditionTimestamps(timestamps);
			}
				
		return p;
	}
	

}