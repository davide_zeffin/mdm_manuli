/************************************************************************

	Copyright (c) 2001 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.ipc.webui.pricing.model;

import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.DimensionalValue;
import com.sap.spc.remote.client.object.IPCPricingConditionSet;

public class PricingItem implements PricingContainer{

	private IPCItem _item;

    public PricingItem(IPCItem item) {
		_item = item;
    }

	public  void pricing(char mode) throws IPCException{
		_item.pricing(mode);
	}

	public  void pricingComplete() throws IPCException{
		_item.getDocument().pricing();
	}

	public  boolean getPerformPricingAnalysis() throws IPCException{
		return _item.getPerformPricingAnalysis();
	}

	public  boolean isGroupConditionProcessingEnabled() throws IPCException{
		return _item.getDocument().isGroupConditionProcessingEnabled();
	}

	public  DimensionalValue getNetWeight() throws IPCException{
		return _item.getNetWeight();
	}

	public DimensionalValue getBaseQuantity() throws IPCException{
		return _item.getBaseQuantity();
	}

	public DimensionalValue getVolume() throws IPCException{
		return _item.getVolume();
	}

	public DimensionalValue getSalesQuantity() throws IPCException{
		return _item.getSalesQuantity();
	}

	public DimensionalValue getGrossWeight() throws IPCException{
		return _item.getGrossWeight();
	}

	public String getExchangeRate() throws IPCException{
		return _item.getExchangeRate();
	}

	public String getLocalCurrency() throws IPCException{
		return _item.getDocument().getLocalCurrency();
	}

	public  IPCPricingConditionSet getPricingConditions() throws IPCException{
		return _item.getPricingConditions();
	}

}