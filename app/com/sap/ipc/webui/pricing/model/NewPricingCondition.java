/************************************************************************

	Copyright (c) 2001 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.ipc.webui.pricing.model;

import java.util.ArrayList;

import com.sap.spc.remote.client.object.IPCException;

public class NewPricingCondition extends PricingCondition {

	private String _stepNo = null;
	private String _counter = null;

	NewPricingCondition(PricingConditionPanel pricingConditionPanel, UIContext uiContext){
		super(pricingConditionPanel, null, uiContext);
//		StringBuffer buffer = new StringBuffer("Z");//last in sorted list
//		_stepNo = buffer.append(new java.rmi.server.UID().toString()).toString();
        ArrayList conditions = pricingConditionPanel.getPricingConditions();
        if (conditions.size() >0) {
	        PricingCondition lastCondition = (PricingCondition)conditions.get(conditions.size() - 1);
			_stepNo = Integer.toString(Integer.parseInt(lastCondition.getStepNo()) + 1);
		}else {
			_stepNo = "000";
		}
		_counter = "00";
		//overwrite the key that has been created in the constructor of the 
		//superclass
		this.key = new Key(_stepNo, _counter);
	}

	public String getStepNo(){
		return _stepNo;
	}

	public String getCounter(){
		return _counter;
	}

	public String getConditionTypeName(){
		return EMPTY_STRING;
	}

	public String getDescription(){
		return EMPTY_STRING;
	}

	public void setConditionRateValue(String conditionRateValue){
		if (conditionRateValue != null){
	    	_pricingConditionPanel.setInvalid();
		}
	}

	public String getConditionRateValue(){
		return EMPTY_STRING;
	}

	public void setConditionCurrency(String conditionCurrency){
		if (conditionCurrency != null){
	    	_pricingConditionPanel.setInvalid();
		}
	}

	public String getConditionCurrency(){
		return EMPTY_STRING;
   }

	public String getConditionRate(){
		return _getUnitValue(getConditionRateValue(), getConditionCurrency());
	}

	public void setPricingUnitValue(String pricingUnitValue){
		if (pricingUnitValue != null){
	    	_pricingConditionPanel.setInvalid();
		}
	}

	public String getPricingUnitValue(){
		return EMPTY_STRING;
	}

	public void setPricingUnitUnit(String pricingUnitUnit){
		if (pricingUnitUnit != null){
		  _pricingConditionPanel.setInvalid();
		}
	}

	public String getPricingUnitUnit(){
		return EMPTY_STRING;
	}

	public String getPricingUnit(){
		return _getUnitValue(getPricingUnitValue(), getPricingUnitUnit());
	}

	public String getPricingUnitDimension(){
		return EMPTY_STRING;
	}

	public void setConditionValueValue(String conditionValueValue){
		if (conditionValueValue != null){
	    	_pricingConditionPanel.setInvalid();
		}
	}

	public String getConditionValueValue(){
		return EMPTY_STRING;
	}

	public String getDocumentCurrency(){
		return EMPTY_STRING;
	}

	public String getConditionValue(){
		return _getUnitValue(getConditionValueValue(), getDocumentCurrency());
	}

	public String getConversionNumerator(){
		return EMPTY_STRING;
	}

	public String getConversionDenominator(){
		return EMPTY_STRING;
	}

	public String getConversionExponent(){
		return EMPTY_STRING;
	}

	public String getConditionBaseValue(){
		return EMPTY_STRING;
	}

	public String getExchangeRate(){
		return EMPTY_STRING;
	}

	public String getDirectExchangeRate(){
		return EMPTY_STRING;
	}

	public boolean isDirectExchangeRateUsed(){
		return false;
	}

	public String getFactor(){
		return EMPTY_STRING;
	}

	public boolean isStatistical(){
		return false;
	}

	public char getInactive(){
		return ' ';
	}

	public char getConditionClass(){
		return ' ';
	}

	public char getCalculationType(){
		return ' ';
	}

	public char getConditionCategory(){
		return ' ';
	}

	public char getConditionControl(){
		return ' ';
	}

	public char getConditionOrigin(){
		return ' ';
	}

	public boolean isChangeOfRatesAllowed(){
		return true;
	}

	public boolean isChangeOfPricingUnitsAllowed(){
		return true;
	}

	public boolean isChangeOfValuesAllowed(){
		return true;
	}

	public boolean isDeletionAllowed(){
		return true;
	}

	public String getConversionUnit1() throws IPCException{
		return EMPTY_STRING;
	}

	public String getConversionUnit2() throws IPCException{
		return EMPTY_STRING;
	}

	public ValueSelection getAvailablePricingUnitUnits() throws IPCException{
		return _pricingConditionPanel.getAllPhysicalUnits();
	}


	/* (non-Javadoc)
	 * @see com.sap.ipc.webui.pricing.model.PricingCondition#getScaleType()
	 */
	public char getScaleType() {
		return ' ';
	}

}