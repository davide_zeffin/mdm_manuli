/************************************************************************

	Copyright (c) 2001 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.ipc.webui.pricing.model;

import com.sap.spc.remote.client.object.IPCException;

import java.util.TreeSet;
import java.util.Collection;


public class ValueSelectionInclValue extends ValueSelection{

	private ValueSelection  _valueSelection = null;
	private String          _additionalValue = null;

	ValueSelectionInclValue(ValueSelection valueSelection, String additionalValue){
		_valueSelection = valueSelection;
		_additionalValue = additionalValue;
	}

	public Collection getValues() throws IPCException{
		if (_values == null)
			(_values =  new TreeSet(_valueSelection.getValues())).add(_additionalValue);
	    return _values;
	}

	public Collection getLabels() throws IPCException{
		if (_labels == null){
			_labels = new TreeSet(_valueSelection.getLabels());
			if (_values.size() > _valueSelection.getValues().size())
				_labels.add(_additionalValue);
		}
	    return _labels;
	}


}