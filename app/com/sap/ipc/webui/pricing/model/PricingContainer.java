/************************************************************************

	Copyright (c) 2001 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.ipc.webui.pricing.model;

import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.DimensionalValue;
import com.sap.spc.remote.client.object.IPCPricingConditionSet;


public interface PricingContainer {

    /** Method for pricing of the item.
	 */
	public  void pricing(char mode) throws IPCException;

	public  void pricingComplete() throws IPCException;

    /** Returns the item's attribute 'performPricingAnalysis'
	 * @return		performPricingAnalysis indicator
	 */
	public  boolean getPerformPricingAnalysis() throws IPCException;

	public  boolean isGroupConditionProcessingEnabled() throws IPCException;

    /** Returns net weight of the item
	 * @return		NetWeight
	 */
	public  DimensionalValue getNetWeight() throws IPCException;

    /** Returns the item's base quantity
	 * @return		base Quantity
	 */
	public DimensionalValue getBaseQuantity() throws IPCException;

    /** Returns item's volume
	 */
	public DimensionalValue getVolume() throws IPCException;

    /** Returns the sales quantity of this item.
	 */
	public DimensionalValue getSalesQuantity() throws IPCException;

    /** Returns the gross weight of item
	 */
	public DimensionalValue getGrossWeight() throws IPCException;

	/** Returns the currency exchange rate
	 * @return      exchange rate
	 */
	public String getExchangeRate() throws IPCException;

	/** Returns local currency of the document
	 *  @return			String		localCurrency
 	 */
	public String getLocalCurrency() throws IPCException;

	/** Returns the pricing conditions of this item.
	 * @return      set of pricing conditions.
	 */
	public  IPCPricingConditionSet getPricingConditions() throws IPCException;

}