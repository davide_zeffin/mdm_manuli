/************************************************************************

	Copyright (c) 2001 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.ipc.webui.pricing.model;

import com.sap.spc.remote.client.object.IPCPricingConditionSet;
import com.sap.spc.remote.client.object.IPCPricingCondition;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCMessage;

import java.util.SortedMap;
import java.util.TreeMap;
import java.util.Iterator;
import java.util.Collection;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Locale;

public class PricingConditionPanel {

	private PricingContainer  _pricingContainer;
	private IPCPricingConditionSet _ipcPricingConditionSet = null;
	private SortedMap _pricingConditions = null;
	private ValueSelection _availableConditionTypeNames = null;
	private ValueSelection _availableQuantityUnits = null;
	private ValueSelection _allCurrencyUnits = null;
	private ValueSelection _allPhysicalUnits = null;
	private boolean _isPricingAnalysisActive = false;
	private SAPEventStack _sapEventStack = null;
	private boolean _isGroupProcessingActive = false;
	private boolean _isInvalid = false;
	private boolean _showBackButton = false;

	//performance issue -> keep values of properties that will never change in this context
	// - otherwise unnecessary remote commands my be triggered
	private String _salesQuantity = null;
	private String _weightUnit = null;
	private String _volumeUnit = null;
	private String _exchangeRate = null;
	private String _localCurrency = null;
	private UIContext _uiContext;

	public PricingConditionPanel(   PricingContainer  pricingContainer,
								    boolean isSAPEventActive,
								    UIContext uiContext) throws IPCException{
		_pricingContainer = pricingContainer;
		_ipcPricingConditionSet = pricingContainer.getPricingConditions();
		_uiContext = uiContext;
		_retrievePricingConditions(uiContext);
		_isPricingAnalysisActive = pricingContainer.getPerformPricingAnalysis();
		_sapEventStack = new SAPEventStack(isSAPEventActive);
		_isGroupProcessingActive = pricingContainer.isGroupConditionProcessingEnabled();

		_salesQuantity = pricingContainer.getSalesQuantity()!=null?pricingContainer.getSalesQuantity().getUnit():null;
		_weightUnit = pricingContainer.getGrossWeight()!=null?pricingContainer.getGrossWeight().getUnit():null;
		_volumeUnit = pricingContainer.getVolume()!=null?pricingContainer.getVolume().getUnit():null;
		_exchangeRate = pricingContainer.getExchangeRate();
		_localCurrency = pricingContainer.getLocalCurrency();
	}

	public String getQuantityUnit() throws IPCException{
		return _salesQuantity;
//		return _pricingContainer.getSalesQuantity().getUnit();
	}

	public String getWeightUnit() throws IPCException{
		return _weightUnit;
//		return _pricingContainer.getGrossWeight().getUnit();
	}

	public String getVolumeUnit() throws IPCException{
		return _volumeUnit;
//		return _pricingContainer.getVolume().getUnit();
	}

	public String getExchangeRate() throws IPCException{
		return _exchangeRate;
//		return _pricingContainer.getExchangeRate();
	}

	public String getLocalCurrency() throws IPCException{
		return _localCurrency;
//		return _pricingContainer.getLocalCurrency();
	}

	public void addPricingCondition(String  conditionTypeName,
									String  conditionRateValue,
									String  conditionCurrency,
									String  pricingUnitValue,
									String  pricingUnitUnit,
									String  conditionValueValue,
									String  decimalSeparator,
									String  groupingSeparator) throws IPCException{
		// add only valid entries
		if (conditionTypeName == null || conditionTypeName.equals(PricingCondition.EMPTY_STRING))
			return;
		_isInvalid = true;
		_ipcPricingConditionSet.addPricingCondition(conditionTypeName,
				conditionRateValue,
				conditionCurrency,
				pricingUnitValue,
				pricingUnitUnit,
				conditionValueValue,
				decimalSeparator,
				groupingSeparator);
	}

	public void removePricingCondition(PricingCondition pricingCondition) throws IPCException{
		_isInvalid = true;
		_ipcPricingConditionSet.removePricingCondition( pricingCondition.getStepNo(),
														pricingCondition.getCounter());
	}

	public ValueSelection getAvailableConditionTypeNames() throws IPCException{
		if (_availableConditionTypeNames == null)
			_availableConditionTypeNames =  new ValueSelection(_ipcPricingConditionSet.getAvailableConditionTypeNames());
		return _availableConditionTypeNames;
	}

	public ValueSelection getAllPhysicalUnits() throws IPCException{
		if (_allPhysicalUnits == null)
			_allPhysicalUnits = new ValueSelection(_ipcPricingConditionSet.getAllPhysicalUnits());
		return _allPhysicalUnits;
	}

	public ValueSelection getAvailablePhysicalUnits(String dimensionName) throws IPCException{
		return new ValueSelection(_ipcPricingConditionSet.getAvailablePhysicalUnits(dimensionName));
	}

	public ValueSelection getAvailableQuantityUnits() throws IPCException{
		if (_availableQuantityUnits == null)
			_availableQuantityUnits =  new ValueSelection(_ipcPricingConditionSet.getAvailableQuantityUnits());
		return _availableQuantityUnits;
	}

	public ValueSelection getAllCurrencyUnits() throws IPCException{
		if (_allCurrencyUnits == null)
			_allCurrencyUnits = new ValueSelection(_ipcPricingConditionSet.getAllCurrencyUnits());
		return _allCurrencyUnits;
	}

	public void pricing(char mode) throws IPCException{
		_isInvalid = true;
		_pricingContainer.pricing(mode);
	}

	public void setPricingAnalysis(boolean isPricingAnalysisActive){
		_isPricingAnalysisActive = isPricingAnalysisActive;
	}

	public boolean isPricingAnalysisActive(){
		return _isPricingAnalysisActive;
	}
	
	public void setShowBackButton (boolean showBackButton) {
		this._showBackButton = showBackButton;
	}
	
	public boolean isShowBackButton() {
		return _showBackButton;
	}

	public ArrayList getPricingConditions() throws IPCException{
		if (_isInvalid)
			_retrievePricingConditions(_uiContext);
		ArrayList pricingConditions = new ArrayList(_pricingConditions.values());
		return pricingConditions;
	}

	private void _retrievePricingConditions(UIContext uiContext) throws IPCException{
		try{
			_pricingConditions = new TreeMap();
			IPCPricingCondition[] ipcPricingConditions = _ipcPricingConditionSet.getPricingConditions();
			for(int i=0; i<ipcPricingConditions.length; i++){
				PricingCondition currentCondition = new PricingCondition(this, ipcPricingConditions[i], uiContext);
				_pricingConditions.put(
						currentCondition.getKey(),
						currentCondition);
			}
			_isInvalid = false;
		}catch (IPCException e){
			_isInvalid = true;
			throw e;
		}
	}

	public PricingCondition findPricingCondition(String stepNo, String counter)/* throws IPCException*/{
/*		if (_isInvalid)
			_retrievePricingConditions();*///TODO
		return (PricingCondition)_pricingConditions.get(PricingCondition.createKey(stepNo, counter));
	}

	public SAPEventStack getSAPEventStack(){
		return _sapEventStack;
	}

	public boolean isGroupProcessingActive(){
		return _isGroupProcessingActive;
	}

	public void commit() throws IPCException{
		_ipcPricingConditionSet.commit();
		_pricingContainer.pricingComplete();
	}

	public void appendEmptyRows(int count){
		for (int i=0; i<count; i++){
			PricingCondition newPrCond = new NewPricingCondition(this, _uiContext);
			_pricingConditions.put(newPrCond.getKey(), newPrCond);
		}
	}

	public IPCMessage[] getMessages(){
		return _ipcPricingConditionSet.getMessages();
	}

	void setInvalid(){
		_isInvalid = true;
	}

	void pricingComplete() throws IPCException{
		_pricingContainer.pricingComplete();
	}

	public boolean isHeader(){
		return (_pricingContainer instanceof PricingDocument);
	}

//	private String _getKey(String stepNo, String counter){
//		StringBuffer buffer = new StringBuffer(stepNo);
//		buffer.append("#");
//		buffer.append(counter);
//		return buffer.toString();
//	}
}