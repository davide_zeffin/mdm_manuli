/************************************************************************

	Copyright (c) 2001 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.ipc.webui.pricing.model;

import java.util.Hashtable;

public class SAPEventStack {

	private Hashtable   _stack; //?stack<->hashtable?
	private boolean     _isActive;

	public SAPEventStack(){
		this(true);
	}

	public SAPEventStack(boolean isActive){
		_stack = new Hashtable();
		_isActive = isActive;
	}

	//will be used by struts-action
	public void addEvent(String action, Hashtable data){
		if (_isActive)
			_stack.put(_getKey(action), data);
	}

	public void addEvent(String action){
		addEvent(action, new Hashtable());
	}

	//will be used by FireSAPEvent-tag
	/**
	 * returns the event data(query_table...) to a event type(action...)
	 */
	public Hashtable processEvent(String action){
		Hashtable data = (Hashtable)_stack.get(_getKey(action));
		_stack.remove(_getKey(action));
		return data;
	}

	private String _getKey(String action){
		StringBuffer buffer = new StringBuffer(action);
		return buffer.toString();
	}
}
