/************************************************************************

	Copyright (c) 2001 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.ipc.webui.pricing.model;

import com.sap.spc.remote.client.object.IPCException;

import java.util.TreeSet;
import java.util.Collection;


public class ValueSelectionInclEmptyValue extends ValueSelectionInclValue{

	ValueSelectionInclEmptyValue(ValueSelection valueSelection){
		super(valueSelection, PricingCondition.EMPTY_STRING);
	}
}