/************************************************************************

	Copyright (c) 2001 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.ipc.webui.pricing.model;

import com.sap.spc.remote.client.object.IPCSession;

public class UIContext {

	private String _decimalSeparator;
	private String _groupingSeparator;
	private String                  _documentId         = null;
	private String                  _itemId             = null;
	private String                  _initialAddress     = null;
	private IPCSession              _ipcSession         = null;
	private PricingConditionPanel   _panel              = null;
	private PricingCondition        _pricingCondition   = null;

    public UIContext() {
    }

	public void setDocumentId(String documentId){
		_documentId = documentId;
	}

	public String getDocumentId(){
		return _documentId;
	}

	public void setItemId(String itemId){
		_itemId = itemId;
	}

	public String getItemId(){
		return _itemId;
	}

	public void setInitialAddress(String initialAddress){
		_initialAddress = initialAddress;
	}

	public String getInitialAddress(){
		return _initialAddress;
	}

	public void setIPCSession(IPCSession ipcSession){
		_ipcSession = ipcSession;
	}

	public IPCSession getIPCSession(){
		return _ipcSession;
	}

	public void setPricingConditionPanel(PricingConditionPanel panel){
		_panel = panel;
	}

	public PricingConditionPanel getPricingConditionPanel(){
		return _panel;
	}

	public void setPricingCondition(PricingCondition pricingCondition){
		_pricingCondition = pricingCondition;
	}

	public PricingCondition getPricingCondition(){
		return _pricingCondition;
	}

	/**
	 * @return
	 */
	public String getDecimalSeparator() {
	    return _decimalSeparator;
	}
	
	public void setDecimalSeparator(String decimalSeparator) {
		_decimalSeparator = decimalSeparator;
	}
	
	public String getGroupingSeparator() {
	    return _groupingSeparator;
	}
	
	public void setGroupingSeparator(String groupingSeparator) {
		_groupingSeparator = groupingSeparator;
	}
}