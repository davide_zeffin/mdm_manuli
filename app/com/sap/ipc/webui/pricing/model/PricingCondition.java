/************************************************************************

	Copyright (c) 2001 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.ipc.webui.pricing.model;

import com.sap.ipc.webui.pricing.serverstub.*;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCPricingCondition;

public class PricingCondition {

	public static final String EMPTY_STRING = "";

	private IPCPricingCondition _ipcPricingCondition;
	protected PricingConditionPanel _pricingConditionPanel;
	private ValueSelection _availablePhysicalUnits = null; //will be sorted
	private UIContext _uiContext;
	protected Key key;

	
	PricingCondition(PricingConditionPanel pricingConditionPanel,
							IPCPricingCondition ipcPricingCondition,
							UIContext uiContext){
		_ipcPricingCondition = ipcPricingCondition;
		_pricingConditionPanel = pricingConditionPanel;
		_uiContext = uiContext;
		if (ipcPricingCondition != null) {
			this.key = new Key(ipcPricingCondition.getStepNo(), ipcPricingCondition.getCounter());
		} else {
			this.key = new Key("0", "0");
		}
	}

/*	public String[] getMessages() throws IPCException{
		String[] messages = _ipcPricingCondition.getMessages();
		_pricingConditionPanel.pricingComplete();
		return messages;
	}
*/
	public Key getKey() {
		return key;
	}
	
	public String getStepNo(){
		String result = _ipcPricingCondition.getStepNo();
		return (result == null)?EMPTY_STRING:result;
	}

	public String getCounter(){
		String result = _ipcPricingCondition.getCounter();
		return (result == null)?EMPTY_STRING:result;
	}

	public String getConditionTypeName(){
		String result = _ipcPricingCondition.getConditionTypeName();
		return (result == null)?EMPTY_STRING:result;
	}

	public String getDescription(){
		String result = _ipcPricingCondition.getDescription();
		return (result == null)?EMPTY_STRING:result;
	}

	public void setConditionRateValue(String conditionRateValue){
		if (conditionRateValue != null){
		//	_ipcPricingCondition.setConditionRate(conditionRateValue);
		//		Changed for Manual condition
				 _ipcPricingCondition.setConditionRate(conditionRateValue,_uiContext.getDecimalSeparator(),_uiContext.getGroupingSeparator());

	    	_pricingConditionPanel.setInvalid();
		}
	}

	public String getConditionRateValue(){
		String result = _ipcPricingCondition.getExternalConditionRateValue(_uiContext.getDecimalSeparator(),_uiContext.getGroupingSeparator());
		return (result == null)?EMPTY_STRING:result;
	}

	public void setConditionCurrency(String conditionCurrency){
		if (conditionCurrency != null){
			_ipcPricingCondition.setConditionCurrency(conditionCurrency);
	    	_pricingConditionPanel.setInvalid();
		}
	}

	public String getConditionCurrency(){
		String result = _ipcPricingCondition.getConditionCurrency();
		return (result == null)?EMPTY_STRING:result;
   }

	public String getConditionRate(){
		return _getUnitValue(getConditionRateValue(), getConditionCurrency());
	}

	public void setPricingUnitValue(String pricingUnitValue){
		if (pricingUnitValue != null){
			_ipcPricingCondition.setExternalPricingUnitValue(pricingUnitValue, _uiContext.getDecimalSeparator(), _uiContext.getGroupingSeparator());
	    	_pricingConditionPanel.setInvalid();
		}
	}

	public String getPricingUnitValue(){
		String result = _ipcPricingCondition.getExternalPricingUnitValue(_uiContext.getDecimalSeparator(), _uiContext.getGroupingSeparator());
		return (result == null)?EMPTY_STRING:result;
	}

	public void setPricingUnitUnit(String pricingUnitUnit){
		if (pricingUnitUnit != null){
  		  _ipcPricingCondition.setPricingUnitUnit(pricingUnitUnit);
		  _pricingConditionPanel.setInvalid();
		}
	}

	public String getPricingUnitUnit(){
		String result = _ipcPricingCondition.getPricingUnitUnit();
		return (result == null)?EMPTY_STRING:result;
	}

	public String getPricingUnit(){
		return _getUnitValue(getPricingUnitValue(), getPricingUnitUnit());
	}

	public String getPricingUnitDimension(){
		String result = _ipcPricingCondition.getPricingUnitDimension();
		return (result == null)?EMPTY_STRING:result;
	}

	public void setConditionValueValue(String conditionValueValue){
		if (conditionValueValue != null){
			//_ipcPricingCondition.setConditionValue(conditionValueValue); 
			/*Changes for manual condition rate change */
			_ipcPricingCondition.setConditionValue(conditionValueValue,_uiContext.getDecimalSeparator(), _uiContext.getGroupingSeparator());			
	    	_pricingConditionPanel.setInvalid();
		}
	}

	public String getConditionValueValue(){
		String result = _ipcPricingCondition.getExternalConditionValue(_uiContext.getDecimalSeparator(), _uiContext.getGroupingSeparator());
		return (result == null)?EMPTY_STRING:result;
	}

	public String getDocumentCurrency(){
		String result = _ipcPricingCondition.getDocumentCurrency();
		return (result == null)?EMPTY_STRING:result;
	}

	public String getConditionValue(String decimalSeparator, String groupingSeparator){
		return _getUnitValue(getConditionValueValue(), getDocumentCurrency());
	}

	public String getConversionNumerator(){
		return _ipcPricingCondition.getConversionNumerator();
	}

	public String getConversionDenominator(){
		return _ipcPricingCondition.getConversionDenominator();
	}

	public String getConversionExponent(){
		return _ipcPricingCondition.getConversionExponent();
	}

	public String getConditionBaseValue(){
		return _ipcPricingCondition.getConditionBaseValue();
	}

	public String getExchangeRate(){
		return _ipcPricingCondition.getExchangeRate();
	}

	public String getDirectExchangeRate(){
		return _ipcPricingCondition.getDirectExchangeRate();
	}

	public boolean isDirectExchangeRateUsed(){
		return _ipcPricingCondition.isDirectExchangeRate();
	}

	public String getLocalCurrency() throws IPCException{
		return _pricingConditionPanel.getLocalCurrency();
	}

	public String getFactor(){
		//oder getFactor/getVariantFactor und Logik im JSP
		String variantFactor = _ipcPricingCondition.getVariantFactor();
		variantFactor = (variantFactor == null)?EMPTY_STRING:variantFactor;
		if (!variantFactor.equals(EMPTY_STRING) && !variantFactor.equals("0")) // 0-Value muss im RC entspr. aufbereitet sein
		    return variantFactor;
		if (_ipcPricingCondition.getScaleType() == CustomizingConstants.ScaleType.TO_INTERVAL_SCALE ||
			_ipcPricingCondition.getConditionCategory() == PricingCustomizingConstants.Category.FREE_GOODS_INCLUSIVE){
	    	String factor = _ipcPricingCondition.getFactor();
		    return (factor == null)?EMPTY_STRING:factor;
		}
		return EMPTY_STRING;
	}

	public boolean isStatistical(){
		return _ipcPricingCondition.isStatistical();
	}

	public char getInactive(){
		return _ipcPricingCondition.getInactive();
	}

	public char getConditionClass(){
		return _ipcPricingCondition.getConditionClass();
	}

	public char getCalculationType(){
		return _ipcPricingCondition.getCalculationType();
	}

	public char getConditionCategory(){
		return _ipcPricingCondition.getConditionCategory();
	}

	public char getConditionControl(){
		return _ipcPricingCondition.getConditionControl();
	}

	public char getConditionOrigin(){
		return _ipcPricingCondition.getConditionOrigin();
	}

	public boolean isChangeOfRatesAllowed(){
		return _ipcPricingCondition.isChangeOfRatesAllowed();
	}

	public boolean isChangeOfPricingUnitsAllowed(){
		return _ipcPricingCondition.isChangeOfPricingUnitsAllowed();
	}

	public boolean isChangeOfValuesAllowed(){
		return _ipcPricingCondition.isChangeOfValuesAllowed();
	}

	public boolean isDeletionAllowed(){
		return _ipcPricingCondition.isDeletionAllowed();
	}

	//property to be used with PricingConditionBooleanTag
/*	public boolean isCalculationTypePercentage(){
		return (com.sap.spe.pricing.customizing.application.PricingCustomizingConstants.CalculationRule.isPercentage(_ipcPricingCondition.getCalculationType()));
	}
*/
	public String getConversionUnit1() throws IPCException{
		char calcType = getCalculationType();
		if (PricingCustomizingConstants.CalculationRule.isRelative(calcType)){
			if (calcType == PricingCustomizingConstants.CalculationRule.QUANTITY_DEP)
				return _pricingConditionPanel.getQuantityUnit();
			if (calcType == PricingCustomizingConstants.CalculationRule.GROSS_WEIGHT_DEP ||
				calcType == PricingCustomizingConstants.CalculationRule.GROSS_WEIGHT_DEP)
				return _pricingConditionPanel.getWeightUnit();
			if (calcType == PricingCustomizingConstants.CalculationRule.VOLUME_DEP)
				return _pricingConditionPanel.getVolumeUnit();
		}
		return "";
	}

	public String getConversionUnit2() throws IPCException{
		return getPricingUnitUnit();
	}

	public ValueSelection getAvailablePhysicalUnits() throws IPCException{
		if (_availablePhysicalUnits == null)
			_availablePhysicalUnits = _pricingConditionPanel.getAvailablePhysicalUnits(getPricingUnitDimension());
		return _availablePhysicalUnits;
	}

	public ValueSelection getAvailablePricingUnitUnits() throws IPCException{
		if (PricingCustomizingConstants.CalculationRule.isPhysicalUnitDependent(this.getCalculationType()))
			return this.getAvailablePhysicalUnits();
		else
			return _pricingConditionPanel.getAvailableQuantityUnits();
	}

	public ValueSelection getAvailableConditionTypeNames() throws IPCException{
		return _pricingConditionPanel.getAvailableConditionTypeNames();
	}

	public ValueSelection getAvailableQuantityUnits() throws IPCException{
		return _pricingConditionPanel.getAvailableQuantityUnits();
	}

	public ValueSelection getAllCurrencyUnits() throws IPCException{
		return _pricingConditionPanel.getAllCurrencyUnits();
	}

	public boolean isHeaderCondition(){
		return _pricingConditionPanel.isHeader();
	}

	protected String _getUnitValue(String value, String unit){
		StringBuffer unitValue = new StringBuffer(value);
		unitValue.append(" ");
		return unitValue.append(unit).toString();
	}

	public static final class PropertyNames{
		public static final String    CONDITION_TYPE_NAME     = "conditionTypeName";
		public static final String    CONDITION_RATE_VALUE    = "conditionRateValue";
		public static final String    CONDITION_CURRENCY      = "conditionCurrency";
		public static final String    PRICING_UNIT_VALUE      = "pricingUnitValue";
		public static final String    PRICING_UNIT_UNIT       = "pricingUnitUnit";
		public static final String    CONDITION_VALUE_VALUE   = "conditionValueValue";
	}
	public char getScaleType(){
		return _ipcPricingCondition.getScaleType();
	}
	
	public static Key createKey(String stepNo, String counter) {
		return new Key(stepNo, counter);
	}
	
	public static void main(String[] args) {
		PricingCondition helperInstance = new PricingCondition(null, null, null);
		Key key1, key2, key3, key4;
		key1 = new Key("11", "0");
		key2 = new Key("100", "0");
		key3 = new Key("10", "0");
		key4 = new Key("11", "1");
	    int result;
	    result = key1.compareTo(key2);
	    result = key1.compareTo(key3);
	    result = key1.compareTo(key4);
	    result = key1.compareTo(key1);
	}

}

class Key implements Comparable {
	Integer stepNo;
	Integer counter;
	Key(String stepNo, String counter) {
		this.stepNo = new Integer(stepNo);
		this.counter = new Integer(counter);
	}
	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(Object o) {
		int result;
		if (o instanceof Key) {
			Key tmpKey = (Key)o;
			result = this.stepNo.compareTo(tmpKey.stepNo);
			if (result == 0) { //same stepNo
				return this.counter.compareTo(tmpKey.counter); 
			}else {
				return result;
			}
		}else {
			return -1;
		}
	}
}
