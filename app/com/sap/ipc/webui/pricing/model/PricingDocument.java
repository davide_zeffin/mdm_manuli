/************************************************************************

	Copyright (c) 2001 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.ipc.webui.pricing.model;

import com.sap.spc.remote.client.object.IPCDocument;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.DimensionalValue;
import com.sap.spc.remote.client.object.IPCPricingConditionSet;

public class PricingDocument implements PricingContainer{

	private IPCDocument _document;

    public PricingDocument(IPCDocument document) {
		_document = document;
    }

	public  void pricing(char mode) throws IPCException{
	}

	public  void pricingComplete() throws IPCException{
	}

	public  boolean getPerformPricingAnalysis() throws IPCException{
		return false;
	}

	public  boolean isGroupConditionProcessingEnabled() throws IPCException{
		return _document.isGroupConditionProcessingEnabled();
	}

	public  DimensionalValue getNetWeight() throws IPCException{
		return null;
	}

	public DimensionalValue getBaseQuantity() throws IPCException{
		return null;
	}

	public DimensionalValue getVolume() throws IPCException{
		return null;
	}

	public DimensionalValue getSalesQuantity() throws IPCException{
		return null;
	}

	public DimensionalValue getGrossWeight() throws IPCException{
		return null;
	}

	public String getExchangeRate() throws IPCException{
		return null;
	}

	public String getLocalCurrency() throws IPCException{
		return _document.getLocalCurrency();
	}

	public  IPCPricingConditionSet getPricingConditions() throws IPCException{
		return _document.getPricingConditions();
	}
}