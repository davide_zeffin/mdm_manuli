/************************************************************************

	Copyright (c) 2001 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.ipc.webui.pricing.model;

import com.sap.spc.remote.client.object.ValueDescriptionPairs;
import com.sap.spc.remote.client.object.IPCException;

import java.util.Arrays;
import java.util.TreeSet;
import java.util.Collection;
import java.util.Iterator;


public class ValueSelection {

	private ValueDescriptionPairs _valueDescriptionPairs = null;
	protected Collection _values = null;
	protected Collection _labels = null;
	private ValueSelection _valueSelectionInclEmptyValue = null;

	protected ValueSelection(){
	}

	ValueSelection(ValueDescriptionPairs valueDescriptionPairs){
		_valueDescriptionPairs = valueDescriptionPairs;
		_valueSelectionInclEmptyValue = new ValueSelectionInclEmptyValue(this);
	}

	public ValueSelection getInclEmptyValue(){
		return _valueSelectionInclEmptyValue;
	}

	public ValueSelection getInclValue(String value) throws IPCException{
		Collection values = getValues();
		if (values.contains(value))
			return this;
		return new ValueSelectionInclValue(this, value);
	}

	public Collection getValues() throws IPCException{
				
					if (_values == null)
						{
						
						if(_valueDescriptionPairs.getValues()!=null)
						{
						_values =  new TreeSet(Arrays.asList(_valueDescriptionPairs.getValues()));
						}
						else
						{
						_values= new TreeSet();
						}
						}
					return _values;
		
	
	}

	public Collection getLabels() throws IPCException{
		if (_labels == null){
			String[] values = _valueDescriptionPairs.getValues();
			String[] descriptions =  _valueDescriptionPairs.getDescriptions();
			_labels = new TreeSet();
			if(values!=null){
			
			for (int i = 0; i < values.length; i++){
			    StringBuffer label = new StringBuffer(values[i]);
				label.append(" ");
				label.append(descriptions[i]);
				_labels.add(label.toString());
			}
			}
		}
	    return _labels;
	}

}