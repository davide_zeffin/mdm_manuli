/************************************************************************

	Copyright (c) 2001 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.ipc.webui.pricing.action;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Locale;
import java.util.Vector;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionServlet;
import org.apache.struts.util.MessageResources;

import com.sap.ipc.webui.pricing.utils.PropertyUtils;
import com.sap.ipc.webui.pricing.UIConstants;
import com.sap.ipc.webui.pricing.model.*;
import com.sap.ipc.webui.pricing.actionform.PricingConditionPanelForm;
import com.sap.ipc.webui.pricing.constants.ActionForwardConstants;
import com.sap.ipc.webui.pricing.constants.InternalRequestParameterConstants;
import com.sap.spc.remote.client.object.IPCException;

public final class ShowPricingConditionPanel extends Action {


	// --------------------------------------------------------- Public Methods

	public ActionForward perform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws IOException, ServletException {

		// Extract attributes we will need
		HttpSession session = request.getSession();

		UIContext uIContext = (UIContext)session.getAttribute(UIConstants.Session.Attribute.UI_CONTEXT);
		if (uIContext == null) {
			servlet.log(" Missing uIContext ");
			request.setAttribute(InternalRequestParameterConstants.IPC_EXCEPTION, new IPCException("error.jsp.application"));
			return mapping.findForward(ActionForwardConstants.INTERNAL_ERROR);
		}
		PricingConditionPanel panel = uIContext.getPricingConditionPanel();
		if (panel == null) {
			servlet.log(" Missing PricingConditionPanel ");
			request.setAttribute(InternalRequestParameterConstants.IPC_EXCEPTION, new IPCException("error.jsp.application"));
			return mapping.findForward(ActionForwardConstants.INTERNAL_ERROR);
		}
		
		// Populate the form
		if (form == null) {
			if (servlet.getDebug() >= 1)
				servlet.log(" Creating new PricingConditionPanelForm bean under key "
							+ mapping.getAttribute());
			form = new PricingConditionPanelForm();
				if ("request".equals(mapping.getScope()))
					request.setAttribute(mapping.getAttribute(), form);
				else
					session.setAttribute(mapping.getAttribute(), form);
		}

		PricingConditionPanelForm pricingConditionPanelForm = (PricingConditionPanelForm)form;
		String mode = pricingConditionPanelForm.getMode();

		try {
			//reads invalid conditions from server
			PropertyUtils.copyProperties(form, panel);
			// append 2 new lines in Edit mode with each roundtrip
			if (mode.equals("Edit")) {
				panel.appendEmptyRows(2);
			}
			// copy the new lines to the form
			PropertyUtils.copyProperties(form, panel);
		} catch (InvocationTargetException e) {
			Throwable t = e.getTargetException();
			if (t == null)
				t = e;
			else if (t instanceof IPCException){
				ActionErrors errors = new ActionErrors();
				errors.add(ActionErrors.GLOBAL_ERROR,
						new ActionError("prc.error.template", e.getMessage()));
				saveErrors(request, errors);
				return (new ActionForward(mapping.getInput()));
			}
			servlet.log("PricingConditionPanelForm.populate", t);
			throw new ServletException("PricingConditionPanelForm.populate", t);
		} catch (Throwable t) {
			servlet.log("PricingConditionPanelForm.populate", t);
			throw new ServletException("PricingConditionPanelForm.populate", t);
		}

		// Forward control to the pricing condition panel page
		return (mapping.findForward("success"));
	}

}
