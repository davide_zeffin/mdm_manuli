/************************************************************************

	Copyright (c) 2001 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.ipc.webui.pricing.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import com.sap.ipc.webui.pricing.UIConstants;
import com.sap.ipc.webui.pricing.model.PricingCondition;
import com.sap.ipc.webui.pricing.model.PricingConditionPanel;
import com.sap.ipc.webui.pricing.model.UIContext;

public final class DeleteCondition extends Action {

	public ActionForward perform(   ActionMapping       mapping,
									ActionForm          form,
									HttpServletRequest  request,
									HttpServletResponse response)
	throws IOException, ServletException {

		// Extract attributes we will need
		MessageResources messages = getResources();
		HttpSession session = request.getSession();

		String stepNo = request.getParameter(UIConstants.Request.Parameter.STEP_NO);
		String counter = request.getParameter(UIConstants.Request.Parameter.COUNTER);

		UIContext uIContext = (UIContext)session.getAttribute(UIConstants.Session.Attribute.UI_CONTEXT);
		if (uIContext == null) {
			servlet.log(" Missing uIContext ");
			response.sendError(HttpServletResponse.SC_BAD_REQUEST,
								messages.getMessage("error.jsp.application"));
			return (null);
		}
		PricingConditionPanel panel = uIContext.getPricingConditionPanel();
		if (panel == null) {
			servlet.log(" Missing PricingConditionPanel ");
			response.sendError(HttpServletResponse.SC_BAD_REQUEST,
							   messages.getMessage("error.jsp.application"));
			return (null);
		}

		PricingCondition pricingCondition = null;
		// Identify the relevant pricing condition
		if ((stepNo == null || stepNo.length() == 0) && (counter == null || counter.length() == 0)){
			// Is there a related PricingCondition object?
			pricingCondition = uIContext.getPricingCondition();
			if (pricingCondition == null) {
				servlet.log(" Missing pricingCondition ");
				response.sendError(HttpServletResponse.SC_BAD_REQUEST, messages.getMessage("error.jsp.application"));
				return (null);
			}
		}
		else {
			pricingCondition = panel.findPricingCondition(stepNo, counter);
			if (pricingCondition == null) {
				if (servlet.getDebug() >= 1)
					servlet.log(" No pricing condition for step " + stepNo + "/" + counter);
				return (mapping.findForward("failure"));
			}
		}
		panel.removePricingCondition(pricingCondition);
		//Excecute Modifications
		panel.commit();

		return (mapping.findForward("success"));

	}

}