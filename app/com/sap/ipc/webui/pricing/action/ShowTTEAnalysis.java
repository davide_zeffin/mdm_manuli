/************************************************************************

	Copyright (c) 2002 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.ipc.webui.pricing.action;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionServlet;
import org.apache.struts.util.MessageResources;

import com.sap.ipc.webui.pricing.UIConstants;
import com.sap.ipc.webui.pricing.model.*;
import com.sap.ipc.webui.pricing.utils.ServletForwarder;
import com.sap.spc.remote.client.object.IPCDocument;
import com.sap.spc.remote.client.object.IPCSession;

public class ShowTTEAnalysis extends Action {
	
	public static final String XML_TTE_ANAL_INPUT_DOCUMENT="inputxmldocument";
	public static final String XML_TTE_ANAL_OUTPUT_DOCUMENT="outputxmldocument";
	

	// --------------------------------------------------------- Public Methods

	/**
	 * Redirect to TTE Analysis Servlet
	 */
	public ActionForward perform(   ActionMapping       mapping,
									ActionForm          form,
									HttpServletRequest  request,
									HttpServletResponse response)
	throws IOException, ServletException {

		// Extract attributes we will need
		MessageResources messages = getResources();
		HttpSession session = request.getSession();
		String[] xmlDocuments=null;
		//Default language is EN
		String language="EN"; 

		UIContext uIContext = (UIContext)session.getAttribute(UIConstants.Session.Attribute.UI_CONTEXT);
		if (uIContext == null) {
			servlet.log(" Missing uIContext ");
			response.sendError(HttpServletResponse.SC_BAD_REQUEST,
								messages.getMessage("error.jsp.application"));
			return (null);
		}
		//show pricing analysis
		if (uIContext!=null) {
			IPCSession ipcSession=uIContext.getIPCSession();
			IPCDocument ipcDocument = ipcSession.getDocument(uIContext.getDocumentId());
			xmlDocuments=ipcDocument.getTTEAnalysisData();
			//language=uIContext.
			
		}

		ServletForwarder servletForwarder = new ServletForwarder(request, response, uIContext);
		//The parameters to be passed
		if (xmlDocuments !=null) {
			servletForwarder.setRequestVariable(XML_TTE_ANAL_INPUT_DOCUMENT,xmlDocuments[0]);
			servletForwarder.setRequestVariable(XML_TTE_ANAL_OUTPUT_DOCUMENT,xmlDocuments[1]);
		}
		
		//servletForwarder.setContextPath("/tteanalysis");
		//Since the tte application is within the pricing application and Request Dispatcher is used
		servletForwarder.setContextPath("");
		servletForwarder.setAdditionalParam("forwarding", "TRUE");
		servletForwarder.forward("/servlet/com.sap.tte.webui.analysis.AnalysisControlerServlet");

		// will be ignored
		// Forward control to the pricing condition panel page
		if (servlet.getDebug() >= 1)
			servlet.log(" Forwarding to 'success' page");
		return (mapping.findForward("success"));
	}

}