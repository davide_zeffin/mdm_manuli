/************************************************************************

	Copyright (c) 2001 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.ipc.webui.pricing.action;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import com.sap.ipc.webui.pricing.UIConstants;
import com.sap.ipc.webui.pricing.actionform.PricingConditionForm;
import com.sap.ipc.webui.pricing.model.PricingCondition;
import com.sap.ipc.webui.pricing.model.PricingConditionPanel;
import com.sap.ipc.webui.pricing.model.UIContext;
import com.sap.ipc.webui.pricing.model.ValueSelection;
import com.sap.ipc.webui.pricing.utils.PropertyUtils;
import com.sap.spc.remote.client.object.IPCException;

public final class EditPricingCondition extends Action {

	public ActionForward perform(   ActionMapping       mapping,
									ActionForm          form,
									HttpServletRequest  request,
									HttpServletResponse response)
	throws IOException, ServletException {

		// Extract attributes we will need
		Locale locale = getLocale(request);
		MessageResources messages = getResources();
		HttpSession session = request.getSession();
		String action = request.getParameter(UIConstants.Request.Parameter.ACTION);
		if (action == null){
			action = UIConstants.Action.EDIT;
			if (servlet.getDebug() >= 1)
			servlet.log("EditPricingConditionPanel:  Processing " + action +
						" action");
		}

		String stepNo = request.getParameter(UIConstants.Request.Parameter.STEP_NO);
		String counter = request.getParameter(UIConstants.Request.Parameter.COUNTER);
		 if (servlet.getDebug() >= 1)
			 servlet.log("EditPricingCondition:  Processing " + action + " action");

		UIContext uIContext = (UIContext)session.getAttribute(UIConstants.Session.Attribute.UI_CONTEXT);
		if (uIContext == null) {
			servlet.log(" Missing uIContext ");
			response.sendError(HttpServletResponse.SC_BAD_REQUEST,
								messages.getMessage("error.jsp.application"));
			return (null);
		}
		PricingConditionPanel panel = uIContext.getPricingConditionPanel();
		if (panel == null) {
			servlet.log(" Missing PricingConditionPanel ");
			response.sendError(HttpServletResponse.SC_BAD_REQUEST,
							   messages.getMessage("error.jsp.application"));
			return (null);
		}

		PricingCondition pricingCondition = null;
		// Identify the relevant pricing condition
		if ((stepNo == null || stepNo.length() == 0) && (counter == null || counter.length() == 0)){
			// Is there a related PricingCondition object?
			pricingCondition = uIContext.getPricingCondition();
			if (pricingCondition == null) {
				servlet.log(" Missing pricingCondition ");
				response.sendError(HttpServletResponse.SC_BAD_REQUEST, messages.getMessage("error.jsp.application"));
				return (null);
			}
		}
		else {
			pricingCondition = panel.findPricingCondition(stepNo, counter);
			if (pricingCondition == null) {
				if (servlet.getDebug() >= 1)
					servlet.log(" No pricing condition for step " + stepNo + "/" + counter);
				return (mapping.findForward("failure"));
			}
			uIContext.setPricingCondition(pricingCondition);
		}

		// Populate the form
		if (form == null) {
			if (servlet.getDebug() >= 1)
				servlet.log(" Creating new PricingConditionForm bean under key "
							+ mapping.getAttribute());
			form = new PricingConditionForm();
				if ("request".equals(mapping.getScope()))
					request.setAttribute(mapping.getAttribute(), form);
				else
					session.setAttribute(mapping.getAttribute(), form);
		}
		PricingConditionForm prform = (PricingConditionForm) form;
		prform.setAction(action);
		try{
			if (action.equals(UIConstants.Action.EDIT)){
				prform.setConditionTypeNameSelection(panel.getAvailableConditionTypeNames().getValues());
				prform.setConditionTypeNameSelectionLabels(panel.getAvailableConditionTypeNames().getLabels());
				//there may be a invalid pricing unit
				ValueSelection pricingUnitSelection = pricingCondition.getAvailablePricingUnitUnits().getInclValue(pricingCondition.getPricingUnitUnit());
				prform.setPricingUnitUnitNameSelection(pricingUnitSelection.getValues());
				prform.setPricingUnitUnitNameSelectionLabels(pricingUnitSelection.getLabels());
				prform.setCurrencyNameSelection(panel.getAllCurrencyUnits().getValues());
				prform.setCurrencyNameSelectionLabels(panel.getAllCurrencyUnits().getLabels());
			}
			if (servlet.getDebug() >= 1)
				servlet.log(" Populating form from " + pricingCondition);
			try {
			    if (pricingCondition != null){//!action.equals(UIConstants.Action.CREATE
					PropertyUtils.copyProperties(prform, pricingCondition);
					prform.setDocExchangeRate(panel.getExchangeRate());//oder entspr. Properties in PricingCondition
			    }
				prform.setAction(action);
			} catch (InvocationTargetException e) {
				Throwable t = e.getTargetException();
				if (t == null)
					t = e;
				else if (t instanceof IPCException){
					throw (IPCException)t;
				}
				servlet.log("PricingConditionForm.populate", t);
				throw new ServletException("PricingConditionForm.populate", t);
			} catch (Throwable t) {
				servlet.log("PricingConditionForm.populate", t);
				throw new ServletException("PricingConditionForm.populate", t);
		   }
		} catch(IPCException e){
			ActionErrors errors = new ActionErrors();
			errors.add(ActionErrors.GLOBAL_ERROR,
						new ActionError("prc.error.template", e.getMessage()));
			saveErrors(request, errors);
			return (new ActionForward(mapping.getInput()));
		}

		// Forward control to the edit pricing condition page
		if (servlet.getDebug() >= 1)
			servlet.log(" Forwarding to 'success' page");
		return (mapping.findForward("success"));

	}

}