/************************************************************************

	Copyright (c) 2001 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.ipc.webui.pricing.action;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionServlet;
import org.apache.struts.util.MessageResources;

import com.sap.ipc.webui.pricing.UIConstants;
import com.sap.ipc.webui.pricing.model.*;
import com.sap.ipc.webui.pricing.utils.ServletForwarder;
import com.sap.spc.remote.client.object.IPCDocument;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.IPCSession;

public class ShowPricingAnalysis extends Action {
	
	//---------------------------------------------------------- Constants
	
	public static final String XML_PRIC_ANAL_INPUT_DOC="xmldocument";

	// --------------------------------------------------------- Public Methods

	/**
	 * Redirect to Pricing Analysis Servlet
	 */
	public ActionForward perform(   ActionMapping       mapping,
									ActionForm          form,
									HttpServletRequest  request,
									HttpServletResponse response)
	throws IOException, ServletException {

		// Extract attributes we will need
		MessageResources messages = getResources();
		HttpSession session = request.getSession();
		String xmlDocument=null;
		
		UIContext uIContext = (UIContext)session.getAttribute(UIConstants.Session.Attribute.UI_CONTEXT);
		if (uIContext == null) {
			servlet.log(" Missing uIContext ");
			response.sendError(HttpServletResponse.SC_BAD_REQUEST,
								messages.getMessage("error.jsp.application"));
			return (null);
		} else {
			IPCSession ipcSession=uIContext.getIPCSession();
			IPCDocument ipcDocument = ipcSession.getDocument(uIContext.getDocumentId());
			IPCItem ipcItem=ipcDocument.getItem(uIContext.getItemId());
			xmlDocument=ipcItem.getPricingAnalysisData();
		}
		//show pricing analysis
		ServletForwarder servletForwarder = new ServletForwarder(request, response, uIContext);
		
		//Set the xml document as a request variable
		if (xmlDocument!=null) {
			servletForwarder.setRequestVariable(XML_PRIC_ANAL_INPUT_DOC,xmlDocument);
		}
		
		//Since the request dispatcher is used and the servlet is inside the same application context 
		//path need not be set
		//servletForwarder.setContextPath(request.getContextPath());
		servletForwarder.setContextPath("");
		servletForwarder.setAdditionalParam("forwarding", "TRUE");
		servletForwarder.forward("/servlet/com.sap.ipc.webui.pricing.servlet.ShowAdvancedXMLPricingAnalysis");

		// will be ignored
		// Forward control to the pricing condition panel page
		if (servlet.getDebug() >= 1)
			servlet.log(" Forwarding to 'success' page");
		return (mapping.findForward("success"));
	}

}