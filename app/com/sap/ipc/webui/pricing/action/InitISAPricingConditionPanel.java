/*
 * Created on 29.09.2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.ipc.webui.pricing.action;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.ipc.webui.pricing.UIConstants;
import com.sap.ipc.webui.pricing.actionform.PricingConditionPanelForm;
import com.sap.ipc.webui.pricing.model.PricingConditionPanel;
import com.sap.ipc.webui.pricing.model.PricingContainer;
import com.sap.ipc.webui.pricing.model.PricingItem;
import com.sap.ipc.webui.pricing.model.UIContext;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.ipc.IPCBOManager;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.WebUtil;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCDocument;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.imp.rfc.RFCIPCItemReference;

/**
 * @author 
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class InitISAPricingConditionPanel extends IPCBaseAction {

	protected static IsaLocation log = IsaLocation.getInstance(InitISAPricingConditionPanel.class.getName());

	public ActionForward ecomPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack)
		throws IOException, ServletException, CommunicationException {

		// Extract attributes we will need
		HttpSession session = request.getSession();
		// parameter server
		// parameter itemreference
		String itemId = request.getParameter(UIConstants.Request.Parameter.ITEM_ID);
		// parameter sapEvent
		// parameter mode
		String mode = request.getParameter(UIConstants.Request.Parameter.MODE);
		if (mode == null){
			mode = UIConstants.Mode.DISPLAY;
			log.warn("missing parameter " + UIConstants.Request.Parameter.MODE);
		}
		((PricingConditionPanelForm)form).setMode(mode);
		
		Object showBackButtonParam = request.getAttribute("showBackButton");

		// create session context
		UIContext uIContext = new UIContext();
		session.setAttribute(UIConstants.Session.Attribute.UI_CONTEXT, uIContext);

		RFCIPCItemReference itemReference =
			(RFCIPCItemReference) request.getAttribute(UIConstants.Request.Parameter.IPC_ITEM_REFERENCE);

		IPCBOManager ipcBoManager =
			(IPCBOManager) userSessionData.getBOM(IPCBOManager.NAME);

		try{
			//the caller of the configuration requests the client to use the same connection
			IPCClient ipcClient = ipcBoManager.createIPCClientUsingConnection(itemReference.getConnectionKey());
			
			// item must exist
			IPCItem ipcItem = ipcClient.getIPCItem(itemReference);
			//create new PricingConditionPanel
			IPCDocument ipcDocument = ipcClient.getIPCDocument(itemReference.getDocumentId());
			if (ipcDocument == null)
				throw new IPCException(new Exception("500, Document "+itemReference.getDocumentId()+" not found"));
			//set mode
			if (ipcDocument.getEditMode() == 'B') //PricingConstants.EditMode.READ_ONLY
				((PricingConditionPanelForm)form).setMode(UIConstants.Mode.DISPLAY);
			else
				((PricingConditionPanelForm)form).setMode(mode);
			// create pricing container
			PricingContainer prContainer = new PricingItem(ipcItem);
			PricingConditionPanel panel = new PricingConditionPanel(prContainer,
																	false, uIContext);
			//put current panel into session context
			uIContext.setPricingConditionPanel(panel);
			//put current SPCsession into context
			uIContext.setIPCSession(ipcClient.getIPCSession());

			if (showBackButtonParam != null) {
				boolean showBackButton = ((Boolean)showBackButtonParam).booleanValue();
				panel.setShowBackButton(showBackButton);
			}
			//put current URI into context		
			//String initialAddr=HttpUtils.getRequestURL(request).toString();
			//This part of code is to reconstruct the url which is used to invoke the Pricing panel UI
			//String initialAddr=request.getRequestURL().toString();
			String initialAddr = WebUtil.getAppsURL(servlet.getServletContext(), request, response, null, "/showPricingConditionPanel.do", null, null, false);
			boolean firstTime=true;
			if(request.getQueryString()!=null) {
				initialAddr+="?"+request.getQueryString();
				firstTime=false;
			}
			Enumeration requestParams=request.getParameterNames();
			while (requestParams.hasMoreElements()) {
				String key=(String)requestParams.nextElement();
				initialAddr+=firstTime?"?":"&";
				initialAddr+=key+"="+(String)request.getParameter(key);
				firstTime=false;
			}
			log.debug("Initial Address is"+initialAddr);
			uIContext.setInitialAddress(initialAddr);
			//this information should be provided by spcSession!
			uIContext.setDocumentId(itemReference.getDocumentId());
			uIContext.setItemId(itemReference.getItemId());
		}catch(com.sap.spc.remote.client.object.IPCException e) {
			request.setAttribute(UIConstants.Request.Parameter.IPC_EXCEPTION, e);
			return (mapping.findForward("ipcFailure"));
		}

		return (mapping.findForward("success"));
	}

}
