package com.sap.ipc.webui.pricing.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.ipc.webui.pricing.constants.ActionForwardConstants;
import com.sap.ipc.webui.pricing.constants.IPCDebugMessages;
import com.sap.ipc.webui.pricing.constants.IPCExceptionConstants;
import com.sap.ipc.webui.pricing.constants.InternalRequestParameterConstants;
import com.sap.ipc.webui.pricing.constants.SessionAttributeConstants;
import com.sap.ipc.webui.pricing.model.UIContext;
import com.sap.isa.businessobject.ipc.IPCBOManager;
import com.sap.isa.core.RequestProcessor;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.isacore.action.EComBaseAction;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.imp.IPCClientObjectFactory;
/**
 * @author 
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
//<--

/**
 * Title:
 * Description:  JSP UI for the IPC.
 * Copyright:    Copyright (c) 2001
 * Company:
 * @author
 * @version 1.0
 */

public abstract class IPCBaseAction extends EComBaseAction {
		
    protected static IsaLocation log = IsaLocation.getInstance(IPCBaseAction.class.getName());
    
    protected IPCClientObjectFactory factory =
        IPCClientObjectFactory.getInstance();
        
	protected IPCClient getIPCClient(HttpServletRequest request) {

		// read businessobjectmanager from Session
		UserSessionData userData = getUserSessionData(request);

		IPCBOManager ipcBoManager = (IPCBOManager)userData.getBOM(IPCBOManager.NAME);
		if (ipcBoManager == null) {
			request.setAttribute(InternalRequestParameterConstants.IPC_EXCEPTION, new IPCException("Couldn't get the Businessobject manager!\n"));
			log.fatal(userData, new IPCException("Couldn't get the Businessobject manager!\n"));
			return null;
		}
		IPCClient ipcClient = ipcBoManager.getIPCClient();

		if (ipcClient == null) {
			request.setAttribute(InternalRequestParameterConstants.IPC_EXCEPTION, new IPCException("Couldn't connect to IPC-Server!\n" +
												                 "Probably the server is down or not started\n" +
																 "or the settings in \n" +
																 "\"<web-app>\\WEB-INF\\classes\\properties\\client.properties\" " +
																 "are wrong"));
			log.fatal(ipcBoManager, new IPCException("Could not determine the IPCClient from the IPCBOManager!"));
			return null;
		}
		return ipcClient;
	}

    private static HttpSession getHttpSession(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session == null) {
            //TODO use msgkey
            log.fatal("Could not get HttpSession in IPCBaseAction!");
        }
        return session;        
    }
    public static UserSessionData getUserSessionData(HttpServletRequest request) {
        HttpSession session = getHttpSession(request);
        UserSessionData userData = UserSessionData.getUserSessionData(session);
        if (userData == null) {
//          TODO use msgkey
			RequestProcessor.setExtendedRequestAttribute(request,
			InternalRequestParameterConstants.IPC_EXCEPTION,
                new IPCException(IPCExceptionConstants.IPCEX19));
            log.fatal(
                session,
                new IPCException(IPCExceptionConstants.IPCEX19));
            return null;
        }
        return userData;
    }
    
    public static UIContext createUIContext(HttpServletRequest request) {
        UserSessionData userSessionData = getUserSessionData(request);
        Object uiContextObject = userSessionData.getAttribute(SessionAttributeConstants.UICONTEXT);
        if (uiContextObject != null) {
            log.error(IPCExceptionConstants.IPCEX20);
        }
        UIContext uiContext = new UIContext();
        userSessionData.setAttribute(SessionAttributeConstants.UICONTEXT, uiContext);
        if (log.isDebugEnabled()) {
            log.debug(IPCDebugMessages.MSG2);
        }
        return uiContext;
    }
    
    protected void removeUIContext(HttpServletRequest request) {
        UserSessionData userSessionData = getUserSessionData(request);
        Object uiContextObject = userSessionData.getAttribute(SessionAttributeConstants.UICONTEXT);
        if (uiContextObject == null) {
            log.error(IPCExceptionConstants.IPCEX18);
        }else {
            userSessionData.removeAttribute(SessionAttributeConstants.UICONTEXT);
            if (log.isDebugEnabled()) {
                log.debug(IPCDebugMessages.MSG1);
            }
        }
    }
    
    public static UIContext getUIContext(
        HttpServletRequest request) {
            
//        HttpSession session = getHttpSession(request);

        UserSessionData userData = getUserSessionData(request);
        
        
        UIContext uiContext = (UIContext)userData.getAttribute(SessionAttributeConstants.UICONTEXT);
        if (uiContext == null) {
			RequestProcessor.setExtendedRequestAttribute(request, InternalRequestParameterConstants.IPC_EXCEPTION,
            //TODO use msgkey
            new IPCException("Could not read the uiContext from the user session!\n"));
            log.fatal("Could not read the uiContext from the user session!\n");
        }
        return uiContext;
    }

	protected ActionForward parameterError(ActionMapping mapping, HttpServletRequest request, String parameterName, String actionName) {
		IPCException ipcException = new IPCException("Required requestParameter " + parameterName +
		 " not found in " + actionName + "!");
		request.setAttribute(InternalRequestParameterConstants.IPC_EXCEPTION, ipcException);
		 return mapping.findForward(ActionForwardConstants.INTERNAL_ERROR);
	}
	    
	public static boolean isSet(String parameter) {
		if (parameter != null && !parameter.equals("") && !parameter.equals("null")) {
			return true;
		}else {
			return false;
		}
	}
	
    
}
