/************************************************************************

	Copyright (c) 2001 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.ipc.webui.pricing.action;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Locale;
import java.util.Hashtable;
import java.util.Arrays;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionServlet;
import org.apache.struts.util.MessageResources;

import com.sap.ipc.webui.pricing.utils.PropertyUtils;
import com.sap.spc.remote.client.object.IPCMessage;
import com.sap.spc.remote.client.object.IPCSession;
import com.sap.spc.remote.client.object.IPCException;

import com.sap.ipc.webui.pricing.UIConstants;
import com.sap.ipc.webui.pricing.model.*;
import com.sap.ipc.webui.pricing.actionform.*;

public final class SavePricingCondition extends Action {


	// --------------------------------------------------------- Public Methods

	public ActionForward perform(   ActionMapping       mapping,
									ActionForm          form,
									HttpServletRequest  request,
									HttpServletResponse response)
	throws IOException, ServletException {

		// Extract attributes and parameters we will need
		Locale locale = getLocale(request);
		MessageResources messages = getResources();
		HttpSession session = request.getSession();
		PricingConditionForm prform = (PricingConditionForm) form;

		UIContext uIContext = (UIContext)session.getAttribute(UIConstants.Session.Attribute.UI_CONTEXT);
		if (uIContext == null) {
			servlet.log(" Missing uIContext ");
			response.sendError(HttpServletResponse.SC_BAD_REQUEST,
								messages.getMessage("error.jsp.application"));
			return (null);
		}

		PricingConditionPanel panel = uIContext.getPricingConditionPanel();
		if (panel == null) {
			servlet.log(" Missing PricingConditionPanel ");
			response.sendError(HttpServletResponse.SC_BAD_REQUEST,
							   messages.getMessage("error.jsp.application"));
			return (null);
		}
		String action = request.getParameter(UIConstants.Request.Parameter.ACTION);
		if (action == null)
			action = "?";
			if (servlet.getDebug() >= 1)
				servlet.log("SavePricingCondition:  Processing " + action + " action");

		// Is there a related PricingCondition object?
		PricingCondition pricingCondition = uIContext.getPricingCondition();
		if (pricingCondition == null) {
			servlet.log(" Missing pricingCondition ");
			response.sendError(HttpServletResponse.SC_BAD_REQUEST,
								messages.getMessage("prc.error.noPrcCondition"));
			return (null);
		}

		// Was this transaction cancelled?
//	    if (isCancelled(request)) {
		if (prform.getCancelled().equalsIgnoreCase("true")) {
			if (servlet.getDebug() >= 1)
				servlet.log(" Transaction '" + action + "' was cancelled");
			// Remove the obsolete form bean and the current pricing condition
			if (mapping.getAttribute() != null) {
				if ("request".equals(mapping.getScope()))
					request.removeAttribute(mapping.getAttribute());
				else
					session.removeAttribute(mapping.getAttribute());
			}
			uIContext.setPricingCondition(null);
			return (mapping.findForward("success"));
		}

		String forwardTo = null;
		IPCMessage[] ipcMessages = null;
		try{
			//HACK: should be provided by <html:hidden property="currencyNameSelection"/>
			//could also be performed after "saveErrors"
			prform.setConditionTypeNameSelection(panel.getAvailableConditionTypeNames().getValues());
			prform.setPricingUnitUnitNameSelection(pricingCondition.getAvailablePricingUnitUnits().getValues());
			prform.setCurrencyNameSelection(panel.getAllCurrencyUnits().getValues());
			// Was this transaction a Delete?
			if (action.equals(UIConstants.Action.DELETE)) {
				if (servlet.getDebug() >= 1)
					servlet.log(" Deleting pricing condition '" + pricingCondition.getStepNo()
									+ "/" + pricingCondition.getCounter() + "'");
				panel.removePricingCondition(pricingCondition);
				forwardTo = "success";
			}

			// Some validations were done by the form itself

			// Was this transaction a Edit?
			if (action.equals(UIConstants.Action.EDIT)) {
				// Update the persistent pricingCondition information
				if (servlet.getDebug() >= 1)
					servlet.log(" Populating persistent data (IPC) from form bean");
				try {
					PropertyUtils.copyProperties(pricingCondition, prform);
				} catch (InvocationTargetException e) {
					Throwable t = e.getTargetException();
					if (t == null)
						t = e;
					servlet.log("PricingCondition.populate", t);
					throw new ServletException("PricingCondition.populate", t);
				} catch (Throwable t) {
					servlet.log("PricingCondition.populate", t);
					throw new ServletException("PricingCondition.populate", t);
				}
				forwardTo = "success";
			}

			//Excecute Modifications
			panel.commit();

		} catch(IPCException e){
			//Get Messages from IPC
			ipcMessages = panel.getMessages();
			//Build Messages for output
			ActionErrors errors = new ActionErrors();
			for(int i=0; i<ipcMessages.length; i++){
				errors.add(ActionErrors.GLOBAL_ERROR,
	    					new ActionError("prc.error.template", ipcMessages[i].getMessage()));
			}
			if(ipcMessages.length == 0)
				errors.add(ActionErrors.GLOBAL_ERROR,
	    					new ActionError("prc.error.template", e.getMessage()));
			saveErrors(request, errors);
			return (new ActionForward(mapping.getInput()));
		}

		//set SAPEvent
		panel.getSAPEventStack().addEvent(UIConstants.SAPEventAction.PRICING_DATA_CHANGED);

		// Remove the obsolete form bean and the current pricing condition
		if (mapping.getAttribute() != null) {
			if ("request".equals(mapping.getScope()))
				request.removeAttribute(mapping.getAttribute());
			else
				session.removeAttribute(mapping.getAttribute());
		}
		uIContext.setPricingCondition(null);

		// Forward control to the specified success URI
		if (servlet.getDebug() >= 1)
			servlet.log(" Forwarding to "+forwardTo+" page");
		return (mapping.findForward(forwardTo));

	}

}
