/************************************************************************

	Copyright (c) 2001 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.ipc.webui.pricing.action;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpUtils;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import com.sap.ipc.webui.pricing.UIConstants;
import com.sap.ipc.webui.pricing.actionform.PricingConditionPanelForm;
import com.sap.ipc.webui.pricing.model.PricingConditionPanel;
import com.sap.ipc.webui.pricing.model.PricingContainer;
import com.sap.ipc.webui.pricing.model.PricingDocument;
import com.sap.ipc.webui.pricing.model.PricingItem;
import com.sap.ipc.webui.pricing.model.UIContext;
import com.sap.ipc.webui.pricing.utils.CodePageUtils;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.WebUtil;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCDocument;
import com.sap.spc.remote.client.object.IPCDocumentProperties;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.IPCItemProperties;
import com.sap.spc.remote.client.object.IPCItemReference;
import com.sap.spc.remote.client.object.IPCSession;
import com.sap.spc.remote.client.object.OriginalRequestParameterConstants;
import com.sap.spc.remote.client.object.imp.DefaultIPCClient;
import com.sap.spc.remote.client.object.imp.IPCClientObjectFactory;
import com.sap.spc.remote.client.object.imp.tcp.TCPIPCItemReference;


public final class InitializePricingConditionPanel extends Action {

	/*Initialize Pricing Panel
	  Parameters provided by request:
	  - server          IPC-Server
	  - port            Port of the IPC-Server
	  - sessionId       Id of the IPCSession to be connect to
	  - documentId      Id of the document to be accessed
	  - itemId          Id of the item to be accessed
	  - SAPEvent        "true", if changes should fire a "SAP Event" - default: "false"
	  - mode            switches between edit ("Edit") and display mode ("Display")
	*/

	// --------------------------------------------------------- Public Methods

	protected static IsaLocation log = IsaLocation.getInstance(InitializePricingConditionPanel.class.getName());

	public ActionForward perform(   ActionMapping       mapping,
									ActionForm          form,
									HttpServletRequest  request,
									HttpServletResponse response)
	throws IOException, ServletException {

		// Extract attributes we will need
		HttpSession session = request.getSession();
		// parameter server
		String server = request.getParameter(UIConstants.Request.Parameter.SERVER);
		// parameter port
		String port = request.getParameter(UIConstants.Request.Parameter.PORT);
		// parameter sessionId
		String sessionId = request.getParameter(UIConstants.Request.Parameter.SESSION_ID);
		// parameter documentId
		String documentId = request.getParameter(UIConstants.Request.Parameter.DOCUMENT_ID);
		// parameter itemId
		String itemId = request.getParameter(UIConstants.Request.Parameter.ITEM_ID);
		// parameter sapEvent
		// parameter mode
		String mode = request.getParameter(UIConstants.Request.Parameter.MODE);
		if (mode == null){
			mode = UIConstants.Mode.DISPLAY;
			log.warn("missing parameter " + UIConstants.Request.Parameter.MODE);
		}
		String decimalSeparator = request.getParameter(UIConstants.Request.Parameter.DECIMAL_SEPARATOR);
		String groupingSeparator = request.getParameter(UIConstants.Request.Parameter.GROUPING_SEPARATOR);

		// create session context
		UIContext uIContext = new UIContext();
		session.setAttribute(UIConstants.Session.Attribute.UI_CONTEXT, uIContext);

		((PricingConditionPanelForm)form).setMode(mode);
		try{
		//attach to IPCSession
			IPCSession ipcSession = _attachToIPCSession(server, port, sessionId, documentId, itemId);
			//create new PricingConditionPanel
			IPCDocument ipcDocument = ipcSession.getDocument(documentId);
			if (ipcDocument == null)
				throw new IPCException(new Exception("500, Document "+documentId+" not found"));
			//initialize the doucumentproperties with parameter from the request
			setDocumentProperties(request, ipcDocument.getDocumentProperties());
			//set mode
			if (ipcDocument.getEditMode() == 'B') //PricingConstants.EditMode.READ_ONLY
				((PricingConditionPanelForm)form).setMode(UIConstants.Mode.DISPLAY);
			else
			    ((PricingConditionPanelForm)form).setMode(mode);
			// create pricing container
			PricingContainer prContainer = null;
			if (itemId != null){
				IPCItem ipcItem = ipcDocument.getItem(itemId);
				if (ipcItem == null)
					throw new IPCException(new Exception("500, Item "+itemId+" not found"));
				//initialize the itemproperties with parameter from the request
				setItemProperties(request, ipcItem.getItemProperties());
				prContainer = new PricingItem(ipcItem);
			}
			else
				prContainer = new PricingDocument(ipcDocument);
			PricingConditionPanel panel = new PricingConditionPanel(prContainer,
																    false, uIContext);
			//put current panel into session context
			uIContext.setPricingConditionPanel(panel);
			//put current SPCsession into context
			uIContext.setIPCSession(ipcSession);
			//put current URI into context
		    uIContext.setDecimalSeparator(decimalSeparator);
			uIContext.setGroupingSeparator(groupingSeparator);
			
			//String initialAddr=HttpUtils.getRequestURL(request).toString();
			//This part of code is to reconstruct the url which is used to invoke the Pricing panel UI
			//String initialAddr=request.getRequestURL().toString();
			String initialAddr = WebUtil.getAppsURL(servlet.getServletContext(), request, response, null, "/showPricingConditionPanel.do", null, null, false);
			boolean firstTime=true;
			if(request.getQueryString()!=null) {
				initialAddr+="?"+request.getQueryString();
				firstTime=false;
			}
			Enumeration requestParams=request.getParameterNames();
			while (requestParams.hasMoreElements()) {
				String key=(String)requestParams.nextElement();
				initialAddr+=firstTime?"?":"&";
				initialAddr+=key+"="+(String)request.getParameter(key);
				firstTime=false;
			}
			log.debug("Initial Address is"+initialAddr);
			uIContext.setInitialAddress(initialAddr);
			//this information should be provided by spcSession!
			uIContext.setDocumentId(documentId);
			uIContext.setItemId(itemId);
		}catch(com.sap.spc.remote.client.object.IPCException e) {
			request.setAttribute(UIConstants.Request.Parameter.IPC_EXCEPTION, e);
			return (mapping.findForward("ipcFailure"));
		}

		return (mapping.findForward("success"));
	}

	private IPCSession _attachToIPCSession( /*HttpSession httpSession, */
										    String server, String port, String sessionId,
											String documentId, String itemId) throws IPCException{

		String encoding = "UnicodeLittle";

		IPCClient client = IPCClientObjectFactory.getInstance().newIPCClient(sessionId);

		if (servlet.getDebug() >= 1)
			servlet.log("InitializePricingConditionPanel:  client successfully created ");

		TCPIPCItemReference ipcItemReference = new TCPIPCItemReference();
		ipcItemReference.setHost(server);
		ipcItemReference.setPort(port);
//		ipcItemReference.setSessionId(sessionId);
		ipcItemReference.setDocumentId(documentId);
		ipcItemReference.setItemId(itemId);
		ipcItemReference.setEncoding(encoding);
		ipcItemReference.setDispatcher(false);

		return IPCClientObjectFactory.getInstance().newIPCSession((DefaultIPCClient)client, ipcItemReference, true);//activate extended data (-> IS_SWINGGUI)

	}
	/*
	 * fill document properties from the request into an IPCDocumentProperties object
	 * @return a IPCDocumentProperties
	 */
	protected void setDocumentProperties(HttpServletRequest request, IPCDocumentProperties p) {
		String performPricingAnanlysis = request.getParameter(OriginalRequestParameterConstants.PERFORM_PRICING_ANALYSIS);
		if (IPCBaseAction.isSet(performPricingAnanlysis)){
			p.setPerformPricingAnalysis(performPricingAnanlysis.equals("Y") ? true : false);
		}
	}
	

	/*
	 * fill item properties from the request into an IPCItemProperties object
	 * @return a IPCItemProperties
	 */
	protected void setItemProperties(HttpServletRequest request, IPCItemProperties p) {
		String performPricingAnanlysis = request.getParameter(OriginalRequestParameterConstants.PERFORM_PRICING_ANALYSIS);
		if (IPCBaseAction.isSet(performPricingAnanlysis)){
			p.setPerformPricingAnalysis(performPricingAnanlysis.equals("Y") ? true : false);
		}
	}
	



}