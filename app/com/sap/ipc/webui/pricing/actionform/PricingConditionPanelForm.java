/************************************************************************

	Copyright (c) 2001 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.ipc.webui.pricing.actionform;

import com.sap.ipc.webui.pricing.model.PricingCondition;
import com.sap.ipc.webui.pricing.model.NewPricingCondition;
import com.sap.ipc.webui.pricing.model.ValueSelection;
import com.sap.ipc.webui.pricing.utils.PropertyUtils;
import com.sap.spc.remote.client.object.IPCException;

import org.apache.struts.action.ActionForm;

import java.util.Iterator;
import java.util.ArrayList;
import java.lang.reflect.InvocationTargetException;


public class PricingConditionPanelForm extends ActionForm  {


   // --------------------------------------------------- Instance Variables

	/**
	 * The maintenance mode we are performing (Display or Edit).
	 */
	private String _mode = com.sap.ipc.webui.pricing.UIConstants.Mode.EDIT;

	private ArrayList _pricingConditions = new ArrayList(0);
	private ArrayList _pricingConditionForms = new ArrayList(0);

	// ----------------------------------------------------------- Properties
	/**
	 * Return the maintenance mode.
	 */
	public String getMode() {
		return _mode;
	}


	/**
	 * Set the maintenance action.
	 *
	 * @param action The new maintenance action.
	 */
	public void setMode(String mode) {
		_mode = mode;
	}

	public void setPricingConditions(ArrayList pricingConditions)
		throws  IPCException,
			    IllegalAccessException,
			    InvocationTargetException,
				NoSuchMethodException{
		_pricingConditions = pricingConditions;
		_pricingConditionForms = new ArrayList(0);
		Iterator iterator = pricingConditions.iterator();
		while(iterator.hasNext()){
			PricingCondition prCondition = (PricingCondition)iterator.next();
			PricingConditionForm prConditionForm = new PricingConditionForm();
			//oder new PricingConditionForm(iterator.next())
			if (prCondition instanceof NewPricingCondition){
				prConditionForm.setAction(com.sap.ipc.webui.pricing.UIConstants.Action.CREATE);
			}
			
			
			if (getMode().equals(com.sap.ipc.webui.pricing.UIConstants.Mode.EDIT)){
				ValueSelection currencyUnits = prCondition.getAllCurrencyUnits();
				ValueSelection pricingUnits = prCondition.getAvailablePricingUnitUnits();
				if (prCondition instanceof NewPricingCondition){
					currencyUnits = currencyUnits.getInclEmptyValue();
					pricingUnits = pricingUnits.getInclEmptyValue();
					ValueSelection conditionTypeNames = prCondition.getAvailableConditionTypeNames();
					conditionTypeNames = conditionTypeNames.getInclEmptyValue();
					prConditionForm.setConditionTypeNameSelection(conditionTypeNames.getValues());
	    			prConditionForm.setConditionTypeNameSelectionLabels(conditionTypeNames.getLabels());
				}
				else{
					//there may be a invalid pricing unit
					pricingUnits = pricingUnits.getInclValue(prCondition.getPricingUnitUnit());
				}
				prConditionForm.setCurrencyNameSelection(currencyUnits.getValues());
	    		prConditionForm.setCurrencyNameSelectionLabels(currencyUnits.getLabels());
		    	prConditionForm.setPricingUnitUnitNameSelection(pricingUnits.getValues());
			    prConditionForm.setPricingUnitUnitNameSelectionLabels(pricingUnits.getLabels());
			}
//			try {
				PropertyUtils.copyProperties(prConditionForm, prCondition);
				_pricingConditionForms.add(prConditionForm);
/*			} catch (InvocationTargetException e) {
				Throwable t = e.getTargetException();
				if (t == null)
					t = e;
				else if (t instanceof IPCException){
					throw (IPCException)t;
				}
*				servlet.log("PricingConditionForm.populate", t);
				throw new ServletException("PricingConditionForm.populate", t);*
			} catch (Throwable t) {
*				servlet.log("PricingConditionForm.populate", t);
				throw new ServletException("PricingConditionForm.populate", t);*
		   }*/
		}
	}

	public ArrayList getPricingConditions(){
		//?! neue (_pricingConditionForms) oder alte (_pricingConditions) ?
		//? wie neue aus Forms erzeugen ?
		return _pricingConditions;
	}

	public void setConditionTypeName(int index, String conditionTypeName) {
		((PricingConditionForm)_pricingConditionForms.get(index)).setConditionTypeName(conditionTypeName);
	}

	public String getConditionTypeName(int index) {
		return ((PricingConditionForm)_pricingConditionForms.get(index)).getConditionTypeName();
	}

	public void setConditionRateValue(int index, String conditionRateValue) {
		((PricingConditionForm)_pricingConditionForms.get(index)).setConditionRateValue(conditionRateValue);
	}

	public String getConditionRateValue(int index) {
		return ((PricingConditionForm)_pricingConditionForms.get(index)).getConditionRateValue();
	}

	public void setConditionCurrency(int index, String conditionCurrency) {
		((PricingConditionForm)_pricingConditionForms.get(index)).setConditionCurrency(conditionCurrency);
	}

	public String getConditionCurrency(int index) {
		return ((PricingConditionForm)_pricingConditionForms.get(index)).getConditionCurrency();
	}

	public void setPricingUnitValue(int index, String pricingUnitValue){
		((PricingConditionForm)_pricingConditionForms.get(index)).setPricingUnitValue(pricingUnitValue);
	}

	public String getPricingUnitValue(int index){
		return ((PricingConditionForm)_pricingConditionForms.get(index)).getPricingUnitValue();
	}

	public void setPricingUnitUnit(int index, String pricingUnitUnit){
		((PricingConditionForm)_pricingConditionForms.get(index)).setPricingUnitUnit(pricingUnitUnit);
	}

	public String getPricingUnitUnit(int index){
		return ((PricingConditionForm)_pricingConditionForms.get(index)).getPricingUnitUnit();
	}

	public void setPricingConditionForms(ArrayList pricingConditionForms){
		_pricingConditionForms = pricingConditionForms;
	}

	public ArrayList getPricingConditionForms(){
		return _pricingConditionForms;
	}

}