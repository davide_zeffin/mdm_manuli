/************************************************************************

	Copyright (c) 2001 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.ipc.webui.pricing.actionform;

import java.util.Collection;
import java.util.ArrayList;
import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import com.sap.spc.remote.client.object.IPCException;

import com.sap.spc.remote.client.object.IPCSession;
import com.sap.ipc.webui.pricing.model.*;

public class PricingConditionForm extends ActionForm {

   // --------------------------------------------------- Instance Variables

	/**
	 * The maintenance action we are performing (Create or Edit).
	 */
	private String _action = com.sap.ipc.webui.pricing.UIConstants.Action.EDIT;

	private Collection _currencyNameSelection = new ArrayList(0);
	private Collection _currencyNameSelectionLabels = new ArrayList(0);
	private Collection _conditionTypeNameSelection = new ArrayList(0);
	private Collection _conditionTypeNameSelectionLabels = new ArrayList(0);
	private Collection _pricingUnitUnitNameSelection = new ArrayList(0);
	private Collection _pricingUnitUnitNameSelectionLabels = new ArrayList(0);

	private String _stepNo = null;
	private String _counter = null;
	private String _conditionTypeName = null;
	private String _description = null;
	private String _conditionRateValue = null;
	private String _conditionCurrency = null;
	private String _pricingUnitValue = null;
	private String _pricingUnitUnit = null;
	private String _conditionValueValue = null;
	private String _documentCurrency = null;
	private String _conversionNumerator = null;
	private String _conversionDenominator = null;
	private String _conversionExponent = null;
	private String _conditionBaseValue = null;

	private String _exchangeRate = null;
	private String _directExchangeRate = null;
	private String _docExchangeRate = null;
	private String _localCurrency = null;
	private String _factor = null;

	private boolean _isStatistical = false;
	private char _inactive = ' ';
	private char _conditionClass = ' ';
	private char _calculationType = ' ';
	private char _conditionCategory = ' ';
	private char _conditionControl = ' ';
	private char _conditionOrigin = ' ';

	private String _conversionUnit1 = null;
	private String _conversionUnit2 = null;

	private String _cancelled = null;
	
	
	//Added newly-for displaying scale type
	private char _scaleType =' ';
	

	// ----------------------------------------------------------- Properties

	/**
	 * Return the maintenance action.
	 */
	public String getAction() {
		return (_action);
	}


	/**
	 * Set the maintenance action.
	 *
	 * @param action The new maintenance action.
	 */
	public void setAction(String action) {
		_action = action;
	}

	public String getStepNo() {
		return _stepNo;
	}

	public void setStepNo(String stepNo) {
		_stepNo = stepNo;
	}

	public String getCounter() {
		return _counter;
	}

	public void setCounter(String counter) {
		_counter = counter;
	}

	public String getConditionTypeName() {
		return _conditionTypeName;
	}

	public void setConditionTypeName(String conditionTypeName) {
		_conditionTypeName = conditionTypeName;
	}

	public void setDescription(String description){
		_description = description;
	}

	public String getDescription(){
		return _description;
	}

	public String getConditionRateValue() {
		return _conditionRateValue;
	}

	public void setConditionRateValue(String conditionRateValue) {
		_conditionRateValue = conditionRateValue;
	}

	public String getConditionCurrency() {
		return _conditionCurrency;
	}

	public void setConditionCurrency(String conditionCurrency) {
		_conditionCurrency = conditionCurrency;
	}

	public void setPricingUnitValue(String pricingUnitValue){
		_pricingUnitValue = pricingUnitValue;
	}

	public String getPricingUnitValue(){
		return _pricingUnitValue;
	}

	public void setPricingUnitUnit(String pricingUnitUnit){
		_pricingUnitUnit = pricingUnitUnit;
	}

	public String getPricingUnitUnit(){
		return _pricingUnitUnit;
	}

	public void setConditionValueValue(String conditionValueValue){
		_conditionValueValue = conditionValueValue;
	}

	public String getConditionValueValue(){
		return _conditionValueValue;
	}

	public void setDocumentCurrency(String documentCurrency){
		_documentCurrency = documentCurrency;
	}

	public String getDocumentCurrency(){
		return _documentCurrency;
	}

	public void setConversionNumerator(String conversionNumerator){
		_conversionNumerator = conversionNumerator;
	}

	public String getConversionNumerator(){
		return _conversionNumerator;
	}

	public void setConversionDenominator(String conversionDenominator){
		_conversionDenominator = conversionDenominator;
	}

	public String getConversionDenominator(){
		return _conversionDenominator;
	}

	public void setConversionExponent(String conversionExponent){
		_conversionExponent = conversionExponent;
	}

	public String getConversionExponent(){
		return _conversionExponent;
	}

	public void setConditionBaseValue(String conditionBaseValue){
		_conditionBaseValue = conditionBaseValue;
	}

	public String getConditionBaseValue(){
		return _conditionBaseValue;
	}

	public void setExchangeRate(String exchangeRate){
		_exchangeRate = exchangeRate;
	}

	public String getExchangeRate(){
		return _exchangeRate;
	}

	public void setDirectExchangeRate(String directExchangeRate){
		_directExchangeRate = directExchangeRate;
	}

	public String getDirectExchangeRate(){
		return _directExchangeRate;
	}

	public void setDocExchangeRate(String docExchangeRate){
		_docExchangeRate = docExchangeRate;
	}

	public String getDocExchangeRate(){
		return _docExchangeRate;
	}

	public void setLocalCurrency(String localCurrency){
		_localCurrency = localCurrency;
	}

	public String getLocalCurrency(){
		return _localCurrency;
	}

	public void setFactor(String factor){
		_factor = factor;
	}

	public String getFactor(){
		return _factor;
	}

	public void setStatistical(boolean isStatistical){
		_isStatistical = isStatistical;
	}

	public boolean isStatistical(){
		return _isStatistical;
	}

	public void setInactive(char inactive){
		_inactive = inactive;
	}

	public char getInactive(){
		return _inactive;
	}

	public void setConditionClass(char conditionClass){
		_conditionClass = conditionClass;
	}

	public char getConditionClass(){
		return _conditionClass;
	}

	public void setCalculationType(char calculationType){
		_calculationType = calculationType;
	}

	public char getCalculationType(){
		return _calculationType;
	}

	public void setConditionCategory(char conditionCategory){
		_conditionCategory = conditionCategory;
	}

	public char getConditionCategory(){
		return _conditionCategory;
	}

	public void setConditionControl(char conditionControl){
		_conditionControl = conditionControl;
	}

	public char getConditionControl(){
		return _conditionControl;
	}

	public void setConditionOrigin(char conditionOrigin){
		_conditionOrigin = conditionOrigin;
	}

	public char getConditionOrigin(){
		return _conditionOrigin;
	}

	public void setConversionUnit1(String conversionUnit1){
		_conversionUnit1 = conversionUnit1;
	}

	public String getConversionUnit1(){
		return _conversionUnit1;
	}

	public void setConversionUnit2(String conversionUnit2){
		_conversionUnit2 = conversionUnit2;
	}

	public String getConversionUnit2(){
		return _conversionUnit2;
	}




	// --------------------------------------------------------- Public Methods

	/**
	 * Validate the properties that have been set from this HTTP request,
	 * and return an <code>ActionErrors</code> object that encapsulates any
	 * validation errors that have been found.  If no errors are found, return
	 * <code>null</code> or an <code>ActionErrors</code> object with no
	 * recorded error messages.
	 *
	 * @param mapping The mapping used to select this instance
	 * @param request The servlet request we are processing
	 */
	public ActionErrors validate(ActionMapping mapping,
								 HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();

		if (_action.equals(com.sap.ipc.webui.pricing.UIConstants.Action.DISPLAY))
			return (errors);

		if (_action.equals(com.sap.ipc.webui.pricing.UIConstants.Action.CREATE)
			&& ((_conditionTypeName == null) || (_conditionTypeName.length() < 1)))
			errors.add("condititionTypeName",
					   new ActionError("prc.error.condTypeName.required"));
		return (errors);

	}

	public void setCurrencyNameSelection(Collection currencyNameSelection){
		_currencyNameSelection = currencyNameSelection;
	}

	public Collection getCurrencyNameSelection(){
		return _currencyNameSelection;
	}

	public void setCurrencyNameSelectionLabels(Collection currencyNameSelectionLabels){
		_currencyNameSelectionLabels = currencyNameSelectionLabels;
	}

	public Collection getCurrencyNameSelectionLabels(){
		return _currencyNameSelectionLabels;
	}

	public void setConditionTypeNameSelection(Collection conditionTypeNameSelection){
		_conditionTypeNameSelection = conditionTypeNameSelection;
	}

	public Collection getConditionTypeNameSelection(){
		return _conditionTypeNameSelection;
	}

	public void setConditionTypeNameSelectionLabels(Collection conditionTypeNameSelectionLabels){
		_conditionTypeNameSelectionLabels = conditionTypeNameSelectionLabels;
	}

	public Collection getConditionTypeNameSelectionLabels(){
		return _conditionTypeNameSelectionLabels;
	}

	public void setPricingUnitUnitNameSelection(Collection pricingUnitUnitNameSelection){
		_pricingUnitUnitNameSelection = pricingUnitUnitNameSelection;
	}

	public Collection getPricingUnitUnitNameSelection(){
		return _pricingUnitUnitNameSelection;
	}

	public void setPricingUnitUnitNameSelectionLabels(Collection pricingUnitUnitNameSelectionLabels){
		_pricingUnitUnitNameSelectionLabels = pricingUnitUnitNameSelectionLabels;
	}

	public Collection getPricingUnitUnitNameSelectionLabels(){
		return _pricingUnitUnitNameSelectionLabels;
	}

	public void setCancelled(String cancelled){
		_cancelled = cancelled;
	}

	public String getCancelled(){
		return _cancelled;
	}
	
	
	public char getScaleType() {
		return _scaleType;
	}
	
	public void setScaleType(char scaleType) {
		_scaleType=scaleType;
	}
}