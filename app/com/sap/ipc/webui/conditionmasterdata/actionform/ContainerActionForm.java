/*******************************************************************************

    Copyright (c) 2005 by SAP AG

    All rights to both implementation and design are reserved.

    Use and copying of this software and preparation of derivative works based
    upon this software are not permitted.

    Distribution of this software is restricted. SAP does not make any warranty
    about the software, its performance or its conformity to any specification.

*******************************************************************************/
package com.sap.ipc.webui.conditionmasterdata.actionform;

import com.sap.ipc.webui.conditionmasterdata.model.InitialiseConditionAnalyserPannel;

import org.apache.struts.action.ActionForm;

/**
 * @author d043319
 */
public class ContainerActionForm extends ActionForm {

private InitialiseConditionAnalyserPannel initCondAnalyserPannel;


/**
 * @return
 */
public InitialiseConditionAnalyserPannel getInitCondAnalyserPannel() {
	return initCondAnalyserPannel;
}

/**
 * @param pannel
 */
public void setInitCondAnalyserPannel(InitialiseConditionAnalyserPannel pannel) {
	initCondAnalyserPannel = pannel;
}

}
