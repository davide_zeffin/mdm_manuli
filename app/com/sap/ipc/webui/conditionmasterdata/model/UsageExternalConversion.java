/*
 * Created on Aug 18, 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.ipc.webui.conditionmasterdata.model;



import java.util.Enumeration;
import java.util.Vector;
import java.util.Hashtable;
import java.util.List;
import java.util.HashMap;

import com.sap.spc.remote.client.object.*;
import com.sap.spc.remote.client.object.imp.tcp.TcpDefaultIPCSession;
import com.sap.spc.remote.shared.command.GetConditionRecordsFromDatabase;
import com.sap.spc.remote.client.tcp.PricingConverter;
import com.sap.spc.remote.shared.command.ConvertInternalToExternal;

/**
 * @author I019099
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */

/*  This class should do all kind of conversion related to usage fields 
 *  
 */
public class UsageExternalConversion {
	
	private String [] _usageFldValues;
	// constructor
	public UsageExternalConversion(String usage, IPCSession IPCsess, String[] usageFldNames, String[] usageFldValues){
		String firstFldName = null;
		String secondFldName = null ;
		TcpDefaultIPCSession tcpIPCSession = (TcpDefaultIPCSession)IPCsess;
		IPCClient client = IPCsess.getIPCClient();
		
		if ( usage.equals("PR")){
			int count = usageFldNames.length;
					
				for (int i=0; i<count; i++){
				// find what appears first and what next? and set the fieldnames accordingly
					if (firstFldName == null){
		              if (usageFldNames[i].equals("KBETR") || usageFldNames[i].equals("KONWA")){
		              	firstFldName = usageFldNames[i];
		              	if( firstFldName.equals("KBETR")) {
		              		secondFldName = "KONWA";
		              	}else{
		              		secondFldName= "KBETR";
		              	}
		              }
					} 
					// convert now 
					if (usageFldNames[i].equals(firstFldName)){
			
						for (int j=i+1; j<count; j++){
						 if (usageFldNames[j].equals(secondFldName)){
						 	if (firstFldName.equals("KBETR")){
						 	usageFldValues[i] = PricingConverter.convertCurrencyValueInternalToExternal(usageFldValues[i],usageFldValues[j],tcpIPCSession, null, null);
						 	 break;
						 	} else if (firstFldName.equals("KONWA")){
								usageFldValues[j] = PricingConverter.convertCurrencyValueInternalToExternal(usageFldValues[j],usageFldValues[i],tcpIPCSession, null, null);
								break;	
						 	} else {
						 		continue;
						 	}
							
			 	
						}
			
					}
		
		
				}
				}
				
		}else if(usage.equals("FG")) {
			
			int count = usageFldNames.length;
			String [] fldName = new String[1];
			String [] intValue = new String[1];
			String [] extValue = new String[1];
			
			for (int i=0; i<count; i++) {
				
				if (usageFldNames[i].equals("FGD_ADD_PRODUCT")){
					fldName[0] = usageFldNames[i];
					intValue[0] = usageFldValues[i];
					HashMap output = client.convertInternalToExternal(fldName, intValue);
					extValue = (String[])output.get(ConvertInternalToExternal.EXTERNAL_VALUES);
					usageFldValues[i] = extValue[0];
				}
			}
			
			
			
			
			
			
			
		} else if (usage.equals("CD")){
			// campaign guid to be converted to campaign id.
			HashMap output = client.convertInternalToExternal(usageFldNames, usageFldValues);
			usageFldValues = (String[])output.get(ConvertInternalToExternal.EXTERNAL_VALUES);
			
			
		}else {
		}
	
	_usageFldValues = usageFldValues;
	

}

public String[] getConvertedUsageFldValues(){
	return _usageFldValues;
}

}


