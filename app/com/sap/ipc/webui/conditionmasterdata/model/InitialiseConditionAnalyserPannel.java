/*******************************************************************************

    Copyright (c) 2005 by SAP AG

    All rights to both implementation and design are reserved.

    Use and copying of this software and preparation of derivative works based
    upon this software are not permitted.

    Distribution of this software is restricted. SAP does not make any warranty
    about the software, its performance or its conformity to any specification.

*******************************************************************************/
package com.sap.ipc.webui.conditionmasterdata.model;

import java.util.Enumeration;
import java.util.Vector;

/**
 * @author d043319
 */
public class InitialiseConditionAnalyserPannel {

private String application;
private String usage;
private String selectedConditionGroup;
private Vector conditionGroupNames;
private Enumeration condGroupEnum;
private SelectionField applicationField;
private SelectionField usageField;
private SelectionField maintGroupField;
private String paramName;
private String paramValue; 
public static final String SessionAttributeName = "InitialiseConditionAnalyserPannel";
public static final String FormFieldNameApplication = "Application";
public static final String FormFieldNameUsage       = "Usage";
public static final String FormFieldNameCondgroup   = "ConditionGroup";
public static final String FormButtonCancel = "org.apache.struts.taglib.html.CANCEL";
public static final String URLMaintGroup = "maintgroup";
public static final String URLUsage = "usage";
public static final String URLApplication = "application";
public static final String URLParamName = "paramname";
public static final String URLParamValue = "paramvalue";

public InitialiseConditionAnalyserPannel()
{
conditionGroupNames = new Vector();
}

/**
 * @return
 */
public String getApplication() {
	return application;
}

/**
 * @return
 */
public String getUsage() {
	return usage;
}

/**
 * @param string
 */
public void setApplication(String string) {
	if ( applicationField == null){
		applicationField = new SelectionField();
	}
	applicationField.setFieldValue(string);
	application = applicationField.getFieldValue();
}

/**
 * @param string
 */
public void setUsage(String string) {
	if ( usageField == null ){
		usageField = new SelectionField();
	}
	usageField.setFieldValue(string);
	usage = usageField.getFieldValue();
}

public void addConditionGroupName( String argGroupName)
{
  conditionGroupNames.add(argGroupName); 	
}

public boolean hasMoreElements()
{
	if ( condGroupEnum == null)
	{
		condGroupEnum = conditionGroupNames.elements();
	}
	
	return condGroupEnum.hasMoreElements();
}

public String nextElement()
{
    if ( condGroupEnum == null )
    {
		condGroupEnum = conditionGroupNames.elements();    	
    }
    
    return (String) condGroupEnum.nextElement();
}

public void resetEnumeration()
{
	condGroupEnum = conditionGroupNames.elements(); 	
}



/**
 * @return
 */
public String getSelectedConditionGroup() {
	return selectedConditionGroup;
}

/**
 * @param string
 */
public void setSelectedConditionGroup(String string) {
	if ( maintGroupField == null){
		maintGroupField = new SelectionField();
	}
	maintGroupField.setFieldValue(string);
	selectedConditionGroup = maintGroupField.getFieldValue();
}

/**
 * @return
 */
public SelectionField getApplicationField() {
	return applicationField;
}

/**
 * @return
 */
public SelectionField getMaintGroupField() {
	return maintGroupField;
}

/**
 * @return
 */
public SelectionField getUsageField() {
	return usageField;
}

/**
 * @param field
 */
public void setApplicationField(SelectionField field) {
	applicationField = field;
	application = applicationField.getFieldValue();
}

/**
 * @param field
 */
public void setMaintGroupField(SelectionField field) {
	maintGroupField = field;
	selectedConditionGroup = maintGroupField.getFieldValue();
}

/**
 * @param field
 */
public void setUsageField(SelectionField field) {
	usageField = field;
	usage = usageField.getFieldValue();
}

/**
 * @return
 */
public String getParamName() {
	return paramName;
}

/**
 * @return
 */
public String getParamValue() {
	return paramValue;
}

/**
 * @param string
 */
public void setParamName(String string) {
	paramName = string;
}

/**
 * @param string
 */
public void setParamValue(String string) {
	paramValue = string;
}

}