/*
 * Created on Jul 13, 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.ipc.webui.conditionmasterdata.model;

/**
 * @author I026516
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class FGScaleStructure {
	
	private String FGScaleLineRefs;
	private String FGMinQuantities;
	private String FGQuantityUnits;
	
	FGScaleStructure() {
	}
	
	public String getFGScaleLineRefs(){
		return this.FGScaleLineRefs;
	}
	
	public void setFGScaleLineRefs(String FGScaleLineRefs){
		this.FGScaleLineRefs = FGScaleLineRefs;
	}

	public String getFGMinQuantities(){
		return this.FGMinQuantities;
	}
	
	public void setFGMinQuantities(String FGMinQuantities){
		this.FGMinQuantities = FGMinQuantities;
	}

	public String getFGQuantityUnits(){
		return this.FGQuantityUnits;
	}
	
	public void setFGQuantityUnits(String FGQuantityUnits){
		this.FGQuantityUnits = FGQuantityUnits;
	}

}
