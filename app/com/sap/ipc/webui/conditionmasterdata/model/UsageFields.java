/*
 * Created on Jul 13, 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.ipc.webui.conditionmasterdata.model;

/**
 * @author I026516
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class UsageFields {
	
	private String usageFieldName;
	private String usageFieldValue;
	private boolean errorStatus;
	
	UsageFields(){
	}
	
	public String getUsageFieldName(){
		return this.usageFieldName;
	}
	
	public void setUsageFieldName(String usageFieldName){
		this.usageFieldName = usageFieldName;
	}
	
	public String getUsageFieldValue(){
		return this.usageFieldValue;
	}
	
	public void setUsageFieldValue(String usageFieldValue){
		this.usageFieldValue = usageFieldValue;
	}
	
	public boolean getErrorStatus(){
		return this.errorStatus;
	}
	
	public void setErrorStatus(boolean errorStatus){
		this.errorStatus = errorStatus;
	}
}
