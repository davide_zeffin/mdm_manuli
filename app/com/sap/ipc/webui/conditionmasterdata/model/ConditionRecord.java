/*
 * Created on Jul 13, 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
 
package com.sap.ipc.webui.conditionmasterdata.model;

import java.util.Vector;
import java.util.Iterator;

//import com.sap.ipc.webui.conditionmasterdata.model.*;
import com.sap.ipc.webui.conditionmasterdata.model.UsageFields;
import com.sap.ipc.webui.conditionmasterdata.model.PricingScaleStructure;

/**
 * @author I026516
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ConditionRecord {
	
	private String numberOfRecordReturned;
	private String conditionRecordIds;
	private String validityEnds;
	
	private String validityStarts;
	private String objectIds;
	private String releaseStatus;
	
	private String conditionTypes;
	private String supplementaryConditionTypes;
	private String groupIds;
	
	private String maintenanceStatus;
	private String supplementaryConditionIds;
	private String dimensionNumbers;
	
	private String applicationFieldNumbers;
	private String applicationOffsets;

	private String usageFieldNumbers;
	private String usageFieldOffsets;
	
	private String scaleBaseTypes;
	private String scaleLineNumbers;
	private String scaleLineOffsets;
	
	private ApplicationFields [] applicationFields = null;
	private UsageFields [] usageFields = null;
	private PricingScaleStructure [] pricingScaleStructure = null;
	
	private FGScaleStructure [] fgScaleStructure = null;
	private FGQuantity [] fgQuantity = null;
	private FGProduct [] fgProduct = null;
	
	private Vector appFieldsBluePrintSequence = new Vector();
	private Vector usgFieldsBluePrintSequence = new Vector();
	private Vector scaleHeaderBluePrintSequence = new Vector();
	private Vector pricingScaleDetailsBluePrintSequence = new Vector();
	
	private boolean scaleExist = false;
	//if inconsistentConditionRecordStatus is false it means condition record is proper and consistent
	private boolean inconsistentConditionRecordStatus = false;
	//if condition record is inconsistent i.e inconsistentConditionRecordStatus is true, 
	//this attribute will store the details of inconsistency
	private Vector inconsistencyDetails = new Vector(); 
	
	private String scaleUnit;
	private String scaleCurrency;
	private String scaleType;
	private String usage;
	
	private final String SCALE_UNIT = "SCALE_UNIT";
	private final String SCALE_CURRENCY = "SCALE_CURRENCY";
	private final String SCALE_TYPE = "SCALE_TYPE";
	private final String FGD_ADD_PRODUCT = "FGD_ADD_PRODUCT";
	private final String FGD_LSTANR = "FGD_LSTANR";
	
	private final String PRICING_USAGE = "PR";
	private final String FREEGOODS_USAGE = "FG";

	public ConditionRecord(){
	}
	
	public String getNumberOfRecordReturned(){
		return this.numberOfRecordReturned;
	}
	
	public void setNumberOfRecordReturned(String numberOfRecordReturned){
		this.numberOfRecordReturned = numberOfRecordReturned;
	}	
	
	public String getConditionRecordIds(){
		return this.conditionRecordIds;
	}
	
	public void setConditionRecordIds(String conditionRecordIds){
			this.conditionRecordIds = conditionRecordIds;
			//checkForInconsistency(conditionRecordIds, "Condition Record Id");
		checkForInconsistency(conditionRecordIds, "cmdd.CondShow.condRecId");
	}
			
	public String getValidityEnds(){
		return this.validityEnds;
	}
	
	public void setValidityEnds(String validityEnds){
			this.validityEnds = validityEnds;
			//checkForInconsistency(validityEnds, "Validity Ends");
		checkForInconsistency(validityEnds, "cmdd.CondShow.validityEnds");
	}
			
	public String getValidityStarts(){
		return this.validityStarts;
	}

	public void setValidityStarts(String validityStarts){
		this.validityStarts = validityStarts;
		//checkForInconsistency(validityStarts, "Validity Starts");
		checkForInconsistency(validityStarts, "cmdd.CondShow.validityStarts");
	}
	
	public String getObjectIds(){
		return this.objectIds;
	}
	
	public void setObjectIds(String objectIds){
		this.objectIds = objectIds;
		//checkForInconsistency(objectIds, "Object Id");
	}

	public String getReleaseStatus(){
		return this.releaseStatus;
	}
	
	public void setReleaseStatus(String releaseStatus){
		this.releaseStatus = releaseStatus;
		//checkForInconsistency(releaseStatus, "Release Status");
		checkForInconsistency(releaseStatus, "cmdd.CondShow.releaseStatus");
	}
		
	public String getConditionTypes(){
		return this.conditionTypes;
	}
	
	public void setConditionTypes(String conditionTypes){
		this.conditionTypes = conditionTypes;
		//checkForInconsistency(conditionTypes,"Condition Type");
		checkForInconsistency(conditionTypes,"cmdd.CondShow.condType");
	}

	public String getSupplementaryConditionTypes(){
		return this.supplementaryConditionTypes;
	}
	
	public void setSupplementaryConditionTypes(String supplementaryConditionTypes){
		this.supplementaryConditionTypes = supplementaryConditionTypes;
		//checkForInconsistency(supplementaryConditionTypes, "Supplementary Condition Type");
	}

	public String getGroupIds(){
		return this.groupIds;
	}
	
	public void setGroupIds(String groupIds){
		this.groupIds = groupIds;
		//checkForInconsistency(groupIds, "Group Id");
	}

	public String getMaintenanceStatus(){
		return this.maintenanceStatus;
	}
	
	public void setMaintenanceStatus(String maintenanceStatus){
		this.maintenanceStatus = maintenanceStatus;
	}
	
	
	public String getSupplementaryConditionIds(){
		return this.supplementaryConditionIds;
	}
	
	public void setSupplementaryConditionIds(String supplementaryConditionIds){
		this.supplementaryConditionIds = supplementaryConditionIds;
		//checkForInconsistency(supplementaryConditionIds, "Supplementary Condition Id");
	}
	
	public String getDimensionNumbers(){
		return this.dimensionNumbers;
	}
	
	public void setDimensionNumbers(String dimensionNumbers){
		this.dimensionNumbers = dimensionNumbers;
		//checkForInconsistency(dimensionNumbers, "Dimension Number");
		checkForInconsistency(dimensionNumbers, "cmdd.CondShow.dimNumber");
		
		if (new Integer(this.dimensionNumbers).intValue() > 0)
		   scaleExist = true;
	}
	
	public String getApplicationFieldNumbers(){
		return this.applicationFieldNumbers;
	}
	
	public void setApplicationFieldNumbers(String applicationFieldNumbers){
		this.applicationFieldNumbers = applicationFieldNumbers;
		//checkForInconsistency(applicationFieldNumbers, "Application Field Number");
	}
	
	public String getApplicationOffsets(){
		return this.applicationOffsets;
	}

	public void setApplicationOffsets(String applicationOffsets){
		this.applicationOffsets = applicationOffsets;
		//checkForInconsistency(applicationOffsets, "Application Field Offsets");
	}
	
	public String getUsageFieldNumbers(){
		return this.usageFieldNumbers;
	}

	public void setUsageFieldNumbers(String usageFieldNumbers){
		this.usageFieldNumbers = usageFieldNumbers;
		//checkForInconsistency(usageFieldNumbers, "Usage Field Number");
	}
	
	public String getUsageFieldOffsets(){
		return this.usageFieldOffsets;
	}
	
	public void setUsageFieldOffsets(String usageFieldOffsets){
		this.usageFieldOffsets = usageFieldOffsets;
		//checkForInconsistency(usageFieldOffsets, "Usage Field Offset");
	}
	
	public String getScaleBaseTypes(){
		return this.scaleBaseTypes;
	}
	
	public void setScaleBaseTypes(String scaleBaseTypes){
		this.scaleBaseTypes = scaleBaseTypes;
		if (scaleExist)
			//checkForInconsistency(scaleBaseTypes, "Scale Base Type");
		checkForInconsistency(scaleBaseTypes, "cmdd.CondShow.scaleBaseType");
	}
	
	public String getScaleLineNumbers(){
		return this.scaleLineNumbers;
	}
	
	public void setScaleLineNumbers(String scaleLineNumbers){
		this.scaleLineNumbers = scaleLineNumbers;
		//checkForInconsistency(scaleLineNumbers, "Scale Line Number");
	}
	
	public String getScaleLineOffsets(){
		return this.scaleLineOffsets;
	}
	
	public void setScaleLineOffsets(String scaleLineOffsets){
		this.scaleLineOffsets = scaleLineOffsets;
		//checkForInconsistency(scaleLineOffsets, "Scale Line Offset");
	}
	
	public void setApplicationFields(String [] appFieldName, String [] appFieldValue, String [] errorStatusAppFields, int dim, int startIndex){
		applicationFields = new ApplicationFields[dim];
		int appFieldLength = dim + startIndex;
		String applicationFieldName = null;
		String applicationFieldValue = null;
		String errorStatus = null;
		int appFieldIndex = 0;
		
		for(int index = startIndex; index < appFieldLength; index++){
			applicationFieldName = appFieldName[index];
			applicationFieldValue = appFieldValue[index];

			applicationFields[appFieldIndex] = new ApplicationFields();
			applicationFields[appFieldIndex].setApplicationFieldName(applicationFieldName);
			applicationFields[appFieldIndex].setApplicationFieldValue(applicationFieldValue);
			
			//checkForInconsistency(applicationFieldName, "Application Field Name");
			checkForInconsistency(applicationFieldName, "cmdd.CondShow.appFieldName");
			//checkForInconsistency(applicationFieldValue,"cmdd.CondShow.appFieldName" + applicationFieldName);
			checkForInconsistency(applicationFieldValue,"cmdd.CondShow.appFieldName",applicationFieldName);
			
			if (errorStatusAppFields != null){
				errorStatus = errorStatusAppFields[index];
				
				if (errorStatus != null){
					if (errorStatus.equalsIgnoreCase("SUCCESS")){
						//means no error
						applicationFields[appFieldIndex].setErrorStatus(false);
					}else{
						applicationFields[appFieldIndex].setErrorStatus(true);
					}
				}else{
					applicationFields[appFieldIndex].setErrorStatus(false);
				}
				
			}else{
				applicationFields[appFieldIndex].setErrorStatus(false);
			}
			appFieldIndex++;
			
		}
	}
	
	public ApplicationFields[] getApplicationFields(){
		return this.applicationFields;
	}
	
	public void setUsageFields(String [] usageFieldName, String [] usageFieldValue, int dim, int startIndex){
		usageFields = new UsageFields[dim];
		int usageFieldLength = dim + startIndex;
		int usgFieldIndex = 0;
		
		for(int index = startIndex; index < usageFieldLength; index++){
			usageFields[usgFieldIndex] = new UsageFields();
			usageFields[usgFieldIndex].setUsageFieldName(usageFieldName[index]);
			usageFields[usgFieldIndex].setUsageFieldValue(usageFieldValue[index]);
			
			//check for inconsistency for all usage fields except for scale currency and scale unit
			//scale currency should be checked for inconsistency only if scale base type is 'B'
			//otherwise scale unit needs to be checked for inconsistency.
			
			if ( !(usageFieldName[index].equalsIgnoreCase(SCALE_UNIT))
			     && !(usageFieldName[index].equalsIgnoreCase(SCALE_CURRENCY))
				 && !(usageFieldName[index].equalsIgnoreCase(FGD_LSTANR))
				 && !(usageFieldName[index].equalsIgnoreCase(FGD_ADD_PRODUCT )) ){
					checkForInconsistency(usageFieldName[index], "cmdd.CondShow.usgFieldName");
					checkForInconsistency(usageFieldValue[index], "cmdd.CondShow.usgFieldName", usageFieldName[index]);
			}
			
			if (usageFieldName[index].equalsIgnoreCase(SCALE_UNIT)){
				scaleUnit = usageFields[usgFieldIndex].getUsageFieldValue();
				if(scaleBaseTypes != null && !(scaleBaseTypes.equalsIgnoreCase("B"))){
					checkForInconsistency(usageFieldName[index], "cmdd.CondShow.usgFieldName");
					checkForInconsistency(usageFieldValue[index], "cmdd.CondShow.usgFieldName", usageFieldName[index]);
				}
			}
				
			if (usageFieldName[index].equalsIgnoreCase(SCALE_CURRENCY)){
				scaleCurrency = usageFields[usgFieldIndex].getUsageFieldValue();
				if( scaleBaseTypes != null && scaleBaseTypes.equalsIgnoreCase("B")){
					checkForInconsistency(usageFieldName[index], "cmdd.CondShow.usgFieldName");
					checkForInconsistency(usageFieldValue[index], "cmdd.CondShow.usgFieldName", usageFieldName[index]);
				}
			}
				
			if (usageFieldName[index].equalsIgnoreCase(SCALE_TYPE))
				scaleType = usageFields[usgFieldIndex].getUsageFieldValue();
				
			usgFieldIndex++;				
				
		}
	}
	
	public UsageFields[] getUsageFields(){
		return this.usageFields;
	}
	
	public void setPricingScaleStructure(String [] prScaleLineRef, String [] prScaleAmounts, String [] prScaleRates,
										 String [] prScaleRatesCurrUnit, String [] prScalePricingUnit, String [] prScaleProductUnit, 
										 int dim, int startIndex){
		pricingScaleStructure = new PricingScaleStructure[dim];
		int prcScaleIndex = 0;
		int pricingScaleLength = dim + startIndex;
		
		for(int index = startIndex; index < pricingScaleLength; index++){
			pricingScaleStructure[prcScaleIndex] = new PricingScaleStructure();
			pricingScaleStructure[prcScaleIndex].setPRScaleLineRefs(prScaleLineRef[index]);
			pricingScaleStructure[prcScaleIndex].setPRScaleAmounts(prScaleAmounts[index]);
			pricingScaleStructure[prcScaleIndex].setPRScaleRates(prScaleRates[index]);
			pricingScaleStructure[prcScaleIndex].setPRScaleRatesCurrUnit(prScaleRatesCurrUnit[index]);
			pricingScaleStructure[prcScaleIndex].setPRScalePricingUnit(prScalePricingUnit[index]);
			pricingScaleStructure[prcScaleIndex].setPRScaleProductUnit(prScaleProductUnit[index]);	
			prcScaleIndex++;	
			
			//checkForInconsistency(prScaleLineRef[index], "Scale Line Refs");
			//checkForInconsistency(prScaleAmounts[index], "Scale Amount");
			checkForInconsistency(prScaleAmounts[index], "cmdd.CondShow.sclAmount");
			//checkForInconsistency(prScaleRates[index], "Scale Rate");
			checkForInconsistency(prScaleRates[index], "cmdd.CondShow.sclRate");

		}
	}
	
	public PricingScaleStructure[] getPricingScaleStructure(){
		return this.pricingScaleStructure;
	}
	
	public void setFGScaleStructure(String [] fgScaleLineRefs, String [] fgMinQuantities, String [] fgQuantityUnits, int dim, int startIndex){
		
		fgScaleStructure = new FGScaleStructure[dim];
		int fgScaleIndex = 0;
		int fgScaleLength = dim + startIndex;
		
		for(int index = startIndex; index < fgScaleLength; index++){
			fgScaleStructure[fgScaleIndex] = new FGScaleStructure();
			fgScaleStructure[fgScaleIndex].setFGScaleLineRefs(fgScaleLineRefs[index]);
			fgScaleStructure[fgScaleIndex].setFGMinQuantities(fgMinQuantities[index]);
			fgScaleStructure[fgScaleIndex].setFGQuantityUnits(fgQuantityUnits[index]);	
			fgScaleIndex++;		
			
			//checkForInconsistency(fgScaleLineRefs[index], "Scale Line Refs");
			//checkForInconsistency(fgMinQuantities[index], "Free Goods Minimum Quantity");
			checkForInconsistency(fgMinQuantities[index], "cmdd.CondShow.fgGoodsMinQty");
			//checkForInconsistency(fgQuantityUnits[index], "Free Goods Quantity Unit");
			checkForInconsistency(fgQuantityUnits[index], "cmdd.CondShow.fgGoodsQtyUnit");
			

		}
	}
	
	public FGScaleStructure[] getFGScaleStructure(){
		return this.fgScaleStructure;
	}
	
	public void setFGQuantity(String [] fgQuantities, String [] fgAddQuantities, String [] fgAddQuantityUnits, int dim, int startIndex){
		
		fgQuantity = new FGQuantity[dim];
		int fgQuantityIndex = 0;
		int fgQuantityLength = dim + startIndex;
		
		for(int index = startIndex; index < fgQuantityLength; index++){
			fgQuantity[fgQuantityIndex] = new FGQuantity();
			fgQuantity[fgQuantityIndex].setFGQuantities(fgQuantities[index]);
			fgQuantity[fgQuantityIndex].setFGAddQuantities(fgAddQuantities[index]);
			fgQuantity[fgQuantityIndex].setFGAddQuantityUnits(fgAddQuantityUnits[index]);	
			fgQuantityIndex++;	
			
			//checkForInconsistency(fgQuantities[index], "Quantity");
			checkForInconsistency(fgQuantities[index], "cmdd.CondShow.fgQty");
			//checkForInconsistency(fgAddQuantities[index], "Free Quantity");
			checkForInconsistency(fgAddQuantities[index], "cmdd.CondShow.freeQuantity");
			//checkForInconsistency(fgAddQuantityUnits[index],"Free Quantity Unit" );
			checkForInconsistency(fgAddQuantityUnits[index],"cmdd.CondShow.freeQuantityUnit" );


		}
	}
	
	public FGQuantity[] getFGQuantity(){
		return this.fgQuantity;
	}
	
	public void setFGProduct(String [] fgAddProducts, String [] fgCalcTypes, String [] fgIndicators, int dim, int startIndex){
		
		fgProduct = new FGProduct[dim];
		int fgProductIndex = 0;
		int fgProductLength = dim + startIndex;
		
		for(int index = startIndex; index < fgProductLength; index++){
			fgProduct[fgProductIndex] = new FGProduct();
			fgProduct[fgProductIndex].setFGAddProducts(fgAddProducts[index]);
			fgProduct[fgProductIndex].setFGCalcTypes(fgCalcTypes[index]);
			fgProduct[fgProductIndex].setFGIndicators(fgIndicators[index]);	
			fgProductIndex++;		
			
			//checkForInconsistency(fgAddProducts[index], "Free Product");
			checkForInconsistency(fgAddProducts[index], "cmdd.CondShow.freeProduct");
			//checkForInconsistency(fgCalcTypes[index], "Calculation Rule");
			checkForInconsistency(fgCalcTypes[index], "cmdd.CondShow.fgCalRule");
			//checkForInconsistency(fgIndicators[index], "Free Goods Type");
			checkForInconsistency(fgIndicators[index], "cmdd.CondShow.fgType");
			

		}
	}
	
	public FGProduct[] getFGProduct(){
		return this.fgProduct;
	}
	
	public void setAppFieldsBluePrintSequence(Vector applicationFields){
		this.appFieldsBluePrintSequence = applicationFields;
	}
	
	public Vector getAppFieldsBluePrintSequence(){
		return this.appFieldsBluePrintSequence;
	}
	
	
	public void setUsgFieldsBluePrintSequence(Vector usageFields){
		this.usgFieldsBluePrintSequence = usageFields;
	}
	
	public Vector getUsgFieldsBluePrintSequence(){
		return this.usgFieldsBluePrintSequence;
	}
	
	public void setScaleHeaderBluePrintSequence(Vector scaleHeader){
		this.scaleHeaderBluePrintSequence = scaleHeader;
	}
	
	public Vector getScaleHeaderBluePrintSequence(){
		return this.scaleHeaderBluePrintSequence;
	}
	
	public void setPricingScaleDetailsBluePrintSequence(Vector pricingScaleDetails){
		
		//if scale base type is B, then scale currency should be shown otherwise scale unit
		if (scaleBaseTypes != null && scaleBaseTypes.equalsIgnoreCase("B"))
			pricingScaleDetails.remove("cmdd.CondShow.scaleUnit");
		else
			pricingScaleDetails.remove("cmdd.CondShow.scaleCurrency");
			
		this.pricingScaleDetailsBluePrintSequence = pricingScaleDetails;
	}
	
	public Vector getPricingScaleDetailsBluePrintSequence(){
		return this.pricingScaleDetailsBluePrintSequence;
	}
	
	
	public boolean getScaleExist(){
		return this.scaleExist;
	}
	
	public boolean isConditionRecordInConsistent(){
		//return true if condition record is inconsistent
		return this.inconsistentConditionRecordStatus;
	}
	
	public void setConditionRecordConsistentStatus(boolean inconsistentConditionRecordStatus){
		this.inconsistentConditionRecordStatus = inconsistentConditionRecordStatus;
	}	
	
	public Vector getInconsistencyDetails(){
		//String [] details = new String[0];
		//details = (String [])inconsistencyDetails.toArray(details);		
		//return details;
		return inconsistencyDetails;
		
	}
	
	public void setInconsistencyDetails(Vector details){
		this.inconsistencyDetails.add(details);
	}
	
	public Vector getScaleDetails(){
		Vector scaleDetails = new Vector();
		if (usage.equalsIgnoreCase(PRICING_USAGE))
			scaleDetails = getPricingScaleDetails();
		else if (usage.equalsIgnoreCase(FREEGOODS_USAGE))
			scaleDetails = getFreeGoodsScaleDetails();
			
		return scaleDetails;
	}
	
	private Vector getPricingScaleDetails(){

		/* Pricing Scale Details will be given in a vector form. First object in the vector will contain
		 * Amount. Amount will be followed by rate, currency and unit 
		 */
		
		Vector pricingScale = new Vector();
		Vector pricingScaleDetail;
		String pricingScaleFieldValue = null;
		int len = 0;
		
		if (pricingScaleStructure != null){
			len = pricingScaleStructure.length;
			for(int index = 0; index < len; index++){
				
				pricingScaleDetail = new Vector();
				pricingScaleFieldValue = pricingScaleStructure[index].getPRScaleAmounts();
				
				if (pricingScaleFieldValue != null && !(pricingScaleFieldValue.trim().equalsIgnoreCase("")) )
					pricingScaleDetail.add(pricingScaleFieldValue);
				else
					pricingScaleDetail.add(null);
									
				
				//if scale base type is B, then scale currency should be shown otherwise scale unit
				if (scaleBaseTypes != null && scaleBaseTypes.equalsIgnoreCase("B")){
					if (scaleCurrency != null && !(scaleCurrency.equals("")))
						pricingScaleDetail.add(scaleCurrency);
					else
						pricingScaleDetail.add(null);
				}else{
					if (scaleUnit != null && !(scaleUnit.equals("")))	
						pricingScaleDetail.add(scaleUnit);
					else
						pricingScaleDetail.add(null);
				}
				
				pricingScaleFieldValue = pricingScaleStructure[index].getPRScaleRates();
				
				if(pricingScaleFieldValue != null){
					pricingScaleFieldValue = pricingScaleFieldValue + " " + pricingScaleStructure[index].getPRScaleRatesCurrUnit();
				}
				
				if (pricingScaleFieldValue != null && !(pricingScaleFieldValue.trim().equalsIgnoreCase("")))
					pricingScaleDetail.add(pricingScaleFieldValue);
				else
					pricingScaleDetail.add(null);
					
				pricingScaleFieldValue = pricingScaleStructure[index].getPRScalePricingUnit();
				
				if (pricingScaleFieldValue != null && !(pricingScaleFieldValue.trim().equalsIgnoreCase("")))
					pricingScaleDetail.add(pricingScaleFieldValue);
				else
					pricingScaleDetail.add(null);

				pricingScaleFieldValue = pricingScaleStructure[index].getPRScaleProductUnit();
				
				if (pricingScaleFieldValue != null && !(pricingScaleFieldValue.trim().equalsIgnoreCase("")))
					pricingScaleDetail.add(pricingScaleFieldValue);
				else
					pricingScaleDetail.add(null);

				pricingScale.add(pricingScaleDetail);			
				
			}
		}
		
		return pricingScale;
	}
	
	private Vector getFreeGoodsScaleDetails(){

		/* Pricing Scale Details will be given in a vector form. First object in the vector will contain
		 * Free Goods Quantity and it will be followed by Free Goods Quantity Unit, Free Quantity, 
		 * Free Quantity Unit, Calculation Rule and Free Goods Type
		 */
		
		Vector fgScale = new Vector();
		Vector fgScaleDetail;
		String fgScaleFieldValue = null;
		int len = 0;
		
		if (fgScaleStructure  != null){
			len = fgScaleStructure.length;
			for(int index = 0; index < len; index++){
				
				fgScaleDetail = new Vector();
				fgScaleFieldValue = fgScaleStructure[index].getFGMinQuantities();
				
				if (fgScaleFieldValue != null && !(fgScaleFieldValue.trim().equalsIgnoreCase("")))
					fgScaleDetail.add(fgScaleFieldValue);
				else
					fgScaleDetail.add(null);
									
				fgScaleFieldValue = fgScaleStructure[index].getFGQuantityUnits();
				
				if (fgScaleFieldValue != null && !(fgScaleFieldValue.trim().equalsIgnoreCase("")))
					fgScaleDetail.add(fgScaleFieldValue);
				else
					fgScaleDetail.add(null);
					
				fgScaleFieldValue = fgQuantity[index].getFGQuantities();
				
				if (fgScaleFieldValue != null && !(fgScaleFieldValue.trim().equalsIgnoreCase("")))
					fgScaleDetail.add(fgScaleFieldValue);
				else
					fgScaleDetail.add(null);
									
					
				fgScaleFieldValue = fgQuantity[index].getFGAddQuantities();
				
				if (fgScaleFieldValue != null && !(fgScaleFieldValue.trim().equalsIgnoreCase("")))
					fgScaleDetail.add(fgScaleFieldValue);
				else
					fgScaleDetail.add(null);
					
				fgScaleFieldValue = fgQuantity[index].getFGAddQuantityUnits();
				
				if (fgScaleFieldValue != null && !(fgScaleFieldValue.trim().equalsIgnoreCase("")))
					fgScaleDetail.add(fgScaleFieldValue);
				else
					fgScaleDetail.add(null);					
					
				fgScaleFieldValue = fgProduct[index].getFGCalcTypes();
				
				if (fgScaleFieldValue != null && !(fgScaleFieldValue.trim().equalsIgnoreCase("")))
					fgScaleDetail.add(fgScaleFieldValue);
				else
					fgScaleDetail.add(null);					

				fgScaleFieldValue = fgProduct [index].getFGIndicators();
				
				if (fgScaleFieldValue != null && !(fgScaleFieldValue.trim().equalsIgnoreCase("")))
					fgScaleDetail.add(fgScaleFieldValue);
				else
					fgScaleDetail.add(null);					
				
				fgScale.add(fgScaleDetail);			
				
			}
		}
		
		return fgScale;
	}	
	
	public String getScaleType(){
		return this.scaleType;
	}	
	
	public String getUsage(){
		return this.usage;
	}
	
	public void setUsage(String usage){
		this.usage = usage;
	}
	
	public Vector getConditionRecord(){
		
		/* Condition Record will be given in a vector form. First object in the vector will contain
		 * condition type. Condition type will be followed by application field values, usage field 
		 * values, valid from,valid to, status, scale indicator. 
		 */
		
		Vector conditionRecord = new Vector();
		String conditionRecordFieldValue = null;
		String fieldName = null;
		int len = 0;
		boolean matchNotFound = true;
		
		Iterator appFieldsBluePrintSequenceIterator;
		Iterator usgFieldsBluePrintSequenceIterator;		
		
		appFieldsBluePrintSequenceIterator = appFieldsBluePrintSequence.iterator();
		usgFieldsBluePrintSequenceIterator = usgFieldsBluePrintSequence.iterator();		
		
		conditionRecordFieldValue = this.getConditionTypes();
		
		if (conditionRecordFieldValue != null)
			conditionRecord.add(conditionRecordFieldValue);
		else
			conditionRecord.add(null);
		
		
		//now adding application field values in the sequence of application blueprint structure

		if (applicationFields != null)
		      len = applicationFields.length;
		      
		while (appFieldsBluePrintSequenceIterator.hasNext()){
			
			fieldName = (String)appFieldsBluePrintSequenceIterator.next();
			
			if (applicationFields != null){
				matchNotFound = true;
				for(int index = 0; index < len; index++){
					if (fieldName.equalsIgnoreCase(applicationFields[index].getApplicationFieldName())){
						
						matchNotFound = false;
						//take the error status of application field value and set the same
						if (applicationFields[index].getErrorStatus())
							conditionRecord.add("true");
						else
							conditionRecord.add("false");
							
						conditionRecordFieldValue = applicationFields[index].getApplicationFieldValue();
						
						if (conditionRecordFieldValue != null && !(conditionRecordFieldValue.trim().equalsIgnoreCase("")))
							conditionRecord.add(conditionRecordFieldValue);
						else
							conditionRecord.add(null);

					}
				}
				
				if (matchNotFound)
					conditionRecord.add(null);	
				
			}else{
				conditionRecord.add(null);
			}
			
		}
		
		//now adding usage field values in the sequence of usage blueprint structure
		matchNotFound = true;
		if (usageFields != null)
	  		len = usageFields.length;
		      
		while (usgFieldsBluePrintSequenceIterator.hasNext()){
			
			fieldName = (String)usgFieldsBluePrintSequenceIterator.next();
			
			if (usageFields != null){
				matchNotFound = true;
				for(int index = 0; index < len; index++){
					if (fieldName.equalsIgnoreCase(usageFields[index].getUsageFieldName())){
						
						matchNotFound = false;
						conditionRecordFieldValue = usageFields[index].getUsageFieldValue();
						
						if (conditionRecordFieldValue != null && !(conditionRecordFieldValue.trim().equalsIgnoreCase("")))
							conditionRecord.add(conditionRecordFieldValue);
						else
							conditionRecord.add(null);						
					}
				}
				
				if (matchNotFound)
					conditionRecord.add(null);	
				
			}else{
				conditionRecord.add(null);
			}
			
		}
			
		if (validityStarts != null)
			conditionRecord.add(validityStarts);
		else
			conditionRecord.add(null);
			
		
		if (validityEnds != null)
			conditionRecord.add(validityEnds);
		else
			conditionRecord.add(null);			

		conditionRecord.trimToSize();
		return conditionRecord;
		
		
	}
	
	public void checkForInconsistency(String fieldValue, String fieldName){
		// if value is null or value string contains 'null' or value string contains 
		//blank string, it means condition record inconsistent status needs to be set to
		//true 
		Vector errorDetails = new Vector();
		
		if (fieldName.equalsIgnoreCase("cmdd.CondShow.releaseStatus") 
			|| fieldName.equalsIgnoreCase("cmdd.CondShow.dimNumber")
			|| fieldName.equalsIgnoreCase("cmdd.CondShow.scaleBaseType") ){
				if ( (fieldValue == null) || fieldValue.equalsIgnoreCase("null")){
					//return true;
					this.inconsistentConditionRecordStatus = true;
					errorDetails.add("cmdd.CondShow.valueFor");
					errorDetails.add(fieldName);
					errorDetails.add("cmdd.CondShow.cantBe");
					errorDetails.add("cmdd.CondShow.null");
					//this.setInconsistencyDetails("Value for " + fieldName + " can't be " + fieldValue);
					this.setInconsistencyDetails(errorDetails);
				}
				
		
		}else{
			if ( (fieldValue == null) || fieldValue.equalsIgnoreCase("null") || fieldValue.trim().equalsIgnoreCase("") ){
				//return true;
				this.inconsistentConditionRecordStatus = true;
				errorDetails.add("cmdd.CondShow.valueFor");
				errorDetails.add(fieldName);
				errorDetails.add("cmdd.CondShow.cantBe");
				
				if (fieldValue.trim().equalsIgnoreCase(""))
					errorDetails.add("cmdd.CondShow.blank");
				else
					errorDetails.add("cmdd.CondShow.null");
				
				//this.setInconsistencyDetails("Value for " + fieldName + " can't be " + fieldValue);
				this.setInconsistencyDetails(errorDetails);
			}
		}

	}
	
	public void checkForInconsistency(String fieldValue, String fieldName, String fieldLabel){
		// if value is null or value string contains 'null' or value string contains 
		//blank string, it means condition record inconsistent status needs to be set to
		//true 
		Vector errorDetails = new Vector();
		
		if (fieldName.equalsIgnoreCase("cmdd.CondShow.releaseStatus") 
			|| fieldName.equalsIgnoreCase("cmdd.CondShow.dimNumber")
			|| fieldName.equalsIgnoreCase("cmdd.CondShow.scaleBaseType") ){
				if ( (fieldValue == null) || fieldValue.equalsIgnoreCase("null")){
					//return true;
					this.inconsistentConditionRecordStatus = true;
					errorDetails.add("cmdd.CondShow.valueFor");
					errorDetails.add(fieldName);
					errorDetails.add(fieldLabel);
					errorDetails.add("cmdd.CondShow.cantBe");
					errorDetails.add("cmdd.CondShow.null");
					//this.setInconsistencyDetails("Value for " + fieldName + " can't be " + fieldValue);
					this.setInconsistencyDetails(errorDetails);
				}
				
		
		}else{
			if ( (fieldValue == null) || fieldValue.equalsIgnoreCase("null") || fieldValue.trim().equalsIgnoreCase("") ){
				//return true;
				this.inconsistentConditionRecordStatus = true;
				errorDetails.add("cmdd.CondShow.valueFor");
				errorDetails.add(fieldName);
				errorDetails.add(fieldLabel);
				errorDetails.add("cmdd.CondShow.cantBe");
				
				if (fieldValue.trim().equalsIgnoreCase(""))
					errorDetails.add("cmdd.CondShow.blank");
				else
					errorDetails.add("cmdd.CondShow.null");
				
				//this.setInconsistencyDetails("Value for " + fieldName + " can't be " + fieldValue);
				this.setInconsistencyDetails(errorDetails);
			}
		}

	}
	
	
}
