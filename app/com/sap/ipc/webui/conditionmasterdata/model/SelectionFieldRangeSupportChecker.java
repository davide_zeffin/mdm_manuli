/*******************************************************************************

    Copyright (c) 2005 by SAP AG

    All rights to both implementation and design are reserved.

    Use and copying of this software and preparation of derivative works based
    upon this software are not permitted.

    Distribution of this software is restricted. SAP does not make any warranty
    about the software, its performance or its conformity to any specification.

*******************************************************************************/
package com.sap.ipc.webui.conditionmasterdata.model;

import java.util.Hashtable;

/**
 * @author d043319
 */
public class SelectionFieldRangeSupportChecker {

	private static Hashtable RANGE_SUPPORTING_FIELDS_LIST = null;
	
	// List of internal field names which supports range selection
	// If the list needs to extended, do it here  
	private static final String[] RANGE_SUPPORTING_FIELD_NAMES = { "PRODUCT", 
                                                                   "DIVISION",
		                                                           "SALES_ORG", 
                                                                //   "DIS_CHANNEL", 
                                                                   "SOLD_TO_PARTY" };

	/*
	 *  Create a Hashtable which contains the list of internal field names which will be
	 *  used to support range selection 
	 */
	private static void initRangeSupportingfields() {
		// init the list of range supporting fields only once
		if (RANGE_SUPPORTING_FIELDS_LIST == null) {
			RANGE_SUPPORTING_FIELDS_LIST = new Hashtable(RANGE_SUPPORTING_FIELD_NAMES.length);
			// loop for all entries in the array and transfer them into the hast table
			for (int loop = 0; loop < RANGE_SUPPORTING_FIELD_NAMES.length; loop++) {
				RANGE_SUPPORTING_FIELDS_LIST.put(RANGE_SUPPORTING_FIELD_NAMES[loop], new Boolean(true));
			}
		}
	}

	/*
	 *  Check if a certain field is supporting range selection ( from / to field )
	 *  If the field does support, true will be returned, otherwise false
	 *  User the internal field name for the check !!!
	 */
	public static boolean isFieldSupportingRangeSelection(String argIntFieldName) {
		boolean retVal = false;

		// check if we need to init the hashtable of field names
		if (RANGE_SUPPORTING_FIELDS_LIST == null) {
			initRangeSupportingfields();
		}

		// check if the given field name is included in the list
		if (argIntFieldName != null) {
			if (RANGE_SUPPORTING_FIELDS_LIST.get(argIntFieldName) != null) {
				retVal = true;
			}
		}
		return retVal;

	}

}
