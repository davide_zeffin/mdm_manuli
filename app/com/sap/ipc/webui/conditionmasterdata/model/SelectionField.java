/*******************************************************************************

    Copyright (c) 2005 by SAP AG

    All rights to both implementation and design are reserved.

    Use and copying of this software and preparation of derivative works based
    upon this software are not permitted.

    Distribution of this software is restricted. SAP does not make any warranty
    about the software, its performance or its conformity to any specification.

*******************************************************************************/
package com.sap.ipc.webui.conditionmasterdata.model;

/**
 * @author d043319
 */
public class SelectionField {

public static String fieldTypeDate   = "DATE";
public static String fieldTypeString = "STRING";
public static String fieldTypeInt    = "INT"; 

public static String FROM_FIELD_EXT = "_FROM";
public static String TO_FIELD_EXT = "_TO";

private String extFieldName;
private String fieldName;
private String fieldType;
private String fieldValueFrom;
private String fieldValueTo; 
private String fieldLabel;
private String dataElementName;
private boolean isFromToField = false;
private boolean isVarKeyField = false;
private boolean convertToUpperCase = false;


/**
 * @return
 */
public static String getFieldTypeString() {
	return fieldTypeString;
}

/**
 * @return
 */
public String getFieldLabel() {
	// do not show empty field labels. use the technical name instead
	if (fieldLabel != null) {
		return fieldLabel;
	}
	else {
		return fieldName;
	}
}

/**
 * @return
 */
public String getFieldName() {
	String retVal;
	if ( isFromToField ) {
		retVal = fieldName + FROM_FIELD_EXT; }
    else {
    	retVal = fieldName;
    }
	return retVal;
}

/**
 * @return
 */
public String getFieldNameTo() {
	String retVal;
	if ( isFromToField ) {
		retVal = fieldName + TO_FIELD_EXT; }
	else {
		retVal = "";
	} 
	return retVal;
}

/**
 * @return Fieldname, independent if this is a From/To field
 *         this method returns the fieldname without any suffix
 */
public String getRealFieldName() {
	return fieldName;
}

/**
 * @return
 */
public String getFieldType() {
	return fieldType;
}

/**
 * @return
 */
public String getFieldValueFrom() {
	if ( convertToUpperCase &&
	     fieldValueFrom != null){
	     	return fieldValueFrom.toUpperCase();
	     }
	return fieldValueFrom;
}

/**
 * @param string
 */
public static void setFieldTypeString(String string) {
	fieldTypeString = string;
}

/**
 * @param string
 */
public void setFieldLabel(String string) {
	fieldLabel = string;
}

/**
 * @param string
 */
public void setFieldName(String string) {
	fieldName = string;
}

/**
 * @param string
 */
public void setFieldType(String string) {
	fieldType = string;
}

/**
 * @param string
 */
public void setFieldValueFrom(String string) {
	fieldValueFrom = string;
}

/**
 * @return
 */
public String getFieldValueTo() {
	if (convertToUpperCase &&
	    fieldValueTo != null ){
	    	return fieldValueTo.toUpperCase();
	    }
	return fieldValueTo;
}

/**
 * @param string
 */
public void setFieldValueTo(String string) {
	fieldValueTo = string;
}

/**
 * @return
 */
public String getFieldValue() {
	if ( convertToUpperCase && 
	     fieldValueFrom != null) {
	     	return fieldValueFrom.toUpperCase();	
	     }
	return fieldValueFrom;
}

/**
 * @param string
 */
public void setFieldValue(String string) {
	fieldValueFrom = string;
}

/**
 * @return
 */
public boolean isFromToField() {
	return isFromToField;
}

/**
 * @param b
 */
public void setFromToField(boolean b) {
	isFromToField = b;
}

/**
 * @return
 */
public boolean isVarKeyField() {
	return isVarKeyField;
}

/**
 * @param b
 */
public void setVarKeyField(boolean b) {
	isVarKeyField = b;
}

/**
 * @return
 */
public String getDataElementName() {
	return dataElementName;
}

/**
 * @param string
 */
public void setDataElementName(String string) {
	dataElementName = string;
}

/**
 * @return
 */
public String getExtFieldName() {
	return extFieldName;
}

/**
 * @param string
 */
public void setExtFieldName(String string) {
	extFieldName = string;
}

/**
 * @return
 */
public boolean isConvertToUpperCase() {
	return convertToUpperCase;
}

/**
 * @param b
 */
public void setConvertToUpperCase(boolean b) {
	convertToUpperCase = b;
}

}
