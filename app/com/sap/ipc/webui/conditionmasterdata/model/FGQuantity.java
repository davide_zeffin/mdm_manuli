/*
 * Created on Jul 13, 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.ipc.webui.conditionmasterdata.model;

/**
 * @author I026516
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class FGQuantity {
	
	private String FGQuantities;
	private String FGAddQuantities;
	private String FGAddQuantityUnits;
	
	FGQuantity() {
	}
	
	public String getFGQuantities(){
		return this.FGQuantities;
	}
	
	public void setFGQuantities(String FGQuantities){
		this.FGQuantities = FGQuantities;
	}
	
	public String getFGAddQuantities(){
		return this.FGAddQuantities;
	}
	
	public void setFGAddQuantities(String FGAddQuantities){
		this.FGAddQuantities = FGAddQuantities;
	}

	public String getFGAddQuantityUnits(){
		return this.FGAddQuantityUnits;
	}
	
	public void setFGAddQuantityUnits(String FGAddQuantityUnits){
		this.FGAddQuantityUnits = FGAddQuantityUnits;
	}

}
