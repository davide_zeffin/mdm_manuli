/*
 * Created on Jul 13, 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.ipc.webui.conditionmasterdata.model;

/**
 * @author I026516
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ApplicationFields {

	private String applicationFieldName = null;
	private String applicationFieldValue = null;
	private boolean errorStatus;
	
	public ApplicationFields(){
	}
	
	public String getApplicationFieldName(){
		return this.applicationFieldName;
	}
	
	public void setApplicationFieldName(String applicationFieldName){
		this.applicationFieldName = applicationFieldName;
	}
	
	public String getApplicationFieldValue(){
		return this.applicationFieldValue;
	}
	
	public void setApplicationFieldValue(String applicationFieldValue){
		this.applicationFieldValue = applicationFieldValue;
	}

	public boolean getErrorStatus(){
		return this.errorStatus;
	}
	
	public void setErrorStatus(boolean errorStatus){
		this.errorStatus = errorStatus;
	}
}
