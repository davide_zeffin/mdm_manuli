/************************************************************************

	Copyright (c) 2001 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
/**
 * @author I019099
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */

package com.sap.ipc.webui.conditionmasterdata.model;

import java.io.IOException;
import org.apache.struts.util.MessageResources;
import com.sap.ipc.webui.pricing.UIConstants;
import com.sap.spc.remote.client.object.*;

import com.sap.spc.remote.client.tcp.ServerResponse;
import com.sap.spc.remote.shared.command.ConvertExternalToInternal;

import java.util.Hashtable;
import java.util.List;
import java.util.HashMap;


public class ProcessSelectionConversion{ 
  private SelectionField[] _selFld;
  private IPCSession _IPCSess;
  private ConditionMastDataDisplayModel _cMDDM;
  public  HashMap conRecExt;
  private static String FN_VALID_FROM = "TIMESTAMP_FROM";		
  private static String FN_VALID_TO   = "TIMESTAMP_TO";
  
 //constructor method.  
  
   public ProcessSelectionConversion( ConditionMastDataDisplayModel cMDDM ,SelectionField[] SelFld ) 
{    _selFld = SelFld; 
	_cMDDM = cMDDM;
     _IPCSess = cMDDM.getIPCSession();
}


// convert to int now
  public void convertToInt(){
	  int noEleCount = 0;
	String Value = new String();
	String valid_from = new String();
	String valid_to = new String();
	String [] names = null;
	String[] externalValue = null ;
	IPCClient m_client = null ;
	  
 if  ( _selFld != null ) {
	 int lastEleCount = _selFld.length;

	for (int i= 0; i<lastEleCount; i++ ){
		if( _selFld[i].isFromToField() == true ){
		   Value = _selFld[i].getFieldValueFrom();
		   if ( !( Value.equals("")|| ( (_selFld[i].getFieldName()).equals(FN_VALID_TO) ) ||
				   ((_selFld[i].getFieldName()).equals(FN_VALID_FROM) ))) {
						   noEleCount++;
				   }
		   Value =	 _selFld[i].getFieldValueTo();		   	   		   
		   if ( !( Value.equals("")|| ( (_selFld[i].getFieldName()).equals(FN_VALID_TO) ) ||
							  ((_selFld[i].getFieldName()).equals(FN_VALID_FROM) ))) {
									  noEleCount++;
							  }
		   
		}
		 else
		{
			Value = _selFld[i].getFieldValue();
			    
			if ( !( Value.equals("")|| ( (_selFld[i].getFieldName()).equals(FN_VALID_TO) ) ||
		((_selFld[i].getFieldName()).equals(FN_VALID_FROM) ))) {
				noEleCount++;
			}
	}
	
	}
				
	 
 if ( noEleCount != 0 ) { 	  
 
       externalValue = new String[noEleCount];
	   names = new String[noEleCount];
       m_client = _IPCSess.getIPCClient();
	
   // iterate over the array and create array which is required to be passed
   // on to the cammand call which does the conversion to internal representation.
    noEleCount = 0;
    for (int i= 0; i<lastEleCount; i++ ){
    	
		if ( !_selFld[i].getFieldValueFrom().equals("") ) {
		 	
		    
		      if ((_selFld[i].getFieldName()).equals(FN_VALID_FROM)){
		      	valid_from = _selFld[i].getFieldValue();
		      	}else if ((_selFld[i].getFieldName()).equals(FN_VALID_TO)){
				valid_to = _selFld[i].getFieldValue();
		       	} else {			
		       
					if ( _selFld[i].isFromToField() == true ){		
		       		
						names[noEleCount] = _selFld[i].getRealFieldName(); 
						externalValue[noEleCount] = _selFld[i].getFieldValueFrom();
						noEleCount++;
						if (!((_selFld[i].getFieldValueTo()).equals("")|| (_selFld[i].getFieldValueTo() == null))){
							names[noEleCount] = _selFld[i].getRealFieldName(); 
							externalValue[noEleCount] = _selFld[i].getFieldValueTo();
							noEleCount++;	
						}
		    	   	} else {			
       	 	  				names[noEleCount] = _selFld[i].getRealFieldName();
		    			    externalValue[noEleCount] = _selFld[i].getFieldValue();
							noEleCount++;
			       			}
		 			
   				 } 	 
   		 }	
    }
 }

 	
  	// process the valid to and valid from now ( it is assumed that valid_to and valid from appears as 
  	//'YYYY.MM.DD' 
  	  if  (!( valid_to.equals("") || valid_to == null )){ 
		String valid_to_YYYYMMDD = getYear(valid_to).concat(getMonth(valid_to).concat(getDay(valid_to).concat("235959")));
		_cMDDM.setValidTo(valid_to_YYYYMMDD);
  	  }
  	
	if  (!( valid_from.equals("") || valid_from == null )){
	 String valid_from_YYYYMMDD = getYear(valid_from).concat(getMonth(valid_from).concat(getDay(valid_from).concat("000000")));
	 _cMDDM.setValidFrom(valid_from_YYYYMMDD);
	}
	
}	
	if ((_cMDDM.getParamNames()) == null) {
	
	 if ( m_client != null && names != null && externalValue != null ) 
	 {
    HashMap Output = m_client.convertExternalToInternal(names, externalValue); 		
	
	String[] names_output = (String[])Output.get(ConvertExternalToInternal.NAMES_OUTPUT);
	String[] internalValues = (String[])Output.get(ConvertExternalToInternal.INTERNAL_VALUES);
	String[] error_msg = (String[])Output.get(ConvertExternalToInternal.ERROR_MESSAGES);
	
	if (error_msg == null) { 
	 
	//Now its time to call up the cammand to get he condition records.
	GetConditionRecords conRecset =  new GetConditionRecords(_cMDDM, names_output, internalValues);
	conRecExt = conRecset.GetConditionRecordsExt();
	_cMDDM.setErrorMessages(error_msg);
	 } else {
	 	
	 // give out message now. that there was error during the conversion from external to internal.
	 	_cMDDM.setErrorMessages(error_msg);
	}
	 }
	} 
		
	 else {
		GetConditionRecords conRecset =  new GetConditionRecords(_cMDDM, _cMDDM.getParamNames(), _cMDDM.getParamValues()); 	
		conRecExt = conRecset.GetConditionRecordsExt();
	}
	
 	
	  
	   _cMDDM.setConditionRecExt(conRecExt);
 
 
  }// end of convertToInt()
  
  




private String getYear(String date){
	 String year = date.substring(0,4);
	return year;
	
}

private String getMonth(String date){
	String month = date.substring(5, 7);
	return month;
}

private String getDay(String date){
	String day = date.substring(8,10);
	return day;
}

}
