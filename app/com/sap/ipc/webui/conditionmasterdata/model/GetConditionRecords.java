/*******************************************************************************

    Copyright (c) 2005 by SAP AG

    All rights to both implementation and design are reserved.

    Use and copying of this software and preparation of derivative works based
    upon this software are not permitted.

    Distribution of this software is restricted. SAP does not make any warranty
    about the software, its performance or its conformity to any specification.

*******************************************************************************/
package com.sap.ipc.webui.conditionmasterdata.model;

import java.util.Enumeration;
import java.util.Vector;
import java.util.Hashtable;
import java.util.List;
import java.util.HashMap;

import com.sap.spc.remote.client.object.*;
import com.sap.spc.remote.client.object.imp.tcp.TcpDefaultIPCSession;
import com.sap.spc.remote.shared.command.ConvertInternalToExternal;
import com.sap.spc.remote.shared.command.GetConditionRecordsFromDatabase;
import com.sap.spc.remote.client.tcp.PricingConverter;
import com.sap.spc.remote.shared.command.GetConditionRecordsFromRefGuid;
/**
 * @author I019099
 */
public class  GetConditionRecords {

private String _application;
private String _usage;
private String _conditionGroupName;
private String[] _selFldIntInclAttr;
private String[] _selFldIntIncValues;
private String[] _selFldIntExclAttr;
private String[] _selFldIntExclValues;
private String _validToDate = new String("99991231000000");
private String _validFromDate = new String("00010101000000"); 
private IPCSession _IPCSess;
private HashMap _recordsetExt;
private ConditionMastDataDisplayModel _cMDDM;
 

public GetConditionRecords(){
}
// constructor
GetConditionRecords( IPCSession IPCSes, String usage, String application, 
									String[] selAttributesInclArray, String[] selAttributesInclValueArray,
									String[] selAttributesExclArray, String[] selAttributesExclValueArray,
									String validityDateStart_yyyyMMddmmhhss, String validityDateEnd_yyyyMMddmmhhss,
									String conditionGroupId,
									ConditionMastDataDisplayModel cMDDM)
{ 
	 HashMap RecordsetInt;
      _IPCSess = IPCSes;
	 _application = application;
	 _usage = usage;
	 _conditionGroupName = conditionGroupId;
	 _selFldIntInclAttr = selAttributesInclArray;
	 _selFldIntIncValues = selAttributesInclValueArray;
	 _selFldIntExclAttr = selAttributesExclArray;
	 _selFldIntExclValues = selAttributesExclValueArray;
	 _cMDDM = cMDDM;
	  
	  String numOfRecs = _cMDDM.getMaxNumberOfHits();
	  if (numOfRecs == null ) {
	  	// if there is no value for the max. hit number, use the default
	  	numOfRecs = InitialiseConditionSelectionPannel.MaxSelectionFieldValue;
	  }
	 
	 try {
     if ( (validityDateStart_yyyyMMddmmhhss != null) && 
          (!validityDateStart_yyyyMMddmmhhss.equals(""))){
	     _validFromDate = validityDateStart_yyyyMMddmmhhss;
	       }
	    if ( (validityDateEnd_yyyyMMddmmhhss != null) && 
	         (!validityDateEnd_yyyyMMddmmhhss.equals(""))) {
	       _validToDate = validityDateEnd_yyyyMMddmmhhss; 
	       }
	  } catch ( Exception e){
	  	 e.printStackTrace();    
	  }
	
	
		IPCClient m_client = _IPCSess.getIPCClient();	 
		TcpDefaultIPCSession tcpIPCSession = (TcpDefaultIPCSession)_IPCSess;
	
	
	if (_cMDDM.getRefGuid() == null ) {

	 RecordsetInt = m_client.getConditionRecordsFromDatabase(_usage, _application, _selFldIntInclAttr, _selFldIntIncValues, 
	_selFldIntExclAttr, _selFldIntExclValues, _validFromDate,  _validToDate,  _conditionGroupName , numOfRecs);
	
	String numberOfRecordReturned = (String)RecordsetInt.get(GetConditionRecordsFromDatabase.NUMBER_OF_RECORD_RETURNED);
	
	int numOfRecSel = m_client.getNumberOfConditionRecordSelected(_usage);
	_cMDDM.setNumOfRecSelected(numOfRecSel);
	
//	if ( (int) (RecordsetInt.get(GetConditionRecordsFromDatabase.NUMBER_OF_RECORD_RETURNED)) != 0){
	if (!(numberOfRecordReturned == null || numberOfRecordReturned.equals("0"))){														
	String[] appFldNames  = (String[])RecordsetInt.get(GetConditionRecordsFromDatabase.APPLICATION_FIELD_NAMES );	
	String[] appFldNumber = (String[])RecordsetInt.get(GetConditionRecordsFromDatabase.APPLICATION_FIELD_NUMBERS);												
	String[] appFldOffSets = (String[])RecordsetInt.get(GetConditionRecordsFromDatabase.APPLICATION_FIELD_OFFSETS);	
	String[] appFldValues = (String[])RecordsetInt.get(GetConditionRecordsFromDatabase.APPLICATION_FIELD_VALUES);
	String[] usageFldNames =(String[])RecordsetInt.get(GetConditionRecordsFromDatabase.USAGE_FIELD_NAMES );	
	String[] usageFldNumber = (String[])RecordsetInt.get(GetConditionRecordsFromDatabase.USAGE_FIELD_NUMBERS);												
	String[] usageFldOffSets = (String[])RecordsetInt.get(GetConditionRecordsFromDatabase.USAGE_FIELD_OFFSETS);	
	String[] usageFldValues = (String[])RecordsetInt.get(GetConditionRecordsFromDatabase.USAGE_FIELD_VALUES);
	String[] validityEnd = (String[])RecordsetInt.get(GetConditionRecordsFromDatabase.VALIDITY_ENDS);
	String[] validityStarts = (String[])RecordsetInt.get(GetConditionRecordsFromDatabase.VALIDITY_STARTS);
	
	
	
	
	
	// Get the vakey values converted now to external representation 
	HashMap Output = m_client.convertInternalToExternal(appFldNames, appFldValues);
	
	String[] appFldValuesExt = (String[])Output.get(ConvertInternalToExternal.EXTERNAL_VALUES);
	_cMDDM.setExternalConversionStatus((String[])Output.get(ConvertInternalToExternal.ERROR_MESSAGES));
	
	// convert the validity periods to be converted into dd/mm/yyyy for format
	
	validityEnd = convertDateToExt(validityEnd, null, "DDMMYYYY");
	validityStarts = convertDateToExt(validityStarts, null, "DDMMYYYY");
	
	// now convert the kbetr into the representation that look good on the ui.
	
	UsageExternalConversion uEC = new UsageExternalConversion(_usage,tcpIPCSession,usageFldNames,usageFldValues);
	usageFldValues = uEC.getConvertedUsageFldValues();
	// now reset the converted things into the record set.
	
	
	RecordsetInt.remove(GetConditionRecordsFromDatabase.APPLICATION_FIELD_VALUES);
	RecordsetInt.put(GetConditionRecordsFromDatabase.APPLICATION_FIELD_VALUES, appFldValuesExt);
	RecordsetInt.remove(GetConditionRecordsFromDatabase.USAGE_FIELD_VALUES);
	RecordsetInt.put(GetConditionRecordsFromDatabase.USAGE_FIELD_VALUES, usageFldValues);
	RecordsetInt.remove(GetConditionRecordsFromDatabase.VALIDITY_ENDS);
	RecordsetInt.put(GetConditionRecordsFromDatabase.VALIDITY_ENDS,validityEnd);
	RecordsetInt.remove(GetConditionRecordsFromDatabase.VALIDITY_STARTS);
	RecordsetInt.put(GetConditionRecordsFromDatabase.VALIDITY_STARTS,validityStarts);
	
	
	_recordsetExt = RecordsetInt;
	
		}
	
	} else { // Guid selection
		RecordsetInt = m_client.getConditionRecordsFromRefGuid(_usage,_application,_cMDDM.getRefGuid(),_cMDDM.getRefTyp(),numOfRecs); 
	String numberOfRecordReturned = (String)RecordsetInt.get(GetConditionRecordsFromRefGuid.NUMBER_OF_RECORD_RETURNED);
	
 	if (!(numberOfRecordReturned == null || numberOfRecordReturned.equals("0"))){
															
		String[] appFldNames  = (String[])RecordsetInt.get(GetConditionRecordsFromRefGuid.APPLICATION_FIELD_NAMES );	
		String[] appFldNumber = (String[])RecordsetInt.get(GetConditionRecordsFromRefGuid.APPLICATION_FIELD_NUMBERS);												
		String[] appFldOffSets = (String[])RecordsetInt.get(GetConditionRecordsFromRefGuid.APPLICATION_FIELD_OFFSETS);	
		String[] appFldValues = (String[])RecordsetInt.get(GetConditionRecordsFromRefGuid.APPLICATION_FIELD_VALUES);
		String[] usageFldNames =(String[])RecordsetInt.get(GetConditionRecordsFromRefGuid.USAGE_FIELD_NAMES );	
		String[] usageFldNumber = (String[])RecordsetInt.get(GetConditionRecordsFromRefGuid.USAGE_FIELD_NUMBERS);												
		String[] usageFldOffSets = (String[])RecordsetInt.get(GetConditionRecordsFromRefGuid.USAGE_FIELD_OFFSETS);	
		String[] usageFldValues = (String[])RecordsetInt.get(GetConditionRecordsFromRefGuid.USAGE_FIELD_VALUES);
		String[] validityEnd = (String[])RecordsetInt.get(GetConditionRecordsFromRefGuid.VALIDITY_ENDS);
		String[] validityStarts = (String[])RecordsetInt.get(GetConditionRecordsFromRefGuid.VALIDITY_STARTS);
		
//		Get the vakey values converted now to external representation 
		 HashMap Output = m_client.convertInternalToExternal(appFldNames, appFldValues);
	
		 String[] appFldValuesExt = (String[])Output.get(ConvertInternalToExternal.EXTERNAL_VALUES);
	
		 // convert the validity periods to be converted into dd/mm/yyyy for format
	
		 validityEnd = convertDateToExt(validityEnd, null, "DDMMYYYY");
		 validityStarts = convertDateToExt(validityStarts, null, "DDMMYYYY");
	
		 // now convert the kbetr into the representation that look good on the ui.
	
		 UsageExternalConversion uEC = new UsageExternalConversion(_usage,tcpIPCSession,usageFldNames,usageFldValues);
		 usageFldValues = uEC.getConvertedUsageFldValues();
		 // now reset the converted things into the record set.
	
	
		 RecordsetInt.remove(GetConditionRecordsFromDatabase.APPLICATION_FIELD_VALUES);
		 RecordsetInt.put(GetConditionRecordsFromDatabase.APPLICATION_FIELD_VALUES, appFldValuesExt);
		 RecordsetInt.remove(GetConditionRecordsFromDatabase.USAGE_FIELD_VALUES);
		 RecordsetInt.put(GetConditionRecordsFromDatabase.USAGE_FIELD_VALUES, usageFldValues);
		 RecordsetInt.remove(GetConditionRecordsFromDatabase.VALIDITY_ENDS);
		 RecordsetInt.put(GetConditionRecordsFromDatabase.VALIDITY_ENDS,validityEnd);
		 RecordsetInt.remove(GetConditionRecordsFromDatabase.VALIDITY_STARTS);
		 RecordsetInt.put(GetConditionRecordsFromDatabase.VALIDITY_STARTS,validityStarts);
	
	
		 _recordsetExt = RecordsetInt;
	
		}	
		
	} 
	
													
}

// Constructor
GetConditionRecords(ConditionMastDataDisplayModel cMDDM,  String[] selFldIntInclAttr, String[] selFldIntInclValues )
{
	
	
	// call the above constructor	 
	//this._IPCSess = IPCSes;
	//_cMDDM = cMDDM;
	
	this(cMDDM.getIPCSession(),
			cMDDM.getUsage(),
			cMDDM.getApplication(),
			selFldIntInclAttr,
			selFldIntInclValues,
			null,
			null,
			cMDDM.getValidFrom(),
			cMDDM.getValidTo(),
			cMDDM.getConditionGroupName(),
			cMDDM);
			
			//_cMDDM = cMDDM;
	
	

}



public HashMap GetConditionRecordsExt(){
	return _recordsetExt;
}

private String[] convertDateToExt( String [] array, String separator, String format  ){
// assumption is that always the sending format is yyyymmddhhmmss	
	 String date;
	 if (separator == null ){ separator ="/"; }
	 if (format == null) {format="YYYYMMDD"; }
	  
	 for (int i=0; i<array.length; i++){
	   if (format.equals("YYYYMMDD")){
	   	  		   
		array[i]= getYear(array[i]).concat(separator.concat(getMonth(array[i]).concat(separator.concat(getDay(array[i])))));
				
		
	   } else if (format.equals("MMDDYYYY")){
	   	
		array[i]= getMonth(array[i]).concat(separator.concat(getDay(array[i]).concat(separator.concat(getYear(array[i])))));
	   	
	   } else if (format.equals("DDMMYYYY")){
	   	
		array[i]= getDay(array[i]).concat(separator.concat(getMonth(array[i]).concat(separator.concat(getYear(array[i])))));
	   	
	   	
	   }
	 }
	 return array;
}


private String getYear(String date){
	 String year = date.substring(0,4);
	return year;
	
}

private String getMonth(String date){
	String month = date.substring(4, 6);
	return month;
}

private String getDay(String date){
	String day = date.substring(6,8);
	return day;
}


	
	


}