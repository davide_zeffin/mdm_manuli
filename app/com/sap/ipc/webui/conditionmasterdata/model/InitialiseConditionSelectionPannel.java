/*******************************************************************************

    Copyright (c) 2005 by SAP AG

    All rights to both implementation and design are reserved.

    Use and copying of this software and preparation of derivative works based
    upon this software are not permitted.

    Distribution of this software is restricted. SAP does not make any warranty
    about the software, its performance or its conformity to any specification.

*******************************************************************************/
package com.sap.ipc.webui.conditionmasterdata.model;

import java.util.Enumeration;
import java.util.Vector;

/**
 * @author d043319
 */
public class InitialiseConditionSelectionPannel implements Enumeration {

	public static final String SessionAttributeName = "InitialiseConditionSelectionPannel";
	public static final String FormButtonCancel = "org.apache.struts.taglib.html.CANCEL";
	public static final String FormButtonClearQuery = "CLEAR_QUERY";	 
	public static final String MaxSelectionFieldName = "MAX_COND_SEL_RECORDS";
	public static final String MaxSelectionFieldValue = "100";

	private Vector selectionFields;
	private Enumeration selFieldEnum;
	private SelectionField maxRecordsToSelect;

	public InitialiseConditionSelectionPannel() {
		selectionFields = new Vector(10);
		selFieldEnum = selectionFields.elements();
	}

	public void addField(SelectionField argSelectionField) {
		if (argSelectionField != null) {
			selectionFields.add(argSelectionField);
		}
	}

	public void resetEnumeration() {
		selFieldEnum = selectionFields.elements();
	}

	/* (non-Javadoc)
	 * @see java.util.Enumeration#hasMoreElements()
	 */
	public boolean hasMoreElements() {
		return selFieldEnum.hasMoreElements();
	}

	/* (non-Javadoc)
	 * @see java.util.Enumeration#nextElement()
	 */
	public Object nextElement() {
		return selFieldEnum.nextElement();
	}


	/**
	 * @return
	 */
	public SelectionField getMaxRecordsToSelect() {
		return maxRecordsToSelect;
	}

	/**
	 * @param field
	 */
	public void setMaxRecordsToSelect(SelectionField field) {
		maxRecordsToSelect = field;
	}

}
