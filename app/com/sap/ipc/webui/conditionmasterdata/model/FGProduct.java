/*
 * Created on Jul 13, 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.ipc.webui.conditionmasterdata.model;

/**
 * @author I026516
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class FGProduct {
	
	private String FGAddProducts;
	private String FGCalcTypes;
	private String FGIndicators;
	
	FGProduct() {
	}
	
	public String getFGAddProducts(){
		return this.FGAddProducts;
	}
	
	public void setFGAddProducts(String FGAddProducts){
		this.FGAddProducts = FGAddProducts;
	}
	
	public String getFGCalcTypes(){
		return this.FGCalcTypes;
	}
	
	public void setFGCalcTypes(String FGCalcTypes){
		this.FGCalcTypes = FGCalcTypes;
	}

	public String getFGIndicators(){
		return this.FGIndicators;
	}
	
	public void setFGIndicators(String FGIndicators){
		this.FGIndicators = FGIndicators;
	}

}
