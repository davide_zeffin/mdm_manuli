/*
 * Created on Jul 13, 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.ipc.webui.conditionmasterdata.model;

/**
 * @author I026516
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class PricingScaleStructure {
	
	private String PRScaleLineRefs;
	private String PRScaleAmounts;
	private String PRScaleRates;
	private String PRScaleRatesCurrUnit;
	private String PRScalePricingUnit;
	private String PRScaleProductUnit;

	PricingScaleStructure() {
	}
	
	public String getPRScaleLineRefs(){
		return this.PRScaleLineRefs;
	}
	
	public void setPRScaleLineRefs(String PRScaleLineRefs){
		this.PRScaleLineRefs = PRScaleLineRefs;
	}
	
	public String getPRScaleAmounts(){
		return this.PRScaleAmounts;
	}
	
	public void setPRScaleAmounts(String PRScaleAmounts){
		this.PRScaleAmounts = PRScaleAmounts;
	}
	
	public String getPRScaleRates(){
		return this.PRScaleRates;
	}
	
	public void setPRScaleRates(String PRScaleRates){
		this.PRScaleRates = PRScaleRates;
	}

	public String getPRScaleRatesCurrUnit(){
		return this.PRScaleRatesCurrUnit;
	}
	
	public void setPRScaleRatesCurrUnit(String PRScaleRatesCurrUnit){
		this.PRScaleRatesCurrUnit = PRScaleRatesCurrUnit;
	}
	
	public String getPRScalePricingUnit(){
		return this.PRScalePricingUnit;
	}
	
	public void setPRScalePricingUnit(String PRScalePricingUnit){
		this.PRScalePricingUnit = PRScalePricingUnit;
	}
	
	public String getPRScaleProductUnit(){
		return this.PRScaleProductUnit;
	}
	
	public void setPRScaleProductUnit(String PRScaleProductUnit){
		this.PRScaleProductUnit = PRScaleProductUnit;
	}
	

}
