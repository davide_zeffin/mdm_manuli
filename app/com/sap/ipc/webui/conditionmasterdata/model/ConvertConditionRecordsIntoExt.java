/************************************************************************

	Copyright (c) 2001 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.ipc.webui.conditionmasterdata.model;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Vector;
import java.util.Hashtable;

import com.sap.spc.remote.client.object.IPCClient;

public class ConvertConditionRecordsIntoExt implements com.sap.spc.remote.shared.command.ConditionSelectionCommand {

	private String numberOfRecordReturned;
	private String [] conditionRecordIds = null;
	private String [] validityEnds = null;

	private String [] validityStarts = null;
	private String [] objectIds = null;
	private String [] releaseStatus = null;

	private String [] conditionTypes = null;
	private String [] supplementaryConditionTypes = null;
	private String [] groupIds = null;

	private String [] maintenanceStatus = null;
	private String [] supplementaryConditionIds = null;
	private String [] dimensionNumbers = null;

	private String [] applicationFieldNumbers = null;
	private String [] applicationOffsets = null;

	private String [] usageFieldNumbers = null;
	private String [] usageFieldOffsets = null;

	private String [] scaleBaseTypes = null;
	private String [] scaleLineNumbers = null;
	private String [] scaleLineOffsets = null;
	
	private String [] applicationFieldNames = new String[1];
	private String [] applicationFieldValues = null;
	private String [] errorStatusApplicationFields = null;
	
	private String [] usageFieldNames = null;
	private String [] usageFieldValues = null;
	//private String [] errorStatusUsageFields = null;	
	
	private String [] prScaleLineRefs = null;
	private String [] prScaleAmounts = null;
	private String [] prScaleRates = null;
	
	private String [] prScaleRatesCurrUnit = null;
	private String [] prScalePricingUnit = null;
	private String [] prScaleProductUnit = null;
	
	private String [] fgScaleLineRefs = null;
	private String [] fgMinQuantities = null;
	private String [] fgQuantityUnits = null;
	
	private String [] fgQuantities = null;
	private String [] fgAddQuantities = null;
	private String [] fgAddQuantityUnits = null;
	
	private String [] fgAddProducts = null;
	private String [] fgCalcTypes = null;
	private String [] fgIndicators = null;
	
	private int noOfConditionRecord = 0;

	private String appFieldNumbers = null;
	private String appFieldOffsets = null;
	private int appFieldDim = 0;
	private int appFieldOffSetValue = 0;
		
	private String usgFieldNumbers = null;
	private String usgFieldOffsets = null;
	private int usgFieldDim = 0;
	private int usgFieldOffSetValue = 0;

	private String sclLineNumbers = null;
	private String sclLineOffsets = null;
	private int sclLineDim = 0;
	private int sclLineOffSetValue = 0;
	
	private String condType = null;
	private String validFrom = null;
	private String validTo = null;
	
	private Vector applicationFields = new Vector();;
	private Vector usageFields       = new Vector();
	private Vector scaleHeader		 = new Vector();
	private Vector pricingScaleDetails = new Vector();
	
		
	private ConditionRecord[] conditionRecord = new ConditionRecord[1];
	//private Vector conditionRecordVector = null;	
	
	private int scaleBaseIndex = 0; 
	private int scaleNumberIndex = 0;
	private int scaleOffsetIndex = 0;
	private String usage;
	
	/*private static String FN_CONDITION_TYPE= "KSCHL";
	private static String FN_VALID_FROM = "TIMESTAMP_FROM";		
	private static String FN_VALID_TO   = "TIMESTAMP_TO";*/
	
	private static String DN_CONDITION_TYPE= "/SAPCND/COND_TYPE";
	private static String DN_VALID_FROM = "/SAPCND/TIMESTAMP_FROM";		
	private static String DN_VALID_TO   = "/SAPCND/TIMESTAMP_TO";
	
	public ConvertConditionRecordsIntoExt(){
	}
	
	public ConditionRecord[] getConditionRecords(HashMap conditionRecordInt, ConditionMastDataDisplayModel cMDDM){
		//In the constructor itself, values from hashmap will be extracted and condition record is formed.
		
		if (conditionRecordInt == null){
			conditionRecord[0] = new ConditionRecord();
			return conditionRecord;
		}
//		    return conditionRecordVector;
		usage = cMDDM.getUsage();
		
		if (conditionRecordInt.get(NUMBER_OF_RECORD_RETURNED) != null){
			
			numberOfRecordReturned = (String)conditionRecordInt.get(NUMBER_OF_RECORD_RETURNED);
			noOfConditionRecord = new Integer(numberOfRecordReturned).intValue();
			
			if (noOfConditionRecord == 0){
				conditionRecord[0] = new ConditionRecord();
				return conditionRecord;
			}

			//conditionRecordVector = new Vector(noOfConditionRecord);
			
			conditionRecord = new ConditionRecord[noOfConditionRecord];
			
			if (conditionRecordInt.get(CONDITION_RECORD_IDS) != null)
				conditionRecordIds = (String [])conditionRecordInt.get(CONDITION_RECORD_IDS);
				
			if (conditionRecordInt.get(VALIDITY_ENDS) != null)				
				validityEnds	   = (String [])conditionRecordInt.get(VALIDITY_ENDS);
				
			if (conditionRecordInt.get(VALIDITY_STARTS) != null)				
				validityStarts     = (String [])conditionRecordInt.get(VALIDITY_STARTS);
			
			if (conditionRecordInt.get(OBJECT_IDS) != null)
				objectIds		   = (String [])conditionRecordInt.get(OBJECT_IDS);
				
			if (conditionRecordInt.get(RELEASE_STATUS) != null)				
				releaseStatus	   = (String [])conditionRecordInt.get(RELEASE_STATUS);
				
			if (conditionRecordInt.get(CONDITION_TYPES) != null)				
				conditionTypes     = (String [])conditionRecordInt.get(CONDITION_TYPES);
			
			if (conditionRecordInt.get(SUPPLEMENTARY_CONDITION_TYPES) != null)
				supplementaryConditionTypes = (String [])conditionRecordInt.get(SUPPLEMENTARY_CONDITION_TYPES);
				
			if (conditionRecordInt.get(GROUP_IDS) != null)	
				groupIds		   = (String [])conditionRecordInt.get(GROUP_IDS);
				
			if (conditionRecordInt.get(SUPPLEMENTARY_CONDITION_IDS) != null)	
				supplementaryConditionIds  = (String [])conditionRecordInt.get(SUPPLEMENTARY_CONDITION_IDS);
			
			if (conditionRecordInt.get(DIMENSION_NUMBERS) != null)
				dimensionNumbers   = (String [])conditionRecordInt.get(DIMENSION_NUMBERS);
				
			if (conditionRecordInt.get(APPLICATION_FIELD_NUMBERS) != null)	
				applicationFieldNumbers = (String [])conditionRecordInt.get(APPLICATION_FIELD_NUMBERS);
				
			if (conditionRecordInt.get(APPLICATION_FIELD_OFFSETS) != null)	
				applicationOffsets = (String [])conditionRecordInt.get(APPLICATION_FIELD_OFFSETS);

			if (conditionRecordInt.get(USAGE_FIELD_NUMBERS) != null)
				usageFieldNumbers  = (String [])conditionRecordInt.get(USAGE_FIELD_NUMBERS);
				
			if (conditionRecordInt.get(USAGE_FIELD_OFFSETS) != null)	
				usageFieldOffsets  = (String [])conditionRecordInt.get(USAGE_FIELD_OFFSETS);
				
			if (conditionRecordInt.get(SCALE_BASE_TYPES) != null)	
				scaleBaseTypes     = (String [])conditionRecordInt.get(SCALE_BASE_TYPES);
			
			if (conditionRecordInt.get(SCALE_LINE_NUMBERS) != null)
				scaleLineNumbers   = (String [])conditionRecordInt.get(SCALE_LINE_NUMBERS);
				
			if (conditionRecordInt.get(SCALE_LINE_OFFSETS) != null)	
				scaleLineOffsets   = (String [])conditionRecordInt.get(SCALE_LINE_OFFSETS);

			if (conditionRecordInt.get(APPLICATION_FIELD_NAMES) != null)
				applicationFieldNames   = (String [])conditionRecordInt.get(APPLICATION_FIELD_NAMES);
				
			if (conditionRecordInt.get(APPLICATION_FIELD_VALUES) != null)	
				applicationFieldValues  = (String [])conditionRecordInt.get(APPLICATION_FIELD_VALUES);
				
			if (conditionRecordInt.get(USAGE_FIELD_NAMES) != null)
				usageFieldNames   = (String [])conditionRecordInt.get(USAGE_FIELD_NAMES);
				
			if (conditionRecordInt.get(USAGE_FIELD_VALUES) != null)	
				usageFieldValues  = (String [])conditionRecordInt.get(USAGE_FIELD_VALUES);
			
			if (conditionRecordInt.get(PR_SCALE_LINE_REFS) != null)
				prScaleLineRefs = (String [])conditionRecordInt.get(PR_SCALE_LINE_REFS);
				
			if (conditionRecordInt.get(PR_SCALE_AMOUNTS) != null)				
				prScaleAmounts  = (String [])conditionRecordInt.get(PR_SCALE_AMOUNTS);
				
			if (conditionRecordInt.get(PR_SCALE_RATES) != null)	
				prScaleRates    = (String [])conditionRecordInt.get(PR_SCALE_RATES);
				
			if (conditionRecordInt.get(PR_SCALE_RATES_CURR_UNIT) != null)	
				prScaleRatesCurrUnit    = (String [])conditionRecordInt.get(PR_SCALE_RATES_CURR_UNIT);
				
			if (conditionRecordInt.get(PR_SCALE_PRICING_UNIT) != null)	
				prScalePricingUnit   = (String [])conditionRecordInt.get(PR_SCALE_PRICING_UNIT);
				
			if (conditionRecordInt.get(PR_SCALE_PRODUCT_UNIT) != null)	
				prScaleProductUnit   = (String [])conditionRecordInt.get(PR_SCALE_PRODUCT_UNIT);
				
			if (conditionRecordInt.get(FG_SCALE_LINE_REFS) != null)	
				fgScaleLineRefs = (String [])conditionRecordInt.get(FG_SCALE_LINE_REFS);
				
			if (conditionRecordInt.get(FG_MIN_QUANTITIES) != null)	
				fgMinQuantities = (String [])conditionRecordInt.get(FG_MIN_QUANTITIES);
				
			if (conditionRecordInt.get(FG_QUANTITY_UNITS) != null)	
				fgQuantityUnits = (String [])conditionRecordInt.get(FG_QUANTITY_UNITS);
				
			if (conditionRecordInt.get(FG_QUANTITIES) != null)	
				fgQuantities = (String [])conditionRecordInt.get(FG_QUANTITIES);

			if (conditionRecordInt.get(FG_ADD_QUANTITIES) != null)	
				fgAddQuantities = (String [])conditionRecordInt.get(FG_ADD_QUANTITIES);

			if (conditionRecordInt.get(FG_ADD_QUANTITY_UNITS) != null)	
				fgAddQuantityUnits = (String [])conditionRecordInt.get(FG_ADD_QUANTITY_UNITS);

			if (conditionRecordInt.get(FG_ADD_PRODUCTS) != null)	
				fgAddProducts = (String [])conditionRecordInt.get(FG_ADD_PRODUCTS);

			if (conditionRecordInt.get(FG_CALC_TYPES) != null)	
				fgCalcTypes = (String [])conditionRecordInt.get(FG_CALC_TYPES);

			if (conditionRecordInt.get(FG_INDICATORS) != null)	
				fgIndicators = (String [])conditionRecordInt.get(FG_INDICATORS);
				
			errorStatusApplicationFields = cMDDM.getExternalConversionStatus();				

			for(int index = 0; index < noOfConditionRecord; index++){
				
				conditionRecord[index] = new ConditionRecord();
				
				conditionRecord[index].setNumberOfRecordReturned(numberOfRecordReturned);
				
				if (conditionRecordIds != null)
					conditionRecord[index].setConditionRecordIds(conditionRecordIds[index]);
				if (validityEnds != null)	
					conditionRecord[index].setValidityEnds(validityEnds[index]);
				if (validityStarts != null)	
					conditionRecord[index].setValidityStarts(validityStarts[index]);
				
				if (objectIds != null)
					conditionRecord[index].setObjectIds(objectIds[index]);
				if (releaseStatus != null)	
					conditionRecord[index].setReleaseStatus(releaseStatus[index]);
				if (conditionTypes != null)	
					conditionRecord[index].setConditionTypes(conditionTypes[index]);
				
				if (supplementaryConditionTypes != null)
					conditionRecord[index].setSupplementaryConditionTypes(supplementaryConditionTypes[index]);
				if (groupIds != null)	
					conditionRecord[index].setGroupIds(groupIds[index]);
				if (supplementaryConditionIds != null)	
					conditionRecord[index].setSupplementaryConditionIds(supplementaryConditionIds[index]);
				
				if (dimensionNumbers != null)
					conditionRecord[index].setDimensionNumbers(dimensionNumbers[index]);
				if (applicationFieldNumbers != null){
					appFieldNumbers = applicationFieldNumbers[index];
					//conditionRecord.setApplicationFieldNumbers(applicationFieldNumbers[index]);
					conditionRecord[index].setApplicationFieldNumbers(appFieldNumbers);
			    }
				if (applicationOffsets != null){
					appFieldOffsets = applicationOffsets[index];
					//conditionRecord.setApplicationOffsets(applicationOffsets[index]);
					conditionRecord[index].setApplicationOffsets(appFieldOffsets);
				}
				
				if (usageFieldNumbers != null){
					usgFieldNumbers = usageFieldNumbers[index];
					//conditionRecord.setUsageFieldNumbers(usageFieldNumbers[index]);
					conditionRecord[index].setUsageFieldNumbers(usgFieldNumbers);
				}					
				if (usageFieldOffsets != null){
					usgFieldOffsets  = usageFieldOffsets[index];
					//conditionRecord.setUsageFieldOffsets(usageFieldOffsets[index]);
					conditionRecord[index].setUsageFieldOffsets(usgFieldOffsets);
				}
									
				if (scaleBaseTypes != null)	{
					if (dimensionNumbers != null){
						if (Integer.parseInt(dimensionNumbers[index]) > 0){
							// check if this is a null value which might appear because of a parser bug
							while ( scaleBaseTypes[scaleBaseIndex] == null ) {
								scaleBaseIndex++; 							
							}
							conditionRecord[index].setScaleBaseTypes(scaleBaseTypes[scaleBaseIndex]);
							scaleBaseIndex++;
						}	
						else					
						conditionRecord[index].setScaleBaseTypes(null);
					}
					
				}

				if (scaleLineNumbers != null){
					if (dimensionNumbers != null){
						if (Integer.parseInt(dimensionNumbers[index]) > 0){		
							sclLineNumbers = scaleLineNumbers[scaleNumberIndex];
							// check if this is a null value which might appear because of a parser bug
							while ( sclLineNumbers == null ){	
								scaleNumberIndex++;				
								sclLineNumbers = scaleLineNumbers[scaleNumberIndex];		
							}
							conditionRecord[index].setScaleLineNumbers(sclLineNumbers);
							scaleNumberIndex++;
						}
						else
							conditionRecord[index].setScaleLineNumbers(null);
					}
				}
					
				if (scaleLineOffsets != null){
					if (dimensionNumbers != null){
						if (Integer.parseInt(dimensionNumbers[index]) > 0){		
							sclLineOffsets = scaleLineOffsets[scaleOffsetIndex];
							// check if this is a null value which might appear because of a parser bug
							while ( sclLineOffsets == null ) {
								scaleOffsetIndex++;
								sclLineOffsets = scaleLineOffsets[scaleOffsetIndex];
							}
							conditionRecord[index].setScaleLineOffsets(sclLineOffsets);
							scaleOffsetIndex++;
						}
						else
						conditionRecord[index].setScaleLineOffsets(null);
					}
					
				}					
				
				if (applicationFieldNames != null && applicationFieldValues != null){

					appFieldDim = new Integer(appFieldNumbers).intValue();
					appFieldOffSetValue = new Integer(appFieldOffsets).intValue();
					conditionRecord[index].setApplicationFields(applicationFieldNames, applicationFieldValues,
					         					  errorStatusApplicationFields,appFieldDim,appFieldOffSetValue);

				}														 
									  					 
															
				if (usageFieldNames != null && usageFieldValues != null){								
					usgFieldDim  = new Integer(usgFieldNumbers).intValue();
					usgFieldOffSetValue  = new Integer(usgFieldOffsets).intValue();

					conditionRecord[index].setUsageFields(usageFieldNames, usageFieldValues,
												   usgFieldDim, usgFieldOffSetValue);
												   
				}												   
														  
				if (prScaleLineRefs != null && prScaleAmounts != null && prScaleRates != null){
						
					if (dimensionNumbers != null){
						if (Integer.parseInt(dimensionNumbers[index]) > 0){
							sclLineDim  = new Integer(sclLineNumbers).intValue();
							sclLineOffSetValue = new Integer(sclLineOffsets ).intValue();
							
							conditionRecord[index].setPricingScaleStructure(prScaleLineRefs, prScaleAmounts, prScaleRates,
																	prScaleRatesCurrUnit, prScalePricingUnit, prScaleProductUnit, 
																	sclLineDim, sclLineOffSetValue);
						}
					}
				}	
				
				if (fgScaleLineRefs != null && fgMinQuantities != null && fgQuantityUnits != null){
						
					if (dimensionNumbers != null){
						if (Integer.parseInt(dimensionNumbers[index]) > 0){
							sclLineDim  = new Integer(sclLineNumbers).intValue();
							sclLineOffSetValue = new Integer(sclLineOffsets ).intValue();
							
							conditionRecord[index].setFGScaleStructure(fgScaleLineRefs, fgMinQuantities, fgQuantityUnits, 
																		sclLineDim, sclLineOffSetValue);
						}
					}
				}
				
				if (fgQuantities != null && fgAddQuantities != null && fgAddQuantityUnits != null){
						
					if (dimensionNumbers != null){
						if (Integer.parseInt(dimensionNumbers[index]) > 0){
							sclLineDim  = new Integer(sclLineNumbers).intValue();
							sclLineOffSetValue = new Integer(sclLineOffsets ).intValue();
							
							conditionRecord[index].setFGQuantity(fgQuantities, fgAddQuantities, fgAddQuantityUnits,
																	sclLineDim, sclLineOffSetValue);
						}
					}
				}	
				
				if (fgAddProducts != null && fgCalcTypes != null && fgIndicators != null){
						
					if (dimensionNumbers != null){
						if (Integer.parseInt(dimensionNumbers[index]) > 0){
							sclLineDim  = new Integer(sclLineNumbers).intValue();
							sclLineOffSetValue = new Integer(sclLineOffsets ).intValue();
							
							conditionRecord[index].setFGProduct(fgAddProducts, fgCalcTypes, fgIndicators,
																sclLineDim, sclLineOffSetValue);
						}
					}
				}					

				conditionRecord[index].setAppFieldsBluePrintSequence(applicationFields);
				conditionRecord[index].setUsgFieldsBluePrintSequence(usageFields);
				conditionRecord[index].setPricingScaleDetailsBluePrintSequence(pricingScaleDetails);
				conditionRecord[index].setScaleHeaderBluePrintSequence(scaleHeader);
				conditionRecord[index].setUsage(usage);
				
			    
				//conditionRecordVector.add(conditionRecord[index]);
				
				//conditionRecord.getConditionRecord();
													  
			}

		}
		else{
		//  throw the exception stating that condition records doesn't exist
			conditionRecord[0] = new ConditionRecord();
		}
		return conditionRecord;
		
	}
	
	public Vector getConditionRecordBluePrintStructure(InitialiseConditionSelectionPannel selectPannel, ConditionMastDataDisplayModel cMDDM){
		
		String condType = null;
		String validFrom = null;
		String validTo = null;		
		String fieldName = null;
		String fieldRealName = null;
		String scaleUnit = null;
		String scaleCurrency = null;
		
		Vector condRecordBluePrintStruc = new Vector();
		UsageDetails usageDetails = new UsageDetails(cMDDM);
		
		SelectionField selField;
		
		if (selectPannel != null)
			selectPannel.resetEnumeration();

		boolean conditionRecordExist = true;
		
		String [] dataElements = new String[1];
		IPCClient client;
		Hashtable fieldNameTexts = null;
		Vector fieldNameVector = new Vector();
		
		String fieldLabel = null;
		String[] fieldTextArray = null;
		final int LONG_TEXT_POSITION = 3;
		
		HashMap conditionRecord = cMDDM.getConditionRecExt(); 
		client = cMDDM.getIPCSession().getIPCClient();
		
		//no need to check for the null for condition record hashmap as already this check is made in
		//action class ShowConditionRecord before call to this method is made.

		if (conditionRecord.get(NUMBER_OF_RECORD_RETURNED) != null){
			
			numberOfRecordReturned = (String)conditionRecord.get(NUMBER_OF_RECORD_RETURNED);
			noOfConditionRecord = new Integer(numberOfRecordReturned).intValue();
			
			if (noOfConditionRecord == 0){
				conditionRecordExist = false;
			}
			
			if (conditionRecord.get(APPLICATION_FIELD_NAMES) != null)
				applicationFieldNames = (String [])conditionRecord.get(APPLICATION_FIELD_NAMES);
				
			if (conditionRecord.get(USAGE_FIELD_NAMES) != null)
				usageFieldNames   = (String [])conditionRecord.get(USAGE_FIELD_NAMES);
				
			if (applicationFieldNames != null){

				for(int index = 0; index < applicationFieldNames.length;index++){
					
					if (!(fieldNameVector.contains(applicationFieldNames[index])))
					fieldNameVector.add(applicationFieldNames[index]);
					
				} //end of for(int index = 0; index < applicationFieldNames.length;index++)
				
				fieldNameVector.trimToSize();
				
				applicationFieldNames = (String[]) fieldNameVector.toArray(dataElements);
				
				//application field name  contains all application fieldn names now.
				//Now we will get the field label for these application field name and form the blue print structure.
				//Get the lables for the extenal fields which are associated to the internal fields we have so fare
				fieldNameTexts = getExtFieldLabelForIntFieldNames(cMDDM.getUsage(), cMDDM.getApplication(), applicationFieldNames, true, client);
				// fieldNameTexts = client.getConditionTableFieldLabel(cMDDM.getUsage(), cMDDM.getApplication(), applicationFieldNames, null, true);
				
				if ( fieldNameTexts != null ){
					for ( int index = 0; index < applicationFieldNames.length; index ++){ 
						// reset the text array
						fieldTextArray = null;
						if (fieldNameTexts != null){						
							// get the corresponding texts
							fieldTextArray = (String[]) fieldNameTexts.get(applicationFieldNames[index]);
						}
				
						if ( fieldTextArray != null ){
							// use the long text as field label
					
							if (fieldTextArray[LONG_TEXT_POSITION] != null && !(fieldTextArray[LONG_TEXT_POSITION].equals(""))){
								fieldLabel = fieldTextArray[LONG_TEXT_POSITION];
								condRecordBluePrintStruc.add(fieldLabel);
								applicationFields.add(applicationFieldNames[index]);
							}else{
								//if we do not have text labels, use the data element name as label
								condRecordBluePrintStruc.add(applicationFieldNames[index]);
								applicationFields.add(applicationFieldNames[index]);
							}
						}else {					
							// if we do not have text labels, use the field name as label
							condRecordBluePrintStruc.add(applicationFieldNames[index]);
							applicationFields.add(applicationFieldNames[index]);
						}
					}
				}
				
			} //end of if (applicationFieldNames != null) 
			
			
		}
		
		//take out the field name from selection panel and add it condition record
		//blue print struct only if its not added to condition record blue print struct.
		if (selectPannel == null){
			selectPannel = new InitialiseConditionSelectionPannel();
					
			//it means, selection is based on parameters passed in URL and we will not get
			//labels for condition type, valid from and valid to as select panel is null, we
    		//will add the same explicitly in fieldNameVector.
			
			String [] selectPannelDateElementName = new String[1];
			
			fieldNameVector.clear();
			fieldNameVector.add(DN_CONDITION_TYPE);
			fieldNameVector.add(DN_VALID_FROM);
			fieldNameVector.add(DN_VALID_TO);
			fieldNameVector.trimToSize();
			selectPannelDateElementName = (String[]) fieldNameVector.toArray(dataElements);			
			
			// fieldNameTexts = client.getConditionTableFieldLabel(cMDDM.getUsage(), cMDDM.getApplication(), null, selectPannelDateElementName, false);
				
			if ( fieldNameTexts != null ){
				for ( int index = 0; index < selectPannelDateElementName.length; index ++){
					// reset the text array
					fieldTextArray = null;
					if (fieldNameTexts != null){						
						// get the corresponding texts
						fieldTextArray = (String[]) fieldNameTexts.get(selectPannelDateElementName[index]);
					}
				
					if ( fieldTextArray != null ){
						// use the long text as field label
					
						if (fieldTextArray[LONG_TEXT_POSITION] != null && !(fieldTextArray[LONG_TEXT_POSITION].equals(""))){
							fieldLabel = fieldTextArray[LONG_TEXT_POSITION];
						}else{
							//if we do not have text labels, use the field name as label
							fieldLabel = selectPannelDateElementName[index];
						}
					}else {					
						// if we do not have text labels, use the field name as label
						fieldLabel = selectPannelDateElementName[index];
					}
				
					if (selectPannelDateElementName[index].equalsIgnoreCase(DN_CONDITION_TYPE)){
						condType = fieldLabel;
					}else if (selectPannelDateElementName[index].equalsIgnoreCase(DN_VALID_FROM)){
						validFrom = fieldLabel;
					}else if (selectPannelDateElementName[index].equalsIgnoreCase(DN_VALID_TO)){
						validTo = fieldLabel;
					}
					
				}
			}			
					
		}


		while (selectPannel.hasMoreElements())
		{

			selField = (SelectionField)selectPannel.nextElement();
			fieldName = selField.getFieldLabel();
			fieldRealName = selField.getRealFieldName();
			
			if (fieldName == null)
				fieldName = fieldRealName;

			//if (fieldName.equalsIgnoreCase("Condition Type"))
			if (fieldRealName.equalsIgnoreCase("KSCHL"))
				condType = selField.getFieldLabel();
			//else if (fieldName.equalsIgnoreCase("Valid from"))				
			else if (fieldRealName.equalsIgnoreCase("TIMESTAMP_FROM"))
				validFrom = selField.getFieldLabel();
			//else if (fieldName.equalsIgnoreCase("Valid to"))
			else if (fieldRealName.equalsIgnoreCase("TIMESTAMP_TO"))
				validTo = selField.getFieldLabel(); 
			//else if (!(fieldName.equalsIgnoreCase("Created by")) && 
			//		 !(fieldName.equalsIgnoreCase("Created on")) &&
			//		 !(fieldName.equalsIgnoreCase("Condition Table")) &&
		   	//		 !(fieldName.equalsIgnoreCase(" "))
			else if (!(fieldRealName.equalsIgnoreCase("CREATED_BY")) && 
					 !(fieldRealName.equalsIgnoreCase("CREATED_ON")) &&
					 !(fieldRealName.equalsIgnoreCase("KOTABNR")) &&
					 !(fieldRealName.equalsIgnoreCase(" "))
		   			 
					){
						if (fieldName != null){
							//condRecordBluePrintStruc.add(fieldName);
							//applicationFields.add(fieldRealName);
							if (!(applicationFields.contains(fieldRealName))){
								condRecordBluePrintStruc.add(fieldName);
								applicationFields.add(fieldRealName);
							}
						}
					}		
							
		}
		
		condRecordBluePrintStruc.add(0,condType);
			
		//take the blue print entries of usage field and add it to condition record
		//blue print structure.	
		condRecordBluePrintStruc.addAll(usageDetails.getUsageFieldBluePrintStruct(usageFieldNames));
		
		//take the data elements of usage fields.
		usageFields = usageDetails.getUsageFields(usageFieldNames);
			
		
		condRecordBluePrintStruc.add(validFrom);
		condRecordBluePrintStruc.add(validTo);
		condRecordBluePrintStruc.add("Status");
		condRecordBluePrintStruc.add("Scale Indicator");
		
		condRecordBluePrintStruc.trimToSize();
		applicationFields.trimToSize();
		usageFields.trimToSize();
		
		scaleHeader = usageDetails.getScaleHeaderBluePrintStruct();
		scaleHeader.trimToSize();
		
		pricingScaleDetails = usageDetails.getScaleDetailsBluePrintStruct();
		pricingScaleDetails.trimToSize();
		
		return condRecordBluePrintStruc;
	}
	

	protected static Hashtable getExtFieldLabelForIntFieldNames( String usage, 
	                                                    String application, 
	                                                    String[] fieldNames, 
	                                                    boolean isApplicationField,
	                                                    IPCClient client )
	{
		Hashtable retVal = new Hashtable();
		Hashtable externalFieldNames;
		Hashtable extFieldNameTexts;
		Hashtable intFieldNameTexts;		
		String[] resultExtFieldNamesArray;
		String[] extFieldNameArray = null;
		String   extFieldName = null;
		String   intFieldName = null;
		String[] fieldLabelArray = null;		
		int ArrayPosition = 0;
		Enumeration keys = null;
		
				
		// check the input parameters			
		if ( (usage != null ) && 
		     (application != null) &&
		     (fieldNames != null) &&
		     (client != null) )
		     {
		     	// first get the external field names for the internal fields
				// convert internal fields to external fields
				// assumption : only application fields availalbe
				externalFieldNames = client.convertFieldnameInternalToExternal(usage, 
				                                                               application, 
				                                                               fieldNames, 
				                                                               isApplicationField);
				                                                               
				resultExtFieldNamesArray = new String[fieldNames.length];  
		
				if ( externalFieldNames != null ){		
					keys = externalFieldNames.keys();   				    	
					// loop for all entries of the hash table with external field names
					while (keys.hasMoreElements()){
						intFieldName = (String) keys.nextElement();
						extFieldNameArray = (String[]) externalFieldNames.get(intFieldName);
						// check how many external field names we have for the internal field
						if ( extFieldNameArray.length > 1){
							// we have more than one ext field name
							// check if this is a knowen ext field so that we can make a selection
							if (intFieldName.equalsIgnoreCase("PRODUCT"))
								extFieldName = "PRODUCT_ID";
							else {
								// concatenate all field names to one long string
								extFieldName = extFieldNameArray[0];
								for (int aLoop = 1; aLoop < extFieldNameArray.length; aLoop ++){
									extFieldName = "_" + extFieldNameArray[aLoop];    
								}
							}
						}
						else {
							// only one ext. fieldname for the int fieldname
							extFieldName = extFieldNameArray[0];
						}
						// overwrite the hash table with the only used ext field name
						externalFieldNames.put(intFieldName, extFieldName);
						// create a temp array of the external field names
						resultExtFieldNamesArray[ArrayPosition] = extFieldName; 
						ArrayPosition ++;
					}
				}		
                
                // now we have a list of external fields
                // get the labels for that external fields
				extFieldNameTexts = client.getConditionTableFieldLabel(usage, 
				                                                       application, 
				                                                       resultExtFieldNamesArray, null, 
				                                                       isApplicationField);
				                                                       
				// for the ones where we do not find external lables, use the internal field lables
				intFieldNameTexts = client.getConditionTableFieldLabel(usage, 
																	   application, 
																	   fieldNames, null, 
																	   isApplicationField);				                                                       
                if ( extFieldNameTexts != null ){
                	// now create a result hashtable with the internal field name as key
                	// and the external field labelarray as value
                	for ( int loop = 0; loop < fieldNames.length; loop ++){
                		intFieldName = fieldNames[loop];
                		extFieldName = (String) externalFieldNames.get(intFieldName);
                		if ( extFieldName != null){
							fieldLabelArray = (String[]) extFieldNameTexts.get(extFieldName);
							// check if the field label array is filled
							if ( (fieldLabelArray == null) || 
							     (fieldLabelArray[0]==null) ){
							     // no labels found for the external name, so use the ones for the internal name	
							     fieldLabelArray = (String[]) intFieldNameTexts.get(intFieldName);	                			
							     }
                		}
                		else {
                			// no external field, use the internal field label if available available
                			fieldLabelArray = (String[]) intFieldNameTexts.get(intFieldName);
                		}
                		// add an entry in the hash table with internal field name as key
                		// and external field label array as value
                		if ( fieldLabelArray != null) {
							retVal.put(intFieldName,fieldLabelArray);	                			
                		}
                		else {
                			// as a last chance to provide anything, return an empty array 
							retVal.put(intFieldName, new String[4]);	                			
                		}
			                                                       
                	}
                }                
		     }    
	                                                    
        return retVal;	                                                    	    
    }
}