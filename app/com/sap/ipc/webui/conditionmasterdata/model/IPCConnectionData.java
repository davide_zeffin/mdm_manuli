/*******************************************************************************

    Copyright (c) 2005 by SAP AG

    All rights to both implementation and design are reserved.

    Use and copying of this software and preparation of derivative works based
    upon this software are not permitted.

    Distribution of this software is restricted. SAP does not make any warranty
    about the software, its performance or its conformity to any specification.

*******************************************************************************/
package com.sap.ipc.webui.conditionmasterdata.model;

/**
 * @author d043319
 */
public class IPCConnectionData {

	public static final String SessionAttributeName = "IPCConnectionData";

	private String host;
	private String port;
	private String client;
	private String mode;

	public IPCConnectionData(String argHost, String argPort, String argClient, String argMode) {
		host = argHost;
		port = argPort;
		client = argClient;
		mode = argMode;
	}

	/**
	 * @return
	 */
	public String getClient() {
		return client;
	}

	/**
	 * @return
	 */
	public String getHost() {
		return host;
	}

	/**
	 * @return
	 */
	public String getMode() {
		return mode;
	}

	/**
	 * @return
	 */
	public String getPort() {
		return port;
	}

	/**
	 * @param string
	 */
	public void setClient(String string) {
		client = string;
	}

	/**
	 * @param string
	 */
	public void setHost(String string) {
		host = string;
	}

	/**
	 * @param string
	 */
	public void setMode(String string) {
		mode = string;
	}

	/**
	 * @param string
	 */
	public void setPort(String string) {
		port = string;
	}

}
