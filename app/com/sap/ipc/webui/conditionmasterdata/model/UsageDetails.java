/*
 * Created on Jul 13, 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.ipc.webui.conditionmasterdata.model;

import com.sap.spc.remote.client.object.IPCClient;

import java.util.*;

/**
 * @author I026516
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class UsageDetails {
	
	private Vector scaleHeaderBluePrintStruct = new Vector();;
	private Vector scaleDetailsBluePrintStruct = new Vector();;
	private Vector usageFieldBluePrintStruct = new Vector();
	private Vector usageFields = new Vector();
	private ConditionMastDataDisplayModel cMDDM = null;
	private String usage;
	private String application;
	private IPCClient client;
	
	//private String[] dataElements = new String[1];
	private String[] fieldNames = new String[1];
	
	//private Hashtable dataElementTexts = null;
	private Hashtable fieldNameTexts = null;
	
	private String[] fieldTextArray = null;
	private String[] usageFieldNames = null;

	protected final int EXTRA_SHORT_TEXT_POSITION = 0;
	protected final int SHORT_TEXT_POSITION = 1; 
	protected final int MEDIUM_TEXT_POSITION = 2;
	protected final int LONG_TEXT_POSITION = 3;		
	
    // usage fields for PR usage	
	private static String FN_KBETR = "KBETR";		//Condition Rate
	private static String FN_KONWA = "KONWA";		//Condition Currency
	private static String FN_KPEIN = "KPEIN";		//Condition Pricing Unit
	private static String FN_KMEIN = "KMEIN";		//Unit of Measure for Product-Specific Quantity
	private static String FN_KRECH = "KRECH";		//Calculation Type for Condition
	
	// usage fields for FG usage
	private static String FN_FGD_QNTY = "FGD_QNTY";						//Free goods quanity
	private static String FN_FGD_ADD_QNTY = "FGD_ADD_QNTY";				//free quantity
	private static String FN_FGD_ADD_QNT_UNIT = "FGD_ADD_QNT_UNIT";		//free quantity unit
	private static String FN_FGD_ADD_PRODUCT = "FGD_ADD_PRODUCT";	    //free product
	private static String FN_FGD_CAL_TYPE = "FGD_CAL_TYPE";				//calculation rule
	private static String FN_FGD_EXCLINCL_IND = "FGD_EXCLINCL_IND";		//free goods type	
	private static String FN_FGD_LSTANR = "FGD_LSTANR";					//free goods delivery control

	// usage fields for CD usage
	private static String FN_CAMPAIGN_ID = "CAMPAIGN_ID";				//Campaign ID
	
	private final String SCALE_UNIT = "SCALE_UNIT";
	private final String SCALE_CURRENCY = "SCALE_CURRENCY";
	private final String SCALE_TYPE = "SCALE_TYPE";
	
	/*
	private static String DN_KBETR = "PRCT_COND_RATE";			  //Condition Rate
	private static String DN_KONWA = "PRCT_COND_CURR";			  //Condition Currency
	private static String DN_KPEIN = "PRCT_COND_PRICING_UNIT";	  //Condition Pricing Unit
	private static String DN_KMEIN = "PRCT_PROD_UOM";			  //Unit of Measure for Product-Specific Quantity
	private static String DN_KRECH = "PRCT_CALCULATION_TYPE";	  //Calculation Type for Condition
	*/
	
	private final String PRICING_USAGE = "PR";
	private final String FREEGOODS_USAGE = "FG";
	private final String CAMPAIGN_DETERMINATION_USAGE = "CD";
	
	
	public UsageDetails(ConditionMastDataDisplayModel cMDDM){
		this.cMDDM = cMDDM;
		application = cMDDM.getApplication();
		usage = cMDDM.getUsage();
		client = cMDDM.getIPCSession().getIPCClient();
	}
	
	public Vector getScaleHeaderBluePrintStruct(){
		/*
		 * getScaleHeaderBluePrintStruct method will return the scale
		 * header columns field labels
		 */
		
		scaleHeaderBluePrintStruct.add("Scale Base Type");
		scaleHeaderBluePrintStruct.add("Scale Type");
		scaleHeaderBluePrintStruct.trimToSize();
		return scaleHeaderBluePrintStruct;
		
	}
	
	public Vector getScaleDetailsBluePrintStruct(){
		/*
		 * getScaleDetailsBluePrintStruct method will return the scale
		 * details columns field labels. we will add the keys and the values will 
		 * be read from property file
		 */		
		
		if (usage.equalsIgnoreCase(PRICING_USAGE)){
			scaleDetailsBluePrintStruct.add("cmdd.CondShow.scaleAmount");
			scaleDetailsBluePrintStruct.add("cmdd.CondShow.scaleUnit");
			scaleDetailsBluePrintStruct.add("cmdd.CondShow.scaleCurrency");
			scaleDetailsBluePrintStruct.add("cmdd.CondShow.scaleRate");
			scaleDetailsBluePrintStruct.add("cmdd.CondShow.pricingUnit");
			scaleDetailsBluePrintStruct.add("cmdd.CondShow.productUnit");
			
			scaleDetailsBluePrintStruct.trimToSize();
		}else if (usage.equalsIgnoreCase(FREEGOODS_USAGE)) {
			scaleDetailsBluePrintStruct.add("cmdd.CondShow.fgMinQuantity");
			scaleDetailsBluePrintStruct.add("cmdd.CondShow.fgQuantityUnit");
			scaleDetailsBluePrintStruct.add("cmdd.CondShow.fgQuantity");
			scaleDetailsBluePrintStruct.add("cmdd.CondShow.fgFreeQuantity");
			scaleDetailsBluePrintStruct.add("cmdd.CondShow.fgFreeQuantityUnit");
			scaleDetailsBluePrintStruct.add("cmdd.CondShow.fgCalRule");
			scaleDetailsBluePrintStruct.add("cmdd.CondShow.fgType");
			
			scaleDetailsBluePrintStruct.trimToSize();			
		}
		
		return scaleDetailsBluePrintStruct;
		
	}
	
	
	public Vector getUsageFieldBluePrintStruct(String [] usageFieldNames){
		
		/*usageFieldBluePrintStruct will contain the usage fields label which will be displayed
		 * as header columns
		 */ 
		
		 this.usageFieldNames = usageFieldNames;
		 String fieldLabel = null;
		 
		//calling addDataElements to get all the field names for the given usage.
		 
		addDataElements();

        // get the field lables but use the field labels of the external fields  
		fieldNameTexts = ConvertConditionRecordsIntoExt.getExtFieldLabelForIntFieldNames( usage, application, fieldNames, false, client);		
		// fieldNameTexts = client.getConditionTableFieldLabel( usage, application, fieldNames, null, false);
			
		//fieldTextArray = (String[]) fieldNameTexts.get(fieldNames[0]);

		if ( fieldNameTexts != null ){
			for ( int index = 0; index < fieldNames.length; index ++){
				// reset the text array
				fieldTextArray = null;
				if (fieldNameTexts != null){						
					// get the corresponding texts
					fieldTextArray = (String[]) fieldNameTexts.get(fieldNames[index]);
				}
				
				if ( fieldTextArray != null ){
					// use the long text as field label
					
					if (fieldTextArray[LONG_TEXT_POSITION] != null && !(fieldTextArray[LONG_TEXT_POSITION].equals(""))){
						fieldLabel = fieldTextArray[LONG_TEXT_POSITION];
						usageFieldBluePrintStruct.add(fieldLabel);
					}else{
						//if we do not have text labels, use the field name as label
						usageFieldBluePrintStruct.add(fieldNames[index]);
					}
				}else {					
					// if we do not have text labels, use the field name as label
					usageFieldBluePrintStruct.add(fieldNames[index]);
				}
			}
		}

		usageFieldBluePrintStruct.trimToSize();
		return usageFieldBluePrintStruct;
		
	}
	
	public Vector getUsageFields(String [] usageFieldNames){
		/*usageFields will contain the usage fields techincal name which will be used to display the
		 * usage fields value in a sequence of usage blue print structure. usageFields will contain
		 * the usage fields in the same sequence as its added in usageFieldBluePrintStruct
		 * 
		 * For a new usage, field names needs to be added here
		 */
		if (usage.equalsIgnoreCase(PRICING_USAGE)){ 
			usageFields.add(FN_KBETR);
			usageFields.add(FN_KONWA);
			usageFields.add(FN_KPEIN);
			usageFields.add(FN_KMEIN);
			usageFields.add(FN_KRECH);
			
		}else if (usage.equalsIgnoreCase(FREEGOODS_USAGE)){
			usageFields.add(FN_FGD_QNTY);
			usageFields.add(FN_FGD_ADD_QNTY);
			usageFields.add(FN_FGD_ADD_QNT_UNIT);
			usageFields.add(FN_FGD_ADD_PRODUCT);
			usageFields.add(FN_FGD_CAL_TYPE);
			usageFields.add(FN_FGD_EXCLINCL_IND);
			usageFields.add(FN_FGD_LSTANR);
		}else if (usage.equalsIgnoreCase(CAMPAIGN_DETERMINATION_USAGE)){
			usageFields.add(FN_CAMPAIGN_ID);
		}
		
		if (usageFieldNames != null){
			for(int index = 0; index < usageFieldNames.length;index++){
				if ( !(usageFieldNames[index].equalsIgnoreCase(SCALE_UNIT))
					 && !(usageFieldNames[index].equalsIgnoreCase(SCALE_CURRENCY))
					 && !(usageFieldNames[index].equalsIgnoreCase(SCALE_TYPE))	
					){
				
						if (!(usageFields.contains(usageFieldNames[index])))
							usageFields.add(usageFieldNames[index]);
					}
			}
		}
		
		usageFields.trimToSize();
		return usageFields;
	}
	
	private void addDataElements(){
	
	/*
	 * addDataElements method will return the required data elements for the given usage.
	 */
	
		ArrayList fieldNameList = new ArrayList();
	 	
	 	if (usage.equalsIgnoreCase(PRICING_USAGE)){
			fieldNameList.add(FN_KBETR);
			fieldNameList.add(FN_KONWA);
			fieldNameList.add(FN_KPEIN);
			fieldNameList.add(FN_KMEIN);
			fieldNameList.add(FN_KRECH);

			fieldNameList.trimToSize();
			fieldNames = (String [])fieldNameList.toArray(fieldNames);
			
	 	} else if (usage.equalsIgnoreCase(FREEGOODS_USAGE)){
			fieldNameList.add(FN_FGD_QNTY);
			fieldNameList.add(FN_FGD_ADD_QNTY);
			fieldNameList.add(FN_FGD_ADD_QNT_UNIT);
			fieldNameList.add(FN_FGD_ADD_PRODUCT);
			fieldNameList.add(FN_FGD_CAL_TYPE);
			fieldNameList.add(FN_FGD_EXCLINCL_IND);
			fieldNameList.add(FN_FGD_LSTANR);
			
			fieldNameList.trimToSize();
			fieldNames = (String [])fieldNameList.toArray(fieldNames);
	 	} else if (usage.equalsIgnoreCase(CAMPAIGN_DETERMINATION_USAGE)){
	 		fieldNameList.add(FN_CAMPAIGN_ID);
	 		
			fieldNameList.trimToSize();
			fieldNames = (String [])fieldNameList.toArray(fieldNames);	 		
	 	}
	 	
	 	if (usageFieldNames != null){
	 		for(int index = 0; index < usageFieldNames.length;index++){
	 			if ( !(usageFieldNames[index].equalsIgnoreCase(SCALE_UNIT))
				     && !(usageFieldNames[index].equalsIgnoreCase(SCALE_CURRENCY))
				     && !(usageFieldNames[index].equalsIgnoreCase(SCALE_TYPE))	
	 			    ){

	 					if (!(fieldNameList.contains(usageFieldNames[index])))
	 						fieldNameList.add(usageFieldNames[index]);
					}	 					
	 		}

			fieldNameList.trimToSize();
			fieldNames = (String [])fieldNameList.toArray(fieldNames);	 		
	 		
	 	}
	 	
	}
	

}
