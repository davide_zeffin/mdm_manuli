/*
 * Created on Jul 13, 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.ipc.webui.conditionmasterdata.model;
import java.util.HashMap;

import com.sap.spc.remote.client.object.*;

/**
 * @author I019099
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ConditionMastDataDisplayModel {
	
private String _application;
private String _usage;
private String _conditionGroupName;
private HashMap _conditionRecExt;
private IPCSession _IPCSession;
private String _valid_From;
private String _valid_To;
private String[] _paramNames;
private String[] _paramValues;
private String _refTyp;
private String _refGuid;
private String[] _errorMessages; 
private String _maxNumberOfHits;
private int _numOfRecordSelected;
private String[] _externalConversionStatus;

public void setApplication (String application){
	_application = application;
		
}

public String getApplication(){
	return (_application);
}

public void setUsage (String usage){
	_usage = usage;
}

public String getUsage (){
	return (_usage);	
}

public void setConditionGroupName (String condGrpName){
	_conditionGroupName = condGrpName;
}

public String getConditionGroupName (){
 return(_conditionGroupName);
}

public void  setConditionRecExt(HashMap condRecExt){
	_conditionRecExt = condRecExt;
}
 
public HashMap getConditionRecExt (){
	return(_conditionRecExt); 
}

public void setIPCSession ( IPCSession IPCSess){
	_IPCSession = IPCSess;
}

public IPCSession getIPCSession (){
	return(_IPCSession);
}
public void setValidTo (String valid_to){
	_valid_To =  valid_to;
}
public String getValidTo(){
	return (_valid_To);
}

public void setValidFrom (String valid_from){
	_valid_From =  valid_from;
}
public String getValidFrom(){
	return (_valid_From);
}

public void setParamNames (String [] paramNames){
	_paramNames =  paramNames;

}

public String[] getParamNames() {
	return (_paramNames );
}

public void setParamValues (String [] paramValues){
	_paramValues =  paramValues;

}

public String[] getParamValues() {
	return (_paramValues );
}

public void setRefTyp (String refTyp){
	_refTyp =  refTyp;

}

public String getRefTyp() {
	return (_refTyp);
}


public void setRefGuid (String refGuid){
	_refGuid =  refGuid;

}

public String getRefGuid() {
	return (_refGuid);
	
	
}

public void setErrorMessages(String [] errorMessages){
	_errorMessages =  errorMessages;

}

public String[] getErrorMessages() {
	return (_errorMessages);
}


	
/**
 * @return
 */
public String getMaxNumberOfHits() {
	return _maxNumberOfHits;
}

/**
 * @param string
 */
public void setMaxNumberOfHits(String string) {
	_maxNumberOfHits = string;
}

public void setNumOfRecSelected (int i){
	_numOfRecordSelected = i;
}

public int getNumOfRecSelected(){
	return _numOfRecordSelected;
}

public void setExternalConversionStatus(String[] string){
	_externalConversionStatus = string;
}

public String[] getExternalConversionStatus (){
	return _externalConversionStatus;
}
}
	
	
