/************************************************************************

	Copyright (c) 2001 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.ipc.webui.conditionmasterdata.action;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sap.ipc.webui.conditionmasterdata.model.IPCConnectionData;
import com.sap.ipc.webui.conditionmasterdata.model.InitialiseConditionAnalyserPannel;
import com.sap.ipc.webui.conditionmasterdata.model.InitialiseConditionSelectionPannel;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import sun.rmi.transport.proxy.HttpReceiveSocket;

import com.sap.ipc.webui.pricing.UIConstants;
import com.sap.isa.core.Constants;
import com.sap.isa.core.InitAction;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCSession;
import com.sap.spc.remote.client.object.imp.IPCClientObjectFactory;
import com.sap.spc.remote.client.object.imp.tcp.TCPIPCItemReference;
import com.sap.ipc.webui.conditionmasterdata.model.*;

public final class InitialiseConditionAnalyser extends Action {

	private static final IsaLocation log = IsaLocation.getInstance(InitAction.class.getName());

	/**
	 * Constructor for InitialiseConditionAnalyser.
	 */
	public InitialiseConditionAnalyser() {
		super();
	}

	private static String DE_APPLICATION_FIELD_NAME = "/SAPCND/APPLICATION";
	private static String DE_USAGE_FIELD_NAME = "/SAPCND/USAGE";
	private static String DE_MAINT_GROUP_NAME = "/SAPCND/MAINT_GROUP";

	/*Initialize Condition Analyse Panel
	  Parameters provided by request:
	  - server          IPC-Server
	  - port            Port of the IPC-Server
	  - client          client number
	  - mode            switches between edit ("Edit") and display mode ("Display")
	                    currently only display is supported
	  - usage           optional prefill of the usage field
	  - application     optional prefill of the application field
	  - maint group     optional prefill of the maint group field     
      - language        optional language parameter
      - country			optional country parameter	  
      - paramName	    optional, used for selection by GUID. Name of the GUID type
      - paramValue      optional, used for selection by GUID. Value of the GUID               
	*/

	// --------------------------------------------------------- Public Methods

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		IPCConnectionData conData;
		String ipcSessionID;
		IPCSession ipcSession = null;
		IPCClient client;
		InitialiseConditionAnalyserPannel initPannel;
		SelectionField applicationField;
		SelectionField usageField;
		SelectionField maintGroupField;
		String usage = null;
		String application = null;
		String maintGroup = null;
		String paramName = null;
		String paramValue = null;
		String[] dataElementNames = null;
		Hashtable fieldTextResult = null;
		String[] fieldTextArray = null;

		// Extract attributes we will need
		// parameter server
		String server = request.getParameter(UIConstants.Request.Parameter.SERVER);
		// parameter port
		String port = request.getParameter(UIConstants.Request.Parameter.PORT);
		// parameter client
		String mandt = request.getParameter(UIConstants.Request.Parameter.CLIENT);
		// parameter mode
		String mode = request.getParameter(UIConstants.Request.Parameter.MODE);
		
		// optional  usage
		usage = request.getParameter(InitialiseConditionAnalyserPannel.URLUsage);
		if (usage == null){
			usage ="";
		}
		// optional  application
		application = request.getParameter(InitialiseConditionAnalyserPannel.URLApplication);
		if (application == null){
			application =""; 	
		}
		// optional  maint group name
		maintGroup = request.getParameter(InitialiseConditionAnalyserPannel.URLMaintGroup);
		if (maintGroup == null){
			maintGroup = "";							
		}		
		// optional paramName
		paramName = request.getParameter(InitialiseConditionAnalyserPannel.URLParamName);
		// optional paramValue
		paramValue = request.getParameter(InitialiseConditionAnalyserPannel.URLParamValue);
		

		// check if there are already connection data in the session
		// this can be tha case if the user pressed 'cancel' in a following screen
		HttpSession session;
		session = request.getSession();
		conData = (IPCConnectionData) session.getAttribute(IPCConnectionData.SessionAttributeName);
		if (conData == null) {
			// no connection data in the session, so they have to be taken from the request parameter
			if (server == null || port == null) {
				String eMsg;
				IPCException e;
				// the connection data was not in the session nor in the request
				// so we have to stop here !!!        	     
				eMsg = "No IPC server, port or client information available! server=" + server + ", port=" + port + ", client=" + mandt;
				log.error(eMsg);
				e = new IPCException(eMsg);
				request.setAttribute(UIConstants.Request.Parameter.IPC_EXCEPTION, e);
				return (mapping.findForward("ipcFailure"));
			} else {
				conData = new IPCConnectionData(server, port, mandt, mode);
			}
		}

		try {
			// check if we have a IPC session id from previous connections
			ipcSessionID = (String) session.getAttribute(UIConstants.Request.Parameter.SESSION_ID);

			if (ipcSessionID == null) {
				// first call, need to create IPCClient and IPCSession
				TCPIPCItemReference itemReference = new TCPIPCItemReference(server, port, false, "UnicodeLittle", "", "", "");
				client = IPCClientObjectFactory.getInstance().newIPCClient(mandt, "ISA", itemReference);
				ipcSession = client.getIPCSession();
				// store the ipc session id in the http session
				ipcSessionID = ipcSession.getSessionId();
				session.setAttribute(UIConstants.Request.Parameter.SESSION_ID, ipcSessionID);
				// store the whole ipc client object in the session
				session.setAttribute("IPC_CLIENT_OBJ", client);
			} else {
				// we had already a ipc session, create ipc client from the session
				client = (IPCClient) session.getAttribute("IPC_CLIENT_OBJ");
				ipcSession = client.getIPCSession();
			}

			initPannel = (InitialiseConditionAnalyserPannel) session.getAttribute(InitialiseConditionAnalyserPannel.SessionAttributeName);
			if (initPannel == null) {
				// this is the first call, need to create a new instance of this
				initPannel = new InitialiseConditionAnalyserPannel();			
				// create the field objects and add them to the pannel
				applicationField = new SelectionField();
				applicationField.setFromToField(false);
				applicationField.setVarKeyField(false);
				applicationField.setDataElementName(DE_APPLICATION_FIELD_NAME);
				applicationField.setFieldName(InitialiseConditionAnalyserPannel.FormFieldNameApplication);
				applicationField.setFieldLabel( applicationField.getFieldName());
				applicationField.setFieldValue(application);
				// convert the field content to upper case
				applicationField.setConvertToUpperCase(true);
				initPannel.setApplicationField(applicationField);

				usageField = new SelectionField();
				usageField.setFromToField(false);
				usageField.setVarKeyField(false);
				usageField.setDataElementName(DE_USAGE_FIELD_NAME);
				usageField.setFieldName(InitialiseConditionAnalyserPannel.FormFieldNameUsage);
				usageField.setFieldLabel(usageField.getFieldName());
				usageField.setFieldValue(usage);
				// convert the field content to upper case
				usageField.setConvertToUpperCase(true);				
				initPannel.setUsageField(usageField);

				maintGroupField = new SelectionField();
				maintGroupField.setFromToField(false);
				maintGroupField.setVarKeyField(false);
				maintGroupField.setDataElementName(DE_MAINT_GROUP_NAME);
				maintGroupField.setFieldName(InitialiseConditionAnalyserPannel.FormFieldNameCondgroup);
				maintGroupField.setFieldLabel(maintGroupField.getFieldName());
				maintGroupField.setFieldValue(maintGroup);
				// convert the field content to upper case
				maintGroupField.setConvertToUpperCase(true);				
				initPannel.setMaintGroupField(maintGroupField);
				
				// now get the field label texts for the fields
				// use the data element names to create the command parameters
				dataElementNames = new String[3];
				dataElementNames[0]= applicationField.getDataElementName();
				dataElementNames[1]= usageField.getDataElementName();
				dataElementNames[2]= maintGroupField.getDataElementName();
				
				// check if the selection screen is used or if selection is done via GUD
				if ( paramName == null && 
				     paramValue == null )
				     {
				    // Selection will be done via the selection screen 	
					// fire the ipc command to get the texts for the screen
					// application field flag is not recognised when working with data elements
					fieldTextResult = client.getConditionTableFieldLabel(usage,application,null,dataElementNames,true);
					if ( fieldTextResult != null){
						// get the label for the application input field
						fieldTextArray = (String[]) fieldTextResult.get(applicationField.getDataElementName());
						if (fieldTextArray != null) {
							applicationField.setFieldLabel(fieldTextArray[ GetConditionGroupfld.MEDIUM_TEXT_POSITION]);
						}
						// get the label for the usage input field
						fieldTextArray = (String[]) fieldTextResult.get(usageField.getDataElementName());
						if (fieldTextArray != null) {
							usageField.setFieldLabel(fieldTextArray[ GetConditionGroupfld.MEDIUM_TEXT_POSITION]);
						}
						// get the label for the maintenance gorup input field
						fieldTextArray = (String[]) fieldTextResult.get(maintGroupField.getDataElementName());
						if (fieldTextArray != null) {
							maintGroupField.setFieldLabel(fieldTextArray[ GetConditionGroupfld.MEDIUM_TEXT_POSITION]);
						}
					}	
				}
				else {
					// Selection will be made via the given GUIDs
					initPannel.setParamName(paramName);
					initPannel.setParamValue(paramValue);			
				}
			}
			else {
				// init pannel already exists from previous connection
				// make sure the latest values are stored in the HTTP session
				initPannel.setParamName(paramName);
				initPannel.setParamValue(paramValue);
				if (application.length() > 0)
					initPannel.setApplication(application);
				if ( maintGroup.length() > 0)					
					initPannel.setSelectedConditionGroup(maintGroup);
				if ( usage.length() > 0)	
					initPannel.setUsage(usage);					
			}

			// store values in the http session
			session.setAttribute(InitialiseConditionAnalyserPannel.SessionAttributeName, initPannel);
			session.setAttribute(IPCConnectionData.SessionAttributeName, conData);

			// Create a model calls reference

			ConditionMastDataDisplayModel cMDDM = new ConditionMastDataDisplayModel();
			// bind reference to the session.              
			session.setAttribute("CONDMASTDATADISMODEL", cMDDM);
			if ( !( ipcSession == null && client == null) ){
			 if ( ipcSession == null)  {
				cMDDM.setIPCSession(client.getIPCSession());
			 } else {
				cMDDM.setIPCSession(ipcSession);
			 }
						 
			 cMDDM.setApplication(initPannel.getApplication());   
			 cMDDM.setConditionGroupName(initPannel.getSelectedConditionGroup());
			 cMDDM.setUsage(initPannel.getUsage());          	
			}			

			// make sure to clean up old selection screen information
			// to force a new selection of maintGroupAttribute fields
			session.setAttribute(InitialiseConditionSelectionPannel.SessionAttributeName, null);
			
			// init the language support if the parameters ar included inside of the URL
			initLanguageSupport( request, session);

			if (log.isDebugEnabled())
				servlet.log(" init of condition master data display finished ");

            // Is seletion via selection screen or given GUD wanted ?
            if ( paramName == null && paramValue == null ){
            	// this path will trigger the selection screen
				return mapping.findForward("success");            	
            }
            else {            
            	// this path will start selection via given GUID
				return mapping.findForward("GuidSelectionSuccess");
            }
		} catch (com.sap.spc.remote.client.object.IPCException e) {
			request.setAttribute(UIConstants.Request.Parameter.IPC_EXCEPTION, e);
			return (mapping.findForward("ipcFailure"));
		}

	}
	
/*
 *  initLanguageSupport
 *  Created an instance of UserSessionData and stores this object inside the HTTP session
 *  Read the parameters "language" and "country" from the HTTP request. Creates a new Locale
 *  object based on this values and store the Locale instance inside of the UserSessionData object.
 *  The language information from the UserSessionData object will be used by the TAG <isa:translate>
 *  to read the corresponding resource file to display the text in the requested language. 	  
 */	
	public static void initLanguageSupport( HttpServletRequest argHttpServletRequest, HttpSession argHttpSession ) 
	{
		if ( argHttpServletRequest != null &&
		     argHttpSession != null){
		     
			// create a user session data object which is directly stored in the http session
			UserSessionData userSessionData = UserSessionData.createUserSessionData(argHttpSession);

			// determin the language
			String language = argHttpServletRequest.getParameter(Constants.LANGUAGE);

			if ((language != null) &&
				(language.trim().length() > 0) ) {
				// determin the country
				String country = argHttpServletRequest.getParameter(Constants.COUNTRY);

				if ((country == null) ||
					(country.trim().length() == 0) ) {
						country = "";
					}
				if( log.isDebugEnabled())
					log.debug("Setting language to " + language+ "country to " + country );

				userSessionData.setLocale(new Locale(language.toLowerCase(), country.toUpperCase()));
			}
		}		
	}

}