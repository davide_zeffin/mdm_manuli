/*******************************************************************************

    Copyright (c) 2005 by SAP AG

    All rights to both implementation and design are reserved.

    Use and copying of this software and preparation of derivative works based
    upon this software are not permitted.

    Distribution of this software is restricted. SAP does not make any warranty
    about the software, its performance or its conformity to any specification.

*******************************************************************************/
package com.sap.ipc.webui.conditionmasterdata.action;

// import java.beans.StaticFieldsPersistenceDelegate;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspFactory;
import javax.servlet.jsp.PageContext;

import com.sap.ipc.webui.conditionmasterdata.model.IPCConnectionData;
import com.sap.ipc.webui.conditionmasterdata.model.InitialiseConditionAnalyserPannel;
import com.sap.ipc.webui.conditionmasterdata.model.InitialiseConditionSelectionPannel;
import com.sap.ipc.webui.conditionmasterdata.model.SelectionField;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import com.sap.isa.core.Constants;
import com.sap.isa.core.InitAction;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.StartupParameter;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCSession;
import com.sap.spc.remote.client.object.imp.IPCClientObjectFactory;
import com.sap.spc.remote.client.object.imp.tcp.TCPIPCItemReference;
import com.sap.ipc.webui.conditionmasterdata.model.*;

import com.sap.ipc.webui.pricing.UIConstants;

/**
 * @author d043319
 */
public class GetConditionGroupfld extends Action { /* (non-Javadoc)
* @see org.apache.struts.action.Action#perform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
*/

	/**
	 * Constructor for ProcessFormAction.
	 */
	public GetConditionGroupfld() {
		super();
	}
	
	protected static int EXTRA_SHORT_TEXT_POSITION = 0;
	protected static int SHORT_TEXT_POSITION = 1; 
	protected static int MEDIUM_TEXT_POSITION = 2;
	protected static int LONG_TEXT_POSITION = 3;		
	
	private static String FN_CREATED_BY = "CREATED_BY";	 
	private static String FN_CREATED_ON = "CREATED_ON";
	private static String FN_CONDITION_TYPE= "KSCHL";
	private static String FN_CONDITION_TABLE = "KOTABNR";			
	public  static String FN_VALID_FROM = "TIMESTAMP_FROM";		
	public  static String FN_VALID_TO   = "TIMESTAMP_TO";		
	
	private static String DN_CREATED_BY = "/SAPCND/CREATED_BY";	
	private static String DN_CREATED_ON = "/SAPCND/CREATED_ON";
	private static String DN_CONDITION_TYPE= "/SAPCND/COND_TYPE";
	private static String DN_CONDITION_TABLE = "/SAPCND/COND_TABLE_ID";			
	private static String DN_VALID_FROM = "/SAPCND/TIMESTAMP_FROM";		
	private static String DN_VALID_TO   = "/SAPCND/TIMESTAMP_TO";
	
	private static final IsaLocation log = IsaLocation.getInstance(InitAction.class.getName());

	//	private IPCClientObjectFactory factory =
	//		IPCClientObjectFactory.getInstance();

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		IPCConnectionData conData;
		HttpSession session;
		SelectionField selField;
		InitialiseConditionSelectionPannel conSelPannel;
		InitialiseConditionAnalyserPannel initPannel;
		String mandt;
		String[] rawSelectionFields;	
		String[] staticFieldDataElements;
		String ipcSessionID;
		IPCSession ipcSession;
		IPCClient client;
		TCPIPCItemReference itemReference;
		Hashtable fieldTexts = null;
		Hashtable dataElementTexts = null;	
		Hashtable dataElementFields = null;	
		String[] fieldTextArray = null;
		ConditionMastDataDisplayModel cMDDM;
		boolean isFromToField = false;

		session = request.getSession();
		// Extract attributes we will need
		// parameter application
		String application = request.getParameter(InitialiseConditionAnalyserPannel.FormFieldNameApplication);
		// parameter usage
		String usage = request.getParameter(InitialiseConditionAnalyserPannel.FormFieldNameUsage);
		// parameter maintenance group
		String maintGroup = request.getParameter(InitialiseConditionAnalyserPannel.FormFieldNameCondgroup);

		// check if there is already data from the init pannel in the session
		initPannel = (InitialiseConditionAnalyserPannel) session.getAttribute(InitialiseConditionAnalyserPannel.SessionAttributeName);
		if (initPannel == null) {
			// no information of the init pannel so fare, need to create one
			initPannel = new InitialiseConditionAnalyserPannel();
		}
		// store the values of the first selection screen in the session		
		initPannel.setApplication(application);
		initPannel.setUsage(usage);
		initPannel.setSelectedConditionGroup(maintGroup);
		session.setAttribute(InitialiseConditionAnalyserPannel.SessionAttributeName, initPannel);
		
		// upper case convertion is done via the field objects inside the initPannel
		application = initPannel.getApplication();
		usage = initPannel.getUsage();
		maintGroup = initPannel.getSelectedConditionGroup();
		
		// put these into the cMDDM object
		
		cMDDM = (ConditionMastDataDisplayModel) session.getAttribute("CONDMASTDATADISMODEL");
		
		cMDDM.setApplication(initPannel.getApplication());   
		cMDDM.setConditionGroupName(initPannel.getSelectedConditionGroup());
		cMDDM.setUsage(initPannel.getUsage());          			

		// check if the needed input parameters are here
		if (application == null || usage == null || maintGroup == null) {
			// error, one of the needed input parameters is not available
			String eMsg;
			IPCException e;     	     	

			eMsg = "Needed value is missing. application=" + application +
								", usage=" + usage +
								", group=" + maintGroup;

		   log.error(eMsg);
		   e = new IPCException(eMsg);		   
		   request.setAttribute(UIConstants.Request.Parameter.IPC_EXCEPTION, e);						   
		   return (mapping.findForward("ipcFailure"));	
		}

		// get the IPC connection data from the session
		conData = (IPCConnectionData) session.getAttribute(IPCConnectionData.SessionAttributeName);

		if (conData == null) {
			// here we have a problem because the session did not contain the ipc connection data
			// try to extract the ipc connection data from the request
			// Extract attributes we will need
			// parameter server
			String server = request.getParameter(UIConstants.Request.Parameter.SERVER);
			// parameter port
			String port = request.getParameter(UIConstants.Request.Parameter.PORT);
			// parameter client
			mandt = request.getParameter(UIConstants.Request.Parameter.CLIENT);
			// parameter mode
			String mode = request.getParameter(UIConstants.Request.Parameter.MODE);

			if (server == null || port == null || mandt == null) {
				// now we are really in trouble, no way to connect to the IPC
				String eMsg;
				IPCException e;        	     	
				// the connection data was not in the session nor in the request
				// so we have to stop here !!!        	     
				eMsg = "No IPC server, port or client information available! server=" + server +
						  ", port=" + port + ", client=" + mandt ;
			   log.error(eMsg);
			   e = new IPCException(eMsg);		   
			   request.setAttribute(UIConstants.Request.Parameter.IPC_EXCEPTION, e);						   
			   return (mapping.findForward("ipcFailure")); 
			} else {
				// store the connection data in the session
				conData = new IPCConnectionData(server, port, mandt, mode);
				session.setAttribute(IPCConnectionData.SessionAttributeName, conData);
			}
		}
		try {
			// check if there is already an object of ConditionSelectionPannel in the session
			// from previous calls
			conSelPannel = (InitialiseConditionSelectionPannel) session.getAttribute(InitialiseConditionSelectionPannel.SessionAttributeName);
			if (conSelPannel == null) {
				// There is no conSelPannel in the session, so this is the first time the page is called
				// or a new selection of maintenance group was made 

				// check if we have a IPC session id from previous connections
				ipcSessionID = (String) session.getAttribute(UIConstants.Request.Parameter.SESSION_ID);

				itemReference = new TCPIPCItemReference(conData.getHost(), conData.getPort(), false, "UnicodeLittle", "", "", "");
				if (ipcSessionID == null) {
					// first call, need to create IPCClient and IPCSession
					client = IPCClientObjectFactory.getInstance().newIPCClient(conData.getClient(), "ISA", itemReference);
					ipcSession = client.getIPCSession();
					cMDDM.setIPCSession(ipcSession);
					// store the ipc session id in the http session
					ipcSessionID = ipcSession.getSessionId();
					session.setAttribute(UIConstants.Request.Parameter.SESSION_ID, ipcSessionID);
					// store the whole ipc lcient object in the session
					session.setAttribute("IPC_CLIENT_OBJ", client);
				} else {
					// we had already a ipc session, create ipc client from the session
					client = (IPCClient) session.getAttribute("IPC_CLIENT_OBJ");
				}
				
				// create a new object to store the content of the selection screen;
				conSelPannel = new InitialiseConditionSelectionPannel();
				
				// add the field for the max. number of hits ( selection max )
				conSelPannel.setMaxRecordsToSelect( getMaxSelectionField());				
				
				// get the list of static fields (dataelements) of the selection screen to the list
				dataElementFields = new Hashtable(6);
				staticFieldDataElements = addStaticSelectionFieldsFromFieldcatalogue( dataElementFields );
				// application field flag is not recognised when working with data element names
				dataElementTexts = client.getConditionTableFieldLabel( usage, application, null, staticFieldDataElements, true);
				// now assign the text labels to the static selection fields
				if ( staticFieldDataElements != null ){
					for ( int loop = 0; loop < staticFieldDataElements.length; loop ++){
						// reset the text array
						fieldTextArray = null;
						if (dataElementTexts != null){						
							// get the corresponding texts
							fieldTextArray = (String[]) dataElementTexts.get(staticFieldDataElements[loop]);
						}
						// get the selection field object from the hashtable
						selField = (SelectionField) dataElementFields.get(staticFieldDataElements[loop]);
						if ( selField != null ){
							if ( fieldTextArray != null ){
								// use the long text as field label
								selField.setFieldLabel( fieldTextArray[LONG_TEXT_POSITION]);
							}
							else {					
								// if we do not have text labels, use the field name as label
								selField.setFieldLabel(selField.getFieldName());
							}
							selField.setFromToField(false);
							selField.setVarKeyField(false);
							conSelPannel.addField(selField);
						}						
					}
				}
				
				
				// get the list of attrbutes which are associated to the selected maintenance group
				rawSelectionFields = client.getConditionGroupAttributes(initPannel.getUsage(), initPannel.getSelectedConditionGroup());		
				
				// if we do not get any fields for the condition maintenance group, the group name is properbly wrong
				if (rawSelectionFields == null){
					String eMsg;
                
					MessageResources messages = getResources(); 
					eMsg = messages.getMessage("cmdd.SelInitForm.ErrMsg1");                      
                      
					eMsg = eMsg + " application=" + application +
										", usage=" + usage +
										", group=" + maintGroup;

                    // add error message to the page context
					ActionErrors errors = new ActionErrors();
					errors.add(ActionErrors.GLOBAL_ERROR,
								new ActionError("prc.error.template", eMsg ));
					saveErrors(request, errors);
					// show the initial page again with an error message
					return mapping.findForward("re_display");			
				}
				
				if (log.isDebugEnabled()) {				
					servlet.log(" query selection fields for condition master data display finished ");
				} 				
				
				// convert internal fields to external fields
				// assumption : only application fields availalbe
				Hashtable externalFieldNames = client.convertFieldnameInternalToExternal(initPannel.getUsage(), initPannel.getApplication(), rawSelectionFields, true);	
				
				// get the texts for the fields
				if (rawSelectionFields != null &&
				    rawSelectionFields.length > 0 &&
				    externalFieldNames != null  ) {
                        String [] extFieldArray = checkAndConvertExternalFieldNames( externalFieldNames, rawSelectionFields.length);
				    	// now get the text labels for the external field names
						// assumption : only application fields availalbe				    	
				    	fieldTexts = client.getConditionTableFieldLabel(usage, application, extFieldArray, null, true);
				}

				// loop for all attribute fields and add them to the selection screen
				if (rawSelectionFields != null) {
					for (int loop = 0; loop < rawSelectionFields.length; loop++) {	
						// create a new selection fields
						selField = new SelectionField();
						selField.setFieldType(SelectionField.fieldTypeString);
						selField.setFieldName(rawSelectionFields[loop]);
						
						// set the external field name
						selField.setExtFieldName( (String) externalFieldNames.get(rawSelectionFields[loop]) );
						
						// reset the text array
						fieldTextArray = null;
						if (fieldTexts != null && 
						    selField.getExtFieldName() != null ){						
							// get the corresponding texts
							fieldTextArray = (String[]) fieldTexts.get(selField.getExtFieldName());
						}						
						
						if ( fieldTextArray != null ){
							// use the long text as field label
							selField.setFieldLabel( fieldTextArray[LONG_TEXT_POSITION]);
						}
						else {					
							// if we do not have text labels, use the field name as label
							selField.setFieldLabel(selField.getFieldName());
						}
						// check if this field should support range selection ( from/to )
						isFromToField = SelectionFieldRangeSupportChecker.isFieldSupportingRangeSelection( selField.getRealFieldName() );
						selField.setFromToField(isFromToField);
						selField.setVarKeyField(true);
						conSelPannel.addField(selField);
					}
				}

				// append the list of selection fields to the session
				session.setAttribute(InitialiseConditionSelectionPannel.SessionAttributeName, conSelPannel);
			
				// add startup parameter to user session which is needed by the calendar popup
				StartupParameter startupParameter = new StartupParameter();			
				UserSessionData userSessionData = (UserSessionData) session.getAttribute(SessionConst.USER_DATA);
				userSessionData.setAttribute(SessionConst.STARTUP_PARAMETER, startupParameter);
				startupParameter.addParameter(Constants.ACCESSIBILITY,"",false);


				if (log.isDebugEnabled())
					servlet.log(" creating selection screen for condition master data display finished "); 
			}
			
			return mapping.findForward("success");
		} catch (com.sap.spc.remote.client.object.IPCException e) {
			request.setAttribute(UIConstants.Request.Parameter.IPC_EXCEPTION, e);
			return (mapping.findForward("ipcFailure"));
		}
	}


	private String[] addStaticSelectionFieldsFromFieldcatalogue( Hashtable argDataElementFields ) {
		String[] completeSelectionField = null;
		String[] staticSelectionFields       = { FN_VALID_FROM, 
			                                     FN_VALID_TO, 
			                                     FN_CREATED_BY, 
			                                     FN_CREATED_ON, 
			                                     FN_CONDITION_TYPE , 
			                                 //    FN_CONDITION_TABLE    // KOTABNR is currently not supported as selection field
			                                   };
		String[] staticSelectionDataElements = { DN_VALID_FROM, 
			                                     DN_VALID_TO, 
			                                     DN_CREATED_BY, 
			                                     DN_CREATED_ON, 
			                                     DN_CONDITION_TYPE, 
			                                 //    DN_CONDITION_TABLE    // KOTABNR is currently not supported as selection field
			                                     };
		
		int loopLimit = 0;
		int numberOfStaticSelectionFields;
		SelectionField selField = null;
		
        if ( argDataElementFields != null){        
			completeSelectionField = new String[staticSelectionFields.length];
			// add the static data element to the result list
	    	loopLimit = staticSelectionFields.length;
			for ( int loop = 0; loop < loopLimit; loop ++ ){
				// create an instance of selection field class for each static selection field
				selField = new SelectionField();
				selField.setFieldName(staticSelectionFields[loop]);
				selField.setDataElementName(staticSelectionDataElements[loop]);
				// convert the field content to upper case
				selField.setConvertToUpperCase(true);	
				// check if this is a date field
				if ( selField.getFieldName().equalsIgnoreCase(FN_VALID_FROM) || 
				     selField.getFieldName().equalsIgnoreCase(FN_VALID_TO) ||
				     selField.getFieldName().equalsIgnoreCase(FN_CREATED_ON) ){
					 // this static field is one of the date fields
					 selField.setFieldType(SelectionField.fieldTypeDate);								
				}
				else {
					// all other static fields are string input fields
					selField.setFieldType(SelectionField.fieldTypeString);					
				}
				// append the fields to the hashtable, use the dataelement name as key
				argDataElementFields.put(selField.getDataElementName(),selField);				
				// add the name of the datalement to the result array
				completeSelectionField[loop] = staticSelectionDataElements[loop];
			}		
        }
		return completeSelectionField;		
	}
	
/*
 *  Check for each internal fieldname in the hashtable the number of external field names
 *  If there are more then one external field names, they will be concatenated to one large string
 *  The string array entries in the hash table will be overwritten with the single string
 *  A result String array will be created and returned
 */	
	private String[] checkAndConvertExternalFieldNames ( Hashtable argExternalFieldNames, int argArraySize){
		String[] resultExtFieldNamesArray = new String[argArraySize];
		String[] extFieldNameArray = null;
		String   extFieldName = null;
		String   intFieldName = null;
		int ArrayPosition = 0;
		Enumeration keys = null;
		
		if ( argExternalFieldNames != null ){		
			keys = argExternalFieldNames.keys();   				    	
			// loop for all entries of the hash table with external field names
			while (keys.hasMoreElements()){
				intFieldName = (String) keys.nextElement();
				extFieldNameArray = (String[]) argExternalFieldNames.get(intFieldName);
				// check how many external field names we have for the internal field
				if ( extFieldNameArray.length > 1){
					// we have more than one ext field name
					// check if this is a knowen ext field so that we can make a selection
					if (intFieldName.equalsIgnoreCase("PRODUCT"))
					    extFieldName = "PRODUCT_ID";
					else {
						// concatenate all field names to one long string
						extFieldName = extFieldNameArray[0];
						for (int aLoop = 1; aLoop < extFieldNameArray.length; aLoop ++){
							extFieldName = "_" + extFieldNameArray[aLoop];    
						}
					}
				}
				else {
					// only one ext. fieldname for the int fieldname
					extFieldName = extFieldNameArray[0];
				}
				// overwrite the hash table with the only used ext field name
				argExternalFieldNames.put(intFieldName, extFieldName);
				// create a temp array of the external field names
				resultExtFieldNamesArray[ArrayPosition] = extFieldName; 
				ArrayPosition ++;
			}
		}		
		return resultExtFieldNamesArray;
	}

/*
 *  Creates a SelectionField object for the Max. condition records input field
 *  The Field will be pre-Filled with a given value
 */	
  private SelectionField getMaxSelectionField()
  {
  	SelectionField maxSel = new SelectionField();
	MessageResources messages = getResources(); 
	String fieldLabel = messages.getMessage("cmdd.SelInitForm.maxRecSelect");       	
  	maxSel.setFieldName(InitialiseConditionSelectionPannel.MaxSelectionFieldName);
  	maxSel.setFieldType(SelectionField.fieldTypeInt);
  	maxSel.setFromToField(false);
  	maxSel.setVarKeyField(false);
  	maxSel.setFieldLabel(fieldLabel);
  	maxSel.setFieldValue(InitialiseConditionSelectionPannel.MaxSelectionFieldValue);  	
  	
  	return maxSel;	
  }

}
