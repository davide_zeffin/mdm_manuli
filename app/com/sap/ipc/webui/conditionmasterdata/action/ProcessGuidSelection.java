/*
 * Created on Jul 21, 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.ipc.webui.conditionmasterdata.action;


import java.io.IOException;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import com.sap.ipc.webui.conditionmasterdata.model.ConditionMastDataDisplayModel;
import com.sap.ipc.webui.conditionmasterdata.model.InitialiseConditionSelectionPannel;
import com.sap.ipc.webui.conditionmasterdata.model.ProcessSelectionConversion;

import com.sap.ipc.webui.conditionmasterdata.model.InitialiseConditionAnalyserPannel;
import com.sap.isa.core.InitAction;
import com.sap.isa.core.logging.IsaLocation;

/**
 * @author I026516
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ProcessGuidSelection extends Action {

	private static final IsaLocation log = IsaLocation.getInstance(InitAction.class.getName());	 

	public ActionForward perform(ActionMapping mapping, ActionForm form,
	                             HttpServletRequest  request,HttpServletResponse response)
			throws IOException, ServletException {
				
		MessageResources messages = getResources();
		HttpSession session;
		session = request.getSession();
		Vector paramNameVector = new Vector();
		Vector paramValueVector = new Vector();
		String [] parameterNames = new String[1];
		String [] parameterValues = new String[1];
		
		// parameter for getting parameter names
		String paramName = request.getParameter(InitialiseConditionAnalyserPannel.URLParamName);				
		// parameter for getting parameter values
		String paramValue = request.getParameter(InitialiseConditionAnalyserPannel.URLParamValue);
		
		ConditionMastDataDisplayModel cMDDM;
		cMDDM = (ConditionMastDataDisplayModel)session.getAttribute("CONDMASTDATADISMODEL");
		
		if (cMDDM.getApplication() == null){
			servlet.log("Application name is missing");
			response.sendError(HttpServletResponse.SC_BAD_REQUEST,
								messages.getMessage("cmdd.CondShow.appMissing"));
									
			return (mapping.findForward("ipcFailure")); 				
		}else if(cMDDM.getApplication().equals("")){
			servlet.log("Application name is missing");
			response.sendError(HttpServletResponse.SC_BAD_REQUEST,
								messages.getMessage("cmdd.CondShow.appMissing"));
									
			return (mapping.findForward("ipcFailure")); 			
		}
		
		if (cMDDM.getUsage() == null){
			servlet.log("Usage name is missing");
			response.sendError(HttpServletResponse.SC_BAD_REQUEST,
								messages.getMessage("cmdd.CondShow.usageMissing"));
									
			return (mapping.findForward("ipcFailure")); 				
		}else if (cMDDM.getUsage().equals("") ){
			servlet.log("Usage name is missing");
			response.sendError(HttpServletResponse.SC_BAD_REQUEST,
								messages.getMessage("cmdd.CondShow.usageMissing"));
									
			return (mapping.findForward("ipcFailure")); 				
		}
		
/*		if (cMDDM.getConditionGroupName() == null){
			servlet.log("Condition Maintenance Group is missing");
			response.sendError(HttpServletResponse.SC_BAD_REQUEST,
								messages.getMessage("cmdd.CondShow.condMaintGrpMissing"));
									
			return (mapping.findForward("ipcFailure")); 				
		}
*/		
		int selectionFieldSize = 0;
		String parameterName, parameterValue;
		int indexOfSeperator;
		boolean refTypeOrGuidPassed = false;
		//this boolean flag will inform us that there are other parameters also if url contains parameters
		//REFTYP,REFGUID. if url contains parameters REFTYP and REFGUID, only allowed parameters with these
		//two parameters are VALID_FROM and VALID_TO and this boolean flag otherParameters will give the
		//required information to do this check.
		boolean otherParameters = false;
		
		if (paramName == null || paramValue == null){
			servlet.log("parameter values are not defined for some parameter names");
			response.sendError(HttpServletResponse.SC_BAD_REQUEST,
								messages.getMessage("cmdd.CondShow.prNameValueMisMatc"));
									
			return (mapping.findForward("ipcFailure")); 						
		}
		
		/*paramName string will contain parameter names in CSV format. same is 
		  true for paramValue. paramValue string will contain corressponding
		  parameter values in CSV format. take out all the parameter names and
		  parameter values and store the same in parameterNames and parameterValues
		  array.
		 */
		if (paramName != null){
			
			while(paramName.length() > 0){
				
				indexOfSeperator = paramName.indexOf(",");
				
				if (indexOfSeperator > -1){
					parameterName = paramName.substring(0,indexOfSeperator);
					paramName = paramName.substring(indexOfSeperator+1);
					
					indexOfSeperator = paramValue.indexOf(",");
					
					if (indexOfSeperator > -1){
						parameterValue = paramValue.substring(0,indexOfSeperator);
						paramValue = paramValue.substring(indexOfSeperator+1);
					} else{
						servlet.log("parameter values are not defined for some parameter names");
						response.sendError(HttpServletResponse.SC_BAD_REQUEST,
											messages.getMessage("cmdd.CondShow.prNameValueMisMatc"));
									
						return (mapping.findForward("ipcFailure")); 						
					}
					
				} else {
					parameterName = paramName;
					parameterValue = paramValue;
					
					if (parameterName.equalsIgnoreCase("VALID_FROM")){
						cMDDM.setValidFrom(parameterValue);
					}else if (parameterName.equalsIgnoreCase("VALID_TO")){
						cMDDM.setValidTo(parameterValue);
					}else if (parameterName.equalsIgnoreCase("REFTYP")){
						refTypeOrGuidPassed = true;
						cMDDM.setRefTyp(parameterValue);
					}else if (parameterName.equalsIgnoreCase("REFGUID")){
						refTypeOrGuidPassed = true;
						cMDDM.setRefGuid(parameterValue);
					}else{
						otherParameters = true;
						paramNameVector.add(parameterName);
						paramValueVector.add(parameterValue);
					}
									
					break;
				}
				
				if (parameterName.equalsIgnoreCase("VALID_FROM")){
					cMDDM.setValidFrom(parameterValue);
				}else if (parameterName.equalsIgnoreCase("VALID_TO")){
					cMDDM.setValidTo(parameterValue);
				}else if (parameterName.equalsIgnoreCase("REFTYP")){
					refTypeOrGuidPassed = true;
					cMDDM.setRefTyp(parameterValue);
				}else if (parameterName.equalsIgnoreCase("REFGUID")){
					refTypeOrGuidPassed = true;
					cMDDM.setRefGuid(parameterValue);
				}else{
					otherParameters = true;
					paramNameVector.add(parameterName);
					paramValueVector.add(parameterValue);
				}
			}
			
			if (refTypeOrGuidPassed == true && otherParameters == true){
				servlet.log("Illegal parameters");
				response.sendError(HttpServletResponse.SC_BAD_REQUEST,
									messages.getMessage("cmdd.CondShow.illegalParameters"));
									
				return (mapping.findForward("ipcFailure")); 			
			}
			
			if (refTypeOrGuidPassed == false && otherParameters == true){
				if (cMDDM.getConditionGroupName() == null){
					servlet.log("Condition Maintenance Group is missing");
					response.sendError(HttpServletResponse.SC_BAD_REQUEST,
										messages.getMessage("cmdd.CondShow.condMaintGrpMissin"));
									
					return (mapping.findForward("ipcFailure")); 				
				}else if (cMDDM.getConditionGroupName() == null){
					servlet.log("Condition Maintenance Group is missing");
					response.sendError(HttpServletResponse.SC_BAD_REQUEST,
										messages.getMessage("cmdd.CondShow.condMaintGrpMissin"));
									
					return (mapping.findForward("ipcFailure")); 				
				}
			}
			
			parameterNames = (String [])paramNameVector.toArray(parameterNames);
			parameterValues = (String [])paramValueVector.toArray(parameterValues);
			
		} else{
			servlet.log("Parameter names are Missing ");
			response.sendError(HttpServletResponse.SC_BAD_REQUEST,
								messages.getMessage("cmdd.CondShow.noParamName"));
									
			return (mapping.findForward("ipcFailure")); 			
		}
		
		         	 
		cMDDM.setParamNames(parameterNames);
		cMDDM.setParamValues(parameterValues); 
		
		// there is no way to specify a max. number of hits during selection by GUID
		// so use the default value
		cMDDM.setMaxNumberOfHits(InitialiseConditionSelectionPannel.MaxSelectionFieldValue);
		
		if (log.isDebugEnabled())
			servlet.log(" Selection via GUID for condition master data display will start now ");		
		
		ProcessSelectionConversion convPro = new ProcessSelectionConversion(cMDDM, null);
		convPro.convertToInt();

		if (log.isDebugEnabled())
			servlet.log(" Selection via GUID for condition master data display is finished ");	
		
		return  mapping.findForward("success");
	}	
}
