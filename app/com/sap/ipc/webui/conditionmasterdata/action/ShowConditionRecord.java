/*
 * Created on Jul 21, 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.ipc.webui.conditionmasterdata.action;


import java.io.IOException;
import java.util.HashMap;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import com.sap.ipc.webui.conditionmasterdata.model.ConditionRecord;
import com.sap.ipc.webui.conditionmasterdata.model.ConvertConditionRecordsIntoExt;
import com.sap.ipc.webui.conditionmasterdata.model.ConditionMastDataDisplayModel;
import com.sap.ipc.webui.conditionmasterdata.model.InitialiseConditionSelectionPannel;
import com.sap.isa.core.InitAction;
import com.sap.isa.core.logging.IsaLocation;

/**
 * @author I026516
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ShowConditionRecord extends Action {
 
	private static final IsaLocation log = IsaLocation.getInstance(InitAction.class.getName()); 
 
	public ActionForward perform(ActionMapping mapping, ActionForm form,
	                             HttpServletRequest  request,HttpServletResponse response)
			throws IOException, ServletException {

			// Extract attributes we will need
			MessageResources messages = getResources();
			HttpSession session = request.getSession();
		
			HashMap conditiondRecordsExtHashMap = null;
			ConvertConditionRecordsIntoExt convertConditionRecordsIntoExt = null;
			Vector conditionRecords = null;
			ConditionMastDataDisplayModel cMDDM = null;
			ConditionRecord [] conditionRecordForDisplay = null;
			String cancelButton = null;
		
			// check if the user has pressed the cancel button on the previous JSP
			cancelButton = request.getParameter("org.apache.struts.taglib.html.CANCEL");
			if ( cancelButton != null ){
				// user pressed cancel button, so forward to cancel jsp
				// action will end here
				return mapping.findForward("cancel");			
			}		
		
			cMDDM = (ConditionMastDataDisplayModel)session.getAttribute("CONDMASTDATADISMODEL");
			
			if (cMDDM == null){
				servlet.log("Model object is Missing ");
				response.sendError(HttpServletResponse.SC_BAD_REQUEST,
									messages.getMessage("cmdd.CondShow.noCMDDM"));
									
				return (mapping.findForward("ipcFailure")); 
				
			} else{
				conditiondRecordsExtHashMap = cMDDM.getConditionRecExt(); 

			}

		    /* if error messages is null and conditiondRecordsExtHashMap is null, it means
		     * condition record object is missing. But it might be possible that error messages
		     * may not be null and in that case user should see the error messages on the screen. 
		     */ 
		    
			/*if (conditiondRecordsExtHashMap == null && cMDDM.getErrorMessages() == null){
				servlet.log("Condition Record object is missing ");
				response.sendError(HttpServletResponse.SC_BAD_REQUEST,
									messages.getMessage("cmdd.CondShow.noHashMap"));
				return (mapping.findForward("ipcFailure")); 
				
			}*/
			
			if (conditiondRecordsExtHashMap != null && cMDDM.getErrorMessages() == null){
				convertConditionRecordsIntoExt = new ConvertConditionRecordsIntoExt();
				
				InitialiseConditionSelectionPannel selectPannel;

				selectPannel= (InitialiseConditionSelectionPannel)session.getAttribute(InitialiseConditionSelectionPannel.SessionAttributeName);
				
				if (log.isDebugEnabled())
					servlet.log(" creating blueprint structure for displaying by condition master data display starts now ");
				
				Vector condRecBluePrintStruc = convertConditionRecordsIntoExt.getConditionRecordBluePrintStructure(selectPannel,cMDDM);
				
				if (log.isDebugEnabled())
					servlet.log(" creating blueprint structure for displaying by condition master data display finished ");
				
				String [] conditionRecordBluePrintStruc = new String[0];
				conditionRecordBluePrintStruc = (String [])condRecBluePrintStruc.toArray(conditionRecordBluePrintStruc);
								
				//session.setAttribute("ConditionRecordBluePrintStruct",condRecBluePrintStruc.iterator());\
				session.setAttribute("ConditionRecordBluePrintStruct",conditionRecordBluePrintStruc);
				
				//setting session attribute if its display by guid selection in order to disable back button
				if (cMDDM.getParamNames() != null)
					session.setAttribute("DisplayBackButton","false");
				else
					session.setAttribute("DisplayBackButton","true");
				  
				if (log.isDebugEnabled())
					servlet.log(" converting internal values of condition record selection result to external values for displaying by condition master data display starts now ");
				
				conditionRecordForDisplay = convertConditionRecordsIntoExt.getConditionRecords(conditiondRecordsExtHashMap, cMDDM);
				
				if (log.isDebugEnabled())
					servlet.log(" converting internal values of condition record selection result to external values for displaying by condition master data display finished ");
				
				session.setAttribute("ConditionRecords",conditionRecordForDisplay);
			} else {
				conditionRecordForDisplay = new ConditionRecord[1];
				
				ConditionRecord conditionRec = new ConditionRecord();
				conditionRec.setNumberOfRecordReturned(null);
				conditionRecordForDisplay[0] = conditionRec;
				session.setAttribute("ConditionRecords",conditionRecordForDisplay);
				
				if (cMDDM.getParamNames() != null)
					session.setAttribute("DisplayBackButton","false");
				else
					session.setAttribute("DisplayBackButton","true");	
					
				String [] conditionRecordBluePrintStruc = new String[1];
				conditionRecordBluePrintStruc[0] = "No Blueprint Structure";
				session.setAttribute("ConditionRecordBluePrintStruct",conditionRecordBluePrintStruc);
			}
			
		
			String [] errorMessages = new String[1];
			errorMessages = cMDDM.getErrorMessages();
				
			if (errorMessages == null){
				errorMessages = new String[1];
				errorMessages[0] = "No Errors";
			}

			//check for error messages
			session.setAttribute("ErrorMessages",errorMessages);
			
		if (log.isDebugEnabled())
			servlet.log(" condition record selection result prepared for displaying by condition master data display. Handover to JSP starts now ");
			
	    //setting the no of records selected in session
	    session.setAttribute("NoOfRecordsSelected", new Integer(cMDDM.getNumOfRecSelected()));
				 
		return  mapping.findForward("success");
	}	
}
