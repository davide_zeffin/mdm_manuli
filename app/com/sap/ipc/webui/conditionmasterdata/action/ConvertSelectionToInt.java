/*******************************************************************************

    Copyright (c) 2005 by SAP AG

    All rights to both implementation and design are reserved.

    Use and copying of this software and preparation of derivative works based
    upon this software are not permitted.

    Distribution of this software is restricted. SAP does not make any warranty
    about the software, its performance or its conformity to any specification.

*******************************************************************************/
package com.sap.ipc.webui.conditionmasterdata.action;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sap.ipc.webui.conditionmasterdata.model.InitialiseConditionSelectionPannel;
import com.sap.ipc.webui.conditionmasterdata.model.SelectionField;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import com.sap.ipc.webui.conditionmasterdata.model.*;
import com.sap.ipc.webui.pricing.UIConstants;
import com.sap.isa.core.InitAction;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.spc.remote.client.object.IPCException;

/**
 * @author d043319
 */
public class ConvertSelectionToInt extends Action{	/* (non-Javadoc)
	 * @see org.apache.struts.action.Action#perform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	 
	private static final IsaLocation log = IsaLocation.getInstance(InitAction.class.getName());	 
	private String dateFormat_YYYYMMDD  = "yyyy.MM.dd";
	private SimpleDateFormat simpDateFormat = new SimpleDateFormat(dateFormat_YYYYMMDD);
	 
	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		HttpSession session;
		SelectionField selField;
		String cancelButton = null ;
		String clearQueryButton = null;		
		InitialiseConditionSelectionPannel conSelPannel;
		session = request.getSession();
		ConditionMastDataDisplayModel cMDDM;
		int numberOfFields = 0;
		SelectionField[] arrayOfSelFields = null; 
		boolean isOneSelectionCriteriaAvailalbe = false;
		Integer iMaxNumberOfHits = null;
		String validFromFieldValue = null;
		String validToFieldValue = null;
		

		// check if the user has pressed the cancel button on the previous JSP
		cancelButton = request.getParameter(InitialiseConditionSelectionPannel.FormButtonCancel);
		if ( cancelButton != null ){
			// user pressed cancel button, so forward to cancel jsp
			// action will end here
			return mapping.findForward("cancel");			
		}
		
		// check if the user has pressed the clear query button on the previous JSP
		clearQueryButton = request.getParameter(InitialiseConditionSelectionPannel.FormButtonClearQuery);
		if ( clearQueryButton != null ){
			// user pressed clear query button, so forward to clear path
			conSelPannel = (InitialiseConditionSelectionPannel) session.getAttribute(InitialiseConditionSelectionPannel.SessionAttributeName);
			if ( conSelPannel != null )
			{
				// loop for all entries in the selection pannel and set a blank value
				conSelPannel.resetEnumeration();
				while ( conSelPannel.hasMoreElements())
				{ 
					// get the meta data of the selection field
					selField = ( SelectionField ) conSelPannel.nextElement();
					if ( selField.isFromToField() )
					{
						// this field has two field entries, ( FROM / TO )
						selField.setFieldValueFrom("");
						selField.setFieldValueTo  ("");					
					}
					else
					{
						// this field has only one entry
						selField.setFieldValue("");					
					}					
				}
				conSelPannel.resetEnumeration();	
				
				// reset the max. hit field to default value
				selField = conSelPannel.getMaxRecordsToSelect(); 
				if ( selField != null )
				   selField.setFieldValue(InitialiseConditionSelectionPannel.MaxSelectionFieldValue);			
			}
			// action will end here
			return mapping.findForward("clear_query");			
		}		
		
		conSelPannel = (InitialiseConditionSelectionPannel) session.getAttribute(InitialiseConditionSelectionPannel.SessionAttributeName);
		// get the field values from the request based on the names
		// which are stored in the conSelPannel instance
		if ( conSelPannel != null )
		{
			// loop for all entries in the selection pannel
			conSelPannel.resetEnumeration();
			while ( conSelPannel.hasMoreElements())
			{
				// get the meta data of the selection field
				selField = ( SelectionField ) conSelPannel.nextElement();
				if ( selField.isFromToField() )
				{
					// this field has two field entries, ( FROM / TO )
					selField.setFieldValueFrom( request.getParameter( selField.getFieldName() ));
					selField.setFieldValueTo  ( request.getParameter( selField.getFieldNameTo() ));	
					// check that the TO field contains only values if the FROM field is also filled
					if ( selField.getFieldValue() != null && 
					     selField.getFieldValueTo() != null  &&
					     selField.getFieldValue().length() == 0 &&
					     selField.getFieldValueTo().length() > 0 ){
					     // user has entered only a value inside the TO field, not in the FROM field
						 // MSG : Invalid input for a selection range
						 String eMsg;                
						 MessageResources messages = getResources(); 
						 eMsg = messages.getMessage("cmdd.SelInitForm.noFromVal");                  
						 // add error message to the page context
						 ActionErrors errors = new ActionErrors();
						 errors.add(ActionErrors.GLOBAL_ERROR,
									 new ActionError("prc.error.template", eMsg ));
						 saveErrors(request, errors);
						 // go back to the selection screen
						 return mapping.findForward("clear_query");
					     }
				}
				else
				{
					// this field has only one entry
					selField.setFieldValue( request.getParameter( selField.getFieldName()) );					
				}
				// check if the user has entered some values
				if ( ( selField.getFieldValue() != null  &&
				       selField.getFieldValue().length() > 0 ) ||
			 	     ( selField.getFieldValueTo() != null  &&
					   selField.getFieldValueTo().length() > 0 ) ){
					   	// there is an input field which contains entered values
					   	// check that this is not the "valid from" / "valid to " field
					   	if ( (!(selField.getFieldName().equalsIgnoreCase( GetConditionGroupfld.FN_VALID_FROM ))) && 
						     (!(selField.getFieldName().equalsIgnoreCase( GetConditionGroupfld.FN_VALID_TO ))) ){
						    // one of the real selection fields is filled, so we can run the query
					   		isOneSelectionCriteriaAvailalbe = true;
						     }
					   }
					   
				// check if this is a date field 
				if ( (selField.getFieldType() != null) && 
				     (selField.getFieldType().equalsIgnoreCase(SelectionField.fieldTypeDate)) &&
				     (selField.getFieldValue() != null) &&
				     (selField.getFieldValue().length() > 0 )				     
				      ){
				     // check if the date string format is 'YYYY.MM.DD'
				     if ( isDateFormatOK(selField.getFieldValue()) == false ) {
				     	// error, the entered date string is not correct
						// MSG : Invalid value for a date field <string>
						String eMsg;                
						MessageResources messages = getResources(); 
						eMsg = messages.getMessage("cmdd.SelInitForm.badDate");
						eMsg = eMsg + " : " + selField.getFieldValue();                  
						// add error message to the page context
						ActionErrors errors = new ActionErrors();
						errors.add(ActionErrors.GLOBAL_ERROR,
									new ActionError("prc.error.template", eMsg ));
						saveErrors(request, errors);
						// go back to the selection screen
						return mapping.findForward("clear_query");				     		 	   
				     }
				}
				
                // store the valid from and valid to date fields for later range check
                if ( GetConditionGroupfld.FN_VALID_TO.equalsIgnoreCase(selField.getFieldName()) ){
                	validToFieldValue = selField.getFieldValue();
                }
                else if ( GetConditionGroupfld.FN_VALID_FROM.equalsIgnoreCase(selField.getFieldName()) ){
				validFromFieldValue = selField.getFieldValue();
			}
				
				numberOfFields ++;
			} 
			
			// check if valid from - valid to range is corret
			if ( isFromDateToDateRangeValid(validFromFieldValue, validToFieldValue) == false ){
				// error, invalid from to date rang
				// MSG : Invalid date range specified by 'valid from' and 'valid to' fields <string>, <string>
				String eMsg;                
				MessageResources messages = getResources(); 
				eMsg = messages.getMessage("cmdd.SelInitForm.badDateRng");
				eMsg = eMsg + " : " + validFromFieldValue + ", " + validToFieldValue;                  
				// add error message to the page context
				ActionErrors errors = new ActionErrors();
				errors.add(ActionErrors.GLOBAL_ERROR,
							new ActionError("prc.error.template", eMsg ));
				saveErrors(request, errors);
				// go back to the selection screen
				return mapping.findForward("clear_query");					
			}
			
			// get the max. hit selection field value
			selField = conSelPannel.getMaxRecordsToSelect();
			if ( selField != null ){
				selField.setFieldValue( request.getParameter( selField.getFieldName()) );
			}
			
			// check if we have at lease one input field wich contains some values
			if (!isOneSelectionCriteriaAvailalbe){
				// nothing enterey by the user, return to the selection screen
				// processing will end there
				String eMsg;                
				MessageResources messages = getResources(); 
				eMsg = messages.getMessage("cmdd.SelInitForm.noSelCrit");                  
				// add error message to the page context
				ActionErrors errors = new ActionErrors();
				errors.add(ActionErrors.GLOBAL_ERROR,
							new ActionError("prc.error.template", eMsg ));
				saveErrors(request, errors);
				// go back to the selection screen
				return mapping.findForward("clear_query");					
			}
			
			// check if the value for max. number of hits is an integer
			try {			
				selField = conSelPannel.getMaxRecordsToSelect();
				String maxNumberOfHits = selField.getFieldValue();				
				// try to convert to integer
				iMaxNumberOfHits = Integer.valueOf(maxNumberOfHits); 
			}
			catch ( Throwable ex ){
				// conversion to interger failed
				iMaxNumberOfHits = null;
			} 
			// check if the number is bigger than 0
			if ( iMaxNumberOfHits != null &&
			     (! (iMaxNumberOfHits.intValue() > 0)  ) ) {
				// bad value for iMaxNumber, reset to null to rais the error message
				iMaxNumberOfHits = null;
			}
			if (iMaxNumberOfHits == null){
				// bad value has been enterey by the user for max. number of hits , return to the selection screen
				// processing will end there
				String eMsg;                
				MessageResources messages = getResources(); 
				eMsg = messages.getMessage("cmdd.SelInitForm.badMaxHitNo");                  
				// add error message to the page context
				ActionErrors errors = new ActionErrors();
				errors.add(ActionErrors.GLOBAL_ERROR,
							new ActionError("prc.error.template", eMsg ));
				saveErrors(request, errors);
				// go back to the selection screen
				return mapping.findForward("clear_query");					
			}			
			
			arrayOfSelFields = new SelectionField[numberOfFields];
			numberOfFields = 0;
			
			conSelPannel.resetEnumeration();
			while ( conSelPannel.hasMoreElements())
			{
				// get the meta data of the selection field
				selField = ( SelectionField ) conSelPannel.nextElement();			
			    arrayOfSelFields[numberOfFields] = selField;
			    numberOfFields ++;
			}
			
			conSelPannel.resetEnumeration();			
			 cMDDM = (ConditionMastDataDisplayModel) session.getAttribute("CONDMASTDATADISMODEL");
			// set the max. number of hits
			cMDDM.setMaxNumberOfHits( conSelPannel.getMaxRecordsToSelect().getFieldValue() );
			// reset the valid from and valid to field to force the underlaying layer to
			// take the new values
			cMDDM.setValidFrom("");
			cMDDM.setValidTo("");
			
			if (log.isDebugEnabled())
				servlet.log(" Converting external selection value to internal values for condition master data display  and record selection will start now "); 			
			
			// now we need to hand over the values to the convertion method
			ProcessSelectionConversion convPro = new ProcessSelectionConversion(cMDDM , arrayOfSelFields );
			convPro.convertToInt();
		}			
		else
		{
			// error  , no selection object in the session	
			String eMsg;
			IPCException e;
			// the selection object could not be found in the session
			// so we have to stop here !!!        	     
			eMsg = "No selection object was found in the session. Please restart application";
			log.error(eMsg);
			e = new IPCException(eMsg);
			request.setAttribute(UIConstants.Request.Parameter.IPC_EXCEPTION, e);
			return (mapping.findForward("ipcFailure"));		
		}
		
		if (log.isDebugEnabled())
			servlet.log(" Converting external selection value to internal values for condition master data display and record selection finished "); 			
				
		return mapping.findForward("success");		
	}

	
	private boolean isDateFormatOK ( String argDateString ){
		Date parseResult;
		String parseResultString;
		boolean retVal = false;

        if ( argDateString != null ){
			try{
				parseResult = simpDateFormat.parse(argDateString);
			}
			catch ( Throwable th){
				// error during date parsing
				parseResult = null;
			}

			if (parseResult != null) {
				// parsing was OK, now compare the result of the parsing
				// this must be donw because Month 13 is transformed to 'January'
				// and does not result in an exception
				parseResultString = simpDateFormat.format(parseResult);
				if ( argDateString.equalsIgnoreCase(parseResultString) ){
					// date string is OK
					retVal = true;
				}
	   		}
       }
       return retVal;			
	}
	
	private boolean isFromDateToDateRangeValid ( String argFromDate, String argToDate ){
		boolean retVal = false;
		String internalFromDate;
		String internalToDate;
		
		if ( argFromDate != null ){
			internalFromDate = argFromDate;
		}
		else {
			internalFromDate = "";
		}
		
		if ( argToDate != null ){
			internalToDate = argToDate;
		}
		else {
			internalToDate = "";
		}
		
		if ( (internalFromDate.length()== 0) &&
		      (internalToDate.length() == 0 ) ){ 
				// no from date and to date is specified, this is valid in any case
				// because dafault from date is 01.01.0001 and to date is 31.12.9999
				retVal = true;		      	
		      }
		
		else if ( (internalFromDate.length() != 0 ) && 
		          (internalToDate.length() == 0 ) ) {
		      // only the from date is specified, this is valid in any case
		      // because dafault to date is 31.12.9999
		      retVal = true;	 
		      }
		      
		else if ( (internalFromDate.length() == 0) && 
			      (internalToDate.length() != 0 ) ) {
			  // only the to date is specified, this is valid in any case
			  // because dafault from date is 01.01.0001
			  retVal = true;	 
			  }		      
		
		else if ( (internalFromDate.length()!= 0) &&
		          (internalToDate.length() != 0)   ){
		    // both dates are given, now check the range		     
			try {		
				Date fromDate = simpDateFormat.parse(internalFromDate);
				Date toDate = simpDateFormat.parse(internalToDate);
		
				if ( fromDate.before(toDate) ){
				// result was OK, fromDate is before toDate
				retVal = true;	
				}
				else {
					if ( fromDate.after(toDate) ) {
					// result was not OK, fromDate is not before toDate
					retVal = false;					
					}
					else {
						// result was OK, fromDate is equal to ToDate
						// this is allowed
						retVal = true;
					}			
				}
			}
			catch ( Throwable th ){
				// something went wrong during the date parsing
				// so in any case, the check failed and the date values needs to be corrected
				retVal = false;
			}	
		}	
		return retVal;
	}

}
