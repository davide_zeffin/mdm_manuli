package com.sap.isa.crt;

/**
 * <p>Call Manager provides a managed runtime for invoking calls
 *              This allows for monitoring of calls (logging, security) and provide
 *              services like asynchronous calls, timeout, retries, recovery, failure
 *              recording etc..
 *
 *              Concrete Call Manager implementation provides the value added services
 *              for Calls
 *
 *              Two types of basic calls are supported:
 * 				<ul>
 *              <li>Synchronous Calls (executed with blocked calling thread)</li>
 *              <li>Asynchronous Calls (executed wout blocking calling thread)</li>
 * 				<ul>
 *
 * </p>
 *
 * Copyright:   SAP Labs Palo Alto, LLC - All rights reserved
 * Company: SAP Labs Palo Alto, LLC
 * @author
 * @version 1.0
 */

public interface CallManager {

    public static final String PROP_CALL_MANAGER_CLASS = "callmanager.crt.isa.sap.com";
    public static final String PROP_CALL_MANAGER_NAME = "name.crt.isa.sap.com";
    
    /**
     * Default is the name of the class
	 * @return String name of CallManager
	 */
	public String getName();
    
    /**
	 * @param name name of Call Manager
	 */
	public void setName(String name);
    

    /**
     * <p>Invokes the Call synchronously</p>
     * @param Call call to invoke
     * @param asyncMode true => call is invoked asynchronously (Result can be type casted to AsyncResult)
     * 					false => call is invoked synchronously		
     * @return Result result
     */
    public Result invoke(Call call, boolean asyncMode);
    

	/**
	 * @param callChain call chain to invoke
	 * @param asyncMode 
	 * @return
	 */    
    public ResultChain invoke(CallChain callChain, boolean asyncMode);
}