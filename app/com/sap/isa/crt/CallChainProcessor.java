/*
 * Created on Nov 17, 2003
 *
 */
package com.sap.isa.crt;

import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;

/**
 * @author I802891
 *
 */
public class CallChainProcessor extends CallProcessor {
	private CallChain chain = null;
	private boolean executing = false;	
	private int index = 0; // starting point of call chain
	private CallProcessor callProcessor;
	
	Location tracer = Location.getLocation(CallChainProcessor.class.getName());

	CallChainProcessor(CallChain chain) {
		this(chain,0);
	}

	CallChainProcessor(CallChain chain, int startIndex) {
		super(null);
		this.chain = chain;
		if(index < 0 || index > chain.getSize()) {
			throw new IllegalArgumentException("start index is invalid");
		}
		this.index = startIndex;
		callProcessor = new CallProcessor(getNextCall());
	}
	
	CallChain getCallChain() {
		return chain;
	}
	
	void setCallChain(CallChain chain) {
		this.chain = chain;
	}
	
	/**
	 * @return
	 */
	public int getNextCallIndex() {
		return index;
	}
	
	
	Call getNextCall() {
		CallChain chain = getCallChain();
		if(index < chain.getSize())
			return chain.getCall(index);
		return null;
	}
	
	ResultStatusEnum getStatus() {
		if(done()) {
			CallChain chain = getCallChain();
			if(chain.isCompleted()) {
				return ResultStatusEnum.PREMATURE_TERMINATION;
			}
			else if(exception != null) {
				return ResultStatusEnum.FAILURE;
			}
			else {
				return ResultStatusEnum.SUCCESS;
			}
		}
		else {
			return ResultStatusEnum.EXECUTING;
		}
	}
	
	boolean done() {
		CallChain chain = getCallChain();
		return exception != null || index >= chain.getSize() || chain.isCompleted();
	}
	
	/**
	 * Actual call processing time sanning Queueing delay in Pool
	 */
	long getCallTime() {
		return callProcessor.getCallTime();
	}

	/*CallProcessor getCallProcessor() {
		return callProcessor;
	}*/
	
	/**
	 * This run method handles the logic of Call processing
	 */
	public void run () {
		Category logger = (Category)ctxt.getLogger();
		if(done()) return;
		CallChain chain = getCallChain();
		Call call = getNextCall();
		setCall(call);
		callProcessor = new CallProcessor(getNextCall());
		
		try {
			synchronized(this) {
				if(executing) throw new ConcurrentExecException("Call Chain is already executing a step");
				executing = true;
			}
			callProcessor.setCallContext(ctxt);
			if(logger != null && logger.beInfo()) {
				logger.infoT(tracer,"Executing call chain " + chain.getName() + " step " + index +  " : " + call.getName());
			}
			callProcessor.run();
			if(logger != null && logger.beInfo()) {
				logger.infoT(tracer,"Executed call chain " + chain.getName() + " step " + index +  " : " + call.getName());
			}
			index++;
		}
		catch(RuntimeException rtex) {
			if(logger != null && logger.beInfo()) {			
				logger.infoT(tracer,"Call chain terminated due to error," + rtex.getMessage());
			}
			// stop processing the call chain
			this.exception = rtex;		
		}
		finally {
			executing = false;
		}
	}
	
	/* (non-Javadoc)
	 * @see com.sap.isa.crt.CallProcessor#getResult()
	 */
	Object getResult() {
		return callProcessor.getResult();
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.crt.CallProcessor#getException()
	 */
	Exception getException() {
		return callProcessor.getException();
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.crt.CallProcessor#getStartTime()
	 */
	long getStartTime() {
		return callProcessor.getStartTime();
	}
}
