package com.sap.isa.crt;

/**
 * Description: <p>This class wraps the Result of invocation of a call</p>
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

class SyncResult implements Result {
	protected long startTime;
	protected Call call;
	protected Object result;
	protected long executionTime;
	protected Exception th;
	
	private ResultChain resultChain;
	
	SyncResult() {
	}

    SyncResult(CallProcessor processor) {
        this.startTime = processor.getStartTime();
        this.call = processor.getCall();
        this.th = processor.getException();
        this.result = processor.getResult();
        this.executionTime = processor.getCallTime();
    }
    
	SyncResult(ResultChain rchain, CallProcessor call) {
		this(call);
		this.resultChain = rchain;
	}

    /**
     * For synchronous calls this method will always return true
     * @return boolean true if the call has completed
     */
    public boolean isCompleted() {
        return true;
    }

    /**
     * @return Call invoked call
     */
    public Call getCall() {
        return call;
    }

    /**
     * If call execution has completed and succeeded, it returns the result
     * @return Object result of invoked call.
     */
    public Object getResult() {
        return result;
    }
    
    /**
     * @return long millis taken to execute the Call
     */
    public long getExecutionTime() {
        return executionTime;
    }

    /**
     * If call execution has completed and failed, it returns the exception
     * @return Throwable if the call invocation fails, the exception is returned
     */
    public java.lang.Exception getException() {
        return th;
    }
    
	/* (non-Javadoc)
	 * @see com.sap.isa.crt.Result#isSuccess()
	 */
	public ResultStatusEnum getStatus() {
		if(getException() != null) {
			return ResultStatusEnum.FAILURE;
		}
		else {
			return ResultStatusEnum.SUCCESS;
		}
	}
	/* (non-Javadoc)
	 * @see com.sap.isa.crt.Result#getResultChain()
	 */
	public ResultChain getResultChain() {
		return resultChain;
	}

	public void setResultChain(ResultChain rchain) {
		resultChain = rchain;
	}
	
	/* (non-Javadoc)
	 * @see com.sap.isa.crt.Result#getStartTime()
	 */
	public long getStartTime() {
		return startTime;
	}
}