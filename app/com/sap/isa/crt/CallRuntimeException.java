package com.sap.isa.crt;

/**
 * This exception is thrown as wrapper exception for all the checked exceptions
 * inside invoke method of Call
 * 
 * @see com.sap.isa.crt.Call#invoke()
 * 
 *
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class CallRuntimeException extends RuntimeException {

    private Throwable th;

    public CallRuntimeException(String msg) {
        super(msg);
    }

    public CallRuntimeException(Throwable nested) {
        this.th = nested;
    }

    public Throwable getNestedThrowable() {
        return th;
    }
}