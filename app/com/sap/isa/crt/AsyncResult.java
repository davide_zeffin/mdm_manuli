package com.sap.isa.crt;

/**
 * Description: <p>Asynchronous Calls return immediately with this result object</p>
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public interface AsyncResult extends Result {

    /**
     * This provides a blocking method for the thread to block till the AsyncCall
     * has completed
     */
    public void waitForCompletion();

    /**
     * Actual Execution Time
     * @return long millis taken to execute the Call
     *          -1 if the call execution has not started
     */
    public long getActualExecutionTime();

    /**
     * @see AsyncCallback
     */
    public void addCallback(AsyncCallback cb);

    public void removeCallback(AsyncCallback cb);

}