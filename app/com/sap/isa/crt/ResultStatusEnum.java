/*
 * Created on Dec 3, 2003
 *
 */
package com.sap.isa.crt;

/**
 * @author I802891
 *
 */
public final class ResultStatusEnum {
	/** These values are mapped to database, DO NOT CHANGE*/
	private static final short STATUS_FAILURE = 0;
	private static final short STATUS_SUCCESS = 1;
	private static final short STATUS_EXECUTING = 2;
	private static final short STATUS_NOT_EXECUTED = 3;
	private static final short STATUS_PREMATURE_TERMINATION = 4;
	
	
	public static final ResultStatusEnum SUCCESS = new ResultStatusEnum(STATUS_SUCCESS,"SUCCESS");
	public static final ResultStatusEnum FAILURE = new ResultStatusEnum(STATUS_FAILURE,"FAILURE");
	public static final ResultStatusEnum EXECUTING = new ResultStatusEnum(STATUS_EXECUTING,"EXECUTING");
	public static final ResultStatusEnum NOT_EXECUTED = new ResultStatusEnum(STATUS_NOT_EXECUTED,"UNKNOWN");	
	public static final ResultStatusEnum PREMATURE_TERMINATION = new ResultStatusEnum(STATUS_PREMATURE_TERMINATION,"PREMATURE_TERMINATION");
	
	private short val;
	private String name;
	
	public String toString() {
		return "[Status = " + name;
	}
	
	public static ResultStatusEnum toEnum(short val) {
		switch(val) {
			case 0: return FAILURE;
			case 1: return SUCCESS;
			case 2: return EXECUTING;
			case 3: return NOT_EXECUTED;
			case 4:; return PREMATURE_TERMINATION;
			default: throw new IllegalArgumentException(val + " is invalid");
		}
	}
	
	public short value() {
		return val;
	}
	
	public String name() {
		return name;
	}
	
	private ResultStatusEnum(short val, String name) {
		this.val = val;
		this.name = name;	
	}
}
