/*
 * Created on Sep 3, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.crt;

import javax.jdo.PersistenceManagerFactory;
import java.util.Properties;

/**
 * @author I802891
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class RecordingCallManagerFactory extends CallManagerFactory {

	/**
	 * <p>
	 * This property allows not to record Successful calls. 
	 * This helps in keeping Recordings manageable
	 * Default is TRUE.
	 * </p>
	 */
	public static final String PROP_RECORD_CALL_SUCCESS = "recordCallSuccess";
	
	private PersistentCallManager pcm = null;
	private PersistenceManagerFactory pmf = null;
	
	/**
	 * <p>
	 * This is the PersistenceManagerFactory used  to obtain datastore connection
	 * This can only be set once during configuration of CallManagerFactory
	 * </p>
	 * @param pmf
	 */
	public void setJDOPMF(PersistenceManagerFactory argPMF) {
		if(this.pmf != null) {
			// TO DO
		}
		this.pmf = argPMF;
	}

	public PersistenceManagerFactory getJDOPMF() {
		return pmf;
	}
	
	/* (non-Javadoc)
	 * @see com.sap.isa.crt.CallManagerFactory#getInstance(java.lang.ClassLoader, java.util.Properties)
	 */
	public synchronized CallManager getInstance(ClassLoader cl, Properties props) {
		checkInitialized();
		if(this.pmf == null) {
			throw new UserException("setJDOPMF not called");
		}
		if(pcm == null) {
			pcm = new PersistentCallManager(this,cl,props == null ? this.props : props);
			pcm.setLogger(logger);
		}
		return pcm;
	}
	
	/* (non-Javadoc)
	 * @see com.sap.isa.crt.CallManagerFactory#releaseInstance(com.sap.isa.crt.CallManager)
	 */
	public void releaseInstance(CallManager cm) {
		// do nothing singleton Manager
	}
	

	public void close() {
		pmf = null;
		if(pcm != null) pcm.close();
		pcm = null;
		super.close();
	}
}
