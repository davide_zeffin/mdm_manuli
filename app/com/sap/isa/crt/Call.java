package com.sap.isa.crt;

/**
 * Description: <p>Basic Call interface for calls made to CallManager
 *              To provide a generic mechanism of handling calls uniformly
 *              type safely is lost. To overcome this components can insure
 *              typesafety by providing typesafe methods in interfaces
 *              e.g.
 *              <code>
 *              AsyncResult beginTimeConsumingAsyncCall(int typesafeArg1, long typesafeArg2,...);
 *              int endTimeConsumingAsyncCall(AsyncResult result);
 *              </code>
 *
 *              Call semantics & granularity is defined by the Application
 *              </p>
 *
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public interface Call {
	/**
	 * @return call chain, if this call is part of the call chain
	 */
	public CallChain getCallChain();
	
	/**
	 * optional
	 * @return String name of the call
	 */
	public String getName();

    /**
     * @param name name of the call argument
     * @return Object value
     */
    public Object getParam(String name);

    /**
     * @return String[] call param names in order from left to right
     */
    public String[] getParamNames();

    /**
     * @param millis timeout millis for call ( 0 => infinite wait)
     */
    public void setTimeout(long millis);

    /**
     * @return millis timeout millis for call ( 0 => infinite wait)
     */
    public long getTimeout();

    /**
     * @param int count max number of retries for a call to be made
     */
    public void setRetryCount(short count);

    /**
     * @return int no of retries for a call to me made
     */
    public short getRetryCount();

	/**
	 * <p>
	 * This method is used by CallManager to attach a unique id to each call
	 * executed with CallManager. Applications can use this to retreive execution
	 * information from CallManager at a later point in time e.g. if a call failed
	 * what was the exception that was raised, what were the call parameters etc
	 * <p>
	 * If the same call instance is executed again after last execution, it is assigned
	 * a new call id
	 * </p>
	 * 
	 * This feature is optional for Call Manager to support
	 * </p>
	 * @param id
	 */
	public void	setCallId(Object id);

	/**
	 * Applications should use Object.toString() to save Id in persistent form 
	 * CallManager
	 * @return
	 */
	public Object getCallId();
	
    /**     
     * @param CallContext ctxt of the invoking call
     *
     * @return Object return value or null
     *
     * @throws Exception a checked exception can be thrown
     */
    public Object invoke(CallContext ctxt) throws Exception;
}