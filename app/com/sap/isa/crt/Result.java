package com.sap.isa.crt;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author i802891
 * @version 1.0
 */

public interface Result {
	/**
	 * @return Result Chain if this result of a Call Chain invocation
	 */
	public ResultChain getResultChain();
	
    /**
     * For synchronous calls this method will always return true
     * @return boolean true if the call has completed
     */
    public boolean isCompleted();

	/**
	 * Indicates whether call completed with Success of failure
	 * @return true => Success
	 * 			false => failure
	 */
	public ResultStatusEnum getStatus();

    /**
     * @return Call invoked call
     */
    public Call getCall();

	/**
	 * @return time epoch when the call execution started
	 */
	public long getStartTime();
	
    /**
     * @return long millis taken to execute the Call
     */
    public long getExecutionTime();

    /**
     * If call execution has completed and succeeded, it returns the result
     * @return Object result of invoked call.
     */
    public Object getResult();
    
    /**
     * If call execution has completed and failed, it returns the exception
     * @return Throwable if the call invocation fails, the exception is returned
     */
    public java.lang.Exception getException();
}