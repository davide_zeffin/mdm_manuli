/*
 * Created on Nov 25, 2003
 *
 */
package com.sap.isa.crt;

import java.util.Collection;
import java.util.Date;

/**
 * @author I802891
 *
 */
public interface RecordingCallManager extends CallManager {
	/**
	 * @param since
	 * @return
	 */
	public Collection getExecutedCalls(Date since);
	
	/**
	 * @param Date since optional (null is allowed)
	 * @return Collection non modificable collection of failed calls
	 */
	public Collection getFailedCalls(Date since);
	
	/**
	 * Check in memory first and then retreive from datastore, if required
	 * <p> Retreives call based on request id</p>
	 * @param id
	 * @return
	 */
	public Call getCallById(String id);
	
	/**
	 * <p>
	 * Since calls are recorded by Persistence Manager it is possible to retreive
	 * the results of all the executed calls at any point in time whether they failed
	 * or succeeded. This feature is especially helpful to get the exception stacktrace
	 * for failed calls
	 * </p>
	 * @param call
	 * @return
	 */
	public Result invocationResult(Call call);
	
	public ResultChain invocationResult(CallChain chain);
}
