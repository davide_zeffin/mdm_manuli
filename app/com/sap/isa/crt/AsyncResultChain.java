/*
 * Created on Dec 3, 2003
 *
 */
package com.sap.isa.crt;

/**
 * @author I802891
 *
 */
public interface AsyncResultChain extends ResultChain, AsyncResult {

}
