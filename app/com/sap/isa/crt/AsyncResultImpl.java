/*
 * Created on Dec 3, 2003
 *
 */
package com.sap.isa.crt;

import java.util.Iterator;
import java.util.LinkedList;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.thread.ThreadControl;

/**
 * @author I802891
 *
 */
public final class AsyncResultImpl extends SyncResult implements AsyncResult {
	private transient CallProcessor processor;	
	/** Control of executing thread */
	private transient ThreadControl ctlr;

	/** List of callbacks */
	private transient LinkedList callbacks;

	/** 
	  *  The IsaLocation of the current Action.
	  *  This will be instantiated during perform and is always present.
	  */
	 protected static IsaLocation log = IsaLocation.getInstance(AsyncResultImpl.class.getName());

	AsyncResultImpl(CallProcessor call, ThreadControl ctlr) {
		super(call);
		this.processor = call;
		this.ctlr = ctlr;
	}
    
	AsyncResultImpl(ResultChain rchain, CallProcessor call, ThreadControl ctlr) {
		super(rchain,call);
		this.ctlr = ctlr;
	}
	
	/**
	 * For anychronous calls this method will return true when the call has
	 * finished execution. Call excecution may fail or succeed. In both cases this
	 * method returns true.
	 * @return boolean true if the call has completed
	 */
	public boolean isCompleted() {
		return ctlr.isFinished();
	}

	/**
	 * This provides a blocking method for the thread to block till the AsyncCall
	 * has completed
	 */
	public void waitForCompletion() {
		if(!ctlr.isFinished()) {
			synchronized(this) {
				try {
					wait(); // thread will be notified of completion
				}
				catch(InterruptedException ex) {
					log.debug("Error ", ex);
				}
			}
		}
	}

	/**
	 * If call execution has completed and succeeded, it returns the result
	 * @return Object result of invoked call.
	 */
	public Object getResult() {
		if(ctlr.isFinished()) return super.getResult();
		return null;
	}

	/**
	 * Queuing delay + Actual Execution Time
	 * @return long millis taken to execute the Call
	 */
	public long getExecutionTime() {
		if(ctlr.isFinished()) return super.getExecutionTime(); 
		else return processor.getCallTime();
	}

	/**
	 * Actual Execution Time
	 * @return long millis taken to execute the Call
	 *          -1 if the call execution has not started
	 */
	public long getActualExecutionTime() {
		return getExecutionTime();
	}

	/**
	 * If call execution has completed and failed, it returns the exception
	 * @return Throwable if the call invocation fails, the exception is returned
	 */
	public java.lang.Exception getException() {
		if(ctlr.isFinished()) return super.getException();
		return null;
	}

	/**
	 * @see AsyncCallback
	 */
	public void addCallback(AsyncCallback cb) {
		if(callbacks == null) callbacks = new LinkedList();
		synchronized(callbacks) {
			if(!callbacks.contains(cb)) callbacks.add(cb);
		}
	}

	public void removeCallback(AsyncCallback cb) {
		if(callbacks != null) {
			synchronized(callbacks) {
				 callbacks.remove(cb);
			}
		}
	}

	/**
	 * This is called after the Call has been invoked by Call Manager
	 */
	void afterInvoke() {
		super.th = processor.getException();
		super.executionTime = processor.getCallTime();
		super.result = processor.getResult();
		super.startTime = processor.getStartTime();

		//clear the processor
		processor = null;
		
		/** notify all waiting threads*/
		synchronized(this) {
			this.notifyAll();
		}

		if(callbacks != null) {
			synchronized(callbacks) {
				Iterator it = callbacks.iterator();

				while(it.hasNext()) {
					AsyncCallback cb = (AsyncCallback)it.next();
					try {
						cb.completed(this);
					}
					catch(RuntimeException ex) {
						log.debug("Error ", ex);
					}
				}
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.crt.Result#isSuccess()
	 */
	public ResultStatusEnum getStatus() {
		// if execution is not finished there is no way to determine if the call
		// execution has been successful or not, return false until final status
		// is known 
		if(!ctlr.isFinished()) {
			return ResultStatusEnum.EXECUTING;
		}
		else {
			return super.getStatus();
		}
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.crt.Result#getStartTime()
	 */
	public long getStartTime() {
		if(ctlr.isFinished()) return super.getStartTime();
		else return processor.getStartTime();
	}
}
