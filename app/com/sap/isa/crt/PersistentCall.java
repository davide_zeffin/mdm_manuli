/*
 * Created on Sep 2, 2003
 *
 */
package com.sap.isa.crt;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.io.Serializable;
import java.util.Map;

/**
 * <p>
 * This is a JDO compliant persistent class for saving calls
 * This is an internal class of CRT
 * </p>
 * @author I802891
 *
 */
public class PersistentCall {
	private static final short TYPE_CALL = 0;
	private static final short TYPE_CALL_CHAIN = 1;
	
	/** Call chain specific*/	
	/** Call chain execution was prematurely terminated and should not be reexecuted*/
	private static final short STATUS_PREMATURE_TERMINATION = 4;
	
	private String id;
	private String callClassName;
	private String callName;
	private short retries;
	private long timeout;
	private Object params; // BLOB of params
	private long startTime;
	private long executionTime;
	private Object result;
	private Object error;
	private short status = ResultStatusEnum.NOT_EXECUTED.value(); 
	private short type = 0; // 0 => Call, 1 => CallChain
	private int index = -1; // index of call in the call chain
	
	private Set steps; // self relation
	private transient List indexedSteps; 

	/**
	 * JDO specs
	 */
	public PersistentCall() {
	}
	
	public static class Id implements java.io.Serializable {
		public String id;
		
		public Id() {
		}
		
		public Id(String id) {
			this.id = id;
		}
		
		public int hashCode() {
			return id.hashCode();
		}
		
		public boolean equals(Object obj) {
			if(!(obj instanceof Id)) {
				return false;
			}
			
			return ((Id)obj).id.equals(this.id);
		}
		
	}
	
	PersistentCall(AbstractCall call, Result callResult) {
		// copies into Persistent representation
		this.callClassName = call.getClass().getName();
		this.callName = call.getName();
		this.id = call.getCallId().toString();
		this.timeout = call.getTimeout();
		this.retries = call.getRetryCount();
		if(call.getParamMap() != null) {
			this.params = (Serializable)new HashMap(call.getParamMap());
		}
		
		if(call instanceof CallChain) {
			CallChain chain = (CallChain)call;
			type = TYPE_CALL_CHAIN;
		}
		else {
			type = TYPE_CALL;
		}
		
		if(callResult != null) {
			updateResult(callResult);
		}
	}
	
	void updateResult(Result callResult) {
		this.startTime = callResult.getStartTime();
		this.executionTime = callResult.getExecutionTime();
		
		status = callResult.getStatus().value();
		
		// result only saved for calls
		if(type == TYPE_CALL) {
			if(callResult.getException() != null)
				this.error = 
					(Serializable)new SerializableException(callResult.getException());
			else {
				this.error = null;			
			}
			this.result = callResult.getResult();			
		}
	}
	
	/**
	 * @return
	 */
	public String getCallName() {
		return callName;
	}

	/**
	 * @return
	 */
	public Map getParams() {
		return (Map)params;
	}

	/**
	 * @return
	 */
	public long getTimeout() {
		return timeout;
	}

	/**
	 * @return
	 */
	public short getRetries() {
		return retries;
	}

	/**
	 * @param string
	 */
	public void setCallName(String string) {
		callName = string;
	}

	/**
	 * @param string
	 */
	public void setParams(Map arg) {
		if(arg != null) {
			params = new HashMap(arg);
		}
		else {
			params = null;
		}
	}

	/**
	 * @param l
	 */
	public void setTimeout(long l) {
		timeout = l;
	}

	/**
	 * @param i
	 */
	public void setRetries(short i) {
		retries = i;
	}
	
	/**
	 * @return
	 */
	public Exception getError() {
		return (Exception)error;
	}

	/**
	 * @return
	 */
	public Object getResult() {
		return result;
	}

	/**
	 * @return
	 */
	public long getStartTime() {
		return startTime;
	}

	/**
	 * @return
	 */
	public long getExecutionTime() {
		return executionTime;
	}

	/**
	 * @param exception
	 */
	public void setError(Exception exception) {
		error = (Serializable)exception;
		if(exception != null) status = ResultStatusEnum.FAILURE.value();
		else status = ResultStatusEnum.SUCCESS.value();
	}

	/**
	 * @param object
	 */
	public void setResult(Object object) {
		result = object;
	}

	/**
	 * @param l
	 */
	public void setStartTime(long l) {
		startTime = l;
	}

	/**
	 * @param l
	 */
	public void setExecutionTime(long l) {
		executionTime = l;
	}

	/**
	 * @return
	 */
	public String getCallClassName() {
		return callClassName;
	}

	/**
	 * @param string
	 */
	public void setCallClassName(String string) {
		callClassName = string;
	}

	/**
	 * @return
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param string
	 */
	public void setId(String string) {
		id = string;
	}
	
	public boolean isCallChain() {
		return type == 1;
	}
	
	public void setCallSteps(Collection steps) {
		if(this.steps == null) this.steps = new HashSet();
		else this.steps.clear(); 
		this.steps.addAll(steps);
		this.indexedSteps = null;
	}

	public Collection getCallSteps() {
		if(indexedSteps == null && steps != null) {
			List list = new ArrayList(steps);
			Comparator comp = new Comparator() {
				public int compare(Object o1, Object o2) {
					PersistentCall po1 = (PersistentCall)o1;
					PersistentCall po2 = (PersistentCall)o2;
					return po1.getIndex() - po2.getIndex();
				}
				public boolean equals(Object o) {
					return true;
				}
				public int hashCode() {
					return id.hashCode();
				}
			};
			Collections.sort(list,comp);
			this.indexedSteps = list;
		}
		// steps are returned sorted by index of Call
		return indexedSteps;
	}

	/**
	 * 
	 */
	public int hashCode() {
		return id.hashCode();
	}
	
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		}
		else if (o == this) {
			return true;
		}
		else if (!(o instanceof PersistentCall)) {
			return false;
		}
		else {
			return id.equals(((PersistentCall)o).id);
		}
	}	
	
	/**
	 * Creates a new instance of the call by reflection
	 * into 
	 * @return
	 */
	Call newCall() throws Exception {
		Call call = null;
		Class clazz = Class.forName(callClassName);
		call = (Call)clazz.newInstance();
		return call;
	}
	
	/**
	 * @return
	 */
	public int getIndex() {
		return index;
	}

	/**
	 * @param i
	 */
	public void setIndex(int i) {
		index = i;
	}
	
	/**
	 * @return
	 */
	public short getStatus() {
		return status;
	}
}
