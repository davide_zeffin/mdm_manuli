package com.sap.isa.crt;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Date;
import java.util.Properties;

import javax.jdo.JDOException;
import javax.jdo.PersistenceManagerFactory;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import com.sap.tc.logging.Location;
import com.sap.isa.auction.util.JDOUtil;
import com.sap.isa.persistence.helpers.OIDGenerator;

/**
 * Description: <p>Persistent Call Manager records failed calls made to recover them
 *              either manually or automatically. All calls made to Recording
 *              Call Manager must have serializable params to replay these calls later
 *              for manual or automatic recovery.</p>
 * 				
 * 				Calls invoked by PersistentCallManager must satisfy following:
 * 				<ul>
 * 				<li>Must be a public class or public static if an internal class</li>
 * 				<li>Provide a public no arg ctor</li>
 * 				<li>Must extend from AbstractCall</li>
 * 				</ul>
 * 
 * 				Properties:
 * 				Ignore non serializable params (persisted as null) (crt.isa.sap.com)
 * 				Persist failed calls
 * 				Persist Sucessful calls			
 * 				
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class PersistentCallManager 
			extends SimpleCallManager 
			implements RecordingCallManager {

	private static Location tracer = Location.getLocation(PersistentCallManager.class.getName());
	private PersistenceManagerFactory pmf = null;
	private boolean recordCallSuccess = true;

	
    public PersistentCallManager(CallManagerFactory cmf, ClassLoader cl, Properties props) {
    	super(cmf,cl,props);
    	pmf = ((RecordingCallManagerFactory)super.cmf).getJDOPMF();
		checkDatabaseSchema();
		if(props != null) {
			String val = props.getProperty(RecordingCallManagerFactory.PROP_RECORD_CALL_SUCCESS);
			if(val != null) {
				if("TRUE".equalsIgnoreCase(val)) {
					recordCallSuccess = true;
				}
				else if("FALSE".equalsIgnoreCase(val)) {
					recordCallSuccess = false;
				}
				else {
					logger.warningT(tracer,"Invalid property RecordingCallManagerFactory.PROP_RECORD_CALL_SUCCESS = " + val + ", Allowed values = true | false");
				}
			}
		}
    }
    

    
    /**
     * This methods validates that DB Tables required by CRT are present in the database
     */
    private void checkDatabaseSchema() throws DatastoreException {
    	PersistenceManager pm = null;
    	try {
    		pm = pmf.getPersistenceManager();
    		Query query = pm.newQuery(PersistentCall.class);
    		// set a filter which returns 0 objects
    		query.setFilter("type == 100");
    		query.execute();
    		query.closeAll();
    	}
    	catch(JDOException ex) {
    		if(logger != null) {
				logger.errorT(tracer,"CRT JDO database check failed");    			
    			logger.errorT(tracer,getMsgPlusStackTraceAsString(ex));
    		} 
    		
    		// Schema does not contains required tables
    		throw new DatastoreException(ex);
    	}
    	finally {
    		if(pm != null) pm.close();
    	}
    }

	/**
	 * Check in memory first and then retreive from datastore, if required
	 * <p> Retreives call based on request id</p>
	 * @param id
	 * @return
	 */
	public Call getCallById(String id) {
		PersistenceManager pm = pmf.getPersistenceManager();
		PersistentCall pcall = null;
		AbstractCall abscall = null;
		try {
			// TEMP: hack for JDO getObjectById bug
			Query query = pm.newQuery(PersistentCall.class);
			query.compile();
			query.closeAll();
			pcall = (PersistentCall)pm.getObjectById(new PersistentCall.Id(id),false);
			abscall = toExternCall(pcall);
		}
		catch(JDOException ex) {
			String msg = "Failed to load call, Call Id : " + id;
			DatastoreException dtex = new DatastoreException(ex);
			if(logger != null) logger.warning(tracer,getMsgPlusStackTraceAsString(dtex));
		}
		catch(RuntimeException ex) {
			String msg = "Call Id : " + id + " is invalid";
			UserException uex = new UserException(ex);
			if(logger != null) logger.warning(tracer,getMsgPlusStackTraceAsString(uex));
		}
		finally {
			pm.close();
		}
	
		return abscall;
	}
	
	private static AbstractCall toExternCall(PersistentCall pcall) {
		AbstractCall abscall = null;
		if(pcall.isCallChain()) {
			CallChainImpl callChain = new CallChainImpl();
			callChain.setCompleted(true);
			syncTo(pcall,callChain);
			
			Iterator it = pcall.getCallSteps().iterator();
			PersistentCall currentPCall = pcall;
			AbstractCall currentAbsCall = null;				
			while(it.hasNext()) {
				try {
					currentPCall = (PersistentCall)it.next();
					currentAbsCall = (AbstractCall)currentPCall.newCall();
					currentAbsCall.setCallChain(callChain);
					syncTo(currentPCall,currentAbsCall);
					currentAbsCall.afterLoad();
					callChain.addCall(currentPCall.getIndex(),currentAbsCall);
				}
				catch (Exception e) {
					return null;
				}
			}
			abscall = callChain;
		}
		else {
			try {
				abscall = (AbstractCall)pcall.newCall();
				syncTo(pcall,abscall);
			} catch (Exception e) {
				return null;
			}
		}
		return abscall;
	}
    
    private static Result toExternResult(Call call, PersistentCall pcall) {
		Result result = null;
		
		if(call instanceof CallChain && pcall.isCallChain()) {
			CallChain chain = (CallChain)call;
			ResultChainImpl resultChain = new ResultChainImpl(chain);
			PersistentCall currentPCall = pcall;
			SimpleResult current = null;
			Iterator it = pcall.getCallSteps().iterator();		
			while(it.hasNext()) {
				currentPCall = (PersistentCall)it.next();
				if(currentPCall.getStatus() == ResultStatusEnum.NOT_EXECUTED.value()) break;
				current = new SimpleResult();
				current.setResultChain(resultChain);
				current.setStartTime(currentPCall.getStartTime());
				current.setExecutionTime(currentPCall.getExecutionTime());
				current.setResult(currentPCall.getResult());
				current.setException(currentPCall.getError());
				current.setCall(chain.getCall(currentPCall.getIndex()));
				resultChain.addResult(current);
			}
			resultChain.setStatus(ResultStatusEnum.toEnum(pcall.getStatus()));
			result = resultChain;
		}
		else {
			SimpleResult simpleResult = new SimpleResult();
			simpleResult.setExecutionTime(pcall.getExecutionTime());
			simpleResult.setResult(pcall.getResult());
			simpleResult.setException(pcall.getError());
			simpleResult.setCall(call);
			result = simpleResult;
		}

		return result;
    }
    
    /**
     * @param Date since optional (null is allowed)
     * @return Collection non modificable collection of failed calls
     */
    public Collection getFailedCalls(Date since) {
		PersistenceManager pm = pmf.getPersistenceManager();
		ArrayList externResult = new ArrayList();
		try {
			Query query = pm.newQuery(PersistentCall.class);
			query.compile();
			StringBuffer filter = new StringBuffer();
			filter.append("success == 0");

			if(since != null) {
				filter.append(" && ");
				filter.append(" startTime >= " + since.getTime());
			}
			query.setFilter(filter.toString()); // failed calls
			query.setOrdering("startTime descending");
			Collection result = (Collection)query.execute();
			Iterator it = result.iterator();
			while(it.hasNext()) {
				PersistentCall pcall = (PersistentCall)it.next();
				try {
					AbstractCall abscall = toExternCall(pcall);					
					externResult.add(abscall);
				}
				catch(Exception ex){
					logger.errorT(tracer,getMsgPlusStackTraceAsString(ex));
				}
			}

			query.close(result);			
		}
		catch(RuntimeException ex) {
			logger.errorT(tracer,getMsgPlusStackTraceAsString(ex));
		}
		finally {
			pm.close();
		}
		return Collections.unmodifiableCollection(externResult);
    }
    
    /**
     * <p>
     * Removes a recorded call, After this the call can no longer can be
     * retreived. Used for clean up of the recordings made
     * </p>
     * @param callId
     */
    private void removeCallById(String id) {
		PersistenceManager pm = pmf.getPersistenceManager();
		PersistentCall pcall = null;
		AbstractCall abscall = null;
		try {
			// TEMP: hack for JDO getObjectById bug
			Query query = pm.newQuery(PersistentCall.class);
			query.compile();
			query.closeAll();
			pcall = (PersistentCall)pm.getObjectById(new PersistentCall.Id(id),false);
			pm.currentTransaction().begin();
			if(pcall.getCallSteps() != null && pcall.getCallSteps().size() > 0)
				pm.deletePersistentAll(pcall.getCallSteps());
			pm.deletePersistent(pcall);
			pm.currentTransaction().commit();
		}
		catch(JDOException ex) {
			String msg = "Failed to load call, Call Id : " + id;
			DatastoreException dtex = new DatastoreException(ex);
			if(logger != null) logger.warning(tracer,getMsgPlusStackTraceAsString(dtex));
		}
		catch(RuntimeException ex) {
			String msg = "Call Id : " + id + " is invalid";
			UserException uex = new UserException(ex);
			if(logger != null) logger.warning(tracer,getMsgPlusStackTraceAsString(uex));
		}
		finally {
			if(pm.currentTransaction().isActive())
				pm.currentTransaction().rollback();
			pm.close();
		}
    }
    
    
    /**
     * <p>
     * This list can be potentially huge so only summary is returned
     * Both Successful and Failed calls are returned
     * </p>
     * @param Date since (list of calls executing since a time period
     * @return Collection
     */
    public Collection getExecutedCalls(Date since) {
		return null;    	
    }
    
    public Iterator getExecutingCalls() {
    	return super.getExecutingCalls();
    }

	/**
	 * <p>
	 * Since calls are recorded by Persistence Manager it is possible to retreive
	 * the results of all the executed calls at any point in time whether they failed
	 * or succeeded. This feature is especially helpful to get the exception stacktrace
	 * for failed calls
	 * </p>
	 * @param call
	 * @return
	 */
	public Result invocationResult(Call call) {
		// call must have an attached id
		if(call.getCallId() == null) {
			// TODO user exception
			return null;
		}
		
		if(call instanceof CallChain) {
			return invocationResult((CallChain)call);
		}
		
		// check if executing 
		synchronized(execCalls) {
			Result result = (Result)execCalls.get(call);
			if(result != null) return result;
		}
		
		PersistenceManager pm = pmf.getPersistenceManager();
		PersistentCall pcall = null;
		Result result = null;
		try {
			// TODO: temp hack for JDO
			Query query = pm.newQuery(PersistentCall.class);
			query.compile();
			query.closeAll();
			pcall = (PersistentCall)pm.getObjectById(new PersistentCall.Id(call.getCallId().toString()),false);
			
			// TODO: Illegal Call Id
			if(pcall == null) {
				return null;
			}
			
			result = toExternResult(call,pcall);			
		}
		catch(RuntimeException ex) {
			logger.errorT(tracer,getMsgPlusStackTraceAsString(ex));
			return null;
		}
		finally {
			// close the pm
			if(pm!=null)
				pm.close();
		}
		
		return result;
	}


	/* (non-Javadoc)
	 * @see com.sap.isa.crt.RecordingCallManager#invocationResult(com.sap.isa.crt.CallChain)
	 */
	public ResultChain invocationResult(CallChain chain) {
		// call chain must have an attached id
		if(chain.getCallId() == null) {
			// TODO user exception
			return null;
		}
		
		// check if executing 
		synchronized(execCalls) {
			ResultChain result = (ResultChain)execCalls.get(chain);
			if(result != null) return result;
		}
		
		PersistenceManager pm = pmf.getPersistenceManager();
		PersistentCall pcall = null;
		ResultChain result = null;
		try {
			// TODO: temp hack for JDO
			Query query = pm.newQuery(PersistentCall.class);
			query.compile();
			query.closeAll();
			pcall = (PersistentCall)pm.getObjectById(new PersistentCall.Id(chain.getCallId().toString()),false);
			
			// TODO: Illegal Call Id
			if(pcall == null) {
				return null;
			}
			
			result = (ResultChain)toExternResult(chain,pcall);			
		}
		catch(RuntimeException ex) {
			logger.errorT(tracer,getMsgPlusStackTraceAsString(ex));
			return null;
		}
		finally {
			pm.close();
		}
		
		return result;
	}
		
    /**
     * <p>
     * Call must extend from AbstractCall and must provide a default ctor
     * </p>
     * @param Call call to invoke
     */
    public Result invoke(Call call, boolean asyncMode) {
		checkCallContract(call);
		
        Result result = super.invoke(call,asyncMode);
        
        return result;
    }
    
    
	/**
	 * <p>
	 * Call must extend from AbstractCall and must provide a default ctor
	 * </p>
	 * @param Call call to invoke
	 */
	public ResultChain invoke(CallChain chain, boolean asyncMode) {
		checkInitialized();
		
		checkValidCallChain(chain);
		
		//checkCallContract(chain);
		
		int index = callChainStartIndex(chain); // new calls

		// last execution was successful
		if(chain.getSize() <= index) {
			throw new UserException("call execution finished already") ;
		}
		
		ResultChain result = null;
		// persist the call before invocation
		if(asyncMode) {
			result = super.asyncInvokeCallChain(chain,index);
		}
		else {
			result = super.invokeCallChain(chain,index);
		}
		
		return result;
	}
    
        
    /**
     * <p>
     * Traverses the call chain to find the First Call in chain to execute
     * Logic:
     * If call is replayed, it should start execution from the last failed point
     * New Call start from first call in Call Chain
     * Returns the call from where to begin the excecution in Call Chain
     * </p>
     * @param call
     */
    private int callChainStartIndex(CallChain chain) {
		int index = 0;
		// re-execution
		if(chain.getCallId() != null) {
			ResultChain resultChain = invocationResult(chain);
			// TO DO: throw User exception
			if(resultChain == null) {
				throw new UserException("Invalid call chain");
			}
			
			if(resultChain.getStatus() == ResultStatusEnum.PREMATURE_TERMINATION ||
				resultChain.getStatus() == ResultStatusEnum.SUCCESS) {
					// no need to reexecute
					return chain.getSize();
			}
			else if(resultChain.getStatus() == ResultStatusEnum.FAILURE) {
				ResultChain.ResultIterator it = resultChain.resultIterator(); 
				while(it.hasNext()) {
					Result result = it.nextResult();
					if(result.getStatus() == ResultStatusEnum.FAILURE) {
						break;
					}
					else {
						index++;
					}
				}
			}
		}
		else {
			index = 0;
		}
    	return index;
    }
    
	/* (non-Javadoc)
	 * @see com.sap.isa.crt.SimpleCallManager#beforeExcecute(com.sap.isa.crt.CallProcessor)
	 */
	protected void beforeExecute(CallProcessor processor) {
		super.beforeExecute(processor);
		Call call = processor.getCall();
		
		// reexecution
		if(call.getCallId() != null) {
			// do nothing
		}
		else {
			// assing a new Id
			call.setCallId(OIDGenerator.newOID());
		}
	}
	
	/* (non-Javadoc)
	 * @see com.sap.isa.crt.SimpleCallManager#afterAsyncExecute(com.sap.isa.crt.CallProcessor)
	 */
	protected void afterExecute(CallProcessor processor, Result result) {
		super.afterExecute(processor,result);
		Call call = processor.getCall();
		persistExecution((AbstractCall)call,result);
	}

	/**
	 * @see com.sap.isa.crt.SimpleCallManager#close()
	 */
	public void close() {
		super.close();
	}

	/**
	 * TODO: Checks for complete call chain
	 * @param call
	 */
	private void checkCallContract(Call call) {
		if(call != null) {
			Class clazz = call.getClass();
			// call does not extend from AbstractCall
			if(!(call instanceof AbstractCall/* || call instanceof Serializable)*/)) {
				throw new UserException("Calls made to PersistentCallmanager must be serializable");
			}
			
			// Class is not public
			if(!(Modifier.isPublic(call.getClass().getModifiers()))) {
				throw new UserException("Calls made to PersistentCallmanager must be serializable");
			}
			
			// internal class not static
			/*
			if(clazz.getDeclaringClass() != null && !Modifier.isStatic(clazz.getModifiers())) {
				throw new UserException("Calls made to PersistentCallmanager must be serializable");
			}*/
			
			// public no no-arg ctor
			try {
				Constructor ctor = clazz.getConstructor(new Class[0]);
				if(!(Modifier.isPublic(ctor.getModifiers()))) {
					throw new UserException("missing public no arg ctor");
				}
			}
			catch(NoSuchMethodException ex) {
				throw new UserException("missing public no arg ctor");
			}
		}
	}

	private void persistExecution(AbstractCall call, Result result) {
		PersistentCall pCall = null;

		call.beforeSave();
		PersistenceManager pm = pmf.getPersistenceManager();
		try {
			// @@temp: begin hack for JDO bug in loading of meta data
			Query query = pm.newQuery(PersistentCall.class);
			query.compile();
			query.closeAll();
			// @@temp : end
			
			pCall = (PersistentCall)pm.getObjectById(new PersistentCall.Id(call.getCallId().toString()),true);
		}
		catch(JDOException ex)  {
			logger.errorT(tracer,getMsgPlusStackTraceAsString(ex));
		}
		
		try {
			JDOUtil.startTransaction(pm);
			if(pCall == null) {
				pCall = new PersistentCall(call,result);
				
				Collection steps = new java.util.ArrayList(); 
				
				// Persist call chain steps as well
				if(call instanceof CallChain) {
					CallChain chain = (CallChain)call;
					for(int i = 0; i < chain.getSize(); i++) {
						AbstractCall stepCall = (AbstractCall)chain.getCall(i);
						PersistentCall pStepCall = new PersistentCall(stepCall,null);
						pStepCall.setIndex(i);
						steps.add(pStepCall);
					}
				}
				pCall.setCallSteps(steps);
			}
			else {
				// since Global state at Call Chain can be modified sync it
				if(call instanceof CallChain) {
					pCall.setParams(call.getParamMap());
				}
				pCall.updateResult(result);
			}
			pm.makePersistent(pCall);
			pm.currentTransaction().commit();
		}
		catch(RuntimeException ex) {
			if(pm.currentTransaction().isActive()) {
				pm.currentTransaction().rollback();			
			}
			// Warning only since the call could be executed
			DatastoreException dex = new DatastoreException(ex);
			if(logger != null) logger.warningT(tracer,getMsgPlusStackTraceAsString(dex));			
		}
		finally {
			if(pm != null) pm.close();
		}
	}
	
	
	
	private static void syncTo(AbstractCall src, PersistentCall tgt) {
		tgt.setCallClassName(src.getClass().getName());
		tgt.setCallName(src.getName());
		tgt.setParams(src.getParamMap());
		tgt.setTimeout(src.getTimeout());
		tgt.setRetries(src.getRetryCount());
		tgt.setId(src.getCallId().toString());
	}
	
	private static void syncTo(PersistentCall src, AbstractCall tgt) {
		tgt.setName(src.getCallName());
		tgt.setParamsMap(src.getParams());
		tgt.setTimeout(src.getTimeout());
		tgt.setRetryCount(src.getRetries());
		tgt.setCallId(src.getId());
	}
	
	/* 
	 * @see com.sap.isa.crt.SimpleCallManager#afterExecute(com.sap.isa.crt.CallChainProcessor, com.sap.isa.crt.ResultChainImpl)
	 */
	protected void afterExecute(
		CallChainProcessor processor,
		ResultChainImpl resultChain) {
		super.afterExecute(processor, resultChain);
		
		// persist call chain execution completed
		persistExecution((AbstractCall)processor.getCallChain(),resultChain);
		
		// delete the Successfully completed call chain from database
		if(resultChain.getStatus() == ResultStatusEnum.SUCCESS && !recordCallSuccess) {
			removeCallById((String)processor.getCallChain().getCallId());
		}
	}

	/* 
	 * Fill the result if this is a reexecution
	 * @see com.sap.isa.crt.SimpleCallManager#beforeExecute(com.sap.isa.crt.CallChainProcessor)
	 */
	protected void beforeExecute(CallChainProcessor processor, ResultChainImpl resultChain) {
		super.beforeExecute(processor,resultChain);		
		// if Call is re-executing, fill the Result till last successful execution step
		CallChain chain = processor.getCallChain();
		// This call is already assigned an id i.e. reexecution
		if(chain.getCallId() != null) {
			ResultChainImpl preResult = (ResultChainImpl)invocationResult(chain);
			ResultChain.ResultIterator it = preResult.resultIterator();
			
			while(it.hasNext()) {
				// Failed Resutl is skipped
				Result result = it.nextResult();
				if(result.getStatus() == ResultStatusEnum.SUCCESS) resultChain.addResult(result);
			}
		}
		else {
			// assign Id to call chain and persist its invocation		
			chain.setCallId(OIDGenerator.newOID());
			CallChain.CallIterator it = chain.callIterator();
			while(it.hasNext()) {
				Call call = it.nextCall();
				call.setCallId(OIDGenerator.newOID());
			}
			persistExecution((AbstractCall)chain,resultChain);
		}
	}
	/* (non-Javadoc)
	 * @see com.sap.isa.crt.SimpleCallManager#checkValidCallChain(com.sap.isa.crt.CallChain)
	 */
	protected void checkValidCallChain(CallChain chain) {

		super.checkValidCallChain(chain);
		
		for(int i = 0; i < chain.getSize(); i++) {
			Call call = chain.getCall(i);
			checkCallContract(call); 
		}
	}

	private static String getStackTraceAsString(Throwable th) {
		if(th == null) return null;
		java.io.StringWriter strW = new java.io.StringWriter(0);
		java.io.PrintWriter prW = new java.io.PrintWriter(strW);
		th.printStackTrace(prW);
		return strW.toString();
	}
	
	private static String getMsgPlusStackTraceAsString(Throwable ex) {
		if(ex == null) return null;		
		String msg = ex.getMessage();
		String st = getStackTraceAsString(ex);
		if(msg != null) return msg + '\n' + st;
		else return st;
	}	
}