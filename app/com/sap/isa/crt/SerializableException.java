/*
 * Created on Sep 3, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.crt;

import java.io.PrintStream;
import java.io.PrintWriter;

/**
 * <p>
 * Converts any exception into a serializable exception
 * maintains the stacktrace and original exception class type
 * </p>
 * @author I802891
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class SerializableException extends Exception {
	private String stacktrace;
	private String message;
	private String exceptionClass;
	
	public SerializableException(Throwable th) {
		java.io.StringWriter sw = new java.io.StringWriter();
		java.io.PrintWriter pw = new java.io.PrintWriter(sw);
		th.printStackTrace(pw);
		this.stacktrace = sw.toString();
		this.exceptionClass = th.getClass().getName();
		this.message = th.getMessage();
	}
	
	public SerializableException() {
	}
	
	/**
	 * @return
	 */
	public String getExceptionClass() {
		return exceptionClass;
	}

	/**
	 * @return
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @return
	 */
	public String getStacktrace() {
		return stacktrace;
	}

	/* (non-Javadoc)
	 * @see java.lang.Throwable#printStackTrace()
	 */
	public void printStackTrace() {
		synchronized (System.err) {
			System.err.println(message);
			System.err.println(stacktrace);
		}

	}

	/* (non-Javadoc)
	 * @see java.lang.Throwable#printStackTrace(java.io.PrintStream)
	 */
	public void printStackTrace(PrintStream s) {
		synchronized(s) {
			s.println(message);
			s.println(stacktrace);
		}
	}

	/* (non-Javadoc)
	 * @see java.lang.Throwable#printStackTrace(java.io.PrintWriter)
	 */
	public void printStackTrace(PrintWriter s) {
		synchronized(s) {
			s.println(message);
			s.println(stacktrace);
		}
	}
}
