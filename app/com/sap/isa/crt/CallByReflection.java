package com.sap.isa.crt;

import java.lang.reflect.Method;
import java.util.ArrayList;

import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;

/**
 *
 * Invokes a given method of an instance using java Reflection
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class CallByReflection extends AbstractCall {
    private static final Location tracer = Location.getLocation(CallByReflection.class.getName());
	private Method _method; // method to call
    private Object _this;   // instance on which to call the method
    private String _methodName;
    private String _className;
    private ArrayList _paramNames; // in order param names

    public CallByReflection(Object argThis, Method argMethod) {
        this._method = argMethod;
        this._this = argThis;

        this._methodName = argMethod.getName();
        this._className = this._method.getDeclaringClass().getName();
        _paramNames = new ArrayList();
    }

    /**
     * @return Object Object on which the call is made
     */
    public Object getThis() {
        return _this;
    }

    /**
     * For recycling
     */
    public void setThis(Object argThis) {
        this._this = argThis;
    }

    /**
     * @return Method Method which is to be invoked
     */
    public java.lang.reflect.Method getMethod() {
        return _method;
    }

    public Object invoke(CallContext ctxt) throws Exception {
        Category logger = ctxt.getLogger();

        String[] argNames = this.getParamNames();

        // param count check
        if(_method.getParameterTypes().length != argNames.length) {
            throw new CallRuntimeException("");
        }

        Object[] argValues = new Object[argNames.length];
        for(int i = 0; i < argNames.length; i++) {
            argValues[i] = this.getParam(argNames[i]);
        }

        if(logger != null && logger.beInfo()) {
            logger.infoT(tracer,"Invoking call by reflection: " + _className + "." + _methodName + "(...)");
        }

        Object result = _method.invoke(_this,argValues);

        if(logger != null && logger.beInfo()) {
            logger.infoT(tracer,"Invoked call by reflection: " + _className + "." + _methodName + "(...)");
        }

        return result;
    }

    public void recycle() {
        super.recycle();
        _this = null;
    }
    
	/* (non-Javadoc)
	 * @see com.sap.isa.crt.Call#getParamNames()
	 */
	public String[] getParamNames() {
		String[] values = new String[_paramNames.size()];
		_paramNames.toArray(values);
		return values;
	}

	/* 
	 * Order of call is maintained in passing params
	 * Params are passed in order in which they are added
	 * @see com.sap.isa.crt.AbstractCall#addParam(java.lang.String, java.lang.Object)
	 */
	public void addParam(String name, Object value) {
		addParam(_paramNames.size(),name,value);
	}
	
	/**
	 * @param index index starts from 0
	 * @param name
	 * @param value
	 */
	public void addParam(int index, String name, Object value) {
		super.addParam(name, value);
		if(index < 0) throw new IllegalArgumentException("index is invalid:" + index);
		if(!_paramNames.contains(name)) {
			if(index > _paramNames.size()) index = _paramNames.size();
			_paramNames.add(index,name);
		}
	}


	/* (non-Javadoc)
	 * @see com.sap.isa.crt.AbstractCall#removeParam(java.lang.String)
	 */
	public Object removeParam(String name) {
		_paramNames.remove(name);
		return super.removeParam(name);
	}

}