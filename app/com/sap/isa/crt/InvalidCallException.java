/*
 * Created on Sep 4, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.crt;

/**
 * @author I802891
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class InvalidCallException extends RuntimeException {
	private Throwable th;

	public InvalidCallException(String msg) {
		super(msg);
	}

	public InvalidCallException(Throwable nested) {
		this.th = nested;
	}

	public Throwable getNestedThrowable() {
		return th;
	}
}
