package com.sap.isa.crt;

/**
 * This exception is thrown if User tries to execute same call concurrently
 *
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class ConcurrentExecException extends RuntimeException {

    public ConcurrentExecException(String msg) {
        super(msg);
    }
}