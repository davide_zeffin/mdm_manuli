package com.sap.isa.crt;

/**
 * Calls implementation can throw this exception to indicate Timeout on operations
 * performed by them
 *
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class TimeoutException extends RuntimeException {

    public TimeoutException(String msg) {
        super(msg);
    }

    /**
     * @return long millis to wait before retry
     */
    public long getRetryInterval() {
        return -1;
    }

    void setRetryInterval(long millis) {
    }
}