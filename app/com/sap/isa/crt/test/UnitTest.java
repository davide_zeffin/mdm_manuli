package com.sap.isa.crt.test;

import java.util.Properties;

import javax.jdo.PersistenceManagerFactory;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.crt.AsyncResult;
import com.sap.isa.crt.CallChain;
import com.sap.isa.crt.CallChainImpl;
import com.sap.isa.crt.RecordingCallManager;
import com.sap.isa.crt.RecordingCallManagerFactory;
import com.sap.isa.crt.Result;
import com.sap.isa.crt.ResultChain;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class UnitTest {

	protected static IsaLocation log = IsaLocation.getInstance(UnitTest.class.getName());

    public UnitTest() {
    }

    public static void main(String[] args) {
		
		/*
		SimpleCallManagerFactory factory = new SimpleCallManagerFactory();
		final SimpleCallManager mgr = (SimpleCallManager)factory.getInstance(null);
		*/
		
        RecordingCallManagerFactory factory = getPCMF();
        final RecordingCallManager mgr = (RecordingCallManager)factory.getInstance(null);
        
        TestCall call1 = new TestCall(false);
        TestCall call2 = new TestCall(true);
		call1.setName("TestCall1");
		call2.setName("TestCall2");
		call1.addParam("state", new TestCall.State());
		
		CallChainImpl chain = new CallChainImpl();
		chain.addCall(call1);
		chain.addCall(call2);
		call1.setCallChain(chain);
		call2.setCallChain(chain);
		       
        call1.setRetryCount((short)2);
		call2.setRetryCount((short)2);
        Result result1 = mgr.invoke(chain,false);
        
        if(result1 instanceof AsyncResult) {
        	((AsyncResult)result1).waitForCompletion();
        }

		if(result1.getException() != null) {
			result1.getException().printStackTrace();
		}
		else {
			System.out.println("Return value:" + result1.getResult().toString());
            log.debug("Return value: " + result1.getResult().toString());
		}
        
        Object obj = result1.getCall().getCallId();
        
		//Call cc = (Call)mgr.getCallById(obj.toString());
		//Result rchain = mgr.invocationResult(cc);

        CallChain cc = (CallChain)mgr.getCallById(obj.toString());
        ResultChain rchain = mgr.invocationResult(cc);
        result1 = mgr.invoke(cc,false);
        
        if(result1.getException() != null) {
            result1.getException().printStackTrace();
        }
        else {
            System.out.println("Return value:" + result1.getResult().toString());
			log.debug("Return value: " + result1.getResult().toString());
        }
        System.out.println("Execution time: " + result1.getExecutionTime());
		log.debug("Execution time: " + result1.getExecutionTime());

        /*
        try {
            java.lang.reflect.Method meth =
                TestCall.class.getMethod("invoke",new Class[]{CallContext.class});
            CallByReflection call =  new CallByReflection(null,meth);
            call.setThis(new TestCall());
            call.addParam("ctxt",new CallContext() {
                    public CallManager getCallManager() {
                        return mgr;
                    }

                    public Object getLogger() {
                        return loc2;
                    }
                });
            mgr.invoke(call);
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }
        */

        /*
        AsyncResult result2 = mgr.asyncInvoke(new TestCall());
        result2.waitForCompletion();
        System.out.println("Return value:" + result2.getResult().toString());
        System.out.println("Execution time: " + result2.getActualExecutionTime());
        */
        //mgr.close();
    }

	private static PersistenceManagerFactory pmFactory;    
	public static PersistenceManagerFactory getPersistenceManagerFactory() {
		if(pmFactory == null) {
			Properties props = new Properties();
			ClassLoader cl = Thread.currentThread().getContextClassLoader();
			props.setProperty("javax.jdo.PersistenceManagerFactoryClass",
				"com.sap.jdo.sql.SQLPMF");
			props.setProperty("javax.jdo.option.ConnectionDriverName",
				"com.sap.sql.jdbc.common.CommonDriver");
			props.setProperty("javax.jdo.option.ConnectionURL",
				"jdbc:sap:sapdb://i802891/C11?timeout=0");
			props.setProperty("javax.jdo.option.ConnectionUserName",
				"SAPC11DB");
			props.setProperty("javax.jdo.option.ConnectionPassword",
				"sap");
			pmFactory = javax.jdo.JDOHelper.getPersistenceManagerFactory(props,cl);
			if(pmFactory.getOptimistic()) {
				pmFactory.setOptimistic(false);
			}
		}
		return pmFactory;
	}
	
	private static RecordingCallManagerFactory pmcf;	
	static RecordingCallManagerFactory getPCMF() {
		if(pmcf == null) {
		pmcf = new RecordingCallManagerFactory();
		pmcf.setJDOPMF(getPersistenceManagerFactory());
		}
		return pmcf;
	} 
}