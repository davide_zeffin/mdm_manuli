package com.sap.isa.crt.test;

import java.io.Serializable;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.crt.AbstractCall;
import com.sap.isa.crt.CallContext;
import com.sap.tc.logging.Category;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class TestCall extends AbstractCall {

	boolean error = false;

	/** 
	  *  The IsaLocation of the current Action.
	  *  This will be instantiated during perform and is always present.
	  */
	 protected static IsaLocation log = IsaLocation.getInstance(TestCall.class.getName());

	public TestCall() {
	}
	
    public TestCall(boolean error) {
    	this.error = error;
    }
	
	public String getName() {
		if(name == null) return super.getName();
		else return name;
	}
	
    public Object invoke(CallContext ctxt) throws Exception {
        Category logger = (Category)ctxt.getLogger();
        try {
            Thread.sleep(300); // sleep 100 ms
        }
        catch(InterruptedException ex) {
        	log.debug("Error ", ex);
        }
		
		if(error)
        	throw new Exception("exception");
		
        return "return";
    }
    
    
    public static class State implements Serializable {
    	private String val1 = "val1";
    	private String val2 = "val2";
    }
    
    public String[] getParamNames() {
    	return new String[0];
    }
}