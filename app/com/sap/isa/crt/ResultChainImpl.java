/*
 * Created on Nov 17, 2003
 *
 */
package com.sap.isa.crt;

import java.util.LinkedList;
import java.util.NoSuchElementException;

/**
 * @author I802891
 *
 */
public class ResultChainImpl implements ResultChain {
	
	private long startTime = -1;	
	private CallChain chain = null;
	private ResultStatusEnum status;
	private LinkedList results = new LinkedList();
	
	public ResultChainImpl(CallChain callchain) {
		this.chain = callchain;
		startTime = System.currentTimeMillis();
	}
	
	/**
	 * For Call Chain executing Asycnchronously this will be addded incrementally
	 * @param result
	 */
	public void addResult(Result result) {
		synchronized(results) {
			results.add(result);
		}
	}
	
	void addResult(int index, Result result) {
		synchronized(results) {
			results.add(index,result);
		}
	}
	
	/* (non-Javadoc)
	 * @see com.sap.isa.crt.Result#getCall()
	 */
	public Call getCall() {
		return chain;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.crt.Result#getException()
	 */
	public Exception getException() {
		Result[] snapshot = null;
		synchronized(results) {
			snapshot = new Result[results.size()];
			results.toArray(snapshot);
		}
		
		if(snapshot.length == 0) {
			return null;
		}
		else {
			// if exception occured it must be in the last result
			Result lastResult = snapshot[snapshot.length - 1];		
			return lastResult.getException();
		}
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.crt.Result#getExecutionTime()
	 */
	public long getExecutionTime() {
		Result[] snapshot = null;
		synchronized(results) {
			snapshot = new Result[results.size()];
			results.toArray(snapshot);
		}
		
		long totalExecTime = 0;
		
		for(int i = 0; i < snapshot.length; i++) {
			totalExecTime += snapshot[i].getExecutionTime();
		}
		
		return totalExecTime;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.crt.Result#getResult()
	 */
	public Object getResult() {
		return resultIterator();
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.crt.ResultChain#resultIterator()
	 */
	public ResultIterator resultIterator() {
		Result[] snapshot = null;
		synchronized(results) {
			snapshot = new Result[results.size()];
			results.toArray(snapshot);
		}
		
		ResultIteratorImpl it = new ResultIteratorImpl(snapshot);
		return it;
	}

	/* 
	 * Premature termination of call chain
	 * @see com.sap.isa.crt.ResultChain#setCompleted()
	 */
	void setStatus(ResultStatusEnum val) {
		this.status = val;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.crt.Result#isCompleted()
	 */
	public boolean isCompleted() {
		return status == ResultStatusEnum.SUCCESS || 
				status == ResultStatusEnum.FAILURE || 
				status == ResultStatusEnum.PREMATURE_TERMINATION;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.crt.Result#getStartTime()
	 */
	public long getStartTime() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	/* (non-Javadoc)
	 * @see com.sap.isa.crt.Result#isSuccess()
	 */
	public ResultStatusEnum getStatus() {
		return status;
	}

	private class ResultIteratorImpl implements ResultIterator {
		private Result[] snapshot;
		private int cursor = 0;
		
		ResultIteratorImpl(Result[] snapshot) {
			this.snapshot = snapshot;
		}
		
		/* (non-Javadoc)
		 * @see com.sap.isa.crt.ResultChain.ResultIterator#hasNext()
		 */
		public boolean hasNext() {
			if(cursor < snapshot.length) return true;
			else return false;
		}

		/* (non-Javadoc)
		 * @see com.sap.isa.crt.ResultChain.ResultIterator#nextResult()
		 */
		public Result nextResult() {
			if(hasNext()) {
				return snapshot[cursor++];
			}
			else {
				throw new NoSuchElementException("no more results");
			}
		}
	}

	/* 
	 * Nested chaining not allowed
	 * @see com.sap.isa.crt.Result#getResultChain()
	 */
	public ResultChain getResultChain() {
		return null;
	}
	/* (non-Javadoc)
	 * @see com.sap.isa.crt.ResultChain#getLastResult()
	 */
	public Result getLastResult() {
		synchronized(results) {
			return (Result)results.getLast();
		}
	}

}
