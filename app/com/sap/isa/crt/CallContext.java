package com.sap.isa.crt;

import com.sap.tc.logging.Category;

/**
 * Description: <p>Call Context propagates the execution environment for the Call </p>
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public interface CallContext {
    /**
     * @return CallManager
     */
    public CallManager getCallManager();

    /**
     * @return Logger logger that can be used for logging within invocation of calls
     *                 Calls can have their own private loggers. This serves as a fallback
     *                 logger
     */
    public Category getLogger();
}