/*
 * Created on Nov 13, 2003
 *
 */
package com.sap.isa.crt;

/**
 * <p>
 * 
 * Call Chain is a sequence of calls to be executed one after the another. 
 * Calls can be chained together to form a logical unit of work(UOW). Call provides
 * granularity to work done by UOW. CallManager executes the Call Chain in sequence
 * 
 * If a call in a chain fails, chain execution is stopped and error 
 * is returned.
 * </p>
 * 
 * @author I802891
 *
 */
public interface CallChain extends Call {
	/**
	 * <p>
	 * This can be used to mark the premature termination of Call Chain. 
	 * A step in call chain may indicate logical completion of Call
	 * by calling this method. If this method is invoked by a step in call chain,
	 * no further steps in call chain are not executed
	 * </p>
	 */	
	void setCompleted(boolean val);
	boolean isCompleted();
	
	CallIterator callIterator();

	int getSize();
	
	Call getCall(int index);
	
	public interface CallIterator {
		Call nextCall();
		boolean hasNext();
	}
}
