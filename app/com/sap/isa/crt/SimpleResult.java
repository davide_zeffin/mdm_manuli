/*
 * Created on Sep 3, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.crt;

/**
 * Internal
 * @author I802891
 *
 */
public final class SimpleResult implements Result {
	private long startTime;
	private ResultChain resultChain;
	private Call call;
	private Object result;
	private long executionTime;
	private Exception th;
	
	public SimpleResult() {
	}
	
	/* (non-Javadoc)
	 * @see com.sap.isa.crt.Result#isCompleted()
	 */
	public boolean isCompleted() {
		return true;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.crt.Result#getCall()
	 */
	public Call getCall() {
		return call;
	}
	
	public void setCall(Call call) {
		this.call = call;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.crt.Result#getExecutionTime()
	 */
	public long getExecutionTime() {
		return executionTime;
	}
	
	public void setExecutionTime(long arg) {
		this.executionTime = arg;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.crt.Result#getResult()
	 */
	public Object getResult() {
		return result;
	}
	
	public void setResult(Object result) {
		this.result = result;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.crt.Result#getException()
	 */
	public Exception getException() {
		return th;
	}

	public void setException(Exception th) {
		this.th = th;
	}
	/* (non-Javadoc)
	 * @see com.sap.isa.crt.Result#isSuccess()
	 */
	public ResultStatusEnum getStatus() {
		if(getException() != null) {
			return ResultStatusEnum.FAILURE;
		}
		else {
			return ResultStatusEnum.SUCCESS;
		}
	}
	
	/* (non-Javadoc)
	 * @see com.sap.isa.crt.Result#getResultChain()
	 */
	public ResultChain getResultChain() {
		return null;
	}
	
	public void setResultChain(ResultChain chain) {
		this.resultChain = chain;
	}
	/* (non-Javadoc)
	 * @see com.sap.isa.crt.Result#getStartTime()
	 */
	public long getStartTime() {
		return startTime;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.crt.Result#getStartTime()
	 */
	public void setStartTime(long time) {
		this.startTime = time;
	}
}
