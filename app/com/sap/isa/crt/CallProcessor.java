package com.sap.isa.crt;

import com.sap.tc.logging.*;

/**
 * <p><b>
 * Internal class not exposed
 * </b></p>
 * 
 * <p>
 * 	This class encapsulates the logic to process the call
 * </p>
 * 
 * 
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

class CallProcessor implements Runnable {

	private static final Location tracer = Location.getLocation(CallProcessor.class.getName());
	
    Call call;
    Object result;
    transient CallContext ctxt;

    long start = -1L;
    long end = -1L;
    Exception exception;

    CallProcessor(Call call) {
        this.call = call;
    }

    Call getCall() {
        return call;
    }
    
    void setCall(Call call) {
    	this.call = call;
    }

	ResultStatusEnum getStatus() {
		if(start == -1) {
			return ResultStatusEnum.NOT_EXECUTED;
		}
		
		if(end == -1) {
			return ResultStatusEnum.EXECUTING;
		}
		
		if(getException() != null) {
			return ResultStatusEnum.FAILURE;
		}
		else {
			return ResultStatusEnum.SUCCESS;
		}
	}
	
    /**
     * This must be set before executing the call with the processor
     */
    void setCallContext(CallContext ctxt) {
        this.ctxt = ctxt;
    }

	long getStartTime() {
		return start;
	}
	
    /**
     * Actual call processing time sanning Queueing delay in Pool
     */
    long getCallTime() {
        if(end != -1 && start != -1) {
            return end - start;
        }
        else if (start != -1) {
            return System.currentTimeMillis() - start;
        }

        return -1;
    }

    Object getResult() {
        return result;
    }

    Exception getException() {
        return exception;
    }

    /**
     * This run method handles the logic of Call processing
     */
    public void run () {
        int count = 0;
        boolean retry = true;
        RuntimeException rtex = null;

        Category logger = (Category)ctxt.getLogger();
        if(logger != null && logger.beInfo()) {
            logger.infoT(tracer,"start executing call " + call);
        }

        start = System.currentTimeMillis();

        while(retry && count++ < call.getRetryCount() + 1) {

            // clear the exception
            rtex = null;

            if(logger != null && logger.beInfo()) {
                logger.infoT(tracer,"executing call " + call + " #" + count);
            }

            try {
                result = call.invoke(ctxt);
                retry = false;
            }
            catch(Exception ex) {
                exception = ex;
                rtex = new CallRuntimeException(ex);
                if(logger != null && logger.beError()) {
                    logger.errorT(tracer,"exception on executing call " + call);
                }

                retry = true;
            }
        }

        end = System.currentTimeMillis();

        if(logger != null && logger.beInfo()) {
            logger.infoT(tracer,"end executing call " + call);
        }

        if(rtex != null) throw rtex; // bubble
    }
}