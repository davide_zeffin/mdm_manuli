package com.sap.isa.crt;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * Call implementation extends from this class. This class is not thread safe.
 * It is expected that a new instance of Call is created each time the Call is to
 * be invoked
 * <br/>
 * Or
 * <br/>
 * User waits till the Call has finished executing before reusing the same instance
 * *
 * </p>
 * Copyright:  Copyright (c) 2003, All rights reserved
 * Company:
 * @author narinder.singh@sap.com
 * @version 1.0
 */

public abstract class AbstractCall implements Call {
	protected CallChain chain =  null;
	protected String name = null;
    protected HashMap args = new HashMap();
    protected long timeout = 0;
    protected short retries = 0;
    protected Object id;

    protected AbstractCall() {
    }
    
	protected AbstractCall(CallChain chain) {
		if(this instanceof CallChain) {
			throw new IllegalArgumentException("Call chain cannot be nested in another call chain");
		}
		this.chain = chain;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.crt.Call#getCallChain()
	 */
	public CallChain getCallChain() {
		return chain;
	}
	
	public void setCallChain(CallChain chain) {
		this.chain = chain;  
	}
	
    /**
     * Call chain provides the global scope for param name
     * @param name name of the call argument
     * @return Object value
     */
    public Object getParam(String name) {
    	if(args.containsKey(name)) {
        	return args.get(name);
    	}
    	else if (chain != null){
			return chain.getParam(name);
    	}
    	else {
    		return null;
    	}
    }

    /**
     * <p>
     * Old value matching the same name is replaced
     * Null are allowed values
     * </p>
     */
    public void addParam(String name, Object value) {
    	if(name == null || name.length() == 0) throw new IllegalArgumentException("arg name is invalid:" + name);
        args.put(name,value);
    }

    /**
     * @param name name of the param
     * @return Object value for the param removed
     */
    public Object removeParam(String name) {
        return args.remove(name);
    }

	/**
	 * This is the map of call params
	 * Additions or removal from this Map effects the underlying call
	 * @return reference of the internal map
	 */    
    public Map getParamMap() {
    	return args;
    }
   
	public void setParamsMap(Map params) {
		this.args.clear();
		if(params != null)
			this.args.putAll(params);
   	}
   
    /**
     * @param millis timeout millis for call ( 0 => infinite wait)
     */
    public void setTimeout(long millis) {
        if(millis < 0) throw new IllegalArgumentException("invalid millis " + millis);
        this.timeout = millis;

    }

    /**
     * @return millis timeout millis for call ( 0 => infinite wait)
     */
    public long getTimeout() {
        return timeout;
    }

    /**
     * @param int count max number of retries for a call to be made
     */
    public void setRetryCount(short count) {
        if(count < 0) throw new IllegalArgumentException("invalid count " + count);
        this.retries = count;
    }

    /**
     *
     */
    public short getRetryCount() {
        return retries;
    }

    /**
     * <p>
     * Invoke this method before reusing a Call instance
     * Args are cleared
     * Timeout
     * Retry count is kept
     * </p>
     */
    public void recycle() {
        args.clear();
    }
    
	/* <p>
	 * Concrete calls can override to provide custom name
	 * returns the class name of the call by default
	 * </p>
	 * @see com.sap.isa.crt.Call#getName()
	 */
	public String getName() {
		if(name != null) return name;
		else return this.getClass().getName();
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	/* @return Object id
	 * @see com.sap.isa.crt.Call#getCallId()
	 */
	public Object getCallId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.crt.Call#setCallId(java.lang.Object)
	 */
	public void setCallId(Object id) {
		this.id = id;
	}
	
	/**
	 * <p>
	 * This call is made before the Call is persisted into datastore. This can be used by the 
	 * call to convert params into Serializable representation or clear off transient params
	 * which don't need to be saved and can be stored after deserialization
	 * </p>
	 *
	 */
	protected void beforeSave() {
	}
	
	/**
	 * <p>
	 * This call is made after the Call is deserailized from datastore. This can be used by the 
	 * call to convert params into Simple representation or reset transient params
	 * </p>
	 *
	 */
	protected void afterLoad() {
	}
	
	public String toString() {
		return this.getName();
	}
}