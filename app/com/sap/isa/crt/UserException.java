/*
 * Created on Sep 8, 2003
 *
 */
package com.sap.isa.crt;

/**
 * @author I802891
 *
 */
public class UserException extends RuntimeException {
	private Throwable th;

	public UserException(String msg) {
		super(msg);
	}

	public UserException(Throwable nested) {
		this.th = nested;
	}

	public Throwable getNestedThrowable() {
		return th;
	}	
}
