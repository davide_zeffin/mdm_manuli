/*
 * Created on Nov 13, 2003
 *
 */
package com.sap.isa.crt;

/**
 * @author I802891
 *
 */
public interface ResultChain extends Result {

	/**
	 * @return the call chain
	 */
	Call getCall();

	/**
	 * <p>
	 * This returns the sum of execution time of each step in the
	 * call chain
	 * </p>
	 * @return long millis taken to execute the Call chain
	 */
	long getExecutionTime();
	
	/**
	 * @return This returns an instance of ResultIterator
	 */
	Object getResult();
	
	/**
	 * @return the last result in the chain
	 */
	Result getLastResult();
	
	/**
	 * @return the exception thrown by failed step in call chain
	 */
	java.lang.Exception getException();
	
	/**
	 * @return Iterator over call chain results
	 */	
	ResultIterator resultIterator();
	
	public interface ResultIterator {
		Result nextResult();
		boolean hasNext();
	}
}
