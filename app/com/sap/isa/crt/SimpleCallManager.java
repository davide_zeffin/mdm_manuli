package com.sap.isa.crt;

import com.sap.isa.thread.*;

import com.sap.tc.logging.*;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;

/**
 * Simple Call Manager
 * <ul>
 *  <li> Only executing calls are visible</li>
 *  <li> Failed calls are not persisted or stored </li>
 *  <li> Thread safe </li>
 *
 * Copyright:  SAP Labs Palo Alto,  Copyright (c) 2003
 * Company:
 * @author narinder.singh@sap.com
 * @version 1.0
 */

public class SimpleCallManager implements CallManager {

    private static Location tracer = Location.getLocation(SimpleCallManager.class.getName());
	protected Category logger = null;    

    /** Context */
    protected CallContext ctxt = null;

    /** Thread Pool */
    protected GenericThreadPool tPool = null;

    /** Currently executing calls*/
    protected HashMap execCalls = new HashMap();

    /** method level sync flag */
    private boolean inside = false;
    
    /** my factory */
    protected CallManagerFactory cmf = null;
    
    protected ClassLoader cl = null;

	/** Configuration props */
	protected Properties props = null;
	
    protected boolean initialized = false;
    
    protected String name;

    public SimpleCallManager(CallManagerFactory argCMF, ClassLoader argCl, Properties props) {
    	if(argCMF == null) throw new IllegalArgumentException("argCMF is null");
    	if(argCl == null) throw new IllegalArgumentException("argCl is null");
    	
    	this.cmf = argCMF;
    	this.cl = argCl;
    	
    	if(props != null)
    		this.props = (Properties)props.clone();
    	
    	this.name = this.getClass().getName();
    	
    	Properties tpProps = new Properties();
    	tpProps.put(ThreadPool.PROP_MIN_THREADS,"0");
    	tpProps.put(ThreadPool.PROP_MAX_THREADS,"5");
        tPool = new GenericThreadPool(name + "_Thread_Pool",tpProps,null);

        ctxt = new CallContext() {
            public CallManager getCallManager() {
                return SimpleCallManager.this;
            }

            public Category getLogger() {
                return logger;
            }
        };

        initialize();
    }
    
	public Category getLogger() {
		return logger;
	}
    
	public void setLogger(Category logger) {
		this.logger = logger;
	}
	
    /**
     * Observer point for completion of Call Processing
     */
    private class Observer implements ThreadPoolCallback {

        public void setThreadDone(Runnable r) {
        	CallProcessor processor = (CallProcessor)r;
        	Result result = null;

        	synchronized(execCalls) {
        		result = (Result)execCalls.get(processor.getCall());
        	}
            afterExecute(processor,result);
        }

        public void setThreadError(Runnable r, Throwable t) {
			// do nothing
        }

    }

	protected void checkValidCallChain(CallChain chain) {
		if(chain.getSize() == 0) {
			throw new InvalidCallException("Call chain size must be > 0");
		}
		if(chain.getSize() > 50) {
			throw new InvalidCallException("Call chain has exceeded the max size limit, check for circular references");				
		}
	}
	
	/**
	 * Checks against circular references and infinite length call chain's
	 * Max call chain size is 50 
	 * @throws InvalidCallException
	 */
	private void checkValidCall(Call call) {
		if(call == null) throw new InvalidCallException("call is null");
	}
	
	/**
	 * @param chain
	 * @return
	 */
	protected ResultChain invokeCallChain(CallChain chain, int startIndex) {
		// check not already executing
		synchronized(execCalls) {
			if(execCalls.containsKey(chain)) {
				throw new ConcurrentExecException(chain.toString() + " call is being executed");
			}

			// add null to avoid concurreny across self & other calls
			execCalls.put(chain,null);
		}
		
		chain.setCompleted(false);
	
		CallChainProcessor ccProcessor = new CallChainProcessor(chain,startIndex);
		ccProcessor.setCallContext(ctxt);
		
		ResultChainImpl chainResult = new ResultChainImpl(chain); 
		beforeExecute(ccProcessor,chainResult);
		
		while(!ccProcessor.done()) {
			Call current = ccProcessor.getNextCall();			
			SyncResult result = invokeCall(current,ccProcessor);
			result.setResultChain(chainResult);
			chainResult.addResult(result);
		}
		
		// Set the result status
		chainResult.setStatus(ccProcessor.getStatus());
		// chain execution has completed		
		chain.setCompleted(true);
		
		afterExecute(ccProcessor,chainResult);
		
		synchronized(execCalls) {
			execCalls.remove(chain);
		}
				
		return chainResult;
	}
	
	protected ResultChain asyncInvokeCallChain(CallChain chain, int startIndex) {
		// check not already executing
		synchronized(execCalls) {
			if(execCalls.containsKey(chain)) {
				throw new ConcurrentExecException(chain.toString() + " call chain is being executed");
			}

			// add null to avoid concurreny across self & other calls
			execCalls.put(chain,null);
		}
		
		chain.setCompleted(false);
		
		CallChainProcessor ccProcessor = new CallChainProcessor(chain,startIndex);
		ccProcessor.setCallContext(ctxt);
		
		AsyncResultChainImpl chainResult = new AsyncResultChainImpl(chain); 
		beforeExecute(ccProcessor,chainResult);		

		synchronized(execCalls) {
			execCalls.put(chain,chainResult);
		}

		Call current = ccProcessor.getNextCall();			
		AsyncResultImpl result = asyncInvokeCall(current,ccProcessor);
		result.setResultChain(chainResult);	
		chainResult.addResult(result);
		
		return chainResult;
	}
	
	protected SyncResult invokeCall(final Call call, final CallProcessor processor) {
		// check not already executing
		synchronized(execCalls) {
			if(execCalls.containsKey(call)) {
				throw new ConcurrentExecException(call + " call chain is being executed");
			}
	
			// add null to avoid concurreny across self & other calls
			execCalls.put(call,null);
		}
		
		processor.setCall(call);
		processor.setCallContext(ctxt);

		// execute on the callee thread
		CallRuntimeException crtex = null;
		beforeExecute(processor);
		
		CallProcessor callProcessor = processor;
				
		try {
			processor.run();
		}
		// call failed
		catch(CallRuntimeException ex) {
			// log it and return
			java.io.StringWriter sw = new java.io.StringWriter();
			java.io.PrintWriter pw = new java.io.PrintWriter(sw);
			ex.printStackTrace(pw);
			pw.flush();
			logger.errorT(tracer,sw.toString());
			crtex = ex;
		}

		SyncResult result = new SyncResult(callProcessor);
		afterExecute(processor,result);
		
		synchronized(execCalls) {
			execCalls.remove(call);
		}
		
		// chain the results
		return result;
	}

	protected AsyncResultImpl asyncInvokeCall(final Call call, final CallProcessor processor) {
		// check not already executing
		synchronized(execCalls) {
			if(execCalls.containsKey(call)) {
				throw new ConcurrentExecException(call.toString() + " call is being executed");
			}

			// add null to avoid concurreny across self & other calls
			execCalls.put(call,null);
		}
		
		processor.setCall(call);
		processor.setCallContext(ctxt);
		
		beforeExecute(processor);
		
		AsyncResultImpl result = null;
		
		synchronized(execCalls) {
			CallProcessor callProcessor = processor;
			// execute on a separate thread
			ThreadControl ctlr = tPool.execute(processor);
			result = new AsyncResultImpl(callProcessor,ctlr);
			execCalls.put(call,result);
		}
		
		return result;
	}
		
    /**
     * Synchronous invocation of the Call
     *
     * @throws ConcurrentExecException
     * @throws CallRuntimeException
     */
    public Result invoke(Call call, boolean asyncMode) {
        checkInitialized();
		
		checkValidCall(call);
		
        inside = true;
        
		CallProcessor processor = new CallProcessor(call);        
		try {
			if(asyncMode) {
				return asyncInvokeCall(call,processor);				
			}
			else {
				return invokeCall(call,processor);
			}
        }
        finally { 
			inside = false;        	
        }
    }

	/* (non-Javadoc)
	 * @see com.sap.isa.crt.CallManager#invoke(com.sap.isa.crt.CallChain, boolean)
	 */
	public ResultChain invoke(CallChain callChain, boolean asyncMode) {
		checkInitialized();
		
		checkValidCallChain(callChain);
		
		inside = true;
        
		try {
			if(asyncMode) {
				return asyncInvokeCallChain(callChain,0);				
			}
			else {
				return invokeCallChain(callChain,0);
			}
		}
		finally { 
			inside = false;        	
		}
	}
	
	/**
	 * @param processor
	 * @return
	 */
	protected void beforeExecute(CallChainProcessor processor, ResultChainImpl resultChain) {
		resultChain.setStatus(processor.getStatus());
	}
	
	protected void afterExecute(CallChainProcessor processor, ResultChainImpl resultChain) {
	}
	
	/**
	 * @param processor
	 */
	protected void beforeExecute(CallProcessor processor) {

	}
	
	/**
	 * This method is called after the completion of execution of
	 * synchronous and asynchronous calls
	 * @param processor
	 */
	protected void afterExecute(CallProcessor processor, Result result) {
		// Asynchronus calls after executes also propagates execution of call chains
		if(result instanceof AsyncResult) {
			AsyncResultImpl asyncResult = (AsyncResultImpl)result;
			asyncResult.afterInvoke();

			Call execCall = processor.getCall();
			// remove from the list of exec calls
			synchronized(execCalls) {
				execCalls.remove(execCall);
			}			

			CallChainProcessor ccProcessor = null;
			if(processor instanceof CallChainProcessor) {
				ccProcessor = (CallChainProcessor)processor;
			}
			
			// continue chain execution	
			if(ccProcessor != null) {
				// @@temp: must be the commented code
				ResultChainImpl resultChain = (ResultChainImpl)execCalls.get(ccProcessor.getCallChain());
				//ResultChainImpl resultChain = (ResultChainImpl)result.getResultChain();
				if(!ccProcessor.done()) {
					Call nextCall = ccProcessor.getNextCall();
					AsyncResultImpl nextResult = asyncInvokeCall(nextCall,processor);
					nextResult.setResultChain(resultChain);
					resultChain.addResult(nextResult);
				}
				else {
					resultChain.setStatus(ccProcessor.getStatus());
					afterExecute(ccProcessor,resultChain);
					ccProcessor.getCallChain().setCompleted(true);					
				}
			}
		}
	}

    /**
     * Both Sync and Async
     * @return Iterator an iterator over currently executing calls
     */
    public Iterator getExecutingCalls() {
        checkInitialized();

        inside = true;

        Iterator it = new java.util.Iterator() {
            private Call[] snapshot = null;
            private int index = 0;

            {
                synchronized(execCalls) {
                    snapshot = new Call[execCalls.size()];
                    execCalls.keySet().toArray(snapshot);
                }
            }

            public boolean hasNext() {
                return index < snapshot.length;
            }

            public Object next() {
                return snapshot[index++];
            }

            public void remove() {
                throw new UnsupportedOperationException("read only iterator");
            }
        };

        inside = false;

        return it;
    }

    protected void initialize() {
        if(initialized) return;
        tPool.setCallback(new Observer());
        tPool.start();
        initialized = true;
    }

    protected void checkInitialized() {
        if(!initialized) throw new IllegalStateException("Not initialized");
    }

    /**
     *
     */
    public void close() {
        checkInitialized();

        // stop any new calls
        initialized = false;

        // wait till existing calls finish
        // synchronized block is required to get the latest state of flag (JMM)
		boolean sleep = false;        
        synchronized(this) {
        	sleep = inside;
        }
        
        while(sleep) {
            try {
                Thread.sleep(100); // sleep for 100 ms
            }
            catch(InterruptedException ex) {
                break; // go ahead
            }
			synchronized(this) {
				sleep = inside;
			}
        }

        // some async calls may be executing
        synchronized(execCalls) {
            if(execCalls.size() > 0) {
                if(logger.beWarning()) {
                    logger.warningT(tracer,"# Calls pending execution: " + execCalls.size());
                }
            }
            execCalls.clear();
        }

        // calls executing will be interrupted
        tPool.shutdown();

        if(logger.beInfo()) {
            logger.infoT(tracer,"Simple Call Manager Closed");
        }
    }
	/* (non-Javadoc)
	 * @see com.sap.isa.crt.CallManager#getName()
	 */
	public String getName() {
		return this.name;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.crt.CallManager#setName(java.lang.String)
	 */
	public void setName(String name) {
		this.name = name;
	}
}