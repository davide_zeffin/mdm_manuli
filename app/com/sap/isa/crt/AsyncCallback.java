package com.sap.isa.crt;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public interface AsyncCallback {
    /**
     * This method is callbacked when the AsyncCall has finished execution
     *
     * @param AsyncResult result
     */
    void completed(AsyncResult result);
}