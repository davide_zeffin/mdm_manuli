package com.sap.isa.crt;

import java.util.Properties;

import com.sap.tc.logging.Category;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public abstract class CallManagerFactory {

	protected Category logger;
	protected boolean initialized = false;
	protected Properties props;
	
    protected CallManagerFactory() {
    }

    /**
     * <p>Current Thread context Classloader is used for setting up Thread context of
     *          invoked calls</p>
     * @param props for looking up CallManager instance
     */
    public CallManager getInstance(Properties props) {
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		return getInstance(cl,props);
    }
    
	public Category getLogger() {
		return logger;
	}
    
	public void setLogger(Category logger) {
		this.logger = logger;
	}

    /**
     * @param ClassLoader cl the classloader to use for setting up Thread context of
     *          invoked calls
     * @param props for looking up CallManager instance
     */
    public abstract CallManager getInstance(ClassLoader cl, Properties props);
    
    /**
     * Client releases the instance of CallManager
     * @param cm
     */
    public abstract void releaseInstance(CallManager cm);
    
    /** Internal life cycle specific*/
	/* (non-Javadoc)
	 * @see com.sap.isa.crt.CallManagerFactory#initialize(java.util.Properties)
	 */
	public void initialize(Properties props) {
		this.props = props;
		initialized = true;
	}

	public Properties getProperties() {
		return props;
	}
	
	public void close() {
		initialized = false;
	}
	
	protected void checkInitialized() {
		if(!initialized) { 
			UserException ex = new UserException("Call Manager Factory not initialized");
			throw ex;
		}
	}
}