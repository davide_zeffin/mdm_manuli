/*
 * Created on Nov 13, 2003
 *
 */
package com.sap.isa.crt;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * @author I802891
 *
 */
public final class CallChainImpl extends AbstractCall implements CallChain {
	
	private boolean completed;

	private ArrayList callList = new ArrayList();
	
	private boolean insideInvoke = false;
	
	private void checkForModify() {
		synchronized(this) {
			if(insideInvoke) {
				throw new UserException("call chain invocation in progress, no changes allowed");				
			}
		}
	}
	
	/* (non-Javadoc)
	 * @see com.sap.isa.crt.CallChain#getCall(int)
	 */
	public Call getCall(int index) {
		return (Call)callList.get(index);
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.crt.CallChain#getCallIterator()
	 */
	public CallIterator callIterator() {
		return new CallIteratorImpl();
	}

	public void addCall(Call call) {
		checkForModify();
		
		if(call == null) throw new IllegalArgumentException("call is null");
		
		if(callList.contains(call)) {
			throw new IllegalArgumentException("call is already part of the chain");
		}
		callList.add(call);
	}
	
	/**
	 * Calls must be added in the order of indexes
	 * @param index
	 * @param call
	 */
	public void addCall(int index, Call call) {
		checkForModify();
		
		if(call == null) throw new IllegalArgumentException("call is null");
		
		if(index == callList.size()) {
			callList.add(call);
		}
		else {
			callList.set(index,call);
		}
	}
	
	public Call removeCall(int index) {
		checkForModify();
		
		return (Call)callList.remove(index);
	}
	
	/* (non-Javadoc)
	 * @see com.sap.isa.crt.CallChain#getSize()
	 */
	public int getSize() {
		return callList.size();
	}

	/* 
	 * (non-Javadoc)
	 * @see com.sap.isa.crt.Call#getParamNames()
	 */
	public String[] getParamNames() {
		String[] params = new String[super.args.size()];
		super.args.keySet().toArray(params);
		return params;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.crt.Call#invoke(com.sap.isa.crt.CallContext)
	 */
	public Object invoke(CallContext ctxt) throws Exception {
		insideInvoke = true;
		
		try {
			// no logic required, do nothing
		}
		finally {
			insideInvoke = false;
		}

		return null;
	}

	private class CallIteratorImpl implements CallIterator {
		Iterator it = callList.iterator();
		/* (non-Javadoc)
		 * @see com.sap.isa.crt.CallChain.CallIterator#hasNext()
		 */
		public boolean hasNext() {
			return it.hasNext();
		}

		/* (non-Javadoc)
		 * @see com.sap.isa.crt.CallChain.CallIterator#nextCall()
		 */
		public Call nextCall() {
			return (Call)it.next();
		}

	}
	/* 
	 * Marker for premature termination of the call
	 * @see com.sap.isa.crt.CallChain#setCompleted()
	 */
	public void setCompleted(boolean val) {
		this.completed = val;
	}
	
	public boolean isCompleted() {
		return completed;
	}
}
