/*
 * Created on Sep 8, 2003
 *
 */
package com.sap.isa.crt;

import java.io.PrintStream;
import java.io.PrintWriter;

/**
 * Wrapper exception for all JDO triggered exceptions
 * @author I802891
 *
 */
public class DatastoreException extends RuntimeException {

	private Throwable th;

	public DatastoreException(String msg) {
		super(msg);
	}

	public DatastoreException(Throwable nested) {
		this.th = nested;
	}

	public Throwable getNestedThrowable() {
		return th;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Throwable#getMessage()
	 */
	public String getMessage() {
		if(th != null) return th.getMessage();
		else return super.getMessage();
	}

	private static final String CAUSED_BY = "\n Caused by:" ;
	
	/* (non-Javadoc)
	 * @see java.lang.Throwable#printStackTrace(java.io.PrintStream)
	 */
	public void printStackTrace(PrintStream s) {
		super.printStackTrace(s);
		if(th != null) {
			s.print(CAUSED_BY);
			th.printStackTrace(s);
		}
	}

	/* (non-Javadoc)
	 * @see java.lang.Throwable#printStackTrace(java.io.PrintWriter)
	 */
	public void printStackTrace(PrintWriter s) {
		super.printStackTrace(s);
		if(th != null) {
			s.print(CAUSED_BY);
			th.printStackTrace(s);
		}
	}

	public static void main(String[] args) {
		Exception nex = new Exception("Nested Exception");
		DatastoreException ex = new DatastoreException(nex);
		ex.printStackTrace(); 
	}
}
