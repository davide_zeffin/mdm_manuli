package com.sap.isa.crt;

import java.util.Properties;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class SimpleCallManagerFactory extends CallManagerFactory {

    public SimpleCallManagerFactory() {
    }
    
	private SimpleCallManager scm = null;    

    /**
     * @param ClassLoader cl the classloader to use for setting up Thread context of
     *          invoked calls
     * @param props for looking up CallManager instance
     */
    public synchronized CallManager getInstance(ClassLoader cl, Properties props) {
    	checkInitialized();
    	if(scm == null) {
	        scm = new SimpleCallManager(this,cl,props);
	        scm.setLogger(logger);
	    }
    	return scm;
    }
    
	/* (non-Javadoc)
	 * @see com.sap.isa.crt.CallManagerFactory#releaseInstance(com.sap.isa.crt.CallManager)
	 */
	public void releaseInstance(CallManager cm) {
		// do nothing
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.crt.CallManagerFactory#close()
	 */
	public void close() {
		if(scm != null) scm.close();
		scm = null;
		super.close();
	}
}