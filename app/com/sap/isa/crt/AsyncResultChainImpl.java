/*
 * Created on Dec 3, 2003
 *
 */
package com.sap.isa.crt;

import java.util.Iterator;
import java.util.LinkedList;

import com.sap.isa.core.logging.IsaLocation;

/**
 * @author I802891
 *
 */
public class AsyncResultChainImpl 
		extends ResultChainImpl 
		implements AsyncResultChain {

			/** 
			  *  The IsaLocation of the current Action.
			  *  This will be instantiated during perform and is always present.
			  */
			 protected static IsaLocation log = IsaLocation.getInstance(AsyncResultChainImpl.class.getName());

	/**
	 * @param callchain
	 */
	AsyncResultChainImpl(CallChain callchain) {
		super(callchain);
	}


	/** List of callbacks */
	private transient LinkedList callbacks;
	
	/* (non-Javadoc)
	 * @see com.sap.isa.crt.AsyncResult#getActualExecutionTime()
	 */
	public long getActualExecutionTime() {
		// TODO Auto-generated method stub
		return 0;
	}


	/* (non-Javadoc)
	 * @see com.sap.isa.crt.AsyncResult#waitForCompletion()
	 */
	public void waitForCompletion() {
		synchronized(this) {
			if(!isCompleted()) {
				try {
					this.wait();
				} catch (InterruptedException e) {
					log.debug("Error ", e);
				}
			}
		}

	}

	/**
	 * @see AsyncCallback
	 */
	public void addCallback(AsyncCallback cb) {
		if(callbacks == null) callbacks = new LinkedList();
		synchronized(callbacks) {
			if(!callbacks.contains(cb)) callbacks.add(cb);
		}
	}

	public void removeCallback(AsyncCallback cb) {
		if(callbacks != null) {
			synchronized(callbacks) {
				 callbacks.remove(cb);
			}
		}
	}

	/**
	 * This is called after the Call has been invoked by Call Manager
	 */
	void afterInvoke() {
		/** notify all waiting threads*/
		synchronized(this) {
			this.notifyAll();
		}

		if(callbacks != null) {
			synchronized(callbacks) {
				Iterator it = callbacks.iterator();

				while(it.hasNext()) {
					AsyncCallback cb = (AsyncCallback)it.next();
					try {
						cb.completed(this);
					}
					catch(RuntimeException ex) {
						log.debug("Error ", ex);
					}
				}
			}
		}
	}
	/* (non-Javadoc)
	 * @see com.sap.isa.crt.ResultChain#setCompleted()
	 */
	public void setStatus(ResultStatusEnum status) {
		super.setStatus(status);
		if(isCompleted()) {		
			synchronized(this) {
				this.notifyAll();
			}
			afterInvoke();
		}
	}

}
