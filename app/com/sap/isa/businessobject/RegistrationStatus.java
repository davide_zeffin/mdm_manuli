/*****************************************************************************
    Class:        RegistrationStatus
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Andreas Jessen
    Created:      29.10.2001
    Version:      1.0
*****************************************************************************/

package com.sap.isa.businessobject;


/**
 * the class provides fix status values
 * for the user login process
 *
 */
public class RegistrationStatus {


   /**
    * possible return value of the
    * register method
    * indicates if the method was sucessful
    *
    */
    public final static RegistrationStatus OK = new RegistrationStatus();


   /**
    * possible return value of the
    * register method
    * indicates if the method was not sucessful
    *
    */
    public final static RegistrationStatus NOT_OK = new RegistrationStatus();


   /**
    * special return value for the register method
    * indicates that the selection of a county is
    * necessary
    */
    public final static RegistrationStatus NOT_OK_ENTER_COUNTY = new RegistrationStatus();


    private RegistrationStatus(){};


    public String toString() {

       if (this == OK) {
         return "OK";
       } else if (this == NOT_OK) {
         return "NOT_OK";
       } else if (this == NOT_OK_ENTER_COUNTY) {
         return "NOT_OK_ENTER_COUNTY";
       }

       return null; // never reached but the compiler wants it
     }


    public int toInteger() {

      if (this == OK) {
         return 0;
      } else if (this == NOT_OK) {
         return 1;
      } else if (this == NOT_OK_ENTER_COUNTY) {
         return 2;
      }

      return -1; // never reached but the compiler wants it
    }
  }