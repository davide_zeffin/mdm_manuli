/*****************************************************************************
    Class:        Address
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Created:      19.3.2001
    Version:      1.0

    $Revision: #3 $
    $Date: 2002/05/23 $
*****************************************************************************/
package com.sap.isa.businessobject;

import com.sap.isa.backend.boi.isacore.AddressData;


/**
 *
 *
 *
 */
public class Address extends BusinessObjectBase implements AddressData {

    private String id = "";
    private String type = "";
    private String origin = "";
    private String personNumber = "";

    private String titleKey = "";
    private String title = "";
    private String titleAca1Key = "";
    private String titleAca1 = "";
    private String firstName = "";
    private String lastName = "";
    private String birthName = "";
    private String secondName = "";
    private String middleName = "";
    private String nickName = "";
    private String initials = "";
    private String name1 = "";
    private String name2 = "";
    private String name3 = "";
    private String name4 = "";
    private String coName = "";
    private String city = "";
    private String district = "";
    private String postlCod1 = "";
    private String postlCod2 = "";
    private String postlCod3 = "";
    private String pcode1Ext = "";
    private String pcode2Ext = "";
    private String pcode3Ext = "";
    private String poBox = "";
    private String poWoNo = "";
    private String poBoxCit = "";
    private String poBoxReg = "";
    private String poBoxCtry = "";
    private String poCtryISO = "";
    private String street = "";
    private String strSuppl1 = "";
    private String strSuppl2 = "";
    private String strSuppl3 = "";
    private String location = "";
    private String houseNo = "";
    private String houseNo2 = "";
    private String houseNo3 = "";
    private String building = "";
    private String floor = "";
    private String roomNo = "";
    private String country = "";
    private String countryISO = "";
    private String region = "";
    private String homeCity = "";
    private String taxJurCode = "";
    private String tel1Numbr = "";
    private String tel1Ext = "";
    private String faxNumber = "";
    private String faxExtens = "";
    private String eMail = "";
    private String countryText = "";
    private String regionText50 = "";
    private String regionText15 = "";

    private String addressPartner = "";

    private String category = "";

    /**
      *  simple constructor
      *
      */
    public Address(){
    }



    /**
     *
     *
     */
    public Address(String titleKey,
                   String title,
                   String titleAca1Key,
                   String titleAca1,
                   String firstName,
                   String lastName,
                   String birthName,
                   String secondName,
                   String middleName,
                   String nickName,
                   String initials,
                   String name1,
                   String name2,
                   String name3,
                   String name4,
                   String coName,
                   String city,
                   String district,
                   String postlCod1,
                   String postlCod2,
                   String postlCod3,
                   String pcode1Ext,
                   String pcode2Ext,
                   String pcode3Ext,
                   String poBox,
                   String poWoNo,
                   String poBoxCit,
                   String poBoxReg,
                   String poBoxCtry,
                   String poCtryISO,
                   String street,
                   String strSuppl1,
                   String strSuppl2,
                   String strSuppl3,
                   String location,
                   String houseNo,
                   String houseNo2,
                   String houseNo3,
                   String building,
                   String floor,
                   String roomNo,
                   String country,
                   String countryISO,
                   String region,
                   String homeCity,
                   String taxJurCode,
                   String tel1Numbr,
                   String tel1Ext,
                   String faxNumber,
                   String faxExtens,
                   String eMail) {

        // this();
        this.titleKey = titleKey;
        this.title    = title;
        this.titleAca1Key = titleAca1Key;
        this.titleAca1 = titleAca1;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthName = birthName;
        this.secondName = secondName;
        this.middleName = middleName;
        this.nickName = nickName;
        this.initials = initials;
        this.name1 = name1;
        this.name2 = name2;
        this.name3 = name3;
        this.name4 = name4;
        this.coName = coName;
        this.city = city;
        this.district = district;
        this.postlCod1 = postlCod1;
        this.postlCod2 = postlCod2;
        this.postlCod3 = postlCod3;
        this.pcode1Ext = pcode1Ext;
        this.pcode2Ext = pcode2Ext;
        this.pcode3Ext = pcode3Ext;
        this.poBox = poBox;
        this.poWoNo = poWoNo;
        this.poBoxCit = poBoxCit;
        this.poBoxReg = poBoxReg;
        this.poBoxCtry = poBoxCtry;
        this.poCtryISO = poCtryISO;
        this.street = street;
        this.strSuppl1 = strSuppl1;
        this.strSuppl2 = strSuppl2;
        this.strSuppl3 = strSuppl3;
        this.location = location;
        this.houseNo = houseNo;
        this.houseNo2 = houseNo2;
        this.houseNo3 = houseNo3;
        this.building = building;
        this.floor = floor;
        this.roomNo = roomNo;
        this.country = country;
        this.countryISO = countryISO;
        this.region = region;
        this.homeCity = homeCity;
        this.taxJurCode = taxJurCode;
        this.tel1Numbr = tel1Numbr;
        this.tel1Ext = tel1Ext;
        this.faxNumber = faxNumber;
        this.faxExtens = faxExtens;
        this.eMail = eMail;

    }


    /**
     *
     */
    public void setAddressValues
    (String titleKey,
     String title,
     String titleAca1Key,
     String titleAca1,
     String firstName,
     String lastName,
     String birthName,
     String secondName,
     String middleName,
     String nickName,
     String initials,
     String name1,
     String name2,
     String name3,
     String name4,
     String coName,
     String city,
     String district,
     String postlCod1,
     String postlCod2,
     String postlCod3,
     String pcode1Ext,
     String pcode2Ext,
     String pcode3Ext,
     String poBox,
     String poWoNo,
     String poBoxCit,
     String poBoxReg,
     String poBoxCtry,
     String poCtryISO,
     String street,
     String strSuppl1,
     String strSuppl2,
     String strSuppl3,
     String location,
     String houseNo,
     String houseNo2,
     String houseNo3,
     String building,
     String floor,
     String roomNo,
     String country,
     String countryISO,
     String region,
     String homeCity,
     String taxJurCode,
     String tel1Numbr,
     String tel1Ext,
     String faxNumber,
     String faxExtens,
     String eMmail) {

        this.titleKey = titleKey;
        this.title    = title;
        this.titleAca1Key = titleAca1Key;
        this.titleAca1 = titleAca1;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthName = birthName;
        this.secondName = secondName;
        this.middleName = middleName;
        this.nickName = nickName;
        this.initials = initials;
        this.name1 = name1;
        this.name2 = name2;
        this.name3 = name3;
        this.name4 = name4;
        this.coName = coName;
        this.city = city;
        this.district = district;
        this.postlCod1 = postlCod1;
        this.postlCod2 = postlCod2;
        this.postlCod3 = postlCod3;
        this.pcode1Ext = pcode1Ext;
        this.pcode2Ext = pcode2Ext;
        this.pcode3Ext = pcode3Ext;
        this.poBox = poBox;
        this.poWoNo = poWoNo;
        this.poBoxCit = poBoxCit;
        this.poBoxReg = poBoxReg;
        this.poBoxCtry = poBoxCtry;
        this.poCtryISO = poCtryISO;
        this.street = street;
        this.strSuppl1 = strSuppl1;
        this.strSuppl2 = strSuppl2;
        this.strSuppl3 = strSuppl3;
        this.location = location;
        this.houseNo = houseNo;
        this.houseNo2 = houseNo2;
        this.houseNo3 = houseNo3;
        this.building = building;
        this.floor = floor;
        this.roomNo = roomNo;
        this.country = country;
        this.countryISO = countryISO;
        this.region = region;
        this.homeCity = homeCity;
        this.taxJurCode = taxJurCode;
        this.tel1Numbr = tel1Numbr;
        this.tel1Ext = tel1Ext;
        this.faxNumber = faxNumber;
        this.faxExtens = faxExtens;
        this.eMail = eMmail;

    }


    public boolean equals(Address address) {
        // check if changeable properties have been changed
        if( ( this.name1.equalsIgnoreCase(address.name1) ) &&
            ( this.name2.equalsIgnoreCase(address.name2) ) &&
            ( this.city.equalsIgnoreCase(address.city) ) &&
            ( this.postlCod1.equalsIgnoreCase(address.postlCod1) ) &&
            ( this.street.equalsIgnoreCase(address.street) ) &&
            ( this.houseNo.equalsIgnoreCase(address.houseNo) ) &&
            ( this.country.equalsIgnoreCase(address.country) ) &&
            ( this.region.equalsIgnoreCase(address.region) ) &&
            ( this.tel1Numbr.equalsIgnoreCase(address.tel1Numbr) ) &&
            ( this.faxNumber.equalsIgnoreCase(address.faxNumber) ) &&
            ( this.eMail.equalsIgnoreCase(address.eMail) ) ) {
            return true;
        }
        else {
            return false;
        }
    }



    /**
     * Set the property id
     *
     * @param id
     *
     */
    public void setId(String id) {
        this.id = id;
    }


    /**
     * Returns the property id
     *
     * @return id
     *
     */
    public String getId() {
       return this.id;
    }


    /**
     * Set the property type
     *
     * @param type
     *
     */
    public void setType(String type) {
        this.type = type;
    }


    /**
     * Returns the property type
     *
     * @return type
     *
     */
    public String getType() {
       return this.type;
    }

	public String getCategory() {
	   return this.category;
	}



    /**
     * Set the property origin
     *
     * @param origin
     *
     */
    public void setOrigin(String origin) {
        this.origin = origin;
    }


    /**
     * Returns the property origin
     *
     * @return origin
     *
     */
    public String getOrigin() {
       return this.origin;
    }


    /**
     * Set the property personNumber
     *
     * @param personNumber
     *
     */
    public void setPersonNumber(String personNumber) {
        this.personNumber = personNumber;
    }


    /**
     * Returns the property personNumber
     *
     * @return personNumber
     *
     */
    public String getPersonNumber() {
       return this.personNumber;
    }



    // setter methods
    public void setTitleKey(String titleKey) {
        this.titleKey = titleKey;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setTitleAca1Key(String titleAca1Key) {
        this.titleAca1Key = titleAca1Key;
    }

    public void setTitleAca1(String titleAca1) {
        this.titleAca1 = titleAca1;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setBirthName(String birthName) {
        this.birthName = birthName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public void setInitials( String initials) {
        this.initials = initials;
    }

    public void setName1(String name1) {
        this.name1 = name1;
    }

    public void setName2(String name2) {
        this.name2 = name2;
    }

    public void setName3(String name3) {
        this.name3 = name3;
    }

    public void setName4(String name4) {
        this.name4 = name4;
    }

    public void setCoName(String coName) {
        this.coName = coName;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public void setPostlCod1(String postlCod1) {
        this.postlCod1 = postlCod1;
    }

    public void setPostlCod2(String postlCod2) {
        this.postlCod2 = postlCod2;
    }

    public void setPostlCod3(String postlCod3) {
        this.postlCod3 = postlCod3;
    }

    public void  setPcode1Ext(String pcode1Ext) {
        this.pcode1Ext = pcode1Ext;
    }

    public void setPcode2Ext(String pcode2Ext) {
        this.pcode2Ext = pcode2Ext;
    }

    public void setPcode3Ext(String pcode3Ext) {
        this.pcode3Ext = pcode3Ext;
    }

    public void setPoBox(String poBox) {
        this.poBox = poBox;
    }

    public void setPoWoNo(String poWoNo) {
        this.poWoNo = poWoNo;
    }

    public void setPoBoxCit(String poBoxCit) {
        this.poBoxCit = poBoxCit;
    }

    public void setPoBoxReg(String poBoxReg) {
        this.poBoxReg = poBoxReg;
    }

    public void setPoBoxCtry(String poBoxCtry) {
        this.poBoxCtry = poBoxCtry;
    }

    public void setPoCtryISO(String poCtryISO) {
        this.poCtryISO = poCtryISO;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public void setStrSuppl1(String strSuppl1) {
        this.strSuppl1 = strSuppl1;
    }

    public void setStrSuppl2(String strSuppl2) {
        this.strSuppl2 = strSuppl2;
    }

    public void setStrSuppl3(String strSuppl3) {
        this.strSuppl3 = strSuppl3;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setHouseNo(String houseNo) {
        this.houseNo = houseNo;
    }

    public void setHouseNo2(String houseNo2) {
        this.houseNo2 = houseNo2;
    }

    public void setHouseNo3(String houseNo3) {
        this.houseNo3 = houseNo3;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public void setRoomNo(String roomNo) {
        this.roomNo = roomNo;
    }

    public void setCountry(String country)  {
        this.country = country ;
    }

    public void setCountryISO(String countryISO) {
        this.countryISO = countryISO;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setHomeCity(String homeCity) {
        this.homeCity = homeCity;
    }

    public void setTaxJurCode(String taxJurCode) {
        this.taxJurCode = taxJurCode;
    }

    public void setTel1Numbr(String tel1Numbr) {
        this.tel1Numbr = tel1Numbr;
    }

    public void setTel1Ext(String tel1Ext) {
        this.tel1Ext = tel1Ext;
    }

    public void setFaxNumber(String faxNumber) {
        this.faxNumber = faxNumber;
    }

    public void setFaxExtens(String faxExtens) {
        this.faxExtens = faxExtens;
    }


    public void setAddressPartner(String partner) {
        this.addressPartner = partner;
    }


    // getter methods
    public String getTitleKey() {
        return this.titleKey;
    }

    public String getTitle() {
        return this.title;
    }

    public String getTitleAca1Key() {
        return this.titleAca1Key;
    }

    public String getTitleAca1() {
        return this.titleAca1;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public String getBirthName() {
        return this.birthName;
    }

    public String getSecondName() {
        return this.secondName;
    }

    public String getMiddleName() {
        return this.middleName;
    }

    public String getNickName() {
        return this.nickName;
    }

    public String getInitials() {
        return this.initials;
    }

    public String getName1() {
        return this.name1;
    }

    public String getName2() {
        return this.name2;
    }

    public String getName3() {
        return this.name3;
    }

    public String getName4() {
        return this.name4;
    }

    public String getCoName() {
        return this.coName;
    }

    public String getCity() {
        return this.city;
    }

    public String getDistrict() {
        return this.district;
    }

    public String getPostlCod1() {
        return this.postlCod1;
    }

    public String getPostlCod2() {
        return this.postlCod2;
    }

    public String getPostlCod3() {
        return this.postlCod3;
    }

    public String  getPcode1Ext() {
        return this.pcode1Ext;
    }

    public String getPcode2Ext() {
        return this.pcode2Ext;
    }

    public String getPcode3Ext() {
        return this.pcode3Ext;
    }

    public String getPoBox() {
        return this.poBox;
    }

    public String getPoWoNo() {
        return this.poWoNo;
    }

    public String getPoBoxCit() {
        return this.poBoxCit;
    }

    public String getPoBoxReg() {
        return this.poBoxReg;
    }

    public String getPoBoxCtry() {
        return this.poBoxCtry;
    }

    public String getPoCtryISO() {
        return this.poCtryISO;
    }

    public String getStreet() {
        return this.street;
    }

    public String getStrSuppl1() {
        return this.strSuppl1;
    }

    public String getStrSuppl2() {
        return this.strSuppl2;
    }

    public String getStrSuppl3() {
        return this.strSuppl3;
    }

    public String getLocation() {
        return this.location;
    }

    public String getHouseNo() {
        return this.houseNo;
    }

    public String getHouseNo2() {
        return this.houseNo2;
    }

    public String getHouseNo3() {
        return this.houseNo3;
    }

    public String getBuilding() {
        return this.building;
    }

    public String getFloor() {
        return this.floor;
    }

    public String getRoomNo() {
        return this.roomNo;
    }

    public String getCountry()  {
        return this.country ;
    }

    public String getCountryISO() {
        return this.countryISO;
    }

    public String getRegion() {
        return this.region;
    }

    public String getHomeCity() {
        return this.homeCity;
    }

    public String getTaxJurCode() {
        return this.taxJurCode;
    }

    public String getTel1Numbr() {
        return this.tel1Numbr;
    }

    public String getTel1Ext() {
        return this.tel1Ext;
    }

    public String getFaxNumber() {
        return this.faxNumber;
    }

    public String getFaxExtens() {
        return this.faxExtens;
    }

    public String getAddressPartner() {
        return this.addressPartner;
    }

    /**
     * Set the property eMail
     *
     * @param eMail
     *
     */
    public void setEMail(String eMail) {
        this.eMail = eMail;
    }


    /**
     * Returns the property eMail
     *
     * @return eMail
     *
     */
    public String getEMail() {
        return this.eMail;
    }


    public String getName() {

        if(this.lastName.length() > 0) {
            return this.lastName;
        }
        else {
            return this.name1;
        }
    }


    public void setCountryText(String countryText){
        this.countryText = countryText;
    }

    public String getCountryText(){
        return countryText;
    }

    public void setRegionText50(String regionText50){
        this.regionText50 = regionText50;
    }

    public String getRegionText50(){
        return regionText50;
    }

    public void setRegionText15(String regionText15){
        this.regionText15 = regionText15;
    }

	public void setCategory(String category) {
		this.category = category;
	}


    public Object clone() {

       Address addressClone = new Address();

       addressClone.setId(this.id);
       addressClone.setType(this.type);
       addressClone.setOrigin(this.origin);
       addressClone.setPersonNumber(this.personNumber);
       addressClone.setTitleKey(this.titleKey);
       addressClone.setTitle(this.title);
       addressClone.setTitleAca1Key(this.titleAca1Key);
       addressClone.setTitleAca1(this.titleAca1);
       addressClone.setFirstName(this.firstName);
       addressClone.setLastName(this.lastName);
       addressClone.setBirthName(this.birthName);
       addressClone.setSecondName(this.secondName);
       addressClone.setMiddleName(this.middleName);
       addressClone.setNickName(this.nickName);
       addressClone.setInitials(this.initials);
       addressClone.setName1(this.name1);
       addressClone.setName2(this.name2);
       addressClone.setName3(this.name3);
       addressClone.setName4(this.name4);
       addressClone.setCoName(this.coName);
       addressClone.setCity(this.city);
       addressClone.setDistrict(this.district);
       addressClone.setPostlCod1(this.postlCod1);
       addressClone.setPostlCod2(this.postlCod2);
       addressClone.setPostlCod3(this.postlCod3);
       addressClone.setPcode1Ext(this.pcode1Ext);
       addressClone.setPcode2Ext(this.pcode2Ext);
       addressClone.setPcode3Ext(this.pcode3Ext);
       addressClone.setPoBox(this.poBox);
       addressClone.setPoWoNo(this.poWoNo);
       addressClone.setPoBoxCit(this.poBoxCit);
       addressClone.setPoBoxReg(this.poBoxReg);
       addressClone.setPoBoxCtry(this.poBoxCtry);
       addressClone.setPoCtryISO(this.poCtryISO);
       addressClone.setStreet(this.street);
       addressClone.setStrSuppl1(this.strSuppl1);
       addressClone.setStrSuppl2(this.strSuppl2);
       addressClone.setStrSuppl3(this.strSuppl3);
       addressClone.setLocation(this.location);
       addressClone.setHouseNo(this.houseNo);
       addressClone.setHouseNo2(this.houseNo2);
       addressClone.setHouseNo3(this.houseNo3);
       addressClone.setBuilding(this.building);
       addressClone.setFloor(this.floor);
       addressClone.setRoomNo(this.roomNo);
       addressClone.setCountry(this.country);
       addressClone.setCountryISO(this.countryISO);
       addressClone.setRegion(this.region);
       addressClone.setHomeCity(this.homeCity);
       addressClone.setTaxJurCode(this.taxJurCode);
       addressClone.setTel1Numbr(this.tel1Numbr);
       addressClone.setTel1Ext(this.tel1Ext);
       addressClone.setFaxNumber(this.faxNumber);
       addressClone.setFaxExtens(this.faxExtens);
       addressClone.setEMail(this.eMail);
       addressClone.setAddressPartner(this.addressPartner);

       return addressClone;
    }

}