package com.sap.isa.businessobject;

import java.util.HashSet;

import com.sap.isa.isacore.ManagedDocumentLargeDoc;

/**
 * Title:        ManagedDocumentDetermination
 * Description:  ManagedDocument, to store some additional informations for the determination process
 * Copyright:    Copyright (c) 2003
 * Company:      SAP AG
 * @author      SAP AG
 * @version 1.0
 */

public class ManagedDocumentDetermination extends ManagedDocumentLargeDoc {

    /**
     * attribute to store what action called the determination
     */
    protected String caller = "";
    
    /**
     * attribute to store the mode of the document
     */
    protected String docMode = "";
    
    /**
     * attribute  to store type of document that should be simulated, in case
     * product determination was called from the simulation Action.
     */
    protected String simulatedDoc = "";
    
    /**
     * attribute  to store for which items the user has choosen, to not
     * select an alternative product
     */
    protected HashSet selectedNone = new HashSet();
    
    /**
     * Possible callers of the product determination
     */
    public static final String CALLER_MAINTAIN_BASKET_REFRESH = "basketrefresh";
    public static final String CALLER_MAINTAIN_BASKET_SEND = "basketsend";
    public static final String CALLER_MAINTAIN_BASKET_SIMULATE ="basketsimulate";
    public static final String CALLER_MAINTAIN_BASKET_POCESS_SOLDTO ="basketsoldto";
    public static final String CALLER_MAINTAIN_BASKET_REPLACE_ITEM ="basketrepitem";
    public static final String CALLER_MAINTAIN_BASKET_BATCH = "basketbatch";

    public static final String CALLER_MAINTAIN_ORDER_REFRESH = "orderrefresh";
    public static final String CALLER_MAINTAIN_ORDER_SEND = "ordersend";
    public static final String CALLER_MAINTAIN_ORDER_SIMULATE = "ordersimulate";
    public static final String CALLER_MAINTAIN_ORDER_REPLACE_ITEM ="orderrepitem";
    public static final String CALLER_MAINTAIN_ORDER_BATCH = "orderbatch";

    /**
     * allowed doc_modes
     */
    public static final String DOC_MODE_NEW = "new";
    public static final String DOC_MODE_CHANGE = "change";
    public static final String DOC_MODE_SIMULATE = "simulate";

    /**
     *  Constructor
     *
     *@param  doc            Reference off the corresponding business object.
     *@param  docType        Type of the document, e.g. "order", "quotation", "order template".
     *@param  docNumber      Number of the document, given by the backend.
     *@param  refNumber      The reference number; the number the customer can assign to the document.
     *@param  refName        The reference name; the name the customer can assign to the document.
     *@param  docDate        Date of the document.
     *@param  forward        String for the logical forward mapping in the struts framework.
     *@param  href           Href for displaying the document
     *@param  hrefParameter  Additional parameters for the String href.
     */
    public ManagedDocumentDetermination(DocumentState doc, String docType, String docNumber, String refNumber,
            String refName, String docDate, String forward, String href, String hrefParameter) {

            super(doc, docType, docNumber, refNumber, refName, docDate, forward, href, hrefParameter);
    }
    
    /**
     * Returns the caller.
     * @return String
     */
    public String getCaller() {
        return caller;
    }

    /**
     * Returns the docMode.
     * @return String
     */
    public String getDocMode() {
        return docMode;
    }

    /**
     * Returns the simualtedDoc.
     * @return String
     */
    public String getSimulatedDoc() {
        return simulatedDoc;
    }

    /**
     * Sets the caller.
     * @param caller The caller to set
     */
    public void setCaller(String caller) {
        this.caller = caller;
    }

    /**
     * Sets the docMode.
     * @param docMode The docMode to set
     */
    public void setDocMode(String docMode) {
        this.docMode = docMode;
    }

    /**
     * Sets the simualtedDoc.
     * @param simualtedDoc The simualtedDoc to set
     */
    public void setSimulatedDoc(String simualtedDoc) {
        this.simulatedDoc = simualtedDoc;
    }

    /**
     * Returns the selectedNone.
     * @return HashSet
     */
    public HashSet getSelectedNone() {
        return selectedNone;
    }

    /**
     * Sets the selectedNone.
     * @param selectedNone The selectedNone to set
     */
    public void setSelectedNone(HashSet selectedNone) {
        this.selectedNone = selectedNone;
    }

}
