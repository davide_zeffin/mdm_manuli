/*
 * Created on 17.03.2006
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.businessobject;

import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;

/**
 * @author SAP AG
 *
 * General interface for UI classes
 * The interface should be used in JSPs for several types of item lists (ProductList, WebCatItemList)
 */
public interface ProductListBaseData extends BusinessObjectBaseData {

	/**
	 * Returns an item of type ProductsBase
	 *
	 * @return item of type ProductsBase
	 */
	public ProductBaseData getProduct(int index);

	/**
	 * Returns the size of the list of items
	 *
	 * @return size as int
	 */
	public int size();



}
