package com.sap.isa.businessobject.item;

import java.util.Locale;

import com.sap.isa.backend.boi.isacore.item.ContractDurationData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.WebUtil;

/**
 * this class describes a contract duration.
 */
public class ContractDuration implements ContractDurationData {
    
    private static IsaLocation log = IsaLocation.getInstance(ContractDuration.class.getName());
    
	private String value;
	private String unit;

	public static final String DAY = "1";
	public static final String MONTH = "2";
	public static final String YEAR = "3";

	/**
	 * constructor with 2 parameters: value for the contract duration and unit for the contract duration unit
	 */
	public ContractDuration(String value, String unit) {
        log.entering("ContractDuration("+value+","+unit+")");
        this.value = delete0(value);
		this.unit = unit;
        log.exiting();
	}

	/**
	 * this constructor initializes the member variable with an empty String 
	 */
	public ContractDuration() {
        log.entering("ContractDuration()");
		this.value = "";
		this.unit = "";
        log.exiting();
	}
    
    private String delete0(String value) {
        log.entering("delete0("+value+");");
        try {
            value = Integer.parseInt(value)+"";
        }
        catch(Exception e) {
            log.error("Exception:"+e.getMessage());    
        }
        log.debug("returned value: "+value);
        log.exiting();
        return value;
    }

	/**
	 * returns the unit for the contract duration
	 * @return "" or ContractDuration.DAY or ContractDuration.MONTH or ContractDuration.YEAR
	 */
	public String getUnit() {
        log.entering("getUnit");
        log.exiting();
		return unit;
	}

	/**
	 * returns the contract duration
	 * @return the contract duration as a String
	 */
	public String getValue() {
        log.entering("getValue()");
        log.exiting();
		return value;
	}

	/**
	 * sets the contract duration unit
	 * 
	 * @param value represents the unit of the duration of the contract as a String
	 */
	public void setUnit(String unit) {
        log.entering("setUnit");
        log.exiting();
		this.unit = unit;
	}

	/**
	 * sets the contract duration
	 * 
	 * @param value represents the duration of the contract as int
	 */
	public void setValue(String value) {
        log.entering("setValue");
        log.exiting();
        if(value != null) {
            this.value = value.trim();
        }
        else {
            this.value = value;
        }
	}

	public String toString(Locale locale) {
        log.entering("toString()");
        log.exiting();
		StringBuffer response = new StringBuffer();
		response.append(value).append(" ").append(unitToString(locale));

		return response.toString();
	}

	private String unitToString(Locale locale) {
        log.entering("unitToString()");
		if (value.equals("")) {
			return "";
		}
		boolean singular = value.equals("0") || value.equals("1");
        log.debug("unit: "+unit+" value:"+value+" singular:"+singular);
        log.exiting();
		if (unit.equals(ContractDuration.DAY)) {
			if (singular) {
				return WebUtil.translate(locale, "contractDuration.day", null);
            } else {
				return WebUtil.translate(locale, "contractDuration.days", null);
			}
		}
		if (unit.equals(ContractDuration.MONTH)) {
			if (singular) {
                return WebUtil.translate(locale, "contractDuration.month", null);
			} else {
                return WebUtil.translate(locale, "contractDuration.months", null);
			}
		}
		if (unit.equals(ContractDuration.YEAR)) {
			if (singular) {
                return WebUtil.translate(locale, "contractDuration.year", null);
			} else {
                return WebUtil.translate(locale, "contractDuration.years", null);
			}
		}
		return "";
	}
}
