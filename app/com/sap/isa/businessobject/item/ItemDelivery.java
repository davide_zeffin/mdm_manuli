/*****************************************************************************
    Class:        ItemDelivery
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author        Ralf Witt
    Created:      April 2001

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/


package com.sap.isa.businessobject.item;

import com.sap.isa.backend.boi.isacore.order.ItemDeliveryData;
import com.sap.isa.businessobject.BusinessObjectBase;

/**
 *
 * The ItemDelivery class defines the structure for the item
 * Item delivery information
 *
 */
public class ItemDelivery extends BusinessObjectBase
                          implements ItemDeliveryData {
    /*
     *  see setter methods for the documentation of private variables
     */
    // private TechKey techKey;                 // DeliveryNumber GUID
    private String objectId;                    // DeliveryNumber ID
    private String deliveryDate;
    private String quantity;
    private String unitOfMeasurement;
    private String trackingURL;
    private boolean isShipped = false;
    private String shippingDate;
    private String deliveryPosition;

    /**
     *
     * Constructor to create a delivery
     *
     */
     public ItemDelivery() {
     }

    /**
     * Returns the objectId (Delivery Number)
     *
     * @return objectId is used to identify the delivery in the backend
     */
    public String getObjectId() {
        return objectId;
    }

    /**
     * Returns the delivery position
     *
     * @return deliveryPosition is used to identify the position
     * within the delivery
     */
    public String getDeliveryPosition() {
        return deliveryPosition;
    }

    /**
     * Returns the date item has been delivered
     *
     * @return deliveryDate
     */
    public String getDeliveryDate() {
        return deliveryDate;
    }
    /**
     * Returns the delivered quantity
     *
     * @return quantity
     */
    public String getQuantity() {
        return quantity;
    }
    /**
     * Returns unit of measurement in which the items has been delivered
     *
     * @return unitOfMeasurement
     */
    public String getUnitOfMeasurement() {
        return unitOfMeasurement;
    }
    /**
     * Returns a URL which leads to a webpage with package tracking information
     *
     * @return trackingURL
     */
    public String getTrackingURL() {
        return trackingURL;
    }
    /**
     * Sets objectId
     *
     */
    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }
    /**
     * Sets deliveryPosition
     *
     */
    public void setDeliveryPosition(String deliveryPosition) {
        this.deliveryPosition = deliveryPosition;
    }
    /**
     * Sets Delivery date
     *
     */
    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }
    /**
     * Sets Quantity
     *
     */
    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }
    /**
     * Sets Unit of Measurement
     *
     */
    public void setUnitOfMeasurement(String uom) {
        this.unitOfMeasurement = uom;
    }
    /**
     * Sets Tracking URL
     *
     */
    public void setTrackingURL(String trackURL) {
        this.trackingURL = trackURL;
    }
    /**
     * Sets Shipping date
     */
    public void setShippingDate(String shippingDate) {
        this.shippingDate = shippingDate;
        this.isShipped = true;
    }
    /**
     * Returns the date of shipment
     *
     * @return goodsIssueDate
     */
    public String getShippingDate() {
        return shippingDate;
    }
    /**
     * Returns Status of shipping
     *
     * @return boolean
     */
    public boolean isShipped() {
        return isShipped;
    }
}
