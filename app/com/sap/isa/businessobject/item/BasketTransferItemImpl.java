/*****************************************************************************
    Class:        BasketTransferItemImpl
    Copyright (c) 2001, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      24.04.2001
    Version:      1.0

    $Revision: #6 $
    $Date: 2002/12/06 $
*****************************************************************************/

package com.sap.isa.businessobject.item;

import com.sap.isa.backend.boi.isacore.order.TextData;
import com.sap.isa.businessobject.BatchCharList;
import com.sap.isa.businessobject.ShipTo;
import com.sap.isa.businessobject.order.CampaignList;
import com.sap.isa.businessobject.order.ExtRefObjectList;
import com.sap.isa.businessobject.order.ExternalReferenceList;
import com.sap.isa.businessobject.order.Text;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.businessobject.BOBase;
/**
 * Default implemantation for the <code>BasketTransferItem</code> interface.
 * Use this class, if you want to add items to a salesdocument using
 * the OCI- or CUA- or Bestseller-Interface.
 * You are free to implement the interface with your own class, if this
 * is more convenient for you.
 *
 * @author SAP AG
 * @version 1.0
 */
public class BasketTransferItemImpl
	extends BOBase 
	implements BasketTransferItem {
	private String quantity;
	private String unit;
	private String productKey;
	private String productId;
	private Object configurationItem;
	private String contractKey;
	private String contractId;
	private String contractItemKey;
	private String contractItemId;
	private String contractConfigFilter;
	private ItemSalesDoc basketItemToReplace;
	private String catalog; // the catalog-guid
	private String variant; // the variant for the catalog
	private String catalogCategory; // where do I get this one from
	private String customerProductNumber;
	private String reqDeliveryDate;
	private Text text; // Text to transfer
	private BatchCharList batchCharListJSP;
	private ShipTo shipTo;
	private String handle;
	private String description;
	private String netPrice;
	private String currency;
	private boolean isRedemptionItem;
	private boolean isBuyPtsItem;
	private boolean isDataSetExternally;
	private CampaignList assignedCampaigns = new CampaignList();
	private ExtRefObjectList assignedExtRefObjects = new ExtRefObjectList();
	private ExternalReferenceList assignedExternalReferences = new ExternalReferenceList();
	private String deliveryPriority;
	private String parentTechKey;
	private String techKey;
	private boolean exploded;
	private boolean mainItem;
	private boolean dependentComponent;
	private boolean salesComponent;
	private boolean ratePlanCombination;
    private String group;
    private String scSelection;
    private TechKey scDocumentGuid;
    private ContractDuration selectedContractDuration;
    private boolean isScSelected;
    private String source;

	/**
	 * Constructor
	 */
	public BasketTransferItemImpl() {
	}

	/**
	 * Set the property quantity
	 *
	 * @param quantity the new quantity
	 *
	 */
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	/**
	 * Returns the property quantity
	 *
	 * @return quantity
	 *
	 */
	public String getQuantity() {
		return quantity;
	}

	/**
	 * Set the property unit
	 *
	 * @param unit the new unit
	 *
	 */
	public void setUnit(String unit) {
		this.unit = unit;
	}

	/**
	 * Returns the property unit
	 *
	 * @return unit
	 *
	 */
	public String getUnit() {
		return unit;
	}

	/**
	 * Set the property text
	 *
	 * @param text the new text
	 *
	 */
	public void setText(Text text) {
		this.text = text;
	}

	/**
	 * Returns the property text
	 *
	 * @return text
	 *
	 */
	public TextData getText() {
		return (TextData) text;
	}

	/**
	 * Set the property product
	 *
	 * @param productKey the new product
	 *
	 */
	public void setProductKey(String productKey) {
		this.productKey = productKey;
	}

	/**
	 * Set the property productId
	 *
	 * @param productId the new product
	 *
	 */
	public void setProductId(String productId) {
		this.productId = productId;
	}

	/**
	 * Returns the property product
	 *
	 * @return product
	 *
	 */
	public String getProductKey() {
		return productKey;
	}

	/**
	 * Returns the property productId
	 *
	 * @return productId
	 *
	 */
	public String getProductId() {
		return productId;
	}

	/**
	 * Set the property configurationItem
	 *
	 * @param configurationItem
	 *
	 */
	public void setConfigurationItem(Object configurationItem) {
		this.configurationItem = configurationItem;
	}

	/**
	 * Returns the property configurationItem
	 *
	 * @return configurationItem
	 *
	 */
	public Object getConfigurationItem() {
		return this.configurationItem;
	}

	/**
	 * Set the property contractKey
	 *
	 * @param contractKey the new contractKey
	 *
	 */
	public void setContractKey(String contractKey) {
		this.contractKey = contractKey;
	}

	/**
	 * Returns the property contractKey
	 *
	 * @return contractKey
	 *
	 */
	public String getContractKey() {
		return contractKey;
	}

	/**
	 * Set the property contractItemKey
	 *
	 * @param contractItemKey the new contractItemKey
	 *
	 */
	public void setContractItemKey(String contractItemKey) {
		this.contractItemKey = contractItemKey;
	}

	/**
	 * Returns the property contractItemKey
	 *
	 * @return contractItemKey
	 *
	 */
	public String getContractItemKey() {
		return contractItemKey;
	}

	/**
	 * Returns the property contractId
	 *
	 * @return contractId
	 *
	 */
	public String getContractId() {
		return contractId;
	}

	/**
	 * sets the property contractId
	 *
	 * @param contractId the new contractId
	 *
	 */
	public void setContractId(String contractId) {
		this.contractId = contractId;
	}

	/**
	 * Set the property contractItemId
	 *
	 * @param contractItemId the new contractItemId
	 *
	 */
	public void setContractItemId(String contractItemId) {
		this.contractItemId = contractItemId;
	}

	/**
	 * Returns the property contractItemId
	 *
	 * @return contractItemId
	 *
	 */
	public String getContractItemId() {
		return contractItemId;
	}

	/**
	 * Retrieves the contract configuration filter of the referenced contract
	 * item.
	 */
	public String getContractConfigFilter() {
		return contractConfigFilter;
	}

	/**
	 * Sets the contract configuration filter of the item.
	 */
	public void setContractConfigFilter(String contractConfigFilter) {
		this.contractConfigFilter = contractConfigFilter;
	}

	/**
	 * Set the property basketItemToReplace
	 *
	 * @param basketItemToReplace the new basketItemToReplace
	 *
	 */
	public void setBasketItemToReplace(ItemSalesDoc basketItemToReplace) {
		this.basketItemToReplace = basketItemToReplace;
	}

	/**
	 * Returns the property basketItemToReplace
	 *
	 * @return basketItemToReplace
	 *
	 */
	public Object getBasketItemToReplace() {
		return basketItemToReplace;
	}

	/**
	 * Set the property catalog
	 *
	 * @param the new catalog
	 *
	 */
	public void setCatalog(String catalog) {
		this.catalog = catalog;
	}

	/**
	 * Returns the property catalog
	 *
	 * @return catalog
	 *
	 */
	public String getCatalog() {
		return catalog;
	}

	/**
	 * Returns the property catalogCategory
	 *
	 * @return catalogCategory
	 *
	 */
	public String getCatalogCategory() {
		return catalogCategory;
	}

	/**
	 * Sets the property catalogCategory
	 *
	 * @param catalogCategory The catalogCategory to be set
	 */
	public void setCatalogCategory(String catalogCategory) {
		this.catalogCategory = catalogCategory;
	}

	/**
	 * Set the property customerProductNumber
	 *
	 * @param the new customerProductNumber
	 *
	 */
	public void setCustomerProductNumber(String customerProductNumber) {
		this.customerProductNumber = customerProductNumber;
	}

	/**
	 * Returns the property customerProductNumber
	 *
	 * @return customerProductNumber
	 *
	 */
	public String getCustomerProductNumber() {
		return customerProductNumber;
	}

	/**
	 * Set the property variant
	 *
	 * @param the new variant
	 *
	 */
	public void setVariant(String variant) {
		this.variant = variant;
	}

	/**
	 * Returns the property variant
	 *
	 * @return variant
	 *
	 */
	public String getVariant() {
		return variant;
	}

	/**
	 * Set the property reqDeliveryDate
	 *
	 * @param the new reqDeliveryDate
	 *
	 */
	public void setReqDeliveryDate(String reqDeliveryDate) {
		this.reqDeliveryDate = reqDeliveryDate;
	}

	/**
	 * Returns the property reqDeliveryDate
	 *
	 * @return reqDeliveryDate
	 *
	 */
	public String getReqDeliveryDate() {
		return reqDeliveryDate;
	}

	/**
	 * Returns the batchCharListJSP.
	 * @return BatchCharList
	 */
	public BatchCharList getBatchCharListJSP() {
		return batchCharListJSP;
	}

	/**
	 * Sets the batchCharListJSP.
	 * @param batchCharListJSP The batchCharListJSP to set
	 */
	public void setBatchCharListJSP(BatchCharList batchCharListJSP) {
		this.batchCharListJSP = batchCharListJSP;
	}
	/**
	 * Sets the ShipTo party
	 * @param ShipTo ShipTo-party to be set
		 */
	public void setShipTo(ShipTo shipTo) {
		this.shipTo = shipTo;
	}
	/**
	 * Gets the ShipTo party
	 * @return shipTo Get ShipTo-party 
	 */
	public ShipTo getShipTo() {
		return this.shipTo;
	}
	/**
	 * Sets a handle
	 * @param String Handle which can be used to i.e. to transfer a shipto party
	 */
	public void setHandle(String handle) {
		this.handle = handle;
	}
	/**
	 * Gets the handle
	 * @return String Get handle 
	 */
	public String getHandle() {
		return this.handle;
	}

	/**
	 * Returns the description.
	 * @return String
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Returns the isDataSetExternally.
	 * @return boolean
	 */
	public boolean isDataSetExternally() {
		return isDataSetExternally;
	}

	/**
	 * Returns the netPrice.
	 * @return String
	 */
	public String getNetPrice() {
		return netPrice;
	}

	/**
	 * Sets the description.
	 * @param description The description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Sets the isDataSetExternally.
	 * @param isDataSetExternally The isDataSetExternally to set
	 */
	public void setDataSetExternally(boolean isDataSetExternally) {
		this.isDataSetExternally = isDataSetExternally;
	}

	/**
	 * Sets the netPrice.
	 * @param netPrice The netPrice to set
	 */
	public void setNetPrice(String netPrice) {
		this.netPrice = netPrice;
	}

	/**
	 * Returns the currency.
	 * @return String
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * Sets the currency.
	 * @param currency The currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * Returns the list of campaigns assigned to the item.
	 *
	 * @return CampaignList the list of campaigns assigned to the item
	 */
	public CampaignList getAssignedCampaigns() {
		return assignedCampaigns;
	}

	/**
	 * Sets the list of campaigns assigned to the item.
	 *
	 * @param CampaignList the list of campaigns assigned to the item
	 */
	public void setAssignedCampaigns(CampaignList assignedCampaigns) {
		this.assignedCampaigns = assignedCampaigns;
	}

	/**
	 * Sets the list of external reference objects assigned to the item.
	 *
	 * @param ExtRefObjectList the list of external reference objects
	 *                         assigned to the item
	 */
	public void setAssignedExtRefObjects(ExtRefObjectList assignedExtRefObjects) {
		this.assignedExtRefObjects = assignedExtRefObjects;
	}

	/**
	 * Returns the list of external reference objects assigned to the item.
	 *
	 * @return ExtRefObjectList the list of external reference objects
	 *                          assigned to the item
	 */
	public ExtRefObjectList getAssignedExtRefObjects() {
		return assignedExtRefObjects;
	}

	/**
	 * Sets the list of external reference numbers assigned to the item.
	 *
	 * @param ExternalReferenceList the list of external reference numbers 
	 *                              assigned to the item
	 */
	public void setAssignedExternalReferences(ExternalReferenceList assignedExternalReferences) {
		this.assignedExternalReferences = assignedExternalReferences;
	}

	/**
	 * Returns the list of external references numberss assigned to the item.
	 *
	 * @return ExternalReferenceList the list of external reference 
	 *                               numbers assigned to the item
	 */
	public ExternalReferenceList getAssignedExternalReferences() {
		return assignedExternalReferences;
	}

	/**
	 * Returns the delivery priority assigned to the item.
	 * 
	 * @return String delivery priority
	 */
	public String getDeliveryPriority() {
		return deliveryPriority;
	}

	/**
	 * Sets the delivery priority assigned to the item
	 * @param String delivery priority to set.
	 */
	public void setDeliveryPriority(String deliveryPriority) {
		this.deliveryPriority = deliveryPriority;
	}

	/**
     * Returns the <code>TeckKey</code> of the parent of this TransferItem.
     * null if no parent.
     * 
     * @return parentTechKey
     */
	public String getParentTechKey() {
		return parentTechKey;
	}

	/**
	 * Returns the <code>TeckKey</code> of this TransferItem.
	 * 
	 * @return parentTechKey
	 */
	public String getTechKey() {
		return techKey;
	}

	/**
	 * returns 
	 * true if the package is exploded
	 * false if the package is not exploded or if the basketTransferItem is not a package
	 * 
	 * @return isExploded
	 */
	public boolean isExploded() {
		return exploded;
	}

	/**
	 * if the basketTransferItem is a package, returns true is this item is main product, false otherwise
	 * 
	 * @return isMainItem
	 */
	public boolean isMainItem() {
		return mainItem;
	}
    
    /**
     * Returns the information, if this <code>WebCatSubItem</code> is a selected by the 
     * solution configurator.
     */
    public boolean isScSelected() {
        return isScSelected;
    }

	/**
	 * if the basketTransferItem is a package component, returns true if the type of interlinkage is dependent component
	 * @return
	 */
	public boolean isDependentComponent() {
		return dependentComponent;
	}

	/**
	 * if the basketTransferItem is a package component, returns true if the type of interlinkage is sale component
	 * @return
	 */
	public boolean isSalesComponent() {
		return salesComponent;
	}

	/**
	 * if the basketTransferItem is a package component, returns true if the type of interlinkage is rate plan combination
	 * @return
	 */
	public boolean isRatePlanCombination() {
		return ratePlanCombination;
	}

	/**
	 * sets the parentTechKey for the basketTransferItem in the case it is package component
	 * @param parentTechKey
	 */
	public void setParentTechKey(String parentTechKey) {
		this.parentTechKey = parentTechKey;
	}

	/**
	 * sets the techKey for the basketTransferItem
	 * @param techKey
	 */
	public void setTechKey(String techKey) {
		this.techKey = techKey;
	}

	/**
	 * sets if the basketTransferItem is an exploded package 
	 * @param exploded
	 */
	public void setExploded(boolean exploded) {
		this.exploded = exploded;
	}

	/**
	 * sets if the basketTransferItem has a dependentComponent interlinked type 
	 * @param dependentComponent
	 */
	public void setDependentComponent(boolean dependentComponent) {
		this.dependentComponent = dependentComponent;
	}

	/**
	 * sets if the basketTransferItem is a mainItem 
	 * @param mainItem
	 */
	public void setMainItem(boolean mainItem) {
		this.mainItem = mainItem;
	}

	/**
	 * sets if the basketTransferItem has a ratePlanCombination interlinked type
	 * @param ratePlanCombination
	 */
	public void setRatePlanCombination(boolean ratePlanCombination) {
		this.ratePlanCombination = ratePlanCombination;
	}

	/**
	 *  sets if the basketTransferItem has a saleComponent interlinked type  
	 * @param saleComponent
	 */
	public void setSalesComponent(boolean salesComponent) {
		this.salesComponent = salesComponent;
	}

	/**
     * Returns the selection status 
     * 
     * The method returns one of the following values: {@link WebCatSubItemData#SELECTED_FOR_EXP}, 
     * {@link WebCatSubItemData#DESELECTED_FOR_EXP} or {@link WebCatSubItemData#NOT_SELECTED_FOR_EXP}.
     * 
     * @return the current selection status for this item as <code>String</code>
     */
	public String getSCSelection() {
		return scSelection;
	}

	/**
	 * returns the group
	 */
	public String getGroup() {
		return group;
	}

	/**
     * 
	 * @param scSelection
	 */
	public void setSCSelection(String scSelection) {
		this.scSelection = scSelection;
	}

	/**
     * 
	 * @param group
	 */
	public void setGroup(String group) {
		this.group = group;
	}

    /**
     * returns the selected contract duration
     * 
     * @return ContractDuration, the selected contract duration, or null if there is no ContractDuration or this item
     */
	public ContractDuration getSelectedContractDuration() {
		return selectedContractDuration;
	}

    /**
     * sets the selected contract duration
     * 
     * @param ContractDuration, the selected contract duration to set
     */
	public void setSelectedContractDuration(ContractDuration selectedContractDuration) {
		this.selectedContractDuration = selectedContractDuration;
	}
    
    /**
     * sets the isScSelected flag
     * 
     * @param boolean, the isScSelected flag to set.
     */
    public void setScSelected(boolean isScSelected) {
        this.isScSelected = isScSelected;
    }

    /**
     * Returns the scDocumentGuid
     * 
     * @return TechKey scDocumentGuid, the scDocumentGuid
     */
    public TechKey getScDocumentGuid() {
        return scDocumentGuid;
    }

    /**
     * sets the scDocumentGuid
     * 
     * @param TechKey scDocumentGuid, the scDocumentGuid to set
     */
    public void setScDocumentGuid(TechKey scDocumentGuid) {
        this.scDocumentGuid = scDocumentGuid;
    }
    
    /**
     * set the source 
     * 
     * @param source the source that creates the object
     */
    public void setSource(String source) {
    	this.source = source;
    }
    
    /** 
     * get the source
     * 
     * @return the source that crates the object
     */
    public String getSource() {
    	return this.source;
    }

	/* (non-Javadoc)
	 * @see com.sap.isa.businessobject.item.BasketTransferItem#isPtsItem()
	 */
	public boolean isPtsItem() {
		return isRedemptionItem;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.businessobject.item.BasketTransferItem#setPtsItem(boolean)
	 */
	public void setPtsItem(boolean isRedemption) {
		isRedemptionItem = isRedemption;
	}
	
	
	/**
	 * @return
	 */
	public boolean isBuyPtsItem() {
		return isBuyPtsItem;
	}

	/**
	 * @param b
	 */
	public void setBuyPtsItem(boolean b) {
		isBuyPtsItem = b;
	}

}
