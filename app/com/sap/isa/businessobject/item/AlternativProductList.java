/*****************************************************************************
    Class:        AlternativProductList
    Copyright (c) 2003, SAP AG, All rights reserved.
    Author:
    Created:      02.5.2003
    Version:      1.0

    $Revision: #1 $
    $Date: 2003/02/05 $
*****************************************************************************/

package com.sap.isa.businessobject.item;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.sap.isa.backend.boi.isacore.order.AlternativProductData;
import com.sap.isa.backend.boi.isacore.order.AlternativProductListData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.logging.IsaLocation;

/**
 * List of alternativ products
 *
 * @author SAP AG
 * @version 1.0
 */
public class AlternativProductList
implements AlternativProductListData, Cloneable {

    // List of product aliases
    protected ArrayList alternativProductList;

	/** 
	  *  The IsaLocation of the current Action.
	  *  This will be instantiated during perform and is always present.
	  */
	 protected IsaLocation log = IsaLocation.getInstance(AlternativProductList.class.getName());

    /**
     * Creates a new <code>AlternativProductList</code> object.
     */
    public AlternativProductList() {
        alternativProductList = new ArrayList();
    }

    /**
     * Creates an empty <code>AlternativProductData</code>.
     *
     * @returns AlternativProduct which can added to the AlternativProductList
     */
    public AlternativProductData createAlternativProduct() {
        return (AlternativProductData) new AlternativProduct();
    }

    /**
     * Creates a initialized <code>AlternativProductData</code> for the basket.
     *
     * @param systemProductId id of the system product
     * @param systemProductGUID techkey of the system product
     * @param description description of the system product
     * @param String enteredProductIdType if the system product was found through determination, this
     *                                    specifies, as what the entred product id was interpreted 
     * @param substitutionReasonId if the system product is a substitute product, this is the
     *                           id for the substitution reason
     * 
     * @returns AlternativProduct which can added to the AlternativProductList
     */
    public  AlternativProductData createAlternativProduct(String systemProductId, TechKey systemProductGUID, String description, 
                                                String enteredProductIdType, String substitutionReasonId) {
        return (AlternativProductData) new AlternativProduct(systemProductId, systemProductGUID, description, 
                                                    enteredProductIdType, substitutionReasonId);
    }
    
    /**
     * clear the alternativ product list
     */
    public void clear() {
        alternativProductList.clear();
    }
    
    /**
     * get the size of the list of alternativ products
     */
    public int size() {
        return alternativProductList.size();
    }
    
    /**
     * returns true if the alternativ product list is empty
     * 
     * @return boolean
     */
    public boolean isEmpty() {
       return alternativProductList.isEmpty();
    }

    /**
     * Returns a string representation of the object.
     *
     * @return object as string
     */
    public String toString() {
        return  alternativProductList.toString();
    }

    /**
     * get the alternativ product list
     *
     * @return List list of alternativ products
     */
    public List getList(){
        return alternativProductList;
    }
    
    /**
     * gets the alternativ product for the given index
     *
     * @return AlternativProduct the alternativ product for the given indexc, or
     * null if index is out of bounds
     */
    public AlternativProduct getAlternativProduct(int i){
        
        AlternativProduct altProd = null;
        try {
            altProd = (AlternativProduct) alternativProductList.get(i);
        }
        catch (IndexOutOfBoundsException ex) {
        	log.debug(ex.getMessage());
        }
        
        return altProd;
    }
    
    /**
     * sets the alternativ product for the given index
     *
     * @param int index to set the product
     * @param AlternativProduct the alternativ product for the given indexc, or
     * null if index is out of bounds
     */
    public void addAlternativProduct(int i, AlternativProductData altProd){ 
        alternativProductList.add(i, altProd);
    }
    
    /**
     * sets the alternativ product for the given index
     *
     * @param AlternativProduct the alternativ product for the given indexc, or
     * null if index is out of bounds
     */
    public void addAlternativProduct(AlternativProductData altProd){ 
        alternativProductList.add(altProd);
    }
    
    
    
    /**
     * Creates and adds an alternativ Product to the ProductAliasList
     *
     * @param systemProductId id of the system product
     * @param systemProductGUID techkey of the system product
     * @param description description of the system product
     * @param String enteredProductIdType if the system product was found
     * through determination, this specifies, as what the entred product id was
     * interpreted
     * @param substitutionReasonId if the system product is a substitute
     * product, this is the id for the substitution reason
     */
    public void addAlternativProduct(String systemProductId, TechKey systemProductGUID, String description, 
                                      String enteredProductIdType, String substitutionReasonId) {
        addAlternativProduct((AlternativProductData) new AlternativProduct(systemProductId, systemProductGUID, description, 
                                                        enteredProductIdType, substitutionReasonId));
    }

    /**
     * set the alternativ product list
     *
     * @param  List new list of alternativ products
     */
    public void setList(List alternativProductListData) {
        alternativProductList.clear();
        alternativProductList.addAll(0, alternativProductList);
    }

    /**
     * Performs a deep copy of this object.
     * Because of the fact that
     * all fields of this object consist of immutable objects like
     * <code>String</code> and <code>TechKey</code> or primitive types
     * the shallow copy is identical with a deep copy. For the
     * <code>payment</code> property an explicit copy operation is performed
     * because this object is the only mutable one.
     *
     * @return shallow (deep-like) copy of this object
     */
    public Object clone() {
        AlternativProductList myClone = new AlternativProductList();
        myClone.alternativProductList = (ArrayList) alternativProductList.clone();
        return myClone;

    }

    /**
     * Returns en iterator over the Entrys of the alternativ product list in
     * form of Map.
     *
     * @return iterator over alternativ products
     *
     */
    public Iterator iterator( ) {

        return alternativProductList.iterator();

    }
    
    /**
     * returns true, if the list contains substitute products
     * product
     * 
     * @return boolean true if the list contains substitute products
     */
    public boolean isSubstituteProductList() {
       return (alternativProductList != null && alternativProductList.size() > 0 && 
                ((AlternativProduct) alternativProductList.get(0)).isSubstituteProduct());
    }
    
    /**
     * returns true, if the list contains determination products
     * 
     * @return boolean true if the list contains determination products
     */
    public boolean isDeterminationProductList() {
        return (alternativProductList != null && alternativProductList.size() > 0 && 
                 ((AlternativProduct) alternativProductList.get(0)).isDeterminationProduct());
    }

}
