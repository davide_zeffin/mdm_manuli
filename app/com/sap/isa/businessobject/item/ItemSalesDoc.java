/*****************************************************************************
    Class:        ItemSalesDoc
    Copyright (c) 2001, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      11.4.2001
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $
*****************************************************************************/
package com.sap.isa.businessobject.item;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.sap.isa.backend.boi.isacore.ConnectedDocumentItemData;
import com.sap.isa.backend.boi.isacore.ExtendedStatusData;
import com.sap.isa.backend.boi.isacore.ShipToData;
import com.sap.isa.backend.boi.isacore.marketing.CUAData;
import com.sap.isa.backend.boi.isacore.marketing.CUAListData;
import com.sap.isa.backend.boi.isacore.order.AlternativProductListData;
import com.sap.isa.backend.boi.isacore.order.CampaignListData;
import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.backend.boi.isacore.order.ItemConditionTableData;
import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.backend.boi.isacore.order.ItemDeliveryData;
import com.sap.isa.businessobject.BatchCharList;
import com.sap.isa.businessobject.Delivery;
import com.sap.isa.businessobject.Product;
import com.sap.isa.businessobject.ProductHolder;
import com.sap.isa.businessobject.ShipTo;
import com.sap.isa.businessobject.order.CampaignList;
import com.sap.isa.businessobject.order.ExtRefObjectList;
import com.sap.isa.businessobject.order.ExternalReferenceList;
import com.sap.isa.businessobject.order.Text;
import com.sap.isa.businessobject.webcatalog.ItemTransferRequest;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.core.Iterable;
import com.sap.isa.core.TechKey;

/**
 * Item of a sales document. Each sales document consists of a header and a
 * number of items. This items are represented by this class.
 *
 * @author  SAP AG
 * @version 1.0
 */
public class ItemSalesDoc extends ItemBase implements ItemData, Iterable, CUAData, ProductHolder {

    /**
	 * @param transferItm
	 * @param string
	 */
	public ItemSalesDoc(BasketTransferItem transferItm, String pointCode) {
		
		init(transferItm);
		setLoyPointCodeId(pointCode);
	}

	private String pointCodeId;
	private boolean isRedemptItem;
	private boolean isBuyPointsItem;
	private String redemptionValue;
	private Product catalogProduct;
    private TechKey contractKey;
    private String contractId;
    private TechKey contractItemKey;
    private String contractItemId;
    private String contractConfigFilter;
    //    private TechKey pcat;
    //    private String  pcatArea;
    //    private String  pcatVariant;
    protected String systemProductId;
    protected String substitutionReasonId;
    private ShipTo shipToLine;
    private Delivery delivery;
    private List itemDelivery = new ArrayList(10);
    private boolean deliveryExists = false;
    protected AlternativProductList alternativProductList = new AlternativProductList();
    protected boolean backordered = false;
    
    protected boolean isCopiedFromOtherItem = false;

    private boolean contractKeyChangeable;
    private boolean contractIdChangeable;
    private boolean contractItemKeyChangeable;
    private boolean contractItemIdChangeable;
    private boolean contractConfigFilterChangeable;
    private boolean batchIdChangeable;
    protected boolean shipToLineChangeable;
    private String paymentTerms;
    private String paymentTermsDesc;
    private boolean paymentTermsChangeable = true;
    private boolean isFromCatalog = false;

    private CUAListData cuaList;

    // variables for batch
    private boolean batchDedicated;
    private boolean batchClassAssigned;
    private String batchID;
    private ArrayList batchOption = new ArrayList();
    private ArrayList batchOptionTxt = new ArrayList();
    private BatchCharList batchCharListJSP = new BatchCharList();

    private String parentHandle;

    //Cancel date 
    protected String latestDlvDate;
    protected boolean latestDlvDateChangeable;

    /**
     * this list holds all campaigns the have been assigend to the item
     */
    protected CampaignList assignedCampaigns = new CampaignList();
    protected boolean assignedCampaignsChangeable;

    /**
     * the item condition
     */
    protected ItemConditionTable itemConditions = new ItemConditionTable();
    protected boolean isFromAuction;
    /**
     * this list holds all campaigns were found via camapign determination and validation and
     * that could be assigend to the item
     */
    protected CampaignList determinedCampaigns = new CampaignList();

    /**
     * Check if the given itemBase object is an instance of ItemSalesDoc. 
     * 
     * @param itemBase item as ItemBase object
     * @return ItemSalesDoc casted ItemBase object as ItemSalesDoc
     */
    static public ItemSalesDoc getItemSalesDoc(ItemBase itemBase) {

        ItemSalesDoc item = null;
        if (itemBase instanceof ItemSalesDoc) {
            item = (ItemSalesDoc) itemBase;
        }
        else {
            log.debug("not allowed instance for ItemSalesDoc object: " + itemBase.getClass().getName());
        }

        return item;
    }

    /**
     * Check if the given itemData object is an instance of ItemSalesDoc. 
     * 
     * @param itemData item as ItemData object
     * @return ItemSalesDoc casted itemData object as ItemSalesDoc
     */
    static public ItemSalesDoc getItemSalesDoc(ItemData itemData) {

        ItemSalesDoc item = null;
        if (itemData instanceof ItemSalesDoc) {
            item = (ItemSalesDoc) itemData;
        }
        else {
            log.debug("not allowed instance for ItemSalesDoc object: " + itemData.getClass().getName());
        }

        return item;
    }

    /**
     * Check if the given itemBase object is an instance of ItemSalesDoc. 
     * 
     * @param itemBase item as ItemBase object
     * @return true if instance of ItemSalesDoc
     */
    static public boolean isItemSalesDoc(ItemBase itemBase) {

        boolean isItemSalesDoc = false;
        if (itemBase instanceof ItemSalesDoc) {
            isItemSalesDoc = true;
        }
        else {
            log.debug("not allowed instance for ItemSalesDoc object: " + itemBase.getClass().getName());
        }

        return isItemSalesDoc;
    }

    /**
     * Default constructor for the Item
     */
    public ItemSalesDoc() {
        super();
    }

    /**
     * Constructor for the object directly taking the technical
     * key for the object
     * if the technical key is empty an unique handle will be generate
     *
     * @param techKey Technical key for the object
     */
    public ItemSalesDoc(TechKey techKey) {
        super(techKey);
    }

    /**
     * Special copy constructor used to create a new ItemSalesDoc for
     * a given <code>BasketTransferItem</code>.
     *
     * @param transferItm Transfer item used for construction
     */
    public ItemSalesDoc(BasketTransferItem transferItm) {
		init(transferItm);
    }

	private void init(BasketTransferItem transferItm) {
		// ItemSalesDoc itm = new ItemSalesDoc();
		// IPCItem ipcItem = null;
		
		// CSN Message 2149045. The BasketTransferItem interface
		// is not correctly implemented by the webcatalog.
		// The catalog name and the catalog variant are both
		// concatenated and stored in the catalog property.
		String catalog = transferItm.getCatalog();
		String pcatString = null;
		String variantString = null;
		String componentType = "";
		boolean isSubItemFromPackage = false;
		
		if (catalog != null) {
		    int length = catalog.length();
		
		    if (length <= 30) {
		        // right behaviour, only the catalog name is stored in
		        // the property.
		        pcatString = transferItm.getCatalog();
		        variantString = transferItm.getVariant();
		    }
		    else {
		        // wrong behaviour, catalog name and variant are concated
		        // and stored both in the catalog property.
		        pcatString = catalog.substring(0, 30).trim();
		        variantString = catalog.substring(30, length).trim();
		    }
		}
		
		// pcatArea      = transferItm.getCatalogCategory();
		pcat = new TechKey(pcatString);
		pcatVariant = variantString;
		contractKey = new TechKey(transferItm.getContractKey());
		contractId = transferItm.getContractId();
		contractItemKey = new TechKey(transferItm.getContractItemKey());
		contractItemId = transferItm.getContractItemId();
		contractConfigFilter = transferItm.getContractConfigFilter();
		pcatArea = transferItm.getCatalogCategory();
		
		// configuration
		externalItem = transferItm.getConfigurationItem();
		
		// weird names here !!!
		// key is id and id is nothing
		// productId holds the real TechKey whereas
		// product holds the id of the product like
		// WP-940 or DPC1 for instance
		productId = new TechKey(transferItm.getProductKey());
		product = transferItm.getProductId();
		quantity = transferItm.getQuantity();
		unit = transferItm.getUnit();
		setPtsItem(transferItm.isPtsItem());
		setBuyPtsItem(transferItm.isBuyPtsItem());
		
		// need the description for populateItems... 
		description = transferItm.getDescription();
		
		// transfer from sales document 
		if (transferItm instanceof BasketTransferItemImpl) {
		    text = (Text) ((BasketTransferItemImpl) transferItm).getText();
		    extensionData = (HashMap) ((HashMap) ((BasketTransferItemImpl) transferItm).getExtensionMap()).clone();
		    isDataSetExternally = ((BasketTransferItemImpl) transferItm).isDataSetExternally();
		    netPrice = ((BasketTransferItemImpl) transferItm).getNetPrice();
		    currency = ((BasketTransferItemImpl) transferItm).getCurrency();
		    assignedCampaigns = (CampaignList) ((BasketTransferItemImpl) transferItm).getAssignedCampaigns().clone();
		    assignedExtRefObjects =
		        (ExtRefObjectList) ((BasketTransferItemImpl) transferItm).getAssignedExtRefObjects().clone();
		    assignedExternalReferences =
		        (ExternalReferenceList) ((BasketTransferItemImpl) transferItm).getAssignedExternalReferences().clone();
		    deliveryPriority = ((BasketTransferItemImpl) transferItm).getDeliveryPriority();
		
		}
		
		//Note:1059235 :Set the gridflag when the backend is "CRM"
		//as it is required for COPY grid items from one document to another
		if (extensionData.get("griditemflag") != null &&
			extensionData.get("griditemflag").toString().equals(ITEM_CONFIGTYPE_GRID)){
			configType = ITEM_CONFIGTYPE_GRID;		
			extensionData.remove("griditemflag");
		}        
		
		// transfer from catalog
		if (transferItm instanceof ItemTransferRequest) {
		    WebCatItem webCatItem = ((ItemTransferRequest) transferItm).getCatItem();
		    extensionData = (HashMap) ((HashMap) webCatItem.getExtensionMap()).clone();
		    catalogProduct = new Product(webCatItem);
		    isFromCatalog = true;
		    if (webCatItem.isExploded()) {
		       // Item is main item of exploded package or tarif          	
				isMainProductFromPackage = webCatItem.isMainItem();
				isPackageExploded = webCatItem.isExploded();
				techKey = webCatItem.getTechKey();
				scDocumentGuid = transferItm.getScDocumentGuid();
		    }            
		    if (webCatItem.getScItemType() != null && webCatItem.getScItemType().length() > 0) {
		        // Item is component of exploded package or tarif
		        if (webCatItem.getScItemType().equals("P")) {
		            itmUsage = ItemBase.ITEM_USAGE_SC_DEPENDENT_COMPONENT;                    
		        }
		        else if (webCatItem.getScItemType().equals("S")) {
		            itmUsage = ItemBase.ITEM_USAGE_SC_SALES_COMPONENT;					
		        }
		        else if (webCatItem.getScItemType().equals("C")) {
		            itmUsage = ItemBase.ITEM_USAGE_SC_RATE_PLAN_COMBINATION_COMPONENT;
					
		        }
				techKey = webCatItem.getTechKey();
				parentId = webCatItem.getParentTechKey();
		    }
		
		    // contract duration
		    if (webCatItem.getSelectedContractDuration() != null) {
		        contractDuration = String.valueOf(webCatItem.getSelectedContractDuration().getValue());
		        contractDurationUnit = String.valueOf(webCatItem.getSelectedContractDuration().getUnit());
		    }
		    if (webCatItem.isPtsItem()) {
		    	isRedemptItem = true;
		    }
			if (webCatItem.isBuyPtsItem()) {
				isBuyPointsItem = true;
			}
		}
		
		// initialize parent-item
		if (parentId == null) {
		    parentId = new TechKey(null);
		}
		
		// create a handle for the new item!
		createUniqueHandle();
	}

    /**
     * Get a new itemDelivery object
     */
    public ItemDeliveryData createItemDelivery() {
        return (ItemDeliveryData) new ItemDelivery();
    }

    /**
     * Get the technical key for the contract associated with this item.
     *
     * @return contract key
     */
    public TechKey getContractKey() {
        return contractKey;
    }

    /**
     * Set the technical key for the contract associated with this item.
     *
     * @param contractKey the key to be set
     */
    public void setContractKey(TechKey contractKey) {
        this.contractKey = contractKey;
    }

    /**
     * Get the id for the contract associated with this item.
     *
     * @return contract id
     */
    public String getContractId() {
        return contractId;
    }

    /**
     * Set the id for the contract associated with this item.
     *
     * @param contractId the id to be set
     */
    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    /**
     * Get the technical key of the item in the contract.
     *
     * @return technical key
     */
    public TechKey getContractItemKey() {
        return contractItemKey;
    }

    /**
     * Set the technical key of the item in the contract.
     *
     * @param contractItemKey technical key in contract
     */
    public void setContractItemKey(TechKey contractItemKey) {
        this.contractItemKey = contractItemKey;
    }

    /**
     * Get the id of the item in the contract.
     *
     * @return id in contract
     */
    public String getContractItemId() {
        return contractItemId;
    }

    /**
     * Set the id of the item in the contract.
     *
     * @param contractItemId id to be set
     */
    public void setContractItemId(String contractItemId) {
        this.contractItemId = contractItemId;
    }

    /**
     *
     */
    public String getContractConfigFilter() {
        return contractConfigFilter;
    }

    /**
     *
     */
    public void setContractConfigFilter(String contractConfigFilter) {
        this.contractConfigFilter = contractConfigFilter;
    }

    /**
     * Get the catalog area used to select the item
     * and put it into the basket.
     *
     * @return catalog area
     */
    public String getPcatArea() {
        return pcatArea;
    }

    /**
     * Get the catalog variant used to select the item
     * and put it into the basket.
     *
     * @return catalog variant
     */
    public String getPcatVariant() {
        return pcatVariant;
    }

    /**
     * Get the catalog product associated with this item. The product
     * wraps around a catalog item and offers convenience methods for the
     * retrievement of catalog data.
     *
     * @return the product
     */
    public Product getCatalogProduct() {
        return catalogProduct;
    }

    /**
     * Set the catalog producr associated with this item. The product
     * wraps around a catalog item and offers convenience methods for the
     * retrievement of catalog data.
     *
     * @param catalogProduct the product to be set
     */
    public void setCatalogProduct(Product catalogProduct) {
        this.catalogProduct = catalogProduct;
    }

    /**
     *
     */
    public ShipTo getShipTo() {
        return shipToLine;
    }

    /**
     *
     */
    public ShipToData getShipToData() {
        return shipToLine;
    }

    public Iterator iterator() {
        return itemDelivery.iterator();
    }

    /**
     *
     */
    public void setShipTo(ShipTo shipToLine) {
        this.shipToLine = shipToLine;
    }

    /**
     *
     */
    public void setShipToData(ShipToData shipToLine) {
        this.shipToLine = (ShipTo) shipToLine;
    }

    /**
     * Indicates whether the batch id of the item is changeable
     */
    public boolean isBatchIdChangeable() {
        return batchIdChangeable;
    }

    /**
     * Determine, if Item's Business Object Type is Service
     */
    public boolean isBusinessObjectTypeService() {
        if (this.businessObjectType != null) {
            return this.businessObjectType.equals(BUSOBJTYPE_SERVICE);
        }
        else {
            return false;
        }
    }

    /**
     * Indicates whether the contract key of the item is changeable
     */
    public boolean isContractKeyChangeable() {
        return contractKeyChangeable;
    }

    /**
     * Indicates whether the contract id of the item is changeable
     */
    public boolean isContractIdChangeable() {
        return contractIdChangeable;
    }

    /**
     * Indicates whether the contract item key of the item is changeable
     */
    public boolean isContractItemKeyChangeable() {
        return contractItemKeyChangeable;
    }

    /**
     * Indicates whether the contract item id of the item is changeable
     */
    public boolean isContractItemIdChangeable() {
        return contractItemIdChangeable;
    }

    /**
     *
     */
    public boolean isContractConfigFilterChangeable() {
        return contractConfigFilterChangeable;
    }

    /**
     *
     */
    public Delivery getDelivery() {
        return delivery;
    }

    /**
     *
     */
    public void setDelivery(Delivery delivery) {
        this.delivery = delivery;
    }

    /**
     * Set item status status to {@link #DOCUMENT_COMPLETION_STATUS_PARTLY_DELIVERED} 
     */
    public void setStatusPartlyDelivered() {
        this.status = DOCUMENT_COMPLETION_STATUS_PARTLY_DELIVERED;
    }

    /**
     * Set item status status to {@link #DOCUMENT_COMPLETION_STATUS_READYTOPICKUP} 
     */
    public void setStatusReadyToPickup() {
        this.status = DOCUMENT_COMPLETION_STATUS_READYTOPICKUP;
    }

    /**
     * Returns <code>true</code> if item status is PARTLY DELIVERED
     */
    public boolean isStatusPartlyDelivered() {
        return status == DOCUMENT_COMPLETION_STATUS_PARTLY_DELIVERED;
    }

    /**
     * Set item backordered flag  
     */
    public void setBackordered(boolean backordered) {
        this.backordered = backordered;
    }

    /**
     * Returns <code>true</code> if the item is backordered
     */
    public boolean isBackordered() {
        return backordered;
    }

    /**
     * Set the CUAList in the a product.
     *
     * @param cUAList reference to the CUA-List
     *
     */
    public void setCUAList(CUAListData cUAList) {
        this.cuaList = cUAList;
    }

    /**
     * Get the CUAList in the a product.
     *
     * @return reference to the CUA-List
     */
    public CUAListData getCUAList() {
        return cuaList;
    }

    /**
     * Set the flag if a product uses batches.
     *
     * @param boolean value
     *
     */
    public void setBatchDedicated(boolean batchDEDICATED) {
        this.batchDedicated = batchDEDICATED;
    }

    /**
     * Get the flag for product batches.
     *
     * @return boolean value if product uses batches
     */
    public boolean getBatchDedicated() {
        return batchDedicated;
    }

    /**
     * Set the flag if a product uses batches.
     *
     * @param boolean value
     *
     */
    public void setBatchClassAssigned(boolean batchClassASSIGNED) {
        this.batchClassAssigned = batchClassASSIGNED;
    }

    /**
     * Get the flag for product batches.
     *
     * @return boolean value if product uses batches
     */
    public boolean getBatchClassAssigned() {
        return batchClassAssigned;
    }

    /**
     * Set batchID.
     *
     * @param int value
     *
     */
    public void setBatchID(String batchid) {
        this.batchID = batchid;
    }

    /**
     * Get batchID.
     *
     * @return batchID
     */
    public String getBatchID() {
        return batchID;
    }

    /**
     * Set values of batch drop down box.
     *
     * @param String with value for one batchOption
     */
    public void setBatchOption(String batchOPTION) {
        this.batchOption.add(batchOPTION);
    }

    /**
     * Get values of batch drop down box.
     *
     * @return one batchOption as a String
     */
    public String getBatchOption(int i) {
        return batchOption.get(i).toString();
    }

    /**
     * Get size drop down box.
     *
     * @return size of batchOption
     */
    public int getBatchOptionNum() {
        return batchOption.size();
    }

    /**
     * Set values of batch text drop down box.
     *
     * @param String with value for one batchOptionTxt
     */
    public void setBatchOptionTxt(String batchOptionTXT) {
        this.batchOptionTxt.add(batchOptionTXT);
    }

    /**
     * Get values of batch text drop down box.
     *
     * @return one batchOptionTxt as a String
     */
    public String getBatchOptionTxt(int i) {
        return batchOptionTxt.get(i).toString();
    }

    public BatchCharList getBatchCharListJSP() {
        return batchCharListJSP;
    }

    public void setBatchCharListJSP(BatchCharList batchCharListJSP) {
        this.batchCharListJSP = batchCharListJSP;
    }

    public String toString() {

        String ret =
            super.toString()
                + "ItemSalesDoc: ["
                + ", contractId=\""
                + contractId
                + "\""
                + ", contractIdChangeable=\""
                + contractIdChangeable
                + "\""
                + ", reqDeliveryDateChangeable=\""
                + reqDeliveryDateChangeable
                + "\""
                + ", shipToLineChangeable=\""
                + shipToLineChangeable
                + "\""
                + ", shipToLine=\""
                + shipToLine
                + "\""
                + ", delivery=\""
                + delivery
                + "\""
                + ", isFromAuction=\""
                + isFromAuction
                + "\""
                + ", itemDelivery=\""
                + itemDelivery
                + "\""
                + ", assignedCampaigns=\""
                + assignedCampaigns.toString()
                + "\""
                + ", assignedCampaignsChangeable=\""
                + assignedCampaignsChangeable
                + "\""
                + ", determinedCampaigns=\""
                + determinedCampaigns.toString()
                + "\"]";

        return ret;

    }
    /**
     * @deprecated
     */
    public void setAllValues(
        boolean configurable,
        boolean configurableChangeable,
        String currency,
        boolean currencyChangeable,
        TechKey contractKey,
        boolean contractKeyChangeable,
        String contractId,
        boolean contractIdChangeable,
        TechKey contractItemKey,
        boolean contractItemKeyChangeable,
        String contractItemId,
        boolean contractItemIdChangeable,
        String contractConfigFilter,
        boolean contractConfigFilterChangeable,
        String description,
        boolean descriptionChangeable,
        ExtendedStatusData extendedStatus,
        boolean extendedStatusChangeable,
        TechKey ipcDocumentId,
        boolean ipcDocumentIdChangeable,
        TechKey ipcItemId,
        boolean ipcItemIdChangeable,
        String ipcProduct,
        boolean ipcProductChangeable,
        String partnerProduct,
        boolean partnerProductChangeable,
        TechKey pcat,
        boolean pcatChangeable,
        String pcatArea,
        boolean pcatAreaChangeable,
        String pcatVariant,
        boolean pcatVariantChangeable,
        String product,
        boolean productChangeable,
        TechKey productId,
        boolean productIdChangeable,
        String quantity,
        boolean quantityChangeable,
        String reqDeliveryDate,
        boolean reqDeliveryDateChangeable,
        ShipToData shipToLine,
        boolean shipToLineChangeable,
        String unit,
        boolean unitChangeable,
        boolean isAvailableInResellerCatalog) {
        super.setAllValues(
            configurable,
            configurableChangeable,
            currency,
            currencyChangeable,
            description,
            descriptionChangeable,
            extendedStatus,
            extendedStatusChangeable,
            partnerProduct,
            partnerProductChangeable,
            pcat,
            pcatChangeable,
            pcatArea,
            pcatAreaChangeable,
            pcatVariant,
            pcatVariantChangeable,
            product,
            productChangeable,
            productId,
            productIdChangeable,
            quantity,
            quantityChangeable,
            reqDeliveryDate,
            reqDeliveryDateChangeable,
            unit,
            unitChangeable,
            isAvailableInResellerCatalog);

        this.contractKey = contractKey;
        this.contractKeyChangeable = contractKeyChangeable;
        this.contractId = contractId;
        this.contractIdChangeable = contractIdChangeable;
        this.contractItemKey = contractItemKey;
        this.contractItemKeyChangeable = contractItemKeyChangeable;
        this.contractItemId = contractItemId;
        this.contractItemIdChangeable = contractItemIdChangeable;
        this.contractConfigFilter = contractConfigFilter;
        this.contractConfigFilterChangeable = contractConfigFilterChangeable;
        this.shipToLine = (ShipTo) shipToLine;
        this.shipToLineChangeable = shipToLineChangeable;
    }

    /**
     * @deprecated
     */
    public void setAllValuesChangeable(
        boolean configurableChangeable,
        boolean currencyChangeable,
        boolean contractKeyChangeable,
        boolean contractIdChangeable,
        boolean contractItemKeyChangeable,
        boolean contractItemIdChangeable,
        boolean contractConfigFilterChangeable,
        boolean reqDeliveryDateChangeable,
        boolean descriptionChangeable,
        boolean extendedStatusChangeable,
        boolean ipcDocumentIdChangeable,
        boolean ipcItemIdChangeable,
        boolean ipcProductChangeable,
        boolean partnerProductChangeable,
        boolean pcatChangeable,
        boolean pcatAreaChangeable,
        boolean pcatVariantChangeable,
        boolean productChangeable,
        boolean productIdChangeable,
        boolean quantityChangeable,
        boolean shipToLineChangeable,
        boolean unitChangeable) {
        this.setAllValuesChangeable(
            configurableChangeable,
            currencyChangeable,
            contractKeyChangeable,
            contractIdChangeable,
            contractItemKeyChangeable,
            contractItemIdChangeable,
            contractConfigFilterChangeable,
            reqDeliveryDateChangeable,
            descriptionChangeable,
            extendedStatusChangeable,
            ipcDocumentIdChangeable,
            ipcItemIdChangeable,
            ipcProductChangeable,
            partnerProductChangeable,
            pcatChangeable,
            pcatAreaChangeable,
            pcatVariantChangeable,
            productChangeable,
            productIdChangeable,
            quantityChangeable,
            shipToLineChangeable,
            unitChangeable,
            false,
            false);
    }

    /**
     * @deprecated
     */
    public void setAllValuesChangeable(
        boolean configurableChangeable,
        boolean currencyChangeable,
        boolean contractKeyChangeable,
        boolean contractIdChangeable,
        boolean contractItemKeyChangeable,
        boolean contractItemIdChangeable,
        boolean contractConfigFilterChangeable,
        boolean reqDeliveryDateChangeable,
        boolean descriptionChangeable,
        boolean extendedStatusChangeable,
        boolean ipcDocumentIdChangeable,
        boolean ipcItemIdChangeable,
        boolean ipcProductChangeable,
        boolean partnerProductChangeable,
        boolean pcatChangeable,
        boolean pcatAreaChangeable,
        boolean pcatVariantChangeable,
        boolean productChangeable,
        boolean productIdChangeable,
        boolean quantityChangeable,
        boolean shipToLineChangeable,
        boolean unitChangeable,
        boolean batchIdChangeable,
        boolean poNumberUCChangeable) {
        super.setAllValuesChangeable(
            configurableChangeable,
            currencyChangeable,
            reqDeliveryDateChangeable,
            descriptionChangeable,
            extendedStatusChangeable,
            partnerProductChangeable,
            pcatChangeable,
            pcatAreaChangeable,
            pcatVariantChangeable,
            productChangeable,
            productIdChangeable,
            quantityChangeable,
            unitChangeable,
            poNumberUCChangeable);
        this.batchIdChangeable = batchIdChangeable;
        this.contractKeyChangeable = contractKeyChangeable;
        this.contractIdChangeable = contractIdChangeable;
        this.contractItemKeyChangeable = contractItemKeyChangeable;
        this.contractItemIdChangeable = contractItemIdChangeable;
        this.contractConfigFilterChangeable = contractConfigFilterChangeable;
        this.shipToLineChangeable = shipToLineChangeable;
    }

    /**
     * deprecated
     * @see com.sap.isa.backend.boi.isacore.order.ItemData#setAllValuesChangeable(boolean, boolean, boolean, boolean, boolean, boolean, boolean, boolean, boolean, boolean, boolean, boolean, boolean, boolean, boolean, boolean, boolean, boolean, boolean, boolean, boolean, boolean, boolean, boolean, boolean)
     */
    public void setAllValuesChangeable(
        boolean configurableChangeable,
        boolean currencyChangeable,
        boolean contractKeyChangeable,
        boolean contractIdChangeable,
        boolean contractItemKeyChangeable,
        boolean contractItemIdChangeable,
        boolean contractConfigFilterChangeable,
        boolean reqDeliveryDateChangeable,
        boolean descriptionChangeable,
        boolean extendedStatusChangeable,
        boolean ipcDocumentIdChangeable,
        boolean ipcItemIdChangeable,
        boolean ipcProductChangeable,
        boolean partnerProductChangeable,
        boolean pcatChangeable,
        boolean pcatAreaChangeable,
        boolean pcatVariantChangeable,
        boolean productChangeable,
        boolean productIdChangeable,
        boolean quantityChangeable,
        boolean shipToLineChangeable,
        boolean unitChangeable,
        boolean batchIdChangeable,
        boolean poNumberUCChangeable,
        boolean deliveryPriorityChangeable) {
        super.setAllValuesChangeable(
            configurableChangeable,
            currencyChangeable,
            reqDeliveryDateChangeable,
            descriptionChangeable,
            extendedStatusChangeable,
            partnerProductChangeable,
            pcatChangeable,
            pcatAreaChangeable,
            pcatVariantChangeable,
            productChangeable,
            productIdChangeable,
            quantityChangeable,
            unitChangeable,
            poNumberUCChangeable,
            deliveryPriorityChangeable);
        this.batchIdChangeable = batchIdChangeable;
        this.contractKeyChangeable = contractKeyChangeable;
        this.contractIdChangeable = contractIdChangeable;
        this.contractItemKeyChangeable = contractItemKeyChangeable;
        this.contractItemIdChangeable = contractItemIdChangeable;
        this.contractConfigFilterChangeable = contractConfigFilterChangeable;
        this.shipToLineChangeable = shipToLineChangeable;
    }

    /**
     * 
     */
    public void setAllValuesChangeable(
        boolean configurableChangeable,
        boolean currencyChangeable,
        boolean contractKeyChangeable,
        boolean contractIdChangeable,
        boolean contractItemKeyChangeable,
        boolean contractItemIdChangeable,
        boolean contractConfigFilterChangeable,
        boolean reqDeliveryDateChangeable,
        boolean descriptionChangeable,
        boolean extendedStatusChangeable,
        boolean ipcDocumentIdChangeable,
        boolean ipcItemIdChangeable,
        boolean ipcProductChangeable,
        boolean partnerProductChangeable,
        boolean pcatChangeable,
        boolean pcatAreaChangeable,
        boolean pcatVariantChangeable,
        boolean productChangeable,
        boolean productIdChangeable,
        boolean quantityChangeable,
        boolean shipToLineChangeable,
        boolean unitChangeable,
        boolean batchIdChangeable,
        boolean poNumberUCChangeable,
        boolean deliveryPriorityChangeable,
        boolean latestDlvDateChangeable,
        boolean assignedCampaignsChangeable,
        boolean paymentTermsChangeable) {
        super.setAllValuesChangeable(
            configurableChangeable,
            currencyChangeable,
            reqDeliveryDateChangeable,
            descriptionChangeable,
            extendedStatusChangeable,
            partnerProductChangeable,
            pcatChangeable,
            pcatAreaChangeable,
            pcatVariantChangeable,
            productChangeable,
            productIdChangeable,
            quantityChangeable,
            unitChangeable,
            poNumberUCChangeable,
            deliveryPriorityChangeable);
        this.batchIdChangeable = batchIdChangeable;
        this.contractKeyChangeable = contractKeyChangeable;
        this.contractIdChangeable = contractIdChangeable;
        this.contractItemKeyChangeable = contractItemKeyChangeable;
        this.contractItemIdChangeable = contractItemIdChangeable;
        this.contractConfigFilterChangeable = contractConfigFilterChangeable;
        this.shipToLineChangeable = shipToLineChangeable;
        this.latestDlvDateChangeable = latestDlvDateChangeable;
        this.assignedCampaignsChangeable = assignedCampaignsChangeable;
        this.paymentTermsChangeable = paymentTermsChangeable;
    }

    public void setChangeable(boolean flag) {
        super.setChangeable(flag);

        contractIdChangeable = flag;
    }

    /**
     * Add a new item delivery detail to the list.
     *
     * @param ItemDelivery which should add to the itemSalesDoc
     *
     * @see ItemDelivery
     * @see ItemDeliveryData
     */
    public void addDelivery(ItemDeliveryData itemDel) {
        itemDelivery.add(itemDel);
        deliveryExists = true;
    }

    /**
     * Do one or more deliveries exist. 
     *
     * @return <code>true</code> if there are any deliveries, otherwise
     *         <code>false</code>.
     */
    public boolean deliveryExists() {
        // Since DocFlow harmonization with CRM 5.2 the connected documents must be checked too!
        if (! deliveryExists) {
            Iterator sucIT = getSuccessorList().iterator();
            while(sucIT.hasNext()) {
                if (HeaderData.DOCUMENT_TYPE_DELIVERY.equals( ((ConnectedDocumentItemData)sucIT.next()).getDocType()) ) {
                    deliveryExists = true;
                }
            }
        }
        
        return deliveryExists;
    }
    /**
     * Return the number of existing deliveries
     * @return int No of existing deliveries
     */
    public int getNoOfDeliveries() {
        //  Since DocFlow harmonization with CRM 5.2 the connected documents must be checked too!
        int retVal = 0;
        if (itemDelivery.size() > 0) { 
            retVal = itemDelivery.size();
        } else {
            Iterator sucIT = getSuccessorList().iterator();
            while(sucIT.hasNext()) {
                if (HeaderData.DOCUMENT_TYPE_DELIVERY.equals( ((ConnectedDocumentItemData)sucIT.next()).getDocType()) ) {
                    retVal++;
                }
            }
        }
        
        return itemDelivery.size();
    }

    /**
     * Performs a shallow copy of this object. Because of the fact that
     * nearly all fields of this object consist of immutable objects like
     * <code>String</code> and <code>TechKey</code> or primitive types
     * the shallow copy is nearly identical with a deep copy. The only
     * difference is that the property <code>itemDelivery</code> (a list)
     * is backed by the same data.
     *
     * @return shallow copy of this object
     */
    public Object clone() {

        ItemSalesDoc myClone = (ItemSalesDoc) super.clone();
        if (myClone != null) {
            if (alternativProductList != null) {
                myClone.alternativProductList = (AlternativProductList) alternativProductList.clone();
            }
            if (assignedCampaigns != null) {
                myClone.assignedCampaigns = (CampaignList) assignedCampaigns.clone();
            }
            if (determinedCampaigns != null) {
                myClone.determinedCampaigns = (CampaignList) determinedCampaigns.clone();
            }
        }
        return myClone;
    }

    /**
     * Creates a new CampaignList.
     *
     * @return CampaignListData
     */
    public CampaignListData createCampaignList() {
        return new CampaignList();
    }

    /**
     *
     */
    public boolean isShipToLineChangeable() {
        return shipToLineChangeable;
    }

    /**
     * Sets the changeable flags for assigned campaigns
     *
     * @param assignedCampaignsChangeable <code>true</code> indicates that the
     *        assigned campaign information is changeable
     */
    public void setAssignedCampaignsChangeable(boolean assignedCampaignsChangeable) {

        this.assignedCampaignsChangeable = assignedCampaignsChangeable;
    }

    /**
     * Checks whether or not the assigned campaigns information is changeable.
     *
     * @return <code>true</code> indicates that the information is changeable,
     *         <code>false</code> indicates that the information cannot be changed.
     */
    public boolean isAssignedCampaignsChangeable() {
        return assignedCampaignsChangeable;
    }

    /**
     * Returns the list of campaigns assigned to the item.
     *
     * @return CampaignList the list of campaigns assigned to the item
     */
    public CampaignList getAssignedCampaigns() {
        return assignedCampaigns;
    }

    /**
     * Returns list of campaigns assigned to the item.
     *
     * @return CampaignListData the list of campaigns assigned to the item
     */
    public CampaignListData getAssignedCampaignsData() {
        return assignedCampaigns;
    }

    /**
     * Returns the list of potential campaigns found via campaign determination and validataion
     *
     * @return CampaignList the list of potential campaigns for the item
     */
    public CampaignList getDeterminedCampaigns() {
        return determinedCampaigns;
    }

    /**
     * Returns the list of potential campaigns found via campaign determination and validataion
     *
     * @return CampaignListData the list of potential campaigns for the item
     */
    public CampaignListData getDeterminedCampaignsData() {
        return determinedCampaigns;
    }

    /**
     * Sets the list of campaigns assigned to the item.
     *
     * @param CampaignList the list of campaigns assigned to the item
     */
    public void setAssignedCampaigns(CampaignList assignedCampaigns) {
        this.assignedCampaigns = assignedCampaigns;
    }

    /**
     * Sets list of campaigns assigned to the item.
     *
     * @param CampaignListData the list of campaigns assigned to the item
     */
    public void setAssignedCampaignsData(CampaignListData assignedCampaigns) {
        this.assignedCampaigns = (CampaignList) assignedCampaigns;
    }

    /**
     * Sets the list of potential campaigns found via campaign determination and validataion
     *
     * @param CampaignList the list of potential campaigns for the item
     */
    public void getDeterminedCampaigns(CampaignList determinedCampaigns) {
        this.determinedCampaigns = determinedCampaigns;
    }

    /**
     * Sets the list of potential campaigns found via campaign determination and validataion
     *
     * @param CampaignListData the list of potential campaigns for the item
     */
    public void setDeterminedCampaignsData(CampaignListData determinedCampaigns) {
        this.determinedCampaigns = (CampaignList) determinedCampaigns;
    }

    /**
     * Returns true, if the determinedCampaigns list is not empty.
     *
     * @return boolean true if the determinedCampaigns list is not empty
     */
    public boolean isDeterminedCampaignAvailable() {
        return !determinedCampaigns.isEmpty();
    }

    /**
     * Returns the substitutionReasonId.
     * 
     * @return String
     */
    public String getSubstitutionReasonId() {
        return substitutionReasonId;
    }

    /**
     * Returns the systemProductId.
     * 
     * @return String
     */
    public String getSystemProductId() {
        return systemProductId;
    }

    /**
     * Sets the substitutionReasonId.
     * 
     * @param substitutionReasonId The substitutionReasonId to set
     */
    public void setSubstitutionReasonId(String substitutionReasonId) {
        this.substitutionReasonId = substitutionReasonId;
    }

    /**
     * Sets the systemProductId.
     * 
     * @param systemProductId The systemProductId to set
     */
    public void setSystemProductId(String systemProductId) {
        this.systemProductId = systemProductId;
    }

    /**
     * Creates a new AlternativProductList.
     * 
     * @return AlternativProductListData
     */
    public AlternativProductListData createAlternativProductList() {
        return new AlternativProductList();
    }

    /**
     * Returns the alternativProductList.
     * 
     * @return AlternativProductList
     */
    public AlternativProductList getAlternativProductList() {
        return alternativProductList;
    }

    /**
     * Returns the alternativProductList.
     * 
     * @return AlternativProductListData
     */
    public AlternativProductListData getAlternativProductListData() {
        return alternativProductList;
    }

    /**
     * Sets the alternativProductList.
     * 
     * @param alternativProductList The alternativProductList to set
     */
    public void setAlternativProductList(AlternativProductList alternativProductList) {
        this.alternativProductList = alternativProductList;
    }

    /**
     * Sets the alternativProductList.
     * 
     * @param alternativProductListData The alternativProductList to set
     */
    public void setAlternativProductListData(AlternativProductListData alternativProductList) {
        this.alternativProductList = (AlternativProductList) alternativProductList;
    }

    /**
     * Returns true, if the alternativProductList is not empty.
     * 
     * @return boolean true if the alternativProductList is not empty
     */
    public boolean isProductAliasAvailable() {
        return !alternativProductList.isEmpty();
    }

    /**
     * Returns the parentHandle, that is the handle of the parent, if the position is a subposition
     * 
     * @return String the parentHandle
     */
    public String getParentHandle() {
        return parentHandle;
    }

    /**
     * Sets the parentHandle, that is the handle of the parent, if the position is a subposition
     * 
     * @param parentHandle the new value for the parentHandle
     */
    public void setParentHandle(String parentHandle) {
        this.parentHandle = parentHandle;
    }

    /**
     * Get the latestDlvDate.
     *
     * @return the latest Dlv Date
     */
    public String getLatestDlvDate() {
        return latestDlvDate;
    }

    /**
     * Set the latestDlvDate.
     *
     * @param latestDlvDate the latest Dlv Date to be set.
     */
    public void setLatestDlvDate(String latestDlvDate) {
        this.latestDlvDate = latestDlvDate;
    }

    /**
     *Checks whether or not the latest Dlv Date is changeable.
     *
     * @return <code>true</code> indicates that the information is changeable,
     *         <code>false</code> indicates that the information cannot be changed.
     */
    public boolean isLatestDlvDateChangeable() {
        return latestDlvDateChangeable;
    }

    /**
     *Set the latestDlvDateChangeable flag
     *@param latestDlvDateChangeable <code>true</code> indicates that the
     *        cancel date is changeable 
     */
    public void setLatestDlvDateChangeable(boolean latestDlvDateChangeable) {
        this.latestDlvDateChangeable = latestDlvDateChangeable;
    }

    /**
     * Get the payment terms.
     * 
     * @return String the payment terms
     */
    public String getPaymentTerms() {
        return paymentTerms;
    }

    /**
     * Set the payment terms.
     * 
     * @param paymentTerms the payment terms to be set.
     */
    public void setPaymentTerms(String paymentTerms) {
        this.paymentTerms = paymentTerms;
    }

    /**
     * Set the payment terms description.
     * 
     * @param paymentTermsDesc the payment terms description to be set.
     */
    public void setPaymentTermsDesc(String paymentTermsDesc) {
        this.paymentTermsDesc = paymentTermsDesc;
    }

    /**
     * Get the payment terms description.
     * 
     * @return String the payment terms description.
     */
    public String getPaymentTermsDesc() {
        return paymentTermsDesc;
    }

    /**
     * Checks whether or not the paymentterms information is changeable.
     *
     * @return <code>true</code> indicates that the information is changeable,
     *         <code>false</code> indicates that the information cannot be changed.
     */
    public boolean isPaymentTermsChangeable() {
        return paymentTermsChangeable;
    }

    /**
     * Set the item condition table
     * @return
     */
    public void setItemConditionTable(ItemConditionTableData conditionList) {
        this.itemConditions = (ItemConditionTable) conditionList;

    }

    /**
     * Determine, whether or no the item is from catalog.
     *
     * @return <code>true</code> if the item is from catalog, otherwise
     *         <code>false</code>.
     */
    public boolean isFromCatalog() {
        return isFromCatalog;
    }

    /**
     * Returns the item condition table
     * @return
     */
    public ItemConditionTableData getItemConditionTable() {
        return this.itemConditions;
    }

    /**
     * Determine, whether or no the item is from auction.
     *
     * @return <code>true</code> if the item is from auction, otherwise
     *         <code>false</code>.
     */
    public boolean isFromAuction() {
        return isFromAuction;
    }

    /**
     *
     */
    public void setFromAuction(boolean fromAuction) {
        this.shipToLineChangeable = !fromAuction;
        this.isFromAuction = fromAuction;
    }

    /**
     * A handy method for Creation a new item condition table.
     *
     * @return ItemConditionTableData
     */
    public ItemConditionTableData createItemConditionTable() {
        return new ItemConditionTable();
    }

    /**
     * 
     */
    public void setAllValues(
        boolean configurable,
        boolean configurableChangeable,
        String currency,
        boolean currencyChangeable,
        TechKey contractKey,
        boolean contractKeyChangeable,
        String contractId,
        boolean contractIdChangeable,
        TechKey contractItemKey,
        boolean contractItemKeyChangeable,
        String contractItemId,
        boolean contractItemIdChangeable,
        String contractConfigFilter,
        boolean contractConfigFilterChangeable,
        String description,
        boolean descriptionChangeable,
        ExtendedStatusData extendedStatus,
        boolean extendedStatusChangeable,
        TechKey ipcDocumentId,
        boolean ipcDocumentIdChangeable,
        TechKey ipcItemId,
        boolean ipcItemIdChangeable,
        String ipcProduct,
        boolean ipcProductChangeable,
        String partnerProduct,
        boolean partnerProductChangeable,
        TechKey pcat,
        boolean pcatChangeable,
        String pcatArea,
        boolean pcatAreaChangeable,
        String pcatVariant,
        boolean pcatVariantChangeable,
        String product,
        boolean productChangeable,
        TechKey productId,
        boolean productIdChangeable,
        String quantity,
        boolean quantityChangeable,
        String reqDeliveryDate,
        boolean reqDeliveryDateChangeable,
        ShipToData shipToLine,
        boolean shipToLineChangeable,
        String unit,
        boolean unitChangeable,
        boolean isAvailableInResellerCatalog,
        String latestDlvDate,
    //earlier cancel Date
    boolean latestDlvDateChangeable, String paymentTerms, boolean paymentTermsChangeable) {
        super.setAllValues(
            configurable,
            configurableChangeable,
            currency,
            currencyChangeable,
            description,
            descriptionChangeable,
            extendedStatus,
            extendedStatusChangeable,
            partnerProduct,
            partnerProductChangeable,
            pcat,
            pcatChangeable,
            pcatArea,
            pcatAreaChangeable,
            pcatVariant,
            pcatVariantChangeable,
            product,
            productChangeable,
            productId,
            productIdChangeable,
            quantity,
            quantityChangeable,
            reqDeliveryDate,
            reqDeliveryDateChangeable,
            unit,
            unitChangeable,
            isAvailableInResellerCatalog);

        this.contractKey = contractKey;
        this.contractKeyChangeable = contractKeyChangeable;
        this.contractId = contractId;
        this.contractIdChangeable = contractIdChangeable;
        this.contractItemKey = contractItemKey;
        this.contractItemKeyChangeable = contractItemKeyChangeable;
        this.contractItemId = contractItemId;
        this.contractItemIdChangeable = contractItemIdChangeable;
        this.contractConfigFilter = contractConfigFilter;
        this.contractConfigFilterChangeable = contractConfigFilterChangeable;
        this.shipToLine = (ShipTo) shipToLine;
        this.shipToLineChangeable = shipToLineChangeable;
        this.latestDlvDate = latestDlvDate;
        this.latestDlvDateChangeable = latestDlvDateChangeable;
        this.paymentTerms = paymentTerms;
        this.paymentTermsChangeable = paymentTermsChangeable;
    }

    /**
     * Determine, if the item is a up selling relevant
     *
     * @return <code>true</code> if the item is a sub component, otherwise
     *         <code>false</code>.
     */
    public boolean isUpSellingRelevant() {
        String method = "isUpSellingRelevant()";
        log.entering(method);
        boolean retVal =
            (parentId == null) || parentId.isInitial() || ((parentId != null) && isProductChangeable() && isItemUsageATP());
        log.debug(method + ": isUpSellingRelevant = " + retVal);
        log.exiting();
        return (retVal);
    }
    
    /**
     * This method returns a flag, that indicates if the item is copied from another 
     * item, e.g. when an order is created from an order template
     * 
     * If so, this flag might be used, to suppress things like campaign determination,
     *  etc. for the copied item.
     * 
     * @return true if this item is copied from another item
     *         false else
     */
    public boolean isCopiedFromOtherItem() {
        return isCopiedFromOtherItem;
    }

    /**
     * This method sets a flag, that indicates if the item is copied from another 
     * item, e.g. when an order is created from an order template
     * 
     * If so, this flag might be used, to suppress things like campaign determination,
     *  etc. for the copied item.
     * 
     * @param true if this item is copied from another item
     *         false else
     */
    public void setCopiedFromOtherItem(boolean isCopiedFromOtherItem) {
        this.isCopiedFromOtherItem = isCopiedFromOtherItem;
    }

	/* (non-Javadoc)
	 * @see com.sap.isa.backend.boi.isacore.order.ItemData#isRedemptionItem()
	 */
	public boolean isPtsItem() {
		return isRedemptItem;
	}


    /* (non-Javadoc)
     * @see com.sap.isa.businessobject.ProductHolder#isBuyPtsItem()
     */
    public boolean isBuyPtsItem(){
        return isBuyPointsItem;
    }


	public void setPtsItem(boolean isRedemption) {
		isRedemptItem = isRedemption;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.backend.boi.isacore.order.ItemData#setLoyBuyPointsItem(boolean)
	 */
	public void setBuyPtsItem(boolean isPointsItem) {
		isBuyPointsItem = isPointsItem;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.backend.boi.isacore.order.ItemData#getRedemptionValue()
	 */
	public String getLoyRedemptionValue() {
		return redemptionValue;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.backend.boi.isacore.order.ItemData#setRedemptionValue(java.lang.String)
	 */
	public void setLoyRedemptionValue(String value) {
		redemptionValue = value;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.backend.boi.isacore.order.ItemData#getPointCode()
	 */
	public String getLoyPointCodeId() {
		return pointCodeId;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.backend.boi.isacore.order.ItemData#setPointCode(java.lang.String)
	 */
	public void setLoyPointCodeId(String code) {
		pointCodeId = code;
	}

}
