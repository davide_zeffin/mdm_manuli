/*****************************************************************************
    Class:        ItemConfigurationTransfer
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Thomas Smits
    Created:      25.07.2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/08/03 $
*****************************************************************************/
package com.sap.isa.businessobject.item;

import java.util.Enumeration;
import java.util.Iterator;

import com.sap.spc.remote.client.util.cfg_ext_cstic_val;
import com.sap.spc.remote.client.util.cfg_ext_cstic_val_seq;
import com.sap.spc.remote.client.util.cfg_ext_inst;
import com.sap.spc.remote.client.util.cfg_ext_inst_seq;
import com.sap.spc.remote.client.util.cfg_ext_price_key;
import com.sap.spc.remote.client.util.cfg_ext_price_key_seq;
import com.sap.spc.remote.client.util.ext_configuration;
import com.sap.spc.remote.client.util.imp.c_ext_cfg_inst_seq_imp;

public class ItemConfigurationTransfer {

    private static final Integer[] INTEGERS = {
        new Integer(0),
        new Integer(1),
        new Integer(2),
        new Integer(3),
        new Integer(4),
        new Integer(5),
        new Integer(6),
        new Integer(7),
        new Integer(8),
        new Integer(9),
        new Integer(11),
        new Integer(12),
        new Integer(13),
        new Integer(14),
        new Integer(15),
        new Integer(16),
        new Integer(17),
        new Integer(18),
        new Integer(19),
        new Integer(20),
        new Integer(21),
        new Integer(22),
        new Integer(23),
        new Integer(24),
        new Integer(25),
        new Integer(26),
        new Integer(27),
        new Integer(28),
        new Integer(29),
        new Integer(30)
    };


    private ext_configuration externalConfig;
    private double[] vkFaktor;
    private String[] vkKey;
    private int[]    vkInstId;
    private String[] vkConfigId;
    private String[] valAuthor;
    private String[] valValueTxt;
    private String[] valValue;
    private String[] valCharcTxt;
    private String[] valCharc;
    private int[] valInstId;
    private String[] valConfigId;
    private boolean[] insConsistent;
    private boolean[] insComplete;
    private String[] insQuantityUnit;
    private String[] insObjTxt;
    private String[] insObjKey;
    private String[] insClassType;
    private String[] insObjType;
    private int[] insInstId;
    private String[] insConfigId;
    private String cfgCfgInfo;
    private String cfgKbLanguage;
    private boolean cfgConsistent;
    private boolean cfgComplete;
    private String cfgKbProfile;
    private String cfgKbVersion;
    private String cfgKbName;
    private String cfgSce;
    private String cfgRootId;
    private String cfgConfigId;


    public ItemConfigurationTransfer(ext_configuration externalConfig) {
        this.externalConfig = externalConfig;

// ---------------- Pricing Keys ---------------------------------------
        cfg_ext_price_key_seq seq = externalConfig.get_price_keys();
        int seqLength = seq.size();

        vkFaktor   = new double[seqLength];
        vkKey      = new String[seqLength];
        vkInstId   = new int[seqLength];
        vkConfigId = new String[seqLength];

        Iterator enum = seq.iterator();
        int position = 0;

        while (enum.hasNext()) {
            cfg_ext_price_key priceKey = (cfg_ext_price_key) enum.next();

            vkFaktor[position]   = priceKey.get_factor();
            vkKey[position]      = priceKey.get_key();
            vkInstId[position]   = priceKey.get_inst_id().intValue();
            vkConfigId[position] = "";

            position++;
        }

// ---------------- Characteristic values -------------------------------
        cfg_ext_cstic_val_seq seqCsticVal = externalConfig.get_cstics_values();
        seqLength = seqCsticVal.size();

        valAuthor   = new String[seqLength];
        valValueTxt = new String[seqLength];
        valValue    = new String[seqLength];
        valCharcTxt = new String[seqLength];
        valCharc    = new String[seqLength];
        valInstId   = new int[seqLength];
        valConfigId = new String[seqLength];

        enum = seqCsticVal.iterator();
        position = 0;

        while (enum.hasNext()) {
            cfg_ext_cstic_val csticVal = (cfg_ext_cstic_val) enum.next();

            valAuthor[position]     = csticVal.get_author();
            valValueTxt[position]   = csticVal.get_value_txt();
            valValue[position]      = csticVal.get_value();
            valCharcTxt[position]   = csticVal.get_charc_txt();
            valCharc[position]      = csticVal.get_charc();
            valInstId[position]     = csticVal.get_inst_id().intValue();
            valConfigId[position]   = "";

            position++;
        }

// ------------------ Instances ------------------------------------
        c_ext_cfg_inst_seq_imp seqInst = (c_ext_cfg_inst_seq_imp)externalConfig.get_insts();
        seqLength = seqInst.size();

        insConsistent   = new boolean[seqLength];
        insComplete     = new boolean[seqLength];
        insQuantityUnit = new String[seqLength];
        insObjTxt       = new String[seqLength];
        insObjKey       = new String[seqLength];
        insClassType    = new String[seqLength];
        insObjType      = new String[seqLength];
        insInstId       = new int[seqLength];
        insConfigId     = new String[seqLength];

        Enumeration enum2 = seqInst.elements();
        position = 0;

        while (enum2.hasMoreElements()) {
            cfg_ext_inst inst = (cfg_ext_inst) enum2.nextElement();

            insQuantityUnit[position]   = inst.get_quantity_unit();
            insObjTxt[position]         = inst.get_obj_txt();
            insObjKey[position]         = inst.get_obj_key();
            insClassType[position]      = inst.get_class_type();
            insObjType[position]        = inst.get_obj_type();
            insInstId[position]         = inst.get_inst_id().intValue();
            insConfigId[position]       = "";
            insConsistent[position]     = inst.is_consistent_p();
            insComplete[position]       = inst.is_complete_p();

            position++;
        }


    }

    public double[] getVkFactor() {
        return vkFaktor;
    }

    public String[] getVkKey() {
        return vkKey;
    }

    public int[] getVkInstId() {
         return vkInstId;
    }

    public String[] getVkConfigId() {
        return vkConfigId;
    }

    public String[] getValAuthor() {
        return valAuthor;
    }

    public String[] getValValueTxt() {
        return valValueTxt;
    }

    public String[] getValValue() {
        return valValue;
    }

    public String[] getValCharcTxt() {
        return valCharcTxt;
    }

    public String[] getValCharc() {
        return valCharc;
    }

    public int[] getValInstId() {
        return valInstId;
    }

    public String[] getValConfigId() {
        return valConfigId;
    }

    public boolean[] isInsConsistent() {
        return insConsistent;
    }

    public boolean[] isInsComplete() {
        return insComplete;
    }

    public String[] getInsQuantityUnit() {
        return insQuantityUnit;
    }

    public String[] getInsObjKey() {
        return insObjKey;
    }

    public String[] getInsClassType() {
        return insClassType;
    }

    public String[] getObjType() {
        return insObjType;
    }

    public int[] getInsInstId() {
        return insInstId;
    }

    public String[] getInsConfigId() {
        return insConfigId;
    }

    public String getCfgCfginfo() {
        return externalConfig.get_cfg_info();
    }

    public String getCfgKbLanguage() {
        return externalConfig.get_language();
    }

    public boolean isCfgConsistent() {
        return externalConfig.is_consistent_p();
    }

    public boolean isCfgComplete() {
        return externalConfig.is_complete_p();
    }

    public String getCfgKbProfile() {
        return externalConfig.get_kb_profile_name();
    }

    public String getCfgKbVersion() {
        return externalConfig.get_kb_version();
    }

    public String getCfgKbName() {
        return externalConfig.get_kb_name();
    }

    public String getCfgSce() {
        return externalConfig.get_sce_version();
    }

    public int getCfgRootId() {
        return externalConfig.get_root_id().intValue();
    }

    public String getCfgConfigId() {
        return "";
    }


}