
package com.sap.isa.businessobject.item;

import com.sap.isa.backend.boi.isacore.order.ItemConditionRecordData;
import com.sap.isa.core.TechKey;


public class ItemConditionRecord implements ItemConditionRecordData, Cloneable { 



	private TechKey techKey;
	private String conditionQuantity;
	private String conditionType;
	private String conditionRate;
	private String conditionUOM;
	private String handle;


	/**
	 * Default constructor
	 */
	public ItemConditionRecord() {
		
	}

	/**
	 * @param conditionType
	 */
	public ItemConditionRecord(String conditionType) {
		
		this.conditionType = conditionType;
	}

	/**
	 * @param techKey
	 * @param conditionType
	 * @param conditionRate
	 * @param conditionQuantity
	 * @param conditionUOM
	 */
	public ItemConditionRecord(TechKey techKey, String handle, 
								String conditionType, String conditionRate, String conditionQuantity, String conditionUOM) {
		this.techKey = techKey;
		this.conditionType = conditionType;
		this.conditionRate = conditionRate;
		this.conditionUOM = conditionUOM;
		this.conditionQuantity = conditionQuantity;
		this.handle = handle;	
	}	
	
	/**
	 * Returns the technical Key where the condition record associated
	 *
	 * @return techkey is used to identify the delivery in the backend
	 */
	public TechKey getTechKey(){
		return techKey;
	}
	
	
	/**
	 * Returns the condition quantity
	 *
	 * @return quantity
	 */
	public String getConditionQuantity(){
		return conditionQuantity;
	}
	
	
	/**
	 * Sets TechKey
	 *
	 */
	public void setTechKey(TechKey techKey){
		this.techKey = techKey;
	}
	
	
	/**
	 * Sets Quantity
	 *
	 */
	public void setConditionQuantity(String quantity){
		this.conditionQuantity = quantity;
	}
	

	/**
	 * Returns the Handle, that is the handle of the item doc, if the position is a subposition
	 * 
	 * @return String the itemHandle
	 */
	public String getItemHandle() {
		return handle;
	}
   
	/**
	 * Sets the itemHandle, that is the handle of the item doc, if the position is a subposition
	 * 
	 * @param parentHandle the new value for the itemHandle
	 */
	public void setItemHandle(String itemHandle){
		this.handle = itemHandle;
	}
	
	/**
	 * Returns the Condition type, that is the condition type for condition record, 
	 * 
	 * @return String the conditionType
	 */
	public String getConditionType() {
		return conditionType;
	}
   
	/**
	 * Sets the conditionType, that is the condition type for condition record
	 * 
	 * @param condition type the new value for the conditionType
	 */
	public void setConditionType(String type){	
		this.conditionType = type;
	}
	
	/**
	 * Returns the Condition rate, that is the condition rate for condition record, 
	 * 
	 * @return String the conditionRate
	 */
	public String getConditionRate() {
		return conditionRate;
	}
   
	/**
	 * Sets the conditionType, that is the condition type for condition record
	 * 
	 * @param condition rate the new value for the conditionRate
	 */
	public void setConditionRate(String rate){	
		this.conditionRate = rate;
	}

	/**
	 * Returns the Condition UOM, that is the condition UOM for condition record, 
	 * 
	 * @return String the conditionUOM
	 */
	public String getConditionUOM() {
		return conditionUOM;
	}
   
	/**
	 * Sets the conditionUOM, that is the condition type for condition UOM
	 * 
	 * @param condition UOM the new value for the conditionUOM
	 */
	public void setConditionUOM(String conditionUOM){
		this.conditionUOM = conditionUOM;	
	}

	/**
	 * Performs a deep copy of this object.
	 * Because of the fact that
	 * all fields of this object consist of immutable objects like
	 * <code>String</code> and <code>TechKey</code> or primitive types
	 * the shallow copy is identical with a deep copy. 
	 *
	 * @return shallow (deep-like) copy of this object
	 */
	public Object clone() {
		ItemConditionRecord myClone = new ItemConditionRecord(techKey, handle, 
								conditionType, 
								conditionRate, conditionQuantity, conditionUOM);
		return myClone;

	}

}
