/*****************************************************************************
    Class:        ItemNegotiatedContract
    Copyright (c) 2002, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      22.112002
    Version:      1.0

    $Revision: #1 $
    $Date: 2002/11/22 $
*****************************************************************************/

package com.sap.isa.businessobject.item;

import com.sap.isa.backend.boi.isacore.negotiatedcontract.ItemNegotiatedContractData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.util.table.ResultData;

/**
 * Item of a sales document. Each sales document consists of a header and a
 * number of items. This items are represented by this class.
 *
 * @author  SAP AG
 * @version 1.0
 */
public class ItemNegotiatedContract extends ItemSalesDoc implements
//            Iterable,
            ItemNegotiatedContractData
//            Cloneable,
//            CUAData,
//            ProductHolder {
{
/*    public static final String DOCUMENT_STATUS_OPEN      = "open";

    public static final String DOCUMENT_STATUS_RELEASED      = "released";

    public static final String DOCUMENT_STATUS_REJECTED      = "rejected";

    public static final String DOCUMENT_STATUS_ACCEPTED      = "accepted";

    public static final String DOCUMENT_STATUS_CANCELLED      = "cancelled";

    public static final String DOCUMENT_STATUS_INQUIRY_SENT      = "inquirysent";

    public static final String DOCUMENT_STATUS_INQUIRY_CONSTRUCTION      = "inquiryconstruction";
*/
//    private Product catalogProduct;
//    private String  currency;
//    private TechKey contractKey;
//    private String contractId;
//    private TechKey contractItemKey;
//    private String contractItemId;
//    private String contractConfigFilter;
//    private String  description;
//    private boolean configurable;
//    private String  deliveredQuantity;
/*    private Object  externalItem;  // must cast to IPCItem for usage
    private String  itmTypeUsage;
    private String  itmUsage;
    private String  numberInt;
    private TechKey pcat;
    private String  pcatArea;
    private String  pcatVariant;
    private String  product;
    private TechKey productId;
    private String  quantity;
    private String  oldQuantity = "";  // for business event modify document
    private String  cumulQuantity;
    private Text    text;
    private String  unit;
    private String  netPriceUnit;
    private String  netQuantPriceUnit;
    private String  cumulQuantityUnit;
    private TechKey parentId;
    private String  partnerProduct;
    private ShipTo  shipToLine;
    private String  strtext;
//    private String  taxValue;
    private String  netValue;
//    private String  freightValue;
//    private String  grossValue;
    private String  netPrice;
//    private String reqDeliveryDate;
//    private String confirmedDeliveryDate;
    private String confirmedQuantity;
    private String validTo;
//    private Delivery delivery;
    private String status;
    private String[] possibleUnits;
    private String quantityToDeliver;
    private List   itemDelivery = new ArrayList(10);
//    private boolean deliveryExists = false;
//    private ArrayList scheduleLines = new ArrayList();
    protected PartnerList partnerList = new PartnerList();

    private boolean pcatChangeable;
    private boolean pcatAreaChangeable;
    private boolean pcatVariantChangeable;
//    private boolean reqDeliveryDateChangeable;
    private boolean currencyChangeable;
//    private boolean contractKeyChangeable;
//    private boolean contractIdChangeable;
//    private boolean contractItemKeyChangeable;
//    private boolean contractItemIdChangeable;
//    private boolean contractConfigFilterChangeable;
    private boolean configurableChangeable;
    private boolean descriptionChangeable;
    private boolean externalItemChangeable;
    private boolean partnerProductChangeable;
    private boolean productChangeable;
    private boolean productIdChangeable;
    private boolean quantityChangeable;
    private boolean shipToLineChangeable;
    private boolean unitChangeable;
    private boolean deletable;
    private boolean cancelable;
    private CUAListData cuaList;
    private boolean onlyVarFind;
    protected boolean isAvailableInResellerCatalog;
*/


    public static final String CONDITION_CLASS_PRICE       = "price";
    public static final String CONDITION_CLASS_REBATE      = "rebate";
    
    
    private String targetValue;
    private String PriceNormal;
    private String UnitPriceNormal;
    private String PricingUnitPriceNormal;    
    private String PriceInquired;
    private String UnitPriceInquired;
    private String PricingUnitPriceInquired;
    private String PriceOffered;
    private String UnitPriceOffered;
    private String PricingUnitPriceOffered;
    private String condIdOffered;
    private String condTypeOffered;
    private String condRateOffered;
    private String condUnitOffered;
    private String condPriceUnitOffered;
    private String condIdInquired;
    private String condTypeInquired;
    private String condRateInquired;
    private String condUnitInquired;
    private String condPriceUnitInquired;
    private String rejectionReason;
    private String itemType;
    private String condClassInquired;
    private String condClassOffered;
    private ResultData condValues;
    private boolean condPriceUnitInquiredChangeable;
    private boolean condRateInquiredChangeable;
    private boolean condTypeInquiredChangeable;       
    private boolean condUnitInquiredChangeable;
   /**
     * Default constructor for the Item
     */
   public ItemNegotiatedContract() {
   }

    /**
     * Constructor for the object directly taking the technical
     * key for the object
     * if the technical key is empty an unique handle will be generate
     *
     * @param techKey Technical key for the object
     */
   public ItemNegotiatedContract(TechKey techKey) {
        super(techKey);
        //if (techKey == null || techKey.isInitial() || techKey.getIdAsString().length()<1) {
            // create a handle for the new item!
        //    createUniqueHandle();
        //}
    }


 
    /**
     * Special copy constructor used to create a new ItemNegotiatedContract for
     * a given <code>BasketTransferItem</code>.
     *
     * @param transferItm Transfer item used for construction
     */
    public ItemNegotiatedContract(BasketTransferItem transferItm) {
 
        
     
        super.setProduct(transferItm.getProductId());
        super.setQuantity(transferItm.getQuantity());
        super.setUnit(transferItm.getUnit());
        
       // initialize parent-item
        super.setParentId(new TechKey(null));

        // create a handle for the new item!
        createUniqueHandle();        
 
    }

    /**
     * Creates a text object. This method is used by backend objects to
     * get instances of the text object.
     *
     * @return newly created text object
     */
/*    public TextData createText() {
        return (TextData) new Text();
    }
*/
    /**
     * Set an array of all possible units for this item.
     *
     * @param possibleUnits the units that can be choosen for this item
     */
/*    public void setPossibleUnits(String[] possibleUnits) {
        this.possibleUnits = possibleUnits;
    }
*/
    /**
     * Get the array of all possible units that can be used for this item.
     *
     * @return list of all possible units
     */
/*    public String[] getPossibleUnits() {
        return possibleUnits;
    }
*/
    /**
     * Set the internal number for the item. The internal number is the
     * item number (Positionsnummer) displayed on the front end and generated
     * by the backend system. In the default case the numbers are starting
     * with 10 and increased by 10, e.g. 10, 20, 30, 40....
     *
     * @param numberInt the item number to be set for this item
     */
/*    public void setNumberInt(String numberInt) {
        this.numberInt = numberInt;
    }
*/
    /**
     * Get the currency for the item.
     *
     * @return the currency
     */
/*    public String getCurrency() {
        return currency;
    }
*/
/*    public void setText(String text) {
      this.strtext = text;
    }
*/
    /**
     * Get the technical key for the contract associated with this item.
     *
     * @return contract key
     */
  /*  public TechKey getContractKey() {
        return contractKey;
    }
  */
    /**
     * Set the technical key for the contract associated with this item.
     *
     * @param contractKey the key to be set
     */
  /*  public void setContractKey(TechKey contractKey) {
        this.contractKey = contractKey;
    }
  */
    /**
     * Get the id for the contract associated with this item.
     *
     * @return contract id
     */
  /*  public String getContractId() {
        return contractId;
    }
  */
    /**
     * Set the id for the contract associated with this item.
     *
     * @param contractId the id to be set
     */
  /*  public void setContractId(String contractId) {
        this.contractId = contractId;
    }
  */
    /**
     * Get the technical key of the item in the contract.
     *
     * @return technical key
     */
  /*  public TechKey getContractItemKey() {
        return contractItemKey;
    }
  */
    /**
     * Set the technical key of the item in the contract.
     *
     * @param contractItemKey technical key in contract
     */
  /*  public void setContractItemKey(TechKey contractItemKey) {
        this.contractItemKey = contractItemKey;
    }
  */
    /**
     * Get the id of the item in the contract.
     *
     * @return id in contract
     */
  /*  public String getContractItemId() {
        return contractItemId;
    }
  */
    /**
     * Set the id of the item in the contract.
     *
     * @param contractItemId id to be set
     */
  /*  public void setContractItemId(String contractItemId) {
        this.contractItemId = contractItemId;
    }
  */
    /**
     *
     */
  /*  public String getContractConfigFilter() {
        return contractConfigFilter;
    }
  */
    /**
     *
     */
  /*  public void setContractConfigFilter(String contractConfigFilter) {
        this.contractConfigFilter = contractConfigFilter;
    }
  */
    /**
     * Get descriptions on the item level.
     *
     * @return description
     */
/*    public String getDescription() {
        return description;
    }
*/
    /**
     * Determine, wheter or no the item is configurable.
     *
     * @return <code>true</code> if the item can be configured, otherwise
     *         <code>false</code>.
     */
/*    public boolean isConfigurable() {
        return configurable;
    }
*/
    /**
     *
     */
/*    public Object getExternalItem() {
        return externalItem;
    }
*/
    /**
     *
     */
/*    public void setItmTypeUsage(String itmTypeUsage) {
        this.itmTypeUsage = itmTypeUsage;
    }
*/
    /**
     *
     */
/*    public String getItmTypeUsage() {
        return itmTypeUsage;
    }
*/
    /**
     *
     */
/*    public void setItmUsage(String itmUsage) {
        this.itmUsage = itmUsage;
    }
*/
    /**
     *
     */
/*    public String getItmUsage() {
        return itmUsage;
    }
*/
    /**
     * Get the internal number for the item. The internal number is the
     * item number (Positionsnummer) displayed on the front end and generated
     * by the backend system. In the default case the numbers are starting
     * with 10 and increased by 10, e.g. 10, 20, 30, 40....
     *
     * @return the item number
     */
/*    public String getNumberInt() {
        return numberInt;
    }
*/
    /**
     * Get the technical key specifying the catalog used to select the item
     * and put it into the basket.
     *
     * @return technical key of catalog
     */
/*    public TechKey getPcat() {
        return pcat;
    }
*/
    /**
     * Get the catalog area used to select the item
     * and put it into the basket.
     *
     * @return catalog area
     */
/*    public String getPcatArea() {
        return pcatArea;
    }
*/
    /**
     * Get the catalog variant used to select the item
     * and put it into the basket.
     *
     * @return catalog variant
     */
/*    public String getPcatVariant() {
        return pcatVariant;
    }
*/
    /**
     * Get the product for the item. The product is the short number, not
     * the technical key, for the item, e.g. <code>tt02</code>. The value
     * returned by this method may not be unique. Use
     * <code>getProductId()</code> instead.
     *
     * @return the product
     */
/*    public String getProduct() {
        return product;
    }
*/
    /**
     * Get the catalog product associated with this item. The product
     * wraps around a catalog item and offers convenience methods for the
     * retrievement of catalog data.
     *
     * @return the product
     */
/*    public Product getCatalogProduct() {
        return catalogProduct;
    }
*/
    /**
     * Set the catalog producr associated with this item. The product
     * wraps around a catalog item and offers convenience methods for the
     * retrievement of catalog data.
     *
     * @param catalogProduct the product to be set
     */
/*    public void setCatalogProduct(Product catalogProduct) {
        this.catalogProduct = catalogProduct;
    }
*/
    /**
     * Get the technical key for the product of this item.
     *
     * @return technical key for the product
     */
/*    public TechKey getProductId() {
        return productId;
    }
*/

    /**
     * Set the property oldQuantity
     *
     * @param oldQuantity
     */
/*    public void setOldQuantity(String oldQuantity) {
        this.oldQuantity = oldQuantity;
    }
*/

    /**
     * Returns the property oldQuantity
     *
     * @return oldQuantity
     */
/*    public String getOldQuantity() {
       return this.oldQuantity;
    }
*/

    /**
     * Get the quantity for this item.
     *
     * @return quantity
     */
/*    public String getQuantity() {
        return quantity;
    }
*/
    /**
     * Get the delivered quantity for this item.
     *
     * @return delivered quantity
     */
  /*  public String getDeliveredQuantity() {
        return deliveredQuantity;
    }
  */
    /**
     *
     */
/*    public String getCumulQuantity() {
        return cumulQuantity;
    }
*/
    /**
     * Get additional text for this item.
     *
     * @return text for this item
     */
/*    public TextData getText() {
        return (TextData)text;
    }
*/
    /**
     * Get the unit, the quantity is measured in.
     *
     * @return the unit
     */
/*    public String getUnit() {
        return unit;
    }
*/
    /**
     * Get the condition unit ,
     *
     * @return the condition unit
     */
/*    public String getNetPriceUnit() {
        return netPriceUnit;
    }
*/
    /**
     * Get the condition pricing unit,
     *
     * @return the condition pricing unit
     */
/*    public String getNetQuantPriceUnit() {
        return netQuantPriceUnit;
    }
*/
    /**
     * Get the unit, the cumulative quantity is measured in.
     *
     * @return the unit
     */
/*    public String getCumulQuantityUnit() {
        return cumulQuantityUnit;
    }
*/
    /**
     *
     */
  /*  public TechKey getParentId() {
        return parentId;
    }
  */
    /**
     *
     */
/*    public String getPartnerProduct() {
        return partnerProduct;
    }
*/
    /**
     * Get the business partner list
     *
     * @return PartnerList list of business partners
     */
/*    public PartnerList getPartnerList() {
        return partnerList;
    }
*/
    /**
     * Set the business partner list
     *
     * @param PartnerList new list of business partners
     */
/*    public void setPartnerList(PartnerList partnerList) {
        this.partnerList = partnerList;
    }
*/
    /**
     * Get the business partner list
     *
     * @return PartnerListData list of business partners
     */
/*    public PartnerListData getPartnerListData() {
        return partnerList;
    }
*/
    /**
     *
     */
/*    public ShipTo getShipTo() {
        return shipToLine;
    }
*/
    /**
     *
     */
/*    public ShipToData getShipToData() {
        return shipToLine;
    }
*/

/*    public Iterator iterator() {
        return itemDelivery.iterator();
    }
*/
    /**
     *
     */
/*    public void setNetValue(String netValue) {
        this.netValue = netValue;
    }
*/
    /**
     *
     */
/*    public String getNetValue() {
        return netValue;
    }
*/
    /**
     *
     */
/*    public void setNetPrice(String netPrice) {
        this.netPrice = netPrice;
    }
*/
    /**
     *
     */
/*    public String getNetPrice() {
        return netPrice;
    }
*/

    /**
     *
     */
/*    public void setCurrency(String currency) {
        this.currency = currency;
    }
*/
    /**
     *
     */
/*    public void setConfigurable(boolean configurable) {
        this.configurable = configurable;
    }
*/
    /**
     *
     */
/*    public void setDeletable(boolean deletable) {
        this.deletable = deletable;
    }
*/
    /**
     *
     */
/*    public void setCancelable(boolean cancelable) {
        this.cancelable = cancelable;
    }
*/
    /**
     *
     */
/*    public void setDescription(String description) {
        this.description = description;
    }
*/
    /**
     *
     */
/*    public void setExternalItem(Object externalItem) {
        this.externalItem = externalItem;
    }
*/
    /**
     *
     */
/*    public void setPartnerProduct(String partnerProduct) {
        this.partnerProduct = partnerProduct;
    }
*/
    /**
     *
     */
/*    public void setProduct(String product) {
        this.product = product;
    }
*/
/*    public void setProductId(TechKey productId) {
        this.productId = productId;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public void setCumulQuantity(String cumulQuantity) {
        this.cumulQuantity = cumulQuantity;
    }

    public void setShipTo(ShipTo shipToLine) {
        this.shipToLine = shipToLine;
    }

    public void setShipToData(ShipToData shipToLine) {
        this.shipToLine = (ShipTo) shipToLine;
    }

    public void setText(Text text) {
        this.text = text;
    }

    public void setText(TextData text) {
        this.text = (Text)text;
    }
    public void setUnit(String unit) {
        this.unit = unit;
    }

    public void setNetPriceUnit(String netPriceUnit) {
        this.netPriceUnit = netPriceUnit;
    }

    public void setNetQuantPriceUnit(String netQuantPriceUnit) {
        this.netQuantPriceUnit = netQuantPriceUnit;
    }

    public void setCumulQuantityUnit(String cumulQuantityUnit) {
        this.cumulQuantityUnit = cumulQuantityUnit;
    }

    public void setUnitChangeable(boolean unitChangeable) {
        this.unitChangeable = unitChangeable;
    }

    public void setQuantityChangeable(boolean quantityChangeable) {
        this.quantityChangeable = quantityChangeable;
    }

    public void setPcat(TechKey pcat) {
        this.pcat = pcat;
    }
*/
    /**
     * Indicates whether the product of the item is available in the catalog of
     * the reseller, if a reseller is assigned to the icon or the header of
     * the salesdocument.
     *
     * @return <code>true</code> if the product is available in the catalog of the reseller
     *         <code>false</code>.
     */
/*    public boolean isAvailableInResellerCatalog() {
        return isAvailableInResellerCatalog;
    }
*/

    /**
     * Sets flag that indicates, if the product of the item is available in the catalog
     * of the reseller, if a reseller is assigned to the icon or the header of
     * the salesdocument.
     *
     * @param <code>true</code> if the product is available in the catalog of the reseller
     *         <code>false</code>.
     */
/*    public void setAvailableInResellerCatalog(boolean isAvailableInResellerCatalog) {
        this.isAvailableInResellerCatalog = isAvailableInResellerCatalog;
    }


    public boolean isCurrencyChangeable() {
        return currencyChangeable;
    }
*/
    /**
     *
     */
  /*  public boolean isContractKeyChangeable() {
        return contractKeyChangeable;
    }
  */
    /**
     *
     */
  /*  public boolean isContractIdChangeable() {
        return contractIdChangeable;
    }
  */
    /**
     *
     */
  /*  public boolean isContractItemKeyChangeable() {
        return contractItemKeyChangeable;
    }
  */
    /**
     *
     */
  /*  public boolean isContractItemIdChangeable() {
        return contractItemIdChangeable;
    }
  */
    /**
     *
     */
  /*  public boolean isContractConfigFilterChangeable() {
        return contractConfigFilterChangeable;
    }
  */
    /**
     *
     */
/*    public boolean isConfigurableChangeable() {
        return configurableChangeable;
    }
*/
    /**
     *
     */
/*    public boolean isDescriptionChangeable() {
        return descriptionChangeable;
    }
*/
    /**
     *
     */
/*    public boolean isExternalItemChangeable() {
        return externalItemChangeable;
    }
*/
    /**
     *
     */
/*    public boolean isPartnerProductChangeable() {
        return partnerProductChangeable;
    }
*/
    /**
     *
     */
/*    public boolean isProductChangeable() {
        return productChangeable;
    }
*/
    /**
     *
     */
/*    public boolean isProductIdChangeable() {
        return productIdChangeable;
    }
*/
    /**
     *
     */
/*    public boolean isQuantityChangeable() {
        return quantityChangeable;
    }
*/
    /**
     *
     */
/*    public boolean isShipToLineChangeable() {
        return shipToLineChangeable;
    }
*/
    /**
     *
     */
/*    public boolean isUnitChangeable() {
        return unitChangeable;
    }

    public boolean isDeletable() {
        return deletable;
    }

    public boolean isCancelable() {
        return cancelable;
    }
*/
    /**
     *
     */
//    public boolean isTaxValueChangeable() {
//      return taxValueChangeable;
//    }

    /**
     *
     */
/*    public boolean isReqDeliveryDateChangeable() {
        return reqDeliveryDateChangeable;
    }
*/
    /**
     *
     */
/*    public Delivery getDelivery( ){
        return delivery;
    }
*/
    /**
     *
     */
  /*  public void setDelivery(Delivery delivery) {
        this.delivery = delivery;
    }
  */
    /**
     *
     */
   /* public ArrayList getScheduleLines(){
        return this.scheduleLines;
    }
  */
    /**
     *
     */
   /* public void setScheduleLines(ArrayList scheduleLines) {
        this.scheduleLines = scheduleLines;
    }
  */
    /**
     *
     */
/*    public String getStatus() {
        return status;
    }

   public void setStatusOpen() {
        status = DOCUMENT_STATUS_OPEN;
    }

   public void setStatusAccepted() {
        status = DOCUMENT_STATUS_ACCEPTED;
    }
    public void setStatusRejected() {
        status = DOCUMENT_STATUS_REJECTED;
    }

    public void setStatusCancelled(){
        this.status = DOCUMENT_STATUS_CANCELLED;
    }

    public void setStatusInquirySent() {
        status = DOCUMENT_STATUS_INQUIRY_SENT;
    }

    public void setStatusInquiryConstruction() {
        status = DOCUMENT_STATUS_INQUIRY_CONSTRUCTION;
    }

    public void setPcatArea(String pcatArea) {
        this.pcatArea = pcatArea;
    }

    public void setPcatVariant(String pcatVariant) {
        this.pcatVariant = pcatVariant;
        }

    public boolean isPcatChangeable() {
        return pcatChangeable;
        }

    public boolean isPcatAreaChangeable() {
        return pcatAreaChangeable;
    }

    public boolean isPcatVariantChangeable() {
        return pcatVariantChangeable;
    }

    /**
     * Set the CUAList in the a product.
     *
     * @param cUAList reference to the CUA-List
     *
     */
/*    public void setCUAList(CUAListData cUAList) {
        this.cuaList = cUAList;
    }
*/

    /**
     * Get the CUAList in the a product.
     *
     * @return reference to the CUA-List
     */
/*    public CUAListData getCUAList() {
        return cuaList;
    }
*/


/*    public String toString() {
        return "ItemSalesDoc [" +
               "techkey=\""                     + techKey + "\"" +
               ", currency=\""                  + currency + "\"" +
//               ", contractId=\""                + contractId + "\"" +
               ", description=\""               + description + "\"" +
               ", configurable=\""              + configurable + "\"" +
//               ", ipcDocumentId=\""             + ipcDocumentId + "\"" +
//               ", ipcItemId=\""                 + ipcItemId + "\"" +
//               ", ipcProduct=\""                + ipcProduct + "\"" +
               ", itmTypeUsage=\""              + itmTypeUsage + "\"" +
               ", numberInt=\""                 + numberInt + "\"" +
               ", pcat=\""                      + pcat + "\"" +
               ", pcatArea=\""                  + pcatArea + "\"" +
               ", pcatVariant=\""               + pcatVariant + "\"" +
               ", product=\""                   + product + "\"" +
               ", productId=\""                 + productId + "\"" +
               ", quantity=\""                  + quantity + "\"" +
               ", oldQuantity=\""               + oldQuantity + "\"" +
               ", text=\""                      + text + "\"" +
               ", unit=\""                      + unit + "\"" +
               ", partnerProduct=\""            + partnerProduct + "\"" +
               ", shipToLine=\""                + shipToLine + "\"" +
//               ", taxValue=\""                  + taxValue + "\"" +
               ", netValue=\""                  + netValue + "\"" +
//               ", freightValue=\""              + freightValue + "\"" +
//               ", grossValue=\""                + grossValue + "\"" +
//               ", reqDeliveryDate=\""           + reqDeliveryDate + "\"" +
//               ", confirmedDeliveryDate=\""     + confirmedDeliveryDate + "\"" +
               ", confirmedQuantity=\""         + confirmedQuantity + "\"" +
               ", validTo=\""                   + validTo + "\"" +
//               ", delivery=\""                  + delivery + "\"" +
               ", status=\""                    + status + "\"" +
               ", possibleUnits=\""             + possibleUnits + "\"" +
               ", itemDelivery=\""              + itemDelivery + "\"" +
               ", partnerList=\""               + partnerList.toString() + "\"" +
               ", pcatChangeable=\""            + pcatChangeable + "\"" +
               ", pcatAreaChangeable=\""        + pcatAreaChangeable + "\"" +
               ", pcatVariantChangeable=\""     + pcatVariantChangeable + "\"" +
//              ", reqDeliveryDateChangeable=\"" + reqDeliveryDateChangeable + "\"" +
               ", currencyChangeable=\""        + currencyChangeable + "\"" +
//               ", contractIdChangeable=\""      + contractIdChangeable + "\"" +
               ", configurableChangeable=\""    + configurableChangeable + "\"" +
               ", descriptionChangeable=\""     + descriptionChangeable + "\"" +
//               ", ipcDocumentIdChangeable=\""   + ipcDocumentIdChangeable + "\"" +
//               ", ipcItemIdChangeable=\""       + ipcItemIdChangeable + "\"" +
//               ", ipcProductChangeable=\""      + ipcProductChangeable + "\"" +
               ", partnerProductChangeable=\""  + partnerProductChangeable + "\"" +
               ", productChangeable=\""         + productChangeable + "\"" +
               ", productIdChangeable=\""       + productIdChangeable + "\"" +
               ", quantityChangeable=\""        + quantityChangeable + "\"" +
               ", shipToLineChangeable=\""      + shipToLineChangeable + "\"" +
               ", unitChangeable=\""            + unitChangeable + "\"" +
               ", isAvailableInResellerCatalog=\""                      + isAvailableInResellerCatalog + "\"" +
               ", text=\""                      + text + "\"" +
               ", messages=\""                  + getMessageList() + "\"" +
               "]";
    }

    public void setAllValues(boolean configurable,
                    boolean configurableChangeable,
                    String  currency,
                    boolean currencyChangeable,
//                    TechKey contractKey,
//                    boolean contractKeyChangeable,
//                    String contractId,
//                    boolean contractIdChangeable,
//                    TechKey contractItemKey,
//                    boolean contractItemKeyChangeable,
//                    String contractItemId,
//                    boolean contractItemIdChangeable,
//                    String contractConfigFilter,
//                    boolean contractConfigFilterChangeable,
//                    String    confirmedDeliveryDate,
//                    boolean confirmedDeliveryDateChangeable,
                    String  description,
                    boolean descriptionChangeable,
//                    String  freightValue,
//                    boolean freightValueChangeable,
//                    String  grossValue,
//                    boolean grossValueChangeable,
//                    Header  header,
//                    boolean headerChangeable,
//                    TechKey id,
//                    boolean idChangeable,
                    TechKey ipcDocumentId,
                    boolean ipcDocumentIdChangeable,
                    TechKey ipcItemId,
                    boolean ipcItemIdChangeable,
                    String  ipcProduct,
                    boolean ipcProductChangeable,
//                    String  itmTypeUsage,
//                    boolean itmTypeUsageChangeable,
//                    String  netValue,
//                    boolean netValueChangeable,
//                    String  numberInt,
//                    boolean numberIntChangeable,
                    String  partnerProduct,
                    boolean partnerProductChangeable,
                    TechKey  pcat,
                    boolean pcatChangeable,
                    String  pcatArea,
                    boolean pcatAreaChangeable,
                    String  pcatVariant,
                    boolean pcatVariantChangeable,
                    String  product,
                    boolean productChangeable,
                    TechKey productId,
                    boolean productIdChangeable,
                    String  quantity,
                    boolean quantityChangeable,
//                    String    reqDeliveryDate,
//                    boolean reqDeliveryDateChangeable,
                    ShipToData  shipToLine,
                    boolean shipToLineChangeable,
                    String  unit,
                    boolean unitChangeable,
                    boolean isAvailableInResellerCatalog
//                    String  taxValue,
//                    boolean taxValueChangeable
                    ) {

        this.configurable                               = configurable;
        this.configurableChangeable     = configurableChangeable;
        this.currency                               = currency;
        this.currencyChangeable         = currencyChangeable;
//        this.contractKey                = contractKey;
//        this.contractKeyChangeable      = contractKeyChangeable;
//        this.contractId                 = contractId;
//        this.contractIdChangeable       = contractIdChangeable;
//        this.contractItemKey            = contractItemKey;
//        this.contractItemKeyChangeable  = contractItemKeyChangeable;
//        this.contractItemId             = contractItemId;
//        this.contractItemIdChangeable   = contractItemIdChangeable;
//       this.contractConfigFilter       = contractConfigFilter;
//        this.contractConfigFilterChangeable = contractConfigFilterChangeable;
//        this.confirmedDeliveryDate               = confirmedDeliveryDate;
//        this.confirmedDeliveryDateChangeable     = confirmedDeliveryDateChangeable;
        this.description                = description;
        this.descriptionChangeable      = descriptionChangeable;
//        this.freightValue               = freightValue;
//        this.freightValueChangeable     = freightValueChangeable;
//        this.grossValue                 = grossValue;
//        this.grossValueChangeable       = grossValueChangeable;
//        this.header                     = header;
//        this.headerChangeable           = headerChangeable;
//        this.id                         = id;
//        this.idChangeable               = idChangeable;
//        this.ipcDocumentId              = ipcDocumentId;
//        this.ipcDocumentIdChangeable    = ipcDocumentIdChangeable;
//        this.ipcItemId                  = ipcItemId;
//        this.ipcItemIdChangeable        = ipcItemIdChangeable;
//        this.ipcProduct                 = ipcProduct;
//        this.ipcProductChangeable       = ipcProductChangeable;
//        this.itmTypeUsage               = itmTypeUsage;
//        this.itmTypeUsageChangeable     = itmTypeUsageChangeable;
//        this.netValue                   = netValue;
//        this.netValueChangeable         = netValueChangeable;
//        this.numberInt                  = numberInt;
//        this.numberIntChangeable        = numberIntChangeable;
        this.partnerProduct             = partnerProduct;
        this.partnerProductChangeable   = partnerProductChangeable;
        this.pcat                       = pcat;
        this.pcatChangeable             = pcatChangeable;
        this.pcatArea                   = pcatArea;
        this.pcatAreaChangeable         = pcatAreaChangeable;
        this.pcatVariant                = pcatVariant;
        this.pcatVariantChangeable      = pcatVariantChangeable;
        this.product                    = product;
        this.productChangeable          = productChangeable;
        this.productId                  = productId;
        this.productIdChangeable        = productIdChangeable;
        this.quantity                   = quantity;
        this.quantityChangeable         = quantityChangeable;
//        this.reqDeliveryDate               = reqDeliveryDate;
//        this.reqDeliveryDateChangeable      = reqDeliveryDateChangeable;
        this.shipToLine                 = (ShipTo) shipToLine;
        this.shipToLineChangeable       = shipToLineChangeable;
        this.unit                       = unit;
        this.unitChangeable             = unitChangeable;
        this.isAvailableInResellerCatalog = isAvailableInResellerCatalog;
//        this.taxValue                   = taxValue;
//        this.taxValueChangeable         = taxValueChangeable;
    }

    public void setAllValuesChangeable(boolean configurableChangeable,
                    boolean currencyChangeable,
                    boolean contractKeyChangeable,
                    boolean contractIdChangeable,
                    boolean contractItemKeyChangeable,
                    boolean contractItemIdChangeable,
                    boolean contractConfigFilterChangeable,
                    boolean reqDeliveryDateChangeable,
                    boolean descriptionChangeable,
//                    boolean freightValueChangeable,
//                    boolean grossValueChangeable,
//                    boolean headerChangeable,
//                    boolean idChangeable,
                    boolean ipcDocumentIdChangeable,
                    boolean ipcItemIdChangeable,
                    boolean ipcProductChangeable,
//                    boolean itmTypeUsageChangeable,
//                    boolean netValueChangeable,
//                    boolean numberIntChangeable,
                    boolean partnerProductChangeable,
                    boolean pcatChangeable,
                    boolean pcatAreaChangeable,
                    boolean pcatVariantChangeable,
                    boolean productChangeable,
                    boolean productIdChangeable,
                    boolean quantityChangeable,
//                    boolean reqDeliveryDateChangeable,
                    boolean shipToLineChangeable,
//                    boolean taxValueChangeable,
                    boolean unitChangeable
                    ) {

        this.configurableChangeable     = configurableChangeable;
        this.currencyChangeable         = currencyChangeable;
//        this.contractKeyChangeable      = contractKeyChangeable;
//        this.contractIdChangeable       = contractIdChangeable;
//        this.contractItemKeyChangeable  = contractItemKeyChangeable;
//        this.contractItemIdChangeable   = contractItemIdChangeable;
//        this.contractConfigFilterChangeable = contractConfigFilterChangeable;
//        this.confirmedDeliveryDateChangeable     = confirmedDeliveryDateChangeable;
        this.descriptionChangeable      = descriptionChangeable;
//        this.freightValueChangeable     = freightValueChangeable;
//        this.grossValueChangeable       = grossValueChangeable;
//        this.headerChangeable           = headerChangeable;
//        this.idChangeable               = idChangeable;
//        this.ipcDocumentIdChangeable    = ipcDocumentIdChangeable;
//        this.ipcItemIdChangeable        = ipcItemIdChangeable;
//        this.ipcProductChangeable       = ipcProductChangeable;
//        this.itmTypeUsageChangeable     = itmTypeUsageChangeable;
//        this.netValueChangeable         = netValueChangeable;
//        this.numberIntChangeable        = numberIntChangeable;
        this.partnerProductChangeable   = partnerProductChangeable;
        this.pcatChangeable             = pcatChangeable;
        this.pcatAreaChangeable         = pcatAreaChangeable;
        this.pcatVariantChangeable      = pcatVariantChangeable;
        this.productChangeable          = productChangeable;
        this.productIdChangeable        = productIdChangeable;
        this.quantityChangeable         = quantityChangeable;
//       this.reqDeliveryDateChangeable      = reqDeliveryDateChangeable;
        this.shipToLineChangeable       = shipToLineChangeable;
        this.unitChangeable             = unitChangeable;
//        this.taxValueChangeable         = taxValueChangeable;
    }

    public void setChangeable(boolean flag) {
        configurableChangeable     = flag;
        currencyChangeable         = flag;
//       contractIdChangeable       = flag;
//       reqDeliveryDateChangeable     = flag;
        descriptionChangeable      = flag;
//        freightValueChangeable     = flag;
//        grossValueChangeable       = flag;
//        headerChangeable           = flag;
//        idChangeable               = flag;
//        ipcDocumentIdChangeable    = flag;
//        ipcItemIdChangeable        = flag;
//        ipcProductChangeable       = flag;
//        itmTypeUsageChangeable     = flag;
//        netValueChangeable         = flag;
//        numberIntChangeable        = flag;
        partnerProductChangeable   = flag;
        pcatChangeable             = flag;
        pcatAreaChangeable         = flag;
        pcatVariantChangeable      = flag;
        productChangeable          = flag;
        productIdChangeable        = flag;
        quantityChangeable         = flag;
//        reqDeliveryDateChangeable      = flag;
//        shipToLineChangeable       = flag;
        unitChangeable             = flag;
//        taxValueChangeable         = flag;
    }
*/

    /**
     *
     */
/*    public String getValidTo() {
        return validTo;
    }
    public void setValidTo(String validTo) {
        this.validTo = validTo;
    }
*/
    /**
     *
     */
 /*   public String getReqDeliveryDate() {
        return reqDeliveryDate;
    }
*/
    /**
     *
     */
  /*  public void setReqDeliveryDate(String reqDeliveryDate) {
        this.reqDeliveryDate = reqDeliveryDate;
    }
  */
    /**
     *
     */
  /*  public void setReqDeliveryDateChangeable(boolean reqDeliveryDateChangeable) {
        this.reqDeliveryDateChangeable = reqDeliveryDateChangeable;
    }
  */
    /**
     * Get the confirmed delivery date for the item.
     *
     * @return confirmed delivery date
     */
   /* public String getConfirmedDeliveryDate() {
        return confirmedDeliveryDate;
    }
  */
    /**
     * Set the confirmed delivery date for the item.
     *
     * @param confirmedDeliveryDate the confirmed delivery date
     */
   /* public void setConfirmedDeliveryDate(String confirmedDeliveryDate){
        this.confirmedDeliveryDate = confirmedDeliveryDate;
    }
   */
    /**
     * Get the confirmed quantity for the item.
     *
     * @return confirmed quantity
     */
/*    public String getConfirmedQuantity() {
        return confirmedQuantity;
    }
*/
    /**
     * Set the confirmed quantity for the item.
     *
     * @param confirmedQuantity the confirmed quantity
     */
/*    public void setConfirmedQuantity(String confirmedQuantity) {
        this.confirmedQuantity = confirmedQuantity;
    }
*/
    /**
     * Add a new item delivery detail to the list.
     *
     * @param ItemDelivery which should add to the itemSalesDoc
     *
     * @see ItemDelivery
     * @see ItemDeliveryData
     */


    /**
     * Gets the flag, if the product variant allows only the
     * replacement by another variant, but no changes of
     * the characteristics.
     *
     * @return <code>true</code> the replacement is only permitted by
     *         another variant and no changes of characteristics are allowed,
     *         otherwise <code>false</code>.
     */
/*    public boolean getOnlyVarFind() {
      return onlyVarFind;
    }
*/
    /**
     * Sets the flag, if the product variant allows only the
     * replacement by another variant, but no changes of
     * the characteristics
     */
/*    public void setOnlyVarFind(boolean onlyVarFind) {
      this.onlyVarFind = onlyVarFind;
    }
*/
	/**
	 * Returns the condIdInquired.
	 * @return String
	 */
	public String getCondIdInquired() {
		return condIdInquired;
	}

	/**
	 * Returns the condIdOffered.
	 * @return String
	 */
	public String getCondIdOffered() {
		return condIdOffered;
	}

	/**
	 * Returns the condPriceUnitInquired.
	 * @return String
	 */
	public String getCondPriceUnitInquired() {
		return condPriceUnitInquired;
	}

	/**
	 * Returns the condPriceUnitOffered.
	 * @return String
	 */
	public String getCondPriceUnitOffered() {
		return condPriceUnitOffered;
	}

	/**
	 * Returns the condRateInquired.
	 * @return String
	 */
	public String getCondRateInquired() {
		return condRateInquired;
	}

	/**
	 * Returns the condRateOffered.
	 * @return String
	 */
	public String getCondRateOffered() {
		return condRateOffered;
	}



	/**
	 * Returns the condTypeOffered.
	 * @return String
	 */
	public String getCondTypeOffered() {
		return condTypeOffered;
	}
 
    /**
     * Returns the condTypeInquired.
     * @return String
     */
    public String getCondTypeInquired() {
        return condTypeInquired;
    }
    
    /**
     * Returns the description of the inquired condition.
     * @return String
     */
    public String getCondInquiredDesc() {
       String condDesc = "";
        String condValueType ="";
    
 
       condValues.beforeFirst();
        while ( condValues.next() ) {
                            
            if ( condTypeInquired.equalsIgnoreCase(condValues.getString(CONDITION_TYPE)) ) 
                condDesc = condValues.getString(DESCRIPTION);
        } 

        return condDesc;

    }
	/**
	 * Returns the condUnitInquired.
	 * @return String
	 */
	public String getCondUnitInquired() {
		return condUnitInquired;
	}

	/**
	 * Returns the condUnitOffered.
	 * @return String
	 */
	public String getCondUnitOffered() {
		return condUnitOffered;
	}

	/**
	 * Returns the priceInquired.
	 * @return String
	 */
	public String getPriceInquired() {
        return PriceInquired;
	}

	/**
	 * Returns the priceNormal.
	 * @return String
	 */
	public String getPriceNormal() {
		return PriceNormal;
	}

	/**
	 * Returns the priceOffered.
	 * @return String
	 */
	public String getPriceOffered() {
		return PriceOffered;
	}

	/**
	 * Returns the targetValue.
	 * @return String
	 */
	public String getTargetValue() {
		return targetValue;
	}

	/**
	 * Sets the condIdInquired.
	 * @param condIdInquired The condIdInquired to set
	 */
	public void setCondIdInquired(String condIdInquired) {
		this.condIdInquired = condIdInquired;
	}

	/**
	 * Sets the condIdOffered.
	 * @param condIdOffered The condIdOffered to set
	 */
	public void setCondIdOffered(String condIdOffered) {
		this.condIdOffered = condIdOffered;
	}

	/**
	 * Sets the condPriceUnitInquired.
	 * @param condPriceUnitInquired The condPriceUnitInquired to set
	 */
	public void setCondPriceUnitInquired(String condPriceUnitInquired) {
		this.condPriceUnitInquired = condPriceUnitInquired;
	}

	/**
	 * Sets the condPriceUnitOffered.
	 * @param condPriceUnitOffered The condPriceUnitOffered to set
	 */
	public void setCondPriceUnitOffered(String condPriceUnitOffered) {
		this.condPriceUnitOffered = condPriceUnitOffered;
	}

	/**
	 * Sets the condRateInquired.
	 * @param condRateInquired The condRateInquired to set
	 */
	public void setCondRateInquired(String condRateInquired) {
		this.condRateInquired = condRateInquired;
	}

	/**
	 * Sets the condRateOffered.
	 * @param condRateOffered The condRateOffered to set
	 */
	public void setCondRateOffered(String condRateOffered) {
		this.condRateOffered = condRateOffered;
	}

	/**
	 * Sets the condTypeInquired.
	 * @param condTypeInquired The condTypeInquired to set
	 */
	public void setCondTypeInquired(String condTypeInquired) {
		this.condTypeInquired = condTypeInquired;
	}

	/**
	 * Sets the condTypeInquiredChangeable flag.
	 * @param condTypeInquiredChangeable The condTypeInquiredChangeable to set
	 */
	public void setCondTypeInquiredChangeable(boolean condTypeInquiredChangeable) {
		this.condTypeInquiredChangeable = condTypeInquiredChangeable;
	}
	/**
	 * Sets the condTypeOffered.
	 * @param condTypeOffered The condTypeOffered to set
	 */
	public void setCondTypeOffered(String condTypeOffered) {
		this.condTypeOffered = condTypeOffered;
	}

	/**
	 * Sets the condUnitInquired.
	 * @param condUnitInquired The condUnitInquired to set
	 */
	public void setCondUnitInquired(String condUnitInquired) {
		this.condUnitInquired = condUnitInquired;
	}

	/**
	 * Sets the condUnitOffered.
	 * @param condUnitOffered The condUnitOffered to set
	 */
	public void setCondUnitOffered(String condUnitOffered) {
		this.condUnitOffered = condUnitOffered;
	}

	/**
	 * Sets the priceInquired.
	 * @param priceInquired The priceInquired to set
	 */
	public void setPriceInquired(String priceInquired) {
		PriceInquired = priceInquired;
	}

	/**
	 * Sets the priceNormal.
	 * @param priceNormal The priceNormal to set
	 */
	public void setPriceNormal(String priceNormal) {
		PriceNormal = priceNormal;
	}

	/**
	 * Sets the priceOffered.
	 * @param priceOffered The priceOffered to set
	 */
	public void setPriceOffered(String priceOffered) {
		PriceOffered = priceOffered;
	}

	/**
	 * Sets the targetValue.
	 * @param targetValue The targetValue to set
	 */
	public void setTargetValue(String targetValue) {
		this.targetValue = targetValue;
	}

	/**
	 * Returns the rejectionReason.
	 * @return String
	 */
	public String getRejectionReason() {
		return rejectionReason;
	}

	/**
	 * Sets the rejectionReason.
	 * @param rejectionReason The rejectionReason to set
	 */
	public void setRejectionReason(String rejectionReason) {
		this.rejectionReason = rejectionReason;
	}

	/**
	 * Returns the item type.
	 * @return String
	 */
	public String getItemType() {
		return itemType;
	}

	/**
	 * Sets the item type.
	 * @param itemType The item type to set
	 */
	public void setItemType(String itemType) {
		this.itemType = itemType;
	}

	/**
	 * Returns the condClassInquired.
	 * @return String
	 */
	public String getCondClassInquired() {
		return condClassInquired;
	}
    
    /**
     * Returns the condClassInquired.
     * @return String
     */
    public String getCondClassOffered() {
        return condClassOffered;
    }

	/**
	 * Sets the condClassInquired.
	 * @param condClassInqired The condClassInquired to set
	 */
	public void setCondClassInquired(String condClassInquired) {
		this.condClassInquired = condClassInquired;
	}
    
    /**
     * Sets the condClassOffered.
     * @param  condClassOffered The CondClassOffered to set
     */
    public void setCondClassOffered(String condClassOffered) {
        this.condClassOffered = condClassOffered;
    }

	/**
	 * Returns the condValues.
	 * @return Table
	 */
	public ResultData getCondValues() {
		return condValues;
	}
	
	/**
	 * Returns the value of the condition type (the currency of the item or %).
	 * @param condType The condition type
	 * @return String
	 */
	public String getCondValueType(String condType) {
		
		String condValueType = "";
		        
                          
		if (condValues.first()) {
			if (condValues.searchRowByColumn(ItemNegotiatedContract.CONDITION_TYPE, condType)) {
 				if ( condValues.getString(ItemNegotiatedContract.CONDITION_CLASS).equalsIgnoreCase(ItemNegotiatedContract.CONDITION_CLASS_REBATE )) {
   					condValueType = "%";
   				}
      				else {
        				condValueType = this.getCurrency();
     			}
			}
		}  
		return condValueType;
	} 
	                              
	/**
	 * Sets the condValues.
	 * @param condValues The condValues to set
	 */
	public void setCondValues(ResultData condValues) {
		this.condValues = condValues;
	}

	/**
	 * Returns the pricingUnitPriceInquired.
	 * @return String
	 */
	public String getPricingUnitPriceInquired() {
		return PricingUnitPriceInquired;
	}

	/**
	 * Returns the pricingUnitPriceNormal.
	 * @return String
	 */
	public String getPricingUnitPriceNormal() {
		return PricingUnitPriceNormal;
	}

	/**
	 * Returns the pricingUnitPriceOffered.
	 * @return String
	 */
	public String getPricingUnitPriceOffered() {
		return PricingUnitPriceOffered;
	}

	/**
	 * Returns the unitPriceInquired.
	 * @return String
	 */
	public String getUnitPriceInquired() {
		return UnitPriceInquired;
	}

	/**
	 * Returns the unitPriceNormal.
	 * @return String
	 */
	public String getUnitPriceNormal() {
		return UnitPriceNormal;
	}

	/**
	 * Returns the unitPriceOffered.
	 * @return String
	 */
	public String getUnitPriceOffered() {
		return UnitPriceOffered;
	}

	/**
	 * Sets the pricingUnitPriceInquired.
	 * @param pricingUnitPriceInquired The pricingUnitPriceInquired to set
	 */
	public void setPricingUnitPriceInquired(String pricingUnitPriceInquired) {
		PricingUnitPriceInquired = pricingUnitPriceInquired;
	}

	/**
	 * Sets the pricingUnitPriceNormal.
	 * @param pricingUnitPriceNormal The pricingUnitPriceNormal to set
	 */
	public void setPricingUnitPriceNormal(String pricingUnitPriceNormal) {
		PricingUnitPriceNormal = pricingUnitPriceNormal;
	}

	/**
	 * Sets the pricingUnitPriceOffered.
	 * @param pricingUnitPriceOffered The pricingUnitPriceOffered to set
	 */
	public void setPricingUnitPriceOffered(String pricingUnitPriceOffered) {
		PricingUnitPriceOffered = pricingUnitPriceOffered;
	}

	/**
	 * Sets the unitPriceInquired.
	 * @param unitPriceInquired The unitPriceInquired to set
	 */
	public void setUnitPriceInquired(String unitPriceInquired) {
		UnitPriceInquired = unitPriceInquired;
	}

	/**
	 * Sets the unitPriceNormal.
	 * @param unitPriceNormal The unitPriceNormal to set
	 */
	public void setUnitPriceNormal(String unitPriceNormal) {
		UnitPriceNormal = unitPriceNormal;
	}

	/**
	 * Sets the unitPriceOffered.
	 * @param unitPriceOffered The unitPriceOffered to set
	 */
	public void setUnitPriceOffered(String unitPriceOffered) {
		UnitPriceOffered = unitPriceOffered;
	}

	/**
	 * Returns the condPriceUnitInquiredChangeable.
	 * @return boolean
	 */
	public boolean isCondPriceUnitInquiredChangeable() {
		return condPriceUnitInquiredChangeable;
	}

	/**
	 * Returns the condRateInquiredChangeable.
	 * @return boolean
	 */
	public boolean isCondRateInquiredChangeable() {
		return condRateInquiredChangeable;
	}
	
	/**
	 * Returns the condTypeInquiredChangeable.
	 * @return boolean
	 */
	public boolean isCondTypeInquiredChangeable() {
		return condTypeInquiredChangeable;
	}
	/**
	 * Returns the condUnitInquiredChangeable.
	 * @return boolean
	 */
	public boolean isCondUnitInquiredChangeable() {
		return condUnitInquiredChangeable;
	}

	/**
	 * Sets the condPriceUnitInquiredChangeable.
	 * @param condPriceUnitInquiredChangeable The condPriceUnitInquiredChangeable to set
	 */
	public void setCondPriceUnitInquiredChangeable(boolean condPriceUnitInquiredChangeable) {
		this.condPriceUnitInquiredChangeable = condPriceUnitInquiredChangeable;
	}

	/**
	 * Sets the condRateInquiredChangeable.
	 * @param condRateInquiredChangeable The condRateInquiredChangeable to set
	 */
	public void setCondRateInquiredChangeable(boolean condRateInquiredChangeable) {
		this.condRateInquiredChangeable = condRateInquiredChangeable;
	}

	/**
	 * Sets the condUnitInquiredChangeable.
	 * @param condUnitInquiredChangeable The condUnitInquiredChangeable to set
	 */
	public void setCondUnitInquiredChangeable(boolean condUnitInquiredChangeable) {
		this.condUnitInquiredChangeable = condUnitInquiredChangeable;
	}

}
