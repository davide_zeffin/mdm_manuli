/*****************************************************************************
    Class:        ItemBillingDoc
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author        Ralf Witt
    Created:      May 2001

    $Revision: #2 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.businessobject.item;

import com.sap.isa.backend.boi.isacore.billing.ItemBillingData;
import com.sap.isa.core.TechKey;

/**
 * Extends the sales document and adds all fields relevant for the billing
 * process.
 *
 * @author Ralf Witt
 * @version 1.0
 */
public class ItemBillingDoc extends ItemSalesDoc
                            implements ItemBillingData {

    private String salesRefDocNo;
    private TechKey salesRefDocTechKey;
    private String salesRefDocPosNo;
    private TechKey salesRefDocPosTechKey;
    private String billingRefDocNo;
    private String billingRefDocPosNo;
    private String taxJurisdictionCode;
    private boolean claimable = true;

    /**
     * Default constructor for the Item
     */
    public ItemBillingDoc() {
    }

    /**
     * Constructor for the object directly taking the technical
     * key for the object
     *
     * @param techKey Technical key for the object
     */
    public ItemBillingDoc(TechKey techKey) {
        super(techKey);
    }

    /**
     * Reference to the sales document
     */
     public void setSalesRefDocNo(String ref) {
         this.salesRefDocNo = ref;
     }

    /**
     * Reference to the sales document TechKey
     */
     public void setSalesRefDocTechKey(TechKey techKey) {
         this.salesRefDocTechKey = techKey;
     }

    /**
     * Reference to the sales document position
     */
     public void setSalesRefDocPosNo(String ref) {
         this.salesRefDocPosNo = ref;
     }

    /**
     * Reference to the sales document position TechKey
     */
     public void setSalesRefDocPosTechKey(TechKey techKey) {
         this.salesRefDocPosTechKey = techKey;
     }

    /**
     * Reference to the billing document
     */

     public void setBillingRefDocNo(String ref) {
         this.billingRefDocNo = ref;
     }

    /**
     * Reference to the billing document position
     */
     public void setBillingRefDocPosNo(String ref) {
         this.billingRefDocPosNo = ref;
     }

    /**
     * Tax jurisdiction Code
     */
     public void setTaxJurisdictionCode(String tjc) {
         this.taxJurisdictionCode = tjc;
     }

     /**
      * Tax jurisdiction Code
      */
      public String getTaxJurisdictionCode() {
          return taxJurisdictionCode;
      }

     /**
      * Reference to the sales document the billing document results from
      */
      public String getSalesRefDocNo() {
          return salesRefDocNo;
      }
     /**
      * Reference to the sales document position the billing document position results from
      */
      public String getSalesRefDocPosNo() {
          return salesRefDocPosNo;
      }
     /**
      * Reference to the sales document TechKey the billing document results from
      */
      public TechKey getSalesRefDocTechKey() {
          return salesRefDocTechKey;
      }
     /**
      * Reference to the sales document position TechKey the billing document position results from
      */
      public TechKey getSalesRefDocPosTechKey() {
          return salesRefDocPosTechKey;
      }
     /**
      * Reference to the billing document the billing document results from (i.e. Cancellation)
      */
      public String getBillingRefDocNo() {
          return billingRefDocNo;
      }
     /**
      * Reference to the billing document position the billing document position results from
      */
      public String getBillingRefDocPosNo() {
          return billingRefDocPosNo;
      }
      /**
       * Set this item to be not claimable 
       */
      public void setNotClaimable() {
          this.claimable = false;
      }
      /**
       * Returns true if the item is claimable
       * @return boolean 
       */
      public boolean isClaimable() {
          return claimable;
      }
}
