
package com.sap.isa.businessobject.item;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.sap.isa.backend.boi.isacore.order.ItemConditionRecordData;
import com.sap.isa.backend.boi.isacore.order.ItemConditionTableData;


public class ItemConditionTable implements ItemConditionTableData, Cloneable {

	private ItemConditionRecord conditionRecord;
	
	// List of condition record
	protected ArrayList conditionRecords;

	/**
	 * Creates a new <code>Condition table</code> object.
	 */
	public ItemConditionTable() {
		conditionRecords = new ArrayList();
	}
	/**
	 * Creates an empty <code>ItemConditionRecordData</code> for the basket.
	 *
	 * @returns ItemConditionRecordData which can added to the conditionTable
	 */
	public ItemConditionRecordData createItemConditionRecord(){
		return new ItemConditionRecord();
	}

	/**
	 * Creates a initialized <code>ItemConditionRecordData</code> for the basket.
	 *
	 * @param condition type 
	 * @returns ItemConditionRecordData which can added to the condition table
	 */
	public ItemConditionRecordData createConditionRecordData(String conditionType){
		return new ItemConditionRecord(conditionType);
		
	}

	/**
	 * Get the condition record for the given condition type
	 *
	 * @param conditionType String the condition type
	 * @return ItemConditionRecordData of the condition table, with the given condition type or null if not found
	 */
	public ItemConditionRecordData getConditionRecordData(String conditionType){
		return null;
	}

	/**
	 * remove the condition record for the given condition type
	 *
	 * @param condition type String the condition type
	 */
	public void removeConditionRecordData(String conditionType){
	}

	
	/**
	 * clear the condition list
	 */
	public void clear(){
		this.conditionRecords.clear();
	}
	
	/**
	 * get the size of the list of conditions
	 */
	public int size(){
		return this.conditionRecords.size();
	}
	
	/**
	 * returns true if the conditions list is empty
	 * 
	 * @return boolean
	 */
	public boolean isEmpty(){
		return this.conditionRecords.isEmpty();
	}
	
	/**
	 * get the condition list
	 *
	 * @return List list of conditions
	 */
	public List getList(){
		return this.conditionRecords;
	}
	
	/**
	 * set the condition list
	 *
	 * @param  List new list of condition records
	 */
	public void setList(List conditionTableData){
		this.conditionRecords = new ArrayList(conditionTableData);
	}
	
	/**
	 * Performs a deep copy of this object.
	 * Because of the fact that
	 * all fields of this object consist of immutable objects like
	 * <code>String</code> and <code>TechKey</code> or primitive types
	 * the shallow copy is identical with a deep copy. For the
	 * <code>payment</code> property an explicit copy operation is performed
	 * because this object is the only mutable one.
	 *
	 * @return shallow (deep-like) copy of this object
	 */
	public Object clone(){
		ItemConditionTable myClone = new ItemConditionTable();
		myClone.setList((ArrayList)this.conditionRecords.clone());
		return myClone;
	}
	
	/**
	 * Returns en iterator over the Entrys of the condition record list in
	 * form of Map.
	 *
	 * @return iterator over condition records
	 *
	 */
	public Iterator iterator(){
		return this.conditionRecords.iterator();
	}
	
}
