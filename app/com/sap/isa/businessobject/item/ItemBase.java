    /*****************************************************************************
    Class:        ItemBase
    Copyright (c) 2001, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      11.4.2001
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $
*****************************************************************************/
package com.sap.isa.businessobject.item;

import com.sap.isa.backend.boi.isacore.*;
import com.sap.isa.backend.boi.isacore.order.*;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.businessobject.order.*;

import com.sap.isa.businessobject.*;

import java.util.*;

/**
 * Item of a sales document. Each sales document consists of a header and a
 * number of items. This items are represented by this class.
 *
 * @author  SAP AG
 * @version 1.0
 */
public class ItemBase
	extends BusinessObjectBase
	implements ItemBaseData, Cloneable {

	final static protected IsaLocation log =
		IsaLocation.getInstance(ItemBase.class.getName());

	protected String currency;
	protected boolean configurable;
	protected String configType;
	protected String statisticPrice;
	protected String description;
	protected String deliveredQuantity;
	protected Object externalItem; // must cast to IPCItem for usage
	protected String itmTypeUsage;
	protected String itmUsage;
	protected String businessObjectType;
	protected String numberInt;
	protected TechKey pcat;
	protected String pcatArea;
	protected String pcatVariant;
	protected String product;
	protected TechKey productId;
	// protected String systemProductId;
	// protected String substitutionReasonId;
	protected String quantity;
	protected String freeQuantity = "";
	protected String oldQuantity = ""; // for business event modify document
	protected String cumulQuantity;
	protected Text text;
	protected Text textHistory;
	protected String unit;
	protected String netPriceUnit;
	protected String netQuantPriceUnit;
	protected String cumulQuantityUnit;
	protected TechKey parentId;
	protected String partnerProduct;
	protected String taxValue;
	protected String netValue;
	protected String netValueWOFreight;
	protected String freightValue;
	protected String grossValue;
	protected String netPrice;
	protected String reqDeliveryDate;
	protected String confirmedDeliveryDate;
	protected String confirmedQuantity;
	protected String validTo;
	protected String status;
	protected ExtendedStatus extendedStatus;
	protected String[] possibleUnits;
	protected String quantityToDeliver;
	protected ArrayList scheduleLines = new ArrayList();
	protected PartnerList partnerList = new PartnerList();
	protected String extNumberInt; // external row number reference
	protected String deliveryPriority;
	protected String usageScope;	

	protected boolean pcatChangeable;
	protected boolean pcatAreaChangeable;
	protected boolean pcatVariantChangeable;
	protected boolean reqDeliveryDateChangeable;
	protected boolean currencyChangeable;
	protected boolean descriptionChangeable;
	protected boolean extendedStatusChangeable;
	protected boolean externalItemChangeable;
	protected boolean partnerProductChangeable;
	protected boolean productChangeable;
	protected boolean productIdChangeable;
	protected boolean quantityChangeable;
	protected boolean unitChangeable;
	protected boolean poNumberUCChangeable;
	protected boolean deletable;
	protected boolean cancelable;
	protected boolean configurableChangeable;
	protected boolean onlyVarFind;
	protected boolean isAvailableInResellerCatalog;
	protected int isAvailableInPartnerCatalog;
	protected boolean isDataSetExternally;
	protected boolean isExtNumberIntChangeable;

	protected boolean isATPRelevant = true;
	protected boolean isPriceRelevant = true;

	protected boolean deliveryPriorityChangeable;

	// predecessor and sucessor
	private ConnectedDocumentItemData predecessor;
	private ConnectedDocumentItemData sucessor;

	//external references (to external documents)   
	protected ExternalReferenceList assignedExternalReferences =
		new ExternalReferenceList();
	protected boolean assignedExternalReferencesChangeable;

	//external reference objects
	protected String extRefObjectType;
	protected ExtRefObjectList assignedExtRefObjects = new ExtRefObjectList();
	protected boolean assignedExtRefObjectsChangeable;

	protected ArrayList attachmentList = new ArrayList();

	// technical Data
	protected TechnicalPropertyGroup assignedTechnicalProperties =
		new TechnicalPropertyGroup();
		
	//packages, solution configuration data
	protected boolean isPackageExploded;
	protected boolean isMainProductFromPackage;
	protected boolean isScSelected;
	protected String scGroup;
	protected String scAuthor;
	protected String scScore;
	protected TechKey scDocumentGuid;
	protected boolean scAlternativeProductAvailable;
	protected String productRole;
    
    protected String recurringNetValue;
    protected String recurringTaxValue;
    protected String recurringGrossValue;
    protected String recurringDuration;
    protected String recurringTimeUnit;
    protected String recurringTimeUnitAbr;
    protected String recurringTimeUnitPlural;
    
    /** Display of contract duration in basket */
    protected String contractDuration;
    protected String contractDurationUnit;
    protected String contractDurationUnitText;
    protected String contractDurationUnitTextPlural;
    protected String contractDurationUnitAbbr;
    
   /** Doc Flow for the document items
    *  since doc flow harmonization for CRM 5.2
    */
	protected ArrayList predecessorList = new ArrayList();
	protected ArrayList successorList = new ArrayList();

	protected TechKey auctionGuid;
	
	/**
	 * Check if the given itemBaseData object is an instance of ItemBase. 
	 * 
	 * @param itemBaseData item as ItemBaseData object
	 * @return ItemSalesDoc casted ItemBaseData object as ItemBase
	 */
	static public ItemBase getItemBase(ItemBaseData itemBaseData) {

		ItemBase item = null;
		if (itemBaseData instanceof ItemBase) {
			item = (ItemBase) itemBaseData;
		} else {
			log.debug(
				"not allowed instance for ItemBase object: "
					+ itemBaseData.getClass().getName());
		}

		return item;
	}

	/**
	 * Check if the given itemBase object is an instance of ItemSalesDoc. 
	 * 
	 * @param itemBase item as ItemBase object
	 * @return true if instance of ItemSalesDoc
	 */
	static public boolean isItemBase(ItemBaseData itemBaseData) {

		boolean isItemBase = false;
		if (itemBaseData instanceof ItemBase) {
			isItemBase = true;
		} else {
			log.debug(
				"not allowed instance for ItemBase object: "
					+ itemBaseData.getClass().getName());
		}

		return isItemBase;
	}

	/**
	 * Default constructor for the Item
	 */
	public ItemBase() {
		createUniqueHandle();
	}

	/**
	 * Constructor for the object directly taking the technical
	 * key for the object
	 * if the technical key is empty an unique handle will be generate
	 *
	 * @param techKey Technical key for the object
	 */
	public ItemBase(TechKey techKey) {
		super(techKey);
		if (techKey == null
			|| techKey.isInitial()
			|| techKey.getIdAsString().length() < 1) {
			// create a handle for the new item!
			createUniqueHandle();
		}
	}

	/**
	 * Creates a text object. This method is used by backend objects to
	 * get instances of the text object.
	 *
	 * @return newly created text object
	 */
	public TextData createText() {
		return (TextData) new Text();
	}

	/**
	 * Set an array of all possible units for this item.
	 *
	 * @param possibleUnits the units that can be choosen for this item
	 */
	public void setPossibleUnits(String[] possibleUnits) {
		this.possibleUnits = possibleUnits;
	}

	/**
	 * Get the array of all possible units that can be used for this item.
	 *
	 * @return list of all possible units
	 */
	public String[] getPossibleUnits() {
		return possibleUnits;
	}

	/**
	 * Get the internal number for the item. The internal number is the
	 * item number (Positionsnummer) displayed on the front end and generated
	 * by the backend system. In the default case the numbers are starting
	 * with 10 and increased by 10, e.g. 10, 20, 30, 40....
	 *
	 * @return the item number
	 */
	public String getNumberInt() {
		return numberInt;
	}

	/**
	 * Set the internal number for the item. The internal number is the
	 * item number (Positionsnummer) displayed on the front end and generated
	 * by the backend system. In the default case the numbers are starting
	 * with 10 and increased by 10, e.g. 10, 20, 30, 40....
	 *
	 * @param numberInt the item number to be set for this item
	 */
	public void setNumberInt(String numberInt) {
		this.numberInt = numberInt;
	}

	/**
	 * Get descriptions on the item level.
	 *
	 * @return description
	 */
	public String getDescription() {
        String descr = description;
        if (descr == null || descr.length() == 0) {
            descr = getProduct();
	}
		return descr;
	}

	/**
	 * Determine, wheter or no the item is configurable.
	 *
	 * @return <code>true</code> if the item can be configured, otherwise
	 *         <code>false</code>.
	 */
	public boolean isConfigurable() {
		return configurable;
	}

	/**
	 *
	 */
	public Object getExternalItem() {
		return externalItem;
	}

	/**
	 *
	 */
	public void setItmTypeUsage(String itmTypeUsage) {
		this.itmTypeUsage = itmTypeUsage;
	}

	/**
	 *
	 */
	public String getItmTypeUsage() {
		return itmTypeUsage;
	}

	/**
	 *
	 */
	public void setItmUsage(String itmUsage) {
		this.itmUsage = itmUsage;
	}

	/**
	 *
	 */
	public String getItmUsage() {
		return itmUsage;
	}

	/**
	 * Get the technical key specifying the catalog used to select the item
	 * and put it into the basket.
	 *
	 * @return technical key of catalog
	 */
	public TechKey getPcat() {
		return pcat;
	}

	/**
	 * Get the catalog area used to select the item
	 * and put it into the basket.
	 *
	 * @return catalog area
	 */
	public String getPcatArea() {
		return pcatArea;
	}

	/**
	 * Get the catalog variant used to select the item
	 * and put it into the basket.
	 *
	 * @return catalog variant
	 */
	public String getPcatVariant() {
		return pcatVariant;
	}

	/**
	 * Get the product for the item. The product is the short number, not
	 * the technical key, for the item, e.g. <code>tt02</code>. The value
	 * returned by this method may not be unique. Use
	 * <code>getProductId()</code> instead.
	 *
	 * @return the product
	 */
	public String getProduct() {
		return product;
	}

	/**
	 * Get the technical key for the product of this item.
	 *
	 * @return technical key for the product
	 */
	public TechKey getProductId() {
		return productId;
	}

	/**
	 * Set the property oldQuantity
	 *
	 * @param oldQuantity
	 */
	public void setOldQuantity(String oldQuantity) {
		this.oldQuantity = oldQuantity;
	}

	/**
	 * Returns the property oldQuantity
	 *
	 * @return oldQuantity
	 */
	public String getOldQuantity() {
		return this.oldQuantity;
	}

	/**
	 * Get the quantity for this item.
	 *
	 * @return quantity
	 */
	public String getQuantity() {
		return quantity;
	}

	/**
	 * Get the delivered quantity for this item.
	 *
	 * @return delivered quantity
	 */
	public String getDeliveredQuantity() {
		return deliveredQuantity;
	}

	/**
	 *
	 */
	public String getCumulQuantity() {
		return cumulQuantity;
	}

	/**
	 * Get additional text for this item.
	 *
	 * @return text for this item
	 */
	public TextData getText() {
		return (TextData) text;
	}

	/**
	 * Check, if there is a item text.
	 * 
	 * @return true, if there is a text
	 *         false, otherwise
	 */
	public boolean hasText() {
		
		if (this.text == null)           
		    return false;
		    
		if (this.text.getText() == null) 
		    return false;
		
		return getText().getText().length() > 0;
	}
	
	/**
	 * Get text History for this item.
	 *
	 * @return text history for this item
	 */
	public TextData getTextHistory() {
		return (TextData) textHistory;
	}

	/**
	 * Get the unit, the quantity is measured in.
	 *
	 * @return the unit
	 */
	public String getUnit() {
		return unit;
	}

	/**
	 * Get the condition unit ,
	 *
	 * @return the condition unit
	 */
	public String getNetPriceUnit() {
		return netPriceUnit;
	}

	/**
	 * Get the condition pricing unit,
	 *
	 * @return the condition pricing unit
	 */
	public String getNetQuantPriceUnit() {
		return netQuantPriceUnit;
	}

	/**
	 * Get the unit, the cumulative quantity is measured in.
	 *
	 * @return the unit
	 */
	public String getCumulQuantityUnit() {
		return cumulQuantityUnit;
	}

	/**
	 *
	 */
	public TechKey getParentId() {
		return parentId;
	}

	/**
	 *
	 */
	public String getPartnerProduct() {
		return partnerProduct;
	}

	/**
	 * Get the business partner list
	 *
	 * @return PartnerList list of business partners
	 */
	public PartnerList getPartnerList() {
		return partnerList;
	}

	/**
	 * Set the business partner list
	 *
	 * @param PartnerList new list of business partners
	 */
	public void setPartnerList(PartnerList partnerList) {
		this.partnerList = partnerList;
	}

	/**
	 * Get the business partner list
	 *
	 * @return PartnerListData list of business partners
	 */
	public PartnerListData getPartnerListData() {
		return partnerList;
	}

	/**
	 *
	 */
	public String getTaxValue() {
		return taxValue;
	}

	/**
	 *
	 */
	public void setTaxValue(String taxValue) {
		this.taxValue = taxValue;
	}

	/**
	 *
	 */
	public void setNetValue(String netValue) {
		this.netValue = netValue;
	}

	/**
	 *
	 */
	public String getNetValue() {
		return netValue;
	}

	/**
	 *
	 */
	public void setNetValueWOFreight(String netValueWOFreight) {
		this.netValueWOFreight = netValueWOFreight;
	}

	/**
	 *
	 */
	public String getNetValueWOFreight() {
		return netValueWOFreight;
	}

	/**
	 *
	 */
	public void setNetPrice(String netPrice) {
		this.netPrice = netPrice;
	}

	/**
	 *
	 */
	public String getNetPrice() {
		return netPrice;
	}

	/**
	 *
	 */
	public void setFreightValue(String freightValue) {
		this.freightValue = freightValue;
	}

	/**
	 *
	 */
	public String getFreightValue() {
		return freightValue;
	}

	/**
	 *
	 */
	public void setGrossValue(String grossValue) {
		this.grossValue = grossValue;
	}

	/**
	 *
	 */
	public String getGrossValue() {
		return grossValue;
	}

	/**
	 * Sets the currency for the item.
	 *
	 * @param currency currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * Gets the currency for the item.
	 *
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 *
	 */
	public void setConfigurable(boolean configurable) {
		this.configurable = configurable;
	}

	public void setConfigurableChangeable(boolean configurableChangeable) {
		this.configurableChangeable = configurableChangeable;
	}

	/**
	 *
	 */
	public void setDeletable(boolean deletable) {
		this.deletable = deletable;
	}
	/**
	 *
	 */
	public void setCancelable(boolean cancelable) {
		this.cancelable = cancelable;
	}
	/**
	 *
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 *
	 */
	public void setExternalItem(Object externalItem) {
		this.externalItem = externalItem;
	}

	/**
	 *
	 */
	public void setParentId(TechKey parentId) {
		this.parentId = parentId;
	}

	/**
	 *
	 */
	public void setPartnerProduct(String partnerProduct) {
		this.partnerProduct = partnerProduct;
	}

	/**
	 *
	 */
	public void setProduct(String product) {
		this.product = product;
	}

	/**
	 *
	 */
	public void setProductId(TechKey productId) {
		this.productId = productId;
	}

	/**
	 *
	 */
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	/**
	 *
	 */
	public void setCumulQuantity(String cumulQuantity) {
		this.cumulQuantity = cumulQuantity;
	}

	/**
	 *
	 */
	public void setText(Text text) {
		this.text = text;
	}

	/**
	 *
	 */
	public void setText(TextData text) {
		this.text = (Text) text;
	}
	/**
	 *
	 */
	public void setTextHistory(TextData text) {
		this.textHistory = (Text) text;
	}
	/**
	 *
	 */
	public void setUnit(String unit) {
		this.unit = unit;
	}

	/**
	 *
	 */
	public void setNetPriceUnit(String netPriceUnit) {
		this.netPriceUnit = netPriceUnit;
	}

	/**
	 *
	 */
	public void setNetQuantPriceUnit(String netQuantPriceUnit) {
		this.netQuantPriceUnit = netQuantPriceUnit;
	}

	/**
	 *
	 */
	public void setCumulQuantityUnit(String cumulQuantityUnit) {
		this.cumulQuantityUnit = cumulQuantityUnit;
	}

	/**
	 *
	 */
	public void setUnitChangeable(boolean unitChangeable) {
		this.unitChangeable = unitChangeable;
	}

	/**
	 *
	 */
	public void setQuantityChangeable(boolean quantityChangeable) {
		this.quantityChangeable = quantityChangeable;
	}

	public void setProductChangeable(boolean productChangeable) {
		this.productChangeable = productChangeable;
	}

	public void setPartnerProductChangeable(boolean changePartnerProduct) {
		this.partnerProductChangeable = changePartnerProduct;
	}

	public void setPoNumberUCChangeable(boolean poNumberUCChangeable) {
		this.poNumberUCChangeable = poNumberUCChangeable;
	}

	/**
	 *
	 */
	public void setDeliverdQuantity(String deliveredQuantity) {
		this.deliveredQuantity = deliveredQuantity;
	}

	/**
	 *
	 */
	public void setPcat(TechKey pcat) {
		this.pcat = pcat;
	}

	/**
	 * Set extended Status object
	 */
	public void setExtendedStatus(ExtendedStatusData status) {
		this.extendedStatus = (ExtendedStatus) status;
	}
	/**
	 * Get an extended Status object
	 */
	public ExtendedStatus getExtendedStatus() {
		return this.extendedStatus;
	}
	/**
	 * Get an extended Status Data object
	 */
	public ExtendedStatusData getExtendedStatusData() {
		return ((ExtendedStatusData) this.extendedStatus);
	}
	/**
	 * Indicates whether the product of the item is available in the catalog of
	 * a partner, if a partner is assigned to the icon
	 *
	 * @return <code>AVAILABLE_AT_PARTNER</code> if the product is available in the catalog of the partner,
	 *         <code>NOT_AVAILABLE_AT_PARTNER</code> if the product is not available in the catalog of the partner,
	 *         <code>AVAILABILITY_UNKNOWN_AT_PARTNER</code> if the availability of product in the catalog of the partner is unknown.
	 */
	public int isAvailableInPartnerCatalog() {
		return isAvailableInPartnerCatalog;
	}

	/**
	 * Sets flag that indicates, if he product of the item is available in the catalog of
	 * a partner, if a partner is assigned to the icon
	 *
	 * @param <code>AVAILABLE_AT_PARTNER</code> if the product is available in the catalog of the partner,
	 *        <code>NOT_AVAILABLE_AT_PARTNER</code> if the product is not available in the catalog of the partner,
	 *        <code>AVAILABILITY_UNKNOWN_AT_PARTNER</code> if the availability of product in the catalog of the partner is unknown.
	 */
	public void setAvailableInPartnerCatalog(int isAvailableInPartnerCatalog) {
		this.isAvailableInPartnerCatalog = isAvailableInPartnerCatalog;
	}

	/**
	 *
	 */
	public boolean isCurrencyChangeable() {
		return currencyChangeable;
	}

	/**
	 *
	 */
	public boolean isPoNumberUCChangeable() {
		return poNumberUCChangeable;
	}

	/**
	 *
	 */
	public boolean isConfigurableChangeable() {
		return configurableChangeable;
	}

	/**
	 *
	 */
	public boolean isDescriptionChangeable() {
		return descriptionChangeable;
	}

	/**
	 *
	 */
	public boolean isExternalItemChangeable() {
		return externalItemChangeable;
	}

	/**
	 *
	 */
	public boolean isPartnerProductChangeable() {
		return partnerProductChangeable;
	}

	/**
	 *
	 */
	public boolean isProductChangeable() {
		return productChangeable;
	}

	/**
	 *
	 */
	public boolean isProductIdChangeable() {
		return productIdChangeable;
	}

	/**
	 *
	 */
	public boolean isQuantityChangeable() {
		return quantityChangeable;
	}

	/**
	 *
	 */
	public boolean isUnitChangeable() {
		return unitChangeable;
	}
	/**
	 *
	 */
	public boolean isDeletable() {
		return deletable;
	}
	/**
	 *
	 */
	public boolean isCancelable() {
		return cancelable;
	}

	/**
	 *
	 */
	public boolean isReqDeliveryDateChangeable() {
		return reqDeliveryDateChangeable;
	}

	/**
	 *
	 */
	public ArrayList getScheduleLines() {
		return this.scheduleLines;
	}

	/**
	 *
	 */
	public void setScheduleLines(ArrayList scheduleLines) {
		this.scheduleLines = scheduleLines;
	}

	/**
	 * Sets the status of the item to the given status
	 *
	 * @param status status value to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 *
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Set item status status to {@link #DOCUMENT_COMPLETION_STATUS_OPEN} 
	 */
	public void setStatusOpen() {
		this.status = DOCUMENT_COMPLETION_STATUS_OPEN;
	}

	/**
	 * Set item status status to {@link #DOCUMENT_COMPLETION_STATUS_IN_PROCESS} 
	 */
	public void setStatusInProcess() {
		this.status = DOCUMENT_COMPLETION_STATUS_IN_PROCESS;
	}

	/**
	 * Set item status status to {@link #DOCUMENT_COMPLETION_STATUS_COMPLETED} 
	 */
	public void setStatusCompleted() {
		this.status = DOCUMENT_COMPLETION_STATUS_COMPLETED;
	}

	/**
	 * Set item status status to {@link #DOCUMENT_COMPLETION_STATUS_CANCELLED} 
	 */
	public void setStatusCancelled() {
		this.status = DOCUMENT_COMPLETION_STATUS_CANCELLED;
	}

	/**
	 * Set item status status to {@link #DOCUMENT_COMPLETION_STATUS_REJECTED} 
	 */
	public void setStatusRejected() {
		this.status = DOCUMENT_COMPLETION_STATUS_REJECTED;
	}
	/**
	 * Returns <code>true</code> if item status is OPEN
	 */
	public boolean isStatusOpen() {
		return status == DOCUMENT_COMPLETION_STATUS_OPEN;
	}
	/**
	 * Returns <code>true</code> if item status is COMPLETED
	 */
	public boolean isStatusCompleted() {
		return status == DOCUMENT_COMPLETION_STATUS_COMPLETED;
	}
	/**
	 * Returns <code>true</code> if item status is CANCELLED
	 */
	public boolean isStatusCancelled() {
		return status == DOCUMENT_COMPLETION_STATUS_CANCELLED;
	}
	/**
	 * Returns <code>true</code> if item status is IN_PROCESS
	 */
	public boolean isStatusInProcess() {
		return status == DOCUMENT_COMPLETION_STATUS_IN_PROCESS;
	}
	/**
	 * Returns <code>true</code> if item status is REJECTED
	 */
	public boolean isStatusRejected() {
		return status == DOCUMENT_COMPLETION_STATUS_REJECTED;
	}

	/** 
	 * Sets item catalog area 
	 * 
	 * @param String the catalog area
	 */
	public void setPcatArea(String pcatArea) {
		this.pcatArea = pcatArea;
	}

	/** 
	 * Sets item catalog variant
	 * 
	 * @param String the catalog variant
	 */
	public void setPcatVariant(String pcatVariant) {
		this.pcatVariant = pcatVariant;
	}

	/**
	 *
	 */
	public boolean isPcatChangeable() {
		return pcatChangeable;
	}

	/**
	 *
	 */
	public boolean isPcatAreaChangeable() {
		return pcatAreaChangeable;
	}

	/**
	 *
	 */
	public boolean isPcatVariantChangeable() {
		return pcatVariantChangeable;
	}

	/**
	 * Set Item's Business Object Type
	 * 
	 * @param String the business object type of the item
	 */
	public void setBusinessObjectType(String busType) {
		this.businessObjectType = busType;
	}

	/**
	 * Returns a String representation of the item
	 */
	public String toString() {
		return "ItemBase ["
			+ "techkey=\""
			+ techKey
			+ "\""
			+ ", currency=\""
			+ currency
			+ "\""
			+ ", description=\""
			+ description
			+ "\""
			+ ", configurable=\""
			+ configurable
			+ "\""
			+ ", itmTypeUsage=\""
			+ itmTypeUsage
			+ "\""
			+ ", busObjType=\""
			+ businessObjectType
			+ "\""
			+ ", numberInt=\""
			+ numberInt
			+ "\""
			+ ", pcat=\""
			+ pcat
			+ "\""
			+ ", pcatArea=\""
			+ pcatArea
			+ "\""
			+ ", pcatVariant=\""
			+ pcatVariant
			+ "\""
			+ ", product=\""
			+ product
			+ "\""
			+ ", productId=\""
			+ productId
			+ "\""
			+ ", quantity=\""
			+ quantity
			+ "\""
			+ ", oldQuantity=\""
			+ oldQuantity
			+ "\""
			+ ", text=\""
			+ text
			+ "\""
			+ ", unit=\""
			+ unit
			+ "\""
			+ ", partnerProduct=\""
			+ partnerProduct
			+ "\""
			+ ", taxValue=\""
			+ taxValue
			+ "\""
			+ ", netValue=\""
			+ netValue
			+ "\""
			+ ", netValueWOFreight=\""
			+ netValueWOFreight
			+ "\""
			+ ", freightValue=\""
			+ freightValue
			+ "\""
			+ ", grossValue=\""
			+ grossValue
			+ "\""
			+ ", reqDeliveryDate=\""
			+ reqDeliveryDate
			+ "\""
			+ ", confirmedDeliveryDate=\""
			+ confirmedDeliveryDate
			+ "\""
			+ ", confirmedQuantity=\""
			+ confirmedQuantity
			+ "\""
			+ ", validTo=\""
			+ validTo
			+ "\""
			+ ", status=\""
			+ status
			+ "\""
			+ ", possibleUnits=\""
			+ possibleUnits
			+ "\""
			+ ", partnerList=\""
			+ partnerList.toString()
			+ "\""
			+ ", pcatChangeable=\""
			+ pcatChangeable
			+ "\""
			+ ", pcatAreaChangeable=\""
			+ pcatAreaChangeable
			+ "\""
			+ ", pcatVariantChangeable=\""
			+ pcatVariantChangeable
			+ "\""
			+ ", reqDeliveryDateChangeable=\""
			+ reqDeliveryDateChangeable
			+ "\""
			+ ", currencyChangeable=\""
			+ currencyChangeable
			+ "\""
			+ ", descriptionChangeable=\""
			+ descriptionChangeable
			+ "\""
			+ ", partnerProductChangeable=\""
			+ partnerProductChangeable
			+ "\""
			+ ", productChangeable=\""
			+ productChangeable
			+ "\""
			+ ", productIdChangeable=\""
			+ productIdChangeable
			+ "\""
			+ ", quantityChangeable=\""
			+ quantityChangeable
			+ "\""
			+ ", unitChangeable=\""
			+ unitChangeable
			+ "\""
			+ ", isAvailableInPartnerCatalog=\""
			+ isAvailableInPartnerCatalog
			+ "\""
			+ ", text=\""
			+ text
			+ "\""
			+ ", messages=\""
			+ getMessageList()
			+ "\""
			+ ", isPriceRelevant=\""
			+ isPriceRelevant
			+ "\""
			+ ", isATPRelevant=\""
			+ isATPRelevant
			+ "\""
			+ ", configurableChangeable=\""
			+ configurableChangeable
			+ "\""
			+ ", deliverPriority=\""
			+ deliveryPriority
			+ "\""
			+ ", deliveryPriorityChangeable\""
			+ deliveryPriorityChangeable
			+ "\""
            + ", recurringNetValue=\""    + recurringNetValue             + "\""
            + ", recurringTaxValue=\""    + recurringTaxValue             + "\""
            + ", recurringGrossValue=\""   + recurringGrossValue           + "\""
            + ", recurringDuration=\""     + recurringDuration             + "\""
            + ", recurringTimeUnit=\""    + recurringTimeUnit             + "\""
            + ", contractDuration=\""    + contractDuration             + "\""
            + ", contractTimeUnit=\""               + contractDurationUnit             + "\""
            + ", contractTimeUnitPlural=\""     + contractDurationUnitTextPlural           + "\""
            + ", contractTimeUnitAbbr=\""     + contractDurationUnitAbbr             + "\""
		    + ", auctionGuid=\""     + auctionGuid             + "\""
			+ "]";
	}

	/**
	 * Returns the valid to date of the item
	 * 
	 * @return valid to date
	 */
	public String getValidTo() {
		return validTo;
	}

	/**
	 * Sets the validd to date of the item
	 */
	public void setValidTo(String validTo) {
		this.validTo = validTo;
	}

	/**
	 * Returns the requested delivery date of the item
	 * 
	 * @return requested delivery date
	 */
	public String getReqDeliveryDate() {
		return reqDeliveryDate;
	}

	/**
	 * Sets the requested delivery date of the item
	 */
	public void setReqDeliveryDate(String reqDeliveryDate) {
		this.reqDeliveryDate = reqDeliveryDate;
	}

	/**
	 *
	 */
	public void setReqDeliveryDateChangeable(boolean reqDeliveryDateChangeable) {
		this.reqDeliveryDateChangeable = reqDeliveryDateChangeable;
	}

	/**
	 * Get the confirmed delivery date for the item.
	 *
	 * @return confirmed delivery date
	 */
	public String getConfirmedDeliveryDate() {
		return confirmedDeliveryDate;
	}

	/**
	 * Set the confirmed delivery date for the item.
	 *
	 * @param confirmedDeliveryDate the confirmed delivery date
	 */
	public void setConfirmedDeliveryDate(String confirmedDeliveryDate) {
		this.confirmedDeliveryDate = confirmedDeliveryDate;
	}

	/**
	 * Get the confirmed quantity for the item.
	 *
	 * @return confirmed quantity
	 */
	public String getConfirmedQuantity() {
		return confirmedQuantity;
	}

	/**
	 * Set the confirmed quantity for the item.
	 *
	 * @param confirmedQuantity the confirmed quantity
	 */
	public void setConfirmedQuantity(String confirmedQuantity) {
		this.confirmedQuantity = confirmedQuantity;
	}

	/**
	 * Create a new ScheduleLine object.
	 *
	 * @see SchedlineData
	 */
	public SchedlineData createScheduleLine() {
		return (SchedlineData) new Schedline();
	}

	/**
	 * Create a new ExtendedStatus object.
	 *
	 * @see ExtendedStatusData
	 */
	public ExtendedStatusData createExtendedStatus() {
		return (ExtendedStatusData) new ExtendedStatus();
	}

	/**
	 * Sets the quantity which is still open to deliver.
	 *
	 * @param qty the quantity to be set
	 */
	public void setQuantityToDeliver(String qty) {
		this.quantityToDeliver = qty;
	}

	/**
	 * Gets the still to deliver quantity. Difference between
	 * ordered quantity and already delivered quantity.
	 *
	 * @return the quantity
	 */
	public String getQuantityToDeliver() {
		return quantityToDeliver;
	}

	/**
	 * Gets the flag, if the product variant allows only the
	 * replacement by another variant, but no changes of
	 * the characteristics.
	 *
	 * @return <code>true</code> the replacement is only permitted by
	 *         another variant and no changes of characteristics are allowed,
	 *         otherwise <code>false</code>.
	 */
	public boolean getOnlyVarFind() {
		return onlyVarFind;
	}

	/**
	 * Sets the flag, if the product variant allows only the
	 * replacement by another variant, but no changes of
	 * the characteristics
	 */
	public void setOnlyVarFind(boolean onlyVarFind) {
		this.onlyVarFind = onlyVarFind;
	}

	/**
	 * Returns the predecessor.
	 * 
	 * @return ConnectedDocumentItemData
	 */
	public ConnectedDocumentItemData getPredecessor() {
		return predecessor;
	}

	/**
	 * Sets the predecessor.
	 * 
	 * @param predecessor The predecessor to set
	 */
	public void setPredecessor(ConnectedDocumentItemData predecessor) {
		this.predecessor = predecessor;
	}

	/**
	 * Returns the sucessor.
	 * 
	 * @return ConnectedDocumentItemData
	 */
	public ConnectedDocumentItemData getSucessor() {
		return sucessor;
	}

	/**
	 * Sets the sucessor.
	 * 
	 * @param sucessor The sucessor to set
	 */
	public void setSucessor(ConnectedDocumentItemData sucessor) {
		this.sucessor = sucessor;
	}

	/**
	 * Create a <code>ConnectedDocumentItemData</code> object
	 *
	 * @return ConnectedDocumentItemData object
	 * @see ConnectedDocumentItemData
	 */
	public ConnectedDocumentItemData createConnectedDocumentItemData() {
		return new ConnectedDocumentItem();
	}

	/**
	 * Performs a shallow copy of this object. Because of the fact that
	 * nearly all fields of this object consist of immutable objects like
	 * <code>String</code> and <code>TechKey</code> or primitive types
	 * the shallow copy is nearly identical with a deep copy. The only
	 * difference is that the property <code>itemDelivery</code> (a list)
	 * is backed by the same data.
	 *
	 * @return shallow copy of this object
	 */
	public Object clone() {

		try {
			ItemBase myClone = (ItemBase) super.clone();
			if (partnerList != null) {
				myClone.partnerList = (PartnerList) partnerList.clone();
			}
			return myClone;
		} catch (CloneNotSupportedException e) {
			return null;
		}
	}

	/**
	 * Returns the isDataSetExternally.
	 *
	 * @return boolean returns true, if item information shouldn't be completed
	 * by the backend
	 */
	public boolean isDataSetExternally() {
		return isDataSetExternally;
	}

	/**
	 * Sets the isDataSetExternally.
	 *
	 * @param isDataSetExternally must be set to true, if the backend shouldn't
	 * complete the information for a newly entered item
	 */
	public void setDataSetExternally(boolean isDataSetExternally) {
		this.isDataSetExternally = isDataSetExternally;
	}

	/**
	 * 
	 */
	public void setAllValues(
		boolean configurable,
		boolean configurableChangeable,
		String currency,
		boolean currencyChangeable,
		String description,
		boolean descriptionChangeable,
		ExtendedStatusData extendedStatus,
		boolean extendedStatusChangeable,
		String partnerProduct,
		boolean partnerProductChangeable,
		TechKey pcat,
		boolean pcatChangeable,
		String pcatArea,
		boolean pcatAreaChangeable,
		String pcatVariant,
		boolean pcatVariantChangeable,
		String product,
		boolean productChangeable,
		TechKey productId,
		boolean productIdChangeable,
		String quantity,
		boolean quantityChangeable,
		String reqDeliveryDate,
		boolean reqDeliveryDateChangeable,
		String unit,
		boolean unitChangeable,
		boolean isAvailableInResellerCatalog) {

		this.configurable = configurable;
		this.currency = currency;
		this.currencyChangeable = currencyChangeable;
		this.description = description;
		this.descriptionChangeable = descriptionChangeable;
		this.extendedStatus = (ExtendedStatus) extendedStatus;
		this.extendedStatusChangeable = extendedStatusChangeable;
		this.partnerProduct = partnerProduct;
		this.partnerProductChangeable = partnerProductChangeable;
		this.pcat = pcat;
		this.pcatChangeable = pcatChangeable;
		this.pcatArea = pcatArea;
		this.pcatAreaChangeable = pcatAreaChangeable;
		this.pcatVariant = pcatVariant;
		this.pcatVariantChangeable = pcatVariantChangeable;
		this.product = product;
		this.productChangeable = productChangeable;
		this.productId = productId;
		this.productIdChangeable = productIdChangeable;
		this.quantity = quantity;
		this.quantityChangeable = quantityChangeable;
		this.reqDeliveryDate = reqDeliveryDate;
		this.reqDeliveryDateChangeable = reqDeliveryDateChangeable;
		this.unit = unit;
		this.unitChangeable = unitChangeable;
		this.isAvailableInResellerCatalog = isAvailableInResellerCatalog;
	}

	/**
	 * @deprecated
	 */
	public void setAllValuesChangeable(
		boolean configurableChangeable,
		boolean currencyChangeable,
		boolean reqDeliveryDateChangeable,
		boolean descriptionChangeable,
		boolean extendedStatusChangeable,
		boolean partnerProductChangeable,
		boolean pcatChangeable,
		boolean pcatAreaChangeable,
		boolean pcatVariantChangeable,
		boolean productChangeable,
		boolean productIdChangeable,
		boolean quantityChangeable,
		boolean unitChangeable) {
		this.setAllValuesChangeable(
			configurableChangeable,
			currencyChangeable,
			reqDeliveryDateChangeable,
			descriptionChangeable,
			extendedStatusChangeable,
			partnerProductChangeable,
			pcatChangeable,
			pcatAreaChangeable,
			pcatVariantChangeable,
			productChangeable,
			productIdChangeable,
			quantityChangeable,
			unitChangeable,
			false);
	}

	/**
	 * @deprecated
	 */
	public void setAllValuesChangeable(
		boolean configurableChangeable,
		boolean currencyChangeable,
		boolean reqDeliveryDateChangeable,
		boolean descriptionChangeable,
		boolean extendedStatusChangeable,
		boolean partnerProductChangeable,
		boolean pcatChangeable,
		boolean pcatAreaChangeable,
		boolean pcatVariantChangeable,
		boolean productChangeable,
		boolean productIdChangeable,
		boolean quantityChangeable,
		boolean unitChangeable,
		boolean poNumberUCChangeable) {
		this.poNumberUCChangeable = poNumberUCChangeable;
		this.currencyChangeable = currencyChangeable;
		this.descriptionChangeable = descriptionChangeable;
		this.extendedStatusChangeable = extendedStatusChangeable;
		this.partnerProductChangeable = partnerProductChangeable;
		this.pcatChangeable = pcatChangeable;
		this.pcatAreaChangeable = pcatAreaChangeable;
		this.pcatVariantChangeable = pcatVariantChangeable;
		this.productChangeable = productChangeable;
		this.productIdChangeable = productIdChangeable;
		this.quantityChangeable = quantityChangeable;
		this.reqDeliveryDateChangeable = reqDeliveryDateChangeable;
		this.unitChangeable = unitChangeable;
	}

	public void setAllValuesChangeable(
		boolean configurableChangeable,
		boolean currencyChangeable,
		boolean reqDeliveryDateChangeable,
		boolean descriptionChangeable,
		boolean extendedStatusChangeable,
		boolean partnerProductChangeable,
		boolean pcatChangeable,
		boolean pcatAreaChangeable,
		boolean pcatVariantChangeable,
		boolean productChangeable,
		boolean productIdChangeable,
		boolean quantityChangeable,
		boolean unitChangeable,
		boolean poNumberUCChangeable,
		boolean deliveryPriorityChangeable) {
		this.configurableChangeable = configurableChangeable;
		this.poNumberUCChangeable = poNumberUCChangeable;
		this.currencyChangeable = currencyChangeable;
		this.descriptionChangeable = descriptionChangeable;
		this.extendedStatusChangeable = extendedStatusChangeable;
		this.partnerProductChangeable = partnerProductChangeable;
		this.pcatChangeable = pcatChangeable;
		this.pcatAreaChangeable = pcatAreaChangeable;
		this.pcatVariantChangeable = pcatVariantChangeable;
		this.productChangeable = productChangeable;
		this.productIdChangeable = productIdChangeable;
		this.quantityChangeable = quantityChangeable;
		this.reqDeliveryDateChangeable = reqDeliveryDateChangeable;
		this.unitChangeable = unitChangeable;
		this.deliveryPriorityChangeable = deliveryPriorityChangeable;
	}

	public void setChangeable(boolean flag) {
		configurableChangeable = flag;
		currencyChangeable = flag;
		reqDeliveryDateChangeable = flag;
		descriptionChangeable = flag;
		extendedStatusChangeable = flag;
		partnerProductChangeable = flag;
		pcatChangeable = flag;
		pcatAreaChangeable = flag;
		pcatVariantChangeable = flag;
		productChangeable = flag;
		productIdChangeable = flag;
		quantityChangeable = flag;
		reqDeliveryDateChangeable = flag;
		unitChangeable = flag;
		deliveryPriorityChangeable = flag;
	}

	/**
	 * Get the external row number reference
	 * 
	 * @return String the external row number reference
	 */
	public String getExtNumberint() {
		return extNumberInt;
	}

	/**
	 * Set the external row number reference
	 * 
	 * @param extNumberint the external row number reference
	 */
	public void setExtNumberint(String extNumberInt) {
		this.extNumberInt = extNumberInt;
	}

	/**
	 * Get the changeable flag for extNumberInt
	 * 
	 * @return true if extNumberInt is changeable
	 *         false else
	 */
	public boolean isExtNumberIntChangeable() {
		return isExtNumberIntChangeable;
	}

	/**
	 * Set the changeable flag for extNumberInt
	 * 
	 * @param isExtNumberIntChangeable 
	 */
	public void setExtNumberIntChangeable(boolean isExtNumberIntChangeable) {
		this.isExtNumberIntChangeable = isExtNumberIntChangeable;
	}

	/**
	 * Returns true, if itemUsage is set to ITEM_USAGE_CONFIGURATION
	 * 
	 * @return boolean true if itemUsage is set to ITEM_USAGE_CONFIGURATION
	 *                 false else
	 */
	public boolean isItemUsageConfiguration() {
		return ITEM_USAGE_CONFIGURATION.equals(itmUsage);
	}

	/**
	 * Returns true, if itemUsage is set to ITEM_USAGE_ATP
	 * 
	 * @return boolean true if itemUsage is set to ITEM_USAGE_ATP
	 *                 false else
	 */
	public boolean isItemUsageATP() {
		return ITEM_USAGE_ATP.equals(itmUsage);
	}

	/**
	 * Returns true, if itemUsage is set to ITEM_USAGE_RETURN 
	 * 
	 * @return boolean true if itemUsage is set to ITEM_USAGE_RETURN 
	 *                 false else
	 */
	public boolean isItemUsageReturn() {
		return ITEM_USAGE_RETURN.equals(itmUsage);
	}

	/**
	 * Returns true, if itemUsage is set to ITEM_USAGE_BOM
	 * 
	 * @return boolean true if itemUsage is set to ITEM_USAGE_BOM
	 *                 false else
	 */
	public boolean isItemUsageBOM() {
		return ITEM_USAGE_BOM.equals(itmUsage);
	}

	/**
	 * Returns true, if itemUsage is set to ITEM_USAGE_FREE_GOOD_INCL
	 * 
	 * @return boolean true if itemUsage is set to ITEM_USAGE_FREE_GOOD_INCL
	 *                 false else
	 */
	public boolean isItemUsageFreeGoodIncl() {
		return ITEM_USAGE_FREE_GOOD_INCL.equals(itmUsage);
	}

	/**
	 * Returns true, if itemUsage is set to ITEM_USAGE_FREE_GOOD_EXCL
	 * 
	 * @return boolean true if itemUsage is set to ITEM_USAGE_FREE_GOOD_EXCL
	 *                 false else
	 */
	public boolean isItemUsageFreeGoodExcl() {
		return ITEM_USAGE_FREE_GOOD_EXCL.equals(itmUsage);
	}

	/**
	 * Returns true, if itemUsage is set to ITEM_USAGE_1_TO_N_SUBST
	 * 
	 * @return boolean true if itemUsage is set to ITEM_USAGE_1_TO_N_SUBST
	 *                 false else
	 */
	public boolean isItemUsage1ToNSubst() {
		return ITEM_USAGE_1_TO_N_SUBST.equals(itmUsage);
	}

	/**
	 * Returns true, if itemUsage is set to ITEM_USAGE_KIT
	 * 
	 * @return boolean true if itemUsage is set to ITEM_USAGE_KIT
	 *                 false else
	 */
	public boolean isItemUsageKit() {
		return ITEM_USAGE_KIT.equals(itmUsage);
	}

	/**
	 * Returns true, if itemUsage is set to ITEM_USAGE_SC_SALES_COMPONENT
	 * 
	 * @return boolean true if itemUsage is set to ITEM_USAGE_SC_SALES_COMPONENT
	 *                 false else
	 */
	public boolean isItemUsageScSalesComponent() {
		return ITEM_USAGE_SC_SALES_COMPONENT.equals(itmUsage);
	}

	/**
	 * Sets itmUsage to ITEM_USAGE_SC_SALES_COMPONENT
	 */
	public void setItemUsageScSalesComponent() {
		itmUsage = ITEM_USAGE_SC_SALES_COMPONENT;
	}

	/**
	 * Returns true, if itemUsage is set to ITEM_USAGE_SC_RATE_PLAN_COMBINATION_COMPONENT
	 * 
	 * @return boolean true if itemUsage is set to ITEM_USAGE_SC_RATE_PLAN_COMBINATION_COMPONENT
	 *                 false else
	 */
	public boolean isItemUsageScRatePlanCombinationComponent() {
		return ITEM_USAGE_SC_RATE_PLAN_COMBINATION_COMPONENT.equals(itmUsage);
	}
			
	/**
	 * Sets itmUsage to ITEM_USAGE_SC_RATE_PLAN_COMBINATION_COMPONENT
	 */
	public void setItemUsageSCRatePlanCombinationComponent() {
		itmUsage = ITEM_USAGE_SC_RATE_PLAN_COMBINATION_COMPONENT;
	}	
	
	/**
	 * Returns true, if itemUsage is set to ITEM_USAGE_SC_DEPENDENT_COMPONENT
	 * 
	 * @return boolean true if itemUsage is set to ITEM_USAGE_SC_DEPENDENT_COMPONENT
	 *                 false else
	 */
	public boolean isItemUsageScDependentComponent() {
		return ITEM_USAGE_SC_DEPENDENT_COMPONENT.equals(itmUsage);
	}
	
	/**
	 * Sets itmUsage to ITEM_USAGE_DEPENDENT_COMPONENT
	 */
	public void setItemUsageDependentComponent() {
		itmUsage = ITEM_USAGE_SC_DEPENDENT_COMPONENT;
	}
	
	/**
	 * Sets itmUsage to ITEM_USAGE_CONFIGURATION
	 */
	public void setItemUsageConfiguration() {
		itmUsage = ITEM_USAGE_CONFIGURATION;
	}

	/**
	 * Sets itmUsage to ITEM_USAGE_ATP
	 */
	public void setItemUsageATP() {
		itmUsage = ITEM_USAGE_ATP;
	}

	/**
	 * Sets itmUsage to ITEM_USAGE_RETURN = 
	 */
	public void setItemUsageReturn() {
		itmUsage = ITEM_USAGE_RETURN;
	}

	/**
	 * Sets itmUsage to ITEM_USAGE_BOM
	 */
	public void setItemUsageBOM() {
		itmUsage = ITEM_USAGE_BOM;
	}

	/**
	 * Sets itmUsage to ITEM_USAGE_FREE_GOOD_INCL
	 */
	public void setItemUsageFreeGoodIncl() {
		itmUsage = ITEM_USAGE_FREE_GOOD_INCL;
	}

	/**
	 * Sets itmUsage to ITEM_USAGE_FREE_GOOD_EXCL
	 */
	public void setItemUsageFreeGoodExcl() {
		itmUsage = ITEM_USAGE_FREE_GOOD_EXCL;
	}

	/**
	 * Sets itmUsage to ITEM_USAGE_1_TO_N_SUBST
	 */
	public void setItemUsage1ToNSubst() {
		itmUsage = ITEM_USAGE_1_TO_N_SUBST;
	}

	/**
	 * Sets itmUsage to ITEM_USAGE_KIT
	 */
	public void setItemUsageKit() {
		itmUsage = ITEM_USAGE_KIT;
	}
	/**
	 * returns true, if the item is ATP relevant
	 * 
	 * @return true, if the item is ATP relevant
	 */
	public boolean isATPRelevant() {
		return isATPRelevant;
	}

	/**
	 * returns true, if the item is price relevant
	 * 
	 * @return true, if the item is price relevant
	 */
	public boolean isPriceRelevant() {
		return isPriceRelevant;
	}

	/**
	 * Sets the flag, that shows, if the position is ATP relevant
	 * 
	 * @param isATPRelevant true or false
	 */
	public void setATPRelevant(boolean isATPRelevant) {
		this.isATPRelevant = isATPRelevant;
	}

	/**
	 * Sets the flag, that shows, if the position is price relevant
	 * 
	 * @param isPriceRelevant true or false
	 */
	public void setPriceRelevant(boolean isPriceRelevant) {
		this.isPriceRelevant = isPriceRelevant;
	}

	/**
	 * Sets the configType flag, if the item is configurable
	 * 
	 * *param configType contains the value <value> A,B,X, G, etc..</value>
	 */
	public void setConfigType(String configType) {
		this.configType = configType;
	}
	/**
	 * Returns configType flag value, if the item is configurable 
	 * 
	 * @return A,B,X,G, etc, if the item is configurable
	 */
	public String getConfigType() {
		return configType;
	}
	/**
	 *  Sets statistical price flag value as follows
	 * 
	 * 'X': No cumulation - Values cannot be used statistically
	 * 'Y': No cumulation - Values can be used statistically
	 * ' ':  System will copy item to header totals
	 */
	public void setStatisticPrice(String statisticPrice) {
		this.statisticPrice = statisticPrice;
	}

	/**
	 * Returns statistical price flag value, if the item is priced at item or
	 * sub-item level 
	 * 
	 * @return X, Y, blank 
	 */
	public String getStatisticPrice() {
		return statisticPrice;
	}

	/**
	 * Returns the delivery priority key
	 * @return String
	 */
	public String getDeliveryPriority() {
		return deliveryPriority;
	}

	/**
	 * Sets the delivery priority key
	 * @param deliveryPriority
	 */
	public void setDeliveryPriority(String deliveryPriority) {
		this.deliveryPriority = deliveryPriority;
	}

	/**
	 * Returns true if the delivery priority is changeable
	 * @return boolean
	 */
	public boolean isDeliveryPriorityChangeable() {
		return deliveryPriorityChangeable;
	}

	/**
	 * Sets the deliveryPriorityChangeable flag
	 * @param deliveryPriorityChangeable
	 */
	public void setDeliveryPriorityChangeable(boolean deliveryPriorityChangeable) {
		this.deliveryPriorityChangeable = deliveryPriorityChangeable;
	}
	/**
	 * Retrieves the free quantity. Only relevant for inclusive
	 * free goods.
	 * 
	 * @return the free quantity in locale specific format.
	 */
	public String getFreeQuantity() {
		return freeQuantity;
	}

	/**
	 * Sets the free quantity. Only relevant for inclusive free goods.
	 * 
	 * @param arg the free quantity in locale specific format.
	 */
	public void setFreeQuantity(String arg) {
		freeQuantity = arg;
	}
	/**
	 * Sets the changeable flags for assigned external reference objects
	 *
	 * @param assignedExtRefObjectsChangeable <code>true</code> indicates that the
	 *        list of assigned external reference objects is changeable
	 */
	public void setAssignedExtRefObjectsChangeable(boolean assignedExtRefObjectsChangeable) {

		this.assignedExtRefObjectsChangeable = assignedExtRefObjectsChangeable;
	}

	/**
	 * Checks whether or not the assigned that the
	 * list of assigned external reference objects is changeable.
	 *
	 * @return <code>true</code> indicates that the external reference object list is changeable,
	 *         <code>false</code> indicates that the list cannot be changed.
	 */
	public boolean isAssignedExtRefObjectsChangeable() {
		return assignedExtRefObjectsChangeable;
	}

	/**
	 * Returns the list of external reference objects assigned to the item.
	 *
	 * @return ExtRefObjectList the list of external reference objects assigned to the item
	 */
	public ExtRefObjectList getAssignedExtRefObjects() {
		return assignedExtRefObjects;
	}

	/**
	 * Returns the list of external reference objects assigned to the item.
	 *
	 * @return ExtRefObjectListData the list of external reference objects assigned to the item
	 */
	public ExtRefObjectListData getAssignedExtRefObjectsData() {
		return (ExtRefObjectListData) assignedExtRefObjects;
	}
	/**
	 * Sets the list of external reference objects assigned to the item.
	 *
	 * @return ExtRefObjectList the list of external reference objects assigned to the item
	 */
	public void setAssignedExtRefObjects(ExtRefObjectList extRefObjectList) {
		assignedExtRefObjects = extRefObjectList;
	}

	/**
	 * Sets the list of external reference objects assigned to the item.
	 *
	 * @return ExtRefObjectListData the list of external reference objects assigned to the item
	 */
	public void setAssignedExtRefObjectsData(ExtRefObjectListData extRefObjectListData) {
		assignedExtRefObjects = (ExtRefObjectList) extRefObjectListData;
	}

	/**
	 * Sets the type of external reference object (e.g. vehicle identification number)
	 * @param extRefObjectType the type of the external reference object
	 */
	public void setExtRefObjectType(String extRefObjectType) {
		this.extRefObjectType = extRefObjectType;
	}

	/**
	 * Returns the type of external reference object (e.g. vehicle identification number)
	 * @return extRefObjectType the type of the external reference object
	 */
	public String getExtRefObjectType() {
		return extRefObjectType;
	}
	/**
	 * Creates a new ExtRefObjectList.
	 *
	 * @return ExtRefObjectListData
	 */
	public ExtRefObjectListData createExtRefObjectList() {
		return new ExtRefObjectList();
	}

	/**
	 * Sets the changeable flags for assigned external reference numbers
	 *
	 * @param assignedExternalReferencesChangeable <code>true</code> indicates that the
	 *        list of assigned external reference numbers is changeable
	 */
	public void setAssignedExternalReferencesChangeable(boolean assignedExternalReferencesChangeable) {

		this.assignedExternalReferencesChangeable =
			assignedExternalReferencesChangeable;
	}

	/**
	 * Checks whether or not the assigned that the
	 * list of assigned external reference numbers is changeable.
	 *
	 * @return <code>true</code> indicates that the external reference numbers list is changeable,
	 *         <code>false</code> indicates that the list cannot be changed.
	 */
	public boolean isAssignedExternalReferencesChangeable() {
		return assignedExternalReferencesChangeable;
	}

	/**
	 * Returns the list of external reference numberss assigned to the item.
	 *
	 * @return ExternalReferenceList the list of external reference numbers assigned to the item
	 */
	public ExternalReferenceList getAssignedExternalReferences() {
		return assignedExternalReferences;
	}

	/**
	 * Returns the list of external reference numbers assigned to the item.
	 *
	 * @return ExternalReferenceListData the list of external reference numbers assigned to the item
	 */
	public ExternalReferenceListData getAssignedExternalReferencesData() {
		return (ExternalReferenceListData) assignedExternalReferences;
	}
	/**
	 * Sets the list of external reference numbers assigned to the item.
	 *
	 * @return ExternalReferenceList the list of external reference numbers assigned to the item
	 */
	public void setAssignedExternalReferences(ExternalReferenceList externalReferenceList) {
		assignedExternalReferences = externalReferenceList;
	}

	/**
	 * Sets the list of external reference numbers assigned to the item.
	 *
	 * @return ExternalReferenceListData the list of external reference numbers assigned to the items
	 */
	public void setAssignedExternalReferencesData(ExternalReferenceListData externalReferenceListData) {
		assignedExternalReferences =
			(ExternalReferenceList) externalReferenceListData;
	}
	/**
	 * Creates a new ExternalReferencList.
	 *
	 * @return ExternalReferenceListData
	 */
	public ExternalReferenceListData createExternalReferenceList() {
		return new ExternalReferenceList();
	}

	/**
	 * Set the attachment list to the item
	 * @param attachmentList, list of attachments
	 */
	public void setAttachmentList(ArrayList attachmentList) {
		this.attachmentList = attachmentList;
	}

	/**
	 * Return the attachment list of this item
	 * @return Attachment list of the item
	 */
	public ArrayList getAttachmentList() {
		return this.attachmentList;
	}

	/**
		 * Return the technical Data list of this item.
		 * @return  Technical Property Group of the item
		 */
	public TechnicalPropertyGroupData getAssignedTechnicalPropertiesData() {
		return (TechnicalPropertyGroupData) assignedTechnicalProperties;
	}

	/**
		* Return the technical Data list of this item.
		* @return  Technical Property Group of the item
		*/
	public TechnicalPropertyGroup getAssignedTechnicalProperties() {
		return assignedTechnicalProperties;
	}


	/**
		 * set the technical Data list of this item.
		 * @param technicalPropertyGroup, List of technical Data
		 */
	public void setAssignedTechnicalPropertiesData(TechnicalPropertyGroupData technicalPropertyGroupData) {
		assignedTechnicalProperties =
			(TechnicalPropertyGroup) technicalPropertyGroupData;
	}

	/**
		* set the technical Data list of this item.
		* @param technicalPropertyGroup, List of technical Data
		*/
	public void setAssignedTechnicalProperties(TechnicalPropertyGroup technicalPropertyGroup) {
		assignedTechnicalProperties = technicalPropertyGroup;
	}

	/**
	 * Creates a new technical Property Group.
	 * @return TechnicalPropertyGroupData
	 */
	public TechnicalPropertyGroupData createTechncialPropertyGroup() {
		return new TechnicalPropertyGroup();
	}


	/**
	 * 
	 * Returns true, if the package is exploded
	 * @return boolean true if the package is exploded 
	 *                 false else 
	 *
	 */
	 public boolean isPackageExploded(){
		return this.isPackageExploded;
	 }

	/**
	 * 
	 * Returns true, if the product is a main product of a package
	 * @return boolean true if the product is a main product of a package 
	 *                 false else 
	 *
	 */
	 public boolean isMainProductFromPackage() {
	 	return this.isMainProductFromPackage;
	 }
     
     /**
      * sets true if the product is a main product of a package
      * else sets false.
      */
     public void setIsMainProductForPackage(boolean mainProductFromPackage) {
         this.isMainProductFromPackage = mainProductFromPackage;
     }
	
	/**
	 *  Returns true, if the product is a component of a package and the 
	 * product is selected
	 * @return boolean true if the product is selected within a package 
	 *                 false else 
	 *
	 */
	 public boolean isScSelected() {
	 	return this.isScSelected;	 	
	 }
	 /**
	  * 
	  * Sets the group of the package subitem 
	  * @param String the group of the package subitem
	  *               
	  *
	  */
	  public void setScGroup(String scGroup) {
	  	this.scGroup = scGroup;	 
	  }
	  
	 /**
	  * 
	  * Returns the group of the package subitem set by the solution configurator
	  * @return String the group of the package subitem
	  *               
	  *
	  */
	  public String getScGroup() {
	  	return this.scGroup;	 
	  }
	 
	  /**
	   * 
	   * Sets the author flag (selected, deselelcted) of the package subitem 
	   * set by the solution configurator
	   * @param String the author flag  of the package subitem
	   *               
	   *
	   */
	   public void setScAuthor(String scAuthor) {
		 this.scAuthor = scAuthor;	 
	   }
	   	 
	 /**
	  * 
	  * Returns the selection flag (selected, deselelcted) of the package item set by
	  * the solution configurator
	  * @return author the selection flag of the package subitem 
	  *
	  */
	  public String getScAuthor() {
	  	return this.scAuthor;
	  }
	  /**
	   * @return the usage scope of Item Type: contractrelevant
	   * deliveryrelevant or space
	   * introduced with CRM Telco solution and usage of several payment methods
	  */
	  public String getUsageScope() {
		  return usageScope;
	 }

	  /**
	  * @param string: sets the usage scope of the item type: 
	  * contract relevant, deliveryrelvant or space.
	  * introduced with CRM Telco solution and usage of several payment methods 
	  */
	  public void setUsageScope(String string) {
           this.usageScope = string;
	  }	  
	/** Determines, if an alternative product is available for the package component
	 * @return <code>true</code> if an alternative product are available for the package components 
	 */
	public boolean isScAlternativeProductAvailable() {
		return scAlternativeProductAvailable;
	}

	/** sets the attribute scAlternativeProductAvailable
	 * @param alternativeProductAvailable set <code> true </code> if item is a package component and 
	 * an alternative product is available otherwise <code>false</code>
	 */
	public void setScAlternativeProductAvailable(boolean alternativeProductAvailable) {
		scAlternativeProductAvailable = alternativeProductAvailable;
	}
    
    /**
     * Returns the recurring Duration
     * @return the recurring Duration
     */
    public String getRecurringDuration() {
        return recurringDuration;
    }
    
    /**
     * Returns the recurring Net value
     * @return the recurring Net value
     */
    public String getRecurringNetValue() {
        return recurringNetValue;
    }

    /**
     * Returns the recurring Gross value
     * @return the recurring Gross value
     */
    public String getRecurringGrossValue() {
        return recurringGrossValue;
    }

    /**
     * Returns the  recurring Tax value
     * @return the recurring Tax value
     */
    public String getRecurringTaxValue() {
        return recurringTaxValue;
    }

    /**
     * Returns the recurring Time unit 
     * @return the recurring Time unit 
     */
    public String getRecurringTimeUnit() {
        return recurringTimeUnit;
    }
    
    /**
     * Returns the recurring Time unit abbreviation 
     * @return the recurring Time unit abbreviation
     */
    public String getRecurringTimeUnitAbr() {
        return recurringTimeUnitAbr;
    }
    
    /**
     * Returns the plural of the recurring Time unit 
     * @return the plural of the recurring Time unit
     */
    public String getRecurringTimeUnitPlural() {
        return recurringTimeUnitPlural;
    }
    
    /**
     * Sets the recurring Duration
     * @param recurringDuration the recurring Duration
     */
    public void setRecurringDuration(String recurringDuration) {
        this.recurringDuration = recurringDuration;
    }
    
    /**
     * Sets the recurring Net value
     * @param recurringNetValue the recurring Net value
     */
    public void setRecurringNetValue(String recurringNetValue) {
        this.recurringNetValue = recurringNetValue;
    }

    /**
     * Sets the recurring Gross value
     * @param recurringGrossValue the recurring Gross value
     */
    public void setRecurringGrossValue(String recurringGrossValue) {
        this.recurringGrossValue = recurringGrossValue;
    }

    /**
     * Sets the recurring Tax value
     * @param recurringTaxValue the recurring Tax value
     */
    public void setRecurringTaxValue(String recurringTaxValue) {
        this.recurringTaxValue = recurringTaxValue;
    }

    /**
     * Sets the recurring Time unit
     * @param recurringTimeUnit the recurring Time unit
     */
    public void setRecurringTimeUnit(String recurringTimeUnit) {
        this.recurringTimeUnit = recurringTimeUnit;
    }
    
    /**
     * Sets the recurring Time unit abbreviation 
     * @param the recurring Time unit abbreviation
     */
    public void setRecurringTimeUnitAbr(String recurringTimeUnitAbr) {
        this.recurringTimeUnitAbr = recurringTimeUnitAbr;
    }
    
    /**
     * Sets the plural of the recurring Time unit 
     * @param the plural of the recurring Time unit
     */
    public void setRecurringTimeUnitPlural(String recurringTimeUnitPlural) {
        this.recurringTimeUnitPlural = recurringTimeUnitPlural;
    }

    /**
     * Returns the contract duration.
     */
    public String getContractDuration() {
        return contractDuration;
    }

    /**
     * Returns the unit for the contract duration.
     */
    public String getContractDurationUnit() {
        return contractDurationUnit;
    }

    /**
     * Returns the abbreviation of the text of the contract duration.
     */
    public String getContractDurationUnitAbbr() {
        return contractDurationUnitAbbr;
    }

    /**
     * Returns the plural of the text for the contract duration.
     */
    public String getContractDurationUnitTextPlural() {
        return contractDurationUnitTextPlural;
    }

    /**
     * Sets the contract duration.
     * 
     * @param   contractDuration  to be interpreted together with contract time unit.
     */
    public void setContractDuration(String contractDuration) {
        this.contractDuration = contractDuration;
    }

    /**
     * Sets the contract duration unit.
     * 
     * @param contractDurationUnit to be interpreted toghether with contractDuration.
     */
    public void setContractDurationUnit(String contractDurationUnit) {
        this.contractDurationUnit = contractDurationUnit;
    }

    /**
     * Sets the abbreviation for the time unit of the contract duration.
     * 
     * @param contractDurationUnitAbbr as abbreviation.
     */
    public void setContractDurationUnitAbbr(String contractDurationUnitAbbr) {
        this.contractDurationUnitAbbr = contractDurationUnitAbbr;
    }

    /**
     * Sets the plural text for the contract duration time unit.
     * 
     * @param contractDurationUnitTextPlural to be used together with contractDuration.
     */
    public void setContractDurationUnitTextPlural(String contractDurationUnitTextPlural) {
        this.contractDurationUnitTextPlural = contractDurationUnitTextPlural;
    }

    /**
     * Returns the text for the contract duration unit.
     */
    public String getContractDurationUnitText() {
        return contractDurationUnitText;
    }

    /**
     * Sets the text for the contract duration unit.
     * 
     * @param contractDurationUnitText for singular
     */
    public void setContractDurationUnitText(String contractDurationUnitText) {
       this.contractDurationUnitText = contractDurationUnitText;
    }

	/** 
	 * Returns the score attribute of a package subitem
	 * @return scScore 
	 */
	public String getScScore() {
		return scScore;
	}

	/**
	 * Sets the score attribute of a package subitem
	 * @param string
	 */
	public void setScScore(String scScore) {
		this.scScore = scScore;
	}

	/**
	 * Returns the product role of the product (rate plan, sales package, combined rate plan) 
	 * @return productRole
	 */
	public String getProductRole() {
		return productRole;
	}

	/**
	 * Sets the product role of the product (rate plan, sales package, combined rate plan)
	 * @param productRole
	 */
	public void setProductRole(String productRole) {
		this.productRole = productRole;
	}

	/**
	 * Returns the GUID of the solution configurator document 
	 * @return SC document GUID
	 */
	public TechKey getScDocumentGuid() {
		return scDocumentGuid;
	}

	/** Sets the GUID of the solution configurator document
	 * @param SC document GUID
	 */
	public void setScDocumentGuid(TechKey key) {
		scDocumentGuid = key;
	}

	/**
	  * Adds a <code>ConnectedDocumentItem</code> to the predecessor list.
	  * A <code>null</code> reference passed to this function will be ignored,
	  * in this case nothing will happen.
	  *
	  * @param predecessorData ConnectedDocumentItem to be added to the predecessor list
	  */
	 public void addPredecessor(ConnectedDocumentItemData predecessorData) {
		 if(predecessorData != null &&
			predecessorData instanceof ConnectedDocumentItem) {
			 ConnectedDocumentItem predecessor = (ConnectedDocumentItem)predecessorData;
			 predecessorList.add(predecessor);
		 }
	 }

	 /**
	  * Get the predecessor list
	  */
	 public List getPredecessorList() {
		 return (List)predecessorList;
	 }

	 /**
	  * Adds a <code>ConnectedDocumentItem</code> to the successor list.
	  * A <code>null</code> reference passed to this function will be ignored,
	  * in this case nothing will happen.
	  *
	  * @param successorData ConnectedDocument to be added to the successor list
	  */
	 public void addSuccessor(ConnectedDocumentItemData successorData) {
		 if(successorData != null &&
			successorData instanceof ConnectedDocumentItem) {
			 ConnectedDocumentItem successor = (ConnectedDocumentItem)successorData;
			 successorList.add(successor);
		 }
	 }

	 /**
	  * Get the successor list
	  */
	 public List getSuccessorList() {
		 return (List)successorList;
	 }
	 
	 /**
	  * Returns the auctionGuid
	  * 
	  * @return TechKey the auction Guid
	  */
	 public TechKey getAuctionGuid() {
		 return auctionGuid;
	 }
     
     /**
      * Returns true, if the auctionGuid is set with a non empty
      * value
      * 
      * @return true if auctionGuid is not null, not initial and not empty and not all spaces
      *         false else 
      */
     public boolean hasAuctionRelation() {
         boolean retVal = false;
         
         if (log.isDebugEnabled()) {
             if (getAuctionGuid() == null) {
                 log.debug("auctionGuid is null");
             }
             else {
                 log.debug("auctionGuid = " + getAuctionGuid().getIdAsString() + ".");
             }
         }
         
         retVal = getAuctionGuid() != null && getAuctionGuid().getIdAsString().trim().length() > 0;
         
         return retVal;
     }

	 /**
	  * Sets the auction Guid
	  * 
	  * @param auctionGuid the auction Guid
	  */
	 public void setAuctionGuid(TechKey auctionGuid) {
		 this.auctionGuid = auctionGuid;
	 }

}
