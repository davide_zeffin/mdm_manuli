/*****************************************************************************
    Class:        ItemListBase
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP AG
    Created:      22.3.2001
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/12/14 $
*****************************************************************************/

package com.sap.isa.businessobject.item;

import com.sap.isa.core.Iterable;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.util.*;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.backend.boi.isacore.order.*;

import java.util.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.*;


/**
 * Represents a List of <code>Item</code> objects. This class can be used to
 * maintain a collection of such objects.
 * The internal storage is organized using a List, so duplicates of items
 * are allowd.
 *
 * @author SAP AG
 * @version 1.0
 *
 * @stereotype collection
 */
public class ItemListBase implements Iterable, ItemListBaseData, Cloneable {
	
    final protected IsaLocation log;

    private List items;

    /**
     * Creates a new <code>ItemList</code> object.	
     */
    public ItemListBase() {
        items = new ArrayList();
        // Remember to change the cast in the clone() method when you
        // switch to another implementor of List!!
        log = IsaLocation.getInstance(this.getClass().getName());
    }

    /**
     * Adds a new <code>Item</code> to the List.
     *
     * @param item Item to be stored in <code>ItemList</code>
     */
    public void add(ItemBase item) {
        items.add(item);
    }

    /**
     * Adds a new <code>Item</code> to the List.
     *
     * @param item Item to be stored in <code>ItemList</code>
     */
    public void add(ItemBaseData item) {
    	if (item instanceof ItemBase) {
    		items.add(item);
    	}
    	else {
    		log.debug("no allowed type in add: " + item.getClass().getName());
    	}  		
    }

    /**
     * Returns the element at the specified position in this list.
     *
     * @param index index of element to return
     * @return the element at the specified position in this list
     */
    public ItemBase getItemBase(int index) {
        return (ItemBase) items.get(index);
    }

    /**
     * Returns the element at the specified position in this list.
     *
     * @param index index of element to return
     * @return the element at the specified position in this list
     */
    public ItemBaseData getItemBaseData(int index) {
        return (ItemBaseData) items.get(index);
    }


    /**
     * Returns the item specified by the given technical key.<br><br>
     * <b>Note -</b> This implementation assumes that the list
     * is small and performs a simple search running in the list from the
     * first element to the last.
     *
     * @param techKey the technical key of the item that should be
     *        retrieved
     * @return the item with the given techical key or <code>null</code>
     *         if no item for that key was found.
     */
    public ItemBase getItemBase(TechKey techKey) {

        return (ItemBase)getItemBaseData(techKey);
    }


        /**
         * Returns the index for the given item key.
         * @param techKey item key
         * @return index in the item list
         */
    protected int getIndex(TechKey techKey) {
        int size = size();

        for (int i = 0; i < size; i++) {
            if (((ItemBase)items.get(i)).getTechKey().equals(techKey)) {
                return i;
            }
        }
        return -1;
    }


    /**
     * Returns the item specified by the given technical key.<br><br>
     * <b>Note -</b> This implementation assumes that the list
     * is small and performs a simple search running in the list from the
     * first element to the last.
     *
     * @param techKey the technical key of the item that should be
     *        retrieved
     * @return the item with the given techical key or <code>null</code>
     *         if no item for that key was found.
     */
    public ItemBaseData getItemBaseData(TechKey techKey) {
		int index = getIndex(techKey);

		if (index != -1) {
			return (ItemBaseData)items.get(index);
		}
		return null;
    }

    /**
     * Removes all entries.
     */
    public void clear() {
        items.clear();
    }

    /**
     * Returns the number of elements in this list.
     *
     * @return the number of elements in this list.
     */
    public int size() {
        return items.size();
    }

    /**
     * Returns true if this list contains no data.
     *
     * @return <code>true</code> if list contains no data.
     */
    public boolean isEmpty() {
        return items.isEmpty();
    }

    /**
     * Returns true if this list contains the specified element.
     *
     * @param value value whose presence in this list is to be tested.
     * @return <code>true</code> if this list contains the specified value.
     */
    public boolean contains(ItemBase value) {
        return items.contains(value);
    }

    /**
     * Returns true if this list contains the specified element.
     *
     * @param value value whose presence in this list is to be tested.
     * @return <code>true</code> if this list contains the specified value.
     */
    public boolean contains(ItemBaseData value) {
        return items.contains(value);
    }

    /**
     * Returns an iterator over the elements contained in the
     * <code>SoldToList</code>.
     *
     * @return Iterator for this object
     */
    public Iterator iterator() {
        return items.iterator();
    }


    /**
     * Remove the element at the specified position in this list.
     *
     * @param index index of element to remove from the list
     */
    public void remove(int index) {
        items.remove(index);
    }


    /**
     * Remove the given element from this list.
     *
     * @param techKey index of element to remove from the list
     */
    public void remove(TechKey techKey) {

        int index = getIndex(techKey);

        if (index != -1) {
            items.remove(index);
        }
    }


    /**
     * Searches the list for double entries of identical products (as
     * shown by the product GUID or the product id) and merges all
     * identical products together to one entry in the basket. The
     * amount of the items is summed up. This is only performed
     * if the unit of the items is identical, too and the items
     * are not configurable or not a package product or package components.
     *
     * @param decimalPointFormat the number format used
     */
    public void mergeIdenticalProducts(DecimalPointFormat decimalPointFormat) {
        int size = items.size();
        Map productItemMap = new HashMap(size);
        ItemBase nullItem = new ItemBase();
        String itemKey = null;

        for (int i = 0; i < size; i++) {
            ItemBase item = (ItemBase) items.get(i);
            
            // Package products or components should not be merged
            if ( item.isMainProductFromPackage()
			    || item.isItemUsageScDependentComponent()
			    || item.isItemUsageScRatePlanCombinationComponent() 
			    || item.isItemUsageScSalesComponent()
			    || item.hasAuctionRelation()) {
			    continue;
			 }

            TechKey productId = item.getProductId();
            
            itemKey = productId.getIdAsString() + "&&&" + item.getUnit();

            ItemBase mapItem = (ItemBase) productItemMap.get(itemKey);

            if (mapItem == null) {
                productItemMap.put(itemKey, item);
            }
            else {
                String quantity1 = mapItem.getQuantity();
                String quantity2 = item.getQuantity();
                String unit1 = mapItem.getUnit();
                String unit2 = item.getUnit();
				boolean isPointsItem1 = false;
				boolean isPointsItem2 = false;
                boolean isBuyPtsItem1 = false;
                boolean isBuyPtsItem2 = false;
                
				if (item instanceof ItemData && mapItem instanceof ItemData) {
					ItemData item1 = (ItemData) mapItem;
					ItemData item2 = (ItemData) item;
                	
					isPointsItem1 = item1.isPtsItem();
					isPointsItem2 = item2.isPtsItem();
                    isBuyPtsItem1 = item1.isBuyPtsItem();
                    isBuyPtsItem2 = item2.isBuyPtsItem();
				}

                // the quantities of buy points items are not added                   
                if ( isBuyPtsItem1 && isBuyPtsItem2 ) {
                    items.set(i, nullItem);
                    continue;     
                }
                
                if (	isPointsItem1 == isPointsItem2 
                		&& unit1.equals(unit2)
                        && !mapItem.isConfigurable()
                        && !item.isConfigurable()) {

                    double quantityDouble1 = 0;
                    double quantityDouble2 = 0;

                    try {
                        quantityDouble1 = decimalPointFormat.parseDouble(quantity1);
                        quantityDouble2 = decimalPointFormat.parseDouble(quantity2);
                    }
                    catch (ParseException ex) {
                        if (log.isDebugEnabled()) {
                            log.debug(ex);
                        }
                    }

                    double newQuantity     = quantityDouble1 + quantityDouble2;

                    mapItem.setQuantity(decimalPointFormat.format(newQuantity));

                    items.set(i, nullItem);
                }
            }
        }

        for (int i = 0; i < items.size(); i++) {
            Object element = items.get(i);

            if (element == nullItem) {
                items.remove(i);
                i--;
            }
        }
    }

    /**
     * <p>
     * Compares the specified object with this list for equality.
     * Returns <code>true</code> if and only if the specified object is also
     * a list, both lists have the same size, and all corresponding pairs of
     * elements in the two lists are <em>equal</em>. (Two elements
     * <code>e1</code> and <code>e2</code> are equal
     * if <code>(e1==null ? e2==null : e1.equals(e2))</code>.) In other words,
     * two lists are defined to be equal if they contain the same elements
     * in the same order. </p>
     * <p>
     * This implementation first checks if the specified object is this list.
     * If so, it returns <code>true</code>; if not, it checks if the specified
     * object is a list. If not, it returns <code>false</code>; if so, it
     * iterates over both lists, comparing corresponding pairs of elements.
     * If any comparison returns <code>false</code>, this method returns
     * <code>false</code>. If either iterator runs out of
     * elements before the other it returns <code>false</code>
     * (as the lists are of unequal length); otherwise it returns true when
     * the iterations complete.
     * </p>
     *
     * @param o the object to be compared for equality with this list.
     * @retrun <code>true</code> if the specified object is equal to this list.
     */
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        else if (o == this) {
            return true;
        }
        else if (!(o instanceof ItemListBase)) {
            return false;
        }
        else {
            return items.equals(((ItemListBase)o).items);
        }
    }

    /**
     * Returns the hash code value for this list. The hash code of a list is
     * defined to be the result of the following calculation:
     * <pre>
     * hashCode = 1;
     * Iterator i = list.iterator();
     * while (i.hasNext()) {
     *   Object obj = i.next();
     *   hashCode = 31*hashCode + (obj==null ? 0 : obj.hashCode());
     * }
     * </pre>
     * This ensures that <code>list1.equals(list2)</code>
     * implies that <code>list1.hashCode()==list2.hashCode()</code>
     * for any two lists, <code>list1</code> and <code>list2</code>,
     * as required by the general contract of <code>Object.hashCode</code>.
     *
     * @return the hash code value for this list.
     */
    public int hashCode() {
        return items.hashCode();
    }

    /**
     * Returns an array containing all of the elements in this list in proper
     * sequence;
     *
     * @return  an array containing the elements of this list.
     */
    public ItemBase[] toBaseArray() {
        int size = items.size();

        ItemBase[] a = new ItemBase[size];

        for (int i = 0; i < size; i++) {
            a[i] = (ItemBase) items.get(i);
        }

        return a;
    }


    /**
     * <p>
     * Returns a string representation of this collection.
     * The string representation consists of a list of the
     * collection's elements in the order they are returned
     * by its iterator, enclosed in square brackets ("[]").
     * Adjacent elements are separated by the characters ", "
     * (comma and space). Elements are converted to strings as
     * by <code>String.valueOf(Object)</code>.
     * </p>
     * <p>
     * This implementation creates an empty string buffer,
     * appends a left square bracket, and iterates over the
     * collection appending the string representation of each
     * element in turn. After appending each element except the
     * last, the string ", " is appended. Finally a right bracket
     * is appended. A string is obtained from the string buffer,
     * and returned.
     * </p>
     *
     * @return a string representation of this collection
     */
    public String toString() {
        return "ItemList " + items.toString();
    }

    /**
     * Performs a shallow copy of the item list. The object returned by this
     * method is an copy of the object you call this method on. Thus the
     * expression <code>x.clone() != x</code> is <code>true</code>. The returned
     * list can be independently used without interferences with the original
     * object. The items stored in the list are not copied (shallow copy).
     * Modifying an item in the original list will modify the item in the
     * copied list, too.
     *
     * @return a copy of this object
     */
    public Object clone() {
        ItemListBase myClone = null;
        try {
            myClone = (ItemListBase) super.clone();
			myClone.items = (List) ((ArrayList)items).clone();
        }
        catch (CloneNotSupportedException ex) {
        	log.debug(ex);
        }
        return myClone;
    }

    /** 
     * returns true or false if the list of items contains an entry with 
     * required usage scope. The usage scope can be contract relevant or
     * delivery relevant.
     * Implemented for CRM 51 telco screnario 
     * @author d034021
     * 
     */
    
    public boolean listItemContainsUsageScope(String usageScope) {
        Iterator it = items.iterator();
        while (it.hasNext()) {    
            ItemBase item = (ItemBase)it.next();
            if (item.getUsageScope().equals(usageScope)) {    	
                return true;
            }       	       	   
        }
        return false;
    }
    
    /**
     * returns true, if the item table column should be desplayed
     * it means, at least one item has a value for the field which
     * belongs to the column.
     * 
     * @param methodName is the method of class ItemBase, which delivers the
     * appropriate values.
     * e.g. getDeliveryPriority - the method will be called with this parameter, 
     * to check if the delivery priority column will be desplayed at the JSP or
     * not.
     * 
     * @return true (display) or false (don't display)
     */
	public boolean listItemContainsValue (String attribute) throws PanicException {
		String methodName = "get" + attribute;
		Iterator it = items.iterator();		
		Method detectedMethod = null;
		Object[] args = null;
		Object methodResult = null; 				   
		ItemBase item = (ItemBase)it.next();			
		Class newItemBaseClass = item.getClass();
		Method[] methods = newItemBaseClass.getMethods();
		boolean visible = false;
		
		// check, that the object has methods, to get the attributes and detect the 
		// needed method!	
		if (methods.length > 0) {
			for (int i = 0; i < methods.length; i++) {
				if (detectedMethod == null) {					
					if (methods[i].getName().equalsIgnoreCase(methodName)){
						detectedMethod = methods[i];
					}					  
			    }					  
			}			   
		}
		else {
			String msg = "No method get" + attribute + "found!";
			throw new PanicException(msg);
		}
			
		// if the method was detected try to find out if the 	
		if ( detectedMethod	!= null) {
		    for (int k = 0; k < items.size(); k++ ) {
		  	    if (visible == false) {		  	
		   	        ItemBase itemToCheck = (ItemBase) items.get(k);
		   	        Object itemToCheckObject = (Object) itemToCheck;
		   	        try {		   	
		   	            methodResult = detectedMethod.invoke(itemToCheckObject, args);
		   	        }
		   	        catch (IllegalAccessException acex){
		   		        log.debug(acex);
		   		        String msg = "No access to method get"+ attribute + "!";
		   		        throw new PanicException(msg);		   		
		   	        }
		            catch (IllegalArgumentException arex) {
		                log.debug(arex);  
		                String msg = "Illegal argument for method get"+ attribute + "!";
		                throw new PanicException(msg);		       
		            } 
		            catch (InvocationTargetException taex) {
		                log.debug(taex); 	
		                String msg = "Invocation Target Exception for method get"+ attribute + "!";
		                throw new PanicException(msg);	        
		            }	
		    
		   	    if ( methodResult != null ) {
		   	    	if (methodResult instanceof ArrayList){
		   	    	    if ( methodResult.toString().equals("[]"))	{
							visible = false;	
		   	    	    }
		   	    	    else {
		   	    	    	visible = true;
		   	    	    	return true;
		   	    	    }
		   	    	}
		   	    	else {
						if ( methodResult.toString().equals(""))	{
						    visible = false;	
						}
						else {
						    visible = true;
						    return true;
						}	
		   	    	}		   	  		   		       	  
		   	    }
		   	    else {
		   		    String msg = "Method result for method get"+ attribute + "for" + itemToCheck.getNumberInt()+ "is null!";
		   		    throw new PanicException(msg); 
		   	    }
		  	   }		  	
		    }
		    if (visible == false) {		   	 
		   	    return false;
		    }		   		   
		}
		else {
			String msg = "No method get" + attribute + "found!";
			throw new PanicException(msg);		
		}
		return visible;					
	}			
	 
}