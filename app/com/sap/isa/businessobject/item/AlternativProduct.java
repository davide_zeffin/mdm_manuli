/*****************************************************************************
    Class:        AlternativProduct
    Copyright (c) 2003, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      15.05.2003
    Version:      1.0

    $Revision: #1 $
    $Date: 2003/05/15 $
*****************************************************************************/

package com.sap.isa.businessobject.item;

import com.sap.isa.backend.boi.isacore.order.AlternativProductData;
import com.sap.isa.core.TechKey;

/**
 * Class to define a alternativ product, that might be an alternative product
 * found by product determination or substitution
 *
 * @author SAP AG
 * @version 1.0
 */
public class AlternativProduct
implements AlternativProductData, Cloneable {

    protected String systemProductId;
    protected TechKey systemProductGUID;
    protected String enteredProductIdType;
    protected String description;
    protected String substitutionReasonId;

    /**
     * Creates a new <code>AlternativProduct</code> object.
     */
    public AlternativProduct() {
    }

    /**
     * Creates a new <code>AlternativProduct</code> object.
     *
     * @param systemProductId id of the system product
     * @param systemProductGUID techkey of the system product
     * @param description description of the system product
     * @param String enteredProductIdType if the system product was found through determination, this
     *                                    specifies, as what the entred product id was interpreted 
     * @param substitutionReasonId if the system product is a substitute product, this is the
     *                           id for the substitution reason
     */
    public AlternativProduct(String systemProductId, TechKey systemProductGUID, String description, 
                        String enteredProductIdType, String substitutionReasonId) {
        this.systemProductId = systemProductId;
        this.systemProductGUID = systemProductGUID;
        this.description = description;
        this.enteredProductIdType = enteredProductIdType;
        this.substitutionReasonId = substitutionReasonId;  
    }

    /**
     * Returns the system product id of the AlternativProduct
     *
     * @return systemProductId of AlternativProduct
     */
    public String getSystemProductId() {
        return systemProductId;
    }

    /**
     * Set the system product id of the AlternativProduct
     *
     * @param systemProductId system product id of AlternativProduct
     */
    public void setSystemProductId(String systemProductId) {
        this.systemProductId = systemProductId;
    }
    
    /**
     * Returns the description of the AlternativProduct
     * 
     * @return String
     */
    public String getDescription() {
        return description;
    }

    /**
     * Returns the enteredProductIdType of the AlternativProduct.
     * 
     * @return String
     */
    public String getEnteredProductIdType() {
        return enteredProductIdType;
    }

    /**
     * Returns the substitutionReasonId of the AlternativProduct.
     * 
     * @return String
     */
    public String getSubstitutionReasonId() {
        return substitutionReasonId;
    }

    /**
     * Returns the systemProductGUID of the AlternativProduct.
     * 
     * @return TechKey
     */
    public TechKey getSystemProductGUID() {
        return systemProductGUID;
    }

    /**
     * Sets the description of the AlternativProduct.
     * 
     * @param description The description of the AlternativProduct
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Sets the enteredProductIdType of the AlternativProduct.
     * 
     * @param enteredProductIdType The enteredProductIdType of the AlternativProduct
     */
    public void setEnteredProductIdType(String enteredProductIdType) {
        this.enteredProductIdType = enteredProductIdType;
    }

    /**
     * Sets the substitutionReasonId of the AlternativProduct.
     * 
     * @param substitutionReasonId The substitutionReasonId of the AlternativProduct
     */
    public void setSubstitutionReasonId(String substitutionReasonId) {
        this.substitutionReasonId = substitutionReasonId;
    }

    /**
     * Sets the systemProductGUID of the AlternativProduct.
     * 
     * @param systemProductGUID The systemProductGUID of the AlternativProduct
     */
    public void setSystemProductGUID(TechKey systemProductGUID) {
        this.systemProductGUID = systemProductGUID;
    }
    
    /**
     * Performs a deep copy of this object.
     * Because of the fact that
     * all fields of this object consist of immutable objects like
     * <code>String</code> and <code>TechKey</code> or primitive types
     * the shallow copy is identical with a deep copy. 
     *
     * @return shallow (deep-like) copy of this object
     */
    public Object clone() {
        AlternativProduct myClone = new AlternativProduct(systemProductId, systemProductGUID, description, 
                                                enteredProductIdType, substitutionReasonId);
        return myClone;

    }
    
    /**
     * returns true, if the alternativ product is a substitute product
     * 
     * @return boolean true if the product alias is a substitute product
     */
    public boolean isSubstituteProduct() {
            return (substitutionReasonId != null && substitutionReasonId.length() > 0 && !isDeterminationProduct());
    }
    
    /**
     * returns true, if the alternativ product is a determination product
     * 
     * @return boolean true if the product alias is a determination product
     */
    public boolean isDeterminationProduct() {
        return (enteredProductIdType != null && enteredProductIdType.length() > 0);
    }

}
