/*****************************************************************************
    Class:        ItemMap
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Thomas Smits
    Created:      19.3.2001
    Version:      1.0

    $Revision: #2 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.businessobject.item;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.sap.isa.backend.boi.isacore.order.ItemMapData;
import com.sap.isa.core.Iterable;
import com.sap.isa.core.TechKey;

/**
 * Represents a Map of Item objects. This class can be used to maintain a
 * collection of item objects and efficiently look them up using a
 * unique key.
 * The internal storage is organized using a Map, so duplicates of items
 * are not stored.
 *
 * @author Thomas Smits
 * @version 1.0
 * @see Item Item
 *
 * @stereotype collection
 */
public class ItemMap implements Iterable, ItemMapData, Cloneable {
    private Map items;

    /**
     * Creates a new <code>ItemMap</code> object.
     */
    public ItemMap() {
        items = new HashMap();
        // Remember to change the cast in the clone() method when you
        // switch to another implementor of Map!!!!
    }

    /** @link dependency */
    /*#Item lnkItem;*/


    /**
     * Adds a new <code>Item</code> to the Map.
     *
     * @param key  Key, uniquely identifying the item
     * @param item Item to be stored in <code>ItemMap</code>
     */
    public void put(TechKey key, ItemSalesDoc item) {
        items.put(key, item);
    }

    /**
     * Removes an item identified by its technical key
     *
     * @param techKey Technical key of item to be removed
     */
    public void remove(TechKey techKey) {
        items.remove(techKey);
    }

    /**
     * Adds a new <code>Item</code> to the Map.
     *
     * @param item Item to be stored in <code>ItemMap</code>
     */
    public void add(ItemSalesDoc item) {
        items.put(item.getTechKey(), item);
    }

    /**
     * Returns the <code>Item</code> to which this map maps the specified key.
     * Returns <code>null</code> if the map contains no mapping for this key.
     * A return value of null does not necessarily indicate that the map
     * contains no mapping for the key; it's also possible that the map
     * explicitly maps the key to null. The containsKey operation may be
     * used to distinguish these two cases.
     *
     * @param key key whose associated value is to be returned.
     * @return the value to which this map maps the specified key
     *
     */
    public ItemSalesDoc get(TechKey key) {
        return (ItemSalesDoc) items.get(key);
    }

    /**
     * Removes all mappings from this map.
     */
    public void clear() {
        items.clear();
    }

    /**
     * Returns the number of key-value mappings in this map.
     *
     * @return the number of key-value mappings in this map.
     */
    public int size() {
        return items.size();
    }

    /**
     * Returns true if this map contains no key-value mappings.
     *
     * @return <code>true</code> if this map contains no key-value mappings.
     */
    public boolean isEmpty() {
        return items.isEmpty();
    }

    /**
     * Returns true if this map maps one or more keys to the specified value.
     *
     * @param value value whose presence in this map is to be tested.
     * @return <code>true</code> if this map maps one or more keys to the
     *          specified value.
     */
    public boolean containsValue(ItemSalesDoc value) {
        return items.containsValue(value);
    }

    /**
     * Returns <tt>true</tt> if this map contains a mapping for the specified
     * key.
     *
     * @return <tt>true</tt> if this map contains a mapping for the specified
     *         key.
     * @param key key whose presence in this Map is to be tested.
     */
    public boolean containsKey(TechKey key) {
        return items.containsKey(key);
    }

    /**
     * Returns an iterator over the elements contained in the
     * <code>ItemMap</code>.
     *
     * @return Iterator for this object
     */
    public Iterator iterator() {
        return items.values().iterator();
    }

    /**
     * <p>
     * Compares the specified object with this map for equality.
     * Returns true if the given object is also a map and the
     * two maps represent the same mappings. More formally,
     * two maps <code>t1</code> and <code>t2</code> represent the same mappings
     * if <code>t1.keySet().equals(t2.keySet())</code> and for every
     * key k in <code>t1.keySet(), (t1.get(k)==null ? t2.get(k)==null :
     * t1.get(k).equals(t2.get(k)))</code> .
     * This ensures that the equals method works properly across
     * different implementations of the map interface.
     * </p>
     * <p>
     * This
     * implementation first checks if the specified object is
     * this map; if so it returns true. Then, it checks if the
     * specified object is a map whose size is identical to the
     * size of this set; if not, it it returns false. If so,
     * it iterates over this map's entrySet collection, and
     * checks that the specified map contains each mapping that
     * this map contains. If the specified map fails to contain
     * such a mapping, false is returned. If the iteration completes,
     * true is returned.
     *
     * @param o object to be compared for equality with this map
     * @return <code>true</code> if the specified object is equal to this map
     */
    public boolean equals(Object o) {
       if (o == null) {
            return false;
        }
        else if (o == this) {
        	return true;
        }
        else if (!(o instanceof ItemMap)) {
            return false;
        }
        else {
            return items.equals(((ItemMap)o).items);
        }
    }

    /**
     * <p>
     * Returns the hash code value for this map. The hash code
     * of a map is defined to be the sum of the hash codes of
     * each entry in the map's entrySet() view. This ensures
     * that <code>t1.equals(t2)</code> implies that
     * <code>t1.hashCode()==t2.hashCode()</code>
     * for any two maps <code>t1</code> and <code>t2</code>, as required by
     * the general
     * contract of <code>Object.hashCode</code>. This implementation iterates
     * over <code>entrySet()</code>, calling <code>hashCode</code>
     * on each element (entry)
     * in the Collection, and adding up the results.
     * </p>
     *
     * @return the hash code value for this map
     */
    public int hashCode() {
        return items.hashCode();
    }


     /**
      * <p>
      * Returns a string representation of this map. The string
      * representation consists of a list of key-value mappings
      * in the order returned by the map's <code>entrySet</code> view's
      * iterator, enclosed in braces ("{}").
      * Adjacent mappings are separated by the
      * characters ", " (comma and space). Each
      * key-value mapping is rendered as the key
      * followed by an equals sign ("=") followed
      * by the associated value. Keys and values
      * are converted to strings as by <code>String.valueOf(Object)</code>.
      * </p>
      * <p>
      * This implementation creates an empty string buffer,
      * appends a left brace, and iterates over the map's <code>entrySet</code>
      * view, appending the string representation of each <code>map.entry</code>
      * in turn. After appending each entry except the last,
      * the string ", " is appended. Finally a right brace is
      * appended. A string is obtained from the stringbuffer,
      * and returned.
      * </p>
      *
      * @return a String representation of this map
      */
    public String toString() {
        return items.toString();
    }

    /**
     * Performs a shallow copy of the item map. The object returned by this
     * method is an copy of the object you call this method on. Thus the
     * expression <code>x.clone() != x</code> is <code>true</code>. The returned
     * mapt can be independently used without interferences with the original
     * object. The items stored in the map are <b>not</b> copied (shallow copy).
     * Modifying an item in the original map will modify the item in the
     * copied map, too.
     *
     * @return a copy of this object
     */
    public Object clone() {
        ItemMap myClone = new ItemMap();
        myClone.items = (Map) ((HashMap)items).clone();
        return myClone;
    }
}