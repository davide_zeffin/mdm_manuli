/*****************************************************************************
    Interface:    BasketTransferItem
    Copyright (c) 2001, SAP AG, Germany, All rights reserved.
    Author:       SAP
    Created:      4.5.2001
    Version:      1.0

    $Revision: #5 $
    $Date: 2002/10/17 $
*****************************************************************************/

package com.sap.isa.businessobject.item;

import com.sap.isa.core.TechKey;

/**
 * This interface represents an item that is to be transfered
 * into a shopping basket
 * It will be used by the OCI-interface as well as the CUA- and the
 * Bestseller-interface.
 * You may use the default implementation for this interface provided
 * by the <code>BasketTransferItemImpl</code> class or implement
 * it with your own class.
 *
 * @author SAP AG
 * @version 1.0
 */
public interface BasketTransferItem {

	/**
	 * Returns the property quantity
	 *
	 * @return quantity
	 *
	 */
	public String getQuantity();

	/**
	 * Returns the property unit
	 *
	 * @return unit
	 *
	 */
	public String getUnit();

	/**
	 * Returns the property text
	 *
	 * @return text
	 *
	 */
	//    abstract public TextData getText();

	/**
	 * Returns the property product
	 *
	 * @return product
	 *
	 */
	public String getProductKey();

	/**
	 * Returns the property productId
	 *
	 * @return productId
	 *
	 */
	public String getProductId();

	/**
	 * Returns the property contractKey
	 *
	 * @return contractKey
	 *
	 */
	public String getContractKey();

	/**
	 * Returns the property contractId
	 *
	  * @return contractId
	 *
	 */
	public String getContractId();

	/**
	 * Returns the property contractItemKey
	 *
	 * @return contractItemKey
	 *
	 */
	public String getContractItemKey();

	/**
	 * Returns the property contractItemId
	 *
	 * @return contractItemId
	 *
	 */
	public String getContractItemId();

	/**
	 * Returns the property contractConfigFilter containing contract-specific
	 * restrictions to the configuration.
	 *
	 * @return contractConfigFilter
	 *
	 */
	public String getContractConfigFilter();

	/**
	 * Returns the property basketItemToReplace
	 *
	 * @return basketItemToReplace
	 *
	 */
	public Object getBasketItemToReplace();

	/**
	 * Reference to IPC-Item containing configuration
	 */
	public Object getConfigurationItem();

	/**
	 * Returns the property catalog
	 *
	 * @return catalog
	 *
	 */
	public String getCatalog();

	/**
	 * Returns the property catalogCategory
	 *
	 * @return catalogCategory
	 *
	 */
	public String getCatalogCategory();

	/**
	 * Returns the property customerProductNumber
	 *
	 * @return customerProductNumber
	 *
	 */
	public String getCustomerProductNumber();

	/**
	 * Returns the property variant
	 *
	 * @return variant
	 *
	 */
	public String getVariant();

	/**
	 * Returns the property reqDeliveryDate
	 *
	 * @return reqDeliveryDate
	 *
	 */
	public String getReqDeliveryDate();

	/**
	 * Returns the property description
	 *
	 * @return description
	 * 
	 */
	public String getDescription();

	/**
	 * Returns the techkey of the parent of this TransferItem.
	 * null if no parent.
	 * 
	 * @return parentTechKey
	 */
	public String getParentTechKey();

	/**
	 * Returns the techkey of this TransferItem.
	 * 
	 * @return techKey
	 */
	public String getTechKey();

	/**
	 * returns 
	 * true if the package is exploded
	 * false if the package is not exploded or if the basketTransferItem is not a package
	 * 
	 * @return isExploded
	 */
	public boolean isExploded();

	/**
	 * if the basketTransferItem is a package, returns true is this item is main product, false otherwise
	 * 
	 * @return isMainItem
	 */
	public boolean isMainItem();

	/**
	 * if the basketTransferItem is a package component, returns true if the type of interlinkage is dependent component
	 * @return
	 */
	public boolean isDependentComponent();

	/**
	 * if the basketTransferItem is a package component, returns true if the type of interlinkage is sale component
	 * @return
	 */
	public boolean isSalesComponent();

	/**
	 * if the basketTransferItem is a package component, returns true if the type of interlinkage is rate plan combination
	 * @return
	 */
	public boolean isRatePlanCombination();

	/**
	 * if the basketTransferItem is a package component, returns the authoring flag
	 * @return
	 */
	public String getSCSelection();

	/**
	 * if the basketTransferItem is a package component, returns the group
	 * @return
	 */
	public String getGroup();

	/**
	 * returns the selected contract duration
	 * 
	 * @return ContractDuration, the selected contract duration, or null if there is no ContractDuration or this item
	 */
	public ContractDuration getSelectedContractDuration();
    
    /**
     * Returns the information, if this <code>WebCatSubItem</code> is a selected by the 
     * solution configurator.
     * The Method implements ProductBaseData.
     * 
     * @return <code>true</code>, if the item is selected by the solution configurator
     *         <code>false</code>, if not
     * 
     */
    public boolean isScSelected();
    
    /**
     * Returns the scDocumentGuid
     * 
     * @return TechKey scDocumentGuid, the scDocumentGuid
     */
    public TechKey getScDocumentGuid();
    
	/**
	 * 
	 */
	public boolean isPtsItem();
	
	/**
	 * 
	 */
	public void setPtsItem(boolean isRedemption);
	
	/**
	 * 
	 */
	public boolean isBuyPtsItem();
	
	/**
	 * 
	 */
	public void setBuyPtsItem(boolean isBuyPtsItem);
	

}
