/*****************************************************************************
    Class:        ItemList
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP AG
    Created:      22.3.2001
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/12/14 $
*****************************************************************************/

package com.sap.isa.businessobject.item;

import com.sap.isa.backend.boi.isacore.order.ItemBaseData;
import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.backend.boi.isacore.order.ItemListData;
import com.sap.isa.core.TechKey;

/**
 * Represents a List of <code>Item</code> objects. This class can be used to
 * maintain a collection of such objects.
 * The internal storage is organized using a List, so duplicates of items
 * are allowd.
 *
 * @author SAP AG
 * @version 1.0
 * @see SoldTo SoldTo
 *
 * @stereotype collection
 */
public class ItemList extends ItemListBase implements ItemListData {

    /**
     * Creates a new <code>ItemList</code> object.
     */
    public ItemList() {
    	super();
    }

	/**
	 * Adds a new <code>Item</code> to the List.
	 *
	 * @param item Item to be stored in <code>ItemList</code>
	 */
	public void add(ItemBaseData item) {
		if (item instanceof ItemSalesDoc) {
			super.add(item);
		}
		else {
			log.debug("no allowed type in add: " + item.getClass().getName());
		}               
	}

	/**
	 * Adds a new <code>Item</code> to the List.
	 *
	 * @param item Item to be stored in <code>ItemList</code>
	 */
	public void add(ItemBase item) {
		add((ItemBaseData) item);
	}


    /**
     * Adds a new <code>Item</code> to the List.
     *
     * @param item Item to be stored in <code>ItemList</code>
     */
    public void add(ItemSalesDoc item) {
       add((ItemBaseData)item);
    }

    /**
     * Adds a new <code>Item</code> to the List.
     *
     * @param item Item to be stored in <code>ItemList</code>
     */
    public void add(ItemData item) {
		add((ItemBaseData) item);
    }

    /**
     * Returns the element at the specified position in this list.
     *
     * @param index index of element to return
     * @return the element at the specified position in this list
     */
    public ItemSalesDoc get(int index) {
        return (ItemSalesDoc) getItemBase(index);
    }

    /**
     * Returns the element at the specified position in this list.
     *
     * @param index index of element to return
     * @return the element at the specified position in this list
     */
    public ItemData getItemData(int index) {
        return (ItemData) getItemBaseData(index);
    }


    /**
     * Returns the item specified by the given technical key.<br><br>
     * <b>Note -</b> This implementation assumes that the list
     * is small and performs a simple search running in the list from the
     * first element to the last.
     *
     * @param techKey the technical key of the item that should be
     *        retrieved
     * @return the item with the given techical key or <code>null</code>
     *         if no item for that key was found.
     */
    public ItemSalesDoc get(TechKey techKey) {      
        return (ItemSalesDoc)getItemBase(techKey);
    }


	/**
     * Returns the item specified by the given technical key.<br><br>
     * <b>Note -</b> This implementation assumes that the list
     * is small and performs a simple search running in the list from the
     * first element to the last.
     *
     * @param techKey the technical key of the item that should be
     *        retrieved
     * @return the item with the given techical key or <code>null</code>
     *         if no item for that key was found.
     */
    public ItemData getItemData(TechKey techKey) {
        return (ItemData) get(techKey);
    }

    /**


    /**
     * Returns the number of main positions in the itemlist.<br>
     * In contrast to size()-method item positions that are sublevel<br>
     * positions to an assigned top level item are not counted.
     *
     * @return the number of main (toplevel) positions in the itemlist.
     */
    public int numberOfMainPositions() {
    	
        int numberOfMainPositions = 0;
        ItemSalesDoc item;
      
        for (int i = 0; i < size(); i++) {
            item = get(i);
            if ((item.getParentId()==null) || (item.getParentId().getIdAsString().equals(""))) {
                numberOfMainPositions++;
            }
        }
        return numberOfMainPositions;
    }


    /**
     * Returns true if this list contains the specified element.
     *
     * @param value value whose presence in this list is to be tested.
     * @return <code>true</code> if this list contains the specified value.
     */
    public boolean contains(ItemSalesDoc value) {
        return contains((ItemBase)value);
    }

    /**
     * Returns true if this list contains the specified element.
     *
     * @param value value whose presence in this list is to be tested.
     * @return <code>true</code> if this list contains the specified value.
     */
    public boolean contains(ItemData value) {
        return contains((ItemBaseData) value);
    }


    /**
     * Returns an array containing all of the elements in this list in proper
     * sequence;
     *
     * @return  an array containing the elements of this list.
     */
    public ItemSalesDoc[] toArray() {
        int size = size();

        ItemSalesDoc[] a = new ItemSalesDoc[size];

        for (int i = 0; i < size; i++) {
            a[i] = get(i);
        }

        return a;
    }



}
