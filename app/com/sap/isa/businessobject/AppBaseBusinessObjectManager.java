/*****************************************************************************
	Class:        AppBaseBusinessObjectManager
	Copyright (c) 2008, SAP AG, Germany, All rights reserved.
	Author:
	Created:      06.03.2008
	Version:      1.0
*****************************************************************************/

package com.sap.isa.businessobject;


import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.maintenanceobject.businessobject.DynamicUIMaintenance;
import com.sap.isa.maintenanceobject.businessobject.DynamicUIMaintenanceAware;


/**
 * Central point to create and access Objects of the business logic. After
 * a request for a specific object, BusinessObjectManager checks if there is already
 * an object created. If so, a reference to this object is returned, otherwise
 * a new object is created and a reference to the new object is returned.
 * The reason for this class is, that the construction process of the businnes
 * logic objects may be quite complicated (because of some contextual
 * information) and most objects should only exist once per session.
 * To achieve this the <code>BusinessObjectManager</code>
 * is used. If you store a reference to this
 * object (for examle in a session context) you can get access to all other
 * business obejects using it.
 *
 * @author SAP
 * @version 1.0
 */
public class AppBaseBusinessObjectManager extends GenericBusinessObjectManager
	implements BackendAware, DynamicUIMaintenanceAware {


		/**
		 * Constant containing the name of this BOM
		 */
		public static final String APPBASE_BOM = "APPBASE-BOM";
		
		
		public AppBaseBusinessObjectManager() {}
		
		
		
		/**
		 * Creates a new IdMapper object. If such an object was already created, a
		 * reference to the old object is returned and no new object is created.
		 *
		 * @return referenc to a newly created or already existend IdMapper object
		 */
		public synchronized IdMapper createIdMapper() {
			return (IdMapper) createBusinessObject(IdMapper.class);
		}
    
		/**
		 * Returns a reference to an existing IdMapper object.
		 *
		 * @return reference to IdMapper object or null if no object is present
		 */
		public IdMapper getIdMapper() {
		  return (IdMapper) getBusinessObject(IdMapper.class);
		}

		/**
		 * Releases references to the IdMapper object.
		 */
		public synchronized void releaseIdMapper() {
		  releaseBusinessObject(IdMapper.class);
		}		
		
		/**
	     * Creates {@link com.sap.isa.user.businessobject.DynamicUIMaintenance} object. 
	     * 
	     * @return DynamicUIMaintenance created {@link com.sap.isa.maintenanceobject.businessobject.DynamicUIMaintenance} object
	     */
		public DynamicUIMaintenance createDynamicUIMaintenance(){
			return (DynamicUIMaintenance) createBusinessObject(DynamicUIMaintenance.class);
		}
		
		
		/**
	     * Retrieves {@link com.sap.isa.maintenanceobject.businessobject.DynamicUIMaintenance} object. 
	     * 
	     * @return DynamicUIMaintenance 
	     */
		public DynamicUIMaintenance getDynamicUIMaintenance(){
		  return (DynamicUIMaintenance) getBusinessObject(DynamicUIMaintenance.class);
		}
		
		/**
		 * Releases references to the DynamicUIMaintenance object.
		 */
		public synchronized void releaseDynamicUIMaintenance() {
		  releaseBusinessObject(DynamicUIMaintenance.class);
		}		
		
		
	}		