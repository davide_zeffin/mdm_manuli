/*****************************************************************************
    Class:        GenericSearchReturn
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.

    $Revision: #2 $
    $DateTime: 2004/01/28 10:41:18 $ (Last changed)
    $Change: 167577 $ (changelist)
    
*****************************************************************************/
package com.sap.isa.businessobject;

import com.sap.isa.backend.boi.appbase.GenericSearchReturnData;
import com.sap.isa.core.util.table.Table;

/**
 * Collection of all return data.  
 * <p>
 * Part of the generic search framework.
 * </p>
 *
 * @author SAP AG
 * @version 1.0
 *
 */

public class GenericSearchReturn 
        implements Cloneable, 
                   GenericSearchReturnData {

    protected Table countedDocTab;
	protected Table reqFieldsTab;
	protected Table documentsTab;
    protected Table messagesTab;
                   	
	/**
     * Sets the return table including the counted documents
	 * @param Table
	 */
	public void setReturnTableCountedDocuments(Table tab) {
		countedDocTab = tab;
	}

	/**
	 * Sets the return table including the requested fields
	 * @param Table
	 */
	public void setReturnTableRequestedFields(Table tab) {
		reqFieldsTab = tab;
	}

	/**
	 * Sets the return table including the selected documents
	 * @param Table
	 */
	public void setReturnTableDocuments(Table tab) {
		documentsTab = tab;
	}

	/**
	 * Returns the table including the counted documents
	 * @return Table  Containing the counted documents
	 */
	public Table getCountedDocuments() {
		return countedDocTab;
	}
	/**
	 * Returns the table including the requested fields
	 * @return Table  Containing the requested fields
	 */
	public Table getRequestedFields() {
		return reqFieldsTab;
	}
	/**
	 * Returns the table including the selected documents
	 * @return Table
	 */
	public Table getDocuments() {
		return documentsTab;
	}
    
    /**
     * Returns the table including the occured messages
     * @return Table  Containing the occured messages
     */
    public Table getReturnTableMessages() {
       return messagesTab;
    }
    
    /**
     * Returns the table including the occured messages
     * @return Table
     */
    public void setReturnTableMessages(Table tab) {
        messagesTab = tab;
    }
                   	
}