/*****************************************************************************
    Class:        GenericSearchSearchCommand
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author

    $Revision: #2 $
    $DateTime: 2004/02/05 10:04:12 $ (Last changed)
    $Change: 168920 $ (changelist)

*****************************************************************************/

package com.sap.isa.businessobject;

import com.sap.isa.backend.boi.appbase.GenericSearchBackend;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.core.logging.IsaLocation;

/**
 * Defines the search command necessary to retrieve a list of service documents
 * available.
 */
public class GenericSearchSearchCommand implements GenericSearchCommand {

    protected GenericSearchSelectOptions selOpt;
    protected String backendImplementation;
	private static final IsaLocation loc = IsaLocation.getInstance(GenericSearchSearchCommand.class.getName());
    /**
     * Creates a new search command for a Select Request
     *
     * @param Select options
     */
    public GenericSearchSearchCommand(GenericSearchSelectOptions selOpt) {
        this.selOpt = selOpt;
    }

    /**
     * Set the Backend Implementation which should be used
     * @param Backend Implementation
     */
    public void setBackendImplementation(String bemImpl) {
    	this.backendImplementation = bemImpl;
    }

    /**
     * Returns the Select Options
     *
     * @return select options
     *
     */
    public GenericSearchSelectOptions getSelectOptions() {
       return this.selOpt;
    }

    /**
     * Returns the instance content as String representation
     *
     * @return String Content as String
     */
    public String toString() {
        StringBuffer sb = new StringBuffer();
        return sb.toString();
    }
    
    
    /**
     * Perform the generic search in the backend.
     * 
     * @param bem With the backend object manager the search command get 
     *             access to the backend.
     * @return SearchResult 
     * @throws CommunicationException
     */
    public GenericSearchReturn performGenericSearch(BackendObjectManager bem)
        throws CommunicationException{
		final String METHOD = "performGenericSearch()";
		loc.entering(METHOD);
		GenericSearchReturn retData;
           
        try { 
        	if (backendImplementation != null) {
  			    GenericSearchBackend searchBE =
                    (GenericSearchBackend) bem.createBackendBusinessObject(backendImplementation, null);

				retData = new GenericSearchReturn();

                searchBE.performSearch(selOpt, retData);

        	} else {
				retData = null;
        	}

        }
        catch (BackendException e) {
            BusinessObjectHelper.splitException(e);
            return null; // never reached
        }
		finally {
			loc.exiting();
		}
    	return retData;
            
            
	}
	/**
	 * DON't USE THIS METHOD. ONLY performGenericSearch IS CORRECTLY IMPLEMENTED.
	 * 
	 * @param bem With the backend object manager the search command get 
	 *             access to the backend.
	 * @return <br>Always null</br> 
	 * @throws CommunicationException
	 */
	public SearchResult performSearch(BackendObjectManager bem)
		throws CommunicationException{
            
        return null;            
            
	}

    
}