/*****************************************************************************
    Class:        UserStatusProfileSearchCommand
    Copyright (c) 2002, SAP AG, Germany, All rights reserved.
    Author

    $Revision: #2 $
    $DateTime: 2003/10/01 16:07:42 $ (Last changed)
    $Change: 151875 $ (changelist)

*****************************************************************************/

package com.sap.isa.businessobject;

import com.sap.isa.backend.BackendTypeConstants;
import com.sap.isa.backend.boi.isacore.ShopBackend;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.table.Table;

/**
 * Defines the search command necessary to retrieve a list of service documents
 * available.
 */
public class UserStatusProfileSearchCommand implements SearchCommand {

    private String     language;
	static final private IsaLocation loc = IsaLocation.getInstance(UserStatusProfileSearchCommand.class.getName());

    /**
     * Creates a new search command for the user status procedure
     *
     * @param language which should be used
     */
    public UserStatusProfileSearchCommand(String language) {
        this.language = language;
    }

    /**
     * Returns the property language
     *
     * @return language
     *
     */
    public String getLanguage() {
       return this.language;
    }

    /**
     * Returns the instance content as String representation
     *
     * @return String Content as String
     */
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append(this.language);
        return sb.toString();
    }
    
    
    /**
     * Perform the search in the backend.
     * 
     * @param bem With the backend object manager the search command get 
     *             access to the backend.
     * @return SearchResult 
     * @throws CommunicationException
     */
    public SearchResult performSearch(BackendObjectManager bem)
        throws CommunicationException{
		final String METHOD = "performSearch()";
		loc.entering(METHOD);
        SearchResult result;
           
        try { 
            ShopBackend sbe =
                    (ShopBackend) bem.createBackendBusinessObject(BackendTypeConstants.BO_TYPE_SHOP, null);

            Table resultTable =
                sbe.readUserStatusProfile(this.getLanguage());


            result = new SearchResult(resultTable, null);

        }
        catch (BackendException e) {
            BusinessObjectHelper.splitException(e);
            return null; // never reached
        }
        finally {
        	loc.exiting();
        }

    	return result;
            
            
	}

    
}