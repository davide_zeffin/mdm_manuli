/*****************************************************************************
    Class:        SalesDocument
    Copyright (c) 2001, SAP AG, All rights reserved.
    Author:		  SAP 	
    Created:      6.6.2001
    Version:      1.0

    $Revision: #26 $
    $Date: 2003/05/06 $
*****************************************************************************/

package com.sap.isa.businessobject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


import com.sap.ecommerce.boi.loyalty.LoyaltyMembershipConfiguration;
import com.sap.isa.backend.boi.isacore.SalesDocumentBackend;
import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.ShipToData;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.order.HeaderBaseData;
import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.backend.boi.isacore.order.ItemBaseData;
import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.backend.boi.isacore.order.ItemListBaseData;
import com.sap.isa.backend.boi.isacore.order.ItemListData;
import com.sap.isa.backend.boi.isacore.order.SalesDocumentAndStatusData;
import com.sap.isa.backend.boi.webcatalog.pricing.PriceInfo;
import com.sap.isa.backend.boi.webcatalog.pricing.PriceType;
import com.sap.isa.businessobject.businessevent.AddToDocumentEvent;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businessobject.businessevent.CreateDocumentEvent;
import com.sap.isa.businessobject.businessevent.DropDocumentEvent;
import com.sap.isa.businessobject.businessevent.DropFromDocumentEvent;
import com.sap.isa.businessobject.businessevent.ModifyDocumentItemEvent;
import com.sap.isa.businessobject.header.HeaderBase;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.item.BasketTransferItem;
import com.sap.isa.businessobject.item.ItemBase;
import com.sap.isa.businessobject.item.ItemList;
import com.sap.isa.businessobject.item.ItemListBase;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.businessobject.lead.Lead;
import com.sap.isa.businessobject.negotiatedcontract.NegotiatedContract;
import com.sap.isa.businessobject.order.Basket;
import com.sap.isa.businessobject.order.CampaignListEntry;
import com.sap.isa.businessobject.order.CollectiveOrder;
import com.sap.isa.businessobject.order.Order;
import com.sap.isa.businessobject.order.PartnerList;
import com.sap.isa.businessobject.order.PartnerListEntry;
import com.sap.isa.businessobject.ordertemplate.OrderTemplate;
import com.sap.isa.businessobject.quotation.Quotation;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.catalog.AttributeKeyConstants;
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.ICatalog;
import com.sap.isa.catalog.boi.IFilter;
import com.sap.isa.catalog.boi.IQuery;
import com.sap.isa.catalog.boi.IQueryStatement;
import com.sap.isa.catalog.filter.CatalogFilterFactory;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.catalog.webcatalog.WebCatItemList;
import com.sap.isa.catalog.webcatalog.WebCatItemPrice;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.businessobject.event.BusinessEvent;
import com.sap.isa.core.businessobject.event.BusinessEventHandlerBase;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.DecimalPointFormat;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.isacore.IsaCoreInit;
import com.sap.isa.isacore.ItemHierarchy;
import com.sap.isa.payment.backend.boi.PaymentBaseTypeData;
import com.sap.isa.payment.businessobject.PaymentBaseType;
import com.sap.spc.remote.client.object.IPCItem;


/**
 * Common superclass for all sales documents that work as a predecessor of
 * an order.
 *
 * @author SAP
 * @version 1.0
 */
public abstract class SalesDocument extends SalesDocumentBase
    implements SalesDocumentData, SalesDocumentAndStatusData {

    // some constants to enable correct display of values in the salesDocument
    // if we have a Java-Basket without forceIPCPricing we only have the netValue,
    // if we have forceIPCPricing selected we have the grossValue, the taxValue, the
    // netValue and the freightValue also
    // it depends on the returns of the calls to the methods isXXXValueAvailable() in 
    // this class
    public static final String IS_GROSS_VALUE_AVAILABLE     = "isGrossValueAvailable";
    public static final String IS_TAX_VALUE_AVAILABLE       = "isTaxValueAvailable";
    public static final String IS_NET_VALUE_AVAILABLE       = "isNetValueAvailable";
    public static final String IS_FREIGHT_VALUE_AVAILABLE   = "isFreightValueAvailable";

    protected SalesDocumentBackend backendService = null;
    protected WebCatInfo salesDocWebCat = null;
    protected boolean alreadyInitialized = false;
    private String campaignKey="";
    private String campaignObjectType="";
    private boolean isExternalToOrder;
    private boolean isAuctionRelated;
    private boolean isDocumentRecoverable;
    private List storedEvents = new ArrayList();
    private boolean closevalue = false;
    protected List shipToList = new ArrayList();
    protected ShipCond shipCond;
    protected boolean isAlternativeProductAvailable;
    protected ArrayList lastEnteredCVVS = new ArrayList(0);
    protected IsaCoreInit isaCoreInit = IsaCoreInit.getInstance();

    /**
     * Reference to the businessEventHandler.
     */
    protected BusinessEventHandler businessEventHandler;

    protected boolean isDeterminationRequired;
	/**
	 * campaign related information
	 */
	protected HashMap campaignDescriptions = new HashMap();
	protected HashMap campaignTypeDescriptions = new HashMap();
	
	/**
	 * payment related information
	 */
	private PaymentBaseType payType;	
	//auction related 

	/**
	 * This attribute actually holds the ID from auction side for the published 
	 * list
	 */
	private String auctionId = null;
	//end auction related
		
	protected static Table cardType;   	
	
	
	// Identifier Mapping 
	IdMapper idMapper = null;
	
	
	/**
     * Sets whether or not the order has to be maintained as an
     * external document to this one.
     *
     * @param  isExternalToOrder <code>true</code> indicates that the order
     *         is maintained
     *         external to this document, <code>false</code> indicates
     *         that the order lies on the same system.
     */
    public void setExternalToOrder(boolean isExternalToOrder) {
        this.isExternalToOrder = isExternalToOrder;
    }

    /**
     * Indicates whether or not the order has to be maintained as an
     * external document to this one.
     *
     * @return <code>true</code> indicates that the order is maintained
     *         external to this document, <code>false</code> indicates
     *         that the order lies on the same system.
     */
    public boolean isExternalToOrder() {
        return isExternalToOrder;
    }

    /**
     * Sets the property isDocumentRecoverable
     *
     * @param isDocumentRecoverable
     *         <code>true</code> indicates that the sales document
     *         can be recovered from the backend after an failure like
     *         loss of connection or system breakdown
     *         <code>false</code> indicates that the sales document
     *         can not be recovered from the backend after an failure like
     *         loss of connection or system breakdown
     */
    public void setDocumentRecoverable(boolean isDocumentRecoverable) {
        this.isDocumentRecoverable = isDocumentRecoverable;
    }

    /**
     * Indicates whether he sales documen can be recovered from the backend
     * after an failure like loss of connection or system breakdown
     *
     * @return <code>true</code> indicates that the sales document
     *         can be recovered from the backend after an failure like
     *         loss of connection or system breakdown
     *         <code>false</code> indicates that the sales document
     *         can not be recovered from the backend after an failure like
     *         loss of connection or system breakdown
     */
    public boolean isDocumentRecoverable() {
        return isDocumentRecoverable;
    }

    /**
     * Searches the list of items for double entries of identical products (as
     * shown by the product GUID or the product id) and merges all
     * identical products together to one entry in the basket. The
     * amount of the items is summed up. This is only performed
     * if the unit of the items is identical, too.<br>
     * This is a convenience method using the <code>mergeIdenticalProducts</code>
     * method of the class <code>ItemList</code>.
     *
     * @param decimalPointFormat the number format used
     */
    public void mergeIdenticalProducts(DecimalPointFormat decimalPointFormat) {
        itemList.mergeIdenticalProducts(decimalPointFormat);
    }

    /**
     * Removes a Item from the document <b>and</b> from the underlying
     * backend storage.
     *
     * @param techKey Technical key of the item to be removed
     */
    public void removeItem(TechKey techKey) throws CommunicationException {
		final String METHOD = "removeItem()";
		log.entering(METHOD);
        // write some debugging info
        if (log.isDebugEnabled()) {
            log.debug("removeItem(): Removing item with technical key " + techKey);
        }
        
        //determine if item is related to auctions
		ItemSalesDoc item =  getItem(techKey);
		
		if (item == null) {
			log.debug("No item found for deletion");
			return;
		}
		
		if (item.hasAuctionRelation()) {
			log.debug("Item is an auction item, Search for all items of the auction");
			TechKey currAuctionGuid = item.getAuctionGuid();
            
            ArrayList itemsToDelete = new ArrayList(1);
			
			Iterator iteratorItems =  this.iterator();
		
			while (iteratorItems.hasNext()) {
			    item = (ItemSalesDoc)iteratorItems.next();
		
				if (item.getAuctionGuid()!= null && item.getAuctionGuid().equals(currAuctionGuid)) {
					if (log.isDebugEnabled()) {
						log.debug("Delete auction item " + item.getProduct());
					}
                    itemsToDelete.add(item.getTechKey());
				}
		    }
            
            for (int i= 0; i < itemsToDelete.size(); i++) {
                deleteItemInt((TechKey) itemsToDelete.get(i));
            }
		}
		else {
			deleteItemInt(techKey);
		}
    }

    /**
     * Deletes the item with the given Techkey
     * 
     * @param techKey
     * @throws CommunicationException
     */
	private void deleteItemInt(TechKey techKey)
		throws
			CommunicationException {
		ItemSalesDoc item;
		if (businessEventHandler != null && businessEventHandler.isActive()) {
		    Iterator iteratorItems =  this.iterator();
		
		    while (iteratorItems.hasNext()) {
		        item = (ItemSalesDoc)iteratorItems.next();
		
		        if (item.getTechKey()!= null && item.getTechKey().equals(techKey)) {
		
		            if (businessEventHandler.isReadMissingInfoFromBackend()) {
		                ItemList itemList = new ItemList();
		                itemList.add(item);
		                readItems(itemList);
		            }
		
		            businessEventHandler.fireDropFromDocumentEvent(
		                    new DropFromDocumentEvent(this, item));
		        }
		    }
		}
		
		try {
		    getBackendService().deleteItemInBackend(this, techKey);
		    isDirty = true;
		    header.setDirty(true);
		}
		catch (BackendException ex) {
		    BusinessObjectHelper.splitException(ex);
		}
		finally {
			log.exiting();
		}
	}

	/**
	 * Removes a Item from the document <b>and</b> from the underlying
	 * backend storage.
	 *
	 * @param techKey Technical key of the item to be removed
	 */
	public void removeItem(ItemSalesDoc item) throws CommunicationException {
		final String METHOD = "removeItem(ItemSalesDoc item)";
		log.entering(METHOD);
		if (businessEventHandler != null && businessEventHandler.isActive()) {
//			if (businessEventHandler.isReadMissingInfoFromBackend()) {
//				ItemList itemList = new ItemList();
//				itemList.add(item);
//				readItems(itemList);
//			}

			businessEventHandler.fireDropFromDocumentEvent(
					new DropFromDocumentEvent(this, item));
		}

		try {
			getBackendService().deleteItemInBackend(this, item.getTechKey());
			isDirty = true;
			header.setDirty(true);
		}
		catch (BackendException ex) {
			BusinessObjectHelper.splitException(ex);
		}
		finally {
			log.exiting();
		}
	}
    /**
     * Deletes an Item(s) from the preOrderSalesDocument.
     *
     * @param techKeys Technical keys of the items to be canceld
     */
    public void deleteItems(TechKey[] techKeys) throws CommunicationException {
		final String METHOD = "deleteItems()";
		log.entering(METHOD);
        if (techKeys == null || techKeys.length == 0) {
            log.debug("techKeys is null or contains no entries. Exit method");
            return;
        }
		// write some debugging info
		 if (log.isDebugEnabled()) {
			 log.debug("deleteItems(): techKeys = " + techKeys);
		 }
        if (businessEventHandler != null && businessEventHandler.isActive()) {
            ItemList itemList = new ItemList();
            for (int i = 0; i < techKeys.length; i++) {
                ItemSalesDoc item = getItem(techKeys[i]);
                if (item != null && item.getTechKey()!= null) {
                    itemList.add(item);
                }
            }

            if (businessEventHandler.isReadMissingInfoFromBackend()) {
                readItems(itemList);
            }

            Iterator iter = itemList.iterator();
            while (iter.hasNext()) {
                ItemSalesDoc item = (ItemSalesDoc) iter.next();
                businessEventHandler.fireDropFromDocumentEvent(
                        new DropFromDocumentEvent(this, item));
            }
        }

        try {
            getBackendService().deleteItemsInBackend(this, techKeys);
            isDirty = true;
            header.setDirty(true);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally {
        	log.exiting();
        }
    }


    /**
     * Performs a pre check operation for the object. This operation is
     * perfromed by the backend system. All errors occuring during this
     * operation are added to the object itself and can be retrieved from
     * it.
     *
     * @param shop The actual shop
     */
    public boolean preCheck(Shop shop) throws CommunicationException {
		final String METHOD = "preCheck()";
		log.entering(METHOD);
        // write some debugging info
        if (log.isDebugEnabled()) {
            log.debug("preCheck(): shop = " + shop);
        }

        try {
            isDirty = true;
            header.setDirty(true);
            return getBackendService().preCheckInBackend(this, shop);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally {
        	log.exiting();
        }
        return false; // never reached
    }

    /**
     * Retrieves informations for all initial items in the SalesDocument
     * from the underlying Catalog.
     * This method is only used from the BasketJDBC Backend Object and from the
     * mergeIdenticalProducts-method in this class
     *
     *@param shop The shop
     *@param forceIPCPricing Force the pricing via IPC, needed especially for the Brand-Owner-stuff
     *
     *@deprecated
     */
    public boolean populateItemsFromCatalog(ShopData shop, boolean forceIPCPricing) {
        return populateItemsFromCatalog(shop, forceIPCPricing, false);
    }
    
	/**
	 * Retrieves informations for all initial items in the SalesDocument
	 * from the underlying Catalog.
	 * This method is only used from the BasketJDBC Backend Object and from the
	 * mergeIdenticalProducts-method in this class
	 *
	 *@param shop The shop
	 *@param forceIPCPricing Force the pricing via IPC, needed especially for the Brand-Owner-stuff
	 *
	 *@deprecated
	 */
	public boolean populateItemsFromCatalog(ShopData shop, boolean forceIPCPricing, boolean preventIPCPricing) {
		return this.populateItemsFromCatalog(shop, forceIPCPricing, preventIPCPricing, true);
	}

    /**
     * Retrieves informations for all initial items in the SalesDocument
     * from the underlying Catalog.
     * This method is only used from the BasketJDBC Backend Object and from the
     * mergeIdenticalProducts-method in this class
     *
     *@param shop The shop
     *@param forceIPCPricing Force the pricing via IPC, needed especially for the Brand-Owner-stuff
     *@param wildcardsInProdName are product names with wildcards allowed?
     *
     */
    public boolean populateItemsFromCatalog(ShopData shop, boolean forceIPCPricing, 
                                            boolean preventIPCPricing, boolean wildcardsInProdName) {
		final String METHOD = "populateItemsFromCatalog()";
		log.entering(METHOD);
        ItemList        itemList  = null;
        ItemSalesDoc    item = null;
        WebCatItem      webCatItem = null;

        // tell them where we are
        if (null == shop) {
        	// WSa
            // shop = this.getHeader().getShop();
        }
        
        if (null == shop) {
            log.debug("populateItemsFromCatalog, shop == null");
            // nothing to do without a valid shop
            log.exiting();
            return false;
        }

        itemList = this.getItems();

        DecimalPointFormat decFormat =
                                DecimalPointFormat.createInstance(shop.getDecimalSeparator().charAt(0),
                                                                    shop.getGroupingSeparator().charAt(0));

        // determine for all items, if information
        // must be retrieved from the catalog
        for (int i=0; i < itemList.size(); i++) {

            // new Item, or changed quantity, or item was not found last tim ?
            item = itemList.get(i);

            double quant = -1.0;
            if (item.getQuantity() != null && item.getQuantity().trim().length() > 0) {
                try {
                    quant = decFormat.parseDouble(item.getQuantity().trim());
                }
                catch (ParseException ex) {
                    log.debug(ex);
                }
                finally {
                    String maxQuant = isaCoreInit.getMaxQuantJavaBasket();
            
                    // the isaCoreInit guarantees a valid value in maxQuant
                    double maxQuantValue = Double.parseDouble(maxQuant);
                    if (quant <= 0.0 || quant > maxQuantValue) {
                        String[] args = new String[3];
                        args[0] = item.getQuantity().trim();
                        args[1] = "0";
                        args[2] = decFormat.format(maxQuantValue);
                        
                        Message msg = new Message(Message.ERROR, "javabasket.invalidquantity", args, "");
                        item.setQuantity("1");
                        item.addMessage(msg);
                    }
                }
            }
            else {
                item.setQuantity("1");
            }

            if (item.getTechKey().isInitial() || item.getTechKey().getIdAsString().length() == 0 // new
               || item.getOldQuantity()== null || !item.getQuantity().trim().equalsIgnoreCase(item.getOldQuantity().trim()) // changed Quantity
               || ((item.getProductId() == null || item.getProductId().getIdAsString().length() == 0) && // item not found last time
                    !item.isDataSetExternally()  || item.isPtsItem()) ||
			// note upport 1342218  
			(item.getNetValue() == null || item.getNetValue().equals("") 
			|| item.getNetValueWOFreight() == null || item.getNetValueWOFreight().equals("") 
			|| item.getNetPrice() == null || item.getNetPrice().equals(""))// price not yet calculated or deleted due to campaign assignment
			// end note upport 1342218
			) {
                log.debug("populateItemsFromCatalog: Item=" + ((item.getProductId() == null || item.getProductId().toString().length() == 0)? item.getProduct(): item.getProductId().toString()));

                if (item.getTechKey() == null ||
                    item.getTechKey().isInitial() ||
                    item.getTechKey().getIdAsString().length() == 0) {

                    // create the items itemGuid = TechKey first
                    TechKey guid = TechKey.generateKey();
                    item.setTechKey(guid);
                    // remove the catalogitem coming from outside, we are not allowed to use this,
                    // because this might be coming from the catalog and still used there. So changing
                    // prices and quantity might screw up the catalog.
                    item.setCatalogProduct(null);
                    log.debug("populateItemsFromCatalog, created TechKey for item : " + item.getProduct());
                }

                if (!item.isDataSetExternally()) {
                    item.setNetValue(""); /* b2b */
                    item.setNetValueWOFreight("");
                    item.setNetPrice(""); /* b2c */
                    item.setCurrency("");
                }

                if (shop.isInternalCatalogAvailable() &&  // if there is no catalog available searching makes no sense
                    (!item.isDataSetExternally() ||       // only retrieve data, if item data is not set externally
                    (forceIPCPricing && shop.isAllProductInMaterialMaster()))) {  // or if all products are known in the backend

                    if (item.getCatalogProduct() == null || 
                        item.getCatalogProduct().getCatalogItem().getPossibleUOMs() == null || 
                        item.getCatalogProduct().getCatalogItem().getPossibleUOMs().length == 0) {
                        // retrieve data for the order item for the first time
                        webCatItem = searchItemInCatalog(item, shop, wildcardsInProdName);
                        if (webCatItem == null) {
                            continue;
                        }
                        item.setCatalogProduct(new Product(webCatItem));
                    }
                    else {
                        // don't search again, if WebCatItem is already known from former search
                        webCatItem = item.getCatalogProduct().getCatalogItem();
                        log.debug("WebCatItem for product " + item.getProduct() + " already known");
                    }

                    item.setProductId(new TechKey(webCatItem.getProductID()));
                    
                    // get extension data from webCatItem
                    if (webCatItem.getExtensionMap() != null) {
                        if (item.getExtensionMap() == null) {
                            // insert new extensions
                            item.setExtensionMap(new HashMap(webCatItem.getExtensionMap()));
                        }
                        else {
                            // add extensions to already existing extensions
                            // from the order.jsp or whereever
                            Map itemExtensionMap = item.getExtensionMap();
                            itemExtensionMap.putAll(webCatItem.getExtensionMap());
                        }
                    }

                    if (!item.isDataSetExternally()) {

                        // we need this explicitely when we performed the search using the TechKey
                        // when the item was added from the "vorgemerkte" products for instance
                        item.setProduct(webCatItem.getProduct());

                        // is it configurable ?
                        item.setConfigurable(
                            (webCatItem.getConfigurableFlag().equals(WebCatItem.PRODUCT_CONFIGURABLE_IPC) ||
                             webCatItem.getConfigurableFlag().equals(WebCatItem.PRODUCT_CONFIGURABLE_IPC_NOITEM))
                                ? true
                                : false);

                        // set the configType flag item - required for Grid product atleast
                        item.setConfigType(webCatItem.getAttributeByKey(AttributeKeyConstants.PRODUCT_CONFIGURABLE_FLAG));
 						
                        item.setPossibleUnits(webCatItem.getPossibleUOMs());
                        item.setDescription(webCatItem.getDescription());
                    
                        if (item.getUnit() == null || item.getUnit().length() == 0) {
                            item.setUnit(webCatItem.getUnit());
                        }
                    
                        // determine version - new version
                        // ask if price is list-price or scale-price --> from index
                        // else, from IPC --> force new pricing procedure for non-configurable products
                        // configurable products will be priced in SalesDocumentJDBC.internalUpdateItemList()
                        if (!forceIPCPricing) {
                            doPricing(webCatItem, item, shop, preventIPCPricing);
                        }
                    } //!item.isDataSetExternally()

                } //!item.isDataSetExternally() ||  shop.isAllProductInMaterialMaster()

            } // if

        } // for
		log.exiting();
        return true;
    }
    
	/**
	 * Searches for the given item in the Catalog and retrieves the related
	 * WebCatitem or null of no item was found. 
	 *
	 * @param item         The item, to search for
	 * @param theShop      The shop
	 * 
	 * @return WebCatItem  The found Catalog Item, or null if nothing was found
	 */
	public WebCatItem searchItemInCatalog(ItemSalesDoc item, ShopData shop) {
		return searchItemInCatalog(item, shop, true);
	}
    
    /**
     * Searches for the given item in the Catalog and retrieves the related
     * WebCatitem or null of no item was found. 
     *
     * @param item         The item, to search for
     * @param theShop      The shop
     * @param wildcardsInProdName are product names with wildcards allowed?
     * 
     * @return WebCatItem  The found Catalog Item, or null if nothing was found
     */
    public WebCatItem searchItemInCatalog(ItemSalesDoc item, ShopData shop, boolean wildcardsInProdName) {
		final String METHOD = "searchItemInCatalog()";
		log.entering(METHOD);
        WebCatItem      webCatItem = null;
        WebCatItemList  webCatItemList = null;
        String          requestedProduct = null;
        ICatalog        theCatalog = null;
        boolean         prodNameSearch = false;
        
        CatalogFilterFactory  myFactory = CatalogFilterFactory.getInstance();

        String backend = shop.getBackend();

        boolean isBackendR3 = backend.equals(Shop.BACKEND_R3)
                             || backend.equals(Shop.BACKEND_R3_40)
                             || backend.equals(Shop.BACKEND_R3_45)
                             || backend.equals(Shop.BACKEND_R3_46)
                             || backend.equals(Shop.BACKEND_R3_PI);

        try {
	        if (shop.isInternalCatalogAvailable()) {
	            // get the catalog
	            theCatalog = salesDocWebCat.getCatalog();
	        }
	        else {
	        	log.exiting();
	            return webCatItem;
	        }
	        
	        try {
	            IQueryStatement queryStmt = theCatalog.createQueryStatement();
	            IFilter search = null;
	            // first check if we can search by the techkey
	            if (null != item.getProductId()
	                && item.getProductId().getIdAsString().length() > 0) {
	                // don't forget to copy the OBJECT_ID to the item in this case
	                search =  myFactory.createAttrEqualValue("OBJECT_GUID", item.getProductId().toString());
	                // store productname, in case nothing is found for the given productguid
	                requestedProduct = item.getProduct();
	            } 
	            else if (item.getProduct() != null && item.getProduct().trim().length() > 0) {
                    if (log.isDebugEnabled()) {
                        log.debug("Search for product name: " + item.getProduct() + 
                                  " and allowWildcardsInProductName = " + wildcardsInProdName);
                    }
                    
					prodNameSearch = true;
                    
                    if (!wildcardsInProdName &&
                        (item.getProduct().indexOf("*") > -1 || item.getProduct().indexOf("?") > -1)) {
                    	log.debug("Product name contains wildcards, don't execute search");
                    	
						item.addMessage(new Message(Message.ERROR, "b2b.order.populitems.nonefound", new String[] {item.getProduct()}, ""));
                    
						return webCatItem;
                    }
                    else {
						// search for the product name, explicitly specify what fields to search in
						search = myFactory.createAttrEqualValue("OBJECT_ID", item.getProduct());
                    }
	            }
	            else {
	            	log.debug("Nothing to search for, skip entry");
	            	return webCatItem;
	            }

				if (getHeader().getShop() != null) { // only if RedemptionShop set these attributes
					if (item.isPtsItem()) {
                        log.debug("Search for redemption item: " + item.getProduct());
						search = myFactory.createAnd(search, myFactory.createAttrEqualValue("IS_PTS_ITEM", "X"));
					}
					else {
                        String loyRedemptionOrderType = getHeader().getShop().getRedemptionOrderType();
                        if (loyRedemptionOrderType != null && loyRedemptionOrderType.length()>0) {
							search = myFactory.createAnd(search, myFactory.createNot( myFactory.createAttrEqualValue("IS_PTS_ITEM", "X")));
                        }
					}
				}
	            
	            queryStmt.setStatement(search, null, null);    
	            IQuery query = theCatalog.createQuery(queryStmt);
	            if (log.isDebugEnabled()) {
	                log.debug("searchItemInCatalog: Query Stmt=" + queryStmt.toString());
	            }
	            webCatItemList = new WebCatItemList(salesDocWebCat, query, true, false, false);
	            // yes, we do want the accessories
	        } 
            catch (CatalogException e) {
	            log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "system.backend.exception", e);
	            return webCatItem;
	        } // try
	    
	        if (webCatItemList == null || webCatItemList.size() == 0) { // not found
	            // have we searche for ProductGuid and a product name was also given, but we
	            // found nothing, so show also the unknown prodduct name, to make the error message
	            // and the postion easier to interprt for the user
	            if (requestedProduct != null) { // only set if searched via productId
	                item.setProduct(requestedProduct);
	                // to generate a not found error message after the next 
	                // refresh the unknown productId must be deleted
	                item.setProductId(null);
	            } 
                else {
                    // this part intentionally left empty
                }
	                            
	            item.addMessage(new Message(Message.ERROR, "b2b.order.populitems.nonefound", 
                                       new String[] { item.getProduct()}, ""));
                                       
	            return webCatItem;
	        } 
            else if (webCatItemList.size() > 1) { // more than one found
	            // log it and take the first one
	            log.debug ("more than one item found in searchItemInCatalog");
	        }
	    
            // retrieve data for order item
            if (isBackendR3) {
                Iterator webCatItemListIter = webCatItemList.iterator();
                String theProductId = item.getProduct();
                if (theProductId == null || theProductId.equals("")) {
                    theProductId = item.getProductId().getIdAsString();
                    if (theProductId == null || theProductId.equals("")) {
                        log.error("Couldn't get ID of SalesDocItem, populate will fail!");
                    }
                }
            
                // see longer comment below
                WebCatItem savedItem = null;
                WebCatItem savedItemClose = null;
				// check if we are using the quicksearch entry and if we have more than one result
				if (item.getDescription() == null && webCatItemList.size() > 1) {
					// we are in quicksearch, check if one of the retrieved product ids equals the entred one
					String enteredProdId = theProductId.toUpperCase();
                while (webCatItemListIter.hasNext()) {
                    WebCatItem theWebCatItem = (WebCatItem) webCatItemListIter.next();
						if (theWebCatItem.getProduct().toUpperCase().equals(enteredProdId)) {
							return theWebCatItem;
						}
					}
					// no product has been found that exactly matches the Product id entered by the user
					// in this case we return the first found product from the webCatItemList
					return webCatItemList.getItem(0);
				} 
				else {
					while (webCatItemListIter.hasNext()) {
						WebCatItem theWebCatItem = (WebCatItem) webCatItemListIter.next();
                        
                        // if the item comes from the catalog we have a description and an unit
                        // they must be the same as the description and the unit in the 
                        // webCatItem to ensure that we really have the correct item
                        String webCatItemDescription = theWebCatItem.getDescription();
                        String webCatItemUnit = theWebCatItem.getUnit();
                
                        String itemDescription = item.getDescription();
                        String itemUnit = item.getUnit();
                
                        // to check if the item was put into the basket
                        // from the catalog or by quick order entry 
                        // we compare the description and the unit of the item also
                        // BUT: if we want to copy an item from a recovered 
                        // orderTemplate, meanwhile the description of the item can 
                        // have changed due to typos or a more detailed description
                        // so we save the first found webCatItem here to return this if
                        // we don't find a matching description/unit
                        savedItem = theWebCatItem;
                        closevalue = false;
                        if (itemUnit.equals(webCatItemUnit)) {   
                            // Abhishek:Taking the close value after matching productid and Unit
                            closevalue = true;
                            savedItemClose = theWebCatItem;
                            // If the item description is null then return this value
                            if (itemDescription == null ||
                                itemDescription.trim().length() < 1 ||
                                itemDescription.equals(webCatItemDescription)) {
                                return theWebCatItem;
                            }
                        }
					}  // endwhile
				}
            
                // Abhishek : return the close value with the matching
                // productid , UOM and description in case it is not null
                // if the description is null then also return the close
                // catalog item with the matching productid and UOM
                if (closevalue) {
                    return savedItemClose;
                }
                // if we reach this point, we didn't find a suitable item
                // so check if we found a sufficient good looking one and return this
                else {
                    if (savedItem != null) {
                        return savedItem;
                    }
                }
            
                // no product found with the given material ID
                if (requestedProduct != null) {
                    // only set if searched via productId
                    item.setProduct(requestedProduct);
                    // to generate a not found error message after the next 
                    // refresh the unknown productId must be deleted
                    item.setProductId(null);
                }

                item.addMessage(new Message(Message.ERROR, "b2b.order.populitems.nonefound", new String[] { item.getProduct()}, ""));
                    
                return webCatItem;
            }
            else { //CRM
                if (webCatItemList.size() > 1 && prodNameSearch) {
                    log.debug("search best match in found products");
                    
                    Iterator webCatItemListIter = webCatItemList.iterator();
                    String theProductId = item.getProduct();
                    boolean found = false;
            
                    // see longer comment below
                    WebCatItem savedItem = null;
                    while (webCatItemListIter.hasNext()) {
                        WebCatItem theWebCatItem = (WebCatItem) webCatItemListIter.next();
                        
                        if (theProductId.equals(theWebCatItem.getProduct())) {
                            webCatItem = theWebCatItem;
                            found = true;
                            break;
                        }
                    }
                    
                    if (!found) {
                        webCatItem = webCatItemList.getItem(0);
                    }
                }
                else {
                    webCatItem = webCatItemList.getItem(0);
                }
                
            } // if (isBackendR3)
	    }
        finally {
    		log.exiting();
    	}
        
        return webCatItem;
    }
    

    /**
     * just extracted the pricing stuff into a separate method
     */
    private void doPricing(WebCatItem webCatItem, ItemSalesDoc item, ShopData shop, boolean preventIPCPricing) {
        PriceInfo[]     itemPriceInfos = null;
        PriceInfo       itemPriceInfo;
        WebCatItemPrice itemPrice;

        //  get all Prices that are available for the item
        webCatItem.setItemPrice(null);
        webCatItem.setQuantity(item.getQuantity().trim());
        webCatItem.setUnit(item.getUnit().trim());
        //itemPrice = webCatItem.readItemPrice();
		itemPrice = webCatItem.retrieveItemPrice();
        if (itemPrice != null) {
            itemPriceInfos = itemPrice.getAllPriceInfos();
        }

        int noOfInfos = 0;

        if (itemPriceInfos != null) {
            noOfInfos = itemPriceInfos.length;
        }

        if (noOfInfos == 0) {
            log.debug("populateItemsFromCatalog: itemPriceInfo : NULL !!!");
        }
		
		//This will be executed only when both List and IPC Price are
		//allowed in the shop.
		if(shop.isListPriceUsed() && shop.isIPCPriceUsed()){
             
			//Extra parameter in basket, if set to true List prices are to be used
			if(!preventIPCPricing){
				log.debug("populateItemsFromCatalog: DUAL Price : ON !!!");
				//Get the PriceInfo object containing IPC Price
				itemPriceInfo = itemPrice.getIPCPriceInfo();				
				PriceType ipcPriceType = itemPriceInfo.getPriceType();
                
                item.setCurrency(itemPriceInfo.getCurrency());
				// the price is comin' from an external pricing engine, IPC ...
				// so we always get a net value, the price for a product
				// multiplied with the quantity
				// only get the price here if we don't have a configurable product
				if (!item.isConfigurable()) {
					if (ipcPriceType.equals(PriceType.NET_VALUE_WITHOUT_FREIGHT)) {
						item.setNetValueWOFreight(itemPriceInfo.getPrice());
					}
					else { // everything else is asssumed, to be the net value
						item.setNetValue(itemPriceInfo.getPrice());
						log.debug("populateItemsFromCatalog: DUAL Price IPC Price: !!!"+itemPriceInfo.getPrice());
					}
				}
			}
			else {				
				for (int j=0; j < noOfInfos; j++) {
					 itemPriceInfo = itemPriceInfos[j];
					 PriceType listPriceType = itemPriceInfo.getPriceType();
					 //This may be a PricInfo object of type IPC: beware
					 if (listPriceType!=PriceType.LIST_VALUE || listPriceType!=PriceType.SCALE_VALUE)
						 continue;  					
					 item.setCurrency(itemPriceInfo.getCurrency());	
					 if (itemPrice.isMultipleAndScalePrice()) {		
						 determineScalePrice(item, itemPriceInfos, shop);
						 log.debug("populateItemsFromCatalog: DUAL Price SCAL Prices: !!!");
						 break;		
					 }
					 else{
						 // Catalog delivers always netPrice right now
						 item.setNetPrice(itemPriceInfo.getPrice());
						 log.debug("populateItemsFromCatalog: DUAL Price LIST Prices: !!!");
						/* b2c and b2b*/						
					}
				}
			}				
			return;
		}

        for (int k = 0; k < noOfInfos; k++) {

            itemPriceInfo = itemPriceInfos[k];
            PriceType priceType = itemPriceInfo.getPriceType();

            // if(preventIPCPricing) priceType = PriceType.LIST_VALUE;

            if (log.isDebugEnabled()) {
                log.debug("populateItemsFromCatalog: itemPriceInfo : " + itemPriceInfo.toString());
            }

            item.setCurrency(itemPriceInfo.getCurrency());

            if (priceType.equals(PriceType.LIST_VALUE)
                || priceType.equals(PriceType.SCALE_VALUE)) {

                // the price is comin' from the catalog's index
                // so we always get a net price, the price for one single product
                if (!itemPrice.isScalePrice()) {
                    // Catalog delivers always netPrice right now
                    item.setNetPrice(itemPriceInfo.getPrice());
                    /* b2c and b2b*/
                }
                else {
                    // scale prices; in this case itemPriceInfos only contains entries for scale prices
                    determineScalePrice(item, itemPriceInfos, shop);

                    // we have to break the outer loop, because all other PriceInfos will be scale price informations,
                    // we already have processed
                    break;

                } // else (itemPrice.isScalePrice()
            }
            else {
                // the price is comin' from an external pricing engine, IPC ...
                // so we always get a net value, the price for a product
                // multiplied with the quantity
                // only get the price here if we don't have a configurable product
                if (!item.isConfigurable() || preventIPCPricing) {
                    if (priceType.equals(PriceType.NET_VALUE_WITHOUT_FREIGHT)) {
                        item.setNetValueWOFreight(itemPriceInfo.getPrice());
                    }
                    else { // everything else is asssumed, to be the net value
                        item.setNetValue(itemPriceInfo.getPrice());
                        item.setNetPrice(itemPriceInfo.getPrice());
                    }
                }
            }
        }
    }

    /**
     * Determine the Scale price for the given item from the given
     * list of price infos
     *
     * @param item the ItemSalesDoc the scale price should be retrieved for
     * @param itemPriceInfos list auf PriceInfos, that where retrieved from the
     * catalog
     * @param shop the shop we are in
     */
    protected void determineScalePrice(ItemSalesDoc item, PriceInfo[] itemPriceInfos, ShopData shop) {

        final int noScale   = 0;
        final int fromScale = 1;
        final int uptoScale = 2;
        int priceScaleType  = noScale;

        DecimalPointFormat decFormat;

        PriceInfo priceInfo = null;
        String usePrice     = null;

        for (int j = 0; j < itemPriceInfos.length; j++) {

             priceInfo = itemPriceInfos[j];

             // under normal circumstances all prices in the priceinfo array should be scale prices
             // but related to customer modifications this might be changed
             // so check type again
             if (priceInfo.getPriceType().equals(PriceType.SCALE_VALUE)) {
                // what's in here ?
                decFormat = DecimalPointFormat.createInstance(shop.getDecimalSeparator().charAt(0),
                                                              shop.getGroupingSeparator().charAt(0));

                double itemQuant    = 0;
                double priceQuant   = 0;

                try {
                    itemQuant    = decFormat.parseDouble(item.getQuantity());
                    priceQuant   = decFormat.parseDouble(priceInfo.getQuantity());
                }
                catch (ParseException ex) {
                    if (log.isDebugEnabled()) {
                        log.debug(ex);
                    }
                }

                // because there is no flag at the product that can tell which
                // type of scale-price is used we have to decide depending on other
                // remarks
                // there can be two different types of price-scales
                // price-scales startig from 0 and price-scales starting from a value for
                // the quantity which is greater than 0

                // corresponding CRM fuba to call - CRM_ISA_PRICESETTINGS_READ (??)

                if (priceScaleType == noScale) {
                    // assume that this is the first entryx of a scale-price
                    // price-array
                    // check the value of the quantity
                    if (priceQuant > 0) {
                        //  von-Staffel
                        priceScaleType = uptoScale;
                    }
                    else {
                        // bis-Staffel
                        priceScaleType = fromScale;
                    }
                }

                if (priceScaleType == uptoScale) {
                    // up to scale
                    if (itemQuant <= priceQuant) {
                        usePrice = priceInfo.getPrice();
                        break;
                    }
                }
                 else {
                    // from scale
                    if (itemQuant >= priceQuant) {
                        usePrice = priceInfo.getPrice();
                    }
                    else {
                        break;
                    }
                }  // endif
            }  // if (priceInfo.getPriceType()...
        }  // endfor

        // so now we have the price for one single item depending
        // on the price scale
        if (usePrice != null) {
            item.setNetPrice(usePrice);
        }
        else {
            if (log.isDebugEnabled()) {
                log.debug("no valid scaleprice found");
                }
        }

    }


    /**
     * Update the object's representation in the underlying storage (backend
     * layer).
     *
     * @deprecated This method will be replaced by <code>update(BusinessPartnerManager bupama)</code>
     * @see #update(BusinessPartnerManager bupama)
     */
    public void update() throws CommunicationException {
        //Dummy to be able to call the standard method
        BusinessPartnerManager bupama = null;

        update(bupama);
    }

    /**
     * Update the object's representation in the underlying storage (backend
     * layer). The method also checks for changes in the businesspartner list
     *
     * @param bupama the business partner manager
     */
    public void update(BusinessPartnerManager bupama) throws CommunicationException {
		final String METHOD = "update()";
		log.entering(METHOD);
        checkDocumentEvent();
        checkBusinessPartners(bupama);

        // write some debugging info
        if (log.isDebugEnabled()) {
            log.debug("update():  bupama: " + bupama);
        }

        try {
            getBackendService().updateInBackend(this);
            isDirty = true;
            header.setDirty(true);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
		finally {
			log.exiting();
		}
        fireDocumentEvents();

    }

    /**
     * Update <b>only</b> the header of the object in the underlying storage (backend
     * layer) using the given user and shop information.  This method does not update
     * any item information. The
     * use is limited to cases where performance considerations require
     * only to update the necessary part of the document in the underlying
     * storage,
     *
     * @param shop The shop
     */
    public void updateHeader(ShopData shop) throws CommunicationException {
        //Dummy to be able to call the standard method
        BusinessPartnerManager bupama = null;

        updateHeader(bupama, shop);
    }

    /**
     * Update <b>only</b> the header of the object in the underlying storage (backend
     * layer) using the given user and shop information.  This method does not update
     * any item information. The
     * use is limited to cases where performance considerations require
     * only to update the necessary part of the document in the underlying
     * storage,
     *
     * @param bupama the business partner manager
     * @param shop The shop
     */
    public void updateHeader(BusinessPartnerManager bupama, ShopData shop) throws CommunicationException {
		final String METHOD = "updateHeader()";
		log.entering(METHOD);
        // write some debugging info
        if (log.isDebugEnabled()) {
            log.debug("updateHeader(): shop = " + shop + " bupama: " + bupama);
        }

        checkBusinessPartners(bupama);

        try {
            getBackendService().updateHeaderInBackend(this, shop);
            header.setDirty(true);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally {
        	log.exiting();
        }

    }

    /**
     * Update the Status elements of the object's representation in the underlying storage (backend
     * layer).
     *
     */
    public void updateStatus() throws CommunicationException {
		final String METHOD = "updateSattue()";
		log.entering(METHOD);
        // write some debugging info

        try {
            getBackendService().updateStatusInBackend(this);
            isDirty = true;
            header.setDirty(true);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally {
        	log.exiting();
        }

    }
    /**
     * Update <b>only</b> the header of the object in the underlying storage (backend
     * layer) using the given user and shop information.  This method does not update
     * any item information. The
     * use is limited to cases where performance considerations require
     * only to update the necessary part of the document in the underlying
     * storage
     *
     * @deprecated This method will be replaced by <code>updateHeader(BusinessPartnerManager bupama)</code>
     * @see #updateHeader(BusinessPartnerManager bupama)
     */
    public void updateHeader() throws CommunicationException {

        //Dummy to be able to call the standard method
        BusinessPartnerManager bupama = null;

        updateHeader(bupama);
    }

    /**
     * Update <b>only</b> the header of the object in the underlying storage (backend
     * layer) using the given user and shop information.  This method does not update
     * any item information. The
     * use is limited to cases where performance considerations require
     * only to update the necessary part of the document in the underlying
     * storage,
     *
     * @param bupama the business partner manager
     */
    public void updateHeader(BusinessPartnerManager bupama) throws CommunicationException {
		final String METHOD = "updateHeader()";
		log.entering(METHOD);
        // write some debugging info
        if (log.isDebugEnabled()) {
            log.debug("updateHeader(): bupama: " + bupama);
        }

        checkBusinessPartners(bupama);

        try {
            getBackendService().updateHeaderInBackend(this);
            header.setDirty(true);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally {
        	log.exiting();
        }

    }

    /**
     * Update the object's representation in the underlying storage (backend
     * layer) using the given user and shop information.
     *
     * @param shop The shop
     *
     * @deprecated This method will be replaced by <code>update(UserData user, BusinessPartnerManager bupama, ShopData shop)</code>
     * @see #update(UserData user, BusinessPartnerManager bupama, ShopData shop)
     */
    public void update(ShopData shop) throws CommunicationException {

        //Dummy to be able to call the standard method
        BusinessPartnerManager bupama = null;

        update(bupama, shop);
    }

    /**
     * Update the object's representation in the underlying storage (backend
     * layer) using the given user and shop information. In addition the business
     * partner list is checked for validity
     *
     * @param bupama the business partner manager
     * @param shop The shop
     */
    public void update(BusinessPartnerManager bupama, ShopData shop) throws CommunicationException {
		final String METHOD = "update()";
		log.entering(METHOD);
        // write some debugging info
        if (log.isDebugEnabled()) {
            log.debug("update(): shop = " + shop + " bupama: " + bupama);
        }

        checkDocumentEvent();
        checkBusinessPartners(bupama);

        try {
            getBackendService().updateInBackend(this, shop);
            isDirty = true;
            header.setDirty(true);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally {
        	log.exiting();
        }

        fireDocumentEvents();
    }


    /**
     * Determine the alternativ product data for the item with given itemsguids.
     * To determine alternative product data for an item, the productId must be
     * set to the product Guid of the alternative product.
     *
     * @param itemsGuidsToProcess  List of Guids of items, to process
     * @param bupama the business partner manager
     * @param shop The shop
     */
    public void replaceItemsByAlternativProduct(List itemGuidsToProcess)
        throws CommunicationException {
		final String METHOD = "replaceItemsByalternativeProduct()";
		log.entering(METHOD);
        // write some debugging info
        if (log.isDebugEnabled()) {
            log.debug(
                "updatePorductDtermination: itemGuidsToProcess: "
                    + itemGuidsToProcess.toString());
        }

        try {
            getBackendService().updateProductDetermination(this, itemGuidsToProcess);
            isDirty = true;
            header.setDirty(true);
        } catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally {
        	log.exiting();
        }
    }


    /**
     * This method checks for newly entered businesspartners and tries to
     * retrive the misiing data.
     * If a newly entered businesspartner can not be foudn an error message
     * is added to the document.
     *
     * @ param  bupama teh business partner manager
     */

    public void checkBusinessPartners(BusinessPartnerManager bupama) {
		final String METHOD = "checkBusinessPartners()";
		log.entering(METHOD);
        if (bupama == null) {
        	if (log.isDebugEnabled())
        		log.debug("BusinessPartnerManager is null");
        	log.exiting();
            return;
        }

        Map.Entry listEntry = null;
        String bpRole = null;
        PartnerListEntry partnerEntry = null;
        Iterator bpIt = header.getPartnerList().getList().entrySet().iterator();

        while (bpIt.hasNext()) {
            listEntry = (Map.Entry) bpIt.next();
            bpRole = (String) listEntry.getKey();
            partnerEntry = (PartnerListEntry) listEntry.getValue();
            if (partnerEntry.getPartnerTechKey() == null ||
                partnerEntry.getPartnerTechKey().getIdAsString().length() == 0) {
                // newly entered business  partner, try to find Techkey
                BusinessPartner bupa = bupama.getBusinessPartner(partnerEntry.getPartnerId());
                // Anything found ?
                if (bupa != null) {
                    partnerEntry.setPartnerTechKey(bupa.getTechKey());
                    partnerEntry.setPartnerId(bupa.getId());
                }
                else {
                    if (bpRole.equals(PartnerFunctionData.SOLDTO)) {
                        String[] params = {partnerEntry.getPartnerId()};
                        Message msg = new Message(Message.ERROR, "b2b.order.display.invalidsoldto", params , "");
                        this.addMessage(msg);
                    }
                }
            }
        }
        log.exiting();
    }


    /*
     * a private method to check, if an business must fired
     *
     */
    private void checkDocumentEvent() {

        if (businessEventHandler != null && businessEventHandler.isActive()) {
            Iterator iteratorItems =  this.iterator();

            while (iteratorItems.hasNext()) {
                ItemSalesDoc item = (ItemSalesDoc)iteratorItems.next();

                if (item.getTechKey() == null || item.getTechKey().getIdAsString().equals("")) {
                    AddToDocumentEvent event = new AddToDocumentEvent(this, item);
                    storedEvents.add(event);
                }
                else {
					if (item.getQuantity() == null || item.getQuantity().trim().equals("") || item.getQuantity().trim().equals("0")) {
						DropFromDocumentEvent event = new DropFromDocumentEvent(this, item);
						storedEvents.add(event);
					}
					else if (!item.getQuantity().equals(item.getOldQuantity())) {
                        ModifyDocumentItemEvent event = new ModifyDocumentItemEvent(this, item);
                        storedEvents.add(event);
                    }

                }
            }
        }

    }

    /**
     * a private method to fire the stored business events
     *
     */
    private void fireDocumentEvents() throws CommunicationException {


        if (businessEventHandler != null && businessEventHandler.isActive()
                && !storedEvents.isEmpty()) {

            if (businessEventHandler.isReadMissingInfoFromBackend()) {
                this.readAllItems();
				isDirty = true;
				header.setDirty(true);
            }

            Iterator iter = storedEvents.iterator();

            while (iter.hasNext()) {
                BusinessEvent event = (BusinessEvent) iter.next();
                if (event instanceof AddToDocumentEvent) {
                    TechKey techKey = ((AddToDocumentEvent)event).getItem().getTechKey();
                    if (techKey != null && techKey.getIdAsString().length() > 0) {
                        ItemSalesDoc item = ((ItemList)itemList).get(techKey);
                        if (item != null) {
                            businessEventHandler.fireAddToDocumentEvent(new AddToDocumentEvent(this,item));
                        }
                    }
                }
                else if (event instanceof ModifyDocumentItemEvent) {
                    ItemSalesDoc oldItem = ((ModifyDocumentItemEvent)event).getItem();
                    TechKey techKey = oldItem.getTechKey();
                    if (techKey != null && techKey.getIdAsString().length() > 0) {
                        ItemSalesDoc item = ((ItemList)itemList).get(techKey);
						if (item != null) {
	                        item.setOldQuantity(oldItem.getOldQuantity());
                            businessEventHandler.fireModifyDocumentItemEvent(new ModifyDocumentItemEvent(this,item));
                        }
                    }
                }
				else if (event instanceof DropFromDocumentEvent) {
					businessEventHandler.fireDropFromDocumentEvent((DropFromDocumentEvent)event);
				}
            }
            storedEvents.clear();
        }
    }




    /**
     * Destroy the data of the document (items and header) in the backend
     * representation. After a
     * call to this method, the object is in an undefined state. You have
     * to call init() before you can use it again.<br>
     * This method is normally called before removing the BO-representation
     * of the object using the BOM.
     */
    public void destroyContent()  throws CommunicationException  {

        // write some debugging info
		final String METHOD = "destroyContent()";
		log.entering(METHOD);

        alreadyInitialized = false;
        try {
            getBackendService().emptyInBackend(this);
            isDirty = true;
            header.setDirty(true);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally {
        	log.exiting();
        }
    }

    /**
     * Removes all items from the object in the underlying storage and the
     * BO-layer. A call to this method implicitly re-initalizes the object.
     * If you empty it using this method, it can be used again
     * in subsequent calls.
     *
     * @param shop The actual shop
     * @param user The current user
     *
     * @deprecated This method will be replaced by <code>emptyContent(Shop shop,
     *                                                                BusinessPartnerManager bupama,
     *                                                                WebCatInfo salesDocWebCat) </code>
     * @see #emptyContent(Shop shop,
     *                    BusinessPartnerManager bupama,
     *                    WebCatInfo salesDocWebCat)
     */
    public void emptyContent(Shop shop, WebCatInfo salesDocWebCat) throws CommunicationException {

        //Dummy to be able to call the standard method
        BusinessPartnerManager bupama = null;

        emptyContent(shop, bupama, salesDocWebCat);
    }

    /**
     * Removes all items from the object in the underlying storage and the
     * BO-layer. A call to this method implicitly re-initalizes the object.
     * If you empty it using this method, it can be used again
     * in subsequent calls.
     *
     * @param shop The actual shop
     * @param bupama the business partner manager
     * @param salesDocWebCat The actual catalog
     */
    public void emptyContent(Shop shop, BusinessPartnerManager bupama, WebCatInfo salesDocWebCat) throws CommunicationException {
		final String METHOD = "emptyContent()";
		log.entering(METHOD);
        // write some debugging info
        if (log.isDebugEnabled()) {
            log.debug("emptyContent(): shop = " + shop + ", bupama = " + bupama);
        }

        try {
			if (businessEventHandler != null && businessEventHandler.isActive()) {
				DropDocumentEvent event = new DropDocumentEvent(this);
				businessEventHandler.fireDropDocumentEvent(event);
			}
            alreadyInitialized = false;
            getBackendService().emptyInBackend(this);
            init(shop, bupama, getHeader().getPartnerList(), salesDocWebCat);
            isDirty = true;
            header.setDirty(true);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally {
        	log.exiting();
        }
    }

    /**
     * Initializes the object according to the given shop and user. The state of
     * the document is dropped as an result of calling this method and
     * after that the necessary backend operations are performed to
     * create a representation of the document in the underlying storage.
     *
     * @param shop Shop used for this basket
     * @param salesDocWebCat the actual catalog
     */
     public void init(Shop shop, WebCatInfo salesDocWebCat) throws CommunicationException {

        //Dummy to be able to call the standard method
        BusinessPartnerManager bupama = null;

        init(shop, bupama, getHeader().getPartnerList(), salesDocWebCat);
    }

	/**
	  * Initializes the object according to the given shop. The state of
	  * the document is dropped as an result of calling this method and
	  * after that the necessary backend operations are performed to
	  * create a representation of the document in the underlying storage.
	  *
	  * @param shop Shop used for this basket
	  * @param bupama the business partner manager
	  * @param partnerList list of business partners for the document
	  * @param salesDocWebCat the actual catalog
	  */
	  public void init(Shop shop, BusinessPartnerManager bupama,
					   PartnerList partnerList, WebCatInfo salesDocWebCat, 
					   String processType, CampaignListEntry campaignListEntry) throws CommunicationException {
		final String METHOD = "init()";
		log.entering(METHOD);
		 // write some debugging info
		 if (log.isDebugEnabled()) {
			 log.debug("init(): shop = " + shop + " bupama: " + bupama + ", processtype = " + processType);
		 }

		 if (shop.isInternalCatalogAvailable() && salesDocWebCat == null) {
			 throw( new IllegalArgumentException("Paramter salesDocWebCat can not be null!"));
		 }

		 clearData();
		 clearShipTos();
		 header.setPartnerList(partnerList);
		 header.setProcessType(processType);
		 getHeader().getAssignedCampaigns().addCampaign(campaignListEntry);

		 // check for valid entries
		 if (bupama != null) {
			 checkBusinessPartners(bupama);
		 }

		 synchronized (this) {
			 // remember the WebCatalog, because it may be needed
			 // by populateItemsFromCatalog()
			 this.salesDocWebCat = salesDocWebCat;

			 if (!alreadyInitialized) {
				 try {
					 alreadyInitialized = true;
					 getBackendService().createInBackend(shop, this);
				 }
				 catch (BackendException ex) {
					 BusinessObjectHelper.splitException(ex);
				 }

				 if (businessEventHandler != null && businessEventHandler.isActive()) {
					 CreateDocumentEvent event = new CreateDocumentEvent(this);
					 businessEventHandler.fireCreateDocumentEvent(event);
				 }
				 isDirty = true;
				 header.setDirty(true);
			 }
		 }
		 log.exiting();
	 }
	
    /**
     * Initializes the object according to the given shop and user. The state of
     * the document is dropped as an result of calling this method and
     * after that the necessary backend operations are performed to
     * create a representation of the document in the underlying storage.
     *
     * @param shop Shop used for this basket
     * @param bupama the business partner manager
     * @param partnerList list of business partners for the document
     * @param salesDocWebCat the actual catalog
     */
     public void init(Shop shop, BusinessPartnerManager bupama,
                      PartnerList partnerList, WebCatInfo salesDocWebCat) throws CommunicationException {
		final String METHOD = "init()";
		log.entering(METHOD);
        // write some debugging info
        if (log.isDebugEnabled()) {
            log.debug("init(): shop = " + shop + ",  bupama: " + bupama);
        }
		try {
		
	        if (shop.isInternalCatalogAvailable() && salesDocWebCat == null) {
	            throw( new IllegalArgumentException("Paramter salesDocWebCat can not be null!"));
	        }
	
	        clearData();
	        clearShipTos();
	        header.setPartnerList(partnerList);
	
	        // check for valid entries
	        if (bupama != null) {
	            checkBusinessPartners(bupama);
	        }
	
	        synchronized (this) {
	            // remember the WebCatalog, because it may be needed
	            // by populateItemsFromCatalog()
	            this.salesDocWebCat = salesDocWebCat;
	
	            if (!alreadyInitialized) {
	                try {
	                    alreadyInitialized = true;
	                    getBackendService().createInBackend(shop, this);
	                }
	                catch (BackendException ex) {
	                    BusinessObjectHelper.splitException(ex);
	                }
	
	                if (businessEventHandler != null && businessEventHandler.isActive()) {
	                    CreateDocumentEvent event = new CreateDocumentEvent(this);
	                    businessEventHandler.fireCreateDocumentEvent(event);
	                }
	                isDirty = true;
	                header.setDirty(true);
	            }
	        }
		} finally {
			log.exiting();
		}
    }

	public void init(Shop shop, BusinessPartnerManager bupama, PartnerList partnerList, 
					 WebCatInfo salesDocWebCat, CampaignListEntry campaignListEntry, LoyaltyMembershipConfiguration memberShip) 
					 throws CommunicationException {
		getHeader().setMemberShip(memberShip);
		init(shop, bupama, partnerList, salesDocWebCat, campaignListEntry );
	}    
	/**
	 * Initializes the object according to the given shop and user. The state of
	 * the document is dropped as an result of calling this method and
	 * after that the necessary backend operations are performed to
	 * create a representation of the document in the underlying storage.
	 *
	 * @param shop Shop used for this basket
	 * @param bupama the business partner manager
	 * @param partnerList list of business partners for the document
	 * @param salesDocWebCat the actual catalog
	 */
	 public void init(Shop shop, BusinessPartnerManager bupama, PartnerList partnerList, 
                      WebCatInfo salesDocWebCat, CampaignListEntry campaignListEntry) 
					  throws CommunicationException {
		final String METHOD = "init()";
		log.entering(METHOD);
		// write some debugging info
		if (log.isDebugEnabled()) {
			log.debug("init(): shop = " + shop + ",  bupama: " + bupama + ",  campaignListEntry: " + campaignListEntry);
		}
		try{
		
			if (shop.isInternalCatalogAvailable() && salesDocWebCat == null) {
				throw( new IllegalArgumentException("Paramter salesDocWebCat can not be null!"));
			}
	
			clearData();
			clearShipTos();
			header.setPartnerList(partnerList);
			getHeader().getAssignedCampaigns().addCampaign(campaignListEntry);
	
			// check for valid entries
			if (bupama != null) {
				checkBusinessPartners(bupama);
			}
	
			synchronized (this) {
				// remember the WebCatalog, because it may be needed
				// by populateItemsFromCatalog()
				this.salesDocWebCat = salesDocWebCat;
	
				if (!alreadyInitialized) {
					try {
						alreadyInitialized = true;
						getBackendService().createInBackend(shop, this);
					}
					catch (BackendException ex) {
						BusinessObjectHelper.splitException(ex);
					}
	
					if (businessEventHandler != null && businessEventHandler.isActive()) {
						CreateDocumentEvent event = new CreateDocumentEvent(this);
						businessEventHandler.fireCreateDocumentEvent(event);
					}
					isDirty = true;
					header.setDirty(true);
				}
			}
		} finally {
			log.exiting();
		}
	}

	/**
	 * Initializes the object according to the given shop. The state of
	 * the document is dropped as an result of calling this method and
	 * after that the necessary backend operations are performed to
	 * create a representation of the document in the underlying storage.
	 *
	 * @param shop Shop used for this basket
	 * @param salesDocWebCat the actual catalog
	 */
	public void init(Shop shop, WebCatInfo salesDocWebCat, CampaignListEntry campaignListEntry) throws CommunicationException {
		final String METHOD = "init()";
		log.entering(METHOD);
		// write some debugging info
		if (log.isDebugEnabled()) {
			log.debug("init(): shop = " + shop);
		}
 		try {
 		
			if (shop.isInternalCatalogAvailable() && salesDocWebCat == null) {
				throw( new IllegalArgumentException("Paramter salesDocWebCat can not be null!"));
			}
	
			clearData();
			getHeader().getAssignedCampaigns().addCampaign(campaignListEntry);
	
			synchronized (this) {
				// remember the WebCatalog, because it may be needed
				// by populateItemsFromCatalog()
				this.salesDocWebCat = salesDocWebCat;
	
				if (!alreadyInitialized) {
					try {
						alreadyInitialized = true;
						getBackendService().createInBackend(shop, this);
	
					}
					catch (BackendException ex) {
						BusinessObjectHelper.splitException(ex);
					}
	
					if (businessEventHandler != null) {
						CreateDocumentEvent event = new CreateDocumentEvent(this);
						businessEventHandler.fireCreateDocumentEvent(event);
					}
					isDirty = true;
					header.setDirty(true);
				}
			}
 		} finally {
 			log.exiting();
 		}
	}

    /**
     * Initializes an empty instance of this object with the data coming from
     * the provided document. The former state of this document is dropped
     * and lost, the fields of the other document are copied into the
     * fields of this document. To do this the <code>clone()</code> method of
     * the fields is used to get an shallow copy and minimize interference
     * between the new and the old object.<br>
     * The creation date of the old document is not copied to the new one but
     * the original date is left as it is.<br>
     * An backend representation of the new
     * object is created and filled with the data retrieved by the
     * copy operation.
     *
     * @param posd The object used for the creation of this object
     * @param shop Shop used for this basket
     * @param user User for this basket
     * @param salesDocWebCat the actual catalog
     *
     * @deprecated This method will be replaced by <code>init(SalesDocument posd,
     *                                                        Shop shop,
     *                                                        BusinessPartnerManager bupama,
     *                                                        WebCatInfo salesDocWebCat) </code>
     * @see #init(SalesDocument posd,
     *            Shop shop,
     *            BusinessPartnerManager bupama,
     *            WebCatInfo salesDocWebCat)
     */
     public void init(SalesDocument posd,
            Shop shop,
            WebCatInfo salesDocWebCat) throws CommunicationException {

        //Dummy to be able to call the standard method
        BusinessPartnerManager bupama = null;

        init(posd, shop, bupama, salesDocWebCat);
    }

    /**
     * Retrieves the item information of the basket.
     * <b>As a side effect of this method the shiptos are also read. This
     * is not supposed to be the right behaviour and is left here only to keep
     * the clients of this class working. All clients should explicitly call
     * the <code>readShipTos()</code> method and not rely
     * on this - we are going to change this soon.</b>
     */
    public void readAllItems() throws CommunicationException {

		final String METHOD = "readAllItems()";
		log.entering(METHOD);

        try {
            //this must be discussed it necessary for new shipto concept
            //it will be better to remove this form the method and
            //call it explicitly in the actions
            getBackendService().readShipTosFromBackend(this);
            if (isDirty) {
                getBackendService().readAllItemsFromBackend(this);
                isDirty = false;
            }
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally {
        	log.exiting();
        }
    }

    /**
     * Retrieves the item information of the document from the backend system
     * for the specified itemList.
     * <b>Only the given itemList is filled. The itemList in the SalesDocument
     * would not be changed! </b>
     *
     * @param itemList list of items which should be read.
     *
     */
    public void readItems(ItemList itemList) throws CommunicationException {
		final String METHOD = "readItems()";
		log.entering(METHOD);
        // write some debugging info
        if (log.isDebugEnabled()) {
            log.debug("itemList ="+itemList);
        }

        try {
            // get all items in Backend
            getBackendService().readItemsFromBackend(this, itemList);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally {
			log.exiting();
        }
    }


    /**
     * Retrieves the ship to information of the basket.
     *
     * @param user The current user
     *
     *
     */
    public void readShipTos() throws CommunicationException {
		final String METHOD = "deleteShipTosInBackend()";
		log.entering(METHOD);
        try {
            getBackendService().readShipTosFromBackend(this);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
		finally {
		  log.exiting();
	  }

    }

    /**
     * Delete all shiptos that are stored for the document
     *
     */
    public void deleteShipTosInBackend() throws CommunicationException  {

		// write some debugging info
		  final String METHOD = "deleteShipTosInBackend()";
		  log.entering(METHOD);
        try {
            getBackendService().deleteShipTosInBackend();
            isDirty = true;
            header.setDirty(true);
            }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        catch (NoSuchMethodException ex) {
            log.error(LogUtil.APPS_BUSINESS_LOGIC, "deleteShipTos() " + ex.toString());
        }
        finally {
        	log.exiting();
        }

    }

    /**
     * Reads the preOrderSalesDocument data from the underlying storage
     * checking the changeability of the fields.
     * <b>As a side effect of this method the shiptos are also read. This
     * is not supposed to be the right behaviour and is left here only to keep
     * the clients of this class working. All clients should explicitly call
     * the <code>readShipTos()</code> method and not rely
     * on this - we are going to change this soon.</b>
     */
    public void readAllItemsForUpdate()
                        throws CommunicationException {

        // write some debugging info
		final String METHOD = "readAllItemsForUpdate()";
		log.entering(METHOD);

        try {
            // read and lock from Backend
            //getBackendService().readHeaderFromBackend(this,
            //                                   true);
            // this call has to be discussed for the new shipto concept
            // it will be better to remove this call here and call the
            // method to get the shiptos explicitly in the actions
            getBackendService().readShipTosFromBackend(this);
            if (isDirty) {
                 getBackendService().readAllItemsFromBackend(this, true);
                 isDirty = false;
            }
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
		finally {
		   log.exiting();
	   }
    }


    /**
     * Retrieves the header information of the basket.
     *
     */
    public void readHeader() throws CommunicationException {

		final String METHOD = "readHeader()";
		log.entering(METHOD);

        try {
            if (header.isDirty()) {
                getBackendService().readHeaderFromBackend(this);
                header.setDirty(false);
            }
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally {
        	log.exiting();
        }
    }


    /**
     * Retrieves the whole document from the underlying storage.
     * <b>As a side effect of this method the shiptos are also read, if
     * the soldto is set in the partner list. This
     * is not supposed to be the right behaviour and is left here only to keep
     * the clients of this class working. All clients should explicitly call
     * the <code>readShipTos()</code> method and not rely
     * on this - we are going to change this soon.</b>
     *
     */
    public void read() throws CommunicationException {
		final String METHOD = "read()";
		log.entering(METHOD);
        
        try {
            // new version
            if (isDirty || header.isDirty()) {
                getBackendService().readFromBackend(this);
                isDirty = false;
                header.setDirty(false);
            }
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally {
        	log.exiting();
        }
    }

    /**
     * Reads the whole document for the given Teckey from the backend. This
     * method is used to restore a sales document after a failure.
     *
     * @param shop The current shop
     * @param userGuid The TechKey of the current user
     * @param webCat The current catalog
     * @param techKey The techKey of the document to be recovered
     */
    public boolean recoverUsingTechKey(Shop shop, TechKey userGuid, WebCatInfo webCat, TechKey techKey) throws CommunicationException {
		final String METHOD = "recoverUsingTechKey()";
		log.entering(METHOD);
        // write some debugging info
        if (log.isDebugEnabled()) {
            log.debug("recoverUsingTechKey(): shop = " + shop + "; userGuid = " + userGuid.getIdAsString() + "; TechKey = " + techKey );
        }
		try {
		
	        if (shop == null) {
	            throw( new IllegalArgumentException("Parameter shop can not be null!"));
	        }
	        if (userGuid == null) {
	            throw( new IllegalArgumentException("Parameter userGuid can not be null!"));
	        }
	        if (webCat == null && shop.isInternalCatalogAvailable()) {
	            throw( new IllegalArgumentException("Parameter webCat can not be null!"));
	        }
	        if (techKey == null) {
	            throw( new IllegalArgumentException("Parameter techKey can not be null!"));
	        }
	        
	        if (salesDocWebCat == null) {
	            salesDocWebCat = webCat;
	        }
	
	        try {
	           if (!getBackendService().recoverFromBackend(this, shop, userGuid, techKey)) {
	               if (log.isDebugEnabled()) {
	                  log.warn(LogUtil.APPS_COMMON_INFRASTRUCTURE, "recoverUsingTechKey(): recovery failed" );
	               }
	               return false;
	           }
	        }
	        catch (BackendException ex) {
	            BusinessObjectHelper.splitException(ex);
	        }
	
	        // complete sales document
	        alreadyInitialized = true;
	        this.salesDocWebCat = webCat;
            
            // set campaign in catalog after recovery if necessary
            // but not if multiple camapigns are allowed
            if (!shop.isSoldtoSelectable() && !shop.isMultipleCampaignsAllowed() && getHeader().getAssignedCampaigns().size() > 0) {
                webCat.setCampaignId(getHeader().getAssignedCampaigns().getCampaign(0).getCampaignId());  
            }
	
	        isDirty = false;
	        header.setDirty(false);
	
	        if (log.isDebugEnabled()) {
	            log.debug("recoverUsingTechKey(): recovery succeeded" );
	        }
		} finally {
			log.exiting();
		}
        return true;
    }

    /**
     * Reads the last document for the given shop and user combination from the
     * backend. This method is used to restore a sales document after a failure.
     *
     * @param shop The current shop
     * @param userGuid The TechKey of the current user
     * @param webCat The current catalog
     */
    public boolean recoverUsingShopAndUser(Shop shop, TechKey userGuid, WebCatInfo webCat) throws CommunicationException {
		final String METHOD = "recoverUsingShopAndUser()";
		log.entering(METHOD);
        // write some debugging info
        if (log.isDebugEnabled()) {
            log.debug("recoverUsingShopAndUser(): shop = " + shop + "; userGuid = " + userGuid.getIdAsString());
        }
		try {
		
	        if (shop == null) {
	            throw( new IllegalArgumentException("Parameter shop can not be null!"));
	        }
	        if (userGuid == null) {
	            throw( new IllegalArgumentException("Parameter userGuid can not be null!"));
	        }
	        if (webCat == null && shop.isInternalCatalogAvailable()) {
	            throw( new IllegalArgumentException("Parameter webCat can not be null!"));
	        }
	        
	        if (salesDocWebCat == null) {
	            salesDocWebCat = webCat;
	        }
	
	        try {
	           if (!getBackendService().recoverFromBackend(this, shop,userGuid, shop.getTechKey(), userGuid)) {
	               if (log.isDebugEnabled()) {
	                  log.warn(LogUtil.APPS_COMMON_INFRASTRUCTURE, "recoverUsingShopAndUser(): recovery failed" );
	               }
	               return false;
	           }
	        }
	        catch (BackendException ex) {
	            BusinessObjectHelper.splitException(ex);
	        }
	
	        // complete sales document
	        alreadyInitialized = true;
	        this.salesDocWebCat = webCat;
            
            // set campaign in catalog after recovery if necessary
            // but not, if multiple camapigns are allowed
            if (!shop.isSoldtoSelectable() && !shop.isMultipleCampaignsAllowed() && getHeader().getAssignedCampaigns().size() > 0) {
                webCat.setCampaignId(getHeader().getAssignedCampaigns().getCampaign(0).getCampaignId());  
            }
	
	        isDirty = false;
	        header.setDirty(false);
			
	        if (log.isDebugEnabled()) {
	           log.debug("recoverUsingShopAndUser(): recovery succeeded" );
	        }
		}finally {
			log.exiting();	
		}
        return true;
    }

    /**
     * Adds a new shipTo to an item of the sales document using the appropriate
     * backend methods. This method is used to create a new shipto and
     * add it to the list of available ship tos for the sales document.
     *
     * @param itemKey the technical key of the item, the shipto belongs to
     * @param the technical key of the actually assigned shipto
     * @param new address to add
     * @param technical key of the soldto
     * @param id of the shop
     *
     * @returns integer value
     *      <code>0</code> if everything is ok
     *      <code>1</code> if an error occured, display corresponding messages
     *      <code>2</code> if an error occured, county selection necessary
     */
    public int addNewShipTo(TechKey itemKey, TechKey oldShipToKey, Address newAddress,
            TechKey soldToKey, TechKey shopKey)
                throws CommunicationException {
		final String METHOD = "addNewShipTo()";
		log.entering(METHOD);
        // write some debugging info
        if (log.isDebugEnabled()) {
            log.debug("addNewShipTo(): itemKey = " + itemKey
                    + ", oldShipToKey = " + oldShipToKey
                    + ", newAddress = " + newAddress
                    + ", soldToKey = " + soldToKey
                    + ", shopKey = " + shopKey);
        }

        try {
            isDirty = true;
            header.setDirty(true);
            return getBackendService().addNewShipToInBackend(this,
                    newAddress, soldToKey, shopKey, itemKey, oldShipToKey);
        }
        catch (BackendException ex) {
             BusinessObjectHelper.splitException(ex);
             return 1; // stands for not ok, but never reached
        }
        finally {
        	log.exiting();
        }
    }

    /**
     * Adds a new shipTo to an item of the sales document using the appropriate
     * backend methods. This method is used to create a new shipto and
     * add it to the list of available ship tos for the sales document.
     * Despite the other addNewShipTo methods the new shipto ies related to
     * a different businesspartner.
     *
     * @param itemKey the technical key of the item, the shipto belongs to
     * @param the technical key of the actually assigned shipto
     * @param new address to add
     * @param id of the shop
     * @param id of the businesspartner
     *
     * @returns integer value
     *      <code>0</code> if everything is ok
     *      <code>1</code> if an error occured, display corresponding messages
     *      <code>2</code> if an error occured, county selection necessary
     */
    public int addNewShipTo(TechKey itemKey, TechKey oldShipToKey, Address newAddress,
                            TechKey soldToKey, TechKey shopKey, String businessPartnerId)
           throws CommunicationException {
		final String METHOD = "addNewShipTo()";
		log.entering(METHOD);
        // write some debugging info
        if (log.isDebugEnabled()) {
            log.debug("addNewShipTo(): itemKey = " + itemKey
                    + ", oldShipToKey = " + oldShipToKey
                    + ", newAddress = " + newAddress
                    + ", soldToKey = " + soldToKey
                    + ", shopKey = " + shopKey
                    + ", businessPartnerId = " + businessPartnerId);
        }

        try {
            isDirty = true;
            header.setDirty(true);
            return getBackendService().addNewShipToInBackend(this,
                    newAddress, shopKey, itemKey, soldToKey, oldShipToKey, businessPartnerId);
        }
        catch (BackendException ex) {
             BusinessObjectHelper.splitException(ex);
             return 1; // stands for not ok, but never reached
        }
        finally {
        	log.exiting();
        }
    }


    /**
     * Adds a new shipTo to the sales document using the appropriate
     * backend methods. This method is used to create a new shipto and
     * add it to the list of available ship tos for the sales document.
     *
     * @param shipTo to be added
     *
     * @returns integer value
     *      <code>0</code> if everything is ok
     *      <code>1</code> if an error occured, display corresponding messages
     *      <code>2</code> if an error occured, county selection necessary
     */
    public int addNewShipTo(ShipTo shipTo)
                throws CommunicationException {
		final String METHOD = "addNewShipTo()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("ShipTo = "+shipTo);
        try {
            isDirty = true;
            header.setDirty(true);
            return getBackendService().addNewShipToInBackend(this, shipTo);
        }
        catch (BackendException ex) {
             BusinessObjectHelper.splitException(ex);
             return 1; // stands for not ok, but never reached
        }
        finally {
        	log.exiting();
        }

    }

     /**
     * Adds a new shipTo to the sales document using the appropriate
     * backend methods. This method is used to create a new shipto and
     * add it to the list of available ship tos for the sales document.
     *
     * @param new address to add
     * @param technical key of the soldto
     * @param id of the shop
     *
     * @returns integer value
     *      <code>0</code> if everything is ok
     *      <code>1</code> if an error occured, display corresponding messages
     *      <code>2</code> if an error occured, county selection necessary
     */
    public int addNewShipTo(Address newAddress, TechKey soldToKey, TechKey shopKey)
           throws CommunicationException {
		final String METHOD = "addNewShipTo()";
		log.entering(METHOD);
        // write some debugging info
        if (log.isDebugEnabled()) {
            log.debug("addNewShipTo(): newAddress = " + newAddress
                    + ", soldToKey = " + soldToKey
                    + ", shopKey = " + shopKey);
        }

        try {
            isDirty = true;
            header.setDirty(true);
            return getBackendService().addNewShipToInBackend(this, newAddress, soldToKey, shopKey);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
		finally {
			log.exiting();
		}
        return 1; // stands for not ok, but never reached
    }

    /**
     * Adds a new shipTo to the sales document using the appropriate
     * backend methods. This method is used to create a new shipto and
     * add it to the list of available ship tos for the sales document.
     * Despite the other addNewShipTo methods the new shipto ies related
     * to a different businesspartner.
     *
     * @param new address to add
     * @param id of the shop
     * @param id of the businesspartner
     *
     * @returns integer value
     *      <code>0</code> if everything is ok
     *      <code>1</code> if an error occured, display corresponding messages
     *      <code>2</code> if an error occured, county selection necessary
     */
    public int addNewShipTo(Address newAddress, TechKey soldToKey, TechKey shopKey, String businessPartnerId)
           throws CommunicationException {
		final String METHOD = "addNewShipTo()";
		log.entering(METHOD);
        // write some debugging info
        if (log.isDebugEnabled()) {
            log.debug("addNewShipTo(): newAddress = " + newAddress
                    + ", soldToKey = " + soldToKey
                    + ", shopKey = " + shopKey
                    + ", businessPartnerId = " + businessPartnerId);
        }

        try {
            isDirty = true;
            header.setDirty(true);
            return getBackendService().addNewShipToInBackend(this, newAddress, soldToKey, shopKey, businessPartnerId);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
		finally {
			log.exiting();
		}
        return 1; // stands for not ok, but never reached
    }



   /**
     * Retrieves a list of available shipping conditions from the
     * backend.
     *
     * @param language The language
     * @return table containing the conditions with the technical key
     *         as row key
     */
    public ResultData readShipCond(String language) throws CommunicationException {
		final String METHOD = "readShipCond()";
		log.entering(METHOD);
        // write some debugging info
        if (log.isDebugEnabled()) {
            log.debug("readShipCond(): language = " + language);
        }

       try {
            // get all items in Backend
            return new ResultData(getBackendService().readShipCondFromBackend(language));
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally {
        	log.exiting();
        }
        return null; // never reached
    }

    /**
     * Method for the lazy retrievement of the ShipTo's
     * address. This method is called by the proxy object generated
     * by the <code>createShipTo</code> method to read the address
     * only if it is really used.
     *
     * @param techKey Technical key of the ship to for which the
     *                address should be read
     * @return Address for the given ship to or <code>null<code> if no
     *         address is found
     */
    protected Address readShipToAddress(TechKey techKey) {
        try {
            return (Address) getBackendService().readShipToAddressFromBackend(this, techKey);
        }
        catch (BackendException e) {
            return null;
        }
    }

    /**
     * Callback method for the <code>BusinessObjectManager</code> to tell
     * the object that life is over and that it has to release
     * all ressources.
     */
    public void destroy() {
        isDirty = true;
        header.setDirty(true);
        try {
            getBackendService().destroyBackendObject();
        }
        catch (BackendException e) {
                if (log.isDebugEnabled()) {
                    log.debug(e.getMessage());
                }
        }
        super.destroy();
		shipToList = null;
		shipCond   = null;
        lastEnteredCVVS.clear();
    }


    /**
     * Method retrieving the backend object for the object. This method is
     * abstract because every concrete subclass of
     * <code>SalesDocument</code> may use its own implementation
     * of a backend object.
     *
     * @return Backend object to be used
     */
    protected abstract SalesDocumentBackend getBackendService()
            throws BackendException;

    /**
     * Assigns the basket to a business partner.
     *
     * @param shop Shop used for this basket
     * @param user User to assign the basket to.
     */
    public void addBusinessPartner(Shop shop, User user)
            throws CommunicationException {
		final String METHOD = "addBusinessPartner()";
		log.entering(METHOD);
        // write some debugging info
        if (log.isDebugEnabled()) {
            log.debug("addBusinessPartner(): shop = " + shop
                    + ", user = " + user);
        }

        try {
            // assign business partner
            getBackendService().addBusinessPartnerInBackend(shop, this);
            isDirty = true;
            header.setDirty(true);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally {
        	log.exiting();
        }
    }

    /**
     * Returns the Key of the campaign used for creating this basket.
     *
     * @return String representing the campaign
     * 
     */
    public String getCampaignKey() {
        return this.campaignKey;
    }

    /**
     * Sets the Key of campaign used for creating this basket
     *
     * @param campaignKey key of the campaign element
     * 
     */
    public void setCampaignKey(String campaignKey) {
        this.campaignKey = campaignKey;
    }

    /**
     * Returns the object type of the campaign element used for creating this
     * basket.
     *
     * @return String representing the campaign element type
     * 
     */
    public String getCampaignObjectType() {
        return campaignObjectType;
    }

    /**
     * Sets the object type of campaign element used for creating this basket
     *
     * @param campaignObjectType object type of campaign element
     * 
     */
    public void setCampaignObjectType(String campaignObjectType) {
        this.campaignObjectType = campaignObjectType;
    }

    /**
     * Adds the IPC configuration data of a basket/order item in the
     * backend.
     *
     * @param itemGuid TechKey of the Item
     */
    public void addItemConfig(TechKey itemGuid) throws CommunicationException {
		final String METHOD = "addItemConfig()";
		log.entering(METHOD);
        // write some debugging info
        if (log.isDebugEnabled()) {
            log.debug("addItemConfig(): itemGuid = " + itemGuid);
        }

        try {
           getBackendService().addItemConfigInBackend(this, itemGuid);
           isDirty = true;
           header.setDirty(true);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally {
        	log.exiting();
        }
    }

    /**
     * Initializes productconfiguration in the IPC, retrieves IPC-parameters from the backend
     * and stores them in this object.
     *
     * @param itemGuid the id of the item for which the configuartion
     *        should be read
     */
    public void getItemConfig(TechKey itemGuid) throws CommunicationException {
		final String METHOD = "getItemConfig()";
		log.entering(METHOD);
        // write some debugging info
        if (log.isDebugEnabled()) {
            log.debug("getItemConfig(): itemGuid = " + itemGuid);
        }

        try {
           getBackendService().getItemConfigFromBackend(this, itemGuid);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally {
        	log.exiting();
        }
    }

    /**
	 * Creates new IPC Item (with sub-items if any for Grid items) from the ipcItem of the current
	 * ISA  item passed 
	 *
	 * @param itemGuid the id of the item for which the configuartion
	 *        should be read
	 */
	public IPCItem copyIPCItem(TechKey itemGuid) throws CommunicationException {
		final String METHOD = "getItemConfig()";
		log.entering(METHOD);
		// write some debugging info
		if (log.isDebugEnabled()) {
			log.debug("getItemConfig(): itemGuid = " + itemGuid);
		}
		IPCItem copyIpcItem =null;
		
		try {
			copyIpcItem = getBackendService().copyIPCItemInBackend(this, itemGuid); 
		   
		}
		catch (BackendException ex) {
			BusinessObjectHelper.splitException(ex);
		}
		finally {
			log.exiting();
		}
		return copyIpcItem;
	}

    /**
     * Gets IPC info from backend
     *
     *
     */

    public void readIpcInfo() throws CommunicationException {
		final String METHOD = "readIpcInfo()";
		log.entering(METHOD);
     
        try {
            getBackendService().readIpcInfoFromBackend(this);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally {
        	log.exiting();
        }
    }


    /**
     * Returns a string representation of the object
     *
     * @return String representation
     */
    public String toString() {
        return this.getClass().getName() + " [techkey=\"" + techKey + "\", header=" + header
             + ", items=" + itemList + ", shipToList="+ shipToList + "]";
    }


    /**
     * Initializes an empty instance of this object with the data coming from
     * the provided document. The former state of this document is dropped
     * and lost, the fields of the other document are copied into the
     * fields of this document. To do this the <code>clone()</code> method of
     * the fields is used to get an shallow copy and minimize interference
     * between the new and the old object.<br>
     * The creation date of the old document is not copied to the new one but
     * the original date is left as it is.<br>
     * An backend representation of the new
     * object is created and filled with the data retrieved by the
     * copy operation.
     *
     * @param posd The object used for the creation of this object
     * @param shop Shop used for this basket
     * @param bupama the business partner manager
     * @param salesDocWebCat The actual catalog
     */
     public void init(SalesDocument posd,
            Shop shop,
            BusinessPartnerManager bupama,
            WebCatInfo salesDocWebCat) throws CommunicationException {
				init(posd, shop, bupama, salesDocWebCat, false);
    }
    
	/**
	 * Initializes an empty instance of this object with the data coming from
	 * the provided document. The former state of this document is dropped
	 * and lost, the fields of the other document are copied into the
	 * fields of this document. To do this the <code>clone()</code> method of
	 * the fields is used to get an shallow copy and minimize interference
	 * between the new and the old object.<br>
	 * The creation date of the old document is not copied to the new one but
	 * the original date is left as it is.<br>
	 * An backend representation of the new
	 * object is created and filled with the data retrieved by the
	 * copy operation.
	 *
	 * @param posd The object used for the creation of this object
	 * @param shop Shop used for this basket
	 * @param bupama the business partner manager
	 * @param salesDocWebCat The actual catalog
	 * @param execCampDet should campaign determination be executed
	 */
	 public void init(SalesDocument posd,
			Shop shop,
			BusinessPartnerManager bupama,
			WebCatInfo salesDocWebCat,
		    boolean execCampDet) throws CommunicationException {
		final String METHOD = "init()";
		log.entering(METHOD);
		// write some debugging info
		if (log.isDebugEnabled()) {
			log.debug("init(): posd = " + posd + ", shop = " + shop + ", bupama = " + bupama);
		}

		if (salesDocWebCat == null && shop.isInternalCatalogAvailable()) {
			log.exiting();
			throw( new IllegalArgumentException("Paramter salesDocWebCat can not be null!"));
		}

		this.salesDocWebCat = salesDocWebCat;

		clearData();

		// copy the shipto list of the
		// predecessor sales document
		String backend = shop.getBackend();
		
		//decide on the backend. Reason: for the R/3 backend, we do not
		//want to read all ship-to addresses which is done during the shipTo.clone
		//(Performance. Some customers have more than 100 ship-to pasties attached
		//to a sold-to).
		//for the CRM backend, the ship-to addresses have to be read 
        
	   if (backend.equals(ShopData.BACKEND_R3) || backend.equals(ShopData.BACKEND_R3_40)
		   ||backend.equals(ShopData.BACKEND_R3_45) || backend.equals(ShopData.BACKEND_R3_46)|| backend.equals(ShopData.BACKEND_R3_PI)){
			   setShipTosWithoutAddress(posd.getShipTos());
		   }
	   else {    
			   setShipTos(posd.getShipTos());
	   }

		// because docflow information is only saved at creation time,
		// we have to set the predecessor list right now
		header = (HeaderSalesDocument) posd.header.clone();
        // Cloning also clones the document type, so the document type needs to be
        // adjusted afterwards.
        adaptHeaderDocumentType();
        
		// to inhibit copying campaigns into positions that don't have campaigns
		// the campaign entry is temporarily removed
		((HeaderSalesDocument)header).getAssignedCampaigns().clear();
		this.setCampaignKey(null);
   
		if (header.getPartnerList().getSoldTo() == null) {
			PartnerListEntry soldTo = new PartnerListEntry();
			BusinessPartner bupa = bupama.getDefaultBusinessPartner(PartnerFunctionData.SOLDTO);
			if (bupa == null) {
				if (log.isDebugEnabled()) {
					log.debug("No soldto available in partnerlist.");
				}
			}
			else {
				soldTo.setPartnerId(bupa.getId());
				soldTo.setPartnerTechKey(bupa.getTechKey());
				header.getPartnerList().setSoldTo(soldTo);
			}
		}


		// restore the saved data
		header.setCreatedAt(null);

		// we have to copy the extension information because it gets lost,
		// when rhe heeader is cloned and the new document is initialized
		HashMap extCopy = (HashMap)((HashMap) posd.header.getExtensionMap()).clone();

		try {
				getBackendService().createInBackend(shop, this);
				alreadyInitialized = true;

			   // remember the WebCatalog, because it may be needed
			   // by populateItemsFromCatalog()
			   this.salesDocWebCat = salesDocWebCat;

			   // this is to add shptos to the new document
			   // if this is really only done for the shiptos it should
			   // be checked if it could be avoided!!! => test
			   //readHeader();     // may throw CommunicationException
			   //readAllItems();   // may throw CommunicationException
		}
		catch (BackendException ex) {
			BusinessObjectHelper.splitException(ex);
		}

		// set the extension information
		header.setExtensionMap(extCopy);

		itemList.clear();
        
		HashMap itemHandle = new HashMap();
		ItemHierarchy itemHier = new ItemHierarchy(posd.getItems());

		int size = posd.itemList.size();

		// Copy the items. Because of the fact, that the CRM has problems
		// dealing with items containing too much data, I do not use the
		// clone() method but copy the relevant fields manually
		for (int i = 0; i < size; i++) {
			ItemSalesDoc oldItem = ((ItemList)(posd.itemList)).get(i);
			if (oldItem.getParentId().isInitial()) {
				// set dumy TechKey to create a unique handle in SalesDocument constructor
				TechKey dummyKey = TechKey.EMPTY_KEY;
				ItemSalesDoc newItem = new ItemSalesDoc(dummyKey);
				newItem.setCopiedFromOtherItem(!execCampDet);
				//newItem.setProductId(oldItem.getProductId());
				setProductId(posd, newItem, oldItem);
				newItem.setProduct(oldItem.getProduct());
				newItem.setDataSetExternally(oldItem.isDataSetExternally());
				newItem.setUnit(oldItem.getUnit());
				newItem.setQuantity(oldItem.getQuantity());
				newItem.setReqDeliveryDate(oldItem.getReqDeliveryDate());
				newItem.setLatestDlvDate(oldItem.getLatestDlvDate());  // set LatestDlvDate required for Grid products
				newItem.setDeliveryPriority(oldItem.getDeliveryPriority());
				newItem.setText(oldItem.getText());
				if (oldItem.getShipTo() != null) {
					newItem.setShipTo(getShipTo(oldItem.getShipTo().getTechKey()));
				}
				if ( oldItem.getContractKey() != null &&
					!oldItem.getContractKey().getIdAsString().equals("") ) {
					newItem.setContractKey(oldItem.getContractKey());
					newItem.setContractItemKey(oldItem.getContractItemKey());
					newItem.setContractConfigFilter(
					oldItem.getContractConfigFilter());
					newItem.setContractId(oldItem.getContractId());
					newItem.setContractItemId(oldItem.getContractItemId());
				}

				//copy extension informations
				if (oldItem.getExtensionDataValues() != null) {
					Iterator extIter = oldItem.getExtensionDataValues().iterator();
					while (extIter.hasNext()) {
						Map.Entry extEntry =  (Map.Entry) extIter.next();
						newItem.addExtensionData(extEntry.getKey(), extEntry.getValue());
					}
				}

				newItem.setExternalItem(oldItem.getExternalItem());
				// add batch attributes
				newItem.setBatchDedicated(oldItem.getBatchDedicated());
				newItem.setBatchClassAssigned(oldItem.getBatchClassAssigned());
				newItem.setBatchID(oldItem.getBatchID());
				newItem.setBatchCharListJSP(oldItem.getBatchCharListJSP());
                
				newItem.setPcat(oldItem.getPcat());
				newItem.setPcatArea(oldItem.getPcatArea());
				newItem.setPcatVariant(oldItem.getPcatVariant());
                
				newItem.setAssignedCampaigns(oldItem.getAssignedCampaigns());
                
                newItem.setAuctionGuid(oldItem.getAuctionGuid());
                newItem.setLoyPointCodeId(oldItem.getLoyPointCodeId());
                newItem.setPtsItem(oldItem.isPtsItem());
                newItem.setBuyPtsItem(oldItem.isBuyPtsItem());
                
//				if (oldItem.getAssignedCampaigns().isEmpty() && !posd.getHeader().getAssignedCampaigns().isEmpty()) {
//					// a dummy entry is created to prevent copying the header campaign into the item in 
//					// the backend in the backend this dummy must be removed or ignored.
//					newItem.getAssignedCampaigns().addCampaign("", TechKey.EMPTY_KEY, "", false, true);
//				}
                
				itemHandle.put(oldItem.getTechKey().getIdAsString(), newItem.getHandle());

				itemList.add(newItem);
			}
			else if (oldItem.isItemUsageBOM()) {
				TechKey dummyKey = TechKey.EMPTY_KEY;
				ItemSalesDoc newItem = new ItemSalesDoc(dummyKey);
				newItem.setCopiedFromOtherItem(!execCampDet);
				String parentHandle = (String) itemHandle.get(oldItem.getParentId().getIdAsString());
				newItem.setParentHandle(parentHandle);
				ConnectedDocumentItem connectedItem = new ConnectedDocumentItem();
				connectedItem.setTechKey(oldItem.getTechKey());
				connectedItem.setDocumentKey(posd.getTechKey());
				newItem.setPredecessor(connectedItem);
				newItem.setItemUsageBOM();
				newItem.setQuantity("");
                
				itemHandle.put(oldItem.getTechKey().getIdAsString(), newItem.getHandle());
                
				itemList.add(newItem);
			}
			else if (itemHier.hasBOMSubItem(oldItem)) {
				TechKey dummyKey = TechKey.EMPTY_KEY;
				ItemSalesDoc newItem = new ItemSalesDoc(dummyKey);
				newItem.setCopiedFromOtherItem(!execCampDet);
				ConnectedDocumentItem connectedItem = new ConnectedDocumentItem();
				connectedItem.setTechKey(oldItem.getTechKey());
				connectedItem.setDocumentKey(posd.getTechKey());
				newItem.setPredecessor(connectedItem);
				newItem.setQuantity("");
                
				itemHandle.put(oldItem.getTechKey().getIdAsString(), newItem.getHandle());
                
				itemList.add(newItem);
			}
		}

		// Copy the reference to the event capturer from the given object
		businessEventHandler = posd.businessEventHandler;
        
		// Set the header campaign info again
		((HeaderSalesDocument)header).setAssignedCampaigns(((HeaderSalesDocument) posd.header).getAssignedCampaigns());
		this.setCampaignKey(posd.getCampaignKey());

		update(bupama, shop);
		log.exiting();
	}

	/**
	 * Initializes an empty instance of this object with the data coming from
	 * the provided document. The former state of this document is dropped
	 * and lost, the fields of the other document are copied into the
	 * fields of this document. To do this the <code>clone()</code> method of
	 * the fields is used to get an shallow copy and minimize interference
	 * between the new and the old object.<br>
	 * The creation date of the old document is not copied to the new one but
	 * the original date is left as it is.<br>
	 * An backend representation of the new
	 * object is created and filled with the data retrieved by the
	 * copy operation.
	 *
	 * @param posd The object used for the creation of this object
	 * @param shop Shop used for this basket
	 * @param bupama the business partner manager
	 * @param salesDocWebCat The actual catalog
	 */
	 public void init(SalesDocument posd,
			Shop shop,
			BusinessPartnerManager bupama,
			WebCatInfo salesDocWebCat,
			String processType ) throws CommunicationException {
				
		    init(posd, shop, bupama, salesDocWebCat, processType, false);
	}

	 
	/**
	 * Initializes an empty instance of this object with the data coming from
	 * the provided document. The former state of this document is dropped
	 * and lost, the fields of the other document are copied into the
	 * fields of this document. To do this the <code>clone()</code> method of
	 * the fields is used to get an shallow copy and minimize interference
	 * between the new and the old object.<br>
	 * The creation date of the old document is not copied to the new one but
	 * the original date is left as it is.<br>
	 * An backend representation of the new
	 * object is created and filled with the data retrieved by the
	 * copy operation.
	 *
	 * @param posd The object used for the creation of this object
	 * @param shop Shop used for this basket
	 * @param bupama the business partner manager
	 * @param salesDocWebCat The actual catalog
	 * @param exceCampDet should campaign determination be executed
	 */
	 public void init(SalesDocument posd,
			Shop shop,
			BusinessPartnerManager bupama,
			WebCatInfo salesDocWebCat,
			String processType,
		    boolean execCampDet) throws CommunicationException {
				
		final String METHOD = "init()";
		log.entering(METHOD);
		// write some debugging info
		if (log.isDebugEnabled()) {
			log.debug("init(): posd = " + posd + ", shop = " + shop + ", bupama = " + bupama + ", processtype = " + processType);
		}

		if (salesDocWebCat == null && shop.isInternalCatalogAvailable()) {
			log.exiting();
			throw( new IllegalArgumentException("Paramter salesDocWebCat can not be null!"));
		}

		this.salesDocWebCat = salesDocWebCat;

		clearData();

		// copy the shipto list of the
		// predecessor sales document
		String backend = shop.getBackend();
		
		//decide on the backend. Reason: for the R/3 backend, we do not
		//want to read all ship-to addresses which is done during the shipTo.clone
		//(Performance. Some customers have more than 100 ship-to pasties attached
		//to a sold-to).
		//for the CRM backend, the ship-to addresses have to be read 
        
	   if (backend.equals(ShopData.BACKEND_R3) || backend.equals(ShopData.BACKEND_R3_40)
		   ||backend.equals(ShopData.BACKEND_R3_45) || backend.equals(ShopData.BACKEND_R3_46)|| backend.equals(ShopData.BACKEND_R3_PI)){
			   setShipTosWithoutAddress(posd.getShipTos());
		   }
	   else{    
			   setShipTos(posd.getShipTos());
	   }

		// because docflow information is only saved at creation time,
		// we have to set the predecessor list right now
		HeaderSalesDocument headerSalesDoc = (HeaderSalesDocument) posd.header.clone();
        
		header = headerSalesDoc;
        // Cloning also clones the document type, so the document type needs to be
        // adjusted afterwards.
        adaptHeaderDocumentType();
        
		// an eventual campaign entry has to be removed because it otherwise 
		// would be inherited to existing item via copy, due to the fact, 
		// that during the createInBackend a changeHead is called
		// it will be set again before the next update
		ArrayList oldHeaderCampaigns = null;
        
		if (headerSalesDoc.getAssignedCampaigns() != null && headerSalesDoc.getAssignedCampaigns().size() > 0) {
			oldHeaderCampaigns = new ArrayList(headerSalesDoc.getAssignedCampaigns().size());
			for (int i=0; i < headerSalesDoc.getAssignedCampaigns().size(); i++) {
				oldHeaderCampaigns.add(headerSalesDoc.getAssignedCampaigns().getCampaign(i));
			}
            
			headerSalesDoc.getAssignedCampaigns().clear();
		}
		this.setCampaignKey(null);
        
		// restore the saved data
		header.setCreatedAt(null);
		
		// set the process type
		header.setProcessType(processType);

		// we have to copy the extension information because it gets lost,
		// when rhe heeader is cloned and the new document is initialized
		HashMap extCopy = (HashMap)((HashMap) posd.header.getExtensionMap()).clone();

		try {
				getBackendService().createInBackend(shop, this);
				alreadyInitialized = true;

			   // remember the WebCatalog, because it may be needed
			   // by populateItemsFromCatalog()
			   this.salesDocWebCat = salesDocWebCat;

			   // this is to add shptos to the new document
			  // if this is really only done for the shiptos it should
			  // be checked if it could be avoided!!! => test
			   //readHeader();         // may throw CommunicationException
			   //readAllItems();   // may throw CommunicationException
		}
		catch (BackendException ex) {
			BusinessObjectHelper.splitException(ex);
		}
        
		// now set old campaign again, if necessary
		if (oldHeaderCampaigns != null ) {
			for (int i=0; i < oldHeaderCampaigns.size(); i++) {
				headerSalesDoc.getAssignedCampaigns().getList().add(oldHeaderCampaigns.get(i));
			};
		}
		this.setCampaignKey(posd.getCampaignKey());

		// set the extension information
		header.setExtensionMap(extCopy);

		itemList.clear();

		int size = posd.itemList.size();

		// Copy the items. Because of the fact, that the CRM has problems
		// dealing with items containing too much data, I do not use the
		// clone() method but copy the relevant fields manually
		StringBuffer debugOutput = new StringBuffer("\nitem copy process");
		for (int i = 0; i < size; i++) {
			ItemSalesDoc oldItem = ((ItemList)(posd.itemList)).get(i);
			ItemSalesDoc oldParentItem = null;
			if (!oldItem.getParentId().isInitial()){
				oldParentItem = ((ItemList)posd.itemList).get(oldItem.getParentId());
			}
			if (log.isDebugEnabled()){
				debugOutput.append("\n old item :"+oldItem.getTechKey());
				if (oldParentItem != null){
					debugOutput.append("\n old item parent:"+oldParentItem.getTechKey());
					debugOutput.append("\n old item parent config type: "+ oldParentItem.getConfigType());
				}
			}
			//we only have BACKEND_R3_PI in 5.0, PI is mandatory
			//parent id not initial -> oldParentItem not null
			if (oldItem.getParentId().isInitial() ||
			   (backend.equals(ShopData.BACKEND_R3_PI) && oldParentItem.getConfigType().equals(ItemData.ITEM_CONFIGTYPE_GRID) ) ){
			   	
				if (log.isDebugEnabled()){
					debugOutput.append("\n item will be copied");
				}
			   	
				// set dumy TechKey to create a unique handle in SalesDocument constructor
				TechKey dummyKey = TechKey.EMPTY_KEY;
				ItemSalesDoc newItem = new ItemSalesDoc(dummyKey);
				// if camp det should be executed anyway, even for a copied item the execCampDet flag must be set to true
				newItem.setCopiedFromOtherItem(!execCampDet);
				//newItem.setProductId(oldItem.getProductId());
                setProductId(posd, newItem, oldItem);
				newItem.setProduct(oldItem.getProduct());
				newItem.setDataSetExternally(oldItem.isDataSetExternally());
				newItem.setUnit(oldItem.getUnit());
				newItem.setQuantity(oldItem.getQuantity());
				
				//set reqDeliveryDate, LatestDlvDate required for main and sub-items if any
				String latestDlvDate = null, reqDeliveryDate = null;
				if (oldItem.getParentId().isInitial()){
					latestDlvDate = oldItem.getLatestDlvDate();
					reqDeliveryDate = oldItem.getReqDeliveryDate();
				}else{
					latestDlvDate = oldParentItem.getLatestDlvDate();
					reqDeliveryDate = oldParentItem.getReqDeliveryDate();
				}
				newItem.setReqDeliveryDate(reqDeliveryDate);
				newItem.setLatestDlvDate(latestDlvDate);
				  
				newItem.setDeliveryPriority(oldItem.getDeliveryPriority());
				newItem.setText(oldItem.getText());
				//Need to be passed for Grid items
				if (oldItem.getConfigType() != null){
					newItem.setConfigType(oldItem.getConfigType());
					
					// Required to set correct Item heirarcy for Item-Subitem structure 
					if (oldItem.getConfigType().equals(ItemData.ITEM_CONFIGTYPE_GRID)){
						newItem.setTechKey(oldItem.getTechKey());
					}
				}
				
				if (oldItem.getShipTo() != null) {
					newItem.setShipTo(getShipTo(oldItem.getShipTo().getTechKey()));
				}
				if ( oldItem.getContractKey() != null &&
					!oldItem.getContractKey().getIdAsString().equals("") ) {
					newItem.setContractKey(oldItem.getContractKey());
					newItem.setContractItemKey(oldItem.getContractItemKey());
					newItem.setContractId(oldItem.getContractId());
					newItem.setContractItemId(oldItem.getContractItemId());
					newItem.setContractConfigFilter(
					oldItem.getContractConfigFilter());
				}

				if (!oldItem.getParentId().isInitial() ){
					newItem.setParentId(oldItem.getParentId());
				}
				//copy extension informations
				if (oldItem.getExtensionDataValues() != null) {
					Iterator extIter = oldItem.getExtensionDataValues().iterator();
					while (extIter.hasNext()) {
						Map.Entry extEntry =  (Map.Entry) extIter.next();
						newItem.addExtensionData(extEntry.getKey(), extEntry.getValue());
					}
				}

				newItem.setExternalItem(oldItem.getExternalItem());
				// add batch attributes
				newItem.setBatchDedicated(oldItem.getBatchDedicated());
				newItem.setBatchClassAssigned(oldItem.getBatchClassAssigned());
				newItem.setBatchID(oldItem.getBatchID());
				newItem.setBatchCharListJSP(oldItem.getBatchCharListJSP());
                
				if (oldItem.getAssignedCampaigns() != null && !oldItem.getAssignedCampaigns().isEmpty()) {
					for (int j = 0; j < oldItem.getAssignedCampaigns().size(); j++) {
						if (oldItem.getAssignedCampaigns().getCampaign(j) != null) {
							CampaignListEntry campEntry = newItem.getAssignedCampaigns().createCampaign();
							campEntry.setCampaignId(oldItem.getAssignedCampaigns().getCampaign(j).getCampaignId());
							newItem.getAssignedCampaigns().addCampaign(campEntry);
						}
					}
				}
				
				newItem.setAuctionGuid(oldItem.getAuctionGuid());

				itemList.add(newItem);
			}
			else{
				if (log.isDebugEnabled()){
					debugOutput.append("\n item won't be copied");
				}
			}
		}
		if (log.isDebugEnabled()){
			log.debug(debugOutput.toString());
		}

		// Copy the reference to the event capturer from the given object
		businessEventHandler = posd.businessEventHandler;

		update(bupama, shop);
		log.exiting();
	}
	
    /**
     * Method used to set a reference to the business event handler for this class
     * from the outside.
     *
     * @param businessEventHandler the handler to be set
     */
    public void setBusinessEventHandler(BusinessEventHandlerBase businessEventHandler) {
        this.businessEventHandler = (BusinessEventHandler)businessEventHandler;
    }

    /**
     * Adds a item coming from the outside world to the sales document. This
     * item is represented by a special crafted transfer
     * object.
     *
     * @param transferItm the Item to be added
     */
    public void addTransferItem(BasketTransferItem transferItm) {
        addItem(new ItemSalesDoc(transferItm));
    }

    /**
     * Adds a item coming from the outside world to the sales document. This
     * item is represented by a special crafted transfer
     * object.
     *
     * @param transferItm the Item to be added
     */
    public void addTransferItem(BasketTransferItem transferItm, String reqDeliverDate) {
        ItemSalesDoc itm = new ItemSalesDoc(transferItm);
        itm.setReqDeliveryDate(reqDeliverDate);
        addItem(itm);
    }

    /**
     * Adds a item coming from the outside world to the sales document. This
     * item is represented by a special crafted transfer object.
     *
     * @param transferItm the Item to be added
     * @param reqDeliverDate the requested delivery date to be set for the Item 
     * @param handle the handle to be set for the Item
     */
    public void addTransferItem(BasketTransferItem transferItm, String reqDeliverDate, String handle) {
         ItemSalesDoc itm = new ItemSalesDoc(transferItm);
         itm.setReqDeliveryDate(reqDeliverDate);
         itm.setHandle(handle);
         addItem(itm);
    }


	/**
	 * Adds a item coming from the outside world to the sales document. This
	 * item is represented by a special crafted transfer object.
	 *
	 * @param transferItm the Item to be added
	 * @param productKey the technical key of the proudct to be set for the Item
	 */
	public void addTransferItem(BasketTransferItem transferItm, TechKey productKey) {
		 ItemSalesDoc itm = new ItemSalesDoc(transferItm);
		 itm.setProductId(productKey);
		 addItem(itm);
	}


	/**
	 * Adds a item coming from the outside world to the sales document. This
	 * item is represented by a special crafted transfer
	 * object.
	 *
	 * @param transferItm the Item to be added
	 * @param reqDeliverDate the requested delivery date to be set for the Item
	 * @param productKey the technical key of the proudct to be set for the Item
	 */
	public void addTransferItem(BasketTransferItem transferItm, String reqDeliverDate, TechKey productKey) {
		ItemSalesDoc itm = null;

		if (getHeader().getMemberShip() != null  && getHeader().getMemberShip().getMembershipId() != null) {
			itm = new ItemSalesDoc(transferItm, getHeader().getShop().getPointCode());
		}
		else {
			itm = new ItemSalesDoc(transferItm);
		}
		itm.setReqDeliveryDate(reqDeliverDate);
		itm.setProductId(productKey);
		addItem(itm);
	}

	/**
	 * Adds a item coming from the outside world to the sales document. This
	 * item is represented by a special crafted transfer object.
	 *
	 * @param transferItm the Item to be added
	 * @param reqDeliverDate the requested delivery date to be set for the Item 
	 * @param handle the handle to be set for the Item
	 * @param productKey the technical key of the proudct to be set for the Item 
	 */
	public void addTransferItem(BasketTransferItem transferItm, String reqDeliverDate, String handle, TechKey productKey) {
		 ItemSalesDoc itm = new ItemSalesDoc(transferItm);
		 itm.setReqDeliveryDate(reqDeliverDate);
		 itm.setHandle(handle);
		 itm.setProductId(productKey);
		 addItem(itm);
	}



    /**
     * Adds a new ShipTo to the sales document
     *
     * @param shipTo The ship to to be added
     */
    public void addShipTo(ShipToData shipTo) {
    	if (log.isDebugEnabled()) {
        	if (!(shipTo instanceof ShipTo)){
        		throw new PanicException("Wrong type of shipTo");
        	}
    	}	
        shipToList.add(shipTo);
    }

    /**
     * Adds a new ShipTo to the sales document
     *
     * @param shipTo The ship to to be added
     */
    public void addShipTo(ShipTo shipTo) {
        shipToList.add(shipTo);
    }

    /**
     * Clears the list of the ship tos. This method is used to release
     * the state of the basket before rereading the data from the
     * underlying storage.
     */
    public void clearShipTos() {
        shipToList.clear();
    }
    
    /**
      * Releaes state of the object. This method is used to drop state
      * information between HTTP request to save memory ressources.
      */
     public void clearData() {
         super.clearData();
         lastEnteredCVVS.clear();
     }

    /**
     * Returns the ship to specified by the given index
     *
     * @param index Index of the ship to to be retrieved
     * @return Returns the found ship to or <code>null</code> if no
     *         element was found at the position
     */
    public ShipTo getShipTo(int index) {
        return (ShipTo) shipToList.get(index);
    }

    /**
     * Searches for the ship to with the given techkey and returns a
     * reference to it
     *
     * @param techKey The technical key of the ship to
     * @return reference to the found shipto or <code>null</code> if no
     *         match was found
     */
    public ShipToData findShipTo(TechKey techKey) {
        return (ShipToData) getShipTo(techKey);
    }

    /**
     * Returns the ship to specified by the given technical key
     *
     * @param techkey Technical key of the ship to to be retrieved
     * @return Returns the found ship to or <code>null</code> if no
     *         element was found at the position
     */
    public ShipTo getShipTo(TechKey techKey) {
        for (int i = 0; i < shipToList.size(); i++) {
            if (((ShipTo) shipToList.get(i)).getTechKey().equals(techKey)) {
                return (ShipTo) shipToList.get(i);
            }
        }
        return null;
    }
    
	/**
	 * Returns the ship to specified by the given shipto key values.
	 * Set the value that should not be taken into account to null.
	 * Set the value to empty space "" to take into account empty key fields. 
	 *
	 * @param shiptoId the busines partner identification of the ship-to-party
	 * @param addrId   the number of the shipto address
	 * @param addrOrign the orgigin value of the shipto address
	 * @param addrType  the type value of the shipto address
	 * @param addrPersonNumber the person number of the shipto address
	 * @return Returns the found ship to or <code>null</code> if no
	 *         element was found at the position
	 */
	public ShipTo getShipTo(String shiptoId, boolean manualAddress, String addrId, String addrOrigin, String addrType, String addrPersonNumber) {
		ShipTo shipto = null;
	    
		for (int i = 0; i < shipToList.size(); i++) {
			shipto = (ShipTo) shipToList.get(i);	
			if (shiptoId != null) {
			  if (!shipto.getId().equals(shiptoId)) {
			    continue;
			  }
			}
			if (shipto.hasManualAddress() != manualAddress) {
			  continue;		   	
			}
			if (addrId != null) { 
			  if (!shipto.getAddress().getId().equalsIgnoreCase(addrId)) {
			  	continue;
			  }  
			}
			if (addrOrigin != null) { 
			  if (!shipto.getAddress().getOrigin().equalsIgnoreCase(addrOrigin)) {
				continue;
			  }  
			}  
			if (addrType != null) { 
			  if (!shipto.getAddress().getType().equalsIgnoreCase(addrType)) {
				continue;
			  }  
			}			 
			if (addrPersonNumber != null) { 
			  if (!shipto.getAddress().getPersonNumber().equalsIgnoreCase(addrPersonNumber)) {
				continue;
			  }  
			}
		     
		    return shipto;
		     
		}		
		
    	return null;
	}

    /**
     * Returns an <code>Iterator</code> to iterate over the ship tos
     *
     * @return Iterator for soldTos
     */
    public Iterator getShipToIterator() {
        return shipToList.iterator();
    }

    /**
     * Returns the <code>List</code> of ship tos
     *
     * @return List with soldTos
     */
    public List getShipToList() {
        return shipToList;
    }

    /**
     * Returns an array containing all ship tos currently stored in the
     * sales document.<br>
     * A shallow copy is peformed. Changing the data of a array element
     * will cause the data of the basket to be changed, too.
     *
     * @return Array containing <code>ShipTo</code> objects
     */
    public ShipTo[] getShipTos() {
        ShipTo[] shipTos = new ShipTo[shipToList.size()];
        shipToList.toArray(shipTos);
        return shipTos;
    }


    /**
     * Clears the the shiptoList and takes the shiptos of the new shipto list
     *
     * @param shipToArray that shall be copied
     */
    public void setShipTos(ShipTo[] shipToArray) {
    
        this.shipToList.clear();
        this.shipToList = new ArrayList();
    
        for ( int i=0; i<shipToArray.length; i++) {
          //Only the shipto list is cleared but the shipto objects 
          // remains thus a cloning could be avoided. This should
          // correct the removed statement in the init method to
          // set the header shipto regarding the new shipto list
          // of an predessor document.
          //ShipToProxy shiptoClone = (ShipToProxy) shipto.clone();
          if (shipToArray[i] instanceof ShipToProxy) {
              ShipToProxy shipto = (ShipToProxy) shipToArray[i];
              this.shipToList.add(shipto);
          } else {
              ShipTo shipto = (ShipTo) shipToArray[i];
              this.shipToList.add(shipto);
          }
        }
    
    }


	/**
	  * Clears the the shiptoList and takes the shiptos of the new shipto list.
	  * Doesn't read the address data of shipto's. Used for the R/3 backend
	  * for performance reasons.
	  *
	  * @param shipToArray that shall be copied
	  */
	 public void setShipTosWithoutAddress(ShipTo[] shipToArray) {

		 this.shipToList.clear();
		 this.shipToList = new ArrayList();

		 for ( int i=0; i<shipToArray.length; i++) {
		   ShipToProxy shipto = (ShipToProxy) shipToArray[i];         
		   this.shipToList.add(shipto);
		 }
	 }
    

    /**
     * Creates a new ship to object
     *
     * @return Newly created Ship to
     */
    public ShipToData createShipTo() {
        return new ShipToProxy();
    }

    /**
     * Gets the number of ship tos currently associated with the sales document
     *
     * @return The number
     */
    public int getNumShipTos() {
        return shipToList.size();
    }

    /**
     * Searches for the ship to with the given values given in
     * the shipto search object and returns a reference to it
     *
     * @param shipto  A shipto object with search values
     * @return reference to the found shipto or <code>null</code> if no
     *         match was found
     */
    public ShipToData findShipTo(ShipToData shipTo) {
      for (int i = 0; i < shipToList.size(); i++) {
         if (((ShipToData) shipToList.get(i)).equals(shipTo)) {
             return (ShipToData) shipToList.get(i);
         }
      }
    
      return null;
    }

    /**
     * Determines if manual Product Determination is necessary for at least one
     * top level item.
     * 
     * @return boolean true if there is at least on item with a non empty
     *          prodcutAliasList false if all productAliasLists are empty
     * 
     * @deprecated please use {@link #isDeterminationRequired() instead
     */
    public boolean isAlternativeProductAvailable() {
        return isAlternativeProductAvailable;
    }

    /**
     * Sets the isAlternativeProductAvailable.
     * 
     * @param isAlternativeProductAvailable true if ther are items with
     * alternativ products
     * 
     * @deprecated please use {@link #setDeterminationRequired(boolean isDeterminationRequired) instead
     */
    public void setAlternativeProductAvailable(boolean isAlternativeProductAvailable) {
        this.isAlternativeProductAvailable = isAlternativeProductAvailable;
    }
    
    /**
     * Determines if manual Product-, Campaign-, ... Determination is necessary 
     * for at least one top level item.
     * 
     * @return boolean true if there is at least on item that needs
     *         manual determination for products, camapigns, etc.
     */
    public boolean isDeterminationRequired(){
        return isDeterminationRequired;
    }
    
    /**
     * Sets the isDeterminationRequired flag
     * 
     * @param isDeterminationRequired  true if there are items that need
     *        manual determination either for cmapigns, substitution products
     *        or something simular
     */
    public void setDeterminationRequired(boolean isDeterminationRequired) {
        this.isDeterminationRequired = isDeterminationRequired;
    }
    
	/**
     * Searches for the first item in the itemlist with the given handle
     * and returns it. If no item with the given handle is found, null is
     * returned. 
     * 
     * @param handle the handle to be set for the Item
     * @return ItemSalesDoc the first item with the given handle, or null
     */
    public ItemSalesDoc getFirstItemWithHandle(String handle) {
    	
        ItemBase itemBase = getFirstItemBaseWithHandle(handle);
        return ItemSalesDoc.getItemSalesDoc(itemBase);      
    }

    /**
     * Retrieves the header information of the sales document.
     *
     * @return Header
     */
    public HeaderSalesDocument getHeader() {
        return HeaderSalesDocument.getHeaderSalesDocument(header);
    }

    /**
     * Retrieves the header information of the sales document.
     *
     * @return Header data
     */
    public HeaderData getHeaderData() {
        return HeaderSalesDocument.getHeaderSalesDocument(header);
    }

    /**
     * Sets the header information. The header information is data
     * common to all items of the basket.
     *
     * @param header Header data to set
     */
    public void setHeader(HeaderData header) {
    	setHeader((HeaderBaseData)header);
    }

    /**
     * Sets the header information. The header information is data
     * common to all items of the basket.
     *
     * @param header Header data to set
     */
    public void setHeader(HeaderSalesDocument header) {
		setHeader((HeaderBaseData)header);
    }

    /**
     * Sets the whole list of items in one method call
     *
     * @param itemList List containing the items
     */
    public void setItems(ItemList itemList) {
        setItemListData(itemList);
    }

    /**
     * Adds a <code>Item</code> to the sales document.
     * This item must be uniquely
     * identified by a technical key.
     * A <code>null</code> reference passed to this function will be ignored,
     * in this case nothing will happen.
     *
     * @param item Item to be added to the basket
     */
    public void addItem(ItemData itemData) {
    	addItem((ItemBaseData)itemData);
    }

	/**
	 * Only in this method should be created the header object which is to 
	 * use in the sales document. Overwrite this method if you want other 
	 * header as {@link com.sap.isa.businessobject.header.HeaderSalesDocument} 
	 * 
	 * @return header as HeaderBase obejct
	 */
	protected HeaderBase createHeaderInstance() {
		return new HeaderSalesDocument();		
	}


    /**
     * Creates a <code>Header</code> for the sales document. <br>
     * The object will be created from the protected method
     * {@link #createHeaderInstance)}. 
     *
     * @return created header
     */
    public HeaderData createHeader() {
      return (HeaderData)createHeaderBase();
    }

	/**
	 * Only in this method should be created an item object, which is to 
	 * use in the sales document. Overwrite this method if you want other 
	 * item as {@link com.sap.isa.businessobject.item.ItemSalesDoc} 
	 * 
	 * @return header as HeaderBase obejct
	 */
	protected ItemBase createItemInstance() {
		return new ItemSalesDoc();		
	}


    /**
     * Creates an <code>ItemData</code> for the sales document.
     * This item must be uniquely identified by a technical key. <br>
     * The object is created with the {@link createItemInstance} method. <br>
     *
     * @return item Item which can added to the basket
     */
    public ItemData createItem() {
      	return (ItemData)createItemInstance();
    }


	/**
	 * Creates an <code>ItemSalesDoc</code> for the sales document.
	 * This item must be uniquely identified by a technical key. <br>
	 * The object is created with the {@link createItemInstance} method. <br>
	 *
	 * @return item Item which can added to the Sales Doc
	 */
	public ItemSalesDoc createItemSalesDoc() {
		return (ItemSalesDoc)createItemInstance();	
	}

    /**
     * Creates a <code>ItemListData</code> for the basket.
     *
     * @return ItemListData an empty ItemListData
     */
    public ItemListData createItemList() {
      return (ItemListData)createItemListInstance();
    }
    
	/**
	 * Only in this method should be created an item list object, which is to 
	 * use in the sales document. Overwrite this method if you want other 
	 * item as {@link com.sap.isa.businessobject.item.ItemListBase} 
	 * 
	 * @return header as HeaderBase obejct
	 */
	protected ItemListBase createItemListInstance() {
		return new ItemList();		
	}

    /**
     * Returns a list of the items currently stored inside the sales document.
     * <br>
     * <b>Note</b> This method performs no copy and returns a reference to
     * the internal representation of the basket's items. Changes done at the
     * items will affect the sales document, too.
     *
     * @return List with items
     */
    public ItemList getItems() {
        return (ItemList)getItemListBaseData();
    }

    /**
     * Returns the item of the document
     * specified by the given technical key.<br><br>
     * <b>Note -</b> This is a convenience method operating on the
     * <code>getItem</code> method of the internal <code>ItemList</code>.
     *
     * @param techKey the technical key of the item that should be
     *        retrieved
     * @return the item with the given techical key or <code>null</code>
     *         if no item for that key was found.
     */
    public ItemSalesDoc getItem(TechKey techKey) {
        return (ItemSalesDoc)getItemBaseData(techKey);
    }

    /**
     * Returns the item of the document
     * specified by the given technical key.<br><br>
     * <b>Note -</b> This is a convenience method operating on the
     * <code>getItem</code> method of the internal <code>ItemList</code>.
     *
     * @param techKey the technical key of the item that should be
     *        retrieved
     * @return the item with the given techical key or <code>null</code>
     *         if no item for that key was found.
     */
    public ItemData getItemData(TechKey techKey) {
        return (ItemData)getItemBaseData(techKey);
    }

    /**
     * Returns a list of the items currently stored for this
     * sales document.
     *
     * @return list of items
     */
    public ItemListData getItemListData() {
        return (ItemListData)getItemListBaseData();
    }

    /**
     * Sets the list of the items for this sales document.
     *
     * @param  itemList list of items to be stored
     */
    public void setItemListData(ItemListData itemList) {
        setItemListBaseData(itemList);
    }

    /**
     * Sets a list of the items currently stored for this sales document.
     *
     * @param itemList list to be set
     *
     */
    public void setItemList(ItemList itemList) {
        setItemListBaseData(itemList);
    }

    /**
	 * Inner class providing proxy functionality for the ShipTo-Object
	 */
	private class ShipToProxy extends ShipTo {

		public Address getAddress() {
			if (super.getAddress() == null) {
				setAddress(readShipToAddress(super.getTechKey()));
			}
			return super.getAddress();
		}


		/**
		 * Clones a new shipto from the given shipto object.
		 *
		 * @result the cloned shipto object
		 *
		 */
		 public Object clone() {
			
		    ShipToProxy shiptoClone = new ShipToProxy();

		    shiptoClone.setTechKey(this.getTechKey());
		    shiptoClone.setId(this.getId());
		    shiptoClone.setManualAddress(this.hasManualAddress());
		    shiptoClone.setShortAddress(this.getShortAddress());
		    shiptoClone.setStatus(this.getStatus());
		    Address shiptoCloneAddress = (Address) this.getAddress().clone();
		    shiptoClone.setAddress(shiptoCloneAddress);

		    return shiptoClone;
		 }
	}


	/**
	 * Check if the header is an instance of the correct class. <br> 
	 * Overwrite this method if you want to use an other 
	 * header as {@link com.sap.isa.businessobject.item.HeaderSalesDocument} 
	 * 
	 * @return <code>true</code> if the header is instance of the correct class
	 */
	protected boolean checkHeaderInstance(HeaderBaseData headerBaseData) {

	   if (!(headerBaseData instanceof HeaderSalesDocument)) {
		   throw (new PanicException ("Wrong type of header:" 
			   + headerBaseData.getClass().getName()));
	   }

	   return true;
	 }


	/**
	 * Check if the item is an instance of the correct class. <br> 
	 * Overwrite this method if you want to use an other 
	 * item as {@link com.sap.isa.businessobject.item.ItemSalesDoc} 
	 * 
	 * @return <code>true</code> if the item is instance of the correct class
	 */
	protected boolean checkItemInstance(ItemBaseData item) {
		return ItemSalesDoc.isItemBase(item);		
	}

	
	/**
	 * Check if the itemlist is an instance of the correct class. <br> 
	 * Overwrite this method if you want to use an other 
	 * itemList as {@link com.sap.isa.businessobject.item.ItemList} 
	 * 
	 * @return <code>true</code> if the itemlist is instance of the correct class
	 */
	protected boolean checkItemListInstance(ItemListBaseData itemListBaseData) {
	   boolean isItemList = false;
	   
	   if (itemListBaseData != null) {
		   if (itemListBaseData instanceof ItemList) {
			   isItemList = true;
		   } 
		   else {
			   log.debug("not allowed instance for ItemList object: " + 
						 itemListBaseData.getClass().getName());        	
		   }  
	   }
	         	
	   return isItemList;
	}

	/**
	 * Update the campaign information for the item with given itemsguids.
	 *
	 * @param itemsGuidsToProcess  List of Guids of items, to process
	 * @param bupama the business partner manager
	 * @param shop The shop
	 */
	public void updateItemCampaignInformation(List itemGuidsToProcess) throws CommunicationException {
		final String METHOD = "updateItemCampaignInformation()";
		log.entering(METHOD);
		// write some debugging info
		if (log.isDebugEnabled()) {
			log.debug(
				"updateCampaignInformation: itemGuidsToProcess: "
					+ itemGuidsToProcess.toString());
		}
	
		try {
			getBackendService().updateCampaignDetermination(this, itemGuidsToProcess);
			isDirty = true;
			header.setDirty(true);
		} catch (BackendException ex) {
			BusinessObjectHelper.splitException(ex);
		} finally {
			log.exiting();
		}
	}

	/**
	 * Get the list of campaign descriptions. 
	 * The string value of the campaign guid b2b_40_SP_COR is the key and the description the value.
	 * 
	 * @return HashMap the map containing all campaign descriptions
	 */
	public HashMap getCampaignDescriptions() {
		return campaignDescriptions;
	}

	/**
	 * Get the list of campaign type descriptions. 
	 * The campaign type id is the key and the description the value.
	 * 
	 * @return HashMap the map containing all campaign type descriptions
	 */
	public HashMap getCampaignTypeDescriptions() {
		return campaignTypeDescriptions;
	}

	/**
	 * Set the list of campaign descriptions. 
	 * The campaign guid as string is the key and the description the value.
	 * 
	 * @param HashMap the map containing all campaign descriptions
	 */
	public void setCampaignDescriptions(HashMap campaignDescriptions) {
		this.campaignDescriptions = campaignDescriptions;
	}

	/**
	 * Set the list of campaign type descriptions. 
	 * The campaign type id is the key and the description the value.
	 * 
	 * @param HashMap the map containing all campaign type descriptions
	 */
	public void setCampaignTypeDescriptions(HashMap campaignTypeDescriptions) {
		this.campaignTypeDescriptions = campaignTypeDescriptions;
	}

	/**
	 * Reads the Stock Indicator information for all valid product variants
	 * of a grid product from the backend 
	 * 
	 * @param itemGuid the id of the item for which the Stock Indictor information
	 *        should be read
	 * 
	 * @return 2dimensional array, of which the 1st one contain the variantIds, and
	 * 			2nd one contain the StockIndicators
	 */
	public String[][] getGridStockIndicator(TechKey itemGuid) throws CommunicationException {

		final String METHOD = "getGridStockIndicator()";
		log.entering(METHOD);
		// write some debugging info
		if (log.isDebugEnabled()) {
			log.debug("getGridStockIndicator: itemGuid = " + itemGuid);
		}

		try {
			String[][] variantStockInd = getBackendService().getGridStockIndicatorFromBackend(this, itemGuid);
		   	return variantStockInd; 
		}
		catch (BackendException ex) {
			BusinessObjectHelper.splitException(ex);
			return null;
		}
		finally {
			log.exiting();
		}
		
	}

	/**
	 * 
	 * Retrieves the available payment types from the backend.
	 *
	 * @return PaymentType object containing the available payment types.
	 */
	public PaymentBaseTypeData readPaymentType() throws CommunicationException {

		// write some debugging info
		if (log.isDebugEnabled()) {
			log.debug("readPaymentType()");
		}

		synchronized (this) {
			if (payType == null) {

				try {
					payType = new PaymentBaseType();

					// get all items in Backend
					(getBackendService()).readPaymentTypeFromBackend(this, payType);
				}
				catch (BackendException ex) {
					BusinessObjectHelper.splitException(ex);
				}
			}
		}

		return payType;
	}


	/**
	 * Retrieves a list of available credit card types from the
	 * backend.
	 *
	 * @return Table containing the credit card types with the technical key
	 *         as row key
	 */
	public ResultData readCardType() throws CommunicationException {

		if (log.isDebugEnabled()) {
			log.debug("readCardType()");
		}

		synchronized (this) {
			if (cardType == null) {
				try {
					cardType = (getBackendService()).readCardTypeFromBackend();					
				}
				catch (BackendException ex) {
					BusinessObjectHelper.splitException(ex);
				}
			}
		}
		if (cardType == null) {
		  return null;
		} else {  
		  return new ResultData(cardType);
		}  
	}
	

	/**
	 * This is the listing number from the auction side, the ID
	 * is retrieved usually from the auction site, it is used to
	 * retrieve all the corelated quotation and orders.
	 */
	public void setAuctionID(String purchaseNumber) {
		auctionId = purchaseNumber;
		getHeader().setPurchaseOrderExt(purchaseNumber);
	}

	/**
	 * Return the auction listing ID
	 * @return the auctiion listing ID, used for tracking order history
	 */
	public String getAuctionID() {
		if (auctionId == null) {
			auctionId = getHeader().getPurchaseOrderExt();
		}
		return auctionId;
	}
	
	/**
	 * Retrurns if the document is auction related
	 */
	public boolean isAuctionRelated(){
		return isAuctionRelated;
	}
	
	/**
	 * Sets the isAuctionRelated flag
	 * 
	 * @param isAuctionRelated  true if the sales document is created for
	 * auction scenario
	 */
	public void setAuctionRelated(boolean auctionRelated){
		this.isAuctionRelated = auctionRelated;
	}
//end auction related

  /**
   * Checks if the recovery of the document is supported by the backend.
   *
   * @return <code>true</code> when the backend supports the recovery
   */
  public boolean checkRecovery() throws CommunicationException {
      
      boolean recovery = false;
	  
	  if (log.isDebugEnabled()) {
		  log.debug("checkRecovery()");
	  }

	  try {
		  recovery = (getBackendService()).checkRecoveryInBackend();
	  }
	  catch (BackendException ex) {
		  BusinessObjectHelper.splitException(ex);
	  }
      return recovery;
  }

 

  /**
   * Checks if the save and load basket function is supported by the backend.
   *
   * @return <code>true</code> when the backend supports the save and load baskets
   */
  public boolean checkSaveBaskets() throws CommunicationException {
      
	  boolean saveBaskets = false;
	  
	  if (log.isDebugEnabled()) {
		  log.debug("checkSaveBaskets");
	  }
	  try {
		  saveBaskets = (getBackendService()).checkSaveBasketsInBackend();
	  }
	  catch (BackendException ex) {
		  BusinessObjectHelper.splitException(ex);
	  }
	  return saveBaskets;
  }  
    /**
     * @return
     */
    public ArrayList getLastEnteredCVVS() {
        return lastEnteredCVVS;
    }

    /**
     * @param list
     */
    public void setLastEnteredCVVS(ArrayList lastEnteredCVVS) {
        this.lastEnteredCVVS = lastEnteredCVVS;
    }

    
    public void setIdMapper(IdMapper idMapper) {
    	this.idMapper = idMapper;
    }
    
    
    protected void setProductId(SalesDocument posd, ItemSalesDoc newItem, ItemSalesDoc oldItem) 
        throws CommunicationException {
		
    	if (idMapper == null) {
          // do it the normal way		
    	  newItem.setProductId(oldItem.getProductId());
    	}
    	else { 
    	  // use the identifier mapping depending on its backend implementation	
    	  newItem.setProductId(new TechKey(idMapper.mapIdentifier(IdMapper.PRODUCT,               // object to map id for 
	   	   			                                  posd.getHeader().getDocumentType(),         // source  
                                                      this.getHeader().getDocumentType(),         // target 
 								                      oldItem.getProductId().getIdAsString())));  // source id
    	}
    }
    
    /**
     * Sets the document type on the document header depending on the Java class instance of the 
     * Sales Document (evaluated with <i><code>instanceof</code></i>).
     */
    protected void adaptHeaderDocumentType() {
        if (this instanceof Basket) {
            this.getHeaderBase().setDocumentType(HeaderData.DOCUMENT_TYPE_BASKET);
        } else if (this instanceof Quotation) {
            this.getHeaderBase().setDocumentType(HeaderData.DOCUMENT_TYPE_QUOTATION);
        } else if (this instanceof Order) {
            this.getHeaderBase().setDocumentType(HeaderData.DOCUMENT_TYPE_ORDER);
        } else if (this instanceof CollectiveOrder) {
            this.getHeaderBase().setDocumentType(HeaderData.DOCUMENT_TYPE_COLLECTIVE_ORDER);
        } else if (this instanceof OrderTemplate) {
            this.getHeaderBase().setDocumentType(HeaderData.DOCUMENT_TYPE_ORDERTEMPLATE);
        } else if (this instanceof Lead) {
            this.getHeaderBase().setDocumentType(HeaderData.DOCUMENT_TYPE_LEAD);
        } else if (this instanceof NegotiatedContract) {
            this.getHeaderBase().setDocumentType(HeaderData.DOCUMENT_TYPE_NEGOTIATED_CONTRACT);
        }
    }
}
