	/*****************************************************************************
    Class:        Schedline
    Copyright (c) 2002, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAPMarkets Europe GmbH
    Created:      April 2002
    Version:      1.0

    $Revision: #0 $
    $Date: 2002/04/25 $
*****************************************************************************/

package com.sap.isa.businessobject;

import com.sap.isa.backend.boi.isacore.SchedlineData;


/**
 * The <code>Schedline</code> represents the schedule line info which is delivered
 * by the backend system.<p>
 *
 * <i>For further details see the interface <code>SchedlineData</code></i>
 *
 * @author SAPMarkets Europe GmbH
 * @version 1.0
 */

public class Schedline extends BusinessObjectBase
                               implements SchedlineData{

    private String committedDate = "";
    private String committedQuantity = "";

    public Schedline() {
    }

    /**
     * Retrieves the committed date of the schedule line
     */
    public String getCommittedDate() {
        return committedDate;
    }

    /**
     * Sets the committed date of the schedule line
     */
     public void setCommittedDate(String committedDate) {
        this.committedDate = committedDate;
    }

        /**
     * Retrieves the committed quantity of the schedule line
     */
    public String getCommittedQuantity() {
        return committedQuantity;
    }

    /**
     * Sets the committed quantity of the schedule line
     */
     public void setCommittedQuantity(String committedQuantity) {
        this.committedQuantity = committedQuantity;
    }

}