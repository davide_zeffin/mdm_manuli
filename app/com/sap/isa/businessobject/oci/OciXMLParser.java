/*****************************************************************************
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP AG
    Created:      10 Feb 2003

    $Revision: #1 $
    $Date: 2001/08/02 $
*****************************************************************************/
package com.sap.isa.businessobject.oci;

import java.io.StringReader;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import com.sap.isa.businessobject.BusinessObjectBase;

/**
 * This one is the abstract base class of the Oci-XML-Parsers.
 * 
 */
public abstract class OciXMLParser extends BusinessObjectBase {
    protected boolean debug;

    protected String    xmlString;

    public OciXMLParser (String xmlString) {
        this.xmlString = xmlString;
        debug   = log.isDebugEnabled(); 
    }
    
    public Document setUpOciDocument() {
		final String METHOD = "setUpOciDocument()";
		log.entering(METHOD);
        Document document = null;

        try {
            //Set the JAXP-Property specifying which DocumentBuilderFactory implementation to use
            //System.setProperty("javax.xml.parsers.DocumentBuilderFactory", "com.inqmy.lib.jaxp.DocumentBuilderFactoryImpl");

            //get a DocumentBuilderFactory from the underlying implementation
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setValidating(true);
            //get a DocumentBuilder from the factory
            DocumentBuilder builder = factory.newDocumentBuilder();

            StringReader sr = new StringReader(xmlString);
            InputSource is = new InputSource(sr);

            //parse the document
            document = builder.parse(is);
        }
        catch (Exception ex) {
            //if there was some error while parsing the file
            if (debug) {
                log.debug(ex);
            }
            document = null;
        } finally {
        	log.exiting();
        }
        return document;
    }
    
    public abstract void parseOciDocument(Document document, List itemList);

}
