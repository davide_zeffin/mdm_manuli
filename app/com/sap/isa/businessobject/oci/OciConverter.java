/*****************************************************************************
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP AG
    Created:      13 September 2001

    $Revision: #1 $
    $Date: 2001/08/02 $
*****************************************************************************/
package com.sap.isa.businessobject.oci;

// some businessobjects
import java.util.List;
import java.util.Locale;

import com.sap.isa.backend.BackendTypeConstants;
import com.sap.isa.backend.boi.isacore.oci.OciConverterBackend;
import com.sap.isa.backend.boi.isacore.oci.OciConverterData;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.businessobject.BusinessObjectException;
import com.sap.isa.businessobject.BusinessObjectHelper;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
/**
 * The OciParser class provides some parsing functionality for the OCI
 * It contains a method for parsing HTML-requests and XML-requests.
 * It MUST be instantiated from within some object that can pass a reference to
 * the bem to the OciConverter.
 */
public class OciConverter extends BusinessObjectBase 
                          implements BackendAware, OciConverterData
{

	protected OciConverterBackend backendService = null;
	protected BackendObjectManager bem;

	
	int iRet;
	
    /**
     * Constructor.
     */
    public OciConverter() {
        /* ich bin */ super(); /* :-) */
    }

	/**
	 * Convert the units
	 *
	 * @param locale The locale to use
	 * @param itemList List of the passed items in the OCI-Request
	 * @param boex The object to tie any errors or warnings to
	 * @return ret 0 if ok, < 0 if an error occurred
	 */
	public int convertUnits(Locale locale, List itemList, BusinessObjectException boex, Shop shop)
			throws CommunicationException {
				
		final String METHOD = "convertUnits()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("locale="+locale+", shop="+shop);	
		try {
			
			iRet = getBackendService().convertUnitsInBackend(locale, itemList, boex, shop);
			
		}
		catch (BackendException ex) {
			BusinessObjectHelper.splitException(ex);
		}
		finally {
		
			log.exiting();
		}
		return iRet;
	}    
    
    
    /**
     * Sets the BackendObjectManager for the basket object. This method is used
     * by the object manager to allow the user object interaction with
     * the backend logic. This method is normally not called
     * by classes other than BusinessObjectManager.
     *
     * @param bem BackendObjectManager to be used
     */
    public void setBackendObjectManager(BackendObjectManager bem) {
        this.bem = bem;
    }
  


	/**
	 * Method retrieving the backend object for the object.
	 *
	 * @return Backend object to be used
	 */
	protected OciConverterBackend getBackendService()
			throws BackendException {
	
		synchronized (this) {
			if (backendService == null) {
				backendService = (OciConverterBackend) 
				bem.createBackendBusinessObject(BackendTypeConstants.BO_TYPE_OCICONVERTER, null);
			}
		}
	
		return backendService;
	}
	
	
	/**
	 * Callback method for the <code>BusinessObjectManager</code> to tell
	 * the object that life is over and that it has to release
	 * all ressources.
	 */
	public void destroy() {
		super.destroy();
		backendService = null;
	}
  
}

