/*****************************************************************************
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      10 May 2000

    $Revision: #6 $
    $Date: 2001/07/30 $
*****************************************************************************/
package com.sap.isa.businessobject.oci;

import com.sap.isa.backend.BackendTypeConstants;
import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.oci.OciDataObjectFactoryBackend;
import com.sap.isa.backend.boi.isacore.oci.OciServerBackend;
import com.sap.isa.backend.boi.isacore.oci.OciVersion;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.businessobject.BusinessObjectHelper;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;

/**
 * The OciServer class is used to export data of baskets and / or quotations
 * using the open catalog interface in the B2B Internet Sales scenario.
 */
public class OciServer extends BusinessObjectBase
                       implements BackendAware,
                                  BackendBusinessObjectParams {

    protected static final OciDataObjectFactoryBackend
        OCI_DATA_OBJECT_FACTORY = new OciDataObjectFactory();

    protected OciServerBackend OciServerBackend;
    protected BackendObjectManager bem;
    protected String hookUrl;
    protected OciVersion ociVersion;

    /**
     * Constructor.
     */
    public OciServer() {
        super();
    }

    /**
     * Sets the BackendObjectManager for the attribute set. This method is used
     * by the object manager to allow the user object interaction with
     * the backend logic. This method is normally not called
     * by classes other than BusinessObjectManager.
     *
     * @param bem BackendObjectManager to be used
     */
    public void setBackendObjectManager(BackendObjectManager bem) {
        this.bem = bem;
    }

    /**
     * Set the hook URL of the oci server.
     */
    public void setHookUrl(String hookUrl) {
        this.hookUrl = hookUrl;
    }

    /**
     * Retrieves the hook URL of the oci server.
     */
    public String getHookUrl() {
        return hookUrl;
    }

    /**
     * Set the oci version of the oci server.
     */
    public void setOciVersion(String ociVersion) {
        this.ociVersion = OciVersion.VERSION_2_0;
        if (ociVersion != null) {

            // HTML case : default or explicitly stated
            if (ociVersion.length() == 0 ||
                ociVersion.equals(OciVersion.VERSION_1_0.toString())) {
                this.ociVersion = OciVersion.VERSION_1_0;
            }

            // XML case : everyting that isn't "" or "1.0"
            else {
                this.ociVersion = OciVersion.VERSION_2_0;
            }
        }

        // ??? - should never happen, admittedly
        else {
            this.ociVersion = OciVersion.VERSION_1_0;
        }
    }

    /**
     * Retrieves the oci version of the oci server.
     */
    public String getOciVersion() {
        return ociVersion.toString();
    }

    /**
     * Read lines of the oci representation of the given sales document.
     *
     */
    public OciLineList readOciLines(
            TechKey salesDocumentKey,
            String language) throws CommunicationException {
            	
		final String METHOD = "readOciLines()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("salesDocumentKey="+salesDocumentKey+", lang="+language);
        OciLineList ociLineList = null;

        // read oci lines from backend
        try {
            ociLineList = (OciLineList)getOciServerBackend().readOciLines(
                salesDocumentKey,
                language,
                hookUrl,
                ociVersion);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally {
        	log.exiting();
        }
        return ociLineList;
    }

	/**
	 * Reads lines of the oci representation of the given sales document. Uses
	 * the ISA sales document and not just the techkey (for the use in the
	 * JavaBasket or ISA R/3 context when there is no ABAP backend being aware of
	 * the document).
	 * @param salesDocument the ISA sales document
	 * @param language the current language
	 * @return the OCI output
	 */
	public OciLineList readOciLines(
			SalesDocumentData salesDocument,
			String language) throws CommunicationException {
            	
		final String METHOD = "readOciLines()";
		log.entering(METHOD);
		OciLineList ociLineList = null;

		// read oci lines from backend
		try {
			ociLineList = (OciLineList)getOciServerBackend().readOciLines(
				salesDocument,
				language,
				hookUrl,
				ociVersion);
		}
		catch (BackendException ex) {
			BusinessObjectHelper.splitException(ex);
		}
		finally {
			log.exiting();
		}
		return ociLineList;
	}

    // get the OciServerBackend, if necessary
    protected OciServerBackend getOciServerBackend()
            throws BackendException {
        if (OciServerBackend == null) {
            OciServerBackend = (OciServerBackend)
                bem.createBackendBusinessObject(BackendTypeConstants.BO_TYPE_OCI_SERVER, OCI_DATA_OBJECT_FACTORY);
        }
        return OciServerBackend;
    }
}
