/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      10 May 2001

    $Revision: #3 $
    $Date: 2001/07/30 $
*****************************************************************************/
package com.sap.isa.businessobject.oci;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.sap.isa.backend.boi.isacore.oci.OciLineListData;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.core.Iterable;

/**
 * Represents a list of <code>OciLine</code>s of the oci.
 */
public class OciLineList extends BusinessObjectBase
                         implements OciLineListData, Iterable {
    protected List ociLines;

    /**
     * Creates a new <code>OciLineList</code> object.
     */
    public OciLineList() {
        ociLines = new ArrayList();
    }

    /**
     * Adds a new <code>OciLine</code> to the list.
     */
    public void add(String ociLine) {
        ociLines.add(ociLine);
    }

    /**
     * Returns the element at the specified position in this list.
      */
    public String get(int index) {
        return (String) ociLines.get(index);
    }

    /**
     * Returns the number of elemts in this list.
     */
    public int size() {
        return ociLines.size();
    }

    /**
     * Returns true if this list contains no data.
     */
    public boolean isEmpty() {
        return ociLines.isEmpty();
    }

    /**
     * Returns an iterator over the elements contained in the list
     */
    public Iterator iterator() {
        return ociLines.iterator();
    }

    /**
     * Converts the data of the list into a string representation.
     * The use of this method is limited to debugging purposes only, because
     * the performance is poor.
     *
     * @return String representation of the list
     */
    public String toString() {
        StringBuffer bufData = new StringBuffer();
        for (int i = 0; i< size(); i++) {
            bufData.append(i + " : ");
            bufData.append(get(i));
            bufData.append("\n");
        }
        return bufData.toString();
    }
}
