/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      10 May 2001

    $Revision: #2 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.businessobject.oci;

import com.sap.isa.backend.boi.isacore.oci.OciDataObjectFactoryBackend;
import com.sap.isa.backend.boi.isacore.oci.OciLineListData;

/**
 * Class used for the creation of oci data objects within the BE.
 */
public class OciDataObjectFactory
       implements OciDataObjectFactoryBackend {

    /**
     * Creates a new <code>OciLineListData</code> object.
     *
     * @return The newly created object
     */
    public OciLineListData createOciLineListData() {
        return (OciLineListData)(new OciLineList());
    }
}