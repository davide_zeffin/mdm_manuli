/*****************************************************************************
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP AG
    Created:      27 July 2001

    $Revision: #1 $
    $Date: 2001/08/02 $
*****************************************************************************/
package com.sap.isa.businessobject.oci;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.sap.isa.businessobject.item.BasketTransferItemImpl;
import com.sap.isa.core.logging.IsaLocation;


/**
 * @author d025715
 *
 * The Oci20XMLParser parses a XML document based on the OCI version 2.0.
 * It uses a DOM to parse the XML-data in the String. Currently all the
 * parsing is really made "by the hand on the arm". I will make it a little more
 * elegant and flexible sometime.
 *
 */
public class Oci20XMLParser extends OciXMLParser {
	static final private IsaLocation loc = IsaLocation.getInstance(Oci20XMLParser.class.getName());

	/**
	 * used to specify transfer item source
	 * 
	 */
	protected final static String TRANSFER_ITEM_SOURCE = "OCI";
	
    /**
     *  Constructor for the DomParse object
     */
    public Oci20XMLParser(String xmlDocument) {
        super(xmlDocument);
    }

    /**
     *  Description of the Method
     *
     *@param  document  Description of Parameter
     */
    public void parseOciDocument(Document document, List itemList) {
        // we need :
        //  quantity, uom, matnr, contractid, contractitemid and vendormatnr
        // we can find it in :
        //  <BusinessDocument><Catalog><Product><VendorDescription><BuyerContract><ContractID>
        //  <BusinessDocument><Catalog><Product><VendorDescription><BuyerContract><ContractItemID>
        //  <BusinessDocument><Catalog><Product><ShoppingBasketItem><Quantity UoM = "xy">
        //  <BusinessDocument><Catalog><Product><ProductID>
        //  <BusinessDocument><Catalog><Product><ManufacturerDescription><PartnerProductID>
        //  <BusinessDocument><Catalog><Product><VendorDescription><PartnerProductID>
		final String METHOD = "parseOciDocument()";
		loc.entering(METHOD);
        try {
            Element root = (Element) document.getElementsByTagName("BusinessDocument").item(0);

            // get the next paragraph, the Catalog
            NodeList catalogs = root.getElementsByTagName("Catalog");

            if ((catalogs != null) && (catalogs.getLength() > 0)) {
                // get the productct data
                Element  catalog  = (Element) catalogs.item(0);
                NodeList products = catalog.getElementsByTagName("Product");

                if ((products != null) && (products.getLength() > 0)) {
                    // iterate over all items
                    for (int i = 0; i < products.getLength(); i++) {
                        // create a BasketTransferItem and store the values
                        BasketTransferItemImpl item = new BasketTransferItemImpl();
                        item.setSource(TRANSFER_ITEM_SOURCE);
                        
                        Element                product = (Element) products.item(i);

                        // get the contract data 
                        NodeList vendorDescs = product.getElementsByTagName("VendorDescription");

                        if ((vendorDescs != null) && (vendorDescs.getLength() > 0)) {
                            Element  vendorDesc = (Element) vendorDescs.item(0);

                            NodeList buyerContracts = vendorDesc.getElementsByTagName(
                                    "BuyerContract");

                            if ((buyerContracts != null) && (buyerContracts.getLength() > 0)) {
                                Element  buyerContract = (Element) buyerContracts.item(0);

                                NodeList contractIDs = buyerContract.getElementsByTagName(
                                        "ContractID");

                                if ((contractIDs != null) && (contractIDs.getLength() > 0)) {
                                    Node contractId = (Node) contractIDs.item(0);

                                    // get the text of this element, which is our value to use
                                    Node ourValue = contractId.getFirstChild();

                                    if (debug) {
                                        loc.debug("found contractId with name : " +
                                            contractId.getNodeName() + " and value : " +
                                            ourValue.getNodeValue());
                                    }

                                    item.setContractKey(ourValue.getNodeValue());
                                }
                                 // contractId is not mandatory

                                NodeList contractItemIDs = buyerContract.getElementsByTagName(
                                        "ContractItemID");

                                if ((contractItemIDs != null) && (contractItemIDs.getLength() > 0)) {
                                    Node contractItemId = (Node) contractItemIDs.item(0);

                                    // get the text of this element, which is our value to use
                                    Node ourValue = contractItemId.getFirstChild();

                                    if (debug) {
                                        loc.debug("found contractId with name : " +
                                            contractItemId.getNodeName() + " and value : " +
                                            ourValue.getNodeValue());
                                    }

                                    item.setContractKey(ourValue.getNodeValue());
                                }
                                 // contractItemId is not mandatory
                            }
                             // endif buyerContracts
                        }
                         // VendorDescription is not mandatory
                    }

                    // endfor
                }
                else {
                    // Error. product is mandatory
                }
            }
            else {
                // Error. catalog is mandatory
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        } finally {
        	loc.exiting();
        }
    }

    /**
     *  The main program for the Oci20XMLParser class
     *
     *@param  args  The command line arguments
     */
    public static void main(String[] args) {
        String       fileName  = args[ 0 ];
        StringBuffer sb        = new StringBuffer();
        String       xmlString = null;

        try {
            BufferedReader br   = new BufferedReader(new FileReader(fileName));
            String         line;

            while ((line = br.readLine()) != null) {
                //                System.out.println(line);
                sb.append(line);
            }

            br.close();

            xmlString = sb.toString();

            //            System.out.println(xmlString);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        Oci20XMLParser ociP = new Oci20XMLParser(xmlString);

        /*
                DOMTraverser domTester = new DOMTraverser();
                //use this internal class to recurse the children in the document
                domTester.recurseChildren(document, "");
        */
    }

    /**
     *  Description of the Method
     *
     *@param  node    Description of Parameter
     *@param  indent  Description of Parameter
     */
    private static void printNode(Node node, int indent) {
        for (int i = 0; i < indent; i++) {
            System.out.print("   ");
        }

        int type = node.getNodeType();

        switch (type) {
        case Node.ATTRIBUTE_NODE:
            System.out.println("ATTRIBUTE_NODE : " + node.getLocalName() + " : " +
                node.getNodeName() + " : value = " + node.getNodeValue());

            break;

        case Node.CDATA_SECTION_NODE:
            System.out.println("CDATA_SECTION_NODE");

            break;

        case Node.COMMENT_NODE:
            System.out.println("COMMENT_NODE");

            break;

        case Node.DOCUMENT_FRAGMENT_NODE:
            System.out.println("DOCUMENT_FRAGMENT_NODE");

            break;

        case Node.DOCUMENT_NODE:
            System.out.println("DOCUMENT_NODE : " + node.getNodeName());

            break;

        case Node.DOCUMENT_TYPE_NODE:
            System.out.println("DOCUMENT_TYPE_NODE");

            break;

        case Node.ELEMENT_NODE:
            System.out.println("ELEMENT_NODE : " + node.getNodeName());

            NamedNodeMap atts = node.getAttributes();

            for (int i = 0; i < atts.getLength(); i++) {
                Node att = atts.item(i);
                printNode(att, indent + 1);
            }

            break;

        case Node.ENTITY_NODE:
            System.out.println("ENTITY_NODE");

            break;

        case Node.ENTITY_REFERENCE_NODE:
            System.out.println("ENTITY_REFERENCE_NODE");

            break;

        case Node.NOTATION_NODE:
            System.out.println("NOTATION_NODE");

            break;

        case Node.PROCESSING_INSTRUCTION_NODE:
            System.out.println("PROCESSING_INSTRUCTION_NODE");

            break;

        case Node.TEXT_NODE:
            System.out.println("TEXT : " + node.getNodeValue());

            break;

        default:
            System.out.println("???");

            break;
        }

        for (Node c = node.getFirstChild(); c != null; c = c.getNextSibling()) {
            printNode(c, indent + 1);
        }
    }
}
