/**
 *  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
 *  Author:       SAP AG
 *  Created:      27 July 2001
 *  $Revision: #1 $
 *  $Date: 2001/08/02 $
 */
package com.sap.isa.businessobject.oci;

// core java
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import com.sap.isa.businessobject.BusinessObjectException;
import com.sap.isa.businessobject.item.BasketTransferItemImpl;
import com.sap.isa.core.logging.IsaLocation;


/**
 *  The OciSaxHandler class provides the handler for the XML-SAX-parsing-thing
 *  in OciParser.java.
 *  Depending on the value of the indentLevel variable the XML contains values for
 *  the shoppingBasketItems. Following the rules defined in the appropriate DTD and/or
 *  Schema the hierarchy is:
 *  <BusinessDocument>.<Catalog>.<Product>.<ProductID>
 *  <ShoppingBasketItem>.<Quantity UoM = "EA">1</Quantity>
 *
 *@author     d025715
 *@created    20. September 2002
 */
public class OciSaxHandler extends DefaultHandler {
	static final private IsaLocation loc = IsaLocation.getInstance(OciSaxHandler.class.getName());

    /**
     *  Name of the tag which contains the shopping basket item
     */
    public String BASKET_ITEM = "ShoppingBasketItem";
    /**
     *  Name of the tag which contains the product id of the shopping basket item
     */
    public String PRODUCT = "Product";
    /**
     *  Name of the tag which contains the product id of the shopping basket item
     */
    public String PRODUCT_ID = "ProductID";
    /**
     *  Name of the tag which contains the quantity of the shopping basket item
     */
    public String QUANTITY = "Quantity";
    /**
     *  Name of the attribute which contains the unit of
     *  mesasurement of the shopping basket item
     */
    public String UOM = "UoM";
    /**
     * used to specify transfer item source
     */
    protected String TRANSFER_ITEM_SOURCE = "OCI";
    /**
     *  Description of the Field
     */
    protected BusinessObjectException boex = null;

    /**
     *  actual content of tag
     */
    protected String tagValue = null;

    /**
     *  actual item
     */
    protected BasketTransferItemImpl item = null;

    /**
     *  The base class provide reference to a log location<br>
     *
     *  <i>By default the constructor get an Instance of com.sapmarket.isa.businessobject </i>
     */
    protected IsaLocation log;

    // some variables for the XML-parsing
    private int indentLevel = 0;
    private Locator locator = null;
    private List items = null;


    /**
     *  Constructor.
     *
     *@param  boex   A reference to the BusinessObjectException that is used to hold
     *  all the messages
     *@param  items  Description of Parameter
     */
    public OciSaxHandler(BusinessObjectException boex, List items) {
        this.boex = boex;
        this.log = IsaLocation.getInstance(this.getClass().getName());
        this.items = items;
    }


    //===========================================================
    // SAX DocumentHandler methods
    //===========================================================

    /**
     *  setDocumentLocator, the locator callback for the parser
     *
     *@param  l        The new DocumentLocator value
     */
    public void setDocumentLocator(Locator l) {
        // Save this to resolve relative URIs or to give diagnostics.
        this.locator = l;
        if (loc.isDebugEnabled()) {
            loc.debug("LOCATOR : SYS ID: " + l.getSystemId());
        }
    }


    /**
     *  startDocument, the entry point
     *
     *@exception  SAXException  Description of Exception
     */
    public void startDocument()
             throws SAXException {
        if (loc.isDebugEnabled()) {
            loc.debug("START DOCUMENT");
        }
    }


    /**
     *  endDocument, the place we want to reach in the end
     *
     *@exception  SAXException  Description of Exception
     */
    public void endDocument()
             throws SAXException {
        if (loc.isDebugEnabled()) {
            loc.debug("END DOCUMENT");
        }
    }


    /**
     *  startElement, a callback for the SAX parser
     *
     *@param  tag
     *@param  attrs
     *@exception  SAXException  Description of Exception
     */
    public void startElement(String tag, Attributes attrs)
             throws SAXException {
             	
		final String METHOD = "startElement()";
		loc.entering(METHOD);
        indentLevel++;

        if (loc.isDebugEnabled()) {
            loc.debug("ELEMENT: " + tag);
        }

        // create a BasketTransferItem and add it to the list
        if (PRODUCT.equals(tag)) {
            item = new BasketTransferItemImpl();
            item.setSource(TRANSFER_ITEM_SOURCE);
            items.add(item);
        }

        if (attrs != null) {
            for (int i = 0; i < attrs.getLength(); i++) {
                
                // This is the XML 1.0 attributeName
                // if we want to support namespaces we need to change this
                String aName = attrs.getQName(i);
                // Attr name
                if ("".equals(aName)) {
                    aName = "EMPTY";
                }

                // get UoM as an attribute of the quantity
                if (QUANTITY.equals(tag) && UOM.equals(aName)) {
                    item.setUnit(attrs.getValue(i));
                }

                if (loc.isDebugEnabled()) {
                    loc.debug("   ATTRNAME: " + aName + " ATTRVALUE : " + attrs.getValue(i));
                }
            }
        }
        loc.exiting();
    }


    /**
     *  endElement, a callback for the SAX parser
     *
     *@param  tag               Description of Parameter
     *@exception  SAXException  Description of Exception
     */
    public void endElement(String tag) throws SAXException {
		final String METHOD = "endElement()";
		loc.entering(METHOD);
        // get the product id
        if (PRODUCT_ID.equals(tag)) {
            item.setProductId(tagValue);
        }

        // get the quantity
        if (QUANTITY.equals(tag)) {
            StringBuffer dest = new StringBuffer();
            if (OciParser.getSafeQuantity(tagValue, dest) == OciParser.OCI_PARSE_SUCCESS) {
                item.setQuantity(dest.toString());
            }
            else {
                item.setQuantity("invalid");
                // anything else ?
            }
        }

        if (loc.isDebugEnabled()) {
            loc.debug("END ELEMENT : " + tag);
        }
        indentLevel--;
        loc.exiting();
    }


    /**
     *  characters, a callback for the SAX parser
     *
     *@param  offset
     *@param  len
     *@param  buf               Description of Parameter
     *@exception  SAXException  Description of Exception
     */
    public void characters(char buf[], int offset, int len)
             throws SAXException {
        tagValue = new String(buf, offset, len);
        if (tagValue.trim().equals("")) {
            tagValue = "<empty>";
        }

        if (loc.isDebugEnabled()) {
            loc.debug("CHARS : " + tagValue);
        }
    }


    /**
     *  ignorableWhitespace, a callback for the validating SAX parser
     *
     *@param  offset
     *@param  len
     *@param  buf               Description of Parameter
     *@exception  SAXException  Description of Exception
     */
    public void ignorableWhitespace(char buf[], int offset, int len)
             throws SAXException {
        // Ignore it
    }


    /**
     *  processingInstruction, a callback for the validating SAX parser
     *
     *@param  target
     *@param  data
     *@exception  SAXException  Description of Exception
     */
    public void processingInstruction(String target, String data)
             throws SAXException {
        if (loc.isDebugEnabled()) {
            loc.debug("PROCESS : <?" + target + " " + data + "?>");
        }
    }


    //===========================================================
    // SAX ErrorHandler methods
    //===========================================================

    /**
     *  error, a callback for the validating SAX parser
     *
     *@param  e
     *@exception  SAXParseException  Description of Exception
     */
    public void error(SAXParseException e)
             throws SAXParseException {
        throw e;
    }


    /**
     *  warning, a callback for the validating SAX parser
     *
     *@param  err
     *@exception  SAXParseException  Description of Exception
     */
    public void warning(SAXParseException err)
             throws SAXParseException {
        if (loc.isDebugEnabled()) {
            loc.debug("** Warning" + ", line " + err.getLineNumber()
                     + ", uri " + err.getSystemId());
            loc.debug("   " + err.getMessage());
        }
    }
}

