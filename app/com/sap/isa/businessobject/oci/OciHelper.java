/*****************************************************************************
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Alexander Staff
    Created:      27 July 2001

    $Revision: #1 $
    $Date: 2001/08/02 $
*****************************************************************************/
package com.sap.isa.businessobject.oci;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.PageContext;

import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.core.util.WebUtil;

/**
 * The OciHelper class provides some useful functions to
 * handle all the stuff related to the oci that can not be clearly assigned 
 * to some handy JAVA-class like the oci-parser-class or something like this.
 */
public class OciHelper extends BusinessObjectBase {

    public static   String hook_action_name = "HOOK_URL";
    public static   String target_name      = "~TARGET";
    public static   String version_name     = "OCI_VERSION";
    public static   String form_name        = "~FORM";
    public static   String caller_name      = "~CALLER";
    public static   String okcode_name      = "~OkCode";

    public static   String oci_hook_action  = "b2b/ocireceive.do";
    public static   String oci_caller       = "CTLG";
    public static   String oci_okcode       = "ADDI";

    /*
     * reads the oci-settings from the web.xml file and constructs the
     * oci-calling-url from them, incl. encodeURL, webappsURL, ...
     *
     * @param context The pageContext from the calling jsp
     * @return the constructed url
     *
     */
    public static String getOciUrl(PageContext pageContext) {

        String          ociUrl  = null;
        ServletContext  context = pageContext.getServletContext();

        String catalogUrl       = context.getInitParameter("catalog.oci.isacore.isa.sap.com");
        String oci_target       = context.getInitParameter("target.oci.isacore.isa.sap.com");
        String oci_version      = context.getInitParameter("version.oci.isacore.isa.sap.com");
        String oci_form         = context.getInitParameter("form.oci.isacore.isa.sap.com");

        // construct the catalog-url-suffix from the arguments
        String completeURL = WebUtil.getAppsURL(pageContext, new Boolean("false"), oci_hook_action, null, null, true);
        ociUrl = catalogUrl + "?" + hook_action_name + "=" + completeURL;

        ociUrl += "&" + target_name + "=" + oci_target;
        ociUrl += "&" + version_name + "=" + oci_version;
        ociUrl += "&" + form_name + "=" + oci_form;
        ociUrl += "&" + caller_name + "=" + oci_caller;
        ociUrl += "&" + okcode_name + "=" + oci_okcode;

        return ociUrl;
    }

	/*
	 * reads the oci-settings from the web.xml file and constructs the
	 * oci-calling-url from them, incl. encodeURL, webappsURL, ...
	 *
	 * @param context The pageContext from the calling jsp
	 * @return the constructed url
	 *
	 */
	public static String getOciUrl(ServletContext servletContext,
									 HttpServletRequest request,
								     HttpServletResponse response) {
								     	
		String          ociUrl  = null;
		
		String catalogUrl       = servletContext.getInitParameter("catalog.oci.isacore.isa.sap.com");
		String oci_target       = servletContext.getInitParameter("target.oci.isacore.isa.sap.com");
		String oci_version      = servletContext.getInitParameter("version.oci.isacore.isa.sap.com");
		String oci_form         = servletContext.getInitParameter("form.oci.isacore.isa.sap.com");

		// construct the catalog-url-suffix from the arguments
		String completeURL = WebUtil.getAppsURL(servletContext, request, response, new Boolean(request.isSecure()), oci_hook_action, null, null, true);
		ociUrl = catalogUrl + "?" + hook_action_name + "=" + completeURL;

		ociUrl += "&" + target_name + "=" + oci_target;
		ociUrl += "&" + version_name + "=" + oci_version;
		ociUrl += "&" + form_name + "=" + oci_form;
		ociUrl += "&" + caller_name + "=" + oci_caller;
		ociUrl += "&" + okcode_name + "=" + oci_okcode;

		return ociUrl;
	}

    /**
     * Constructor.
     */
    public OciHelper() {
        /* ich bin */ super(); /* :-) */
    }

}
