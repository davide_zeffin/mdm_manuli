/*****************************************************************************
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP AG
    Created:      10 Feb 2003

    $Revision: #1 $
    $Date: 2001/08/02 $
*****************************************************************************/
package com.sap.isa.businessobject.oci;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.sap.isa.core.logging.IsaLocation;

/**
 *  This class recurses all the Element children of a node and prints the Text - content
 *
 *@author     SAP AG
 *@created    5. Februar 2003
 */
public class DOMTraverser {
	static final private IsaLocation loc = IsaLocation.getInstance(DOMTraverser.class.getName());

    /**
     *  Description of the Method
     *
     *@param  node  Description of Parameter
     */
    public void recurseChildren(Node node, String indent) {
		final String METHOD = "recurseChildren()";
		loc.entering(METHOD);
		if (loc.isDebugEnabled())
			loc.debug("node="+node+", indent="+indent);
        //Get the children of this Node
        NodeList children = node.getChildNodes();
        
        //go through all the children of the node
        for (int i = 0; i < children.getLength(); i++) {
            //get the next child
            Node child = children.item(i);
            //get the type of the child
            short childType = child.getNodeType();
            if (childType == Node.ELEMENT_NODE) {
                //if the child is an Element then print the start and end tags and recurse the content
                String nodeName = child.getNodeName();
                System.out.print(indent +"<" + nodeName + ">");
                recurseChildren(child, indent + "    ");
                System.out.println(indent + "</" + nodeName + ">");
            }
            else if (childType == Node.TEXT_NODE) {
                //if the child is a Text node just print the text content

                String data = child.getNodeValue();
                loc.debug("This is " + data);
            }
        }
        loc.exiting();
    }
}
