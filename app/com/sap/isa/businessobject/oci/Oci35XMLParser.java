/*****************************************************************************
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP AG
    Created:      27 July 2001

    $Revision: #1 $
    $Date: 2001/08/02 $
*****************************************************************************/
package com.sap.isa.businessobject.oci;

import java.io.BufferedReader;
import java.io.FileReader;
import java.math.BigDecimal;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.businessobject.item.BasketTransferItemImpl;
import com.sap.isa.core.logging.IsaLocation;


/**
 * @author d025715
 *
 * The Oci20XMLParser parses a XML document based on the OCI version 2.0.
 * It uses a DOM to parse the XML-data in the String. Currently all the
 * parsing is really made "by the hand on the arm". I will make it a little more
 * elegant and flexible sometime.
 *
 */
public class Oci35XMLParser extends OciXMLParser {
	
	// some constants to describe error situations
	private static final int OCI_PARSE_SUCCESS             = 0;
	private static final int OCI_NO_XML_DOCUMENT           = -1;	
	
	// constant to specify the item transfer source
	private static final String OCI_IS_SOURCE              = "OCI";
	
	
	static final private IsaLocation loc = IsaLocation.getInstance(Oci35XMLParser.class.getName());

    /**
     *  Constructor for the DomParse object
     */
    public Oci35XMLParser(String xmlDocument) {
        super(xmlDocument);
    }

    /**
     *  Description of the Method
     *
     *@param  document  Description of Parameter
     *@param  itemList  The list of items to fill with the stuff coming from the
     *oci
     */
    public void parseOciDocument(Document document, List itemList) {
		final String METHOD = "parseOciDocument()";
		loc.entering(METHOD);
        try {
            // we need :
            //  quantity, uom, matnr, contractid, contractitemid and vendormatnr
            // we can find it in :
            //  <EBP:CatalogItems><CatalogItem><References><Contract><ReferenceID>
            //  <EBP:CatalogItems><CatalogItem><References><Contract><ReferenceItemID>
            //  <EBP:CatalogItems><CatalogItem><Quantity><Value>
            //  <EBP:CatalogItems><CatalogItem><Quantity><UoM>
            //  <EBP:CatalogItems><CatalogItem><Product><Identifier><ProductID>4711</ProductID>
            //  <EBP:CatalogItems><CatalogItem><Product><Identifier><ProductIDVendorAssigned>12354864</ProductIDVendorAssigned>
            //  <EBP:CatalogItems><CatalogItem><Product><Identifier><ProductIDManufacturerAssigned>77899694</ProductIDManufacturerAssigned>
            // get all the CatalogItems from the root (EBP:CatalogItems) and iterate over them
            Element  root = (Element) document.getElementsByTagName("EBP:CatalogItems").item(0);

            NodeList catalogItems = root.getElementsByTagName("CatalogItem");

            for (int i = 0;i < catalogItems.getLength();i++) {
                
                // create a BasketTransferItem and store the values
                BasketTransferItemImpl item = new BasketTransferItemImpl();
                item.setSource(OCI_IS_SOURCE);
                
                Element catalogItem = (Element) catalogItems.item(i);

                // get the contract data
                NodeList refData = catalogItem.getElementsByTagName("References");

                if ((refData != null) && (refData.getLength() > 0)) {
                    Element  ref = (Element) refData.item(0);

                    NodeList contracts = ref.getElementsByTagName("Contract");

                    if ((contracts != null) && (contracts.getLength() > 0)) {
                        Element  contract = (Element) contracts.item(0);
                        NodeList ids = contract.getElementsByTagName("ReferenceID");

                        if ((ids != null) && (ids.getLength() > 0)) {
                            Node contractId = (Node) ids.item(0);

                            // get the text of this element, which is our value to use
                            Node ourValue = contractId.getFirstChild();
                            if (ourValue != null) {
	                            if ( debug ) { 
	                                loc.debug("found contractId with name : " +
	                                contractId.getNodeName() + " and value : " +
	                                ourValue.getNodeValue());
	                            }
                            	item.setContractKey(ourValue.getNodeValue());
                            }
                            else {
                            	if (debug) {
                            		loc.debug("Contract -> ReferenceID is empty");
                            	}
                            }
                        }
                        else {
                            // Error. If we have a contract, we must have a ReferenceID/ContractID  
                        }

                        NodeList itemIds = contract.getElementsByTagName("ReferenceItemID");

                        if ((itemIds != null) && (itemIds.getLength() > 0)) {
                            Node contractItemId = (Node) itemIds.item(0);

                            // get the text of this element, which is our value to use
                            Node ourValue = contractItemId.getFirstChild();
							if (ourValue != null) {
	                            if ( debug ) { 
	                                loc.debug("found contractItemId with name : " +
	                                contractItemId.getNodeName() + " and value : " +
	                                ourValue.getNodeValue());
	                            }
	                            item.setContractItemKey(ourValue.getNodeValue());
							}
							else {
								if (debug) {
									loc.debug("Contract -> ReferenceItemID is empty");
								}								
							}
                        }
                        else {
                            // Error. If we have a contract, we must have a ReferenceItemID/ContractItemID  
                        }
                    }   // contracts are not mandatory
                }   // references are not mandatory

                // end of getting contract data
                // get the quantity
                NodeList quantityParams = catalogItem.getElementsByTagName("Quantity");

                if ((quantityParams != null) && (quantityParams.getLength() > 0)) {
                    Element  quantity = (Element) quantityParams.item(0);
                    NodeList values = quantity.getElementsByTagName("Value");

                    if ((values != null) && (values.getLength() > 0)) {
                        Node value = (Node) values.item(0);

                        // get the text of this element, which is our value to use
                        Node ourValue = value.getFirstChild();
                        if ( debug ) { 
                            loc.debug("found Quantity-Value with name : " +
                            value.getNodeName() + " and value : " + ourValue.getNodeValue());
                        }
                        item.setQuantity(ourValue.getNodeValue());
                    }
                    else {
                        // error, value is mandatory
                    }

                    NodeList uoms = quantity.getElementsByTagName("UoM");

                    if ((uoms != null) && (uoms.getLength() > 0)) {
                        Node uom = (Node) uoms.item(0);

                        // get the text of this element, which is our value to use
                        Node ourValue = uom.getFirstChild();
                        if ( debug ) { 
                            loc.debug("found Quantity-uom with name : " + uom.getNodeName() +
                            " and value : " + ourValue.getNodeValue());
                        }
                        item.setUnit(ourValue.getNodeValue());
                    }
                    else {
                        // error, uom is mandatory
                    }
                }
                else {
                    // error, quantity is mandatory
                }

                // end of getting quantity
                // get the productIds
                // difficult here, which one of the legions of productids do we need ?
                // let the user decide
                // "hey user, ..."
                NodeList prodData = catalogItem.getElementsByTagName("Product");

                if ((prodData != null) && (prodData.getLength() > 0)) {
                    Element  prod = (Element) prodData.item(0);

                    NodeList ids = prod.getElementsByTagName("Identifier");

                    if ((ids != null) && (ids.getLength() > 0)) {
                        Element  id         = (Element) ids.item(0);
                        NodeList productIds = id.getElementsByTagName("ProductID");

                        if ((productIds != null) && (productIds.getLength() > 0)) {
                            Node productId = (Node) productIds.item(0);

                            // get the text of this element, which is our value to use
                            Node ourValue = productId.getFirstChild();
                            if ( debug ) {
                                loc.debug("found productId with name : " +
                                productId.getNodeName() + " and value : " +
                                ourValue.getNodeValue());
                            }
                            item.setProductId(ourValue.getNodeValue());
                        }
                        else {
                            // error, productid is mandatory
                        }

                        NodeList vendorProdIds = id.getElementsByTagName("ProductIDVendorAssigned");

                        if ((vendorProdIds != null) && (vendorProdIds.getLength() > 0)) {
                            Node vendorItemId = (Node) vendorProdIds.item(0);

                            // get the text of this element, which is our value to use
                            Node ourValue = vendorItemId.getFirstChild();
                            if ( debug ) {
                                loc.debug("found vendorItemId with name : " +
                                vendorItemId.getNodeName() + " and value : " +
                                ourValue.getNodeValue());
                            }
                        //    item.set
                        }
                        else {
                            // error, vendor product number is mandatory
                        }

                        NodeList manufactProdIds = id.getElementsByTagName(
                                "ProductIDManufacturerAssigned");

                        if ((manufactProdIds != null) && (manufactProdIds.getLength() > 0)) {
                            Node manufactItemId = (Node) manufactProdIds.item(0);

                            // get the text of this element, which is our value to use
                            Node ourValue = manufactItemId.getFirstChild();
                            if ( debug ) {
                                loc.debug("found manufactItemId with name : " +
                                manufactItemId.getNodeName() + " and value : " +
                                ourValue.getNodeValue());
                            }
                            //    item.set
                        }
                        else {
                            // error, manufacturer product number is mandatory
                            // is it ? really ?
                        }
                    }
                    else {
                        // error, identifier is mandatory
                    }
                }
                else {
                    // error, product is mandatory
                }

                // end of getting the productid(s)
            }
        }
         catch (Exception e) {
            e.printStackTrace();
        }
        finally {
        	loc.exiting();
        }
    }


	/**
	 *  Description of the Method
	 *
	 *@param  document  Description of Parameter
	 *@param  itemList  The list of items to fill with the stuff coming from the
	 *oci
	 */
	public int parseXMLDocument(Document document, List itemList, ShopData shop) {
		final String METHOD = "parseXMLDocument()";
		loc.entering(METHOD);
		int ret = OCI_PARSE_SUCCESS;
		try {
			// we need :
			//  quantity, uom, matnr, contractid, contractitemid and vendormatnr
			// we can find it in :
			//  <EBP:CatalogItems><CatalogItem><References><Contract><ReferenceID>
			//  <EBP:CatalogItems><CatalogItem><References><Contract><ReferenceItemID>
			//  <EBP:CatalogItems><CatalogItem><Quantity><Value>
			//  <EBP:CatalogItems><CatalogItem><Quantity><UoM>
			//  <EBP:CatalogItems><CatalogItem><Product><Identifier><ProductID>4711</ProductID>
			//  <EBP:CatalogItems><CatalogItem><Product><Identifier><ProductIDVendorAssigned>12354864</ProductIDVendorAssigned>
			//  <EBP:CatalogItems><CatalogItem><Product><Identifier><ProductIDManufacturerAssigned>77899694</ProductIDManufacturerAssigned>
			// get all the CatalogItems from the root (EBP:CatalogItems) and iterate over them
			Element  root = (Element) document.getElementsByTagName("EBP:CatalogItems").item(0);

			NodeList catalogItems = root.getElementsByTagName("CatalogItem");
			
			for (int i = 0;i < catalogItems.getLength();i++) {
                
				// create a BasketTransferItem and store the values
				BasketTransferItemImpl item = new BasketTransferItemImpl();
				item.setSource(OCI_IS_SOURCE);

				//boolean addToList = true;
				boolean error = false;

				Element catalogItem = (Element) catalogItems.item(i);

				// get the contract data
				NodeList refData = catalogItem.getElementsByTagName("References");

				if ((refData != null) && (refData.getLength() > 0)) {
					Element  ref = (Element) refData.item(0);
					NodeList contracts = ref.getElementsByTagName("Contract");

					if ((contracts != null) && (contracts.getLength() > 0)) {
						Element  contract = (Element) contracts.item(0);
						NodeList ids = contract.getElementsByTagName("ReferenceID");
						if ((ids != null) && (ids.getLength() > 0)) {
							Node contractId = (Node) ids.item(0);
							// get the text of this element, which is our value to use
							Node ourValue = contractId.getFirstChild();
							if (ourValue != null) {
								if ( debug ) { 
									loc.debug("found contractId with name : " +
									contractId.getNodeName() + " and value : " +
									ourValue.getNodeValue());
								}
								item.setContractKey(ourValue.getNodeValue());
							}
							else {
								if (debug) {
									loc.debug("Contract -> ReferenceID is empty");
								}
							}
						}
						else {
							// Error. If we have a contract, we must have a ReferenceID/ContractID 
							if (debug) {
								loc.debug("Error. If we have a contract, we must have a ReferenceItemID/ContractItemID");
							}
							error = true;
						}

						NodeList itemIds = contract.getElementsByTagName("ReferenceItemID");

						if ((itemIds != null) && (itemIds.getLength() > 0)) {
							Node contractItemId = (Node) itemIds.item(0);

							// get the text of this element, which is our value to use
							Node ourValue = contractItemId.getFirstChild();
							if (ourValue != null) {
								if ( debug ) { 
									loc.debug("found contractItemId with name : " +
									contractItemId.getNodeName() + " and value : " +
									ourValue.getNodeValue());
								}
								item.setContractItemKey(ourValue.getNodeValue());
							}
							else {
								if (debug) {
									loc.debug("Contract -> ReferenceItemID is empty");
								}								
							}
						}
						else {
//							Error. If we have a contract, we must have a ReferenceItemID/ContractItemID
							if (debug) {
								loc.debug("Error. If we have a contract, we must have a ReferenceItemID/ContractItemID");
							}
							error = true;
						}
					}   // contracts are not mandatory
					
					if (debug) {
						loc.debug("contracts are not mandatory");
					}
				}   // references are not mandatory

				// end of getting contract data
				// get the quantity
				NodeList quantityParams = catalogItem.getElementsByTagName("Quantity");

				if ((quantityParams != null) && (quantityParams.getLength() > 0)) {
					Element  quantity = (Element) quantityParams.item(0);
					NodeList values = quantity.getElementsByTagName("Value");

					if ((values != null) && (values.getLength() > 0)) {
						Node value = (Node) values.item(0);

						// get the text of this element, which is our value to use
						Node ourValue = value.getFirstChild();
						if ( debug ) { 
							loc.debug("found Quantity-Value with name : " +
							value.getNodeName() + " and value : " + ourValue.getNodeValue());
						}
						item.setQuantity(ourValue.getNodeValue());
					}
					else {
						// error, value is mandatory
						if (debug) {
							loc.debug("Error: Quantity value is mandatory");
						}
						error = true;
					}

					NodeList uoms = quantity.getElementsByTagName("UoM");

					if ((uoms != null) && (uoms.getLength() > 0)) {
						Node uom = (Node) uoms.item(0);

						// get the text of this element, which is our value to use
						Node ourValue = uom.getFirstChild();
						if ( debug ) { 
							loc.debug("found Quantity-uom with name : " + uom.getNodeName() +
							" and value : " + ourValue.getNodeValue());
						}
						item.setUnit(ourValue.getNodeValue());
					}
					else {
						// error, uom is mandatory
						if (debug) {
							loc.debug("Error: UoM is mandatory");
						}
						error = true;
					}
				}
				else {
					// error, quantity is mandatory
					if (debug) {
						loc.debug("Error: Quantity is mandatory");
					}
					error = true;
				}
				// end of getting quantity 
				
				// get the price and currency 
				// only if we are in the enhanced OCI scenario
				if (shop.isProductInfoFromExternalCatalogAvailable()) {
					
					NodeList priceParams = catalogItem.getElementsByTagName("Price");
					
					if ((priceParams != null) && (priceParams.getLength() > 0)) {
						Element price = (Element) priceParams.item(0);
						NodeList values = price.getElementsByTagName("Value");
						
						if ((values != null) && (values.getLength() > 0)) {
							Node value = (Node) values.item(0);
							
							// get the text of this element, which is our value to use
							Node ourValue = value.getFirstChild();
							if (ourValue != null) {
								item.setNetPrice(ourValue.getNodeValue());
								if ( debug ) { 
									loc.debug("found Price-Value with name : " +
									value.getNodeName() + " and value : " + ourValue.getNodeValue());
								}								
							} else {
								item.setNetPrice("0.000");
								if (debug) {
									loc.debug("The field Price - Value is empty so we set price 0.000");
								}									
							}
						}
						else {
							// error, value is mandatory
							if (debug) {
								loc.debug("Price value is mandatory but we set net price = 0");
							}
							item.setNetPrice("0.000");
						}							
						
						NodeList currencies = price.getElementsByTagName("Currency");
						
						if ((currencies != null) && (currencies.getLength() > 0)) {
							Node currency = (Node) currencies.item(0);
		
							// get the text of this element, which is our value to use
							Node ourValue = currency.getFirstChild();
							if (ourValue != null) {
								item.setCurrency(ourValue.getNodeValue());
								if ( debug ) { 
									loc.debug("found Price-Currency with name : " +
									currency.getNodeName() + " and value : " + ourValue.getNodeValue());
								}
							} else {
								if (debug) {
									loc.debug("Currency is empty");
								}																									
							}
						}
						else {
							// error, currency is mandatory
							if (debug) {
								loc.debug("Currency is mandatory but is not available");
							}
						}
						
						NodeList priceUnits = price.getElementsByTagName("PriceBasisQuantity");
						
						if ((priceUnits != null) && (priceUnits.getLength() > 0)) {
							Node priceUnit = (Node) priceUnits.item(0);
							
								// get the text of this element, which is our value to use
								Node ourValue = priceUnit.getFirstChild();
								if (ourValue != null) {
									if (ourValue.getNodeValue().equals("1")) {
										// nothing to do 
										if ( debug ) { 
											loc.debug("found Price-Unit with name : " +
											priceUnit.getNodeName() + " and value : " + ourValue.getNodeValue());
										}										
									} else {
	//									Calculate the net price
										String netPrice = item.getNetPrice();
										if ((netPrice != null) &&(netPrice.length() > 0)) {
											try {
												String logNetPrice = netPrice;
												BigDecimal netPriceTemp = new BigDecimal(netPrice);
												BigDecimal priceUnitDec = new BigDecimal(ourValue.getNodeValue());	
												netPriceTemp = netPriceTemp.divide(priceUnitDec, 3, 5);
												netPrice = netPriceTemp.toString();
												if (debug) {
													loc.debug("The new net price is " + netPrice +
																" we calculate netPrice " + logNetPrice +
																" / Price Unit " + priceUnitDec.toString());
												}
											} catch (NumberFormatException nex) {
												loc.error("Error: calculate the new net price. " + nex.getMessage());
												item.setNetPrice("0.000");
											}
											item.setNetPrice(netPrice);
										}
									}
													
								}
								// price unit = 1 ==> nothing to do 
						}
						// Price Unit are not mandatory
						if (debug) {
							loc.debug("Price Unit are not mandatory.");
						}
					}
					
				}
				// end of getting price
				
				// get the productIds
				// difficult here, which one of the legions of productids do we need ?
				// let the user decide
				// "hey user, ..."
				NodeList prodData = catalogItem.getElementsByTagName("Product");

				if ((prodData != null) && (prodData.getLength() > 0)) {
					Element  prod = (Element) prodData.item(0);

					NodeList ids = prod.getElementsByTagName("Identifier");

					if ((ids != null) && (ids.getLength() > 0)) {
						Element  id         = (Element) ids.item(0);
/**						
						// We are in the normal OCI scenario
						if (!shop.isProductInfoFromExternalCatalogAvailable()) {
						
							NodeList productIds = id.getElementsByTagName("ProductID");
	
							if ((productIds != null) && (productIds.getLength() > 0)) {
								Node productId = (Node) productIds.item(0);
	
								// get the text of this element, which is our value to use
								Node ourValue = productId.getFirstChild();
								if ( debug ) {
									loc.debug("found productId with name : " +
									productId.getNodeName() + " and value : " +
									ourValue.getNodeValue());
								}
								item.setProductId(ourValue.getNodeValue());
							}
							else {
								// error, productid is mandatory
								if (debug) {
									loc.debug("Error: ProductID is mandatory");
								}
								error = true;
							}
						}
*/						
							// first we have to read the field ProductIDVendorAssigned
							// if this is empty we read the field ProductID
							boolean productIdVendorAss = false;
							NodeList vendorProdIds = id.getElementsByTagName("ProductIDVendorAssigned");
							
							if ((vendorProdIds != null) && (vendorProdIds.getLength() > 0)) {
								Node vendorItemId = (Node) vendorProdIds.item(0);
	
								// get the text of this element, which is our value to use
								Node ourValue = vendorItemId.getFirstChild();
								if (ourValue != null) {
									productIdVendorAss = true;
									item.setProductId(ourValue.getNodeValue());	
									
									if ( debug ) {
										loc.debug("found vendorItemId with name : " +
										vendorItemId.getNodeName() + " and value : " +
										ourValue.getNodeValue());
									}																												 
								}
								else {
									if (debug) {
										loc.debug("The field ProductIdVendorAssigned is empty we have to read ProductID");
									}
								}								
							}
							else {
								if (debug) {
									loc.debug("The field ProductIdVendorAssigned is empty we have to read ProductID");
								}
							}
							// Should we read the ProductID field
							if (!productIdVendorAss) {
								NodeList productIds = id.getElementsByTagName("ProductID");
	
								if ((productIds != null) && (productIds.getLength() > 0)) {
									Node productId = (Node) productIds.item(0);
	
									// get the text of this element, which is our value to use
									Node ourValue = productId.getFirstChild();
									if ( debug ) {
										loc.debug("found productId with name : " +
										productId.getNodeName() + " and value : " +
										ourValue.getNodeValue());
									}
									if (ourValue.getNodeValue().length() > 0) {
										item.setProductId(ourValue.getNodeValue());
									} else {
										// error, ProductIDVendorAssigned or ProductID is mandatory
										if (debug) {
											loc.debug("Error: ProductIDVendorAssigned or ProductID is mandatory");
										}
										error = true;										
									}
								}
								else {
									// error, ProductIDVendorAssigned or ProductID is mandatory
									if (debug) {
										loc.debug("Error: ProductIDVendorAssigned or ProductID is mandatory");
									}
									error = true;
								}
							}
							
							
//						} // End of enhanced OCI scenario
					}
					else {
						// error, identifier is mandatory
						if (debug) {
							loc.debug("Error: Identfier is mandatory");
						}
						error = true;
					}
//					We are in the enhanced OCI scenario					
					if (shop.isProductInfoFromExternalCatalogAvailable()) {
						NodeList descriptions = prod.getElementsByTagName("Description");
						
						if ((descriptions != null) && (descriptions.getLength() > 0)) {
							Node description = (Node) descriptions.item(0);
							
							// get the text of this element, which is our value to use
							Node ourValue = description.getFirstChild();
							if (ourValue != null) {
								if ( debug ) {
									loc.debug("found Product description with name : " +
									description.getNodeName() + " and value : " +
									ourValue.getNodeValue());
								}
								item.setDescription(ourValue.getNodeValue());
							} else {
								// description is not available
								if (debug) {
									loc.debug("No product description available");
								}																					
							}
						}
						else {
							// description is not available
							if (debug) {
								loc.debug("No product description available");
							}													
						}

					}
					
				}
				else {
					// error, product is mandatory
					if (debug) {
						loc.debug("Error: Product is mandatory");
					}
					error = true;
				}

				// end of getting the productid(s)
				
				if (shop.isProductInfoFromExternalCatalogAvailable()) {
					item.setDataSetExternally(true);
				}
				
				// if an error occured return ==> error
				if (error) {
					return OCI_NO_XML_DOCUMENT;
				}
								
				itemList.add(item);
				
			}
			if (itemList.isEmpty()) {
				ret = OCI_NO_XML_DOCUMENT;
			}				
		}
		 catch (Exception e) {
			e.printStackTrace();
			return OCI_NO_XML_DOCUMENT;
		} finally {
			loc.exiting();	
		 }
		return ret;
	}


    /**
     *  The main program for the DomParse class
     *
     *@param  args  The command line arguments
     */
    public static void main(String[] args) {
        String       fileName  = args[0];
        StringBuffer sb        = new StringBuffer();
        String       xmlString = null;

        try {
            BufferedReader br   = new BufferedReader(new FileReader(fileName));
            String         line;

            while ((line = br.readLine()) != null) {
                //                System.out.println(line);
                sb.append(line);
            }

            br.close();

            xmlString = sb.toString();

            //            System.out.println(xmlString);
        }
         catch (Exception e) {
            e.printStackTrace();
        }

        Oci35XMLParser ociP = new Oci35XMLParser(xmlString);

        /*
                DOMTraverser domTester = new DOMTraverser();
                //use this internal class to recurse the children in the document
                domTester.recurseChildren(document, "");
        */
    }

    /**
     *  Description of the Method
     *
     *@param  node    Description of Parameter
     *@param  indent  Description of Parameter
     */
    private static void printNode(Node node, int indent) {
        for (int i = 0;i < indent;i++) {
            System.out.print("   ");
        }

        int type = node.getNodeType();

        switch (type) {
        case Node.ATTRIBUTE_NODE:
            System.out.println("ATTRIBUTE_NODE : " + node.getLocalName() + " : " +
                node.getNodeName() + " : value = " + node.getNodeValue());

            break;

        case Node.CDATA_SECTION_NODE:
            System.out.println("CDATA_SECTION_NODE");

            break;

        case Node.COMMENT_NODE:
            System.out.println("COMMENT_NODE");

            break;

        case Node.DOCUMENT_FRAGMENT_NODE:
            System.out.println("DOCUMENT_FRAGMENT_NODE");

            break;

        case Node.DOCUMENT_NODE:
            System.out.println("DOCUMENT_NODE : " + node.getNodeName());

            break;

        case Node.DOCUMENT_TYPE_NODE:
            System.out.println("DOCUMENT_TYPE_NODE");

            break;

        case Node.ELEMENT_NODE:
            System.out.println("ELEMENT_NODE : " + node.getNodeName());

            NamedNodeMap atts = node.getAttributes();

            for (int i = 0;i < atts.getLength();i++) {
                Node att = atts.item(i);
                printNode(att, indent + 1);
            }

            break;

        case Node.ENTITY_NODE:
            System.out.println("ENTITY_NODE");

            break;

        case Node.ENTITY_REFERENCE_NODE:
            System.out.println("ENTITY_REFERENCE_NODE");

            break;

        case Node.NOTATION_NODE:
            System.out.println("NOTATION_NODE");

            break;

        case Node.PROCESSING_INSTRUCTION_NODE:
            System.out.println("PROCESSING_INSTRUCTION_NODE");

            break;

        case Node.TEXT_NODE:
            System.out.println("TEXT : " + node.getNodeValue());

            break;

        default:
            System.out.println("???");

            break;
        }

        for (Node c = node.getFirstChild();c != null;c = c.getNextSibling()) {
            printNode(c, indent + 1);
        }
    }
    
}
