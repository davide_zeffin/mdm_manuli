/*****************************************************************************
    Class:        HookUrlParser
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      30.7.2001

    $Revision: #1 $
    $Date: 2001/07/31 $
*****************************************************************************/
package com.sap.isa.businessobject.oci;

/**
 * Utility class to handle parameters in the hook url. Assumes that only one
 * value may be present for each parameter.
 */
public class HookUrlParser {
    protected final Parameter NO_PARAMETER = new Parameter(null, false);
    protected String url;
    protected String urlLowerCase;

    /**
     * Inner class representing an URL parameter.
     */
    public final class Parameter {
        protected String value;
        private boolean isSet;

        /**
         * Create a new <code>Parameter</code> object using a value.
         *
         * @param value Value used for construction of object
         * @param isSet Indicates whether parameter is set in URL at all
         */
        private Parameter(String value, boolean isSet) {
            this.isSet = isSet;
            if (isSet) {
                this.value = value;
            }
            else {
                this.value = null;
            }
        }

        /**
         * Indicates whether the URL parameter was set in the
         * URL or not.
         *
         * @return <code>true</code> if parameter is present; <code>false</code>
         *         if not.
         */
        public boolean isSet() {
            return isSet;
        }

        /**
         * Returns the value of the parameter.
         *
         * @return Value found at the first position or <code>null</code>
         *         if no value was found.
         */
        public String getValue() {
            return value;
        }
    }

    /**
     * Creates a new object for a given request context.
     *
     * @param req The request context you want to wrap
     */
    public HookUrlParser(String url) {
        this.url = url.trim();
        this.urlLowerCase = url.toLowerCase();
    }

    /**
     * Returns a <code>Parameter</code> object for the specified name. The data
     * to construct this object is not taken from the request parameters, but
     * from the request attributes. The data in the request is converted
     * to a <code>String</code> using its <code>toString()</code> method.
     * <p>
     * The usage of indexed parameters is not supported here.
     * </p>
     *
     * @param name Name of the request attribute to work with
     * @return Data of request attribute wrapped in an object
     */
    public Parameter getParameter(String name) {
        Parameter parameter = null;
        String value = null;

        // it is assumed that the parameter appears at most once
        if (name != null && name.length() > 0) {
            name = name.concat("=");
            int beginIndex = url.indexOf(name);
            if (beginIndex > 0) {
                beginIndex += name.length();
                int endindex = url.indexOf("&", beginIndex);
                if (endindex < 0) {
                    endindex = url.length();
                }
                value = url.substring(beginIndex, endindex);
            }
        }
        if (value != null) {
            parameter = new Parameter(value, true);
        }
        else {
            parameter = NO_PARAMETER;
        }
        return parameter;
    }

}