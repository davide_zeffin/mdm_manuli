package com.sap.isa.businessobject.oci;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.StringTokenizer;

import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.sap.isa.backend.BackendTypeConstants;
import com.sap.isa.backend.boi.isacore.CurrencyConverterBackend;
import com.sap.isa.backend.boi.isacore.UOMConverterBackend;
import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.backend.boi.isacore.order.ItemListData;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.core.logging.LogUtil;

/**
 * This class is the class used for generating the oci data in both xml and html formats
 * There is a similar class to this.which is ociserver where the oci is generated
 * in the crm world. This business class does not have any corresponding the backend object
 * The only purpose of this class being bom accessible business object is that it should be
 * to access other conversion routines for the uom for example. Right now all the calls
 * are the stateless.
 * @todo close the uomconverterbackend once this business object goes out of scope
 */
public class OCIGenerator extends BusinessObjectBase implements BackendAware {

	
	/**
	 * Backend for the uom converter
	 */
	private UOMConverterBackend uomconvBackend = null;

	/**
	 * Backend for the currency converter
	 */
	private CurrencyConverterBackend currencyConverterBackend = null;

	public OCIGenerator() {
	}
	public static final byte XML_FORMAT = 0x01;
	public static final byte HTML_FORMAT = 0x02;

	//    public static final OCIGenerator XML_OCI_GEN = new XMLOCIGenerator();
	//    public static final OCIGenerator HTML_OCI_GEN = new HTMLOCIGenerator();
	private BackendObjectManager bem;

	/**
	 * Sets the BackendObjectManager for the basket object. This method is used
	 * by the object manager to allow the user object interaction with
	 * the backend logic. This method is normally not called
	 * by classes other than BusinessObjectManager.
	 *
	 * @param bem BackendObjectManager to be used
	 */
	public void setBackendObjectManager(BackendObjectManager bem) {
		this.bem = bem;
		// get the uomconvbackend
		try {
			uomconvBackend =
				(UOMConverterBackend) bem.createBackendBusinessObject(
					BackendTypeConstants.BO_TYPE_UOMCONVERTER,
					null);
		} catch (BackendException ex) {
			log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, 
				"system.exception",
				new Object[] { ex.getLocalizedMessage()},
				ex);
			uomconvBackend = null;
		} catch (Exception e) {
			if (log.isDebugEnabled()) {
				log.debug("Error ", e);
			}
		}
		// get the currencyconvBackend
		try {
			currencyConverterBackend =
				(CurrencyConverterBackend) bem.createBackendBusinessObject(
					BackendTypeConstants.BO_TYPE_CURRENCYCONVERTER,
					null);
		} catch (BackendException ex) {
			log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, 
				"system.exception",
				new Object[] { ex.getLocalizedMessage()},
				ex);
			currencyConverterBackend = null;
		} catch (Exception e) {
			if (log.isDebugEnabled()) {
	           log.debug("Error ", e);
            }
		}		
	}

	/**
	 * This method generates the OCI string either in the xml or html format
	 * @param format the format of the oci has to be either XML_FORMAT or HTML_FORMAT
	 * @param obj This will be salesdocument
	 * @param version the version of the OCI
	 * @returns String the generated OCI string
	 */
	public String generate(
		byte format,
		Shop shop,
		SalesDocument obj,
		String version) {
		String str = null;
		return generate(format, shop, obj, version, str);
	}

	/**
	 * This method generates the OCI string either in the xml or html format
	 * @param format the format of the oci has to be either XML_FORMAT or HTML_FORMAT
	 * @param object currently this is set to generic.This will be salesdocument
	 * @param partnerId the restricting way of sending the oci through the use of partnerid
	 * @param version the version of the OCI
	 * @returns String the generated OCI string
	 */
	public String generate(
		byte format,
		Shop shop,
		SalesDocument obj,
		String version,
		String partnerId) {
		if (format == XML_FORMAT)
			return generateXML(shop, obj, version, partnerId);
		if (format == HTML_FORMAT)
			return generateXML(shop, obj, version, partnerId);
		throw new IllegalArgumentException("Format is invalid.Has to be either XML_FORMAT or HTML_FORMAT");
	}

	/**
	 * a utility method to encode only the xml data on the front end
	 */
	public static final String SAP_encode_b64(String str) {
		String encStr = "";
		char[] base64 =
			{
				'A',
				'B',
				'C',
				'D',
				'E',
				'F',
				'G',
				'H',
				'I',
				'J',
				'K',
				'L',
				'M',
				'N',
				'O',
				'P',
				'Q',
				'R',
				'S',
				'T',
				'U',
				'V',
				'W',
				'X',
				'Y',
				'Z',
				'a',
				'b',
				'c',
				'd',
				'e',
				'f',
				'g',
				'h',
				'i',
				'j',
				'k',
				'l',
				'm',
				'n',
				'o',
				'p',
				'q',
				'r',
				's',
				't',
				'u',
				'v',
				'w',
				'x',
				'y',
				'z',
				'0',
				'1',
				'2',
				'3',
				'4',
				'5',
				'6',
				'7',
				'8',
				'9',
				'+',
				'/' };
		int length = str.length();
		for (int i = 0; i < length; i += 3) {
			encStr += base64[((int) str.charAt(i) >>> 2)];
			if (i + 1 >= length) {
				encStr += "==";
				break;
			}
			encStr
				+= base64[(
					(((int) str.charAt(i) & 0x03) << 4)
						| (int) str.charAt(i + 1)
						>>> 4)];
			if (i + 2 >= length) {
				encStr += '=';
				break;
			}
			encStr
				+= base64[(
					(((int) str.charAt(i + 1) & 0x0F) << 2)
						| (int) str.charAt(i + 2)
						>>> 6)];
			encStr += base64[((int) str.charAt(i + 2) & 0x3F)];
		}
		return encStr;
	}
	/**
	 * utility method to format the number according currency
	 * according to the standard format of '.' for the decimals
	 *  which is the case for most of the xmls
	 */
	public String formatNumber(
		String price,
		char groupSeparator,
		char decimalPointSeparator) {
		try {
			StringBuffer output = new StringBuffer("");
			String outputStr = price.replace(decimalPointSeparator, '#');
			StringTokenizer strTok =
				new StringTokenizer(outputStr, "" + groupSeparator);
			while (strTok.hasMoreTokens()) {
				output.append(strTok.nextToken());
			}
			return output.toString().replace('#', '.');
		} catch (Exception e) {
			return price;
		}
	}

	/**
	 * This method generates the array of OCI strings
	 * @param obj This is be salesdocument
	 * @param version the version of the OCI
	 * @param partnerId restrict the oci content related to the partner id.
	 * @returns array of the generated OCI strings corresponding to the
	 *                          ones in the partnerids array
	 */
	public String[] generate(
		byte format,
		Shop shop,
		SalesDocument obj,
		String version,
		String[] partnerIds) {

		int length = -1;
		if (partnerIds == null) {
			length = 1;
		} else {
			length = partnerIds.length;
		}
		String[] str = new String[length];
		String partnerId = null;
		for (int i = 0; i < length; i++) {
			if (partnerIds != null)
				partnerId = partnerIds[i];
			str[i] = generate(format, shop, obj, version, partnerId);
		}
		return str;
	}

	private static final String OCI_TEMPL_STRING35 =
		"<?xml version=\"1.0\" encoding=\"UTF-8\" ?> <EBP:CatalogItems xmlns:EBP=\"http://sap.com/xi/EBP\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://sap.com/swcl/BBPCRM OpenCatalogInterface.xsd\"> <CatalogItem> <References> <Quote><ReferenceIDVendorAssigned/> <ReferenceItemIDVendorAssigned/> </Quote> <Contract> <ReferenceID/> <ReferenceItemID/></Contract> </References> <Vendor> <Identifier> <PartnerID/> <DUNS/> </Identifier> </Vendor> <Quantity> <Value/> <UoM/></Quantity> <Price> <Value/> <Currency/> <PriceBasisQuantity/> </Price> <LeadTime/> <Product> <Type/> <Identifier><ProductID/> <SystemID/> <ProductIDVendorAssigned/> <ManufacturerID/> <ProductIDManufacturerAssigned/> <GTIN/></Identifier> <Description/> </Product> <Category> <CategoryID/> <StandardCategory> <Schema/> <ClassificationID/><TechnicalID/> </StandardCategory> </Category> <Text> <Content/> <Language/> </Text> <Attachment> <URL/> <Description/></Attachment> <Configuration> <URL/>"
			+ "<Description/> </Configuration> <UserDefinedExtension> <Name/> <Value/></UserDefinedExtension> </CatalogItem> </EBP:CatalogItems>";
	//    private static final String OCI_TEMPL_STRING35 = "<CatalogItems> <CatalogItem> <References> <Quote><ReferenceIDVendorAssigned/> <ReferenceItemIDVendorAssigned/> </Quote> <Contract> <ReferenceID/> <ReferenceItemID/></Contract> </References> <Vendor> <Identifier> <PartnerID/> <DUNS/> </Identifier> </Vendor> <Quantity> <Value/> <UoM/></Quantity> <Price> <Value/> <Currency/> <PriceBasisQuantity/> </Price> <LeadTime/> <Product> <Type/> <Identifier><ProductID/> <SystemID/> <ProductIDVendorAssigned/> <ManufacturerID/> <ProductIDManufacturerAssigned/> <GTIN/></Identifier> <Description/> </Product> <Category> <CategoryID/> <StandardCategory> <Schema/> <ClassificationID/><TechnicalID/> </StandardCategory> </Category> <Text> <Content/> <Language/> </Text> <Attachment> <URL/> <Description/></Attachment> <Configuration> <URL/>"+
	//    "<Description/> </Configuration> <UserDefinedExtension> <Name/> <Value/></UserDefinedExtension> </CatalogItem> </CatalogItems>";

	/**
	 * This method generates the OCI string either in the XML
	 * @param object currently this is set to generic.This will be salesdocument
	 * @param version the version of the OCI
	 * @param partnerId restrict the oci content related to the partner id.
	 * @returns String the generated OCI string
	 */
	public String generateXML(
		Shop shop,
		SalesDocument obj,
		String version,
		String partnerId) {
			
		final String METHOD = "generateXML()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("version="+version+", partnerId="+partnerId
				+", shop="+shop+", salesDocument="+obj);
		try {
		
			if (!"3.5".equals(version))
				throw new IllegalArgumentException(
					"This version " + version + " is not currently supported");
			InputStream fin = null;
			try {
				SalesDocument basket = (SalesDocument) obj;
				InputStream bytesInput =
					new ByteArrayInputStream(OCI_TEMPL_STRING35.getBytes());
				DocumentBuilderFactory docfactoryimpl =
					DocumentBuilderFactory.newInstance();
				docfactoryimpl.setNamespaceAware(true);
				Document doc =
					docfactoryimpl.newDocumentBuilder().parse(bytesInput);
				//DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(bytesInput);
				Element root =
					(Element) doc.getElementsByTagName("EBP:CatalogItems").item(0);
				Element catalogItem =
					(Element) root.getElementsByTagName("CatalogItem").item(0);
	
				ItemListData items = basket.getItemListData();
				/**
				 * Get to the each item data
				 */
				for (int i = 0; i < items.size(); i++) {
					ItemData item = items.getItemData(i);
					String itemPartnerId = null;
					if (item.getPartnerListData() != null
						&& item.getPartnerListData().getSoldFromData() != null)
						itemPartnerId =
							item
								.getPartnerListData()
								.getSoldFromData()
								.getPartnerId();
					/**
					 * Check whether this item really belongs to the partnerId specified
					 * Else iterate to the next item in the itemList
					 */
	
					if (partnerId != null)
						if (itemPartnerId == null
							|| !partnerId.equals(itemPartnerId))
							continue;
					Element newCatalogItem = (Element) catalogItem.cloneNode(true);
					root.appendChild(newCatalogItem);
					Element referencesElement =
						(Element) newCatalogItem.getElementsByTagName(
							"References").item(
							0);
					/**
					 * First one is the quote.Has to be neglected as such as
					 * there is no support from the api as such.
					 * Normally it should be quoteid and quoteitemid for the item object
					 */
	
					/**
					 * contract item of the references.If the values are not present
					 * the contract element will not be present
					 */
					Element contract =
						(Element) referencesElement.getElementsByTagName(
							"Contract").item(
							0);
					if (item.getContractId() != null
						&& item.getContractItemId() != null) {
						Element refContractID =
							(Element) ((Element) referencesElement
								.getElementsByTagName("Contract")
								.item(0))
								.getElementsByTagName("ReferenceID")
								.item(0);
						Element refContractItemID =
							(Element) ((Element) referencesElement
								.getElementsByTagName("Contract")
								.item(0))
								.getElementsByTagName("ReferenceItemID")
								.item(0);
						addElementValue(doc, refContractID, item.getContractId());
						addElementValue(
							doc,
							refContractID,
							item.getContractItemId());
					} else {
						contract.getParentNode().removeChild(contract);
					}
					/**
					 * Vendor Element.Misssing information to be filled in the backend layer
					 */
					Element vendorElement =
						(Element) newCatalogItem.getElementsByTagName(
							"Vendor").item(
							0);
					Element partnerID =
						(Element) ((Element) vendorElement
							.getElementsByTagName("Identifier")
							.item(0))
							.getElementsByTagName("PartnerID")
							.item(0);
					//addElementValue(doc,partnerID,item.get);
					/**
					 * Quantity Element
					 */
					Element quantityValue =
						(Element) ((Element) newCatalogItem
							.getElementsByTagName("Quantity")
							.item(0))
							.getElementsByTagName("Value")
							.item(0);
					Element quantityUOM =
						(Element) ((Element) newCatalogItem
							.getElementsByTagName("Quantity")
							.item(0))
							.getElementsByTagName("UoM")
							.item(0);
					addElementValue(
						doc,
						quantityValue,
						formatNumber(
							item.getQuantity(),
							shop.getGroupingSeparator().charAt(0),
							shop.getDecimalSeparator().charAt(0)));
					/**
					 * Try to format the unit using the uom conv backend
					 */
					String uomStr = null;
					try {
						if (uomconvBackend != null)
							uomStr =
								uomconvBackend.convertUOM(true, item.getUnit());
						if (uomStr == null || uomStr.length() <= 0)
							uomStr = item.getUnit();
	
					} catch (Exception e) {
						uomStr = item.getUnit();
					}
	
					addElementValue(doc, quantityUOM, uomStr);
					/**
					 * Price Element
					 */
					Element priceValue =
						(Element) ((Element) newCatalogItem
							.getElementsByTagName("Price")
							.item(0))
							.getElementsByTagName("Value")
							.item(0);
					Element priceCurrency =
						(Element) ((Element) newCatalogItem
							.getElementsByTagName("Price")
							.item(0))
							.getElementsByTagName("Currency")
							.item(0);
					Element priceBasisQuantity =
						(Element) ((Element) newCatalogItem
							.getElementsByTagName("Price")
							.item(0))
							.getElementsByTagName("PriceBasisQuantity")
							.item(0);
	
					addElementValue(
						doc,
						priceValue,
						formatNumber(
							item.getNetPrice(),
							shop.getGroupingSeparator().charAt(0),
							shop.getDecimalSeparator().charAt(0)));
	
					String currencyStr = item.getCurrency();
					if(currencyConverterBackend!=null){
						try {
							currencyStr =
								currencyConverterBackend.convertCurrency(
									true,
									item.getCurrency());
							if (currencyStr == null || currencyStr.length() < 1)
								currencyStr = item.getCurrency();
						} catch (Exception e) {
							currencyStr = item.getCurrency();
							log.debug(e);
						}
					}
					addElementValue(doc, priceCurrency, currencyStr);
					addElementValue(
						doc,
						priceBasisQuantity,
						item.getNetQuantPriceUnit());
					/**
					 *
					 * Lead Time Element.Lead time not present
					 */
	
					Element leadTime =
						((Element) newCatalogItem
							.getElementsByTagName("LeadTime")
							.item(0));
	
					/**
					 * @todo currently not available are the leadtimes
					 * !!Deleting the lead element
					 */
					leadTime.getParentNode().removeChild(leadTime);
					//addElementValue(doc,leadTime,item.getI);
					/**
					 * Product Element
					 */
					Element product =
						(Element) newCatalogItem.getElementsByTagName(
							"Product").item(
							0);
					Node type = product.getElementsByTagName("Type").item(0);
					addElementValue(doc, type, "Material");
					Element prodId =
						(Element) ((Element) product
							.getElementsByTagName("Identifier")
							.item(0))
							.getElementsByTagName("ProductID")
							.item(0);
					addElementValue(doc, prodId, item.getProduct());
					/**
					 * Category Element
					 */
					Element category =
						(Element) newCatalogItem.getElementsByTagName(
							"Category").item(
							0);
					Element categoryID =
						(Element) ((Element) category
							.getElementsByTagName("CategoryID")
							.item(0));
					addElementValue(doc, categoryID, item.getPcatArea());
					/**
					 * No text,attachment,configuration and UserDefinedConfiguration available.Needs to be
					 * deleted
					 */
	
				}
				catalogItem.getParentNode().removeChild(catalogItem);
				return root.toString();
			} catch (Exception e) {
				log.error(LogUtil.APPS_BUSINESS_LOGIC, "A exception occurred in XML OCI generation ", e);
			} finally {
				try {
					if (fin != null)
						fin.close();
				} catch (Exception e) {
					if (log.isDebugEnabled()) {
						log.debug("Error ", e);
					}					
				}
	
			}
		} finally {
			log.exiting();
		}
		return null;
	}

	/**
	 * Utility method to create a element within the parentElement
	 * @param doc the document of the XML
	 * @param parentElm the parent element
	 * @param elementName the element name of the child to be created
	 */
	private Element createElement(
		Document doc,
		Node parentElm,
		String elementName) {
		Element newElem = doc.createElement(elementName);
		parentElm.appendChild(newElem);
		return newElem;
	}
	/**
	 * Utility method to add value to an element.Basically creates a text node
	 * within the element
	 * @param doc the document of the XML
	 * @param elem the element for which value needs to be added
	 * @param val the value of the element
	 */
	private void addElementValue(Document doc, Node elem, String val) {
		if (val == null)
			return;
		if (val != null || val != "")
			elem.appendChild(doc.createTextNode(val));
	}

	/**
	 * Utility method to create attribute and sets value to
	 * within an element.
	 * @param doc the document of the XML
	 * @param elm the element for which attribute needs to be added
	 * @param val the value of the element in string
	 * @param attrName the name of the attribute to be created
	 * @param value the value of the attribute
	 */
	private Attr addAttribute(
		Document doc,
		Element elm,
		String attrName,
		String val) {
		Attr newAttr = doc.createAttribute(attrName);
		if (val != null)
			newAttr.setNodeValue(val);
		((Node) elm).appendChild(newAttr);
		return newAttr;
	}

	/**
	 * This method generates the OCI string either in the XML
	 * @param object currently this is set to generic.This will be salesdocument
	 * @param version the version of the OCI
	 * @param partnerId restrict the oci content related to the partner id.
	 * @returns String the generated OCI string
	 */
	public String generateHTML(
		Shop shop,
		SalesDocument obj,
		String version,
		String partnerId) {
		final String METHOD = "generateHTML";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("version="+version+", partnerId="+partnerId
				+", shop="+shop+", salesDocument="+obj);
		if (!"3.5".equals(version)) {
			log.exiting();	
			throw new IllegalArgumentException(
				"This version " + version + " is not currently supported");
		}
		StringBuffer htmlStr = new StringBuffer("");
		;
		SalesDocument basket = (SalesDocument) obj;
		//            if(bom!=null)
		//                shop = bom.getShop();
		// append lines to the list
		int counter = 0;
		ItemListData itemList = basket.getItemListData();
		for (int i = 0; i < itemList.size(); i++) {
			//            String iString = Integer.toString(i+1);
			ItemData item = itemList.getItemData(i);
			String itemPartnerId = null;
			if (item.getPartnerListData() != null
				&& item.getPartnerListData().getSoldFromData() != null)
				itemPartnerId =
					item.getPartnerListData().getSoldFromData().getPartnerId();
			/**
			 * Check whether this item really belongs to the partnerId specified
			 * Else iterate to the next item in the itemList
			 */

			if (partnerId != null)
				if (itemPartnerId == null || !partnerId.equals(itemPartnerId))
					continue;
			counter++;
			String iString = Integer.toString(counter);
			htmlStr.append(
				"<input type=\"hidden\" name=\"NEW_ITEM-DESCRIPTION["
					+ iString
					+ "]\" value=\""
					+ escapeQuotes(item.getDescription())
					+ "\">");
			htmlStr.append(
				"<input type=\"hidden\" name=\"NEW_ITEM-MATNR["
					+ iString
					+ "]\" value=\""
					+ escapeQuotes(item.getProduct())
					+ "\">");
			htmlStr.append(
				"<input type=\"hidden\" name=\"NEW_ITEM-QUANTITY["
					+ iString
					+ "]\" value=\""
					+ formatNumber(
						item.getQuantity(),
						shop.getGroupingSeparator().charAt(0),
						shop.getDecimalSeparator().charAt(0))
					+ "\">");
			String uomStr = null;
			try {
				if (uomconvBackend != null)
					uomStr = uomconvBackend.convertUOM(true, item.getUnit());
			} catch (Exception e) {
				if (log.isDebugEnabled()) {
					log.debug("Error ", e);
				}				
			}
			if (uomStr == null || uomStr.length() <= 0)
				uomStr = item.getUnit();

			htmlStr.append(
				"<input type=\"hidden\" name=\"NEW_ITEM-UNIT["
					+ iString
					+ "]\" value=\""
					+ uomStr
					+ "\">");
			htmlStr.append(
				"<input type=\"hidden\" name=\"NEW_ITEM-PRICE["
					+ iString
					+ "]\" value=\""
					+ formatNumber(
						item.getNetPrice(),
						shop.getGroupingSeparator().charAt(0),
						shop.getDecimalSeparator().charAt(0))
					+ "\">");
			String currencyStr = item.getCurrency();
			if(currencyConverterBackend!=null){
				try {
					currencyStr =
						currencyConverterBackend.convertCurrency(
							true,
							item.getCurrency());
					if (currencyStr == null || currencyStr.length() < 1)
						currencyStr = item.getCurrency();
				} catch (Exception e) {
					currencyStr = item.getCurrency();
					log.debug(e);
				}
			}
			htmlStr.append(
				"<input type=\"hidden\" name=\"NEW_ITEM-CURRENCY["
					+ iString
					+ "]\" value=\""
					+ currencyStr
					+ "\">");
			htmlStr.append(
				"<input type=\"hidden\" name=\"NEW_ITEM-LEADTIME["
					+ iString
					+ "]\" value=\""
					+ ""
					+ "\">");
			htmlStr.append(
				"<input type=\"hidden\" name=\"NEW_ITEM-VENDOR["
					+ iString
					+ "]\" value=\""
					+ escapeQuotes("")
					+ "\">");
			htmlStr
				.append(
					"<input type=\"hidden\" name=\"NEW_ITEM-VENDORMAT["
					+ iString
					+ "]\" value=\""
					+ escapeQuotes(item.getProduct()
			/*basketItemBbp.getString("VENDORMAT")*/
			) + "\">");
			htmlStr
				.append(
					"<input type=\"hidden\" name=\"NEW_ITEM-MANUFACT["
					+ iString
					+ "]\" value=\""
					+ escapeQuotes(
			/*basketItemBbp.getString("MANUFACT")*/
			"") + "\">");
			htmlStr
				.append(
					"<input type=\"hidden\" name=\"NEW_ITEM-MANUFACTMAT["
					+ iString
					+ "]\" value=\""
					+ escapeQuotes(""
			/*basketItemBbp.getString("MANUFACTMAT")*/
			) + "\">");
			htmlStr
				.append(
					"<input type=\"hidden\" name=\"NEW_ITEM-PRICEUNIT["
					+ iString
					+ "]\" value=\""
					+ 
			/*basketItemBbp.getString("PRICEUNIT")*/
			item.getNetPriceUnit() + "\">");
			htmlStr
				.append(
					"<input type=\"hidden\" name=\"NEW_ITEM-MATGROUP["
					+ iString
					+ "]\" value=\""
					+ escapeQuotes(
			/*basketItemBbp.getString("MATGROUP")*/
			"") + "\">");
			htmlStr
				.append(
					"<input type=\"hidden\" name=\"NEW_ITEM-EXT_QUOTE_ID["
					+ iString
					+ "]\" value=\""
					+ escapeQuotes(
			/*basketItemBbp.getString("QUOTEID")*/
			"") + "\">");
			htmlStr
				.append(
					"<input type=\"hidden\" name=\"NEW_ITEM-EXT_QUOTE_ITEM["
					+ iString
					+ "]\" value=\""
					+ escapeQuotes(""
			/*basketItemBbp.getString("QUOTEITEM")*/
			) + "\">");
			htmlStr
				.append(
					"<input type=\"hidden\" name=\"NEW_ITEM-CONTRACT["
					+ iString
					+ "]\" value=\""
					+ escapeQuotes(item.getContractId()
			/*basketItemBbp.getString("CONTRACT")*/
			) + "\">");
			htmlStr
				.append(
					"<input type=\"hidden\" name=\"NEW_ITEM-CONTRACT_ITEM["
					+ iString
					+ "]\" value=\""
					+ escapeQuotes(item.getContractItemId()
			/*basketItemBbp.getString("CONTRACT_ITEM")*/
			) + "\">");
			htmlStr
				.append(
					"<input type=\"hidden\" name=\"NEW_ITEM-ATTACHMENT["
					+ iString
					+ "]\" value=\""
					+ escapeQuotes(
			/*basketItemBbp.getString("ATTACHMENT")*/
			"") + "\">");
			htmlStr
				.append(
					"<input type=\"hidden\" name=\"NEW_ITEM-ATTACHMENT_TITLE["
					+ iString
					+ "]\" value=\""
					+ escapeQuotes(
			/*basketItemBbp.getString("ATTACHMENT_TITLE")*/
			"") + "\">");
			htmlStr
				.append(
					"<input type=\"hidden\" name=\"NEW_ITEM-ATTACHMENT_PURPOSE["
					+ iString
					+ "]\" value=\""
					+ escapeQuotes(
			/*basketItemBbp.getString("ATTACHMENT_PURPOSE")*/
			"") + "\">");
		}
		log.exiting();
		return htmlStr.toString();
	}
	public void destroy(){
		super.destroy();
		uomconvBackend = null;
		currencyConverterBackend = null;
	}
	// replace " by &quot;
	private String escapeQuotes(String escapeMe) {
		int i;
		int length = escapeMe.length();

		// any " to escape?
		i = escapeMe.lastIndexOf('"');
		if (i > -1 && i < length) {
			StringBuffer escapeBuffer = new StringBuffer(escapeMe);
			escapeBuffer.replace(i, i + 1, "&quot;");
			while ((i = escapeMe.lastIndexOf('"', --i)) > -1) {
				escapeBuffer.replace(i, i + 1, "&quot;");
			}
			return escapeBuffer.toString();
		} else {
			return escapeMe;
		}
	}
	public static void main(String[] args) {
		OCIGenerator oci = new OCIGenerator();
		oci.generate(
			OCIGenerator.XML_FORMAT,
			(Shop) null,
			(SalesDocument) null,
			"3.5");
	}
}
