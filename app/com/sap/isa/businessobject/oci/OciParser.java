/*****************************************************************************
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP AG
    Created:      27 July 2001

    $Revision: #1 $
    $Date: 2001/08/02 $
*****************************************************************************/
package com.sap.isa.businessobject.oci;

// some businessobjects
import java.io.IOException;
import java.io.StringReader;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.businessobject.BusinessObjectException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.item.BasketTransferItemImpl;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.RequestParser;



/**
 * The OciParser class provides some parsing functionality for the OCI
 * It contains a method for parsing HTML-requests and XML-requests.
 */
public class OciParser extends BusinessObjectBase {

    // some constants to describe error situations
    public static final int OCI_PARSE_SUCCESS             = 0;
    public static final int OCI_NO_XML_DOCUMENT           = -1;
    public static final int OCI_EMPTY_XML_DOCUMENT        = -2;
    public static final int OCI_HTML_QUANTITY             = -3;
    public static final int OCI_HTML_UOM                  = -4;
    public static final int OCI_HTML_MATNR                = -5;
    public static final int OCI_FORM_UNKNOWN              = -6;
    public static final int OCI_FORM_UNSUPPORTED          = -7;
    public static final int OCI_HTML_CONTRACTDATA         = -8;
    public static final int OCI_HTML_MATNR_LENGTH         = -9;
    public static final int OCI_HTML_QUANTITY_LENGTH      = -10;
    public static final int OCI_HTML_QUANTITY_FORMAT      = -11;
    public static final int OCI_HTML_UOM_LENGTH           = -12;
    public static final int OCI_HTML_CONTRACTKEY          = -13;
    public static final int OCI_HTML_CONTRACTKEY_LENGTH   = -14;
    public static final int OCI_HTML_CONTRACTITEM_LENGTH  = -15;
    public static final int OCI_PARSE_KEY_NOT_FOUND       = -16;
    public static final int OCI_PARSE_LENGTH_EXCEEDED     = -17;
    public static final int OCI_HTML_VENDORMATNR          = -18;
    public static final int OCI_HTML_VENDORMATNR_LENGTH   = -19;


    /**
     * length of buffer to be used for reading from the request
     */
    public static int OCI_PARSE_BUFLEN              = 40;

    /**
     * maximum length of the product number
     */
    public static int OCI_PARSE_MAXLEN_MATNR        = 40;

    /**
     * maximum length of the vendor material number
     */
    public static int OCI_PARSE_MAXLEN_VENDORMATNR  = 40;

    /**
     * maximum length of the contract key
     */
    public static int OCI_PARSE_MAXLEN_CONTRACTKEY  = 40;

    /**
     * maximum length of the contract item key
     */
    public static int OCI_PARSE_MAXLEN_CONTRACTITEM = 40;

    /**
     * maximum length of the quantity
     */
    public static int OCI_PARSE_MAXLEN_QTY      = 15;

    /**
     * exact length of the fractional part of the quantity
     */
    public static int OCI_PARSE_QTY_FRAC = 3;

    /**
     * exact length of the uom
     */
    public static int OCI_PARSE_MAXLEN_UOM = 3;
    

    // some variables for the XML-parsing
//    private String indentString = "    "; // Amount to indent
    private int indentLevel = 0;

    // create the names of the parameters in the request
    private static final String MATNR               = "NEW_ITEM-MATNR[";
    private static final String QUANT               = "NEW_ITEM-QUANTITY[";
    private static final String UOM                 = "NEW_ITEM-UNIT[";
    private static final String CONTRACTKEY         = "NEW_ITEM-CONTRACT[";
    private static final String CONTRACTITEMKEY     = "NEW_ITEM-CONTRACT_ITEM[";
    private static final String VENDORMATNR         = "NEW_ITEM-VENDORMAT[";
    
    // to set the source of a basket transfer item
    private static final String OCI_IS_SOURCE       = "OCI";
    
    protected   String  ociVersion;
    protected Shop shop;
    

    /**
     * Constructor.
     * Currently empty
     */
    public OciParser() {
        /* ich bin */ super(); /* :-) */
    }

    /**
     * readStringFromRequest reads a string from the request
     * returns OCI_PARSE_KEY_NOT_FOUND also if the content is smaller than the min-length
     */
    public int readStringFromRequest(   RequestParser   requestParser,
                                        String          key,
                                        int             minLen,
                                        int             maxLen,
                                        StringBuffer    dest) {

		final String METHOD = "readStringFromRequest()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("key="+key+", minLen="+minLen+", maxLen="+maxLen+", dest="+dest);
        int                     ret     = OCI_PARSE_SUCCESS;
        RequestParser.Parameter rparam;

        rparam = requestParser.getParameter(key);
        if (rparam.isSet()) {
            String content = rparam.getValue().getString();
            if (content.length() < minLen) {
                // error
                // return OCI_PARSE_KEY_NOT_FOUND
                ret = OCI_PARSE_KEY_NOT_FOUND;
            }
            else if (content.length() > maxLen ) {
                ret = OCI_PARSE_LENGTH_EXCEEDED;
            }
            else {
                dest.append(content);
            }
        }
        else {
            ret = OCI_PARSE_KEY_NOT_FOUND;
        }
		log.exiting();
        return ret;
    }


    /**
     * addErrorMessage adds an error message to the BusinessObjectException
     */
    public static void addErrorMessage( BusinessObjectException boex,
                                        String                  messageParam,
                                        String                  key) {

        Message     msg;
        String[]    msgParams = new String[1];

        msgParams[0] = messageParam;
        msg = new Message( Message.ERROR, key, msgParams, "" );
        boex.addMessage(msg);
    }

    /**
     * readVendorMatNr reads the vendor material number from the request and checks it for
     * it's max. length, ...
     */
    public int readVendorMatNr( RequestParser           requestParser,
                                BusinessObjectException boex,
                                StringBuffer            vendorMatNr,
                                int                     index,
                                BasketTransferItemImpl  item) {
		final String METHOD = "readVendorMatNr()";
		if (log.isDebugEnabled())
			log.debug("index="+index+", vendor Materail Nr="+vendorMatNr);
		log.entering(METHOD);
        int ret     = OCI_PARSE_SUCCESS;

        ret = readStringFromRequest(requestParser, VENDORMATNR + index + "]", 1, OCI_PARSE_MAXLEN_VENDORMATNR, vendorMatNr);
        switch (ret) {
            case OCI_PARSE_KEY_NOT_FOUND:
                // no error message here
                // this is only the flag indicating that no more items follow
                // should not happen if this is called (indirectly) from ParseHTMLRequest()
                ret = OCI_HTML_VENDORMATNR;
                break;
            case OCI_PARSE_LENGTH_EXCEEDED:
                // error, mat number is too long
                addErrorMessage(boex, "", "oci.vendorMatNr.length");
                ret = OCI_HTML_VENDORMATNR_LENGTH;
                break;
            default: // ok
                break;
        }
		log.exiting();
        return ret;
    }

    /**
     * readMatNr reads the material number from the request and checks it for
     * it's max. length, ...
     */
    public int readMatNr( RequestParser           requestParser,
                          BusinessObjectException boex,
                          StringBuffer            matNr,
                          int                     index,
                          BasketTransferItemImpl  item) {
		final String METHOD = "readMatNr()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("index="+index+", materialNr="+matNr);
        int ret     = OCI_PARSE_SUCCESS;

        ret = readStringFromRequest(requestParser, MATNR + index + "]", 1, OCI_PARSE_MAXLEN_MATNR, matNr);
        switch (ret) {
            case OCI_PARSE_KEY_NOT_FOUND:
                // no error message here
                // this is only the flag indicating that no more items follow
                // should not happen if this is called (indirectly) from ParseHTMLRequest()
                ret = OCI_HTML_MATNR;
                break;
            case OCI_PARSE_LENGTH_EXCEEDED:
                // error, mat number is too long
                addErrorMessage(boex, "", "oci.matNr.length");
                ret = OCI_HTML_MATNR_LENGTH;
                break;
            default: // ok
                break;
        }
		log.exiting();
        return ret;
    }

    /**
     *  get the quantity as a StringBuffer from the string including
     *  some checks and reformatting
     *
     *@param  source  The Quantity to converted inside a String
     *@param  dest    The converted Quantity in a StringBuffer
     *@return         The SafeQuantity value
     */
    public static int getSafeQuantity(String source, StringBuffer dest) {
        int ret = OCI_PARSE_SUCCESS;
        try {
            double value = Double.parseDouble(source);

            // three digits to the right of the point
            DecimalFormat df = new DecimalFormat("#0.000");
            DecimalFormatSymbols dfs = df.getDecimalFormatSymbols();
            dfs.setDecimalSeparator('.');
            df.setDecimalFormatSymbols(dfs);
            
            dest.append(df.format(value));

        }
        catch (NumberFormatException ex) {
            System.out.println("NumberFormatException occured for : " + source);
            ret = OCI_HTML_QUANTITY_FORMAT;

        }
        return ret;
    }


    /**
     * readQuantity reads the quantity from the OCI-request and checks it
     * for it's correct format
     */
    public int readQuantity(RequestParser           requestParser,
                            BusinessObjectException boex,
                            StringBuffer            quantity,
                            int                     index,
                            BasketTransferItemImpl  item) {
		final String METHOD = "readQuantity()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("index="+index+", quantity="+quantity);
        int                     ret     = OCI_PARSE_SUCCESS;
        String                  qty;
        RequestParser.Parameter rparam;

        rparam = requestParser.getParameter(QUANT + index + "]");
        if (rparam.isSet()) {
            qty = rparam.getValue().getString();

            // check the format of the quantity
            // do it with our own hands, do not use any util-class here
            // maxlen = OCI_PARSE_MAXLEN_QTY and
            // number of digits to the right of the decimal point must be
            // OCI_PARSE_QTY_FRAC
            if (OCI_PARSE_MAXLEN_QTY >= qty.length()) {

                // check if there os no letter in the quantity to avoid 
                // something like 3E12
                if (getSafeQuantity(qty, quantity) == OCI_PARSE_SUCCESS) {
                    // ok
                }
                else {
                    // wrong format
                    addErrorMessage(boex, item.getProductId(), "oci.Quantity.format");
                    ret = OCI_HTML_QUANTITY_FORMAT;
                    
                    if (log.isDebugEnabled()) {
                        log.debug(qty + " could not be converted to a valid double value");
                    }
                }
                
            }  // endif OCI_PARSE_MAXLEN_QTY >= qty.length()
            else {
                // too long
                addErrorMessage(boex, item.getProductId(), "oci.Quantity.length");
                ret = OCI_HTML_QUANTITY_LENGTH;
            }
        }  // endif rparam.isSet()
        else {
            // error, quantity is a required value
            // provide some error message here
            addErrorMessage(boex, item.getProductId(), "oci.noQuantity");
            ret = OCI_HTML_QUANTITY;
        }
		log.exiting();
        return ret;
    }

    /**
     * readUOM reads the unit of measurement from the OCI-request
     * and checks it for it's maximum length
     */
    public int readUOM( RequestParser           requestParser,
                        BusinessObjectException boex,
                        StringBuffer            unitBuffer,
                        int                     index,
                        BasketTransferItemImpl  item) {

		final String METHOD = "readUOM()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("index="+index+", unitBuffer="+unitBuffer);
        int                     ret     = OCI_PARSE_SUCCESS;

        ret = readStringFromRequest(requestParser, UOM + index + "]", 1, OCI_PARSE_MAXLEN_UOM, unitBuffer);
        switch (ret) {
            case OCI_PARSE_KEY_NOT_FOUND:
                // error, UOM is a required value
                // provide some error message here
                addErrorMessage(boex, item.getProductId(), "oci.noUom" );
                ret = OCI_HTML_UOM;
                break;
            case OCI_PARSE_LENGTH_EXCEEDED:
                // too long
                addErrorMessage(boex, item.getProductId(), "oci.Uom.length");
                ret = OCI_HTML_UOM_LENGTH;
                break;
            default: // ok
                break;
        }
		log.exiting();
        return ret;
    }

    /**
     * readContractKey reads any optional contract key from the OCI-request
     *
     */
    public int readContractKey( RequestParser           requestParser,
                                BusinessObjectException boex,
                                StringBuffer            ctkBuffer,
                                int                     index,
                                BasketTransferItemImpl  item) {

		final String METHOD = "readContractKey()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("index="+index+", ctkBuffer="+ctkBuffer);
        int                     ret     = OCI_PARSE_SUCCESS;

        ret = readStringFromRequest(requestParser, CONTRACTKEY + index + "]", 1, OCI_PARSE_MAXLEN_CONTRACTKEY, ctkBuffer);
        switch (ret) {
            case OCI_PARSE_KEY_NOT_FOUND:
                // no error, the CONTRACTKEY is not required
                ret = OCI_HTML_CONTRACTKEY;
                break;
            case OCI_PARSE_LENGTH_EXCEEDED:
                // too long
                addErrorMessage(boex, item.getProductId(), "oci.ContractKey.length");
                ret = OCI_HTML_CONTRACTKEY_LENGTH;
                break;
            default: // ok
                break;
        }
		log.exiting();
        return ret;
    }

    /**
     * readContractItem reads the contract item from the OCI-request
     * if the contract key exists, the item to the contract key is required
     */
    public int readContractItem(RequestParser           requestParser,
                                BusinessObjectException boex,
                                StringBuffer            ctiBuffer,
                                int                     index,
                                BasketTransferItemImpl  item) {

		final String METHOD = "readContractItem()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("index="+index+", ctiBuffer="+ctiBuffer);
        int ret = OCI_PARSE_SUCCESS;

        ret = readStringFromRequest(requestParser, CONTRACTITEMKEY + index + "]", 1, OCI_PARSE_MAXLEN_CONTRACTITEM, ctiBuffer);
        switch (ret) {
            case OCI_PARSE_KEY_NOT_FOUND:
                // error, CONTRACTITEMKEY is a required value
                // provide some error message here
                addErrorMessage(boex, item.getProductId(), "oci.noContractItemKey" );
                ret = OCI_HTML_UOM;
                break;
            case OCI_PARSE_LENGTH_EXCEEDED:
                // too long
                addErrorMessage(boex, item.getProductId(), "oci.ContractItemKey.length");
                ret = OCI_HTML_CONTRACTITEM_LENGTH;
                break;
            default: // ok
                break;
        }
		log.exiting();
        return ret;
    }

    /**
     * readOCIItemData reads the data for one OCI-Item from the request and stores
     * the result in the item or any error message in the boex
     *
     */
    public int readOCIItemData( RequestParser           requestParser,
                                BusinessObjectException boex,
                                int                     index,
                                BasketTransferItemImpl  item) {

		final String METHOD = "readOCIItemData()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("index="+index);
        int             ret     =   OCI_PARSE_SUCCESS;
        StringBuffer    buffer  =   new StringBuffer(OCI_PARSE_BUFLEN);

        buffer.delete(0, OCI_PARSE_BUFLEN + 1);
        ret = readMatNr(requestParser, boex, buffer, index, item);
        if (OCI_PARSE_SUCCESS == ret) {
            //store the number in the item
            item.setProductId(buffer.toString());

            // read the quantity
            buffer.delete(0, OCI_PARSE_BUFLEN + 1);
            ret = readQuantity(requestParser, boex, buffer, index, item);
            if (OCI_PARSE_SUCCESS == ret) {
                item.setQuantity(buffer.toString());

                buffer.delete(0, OCI_PARSE_BUFLEN + 1);
                ret = readUOM(requestParser, boex, buffer, index, item);
                if (OCI_PARSE_SUCCESS == ret) {
                    item.setUnit(buffer.toString());

                    // read any optional contract data
                    buffer.delete(0, OCI_PARSE_BUFLEN + 1);
                    ret = readContractKey(requestParser, boex, buffer, index, item);
                    if (OCI_PARSE_SUCCESS == ret) {
                        item.setContractKey(buffer.toString());
                        // read the contractitem
                        // if the CONTRACTKEY is passed, the item must exist as well
                        buffer.delete(0, OCI_PARSE_BUFLEN + 1);
                        ret = readContractItem(requestParser, boex, buffer, index, item);
                        if (OCI_PARSE_SUCCESS == ret) {
                            item.setContractItemKey(buffer.toString());
                        }
                    }
                    else if (OCI_HTML_CONTRACTKEY == ret) {
                        // no error, contract data is not mandatory
                        // reset the flag
                        ret = OCI_PARSE_SUCCESS;
                    }
                }
            }
        }
        log.exiting();
        return ret;

     }

    /**
     * Parse the HTML request reading the OCI's parameters and creating BasketTransferItems
     * from them
     *
     * @param request The request
     * @param requestParser The requestParser
     * @param itemList The ArrayList that will hold the BasketTransferItems in the end (hopefully)
     * @param boex The businessobjectexception-object that any errors will be fixed to, if any.
     *
     * @return ret 0 if ok, < 0 if an error occurred
     */
    public int ParseHTMLRequest( HttpServletRequest  request,
                                 RequestParser       requestParser,
                                 List                itemList,
                                 BusinessObjectException boex ) {

		final String METHOD = "ParseHTMLRequest()";
		log.entering(METHOD);
        boolean retry   = true;

        // the first index in the list is 1
        int index = 1;

        // the return value
        int ret = OCI_PARSE_SUCCESS;

        RequestParser.Parameter rparam;
        do {
            rparam = requestParser.getParameter(MATNR + index + "]");
            if (rparam.isSet()) {

                // create a BasketTransferItem and store the values
                BasketTransferItemImpl item = new BasketTransferItemImpl();
                item.setSource(OCI_IS_SOURCE);

                ret = readOCIItemData(requestParser, boex, index, item);
                if (ret == OCI_HTML_MATNR) {
                    // no VENDORMATNR found
                    // this is only a flag to end the loop
                    ret = OCI_PARSE_SUCCESS;
                    retry = false;
                }
                else if (ret == OCI_PARSE_SUCCESS) {
                    // add the item to the list and increment the index
                    itemList.add(item);
                    index++;
                }
                else {
                    retry = false;
                }
            }
            else {
                // no further entry found, exit the loop
                retry = false;
            }
        } while (retry == true);

        // push the boException into the request if an error occured
        if (ret != OCI_PARSE_SUCCESS) {
            request.setAttribute("Exception", boex);
        }

        // don't really evaluate the return parameter
        // it should only be checked wether it is OK or not
        log.exiting();
        return ret;
    }

    /**
     * Check some parameters and set up the XML parsing process, delegating it
     * to the appropriate parser
     *
     * @param request The request
     * @param requestParser The requestParser
     * @param itemList The ArrayList that will hold the BasketTransferItems in the end (hopefully)
     * @param boex The businessobjectexception-object that any errors will be fixed to, if any.
     *
     * @return ret 0 if ok, < 0 if an error occurred
     */
    public int ParseOciXMLRequest( HttpServletRequest  request,
                                   RequestParser       requestParser,
                                   List                itemList,
                                   BusinessObjectException boex ) {
                                    
        int             ret     = OCI_PARSE_SUCCESS;
        
        
        
        return ret; 
    }

    /**
     * Parse the XML request reading the OCI's parameters and creating BasketTransferItems
     * from them
     *
     * @param request The request
     * @param requestParser The requestParser
     * @param itemList The ArrayList that will hold the BasketTransferItems in the end (hopefully)
     * @param boex The businessobjectexception-object that any errors will be fixed to, if any.
     *
     * @return ret 0 if ok, < 0 if an error occurred
     * @deprecated This method is deprecated since ISA 4.0, use ParseOciXMLRequest
     * instead
     */
    public int ParseXMLRequest( HttpServletRequest  request,
                                RequestParser       requestParser,
                                List                itemList,
                                BusinessObjectException boex ) {

		final String METHOD = "ParseXMLRequest()";
		log.entering(METHOD);
        Properties      props   = new Properties();
        OciSaxHandler   ociH    = new OciSaxHandler(boex, itemList);
        String          xml     = null;
        int             ret     = OCI_PARSE_SUCCESS;

        // check if the ~xmlDocument-field is in the request
        RequestParser.Parameter rparam;
        rparam = requestParser.getParameter("~xmlDocument");

        if (!rparam.isSet()) {
            // looking for an xml document in the request attribute
            // if no parameter could be found
            rparam = requestParser.getAttribute("~xmlDocument");
        }
		try {
	        if (rparam.isSet()) {
	            xml = rparam.getValue().getString();
	            
	            if ( xml != null && xml.length() > 0 ) {
	                // Use an instance of the OciSaxHAndler as the SAX event handler
	                
					if (log.isDebugEnabled()) {
						log.debug("XML Document: " + xml);
					}                 
	
	                DefaultHandler handler = ociH;
	                // Use the validating parser
	                SAXParserFactory factory = SAXParserFactory.newInstance();
	                // factory.setValidating(true); no validating currently, maybe later
	                // factory.setNamespaceAware(true);
	                try {
	                    // Parse the input
	                    SAXParser saxParser = factory.newSAXParser();
	                    saxParser.parse( new InputSource(new StringReader(xml)), handler);
	                }
	                catch (SAXParseException spe) {
	                   // Error generated by the parser
	                   if (log.isDebugEnabled()) {
	                       log.debug("** Parsing error" + ", line " + spe.getLineNumber()
	                          + ", uri " + spe.getSystemId());
	                       log.debug( spe.getMessage() );
	                   }
	
	                   // Use the contained exception, if any
	                   Exception  x = spe;
	                   if (spe.getException() != null) {
	                       x = spe.getException();
	                   }
	                   x.printStackTrace();
	
	                }
	                catch (SAXException sxe) {
	                   // Error generated by this application
	                   // (or a parser-initialization error)
	                   Exception  x = sxe;
	                   if (sxe.getException() != null) {
	                       x = sxe.getException();
	                   }
	                   x.printStackTrace();
	
	                }
	                catch (ParserConfigurationException pce) {
	                    // Parser with specified options can't be built
	                    pce.printStackTrace();
	
	                }
	                catch (IOException ioe) {
	                   // I/O error
	                   ioe.printStackTrace();
	                }
	            }
	            else {
	                // add some error messages to the boex and exit with error code
	                return OCI_EMPTY_XML_DOCUMENT;
	            }
	        }
	        else {
	            // add some error messages to the boex and exit with error code
	            return OCI_NO_XML_DOCUMENT;
	        }
        } finally {
        	log.exiting();
        }
        return ret;
    }

    /**
     * Depending on the form of the request, HTML or XML, call the appropriate
     * parsing method
     *
     * @param request The request
     * @param requestParser The requestParser
     * @param itemList The ArrayList that will hold the BasketTransferItems in the end (hopefully)
     * @param boex The businessobjectexception-object that any errors will be fixed to, if any.
     * @param requestForm The form of the oci-request. Currently "HTML" or "XML"
     *
     * @return ret 0 if ok, < 0 if an error occurred
     */
    public int ParseRequest(HttpServletRequest  request,
                            RequestParser       requestParser,
                            List                itemList,
                            BusinessObjectException boex,
                            String              requestForm ) {
		final String METHOD = "ParseRequest()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("requestForm="+requestForm);
		try {
		
	        if (requestForm.equals("HTML")) {
	            return ParseHTMLRequest(request, requestParser, itemList, boex);
	        }
	        else if ((requestForm.equals("XML")) && (getOciVersion().equals("2.0"))) {
	            return ParseXMLRequest(request, requestParser, itemList, boex);
	           // return OCI_FORM_UNSUPPORTED;
	        }
			else if ((requestForm.equals("XML")) && (getOciVersion().equals("3.5"))) {
				RequestParser.Parameter rparam;
				rparam = requestParser.getParameter("~xmlDocument");
				if (!rparam.isSet()) {
					// looking for an xml document in the request attribute
					// if no parameter could be found
					rparam = requestParser.getAttribute("~xmlDocument");
				}
	
				if (rparam.isSet()) {
					String xmlString = null;
					xmlString = rparam.getValue().getString();
					if ( xmlString != null && xmlString.length() > 0 ) {
						try {
						DocumentBuilderFactory docfactoryimpl = DocumentBuilderFactory.newInstance();
						docfactoryimpl.setNamespaceAware(true);	 
						InputSource is = new InputSource(new StringReader(xmlString));				
					    Document doc = docfactoryimpl.newDocumentBuilder().parse(is);
						Oci35XMLParser ociP = new Oci35XMLParser(xmlString);    
						
						return ociP.parseXMLDocument(doc, itemList, getShop());
						}
						catch(Exception e) {
							log.error("A exception occurred in XML OCI generation " ,e);
							return OCI_FORM_UNSUPPORTED;
						}
					}
					if (log.isDebugEnabled()) {
						log.debug("The Attribute ~xmlDocument is empty");
					}					
				} else {
					if (log.isDebugEnabled()) {
						log.debug("There is no Parameter ~xmlDocument");
					}	
					return OCI_FORM_UNSUPPORTED;
					
				}
			    return OCI_FORM_UNSUPPORTED;
			}
	        else {
	        	if (log.isDebugEnabled()) {
	        		log.debug("Unknown OciForm " + requestForm + 
	        				  "  or unsupported Version + " + getOciVersion());
	        	}	
	            return OCI_FORM_UNKNOWN;
	        	
	        }
		} finally {
			log.exiting();
		}
    }
	/**
	 * Returns the ociVersion.
	 * @return StringBuffer
	 */
	public String getOciVersion() {
		return ociVersion;
	}

	/**
	 * Sets the ociVersion.
	 * @param ociVersion The ociVersion to set
	 */
	public void setOciVersion(String ociVersion) {
		this.ociVersion = ociVersion;
	}
	
	/**
	 * Get the Shop
	 */
	public ShopData getShop() {
		return (ShopData)shop;
	}

	/**
	 * Set the Shop
	 */
	public void setShop(Shop shop) {
		this.shop = shop;
	}	

}

