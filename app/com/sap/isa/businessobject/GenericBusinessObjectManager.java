/*****************************************************************************
    Class:        GenericBusinessObjectManager
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP AG
    Created:      23.7.2001
    Version:      1.0

    $Revision: #5 $
    $Date: 2001/07/26 $
*****************************************************************************/

package com.sap.isa.businessobject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.businessobject.ObjectBase;
import com.sap.isa.core.businessobject.management.BOManager;
import com.sap.isa.core.businessobject.management.DefaultBusinessObjectManager;
import com.sap.isa.core.logging.IsaLocation;

/**
 * Superclass for the easy implementation of business object managers. You
 * have to create a subclass of this class and implement type safe creation
 * methods for the business objects.
 *
 * @author SAP AG
 * @version 1.0
 */
public class GenericBusinessObjectManager extends DefaultBusinessObjectManager
         implements BOManager, BackendAware {

    // Number of instances expected for one type. This value is no
    // upper limit and only used to initialize the lists with the
    // right size
    private static final int NUM_INSTANCES = 2;

    protected Map  createdObjects;               // Map to store all created objects

    /**
     * Constructs a new instance of the manager providing a reference
     * to the backend object manager that is used.
     *
     * @param bem reference to the backend object manager
     */
    protected GenericBusinessObjectManager() {
        createdObjects = new HashMap();
        log = IsaLocation.getInstance(this.getClass().getName());
    }


    /**
     * Creates a business object of the given type.
     * If such an object was already created, a reference to the old object is
     * returned and no new object is created. This features gurantees that
     * only on instance per given type is available in the application.
     * If this is not what you want, because you need a limited number of
     * instances, use the indexed creation methods.<br>
     *
     * @param type type of the business object that is requested
     * @return reference to a newly created or already existing
     *         business object
     */
    protected synchronized Object createBusinessObject(Class type) {
        return createBusinessObject(type, 0);
    }

    /**
     * Prepares the internal list of created objects for adding an new
     * business object of the given type at the given position and
     * returns a reference to the list of objects maintained for the given
     * type.
     *
     * @param type type of the business object that is requested
     * @param index the index of the object that is requested
     */
    protected List prepareList(Class type, int index) {

        List allInstances = (List) createdObjects.get(type);

        if (allInstances == null) {
            // No object and no index up to now
            allInstances = new ArrayList(NUM_INSTANCES);
            createdObjects.put(type, allInstances);
        }

        // Not enough entries in the list, fill it
        if (index >= allInstances.size()) {
            for (int i = allInstances.size(); i <= index; i++) {
                allInstances.add(null);
            }
        }

        return allInstances;
    }


    /**
     * Creates a business object of the given type but allows to create more
     * than one object per given type. This is necessary to create for example
     * two orders.<br>
     * If such an object for the given index was already created, a reference
     * to the old object is returned and no new object is created.<br>
     *
     * @param type type of the business object that is requested
     * @param index the index of the object that is requested
     * @return reference to a newly created or already existing
     *         business object
     */
    protected synchronized Object createBusinessObject(Class type, int index) {
		final String METHOD = "createBusinessObject()";
		log.entering(METHOD);
        if (log.isDebugEnabled()) {
            log.debug("createBusinessObject('"
                    + type.getName() + "'," + index + ")");
        }

        List allInstances = prepareList(type, index);

        Object instance = allInstances.get(index);

        if (instance == null) {

            try {
                instance = type.newInstance();
            }
            catch (InstantiationException ex1) {
                instance = null;

                if (log.isDebugEnabled()) {
                    log.debug("exception occured during creation "
                            + ex1.toString());
                }
            }
            catch (IllegalAccessException ex2) {
                instance = null;

                if (log.isDebugEnabled()) {
                    log.debug("exception occured during creation "
                            + ex2.toString());
                }

            }

            assignBackendObjectManager(instance);
            
            // Replace the object with the given index (add would insert it!) 
            allInstances.set(index, instance);
            creationNotification(instance);
        }
		log.exiting();
        return instance;

    }

    /**
     * Returns a reference to an existing business object.
     *
     * @param type type of the business object to be returned
     * @return reference to the object or null if no such object is present
     */
    protected synchronized Object getBusinessObject(Class type) {
        return getBusinessObject(type, 0);
    }

    /**
     * Returns a reference to an existing business object referenced by the
     * given index.
     *
     * @param type type of the business object to be returned
     * @param index the index of the object to be returned
     * @return reference to basket object or null if no such object is present
     */
    protected synchronized Object getBusinessObject(Class type, int index) {
		final String METHOD = "getBusinessObject()";
		log.entering(METHOD);
        Object instance = null;
        List allInstances = (List) createdObjects.get(type);

        if (log.isDebugEnabled()) {
            log.debug("getBusinessObject('"
                    + type.getName() + "', " + index + ")");
        }

        if (allInstances != null) {
             instance = allInstances.get(index);
        }

//--        if ((paranoid) && (instance == null)) {
//--            throw new BusinessObjectNotAvailableException(
//--                "Object of type '" + type.getName()+ "' not available.");
//--        }
		log.exiting();
        return instance;
    }

    /**
     * Releases the reference the bom has to an object. Please note, that
     * calling this method for a given business object means that the
     * object is not used anywhere any longer. The <code>destroy</code> method of
     * the object is called. After this the object must be considered to
     * be in an illegal state. Do not use a reference to a business object
     * after calling this method.
     *
     * @param type the type of object to be released
     * @param index the index of the object
     */
    protected synchronized void releaseBusinessObject(Class type, int index) {
		final String METHOD = "releaseBusinessObject()";
		log.entering(METHOD);
        List allInstances = (List) createdObjects.get(type);

        if (log.isDebugEnabled()) {
            log.debug("releaseBusinessObject('"
                    + type.getName() + "', " + index + ")");
        }

        if (allInstances == null) {
        	log.exiting();
            return;
        }

        Object instance = allInstances.get(index);

        if (instance != null) {
            removalNotification(instance);
            callbackDestroy(instance);
            
            // set it to null
            // DO NOT remove it !
            allInstances.set(index, null);
            
            // NO
            // createdObjects.remove(type);
        }
        log.exiting();
    }

    /**
     * Releases the reference the bom has to an object. Please note, that
     * calling this method for a given business object means that the
     * object is not used anywhere any longer. The <code>destroy</code> method of
     * the object is called. After this the object must be considered to
     * be in an illegal state. Do not use a reference to a business object
     * after calling this method.
     *
     * @param type the type of object to be released
     */
    protected synchronized void releaseBusinessObject(Class type) {
        releaseBusinessObject(type, 0);
    }

    /**
     * Makes all business objects invalid by calling their destroy method and
     * calls the destroy() callback on the backend object manager.
     * After calling this method you should not use any business objects or the
     * <code>BackendObjectManager</code> at all. The only thing you are allowed
     * to do after a call to this method is to throw away your reference to
     * this object.<br><br>
     * <b>
     * Normally <code>destroy()</code> is only called internally when the session
     * times out.
     * </b>
     */
    public void release() {
		final String METHOD = "release()";
		log.entering(METHOD);
		Collection objectsList = createdObjects.values();
		Iterator it = objectsList.iterator();

		if (log.isDebugEnabled()) {
			log.debug("release was called for all objects");
		}

		while (it.hasNext()) {
		  Object objects = it.next();
		  if(objects == null || ! (objects instanceof List))
		  	continue;
		  List subList = (List)objects;
		  Iterator subIt = subList.iterator();
		  while(subIt.hasNext()) {	
			Object element = subIt.next();
			
            // set references to the bem to null
            if (element instanceof BackendAware) {
                ((BackendAware) element).setBackendObjectManager(null);
            }

            // tell business objects to drop ressources
            if (element instanceof ObjectBase) {
                ((ObjectBase) element).destroy();
            }
		  }
        }
        log.exiting();
    }


    protected void callbackDestroy(Object o) {
        if (o instanceof ObjectBase) {
            ((ObjectBase)o).destroy();
        }
    }

}