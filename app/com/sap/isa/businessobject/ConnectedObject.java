/*****************************************************************************
    Class:        ConnectedObject
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Created:      December 2002

    $Revision: #0 $
    $Date: 2002/05/28 $
*****************************************************************************/
package com.sap.isa.businessobject;

import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.backend.boi.isacore.ConnectedObjectData;

/**
 * The Connected Object represents an entry in the document flow
 */
abstract public class ConnectedObject extends BusinessObjectBase
                               implements ConnectedObjectData {

    protected String docNumber = "";
    protected String docItemNumber = "";
    protected String docType = "";
    protected boolean displayable = false;
    protected String transferUpdateType = UNDEFINED;


    /**
     * Returns the document number 
     *
     * @return document number is used to identify the document in the backend
     */
    public String getDocNumber() {
        return docNumber;
    }
    /**
     * Returns the document item number 
     *
     * @return document number is used to identify the document item in the backend
     */
    public String getDocItemNumber() {
        return docItemNumber;
    }

    /**
     * Sets the document number
     */
    public void setDocNumber(String docNumber) {
        this.docNumber = docNumber;
    }

    /**
     * Sets the document number
     */
    public void setDocItemNumber(String docItemNumber) {
        this.docItemNumber = docItemNumber;
    }

    /**
     * Returns the binary transfer update type
     *
     * @return binary transfer update type
     */
    public String getTransferUpdateType() {
        return transferUpdateType;
    }

    /**
     * Sets the binary transfer update type
     */
    public void setTransferUpdateType(String transferUpdateType) {
        this.transferUpdateType = transferUpdateType;
    }


    /**
     * Returns the document type
     *
     * @return document type charaterizes the document in the backend
     * (e.g., order, quotation, order template)
     */
    public String getDocType() {
        return docType;
    }

    /**
     * Sets the document type
     */
    public void setDocType(String docType) {
        this.docType = docType;
    }

    /**
     * Returns the displayable property
     *
     * @return displayable property determines if the document may be
     * displayed in the order status
     */
    public boolean isDisplayable() {
        return displayable;
    }

    /**
     * Sets the displayable property
     */
    public void setDisplayable(boolean displayable) {
        this.displayable = displayable;
    }
}
