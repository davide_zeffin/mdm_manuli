/*****************************************************************************
	Class         EAnalB0ScenarioMapper
	Copyright (c) 2004, SAP Labs LLC, PaloAlto All rights reserved.
	Author:       
	Created:      Created on Jan 6, 2004
	Version:      1.0

	$Revision$
	$Date$
*****************************************************************************/
package com.sap.isa.businessobject;

import java.util.HashMap;

import com.sap.isa.businessobject.capture.CapturerConfig;
import com.sap.isa.core.logging.IsaLocation;

/**
 * 
 * Maintains Map between XCM scenarios and its corresponding Capture
 * configuration.
 * Each scenario will have one capture configuration.
 */
public class EAnalB0ScenarioMapper{
		

	private static HashMap captureConfigMap =  new HashMap(4);
	static final private IsaLocation loc = IsaLocation.getInstance(EAnalB0ScenarioMapper.class.getName());

	
	/**
	 * Returns existing reference to CaptureConfig, if existing
	 * Otherwise create a new BO CaptureConfig
	 * and load the configuraiton from backend.
	 * @param scenario name of the XCM scenario
	 * @param bom ISACORE-BOM
	 * @return reference to Captureconfig object, with loaded configuraiton
	 * from backend
	 */
	public static synchronized CapturerConfig getCaptureConfig(String scenario, BusinessObjectManager bom) {
		final String METHOD = "getCaptureConfig()";
		loc.entering(METHOD);
		if (loc.isDebugEnabled())
			loc.debug("scenario = "+scenario);
		CapturerConfig config =  (CapturerConfig) captureConfigMap.get(scenario);
		  if(config == null){
			config =  (CapturerConfig) bom.createCaptureConfig();
			captureConfigMap.put(scenario, config); 
			try {
				config.loadConfiguration(scenario);
			} catch (RuntimeException e) {
				config.setConfigFetchAttempted(true);
				//ignore
			}
		  }
		 loc.exiting();
	 	 return config;	
	}
	

	/* 
	 * Release all BackendObject managers associated with BOs store in 
	 * captureConfig map for various scenarios
	 * @see com.sap.isa.core.businessobject.management.BOManager#release()
	 */
	public static synchronized void release() {
		captureConfigMap = null; //@TODO - check its completeness
	}

}
