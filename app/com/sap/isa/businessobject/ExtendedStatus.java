/*****************************************************************************
    Class:        ExtendedStatus
    Copyright (c) 2002, SAP AG, Germany, All rights reserved.
    Author:

    $Revision: #1 $
    $DateTime: 2003/09/25 18:19:45 $ (Last changed)
    $Change: 150323 $ (changelist)

*****************************************************************************/

package com.sap.isa.businessobject;

import com.sap.isa.core.TechKey;
import com.sap.isa.backend.boi.isacore.*;
import java.util.*;

/**
 *
 * This class contains all data regarding status information for a object.
 * Following example show how it can be used on item-level.<br>
 * <pre>
 *   <td id="row_<%=line%>">
 *    <% Iterator extStatus = item.getExtendedStatus().getStatusIterator();
 *       request.setAttribute("EXTENDEDSTATUS",extStatus);%>
 *    <select id="status_<%=line%>" name="statusname_<%=line%>" class="bigCatalogInput">
 *        <isa:iterate id="eStatus"
 *                     name="EXTENDEDSTATUS"
 *                     type="com.sap.isa.businessobject.ExtendedStatusListEntry">
 *         <option value="<%=eStatus.getTechKey().getIdAsString()%>"
 *                        <%= eStatus.isCurrentStatus() ? "selected" : ""%>><%=eStatus.getDescription()%></option>
 *         </isa:iterate>
 *    </select>
 *   </td>
 * </pre>
 *
 * @version 1.0
 */
public class ExtendedStatus
             implements ExtendedStatusData {

  private List statusList;
  private ExtendedStatusListEntry currentStatus;
  private ExtendedStatusListEntry selectedStatus;

  /**
   * Constructor
   */
    public ExtendedStatus() {
        statusList = new ArrayList(10);
    }
    /**
     * Factory method to create ExtendedStatusListEntryData objects
     */
    public ExtendedStatusListEntryData createExtendedStatusListEntry(
                                              TechKey statusKey,
                                              String entryPos,
                                              String descript,
                                              String descript_short,
                                              String busprocess) {
       return (ExtendedStatusListEntryData)new ExtendedStatusListEntry(
                                                  statusKey,
                                                  entryPos,
                                                  descript,
                                                  descript_short,
                                                  busprocess);
   }

   /**
    * Add a new ExtendedStatusListEntry object to the list
    * @param entry ExtendedStatusListEntry object
    */
    public void addStatusEntry(ExtendedStatusListEntryData entry) {
         statusList.add(entry);
    }
   /**
    * Get the <code>Iterator</code> for the ExtendedStatusList. List contains all
    * status which could be set for this item.
    */
    public Iterator getStatusIterator() {
        return statusList.iterator();
     }
   /**
    * Set the new selected status. Use this method to inform process which status
    * has been set via Web.
    */
    public void setSelectedStatus(ExtendedStatusListEntry selectedStatus) {
        this.selectedStatus = selectedStatus;
    }
   
    
   /**
    * Get selected status. Method provides the status which has been set via Web.
    * Do not use to display status on Web. Use </code>getStatusIterator()</code> instead.
    */
   public ExtendedStatusListEntryData getSelectedStatus() {
       return (ExtendedStatusListEntryData)this.selectedStatus;
   }
   
   /**
    * Get the current status. Method provides the current status in The backend.
    */
   public ExtendedStatusListEntryData getCurrentStatus() {
       return (ExtendedStatusListEntryData)this.currentStatus;
   }
   
   /**
    * Backend Method used to set current status for the item.
    */
    public void setCurrentStatus(ExtendedStatusListEntryData entry) {
        Iterator itSL = statusList.iterator();
        // Find index off current status in List
        while (itSL.hasNext()) {
            ExtendedStatusListEntry statusEntry = (ExtendedStatusListEntry)itSL.next();
            statusEntry.setCurrentStatus(false);
            if (statusEntry.getTechKey().getIdAsString().equals(
                    ((ExtendedStatusListEntry)entry).getTechKey().getIdAsString())) {
                this.currentStatus = statusEntry;
                statusEntry.setCurrentStatus(true);
                break;
            }
        }
        if (currentStatus == null) {
            // Current Status not included in the list
            this.currentStatus = (ExtendedStatusListEntry)entry;
        }
    }
    /**
     * Object related toString method
     */
    public String toString() {
        String fs = "";
        // current Status
        fs.concat("Current Status: " + this.currentStatus.toString());
        // selected Status
        fs.concat("Selected Status: " + this.selectedStatus.toString());
        // Status List
        Iterator itSL = statusList.iterator();
        int idx = 1;
        while (itSL.hasNext()) {
            ExtendedStatusListEntry statusEntry = (ExtendedStatusListEntry)itSL.next();
            fs.concat("StatusList: " + Integer.toString(idx));
            fs.concat(statusEntry.toString());
        }
        return fs;
    }
}
