package com.sap.isa.businessobject;

/*****************************************************************************
    Copyright (c) 2000, SAPLAbs Europe GmbH, Germany, All rights reserved.
*****************************************************************************/

import com.sap.isa.backend.BackendTypeConstants;
import com.sap.isa.backend.boi.isacore.UOMConverterBackend;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;

/**
 * This is used to convert the order object into PDF format.
 * 
 */
public class UOMConverter extends BusinessObjectBase
implements BackendAware,
    BackendBusinessObjectParams {
    protected UOMConverterBackend uomConverterBackend;
    protected BackendObjectManager bem;
    
    /**
     * Constructor.
     */
    public UOMConverter() {
        super();
    }
    
    /**
     * Sets the BackendObjectManager for the attribute set. This method is used
     * by the object manager to allow the user object interaction with
     * the backend logic. This method is normally not called
     * by classes other than BusinessObjectManager.
     *
     * @param bem BackendObjectManager to be used
     */
    public void setBackendObjectManager(BackendObjectManager bem) {
        this.bem = bem;
    }
    
    // get the UOMConverterBackend, if necessary
    protected UOMConverterBackend getUOMConverterBackend()
        throws BackendException {
        if (uomConverterBackend == null) {
			uomConverterBackend = (UOMConverterBackend)
            bem.createBackendBusinessObject(BackendTypeConstants.BO_TYPE_UOMCONVERTER, null);
        }
        return uomConverterBackend;
    }
    
    /**
    *
    * Convert the order to PDF using its techkey and the language
    *
    */
    public String convertUOM(boolean saptoiso,String uom) throws CommunicationException {
        // read oci lines from backend
		final String METHOD = "convertUOM()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("saptoiso="+saptoiso+", uom="+uom);
        String str=null;
		try {
	            str = getUOMConverterBackend().convertUOM(saptoiso,uom);
	        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        } finally{
        	log.exiting();
        }
        return str;
    }
    
    /**
    *
    * Convert the given unit of measurment for  the given language.
    * 
    * It is necessary, that the used Backend class uses an ISAStateless connection, not an ISACoreStateless
    * for this method, to work correctly.
    * 
    * @param boolean saptoiso converting uom from sap to iso format or the other way around
    * @param String uom the uom to convert
    * @param String languageIso the language, to convert for.
    */
    public String convertUOM(boolean saptoiso, String uom, String languageIso) throws CommunicationException {
        // read oci lines from backend
        final String METHOD = "convertUOM()";
        log.entering(METHOD);
        if (log.isDebugEnabled())
            log.debug("saptoiso=" + saptoiso + ", uom=" + uom + ", languageIso=" + languageIso);
        String str=null;
        try {
                str = getUOMConverterBackend().convertUOM(saptoiso, uom, languageIso);
            }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        } finally{
            log.exiting();
        }
        return str;
    }
    
}
