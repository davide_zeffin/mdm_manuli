/*****************************************************************************
    Class:        IsaClient
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Alexander Staff
    Created:      26.3.2001
    Version:      1.0

    $Revision: #2 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.businessobject;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;

import com.sap.isa.catalog.boi.IClient;
import com.sap.isa.catalog.impl.CatalogClient;
import com.sap.isa.catalog.impl.CatalogView;

/**
 * Class representing the client for a catalog object.
 * Needed to instantiate the WebCatInfo-object in the bom
 *
 * @author Alexander Staff
 * @version 1.0
 */
public final class IsaClient extends CatalogClient implements IClient {

    /**
     * Creates a new instance
     *
     */
    public IsaClient() {
        this.theViews = new ArrayList();
    }

    /**
     * Sets the locale of the client as a java.util.locale
     *
     * @param locale Locale of the client
     */
    public void setLocale( Locale locale ) {
        this.theLocale = locale;
    }

    /**
     * Sets the name of the catalog user/usergroup the client belongs to or represents
     *
     * @param name Catalog user/usergroup the client belongs to or represents
     */
    public void setName( String name ) {
        this.theName = name;
    }

    /**
     * Sets the password of the user/usergroup for the catalog server
     *
     * @param password The password for the user on the catalog
     */
    public void setPWD( String password ) {
        this.thePWD = password;
    }

    /**
     * Sets the views for the client/user/catalog combination
     *
     * @param views The valid views
     */
    public void setViews( ArrayList views ) {
        this.theViews = views;
    }

    /**
     * adds a view for the client/user/catalog combination
     *
     * @param view The view to add as a String
     */
    public void addView( String view ) {
        this.theViews.add(new CatalogView(view));
    }

    /**
     * Returns the guid of the session
     *
     * @return guid
     */
    public String getGuid() {
        return null;
    }

    /**
     * Returns the permissions
     *
     * @return permissions
     */
    public Iterator getPermissions() {
        return null;
    }

}