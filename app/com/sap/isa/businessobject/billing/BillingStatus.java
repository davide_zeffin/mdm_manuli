/*****************************************************************************
    Class:        BillingStatus
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author        Ralf Witt
    Created:      May 2001

    $Revision: #4 $
    $Date: 2004/04/05 $
*****************************************************************************/

package com.sap.isa.businessobject.billing;

import java.util.ArrayList;
import java.util.Iterator;

import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.billing.BillingStatusBackend;
import com.sap.isa.backend.boi.isacore.billing.BillingStatusData;
import com.sap.isa.backend.boi.isacore.billing.HeaderBillingData;
import com.sap.isa.backend.boi.isacore.order.DocumentListFilterData;
import com.sap.isa.businessobject.BusinessObjectHelper;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.order.DocumentListFilter;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.core.Iterable;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.eai.BackendException;
       
/**
 *
 * The BillingStatus class handle the questionary which is
 * used to retrieve detail status information of billing documents
 *
 */
public class BillingStatus extends BillingStatusDocument
                implements BackendAware, Iterable, BillingStatusData, DocumentState{

    protected Shop shop;

    private DocumentListFilter filter;

    private ArrayList billingHeader = new ArrayList(50);

    private int billingCount = 0;
	
    /**
      Constructor
      @param Default
    */
    public BillingStatus() {
    }


    /**
     * Add a new billing Header to the list
     *
     * @param HeaderBillingDocumentData which should add to the billinglist
     *
     * @see HeaderBillingDocument
     * @see HeaderBillingDocumentData
     */
    public void addBillingHeader(HeaderBillingData header) {
        billingHeader.add(header);
        billingCount++;
    }

    /**
     * Get the Shop
     */
    public ShopData getShop() {
        return (ShopData)shop;
    }


    /**
     * Set the Shop
     */
    public void setShop(Shop shop) {
        this.shop = shop;
    }

    
    /**
     * Get the filter parameter (selection criteria)
     */
    public DocumentListFilterData getFilter() {
        // When used as data container, object not yet set!!
        if (filter == null)
             filter = new DocumentListFilter();

        return (DocumentListFilterData)filter;
    }


    /**
     * Gets the number of read document headers
     */
    public int getBillingCount() {
      return billingCount;
    }


     /**
     * Returns an iterator for the billing headers. Method necessary because of
     * the <code>Iterable</code> interface.
     *
     * @return iterator to iterate over billing document headers
     *
     */
    public Iterator iterator() {
        return billingHeader.iterator();
    }



    /* get the BackendService, if necessary */
    private BillingStatusBackend getBillingStatusBackend()
            throws BackendException{

		return (BillingStatusBackend)createBackendService();
    } // OrderListBackend


    /**
     * Read the object for the given techKey
     * @param Techkey         of the document to read
     * @param String          origin of the docuement (system)
     * @param Shop            reference
     * @param BusinessPartner of type soldto
     */
    public void readBillingStatus(TechKey techKey, String docsOrigin, Shop shop, BusinessPartner soldTo)
            throws CommunicationException {
		final String METHOD = "readBillingStatus()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("techKey="+techKey+", docsOrgin="+docsOrigin+", shop="+shop+"soldTo="+soldTo);
        this.shop = shop;
        // billingHeader.clear();
        TechKey soldToKey = (soldTo != null ? soldTo.getTechKey() : null);
        readBillingStatus(techKey,docsOrigin,shop, soldToKey);
        log.exiting();
    }


     /**
     * Read the BillingHeaders for the given business partners (sold_to/contact USER) from backend
     *
     * @param user  contains the soldTo GUID which identify a business partner in the backend
     * @param shop  GUID which identify a shop in the backend
     * @param filter specifice the select options for the list
     */
    public void readBillingHeaders(TechKey soldToKey, Shop shop, DocumentListFilter filter)
            throws CommunicationException {
		final String METHOD = "readBillingHeaders()";
		log.entering(METHOD);
        this.soldToKey = soldToKey;
        
        this.shop   = shop;
        this.filter = filter;
        billingHeader.clear();
        this.billingCount = 0;

        try {
            // read BillingList from Backend
			getBillingStatusBackend().readBillingHeaders(this);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally {
        	log.exiting();
        }
    }
}
