/*****************************************************************************
    Class:        BillingStatusDocument
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author        Ralf Witt
    Created:      May 2001

    $Revision: #4 $
    $Date: 2004/04/30 $
*****************************************************************************/

package com.sap.isa.businessobject.billing;

import java.util.ArrayList;
import java.util.Iterator;

import com.sap.isa.backend.BackendTypeConstants;
import com.sap.isa.backend.boi.isacore.BaseConfiguration;
import com.sap.isa.backend.boi.isacore.DocumentStatusConfiguration;
import com.sap.isa.backend.boi.isacore.billing.BillingStatusDocumentBackend;
import com.sap.isa.backend.boi.isacore.billing.BillingStatusDocumentData;
import com.sap.isa.backend.boi.isacore.billing.HeaderBillingData;
import com.sap.isa.backend.boi.isacore.billing.ItemBillingData;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.businessobject.BusinessObjectHelper;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.header.HeaderBillingDocument;
import com.sap.isa.businessobject.item.ItemBillingDoc;
import com.sap.isa.businessobject.item.ItemList;
import com.sap.isa.businessobject.item.ItemMap;
import com.sap.isa.core.Iterable;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.eai.BackendBusinessObject;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;

/**
 *
 * The BillingStatusDocument class handle the questionary which is
 * used to retrieve detail status information of billing documents
 *
 */
public class BillingStatusDocument extends BusinessObjectBase
                implements BackendAware, Iterable, BillingStatusDocumentData, DocumentState{

    private BackendObjectManager bem;

	// is private use {@link #createBackendService} instead to get the backend object.
    private BackendBusinessObject backendService;

    protected String billingDocNumber;
    protected String billingDocsOrigin;
    protected HeaderBillingDocument singleDocumentHeader;
    protected ItemMap itemMap = new ItemMap();
    protected ItemList itemList = new ItemList();
	protected TechKey soldToKey;


    protected int state = DocumentState.UNDEFINED;
    /**
     * in case that for large doucments not all items of the doucment are read 
     * into the itemList, this field holds the original no of items in this doucment.
     * This means the number of items that were present when the document was first
     * read. Adding new items or deleting old ones will not change this value.
     */
    protected int noOfOriginalItems = NO_OF_ITEMS_UNKNOWN;
    /**
     * if only certain items of the document should be shown or changed, this list is used, to store the guids of those items 
     */
    protected ArrayList selectedItemGuids = new ArrayList();

    /**
      Constructor
      @param Default
    */
    public BillingStatusDocument() {
    }


	 /**
	 * Gets the state of the document
	 *
	 * @return the state as described by the constants of interface DocumentState
	 */	
	 public int getState() {
	 	return state;
	 }

	/**
	  * Sets the state of the document
	  *
	  * @param state the state as described by the constants of interface DocumentState
	  */
	public void setState(int state) {
	    this.state = state;
	}
	

    /**
     * Get TechKey of the Sold-To-Party
     */
    public TechKey getSoldToTechKey() {
        return soldToKey;
    }


    /**
     * Get the Billing document number (ID)
     */
    public String getBillingDocumentNumber() {
        return billingDocNumber;
    }

    /**
     * Set the Billing document number (ID)
     */
    public void setBillingDocumentNumber(String billingDocNo) {
        this.billingDocNumber = billingDocNo.trim();
    }

    /**
     * Get a new Header object
     */
    public HeaderBillingData createHeader() {
        return new HeaderBillingDocument();
    }
    /**
     * Get a new Item object
     */
    public ItemBillingData createItem() {
        return (ItemBillingData)new ItemBillingDoc();
    }

    /**
     * Adds a <code>Item</code> to the itemlist. This item must be uniquely
     * identified by a technical key.
     * A <code>null</code> reference passed to this function will be ignored,
     * in this case nothing will happen.
     *
     * @param item Item to be added to the basket
     */
    public void addItem(ItemBillingData itemData) {
		final String METHOD = "addItem()";
		log.entering(METHOD);
		if(log.isDebugEnabled())
			log.debug("itemData="+itemData);
		  if (itemData instanceof ItemBillingDoc) {
		    ItemBillingDoc item = (ItemBillingDoc)itemData;
		    if (item != null) {
		        itemMap.add(item);          // for searching
		        itemList.add(item);         // for listing
		    }
		  }
		  else {
		    log.debug("Wrong type of item: " + itemData.getClass().getName());
		      }
		  log.exiting();
    }

    /**
     * Get item by its techkey
     */
     public ItemBillingDoc getItemByTechKey(TechKey techKey) {
         return (ItemBillingDoc)itemMap.get(techKey);
     }

    /**
     * Returns <tt>true</tt> if item exists
     *
     * @return <tt>true</tt> if item exists
     *
     * @param techKey items techKey whose presence in this Map is to be tested.
     */
     public boolean itemExists(TechKey techKey) {
         return itemMap.containsKey(techKey);
     }


    /**
     * Get the billing documents origin, where the document has been created
     */
    public String getBillingDocumentsOrigin() {
        return billingDocsOrigin;
    }

    /**
     * Set the billing documents origin, where the document has been created
     */
    public void setBillingDocumentsOrigin(String DocsOrigin) {
        this.billingDocsOrigin =  DocsOrigin.trim();
    }

     /**
      * Get single billing document header
      */
     public HeaderBillingDocument getDocumentHeader() {
         return this.singleDocumentHeader;
     }

    /**
     * Set a single billing document header (belonging to the items)
     */
    public void setDocumentHeader(HeaderBillingData header) {
        this.singleDocumentHeader = (HeaderBillingDocument)header;
    }


     /**
     * Returns an iterator for for the billing items. Method necessary because of
     * the <code>Iterable</code> interface.
     *
     * @return iterator to iterate over billing document headers
     *
     */
    public Iterator iterator() {
        return itemList.iterator();
    }

    /**
     * Get ItemList
     */
     public ItemList getItemList() {
         return itemList;
     }

     /**
     * Returns an iterator for the billing items. 
     *
     * @return iterator to iterate over billing document items
     *
     */
    public Iterator getItemsIterator() {
        return itemList.iterator();
    }

   /**
     * Sets the BackendObjectManager for the BillingStatus. This method is used
     * by the object manager to allow the user object interaction with
     * the backend logic. This method is normally not called
     * by classes other than BusinessObjectManager.
     *
     * @param bem BackendObjectManager to be used
     */
    public void setBackendObjectManager(BackendObjectManager bem) {
        this.bem = bem;
    } // setBackendObjectManager

    /* get the BackendService, if necessary */
    private BillingStatusDocumentBackend getBackendStatusDocumentService()
            throws BackendException{
    	return (BillingStatusDocumentBackend)createBackendService();        	
    }
    /**
     * For large documents, where not all items are loaded into the itemlist
     * this should return the total no of items, belonging to the document.
     * This means the number of items that were present when the document was first
     * read. Adding new items or deleting old ones will not change this value.
     * 
     * @return the number of items in the document or 0 if unknown
     */
    public int getNoOfOriginalItems() {
        return noOfOriginalItems;
    }

    /**
     * Returns true, if the document is considered to be a large
     * document. This is the fact, if the value of noOfItems is
     * greater or equal to shop.getLargeDocNoOfItemsThreshold().
     * 
     * @param largeDocNoOfRows number of rows from that a document is 
     *        considered as large
     * @return boolean true if document is a large document,
     *         false else
     */
    public boolean isLargeDocument(int largeDocNoOfRowsThres) {
        return largeDocNoOfRowsThres != BaseConfiguration.INFINITE_NO_OF_ITEMS && noOfOriginalItems != NO_OF_ITEMS_UNKNOWN && noOfOriginalItems > largeDocNoOfRowsThres;
    }
 
    /**
     * For large documents, where not all items are loaded at a time, this list
     * contains all guids of items that should currently be loaded.
     * 
     * @return list of selected items
     */
    public ArrayList getSelectedItemGuids() {
        return selectedItemGuids;
    }

    /**
     * For large documents, where not all items are loaded at a time, this list
     * contains all guids of items that should currently be loaded.
     * 
     * @param selectedItemGuids list of selected items
     */
    public void setSelectedItemGuids(ArrayList selectedItemGuids) {
        this.selectedItemGuids = selectedItemGuids;
    }
    
    /**
     * For large documents, where not all items are loaded into the itemlist
     * the total no of items, belonging to the document can be set.
     * This means the number of items that were present when the document was first
     * read. Adding new items or deleting old ones will not change this value.
     * 
     * @param noOfItems the number of items in the document, or 0 if unknown
     */
    public void setNoOfOriginalItems(int noOfOriginalItems) {
        this.noOfOriginalItems = noOfOriginalItems;
    }

	/**
	 * Get the reference of the backend service and creates it when necessary
	 * this protected method can be used by inherited classes to get
	 * the backend service
	 *
	 * @return the "typeless" backend business object
	 *
	 */
	protected BackendBusinessObject createBackendService()
		throws BackendException {

        if (backendService == null) {

            try {
                // get the Backend from the Backend Manager
                // method not found in BackendManager
                backendService =
                         bem.createBackendBusinessObject(BackendTypeConstants.BO_TYPE_BILLINGSTATUS, null);
            }
            catch (Exception ex) {
                // debug:begin
                if (log.isDebugEnabled()) log.debug("BillingStatus(BillingStatusBackend):" + ex);
               // debug:end
                throw(new BackendException(ex.toString()));
            }

        }
        return backendService;
    } // OrderListBackend


    /**
     * Read the object for the given techKey
     *
     */
    public void readBillingStatus(TechKey techKey, 
    							  String docsOrigin, 
								  DocumentStatusConfiguration configuration, 
    							  TechKey soldToKey)
            throws CommunicationException {
		final String METHOD = "readBillingStatus()";
		log.entering(METHOD);
		if(log.isDebugEnabled())
			log.debug("techKey="+techKey+", docsOrgin="+docsOrigin+"configuration="+configuration+"soldToKey="+soldToKey);
        try{
	        if (techKey == null) {
	            // re-read requested if neccessary
	            if ( ! this.equals(singleDocumentHeader.getTechKey()) ) {
	               itemMap.clear();
	               itemList.clear();
	
	            } else {
	              // quit method, data still up-to-date
	              return;
	            }
	
	        } else {
	            // Normal entry, read details
	            if ( ! this.techKey.equals(techKey)  &&
	                 !"".equals(techKey.getIdAsString()) ) {
	            	selectedItemGuids.clear();
	            }
	            if (!"".equals(techKey.getIdAsString())) {
                    //	Empty techkey => Keep original controlling values
					this.techKey = techKey; 
					this.soldToKey = soldToKey;
					this.billingDocsOrigin = docsOrigin;
	            }
	            itemMap.clear();
	            itemList.clear();
	        }
	
	
	        try {
	            // read AttributeSet from Backend
				getBackendStatusDocumentService().readBillingStatus(this, configuration);
	        }
	        catch (BackendException ex) {
	            BusinessObjectHelper.splitException(ex);
	        }
        } finally {
        	log.exiting();
        }
    }
    /**
     * Check both techkeys are equal
     * @param Techkey to check
     * @return boolean true if both techkeys are equal
     */
    protected boolean equals(TechKey keyToCompare) {
        String key1 = removeLeadingZero(keyToCompare.getIdAsString().trim());
        String key2 = removeLeadingZero(this.getTechKey().toString().trim());
        
        return key1.equals(key2);
    }
    /**
     * Remove leading zeros
     */
    protected String removeLeadingZero(String str) {
        if (str == null) {        
            return null;    
        }   
        char[] chars = str.toCharArray();
        int index = 0;
        for (; index < str.length(); index++) {       
            if (chars[index] != '0') {            
                break;       
            }    
        }    
        return (index == 0) ? str : str.substring(index);
    }
}
