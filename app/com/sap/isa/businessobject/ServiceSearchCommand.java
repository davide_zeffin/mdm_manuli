/*****************************************************************************
    Class:        ServiceSearchCommand
    Copyright (c) 2002, SAP AG, Germany, All rights reserved.
    Author        SAP
*****************************************************************************/

package com.sap.isa.businessobject;

import com.sap.isa.backend.BackendTypeConstants;
import com.sap.isa.backend.boi.isacore.service.ServiceSearchBackend;
import com.sap.isa.businessobject.order.DocumentListFilter;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.table.Table;


/**
 * Defines the search command necessary to retrieve a list of service documents
 * available.
 */
public class ServiceSearchCommand implements SearchCommand {

    private TechKey            soldToKey;
    private DocumentListFilter docFilter;
    private String             processType;
    private String             language;
	static final private IsaLocation loc = IsaLocation.getInstance(ServiceSearchCommand.class.getName());

    /**
     * Creates a new search command for the soldTo and document list filter
     *
     * @param SoldTo for which the search should be performed
     * @param DocumentListFilter descripts date ranges and service types
     */
    public ServiceSearchCommand(TechKey soldToKey,
                                DocumentListFilter docFilter,
                                String processType,
                                String language) {
        this.soldToKey = soldToKey;
        this.language = language;
        this.processType = processType;
        this.docFilter = docFilter;
    }

    /**
     * Returns the SoldTo, this search is performed for.
     *
     * @return TechKey specifying the soldTo
     */
    public TechKey getSoldTo() {
        return soldToKey;
    }

    /**
     * Returns the property language
     *
     * @return language
     *
     */
    public String getLanguage() {
       return this.language;
    }

    /**
     * Returns the property process type
     *
     * @return processType
     *
     */
    public String getProcessType() {
       return this.processType;
    }

    /**
     * Returns the DocumentListFilter, this search is performed for.
     *
     * @return DocumentListFilter
     *
     */
    public DocumentListFilter getDocumentListFilter() {
       return this.docFilter;
    }


    /**
     * Perform the search in the backend.
     * 
     * @param bem With the backend object manager the search command get 
     *             access to the backend.
     * @return SearchResult 
     * @throws CommunicationException
     */
    public SearchResult performSearch(BackendObjectManager bem)
        	throws CommunicationException {
		final String METHOD = "performSearch()";
		loc.entering(METHOD);
        SearchResult result = null;

        try {
            ServiceSearchBackend ssb =
                (ServiceSearchBackend) bem.createBackendBusinessObject(BackendTypeConstants.BO_TYPE_SERVICE_SEARCH, null);

            Table resultTable = ssb.readServices(this.soldToKey.getIdAsString(),
                                                 this.getDocumentListFilter().getType(),     // businessObjectType
                                                 this.getProcessType(),
                                                 this.getDocumentListFilter().getServiceStatus(),
                                                 this.getDocumentListFilter().getChangedDate(),
                                                 this.getDocumentListFilter().getChangedToDate(),
                                                 this.getLanguage());

            result = new SearchResult(resultTable, null);

        } catch (BackendException e) {
            BusinessObjectHelper.splitException(e);
            return null; // never reached
        }
		finally {
			loc.exiting();
		}
        return result;
    }



}