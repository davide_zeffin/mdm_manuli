package com.sap.isa.businessobject.activity;

/**
 * Title:        eCall Project
 * Description:  eCall projects provides multi-channel functionality for service
 *  * scenario. It supports chat, call me back, VoIP and email and integartion to
 *  * backend systems.
 * Copyright:    Copyright (c) 2001
 * Company:      SAPMarkets Inc. Palo Alto, CA
 * @version 1.0
 */
import com.sap.isa.backend.boi.isacore.activity.ActivityDateData;

public class ActivityDate implements ActivityDateData{

	 /** private attributes **/
	 private String guid = "";
	 private String refHandle = "";
	 private String refKind = "";
	 private String apptType = "";
	 private String timeStampFrom = "";
	 private String timeZoneFrom = "";
	 private String timeStampTo = "";
	 private String timeZoneTo = "";
	 private String dateFrom = "";
	 private String dateTo = "";
	 private String timeFrom = "";
	 private String timeTo = "";


    public ActivityDate() {
    }

    public void setGuid (String guid) {
	     this.guid = guid;
    }
    public String getGuid (){
	     return this.guid;
    }

    public void setHandle (String handle){
	     this.refHandle = handle;
    }
    public String getHandle (){
	     return this.refHandle;
    }

    public void setKind (String kind){
	     this.refKind = kind;
    }

    public String getKind (){
	     return this.refKind;
    }

    public void setApptType (String type){
	     this.apptType = type;
    }

    public String getApptType (){
	     return this.apptType;
    }

    public void setTimeStampFrom (String from){
	     this.timeStampFrom = from;
    }

    public String getTimeStampFrom (){
	     return this.timeStampFrom;
    }

    public void setTimeZoneFrom (String from){
	     this.timeZoneFrom = from;
    }
    public String getTimeZoneFrom () {
	     return this.timeZoneFrom;
    }

    public void setTimeStampTo (String to){
	     this.timeStampTo = to;
    }

    public String getTimeStampTo (){
	     return this.timeStampTo;
    }

    public void setTimeZoneTo (String to){
	     this.timeZoneTo = to;
    }

    public String getTimeZoneTo (){
	     return this.timeZoneTo;
    }

    public void setTimeFrom (String from){
	     this.timeFrom = from;
    }

    public String getTimeFrom (){
	     return this.timeFrom;
    }

    public void setTimeTo (String to){
	     this.timeTo = to;
    }

    public String getTimeTo () {
	     return this.timeTo;
    }

    public void setDateFrom (String from){
	     this.dateFrom = from;
    }

    public String getDateFrom (){
	     return this.dateFrom;
    }

    public void setDateTo (String to){
	  this.dateTo = to;
    }

    public String getDateTo (){
	     return this.dateTo;
    }
}