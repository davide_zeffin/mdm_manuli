package com.sap.isa.businessobject.activity;

/**
 * Title:        eCall Project
 * Description:  eCall projects provides multi-channel functionality for service
 *  * scenario. It supports chat, call me back, VoIP and email and integartion to
 *  * backend systems.
 * Copyright:    Copyright (c) 2001
 * Company:      SAPMarkets Inc. Palo Alto, CA
 * @version 1.0
 */
import com.sap.isa.backend.boi.isacore.activity.ActivityTextData;

public class ActivityText implements ActivityTextData{


	 /** private attributes **/
	 private String guid = "";
	 private String refHandle = "";
	 private String refKind = "";
	 private String tdId = "";
	 private String tdSpras = "";
	 private String langIso = "";
	 private String tdStyle = "";
	 private String tdForm = "";
	 private String tdFormat = "";
	 private String tdLine = "";
	 private String mode = "";

       public ActivityText() {
       }

	  public void setGuid (String guid) {
		this.guid = guid;
	  }
	  public String getGuid (){
		return this.guid;
	  }

	  public void setHandle (String handle){
		this.refHandle = handle;
	  }
	  public String getHandle (){
		return this.refHandle;
	  }

	  public void setKind (String kind){
		   this.refKind = kind;
	  }
	  public String getKind (){
		return this.refKind;
	  }

	  public void setTextId (String id){
		this.tdId = id;
	  }
	  public String getTextId (){
		return this.tdId;
	  }

	  public void setTextLang (String lang){
		this.tdSpras = lang;
	  }
	  public String getTextLang (){
		return this.tdSpras;
	  }

	  public void setTextISO (String iso){
		   this.langIso = iso;
	  }
	  public String getTextISO (){
		return this.langIso;
	  }

	  public void setStyle (String style){
		this.tdStyle = style;
	  }
	  public String getStyle (){
		return this.tdStyle;
	  }

	  public void setForm (String form){
		this.tdForm = form;
	  }
	  public String getForm (){
		return this.tdForm;
	  }

	  public void setFormat(String format){
		this.tdFormat = format;
	  }
	  public String getFormat (){
		return this.tdFormat;
	  }

	  public void setLine (String line){
		this.tdLine = line;
	  }
	  public String getLine (){
		return this.tdLine;
	  }

	  public void setMode (String mode){
		this.mode = mode;
	  }
	  public String getMode (){
		return this.mode;
	  }

}