package com.sap.isa.businessobject.activity;

/**
 * Title:        eCall Project
 * Description:  eCall projects provides multi-channel functionality for service
 *  * scenario. It supports chat, call me back, VoIP and email and integartion to
 *  * backend systems.
 * Copyright:    Copyright (c) 2001
 * Company:      SAPMarkets Inc. Palo Alto, CA
 * @version 1.0
 */
import com.sap.isa.backend.boi.isacore.activity.ActivityHeaderData;
import com.sap.isa.businessobject.BusinessObjectBase;

public class ActivityHeader extends BusinessObjectBase implements ActivityHeaderData{

   private String           guid = "";
   private String           handle = "";
   private String           processType = "";
   private String           objectId = "";
   private String           predecessorProcess = "";
   private String           predecessorObjectType = "";
   private String           predecessorLogSystem = "";
   private String           binRelationType = "";
   private String           logicalSystem = "";
   private String           descrLanguage = "";
   private String           languISO = "";
   private String           description = "";
   private String           category = "";
   private String	            priority = "";
   private String		      objective = "";
   private String			direction = "";
   private String			externActId = "";
   private String			addressId = "";
   private String             postingDate = "";
   private String             mode = "";
   private String             completion = "";

    private String BSPAppl;
    private String BSPView;

   /**
    * the default constructor
    */
    public ActivityHeader() {
    }



    /**
    * the setter and getter methods for the guid
    */
   public void setGuid (String guid) {
          this.guid = guid;
   }

   public String getGuid (){
          return this.guid;
   }

    public void setHandle (String handle){
	  this.handle = handle;
    }

    public String getHandle (){
	  return handle;
    }

    public void setProcessType (String type){
	     this.processType = type;
    }

    public String getProcessType () {
	     return processType;
    }

    public void setObjectId (String id) {
	     this.objectId = id;
    }

    public String getObjectId () {
	     return objectId;
    }

    public void setPredecessorProcess (String process) {
	     this.predecessorProcess = process;
    }

    public String  getPredecessorProcess () {
	     return this.predecessorProcess;
    }


    public void setPredecessorObjectType (String type){
	     this.predecessorObjectType = type;
    }

    public String  getPredecessorObjectType (){
	     return this.predecessorObjectType;
    }


    public void setPredecessorLogSystem (String system){
	  this.predecessorLogSystem = system;
    }

    public String  getPredecessorLogSystem (){
	     return this.predecessorLogSystem;
    }

    public void setBinRelationType (String type){
	     this.binRelationType = type;
    }

    public String getBinRelationType (){
	     return this.binRelationType;
    }

    public void setLogicalSystem (String system){
	     this.logicalSystem = system;
    }

    public String  getLogicalSystem () {
	  return this.logicalSystem;
    }

    public String getDescrLanguage (){
	     return this.descrLanguage;
    }

    public void setDescrLanguage (String lang){
	     this.descrLanguage = lang;
    }

    public String getLanguISO (){
	     return this.languISO;
    }

    public void setLanguISO (String iso){
	     this.languISO =  iso;
    }

    public String getDescription (){
	     return this.description;
    }

    public void setDescription (String desc){
	     this.description = desc;
    }

    public void setCategory (String category){
	     this.category = category;
    }

    public String getCategory (){
	     return this.category;
    }

    public String getPriority (){
	     return this.priority;
    }

    public void SetPriority (String prio){
	     this.priority = prio;
    }

    public String getObjective (){
	     return this.objective;
    }

    public void setObjective (String obj){
	     this.objective = obj;
    }

    public void setDirection (String direction){
	     this.direction = direction;
    }

    public String getDirection (){
	     return this.direction;
    }

    public String getExternActId (){
	     return this.externActId;
    }

    public void setExternActId (String id){
	     this.externActId = id;
    }

    public String getAddressId (){
	     return this.addressId;
    }

    public void setAddressId (String id){
	     this.addressId = id;
    }

    public String getPostingDate (){
	     return this.postingDate;
    }

    public void setPostingDate (String date){
	     this.postingDate= date;
    }

    public String getMode (){
	   return this.mode;
    }

    public void setMode (String mode){
	     this.mode = mode;
    }

    public String getCompletion (){
	   return this.completion;
    }

    public void setCompletion (String completion){
	     this.completion = completion;
    }

    public void setBSPAppl(String BSPAppl) {
      this.BSPAppl = BSPAppl;
    }

    public String getBSPAppl() {
      return BSPAppl;
    }

    public void setBSPView(String BSPView) {
      this.BSPView = BSPView;
    }

    public String getBSPView() {
      return BSPView;
    }
}