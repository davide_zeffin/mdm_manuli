package com.sap.isa.businessobject.activity;

/**
 * Title:        eCall Project
 * Description:  eCall projects provides multi-channel functionality for service
 *  * scenario. It supports chat, call me back, VoIP and email and integartion to
 *  * backend systems.
 * Copyright:    Copyright (c) 2001
 * Company:      SAPMarkets Inc. Palo Alto, CA
 * @version 1.0
 */
import com.sap.isa.backend.boi.isacore.activity.ActivityPartnerData;
import com.sap.isa.businessobject.BusinessObjectBase;

public class ActivityPartner extends BusinessObjectBase implements ActivityPartnerData{

    /** private attributes **/
    private String guid = "";
    private String refHandle = "";
    private String refKind = "";
    private String refPartnerHandle = "";
    private String refPartnerFct = "";
    private String refPartnerNo = "";
    private String refNoType = "";
    private String refDisplayType = "";
    private String kindofEntry = "";
    private String mainPartner = "";
    private String relationPartner = "";
    private String businessPartnerGuid = "";
    private String addressGuid = "";
    private String addressNr = "";
    private String partnerFct = "";
    private String partnerNo = "";
    private String noType = "";
    private String displayType = "";

    public ActivityPartner() {
    }

       /**
    * the setter method for the guid
    */
    public void setGuid (String guid) {
	     this.guid = guid;
    }

    public String getGuid (){
	     return this.guid;
    }

    public void setRefHandle (String handle){
	     this.refHandle = handle;
    }

    public String getRefHandle (){
	     return this.refHandle;
    }

    public void setRefKind (String kind){
	     this.refKind = kind;
    }

    public String getRefKind (){
	     return this.refKind;
    }

    public void setRefPartnerHandle (String handle){
	     this.refPartnerHandle = handle;
    }

    public String getRefPartnerHandle () {
	     return this.refPartnerHandle;
    }

    public void setRefPartnerFct (String fct){
	     this.refPartnerFct = fct;
    }

    public String getRefPartnerFct (){
	     return this.refPartnerFct;
    }

    public void setRefPartnerNo (String no){
	     this.refPartnerNo = no;
    }

    public String getRefPartnerNo (){
	     return this.refPartnerNo;
    }

    public void setRefNoType (String type){
	     this.refNoType = type;
    }

    public String getRefNoType (){
	     return this.refNoType;
    }

    public void setRefDisplayType (String type){
	     this.refDisplayType = type;
    }

    public String getRefDisplayType (){
	     return this.refDisplayType;
    }

    public void setPartnerFct (String fct){
	     this.partnerFct = fct;
    }

    public String getPartnerFct (){
	  return this.partnerFct;
    }

    public void setPartnerNo (String no){
	     this.partnerNo = no;
    }

    public String getPartnerNo (){
	     return this.partnerNo;
    }

    public void setNoType (String type){
	     this.noType = type;
    }

    public String getNoType (){
	     return this.noType;
    }

    public void setDisplayType (String type){
	     this.displayType = type;
    }

    public String getDisplayType (){
	     return this.displayType;
    }


    public void setKindofEntry (String entry){
	     this.kindofEntry = entry;
    }

    public String getKindofEntry (){
	     return this.kindofEntry;
    }

    public void setMainPartner (String partner){
	     this.mainPartner = partner;
    }

    public String getMainPartner (){
	     return this.mainPartner;
    }

    public void setRelationPartner (String partner){
	     this.relationPartner = partner;
    }

    public String getRelationPartner (){
	     return this.relationPartner;
    }

    public void setBusinessPartnerGuid (String guid){
	     this.businessPartnerGuid = guid;
    }

    public String getBusinessPartnerGuid (){
	     return this.businessPartnerGuid;
    }

    public void setAddressGuid (String guid){
	     this.addressGuid = guid;
    }

    public String getAddressGuid (){
	     return this.addressGuid;
    }

    public void setAddressNr (String nr){
	     this.addressNr = nr;
    }

    public String getAddressNr (){
	     return this.addressNr;
    }

}