/*****************************************************************************
    Class:        BusinessObjectDecoratorBase
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP
    Created:      November 2002
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/06/26 $
*****************************************************************************/

package com.sap.isa.businessobject;

import java.util.Iterator;
import java.util.Set;

import com.sap.isa.core.TechKey;
import com.sap.isa.core.businessobject.boi.ObjectBaseData;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.MessageList;
import com.sap.isa.core.util.MessageListHolder;

/**
 * Base class for all decorator classes working with the <code>BusinessObjectBase</code>
 * object. So the Decorator implements the <code>ObjectBaseData</code>
 * @see com.sap.isa.businessobject.BusinessObjectBase
 * @see com.sap.isa.core.businessobject.boi.ObjectBaseData
 *
 * @author SAP
 * @version 1.0
 */
public abstract class BusinessObjectDecoratorBase 
			extends DecoratorBase
			implements ObjectBaseData, MessageListHolder {

    /**
     * Returns the BusinessObject, which should be decorated.
     */
    abstract protected BusinessObjectBase getBob();

    // methods requierd to implement ObjectBase

    /**
     * Retrieves the key for the object.
     *
     * @return The object's key
     */
    public TechKey getTechKey() {
        return getBob().getTechKey();
    }

    /**
     * Sets the key for the document.
     *
     * @param key Key to be set
     */
    public void setTechKey(TechKey techKey) {
        getBob().setTechKey(techKey);
    }


    /**
     *
     * The method returns an iterator over all sub objects of the BusinessObject
     *
     * @return Iterator to loop over sub objects
     *
     */
    public Iterator getSubObjectIterator() {
        return getBob().getSubObjectIterator();
    }


    /**
     *
     * This method returns the handle, as an alternative key for the business object,
     * because at the creation point no techkey for the object exists. Therefore
     * maybay an alternative key is needed to identify the object in backend
     * <b>Overwrite this method if you use extension for your business object!!</b>
     *
     * return the handle of business object which is needed to identify the object
     * in the backend, if the techkey still not exists
     *
     */
     public String getHandle() {
         return getBob().getHandle();
     }


     /**
      *
      * This method creates a unique handle, as an alternative key for the business object,
      * because at the creation point no techkey for the object exists. Therefore
      * maybay the handle is needed to identify the object in backend
      *
      */
     public void createUniqueHandle() {
        getBob().createUniqueHandle();

     }


      /**
       * This method sets the handle, as an alternative key for the business object,
       * because at the creation point no techkey for the object exists. Therefore
       * maybay the handle is needed to identify the object in backend
       *
       * @param handle the handle of business object which identifies the object
       * in the backend, if the techkey still not exists
       *
       */
      public void setHandle(String handle) {
          getBob().setHandle(handle);
      }


    /**
     *
     * The method returns an reference to a sub objects for a given tech key
     *
     * @return sub object
     *
     */
    public ObjectBaseData getSubObject(TechKey techKey) {
        return getBob().getSubObject(techKey);
    }


    /**
     * This method stores arbitrary data within this Business Object
     * @param key key with which the specified value is to be associated.
     * @param value value to be associated with the specified key.
     */
    public void addExtensionData(Object key, Object name) {
        getBob().addExtensionData(key,name);
    }

    /**
    * This method retrieves extension data associated with the
    * Business Object
    * @param key key with which the specified value is to be associated.
    */
    public Object getExtensionData(Object key) {
        return getBob().getExtensionData(key);
    }

    /**
     * This method removes extension data from the Business Object
     */
    public void removeExtensionData(Object key) {
        getBob().removeExtensionData(key);
    }


    /**
     * This method retrieves all extension data associated with the
     * Business Object
     */
    public Set getExtensionDataValues() {
        return getBob().getExtensionDataValues();
    }


    /**
     * This method removes all extensions data from the Business Object
     */
    public void removeExtensionDataValues() {
      getBob().removeExtensionDataValues();
      }


    /**
     * Add a message to messagelist
     *
     * @param message message to add
     */
    public void addMessage(Message message) {
        getBob().addMessage(message);    
    }


    /**
     *
     * clear all messages in the message list
     *
     */
    public void clearMessages() {
        getBob().clearMessages();        
    }


    /**
     * Returns the messages of the Business Object.
     *
     * @return message list of Business Object
     */
    public MessageList getMessageList() {
        return getBob().getMessageList();
    }


}
