/*****************************************************************************
    Interface:    DocumentState
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP
    Created:      20.2.2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/06/26 $
*****************************************************************************/

package com.sap.isa.businessobject;


/**
 * Interface representing the ability to tell someone about its state
 *
 * @author SAP
 * @version 1.0
 */
public interface DocumentState {

    /**
     * State of the document is of no interest and none of the other
     * states
     */
    public static final int UNDEFINED = 0;

    /**
     * Document may be a target of insert operations
     */
    public static final int TARGET_DOCUMENT = 1;

    /**
     * Document may only be viewed
     */
    public static final int VIEW_DOCUMENT = 2;

    /**
     * Gets the state of the document
     *
     * @return the state as described by the constants of this interface
     */
    public int getState();

    /**
     * Sets the state of the document
     *
     * @param state the state as described by the constants of this interface
     */
    public void setState(int state);
}