/*****************************************************************************
    Class:        QuotationChange
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Thomas Smits
    Created:      11.4.2001
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/25 $
*****************************************************************************/

package com.sap.isa.businessobject.quotation;

import com.sap.isa.backend.BackendTypeConstants;
import com.sap.isa.backend.boi.isacore.order.QuotationBackend;
import com.sap.isa.businessobject.BORuntimeException;
import com.sap.isa.businessobject.BusinessObjectException;
import com.sap.isa.businessobject.BusinessObjectHelper;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.core.logging.IsaLocation;


/**
 *
 * @author Thomas Smits
 * @version 1.0
 */
public class QuotationChange extends QuotationDecoratorBase
        implements BackendAware {

    private QuotationBackend backendService;
    private BackendObjectManager bem;
	static final private IsaLocation loc = IsaLocation.getInstance(QuotationChange.class.getName());

    /**
     * Creates a new instance without parameters
     *
     */
    public QuotationChange() {
    }

    /**
     * Creates a new instance for the given <code>Quotation</code> object.
     *
     * @param order Quotation to decorate
     */
    public QuotationChange(Quotation quotation) {
        super(quotation);
    }

   /**
     * Sets the BackendObjectManager for the Quotation. This method is used
     * by the object manager to allow the user object interaction with
     * the backend logic. This method is normally not called
     * by classes other than BusinessObjectManager.
     *
     * @param bem BackendObjectManager to be used
     */
    public void setBackendObjectManager(BackendObjectManager bem) {
        this.bem = bem;
    } // setBackendObjectManager

    /**
     * get the BackendService, if necessary
    **/
    protected QuotationBackend getBackendService()
            throws BackendException{

        if (backendService == null) {

            // get the Backend from the Backend Manager
            // method not found in BackendManager
            backendService = (QuotationBackend)
                bem.createBackendBusinessObject(
                    BackendTypeConstants.BO_TYPE_QUOTATION, null);
        }
        return backendService;
    }

    /**
     * Cancel a Quotation
     */
    public void cancel(TechKey techKey) throws  BusinessObjectException,
                                                BORuntimeException {
		final String METHOD = "cancel()";
		loc.entering(METHOD);
		if (loc.isDebugEnabled())
			loc.debug("The techkey = "+techKey);
        quotation.setTechKey(techKey);

        try {
            // cancel quotation in Backend
            getBackendService().cancelInBackend(quotation);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally {
        	loc.exiting();
        }

    }
}
