/*****************************************************************************
    Class:        QuotationDecoratorBase
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Thomas Smits
    Created:      11.4.2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/06/26 $
*****************************************************************************/

package com.sap.isa.businessobject.quotation;

import com.sap.isa.businessobject.DecoratorBase;

/**
 *
 * @author Thomas Smits
 * @version 1.0
 */
public class QuotationDecoratorBase extends DecoratorBase {
     protected Quotation quotation;

    /**
     * Creates a new decorator wrapped around an <code>Quotation</code>
     * object.
     */
    public QuotationDecoratorBase(Quotation quotation) {
        this.quotation = quotation;
    }

    // DEBUG START
    public QuotationDecoratorBase() {
    }
    // DEBUG END
}
