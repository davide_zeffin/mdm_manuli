/*****************************************************************************
    Class:        QuotationCreate
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Thomas Smits
    Created:      11.4.2001
    Version:      1.0

    $Revision: #3 $
    $Date: 2002/10/01 $
*****************************************************************************/

package com.sap.isa.businessobject.quotation;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.item.ItemList;
import com.sap.isa.businessobject.order.Basket;
import com.sap.isa.businessobject.order.CampaignListEntry;
import com.sap.isa.businessobject.order.PartnerList;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.logging.IsaLocation;

/**
 *
 * @author Thomas Smits
 * @version 1.0
 */
public class QuotationCreate extends QuotationDecoratorBase {
	static final private IsaLocation loc = IsaLocation.getInstance(QuotationCreate.class.getName());

    /**
     * Creates a new instance for the given <code>Quotation</code> object.
     *
     * @param order Quotation to decorate
     */
    public QuotationCreate(Quotation quotation) {
        super(quotation);
    }

    /**
     * Copies the basket's properties to the quotation object.
     *
     * @param baskt Basket which is used as source
     */
    public void copyFromBasket(Basket basket) {

        // copy of basket properties
        setTechKey(basket.getTechKey());
        setHeader(basket.getHeader());
        setItems(basket.getItems());
    }

    /**
     *
     */
    public void setTechKey(TechKey techKey) {
        quotation.setTechKey(techKey);
    }

    /**
     *
     *
     */
    public void setHeader(HeaderSalesDocument header) {
        quotation.setHeader(header);
    }

    /**
     *
     *
     */
    public void setItems(ItemList items) {
        quotation.setItems(items);
    }

    /**
     * Create an quotation as a target document
     * @param shop The shop of the quotation.
     *
     * @deprecated This method will be replaced by <code>create(Shop shop,
     *                                                          BusinessPartnerManager bupama,
     *                                                          PartnerList partnerList,
     *                                                          WebCatInfo salesDocWebCat) </code>
     * @see #create(Shop shop,
     *              BusinessPartnerManager bupama,
     *              PartnerList partnerList,
     *              WebCatInfo salesDocWebCat)
     */
    public void create(Shop shop, WebCatInfo webCat) throws CommunicationException {
		final String METHOD = "create()";
		loc.entering(METHOD);
        quotation.init(shop, webCat);
        quotation.setState(DocumentState.TARGET_DOCUMENT);
        loc.exiting();
    }

    /**
     * Create an quotation as a target document
     *
     * @param shop Shop used for this basket
     * @param bupama the business partner manager
     * @param partnerList list of business partners for the document
     * @param salesDocWebCat the actual catalog
     */
    public void create(Shop shop, BusinessPartnerManager bupama,
                      PartnerList partnerList, WebCatInfo webCat) throws CommunicationException {
		final String METHOD = "create()";
		loc.entering(METHOD);
        quotation.init(shop, bupama, partnerList, webCat);
        quotation.setState(DocumentState.TARGET_DOCUMENT);
        loc.exiting();
    }

	/**
	 * Create an quotation as a target document
	 *
	 * @param shop Shop used for this basket
	 * @param bupama the business partner manager
	 * @param partnerList list of business partners for the document
	 * @param salesDocWebCat the actual catalog
	 * @param processType process type of the target document
	 * 
     * @deprecated This method will be replaced by <code>create(Shop shop,
     *                                                          BusinessPartnerManager bupama,
     *                                                          PartnerList partnerList,
     *                                                          WebCatInfo webCat,
     * 															String processType,
     * 															CampaignListEntry campaignListEntry)</code>
	 */
	public void create(Shop shop, BusinessPartnerManager bupama,
					  PartnerList partnerList, WebCatInfo webCat, 
					  String processType) throws CommunicationException {
		final String METHOD = "create()";
		loc.entering(METHOD);
		quotation.init(shop, bupama, partnerList, webCat, processType, null);
		quotation.setState(DocumentState.TARGET_DOCUMENT);
		loc.exiting();
	}

	/**
	 * Create an quotation as a target document
	 * 
     * @param shop Shop used for this basket
     * @param bupama the business partner manager
     * @param partnerList list of business partners for the document
     * @param webCat the actual catalog
     * @param processType type of the target document
     * @param campaignListEntry
     * @throws CommunicationException
     */
    public void create(Shop shop, BusinessPartnerManager bupama,
					  PartnerList partnerList, WebCatInfo webCat, 
					  String processType, CampaignListEntry campaignListEntry) throws CommunicationException {
		final String METHOD = "create()";
		loc.entering(METHOD);
		quotation.init(shop, bupama, partnerList, webCat, processType, campaignListEntry);
		quotation.setState(DocumentState.TARGET_DOCUMENT);
		loc.exiting();
	}
	
    /**
     * Saves the quotation in the backend.
     * Saves only lean quotations in the backend; here for compatibility
     */
    public void save() throws CommunicationException {
        quotation.save();
    }

    /**
     * Saves both lean or extended quotations in the backend; 
     * needs shop to decide
     */
    public void save(Shop shop) throws CommunicationException {
        quotation.save(shop);
    }

}
