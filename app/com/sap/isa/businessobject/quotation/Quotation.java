/*****************************************************************************
    Class:        Quotation
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Thomas Smits
    Created:      11.4.2001
    Version:      1.0

    $Revision: #2 $
    $Date: 2001/10/29 $
*****************************************************************************/

package com.sap.isa.businessobject.quotation;

import com.sap.isa.backend.BackendTypeConstants;
import com.sap.isa.backend.boi.isacore.SalesDocumentBackend;
import com.sap.isa.backend.boi.isacore.order.QuotationBackend;
import com.sap.isa.backend.boi.isacore.order.QuotationData;
import com.sap.isa.businessobject.BusinessObjectHelper;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;

/**
 * Representation of a quotation.
 *
 * @author Thomas Smits
 * @version 1.0
 */
public class Quotation extends SalesDocument
                       implements QuotationData {

	
    /**
     * Saves both lean or extended quotations in the backend;
     * needs shop to decide
     */
    public void save(Shop shop) throws CommunicationException {
		final String METHOD = "save()";
		log.entering(METHOD);
        // write some debugging info
        if (log.isDebugEnabled()) {
            log.debug("save(): shop = "+shop);
        }

        try {
            // Remove quotation status in Backend
            ((QuotationBackend) getBackendService()).saveInBackend(shop, this);
            isDirty = true;
            header.setDirty(true);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally {
        	log.exiting();
        }
    }

    /**
     * Saves only lean quotations in the backend; here for compatibility
     */
    public void save() throws CommunicationException {

        // write some debugging info
		final String METHOD = "save()";
		log.entering(METHOD);

        try {
            // Remove quotation status in Backend
            ((QuotationBackend) getBackendService()).saveInBackend(this);
            isDirty = true;
            header.setDirty(true);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally {
        	log.exiting();
        }
    }

    /**
     * Cancel a Quotation
     */
    public void cancel(TechKey techKey) throws CommunicationException {
        this.techKey = techKey;
		
		final String METHOD = "cancel()";
		log.entering(METHOD);
        // write some debugging info
        if (log.isDebugEnabled()) {
            log.debug("cancel(): techKey = " + techKey);
        }

        try {
            // cancel quotation in Backend
            isDirty = true;
            header.setDirty(true);
            ((QuotationBackend) getBackendService()).cancelInBackend(this);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally {
        	log.exiting();
        }
    }

    /**
     * Switchs a quotation to a order, meaning order a quotation
     */
    public void switchToOrder(TechKey techKey) throws CommunicationException {
		final String METHOD = "switchToOrder()";
		log.entering(METHOD);
        this.techKey = techKey;

        // write some debugging info
        if (log.isDebugEnabled()) {
            log.debug("switchToOrder(): techKey = " + techKey);
        }

        try {
            // Remove quotation status in Backend
            ((QuotationBackend) getBackendService()).switchToOrder(this);
            isDirty = true;
            header.setDirty(true);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally {
        	log.exiting();
        }
    }

    /**
     * Simulates the quotation in the backend.
     *
     */
    public void simulate() throws CommunicationException {

        // write some debugging info
		final String METHOD = "simulate()";
		log.entering(METHOD);

        // call backend here
        try {
             ((QuotationBackend) getBackendService()).simulateInBackend(this);
             isDirty = true;
             header.setDirty(true);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally {
        	log.exiting();
        }
    }

    /**
     * Read status information for a quotation an add it to its header object.
     */
    public void readHeaderStatus() throws CommunicationException {

        // write some debugging info
		final String METHOD = "readHeaderStatus()";
		log.entering(METHOD);

        try {
            // Remove quotation status in Backend
            ((QuotationBackend) getBackendService()).readHeaderStatus(this);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally {
        	log.exiting();
        }
    }

    /**
     * Method retrieving the backend object for the object.
     *
     * @return Backend object to be used
     */
    protected SalesDocumentBackend getBackendService()
            throws BackendException {

        synchronized (this) {
            if (backendService == null) {
                backendService =
                    (QuotationBackend)
                         bem.createBackendBusinessObject(BackendTypeConstants.BO_TYPE_QUOTATION, null);
            }
        }
        return backendService;
    }

}
