/*****************************************************************************
    Class:        ProductList
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP
    Created:      April 2001
    Version:      1.0

    $Revision: #4 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.businessobject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.sap.isa.backend.boi.isacore.ProductData;
import com.sap.isa.backend.boi.isacore.ProductListData;
import com.sap.isa.backend.boi.webcatalog.pricing.Prices;
import com.sap.isa.businessobject.webcatalog.pricing.PriceCalculator;
import com.sap.isa.catalog.webcatalog.WebCatArea;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.catalog.webcatalog.WebCatItemList;
import com.sap.isa.catalog.webcatalog.WebCatItemPrice;
import com.sap.isa.core.Iterable;
import com.sap.isa.core.TechKey;

/**
 *
 * The ProductList is a super class for BusinessObejcts which hold a list of
 * <code>Product</code>s or some object which are inherited from the <code>Product</code>
 * class which is used to handle a product.
 *
 * @see com.sap.isa.businessobject.Product
 *
 * @author  SAP
 * @version 1.0
 *
 */
public class ProductList
        extends BusinessObjectBase
        implements ProductListData, Iterable {

    protected List productList = new ArrayList(20);
    protected boolean isEnhanced = false;

    /**
     * Constructor to create a ProductList
     *
     */
    public ProductList() {
    }


    /**
     * Constructor to create a ProductList from a webCatItemList
     *
     */
    public ProductList(WebCatItemList itemList) {

      Iterator i =  itemList.iterator();
      Product product;

      while (i.hasNext()) {
          WebCatItem item = (WebCatItem)i.next();
          product = new Product( new TechKey(item.getProductID()),item.getProduct());
          product.setCatalogItem(item);
          productList.add(product);
      }
      isEnhanced = true;
    }


    /**
     * Constructor to create a ProductList from a webCatItemList and calculate
     * all prices if necessary
     *
     * @param priceCalculator A reference to the price calculator which will determine
     *        the prices
     *
     */
    public ProductList(WebCatItemList itemList, PriceCalculator priceCalculator) {

        this(itemList);

        determinePrices(priceCalculator);
    }


    /**
     * Returns if the productlist is enhanced
     *
     * @return <code>true</code> if the productList is enhanced
     *
     */
    public boolean isEnhanced() {
       return isEnhanced;
    }


    /**
     * Add a new product to the product list, must overwrite from objects which
     * didn't use the <code>Product</code> directly.
     *
     * @param Product which should add to the orderlist
     *
     * @see Product
     */
    public void addProduct(ProductData product) {
        if (product instanceof Product ) {
            productList.add(product);
        }
    }

    /**
     * create a product
     *
     * @param techKey  techKey of the product
     * @param id product id
     *
     */
    public ProductData createProduct(TechKey techKey,String id){
        return (ProductData) new Product(techKey, id);
    }


    /**
     * enhance all given elements with the data provided by the
     * given catalog. The product information originally stored
     * in the items is lost and replaced by the new gathered informations.
     *
     * @param catalog The catalog to be used
     * @param items   The items to be enhanced
     */
    public static void enhance(WebCatInfo catalog, ProductHolder[] items) {
		final String METHOD = "enhance()";
		log.entering(METHOD);
		try {
		
			if (catalog != null) {
			
		        int size = items.length;
		
		        if ((size == 0) || (items == null)) {
		            return;     // nothig to enhance
		        }
		
		        String[] techKeys = new String[size];
                boolean checkRegularCatalog = false;
                boolean checkRewardCatalog = false;
                boolean checkBuyPtsCatalog = false;
		
		        for (int i=0; i < size; i++) {
		            techKeys[i] = items[i].getProductId().getIdAsString();
                    
                    if (items[i].isPtsItem()) {
                        checkRewardCatalog = true;
                    }
                    else if (items[i].isBuyPtsItem()) {
                        checkBuyPtsCatalog = true;
                    }
                    else {
                        checkRegularCatalog = true;
                    }
		        }

                // read WebCatItems  
                Map regularCatalogItems = null;
                Map rewardCatalogItems = null;
                Map buyPtsCatalogItems = null;
                if (checkRegularCatalog) {
                    regularCatalogItems = catalog.getProductMap(techKeys, true, WebCatArea.AREATYPE_REGULAR_CATALOG);
                }
                if (checkRewardCatalog) {
                    rewardCatalogItems = catalog.getProductMap(techKeys, true, WebCatArea.AREATYPE_LOY_REWARD_CATALOG);
                }
                if (checkBuyPtsCatalog) {
                    buyPtsCatalogItems = catalog.getProductMap(techKeys, true, WebCatArea.AREATYPE_LOY_BUY_POINTS);
                }
		
		        for (int i=0; i < size; i++) {
		            List productList = null;
                    
                    if (items[i].isPtsItem()) {
                        productList = (List) rewardCatalogItems.get(techKeys[i]);
                    }
                    else if (items[i].isBuyPtsItem()) {
                        productList = (List) buyPtsCatalogItems.get(techKeys[i]);
                    }
                    else {
                        productList = (List) regularCatalogItems.get(techKeys[i]);
                    }
		
		            Product product = new Product(items[i].getProductId());
		
		            if (productList != null) {
		                WebCatItem webcatItem = (WebCatItem) productList.get(0);
		                product.setCatalogItem(webcatItem);
		            }
		
		            items[i].setCatalogProduct(product);
		        }
	        }
		} finally{
			log.exiting();
		}
    }
    
	/**
	 *
	 * enhance all products in the list with catalog data.
	 * if no catalog data could dound the product will be removed from the list
	 *
	 * @param catalog A reference to the catalog which will enhance the product
	 *        data
	 *
	 */
	public void enhance(WebCatInfo catalog) {
		enhance(catalog, true);
	}

    /**
     *
     * enhance all products in the list with catalog data.
     * if no catalog data could dound the product will be removed from the list
     *
     * @param catalog A reference to the catalog which will enhance the product
     *        data
     * @param withSubComponent
     *
     */
    public void enhance(WebCatInfo catalog, boolean withSubComponent) {
		final String METHOD = "enhance()";
		log.entering(METHOD);
        Map catalogItems = null;
        Product product;
		try {
		
			if (catalog != null) {
		        String[] techKeyStrings = new String[productList.size()];
		
		        if (productList.size() == 0) {
		            return;
		        }
		
		        // first get create a string array which the techKeys of the products
		        for (int i=0; i < productList.size(); i++) {
		            product = (Product)productList.get(i);
		            techKeyStrings[i] = product.getTechKey().getIdAsString();
		        }
		
				//if we want a list without sub component , remove them and sort the list -- first mainComponents and then accessories
				if ( !withSubComponent ) {
					// get the webCatItems from the catalog
					catalogItems = catalog.getProductMap(techKeyStrings, false);
				}
				else {
					// get the webCatItems from the catalog
					catalogItems = catalog.getProductMap(techKeyStrings);					
				}
			
		        // check, if for every product a catalog item exits and set it
		        WebCatItem catalogItem;
		
		        int i = 0;
		        while ( i < productList.size()) {
		
		            product = (Product)productList.get(i);
		
		            // the Hash Table contains an Array List for the product, because the
		            // technical key is not unique because a product could occur in different
		            // areas
		            List catalogItemList = (List)catalogItems.get(product.getTechKey().getIdAsString());
		
		            catalogItem = null;
		
		            if (catalogItemList != null) {
		                // we use only the first entry in the list, that is not a package component
                        int listSize = catalogItemList.size();
                        for( int j = 0; j<listSize; j++ ) {
 		                   catalogItem = (WebCatItem) catalogItemList.get(j);
		                   if (catalogItem != null) {
                               if( !catalogItem.isSubComponent()) {
                                   product.setCatalogItem(catalogItem);         
                                   break;
                               }
		                   }
                        }
                        i++;
		            }
		
		            if (catalogItem == null ) {
		                productList.remove(i);
		            }
		
		        }
	        } 
		} finally{
        	log.exiting();
		}
        isEnhanced = true;
    }

    /**
     *
     * enhance all products in the list with catalog data.
     * if no catalog data could dound the product will be removed from the list
     *
     * also it would be checked if for all items in the list are determined and
     * determine all missing in one step for a better performance
     *
     * @param catalog A reference to the catalog which will enhance the product
     *        data
     * @param priceCalculator A reference to the price calculator which will determine
     *        the prices
     *
     */
    public void enhance(WebCatInfo catalog, PriceCalculator priceCalculator) {

        enhance(catalog);

        determinePrices(priceCalculator);

    }
    
	public void enhance(WebCatInfo catalog, PriceCalculator priceCalculator, boolean withSubComponent) {

		enhance(catalog, withSubComponent);
		determinePrices(priceCalculator);

		}

    /**
     *
     * check if for all items in the list are determined and determine all missing
     * in one step for a better performance
     *
     * @param priceCalculator A reference to the price calculator which will determine
     *        the prices
     *
     */
    public void determinePrices(PriceCalculator priceCalculator) {
		final String METHOD = "determinePrices()";
		log.entering(METHOD);
        Iterator iter = iterator();

        // first of all, collect all the items without a price
        List  items = new ArrayList();

        while (iter.hasNext()) {

            WebCatItem item = (WebCatItem)((Product) iter.next()).getCatalogItem();
            // if price info has been evaluated for this item don't do it again.
            // item without price has empty price, item which has
            // not been evaluated yet has no prices at all!
            if (item.getItemPrice() == null) {
                items.add(item);
            }
            Collection subItems = item.getContractItems();
            if ((subItems != null) && (!subItems.isEmpty())) {
                Iterator subItemIterator = subItems.iterator();
                while (subItemIterator.hasNext()) {
                    WebCatItem subItem = (WebCatItem)subItemIterator.next();
                    if (subItem.getItemPrice() == null) {
                        items.add(subItem);
                    }
                }
            }
        }

        // price the items that are not already priced
        if (!items.isEmpty()) {
            if (priceCalculator != null) {
                WebCatItem[] itemArray = new WebCatItem[items.size()];
                items.toArray(itemArray);
                Prices[] prices = priceCalculator.getPrices(itemArray);

                if (prices == null || prices.length != items.size()) {
                    for (int i = 0; i < items.size(); i++) {
                        ((WebCatItem) items.get(i)).setItemPrice(new WebCatItemPrice());
                    }
                }
                else {
                    for (int i = 0; i < items.size(); i++) {
                        ((WebCatItem) items.get(i)).setItemPrice(new WebCatItemPrice(prices[i]));
                    }
                }
            }
        }
		log.exiting();
    }


    /**
     *
     * create a WebCatItemlist for all entries in the list.
     *
     *
     * @param catalog A reference to the catalog which will enhance the product
     *        data
     *
     * @return the <code>WebCatItemList</code>
     *
     */
    public WebCatItemList createWebCatItemList(WebCatInfo catalog) {
		final String METHOD = "createWebCatItemList()";
		log.entering(METHOD);
        WebCatItemList itemList = new WebCatItemList(catalog);

        Iterator i = iterator();

        while (i.hasNext()) {
            itemList.addItem(((Product)i.next()).getCatalogItem());
        }
		log.exiting();
        return itemList;
    }

    /**
     *
     * returns the object as string
     *
     * @return String which contains all products
     *
     */
    public String toString() {
        String string = "";

        for (int i=0; i < productList.size(); i++) {
            string = string + productList.get(i).toString() + "\n";
        }

        return string;
    }


    /**
     * Returns the number of products in the list
     *
     * @return count of products
     *
     */
    public int size() {
        return productList.size();
    }


    /**
     * remove the product with index <code>index</code> from the product list.
     *
     * @param index of the product to remove from product list
     *
     */
    public void remove(int index) {
        productList.remove(index);

    }


    /**
     * Returns an iterator for the products. Method necessary because of
     * the <code>Iterable</code> interface.
     *
     * @return iterator to iterate over products
     *
     */
    public Iterator iterator() {
        return productList.iterator();
    }


    /**
     * get the item with the requested itemId
     * @param itemId
     * @return prod
     */
    public Product getItem(String itemId) {
		final String METHOD = "getItem()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("ItemId ="+itemId);
        Product prod = null;

        Iterator iter = this.productList.iterator();
        while(iter.hasNext()) {
            Product temp = (Product) iter.next();
            if(temp.getItemId().equals(itemId)) {
                prod = temp;
                break;
            }
        }  // endwhile
		log.exiting();
        return prod;
    }



    /**
     * Returns a copy of this object.
     *
     * @ return Copy of this object
     */
    public Object clone() {

        try {
            ProductList newProductList = (ProductList)this.getClass().newInstance();


            for(int i=0;i<productList.size();i++) {
                // clone also the Product
                Product product = (Product)(productList.get(i));
                Product newProduct = (Product)product.clone();
                newProductList.addProduct((ProductData)newProduct);
            }

            newProductList.isEnhanced = this.isEnhanced;

            return newProductList;
        }
        catch(Exception ex) {
        	log.debug(ex.getMessage());
        }
        return null;
    }


    /**
     * Reset the enhanced flag and force so a new enhancement of the
     * productlist
     *
     */
    public void resetEnhanced() {
        isEnhanced = false;
    }

}
