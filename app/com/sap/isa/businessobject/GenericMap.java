/*****************************************************************************
    Class:        GenericMap
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Thomas Smits
    Created:      20.3.2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/06/26 $
*****************************************************************************/

package com.sap.isa.businessobject;

import java.util.HashMap;
import java.util.Iterator;

import com.sap.isa.core.Iterable;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.businessobject.ObjectBase;

/**
 * Represents a Map of <code>ObjectBase</code> objects.
 * This class can be used to maintain a collection of objects and efficiently
 * look them up using a unique key. <br>
 * This implementation is not meant to be used for all purposes, you
 * should always use type safe collection classes like <code>ItemMap</code>
 * or <code>OrderList</code>. Use this class only, if you want to store
 * business objects and the only thing you are interested in is quickly
 * retrieve them using their <code>TechKey</code>.
 * <br>
 * The internal storage is organized using a Map, so duplicates of objects
 * are not stored.
 *
 * @author Thomas Smits
 * @version 1.0
 * @see ObjectBase ObjectBase
 *
 * @stereotype collection
 */
public class GenericMap implements Iterable {
    private HashMap items;

    /**
     * Creates a new <code>GenericMap</code> object.
     */
    public GenericMap() {
        items = new HashMap();
    }

    /** @link dependency */
    /*#Item lnkItem;*/

    /**
     * Adds a new <code>ObjectBase</code> object to the Map.
     *
     * @param key  Key, uniquely identifying the item
     * @param item Item to be stored in <code>ItemMap</code>
     */
    public void put(TechKey key, ObjectBase item) {
        items.put(key, item);
    }

    /**
     * Returns the <code>ObjectBase</code> to which this map maps the specified key.
     * Returns <code>null</code> if the map contains no mapping for this key.
     * A return value of null does not necessarily indicate that the map
     * contains no mapping for the key; it's also possible that the map
     * explicitly maps the key to null. The containsKey operation may be
     * used to distinguish these two cases.
     *
     * @param key key whose associated value is to be returned.
     * @return the value to which this map maps the specified key
     *
     */
    public ObjectBase get(TechKey key) {
        return (ObjectBase) items.get(key);
    }

    /**
     * Removes all mappings from this map.
     */
    public void clear() {
        items.clear();
    }

    /**
     * Returns the number of key-value mappings in this map.
     *
     * @return the number of key-value mappings in this map.
     */
    public int size() {
        return items.size();
    }

    /**
     * Returns true if this map contains no key-value mappings.
     *
     * @return <code>true</code> if this map contains no key-value mappings.
     */
    public boolean isEmpty() {
        return items.isEmpty();
    }

    /**
     * Returns true if this map maps one or more keys to the specified value.
     *
     * @param value value whose presence in this map is to be tested.
     * @return <code>true</code> if this map maps one or more keys to the
     *          specified value.
     */
    public boolean containsValue(ObjectBase value) {
        return items.containsValue(value);
    }

    /**
     * Returns an iterator over the elements contained in the
     * <code>GenericMap</code>.
     *
     * @return Iterator for this object
     */
    public Iterator iterator() {
        return items.values().iterator();
    }

    /**
     * <p>
     * Compares the specified object with this map for equality.
     * Returns true if the given object is also a map and the
     * two maps represent the same mappings. More formally,
     * two maps <code>t1</code> and <code>t2</code> represent the same mappings
     * if <code>t1.keySet().equals(t2.keySet())</code> and for every
     * key k in <code>t1.keySet(), (t1.get(k)==null ? t2.get(k)==null :
     * t1.get(k).equals(t2.get(k)))</code> .
     * This ensures that the equals method works properly across
     * different implementations of the map interface.
     * </p>
     * <p>
     * This
     * implementation first checks if the specified object is
     * this map; if so it returns true. Then, it checks if the
     * specified object is a map whose size is identical to the
     * size of this set; if not, it it returns false. If so,
     * it iterates over this map's entrySet collection, and
     * checks that the specified map contains each mapping that
     * this map contains. If the specified map fails to contain
     * such a mapping, false is returned. If the iteration completes,
     * true is returned.
     *
     * @param o object to be compared for equality with this map
     * @return <code>true</code> if the specified object is equal to this map
     */
    public boolean equals(Object o) {
       if (o == null) {
            return false;
        }
        else if (o == this) {
                return true;
        }
        else if (!(o instanceof GenericMap)) {
            return false;
        }
        else {
            return items.equals(((GenericMap)o).items);
        }
    }

    /**
     * <p>
     * Returns the hash code value for this map. The hash code
     * of a map is defined to be the sum of the hash codes of
     * each entry in the map's entrySet() view. This ensures
     * that <code>t1.equals(t2)</code> implies that
     * <code>t1.hashCode()==t2.hashCode()</code>
     * for any two maps <code>t1</code> and <code>t2</code>, as required by
     * the general
     * contract of <code>Object.hashCode</code>. This implementation iterates
     * over <code>entrySet()</code>, calling <code>hashCode</code>
     * on each element (entry)
     * in the Collection, and adding up the results.
     * </p>
     *
     * @return the hash code value for this map
     */
    public int hashCode() {
        return items.hashCode();
    }


     /**
      * <p>
      * Returns a string representation of this map. The string
      * representation consists of a list of key-value mappings
      * in the order returned by the map's <code>entrySet</code> view's
      * iterator, enclosed in braces ("{}").
      * Adjacent mappings are separated by the
      * characters ", " (comma and space). Each
      * key-value mapping is rendered as the key
      * followed by an equals sign ("=") followed
      * by the associated value. Keys and values
      * are converted to strings as by <code>String.valueOf(Object)</code>.
      * </p>
      * <p>
      * This implementation creates an empty string buffer,
      * appends a left brace, and iterates over the map's <code>entrySet</code>
      * view, appending the string representation of each <code>map.entry</code>
      * in turn. After appending each entry except the last,
      * the string ", " is appended. Finally a right brace is
      * appended. A string is obtained from the stringbuffer,
      * and returned.
      * </p>
      *
      * @return a String representation of this map
      */
    public String toString() {
        return items.toString();
    }
}