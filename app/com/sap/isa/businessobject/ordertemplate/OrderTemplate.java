/*****************************************************************************
    Class:        OrderTemplate
    Copyright (c) 2001, SAP AG, Germany, All rights reserved.
    Author:
    Created:      11.4.2001
    Version:      1.0

    $Revision: #4 $
    $Date: 2002/08/05 $
*****************************************************************************/

package com.sap.isa.businessobject.ordertemplate;

import com.sap.isa.backend.BackendTypeConstants;
import com.sap.isa.backend.boi.isacore.SalesDocumentBackend;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.order.BasketData;
import com.sap.isa.backend.boi.isacore.order.OrderTemplateBackend;
import com.sap.isa.backend.boi.isacore.order.OrderTemplateData;
import com.sap.isa.businessobject.BusinessObjectHelper;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.item.ItemList;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;

/**
 * Class representing the orderTemplate of the internet sales scenario.
 *
 * @author SAP
 * @version 1.0
 */
public class OrderTemplate extends SalesDocument
                    implements OrderTemplateData {


    private TechKey basketId;
    private String orderNumber;
    private boolean invalid = false;

    /**
     * Returns the TechKey of the basket used for creating this orderTemplate.
     *
     * @return TechKey of Basket
     */
    public TechKey getBasketId() {
		final String METHOD = "getBasketId()";
		log.entering(METHOD);
		try {
			
	        if (invalid) {
	            try {
	                read();
	            }
	            catch (CommunicationException e) {
	                return null;
	            }
	            invalid = false;
	        } 
		} finally {
        	log.exiting();
        }
        return basketId;
    }

    /**
     * Sets the TechKey of basket used for creating this orderTemplate
     *
     * @param basketId TechKey of the basket
     */
    public void setBasketId(TechKey basketId) {
        this.basketId = basketId;
    }

    /**
     * Set global data in the backend
     *
     * @return <true> if check was successful; otherwise <code>false</code>
     */
    public void setGData(ShopData shop, WebCatInfo salesDocWebCat) throws CommunicationException {
		final String METHOD = "setGData()";
		log.entering(METHOD);
        // write some debugging info
        if (log.isDebugEnabled()) {
            log.debug("setGData(): shop = " + shop + ", salesDocWebCat = " + salesDocWebCat);
        }
		try {
		
	        if (salesDocWebCat == null && shop.isInternalCatalogAvailable()) {
	            throw( new IllegalArgumentException("Parameter salesDocWebCat can not be null!"));
	        }
	        this.salesDocWebCat = salesDocWebCat;
	
	        try {
	            ((OrderTemplateBackend) getBackendService()).setGData(this, shop);
	            isDirty = true;
	            header.setDirty(true);
	        }
	        catch (BackendException ex) {
	            BusinessObjectHelper.splitException(ex);
	        }
		} finally {
			log.exiting();
		}
    }

    /**
     * Returns the number or the ordertemplate
     *
     * @return Order number
     */
    public String getOrderNumber() {
		final String METHOD = "getOrderNumber()";
		log.entering(METHOD);
		try {
		
	        if (invalid) {
	            try {
	                read();
	            }
	            catch (CommunicationException e) {
	                return null;
	            }
	            invalid = false;
	        }
		} finally {
			log.exiting();
		}
        return orderNumber;
    }

    /**
     * Sets the order number
     *
     * @param orderNumber number to be set
     */
    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }


    /**
     * Deletes the underlying representation of this object. After a call
     * to this method the object is left in an undefined stated and
     * can not be used for further calls. Remove the business object
     * after a call to this object.
     */
    public void delete() throws CommunicationException {
		final String METHOD = "delete()";
		log.entering(METHOD);
       
        try {
            isDirty = true;
            header.setDirty(true);
            ((OrderTemplateBackend)getBackendService()).deleteFromBackend(this);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally {
        	log.exiting();
        }
    }

    /**
     * Releases the lock of the order in the backend.
     *
     * @param ordr business object to be stored
     */
    public void dequeue() throws CommunicationException {

		final String METHOD = "dequeue()";
		log.entering(METHOD);
        try {
            ((OrderTemplateBackend) getBackendService()).dequeueInBackend(this);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally {
        	log.exiting();
        }
    }

    /**
     * Saves the ordertemplate in the backend.
     *
     * @param ordr business object to be stored
     */
    public void save() throws CommunicationException {

		final String METHOD = "save()";
		log.entering(METHOD);

        try {
            ((OrderTemplateBackend)getBackendService()).saveInBackend(this);
            isDirty = true;
            header.setDirty(true);
        }
        catch (BackendException ex) {

            BusinessObjectHelper.splitException(ex);
        }
        finally {
        	log.exiting();
        }
    }


    /**
     * Initializes the OrderTemplate according to the given basket.
     *
     * @param bskt Basket used for this ordertemplate
     *
     */
     public void initOrderTemplate(BasketData bskt) throws BackendException {
        header = new HeaderSalesDocument();
        itemList = new ItemList();

/*
        try {
            // create Basket in Backend
            ((OrderTemplateBackend)getBackendService()).createInBackend(this, bskt);
        }
        catch (BackendException ex) {
            throw new BackendException(ex.toString());
        }
*/
    }

    /**
     * Reads the order data from the underlying storage for the special
     * case of a change of the order.
     */
    public void readForUpdate()
                        throws CommunicationException {

        // write some debugging info
		final String METHOD = "readForUpdate()";
		log.entering(METHOD);
        try {
            // read and lock from Backend
            if (header.isDirty()) {
                ((OrderTemplateBackend) getBackendService()).readHeaderFromBackend(this, true);
                header.setDirty(false);
            }
            getBackendService().readShipTosFromBackend(this);
            if (isDirty) {
                ((OrderTemplateBackend) getBackendService()).readAllItemsFromBackend(this, true);
                isDirty = false;
            }
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally {
        	log.exiting();
        }
    }

	/**
	  * Reads the order data from the underlying storage for the special
	  * case of a change of the order.
	  *
	  * @param shop The shop
	  */
	 public void readForUpdate(ShopData shop)
						 throws CommunicationException {

		 // write some debugging info
		 final String METHOD = "readForUpdate()";
 		log.entering(METHOD);
 		if (log.isDebugEnabled())
 			log.debug("readForUpdate: ShopData = "+shop);
		 try {
			 // read and lock from Backend
			 if (header.isDirty()) {
				if (isLargeDocument(shop.getLargeDocNoOfItemsThreshold()) && !isChangeHeaderOnly()) {
				//Large Document and no header change
					((OrderTemplateBackend) getBackendService()).readHeaderFromBackend(this, false);
				 } else {
					 ((OrderTemplateBackend) getBackendService()).readHeaderFromBackend(this, true);
				 }
			     header.setDirty(false);
		     }			 
			 getBackendService().readShipTosFromBackend(this);
			 if (isDirty && (!isHeaderChangeInLargeDocument(shop.getLargeDocNoOfItemsThreshold()) || selectedItemGuids.size() > 0)) {
				 ((OrderTemplateBackend) getBackendService()).readAllItemsFromBackend(this, !isChangeHeaderOnly());
				 isDirty = false;
			 }
		 }
		 catch (BackendException ex) {
			 BusinessObjectHelper.splitException(ex);
		 }
		 finally {
		 	log.exiting();
		 }
	 }
    /**
     * Method retrieving the backend object for the object.
     *
     * @return Backend object to be used
     */
    protected SalesDocumentBackend getBackendService()
            throws BackendException{

        synchronized (this) {
            if (backendService == null) {
                    backendService = (SalesDocumentBackend)
                            bem.createBackendBusinessObject(BackendTypeConstants.BO_TYPE_ORDERTEMPLATE, null);
            }
        }
        return backendService;
    }
    
    /**
     * Simulates the quotation in the backend.
     *
     */
    public void simulate() throws CommunicationException {

		final String METHOD = "simulate()";
		log.entering(METHOD);

        // call backend here
        try {
             ((OrderTemplateBackend) getBackendService()).simulateInBackend(this);
             isDirty = true;
             header.setDirty(true);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally {
        	log.exiting();
        }
    }
}
