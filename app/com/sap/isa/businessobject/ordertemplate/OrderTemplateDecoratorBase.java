/*****************************************************************************
    Class:        OrderTemplateDecoratorBase
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Thomas Smits
    Created:      11.4.2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/06/26 $
*****************************************************************************/

package com.sap.isa.businessobject.ordertemplate;

import com.sap.isa.businessobject.DecoratorBase;

/**
 * Base class for all decorator classes working with the
 * <code>OrderTemplate</code> object.
 *
 * @author Thomas Smits
 * @version 1.0
 */
public class OrderTemplateDecoratorBase extends DecoratorBase {
     protected OrderTemplate orderTemplate;

    /**
     * Creates a new decorator wrapped around an <code>OrderTemplate</code>
     * object.
     */
    public OrderTemplateDecoratorBase(OrderTemplate orderTemplate) {
        this.orderTemplate = orderTemplate;
    }
}
