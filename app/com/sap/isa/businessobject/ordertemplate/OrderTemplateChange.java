/*****************************************************************************
    Class:        OrderTemplateChange
    Copyright (c) 2001, SAP AG, Germany, All rights reserved.
    Author:
    Created:      11.4.2001
    Version:      1.0

    $Revision: #3 $
    $Date: 2002/08/05 $
*****************************************************************************/

package com.sap.isa.businessobject.ordertemplate;

import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.ShipTo;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.item.ItemList;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.businessobject.order.OrderChange;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.util.MessageList;
import com.sap.isa.core.util.table.ResultData;

/**
 * Decorator for the order template to provide functionality only available in
 * special situations.
 * This class acts as a decorator for the order template and offers methods
 * that can only be applied when the template is in the state of being changed.
 *
 * @author Thomas Smits
 * @version 1.0
 */
public class OrderTemplateChange extends OrderChange {

    protected OrderTemplate orderTemplate;
    
    /**
     * @see com.sap.isa.businessobject.
     * BusinessObjectDecoratorBase#getBob()
     */
    protected BusinessObjectBase getBob() {
        return orderTemplate;
    }

    /**
     * Creates a new instance for the given <code>OrderTemplate</code> object.
     *
     * @param order OrderTemplate to decorate
     */
    public OrderTemplateChange(OrderTemplate orderTemplate) {
        this.orderTemplate = orderTemplate;
    }
    
    /**
     * returns decorated object
     * 
     * @return Saledocument the decorated object
     */   
    public SalesDocument getSalesDocument() {
        return orderTemplate;
    }

    /**
     * Saves the ordertemplate in the backend.
     *
     */
    public void saveChanges() throws CommunicationException {
        orderTemplate.save();
    }

    /**
     * Clears all fields of the ordertemplate.
     */
    public void clearData() {
        orderTemplate.clearHeader();
        orderTemplate.clearItems();
    }

    /**
     * Clears the header fields of the ordertemplate.
     */
    public void clearHeader() {
        orderTemplate.clearHeader();
    }

    /**
     * Deletes the messages of the ordertemplate
     *
     */
     public void clearMessages() {
       orderTemplate.clearMessages();
     }

     /**
     * Clears the item list of the ordertemplate.
     */
    public void clearItems() {
        orderTemplate.clearItems();
    }

    /**
     * Deletes an Item(s) from the ordertemplate.
     *
     * @param techKeys Technical keys of the items to be deleted
    */
    public void deleteItems(TechKey[] techKeys) throws CommunicationException {
            orderTemplate.deleteItems(techKeys);
    }
    
    /**
     * Determines if manual Product Determination is necessary for at least one
     * top level item.
     * 
     * @return boolean true if there is at least on item with a non empty
     * prodcutAliasList          false if all productAliasLists are empty
     * 
     * @deprecated please use {@link #setDeterminationRequired(boolean isDeterminationRequired) instead 
     */
    public boolean isAlternativeProductAvailable() {
            return orderTemplate.isAlternativeProductAvailable();
    }
    
    /**
     * Determines if manual Product-, Campaign-, ... Determination is necessary 
     * for at least one top level item.
     * 
     * @return boolean true if there is at least on item that needs
     *         manual determination for products, camapigns, etc.
     */
    public boolean isDeterminationRequired(){
        return orderTemplate.isDeterminationRequired();
    }

    /**
     * Delete the ordertemplate in the backend
     *
     */
     public void deleteOrderTemplate() throws CommunicationException {
        orderTemplate.delete();
    }

    /**
     * Releases the lock of the ordertemplate in the backend.
     */
    public void dequeueOrder() throws CommunicationException {
        orderTemplate.dequeue();
    }

    /* Destroy the data of the document (items and header) in the backend
     * representation. After a
     * call to this method, the object is in an undefined state. You have
     * to call init() before you can use it again.<br>
     * This method is normally called before removing the BO-representation
     * of the object using the BOM.
     */
     public void destroyContent()  throws CommunicationException  {
       orderTemplate.destroyContent();
     }

    /**
     * Perform a precheck of ordertemplate data.
     *
     * @return <true> if check was successful; otherwise <code>false</code>
     */
    public boolean preCheck(ShopData shp) throws CommunicationException{
        // call backend for precheck here
        return orderTemplate.preCheck((Shop) shp);
    }

    /**
     * Retrieves the header information of the ordertemplate.
     *
     * @return The header of the order template
     */
    public HeaderSalesDocument getHeader() throws CommunicationException {
        return orderTemplate.getHeader();
    }

    /**
     * Returns the messages of the ordertemplate
     *
     * @return message list of the ordertemplate
     */
    public MessageList getMessageList() {
        return orderTemplate.getMessageList();
    }

    /**
     * Returns the shipto specified by the given technical key
     *
     * @param techkey Technical key of the shipto to be retrieved
     * @return Returns the found shipto or <code>null</code> if no
     *         element was found at the position
     */
    public ShipTo getShipTo(TechKey techKey) {
       return orderTemplate.getShipTo (techKey);
    }

    /**
     * Returns an array containing all ship tos currently stored in the
     * sales document.<br>
     * A shallow copy is peformed. Changing the data of a array element
     * will cause the data of the basket to be changed, too.
     *
     * @return Array containing <code>ShipTo</code> objects
     */
    public ShipTo[] getShipTos() {
        return orderTemplate.getShipTos();
    }

    /**
     * Returns the state of the order
     *
     */
     public boolean isvalid() {
       return orderTemplate.isValid();
     }

    /**
     * Returns the item of the document
     * specified by the given technical key.<br><br>
     * <b>Note -</b> This is a convenience method operating on the
     * <code>getItem</code> method of the internal <code>ItemList</code>.
     *
     * @param techKey the technical key of the item that should be
     *        retrieved
     * @return the item with the given techical key or <code>null</code>
     *         if no item for that key was found.
     */
    public ItemSalesDoc getItem(TechKey techKey) {
        return orderTemplate.getItem(techKey);
    }

    /**
     * Retrieves the header from the backend and stores the data
     * in this object.
     */
    public void getItemConfig(TechKey itemGuid) throws CommunicationException {
         orderTemplate.getItemConfig(itemGuid);
    }

     /**
     * Returns <code>ItemList</code> containing all items of the ordertemplate
     *
     * @return items of ordertemplate
     */
    public ItemList getItems() {
        return orderTemplate.getItems();
    }

    /**
     * Sets the header data
     *
     * @param header Header data to be set
     */
     public void setHeader(HeaderSalesDocument header) {
        orderTemplate.setHeader(header);
    }

    /**
     * Adds a <code>Item</code> to the ordertemplate. This item must be uniquely
     * identified by a technical key.
     * A <code>null</code> reference passed to this function will be ignored,
     * in this case nothing will happen.
     *
     * @param item Item to be added to the ordertemplate
     */
     public void addItem(ItemData item) {
        orderTemplate.addItem(item);
    }

    /**
      * Adds the IPC configuration data of an item in the
     * backend.
     *
     * @param itemGuid TechKey of the Item
     */
    public void addItemConfig(TechKey itemGuid) throws CommunicationException {
         orderTemplate.addItemConfig(itemGuid);
    }

    /**
     * Updates object representation in underlying storage.
     *
     * @deprecated This method will be replaced by <code>update(BusinessPartnerManager bupama)</code>
     * @see #update(BusinessPartnerManager bupama)
     */
    public void update() throws CommunicationException {
        BusinessPartnerManager bupama = null;
        orderTemplate.update(bupama);
    }

    /**
     * Updates object representation in underlying storage.
     */
    public void update(BusinessPartnerManager bupama) throws CommunicationException {
        orderTemplate.update(bupama);
    }

    /**
     * Updates object representation in underlying storage.
     *
     * @return <true> if update was successful; otherwise <code>false</code>
     *
     * @deprecated This method will be replaced by <code>update(BusinessPartnerManager bupama, ShopData shop)</code>
     * @see #update(BusinessPartnerManager bupama, ShopData shop)
     */
    public boolean update(ShopData shop) throws CommunicationException {
        //Dummy to be able to call the standard method
         BusinessPartnerManager bupama = null;

        orderTemplate.update(bupama, (ShopData) shop);
        return true;
    }

    /**
     * Updates object representation in underlying storage.
     *
     * @return <true> if update was successful; otherwise <code>false</code>
     */
    public void update(BusinessPartnerManager bupama, Shop shop) throws CommunicationException {
        orderTemplate.update(bupama, shop);
    }

    /**
     * Removes an Item from the ordertemplate.
     *
     * @param techKeys Technical keys of the items to be removed
     */
    public void removeItem(TechKey techKey) throws CommunicationException {
            orderTemplate.removeItem(techKey);
    }

    /**
     * Retrieves the header information of the order.
     */
     public void readForUpdate()
           throws CommunicationException {
        orderTemplate.readForUpdate();
    }
    
	/**
	 * Retrieves the whole document from the underlying storage.
	 * <b>As a side effect of this method the shiptos are also read, if
	 * the soldto is set in the partner list. This
	 * is not supposed to be the right behaviour and is left here only to keep
	 * the clients of this class working. All clients should explicitly call
	 * the <code>readShipTos()</code> method and not rely
	 * on this - we are going to change this soon.</b>
	 */
	public void read()
		   throws CommunicationException {
		orderTemplate.read();
	}    
    
	/**
	 * Retrieves the header information of the order.
	 */
	public void readForUpdate(ShopData shop)
		   throws CommunicationException {
		orderTemplate.readForUpdate(shop);
	}

    /**
     * Retrieves the items' information of the ordertemplate.
     */
    public void readAllItems() throws CommunicationException {
        orderTemplate.readAllItems();
    }

    /**
     * Retrieves the header information of the ordertemplate.
     *
     */
    public void readOrderTemplateHeader() throws CommunicationException {
        orderTemplate.readHeader();
    }

    /**
     * Retrieves a list of available shipping conditions from the
     * backend.
     *
     * @param language The language
     * @return table containing the conditions with the technical key
     *         as row key
     */
    public ResultData readShipCond(String language) throws CommunicationException {
       return orderTemplate.readShipCond(language);
    }
    
    /**
     * Get the dirty flag
     *
     * @return isDirty will the document be read from the backend when 
     *                 the a read method is called true/false
     */
    public boolean isDirty() {
        return orderTemplate.isDirty();
    }

    /**
     * Set the dirty flag
     *
     * @param isDirty if set to true, the document will be read from the backend when a read method is called
     *                if set to false, the next call to a read method won't fill the object from the backend 
     */
    public void setDirty(boolean isDirty) {
        orderTemplate.setDirty(isDirty);
    }

}
