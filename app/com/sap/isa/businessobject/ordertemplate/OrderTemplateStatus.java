/*****************************************************************************
    Class:        OrderTemplateStatus
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Thomas Smits
    Created:      11.4.2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/06/26 $
*****************************************************************************/

package com.sap.isa.businessobject.ordertemplate;

/**
 * Decorator for the order template to provide functionality only available in
 * special situations.
 * This class acts as a decorator for the order template and offers methods
 * that can only be applied when the template is in the state of being displayed.
 *
 * @author Thomas Smits
 * @version 1.0
 */
public class OrderTemplateStatus extends OrderTemplateDecoratorBase {

    /**
     * Creates a new instance for the given <code>OrderTemplate</code> object.
     *
     * @param order OrderTemplate to decorate
     */
    public OrderTemplateStatus(OrderTemplate orderTemplate) {
        super(orderTemplate);
    }
}
