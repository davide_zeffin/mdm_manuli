/*****************************************************************************
    Class:        OrderTemplateCreate
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:
    Created:      11.4.2001
    Version:      1.0

    $Revision: #5 $
    $Date: 2002/06/21 $
*****************************************************************************/

package com.sap.isa.businessobject.ordertemplate;

import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.item.ItemList;
import com.sap.isa.businessobject.order.Basket;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.MessageList;

/**
 * Decorator for the order template to provide functionality only available in
 * special situations.
 * This class acts as a decorator for the order template and offers methods
 * that can only be applied when the template is in the state of being created.
 *
 * @author Thomas Smits
 * @version 1.0
 */
public class OrderTemplateCreate extends OrderTemplateDecoratorBase {
	static final private IsaLocation loc = IsaLocation.getInstance(OrderTemplateCreate.class.getName());

    /**
     * Creates a new instance for the given <code>OrderTemplate</code> object.
     *
     * @param order OrderTemplate to decorate
     */
    public OrderTemplateCreate(OrderTemplate orderTemplate) {
        super(orderTemplate);
    }
    
    /**
     * Returns if the business object is valid
     *
     * @return valid
     *
     */
    public boolean isValid() {
        return orderTemplate.isValid();
    }
    
	/**
     * Returns the messages of the Business Object.
     *
     * @return message list of Business Object
     */
	public MessageList getMessageList() {
		return orderTemplate.getMessageList();
	}

    /**
     * Copies the basket's properties to the orderTemplate object
     *
     * @param bskt Basket which is used as source
     */
    public void copyFromBasket(Basket bskt) {
		final String METHOD = "copyFromBasket()";
		loc.entering(METHOD);
		  // copy of basket properties
		  setTechKey(bskt.getTechKey());
		  setBasketId(bskt.getTechKey());
		  setHeader(bskt.getHeader());
		  setItems(bskt.getItems());
		 loc.exiting();
    }
    
    

    /**
     * Creates an ordertemplate from an external basket.
     *
     * @param documentKey <code>TechKey</code> of an external basket
     * @param soldtoKey <code>TechKey</code> of the sold to for whom the order
     * shall be created
     * @param shop the shop the action takes place in
     */
    public void createFromExternalBasket(
            SalesDocument posd,
            Shop shop,
            BusinessPartnerManager bupama,
            WebCatInfo webCat) throws CommunicationException {
            	
		final String METHOD = "createFromExternalBasket()";
		loc.entering(METHOD);
        // create the order on the backend
        orderTemplate.init(posd, shop, bupama, webCat );
        loc.exiting();
   }

    /**
     * Creates an ordertemplate from an external basket.
     *
     * @param documentKey <code>TechKey</code> of an external basket
     * @param soldtoKey <code>TechKey</code> of the sold to for whom the order
     * shall be created
     * @param shop the shop the action takes place in
     *
     * @deprecated This method will be replaced by <code>createFromExternalBasket(SalesDocument posd,
     *                                                        Shop shop,
     *                                                        BusinessPartnerManager bupama,
     *                                                        WebCatInfo salesDocWebCat) </code>
     *
     * @see #createFromExternalBasket(SalesDocument posd,
     *            Shop shop,
     *            BusinessPartnerManager bupama,
     *            WebCatInfo salesDocWebCat)
     */
    public void createFromExternalBasket(
            SalesDocument posd,
            Shop shop,
            WebCatInfo webCat) throws CommunicationException {
		final String METHOD = "createFromExternalBasket()";
		loc.entering(METHOD);
        BusinessPartnerManager bupama = null;

        // create the order on the backend
        createFromExternalBasket(
            posd,
            shop,
            bupama,
            webCat );
        loc.exiting();
   }


    /**
     * Saves the ordertemplate in the backend.
     *
     */
    public void saveOrderTemplateCreate() throws CommunicationException {
        orderTemplate.save();
    }


    /**
     * Retrieves the items' information of the ordertemplate.
     */
    public void readAllItems() throws CommunicationException {
        orderTemplate.readAllItems();
    }

    /**
     * Retrieves the header information of the ordertemplate.
     *
     */
    public void readOrderTemplateHeader() throws CommunicationException {
        orderTemplate.readHeader();
    }

    /**
     * Retrieves the header information of the ordertemplate.
     *
     */
    public HeaderSalesDocument getHeader() {
        return orderTemplate.getHeader();
    }


    /**
     *
     */
    public ItemList getItems() {
        return orderTemplate.getItems();
    }

    /**
     *
     */
     public void setBasketId (TechKey techKey) {
        orderTemplate.setBasketId(techKey);
    }

    /**
     *
     */
     public void setTechKey (TechKey techKey) {
        orderTemplate.setTechKey(techKey);
    }

    /**
     *
     */
     public void setHeader(HeaderSalesDocument header) {
        orderTemplate.setHeader(header);
    }

    public void setItems(ItemList items) {
        orderTemplate.setItems(items);
    }

    /**
     *
     */
    public String getDescription() {
        return orderTemplate.getHeader().getDescription();
    }

    /**
     *
     */
    public String getFreightValue() {
        return orderTemplate.getHeader().getFreightValue();
    }

    /**
     *
     */
    public String getNetValue() {
        return orderTemplate.getHeader().getNetValue();
    }

    /**
     *
     */
    public String getSalesDocNumber() {
        return orderTemplate.getHeader().getSalesDocNumber();
    }

    /**
     *
     */
    public String getGrossValue() {
        return orderTemplate.getHeader().getGrossValue();
    }

    /**
     *
     */
    public String getTaxValue() {
        return orderTemplate.getHeader().getTaxValue();
    }

    /**
     * Updates object representation in underlying storage.
     *
     * @return <true> if update was successful; otherwise <code>false</code>
     *
     * @deprecated This method will be replaced by <code>upadte(BusinessPartnerManager bupama)</code>
     * @see #upadte(BusinessPartnerManager bupama)
     */
    public boolean update() throws CommunicationException {
        BusinessPartnerManager bupama = null;
        return update(bupama);
    }

    /**
     * Updates object representation in underlying storage.
     *
     * @return <true> if update was successful; otherwise <code>false</code>
     */
    public boolean update(BusinessPartnerManager bupama) throws CommunicationException {
        orderTemplate.update(bupama);
        return true;
    }

    /**
     * Updates object representation in underlying storage.
     *
     * @return <true> if update was successful; otherwise <code>false</code>
     *
     * @deprecated This method will be replaced by <code>update(BusinessPartnerManager bupama, ShopData shop)</code>
     * @see #update(BusinessPartnerManager bupama, ShopData shop)
     */
    public boolean update(ShopData shop) throws CommunicationException {
        BusinessPartnerManager bupama = null;
        return update(bupama,  shop);
    }

    /**
     * Updates object representation in underlying storage.
     *
     * @return <true> if update was successful; otherwise <code>false</code>
     */
    public boolean update(BusinessPartnerManager bupama, ShopData shop) throws CommunicationException {
        orderTemplate.update(bupama, (ShopData) shop);
        return true;
    }
    
    /**
     * Get the dirty flag
     *
     * @return isDirty will the document be read from the backend when 
     *                 the a read method is called true/false
     */
    public boolean isDirty() {
        return orderTemplate.isDirty();
    }

    /**
     * Set the dirty flag
     *
     * @param isDirty if set to true, the document will be read from the backend when a read method is called
     *                if set to false, the next call to a read method won't fill the object from the backend 
     */
    public void setDirty(boolean isDirty) {
        orderTemplate.setDirty(isDirty);
    }

}
