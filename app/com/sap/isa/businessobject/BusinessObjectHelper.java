
/*****************************************************************************
    Class:        BusinessObjectHelper
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      M�rz 2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/06/26 $
*****************************************************************************/

package com.sap.isa.businessobject;

import com.sap.isa.core.eai.BackendCommunicationException;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendLogonException;
import com.sap.isa.core.eai.BackendServerStartupException;
import com.sap.isa.core.eai.BackendSystemFailureException;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.logging.LogUtil;

public abstract class BusinessObjectHelper  {

    protected static IsaLocation log = IsaLocation.getInstance(BusinessObjectHelper.class.getName());

    /**
     * Splits the general purpose Exception <code>JCO.Exception</code> used
     * by JCo into some mor meaningful and JCo independant exceptions.
     *
     * @param jcoEx The JCo exception to be splitted
     */
    public static void splitException(BackendException ex)
                            throws CommunicationException,
                                   LogonException,
                                   SystemFailureException,
                                   ServerStartupException,
                                   BORuntimeException{

        log.debug(ex);

        if (ex instanceof BackendLogonException) {
        	String messageKey = "exception.communication";
        	log.error(LogUtil.APPS_COMMON_SECURITY, messageKey, null, ex);
        	LogonException wrapperEx = new LogonException(ex.getMessage(),
											new Message(Message.ERROR, messageKey, null, null));
        	log.throwing(messageKey, wrapperEx);
          	throw wrapperEx;
        }
        else if (ex instanceof BackendSystemFailureException) {
			String messageKey = "exception.communication";
			log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, messageKey, null, ex);
			SystemFailureException wrapperEx = new SystemFailureException(ex.getMessage(),
			new Message(Message.ERROR,messageKey,null,null));
 			log.throwing(messageKey, wrapperEx);
 			throw wrapperEx;
        }
        else if (ex instanceof BackendServerStartupException) {
			String messageKey = "exception.communication";
			log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, messageKey, null, ex);
			ServerStartupException wrapperEx = new ServerStartupException(ex.getMessage(),
			new Message(Message.ERROR,messageKey,null,null));
			log.throwing(messageKey, wrapperEx);
			throw wrapperEx;
        }
        else if (ex instanceof BackendCommunicationException ) {
			String messageKey = "exception.communication";
			log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, messageKey, null, ex);
			CommunicationException wrapperEx = new CommunicationException(ex.getMessage(),
			new Message(Message.ERROR,messageKey,null,null));
			log.throwing(messageKey, wrapperEx);
			throw wrapperEx;
        }
        else {
			String messageKey = "exception.communication";
			log.error(LogUtil.APPS_BUSINESS_LOGIC, messageKey, null, ex);
			BORuntimeException wrapperEx = new BORuntimeException(ex.getMessage());
			log.throwing(messageKey, wrapperEx);
			throw wrapperEx;
        }

    }

}
