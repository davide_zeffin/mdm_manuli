/*****************************************************************************
    Class         CreateDocumentEvent
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Action add a product to the basket
    Author:       Wolfgang Sattler
    Created:      May 2001
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/10/29 $
*****************************************************************************/
package com.sap.isa.businessobject.businessevent;

import com.sap.isa.businessobject.SalesDocumentBase;
import com.sap.isa.core.businessobject.event.BusinessEvent;


/**
 * Event fired, if a document is created.
 * The document is store in the event source.
 *
 */
public class CreateDocumentEvent extends BusinessEvent {

    /**
     * constructor
     */
    public CreateDocumentEvent(SalesDocumentBase document) {
        super(document);
    }

}
