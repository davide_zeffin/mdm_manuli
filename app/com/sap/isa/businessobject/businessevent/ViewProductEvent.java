/*****************************************************************************
    Class         ViewCategoryEvent
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      May 2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/06/26 $
*****************************************************************************/
package com.sap.isa.businessobject.businessevent;

import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.core.businessobject.event.BusinessEvent;


/**
 * Event fired, if the user view a product in catalog
 * The catalog is store in the event source, the product is a property of the event
 *
 */
public class ViewProductEvent extends BusinessEvent {

    private WebCatItem item;

    /**
     * constructor
     */
    public ViewProductEvent(WebCatInfo catalog, WebCatItem item) {
        super(catalog);
        this.item = item;
    }


    /**
     * Returns the property item
     *
     * @return item
     *
     */
    public WebCatItem getItem() {
       return this.item;
    }

}
