/*****************************************************************************
    Class         SearchEvent
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      June 2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/06/26 $
*****************************************************************************/
package com.sap.isa.businessobject.businessevent;

import com.sap.isa.businessobject.IsaQuickSearch;
import com.sap.isa.core.businessobject.event.BusinessEvent;


/**
 * Event fired, if the user use the quicksearch
 * The quicksearch object is store in the event source.
 *
 */
public class SearchEvent extends BusinessEvent {

    /**
     * constructor
     */
    public SearchEvent(IsaQuickSearch search) {
        super(search);
    }

}
