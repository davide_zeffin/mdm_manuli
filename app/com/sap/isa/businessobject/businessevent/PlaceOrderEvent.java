/*****************************************************************************
    Class         PlaceOrderEvent
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      May 2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/06/26 $
*****************************************************************************/
package com.sap.isa.businessobject.businessevent;

import com.sap.isa.businessobject.User;
import com.sap.isa.businessobject.order.Order;
import com.sap.isa.core.businessobject.event.BusinessEvent;


/**
 * Event fired, if the user place an order.
 * The order is store in the event source. The user is property of the event
 *
 */
public class PlaceOrderEvent extends BusinessEvent {

    private User user;

    /**
     * constructor
     */
    public PlaceOrderEvent(Order order, User user) {
        super(order);
        this.user = user;
    }


    /**
     * Returns the property user
     *
     * @return user
     *
     */
    public User getUser() {
       return this.user;
    }


}
