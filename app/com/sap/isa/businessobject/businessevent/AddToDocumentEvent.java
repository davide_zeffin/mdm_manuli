/*****************************************************************************
    Class         AddToDocumentEvent
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Action add a product to the basket
    Author:       Wolfgang Sattler
    Created:      May 2001
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/10/29 $
*****************************************************************************/
package com.sap.isa.businessobject.businessevent;

import com.sap.isa.businessobject.SalesDocumentBase;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.core.businessobject.event.BusinessEvent;

/**
 * Event fired, if a item is add to the document.
 * The document is store in the event source, the item is a property of the event
 *
 */
public class AddToDocumentEvent extends BusinessEvent {

    private ItemSalesDoc item;

    /**
     * constructor
     */
    public AddToDocumentEvent(SalesDocumentBase document, ItemSalesDoc item ) {
        super(document);
        this.item = item;
    }


    /**
     * Returns the property item
     *
     * @return item
     *
     */
    public ItemSalesDoc getItem() {
       return this.item;
    }



}
