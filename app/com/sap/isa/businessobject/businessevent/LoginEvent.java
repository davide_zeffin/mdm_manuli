/*****************************************************************************
    Class         LoginEvent
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      May 2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/06/26 $
*****************************************************************************/
package com.sap.isa.businessobject.businessevent;

import com.sap.isa.businessobject.User;
import com.sap.isa.core.businessobject.event.BusinessEvent;

/**
 * Fired an event, if the user login to internet sales scenario.
 *
 */
public class LoginEvent extends BusinessEvent {

    /**
     * constructor
     */
    public LoginEvent(User user) {
        super(user);
    }


}
