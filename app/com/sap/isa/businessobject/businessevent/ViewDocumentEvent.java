/*****************************************************************************
    Class         ViewDocumentEvent
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      June 2001
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/10/29 $
*****************************************************************************/
package com.sap.isa.businessobject.businessevent;

import com.sap.isa.businessobject.SalesDocumentBase;
import com.sap.isa.core.businessobject.event.BusinessEvent;


/**
 * Event fired, if a document isviewed.
 * The document is store in the event source.
 *
 */
public class ViewDocumentEvent extends BusinessEvent {

    /**
     * constructor
     */
    public ViewDocumentEvent(SalesDocumentBase document) {
        super(document);
    }

}
