/*****************************************************************************
    Class         BusinessEventHandler
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      May 2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/06/26 $
*****************************************************************************/
package com.sap.isa.businessobject.businessevent;

import com.sap.isa.core.businessobject.event.BusinessEventHandlerBase;

/**
 * This interface must implement by a classes which will handle events.
 * It will used by all objects which implement the <code>BusinessEventSource</code>
 * interface to fire <code>BusinessEvent</code>s
 *
 * @see BusinessEvent BusinessEventSource
 *
 */

public interface BusinessEventHandler extends BusinessEventHandlerBase {


    /**
     * Fired a login event
     *
     */
    public void fireLoginEvent(LoginEvent event);


    /**
     * Fired a register event
     *
     */
    public void fireRegisterEvent(RegisterEvent event);


    /**
     * Fire a Search event
     *
     */
    public void fireSearchEvent(SearchEvent event);


    /**
     * Fire a CreateDocument event
     *
     */
    public void fireCreateDocumentEvent(CreateDocumentEvent event);


    /**
     * Fire a ViewDocument event
     *
     */
    public void fireViewDocumentEvent(ViewDocumentEvent event);


    /**
     * Fired an AddToDocument event
     *
     */
    public void fireAddToDocumentEvent(AddToDocumentEvent event);


    /**
     * Fired an ModifyDocumentItem event
     *
     */
    public void fireModifyDocumentItemEvent(ModifyDocumentItemEvent event);


    /**
     * Fire a DropFromDocument event
     *
     */
    public void fireDropFromDocumentEvent(DropFromDocumentEvent event);


    /**
     * Fire a DropDocument event
     *
     */
    public void fireDropDocumentEvent(DropDocumentEvent event);


    /**
     * Fire a PlaceOrder event
     *
     */
    public void firePlaceOrderEvent(PlaceOrderEvent event);


    /**
     * Fire a ViewCategory event
     *
     */
    public void fireViewCategoryEvent(ViewCategoryEvent event);


    /**
     * Fire a ViewProduct event
     *
     */
    public void fireViewProductEvent(ViewProductEvent event);


}
