/*****************************************************************************
    Class         RegisterEvent
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Action add a product to the basket
    Author:       Wolfgang Sattler
    Created:      May 2001
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/05 $
*****************************************************************************/
package com.sap.isa.businessobject.businessevent;

import com.sap.isa.businessobject.Address;
import com.sap.isa.businessobject.User;
import com.sap.isa.core.businessobject.event.BusinessEvent;

/**
 * Fired if a user register in internet sales scenario
 *
 */
public class RegisterEvent extends BusinessEvent {

    private Address address;

    /**
     * constructor
     */
    public RegisterEvent(User user, Address address) {
        super(user);
        this.address = address;
    }


    /**
     * Returns the property address
     *
     * @return address
     *
     */
    public Address getAddress() {
       return address;
    }

}
