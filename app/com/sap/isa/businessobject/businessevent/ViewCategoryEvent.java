/*****************************************************************************
    Class         ViewCategoryEvent
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Action add a product to the basket
    Author:       Wolfgang Sattler
    Created:      May 2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/06/26 $
*****************************************************************************/
package com.sap.isa.businessobject.businessevent;

import com.sap.isa.catalog.webcatalog.WebCatArea;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.businessobject.event.BusinessEvent;


/**
 * Event fired, if the user view a category
 * The catalog is store in the event source, the category is a property of the event
 *
 */
public class ViewCategoryEvent extends BusinessEvent {

    private WebCatArea area;

    /**
     * constructor
     */
    public ViewCategoryEvent(WebCatInfo catalog, WebCatArea area) {
        super(catalog);
        this.area = area;
    }


    /**
     * Returns the property area
     *
     * @return area
     *
     */
    public WebCatArea getArea() {
       return this.area;
    }

}
