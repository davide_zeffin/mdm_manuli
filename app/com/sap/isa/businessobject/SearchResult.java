/*****************************************************************************
    Class:        SearchResult
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP
    Created:      29.3.2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/06/26 $
*****************************************************************************/

package com.sap.isa.businessobject;

import java.util.Iterator;

import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.MessageList;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.core.util.table.Table;


/**
 * Represents the result of a search operation performed against
 * the underlying data storage. <br> 
 * The data is stored in a {@link com.sap.isa.core.util.table.ResultData} 
 * object. 
 *
 * @see com.sap.isa.core.util.table.Table
 * @see com.sap.isa.core.util.table.ResultData
 * 
 * @author SAP
 * @version 1.0
 */
public class SearchResult extends BusinessObjectBase {

    private Table tbl;


	/**
	 * Constructor for the search results. <br>
	 * 
     * @param tbl table with the search results 
     * @param messageList messages
     */
    public SearchResult(Table tbl, MessageList messageList) {
		this.tbl = tbl;
        setMessages(messageList);
	}


    /**
     * Returns the Number of Hits. <br>
     * 
     * @return number of Hits
     */
    public int getNumberOfHits() {
		this.getMessageList();
        return tbl.getNumRows();
    }


    /**
     * Set the messages which occurs while the search. <br>
     * 
     * @param messageList list with message
     */
    public void setMessageList(MessageList messageList) {
		setMessages(messageList);
    }


	/**
	 * Returns the search results in a 
	 * {@link com.sap.isa.core.util.table.Table}
	 * object. 
	 * 
	 * @return table with the search results
	 */
    public Table getTable() {
        return tbl;
    }

	/**
	 * Returns the search results in a 
	 * {@link com.sap.isa.core.util.table.ResultData}
	 * object. 
	 * 
	 * @return result data with the search results
	 */
    public ResultData getResultData() {
        return new ResultData(tbl);
    }


	// private method to add the messages
	private void setMessages(MessageList messageList) {
		if (messageList != null) {
			Iterator iter = messageList.iterator();
			while (iter.hasNext()) {
				this.addMessage((Message) iter.next());
			}
		}
	}

}