/*****************************************************************************
    Class:        ShipCond
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Created:      10.5.2001
    Version:      1.0

    $Revision: #2 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.businessobject;

import com.sap.isa.backend.boi.isacore.ShipCondData;
import com.sap.isa.core.businessobject.ObjectBase;

/**
 * Class representing the shipping condition for a sales document.
 *
 * @version 1.0
 */
public class ShipCond extends ObjectBase implements ShipCondData {

    private String description;

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}