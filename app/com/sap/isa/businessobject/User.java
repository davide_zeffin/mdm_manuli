/*****************************************************************************
    Class:        User
    Copyright (c) 2001, SAP Germany, All rights reserved.
    Author:       Andreas Jessen
    Created:      19.3.2001
    Version:      1.0

    $Revision: #3 $
    $Date: 2005/04/12 $
*****************************************************************************/

package com.sap.isa.businessobject;

import com.sap.isa.backend.BackendTypeConstants;
import com.sap.isa.backend.boi.isacore.UserBackend;
import com.sap.isa.backend.boi.isacore.UserData;
import com.sap.isa.backend.boi.isacore.marketing.MarketingConfiguration;
import com.sap.isa.backend.boi.isacore.marketing.MarketingUser;
import com.sap.isa.backend.boi.isacore.marketing.MktProfileBackend;
import com.sap.isa.backend.boi.isacore.marketing.MktRecommendationBackend;
import com.sap.isa.backend.boi.webcatalog.CatalogUser;
import com.sap.isa.businessobject.marketing.MktPartner;
import com.sap.isa.businessobject.marketing.MktProfile;
import com.sap.isa.businessobject.marketing.PersonalizedRecommendation;
import com.sap.ecommerce.businessobject.loyalty.LoyaltyMembership;
import com.sap.ecommerce.boi.loyalty.LoyaltyUserBackend;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.user.businessobject.IsaUserBase;
import com.sap.isa.user.util.LoginStatus;
import com.sap.isa.user.util.RegisterStatus;


/**
 * Representation of the user of the system.
 *
 * @author Andreas Jessen
 * @version 1.0
 */
public class User extends IsaUserBase implements BackendAware, UserData, MarketingUser, CatalogUser {

    // attributes
    // private UserBackend backendService;

    // used to support multiple invocations
    private RegisterStatus registerStatus;

	// private variable to store the campagin object type
	private String campaginObjectType = "";

    /* private variables for marketing functions */
    private MktProfileBackend profileBackendService;
    private MktProfile profile;
	private MktProfile newsletterProfile;

    private MktPartner mktPartner = new MktPartner();    
    private MktRecommendationBackend recommendationBackendService;
    private PersonalizedRecommendation  recommendations;
	static final private IsaLocation loc = IsaLocation.getInstance(User.class.getName());


    /**
     *  just a simple constructor
     */
    public User() {
        super();
    }


    /**
     * with this constructor the userId and
     * the password are set in the user object
     *
     * @param userid of the internet user
     * @param password of the internet user
     *
     */
    public User(String userId, String password) {
        super(userId, password);
    }

    /**
     * with this constructor the userId,
     * the password and the language
     * are set in the user object
     *
     * @param userid of the internet user
     * @param password of the internet user
     * @param language of the internet user
     *
     */
    public User(String userId, String password, String language) {
        super(userId, password, language);
    }


    /**
     * get the reference of the user backend object
     *
     */
    private UserBackend getBackendService ()
    throws BackendException {

        return (UserBackend)createBackendService();

    }


    /**
     * login of the internet user into
     * the b2b or b2c szenario
     *
     * @return integer value that indicates if
     *         the login was sucessful
     *
     */
    public LoginStatus login() throws CommunicationException {
		final String METHOD = "login()";
		loc.entering(METHOD);
        LoginStatus status = super.login();

        if (status == LoginStatus.OK || status == LoginStatus.NOT_OK_PASSWORD_EXPIRED) {
            // reset recommendation for b2c
            resetMarketingSettings();
        }
        loc.exiting();
        return status;
    }

    /**
     * login of the internet user into
     * the b2b or b2c szenario
     *
     * @param id of the internet user
     * @param password of the internet user
     *
     * @return integer value that indicates if
     *         the login was sucessful
     */
    public LoginStatus login(String userId, String password) throws CommunicationException {
		final String METHOD = "login()";
		loc.entering(METHOD);
		if (loc.isDebugEnabled())
			loc.debug("userId="+userId);
        LoginStatus status = super.login(userId, password);

        if (status == LoginStatus.OK || status == LoginStatus.NOT_OK_PASSWORD_EXPIRED) {
            // reset recommendation for b2c
            resetMarketingSettings();
        }
        loc.exiting();
        return status;
    }


    /**
     * login of the internet user into
     * the b2b szenario after a new
     * password is entered
     *
     * @param password of the internet user
     *
     */
    public LoginStatus createNewInternetUser(String password)
            throws CommunicationException {

		final String METHOD = "createNewInternetUser()";
		loc.entering(METHOD);
        
        LoginStatus loginStatus = super.createNewInternetUser(password);
        if (loginStatus == LoginStatus.OK) {
            resetMarketingSettings();
        }
      	loc.exiting();
        return loginStatus;
    }

    /**
     * register a customer with the
     * data of the parameter address
     *
     * as a side effect the salutation values are set
     * via the register method
     *
     */
    public RegisterStatus register(String shopId,
                                       Address address,
                                       String password,
                                       String password_verify)
            throws CommunicationException {

		final String METHOD = "register()";
		loc.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("shopId="+shopId+", address="+address);
        try {
            registerStatus = getBackendService().register(this, shopId, address, password, password_verify);
            loginSuccessful = (registerStatus == RegisterStatus.OK) ? true : false;

            if (loginSuccessful) {
                resetMarketingSettings();
                
            }    

            return registerStatus;
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
		finally {
			loc.exiting();
		}
        return RegisterStatus.NOT_OK;
    }



    /**
     * change customer data
     *
     * as a side effect the values for the salutation
     * are set via the changeCustomer method
     *
     */
    public RegisterStatus changeCustomer(String shopId, Address address)
        throws CommunicationException {
		final String METHOD = "changeCustomer()";
		loc.entering(METHOD);
		if (loc.isDebugEnabled())
			loc.debug("shopId="+shopId+ ", address="+address);
        try {
            registerStatus = getBackendService().changeCustomer(this, shopId, address);
			
			/* The loginSuccessful field will not be set here because the 
			 * changeCustomer method returns "false" as well if some invalid 
			 * values were entered - e.g. "Please enter City", "Email is invalid" ...
			 * The user should be logged in in this case further on.
			 */
            // loginSuccessful = (registerStatus == RegisterStatus.OK) ? true : false;

            return registerStatus;
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
		finally {
			loc.exiting();
		}
        return RegisterStatus.NOT_OK;
    }


    /**
     * returns a list of catalogs
     * each row of the list contains the catalog id, the catalog description,
     * the variant id, the variant description and a concatenation of all
     * views of a catalog
     *
     * @param the id of the webshop
     * @return a list of catalogs (s.a.)
     */
    public ResultData getCatalogs(String shopId, TechKey soldToTechKey, TechKey contactTechKey)
            throws CommunicationException {
		final String METHOD = "getCatalogs()";
		loc.entering(METHOD);
		if (loc.isDebugEnabled())
			loc.debug("shopId="+shopId);
        try {
            return getBackendService().getCatalogList(this, shopId, soldToTechKey, contactTechKey);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
            return null; //that's for the compiler, never reached
        }
		finally {
			loc.exiting();
		}
    }


    /**
     * returns if the user login or registration
     * process was sucessful
     *
     */
    public boolean isUserLogged() {
        return loginSuccessful;
    }

    /**
     * read the campaign key in backend for a given mail idenifier
     *
     * @param identifier from email campaign
     */
    public String getCampaignKeyFromMailIdentifier(String mailIdentifier)
    		throws CommunicationException {
		final String METHOD = "getCampaignKeyFromMailIdentifier()";
		loc.entering(METHOD);
		if (loc.isDebugEnabled())
			loc.debug("mailIdentifier="+mailIdentifier);
        try {
            return getBackendService().getCampaignKeyFromMailIdentifier(this, mailIdentifier);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
		finally {
			loc.exiting();
		}
        return "";
    }
    
	/**
	 * read the campaign key in backend for a given mail idenifier
	 *
	 * @param mailIdentifier identifier from email campaign
	 * @param mailIdentifier identifier for the url
	 */
	public String getCampaignKeyFromMailIdentifier(String mailIdentifier, String urlKey)
			throws CommunicationException {
		final String METHOD = "getCampaignKeyFromMailIdentifier()";
		loc.entering(METHOD);
		if (loc.isDebugEnabled())
			loc.debug("mailIdentifier="+mailIdentifier);
		try {
			return getBackendService().getCampaignKeyFromMailIdentifier(this, mailIdentifier,urlKey);
		}
		catch (BackendException ex) {
			BusinessObjectHelper.splitException(ex);
		}
		finally {
			loc.exiting();
		}
		return "";
	}
    

	/**
	 * 
	 */
	public boolean isLoyaltyCampaign(String loyaltyCampaignKey)
	       throws CommunicationException {
		final String METHOD = "isLoyaltyCampaign()";
		loc.entering(METHOD);
		
		if (loc.isDebugEnabled())
			loc.debug("loyaltyCampaign="+loyaltyCampaignKey);
		
		try {
			return getLoyaltyUserBackendService().isLoyaltyCampaign(this, loyaltyCampaignKey);
		}
		catch (BackendException ex) {
			BusinessObjectHelper.splitException(ex);
		}
		finally {
			loc.exiting();
		}
		return false;
	
	}
    
    /**
     * Returns the campaginObjectType.
     * @return String
     */
    public String getCampaginObjectType() {
        return campaginObjectType;
    }

    /**
     * Sets the campaginObjectType.
     * @param campaginObjectType The campaginObjectType to set
     */
    public void setCampaginObjectType(String campaginObjectType) {
        this.campaginObjectType = campaginObjectType;
    }

	/* marketing functions */
	/**************************************************************************/


    /**
     * Returns the marketing partner object, which give you information over the
     * the buisness partner to use within marketing functions.
     * 
     * @return MktPartner
     */
    public MktPartner getMktPartner() {
        return mktPartner;
    }


    /**
     * Return Set the the BackendService for marketing functionality. <br>
     * 
     * @return {@link MktProfileBackend}
     * @throws BackendException
     */
    protected MktProfileBackend getProfileBackendService()
            throws BackendException {

        if (profileBackendService == null) {
            // get the Backend from the Backend Manager
            profileBackendService = (MktProfileBackend)
                                    bem.createBackendBusinessObject(BackendTypeConstants.BO_TYPE_MKT_PROFILE, BusinessObjectManager.OBJECT_FACTORY);
        }
        return profileBackendService;
    }


	/**
	 * Return Set the the BackendService for marketing functionality. <br>
	 * 
	 * @return {@link MktRecommendationBackend}
	 * @throws BackendException
	 */
    protected MktRecommendationBackend getRecommendationBackendService()
            throws BackendException {

        if (recommendationBackendService == null) {
            // get the Backend from the Backend Manager
            recommendationBackendService = (MktRecommendationBackend)
                                           bem.createBackendBusinessObject(BackendTypeConstants.BO_TYPE_MKT_RECOMMENDATION, BusinessObjectManager.OBJECT_FACTORY);
        }
        return recommendationBackendService;
    }

    /**
     * set the reference to a profile
     *
     * @param profile
     */
    public void setMktProfile(MktProfile profile){
        this.profile = profile;
    }

    /**
     *  returns a reference of the profile
     */
    public MktProfile getMktProfile(){
        return profile;
    }


	/**
	 * Return the profile for newletter subscription. <br>
	 * 
	 * @return marketing profile for newsletters. 
	 */
	public MktProfile getNewsletterProfile() {
		return newsletterProfile;
	}


	/**
	 * Set the profile for newletter subscription. <br>
	 * 
	 * @param newsletterProfile marketing profile for newsletters.
	 */
	public void setNewsletterProfile(MktProfile newsletterProfile) {
		this.newsletterProfile = newsletterProfile;
	}


	/**
	 * Read the profile from backend, for the given attribute set. <br>
	 * 
	 */
	public MktProfile readProfile(TechKey attributeSetKey, Shop shop)
			throws CommunicationException {
        
		MktProfile profile = null;
		    	
		final String METHOD = "readProfile()";
		loc.entering(METHOD);
		if (loc.isDebugEnabled())
			loc.debug("attributeeSetkey="+attributeSetKey+ ", shop="+shop);
		try {
			// read profile from Backend
			profile = (MktProfile)getProfileBackendService().read(mktPartner,
																  attributeSetKey,
																  shop);
		}
		catch (BackendException ex) {
			BusinessObjectHelper.splitException(ex);
		}
		finally {
			loc.exiting();
		}
		
		return profile;
	}
    
    /**
     * Read the profile from backend, for the given attribute set. <br>
     * 
     */
    public MktProfile readProfile(TechKey attributeSetKey, MarketingConfiguration configuration)
            throws CommunicationException {
        
        MktProfile profile = null;
                
        final String METHOD = "readProfile()";
        loc.entering(METHOD);
        if (loc.isDebugEnabled())
            loc.debug("attributeeSetkey="+attributeSetKey+ ", configuration=" + configuration);
        try {
            // read profile from Backend
            profile = (MktProfile)getProfileBackendService().read(mktPartner,
                                                                  attributeSetKey,
                                                                  configuration);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally {
            loc.exiting();
        }
        
        return profile;
    }


    /**
     * Read the profile from backend, for the given attribute set.
     * and adjust the local profile. <br>
     */
    public MktProfile readMktProfile(TechKey attributeSetKey, Shop shop)
            throws CommunicationException {
            	
		final String METHOD = "readMktProfile()";
		loc.entering(METHOD);
		
		MktProfile profile = readProfile(attributeSetKey, shop); 
		if (profile != null) {
			this.profile = profile;
		}
		
		loc.exiting();
        return this.profile;
    }
    
    
    /**
     * Read the profile from backend, for the given attribute set.
     * and adjust the local profile. <br>
     */
    public MktProfile readMktProfile(TechKey attributeSetKey, MarketingConfiguration configuration)
            throws CommunicationException {
                
        final String METHOD = "readMktProfile()";
        loc.entering(METHOD);
        
        MktProfile profile = readProfile(attributeSetKey, configuration); 
        if (profile != null) {
            this.profile = profile;
        }
        
        loc.exiting();
        return this.profile;
    }


	/**
	 * Save the given profile in the backend. <br>
	 * 
     * @param profile
     * @param shop
     * @throws CommunicationException
	 */
	public void saveProfile(MktProfile profile, Shop shop)
			throws CommunicationException {
		final String METHOD = "saveProfile()";
		loc.entering(METHOD);
		if (loc.isDebugEnabled())
			loc.debug("profile="+profile+ ", shop="+shop);
		try {
			// save profile in the backend
			getProfileBackendService().save(mktPartner, profile, shop);
		}
		catch (BackendException ex) {
			BusinessObjectHelper.splitException(ex);
		}
		finally {
			loc.exiting();
		}
	}
    
    /**
     * Save the given profile in the backend. <br>
     * 
     * @param profile
     * @param configuration
     * @throws CommunicationException
     */
    public void saveProfile(MktProfile profile, MarketingConfiguration configuration)
            throws CommunicationException {
        final String METHOD = "saveProfile()";
        loc.entering(METHOD);
        if (loc.isDebugEnabled())
            loc.debug("profile="+profile+ ", configuration=" + configuration);
        try {
            // save profile in the backend
            getProfileBackendService().save(mktPartner, profile, configuration);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally {
            loc.exiting();
        }
    }


    /**
     *  Save the given profile in the backend and adjust the property profile. <br>
     * 
     * @param profile
     * @param shop
     * @throws CommunicationException
     */
    public void saveMktProfile(MktProfile profile, Shop shop)
            throws CommunicationException {

		final String METHOD = "saveMktProfile()";
		loc.entering(METHOD);

		saveProfile(profile,shop);
        this.profile = profile;
        if (profile.isValid()) {
            // read the recommendation from backend, after profile is saved
            recommendations = null;
        }
        
		loc.exiting();
    }
    
    /**
     *  Save the given profile in the backend and adjust the property profile. <br>
     * 
     * @param profile
     * @param configuration 
     * @throws CommunicationException
     */
    public void saveMktProfile(MktProfile profile, MarketingConfiguration configuration)
            throws CommunicationException {

        final String METHOD = "saveMktProfile()";
        loc.entering(METHOD);

        saveProfile(profile, configuration);
        this.profile = profile;
        if (profile.isValid()) {
            // read the recommendation from backend, after profile is saved
            recommendations = null;
        }
        
        loc.exiting();
    }


    /**
     * Set the property recommendation
     *
     * @param recommendation
     *
     */
    public void setRecommendation(PersonalizedRecommendation recommendation) {
        this.recommendations = recommendation;
    }

    /**
     * Returns the property recommendation. <br>
     *
     * @return recommendation
     *
     */
    public PersonalizedRecommendation getRecommendation() {
        return recommendations;
    }


    /**
     *
     * Read the personalized product recommendation for the a logged user
     *
     * @return reference to the personalized recommendation
     */
    public PersonalizedRecommendation readPersonalizedRecommendation(Shop shop)
            throws CommunicationException {
		final String METHOD = "readPersonalizedRecommendation()";
		loc.entering(METHOD);
		if (loc.isDebugEnabled())
			loc.debug("shop="+shop);
		try {
	        if (recommendations == null) {
	            try {
	                recommendations = new PersonalizedRecommendation();
	                getRecommendationBackendService().readPersonalizedRecommendation(recommendations, 
                                                                                     mktPartner, 
                                                                                     shop);
	            }
	            catch (BackendException e) {
	                BusinessObjectHelper.splitException(e);
	            }
	        }
		} finally {
			loc.exiting();
		}
        return recommendations;
    }
    
    /**
     *
     * Read the personalized product recommendation for the a logged user
     *
     * @return reference to the personalized recommendation
     */
    public PersonalizedRecommendation readPersonalizedRecommendation(MarketingConfiguration configuration)
            throws CommunicationException {
        final String METHOD = "readPersonalizedRecommendation()";
        loc.entering(METHOD);
        if (loc.isDebugEnabled())
            loc.debug("configuration = " + configuration);
        try {
            if (recommendations == null) {
                try {
                    recommendations = new PersonalizedRecommendation();
                    getRecommendationBackendService().readPersonalizedRecommendation(recommendations, 
                                                                                     mktPartner, 
                                                                                     configuration);
                }
                catch (BackendException e) {
                    BusinessObjectHelper.splitException(e);
                }
            }
        } finally {
            loc.exiting();
        }
        return recommendations;
    }

    /**
     * Read the personlized product recommendation for a known but unlogged user.
     * This method was written for the b2c szenario only. The user is declared to be known if
     * the soldToTechKey is delivered.
     *
     * @param soldToTechKey technical key of the sold-to in the b2c scenario
     * @param shop the currently visited shop
     *
     * @return reference to the personalized recommendation
     */
    public PersonalizedRecommendation readPersonalizedRecommendation(TechKey soldToTechKey, Shop shop)
            throws CommunicationException {
		final String METHOD = "readPersonalizedRecommendation()";
		loc.entering(METHOD);
		if (loc.isDebugEnabled())
			loc.debug("soldToTechKey="+soldToTechKey+ ", shop="+shop);
		try {
		
	        if (recommendations == null) {
	            try {
	                recommendations = new PersonalizedRecommendation();
	                getRecommendationBackendService().readPersonalizedRecommendation(recommendations, soldToTechKey, shop);
	            }
	            catch (BackendException e) {
	                BusinessObjectHelper.splitException(e);
	            }
	        }
		} 
		finally {
			loc.exiting();
		}
        return recommendations;
    }


    public RegisterStatus getRegisterStatus() {

        return registerStatus;
    }
	
	
	/**
	 * Reset the marketing settings. I.e. recommendations, profiles
	 * newsletterprofiles are set to null.
	 * A new MktPartner is created based on the business partner
	 * provided.
	 * 
	 *@param business partner to create marketing user
	 * 
	 */
	public void resetMarketingSettings(TechKey businessPartner) {

		// reset recommendation for b2c
		recommendations = null;
		profile = null;
		newsletterProfile = null;
		mktPartner = new MktPartner();
		mktPartner.setTechKey(businessPartner);
        
	}    


    private void resetMarketingSettings() {

        // reset recommendation for b2c
        recommendations = null;
        profile = null;
        newsletterProfile = null;
        mktPartner = new MktPartner();
        mktPartner.setTechKey(businessPartner);
        
    }    

    /* 
     * in call center mode the business partner assignment is done independently of login
     *  this requires an external reset of the marketing settings
     */ 
    
    public void resetMarketingSettingsCCM() {
        if (this.IsCallCenterAgent()) {
            // reset recommendation for b2c, only allowed for call center agents!
            recommendations = null;
            profile = null;
            newsletterProfile = null;
            mktPartner = new MktPartner();
            mktPartner.setTechKey(businessPartner);
        }
        
    }    


    /**
     * Register a new consumer and create a new
     * loyalty program membership for this consumer.
     * 
     * 
     * 
     */
     public RegisterStatus registerAndJoinLoyaltyProgram(String shopId,
                                                         String pointCode, 
	                                                     LoyaltyMembership loyMemberShip,
								                         Address address,
								                         String password,
								                         String password_verify)
		throws CommunicationException {

	      final String METHOD = "register()";
	      loc.entering(METHOD);
	      if (log.isDebugEnabled())
		     log.debug("shopId="+shopId+", address="+address);
	      try {
		     registerStatus = getLoyaltyUserBackendService().registerAndJoinLoyaltyProgram(this, shopId, pointCode, loyMemberShip, address, password, password_verify);
		     loginSuccessful = (registerStatus == RegisterStatus.OK) ? true : false;

		     if (loginSuccessful) {
			    resetMarketingSettings();               
		     }    

		     return registerStatus;
	      }
	      catch (BackendException ex) {
		     BusinessObjectHelper.splitException(ex);
   	      }
	      finally {
		    loc.exiting();
	      }
	      return RegisterStatus.NOT_OK;
     }
      
      
	/**
	 * login of the internet user into
	 * the b2b or b2c szenario and
	 * check if the user is member of
	 * a loyalty program
	 *
	 * @param id of the internet user
	 * @param password of the internet user
	 * @param loyalty membership 
	 *
	 * @return integer value that indicates if
	 *         the login was sucessful
	 */
	public LoginStatus loginAndCheckLoyaltyMembership(LoyaltyMembership loyMembership)
	    throws CommunicationException {
		
		final String METHOD = "loginAndCheckLoyaltyMembership()";
		loc.entering(METHOD);
		
		LoginStatus status = LoginStatus.NOT_OK;
		

	    try {		
		   status = getLoyaltyUserBackendService().loginAndCheckLoyaltyMembership(this, loyMembership);
		   loginSuccessful = (status.toString().equals("OK")) ? true : false;
		    if (status == LoginStatus.OK || status == LoginStatus.NOT_OK_PASSWORD_EXPIRED) {
			   // reset recommendation for b2c
			   resetMarketingSettings();
		    }
	    }
		catch (BackendException ex) {
           BusinessObjectHelper.splitException(ex);
        }
        finally {
           loc.exiting();
        }
		 
		return status;
	}      
  
	/**
	 * login of the internet user into
	 * the b2b or b2c szenario and
	 * join the given loyalty program
	 *
	 * @param id of the internet user
	 * @param password of the internet user
	 * @param loyalty membership 
	 *
	 * @return integer value that indicates if
	 *         the login was sucessful
	 */
	public LoginStatus loginAndJoinLoyaltyProgram(LoyaltyMembership loyMembership)
		throws CommunicationException {
		
		final String METHOD = "loginAndCheckLoyaltyMembership()";
		loc.entering(METHOD);
		
		LoginStatus status = LoginStatus.NOT_OK;
		
		try {		
		   status = getLoyaltyUserBackendService().loginAndJoinLoyaltyProgram(this, loyMembership);
		   loginSuccessful = (status.toString().equals("OK")) ? true : false;
		if (status == LoginStatus.OK || status == LoginStatus.NOT_OK_PASSWORD_EXPIRED) {
			// reset recommendation for b2c
			resetMarketingSettings();
		}
		}
		catch (BackendException ex) {
		   BusinessObjectHelper.splitException(ex);
		}
		finally {
		   loc.exiting();
		}
		 
		return status;
	}      


  
	/**
	 * get the reference of the user backend object
	 *
	 */
	private LoyaltyUserBackend getLoyaltyUserBackendService()
		throws BackendException {

		return (LoyaltyUserBackend)getBackendService();
	}    
}
