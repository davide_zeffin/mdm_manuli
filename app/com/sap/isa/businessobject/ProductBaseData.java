/*****************************************************************************
	Inteface:     ProductBaseData
	Copyright (c) 2006, SAP AG, Germany, All rights reserved.
	Author:       SAP
	Created:      March 2006
	Version:      1.0

	$Revision: #3 $
	$Date: 2006/03/16 $
*****************************************************************************/
package com.sap.isa.businessobject;

import java.util.ArrayList;

import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;
import com.sap.isa.businessobject.item.ContractDuration;
import com.sap.isa.catalog.webcatalog.WebCatItemPrice;
import com.sap.isa.core.TechKey;

/**
 * @author SAP AG
 *
 * General interface for UI classes
 * The interface should be used in JSPs for several types of items (Product, WebCatItem)
 * 
 */
public interface ProductBaseData extends BusinessObjectBaseData {

	/**
	 * Returns the description of the product
	 *
	 * @return description
	 */
	public String getDescription();

	/**
	 * returns eye catcher text.
	 * 
	 * @return eye catcher text as String
	 */
	public String getEyeCatcherText();

	/**
	 * Returns the product ID
	 *
	 * @return id
	 */
	public String getId();

	/**
	 * Returns the unique ItemId.
	 *
	 * @return ItemId
	 *
	 */
	public String getItemId();

	/**
	 * returns eye catcher text.
	 * 
	 * @return eye catcher text as String
	 */
	public String getPriceEyeCatcherText();

	/**
	 * Returns the URL for the picture of the product
	 *
	 * @return picture
	 */
	public String getPicture();

	/**
	 * Returns WebCatItemPrice object if present
	 * else null
	 *
	 * @return WebCatItemPrice object if present
	 *         else null
	 */
	public WebCatItemPrice getProductPrice();

	/**
	* Returns the URL for the thumbnail of the product. <br>
	*
	* @return thumb
	*/
	public String getThumb();

	/**
	* Returns the group name of the product. <br>
	*
	* @return group description
	*/
	public String getGroupKey();
    
    /**
     * This methods returns the hierarchy ids of the WebCatItem if present,
     * otherwise a String array of length 0 is returned.
     */
    public String[] getHierarchyIds();
    
    /**
     * This methods returns the hierarchy description of the WebCatItem if present,
     * otherwise a String array of length 0 is returned.
     * AN unknown desription will be indetified by -
     */
    public String[] getHierarchyDescriptions();

	/**
	* Returns the hierarchy level of the product. <br>
	*
	* @return hierarchy level as int value
	*/
	public int getHierarchyLevel();

	/**
	 * Get flag whether this item is selected via checkbox or radio button.
	 *
	 * @return selection value of type boolean 
	 */
	public boolean getAuthorFlag();

	/**
	 * Get Solution configurator selection flag.
	 *
	 * @return selection flag as String
	 */
	public String getAuthor();
    
    /**
     * This methods returns the category ids of the WebCatItem if present,
     * otherwise a String array of length 0 is returned.
     */
    public String[] getCategoryIds();
    
    /**
     * This methods returns the category description of the WebCatItem if present,
     * otherwise a String array of length 0 is returned.
     * AN unknown desription will be indetified by -
     */
    public String[] getCategoryDescriptions();

    /**
     * Returns the information, if this product is selected by the 
     * solution configurator.
     * 
     * @return <code>true</code>, if the item is selected by the solution configurator
     *         <code>false</code>, if not
     */
    public boolean isScSelected();

	/**
	 * Checks, if the item is a Sales Package.
	 * The method implements ProductBaseData
	 * 
	 * @return  true, if the item is a Sales Package
	 *          false, otherwise.
	 */
	public boolean isSalesPackage();

	/**
	 * Checks, if the product is classified as a Combined Rate Plan.
	 * 
	 * This is the case, if the attribute IS_RATEPLAN_PROD is set to "X".
	 * 
	 * @return  true, if the product is a Combined Rate Plan 
	 *          false, otherwise.
	 */
	public boolean isCombinedRatePlan();

	/**
	 * Checks, if the product is classified as a Dependent Component.
	 * 
	 * @return  true, if the product is a dependent component
	 *          false, otherwise.
	 */
	public boolean isDependentComponent();
    
    /**
     * Checks if this item is classified as Rate Plan.
     * The method implements ProductBaseData
     * 
     * @return  true, if the item is a rate plan product,
     *          false, otherwise.
     */
    public boolean isRatePlan();

	/**
	  * Checks, if the product is classified as a Rate Plan Combination
	  * 
	  * @return  true, if the product is a Rate Plan Combination 
	  *          false, otherwise.
	  */
	public boolean isRatePlanCombination();

	/**
	 * Checks, if the product is classified as a Sales Component.
	 * 
	 * @return  true, if the product is a sales component
	 *          false, otherwise.
	 */
	public boolean isSalesComponent();

	/**
	* Checks if this product is optional.
	* 
	* @return  true, if the product is optional,
	*          false, otherwise.
	*/
	public boolean isOptional();

	/**
	 * Checks if this product is in a group.
	 * 
	 * @return  true, if the product is in a group,
	 *          false, otherwise.
	 */
	public boolean isPartOfAGroup();

	/**
	 * Checks if this product is a product variant.
	 * 
	 * @return  true, if the product is a product variant,
	 *          false, otherwise.
	 */
	public boolean isProductVariant();

	/**
	 * set flag whether this item is selected via checkbox or radio button.
	 *
	 * @param selectionFlag of type boolean as it needs to be checked for web 
	 *         display
	 */
	public void setAuthorFlag(boolean selectionFlag);

	/**
	 * returns true if it is a configurable product, false otherwise
	 * 
	 * @return true if it is a configurable product, 
	 *         false otherwise
	 */
	public boolean isConfigurable();

	/**
	 * Check whether the configuration of the item is complete and consistent
	 *
	 * @return boolean indicating whether the items configuration contains errors
	 */
	public boolean isConfiguredCompletely();

	/**
	 * Returns true, if the product is old fashion configurable
	 *
	 * @return true   if the product is old fashion configurable, false  otherwise.
	 */
	public boolean isItemOldFashionConfigurable();

	/**
	 * get flag whether this item is selected via checkbox for comparing.
	 *
	 * @return selected boolean value 
	 */
	public boolean isSelected();

	/**
	 * get flag whether this item is selected via checkbox for comparing.
	 *
	 * @return selected boolean value, represented as String as it needs to be checked for web 
	 *         display
	 */
	public String isSelectedStr();

	/**
	 * Get parent product of a product.
	 * 
	 * @return  parent product of type ProductBaseData 
	 */
	public ProductBaseData getParent();

	/**
	 * Returns quantity. Quantity is not a catalog data, but a property used for the web 
	 * visualization to be able to select the quantity before adding products to a shopping basket, 
	 * order etc. This quantity is only valid for the lifetime of the itemKey and is not saved
	 * anywhere; it should be used when adding this item to some basket etc.
	 *
	 * @return quantity of this item, as String
	 */
	public String getQuantityAsStr();
    
    /**
     * Returns all attributes with key <code>AttributeKeyConstants.UNIT_OF_MEASUREMENT</code>.
     *
     * @return array with attributes
     */
    public String[] getUnitsOfMeasurement();

	/**
	 * Returns the first unit of measurement for a product
	 *
	 * @return unit of measurement, as String
	 */
	public String getUnit();
    
    /**
     * returns true if it is a subcomponent, false otherwise
     * 
     * @return boolean, true if is a subcomponent
     */
    public boolean isSubComponent();

	/**
	 * Checks if the item is relevant for explosion with the Solution Configurator.<br> 
	 * 
	 * @return true, if the item is relevant for explosion.<br>
	 *         false, otherwise
	 */
	public boolean isRelevantForExplosion();

	/**
	 * Get parent techKey of a product.
	 * The method implements a method of ProductBaseData.
	 * 
	 * @return  parent techKey  
	 */
	public TechKey getParentTechKey();

	/**
	 * returns the default contract duration for this WebCatItem
	 * 
	 * @return the default contract duration for this WebCatItem
	 */
	public ContractDuration getDefaultContractDuration();

	/**
	 * returns the table of all contract duration for this WebCatItem
	 * 
	 * @return the array with the different contract duration for this WebCatItem
	 */
	public ArrayList getContractDurationArray();

	/**
	  * returns the selected contract duration for this WebCatItem. 
	  * The selected contract duration is set in the JSP pages.
	  * 
	  * @return the selected contract duration for this WebCatItem
	  */
	public ContractDuration getSelectedContractDuration();
    
    /**
     * Returns true, if the item is subitems
     * 
     * @param true if the underlying WebCatItem has subItems
     * 
     */
    public boolean hasSubItems();

	/**
	 * sets the default contract duration
	 * 
	 * @param defaultContractDuration : the defaultContractDuration for this WebCatItem
	 * 
	 */
	public void setDefaultContractDuration(ContractDuration defaultContractDuration);

	/**
	 * sets the contract duration array
	 * 
	 * @param contractDurationArray : the contractDurationArray for this WebCatItem
	 * 
	 */
	public void setContractDurationArray(ArrayList contractDurationArray);

	/**
	 * sets the selected contract duration
	 * 
	 * @param selectedContractDuration : the selectedContractDuration for this WebCatItem
	 * 
	 */
	public void setSelectedContractDuration(ContractDuration selectedContractDuration);

    /**
     * Returns the points of the product
     * @param loyPtCodeId, Point Code as String
     * @return Points as String
     */
    public String getPoints(String loyPtCodeId);

    /**
     * Checks if the items is a points item of a reward area
     * 
     * @return isPtsItem flag as boolean
     */
    public boolean isPtsItem();

    /**
     * Checks if the items is a buy points item of a buy points area
     * 
     * @return isBuyPtsItem flag as boolean
     */
    public boolean isBuyPtsItem();

}
