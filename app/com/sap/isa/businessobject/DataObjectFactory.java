/*****************************************************************************
    Class:        DataObjectFactory
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Thomas Smits
    Created:      29.3.2001
    Version:      1.0

    $Revision: #2 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.businessobject;

import com.sap.isa.backend.BackendDataObjectFactory;
import com.sap.isa.backend.boi.isacore.ProductBatchData;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.UserData;
import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.businessobject.marketing.MarketingDataObjectFactory;

/**
 * Class used for the creation of business objects.
 *
 * @author Thomas Smits
 * @version 1.0
 */
public class DataObjectFactory extends MarketingDataObjectFactory implements BackendDataObjectFactory {

    /**
     * Creates a new <code>ItemData</code> object.
     *
     * @return The newly created object
     */
    public ItemData createItemData() {
        return new ItemSalesDoc();
    }


    /**
     * Creates a new <code>ShopData</code> object.
     *
     * @return The newly created object
     */
    public ShopData createShopData() {
        return new Shop();
    }

	/**
     * Creates a new <code>ProductBatchData</code> object.
     *
     * @return The newly created object
     */
    public ProductBatchData createProductBatchData() {
        return new ProductBatch();
    }

   /**
    * Creates a new <code>UserData</code> object.
    *
    * @return The newly created object
    */
    public UserData createUserData() {
        return new User();
    }
}