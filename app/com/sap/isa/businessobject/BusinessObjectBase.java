/*****************************************************************************
    Class:        BusinessObjectBase
    Copyright (c) 2001, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      M�rz 2001
    Version:      1.0

    $Revision: #8 $
    $Date: 2003/03/31 $
*****************************************************************************/
package com.sap.isa.businessobject;

import java.util.Iterator;

import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.businessobject.ObjectBase;
import com.sap.isa.core.businessobject.event.BusinessEventHandlerBase;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.MessageList;


/**
 *
 * The BusinessObjectBase(BOB) is a possible super class for business objects (BO), which
 * will handle with states and messages which will be logged or displayed in JSP.
 *
 * <p>You can assign messages to the Business Object. With the message type <code>ERROR</code>
 * you set the state of the BO to invalid. For example if you assign an error
 * message to the BO, the method <code>isValid</code> will return <code>false</code>.
 * You can also set a Business Object directly with method <code>SetInvalid</code>
 * to invalid.<br>
 *
 * With the method <code>clearMessages</code> you can clear all messages you have assigned
 * to the BO and the state will be reset to valid.
 *
 * <p><i><b>Note:</b> By default the methods <code>IsValid</code> and
 * <code>clearMessages</code> will work also of all sub objects
 * of the objects. An object has sub object, if it implements <code>Iterable</code>
 * and provide an iterator over objects which extends
 * <code>BusinessObjectBase</code>. </i>
 *
 * <p>The BOB also hold a reference to an <code>IsaLocation</code> object that you can use
 * directly within your class or you could get a reference with the method <code>getLog</code>. <br>
 * You have also the possiblity to log a message with the method <code>logMessage</code>
 *
 *
 * <p><b>Example</b>
 * <pre>
 * class MktQuestionary extends BusinessObjectBase{ ...
 *
 * MktQuestionary questionary;
 * ...
 *
 *
 * // You can use the isValid method in Action
 * if (!questionary.isValid()){
 *      // write object to context to use it in ERROR.JSP
 *      request.setAttribute(CONTEXTNAME, questionary);
 *      return mapping.findForward("error");
 * }
 * </pre>
 *
 * The message could diplay with the message tag on jsp.
 * <p><b>Example</b>
 * <pre>
 * &lt;isa:message  id="description"
 *                  name="&lt;%=BusinessObjectBase.CONTEXT_NAME %&gt;"
 *                  type="&lt;%=Message.Error%&gt;"&gt;
 *      &lt;%=description%&gt;
 * &lt;/isa:message&gt;
 * </pre>
 *
 * A Message also have a property called property to describe the property of BO
 * where the error occured. For example in the User-Object the field "Street" could be a property.
 * Then you can directly access with property to assigned messages
 * <p><b>Example</b>
 * <pre>
 * &lt;isa:message  id="description"
 *                  name="&lt;%=BusinessObjectBase.CONTEXT_NAME %&gt;"
 *                  type="&lt;%=Message.Error%&gt;"
 *                  property="street"&gt;
 *      &lt;%=description%&gt;
 * &lt;/isa:message&gt;
 * </pre>
 *
 *
 * @author  SAP AG
 * @version 1.0
 *
 * @see com.sap.isa.core.util.Message
 *
 */

public abstract class BusinessObjectBase extends ObjectBase
    implements BusinessObjectBaseData {

  //  implements BusinessObjectBaseData, BusinessEventSource {


    /**
     * Name to stored a BusinessObject in the request context.
     */
    public final static String CONTEXT_NAME = "BusinessObjectBase.businessobject.isa.sap.com" ;

    private MessageList bobMessages;
    private int bobState;

    /**
     *  The base class provide reference to a log location<br>
     *
     *  <i>By default the constructor get an Instance of <code>this.getClass().getName()</code> </i>
     */
    protected static IsaLocation log;


    /**
     * Reference to the businessEventHandler.
     */
    protected BusinessEventHandlerBase businessEventHandlerBase;


    /**
     * Method used to set a reference to the business event handler for this class
     * from the outside.
     *
     * @param businessEventHandler the handler to be set
     */
    public void setBusinessEventHandler(BusinessEventHandlerBase businessEventHandler) {
        this.businessEventHandlerBase = businessEventHandler;
    }

    /**
     * standard constructor
     */
    public BusinessObjectBase() {
        bobMessages = new MessageList();
        log = IsaLocation.getInstance(this.getClass().getName());
        bobState = VALID;
    }

    /**
     * Constructor for the object directly taking the technical
     * key for the object
     *
     * @param techKey Technical key for the object
     */
    protected BusinessObjectBase(TechKey techKey) {
        super(techKey);
        bobMessages = new MessageList();
        log = IsaLocation.getInstance(this.getClass().getName());
        bobState = VALID;
    }


    /**
     * Returns the property log
     *
     * @return log
     *
     */
    public IsaLocation getLog() {
        return log;
    }


    /**
     * Add a message to messagelist of Business Object
     *
     * @param message message to add
     */
    public void addMessage(Message message) {

        if (message.isError()) {
            this.bobState = INVALID;
        }

        bobMessages.add(message);
    }



    /**
     * copy the messages from an another BusinessObject and add this to the object
     *
     * @param bob reference to a <code>BusinessObjectBase</code> object
     *
     */
    public void copyMessages(BusinessObjectBase bob) {

        Iterator i = bob.getMessageList().iterator();

        while (i.hasNext()) {
            addMessage((Message)i.next());
        }
    }



    /**
     * Set the Business Object invalid, no property
     *
     */
    public void setInvalid() {
        this.bobState = INVALID;
    }


	/**
	 * Set the Business Object valid, no property
	 *
	 */
	public void setValid() {
		this.bobState = VALID;
	}


    /**
     * Returns if the business object is valid
     *
     * @return valid
     *
     */
    public boolean isValid() {

        int bobState = this.bobState;

        if (bobState == INVALID) {
            return (false);
        }

		Iterator i = getSubObjectIterator();

        while (i != null && i.hasNext()) {
            Object obj = i.next();
            if (obj instanceof BusinessObjectBase ) {
                BusinessObjectBase bo = (BusinessObjectBase) obj;
                if (!bo.isValid()) {
                    return false;
                }
            }
        }

        return true;
    }

    
    /**
     * Returns if the business object or one of it sub objects 
     * has a message.
     *
     * @return <code>true</code>, if at least one message exists
     */
    public boolean hasMessages() {
    	
        if (!bobMessages.isEmpty()) {
        	return true;
        }
        
        Iterator iter = getSubObjectIterator();
        while (iter != null && iter.hasNext()) {
            Object obj = iter.next();
            if (obj instanceof BusinessObjectBase ) {
                BusinessObjectBase bo = (BusinessObjectBase) obj;
                if (!bo.getMessageList().isEmpty()) {
                    return true;
                }
            }
        }
		return false;        
    }


    /**
     *
     * clear all messages and set state of the Business Object to valid
     * by default all messages of subobjects will cleared also
     *
     */
    public void clearMessages( ) {

        bobState = VALID;
        bobMessages.clear();

        Iterator i = getSubObjectIterator();

        while (i != null &&i.hasNext()) {
            Object obj = i.next();
            if (obj instanceof BusinessObjectBase ) {
                BusinessObjectBase bo = (BusinessObjectBase) obj;
                bo.clearMessages();
            }
        }

    }


    /**
     * Removes all messages with the given key from the list of messages.<br>
     *
     * @param resourceKey resourceKey of messages 
     */
    public void clearMessages(String resourceKey ) {

        bobMessages.remove(new String[] {resourceKey});

    }

    /**
     * Returns the messages of the Business Object.
     *
     * @return message list of Business Object
     */
    public MessageList getMessageList() {
        return bobMessages;
    }



    /**
     * Log an message to the IsaLocation of the Business Object
     *
     * @param message message to log
     */
    public void logMessage(Message message) {

        if (message.isError()) {
            this.bobState = INVALID;
        }

        message.log(log);
    }
    
    
	/**
     * Makes a copy of the object.<br> 
     * 
     * The MessageList is copied.
     * 
     * @returns a copy of object. 
     * 
	 */
	protected Object clone() throws CloneNotSupportedException {
	
        BusinessObjectBase bob = (BusinessObjectBase)super.clone();

        bob.bobMessages = new MessageList();
        
        bob.copyMessages(this);                
        
    	return bob;
	}

}




