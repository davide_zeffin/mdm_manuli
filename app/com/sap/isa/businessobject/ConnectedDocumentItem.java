/*****************************************************************************
    Class:        ConnectedDocumentItem
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Created:      May 2002

    $Revision: #0 $
    $Date: 2002/05/28 $
*****************************************************************************/
package com.sap.isa.businessobject;

import com.sap.isa.core.TechKey;
import com.sap.isa.backend.boi.isacore.ConnectedDocumentItemData;

/**
 * The ConnectedDocumentItem represents an entry in the document flow
 */
public class ConnectedDocumentItem extends ConnectedObject
                               implements ConnectedDocumentItemData {

    /**
     * position number of the item 
     */
    protected String posNumber = "";
    /**
     * technical key for the document
     */
    protected TechKey documentKey = TechKey.EMPTY_KEY;
    /**
     * date as attribute for the doc flow 
     * e.g. delivery date in the item doc flow entry
     * for a order-delivery ralation
     */
    protected String date = "";
	/**
	 * quantity as attribute for the doc flow 
	 * e.g. delivered quantity in the item doc flow entry
	 * for a order-delivery ralation
	 */	
	protected String quantity = ""; 
	/**
	 * quantity unit as attribute for the doc flow 
	 * see the reference attribute 'quantity'
	 */	
	protected String unitOfMesure = "";	 
	/**
	 * guid of the connected item
	 */
	protected String documentPosKey = ""; 
	/**
     * origin of the connected item
     */
	protected String docOrigin = ""; 
	/**
	 * application type 
	 */
	protected String appTyp = "";
	/**
	 * Reference guid
	 */
	protected String refGuid = "";
	/**
	 * header key for one order objects
	 */
	protected String headerKey = "";
	/**
	 * tracking URL for Delivery connected documents
	 */
	protected String trackingURL = "";	
    /**
     * Returns the key of the document.
     * @return TechKey     
     */        	    
    public TechKey getDocumentKey() {
        return documentKey;
    }

    /**
     * Sets the document key.
     * @param documentKey The document key to set
     */
    public void setDocumentKey(TechKey documentKey) {
        this.documentKey = documentKey;
    }
    

    /**
     * Set the Position Number of the connected Item
     *
     * @param posNumber position number
     *
     */
    public void setPosNumber(String posNumber) {
        this.posNumber = posNumber;
    }


    /**
     * Returns the Position Number of the connected Item
     *
     * @return position number
     *
     */
    public String getPosNumber() {
       return this.posNumber;
    }
    
    /**
     * Returns the date of the relation item
     * e.g. delivery date
     */
	public String getDate() {
		return date;
	}
	
    /**
     * Sets the date for the relation item
     * e.g. delivery date, provided by the doc flow 
     */
	public void setDate(String string) {
		date = string;
	}

    /**
     * returns the quantity for the relation item
     * e.g. delivered quantity
     */
	public String getQuantity() {
		return quantity;
	}

    /**
     * sets the quantity for the relation item
     * e.g. delivered quantity, provided by the doc flow
     */
	public void setQuantity(String quant) {
		this.quantity = quant;
	}
	
    /**
     * returns the quantity unit 
     * see the reference attribute 'quantity'
     */
	public String getUnit() {
		return this.unitOfMesure;
	}

    /**
     * sets the quantity unit provided by the doc flow
     * as attribute of the item relation
     */
	public void setUnit(String quantUnit) {
		this.unitOfMesure = quantUnit;
	}
	/**
	  * Gets the document position key (e.g. guid of the item).
	  * returns documentPosKey 
	  */
	public String getDocumentPosKey() {
		return this.documentPosKey;
	}
	/**
	  * Sets the document position key (e.g. guid of the item).
	  * @param documentPosKey The document position key to set
	  */
	public void setDocumentPosKey(String string) {
		this.documentPosKey = string;
	}

    /** 
     * returns the reference document origin
     */
	public String getDocOrigin() {
		return docOrigin;
	}

   /**
    * sets the document reference origin
    */
	public void setDocOrigin(String string) {
		docOrigin = string;
	}

	/** 
	 * returns the applicaion type
	 */
	public String getAppTyp() {
		return appTyp;
	}

	/** 
	 * sets the application type of reference document
	 */
	public void setAppTyp(String string) {
		appTyp = string;
	}
	
    /**
     * returns the reference guid
     */
	public String getRefGuid() {
		return refGuid;
	}
	
   /**
    * sets the reference guid
    */
	public void setRefGuid(String refGuid) {
		this.refGuid = refGuid;
	}
    /** 
     * returns the header key for item of one order documents
     */
	public String getHeaderKey() {
		return headerKey;
	}
	/** 
	 * sets the header key for item of one order documents
	 */
	public void setHeaderKey(String string) {
		this.headerKey = string;
	}
	/** 
	 * returns the tracking URL for delivery doc flow entry 
	 */
	public String getTrackingURL() {
		return trackingURL;
	}
	/** 
	 * sets the tracking URL for delivery doc flow entry 
	 */
	public void setTrackingURL(String string) {
		this.trackingURL = string;
	}

}
