/* Generated by Together */

package com.sap.isa.businessobject.ipc;

import java.util.Properties;
import com.sap.spc.remote.client.object.IPCBOManagerBase;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCConfigReference;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItemReference;
import com.sap.util.monitor.jarm.IMonitor;

public interface IPCBOManager extends IPCBOManagerBase {

    public static final String NAME = "IPC-BOMANAGER";

	/**
	 * Creates an IPCClient as member if not yet initialized and returns that IPCClient.
	 * The language is required in order to initialize the RFC connection in the backend.
	 * The connection parameter in XCM which are used to define the default connection
	 * parameter might not correspond to the language of the users session.
	 * With this API, the first caller of a session has the possibility to overwrite the 
	 * XCM connnection parameter.
	 */
	public IPCClient createIPCClient(String language);
	
	/**
	 * @param language Used to initialize the language of the RFC (JCO) connection.
	 * @param appServer Used to open an RFC (JCO) connection to a specific application server.
	 * This is needed to use data published by sessions of this application server.
	 * @param sysNumber Used to open an RFC (JCO) connection to a specific applcation server.
	 * This is needed to use data published by sessions of this application server.
	 * @return A new IPCClient if the IPCBOManager does not have an initialized member IPCClient.
	 * The reference to an existing member IPCClient otherwise.
	 */

   /**
    * Creates an IPCClient as member if not yet initialized and returns an IPCClient.
    * Implementations should not add functionality, since the method will be duplicated for each BOManager in an application.
    **/
    public IPCClient createIPCClient(String mandt, String type, IPCItemReference reference)throws IPCException;

   /**
    * Creates an IPCClient as member if not yet initialized and returns an IPCClient.
    * Implementations should not add functionality, since the method will be duplicated for each BOManager in an application.
    **/
    public IPCClient createIPCClient(String mandt, String type, IPCConfigReference reference)throws IPCException;


	/**
	 * Creates an IPCClient as member if not yet initialized and returns an IPCClient.
	 * Implementations should not add functionality, since the method will be duplicated for each BOManager in an application.
	 **/
	 public IPCClient createIPCClient(String mandt, String type, IPCItemReference reference, Properties securityProperties)throws IPCException;

	/**
	 * Creates an IPCClient as member if not yet initialized and returns an IPCClient.
	 * Implementations should not add functionality, since the method will be duplicated for each BOManager in an application.
	 **/
	 public IPCClient createIPCClient(String mandt, String type, IPCConfigReference reference, Properties securityProperties)throws IPCException;

   /**
    * Returns a reference to the IPCClient object.
    **/
    public IPCClient getIPCClient();

	/**
	 * Remove the IPCClient member
	 */
    public void release();

	/**
	 * sets the single activity trace monitor object
	 */
	 public void setJARMMonior(IMonitor monitor);
     
    /**
     * gets the IPCClient from the backend. It is configured as specified in the eai-/XCM-layer.
     * If it does not already exist it is created.
     * @return ipcClient com.sap.isa.businessobject.ipc.IPCClient to use for every access to IPC in ISA 
     */    
//    public IPCClient getBackendIPCClient() throws BackendException;

	/**
	 * Should be used together with the Logon Module. The Logon module will extract the
	 * language from the SSO Ticket, so that we do not need it here.
	 * The Logon module will also set appserver, client and sysnumber, if present in request
	 * 
	 * @param tokenId
	 * @param configId
	 * @return
	 */
 	public IPCClient createIPCClient( String tokenId, String configId);
 	
	/**
	 * @param tokenId
	 * @param configId
	 * @param language
	 * @return
	 */
	public IPCClient createIPCClient( String language, String tokenId, String configId);
			

}
