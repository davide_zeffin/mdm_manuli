/*
 * Created on 09.04.2008
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.businessobject.ipc.imp;

import com.sap.isa.businessobject.GenericBusinessObjectManager;
import com.sap.isa.user.businessobject.UserBase;
import com.sap.isa.user.businessobject.UserBaseAware;

/**
 * @author D048393
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class IPCBOUserManager extends GenericBusinessObjectManager implements UserBaseAware {



	/* (non-Javadoc)
	 * @see com.sap.isa.user.businessobject.UserBaseAware#getUserBase()
	 */
	public UserBase getUserBase() {
		return (UserBase) getBusinessObject(UserBase.class);
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.user.businessobject.UserBaseAware#createUserBase()
	 */
	public UserBase createUserBase() {
			return (UserBase) createBusinessObject(UserBase.class);
		
	}
	
}
