package com.sap.isa.businessobject.ipc.imp;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import com.sap.isa.backend.BackendTypeConstants;
import com.sap.isa.businessobject.ipc.IPCBOManager;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.businessobject.management.BusinessObjectManagerBase;
import com.sap.isa.core.eai.BackendBusinessObject;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.core.eai.sp.ipc.IsaClientLogListener;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.spc.remote.client.ResourceAccessor;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCConfigReference;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItemReference;
import com.sap.spc.remote.client.object.imp.IPCClientObjectFactory;
import com.sap.spc.remote.client.object.imp.rfc.IPCClientBackendBOParams;
import com.sap.spc.remote.client.object.imp.rfc.RFCDefaultClient;
import com.sap.spc.remote.client.object.imp.rfc.RfcDefaultIPCSession;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.tc.logging.Severity;
import com.sap.util.monitor.jarm.IMonitor;


//<--
// implemented the Interface BackendAware for fixing the multiple-IPCClient-bug in ISA
// OSS-message 359993 2004 - Exception beim Aufruf der Konfig. im Katalog ISA R/3 4.0
public class DefaultIPCBOManager extends BusinessObjectManagerBase implements IPCBOManager, BackendAware {
	
    protected BackendObjectManager bem;
    private IPCClient              ipcClient = null;
    private Map ipcClientsByConnectionKey = new HashMap();

	private static final Category category = ResourceAccessor.category;
	private static final Location location = ResourceAccessor.getLocation(BackendObjectManager.class);
	
    //BD 18062003: Changelist 42773 IPC 4.0 SP2 SAT
    //new-->
    // the Single Activity Trace Monitor
    protected IMonitor _monitor = null;

    //<--

    /**
     * Creates a new IPCClient member object and returns it, if the member has not yet been created.
     */
//    public synchronized IPCClient createIPCClient(String mandt, String type) throws IPCException {
//        if (ipcClient == null) {
//            // IPC team changed signature of DefaultIPCClient's constructor
//            // to throw an exception. To fix this, added the
//            // try catch block.
//            try {
//                //BD 19062003: Changelist 42773 IPC 4.0 SP2 SAT
//                //new-->
//                if (_monitor != null) {
//                    ipcClient = IPCClientObjectFactory.getInstance().newIPCClient(mandt, type, _monitor);
//                }
//                else {
//                    //<--
//                    ipcClient = IPCClientObjectFactory.getInstance().newIPCClient(mandt, type);
//                }
//
//                ipcClient.addLogListener(new IsaClientLogListener());
//            }
//            catch (IPCException e) {
//                throw e;
//            }
//            catch (Exception e) {
//                log.fatal("ipcClient could not be created!", e);
//                throw new IPCException(e);
//            }
//
//            //internalSetBackendObjectManager(ipcClient);
//            creationNotification(ipcClient);
//        }
//
//        return ipcClient;
//    }

    /**
     * Creates a client-side session that attaches to the session that is specified by reference.
     * Only host, port, encoding and sessionId of the IPCItemReference are used.
     */
    public synchronized IPCClient createIPCClient(String mandt, String type, IPCItemReference reference) throws IPCException {
        if (ipcClient == null) {
            // IPC team changed signature of DefaultIPCClient's constructor
            // to throw an exception. To fix this, added the
            // try catch block.
            try {
                //BD 19062003: Changelist 42773 IPC 4.0 SP2 SAT
                //new-->
                if (_monitor != null) {
                    ipcClient = IPCClientObjectFactory.getInstance().newIPCClient(mandt, type, reference, _monitor);
                }
                else {
                    //<--
                    ipcClient = IPCClientObjectFactory.getInstance().newIPCClient(mandt, type, reference);
                }

                ipcClient.addLogListener(new IsaClientLogListener());
            }
            catch (IPCException e) {
                throw e;
            }
            catch (Exception e) {
                log.fatal("ipcClient could not be created!", e);
                throw new IPCException(e);

                //ipcClient = null;
            }

            //internalSetBackendObjectManager(ipcClient);
            creationNotification(ipcClient);
        }

        return ipcClient;
    }

    /**
     * Creates a client-side session that attaches to the session that is specified by reference.
     * Only host, port, encoding and sessionId of the IPCConfigReference are used.
     */
    public synchronized IPCClient createIPCClient(String mandt, String type, IPCConfigReference reference) throws IPCException {
        if (ipcClient == null) {
            // IPC team changed signature of DefaultIPCClient's constructor
            // to throw an exception. To fix this, added the
            // try catch block.
            try {
                //BD 19062003: Changelist 42773 IPC 4.0 SP2 SAT
                //new-->
                if (_monitor != null) {
                    ipcClient = IPCClientObjectFactory.getInstance().newIPCClient(mandt, type, reference, _monitor);
                }
                else {
                    //<--
                    ipcClient = IPCClientObjectFactory.getInstance().newIPCClient(mandt, type, reference);
                }

                ipcClient.addLogListener(new IsaClientLogListener());
            }
            catch (IPCException e) {
                throw e;
            }
            catch (Exception e) {
                log.fatal("ipcClient could not be created!", e);
                throw new IPCException(e);

                //ipcClient = null;
            }

            creationNotification(ipcClient);
        }

        return ipcClient;
    }

	/**
	 * Creates a client-side session that attaches to the session that is specified by reference.
	 * Only host, port, encoding and sessionId of the IPCConfigReference are used.
	 * Security properties were also passed.
	 */
	public synchronized IPCClient createIPCClient(String mandt, String type, IPCConfigReference reference, Properties securityProperties) throws IPCException {
		if (ipcClient == null) {
			try {
				ipcClient = IPCClientObjectFactory.getInstance().newIPCClient(mandt, type, reference, securityProperties);
				if (_monitor != null && ipcClient != null) {
					ipcClient.setJARMMonitor(_monitor);
				}

				ipcClient.addLogListener(new IsaClientLogListener());
			}
			catch (IPCException e) {
				throw e;
			}
			catch (Exception e) {
				log.fatal("ipcClient could not be created!", e);
				throw new IPCException(e);
			}
			creationNotification(ipcClient);
		}

		return ipcClient;
	}

	/**
	 * Creates a client-side session that attaches to the session that is specified by reference.
	 * Only host, port, encoding and sessionId of the IPCItemReference are used.
	 * Security properties were also passed. 
	 */
	public synchronized IPCClient createIPCClient(String mandt, String type, IPCItemReference reference, Properties securityProperties) throws IPCException {
		if (ipcClient == null) {
			try {
				ipcClient = IPCClientObjectFactory.getInstance().newIPCClient(mandt, type, reference, securityProperties);
				if (_monitor != null && ipcClient != null) {
					ipcClient.setJARMMonitor(_monitor);
				}

				ipcClient.addLogListener(new IsaClientLogListener());
			}
			catch (IPCException e) {
				throw e;
			}
			catch (Exception e) {
				log.fatal("ipcClient could not be created!", e);
				throw new IPCException(e);
			}
			creationNotification(ipcClient);
		}

		return ipcClient;
	}

	private IPCClient createIPCClient(BackendBusinessObject rfcClientSupport) {
		location.entering("createIPCClient(com.sap.isa.core.eai.BackendBusinessObject)",new Object[]{rfcClientSupport});
		IPCClient ipcClient;
		try {
			//BD 19062003: Changelist 42773 IPC 4.0 SP2 SAT
			//new-->
			if (_monitor != null) {
				ipcClient = IPCClientObjectFactory.getInstance().newIPCClient(rfcClientSupport, _monitor);
			}
			else {
				//<--
				ipcClient = IPCClientObjectFactory.getInstance().newIPCClient(rfcClientSupport);
			}
			category.logT(Severity.DEBUG,location,"Created IPCClient successfully using BackendBusinessObject");
			ipcClient.addLogListener(new IsaClientLogListener());
		}
		catch (IPCException e) {
			throw e;
		}
		catch (Exception e) {
			log.fatal("ipcClient could not be created!", e);
			throw new IPCException(e);
		}

		//internalSetBackendObjectManager(ipcClient);
		creationNotification(ipcClient);
		location.exiting();
		return ipcClient;
	}

	private IPCClient createIPCClient(BackendBusinessObject rfcClientSupport, String tokenId, String configId) {
		location.entering("createIPCClient(com.sap.isa.core.eai.BackendBusinessObject)",new Object[]{rfcClientSupport});
		IPCClient ipcClient;
		try {
			//BD 19062003: Changelist 42773 IPC 4.0 SP2 SAT
			//new-->
			if (_monitor != null) {
				ipcClient = IPCClientObjectFactory.getInstance().newIPCClient(rfcClientSupport, _monitor, tokenId, configId);
			}
			else {
				//<--
				ipcClient = IPCClientObjectFactory.getInstance().newIPCClient(rfcClientSupport, tokenId, configId);
			}
			category.logT(Severity.DEBUG,location,"Created IPCClient successfully using BackendBusinessObject");
			ipcClient.addLogListener(new IsaClientLogListener());
		}
		catch (IPCException e) {
			throw e;
		}
		catch (Exception e) {
			log.fatal("ipcClient could not be created!", e);
			throw new IPCException(e);
		}

		//internalSetBackendObjectManager(ipcClient);
		creationNotification(ipcClient);
		location.exiting();
		return ipcClient;
	}


	/**
	 * Creates a new IPCClient member object and returns it, if the member has not yet been created.
	 */
	public synchronized IPCClient createIPCClient() throws IPCException {
		if (ipcClient == null) {
			BackendBusinessObject rfcClientSupport;
			try {
//				String language = (String)bem.getAttribute(BackendObjectManager.LANGUAGE);
//				IPCClientBackendBOParams params = new IPCClientBackendBOParams(language);
				rfcClientSupport =
					bem.createBackendBusinessObject(
						BackendTypeConstants.BO_TYPE_IPC_CLIENT);
				category.logT(Severity.DEBUG,location,"Created BackendBusinessObject for type " + BackendTypeConstants.BO_TYPE_IPC_CLIENT + "successfully.");
			} catch (BackendException e) {
				throw new IPCException(e);
			}
			ipcClient = createIPCClient(rfcClientSupport);
			String connKey = getConnectionKey(ipcClient);
			ipcClientsByConnectionKey.put(connKey, ipcClient);
		}

		return ipcClient;
	}

	private static String getConnectionKey(IPCClient ipcClient) {
		RfcDefaultIPCSession session = (RfcDefaultIPCSession)ipcClient.getIPCSession();
		RFCDefaultClient rfcClient = (RFCDefaultClient)session.getClient();
		JCoConnection connection = rfcClient.getDefaultIPCJCoConnection(); 
		return connection.getConnectionKey();           
	}
	/**
	 * Returns the IPCClient for the given connection key if it exists or creates one,
	 * if it does not exist. Does not set the member ipcClient of this DefaultIPCBOManager.
	 * If you create the IPCClient using this method, you only get it using this method.
	 */
	public synchronized IPCClient createIPCClientUsingConnection(String connectionKey) {
		IPCClient ipcClient;
		Object ipcClientObject;
		ipcClientObject = ipcClientsByConnectionKey.get(connectionKey); //first look up
		//in the cache of already available clients
		if (ipcClientObject == null) { //not yet available, create one
			if ("".equals(connectionKey) || connectionKey == null) { //use the default connection
				ipcClientObject = createIPCClient(); // language needs to be taken from backend
				ipcClient = (IPCClient)ipcClientObject;
				ipcClientsByConnectionKey.put("", ipcClient); //add the client with "" also
				//for the case that someone first creates the client with the key of the default
				//connection and then calls this method with "". In both cases the same ipcClient
				//should be returned
				String connKey = getConnectionKey(ipcClient);
				ipcClientsByConnectionKey.put(connKey, ipcClient); //put it under the key of the 
				//default connection
			}else { //lookup cached ipcClients or create one with the connection key
					BackendBusinessObject rfcClientSupport;
					try {
						IPCClientBackendBOParams params = new IPCClientBackendBOParams();
						params.setConnectionKey(connectionKey);
						rfcClientSupport =
							bem.createBackendBusinessObject(
								BackendTypeConstants.BO_TYPE_IPC_CLIENT,
								params);
						category.logT(Severity.DEBUG,location,"Created BackendBusinessObject for type " + BackendTypeConstants.BO_TYPE_IPC_CLIENT + "successfully.");
					} catch (BackendException e) {
						throw new IPCException(e);
					}
					ipcClient = createIPCClient(rfcClientSupport);
					ipcClientsByConnectionKey.put(connectionKey, ipcClient);
			}
		}else {
			ipcClient = (IPCClient)ipcClientObject;
		}
		return ipcClient;
	}
	
	public synchronized IPCClient createIPCClient(String language) {
		if (ipcClient == null) {
			IPCClientBackendBOParams params = new IPCClientBackendBOParams(language);
			BackendBusinessObject rfcClientSupport;
			try {
				rfcClientSupport =
					bem.createBackendBusinessObject(
						BackendTypeConstants.BO_TYPE_IPC_CLIENT,
						params);
			} catch (BackendException e) {
				throw new IPCException(e);
			}
			ipcClient = createIPCClient(rfcClientSupport);
			String connKey = getConnectionKey(ipcClient);
			ipcClientsByConnectionKey.put(connKey, ipcClient);
		}
		return ipcClient;
	}


	public synchronized IPCClient createIPCClient(String tokenId, String configId) {
		BackendBusinessObject rfcClientSupport;
		try {
			rfcClientSupport =
				bem.createBackendBusinessObject(
					BackendTypeConstants.BO_TYPE_IPC_CLIENT);
		} catch (BackendException e) {
			throw new IPCException(e);
		}
		ipcClient = createIPCClient(rfcClientSupport, tokenId, configId);
	String connKey = getConnectionKey(ipcClient);
	ipcClientsByConnectionKey.put(connKey, ipcClient);
	return ipcClient;
}


	public synchronized IPCClient createIPCClient(String language, String tokenId, String configId) {
	IPCClientBackendBOParams params = new IPCClientBackendBOParams(language);
		BackendBusinessObject rfcClientSupport;
		try {
			rfcClientSupport =
				bem.createBackendBusinessObject(
					BackendTypeConstants.BO_TYPE_IPC_CLIENT,
					params);
		} catch (BackendException e) {
			throw new IPCException(e);
		}
		ipcClient = createIPCClient(rfcClientSupport, tokenId, configId);
	String connKey = getConnectionKey(ipcClient);
	ipcClientsByConnectionKey.put(connKey, ipcClient);
	return ipcClient;
}

	

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public synchronized IPCClient getIPCClient() {
        return ipcClient;
    }

    /**
     * DOCUMENT ME!
     */
    public synchronized void release() {
        if (ipcClient != null) {
            removalNotification(ipcClient);
        }

        ipcClient = null;
    }

    /**
     * sets the single activity trace monitor object.
     */
    public void setJARMMonior(IMonitor monitor) {
        _monitor = monitor;
    }

//    /**
//     * Utility method which can be used to assign the BackendObjectManager
//     * to Business Objects which implement the BackendAware
//     * @see BackendAware BackendAware
//     * TODO bem is the member variable bem of this class. It is assigned to itself in setBackendObjectManager(bem) ????
//     */
//    protected void assignBackendObjectManager(Object obj) {
//        if ((obj instanceof BackendAware) && (obj != null)) {
//            ((BackendAware) obj).setBackendObjectManager(bem);
//        }
//    }

    /**
     * Set the BackendBusinessobjectManager into this object
     *
     * @param Backend object to be used
     */
    public void setBackendObjectManager(BackendObjectManager bem) {
        this.bem = bem;
        initBackend();
    }

	/**
	 * create the dummy backend object, which stores the BO Manager in the backend context.
	 *
	 */
	protected void initBackend() {
		try {
			IPCClientBackendBOParams params = new IPCClientBackendBOParams();
			params.setIPCBOManager(this);
				bem.createBackendBusinessObject(
					IPCBOManager.NAME,
					params);
			category.logT(Severity.DEBUG,location,"Created BackendBusinessObject for type " + BackendTypeConstants.BO_TYPE_IPC_CLIENT + "successfully.");
		} catch (BackendException e) {
			throw new IPCException(e);
		}
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCBOManagerBase#createIPCClient()
	 */


//    /**
//     * gets the IPCClient from the backend. It is configured as specified in the eai-/XCM-layer.
//     * If it does not already exist it is created.
//     * @return ipcClient com.sap.isa.businessobject.ipc.IPCClient to use for every access to IPC in ISA 
//     */
//    public synchronized IPCClient getBackendIPCClient() throws BackendException {
//        if (log.isDebugEnabled()) {
//            log.debug("Getting an IPCClient for ISA from the backend");
//        }
//        
//        if (this.ipcClient == null) {
//            this.ipcClient = createIPCClient();
//            if (log.isDebugEnabled()) {
//                log.debug("Successfully got an IPCClient for ISA from the backend");
//            }
//        }
//        else {
//            if (log.isDebugEnabled()) {
//                log.debug("Request for an IPCClient for ISA from the backend could be satisfied from the buffered ipcClient");
//            }
//        }
//
//
//
//        return ipcClient;
//    }
}
