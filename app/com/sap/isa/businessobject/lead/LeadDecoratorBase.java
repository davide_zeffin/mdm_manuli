package com.sap.isa.businessobject.lead;

import java.util.Iterator;
import java.util.Set;

import com.sap.isa.businessobject.DecoratorBase;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.businessobject.boi.ObjectBaseData;

/**
 * @author d028980
 *
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates.
 * To enable and disable the creation of type comments go to
 * Window>Preferences>Java>Code Generation.
 */
public class LeadDecoratorBase extends DecoratorBase {

    protected Lead lead;

    /**
     * Creates a new decorator wrapped around an order object.
     */
    public LeadDecoratorBase(Lead lead) {
        this.lead = lead;
    }

    /**
     * Only for backward compatibility with older classes. Will be removed
     * soon.
     */
    public LeadDecoratorBase() {
    }


    // methods requierd to implement ObjectBase

    /**
     * Retrieves the key for the object.
     *
     * @return The object's key
     */
    public TechKey getTechKey() {
        return lead.getTechKey();
    }

    /**
     * Sets the key for the document.
     *
     * @param key Key to be set
     */
    public void setTechKey(TechKey techKey) {
        lead.setTechKey(techKey);
    }


    /**
     *
     * The method returns an iterator over all sub objects of the BusinessObject
     *
     * @return Iterator to loop over sub objects
     *
     */
    public Iterator getSubObjectIterator() {
        return lead.getSubObjectIterator();
    }


    /**
     *
     * This method returns the handle, as an alternative key for the business object,
     * because at the creation point no techkey for the object exists. Therefore
     * maybay an alternative key is needed to identify the object in backend
     * <b>Overwrite this method if you use extension for your business object!!</b>
     *
     * return the handle of business object which is needed to identify the object
     * in the backend, if the techkey still not exists
     *
     */
     public String getHandle() {
         return lead.getHandle();
     }


     /**
      *
      * This method creates a unique handle, as an alternative key for the business object,
      * because at the creation point no techkey for the object exists. Therefore
      * maybay the handle is needed to identify the object in backend
      *
      */
     public void createUniqueHandle() {
        lead.createUniqueHandle();

     }


      /**
       * This method sets the handle, as an alternative key for the business object,
       * because at the creation point no techkey for the object exists. Therefore
       * maybay the handle is needed to identify the object in backend
       *
       * @param handle the handle of business object which identifies the object
       * in the backend, if the techkey still not exists
       *
       */
      public void setHandle(String handle) {
          lead.setHandle(handle);
      }


    /**
     *
     * The method returns an reference to a sub objects for a given tech key
     *
     * @return sub object
     *
     */
    public ObjectBaseData getSubObject(TechKey techKey) {
        return lead.getSubObject(techKey);
    }


    /**
     * This method stores arbitrary data within this Business Object
     * @param key key with which the specified value is to be associated.
     * @param value value to be associated with the specified key.
     */
    public void addExtensionData(Object key, Object name) {
        lead.addExtensionData(key,name);
    }

    /**
    * This method retrieves extension data associated with the
    * Business Object
    * @param key key with which the specified value is to be associated.
    */
    public Object getExtensionData(Object key) {
        return lead.getExtensionData(key);
    }

    /**
     * This method removes extension data from the Business Object
     */
    public void removeExtensionData(Object key) {
        lead.removeExtensionData(key);
    }


    /**
     * This method retrieves all extension data associated with the
     * Business Object
     */
    public Set getExtensionDataValues() {
        return lead.getExtensionDataValues();
    }


    /**
     * This method removes all extensions data from the Business Object
     */
    public void removeExtensionDataValues() {
      lead.removeExtensionDataValues();
      }




}
