package com.sap.isa.businessobject.lead;

import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.UserData;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.ShipTo;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.item.ItemList;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.util.MessageList;

/**
 * @author d028980
 *
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates.
 * To enable and disable the creation of type comments go to
 * Window>Preferences>Java>Code Generation.
 */
public class LeadChange extends LeadDecoratorBase {

    protected Lead lead;
    
    /**
     * Default constructor. Only used by subclasses.
     */
    protected LeadChange() {
    }

    /**
     * Creates a new instance for the given <code>Lead</code> object.
     *
     * @param order Order to decorate
     */
    public LeadChange(Lead lead) {
        super(lead);
    }
    
    /**
     * Changes the lead's status.
     *
     */
    public void changeStatus() throws CommunicationException {
        lead.changeStatus();
    }



    /**
     * Saves the lead in the backend.
     *
     */
    public void saveChanges() throws CommunicationException {
        lead.save();
    }
    
    /**
     * Clears all fields of the lead.
     */
    public void clearData() {
        lead.clearHeader();
        lead.clearItems();
    }
    
        /**
     * Clears the header fields of the lead.
     */
    public void clearHeader() {
        lead.clearHeader();
    }

    /**
     * Deletes the messages of the lead
     *
     */
     public void clearMessages() {
        lead.clearMessages();
     }

     /**
     * Clears the item list of the lead.
     */
    public void clearItems() {
        lead.clearItems();
    }


    /**
     * Deletes an Item(s) from the ordertemplate.
     *
     * @param techKeys Technical keys of the items to be deleted
    */
    public void deleteItems(TechKey[] techKeys) throws CommunicationException {
            lead.deleteItems(techKeys);
    }


    /* Destroy the data of the document (items and header) in the backend
     * representation. After a
     * call to this method, the object is in an undefined state. You have
     * to call init() before you can use it again.<br>
     * This method is normally called before removing the BO-representation
     * of the object using the BOM.
     */
     public void destroyContent()  throws CommunicationException  {
       lead.destroyContent();
     }
     
    /**
     * Retrieves the header information of the ordertemplate.
     *
     * @return The header of the order template
     */
    public HeaderSalesDocument getHeader() throws CommunicationException {
        return lead.getHeader();
    }
    
    /**
     * Returns the messages of the lead
     *
     * @return message list of the lead
     */
    public MessageList getMessageList() {
        return lead.getMessageList();
    }
    
    /**
     * Returns the shipto specified by the given technical key
     *
     * @param techkey Technical key of the shipto to be retrieved
     * @return Returns the found shipto or <code>null</code> if no
     *         element was found at the position
     */
    public ShipTo getShipTo(TechKey techKey) {
       return lead.getShipTo (techKey);
    }

    /**
     * Returns an array containing all ship tos currently stored in the
     * sales document.<br>
     * A shallow copy is peformed. Changing the data of a array element
     * will cause the data of the basket to be changed, too.
     *
     * @return Array containing <code>ShipTo</code> objects
     */
    public ShipTo[] getShipTos() {
        return lead.getShipTos();
    }

    /**
     * Returns the state of the order
     *
     */
     public boolean isvalid() {
       return lead.isValid();
     }
     
     /**
     * Returns the item of the document
     * specified by the given technical key.<br><br>
     * <b>Note -</b> This is a convenience method operating on the
     * <code>getItem</code> method of the internal <code>ItemList</code>.
     *
     * @param techKey the technical key of the item that should be
     *        retrieved
     * @return the item with the given techical key or <code>null</code>
     *         if no item for that key was found.
     */
    public ItemSalesDoc getItem(TechKey techKey) {
        return lead.getItem(techKey);
    }

    /**
     * Retrieves the header from the backend and stores the data
     * in this object.
     */
    public void getItemConfig(TechKey itemGuid) throws CommunicationException {
         lead.getItemConfig(itemGuid);
    }
    
    /**
     * Returns <code>ItemList</code> containing all items of the lead
     *
     * @return items of ordertemplate
     */
    public ItemList getItems() {
        return lead.getItems();
    }

    /**
     * Sets the header data
     *
     * @param header Header data to be set
     */
     public void setHeader(HeaderSalesDocument header) {
        lead.setHeader(header);
    }

    /**
     * Updates object representation in underlying storage.
     *
     * @deprecated This method will be replaced by <code>update(BusinessPartnerManager bupama)</code>
     * @see #update(BusinessPartnerManager bupama)
     */
    public void update() throws CommunicationException {
        BusinessPartnerManager bupama = null;
        lead.update(bupama);
    }

    /**
     * Updates object representation in underlying storage.
     */
    public void update(BusinessPartnerManager bupama) throws CommunicationException {
        lead.update(bupama);
    }

    /**
     * Updates object representation in underlying storage.
     *
     * @return <true> if update was successful; otherwise <code>false</code>
     *
     * @deprecated This method will be replaced by <code>update(BusinessPartnerManager bupama, ShopData shop)</code>
     * @see #update(BusinessPartnerManager bupama, ShopData shop)
     */
    public boolean update(ShopData shop) throws CommunicationException {
        //Dummy to be able to call the standard method
         BusinessPartnerManager bupama = null;

        lead.update(bupama, (ShopData) shop);
        return true;
    }

    /**
     * Updates object representation in underlying storage.
     *
     * @return <true> if update was successful; otherwise <code>false</code>
     */
    public void update(BusinessPartnerManager bupama, Shop shop) throws CommunicationException {
        lead.update(bupama, shop);
    }
    
    
    /**
     * Removes an Item from the ordertemplate.
     *
     * @param techKeys Technical keys of the items to be removed
     */
    public void removeItem(TechKey techKey) throws CommunicationException {
            lead.removeItem(techKey);
    }
    
    /**
     * Retrieves the header information of the order.
     */
    public void readForUpdate()
           throws CommunicationException {
        lead.readForUpdate();
    }

    /**
     * Retrieves the items' information of the ordertemplate.
     */
    public void readAllItems() throws CommunicationException {
        lead.readAllItems();
    }
    
    /**
     * Retrieves the header information of the ordertemplate.
     *
     */
    public void readOrderTemplateHeader() throws CommunicationException {
        lead.readHeader();
    }



    /**
     * Releases the lock of the order in the backend.
     *
     * @param ordr business object to be stored
     */
    public void dequeueLead() throws CommunicationException {
        lead.dequeue();
    }





}
