package com.sap.isa.businessobject.lead;


import java.io.BufferedReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import com.sap.isa.backend.boi.isacore.SalesDocumentBackend;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.UserData;
import com.sap.isa.backend.boi.isacore.lead.LeadBackend;
import com.sap.isa.backend.boi.isacore.lead.LeadData;
import com.sap.isa.businessobject.BusinessObjectHelper;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.User;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.eai.BackendException;

/**
 * @author d028980
 *
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates.
 * To enable and disable the creation of type comments go to
 * Window>Preferences>Java>Code Generation.
 */
public class Lead extends SalesDocument
                    implements LeadData {

    private boolean invalid = false;
    private String description;
    private String leadID; //GUID for SAP CRM
    private String bpID;
    private String language;
    private List products;
    private List productQuantities;
    private List productUnits;
    private List leadTexts;
    private int oldTextIndex = 0;
    private int oldProductIndex = 0;

    private String BSPAppl;
    private String BSPView;
  
    /*
    private String newProduct;
    private String newProductDescription;
    private String newText;
    */

    /**
     * Creates a new lead object without any parameters.
     */
    public Lead() {
      products = new ArrayList();
      productQuantities = new ArrayList();
	  productUnits = new ArrayList();
      leadTexts = new ArrayList();
    }
    /**
     * Clears the lead information
     */
    public void clearData(){
      description = null;
      bpID = null;
      leadID = null;
      /*
      newProduct = null;
      newProductDescription = null;
      newText = null;*/
      products.clear();
      productQuantities.clear();
	  productUnits.clear();
      leadTexts.clear();
      oldProductIndex = 0;
      oldTextIndex = 0;
    }

    public String getDescription(){
      return description;
    }

    public String getLeadID(){
      return leadID;
    }

    public String getBPID(){
      return bpID;
    }

    public void setDescription(String desc){
      description = desc;
    }

    public void setLeadID(String leadID){
      this.leadID = leadID;
    }

    public void setBPID(String bpID){
      this.bpID = bpID;
    }

    public void changeStatus()
                throws CommunicationException {
		final String METHOD = "changeStatus()";
		log.entering(METHOD);
        try {

           ((LeadBackend) getBackendService()).changeStatus(this);
           isDirty = true;
           header.setDirty(true);

        } catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        } finally {
        	log.exiting();
        }
    }

     public void changeStatusToWon()
                 throws CommunicationException {
		final String METHOD = "changeStatusToWon()";
		log.entering(METHOD);
         try {
 
            ((LeadBackend) getBackendService()).changeStatusToWon(this);
            isDirty = true;
            header.setDirty(true);
 
         } catch (BackendException ex) {
             BusinessObjectHelper.splitException(ex);
         } finally {
         	log.exiting();
         }
     }
     



    /**
     * Reads the order data from the underlying storage for the special
     * case of a change of the order.
     *
     * @param user The current user
     */
    public void readForUpdate()
                        throws CommunicationException {
		final String METHOD = "readForUpdate()";
		log.entering(METHOD);
        // write some debugging info
        try {
            // read and lock from Backend
            if (header.isDirty()) {
                ((LeadBackend) getBackendService()).readHeaderFromBackend(this, true);
                header.setDirty(false);
            }

            if (isDirty) {
                ((LeadBackend) getBackendService()).readAllItemsFromBackend(this, true);
                isDirty = false;
            }
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        } finally {
        	log.exiting();
        }
    }




    /**
     * Retrieves the item information of the lead from the backend and
     * stores the data in this object.
     *
     *
     * <b>As a side effect of this method the shiptos are also read. This
     * is not supposed to be the right behaviour and is left here only to keep
     * the clients of this class working. All clients should explicitly call
     * the <code>readShipTosFromBackend()</code> method and not rely
     * on this - we are going to change this soon.</b>
     */
    public void readAllItems() throws CommunicationException {
		final String METHOD = "readAllItems()";
		log.entering(METHOD);
        // write some debugging info
        try {
            // get all items in Backend
            //shiptos needed?
            //getBackendService().readShipTosFromBackend(this, user);
            if (isDirty) {
                getBackendService().readAllItemsFromBackend(this);
                isDirty = false;
            }
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        } finally {
        	log.exiting();
        }
    }

    /**
     * Set global data in the backend
     *
     * @return <true> if check was successful; otherwise <code>false</code>
     * @deprecated This method will be replaced by <code>setGData(ShopData shop,
     *                                                            UserData user,
     *                                                            WebCatInfo salesDocWebCat)</code>
     * @see #setGData(ShopData shop,
     *               UserData user,
     *               WebCatInfo salesDocWebCat)
     */
    public void setGData(ShopData shop) throws CommunicationException {
		final String METHOD = "setGData()";
		log.entering(METHOD);
        // write some debugging info
        if (log.isDebugEnabled()) {
            log.debug("setGData(): shop = " + shop);
        }

        try {
            ((LeadBackend) getBackendService()).setGData(this, shop);
            isDirty = true;
            header.setDirty(true);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        } finally {
        	log.exiting();
        }
    }

    /**
     * DO NOT USE, "SOLD-TO" IS NOT PASSED
     * Set global data in the backend
     * 
     * @return <true> if check was successful; otherwise <code>false</code>
     */
    public void setGData(ShopData shop, WebCatInfo salesDocWebCat) throws CommunicationException {

		final String METHOD = "setGData()";
		log.entering(METHOD);
        // write some debugging info
        if (log.isDebugEnabled()) {
            log.debug("setGData(): shop = " + shop +", salesDocWebCat = " + salesDocWebCat);
        }
		try {
	        if (salesDocWebCat == null && shop.isInternalCatalogAvailable()) {
	            throw( new IllegalArgumentException("Parameter salesDocWebCat can not be null!"));
	        }
	        this.salesDocWebCat = salesDocWebCat;
	
	        try {
	            ((LeadBackend) getBackendService()).setGData(this, shop);
	            isDirty = true;
	            header.setDirty(true);
	        }
	        catch (BackendException ex) {
	            BusinessObjectHelper.splitException(ex);
	        }
		} finally {
			log.exiting();
		}
    }

    /**
     * Saves the lead in the backend.
     *306G
     * @param lead business object to be stored
     */
    public void save() throws CommunicationException {

		final String METHOD = "save()";
		log.entering(METHOD);

        try {
            ((LeadBackend) getBackendService()).saveInBackend(this);
            isDirty = true;
            header.setDirty(true);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        } finally {
        	log.exiting();
        }
    }




    /**
     * Releases the lock of the lead in the backend.
     *
     * @param ordr business object to be stored
     */
    public void dequeue() throws CommunicationException {

		final String METHOD = "dequeue()";
		log.entering(METHOD);

        try {
            ((LeadBackend) getBackendService()).dequeueInBackend(this);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        } finally {
        	log.exiting();
        }
    }

    /**
     * Create a lead in the backend
     */
    public String  createLead(String lang)
                throws CommunicationException {
		final String METHOD = "createLead()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("language="+lang);
        try {
          language = lang;
          if(language == null)
            language = "EN";
           String result = ((LeadBackend) getBackendService()).createLead(this);
           isDirty = true;
           header.setDirty(true);
           /*if(result != null && newProduct != null) {
              products.add(newProduct);
              newProduct = null;
              productDescriptions.add(newProductDescription);
              newProductDescription = null;
              leadTexts.add(newText);
              newText = null;
           }*/
           return result;
        } catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        } finally {
        	log.exiting();
        }
        return null;
    }

    /**
     * Modify an existing lead instead of creating a new one
     */
    public boolean  modifyLead()throws CommunicationException{
		final String METHOD = "modifyLead()";
		log.entering(METHOD);
	      try {
	           boolean result = ((LeadBackend) getBackendService()).modifyLead(this);
	           isDirty = true;
	           header.setDirty(true);
	           /*if(result) {
	              products.add(newProduct);
	              newProduct = null;
	              productDescriptions.add(newProductDescription);
	              newProductDescription = null;
	              leadTexts.add(newText);
	              newText = null;
	           }*/
	           return result;
	        } catch (BackendException ex) {
	            BusinessObjectHelper.splitException(ex);
	        } finally {
	        	log.exiting();
	        }
        return false;
    }

    /* get the BackendService, if necessary */
    protected SalesDocumentBackend getBackendService()
            throws BackendException{

        synchronized (this) {
            if (backendService == null) {
                backendService =
                    (LeadBackend) bem.createBackendBusinessObject("Lead", null);
            }
        }
        return (LeadBackend) backendService;
    }

    public void addProduct(String productGUID, String quantity, String productDescription, String text)
    {
      products.add(productGUID);
      productQuantities.add(quantity);
	  productUnits.add("");
      addText(productDescription + ":");
      addText(text);
      /*
      newProduct = productGUID;
      newText = text;
      newProductDescription = productDescription;
      */
    }

	public void addProduct(String productGUID, String quantity, String unit, String productDescription, String text)
	{
	  products.add(productGUID);
	  productQuantities.add(quantity);
	  productUnits.add(unit);
	  addText(productDescription + ":");
	  addText(text);
	  /*
	  newProduct = productGUID;
	  newText = text;
	  newProductDescription = productDescription;
	  */
	}

    public void addText(String text)
    {
      if(text == null)
        return;
      try {
        BufferedReader in = new BufferedReader(
        new StringReader(text));
        String s = null;
        while ((s = in.readLine()) != null)
        {
          leadTexts.add(s);
        }
      } catch (Exception ex) {
        ex.printStackTrace();
      }
    }

    /*
    public String getNewText() {
      return newText;
    }

    public String getNewProduct() {
      return newProduct;
    }

    public String getNewProductDescription() {
      return newProductDescription;
    }*/

    public int getProductNum() {
      return products.size();
    }

    public String getProductID(int idx) {
      return (String)products.get(idx);
    }

    public String getProductQuantity(int idx) {
      return (String)productQuantities.get(idx);
    }

	public String getProductUnit(int idx) {
	  return (String)productUnits.get(idx);
	}
	
    /*
    public String getProductDescription(int idx) {
      return (String)productDescriptions.get(idx);
    }
    */

    public int getTextNum() {
      return leadTexts.size();
    }

    public String getText(int idx) {
      return (String)leadTexts.get(idx);
    }

    public String getLanguage() {
      return language;
    }

    public int getOldTextIndex()
    {
      return oldTextIndex;
    }

    public int getOldProductIndex()
    {
      return oldProductIndex;
    }

    public void UpdateOldIndex()
    {
      oldTextIndex = leadTexts.size();
      oldProductIndex = products.size();
    }

    public void setBSPAppl(String BSPAppl) {
      this.BSPAppl = BSPAppl;
    }

    public String getBSPAppl() {
      return BSPAppl;
    }

    public void setBSPView(String BSPView) {
      this.BSPView = BSPView;
    }

    public String getBSPView() {
      return BSPView;
    }
}
