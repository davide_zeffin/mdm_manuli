package com.sap.isa.businessobject;

import com.sap.isa.core.eai.BackendObjectManager;

public class SearchShiptoCommand extends BusinessObjectBase implements SearchCommand {

  private String shiptoBPartnerId;
  private String partner;
  private String name1;
  private String name2;
  private String city;
  private String zipCode;
  private String country;
  private String countryiso;
  private String region;
  private String street;
  private String houseNo;


  public SearchResult performSearch(BackendObjectManager bem) 
     		throws CommunicationException {
     			return null;
  }

  public SearchShiptoCommand() {
  }

    /**
     * Set the property partner
     *
     * @param partner
     *
     */
    public void setPartner(String partner) {
        if (partner != null) {
            this.partner = partner;
        }
    }

	/**
	 * Set the property shiptoBPartnerId
	 *
	 * @param partner
	 *
	 */
	public void setShiptoBPartnerId(String partner) {
		if (partner != null) {
			this.shiptoBPartnerId = partner;
		}
	}


    /**
     * Returns the property shiptoBPartnerId
     *
     * @return shiptoBPartnerId
     *
     */
    public String getShiptoBPartnerId() {
       return this.shiptoBPartnerId;
    }

	/**
	 * Returns the property partner
	 *
	 * @return partner
	 *
	 */
	public String getPartner() {
	   return this.partner;
	}

    /**
     * Set the property name1
     *
     * @param name1
     *
     */
    public void setName1(String name1) {
        if (name1 != null) {
            this.name1 = name1;
        }
    }

    /**
     * Returns the property name1
     *
     * @return name1
     *
     */
    public String getName1() {
       return this.name1;
    }

      /**
     * Set the property name2
     *
     * @param name2
     *
     */
    public void setName2(String name2) {
        if (name2 != null) {
            this.name2 = name2;
        }
    }

    /**
     * Returns the property name2
     *
     * @return name2
     *
     */
    public String getName2() {
       return this.name2;
    }



    /**
     * Set the property city
     *
     * @param city
     *
     */
    public void setCity(String city) {
        if (city != null) {
            this.city = city;
        }
    }

    /**
     * Returns the property city
     *
     * @return city
     *
     */
    public String getCity() {
       return this.city;
    }

    /**
     * Set the property zipCode
     *
     * @param zipCode
     *
     */
    public void setZipCode(String zipCode) {
        if (zipCode != null) {
            this.zipCode = zipCode;
        }
    }

    /**
     * Returns the property zipCode
     *
     * @return zipCode
     *
     */
    public String getZipCode() {
       return this.zipCode;
    }

      /**
     * Set the property country
     *
     * @param country
     *
     */
    public void setCountry(String country) {
        if (country != null) {
            this.country = country;
        }
    }

    /**
     * Returns the property country
     *
     * @return country
     *
     */
    public String getCountry() {
       return this.country;
    }

    /**
     * Set the property countryiso
     *
     * @param countryiso
     *
     */
    public void setCountryiso(String countryiso) {
        if (countryiso != null) {
            this.countryiso = countryiso;
        }
    }

    /**
     * Returns the property countryiso
     *
     * @return countryiso
     *
     */
    public String getCountryiso() {
       return this.countryiso;
    }

    /**
     * Set the property region
     *
     * @param region
     *
     */
    public void setRegion(String region) {
        if (region != null) {
            this.region = region;
        }
    }

    /**
     * Returns the property region
     *
     * @return region
     *
     */
    public String getRegion() {
       return this.region;
    }

    /**
     * Set the property street
     *
     * @param street
     *
     */
    public void setStreet(String street) {
        if (street != null) {
            this.street = street;
        }
    }

    /**
     * Returns the property street
     *
     * @return street
     *
     */
    public String getStreet() {
       return this.street;
    }

    /**
     * Set the property houseNo
     *
     * @param houseNo
     *
     */
    public void setHouseNo(String houseNo) {
        if (houseNo != null) {
            this.houseNo = houseNo;
        }
    }

    /**
     * Returns the property houseNo
     *
     * @return houseNo
     *
     */
    public String getHouseNo() {
       return this.houseNo;
    }


}