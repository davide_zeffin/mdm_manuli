package com.sap.isa.businessobject.ppr;

import java.util.ArrayList;

import com.sap.isa.backend.boi.isacore.ppr.PPRPartnerData;
import com.sap.isa.backend.boi.isacore.ppr.PartnerProductRangeBackend;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;




public class PartnerProductRange extends BusinessObjectBase
implements BackendAware {

  /**
     * Reference to the backend object manager.
     */
    private BackendObjectManager bem;

    /**
     * Reference to the contract backend object.
     */
    private PartnerProductRangeBackend backendService;


    public PartnerProductRange() {
    }

    /**
     * maintain API, this API will all the
     * It updates (create/update)a PPR Item for a business partner and for a collection of
     * products with extensions,
     * it does not solve the problems with duplicate product record, user
     * has to know which product exists, then use different mode
     * @import, guid, the guid of the PPR item
     * partner --the business partner which is used
     * products-- a list of products with their extensions
     */
    public boolean maintainPPRItem (String guid,
                                 PPRPartnerData partner,
                                 ArrayList products){
		final String METHOD = "maintainPPRItem()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("maintainPPRItem: guid="+guid+" partner="+partner);
        boolean maintainSuccessful = false;
        try {
            maintainSuccessful = this.getBackendService()
                .maintainPPRItem(guid,partner,products);
        } catch(BackendException e) {
            log.debug("Error in maintaining the PPRs " , e);
        }	
        log.exiting();
        return  maintainSuccessful;

    }


    /**
     * Propose API, it plays a role of search API
     * This API searches through all the PPRs and PPR items to see if a PPR item
     * already exists for a given business partner, if exist, then it returns
     * a list of products as well as extension fields, and true
     * if not, it returns false, and the product list and extension fileds are
     * empty
     * criteria --usually the business partner which is used
     * products-- a list of product with their extensions
     */
    public ArrayList searchPPRItems(String bpguid,
                                    ArrayList productguids) {

		final String METHOD = "searchPPRItems()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("bpguid="+bpguid); 
        PPRSearchCriteria criteria = new PPRSearchCriteria();
        criteria.setBusinessPartnerGUID(bpguid);
        ArrayList products = new ArrayList();
        ArrayList productextfield = new ArrayList();
        boolean searchSuccessful = false;

        for(int i=0; i<productguids.size(); i++) {
            PPRProduct product = new PPRProduct((String)productguids.get(i),
                                                null);
            products.add(product);
        }

        try {
            searchSuccessful = this.getBackendService()
               .proposePPRItems(criteria,null,products,productextfield);
            if(!searchSuccessful)
               log.debug("Search was NOT successful at the backend");
        } catch(BackendException e) {
            log.debug("Error in fetching the Product Extension fields " , e);
        }
        log.exiting();
        
        return productextfield;
    }

    /**
     * Delete the entries in an PPR item for a business partner
     * partner --the business partner which is used
     * products-- a list of products
     */
    public boolean deletePPRItem (PPRPartnerData partner,
                                 ArrayList products){
		final String METHOD = "deletePPRItem()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("partner="+partner); 
        boolean deleteSuccessful = false;
        try {
            deleteSuccessful = this.getBackendService()
                .deletePPRItem(partner,products);
        } catch(BackendException e) {
            log.debug("Error in deleting the PPRs " , e);
        }
        log.exiting();
        return  deleteSuccessful;

    }

    /**
       *
       */
    public void setBackendObjectManager(BackendObjectManager bem) {
        this.bem = bem;
    }

    /**
     *
     */
    private PartnerProductRangeBackend getBackendService() throws BackendException {
        if (backendService == null) {
            try {
                // get the Backend from the Backend Manager
                backendService = (PartnerProductRangeBackend) bem
                    .createBackendBusinessObject("PartnerProductRange",null);

            }
            catch (Exception ex) {
                if(log.isDebugEnabled()) {
                    log.debug("Exception occured in getBackendService():",
                                 ex);
                }
                throw(new BackendException(ex));
            }
        }
        return backendService;
    }

}
