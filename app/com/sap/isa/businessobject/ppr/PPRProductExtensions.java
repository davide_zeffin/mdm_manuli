package com.sap.isa.businessobject.ppr;


/**
 * Title:        Product Avaliability vs Traffic Light Project
 * Description:  This project covers the customization of product availability by using partner product range (PPR) functionality in CRM as well as status rendering in Channel Commerce Hub via traffic lights. The maintenance of product availability is 'empirical based' for channel partners, and is maintained through the "Shared manufacturer catalog" hosted by the brand owner. In the brand-owner web shop, the customer sees the list of partners who can/cannot satisfy the list of products in her basket and this is signaled visually as Traffic lights.
 * Copyright:    Copyright (c) 2002
 * Company:      SAP Labs LLC, Palo Alto, California
 * @author
 * @version 1.0
 */
import com.sap.isa.backend.boi.isacore.ppr.PPRProductExtensionData;

public class PPRProductExtensions implements PPRProductExtensionData {
  private String processingMode;
  private String pPRType;
  private String GUID;
  private String handle;
  private String referenceType;
  private String productId;
  private String productType;
  private String productRefGuid;
  private String productArea;
  private String baseUOM;
  private String salesUOM;

  private int leadsTime;
  private float maxQuantity;

  /**
   * To indicate if the extension filed has been changed at front end
   */
  private boolean changable;

  /**
   * default constructor
   */
  public PPRProductExtensions() {
  }


  /**
   * Constructor with product ID and extension fields
   */
  public PPRProductExtensions (String guid, int lTime, float mQuantity) {
    productRefGuid = guid;
    leadsTime = lTime;
    maxQuantity = mQuantity;
  }

  public PPRProductExtensions (String pId, String guid,
                              int lTime, float mQuantity) {
    productRefGuid = guid;
    productId= pId;
    leadsTime = lTime;
    maxQuantity = mQuantity;
  }

  public PPRProductExtensions (String pId, String guid,
                              int lTime, float mQuantity,
                              String bUOM) {
    productRefGuid = guid;
    productId= pId;
    leadsTime = lTime;
    maxQuantity = mQuantity;
    baseUOM = bUOM;
  }
   /**
  * Processing type
  */
  public String getProcessingMode(){
    return processingMode;
  }

  /**
   * The PPR type
   */
  public String getPPRType(){
    return pPRType;
  }

  /**
   * The GUID to track of
   */
  public String getGUID(){
    return GUID;
  }


  /**
   * Handle
   */
  public String getHandle(){
    return handle;
  }

  /**
   * Reference Type - Product / Catalog Area
   */
  public String getReferenceType(){
    return referenceType;
  }

  /**
   * Product ID, not the GUID
   */
  public String getProductId(){
    return productId;
  }

  /**
   * Product Type
   */
  public String getProductType(){
    return productType;
  }


  /**
   * Product Refer GUID
   */
   public String getProductRefGuid(){
    return productRefGuid;
   }

   /**
    * Product Area
    */
   public String getProductArea(){
    return productArea;
   }

   /**
    * product base unit of measure
    */
   public String getBaseUOM(){
    return baseUOM;
   }

   /**
    * product sales unit of measure
    */
   public String getSalesUOM(){
    return salesUOM;
   }

   public void setBaseUOM(String baseUOM) {
    this.baseUOM = baseUOM;
   }

  public void setGUID(String GUID) {
    this.GUID = GUID;
  }
  public void setHandle(String newHandle) {
    this.handle = newHandle;
  }
  public void setReferenceType(String refType){
    referenceType = refType;
  }
  public void setPPRType(String PPRType) {
    this.pPRType = PPRType;
  }
  public void setProcessingMode(String pMode) {
    this.processingMode = pMode;
  }
  public void setProductArea(String pArea) {
    this.productArea = pArea;
  }
  public void setProductId(String pId) {
    this.productId = pId;
  }
  public void setProductRefGuid(String pRefGuid) {
    this.productRefGuid = pRefGuid;
  }

  public void setProductType(String pType) {
    this.productType = pType;
  }

  public void setSalesUOM(String sUOM) {
    this.salesUOM = sUOM;
  }

  public int getLeadsTime() {
    return leadsTime;
  }

  public void setLeadsTime(int newTime) {
    this.leadsTime = newTime;
  }

  public void setMaxQuantity(float newQuantity) {
    this.maxQuantity = newQuantity;
  }

  public float getMaxQuantity() {
    return this.maxQuantity;
  }

  /**
   * To get if the PPR Product extension filed is changed
   */
  public boolean isChanged () {
    return changable;
  }

    /**
     * To set if the PPR product extension field changed
     */
  public void setChangable (boolean changed) {
    changable = changed;
  }
}