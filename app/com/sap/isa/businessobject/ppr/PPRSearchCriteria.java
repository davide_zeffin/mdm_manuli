package com.sap.isa.businessobject.ppr;

/**
 * Title:        Product Avaliability vs Traffic Light Project
 * Description:  This project covers the customization of product availability
 * by using partner product range (PPR) functionality in CRM as well as status
 * rendering in Channel Commerce Hub via traffic lights. The maintenance of
 * product availability is 'empirical based' for channel partners, and is
 * maintained through the "Shared manufacturer catalog" hosted by the brand o
 * wner. In the brand-owner web shop, the customer sees the list of partners
 * who can/cannot satisfy the list of products in her basket and this is
 * signaled visually as Traffic lights.
 * Copyright:    Copyright (c) 2002
 * Company:      SAP Labs LLC, Palo Alto, California
 * @author
 * @version 1.0
 */
import java.util.ArrayList;

import com.sap.isa.backend.boi.isacore.ppr.PPRSearchCriteriaData;

public class PPRSearchCriteria implements PPRSearchCriteriaData{

  private String bpGuid;
  private String productGuid;
  private String pprType;
  private ArrayList pprStatuses;

  public PPRSearchCriteria() {
  }

      /**
     * business partner GUID
     */
     public void setBusinessPartnerGUID(String guid){
      bpGuid = guid;
     }

     /**
      * Product GUID
      */
     public void setProductGUID(String guid){
      productGuid = guid;
     }

     /**
      * PPR type
      */
     public void setPPRType(String type){
      pprType = type;
     }

     /**
      * statuses
      */
     public void setPPRStatuses(ArrayList statuses) {
        pprStatuses = statuses;
     }


  public String getBusinessPartnerGUID() {
    return bpGuid;
  }

  public ArrayList getPPRStatuses() {
    return pprStatuses;
  }

  public String getPPRType() {
    return pprType;
  }


  public String getProductGUID() {
    return productGuid;
  }
}