package com.sap.isa.businessobject.ppr;

/**
 * Title:        Product Avaliability vs Traffic Light Project
 * Description:  This project covers the customization of product availability
 * by using partner product range (PPR) functionality in CRM as well as status
 * rendering in Channel Commerce Hub via traffic lights. The maintenance of
 * product availability is 'empirical based' for channel partners, and is
 * maintained through the "Shared manufacturer catalog" hosted by the brand
 * owner. In the brand-owner web shop, the customer sees the list of partners
 * who can/cannot satisfy the list of products in her basket and this is
 * signaled visually as Traffic lights.
 * Copyright:    Copyright (c) 2002
 * Company:      SAP Labs LLC, Palo Alto, California
 * @author
 * @version 1.0
 */
import com.sap.isa.backend.boi.isacore.ppr.PartnerProductRangeExtensionData;

public class PartnerProductRangeExtension implements
                                      PartnerProductRangeExtensionData{
  /**
   * This class can be enhance to add more attriibutes
   */
  private int leadsTime;
  private float maxQuantity;
  public PartnerProductRangeExtension() {

  }

  public int getLeadsTime() {
    return leadsTime;
  }

  public void setLeadsTime(int leadsTime) {
    this.leadsTime = leadsTime;
  }

  public void setMaxQuantity(float maxQuantity) {
    this.maxQuantity = maxQuantity;
  }

  public float getMaxQuantity() {
    return maxQuantity;
  }


}