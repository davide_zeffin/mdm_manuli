package com.sap.isa.businessobject.ppr;

/**
 * Title:        Product Avaliability vs Traffic Light Project
 * Description:  This project covers the customization of product availability by using partner product range (PPR) functionality in CRM as well as status rendering in Channel Commerce Hub via traffic lights. The maintenance of product availability is 'empirical based' for channel partners, and is maintained through the "Shared manufacturer catalog" hosted by the brand owner. In the brand-owner web shop, the customer sees the list of partners who can/cannot satisfy the list of products in her basket and this is signaled visually as Traffic lights.
 * Copyright:    Copyright (c) 2002
 * Company:      SAP Labs LLC, Palo Alto, California
 * @author
 * @version 1.0
 */
import com.sap.isa.backend.boi.isacore.ppr.PPRPartnerData;
import com.sap.isa.businessobject.BusinessObjectBase;

public class PPRPartner extends BusinessObjectBase implements PPRPartnerData{
  private String pprType;
  private String GUID;
  private String partnerId;
  private String partnerType;
  private String targetGroupId;
  private String bupaRefGuid;

  public PPRPartner() {
  }

    /**
   * The PPR type
   */
  public String getPPRType(){
    return pprType;
  }

  /**
   * The GUID to track of
   */
  public String getGUID(){
    return GUID;
  }

  /**
   * Partner ID, not the GUID
   */
  public String getPartnerId(){
    return partnerId;
  }

  /**
   * Partner Type
   */
  public String getPartnerType(){
    return partnerType;
  }

  /**
   * Target Group ID
   */
   public String getTargetGroupId(){
    return targetGroupId;
   }

  /**
   * Partnet Refer GUID
   */
   public String getBupaRefGuid(){
    return bupaRefGuid;
   }

  public void setBupaRefGuid(String bupaRefGuid) {
    this.bupaRefGuid = bupaRefGuid;
  }

  public void setGUID(String GUID) {
    this.GUID = GUID;
  }

  public void setPartnerId(String partnerId) {
    this.partnerId = partnerId;
  }

  public void setPartnerType(String partnerType) {
    this.partnerType = partnerType;
  }

  public void setPPRType(String PPRType) {
    this.pprType = PPRType;
  }

  public void setTargetGroupId(String targetGroupId) {
    this.targetGroupId = targetGroupId;
  }
}