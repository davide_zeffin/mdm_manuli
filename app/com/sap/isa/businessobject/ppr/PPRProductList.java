package com.sap.isa.businessobject.ppr;

/**
 * Title:        Product Avaliability vs Traffic Light Project
 * Description:  This project covers the customization of product availability by using partner product range (PPR) functionality in CRM as well as status rendering in Channel Commerce Hub via traffic lights. The maintenance of product availability is 'empirical based' for channel partners, and is maintained through the "Shared manufacturer catalog" hosted by the brand owner. In the brand-owner web shop, the customer sees the list of partners who can/cannot satisfy the list of products in her basket and this is signaled visually as Traffic lights.
 * Copyright:    Copyright (c) 2002
 * Company:      SAP Labs LLC, Palo Alto, California
 * @author
 * @version 1.0
 */
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.sap.isa.backend.boi.isacore.ppr.PPRProductExtensionData;
import com.sap.isa.core.Iterable;
import com.sap.isa.core.logging.IsaLocation;

public class PPRProductList implements Iterable{
    private List items;
    private IsaLocation log;

    /**
     * Creates a new <code>ItemList</code> object.
     */
    public PPRProductList() {
        items = new ArrayList();
        // Remember to change the cast in the clone() method when you
        // switch to another implementor of List!!
        log = IsaLocation.getInstance(this.getClass().getName());
    }

    public void addProduct(PPRProductExtensionData product) {
      items.add(product);
    }

    /**
     * Returns the number of elements in this list.
     */
    public int size() {
        return items.size();
    }

    /**
     * Returns true if this list contains no data.
     */
    public boolean isEmpty() {
        return items.isEmpty();
    }

    /**
     * Returns an iterator over the elements contained in the list
     */
    public Iterator iterator() {
        return items.iterator();
    }
}