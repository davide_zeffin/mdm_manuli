package com.sap.isa.businessobject.ppr;

/**
 * Title:        Product Avaliability vs Traffic Light Project
 * Description:  This project covers the customization of product availability
 * by using partner product range (PPR) functionality in CRM as well as status
 * rendering in Channel Commerce Hub via traffic lights. The maintenance of
 * product availability is 'empirical based' for channel partners, and is
 * maintained through the "Shared manufacturer catalog" hosted by the brand
 * owner. In the brand-owner web shop, the customer sees the list of partners
 *  who can/cannot satisfy the list of products in her basket and this is
 *  signaled visually as Traffic lights.
 * Copyright:    Copyright (c) 2002
 * Company:      SAP Labs LLC, Palo Alto, California
 * @author
 * @version 1.0
 */
import com.sap.isa.backend.boi.isacore.ppr.PartnerProductRangeHeaderData;

public class PartnerProductRangeHeader implements PartnerProductRangeHeaderData{
  private String aGUID;
  private String id;
  private String type;
  private String handle;
  private String processingMode;
  private String pPRStatus;
  private boolean exclusive;

  public PartnerProductRangeHeader() {
  }

    /**
   * The name of the PPR GUID
   */
  public String getGUID(){
    return aGUID;
  }

  public void setGUID(String guid){
    aGUID = guid;
  }
  /**
   * The id of the PPR
   */
  public String getId(){
    return id;
  }

  public void setId(String aId) {
      this.id = aId;
  }
  /**
   * The type of the PPR
   */
  public String getType(){
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }
  /**
   * The handle of the PPR item
   */
  public String getHandle(){
    return handle;
  }

  /**
   * The processing mode of the PPR type, create, update and delete
   */
  public String getProcessingMode(){
    return processingMode;

  }

  /**
   * The PPR Status
   */
  public String getPPRStatus() {
    return pPRStatus;
  }

  /**
   * The exclusion of the flag, the flag is used for all
   */
  public boolean getExclusionHeader() {
    return true;
  }

  /**
   * The exclusion of the flag, the flag is used for all
   */
  public void setExclusionHeader(boolean excl) {
    exclusive = excl;
  }

}