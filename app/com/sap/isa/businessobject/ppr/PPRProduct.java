package com.sap.isa.businessobject.ppr;

/**
 * Title:        Product Avaliability vs Traffic Light Project
 * Description:  This project covers the customization of product availability by using partner product range (PPR) functionality in CRM as well as status rendering in Channel Commerce Hub via traffic lights. The maintenance of product availability is 'empirical based' for channel partners, and is maintained through the "Shared manufacturer catalog" hosted by the brand owner. In the brand-owner web shop, the customer sees the list of partners who can/cannot satisfy the list of products in her basket and this is signaled visually as Traffic lights.
 * Copyright:    Copyright (c) 2002
 * Company:      SAP Labs LLC, Palo Alto, California
 * @author
 * @version 1.0
 */
import com.sap.isa.backend.boi.isacore.ppr.PPRProductData;


public class PPRProduct implements PPRProductData{


  public static String REFERENCE_TYPE_PRODUCT ="1";
  public static String REFERENCE_TYPE_CATALOGAREA ="72";

  private String processingMode;
  private String pPRType;
  private String GUID;
  private String handle;
  private String referenceType;
  private String productId;
  private String productType;
  private String productRefGuid;
  private String productArea;
  private String baseUOM;
  private String salesUOM;

  /**
   * Constructor
   */
  public PPRProduct(String aProductId) {
    productId = aProductId;
  }

  /**
   * Constructor with product ID and product GUID
   */
  public PPRProduct (String guid, String pId) {
    productId = pId;
    productRefGuid = guid;
  }

   /**
  * Processing type
  */
  public String getProcessingMode(){
    return processingMode;
  }

  /**
   * The PPR type
   */
  public String getPPRType(){
    return pPRType;
  }

  /**
   * The GUID to track of
   */
  public String getGUID(){
    return GUID;
  }


  /**
   * Handle
   */
  public String getHandle(){
    return handle;
  }

  /**
   * Reference Type - Product / Catalog Area
   */
  public String getReferenceType(){
    return referenceType;
  }

  /**
   * Product ID, not the GUID
   */
  public String getProductId(){
    return productId;
  }

  /**
   * Product Type
   */
  public String getProductType(){
    return productType;
  }


  /**
   * Product Refer GUID
   */
   public String getProductRefGuid(){
    return productRefGuid;
   }

   /**
    * Product Area
    */
   public String getProductArea(){
    return productArea;
   }

   /**
    * product base unit of measure
    */
   public String getBaseUOM(){
    return baseUOM;
   }

   /**
    * product sales unit of measure
    */
   public String getSalesUOM(){
    return salesUOM;
   }

   public void setBaseUOM(String baseUOM) {
    this.baseUOM = baseUOM;
   }

  public void setGUID(String GUID) {
    this.GUID = GUID;
  }
  public void setHandle(String handle) {
    this.handle = handle;
  }
  public void setReferenceType(String refType){
    referenceType = refType;
  }
  public void setPPRType(String PPRType) {
    this.pPRType = PPRType;
  }
  public void setProcessingMode(String processingMode) {
    this.processingMode = processingMode;
  }
  public void setProductArea(String productArea) {
    this.productArea = productArea;
  }
  public void setProductId(String productId) {
    this.productId = productId;
  }
  public void setProductRefGuid(String productRefGuid) {
    this.productRefGuid = productRefGuid;
  }

  public void setProductType(String productType) {
    this.productType = productType;
  }

  public void setSalesUOM(String salesUOM) {
    this.salesUOM = salesUOM;
  }

}