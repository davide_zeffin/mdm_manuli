package com.sap.isa.businessobject.ppr;

/**
 * Title:        Product Avaliability vs Traffic Light Project
 * Description:  This project covers the customization of product availability
 * by using partner product range (PPR) functionality in CRM as well as status
 * rendering in Channel Commerce Hub via traffic lights. The maintenance of
 * product availability is 'empirical based' for channel partners, and is
 * maintained through the "Shared manufacturer catalog" hosted by the brand
 * owner. In the brand-owner web shop, the customer sees the list of partners
 * who can/cannot satisfy the list of products in her basket and this is
 * signaled visually as Traffic lights.
 * Copyright:    Copyright (c) 2002
 * Company:      SAP Labs LLC, Palo Alto, California
 * @author
 * @version 1.0
 */

import com.sap.isa.businessobject.GenericBusinessObjectManager;
import com.sap.isa.core.businessobject.BackendAware;

public class PPRBusinessObjectManager extends GenericBusinessObjectManager
    implements BackendAware {

    protected PartnerProductRange ppr = null;

    /**
     * Constant containing the name of this BOM
     */
    public static final String PPR_BOM = "PPR-BOM";

    /**
     * Create a new instance of the object.
     */
    public PPRBusinessObjectManager() {
    }

    /**
     * Creates a new user object. If such an object was already created, a
     * reference to the old object is returned and no new object is created.
     *
     * @return referenc to a newly created or already existend user object
     */
    public synchronized PartnerProductRange createPPR() {
		final String METHOD = "createPPR()";
		log.entering(METHOD);
    	if (ppr == null) {
          ppr = (PartnerProductRange)createBusinessObject(
                                                    PartnerProductRange.class);
          //ppr.init();
        }
        log.exiting();
        return ppr;
    }

    /**
     * Returns a reference to an existing user object.
     *
     * @return reference to user object or null if no object is present
     */
    public PartnerProductRange getPPR() {
      return (PartnerProductRange) getBusinessObject(PartnerProductRange.class);
    }

    /**
     * Release the references to created user object.
     */
    public synchronized void releasePPR() {
        releaseBusinessObject(PartnerProductRange.class);
    }




}