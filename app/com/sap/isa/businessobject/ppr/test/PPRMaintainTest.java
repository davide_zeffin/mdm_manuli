package com.sap.isa.businessobject.ppr.test;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Properties;

import com.sap.isa.businessobject.ppr.PPRPartner;
import com.sap.isa.businessobject.ppr.PPRProductExtensions;
import com.sap.isa.businessobject.ppr.PartnerProductRange;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.core.eai.BackendObjectManagerImpl;
import com.sap.isa.core.init.StandaloneHandler;
import com.sap.isa.core.xcm.ConfigContainer;
import com.sap.isa.core.xcm.ExtendedConfigManager;


public class PPRMaintainTest {

    public PPRMaintainTest() {
    }

    /**
     * Test the eai framework
     */
    public static void main(String[] args) throws Exception{


        StandaloneHandler standaloneHandler = new StandaloneHandler();

        standaloneHandler.setBootStrapInitConfigPath("/WEB-INF/xcm/sap/system/bootstrap-config.xml");
        standaloneHandler.setInitConfigPath("/WEB-INF/xcm/sap/system/init-config.xml");
        standaloneHandler.setClassNameMessageResources("com.sap.isa.ISAResources");
        standaloneHandler.setBaseDir("C:/Perforce_3100/sap/ESALES_InternetSales/40_SP_COR/src/_sharedcatalog/web/");
        Hashtable parameters = new Hashtable();
        standaloneHandler.setParameter(parameters);
        parameters.put("customer.config.path.xcm.config.isa.sap.com",
                       "/WEB-INF/xcm/customer/configuration");
        parameters.put("scenario.xcm.config.isa.sap.com",
                       "CRM_Q4C_705");
        try {
            standaloneHandler.initialize();
        } catch (Exception ex) {
            ex.printStackTrace();
        }


        PartnerProductRange ppr = new PartnerProductRange();
        // ConfigContainer cc = new ConfigContainer();
        // cc.

        Properties props = new Properties();
        ExtendedConfigManager mgr = ExtendedConfigManager.getExtConfigManager();
        ConfigContainer config = mgr.getConfig("CRM_Q4C_705");
        props.setProperty(SessionConst.XCM_CONF_KEY, config.getConfigKey());
        BackendObjectManager bm = new BackendObjectManagerImpl(props);
        System.out.println(bm);


        ppr.setBackendObjectManager(bm);

        String pprguid = null;
        PPRPartner partner = new PPRPartner();
        partner.setBupaRefGuid("3DF575018F5E4379E10000000A1145AC"); // 408317



        ArrayList products = new ArrayList();

        // First product
        PPRProductExtensions pe =
            new PPRProductExtensions();
        pe.setProductRefGuid("3E958D9187F90DC4E10000000A1145B2");
                                     // MONITOR CAtalog area
        pe.setLeadsTime(1);
        pe.setMaxQuantity(10);

        pe.setReferenceType("72");
        products.add(pe);

        pe = new PPRProductExtensions("3DB0E2DF87685AF3E10000000A114606",
                                      2,90); // Product ABC
        pe.setReferenceType("1");
        products.add(pe);

        boolean isSuccessful = ppr.maintainPPRItem(pprguid,
                                                   partner, products);
        System.out.println("Is succsessful " + isSuccessful);

    }

}
