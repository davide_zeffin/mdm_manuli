package com.sap.isa.businessobject.ppr.test;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Properties;

import com.sap.isa.businessobject.ppr.PPRPartner;
import com.sap.isa.businessobject.ppr.PPRProduct;
import com.sap.isa.businessobject.ppr.PartnerProductRange;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.core.eai.BackendObjectManagerImpl;
import com.sap.isa.core.init.StandaloneHandler;
import com.sap.isa.core.xcm.ConfigContainer;
import com.sap.isa.core.xcm.ExtendedConfigManager;


public class PPRDeleteTest {

    public PPRDeleteTest() {
    }

    /**
     * Test the eai framework
     */
    public static void main(String[] args) throws Exception{


        StandaloneHandler standaloneHandler = new StandaloneHandler();

        standaloneHandler.setBootStrapInitConfigPath("/WEB-INF/xcm/sap/system/bootstrap-config.xml");
        standaloneHandler.setInitConfigPath("/WEB-INF/xcm/sap/system/init-config.xml");
        standaloneHandler.setClassNameMessageResources("com.sap.isa.ISAResources");
        standaloneHandler.setBaseDir("C:/Perforce_3100/sap/ESALES_InternetSales/40_SP_COR/src/_sharedcatalog/web/");
        Hashtable parameters = new Hashtable();
        standaloneHandler.setParameter(parameters);
        parameters.put("customer.config.path.xcm.config.isa.sap.com",
                       "/WEB-INF/xcm/customer/configuration");
        parameters.put("scenario.xcm.config.isa.sap.com",
                       "CRM_Q4C_705");
        try {
            standaloneHandler.initialize();
        } catch (Exception ex) {
            ex.printStackTrace();
        }


        PartnerProductRange ppr = new PartnerProductRange();

        Properties props = new Properties();
        ExtendedConfigManager mgr = ExtendedConfigManager.getExtConfigManager();
        ConfigContainer config = mgr.getConfig("CRM_Q4C_705");
        props.setProperty(SessionConst.XCM_CONF_KEY, config.getConfigKey());
        BackendObjectManager bm = new BackendObjectManagerImpl(props);
        System.out.println(bm);


        ppr.setBackendObjectManager(bm);

        PPRPartner partner = new PPRPartner();
        partner.setBupaRefGuid("3DF575018F5E4379E10000000A1145AC"); // 408317

        ArrayList products = new ArrayList();

        // First product
        PPRProduct pe =
            new PPRProduct("3E958D9187F90DC4E10000000A1145B2",null);
        // MONITOR CAtalog area
        products.add(pe);

        pe = new PPRProduct("3DB0E2DF87685AF3E10000000A114606",
                            null);// Product ABC
        products.add(pe);

        boolean successful = ppr.deletePPRItem(partner,products);
        System.out.println("Is Success " + successful);

    }

}
