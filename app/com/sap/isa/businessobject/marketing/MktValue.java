/*****************************************************************************
    Class:        MktValue
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      15 Februar 2001
    Version:      0.1
*****************************************************************************/

package com.sap.isa.businessobject.marketing;

import com.sap.isa.backend.boi.isacore.marketing.MktValueData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.businessobject.ObjectBase;

/**
 *
 * The MktValue describe a value which can used in the MktAttribute class
 * which will represent answers in the questionary to define user profiles.
 *
 */
public class MktValue
        extends ObjectBase
        implements MktValueData {

    /*
       see setter methods for the documentation of private variables
    */
    private String id;
    private String description;
    private boolean isDefaultValue = false;

    /**
     *  Constructor.
     *
     */
    public  MktValue() {
        setTechKey(TechKey.EMPTY_KEY);
    }


    /**
     * Constructor.
     *
     * @param techKey TechKey for the value
     * @param id the id of the value
     * @param description the description to the value
     *
     */
    public  MktValue(TechKey techKey, String id, String description) {
        setTechKey(techKey);
        this.id = id;
        this.description = description;
    }


    /**
     * Constructor.
     *
     * @param techKey TechKey for the value
     * @param description the description to the value
     *
     */
    public  MktValue(TechKey techKey, String description) {
        setTechKey(techKey);
        id = techKey.toString();
        this.description = description;
    }



    /**
     * Set the property defaultValue
     *
     * @param defaultValue
     *
     */
    public void setDefaultValue(boolean defaultValue) {
        this.isDefaultValue = defaultValue;
    }


    /**
     * Returns the property defaultValue
     *
     * @return defaultValue
     *
     */
    public boolean isDefaultValue() {
       return this.isDefaultValue;
    }



    /**
     *  Get the description of the value
     *
     *  @param description Description of the value
     */
    public String getDescription() {
        return description;
    }


    /**
     *  Set the description of the value
     *
     *  @param description Description of the value
     */
    public void setDescription(String description) {
        this.description = description;
    }


    /**
     * Returns the id of the value
     *
     * @return The id is used to identify the value in the front- and in
     *         backend.
     */
    public String getId() {
        return id;
    }


    /**
     * Set the id of the value
     *
     * @param id The id is used to identify the value in the front- and
     *        in backend.
     */
    public void setId(String id) {
        this.id = id;
    }



    /**
     *
     * returns the object as string
     *
     * @return String which contains all fields of the object
     *
     */
    public String toString( ) {
        String str =  "techKey: " + getTechKey().toString() + "\n" +
                      "id: " + id + "\n" +
                      "description: " + description + "\n";
        return str;
    }


}



