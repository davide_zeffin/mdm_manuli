/*****************************************************************************
    Class:        PersonalizedRecommendation
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      March 2001
    Version:      1.0

    $Revision: #2 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.businessobject.marketing;

import com.sap.isa.backend.boi.isacore.marketing.PersonalizedRecommendationData;
import com.sap.isa.businessobject.ProductList;

/**
 *
 * PersonalizedRecommendation is a product list with the personlized product
 * recommendations
 *
 * @author  Wolfgang Sattler
 * @version 1.0
 *
 */
public class PersonalizedRecommendation extends ProductList
        implements PersonalizedRecommendationData{


}
