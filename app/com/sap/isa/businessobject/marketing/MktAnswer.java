/*****************************************************************************
    Class:        MktAnswer
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      15 Februar 2001
    Version:      0.1
*****************************************************************************/
package com.sap.isa.businessobject.marketing;

import com.sap.isa.core.TechKey;


/**
 *
 * The MktAnswer represent an answer for one question. It's the combination
 * between an chosen and an allowed value of an attribute.
 * The object have further properties which are relevant for the UI-Layer
 *
 **/
public class MktAnswer {

    private boolean  isChosen;
    private boolean  isFreeValue;
    private int      dataType;
    private MktValue value; // reference to the value

    /**
     *
     * Constructor to create an answer from a value and further informations
     *
     * @param value reference to MktValue object
     * @param isChosen value is found in chosen values
     * @param isFreeValue value isn't in the allowed value list
     * @param dataType the data type of the value
     *
     */
    public MktAnswer(MktValue value,
                     boolean isChosen,
                     boolean isFreeValue,
                     int dataType) {
        this.value = value;
        this.isChosen = isChosen;
        this.isFreeValue = isFreeValue;
        this.dataType = dataType;
    }


    /**
     * Returns if the value is chosen
     *
     * @return true if the value is chosen
     */
    public boolean isChosen() {
        return isChosen;
    }


    /**
     * Set isChosen flag of a value
     *
     * @param isChosen shows that this value is chosen
     */
    public void setChosen(boolean isChosen){
        this.isChosen = isChosen;
    }


    /**
     *  Get the description of the answer
     *
     *  @param description Description of the answer
     */
    public String getDescription() {
        return value.getDescription();
    }


    /**
     *  Set the description of the answer
     *
     *  @param description Description of the answer
     */
    public void setDescription(String description) {
        value.setDescription(description);
    }


    /**
     * Returns if value is not an allowed value
     *
     * @return flag if value is not an allowed value
     */
    public boolean isFreeValue() {
        return isFreeValue ;
    }


    /**
     * Set isFreeValue flag of a value
     *
     * @param isFreeValue: shows that this value is not in the allowed values of the attribute
     */
    public void setFreeValue(boolean isFreeValue){
        this.isFreeValue = isFreeValue;
    }


    /**
     * Returns the id of the value
     *
     * @return The id is used to identify the value in the front- and in
     *         backend.
     */
    public String getId() {
        return value.getId();
    }


    /**
     * Set the id of the value
     *
     * @param id The id is used to identify the value in the front- and
     *        in backend.
     */
    public void setId(String id) {
        value.setId(id);
    }


    /**
     * Returns the techKey of the value
     *
     * @return The techKey is used to identify the value in the front- and in
     *         backend.
     */
    public TechKey getTechKey() {
        return value.getTechKey();
    }



}


