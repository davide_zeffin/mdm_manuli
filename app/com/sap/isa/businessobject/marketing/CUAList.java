/*****************************************************************************
    Class:        CUAList
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      May 2001
    Version:      1.0

    $Revision: #4 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.businessobject.marketing;

import java.util.ArrayList;
import java.util.Iterator;

import com.sap.isa.backend.boi.isacore.marketing.CUAListData;
import com.sap.isa.backend.boi.isacore.marketing.CUAProductData;
import com.sap.isa.businessobject.ProductList;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.catalog.webcatalog.WebCatItemList;
import com.sap.isa.core.TechKey;

/**
 *
 * The CUAList class holds all CUAProducts which belongs to one product.
 * The techKey of the object contains the techKey of the product.
 *
 */
public class CUAList extends ProductList
        implements CUAListData {

    /**
     * types of WebCatItemList
     */
     public final static int CV_LISTTYPE_CUA = 5;
  
     private String productId = "";


    /**
     * Constructor
     *
     * @param relatedProduct techKey of the product
     *
     */
    public CUAList(TechKey relatedProduct) {
        setTechKey(relatedProduct);
    }


    /**
     * Constructor to create a CUAList from a webCatItemList with the given type
     *
     */
    public CUAList(TechKey relatedProduct, ArrayList itemList, String type) {

        createFromItemList(relatedProduct, itemList, type);
    }


    /**
     * Constructor
     *
     * @param techKey of product
     *
     */
    public CUAList() {
    }


    /**
     * create a CUA product
     *
     * @param techKey         techKey of the product
     * @param relatedProduct  techKey of the related product
     * @param type            type of CUA
     *
     */
    public CUAProductData createProduct(TechKey techKey, TechKey relatedProduct, String type) {
        return new CUAProduct(techKey,relatedProduct,type);

    }


    /**
     *
     * create a WebCatItemlist for all entries in the list with the given type.
     *
     *
     * @param catalog A reference to the catalog which will enhance the product
     *        data
     * @param cuaType CUAType which will be used for the list
     *
     * @return the <code>WebCatItemList</code>
     *
     */
    public WebCatItemList createWebCatItemList(WebCatInfo catalog, String cuaType) {
		final String METHOD = "createWebCatItemList()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("cusType="+cuaType);
        WebCatItemList itemList = new WebCatItemList(catalog);
        itemList.setListType(CV_LISTTYPE_CUA);

        Iterator i = iterator();

        while (i.hasNext()) {
            CUAProduct cuaProduct = (CUAProduct)i.next();
            if (cuaProduct.getCuaType().equals(cuaType)) {
                itemList.addItem(cuaProduct.getCatalogItem());
            }
        }
		log.exiting();
        return itemList;
    }


    /**
     * Set the property productId
     *
     * @param productId
     *
     */
    public void setProductId(String productId) {
        this.productId = productId;
    }


    /**
     * Returns the property productId
     *
     * @return productId
     *
     */
    public String getProductId() {
       return productId;
    }



    /**
     * Filter products with the given type filter
     *
     *
     * @param types array to filter cua types
     *
     */
    public void filterCUAWithTypes(String[] types) {
        int i = 0;

        while (i < productList.size()) {
            boolean found = false;
            for (int j = 0; j < types.length; j++) {
                if ( ((CUAProduct)productList.get(i)).getCuaType().equals(types[j])) {
                    found = true;
                }
            }

            if (found) {
                i++;
            }
            else {
                productList.remove(i);
            }
        }
    }

    /**
     * Filter products with the given type filter
     *
     *
     * @param types array to filter cua types
     *
     */
    public void removeCUAWithTypes(String[] types) {
        int i = 0;

        while (i < productList.size()) {
            boolean found = false;
            for (int j = 0; j < types.length; j++) {
                if ( ((CUAProduct)productList.get(i)).getCuaType().equals(types[j])) {
                    found = true;
                }
            }

            if (found) {
                productList.remove(i);
            }
            else {
                i++;
            }
        }

    }
    


    /**
     * @see com.sap.isa.businessobject.ProductList#enhance(com.sap.isa.catalog.webcatalog.WebCatInfo)
     */
    public void enhance(WebCatInfo catalog) {

        super.enhance(catalog);

        if (catalog != null) {
            sortProducts(CUAProduct.ACCESSORY,catalog);
        }

    }


    /**
     * Sort the products of the given type.
     *
     * @param type type to be removed from the list
     *
     */
    protected void sortProducts(String type, WebCatInfo catalog) {

        WebCatItemList list = new WebCatItemList(catalog);
        list.setListType(CV_LISTTYPE_CUA);

        int i = 0;

        // create a webCatItemList with the products
        // of the given type and remove them from the productlist
        while (i < productList.size()) {

            CUAProduct cUAProduct = (CUAProduct)productList.get(i);

            if (cUAProduct.getCuaType().equals(type)) {
                productList.remove(i);
                if(cUAProduct.getCatalogItem() != null) {
                    list.addItem(cUAProduct.getCatalogItem());
                }
            }
            else {
                i++;
            }
        }

        if (list.size() > 0) {
            // sort the webCatItemList
            list.sort("POS_NR");

            // append the products to the list again
            Iterator iter = list.iterator();
            while (iter.hasNext()) {
                addProduct(new CUAProduct((WebCatItem)iter.next(),techKey, type));

            }
        }

    }


    /**
     *
     * Constructor to create a CUAList from a webCatItemList with the given type
     *
     */
    private void createFromItemList(TechKey relatedProduct, ArrayList itemList, String type) {
		final String METHOD = "createFromItemList()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("type="+type);
        setTechKey(relatedProduct);

        if (itemList != null) {
            Iterator i =  itemList.iterator();
            CUAProduct cuaProduct;

            while (i.hasNext()) {
                  WebCatItem item = (WebCatItem)i.next();
                  cuaProduct = new CUAProduct( new TechKey(item.getProductID()),relatedProduct, type);
                  cuaProduct.setCatalogItem(item);
                  productList.add(cuaProduct);
              }
        }
        isEnhanced = true;
        log.exiting();
    }

}

