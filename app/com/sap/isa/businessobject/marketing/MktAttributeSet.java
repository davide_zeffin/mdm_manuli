/*****************************************************************************
    Class:        MktAttributeSet
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      15 Februar 2001
    Version:      0.1
*****************************************************************************/

package com.sap.isa.businessobject.marketing;

import java.util.ArrayList;
import java.util.Iterator;

import com.sap.isa.backend.BackendTypeConstants;
import com.sap.isa.backend.boi.isacore.marketing.MarketingConfiguration;
import com.sap.isa.backend.boi.isacore.marketing.MktAttributeData;
import com.sap.isa.backend.boi.isacore.marketing.MktAttributeSetBackend;
import com.sap.isa.backend.boi.isacore.marketing.MktAttributeSetData;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.businessobject.BusinessObjectHelper;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.Iterable;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.core.logging.IsaLocation;


/**
 *
 * The MktAttributeSet class handle the questionary which is
 * used to define an userprofile.
 *
 */
public class MktAttributeSet extends BusinessObjectBase
        implements MktAttributeSetData, BackendAware, Iterable {

    /*
     *  see setter methods for the documentation of private variables
     */
    private BackendObjectManager bem;
    private MktAttributeSetBackend backendService;
    private String id;
    private ArrayList attributes;
    private String usage = "";
    
	static final private IsaLocation loc = IsaLocation.getInstance(MktAttributeSet.class.getName());


    public MktAttributeSet() {
        attributes =  new ArrayList(20);
    }

    /**
     * Returns the id of the attribute set
     *
     * @return The id is used to identify the attribute set in the front- and in
     *         backend.
     */
    public String getId() {
        return id;
    }


    /**
     * Set the id of the attribute
     *
     * @param id The id is used to identify the attribute set in the front- and
     *        in backend.
     */
    public void setId(String id) {

        this.id = id;
    }


    /**
     * Add an attribute to the attribute set
     *
     * @param attribute attribute which should add to attribute set
     *
     * @see MktAttribute
     */
    public void addAttribute(MktAttributeData attribute) {
        if (attribute instanceof MktAttribute) {
            attributes.add(attribute);
        }
    }


    /**
     * Factory method to create MktAttributeData object with MktAttribute object
     */
    public MktAttributeData createAttribute() {
        return (MktAttributeData) new MktAttribute();
    }


    /**
     * Factory method to create MktAttributeData object with MktAttribute object
     * @param techKey TechKey for the attribute
     * @param id The id is used to identify the Attribute in the Front- and
     *        in Backend
     * @param description Description of the Attribute
     * @param datatype The data type categorizes an attribute and shows the
     *        format for entering values.
     * @param length Length of the attribute
     * @param decimals Decimals of the attribute
     * @param isRequired Flag, if this Attribute is required
     * @param isMultiple flag, if this attribute is multivalued, which means
     *        that the corresponding chosen values could have more than one
     *        value
     * @param isRangeAllowed if an attribute allow ranges, the values to this
     *        attribute are allow to be ranges
     * @param hasSign flag, if sign is allowed by numerical types
     * @param isCaseSensitive flag if the attribute is case sensitiv
     * @param areFreeValuesAllowed This flag describe whether values that are
     *        not defined as allowed values can be assigned to the attribute
     * @param unit Unit or Currency of the attribute
     *
     */
    public MktAttributeData createAttribute(
            TechKey techKey,
            String id,
            String description,
            int dataType,
            int length,
            int decimals,
            boolean isRequired, boolean isMultiple,
            boolean isRangeAllowed,
            boolean isCaseSensitive,
            boolean isSigned,
            boolean freeValuesAllowed, String unit) {

        return (MktAttributeData)
                new MktAttribute(techKey,
                                 id,
                                 description,
                                 dataType,
                                 length,
                                 decimals,
                                 isRequired,
                                 isMultiple,
                                 isRangeAllowed,
                                 isCaseSensitive,
                                 isSigned,
                                 freeValuesAllowed,
                                 unit);
    }


    /**
     *  remove all attribute from the list
     *
     */
    public void removeAttributes() {

        attributes.clear();

    }

	/**
	 * Return the usage of the attribute set. <br>
	 * 
	 * @return usage
	 */
	public String getUsage() {
		return usage;
	}

	/**
	 * Set the usage of the attribute set. <br>
	 * 
	 * @param usage
	 */
	public void setUsage(String usage) {
		this.usage = usage;
	}

    /**
     * Returns an iterator for the attributes. Method necessary because of
     * the <code>Iterable</code> interface.
     *
     * @return iterator to iterate over the attributes
     *
     */
    public Iterator iterator() {
        return attributes.iterator();
    }


    /**
     * Sets the BackendObjectManager for the attribute set. This method is used
     * by the object manager to allow the user object interaction with
     * the backend logic. This method is normally not called
     * by classes other than BusinessObjectManager.
     *
     * @param bem BackendObjectManager to be used
     */
    public void setBackendObjectManager(BackendObjectManager bem) {
        this.bem = bem;
    }


    /* get the BackendService, if necessary */
    private MktAttributeSetBackend getBackendService()
            throws BackendException{

        if (backendService == null) {
            // get the Backend from the Backend Manager
            backendService = (MktAttributeSetBackend) bem.createBackendBusinessObject(BackendTypeConstants.BO_TYPE_MKT_ATTRIBUTE_SET, null);
        }
        return backendService;
    }


	/**
	 * Read the object for the given id from backend
	 *
	 * @param id The id which identify the attribute set in the backend
	 */
	public void read(TechKey key, MarketingConfiguration configuration)
				throws CommunicationException {
		final String METHOD = "read()";
		loc.entering(METHOD);
		try {            
			// delete old entries 
			attributes.clear();
			setTechKey(key);
			// read AttributeSet from Backend
			getBackendService().read(this, configuration);
		}
		catch (BackendException ex) {
			BusinessObjectHelper.splitException(ex);
		} finally {
			loc.exiting();
		}
	
	}
				
    /**
     * Read the object for the given id from backend. <br>
     *
     * @param id The id which identify the attribute set in the backend
     */
    public void read(MarketingConfiguration configuration)
                throws CommunicationException {
                	
        read (null,configuration);
    }



}








