/*****************************************************************************
    Class:        CUAManager
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      May 2001
    Version:      1.0

    $Revision: #6 $
    $Date: 2001/07/25 $
*****************************************************************************/


package com.sap.isa.businessobject.marketing;

import java.util.ArrayList;
import java.util.Iterator;

import com.sap.isa.backend.BackendTypeConstants;
import com.sap.isa.backend.boi.isacore.marketing.CUAData;
import com.sap.isa.backend.boi.isacore.marketing.MarketingBusinessObjectsAware;
import com.sap.isa.backend.boi.isacore.marketing.MarketingConfiguration;
import com.sap.isa.backend.boi.isacore.marketing.MarketingUser;
import com.sap.isa.backend.boi.isacore.marketing.MktRecommendationBackend;
import com.sap.isa.businessobject.BusinessObjectHelper;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.core.Iterable;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.core.logging.IsaLocation;

/**
 *
 * The CUAManager reads CUA Products in the Backend
 *
 */
public class CUAManager implements BackendAware {

    private BackendObjectManager bem;
    private MktRecommendationBackend backendService;
	static final private IsaLocation loc = IsaLocation.getInstance(CUAManager.class.getName());


    /**
     * Sets the BackendObjectManager for the attribute set. This method is used
     * by the object manager to allow the user object interaction with
     * the backend logic. This method is normally not called
     * by classes other than BusinessObjectManager.
     *
     * @param bem BackendObjectManager to be used
     */
    public void setBackendObjectManager(BackendObjectManager bem) {
        this.bem = bem;
    }


    /* get the BackendService, if necessary */
    private MktRecommendationBackend getBackendService()
            throws BackendException{

        if (backendService == null) {
            backendService = (MktRecommendationBackend)
                    bem.createBackendBusinessObject(BackendTypeConstants.BO_TYPE_MKT_RECOMMENDATION,
                                                    MarketingBusinessObjectsAware.OBJECT_FACTORY);
        }
        return backendService;
    }


    /**
     * Read cua information for a product and enhance them with catalog data
     *
     * @param productList list of products
     * @param user tech key for the user
     * @param MarketingConfiguration tech key for the MarketingConfiguration
     * @param catalog to enhance product data (optional)
     * @param types array to filter cua types (optional)
     *
     */
    public CUAList readCUAList(TechKey    productKey,
                               MarketingUser       user,
                               MarketingConfiguration       configuration,
                               WebCatInfo catalog,
                               String[]   types)
                throws CommunicationException {
		final String METHOD = "readCUAList()";
		loc.entering(METHOD);
		if (loc.isDebugEnabled())
			loc.debug("productKey="+productKey+", user="+user
				+", configuration="+configuration);
        try {

            if (!configuration.isCuaAvailable()) {
                return new CUAList();
            }

            // read AttributeSet from Backend
            CUAList cUAList = (CUAList) getBackendService().readCUAList(productKey,
                                                                        user.getMktPartner(),
                                                                        configuration);
                                                                        
            if (types != null) {
                cUAList.filterCUAWithTypes(types);
            }

            if (catalog != null) {
                cUAList.enhance(catalog);
            }

            return cUAList;
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally {
        	loc.exiting();
        }

        return null;
    }


    /**
     * Read cua information for a catalog item and enhance them with catalog data.<br>
     * The information over accessories will be taken from the catalog.
     *
     * @param productList list of products
     * @param user tech key for the user
     * @param configuration tech key for the configuration
     * @param catalog to enhance product data (optional)
     * @param types array to filter cua types (optional)
     *
     */
    public CUAList readCUAList(WebCatItem webCatItem,
                               MarketingUser       user,
                               MarketingConfiguration       configuration,
                               WebCatInfo catalog,
                               String[]   types)

            throws CommunicationException {
            	
		final String METHOD = "readCUAList()";
		loc.entering(METHOD);
		if (loc.isDebugEnabled())
			loc.debug("configuration=" + configuration);
        TechKey productKey = new TechKey(webCatItem.getProductID());

        String[] myTypes = null;

        boolean readAccessory = true;

        // check, if accessories should be read
        if (types!=null) {
        	
			readAccessory = false;
            myTypes = new String[types.length];

            for (int i=0;i<types.length;i++) {
                if (types[i].equals(CUAProduct.ACCESSORY)) {
                    readAccessory = true;
                }
                else {
                    myTypes[i] = types[i];
                }
            }
        }
        
        // now call the standard method. The filter contains always  CUAProduct.ACCESSORY
        CUAList cUAList = readCUAList(productKey, user, configuration, catalog, myTypes);

        // if types is empty then remove all accessoiries read in backend 
        // and read them from catalog.
        if (myTypes == null) {
        	cUAList.removeCUAWithTypes(new String[] {CUAProduct.ACCESSORY});
        }
        
        // if necessary get the accessories from catalog
        if (readAccessory) {
            addAccessoriesFromCatalog(webCatItem, cUAList);
        }
		loc.exiting();
        return cUAList;
    }


    /**
     * Read accessories for a webCatItem.<br>
     * <i>Sorry for the missunstanding name;-) </i>
     *
     * @param webCatItem read all accessories for this item
     * @param catalog to enhance product data (optional)
     * @deprecated please use {@link com.sap.isa.businessobject.marketing.CUAManager#readAccessoryList}
     * instead.
     *
     */
    public CUAList readCUAList(WebCatItem webCatItem,
                               WebCatInfo catalog) {

        return readAccessoryList(webCatItem);
    }


    /**
     * Read accessories for a webCatItem.<br>
     *
     * @param webCatItem read all accessories for this item
     *
     */
    public CUAList readAccessoryList(WebCatItem webCatItem) {
		final String METHOD = "readAccessoryList()";
		loc.entering(METHOD);
        TechKey productKey = new TechKey(webCatItem.getProductID());

        ArrayList accessories = webCatItem.getAccesories();

        CUAList cUAList = new CUAList(productKey, accessories, CUAProduct.ACCESSORY);
		loc.exiting();
        return cUAList;
    }



    /**
     * Read accessory for a webCatItem and them to the given CUAList
     *
     * @param webCatItem read all accessories for this item
     * @param cuaList to enhance
     *
     */
    protected void addAccessoriesFromCatalog(WebCatItem webCatItem,
                                              CUAList cUAList) {


        TechKey productKey = new TechKey(webCatItem.getProductID());

        ArrayList accessories = webCatItem.getAccesories();

        if (accessories != null) {            
            for (int i=0;i<accessories.size();i++) {
                CUAProduct product = new CUAProduct((WebCatItem)accessories.get(i),productKey, CUAProduct.ACCESSORY);
                cUAList.addProduct(product);
            }
        }
    }


    /**
     * Read cua information for all objects which are given with the iterator
     * productList and enhance them with catalog data<br>
     * the objects must therefore implement the interface CUAData.
     *
     * @param productList list of products
     * @param user tech key for the user
     * @param configuration tech key for the configuration
     * @param catalog to enhance product data
     *
     * return true, if any cua product could be found
     *
     */
    public boolean readAllCUAList(Iterable   productList,
                                  MarketingUser       user,
                                  MarketingConfiguration       configuration,
                                  WebCatInfo catalog)
    throws CommunicationException {
		final String METHOD = "readAllCUAList()";
		loc.entering(METHOD);
		if (loc.isDebugEnabled())
			loc.debug("user="+user+", configuration="+configuration);
		boolean found = false;
		try {
	        if (!configuration.isCuaAvailable()) {
	            return found;
	        }
	
	        try {
	            Iterator i = productList.iterator();
	            // read AttributeSet from Backend
	            getBackendService().readAllCUAList(i,
	                                               user.getMktPartner(),
	                                               configuration);
	
	            i = productList.iterator();
	            while (i.hasNext()) {
	                Object obj = i.next();
	                if (obj instanceof CUAData) {
	                    CUAData cua = (CUAData)obj;
	
	                    if (cua != null) {
	                        CUAList cUAList = (CUAList)cua.getCUAList();
	                        if (cUAList != null && cUAList.size()>0) {
                                // delete up selling products for sub components
                                if (!cua.isUpSellingRelevant()) {
                                    cUAList.removeCUAWithTypes(new String[] {CUAProduct.UPSELLING, CUAProduct.DOWNSELLING});
                                }
                                // enhance cua list 
	                            cUAList.enhance(catalog);
	                            // maybe some products will be removed
	                            if (cUAList.size()>0) {
	                                found = true;
	                            }
	                        }
	                    }
	                }
	            }
	
	        }
	        catch (BackendException ex) {
	            BusinessObjectHelper.splitException(ex);
	            return (false);
	        }
		} finally {
			loc.exiting();
		}
        return found;
    }

}

