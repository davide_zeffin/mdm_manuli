/*****************************************************************************
    Class:        MktQuestionary
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      15 Februar 2001
    Version:      0.1
*****************************************************************************/
package com.sap.isa.businessobject.marketing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.core.Iterable;
import com.sap.isa.core.TechKey;


/**
 *
 * The MktQuestionary is create from a MktAttributeSet to extend the Business Objekt
 * with attributes which are only relevant from the UI-Layer <br>
 *
 * The techKey will be taken from the profile
 *
 **/
public class MktQuestionary extends BusinessObjectBase
        implements Iterable {

    /* the questions will handle within ArrayList */
    private TechKey attributeSetKey;
    private String id;
    private boolean isSaved;
    private ArrayList questionList;
    private boolean requiredValues = false;
	private String usage = "";


    /**
     *
     * Constructor to create an questionary from an attribute set and
     * from a profile to handle the questionary screen
     *
     * @param attributeSet: reference to MktAttributeSet object
     * @param profile: reference to MktProfile object
     */
    public MktQuestionary(MktAttributeSet attributeSet, MktProfile profile){

        setTechKey(TechKey.EMPTY_KEY);

        Map chosenValueMap = new HashMap();

        if (profile != null) {
            // get the techKey from the profile
            setTechKey(profile.getTechKey());
            // save all chosen Value in a Hash Map with Attribute ID as key
            Iterator i = profile.iterator();
            while (i.hasNext()) {
                MktChosenValues chosenValues = (MktChosenValues)i.next();
                 chosenValueMap.put(chosenValues.getAttributeKey(),chosenValues);
            }
        }

        // get the attribute values from the given attribute object
        attributeSetKey=attributeSet.getTechKey();
        this.id = attributeSet.getId();

        questionList = new ArrayList(20);

        Iterator attributes = attributeSet.iterator();

        while (attributes.hasNext()) {

            MktAttribute attribute = (MktAttribute)attributes.next();

            MktQuestion  question =
                    new MktQuestion(attribute,
                                    (MktChosenValues)chosenValueMap.get((TechKey)attribute.getTechKey()));

            questionList.add(question);
            
            if (attribute.isRequired()) {
				requiredValues = true;
            }
        } //while


		usage = attributeSet.getUsage();
		
        // copy the message from both attributeSet and profile
        copyMessages(attributeSet);
        if (profile != null) {
            copyMessages(profile);
        }

    }



    /**
     * Set the property attributeSetKey
     *
     * @param attributeSetKey
     *
     */
    public void setAttributeSetKey(TechKey attributeSetKey) {
        this.attributeSetKey = attributeSetKey;
    }


    /**
     * Returns the property attributeSetKey
     *
     * @return attributeSetKey
     *
     */
    public TechKey getAttributeSetKey() {
       return this.attributeSetKey;
    }


    /**
     * Returns the id of the attribute set
     *
     * @return The id is used to identify the attribute set in the front- and in
     *         backend.
     */
    public String getId() {
        return id;
    }


    /**
     * Set the id of the attribute
     *
     * @param id The id is used to identify the attribute set in the front- and
     *        in backend.
     */
    public void setId(String id) {
        this.id = id;
    }


    /**
     * Set Flag which describes if the "SAVE"-Button was pressed
     *
     * @param isSaved flag, if questionary, if saved
     */
    public void setSaved(boolean isSaved) {
        this.isSaved = isSaved;
    }


    /**
     * Returns if the questionary was saved in the current request
     *
     * @return if questionary is saved
     */
    public boolean isSaved() {
        return isSaved;
    }


	/**
	 * Return the usage of the attribute set. <br>
	 * 
	 * @return usage
	 */
	public String getUsage() {
		return usage;
	}


    /**
     * Returns an iterator for questions.
     *
     * @return Iterator tp loop over the questions
     */
    public Iterator iterator() {
        return questionList.iterator();
    }
    
    
    /**
     * Return if the questionary contains required values. <br>
     * 
     * @return <code>true</code> if required values are available. <br>
     */
    public boolean hasRequiredValues() {
        return requiredValues;
    }

}


