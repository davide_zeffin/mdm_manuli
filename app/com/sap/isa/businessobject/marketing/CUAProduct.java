/*****************************************************************************
    Class:        CUAProduct
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      March 2001
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.businessobject.marketing;

import com.sap.isa.backend.boi.isacore.marketing.CUAProductData;
import com.sap.isa.businessobject.Product;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.core.TechKey;


/**
 * Class representing a cua product
 *
 * @author Wolfgang Sattler
 * @version 1.0
 */
public class CUAProduct extends Product
    implements CUAProductData {

    private String cuaType;
    private TechKey relatedProduct;


    /**
     * Constructor
     *
     * @param techKey  techKey of the product
     * @param id product id
     * @param type of CUA
     *
     */
    public CUAProduct(TechKey techKey, TechKey relatedProduct, String type)
    {
      super(techKey);
      this.cuaType = type;
      this.relatedProduct = relatedProduct;
    }


    /**
     * Constructor
     *
     * @param webCatItem  catalog item to create the product
     * @param id product id
     * @param type of CUA
     *
     */
    public CUAProduct(WebCatItem webCatItem, TechKey relatedProduct, String type)
    {
      super(webCatItem);
      this.cuaType = type;
      this.relatedProduct = relatedProduct;
    }


    /**
     * Set the property cuaType
     *
     * @param cuaType
     *
     */
    public void setCuaType(String cuaType) {
        this.cuaType = cuaType;
    }


    /**
     * Returns the property cuaType
     *
     * @return cuaType
     *
     */
    public String getCuaType() {
       return this.cuaType;
    }



    /**
     * Set the property relatedProduct
     *
     * @param relatedProduct
     *
     */
    public void setRelatedProduct(TechKey relatedProduct) {
        this.relatedProduct = relatedProduct;
    }


    /**
     * Returns the property relatedProduct
     *
     * @return relatedProduct
     *
     */
    public TechKey getRelatedProduct() {
       return this.relatedProduct;
    }


}


