/*****************************************************************************
    Class:        MktProfile
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      March 2001
    Version:      1.0

    $Revision: #2 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.businessobject.marketing;

import java.util.ArrayList;
import java.util.Iterator;

import com.sap.isa.backend.boi.isacore.marketing.MktAttributeData;
import com.sap.isa.backend.boi.isacore.marketing.MktChosenValuesData;
import com.sap.isa.backend.boi.isacore.marketing.MktProfileData;
import com.sap.isa.backend.boi.isacore.marketing.MktValueData;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.core.Iterable;
import com.sap.isa.core.TechKey;


/**
 *
 * The MktProfile class handle the profile of an User, which will be
 * definied by an attribute set and the user id.
 *
 */
public class MktProfile
        extends    BusinessObjectBase
        implements MktProfileData, Iterable {

    private ArrayList chosenValuesList;
    private TechKey attributeSetKey;

    /**
     * Standard Constructor
     */
    public MktProfile() {
        chosenValuesList = new ArrayList(20);
        setTechKey(TechKey.EMPTY_KEY);
        attributeSetKey = TechKey.EMPTY_KEY;
    }


    /**
     * Standard Constructor
     *
     * @param techKey techKey for the profile
     * @param attributeSetKey techKey for the corresponding attribute set
     *
     */
    public MktProfile(TechKey techKey,TechKey attributeSetKey) {
        chosenValuesList = new ArrayList(20);
        setTechKey(techKey);
        this.attributeSetKey = attributeSetKey;
    }


    /**
     * Returns the key of the cooresponding attribute set
     *
     * @return The key used to identify the cooresponding attribute set
     */
    public TechKey getAttributeSetKey() {
        return attributeSetKey;
    }


    /**
     * Set key of the cooresponding attribute set
     *
     * @param attributeSetKey The key is used to identify the cooresponding
     *        attribute set
     */
    public void setAttributeSetKey(TechKey attributeSetKey) {
        this.attributeSetKey = attributeSetKey;
    }


   /**
    * Add the chosen values for one attribute to the profile
    *
    * @param chosenValues The chosen values for one attribute
    *
    * @see MktChosenValues
    */
    public void addChosenValues(MktChosenValuesData chosenValues) {
        if (chosenValues instanceof MktChosenValues) {
            chosenValuesList.add((MktChosenValues)chosenValues);
        }
    }



    /**
     * Factory method to create MktAttributeData object with MktAttribute object
     */
    public MktAttributeData createAttribute() {
        return (MktAttributeData) new MktAttribute();
    }


    /**
     * Factory method to create MktChosenValuesData object with MktChosenValues object
     *
     * @param attributeKey TechKey for the attribute
     * @param datatype The data type categorizes an attribute and shows the
     *        format for entering values.
     *
     */
    public MktChosenValuesData createChosenValues(
            TechKey techKey,
            TechKey attributeKey,
            int     dataType) {

        return (MktChosenValuesData)
                new MktChosenValues(techKey, attributeKey, dataType);
    }


    /**
     * Factory method to create MktChosenValuesData object with MktChosenValues object
     *
     */
    public MktChosenValuesData createChosenValues() {

        return (MktChosenValuesData) new MktChosenValues();
    }


    /**
     * Factory method to create MktValueData object with MktValue object
     *
     * @param techKey TechKey for the value
     * @param id the id of the value
     * @param description the description to the value
     *
     */
    public MktValueData createValue(TechKey techKey, String id, String description) {
        return (MktValueData) new MktValue(techKey, id, description);
    }


    /**
     * Returns an iterator for the chosen values of the profile.
     *
     * @return iterator to loop over the list of chosen values
     */
    public Iterator iterator() {
        return chosenValuesList.iterator();
    }




}
