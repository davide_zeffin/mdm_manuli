/*****************************************************************************
    Class:        MktQuestion
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      15 Februar 2001
    Version:      0.1
*****************************************************************************/
package com.sap.isa.businessobject.marketing;

import java.util.ArrayList;
import java.util.Iterator;

import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.core.Iterable;
import com.sap.isa.core.TechKey;

/**
 *
 * The MktQuestion is create from MktAttribute to extend the Business Objekt
 * with attributes which are only relevant from the UI-Layer<br>
 *
 * The techKey will be taken from the chosen value object
 *
 **/
public class MktQuestion
        extends BusinessObjectBase
        implements Iterable {

    /**  *** CASE 1:  CHARACTERISTICS WITH LIST OF PREDEFINED VALUES ***
     */
    public static final int PREDEFINED_VALUES             = 1;
    /*** CASE 2b:  CHARACTERISTICS WITHOUT LIST OF PREDEFINED VALUES, MULTIPLE-VALUED ***/
    public static final int NO_PREDEFINED_VALUES_SINGLE   = 2;
    /* *** CASE 2a:  CHARACTERISTICS WITHOUT LIST OF PREDEFINED VALUES, SINGLE-VALUED *** */
    public static final int NO_PREDEFINED_VALUES_MULTIPLE = 3;
    /* *** CASE 2:  CHARACTERISTICS WITHOUT LIST OF PREDEFINED VALUES *** */

    private int renderType;
    private int inputFieldLength;
    private MktAttribute attribute;
    private ArrayList answerList;


    /**
     *
     * Constructor to create a question from an attribute and the chosen values
     *
     * @param attribute: reference to MktAttribute object
     * @param chosenValues: reference to MktChosenValues object
     * @param index: array index, which is need to found the attribute on the jsp
     *
     */
    public MktQuestion(MktAttribute attribute,
                       MktChosenValues chosenValues) {

        // take techKey from chosen Values
        if (chosenValues != null) {
            setTechKey(chosenValues.getTechKey());
        }
        else {
            setTechKey(new TechKey(""));
        }

        this.attribute = attribute;
        int dataType = attribute.getDataType();
        int length   = attribute.getLength();

        if (((dataType == MktAttribute.DTNUMC) || (dataType == MktAttribute.DTCURR))
            && (attribute.getDecimals() > 0)) {
            inputFieldLength = length + 1;
			if (length - attribute.getDecimals() > 3) {
				int digit = (length - attribute.getDecimals()) % 3;
				if (digit == 0) length--;
				digit = (length - attribute.getDecimals()) / 3;
				inputFieldLength = inputFieldLength + digit;
			}            
        }
        else {
            inputFieldLength = length;
        }

        if ((dataType == MktAttribute.DTNUMC) || (dataType == MktAttribute.DTCURR)) {
            if (attribute.isSigned()) {
                inputFieldLength++;
            }

            int unitLength = attribute.getUnit().length();
            if (unitLength > 0) {
                inputFieldLength = inputFieldLength + unitLength + 1;
            }
        }

        if (attribute.isRangeAllowed()) {
            inputFieldLength = 2*inputFieldLength + 7;
        }


        if (attribute.hasAllowedValues()) {
            renderType = PREDEFINED_VALUES;
        }
        else {
            if (attribute.isMultiple() == false) {
                renderType = NO_PREDEFINED_VALUES_SINGLE;
            }
            else {
                renderType = NO_PREDEFINED_VALUES_MULTIPLE;
            }
        }

        answerList = new ArrayList(20);

        // first take all allowedValues
        Iterator allowedValues = attribute.iterator();

        while (allowedValues.hasNext()) {
                       
            boolean isChosen = false;

            MktValue allowedValue = (MktValue) allowedValues.next();

            if (chosenValues != null) {
                Iterator chValues = chosenValues.iterator();

                while (chValues.hasNext()) {
                    MktValue chValue = (MktValue) chValues.next();

                    if (chValue.getId().equals(allowedValue.getId())) {
                        isChosen = true;
                    }
                } // while
            }
            else {
                // else check, if value is default value
                if (allowedValue.isDefaultValue()) {
                    isChosen = true;
                }
            }

            MktAnswer answer = new MktAnswer(allowedValue, isChosen, false, dataType);
            answerList.add(answer);
        } // while

        if (chosenValues != null) {
            //
            Iterator chValues = chosenValues.iterator();

            while (chValues.hasNext()) {
                boolean found = false;

                MktValue chValue = (MktValue) chValues.next();
                allowedValues = attribute.iterator();
                while (allowedValues.hasNext()) {
                    MktValue allowedValue = (MktValue) allowedValues.next();

                    if (chValue.getId().equals(allowedValue.getId())) {
                        found = true;
                    }
                } // while
                if (!found) {
                    MktAnswer answer = new MktAnswer(chValue, true, true, dataType);
                    answerList.add(answer);
                }
            }  // while
        } // if

        // add at least one answer, if not answer is given
        if (answerList.isEmpty()) {
            MktValue chValue = new MktValue(TechKey.EMPTY_KEY,"","");
            MktAnswer answer = new MktAnswer(chValue, false, true, dataType);
            answerList.add(answer);
            if (attribute.isMultiple()) {
                answerList.add(answer);
                answerList.add(answer);
            }
        }
        answerList.trimToSize();

        // copy the message from both attribute and chosenValues
        copyMessages(attribute);
        if (chosenValues != null) {
            copyMessages(chosenValues);
        }
    }

    /**
     * Add new empty answers for multiple free entries
     *
     * @param count count of free answers
     *
     */
    public void addFreeAnswers(int count) {
        MktValue chValue = new MktValue(TechKey.EMPTY_KEY,"","");
        MktAnswer answer = new MktAnswer(chValue, false, true, attribute.getDataType());
        for (int i = 0; i < count; i++) {
            answerList.add(answer);
        }
    }


    /**
     * Returns an reference to the corresponding attribute
     *
     * @return Reference to the attribute
     *
     */
    public MktAttribute getAttribute() {
        return attribute;
    }


    /**
     * Returns the length of the attribute
     *
     * @return the length of the attribute
     */
    public int getLength() {
        return inputFieldLength;
    }


    /*
     * Get the rendertype of an attribute
     *
     * @return The render type describes how to render an attribute
     */
    public int getRenderType() {
        return(renderType);
    }

    /**
     * Set the rendertype of an attribute
     *
     * @param renderType The render type describes how  to render an attribute
     */
    public void setRenderType(int renderType) {
        this.renderType = renderType;
    }


    /**
     * Returns the techKey of the cooresponding attribute
     *
     * @return attributeKey
     *
     */
    public TechKey getAttributeKey() {
       return this.attribute.getTechKey();
    }


    /**
      * Returns an iterator for the elements of this object.
      * The elements are of type MktValueBean.
      */

    public Iterator iterator() {
        return answerList.iterator();
    }


	/**
     * Returns the the size of the answer list.
     * 
     * @return int
     */
    public int getAnswerCount() {
	    return answerList.size();
	}


	/**
	 * Returns the visible length of the attribute
	 *
	 * @return the visible length of the attribute
	 */
	public int getVisibleLength() {
		int i = 0;
		if ((inputFieldLength - 10) > 0) { 
			i = (inputFieldLength - 10) / 3;
		}
		return inputFieldLength + 1 + i;
	}


}


