/*****************************************************************************
    Class:        MktPartner
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      March 2001
    Version:      1.0

    $Revision: #2 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.businessobject.marketing;

import com.sap.isa.backend.boi.isacore.marketing.MktPartnerData;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.core.TechKey;

/**
 * This class describes the business partner(s) to be use with the marketing 
 * activities.
 * The class take cares of the differnt requirements form B2B and B2C.
 * This class allows a clear access to the business partner in each scenario.
 * 
 * @author SAP
 * @version 1.0
 *
 */
public class MktPartner extends BusinessObjectBase implements MktPartnerData {

    private TechKey alternativePartner;    
    private boolean known = true; 


	/**
	 * Returns the alternativePartner.
	 * @return TechKey
	 */
	public TechKey getAlternativePartner() {
		return alternativePartner;
	}


	/**
	 * Sets the alternativePartner.
	 * @param alternativePartner The alternativePartner to set
	 */
	public void setAlternativePartner(TechKey alternativePartner) {
		this.alternativePartner = alternativePartner;
	}


	/**
	 * Returns if the partner is known.
	 * @return boolean
	 */
	public boolean isKnown() {
		return known;
	}
    
    
	/**
	 * Sets if the partner is an unknown user.
	 * @param known Flag if the partner is know.
	 */
	public void setKnown(boolean known) {
		this.known = known;
	}

}
