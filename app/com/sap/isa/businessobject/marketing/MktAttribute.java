/*****************************************************************************
    Class:        MktAttribute
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      March 2001
    Version:      1.0

    $Revision: #2 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.businessobject.marketing;

import java.util.ArrayList;
import java.util.Iterator;

import com.sap.isa.backend.boi.isacore.marketing.MktAttributeData;
import com.sap.isa.backend.boi.isacore.marketing.MktValueData;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.core.Iterable;
import com.sap.isa.core.TechKey;


/**
 *
 * The MktAttribute class handle one question in the questionary which is
 * used to define an user profile.
 *
 *
 */
public class MktAttribute extends BusinessObjectBase implements MktAttributeData, Iterable {

    /**
     *  constant to define dataType Character.
     */
    final public static int DTCHAR = 1;

    /**
     *  constant to define dataType Numeric.
     */
    final public static int DTNUMC = 2;

    /**
     *  constant to define dataType Time.
     */
    final public static int DTTIME = 3;

    /**
     *  constant to define dataType Date.
     */
    final public static int DTDATE = 4;

    /**
     *  constant to define dataType Curr.
     */
    final public static int DTCURR = 5;

    /**
     *  constant to define an other dataType .
     */
    final public static int DTOTHER = 6;


    /*
     *  see setter methods for the documentation of private variables
     */
    private String id;
    private String description;
    private int dataType;
    private int length;
    private int decimals;
    private boolean isRequired;
    private boolean isMultiple;
    private boolean isRangeAllowed;
    private boolean isCaseSensitive;
    private boolean isSigned;
    private boolean freeValuesAllowed;
    private String unit;
    private ArrayList allowedValues;


    /**
     * standard constructor
     */
    public MktAttribute() {
        allowedValues = new ArrayList(20);
    }


    /**
     *
     * Constructor to create an Attribute
     *
     * @param techKey TechKey for the attribute
     * @param id The id is used to identify the Attribute in the Front- and
     *        in Backend
     * @param description Description of the Attribute
     * @param datatype The data type categorizes an attribute and shows the
     *        format for entering values.
     * @param length Length of the attribute
     * @param decimals Decimals of the attribute
     * @param isRequired Flag, if this Attribute is required
     * @param isMultiple flag, if this attribute is multivalued, which means
     *        that the corresponding chosen values could have more than one
     *        value
     * @param isRangeAllowed if an attribute allow ranges, the values to this
     *        attribute are allow to be ranges
     * @param hasSign flag, if sign is allowed by numerical types
     * @param isCaseSensitive flag if the attribute is case sensitiv
     * @param areFreeValuesAllowed This flag describe whether values that are
     *        not defined as allowed values can be assigned to the attribute
     * @param unit Unit or Currency of the attribute
     *
     */
    public MktAttribute(TechKey techKey,
                        String id,
                        String description,
                        int dataType,
                        int length,
                        int decimals,
                        boolean isRequired, boolean isMultiple,
                        boolean isRangeAllowed,
                        boolean isCaseSensitive,
                        boolean isSigned,
                        boolean freeValuesAllowed, String unit) {

        setTechKey(techKey);
        this.id = id;
        this.description = description;
        this.dataType = dataType;
        this.length = length;
        this.decimals = decimals;
        this.isRequired = isRequired;
        this.isMultiple = isMultiple;
        this.isRangeAllowed = isRangeAllowed;
        this.isCaseSensitive = isCaseSensitive;
        this.isSigned = isSigned;
        this.freeValuesAllowed = freeValuesAllowed;
        this.unit = unit;
        allowedValues = new ArrayList(20);
    }


    /**
     * Returns if attribute is case sensitive
     *
     * @return flag if the attribute is case sensitiv
     */
    public boolean isCaseSensitive() {
        return isCaseSensitive;
    }


    /**
     * Set the flag isCaseSensitive
     *
     * @param isCaseSensitive flag if the attribute is case sensitiv
     */
    public void setCaseSensitive(boolean isCaseSensitive) {
        this.isCaseSensitive = isCaseSensitive;
    }


    /**
     * Returns the data type of the attribute
     *
     * @see MktAttribute#setDataType for a description of the types
     */
    public int getDataType() {
        return dataType ;
    }


    /**
     * Set the datatype of an attribute <br>
     * You can use the possible entries to select one of the following
     * predefined data types
     * <ol type="1">
     * <li value="1">Character format: for characteristic values that consist of
     *               a character string </li>
     * <li value="2">Numeric format: for numeric characteristic values </li>
     * <li value="3">Date format: for characteristic values that represent
     *               a date </li>
     * <li value="4">Time format: for characteristic values that represent
     *               a time </li>
     * <li value="5">Currency format: for characteristic values that are entered
     *               in a currency </li> </ol>

     * @param datatype The data type categorizes an attribute and shows the
     *        format for entering  values.
     *
     */
    public void setDataType(int dataType) {
        this.dataType = dataType;
    }


    /**
     * Get the decimals of the attribute
     *
     * @return Decimals of the attribute
     */
    public int getDecimals() {
        return decimals;
    }


    /**
     * Set the decimals of the attribute
     *
     * @param decimals Decimals of the attribute
     */
    public void setDecimals(int decimals) {
        this.decimals = decimals;
    }


    /**
     *  Set the description of the attribute
     *
     *  @param description Description of the attribute
     */
    public String getDescription() {
        return description;
    }


    /**
     *  Set the description of the attribute
     *
     *  @param description Description of the attribute
     */
    public void setDescription(String description) {
        this.description = description;
    }


    /**
     * Returns the id of the attribute
     *
     * @return The id is used to identify the attribute in the front- and in
     *         backend.
     */
    public String getId() {
        return id;
    }


    /**
     * Set the id of the attribute
     *
     * @param id The id is used to identify the attribute in the front- and in backend.
     */
    public void setId(String id) {

        this.id = id;
    }


    /**
     * Returns the length of the attribute
     *
     * @return the length of the attribute
     */
    public int getLength() {
        return length;
    }


    /**
     * Set the length of the attribute
     *
     * @param length The length of the attribute
     */
    public void setLength(int length) {
        this.length = length;
    }


    /**
     * Return the flag isMultiple, which describes, if an attribute is single or
     * multivalued
     *
     * @return if an attribute is multivalued, which means that the corresponding
     *         chosen values could have more than one value
     */
    public boolean isMultiple() {

        return isMultiple;
    }


    /**
     * Set the flag isMultiple, which describes, if an attribute is single or
     * multivalued
     *
     * @param isMultiple flag, if an attribute is multivalued, which means that
     *        the corresponding chosen values could have more than one value
     */
    public void setMultiple (boolean isMultiple) {
        this.isMultiple = isMultiple;
    }


    /**
     * Returns, if the attribute is required
     *
     * @return flag, if attrubute is required
     */
    public boolean isRequired() {
        return isRequired;
    }


    /**
     * Set the flag isRequired, which describes, if an attribute is required
     * or is optional.
     * If the attribute is required the chosen values to this attribute must
     * have at least one value
     *
     * @param isRequired flag, if this attribute is required
     */
    public void setRequired(boolean isRequired) {
        this.isRequired = isRequired;
    }


    /**
     * Returns, if ranges are allowed values for the attribute
     *
     * @return flag, if ranges are allowed values for the attribute
     */
    public boolean isRangeAllowed() {
        return isRangeAllowed;
    }


    /**
     * Set a flag, if ranges allowed values to this attribute
     *
     * @param isRangeAllowed If an attribute allow ranges, the values to this
     *        attribute are allowed to be ranges
     */
    public void setRangeAllowed(boolean isRangeAllowed) {
        this.isRangeAllowed = isRangeAllowed;
    }

    /**
     * Set if the attribute could have an sign
     *
     * @param isSigned flag, if sign is allowed by numerical types
     */
    public void setSigned(boolean isSigned) {
        this.isSigned = isSigned;
    }


    /**
     * Returns if the attribute could have an sign
     *
     * @return flag, if sign is allowed by numerical types
     */
    public boolean isSigned() {
        return isSigned;
    }


    /**
     * Returns if free values for the attrubte are allowed
     *
     * @return a flag which describe if values that are not
     *        defined as allowed values can be assigned to the attribute
     */
    public boolean getFreeValuesAllowed() {
        return freeValuesAllowed;
    }


    /**
     * Set if free values for the attrubte are allowed
     *
     * @param freeValuesAllowed: This flag describe whether values that are not
     *        defined as allowed values can be assigned to the attribute
     */
    public void setFreeValuesAllowed(boolean freeValuesAllowed) {
        this.freeValuesAllowed = freeValuesAllowed;
    }


    /**
     * Get the unit of the Attribute
     *
     * @return Unit or currency of the attribute
     */
    public String getUnit() {
        return unit;
    }


    /**
     * Set the unit of the Attribute
     *
     * @param unit Unit or currency of the attribute
     */
    public void setUnit(String unit) {
        this.unit = unit;
    }


    /**
     * Add an allowed values of the attribute
     *
     * @param allowedValue A allowed value for the attribute
     */
    public void addAllowedValue(MktValueData allowedValue) {
        if (allowedValue instanceof MktValue) {
            allowedValues.add((MktValue)allowedValue);
        }
    }


    /**
     * Factory method to create MktValueData object with MktValue object
     */
    public MktValueData createValue() {
        return (MktValueData) new MktValue();
    }


    /**
     * Factory method to create MktValueData object with MktValue object
     *
     * @param techKey techKey for the value
     * @param id the id of the value
     * @param description the description to the value
     *
     */
    public MktValueData createValue(TechKey techKey, String id, String description) {
        return (MktValueData) new MktValue(techKey, id, description);
    }


    /**
     * Returns an iterator for the allowed values.
     * The elements are of type MktValues.
     *
     * @return Iterator to loop over the allowed values
     * @see MktValue
     */
    public Iterator iterator() {
        return allowedValues.iterator();
    }


    /**
     * Returns if the attribute have allowed values
     *
     * @return true if the attribute have allowed values
     */
    public boolean hasAllowedValues() {
        return allowedValues.isEmpty() ? false : true ;
    }
}
