/*****************************************************************************
    Class:        DataObjectFactory
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Thomas Smits
    Created:      29.3.2001
    Version:      1.0

    $Revision: #2 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.businessobject.marketing;

import com.sap.isa.backend.boi.isacore.marketing.BestsellerData;
import com.sap.isa.backend.boi.isacore.marketing.CUAListData;
import com.sap.isa.backend.boi.isacore.marketing.MktPartnerData;
import com.sap.isa.backend.boi.isacore.marketing.MktProfileData;
import com.sap.isa.backend.marketing.MarketingBackendDataObjectFactory;

/**
 * Class used for the creation of business objects.
 *
 * @author Thomas Smits
 * @version 1.0
 */
public class MarketingDataObjectFactory implements MarketingBackendDataObjectFactory {


    /**
     * Creates a new <code>MktProfile</code> object.
     *
     * @return The newly created object
     */
    public MktProfileData createMktProfileData() {
        return new MktProfile();
    }


    /**
     * Creates a new <code>MktPartner</code> object.
     *
     * @return The newly created object
     */
    public MktPartnerData createMktPartnerData() {
        return new MktPartner();
    }


    /**
     * Creates a new <code>Bestseller</code> object.
     *
     * @return The newly created object
     */
    public BestsellerData createBestsellerData() {
        return new Bestseller();
    }


    /**
     * Creates a new <code>CUAList</code> object.
     *
     * @return The newly created object
     */
    public CUAListData createCUAListData() {
        return new CUAList();
    }

}