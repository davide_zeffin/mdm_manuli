/*****************************************************************************
  Class:        MktChosenValues
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Wolfgang Sattler
    Created:      March 2001
    Version:      1.0

    $Revision: #2 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.businessobject.marketing;

import java.util.ArrayList;
import java.util.Iterator;

import com.sap.isa.backend.boi.isacore.marketing.MktChosenValuesData;
import com.sap.isa.backend.boi.isacore.marketing.MktValueData;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.core.Iterable;
import com.sap.isa.core.TechKey;


/**
 *
 * The MktChosenValue class handle the answer for one question in the questionary
 * which is used to define an user profile.
 * The techKey give the reference to the corresponding attribute
 *
 */
public class MktChosenValues
        extends BusinessObjectBase
        implements MktChosenValuesData, Iterable{

    /*
     *  see setter methods for the documentation of private variables
     */
    private TechKey attributeKey;
    private int dataType;
    private ArrayList values;


    /**
     * Constructor to create a ChosenValues object
     *
     */
    public MktChosenValues() {
        values = new ArrayList(20);
        setTechKey(TechKey.EMPTY_KEY);
        attributeKey = TechKey.EMPTY_KEY;
    }


    /**
     * Constructor to create a ChosenValues object
     *
     * @param techKey techKey for the chosen values
     * @param attributeKey TechKey for the attribute
     * @param datatype The data type categorizes an attribute and shows the
     *        format for entering values.
     */
    public MktChosenValues(TechKey techKey, TechKey attributeKey, int dataType) {
        setTechKey(techKey);
        this.attributeKey = attributeKey;
        this.dataType = dataType;
        values = new ArrayList(20);
    }

    /* Definition of public methods */

    /**
     * Returns the techKey of the corresponding attribute
     *
     * @return The techKey which is used to identify the corresponding attribute.
     */
    public TechKey getAttributeKey() {
        return attributeKey;
    }


    /**
     * Set the techKey of the corresponding attribute
     *
     * @param attributeKey The key is used to identify the corresponding attribute
     */
    public void setAttributeKey(TechKey attributeKey){
        this.attributeKey = attributeKey;
    }



    /**
     * Returns the datatype of the corresponding attribute
     *
     * @return The data type categorizes an attribute and shows the
     *        format for entering values.
     */
    public int getDataType() {
        return dataType;
    }


    /**
     * Set the datatype of the corresponding attribute
     *
     * @param datatype The data type categorizes an attribute and shows the
     *        format for entering  values.
     */
    public void setDataType(int dataType) {
        this.dataType = dataType;
    }


    /**
     * Add a chosen value
     *
     * @param value A MktValue object
     */
    public void addValue(MktValueData value) {
        if (value instanceof MktValue) {
            values.add((MktValue)value);
        }
    }


    /**
     * Factory method to create MktValueData object with MktValue object
     */
    public MktValueData createValue() {
        return (MktValueData) new MktValue();
    }


    /**
     * Factory method to create MktValueData object with MktValue object
     *
     * @param techKey techKey for the value
     * @param id the id of the value
     * @param description the description to the value
     *
     */
    public MktValueData createValue(TechKey techKey, String id, String description) {
        return (MktValueData) new MktValue(techKey, id, description);
    }


    /**
     * Returns an iterator for the chosen values .
     *
     * @return Iterator to loop over the chosen values
     * @see MktValue
     */
    public Iterator iterator() {
        return values.iterator();
    }

}
