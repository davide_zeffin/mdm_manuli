/*****************************************************************************
    Class:        Bestseller
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      March 2001
    Version:      1.0

    $Revision: #2 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.businessobject.marketing;

import com.sap.isa.backend.boi.isacore.marketing.BestsellerData;
import com.sap.isa.businessobject.ProductList;

/**
 *
 * Bestseller
 *
 */
public class Bestseller extends ProductList
        implements BestsellerData{

}
