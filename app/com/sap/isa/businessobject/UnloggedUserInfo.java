/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Thorsten Mohr
  Created:      December 2001

*****************************************************************************/

package com.sap.isa.businessobject;

import com.sap.isa.core.TechKey;

/**
 * An object of the class <code>UnloggedUserInfo</code> should be used to store
 * informations of a known but unlogged user. The user is known if its technical
 * sold-to key is known.
 */
public class UnloggedUserInfo
{
    private TechKey soldToTechKey;

    /**
     * This one argument constructor initializes the object with a given technical
     * sold-to key.
     *
     * @param soldToTechKey technical key of the sold-to
     */
    public UnloggedUserInfo(TechKey soldToTechKey) {
        this.soldToTechKey = soldToTechKey;
    }

    /**
     * Returns the technical sold-to key of the user.
     */
    public TechKey getSoldToTechKey() {
        return soldToTechKey;
    }
}