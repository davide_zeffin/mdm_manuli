/*****************************************************************************
    Interface:    SearchCommand
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Thomas Smits
    Created:      29.3.2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/06/26 $
*****************************************************************************/

package com.sap.isa.businessobject;

import com.sap.isa.core.eai.*;

/**
 * Represents a search request performed against the underlying data storage.
 *
 * @author Thomas Smits
 * @version 1.0
 */
public interface SearchCommand {
    
   
   	/**
     * Perform the search in the backend.
     * 
     * @param bem With the backend object manager the search command get 
     *             access to the backend.
     * @return SearchResult 
     * @throws CommunicationException
     */
    public SearchResult performSearch(BackendObjectManager bem)
        throws CommunicationException;
    
}