/*****************************************************************************
    Class:        IsaQuickSearch
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP AG
    Created:      03.04.2001
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/26 $
*****************************************************************************/

package com.sap.isa.businessobject;

import java.util.Map;
import java.util.StringTokenizer;

import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.ICatalog;
import com.sap.isa.catalog.boi.IFilter;
import com.sap.isa.catalog.boi.IQuery;
import com.sap.isa.catalog.boi.IQueryStatement;
import com.sap.isa.catalog.filter.CatalogFilterFactory;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.catalog.webcatalog.WebCatItemList;
import com.sap.isa.isacore.IsaCoreInit;

/**
 * Class representing the business-object funtionality for
 * the Isa quickSearch.
 * Gets the attribute to search in from the appropriate action
 * and performs the search
 *
 * @author SAP AG
 * @version 1.0
 */
public final class IsaQuickSearch extends BusinessObjectBase {

    private IsaCatalogAttributes    attributes;
    private int                     displayHits;        // max. hits to display on one page, needed to calc the number of pages
    private int                     maxHits;            // max. hits to return from the search
    private int                     totalHits;          // total number of hits, less than or equal to maxHits
    private int                     curPage;            // should be saved in a hidden field
    private int                     curFrom;            // should be saved in a hidden field
    private int                     curTo;              // should be saved in a hidden field
    private int                     totalPages;         // number of pages returned by the last search coommand
    private int                     retCount;           // number of items returned by the last search command
    private String                  searchexpr;
    private WebCatInfo              webCatInfo;
    private String                  searchAttribute;    // the attribute to search in
    private int                     sortCol;            // the index of the attribute to sort for, this index is not the index in the array of attributes to search for, but in the index of the attributes to display.
    private int                     sortOrder;          // the order to sort in, 1 means ascending, -1 means descending
    private String[]                displayAttributes;  // attributes to display in the search's result-list
    private String                  backendType;        // the type of the backend that is currently used
    private boolean                exactSearch;        // perform an exact or non-exact search, it's defined in web.xml
    private ProductList 			 prodItems;			 // hitlist of last succesful search
    private boolean				 isValid;			 // shows whether current search result is valid - if false a new search has to be performed

    private CatalogFilterFactory    myFactory;

    private WebCatItemList          webCatItemList;
    
    public static final int ASCENDING_ORDER     = 1;    // sort the search results ascending
    public static final int DESCENDING_ORDER    = -1;   // sort the search results descending
	
    /**
     * Constructor comes first
     *
     */
    public IsaQuickSearch() {
        myFactory           = CatalogFilterFactory.getInstance();
        maxHits             = -1;  // value below zero means error, zero means "return all items"
        totalHits           = -1;  // value below zero means error, zero means "nothing found"
        displayHits         = 0;
        attributes          = new IsaCatalogAttributes();
        webCatInfo          = null;
        searchAttribute     = "";
        searchexpr			= "";
        sortCol             = 0;
        sortOrder           = ASCENDING_ORDER;  // the only supported order in this version
        curPage             = 1;
        curFrom             = 1;
        curTo               = 0;
        totalPages          = 0;
        displayAttributes   = new String[2];
        exactSearch         = false;
        prodItems 			= null;
        isValid				= false;

        // the two "standard" attributes from the index to display
        // OBJECT_ID and OBJECT_DESCRIPTION
        // debug:HACK:begin
        displayAttributes[0] = "OBJECT_ID";
        displayAttributes[1] = "OBJECT_DESCRIPTION";
        // debug:HACK:end
    }


    /**
     * Gets the attributes
     *
     * @return The IsaCatalogAttributes set here
     */
    public IsaCatalogAttributes getAttributes() {
        return this.attributes;
    }

    /**
     * Set the attributes to search in
     *
     * @param attributeIds The array of the guids of the attributes to search in
     */
    public void setAttributes( IsaCatalogAttributes attributes ) {
        this.attributes = attributes;
    }

    /**
     * Get the max. number of hits to read from the catalog engine
     *
     * @return maxHits The maximum number of hits to read from the catalog engine
     */
    public int getMaxHits() {
        return this.maxHits;
    }

    /**
     * Set the max. of hits to retad from the catalog engine
     *
     * @param maxHits The maximum amount of items to read
     */
    public void setMaxHits( int maxHits ) {
    	if (this.maxHits != maxHits) {
    		// if maxHits in form has changed, the saved hitlist is invalid
    		setIsValid(false);
    	}
        this.maxHits = maxHits;
    }

    /**
     * Get the max. number of items to return to the "search-client"
     *
     * @return displayHits The maximum amount of items to return
     */
    public int getDisplayHits() {
        return this.displayHits;
    }

    /**
     * Get the max. number of items to return to the "search-client" as a String
     *
     * @return displayHits The maximum amount of items to return
     */
    public String getDisplayHitsAsString() {
        return Integer.toString(this.displayHits);
    }

    /**
     * Set the max. number of items to return to the "search-client"
     *
     * @param displayHits The maximum amount of items to return
     */
    public void setDisplayHits( int displayHits ) {
    	if (this.displayHits != displayHits) {
    		// if displayHits in form has changed, the saved hitlist is invalid
    		setIsValid(false);
    	}
        this.displayHits = displayHits;
    }

    /**
     * Get the current page of the search results
     *
     * @return curPage The current page
     */
    public int getCurPage() {
        return this.curPage;
    }

    /**
     * Set the current page
     * Do NOT use this method to toggle the pages in the result list !!
     *
     * @param curPage The new current page
     */
    public void setCurPage( int curPage ) {
        this.curPage = curPage;
    }

    /**
     * Get the current starting row of the search
     *
     * @return curFrom The current starting line
     */
    public int getCurFrom() {
        return this.curFrom;
    }

    /**
     * Set the current starting line
     * Do NOT use this method to toggle the pages in the result list !!
     *
     * @param curFrom The new starting line
     */
    public void setCurFrom( int curFrom ) {
        this.curFrom = curFrom;
    }

    /**
     * Get the current last row of the search
     *
     * @return curTo The last line
     */
    public int getCurTo() {
        return this.curTo;
    }

    /**
     * Set the last row of the search result
     * Do NOT use this method to toggle the pages in the result list !!
     *
     * @param curTo The new last row
     */
    public void setCurTo( int curTo ) {
        this.curTo = curTo;
    }

    /**
     * Get the expression to search for
     *
     * @return searchexpr The expression to search for
     */
    public String getSearchexpr() {
        return this.searchexpr;
    }

    /**
     * Set the expression to search for
     *
     * @param searchexpr The search expression
     */
    public void setSearchexpr( String searchexpr ) {
    	if (!this.searchexpr.equals(searchexpr)) {
    		// if searchexpression in form has changed, the saved hitlist is invalid
    		setIsValid(false);
    	}
        this.searchexpr = searchexpr;
    }

    /**
     * Get the catalog to search in
     *
     * @return webCatInfo The WebCatInfo class to use for this search
     */
    public WebCatInfo getWebCatInfo() {
        return this.webCatInfo;
    }

    /**
     * Set the catalog to search in
     *
     * @param webCatInfo The WebCatInfo class to use for this search
     */
    public void setWebCatInfo( WebCatInfo webCatInfo ) {
        this.webCatInfo = webCatInfo;
    }

    /**
     * Get the attribute to search in
     *
     * @return searchAttribute The attribute to search in
     */
    public String getSearchAttribute() {
        return this.searchAttribute;
    }

    /**
     * Set the attribute to search in
     *
     * @param searchAttribute The attribute to search in
     */
    public void setSearchAttribute(String searchAttribute) {
    	if (!this.searchAttribute.equals(searchAttribute)) {
    		// if searchattribute in form has changed, the saved hitlist is invalid
    		setIsValid(false);
    	}
        this.searchAttribute = searchAttribute;
    }

    /**
     * Get the index of the attribute to sort after
     *
     * @return sortCol The index of the attribute to sort after
     */
    public int getSortCol() {
        return this.sortCol;
    }

    /**
     * Set the index of the attribute to sort after
     *
     * @param sortCol The index of the attribute to sort after
     */
    public void setSortCol(int sortCol) {
        this.sortCol = sortCol;
    }

    /**
     * Get the total number of pages for the current search calculated using the current value of displayHits
     *
     * @return totalPages The number of pages returned by the last search command
     */
    public int getTotalPages() {
        return this.totalPages;
    }


    /**
     * Get the number of items returned by the last search command
     *
     * @return  The number of items returned by the last search command
     */
    public int getRetCount() {
        return this.retCount;
    }

    /**
     * Get the number of items returned by the last search command as String
     *
     * @return  The number of items returned by the last search command
     */
    public String getRetCountAsString() {
        return Integer.toString(getRetCount());
    }

    /**
     * Set the number of items returned by the last search command
     *
     * @param retCount The number of items returned by the last search command
     */
    private void setRetCount(int retCount) {
        this.retCount = retCount;
    }

    /**
     * Get the total number of items returned by the last firstsearch command
     *
     * @return  The number of items returned by the last firstsearch command
     */
    public int getTotalHits() {
        return this.totalHits;
    }

    /**
     * Get the number of items returned by the last firstsearch command as String
     *
     * @return  The number of items returned by the last firstsearch command
     */
    public String getTotalHitsAsString() {
        return Integer.toString(getTotalHits());
    }


    /**
     * decide if we perform an exact- or non-exact search
     * 
     * @param exact
     */
    public void setExactSearch(boolean exact) {
        exactSearch = exact;
    }
    
    /**
     * ask the flag if we perform an exact- or non-exact search
     * 
     * @return exact
     */
    public boolean getExactSearch() {
        return exactSearch;
    }

    /**
     * Perform the search starting at the 1st item
     *
     * @return items The items found after performing the search
     */
    public ProductList firstsearch() {
		final String METHOD = "firstsearch()";
		log.entering(METHOD);
        setCurPage( 1 );
        setCurFrom( 1 );
        setCurTo( maxHits );

        // bad thing this one
        // but the catalog does not retrieve the number of hits only
        // so I select one small attribute and count the number of retrieved items,
        // "POS_NR" here. This attribute should be included in all IMS indices.
        // the "real" search selects all attributes
        String[] countAttr = new String[1];
        countAttr[0] = "POS_NR";   // this seems to me the attribute with the smallest size
        search(countAttr, 1, maxHits);
		try {
		
	        if ( retCount >= maxHits ) {
	            return null;
	        }
	        else  if( retCount > 0 ) {
	            // calculate the number of pages
	            totalHits  = retCount;
	            totalPages = retCount / displayHits;
	            if ( retCount % displayHits != 0 ) {
	                totalPages++;
	            }
	            return gotoPage( 1 );
	        }
	        else {
	            return null;
	        }
		}finally{
			log.exiting();
		}
    }

    /**
     * Perform the search
     * the number of found items can be retrieved using the method getRetcount
     *
     * @param fromRow The starting number of the row of the table that will be returned
     * @param toRow The ending number of the row of the table that will be returned
     *
     * @return items The items found after performing the search
     */
    public ProductList search( int fromRow, int toRow ) {
        return search( displayAttributes, fromRow, toRow );
    }

    /**
     * Perform the search
     * the number of found items can be retrieved using the method getRetcount
     *
     * @param attributeList An array of Strings that define the names of the columns to return
     * @param fromRow The starting number of the row of the table that will be returned
     * @param toRow The ending number of the row of the table that will be returned
     *
     * @return items The items found after performing the search
     */
    public ProductList search( String[] attributeList, int fromRow, int toRow ) {
		final String METHOD = "search()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("fromRow = "+fromRow+ " toRow = "+toRow );
        // map the parameter to the corresponding entry in the list of displayed attributes
        String[] sortBy = new String[1];
        sortBy[0] = displayAttributes[sortCol];
        log.exiting();
        return search( attributeList, sortBy, fromRow, toRow );
    }

    /**
     * Perform a sorted search
     * the number of found items can be retrieved using the method getRetcount
     *
     * @param sortCol The column to sort the results by. It is the index of the String-Array containing the attributes to display, starting with 0
     *
     * @return items The items found after performing the search
     */
    public ProductList sort( int sortCol ) {
		 // map the parameter to the corresponding entry in the list of displayed attributes
        String[] sortBy = new String[1];

        setSortCol(sortCol);

        sortBy[0] = displayAttributes[sortCol];
		return search( displayAttributes, sortBy, curFrom, curTo );
    }

    /**
     * creates a filter-object for the call to the search API
     * @param attribute The attribute to search in
     * @param expression The expression to search for
     */
    private IFilter createSearchFilter(String attribute, String expression) {
        IFilter filter = null;
        
        if ("CRM".equalsIgnoreCase(backendType)) {
            if( exactSearch == true) {
                filter = myFactory.createAttrEqualValue(attribute, expression);
            }
            else {
                filter = myFactory.createAttrContainValue(attribute, expression);
            }
        }
        else {
            // AttrContainValue does not work here in the R/3 environment
            filter  = myFactory.createAttrEqualValue(attribute, expression);
        }
            
        return filter;
    }

    /**
     * Perform the search
     * the number of found items can be retrieved using the method getRetcount
     * the order of the returned items can be defined using the sortOrder argument
     *
     * Because the R/3-adapter's search API can not handle combined search-statements,
     * like ROLE != B AND some-attribute == 0815, we have to distinguish between the CRM
     * and the R/3-backends. The type of the backend is held in the attribute backendType
     * and it equals to CRM if we have a CRM system.
     *
     * @param attributeList An array of Strings that define the names of the columns to return
     * @param sortAttributes An array of Strings that define the attributes the result is sorted by. We currently use only one attribute. The elements are sorted ascending.
     * @param fromRow The starting number of the row of the table that will be returned
     * @param toRow The ending number of the row of the table that will be returned
     *
     * @return items The items found after performing the search
     */
    public ProductList search( String[] attributeList, String[] sortAttributes, int fromRow, int toRow ) {
		final String METHOD = "search()";
		log.entering(METHOD);
        IFilter completeFilter  = null;
        IFilter workFilter      = null;
        
        IsaCoreInit ici = IsaCoreInit.getInstance();
        
        // get those multiple search attribute list
        Map multipleAttrMap = ici.getMultipleAttrSearchMap();
        
        // if the multipleAttrList is empty, behave like before
        // else check if the selected attribute is in our list and which 
        // attributes it maps to.
        
        // don't care about accessories here 
        // the catalog filters them anyway
            
        if (multipleAttrMap.isEmpty() || multipleAttrMap.get(searchAttribute) == null) {
            // behave like before
            completeFilter = createSearchFilter(searchAttribute, searchexpr);
            
            if (log.isDebugEnabled()) {
                log.debug("Perform single attribute search");
            }
        }
        else {
            String searchAttribs = (String) multipleAttrMap.get(searchAttribute);
            // resolve attribute list and create search command combined from
            // OR concatenated search filters for the attributes
            StringTokenizer st = new StringTokenizer(searchAttribs, ",");
            while (st.hasMoreTokens()) {
                String attribute = st.nextToken().trim();
                if (attribute.length() > 0 ) {
                    workFilter = createSearchFilter(attribute, searchexpr);
                    if (completeFilter == null) {
                        completeFilter = workFilter;
                    }
                    else {
                        completeFilter = myFactory.createOr(completeFilter, workFilter);
                    }
                }  
            } // endwhile
            if (log.isDebugEnabled()) {
                log.debug("Perform multiple attribute search with key " + searchAttribute + " and list : " + searchAttribs);
            }
        }

        // get the catalog
        ICatalog catalog = webCatInfo.getCatalog();

        // create the querystatement
        IQueryStatement statement   = null;
        IQuery query                = null;
        try {
            statement = catalog.createQueryStatement();
            // set the statement
            //  statement.setStatement(filter, attributeList, sortAttributes, fromRow, toRow);
            // don't explicitly define the attributes to retrieve here because if
            // we have list-prices we need to read the "attribute" price from the index
            // and we don't know the name of the attribute the price is stored in
            statement.setStatement(completeFilter, null, sortAttributes, fromRow, toRow);

            // get the query interface
            query = catalog.createQuery(statement);

        }
        catch(CatalogException e) {
        	log.exiting();
            return null;
        }

        // put the statement into the WebCatItemList
        // the query is implicitely submitted when I use this
        // constructor
        if (log.isDebugEnabled()) {
            log.debug("Calling the WebCatItemList's constructor");
        }
        
        webCatItemList = new WebCatItemList( webCatInfo, query, false );

        // set for catalog management
        // webCatInfo.setCurrentItemList(webCatItemList);

        // get the items from the catalog
        // the items have the type WebCatItem
        if (log.isDebugEnabled()) {
            log.debug("Stuffing it all into the Productlist");
        }
        prodItems = new ProductList(webCatItemList);

        // set the number of items
        if (log.isDebugEnabled()) {
            log.debug("Get the number of found items from the webCatItemList");
        }
        setRetCount( webCatItemList.getItems().size() );

        if (log.isDebugEnabled()) {
            log.debug("Goodbye");
        }
        
        if (prodItems.size() > 0) {
        	setIsValid(true);
        }
        else {
        	setIsValid(false);
        }
        log.exiting();
        return prodItems;
    }

    /**
     * Leaf through the results of a former search
     *
     * @param  direction The direction to leaf to, 1 means forward, -1 means backward
     *
     * @return items     The items found after performing the search
     */
    public ProductList leafThrough( int direction)
    {
        // we calculate the new limits for the search
        // by incrementing or decrementing the page
        return gotoPage( getCurPage() + direction );
    }

    /**
     * go to the page passed as argument
     *
     * @param  page The page to retrieve the items from
     *
     * @return items  The items found after performing the search
     */
    public ProductList gotoPage( int page)
    {
        // calculate the "new" values for fromRow and toRow using the curPage
        // and the displayHits
        int fromRow = 1 + (page - 1) * displayHits;
        int toRow   = fromRow + displayHits - 1;
        
       
        // set the values
        setCurPage( page );
        setCurFrom( fromRow );
        setCurTo( toRow );

		WebCatItemList list = new WebCatItemList(webCatInfo);
		if(webCatItemList != null){
			WebCatItem item = null;
			int start = fromRow - 1;
			int end = Math.min(toRow,webCatItemList.size());
			for(int i = start; i < end; i++){
				item = webCatItemList.getItem(i);
				if(item.isMainItem()){
					list.addItem(item);
				}
				else{
					i--;
				}
			}
		}
		return new ProductList(list);
        // re-perform the search
        //return search( fromRow, toRow );

    }

    /**
     * set the backend type
     *
     * @param  backendType The type of backend that is currently used
     *
     */
    public void setBackendType(String backendType)
    {
        this.backendType = backendType;
    }
	
	
	/**
	 * Returns the found prodItems of the search.
	 * If a valid hitlist for the given search criteria is already stored this is returned 
	 * without a new search; otherwhise a new search is initiated.
	 * @return ProductList
	 */
	public ProductList getProdItems() {
		if (prodItems == null || !isValid()) {
			return firstsearch();
		}
		else  {
    		return gotoPage(curPage);

		}
		
	}

	/**
	 * Sets the prodItems.
	 * @param prodItems The prodItems to set
	 */
/*
	public void setProdItems(ProductList prodItems) {
		this.prodItems = prodItems;
	}
*/

	/**
	 * Returns the isValid flag for the stored hitlist; 
	 * if true, the stored hitlist can be displayed otherwhise
	 * a new search has to be performed
	 * @return boolean
	 */
	public boolean isValid() {
		return isValid;
	}


	/**
	 * Sets the isValid flag
	 * false: a new search has to be performed
	 * true:  stored hitlist is valid an can be shown 
	 * @param isValid The isValid to set
	 */
	public void setIsValid(boolean isValid) {
		this.isValid = isValid;
	}
	
	
	/**
	 * Clears data of the IsaQuickSearch-Object after invalid search requests.
	 * In this case the stored hitlist is invalid and some attributes
	 * of the IsaQuickSearchObject have to be set to initial values.
	 * Restored attributes: searchexpression, totalHits
	 */
	public void setInitialData() {
		// method necessary because in case of an invalid search request the stored hitlist
		// is marked as invalid but the IsaQickSearch object still contains the data of the last
		// successful search. This old data (searchexpression, searchattribute) would be displayed
		// in search form. In order to get an initial search form, these data are set to initial values. 
		this.searchexpr = "";
		this.searchAttribute = "";
		this.totalHits = -1;		// value below zero means error (no valid last search)
		this.maxHits = -1;			// value below zero means error, zero means "return all items"
        this.displayHits = 0;
        
	}


    /**
     * Return the web cat item which was create while the search. <br>
     * 
     * @return WebCatItemList
     */
    public WebCatItemList getWebCatItemList() {
        return webCatItemList;
    }

}
