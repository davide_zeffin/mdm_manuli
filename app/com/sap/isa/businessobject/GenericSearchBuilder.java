/*****************************************************************************
    Class:        GenericSearchBuilder
    Copyright (c) 2003, SAP AG, Germany, All rights reserved.

    $Revision: #9 $
    $DateTime: 2004/03/03 16:43:39 $ (Last changed)
    $Change: 174122 $ (changelist)
    
*****************************************************************************/
package com.sap.isa.businessobject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.maintenanceobject.backend.boi.MaintenanceObjectBackend;
import com.sap.isa.maintenanceobject.businessobject.GSAllowedValue;
import com.sap.isa.maintenanceobject.businessobject.GSProperty;
import com.sap.isa.maintenanceobject.businessobject.GSPropertyGroup;
import com.sap.isa.maintenanceobject.businessobject.GSScreenGroup;
import com.sap.isa.maintenanceobject.businessobject.MaintenanceObject;


/**
 * This class is a implemented version of the MaintenanceObject class, and used 
 * to retrieve the configuration information from the XML storage (generic-searchbackend-config.xml).
 * <p>
 * Part of the generic search framework.
 * </p>
 *
 * @author SAP AG
 * @version 1.0
 *
 */

public class GenericSearchBuilder extends MaintenanceObject
        implements Cloneable {

    /**
     * Possible type of a screen group
     */
    public static final String TYPE_SEARCH = "search";
	/**
	 * Possible type of a screen group
	 */
	public static final String TYPE_RESULT = "result";

    /**
     * Just to fulfill the requirements from the super class.<br>
     * <b>Returns always null</b>
     */
    protected MaintenanceObjectBackend getBackendService() throws BackendException {
	    return null;
    }

    /**
     * Return a screen group either of type <code>SEARCH</code> or <code>RESULT</code>
     * @param type
     * @return GSScreenGroup
     */
	protected GSScreenGroup getScreenGroupOfType(String type) {
		GSScreenGroup gssg = null;
		GSScreenGroup gssgReturn = null;

		// No return if type is not provided
		if (type == null) return null;
		
		List sgl = super.getScreenGroupList();

		for (int i=0; i < sgl.size(); i++) {
			gssg = (GSScreenGroup)sgl.get(i);
			if (type.equals(gssg.getType())) {
				gssgReturn = gssg;
			}
		}
		if (gssgReturn == null) {
			log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "No GSScreenGroup of Type '" + type + "' found");
		}
		return gssgReturn;
	}

    /**
     * Return the Resultlist description which is assigned to this search request. 
     * The search request is a collection of properties, where one of the properties
     * has a attribute <code>implementationfilter<code>. The AllowedValues of this
     * Property can have a different Resultlist name for each value. If not the 
     * Resultlist name will be taken from the Property itself.  
     * @param String Name of the PropertyGroup responsible for the search criteria
     * @param String Name of the Property marked as "implementationFilter"
     * @param String Name of the selected Value (from the AllowedValue list)
     * @return PropertyGroup describing the resultlist
     */
    public GSPropertyGroup getResultListDescription(String ptyGrpName, String ptyName, String avValue) {
		final String METHOD = "getResultListDescription()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("propertyGroup="+ptyGrpName+", propertyName="+ptyName+", selectedValue="+avValue);
    	String resultlistName = null;
    	
    	GSPropertyGroup ptyGrp = getPropertyGroupByName(ptyGrpName, GenericSearchBuilder.TYPE_SEARCH);
    	if (ptyGrp == null) return null;
        GSProperty pty = (GSProperty)ptyGrp.getProperty(ptyName);
        if (pty == null) return null;
        Iterator itAV = pty.iterator();
        // Check AllowedValues carry a separat resultlist name
        while (itAV.hasNext()) {
            GSAllowedValue av = (GSAllowedValue)itAV.next();
    		if (avValue.equals(av.getValue())) {
    			resultlistName = av.getResultlistName();
    		}
    	}
    	if (resultlistName == null &&
    	    pty.getResultlistName() == null) {
    		log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "GSProperty with 'implementationFilter' but without 'resultlistName'");
    		log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "GSScreenGroup: " + getScreenGroupOfType(TYPE_SEARCH).getName() +
    		          " :  GSPropertyGroup: " + ptyGrp.getName() + ": avValue: " + avValue);
    	}
    	if (resultlistName == null  &&
    	    pty.getResultlistName() != null) {
    	    resultlistName = pty.getResultlistName();
    	}
    	// Next Step, return the propery group with this Name in the screen group
    	// of type 'RESULT' 
        GSPropertyGroup resultList = (GSPropertyGroup)getPropertyGroupByName(resultlistName, GenericSearchBuilder.TYPE_RESULT);
        if (resultList != null) {
            resultList = reorderOutputSequence(resultList);
        }
        log.exiting();
    	return resultList;
    }
    /**
     * Return <code>maxHitsToSelect</code> attribute which is assigned to this search request. 
     * The search request is a collection of properties, where one of the properties
     * has a attribute <code>implementationfilter<code>. The AllowedValues of this
     * Property can have a different Resultlist name for each value. If not the 
     * Resultlist name will be taken from the Property itself.  
     * @param String Name of the PropertyGroup responsible for the search criteria
     * @param String Name of the Property marked as "implementationFilter"
     * @param String Name of the selected Value (from the AllowedValue list)
     * @return int max hits parameter
     */
    public int getMaxHitsOption(String ptyGrpName, String ptyName, String avValue) {
		final String METHOD = "getMaxHitsOption()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("propertyGroup="+ptyGrpName+", propertyName="+ptyName+", selectedValue="+avValue);
        int maxHits = -1;
        
        GSPropertyGroup ptyGrp = getPropertyGroupByName(ptyGrpName, GenericSearchBuilder.TYPE_SEARCH);
        if (ptyGrp == null) return 0;
        GSProperty pty = (GSProperty)ptyGrp.getProperty(ptyName);
        if (pty == null) return 0;
        Iterator itAV = pty.iterator();
        // Check AllowedValues carry a separat max hits attribute
        while (itAV.hasNext()) {
            GSAllowedValue av = (GSAllowedValue)itAV.next();
            if (avValue.equals(av.getValue())) {
                maxHits = av.getMaxHitsToSelect();
            }
        }
        // Not yet specified, take default from property
        if (maxHits == -1) {
            maxHits = pty.getMaxHitsToSelect();
        }
        log.exiting();
        return (maxHits == -1 ? 0 : maxHits);
    }
    /**
     * Method re-orders sequence of the property of the given property-group. The re-order process
     * is driven by the fact, a property could have set the attribute <code>writeUnderProperty</code> (to
     * create a multi line resultlist). <br>
     * It also takes care for special attributes $MSGLINE and $MSGTYPE. Both will be added to the very
     * end of the list.
     * @param GSPropertyGroup which should be re-ordered
     * @return GSPropertyGroup re-order one
     */
    protected GSPropertyGroup reorderOutputSequence(GSPropertyGroup ptg) {
        GSPropertyGroup newPtg = new GSPropertyGroup();
        // Set relevant attributes
        newPtg.setBodyInclAfterResult(ptg.getBodyInclAfterResult());
        newPtg.setBodyInclBeforeResult(ptg.getBodyInclBeforeResult());
        newPtg.setExpandedResultlist(ptg.isExpandedResultlist());
        newPtg.setMsgKeyObjectFound(ptg.getMsgKeyObjectFound());
        // --
        Map dpPty = new HashMap();
        // First build table which includes per property its sub properties (as ArrayList)
        Iterator itPTG = ptg.iterator();
        while (itPTG.hasNext()) {
            GSProperty pty = (GSProperty)itPTG.next();
            if (pty.getWriteUnderProperty() != null  &&  pty.getWriteUnderProperty().length() > 0) {
                List depPty = null;
                if (dpPty.get(pty.getWriteUnderProperty()) == null) {
                    // Property is not yet occured
                    depPty = new ArrayList();
                    dpPty.put(pty.getWriteUnderProperty(), depPty);
                } else {
                    depPty = (List)dpPty.get(pty.getWriteUnderProperty());
                }
                depPty.add(pty);
            }
        }
        // Second: build up an array list with the right sequence of the properties
        itPTG = ptg.iterator();
        List rPty = new ArrayList();
        while (itPTG.hasNext()) {
            GSProperty pty = (GSProperty)itPTG.next();
            if ( ! rPty.contains(pty)) {
                reorderOutputSequence2(pty, rPty, dpPty);
            }
        }
        // Third: new property group considering the new property order given by the ArrayList
        GSProperty ptyMSGL = null; 
        GSProperty ptyMSGT = null;
        if (rPty != null) {
            itPTG = rPty.iterator();
            while (itPTG.hasNext()) {
                GSProperty nPty = (GSProperty)itPTG.next();
                if ("$MSGLINE".equals(nPty.getName())) {
                	ptyMSGL = nPty;
                } else if ("$MSGTYPE".equals(nPty.getName())) {
					ptyMSGT = nPty;
				} else {
					newPtg.addProperty(nPty);
				}
            }
        }
        // If occured add special properties $MSGLINE and $MSGTYPE
        if (ptyMSGL != null) {
        	 newPtg.addProperty(ptyMSGL);
        }
		if (ptyMSGT != null) {
			 newPtg.addProperty(ptyMSGT);
		}
        // Set missing attribute
        newPtg.setName(ptg.getName());
        return newPtg;
    }
    /**
     * Adds the depending properties to the property list.
     * Recursive Methode!
     * @param GSProperty property which should be investigated
     * @param List list of properties which should be build up
     * @param Map containing dependend properties
     */
    protected void reorderOutputSequence2(GSProperty pty, List rPty, Map dpPty) {
        if (pty.getWriteUnderProperty() ==  null  ||  pty.getWriteUnderProperty().length() <= 0) {
             // Property is not depending of another one (otherwise it will be added 
             // in the loop below)
             rPty.add(pty);
         }
         // Now add the depending properties immediately below
         List depPty =  (List)dpPty.get(pty.getName());
         if (depPty != null) {
             Iterator itDEPPTY = depPty.iterator();
             while (itDEPPTY.hasNext()) {
                 GSProperty dPty = (GSProperty)itDEPPTY.next();
                 rPty.add(dPty);
                 reorderOutputSequence2(dPty, rPty, dpPty);
             }
         }
    }
    
    /**
     * Return the PropertyGroup of that name. The name can be suffixed by the location (@loation). The
     * location will be removed to gain the original name fitting to the XML file.
     * @param String Property Group Name (which could be suffixed by the location @location)
     * @param String Screen Group Type (search or resultlist)   
     */
    public GSPropertyGroup getPropertyGroupByName(String ptyGrpName, String scrGrpType) {
        // Is Location provided (searchDescription@location)
        String ptyGrpNameTmp = ptyGrpName; 
        if (ptyGrpName.indexOf("@") >= 0) {
            ptyGrpNameTmp = ptyGrpName.substring(0, ptyGrpName.indexOf("@"));            
        }
    	GSScreenGroup scrGrp = getScreenGroupOfType(scrGrpType);
    	if (scrGrp == null) return null;
        GSPropertyGroup ptyGrp = (GSPropertyGroup)scrGrp.getPropertyGroup(ptyGrpNameTmp);
        String scrGrpTypeDes = "search description";
        if ( ! "search".equals(scrGrpType)) {
            scrGrpTypeDes = "result list";
        }
        if (ptyGrp == null) {
            log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "No GSPropertyGroup '" + ptyGrpNameTmp + "' as " + scrGrpTypeDes + " found");
        }
        return ptyGrp;
    }
    
    /**
     * Return the search criteria assigned to this name.
     * <p>&lt;property-group <br>name="SearchCriteria_B2B"<br>&gt;</p>
     * @param Name of the search criteria
     * @return PropertyGroup descriping the search criteria
     */
    public GSPropertyGroup getSearchCriteriaDescription(String ptyGrpName) {
		return ( (GSPropertyGroup)getPropertyGroupByName(ptyGrpName, GenericSearchBuilder.TYPE_SEARCH) );
    }

    /**
     * Return the full qualified name of the Class with will be used to 
     * build the genericsearch.jsp.
     * @return UIClassName 
     */
    public String getUIClassName() {
		final String METHOD = "getUIClassName()";
		log.entering(METHOD);
		// Set default
    	String retUIClassName = "com.sap.isa.ui.uiclass.genericsearch.GenericSearchBaseUI";
    	
    	 GSScreenGroup scrGrp = getScreenGroupOfType(TYPE_SEARCH);
    	 if (scrGrp.getUiClassName() != null  &&  scrGrp.getUiClassName().length() > 0) {
    	 	retUIClassName = scrGrp.getUiClassName();
    	 }
    	if (log.isDebugEnabled()) log.debug("Returning UI class name is '" + retUIClassName + "'"); 
    	log.exiting();
    	return retUIClassName;
    }
}
