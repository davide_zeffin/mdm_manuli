/*****************************************************************************
    Class:        IsaCatalogAttributes
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Alexander Staff
    Created:      27.3.2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/06/26 $
*****************************************************************************/

package com.sap.isa.businessobject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.sap.isa.catalog.impl.CatalogAttribute;
import com.sap.isa.core.Iterable;

/**
 * Class representing the list of the catalog's attributes
 *
 * @author Alexander Staff
 * @version 1.0
 */
public final class IsaCatalogAttributes implements Iterable {

    // the list of attributes
    private List attributes;

    /**
     * Creates a new instance
     *
     */
    public IsaCatalogAttributes() {
        attributes = new ArrayList();
    }

    /**
     * Returns the list of the attributes
     *
     * @return attributes
     */
    public List getAttributes() {
        return this.attributes;
    }

    /**
     * Sets the list of attributes
     *
     * @param attributes List of attributes
     */
    public void setAttributes( List attributes ) {
        this.attributes = attributes;
    }

    /**
     * Returns the attribute at position index
     *
     * @param index Index of the attribute to return
     * @return attribute
     */
    public CatalogAttribute getAttribute( int index) {
        return (CatalogAttribute) this.attributes.get(index);
    }

    /**
     * Sets the attribute at position index
     *
     * @param index Index to set the attribute at
     * @param attribute Attribute to set
     */
    public void setAttribute( int index, CatalogAttribute attribute ) {
        this.attributes.set(index,  attribute);
    }


    /**
     * adds an attribute to the list
     *
     * @param attribute The attribute to add as a CatalogAttribute
     */
    public void addAttribute( CatalogAttribute attribute ) {
        this.attributes.add( attribute );
    }

    /**
     * returns the number of attributes in this list
     *
     */
    public int getAttributeCount( ) {
        return this.attributes.size();
    }

    /**
     * gets the guid of an attribute in the list
     *
     * @param index The index of the attribute in the list
     */
    public String getAttributeGuid( int index ) {
        CatalogAttribute attribute = (CatalogAttribute) this.attributes.get(index);
        return attribute.getGuid();
    }

    /**
     * gets the name of an attribute in the list
     *
     * @param index The index of the attribute in the list
     */
    public String getAttributeName( int index ) {
        CatalogAttribute attribute = (CatalogAttribute) this.attributes.get(index);
        return attribute.getName();
    }

    /**
     * Returns an iterator over the attributes
     *
     * @return Iterator for this object
     */
    public Iterator iterator () {
        return this.attributes.iterator();
    }

}
