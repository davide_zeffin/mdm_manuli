/*****************************************************************************
    Class:        ProductMap
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Sabine Heider
    Created:      09.05.2001
    Version:      1.0

*****************************************************************************/

package com.sap.isa.businessobject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.core.Iterable;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.logging.IsaLocation;

/**
 * Represents a Map of Product objects.
 * <p>
 * This class can be used to maintain a collection of product objects
 * and efficiently look them up using a unique key.
 * The internal storage is organized using a Map, so duplicates of items
 * are not stored.
 * </p>
 *
 * @author Sabine Heider
 * @version 1.0
 * @see Product Product
 *
 * @stereotype collection
 */
public class ProductMap implements Iterable {
	
    private Map products = new HashMap();
	static final private IsaLocation loc = IsaLocation.getInstance(ProductMap.class.getName());


    /**
     * Adds a new <code>Product</code> to the Map.
     *
     * @param key Key uniquely identifying the product
     * @param product Product to be stored in the <code>ProductMap</code>
     */
    public void put(TechKey key, Product product) {
        products.put(key, product);
    }


    /**
     * Removes a product identified by the key under which it has been stored.
     *
     * @param key Key uniquely identifying the product to be removed.
     */
    public void remove(TechKey key) {
        products.remove(key);
    }


    /**
     * Adds a new <code>Product</code> to the Map.
     *
     * @param product Product to be stored in the <code>ProductMap</code>
     */
    public void add(Product product) {
        products.put(product.getTechKey(), product);
    }


    /**
     * Returns the <code>Product</code> to which this map maps the specified
     * key. Returns <code>null</code> if the map contains no mapping for this
     * key. A return value of null does not necessarily indicate that the map
     * contains no mapping for the key; it's also possible that the map
     * explicitly maps the key to null. The containsKey operation may be
     * used to distinguish these two cases.
     *
     * @param key key whose associated product is to be returned.
     * @return the product to which this map maps the specified key
     *
     */
    public Product get(TechKey key) {
        return (Product) products.get(key);
    }


    /**
     * Removes all mappings from this map.
     */
    public void clear() {
        products.clear();
    }


    /**
     * Returns the number of key-value mappings in this map.
     *
     * @return the number of key-value mappings in this map.
     */
    public int size() {
        return products.size();
    }


    /**
     * Returns true if this map contains no key-value mappings.
     *
     * @return <code>true</code> if this map contains no key-value mappings.
     */
    public boolean isEmpty() {
        return products.isEmpty();
    }


    /**
     * Returns true if this map contains a mapping for the specified key.
     *
     * @param key key whose presence in this map is to be tested.
     * @return <code>true</code> if this map contains a mapping for the
     *         specified key.
     */
    public boolean containsKey(TechKey key) {
        return products.containsKey(key);
    }


    /**
     * Returns true if this map maps one or more keys to the specified value.
     *
     * @param value value whose presence in this map is to be tested.
     * @return <code>true</code> if this map maps one or more keys to the
     *          specified value.
     */
    public boolean containsValue(Product value) {
        return products.containsValue(value);
    }


    /**
     * Returns an iterator over the elements contained in the
     * <code>ProductMap</code>.
     *
     * @return Iterator for this object
     */
    public Iterator iterator() {
        return products.values().iterator();
    }


    /**
     * <p>
     * Compares the specified object with this map for equality.
     * Returns true if the given object is also a map and the
     * two maps represent the same mappings. More formally,
     * two maps <code>t1</code> and <code>t2</code> represent the same mappings
     * if <code>t1.keySet().equals(t2.keySet())</code> and for every
     * key k in <code>t1.keySet(), (t1.get(k)==null ? t2.get(k)==null :
     * t1.get(k).equals(t2.get(k)))</code> .
     * This ensures that the equals method works properly across
     * different implementations of the map interface.
     * </p>
     * <p>
     * This
     * implementation first checks if the specified object is
     * this map; if so it returns true. Then, it checks if the
     * specified object is a map whose size is identical to the
     * size of this set; if not, it it returns false. If so,
     * it iterates over this map's entrySet collection, and
     * checks that the specified map contains each mapping that
     * this map contains. If the specified map fails to contain
     * such a mapping, false is returned. If the iteration completes,
     * true is returned.
     *
     * @param o object to be compared for equality with this map
     * @return <code>true</code> if the specified object is equal to this map
     */
    public boolean equals(Object o) {
       if (o == null) {
            return false;
        }
        else if (o == this) {
            return true;
        }
        else if (!(o instanceof ProductMap)) {
            return false;
        }
        else {
            return products.equals(((ProductMap)o).products);
        }
    }


    /**
     * <p>
     * Returns the hash code value for this map. The hash code
     * of a map is defined to be the sum of the hash codes of
     * each entry in the map's entrySet() view. This ensures
     * that <code>t1.equals(t2)</code> implies that
     * <code>t1.hashCode()==t2.hashCode()</code>
     * for any two maps <code>t1</code> and <code>t2</code>, as required by
     * the general
     * contract of <code>Object.hashCode</code>. This implementation iterates
     * over <code>entrySet()</code>, calling <code>hashCode</code>
     * on each element (entry)
     * in the Collection, and adding up the results.
     * </p>
     *
     * @return the hash code value for this map
     */
    public int hashCode() {
        return products.hashCode();
    }


     /**
      * <p>
      * Returns a string representation of this map. The string
      * representation consists of a list of key-value mappings
      * in the order returned by the map's <code>entrySet</code> view's
      * iterator, enclosed in braces ("{}").
      * Adjacent mappings are separated by the
      * characters ", " (comma and space). Each
      * key-value mapping is rendered as the key
      * followed by an equals sign ("=") followed
      * by the associated value. Keys and values
      * are converted to strings as by <code>String.valueOf(Object)</code>.
      * </p>
      * <p>
      * This implementation creates an empty string buffer,
      * appends a left brace, and iterates over the map's <code>entrySet</code>
      * view, appending the string representation of each <code>map.entry</code>
      * in turn. After appending each entry except the last,
      * the string ", " is appended. Finally a right brace is
      * appended. A string is obtained from the stringbuffer,
      * and returned.
      * </p>
      *
      * @return a String representation of this map
      */
    public String toString() {
        return products.toString();
    }


    /**
     * Enhances all products in the map with catalog data.
     * <p>
     * If no catalog data could be found for a product, it is removed from
     * the map. The method returns an array of techkeys of all products that
     * have been removed.
     * </p>
     *
     * @param catalog Reference to the catalog which will enhance the product
     *                data.
     * @return Array of techkeys of all products that have been removed.
     */
    public TechKey[] enhance(WebCatInfo catalog) {

        // create array of techKeys
        TechKey[] techKeys = new TechKey[products.size()];
        Iterator it = this.iterator();
        int i=0;
        while (it.hasNext()) {
            techKeys[0] = ((Product) it.next()).getTechKey();
            i++;
        }

        // enhance products
        return enhance(catalog, techKeys);
    }


    /**
     * Enhances an arbitrary number of products with catalog data.
     * The products' technical keys are provided in an array.
     * <p>
     * If no catalog data could be found for a product, it is removed from
     * the map. The method returns an array of techkeys of all products that
     * have been removed.
     * </p>
     *
     * @param catalog Reference to the catalog which will enhance the product
     *                data.
     * @param techKeys TechKeys of the products to be enhanced.
     * @return Array of techkeys of all products that have been removed.
     *
     */
    public TechKey[] enhance(WebCatInfo catalog, TechKey[] techKeys) {
		final String METHOD = "enhance()";
		loc.entering(METHOD);
        Product product = null;
        WebCatItem catalogItem = null;
        ArrayList removedProducts = new ArrayList();

        if (catalog != null && techKeys != null && techKeys.length > 0) {
            // create array of techKeys in String representation
            String[] techKeyStrings = new String[techKeys.length];
            for (int i=0; i<techKeys.length; i++) {
                techKeyStrings[i] = techKeys[i].getIdAsString();
            }

            // get the webCatItems from the catalog
            Map catalogItemMap = catalog.getProductMap(techKeyStrings);

            // find catalog items for each entry
            for (int i=0; i<techKeys.length; i++) {
                TechKey techKey = techKeys[i];
                product = (Product) products.get(techKey);

                if (product == null) {
                    // ignore if not found
                    continue;
                }

                // Hash table contains a list of WebCatItems (technical key
                // is not unique because a product could occur in different
                // areas)
                List catalogItemList = (List) catalogItemMap.get(techKey.getIdAsString());

                if (catalogItemList != null) {
                    // we use only the first entry in the list
                    catalogItem = (WebCatItem) catalogItemList.get(0);
                }
                else {
                    catalogItem = null;
                }

                if (catalogItem != null) {
                    // set the product's catalog item
                    product.setCatalogItem(catalogItem);
                 }
                else {
                    // remove product from map
                    products.remove(techKey);
                    removedProducts.add(techKey);
                }
            }
        }
		loc.exiting();
        return (TechKey[]) removedProducts.toArray(new TechKey[0]);
    }

}