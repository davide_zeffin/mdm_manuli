/*****************************************************************************
    Class:        BORuntimeException
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      28.3.2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/06/26 $
*****************************************************************************/

package com.sap.isa.businessobject;

/**
 * Exception for Errors caused by the developer of the isales application.
 * <br>
 * Don't catch this Exception except in the action layer because it
 * represents an error in the implementation and not the underlying
 * backend system.
 *
 * @author Wolfgang Sattler
 * @version 1.0
 */
public class BORuntimeException extends RuntimeException {

    /**
     * Creates a new exception without any message
     */
    public BORuntimeException() {
        super();
    }

    /**
     * Creates a new exception with a given message
     *
     * @param msg Message to give further information about the exception
     */
    public BORuntimeException(String msg) {
        super(msg);
    }
}