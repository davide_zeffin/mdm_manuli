/*****************************************************************************
    Class:        PricingInfo
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler, Stefan Dendl
    Created:      November 2001
    Version:      1.0

    $Revision: #7 $
    $Date: 2001/08/01 $
*****************************************************************************/

package com.sap.isa.businessobject;

import java.util.Map;

import com.sap.isa.backend.boi.isacore.PricingInfoData;

/**
 * The <code>PricingInfo</code> handles the pricing infos, which are e.g. necessary
 * to handle the Basket within the IPC.<p>
 *
 * To read the pricing info from the backend you can use the <code>shopData</code>
 * object.<br>
 * <i>For further details see the interface <code>PricingInfoData</code></i>
 *
 * @see com.sap.isa.backend.boi.isacore.PricingInfoData
 * @see com.sap.isa.backend.boi.isacore.ShopData
 *
 * @author Stefan Dendl, Wolfgang Sattler
 * @version 1.0
 */
public class PricingInfo implements PricingInfoData {

    private String procedureName;
    private String fGProcedureName;
    private String documentCurrencyUnit;
    private String localCurrencyUnit;
    private String language;
    private String country;
    private String salesOrganisation;
    private String salesOrganisationCrm;
    private String distributionChannel;
    private String distributionChannelOriginal;
    private Map    headerAttributes;
    private Map    itemAttributes;

    /**
     * Returns the property procedureName
     *
     * @return procedureName
     *
     */
    public String getProcedureName() {
        return procedureName;
    }

    /**
     * sets the property procedureName
     *
     * @param procedureName
     *
     */
    public void setProcedureName(String procedureName) {
        this.procedureName = procedureName;
    }

    /**
     * Returns the property documentCurrencyUnit
     *
     * @return documentCurrencyUnit
     *
     */
    public String getDocumentCurrencyUnit() {
        return documentCurrencyUnit;
    }

    /**
     *  sets the property documentCurrencyUnit
     *
     * @param documentCurrencyUnit
     *
     */
    public void setDocumentCurrencyUnit(String documentCurrencyUnit) {
        this.documentCurrencyUnit = documentCurrencyUnit;
    }

    /**
     * Returns the property localCurrencyUnit
     *
     * @return localCurrencyUnit
     *
     */
    public String getLocalCurrencyUnit() {
        return localCurrencyUnit;
    }

    /**
     * sets the property localCurrencyUnit
     *
     * @param localCurrencyUnit
     *
     */
    public void setLocalCurrencyUnit(String localCurrencyUnit) {
        this.localCurrencyUnit = localCurrencyUnit;
    }


    /**
     * Returns the property salesOrganisation
     *
     * @return salesOrganisation
     *
     */
    public String getSalesOrganisation() {
        return salesOrganisation;
    }

    /* sets the property salesOrganisation
     *
     * @param salesOrganisation
     *
     */
    public void setSalesOrganisation(String salesOrganisation) {
        this.salesOrganisation = salesOrganisation;
    }

    /**
     * Returns the property salesOrganisationCrm
     *
     * @return salesOrganisationCrm
     *
     */
    public String getSalesOrganisationCrm() {
        return salesOrganisationCrm;
    }

    /**
     * sets the property salesOrganisationCrm
     *
     * @param salesOrganisationCrm
     *
     */
    public void setSalesOrganisationCrm(String salesOrganisationCrm) {
        this.salesOrganisationCrm = salesOrganisationCrm;
    }

    /**
     * Returns the property distributionChannel
     *
     * @return distributionChannel
     */
    public String getDistributionChannel() {
        return distributionChannel;
    }

    /* sets the property distributionChannel
     *
     * @param distributionChannel
     *
     */
    public void setDistributionChannel(String distributionChannel) {
        this.distributionChannel = distributionChannel;
    }

    /**
     * Returns the property distributionChannelOriginal
     *
     * @return distributionChannelOriginal
     */
    public String getDistributionChannelOriginal() {
        return distributionChannelOriginal;
    }

    /**
     * sets the property distributionChannelOriginal
     *
     * @return distributionChannelOriginal
     */
    public void setDistributionChannelOriginal(String distributionChannelOriginal) {
        this.distributionChannelOriginal = distributionChannelOriginal;
    }

    /**
     * Returns the property headerAttributes
     *
     * @return headerAttributes
     */
    public Map getHeaderAttributes() {
        return headerAttributes;
    }

    /**
     * Sets the property headerAttributes
     *
     * @param headerAttributes
     */
    public void setHeaderAttributes(Map headerAttributes) {
        this.headerAttributes = headerAttributes;
    }

    /**
     * Sets the property itemAttributes
     *
     * @param itemAttributes
     */
    public void setItemAttributes(Map itemAttributes) {
        this.itemAttributes = itemAttributes;
    }

    /**
     * Returns the property itemAttributes
     * <p>
     * To extract the item attributes use coding like this<br>
     * <p>
     *     Map attributes = (Map)pricingInfo.getItemAttributes().get(itemGuid);
     *     String soldFromGuid = (String)attributes.get("SOLD_FROM");
     * </p>
     * </p>
     *
     * @return itemAttributes
     */
    public Map getItemAttributes() {
        return itemAttributes;
    }
    
	/**
	 * Sets the free good procedure.
	 * 
	 * @param fGProcedureName key of the free good procedure
	 */
	public void setFGProcedureName(String fGProcedureName){
		this.fGProcedureName = fGProcedureName;    
	}
	
	/**
	 * Returns the free good procedure.
	 * @return the free good procedure
	 */
	public String getFGProcedureName(){
		return fGProcedureName;	
	}

    /**
     * Returns a copy of this object.
     *
     * @return Copy of this object
     */
    public Object clone() {

        PricingInfo pricingInfo = new PricingInfo();

        pricingInfo.procedureName               = procedureName;
        pricingInfo.documentCurrencyUnit        = documentCurrencyUnit;
        pricingInfo.documentCurrencyUnit        = documentCurrencyUnit;
        pricingInfo.localCurrencyUnit           = localCurrencyUnit;
        pricingInfo.language                    = language;
        pricingInfo.country                     = country;
        pricingInfo.salesOrganisation           = salesOrganisation;
        pricingInfo.salesOrganisationCrm        = salesOrganisationCrm;
        pricingInfo.distributionChannel         = distributionChannel;
        pricingInfo.distributionChannelOriginal = distributionChannelOriginal;
        pricingInfo.headerAttributes            = headerAttributes;
        pricingInfo.itemAttributes              = itemAttributes;
        pricingInfo.fGProcedureName             = fGProcedureName;

        return pricingInfo;
    }
}
