/*****************************************************************************
    Class:        GenericSearchRequestedFieldLine
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.

    $Revision: #1 $
    $DateTime: 2004/01/27 08:20:30 $ (Last changed)
    $Change: 167408 $ (changelist)
    
*****************************************************************************/
package com.sap.isa.businessobject;

import com.sap.isa.backend.boi.appbase.GenericSearchRequestedFieldLineData;


/**
 * One Single select option line 
 * <p>
 * Part of the generic search framework.
 * </p>
 *
 * @author SAP AG
 * @version 1.0
 *
 */

public class GenericSearchRequestedFieldLine 
      implements GenericSearchRequestedFieldLineData {
	
	protected int    handle = 0;
	protected int    field_index = 0;
	protected String fieldname = "";
	protected String attribute = "";
	
    /**
     * Constructor with all attributes
     */
    public GenericSearchRequestedFieldLine(
               int    handle,
               int    field_index,
               String fieldname,
               String attribute) {
        this.handle       = handle;
        this.field_index  = field_index;
        this.fieldname    = fieldname;
        this.attribute    = attribute;
    }


	/**
	 * Returns the Attribute
	 * @return String Attribute
	 */
	public String getAttribute() {
		return attribute;
	}

	/**
	 * Returns the Field index. 
	 * @return int Field index
	 */
	public int getFieldIndex() {
		return field_index;
	}

	/**
	 * Returns the name of the Field
	 * @return String Fieldname
	 */
	public String getFieldname() {
		return fieldname;
	}

	/**
	 * Returns the Handle
	 * @return int Handle
	 */
	public int getHandle() {
		return handle;
	}

	/**
	 * Sets the Attribute
	 * @param Attribute
	 */
	public void setAttribute(String string) {
		attribute = string;
	}

	/**
	 * Sets the Field index 
	 * @param Field index
	 */
	public void setFieldindex(int i) {
		field_index = i;
	}

	/**
	 * Sets the Name of the Field
	 * @param Fieldname
	 */
	public void setFieldname(String string) {
		fieldname = string;
	}

	/**
	 * Set the Handle
	 * @param Handle
	 */
	public void setHandle(int i) {
		handle = i;
	}

    /**
     * To string method of this object
     * @return String representing this object
     */
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("\nHandle: " + handle + ", ");
        sb.append("Field Index: " + field_index + ", ");
        sb.append("Field Name: " + fieldname + ", ");
        sb.append("Attribute: " + attribute);

        return sb.toString();
    }

}
