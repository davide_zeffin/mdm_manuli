/*****************************************************************************
    Class:        GenericSearchSelectOptions
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author        SAP
*****************************************************************************/
package com.sap.isa.businessobject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.sap.isa.backend.boi.appbase.GenericSearchSelectOptionsData;
import com.sap.isa.core.logging.IsaLocation;


/**
 * Collection of all select options 
 * <p>
 * Part of the generic search framework.
 * </p>
 *
 * @author SAP AG
 * @version 1.0
 *
 */

public class GenericSearchSelectOptions 
        implements Cloneable, 
                   GenericSearchSelectOptionsData {

    protected List selOptLines = new ArrayList(20);
	protected List reqFldLines = new ArrayList(10);
    protected List ctrOptLines = new ArrayList(10);
    
	static final private IsaLocation loc = IsaLocation.getInstance(GenericSearchSelectOptions.class.getName());

	/**
     * Adds a Select Option Line to the list of all Select Options
     * @param line with a Select option
     */
    public void addSelectOptionLine(GenericSearchSelectOptionLine line) {
        selOptLines.add(line);
    }

    /**
     * Sets the Control Option Line. If a line with the same <code>Handle</code> exists, it 
     * will be replace with the new one. 
     * @param line with the control options
     */
    public void setControlOptionLine(GenericSearchControlOptionLine line) {
		final String METHOD = "setControlOptionLine()";
		loc.entering(METHOD);
		if (loc.isDebugEnabled())
			loc.debug("searchControlOptionLine="+line);
        for (int i = 0; i < ctrOptLines.size(); i++) {
            GenericSearchControlOptionLine col = (GenericSearchControlOptionLine)ctrOptLines.get(i);
            if (line != null  &&  col != null) {
                if (line.getHandle() == col.getHandle()) {
                    ctrOptLines.remove(i);
                }
            }
        }
        ctrOptLines.add(line);
        loc.exiting();
    }

    /**
     * Returns the control option line for a given handle. Returns null if not found!
     * @param int Handle for which the control option line should be returned
     * @return GenericSearchControlOptionLine control option line
     */
    public GenericSearchControlOptionLine getControlOptionLine(int handle) {
		final String METHOD = "getControlOptionLine()";
		loc.entering(METHOD);
		if (loc.isDebugEnabled())
			loc.debug("handle="+handle);
        for (int i = 0; i < ctrOptLines.size(); i++) {
            GenericSearchControlOptionLine col = (GenericSearchControlOptionLine)ctrOptLines.get(i);
            if (col != null) {
                if (handle == col.getHandle()) {
                	loc.exiting();
                    return col;
                }
            }
        }
        loc.exiting();
        return null;
    }
	/**
	 * Adds a Requested Field Line to the list of all Requested Fields
	 * @param line with a Requested Field
	 */
	public void addRequestedFieldLine(GenericSearchRequestedFieldLine line) {
		reqFldLines.add(line);
	}

    /**
     * Returns the iterator for the list of the select options 
     */
    public Iterator getSelectOptionLineIterator() {
    	return selOptLines.iterator();
    }

	/**
	 * Returns the iterator for the list of the Requested Fields 
	 */
	public Iterator getRequestedFieldLineIterator() {
		return reqFldLines.iterator();
	}
    /**
     * Returns the iterator for the list of the control options 
     */
    public Iterator getControlOptionLineIterator() {
        return ctrOptLines.iterator();
    }
    /**
     * To string method of this object
     * @return String representing this object
     */
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("Control options" + ctrOptLines);
        sb.append("\nSelect options" + selOptLines);
        sb.append("\nRequested Fields" + reqFldLines);

        return sb.toString();
    }
}
