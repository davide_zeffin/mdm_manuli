/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      15 February 2001
*****************************************************************************/

package com.sap.isa.businessobject;

import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.MessageList;
import com.sap.isa.core.util.MessageListHolder;

/**
 * Thrown if something goes wrong in the backend layer
 */
public class BusinessObjectException extends Exception
    implements MessageListHolder {

    protected MessageList msgList = new MessageList();

    /**
     * Constructor
     *
     * @param msg Message for the Excepetion
     * @param msgList List of the messages added to the exception
     */
    public BusinessObjectException(String msg, MessageList msgList) {
        super(msg);
        this.msgList = msgList;
    }


    /**
     * Constructor
     *
     * @param msg Message for the Excepetion
     * @param message message added to the exception
     */
    public BusinessObjectException(String msg, Message message) {
        super(msg);
        this.msgList = new MessageList();
        msgList.add(message);
    }


    /**
     * Constructor
     *
     * @param msg Message for the Excepetion
     */
    public BusinessObjectException(String msg) {
        super(msg);
    }

    /**
     * Constructor
     */
    public BusinessObjectException() {
        super();
    }

    /**
     * Adds a new message to the Excpetion
     *
     * @param msg Message to be added
     */
    public void addMessage(Message message) {
        if (msgList == null) {
            msgList = new MessageList();
        }
        msgList.add(message);
    }

    /**
     * Returns a list of the message appended to the exception.
     *
     * @return List of messages
     */
    public MessageList getMessageList() {
        return msgList;
    }


    /**
     *
     * clear all messages
     *
     */
    public void clearMessages( ) {
        msgList.clear();
    }

}