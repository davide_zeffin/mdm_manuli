/*****************************************************************************
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Stefan Hunsicker
  Created:      18 April 2001

  $Revision: #2 $
  $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.businessobject.contract;

import java.util.ArrayList;
import java.util.Iterator;

import com.sap.isa.backend.boi.isacore.contract.ContractReferenceData;
import com.sap.isa.backend.boi.isacore.contract.ContractReferenceListData;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.core.Iterable;

/**
 * Represents a list of <code>ContractReference</code>s of a contract.
 */
public class ContractReferenceList
    extends BusinessObjectBase
    implements ContractReferenceListData, Iterable {
    protected ArrayList contractReferences;


    /**
     * Creates a new <code>ContractReferenceList</code> object.
     */
    public ContractReferenceList () {
        contractReferences = new ArrayList();
    }

    /**
     * Adds a new <code>ContractReference</code> to the list.
     */
    public void add (ContractReference contractReference) {
        contractReferences.add(contractReference);
    }

    /**
     * Adds a new <code>ContractReferenceData</code> object to the list.
     */
    public void add (ContractReferenceData contractReference) {
        if (contractReference instanceof ContractReference) {
            add((ContractReference) contractReference);
        }
    }

    /**
     * Returns the <code>ContractReference</code> at the specified position in
     * this list.
     */
    public ContractReference get (int index) {
        return (ContractReference) contractReferences.get(index);
    }

    /**
     * Returns the number of <code>ContractReference</code>s in this list.
     */
    public int size () {
        return contractReferences.size();
    }

    /**
     * Returns true if this list contains no <code>ContractReference</code>.
     */
    public boolean isEmpty () {
        return contractReferences.isEmpty();
    }

    /**
     * Returns an iterator over the <code>ContractReference</code>s contained
     * in the list
     */
    public Iterator iterator () {
        return contractReferences.iterator();
    }
}
