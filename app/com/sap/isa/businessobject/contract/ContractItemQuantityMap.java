/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      20 March 2001

    $Revision: #1 $
    $Date: 2001/06/26 $
*****************************************************************************/

package com.sap.isa.businessobject.contract;

import java.util.HashMap;
import java.util.Iterator;

/**
 */
public class ContractItemQuantityMap {
    protected HashMap contractItemQuantities;

    /**
     * Inner class to store the requested quantity information.
     */
    protected class RequestedQuantity {
        protected String quantity;
        protected String unit;
        public RequestedQuantity() {
            quantity = "1";
        }
        public RequestedQuantity(String quantity, String unit) {
            this.quantity = quantity;
            this.unit = unit;
        }
        public String getQuantity() {
            return quantity;
        }
        public String getUnit() {
            return unit;
        }
    }

    /**
     * Creates a new <code>ContractItemQuantityMap</code> object.
     */
    public ContractItemQuantityMap() {
        contractItemQuantities = new HashMap();
    }

    /**
     * Adds a new quantity to the Map.
     */
    public void put(
            ContractItemTechKey itemKey,
            String quantity,
            String unit) {
        contractItemQuantities.put(
            itemKey, new RequestedQuantity(quantity, unit));
    }

    /**
     * Returns the quantity for the specified item key. "1" is the default
     * answer if no entry is found in the map.
     */
    public String getQuantity(ContractItemTechKey itemKey) {
        if (containsKey(itemKey)) {
            return (String)((RequestedQuantity)contractItemQuantities.
                get(itemKey)).getQuantity();
        }
        else {
            return "1";
        }
    }

    /**
     * Returns the unit for the specified item key. "" is the default
     * answer if no entry is found in the map.
     */
    public String getUnit(ContractItemTechKey itemKey) {
        if (containsKey(itemKey)) {
            return (String)((RequestedQuantity)contractItemQuantities.
                get(itemKey)).getUnit();
        }
        else {
            return "";
        }
    }

    /**
     * Removes the quantity for the specified item key from the map.
     */
    public void remove(ContractItemTechKey itemKey) {
        contractItemQuantities.remove(itemKey);
    }

    /**
     * Removes all quantities from this map.
     */
    public void clear() {
        contractItemQuantities.clear();
    }

    /**
     * Returns the number of quantities in this map.
     */
    public int size() {
        return contractItemQuantities.size();
    }

    /**
     * Returns true if this map contains no quantities.
     */
    public boolean isEmpty() {
        return contractItemQuantities.isEmpty();
    }

    /**
     * Checks if the map contains a given key.
     */
    public boolean containsKey(ContractItemTechKey itemKey) {
        return contractItemQuantities.containsKey(itemKey);
    }

    /**
     * Returns an iterator view of the keys contained in this map.
     */
    public Iterator getItemKeyIterator() {
        return contractItemQuantities.keySet().iterator();
    }
}