/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      20 March 2001

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.businessobject.contract;

import com.sap.isa.backend.boi.isacore.contract.ContractItemConfigurationReferenceData;
import com.sap.isa.backend.boi.isacore.contract.ContractItemData;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.core.TechKey;

/**
 * Representation of a contract item.
 */
public class ContractItem extends BusinessObjectBase
                          implements ContractItemData {
    protected ContractItemTechKey contractItemKey;
    private String id = "";
    private String configFilter = "";
    private boolean multiProduct = false;
    private boolean allProduct = false;
    private String productId = "";
    private String productDescription = "";
    private String quantity = "";
    private String unit = "";
    private ContractItemPrice itemPrice;
    protected ContractItemConfigurationReference configurationReference;
    private String configurationType;
    private String price = "";
    private String priceUnit = "";
    private String priceQuantUnit = "";
    private String currency = "";
    private Object externalItem;
    private boolean ipcConfigurable = false;
    private boolean isVariant = false;
    private boolean expanded = false;
    private boolean collapsed = false;
    private boolean newMainItem = false;
    private boolean requested = false;
	private String status = "";    
	
	private static final String STATUS_OPEN         = "open"; 
	private static final String STATUS_IN_PROCESS   = "inProcess";
	private static final String STATUS_COMPLETED    = "completed"; 	

    /**
     * Default constructor.
     */
    public ContractItem() {}

    /**
     * Retrieves the key, concatenated from product and contract item key.
     */
    public ContractItemTechKey getContractItemTechKey() {
        return contractItemKey;
    }

    /**
     * Retrieves a String representation of the key, concatenated from product
     * and contract item key.
     */
    public String getKeyAsString() {
        return contractItemKey.getKeyAsString();
    }

    /**
     * Sets the key, concatenated from product and contract item key.
     */
    public void setContractItemTechKey(TechKey itemKey, TechKey productKey) {
        this.contractItemKey = new ContractItemTechKey(itemKey,productKey);
    }

    /**
     * Retrieves the contract item part of the key only. This is needed, e.g.,
     * for extensions.
     * <p>
     * <em>This key does not uniquely identify a ContractItem object in a
     * ContractItemList, use the getContractItemTechKey() method for this
     * purpose instead!</em>
     */
    public TechKey getTechKey() {
        return contractItemKey.getItemKey();
    }

    /**
     * Retrieves the contract id of the item.
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the contract id of the item.
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Retrieves the contract configuration filter of the item.
     */
    public String getConfigFilter() {
        return configFilter;
    }

    /**
     * Sets the contract configuration filter of the item.
     */
    public void setConfigFilter(String configFilter) {
        this.configFilter = configFilter;
    }

    /**
     * Indicates if the item has more than one product.
     */
    public boolean isMultiProduct() {
        return multiProduct;
    }

    /**
     * Specifies if the item has more than one product.
     */
    public void setMultiProduct(boolean multiProduct) {
        this.multiProduct = multiProduct;
    }

    /**
     * Indicates if the item allows all products in the system.
     */
    public boolean isAllProduct() {
        return allProduct;
    }

    /**
     * Specifies if the item allows all products in the system.
     */
    public void setAllProduct(boolean allProduct) {
        this.allProduct = allProduct;
    }

    /**
     * Retrieves the product id of the item.
     */
    public String getProductId() {
        return productId;
    }

    /**
     * Set the product id of the item.
     */
    public void setProductId(String productId) {
        this.productId = productId;
    }

    /**
     * Retrieves the product description of the item.
     */
    public String getProductDescription() {
        return productDescription;
    }

    /**
     * Sets the product description of the item.
     */
    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    /**
     * Retrieves the requested quantity of the product of the item.
     */
    public String getQuantity() {
        return quantity;
    }

    /**
     * Sets the requested quantity of the product of the item.
     */
    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    /**
     * Retrieves the unit of the requested quantity of the product of the item.
     */
    public String getUnit() {
        return unit;
    }

    /**
     * Sets the unit of the requested quantity of the product of the item.
     */
    public void setUnit(String unit) {
        this.unit = unit;
    }

    /**
     * Retrieves the ContractItemPrice of the item.
     */
    public ContractItemPrice getItemPrice() {
        return itemPrice;
    }

    /**
     * Sets the ContractItemPrice of the item.
     */
    public void setItemPrice(ContractItemPrice itemPrice) {
        this.itemPrice = itemPrice;
    }

    /**
     * Retrieves the ContractItemConfigurationReference of the item.
     */
    public ContractItemConfigurationReference getConfigurationReference() {
        return configurationReference;
    }

    /**
     * Sets the ContractItemConfigurationReference of the item.
     */
    public void setConfigurationReference(
                ContractItemConfigurationReferenceData configurationReference) {
        if (configurationReference instanceof
                ContractItemConfigurationReference) {
            this.configurationReference =
                (ContractItemConfigurationReference)configurationReference;
        }
    }

    /**
     * Retrieves the ConfigurationType of the item.
     */
    public String getConfigurationType() {
        return configurationType;
    }

    /**
     * Sets the ConfigurationType of the item.
     */
    public void setConfigurationType(String configurationType) {
        this.configurationType = configurationType;
    }

    /**
     * Indicates if the product of the item is configurable using IPC.
     */
    public boolean isIpcConfigurable() {
        return ipcConfigurable;
    }

    /**
     * Specifies if the product of the item is configurable using IPC.
     */
    public void setIpcConfigurable(boolean ipcConfigurable) {
        this.ipcConfigurable = ipcConfigurable;
    }

    /**
     * Indicates if the product of the item is a variant.
     */
    public boolean isVariant() {
        return isVariant;
    }

    /**
     * Specifies if the product of the item is a variant.
     */
    public void setVariant(boolean isVariant) {
        this.isVariant = isVariant;
    }

    /**
     * Indicates if the item is expanded.
     */
    public boolean isExpanded() {
        return expanded;
    }

    /**
     * Specifies if the item is expanded.
     */
    public void setExpanded(boolean expanded) {
        this.expanded = expanded;
    }

    /**
     * Indicates if the item is collapsed.
     */
    public boolean isCollapsed() {
        return collapsed;
    }

    /**
     * Specifies if the item is collapsed.
     */
    public void setCollapsed(boolean collapsed) {
        this.collapsed = collapsed;
    }

    /**
     * Specifies if the item represents the first occurence of a new main item
     * in the list.
     */
    public void setNewMainItem(boolean newMainItem) {
        this.newMainItem = newMainItem;
    }

    /**
     * Indicates the first occurence of a new main item in the list.
     */
    public boolean isNewMainItem() {
        return newMainItem;
    }

    /**
     * Indicates if the item is requested.
     */
    public boolean isRequested() {
        return requested;
    }

    /**
     * Specifies if the item is requested.
     */
    public void setRequested(boolean requested) {
        this.requested = requested;
    }
	/**
	 * Specifies the currency of the price
	 * @return
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * Specifies the IPCItem
	 * @return
	 */
	public Object getExternalItem() {
		return externalItem;
	}

	/**
	 * Specificies the price of item
	 * @return
	 */
	public String getPrice() {
		return price;
	}

	/**
	 * Specifies the quantity unit of the price
	 * @return
	 */
	public String getPriceQuantUnit() {
		return priceQuantUnit;
	}

	/**
	 * Specifies the price unit of the price
	 * @return
	 */
	public String getPriceUnit() {
		return priceUnit;
	}

	/**
	 * Sets the currency of the price
	 * @param string
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * Sets the external item (e.g. IPC Item) 
	 * @param object
	 */
	public void setExternalItem(Object externalItem) {
		this.externalItem = externalItem;
	}

	/**
	 * sets the price of the item
	 * @param string
	 */
	public void setPrice(String price) {
		this.price = price;
	}

	/**
	 * sets the quantity unit of the price
	 * @param string
	 */
	public void setPriceQuantUnit(String priceQuantUnit) {
		this.priceQuantUnit = priceQuantUnit;
	}

	/**
	 * Sets the price unit of the price
	 * @param string
	 */
	public void setPriceUnit(String priceUnit) {
		this.priceUnit = priceUnit;
	}
	/**
	 * Sets the status IN_PROCESS
	 */
	public void setStatusInProcess() {
		status = STATUS_IN_PROCESS;
	}
	/**
	 * Sets the status OPEN
	 */
	public void setStatusOpen() {
		status = STATUS_OPEN;
	}	
	/**
	* Sets the status COMPLETED
	*/
   public void setStatusCompleted() {
	   status = STATUS_COMPLETED;
   }
   /**	
   * Returns true, of the status is OPEN
   */
  public boolean isStatusInOpen() {
	  return (status == STATUS_OPEN);
  }   
   /**	
   * Returns true, of the status is IN_PROCESS
   */
  public boolean isStatusInProcess() {
	  return (status == STATUS_IN_PROCESS);
  }   
  /**	
  * Returns true, of the status is COMPLETED
  */
 public boolean isStatusCompleted() {
	 return (status == STATUS_COMPLETED);
 } 
}