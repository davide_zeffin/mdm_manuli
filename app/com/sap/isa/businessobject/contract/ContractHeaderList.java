/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      20 March 2001

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.businessobject.contract;

import java.util.ArrayList;
import java.util.Iterator;

import com.sap.isa.backend.boi.isacore.contract.ContractAttributeListData;
import com.sap.isa.backend.boi.isacore.contract.ContractHeaderData;
import com.sap.isa.backend.boi.isacore.contract.ContractHeaderListData;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.core.Iterable;

/**
 * Represents a list of <code>ContractHeader</code>s of contracts.
 */
public class ContractHeaderList extends BusinessObjectBase
                                implements ContractHeaderListData, Iterable {
    protected ArrayList contractHeaders;
    protected ContractAttributeList attributes;

    /**
     * Creates a new <code>ContractHeaderList</code> object.
     */
    public ContractHeaderList() {
        contractHeaders = new ArrayList();
    }

    /**
     * Adds a new <code>ContractHeader</code> to the list.
     */
    public void add(ContractHeader contractHeader) {
        contractHeaders.add(contractHeader);
    }

    /**
     * Adds a new <code>ContractHeaderData</code> to the list.
     */
    public void add(ContractHeaderData contractHeader) {
        if (contractHeader instanceof ContractHeader) {
            contractHeaders.add((ContractHeader)contractHeader);
        }
    }

    /**
     * Returns the element at the specified position in this list.
     *
     * @param  index index of element to return.
     * @return the header at the specified position in this list.
     * @throws    IndexOutOfBoundsException if index is out of range <tt>(index
     *            &lt; 0 || index &gt;= size())</tt>.
     */
    public ContractHeader get(int index) {
        return (ContractHeader) contractHeaders.get(index);
    }

    /**
     * Retrieves the attribute list of the header list.
     */
    public ContractAttributeList getAttributes() {
        return attributes;
    }

    /**
     * Adds an attribute list to the header list
     */
    public void setItemAttributes(ContractAttributeList attributes) {
        this.attributes = attributes;
    }

    /**
     * Adds an attribute list to the header list
     */
    public void setItemAttributes(ContractAttributeListData attributes) {
        if (attributes instanceof ContractAttributeList) {
            this.attributes = (ContractAttributeList)attributes;
        }
    }

    /**
     * Returns the number of elements in this list.
     */
    public int size() {
        return contractHeaders.size();
    }

    /**
     * Returns true if this list contains no data.
     */
    public boolean isEmpty() {
        return contractHeaders.isEmpty();
    }

    /**
     * Returns an iterator over the elements contained in the list
     */
    public Iterator iterator() {
        return contractHeaders.iterator();
    }
}
