/*****************************************************************************
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      18 April 2000

    $Revision: #2 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.businessobject.contract;

import com.sap.isa.backend.boi.isacore.contract.ContractAttributeValueListData;
import com.sap.isa.backend.boi.isacore.contract.ContractItemConfigurationReferenceData;
import com.sap.isa.backend.boi.isacore.contract.ContractReferenceData;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.core.TechKey;

/**
 * Represents a reference to a contract item (e.g., of a product in a product
 * catalog).
 */
public class ContractReference extends BusinessObjectBase
                               implements ContractReferenceData{
    private TechKey ContractKey;
    private String contractId;
    private TechKey ItemKey;
    private String ItemID;
    private String ConfigFilter;
    protected ContractAttributeValueList attributeValues;
    protected ContractItemConfigurationReference configurationReference;

    /**
     * Retrieves the technical key of the referenced contract.
     */
    public TechKey getContractKey() {
        return ContractKey;
    }

    /**
     * Sets the technical key of the referenced contract.
     */
    public void setContractKey(TechKey ContractKey) {
        this.ContractKey = ContractKey;
    }

    /**
     * Retrieves the id of the referenced contract.
     */
    public String getContractId() {
        return contractId;
    }

    /**
     * Sets the id of the referenced contract.
     */
    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    /**
     * Retrieves the technical key of the referenced contract item.
     */
    public TechKey getItemKey() {
        return ItemKey;
    }

    /**
     * Sets the technical key of the referenced contract item.
     */
    public void setItemKey(TechKey ItemKey) {
        this.ItemKey = ItemKey;
    }

    /**
     * Retrieves the id of the referenced contract item.
     */
    public String getItemID() {
        return ItemID;
    }

    /**
     * Sets the id of the referenced contract item.
     */
    public void setItemID(String ItemID) {
        this.ItemID = ItemID;
    }

    /**
     * Retrieves the contract configuration filter of the referenced contract
     * item.
     */
    public String getConfigFilter() {
        return ConfigFilter;
    }

    /**
     * Sets the contract configuration filter of the item.
     */
    public void setConfigFilter(String ConfigFilter) {
        this.ConfigFilter = ConfigFilter;
    }

    /**
     * Retrieves the map of attribute values of the item list.
     */
    public ContractAttributeValueList getAttributeValues() {
        return attributeValues;
    }

    /**
     * Adds an attribute value map to the item list
     */
    public void setAttributeValues(ContractAttributeValueList attributeValues) {
        this.attributeValues = attributeValues;
    }

    /**
     * Adds an attribute value map to the item list
     */
    public void setAttributeValues(ContractAttributeValueListData
            attributeValues) {
        if (attributeValues instanceof ContractAttributeValueList) {
            this.attributeValues = (ContractAttributeValueList)attributeValues;
        }
    }

    /**
     * Retrieves the ContractItemConfigurationReference of the reference.
     */
    public ContractItemConfigurationReference getConfigurationReference() {
        return configurationReference;
    }

    /**
     * Sets the ContractItemConfigurationReference of the reference.
     */
    public void setConfigurationReference(
                ContractItemConfigurationReferenceData configurationReference) {
        if (configurationReference instanceof
                ContractItemConfigurationReference) {
            this.configurationReference =
                (ContractItemConfigurationReference)configurationReference;
        }
    }
}