/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      25 April 2001

    $Revision: #2 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.businessobject.contract;

import com.sap.isa.backend.boi.isacore.contract.ContractAttributeValueData;
import com.sap.isa.businessobject.BusinessObjectBase;

/**
 * Represents an attribute value of a contract item.
 */
public class ContractAttributeValue extends BusinessObjectBase
                                    implements ContractAttributeValueData {
    private String id;
    private String value;
    private String unitValue;

    /**
     * Retrieves the language independent id of the attribute.
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the language independent id of the attribute.
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Retrieves the value of the attribute.
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the value of the attribute.
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Retrieves the unit of the attribute.
     */
    public String getUnitValue() {
        return unitValue;
    }

    /**
     * Sets the unit of the attribute.
     */
    public void setUnitValue(String unitValue) {
        this.unitValue = unitValue;
    }
}
