/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      20 March 2001

    $Revision: #2 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.businessobject.contract;

import com.sap.isa.backend.boi.isacore.contract.ContractHeaderData;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.core.TechKey;

/**
 * Repesentation of a contract header. The header contains information
 * common to all items of a contract.
 */
public class ContractHeader extends BusinessObjectBase
                            implements ContractHeaderData,
                                       DocumentState {
    private TechKey headerKey;
    private String id;
    private String externalRefNo;
    private String validFromDate;
    private String validToDate;
    private String description;
    private String status;
	private String ipcConnectionKey = "";
	private String ipcClient;
	private TechKey ipcDocumentId;	
    
 
    private static final String STATUS_IN_PROCESS   = "inProcess";
    private static final String STATUS_REJECTED     = "rejected";
    private static final String STATUS_QUOTATION    = "quotation";  
    private static final String STATUS_SENT         = "inquirySent";  
    private static final String STATUS_RELEASED     = "released";   
    private static final String STATUS_ACCEPTED     = "accepted"; 
    private static final String STATUS_COMPLETED    = "completed";               
    

    /**
     * Creates a contract header object.
     */
    public ContractHeader() {
    }

    /**
     * Sets the technical key of the contract header.
     */
    public void setTechKey(TechKey headerKey) {
        this.headerKey = headerKey;
    }

    /**
     * Retrieves the technical key of the contract header.
     */
    public TechKey getTechKey() {
        return headerKey;
    }

    /**
     * Gets the state of the contract
     *
     * @return the state as described by the constants of the DocumentState
     * interface
     */
    public int getState() {
        return DocumentState.VIEW_DOCUMENT;
    }

    /**
     * Sets the state of the contract
     *
     * @param  the state as described by the constants of the DocumentState
     * interface
     */
    public void setState(int state) {
    }

    /**
     * Retrieves a String representation of the technical key of the contract
     * header.
     */
    public String getKeyAsString() {
        return headerKey.getIdAsString();
    }

    /**
     * Sets the id of the contract header.
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Retrieves the contract id of the header.
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the external reference number of the header.
     */
    public void setExternalRefNo(String externalRefNo) {
        this.externalRefNo = externalRefNo;
    }

    /**
     * Retrieves the external reference number of the header.
     */
    public String getExternalRefNo() {
        return externalRefNo;
    }

    /**
     * Sets the valid from date of the header.
     */
    public void setValidFromDate(String validFromDate) {
        this.validFromDate = validFromDate;
    }

    /**
     * Retrieves the valid from date of the header.
     */
    public String getValidFromDate() {
        return validFromDate;
    }

    /**
     * Sets the valid to date of the header.
     */
    public void setValidToDate(String validToDate) {
        this.validToDate = validToDate;
    }

    /**
     * Retrieves the valid to date of the header.
     */
    public String getValidToDate() {
        return validToDate;
    }

    /**
     * Sets the language dependend description of the header.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Retrieves the language dependend description of the header.
     */
    public String getDescription() {
        return description;
    }
	/**
	 * Returns the status.
	 * @return String
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 * @param status The status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Sets the status to accepted
	 */
	public void setStatusAccepted() {
		status = STATUS_ACCEPTED;
	}

    /**
     * Sets the status to completed
     */
    public void setStatusCompleted() {
        status = STATUS_COMPLETED;
    }
	/**
	 * Sets the status to IN_PROCESS
	 */
	public void setStatusInProcess() {
		status = STATUS_IN_PROCESS;
	}

	/**
	 * Sets the status to QUOTATION
	 */
	public void setStatusQuotation() {
		status = STATUS_QUOTATION;
	}

	/**
	 * Sets the status to REJECTED.
	 */
	public void setStatusRejected() {
		status = STATUS_REJECTED;
	}

	/**
	 * Sets the status RELEASED.
	 */
	public void setStatusReleased() {
		status = STATUS_RELEASED;
	}

	/**
	 * Sets the status to SENT.
	 */
	public void setStatusSent() {
		status = STATUS_SENT;
	}
    
    /**
     * Sets the status to accepted
     */
    public boolean isStatusAccepted() {
        return (status == STATUS_ACCEPTED);
    }

    /**
     * Sets the status to IN_PROCESS
     */
    public boolean isStatusInProcess() {
        return (status == STATUS_IN_PROCESS);
    }

    /**
     * Sets the status to QUOTATION
     */
    public boolean isStatusQuotation() {
        return (status == STATUS_QUOTATION);
    }

    /**
     * Sets the status to REJECTED.
     */
    public boolean isStatusRejected() {
        return (status == STATUS_REJECTED);
    }

    /**
     * Sets the status RELEASED.
     */
    public boolean isStatusReleased() {
        return (status == STATUS_RELEASED);
    }

    /**
     * Sets the status to SENT.
     */
    public boolean isStatusSent() {
        return (status == STATUS_SENT);
    }
    
    /**
     * Sets the status to COMPLETED.
     */
    public boolean isStatusCompleted() {
        return (status == STATUS_COMPLETED);
    }    

	/**
	 * Retrieves the IPC client.
	 *
	 */
	public String getIpcClient() {
		return ipcClient;
	}

	/**
	 * Retrieves the IPC connection key.
	 */
	public String getIpcConnectionKey() {
		return ipcConnectionKey;
	}

	/**
	 * Retrieves the IPC document ID.
	 */
	public TechKey getIpcDocumentId() {
		return ipcDocumentId;
	}

	/**
	 * @param string
	 */
	public void setIpcClient(String ipcClient) {
		this.ipcClient = ipcClient;
	}

	/**
	 * @param string
	 */
	public void setIpcConnectionKey(String ipcConnectionKey) {
		this.ipcConnectionKey = ipcConnectionKey;
	}

	/**
	 * @param key
	 */
	public void setIpcDocumentId(TechKey ipcDocumentId) {
		this.ipcDocumentId = ipcDocumentId;
	}

}
