/*****************************************************************************
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      20 March 2000

    $Revision: #11 $
    $Date: 2001/08/01 $
*****************************************************************************/
package com.sap.isa.businessobject.contract;


import java.util.HashMap;
import java.util.Iterator;

import com.sap.isa.backend.BackendTypeConstants;
import com.sap.isa.backend.boi.isacore.contract.ContractBackend;
import com.sap.isa.backend.boi.isacore.contract.ContractDataObjectFactoryBackend;
import com.sap.isa.backend.boi.isacore.contract.ContractView;
import com.sap.isa.backend.boi.isacore.contract.TechKeySetData;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.businessobject.BusinessObjectHelper;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.core.util.Message;

/**
 * The Contract class makes (value/quantity) contracts of a user accessible
 * for browsing and ordering including contract call off in the B2B Internet
 * Sales scenario.
 */
public class Contract extends BusinessObjectBase
                      implements BackendAware,
                                 BackendBusinessObjectParams {
    /**
     * Constant for default page size.
     */
    public static final int DEFAULT_PAGE_SIZE = 5;

    /**
     * Factory for contract-related objects in the Backend.
     */
    protected static final ContractDataObjectFactoryBackend
        CONTRACT_DATA_OBJECT_FACTORY = new ContractDataObjectFactory();

    /**
     * Buffer for contract headers already read.
     */
    protected HashMap headerMap;

    /**
     * Reference to the backend object manager.
     */
    private BackendObjectManager bem;

    /**
     * Reference to the contract backend object.
     */
    private ContractBackend contractBackend;

    /**
     * Constructor.
     */
    public Contract() {
        super();
        headerMap = new HashMap();
        contractAttributes = new ContractAttributes();
    }

    /**
     * Exception thrown if contracts are not allowed by shop, but contract data
     * are requested.
     */
    public static class ContractNotAllowedException extends RuntimeException {
        public ContractNotAllowedException() {
            super();
        }
        public ContractNotAllowedException(String msg) {
            super(msg);
        }
    }

    /**
     * Exception thrown if data for a contract are requested that was not read
     * before.
     */
    public static class UnknownKeyException extends RuntimeException {
        public UnknownKeyException() {
            super();
        }
        public UnknownKeyException(String msg) {
            super(msg);
        }
    }

    /*
     * Class storing data of the selected contract in the scenario.
     */
    protected class SelectedContract{
        protected ContractHeader contract;
        int pageNumber;
        int pageSize;
        int lastPageNumber;
        TechKeySet expandedItemSet;
        ContractItemQuantityMap releaseQuantities;
        ContractItemPriceConfigMap releasePriceConfigs;

        // constructa
        public SelectedContract(ContractHeader contract) {
            this.contract = contract;
            pageNumber = 1;
            pageSize = DEFAULT_PAGE_SIZE;
            lastPageNumber = 1;
            releaseQuantities = new ContractItemQuantityMap();
            releasePriceConfigs = new ContractItemPriceConfigMap();
            expandedItemSet = new TechKeySet();
        }

        // returns selected contract (header)
        public ContractHeader getContract() {
            return contract;
        }

        // replace expanded item set from request context
        public void updateExpandedItems(TechKeySet expandedItemSet) {
			final String METHOD = "updateExpandedItems()";
			log.entering(METHOD);
            TechKey itemKey = null;

            // null input means no update
            if (expandedItemSet != null) {
                this.expandedItemSet.clear();
                Iterator itemKeyIterator = expandedItemSet.iterator();
                while (itemKeyIterator.hasNext()) {
                    this.expandedItemSet.add((TechKey)itemKeyIterator.next());
                }
            }
            log.exiting();
        }

        // mixes release quantities (visible on a page) into the corr. map
        public void updateReleaseQuantities(
                ContractItemQuantityMap releaseQuantities) {
                	
			final String METHOD = "updateReleaseQuantities()";
			log.entering(METHOD);
            ContractItemTechKey itemKey;
            String quantity;

            // null input means no update
            if (releaseQuantities != null) {
                Iterator itemKeyIterator = releaseQuantities.getItemKeyIterator();
                while (itemKeyIterator.hasNext()) {
                    itemKey = (ContractItemTechKey)itemKeyIterator.next();
                    quantity = releaseQuantities.getQuantity(itemKey);

                    // insert / overwrite quantity if so requested
                    if (quantity.length() > 0) {
                        this.releaseQuantities.put(
                            itemKey, quantity,
                            releaseQuantities.getUnit(itemKey));
                    }

                    // remove quantiy form map if so requested
                    else if (this.releaseQuantities.containsKey(itemKey)) {
                        this.releaseQuantities.remove(itemKey);
                    }
                }
            }
            log.exiting();
        }

        // adds prices & config data into the corr. map
        public void updateReleasePriceConfigs(
                ContractItemPriceConfigMap releasePriceConfigs) {
            
			final String METHOD = "updateReleasePriceConfigs()";
			log.entering(METHOD);
            ContractItemTechKey itemKey;

            // null input means no update
            if (releasePriceConfigs != null) {
                Iterator itemKeyIterator = releasePriceConfigs.
                        getItemKeyIterator();
                while (itemKeyIterator.hasNext()) {
                    itemKey = (ContractItemTechKey)itemKeyIterator.next();
                    this.releasePriceConfigs.put(
                            itemKey,
                            releasePriceConfigs.getItemPrice(itemKey),
                            releasePriceConfigs.getConfigurationType(itemKey),
                            releasePriceConfigs.isIpcConfigurable(itemKey),
                            releasePriceConfigs.isVariant(itemKey),
                            releasePriceConfigs.getConfigItemReference(itemKey),
                            releasePriceConfigs.getConfigFilter(itemKey));
                }
            }
			log.exiting();
        }
        
    }
    protected SelectedContract selectedContract;

    // map of contract attributes
    protected class ContractAttributes {
        protected HashMap attributeMap;
        ContractAttributes() {
            attributeMap = new HashMap();
        }
        public void put(ContractView view,
                ContractAttributeList attributeList) {
            attributeMap.put(view, attributeList);
        }
        public ContractAttributeList get(ContractView view) {
            return (ContractAttributeList)attributeMap.get(view);
        }
        public boolean containsKey(ContractView view) {
            return attributeMap.containsKey(view);
        }
    }
    protected ContractAttributes contractAttributes;

    /**
     * Sets the BackendObjectManager for the attribute set. This method is used
     * by the object manager to allow the user object interaction with
     * the backend logic. This method is normally not called
     * by classes other than BusinessObjectManager.
     *
     * @param bem BackendObjectManager to be used
     */
    public void setBackendObjectManager(BackendObjectManager bem) {
        this.bem = bem;
    }

    /**
     * Read a list of all contract headers fulfilling the given search criteria
     * from the backend and return the data as a table.
     * @param soldTo The sold to party the contract list is searched for.
     * @param shop The shop the contract list is searched in.
     * @param contractListFilter The filter criteria the contract list is
     * searched for.
     */
    public ContractHeaderList readHeaders(
                TechKey soldToKey,
                Shop shop,
                ContractSearchCriteria contractSearchCriteria
                ) throws CommunicationException {
                	
		final String METHOD = "readHeaders()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("soldToKey="+soldToKey+", shop="+shop);
        ContractHeaderList contractHeaderList = null;

		try {
	        // check if contracts are allowed by shop
	        if (!shop.isContractAllowed()) {
	            throw new ContractNotAllowedException();
	        }
	
	        // read the header list
	        try {
	        //    if (contractSearchCriteria.getStatus().equalsIgnoreCase() 
	            contractHeaderList =
	                (ContractHeaderList)getContractBackend().readHeaders(
	                        soldToKey,
	                        shop.getSalesOrganisation(),
	                        shop.getDistributionChannel(),
	                        shop.getLanguage(),
	                        contractSearchCriteria.getId(),
	                        contractSearchCriteria.getExternalRefNo(),
	                        contractSearchCriteria.getValidFromDate(),
	                        contractSearchCriteria.getValidToDate(),
	                        contractSearchCriteria.getDescription(),
	                        contractSearchCriteria.getStatus() );
	             
	        }
	        catch (BackendException backendException) {
	            BusinessObjectHelper.splitException(backendException);
	            return null; // never reached
	        }
	
	        // store attributes for subsequent reads
	        if (!contractAttributes.containsKey(ContractView.CONTRACT_LIST)) {
	            contractAttributes.put(ContractView.CONTRACT_LIST,
	                contractHeaderList.getAttributes());
	        }
	
	        // message if no headers were found
	        int numOfHeaders = contractHeaderList.size();
	        if (numOfHeaders == 0) {
	            contractHeaderList.addMessage(
	                new Message(Message.INFO,"b2b.contract.message.noHits",null,"hits"));
	
	        // store headers for subsequent reads
	        } else {
	            for (int i = 0; i < numOfHeaders; i++) {
	                ContractHeader header = contractHeaderList.get(i);
	                headerMap.put(header.getTechKey(),header);
	            }
	        }
		} finally {
			log.exiting();
		}
        return contractHeaderList;
    }

	/**
	 * Read a list of all contract headers fulfilling the given search criteria
	 * from the backend and return the data as a table.
	 * @param soldTo The sold to party the contract list is searched for.
	 * @param shop The shop the contract list is searched in.
	 * @param contractListFilter The filter criteria the contract list is
	 * searched for.
	 */
	public ContractHeaderList readHeader(
				Shop shop,
				ContractSearchCriteria contractSearchCriteria
				) throws CommunicationException {
                	
		final String METHOD = "readHeader()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("shop="+shop);
	 		ContractHeaderList contractHeaderList = null;

		try {
			// check if contracts are allowed by shop
			if (!shop.isContractAllowed()) {
				throw new ContractNotAllowedException();
			}
	
			// read the header list
			try {
				contractHeaderList =
					(ContractHeaderList)getContractBackend().readHeader(
						contractSearchCriteria.getTechKey(),
						shop.getLanguage());
	             
			}
			catch (BackendException backendException) {
				BusinessObjectHelper.splitException(backendException);
				return null; // never reached
			}
	
			// store attributes for subsequent reads
			if (!contractAttributes.containsKey(ContractView.CONTRACT_LIST)) {
				contractAttributes.put(ContractView.CONTRACT_LIST,
					contractHeaderList.getAttributes());
			}
	
			// message if no headers were found
			int numOfHeaders = contractHeaderList.size();
			if (numOfHeaders == 0) {
				contractHeaderList.addMessage(
					new Message(Message.INFO,"b2b.contract.message.noHits",null,"hits"));
			// store headers for subsequent reads
			} else {
				for (int i = 0; i < numOfHeaders; i++) {
					ContractHeader header = contractHeaderList.get(i);
					headerMap.put(header.getTechKey(),header);
				}
			}
		} finally {
			log.exiting();
		}
		return contractHeaderList;
	}
    /**
     * Read the item list for a given contract from the backend.
     * @param contractKey The technical key the selected contract.
     * @param shop The shop the contract item list is read in.
     * @param pageNumber Number of page within the item list
     * @param expandedItems List of items whose product list is expanded
     * @param RequestedQuantities Quantties of the items for call off
     */
    public ContractItemList readItems(
            TechKey contractKey,
            Shop shop,
            int pageNumber,
            int pageSize,
            TechKey visibleItem,
            TechKeySet expandedItemSet,
            ContractItemQuantityMap releaseQuantities)
            throws CommunicationException {
        
		final String METHOD = "readItems()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("contractKey="+contractKey+", shop="+shop
				+", pageNumber="+pageNumber+", pageSize="+pageSize
				+", visibleItem="+visibleItem);
        ContractItemList contractItemList = null;

        try {
        
	        // check if contracts are allowed by shop
	        if (!shop.isContractAllowed()) {
	            throw new ContractNotAllowedException();
	        }
	
	        // save state in inner selectect contract object
	        setSelectedContract(contractKey);
	        selectedContract.pageNumber = pageNumber;
	        selectedContract.pageSize = pageSize;
	        selectedContract.updateExpandedItems(expandedItemSet);
	        selectedContract.updateReleaseQuantities(releaseQuantities);
	
	        // read contract items from backend
	        try {
	            contractItemList = (ContractItemList)getContractBackend().readItems(
	                    contractKey,
	                    shop.getSalesOrganisation(),
	                    shop.getDistributionChannel(),
	                    shop.getLanguage(),
	                    pageNumber,
	                    pageSize,
	                    visibleItem,
	                    (TechKeySetData)expandedItemSet);
	        }
	        catch (BackendException ex) {
	            BusinessObjectHelper.splitException(ex);
	        }
	
	        // store attributes for subsequent reads
	        if (!contractAttributes.containsKey(ContractView.CONTRACT_LIST)) {
	            contractAttributes.put(ContractView.CONTRACT_LIST,
	                contractItemList.getAttributes());
	        }
	
	        // enhance the data of the item list
	        contractItemList.setContractHeader(selectedContract.getContract());
	        contractItemList.setReleaseQuantities(
	            selectedContract.releaseQuantities);
	        contractItemList.setExpansionMarks(expandedItemSet);
	        contractItemList.markNewMainItems();
	        contractItemList.setPageSize(pageSize);
	        selectedContract.lastPageNumber = contractItemList.getLastPageNumber();
        } finally {
        	log.exiting();
        }
        return contractItemList;
    }

    /**
     * Read a single item of a contract from the backend.
     * @param contractKey The technical key of the contract.
     * @param contractItemKey The technical key the item.
     * @param shop The shop the contract item list is read in.
     */
    public ContractItemList readItem(
            TechKey contractKey,
            TechKey contractItemKey,
            Shop shop)
            throws CommunicationException {
        
		final String METHOD = "readItem()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("contractKey="+contractKey+", contractItemKey="+contractItemKey
				+"shop="+shop);
        ContractItemList contractItemList = null;

		try {
		
	        // check if contracts are allowed by shop
	        if (!shop.isContractAllowed()) {
	            throw new ContractNotAllowedException();
	        }
	
	        // read contract items from backend
	        try {
	            contractItemList = (ContractItemList)getContractBackend().readItems(
	                    contractKey,
	                    shop.getSalesOrganisation(),
	                    shop.getDistributionChannel(),
	                    shop.getLanguage(),
	                    1,
	                    1,
	                    contractItemKey,
	                    new TechKeySet());
	        }
	        catch (BackendException ex) {
	            BusinessObjectHelper.splitException(ex);
	        }
	
	        // store attributes for subsequent reads
	        if (!contractAttributes.containsKey(ContractView.CONTRACT_LIST)) {
	            contractAttributes.put(ContractView.CONTRACT_LIST,
	                contractItemList.getAttributes());
	        }
		} finally {
			log.exiting();
		}
        // return item in a list
        return contractItemList;
    }

    /**
     * Read the contract references for a given list of products from the
     * backend.
     * @param productKeySet A list of product keys.
     * @param soldTo The sold to party the contract list is searched for.
     * @param shop The shop the contract item list is read in.
     * @param view Desired view onto the contract data (product list, detail or
     * contract list).
     * @return Contract references for the products in form of a
     * <code>ContractReferenceMap</code>.
     */
    public ContractReferenceMap readReferences(
            TechKeySet productKeySet,
            TechKey soldTo,
            Shop shop,
            ContractView view)
            throws CommunicationException {
        ContractReferenceMap contractReferenceMap = null;

		final String METHOD = "readReferences()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("soldTo="+soldTo+"shop="+shop);
		try {

        // check if contracts are allowed by shop
	        if (!shop.isContractAllowed()) {
	            throw new ContractNotAllowedException();
	        }
	
	        // read contract references from backend
	        try {
	            contractReferenceMap =
	            (ContractReferenceMap)getContractBackend().readReferences(
	                (TechKeySetData)productKeySet,
	                soldTo,
	                shop.getSalesOrganisation(),
	                shop.getDistributionChannel(),
	                shop.getLanguage(),
	                view);
	        }
	        catch (BackendException ex) {
	            BusinessObjectHelper.splitException(ex);
	        }
	
	        // store attributes for subsequent reads
	        if (!contractAttributes.containsKey(view)) {
	            contractAttributes.put(view, contractReferenceMap.getAttributes());
	        }
        } finally {
        	log.exiting();
        }
        return contractReferenceMap;
    }

    /**
     * Read the contract attributes for a given shop & view, if necessary from
     * the backend.
     * @param shop The shop the contract item list is read in.
     * @param view Desired view onto the contract data (product list, detail or
     * contract list).
     * @return Contract attributes in form of a
     * <code>ContractAttributeList</code>.
     */
    public ContractAttributeList readAttributes(
            Shop shop,
            ContractView view)
            throws CommunicationException {
        
		final String METHOD = "readAttributes()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("shop="+shop);
        ContractAttributeList contractAttributeList = null;
		try {
	
	        // check if contracts are allowed by shop
	        if (!shop.isContractAllowed()) {
	            throw new ContractNotAllowedException();
	        }
	
	        // if not already done so, read from backend
	        if (!contractAttributes.containsKey(view)) {
	            try {
	                contractAttributeList =
	                (ContractAttributeList)getContractBackend().readAttributes(
	                    shop.getLanguage(),
	                    view);
	                contractAttributes.put(view, contractAttributeList);
	            }
	            catch (BackendException ex) {
	                BusinessObjectHelper.splitException(ex);
	            }
	        }
	        else {
	            contractAttributeList = contractAttributes.get(view);
	        }
		} finally {
			log.exiting();
		}
        return contractAttributeList;
    }

    /**
     * Returns the page number currently specified.
     */
    public int getPageNumber() {
        return selectedContract.pageNumber;
    }

    /**
     * Returns the last page number.
     */
    public int getLastPageNumber() {
        return selectedContract.lastPageNumber;
    }

    /**
     * Returns the page size currently specified.
     */
    public int getPageSize() {
        return selectedContract.pageSize;
    }

    /**
     * Returns the set of expanded items currently specified.
     */
    public TechKeySet getExpandedItems() {
        return selectedContract.expandedItemSet;
    }

    /**
     * Returns the map of quantities currently specified for release order.
     */
    public ContractItemQuantityMap getReleaseQuantities() {
        return selectedContract.releaseQuantities;
    }

    /**
     * Adds/updates the map of configurations currently specified for release
     * order.
     */
    public void setReleasePriceConfigs(
            ContractItemPriceConfigMap releasePriceConfigs) {
        selectedContract.updateReleasePriceConfigs(releasePriceConfigs);
    }

    /**
     * Returns the map of configurations currently specified for release order.
     */
    public ContractItemPriceConfigMap getReleasePriceConfigs() {
        return selectedContract.releasePriceConfigs;
    }

    /**
     * Returns the currently selected contract.
     */
    public ContractHeader getSelectedContract() {
        if (selectedContract == null) {
            return null;
        }
        else {
            return selectedContract.getContract();
        }
    }

    /**
     * Retrieves the key of the selected contract.
     *
     * @return The selected contract's key
     */
    public TechKey getTechKey() {
        if (selectedContract != null) {
            ContractHeader header = selectedContract.getContract();
            if (header != null) {
                return header.getTechKey();
            }
        }
        return null;
    }

    /**
     * Set the currently selected contract. If the contract passed differs from
     * the contract currently selected, the selected contract and its data will
     * be lost.
     * @param contract <code>ContractHeader</code> of the contract to be
     * selected
     */
    public void setSelectedContract(TechKey contractKey) {
        ContractHeader contract = null;

        // store corresponding contract head
        if (!headerMap.containsKey(contractKey)) {
            throw new UnknownKeyException("Contract with key [" +
                    contractKey.getIdAsString() + "] not found!");
        }
        contract = (ContractHeader)headerMap.get(contractKey);
        if(selectedContract == null ||
            !selectedContract.getContract().getTechKey().equals(
                    contract.getTechKey())){
            selectedContract = new SelectedContract(contract);
        }
    }

    /**
     * Remove the currently selected contract.
     */
    public void removeSelectedContract() {
        selectedContract = null;
    }

    // get the contractBackend, if necessary
    protected ContractBackend getContractBackend()
            throws BackendException {
        if (contractBackend == null) {
            contractBackend = (ContractBackend) bem.createBackendBusinessObject(
                    BackendTypeConstants.BO_TYPE_CONTRACT,
                    CONTRACT_DATA_OBJECT_FACTORY);
        }
        return contractBackend;
    }
}
