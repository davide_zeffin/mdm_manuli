/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      7 August 2001

    $Revision: #1 $
    $Date: 2001/08/07 $
*****************************************************************************/
package com.sap.isa.businessobject.contract;

import com.sap.isa.backend.boi.isacore.contract.ContractItemConfigurationReferenceData;
import com.sap.isa.businessobject.BusinessObjectBase;

/**
 * Representation of a configuration reference of a contract item.
 */
public class ContractItemConfigurationReference
            extends BusinessObjectBase
            implements ContractItemConfigurationReferenceData {
    boolean initial = true;
    private String documentId = "";
    private String itemId = "";
    private String host = "";
    private String port = "";
    private String session = "";
    private String client = "";

    /**
     * Tells you, wheter the actual configuration reference data are initial or
     * not, i.e., if the item has been configured before or not.
     *
     * @return <code>true</code> if the configuration reference data are initial
     *         or <code>false</code>
     *         if its not.
     */
    public boolean isInitial() {
        return initial;
    }

    /**
     * Set the initial property.
     */
    public void setInitial(boolean initial) {
        this.initial = initial;
    }

    /**
     * Retrieves the documentId for the item configuration.
     */
    public String getDocumentId() {
        return documentId;
    }

    /**
     * Sets the documentId of the item.
     */
    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    /**
     * Retrieves the itemId for the configuration of the contract item.
     */
    public String getItemId() {
        return itemId;
    }

    /**
     * Sets the itemId of the item.
     */
    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    /**
     * Retrieves the host for the item configuration.
     */
    public String getHost() {
        return host;
    }

    /**
     * Sets the host of the item.
     */
    public void setHost(String host) {
        this.host = host;
    }

    /**
     * Retrieves the port for the item configuration.
     */
    public String getPort() {
        return port;
    }

    /**
     * Sets the port of the item.
     */
    public void setPort(String port) {
        this.port = port;
    }

    /**
     * Retrieves the session for the item configuration.
     */
    public String getSession() {
        return session;
    }

    /**
     * Sets the session of the item.
     */
    public void setSession(String session) {
        this.session = session;
    }

    /**
     * Retrieves the client for the item configuration.
     */
    public String getClient() {
        return client;
    }

    /**
     * Sets the client of the item.
     */
    public void setClient(String client) {
        this.client = client;
    }
}