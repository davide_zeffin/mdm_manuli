/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      20 March 2001

    $Revision: #2 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.businessobject.contract;

import java.util.ArrayList;
import java.util.Iterator;

import com.sap.isa.backend.boi.isacore.contract.ContractAttributeListData;
import com.sap.isa.backend.boi.isacore.contract.ContractAttributeValueMapData;
import com.sap.isa.backend.boi.isacore.contract.ContractItemData;
import com.sap.isa.backend.boi.isacore.contract.ContractItemListData;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.core.Iterable;
import com.sap.isa.core.TechKey;

/**
 * Represents a list of <code>ContractItem</code>s of a contract. Only the
 * items contained in the given page are contained.
 */
public class ContractItemList extends BusinessObjectBase
                              implements ContractItemListData, Iterable {
    protected ArrayList contractItems;
    protected ContractAttributeList attributes;
    protected ContractAttributeValueMap attributeValues;
    private ContractHeader contractHeader;
    private int pageNumber;
    private int lastPageNumber;
    private int pageSize;

    /**
     * Creates a new <code>ContractItemList</code> object.
     */
    public ContractItemList() {
        contractItems = new ArrayList();
    }

    /**
     * Sets the page number of the list.
     */
    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    /**
     * Retrieves the page number of the list.
     */
    public int getPageNumber() {
        return pageNumber;
    }

    /**
     * Sets the page size of the list.
     */
    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    /**
     * Retrieves the page size of the list.
     */
    public int getPageSize() {
        return pageSize;
    }

    /**
     * Sets the number of pages of the list.
     */
    public void setLastPageNumber(int lastPageNumber) {
        this.lastPageNumber = lastPageNumber;
    }

    /**
     * Retrieves the number of pages of the list.
     */
    public int getLastPageNumber() {
        return lastPageNumber;
    }

    /**
     * Sets the contract header key of the list.
     */
    public void setContractHeader(ContractHeader contractHeader) {
        this.contractHeader = contractHeader;
    }

    /**
     * Retrieves the contract header key of the list.
     */
    public ContractHeader getContractHeader() {
        return contractHeader;
    }

    /**
     * Adds a new <code>ContractItem</code> to the list.
     */
    public void add(ContractItem contractItem) {
        contractItems.add(contractItem);
    }

    /**
     * Adds a new <code>ContractItemData</code> to the list.
     */
    public void add(ContractItemData contractItem) {
        if (contractItem instanceof ContractItem) {
            contractItems.add((ContractItem)contractItem);
        }
    }

    /**
     * Returns the element at the specified position in this list.
      */
    public ContractItem get(int index) {
        return (ContractItem) contractItems.get(index);
    }

    /**
     * Returns the number of elemts in this list.
     */
    public int size() {
        return contractItems.size();
    }

    /**
     * Returns true if this list contains no data.
     */
    public boolean isEmpty() {
        return contractItems.isEmpty();
    }

    /**
     * Returns an iterator over the elements contained in the list
     */
    public Iterator iterator() {
        return contractItems.iterator();
    }

    /**
     * Retrieves the attribute list of the item list.
     */
    public ContractAttributeList getAttributes() {
        return attributes;
    }

    /**
     * Adds an attribute list to the item list
     */
    public void setAttributes(ContractAttributeList attributes) {
        this.attributes = attributes;
    }

    /**
     * Adds an attribute list to the item list
     */
    public void setAttributes(ContractAttributeListData attributes) {
        if (attributes instanceof ContractAttributeList) {
            this.attributes = (ContractAttributeList)attributes;
        }
    }

    /**
     * Retrieves the map of attribute values of the item list.
     */
    public ContractAttributeValueMap getAttributeValues() {
        return attributeValues;
    }

    /**
     * Adds an attribute value map to the item list
     */
    public void setAttributeValues(ContractAttributeValueMap attributeValues) {
        this.attributeValues = attributeValues;
    }

    /**
     * Adds an attribute value map to the item list
     */
    public void setAttributeValues(ContractAttributeValueMapData
            attributeValues) {
        if (attributeValues instanceof ContractAttributeValueMap) {
            this.attributeValues = (ContractAttributeValueMap)attributeValues;
        }
    }

    // Set release quantities for the items.
    public void setReleaseQuantities(
            final ContractItemQuantityMap releaseQuantities) {
        ContractItem item;
        for (int i = 0; i < size(); i++) {
            item = get(i);
            item.setRequested(releaseQuantities.containsKey(
                item.getContractItemTechKey()));
            item.setQuantity(releaseQuantities.getQuantity(
                item.getContractItemTechKey()));
        }
    }

    // Set expansion marks for the items.
    void setExpansionMarks (final TechKeySet expandedItems) {
        Iterator expandedItemsIterator = expandedItems.iterator();
        ContractItem item;
        TechKey oldKey = TechKey.EMPTY_KEY;
        TechKey newKey;
        for (int i = 0; i < size(); i++) {
            item = get(i);
            if (item.isMultiProduct() && !oldKey.equals(
                    newKey = item.getContractItemTechKey().getItemKey())) {

                // we have a new item, lets check for expansion
                if (expandedItems.contains(newKey)) {
                    item.setExpanded(true);
                    item.setCollapsed(false);
                }
                else {
                    item.setExpanded(false);
                    item.setCollapsed(true);
                }
                oldKey = newKey;
            }
        }
    }

    // Mark the first occurrence of a new main item in the list
    void markNewMainItems() {
        ContractItem item;
        TechKey oldKey = TechKey.EMPTY_KEY;
        TechKey newKey;
        for (int i = 0; i < size(); i++) {
            item = get(i);
            if (!oldKey.equals(newKey =
                    item.getContractItemTechKey().getItemKey())) {
                item.setNewMainItem(true);
                oldKey = newKey;
            }
        }
    }
}
