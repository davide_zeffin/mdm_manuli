/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      9 July 2001

    $Revision: #2 $
    $Date: 2001/07/13 $
*****************************************************************************/

package com.sap.isa.businessobject.contract;

import java.util.HashMap;
import java.util.Iterator;

/**
 * Map containing the prices and configuration values of products in a contract.
 */
public class ContractItemPriceConfigMap {
    protected HashMap contractItemPriceConfigs;

    /**
     * Inner class to store the requested PriceConfig information.
     */
    protected class RequestedPriceConfig {
        protected ContractItemPrice itemPrice = null;
        protected String configurationType = "";
        protected boolean ipcConfigurable = false;
        protected boolean isVariant = false;
        protected ContractItemConfigurationReference configItemReference = null;
        protected String configFilter = "";

        // some constructors
        public RequestedPriceConfig(
                ContractItemPrice itemPrice,
                String configurationType,
                boolean ipcConfigurable,
                boolean isVariant,
                ContractItemConfigurationReference configItemReference,
                String configFilter) {
            this.configurationType = configurationType;
            this.ipcConfigurable = ipcConfigurable;
            this.isVariant = isVariant;
            this.configItemReference = configItemReference;
            this.configFilter = configFilter;
        }
        public RequestedPriceConfig(ContractItem contractItem) {
            configurationType = contractItem.getConfigurationType();
            ipcConfigurable = contractItem.isIpcConfigurable();
            isVariant = contractItem.isVariant();
            itemPrice = contractItem.getItemPrice();
            configItemReference = contractItem.getConfigurationReference();
            configFilter = contractItem.getConfigFilter();
        }
        public ContractItemPrice getItemPrice() {
            return itemPrice;
        }
        public String getConfigurationType() {
            return configurationType;
        }
        public boolean isIpcConfigurable() {
            return ipcConfigurable;
        }
        public boolean isVariant() {
            return isVariant;
        }
        public ContractItemConfigurationReference getConfigItemReference() {
            return configItemReference;
        }
        public String getConfigFilter() {
            return configFilter;
        }
    }

    /**
     * Creates a new <code>ContractItemPriceConfigMap</code> object.
     */
    public ContractItemPriceConfigMap() {
        contractItemPriceConfigs = new HashMap();
    }

    /**
     * Creates a new <code>ContractItemPriceConfigMap</code> object from a
     * ContractItemList object.
     */
    public ContractItemPriceConfigMap(ContractItemList contractItemList) {
        this();
        if (contractItemList != null) {
            for (int i = 0; i < contractItemList.size(); i++) {
                put(contractItemList.get(i));
            }
        }
    }

    /**
     * Adds configration information to the Map.
     */
    public void put(
                ContractItemTechKey itemKey,
                ContractItemPrice itemPrice,
                String configurationType,
                boolean ipcConfigurable,
                boolean isVariant,
                ContractItemConfigurationReference configItemReference,
                String configFilter) {
        if (!containsKey(itemKey)) {
            contractItemPriceConfigs.put(
                itemKey, new RequestedPriceConfig(
                    itemPrice,
                    configurationType,
                    ipcConfigurable,
                    isVariant,
                    configItemReference,
                    configFilter));
        }
    }

    /**
     * Adds the configration information of a contract item to the Map.
     */
    public void put(ContractItem contractItem) {
        ContractItemTechKey itemKey = contractItem.getContractItemTechKey();
        if (!containsKey(itemKey)) {
            contractItemPriceConfigs.put(
                itemKey, new RequestedPriceConfig(contractItem));
        }
    }

    /**
     * Retrieves the ContractItemPrice for the specified item key.
     */
    public ContractItemPrice getItemPrice(ContractItemTechKey itemKey) {
        if (containsKey(itemKey)) {
            return ((RequestedPriceConfig)contractItemPriceConfigs.
                get(itemKey)).getItemPrice();
        }
        else {
            return null;
        }
    }

    /**
     * Returns the configuration type for the specified item key. "" is the
     * default answer if no entry is found in the map.
     */
    public String getConfigurationType(ContractItemTechKey itemKey) {
        if (containsKey(itemKey)) {
            return (String)((RequestedPriceConfig)contractItemPriceConfigs.
                get(itemKey)).getConfigurationType();
        }
        else {
            return "";
        }
    }

    /**
     * Returns the isConfigurable property for the specified item key. false is
     * the default answer if no entry is found in the map.
     */
    public boolean isIpcConfigurable(ContractItemTechKey itemKey) {
        if (containsKey(itemKey)) {
            return ((RequestedPriceConfig)contractItemPriceConfigs.
                get(itemKey)).isIpcConfigurable();
        }
        else {
            return false;
        }
    }

    /**
     * Returns the isVariant property for the specified item key. false is
     * the default answer if no entry is found in the map.
     */
    public boolean isVariant(ContractItemTechKey itemKey) {
        if (containsKey(itemKey)) {
            return ((RequestedPriceConfig)contractItemPriceConfigs.
                get(itemKey)).isVariant();
        }
        else {
            return false;
        }
    }

    /**
     * Returns the configuration item for the specified item key. null is the
     * default answer if no entry is found in the map.
     */
    public ContractItemConfigurationReference
            getConfigItemReference(ContractItemTechKey itemKey) {
        if (containsKey(itemKey)) {
            return ((RequestedPriceConfig)contractItemPriceConfigs.
                get(itemKey)).getConfigItemReference();
        }
        else {
            return null;
        }
    }

    /**
     * Returns the configuration type for the specified item key. "" is the
     * default answer if no entry is found in the map.
     */
    public String getConfigFilter(ContractItemTechKey itemKey) {
        if (containsKey(itemKey)) {
            return (String)((RequestedPriceConfig)contractItemPriceConfigs.
                get(itemKey)).getConfigFilter();
        }
        else {
            return "";
        }
    }

    /**
     * Removes the quantity for the specified item key from the map.
     */
    public void remove(ContractItemTechKey itemKey) {
        contractItemPriceConfigs.remove(itemKey);
    }

    /**
     * Removes all quantities from this map.
     */
    public void clear() {
        contractItemPriceConfigs.clear();
    }

    /**
     * Returns the number of quantities in this map.
     */
    public int size() {
        return contractItemPriceConfigs.size();
    }

    /**
     * Returns true if this map contains no quantities.
     */
    public boolean isEmpty() {
        return contractItemPriceConfigs.isEmpty();
    }

    /**
     * Checks if the map contains a given key.
     */
    public boolean containsKey(ContractItemTechKey itemKey) {
        return contractItemPriceConfigs.containsKey(itemKey);
    }

    /**
     * Returns an iterator view of the keys contained in this map.
     */
    public Iterator getItemKeyIterator() {
        return contractItemPriceConfigs.keySet().iterator();
    }
}