/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      20 March 2001

    $Revision: #2 $
    $Date: 2001/07/10 $
*****************************************************************************/

package com.sap.isa.businessobject.contract;

import com.sap.isa.core.TechKey;

/**
 * Class representing an identifier for an contract item object.
 * The key consists of a contract and a product technical key.
 * The technical keys may be a database primary key, an GUID or some other
 * identifier.
 */
public class ContractItemTechKey {
    protected TechKey itemKey;
    protected TechKey productKey;

    /**
     * Creates a new instance by providing a <code>TechKey</code> representation
     * of the item and product keys of a line item
     *
     * @param itemKey key of contract item
     * @param productKey key of contract item
     */
    public ContractItemTechKey(TechKey itemKey, TechKey productKey) {
        this.itemKey = itemKey;
        this.productKey = productKey;
    }

    /**
     * Creates a new instance by providing a <code>String</code> representation
     * of the item and product keys of a line item
     *
     * @param itemKey key of contract item as <code>String</code>
     * @param productKey key of contract item as <code>String</code>
     */
    public ContractItemTechKey(String itemKey, String productKey) {
        this.itemKey = new TechKey(itemKey);
        this.productKey = new TechKey(productKey);
    }

    /**
     * Retrieves the item key of the contract item key
     *
     * @return item key
     */
    public TechKey getItemKey() {
        return itemKey;
    }

    /**
     * Retrieves the product key of the contract item key
     *
     * @return product key
     */
    public TechKey getProductKey() {
        return productKey;
    }

    /**
     * Retrieves the contract item key as a <code>String</code>
     *
     * @return String representation of the contract item key
     */
    public String getKeyAsString() {
        return itemKey.getIdAsString() + "." + productKey.getIdAsString();
    }

    /**
     * Returns the hash code of the contract item key
     *
     * @return hash code
     */
    public int hashCode() {
        return itemKey.hashCode() ^ productKey.hashCode();
    }

    /**
     * Compares this contract item key to the specified object.
     * The result is <code>true</code> if and only if the argument is not
     * <code>null</code> and is a <code>ContractItemKey</code> object that
     * represents the same key value as this object.
     *
     * @param object Object to compare with
     * @return <code>true</code> if the keys are identical; otherwiese
     *         <code>false</code>.
     */
    public boolean equals(Object object) {
        if ((object == null) || !(object instanceof ContractItemTechKey)) {
            return false;
        }
        ContractItemTechKey castedObject = (ContractItemTechKey)object;
        return itemKey.equals(castedObject.getItemKey()) &&
            productKey.equals(castedObject.getProductKey());
    }
}
