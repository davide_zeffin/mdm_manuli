/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      3 April 2001

    $Revision: #3 $
    $Date: 2001/07/31 $
*****************************************************************************/
package com.sap.isa.businessobject.contract;

import com.sap.isa.backend.boi.isacore.contract.ContractAttributeData;
import com.sap.isa.backend.boi.isacore.contract.ContractAttributeListData;
import com.sap.isa.backend.boi.isacore.contract.ContractAttributeValueData;
import com.sap.isa.backend.boi.isacore.contract.ContractAttributeValueListData;
import com.sap.isa.backend.boi.isacore.contract.ContractAttributeValueMapData;
import com.sap.isa.backend.boi.isacore.contract.ContractDataObjectFactoryBackend;
import com.sap.isa.backend.boi.isacore.contract.ContractHeaderData;
import com.sap.isa.backend.boi.isacore.contract.ContractHeaderListData;
import com.sap.isa.backend.boi.isacore.contract.ContractItemConfigurationReferenceData;
import com.sap.isa.backend.boi.isacore.contract.ContractItemData;
import com.sap.isa.backend.boi.isacore.contract.ContractItemListData;
import com.sap.isa.backend.boi.isacore.contract.ContractReferenceData;
import com.sap.isa.backend.boi.isacore.contract.ContractReferenceListData;
import com.sap.isa.backend.boi.isacore.contract.ContractReferenceMapData;

/**
 * Class used for the creation of contract data objects within the BE.
 */
public class ContractDataObjectFactory
       implements ContractDataObjectFactoryBackend {

    /**
     * Creates a new <code>ContractHeaderData</code> object.
     *
     * @return The newly created object
     */
    public ContractHeaderData createContractHeaderData() {
        return (ContractHeaderData)(new ContractHeader());
    }

    /**
     * Creates a new <code>ContractHeaderListData</code> object.
     *
     * @return The newly created object
     */
    public ContractHeaderListData createContractHeaderListData() {
        return (ContractHeaderListData)(new ContractHeaderList());
    }

    /**
     * Creates a new <code>ContractItemData</code> object.
     *
     * @return The newly created object
     */
    public ContractItemData createContractItemData() {
        return (ContractItemData)(new ContractItem());
    }

    /**
     * Creates a new <code>ContractItemListData</code> object.
     *
     * @return The newly created object
     */
    public ContractItemListData createContractItemListData() {
        return (ContractItemListData)(new ContractItemList());
    }

    /**
     * Creates a new <code>ContractReferenceData</code> object.
     *
     * @return The newly created object
     */
    public ContractReferenceData createContractReferenceData() {
        return (ContractReferenceData)(new ContractReference());
    }

    /**
     * Creates a new <code>ContractReferenceListData</code> object.
     *
     * @return The newly created object
     */
    public ContractReferenceListData createContractReferenceListData() {
        return (ContractReferenceListData)(new ContractReferenceList());
    }

    /**
     * Creates a new <code>ContractReferenceMapData</code> object.
     *
     * @return The newly created object
     */
    public ContractReferenceMapData createContractReferenceMapData(){
        return (ContractReferenceMapData)(new ContractReferenceMap());
    }

    /**
     * Creates a new <code>ContractAttributeData</code> object.
     *
     * @return The newly created object
     */
    public ContractAttributeData createContractAttributeData() {
        return (ContractAttributeData)(new ContractAttribute());
    }

    /**
     * Creates a new <code>ContractAttributeListData</code> object.
     *
     * @return The newly created object
     */
    public ContractAttributeListData createContractAttributeListData() {
        return (ContractAttributeListData)(new ContractAttributeList());
    }

    /**
     * Creates a new <code>ContractAttributeValueData</code> object.
     *
     * @return The newly created object
     */
    public ContractAttributeValueData createContractAttributeValueData() {
        return (ContractAttributeValueData)(new ContractAttributeValue());
    }

    /**
     * Creates a new <code>ContractAttributeValueListData</code> object.
     *
     * @return The newly created object
     */
    public ContractAttributeValueListData
            createContractAttributeValueListData() {
        return (ContractAttributeValueListData)(
            new ContractAttributeValueList());
    }

    /**
     * Creates a new <code>ContractAttributeValueMapData</code> object.
     *
     * @return The newly created object
     */
    public ContractAttributeValueMapData createContractAttributeValueMapData(){
        return (ContractAttributeValueMapData)(new ContractAttributeValueMap());
    }

    /**
     * Creates a new <code>ContractItemConfigurationReferenceData</code> object.
     *
     * @return The newly created object
     */
    public ContractItemConfigurationReferenceData
            createContractItemConfigurationReferenceData() {
        return (ContractItemConfigurationReferenceData)
            (new ContractItemConfigurationReference());
    }
}