/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      1 June 2001

    $Revision: #3 $
    $Date: 2001/07/25 $
*****************************************************************************/
package com.sap.isa.businessobject.contract;

import com.sap.isa.backend.boi.ipc.ItemConfigurationReferenceData;
import com.sap.isa.backend.boi.webcatalog.pricing.ItemPriceRequest;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.core.TechKey;

/**
 * Class containing the data needed by the ipc to calculate the price of a
 * product in a contract.
 */
public class ContractItemPriceRequest implements ItemPriceRequest {
    private TechKey contractKey = null;
    protected ContractItem contractItem = null;

    /**
     * Constructor.
     */
    public ContractItemPriceRequest(TechKey contractKey, ContractItem contractItem) {

        this.contractKey = contractKey;
        this.contractItem = contractItem;
    }

    /**
     * Returns the property product
     */
    public String getProductKey() {
        return contractItem.getContractItemTechKey().getProductKey().getIdAsString();
    }

    /**
     * Returns the property product
     */
    public String getProductId() {
        return contractItem.getProductId();
    }

    /**
     * Retrieves the requested quantity of the product of the item.
     */
    public String getQuantity() {
        return contractItem.getQuantity();
    }

    /**
     * Retrieves the unit of the requested quantity of the product of the item.
     */
    public String getUnit() {
        return contractItem.getUnit();
    }

    /**
     * Returns the property contractKey
     */
    public String getContractKey() {
        return contractKey.getIdAsString();
    }

    /**
     * Returns the property contractItemKey
     */
    public String getContractItemKey() {
        return contractItem.getContractItemTechKey().getItemKey().getIdAsString();
    }

    /**
     * Returns the catalog item, only needed for list pricing.
     */
    public WebCatItem getCatItem() {
        return (WebCatItem) null;
    }

    /**
     * Returns a configuration reference of the item needed for pricing (if
     * existing).
     *
     * @return configurationReferenceData
     */
    public ItemConfigurationReferenceData getConfigurationReference() {
        return (ItemConfigurationReferenceData) contractItem.getConfigurationReference();
    }

    public Object getConfigurationItem() {
        return null;
    }

    public String toString() {

        StringBuffer buf = new StringBuffer("ContractItemPriceRequest:");
        buf.append("Product=").append(getProductId());
        buf.append(",ProductKey=").append(getProductKey());
        buf.append(",Quantity=").append(getQuantity());
        buf.append(",Unit=").append(getUnit());
        buf.append(",CatalogItem=").append(getCatItem());
        buf.append(",ConfigItem=").append(getConfigurationItem());
        buf.append(",ConfigRef=").append(getConfigurationReference());
        buf.append(",ContractKey=").append(getContractKey());
        buf.append(",ContractItemKey=").append(getContractItemKey());


        return buf.toString();
    }
}