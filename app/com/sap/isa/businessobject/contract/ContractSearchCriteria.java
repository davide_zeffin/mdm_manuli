/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      30.4.2001

    $Revision: #1 $
    $Date: 2001/06/26 $
*****************************************************************************/
package com.sap.isa.businessobject.contract;
import com.sap.isa.core.TechKey;

/**
 * Command for the search logic to retrieve a list of all contracts matching
 * the given criteria.
 *
 */
public class ContractSearchCriteria {
	private TechKey techKey;
    private String id;
    private String externalRefNo;
    private String description;
    private String validFromDate;
    private String validToDate;
    private String status;
    public final static String STATUS_ALL        = "ALL";
    public final static String STATUS_IN_PROCESS = "IN_PROCESS";
    public final static String STATUS_RELEASED   = "RELEASED";
    public final static String STATUS_REJECTED   = "REJECTED";
    

    /**
     * No-argument constructor.
     */
    public ContractSearchCriteria() {
        myClear();
    }

    /**
     * Constructor setting all filter criteria.
     */
    public ContractSearchCriteria (
            String id,
            String externalRefNo,
            String description,
            String validFromDate,
            String validToDate,
            String status) {
        this.techKey = null;
        this.id = id;
        this.externalRefNo = externalRefNo;
        this.validFromDate = validFromDate;
        this.validToDate = validToDate;
        this.description = description;
        this.status = status;
    }

	/**
	 * Constructor setting all filter criteria.
	 */
	public ContractSearchCriteria (
	        TechKey techKey,
			String id,
			String externalRefNo,
			String description,
			String validFromDate,
			String validToDate,
			String status) {
		this.techKey = techKey;
		this.id = id;
		this.externalRefNo = externalRefNo;
		this.validFromDate = validFromDate;
		this.validToDate = validToDate;
		this.description = description;
		this.status = status;
	}
	
    /**
     * Retrieves the filter criterion for the contract id.
     *
     * @return The contract id
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the filter criterion for the contract id.
     *
     * @param id The id to be set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Retrieves the filter criterion for the external reference number.
     */
    public String getExternalRefNo() {
        return externalRefNo;
    }

    /**
     * Sets the filter criterion for the external reference number.
     */
    public void setExternalRefNo(String externalRefNo) {
        this.externalRefNo = externalRefNo;
    }

    /**
     * Retrieves the filter criterion for the valid from date.
     */
    public String getValidFromDate() {
        return validFromDate;
    }

    /**
     * Sets the filter criterion for the valid from date.
     */
    public void setValidFromDate(String validFromDate) {
        this.validFromDate = validFromDate;
    }

    /**
     * Retrieves the filter criterion for the valid to date.
     */
    public String getValidToDate() {
        return validToDate;
    }

    /**
     * Sets the filter criterion for the valid to date.
     */
    public void setValidToDate(String validToDate) {
        this.validToDate = validToDate;
    }

    /**
     * Retrieves the filter criterion for the status.
     */
    public String getStatus() {
        return status;
    }

   /**
     * Sets the filter criterion for the status all
     */
    public void setStatusAll() {
        this.status = STATUS_ALL;
    }
    
    /**
     * Sets the filter criterion for the status in process
     */
    public  void setStatusInProcess() {
        this.status = STATUS_IN_PROCESS;
    }

   /**
     * Sets the filter criterion for the status rejected 
     */
    public  void setStatusRejected() {
        this.status = STATUS_REJECTED;
    }  
 
   /**
     * Sets the filter criterion for the status released 
     */
    public void setStatusReleased() {
        this.status = STATUS_RELEASED;
    } 
         
    /**
     * Retrieves the filter criterion for the language dependent description.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the filter criterion for the language dependent description.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /* clears the properties */
    protected void myClear() {
        id = "";
        externalRefNo = "";
        validFromDate = "";
        validToDate = "";
        description = "";
        status = "";
    }

    /**
     * Clears all property fields
     */
    public void clear() {
        myClear();
    }
	/**
	 * Retrieves the filter criterion for the contract key
	 * 
	 * @return
	 */
	public TechKey getTechKey() {
		return techKey;
	}

	/**
	 * Sets the filter criterion for the contract key
	 * 
	 * @param key
	 */
	public void setTechKey(TechKey key) {
		techKey = key;
	}

}