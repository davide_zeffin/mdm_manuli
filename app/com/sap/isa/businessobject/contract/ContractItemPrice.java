/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      14 June 2001

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.businessobject.contract;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.sap.spc.remote.client.object.IPCItem;
import com.sap.isa.backend.boi.webcatalog.pricing.PriceInfo;

/**
 * Price and configuration values of a product in a contract.
 */
public class ContractItemPrice {
    protected List priceInfoList = null;

    // reference to the configuration, to be used by some external configuration
    // engine. Currently, this is allways the IPC.
    protected IPCItem configItemReference = null;

    // separator between the components (e.g. value, currency, unit) of a price
    protected final static String SEPARATOR = "&nbsp;";

    /**
     * Constructor. Creates a price instance from existing list of priceInfos
     * that either contains a single price or a whole price scale for the item.
     */
    public ContractItemPrice(List priceInfoList) {
        if (priceInfoList != null) {
            this.priceInfoList = priceInfoList;
        }
        else {
            this.priceInfoList = new ArrayList();
        }
    }

    /**
     * Innner class that provides a String representation of a Price, separated
     * into value and remainder.
     */
    public final class PriceAsString {
        protected String value = "";
        protected String currencyAndUnit = "";

        PriceAsString (PriceInfo priceInfo) {
            if (priceInfo != null) {
                value = priceInfo.getPrice();

                StringBuffer entry = new StringBuffer();
                entry.append(priceInfo.getCurrency());

                // scale type (from, up to)
                String type = priceInfo.getScaletype();
                if(type != null && type.length() != 0) {
                    entry.append(SEPARATOR);
                    entry.append(type);
                }
                String quantity = priceInfo.getQuantity();
                if (quantity != null && quantity.length() > 0) {
                    entry.append(SEPARATOR);
                    entry.append(quantity);
                }
                String unit = priceInfo.getUnit();
                if (unit != null && unit.length() > 0) {
                    entry.append(SEPARATOR);
                    entry.append(unit);
                }
                currencyAndUnit = entry.toString();
            }
        }

        public String getValue() {
            return value;
        }

        public String getCurrencyAndUnit () {
            return currencyAndUnit;
        }
    }

    /**
     * Retrieves price information as PriceInfo object.
     * Retrieve the price as defined for this item. The price may be calculated
     * via IPC or may be a list or scale price.
     * In case of scale prices, this method only returns the first object.
     */
    public PriceInfo getPriceInfo() {
        if (priceInfoList.isEmpty()) {
            return null;
        }

        // return only first PriceInfo with this method.
        else {
            return (PriceInfo) priceInfoList.get(0);
        }
    }

    /**
     * Retrieves a string representation of the price of a contract item.
     * Retrieve the price as defined for this item. The price may be calculated
     * via IPC or may be a list or scale price.
     * In case of scale prices, this method only returns the first line. Use
     * <code>getPrices()</code> instead for items with scale prices.
     */
    public PriceAsString getPrice() {
        if (priceInfoList.isEmpty()) {
            return new PriceAsString( (PriceInfo)null );
        }

        // return only the first price with this method, even if we have scale
        // prices
        else {
            return new PriceAsString( (PriceInfo)priceInfoList.get(0) );
        }
    }

    /**
      * Returns reference to configuration (if available).
      * @return reference to configuration
      */
    public IPCItem getConfigItemReference() {
        PriceInfo priceInfo = null;

        // if not determined so far, try to determine now
        if (configItemReference == null) {
            priceInfo = getPriceInfo();
            if (priceInfo != null) {
                configItemReference =
                    (IPCItem) priceInfo.getPricingItemReference();
            }
        }
        return configItemReference;
    }

    /**
     * Retrieves an Iterator of string representations of the scale prices of a
     * contract item.
     * This method is useful if scale pricing is used.
     * If the item has no scale price the returned Iterator only has one element
     * which is the same as would be retrieved from <code>getPrice()</code>.
     * If no prices are available an empty iterator will be returned.
     * @return iterator Iterator of the scale prices.
     */
    public Iterator getPrices() {
        ArrayList pricesAsString = new ArrayList(priceInfoList.size());
        Iterator priceIterator = priceInfoList.iterator();
        while (priceIterator.hasNext()) {
            PriceInfo priceInfo = (PriceInfo) priceIterator.next();
            pricesAsString.add(new PriceAsString(priceInfo));
        }
        return pricesAsString.iterator();
    }

    /**
     * Retrieves the scalePrice property.
     */
    public boolean isScalePrice() {
        return (priceInfoList.size() > 1) ? true : false;
    }
}