/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      25 April 2001

    $Revision: #2 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.businessobject.contract;

import java.util.ArrayList;
import java.util.Iterator;

import com.sap.isa.backend.boi.isacore.contract.ContractAttributeData;
import com.sap.isa.backend.boi.isacore.contract.ContractAttributeListData;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.core.Iterable;

/**
 * Represents a list of <code>ContractAttribute</code>s of a contract item.
 */
public class ContractAttributeList
                    extends BusinessObjectBase
                    implements ContractAttributeListData, Iterable {
    private String itemId = "";
    protected ArrayList contractAttributes;

    /**
     * Creates a new <code>ContractAttributeList</code> object.
     */
    public ContractAttributeList() {
        contractAttributes = new ArrayList();
    }

    /**
     * Adds a new <code>ContractAttribute</code> to the list.
     */
    public void add(ContractAttribute contractAttribute) {
        contractAttributes.add(contractAttribute);
    }

    /**
     * Adds a new <code>ContractAttribute</code> to the list.
     */
    public void add(ContractAttributeData contractAttribute) {
        if (contractAttribute instanceof ContractAttribute) {
            add((ContractAttribute)contractAttribute);
        }
    }

    /**
     * Returns the element at the specified position in this list.
      */
    public ContractAttribute get(int index) {
        return (ContractAttribute) contractAttributes.get(index);
    }

    /**
     * Set the item id of the attribute list
     */
    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    /**
     * Retrieves the item id of the attribute list
     */
    public String getItemId() {
        return itemId;
    }

    /**
     * Returns the number of elemts in this list.
     */
    public int size() {
        return contractAttributes.size();
    }

    /**
     * Returns true if this list contains no data.
     */
    public boolean isEmpty() {
        return contractAttributes.isEmpty();
    }

    /**
     * Returns an iterator over the elements contained in the list
     */
    public Iterator iterator() {
        return contractAttributes.iterator();
    }
}
