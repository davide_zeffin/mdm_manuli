/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      25 April 2001

    $Revision: #2 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.businessobject.contract;

import java.util.ArrayList;
import java.util.Iterator;

import com.sap.isa.backend.boi.isacore.contract.ContractAttributeValueData;
import com.sap.isa.backend.boi.isacore.contract.ContractAttributeValueListData;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.core.Iterable;

/**
 * Represents a list of <code>ContractAttributeValue</code>s of a contract item.
 */
public class ContractAttributeValueList
                    extends BusinessObjectBase
                    implements ContractAttributeValueListData, Iterable {
    private String itemId = "";
    protected ArrayList contractItemAttributes;

    /**
     * Creates a new <code>ContractAttributeValueList</code> object.
     */
    public ContractAttributeValueList() {
        contractItemAttributes = new ArrayList();
    }

    /**
     * Adds a new <code>ContractAttributeValue</code> to the list.
     */
    public void add(ContractAttributeValue contractItemAttribute) {
        contractItemAttributes.add(contractItemAttribute);
    }

    /**
     * Adds a new <code>ContractAttributeValue</code> to the list.
     */
    public void add(ContractAttributeValueData contractItemAttribute) {
        if (contractItemAttribute instanceof ContractAttributeValue) {
            add((ContractAttributeValue)contractItemAttribute);
        }
    }

    /**
     * Returns the element at the specified position in this list.
      */
    public ContractAttributeValue get(int index) {
        return (ContractAttributeValue) contractItemAttributes.get(index);
    }

    /**
     * Set the item id of the attribute list
     */
    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    /**
     * Retrieves the item id of the attribute list
     */
    public String getItemId() {
        return itemId;
    }

    /**
     * Returns the number of elemts in this list.
     */
    public int size() {
        return contractItemAttributes.size();
    }

    /**
     * Returns true if this list contains no data.
     */
    public boolean isEmpty() {
        return contractItemAttributes.isEmpty();
    }

    /**
     * Returns an iterator over the elements contained in the list
     */
    public Iterator iterator() {
        return contractItemAttributes.iterator();
    }
}
