/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      25 April 2001

    $Revision: #2 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.businessobject.contract;

import java.util.HashMap;

import com.sap.isa.backend.boi.isacore.contract.ContractAttributeValueListData;
import com.sap.isa.backend.boi.isacore.contract.ContractAttributeValueMapData;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.core.TechKey;

/**
 * Map containing the <code>ContractAttributeValueList</code>s of contract
 * items.
 */
public class ContractAttributeValueMap extends BusinessObjectBase
                                       implements ContractAttributeValueMapData {
    protected HashMap contractAttributeValues;

    /**
     * Creates a new <code>ContractAttributeValueMap</code> object.
     */
    public ContractAttributeValueMap() {
        contractAttributeValues = new HashMap();
    }

    /**
     * Adds a new list of attributes to the Map.
     */
    public void put(TechKey itemKey,
            ContractAttributeValueList attributeList) {
        contractAttributeValues.put(itemKey, attributeList);
    }

    /**
     * Adds a new list of attributes to the Map.
     */
    public void put(TechKey itemKey,
            ContractAttributeValueListData attributeList){
        if (attributeList instanceof ContractAttributeValueList) {
            contractAttributeValues.put(itemKey,
                (ContractAttributeValueList)attributeList);
        }
    }

    /**
     * Returns the list of attributes for the specified item key.
     */
    public ContractAttributeValueList get(TechKey itemKey) {
        if (containsKey(itemKey)) {
            return (ContractAttributeValueList)contractAttributeValues.
                get(itemKey);
        }
        else {
            return new ContractAttributeValueList();
        }
    }

    /**
     * Returns the list of attributes for the specified item key as an
     * <code>ContractAttributeValueListData</code> object.
     */
    public ContractAttributeValueListData getData(TechKey itemKey) {
        if (containsKey(itemKey)) {
            return (ContractAttributeValueListData)contractAttributeValues.
                get(itemKey);
        }
        else {
            return (ContractAttributeValueListData)
                (new ContractAttributeValueList());
        }
    }

    /**
     * Removes the list of attributes for the specified item key from the map.
     */
    public ContractAttributeValueList remove(TechKey itemKey) {
        return (ContractAttributeValueList)contractAttributeValues.
            remove(itemKey);
    }

    /**
     * Removes all lists of attributes from this map.
     */
    public void clear() {
        contractAttributeValues.clear();
    }

    /**
     * Returns the number of lists of attributes in this map.
     */
    public int size() {
        return contractAttributeValues.size();
    }

    /**
     * Returns true if this map contains no lists of attributes.
     */
    public boolean isEmpty() {
        return contractAttributeValues.isEmpty();
    }

    /**
     * Checks if the map contains a given key.
     */
    public boolean containsKey(TechKey itemKey) {
        return contractAttributeValues.containsKey(itemKey);
    }
}