/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      20 March 2001

    $Revision: #2 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.businessobject.contract;

import java.util.HashSet;
import java.util.Iterator;

import com.sap.isa.backend.boi.isacore.contract.TechKeySetData;
import com.sap.isa.core.Iterable;
import com.sap.isa.core.TechKey;

/**
 * Class that provides a set of TechKeys.
 */
public class TechKeySet implements TechKeySetData, Iterable {
    protected HashSet techKeys;

    /**
     * Creates a new <code>TechKeySet</code> object.
     */
    public TechKeySet() {
        techKeys = new HashSet();
    }

    /**
     * Adds a new key to the set.
     */
    public boolean add(TechKey key) {
        return techKeys.add(key);
    }

    /**
     * Removes the specified key from the set.
     */
    public boolean remove(TechKey key) {
        return techKeys.remove(key);
    }

    /**
     * Removes all keys from this Set.
     */
    public void clear() {
        techKeys.clear();
    }

    /**
     * Returns the number of keys in this set.
     */
    public int size() {
        return techKeys.size();
    }

    /**
     * Returns true if this set contains no keys.
     */
    public boolean isEmpty() {
        return techKeys.isEmpty();
    }

    /**
     * Checks if the set contains a given key.
     */
    public boolean contains(TechKey key) {
        return techKeys.contains(key);
    }

    /**
     * Returns an <code>Iterator</code> over the keys contained in the set.
     */
    public Iterator iterator() {
        return techKeys.iterator();
    }
}