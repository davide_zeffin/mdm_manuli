/*****************************************************************************
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      18 April 2000

    $Revision: #2 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.businessobject.contract;

import java.util.HashMap;

import com.sap.isa.backend.boi.isacore.contract.ContractAttributeListData;
import com.sap.isa.backend.boi.isacore.contract.ContractReferenceListData;
import com.sap.isa.backend.boi.isacore.contract.ContractReferenceMapData;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.core.TechKey;

/**
 * Class containing contractreferences of products.
 */
public class ContractReferenceMap
    extends BusinessObjectBase
    implements ContractReferenceMapData {
    protected HashMap referenceMap;
    private ContractAttributeList attributes;

    /**
     * Creates a new <code>ContractReferenceMap</code> object.
     */
    public ContractReferenceMap() {
        referenceMap = new HashMap();
    }

    /**
     * Adds a new <code>ContractReferenceList</code> object to the Map.
     */
    public void put(TechKey productKey,
            ContractReferenceList contractReference) {
        referenceMap.put(productKey, contractReference);
    }

    /**
     * Adds a new <code>ContractReferenceListData</code> object to the Map.
     */
    public void put(TechKey productKey,
            ContractReferenceListData contractReference){
        if (contractReference instanceof ContractReferenceList) {
            referenceMap.put(productKey,
                (ContractReferenceList)contractReference);
        }
    }

    /**
     * Returns the list of contract references for the specified item key.
     */
    public ContractReferenceList get(TechKey productKey) {
        if (containsKey(productKey)) {
            return (ContractReferenceList)referenceMap.
                get(productKey);
        }
        else {
            return new ContractReferenceList();
        }
    }

    /**
     * Removes the list of contract references for the specified item key from the map.
     */
    public ContractReferenceList remove(TechKey productKey) {
        return (ContractReferenceList)referenceMap.
            remove(productKey);
    }

    /**
     * Removes all lists of contract references from this map.
     */
    public void clear() {
        referenceMap.clear();
    }

    /**
     * Returns the number of lists of contract references in this map.
     */
    public int size() {
        return referenceMap.size();
    }

    /**
     * Returns true if this map contains no lists of contract references.
     */
    public boolean isEmpty() {
        return referenceMap.isEmpty();
    }

    /**
     * Checks if the map contains a given key.
     */
    public boolean containsKey(TechKey productKey) {
        return referenceMap.containsKey(productKey);
    }

    /**
     * Retrieves the attribute list of the reference map.
     */
    public ContractAttributeList getAttributes() {
        return attributes;
    }

    /**
     * Adds an attribute list to the reference map.
     */
    public void setAttributes(ContractAttributeList attributes) {
        this.attributes = attributes;
    }

    /**
     * Adds an attribute list to the reference map.
     */
    public void setAttributes(ContractAttributeListData attributes) {
        if (attributes instanceof ContractAttributeList) {
            this.attributes = (ContractAttributeList)attributes;
        }
    }
}
