/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      25 April 2001

    $Revision: #2 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.businessobject.contract;

import com.sap.isa.backend.boi.isacore.contract.ContractAttributeData;
import com.sap.isa.businessobject.BusinessObjectBase;

/**
 * Represents an attribute of a contract item.
 */
public class ContractAttribute extends BusinessObjectBase
                               implements ContractAttributeData {
    private String id;
    private String description;
    private String unitDescription;

    /**
     * Retrieves the language independent id of the attribute.
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the language independent id of the attribute.
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Retrieves the language dependent name of the attribute.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the language dependent name of the attribute.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Retrieves the language dependent name of the unit of the attribute.
     */
    public String getUnitDescription() {
        return unitDescription;
    }

    /**
     * Sets the language dependent name of the unit of the attribute.
     */
    public void setUnitDescription(String unitDescription) {
        this.unitDescription = unitDescription;
    }
}
