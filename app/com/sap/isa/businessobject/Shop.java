/*****************************************************************************
    Class:        Shop
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Created:      M�rz 2001
    Version:      1.0

    $Revision: #8 $
    $Date: 2002/02/27 $
*****************************************************************************/
package com.sap.isa.businessobject;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.sap.isa.backend.BackendTypeConstants;
import com.sap.isa.backend.boi.isacore.ExtendedShopData;
import com.sap.isa.backend.boi.isacore.PricingInfoData;
import com.sap.isa.backend.boi.isacore.ShopBackend;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.order.ItemListData;
import com.sap.isa.core.Iterable;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.dealerlocator.backend.boi.DealerLocatorConfiguration;
import com.sap.isa.isacore.auction.AuctionBasketHelperData;

/**
 * The Shop object contains all kind of settings for the application.<p>
 *
 * The shop object use <code>ShopBackend</code> to comunicate with the backend.<br>
 *
 * <i>For further details see the interface <code>ShopData</code></i>
 *
 * @see com.sap.isa.backend.boi.isacore.ShopData
 * @see com.sap.isa.backend.boi.isacore.ShopBackend
 *
 * @version 1.0
 */
public class Shop extends BusinessObjectBase implements BackendAware, Iterable,
            ShopData, DealerLocatorConfiguration,  Cloneable, ExtendedShopData {

    private BackendObjectManager bem;
    private ShopBackend backendService;

    private String defaultCountry;
    private ResultData countryList;
    private Map regionTableMap = new HashMap();
    private Map personTitleKeys = new HashMap();
    private Map taxCodeTableMap = new HashMap();
    private Map countryDescriptions = new HashMap();

    private Map  availibiltyEvents     = new HashMap();

    // product substitution and determination
    protected HashMap substitutionReasons = new HashMap();
    protected HashMap enteredProductIdTypes = new HashMap();
	protected HashMap pricingInfoMap = new HashMap();


    private List availibilityIntervals = new ArrayList();
    private List trafficLightList = new ArrayList();

    private ResultData titleList;

	private TechKey attributeSetId;
	private TechKey newsletterAttributeSetId;

	private String resellerPartnerFunction;
	private String application;
    private String id;
    private String idTypeForVerification;
    private String country;
    private String currency;
    private String dateFormat;
    private String decimalPointFormat;
    private String decimalSeparator;
    private String description;
    private String division;
    private String distributionChannel;
    private String groupingSeparator;
    private String language;
    
    // date to be used for IPC issues during catalogue staging tests
	private String catalogStagingIPCDate;
    
    private String languageIso;
    
    private String responsibleSalesOrganisation = "";
    private String salesOffice;
    private String salesOrganisation;
    private String scenario;
    private String catalog;
    private String[] views;
    private String processType;
    private String lateDecisionProcType;
    private String quotationProcessType;
    private String companyPartnerFunction;
    private String campaignKey;
    private ShopData.BillingSelect billingSelection = null;
    private int addressFormat;
    private String selectionProcessType = "";
    private String payer;
    
    private ResultData contractNegotiationProcessTypes;
    private ResultData trafficLights;

    private NumberFormat numberFormat = NumberFormat.getInstance();
    private boolean numberFormatChanged;

	private boolean isBpHierarchyEnable = false;

    private boolean isListPriceUsed;
    private boolean isNoPriceUsed;
	private boolean isIPCPriceUsed;
    private boolean isEventCapturingActived;
    private boolean isQuotationAllowed;
    private boolean isQuotationOrderingAllowed;
    private boolean isQuotationExtended = false;
    private boolean isQuotationSearchAllTypesRequested = false;
	private boolean isContractInCatalogAllowed = false;
    private boolean isContractAllowed;
    private boolean isContractNegotiationAllowed = false;
    private boolean isTemplateAllowed;
	private boolean isSaveBasketAllowed;
	private boolean isPaymAtBpEnabled;
	private boolean isPayMask4CollAuthEnabled;
    private boolean isDocTypeLateDecision;
    private boolean isSoldtoSelectable;
    private boolean isCatalogDeterminationActived;
    private boolean isBestsellerAvailable;
    private boolean isRecommendationAvailable;
    private boolean isPersonalRecommendationAvailable;
    private boolean isUserProfileAvailable;
    private boolean isOrderingOfNonCatalogProductsAllowed;
    private boolean isPricingCondsAvailable;
    private boolean isCuaAvailable;
    private boolean isMarketingForUnknownUserAllowed = false;
    private boolean isAccessoriesAvailable;
    private boolean isCrossSellingAvailable;
	private boolean isRemanufactureAvailable;
	private boolean isNewPartAvailable;
    private boolean isAlternativeAvailable;
    private boolean isShowConfirmedDeliveryDateRequested = false;
    private boolean isOrderSimulateAvailable;
    private boolean isBillingDocsEnabled = true;
    private boolean isHomActivated = false;
	private boolean isDirectPaymentMaintenanceAllowed = true;
    
    // flags to steer the catalog behaviour
    private boolean isStandaloneCatalog = false;
    protected boolean showCatAddoLeaflet = false;
    protected boolean showCatATP = false;
    protected boolean showCatCompToSim = false;
    protected boolean showCatProdCateg = false;
    protected boolean showCatCUAButtons = false;
    protected boolean showCatExchProds = false;
    protected boolean showCatAVWLink = false;
    protected boolean showCatMultiSelect = false;
    protected boolean showCatContracts = false;
    protected boolean showCatPersRecommend = false;
	protected boolean showCatSpecialOffers = false;
    protected boolean showCatListCUALink = false;
    protected boolean showCatQuickSearchInCat = false;
	protected boolean showCatQuickSearchInNavigationBar = false;
	protected boolean catUseReloadOneTime = false;
	protected boolean suppCatEventCapturing = false;

	private boolean isNewsletterSubscriptionAvailable = false;

    private boolean isProductDeterminationInfoDisplayed = false;

 // properties for channel commerce hub
    private boolean isShowPartnerOnLineItem;
    private boolean isPartnerAvailabilityInfoAvailable;
    private boolean isOciBasketForwarding;
    private boolean isPartnerLocatorAvailable;
    private boolean isBatchAvailable;

    private boolean isPartnerBasket;

    private boolean isOnlyValidDocumentAllowed = true;

    private boolean isLeadCreationAllowed = false;

	private boolean isBackorderSelectionAllowed = false;


    private String  backend = BACKEND_CRM;

	//properties for the auction scenario
	private boolean isEAuctionUsed;
	private String auctionQuoteType;
	private String auctionOrderType;
	private String auctionPriceType;
	
    //properties for OCI
    private boolean isOciAllowed = false;
    private String   externalCatalogURL;
    private boolean isInternalCatalogAvailable = true;
    private boolean isProductInfoFromExternalCatalogAvailable = false;
    private boolean isAllProductInMaterialMaster = false;
    
    //properties for campaigns
	private boolean isManualCampaignEntryAllowed = false;
	private boolean isMultipleCampaignsAllowed = false;
    private boolean isEnrollmentAllowed = false;
    private String textTypeForCampDescr = "";
     
    
    //properties for loyalty management
    private boolean isEnabledLoyaltyProgram = false;
    private String loyaltyProgramID;
    private String pointCode;
    private String pointCodeDescr = "";
    private TechKey loyaltyProgramGUID;
    private String redemptionOrderType; 
    private String buyPtsOrderType; 
    private String loyProgDescr = null;  
    
    private ResultData processTypes;
    private ResultData processTypesForDisplay;
    private ResultData quotationProcessTypes;
    private ResultData quotationProcessTypesForDisplay;
    private String templateProcessType;
    private boolean isOrderGroupSelected;
    private boolean isQuotationGroupSelected;
	private ResultData condPurposesTypes;
    private Map successorProcTypeAllowedTableMap = new HashMap();
    
    // properties for Partner Locator from Rel. 5.0
    private String partnerLocatorBusinessPartnerId;
    private String relationshipGroup;
	//properties for exchange products
	private String exchProfileGroup;
    
    /**
     * maximum no of items a document can contain before it is considered
     * a large document and only am subset of the items are read from the
     * backend.
     */
    private int largeDocNoOfItemsThreshold = INFINITE_NO_OF_ITEMS; 
    
    // auctionBasketHelper used in backends
    private AuctionBasketHelperData auctionBasketHelper;
    

	/**
	 * Sets the document type that is used for procedure
	 * determination in basket when late decision is active.
	 * 
	 * @param arg the document type
	 */
	public void setLateDecisionProcType(String arg){
		lateDecisionProcType = arg;
	}

	/**
	 * Returns the document type that is used for procedure
	 * determination in basket when late decision is active.
	 * 
	 * @return the document type
	 */
	public String getLateDecisionProcType(){
		return lateDecisionProcType;
	}
	
		    
    /**
     * Set the property addressFormat
     *
     * @param addressFormat
     *
     */
    public void setAddressFormat(int addressFormat) {
        this.addressFormat = addressFormat;
    }


    /**
     * Returns the property addressFormat
     *
     * @return addressFormat
     *
     */
    public int getAddressFormat() {
       return addressFormat;
    }


    /**
     * Returns the property application.
     * The application describes the web application which is used
     * used  to realize the given scenario.
     *
     * @return application use constants B2B and B2C
     *
     */
    public String getApplication() {
    	return application;
    }


    /**
     * Set the property application
     * The application describes the web application which is used
     * used  to realize the given scenario.
     *
     * @param application  use constants B2B and B2C
     *
     */
    public void setApplication(String application) {
    		this.application = application;
    }


	/**
	 * Return the marketing attribute set id for the maintenance of the 
	 * user profile. <br>
	 * 
	 * @return marketing attribute set id
	 */
	public TechKey getAttributeSetId() {
		return attributeSetId;
	}

	/**
	 * Return the marketing attribute set id for the maintenance of the 
	 * user profile. <br>
	 * 
	 * @param key
	 */
	public void setAttributeSetId(TechKey attributeSetId) {
		this.attributeSetId = attributeSetId;
	}

    /**
     * Sets the property backend
     *
     * @param backend
     *
     */
    public void setBackend(String backend) {
       this. backend = backend;
    }


    /**
     * Returns the property backend
     *
     * @return backend
     *
     */
    public String getBackend() {
       return backend;
    }

	/**
	 * Return if the selection of backorders is possible. <br>
	 * 
	 * @return if backorders could selected
	 */
	public boolean isBackorderSelectionAllowed() {
		return isBackorderSelectionAllowed;
	}


	/**
	 * Return if the selection of backorders is possible. <br>
	 * 
	 * @param isBackorderSelectionAllowed flag if backorders could selected
	 */
	public void setBackorderSelectionAllowed(boolean isBackorderSelectionAllowed) {
		this.isBackorderSelectionAllowed = isBackorderSelectionAllowed;
	}


    /**
     * Set the property isBestsellerAvailable
     *
     * @param isBestsellerAvailable
     */
    public void setBestsellerAvailable(boolean isBestsellerAvailable) {
        this.isBestsellerAvailable = isBestsellerAvailable;
    }

    /**
     * Returns the property isBestsellerAvailable
     *
     * @return isBestsellerAvailable
     */
    public boolean isBestsellerAvailable() {
        return isBestsellerAvailable;
    }


	/**
	 * Returns if  the batches are available.
	 * @return boolean
	 */
	public boolean isBatchAvailable() {
		return isBatchAvailable;
	}


	/**
	 * Sets if the batches are available.
	 * @param batchAvailable A flag, if batches are supported
	 */
	public void setBatchAvailable(boolean batchAvailable) {
		this.isBatchAvailable = batchAvailable;
	}


    /**
     * Sets the property isBillingDocsEnabled
     *
     * @param isBillingDocsEnabled
     */
    public void setBillingDocsEnabled (boolean isBillingDocsEnabled) {
        this.isBillingDocsEnabled = isBillingDocsEnabled;
    }

    /**
     * @see package com.sap.isa.backend.boi.isacore.ShopData#setBillingSelection
     */
    public void setBillingSelection(ShopData.BillingSelect bs) {
        this.billingSelection = bs;
        if (bs instanceof ShopData.BillingSelectNODOC) {
            this.isBillingDocsEnabled = false;
        }
    }
    /**
     * Returns true if no Billing documents should be diplayed in the shop.
     * @return boolean
     */
    public boolean isBillingSelectionNoDoc() {
        return (billingSelection == null  || 
                billingSelection instanceof ShopData.BillingSelectNODOC);
    }
    /**
     * Returns true if R3 Billing documents should be diplayed in the shop.
     * @return boolean
     */
    public boolean isBillingSelectionR3() {
        return (billingSelection instanceof ShopData.BillingSelectR3);
    }
    /** 
     * Returns true if CRM Billing documents should be diplayed in the shop.
     * @return boolean
     */
    public boolean isBillingSelectionCRM() {
        return (billingSelection instanceof ShopData.BillingSelectCRM);
    }
    /** 
     * Returns true if R3 and CRM Billing documents should be diplayed in the shop.
     * @return boolean
     */
    public boolean isBillingSelectionR3CRM() {
        return (billingSelection instanceof ShopData.BillingSelectR3CRM);
    }

    /**
     * Returns the property isBillingDocsEnabled
     *
     * @return isBillingDocsEnabled
     */
    public boolean isBillingDocsEnabled() {
        return isBillingDocsEnabled;
    }


	/**
	 * Return the contact person could also handle for other partner in 
	 * the business partner hierarchy. <br>
	 * 
	 * @return
	 */
	public boolean isBpHierarchyEnable() {
		return isBpHierarchyEnable;
	}


	/**
	 * Set the switch, if the contact person could also handle for other partner
	 * in the business partner hierarchy.. <br>
	 * 
	 * @param bpHierarchyEnable
	 */
	public void setBpHierarchyEnable(boolean bpHierarchyEnable) {
		isBpHierarchyEnable = bpHierarchyEnable;
	}


    /**
     * Set the property catalog
     * <b>Don't use this method, use readOrgData instead <br>
     *    This method is only needed in the backend!</b>
     *
     * @param catalog
     */
    public void setCatalog(String catalog) {
        this.catalog = catalog;
    }


    /**
     * Returns the property catalog
     *
     * @return catalog
     */
    public String getCatalog() {
       return catalog;
    }


    /**
     * Set the property catalogDeterminationActived
     *
     * @param catalogDeterminationActived
     */
    public void setCatalogDeterminationActived(boolean catalogDeterminationActived) {
        this.isCatalogDeterminationActived = catalogDeterminationActived;
    }


    /**
     * Returns the property catalogDeterminationActived
     *
     * @return catalogDeterminationActived
     */
    public boolean isCatalogDeterminationActived() {
        return isCatalogDeterminationActived;
    }
    
    /**
     * Returns if the Check Profile should be set.
     * @return boolean
     */
    public boolean isCheckProfile() {
        return getApplication().equals(ShopData.B2B);
    }

    /**
     * Set the partner function which is used for the company.
     *
     * Use the constants from the <code>PartnerFunctionData</code> to do this.<br>
     *
     * @param companyPartnerFunction
     *
     * @see com.sap.isa.businesspartner.backend.boi.PartnerFunctionData
     *
     */
    public void setCompanyPartnerFunction(String companyPartnerFunction) {
        this.companyPartnerFunction = companyPartnerFunction;
    }


    /**
     * Returns the partner function for the company.
     * Therfore the constants from the <code>PartnerFunctionData</code> are used.
     *
     * @return companyPartnerFunction
     *
     * @see com.sap.isa.businesspartner.backend.boi.PartnerFunctionData
     *
     */
    public String getCompanyPartnerFunction() {
       return this.companyPartnerFunction;
    }



    /**
     * Set the property contractAllowed
     *
     * @param contractAllowed
     */
    public void setContractAllowed(boolean contractAllowed) {
        this.isContractAllowed = contractAllowed;
    }


    /**
     * Returns the property contractAllowed
     *
     * @return contractAllowed
     */
    public boolean isContractAllowed() {
       return isContractAllowed;
    }


	/**
	 * Returns the switch <i>contractInCatalogAllowed</i>. <br>
	 * The switch controls, if the contract information should be displayed
	 * in the catalog. 
	 * 	 
	 * @return <code>true</code> if the contract information should be displayed 
	 * in the catalog
	 */
	public boolean isContractInCatalogAllowed() {
		return isContractInCatalogAllowed;
	}


	/**
	 * Set the switch contractAllowed. <br>
	 * The switch controls, if the contract information should be displayed
	 * in the catalog. 
	 * 
	 * @param isContractInCatalogAllowed
	 */
	public void setContractInCatalogAllowed(boolean isContractInCatalogAllowed) {
		this.isContractInCatalogAllowed = isContractInCatalogAllowed;
	}


	/**
	 * Returns the switch isContractNegotiationAllowed.
	 * 
	 * @return boolean
	 */
	public boolean isContractNegotiationAllowed() {
		return isContractNegotiationAllowed;
	}


	/**
	 * Sets the isContractNegotiationAllowed.
	 * @param contractNegotiationAllowed The contractNegotiationAllowed to set
	 */
	public void setContractNegotiationAllowed(boolean contractNegotiationAllowed) {
		this.isContractNegotiationAllowed = contractNegotiationAllowed;
	}

    /**
     * Sets to campaign key valid for this shop (entry). Campaign key is used
     * for pricing purposes.
     */
    public void setCampaignKey(String campaignKey) {
    	this.campaignKey = campaignKey;
    }

	/**
	 * Returns the campaign key valid for this shop (entry). Campaign key is
	 * used for pricing purposes.
	 */
	public String getCampaignKey() {
		return this.campaignKey;
	}

	/**
	 * Returns the contractNegotiationProcessTypes.
	 * @return contractNegotiationProcessTypes  the <code>ResultData</code>
	 *  contractNegotiationProcessTypes has tree entries
     *          <ul>
     *          <li>ID id of the process type </li>
     *          <li>DESCRIPTION description of the process type </li>
     *          <li>CONTRACT_TYPE allowed contract type </li>
     *          </ul>
     *          <i>The name of columns are defined as constants in ShopData class </i>
	 *
	 * @see	com.sap.isa.core.util.table.ResultData
	 */
	public ResultData getContractNegotiationProcessTypes() {
		return contractNegotiationProcessTypes;
	}

	/**
	 * Reads the contractNegotiationProcessTypes from the backend.
	 * @return contractNegotiationProcessTypes  the <code>ResultData</code>
	 *  contractNegotiationProcessTypes has tree entries
     *          <ul>
     *          <li>ID id of the process type </li>
     *          <li>DESCRIPTION description of the process type </li>
     *          <li>CONTRACT_TYPE allowed contract type </li>
     *          </ul>
     *          <i>The name of columns are defined as constants in ShopData class </i>
	 *
	 * @see	com.sap.isa.core.util.table.ResultData
	 */
	public void readContractNegotiationProcessTypes(String soldto)
				throws CommunicationException {
		final String METHOD = "readContractNegotiationProcessTypes()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("Soldto ="+soldto);
        try {
             getBackendService().readContractNegotiationProcessTypes(this, soldto);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally {
        	log.exiting();
        }
    }


	/**
	 * Sets the contractNegotiationProcessTypes.
	 * @param contractNegotiationProcessTypes The contractNegotiationProcessTypes to set
	 */
	public void setContractNegotiationProcessTypes(ResultData contractNegotiationProcessTypes) {
		this.contractNegotiationProcessTypes = contractNegotiationProcessTypes;
	}


    /**
     * Set the property country
     *
     * @param country
     */
    public void setCountry(String country) {
        this.country = country;
        numberFormatChanged = true;
    }


    /**
     * Returns the property country
     *
     * @return country
     */
    public String getCountry() {
        return country;
    }


    /**
     * Create the property countryList
     *
     * @param countryTable
     */
    public void createCountryList(Table countryTable) {
		final String METHOD = "createCountryList()";
		log.entering(METHOD);
        this.countryList = new ResultData(countryTable);

        countryList.beforeFirst();

        while (countryList.next()) {
            countryDescriptions.put(countryList.getString("ID"),
                                    countryList.getString("DESCRIPTION"));
        }

        countryList.beforeFirst();
        log.exiting();
    }


    /**
     * Returns the property countryList
     *
     * @return countryList the <code>ResultData countryList</code> has tree entries
     *         <ul>
     *         <li>ID id of the region </li>
     *         <li>DESCRIPTION description of the country </li>
     *         <li>REGION_FLAG flag if the region is necessary for adresses in this country </li>
     *         </ul>
     *         <i>The name of columns are defined as constants in ShopData class </i>
     *
     */
    public ResultData getCountryList() {
        return countryList;
    }


    /**
     * Find the country corresponding to the given id. The search is performed
     * on the country list of the shop.
     *
     * @param id id of country to be searched
     * @return description of country, i.e. country's name
     */
    public String getCountryDescription(String id) {
        return (String) countryDescriptions.get(id);
    }

    /**
     * Set the property isCuaAvailable
     *
     * @param isCuaAvailable
     */
    public void setCuaAvailable(boolean isCuaAvailable) {
        this.isCuaAvailable = isCuaAvailable;
    }


    /**
     * Returns the property isCuaAvailable
     *
     * @return isCuaAvailable
     */
    public boolean isCuaAvailable() {
        return isCuaAvailable;
    }



    /**
     * Set the property dateFormat
     *
     * @param dateFormat
     */
    public void setDateFormat(String dateFormat) {
        this.dateFormat = dateFormat;
    }


    /**
     * Returns the property dateFormat
     *
     * @return dateFormat
     */
    public String getDateFormat() {
        return dateFormat;
    }


    /**
     * Set the property decimalPointFormat
     *
     * @param decimalPointFormat
     */
    public void setDecimalPointFormat(String decimalPointFormat) {
        this.decimalPointFormat = decimalPointFormat;
        numberFormatChanged = true;
    }


    /**
     * Returns the property decimalPointFormat
     *
     * @return decimalPointFormat
     */
    public String getDecimalPointFormat() {
        return decimalPointFormat;
    }


    /**
     * Set the property decimalSeparator
     *
     * @param decimalSeparator
     */
    public void setDecimalSeparator(String decimalSeparator) {
        this.decimalSeparator = decimalSeparator;
        numberFormatChanged = true;
    }


    /**
     * Returns the property decimalSeparator
     *
     * @return decimalSeparator
     */
    public String getDecimalSeparator() {
        return decimalSeparator;
    }


    /**
     * Set the property defaultCountry
     *
     * @param defaultCountry
     */
    public void setDefaultCountry(String defaultCountry) {
        this.defaultCountry = defaultCountry;
    }


    /**
     * Returns the property defaultCountry
     *
     * @return defaultCountry
     */
    public String getDefaultCountry() {
        return defaultCountry;
    }



    /**
     * Set the property docTypeLateDecision.
     * If <code>true</code> the user can decide which document type he want to
     * use after the creation of adocument.
     *
     * @param docTypeLateDecision
     *
     */
    public void setDocTypeLateDecision(boolean docTypeLateDecision) {
        this.isDocTypeLateDecision = docTypeLateDecision;
    }


    /**
     * Returns the property docTypeLateDecision
     * If <code>true</code> the user can decide which document type he want to
     * use after the creation of adocument.
     *
     * @return docTypeLateDecision
     *
     */
    public boolean isDocTypeLateDecision() {
       return this.isDocTypeLateDecision;
    }



    /**
     * Set the property currency
     *
     * @param currency
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }


    /**
     * Returns the property currency
     *
     * @return currency
     */
    public String getCurrency() {
        return currency;
    }


    /**
     * Set the property description
     *
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }


    /**
     * Returns the property description
     *
     * @return description
     */
    public String getDescription() {
       return description;
    }


     /**
     * Set the property distributionChannel
     *
     * @param distributionChannel
     */
    public void setDistributionChannel(String distributionChannel) {
        this.distributionChannel = distributionChannel;
    }


    /**
     * Returns the property distributionChannel
     *
     * @return distributionChannel
     */
    public String getDistributionChannel() {
       return distributionChannel;
    }



    /**
     * Set the property division
     *
     * @param division
     */
    public void setDivision(String division) {
        this.division = division;
    }


    /**
     * Returns the property division
     *
     * @return division
     *
     */
    public String getDivision() {
       return division;
    }



    /**
     * Set the property eAuctionUsed
     *
     * @param eAuctionUsed
     *
     */
    public void setEAuctionUsed(boolean eAuctionUsed) {
        this.isEAuctionUsed = eAuctionUsed;
    }


    /**
     * Returns the property eAuctionUsed
     *
     * @return eAuctionUsed
     *
     */
    public boolean isEAuctionUsed() {
       return isEAuctionUsed;
    }


	/**
	 * Getter for the quotation process type 
	 * @return Quotation Process type
	 */
	public String getAuctionQuoteType(){
		return auctionQuoteType;
	}
	
	
	/**
	 * Setter for the auction quotation process type
	 * @param The quotation proces type
	 */
	public void setAuctionQuoteType(String type){
		auctionQuoteType = type;
	}
	
	/**
	 * Getter for the sales order process type 
	 * @return sales order Process type
	 */
	public String getAuctionOrderType(){
		return auctionOrderType;
	}
	
	
	/**
	 * Setter for the auction sales order process type
	 * @param The sales order proces type
	 */
	public void setAuctionOrderType(String type){
		auctionOrderType = type;
	}

	/**
	 * Getter for auction winning price condition type 
	 * @return price condition type
	 */
	public String getAuctionPriceType(){
		return auctionPriceType;	
	}
	
	/**
	 * Setter for the auction price condition type
	 * @param The auction price condition type
	 */
	public void setAuctionPriceType(String type){
		auctionPriceType = type;
	}


    /**
     * Set the property eventCapturingActived
     *
     * @param eventCapturingActived
     */
    public void setEventCapturingActived(boolean eventCapturingActived) {
        this.isEventCapturingActived = eventCapturingActived;
    }


    /**
     * Returns the property eventCapturingActived
     *
     * @return eventCapturingActived
     */
    public boolean isEventCapturingActived() {
        return isEventCapturingActived;
    }


	/**
	 * Returns the ociBasketForwarding.
	 * @return boolean
	 */
	public boolean isOciBasketForwarding() {
		return isOciBasketForwarding;
	}

	/**
	 * Sets the ociBasketForwarding.
	 * @param ociBasketForwarding The ociBasketForwarding to set
	 */
	public void setOciBasketForwarding(boolean ociBasketForwarding) {
		isOciBasketForwarding = ociBasketForwarding;
	}


    /**
     * Set the property orderingOfNonCatalogProductsAllowed
     *
     * @param orderingOfNonCatalogProductsAllowed
     */
    public void setOrderingOfNonCatalogProductsAllowed(boolean orderingOfNonCatalogProductsAllowed) {
        this.isOrderingOfNonCatalogProductsAllowed = orderingOfNonCatalogProductsAllowed;
    }


    /**
     * Returns the property orderingOfNonCatalogProductsAllowed
     *
     * @return orderingOfNonCatalogProductsAllowed
     */
    public boolean isOrderingOfNonCatalogProductsAllowed() {
        return isOrderingOfNonCatalogProductsAllowed;
    }


	/**
	 * Returns the partnerAvailabilityInfoAvailable.
	 * @return boolean
	 */
	public boolean isPartnerAvailabilityInfoAvailable() {
		return isPartnerAvailabilityInfoAvailable;
	}

	/**
	 * Sets the partnerAvailabilityInfoAvailable.
	 * @param partnerAvailabilityInfoAvailable The partnerAvailabilityInfoAvailable to set
	 */
	public void setPartnerAvailabilityInfoAvailable(boolean partnerAvailabilityInfoAvailable) {
		isPartnerAvailabilityInfoAvailable =
			partnerAvailabilityInfoAvailable;
	}


    /**
     * Set the property groupingSeparator
     *
     * @param groupingSeparator
     */
    public void setGroupingSeparator(String groupingSeparator) {
        this.groupingSeparator = groupingSeparator;
        numberFormatChanged = true;
    }


    /**
     * Returns the property groupingSeparator
     *
     * @return groupingSeparator
     */
    public String getGroupingSeparator() {
       return groupingSeparator;
    }


	/**
	 * Return if the Hosted Order Management is actived.
	 *
	 * @return boolean
	 */
	public boolean isHomActivated() {
		return isHomActivated;
	}

	/**
	 * Activate or deactivate the Hosted Order Management.
	 *
	 * @param homActivated Flag to activate or deactivate the hosted order management
	 */
	public void setHomActivated(boolean homActivated) {
		this.isHomActivated = homActivated;
	}



    /**
     * Set the property id
     *
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }


    /**
     * Returns the property id
     *
     * @return id
     */
    public String getId() {
       return id;
    }

    /**
     * Returns if the marketing functions are allowed also for unknown users .
     * @return boolean
     */
    public boolean isMarketingForUnknownUserAllowed() {
        return isMarketingForUnknownUserAllowed;
    }

    /**
     * Sets, if the marketing functions are allowed also for unknown users .
     * @param marketingForUnknownUserAllowed flag.
     */
    public void setMarketingForUnknownUserAllowed(boolean marketingForUnknownUserAllowed) {
        isMarketingForUnknownUserAllowed = marketingForUnknownUserAllowed;
    }


    /**
     * Set the property language
     *
     * @param language
     */
    public void setLanguage(String language) {
        this.language = language;
    }


    /**
     * Returns the property language
     *
     * @return language
     */
    public String getLanguage() {
        return language;
    }


	/**
	 * Set the switch for the usages of IPC prices. <br>
	 *
	 * @param listPriceUsed 
	 */
	public void setIPCPriceUsed(boolean iPCPriceUsed) {
		isIPCPriceUsed = iPCPriceUsed;
	}


	/**
	 * Return the switch for the usages of IPC prices. <br>
	 *
	 * @return listPriceUsed
	 */
	public boolean isIPCPriceUsed() {
		return isIPCPriceUsed;
	}


    /**
     * Set the switch for the usages of list prices. <br>
     *
     * @param listPriceUsed 
     */
    public void setListPriceUsed(boolean listPriceUsed) {
        isListPriceUsed = listPriceUsed;
    }


    /**
     * Return  the switch for the usages of list prices. <br>
     *
     * @return listPriceUsed
     */
    public boolean isListPriceUsed() {
        return isListPriceUsed;
    }
    
    /**
     * Set the switch for the usages of no prices. <br>
     *
     * @param noPriceUsed 
     */
    public void setNoPriceUsed(boolean noPriceUsed) {
        isNoPriceUsed = noPriceUsed;
    }


    /**
     * Return  the switch for the usages of no prices. <br>
     *
     * @return noPriceUsed
     */
    public boolean isNoPriceUsed() {
        return isNoPriceUsed;
    }


     /**
     * Returns the property isOrderSimulateAvailable
     *
     * @return isOrderSimulateAvailable
     */
    public boolean isOrderSimulateAvailable() {
        return isOrderSimulateAvailable;
    }


    /**
     * Sets the property isOrderSimulateAvailable
     *
     * @param isOrderSimulateAvailable
     */
    public void setOrderSimulateAvailable(boolean isOrderSimulateAvailable) {
        this.isOrderSimulateAvailable = isOrderSimulateAvailable;
    }



	/**
	 * Returns the property numberFormat
	 *
	 * @return numberFormat
	 *
	 */
	public Date parseDate(String date) {
		final String METHOD = "parseDate()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("date="+date);
		if (date == null  ||  date.length() == 0) {
			log.exiting();
			return null;
		}

		char separator = '.';

		if (dateFormat.equals(DATE_FORMAT2) || dateFormat.equals(DATE_FORMAT5)) {
			separator = '/';
		}
	    else if (dateFormat.equals(DATE_FORMAT3) || dateFormat.equals(DATE_FORMAT6)) {
			separator = '-';
		}

		int firstOccurance = date.indexOf(separator);

		int secondOccurance = date.indexOf(separator,firstOccurance + 1);

		int end = date.indexOf(' ',secondOccurance + 1);

		String day = "", month = "", year = "";

		String part1 = "", part2 = "", part3 = "";

		part1 = date.substring(0,firstOccurance);
		part2 = date.substring(firstOccurance+1,secondOccurance);
		if (end > 0) {
			part3 = date.substring(secondOccurance+1,end);
		}
		else {
			part3 = date.substring(secondOccurance+1);
		}

		if (dateFormat.equals(DATE_FORMAT1)) {
			day = part1;
			month = part2;
			year = part3;
		}
		else if (dateFormat.equals(DATE_FORMAT2) || dateFormat.equals(DATE_FORMAT5)) {
			month = part1;
			day = part2;
			year = part3;
		}
		else {
			year = part1;
			month = part2;
			day = part3;
		}


		GregorianCalendar calendar
			= new GregorianCalendar(Integer.parseInt(year)-1900,
			                        Integer.parseInt(month)-1,
			                        Integer.parseInt(day));
		log.exiting();
		return calendar.getTime();

	}



    /**
     * Returns the property numberFormat
     *
     * @return numberFormat
     *
     */
    public NumberFormat getNumberFormat() {

       if (numberFormatChanged) {
        numberFormatChanged = false;
        Locale locale = new Locale("",country);
        DecimalFormat decimalFormat = (DecimalFormat) NumberFormat.getInstance(locale);

        DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols(locale);
        decimalFormatSymbols.setDecimalSeparator(decimalSeparator.charAt(0));
        decimalFormatSymbols.setGroupingSeparator(groupingSeparator.charAt(0));

        decimalFormat.setDecimalFormatSymbols(decimalFormatSymbols);

        numberFormat = decimalFormat;
       }

       return this.numberFormat;
    }


	/**
	 * Returns the partnerBasket flag.
	 * This flag describes, if the basket will be create for
	 *        one or more channel partner (Soldfrom)
	 *
	 * @return if the basket will be create for
	 *        one or more channel partner (Soldfrom)
	 */
	public boolean isPartnerBasket() {
		return isPartnerBasket;
	}

	/**
	 * Sets the partnerBasket flag.
	 * This flag describes, if the basket will be create for
	 *        one or more channel partner (Soldfrom)
	 *
	 * @param partnerBasket Flag if the basket will be create for
	 *        one or more channel partner (Soldfrom)
	 */
	public void setPartnerBasket(boolean partnerBasket) {
		this.isPartnerBasket = partnerBasket;
	}



	/**
	 * Returns if the partner locator is available.
	 *
	 * @return boolean
	 * @see com.sap.isa.dealerlocator.businessobject.DealerLocator
	 */
	public boolean isPartnerLocatorAvailable() {
		return isPartnerLocatorAvailable;
	}


	/**
	 * Sets the flag, if the partner locator is available
	 * @param isPartnerLocatorAvailable Flag, if the partner locator is available
	 * @see com.sap.isa.dealerlocator.businessobject.DealerLocator
	 */
	public void setPartnerLocatorAvailable(boolean partnerLocatorAvailable) {
		this.isPartnerLocatorAvailable = partnerLocatorAvailable;
	}


    /**
     * Set the property personalRecommendationAvailable
     *
     * @param personalRecommendationAvailable
     *
     */
    public void setPersonalRecommendationAvailable(boolean personalRecommendationAvailable) {
        this.isPersonalRecommendationAvailable = personalRecommendationAvailable;
    }


    /**
     * Returns the property personalRecommendationAvailable
     *
     * @return personalRecommendationAvailable
     *
     */
    public boolean isPersonalRecommendationAvailable() {
       return this.isPersonalRecommendationAvailable;
    }


    /**
     * Set the property pricingCondsAvailable
     *
     * @param pricingCondsAvailable
     *
     */
    public void setPricingCondsAvailable(boolean pricingCondsAvailable) {
        this.isPricingCondsAvailable = pricingCondsAvailable;
    }


    /**
     * Returns the property pricingCondsAvailable
     *
     * @return pricingCondsAvailable
     *
     */
    public boolean isPricingCondsAvailable() {
       return this.isPricingCondsAvailable;
    }


    /**
     * Set the property processType
     *
     * @param processType
     */
    public void setProcessType(String processType) {
        this.processType = processType;
    }

    /**
     * Returns the property processType
     *
     * @return processType
     */
    public String getProcessType() {
        return processType;
    }


    /**
     * Returns the isProductDeterminationInfoDisplayed.
     * @return boolean
     */
    public boolean isProductDeterminationInfoDisplayed() {
        return isProductDeterminationInfoDisplayed;
    }

    /**
     * Sets the isProductDeterminationInfoDisplayed.
     * @param isProductDeterminationInfoDisplayed The isProductDeterminationInfoDisplayed to set
     */
    public void setProductDeterminationInfoDisplayed(boolean productDeterminationInfoDisplayed) {
        this.isProductDeterminationInfoDisplayed = productDeterminationInfoDisplayed;
    }


    /**
     * Set the property quotationAllowed
     *
     * @param quotationAllowed
     *
     */
    public void setQuotationAllowed(boolean quotationAllowed) {
        this.isQuotationAllowed = quotationAllowed;
    }


    /**
     * Returns the property quotationAllowed
     *
     * @return quotationAllowed
     */
    public boolean isQuotationAllowed() {
        return isQuotationAllowed;
    }


    public boolean isQuotationOrderingAllowed() {
		return isQuotationOrderingAllowed;
	}

	public void setQuotationOrderingAllowed(boolean isQuotationOrderingAllowed) {
		this.isQuotationOrderingAllowed = isQuotationOrderingAllowed;
	}

    /**
     * Set the property isQuotationExtended
     *
     * @param isQuotationExtended
     */
    public void setQuotationExtended(boolean isQuotationExtended) {
        this.isQuotationExtended = isQuotationExtended;
    }


    /**
     * Returns the property isQuotationExtended
     *
     * @return isQuotationExtended
     */
    public boolean isQuotationExtended() {
        return isQuotationExtended;
    }


    /**
     * Set the property isQuotationSearchAllTypesRequested
     *
     * @param isQuotationSearchAllTypesRequested
     */
    public void setQuotationSearchAllTypesRequested(
                    boolean isQuotationSearchAllTypesRequested) {
        this.isQuotationSearchAllTypesRequested =
            isQuotationSearchAllTypesRequested;
    }


    /**
     * Returns the property isQuotationSearchAllTypesRequested
     *
     * @return isQuotationSearchAllTypesRequested
     *
     */
    public boolean isQuotationSearchAllTypesRequested() {
        return isQuotationSearchAllTypesRequested;
    }


    /**
     * Set the processType for quotations
     *
     * @param processType
     */
    public void setQuotationProcessType(String processType) {
        this.quotationProcessType = processType;
    }


    /**
     * Returns processType for quotations
     *
     * @return processType
     */
    public String getQuotationProcessType() {
        return quotationProcessType;
    }


    /**
     * Set the property isRecommendationAvailable
     *
     * @param isRecommendationAvailable
     */
    public void setRecommendationAvailable(boolean isRecommendationAvailable) {
        this.isRecommendationAvailable = isRecommendationAvailable;
    }


    /**
     * Returns the property isRecommendationAvailable
     *
     * @return isRecommendationAvailable
     */
    public boolean isRecommendationAvailable() {
        return isRecommendationAvailable;
    }



    /**
     * Set the property responsibleSalesOrganisation
     *
     * @param responsibleSalesOrganisation
     */
    public void setResponsibleSalesOrganisation(String responsibleSalesOrganisation) {
        this.responsibleSalesOrganisation = responsibleSalesOrganisation;
    }


    /**
     * add the regionTable for the country <code>country</code> to regionTableMap
     *
     * @param regionTable
     * @param country to which the regions belong
     */
    public void addRegionList(Table regionTable, String country) {
        regionTableMap.put(country,new ResultData(regionTable));
    }


    /**
     * Returns the regionList for a given country
     *
     * @param country country for which the region list should return
     *
     * @return regionList the <code>ResultData regionList</code> has two entries
     *          <ul>
     *          <li>ID id of the region </li>
     *          <li>DESCRIPTION description of the region </li>
     *          </ul>
     *          <i>The name of columns are defined as constants in ShopData class </i>
     */
    public ResultData getRegionList(String country)
            throws CommunicationException {
            	
				final String METHOD = "getRegionList()";
				log.entering(METHOD);
				if (log.isDebugEnabled())
					log.debug("country = "+country);
       
			   // Upport Note 835465
				ResultData regionList = null;
	    
				countryList.beforeFirst();
				while(countryList.next()) {
			
					if(countryList.getString("ID").equals(country)) {
						if ((((Boolean)(countryList.getObject("REGION_FLAG"))).booleanValue())) {		 	
							regionList = (ResultData)regionTableMap.get(country);
							if (regionList == null) {
								try {
									if (getBackendService().readRegionList(this, country)){
										regionList = (ResultData)regionTableMap.get(country);
									}
								}
								catch (BackendException ex) {
									BusinessObjectHelper.splitException(ex);
								}
							}
						}
					}
				}  /* endwile */
			         
				countryList.beforeFirst(); 
       
				log.exiting();
       
				return regionList;
    }


    /**
     * Returns the property responsibleSalesOrganisation
     *
     * @return responsibleSalesOrganisation
     */
    public String getResponsibleSalesOrganisation() {
        return responsibleSalesOrganisation;
    }


    /**
     * Set the property salesOffice
     *
     * @param salesOffice
     */
    public void setSalesOffice(String salesOffice) {
        this.salesOffice = salesOffice;
    }


    /**
     * Returns the property salesOffice
     *
     * @return salesOffice
     */
    public String getSalesOffice() {
        return salesOffice;
    }



    /**
     * Set the property salesOrganisation
     *
     * @param salesOrganisation
     */
    public void setSalesOrganisation(String salesOrganisation) {
        this.salesOrganisation = salesOrganisation;
    }


    /**
     * Returns the property salesOrganisation
     *
     * @return salesOrganisation
     */
    public String getSalesOrganisation() {
        return salesOrganisation;
    }


	/**
 	 * Returns the showPartnerOnLineItem.
	 * @return boolean
     */
	public boolean isShowPartnerOnLineItem() {
		return isShowPartnerOnLineItem;
	}


	/**
	 * Sets the showPartnerOnLineItem.
	 * @param showPartnerOnLineItem The showPartnerOnLineItem to set
	 */
	public void setShowPartnerOnLineItem(boolean showPartnerOnLineItem) {
		isShowPartnerOnLineItem = showPartnerOnLineItem;
	}


    /**
     * Set the property soldtoSelectable.
     * If <code>true</code> the soldto for the order can be selected within the
     * order pages.
     *
     * @param soldtoSelectable
     *
     */
    public void setSoldtoSelectable(boolean soldtoSelectable) {
        this.isSoldtoSelectable = soldtoSelectable;
    }

	/**
	 * If catalogue staging is used, to test catalogues, this
	 * date might be set as date to be used for IPC issues
	 * 
	 * @param catalogStagingIPCDate the catalogStagingIPCDate to use
	 */
	public void setCatalogStagingIPCDate(String catalogStagingIPCDate) {
		this.catalogStagingIPCDate = catalogStagingIPCDate;
	}

    /**
     * Returns the property soldtoSelectable
     * If <code>true</code> the soldto for the order can be selected within the
     * order pages.
     *
     * @return soldtoSelectable
     *
     */
    public boolean isSoldtoSelectable() {
       return this.isSoldtoSelectable;
    }


    /**
     * Returns the taxCodeList for a given country, region, zip-code and city
     *
     * @param country country for which the tax code list should return
     * @param region  region for which the tax code list should return
     * @param zipCode zip code for which the tax code list should return
     * @param city    city for which the tax code list should return
     *
     * @return taxCodeList the <code>ResultData taxCodeList</code>
     */
    public ResultData getTaxCodeList(String country,
                                     String region,
                                     String zipCode,
                                     String city)
            throws CommunicationException {
		final String METHOD = "getTaxCodeList()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("country = "+ country + ", region="+region+ ", zipCode = "+zipCode+ ", city="+city);
       TaxCodeKey key = new TaxCodeKey(country, region, zipCode, city);
       ResultData taxCodeList = (ResultData)taxCodeTableMap.get(key);
		try {
		
	       if (taxCodeList == null) {
	            try {
	                taxCodeList = getBackendService().readTaxCodeList(this,
	                                                                  country,
	                                                                  region,
	                                                                  zipCode,
	                                                                  city);
	                taxCodeTableMap.put(key, taxCodeList);
	            }
	            catch (BackendException ex) {
	                BusinessObjectHelper.splitException(ex);
	            }
	       }
		}finally {
			log.exiting();
		}

       return taxCodeList;
    }
    
	/**
	 * Set the property templateProcessType
	 *
	 * @param templateProcessType
	 */
	public void setTemplateProcessType(String templateProcessType) {
		this.templateProcessType = templateProcessType;
	}
	
	
	/**
	 * Returns the property templateProcessType
	 *
	 * @return templateProcessType
	 */
	public String getTemplateProcessType() {
		return templateProcessType;
	}
	

    /**
     * Set the property templateAllowed.
     * The property controls if order templates are allowed.
     *
     * @param templateAllowed
     *
     */
    public void setTemplateAllowed(boolean templateAllowed) {
        this.isTemplateAllowed = templateAllowed;
    }


    /**
     * Returns the property templateAllowed.
     * The property controls if order templates are allowed.
     *
     * @return templateAllowed
     *
     */
    public boolean isTemplateAllowed() {
       return this.isTemplateAllowed;
    }


    /**
     * Create the property titleList
     *
     * @param titleTable
     */
    public void createTitleList(Table titleTable) {
		final String METHOD = "createTitleList()";
		log.entering(METHOD);
        this.titleList = new ResultData(titleTable);

        // Get the titleTable entries that describe a person
        // and store them in a map for simple retrival of the
        // data
        // Reset the result set
        titleList.beforeFirst();

        while (titleList.next()) {
            if (titleList.getBoolean(ShopData.FOR_PERSON)) {
                personTitleKeys.put(
                    titleList.getString(ShopData.ID),
                    titleList.getString(ShopData.DESCRIPTION));
            }
        }

        // Reset the result set
        titleList.beforeFirst();
        log.exiting();
    }



    /**
     * Returns the property titleList
     *
     ** @return titleList  the <code>ResultData titleList</code> has four entries
     *          <ul>
     *          <li>ID id of the region </li>
     *          <li>DESCRIPTION description of the country </li>
     *          <li>FOR_PERSON title is used for persons </li>
     *          <li>FOR_ORGANISATION title is used for organisation </li>
     *          </ul>
     *          <i>The name of columns are defined as constants in ShopData class </i>
     */
    public ResultData getTitleList() {
        return titleList;
    }

    /**
     * Returns true if the provided titleKey is configured as a key
     * for a person.
     *
     * @param titleKey the key to be checked
     * @return <code>true</code> if the provided key is configured
     *         as a key for a person or <code>false</code> if not.
     */
    public boolean isTitleKeyForPerson(String titleKey) {
        return personTitleKeys.containsKey(titleKey);
    }


    /**
     * Set the property scenario
     *
     * @param scenario  use constants B2B and B2C
     */
    public void setScenario(String scenario) {
        this.scenario = scenario;
    }


    /**
     * Returns the property szenario
     *
     * @return scenario use constants B2B and B2C
     */
    public String getScenario() {
       return scenario;
    }

    /**
     * Sets the property 'isShowConfirmedDeliveryDateRequested'
     */
    public void setShowConfirmedDeliveryDateRequested(boolean availability_check) {
        this.isShowConfirmedDeliveryDateRequested = availability_check;
    }


    /**
     * Returns the property isShowConfirmedDeliveryRequested
     *
     * @return isShowConfirmedDeliveryDateRequested
     */
    public boolean isShowConfirmedDeliveryDateRequested() {
        return this.isShowConfirmedDeliveryDateRequested;
    }


    /**
     * Set the property views
     *
     * @param views
     */
    public void setViews(String[] views) {
        this.views = views;
    }


    /**
     * Returns the property views
     *
     * @return views
     */
    public String[] getViews() {
        return views;
    }


    /**
     * Set the property userProfileAvailable
     *
     * @param userProfileAvailable
     *
     */
    public void setUserProfileAvailable(boolean userProfileAvailable) {
        this.isUserProfileAvailable = userProfileAvailable;
    }


    /**
     * Returns the property userProfileAvailable
     *
     * @return userProfileAvailable
     *
     */
    public boolean isUserProfileAvailable() {
       return isUserProfileAvailable; 
    }

    /**
     * Set the property accessoriesAvailable
     *
     * @param accessoriesAvailable
     *
     */
     public void setAccessoriesAvailable(boolean accessoriesAvailable) {
        this.isAccessoriesAvailable = accessoriesAvailable;
    }

    /**
     * Returns the property accessoriesAvailable
     *
     * @return accessoriesAvailable
     *
     */
     public boolean isAccessoriesAvailable() {
        return isAccessoriesAvailable;
    }
    
	/**
	* Set the property remanufacturedAvailable
	*
	* @param remanufacturedAvailable
	*
	*/
	public void setRemanufactureAvailable(boolean remanufactureAvailable) {
		   this.isRemanufactureAvailable = remanufactureAvailable;
	 }
	   
	/**
	 * Returns the property remanufacturedAvailable
	 *
	 * @return remanufacturedAvailable
	 */
	public boolean isRemanufactureAvailable() {
		   return isRemanufactureAvailable;
	 }	  
	
	 /**
	   * Set the property remanufacturedAvailable
	   *
	   * @param remanufacturedAvailable
	   *
	   */
	 public void setNewPartAvailable(boolean NewPartAvailable) {
			 this.isNewPartAvailable = NewPartAvailable;
	   }
	   
	  /**
	  * Returns the property remanufacturedAvailable
	  *
	  * @return remanufacturedAvailable
	  */
	  public boolean isNewPartAvailable() {
			 return isNewPartAvailable;
	   }	
	   

	  
	   /**
		* set Condition Purpose group(Exchange Profile group from the shop Admin
		* This for the pricing attributes for exchange products
		*/
	   public void setCondPurpGroup(String exchProfileGroup)
	   {
		   this.exchProfileGroup = exchProfileGroup;
	   }
    
	   /**
		* Return the condition Purpose group
		*/

	   public String getCondPurpGroup()
	   {
		   return exchProfileGroup;
	   }
	   /**
		* Given the Condition purpose group(Exchange Profile group).
		* Reads the condition purposes associated with it.
		* returns the condition purposes & the description(semantics of it)
		* @param language
		* @throws CommunicationException
		*/
	   public void readCondPurpsTypes(String language)
				   throws CommunicationException {
		try {
				getBackendService().readConditionPurposes(this,language);
			}
	   catch (BackendException ex) {
			   BusinessObjectHelper.splitException(ex);
		   }
		}


	 /**
	  * Sets the CondPurposesTypes.
	  * @param CondPurposes
	  */
	  public void setCondPurpTypes(ResultData condPurposesTypes) {
	   this.condPurposesTypes = condPurposesTypes;
	  }
	
	  /**
	   * Gets the ConditionPurposesTable
	   */	

	 public ResultData getCondPurpTypes()
	  {
		  return condPurposesTypes;
	  }

    /**
     * Set the property crossSellingAvailable
     *
     * @param crossSellingAvailable
     *
     */
     public void setCrossSellingAvailable(boolean crossSellingAvailable) {
        this.isCrossSellingAvailable = crossSellingAvailable;
    }

    /**
     * Returns the property crossSellingAvailable
     *
     * @return crossSellingAvailable
     *
     */
     public boolean isCrossSellingAvailable() {
        return isCrossSellingAvailable;
    }


    /**
     * Set the property alternativeAvailable
     *
     * @param alternativeAvailable
     *
     */
     public void setAlternativeAvailable(boolean alternativeAvailable) {
        this.isAlternativeAvailable = alternativeAvailable;
    }

    /**
     * Returns the property alternativeAvailable
     *
     * @return alternativeAvailable
     *
     */
     public boolean isAlternativeAvailable() {
        return isAlternativeAvailable;
    }


    /**
     * Returns an iterator for the shop list. Method necessary because of
     * the <code>Iterable</code> interface.
     *
     * @return iterator to iterate over the shopList
     */
    public Iterator iterator() {
        return null;
    }


    /**
     * Sets the BackendObjectManager for the shop. This method is used
     * by the object manager to allow the user object interaction with
     * the backend logic. This method is normally not called
     * by classes other than BusinessObjectManager.
     *
     * @param bem BackendObjectManager to be used
     */
    public void setBackendObjectManager(BackendObjectManager bem) {
        this.bem = bem;
    }


    /* get the BackendService, if necessary */
    private ShopBackend getBackendService()
            throws BackendException {

        if (backendService == null) {
            // get the Backend from the Backend Manager
            backendService =
                    (ShopBackend) bem.createBackendBusinessObject(BackendTypeConstants.BO_TYPE_SHOP, null);
        }
        return backendService;
    }



    /**
     * Read the OrgData for a given catalog from the backend and set the
     * property catalog.
     *
     * @param catalog key for catalog which contains the OrgData
     */
    public void readOrgData(String catalog)
                throws CommunicationException {
		final String METHOD = "readOrgData()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("catalog="+catalog);
        try {
            this.catalog = catalog;
            getBackendService().readOrgData(this);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally {
        	log.exiting();
        }
    }


    /**
     * Read the pricing info for a given soldto from the backend.
     *
     * @param soldtoId Id for reading customer depend pricing infos
     */
    public PricingInfoData readPricingInfo(String soldtoId, boolean forBasket)  {
		final String METHOD = "readPricingInfo()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("soldtoId = "+soldtoId + ", forBasket="+forBasket);
       	PricingInfo pricingInfo;
       	String pricingInfoKey = soldtoId + forBasket; 
		if ( !pricingInfoMap.containsKey(pricingInfoKey) ) {
			
			pricingInfo = new PricingInfo();
			
			try {
				getBackendService().readPricingInfo(pricingInfo,this,soldtoId, forBasket);
				pricingInfoMap.put(pricingInfoKey, pricingInfo);
			}
			catch (BackendException ex) {
				return null;
			}
			finally {
				log.exiting();
			}
		}
		else {
			pricingInfo = (PricingInfo) pricingInfoMap.get(pricingInfoKey);
		}

        return pricingInfo;
    }


    /**
     * Read the pricing info from the backend.
     *
     */
    public PricingInfoData readPricingInfo(boolean forBasket)  {
        String soldtoId = null;
        return readPricingInfo(soldtoId, forBasket);
    }

    /**
     * Read the pricing info per Item - Sales Partners (SoldTo, Reseller / Distributor)
     * Relation from the backend.<br>
     * <p>
     * Each item must at least have a techkey and partnerlist filled.
     * </p>
     *
     * @param ItemList itemList for reading Item - Sales Partners depend pricing infos
     */
    public PricingInfoData readItemPricingInfo(ItemListData itemList)  {
		final String METHOD = "readItemPricingInfo()";
		log.entering(METHOD);
       PricingInfo pricingInfo = new PricingInfo();

        try {
            getBackendService().readItemPricingInfo(pricingInfo, this, itemList);
        }
        catch (BackendException ex) {
            return null;
        }
        finally {
        	log.exiting();
        }
        return pricingInfo;
    }


	/**
	 * Get the name of the traffic light image for the product availablity
	 *
	 * @param scheduleLines schedule lines
	 * @return name of traffic light image and message. If the schedule lines are emtpy
	 *         <code>TL_EMPTY</code> will be returned
	 */
	public String[] getTrafficLight(List scheduleLines) {
		final String METHOD = "getTrafficLight()";
		log.entering(METHOD);
        checkTrafficLight();

        if (scheduleLines.isEmpty()) {
        	log.exiting();
			return getTrafficLight(EV_NO_INFO);
		}

        Schedline schedline = (Schedline)scheduleLines.get(0);

        int days = date2Days(schedline.getCommittedDate());

        Iterator iter = availibilityIntervals.iterator();

        while (iter.hasNext()) {
			TrafficLightInterval interval = (TrafficLightInterval) iter.next();

            if (days <= interval.getLimit()) {
            	log.exiting();
                return interval.getTrafficLight();
            }
		}
		log.exiting();
        return getTrafficLight(EV_NO_INFO);
	}

    /**
     * Get the name of the traffic light image for the product availablity
     *
     * @param scheduleLines schedule lines
     * @param message info message for the value
     * @return name of traffic light image and message. If the schedule lines are emtpy
     *         <code>TL_EMPTY</code> will be returned
     */
    public String[] getTrafficLight(String event) {

        checkTrafficLight();

        TrafficLight trafficLight = (TrafficLight)availibiltyEvents.get(event);

        if (trafficLight != null) {
            return trafficLight.getTrafficLight();
        }

        return new String[] {"",""};

    }


	/**
	 * Get all possible "traffic lights".
	 * @return iterator over String[]: the first value in the StringArray get the name of the
     * of the traffic light image the second is an explantion for the image
	 */
    public Iterator getTrafficLights() {

        checkTrafficLight();

        ArrayList list = new ArrayList(10);

        Iterator iter = trafficLightList.iterator();

        while (iter.hasNext()) {
            list.add(((TrafficLight)iter.next()).getTrafficLight());
		}

        return list.iterator();
    }


	/**
     * Commit a business transaction.
     *
     * @throws CommunicationException
     */
    public void commitWork() throws CommunicationException {

        try {
            getBackendService().commitWork();
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }

	}


    /**
     * Roll a business transaction back.
     *
     * @throws CommunicationException
     */
    public void rollBack() throws CommunicationException {

        try {
            getBackendService().rollBack();
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }

    }


    /**
     * Returns a copy of this object.
     *
     * @return Copy of this object
     */
    public Object clone() {

    	Shop s = null;

    	try {

			s = (Shop) super.clone();

    	} catch (CloneNotSupportedException ex) {
			if (log.isDebugEnabled()) {
				log.debug("Error ", ex);
			}    		
    	}

    	return s;

    }


    // private method to get the traffic lights and fills the corresponding
    // structures
    private void checkTrafficLight() {

     	if (trafficLights == null) {
            try {
                trafficLights = new ResultData(getBackendService().readTrafficLights(this));
            }
            catch (BackendException ex) {
                return;
            }

        	List intervals = new ArrayList();
            availibilityIntervals.clear();
            availibiltyEvents.clear();
            trafficLightList.clear();

            // Iterate over resultData and fill the structures
            trafficLights.beforeFirst();

            while (trafficLights.next()) {

				String event = trafficLights.getString(TL_EVENT);
                TrafficLight trafficLight = null;

                if (event.equals("INTERVALL")) {
                    trafficLight =
                        new TrafficLightInterval(trafficLights.getString(TL_LIMIT),
                                                 trafficLights.getString(TL_ICON),
                                                 trafficLights.getString(TL_TEXT));

                    intervals.add(trafficLight);

                }
                else {
                    trafficLight =
                        new TrafficLight(trafficLights.getString(TL_ICON),
                                         trafficLights.getString(TL_TEXT));
                    availibiltyEvents.put(event,trafficLight);
                }

                if (!trafficLightList.contains(trafficLight)) {
                    trafficLightList.add(trafficLight);
                }
			}

			int size = intervals.size();

			// sort the intervalls, easiest method nxn
			for (int i = 0; i < size; i++) {

                int foundIndex = 0;
                TrafficLightInterval minValue = (TrafficLightInterval)intervals.get(0);
                for (int j = 1; j < intervals.size(); j++) {
                    TrafficLightInterval tmpValue = (TrafficLightInterval)intervals.get(j);
                    if (tmpValue.getLimit() < minValue.getLimit()) {
                        foundIndex = j;
                        minValue = tmpValue;
                    }
                }

                availibilityIntervals.add(minValue);
                intervals.remove(foundIndex);
            }
        }
    }


    // private method to get the traffic lights and fills the corresponding
    // structures
    private int date2Days(String date) {

        String days;
        try {
            days = getBackendService().getDaysFromDate(date);
        }
        catch (BackendException ex) {
            days = "0";
        }

        if (days.length() > 0) {
            return Integer.parseInt(days);
        }
        else {
            return 0;
        }

    }


    private class TrafficLight {

        private String icon;
        private String text;

        public TrafficLight(String icon,
                            String text){
            this.icon = icon;
            this.text = text;
        }

		/**
		 * Returns the icon.
		 * @return String
		 */
		public String getIcon() {
			return icon;
		}

		/**
		 * Returns the text.
		 * @return String
		 */
		public String getText() {
			return text;
		}

		/**
		 * Sets the icon.
		 * @param icon The icon to set
		 */
		public void setIcon(String icon) {
			this.icon = icon;
		}

		/**
		 * Sets the text.
		 * @param text The text to set
		 */
		public void setText(String text) {
			this.text = text;
		}


        public String[] getTrafficLight() {
            return new String[] {icon, text };
        }


        /**
         * @see java.lang.Object#equals(java.lang.Object)
         */
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            else if (obj == this) {
                return true;
            }
            else if (obj instanceof TrafficLight) {
                TrafficLight other = (TrafficLight)obj;

                return (other.icon.equals(icon)
                		&& other.text.equals(text));

            }
            return false;
        }
        
        public int hashCode() {
        	return super.hashCode();
        }

    }


    private class TrafficLightInterval extends TrafficLight {

        public TrafficLightInterval(String limit,
                                    String icon,
                                    String text){
            super(icon, text);
            setLimit(limit);
        }


        private int limit;


		/**
		 * Returns the limit.
		 * @return int
		 */
		public int getLimit() {
			return limit;
		}

		/**
		 * Sets the limit.
		 * @param limit The limit to set
		 */
		public void setLimit(String limit) {
			this.limit = Integer.parseInt(limit);
		}

    }

    /* inner class for taxCode Map */
    private class TaxCodeKey {

        private String country;
        private String region;
        private String zipCode;
        private String city;
        private int hashCode = 0;

        public TaxCodeKey (String country,
                           String region,
                           String zipCode,
                           String city){

            this.country = country;
            this.region = region;
            this.zipCode = zipCode;
            this.city = city;
        }


        /*
         *
         * overwrite the method of the object class
         *
         * @param obj the object which will be compare with the object
         * @return true if the objects are equal
         *
         */
        public boolean equals(Object obj) {

            if (obj == null) {
                return false;
            }
            else if (obj == this) {
                return true;
            }
            else if (obj instanceof TaxCodeKey) {
                TaxCodeKey otherKey = (TaxCodeKey)obj;

                int myHashCode    = hashCode();
                int otherHashCode = otherKey.hashCode();

                if (myHashCode != otherHashCode) {
                    return false;
                }
                else {
                    boolean result;

                    result = otherKey.country.equals(country);
                    result &= otherKey.region.equals(region);
                    result &= otherKey.zipCode.equals(zipCode);
                    result &= otherKey.city.equals(city);

                    return result;
                }
            }
            return false;
        }



        /*
         * Returns hashcode for the message.
         *
         * @return hashcode of the objects
         */
        public int hashCode( ) {

            if (hashCode == 0) {
                hashCode = country.hashCode() ^
                           region.hashCode() ^
                           zipCode.hashCode() ^
                           city.hashCode();
            }
            return hashCode;
        }


    }



	/**
	 * Sets the reseller partner function. Currently only
	 * used for R/3 backend.
	 * @param arg the R/3 partner function used for linking customers to resellers
	 */
	public void setResellerPartnerFunction(String arg){
		resellerPartnerFunction = arg;
	}

	/**
	 * Retrieves the reseller partner function. Currently only
	 * used for R/3 backend.	  
	 * @return the R/3 partner function used for linking customers to resellers
	 */
	public String getResellerPartnerFunction(){
		return resellerPartnerFunction;
	}


	/**
	 * Returns the onlyValidDocumentAllowed.
	 * @return boolean
	 */
	public boolean isOnlyValidDocumentAllowed() {
		return isOnlyValidDocumentAllowed;
	}

	/**
	 * Sets the onlyValidDocumentAllowed.
	 * @param onlyValidDocumentAllowed The onlyValidDocumentAllowed to set
	 */
	public void setOnlyValidDocumentAllowed(boolean onlyValidDocumentAllowed) {
		this.isOnlyValidDocumentAllowed = onlyValidDocumentAllowed;
	}

	/**
	 * Returns the isLeadCreationAllowed.
	 * @return boolean
	 */
	public boolean isLeadCreationAllowed() {
		return isLeadCreationAllowed;
	}

	/**
	 * Sets the isLeadCreationAllowed.
	 * @param leadCreationAllowed The isLeadCreationAllowed to set
	 */
	public void setLeadCreationAllowed(boolean leadCreationAllowed) {
		isLeadCreationAllowed = leadCreationAllowed;
	}

	/**
	 *	Sets the isOciAloowed.
	 *	@param ociAllowed
	 */
	public void setOciAllowed(boolean ociAllowed) {
		this.isOciAllowed = ociAllowed;
	}

	/**
	 * Returns the isOciAllowed (is OCI enabled in XCM)
	 * @return boolean
	 */
	public boolean isOciAllowed() {
		return isOciAllowed;
	}

	/**
	 * Sets the external catalog URL
	 * @param externalCatalogURL
	 */
	public void setExternalCatalogURL(String externalCatalogURL) {
		this.externalCatalogURL = externalCatalogURL;
	}

	/**
	 * Returns the URL of the external catalog
	 * @return String
	 */
	public String getExternalCatalogURL() {
		return externalCatalogURL;
	}

    /**
     * Returns the enteredProductIdTypes.
     *
     * @return HashMap
     */
    public HashMap getEnteredProductIdTypes() {
        return enteredProductIdTypes;
    }

    /**
     * Returns the substitutionReasons.
     *
     * @return HashMap
     */
    public HashMap getSubstitutionReasons() {
        return substitutionReasons;
    }

    /**
     * Sets the enteredProductIdTypes.
     *
     * @param enteredProductIdTypes The enteredProductIdTypes to set
     */
    public void setEnteredProductIdTypes(HashMap enteredProductIdTypes) {
        this.enteredProductIdTypes = enteredProductIdTypes;
    }

    /**
     * Sets the substitutionReasons.
     *
     * @param substitutionReasons The substitutionReasons to set
     */
    public void setSubstitutionReasons(HashMap substitutionReasons) {
        this.substitutionReasons = substitutionReasons;
    }

    /**
      * Seeks the entered product id type description for the given ennetred
      * product id type id.
      *
      * @param enteredProductIdTypeId the id to search for
      * @return String the product id type descripition for the the given id or
      * an empty String if not
      */
    public String getEnteredProductIdDesc(String enteredProductIdTypeId) {
        String productIdDesc = "";

        if (substitutionReasons != null) {
            productIdDesc = (String) enteredProductIdTypes.get(enteredProductIdTypeId);
        }
        if (productIdDesc == null) {
            productIdDesc = "";
        }

        return productIdDesc;
    }

    /**
     * Seeks the substitution Reasons description for the given substitution
     * reason id.
     *
     * @param substitutionReasonId the id to search for
     * @return String the substitution reason for the the given id or an empty
     * String if not
     */
    public String getSubstitutionReasonDesc(String substitutionReasonId) {
        String substDesc = "";

        if (substitutionReasons != null) {
            substDesc = (String) substitutionReasons.get(substitutionReasonId);
        }
        if (substDesc == null) {
            substDesc = "";
        }

        return substDesc;
    }

	/**
	 *	Sets the isInternalCatalogAvailable.
	 *	@param internalCatalogAvailable
	 */
	public void setInternalCatalogAvailable(boolean internalCatalogAvailable) {
		this.isInternalCatalogAvailable = internalCatalogAvailable;
	}

	/**
	 * Returns the isInternalCatalogAvailable
	 * @return boolean
	 */
	public boolean isInternalCatalogAvailable() {
		 return isInternalCatalogAvailable;
	}

	/**
	 * Return the marketing attribute set id for the newsletter subscription. <br>
	 * 
	 * @return marketing attribute set id
	 */
	public TechKey getNewsletterAttributeSetId() {
		return newsletterAttributeSetId;
	}

	/**
	 * Return the marketing attribute set id for the newsletter subscription. <br>
	 * 
	 * @param newsletterAttributeSetId
	 */
	public void setNewsletterAttributeSetId(TechKey newsletterAttributeSetId) {
		this.newsletterAttributeSetId = newsletterAttributeSetId;
	}

	/**
	 * Return if the newslettter subscription is available. <br>
	 * 
	 * @return
	 */
	public boolean isNewsletterSubscriptionAvailable() {
		return isNewsletterSubscriptionAvailable;
	}

	/**
	 * Return if the newslettter subscription is available. <br>
	 * 
	 * @param isNewsletterSubscriptionAvailable
	 */
	public void setNewsletterSubscriptionAvailable(boolean isNewsletterSubscriptionAvailable) {
		this.isNewsletterSubscriptionAvailable = isNewsletterSubscriptionAvailable;
	}


	/**
	 *	Sets the isProductInfoFromExternalCatalogAvailable.
	 *	@param productInfoFromExternalCatalogAvailable
	 */
	public void setProductInfoFromExternalCatalogAvailable(boolean productInfoFromExternalCatalogAvailable) {
		this.isProductInfoFromExternalCatalogAvailable = productInfoFromExternalCatalogAvailable;
	}

	/**
	 * Returns the isProductInfoFromExternalCatalogAvailable
	 * @return boolean
	 */
	public boolean isProductInfoFromExternalCatalogAvailable() {
		return isProductInfoFromExternalCatalogAvailable;
	}

	/**
	 *	Sets the isAllProductInMaterialMaster.
	 *	@param allProductInMaterialMaster
	 */
	public void setAllProductInMaterialMaster(boolean allProductInMaterialMaster) {
		this.isAllProductInMaterialMaster = allProductInMaterialMaster;
	}

	/**
	 * Returns the isAllProductInMaterialMaster
	 * @return boolean
	 */
	public boolean isAllProductInMaterialMaster() {
		return isAllProductInMaterialMaster;
	}



    /**
     * read the description text of the country with id <code>id</code>
     * from the backend
     *
     * @param id of the country for which the description should be determined
     * @return description of the country
     *
     */
    public String readCountryDescription(String id) {
		final String METHOD = "readCountryDescription()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("country id = "+id);
        try {
            return getBackendService().readCountryDescription(this,id);
        }
        catch (BackendException ex) {
            return null;
        }
		finally {
			log.exiting();
		}
    }


    /**
     * read the description text of the region with id <code>id</code>
     * from the backend
     *
     * @param id of the region for which the description should be determined
     * @return description of the region
     *
     */
    public String readRegionDescription(String regionId, String countryId) {
		final String METHOD = "readRegionDescription()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("regionid = "+regionId+ ", countryId="+countryId);
        try {
            return getBackendService().readRegionDescription(this,regionId, countryId);
        }
        catch (BackendException ex) {
            return null;
        }
		finally {
			log.exiting();
		}
    }


    /**
     * read the description text of the title with id <code>id</code>
     * from the backend
     *
     * @param id of the tilte for which the description should be determined
     * @return description of the title
     *
     */
    public String readTitleDescription(String id) {
		final String METHOD = "readTitleDescription()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("title id = "+id);
        try {
            return getBackendService().readTitleDescription(this,id);
        }
        catch (BackendException ex) {
            return null;
        }
		finally {
			log.exiting();
		}
    }

    /**
     * read the description text of the academic title with id <code>id</code>
     * from the backend
     *
     * @param id of the acadmic title for which the description should be determined
     * @return description of the acadeic title
     *
     */
    public String readAcademicTitleDescription(String id) {
		final String METHOD = "readAcademicTitleDescription()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("academic title id = "+id);
        try {
            return getBackendService().readAcademicTitleDescription(this,id);
        }
        catch (BackendException ex) {
            return null;
        }
        finally {
        	log.exiting();
        }
    }

	/* (non-Javadoc)
	 * @see com.sap.isa.backend.boi.isacore.ShopData#getLanguageIso()
	 */
	public String getLanguageIso() {
		return languageIso;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.backend.boi.isacore.ShopData#setIsoLanguage(java.lang.String)
	 */
	public void setLanguageIso(String languageIso) {
        this.languageIso = languageIso;
	}


	/**
	 * Returns the processTypes.
	 * @return processTypes
	 */
	public ResultData getProcessTypes() {
		return processTypes;
	}

	/**
	 * Returns the processTypes for display.
	 * @return processTypes
	 */
	public ResultData getProcessTypesForDisplay() {
		if (processTypesForDisplay == null)
			return processTypes;
		return processTypesForDisplay;
	}

	/**
	 * Reads the processTypes from the backend.
	 * @return processTypes  the <code>ResultData</code>
	 *  processTypes has tree entries
	 *          <ul>
	 *          <li>ID id of the process type </li>
	 *          <li>DESCRIPTION description of the process type </li>
	 *          <li>empty</li>
	 *          </ul>
	 *          <i>The name of columns are defined as constants in ShopData class </i>
	 *
	 * @see	com.sap.isa.core.util.table.ResultData
	 */
	public void readProcessTypes(String soldto)
				throws CommunicationException {
		final String METHOD = "readProcessTypes()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("soldto =" +soldto);
		 try {
			 getBackendService().readProcessTypes(this, soldto);
		}
		catch (BackendException ex) {
			BusinessObjectHelper.splitException(ex);
		}
		finally {
			log.exiting();
		}
	}


	/**
	 * Sets the processTypes.
	 * @param processTypes
	 */
	public void setProcessTypes(ResultData processTypes) {
		this.processTypes = processTypes;
	}


	/**
	 * Sets the processTypes for display.
	 * @param processTypes
	 */
	public void setProcessTypesForDisplay(ResultData processTypesForDisplay) {
		this.processTypesForDisplay = processTypesForDisplay;
	}


	/**
	 * Returns the quotationProcessTypes.
	 * @return quotationProcessTypes
	 */
	public ResultData getQuotationProcessTypes() {
		return quotationProcessTypes;
	}


	/**
	 * Returns the quotationProcessTypes.
	 * @return quotationProcessTypes
	 */
	public ResultData getQuotationProcessTypesForDisplay() {
		if (quotationProcessTypesForDisplay == null)
			return quotationProcessTypes;
		return quotationProcessTypesForDisplay;
	}


	/**
	 * Reads the quotationProcessTypes from the backend.
	 * @return quotationProcessTypes  the <code>ResultData</code>
	 *  processTypes has tree entries
	 *          <ul>
	 *          <li>ID id of the process type </li>
	 *          <li>DESCRIPTION description of the process type </li>
	 *          <li>empty</li><br>
	 *          </ul>
	 *          <i>The name of columns are defined as constants in ShopData class </i>
	 *
	 * @see	com.sap.isa.core.util.table.ResultData
	 */
	public void readQuotationProcessTypes(String soldto)
				throws CommunicationException {
		final String METHOD = "readQuotationProcessTypes()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("soldto =" +soldto);
		try {
			 getBackendService().readQuotationProcessTypes(this, soldto);
		}
		catch (BackendException ex) {
			BusinessObjectHelper.splitException(ex);
		}
		finally {
			log.exiting();
		}
	}

	/**
	 * Sets the quotationsProcessTypes.
	 * @param quotationProcessTypes
	 */
	public void setQuotationProcessTypes(ResultData quotationProcessTypes) {
		this.quotationProcessTypes = quotationProcessTypes;
	}

	/**
	 * Sets the quotationsProcessTypes.
	 * @param quotationProcessTypes
	 */
	public void setQuotationProcessTypesForDisplay(ResultData quotationProcessTypes) {
		this.quotationProcessTypesForDisplay = quotationProcessTypes;
	}

	/**
	 * Returns the property orderGroupSelected
	 * @return boolean isOrderGroupSelected
	 */
	public boolean isOrderGroupSelected() {
		return isOrderGroupSelected;
	}

	/**
	 * Sets the property orderGroupSelected
	 * @param isOrderGroupSelected
	 */
	public void setOrderGroupSelected(boolean isOrderGroupSelected) {
		this.isOrderGroupSelected = isOrderGroupSelected;
	}

	/**
	 * Returns the property quotationGroupSelected
	 * @return boolean isQuotationGroupSelected
	 */
	public boolean isQuotationGroupSelected() {
		return isQuotationGroupSelected;
	}

	/**
	 * Sets the property quotationGroupSelected
	 * @param isQuotationGroupSelected
	 */
	public void setQuotationGroupSelected(boolean isQuotationGroupSelected) {
		this.isQuotationGroupSelected = isQuotationGroupSelected;
	}


	/**
	 * Add the sucProcTypesTable for the predProcType to successorProcTypeTableMap
	 * @param sucProcTypesTable
	 * @param predProcType
	 */
	public void addSucProcTypeAllowedList(Table sucProcTypeAllowedTable, String predProcType) {
		successorProcTypeAllowedTableMap.put(predProcType,
											 new ResultData(sucProcTypeAllowedTable)); 
	}

	/**
	 * Returns the regionList for a given country
	 *
	 * @param country country for which the region list should return
	 *
	 * @return regionList the <code>ResultData regionList</code> has two entries
	 *          <ul>
	 *          <li>ID id of the region </li>
	 *          <li>DESCRIPTION description of the region </li>
	 *          </ul>
	 *          <i>The name of columns are defined as constants in ShopData class </i>
	 */
	/**
	 * Returns the sucProcTypeAllowedList for a given predProcType
	 * 
	 * @param predProcType
	 * @return sucProcTypeAllowedList the ResultData sucProcTypeAllowedList has two entries
	 *          <ul>
	 *          <li>PROCESS TYPE of the successor process type </li>
	 *          <li>DESCRIPTION description of the successor process type </li>
	 *          </ul>
	 *          <i>The name of columns are defined as constants in ShopData class </i>
	 * 
	 * @throws CommunicationException
	 */
	public ResultData getSucProcTypeAllowedList(String predProcType)
			throws CommunicationException {
		final String METHOD = "getSucProcTypeAllowedList()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("prodProcType =" +predProcType);
		ResultData sucProcTypeAllowedList =
					 (ResultData) successorProcTypeAllowedTableMap.get(predProcType);
		try {			 
		
			if (sucProcTypeAllowedList == null) {
				
					try {
						getBackendService().readSuccessorProcessTypesAllowed(this);
					}
					catch (BackendException ex) {
						BusinessObjectHelper.splitException(ex);
					}
					
					sucProcTypeAllowedList = 
						(ResultData) successorProcTypeAllowedTableMap.get(predProcType);
			}
		 
		}finally {
			log.exiting();
		}	
		return sucProcTypeAllowedList;	
	}
	
	/**
	 * Returns the maximum no of items a document can contain before it is
     * considered a large document and only a subset of the items are read 
     * from the backend.
     * 
	 * @return threshold for the no of items, from which a doucment 
	 *         is treated as a large doucment
	 *         INFINITE_NO_OF_ITEMS if no threshold is set
	 */
	public int getLargeDocNoOfItemsThreshold() {
		return largeDocNoOfItemsThreshold;
	}

	/**
	 * Sets the maximum no of items a document can contain before it is
     * considered to be large document and only a subset of the items are read 
     * from the backend.
     * 
	 * @param largeDocNoOfItemsThreshold threshold for the no of items, from
	 *        which  a doucment is treated as a large doucment. Should be set 
	 *        to INFINITE_NO_OF_ITEMS, no threshold is wanted 
	 */
	public void setLargeDocNoOfItemsThreshold(int largeDocNoOfItemsThreshold) {
		this.largeDocNoOfItemsThreshold = largeDocNoOfItemsThreshold;
	}
	
	/**
	 * Returns the property manualCampainEntryAllowed
	 *
	 * @return manualCampainEntryAllowed
	 *
	 */
	 public boolean isManualCampainEntryAllowed() {
//         return false;
	 	return isManualCampaignEntryAllowed;
	 }
	
	/**
	 * Set the property manualCampainEntryAllowed
	 *
	 * @param manualCampainEntryAllowed
	 *
	 */
	 public void setManualCampainEntryAllowed(boolean manualCampainEntryAllowed) {
	 	this.isManualCampaignEntryAllowed = manualCampainEntryAllowed; 	
	 }

	/**
	 * Returns the property partnerLocatorBusinessPartnerId
	 * 
	 * @return partnerLocatorBusinessPartnerId
	 */
	public String getPartnerLocatorBusinessPartnerId() {
		return partnerLocatorBusinessPartnerId;
	}

	/**
	 * Set the property partnerLocatorBusinessPartnerId
	 * 
	 * @param partnerLocatorBusinessPartnerId
	 */
	public void setPartnerLocatorBusinessPartnerId(String partnerLocatorBusinessPartnerId) {
		this.partnerLocatorBusinessPartnerId = partnerLocatorBusinessPartnerId;
	}

	/**
	 * Returns the property relationshipGroup
	 * 
	 * 
	 * @return relationshipGroup
	 */
	public String getRelationshipGroup() {
		return relationshipGroup;
	}

	/**
	 * Set the property relationshipGroup
	 * 
	 * @param relationshipGroup
	 */
	public void setRelationshipGroup(String relationshipGroup) {
		this.relationshipGroup = relationshipGroup;
	}

   /**
    * Set the property selectionProcesstYpe
    * 
    * @param selectionProcessType
    */
   public void setSelectionProcessType(String selectionProcessType) {
       this.selectionProcessType = selectionProcessType;
   }
   
   /**
    * Return the property selectionProcessType
    * 
    * @return selectionProcessType
    */
   public String getSelectionProcessType() {
       return selectionProcessType;
   }

   /**
	* If catalogue staging is used, to test catalogues, this
	* date might be set as date to be used for IPC issues
	* 
	* @return String the catalogStagingIPCDate
	*/
   public String getCatalogStagingIPCDate() {
	   return catalogStagingIPCDate;
   }
   
   /**
	* Set the property saveBasketAllowed.
	* The property controls if saving of basket is allowed.
	*
	* @param templateAllowed
	*
	*/
   public void setSaveBasketAllowed(boolean saveBasketAllowed) {
	   this.isSaveBasketAllowed = saveBasketAllowed;
   }


   /**
	* Returns the property saveBasketAllowed.
	* The property controls if saving of basket is allowed.
	*
	* @return saveBasketAllowed
	*
	*/
   public boolean isSaveBasketAllowed() {
	  return this.isSaveBasketAllowed;
   }

   /**
    * Currently this String is hardcoded, but in future it has to be read from the shop!
    */
   public String getIdTypeForVerification() {
   	  	
   	  if (idTypeForVerification == null || idTypeForVerification.length()==0) {
   	  	idTypeForVerification = "IST001"; 
   	  }
   	  	
	  return idTypeForVerification;
   }

  public void setIdTypeForVerification(String idType) {
	idTypeForVerification = idType;
  }   

    /**
     * @return shop parameter for payer
     * implemented for CRM51 TELCO scenario
     * payer is used in order, if the payment method COD was chosen by the
     * user for items with usage scope delivery
     * in the standard scenario the deviating payer will be used for the 
     * complete order
     */
   public String getPayer() {
      return payer;
   }

   /**
    * @param shopPayer
    * sets the payer id, entered by the user in shop administration tool
    * implemented for CRM51 TELCO scenario
    * payer is used in order, if the payment method COD was chosen by the
    * user for items with usage scope delivery
    * in the standard scenario the deviating payer will be used for the 
    * complete order 
    */
    public void setPayer(String shopPayer) {
        this.payer = shopPayer;
    } 
    
   /**
    * @return boolean parameter, which indicates if the payment information entered in the 
    * Web Shop checkout process has to be added to the BP master data;
    * The Web Shop parameter should be flagged in the case Business Agreements are used
    * In TELCO case the parameter is initially flagged and isn't changeable.
    */
	public boolean isPaymAtBpEnabled() {
		return isPaymAtBpEnabled;
	}

   /**
    * sets the parameter, which indicates if the payment information entered in the 
    * Web Shop checkout process has to be added to the BP master data;
    * In TELCO case the parameter is initially flagged and isn't changeable.     
    */
	public void setPaymAtBpEnabled(boolean b) {
		isPaymAtBpEnabled = b;
	}

   /**
    * @return boolean parameter, which indicates if the web user must confirm, that the entered 
    * bank credentials can be used by the shop owner to deduct the costs from users bank account
    * (country dependant law settings)
    */
	public boolean isPayMask4CollAuthEnabled() {
		return isPayMask4CollAuthEnabled;
	}

   /**
    * sets the boolean parameter, which indicates if the web user must confirm, that the entered 
    * bank credentials can be used by the shop owner to deduct the costs from users bank account
    * (country dependant law settings)
    */
	public void setPayMask4CollAuthEnabled(boolean b) {
		isPayMask4CollAuthEnabled = b;
	}

	/**
	 * Returns the flag, if multpile campaign entries are allowed on header and item level. 
	 * This flag also steers, if multiple camapigns, that were found by campaign determination,
	 * are automatically assigend to an item.
	 * 
	 * @return true if multple campaign entries are allowed and multiple campaigns found 
	 *              through campaign determination should automatically assigend to an 
	 *              item of a sales document, false else
	 */
	public boolean isMultipleCampaignsAllowed() {
		return isMultipleCampaignsAllowed;
	}

	/**
	 * Sets the flag, if multple campaigns found through campaign determination 
	 * should automatically  assigend to an item of a sales document
	 * 
	 * @param boolean are multple campaign entries allowed and should multiple campaigns 
	 *                found through campaign determination should automatically assigend 
	 *                to an item of a sales document, false else
	 */
	public void setMultipleCampaignsAllowed(boolean isMultipleCampaignsAllowed) {
		this.isMultipleCampaignsAllowed = isMultipleCampaignsAllowed;
	}

    /**
     * Returns the isEnrollmentAllowed
     * @return boolean
     */
    public boolean isEnrollmentAllowed() {
        return this.isEnrollmentAllowed;
    }

    /**
     * Sets the setIsEnrollmentAllowed
     * @param isEnrollmentAllowed The isEnrollmentAllowed to set
     */
    public void setIsEnrollmentAllowed(boolean isEnrollmentAllowed) {
        this.isEnrollmentAllowed = isEnrollmentAllowed;
    }

    /**
     * Returns the textTypeForCampDescr
     * @return text type as String
     */
    public String getTextTypeForCampDescr() {
        return this.textTypeForCampDescr;
    }

    /**
     * Sets the textTypeForCampDescr
     * @param textType The textTypeForCampDescr to set
     */
    public void setTextTypeForCampDescr(String textType) {
        this.textTypeForCampDescr = textType;
    }
    
    /**
     * Gets the auctionBasketHelper
     * @return AuctionBasketHelperData The auctionBasketHelper to set
     */
    public AuctionBasketHelperData getAuctionBasketHelper() {
        return auctionBasketHelper;
    }
     
    /**
     * Sets the auctionBasketHelper
     * @param AuctionBasketHelperData The auctionBasketHelper to set
     */
    public void setAuctionBasketHelper(AuctionBasketHelperData auctionBasketHelper) {
        this.auctionBasketHelper = auctionBasketHelper;
    }
    
    /**
     * Returns the attribute name for the backend config attribute
     * @return  the attribute name for the backend config attribute
     */
     public String getBackendConfigAttribName() {
         final String METHOD = "getBackendConfigAttribName()";
         log.entering(METHOD);
         
         String configAttrName = "";

         try {
             configAttrName = getBackendService().getBackendConfigAttribName();

         }
         catch (BackendException ex) {
             try {
                 BusinessObjectHelper.splitException(ex);
             }
             catch (Exception e) {
                 log.error(e.getMessage());
                 log.error(e.getStackTrace().toString());
             }
         }
         
         log.exiting();
         return configAttrName;
           
     }
     
     /**
      * is loyalty program enabled
      * @return  boolean
      */     
    public boolean isEnabledLoyaltyProgram() {
		return isEnabledLoyaltyProgram;
	}

	/**
	 * enables loyalty program
	 * @param boolean
	 */
	public void setEnabledLoyaltyProgram(boolean isEnabledLoyaltyProgram) {
		this.isEnabledLoyaltyProgram = isEnabledLoyaltyProgram;
	}

	   /**
     * Returns loyalty program ID
     * @return  loyalty program ID
     */	
	public String getLoyaltyProgramID() {
		return loyaltyProgramID;
	}

	/**
	 * Sets loyalty program ID
	 * @param loyalty program ID
	 */
	public void setLoyaltyProgramID(String loyaltyProgramID) {
		if (loyaltyProgramID != null) 
			this.loyaltyProgramID = loyaltyProgramID;
	}

	   /**
     * Returns loyalty program GUID
     * @return  loyalty program GUID
     */	
	public TechKey getLoyaltyProgramGUID() {
		return loyaltyProgramGUID;
	}

	/**
	 * Sets loyalty program GUID
	 * @param loyalty program GUID
	 */
	public void setLoyaltyProgramGUID(String loyaltyProgramGUID) {
		if (loyaltyProgramGUID != null) 
			this.loyaltyProgramGUID = new TechKey(loyaltyProgramGUID);
	}	
		
    /**
     * Returns point code description
     * @return point code description
     */	
 	public String getPointCodeDescription() {
		return pointCodeDescr;
	}

	/**
	 * Sets point code description
	 * @param point code description
	 */
	public void setPointCodeDescription(String pointCodeDescr) {
		if (pointCodeDescr != null)
			this.pointCodeDescr = pointCodeDescr;
	}
 
	   /**
     * Returns point code
     * @return point code
     */	
 	public String getPointCode() {
		return pointCode;
	}

	/**
	 * Sets point code
	 * @param point code
	 */
	public void setPointCode(String pointCode) {
		if (pointCode != null)
			this.pointCode = pointCode;
	}	
	
	 /**
     * Returns redemption order type for loyalty
     * @return redemption order type
     */		
    public String getRedemptionOrderType() {
		return redemptionOrderType;
	}
    
	/**
	 * Sets redemption order type for loyalty
	 * @param redemption order type
	 */
	public void setRedemptionOrderType(String redemptionOrderType) {
		this.redemptionOrderType = redemptionOrderType;
	}

    /**
    * Returns buy points order type for loyalty
    * @return buy points order type
    */      
   public String getBuyPtsOrderType() {
       return this.buyPtsOrderType;
   }
    
   /**
    * Sets buy points order type for loyalty
    * @param buy points order type
    */
   public void setBuyPtsOrderType(String buyPtsOrderType) {
       this.buyPtsOrderType = buyPtsOrderType;
   }

	/**
     * Gets the isStandaloneCatlog flag, used to indicate, if the 
     * catalog is run in standalone mode
     * 
     * @return true if the catalog is run in standalone mode
     *         false else
     */
    public boolean isStandaloneCatalog() {
        return isStandaloneCatalog;
    }

    /**
     * Sets the isStandaloneCatlog flag, used to indicate, if the 
     * catalog is run in standalone mode
     * 
     * @param isStandaloneCatalog 
     */
    public void setStandaloneCatalog(boolean isStandaloneCatalog) {
        this.isStandaloneCatalog = isStandaloneCatalog;
    }
    
    /**
     * Gets the flag, used to indicate, if contract data should 
     * be displayed in the catalog
     * 
     * @return true   if contract data should be displayed in the catalog,
     *         false  otherwise
     */
    public boolean showCatContracts() {
        return showCatContracts;
    }
    
    /**
     * Sets the flag, used to indicate, if contract data should 
     * be displayed in the catalog
     * 
     * @param true   if contract data should be displayed in the catalog,
     *         false  otherwise
     */
    public void setShowCatContracts(boolean showCatContracts) {
        this.showCatContracts = showCatContracts;
    }
    
    /**
     * Gets the flag, used to indicate, if multi select buttons should 
     * be displayed in the catalog
     * 
     * @return true   if multi select buttons should be displayed in the catalog,
     *         false  otherwise
     */
    public boolean showCatMultiSelect() {
        return showCatMultiSelect;
    }
    
    /**
     * Sets the flag, used to indicate, if multi select buttons should 
     * be displayed in the catalog
     * 
     * @param true   if multi select buttons should be displayed in the catalog,
     *         false  otherwise
     */
    public void setShowCatMultiSelect(boolean showCatMultiSelect) {
        this.showCatMultiSelect = showCatMultiSelect;
    }
    
    /**
     * Gets the flag, used to indicate, if the AVW link should 
     * be displayed in the catalog
     * 
     * @return true   if AVW link should be displayed in the catalog,
     *         false  otherwise
     */
    public boolean showCatAVWLink() {
        return showCatAVWLink;
    }
    
    /**
     * Sets the flag, used to indicate, if the AVW link should 
     * be displayed in the catalog
     * 
     * @param true   if AVW link should be displayed in the catalog,
     *         false  otherwise
     */
    public void setShowCatAVWLink(boolean showCatAVWLink) {
        this.showCatAVWLink = showCatAVWLink;
    }
    
    /**
     * Gets the flag, used to indicate, if Exchange products should 
     * be displayed in the catalog
     * 
     * @return true   if Exchange products should be displayed in the catalog,
     *         false  otherwise
     */
    public boolean showCatExchProds() {
        return showCatExchProds;
    }
    
    /**
     * Sets the flag, used to indicate, if Exchange products should 
     * be displayed in the catalog
     * 
     * @param true   if Exchange products should be displayed in the catalog,
     *         false  otherwise
     */
    public void setShowCatExchProds(boolean showCatExchProds) {
        this.showCatExchProds = showCatExchProds;
    }
    
    /**
     * Gets the flag, used to indicate, if CUA Buttons should 
     * be displayed in the catalog
     * 
     * @return true   if CUA Buttons should be displayed in the catalog,
     *         false  otherwise
     */
    public boolean showCatCUAButtons() {
        return showCatCUAButtons;
    }
    
    /**
     * Sets the flag, used to indicate, if CUA Buttons should 
     * be displayed in the catalog
     * 
     * @param true   if CUA Buttons should be displayed in the catalog,
     *         false  otherwise
     */
    public void setShowCatCUAButtons(boolean showCatCUAButtons) {
        this.showCatCUAButtons = showCatCUAButtons;
    }
    
    /**
     * Gets the flag, used to indicate, if product category and hierarchy
     * data should be displayed in the catalog
     * 
     * @return true   if product category and hierarchy data should be displayed in the catalog,
     *         false  otherwise
     */
    public boolean showCatProdCateg() {
        return showCatProdCateg;
    }
    
    /**
     * Sets the flag, used to indicate, if product category and hierarchy
     * data should be displayed in the catalog
     * 
     * @param true   if product category and hierarchy data should be displayed in the catalog,
     *         false  otherwise
     */
    public void setShowCatProdCateg(boolean showCatProdCateg) {
        this.showCatProdCateg = showCatProdCateg;
    }
    
    /**
     * Gets the flag, used to indicate, if Compare to Similar link should 
     * be displayed in the catalog
     * 
     * @return true   if Compare to Similar link should be displayed in the catalog,
     *         false  otherwise
     */
    public boolean showCatCompToSim() {
        return showCatCompToSim;
    }
    
    /**
     * Sets the flag, used to indicate, if Compare to Similar link should 
     * be displayed in the catalog
     * 
     * @param true   if Compare to Similar link should be displayed in the catalog,
     *         false  otherwise
     */
    public void setShowCatCompToSim(boolean showCatCompToSim) {
        this.showCatCompToSim = showCatCompToSim;
    }
    
    /**
     * Gets the flag, used to indicate, if ATP infos should 
     * be displayed in the catalog
     * 
     * @return true   if ATP infos should be displayed in the catalog,
     *         false  otherwise
     */
    public boolean showCatATP() {
        return showCatATP;
    }
    
    /**
     * Sets the flag, used to indicate, if ATP infos should 
     * be displayed in the catalog
     * 
     * @param true   if ATP infos should be displayed in the catalog,
     *         false  otherwise
     */
    public void setShowCatATP(boolean showCatATP) {
        this.showCatATP = showCatATP;
    }
    
    /**
     * Gets the flag, used to indicate, if add to leaflet should 
     * be displayed in the catalog
     * 
     * @return true   if ATP infos should be displayed in the catalog,
     *         false  otherwise
     */
    public boolean showCatAddoLeaflet() {
        return showCatAddoLeaflet;
    }
    
    /**
     * Sets the flag, used to indicate, if add to leaflet should 
     * be displayed in the catalog
     * 
     * @param true   if ATP infos should be displayed in the catalog,
     *         false  otherwise
     */
    public void setShowCatAddoLeaflet(boolean showCatAddoLeaflet) {
        this.showCatAddoLeaflet = showCatAddoLeaflet;
    }
    
    /**
     * Gets the flag, used to indicate, if personal recommendations should 
     * be displayed in the catalog
     * 
     * @return true   if ATP infos should be displayed in the catalog,
     *         false  otherwise
     */
    public boolean showCatPersRecommend() {
        return showCatPersRecommend;
    }
    
    /**
     * Sets the flag, used to indicate, if add personal recommendations should 
     * be displayed in the catalog
     * 
     * @param true   if ATP infos should be displayed in the catalog,
     *         false  otherwise
     */
    public void setShowCatPersRecommend(boolean showCatPersRecommend) {
        this.showCatPersRecommend = showCatPersRecommend;
    }
 
	/**
	 * Gets the flag, used to indicate, if special offers should 
	 * be displayed in the catalog
	 * 
	 * @return true   if special offers infos should be displayed in the catalog,
	 *         false  otherwise
	 */
	public boolean showCatSpecialOffers() {
		return showCatSpecialOffers;
	}
    
	/**
	 * Sets the flag, used to indicate, if special offers should 
	 * be displayed in the catalog
	 * 
	 * @param true   if special offers infos should be displayed in the catalog,
	 *         false  otherwise
	 */
	public void setShowCatSpecialOffers(boolean showCatSpecialOffers) {
	  this.showCatSpecialOffers = showCatSpecialOffers;
	}   
	
    
    /**
     * Gets the flag, used to indicate, if CUA links should 
     * be displayed in the catalog list
     * 
     * @return true   if ATP infos should be displayed in the catalog,
     *         false  otherwise
     */
    public boolean showCatListCUALink() {
        return showCatListCUALink;
    }
    
    /**
     * Sets the flag, used to indicate, if aCUA links should 
     * be displayed in the catalog list
     * 
     * @param true   if ATP infos should be displayed in the catalog,
     *         false  otherwise
     */
    public void setShowCatListCUALink(boolean showCatListCUALink) {
        this.showCatListCUALink = showCatListCUALink;
    }
    
    /**
     * Gets the flag, used to indicate, if the Quick search should be shown
     * in the categories display area
     * 
     * @return true   if ATP infos should be displayed in the catalog,
     *         false  otherwise
     */
    public boolean showCatQuickSearchInCat() {
        return showCatQuickSearchInCat;
    }
    
    /**
     * Sets the flag, used to indicate, if the Quick search should be shown
     * in the categories display area
     * 
     * @param true   if ATP infos should be displayed in the catalog,
     *         false  otherwise
     */
    public void setShowCatQuickSearchInCat(boolean showCatQuickSearchInCat) {
        this.showCatQuickSearchInCat = showCatQuickSearchInCat;
    }
    
	/**
	 * Gets the flag, used to indicate, if the Quick search should be shown
	 * in the navigation bar
	 * 
	 * @return true   if quick search should be displayed in the catalog,
	 *         false  otherwise
	 */
	public boolean showCatQuickSearchInNavigationBar(){
		return showCatQuickSearchInNavigationBar;
	}
    
	/**
	 * Sets the flag, used to indicate, if the Quick search should be shown
	 * in the navigation bar
	 * 
	 * @param true   if quick search should be displayed in the catalog,
	 *         false  otherwise
	 */
	public void setShowCatQuickSearchInNavigationBar(boolean showCatQuickSearchInNavigationBar){
		this.showCatQuickSearchInNavigationBar = showCatQuickSearchInNavigationBar;
	}
        
    /**
     * Gets the flag, used to indicate, if the catalog should use the
     * ReloadOneTimeAction
     * 
     * @return true   if the catalog should use the ReloadOneTimeAction
     *         false  otherwise
     */
    public boolean catUseReloadOneTime() {
        return catUseReloadOneTime;
    }
    
    /**
     * Sets the flag, used to indicate, if the catalog should use the
     * ReloadOneTimeAction
     * 
     * @param true   if the catalog should use the ReloadOneTimeAction
     *         false  otherwise
     */
    public void setCatUseReloadOneTime(boolean catUseReloadOneTime) {
        this.catUseReloadOneTime = catUseReloadOneTime;
    }

	public boolean isDirectPaymentMaintenanceAllowed() {
		return isDirectPaymentMaintenanceAllowed;
	}

	public void setDirectPaymentMaintenanceAllowed(
			boolean isDirectPaymentMaintenanceAllowed) {
		this.isDirectPaymentMaintenanceAllowed = isDirectPaymentMaintenanceAllowed;
	}

    /**
     * Returns the loyalty program description
     * 
     * @return loyProgDescr the description of the loyalty program
     */
    public String getLoyProgDescr() {
        return this.loyProgDescr;
    }

    /**
     * Sets the loyalty program description
     * 
     * @param loyProgDescr as String
     */
    public void setLoyProgDescr(String newDescr) {
        this.loyProgDescr = newDescr;
    }

    /**
     * Returns the flag, to indicate if Event Capturing 
     * should be supported by the catalogue
     * 
     * @return True if Event Capturing should be supported by the catalogue
     *         false else
     */
    public boolean isSuppCatEventCapturing() {
        return suppCatEventCapturing;
    }

    /**
     * Sets the flag, to indicate if Event Capturing 
     * should be supported by the catalogue
     * 
     * @param suppCatEventCapturing the flag value
     */
    public void setSuppCatEventCapturing(boolean suppCatEventCapturing) {
        this.suppCatEventCapturing = suppCatEventCapturing;
    }

}
