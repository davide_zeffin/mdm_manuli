/*****************************************************************************
    Class:        ServiceTypesSearchCommand
    Copyright (c) 2002, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author        Ralf Witt

    $Revision: #2 $
    $DateTime: 2003/10/01 16:07:42 $ (Last changed)
    $Change: 151875 $ (changelist)

*****************************************************************************/

package com.sap.isa.businessobject;

import com.sap.isa.backend.BackendTypeConstants;
import com.sap.isa.backend.boi.isacore.service.ServiceSearchBackend;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.table.Table;


/**
 * Defines the search command necessary to retrieve a list of service documents
 * available.
 */
public class ServiceTypesSearchCommand implements SearchCommand {

    private String  language;
	static final private IsaLocation loc = IsaLocation.getInstance(ServiceTypesSearchCommand.class.getName());

    /**
     * Creates a new search command for the soldTo and document list filter
     *
     * @param language for which the search should be performed
     */
    public ServiceTypesSearchCommand(String language) {
        this.language = language;
    }

    /**
     * Returns the property language
     *
     * @return language
     *
     */
    public String getLanguage() {
       return this.language;
    }


    /**
     * Perform the search in the backend.
     * 
     * @param bem With the backend object manager the search command get 
     *             access to the backend.
     * @return SearchResult 
     * @throws CommunicationException
     */
    public SearchResult performSearch(BackendObjectManager bem)
        	throws CommunicationException{
		final String METHOD = "performSearch()";
		loc.entering(METHOD);
        SearchResult result = null;

        try {

            ServiceSearchBackend ssb =
                (ServiceSearchBackend) bem.createBackendBusinessObject(BackendTypeConstants.BO_TYPE_SERVICE_SEARCH, null);

            Table resultTable = ssb.readServiceTypes(this.getLanguage());

            result = new SearchResult(resultTable, null);

        } catch (BackendException e) {
            BusinessObjectHelper.splitException(e);
            return null; // never reached
        }
        finally {
        	loc.exiting();
        }

        return result;
            
    }



}