/*****************************************************************************
    Class:        BatchCharList
    Copyright (c) 2002, SAP AG, Germany, All rights reserved.
    Author:       Daniel Seise
    Created:      07.12.2002
    Version:      1.0

    $Revision: #01 $
    $Date: 2002/12/07 $
*****************************************************************************/

package com.sap.isa.businessobject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.sap.isa.backend.boi.isacore.BatchCharListData;
import com.sap.isa.backend.boi.isacore.BatchCharValsData;
import com.sap.isa.core.Iterable;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.logging.IsaLocation;


/**
 * Represents a List of <code>Element</code> objects. This class can be used to
 * maintain a collection of such objects.
 * The internal storage is organized using a List, so duplicates of elements
 * are allowed.
 *
 * @author Daniel Seise
 * @version 1.0
 *
 * @stereotype collection
 */
public class BatchCharList extends BusinessObjectBase 
				implements Iterable, BatchCharListData{
	
	private List batchs;
	private IsaLocation log;
	private TechKey itemGuid;
	
	/**
     * Creates a new <code>BatchCharList</code> object.
     */
	public BatchCharList(){
		batchs = new ArrayList();
		log = IsaLocation.getInstance(this.getClass().getName());
	}
	
	public BatchCharList(TechKey itemGuid){
		batchs = new ArrayList();
		this.itemGuid = itemGuid;
		log = IsaLocation.getInstance(this.getClass().getName());
	}
	
					
	/**
     * Creats a new element.
     *
     * @return a element 
     */
	public BatchCharValsData createBatchCharVals(){
		return (BatchCharValsData) new BatchCharVals();
	}
	
	/**
     * Creats a new element with a specific TechKey
     *
     * @param a TechKey for the element
     * @return a element 
     */
	public BatchCharVals create(TechKey batchCharValsGuid){
		return new BatchCharVals(batchCharValsGuid);
	}
	
	
	/**
     * Adds a new <code>Element</code> to the List.
     *
     */
	public void add(){
		batchs.add(new BatchCharVals());
	}
	
	
	/**
     * Adds a new <code>Element</code> to the List.
     *
     * @param batch Element to be stored on <code>BatchCharList</code>
     */
	public void add(BatchCharVals batch){
		batchs.add(batch);
	}
	
	
	/**
     * Adds a new <code>Element</code> to the List.
     *
     * @param batch Element to be stored on <code>BatchCharList</code>
     */
	public void add(BatchCharValsData batch){
		add((BatchCharVals)batch);
	}
	
	
	/**
     * Returns the element at the specified position in this list.
     *
     * @param index index of element to return
     * @return the element at the specified position in this list
     */
	public BatchCharVals get() {
        return new BatchCharVals();
    }
    
	
	/**
     * Returns the element at the specified position in this list.
     *
     * @param index index of element to return
     * @return the element at the specified position in this list
     */
	public BatchCharVals get(int index) {
        return (BatchCharVals) batchs.get(index);
    }

    /**
     * Returns the element at the specified position in this list.
     *
     * @param index index of element to return
     * @return the element at the specified position in this list
     */
    public BatchCharValsData getBatchCharValsData(int index) {
        return (BatchCharValsData) batchs.get(index);
    }
    
    /**
     * Returns the element specified by the given technical key.<br><br>
     * <b>Note -</b> This implementation assumes that the list
     * is small and performs a simple search running in the list from the
     * first element to the last.
     *
     * @param techKey the technical key of the item that should be
     *        retrieved
     * @return the element with the given techical key or <code>null</code>
     *         if no element for that key was found.
     */
    public BatchCharVals get(TechKey techKey) {
        int size = size();
        for (int i = 0; i < size; i++) {
            if (((BatchCharVals)batchs.get(i)).getTechKey().equals(techKey)) {
                return (BatchCharVals)batchs.get(i);
            }
        }
        return null;
    }

    /**
     * Returns the element specified by the given technical key.<br><br>
     * <b>Note -</b> This implementation assumes that the list
     * is small and performs a simple search running in the list from the
     * first element to the last.
     *
     * @param techKey the technical key of the item that should be
     *        retrieved
     * @return the element with the given techical key or <code>null</code>
     *         if no element for that key was found.
     */
    public BatchCharValsData getBatchCharValsData(TechKey techKey) {
        return (BatchCharValsData) get(techKey);
    }
    
    /**
     * Removes all entries.
     */
    public void clear() {
        batchs.clear();
    }
    
    /**
     * Returns the number of elements in this list.
     *
     * @return the number of elements in this list.
     */
    public int size(){
    	return batchs.size();
    }

    
    /**
     * Returns true if this list contains no data.
     *
     * @return <code>true</code> if list contains no data.
     */
    public boolean isEmpty(){
    	return batchs.isEmpty();
    }
	
	/**
     * Returns an iterator over the elements.
     *
     * @return Iterator for this object
     */
	public Iterator iterator(){
		return batchs.iterator();
	}
    
    /**
     * Returns an array containing all of the elements in this list in proper
     * sequence;
     *
     * @return  an array containing the elements of this list.
     */
    public BatchCharVals[] toArray(){
    	int size = batchs.size();

        BatchCharVals[] a = new BatchCharVals[size];

        for (int i = 0; i < size; i++) {
            a[i] = (BatchCharVals) batchs.get(i);
        }

        return a;
    }
    
    /**
     * <p>
     * Returns a string representation of this collection.
     * The string representation consists of a list of the
     * collection's elements in the order they are returned
     * by its iterator, enclosed in square brackets ("[]").
     * Adjacent elements are separated by the characters ", "
     * (comma and space). Elements are converted to strings as
     * by <code>String.valueOf(Object)</code>.
     * </p>
     * <p>
     * This implementation creates an empty string buffer,
     * appends a left square bracket, and iterates over the
     * collection appending the string representation of each
     * element in turn. After appending each element except the
     * last, the string ", " is appended. Finally a right bracket
     * is appended. A string is obtained from the string buffer,
     * and returned.
     * </p>
     *
     * @return a string representation of this collection
     */
    public String toString() {
        return "BatchCharList " + batchs.toString();
    }

}
