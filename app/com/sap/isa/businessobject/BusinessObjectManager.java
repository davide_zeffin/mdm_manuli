/*****************************************************************************
    Class:        BusinessObjectManager
    Copyright (c) 2001, SAP AG, Germany, All rights reserved.
    Author:
    Created:      20.2.2001
    Version:      1.0

    $Revision: #9 $
    $Date: 2002/10/08 $
*****************************************************************************/

package com.sap.isa.businessobject;

// Standard Java imports 
import java.util.List;

import com.sap.isa.backend.BackendDataObjectFactory;
import com.sap.isa.backend.BackendTypeConstants;
import com.sap.isa.backend.boi.PricingBusinessObjectsAware;
import com.sap.isa.backend.boi.PricingConfiguration;
import com.sap.isa.backend.boi.isacore.ShopBackend;
import com.sap.isa.backend.boi.isacore.marketing.MarketingBusinessObjectsAware;
import com.sap.isa.backend.boi.isacore.marketing.MarketingConfiguration;
import com.sap.isa.backend.boi.isacore.marketing.MarketingUser;
import com.sap.isa.backend.boi.isacore.marketing.MktRecommendationBackend;
import com.sap.isa.backend.boi.webcatalog.CatalogBusinessObjectsAware;
import com.sap.isa.backend.boi.webcatalog.CatalogConfiguration;
import com.sap.isa.backend.boi.webcatalog.CatalogUser;
import com.sap.isa.businessobject.billing.BillingStatus;
import com.sap.isa.businessobject.capture.CapturerConfig;
import com.sap.isa.businessobject.contract.Contract;
import com.sap.isa.businessobject.lead.Lead;
import com.sap.isa.businessobject.marketing.Bestseller;
import com.sap.isa.businessobject.marketing.CUAManager;
import com.sap.isa.businessobject.marketing.MktAttributeSet;
import com.sap.isa.businessobject.negotiatedcontract.NegotiatedContract;
import com.sap.isa.businessobject.oci.OCIGenerator;
import com.sap.isa.businessobject.oci.OciConverter;
import com.sap.isa.businessobject.oci.OciServer;
import com.sap.isa.businessobject.order.Basket;
import com.sap.isa.businessobject.order.CollectiveOrder;
import com.sap.isa.businessobject.order.CollectiveOrderStatus;
import com.sap.isa.businessobject.order.CustomerOrder;
import com.sap.isa.businessobject.order.CustomerOrderStatus;
import com.sap.isa.businessobject.order.Leaflet;
import com.sap.isa.businessobject.order.Order;
import com.sap.isa.businessobject.order.OrderDataContainer;
import com.sap.isa.businessobject.order.OrderStatus;
import com.sap.isa.businessobject.order.OrderToPDFConverter;
import com.sap.isa.businessobject.ordertemplate.OrderTemplate;
import com.sap.isa.businessobject.quotation.Quotation;
import com.sap.isa.businesspartner.businessobject.BuPaManagerAware;
import com.sap.isa.businesspartner.businessobject.BupaSearch;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.CacheManager;
import com.sap.isa.core.util.GenericCacheKey;
import com.sap.isa.core.util.GenericFactory;
import com.sap.isa.dealerlocator.businessobject.Dealer;
import com.sap.isa.dealerlocator.businessobject.DealerLocator;
import com.sap.isa.helpvalues.businessobject.HelpValuesSearchHandler;
import com.sap.isa.portalurlgenerator.businessobject.PortalUrlGenerator;
import com.sap.isa.user.businessobject.IsaUserBase;
import com.sap.isa.user.businessobject.IsaUserBaseAware;
import com.sap.isa.user.businessobject.UserBase;
import com.sap.isa.user.businessobject.UserBaseAware;


/**
 * Central point to create and access Objects of the business logic. After
 * a request for a specific object, BusinessObjectManager checks if there is already
 * an object created. If so, a reference to this object is returned, otherwise
 * a new object is created and a reference to the new object is returned.
 * The reason for this class is, that the construction process of the businnes
 * logic objects may be quite complicated (because of some contextual
 * information) and most objects should only exist once per session.
 * To achieve this the <code>BusinessObjectManager</code>
 * is used. If you store a reference to this
 * object (for examle in a session context) you can get access to all other
 * business obejects using it.
 *
 * @author SAP
 * @version 1.0
 */
public class BusinessObjectManager extends GenericBusinessObjectManager
    implements BackendAware, BuPaManagerAware, UserBaseAware ,IsaUserBaseAware, 
               MarketingBusinessObjectsAware, CatalogBusinessObjectsAware,
               PricingBusinessObjectsAware {

    /**
     * Factory object to create business objects from the backend.
     */
    public static final BackendDataObjectFactory OBJECT_FACTORY = 
    	(BackendDataObjectFactory)GenericFactory.getInstance("dataObjectFactory");


    // Cache parameter
    private static final String BESTSELLER_CACHE_NAME  = "BoBestsellerCache";
    private static final int    BESTSELLER_CACHE_SIZE  = 100;

    /**
     * Constant containing the name of this BOM
     */
    public static final String ISACORE_BOM = "ISACORE-BOM";


    protected Bestseller bestseller;
    protected Shop shop;
    protected Contract contract;
    
    protected ProductBatch productBatch;

    protected DealerLocator dealerLocator;
    protected Dealer        dealer;
    
	protected ClientSystemIdHolder clientSystemIdHolder;

    protected PortalUrlGenerator portalUrlGenerator;

    static {
        CacheManager.addCache(BESTSELLER_CACHE_NAME, BESTSELLER_CACHE_SIZE);
    }
	static final private IsaLocation loc = IsaLocation.getInstance(BusinessObjectManager.class.getName());


    /**
     * Create a new instance of the object.
     */
    public BusinessObjectManager() {
    }

    /**
     * Creates a new <code>Search</code> object. If such an object was already
     * created, a reference to the old object is returned and no new object
     * is created.
     *
     * @return reference to a newly created or already existing
     *         <code>Search</code> object
     */
    public synchronized Search createSearch() {
        return (Search) createBusinessObject(Search.class);
    }

    /**
     * Returns a reference to an existing <code>Search</code> object.
     *
     * @return reference to <code>Search</code> object or null if no
     *         object is present
     */
    public Search getSearch() {
        return (Search) getBusinessObject(Search.class);
    }

    /**
     * Releases references to the search object.
     */
    public synchronized void releaseSearch() {
        releaseBusinessObject(Search.class);
    }

	/**
	 * Creates a new <code>GenericSearch</code> object. If such an object was already
	 * created, a reference to the old object is returned and no new object
	 * is created.
	 *
	 * @return reference to a newly created or already existing
	 *         <code>GenericSearch</code> object
	 */
	public synchronized GenericSearch createGenericSearch() {
		return (GenericSearch) createBusinessObject(GenericSearch.class);
	}

	/**
	 * Returns a reference to an existing <code>GenericSearch</code> object.
	 *
	 * @return reference to <code>GenericSearch</code> object or null if no
	 *         object is present
	 */
	public GenericSearch getGenericSearch() {
		return (GenericSearch) getBusinessObject(GenericSearch.class);
	}

	/**
	 * Releases references to the genericsearch object.
	 */
	public synchronized void releaseGenericSearch() {
		releaseBusinessObject(GenericSearch.class);
	}

    /**
     * Creates a shop for the given technical key and stores a reference
     * to this shop in the bom. This reference can be retrieved with
     * the <code>getShop</code> method. The shops are held
     * in a cache, so that only the first call really reads the
     * shop data from the underlying storage. All later calls for the
     * same key will be answered directly by the data stored in the
     * cache.
     *
     * @param key Technical key of the desired shop
     * @return The shop read
    */
    public synchronized Shop createShop(TechKey techKey) throws CommunicationException {

		final String METHOD = "createShop()";
		loc.entering(METHOD);
        if (loc.isDebugEnabled()) {
            loc.debug("createShop(" + techKey + ")");
        }
		try {
		
	        if (shop == null) {
	            try {
	                ShopBackend shopBE =
	                    (ShopBackend) bem.createBackendBusinessObject(BackendTypeConstants.BO_TYPE_SHOP, BusinessObjectManager.OBJECT_FACTORY);
	
	                shop = (Shop) shopBE.read(techKey);
	            }
	            catch (BackendException e) {
	                BusinessObjectHelper.splitException(e);
	                return null; // never reached, just to satisfy the compiler
	            }
	            assignBackendObjectManager(shop);
	            creationNotification(shop);
	        }
		} finally{
			loc.exiting();
		}
        return shop;
    }
    
    /**
     * Creates a new CatalogConfiguration object. If such an object was already created, a
     * reference to the old object is returned and no new object is created.
     *
     * @return reference to a newly created or already existend CatalogConfiguration object
     */
    public CatalogConfiguration createCatalogConfiguration(TechKey techKey) throws CommunicationException {
        return createShop(techKey);
    }

	/**
	 * Creates and returns a shop for the given technical key and the reference
	 * to this temporay shop is not stored in the bom. The shops are not held
	 * in a cache. 
	 *
	 * @param key Technical key of the desired shop
	 * @return The shop read
	*/
	public synchronized Shop getShop(TechKey techKey) throws CommunicationException {

		final String METHOD = "getShop()";
		loc.entering(METHOD);
		if (loc.isDebugEnabled()) {
			loc.debug("getShop(" + techKey + ")");
		}
		Shop tempShop = null;
		try {
				try {
					ShopBackend shopBE =
						(ShopBackend) bem.createBackendBusinessObject(BackendTypeConstants.BO_TYPE_SHOP, BusinessObjectManager.OBJECT_FACTORY);
	
					tempShop = (Shop) shopBE.read(techKey);
				}
				catch (BackendException e) {
					BusinessObjectHelper.splitException(e);
					return null; // never reached, just to satisfy the compiler
				}
				assignBackendObjectManager(tempShop);
				//creationNotification(tempShop);
		} finally{
			loc.exiting();
		}
		return tempShop;
	}

    /**
     * Returns a reference to an existing shop object.
     *
     * @return reference to shop object or null if no object is present
     */
    public Shop getShop() {
        return shop;
    }

    /**
     * Releases reference to the shop object.
     */
    public synchronized void releaseShop() {
        if (shop != null) {
            removalNotification(shop);
            callbackDestroy(shop);
            shop = null;
        }
    }
    
    /**
     * Releases reference to the CatalogConfiguration object.
     */
    public void releaseCatalogConfiguration() {
        releaseShop();
    }

    /**
     * Creates a new IsaQuickSearch object. If such an object was already created, a
     * reference to the old object is returned and no new object is created.
     *
     * @return reference to a newly created or already existing IsaQuickSearch object
     */
    public synchronized IsaQuickSearch createIsaQuickSearch() {
        return (IsaQuickSearch) createBusinessObject(IsaQuickSearch.class);
    }

    /**
     * Returns a reference to an existing IsaQuickSearch object.
     *
     * @return reference to IsaQuickSearch object or null if no object is present
     */
    public IsaQuickSearch getIsaQuickSearch() {
        return (IsaQuickSearch) getBusinessObject(IsaQuickSearch.class);
    }

    /**
     * Releases reference to the IsaQuickSearch object.
     */
    public synchronized void releaseIsaQuickSearch() {
        releaseBusinessObject(IsaQuickSearch.class);
    }

    /**
     * Creates a new OciConverter object. If such an object was already created, a
     * reference to the old object is returned and no new object is created.
     *
     * @return reference to a newly created or already existing OciCOnverter object
     */
    public synchronized OciConverter createOciConverter() {
        return (OciConverter) createBusinessObject(OciConverter.class);
    }

    /**
     * Returns a reference to an existing OciConverter object.
     *
     * @return reference to OciConverter object or null if no object is present
     */
    public OciConverter getOciConverter() {
        return (OciConverter) getBusinessObject(OciConverter.class);
    }

    /**
     * Releases reference to the OciConverter object.
     */
    public synchronized void releaseOciConverter() {
        releaseBusinessObject(OciConverter.class);
    }

    /**
     * Creates a new OCIGenerator object. If such an object was already created, a
     * reference to the old object is returned and no new object is created.
     *
     * @return reference to a newly created or already existing OCIGenerator object
     */
    public synchronized OCIGenerator createOCIGenerator() {
        return (OCIGenerator) createBusinessObject(OCIGenerator.class);
    }

    /**
     * Returns a reference to an existing OCIGenerator object.
     *
     * @return reference to OCIGenerator object or null if no object is present
     */
    public OCIGenerator getOCIGenerator() {
        return (OCIGenerator) getBusinessObject(OCIGenerator.class);
    }

    /**
     * Releases reference to the OCIGenerator object.
     */
    public synchronized void releaseOCIGenerator() {
        releaseBusinessObject(OCIGenerator.class);
    }
    /**
     * Creates a new attribute set object. If such an object was already created, a
     * reference to the old object is returned and no new object is created.
     *
     * @return referenc to a newly created or already existend attribute set object
     */
    public synchronized OrderToPDFConverter createOrderToPDFConverter() {
        return (OrderToPDFConverter) createBusinessObject(OrderToPDFConverter.class);
    }

	/**
	 * Creates a new attribute set object. If such an object was already created, a
	 * reference to the old object is returned and no new object is created.
	 *
	 * @return referenc to a newly created or already existend attribute set object
	 */
	public synchronized ClientSystemIdHolder createClientSystemIdHolder() {
		return (ClientSystemIdHolder) createBusinessObject(ClientSystemIdHolder.class);
	}

    /**
     * Returns a reference to an oci server object.
     *
     * @return reference to OciServer object or null if no object is present
     */
    public OrderToPDFConverter getOrderToPDFConverter() {
        return (OrderToPDFConverter) getBusinessObject(OrderToPDFConverter.class);
    }

	/**
	 * Returns a reference to an oci server object.
	 *
	 * @return reference to ClientSystemIdHolder object or null if no object is present
	 */
	public ClientSystemIdHolder getClientSystemIdHolder() {
		return (ClientSystemIdHolder) getBusinessObject(ClientSystemIdHolder.class);
	}

    /**
     * Release the references to created attribute set objects.
     *
     */
    public synchronized void releaseOrderToPDFConverter() {
        releaseBusinessObject(OrderToPDFConverter.class);
    }

	/**
	 * Release the references to created attribute set objects.
	 *
	 */
	public synchronized void releaseClientSystemIdHolder() {
		releaseBusinessObject(ClientSystemIdHolder.class);
	}

    /**
     * Creates a new attribute set object. If such an object was already created, a
     * reference to the old object is returned and no new object is created.
     *
     * @return referenc to a newly created or already existend attribute set object
     */
    public synchronized UOMConverter createUOMConverter() {
        return (UOMConverter) createBusinessObject(UOMConverter.class);
    }


    /**
     * Returns a reference to an oci server object.
     *
     * @return reference to UOMConverter object or null if no object is present
     */
    public UOMConverter getUOMConverter() {
        return (UOMConverter) getBusinessObject(UOMConverter.class);
    }

    /**
     * Release the references to created attribute set objects.
     *
     */
    public synchronized void releaseUOMConverter() {
        releaseBusinessObject(UOMConverter.class);
    }

	/**
	 * Creates a new attribute set object. If such an object was already created, a
	 * reference to the old object is returned and no new object is created.
	 *
	 * @return referenc to a newly created or already existend attribute set object
	 */
	public synchronized CurrencyConverter createCurrencyConverter() {
		return (CurrencyConverter) createBusinessObject(CurrencyConverter.class);
	}


	/**
	 * Returns a reference to an currency converted object.
	 *
	 * @return reference to CurrencyConverter object or null if no object is present
	 */
	public CurrencyConverter getCurrencyConverter() {
		return (CurrencyConverter) getBusinessObject(CurrencyConverter.class);
	}

	/**
	 * Release the references to created attribute set objects.
	 *
	 */
	public synchronized void releaseCurrencyConverter() {
		releaseBusinessObject(CurrencyConverter.class);
	}
    /**
     * Creates a new basket object. If such an object was already created, a
     * reference to the old object is returned and no new object is created.
     *
     * @return reference to a newly created or already existing basket object
     */
    public synchronized Basket createBasket() {
        return (Basket) createBusinessObject(Basket.class);
    }

    /**
     * Returns a reference to an existing basket object.
     *
     * @return reference to basket object or null if no object is present
     */
    public Basket getBasket() {
        return (Basket) getBusinessObject(Basket.class);
    }

    /**
     * Releases references to the basket
     */
    public synchronized void releaseBasket() {
        releaseBusinessObject(Basket.class);
    }
    
    /**
     * Creates a new batchList object. If such an object was already created, a
     * reference to the old object is returned and no new object is created.
     *
     * @return reference to a newly created or already existing basket object
     */
    public synchronized ProductBatch createProductBatch() {
    	return (ProductBatch) createBusinessObject(ProductBatch.class);
    }

    /**
     * Returns a reference to an existing batchList object.
     *
     * @return reference to basket object or null if no object is present
     */
    public ProductBatch getProductBatch() {
        return (ProductBatch) getBusinessObject(ProductBatch.class);
    }

    /**
     * Releases references to the batchList
     */
    public synchronized void releaseProductBatch() {
        releaseBusinessObject(ProductBatch.class);
    }

    /**
     * Creates a new order object. If such an object was already created, a
     * reference to the old object is returned and no new object is created.
     *
     * @return reference to a newly created or already existing order object
     */
    public synchronized Order createOrder() {
        return createOrder(0);
    }

    /**
     * Creates a new order object. If such an object was already created, a
     * reference to the old object is returned and no new object is created.
     *
     * @param index Index of the order
     * @return reference to a newly created or already existing order object or
     *         <code>null</code> if the index is exceeded
     */
    public synchronized Order createOrder(int index) {
        return (Order) createBusinessObject(Order.class, index);
    }

    /**
     * Creates a new order object using the given <code>Quotation</code>. If
     * the order was already created, a reference to that object is returned.
     * This is done without any considerations about the Quotation supplied
     * to the method. That means, if there is already an order present and
     * this method is called, a reference to the order is returned independent
     * of the fact if the quotation is the same or not used in creation of that
     * order.
     *
     * @param quotation Quotation used for the creation of the order
     * @return reference to a newly created or already existing order object
     */
    public synchronized Order createOrder(Quotation quotation) {
        return createOrder(0, quotation);
    }

    /**
     * Creates a new order object using the given <code>Quotation</code>. If
     * the order was already created, a reference to that object is returned.
     * This is done without any considerations about the quotation supplied
     * to the method. That means, if there is already an order present and
     * this method is called, a reference to the order is returned independent
     * of the fact if the quotation is the same or not used in creation of that
     * order.
     *
     * @param index Index of the order
     * @param quotation Quotation used for the creation of the order
     * @return reference to a newly created or already existing order object
     */
    public synchronized Order createOrder(int index, Quotation quotation) {
		final String METHOD = "createOrder()";
		loc.entering(METHOD);
        if (loc.isDebugEnabled()) {
            loc.debug("createOrder("
                    + index + ", " + quotation + ")");
        }

        List allInstances = prepareList(Order.class, index);

        Object instance = allInstances.get(index);

        if (instance == null) {
            instance = new Order(quotation);

            assignBackendObjectManager(instance);
            allInstances.add(index, instance);
            creationNotification(instance);
        }
		loc.exiting();
        return (Order) instance;
    }

    /**
     * Returns a reference to an existing order object.
     *
     * @return reference to basket object or null if no object is present
     */
    public Order getOrder() {
        return getOrder(0);
    }

    /**
     * Returns a reference to an existing order object.
     *
     * @param index Index of the order
     * @return reference to order object, <code>null</code> if no object is present
     *         at the given position of if the index is exceeded
     */
    public Order getOrder(int index) {
        return (Order) getBusinessObject(Order.class, index);
    }

    /**
     * Releases references to the order
     */
    public synchronized void releaseOrder() {
        releaseOrder(0);
    }

    /**
     * Releases references to the order object specified by the index
     *
     * @param index Index of the order to be released
     */
    public synchronized void releaseOrder(int index) {
        releaseBusinessObject(Order.class, index);
    }

    /**
     * Returns the index of a free order position (index) that could be used
     * in calls to the createOrder method to get a new order
     *
     * @return Index of the free position or <code>-1</code> if not free
     *         slot is found
     */
/*    public int getFreeOrderIndex() {
        for (int i = 0; i < orderArray.length; i++) {
            if (orderArray[i] == null) {
                return i;
            }
        }
        return -1;
    }
*/


    /**
     * Creates a new order object. If such an object was already created, a
     * reference to the old object is returned and no new object is created.
     *
     * @return reference to a newly created or already existing order object
     */
    public synchronized CollectiveOrder createCollectiveOrder() {
        return createCollectiveOrder(0);
    }

    /**
     * Creates a new order object. If such an object was already created, a
     * reference to the old object is returned and no new object is created.
     *
     * @param index Index of the order
     * @return reference to a newly created or already existing order object or
     *         <code>null</code> if the index is exceeded
     */
    public synchronized CollectiveOrder createCollectiveOrder(int index) {
        return (CollectiveOrder) createBusinessObject(CollectiveOrder.class, index);
    }


    /**
     * Returns a reference to an existing order object.
     *
     * @return reference to basket object or null if no object is present
     */
    public CollectiveOrder getCollectiveOrder() {
        return getCollectiveOrder(0);
    }

    /**
     * Returns a reference to an existing order object.
     *
     * @param index Index of the order
     * @return reference to order object, <code>null</code> if no object is present
     *         at the given position of if the index is exceeded
     */
    public CollectiveOrder getCollectiveOrder(int index) {
        return (CollectiveOrder) getBusinessObject(CollectiveOrder.class, index);
    }

    /**
     * Releases references to the order
     */
    public synchronized void releaseCollectiveOrder() {
        releaseCollectiveOrder(0);
    }

    /**
     * Releases references to the order object specified by the index
     *
     * @param index Index of the order to be released
     */
    public synchronized void releaseCollectiveOrder(int index) {
        releaseBusinessObject(CollectiveOrder.class, index);
    }


    /**
     * Creates a new order object. If such an object was already created, a
     * reference to the old object is returned and no new object is created.
     *
     * @return reference to a newly created or already existing order object
     */
    public synchronized CustomerOrder createCustomerOrder() {
        return createCustomerOrder(0);
    }

   /**
     * Creates a new order object. If such an object was already created, a
     * reference to the old object is returned and no new object is created.
     *
     * @param index Index of the order
     * @return reference to a newly created or already existing order object or
     *         <code>null</code> if the index is exceeded
     */
    public synchronized CustomerOrder createCustomerOrder(int index) {
        return (CustomerOrder) createBusinessObject(CustomerOrder.class, index);
    }


    /**
     * Returns a reference to an existing order object.
     *
     * @return reference to basket object or null if no object is present
     */
    public CustomerOrder getCustomerOrder() {
        return getCustomerOrder(0);
    }

    /**
     * Returns a reference to an existing order object.
     *
     * @param index Index of the order
     * @return reference to order object, <code>null</code> if no object is present
     *         at the given position of if the index is exceeded
     */
    public CustomerOrder getCustomerOrder(int index) {
        return (CustomerOrder) getBusinessObject(CustomerOrder.class, index);
    }

    /**
     * Releases references to the order
     */
    public synchronized void releaseCustomerOrder() {
        releaseCustomerOrder(0);
    }

    /**
     * Releases references to the order object specified by the index
     *
     * @param index Index of the order to be released
     */
    public synchronized void releaseCustomerOrder(int index) {
        releaseBusinessObject(CustomerOrder.class, index);
    }


    /**
     * Creates a new order status object using a given order.
     * Method will be removed with Internet Sales Release 5.0.<br>
     * Use method <code>createOrderStatus()</code>instead
     * @deprecated
     * @see com.sap.isa.businessobject. SalesDocumentStatus . getOrder()
     * also
     *
     * @param order the order object used in the creation
     * @return reference to the object
     */
    public synchronized OrderStatus createOrderStatus(Order order) {
		final String METHOD = "crateOrderStatus()";
		loc.entering(METHOD);
        if (loc.isDebugEnabled()) {
            loc.debug("createOrderStatus(" + order + ")");
        }

        List allInstances = prepareList(OrderStatus.class, 0);

        Object instance = allInstances.get(0);

        if (instance == null) {
            if (order != null) {
                instance = new OrderStatus(order);
                // fix for missing bem reference in orders created using
                // the DocumentStatusDetailPrepareAction
                assignBackendObjectManager(order);
            }
            else {
                instance = new OrderStatus();
            }

            assignBackendObjectManager(instance);
            allInstances.add(0, instance);
            creationNotification(instance);
        }
		loc.exiting();
        return (OrderStatus) instance;
    }
    /**
     * Creates a new order status object using a given OrderDataContainer.
     * @see com.sap.isa.businessobject. SalesDocumentStatus . getOrder()
     * also
     *
     * @param OrderDataContainer The order object used in the creation
     * @return reference to the object
     */
    public synchronized OrderStatus createOrderStatus(OrderDataContainer order) {
		final String METHOD = "createOrderStatus()";
		loc.entering(METHOD);
	    if (loc.isDebugEnabled()) {
		    loc.debug("createOrderStatus(" + order + ")");
	    }

	    List allInstances = prepareList(OrderStatus.class, 0);

	    Object instance = allInstances.get(0);

    	if (instance == null) {
	    	if (order != null) {
		    	instance = new OrderStatus(order);
    		}
    		else {
    			instance = new OrderStatus();
    		}
			assignBackendObjectManager(instance);
			allInstances.add(0, instance);
			creationNotification(instance);
    	}
		loc.exiting();
    	return (OrderStatus) instance;
    }


    /**
     * Creates a new order status object.
     *
     * @return reference to the order status object
     */
    public synchronized OrderStatus createOrderStatus() {
        return createOrderStatus(null);
    }

    /**
     * Creates a new orderStatus object. If such an object was already created, a
     * reference to the old object is returned and no new object is created.
     *
     * @return reference to a newly created or already existing orderStatus object
     */
    public OrderStatus getOrderStatus() {
        return (OrderStatus) getBusinessObject(OrderStatus.class);
    }

    /**
     * Release the references to created orderStatus object.
     *
     */
    public synchronized void releaseOrderStatus() {
        releaseBusinessObject(OrderStatus.class);
    }

    /**
     * Creates a new order status object using a given order.
     *
     * @param order the order object used in the creation
     * @return reference to the object
     */
    public synchronized CustomerOrderStatus createCustomerOrderStatus(CustomerOrder order) {
		final String METHOD = "createCustomerOrderStatus()";
		loc.entering(METHOD);
        if (loc.isDebugEnabled()) {
            loc.debug("createCustomerOrderStatus(" + order + ")");
        }

        List allInstances = prepareList(CustomerOrderStatus.class, 0);

        Object instance = allInstances.get(0);

        if (instance == null) {
            if (order != null) {
                instance = new CustomerOrderStatus(order);
            }
            else {
                instance = new CustomerOrderStatus();
            }

            // fix for missing bem reference in orders created using
            // the DocumentStatusDetailPrepareAction
            CustomerOrderStatus orderStatus = (CustomerOrderStatus)instance;
            if (orderStatus.getOrder()!= null) {
               assignBackendObjectManager(orderStatus.getOrder());
            }
            assignBackendObjectManager(instance);
            allInstances.add(0, instance);
            creationNotification(instance);
        }
		loc.exiting();
        return (CustomerOrderStatus) instance;
    }


    /**
     * Creates a new order status object.
     *
     * @return reference to the order status object
     */
    public synchronized CustomerOrderStatus createCustomerOrderStatus() {
        return createCustomerOrderStatus(null);
    }

    /**
     * Creates a new orderStatus object. If such an object was already created, a
     * reference to the old object is returned and no new object is created.
     *
     * @return reference to a newly created or already existing orderStatus object
     */
    public CustomerOrderStatus getCustomerOrderStatus() {
        return (CustomerOrderStatus) getBusinessObject(CustomerOrderStatus.class);
    }

    /**
     * Release the references to created orderStatus object.
     *
     */
    public synchronized void releaseCustomerOrderStatus() {
        releaseBusinessObject(CustomerOrderStatus.class);
    }

    /**
     * Creates a new collective order status object using a given collective order.
     *
     * @param order the order object used in the creation
     * @return reference to the object
     */
    public synchronized CollectiveOrderStatus createCollectiveOrderStatus(CollectiveOrder order) {
		final String METHOD = "createCollectiveOrderStatus()";
		loc.entering(METHOD);
        if (loc.isDebugEnabled()) {
            loc.debug("createCollectiveOrderStatus(" + order + ")");
        }

        List allInstances = prepareList(CollectiveOrderStatus.class, 0);

        Object instance = allInstances.get(0);

        if (instance == null) {
            if (order != null) {
                instance = new CollectiveOrderStatus(order);
            }
            else {
                instance = new CollectiveOrderStatus();
            }

            // fix for missing bem reference in orders created using
            // the DocumentStatusDetailPrepareAction
            CollectiveOrderStatus orderStatus = (CollectiveOrderStatus)instance;
            if (orderStatus.getOrder()!= null) {
               assignBackendObjectManager(orderStatus.getOrder());
            }
            assignBackendObjectManager(instance);
            allInstances.add(0, instance);
            creationNotification(instance);
        }
		loc.exiting();
        return (CollectiveOrderStatus) instance;
    }


    /**
     * Creates a new order status object.
     *
     * @return reference to the order status object
     */
    public synchronized CollectiveOrderStatus createCollectiveOrderStatus() {
        return createCollectiveOrderStatus(null);
    }

    /**
     * Creates a new orderStatus object. If such an object was already created, a
     * reference to the old object is returned and no new object is created.
     *
     * @return reference to a newly created or already existing orderStatus object
     */
    public CollectiveOrderStatus getCollectiveOrderStatus() {
        return (CollectiveOrderStatus) getBusinessObject(CollectiveOrderStatus.class);
    }

    /**
     * Release the references to created orderStatus object.
     *
     */
    public synchronized void releaseCollectiveOrderStatus() {
        releaseBusinessObject(CollectiveOrderStatus.class);
    }

    /**
     * Creates a new Quotation object. If such an object was already created, a
     * reference to the old object is returned and no new object is created.
     *
     * @return reference to a newly created or already existing Quotation object
     */
    public synchronized Quotation createQuotation() {
        return (Quotation) createBusinessObject(Quotation.class);
    }

    /**
     * Returns a reference to an existing Quotation object.
     *
     * @return reference to Quotation object or null if no object is present
     */
    public Quotation getQuotation() {
        return (Quotation) getBusinessObject(Quotation.class);
    }

     /**
     * Release the references to created quotation object.
     */
    public synchronized void releaseQuotation() {
        releaseBusinessObject(Quotation.class);
    }

	 /**
	 * Creates a new NegotiatedContract object, which should be used for
	 * editing a contract. If such an object was already created, a reference to
	 * the old object is returned and no new object is created.
	 *
	 * @return reference to a newly created or already existing NegotiatedContract object
	 */
	public synchronized NegotiatedContract createNegotiatedContract() {
		if (loc.isDebugEnabled()) { loc.debug("createNegotiatedContract()");}
		return (NegotiatedContract) createBusinessObject(NegotiatedContract.class, 0);
	}
	
	/**
	 * Returns a reference to an existing NegotiatedContract object.
	 *
	 * @return reference to NegotiatedContract object or null if no object is present
	 */
	public NegotiatedContract getNegotiatedContract() {
		if (loc.isDebugEnabled()) { loc.debug("getNegotiatedContract()");}
		return (NegotiatedContract) getBusinessObject(NegotiatedContract.class, 0);
	}
	
	/**
	* Release the references to created negotiated contract object.
	*/
	public synchronized void releaseNegotiatedContract() {
		if (loc.isDebugEnabled()) { loc.debug("releaseNegotiatedContract()");}
		releaseBusinessObject(NegotiatedContract.class, 0);
	}
	
	/**
	 * Creates a new NegotiatedContract object, which should be used for
	 * displaying. If such an object was already created, a reference to the old
	 * object is returned and no new object is created.
	 *
	 * @return reference to a newly created or already existing NegotiatedContract object
	 */
	public synchronized NegotiatedContract createNegotiatedContractStatus() {
		if (loc.isDebugEnabled()) { loc.debug("createNegotiatedContractStatus()");}
		return (NegotiatedContract) createBusinessObject(NegotiatedContract.class, 1);
	}
	
	/**
	 * Returns a reference to an existing NegotiatedContract object.
	 *
	 * @return reference to NegotiatedContract object or null if no object is present
	 */
	public NegotiatedContract getNegotiatedContractStatus() {
		if (loc.isDebugEnabled()) { loc.debug("getNegotiatedContractStatus()");}
		return (NegotiatedContract) getBusinessObject(NegotiatedContract.class, 1);
	}
	
	/**
	* Release the references to created negotiated contract object.
	*/
	public synchronized void releaseNegotiatedContractStatus() {
		if (loc.isDebugEnabled()) { loc.debug("releaseNegotiatedContractStatus()");}
		releaseBusinessObject(NegotiatedContract.class, 1);
	}
	
    /**
     * Creates a new billing status object.
     *
     * @return reference to the newly created object
     */
    public synchronized BillingStatus createBillingStatus() {
        return (BillingStatus) createBusinessObject(BillingStatus.class);
    }

    /**
     * Creates a new billingStatus object. If such an object was already created, a
     * reference to the old object is returned and no new object is created.
     *
     * @return reference to a newly created or already existing billingStatus object
     */
    public BillingStatus getBillingStatus() {
        return (BillingStatus) getBusinessObject(BillingStatus.class);
    }

    /**
     * Release the references to created billingStatus object.
     *
     */
    public synchronized void releaseBillingStatus() {
        releaseBusinessObject(BillingStatus.class);
    }

    /**
     * Creates a order template.
     *
     * @return reference to the order template
     */
    public synchronized OrderTemplate createOrderTemplate() {
        return (OrderTemplate) createBusinessObject(OrderTemplate.class);
    }

    /**
     * Creates a new orderTemplate object. If such an object was already created, a
     * reference to the old object is returned and no new object is created.
     *
     * @return reference to a newly created or already existing orderTemplate object
     */
    public OrderTemplate getOrderTemplate() {
        return (OrderTemplate) getBusinessObject(OrderTemplate.class);
    }

    /**
     * Release the references to created billingStatus object.
     *
     */
    public synchronized void releaseOrderTemplate() {
        releaseBusinessObject(OrderTemplate.class);
    }

    /**
     * Creates a new leaflet object. If such an object was already created, a
     * reference to the old object is returned and no new object is created.
     *
     * @return reference to a newly created or already existing leaflet object
     */
    public synchronized Leaflet createLeaflet() {
        return (Leaflet) createBusinessObject(Leaflet.class);
    }

    /**
     * Returns a reference to an existing leaflet object.
     *
     * @return reference to leaflet object or null if no object is present
     */
    public Leaflet getLeaflet() {
        return (Leaflet) getBusinessObject(Leaflet.class);
    }

    /**
     * Release the reference to the leaflet.
     */
    public synchronized void releaseLeaflet() {
        releaseBusinessObject(Leaflet.class);
    }

    /**
     * Creates a new user object. If such an object was already created, a
     * reference to the old object is returned and no new object is created.
     *
     * @return referenc to a newly created or already existend user object
     */
    public synchronized User createUser() {
        return (User) createBusinessObject(User.class);
    }
    
    /**
     * Creates a new user object. If such an object was already created, a
     * reference to the old object is returned and no new object is created.
     *
     * @return reference to a newly created or already existend user object
     */
    public CatalogUser createCatalogUser() {
        return createUser();
    }

    /**
     * Returns a reference to an existing user object.
     *
     * @return reference to user object or null if no object is present
     */
    public User getUser() {
      return (User) getBusinessObject(User.class);
    }
    
    /**
     * Returns the CatalogUser object
     * 
     * @return the CatalogUser object
     */
    public CatalogUser getCatalogUser() {
        return getUser();
    }

    /**
     * Release the references to created user object.
     */
    public synchronized void releaseUser() {
        releaseBusinessObject(User.class);
    }
    
    /**
     * Creates new {@link com.sap.isa.user.businessobject.UserBase} object.<br>
     * The functionality will be mapped to the {@link #createUser()} method
     * and the result will be casted to {@link com.sap.isa.user.businessobject.UserBase} object.
     * 
     * @see com.sap.isa.user.businessobject.UserBaseAware#createUserBase()
     */
    public UserBase createUserBase() {
        return (UserBase) createBusinessObject(User.class);
    }

    /**
     * Returns a reference to an existing {@link com.sap.isa.user.businessobject.UserBase} object.<br>
     * The functionality will be mapped to the {@link #getUser()} method
     * and the result will be casted to {@link com.sap.isa.user.businessobject.UserBase} object.
     *
     * @return reference to UserBase object or null if no object is present
     */    
    public UserBase getUserBase() {
        return (UserBase)getUser();
    }

	public IsaUserBase createIsaUserBase() {
		return (IsaUserBase)createUser();
	}

    /**
     * Creates a new attribute set object. If such an object was already created, a
     * reference to the old object is returned and no new object is created.
     *
     * @return referenc to a newly created or already existend attribute set object
     */
    public synchronized MktAttributeSet createMktAttributeSet() {
        return (MktAttributeSet) createBusinessObject(MktAttributeSet.class);
    }

    /**
     * Returns a reference to an existing marketing attribute set.
     *
     * @return reference to MktAttributeSet object or null if no object is present
     */
    public MktAttributeSet getMktAttributeSet() {
        return (MktAttributeSet) getBusinessObject(MktAttributeSet.class);
    }
    
    /**
     * Returns the MarketingConfiguration object
     * 
     * @return the MarketingConfiguration object
     */
    public MarketingConfiguration getMarketingConfiguration() {
        return getShop();
    }
    
    /**
     * Returns the MarketingUser object
     * 
     * @return the MarketingUser object
     */
    public MarketingUser getMarketingUser() {
        return getUser();
    }
    
    /**
     * Returns the CatalogConfiguration object
     * 
     * @return the CatalogConfiguration object
     */
    public CatalogConfiguration getCatalogConfiguration() {
        return getShop();
    }

    /**
     * Release the references to created attribute set objects.
     */
    public synchronized void releaseMktAttributeSet() {
        releaseBusinessObject(MktAttributeSet.class);
    }

    /**
     * Creates a new bestseller object, if it couldn't found in the cache.
     * If such an object was already created, a
     * reference to the old object is returned and no new object is created.
     *
     * @return referenc to a newly created or already existend attribute set object
     * @deprecated
     */
    public synchronized Bestseller createBestseller(TechKey shop,
                                                    String  configuration)
    
            throws CommunicationException {
        final String METHOD = "createBestseller()";
        loc.entering(METHOD);
        if (loc.isDebugEnabled()) {
            loc.debug("createBestseller("
                    + shop + ", " +configuration+ ")");
        }
        try {
            
            if (bestseller == null) {
        
                BestsellerKey bestsellerKey = new BestsellerKey (shop, configuration);
        
                bestseller = (Bestseller)CacheManager.get(BESTSELLER_CACHE_NAME, bestsellerKey);
                if (bestseller == null) {
                    try {
                        MktRecommendationBackend backendService =
                            (MktRecommendationBackend) bem.createBackendBusinessObject(BackendTypeConstants.BO_TYPE_MKT_RECOMMENDATION, BusinessObjectManager.OBJECT_FACTORY);
        
                        bestseller = (Bestseller)backendService.readBestsellers(shop);
                        if (bestseller != null) {
                            //  store only a copy from the bestseller without catalog information in the cache. 
                            CacheManager.put(BESTSELLER_CACHE_NAME, bestsellerKey, bestseller.clone());
                        }
                   }
                   catch (BackendException e) {
                       if (loc.isDebugEnabled()) loc.debug(e);
                       BusinessObjectHelper.splitException(e);
                       return null; // never reached, just to satisfy the compiler
                   }
                }
                else {
                    // return only a copy from the bestseller and reset the enhancement
                    // to calculate the price for the current user
                    bestseller = (Bestseller)bestseller.clone();
                    bestseller.resetEnhanced();
                }
                creationNotification(bestseller);
            }
        } finally{
            loc.exiting();
        }
        return bestseller;
    }

    
    /**
     * Creates a new bestseller object, if it couldn't found in the cache.
     * If such an object was already created, a
     * reference to the old object is returned and no new object is created.
     *
     * @return referenc to a newly created or already existend attribute set object
     */
    public synchronized Bestseller createBestseller(MarketingConfiguration mktConfig,
                                                    String  configuration)

            throws CommunicationException {
		final String METHOD = "createBestseller()";
		loc.entering(METHOD);
        if (loc.isDebugEnabled()) {
            loc.debug("createBestseller("
                    + shop + ", " +configuration+ ")");
        }
		try {
		
	        if (bestseller == null) {
	
	            BestsellerKey bestsellerKey = new BestsellerKey (mktConfig.getTechKey(), configuration);
	
	            bestseller = (Bestseller)CacheManager.get(BESTSELLER_CACHE_NAME, bestsellerKey);
	            if (bestseller == null) {
	                try {
	                    MktRecommendationBackend backendService =
	                        (MktRecommendationBackend) bem.createBackendBusinessObject(BackendTypeConstants.BO_TYPE_MKT_RECOMMENDATION, BusinessObjectManager.OBJECT_FACTORY);
	
	                    bestseller = (Bestseller)backendService.readBestsellers(mktConfig);
	                    if (bestseller != null) {
							//	store only a copy from the bestseller without catalog information in the cache. 
	                        CacheManager.put(BESTSELLER_CACHE_NAME, bestsellerKey, bestseller.clone());
	                    }
	               }
	               catch (BackendException e) {
	                   if (loc.isDebugEnabled()) loc.debug(e);
	                   BusinessObjectHelper.splitException(e);
	                   return null; // never reached, just to satisfy the compiler
	               }
	            }
	            else {
	                // return only a copy from the bestseller and reset the enhancement
	                // to calculate the price for the current user
	                bestseller = (Bestseller)bestseller.clone();
	                bestseller.resetEnhanced();
	            }
	            creationNotification(bestseller);
	        }
		} finally{
			loc.exiting();
		}
        return bestseller;
    }

    /**
     * Returns a reference to an existing bestseller list.
     *
     * @return reference to Bestseller object or null if no object is present
     */
    public Bestseller getBestseller() {
        return bestseller;
    }

    /**
     *   Returns Business Object BupaSearch
     */
    public synchronized BupaSearch createBupaSearch() {
        return (BupaSearch) createBusinessObject(BupaSearch.class);
    }

    /**
     * Creates a new CUAManager object. If such an object was already created, a
     * reference to the old object is returned and no new object is created.
     *
     * @return reference to a newly created or already existing object
     */
    public synchronized CUAManager createCUAManager() {
        return (CUAManager) createBusinessObject(CUAManager.class);
    }

    /**
     * Returns a reference to an existing CUAManager object.
     *
     * @return reference to object
     */
    public CUAManager getCUAManager() {
        return (CUAManager) getBusinessObject(CUAManager.class);
    }

    /**
     * Release the references to created CUAManager object.
     */
    public synchronized void releaseCUAManager() {
        releaseBusinessObject(CUAManager.class);
    }

    /**
     * Creates a new contract object. If such an object was already created, a
     * reference to the old object is returned and no new object is created.
     *
     * @return reference to a newly created or already existing contract object
     */
    public synchronized Contract createContract() {
        return (Contract) createBusinessObject(Contract.class);
    }

    /**
     * Returns a reference to an existing contract.
     *
     * @return reference to contract object or null if no object is present
     */
    public Contract getContract() {
        return (Contract) getBusinessObject(Contract.class);
    }

    /**
     * Release the references to created contract object.
     *
     */
    public void releaseContract() {
        releaseBusinessObject(Contract.class);
    }

    /**
     * Creates a new attribute set object. If such an object was already created, a
     * reference to the old object is returned and no new object is created.
     *
     * @return referenc to a newly created or already existend attribute set object
     */
    public synchronized OciServer createOciServer() {
        return (OciServer) createBusinessObject(OciServer.class);
    }

    /**
     * Returns a reference to an oci server object.
     *
     * @return reference to OciServer object or null if no object is present
     */
    public OciServer getOciServer() {
        return (OciServer) getBusinessObject(OciServer.class);
    }

    /**
     * Release the references to created attribute set objects.
     *
     */
    public synchronized void releaseOciServer() {
        releaseBusinessObject(OciServer.class);
    }

   /**
     * Creates a new portalUrlGenerator object. If such an object was already created, a
     * reference to the old object is returned and no new object is created.
     *
     * @return referenc to a newly created or already existend portalUrlGenerator object
     */
    public synchronized PortalUrlGenerator createPortalUrlGenerator() {
        portalUrlGenerator = (PortalUrlGenerator)createBusinessObject(PortalUrlGenerator.class);
        return portalUrlGenerator;
    }

    /**
     * Returns a reference to an existing portalUrlGenerator object.
     *
     * @return reference to portalUrlGenerator object or null if no object is present
     */
    public PortalUrlGenerator getPortalUrlGenerator() {
      return (PortalUrlGenerator) getBusinessObject(PortalUrlGenerator.class);
    }


    /**
     * Release the references to created portalUrlGenerator object.
     */
    public synchronized void releasePortalUrlGenerator() {
        releaseBusinessObject(PortalUrlGenerator.class);
    }
    
    /**
     * Returns the PricingConfiguration object
     * 
     * @return the PricingConfiguration object
     */
    public PricingConfiguration getPricingConfiguration() {
        return getShop();
    }

   /**
     * Creates a new user object. If such an object was already created, a
     * reference to the old object is returned and no new object is created.
     *
     * @return referenc to a newly created or already existend user object
     */
    public synchronized DealerLocator createDealerLocator() {
        dealerLocator = (DealerLocator)createBusinessObject(DealerLocator.class);
        return dealerLocator;
    }

    /**
     * Returns a reference to an existing user object.
     *
     * @return reference to user object or null if no object is present
     */
    public DealerLocator getDealerLocator() {
      return (DealerLocator) getBusinessObject(DealerLocator.class);
    }


    /**
     * Release the references to created user object.
     */
    public synchronized void releaseDealerLocator() {
        releaseBusinessObject(DealerLocator.class);
    }



    /**
     * Creates a dealer and stores a reference
     * to this dealer in the bom. This reference can be retrieved with
     * the <code>getDealer</code> method.
     *
     *
     * @return The dealerLocator read
    */
    public synchronized Dealer createDealer(TechKey dealerKey)
            throws CommunicationException {
		final String METHOD = "createDealer()";
		loc.entering(METHOD);
		if (loc.isDebugEnabled())
			loc.debug("Dealer techkey = "+dealerKey);
        if (dealer == null) {
            if (dealerLocator == null) {
                createDealerLocator();
            }

            if (dealerLocator != null) {
                dealer = dealerLocator.createDealer(dealerKey);
            }
        }
        else {
            dealer = dealerLocator.createDealer(dealerKey);
        }
		loc.exiting();
        return dealer;
    }

    /**
     * Returns a reference to an existing dealer object.
     *
     * @return reference to dealer object or null if no object is present
     */
    public Dealer getDealer() {
        return dealer;
    }

    /**
     * Releases reference to the dealer admin object.
     */
    public synchronized void releaseDealer() {
		final String METHOD = "releaseDealer()";
		loc.entering(METHOD);
        if (dealer != null) {
            removalNotification(dealer);
            callbackDestroy(dealer);
            dealer = null;
        }
        loc.exiting();
    }


    /**
     * Callback for the manager of the boms to inform this object of the
     * end of its life. <b>This method should only be called by the
     * MetaBusinessObjectManager</b>.
     */
    public void release() {
		final String METHOD = "release()";
		loc.entering(METHOD);
        super.release();

        if (shop != null) {
            shop.destroy();
            shop.setBackendObjectManager(null);
        }
		loc.exiting();
    }

    /**
     * Creates a new BusinessPartnerManager object. If such an object was already created, a
     * reference to the old object is returned and no new object is created.
     *
     * @return reference to a newly created or already existing object
     */
    public synchronized BusinessPartnerManager createBUPAManager() {
        return (BusinessPartnerManager) createBusinessObject(BusinessPartnerManager.class);
    }

    /**
     * Returns a reference to an existing CUAManager object.
     *
     * @return reference to object
     */
    public BusinessPartnerManager getBUPAManager() {
        return (BusinessPartnerManager) getBusinessObject(BusinessPartnerManager.class);
    }


    /**
     * Creates a order template.
     *
     * @return reference to the order template
     */
    public synchronized Lead createLead() {
        return (Lead) createBusinessObject(Lead.class);
    }

    /**
     * Creates a new orderTemplate object. If such an object was already created, a
     * reference to the old object is returned and no new object is created.
     *
     * @return reference to a newly created or already existing orderTemplate object
     */
    public Lead getLead() {
        return (Lead) getBusinessObject(Lead.class);
    }


    /**
     * Release the reference to the lead.
     */
    public synchronized void releaseLead() {
        releaseBusinessObject(Lead.class);
    }



	/**
	 * Releases references to CaptureConfig
	 */
	public synchronized void releaseCaptureConfig() {
		releaseBusinessObject(CapturerConfig.class);
	}
		
	/**
	 * Returns existing reference to CaptureConfig
	 * 
	 * @return reference to Captureconfig object, null if the object doesn't exist
	 */
	public synchronized CapturerConfig getCaptureConfig() {
	  	return  (CapturerConfig) getBusinessObject(CapturerConfig.class);
	}
	
	/**
	 * creates a new CaptureConfig
	 * 
	 * @return reference to Captureconfig object, null if the object can't be created
	 */
	public synchronized CapturerConfig createCaptureConfig() {
		return  (CapturerConfig) createBusinessObject(CapturerConfig.class);
	}	
	


	/**
	 * Creates a new helpValuesSearchHandler object. <br> 
	 * If such an object was already created, a reference to the old object is
	 * returned and no new object is created.
	 *
	 * @return referenc to a newly created or already existend 
	 *          HelpValuesSearchHandler object
	 */
	public synchronized HelpValuesSearchHandler createHelpValuesSearchHandler() {
		return(HelpValuesSearchHandler)createBusinessObject(HelpValuesSearchHandler.class);
	}
	
	/**
	 * Returns a reference to an existing HelpValuesSearchHandler object. <br>
	 *
	 * @return reference to HelpValuesSearchHandler object or null if no 
	 *          object is present
	 */
	public HelpValuesSearchHandler getHelpValuesSearchHandler() {
	  return createHelpValuesSearchHandler();
	}
	
	
	/**
	 * Release the references to created HelpValuesSearchHandler object.
	 */
	public synchronized void releaseHelpValuesSearchHandler() {
		releaseBusinessObject(HelpValuesSearchHandler.class);
	}
	

    /*
     * inner class to store bestseller in cache
     *
     */
    protected class BestsellerKey extends GenericCacheKey {

        public BestsellerKey(TechKey shop, String configuration) {
        	super(new Object[] {shop, configuration});
        }
    }

	/**
 	 * Creates a new <code>ShiptoSearch</code> object. If such an object was already
 	 * created, a reference to the old object is returned and no new object
 	 * is created.
 	 *
 	 * @return reference to a newly created or already existing
 	 *         <code>ShiptoSearch</code> object
 	 */
	public synchronized ShiptoSearch createShiptoSearch() {
		return (ShiptoSearch) createBusinessObject(ShiptoSearch.class);
	}

	/**
	 * Returns a reference to an existing <code>Search</code> object.
	 *
	 * @return reference to <code>Search</code> object or null if no
	 *         object is present
	 */
	public ShiptoSearch getShiptoSearch() {
		return (ShiptoSearch) getBusinessObject(ShiptoSearch.class);
	}
	
	/**
	 * Releases references to the search object.
	 */
	public synchronized void releaseShiptoSearch() {
		releaseBusinessObject(ShiptoSearch.class);
	}

}
