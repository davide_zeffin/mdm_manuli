/*****************************************************************************
    Class:        SalesDocumentStatus
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author        SAP
    Created:      March 2001

    $Revision: #9 $
    $Date: 2001/07/25 $
*****************************************************************************/

package com.sap.isa.businessobject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.sap.isa.backend.boi.isacore.BaseConfiguration;
import com.sap.isa.backend.boi.isacore.ConnectedDocumentItemData;
import com.sap.isa.backend.boi.isacore.SalesDocumentConfiguration;
import com.sap.isa.backend.boi.isacore.SalesDocumentStatusData;
import com.sap.isa.backend.boi.isacore.ShipToData;
import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.backend.boi.isacore.order.ItemDeliveryData;
import com.sap.isa.backend.boi.isacore.order.ItemListData;
import com.sap.isa.backend.boi.isacore.order.TextData;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.item.ItemDelivery;
import com.sap.isa.businessobject.item.ItemList;
import com.sap.isa.businessobject.item.ItemMap;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.businessobject.order.Text;
import com.sap.isa.businesspartner.businessobject.PartnerFunctionBase;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.eai.BackendObjectManager;

/**
 *
 * The SalesDocument class hold the detail data of an existing sales document.
 * used to retrieve detail status information of sales document
 * 
 * <b>Support of following object usage will be stopped with
 * Internet Sales Release 5.0</b>
 * To be called by action with 
 * <code>
 * Order order = new Order(); OrderStatus
 * orderStatus = bom.createOrderStatus(order);
 * </code>
 * Means, order is only used as a datacontainer, but not to communicate with backend.
 * Thats the part of the orderstatus object.
 *
 * <b>Use method <code>bom.createOrderStatus()</code> instead
 */
public class SalesDocumentStatus extends BusinessObjectBase
       implements BackendAware, SalesDocumentStatusData, DocumentState {

    /**
     * reference to the backend object manager
     */
    protected BackendObjectManager bem;
	protected HeaderSalesDocument singleOrderHeader;
	protected String salesDocNumber;
	protected String salesDocsOrigin;
    protected ItemMap itemMap = new ItemMap();
	protected ItemList itemList = new ItemList();
	protected Text text;
    protected int state = DocumentState.UNDEFINED;
	protected boolean contractDataExist = false;

	protected List shipToList = new ArrayList();

    protected SalesDocumentBase salesDocDataContainer = null;

	protected boolean documentExist = false;
	protected String[] requestedPartnerFunctions = new String[]{PartnerFunctionBase.SOLDTO};
    
    // Storage of IPC connection infos for config. Items
	// protected String  ipcHost       = null;
	// protected String  ipcPort       = null;
    // protected String  ipcSessionId  = null;
	// protected TechKey ipcDocumentId = null;
	    
	/**
	 * in case that for large doucments not all items of the doucment are read 
	 * into the itemList, this field holds the original no of items in this doucment.
	 * This means the number of items that were present when the document was first
	 * read. Adding new items or deleting old ones will not change this value.
	 */
	protected int noOfOriginalItems = NO_OF_ITEMS_UNKNOWN;
	
	/**
	 * if only certain items of the document should be shown or changed, this list is used, to store the guids of those items 
	 */
	protected ArrayList selectedItemGuids = new ArrayList();
	
	/**
	 * campaign related information
	 */
	protected HashMap campaignDescriptions = new HashMap();
	protected HashMap campaignTypeDescriptions = new HashMap();

    /**
     * Constructor
     */
    public SalesDocumentStatus() {
    }


	/**
	 * Get the Shop
	 */
	public SalesDocumentConfiguration getSalesDocumentConfiguration(){
			return null; 
	} 

	/**
	 * Adds a new ShipTo to the sales document
	 *
	 * @param shipTo The ship to to be added
	 */
	public void addShipTo(ShipToData shipTo) {
		shipToList.add(shipTo);
	}

	/**
	 * Clears the list of the ship tos. This method is used to release
	 * the state of the basket before rereading the data from the
	 * underlying storage.
	 */
	public void clearShipTos() {
		shipToList.clear();
	}
	
	/**
	 * Clears the item list.	 
	 */
	public void clearItems(){
		itemList.clear();
		itemMap.clear();
	}


	/**
	 * Returns an array containing all ship tos currently stored in the
	 * sales document.<br>
	 *
	 * @return Array containing <code>ShipTo</code> objects
	 */
	public ShipTo[] getShipTos() {
		ShipTo[] shipTos = new ShipTo[shipToList.size()];
		shipToList.toArray(shipTos);
		return shipTos;
	}


     /**
     * Gets the state of the document
     *
     * @return the state as described by the constants of interface DocumentState
     */
    public int getState() {
        return state;
    }

   /**
     * Sets the state of the document
     *
     * @param state the state as described by the constants of interface DocumentState
     */
    public void setState(int state) {
        this.state = state;
    }
    

    /**
     * Set a single order header (belonging to the items)
     */
    public void setOrderHeader(HeaderData header) {
        this.singleOrderHeader = (HeaderSalesDocument)header;
    }

    /**
     * Set flag if one of the items carries contract data
     * @see isContractAvailable
     */
    public void setContractDataExist(boolean flag) {
        this.contractDataExist = flag;
    }

    /**
     * Signals that at least one item carries contract data
     * @see setContractDataExist
     */
    public boolean isContractAvailable() {
        return contractDataExist;
    }

    /**
     * Get the sales document number (ID)
     */
    public String getSalesDocumentNumber() {
        return salesDocNumber;
    }

    /**
     * Set the Sales document number (ID)
     */
    public void setSalesDocumentNumber(String salesDocNo) {
        this.salesDocNumber = salesDocNo.trim();
    }

    /**
     * Get the sales documents origin, where the document has been created
     * (Necessary for the multiple action communication)
     */
    public String getSalesDocumentsOrigin() {
        return salesDocsOrigin;
    }

    /**
     * Set the sales documents origin, where the document has been created
     * (Necessary for the multiple action communication)
     */
    public void setSalesDocumentsOrigin(String salesDocsOrigin) {
        this.salesDocsOrigin =  salesDocsOrigin.trim();
    }

    /**
     * Get a new Header object
     */
    public HeaderData createHeader() {
        return (HeaderData)new HeaderSalesDocument();
    }
    /**
     * Get a new Item object
     */
    public ItemData createItem() {
        return (ItemData)new ItemSalesDoc();
    }


    /**
     * Adds a <code>Item</code> to the basket. This item must be uniquely
     * identified by a technical key.
     * A <code>null</code> reference passed to this function will be ignored,
     * in this case nothing will happen.
     *
     * @param item Item to be added to the basket
     */
    public void addItem(ItemData itemData) {
		final String METHOD = "addItem()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("itemData="+itemData);
	  if (itemData instanceof ItemSalesDoc) {
	    ItemSalesDoc item = (ItemSalesDoc)itemData;
	    if (item != null) {
	        itemMap.add(item);                        // for searching
	    	itemList.add(item);                       // for listing
	    }
	  }
	  else {
	  	 log.debug("Wrong type of item: " + itemData.getClass().getName());
	  }
	  log.exiting();
    }


    /**
     * Get ItemList
     */
     public ItemList getItemList() {
         return itemList;
     }


    /**
     * Returns the itemList as ItemListData
     * 
     * @return itemLIstData 
     */
     public ItemListData getItemListData() {
         return itemList;
     }


    /**
     * Get item by its techkey
     */
     public ItemSalesDoc getItemByTechKey(TechKey techKey) {
         return itemMap.get(techKey);
     }

    /**
     * Returns <tt>true</tt> if item exists
     *
     * @return <tt>true</tt> if item exists
     *
     * @param techKey items techKey whose presence in this Map is to be tested.
     */
     public boolean itemExists(TechKey techKey) {
         return itemMap.containsKey(techKey);
     }

    
    /**
     * Get a new itemDelivery object
     */
    public ItemDeliveryData createItemDelivery() {
      return (ItemDeliveryData)new ItemDelivery();
    }
    /**
     * Get a new text object
     */
    public TextData createText() {
      return (TextData)new Text();
    }
    /**
     * Get a new ShipTo Object
     */
    public ShipToData createShipTo() {
      return (ShipToData)new ShipTo();
    }

    /**
     * Set Flag whether a document exists or not
     */
     public void setDocumentExist(boolean x) {
         this.documentExist = x;
     }
     /**
      * Flag which is to check whether a document exists or not
      */
     public boolean isDocumentExistent() {
         return this.documentExist;
     }
     /**
      * Get single order header
      */
     public HeaderSalesDocument getOrderHeader() {
         return this.singleOrderHeader;
     }


    /**
     * Get single order headerData
     */
     public HeaderData getOrderHeaderData() {
	     return (HeaderData)this.singleOrderHeader;
     }


	/**
	 * Iterate over all items. <br>
	 * 
	 * @return iterator to iterate over all items.
	 * 
	 * @see com.sap.isa.backend.boi.isacore.SalesDocumentStatusData#getItemsIterator()
	 */
	public Iterator iterator() {
		return itemList.iterator();
	}


    public Iterator getItemsIterator() {
        return itemList.iterator();
    }
    
    
   /**
     * Sets the BackendObjectManager for the OrderList and OrderStatus. This method is used
     * by the object manager to allow the user object interaction with
     * the backend logic. This method is normally not called
     * by classes other than BusinessObjectManager.
     *
     * @param bem BackendObjectManager to be used
     */
    public void setBackendObjectManager(BackendObjectManager bem) {
        this.bem = bem;
    }


	/**
	 * Method enhanceConnectedDocument.
	 * 
	 * Set the Properties "docType" and "displayable" in the ConnectedDocument.
	 * 
	 */
	public void enhanceConnectedDocument() {
		final String METHOD = "enhanceConnectedDocument()";
		log.entering(METHOD);
		List predecessorList = this.getOrderHeader().getPredecessorList();
		
		if (!predecessorList.isEmpty()) {
			
			ConnectedDocument predecessor;
			Map predecessorMap = new HashMap(predecessorList.size());
			for (int i = 0; i < predecessorList.size(); i++) {
				predecessor = (ConnectedDocument)predecessorList.get(i);
				predecessorMap.put(predecessor.getTechKey(), predecessor);
			}
			
			ItemSalesDoc item = null;
			ItemList itemList = this.getItemList();
			
			for (int i = 0; i < itemList.size(); i++) {
				
				item = itemList.get(i);
				
				ConnectedDocumentItemData connectedItem = item.getPredecessor();
				if (connectedItem != null) {
					predecessor = (ConnectedDocument)predecessorMap.get(connectedItem.getDocumentKey());
					if (predecessor != null) {
						connectedItem.setDocType(predecessor.getDocType());
						connectedItem.setDisplayable(predecessor.isDisplayable());
					}
                }				
			}
						
		}
		log.exiting();
		
	}

	/**
	 * For large documents, where not all items are loaded into the itemlist
	 * this should return the total no of items, belonging to the document.
	 * This means the number of items that were present when the document was first
	 * read. Adding new items or deleting old ones will not change this value.
	 * 
	 * @return the number of items in the document or 0 if unknown
	 */
	public int getNoOfOriginalItems() {
		return noOfOriginalItems;
	}
	
	/**
	 * For large documents, where not all items are loaded at a time, this list
	 * contains all guids of items that should currently be loaded.
	 * 
	 * @return list of selected items
	 */
	public ArrayList getSelectedItemGuids() {
		return selectedItemGuids;
	}
	
	/**
	 * For large documents, where not all items are loaded into the itemlist
	 * the total no of items, belonging to the document can be set.
	 * This means the number of items that were present when the document was first
	 * read. Adding new items or deleting old ones will not change this value.
	 * 
	 * @param noOfItems the number of items in the document, or 0 if unknown
	 */
	public void setNoOfOriginalItems(int noOfOriginalItems) {
		this.noOfOriginalItems = noOfOriginalItems;
	}
	
	/**
	 * For large documents, where not all items are loaded at a time, this list
	 * contains all guids of items that should currently be loaded.
	 * 
	 * @param selectedItemGuids list of selected items
	 */
	public void setSelectedItemGuids(ArrayList selectedItemGuids) {
		this.selectedItemGuids = selectedItemGuids;
	}
		
	/**
	 * Returns true, if the document is considered to be a large
	 * document. This is the fact, if the value of noOfItems is
	 * greater or equal to shop.getLargeDocNoOfItemsThreshold().
	 * 
	 * @param largeDocNoOfRows number of rows from that a document is 
	 *        considered as large
	 * @return boolean true if document is a large document,
	 *         false else
	 */
	public boolean isLargeDocument(int largeDocNoOfRowsThres) {
		return largeDocNoOfRowsThres != BaseConfiguration.INFINITE_NO_OF_ITEMS && noOfOriginalItems != NO_OF_ITEMS_UNKNOWN && noOfOriginalItems > largeDocNoOfRowsThres;
	}
		
	/**
	 * Returns true for large documents, if only the header should
	 * be changed
	 * 
	 * @param largeDocNoOfRows number of rows from that a document is 
	 *         considered as large
	 * @return boolean true if document is a large document and only 
	 *         the header should be changed,
	 *         false else
	 */
	public boolean isHeaderChangeInLargeDocument(int largeDocNoOfRowsThres) {
			
		return isLargeDocument(largeDocNoOfRowsThres) && (selectedItemGuids.size() == 0);
	}
	
	/**
	 * Get the list of campaign descriptions. 
	 * The string value of the campaign guid b2b_40_SP_COR is the key and the description the value.
	 * 
	 * @return HashMap the map containing all campaign descriptions
	 */
	public HashMap getCampaignDescriptions() {
		return campaignDescriptions;
	}
	

	/**
	 * Get the list of campaign type descriptions. 
	 * The campaign type id is the key and the description the value.
	 * 
	 * @return HashMap the map containing all campaign type descriptions
	 */
	public HashMap getCampaignTypeDescriptions() {
		return campaignTypeDescriptions;
	}
	
	/**
	 * Set the list of campaign descriptions. 
	 * The campaign guid is the key and the description the value.
	 * 
	 * @param HashMap the map containing all campaign descriptions
	 */
	public void setCampaignDescriptions(HashMap campaignDescriptions) {
		this.campaignDescriptions = campaignDescriptions;
	}

	/**
	 * Set the list of campaign type descriptions. 
	 * The campaign type id is the key and the description the value.
	 * 
	 * @param HashMap the map containing all campaign type descriptions
	 */
	public void setCampaignTypeDescriptions(HashMap campaignTypeDescriptions) {
		this.campaignTypeDescriptions = campaignTypeDescriptions;
	}
    
    /**
     * Set the list of shiptos. 
     * 
     * @param List the list of shiptos
     */
    public void setShipTos(List shipToList) {
        this.shipToList = shipToList;
    }
    
	/**
	* Returns an array containing all ship tos currently stored in the
	* sales document.<br>
	*
	* @return Array containing <code>ShipTo</code> objects
	*/
	public List getShipToList() {
		return shipToList;   
	}
    
    /**
     * Returns the SalesDocument Data container. Reference is only available if
     * set by an sub class (e.g new OrderStatus(new Order()) -> salesDoc = order).
     */
    public SalesDocumentBase getSalesDocumentDataContainer() {
        return this.salesDocDataContainer;
    }
}
