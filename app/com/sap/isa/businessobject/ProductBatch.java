/*****************************************************************************
    Class:        ProductBatch
    Copyright (c) 2001, SAP AG, Germany, All rights reserved.
    Author:		  Daniel Seise
    Created:      07.12.2002
    Version:      1.0

    $Revision: #01 $
    $Date: 2002/12/07 $
*****************************************************************************/

package com.sap.isa.businessobject;

import com.sap.isa.backend.BackendTypeConstants;
import com.sap.isa.backend.boi.isacore.ProductBatchBackend;
import com.sap.isa.backend.boi.isacore.ProductBatchData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;


public class ProductBatch extends ProductBatchBase 
			implements BackendAware, ProductBatchData{
				
				
	protected ProductBatchBackend backendService = null;
	protected BackendObjectManager bem = null;

	/**
     * Retrieves the element information for a specific product from the backend system.
     * 
     * @param productGuid Techkey of the product 
     */
	public void readAllElements(TechKey productGuid) throws CommunicationException {

        // write some debugging info
		final String METHOD = "readAllElements()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("productGuid = "+productGuid);

        try {
            // get all items in Backend
            getBackendService().readAllElementsFromBackend(this, productGuid);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally{
        	log.exiting();
        }
    }
    
    /**
     * Retrieves the element information for a specific product from the backend system.
     * 
     * @param productGuid Techkey of the product 
     */
    public void readAllElementsForUpdate(TechKey productGuid)
                        throws CommunicationException {

		final String METHOD = "readAllElementsForUpdate()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("productGuid = "+productGuid);
        try {
            // read and lock from Backend
            getBackendService().readAllElementsFromBackend(this, productGuid);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
		finally{
		  log.exiting();
	  	}
    }
	
	/**
     * Sets the BackendObjectManager for the product object. This method is used
     * by the object manager to allow the user object interaction with
     * the backend logic. This method is normally not called
     * by classes other than BusinessObjectManager.
     *
     * @param bem BackendObjectManager to be used
     */
	public void setBackendObjectManager(BackendObjectManager bem) {
		this.bem = bem;
	}
	
	/**
     * Method retrieving the backend object for the object. This method is
     * abstract because every concrete subclass of
     * <code>ProductBatch</code> may use its own implementation
     * of a backend object.
     *
     * @return Backend object to be used
     */
	private ProductBatchBackend getBackendService()
            throws BackendException{

        synchronized (this) {
            if (backendService == null) {
                backendService = (ProductBatchBackend)
                        bem.createBackendBusinessObject(BackendTypeConstants.BO_TYPE_PRODUCT_BATCH);
            }
        }

        return backendService;
    }

	/**
     * Returns a string representation of the object
     *
     * @return String representation
     */
	public String toString() {
        return this.getClass().getName() + " [techkey=\"" + techKey 
             + ", elements=" + batchListDB + "]";
    }
	
	
	/**
     * Callback method for the <code>BusinessObjectManager</code> to tell
     * the object that life is over and that it has to release
     * all ressources.
     */
	public void destroy() {
        super.destroy();
    }
    

}
