/*****************************************************************************
    Class:        NegotiatedContract
    Copyright (c) 2002, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      15.10.2002
    Version:      1.0

    $Revision: $
    $Date:  $
*****************************************************************************/

package com.sap.isa.businessobject.negotiatedcontract;

import com.sap.isa.backend.BackendTypeConstants;
import com.sap.isa.backend.boi.isacore.SalesDocumentBackend;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.negotiatedcontract.NegotiatedContractBackend;
import com.sap.isa.backend.boi.isacore.negotiatedcontract.NegotiatedContractData;
import com.sap.isa.backend.boi.isacore.order.HeaderBaseData;
import com.sap.isa.backend.boi.isacore.order.ItemBaseData;
import com.sap.isa.businessobject.BusinessObjectHelper;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.businessevent.CreateDocumentEvent;
import com.sap.isa.businessobject.header.HeaderBase;
import com.sap.isa.businessobject.header.HeaderNegotiatedContract;
import com.sap.isa.businessobject.item.BasketTransferItem;
import com.sap.isa.businessobject.item.ItemBase;
import com.sap.isa.businessobject.item.ItemNegotiatedContract;
import com.sap.isa.businessobject.order.PartnerList;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
/**
 * Representation of a negotiated contract.
 *
 *
 * @author SAP AG
 * @version 1.0
 */

 public class NegotiatedContract extends SalesDocument implements NegotiatedContractData {

  //  private static final String CONDITION_TYPE = "ConditionType";
 
    public NegotiatedContract()  throws CommunicationException{

    }
  
    /**
     * Adds a item coming from the outside world to the sales document. This
     * item is represented by a special crafted transfer
     * object.
     *
     * @param transferItm the Item to be added
     */
    public void addTransferItem(BasketTransferItem transferItm,
                                 String reqDeliverDate) {
        ItemNegotiatedContract itm = new ItemNegotiatedContract(transferItm);
        itm.setReqDeliveryDate(reqDeliverDate);
        addItem(itm);
    }

	/* The second implementation of this method is needed since Lean Order Implementation in CRM 7.0
	 * see extended siganture
	 */
	public void addTransferItem(BasketTransferItem transferItm, String reqDeliverDate, TechKey productKey) {		
		this.addTransferItem(transferItm, reqDeliverDate);
	}   
    
     public void update(BusinessPartnerManager bupama ) throws CommunicationException {
		final String METHOD = "update()";
		log.entering(METHOD);
//        checkDocumentEvent();
        checkBusinessPartners(bupama);
 
     	try {
        // Todo: Fehlerbehandlung
 	     ((NegotiatedContractBackend)backendService).updateInBackend(this, null);
 	     isDirty = true;
 	     header.setDirty(true);
       	}
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }  finally {
        	log.exiting();     
        }
    } 

    public void update(Shop shop ) throws BackendException {
		final String METHOD = "update()";
		log.entering(METHOD);
        // write some debugging info
        if (log.isDebugEnabled()) {
            log.debug("update(): shop = "+ shop);
        }
        // Todo: Fehlerbehandlung
	      ((NegotiatedContractBackend)backendService).updateInBackend(this, shop);
	      isDirty = true;
	      header.setDirty(true);
      	log.exiting();
    }


    /**
     * needs shop and processtype to decide
     */
    public void save(int processtype, Shop shop) throws CommunicationException {
		final String METHOD = "save()";
		log.entering(METHOD);
        // write some debugging info
        if (log.isDebugEnabled()) {
            log.debug("save(): processtype = "+processtype+ ", shop = "+shop);
        }
        try {
              ((NegotiatedContractBackend)getBackendService()).saveInBackend(this, processtype, shop);
              isDirty = true;
              header.setDirty(true);
        } catch (BackendException ex) {
               BusinessObjectHelper.splitException(ex);
        }
    }


    /**
     * Reads the negotiated contract data from the underlying storage for the special
     * case of a change of the object.
     */
    public void readForUpdate()
                        throws CommunicationException {

		final String METHOD = "readForUpdate()";
		log.entering(METHOD);

        try {
            // read and lock from Backend
            if (header.isDirty()) {
                ((NegotiatedContractBackend) getBackendService()).readHeaderFromBackend(this, true);
                 header.setDirty(false);
            }
            if (isDirty) {
                ((NegotiatedContractBackend) getBackendService()).readAllItemsFromBackend(this, true);
                isDirty = false;
            }
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        } finally {
        	log.exiting();
        }
    }

    /**
     * Sets the necessary dirty flags in order to be able to read the contract from the
     * backend.
     */
    public void setDirty()
                        throws CommunicationException {

        // write some debugging info
		final String METHOD = "setDirty()";
		log.entering(METHOD);
        header.setDirty(true);
        isDirty = true;
		log.exiting();

    }

    /**
     * Retrieves the header from the backend and stores the data
     * in this object.
     */
    public void readHeader() throws CommunicationException {

		final String METHOD = "readHeader()";
		log.entering(METHOD);

        try {
 
            // read from Backend
            if (header.isDirty() == true) {
                ((NegotiatedContractBackend) getBackendService()).readHeaderFromBackend(this, false);
                 header.setDirty(false);
            }        
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        } finally {
        	log.exiting();
        }
    }

    /**
     * Retrieves the items from the backend and stores the data
     * in this object.
     */
    public void readAllItems() throws CommunicationException {

		final String METHOD = "readAllItems()";
		log.entering(METHOD);
        try {
            // read items from Backend
            if (isDirty == true) {
                ((NegotiatedContractBackend) getBackendService()).readAllItemsFromBackend(this, false);
                isDirty = false;
            }            
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        } finally {
        	log.exiting();
        }
    }
    
    /**
     * Reads the preOrderSalesDocument data from the underlying storage
     * checking the changeability of the fields.
     * <b>As a side effect of this method the shiptos are also read. This
     * is not supposed to be the right behaviour and is left here only to keep
     * the clients of this class working. All clients should explicitly call
     * the <code>readShipTos()</code> method and not rely
     * on this - we are going to change this soon.</b>
     */
    public void readAllItemsForUpdate()
                        throws CommunicationException {

		final String METHOD = "readAllItemsForUpdate()";
		log.entering(METHOD);

        try {
            // read and lock from Backend
            //getBackendService().readHeaderFromBackend(this,
            //                                   true);
            // this call has to be discussed for the new shipto concept
            // it will be better to remove this call here and call the
            // method to get the shiptos explicitly in the actions
            // getBackendService().readShipTosFromBackend(this, user);
            if (isDirty) {
                 
                 ((NegotiatedContractBackend)getBackendService()).readAllItemsFromBackend(this, true);
                 isDirty = false;
            }
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        } finally {
        	log.exiting();
        }
    }  

   /**
     * Releases the lock of the negotiated contract in the backend.
     *
     */
    public void dequeue() throws CommunicationException {

		final String METHOD = "dequeue()";
		log.entering(METHOD);

        try {
            ((NegotiatedContractBackend) getBackendService()).dequeueInBackend(this);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        } finally {
        	log.exiting();
        }
    }



     /**
     * Method retrieving the backend object for the object.
     *
     * @return Backend object to be used
     */
    protected SalesDocumentBackend getBackendService()
            throws BackendException {

        synchronized (this) {
            if (backendService == null) {
                backendService = (NegotiatedContractBackend)bem.createBackendBusinessObject(BackendTypeConstants.BO_TYPE_NEGOTIATEDCONTRACT, null);
            }

        }

        return backendService;
    }


	/**
	 * Only in this method should be created the header object which is to 
	 * use in the sales document. Overwrite this method if you want other 
	 * header as {@link com.sap.isa.businessobject.header.HeaderNegotiatedContract} 
	 * 
	 * @return header as HeaderBase obejct
	 */
	protected HeaderBase createHeaderInstance() {
		return new HeaderNegotiatedContract();		
	}

	/**
	 * Only in this method should be created an item object, which is to 
	 * use in the sales document. Overwrite this method if you want other 
	 * item as {@link com.sap.isa.businessobject.item.ItemNegotiatedContract} 
	 * 
	 * @return header as HeaderBase obejct
	 */
	protected ItemBase createItemInstance() {
		return new ItemNegotiatedContract();		
	}


	/**
	 * Check if the header is an instance of the correct class. <br> 
	 * Overwrite this method if you want to use an other 
	 * header as {@link com.sap.isa.businessobject.header.HeaderNegotiatedContract} 
	 * 
	 * @return <code>true</code> if the header is instance of the correct class
	 */
	protected boolean checkHeaderInstance(HeaderBaseData headerBaseData) {

	   if (!(headerBaseData instanceof HeaderNegotiatedContract)) {
		   throw (new PanicException ("Wrong type of header:" 
			   + headerBaseData.getClass().getName()));
	   }

	   return true;
	 }


	/**
	 * Check if the item is an instance of the correct class. <br> 
	 * Overwrite this method if you want to use an other 
	 * item as {@link com.sap.isa.businessobject.item.ItemNegotiatedContract} 
	 * 
	 * @return <code>true</code> if the item is instance of the correct class
	 */
	protected boolean checkItemInstance(ItemBaseData itemBaseData) {
		if (!(itemBaseData instanceof ItemNegotiatedContract)) {
			throw (new PanicException ("Wrong type of header:" 
				+ itemBaseData.getClass().getName()));
		}

		return true;
	}

 
    /**
     * Initializes the object according to the given shop. The state of
     * the document is dropped as an result of calling this method and
     * after that the necessary backend operations are performed to
     * create a representation of the document in the underlying storage.
     *
     * @param shop Shop used for this basket
     * @param bupama the business partner manager
     * @param partnerList list of business partners for the document
     * @param salesDocWebCat the actual catalog
     */
     public void init(Shop shop,BusinessPartnerManager bupama,
                      PartnerList partnerList, WebCatInfo salesDocWebCat, String processType) throws CommunicationException {
		
		final String METHOD = "init()";
		log.entering(METHOD);
        // write some debugging info
        if (log.isDebugEnabled()) {
            log.debug("init(): shop = " + shop + " bupama: " + bupama);
        }
		try {
		
	        if (salesDocWebCat == null && shop.isInternalCatalogAvailable()) {
	            throw( new IllegalArgumentException("Paramter salesDocWebCat can not be null!"));
	        }
	
	        clearData();
	
	        header.setPartnerList(partnerList);
	
	        // check for valid entries
	        if (bupama != null) {
	            checkBusinessPartners(bupama);
	        }
	
	        synchronized (this) {
	            // remember the WebCatalog, because it may be needed
	            // by populateItemsFromCatalog()
	            this.salesDocWebCat = salesDocWebCat;
	
	            if (!alreadyInitialized) {
	                try {
	                    alreadyInitialized = true;
	                    ((NegotiatedContractBackend)getBackendService()).createInBackend(shop, this, processType);
	                }
	                catch (BackendException ex) {
	                    BusinessObjectHelper.splitException(ex);
	                }
	
	                if (businessEventHandler != null) {
	                    CreateDocumentEvent event = new CreateDocumentEvent(this);
	                    businessEventHandler.fireCreateDocumentEvent(event);
	                }
	            }
	        }
		} finally {
			log.exiting();
		}
    }


    /**
     * Set global data in the backend
     *
     * @return <true> if check was successful; otherwise <code>false</code>
     */
    public void setGData(ShopData shop, WebCatInfo salesDocWebCat) throws CommunicationException {
		final String METHOD = "setGData()";
		log.entering(METHOD);
        // write some debugging info
        if (log.isDebugEnabled()) {
            log.debug("setGData(): shop = " + shop + ", salesDocWebCat = " + salesDocWebCat);
        }
		try {
		
	        if (salesDocWebCat == null && shop.isInternalCatalogAvailable()) {
	            throw( new IllegalArgumentException("Parameter salesDocWebCat can not be null!"));
	        }
	        this.salesDocWebCat = salesDocWebCat;
	
	        try {   
	            ((NegotiatedContractBackend)getBackendService()).setGData(this, shop);
	            isDirty = true;
	            header.setDirty(true);
	        }
	        catch (BackendException ex) {
	            BusinessObjectHelper.splitException(ex);
	        }
		} finally {
			log.exiting();
		}
    }
    
    /**
     * Method retrieving the backend object for the object.
     *
     * @return Backend object to be used
     */
    public void setStatusInquiry()
            throws CommunicationException {
		final String METHOD = "setStatusInquiry()";
		log.entering(METHOD);
        try {   
            ((NegotiatedContractBackend)getBackendService()).setStatusInquiry(this);
            isDirty = true;
            header.setDirty(true);
        }
        catch (BackendException ex) {
               BusinessObjectHelper.splitException(ex);
        } finally {
        	log.exiting();
        }
       
    }    
    

}
