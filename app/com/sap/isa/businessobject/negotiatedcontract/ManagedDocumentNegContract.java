package com.sap.isa.businessobject.negotiatedcontract;

import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.isacore.ManagedDocumentLargeDoc;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public class ManagedDocumentNegContract extends ManagedDocumentLargeDoc {

    /**
     *  Marks if the NegotiatedContract is displayed in changeMode, which means,
     *  that input fields will be generated, or in display mode.
     */
    protected boolean changeMode = false;

    public void setChangeMode(boolean mode) {
        changeMode = mode;
    }

    public boolean getChangeMode() {
        return changeMode;
    }


    /**
     *  Constructor
     *
     *@param  doc            Reference off the corresponding business object.
     *@param  docType        Type of the document, e.g. "order", "quotation", "order template".
     *@param  docNumber      Number of the document, given by the backend.
     *@param  refNumber      The reference number; the number the customer can assign to the document.
     *@param  refName        The reference name; the name the customer can assign to the document.
     *@param  docDate        Date of the document.
     *@param  forward        String for the logical forward mapping in the struts framework.
     *@param  href           Href for displaying the document
     *@param  hrefParameter  Additional parameters for the String href.
     */
    public ManagedDocumentNegContract(DocumentState doc, String docType, String docNumber, String refNumber,
            String refName, String docDate, String forward, String href, String hrefParameter) {

            super(doc, docType, docNumber, refNumber, refName, docDate, forward, href, hrefParameter);
    }
}
