/*****************************************************************************
    Interface:    ProductHolder
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Thomas Smits
    Created:      5.7.2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/07/05 $
*****************************************************************************/

package com.sap.isa.businessobject;

import com.sap.isa.core.TechKey;

/**
 * Interface for access to additional product information.
 *
 * @author Thomas Smits
 * @version 1.0
 */
public interface ProductHolder {

    /**
     * Get additional product information provided by the catalog.
     *
     * @return the product information
     */
    public Product getCatalogProduct();

    /**
     * Set the additional product information provided by the catalog.
     *
     * @param catlogProduct the product information to be set
     */
    public void setCatalogProduct(Product catalogProduct);

    /**
     * Returns a technical key for the product.
     *
     * @return technical key
     */
    public TechKey getProductId();
    
    /**
     * Checks if the items is a points item of a reward area
     * 
     * @return isPtsItem flag as boolean
     */
    public boolean isPtsItem();

    /**
     * Checks if the items is a buy points item of a buy points area
     * 
     * @return isBuyPtsItem flag as boolean
     */
    public boolean isBuyPtsItem();

}