package com.sap.isa.businessobject;

import com.sap.isa.backend.boi.isacore.ShiptoSearchBackend;
import com.sap.isa.backend.boi.isacore.ShiptoSearchData;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.core.util.table.ResultData;


public class ShiptoSearch extends BusinessObjectBase implements BackendAware, ShiptoSearchData {

  public static final String SHIPTO_SEARCH = "ShiptoSearch";
  private ShiptoSearchBackend backendService;
  private BackendObjectManager bem;

  public ShiptoSearch() {
  }
    /**
     * Performs the search operation specified by the given command object and
     * returns the result. To create a new search operation, you have to write
     * your own class implementing the <code>SearchCommand</code> interface
     * and change the implementation of this method, to handle this
     * new kind of search reqeust.
     *
     * @param cmd Command describing what to search
     * @return Result of the search operation
     */
    public ResultData searchShipto(SearchCommand cmd)
            throws CommunicationException {


        try {

            if (cmd instanceof SearchShiptoCommand) {
                return  getBackendService().searchShipto(this, (SearchShiptoCommand) cmd);
            }

            else {
                throw new CommunicationException("Search command unknown");
            }
        }
        catch (BackendException e) {
//           if (Debug.DO_DEBUGGING) Debug.print(e);
              BusinessObjectHelper.splitException(e);
              return null; // never reached
       }

    }

	/**
	 * Performs the search operation specified by the given command object and the 
	 * shop object.  
	 * It returns the search result. To create a new search operation, you have 
	 * to write your own class implementing the <code>SearchCommand</code> 
	 * interface and change the implementation of this method, to handle this
	 * new kind of search reqeust.
	 *
	 * @param cmd Command describing what to search
	 * @return Result of the search operation
	 */
	public ResultData searchShipto(SearchCommand cmd, ShopData shop)
			throws CommunicationException {


		try {

			if (cmd instanceof SearchShiptoCommand) {
				return  getBackendService().searchShipto(this, (SearchShiptoCommand) cmd, shop);
			}

			else {
				throw new CommunicationException("Search command unknown");
			}
		}
		catch (BackendException e) {
//		   if (Debug.DO_DEBUGGING) Debug.print(e);
			  BusinessObjectHelper.splitException(e);
			  return null; // never reached
	   }

	}


   /**
     * Method the object manager calls after a new object is created.
     *
     * @param bom Reference to the BackendObjectManager object
     */
    public void setBackendObjectManager(BackendObjectManager bem) {
        this.bem = bem;
    }

    /**
     * get the reference of the user backend object
     *
     */
    private ShiptoSearchBackend getBackendService ()
            throws BackendException {

        if (backendService == null) {
            backendService = (ShiptoSearchBackend) bem.createBackendBusinessObject(SHIPTO_SEARCH, null);
        }
        return backendService;
    }

}