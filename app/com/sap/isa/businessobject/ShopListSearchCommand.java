/*****************************************************************************
    Class:        ShopListSearchCommand
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Thomas Smits
    Created:      2.4.2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/06/26 $
*****************************************************************************/

package com.sap.isa.businessobject;

import com.sap.isa.backend.BackendTypeConstants;
import com.sap.isa.backend.boi.isacore.ShopBackend;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.CacheManager;
import com.sap.isa.core.util.table.Table;


/**
 * Defines the search command necessary to retrieve a list of shops
 * available.
 *
 * @author Thomas Smits
 * @version 1.0
 */
public class ShopListSearchCommand implements SearchCommand {

    private static final String CACHE_SHOPLIST = "Shoplist";
	static final private IsaLocation loc = IsaLocation.getInstance(ShopListSearchCommand.class.getName());

    static {
        CacheManager.addCache(CACHE_SHOPLIST, 2);
    }


    /**
     * Already created object for the search for shoplists in
     * the B2B scenario.
     */
    public static final ShopListSearchCommand B2B_SEARCH =
            new ShopListSearchCommand(Shop.B2B,"","");

    /**
     * Already created object for the search for shoplists in
     * the B2C scenario.
     */
    public static final ShopListSearchCommand B2C_SEARCH =
            new ShopListSearchCommand(Shop.B2C,"","");

    private String scenario;
    private String application = "";
    private String language = "";
    private String configuration;

    /**
     * Creates a new search command for the given scenario.
     *
     * @param scenario String specifying the scenario. See constants
     *                 in <code>Shop</code> for the possible values.
     */
    public ShopListSearchCommand(String scenario, String language, String configuration) {
        this.scenario = scenario;
        setLanguage(language);
        this.configuration = configuration;
    }

    /**
     * Creates a new search command for the given scenario.
     *
     * @param scenario String specifying the scenario. See constants
     *                 in <code>Shop</code> for the possible values.
     */
   	public ShopListSearchCommand(String application,
   								 String scenario, 
    						     String language, 
    						     String configuration) {
    						         
    	this.application = application;					         
        this.scenario = scenario;
        setLanguage(language);
        this.configuration = configuration;
    }


    /**
     * Returns the application.
     * @return String
     */
    public String getApplication() {
        return application;
    }

    /**
     * Sets the application.
     * @param application The application to set
     */
    public void setApplication(String application) {
        this.application = application;
    }

    /**
     * Returns the scenario, this search is performed for.
     *
     * @return String specifying the scenario
     */
    public String getScenario() {
        return scenario;
    }

    /**
     * Set the property language
     *
     * @param language
     *
     */
    public void setLanguage(String language) {
        if (language != null) {
            this.language = language;
        }
    }

    /**
     * Returns the property language
     *
     * @return language
     *
     */
    public String getLanguage() {
       return this.language;
    }

    /**
     * Returns the hash code for this object.
     *
     * @return Hash code for the object
     */
    public int hashCode() {
        return scenario.hashCode()^
               language.hashCode()^
               configuration.hashCode();
    }

    /**
     * Returns true if this object is equal to the provided one.
     *
     * @param o Object to compare this with
     * @return <code>true</code> if both objects are equal,
     *         <code>false</code> if not.
     */
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        else if (o == this) {
                return true;
        }
        else if (!(o instanceof ShopListSearchCommand)) {
            return false;
        }
        else {
            ShopListSearchCommand command = (ShopListSearchCommand) o;
            return scenario.equals(command.scenario)
                   & language.equals(command.language)
                   & configuration.equals(command.configuration);
        }
    }

    
    /**
     * Perform the search in the backend.
     * 
     * @param bem With the backend object manager the search command get 
     *             access to the backend.
     * @return SearchResult 
     * @throws CommunicationException
     */
    public SearchResult performSearch(BackendObjectManager bem)
        	throws CommunicationException {
		final String METHOD = "performSearch()";
		loc.entering(METHOD); 
        SearchResult result = (SearchResult) CacheManager.get(CACHE_SHOPLIST, this);
		try {
		
	        if (result == null) {
	            try {
	                ShopBackend sbe =
	                        (ShopBackend) bem.createBackendBusinessObject(BackendTypeConstants.BO_TYPE_SHOP, null);
	
	                Table resultTable =
	                    sbe.readShopList(this.getScenario());
	
	                result = new SearchResult(resultTable, null);
	
	                CacheManager.put(CACHE_SHOPLIST, this, result);
	            }
	            catch (BackendException e) {
	                BusinessObjectHelper.splitException(e);
	                return null; // never reached
	            }
	        }
		} finally {
			loc.exiting();
		}
        return result;
            
 	}
   
}