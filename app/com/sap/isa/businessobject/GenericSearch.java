/*****************************************************************************
    Class:        GenericSearch
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
*****************************************************************************/

package com.sap.isa.businessobject;


/**
 * This class is a extension for class Search supporting the GenericSearch
 * Framework concept. 
 *
 */
public class GenericSearch extends Search 
                           implements DocumentState{
    protected int state = DocumentState.UNDEFINED;
	/**
	 * Performs the search operation specified by the given command object and
	 * returns the result. To create a new search operation, you have to write
	 * your own class implementing the <code>GenericSearchCommand</code> interface.
	 *
	 * @param cmd Command describing what to search
	 * @return Result of the search operation
	 */
	public GenericSearchReturn performGenericSearch(GenericSearchCommand cmd)
			throws CommunicationException {

		return cmd.performGenericSearch(bem);
	}
    /**
    * Gets the state of the document
    *
    * @return the state as described by the constants of interface DocumentState
    */
   public int getState() {
       return state;
   }
   /**
     * Sets the state of the document
     *
     * @param state the state as described by the constants of interface DocumentState
     */
    public void setState(int state) {
        this.state = state;
    }

}