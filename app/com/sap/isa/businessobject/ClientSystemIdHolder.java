/*****************************************************************************
    Copyright (c) 2000, SAPLAbs Europe GmbH, Germany, All rights reserved.
*****************************************************************************/
package com.sap.isa.businessobject;

import com.sap.isa.backend.BackendTypeConstants;
import com.sap.isa.backend.boi.isacore.ServiceBackend;
import com.sap.isa.backend.boi.isacore.UserBackend;
import com.sap.isa.backend.boi.isacore.order.OrderBackend;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;

/**
 * This is used to convert the order object into PDF format.
 * 
 */
public class ClientSystemIdHolder extends BusinessObjectBase
implements BackendAware,
    BackendBusinessObjectParams {

    protected BackendObjectManager bem;
	ServiceBackend  service = null;
    
    /**
     * Constructor.
     */
    public ClientSystemIdHolder() {
        super();
    }
    
    /**
     * Sets the BackendObjectManager for the attribute set. This method is used
     * by the object manager to allow the user object interaction with
     * the backend logic. This method is normally not called
     * by classes other than BusinessObjectManager.
     *
     * @param bem BackendObjectManager to be used
     */
    public void setBackendObjectManager(BackendObjectManager bem) {
        this.bem = bem;
    }
    
	/**
	 * retrieve a reference to the ServiceBackend-object
	 * 
	 * @return ServiceBackend
	 */
	public ServiceBackend getServiceBackend() throws CommunicationException {
		UserBackend backend = null;
		if (null == service) {
			try {
				backend = (UserBackend)
				bem.createBackendBusinessObject(BackendTypeConstants.BO_TYPE_USER, null);
			} catch (BackendException e) {
				BusinessObjectHelper.splitException(e);
			}
			service = (ServiceBackend) backend.getContext().getAttribute(ServiceBackend.TYPE);
			if (null == service) {
				if (log.isDebugEnabled()) {
					log.debug("No ServiceBackend available.");
				}
			}
		}

		return service;
	}    

 	public String getClient() throws CommunicationException{
 		if(getServiceBackend()!=null)
 			return getServiceBackend().getClient();
 		return null;
 	}
	
	public String getSystemId() throws CommunicationException{
		if(getServiceBackend()!=null)
			return getServiceBackend().getSystemId();
		return null;
	} 	
}
