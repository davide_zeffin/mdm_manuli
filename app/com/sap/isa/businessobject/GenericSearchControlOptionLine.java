/*****************************************************************************
    Class:        GenericSearchControlOptionLine
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
*****************************************************************************/
package com.sap.isa.businessobject;

import com.sap.isa.backend.boi.appbase.GenericSearchControlOptionLineData;

/**
 * One Single control option line 
 * <p>
 * Part of the generic search framework.
 * </p>
 *
 * @author SAP AG
 * @version 1.0
 *
 */

public class GenericSearchControlOptionLine  
    implements GenericSearchControlOptionLineData {
	
	protected int    handle = 0;
	protected boolean countOnly = false;
	protected int maxHits = 0;                    // 0 = no restriction
	
    /**
     * Constructor with all attributes
     */
    public GenericSearchControlOptionLine(
               int      handle,
               boolean countOnly,
               int     maxHits) {
        this.handle           = handle;
        this.countOnly        = countOnly;
        this.maxHits          = maxHits; 
    }

    /**
     * Returns the control option CountOnly
     * @return boolean count only flag
     */
    public boolean isCountOnly() {
        return countOnly;
    }

    /**
     * Returns the control option MaxHits
     * @return int number of maximum select hits
     */
    public int getMaxHits() {
        return maxHits;
    }

    /**
     * Sets the control option CountrOnly
     * @param boolean count only flag
     */
    public void setCountOnly(boolean b) {
        countOnly = b;
    }

    /**
     * Sets the control option MaxHits
     * @param int number of maximum select hits
     */
    public void setMaxHits(int i) {
        maxHits = i;
    }

    /**
     * Returns the handle of the control option line
     * @return int Handle
     */
    public int getHandle() {
        return handle;
    }

    /**
     * Sets the handle of the control option line
     * @param int Handle
     */
    public void setHandle(int i) {
        handle = i;
    }

    /**
     * To string method of this object
     * @return String representing this object
     */
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("\nHandle: " + handle + ", ");
        sb.append("Count Only: " + countOnly + ", ");
        sb.append("Max Hits: " + maxHits);

        return sb.toString();
    }

}
