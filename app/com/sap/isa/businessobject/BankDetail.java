package com.sap.isa.businessobject;

/*****************************************************************************
    Class:        BankDetail
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author        Jochen Schmitt
    @version      0.1
    Created:      03 Mai 2001

*****************************************************************************/

import com.sap.isa.backend.boi.isacore.BankDetailData;

/**
 * Class representing a bankdetail object (which holds bank account data).
 * This is used by the business agreement.
 */
public class BankDetail extends BusinessObjectBase implements BankDetailData {

  private String country;
  private String countryISO;
  private String bankKey;
  private String accountNo;
  private String bankRef;
  private String accountHolder;
  private boolean collAuth;
  private String externalBankID;

  public BankDetail() {
  }
}