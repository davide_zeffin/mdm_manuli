/*****************************************************************************
    Class:        ConnectedDocument
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Created:      May 2002

    $Revision: #0 $
    $Date: 2002/05/28 $
*****************************************************************************/
package com.sap.isa.businessobject;

import com.sap.isa.backend.boi.isacore.ConnectedDocumentData;

/**
 * The ConnectedDocument represents an entry in the document flow.
 *
 * @see com.sap.isa.businessobject.ConnectedObject
 */
public class ConnectedDocument
	extends ConnectedObject
	implements ConnectedDocumentData {

	protected String refGuid = "";
	protected String appTyp = "";
    protected String docOrigin = "";
	
	/**
		* Returns the document number
		*
		* @return document number is used to identify the document in the backend
		*/
	public String getRefGuid() {
		return refGuid;
	}

	/**
		* Sets the document number
		*/
	public void setRefGuid(String refGuid) {
		this.refGuid = refGuid;
	}
	
	        /**
			* Returns the document number
			*
			* @return document type is used to identify the  backend of the document 
			* (one order document (1) or from CRM billing (B)
			*/
		public String getAppTyp() {
			return appTyp;
		}

		/**
			* Sets the document typ
			* used to identify the  backend of the document 
			* (one order document or from CRM billing)
			*/
		public void setAppTyp(String appTyp) {
			this.appTyp = appTyp;
		}
        /**
         * Set the origin (e.g RFC destination) of a document
         * @param String containg a documents origin
         */
        public void setDocumentsOrigin(String docOrigin) {
            this.docOrigin = docOrigin;
        }
        /**
         * Returns the origin (e.g RFC destination) of a document
         * @return String containg a documents origin
         */
        public String getDocumentsOrigin() {
            return this.docOrigin;
        }
        
}
