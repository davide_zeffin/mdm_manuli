/*****************************************************************************
	Class:        IdMapper
	Copyright (c) 2008, SAP AG, Germany, All rights reserved.
	Author:       SAP AG
	Created:      February 2008
	Version:      1.0
*****************************************************************************/

package com.sap.isa.businessobject;


import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.eai.*;
import com.sap.isa.backend.boi.appbase.IdMapperBackend;
import com.sap.isa.backend.boi.appbase.IdMapperData;
 


   /**
    * This class could be used to map the identification of an object in one context 
    * to the identification of the object in another context.
    * 
    * For example the business partner techkey could be in one context the business partner id
    * but in the other context it could be the business partner GUID. 
    * 
    * The object type, target and source context are constants that has to be defined in 
    * the interface class <code> IdMapperData </code>.
    * 
    * The real mapping has to be done in the appropriate IdMapper backend business object.
    * Here it must be decided if a mapping is necessary or not.
    * 
    * Currently the mapping of identifiers is done in the following actions
    * 
    * - ReadSoldToAction  ( sold to party id is mapped to marketing user id)
    * - AddToBasketAction (catalog product id is mapped to basket/document product id)
    * - ISAEntryAction    (soldto id is mapped to soldto id needed for catalog pricing)
    * - ShowCUAAction     (product id in basket is mapped to product id in catalog)
    * 
    */
   public class IdMapper extends BusinessObjectBase implements BackendAware, IdMapperData {


    // backend regocnition
	protected BackendObjectManager bem;
	protected BackendBusinessObject backendService;

		

		
	/**
	 * Get the identifier of the target context for the given id of the source context 
	 * for the object specified by type.
	 * 
	 *@param specifying the object to get the identifier for
	 *@param the source context
	 *@param the target context 
	 *@param the object identifier in the source context
	 *
	 *@result the object identifier in the target context
	 */	
	public String getIdentifier(String type, String source, String target, String sourceId)
	     throws CommunicationException  {
	     	
	   String targetId = null;
	   
	   if (type == null || source == null || target == null || sourceId == null) {
		 return sourceId;  
	   }
        
		// get Identifier from backend
		try {
		  targetId = getBackendService().getIdentifier(this, type, source, target, sourceId); 
		  
		  if (targetId == null) {
		  	throw new CommunicationException(IdMapperData.EX_MSG_ID_NULL);
		  }
		}
		catch (BackendException ex) {
			BusinessObjectHelper.splitException(ex);	
		}   
		
		return targetId;          
	}	
		
				
	/**
	 * Set the identifier of the target context for the identifier of the source context 
	 * for the object specified by type.
	 * 
	 *@param specifying the object to set the identifier for
	 *@param the source context
	 *@param the target context 
	 *@param the object identifier in the source context
	 *@param the object identifier in the target context
	 * 
	 */			
	public void setIdentifier(String type, String source, String target, String sourceId, String targetId)
	   throws CommunicationException {
			
		if (type == null || source == null || target == null) {
			 return;  
		}
		
		//set Identifier in backend
		try {
		getBackendService().setIdentifier(this, type, source, target, sourceId, targetId);             
		}
		catch (BackendException ex) {
			BusinessObjectHelper.splitException(ex);	
		}
	}					


	/**
	 * Map the identifier of the source context to the given identifier of the target context 
	 * for the object specified by type.
	 * The method could be used to set and get the target identifier in one step.
	 * The method calls at first the <code> setIdentifier(String type, String source,
	 *    String target, String sourceId, String targetId) </code> to set the identifier mapping 
	 * for later accesses and afterwards calls the method <code> getIdentifier(String type, String source,
	 *    String target, String sourceId) </code> to return the mapped target context identifier. 
	 * 
	 *@param specifying the object to get the identifier for
	 *@param the source context
	 *@param the target context 
	 *@param the object identifier in the source context
	 *@param the object identifier in the target context
	 *
	 *@result the object identifier in the target context
	 */
    public String mapIdentifier(String type, String source, String target, String sourceId, String targetId) 
        throws CommunicationException {
    

        setIdentifier(type, source, target, sourceId, targetId);
        return getIdentifier(type, source, target, sourceId);

    }



	
	/**
	 * Map the identifier of the source context to the identifier of the target context 
	 * for the object specified by type.
	 *  
	 * The method calls the method <code> getIdentifier(String type, String source,
	 *  String target, String sourceId) </code> to return the mapped target context identifier. 
	 * 
	 *@param specifying the object to get the identifier for
	 *@param the source context
	 *@param the target context 
	 *@param the object identifier in the source context
	 *@param the object identifier in the target context
	 *
	 *@result the object identifier in the target context
	 */
     public String mapIdentifier(String type, String source, String target, String sourceId)
         throws CommunicationException {
         	
        return getIdentifier(type, source, target, sourceId); 	
     
     }
     
    
  
    //*************************************************************
    // Backend Aware Handling
	//*************************************************************

	/**
	 * Method the object manager calls after a new object is created.
	 *
	 * @param bom Reference to the BackendObjectManager object
	 */
	public void setBackendObjectManager(BackendObjectManager bem) {
		this.bem = bem;
	}


	/**
	 * Returns the backend object manager.
	 * @return BackendObjectManager
	 */
	protected BackendObjectManager getBem() {
		return bem;
	}	
	
		
	/**
	 * get the reference of the user backend object
	 *
	 */
	protected IdMapperBackend getBackendService()
		throws BackendException {

		return (IdMapperBackend)createBackendService();
	}

	/**
	 * Get the reference of the backend service and creates it when necessary
	 * this protected method can be used by inherited classes to get
	 * the backend service
	 *
	 * @return the "typeless" backend business object
	 *
	 */
	protected BackendBusinessObject createBackendService()
		throws BackendException {

		if (backendService == null) {
			backendService = (IdMapperBackend) bem.createBackendBusinessObject(IdMapperBackend.ID_MAPPER_BO_TYPE, null);
		}
		return backendService;
	}		
		
		
}