/*****************************************************************************
    Class:        GenericSearchCommand
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.

    $Revision: #1 $
    $DateTime: 2004/01/27 08:20:30 $ (Last changed)
    $Change: 167408 $ (changelist)
    
*****************************************************************************/

package com.sap.isa.businessobject;

import com.sap.isa.core.eai.*;

/**
 * Represents a search request performed against the underlying data storage.
 *
 * @author Thomas Smits
 * @version 1.0
 */
public interface GenericSearchCommand {
    
   
   	/**
     * Perform the search in the backend.
     * 
     * @param bem With the backend object manager the search command get 
     *             access to the backend.
     * @return SearchResult 
     * @throws CommunicationException
     */
    public GenericSearchReturn performGenericSearch(BackendObjectManager bem)
        throws CommunicationException;
    
}