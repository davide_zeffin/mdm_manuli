/*****************************************************************************
 	Class:        DynamicUIMaintenanceBusinessObjectManager
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Author:       Wei-Ming Tchay
    Created:      19.02.20088
    Version:      1.0

    $Revision: #1 $
    $Date: 2008/02/19 $
*****************************************************************************/
package com.sap.isa.businessobject;

import com.sap.isa.maintenanceobject.businessobject.DynamicUIMaintenance;
import com.sap.isa.maintenanceobject.businessobject.DynamicUIMaintenanceAware;

public class DynamicUIMaintenanceBusinessObjectManager extends GenericBusinessObjectManager 
				implements DynamicUIMaintenanceAware{

	 private DynamicUIMaintenance dynamicUIMaintenance;
	 
	/**
     * Constant containing the name of this BOM
     */
    public static final String DYNAMICUIMAINTENANCE_BOM = "DYNAMICUIMAINTENANCE-BOM";
    
    /**
     * Create a new instance of the object.
     */
    public DynamicUIMaintenanceBusinessObjectManager(){    	
    }
    
    /**
     * Creates a new DynamicUIMaintenance object. If such an object was already created, a
     * reference to the old object is returned and no new object is created.
     *
     * @return referenc to a newly created or already existend DynamicUIMaintenance object
     */
    public synchronized DynamicUIMaintenance createDynamicUIMaintenance() {
    	dynamicUIMaintenance = (DynamicUIMaintenance)createBusinessObject(DynamicUIMaintenance.class);
        return dynamicUIMaintenance;
    }

    /**
     * Returns a reference to an existing DynamicUIMaintenance object.
     *
     * @return reference to DynamicUIMaintenance object or null if no object is present
     */
    public DynamicUIMaintenance getDynamicUIMaintenance() {
      return (DynamicUIMaintenance) getBusinessObject(DynamicUIMaintenance.class);
    }


    /**
     * Release the references to created DynamicUIMaintenance object.
     */
    public synchronized void releaseDynamicUIMaintenance() {
        releaseBusinessObject(DynamicUIMaintenance.class);
    }
}
