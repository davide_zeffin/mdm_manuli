/*****************************************************************************
    Class:        SalesDocumentBase
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP AG
    Created:      25.4.2001
    Version:      1.0

    $Revision: #5 $
    $Date: 2002/04/17 $
****************************************************************************/
package com.sap.isa.businessobject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.backend.boi.isacore.BaseConfiguration;
import com.sap.isa.backend.boi.isacore.ConnectedDocumentItemData;
import com.sap.isa.backend.boi.isacore.SalesDocumentBaseData;
import com.sap.isa.backend.boi.isacore.order.HeaderBaseData;
import com.sap.isa.backend.boi.isacore.order.ItemBaseData;
import com.sap.isa.backend.boi.isacore.order.ItemListBaseData;
import com.sap.isa.businessobject.header.HeaderBase;
import com.sap.isa.businessobject.item.ItemBase;
import com.sap.isa.businessobject.item.ItemListBase;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.businessobject.event.BusinessEventSource;
import com.sap.isa.core.eai.BackendObjectManager;

/**
 * Base class for all sales documents, e.g. Basket, Order etc.<br>
 *
 * The document consist of a {@link com.sap.isa.businessobejct.header.HeaderBase} object
 * and a {@link com.sap.isa.businessobejct.item.ItemListBase} object. The item list itself 
 * consist of {@link com.sap.isa.businessobejct.item.Item} objects.
 * There are some convient method which allows to extent this objects.
 * Overwrite the methods {@link #createHeaderInstance}, {@link #createItemListInstance} 
 * or the {@link #createItemInstance} to create instance of extended classes. <br>
 * If so also overwrite the corresponding check methods: {@link #checkHeaderInstance},
 * {@link #checkItemListInstance} or the {@link #checkItemInstance}. <br>
 *
 * If you implement further <i>get</i>, <i>set</i> or <i>add</i> methods 
 * use always the bases class like
 * {@link #addItem} or {@link #getHeaderBaseData} instead the direct access to the properties, 
 * because the methods call the check routines to serve the correct classes. 
 *
 * @author SAP
 * @version 1.0
 */
public abstract class SalesDocumentBase extends BusinessObjectBase
        implements DocumentState, BackendAware, SalesDocumentBaseData, BusinessEventSource {

    protected HeaderBase header;
    protected BackendObjectManager bem;
    protected ItemListBase itemList; 
    protected int state = DocumentState.UNDEFINED;
    protected boolean isCustomerDocument = false;

    protected boolean isFreightValueAvailable = true;
    protected boolean isTaxValueAvailable = true;
    protected boolean isNetValueAvailable = true;
    protected boolean isGrossValueAvailable = true;
    protected boolean isDirty = false;
    protected boolean isChangeHeaderOnly = false;
    
	/**
	 * in case that for large doucments not all items of the doucment are read 
	 * into the itemList, this field holds the original no of items in this doucment.
	 * This means the number of items that were present when the document was first
	 * read. Adding new items or deleting old ones will not change this value.
	 */
	protected int noOfOriginalItems = NO_OF_ITEMS_UNKNOWN;
	
	/**
	 * for large doucments, initial size of items in SelectedItems
	 */
	protected int initialSizeSelectedItems = 0;
	
	/**
	 * if only certain items of the document should be shown or changed, this list is used, to store the guids of those items 
	 */
	protected ArrayList selectedItemGuids = new ArrayList(); 

    
	public SalesDocumentBase() {
		this.header = createHeaderInstance();
		this.itemList = createItemListInstance();
	}


    /**
     * Gets the state of the document
     *
     * @return the state as described by the constants of this interface
     */
    public int getState() {
        return state;
    }


    /**
     * Sets the state of the document
     *
     * @param state the state as described by the constants of this interface
     */
    public void setState(int state) {
        this.state = state;
    }


	/**
	 * Searches for the first item in the itemlist with the given handle
	 * and returns it. If no item with the given handle is found, null is
	 * returned. 
	 * 
	 * @param handle the handle to be set for the Item
	 * @return ItemSalesDoc the first item with the given handle, or null
	 */
	public ItemBase getFirstItemBaseWithHandle(String handle) {
		ItemBase item = null;
		String itemHandle;
        
		for (int i = 0; i < itemList.size(); i++) {
			itemHandle = itemList.getItemBase(i).getHandle();
			if (itemHandle != null && itemHandle.equals(handle)) {
				item = itemList.getItemBase(i);
				break;
			}
		}
        
		return item;      
	}
    

    /**
     * Returns an iterator for the items. Method necessary because of
     * the <code>Iterable</code> interface.
     *
     * @return iterator to iterate over the items
     *
     */
    public Iterator iterator() {
        if (itemList != null) {
            return itemList.iterator();
        } else {
            return null;
        }
    }

    /**
	 * Retrieves the header information of the sales document.
	 *
	 * @return Header data
	 */
	public HeaderBaseData getHeaderBaseData(){
		return header;
	}


    /**
	 * Retrieves the header information of the sales document.
	 *
	 * @return Header data
	 */
	public HeaderBase getHeaderBase(){
		return header;
	}


	/**
	 * Sets the header information. The header information is data
	 * common to all items of the basket.
	 *
	 * @param header Header data to set
	 */
	public void setHeader(HeaderBaseData header){
		if (checkHeaderInstance(header)) {
			this.header = (HeaderBase)header;
		} 
	}


    /**
     * Removes a Item from the basket.
     *
     * @param techKey Technical key of the item to be removed
     */
    public abstract void removeItem(TechKey techKey)
            throws CommunicationException;


    /**
	 * Adds a <code>Item</code> to the sales document.
	 * This item must be uniquely
	 * identified by a technical key.
	 * A <code>null</code> reference passed to this function will be ignored,
	 * in this case nothing will happen.<br>
	 * The method expects a 
	 *
	 * @param item Item to be added to the basket
	 */
	public void addItem(ItemBaseData itemBaseData) {
	  	if (itemBaseData != null && checkItemInstance(itemBaseData)) {
		  itemList.add(itemBaseData);
	  	}
	}


	/**
	 * Only in this method should be created the header object which is to 
	 * use in the sales document. Overwrite this method if you want other 
	 * header as {@link com.sap.isa.businessobject.header.HeaderBase} 
	 * 
	 * @return header as HeaderBase obejct
	 */
	protected HeaderBase createHeaderInstance() {
		return new HeaderBase();		
	}


    /**
	 * Creates a <code>Header</code> for the sales document. <br>
     * The object will be created from the protected method
	 * {@link #createHeaderInstance)}. 
	 *
	 * @return created header
	 */
	 public HeaderBaseData createHeaderBase() {
		return createHeaderInstance();
	 }


	 /**
	  * Only in this method should be created an item object, which is to 
	  * use in the sales document. Overwrite this method if you want other 
	  * item as {@link com.sap.isa.businessobject.item.ItemBase} 
	  * 
	  * @return header as HeaderBase obejct
	  */
	 protected ItemBase createItemInstance() {
		 return new ItemBase();		
	 }

	 /**
	  * Check if the header is an instance of the correct class. <br> 
	  * Overwrite this method if you want to use an other 
	  * item as {@link com.sap.isa.businessobject.header.HeaderBase} 
	  * 
	  * @return <code>true</code> if the header is instance of the correct class
	  */
	 protected boolean checkHeaderInstance(HeaderBaseData headerBaseData) {

		if (!(headerBaseData instanceof HeaderBase)) {
			throw (new PanicException ("Wrong type of header:" 
				+ headerBaseData.getClass().getName()));
		}

		return true;
	  }


	 /**
	  * Check if the item is an instance of the correct class. <br> 
	  * Overwrite this method if you want to use an other 
	  * item as {@link com.sap.isa.businessobject.item.ItemBase} 
	  * 
	  * @return <code>true</code> if the item is instance of the correct class
	  */
	 protected boolean checkItemInstance(ItemBaseData item) {
		 return ItemBase.isItemBase(item);		
	 }

	
	 /**
	  * Check if the itemlist is an instance of the correct class. <br> 
	  * Overwrite this method if you want to use an other 
	  * itemList as {@link com.sap.isa.businessobject.item.ItemListBase} 
	  * 
	  * @return <code>true</code> if the itemlist is instance of the correct class
	  */
	 protected boolean checkItemListInstance(ItemListBaseData itemListBaseData) {
		boolean isItemListBase = false;
		
		if (itemListBaseData != null) {
			if (itemListBaseData instanceof ItemListBase) {
				isItemListBase = true;
			} 
			else {
				log.debug("not allowed instance for ItemListBase object: " + 
						  itemListBaseData.getClass().getName());        	
			}   
	    }     	
		return 	isItemListBase;
	 }


    /**
	 * Creates an <code>ItemBaseData</code> for the sales document.
	 * This item must be uniquely identified by a technical key.
	 *
	 * @return item Item which can added to the basket
	 */
   public ItemBaseData createItemBase() {
	return (ItemBaseData)createItemInstance();
   }



	/**
	 * Only in this method should be created an item list object, which is to 
	 * use in the sales document. Overwrite this method if you want other 
	 * item as {@link com.sap.isa.businessobject.item.ItemListBase} 
	 * 
	 * @return header as HeaderBase obejct
	 */
	protected ItemListBase createItemListInstance() {
		return new ItemListBase();		
	}



    /**
	 * Creates a <code>ItemListBaseData</code> for the basket.
	 *
	 * @return ItemListBaseData an empty ItemListBaseData
	 */
	public ItemListBaseData createItemListBase() {
		return createItemListInstance();
	}



    /**
     * Releaes state of the header. This method is used to drop state
     * information between HTTP request to save memory ressources.
     */
    public void clearHeader() {
        header.clear();
    }


   /**
     * Releaes state of the object. This method is used to drop state
     * information between HTTP request to save memory ressources.
     */
    public void clearData() {
        header.clear();
        itemList.clear();
		clear(); 
    }

    /**
     * Clear all local data
     */
	public void clear() {
		isFreightValueAvailable = true;
		isTaxValueAvailable = true;
		isNetValueAvailable = true;
		isGrossValueAvailable = true;
		isDirty = true;
		isChangeHeaderOnly = false;
		noOfOriginalItems = NO_OF_ITEMS_UNKNOWN;
		initialSizeSelectedItems = 0;
		selectedItemGuids = new ArrayList();
	}


    /**
     * Returns a list of the items currently stored inside the sales document.
     * <br>
     * <b>Note</b> This method performs no copy and returns a reference to
     * the internal representation of the basket's items. Changes done at the
     * items will affect the sales document, too.
     *
     * @return List with items
     */
    public ItemListBaseData getItemListBaseData() {
		if (checkItemListInstance(itemList)) {
			return itemList;
		}
		return null;
    }



    /**
     * Returns the item of the document
     * specified by the given technical key.<br><br>
     * <b>Note -</b> This is a convenience method operating on the
     * <code>getItem</code> method of the internal <code>ItemList</code>.
     *
     * @param techKey the technical key of the item that should be
     *        retrieved
     * @return the item with the given techical key or <code>null</code>
     *         if no item for that key was found.
     */
	public ItemBaseData getItemBaseData(TechKey techKey) {
		ItemBaseData item = itemList.getItemBase(techKey);
		if (item != null && checkItemInstance(item)) {
			return item;
		}
		return null;		
	}




    /**
     * Sets a list of the items currently stored for this sales document.
     *
     * @param itemList list to be set
     *
     */
    public void setItemListBaseData(ItemListBaseData itemList) {
		if (checkItemListInstance(itemList)) {
			this.itemList = (ItemListBase)itemList;
		}
    }



    /**
     * Returns if the document is a customer document.
     * Then the user has the view as soldfrom to the document.
     * One comes only in frontend to this decision.
     * This property is only relevant for the CustomerOrder object.
     * The property is only in the SalesDocument for convenience.
     * So we could transfer the info over the customer document
     * in the standard document and could handle all document in the
     * same way.
     * The value is only <code>true</code> in the CustomerOrder object.
     *
     * @return boolean
     */
    public boolean isCustomerDocument() {
            return isCustomerDocument;
    }

    /**
     * Sets the flag, if the document is a customer document.
     * Then the user has the view as soldfrom to the document.
     *
     * @param boolean possible values <code>true</code> or <code>false</code>
     */
    public void setCustomerDocument(boolean isCustomerDocument) {
            this.isCustomerDocument = isCustomerDocument;
    }

    /**
     * Sets the BackendObjectManager for the basket object. This method is used
     * by the object manager to allow the user object interaction with
     * the backend logic. This method is normally not called
     * by classes other than BusinessObjectManager.
     *
     * @param bem BackendObjectManager to be used
     */
    public void setBackendObjectManager(BackendObjectManager bem) {
        this.bem = bem;
    }

    /**
     * Creates a new Address object.
     *
     * @return Newly created AddressData object
     */
    public AddressData createAddress() {
        return new Address();
    }

    /**
     * Callback method for the <code>BusinessObjectManager</code> to tell
     * the object that life is over and that it has to release
     * all ressources.
     */
    public void destroy() {
        super.destroy();
        header     = null;
        bem        = null;
        itemList   = null;
        state      = DocumentState.UNDEFINED;
    }

    /**
     * Sets the property isFreightValueAvailable
     *
     * @param isFreightValueAvailable
     *         <code>true</code> indicates that the sales document
     *         can deliver the freight value
     *         <code>false</code> indicates that the sales document
     *         can not deliver the freight value.
     */
    public void setFreightValueAvailable(boolean isFreightValueAvailable) {
        this.isFreightValueAvailable = isFreightValueAvailable;
    }

    /**
     * Indicates whether or not the sales document can deliver a freight value
     *
     * @return <code>true</code> indicates that the sales document
     *         can deliver the freight value
     *         <code>false</code> indicates that the sales document
     *         can not deliver the freight value.
     */
    public boolean isFreightValueAvailable() {
        return isFreightValueAvailable;
    }

    /**
     * Sets the property isTaxValueAvailable
     *
     * @param isTaxValueAvailable
     *         <code>true</code> indicates that the sales document
     *         can deliver the tax value
     *         <code>false</code> indicates that the sales document
     *         can not deliver the tax value.
     */
    public void setTaxValueAvailable(boolean isTaxValueAvailable) {
        this.isTaxValueAvailable = isTaxValueAvailable;
    }

    /**
     * Indicates whether or not the sales document can deliver a tax value
     *
     * @return <code>true</code> indicates that the sales document
     *         can deliver the tax value
     *         <code>false</code> indicates that the sales document
     *         can not deliver the tax value.
     */
    public boolean isTaxValueAvailable() {
        return isTaxValueAvailable;
    }

    /**
     * Sets the property isNetValueAvailable
     *
     * @param isNetValueAvailable
     *         <code>true</code> indicates that the sales document
     *         can deliver the net value
     *         <code>false</code> indicates that the sales document
     *         can not deliver the net value.
     */
    public void setNetValueAvailable(boolean isNetValueAvailable) {
        this.isNetValueAvailable = isNetValueAvailable;
    }

    /**
     * Indicates whether or not the sales document can deliver a net value
     *
     * @return <code>true</code> indicates that the sales document
     *         can deliver the net value
     *         <code>false</code> indicates that the sales document
     *         can not deliver the net value.
     */
    public boolean isNetValueAvailable() {
        return isNetValueAvailable;
    }

    /**
     * Sets the key for the document.
     *
     * @param key Key to be set
     */
    public void setTechKey(TechKey techKey) {
        header.setTechKey(techKey);
        super.setTechKey(techKey);
        isDirty = true;
        header.setDirty(true);
    }

    /**
     * Sets the property isGrossValueAvailable
     *
     * @param isGrossValueAvailable
     *         <code>true</code> indicates that the sales document
     *         can deliver the gross value
     *         <code>false</code> indicates that the sales document
     *         can not deliver the gross value.
     */
    public void setGrossValueAvailable(boolean isGrossValueAvailable) {
        this.isGrossValueAvailable = isGrossValueAvailable;
    }

    /**
     * Indicates whether or not the sales document can deliver a Gross value
     *
     * @return <code>true</code> indicates that the sales document
     *         can deliver the gross value
     *         <code>false</code> indicates that the sales document
     *         can not deliver the gross value.
     */
    public boolean isGrossValueAvailable() {
        return isGrossValueAvailable;
    }


    /**
      * Method enhanceConnectedDocument.
      *
      * Set the Properties "docType" and "displayable" in the ConnectedDocument.
      *
      */
    public void enhanceConnectedDocument() {
		final String METHOD = "enhanceConnectedDocument()";
		log.entering(METHOD);
        List predecessorList = this.getHeaderBaseData().getPredecessorList();
    
        if (!predecessorList.isEmpty()) {
    
            ConnectedDocument predecessor;
            Map predecessorMap = new HashMap(predecessorList.size());
            for (int i = 0; i < predecessorList.size(); i++) {
                predecessor = (ConnectedDocument) predecessorList.get(i);
                predecessorMap.put(predecessor.getTechKey(), predecessor);
            }
    
            ItemBase item = null;
    
            for (int i = 0; i < itemList.size(); i++) {
    
                item = itemList.getItemBase(i);
    
                ConnectedDocumentItemData connectedItem = item.getPredecessor();
                if (connectedItem != null) {
                    predecessor = (ConnectedDocument) predecessorMap.get(connectedItem.getDocumentKey());
                    if (predecessor != null) {
                        connectedItem.setDocType(predecessor.getDocType());
                        connectedItem.setDisplayable(predecessor.isDisplayable());
                    }
                }
    
            }
    
        }
        log.exiting();
    
    }


    /**
     * Returns a string representation of the object
     *
     * @return String representation
     */
    public String toString() {
        return this.getClass().getName() + " [techkey=\"" + techKey + "\", header=" + header
             + ", items=" + itemList + "]";
    }


    /**
     * Releaes state of the itemmap. This method is used to drop state
     * information between HTTP request to save memory ressources.
     */
    public void clearItems() {
    	if (itemList != null) {
			itemList.clear();
    	}
		isDirty = true;
    }


    /**
     *
     * clear all messages and set state of the Business Object to valid
     * by default all messages of subobjects will cleared also
     *
     */
    public void clearMessages() {
        super.clearMessages();
        isDirty = true;
        header.setDirty(true);
    }

    
	/**
	 * For large documents, where not all items are loaded into the itemlist
	 * this should return the total no of items, belonging to the document.
	 * This means the number of items that were present when the document was first
	 * read. Adding new items or deleting old ones will not change this value.
	 * 
	 * @return the number of items in the document or 0 if unknown
	 */
	public int getNoOfOriginalItems() {
		return noOfOriginalItems;
	}

	/**
	 * For large documents, where not all items are loaded at a time, this list
	 * contains all guids of items that should currently be loaded.
	 * 
	 * @return list of selected items
	 */
	public ArrayList getSelectedItemGuids() {
		return selectedItemGuids;
	}

	/**
	 * For large documents, where not all items are loaded into the itemlist
	 * the total no of items, belonging to the document can be set.
	 * This means the number of items that were present when the document was first
	 * read. Adding new items or deleting old ones will not change this value.
	 * 
	 * @param noOfItems the number of items in the document, or 0 if unknown
	 */
	public void setNoOfOriginalItems(int noOfOriginalItems) {
		this.noOfOriginalItems = noOfOriginalItems;
	}

	/**
	 * For large documents, where not all items are loaded at a time, this list
	 * contains all guids of items that should currently be loaded.
	 * 
	 * @param selectedItemGuids list of selected items
	 */
	public void setSelectedItemGuids(ArrayList selectedItemGuids) {
		this.selectedItemGuids = selectedItemGuids;
	}
	
	/**
	 * Returns true, if the document is considered to be a large
	 * document. This is the fact, if the value of noOfItems is
	 * greater or equal to shop.getLargeDocNoOfItemsThreshold().
	 * 
	 * @param largeDocNoOfRows number of rows from that a document is 
	 *        considered as large
	 * @return boolean true if document is a large document,
	 *         false else
	 */
	public boolean isLargeDocument(int largeDocNoOfRowsThres) {
		return largeDocNoOfRowsThres != BaseConfiguration.INFINITE_NO_OF_ITEMS && noOfOriginalItems != NO_OF_ITEMS_UNKNOWN && noOfOriginalItems >= largeDocNoOfRowsThres;
	}
	
	/**
	 * Returns true for large documents, if only the header should
	 * be changed
	 * 
	 * @param largeDocNoOfRows number of rows from that a document is 
	 *         considered as large
	 * @return boolean true if document is a large document and only 
	 *         the header should be changed,
	 *         false else
	 */
	public boolean isHeaderChangeInLargeDocument(int largeDocNoOfRowsThres) {
		
		return isLargeDocument(largeDocNoOfRowsThres) && isChangeHeaderOnly;
	}
	
	/**
	 * For large documents
	 * 
	 * Returns the initial number of entries in SelectedItems 
	 * 
	 * @return int initial number of entries in SelectedItems
	 */
	public int getInitialSizeSelectedItems() {
		return initialSizeSelectedItems;
	}

	/**
	 * For large documents
	 * 
	 * Sets the initial number of entries in SelectedItems
	 * 
	 * @param initialSizeSelectedItems initial number of entries in SelectedItems
	 */
	public void setInitialSizeSelectedItems(int initialSizeSelectedItems) {
		this.initialSizeSelectedItems = initialSizeSelectedItems;
	}
	
	/**
	 * Determines the current number of items in the salesdoc based on the values of
	 * InitialSizeSelectedItems, itemList.size() and noOfOriginalItems
	 * 
	 * @return int number of items currently in the order
	 */
	public int getCurrentNoOfPositions() {
		return noOfOriginalItems - (initialSizeSelectedItems - itemList.size());
	}

    /**
     * Sets the flag that we are in change header only mode
     * 
     * @param isChangeHeaderOnly true if we are in changeHeaderOnly mode
     *                           fasle else
     */
    public void setChangeHeaderOnly(boolean isChangeHeaderOnly) {
        this.isChangeHeaderOnly = isChangeHeaderOnly;
    }

    /**
     * Returns true if only the header should be changed
     * 
     * @return boolean true if only the header should be changed,
     *         false else
     */
    public boolean isChangeHeaderOnly() {       
        return isChangeHeaderOnly;
    }


    /**
     * Get the dirty flag
     *
     * @return isDirty will the document be read from the backend when 
     *                 the a read method is called true/false
     */
    public boolean isDirty() {
        return isDirty;
    }

    /**
     * Set the dirty flag
     *
     * @param isDirty if set to true, the document will be read from the backend when a read method is called
     *                if set to false, the next call to a read method won't fill the object from the backend 
     */
    public void setDirty(boolean isDirty) {
        this.isDirty = isDirty;
        if (getHeaderBase() != null) {
			getHeaderBase().setDirty(true);
        }
    }

 }


