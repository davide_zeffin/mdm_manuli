/*****************************************************************************
    Class:        Product
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP
    Created:      March 2001
    Version:      2.0

    $Revision: #2 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.businessobject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.sap.isa.backend.boi.isacore.ProductData;
import com.sap.isa.backend.boi.webcatalog.pricing.PriceInfo;
import com.sap.isa.businessobject.item.BasketTransferItem;
import com.sap.isa.businessobject.item.ContractDuration;
import com.sap.isa.businessobject.webcatalog.atp.Atp;
import com.sap.isa.businessobject.webcatalog.atp.AtpResult;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.catalog.webcatalog.WebCatItemPrice;
import com.sap.isa.catalog.webcatalog.WebCatSubItem;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.IPCItemReference;


/**
 * Class representing a product. <br> 
 * 
 * The product could get the all information from the catalog item with the 
 * {@link #setCatalogItem(WebCatItem)} method. Some methods are wrapper for 
 * corresponding catalog item calls. <br>
 * Nevertheless all properties, for which a setter method exist could used without
 * a catalog item.<br> 
 * 
 * @author SAP
 * @version 2.0
 */
public class Product extends BusinessObjectBase 
		implements ProductData, Cloneable, ProductBaseData {

    private static IsaLocation log = IsaLocation.getInstance(Product.class.getName());


    /**
     * Constants for Attributename for Picture
     *
     */
    public final static String AN_PICTURE1 = "DOC_PC_CRM_IMAGE";
    public final static String AN_PICTURE2 = "DOC_P_CRM_IMAGE";


    /**
     * Constants for Attributename for Thumbnail
     *
     */
    public final static String AN_THUMBNAIL1 = "DOC_PC_CRM_THUMB";
    public final static String AN_THUMBNAIL2 = "DOC_P_CRM_THUMB";
    public final static String AN_THUMBNAIL3 = "DOC_P_BDS_IMAGE";

    public final static String URLSeparator  = "/";

    /**
     * Constants for Attributename for Configurable
     *
     */
    public final static String AN_CONFIGURABLE = "PRODUCT_CONFIGURABLE_FLAG ";

	/**
	 * Product id.<br>
	 */
    protected String id = "";
    
	/**
	 * Product description.<br>
	 */
    protected String description = "";
    
    private boolean isAvailable;
    private Date availabilityDate;
    
    private WebCatItem catalogItem;

	/**
	 * URL of the product thumbnail.<br>
	 */
	protected String thumb = ""; 

	/**
	 * URL of the product picture.<br>
	 */
	protected String picture = ""; 

	/**
	 * Unit of the product.<br>
	 */
	protected String unit = "";

	// the following properties will read directly from the catalogItem:
	// therefore no setter are available

	// price - the product's price, usually from the IPC, but we never really know where the
	//         catalog interface get's it from in the end
	// currency - the currency for the product
	// configurable - flag whether the product is configurable
	

    // reference to the product configuration (if available)
    private Object productConfiguration = null;


    /**
     * Constructor
     *
     * @param techKey  techKey of the product
     *
     */
    public Product(TechKey techKey) {
      setTechKey(techKey);
    }


    /**
     * Constructor.
     *
     * @param techKey  techKey of the product
     * @param id product id
     *
     */
    public Product(TechKey techKey, String id) {
      setTechKey(techKey);
      this.id = id;
    }


    /**
     * Constructor used to create a new Product for a given implementor of
     * <code>BasketTransferItem</code>
     *
     * @param transferItem Item to be used
     */
    public Product(BasketTransferItem transferItem) {
      setTechKey( new TechKey(transferItem.getProductKey()) );
      // the product configuration may also be 'null' (if there is no configuration)
      productConfiguration = transferItem.getConfigurationItem();
    }


    /**
     * Constructor used to create a new Product for a given implementor of
     * <code>WebCatItem</code>
     *
     * @param webCatItem Item to be used
     */
    public Product(WebCatItem catalogItem) {
      setTechKey( new TechKey(catalogItem.getProductID()));
      setCatalogItem(catalogItem);
    }


	/**
	 * Returns the AreaId of the corresponding catalog area.
	 *
	 * @return AreaId
	 *
	 */
	public String getArea() {
		if (catalogItem != null) {
		   return catalogItem.getAreaID();
		}
		return null;
	}


    /**
     *
     * With this method you can set availability information for a product
     * the results can read over the corresponding properties
     *
     * @param Atp atp object from catalog bom
     * @param catalogKey catalog key
     * @param shopID Id of the shop
     * @param quantity which is requested
     * @deprecated
     *
     */
    public void setAvailabilityData(Atp atp, String catalogKey, String shopId , String quantity) {
		final String METHOD = "setAvailabilityData()";
		log.entering(METHOD);
		if (log.isDebugEnabled()) {
			log.debug("atp=" + atp + ", catalogKey=" + catalogKey + ", shopId=" + shopId + ", quantity=" + quantity);
		}
        this.isAvailable = false;
        availabilityDate = new Date();
        
        if (catalogItem != null) {
            try {
	             String cat="";
	             String variant="";
	             String catalogGuid = catalogKey;
	             if (catalogGuid.length() == 60) {
	               cat = catalogGuid.substring(0,30);
	               variant = catalogGuid.substring(30);
	             }
             	 catalogItem.setQuantity(quantity);
                 AtpResult atpResult = atp.readAtpResult(shopId,
                                                         catalogItem,   // the Item Itself,
                                                         null,                       // requested Date
                                                         cat,
                                                         variant);
              if (atpResult != null) {

                if (!atpResult.getCommittedQuantity().equals("0")) {
                    isAvailable = true;
                }

                availabilityDate = atpResult.getCommittedDate();
              }

            }
            catch (CommunicationException ex) {
            	log.debug(ex.getMessage());
            }
        }
        log.exiting();
    }

    /**
     *
     * With this method you can set availability information for a product
     * the results can read over the corresponding properties
     *
     * @param Atp atp object from catalog bom
     * @param catalogKey catalog key
     * @param shopID Id of the shop
     * @param country Country of the shop
     * @param quantity which is requested
     *
     */
    public void setAvailabilityData(Atp atp, String catalogKey, String shopId , String country, String quantity) {
        final String METHOD = "setAvailabilityData()";
        log.entering(METHOD);
		if (log.isDebugEnabled()) {
			log.debug("atp=" + atp + ", catalogKey=" + catalogKey + ", shopId=" + shopId + ", quantity=" + quantity);
		}
        this.isAvailable = false;
        availabilityDate = new Date();

        if (catalogItem != null) {
            try {
                 String cat="";
                 String variant="";
                 String catalogGuid = catalogKey;
                 if (catalogGuid.length() == 60) {
                   cat = catalogGuid.substring(0,30);
                   variant = catalogGuid.substring(30);
                 }
                 catalogItem.setQuantity(quantity);
                 AtpResult atpResult = atp.readAtpResult(shopId,
                                                         country,
                                                         catalogItem,   // the Item Itself,
                                                         catalogItem.getProductID(),
                                                         null,                       // requested Date
                                                         cat,
                                                         variant);
              if (atpResult != null) {

                if (!atpResult.getCommittedQuantity().equals("0")) {
                    isAvailable = true;
                }

                availabilityDate = atpResult.getCommittedDate();
              }

            }
            catch (CommunicationException ex) {
                log.debug(ex.getMessage());
            }
        }
        log.exiting();
    }


    /**
     * Returns the property available
     *
     * @return available
     *
     */
    public boolean isAvailable() {
       return this.isAvailable;
    }


    /**
     * Set the property availabilityDate
     *
     * @param availabilityDate
     *
     */
    public void setAvailabilityDate(Date availabilityDate) {
        this.availabilityDate = availabilityDate;
    }


    /**
     * Returns the property availabilityDate
     *
     * @return availabilityDate
     *
     */
    public Date getAvailabilityDate() {
       return this.availabilityDate;
    }


    /**
     * Set the property catalogItem
     *
     * @param catalogItem
     *
     */
    public void setCatalogItem(WebCatItem catalogItem) {
		final String METHOD = "setCatalogItem()";
		log.entering(METHOD);
		if (log.isDebugEnabled()) {
			log.debug("catalogItem=" + catalogItem);
		}
        this.catalogItem = catalogItem;

        description = catalogItem.getDescription();
        
        thumb = catalogItem.getThumb();
		picture = catalogItem.getPicture();
		
        unit = catalogItem.getUnit();
        id = catalogItem.getProduct();
        log.exiting();              
    }


    /**
     * Returns the property catalogItem
     *
     * @return catalogItem
     *
     */
    public WebCatItem getCatalogItem() {
       return this.catalogItem;
    }


	/**
	 * Returns the property catalogItem
	 *
	 * @return catalogItem
	 *
	 */
	public Object getCatalogItemAsObject() {
		return catalogItem;
	}

    /**
     * This methods returns the category ids of a product if present,
     * otherwise a String array of length 0 is returned.
     */
    public String[] getCategoryIds() {
        log.entering("getCategoryIds()");
        
        String[] retVal = new String[0];
        
        if (catalogItem != null) {
            retVal = catalogItem.getCategoryIds();
        }
        
        log.exiting();
        return retVal;
    }
    
    /**
     * This methods returns the category description of  a product  if present,
     * otherwise a String array of length 0 is returned.
     * AN unknown desription will be indetified by -
     */
    public String[] getCategoryDescriptions() {
        log.entering("getCategoryDescriptions()");
        
        String[] retVal = new String[0];
        
        if (catalogItem != null) {
            retVal = catalogItem.getCategoryDescriptions();
        }
        
        log.exiting();
        return retVal;
    }

    /**
     * Returns <code>true</code> if the product is configurable, false otherwise
     * The method implements ProductBaseData.
     * 
     * @return <code>true</code> if the product is configurable, 
     *         false otherwise
     */
    public boolean isConfigurable() {
        String method = "isConfigurable()";
        log.entering(method);
        boolean isConfigurable = false;
        
        if (productConfiguration != null) {
            isConfigurable = true;
            // may be the product configuration reference is not maintained
            // (it depends on what constructor was used), so ask the catalog item
        }
        else if (catalogItem != null) {
            isConfigurable = catalogItem.isConfigurable();
        }
        if (log.isDebugEnabled()) {
			log.debug(method + ": is configurable = " + isConfigurable);
        }
        
        log.exiting();
        return isConfigurable;
    }
    

    /**
     * Returns <code>true</code> if the product contains sub items.
     *
     * @return <code>true</code> if the product contains sub items.
     */
    public boolean hasSubItems() {

        final String METHOD = "hasSubItems()";
        log.entering(METHOD);

        boolean ret = false;  
        if (isRelevantForExplosion()) { 
			if (log.isDebugEnabled()) {
				log.debug(METHOD + ": is relevant for explosion");
			}
            
            if (getCatalogItem() != null && getCatalogItem().hasSubItems()) {
				if (log.isDebugEnabled()) {
					log.debug(METHOD + ": has sub items");
				}
                ret = true;
            } else if (log.isDebugEnabled()) {
                log.debug(METHOD + ": no sub items found");
            }
        }
		if (log.isDebugEnabled()) {
			log.debug(METHOD + ": ret =" + ret);
		}
        
        log.exiting();              
        return ret;
    }

	/**
	 * Returns currency for the product
	 *
	 * @return Currency
	 */
	public String getCurrency() {
		if (catalogItem != null) {
			try {
				//WebCatItemPrice itemPrice = catalogItem.readItemPrice();
				WebCatItemPrice itemPrice = catalogItem.retrieveItemPrice();
				PriceInfo priceInfo = itemPrice.getPriceInfo();
				if (priceInfo != null) {
					if (priceInfo.getCurrency()!=null) {
						return priceInfo.getCurrency();
					}
				}
			}
			catch (Exception ex) {
				log.debug(ex.getMessage());
				return ("???");
			}
		}
		return "";
	}


    /**
     * Set the description of the product. <br>
     * 
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

	/**
	 * Returns the description of the product
     * Method implements ProductBaseData.
	 *
	 * @return description
	 */
	public String getDescription() {
		return description;
	}


	/**
	 * Set the product id. <br>
	 * 
	 * @param id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Returns the product ID
     * Method implements ProductBaseData.
	 *
	 * @return id
	 */
	public String getId() {
		return id;
	}


    /**
     * Returns the unique ItemId of the catalog id.
     * Method implements ProductBaseData.
     *
     * @return ItemId
     *
     */
    public String getItemId() {
        if (catalogItem != null) {
           return catalogItem.getItemID();
        }
        return null;
    }


    /**
     * Returns the URL for the picture of the product
     * Method implements ProductBaseData.
     *
     * @return picture
     */
    public String getPicture() {
        return picture;
    }

    /**
     * Returns price of the product with currency
     *
     * @return price with currency
     */
    public String getPrice() {
        if (catalogItem != null) {
            try {
                //WebCatItemPrice itemPrice = catalogItem.readItemPrice();
				WebCatItemPrice itemPrice = catalogItem.retrieveItemPrice();
                String price = itemPrice.getPrice();
                if (price != null) {
                    return price;
                }
            }
            catch (Exception ex) {
				log.debug(ex.getMessage());
                return ("error");
            }
        }
        return "";
    }
    
    /**
     * Returns the price eye catcher text.
     * The method implements a method of ProductBaseData.
     * 
     * @return preice eye catcher text as String
     * 
     */
    public String getPriceEyeCatcherText() {
        String method = "getPriceEyeCatcherText()";
        log.entering(method);
        String priceEyeCatcherText = "";

        if (catalogItem != null) {
            priceEyeCatcherText = catalogItem.getPriceEyeCatcherText();
        }
        else {
            log.debug("catalogItem is null");
        }

		if (log.isDebugEnabled()) {
			log.debug(method + ": PriceEyeCatcherText =" + priceEyeCatcherText);
		}
        
        log.exiting();              
        return priceEyeCatcherText;
    }
    
    
    /**
     * Returns WebCatItemPrice object if present
     * else null
     *
     * @return WebCatItemPrice object if present
     *         else null
     */
    public WebCatItemPrice getProductPrice() {
        
        log.entering("getProductPrice()");
        
        WebCatItemPrice price = null;
        
        if (catalogItem != null) {
            try {
                WebCatItemPrice itemPrice = catalogItem.retrieveItemPrice();
                price = itemPrice;
            }
            catch (Exception ex) {
                log.error("exception has occured", ex);
            }
        }
        
        log.exiting();
        return price;
    }


	/**
	 * Returns the product ID
	 *
	 * @return id
	 */
	public String getProductId() {
		return id;
	}

	/**
	 * Returns customer specific price of the product with currency. <br>
	 *
	 * @return price with currency
	 */
	public String getCustomerSpecificPrice() {
		if (catalogItem != null) {
			try {
				//String price = catalogItem.readItemPrice().getIPCPrice();
                // just to be sure
                // whyever the unit gets lost in the catalogItem after passing it to this product
				String price = catalogItem.retrieveItemPrice().getIPCPrice();
				if (price != null) {
					return price;
				}
			}
			catch (Exception ex) {
				log.error(LogUtil.APPS_BUSINESS_LOGIC,ex.getMessage(),ex);
			}
		}
		return "";
	}


	/**
	 * Returns list price of the product with currency. <br>
	 *
	 * @return price with currency
	 */
	public String getListPrice() {
        final String METHOD = "getListPrice";
        log.entering(METHOD);


		if (catalogItem != null) {
			try {
				//String price = catalogItem.readItemPrice().getListPrice();
				String price = catalogItem.retrieveItemPrice().getListPrice();
				if (price != null) {
					if (log.isDebugEnabled()) {
						log.debug(METHOD + ": price = " + price);
					}
                    
					return price;
				}
			}
			catch (Exception ex) {
				log.error(LogUtil.APPS_BUSINESS_LOGIC,ex.getMessage(),ex);
            }
        }
		if (log.isDebugEnabled()) {
			log.debug(METHOD + ": no price");
		}
        
        log.exiting();
        return "";
    }	


    /**
     * Returns price of the product with currency. You should use this methoed instead of<br>
     * getListPrice() etc.
     *
     * @return price with currency
     */
    public String getItemPrice() {
        if (catalogItem != null) {
            try {
                //String price = catalogItem.readItemPrice().getListPrice();
                String price = catalogItem.getItemPrice().getPrice();
                if (price != null) {
                    return price;
                }
            }
            catch (Exception ex) {
                log.error(LogUtil.APPS_BUSINESS_LOGIC,ex.getMessage(),ex);
            }
        }
        return "";
        
    }   


    /**
     * Returns only the price of the product without currency. <br>
     *
     * @return price without currency
     */
    public String getPriceValue() {
        if (catalogItem != null) {
            try {
                //WebCatItemPrice itemPrice = catalogItem.readItemPrice();
				WebCatItemPrice itemPrice = catalogItem.retrieveItemPrice();
                PriceInfo priceInfo = itemPrice.getPriceInfo();
                if (priceInfo != null) {
                    if (priceInfo.getPrice()!=null) {
                        return priceInfo.getPrice();
                    }
                }
            }
            catch (Exception ex) {
                return ("error");
            }
        }
        return "";
    }


	/**
	 * Set the URL for the thumbnail of the product. <br>
	 * 
	 * @param thumb
	 */
	public void setThumb(String thumb) {
		this.thumb = thumb;
	}

	/**
	 * Returns the URL for the thumbnail of the product. <br>
	 *
	 * @return thumb
	 */
	public String getThumb() {
		return thumb;
	}


    /**
     * Set the unit of the product. <br>
     * 
     * @param unit
     */
    public void setUnit(String unit) {
        this.unit = unit;
    }


	/**
	 * Returns the unit of the product. <br>
     * The method implements ProductBaseData.
	 *
	 * @return unit
	 */
	public String getUnit() {
        if (catalogItem != null) {
            return catalogItem.getUnit();
        }
		return unit;
	}


    /**
     *
     * returns the object as string
     *
     * @return String which returns all fields
     *
     */
    public String toString() {
       return getTechKey().getIdAsString() + " "  + id;
    }


    /**
     *
     * enhance a products with catalog data.
     * if no catalog data could found the method returns <code>false</code>
     * and the product shouldn't add to productlist then
     *
     * @param catalog A reference to the catalog which will enhance the product
     *        data
     * @return returns <code>true</code> if the product is found in the catalog
     *
     */
    public boolean enhance(WebCatInfo catalog) {

        return enhance(catalog, true, null);
    }


    /**
     * Enhance a products with catalog data. <br>
     * If no catalog data could found the method returns <code>false</code>
     * and the product shouldn't add to productlist then
     *
     * @param catalog A reference to the catalog which will enhance the product
     *        data
     * @return returns <code>true</code> if the product is found in the catalog
     *
     */
    public boolean enhance(WebCatInfo catalog, boolean accessoryAllowed) {
        
        return enhance(catalog, accessoryAllowed, null);
    }


    /**
     * Enhance a products with catalog data. <br>
     * If no catalog data could found the method returns <code>false</code>
     * and the product shouldn't add to productlist then
     *
     * @param catalog A reference to the catalog which will enhance the product data
     * @param areaType as String, type of the area in which the product needs to be searched
     * 
     * @return returns <code>true</code> if the product is found in the catalog
     *
     */
    public boolean enhance(WebCatInfo catalog, boolean accessoryAllowed, String areaType) {
        
        final String METHOD = "enhance()";
        log.entering(METHOD);
        
        if (log.isDebugEnabled()) {
			log.debug("catalog="+catalog+", accessoryAllowed="+accessoryAllowed+", areaType="+areaType);
        }  

        // enhance only allow, if the catalog exist
        if (catalog == null) {
            return false;
        }
            
        try {

            HashMap catalogItems = null;
            
            String[] techKeyStrings = new String[1];
    
            techKeyStrings[0] = getTechKey().getIdAsString();
    
            // get the webCatItems from the catalog
            catalogItems = catalog.getProductMap(techKeyStrings, true, areaType);
    
            // check, if for every product a catalog item exits and set it
            WebCatItem catalogItem = null;
    
            if (catalogItems != null) {
                // the Hash Table contains an Array List for the product, because the
                // technical key is not unique because a product could occur in different
                // areas
                List catalogItemList = (List)catalogItems.get(getTechKey().getIdAsString());
    
                if (catalogItemList != null) {
                    if (accessoryAllowed) {
                        // we use only the first entry in the list
                        catalogItem = (WebCatItem) catalogItemList.get(0);
                    }
                    else {
                        for (int i=0; i < catalogItemList.size(); i++) {
                            WebCatItem webCatItem = (WebCatItem)catalogItemList.get(i);
                            if (!webCatItem.getAttribute("ROLE").equals("B")) {
                                catalogItem = webCatItem;
                                break;
                            }
                        }
                    }
                }
            }
    
            if (catalogItem != null) {
                setCatalogItem(catalogItem);
                return true;
            }
            return false;
            } finally {
                log.exiting();
        }
    }


  /**
   * Returns a reference to the product configuration. If no product configuration is
   * available <em>null</em> will be returned. The product configuration is built up
   * by an external configuration engine.<br>
   * If the IPC engine is used the returned object should be casted to an
   * <code>com.sap.spc.remote.client.object.IPCItem</code> instance.<br>
   * If another engine is used also another appropriate cast should occur.
   * @return a reference to the product configuration or <em>null</em> if there is no
   * configuration
   */
    public Object getProductConfiguration() {
        // If the configuration reference is maintained return this reference directly,
        // otherwise ask the catalog item for it.
        if (productConfiguration != null) {
            return productConfiguration;
        }
        if (catalogItem != null) {
            return catalogItem.getConfigItemReference();
        }
        // at this point there is certainly no configuration
        return null;
    }


	/**
	 * Return the IPCItem reference for the price. <br> 
	 * See also {@link #IPCItemReference}. <br>
	 * 
	 * @return  {@link #IPCItemReference}
	 */    
	public IPCItemReference getIPCItemReference() {   

        IPCItemReference ipcItemRef = null;
        if (catalogItem != null) {
            IPCItem priceRef;
            WebCatItem item = (WebCatItem) catalogItem;
            if (item != null) {
                if (item.getItemPrice() != null) {
                    if (item.getItemPrice().getPriceInfo() != null) {
                        priceRef =(IPCItem) item.getItemPrice().getPriceInfo()
                                .getPricingItemReference();
                        if (priceRef != null) {
                            ipcItemRef = priceRef.getItemReference();
                        }
                    }
                }
            }
        }
		return ipcItemRef;
	}					  


    /**
     * Returns a copy of this object.
     *
     * @ return Copy of this object
     */
    public Object clone() {

		Product newProduct = null;
        try {
            newProduct = (Product) super.clone();
        }
        catch (CloneNotSupportedException exception) {
        	log.error(LogUtil.APPS_BUSINESS_LOGIC,exception.getMessage(),exception);
        }

        return newProduct;
    }

    /**
     * Returns the group key.
     * Method implements ProductBaseData.
     * 
     * @return group key as <code>String</code>
     * 
     */
    public String getGroupKey() {
        String method = "getGroupKey()";
        log.entering(method);
        String groupKey = "";

        if (catalogItem != null) {
           groupKey = catalogItem.getGroupKey();
        }

		if (log.isDebugEnabled()) {
			log.debug(method + ": groupKey =" + groupKey);
		}  
        
        log.exiting();              
        return groupKey;
    }
    
    /**
     * This methods returns the hierarchy ids of a product if present,
     * otherwise a String array of length 0 is returned.
     */
    public String[] getHierarchyIds() {
        log.entering("getHierarchyIds()");
        
        String[] retVal = new String[0];
        
        if (catalogItem != null) {
            retVal = catalogItem.getHierarchyIds();
        }
        
        log.exiting();
        return retVal;
    }
    
    /**
     * This methods returns the hierarchy description of a product if present,
     * otherwise a String array of length 0 is returned.
     * AN unknown desription will be indetified by -
     */
    public String[] getHierarchyDescriptions() {
        log.entering("getHierarchyDescriptions()");
        
        String[] retVal = new String[0];
        
        if (catalogItem != null) {
            retVal = catalogItem.getHierarchyDescriptions();
        }
        
        log.exiting();
        return retVal;
    }

    /**
     * Returns the hierarchy level of a product in a list of products.
     * Method implements ProductBaseData.
     * 
     */
    public int getHierarchyLevel() {
        String method = "getHierarchyLevel()";
        log.entering(method);
        int hier = 0;
        if (catalogItem != null) {
           hier = catalogItem.getHierarchyLevel();
        }

		if (log.isDebugEnabled()) {
			log.debug(method + ": Hierarchy Level =" + hier);
		}
        
        log.exiting();
        return hier;
    }

    /**
     * get flag whether this item is selected via checkbox.
     * The method implements ProductBaseData.
     *
     * @return selected boolean value, represented as String as it needs to be checked for web 
     *         display
     */
    public String isSelectedStr() {
        String method = "isSelectedStr()";
        log.entering(method);

        String retVal = "";
        if (catalogItem != null) {
           retVal = catalogItem.isSelectedStr();
        }

		if (log.isDebugEnabled()) {
			log.debug("isSelectedStr=" + retVal);
		}
        
        log.exiting();
        return retVal;
    }

    /**
     * Returns value of selection mark.
     * The method implements ProductBaseData.
     *
     * @return true if this item is selected; false otherwise
     */
    public boolean isSelected() {
        String method = "isSelected()";
        log.entering(method);
        boolean retVal = true;
        if (catalogItem != null) {
           retVal = catalogItem.isSelected();
        }
		if (log.isDebugEnabled()) {
			log.debug(method + ": isSelected=" + retVal);
		}
        
        log.exiting();
        return retVal;
    }

    /**
     * Checks if this item is classified as Dependent Components Product.
     * The method implements ProductBaseData.
     * 
     * @return  true, if the item is a dependent components product,
     *          false, otherwise.
     */
    public boolean isDependentComponent() {
        String method = "isDependentComponent()";
        log.entering(method);
        boolean tvalue = false;
        if (catalogItem != null) {
           tvalue = catalogItem.isDependentComponent();
        }
		if (log.isDebugEnabled()) {
			log.debug(method + ": the product is a dependent component =" + tvalue);
		}
        
        log.exiting();
        return tvalue;
    }
    
    /**
     * Checks if this item is classified as Rate Plan.
     * The method implements ProductBaseData
     * 
     * @return  true, if the item is a rate plan product,
     *          false, otherwise.
     */
    public boolean isRatePlan() {
        String method = "isRatePlan()";
        log.entering(method);
        boolean retVal = false; 
        
        if (catalogItem != null) {
            retVal = catalogItem.isRatePlan();
        }
        
		if (log.isDebugEnabled()) {
			log.debug(method +": isRatePlan (from TREX) = " + retVal);
		} 
        
        log.exiting();

        return retVal;
    }

    /**
     * Checks if this item is classified as Rate Plan Combination Product.
     * The method implements ProductBaseData.
     * 
     * @return  true, if the item is a Rate Plan Combination product,
     *          false, otherwise.
     */
    public boolean isRatePlanCombination() {
        String method = "isRatePlanCombination()";
        log.entering(method);
        boolean tvalue = false;
        if (catalogItem != null) {
           tvalue = catalogItem.isRatePlanCombination();
        }
		if (log.isDebugEnabled()) {
			log.debug(method + ": the product is a Rate Plan Combination =" + tvalue);
		} 
        
        log.exiting();
        return tvalue;
    }

    /**
     * Checks if this item is classified as Sales Components Product.
     * The method implements ProductBaseData.
     * 
     * @return  true, if the item is a sales components product,
     *          false, otherwise.
     */
    public boolean isSalesComponent() {
        String method = "isSalesComponent()";
        log.entering(method);
        boolean tvalue = false;
        if (catalogItem != null) {
           tvalue = catalogItem.isSalesComponent();
        }
		if (log.isDebugEnabled()) {
			log.debug(method + ": the product is a sales component =" + tvalue);
		}
        
        log.exiting();
        return tvalue;
    }

    /**
     * Get parent techKey of a product.
     * The method implements a method of ProductBaseData.
     * 
     * @return  parent techKey  
     */
    public TechKey getParentTechKey() {
        String method = "getParentTechKey()";
        log.entering(method);
        TechKey techKey = null;
        if (catalogItem != null) {
            techKey = catalogItem.getParentTechKey();
        }
        log.exiting();
        return (techKey); 
    }

    /**
     * Get parent product of a product.
     * The method implements a method of ProductBaseData.
     * 
     * @return  parent product of type ProductBaseData 
     */
    public ProductBaseData getParent() {
        String method = "getParent()";
        log.entering(method);
        ProductBaseData product = null;
        if (catalogItem != null) {
           product = catalogItem.getParent();
        }
        log.exiting();
        return (product); 
    }

    /**
     * Returns eye catcher text.
     * The method implements a method of ProductBaseData.
     * 
     * @return eye catcher text as String
     * 
     */
    public String getEyeCatcherText() {
        String method = "getEyeCatcherText()";
        log.entering(method);
        String eyecatchertext = "";

        if (catalogItem != null) {
            eyecatchertext = catalogItem.getEyeCatcherText();
        }

		if (log.isDebugEnabled()) {
			log.debug(method + ": EyeCatcherText =" + eyecatchertext);
		}
        
        log.exiting();              
        return eyecatchertext;
    }

    /**
     * Returns the quantity of the product. <br>
     * The method implements ProductBaseData.
     *
     * @return quantity as String
     */
    public String getQuantityAsStr() {
        String method = "getQuantityAsStr()";
        log.entering(method);
        String quantity = "";

        if (catalogItem != null) {
            quantity = catalogItem.getQuantityAsStr();
        }
       
		if (log.isDebugEnabled()) {
			log.debug(method + ": quantity =" + quantity);
		}
        log.exiting();              
        return quantity;
    }

    /**
     * Returns the information, if this <code>Product</code> is optional. 
     * The method implements ProductBaseData.
     * 
     * @return <code>false</code>
     */
    public boolean isOptional() {
        log.entering("isOptional()");
        log.exiting();
        return false;
    }

    /**
     * Returns the information, if this <code>Product</code> is part of a group. 
     * The method implements ProductBaseData.
     * 
     * @return <code>false</code>
     */
    public boolean isPartOfAGroup() {
        String method = "isPartOfAGroup()";
        log.entering(method);
        boolean retVal = false;
        if (catalogItem != null) {
            retVal = catalogItem.isPartOfAGroup();
        }
		if (log.isDebugEnabled()) {
			log.debug(method + ": return value =" + retVal);
		}
        
        log.exiting();
        return retVal;
    }

    /**
     * Get flag whether this item is selected via checkbox or radio button.
     *
     * @return selection value of type boolean 
     */
    public boolean getAuthorFlag() {
        String method = "getAuthorFlag()";
        log.entering(method);
        boolean retVal = false;
        if (catalogItem != null) {
            retVal = catalogItem.getAuthorFlag();
        }
		if (log.isDebugEnabled()) {
	        log.debug(method + ": return value =" + retVal);
        }
        log.exiting();
        return retVal;
    }

    /**
     * Get Solution configurator selection flag.
     *
     * @return selection flag as String
     */
    public String getAuthor() {
        String method = "getAuthor()";
        log.entering(method);
        String scSelFlag = "";
        if (catalogItem != null) {
            scSelFlag = catalogItem.getAuthor();
        }
		if (log.isDebugEnabled()) {
			log.debug(method + ": SC selection flag =" + scSelFlag);
		}
        
        log.exiting();
        return scSelFlag;
    }

    
    /**
     * set flag whether this item is selected via checkbox or radio button.
     *
     * @param selectFlag of type boolean as it needs to be checked for web 
     *         display
     */
    public void setAuthorFlag(boolean selectFlag){
        String method = "setAuthorFlag(boolean selectFlag)";
        log.entering(method);
        if (catalogItem != null) {
            catalogItem.setAuthorFlag(selectFlag);
        }
		if (log.isDebugEnabled()) {
			log.debug(method + ": select flag = " + selectFlag);
		}
        
        log.exiting();
    }

    /**
     * Checks, if the product is classified as a Combined Rate Plan.
     * The method implements ProductBaseData.
     * 
     * @return <code>false</code>
     * 
     * @return  true, if the item is a Combined Rate Plan 
     *          false, otherwise.
     */
    public boolean isCombinedRatePlan() {
        String method = "isCombinedRatePlan()";
        log.entering(method);
        boolean retVal = false;
        if (catalogItem != null) {
            retVal = catalogItem.isCombinedRatePlan();
        }
		if (log.isDebugEnabled()) {
			log.debug(method + ": return value =" + retVal);
		}
        
        log.exiting();
        return retVal;
    }

    /**
     * Check whether the configuration of the item is complete and consistent
     *
     * @return boolean indicating whether the items configuration contains errors
     */
    public boolean isConfiguredCompletely() {
        String method = "isConfiguredCompletely()";
        log.entering(method);
        boolean isConfiguredCompletely = false;
        if (catalogItem != null) {
            isConfiguredCompletely = catalogItem.isConfiguredCompletely();
        }
		if (log.isDebugEnabled()) {
			log.debug(method + ": is configured completely = " + isConfiguredCompletely);
		}
        
        log.exiting();
        return isConfiguredCompletely;
    }

    /**
     * Checks if this product is a product variant.
     * The method implements ProductBaseData.
     * 
     * @return  true, if the product is a product variant,
     *          false, otherwise.
     */
    public boolean isProductVariant() {
        String method = "isProductVariant()";
        log.entering(method);
        boolean isProductVariant = false;
        if (catalogItem != null) {
            isProductVariant = catalogItem.isProductVariant();
        }
		if (log.isDebugEnabled()) {
			log.debug(method + ": is a product variant = " + isProductVariant);
		}
        
        log.exiting();
        return isProductVariant;
    }

    /**
     * Returns true, if the product is old fashion configurable
     * The method implements ProductBaseData.
     *
     * @return true, if the product is old fashion configurable, false  otherwise.
     */
    public boolean isItemOldFashionConfigurable(){
        String method = "isItemOldFashionConfigurable()";
        log.entering(method);
        boolean isConfigurable = false;
        if (catalogItem != null) {
            isConfigurable = catalogItem.isItemOldFashionConfigurable();
        }
		if (log.isDebugEnabled()) {
			log.debug(method + ": is old fashioned configurable = " + isConfigurable);
		}
        
        log.exiting();
        return isConfigurable;
    }
    
    /**
     * Checks if the item is relevant for explosion with the Solution Configurator.<br> 
     * The method implements ProductBaseData.
     * 
     * @return true, if the item is relevant for explosion.<br>
     *         false, otherwise
     */
    public boolean isRelevantForExplosion() {
        String method = "isRelevantForExplosion()";
        log.entering(method);

        boolean isRelevantForExplosion = false;
        if (catalogItem != null) {
            isRelevantForExplosion = catalogItem.isRelevantForExplosion();
        }
		if (log.isDebugEnabled()) {
			log.debug(method + ": is relevant for explosion = " + isRelevantForExplosion);
		}
        
        log.exiting();
        return isRelevantForExplosion;
    }

    /**
     * returns true if the item has sales component product, false otherwise
     * 
     * @deprecated
     */
    public boolean hasSalesComponents() {
        String method = "hasSalesComponents()";
        log.entering(method);

        boolean hasSalesComponents = false;
        if (catalogItem != null && catalogItem.getWebCatSubItemList() != null) {
            Iterator it = catalogItem.getWebCatSubItemList().iterator();
            WebCatSubItem subItem = null;
            while(it.hasNext()) {
                subItem = (WebCatSubItem)it.next();
                if(subItem.isSalesComponent() ||
                   subItem.isCombinedRatePlan()) {
                    hasSalesComponents = true;
                    break;
                }
            }
        }
		if (log.isDebugEnabled()) {
			log.debug(method + ": has sales components = " + hasSalesComponents);
		}
        
        log.exiting();
        return hasSalesComponents;
    }

    /**
     * returns true if the item has sales component product, false otherwise
     */
    public boolean hasDefaultComponents() {
        String method = "hasDefaultComponents()";
        log.entering(method);

        boolean hasDefaultComponents = false;
        if (catalogItem != null && catalogItem.getWebCatSubItemList() != null) {
            Iterator it = catalogItem.getWebCatSubItemList().iterator();
            WebCatSubItem subItem = null;
            while(it.hasNext()) {
                subItem = (WebCatSubItem)it.next();
                if((subItem.isSalesComponent() || subItem.isRatePlanCombination()) &&
                   subItem.isScSelected()) {
                    hasDefaultComponents = true;
                    break;
                }
            }
        }
		if (log.isDebugEnabled()) {
			log.debug(method + ": has default components = " + hasDefaultComponents);
		}
        
        log.exiting();
        return hasDefaultComponents;
    }

    /**
     * returns the list of sales component product for an item with webCatSubItems
     */
    public Iterator getSalesComponentsProduct() {
       String method = "getSalesComponentsProduct()";
       log.entering(method);

       ArrayList arrayList = new ArrayList();
       if (catalogItem != null && catalogItem.getWebCatSubItemList() != null) {
          Iterator it = catalogItem.getWebCatSubItemList().iterator();
          WebCatSubItem subItem = null;
          while(it.hasNext()) {
             subItem = (WebCatSubItem)it.next();
             if(subItem.isSalesComponent()) {
                arrayList.add(subItem);
				if (log.isDebugEnabled()) {
					log.debug(method+": add subItem: "+ subItem.getItemId());
				}
             }
          }
       }
       log.exiting();
       return arrayList.iterator();
    }

    /**
     * Checks, if the item is a Sales Package.
     * The method implements ProductBaseData
     * 
     * @return  true, if the item is a Sales Package
     *          false, otherwise.
     */
    public boolean isSalesPackage() {
        String method = "isSalesPackage()";
        log.entering(method);

        boolean isSalesPackage = false;
        if (catalogItem != null) {
            isSalesPackage = catalogItem.isSalesPackage();
        }
		if (log.isDebugEnabled()) {
			log.debug(method + ": is a Sales Package = " + isSalesPackage);
		}
        
        log.exiting();
        return isSalesPackage;
    }
    
    /**
    * returns the default contract duration for this WebCatItem
    * 
    * @return the default contract duration for this WebCatItem
    */
   public ContractDuration getDefaultContractDuration() {
       log.entering("getDefaultContractDuration");
       ContractDuration defContractDuration = null;
       if (catalogItem != null) {
           defContractDuration = catalogItem.getDefaultContractDuration();
       }
       log.exiting();
       return defContractDuration;
   }

   /**
    * returns the table of all contract duration for this WebCatItem
    * 
    * @return the array with the different contract duration for this WebCatItem
    */
   public ArrayList getContractDurationArray() {
       log.entering("getContractDurationArray");
       ArrayList contractDurationArray = null;
       if (catalogItem != null) {
           contractDurationArray = catalogItem.getContractDurationArray();
       }
       log.exiting();
       return contractDurationArray;
   }

   /**
     * returns the selected contract duration for this WebCatItem. 
     * The selected contract duration is set in the JSP pages.
     * 
     * @return the selected contract duration for this WebCatItem
     */
   public ContractDuration getSelectedContractDuration() {
       log.entering("getSelectedContractDuration");
       ContractDuration contractDuration = null;
       if (catalogItem != null) {
           contractDuration = catalogItem.getSelectedContractDuration();
       }
       log.exiting();
       return contractDuration;
   }

   /**
    * sets the default contract duration
    * 
    * @param defaultContractDuration : the defaultContractDuration for this WebCatItem
    * 
    */
   public void setDefaultContractDuration(ContractDuration defaultContractDuration) {
       log.entering("setDefaultContractDuration");
       
       if (catalogItem != null) {
           catalogItem.setDefaultContractDuration(defaultContractDuration);
       } 
       log.exiting();   
   }

   /**
    * sets the contract duration array
    * 
    * @param contractDurationArray : the contractDurationArray for this WebCatItem
    * 
    */
   public void setContractDurationArray(ArrayList contractDurationArray) {
       log.entering("setContractDurationArray");
       
       if (catalogItem != null) {
           catalogItem.setContractDurationArray( contractDurationArray);
       }
       log.exiting();    
   }

   /**
    * sets the selected contract duration
    * 
    * @param selectedContractDuration : the selectedContractDuration for this WebCatItem
    * 
    */
   public void setSelectedContractDuration(ContractDuration selectedContractDuration) {
       log.entering("setSelectedContractDuration");
       
       if (catalogItem != null) {
           catalogItem.setSelectedContractDuration(selectedContractDuration);
       }   
       log.exiting(); 
   }

   public boolean isSubComponent() {
       log.entering("isSubComponent()");
       log.exiting();
       if(catalogItem != null ) {
           return catalogItem.isSubComponent();
       }
       return false;
   }
   
   /**
    * Returns all attributes with key <code>AttributeKeyConstants.UNIT_OF_MEASUREMENT</code>.
    *
    * @return array with attributes
    */
    public String[] getUnitsOfMeasurement() {
        log.entering("getUnitsOfMeasurement()");
        log.exiting();
        if(catalogItem != null ) {
           return catalogItem.getUnitsOfMeasurement();
        }
        return new String[]{};  
    }
    /**
     * Returns the information, if this <code>WebCatSubItem</code> is a selected by the 
     * solution configurator.
     * The Method implements ProductBaseData.
     * 
     * @return <code>true</code>, if the item is selected by the solution configurator
     *         <code>false</code>, if not
     * 
     */
    public boolean isScSelected() {
        log.entering("isScSelected()");
        boolean isScSelected = false;
        if (catalogItem != null) {
            isScSelected = catalogItem.isScSelected();
        }
        log.exiting();
        return isScSelected;
    }

    /**
     * Returns subTitel of the product
     *
     * @return subtitle
     */
    public String getSubtitle() {
        log.entering("getSubtitle()");
        String subtitle = "";
        if (catalogItem != null) {
            subtitle = catalogItem.getSubtitle();
        }
        log.exiting();
        return subtitle;
    }

    /**
     * Returns the points of the product
     * @param loyPtCodeId, Point Code as String
     * @return Points as String
     */
    public String getPoints(String loyPtCodeId) {
        log.entering("getPoints()");

        String points = null;
        if (catalogItem != null) {
            points = catalogItem.getPoints(loyPtCodeId);
        }

        log.exiting();
        return points;
    }

    /**
     * Checks if the items is a points item of a reward area
     * 
     * @return isPtsItem flag as boolean
     */
    public boolean isPtsItem() {
        log.entering("isPtsItem()");

        boolean isPtsItem = false;
        if (catalogItem != null) {
            isPtsItem = catalogItem.isPtsItem();
        }
    
        log.exiting();
        return isPtsItem;
    }


    /**
     * Checks if the items is a buy points item of a buy points area
     * 
     * @return isBuyPtsItem flag as boolean
     */
    public boolean isBuyPtsItem() {
        log.entering("isBuyPtsItem()");
    
        boolean isBuyPtsItem = false;
        if (catalogItem != null) {
            isBuyPtsItem = catalogItem.isBuyPtsItem();
        }
    
        log.exiting();
        return isBuyPtsItem;
    }

}
