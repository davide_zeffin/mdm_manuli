/*****************************************************************************
    Class:        HeaderNegotiatedContract
    Copyright (c) 2002, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Created:      31.10.2002
    Version:      1.0

    $Revision:$
    $Date:$
*****************************************************************************/
package com.sap.isa.businessobject.header;

import com.sap.isa.backend.boi.isacore.negotiatedcontract.HeaderNegotiatedContractData;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.core.util.table.ResultData;


public class HeaderNegotiatedContract extends HeaderSalesDocument implements HeaderNegotiatedContractData {

  /* obsolet, to be deleted
    private String status;
    private String reference;
    private String oldComments;
    private String newComments;
    private String targetValue;
   end of deletion */

    private String contractEnd;
    private String contractStart;
    private String rejectionReason;  
    


    public static final String DOCUMENT_STATUS_REJECTED      = "rejected";

    public static final String DOCUMENT_STATUS_INQUIRY_SENT      = "inquirysent";

    public static final String DOCUMENT_STATUS_INQUIRY_CONSTRUCTION      = "inquiryconstruction";
 	
    //public static final String QUANTITY_RELATED_CONTRACT     = "quantitiyrelated";
    
   // public static final String VALUE_RELATED_CONTRACT     = "valuerelated";




 /*   obsolet: should be deleted    */

     /**
     * Get new comments on the header level of the document.
     *
     * @return the new comments
     */
/*    public String getNewComments() {
        return newComments;
    }
*/
    /**
     * Set the new comments on the header level of the document.
     *
     * @param new comments the new comments to be set
     */
/*    public void setNewComments(String newComments) {
        this.newComments = newComments;
    }
*/
    /**
     * Get old comments on the header level of the document.
     *
     * @return the old comments
     */
/*    public String getOldComments() {
        return oldComments;
    }
*/
    /**
     * Set the old comments on the header level of the document.
     *
     * @param old comments the old comments to be set
     */
/*    public void setOldComments(String oldComments) {
        this.oldComments = oldComments;
    }
*/
    /**
     * Set the new comments on the header level of the document.
     *
     * @param new comments the new comments to be set
     */
/*    public void setReference(String reference) {
        this.reference = reference;
    }
*/
    /**
     * Set the new comments on the header level of the document.
     *
     * @param new comments the new comments to be set
     */
/*    public String getReference() {
        return this.reference;
    }
*/
/*    end of deletion  */



   /**
     * Get target values on the header level of the document.
     *
     * @return the target value
     */
/*    public String getTargetValue() {
        return targetValue;
    }
*/
    /**
     * Set the new comments on the header level of the document.
     *
     * @param new comments the new comments to be set
     */
/*    public void setTargetValue(String targetValue) {
        this.targetValue = targetValue;
    }
*/

    /**
     * Set OVERALL status to rejected
     */
/*    public void setStatusRejected() {
        status = DOCUMENT_STATUS_REJECTED;
    }
*/
    /**
     * Determines whether or not, the document's status is rejected.
     *
     * @return <code>true</code> if the object is in status rejected, otherwise
     *         <code>false</code>.
     */
/*    public boolean isStatusRejected() {
        return (status == DOCUMENT_STATUS_REJECTED);
    }
*/


    /**
     * set OVERALL status to Inquiry Sent
     */
/*    public void setStatusInquirySent() {
        status = DOCUMENT_STATUS_INQUIRY_SENT;
    }
*/
    /**
     * Determines whether or not, the document's status is Inquiry Sent.
     *
     * @return <code>true</code> if the object is in status Inquiry Sent, otherwise
     *         <code>false</code>.
     */
/*    public boolean isStatusInquirySent() {
        return (status == DOCUMENT_STATUS_INQUIRY_SENT);
    }
*/

     /**
     * set OVERALL status to Inquiry Construction
     */
/*    public void setStatusInquiryConstruction() {
        status = DOCUMENT_STATUS_INQUIRY_CONSTRUCTION;
    }
*/
    /**
     * Determines whether or not, the document's status is Inquiry Construction.
     *
     * @return <code>true</code> if the object is in status Inquiry Construction, otherwise
     *         <code>false</code>.
     */
/*    public boolean isStatusInquiryConstruction() {
        return (status == DOCUMENT_STATUS_INQUIRY_CONSTRUCTION);
    }
*/

    /**
     * Sets the document type to ORDER.
     */
    //public void setDocumentTypeNegotiatedContract(){
    //    documentType = DOCUMENT_TYPE_NEGOTIATEDCONTRACT;
    //}


    /**
     * Determines whether or not, the document's type is ORDER.
     *
     * @return <code>true</code> if the type is ORDER, otherwise
     *         <code>false</code>.
     */
    //public boolean isDocumentTypeNegotiatedContract() {
    //    return (documentType == DOCUMENT_TYPE_NEGOTIATEDCONTRACT);
    //}


	/**
	 * Returns the contractEnd.
	 * @return String
	 */
	public String getContractEnd() {
        //contractEnd="12.12.2003";
		return contractEnd;
	}

	/**
	 * Returns the contractStart.
	 * @return String
	 */
	public String getContractStart() {
        //contractStart="11.11.2002";
		return contractStart;
	}

	/**
	 * Sets the contractEnd.
	 * @param contractEnd The contractEnd to set
	 */
	public void setContractEnd(String contractEnd) {
		this.contractEnd = contractEnd;
	}

	/**
	 * Sets the contractStart.
	 * @param contractStart The contractStart to set
	 */
	public void setContractStart(String contractStart) {
		this.contractStart = contractStart;
	}

	/**
	 * Returns the rejectionReason.
	 * @return String
	 */
	public String getRejectionReason() {
		return rejectionReason;
	}

	/**
	 * Sets the rejectionReason.
	 * @param rejectionReason The rejectionReason to set
	 */
	public void setRejectionReason(String rejectionReason) {
		this.rejectionReason = rejectionReason;
	}
    
    
    /**
     * Returns the type of the contract (quantity or validity related contract).
     * @param rejectionReason The rejectionReason to set
     */
    public String getContractType(Shop shop) {
		final String METHOD = "getContractType()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("shop="+shop);
        String contractType = "";
    
        ResultData processTypes = shop.getContractNegotiationProcessTypes();
        if (processTypes.first()) {
           if ( (processTypes.searchRowByColumn(1, this.getProcessType())) == true) {
              contractType = processTypes.getString(3); 
           }
        }
        log.exiting();
        return contractType;
    }
    
    
    

}
