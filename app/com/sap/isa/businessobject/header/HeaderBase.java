/*****************************************************************************
    Class:        HeaderBase
    Copyright (c) 2001, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      11.4.2001
    Version:      1.0

    $Revision: #12 $
    $Date: 2003/01/02 $
*****************************************************************************/

package com.sap.isa.businessobject.header;

import java.util.ArrayList;
import java.util.List;

import com.sap.isa.backend.boi.isacore.ConnectedDocumentData;
import com.sap.isa.backend.boi.isacore.order.ExtRefObjectListData;
import com.sap.isa.backend.boi.isacore.order.ExternalReferenceListData;
import com.sap.isa.backend.boi.isacore.order.HeaderBaseData;
import com.sap.isa.backend.boi.isacore.order.PartnerListData;
import com.sap.isa.backend.boi.isacore.order.TextData;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.businessobject.ConnectedDocument;
import com.sap.isa.businessobject.order.ExtRefObjectList;
import com.sap.isa.businessobject.order.ExternalReferenceList;
import com.sap.isa.businessobject.order.PartnerList;
import com.sap.isa.businessobject.order.PartnerListEntry;
import com.sap.isa.businessobject.order.Text;
import com.sap.isa.core.TechKey;

/**
 * Common Header Information for all objects of the bo layer that are considered
 * to be sales documents.
 *
 * @author SAP AG
 * @version 1.0
 */
public class HeaderBase extends BusinessObjectBase  implements HeaderBaseData, Cloneable {

	//final static protected IsaLocation log = IsaLocation.getInstance(HeaderBase.class.getName());


    protected boolean changeable;        // Document changeable in general (R/3 documents are not changeable yet)
    protected String changedAt;
    protected String createdAt;
    protected String currency;
    protected String description;
    protected String division;
    protected String disChannel;
    protected String documentType;
    protected String freightValue;
    protected String grossValue;
    protected String netValue;
    protected String netValueWOFreight;
    protected PartnerList partnerList = new PartnerList();
    protected String processType;
    protected String processTypeDesc;
    protected String processTypeUsage;
    protected String salesDocNumber;
    protected String purchaseOrderExt;
    protected String postingDate;
    protected String salesDocumentsOrigin;
    protected String salesOrg;
    protected String salesOffice;
    protected String reqDeliveryDate;
    protected String contractStartDate;
    protected String shipCond;
    protected String status;
    protected String taxValue;
    protected Text text;
    protected Text textHistory;
    protected String validTo;
    protected String deliveryPriority;
    protected boolean purchaseOrderChangeable;
    protected boolean purchaseOrderExtChangeable;
    protected boolean salesOrgChangeable;
    protected boolean salesOfficeChangeable;
    protected boolean reqDeliveryDateChangeable;
    protected boolean contractStartDateChangeable;
    protected boolean shipCondChangeable;
    protected boolean descriptionChangeable;
	protected boolean textChangeable;
	protected boolean deliveryPriorityChangeable;
	protected ArrayList predecessorList = new ArrayList();
    protected ArrayList successorList = new ArrayList();
	//external references (to external documents)  
    protected ExternalReferenceList assignedExternalReferences = new ExternalReferenceList();
    protected boolean assignedExternalReferencesChangeable;
	//external reference objects
	protected String extRefObjectType;
	protected String extRefObjectTypeDesc;
	protected ExtRefObjectList assignedExtRefObjects = new ExtRefObjectList();
	protected boolean assignedExtRefObjectsChangeable;    
	protected boolean isDirty = false; // must header be read from backend ?
	protected ArrayList attachmentList = new ArrayList();
    protected String recurringNetValue;
    protected String recurringTaxValue;
    protected String recurringGrossValue;
    protected String recurringDuration;
    protected String recurringTimeUnit;
    protected String recurringTimeUnitAbr;
    protected String recurringTimeUnitPlural;
    protected String nonRecurringNetValue;
    protected String nonRecurringTaxValue;
    protected String nonRecurringGrossValue;

    /**
     * Drops the state of the object. All reference fields, except partnerList, are
     * set to null, all primitive types are set to the default values they would have
     * after the creation of a new instance. Use this method to reset the state to the
     * state a newly created object would have. The advantage is, that the
     * overhead caused by the normal object creation is omitted.
     */
    public void clear() {
        changedAt                   = null;
        handle						= null;
        createdAt                   = null;
        currency                    = null;
        description                 = null;
        division                    = null;
        disChannel                  = null;
        documentType                = null;
        freightValue                = null;
        grossValue                  = null;
        netValue                    = null;
        partnerList.clearList();
        processType                 = null;
        salesDocNumber              = null;
        purchaseOrderExt            = null;
        postingDate                 = null;
        salesDocumentsOrigin        = null;
        salesOrg                    = null;
        salesOffice                 = null;
        reqDeliveryDate             = null;
        contractStartDate           = null;
        shipCond                    = null;
        status                      = null;
        taxValue                    = null;
        text                        = null;
        validTo                     = null;
        deliveryPriority			= null;
		assignedExtRefObjects.clear();
		assignedExternalReferences.clear();
        purchaseOrderChangeable     = false;
        purchaseOrderExtChangeable  = false;
        salesOrgChangeable          = false;
        salesOfficeChangeable       = false;
        reqDeliveryDateChangeable   = false;
        contractStartDateChangeable = false;
        shipCondChangeable          = false;
        descriptionChangeable       = false;
        changeable                  = false;
		deliveryPriorityChangeable	= false;
		assignedExternalReferencesChangeable = false;
        removeExtensionDataValues();
        predecessorList.clear();
        successorList.clear();
        recurringNetValue			= null;
        recurringTaxValue			= null;
        recurringGrossValue			= null;
        recurringDuration			= null;
        recurringTimeUnit			= null;
        nonRecurringNetValue		= null;
        nonRecurringTaxValue		= null;
        nonRecurringGrossValue		= null;
        isDirty = true;
        /*
                ipcHost                     = null;
                ipcPort                     = null;
                ipcSession                  = null;
                ipcClient                   = null;
                ipcDocumentId               = null;
        */
    }

    /**
     * Creates a new <code>Text</code> object casted to the backend enabled
     * <code>TextData</code> interface. This method is normally only used
     * by backend implementations as a factory method to get instances of
     * business objects.
     *
     * @return a newly created <code>Text</code> object
     */
    public TextData createText() {
        return (TextData) new Text();
    }

    /**
     * Get the requested delivery date.
     *
     * @return the requestet delivery date
     */
    public String getReqDeliveryDate() {
        return reqDeliveryDate;
    }

    /**
     * Set the requested delivery date.
     *
     * @param reqDeliveryDate the requested delivery date to be set.
     */
    public void setReqDeliveryDate(String reqDeliveryDate) {
        this.reqDeliveryDate = reqDeliveryDate;
    }

    /**
     * Get the business partner list
     *
     * @return PartnerList list of business partners
     */
    public PartnerList getPartnerList() {
        return partnerList;
    }

    /**
     * Set the business partner list
     *
     * @param PartnerList new list of business partners
     */
    public void setPartnerList(PartnerList partnerList) {
        if (partnerList != null) {
            this.partnerList = partnerList;;
        }
        else {
            this.partnerList.clearList();
        }
    }

    /**
     * Get the business partner list
     *
     * @return PartnerListData list of business partners
     */
    public PartnerListData getPartnerListData() {
        return partnerList;
    }

    /**
     * Sets the business partner list.
     *
     * @param list list of business partners
     */
    public void setPartnerListData(PartnerListData list) {
        partnerList = (PartnerList) list;
    }

    /**
     * Get the date, the document was changed the last time.
     *
     * @return the date, the document was changed the last time
     */
    public String getChangedAt() {
        return changedAt;
    }
    
    /**
     * Set the date, the document was changed the last time.
     *
     * @param changedAt the date, the document was changed the last time
     */
    public void setChangedAt(String changedAt){
        this.changedAt = changedAt;
    }
    
    /**
     * Get the contract start date.
     *
     * @return the contract start date
     */
    public String getContractStartDate() {
        return contractStartDate;
    }
    
    /**
     * Set the contract start date.
     *
     * @param contractStartDate the contract start date
     */
    public void setContractStartDate(String contractStartDate) {
        this.contractStartDate = contractStartDate;
    }

    /**
     * Get the date, the document was created.
     *
     * @return date, the document was created
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * Set the date, the document was cerated.
     *
     * @param createdAt the date, the document was cerated
     */
    public void setCreatedAt(String createdAt){
        this.createdAt = createdAt;
    }

    /**
     * Get the currency used for this document.
     *
     * @return the currency used for this document
     */
    public String getCurrency(){
        return currency;
    }

    /**
     * Set the currency used for this document.
     *
     * @param currency the currency used for this document
     */
    public void setCurrency(String currency){
        this.currency = currency;
    }

    /**
     * Get description added on the header level.
     *
     * @return description
     */
    public String getDescription(){
        return description;
    }

    /**
     * Sets the description on the header level.
     *
     * @param description the description
     */
    public void setDescription(String description){
        this.description = description;
    }

    /**
     * Get the devision.
     *
     * @return the devision
     */
    public String getDivision(){
        return division;
    }

    /**
     * Set the devision.
     *
     * @param devision the devision to be set
     */
    public void setDivision(String division){
        this.division = division;
    }

    /**
     * Get the distribution channel.
     *
     * @return distribution channel
     */
    public String getDisChannel(){
        return disChannel;
    }

    /**
     * Set the distribution channel.
     *
     * @param disChannel the distribution channel to be set
     */
    public void setDisChannel(String disChannel){
        this.disChannel = disChannel;
    }

    /**
     * Get the price for the freigth of the order.
     *
     * @return the price for the freight of the
     */
    public String getFreightValue(){
        return freightValue;
    }

    /**
     * Set the price for the freight of the order.
     *
     * @param freightValue the price for the freight
     */
    public void setFreightValue(String freightValue){
        this.freightValue = freightValue;
    }

    /**
     * Get the price including all taxes but not the freight.
     *
     * @return the value
     */
    public String getGrossValue(){
        return grossValue;
    }

    /**
     * Set the price including all taxes but not the freight.
     *
     * @param grossValue the price to be set
     */
    public void setGrossValue(String grossValue){
        this.grossValue = grossValue;
    }

    /**
     * Get the net price
     *
     * @return the price
     */
    public String getNetValue(){
        return netValue;
    }

    /**
     * Set the net price
     *
     * @param netValue the price to be set
     */
    public void setNetValue(String netValue){
        this.netValue = netValue;
    }

    /**
     * Get the net price without freight.
     *
     * @return the price
     */
    public String getNetValueWOFreight(){
        return netValueWOFreight;
    }

    /**
     * Set the net price without freight.
     *
     * @param netValue the price to be set
     */
    public void setNetValueWOFreight(String netValueWOFreight){
        this.netValueWOFreight = netValueWOFreight;
    }


    /**
     * Get the process type of the document.
     *
     * @return process type
     */
    public String getProcessType(){
        return processType;
    }

    /**
     * Set the process type of the document.
     *
     * @param processType the process type to be set
     */
    public void setProcessType(String processType){
        this.processType = processType;
    }

    /**
     * Get the number of the sales document the header belongs to.
     *
     * @return the number of the sales document
     */
    public String getSalesDocNumber(){
        return salesDocNumber;
    }

    /**
     * Set the number of the sales document the header belongs to.
     *
     * @param salesDocNumber the number of the sales document
     */
    public void setSalesDocNumber(String salesDocNumber){
        this.salesDocNumber = salesDocNumber;
    }

    /**
     * Get the external pruchase order number.
     *
     * @return the purchase order number
     */
    public String getPurchaseOrderExt(){
        return purchaseOrderExt;
    }

    /**
     * Set the external purchase order number.
     *
     * @param purchaseOrderExt the number to be set
     */
    public void setPurchaseOrderExt(String purchaseOrderExt){
        this.purchaseOrderExt = purchaseOrderExt;
    }

    /**
     * Get the date the order was created from the customer's point of view.
     *
     * @return the posting date
     */
    public String getPostingDate(){
        return postingDate;
    }

    /**
     * Set the date the order was created from the customer's point of view.
     *
     * @param postingData the date to be set
     */
    public void setPostingDate(String postingDate){
        this.postingDate = postingDate;
    }

    /**
     * Get the sales organization for the document.
     *
     * @return the sales organization
     */
    public String getSalesOrg(){
        return salesOrg;
    }

    /**
     * Set the sales organization for the document.
     *
     * @param salesOrg the sales organization.
     */
    public void setSalesOrg(String salesOrg){
        this.salesOrg = salesOrg;
    }

    /**
     * Get the sales office.
     *
     * @return the sales office
     */
    public String getSalesOffice(){
        return salesOffice;
    }

    /**
     * Set the sales office.
     *
     * @param salesOffice the sales office
     */
    public void setSalesOffice(String salesOffice){
        this.salesOffice = salesOffice;
    }

    /**
     * Get the shipping conditions for the document.
     *
     * @return the shipping conditions
     */
    public String getShipCond(){
        return shipCond;
    }

    /**
     * Set the shipping conditions for the document.
     *
     * @param shipCond the shipping conditions
     */
    public void setShipCond(String shipCond){
        this.shipCond = shipCond;
    }

    /**
     * Get the taxes that have to be paid for the document.
     *
     * @return the taxes
     */
    public String getTaxValue(){
        return taxValue;
    }

    /**
     * Set the taxes that have to be paid for the document.
     *
     * @param taxValue the taxes to be set
     */
    public void setTaxValue(String taxValue){
        this.taxValue = taxValue;
    }

    /**
     * Get text on the header level of the document.
     *
     * @return the text
     */
    public TextData getText() {
        return text;
    }

    /**
     * Set the text on the header level of the document.
     *
     * @param text the text to be set
     */
    public void setText(TextData text) {
        this.text = (Text) text;
    }

    /**
      * Get text history on the header level of the document.
      *
      * @return the text history
      */
    public TextData getTextHistory() {
        return textHistory;
    }

    /**
     * Set the text history on the header level of the document.
     *
     * @param text the text history to be set
     */
    public void setTextHistory(TextData text) {
        this.textHistory = (Text) text;
    }
    /**
     * Get the date, the document is valid to.
     *
     * @return the date
     */
    public String getValidTo(){
        return validTo;
    }

    /**
     * Set the date, the document is valid to.
     *
     * @param validTo the date to be set
     */
    public void setValidTo(String validTo){
        this.validTo = validTo;
    }

    /**
     * Check, if there is a header text.
     * 
     * @return true, if there is a text
     *         false, otherwise
     */
    public boolean hasText() {
    	
		if (this.text == null)           
			return false;
		    
		if (this.text.getText() == null) 
			return false;
			
		return getText().getText().length() > 0;
    }
    
    /**
     * Check whether or not the document is changeable.
     *
     * @return <code>true</code> if the document is changeable,
     *         otherwise <code>false</code>.
     */
    public boolean isChangeable(){
        return changeable;
    }

    /**
     * Set whether or not the document is changeable using a String parameter.
     *
     * @param changeable <code>" "</code> or <code>""</code> indicates that the
     *                   document is changeable, all other values that it is
     *                   changeable.
     */
    public void setChangeable(String changeable){
        if(changeable.equals("") || changeable.equals(" ")) {
            this.changeable = false;
        }
        else {
            this.changeable = true;
        }
    }

    /**
     * Set whether or not the document is changeable.
     *
     * @param changeable <code>true</code> indicates that the document is changeable,
     *                   <code>false</code> indicates that it is not.
     */
    public void setChangeable(boolean changeable){
        this.changeable = changeable;
    }

    /**
     * Sets all changeable flags for the different fields of the header in
     * on step.
     *
     * @param purchaseOrderChangeable <code>true</code> indicates that the
     *        purchase order information is changeable
     * @param purchaseOrderExtChangeable <code>true</code> indicates that the
     *        external purchase order information is changeable
     * @param salesOrgChangeable <code>true</code> indicates that the
     *        sales organization information is changeable
     * @param salesOfficeChangeable <code>true</code> indicates that the
     *        sales office information is changeable
     * @param reqDeliveryDateChangeable <code>true</code> indicates that the
     *        requested delivery date is changeable
     * @param shipCondChangeable <code>true</code> indicates that the
     *        shipping conditions are changeable
     * @param descriptionChangeable <code>true</code> indicates that the
     *        description is changeable
     */
    public void setAllValuesChangeable(
                                      boolean purchaseOrderChangeable,
                                      boolean purchaseOrderExtChangeable,
                                      boolean salesOrgChangeable,
                                      boolean salesOfficeChangeable,
                                      boolean reqDeliveryDateChangeable,
                                      boolean shipCondChangeable,
                                      boolean descriptionChangeable) {

        this.purchaseOrderChangeable     = purchaseOrderChangeable;
        this.purchaseOrderExtChangeable  = purchaseOrderExtChangeable;
        this.salesOrgChangeable          = salesOrgChangeable;
        this.salesOfficeChangeable       = salesOfficeChangeable;
        this.reqDeliveryDateChangeable   = reqDeliveryDateChangeable;
        this.shipCondChangeable          = shipCondChangeable;
        this.descriptionChangeable       = descriptionChangeable;
    }
    
	/**
	 * Sets all changeable flags for the different fields of the header in
	 * on step.
	 *
	 * @param purchaseOrderChangeable <code>true</code> indicates that the
	 *        purchase order information is changeable
	 * @param purchaseOrderExtChangeable <code>true</code> indicates that the
	 *        external purchase order information is changeable
	 * @param salesOrgChangeable <code>true</code> indicates that the
	 *        sales organization information is changeable
	 * @param salesOfficeChangeable <code>true</code> indicates that the
	 *        sales office information is changeable
	 * @param reqDeliveryDateChangeable <code>true</code> indicates that the
	 *        requested delivery date is changeable
	 * @param shipCondChangeable <code>true</code> indicates that the
	 *        shipping conditions are changeable
	 * @param descriptionChangeable <code>true</code> indicates that the
	 *        description is changeable
	 * @param textChangeable <code>true</code> indicates that the
	 *        text is changeable
	 * 
	 * @deprecated  
	 */
	public void setAllValuesChangeable(
									  boolean purchaseOrderChangeable,
									  boolean purchaseOrderExtChangeable,
									  boolean salesOrgChangeable,
									  boolean salesOfficeChangeable,
									  boolean reqDeliveryDateChangeable,
									  boolean shipCondChangeable,
									  boolean descriptionChangeable,
	                                  boolean textChangeable) {

		this.purchaseOrderChangeable     = purchaseOrderChangeable;
		this.purchaseOrderExtChangeable  = purchaseOrderExtChangeable;
		this.salesOrgChangeable          = salesOrgChangeable;
		this.salesOfficeChangeable       = salesOfficeChangeable;
		this.reqDeliveryDateChangeable   = reqDeliveryDateChangeable;
		this.shipCondChangeable          = shipCondChangeable;
		this.descriptionChangeable       = descriptionChangeable;
		this.textChangeable              = textChangeable;
	}

	/**
	 * Sets all changeable flags for the different fields of the header in
	 * on step.
	 *
	 * @param purchaseOrderChangeable <code>true</code> indicates that the
	 *        purchase order information is changeable
	 * @param purchaseOrderExtChangeable <code>true</code> indicates that the
	 *        external purchase order information is changeable
	 * @param salesOrgChangeable <code>true</code> indicates that the
	 *        sales organization information is changeable
	 * @param salesOfficeChangeable <code>true</code> indicates that the
	 *        sales office information is changeable
	 * @param reqDeliveryDateChangeable <code>true</code> indicates that the
	 *        requested delivery date is changeable
	 * @param shipCondChangeable <code>true</code> indicates that the
	 *        shipping conditions are changeable
	 * @param descriptionChangeable <code>true</code> indicates that the
	 *        description is changeable
	 * @param textChangeable <code>true</code> indicates that the
	 *        text is changeable
	 * @param deliveryPriorityChangeable <code>true</code> indicates that the 
	 * 		  delivery priority is changeable
	 * @param extRefObjectChangeable <code>true</code> indicates that the
	 * 		  external reference object list is changeable
	 * @param externalReferencesChangeable <code>true</code> indicates that the
	 * 		  external reference number list is changeable 
	 *	 */
	public void setAllValuesChangeable(
									  boolean purchaseOrderChangeable,
									  boolean purchaseOrderExtChangeable,
									  boolean salesOrgChangeable,
									  boolean salesOfficeChangeable,
									  boolean reqDeliveryDateChangeable,
									  boolean shipCondChangeable,
									  boolean descriptionChangeable,
									  boolean textChangeable,
									  boolean deliveryPriorityChangeable,
									  boolean extRefObjectChangeable,
									  boolean externalReferencesChangeable) {

		this.purchaseOrderChangeable     = purchaseOrderChangeable;
		this.purchaseOrderExtChangeable  = purchaseOrderExtChangeable;
		this.salesOrgChangeable          = salesOrgChangeable;
		this.salesOfficeChangeable       = salesOfficeChangeable;
		this.reqDeliveryDateChangeable   = reqDeliveryDateChangeable;
		this.shipCondChangeable          = shipCondChangeable;
		this.descriptionChangeable       = descriptionChangeable;
		this.textChangeable              = textChangeable;
		this.deliveryPriorityChangeable	 = deliveryPriorityChangeable;
		this.assignedExtRefObjectsChangeable = extRefObjectChangeable;
		this.assignedExternalReferencesChangeable = externalReferencesChangeable;
	}
    
    /**
     * Sets all changeable flags for the different fields of the header in
     * on step.
     *
     * @param purchaseOrderChangeable <code>true</code> indicates that the
     *        purchase order information is changeable
     * @param purchaseOrderExtChangeable <code>true</code> indicates that the
     *        external purchase order information is changeable
     * @param salesOrgChangeable <code>true</code> indicates that the
     *        sales organization information is changeable
     * @param salesOfficeChangeable <code>true</code> indicates that the
     *        sales office information is changeable
     * @param reqDeliveryDateChangeable <code>true</code> indicates that the
     *        requested delivery date is changeable
     * @param shipCondChangeable <code>true</code> indicates that the
     *        shipping conditions are changeable
     * @param descriptionChangeable <code>true</code> indicates that the
     *        description is changeable
     * @param textChangeable <code>true</code> indicates that the
     *        text is changeable
     * @param deliveryPriorityChangeable <code>true</code> indicates that the 
     *        delivery priority is changeable
     * @param extRefObjectChangeable <code>true</code> indicates that the
     *        external reference object list is changeable
     * @param externalReferencesChangeable <code>true</code> indicates that the
     *        external reference number list is changeable 
     * @param contractStartDateChangeable <code>true</code> indicates that the
     *        contractStartDateChangeable is changeable 
     *   */
    public void setAllValuesChangeable(
                                      boolean purchaseOrderChangeable,
                                      boolean purchaseOrderExtChangeable,
                                      boolean salesOrgChangeable,
                                      boolean salesOfficeChangeable,
                                      boolean reqDeliveryDateChangeable,
                                      boolean shipCondChangeable,
                                      boolean descriptionChangeable,
                                      boolean textChangeable,
                                      boolean deliveryPriorityChangeable,
                                      boolean extRefObjectChangeable,
                                      boolean externalReferencesChangeable,
                                      boolean contractStartDateChangeable) {

        this.purchaseOrderChangeable     = purchaseOrderChangeable;
        this.purchaseOrderExtChangeable  = purchaseOrderExtChangeable;
        this.salesOrgChangeable          = salesOrgChangeable;
        this.salesOfficeChangeable       = salesOfficeChangeable;
        this.reqDeliveryDateChangeable   = reqDeliveryDateChangeable;
        this.shipCondChangeable          = shipCondChangeable;
        this.descriptionChangeable       = descriptionChangeable;
        this.textChangeable              = textChangeable;
        this.deliveryPriorityChangeable  = deliveryPriorityChangeable;
        this.assignedExtRefObjectsChangeable = extRefObjectChangeable;
        this.assignedExternalReferencesChangeable = externalReferencesChangeable;
        this.contractStartDateChangeable = contractStartDateChangeable;
    }
	
	/**
     * Checks whether or not the purchase order information is changeable.
     *
     * @return <code>true</code> indicates that the information is changeable,
     *         <code>false</code> indicates that the information cannot be changed.
     */
    public boolean isPurchaseOrderChangeable() {
        return purchaseOrderChangeable;
    }

    /**
     * Checks whether or not the external purchase order information is changeable.
     *
     * @return <code>true</code> indicates that the information is changeable,
     *         <code>false</code> indicates that the information cannot be changed.
     */
    public boolean isPurchaseOrderExtChangeable() {
        return purchaseOrderExtChangeable;
    }

    /**
     * Checks whether or not the sales organisation is changeable.
     *
     * @return <code>true</code> indicates that the information is changeable,
     *         <code>false</code> indicates that the information cannot be changed.
     */
    public boolean isSalesOrgChangeable() {
        return salesOrgChangeable;
    }

    /**
     * Checks whether or not the sales office is changeable.
     *
     * @return <code>true</code> indicates that the information is changeable,
     *         <code>false</code> indicates that the information cannot be changed.
     */
    public boolean isSalesOfficeChangeable() {
        return salesOfficeChangeable;
    }

    /**
     * Checks whether or not the requested delivery date is changeable.
     *
     * @return <code>true</code> indicates that the information is changeable,
     *         <code>false</code> indicates that the information cannot be changed.
     */
    public boolean isReqDeliveryDateChangeable() {
        return reqDeliveryDateChangeable;
    }

    /**
     * Checks whether or not the description is changeable.
     *
     * @return <code>true</code> indicates that the information is changeable,
     *         <code>false</code> indicates that the information cannot be changed.
     */
    public boolean isDescriptionChangeable() {
        return descriptionChangeable;
    }

    /**
     * Checks whether or not the shipping condition is changeable.
     *
     * @return <code>true</code> indicates that the information is changeable,
     *         <code>false</code> indicates that the information cannot be changed.
     */
    public boolean isShipCondChangeable() {
        return shipCondChangeable;
    }
    
	/**
	 * Checks whether or not the text is changeable.
	 *
	 * @return <code>true</code> indicates that the information is changeable,
	 *         <code>false</code> indicates that the information cannot be changed.
	 */
	public boolean isTextChangeable() {
		return textChangeable;
	}
	
	/**
	 * Sets the flag, whether or not the text is changeable.
	 */
	public void setTextChangeable(boolean textChangeable) {
		this.textChangeable = textChangeable;
	}

    /**
     * Get the origin of the sales document.
     *
     * @return the origin of the sales document
     */
    public String getSalesDocumentsOrigin(){
        return salesDocumentsOrigin;
    }

    /**
     * Set the origin of the sales document.
     *
     * @param salesDocOrigin the origin to be set
     */
    public void setSalesDocumentsOrigin(String salesDocOrigin){
        this.salesDocumentsOrigin = salesDocOrigin;
    }

    /**
     * Set the dirty flag
     *
     * @param isDirty must the header be read from the backend true/false
     */
    public void setDirty(boolean isDirty){
        this.isDirty = isDirty;
    }

    /**
     * get the dirty flag
     *
     * @return isDirty must the header be read from the backend true/false
     */
    public boolean isDirty(){
        return isDirty;
    }

    /**
     * Get OVERALL status. Might be different from DELIVERY status.
     *
     * @return one of the possible status values represented by the constants
     *         defined in this class with the names <code>DOCUMENT_COMPLETION_*</code>.
     */
    public String getStatus() {
        return status;
    }

    /**
     * Set OVERALL status to open
     */
    public void setStatusOpen() {
        status = DOCUMENT_COMPLETION_STATUS_OPEN;
    }

    /**
     * Set the status to whatever passed through
     *
     * @param status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * Determines whether or not, the document's status is OPEN.
     *
     * @return <code>true</code> if the object is in status OPEN, otherwise
     *         <code>false</code>.
     */
    public boolean isStatusOpen() {
        return (DOCUMENT_COMPLETION_STATUS_OPEN.equals(status));
    }

    /**
     * Set OVERALL status to completed
     */
    public void setStatusCompleted() {
        status = DOCUMENT_COMPLETION_STATUS_COMPLETED;
    }

    /**
     * Determines whether or not, the document's status is COMPLETED.
     *
     * @return <code>true</code> if the object is in status COMPLETED, otherwise
     *         <code>false</code>.
     */
    public boolean isStatusCompleted() {
        return (DOCUMENT_COMPLETION_STATUS_COMPLETED.equals(status));
    }


    /**
     * Set OVERALL status to accepted
     */
    public void setStatusAccepted() {
        status = DOCUMENT_COMPLETION_STATUS_ACCEPTED;
    }

    /**
     * Determines whether or not, the document's status is ACCEPTED.
     *
     * @return <code>true</code> if the object is in status ACCEPTED, otherwise
     *         <code>false</code>.
     */
    public boolean isStatusAccepted() {
        return (DOCUMENT_COMPLETION_STATUS_ACCEPTED.equals(status));
    }

    /**
     * Set OVERALL status to released
     */
    public void setStatusReleased() {
        status = DOCUMENT_COMPLETION_STATUS_RELEASED;
    }

    /**
     * Determines whether or not, the document's status is RELEASED.
     *
     * @return <code>true</code> if the object is in status RELEASED, otherwise
     *         <code>false</code>.
     */
    public boolean isStatusReleased() {
        return (DOCUMENT_COMPLETION_STATUS_RELEASED.equals(status));
    }

    /**
     * set OVERALL status to cancelled
     */
    public void setStatusCancelled() {
        status = DOCUMENT_COMPLETION_STATUS_CANCELLED;
    }


    /**
     * Determines whether or not, the document's status is CANCELLED.
     *
     * @return <code>true</code> if the object is in status CANCELLED, otherwise
     *         <code>false</code>.
     */
    public boolean isStatusCancelled() {
        return (DOCUMENT_COMPLETION_STATUS_CANCELLED.equals(status));
    }

    /**
     * set OVERALL status to rejected
     */
    public void setStatusRejected() {
        status = DOCUMENT_COMPLETION_STATUS_REJECTED;
    }

    /**
     * Determines whether or not, the document's status is REJECTED.
     *
     * @return <code>true</code> if the object is in status REJECTED, otherwise
     *         <code>false</code>.
     */
    public boolean isStatusRejected() {
        return (DOCUMENT_COMPLETION_STATUS_REJECTED.equals(status));
    }


    /**
      * Set status to in process
      */
    public void setStatusInProcess() {
        status = DOCUMENT_COMPLETION_STATUS_INPROCESS;
    }

    /**
     * Set status to in process
     */
    public boolean isStatusInProcess() {
        return (DOCUMENT_COMPLETION_STATUS_INPROCESS.equals(status));
    }


    /**
     * Returns the type of the document, the header belongs to.
     *
     * @return the document type using one of the string constants defined in
     *         this class or its subclasses: <code>DOCUMENT_TYPE_*</code>.
     */
    public String getDocumentType() {
        return documentType;
    }

    /**
     * Sets the document type.
     *
     * @param type document type to be set
     */
    public void setDocumentType(String documentType){
        this.documentType = documentType;
    }

    /**
     * create a <code>ConnectedDocumentData</code> object.
     *
     */
    public ConnectedDocumentData createConnectedDocumentData() {
        return new ConnectedDocument();
    }

    /**
     * Adds a <code>ConnectedDocument</code> to the predecessor list.
     * A <code>null</code> reference passed to this function will be ignored,
     * in this case nothing will happen.
     *
     * @param predecessorData ConnectedDocument to be added to the predecessor list
     */
    public void addPredecessor(ConnectedDocumentData predecessorData) {
        if(predecessorData != null &&
           predecessorData instanceof ConnectedDocument) {
            ConnectedDocument predecessor = (ConnectedDocument)predecessorData;
            predecessorList.add(predecessor);
        }
    }

    /**
     * Get the predecessor list
     */
    public List getPredecessorList() {
        return (List)predecessorList;
    }

    /**
     * Adds a <code>ConnectedDocument</code> to the successor list.
     * A <code>null</code> reference passed to this function will be ignored,
     * in this case nothing will happen.
     *
     * @param successorData ConnectedDocument to be added to the successor list
     */
    public void addSuccessor(ConnectedDocumentData successorData) {
        if(successorData != null &&
           successorData instanceof ConnectedDocument) {
            ConnectedDocument successor = (ConnectedDocument)successorData;
            successorList.add(successor);
        }
    }

    /**
     * Get the successor list
     */
    public List getSuccessorList() {
        return (List)successorList;
    }

    /**
     * Returns a string representation of the object.
     *
     * @return object as string
     */
    public String toString() {
        return  "HeaderBase [" +
                "changeable=\""                     + changeable                    + "\"" +
                ", changedAt=\""                    + changedAt                     + "\"" +
                ", createdAt=\""                    + createdAt                     + "\"" +
                ", currency=\""                     + currency                      + "\"" +
                ", description=\""                  + description                   + "\"" +
                ", division=\""                     + division                      + "\"" +
                ", disChannel=\""                   + disChannel                    + "\"" +
                ", freightValue=\""                 + freightValue                  + "\"" +
                ", grossValue=\""                   + grossValue                    + "\"" +
                ", netValue=\""                     + netValue                      + "\"" +
                ", processType=\""                  + processType                   + "\"" +
                ", salesDocNumber=\""               + salesDocNumber                + "\"" +
                ", partnerList=\""                  + partnerList.toString()        + "\"" +
                ", purchaseOrderExt=\""             + purchaseOrderExt              + "\"" +
                ", postingDate=\""                  + postingDate                   + "\"" +
                ", salesDocumentsOrigin=\""         + salesDocumentsOrigin          + "\"" +
                ", salesOrg=\""                     + salesOrg                      + "\"" +
                ", salesOffice=\""                  + salesOffice                   + "\"" +
                ", reqDeliveryDate=\""              + reqDeliveryDate               + "\"" +
                ", contractStartDate=\""            + contractStartDate               + "\"" +
                ", shipCond=\""                     + shipCond                      + "\"" +
                ", status=\""                       + status                        + "\"" +
                ", taxValue=\""                     + taxValue                      + "\"" +
                ", text=\""                         + text                          + "\"" +
                ", techKey=\""                      + techKey                       + "\"" +
                ", validTo=\""                      + validTo                       + "\"" +
                ", deliveryPriority=\""				+ deliveryPriority				+ "\"" +
                ", purchaseOrderChangeable=\""      + purchaseOrderChangeable       + "\"" +
                ", purchaseOrderExtChangeable=\""   + purchaseOrderExtChangeable    + "\"" +
                ", salesOrgChangeable=\""           + salesOrgChangeable            + "\"" +
                ", salesOfficeChangeable=\""        + salesOfficeChangeable         + "\"" +
                ", reqDeliveryDateChangeable=\""    + reqDeliveryDateChangeable     + "\"" +
                ", contractStartDateChangeable=\""  + contractStartDateChangeable   + "\"" +
                ", shipCondChangeable=\""           + shipCondChangeable            + "\"" +
                ", descriptionChangeable=\""        + descriptionChangeable         + "\"" +
                ", deliveryPriorityChangeable=\""	+ deliveryPriorityChangeable	+ "\"" +
                ", recurringNetValue=\""            + recurringNetValue             + "\"" +
                ", recurringTaxValue=\""            + recurringTaxValue             + "\"" +
                ", recurringGrossValue=\""          + recurringGrossValue           + "\"" +
                ", recurringDuration=\""            + recurringDuration             + "\"" +
                ", recurringTimeUnit=\""            + recurringTimeUnit             + "\"" +
                ", nonRecurringNetValue=\""         + nonRecurringNetValue          + "\"" +
                ", nonRecurringTaxValue=\""         + nonRecurringTaxValue          + "\"" +
                ", nonRecurringGrossValue=\""       + nonRecurringGrossValue        + "\"" +
                ", isDirty=\""                      + isDirty                       + "\"" +
                "]";
    }

    /**
     * Performs a shallow copy of this object that behaves like a deep copy.
     * Because of the fact that
     * all fields of this object consist of immutable objects like
     * <code>String</code> and <code>TechKey</code> or primitive types
     * the shallow copy is identical with a deep copy. For the
     * <code>payment</code> property an explicit copy operation is performed
     * because this object is the only mutable one.
     *
     * @return shallow (deep-like) copy of this object
     */
    public Object clone() {
        try {
            HeaderBase myClone = (HeaderBase) super.clone();
            if(partnerList != null) {
                myClone.partnerList = (PartnerList) partnerList.clone();
            }
            if(predecessorList != null) {
                myClone.predecessorList = (ArrayList) predecessorList.clone();
            }
            if(successorList != null) {
                myClone.successorList = (ArrayList) successorList.clone();
            }
            myClone.isDirty = false;

            return myClone;
        }
        catch(CloneNotSupportedException e) {
            return null;
        }
    }

    /**
     * Sets the key for the document.
     *
     * @param key Key to be set
     */
    public void setTechKey(TechKey techKey){
        super.setTechKey(techKey);
        isDirty = true;
    }

    /**
     * Returns the descripton of the process type.
     * @return String
     */
    public String getProcessTypeDesc() {
        return processTypeDesc;
    }

    /**
     * Sets the descripton of the process type
     * @param processTypeDesc The processTypeDesc to set
     */
    public void setProcessTypeDesc(String processTypeDesc) {
        this.processTypeDesc = processTypeDesc;
    }


    /**
     * Returns the partner key for the given partner function.
     * @param partnerFunction
     * @return TechKey
     */
    public TechKey getPartnerKey(String partnerFunction) {

        if(partnerList != null) {
            PartnerListEntry partner = partnerList.getPartner(partnerFunction);
            if(partner != null) {
                return partner.getPartnerTechKey();
            }
        }

        return null;
    }

    /**
     * Returns the partner id for the given partner function.
     * @param partnerFunction
     * @return String
     */
    public String getPartnerId(String partnerFunction) {

        if(partnerList != null) {
            PartnerListEntry partner = partnerList.getPartner(partnerFunction);
            if(partner != null) {
                return partner.getPartnerId();
            }
        }

        return null;
    }
	/**
	 * Returns the delivery priority key
	 * @return String
	 */
	public String getDeliveryPriority() {
		return deliveryPriority;
	}

	/**
	 * Sets the delivery priority key
	 * @param deliveryPriority
	 */
	public void setDeliveryPriority(String deliveryPriority) {
		this.deliveryPriority = deliveryPriority;
	}

	/**
	 * Checks whether or not the delivery priority is changeable.
	 *
	 * @return <code>true</code> indicates that the information is changeable,
	 *         <code>false</code> indicates that the information cannot be changed.
	 */
	public boolean isDeliveryPriorityChangeable() {
		return deliveryPriorityChangeable;
	}

	/**
	 * Sets the flag, whether or not the delivery priority is changeable.
	 * @param deliveryPriorityChangeable
	 */
	public void setDeliveryPriorityChangeable(boolean deliveryPriorityChangeable) {
		this.deliveryPriorityChangeable = deliveryPriorityChangeable;
	}

	/**
	 * Returns the type of external reference object (e.g. vehicle identification number)
	 * @return
	 */
	public String getExtRefObjectType() {
		return extRefObjectType;
	}

	/**
	 * Sets the type of external reference object (e.g. vehicle identification number)
	 * @param extRefObjectType the type of the external reference object
	 */
	public void setExtRefObjectType(String extRefObjectType) {
		this.extRefObjectType = extRefObjectType;
	}
	
	/**
	 * Sets the type description of external reference object (e.g. vehicle identification number)
	 * @param extRefObjectTypeDesc the type description of the external reference object
	 */
	public void setExtRefObjectTypeDesc(String extRefObjectTypeDesc) {
		this.extRefObjectTypeDesc = extRefObjectTypeDesc;
	}
	
	/**
	 * Returns the type description of external reference object (e.g. vehicle identification number)
	 * @return extRefObjectTypeDesc the type description of the external reference object
	 */
	public String getExtRefObjectTypeDesc() {
		return this.extRefObjectTypeDesc;
	}			
	/**
	 * Sets the changeable flags for assigned external reference objects
	 *
	 * @param assignedExtRefObjectsChangeable <code>true</code> indicates that the
	 *        list of assigned external reference objects is changeable
	 */
	public void setAssignedExtRefObjectsChangeable(boolean assignedExtRefObjectsChangeable) {
	
		this.assignedExtRefObjectsChangeable     = assignedExtRefObjectsChangeable;
	}


	/**
	 * Checks whether or not the assigned that the
	 * list of assigned external reference objects is changeable.
	 *
	 * @return <code>true</code> indicates that the external reference object list is changeable,
	 *         <code>false</code> indicates that the list cannot be changed.
	 */
	public boolean isAssignedExtRefObjectsChangeable() {
		return assignedExtRefObjectsChangeable;
	}


	/**
	 * Returns the list of external reference objects assigned to the header.
	 *
	 * @return ExtRefObjectList the list of external reference objects assigned to the header
	 */
	public ExtRefObjectList getAssignedExtRefObjects() {
		return assignedExtRefObjects;
	}

	/**
	 * Returns the list of external reference objects assigned to the header.
	 *
	 * @return ExtRefObjectListData the list of external reference objects assigned to the header
	 */
	public ExtRefObjectListData getAssignedExtRefObjectsData() {
		return (ExtRefObjectListData)assignedExtRefObjects;
	}
	/**
	 * Sets the list of external reference objects assigned to the header.
	 *
	 * @return ExtRefObjectList the list of external reference objects assigned to the header
	 */
	public void setAssignedExtRefObjects(ExtRefObjectList extRefObjectList) {
		assignedExtRefObjects = extRefObjectList;
	}
	
	/**
	 * Sets the list of external reference objects assigned to the header.
	 *
	 * @return ExtRefObjectListData the list of external reference objects assigned to the header
	 */
	public void setAssignedExtRefObjectsData(ExtRefObjectListData extRefObjectListData) {
		assignedExtRefObjects = (ExtRefObjectList)extRefObjectListData;
	}	
	
	/**
	 * Sets the changeable flags for assigned external reference numbers
	 *
	 * @param assignedExternalReferencesChangeable <code>true</code> indicates that the
	 *        list of assigned external reference numbers is changeable
	 */
	public void setAssignedExternalReferencesChangeable(boolean assignedExternalReferencesChangeable) {
	
		this.assignedExternalReferencesChangeable     = assignedExternalReferencesChangeable;
	}


	/**
	 * Checks whether or not the assigned that the
	 * list of assigned external reference numbers is changeable.
	 *
	 * @return <code>true</code> indicates that the external reference number list is changeable,
	 *         <code>false</code> indicates that the list cannot be changed.
	 */
	public boolean isAssignedExternalReferencesChangeable() {
		return assignedExternalReferencesChangeable;
	}


	/**
	 * Returns the list of external reference numbers assigned to the header.
	 *
	 * @return ExternalReferenceList the list of external reference numbers assigned to the header
	 */
	public ExternalReferenceList getAssignedExternalReferences() {
		return assignedExternalReferences;
	}

	/**
	 * Returns the list of external reference numbers assigned to the header.
	 *
	 * @return ExternalReferenceListData the list of external reference numbers assigned to the header
	 */
	public ExternalReferenceListData getAssignedExternalReferencesData() {
		return (ExternalReferenceListData)assignedExternalReferences;
	}
	/**
	 * Sets the list of external reference numbers assigned to the header.
	 *
	 * @param ExternalReferencesList the list of external reference numbers assigned to the header
	 */
	public void setAssignedExternalReferences(ExternalReferenceList externalReferences) {
		assignedExternalReferences = externalReferences;
	}
	
	/**
	 * Sets the list of external reference numbers assigned to the header.
	 *
	 * @param ExternalReferenceListData the list of external reference numbers assigned to the header
	 */
	public void setAssignedExternalReferencesData(ExternalReferenceListData externalReferences) {
		assignedExternalReferences = (ExternalReferenceList)externalReferences;
	}		
	/**
	 * Returns the usage of the process type
	 * @return
	 */
	public String getProcessTypeUsage() {
		return processTypeUsage;
	}

	/**
	 * Sets the usage of the process type
	 * @param string
	 */
	public void setProcessTypeUsage(String string) {
		processTypeUsage = string;
	}
	
	/**
	 * Set the attachment list to the header
	 * @param attachmentList, list of attachments
	 */
	public void setAttachmentList(ArrayList attachmentList){
		this.attachmentList = attachmentList;
	}
	
	/**
	 * Return the attachment list of this header
	 * @return Attachment list of the header
	 */
	public ArrayList getAttachmentList(){
		return this.attachmentList;
	}

    /**
     * Returns the non recurring Gross value
     * @return the non recurring Gross value
     */
    public String getNonRecurringGrossValue() {
        return nonRecurringGrossValue;
    }

    /**
     * Returns the non recurring Net value
     * @return the non recurring Net value
     */
    public String getNonRecurringNetValue() {
        return nonRecurringNetValue;
    }

    /**
     * Returns the non recurring Tax value
     * @return the non recurring Tax value
     */
    public String getNonRecurringTaxValue() {
        return nonRecurringTaxValue;
    }

    /**
     * Returns the recurring Duration
     * @return the recurring Duration
     */
    public String getRecurringDuration() {
        return recurringDuration;
    }
    
    /**
     * Returns the recurring Net value
     * @return the recurring Net value
     */
    public String getRecurringNetValue() {
        return recurringNetValue;
    }

    /**
     * Returns the recurring Gross value
     * @return the recurring Gross value
     */
    public String getRecurringGrossValue() {
        return recurringGrossValue;
    }

    /**
     * Returns the  recurring Tax value
     * @return the recurring Tax value
     */
    public String getRecurringTaxValue() {
        return recurringTaxValue;
    }

    /**
     * Returns the recurring Time unit 
     * @return the recurring Time unit 
     */
    public String getRecurringTimeUnit() {
        return recurringTimeUnit;
    }
    
    /**
     * Returns the recurring Time unit abbreviation 
     * @return the recurring Time unit abbreviation
     */
    public String getRecurringTimeUnitAbr() {
        return recurringTimeUnitAbr;
    }
    
    /**
     * Returns the plural of the recurring Time unit 
     * @return the plural of the recurring Time unit
     */
    public String getRecurringTimeUnitPlural() {
        return recurringTimeUnitPlural;
    }

    /**
     * Sets the non recurring Gross value
     * @param nonRecurringGrossValue the non recurring Gross value
     */
    public void setNonRecurringGrossValue(String nonRecurringGrossValue) {
        this.nonRecurringGrossValue = nonRecurringGrossValue;
    }

    /**
     * Sets the non recurring Net value
     * @param nonRecurringNetValue the non recurring Net value
     */
    public void setNonRecurringNetValue(String nonRecurringNetValue) {
        this.nonRecurringNetValue = nonRecurringNetValue;
    }

    /**
     * Sets the non recurring Tax value
     * @param nonRecurringTaxValue the non recurring Tax value
     */
    public void setNonRecurringTaxValue(String nonRecurringTaxValue) {
        this.nonRecurringTaxValue = nonRecurringTaxValue;
    }

    /**
     * Sets the recurring Duration
     * @param recurringDuration the recurring Duration
     */
    public void setRecurringDuration(String recurringDuration) {
        this.recurringDuration = recurringDuration;
    }
    
    /**
     * Sets the recurring Net value
     * @param recurringNetValue the recurring Net value
     */
    public void setRecurringNetValue(String recurringNetValue) {
        this.recurringNetValue = recurringNetValue;
    }

    /**
     * Sets the recurring Gross value
     * @param recurringGrossValue the recurring Gross value
     */
    public void setRecurringGrossValue(String recurringGrossValue) {
        this.recurringGrossValue = recurringGrossValue;
    }

    /**
     * Sets the recurring Tax value
     * @param recurringTaxValue the recurring Tax value
     */
    public void setRecurringTaxValue(String recurringTaxValue) {
        this.recurringTaxValue = recurringTaxValue;
    }

    /**
     * Sets the recurring Time unit
     * @param recurringTimeUnit the recurring Time unit
     */
    public void setRecurringTimeUnit(String recurringTimeUnit) {
        this.recurringTimeUnit = recurringTimeUnit;
    }
    
    /**
     * Sets the recurring Time unit abbreviation 
     * @param the recurring Time unit abbreviation
     */
    public void setRecurringTimeUnitAbr(String recurringTimeUnitAbr) {
        this.recurringTimeUnitAbr = recurringTimeUnitAbr;
    }
    
    /**
     * Sets the plural of the recurring Time unit 
     * @param the plural of the recurring Time unit
     */
    public void setRecurringTimeUnitPlural(String recurringTimeUnitPlural) {
        this.recurringTimeUnitPlural = recurringTimeUnitPlural;
    }

}
