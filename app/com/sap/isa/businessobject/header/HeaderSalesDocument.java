/*****************************************************************************
    Class:        HeaderSalesDocument
    Copyright (c) 2001, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      11.4.2001
    Version:      1.0

    $Revision: #12 $
    $Date: 2003/01/02 $
*****************************************************************************/

package com.sap.isa.businessobject.header;


import com.sap.ecommerce.boi.loyalty.LoyaltyMembershipConfiguration;
import com.sap.isa.backend.boi.isacore.SalesDocumentConfiguration;
import com.sap.isa.backend.boi.isacore.ShipToData;
import com.sap.isa.backend.boi.isacore.order.CampaignListData;
import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.businessobject.ShipTo;
import com.sap.isa.businessobject.order.CampaignList;
import com.sap.isa.core.TechKey;
import com.sap.isa.payment.backend.boi.PaymentBaseData;
import com.sap.isa.payment.backend.boi.PaymentBaseTypeData;
import com.sap.isa.payment.businessobject.PaymentBase;
import com.sap.isa.payment.businessobject.PaymentBaseType;

/**
 * Common Header Information for all objects of the bo layer that are considered
 * to be sales documents.
 *
 * @author SAP AG
 * @version 1.0
 */
public class HeaderSalesDocument extends HeaderBase
        implements HeaderData {

    private LoyaltyMembershipConfiguration memberShipId;
	private String totalRedemptionValue;
	private String loyaltyType = LOYALTY_TYPE_ANY;
	private SalesDocumentConfiguration shop;
    private ShipTo shipTo;
	private boolean shipToChangeable = true;
    private boolean quotationExtended;
    private String ipcConnectionKey = "";
    private String ipcHost;
    private String ipcPort;
    private String ipcSession;
    private String ipcClient;
    private TechKey ipcDocumentId;
    private PaymentBase payment;
    private String statusDelivery;

	private String paymentTerms;
	private boolean paymentTermsChangeable = true;
	private String paymentTermsDesc;
	private String incoTerms1;
	private boolean incoTerms1Changeable = true;
	private String incoTerms1Desc;
	private String incoTerms2;
	private boolean incoTerms2Changeable = true;
	
	private TechKey scDocumentGuid;
    
    //Service Recall information
    protected String recallId;
    protected String recallDesc;
	
	protected boolean assignedCampaignsChangeable;
	/**
	 * this list holds all campaigns the have been assigend to the item
	 */
	protected CampaignList assignedCampaigns = new CampaignList();
    
    protected boolean hasATPSubstOccured;
    
    //Cancel date 
	protected String  latestDlvDate;
	protected boolean latestDlvDateChangeable;
	
	//auction related
	/**
	 * Total pricing
	 */
	private String totalPriceType = null;
	/**
	 * Shipping price type
	 */
	private String shippingPriceType = null;	
	/**
	 * Check if the given headerBase object is an instance of HeaderSalesDocument. 
	 * 
	 * @param headerBase item as HeaderBase object
	 * @return HeaderSalesDoc casted item object as HeaderSalesDocument
	 */
	static public HeaderSalesDocument getHeaderSalesDocument(HeaderBase headerBase) {
    	
		HeaderSalesDocument header = null;
		if (headerBase != null) {
			if (headerBase instanceof HeaderSalesDocument) {
				header = (HeaderSalesDocument) headerBase;
			} 
			else {
				log.debug("not allowed instance for HeaderSalesDocument object: " + 
						headerBase.getClass().getName());        	
			}        	
		}
        
		return header;      
	}
    

    /**
     * Drops the state of the object. All reference fields, except partnerList, are
     * set to null, all primitive types are set to the default values they would have
     * after the creation of a new instance. Use this method to reset the state to the
     * state a newly created object would have. The advantage is, that the
     * overhead caused by the normal object creation is omitted.
     */
    public void clear() {
    	super.clear();
        shop                        = null;
        statusDelivery              = null;
        shipTo                      = null;
        ipcConnectionKey           = "";
        ipcHost                     = null;
        ipcPort                     = null;
        ipcSession                  = null;
        ipcClient                   = null;
        ipcDocumentId               = null;
        payment                     = null;
        quotationExtended           = false;
		assignedCampaigns.clear();
		paymentTerms				= null;
		paymentTermsDesc			= null;
		incoTerms1					= null;
		incoTerms1Desc				= null;
		incoTerms2					= null;
		recallId                    = null;
		recallDesc                  = null;
		deliveryPriority			= null;
    }

    /**
     * Get the host the IPC runs on.
     * @deprecated
     *
     * @return hostname of the host the IPC runs on
     */
    public String getIpcHost() {
        return ipcHost;
    }

    /**
     * Sets the host the IPC runs on.
     * @deprecated
     *
     * @param ipcHost the hostname of the ipc host
     */
    public void setIpcHost(String ipcHost) {
        this.ipcHost = ipcHost;
    }

    /**
     * Get an identifier for the current IPC session.
     * @deprecated
     *
     * @return identifier for the current IPC session
     */
    public String getIpcSession() {
        return ipcSession;
    }

    /**
     * Sets the identifier for the current IPC session.
     * @deprecated
     *
     * @param ipcSession the identifier for the current IPC session to be set
     */
    public void setIpcSession(String ipcSession) {
        this.ipcSession = ipcSession;
    }

    /**
     * Get the port number of the IPC server.
     * @deprecated
     *
     * @return the port number of the IPC server
     */
     public String getIpcPort() {
        return ipcPort;
    }

    /**
     * Set the port number of the ipc server.
     * @deprecated
     *
     * @param ipcPort the port number of the IPC server to be set
     */
    public void setIpcPort(String ipcPort) {
        this.ipcPort = ipcPort;
    }

    /**
     * Get the IPC client.
     * @deprecated
     *
     * @return the IPC client
     */
     public String getIpcClient() {
        return ipcClient;
    }

    /**
     * Set the IPC client.
     * @deprecated
     *
     * @param ipcClient the IPC client to be set
     */
    public void setIpcClient(String ipcClient) {
        this.ipcClient = ipcClient;
    }

    /**
     * Get the IPC document id.
     *
     * @return the IPC document id
     */
    public TechKey getIpcDocumentId() {
        return ipcDocumentId;
    }

    /**
     * Set the IPC document id.
     *
     * @param ipcDocumentId the IPC document id to be set
     */
    public void setIpcDocumentId(TechKey ipcDocumentId) {
        this.ipcDocumentId = ipcDocumentId;
    }

	public String getIpcConnectionKey() {
		return ipcConnectionKey;
	}

	/**
	 * Set the IPC document id.
	 *
	 * @param ipcDocumentId the IPC document id to be set
	 */
	public void setIpcConnectionKey(String ipcConnectionKey) {
		this.ipcConnectionKey = ipcConnectionKey;
	}
    /**
     * Get the shop the document belongs to.
     *
     * @return the shop
     */
    public SalesDocumentConfiguration getShop(){
        return shop;
    }

    /**
     * Set the shop the document belongs to.
     *
     * @param shop the shop to be set
     */
    public void setShop(SalesDocumentConfiguration shop){
        this.shop = shop;
    }

    /**
     * Set the status to whatever passed through
     * @param status
     * @deprecated use method #setStatus instead
     */
    public void setStatusOther(String status) {
        this.status = status;
    }

    /**
     * Set OVERALL status to partly delivered
     */
    public void setStatusPartlyDelivered() {
        status = DOCUMENT_COMPLETION_STATUS_PARTLY_DELIVERED;
    }

    /**
     * Determines whether or not, the document's status is DELIVERED.
     *
     * @return <code>true</code> if the object is in status DELIVERED, otherwise
     *         <code>false</code>.
     */
    public boolean isStatusPartlyDelivered() {
        return (status == DOCUMENT_COMPLETION_STATUS_PARTLY_DELIVERED);
    }

    /**
     * Set OVERALL status to Ready to Pickup
     */
    public void setStatusReadyToPickup() {
                    status = DOCUMENT_COMPLETION_STATUS_READY_TO_PICKUP;
    }


	/**
	 * Determines whether or not, the document's status is Ready to Pickup.
	 *
	 * @return <code>true</code> if the object is in status Ready to Pickup, otherwise
	 *         <code>false</code>.
	 */
	public boolean isStatusReadyToPickup() {
		return (status == DOCUMENT_COMPLETION_STATUS_READY_TO_PICKUP);	
	}


    /**
     * set OVERALL status to inquiry sent
     */
    public void setStatusInquirySent() {
        status = DOCUMENT_COMPLETION_STATUS_INQUIRYSENT;
    }

    /**
     * Determines whether or not, the document's status is INQUIRYSENT.
     *
     * @return <code>true</code> if the object is in status INQUIRYSENT, otherwise
     *         <code>false</code>.
     */
    public boolean isStatusInquirySent() {
        return (status == DOCUMENT_COMPLETION_STATUS_INQUIRYSENT);
    }

    /**
     * set OVERALL status to quotation
     */
    public void setStatusQuotation() {
        status = DOCUMENT_COMPLETION_STATUS_QUOTATION;
    }

    /**
     * Determines whether or not, the document's status is QUOTATION.
     *
     * @return <code>true</code> if the object is in status QUOTATION, otherwise
     *         <code>false</code>.
     */
    public boolean isStatusQuotation() {
        return (status == DOCUMENT_COMPLETION_STATUS_QUOTATION);
    }
    /**
     * Get DELIVERY status. Might be different from OVERALL status.
     *
     * @return one of the possible status values represented by the constants
     *         defined in this class with the names <code>DOCUMENT_COMPLETION_*</code>.
     */
    public String getDeliveryStatus(){
        return statusDelivery;
    }

    /**
     * Set the DELIVERY status to open, represented by the constant .
     */
    public void setDeliveryStatusOpen() {
        statusDelivery = DOCUMENT_COMPLETION_STATUS_OPEN;
    }

    /**
     * Determines whether or not, the document's delivery status is OPEN.
     *
     * @return <code>true</code> if the object is in status OPEN, otherwise
     *         <code>false</code>.
     */
    public boolean isDeliveryStatusOpen() {
        return (statusDelivery == DOCUMENT_COMPLETION_STATUS_OPEN);
    }

    /**
     * Set DELIVERY status to completed.
     */
    public void setDeliveryStatusCompleted() {
        statusDelivery = DOCUMENT_COMPLETION_STATUS_COMPLETED;
    }

    /**
     * Determines whether or not, the document's delivery status is COMPLETED.
     *
     * @return <code>true</code> if the object is in status COMPLETED, otherwise
     *         <code>false</code>.
     */
    public boolean isDeliveryStatusCompleted() {
        return (statusDelivery == DOCUMENT_COMPLETION_STATUS_COMPLETED);
    }

    /**
     * Set DELIVERY status to in process
     */
    public void setDeliveryStatusInProcess() {
        statusDelivery = DOCUMENT_COMPLETION_STATUS_INPROCESS;
    }

   /**
     * Determines whether or not, the document's delivery status is INPROCESS.
     *
     * @return <code>true</code> if the object is in status INPROCESS, otherwise
     *         <code>false</code>.
     */
    public boolean isDeliveryStatusInProcess() {
        return (statusDelivery == DOCUMENT_COMPLETION_STATUS_COMPLETED);
    }

    /**
     * Sets the document type to ORDER.
     */
    public void setDocumentTypeOrder(){
        documentType = DOCUMENT_TYPE_ORDER;
    }

    /**
     * Sets the document type to whatever passed through.
     * @param documentType
     * @deprecated use method #setDocumentType instead
    */
    public void setDocumentTypeOther(String documentType){
        this.documentType = documentType;
    }

        /**
     * Determines whether or not, the document's type is ORDER.
     *
     * @return <code>true</code> if the type is ORDER, otherwise
     *         <code>false</code>.
     */
    public boolean isDocumentTypeOrder() {
        return (documentType == DOCUMENT_TYPE_ORDER);
    }


    /**
     * Sets the document type to COLLECTIVE ORDER.
     */
    public void setDocumentTypeCollectiveOrder(){
        documentType = DOCUMENT_TYPE_COLLECTIVE_ORDER;
    }

    /**
     * Determines whether or not, the document's type is COLLECTIVE ORDER.
     *
     * @return <code>true</code> if the type is COLLECTIVE ORDER, otherwise
     *         <code>false</code>.
     */
    public boolean isDocumentTypeCollectiveOrder() {
        return (documentType == DOCUMENT_TYPE_COLLECTIVE_ORDER);
    }


    /**
     * Sets the document type to ORDERTEMPLATE.
     */
    public void setDocumentTypeOrderTemplate(){
        documentType = DOCUMENT_TYPE_ORDERTEMPLATE;
    }

    /**
     * Determines whether or not, the document's type is ORDERTEMPLATE.
     *
     * @return <code>true</code> if the type is ORDERTEMPLATE, otherwise
     *         <code>false</code>.
     */
    public boolean isDocumentTypeOrderTemplate() {
        return (documentType == DOCUMENT_TYPE_ORDERTEMPLATE);
    }

    /**
     * Sets the document type to QUOTATION.
     */
    public void setDocumentTypeQuotation(){
        documentType = DOCUMENT_TYPE_QUOTATION;
    }

	/**
	 * Determines whether or not, the document's type is QUOTATION.
	 *
	 * @return <code>true</code> if the type is QUOTATION, otherwise
	 *         <code>false</code>.
	 */
	public boolean isDocumentTypeQuotation() {
		return (documentType == DOCUMENT_TYPE_QUOTATION);
	}
	
	/**
	  * Sets the document type to NegotiatedContract.
	  */
	 public void setDocumentTypeNegotiatedContract(){
		 documentType = DOCUMENT_TYPE_NEGOTIATED_CONTRACT;
	 }

	 /**
	  * Determines whether or not, the document's type is NEGOTIATED CONTRACT.
	  *
	  * @return <code>true</code> if the type is NEGOTIATED CONTRACT, otherwise
	  *         <code>false</code>.
	  */
	 public boolean isDocumentTypeNegotiatedContract() {
		 return (documentType == DOCUMENT_TYPE_NEGOTIATED_CONTRACT);
	 }
    /**
     * Sets the default ship to for the header. This ship to is used, if
     * no special information is set at the item level.
     *
     * @param shipTo The ship to, to be set
     */
    public void setShipTo(ShipTo shipTo) {
        this.shipTo = shipTo;
    }

    /**
     * Sets the default ship to for the header. This ship to is used, if
     * no special information is set at the item level.
     *
     * @param shipTo The ship to, to be set
     */
    public void setShipTo(ShipToData shipTo) {
        this.shipTo = (ShipTo) shipTo;
    }

    /**
     * Returns the ship to for the header. This ship to is used, if
     * no special information is set at the item level.
     *
     * @return The ship to for the header
     */
    public ShipTo getShipTo() {
        return shipTo;
    }

    /**
     * Get the ship to information as an backend layer interface.
     *
     * @return the ship to information
     */
    public ShipToData getShipToData() {
        return shipTo;
    }

    /**
     * Set the ship to information using the backend layer interface.
     *
     * @param shipToLine the ship to to be set
     */
    public void setShipToData(ShipToData shipToLine) {
        this.shipTo = (ShipTo) shipToLine;
    }

    /**
     * Get the payment information using the backend interface.
     *
     * @return payment information
     */
    public PaymentBaseData getPaymentData() {
        return payment;
    }

    /**
     * Returns the payment information using the backend interface.
     * If no payment object exists, a new one is created.
     *
     * @return payment information
     */
    public PaymentBaseData createPaymentData() {
        if (payment == null) {
            payment = new PaymentBase();
        }
        return payment;
    }
    
    
    /**
     * Creates a new PaymentTypeData object
     *
     * @return new PaymentTypeData object
     */
    public PaymentBaseTypeData createPaymentTypeData(){
        return new PaymentBaseType();
    }

    /**
     * Set the payment information using the backend interface
     *
     * @param payment the payment information to be set
     */
    public void setPaymentData(PaymentBaseData payment) {
        this.payment = (PaymentBase) payment;
    }
    
	/**
	 * Creates a new CampaignList.
	 *
	 * @return CampaignListData
	 */
	public CampaignListData createCampaignList() {
		return new CampaignList();
	}

    /**
     * Characterizes an extended QUOTATION.
     */
    public void setQuotationExtended() {
            quotationExtended = true;
    }

    /**
     * Determines whether or not, a QUOTATION is extended.
     *
     * @return <code>true</code> if the QUOTATION is extended, otherwise
     *         <code>false</code>.
     */
    public boolean isQuotationExtended() {
            return quotationExtended;
    }


    /**
     * Returns a string representation of the object.
     *
     * @return object as string
     */
    public String toString() {
    	String ret = super.toString() +
		        "HeaderSalesDocument [" +
				", payment=\""                      + payment                       + "\"" +
				", shop=\""                         + shop                          + "\"" +
				", shipTo=\""                       + shipTo                        + "\"" +
		        ", assignedCampaigns=\""            + assignedCampaigns.toString()  + "\"" +
		        ", assignedCampaignsChangeable=\""  + assignedCampaignsChangeable   + "\"" +
		        ", recallId=\""                     + recallId                      + "\"" +
		        ", recallDesc=\""                   + recallDesc                    + "\"" +
                "]";
  		return ret;  
    }

    /**
     * Performs a shallow copy of this object that behaves like a deep copy.
     * Because of the fact that
     * all fields of this object consist of immutable objects like
     * <code>String</code> and <code>TechKey</code> or primitive types
     * the shallow copy is identical with a deep copy. For the
     * <code>payment</code> property an explicit copy operation is performed
     * because this object is the only mutable one.
     *
     * @return shallow (deep-like) copy of this object
     */
    public Object clone() {
        HeaderSalesDocument myClone = (HeaderSalesDocument) super.clone();
        if (myClone != null) {          
            // explicitly clone the payment object
            if (payment != null) {
            	myClone.setPaymentData(this.payment);                
            }
			if (assignedCampaigns != null) {
				myClone.assignedCampaigns = (CampaignList) assignedCampaigns.clone();
			}
			if (this.totalPriceType != null) {
				myClone.setTotalManualPriceCondition(this.totalPriceType);
			}
			if (this.shippingPriceType != null){
				myClone.setShippingManualPriceCondition(this.shippingPriceType);
			}
			if (this.processType != null){
				myClone.setProcessType(this.processType);
			}
			if (this.shipTo != null) {
				myClone.shipTo = this.shipTo;
			}
		}

        return myClone;
    }
    

	/**
	 * Sets all changeable flags for the different fields of the header in
	 * on step.
	 *
	 * @param purchaseOrderChangeable <code>true</code> indicates that the
	 *        purchase order information is changeable
	 * @param purchaseOrderExtChangeable <code>true</code> indicates that the
	 *        external purchase order information is changeable
	 * @param salesOrgChangeable <code>true</code> indicates that the
	 *        sales organization information is changeable
	 * @param salesOfficeChangeable <code>true</code> indicates that the
	 *        sales office information is changeable
	 * @param reqDeliveryDateChangeable <code>true</code> indicates that the
	 *        requested delivery date is changeable
	 * @param shipCondChangeable <code>true</code> indicates that the
	 *        shipping conditions are changeable
	 * @param descriptionChangeable <code>true</code> indicates that the
	 *        description is changeable
	 * @param shipToChangeable <code>true</code> indicates that the
	 *        shipTo is changeable
	 * 
	 * @deprecated
	 */
	public void setAllValuesChangeable(
									  boolean purchaseOrderChangeable,
									  boolean purchaseOrderExtChangeable,
									  boolean salesOrgChangeable,
									  boolean salesOfficeChangeable,
									  boolean reqDeliveryDateChangeable,
									  boolean shipCondChangeable,
									  boolean descriptionChangeable,
			                          boolean shipToChangeable) {
			                          	
	    setAllValuesChangeable(purchaseOrderChangeable, 
	                           purchaseOrderExtChangeable,
							   salesOrgChangeable,
							   salesOfficeChangeable,
							   reqDeliveryDateChangeable,
						       shipCondChangeable,
							   descriptionChangeable);
							   
		this.shipToChangeable = shipToChangeable;		   
	}
	


	/**
	 * Sets all changeable flags for the different fields of the header in
	 * on step.
	 *
	 * @param purchaseOrderChangeable <code>true</code> indicates that the
	 *        purchase order information is changeable
	 * @param purchaseOrderExtChangeable <code>true</code> indicates that the
	 *        external purchase order information is changeable
	 * @param salesOrgChangeable <code>true</code> indicates that the
	 *        sales organization information is changeable
	 * @param salesOfficeChangeable <code>true</code> indicates that the
	 *        sales office information is changeable
	 * @param reqDeliveryDateChangeable <code>true</code> indicates that the
	 *        requested delivery date is changeable
	 * @param shipCondChangeable <code>true</code> indicates that the
	 *        shipping conditions are changeable
	 * @param descriptionChangeable <code>true</code> indicates that the
	 *        description is changeable
	 * @param textChangeable <code>true</code> indicates that the
	 *        text is changeable
	 * @param shipToChangeable <code>true</code> indicates that the
	 *        shipTo is changeable
     * 
     * @deprecated use setAllValuesChangeable(
     *                                boolean purchaseOrderChangeable,
     *                                boolean purchaseOrderExtChangeable,
     *                                boolean salesOrgChangeable,
     *                                boolean salesOfficeChangeable,
     *                                boolean reqDeliveryDateChangeable,
     *                                boolean latestDlvDateChangeable,
     *                                boolean shipCondChangeable,
     *                                boolean descriptionChangeable,
     *                                boolean shipToChangeable,
     *                                boolean incoTerms1Changeable,
     *                                boolean incoTerms2Changeable,
     *                                boolean paymentTermsChangeable,
     *                                boolean assignedCampaignsChangeable,
     *                                boolean deliveryPriorityChangeable) 
     *           instead
     */
	public void setAllValuesChangeable(
									  boolean purchaseOrderChangeable,
									  boolean purchaseOrderExtChangeable,
									  boolean salesOrgChangeable,
									  boolean salesOfficeChangeable,
									  boolean reqDeliveryDateChangeable,
									  boolean shipCondChangeable,
									  boolean descriptionChangeable,
									  boolean textChangeable,
									  boolean shipToChangeable) {
			                          	
		setAllValuesChangeable(purchaseOrderChangeable, 
							   purchaseOrderExtChangeable,
							   salesOrgChangeable,
							   salesOfficeChangeable,
							   reqDeliveryDateChangeable,
							   shipCondChangeable,
							   descriptionChangeable,
                               shipToChangeable);

		this.textChangeable = textChangeable;		   
	}
    
    /**
     * Sets all changeable flags for the different fields of the header in
     * on step.
     *
     * @param purchaseOrderChangeable <code>true</code> indicates that the
     *        purchase order information is changeable
     * @param purchaseOrderExtChangeable <code>true</code> indicates that the
     *        external purchase order information is changeable
     * @param salesOrgChangeable <code>true</code> indicates that the
     *        sales organization information is changeable
     * @param salesOfficeChangeable <code>true</code> indicates that the
     *        sales office information is changeable
     * @param reqDeliveryDateChangeable <code>true</code> indicates that the
     *        requested delivery date is changeable
     * @param shipCondChangeable <code>true</code> indicates that the
     *        shipping conditions are changeable
     * @param descriptionChangeable <code>true</code> indicates that the
     *        description is changeable
     * @param shipToChangeable <code>true</code> indicates that the
     *        shipTo is changeable
     * @param incoTerms1Changeable <code>true</code> indicates that the
     *        incoterms1 field is changeable
     * @param incoTerms2Changeable <code>true</code> indicates that the
     *        incoterms2 field is changeable
     * @param paymentTermsChangeable <code>true</code> indicates that the
     *        payment terms are changeable
     * @param assignedCampaignsChangeable <code>true</code> indicates that the
     *        assigned campaigns are changeable
     * 
     * @deprecated
     *   */
    public void setAllValuesChangeable(
                                      boolean purchaseOrderChangeable,
                                      boolean purchaseOrderExtChangeable,
                                      boolean salesOrgChangeable,
                                      boolean salesOfficeChangeable,
                                      boolean reqDeliveryDateChangeable,
                                      boolean shipCondChangeable,
                                      boolean descriptionChangeable,
                                      boolean shipToChangeable,
                                      boolean incoTerms1Changeable,
                                      boolean incoTerms2Changeable,
                                      boolean paymentTermsChangeable,
                                      boolean assignedCampaignsChangeable) {
                                        
        setAllValuesChangeable(purchaseOrderChangeable, 
                               purchaseOrderExtChangeable,
                               salesOrgChangeable,
                               salesOfficeChangeable,
                               reqDeliveryDateChangeable,
                               shipCondChangeable,
                               descriptionChangeable);
                               
        this.shipToChangeable = shipToChangeable;
        this.incoTerms1Changeable = incoTerms1Changeable;
        this.incoTerms2Changeable = incoTerms2Changeable;
        this.paymentTermsChangeable = paymentTermsChangeable;
        this.assignedCampaignsChangeable = assignedCampaignsChangeable;        
    }
    
    /**
     * Sets all changeable flags for the different fields of the header in
     * on step.
     *
     * @param purchaseOrderChangeable <code>true</code> indicates that the
     *        purchase order information is changeable
     * @param purchaseOrderExtChangeable <code>true</code> indicates that the
     *        external purchase order information is changeable
     * @param salesOrgChangeable <code>true</code> indicates that the
     *        sales organization information is changeable
     * @param salesOfficeChangeable <code>true</code> indicates that the
     *        sales office information is changeable
     * @param reqDeliveryDateChangeable <code>true</code> indicates that the
     *        requested delivery date is changeable
     * @param shipCondChangeable <code>true</code> indicates that the
     *        shipping conditions are changeable
     * @param descriptionChangeable <code>true</code> indicates that the
     *        description is changeable
     * @param shipToChangeable <code>true</code> indicates that the
     *        shipTo is changeable
     * @param incoTerms1Changeable <code>true</code> indicates that the
     *        incoterms1 field is changeable
     * @param incoTerms2Changeable <code>true</code> indicates that the
     *        incoterms2 field is changeable
     * @param paymentTermsChangeable <code>true</code> indicates that the
     *        payment terms are changeable
     * @param assignedCampaignsChangeable <code>true</code> indicates that the
     *        assigned campaigns are changeable
     * 
     * @deprecated
     *   
     */
    public void setAllValuesChangeable(
                                      boolean purchaseOrderChangeable,
                                      boolean purchaseOrderExtChangeable,
                                      boolean salesOrgChangeable,
                                      boolean salesOfficeChangeable,
                                      boolean reqDeliveryDateChangeable,
                                      boolean shipCondChangeable,
                                      boolean descriptionChangeable,
                                      boolean shipToChangeable,
                                      boolean incoTerms1Changeable,
                                      boolean incoTerms2Changeable,
                                      boolean paymentTermsChangeable,
                                      boolean assignedCampaignsChangeable,
                                      boolean deliveryPriorityChangeable) {
                                        
        setAllValuesChangeable(purchaseOrderChangeable, 
                               purchaseOrderExtChangeable,
                               salesOrgChangeable,
                               salesOfficeChangeable,
                               reqDeliveryDateChangeable,
                               shipCondChangeable,
                               descriptionChangeable);
                               
        this.shipToChangeable = shipToChangeable;
        this.incoTerms1Changeable = incoTerms1Changeable;
        this.incoTerms2Changeable = incoTerms2Changeable;
        this.paymentTermsChangeable = paymentTermsChangeable;
        this.assignedCampaignsChangeable = assignedCampaignsChangeable;
        this.deliveryPriorityChangeable = deliveryPriorityChangeable;          
    }
  
    /**
     * Sets all changeable flags for the different fields of the header in
     * on step.
     *
     * @param purchaseOrderChangeable <code>true</code> indicates that the
     *        purchase order information is changeable
     * @param purchaseOrderExtChangeable <code>true</code> indicates that the
     *        external purchase order information is changeable
     * @param salesOrgChangeable <code>true</code> indicates that the
     *        sales organization information is changeable
     * @param salesOfficeChangeable <code>true</code> indicates that the
     *        sales office information is changeable
     * @param reqDeliveryDateChangeable <code>true</code> indicates that the
     *        requested delivery date is changeable
     * @param latestDlvDateChangeable <code>true</code> indicates that the
     *        cancel date is changeable
     * @param shipCondChangeable <code>true</code> indicates that the
     *        shipping conditions are changeable
     * @param descriptionChangeable <code>true</code> indicates that the
     *        description is changeable
     * @param shipToChangeable <code>true</code> indicates that the
     *        shipTo is changeable
     * @param incoTerms1Changeable <code>true</code> indicates that the
     *        incoterms1 field is changeable
     * @param incoTerms2Changeable <code>true</code> indicates that the
     *        incoterms2 field is changeable
     * @param paymentTermsChangeable <code>true</code> indicates that the
     *        payment terms are changeable
     * @param assignedCampaignsChangeable <code>true</code> indicates that the
     *        assigned campaigns are changeable
     *   
     */
    public void setAllValuesChangeable(
                                      boolean purchaseOrderChangeable,
                                      boolean purchaseOrderExtChangeable,
                                      boolean salesOrgChangeable,
                                      boolean salesOfficeChangeable,
                                      boolean reqDeliveryDateChangeable,
                                      boolean latestDlvDateChangeable,
                                      boolean shipCondChangeable,
                                      boolean descriptionChangeable,
                                      boolean shipToChangeable,
                                      boolean incoTerms1Changeable,
                                      boolean incoTerms2Changeable,
                                      boolean paymentTermsChangeable,
                                      boolean assignedCampaignsChangeable,
                                      boolean deliveryPriorityChangeable,
                                      boolean assignedExtRefObjectChangeable,
                                      boolean assignedExternalReferencesChangeable) {
                                        
        setAllValuesChangeable(purchaseOrderChangeable, 
                               purchaseOrderExtChangeable,
                               salesOrgChangeable,
                               salesOfficeChangeable,
                               reqDeliveryDateChangeable,
                               shipCondChangeable,
                               descriptionChangeable);
                               
        this.shipToChangeable = shipToChangeable;
        this.incoTerms1Changeable = incoTerms1Changeable;
        this.incoTerms2Changeable = incoTerms2Changeable;
        this.paymentTermsChangeable = paymentTermsChangeable;
        this.assignedCampaignsChangeable = assignedCampaignsChangeable; 
        this.latestDlvDateChangeable = latestDlvDateChangeable; 
        this.deliveryPriorityChangeable = deliveryPriorityChangeable;    
        this.assignedExtRefObjectsChangeable = assignedExtRefObjectChangeable;  
        this.assignedExternalReferencesChangeable = assignedExternalReferencesChangeable;
    }
    
    /**
     * Sets all changeable flags for the different fields of the header in
     * on step.
     *
     * @param purchaseOrderChangeable <code>true</code> indicates that the
     *        purchase order information is changeable
     * @param purchaseOrderExtChangeable <code>true</code> indicates that the
     *        external purchase order information is changeable
     * @param salesOrgChangeable <code>true</code> indicates that the
     *        sales organization information is changeable
     * @param salesOfficeChangeable <code>true</code> indicates that the
     *        sales office information is changeable
     * @param reqDeliveryDateChangeable <code>true</code> indicates that the
     *        requested delivery date is changeable
     * @param latestDlvDateChangeable <code>true</code> indicates that the
     *        cancel date is changeable
     * @param shipCondChangeable <code>true</code> indicates that the
     *        shipping conditions are changeable
     * @param descriptionChangeable <code>true</code> indicates that the
     *        description is changeable
     * @param shipToChangeable <code>true</code> indicates that the
     *        shipTo is changeable
     * @param incoTerms1Changeable <code>true</code> indicates that the
     *        incoterms1 field is changeable
     * @param incoTerms2Changeable <code>true</code> indicates that the
     *        incoterms2 field is changeable
     * @param paymentTermsChangeable <code>true</code> indicates that the
     *        payment terms are changeable
     * @param assignedCampaignsChangeable <code>true</code> indicates that the
     *        assigned campaigns are changeable
     * @param contractStartDateChangeable <code>true</code> indicates that the
     *        contractStartDateChangeable is changeable    
     */
    public void setAllValuesChangeable(
                                      boolean purchaseOrderChangeable,
                                      boolean purchaseOrderExtChangeable,
                                      boolean salesOrgChangeable,
                                      boolean salesOfficeChangeable,
                                      boolean reqDeliveryDateChangeable,
                                      boolean latestDlvDateChangeable,
                                      boolean shipCondChangeable,
                                      boolean descriptionChangeable,
                                      boolean shipToChangeable,
                                      boolean incoTerms1Changeable,
                                      boolean incoTerms2Changeable,
                                      boolean paymentTermsChangeable,
                                      boolean assignedCampaignsChangeable,
                                      boolean deliveryPriorityChangeable,
                                      boolean assignedExtRefObjectChangeable,
                                      boolean assignedExternalReferencesChangeable,
                                      boolean contractStartDateChangeable) {
                                        
        setAllValuesChangeable(purchaseOrderChangeable, 
                               purchaseOrderExtChangeable,
                               salesOrgChangeable,
                               salesOfficeChangeable,
                               reqDeliveryDateChangeable,
                               shipCondChangeable,
                               descriptionChangeable);
                               
        this.shipToChangeable = shipToChangeable;
        this.incoTerms1Changeable = incoTerms1Changeable;
        this.incoTerms2Changeable = incoTerms2Changeable;
        this.paymentTermsChangeable = paymentTermsChangeable;
        this.assignedCampaignsChangeable = assignedCampaignsChangeable; 
        this.latestDlvDateChangeable = latestDlvDateChangeable; 
        this.deliveryPriorityChangeable = deliveryPriorityChangeable;    
        this.assignedExtRefObjectsChangeable = assignedExtRefObjectChangeable;  
        this.assignedExternalReferencesChangeable = assignedExternalReferencesChangeable;
        this.contractStartDateChangeable = contractStartDateChangeable;
    }
    
	/**
     * Checks whether or not the shipto is changeable.
     *
     * @return <code>true</code> indicates that the shipto is changeable,
     *         <code>false</code> indicates that the shipto cannot be changed.
	 */
	public boolean isShipToChangeable() {
		return shipToChangeable;
	}

	/**
	 * Sets the fag, if the shipTo is changeable or not.
	 * 
	 * @param shipToChangeable new value to set
	 */
	public void setShipToChangeable(boolean isShipToChangeable) {
		this.shipToChangeable = isShipToChangeable;
	}


	/**
	 * Sets the changeable flags for assigned campaigns
	 *
	 * @param assignedCampaignsChangeable <code>true</code> indicates that the
	 *        assigned campaign information is changeable
	 */
	public void setAssignedCampaignsChangeable(boolean assignedCampaignsChangeable) {
	
		this.assignedCampaignsChangeable     = assignedCampaignsChangeable;
	}


	/**
	 * Checks whether or not the assigned campaigns information is changeable.
	 *
	 * @return <code>true</code> indicates that the information is changeable,
	 *         <code>false</code> indicates that the information cannot be changed.
	 */
	public boolean isAssignedCampaignsChangeable() {
		return assignedCampaignsChangeable;
	}


	/**
	 * Returns the list of campaigns assigned to the item.
	 *
	 * @return CampaignList the list of campaigns assigned to the item
	 */
	public CampaignList getAssignedCampaigns() {
		return assignedCampaigns;
	}


	/**
	 * Returns list of campaigns assigned to the item.
	 *
	 * @return CampaignListData the list of campaigns assigned to the item
	 */
	public CampaignListData getAssignedCampaignsData() {
		return assignedCampaigns;
	}


	/**
	 * Sets the list of campaigns assigned to the item.
	 *
	 * @param CampaignList the list of campaigns assigned to the item
	 */
	public void setAssignedCampaigns(CampaignList assignedCampaigns) {
		this.assignedCampaigns = assignedCampaigns;
	}


	/**
	 * Sets list of campaigns assigned to the item.
	 *
	 * @param CampaignListData the list of campaigns assigned to the item
	 */
	public void setAssignedCampaignsData(CampaignListData assignedCampaigns) {
		this.assignedCampaigns = (CampaignList) assignedCampaigns;
	}


	/**
	 * Get the payment terms.
	 * 
	 * @return String the payment terms
	 */
	public String getPaymentTerms() {
		return paymentTerms;
	}
	

	/**
	 * Set the payment terms.
	 * 
	 * @param paymentTerms the payment terms to be set.
	 */
	public void setPaymentTerms(String paymentTerms) {
		this.paymentTerms = paymentTerms;
	}
	
	/**
	 * Checks whether or not the paymentterms information is changeable.
	 *
	 * @return <code>true</code> indicates that the information is changeable,
	 *         <code>false</code> indicates that the information cannot be changed.
	 */
	public boolean isPaymentTermsChangeable() {
		return paymentTermsChangeable;
	}

	/**
	 * Get the payment terms description.
	 * 
	 * @return String the payment terms description.
	 */
	public String getPaymentTermsDesc() {
		return paymentTermsDesc;
	}


	/**
	 * Set the payment terms description
	 * 
	 * @param paymentTermsDesc the payment terms description to be set.
	 */
	public void setPaymentTermsDesc(String paymentTermsDesc) {
		this.paymentTermsDesc = paymentTermsDesc;
	}


	/**
	 * Get the incoterms1
	 * 
	 * @return String the incoterms1
	 */
	public String getIncoTerms1() {
		return incoTerms1;
	}


	/**
	 * Set the incoterms1.
	 * 
	 * @param incoTerms1 the incoterms1 to be set.
	 */
	public void setIncoTerms1(String incoTerms1) {
		this.incoTerms1 = incoTerms1;
	}
	
	/**
	 * Checks whether or not the incoterms1 information is changeable.
	 *
	 * @return <code>true</code> indicates that the information is changeable,
	 *         <code>false</code> indicates that the information cannot be changed.
	 */
	public boolean isIncoTerms1Changeable() {
		return incoTerms1Changeable;
	}

	/**
	 * Get the incoterms1 description
	 * 
	 * @return String the incoterms1 description
	 */
	public String getIncoTerms1Desc() {
		return incoTerms1Desc;
	}


	/**
	 * Set the incoterms1 description
	 * 
	 * @param incoTerms1Desc the incoterms1 description to be set.
	 */
	public void setIncoTerms1Desc(String incoTerms1Desc) {
		this.incoTerms1Desc = incoTerms1Desc;
	}


	/**
	 * Get the incoterms2.
	 * 
	 * @return String the incoterms2.
	 */
	public String getIncoTerms2() {
		return incoTerms2;
	}


	/**
	 * Set the incoterms2.
	 * 
	 * @param incoTerms2 the incoterms2 to be set.
	 */
	public void setIncoTerms2(String incoTerms2) {
		this.incoTerms2 = incoTerms2;
	}
	
	/**
	 * Checks whether or not the incoterms2 information is changeable.
	 *
	 * @return <code>true</code> indicates that the information is changeable,
	 *         <code>false</code> indicates that the information cannot be changed.
	 */
	public boolean isIncoTerms2Changeable() {
		return incoTerms2Changeable;
	}
    
    /**
     * Returns the recall description
     * 
     * @return the recall description
     */
    public String getRecallDesc() {
        return recallDesc;
    }

    /**
     * Returns the recall id
     * 
     * @return the recall id
     */
    public String getRecallId() {
        return recallId;
    }

    /**
     * Sets the recall description
     * 
     * @param recallDesc the recall description
     */
    public void setRecallDesc(String recallDesc) {
        this.recallDesc = recallDesc;
    }

    /**
     * Sets the recall id
     * 
     * @param recallId the recall id
     */
    public void setRecallId(String recallId) {
        this.recallId = recallId;
    }
    
    /**
     * Determines if manual an ATP substitution occured during the last update
     * 
     * @return boolean true if there was at least on item where an ATP
     *         substitution occured during the last update
     */
    public boolean hasATPSubstOccured() {
        return hasATPSubstOccured;
    }

    /**
     * Sets the hasATPSubstOccured flag
     * 
     * @param hasATPSubstOccured true if there are items were
     * an ATP substitution took plave during the last update
     */
    public void setATPSubstOccured(boolean hasATPSubstOccured) {
        this.hasATPSubstOccured = hasATPSubstOccured;
    }
    
	/**
	 * Get the LatestDlvDate.
	 *
	 * @return the Latest Delivery Date
	 */
	public String getLatestDlvDate() {
		return latestDlvDate;
	}

	/**
	 * Set the Latest Delivery Date.
	 *
	 * @param LatestDlvDate the Latest Delivery Date to be set.
	 */
	public void setLatestDlvDate(String latestDlvDate) {
		this.latestDlvDate = latestDlvDate;
	}

	/**
	 * Checks whether or not the Latest Delivery Date is changeable.
	 *
	 * @return <code>true</code> indicates that the information is changeable,
	 *         <code>false</code> indicates that the information cannot be changed.
	 */
	public boolean isLatestDlvDateChangeable() {
		return latestDlvDateChangeable;
	}
	
//	auction related	
	
	 /**
	  * Returns total manual price condition
	  * @return total manual price condition
	  */
	 public String getTotalManualPriceCondition() {
		 return this.totalPriceType;
	 }

	 /*
	  * Sets the total manual price condition
	  * @param priceType manual price conidtion
	  */
	 public void setTotalManualPriceCondition(String priceType) {
		 this.totalPriceType = priceType;		
	 }

	 /* 
	  * Returns shipping manual price condition
	  */
	 public String getShippingManualPriceCondition() {
		 return shippingPriceType;
	 }

	 /* 
	  * Sets the shipping manual price condition 
	  * @param priceType new shipping manual price condition
	  */
	 public void setShippingManualPriceCondition(String priceType) {
		 shippingPriceType = priceType;
	 }	


	/*
	 * Sets the document GUID of the solution configurator 
	 * @return GUID of the SC document
	 */
	public TechKey getScDocumentGuid() {
		return scDocumentGuid;
	}

	/*
	 * Returns the document GUID of the solution configurator
	 * @param scDocumentGuid GUID of the SC document
	 */
	public void setScDocumentGuid(TechKey key) {
		scDocumentGuid = key;
	}


	/* (non-Javadoc)
	 * @see com.sap.isa.backend.boi.isacore.order.HeaderData#getLoyaltyType()
	 * Gets one of the HeaderData.LOYALTY_TYPE_... values
	 */
	public String getLoyaltyType() {
		return loyaltyType;
	}


	/* (non-Javadoc)
	 * @see com.sap.isa.backend.boi.isacore.order.HeaderData#getTotalRedemptionValue()
	 */
	public String getTotalRedemptionValue() {
		return totalRedemptionValue;
	}


	/* (non-Javadoc)
	 * @see com.sap.isa.backend.boi.isacore.order.HeaderData#getMemberShipId()
	 */
	public LoyaltyMembershipConfiguration getMemberShip() {
		return memberShipId;
	}


	/* (non-Javadoc)
	 * @see com.sap.isa.backend.boi.isacore.order.HeaderData#setMemberShipId(java.lang.String)
	 */
	public void setMemberShip(LoyaltyMembershipConfiguration loyMembershipId) {
		memberShipId = loyMembershipId;
	}


	/* (non-Javadoc)
	 * @see com.sap.isa.backend.boi.isacore.order.HeaderData#setTotalRedemptionValue(java.lang.String)
	 */
	public void setTotalRedemptionValue(String totalValue) {
		totalRedemptionValue = totalValue;
	}


	/* (non-Javadoc)
	 * @see com.sap.isa.backend.boi.isacore.order.HeaderData#setLoyaltyType(java.lang.String)
	 * Sets one of the HeaderData.LOYALTY_TYPE_... values
	 */
	public void setLoyaltyType(String type) {
		loyaltyType = type;
	}

}
