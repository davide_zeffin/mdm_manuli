/*****************************************************************************
    Class:        HeaderBillingDocument
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author        Ralf Witt
    Created:      May 2001

    $Revision: #2 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.businessobject.header;

import com.sap.isa.backend.boi.isacore.billing.HeaderBillingData;

/**
 * Common Header Information for all objects of the bo layer.
 *
 * @author Ralf witt
 * @version 1.0
 */
public class HeaderBillingDocument extends HeaderSalesDocument
               implements HeaderBillingData, Cloneable {


    private String dueAt;
    private String billingDocNo;
    private String paymentTerms;
    private String billingDocumentsOrigin;
    private String companyCode;
    private boolean reverseDocument = false;

    /**
     *
     */
    public String getDueAt() {
        return dueAt;
    }

    /**
     *
     */
    public void setDueAt(String dueAt) {
        this.dueAt = dueAt;
    }

    /**
     *
     */
    public String getBillingDocNo() {
        return billingDocNo;
    }

    /**
     *
     */
    public void setBillingDocNo(String billingDocNo) {
        this.billingDocNo = billingDocNo;
    }

    /**
     *
     */
    public String getPaymentTerms() {
        return paymentTerms;
    }

    /**
     *
     */
    public void setPaymentTerms(String payTerms) {
        this.paymentTerms = payTerms;
    }
    /**
     *
     */
    public String getBillingDocumentsOrigin(){
        return billingDocumentsOrigin;
    }

    /**
     *
     */
    public void setBillingDocumentsOrigin(String billingDocOrigin){
        this.billingDocumentsOrigin = billingDocOrigin.trim();
    }

    /**
     *
     */
    public void setDocumentTypeInvoice(){
        this.documentType = DOCUMENT_TYPE_INVOICE;
    }

    /**
     *
     */
    public void setDocumentTypeCreditMemo(){
        this.documentType = DOCUMENT_TYPE_CREDITMEMO;
    }
    /**
     *
     */
    public void setDocumentTypeDownPayment(){
        this.documentType = DOCUMENT_TYPE_DOWNPAYMENT;
    }
	/**
	 * Set Document is a reverse document to TRUE (i.e. original invoice has
	 * been cancelled and this is the related cancellation / reverse document)
	 */
	public void setReverseDocumentTrue() {
		this.reverseDocument = true;
	}
	/**
	 * Get cancellation info for this document (i.e. original invoice has been
	 * cancelled and this is the related cancellation / reverse document)
     */
	public boolean isReversedDocument() {
		return this.reverseDocument;
	}
    /**
     * Set Companycode for the document
     */
    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }
    /**
     * Get document's company code
     */
    public String getCompanyCode() {
    	return this.companyCode;
    }
    
    /**
     * Performs a shallow copy of this object. Because of the fact that
     * all fields of this object consist of immutable objects like
     * <code>String</code> and <code>TechKey</code> the shallow copy behaves
     * like a deep copy.
     *
     * @return shallow copy of this object
     */
    public Object clone() {
        return super.clone();
    }
}
