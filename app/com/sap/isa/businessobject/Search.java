/*****************************************************************************
    Class:        Search
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP
    Created:      29.3.2001
    Version:      1.0

    $Revision: #4 $
    $Date: 2001/07/25 $
*****************************************************************************/

package com.sap.isa.businessobject;

import com.sap.isa.core.eai.*;
import com.sap.isa.core.businessobject.BackendAware;


/**
 * This class is used for searching data in the underlying backend data
 * storage and provide therefor a reference to the BackendObjectManager.<br>
 * Do define a search create a class, which implements the {@link SearchCommand} 
 * interface. The search will be started with the {@link #performSearch} method. <p>
 * <b>Example</b>
 * <pre>
 * 
 * Search search = bom.getSearch();		
 * 		
 * SearchCommand searchCommand = new MySearchCommand();
 * 
 * SearchResult searchResult = search.performSearch(searchCommand);
 * 
 * </pre>
 *
 * @author SAP
 * @version 1.0
 */
public class Search implements BackendAware {

    protected BackendObjectManager bem;       

    /**
     * Exception thrown if the given implementation of
     * <code>SearchCommand</code> is unknown.
     *
     * @author Thomas Smits
     * @version 1.0
     */
    public static class UnknownSearchCommandException extends RuntimeException {
        public UnknownSearchCommandException() {
            super();
        }

        public UnknownSearchCommandException(String msg) {
            super(msg);
        }
    }

    public Search() {
    }

    /**
     * Performs the search operation specified by the given command object and
     * returns the result. To create a new search operation, you have to write
     * your own class implementing the <code>SearchCommand</code> interface.
     *
     * @param cmd Command describing what to search
     * @return Result of the search operation
     */
    public SearchResult performSearch(SearchCommand cmd)
            throws CommunicationException {

		return cmd.performSearch(bem);
    }


    /**
     * Method the object manager calls after a new object is created.
     *
     * @param bom Reference to the BackendObjectManager object
     */
    public void setBackendObjectManager(BackendObjectManager bem) {
        this.bem = bem;
    }
    
    
    /**
     * Returns the backend object manager.
     * @return BackendObjectManager
     */
    protected BackendObjectManager getBem() {
        return bem;
    }

}