/*****************************************************************************
    Class:        ProductBatchBase
    Copyright (c) 2001, SAP AG, Germany, All rights reserved.
    Author:       Daniel Seise
    Created:      07.12.2002
    Version:      1.0

    $Revision: #1 $
    $Date: 2002/12/07 $
****************************************************************************/

package com.sap.isa.businessobject;

import java.util.Iterator;

import com.sap.isa.backend.boi.isacore.BatchCharListData;
import com.sap.isa.backend.boi.isacore.BatchCharValsData;
import com.sap.isa.backend.boi.isacore.ProductBatchBaseData;
import com.sap.isa.core.Iterable;
import com.sap.isa.core.TechKey;



public class ProductBatchBase
	extends BusinessObjectBase
	implements  Iterable, ProductBatchBaseData{

	//protected BackendObjectManager bem;
	protected BatchCharList batchListDB = new BatchCharList();
    
    
    /**
     * Sets the whole list of elements in one method call
     *
     * @param batchList List containing the elements
     */
    public void setBatchCharListDB(BatchCharList batchListDB) {
        this.batchListDB = batchListDB;
    }
    
    /**
     * Adds an <code>Element</code> to the productbatch.
     * This elment must be uniquely
     * identified by a technical key.
     * A <code>null</code> reference passed to this function will be ignored,
     * in this case nothing will happen.
     *
     * @param item Item to be added to the basket
     */
    public void addElement(BatchCharValsData batchData) {
      if (batchData instanceof BatchCharVals) {
        BatchCharVals element = (BatchCharVals)batchData;
        if (element != null) {
            batchListDB.add(element);
        }
      }
    }
    
    /**
     * Creates an <code>BatchCharValsData</code> for the productbatch.
     * This element must be uniquely identified by a technical key.
     *
     * @return element Element
     */
    public BatchCharValsData createElement() {
      return (BatchCharValsData) new BatchCharVals();
    }
    
    /**
     * Creates a <code>BatchCharListData</code>.
     *
     * @return BatchCharListData an empty BatchCharListData
     */
    public BatchCharListData createBatchListDB() {
      return (BatchCharListData) new BatchCharList();
    }
    
    /**
     * Releaes state of the elementmap. This method can be used to drop state
     * information between HTTP request to save memory ressources.
     */
    public void clearElements() {
        batchListDB.clear();
    }
    
    /**
     * Returns a list of the elements currently stored inside the productbatch.
     *  
     *
     * @return List with elements
     */
    public BatchCharList getElements() {
        return batchListDB;
    }
    
    /**
     * Returns the element of the productbatch
     * specified by the given technical key.<br><br>
     * 
     * @param techKey the technical key of the element that should be
     *        retrieved
     * @return the element with the given techical key or <code>null</code>
     *         if no item for that key was found.
     */
    public BatchCharVals getElement(TechKey techKey) {
        return batchListDB.get(techKey);
    }

   
    /**
     * Returns the element of the productbatch
     * specified by the given technical key.<br><br>
     * 
     * @param techKey the technical key of the element that should be
     *        retrieved
     * @return the element with the given techical key or <code>null</code>
     *         if no item for that key was found.
     */
    public BatchCharValsData getBatchCharValsData(TechKey techKey) {
        return batchListDB.getBatchCharValsData(techKey);
    }

    
    /**
     * Returns a list of the elements currently stored for this productbatch.
     *
     * @return list of elements
     */
    public BatchCharListData getBatchCharListDBData() {
        return (BatchCharListData) batchListDB;
    }

    
    /**
     * Sets the list of the elements for this product batch.
     *
     * @param  batchList list of elements to be stored
     */
    public void setBatchCharListDBData(BatchCharListData batchListDB) {
        this.batchListDB = (BatchCharList) batchListDB;
    }
            
	/**
     * Sets the BackendObjectManager for the product object. This method is used
     * by the object manager to allow the user object interaction with
     * the backend logic. This method is normally not called
     * by classes other than BusinessObjectManager.
     *
     * @param bem BackendObjectManager to be used
     */
	//public void setBackendObjectManager(BackendObjectManager bem) {
	//	this.bem = bem;
	//}


	/**
     * Returns an iterator for the items. Method necessary because of
     * the <code>Iterable</code> interface.
     *
     * @return iterator to iterate over the items
     *
     */
	public Iterator iterator() {
		if (batchListDB != null) {
            return batchListDB.iterator();
        } else {
            return null;
        }
	}
	
	/**
     * Callback method for the <code>BusinessObjectManager</code> to tell
     * the object that life is over and that it has to release
     * all ressources.
     */
	public void destroy() {
        super.destroy();
        //bem        = null;
        batchListDB   = null;
    }

}
