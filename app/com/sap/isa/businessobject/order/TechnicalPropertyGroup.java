/*****************************************************************************
    Class:        TechnicalPropertyGroup
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Annette Stasch
    Created:      Januray 2006
    Version:      1.0

*****************************************************************************/
package com.sap.isa.businessobject.order;

import com.sap.isa.backend.boi.isacore.order.TechnicalPropertyData;
import com.sap.isa.backend.boi.isacore.order.TechnicalPropertyGroupData;

import com.sap.isa.maintenanceobject.businessobject.PropertyGroup;

/**
 * The class describes a technical property List object.
 *
 * @author Annette Stasch
 * @version 1.0
 */

public class TechnicalPropertyGroup
	extends PropertyGroup
	implements TechnicalPropertyGroupData {

	

	/**
	  * Standard Constructor.
	  *
	  */
	public TechnicalPropertyGroup() {
	}

	/**
	 * Constructor for a technical property group with the given name.
	 * @param name, name of group
	 *
	 */
	public TechnicalPropertyGroup(String name) {
		super(name);
	}

	
	

	/**
	 * creates a new technical property.
	 * @param technicalPropertyName
	 * @return TechnicalPropertyData
	 */
	public TechnicalPropertyData createTechnicalProperty(String technicalPropertyName) {
		return new TechnicalProperty(technicalPropertyName);
	}

	/**
	 * creates a new technical property.
	 * @param technicalPropertyName attributeGuide
	 * @return TechnicalPropertyData
	 */
	public TechnicalPropertyData createTechnicalProperty(String technicalPropertyName, String attributeGuide) {
		return new TechnicalProperty(technicalPropertyName, attributeGuide);
	}

	

	

	

}
