/*****************************************************************************
    Class:        CollectiveOrder
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:
    Created:      November 2002
    Version:      1.0

    $Revision: #12 $
    $Date: 2002/08/05 $
*****************************************************************************/
package com.sap.isa.businessobject.order;

import com.sap.isa.backend.BackendTypeConstants;
import com.sap.isa.backend.boi.isacore.SalesDocumentBackend;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.order.CollectiveOrderBackend;
import com.sap.isa.backend.boi.isacore.order.CollectiveOrderData;
import com.sap.isa.businessobject.BusinessObjectHelper;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;


/**
 * Class representing the collective order of the internet sales scenario.
 * The collective order allows to handle a persistent order object over a long
 * time. To order the collective order finally use the <code>send</code> method.
 * 
 * @author SAP
 * @version 1.0
 *
 */
public class CollectiveOrder extends Order implements CollectiveOrderData {

    /**
     * Create a collective order in the backend.
     * The order will be set persistent directly.  
     */
    public void create(Shop shop, PartnerList partnerList, WebCatInfo catalog, CampaignListEntry campaignListEntry) 
        throws CommunicationException {
		final String METHOD = "create()";
		log.entering(METHOD);
        init(shop,null,partnerList,catalog,campaignListEntry); 
        // save the document in tne backend
        saveAndCommit();
        log.exiting();
    }


    /**
     * Finally sends the order in the backend.
     */
    public void send() throws CommunicationException {

		final String METHOD = "send()";
		log.entering(METHOD);

        try {
            ((CollectiveOrderBackend) getBackendService()).sendInBackend(this);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally {
        	log.exiting();
        }
    }


    /**
     * Enqueue Order for creation of unique order for the combination contact und soldto.
     *
     * @param contact contact 
     * @param soldto soldto 
     *
     */
    public void enqueue(TechKey contact, TechKey soldto)
            throws CommunicationException {
		final String METHOD = "enqueue()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("contact="+contact+", soldTo="+soldto);   
       try {
            ((CollectiveOrderBackend) getBackendService()).enqueueInBackend(this, contact, soldto);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally {
        	log.exiting();
        }
    }


    /**
     * Dequeue Order for creation for the combination contact und soldto.
     *
     * @param contact contact 
     * @param soldto soldto 
     *
     */
    public void dequeue(TechKey contact, TechKey soldto)
            throws CommunicationException {
		final String METHOD = "dequeue()";
		log.entering(METHOD);  
		if (log.isDebugEnabled())
			log.debug("contact="+contact+", soldTo="+soldto);     
       try {
            ((CollectiveOrderBackend) getBackendService()).dequeueInBackend(this, contact, soldto);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally {
        	log.exiting();
        }
    }


	/**
	 * @see com.sap.isa.businessobject.SalesDocument#getBackendService()
	 */
	protected SalesDocumentBackend getBackendService()
		throws BackendException {

        synchronized (this) {
            if (backendService == null) {
                backendService =
                    (CollectiveOrderBackend) bem.createBackendBusinessObject(BackendTypeConstants.BO_TYPE_COLLECTIVEORDER, null);
            }
        }
        return backendService;
	}


    /**
     * Set global data in the backend
     *
     * @return <true> if check was successful; otherwise <code>false</code>
     */
    public void setGData(ShopData shop, WebCatInfo salesDocWebCat) throws CommunicationException {
		final String METHOD = "setGData()";
		log.entering(METHOD);
        // write some debugging info
        if (log.isDebugEnabled()) {
            log.debug("setGData(): shop = " + shop + ", salesDocWebCat = " + salesDocWebCat);
        }

        this.salesDocWebCat = salesDocWebCat;

        try {
            ((CollectiveOrderBackend) getBackendService()).setGData(this, shop);
            isDirty = true;
            header.setDirty(true);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally {
        	log.exiting();
        }
    }
    
	/**
	 * Set global data in the backend.
	 *
	 * @return <true> if check was successful; otherwise <code>false</code>
	 */
	public void setGData(ShopData shop) throws CommunicationException {
		setGData(shop, null);
	}	
  
}
