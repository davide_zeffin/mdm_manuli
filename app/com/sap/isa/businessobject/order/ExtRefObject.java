/*****************************************************************************
	Class:        ExtRefObject
	Copyright (c) 2004, SAP AG, Germany, All rights reserved.

	Revision:	  01
	Date: 		  April 21, 2005
*****************************************************************************/
package com.sap.isa.businessobject.order;


import com.sap.isa.backend.boi.isacore.order.ExtRefObjectData;
import com.sap.isa.businessobject.BusinessObjectBase;

/**
 * Action performs somthing.
 *
 * @author SAP
 * @version 1.0
 * 
 */
public class ExtRefObject extends BusinessObjectBase
							   implements ExtRefObjectData {
							   	
protected String refObjectData;							   	
							   	
	/**
	 * Gets external reference data
	 * @return external reference data
	*/
	
	public String getData() {
		return this.refObjectData;
	}
	
	/**
	 * Sets external reference data
	 * @param refData external reference data
	*/
	public void setData(String refObjectData) {
		this.refObjectData = refObjectData;
	}
							   	
}