/*****************************************************************************
    Class:        PartnerListEntry
    Copyright (c) 2002, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      11.06.2002
    Version:      1.0

    $Revision: #2 $
    $Date: 2002/11/25 $
*****************************************************************************/

package com.sap.isa.businessobject.order;

import com.sap.isa.backend.boi.isacore.order.PartnerListEntryData;
import com.sap.isa.businessobject.Address;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.core.TechKey;

/**
 * Entry for a businesspartner
 *
 * @author
 * @version 1.0
 */
public class PartnerListEntry 
implements PartnerListEntryData, Cloneable {

    // List of businesspartners
    protected TechKey partnerGUID;
    protected String partnerId;
	protected String  handle = "";

	
    /**
     * Creates a new <code>PartnerListEntry</code> object.
     */
    public PartnerListEntry() {
    }

    /**
     * Creates a new <code>PartnerListEntry</code> object.
     *
     * @param partnerGUID techkey of the business partner
     * @param partnerid id of the business partner
     */
    public PartnerListEntry(TechKey partnerGUID, String partnerId) {
        this.partnerGUID = partnerGUID;
        this.partnerId = partnerId;
    }

    /**
     * Get the TechKey of the PartnerListEntry
     *
     * @return TechKey of entry
     */
    public TechKey getPartnerTechKey() {
        return partnerGUID;
    }

    /**
     * Set the TechKey of the PartnerListEntry
     *
     * @param partnerGUID techkey of the business partner
     */
    public void setPartnerTechKey(TechKey partnerGUID) {
        this.partnerGUID = partnerGUID;
    }

    /**
     * get the Id of the PartnerListEntry
     *
     * @return id of entry
     */
    public String getPartnerId() {
        return partnerId;
    }


    /**
     * Set the TechKey of the PartnerListEntry
     *
     * @param bpTechKey theckey of the business partner
     */
    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }


    /**
     * get the first- and lastname of the Partner, separated by a space
     *
     * @return first and lastname of partner
     */
    public String getPartnerName(BusinessPartnerManager bupama) {
        Address bpAddress = bupama.getBusinessPartner(getPartnerTechKey()).getAddress();
        return bpAddress.getFirstName() + " " + bpAddress.getName();
    }


    /**
     * get the first- and lastname and the city of the Partner
     *
     * @return first, fastname and the city of the partner
     */
    public String getPartnerNameAndCity(BusinessPartnerManager bupama) {
        Address bpAddress = bupama.getBusinessPartner(getPartnerTechKey()).getAddress();
        return bpAddress.getFirstName() + " " + bpAddress.getName()+ ", " + bpAddress.getPostlCod1() + " " + bpAddress.getCity();
    }

    /**
     * get the first- and lastname, the street and the city of the Partner
     *
     * @return first, fastname, stret and the city of the partner
     */
    public String getPartnerNameAndStreetAndCity(BusinessPartnerManager bupama) {
        Address bpAddress = bupama.getBusinessPartner(getPartnerTechKey()).getAddress();
        return bpAddress.getFirstName() + " " + bpAddress.getName()
               + ", " + bpAddress.getStreet() + " " + bpAddress.getHouseNo()
               + ", " + bpAddress.getPostlCod1() + " " + bpAddress.getCity();
    }



    /**
     * get the Address Object the Partner
     *
     * @return address of the partner
     */
    public Address getPartnerAddress(BusinessPartnerManager bupama) {
        return bupama.getBusinessPartner(getPartnerTechKey()).getAddress();
    }


    /**
     * Performs a deep copy of this object.
     * Because of the fact that
     * all fields of this object consist of immutable objects like
     * <code>String</code> and <code>TechKey</code> or primitive types
     * the shallow copy is identical with a deep copy. For the
     * <code>payment</code> property an explicit copy operation is performed
     * because this object is the only mutable one.
     *
     * @return shallow (deep-like) copy of this object
     */
    public Object clone() {
        PartnerListEntry myClone = new PartnerListEntry(partnerGUID, partnerId);
        return myClone;

    }
    

	/**
	  *
	  * This method returns the handle, as an alternative key for the business object,
	  * because at the creation point no techkey for the object exists. Therefore
	  * maybay the handle is needed to identify the object in backend
	  *
	  * return the handle of business object which is needed to identify the object
	  * in the backend, if the techkey still not exists
	  *
	  */	 
	public String getHandle() {
		return handle;
	}
	
	/**
	 * This method sets the handle, as an alternative key for the business object,
	 * because at the creation point no techkey for the object exists. Therefore
	 * maybay the handle is needed to identify the object in backend
	 *
	 * @param handle the handle of business object which identifies the object
	 * in the backend, if the techkey still not exists
	 *
	 */
	public void setHandle(String string) {
		this.handle = string;
	}

}
