/*****************************************************************************
    Class:        CollectiveOrderChange
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:
    Created:      14.10.2002
    Version:      1.0

    $Revision: #11 $
    $Date: 2002/08/05 $
*****************************************************************************/
package com.sap.isa.businessobject.order;

import com.sap.isa.businessobject.CommunicationException;


/**
 * Decorator for the collective order to provide functionality only available in special
 * situations. This class acts as a decorator for the order and offers methods
 * that can only be applied when the order is in the state of being changed.
 *
 * @see com.sap.isa.businessobject.order.CollectiveOrder
 *
 * @author SAP
 * @version 1.0
 *
 */
public class CollectiveOrderChange extends OrderChange {

    /**
     * Constructor for CollectiveOrderChange.
     */
    public CollectiveOrderChange() {
        super();
    }


    /**
     * Constructor for CollectiveOrderChange.
     * @param order
     */
    public CollectiveOrderChange(CollectiveOrder collectiveOrder) {
        super(collectiveOrder);
    }


    /**
     * Finally sends the order in the backend.
     */
    public void send() throws CommunicationException {
       ((CollectiveOrder)order).send();         
    }


}
