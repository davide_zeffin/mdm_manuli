/*****************************************************************************
    Class:        OrderDataContainer
    Copyright (c) 2003, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    
    $Revision: #2 $
    $DateTime: 2003/10/01 16:07:42 $ (Last changed)
    $Change: 151875 $ (changelist)
*****************************************************************************/
package com.sap.isa.businessobject.order;

import com.sap.isa.backend.boi.isacore.SalesDocumentBackend;
import com.sap.isa.core.eai.BackendException;

/**
 * Class representing an order as a data container in the internet sales
 * scenario.<br><br>
 * Class overrides method getBackendService which allways end in a
 * BackendException. Class is only intended to be used as a Data Container
 *
 */
public class OrderDataContainer extends Order {
        
    boolean throwEx = false;
                    
    public OrderDataContainer() {
        throwEx = true;
        isDirty = true;
    }
                    
	protected SalesDocumentBackend getBackendService()
			throws BackendException{

        if (throwEx) {
            throw new BackendException("The OrderDataContainer class is by design not backend aware !");
        }

        return null;
	}


}
