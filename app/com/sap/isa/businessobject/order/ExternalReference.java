/*****************************************************************************
	Class:        ExternalReference
	Copyright (c) 2004, SAP AG, Germany, All rights reserved.

	Revision:	  01
	Date: 		  Dec 2, 2004
*****************************************************************************/
package com.sap.isa.businessobject.order;

import com.sap.isa.backend.boi.isacore.order.ExternalReferenceData;
import com.sap.isa.businessobject.BusinessObjectBase;

/**
 * Action performs somthing.
 *
 * @author SAP
 * @version 1.0
 * 
 */
public class ExternalReference extends BusinessObjectBase
							   implements ExternalReferenceData {

	protected String description;
	protected String refType;
	protected String refData;
	 
	/**
	 * Gets description of external reference
	 * @return description
	 */
	public String getDescription() {
		return this.description;
	}
	
	/**
	 * Sets description of external reference
	 * @param description description
	 */
	public void setDescription(String description){
		this.description = description;		
	}
		
	/**
	 * Gets external reference type
	 * @return type
	 */
	public String getRefType() {
		return this.refType;
	}
	
	/**
	 * Sets external reference type
	 * @param refType external reference type
	 */
	public void setRefType(String refType) {
		this.refType = refType;
	}
	
	/**
	 * Gets external reference data
	 * @return external reference data
	 */
	public String getData() {
		return this.refData;
	}
	
	/**
	 * Sets external reference data
	 * @param refData external reference data
	 */
	public void setData(String refData) {
		this.refData = refData;
	}


}
