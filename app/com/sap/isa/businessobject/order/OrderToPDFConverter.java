/*****************************************************************************
    Copyright (c) 2000, SAPLAbs Europe GmbH, Germany, All rights reserved.
*****************************************************************************/
package com.sap.isa.businessobject.order;

import java.util.Collection;

import com.sap.isa.backend.BackendTypeConstants;
import com.sap.isa.backend.boi.isacore.order.OrderToPDFConverterBackend;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.businessobject.BusinessObjectHelper;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;

/**
 * This is used to convert the order object into PDF format.
 * 
 */
public class OrderToPDFConverter extends BusinessObjectBase
implements BackendAware,
    BackendBusinessObjectParams {
    protected OrderToPDFConverterBackend orderToPDFConverterBackend;
    protected BackendObjectManager bem;

    /**
     * Constructor.
     */
    public OrderToPDFConverter() {
        super();
    }
    
    /**
     * Sets the BackendObjectManager for the attribute set. This method is used
     * by the object manager to allow the user object interaction with
     * the backend logic. This method is normally not called
     * by classes other than BusinessObjectManager.
     *
     * @param bem BackendObjectManager to be used
     */
    public void setBackendObjectManager(BackendObjectManager bem) {
        this.bem = bem;
    }
    
    // get the OrderToPDFConverterBackend, if necessary
    protected OrderToPDFConverterBackend getOrderToPDFConverterBackend()
        throws BackendException {
        if (orderToPDFConverterBackend == null) {
            orderToPDFConverterBackend = (OrderToPDFConverterBackend)
            bem.createBackendBusinessObject(BackendTypeConstants.BO_TYPE_ORDER2PDFCONVERTER, null);
        }
        return orderToPDFConverterBackend;
    }
    
    /**
    *
    * Convert the order to PDF using its techkey and the language
    *
    */
    public byte [] convertOrderToPDF(TechKey key,
                                    String language) throws CommunicationException {
		final String METHOD = "convertOrderToPDF()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("key="+key+", lang="+language);   
        byte [] str = null;
        // read oci lines from backend
        try {
            str = getOrderToPDFConverterBackend().convertOrderToPDF(key,
                                                                    language);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally {
        	log.exiting();
        }
        return str;
    }
    
    /**
    *
    * For the mass download of orders 
    * Convert the orders to PDF using its techkey and the language
    * Collection of keys to the backend and it will return the value in string
    *
    */
    public byte [] convertOrderToPDF(Collection keys,
                                    String language) throws CommunicationException {
		final String METHOD = "convertOrderToPDF()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("language="+language);   
        byte [] str = null;
        // read oci lines from backend
        try {
            str = getOrderToPDFConverterBackend().convertOrderToPDF(keys,
                                                                    language);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally {
        	log.exiting();
        }
        return str;
    }
}
