/*****************************************************************************
    Class:        DocumentListFilter
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author        Ralf Witt
    Created:      March 2001

    $Revision: #4 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.businessobject.order;

import java.util.ArrayList;
import java.util.Iterator;

import com.sap.isa.backend.boi.isacore.order.DocumentListFilterData;

/**
 * The DocumentListFilter class represents the a filter structure for documents
 *
 * @author Ralf Witt
 * @version 1.0
 */
public class DocumentListFilter implements DocumentListFilterData, Cloneable {


    /*
     *  see setter methods for the documentation of private variables
     */
    private String type;
    private String status;
    private String changedDate;
    private String changedToDate;
    private String externalRefNo;
    private String description;
    private String id;
    private String product;
    private String soldto;
    private String userStatus;

    private ArrayList serviceStatus = new ArrayList();
    /**
     * Default constructor to create a DocumentListFilter
     */
     public DocumentListFilter() {
       // Set default
       type = SALESDOCUMENT_TYPE_ORDER;
       status = SALESDOCUMENT_STATUS_ALL;
       userStatus = "";                                                       // INIT
      }

    /**
     * Returns the id of the sales document
     *
     * @return ID is used to identify the sales document in the front- and in
     *         backend.
     */
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    /**
     * Returns the external Referencenumber of the sales document
     *
     * @return externalRefNo is used to identify the sales document in the front- and in
     *         backend from outside
     */
    public String getExternalRefNo() {
        return externalRefNo;
    }

    public void setExternalRefNo(String externalRefNo) {
        this.externalRefNo = externalRefNo;
    }

    /**
     * Returns the last changed date of the document
     *
     * @return changedDate is the data the document has been last changed/created.
     */
    public String getChangedDate() {
        return changedDate;
    }

    public void setChangedDate(String changedDate) {
        this.changedDate = changedDate;
    }

    /**
     *  Set the To Date of the Period
     */
    public void setChangedToDate(String changedToDate) {
        this.changedToDate = changedToDate;
    }

    /**
     * Returns the changed to date of the document
     *
     * @return changedToDate is the data the document has been last changed/created.
     */
    public String getChangedToDate() {
        return changedToDate;
    }

    /**
     * Returns the status set to filter sales and billing documents
     *
     * @return status is the process state the salesdocument is in.
     */
    public String getStatus() {
        /* To be able to mix system status and user status on one JSP via one
           request parameter, both use one getter method
        */
        if (status == null   ||    status.equals("")) {
            return getUserStatus();
        } else {
            return status;
        }
    }

    public void setStatusACCEPTED() {
        this.status = SALESDOCUMENT_STATUS_ACCEPTED;
        this.userStatus = "";
    }

    public void setStatusREADYTOPICKUP() {
        this.status = SALESDOCUMENT_STATUS_READYTOPICKUP;
        this.userStatus = "";
    }

    /**
     * Returns the status set to filter service documents
     *
     * @return iterator to get all status
     */
   public Iterator getServiceStatus(){
       return serviceStatus.iterator();
   }

    /**
     * Returns the status set to filter sales documents by user status
     *
     * @return String to get user status
     */
   public String getUserStatus(){
       return this.userStatus;
   }

    public void setStatusALL() {
        this.status = SALESDOCUMENT_STATUS_ALL;
        this.userStatus = "";
    }

    public void setStatusOPEN() {
        this.status = SALESDOCUMENT_STATUS_OPEN;
        this.userStatus = "";
    }

    public void setStatusRELEASED() {
        this.status = SALESDOCUMENT_STATUS_RELEASED;
        this.userStatus = "";
    }

    public void setStatusCOMPLETED() {
        this.status = SALESDOCUMENT_STATUS_COMPLETED;
        this.userStatus = "";
    }

    public void setStatusCANCELLED() {
        this.status = SALESDOCUMENT_STATUS_CANCELLED;
        this.userStatus = "";
    }
    /**
     * Set User Status (E0001, E0002, ...)
     * Please be aware of the fact, if user status will be used, regular
     * status like open, completed will not taken into account. Meaning there
     * is no logical AND conjunction.
     */
    public void SetStatusUSERSTATUS(String status) {
        this.userStatus = status;
        this.status = "";
    }

    /**
     * Adds a new Service Status (E0001, E0002, ...)
     */
    public void addServiceStatus(String eStatus) {
        serviceStatus.add((Object)eStatus);
    }

    /**
     * Returns the type of the sales document (Order, Quotation, Order Template, ...)
     *
     * @return type is the sales document type
     *
     */
    public String getType() {
        return type;
    }

    public void setTypeORDER() {
        this.type = SALESDOCUMENT_TYPE_ORDER;
    }

    public void setTypeCOLLECTIVEORDER() {
        this.type = SALESDOCUMENT_TYPE_COLLECTIVE_ORDER;
        this.userStatus = "";
    }

    public void setTypeORDERTEMPLATE() {
        this.type = SALESDOCUMENT_TYPE_ORDERTEMPLATE;
    }

    public void setTypeQUOTA() {
        this.type = SALESDOCUMENT_TYPE_QUOTATION;
    }

    public void setTypeINVOICE() {
        this.type = BILLINGDOCUMENT_TYPE_INVOICE;
    }

    public void setTypeCREDITMEMO() {
        this.type = BILLINGDOCUMENT_TYPE_CREDITMEMO;
    }

    public void setTypeDOWNPAYMENT() {
        this.type = BILLINGDOCUMENT_TYPE_DOWNPAYMENT;
    }

    public void setTypeService(String serviceType) {
        this.type = serviceType;
    }

    /**
     * Returns the language dependend description of the sales document
     *
     * @return description is used to identify the sales document in a readable form
     */
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Returns a product
     *
     * @return product
     */
    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }


    /**
     * Set the property soldto
     *
     * @param soldto
     *
     */
    public void setSoldto(String soldto) {
        this.soldto = soldto;
    }


    /**
     * Returns the property soldto
     *
     * @return soldto
     *
     */
    public String getSoldto() {
       return this.soldto;
    }


    /**
     * Return a string repersentation of this object.
     *
     * @return String representation
     */
    public String toString() {
        StringBuffer buf = new StringBuffer(100);

        buf.append("DocumentListFilter [")
           .append("changedDate='").append(getChangedDate()).append("', ")
           .append("changedToDate='").append(getChangedToDate()).append("', ")
           .append("description='").append(getDescription()).append("', ")
           .append("externalRefNo='").append(getExternalRefNo()).append("', ")
           .append("id='").append(getId()).append("', ")
           .append("product='").append(getProduct()).append("', ")
           .append("soldto='").append(getSoldto()).append("', ")
           .append("status='").append(getStatus()).append("', ")
           .append("userStatus='").append(getUserStatus().toString()).append("', ")
           .append("type='").append(getType()).append("']");

        return buf.toString();
    }

    /**
     * Returns a shallow copy of this object. Because of the fact that this
     * class only contains immutuable objects like String, the shallow
     * copy produced by this method behaves like a deep copy.
     *
     * @return shallow copy of the object
     */
    public Object clone() {
        try {
            return super.clone();
        }
        catch (CloneNotSupportedException e) {
            return null;
        }
    }
}
