/*****************************************************************************
	Class:        ExternalReferenceList
	Copyright (c) 2004, SAP AG, Germany, All rights reserved.

	Revision:	  01
	Date: 		  Dec 2, 2004
*****************************************************************************/
package com.sap.isa.businessobject.order;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.sap.isa.backend.boi.isacore.order.ExternalReferenceData;
import com.sap.isa.backend.boi.isacore.order.ExternalReferenceListData;
import com.sap.isa.core.Iterable;
import com.sap.isa.core.TechKey;

/**
 * Class representing the list of external reference
 *
 * @author SAP
 * @version 1.0
 * 
 */
public class ExternalReferenceList implements ExternalReferenceListData, Iterable {
    protected List extRefList;

	/**
	 * Constructor
	 */
    public ExternalReferenceList() {
		extRefList = new ArrayList();
    }

	/**
	 * Gets list as Iterator
	 */
    public Iterator iterator() {
		return extRefList.iterator();
    }

	/**
	 * Adds new external reference
	 * @param extRef external reference object
	 */
	public void addExtRef(ExternalReferenceData extRef) {
		extRefList.add(extRef);
    }

	/**
	 * Removes an external reference
	 * @param extRef external reference object
	 * @return reference to removed external reference object
	 */
    public ExternalReferenceData removeExtRef(ExternalReferenceData extRef) {
		extRefList.remove(extRef);
		return extRef;
    }

	/**
	 * Removes an external reference
	 * @param techKey key of external reference
	 * @return reference to removed external reference object if found, otherwise null will be returned
	 */
    public ExternalReferenceData removeExtRef(TechKey techKey) {
    	Iterator itList = this.iterator();
		while (itList.hasNext()) {
			ExternalReferenceData extRef = (ExternalReferenceData)itList.next();
			if (extRef.getTechKey().equals(techKey)) {
				extRefList.remove(extRef);
				return extRef;				
			}
		}
		return null;
    }

	/**
	 * Gets external reference
	 * @param techKey key of external reference
	 * @return reference to external reference object if found, otherwise null will be returned
	 */	
    public ExternalReferenceData getExtRef(TechKey techKey) {
    	Iterator itemList = this.iterator(); 
		while (itemList.hasNext()) {
			ExternalReferenceData extRef = (ExternalReferenceData)itemList.next();
			if (extRef.getTechKey().equals(techKey)) {
				return extRef;				
			}
		}
		return null;
    }
    
	/**
	 * Reinitializes the list
	 */
    public void clear() {
		extRefList.clear();
    }

	/**
	 * Gets number of external reference in the list
	 * @return number of external reference
	 */
    public int size() {
		return extRefList.size();
    }

	/**
	 * Is empty list
	 * @return true if the list is empty, otherwise false
	 */
    public boolean isEmpty() {
    	return extRefList.isEmpty();
    }

	/**
	 * Checks if a specific external reference exists in the list
	 * @param extRef external reference object
	 * @return true if exists, otherwise false
	 */
    public boolean contains(ExternalReferenceData extRef) {
		return extRefList.contains(extRef);
    }
    
	/**
	 * Creates an empty <code>ExternalReferenceData</code>.
	 *
	 * @returns ExternalReferenceData which can be added to the external reference list
	 */
	public ExternalReferenceData createExternalReferenceListEntry()	 {
		return new ExternalReference();
	}   
	
	/**
	 * gets the external reference   for the given index
	 *
	 * @return String the external reference for the given index, or
	 *                  null if index is out of bounds
	 */
	public ExternalReferenceData getExtRef(int i) {
		return (ExternalReferenceData)extRefList.get(i); 
	}
	
	/**
	 * Performs a copy of this object.
	 *
	 * @return Object copy of this object
	 */
	public Object clone() {

		ExternalReferenceList myClone = new ExternalReferenceList();
		if (!extRefList.isEmpty() ) {
			for (int i=0; i < extRefList.size(); i++) {
				ExternalReference extRef = new ExternalReference();
				extRef.setData(((ExternalReference)extRefList.get(i)).getData()) ;
				extRef.setRefType(((ExternalReference)extRefList.get(i)).getRefType());
				myClone.addExtRef((ExternalReferenceData)extRef);
			}
		}		
		return myClone;
	}		
}
