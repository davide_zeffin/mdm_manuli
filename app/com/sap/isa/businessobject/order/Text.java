/*****************************************************************************
  Class:        Text
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Thomas Smits
  Created:      9.03.2001
  Version:      1.0

  $Revision: #2 $
  $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.businessobject.order;

import com.sap.isa.backend.boi.isacore.order.TextData;

/**
 * Representation of user configuarable texts. A text consists of an
 * id identifying the type of the text and the textual information itself.
 *
 * @author Thomas Smits
 * @version 1.0
 */
public class Text implements TextData {
    private String id = "";
    private String text = "";

    /**
     * Creates a new object without initialization of the id and text
     * information. After calling this constructor both fields are
     * initialized with <code>null</code>.
     */
    public Text() {
    }

    /**
     * Creates a new object and sets the id and text information.
     *
     * @param id Type of the text
     * @param text The text itself
     */
    public Text(String id, String text) {
        this.id = id;
        this.text = text;
    }

    /**
     * Reads the type of the text
     *
     * @return Type of text
     */
    public String getId(){
        return id;
    }

    /**
     * Sets the type of the text
     *
     * @param id The type to be set
     */
    public void setId(String id){
        if (id == null) {
            id = "";
        }
        this.id = id;
    }

    /**
     * Reads the text
     *
     * @return Text
     */
    public String getText(){
        return text;
    }

    /**
     * Sets the text
     *
     * @param text Text to be set
     */
    public void setText(String text){
        if (text == null) {
            text = "";
        }
        this.text = text;
    }

    /**
     * Returns a string Representation
     *
     * @return String giving information about the object
     */
    public String toString() {
        return "Text [id=\"" + id + "\", text=\"" + text + "\"]";
    }

    /**
     * Determines whether the given object is equal to this object
     *
     * @param o Object to compare with
     * @return <code>true</code> if the object is equal, <code>false</code>
     *         if not
     */
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        else if (o == this) {
            return true;
        }
        else if (o instanceof Text) {
            return text.equals(((Text)o).text) &&
                   id.equals(((Text)o).id);
        }
        else {
            return false;
        }
    }

    /**
     * Returns the hash code of this object
     *
     * @return hash code
     */
    public int hashCode() {
        return text.hashCode() ^ id.hashCode();
    }
}
