/*****************************************************************************
    Class:        OrderStatus
    Copyright (c) 2001, SAP AG, Germany, All rights reserved.
    Author        SAP
    Created:      March 2001
*****************************************************************************/

package com.sap.isa.businessobject.order;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.sap.ecommerce.businessobject.loyalty.LoyaltyMembership;
import com.sap.isa.backend.BackendTypeConstants;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.order.DocumentListFilterData;
import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.backend.boi.isacore.order.OrderData;
import com.sap.isa.backend.boi.isacore.order.OrderStatusBackend;
import com.sap.isa.backend.boi.isacore.order.OrderStatusData;
import com.sap.isa.backend.boi.isacore.order.PartnerListData;
import com.sap.isa.backend.boi.isacore.order.SalesDocumentAndStatusData;
import com.sap.isa.businessobject.BusinessObjectHelper;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.SalesDocumentStatus;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.item.ItemList;
import com.sap.isa.businessobject.item.ItemMap;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;

/**
 * The OrderStatus class handle the questionary which is
 * used to retrieve detail status information of sales document
 *
 * To be called by action with
 * <code>
 *  OrderStatus orderStatus = bom.createOrderStatus(new OrderDataContainer());
 * </code>
 * Means, order is only used as a datacontainer, but not to communicate with backend.
 * Thats the part of the orderstatus object.
 * 
 * <b>Support of following object usage will be stopped with
 * Internet Sales Release 5.0</b>
 * To be called by action with 
 * <code>
 * Order order = new Order(); 
 * orderStatus = bom.createOrderStatus(order);
 * </code>
 * 
 * 
 * @author SAP
 * @version 1.0
 *
 */
public class OrderStatus extends SalesDocumentStatus
      implements OrderStatusData, SalesDocumentAndStatusData {

	protected Order order;

	protected User user;
	protected TechKey soldToKey;

	
	protected Shop shop;
	protected PartnerList partnerList;

	protected DocumentListFilter filter;
	
	private OrderStatusBackend orderStatusBackend;
	private OrderStatusBackend orderTemplateStatusBackend;
	private OrderStatusBackend quatationStatusBackend;

	private boolean isMultiPartnerScenario;

	private List orderHeader = new ArrayList(50);
	private int orderCount = 0;

    /**
     * Creates a new instance for the given <code>Order</code> object.<br>
     * Since method is deprecated use OrderStatus(OrderDataContainer) method
     * instead.
     *
     * @param order Order to decorate
     * @deprecated
     */
    public OrderStatus(Order order) {
    	super();
        this.order = order;
        this.salesDocDataContainer = order; // same object but only different interface view (eCommerceBase Layer)
    }

	/**
	 * Creates a new instance for the given <code>Order</code> object.
	 *
	 * @param OrderDataContainer Order to decorate and used as Data Container
	 */
	public OrderStatus(OrderDataContainer order) {
		super();
		this.order = order;
        this.salesDocDataContainer = order; // same object but only different interface view (eCommerceBase Layer)
	}


    /**
     * Creates a new instance for the given <code>Order</code> object.
     *
     * @param order Order to decorate
     */
    public OrderStatus() {
        super();
        order = new Order();
        this.salesDocDataContainer = order; // same object but only different interface view (eCommerceBase Layer)
    }


	/**
	 * Add a new order Header to the list
	 *
	 * @param HeaderSalesDocumentData which should add to the orderlist
	 *
	 * @see HeaderSalesDocument
	 * @see HeaderSalesDocumentData
	 */
	public void addOrderHeader(HeaderData header) {
		orderHeader.add(header);
		orderCount++;
	}

	/**
	 * Remove all Head from the header list. <br>
	 */
	public void clearOrderHeader() {
		orderHeader.clear();
		orderCount = 0;
	}	
	

	/**
	 * Return flag for multi partner selections
	 *
	 * Returns true, if the selection regards more then one partner function type
	 */
	 public boolean isMultiPartnerScenario() {
		return isMultiPartnerScenario;
	 }


	/**
	 * Set flag for multi partner selections
	 *
	 */
	 public void setMultiPartnerScenario(boolean isMultiPartnerScenario) {
		 this.isMultiPartnerScenario = isMultiPartnerScenario;
	 }

    
	/**
	 * Get the Order object.<br><br>
	 * Method will return an Order / OrderDataContaienr object which has no
	 * backend representation nor it is backend aware.
	 * 
	 * @see com.sap.isa.businessobject.order.orderstatus
	 */
	public OrderData getOrder() {
	  return (OrderData)order;
	}
       

	/**
	 * Gets the number of read document headers
	 */
	public int getOrderCount() {
	  return orderCount;
	}

	/**
	 * Get the Shop
	 */
	public ShopData getShop() {
		return (ShopData)shop;
	}

	/**
	 * Set the Shop
	 */
	public void setShop(Shop shop) {
		this.shop = shop;
	}    

	/**
	 * Get the filter parameter (selection criteria)
	 */
	public DocumentListFilterData getFilter() {
		// When used as data container, object not yet set!!
		if (filter == null)
			 filter = new DocumentListFilter();

		return (DocumentListFilterData)filter;
	}


	/**
	 * Get a list of all partner to search for
	 *
	 * @returns list with partners
	 */
	public PartnerList getPartnerList() {
		if (partnerList == null) {
			partnerList = new PartnerList();
		}
		return partnerList;

	}


	/**
	 * Get a list of all partner to search for
	 *
	 * @returns list with partners
	 */
	public PartnerListData getPartnerListData() {
		return (PartnerListData)getPartnerList();

	}

	/**
	 * Get a array with the names of the requested partner functions to read in 
	 * the multi partner scenario.
	 * @return array of partner functions
	 */
	public String[] getRequestedPartnerFunctions(){
		return requestedPartnerFunctions;	
	}	

	/**
	 * Set a array with the names of the requested partner functions to read in 
	 * the multi partner scenario.
	 * @param partnerFunctions array of partner functions
	 */
	public void setRequestedPartnerFunctions(String[] partnerFunctions) {
		requestedPartnerFunctions = partnerFunctions;	
	}	


	/**
	 * Return the TechKey of the Sold-To-Party. <br>
	 * @return tech key.
	 */
	public TechKey getSoldToTechKey() {
		return soldToKey;
	}

	/**
	 * Returns an iterator for the order headers. Method necessary because of
	 * the <code>Iterable</code> interface.
	 *
	 * @return iterator to iterate over sales document item delivery
	 *
	 */
	public Iterator iterator() {
		return orderHeader.iterator();
	}

	/**
	  * get the suitable BackendService for the given sales document type
	  *
	  * @param type sales document type (see <code>DocumentListFilterData</code>
	  *             for allowed values
	  *
	  * @see com.sap.isa.backend.boi.isacore.order.DocumentListFilterData
	  */
	protected OrderStatusBackend getBackendService(String type)
				throws BackendException {

		OrderStatusBackend backendService = null;

		if (type.equals(DocumentListFilter.SALESDOCUMENT_TYPE_ORDER)) {
			if (orderStatusBackend == null) {
				orderStatusBackend = (OrderStatusBackend)
						bem.createBackendBusinessObject(BackendTypeConstants.BO_TYPE_ORDERSTATUS);
			}
			backendService = orderStatusBackend;
		}
		else if (type.equals(DocumentListFilter.SALESDOCUMENT_TYPE_ORDERTEMPLATE)) {
			if (orderTemplateStatusBackend == null) {
				orderTemplateStatusBackend = (OrderStatusBackend)
						bem.createBackendBusinessObject(BackendTypeConstants.BO_TYPE_ORDERTEMPLATESTATUS);
			}
			backendService = orderTemplateStatusBackend;
		}
		else if (type.equals(DocumentListFilter.SALESDOCUMENT_TYPE_QUOTATION)) {
			if (quatationStatusBackend == null) {
				quatationStatusBackend = (OrderStatusBackend)
						bem.createBackendBusinessObject(BackendTypeConstants.BO_TYPE_QUOTATIONSTATUS);
			}
			backendService = quatationStatusBackend;
		}

		return backendService;
	} // OrderListBackend



	/**
	 * Re-Read the object with the given key
	 *
	 */
	public void reReadOrderStatus()
			throws CommunicationException {

		final String METHOD = "reReadOrderStatus()";
		log.entering(METHOD);
		
        String docType = singleOrderHeader.getDocumentType();
		singleOrderHeader = null;
		itemMap.clear();
		itemList.clear();

		try {
			// read AttributeSet from Backend
			getBackendService(docType).readOrderStatus(this);
		}
		catch (BackendException ex) {
			BusinessObjectHelper.splitException(ex);
		} 
		finally {
			log.exiting();
		}
	}
    
    
	/**
	 * Read the object for the given techKey
	 *
	 */
	public void readOrderStatus(TechKey techKey,
								String orderNumber,
								String ordersOrigin,
								String orderType,
								LoyaltyMembership loyMemShip)
			throws CommunicationException {

		final String METHOD = "readOrderStatus()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("techKey="+techKey+", orderNumber="+orderNumber
				+", ordersOrigin="+ordersOrigin+", orderType="+orderType);  
		String type = orderType;

		if (techKey == null) {
			// re-read requested if neccessary

			HeaderSalesDocument headerSalesDoc = this.singleOrderHeader;
			if ( headerSalesDoc != null) {
				headerSalesDoc.setMemberShip(loyMemShip);
			}
			if ( headerSalesDoc == null  ||
			   ! headerSalesDoc.getTechKey().getIdAsString().trim().equals(
							order.getTechKey().getIdAsString().trim())) {
				// Last read details doesn't meet requested documents detail, so read again
			   singleOrderHeader = null;
			   itemMap.clear();
			   itemList.clear();
			   campaignDescriptions.clear();
			   campaignTypeDescriptions.clear();
			   
               // This will garantee to have an initial OrderDataContainer per document. R/3 
               // had problems while pricing flags had been set before by an previous document
               // which will not be resetted for a new document.
               
               // WSa: extension data will be puffered because in R/3 only the extension data
               // of the order document can be use. In CRM extension data of the order status object
               // will not used as inport data. 
			   HashMap extensionData = (HashMap)order.getExtensionMap();
               this.order = new OrderDataContainer();
               order.setExtensionMap(extensionData);
               order.getHeader().setMemberShip(loyMemShip);
			   selectedItemGuids.clear();
			   if (headerSalesDoc != null) {
				   type = headerSalesDoc.getDocumentType();
			   } else {
				   // Panic
			   }

			} else {
			  // quit method, data still up-to-date
			  return;
			}

		} else {
			// Normal entry, read details

            // This will garantee to have an initial OrderDataContainer per document. R/3 
            // had problems while pricing flags had been set before by an previous document
            // which will not be resetted for a new document. 

			// WSa: extension data will be puffered because in R/3 only the extension data
			// of the order document can be use. In CRM extension data of the order status object
			// will not used as inport data. 
			HashMap extensionData = (HashMap)order.getExtensionMap();
            this.order = new OrderDataContainer();
            this.salesDocDataContainer = order; // same object but only different interface view
			order.setExtensionMap(extensionData);
			order.setTechKey(techKey);
			if (orderNumber != null) { 
				this.salesDocNumber = orderNumber;
			}
			if (salesDocsOrigin != null) { 
				this.salesDocsOrigin = ordersOrigin;
			}	
			if (singleOrderHeader != null  &&
				! singleOrderHeader.getTechKey().equals(techKey)) {
				// only clear guids if we are reading another document. 
				selectedItemGuids.clear();
			}
			singleOrderHeader = null;
			itemMap.clear();
			itemList.clear();
			((OrderStatusData)this).setOrderHeader(createHeader());
			getOrderHeaderData().setMemberShip(loyMemShip);
			campaignDescriptions.clear();
			campaignTypeDescriptions.clear();
		}

		try {
			// read AttributeSet from Backend
			getBackendService(type).readOrderStatus(this);
		}
		catch (BackendException ex) {
			BusinessObjectHelper.splitException(ex);
		}
		finally {
			log.exiting();
		}
	}




	/**
	 * Read the OrderHeaders for the given business partner sold_to from backend. <br>
	 *
	 * @param soldToKey  contains the soldTo GUID which identify a business partner in the backend
	 * @param shop  GUID which identify a shop in the backend
	 * @param filter specifice the select options for the list
	 */
	public void readOrderHeaders(TechKey soldToKey, Shop shop, DocumentListFilter filter)
			throws CommunicationException {
				
		final String METHOD = "readOrderHeaders()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("soldToKey="+soldToKey+", shop="+shop);   
		this.soldToKey = soldToKey;
		this.shop   = shop;
		this.filter = filter;
		orderHeader.clear();
		this.orderCount = 0;

		try {
			// read OrderList from Backend
			getBackendService(filter.getType()).readOrderHeaders(this);
		}
		catch (BackendException ex) {
			BusinessObjectHelper.splitException(ex);
		}
		finally {
			log.exiting();
		}
	}
	




	/** 
	 * Copy the data from one document to an other. <br> 
	 * <i>This methods copies only data for one document not for the list.
	 * </i> <br>
	 * 
	 * This method is needed, if we want create an customerOrderStatus from an
	 * orderStatus object.
	 * 
	 * @param salesDocumentStatus documentStatus to use to create the object.
	 *  
	 */
	public void copyDataFromDocument(OrderStatus salesDocumentStatus) {
	    
		techKey = salesDocumentStatus.techKey;
		copyMessages(salesDocumentStatus);	
	     
		shop = salesDocumentStatus.shop;
		user = salesDocumentStatus.user;
	        
		singleOrderHeader = (HeaderSalesDocument)salesDocumentStatus.singleOrderHeader.clone();
		salesDocNumber = salesDocumentStatus.salesDocNumber;
		salesDocsOrigin = salesDocumentStatus.salesDocsOrigin;
		itemMap = (ItemMap)salesDocumentStatus.itemMap.clone();
		itemList = (ItemList)salesDocumentStatus.itemList.clone();
		text = salesDocumentStatus.text;
		state = salesDocumentStatus.state;
		contractDataExist = salesDocumentStatus.contractDataExist;
		shipToList = salesDocumentStatus.shipToList;
	
		documentExist = salesDocumentStatus.documentExist;
		
		try {
			order = (Order)salesDocumentStatus.order.clone();
		}
		catch (CloneNotSupportedException e) {
			if (log.isDebugEnabled()) {
				log.debug("Error ", e);
			}
		}	
	}
	
	/**
			 * Initializes productconfiguration in the IPC, retrieves IPC-parameters from the backend
			 * and stores them in this object.
			 *
			 * @param itemGuid the id of the item for which the configuartion
			 *        should be read
			 */
			public void getItemConfig(String orderType, TechKey itemGuid) throws CommunicationException {
				// write some debugging info
				String type = orderType;

				if (log.isDebugEnabled()) {
					log.debug("getItemConfig(): itemGuid = " + itemGuid);
				}
				try {
					getBackendService(type).getItemConfigFromBackend(this, itemGuid);
				}
				catch (BackendException ex) {
					BusinessObjectHelper.splitException(ex);
				}
			}

	
	/**
	  * Returns the <code>List</code> of ship tos
	  *
	  * @return List with soldTos
	  */
	 public List getShipToList() {
	 	return shipToList;
	 }

	/**
	 * @param documentKey
	 * @param documentId
	 * @param documentsOrigin
	 * @param documentType
	 * @param object
	 */
	public void readOrderStatus(TechKey documentKey, String documentId, String documentsOrigin, String documentType) throws CommunicationException {
		readOrderStatus( documentKey,  documentId,  documentsOrigin,  documentType, null);
	}
}
