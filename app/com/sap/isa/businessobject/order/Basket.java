/*****************************************************************************
    Class:        Basket
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Thomas Smits
    Created:      19.3.2001
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/10/29 $
*****************************************************************************/

package com.sap.isa.businessobject.order;

import com.sap.isa.backend.BackendTypeConstants;
import com.sap.isa.backend.boi.isacore.SalesDocumentBackend;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.order.BasketBackend;
import com.sap.isa.backend.boi.isacore.order.BasketData;
import com.sap.isa.backend.boi.isacore.order.CopyMode;
import com.sap.isa.businessobject.BusinessObjectHelper;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;

/**
 * Shopping basket for internet sales. The well known methaphor of a shopping
 * basket ist implemented using this class. The basket consist of several items,
 * and header information.
 *
 * @author Thomas Smits
 * @version 1.0
 */
public class Basket extends SalesDocument
                    implements BasketData {

 	private boolean isAuctionBasket = false; // for auction scenario
 	
    /**
     * Creates a new basket.
     */
    public Basket() {
        // basket is always the target
        state = DocumentState.TARGET_DOCUMENT;
    }

	/**
	 * Sets the basket type as auction basket
	 * @param bool
	 */
	public void setAuctionBasket(boolean bool){
		isAuctionBasket = bool;
	}
	
	/**
	 * Gets whether the basket type is auction basket or not
	 * @return
	 */
	public boolean getAuctionBasket(){
		return isAuctionBasket;
	}
	
    /**
     * Initializes a basket from an auction predecessor document.
     *
     * @param predecessorKey <code>TechKey</code> of a predecessor document
     * @param processType process type that shall be used for the resulting
     * order. If initial (""), the process type specified in the shop will be
     * used.
     * @param soltoKey <code>TechKey</code> of the sold to for whom the order
     * shall be created
     * @param shop the shop the action takes place in
     */
    public void initWithAuctionRef(
            TechKey predecessorKey,
            String processType,
            TechKey solToKey,
            Shop shop
            ) throws CommunicationException {
        clearData();
		
		final String METHOD = "initWithAuctionRef()";
		log.entering(METHOD);
        // write some debugging info
        if (log.isDebugEnabled()) {
            log.debug("initWithAuctionRef(): predecessorKey = " + predecessorKey
                    + ", processType = " + processType
                    + ", solToKey = " + solToKey
                    + ", shop = " + shop);
        }

		try {
		
	        if (!alreadyInitialized) {
	            try {
	                // create Basket in Backend
	                ((BasketBackend)getBackendService()).createWithReferenceInBackend(
	                        predecessorKey,
	                        CopyMode.TRANSFER_AND_UPDATE,
	                        processType,
	                        solToKey,
	                        (ShopData)shop,
	                        (BasketData)this
	                        );
	                alreadyInitialized = true;
	                isDirty = true;
	                header.setDirty(true);
	            }
	            catch (BackendException ex) {
	                // debug:begin
	                if (log.isDebugEnabled()) log.debug(ex);
	                // debug:end
	                BusinessObjectHelper.splitException(ex);
	            }
	        }
		} finally {
		
        	log.exiting();
		}
    }


    /**
     * Method retrieving the backend object for the object.
     *
     * @return Backend object to be used
     */
    protected SalesDocumentBackend getBackendService()
            throws BackendException {

        synchronized (this) {
            if (backendService == null) {
            	if(isAuctionBasket){
					backendService = (BasketBackend)
							bem.createBackendBusinessObject(BackendTypeConstants.BO_TYPE_AUCTION_BASKET, null);

            	}else{
					backendService = (BasketBackend)
											bem.createBackendBusinessObject(BackendTypeConstants.BO_TYPE_BASKET, null);
            	}
            }
        }

        return backendService;
    }

    /**
     * Callback method for the <code>BusinessObjectManager</code> to tell
     * the object that life is over and that it has to release
     * all ressources.
     */
    public void destroy() {
        super.destroy();
        backendService = null;
        alreadyInitialized = false;
    }
    
	/**
	 * Releases the lock of the basket in the backend.
	 *
	 * @param basket business object to be unlocked
	 */
	public void dequeue() throws CommunicationException {

		final String METHOD = "dequeue()";
		log.entering(METHOD);
		try {
			((BasketBackend) getBackendService()).dequeueInBackend(this);
		}
		catch (BackendException ex) {
			BusinessObjectHelper.splitException(ex);
		}
		finally {
			log.exiting();
		}
	}    
	
	/**
	 * Set global data in the backend
	 *
	 * @return <true> if check was successful; otherwise <code>false</code>
	 */
	public void setGData(ShopData shop, WebCatInfo salesDocWebCat) throws CommunicationException {
		final String METHOD = "setGData()";
		log.entering(METHOD);
		// write some debugging info
		if (log.isDebugEnabled()) {
			log.debug("setGData(): shop = " + shop + ", salesDocWebCat = " + salesDocWebCat);
		}

		if (shop.isInternalCatalogAvailable() && salesDocWebCat == null) {
			throw( new IllegalArgumentException("Parameter salesDocWebCat can not be null!"));
		}
		this.salesDocWebCat = salesDocWebCat;

		try {
			((BasketBackend) getBackendService()).setGData(this, shop);
			isDirty = true;
			header.setDirty(true);
		}
		catch (BackendException ex) {
			BusinessObjectHelper.splitException(ex);
		}
		finally {
			log.exiting();
		}
	}
	
	/**
	 * Saves the order in the backend.<br>
	 * 
	 * <b>Note: <b> Since release 4.0 the method don't commit automaticly
	 * 
	 */
	public void save() throws CommunicationException {

		// write some debugging info
		final String METHOD = "save()";
		log.entering(METHOD);

		try {
			((BasketBackend) getBackendService()).saveInBackend(this, false);
			isDirty = true;
			header.setDirty(true);
		}
		catch (BackendException ex) {
			BusinessObjectHelper.splitException(ex);
		}
		finally {
			log.exiting();
		}
	}


	/**
	 * Saves the order in the backend.
	 */
	public void saveAndCommit() throws CommunicationException {

		final String METHOD = "saveAndCommit()";
		log.entering(METHOD);

		try {
			((BasketBackend) getBackendService()).saveInBackend(this, true);
			isDirty = true;
			header.setDirty(true);
		}
		catch (BackendException ex) {
			BusinessObjectHelper.splitException(ex);
		}
		finally {
			log.exiting();
		}
	}
	
	/**
	 * Reads the basket data from the underlying storage for the special
	 * case of a change of the basket.
	 */
	public void readForUpdate()
						throws CommunicationException {
		final String METHOD = "readForUpdate()";
		log.entering(METHOD);
       
		try {
        	
			// read and lock from Backend
			if (header.isDirty()) {
				   ((BasketBackend) getBackendService()).readHeaderFromBackend(this, true);
				header.setDirty(false);
			}
			readAllItemsForUpdate();
		}
		catch (BackendException ex) {
			BusinessObjectHelper.splitException(ex);
		}
		finally {
			log.exiting();
		}
	}	
}
