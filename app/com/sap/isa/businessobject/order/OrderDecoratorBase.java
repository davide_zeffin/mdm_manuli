/*****************************************************************************
    Class:        OrderDecoratorBase
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Thomas Smits
    Created:      11.4.2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/06/26 $
*****************************************************************************/

package com.sap.isa.businessobject.order;

import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.businessobject.BusinessObjectDecoratorBase;
import com.sap.isa.businessobject.SalesDocument;

/**
 * Base class for all decorator classes working with the <code>Order</code>
 * object.
 *
 * @author Thomas Smits
 * @version 1.0
 */
public abstract class OrderDecoratorBase extends BusinessObjectDecoratorBase {
    
    protected Order order;

    /**
     * Creates a new decorator wrapped around an order object.
     */
    public OrderDecoratorBase(Order order) {
        this.order = order;
    }

    /**
     * Only for backward compatibility with older classes. Will be removed
     * soon.
     * @deprecated Only for backward compatibility with older classes.
     */
    public OrderDecoratorBase() {
    }
    
    /**
     * returns decorated object
     * 
     * @return Saledocument the decorated object
     */   
    public SalesDocument getSalesDocument() {
        return order;
    }

	/**
	 * @see com.sap.isa.businessobject.BusinessObjectDecoratorBase#getBob()
	 */
	protected BusinessObjectBase getBob() {
		return order;
	}

}
