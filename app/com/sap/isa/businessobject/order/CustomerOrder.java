/*****************************************************************************
    Class:        CustomerOrder
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:
    Created:      October 2002
    Version:      1.0

    $Revision: #12 $
    $Date: 2002/08/05 $
*****************************************************************************/

package com.sap.isa.businessobject.order;

import com.sap.isa.backend.BackendTypeConstants;
import com.sap.isa.backend.boi.isacore.SalesDocumentBackend;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.order.OrderBackend;
import com.sap.isa.businessobject.BusinessObjectHelper;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.eai.BackendException;


/**
 * Class representing the customer order in the partner order management.
 * The customer order represents the order which is created in the
 * channnel commerce hub and will now process in partner order management as
 * customer.
 *
 * <i>Note:</i> The own class allows to use an own backend for the customer order.
 *
 * @author SAP
 * @version 1.0
 *
 */
public class CustomerOrder extends Order {

    /**
     * Creates a new order object without any parameters.
     */
    public CustomerOrder() {
        isCustomerDocument = true;
    }


    /**
     * get the BackendService, if necessary
     *
         * @return Backend service
     */
    protected SalesDocumentBackend getBackendService()
            throws BackendException{

        synchronized (this) {
            if (backendService == null) {
                backendService =
                    (OrderBackend) bem.createBackendBusinessObject(BackendTypeConstants.BO_TYPE_ORDER, null);
            }
        }
        return backendService;
    }


    /**
     * Set global data in the backend
     *
     * @return <true> if check was successful; otherwise <code>false</code>
     */
    public void setGData(ShopData shop, WebCatInfo salesDocWebCat) throws CommunicationException {
		final String METHOD = "setGData()";
		log.entering(METHOD);
        // write some debugging info
        if (log.isDebugEnabled()) {
            log.debug("setGData(): shop = " + shop + ", salesDocWebCat = " + salesDocWebCat);
        }

        this.salesDocWebCat = salesDocWebCat;

        try {
            ((OrderBackend) getBackendService()).setGData(this, shop);
            isDirty = true;
            header.setDirty(true);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally {
        	log.exiting();
        }
    }


}
