/*****************************************************************************
    Class:        CustomerOrderStatus
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author
    Created:      October 2002

    $Revision: #9 $
    $Date: 2001/07/25 $
*****************************************************************************/

package com.sap.isa.businessobject.order;

import com.sap.isa.backend.BackendTypeConstants;
import com.sap.isa.backend.boi.isacore.order.OrderStatusBackend;
import com.sap.isa.core.eai.BackendException;


/**
 * The customer order status allows the selection and display of existing
 * customer orders. The customer order will be create in Channel Commerce Hub
 * and then process in the partner order management.
 *
 * @com.sap.isa.businessobject.order.CustomerOrder
 *
 * @author SAP
 * @version 1.0
 *
 */
public class CustomerOrderStatus extends OrderStatus {


    private OrderStatusBackend backendService;


    /**
     * Constructor for CustomerOrderStatus.
     * @param order
     */
    public CustomerOrderStatus(CustomerOrder order) {
        super(order);
        setMultiPartnerScenario(true);
    }


    /**
     * Constructor for CustomerOrderStatus.
     */
    public CustomerOrderStatus() {
        this.order = new CustomerOrder();
        setMultiPartnerScenario(true);
    }


    /**
     * @see com.sap.isa.businessobject.SalesDocumentStatus#getBackendService(String)
     */
    protected OrderStatusBackend getBackendService(String type)
            throws BackendException {

        if(backendService == null) {
            backendService = (OrderStatusBackend)
                             bem.createBackendBusinessObject(BackendTypeConstants.BO_TYPE_CUSTOMERORDERSTATUS);
        }

        return backendService;
    }

}
