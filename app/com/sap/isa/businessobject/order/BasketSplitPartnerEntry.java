/*****************************************************************************
  Class:        BasketSplitPartnerEntry
  Copyright (c) 2002, SAP AG, Germany, All rights reserved.
  Author:
  Created:      14.11.2002
  Version:      1.0

  $Revision: #2 $
  $Date: 2002/11/26 $
*****************************************************************************/

package com.sap.isa.businessobject.order;

import com.sap.isa.businessobject.item.ItemList;
import com.sap.isa.core.TechKey;

/**
 * class to store all informations of a soldFrom, that are required
 * in the basket split or oci order foward process of the channel
 * commerce hub scenario
 *
 * @author SAP AG
 * @version 1.0
 */

public class BasketSplitPartnerEntry {

    protected boolean isBasketForwarded;
    protected String createdOrderNumber="";
    protected TechKey createdOrderGUID;
    protected TechKey soldFromTechKey;
    protected String  soldFromId;
    protected String soldFromName;
    protected String soldFromAddress;
    protected TechKey basketGuid;
    protected ItemList items;

    public BasketSplitPartnerEntry(TechKey basketGuid, TechKey soldFromTechKey, String  soldFromId,
                                   String soldFromName, String soldFromAddress) {
        super();
        this.basketGuid = basketGuid;
        this.soldFromTechKey = soldFromTechKey;
        this.soldFromId = soldFromId;
        this.soldFromName = soldFromName;
        this.soldFromAddress = soldFromAddress;
        items = new ItemList();
    }

    public TechKey getBasketGuid() {
        return basketGuid;
    }

    public TechKey getSoldFromTechKey() {
        return soldFromTechKey;
    }

    public String getCreatedOrderNumber() {
            return createdOrderNumber;
    }

    public void setCreatedOrderNumber(String createdOrderNumber) {
        this.createdOrderNumber = createdOrderNumber;
    }

    public TechKey getCreatedOrderGUID() {
            return createdOrderGUID;
    }

    public void setCreatedOrderGUID(TechKey createdOrderGUID) {
        this.createdOrderGUID = createdOrderGUID;
    }

    public String getSoldFromId() {
        return soldFromId;
    }

    public String getSoldFromName() {
        return soldFromName;
    }

    public String getSoldFromAddress() {
        return soldFromAddress;
    }

    public boolean isBasketForwarded() {
        return isBasketForwarded;
    }

    public void setBasketForwarded(boolean isBasketForwarded) {
        this.isBasketForwarded = isBasketForwarded;
    }

    public ItemList getItemList() {
        return items;
    }

    public void setItemList(ItemList items) {
        this.items = items;
    }
}