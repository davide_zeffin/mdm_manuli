/*****************************************************************************
    Class:        CampaignListEntry
    Copyright (c) 2004, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      20.02.2004
    Version:      1.0

    $Revision: #1 $
    $Date: 2004/02/20 $
*****************************************************************************/

package com.sap.isa.businessobject.order;

import com.sap.isa.backend.boi.isacore.order.CampaignListEntryData;
import com.sap.isa.core.TechKey;

/**
 * Class to define a camapign that is related to an item or the header
 *
 * @author SAP AG
 * @version 1.0
 */
public class CampaignListEntry
implements CampaignListEntryData, Cloneable {

    protected String campaignId;
    protected TechKey campaignGUID;
    protected String campaignTypeId;
    protected boolean campaignValid=true;
	protected boolean manuallyEntered=true;
    
    /**
     * Dummy campaign that is used when items are copied, to indicate, that the header
     * campaign should not be copied into that item.
     */
    public static CampaignListEntryData DUMMY_CAMPAIGN = new CampaignListEntry("", TechKey.EMPTY_KEY, "", false, true);

    /**
     * Creates a new <code>CampaignListEntry</code> object.
     */
    public CampaignListEntry() {
    }   

    /**
     * Creates a new <code>CampaignListEntry</code> object.
     *
     * @param String campaignId id of the campaign
     * @param TechKey campaignGUID techkey of the campaign
     * @param String campaignTypeId id of the campaign type
     * @param boolean campaignValid flag that indicates, if the campaign is valid#
     * @param boolean manuallyEntered flag that indicates, if the campaign was manually entered
     */
    public CampaignListEntry(String campaignId, TechKey campaignGUID, String campaignTypeId, 
	                boolean campaignValid, boolean manuallyEntered) {
        this.campaignId = campaignId;
        this.campaignGUID = campaignGUID;
        this.campaignTypeId = campaignTypeId;
        this.campaignValid = campaignValid; 
        this.manuallyEntered = manuallyEntered;
    }

    /**
     * Returns the campaign id of the AlternativProduct
     *
     * @return campaignId of AlternativProduct
     */
    public String getCampaignId() {
        return campaignId;
    }

    /**
     * Set the campaign id of the CampaignListEntry
     *
     * @param campaignId campaign id of CampaignListEntry
     */
    public void setCampaignId(String CampaignId) {
        this.campaignId = CampaignId;
    }

    /**
     * Returns the campaignTypeId of the CampaignListEntry.
     * 
     * @return String
     */
    public String getCampaignTypeId() {
        return campaignTypeId;
    }

    /**
     * Returns the campaignValid flag of the CampaignListEntry.
     * 
     * @return boolean
     */
    public boolean isCampaignValid() {
        return campaignValid;
    }

    /**
     * Returns the CampaignGUID of the CampaignListEntry.
     * 
     * @return TechKey
     */
    public TechKey getCampaignGUID() {
        return campaignGUID;
    }

    /**
     * Sets the campaignTypeId of the CampaignListEntry.
     * 
     * @param campaignTypeId The campaignTypeId of the CampaignListEntry
     */
    public void setCampaignTypeId(String campaignTypeId) {
        this.campaignTypeId = campaignTypeId;
    }

    /**
     * Sets the campaignValid flag of the CampaignListEntry.
     * 
     * @param campaignValid The campaignValid of the CampaignListEntry
     */
    public void setCampaignValid(boolean campaignValid) {
        this.campaignValid = campaignValid;
    }

    /**
     * Sets the CampaignGUID of the CampaignListEntry.
     * 
     * @param CampaignGUID The CampaignGUID of the CampaignListEntry
     */
    public void setCampaignGUID(TechKey CampaignGUID) {
        this.campaignGUID = CampaignGUID;
    }
    
    /**
     * Performs a deep copy of this object.
     * Because of the fact that
     * all fields of this object consist of immutable objects like
     * <code>String</code> and <code>TechKey</code> or primitive types
     * the shallow copy is identical with a deep copy. 
     *
     * @return shallow (deep-like) copy of this object
     */
    public Object clone() {
        CampaignListEntry myClone = new CampaignListEntry(campaignId, campaignGUID, campaignTypeId, campaignValid, manuallyEntered);
        return myClone;

    }
	/**
     * Gets the manuallyEntered flag of the CampaignListEntry.
     * 
     * @return manuallyEntered The manuallyEntered of the CampaignListEntry
	 */
	public boolean isManuallyEntered() {
		return manuallyEntered;
	}

	/**
     * Sets the manuallyEntered flag of the CampaignListEntry.
     * 
     * @param manuallyEntered The manuallyEntered of the CampaignListEntry
	 */
	public void setManuallyEntered(boolean manuallyEntered) {
		this.manuallyEntered = manuallyEntered;
	}

}
