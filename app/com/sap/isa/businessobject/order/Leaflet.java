/*****************************************************************************
    Class:        Leaflet
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Sabine Heider
    Created:      02.04.2001
    Version:      1.0

    $Revision: #2 $
    $Date: 2001/07/04 $
*****************************************************************************/

package com.sap.isa.businessobject.order;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.StringTokenizer;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import com.sap.isa.businessobject.Product;
import com.sap.isa.businessobject.ProductList;
import com.sap.isa.businessobject.ProductMap;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.logging.IsaLocation;

/**
 * Representation of the leaflet in internet sales.
 * <p>
 * The leaflet can be thought of as a memo where the user can add products
 * when browsing through the catalog. In contrast to the basket, the leaflet
 * keeps items that the user not necessarily wants to order immediately.
 * It is rather a reminder for a possible later order or just a bookmark for
 * products the user is interested in.
 * </p><p>
 * Products cannot be ordered directly from the leaflet. To order a product
 * kept in the leaflet, the user has to place the item into his shopping
 * basket.
 * </p><p>
 * The leaflet shall keep its contents even beyond the user's current session.
 * Therefore, persistent cookies are used to store the items in the leaflet.
 * If a user has disabled cookies, this feature is not availabe. Nevertheless,
 * the leaflet functionality is availabe during the current session.
 * </p>
 *
 * @author Sabine Heider
 * @version 1.0
 */
public class Leaflet {

    /**
     * Maximum size of the cookie string in byte.
     * <p>
     * The total size of the cookie (as it appears in the HTTP header)
     * is limited to 4K. To be on the safe side, we limit the cookie's value
     * (i.e. the string plus the hashCode) to 3.5K.
     */
    private static final int MAX_COOKIE_VALUE_SIZE = 3584;
    private static final String COOKIE_ITEM_SEPARATOR = "&";

    /**
     * Prefix for the name of the cookie which stores the leaflet data.
     * <p>
     * The complete name is built from the prefix and the techKey of the shop.
     * </p>
     */
    private static final String LEAFLET_COOKIE_NAME_PREFIX = "leaflet";

    /**
     * Maximum age of the cookie in seconds.
     */
    private static final int COOKIE_MAX_AGE = 100 * 24 * 60 * 60;
    private static final int COOKIE_DELETE = 0;

	static final private IsaLocation loc = IsaLocation.getInstance(Leaflet.class.getName());
	private WebCatInfo catalog;
    private ProductMap productMap = new ProductMap();
    private Set setOfNotEnhancedProducts = new HashSet();

    /**
     * Name of the cookie which stores the leaflet data.
     */
    private String cookieName = LEAFLET_COOKIE_NAME_PREFIX;

    /**
     * Value of the cookie storing the leaflet items (without hashcode).
     * <p>
     * The cookie value is build from the productTechKeys of the leaflet items.
     * It is a concatenation of the String representations of the techKeys
     * separated by '&' characters. A checksum (hashCode) is added after the
     * last techKey (again separated by '&') to protect the String against
     * manipulation.
     * </p><p>
     */
    private String cookieString = "";


    /**
     * Initializes the cookie according to the given shop and catalog.
     *
     * @param shop Reference to the shop business object
     * @param catalog Reference to the catalog business object
     */
    public void init(Shop shop, WebCatInfo catalog) {
        setCatalog(catalog);
        buildCookieName(shop);
    }


    /**
     * Specifies the catalog business object.
     * <p>
     * The catalog needs a reference to the catalog business object, because
     * it requests the catalog for additional information on the leaflet
     * items.
     * </p>
     *
     * @param catalog Reference to the catalog business object
     */
    public void setCatalog(WebCatInfo catalog) {
        this.catalog = catalog;
    }


    /**
     * Builds the name of the cookie which stores the leaflet data. The name
     * is a concatenation of a constant prefix and the technical key of the
     * shop.
     *
     * @param shop Reference to the shop business object
     */
    public void buildCookieName(Shop shop) {
        if (shop != null && shop.getTechKey() != null) {
            cookieName = LEAFLET_COOKIE_NAME_PREFIX + shop.getTechKey().getIdAsString();
        }
    }


    /**
     * Returns the name of the cookie which stores the leaflet data.
     *
     */
    public String getCookieName() {
        return cookieName;
    }


    /**
     * Returns the number of items currently stored in the leaflet.
     *
     * @return Number of items in the leaflet
     */
    public int getNumItems() {
       return productMap.size();
    }


    /**
     * Returns a map of the products currently stored inside the leaflet.
     *
     * @return Map with products in the leaflet
     */
    public ProductMap getItems() {
       return productMap;
    }


    /**
     * Returns <code>true</code> if the leaflet contains a product with the
     * specified technical key.
     *
     * @param techKey Technical key of the product
     * @return <code>true</code> if the product is contained in the leaflet.
     */
    public boolean containsItem(TechKey techKey) {
        return productMap.containsKey(techKey);
    }


    /**
     * Returns <code>true</code> if the leaflet cookie contains the
     * specified technical key.
     *
     * @param techKey Technical key of the product
     * @return <code>true</code> if the technical key is contained in the
     *         leaflet cookie, false otherwise or if the technical key is
     *         <code>null</code>.
     */
    public boolean containsItemInCookie(TechKey techKey) {
        // Note: TechKey->getIdAsString() never returns null!
        return (techKey != null)
                && (cookieString != null)
                && (cookieString.indexOf(techKey.getIdAsString()) != -1);
    }


    /**
     * Returns the product with the specified technical key.
     * Returns <code>null</code> if the leaflet contains no product with this key.
     *
     * @param techKey Technical key of the product
     * @return the product in the leaflet matching the specified technical key
     *
     */
    public Product getItem(TechKey techKey) {
        return productMap.get(techKey);
    }


    /**
     * Returns a map of the products currently stored inside the leaflet.
     * In contrast to the <code>getItems()<code> method, this method ensures
     * that the data is enhanced with catalog information.
     *
     * @return Map with leaflet items
     */
    public ProductMap readItems() {
        enhanceProducts();
        return productMap;
    }


    /**
     * Adds a <code>Product</code> to the leaflet.
     * <p>
     * A product identified by a unique technical key can only be stored once
     * in the leaflet. Any further attempts to store the product again are
     * ignored. The method <code>containsItem(TechKey techKey)</code> can be
     * used to check if the product is already contained in the leaflet.
     * A <code>null</code> reference passed to this function will also be
     * ignored.
     * Before the product is stored, its techKey is added to the cookie. If this
     * fails for any reason (e.g. because the cookie's maximum size would be
     * exceeded, the request is ignored as well.
     * </p><p>
     * The technical key of the item is added to the list of items that haven't
     * been enhanced yet.
     * </p>
     *
     * @param item <code>Product</code> to be added to the leaflet.
     * @see #containsItem(TechKey techKey)
     */
    public void addItem(Product product) {
		final String METHOD = "addItem()";
		loc.entering(METHOD);
        if (product != null) {
            TechKey techKey = product.getTechKey();

            // add item to the cookie
            boolean addedToCookie = addToCookie(techKey);

            if (addedToCookie) {
                // add item to the leaflet
                productMap.put(techKey, product);

                // add technical key to the list of non-enhanced items
                setOfNotEnhancedProducts.add(techKey);
            }
        }
        loc.exiting();
    }


    /**
     * Adds a <code>ProductList</code> to the leaflet.
     * <p>
     * A product identified by a unique technical key can only be stored once
     * in the leaflet. Any further attempts to store the product again are
     * ignored. The method <code>containsItem(TechKey techKey)</code> can be
     * used to check if the product is already contained in the leaflet.
     * A <code>null</code> reference passed to this function will also be
     * ignored.
     * Before the product is stored, its techKey is added to the cookie. If this
     * fails for any reason (e.g. because the cookie's maximum size would be
     * exceeded, the request is ignored as well.
     * </p><p>
     * The technical key of the item is added to the list of items that haven't
     * been enhanced yet.
     * </p>
     *
     * @param itemList List of products to be added to the leaflet.
     * @see #containsItem(TechKey techKey)
     */
    public void addItem(ProductList productList) {
		final String METHOD = "addItem()";
		loc.entering(METHOD);
        if (productList != null) {
            Iterator it = productList.iterator();

            while (it.hasNext()) {
                addItem((Product) it.next());
            }
        }
        loc.exiting();
    }


    /**
     * Removes a product from the leaflet.
     *
     * @param techKey Technical key of the product to be removed
     */
    public void removeItem(TechKey techKey) {
		final String METHOD = "removeItem()";
		loc.entering(METHOD);
        // remove from item list
        productMap.remove(techKey);

        // remove from not-enhanced-item list
        setOfNotEnhancedProducts.remove(techKey);

        // remove from cookie
        removeFromCookie(techKey);
        loc.exiting();
    }


    /**
     * Removes all products from the leaflet.
     *
     */
    public void removeAllItems() {
		final String METHOD = "removeAllItems()";
		loc.entering(METHOD);
        // empty item list
        productMap.clear();

        // empty not-enhanced-item list
        setOfNotEnhancedProducts.clear();

        // empty cookie
        emptyCookie();
        loc.exiting();
    }

    /**
     * Reads the additional information on products, that haven't been enhanced
     * yet, from the catalog.
     * This is done for all items listed in the <code>setOfNotEnhancedProducts</code>
     * set. After enhancing, the items are removed from the set.
     *
     */
    private void enhanceProducts() {

        // no enhancement without catalog
        if (catalog == null) {
            // TODO: some error handling
        }
        else {
            productMap.enhance(catalog, (TechKey[]) (setOfNotEnhancedProducts.toArray(new TechKey[0])) );
            setOfNotEnhancedProducts.clear();
        }
    }


    /**
     * Returns the value of the cookie's internal representation.
     * <p>
     * The cookie value is build from the techKeys of the products.
     * It is a concatenation of the String representations of the techKeys
     * separated by '&' characters. A checksum (hashCode) is added after the
     * last techKey (again separated by '&') to protect the String against
     * manipulation.
     * </p>
     *
     * @return Value of the cookie storing the products in the leaflet.
     */
    public String getCookieValue() {

        String cookieValue = null;

        if (cookieString != null) {
            String checkString = createCheckSum();
            cookieValue = cookieString + COOKIE_ITEM_SEPARATOR + checkString;
        }

        return cookieValue;
    }


    /**
     * Adds the cookie storing the leaflet data to the HTTP response. If the
     * cookie already exists, it is updated with the current items. If the
     * leaflet doesn't contain any items, the cookie is deleted.
     *
     * @param response HTTP response.
     */
    public void addCookieToResponse(HttpServletResponse response) {
		final String METHOD = "addCookieToResponse()";
		loc.entering(METHOD);
        String cookieValue = getCookieValue();
        Cookie cookie = new Cookie(cookieName, cookieValue);

        if (cookieValue == null) {
            // delete cookie
            cookie.setMaxAge(COOKIE_DELETE);
        }
        else {
            cookie.setMaxAge(COOKIE_MAX_AGE);
        }

        cookie.setPath("/");

        response.addCookie(cookie);
        loc.exiting();
    }


    /**
     * Fills the leaflet with the data provided.
     * Any data that might already be stored in the leaflet is discarded.
     * <p>
     * If the hashCode appended does not match the cookie string, the data
     * is considered manipulated or corrupt, and no data is filled into the
     * leaflet at all.
     *
     * @param cookieValue Value of the cookie as it comes with the HTTP request.
     */
    public void fill(String cookieValue) {
		final String METHOD = "fill()";
		loc.entering(METHOD);
		if (loc.isDebugEnabled())
			loc.debug("fill(): cookieValue = "+ cookieValue);
        // discard all previous entries
        removeAllItems();

        if (cookieValue != null && cookieValue.length() > 0) {
            // position of the last separator (separating techKeys and hash code)
            int splitPos = cookieValue.lastIndexOf(COOKIE_ITEM_SEPARATOR);
            // position of the hashcode
            int startHash = splitPos + COOKIE_ITEM_SEPARATOR.length();

            // cookie value must contain separator and must neither start
            // nor end with it
            if (splitPos > 0  && startHash < cookieValue.length()) {
                String hashString = cookieValue.substring(startHash);
                cookieString = cookieValue.substring(0, splitPos);

                // verify checksum
                if ( !verifyCheckSum(hashString) ) {
                    cookieString = null;
                }
                else {
                    // build up leaflet
                    StringTokenizer tokens =
                            new StringTokenizer(cookieString, COOKIE_ITEM_SEPARATOR);

                    while (tokens.hasMoreElements()) {
                        // create product with given techKey
                        TechKey techKey = new TechKey(tokens.nextToken());
                        Product item = new Product(techKey);

                        // add to leaflet
                        addItem(item);
                    }
                }
            }
        }
        loc.exiting();
    }


    /**
     * Adds the product to the cookie's internal String representation.
     * <p>
     * The cookie value is build from the techKeys of the products.
     * It is a concatenation of the String representations of the techKeys
     * separated by '&' characters. A checksum (hashCode) is added after the
     * last techKey (again separated by '&') to protect the String against
     * manipulation.
     * </p><p>
     * The size of a cookie is limited to 4K. Before adding a new item, the
     * <code>Leaflet</code> therefore checks if the size of the new cookie value
     * would exceed the limit provided in MAX_COOKIE_VALUE_SIZE. In that case,
     * the item is not added to the leaflet, and the method returns
     * <code>false</code>. Similarly, if the techKey of the item is
     * <code>null</code>, the item isn't added and the method returns
     * <code>false</code>.
     * </p><p>
     * Since the leaflet object has no access to the HTTP response, the real
     * cookie is not updated here. This can only be done by an action class.
     * </p>
     *
     * @param techKey Technical key of the leaflet item to be added
     *                to the cookie.
     * @return <code>true</code> if the item has been added to the cookie
     *         string successfully or if the item has already been in it,
     *         <code>false</code> otherwise.
     */
    private boolean addToCookie(TechKey techKey) {
		final String METHOD = "addToCookie()";
		loc.entering(METHOD);
        if (loc.isDebugEnabled()) {
            loc.debug("addToCookie(): before - " + cookieString);
        }

        boolean itemInCookie = false;
        String newItem = techKey.getIdAsString();

        // check if item isn't already contained in leaflet
        if ( !containsItem(techKey) ) {

            // check if item isn't already contained in cookie
            if ( !containsItemInCookie(techKey) ) {
                // check if new cookie value would exceed max. size
                boolean sizeCheckOK = checkCookieSize(newItem, COOKIE_ITEM_SEPARATOR);

                if (sizeCheckOK) {
                    // add item to the cookie string
                    if (cookieString == null) {
                        cookieString = newItem;
                    }
                    else {
                        cookieString = cookieString + COOKIE_ITEM_SEPARATOR + newItem;
                    }
                    itemInCookie = true;
                }
            }
            else {
                // item already contained in cookie
                itemInCookie = true;
            }
        }

        if (loc.isDebugEnabled()) {
            loc.debug("addToCookie(): after - " + cookieString);
        }
		loc.exiting();
        return itemInCookie;
    }


    /**
     * Checks if the size of the new cookie value would exceed the limit
     * provided in MAX_COOKIE_VALUE_SIZE.
     * The cookie data is written to the HTTP request as a byte stream, so
     * the size is equal to the number of characters.
     *
     * @param newItem String to be added to the cookie to represent the new
     *                item.
     * @param sep String separating the entries in the cookie.
     * @return <code>false</code> if the new cookie value would exceed the
     *         allowed size, <code>true</code> otherwise.
     */
    private boolean checkCookieSize(String newItem, String sep) {

        int currentLength;
        if (cookieString == null) {
            currentLength = 0;
        }
        else {
            currentLength = cookieString.length();
        }

        int newLength = currentLength +
                        newItem.length() +
                        // max. length of hashCode
                        Integer.toString(Integer.MAX_VALUE).length() +
                        // separators before new item and hashCode
                        2 * sep.length();

        if(newLength > MAX_COOKIE_VALUE_SIZE) {
            return false;
        }
        else {
            return true;
        }
    }


    /**
     * Removes the item with the given technical key from the cookies internal
     * String representation.
     * <p>
     * Since the leaflet object has no access to the HTTP response, the real
     * cookie is not updated here. This can only be done by an action class.
     * </p>
     *
     */
    private void removeFromCookie(TechKey techKey) {

		final String METHOD = "removeFromCookie()";
		loc.entering(METHOD);
        StringBuffer work = new StringBuffer(cookieString);
        String techKeyAsString = techKey.getIdAsString();
        int deleteFrom = -1;
        int deleteTo;
        int nextSep;

        if (loc.isDebugEnabled()) {
            loc.debug("removeFromCookie(): before - " + cookieString);
        }

        // find techKey in cookie
        int pos = cookieString.indexOf(techKeyAsString);

        // if techKey found
        if (pos > -1) {
            // look for separator before techKey
            deleteFrom = cookieString.lastIndexOf(COOKIE_ITEM_SEPARATOR, pos);
            if (deleteFrom > -1) {
                deleteTo = pos + techKeyAsString.length();

                // delete substring
                work.delete(deleteFrom, deleteTo);
                cookieString = work.toString();
            }
            else {
                // look for separator behind techKey
                nextSep = cookieString.indexOf(COOKIE_ITEM_SEPARATOR, pos);
                if (nextSep > -1) {
                    deleteFrom = pos;
                    deleteTo = nextSep + COOKIE_ITEM_SEPARATOR.length();

                    // delete substring
                    work.delete(deleteFrom, deleteTo);
                    cookieString = work.toString();
                }
                else {
                    // then it's the only entry
                    cookieString = null;
                }
            }
        }

        if (loc.isDebugEnabled()) {
            loc.debug("removeFromCookie(): after - " + cookieString);
        }
		loc.exiting();
    }


    /**
     * Sets the cookies internal String representation to null.
     * <p>
     * Since the leaflet object has no access to the HTTP response, the real
     * cookie is not deleted here. This can only be done by an action class.
     * </p>
     *
     */
    private void emptyCookie() {
        cookieString = null;
    }


    /**
     * Creates a checksum for the cookie string. This implementation simply
     * uses the string's hashCode in its string representation.
     */
    private String createCheckSum() {

        return Integer.toString(cookieString.hashCode());
    }


    /**
     * Verifies if the provided checksum matches the cookie string.
     *
     * @return <code>true</code> if the checksum is okay,
     *         <code>false</code> otherwise.
     */
    private boolean verifyCheckSum(String checkSum) {

        String compareSum = createCheckSum();
        return compareSum.equals(checkSum);
    }
}
