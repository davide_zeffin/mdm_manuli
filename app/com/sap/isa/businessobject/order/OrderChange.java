/*****************************************************************************
    Class:        OrderChange
    Copyright (c) 2001, SAP AG, Germany, All rights reserved.
    Author:
    Created:      11.4.2001
    Version:      1.0

    $Revision: #11 $
    $Date: 2002/08/05 $
*****************************************************************************/

package com.sap.isa.businessobject.order;

import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.ShipTo;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.item.ItemList;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.util.MessageList;
import com.sap.isa.core.util.table.ResultData;

/**
 * Decorator for the order to provide functionality only available in special
 * situations. This class acts as a decorator for the order and offers methods
 * that can only be applied when the order is in the state of being changed.
 *
 * @author SAP
 * @version 1.0
 */
public class OrderChange extends OrderDecoratorBase {
    private boolean alreadyInitialized = false;
    /**
     * Default constructor. Only used by subclasses.
     */
    protected OrderChange() {
    }

    /**
     * Creates a new instance for the given <code>Order</code> object.
     *
     * @param order Order to decorate
     */
    public OrderChange(Order order) {
        super(order);
    }


    /**
     * Saves the order in the backend.<br>
     * 
     */
    public void saveChanges() throws CommunicationException {
        order.saveAndCommit();
    }

    /**
     * Saves the order in the backend.
     * The returned int value indicates if the order
     * could be save correctly or not.
     * 
     * @return  = 0 if the order could be saved
     *         != 0 if the order could not be saved
     */
    public int saveOrderChanges() throws CommunicationException {
       return order.saveOrderAndCommit();	
    }
    
    /**
     * Determines if manual Product Determination is necessary for at least one
     * top level item.
     * 
     * @return boolean true if there is at least on item with a non empty
     * prodcutAliasList          false if all productAliasLists are empty
     * 
     * @deprecated please use {@link #isDeterminationRequired() instead
     */
    public boolean isAlternativeProductAvailable() {
        return order.isAlternativeProductAvailable();
    }
    
    /**
     * Determines if manual Product-, Campaign-, ... Determination is necessary 
     * for at least one top level item.
     * 
     * @return boolean true if there is at least on item that needs
     *         manual determination for products, camapigns, etc.
     */
    public boolean isDeterminationRequired(){
        return order.isDeterminationRequired();
    }

    /**
     * The Order will be saved without a commit
     */
    public void saveChangesWithoutCommit() throws CommunicationException {
        order.save();
    }



    /**
     * Releases the lock of the order in the backend.
     *
     * @param ordr business object to be stored
     */
    public void dequeueOrder() throws CommunicationException {
        order.dequeue();
    }

    /**
     * Perform a precheck of order data.
     *
     * @return <true> if check was successful; otherwise <code>false</code>
     */
    public boolean preCheck(ShopData shp) throws CommunicationException {
        // call backend for precheck here
        return order.preCheck(shp);
    }

    /**
     * Deletes the messages of the order
     *
     */
     public void clearMessages() {
       order.clearMessages();
     }
     
    /**
     * Returns the messages of the order
     *
     * @return message list of the order
     */
    public MessageList getMessageList() {
        return order.getMessageList();
    }
    
    /**
     * Returns the state of the order
     *
     */
     public boolean isvalid() {
       return order.isValid();
     }
     
    /**
     * Retrieves the header information of the order.
     */
    public void readForUpdate()
           throws CommunicationException {
        order.readForUpdate();
    }
    
	/**
	 * Retrieves the header information of the order.
	 */
	public void readForUpdate(ShopData shop)
		   throws CommunicationException {
		order.readForUpdate(shop);
	}
    
    /**
     * Retrieves the whole document from the underlying storage.
     * <b>As a side effect of this method the shiptos are also read, if
     * the soldto is set in the partner list. This
     * is not supposed to be the right behaviour and is left here only to keep
     * the clients of this class working. All clients should explicitly call
     * the <code>readShipTos()</code> method and not rely
     * on this - we are going to change this soon.</b>
     */
    public void read()
           throws CommunicationException {
        order.read();
    }
    
    /**
     * Retrieves the item information of the basket.
     * <b>As a side effect of this method the shiptos are also read. This
     * is not supposed to be the right behaviour and is left here only to keep
     * the clients of this class working. All clients should explicitly call
     * the <code>readShipTos()</code> method and not rely
     * on this - we are going to change this soon.</b>
     */
    public void readAllItems()
           throws CommunicationException {
        order.readAllItems();
    }
    
    /**
     * Retrieves the header from the backend and stores the data
     * in this object.
     */
    public void getItemConfig(TechKey itemGuid) throws CommunicationException {
         order.getItemConfig(itemGuid);
    }

    /**
      * Adds the IPC configuration data of a basket/order item in the
     * backend.
     *
     * @param itemGuid TechKey of the Item
     */
    public void addItemConfig(TechKey itemGuid) throws CommunicationException {
         order.addItemConfig(itemGuid);
    }
    /**
     * Retrieves the header information of the order.
     *
     */
    public HeaderSalesDocument getHeader() throws CommunicationException {
        return order.getHeader();
    }

    /**
     * Returns <code>ItemList</code> containing all items of the order
     *
     * @return items of order
     */
    public ItemList getItems() {
        return order.getItems();
    }

    /**
     * Retrieves a list of available shipping conditions from the
     * backend.
     *
     * @param language The language
     * @return table containing the conditions with the technical key
     *         as row key
     */
    public ResultData readShipCond(String language) throws CommunicationException {
       return order.readShipCond(language);
    }

    /**
     * Clears all fields of the order.
     */
    public void clearData() {
        order.clearData();
    }

    /**
     * Clears the header fields of the order.
     */
    public void clearHeader() {
        order.clearHeader();
    }

    /**
     * Clears the item list of the order.
     */
    public void clearItems() {
        order.clearItems();
    }


   /* Destroy the data of the document (items and header) in the backend
     * representation. After a
     * call to this method, the object is in an undefined state. You have
     * to call init() before you can use it again.<br>
     * This method is normally called before removing the BO-representation
     * of the object using the BOM.
     */
    public void destroyContent()  throws CommunicationException  {

      order.destroyContent();
    }
    /**
     * Sets the header data
     *
     * @param header Header data to be set
     */
     public void setHeader(HeaderSalesDocument header) {
        order.setHeader(header);
    }

    /**
     * Adds a <code>Item</code> to the order. This item must be uniquely
     * identified by a technical key.
     * A <code>null</code> reference passed to this function will be ignored,
     * in this case nothing will happen.
     *
     * @param item Item to be added to the order
     */
     public void addItem(ItemData item) {
        order.addItem(item);
    }

    /**
     * Updates object representation in underlying storage.
     *
     * @return <true> if update was successful; otherwise <code>false</code>
     *
     * @deprecated This method will be replaced by <code>update(BusinessPartnerManager bupama)</code>
     * @see #update(BusinessPartnerManager bupama)
     */
    public void update() throws CommunicationException {
        BusinessPartnerManager bupama = null;
        order.update(bupama);
    }

    /**
     * Updates object representation in underlying storage.
     */
    public void update(BusinessPartnerManager bupama) throws CommunicationException {
        order.update(bupama);
    }

    /**
     * Updates Status elements only of the object representation in underlying storage.
     */
    public void updateStatus() throws CommunicationException {
        order.updateStatus();
    }

    /**
     * Updates object representation in underlying storage.
     *
     * @deprecated This method will be replaced by <code>update(BusinessPartnerManager bupama, ShopData shop)</code>
     * @see #update(BusinessPartnerManager bupama, ShopData shop)
     */
     public void update(Shop shop) throws CommunicationException {

         //Dummy to be able to call the standard method
         BusinessPartnerManager bupama = null;

         order.update(bupama, shop);
    }

    /**
     * Updates object representation in underlying storage.
     */
     public void update(BusinessPartnerManager bupama, Shop shop) throws CommunicationException {
         order.update(bupama, shop);
    }

    /**
     * Deletes an Item(s) from the order.
     *
     * @param techKeys Technical keys of the items to be deleted
    */
    public void deleteItems(TechKey[] techKeys) throws CommunicationException {
            order.deleteItems(techKeys);
    }

    /**
     * Cancels an Item(s) from the order.
     *
     * @param techKeys Technical keys of the items to be canceld
     */
    public void cancelItems(TechKey[] techKeys) throws CommunicationException {
            order.cancelItems(techKeys);
    }


    /**
     * Returns the item of the document
     * specified by the given technical key.<br><br>
     * <b>Note -</b> This is a convenience method operating on the
     * <code>getItem</code> method of the internal <code>ItemList</code>.
     *
     * @param techKey the technical key of the item that should be
     *        retrieved
     * @return the item with the given techical key or <code>null</code>
     *         if no item for that key was found.
     */
    public ItemSalesDoc getItem(TechKey techKey) {
        return order.getItem(techKey);
    }

    /**
     * Returns the shipto specified by the given technical key
     *
     * @param techkey Technical key of the shipto to be retrieved
     * @return Returns the found shipto or <code>null</code> if no
     *         element was found at the position
     */
    public ShipTo getShipTo(TechKey techKey) {
       return order.getShipTo (techKey);
    }
    /**
     * Returns an array containing all ship tos currently stored in the
     * sales document.<br>
     * A shallow copy is peformed. Changing the data of a array element
     * will cause the data of the basket to be changed, too.
     *
     * @return Array containing <code>ShipTo</code> objects
     */
    public ShipTo[] getShipTos() {
        return order.getShipTos();
    }
    
	/**
	  * Set the dirty flag
	  *
	  * @param isDirty if set to true, the document will be read from the backend when a read method is called
	  *                if set to false, the next call to a read method won't fill the object from the backend 
	  */
	 public void setDirty(boolean isDirty) {
		 order.setDirty(isDirty);
	 }
}
