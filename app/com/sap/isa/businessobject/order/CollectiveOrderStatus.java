/*****************************************************************************
    Class:        CollectiveOrderStatus
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author
    Created:      October 2002

    $Revision: #9 $
    $Date: 2001/07/25 $
*****************************************************************************/

package com.sap.isa.businessobject.order;

import com.sap.isa.backend.BackendTypeConstants;
import com.sap.isa.backend.boi.isacore.order.OrderStatusBackend;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.logging.IsaLocation;

/**
 * The collective order status allows the selection and display of existing
 * collective orders.
 *
 * @com.sap.isa.businessobject.order.CollectiveOrder
 *
 * @author SAP
 * @version 1.0
 */
public class CollectiveOrderStatus extends OrderStatus {

    private OrderStatusBackend backendService;
	static final private IsaLocation loc = IsaLocation.getInstance(CollectiveOrderStatus.class.getName());

    /**
     * Constructor for CollectiveOrderStatus.
     * @param order
     */
    public CollectiveOrderStatus(CollectiveOrder order) {
        super(order);
        setMultiPartnerScenario(true);       
    }


    /**
     * Constructor for CollectiveOrderStatus.
     */
    public CollectiveOrderStatus() {
        this.order = new CollectiveOrder();
        setMultiPartnerScenario(true);
    }


    /**
     * Is overwriten, to ensure that only collective orders could be selected.
     * 
     * @see com.sap.isa.businessobject.SalesDocumentStatus#readOrderHeaders(TechKey, Shop, DocumentListFilter)
     */
    public void readOrderHeaders(TechKey soldTo,
                                 Shop shop,
                                 DocumentListFilter filter)
              throws CommunicationException {
		final String METHOD = "readOrderHeaders()";
		loc.entering(METHOD);
        // select only collective orders
        filter.setTypeCOLLECTIVEORDER();        
        super.readOrderHeaders(soldTo, shop, filter);
        loc.exiting();
    }


    /**
     * Use the order backend at the moment. 
     * 
     * @see com.sap.isa.businessobject.SalesDocumentStatus#getBackendService(String)
     */
    protected OrderStatusBackend getBackendService(String type)
        throws BackendException {

        if(backendService == null) {
            backendService = (OrderStatusBackend)
                             bem.createBackendBusinessObject(BackendTypeConstants.BO_TYPE_ORDERSTATUS);
        }

        return backendService;
    }


}
