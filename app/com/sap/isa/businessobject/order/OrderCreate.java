/*****************************************************************************
    Class:        OrderCreate
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAPMarkets Europe GmbH
    Created:      11.4.2001
    Version:      1.0

    $Revision: #7 $
    $Date: 2002/05/08 $
*****************************************************************************/

package com.sap.isa.businessobject.order;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.UserData;
import com.sap.isa.backend.boi.isacore.order.CopyMode;
import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.item.ItemList;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.MessageList;

/**
 * Decorator for the order to provide functionality only available in special
 * situations. This class acts as a decorator for the order and offers methods
 * that can only be applied when the order is in the state of being created.
 *
 * @author
 * @version 1.0
 */
public class OrderCreate extends OrderDecoratorBase {
    static final private IsaLocation loc = IsaLocation.getInstance(OrderCreate.class.getName());

    /**
     * Creates a new instance for the given <code>Order</code> object.
     *
     * @param order Order to decorate
     */
    public OrderCreate(Order order) {
        super(order);
    }

    /**
     * Copies the basket's properties to the order object
     *
     * @param bskt Basket which is used as source
     */
    public void copyFromDocument(SalesDocument posd) {
        final String METHOD = "copyFromDocument()";
        loc.entering(METHOD);
        // copy of basket properties
        if (posd.getTechKey() != null) {
            setTechKey(posd.getTechKey());
        }
        setBasketId(posd.getTechKey());
        setHeader(posd.getHeader());
		getHeader().setDocumentType(HeaderData.DOCUMENT_TYPE_ORDER);
        setItems(posd.getItems());
        if (!posd.getHeader().getAssignedCampaigns().isEmpty() && posd.getHeader().getAssignedCampaigns().getCampaign(0) != null) {
            setCampaignKey(posd.getHeader().getAssignedCampaigns().getCampaign(0).getCampaignGUID().getIdAsString());
        }
        else {
            setCampaignKey(posd.getCampaignKey());
        }
        this.order.setShipTos(posd.getShipTos());
        loc.exiting();
    }

    /**
     * Creates an order from an auction predecessor document.
     *
     * @param auctionPredecessorKey <code>TechKey</code> of an auction
     * predecessor document
     * @param processType process type that shall be used for the resulting
     * order. If initial (""), the process type specified in the shop will be
     * used.
     * @param soltoKey <code>TechKey</code> of the sold to for whom the order
     * shall be created
     * @param shop the shop the action takes place in
     */
    public void createWithAuctionRef(TechKey auctionPredecessorKey, String processType, TechKey soltoKey, Shop shop) throws CommunicationException {
        final String METHOD = "createWithAuctionRef()";
        loc.entering(METHOD);
        if (loc.isDebugEnabled())
            loc.debug("auctionPredecessorKey=" + auctionPredecessorKey + ", processType=" + processType + ", soltoKey=" + soltoKey + ", shop=" + shop);

        // create the order on the backend by copying
        order.createWithReference(auctionPredecessorKey, CopyMode.TRANSFER_AND_UPDATE, processType, soltoKey, shop);
        loc.exiting();
    }

    /**
     * Creates an order from an external basket.
     *
     * @param documentKey <code>TechKey</code> of an external basket
     * @param soldtoKey <code>TechKey</code> of the sold to for whom the order
     * shall be created
     * @param shop the shop the action takes place in
     *
     * @deprecated This method will be replaced by <code>createFromExternalBasket(Shop shop,
     *                                                        BusinessPartnerManager bupama,
     *                                                        PartnerList partnerList,
     *                                                        WebCatInfo salesDocWebCat) </code>
     * @see #createFromExternalBasket(Shop shop,
     *            BusinessPartnerManager bupama,
     *            PartnerList partnerList,
     *            WebCatInfo salesDocWebCat)
     */
    public void createFromExternalBasket(SalesDocument posd, Shop shop, WebCatInfo webCat) throws CommunicationException {
        final String METHOD = "createFromExternalBasket()";
        loc.entering(METHOD);
        BusinessPartnerManager bupama = null;
        // create the order on the backend
        createFromExternalBasket(posd, shop, bupama, webCat);
        loc.exiting();
    }

    /**
     * Creates an order from an external basket.
     *
     * @param shop Shop used for this basket
     * @param user User for this basket
     * @param bupama the business partner manager
     * @param partnerList list of business partners for the document
     * @param salesDocWebCat the actual catalog
     */
    public void createFromExternalBasket(SalesDocument posd, Shop shop, BusinessPartnerManager bupama, WebCatInfo webCat) throws CommunicationException {

        final String METHOD = "createFromExternalBasket()";
        loc.entering(METHOD);
        if (!posd.getHeader().getAssignedCampaigns().isEmpty() && posd.getHeader().getAssignedCampaigns().getCampaign(0) != null) {
            order.setCampaignKey(posd.getHeader().getAssignedCampaigns().getCampaign(0).getCampaignGUID().getIdAsString());
            order.setCampaignObjectType(posd.getHeader().getAssignedCampaigns().getCampaign(0).getCampaignTypeId());
        }
        else {
            // setting the campaign key must be done,
            // before init is called, bacause only init
            // stores the campaign key
            order.setCampaignKey(posd.getCampaignKey());
            order.setCampaignObjectType(posd.getCampaignObjectType());
        }

        // transfer auction infos
        order.setAuctionID(posd.getAuctionID());
        order.setAuctionRelated(posd.isAuctionRelated());

        // create the order on the backend
        order.init(posd, shop, bupama, webCat);
        order.setBasketId(posd.getTechKey());
        loc.exiting();
    }

    /**
    	* Creates an order from an external basket.
    	*
    	* @param shop Shop used for this basket
    	* @param bupama the business partner manager
    	* @param partnerList list of business partners for the document
    	* @param salesDocWebCat the actual catalog
    	* @param processType process type for this basket
    	* @param execCampDet should campaign determination be executed
    	*/
    public void createFromExternalBasket(SalesDocument posd, Shop shop, BusinessPartnerManager bupama, WebCatInfo webCat, String processType, boolean execCampDet)
        throws CommunicationException {

        final String METHOD = "createFromExternalBasket()";
        loc.entering(METHOD);
        // setting the campaign key must be done,
        // before init is called, bacause only init
        // stores the campaign key
        if (!posd.getHeader().getAssignedCampaigns().isEmpty() && posd.getHeader().getAssignedCampaigns().getCampaign(0) != null) {
            order.setCampaignKey(posd.getHeader().getAssignedCampaigns().getCampaign(0).getCampaignGUID().getIdAsString());
            order.setCampaignObjectType(posd.getHeader().getAssignedCampaigns().getCampaign(0).getCampaignTypeId());
        }
        else {
            order.setCampaignKey(posd.getCampaignKey());
            order.setCampaignObjectType(posd.getCampaignObjectType());
        }
        // create the order on the backend
        order.init(posd, shop, bupama, webCat, processType, execCampDet);
        order.setBasketId(posd.getTechKey());
        loc.exiting();
    }

    /**
    	* Creates an order from an external basket.
    	*
    	* @param shop Shop used for this basket
    	* @param bupama the business partner manager
    	* @param partnerList list of business partners for the document
    	* @param salesDocWebCat the actual catalog
    	* @param processType process type for this basket
    	*/
    public void createFromExternalBasket(SalesDocument posd, Shop shop, BusinessPartnerManager bupama, WebCatInfo webCat, String processType) throws CommunicationException {

        final String METHOD = "createFromExternalBasket()";
        createFromExternalBasket(posd, shop, bupama, webCat, processType, false);
        loc.exiting();
    }
    /**
      * Creates an order from an external basket for all items of the given sold from partner.
      *
      * @param shop Shop used for this basket
      * @param bupama the business partner manager
      * @param partnerList list of business partners for the document
      * @param salesDocWebCat the actual catalog
      * @param soldFromTechKey Techkey of the soldFrom
      * @param soldFromId Id of the soldFrom
      */
    public void createFromBasketForSoldFrom(SalesDocument posd, Shop shop, TechKey soldFromTechKey, String soldFromId, BusinessPartnerManager bupama, WebCatInfo webCat)
        throws CommunicationException {

        final String METHOD = "createFromBasketForSoldFrom()";
        loc.entering(METHOD);
        if (loc.isDebugEnabled())
            loc.debug("soldFromTechKey=" + soldFromTechKey + ", soldFromId=" + soldFromId);
        // setting the campaign key must be done,
        // before init is called, bacause only init
        // stores the campaign key
        if (!posd.getHeader().getAssignedCampaigns().isEmpty() && posd.getHeader().getAssignedCampaigns().getCampaign(0) != null) {
            order.setCampaignKey(posd.getHeader().getAssignedCampaigns().getCampaign(0).getCampaignGUID().getIdAsString());
        }
        else {
            order.setCampaignKey(posd.getCampaignKey());
        }
        // create the order on the backend
        PartnerList partnerList = (PartnerList) posd.getHeader().getPartnerList().clone();

        //         // soldFrom is added as SoldFrom Partner to partnerlist
        //         partnerList.removePartner(PartnerFunctionData.SOLDFROM);
        //         partnerList.setSoldFrom(new PartnerListEntry(soldFromTechKey, soldFromId));

        order.init(shop, bupama, partnerList, webCat);

        order.setShipTos(posd.getShipTos());

        order.readHeader(); // may throw CommunicationException
        order.readAllItems(); // may throw CommunicationException

        //       soldFrom is added as SoldFrom Partner to partnerlist
        partnerList.removePartner(PartnerFunctionData.SOLDFROM);
        partnerList.setSoldFrom(new PartnerListEntry(soldFromTechKey, soldFromId));

        // Save the creation date, and the ship to before
        // copying the header
        String createdAt = order.getHeader().getCreatedAt();

        HeaderSalesDocument header = null;

        header = (HeaderSalesDocument) posd.getHeader().clone();

        // restore the saved data
        header.setCreatedAt(createdAt);

        // set additional information for ordersplit
        header.setPurchaseOrderExt(posd.getHeader().getSalesDocNumber());
        header.setPartnerList(partnerList);

        header.setTechKey(order.getTechKey());

        order.setHeader(header);

        // the extension information has to be copied again, because it gets
        // lost during the init process. Unfortunately a real copy is not possible
        // because we don't know what type of informtaion is stored
        header.setExtensionMap((HashMap) ((HashMap) posd.getHeader().getExtensionMap()).clone());

        ItemList itemList = order.getItems();

        itemList.clear();

        int size = posd.getItems().size();

        // Copy the items, that belong to the given soldFrom
        for (int i = 0; i < size; i++) {
            ItemSalesDoc oldItem = posd.getItems().get(i);
            // does the item belongs to the right reseller ?
            if (!oldItem.getPartnerList().getSoldFrom().getPartnerTechKey().equals(soldFromTechKey)) {
                continue;
            }
            if (oldItem.getParentId().isInitial()) {
                ItemSalesDoc newItem = new ItemSalesDoc();
                newItem.setProductId(oldItem.getProductId());
                if (oldItem.getProductId() == null || oldItem.getProductId().getIdAsString().length() == 0) {
                    newItem.setProduct(oldItem.getProduct());
                }
                newItem.setDataSetExternally(oldItem.isDataSetExternally());
                newItem.setUnit(oldItem.getUnit());
                newItem.setQuantity(oldItem.getQuantity());
                newItem.setReqDeliveryDate(oldItem.getReqDeliveryDate());
                newItem.setText(oldItem.getText());
                newItem.setShipTo(oldItem.getShipTo());
                if (oldItem.getContractKey() != null && !oldItem.getContractKey().getIdAsString().equals("")) {
                    newItem.setContractKey(oldItem.getContractKey());
                    newItem.setContractItemKey(oldItem.getContractItemKey());
                    newItem.setContractConfigFilter(oldItem.getContractConfigFilter());
                }
                //copy extension informations
                if (oldItem.getExtensionDataValues() != null) {
                    Iterator extIter = oldItem.getExtensionDataValues().iterator();
                    while (extIter.hasNext()) {
                        Map.Entry extEntry = (Map.Entry) extIter.next();
                        newItem.addExtensionData(extEntry.getKey(), extEntry.getValue());
                    }
                }
                newItem.setExternalItem(oldItem.getExternalItem());
                // add batch attributes
                newItem.setBatchDedicated(oldItem.getBatchDedicated());
                newItem.setBatchClassAssigned(oldItem.getBatchClassAssigned());

                itemList.add(newItem);
            }
        }

        order.setBasketId(posd.getTechKey());
        loc.exiting();
    }

    /**
     * Saves the order in the backend.
     *
     * @param ordr business object to be stored
     */
    public void saveOrderCreate() throws CommunicationException {
        order.saveAndCommit();
    }

    /**
     * Saves the order in the backend.
     *
     * @param ordr business object to be stored
     */
    public int save() throws CommunicationException {
        return order.saveOrderAndCommit();
    }

    /**
    * Simulates the order in the backend.
    *
    */
    public void simulate() throws CommunicationException {
        order.simulate();
    }

    /**
     * Retrieves the header information of the order.
     *
     * @return Header data
     */
    public void readAllItems() throws CommunicationException {
        order.readAllItems();
    }

    /**
     * Retrieves the header information of the order.
     *
     */
    public void readOrder() throws CommunicationException {
        order.readHeader();
    }

    /**
     * Reads order with header and items.
     */
    public void read() throws CommunicationException {
        order.read();
    }

    /**
     * Updates object representation in underlying storage.
     *
     * @return <true> if update was successful; otherwise <code>false</code>
     *
     * @deprecated This method will be replaced by <code>update(BusinessPartnerManager bupama)</code>
     * @see #update(BusinessPartnerManager bupama)
     */
    public void update() throws CommunicationException {
        BusinessPartnerManager bupama = null;
        order.update(bupama);
    }

    /**
     * Updates object representation in underlying storage.
     */
    public void update(BusinessPartnerManager bupama) throws CommunicationException {
        order.update(bupama);
    }

    /**
     * Updates object representation in underlying storage.
     *
     * @deprecated This method will be replaced by <code>update(BusinessPartnerManager bupama, ShopData shop)</code>
     * @see #update(BusinessPartnerManager bupama, ShopData shop)
     */
    public void update(ShopData shop) throws CommunicationException {
        final String METHOD = "update()";
        loc.entering(METHOD);
        //Dummy to be able to call the standard method
        BusinessPartnerManager bupama = null;

        order.update(bupama, shop);
        loc.exiting();
    }

    /**
     * Updates object representation in underlying storage.
     */
    public void update(BusinessPartnerManager bupama, ShopData shop) throws CommunicationException {
        order.update(bupama, (ShopData) shop);
    }

    /**
     *
     */
    public HeaderSalesDocument getHeader() {
        return order.getHeader();
    }

    /**
     * Returns a list of the items of the order.
     *
     * @return list of order's items
     */
    public ItemList getItems() {
        return order.getItems();
    }

    /**
     * Returns a list of the messages of the order.
     *
     * @return list of order's messages
     */
    public MessageList getMessageList() {
        return order.getMessageList();
    }

    /**
     *
     */
    public void setBasketId(TechKey techKey) {
        order.setBasketId(techKey);
    }

    /**
     *
     */
    public void setCampaignKey(String campaignKey) {
        order.setCampaignKey(campaignKey);
    }

    /**
     *
     */
    public void setTechKey(TechKey techKey) {
        order.setTechKey(techKey);
    }

    /**
     * 
     * @param shop
     */
    public void setGData(ShopData shop) throws CommunicationException {
        order.setGData(shop);
    }

    /**
     *
     *
     */
    public void setHeader(HeaderSalesDocument header) {
        order.setHeader(header);
    }

    /**
     *
     */
    public void setItems(ItemList items) {
        order.setItems(items);
    }

    /**
     *
     */
    public String getDescription() {
        return order.getHeader().getDescription();
    }

    /**
     *
     */
    public String getFreightValue() {
        return order.getHeader().getFreightValue();
    }

    /**
     *
     */
    public String getNetValue() {
        return order.getHeader().getNetValue();
    }

    /**
     *
     */
    public String getSalesDocNumber() {
        return order.getHeader().getSalesDocNumber();
    }

    /**
     *
     */
    public String getGrossValue() {
        return order.getHeader().getGrossValue();
    }

    /**
     *
     */
    public String getTaxValue() {
        return order.getHeader().getTaxValue();
    }
    /**
     *
     */
    public void clearMessages() {
        order.clearMessages();
    }

}
