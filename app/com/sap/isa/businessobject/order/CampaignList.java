/*****************************************************************************
    Class:        CampaignList
    Copyright (c) 2004, SAP AG, All rights reserved.
    Author:
    Created:      20.02.2004
    Version:      1.0

    $Revision: #1 $
    $Date: 2004/02/20 $
*****************************************************************************/

package com.sap.isa.businessobject.order;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.sap.isa.backend.boi.isacore.order.CampaignListData;
import com.sap.isa.backend.boi.isacore.order.CampaignListEntryData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.logging.IsaLocation;

/**
 * List of campaigns
 *
 * @author SAP AG
 * @version 1.0
 */
public class CampaignList
implements Cloneable, CampaignListData
{

	// List of campaigns
	protected ArrayList campaignList;

	/** 
	  *  The IsaLocation of the current Action.
	  *  This will be instantiated during perform and is always present.
	  */
	 protected IsaLocation log = IsaLocation.getInstance(CampaignList.class.getName());

	/**
	 * Creates a new <code>CampaignList</code> object.
	 */
	public CampaignList() {
		campaignList = new ArrayList();
	}
    
    
	/**
	 * Creates an empty <code>CampaignListEntry</code>.
	 *
	 * @returns CampaignListEntry which can added to the CampaignList
	 */
	public CampaignListEntry createCampaign() {
		return new CampaignListEntry();
	}
		
	/**
	 * Creates a initialized <code>CampaignListEntry</code> for the basket.
	 *
	 * @param String campaignId id of the campaign
	 * @param TechKey campaignGUID techkey of the campaign
	 * @param String campaignTypeId id of the campaign type
	 * @param boolean campaignValid flag that indicates, if the campaign is valid
	 * @param boolean manuallyEntered flag that indicates, if the campaign was manually entered
	 * 
	 * @returns CampaignListEntry which can added to the CampaignList
	 */
	public  CampaignListEntry createCampaign(String campaignId, TechKey campaignGUID, String campaignTypeId, 
										boolean campaignValid, boolean manuallyEntered) {
		return new CampaignListEntry(campaignId, campaignGUID, campaignTypeId, campaignValid,  manuallyEntered);
	}

	/**
	 * Creates an empty <code>CampaignListEntryData</code>.
	 *
	 * @returns CampaignListEntry which can added to the CampaignList
	 */
	public CampaignListEntryData createCampaignData() {
		return (CampaignListEntryData) new CampaignListEntry();
	}

	/**
	 * Creates a initialized <code>CampaignListEntryData</code> for the basket.
	 *
	 * @param String campaignId id of the campaign
	 * @param TechKey campaignGUID techkey of the campaign
	 * @param String campaignTypeId id of the campaign type
	 * @param boolean campaignValid flag that indicates, if the campaign is valid
	 * @param boolean manuallyEntered flag that indicates, if the campaign was manually entered
	 * 
	 * @returns CampaignListEntry which can added to the CampaignList
	 */
	public  CampaignListEntryData createCampaignData(String campaignId, TechKey campaignGUID, String campaignTypeId, 
										boolean campaignValid, boolean manuallyEntered) {
		return (CampaignListEntryData) new CampaignListEntry(campaignId, campaignGUID, campaignTypeId, campaignValid, manuallyEntered);
	}
    
	/**
	 * clear the campaign list
	 */
	public void clear() {
		campaignList.clear();
	}
    
	/**
	 * get the size of the list of campaigns
	 */
	public int size() {
		return campaignList.size();
	}
    
	/**
	 * returns true if the campaign list is empty
	 * 
	 * @return boolean
	 */
	public boolean isEmpty() {
	   return campaignList.isEmpty();
	}

	/**
	 * Returns a string representation of the object.
	 *
	 * @return object as string
	 */
	public String toString() {
		return  campaignList.toString();
	}

	/**
	 * get the campaign list
	 *
	 * @return List list of campaigns
	 */
	public List getList() {
		return campaignList;
	}
    
	/**
	 * gets the campaign for the given index
	 *
	 * @return CampaignListEntry the campaign for the given index, or
	 *                  null if index is out of bounds
	 */
	public CampaignListEntry getCampaign(int i) {
        
		CampaignListEntry campaign = null;
		try {
			campaign = (CampaignListEntry) campaignList.get(i);
		}
		catch (IndexOutOfBoundsException ex) {
			log.debug(ex.getMessage());
		}
        
		return campaign;
	}
    
	/**
	 * sets the campaign for the given index
	 *
	 * @param int index to set the product
	 * @param CampaignListEntry the campaign for the given index, or
	 *                 null if index is out of bounds
	 */
	public void addCampaign(int i, CampaignListEntryData campaign) { 
		if (campaign != null) {
		    campaignList.add(i, campaign);
		}
	}
    
	/**
	 * sets the campaign for the given index
	 *
	 * @param CampaignListEntry the campaign for the given indexc, or
	 *                 null if index is out of bounds
	 */
	public void addCampaign(CampaignListEntryData campaign) { 
		if (campaign != null) {
			campaignList.add(campaign);
		}
	}
    
	/**
	 * Creates and adds an campaign to the campaignList
	 *
	 * @param String campaignId id of the campaign
	 * @param TechKey campaignGUID techkey of the campaign
	 * @param String campaignTypeId id of the campaign type
	 * @param boolean campaignValid flag that indicates, if the campaign is valid
	 * @param boolean manuallyEntered flag that indicates, if the campaign was manually entered
	 */
	public void addCampaign(String campaignId, TechKey campaignGUID, String campaignTypeId, 
							boolean campaignValid, boolean manuallyEntered) {
		addCampaign((CampaignListEntryData) new CampaignListEntry(campaignId, campaignGUID, campaignTypeId, campaignValid, manuallyEntered));
	}
    
    /**
     * Returns the dmmy campaign that is used when items are copied, to indicate, that the header
     * campaign should not be copied into that item.
     * 
     * @return CampaignListEntryData the dummy campaign
     */
    public CampaignListEntryData getDummyCampaign() {
        return CampaignListEntry.DUMMY_CAMPAIGN;
    }

	/**
	 * set the campaign list
	 *
	 * @param  List new list of campaigns
	 */
	public void setList(List campaignListData) {
		campaignList.clear();
		campaignList.addAll(0, campaignList);
	}

	/**
	 * Performs a deep copy of this object.
	 * Because of the fact that
	 * all fields of this object consist of immutable objects like
	 * <code>String</code> and <code>TechKey</code> or primitive types
	 * the shallow copy is identical with a deep copy. For the
	 * <code>payment</code> property an explicit copy operation is performed
	 * because this object is the only mutable one.
	 *
	 * @return shallow (deep-like) copy of this object
	 */
	public Object clone() {
		CampaignList myClone = new CampaignList();
		myClone.campaignList = (ArrayList) campaignList.clone();
		return myClone;
	}

	/**
	 * Returns en iterator over the Entrys of the CampaignListEntry list in
	 * form of Map.
	 *
	 * @return iterator over campaigns
	 *
	 */
	public Iterator iterator( ) {
		return campaignList.iterator();
	}

}
