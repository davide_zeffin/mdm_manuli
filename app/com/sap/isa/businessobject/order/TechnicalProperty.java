/*****************************************************************************
    Class:        TechnicalProperty
    Copyright (c) SAP AG
    Author:       D023061
    Created:      Januray 2006
    Version:      1.0

*****************************************************************************/
package com.sap.isa.businessobject.order;

import com.sap.isa.backend.boi.isacore.order.TechnicalPropertyData;
import com.sap.isa.maintenanceobject.businessobject.Property;

/**
 * The class describes a technical property object.
 *
 * @author D023061
 * @version 1.0
 */

public class TechnicalProperty extends Property implements TechnicalPropertyData {

	private String attributeGuid;
	// data type from Backend
	private String dataTypeExternal;

	private String attributeId;

	/**
	  * Standard Constructor.
	  *
	  */
	public TechnicalProperty() {
	}

	/**
	 * Constructor with name of the Technical property Object.
	 * @param name, Name of the technical property object
	 */
	public TechnicalProperty(String name) {
		super(name);
		this.isRessourceKeyUsed = false;
	}

	/**
	 * Constructor with name and AttributeGuid of the Technical property Object.
	 * @param name, name of the technical property object
	 * @param attributeGuid, attributeGuid of the technical property object
	 */
	public TechnicalProperty(String name, String attributeGuid) {
		super();
		this.attributeGuid = attributeGuid;
		this.attributeId = name;
		this.isRessourceKeyUsed = false;
	}

	/**
	 * Returns the AttributeGuid.
	 * @return AttributeGuid
	 */
	public String getAttributeGuid() {
		return attributeGuid;
	}

	/**
	  * Sets the AttributeGuid.
	  * @param attributeGuid, AttributeGuid
	  */
	public void setAttributeGuid(String attributeGuid) {
		this.attributeGuid = attributeGuid;
	}

	/**
	 * Returns the dataTypeExternal.
	 * @return dataTypeExternal
	 */
	public String getDataTypeExternal() {
		return dataTypeExternal;
	}

	/**
	 * Sets the dataTypeExternal.
	 * @param string , dataType
	 */
	public void setDataTypeExternal(String dataType) {
		dataTypeExternal = dataType;
	}

	protected String convertDataTypeExternalToType(String dataType) {
		String javaType;

		//Default value is String
		javaType = TYPE_STRING;

		/*  Suppported Backend types
		*    ACCP	Buchungsperiode JJJJMM
		*    CHAR	Zeichenfolge
		*    CLNT	Mandant
		*    CUKY	Währungsschlüssel, wird von CURR-Feldern referiert
		*    CURR	Währungsfeld, abgelegt als DEC
		*    DATS	Datumsfeld (JJJJMMDD), abgelegt als Char(8)
		*    DEC	Rechen- oder Betragsfeld mit Komma und Vorzeichen
		*    FLTP	Gleitpunktzahl mit 8 Byte Genauigkeit
		*    INT1	1-Byte Integer, ganze Zahl <= 255
		*    INT2	2-Byte Integer, nur für Längenfeld vor LCHR oder LRAW
		*    INT4	4-Byte Integer, ganze Zahl mit Vorzeichen
		*    LANG	Sprachkennzeichen
		*    LCHR	Lange Zeichenfolge, benötigt voranstehendes INT2-Feld
		*    LRAW	Lange Bytefolge, benötigt voranstehendes INT2-Feld
		*    NUMC	Zeichenfolge nur mit Ziffern
		*    PREC	Genauigkeit eines QUAN-Feldes
		*    QUAN	Mengenfeld, zeigt auf ein Einheitenfeld mit Format UNIT
		*    RAW	Uninterpretierte Folge von Bytes
		*    RSTR	variabel lange Folge von Bytes
		*    SSTR	kurze variabel lange Zeichenfolge
		*    STRG	variabel lange Zeichenfolge
		*    TIMS	Zeitfeld (hhmmss),  abgelegt als Char(6)
		*    VARC	Lange Zeichenfolge, ab Rel. 3.0 nicht mehr unterstützt
		*    UNIT	Einheitenschlüssel für QUAN-Felder
		*/

		if (dataType.equals("ACCP")
			|| dataType.equals("CHAR")
			|| dataType.equals("CLNT")
			|| dataType.equals("CUKY")
			|| dataType.equals("CURR")
			|| dataType.equals("DATS")
			|| dataType.equals("LANG")
			|| dataType.equals("LCHR")
			|| dataType.equals("RAW")
			|| dataType.equals("RSTR")
			|| dataType.equals("SSTR")
			|| dataType.equals("STRG")) {
				
			javaType = TYPE_STRING;
			
		} else if (dataType.equals("NUMC") || dataType.equals("INT1") || dataType.equals("INT2") || dataType.equals("INT4")) {
			
			javaType = TYPE_INTEGER;
			
		} else if (dataType.equals(TYPE_BOOLEAN)) {
			
			javaType = TYPE_BOOLEAN;
			
		} 

		return javaType;
	
		
	}




	/** 
	 * Set the property type and make conversion to java supported type
	 *
	 * @param type
	 *
	 */
	public void setType(String type) {
		super.setType(this.convertDataTypeExternalToType(type));
		this.setDataTypeExternal(type);

	}

	/**
	* Set the property required
	* @param required
	*
	*/
	public void setRequired(String required) {
		String test = "X";
		if (required.equals("X")) {
			this.isRequired = true;
		} else {
			this.isRequired = false;
		}
	}

	/**
	* Set the property isDisabled.
	* @param disabled
	*
	*/
	public void setDisabled(String disabled) {
		if (disabled.equals("X")) {
			this.isDisabled = true;
			if (!getType().equals(TYPE_BOOLEAN)) {
				// set OutputOnly in case it is not already type Boolean
				this.isReadOnly = true;
			}
		} else {
			this.isDisabled = false;
		}
	}

	/**
	* Set Datatype to boolean if checkbox attribute is set.
	* @param String checkbox, String checkboxValue
	*
	*/
	public void setCheckbox(String checkbox, String checkboxValue) {
		if (checkbox.equals("X")) {
			this.setType(TYPE_BOOLEAN);
			this.setValue(false);
			if (checkboxValue.equals("X")) {
				this.setValue(true);
			}
		}
	}

	/**
	 * Returns the attributeId.
	 * @return attributeId
	 */
	public String getAttributeId() {
		return attributeId;
	}

	/**
	 * sets the property AttributeId
	 * @param string
	 */
	public void setAttributeId(String attributeId) {
		this.attributeId = attributeId;
	}

	/**
	 * Returns the converted value depending on the Type.
	 * @return String value
	 */
	public String getConvertedValue() {

		String techValue = " ";
		if (this.getValue() != null) {
			techValue = this.getValue().toString();
			if (this.getType().equals(TYPE_BOOLEAN)) {
				if (techValue.equals("true")) {
					return "X";
				}
				return " ";
			}
		}
		return techValue;

	}

	/**
	 * Set the property disabled and in addition output only type
	 *
	 * @param disabled
	 *
	 */
	public void setDisabled(boolean disabled) {
		this.isDisabled = disabled;
		if (disabled) {
			this.isReadOnly = true;
		}

	}
}
