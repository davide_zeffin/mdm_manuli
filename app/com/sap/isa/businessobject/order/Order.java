/*****************************************************************************
    Class:        Order
    Copyright (c) 2001, SAP AG, Germany, All rights reserved.
    Author:
    Created:      7.3.2001
    Version:      1.0

    $Revision: #12 $
    $Date: 2002/08/05 $
*****************************************************************************/

package com.sap.isa.businessobject.order;

import com.sap.isa.backend.BackendTypeConstants;
import com.sap.isa.backend.boi.isacore.SalesDocumentBackend;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.order.CopyMode;
import com.sap.isa.backend.boi.isacore.order.OrderBackend;
import com.sap.isa.backend.boi.isacore.order.OrderData;
import com.sap.isa.backend.boi.isacore.order.QuotationData;
import com.sap.isa.businessobject.Address;
import com.sap.isa.businessobject.BusinessObjectHelper;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.item.ItemList;
import com.sap.isa.businessobject.quotation.Quotation;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.util.CacheManager;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.core.util.table.Table;

import com.sap.isa.payment.backend.boi.PaymentBaseData;
import com.sap.isa.payment.backend.boi.PaymentBaseTypeData;
import com.sap.isa.payment.businessobject.BankTransfer;
import com.sap.isa.payment.businessobject.PaymentBase;
import com.sap.isa.payment.businessobject.PaymentBaseType;


/**
 * Class representing the order of the internet sales scenario.
 *
 * @author SAP AG
 * @version 1.0
 */
public class Order extends SalesDocument
                    implements OrderData {

    private TechKey basketId;
    private boolean invalid = false;
    /**
     * Backend to the order object
     */
    protected OrderBackend backendService;
    private boolean createdFromQuotation = false;
    private PaymentBaseType payType;
    private static final String CARD_TYPE = "CardType";
    private static final String SHIPPING_COND = "ShippingConditions";
    private PaymentBase payment;
    
    static {
        // Card type cache and shipping conditions cache removed due to problems with different backends.
        // The cache doesn't contain e.g. the configuration to distinguish
        // between different backends       
        //CacheManager.addCache(CARD_TYPE, 1);
        //CacheManager.addCache(SHIPPING_COND, 10);
    }

    /**
     * Creates a new order object without any parameters.
     */
    public Order() {
        this.payment = new PaymentBase();
    }

    /**
     * Creates a new Order using the quoatition as a template for it.
     *
     * @param quotation The quotation to be used in the creation
     *        process
     */
    public Order(Quotation quotation) {
        this();
        this.techKey = quotation.getTechKey();
        createdFromQuotation = true;
    }

    /**
     * Returns the TechKey of the basket used for creating this order.
     *
     * @return TechKey of Basket
     */
    public TechKey getBasketId() {
        if (invalid) {
            try {
                readHeader();
            }
            catch (CommunicationException e) {
                return null;
            }
        }
        return basketId;
    }

    /**
     * Sets the TechKey of basket used for creating this order
     *
     * @param basketId TechKey of the basket
     */
    public void setBasketId(TechKey basketId) {
        this.basketId = basketId;
    }

    /**
     * Reads the order data from the underlying storage for the special
     * case of a change of the order.
     */
    public void readForUpdate()
                        throws CommunicationException {
        final String METHOD = "readForUpdate()";
        log.entering(METHOD);
       
        try {
            int largeDocNoOfItemsThreshold = 0;
            
            if (getHeader()!= null && getHeader().getShop()!= null) {
                largeDocNoOfItemsThreshold = this.getHeader().getShop().getLargeDocNoOfItemsThreshold();
            }
            
            // read and lock from Backend
            if (header.isDirty()) {
                if (isChangeHeaderOnly()) {
                   ((OrderBackend) getBackendService()).readHeaderFromBackend(this, true);
                } else {
                    ((OrderBackend) getBackendService()).readHeaderFromBackend(this, false);
                }
                header.setDirty(false);
            }
            getBackendService().readShipTosFromBackend(this);
            if (isDirty && (!isHeaderChangeInLargeDocument(largeDocNoOfItemsThreshold) || selectedItemGuids.size() > 0)) {
                ((OrderBackend) getBackendService()).readAllItemsFromBackend(this, !isChangeHeaderOnly());
                isDirty = false;
            }
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally {
            log.exiting();
        }
    }

    /**
      * Reads the order data from the underlying storage for the special
      * case of a change of the order.
      *
      * @param shop The shop
      */
     public void readForUpdate(ShopData shop )
                         throws CommunicationException {

        final String METHOD = "readForUpdate()";
        log.entering(METHOD);
         // write some debugging info
         if (log.isDebugEnabled()) {
             log.debug("readForUpdate(): ShopData = "+shop);
         }

         try {

             // read and lock from Backend
             if (header.isDirty()) {
                //Large Document and no header change
                 if (isLargeDocument(shop.getLargeDocNoOfItemsThreshold()) && !isChangeHeaderOnly()) {
                    ((OrderBackend) getBackendService()).readHeaderFromBackend(this, false);
                 } else {
                    ((OrderBackend) getBackendService()).readHeaderFromBackend(this, true);
                 }
                 header.setDirty(false);
             }
             getBackendService().readShipTosFromBackend(this);
             if (isDirty && (!isHeaderChangeInLargeDocument(shop.getLargeDocNoOfItemsThreshold()) || selectedItemGuids.size() > 0)) {
                 ((OrderBackend) getBackendService()).readAllItemsFromBackend(this, !isChangeHeaderOnly());
                 isDirty = false;
             }
         }
         catch (BackendException ex) {
             BusinessObjectHelper.splitException(ex);
         }
         finally {
            log.exiting();
         }
     }
    /**
     * Retrieves the header from the backend and stores the data
     * in this object.
     */
    public void readHeader() throws CommunicationException {
        
        final String METHOD = "readHeader()";
        log.entering(METHOD);
        invalid = false;

        try {
            // get header in Backend
            if (header.isDirty()) {
                getBackendService().readHeaderFromBackend(this);
                header.setDirty(false);
            }
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally {
            log.exiting();
        }
    }

   /**
     * Retrieves the item information of the order from the backend and
     * stores the data in this object.
     *
     *
     * @param user The current user
     */
    public void readAllItemsForChange() throws CommunicationException {

        final String METHOD = "readAllItemsForChange()";
        log.entering(METHOD);
        try {
            // get all items in Backend
            if (isDirty) {
                getBackendService().readAllItemsFromBackend(this);
                isDirty = false;
            }
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally {
            log.exiting();
        }
    }

    /**
     * Simulates the order in the backend.
     *
     */
    public void simulate() throws CommunicationException {

        final String METHOD = "simulate()";
        log.entering(METHOD);

        // call backend here
        try {
             ((OrderBackend) getBackendService()).simulateInBackend(this);
             isDirty = true;
             header.setDirty(true);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally {
            log.exiting();
        }
    }


    /**
     * Perform a precheck of order data.
     *
     * @return <true> if check was successful; otherwise <code>false</code>
     */
    public boolean preCheck(ShopData shop) throws CommunicationException {
        // call backend for precheck here

        final String METHOD = "preCheck()";
        log.entering(METHOD);
        if (log.isDebugEnabled())
            log.debug("preCheck: ShopData = "+shop);
        try {
            isDirty = true;
            header.setDirty(true);
            return getBackendService().preCheckInBackend(this, (ShopData) shop);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally {
            log.exiting();
        }
        return true; // never reached
    }

    /**
     * Set global data in the backend
     *
     * @return <true> if check was successful; otherwise <code>false</code>
     * @deprecated This method will be replaced by <code>setGData(ShopData shop,
     *                                                            WebCatInfo salesDocWebCat)</code>
     * @see #setGData(ShopData shop,
     *               WebCatInfo salesDocWebCat)
     */
    public void setGData(ShopData shop) throws CommunicationException {
        
        final String METHOD = "setGData()";
        log.entering(METHOD);
        // write some debugging info
        if (log.isDebugEnabled()) {
            log.debug("setGData(): shop = " + shop);
        }

        try {
            ((OrderBackend) getBackendService()).setGData(this, shop);
            isDirty = true;
            header.setDirty(true);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally {
            log.exiting();
        }
    }

    /**
     * Set global data in the backend
     *
     * @return <true> if check was successful; otherwise <code>false</code>
     */
    public void setGData(ShopData shop, WebCatInfo salesDocWebCat) throws CommunicationException {
        final String METHOD = "setGData()";
        log.entering(METHOD);
        // write some debugging info
        if (log.isDebugEnabled()) {
            log.debug("setGData(): shop = " + shop + ", salesDocWebCat = " + salesDocWebCat);
        }

        if (shop.isInternalCatalogAvailable() && salesDocWebCat == null) {
            throw( new IllegalArgumentException("Parameter salesDocWebCat can not be null!"));
        }
        this.salesDocWebCat = salesDocWebCat;

        try {
            ((OrderBackend) getBackendService()).setGData(this, shop);
            isDirty = true;
            header.setDirty(true);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally {
            log.exiting();
        }
    }


    /**
     * Saves the order in the backend.<br>
     * 
     * <b>Note: <b> Since release 4.0 the method don't commit automaticly
     * 
     */
    public void save() throws CommunicationException {

        // write some debugging info
        final String METHOD = "save()";
        log.entering(METHOD);

        try {
            ((OrderBackend) getBackendService()).saveInBackend(this, false);
            isDirty = true;
            header.setDirty(true);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally {
            log.exiting();
        }
    }


    /**
     * Saves the order in the backend.
     */
    public void saveAndCommit() throws CommunicationException {

        final String METHOD = "saveAndCommit()";
        log.entering(METHOD);

        try {
            ((OrderBackend) getBackendService()).saveInBackend(this, true);
            isDirty = true;
            header.setDirty(true);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally {
            log.exiting();
        }
    }


    /**
     * Saves the order in the backend.
     */
    public int saveOrderAndCommit() throws CommunicationException {

        final String METHOD = "saveOrderAndCommit()";
        log.entering(METHOD);
        int returnCode = 0;
   

        try {
            returnCode = ((OrderBackend) getBackendService()).saveOrderInBackend(this, true);
            isDirty = true;
            header.setDirty(true);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally {
            log.exiting();
        }
        return returnCode;
    }


    /**
     * Releases the lock of the order in the backend.
     *
     * @param ordr business object to be stored
     */
    public void dequeue() throws CommunicationException {

        final String METHOD = "dequeue()";
        log.entering(METHOD);

        try {
            ((OrderBackend) getBackendService()).dequeueInBackend(this);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally {
            log.exiting();
        }
    }

    /* Destroy the data of the document (items and header) in the backend
     * representation. After a
     * call to this method, the object is in an undefined state. You have
     * to call init() before you can use it again.<br>
     * This method is normally called before removing the BO-representation
     * of the object using the BOM.
     */
    public void destroyContent()  throws CommunicationException  {
        final String METHOD = "destroyContent()";
        log.entering(METHOD);

        try {
            isDirty = true;
            header.setDirty(true);
            getBackendService().emptyInBackend(this);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally {
            log.exiting();
        }
    }

    /**
     * Creates an order from a quotation
     *
     * @param quotation as predecessor document,
     * @param soldtoKey <code>TechKey</code> of the sold to for whom the order
     * shall be created
     * @param shop the shop the action takes place in
     */
    public void createFromQuotation(Quotation quotation, Shop shop) 
         throws CommunicationException {
        final String METHOD = "createFromQuotation()";
        log.entering(METHOD);
        if (log.isDebugEnabled())
            log.debug("quotation="+quotation+", shop="+shop); 
        // create order in Backend
        try {
            try {
                ((OrderBackend) getBackendService()).createFromQuotation(
                        (QuotationData)quotation,
                        (ShopData)shop,
                        (OrderData)this
                        );
                isDirty = true;
                header.setDirty(true);
            }
            catch (BackendException ex) {
                BusinessObjectHelper.splitException(ex);
            }
    
            // check if order is valid
            TechKey orderKey = getTechKey();
            if (orderKey == null ||
                orderKey.isInitial()) {
                invalid = true;
                setInvalid();
            }
            else {
                setBasketId(orderKey);
            }
        } finally {
            log.exiting();
        }
    }

    /**
     * Creates an order from a predecessor document.
     *
     * @param predecessorKey <code>TechKey</code> of a predecessor document,
     *        e.g. a quotation, to create this order from
     * @param copyMode determines if data are copied form the predecessor and
     *        if the predecessor data will be updated during copying,
     * @see com.sap.isa.backend.boi.isacore.order.copyMode
     * <code>copyMode</code>
     * @param processType process type that shall be used for the resulting
     * order. If initial (""), the process type specified in the shop will be
     * used.
     * @param soltoKey <code>TechKey</code> of the sold to for whom the order
     * shall be created
     * @param shop the shop the action takes place in
     */
    public void createWithReference(
            TechKey predecessorKey,
            CopyMode copyMode,
            String processType,
            TechKey soldToKey,
            Shop shop
            ) throws CommunicationException {
                
        final String METHOD = "createWithReference()";
        log.entering(METHOD);
        // write some debugging info
        if (log.isDebugEnabled()) {
            log.debug("createWithReference(): predecessorKey = " + predecessorKey
                    + ", copyMode = " + copyMode
                    + ", processTyoe = " + processType
                    + ", soldToKey = " + soldToKey
                    + ", shop = " + shop);
        }

        header = new HeaderSalesDocument();
        itemList = new ItemList();
        TechKey orderKey = null;
        try {
        
            try {
                // create Basket in Backend
                ((OrderBackend) getBackendService()).createWithReference(
                        predecessorKey,
                        copyMode,
                        processType,
                        soldToKey,
                        (ShopData)shop,
                        (OrderData)this
                        );
                isDirty = true;
                header.setDirty(true);
            }
            catch (BackendException ex) {
                BusinessObjectHelper.splitException(ex);
            }
    
            // check if order is valid
            orderKey = getTechKey();
            if (orderKey == null || orderKey.isInitial()) {
                invalid = true;
                setInvalid();
            }
            else {
                setBasketId(orderKey);
            }
        } finally {
            log.exiting();
        }
    }

    /**
     * Initializes an empty instance of this object with the data coming from
     * the provided document. The former state of this document is dropped
     * and lost, the fields of the other document are copied into the
     * fields of this document. To do this the <code>clone()</code> method of
     * the fields is used to get an shallow copy and minimize interference
     * between the new and the old object.<br>
     * An backend representation of the new
     * object is created and filled with the data retrieved by the
     * copy operation.
     *
     * @param posd The object used for the creation of this object
     * @param shop Shop used for this basket
     * @param user User for this basket
     */
/*
     public void init(SalesDocument posd,
            Shop shop,
            User user) throws CommunicationException {

        // write some debugging info
        if (log.isDebugEnabled()) {
            log.debug("init(): posd = " + posd + ", shop = " + shop + ", user = " + user);
        }

        clearData();
        payType = null;

        try {
            getBackendService().createInBackend(shop, posd, user);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }

        // Copy the basket data to the created order
        header   = (HeaderSalesDocument) posd.getHeader().clone();
        this.setTechKey(posd.getTechKey());

        itemList.clear();

        int size = posd.getItems().size();

        // Copy the items. Because of the fact, that the CRM has problems
        // dealing with items containing too much data, I do not use the
        // clone() method but copy the relevant fields manually
        for (int i = 0; i < size; i++) {
            ItemSalesDoc newItem = new ItemSalesDoc();
            ItemSalesDoc oldItem = posd.getItems().get(i);
            newItem.setProductId(oldItem.getProductId());
            newItem.setUnit(oldItem.getUnit());
            newItem.setQuantity(oldItem.getQuantity());
            newItem.setReqDeliveryDate(oldItem.getReqDeliveryDate());
            newItem.setText(oldItem.getText());
            newItem.setShipTo(oldItem.getShipTo());
            itemList.add(newItem);
        }

        update(user, shop);
    }
*/

    /* get the BackendService, if necessary */
    protected SalesDocumentBackend getBackendService()
            throws BackendException{

        synchronized (this) {
            if (backendService == null) {
                backendService =
                    (OrderBackend) bem.createBackendBusinessObject(BackendTypeConstants.BO_TYPE_ORDER, null);
            }
        }
        return backendService;
    }

    // Methods necessary to satisfy the data interface


    /**
     * Method for the lazy retrievement of the ShipTo's
     * address. This method is called by the proxy object generated
     * by the <code>createShipTo</code> method to read the address
     * only if it is really used.
     *
     * @param techKey Technical key of the ship to for which the
     *                address should be read
     * @return Address for the given ship to or <code>null<code> if no
     *         address is found
     */
    protected Address readShipToAddress(TechKey techKey) {
        
        try {
            return (Address) getBackendService().readShipToAddressFromBackend(this, techKey);
        }
        catch (BackendException e) {
            return null;
        }
    }

    /**
     * Removes a Item from the order.
     *
     * @param techKey Technical key of the item to be removed
     */
    public void removeItem(TechKey techKey) throws CommunicationException {
        super.removeItem(techKey);                                              //INS 902771
        // throw new UnsupportedOperationException("method not implemented");   //DEL 902771
    }

    /**
     * Cancels an Item(s) from the order.
     *
     * @param techKeys Technical keys of the items to be canceld
     */
    protected void cancelItems(TechKey[] techKeys) throws CommunicationException {
        final String METHOD = "cancelItems()";
        log.entering(METHOD);
        
        if (techKeys == null || techKeys.length == 0) {
            log.debug("techKeys is null or contains no entries. Exit method");
            return;
        }
        
        // write some debugging info
        if (log.isDebugEnabled()) {
            log.debug("cancelItems(): techKeys = " + techKeys);
        }

        try {
            ((OrderBackend) getBackendService()).cancelItemsInBackend(this, techKeys);
            isDirty = true;
            header.setDirty(true);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally {
            log.exiting();
        }
    }

    /**
     * Retrieves a list of available shipping conditions from the
     * backend.
     *
     * @param language The language
     * @return table containing the conditions with the technical key
     *         as row key
     */
    public ResultData readShipCond(String language) throws CommunicationException {
        final String METHOD = "readShipCond()";
        log.entering(METHOD);
        // write some debugging info
        if (log.isDebugEnabled()) {
            log.debug("readShipCond(): language = " + language);
        }
        Table shippingCond = null;
        // Use of cachemanager removed since different backend (system, clients)  
        // are not consider here and could lead to a mixture of available values
		/** try {
        
            shippingCond = (Table) CacheManager.get(SHIPPING_COND, language);
    
            synchronized (this) {
                if (shippingCond == null) {
                    try {
                        shippingCond = getBackendService().readShipCondFromBackend(language);
                        CacheManager.put(SHIPPING_COND, language, shippingCond);
                    }
                    catch (BackendException ex) {
                        BusinessObjectHelper.splitException(ex);
                    }
                }
            }
        **/
		try {
			  // get all items in Backend
			  return new ResultData(getBackendService().readShipCondFromBackend(language));
		  }
		catch (BackendException ex) {
			BusinessObjectHelper.splitException(ex);
        } finally {
            log.exiting();
        }
        
        return new ResultData(shippingCond);
    }

    /**
     * Retrieves the available payment types from the backend.
     *
     * @return PaymentType object containing the available payment types.
     */
    public PaymentBaseTypeData readPaymentType() throws CommunicationException {

        final String METHOD = "readPaymentType()";
        log.entering(METHOD);
        try {
        
            synchronized (this) {
                if (payType == null) {
    
                    try {
                        payType = new PaymentBaseType();
    
                        // get all items in Backend
                        ((OrderBackend) getBackendService()).readPaymentTypeFromBackend(this, payType);
                    }
                    catch (BackendException ex) {
                        BusinessObjectHelper.splitException(ex);
                    }
                }
            }
        } finally {
            log.exiting();
        }
        return payType;
    }

    /**
     * Reads the Bank Name for a Bank Key from backend
     * 
     */
    public void readBankName(BankTransfer bankTransfer) throws CommunicationException {
         
        final String METHOD = "readBankName()";
        log.entering(METHOD);

           try {
             ((OrderBackend) getBackendService()).readBankNameFromBackend(bankTransfer);
           }
           catch (BackendException ex) {
             BusinessObjectHelper.splitException(ex);
           }        
           finally {
             log.exiting();
           }        
   }
    
//  Method is inherited from SalesDocument 
    /**
     * Retrieves a list of available credit card types from the
     * backend.
     *
     * @return Table containing the credit card types with the technical key
     *         as row key
     */
//    public ResultData readCardType() throws CommunicationException {
//
//      final String METHOD = "readCardType()";
//      log.entering(METHOD);
//        Table cardType;
//      try {
//          
//          synchronized (this) {
//  
//              cardType = (Table) CacheManager.get(CARD_TYPE, "X");
//  
//              if (cardType == null) {
//  
//                  try {
//                      cardType = ((OrderBackend) getBackendService()).readCardTypeFromBackend();
//                      CacheManager.put(CARD_TYPE, "X", cardType);
//                  }
//                  catch (BackendException ex) {
//                      BusinessObjectHelper.splitException(ex);
//                  }
//              }
//          }
//      } finally {
//          log.exiting();
//      }
//        return new ResultData(cardType);
//    }
    
    
    
    /**
     * Makes a copy of the object.<br> 
     * 
     * @returns a copy of object. 
     * 
     */
    protected Object clone() throws CloneNotSupportedException {    
        
        return super.clone();
    }
    
    /**
     * Creates an order from a quotation
     *
     * @param quotation as predecessor document,
     * @param soldtoKey <code>TechKey</code> of the sold to for whom the order
     * shall be created
     * @param shop the shop the action takes place in
     */
    public void createFromQuotation(Quotation quotation, Shop shop, boolean saveOrder) 
         throws CommunicationException {

        // create order in Backend but don't save it directly
        try {
            ((OrderBackend) getBackendService()).createFromQuotation(
                    (QuotationData)quotation,
                    (ShopData)shop,
                    (OrderData)this,
                    saveOrder);
            isDirty = true;
            header.setDirty(true);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }

        // check if order is valid
        TechKey orderKey = getTechKey();
        if (orderKey == null ||
            orderKey.isInitial()) {
            invalid = true;
            setInvalid();
        }
        else {
            setBasketId(orderKey);
        }
    }
    /**
      * Set the payment information using the backend interface
      *
      * @param payment the payment information to be set
      */
     public void setPayment(PaymentBase payment) {
         this.payment = (PaymentBase) payment;
     }
     
     /**
      * Get the payment information using the backend interface.
      *
      * @return payment information
      */
     public PaymentBase getPayment() {
         return payment;
     }

     /**
      * Get the payment information using the backend interface.
      *
      * @return payment information
      */
     public PaymentBaseData getPaymentData() {
         return (PaymentBaseData)payment;
     }
     
     /**
      * Saves the order in the backend.
      */
     public void createPayment() throws CommunicationException {

         final String METHOD = "createPayment()";
         log.entering(METHOD);
         int returnCode = 0;
   

         try {
             ((OrderBackend) getBackendService()).createPayment(this);
         }
         catch (BackendException ex) {
             BusinessObjectHelper.splitException(ex);
         }
         finally {
             log.exiting();
         }
         
     }
         
}
