/*****************************************************************************
    Class:        OrderStatusBase
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      02.02.2004
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.businessobject.order;

import com.sap.isa.backend.boi.isacore.DocumentStatusConfiguration;
import com.sap.isa.backend.boi.isacore.SalesDocumentStatusBackend;
import com.sap.isa.businessobject.BusinessObjectHelper;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.SalesDocumentStatus;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;

/**
 * The base class to read and display sales documents. <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public abstract class OrderStatusBase extends SalesDocumentStatus {

    
	/**
	  * Get the suitable BackendService for the used sales document. <br>
	  */
	protected abstract SalesDocumentStatusBackend getBackendService()
				throws BackendException;

    
	/**
	 * Read the object for the given techKey. <br>
	 * 
	 * @param techKey techKey to identify the document 
	 * @param documentStatusConfiguration configuration data
	 * 
	 * @throws CommunicationException
	 */
	public void readDocumentx(TechKey techKey,
							 DocumentStatusConfiguration documentStatusConfiguration)
			throws CommunicationException {
		final String METHOD = "readDocument()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("techKey="+techKey);
        /* check if document is still read */
        if (!getTechKey().equals(techKey)) {

            singleOrderHeader = null;
            itemMap.clear();
            itemList.clear();
            shipToList.clear();
            clearMessages();
            setTechKey(techKey);
            try {
                // read AttributeSet from Backend
                getBackendService().readDocumentStatus(this, documentStatusConfiguration);
            }
            catch (BackendException ex) {
            	log.exiting();
                BusinessObjectHelper.splitException(ex);
            }
        }
        log.exiting();
    }
	/**
	 * Read the object for the given techKey. <br>
	 * 
	 * @param techKey techKey to identify the document 
	 * @param documentStatusConfiguration configuration data
	 * 
	 * @throws CommunicationException
	 */
	public void readDocument(TechKey techKey,
							 DocumentStatusConfiguration documentStatusConfiguration)
			throws CommunicationException {
		final String METHOD = "readDocument()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("techKey="+techKey);
		/* check if document is still read */
		if (!getTechKey().equals(techKey)  ||
		    !selectedItemGuids.isEmpty()) {  // In case of large document, read always selected items
 		    if (singleOrderHeader != null  &&
				! singleOrderHeader.getTechKey().equals(techKey)) {
				// only clear guids if we are reading another document. 
				selectedItemGuids.clear();
			}
			singleOrderHeader = null;
			itemMap.clear();
			itemList.clear();
			shipToList.clear();
			clearMessages();
			setTechKey(techKey);
			campaignDescriptions.clear();
			campaignTypeDescriptions.clear();
			try {
				// read AttributeSet from Backend
				getBackendService().readDocumentStatus(this, documentStatusConfiguration);
			}
			catch (BackendException ex) {
				log.exiting();
				BusinessObjectHelper.splitException(ex);
			}
		}
		log.exiting();
	}
    

}
