/*****************************************************************************
	Class:        ExtRefObjectList
	Copyright (c) 2004, SAP AG, All rights reserved.
	Author:
	Created:      13.01.2005
	Version:      1.0

	$Revision: #1 $
	$Date: 2005/01/13 $
*****************************************************************************/

package com.sap.isa.businessobject.order;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import com.sap.isa.backend.boi.isacore.order.ExtRefObjectListData;
import com.sap.isa.backend.boi.isacore.order.ExtRefObjectData;
import com.sap.isa.core.TechKey;

/**
 * List of external reference objects
 *
 * @author SAP AG
 * @version 1.0
 */
public class ExtRefObjectList implements Cloneable, ExtRefObjectListData{
	
	// List of external reference objects
	protected ArrayList extRefObjectList;

	/**
	 * Creates a new <code>ExtRefObjectList</code> object.
	 */
	public ExtRefObjectList() {
		extRefObjectList = new ArrayList();
	}
  
	/**
	 * adds the external reference object to the list
	 *
	 * @param extRefObjectListEntry the external reference object 
	 *                 
	 */
	public void addExtRefObject(ExtRefObjectData extRefObject) { 
		if (extRefObject != null) {
			extRefObjectList.add(extRefObject);
		}
	}  	
	
	/**
	 * sets the external reference object for the given index
	 *
	 * @param int index to set the external reference object
	 * @param extRefObjectListEntry the external reference object 
	 *        for the given index
	 */
	public void addExtRefObject(int i, ExtRefObjectData extRefObject) { 
		if (extRefObject != null) {
			extRefObjectList.add(i, extRefObject);
		}
	}
	/**
	 * clear the external reference object list
	 */
	public void clear() {

		extRefObjectList.clear();
	}
    
	/**
	 * get the size of the list of external reference objects
	 */
	public int size() {
		return extRefObjectList.size();
	}
    
	/**
	 * returns true if the external reference object list is empty
	 * 
	 * @return boolean
	 */
	public boolean isEmpty() {
	   return extRefObjectList.isEmpty();
	}
	
	/**
	 * Returns en iterator over the Entrys of the external reference object list in
	 * form of Map.
	 *
	 * @return iterator over external reference objects list
	 *
	 */
	public Iterator iterator( ) {
		return extRefObjectList.iterator();
	}
	
	/**
	 * get the external reference object list
	 *
	 * @return List list of external reference object
	 */
	public List getList() {
		return extRefObjectList;
	}
	
	/**
	 * set the external reference object list
	 *
	 * @param  List new list of external reference objects
	 */
	public void setList(List extRefObjectList) {
		extRefObjectList.clear();
		extRefObjectList.addAll(0, extRefObjectList);
	}
	
	/**
	 * Performs a copy of this object.
	 *
	 * @return Object copy of this object
	 */
	public Object clone() {
		ExtRefObjectData extRefObj;
		ExtRefObjectList myClone = new ExtRefObjectList();
		myClone.extRefObjectList = (ArrayList) extRefObjectList.clone();
		for (int i = 0; i < myClone.size(); i++) {
			extRefObj = myClone.getExtRefObject(i);
			extRefObj.setTechKey(new TechKey(""));
		}
		return myClone;
	}	
	
	/**
	 * gets the external reference object  for the given index
	 *
	 * @return String the external reference object for the given index, or
	 *                  null if index is out of bounds
	 */
	public ExtRefObjectData getExtRefObject(int i) {
		return (ExtRefObjectData)extRefObjectList.get(i); 
	}
	
	/**
	 * Creates an empty <code>ExtRefObjectData</code>.
	 *
	 * @returns ExtRefObjectData which can be added to the external reference object list
	 */
	public ExtRefObjectData createExtRefObjectListEntry() {
		return new ExtRefObject();	
	}
		
		
}