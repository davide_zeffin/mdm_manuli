/*****************************************************************************
    Class:        TechnicalPropertyMaintenace
    Copyright (c) SAP AG
    Author:       D023061
    Created:      Januray 2006
    Version:      1.0

*****************************************************************************/
package com.sap.isa.businessobject.order;

import com.sap.isa.backend.boi.isacore.order.TechnicalPropertyMaintenanceObjectData;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.maintenanceobject.backend.boi.MaintenanceObjectBackend;
import com.sap.isa.maintenanceobject.businessobject.MaintenanceObject;

/**
 * The class describes a Maintenance Object for a technicalPropertyGroup.
 *
 * @author D023061
 * @version 1.0
 */
public class TechnicalPropertyMaintenanceObject extends MaintenanceObject implements TechnicalPropertyMaintenanceObjectData {

	/* (non-Javadoc)
	 * @see com.sap.isa.maintenanceobject.businessobject.MaintenanceObject#getBackendService()
	 */
	protected MaintenanceObjectBackend getBackendService() throws BackendException {
		// TODO Auto-generated method stub
		// Nothing to do there is no BackendObject
		return null;
	}
	
	
	/**
	 * Standard Constructor.
	 *
	 */
	public TechnicalPropertyMaintenanceObject() {
      super();
	};

}
