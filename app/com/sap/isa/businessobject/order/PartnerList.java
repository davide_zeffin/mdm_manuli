/*****************************************************************************
    Class:        PartnerList
    Copyright (c) 2002, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:
    Created:      28.5.2002
    Version:      1.0

    $Revision: #4 $
    $Date: 2002/11/05 $
*****************************************************************************/

package com.sap.isa.businessobject.order;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.sap.isa.backend.boi.isacore.order.PartnerListData;
import com.sap.isa.backend.boi.isacore.order.PartnerListEntryData;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.core.TechKey;

/**
 * List of business partners and their partner functions. <br>
 * 
 *
 * @author
 * @version 1.0
 */
public class PartnerList
implements PartnerListData, Cloneable {

    // List of businesspartners
    protected HashMap partnerList;

    /**
     * Creates a new <code>PartnerList</code> object.
     */
    public PartnerList() {
        partnerList = new HashMap();
    }

    /**
     * Creates an empty <code>PartnerListEntryData</code> for the basket.
     *
     * @returns PartnerListEntry which can added to the partnerList
     */
    public PartnerListEntryData createPartnerListEntry() {
        return new PartnerListEntry();
    }

    /**
     * Creates a initialized <code>PartnerListEntryData</code> for the basket.
     *
     * @param partnerGUID techkey of the business partner
     * @param partnerid id of the business partner
     * @returns PartnerListEntry which can added to the partnerList
     */
    public PartnerListEntryData createPartnerListEntry(TechKey partnerGUID, String partnerId) {
        return new PartnerListEntry(partnerGUID, partnerId);
    }

    /**
     * Get the business partner for the given partner function(bpRole). <br>
     *
     * @param bpRole Name of the partner function
     * @return PartnerListEntry of the business partner, with the given 
     *         partner function (role) or null if not found
     */
    public PartnerListEntry getPartner(String bpRole) {
        PartnerListEntry entry = null;

        //try to find requeuested business partner
        entry = (PartnerListEntry) partnerList.get(bpRole);

        return entry;
    }

    /**
     * Get the business partner for the given partner function(role).
     *
     * @param bpRole Name of the partner function
     * @return PartnerListEntryData of the business partner, with the given
     *         partner function (role) or null if not found
     */
    public PartnerListEntryData getPartnerData(String bpRole) {
        return (PartnerListEntryData) getPartner(bpRole);
    }

    /**
     * Convenience method to Get the soldto business partner, or null, if not available
     *
     * @return PartnerListEntry of the soldTo business partner, or null if not found
     */
    public PartnerListEntry getSoldTo() {
        return getPartner(PartnerFunctionData.SOLDTO);
    }

    /**
     * Convenience method to Get the soldto business partner, or null, if not available
     *
     * @return PartnerListEntryData of the soldTo business partner, or null if not found
     */
    public PartnerListEntryData getSoldToData() {
        return (PartnerListEntryData) getSoldTo();
    }

    /**
     * Convenience method to Get the reseller business partner, or null, if not available
     *
     * @return PartnerListEntry of the reseller business partner, or null if not found
     */
    public PartnerListEntry getReseller() {
        return getPartner(PartnerFunctionData.RESELLER);
    }

    /**
     * Convenience method to Get the reseller business partner, or null, if not available
     *
     * @return PartnerListEntryData of the reseller business partner, or null if not found
     */
    public PartnerListEntryData getResellerData() {
        return (PartnerListEntryData) getReseller();
    }

    /**
     * Convenience method to Get the contact business partner, or null, if not available
     *
     * @return PartnerListEntry of the contact business partner, or null if not found
     */
    public PartnerListEntry getContact() {
        return getPartner(PartnerFunctionData.CONTACT);
    }

    /**
     * Convenience method to Get the contact business partner, or null, if not available
     *
     * @return PartnerListEntry of the contact business partner, or null if not found
     */
    public PartnerListEntryData getContactData() {
        return (PartnerListEntryData) getContact();
    }

    /**
     * Convenience method to Get the soldfrom business partner, or null, if not available
     *
     * @return PartnerListEntry of the soldfrom business partner, or null if not found
     */
    public PartnerListEntry getSoldFrom() {
        return getPartner(PartnerFunctionData.SOLDFROM);
    }

    /**
     * Convenience method to Get the soldfrom business partner, or null, if not available
     *
     * @return PartnerListEntry of the soldfrom business partner, or null if not found
     */
    public PartnerListEntryData getSoldFromData() {
        return (PartnerListEntryData) getSoldFrom();
    }
	/**
	 * Convenience method to Get the payer business partner, or null, if not available
	 *
	 * @return PartnerListEntry of the payer business partner, or null if not found
	 */
	public PartnerListEntry getPayer() {
		return getPartner(PartnerFunctionData.PAYER);
	}

	/**
	 * Convenience method to Get the payer business partner, or null, if not available
	 *
	 * @return PartnerListEntryData of the payer business partner, or null if not found
	 */
	public PartnerListEntryData getPayerData() {
		return (PartnerListEntryData) getPayer();
	}
    /**
     * Set the business partner for the given partner function (role). <br>
     *
     * @param bpRole Name of the partner function
     * @param PartnerListEntry the PartnerListEntry for the business partner
     */
    public void setPartner(String bpRole, PartnerListEntry entry) {
        partnerList.put(bpRole, entry);
    }

    /**
     * Set the business partner for the given function (role). <br>
     *
     * @param bpRole Name of the partner function
     * @param PartnerListEntryData the PartnerListEntry for the business partner
     */
    public void setPartnerData(String bpRole, PartnerListEntryData entry) {
        partnerList.put(bpRole, entry);
    }

    /**
     * Convenience method to set the soldto business partner
     *
     * @param PartnerListEntry of the soldTo business partner
     */
    public void setSoldTo(PartnerListEntry entry) {
        setPartner(PartnerFunctionData.SOLDTO, entry);
    }

        /**
     * Convenience method to set the soldto business partner
     *
     * @param PartnerListEntryData of the soldTo business partner
     */
    public void setSoldToData(PartnerListEntryData entry) {
        setSoldTo((PartnerListEntry) entry);
    }

    /**
     * Convenience method to set the contact business partner
     *
     * @param PartnerListEntry of the contact business partner
     */
    public void setContact(PartnerListEntry entry) {
        setPartner(PartnerFunctionData.CONTACT, entry);
    }

    /**
     * Convenience method to set the contact business partner
     *
     * @param PartnerListEntryData of the contact business partner
     */
    public void setContactData(PartnerListEntryData entry) {
        setContact((PartnerListEntry) entry);
    }

    /**
     * Convenience method to set the soldfrom business partner
     *
     * @param PartnerListEntry of the soldfrom business partner
     */
    public void setSoldFrom(PartnerListEntry entry) {
        setPartner(PartnerFunctionData.SOLDFROM, entry);
    }

    /**
     * Convenience method to set the soldfrom business partner
     *
     * @param PartnerListEntryData of the soldfrom business partner
     */
    public void setSoldFromData(PartnerListEntryData entry) {
         setSoldFrom((PartnerListEntry) entry);
    }

    /**
     * Convenience method to set the reseller business partner
     *
     * @param PartnerListEntry of the reseller business partner
     */
    public void setReseller(PartnerListEntry entry) {
        setPartner(PartnerFunctionData.RESELLER, entry);
    }

    /**
     * Convenience method to set the reseller business partner
     *
     * @param PartnerListEntryData of the reseller business partner
     */
    public void setResellerData(PartnerListEntryData entry) {
        setReseller((PartnerListEntry) entry);
    }

	/**
	  * Convenience method to set the payer business partner
	  *
	  * @param PartnerListEntry of the soldfrom business partner
	  */
	 public void setPayer(PartnerListEntry entry) {
		 setPartner(PartnerFunctionData.PAYER, entry);
	 }

	 /**
	  * Convenience method to set the payer business partner
	  *
	  * @param PartnerListEntryData of the payer business partner
	  */
	 public void setPayerData(PartnerListEntryData entry) {
		  setPayer((PartnerListEntry) entry);
	 }

    /**
     * remove the business partner for the given partner function (role). <br>
     *
     * @param bpRole Name of the partner function
     */
    public void removePartner(String bpRole) {
        partnerList.remove(bpRole);
    }

    /**
     * remove the business partner for the given partner function (role). <br>
     *
     * @param bpRole Name of the partner function
     */
    public void removePartnerData(String bpRole) {
        removePartner(bpRole);
    }

    /**
     * Remove the business partner with the given techKey. <br> 
     *
     * @param bpTechKey TechKey of the businesspartner, to delete
     */
    public void removePartner(TechKey bpTechKey) {
        Iterator entries = partnerList.entrySet().iterator();
        while (entries.hasNext()) {
           PartnerListEntry entry = (PartnerListEntry) ((Map.Entry) entries.next()).getValue();
           if (entry.getPartnerTechKey().equals(bpTechKey)) {
              entries.remove();
           }
        }
    }

    /**
     * remove the business partner with the given techKey. <br>
     *
     * @param bpTechKey TechKey of the businesspartner, to delete
     */
    public void removePartnerData(TechKey bpTechKey) {
        removePartner(bpTechKey);
    }

    /**
     * clear the business partner list
     */
    public void clearList() {
        partnerList.clear();
    }

    /**
     * Returns a string representation of the object.
     *
     * @return object as string
     */
    public String toString() {
        return  partnerList.toString();
    }

    /**
     * get the business partner list
     *
     * @return MashMap list of the business partners
     */
    public Map getList(){
        return partnerList;
    }

    /**
     * set the business partner list
     *
     * @param  HashMap new list of the business partners
     */
    public void setList(Map partnerList) {
        this.partnerList.clear();
        this.partnerList.putAll(partnerList);
    }

    /**
     * Performs a deep copy of this object.
     * Because of the fact that
     * all fields of this object consist of immutable objects like
     * <code>String</code> and <code>TechKey</code> or primitive types
     * the shallow copy is identical with a deep copy. For the
     * <code>payment</code> property an explicit copy operation is performed
     * because this object is the only mutable one.
     *
     * @return shallow (deep-like) copy of this object
     */
    public Object clone() {
        PartnerList myClone = new PartnerList();
        myClone.partnerList = (HashMap) partnerList.clone();
        return myClone;

    }


    /**
     * Returns en iterator over the Entrys of Partner List in form of Map.Entry
     * The keys are the partner function and the values are <code>ParterListEntry</code>
     * objects.
     *
     * @return iterator over the partners and their parternfunctions
     *
     */
    public Iterator iterator( ) {

        Set partnerSet = partnerList.entrySet();

        return partnerSet.iterator();

    }




}
