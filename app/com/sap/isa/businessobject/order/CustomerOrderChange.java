/*****************************************************************************
    Class:        CustomerOrderChange
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:
    Created:      14.10.2002
    Version:      1.0

    $Revision: #11 $
    $Date: 2002/08/05 $
*****************************************************************************/

package com.sap.isa.businessobject.order;


/**
 * Decorator for the customer order to provide functionality only available in special
 * situations. This class acts as a decorator for the order and offers methods
 * that can only be applied when the order is in the state of being changed.
 *
 * @see com.sap.isa.businessobject.order.CustomerOrder
 *
 * @author SAP
 * @version 1.0
 *
 */
public class CustomerOrderChange extends OrderChange {

    /**
     * Constructor for CustomerOrderChange.
     */
    public CustomerOrderChange() {
        super();
    }

    /**
     * Constructor for CustomerOrderChange.
     * @param order
     */
    public CustomerOrderChange(CustomerOrder customerOrder) {
        super(customerOrder);
    }



}
