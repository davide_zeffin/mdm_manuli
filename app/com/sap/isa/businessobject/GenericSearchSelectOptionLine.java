/*****************************************************************************
    Class:        GenericSearchSelectOptionLine
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.

    $Revision: #1 $
    $DateTime: 2004/01/27 08:20:30 $ (Last changed)
    $Change: 167408 $ (changelist)
    
*****************************************************************************/
package com.sap.isa.businessobject;

import com.sap.isa.backend.boi.appbase.GenericSearchSelectOptionLineData;

/**
 * One Single select option line 
 * <p>
 * Part of the generic search framework.
 * </p>
 *
 * @author SAP AG
 * @version 1.0
 *
 */

public class GenericSearchSelectOptionLine 
    implements GenericSearchSelectOptionLineData {
	
	protected int    handle = 0;
	protected String select_param = "";
	protected String entitytype = "";
	protected String param_function = "";
	protected String param_func_type = "";
	protected String token = "";
	protected String sign = "";
	protected String option = "";
	protected String low = "";
	protected String high = "";
	
    /**
     * Constructor with all attributes
     */
    public GenericSearchSelectOptionLine(
               int    handle,
               String select_param,
               String entitytype,
               String param_function,
               String param_func_type,
               String token,
               String sign,
               String option,
               String low,
               String high ) {
        this.handle           = handle;
        this.select_param     = select_param;
        this.entitytype       = entitytype;
        this.param_function   = param_function;
        this.param_func_type  = param_func_type;
        this.token            = token;
        this.sign             = sign;
        this.option           = option;
        this.low              = low;
        this.high             = high; 
    }

	/**
	 * Returns the select option Entitytype
	 * @return Entitytype
	 */
	public String getEntitytype() {
		return entitytype;
	}

	/**
	 * Returns the select option Handle
	 * @return Handle
	 */
	public int getHandle() {
		return handle;
	}

	/**
	 * Returns the select option High Value
	 * @return High Value
	 */
	public String getHigh() {
		return high;
	}

	/**
	 * Returns the select option Low Value
	 * @return Low Value
	 */
	public String getLow() {
		return low;
	}

	/**
	 * Returns the select option Range Option 
	 * (i.e. <code>EQ</code> equals, <code>BT</code> between)
	 * @return Range Option
	 */
	public String getOption() {
		return option;
	}

	/**
	 * Returns the select option Parameter Function Type
	 * @return Parameter function type
	 */
	public String getParam_func_type() {
		return param_func_type;
	}

	/**	 
	 * Returns the select option Parameter Function
	 * @return Parameter Function
	 */
	public String getParam_function() {
		return param_function;
	}

	/**
	 * Returns the select option Selection Parameter
	 * @return Select parameter
	 */
	public String getSelect_param() {
		return select_param;
	}

	/**
	 * Returns the select option Range sign
	 * (i.e. <code>I</code>ncluded, <code>E</code>xcluded)
	 * @return Range Sign
	 */
	public String getSign() {
		return sign;
	}

	/**
	 * Returns the select option Token
	 * @return Token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * Sets the select option field Entitytype
	 * @param string
	 */
	public void setEntitytype(String string) {
		entitytype = string;
	}

	/**
	 * Sets the select option field Handle
	 * @param intVal
	 */
	public void setHandle(int intVal) {
		handle = intVal;
	}

	/**
	 * Sets the select option field High
	 * @param string
	 */
	public void setHigh(String string) {
		high = string;
	}

	/**
	 * Sets the select option field Low
	 * @param string
	 */
	public void setLow(String string) {
		low = string;
	}

	/**
	 * Sets the select option field Option
	 * (i.e. <code>EQ</code> equals, <code>BT</code> between)
	 * @param string
	 */
	public void setOption(String string) {
		option = string;
	}

	/**
	 * Sets the select option field Parameter function type (param func type)
	 * @param string
	 */
	public void setParam_func_type(String string) {
		param_func_type = string;
	}

	/**
	 * Sets the select option field Parameter function
	 * @param string
	 */
	public void setParam_function(String string) {
		param_function = string;
	}

	/**
	 * Sets the select option field Select Parameter (select param)
	 * @param string
	 */
	public void setSelect_param(String string) {
		select_param = string;
	}

	/**
	 * Sets the select option field Sign
	 * (i.e. <code>I</code>ncluded, <code>E</code>xcluded)
	 * @param string
	 */
	public void setSign(String string) {
		sign = string;
	}

	/**
	 * Sets the select option field Token
	 * @param string
	 */
	public void setToken(String string) {
		token = string;
	}

    /**
     * To string method of this object
     * @return String representing this object
     */
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("\nHandle: " + handle + ", ");
        sb.append("Select Param: " + select_param + ", ");
        sb.append("Entitytype: " + entitytype + ", ");
        sb.append("Param Func: " + param_function + ", ");
        sb.append("Param Func.type: " + param_func_type + ", ");
        sb.append("Token: " + token + ", ");
        sb.append("Sign: " + sign + ", ");
        sb.append("Option: " + option + ", ");
        sb.append("Low: " + low + ", ");
        sb.append("High: " + high);
        
        return sb.toString();
    }
}
