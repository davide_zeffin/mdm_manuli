package com.sap.isa.businessobject;

import com.sap.isa.businesspartner.businessobject.BuPaManagerAware;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.user.businessobject.IsaUserBase;
import com.sap.isa.user.businessobject.IsaUserBaseAware;
import com.sap.isa.user.businessobject.UserBase;
import com.sap.isa.user.businessobject.UserBaseAware;


/**
 * The purpose of this extended class is to use the extended 
 * WebCatInfoModifiable instead of WebCatInfo as business object class.
 * This is needed for the shared manufacturer catalog, as it allows 
 * modification and enhancement of the catalog as defined there.
 * In the usual catalog browsing scenarios, allways use the
 * CatalogBusinessObjectManager instead.
 */
public class IsaUserBusinessObjectManager 
  extends GenericBusinessObjectManager implements IsaUserBaseAware, UserBaseAware, BuPaManagerAware {


	/**
	 * Constant containing the name of this BOM
	 */
	public static final String ISAUSER_BOM = "ISAUSER-BOM";


	/**
	 * Creates a new user object. If such an object was already created, a
	 * reference to the old object is returned and no new object is created.
	 *
	 * @return referenc to a newly created or already existend user object
	 */
	public synchronized IsaUserBase createUser() {
		return (IsaUserBase) createBusinessObject(IsaUserBase.class);
	}

	/**
	 * Returns a reference to an existing user object.
	 *
	 * @return reference to user object or null if no object is present
	 */
	public IsaUserBase getUser() {
	  return (IsaUserBase) getBusinessObject(IsaUserBase.class);
	}

	/**
	 * Release the references to created user object.
	 */
	public synchronized void releaseUser() {
		releaseBusinessObject(IsaUserBase.class);
	}


	/**
	 * Creates a new BusinessPartnerManager object. If such an object was already created, a
	 * reference to the old object is returned and no new object is created.
	 *
	 * @return reference to a newly created or already existing object
	 */
	public synchronized BusinessPartnerManager createBUPAManager() {
		return (BusinessPartnerManager) createBusinessObject(BusinessPartnerManager.class);
	}
	
	/**
	 * Returns a reference to an existing BusinessPartnerManager object.
	 *
	 * @return reference to object
	 */
	public BusinessPartnerManager getBUPAManager() {
		return (BusinessPartnerManager) getBusinessObject(BusinessPartnerManager.class);
	}
	
	
	/**
	 * Release the references to created BusinessPartnerManager object.
	 */
	public synchronized void releaseBUPAManager() {
		releaseBusinessObject(BusinessPartnerManager.class);
	}

	/**
	 * Creates {@link com.sap.isa.user.businessobject.IsaUserBase} object. 
	 * 
	 * @return IsaUserBase created {@link com.sap.isa.user.businessobject.IsaUserBase} object
	 */
	public IsaUserBase createIsaUserBase() {

		return createUser();
	}
	

	/**
	 * Returns a reference to an existing user object.
	 *
	 * @return reference to user object or null if no object is present
	 */
	public UserBase getUserBase() {
		return getUser();
	}


	/**
	 * Creates a new user object. If such an object was already created, a
	 * reference to the old object is returned and no new object is created.
	 *
	 * @return referenc to a newly created or already existend user object
	 */
	public UserBase createUserBase() {
		return createUser();
	}	

  /**
   * Constructor for SharedCatalogBusinessObjectManager.
   */
  public IsaUserBusinessObjectManager()
  {
    super();
  }

}
