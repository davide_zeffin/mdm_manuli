/*****************************************************************************
    Interface:    ItemDetailRequest
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Created:      09.04.2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/06/26 $
*****************************************************************************/

package com.sap.isa.businessobject.webcatalog;

/**
 * Interface to transfer data when requesting item detail information from
 * web catalog
 */
public interface ItemDetailRequest {

  /**
   * The ID of the product to be requested
   */
  String getProductID();

  /**
   * Reference to IPC-Item containing configuration
   */
  Object getConfigurationItem();

//  /**
//   * ID/GUID of the contract this item refers to, null if none.
//   * Needed for contract info in product details.
//   */
//  String getContractID();
//
//  /**
//   * ID/GUID of the contract item this item refers to, null if none.
//   * Needed for contract info in product details.
//   */
//  String getContractItemID();

  /**
   * Key for catalog this item was taken from
   */
  String getCatalogID();

  /**
   * Key for catalog area this item was taken from
   */
  String getAreaID();

  String getDetailScenario();
}