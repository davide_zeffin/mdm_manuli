package com.sap.isa.businessobject.webcatalog.views;

import com.sap.isa.backend.BackendTypeConstants;
import com.sap.isa.backend.boi.webcatalog.views.CatalogReferenceData;
import com.sap.isa.backend.boi.webcatalog.views.ViewBackend;
import com.sap.isa.backend.boi.webcatalog.views.ViewReferenceData;
import com.sap.isa.backend.boi.webcatalog.views.ViewRelevant;
import com.sap.isa.catalog.webcatalog.WebCatBusinessObjectBase;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.core.logging.IsaLocation;


/**
 * BusinessObject view. This object is used in the maintenance of the so called
 * collaborative views.
 * The view object represents one view which belongs to one distinc catalog.
 */
public class View extends     WebCatBusinessObjectBase
                  implements  BackendAware
{

  protected BackendObjectManager bem;
  protected ViewBackend viewBackend;
  protected ViewReferenceData viewReference;
  private static IsaLocation log =
      IsaLocation.getInstance(View.class.getName());

  public final static String AUTH_CVIEW_LOGIN   = "loginCView";
  public final static String AUTH_CVIEW_CHANGE  = "changeCView";
  public final static String AUTH_CVIEW_CREATE  = "createCView";
  public final static String AUTH_CVIEW_DELETE  = "deleteCView";
  public final static String AUTH_CVIEW_PARTNER = "assignPartnerCView";
  public final static String AUTH_SHRDCAT_ADDITEM = "addItemsShrdcat";

  public View()
  {
    super();
  }

  /**
   * @deprecated please provide also the logon language by using inti(String[], String) instead.
   * @param owner
   * @throws BackendException
   */
  public void init(String[] owner) throws BackendException
  {
    getViewBackend().setOwner(owner);
  }

  /**
   * Init the view Object with the owner and language information
   * @param owner
   * @param sapLanguage
   * @throws BackendException
   */
  public void init(String[] owner, String sapLanguage) throws BackendException
  {
    getViewBackend().setOwner(owner);
    getViewBackend().setLanguage(sapLanguage);
  }
  
  public CatalogReferenceData[] getMaintainableCatalogs() throws BackendException
  {
    return getViewBackend().getMaintainableCatalogs();
  }

  public void setCatalogId(String catalogId) throws BackendException
  {
    getViewBackend().setCatalog(catalogId);
  }

  public ViewReferenceData[] getViews()
  {
    return getViewBackend().getViews(null);
  }

  public void createView() throws BackendException
  {
    getViewBackend().createView();
  }

  public void loadView(String viewId) throws BackendException
  {
    getViewBackend().loadView(viewId);
  }

  public void deleteView(String viewId) throws BackendException
  {
    getViewBackend().deleteView(viewId);
  }

  public ViewReferenceData getViewData()
  {
    return getViewBackend().getViewData();
  }

  public void addObject(ViewRelevant obj)
  {
    getViewBackend().addObject(obj);
  }

  public void removeObject(ViewRelevant obj)
  {
    getViewBackend().removeObject(obj);
  }

  public boolean isObjectInView(ViewRelevant obj)
  {
    return getViewBackend().isObjectInView(obj);
  }

  public ContactAssignment[] getContactAssignments()
  {
    return getViewBackend().getContactsAssigned();
  }

  public ContactAssignment[] checkContactAssignments(String[] contacts)
  {
    ContactAssignment[] assignments = new ContactAssignment[contacts.length];
    for (int i=0; i<contacts.length; i++)
    {
      assignments[i] = new ContactAssignment();
      assignments[i].setContact(contacts[i]);
      assignments[i].setIncluded(getViewBackend().isContactAssigned(contacts[i]));
    }
    return assignments;
  }

  public void addContactAssignment(String contact)
  {
    getViewBackend().addContactAssignment(contact);
  }

  public void removeContactAssignment(String contact)
  {
    getViewBackend().removeContactAssignment(contact);
  }

  public boolean isContactAssigned(String contact)
  {
    return getViewBackend().isContactAssigned(contact);
  }

  public void saveView() throws BackendException
  {
    getViewBackend().saveView();
  }

  /**
   * Check authorization to do a distinct process
   */
  public boolean checkAuthorization(String auth)
  {
    boolean allowed = getViewBackend().checkAuthorization(auth);
    if (log.isDebugEnabled())
      log.debug("check authorization "+auth+": "+allowed);
    return allowed;
  }

  /**
   * Sets the BackendObjectManager for the attribute set. This method is used
   * by the object manager to allow the user object interaction with
   * the backend logic. This method is normally not called
   * by classes other than BusinessObjectManager.
   *
   * @param bem BackendObjectManager to be used
   */
  public void setBackendObjectManager(BackendObjectManager bem) {
      this.bem = bem;
  }

  /**
   * Helper method to determine the backend view, if not done already.
   */
  protected ViewBackend getViewBackend()
  {
      if (viewBackend == null)
      {
        try
        {
          viewBackend =
              (ViewBackend) bem.createBackendBusinessObject(
                  BackendTypeConstants.BO_TYPE_VIEW,
                  viewReference);
        }
        catch (BackendException be)
        {
          log.error("system.eai.exception",be);
          viewBackend = null;
        }
      }
      return viewBackend;
  }

  public void release()
  {
    getViewBackend().destroyBackendObject();
  }

  public String toString()
  {
    return this.getClass().getName()+" -> "+getViewData().getViewId();
  }
}