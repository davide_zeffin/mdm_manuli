package com.sap.isa.businessobject.webcatalog.views;

/**
 * Title:        Catalog in ESALES_base dev branch
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public class ContactAssignment {

  private String contact;
  private boolean included;

  public ContactAssignment() {
  }

  public String getContact() {
    return contact;
  }

  public void setContact(String contact) {
    this.contact = contact;
  }

  public void setIncluded(boolean included) {
    this.included = included;
  }

  public boolean isIncluded() {
    return included;
  }

}