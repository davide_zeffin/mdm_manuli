/*****************************************************************************
	Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
	Created:      04 April 2001

	$Revision: #2 $
	$Date: 2001/07/25 $
*****************************************************************************/
package com.sap.isa.businessobject.webcatalog.exchproducts;

import com.sap.isa.backend.boi.webcatalog.exchproducts.IExchProductResult;
import com.sap.isa.businessobject.BusinessObjectBase;

/**
 * Represents a reference to ExchangeProduct information
 */
public class ExchProductResult extends BusinessObjectBase
							   implements IExchProductResult{
	private int attrition_period;
	private int core_period;
	private String attrition_period_unit ;
	private String core_period_unit ;
	
	/**
	* Retrieves the AttritionPeriod of the referenced exchproduct result
	*/
	public int getAttritionPeriod() {
		   return attrition_period;
	   }
	   
	/**
	 * Retrieves the CorePeriod of the referenced exchproduct result
	 */
	 public int getCorePeriod() {
	       return core_period;
	   }
	   
	/**
	 * Sets the AttritionPeriod of the referenced exchproduct result
	 */
	 public void setAttritionPeriod(int attrition_period) {
		  this.attrition_period = attrition_period;
	  } 
	
	 /**
	 * Sets the CorePeriod of the referenced exchproduct result
	 */
	 public void setCorePeriod(int core_period) {
		   this.core_period = core_period;
	  } 
	 
	  /**
	   * Retrieves the CorePeriodUnit of the referenced CorePeriod.
	   */
	   
	  public String getCorePeriodUnit()
	  {
	  	return core_period_unit;
	  }

	   /**
		* Sets the CorePeriodUnit of the referenced exchproduct result.
		*/
	  public void setCorePeriodUnit(String core_period_unit)
	  {
	  	this.core_period_unit = core_period_unit;	
	  }
	  
	
	   /**
		* Retrieves the AttritionPeriodUnit of the referenced AttritionPeriod.
		*/
	   public String getAttritionPeriodUnit()
	   {
	   	return attrition_period_unit;
	   }

	   /**
		* Sets the AttritionPeriodUnit of the referenced AttritionPeriod.
		*/
	   public void setAttritionPeriodUnit(String attrition_period_uint)
	   {
	   	   this.attrition_period_unit = attrition_period_uint;
	   }
	   
	   /**
	    * to display the output
	    */
	   public String toString()
	   {
	   	return ("the attrition period is :"  + attrition_period +"\n" +"the core period is "+core_period +"\n" + "the attrition period unit is :"  + attrition_period_unit +"\n" +"the core period unit is "+core_period_unit);
	   }
	   
	
}