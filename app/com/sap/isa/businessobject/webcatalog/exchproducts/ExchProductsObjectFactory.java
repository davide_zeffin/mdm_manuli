/*****************************************************************************
	Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
	Created:      27 April 2004

	$Revision: #1 $
	$Author V.Manjunath Harish
*****************************************************************************/
package com.sap.isa.businessobject.webcatalog.exchproducts;

import com.sap.isa.backend.boi.webcatalog.exchproducts.IExchProductResult;
import com.sap.isa.backend.boi.webcatalog.exchproducts.IExchProductsObjectFactoryBackend;

/**
 * Class used for the creation of iexchproduct objects within the BE.
 */
public class ExchProductsObjectFactory
	   implements IExchProductsObjectFactoryBackend {

	/**
	 * Creates a new <code>IExchProduct</code> object.
	 *
	 * @return The newly created object
	 */
	public IExchProductResult createIExchProductResult() {
		return (IExchProductResult)(new ExchProductResult());
	}

}