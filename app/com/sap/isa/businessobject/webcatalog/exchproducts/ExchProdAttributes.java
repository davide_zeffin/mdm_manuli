/*****************************************************************************
	Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
	Created:      28 April 2004

	$Revision: #1 $
	$Author:V.Manjunath Harish
*****************************************************************************/
package com.sap.isa.businessobject.webcatalog.exchproducts;

import com.sap.isa.backend.BackendTypeConstants;
import com.sap.isa.backend.boi.webcatalog.exchproducts.IExchProductsBackend;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.mw.jco.JCO;

/**
 * The ExchProdAttributes class makes (Attrition/Core period) attributes of a exchange product accessible
 * for display in the B2B Internet Sales scenario.
 */
public class ExchProdAttributes extends BusinessObjectBase
					  implements BackendAware,
								 BackendBusinessObjectParams {
	private static final ExchProductsObjectFactory
		EXCHPRODUCTS_OBJECT_FACTORY = new ExchProductsObjectFactory();
	private BackendObjectManager bem;
	private IExchProductsBackend iExchProdBackend;

	/**
	 * Constructor.
	 */
	public ExchProdAttributes() {
		super();
	}

	/**
	 * Sets the BackendObjectManager for the ExchProdAttributes set. This method is used
	 * by the object manager to allow the user object interaction with
	 * the backend logic. This method is normally not called
	 * by classes other than BusinessObjectManager.
	 *
	 * @param bem BackendObjectManager to be used
	 */
	public void setBackendObjectManager(BackendObjectManager bem) {
		this.bem = bem;
	}


	/**
	* Read the exchange product attribytes for a given item from the backend(entitlement engine).
	* @param User to read the businees partner information.
	* @param item for which exchange product to read the information.
	*/
	 public ExchProductResult readExchProductAttributes(WebCatInfo theCatalog ) throws CommunicationException {

		   ExchProductResult exchprodResult = null;

	   // read exchprodResult info from backend
	   try {
		   iExchProdBackend = getIExchProductsBackend();
		   exchprodResult = (ExchProductResult) iExchProdBackend.readExchProductResult(theCatalog);
	       }
	   catch (BackendException ex) {
//			 BusinessObjectHelper.splitException(ex);
	   log.debug("Encountered exception on ExchangeProducts process", ex);
	   exchprodResult = null;
	   }
	   catch (JCO.AbapException abapEx) {
	   log.debug("Encountered exception on ExchangeProducts process", abapEx);
	   exchprodResult = null;
	   }
	  return exchprodResult;
   }

	// get the iExchProdBackend, if necessary
	private IExchProductsBackend getIExchProductsBackend()
			throws BackendException {
		if (iExchProdBackend == null) {
			iExchProdBackend = (IExchProductsBackend)bem.createBackendBusinessObject(BackendTypeConstants.BO_TYPE_EXCHPRODUCTS,
			EXCHPRODUCTS_OBJECT_FACTORY);
		}
		return iExchProdBackend;
	}
}
