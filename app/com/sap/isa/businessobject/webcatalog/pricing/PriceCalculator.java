/*****************************************************************************
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:   Roland Huecking
  Created:  11 May 2001

  $Revision: #5 $
  $Date: 2001/08/06 $
*****************************************************************************/

package com.sap.isa.businessobject.webcatalog.pricing;

import java.util.Date;

import com.sap.isa.backend.BackendTypeConstants;
import com.sap.isa.backend.boi.webcatalog.pricing.ItemPriceRequest;
import com.sap.isa.backend.boi.webcatalog.pricing.PriceCalculatorBackend;
import com.sap.isa.backend.boi.webcatalog.pricing.PriceCalculatorInitData;
import com.sap.isa.backend.boi.webcatalog.pricing.Prices;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.businessobject.webcatalog.ItemTransferRequest;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.core.logging.IsaLocation;

/**
 * This calls implements the functionality that is necessary to calculate
 * prices for given item of the catalog which it was defined via the interface.
 *
 * @author      Roland Huecking, Georg Lenz
 * @version     1.0
 */
public class PriceCalculator extends BusinessObjectBase implements BackendAware, BackendBusinessObjectParams {

    private PriceCalculatorBackend thePriceCalcBackend;
    private PriceCalculatorInitData initData = null;
    private BackendObjectManager bem;

    private static IsaLocation theLocToLog = IsaLocation.getInstance(PriceCalculator.class.getName());

    /**
     * Default constructor which is called from the business object manager.
     * The instances has to be initialized via the <code>init(...)</code> method.
     */
    public PriceCalculator() {
    }

    public void setBackendObjectManager(BackendObjectManager bem) {
        if (bem == null)
            thePriceCalcBackend = null;
        else {
            this.bem = bem;
            try {
                thePriceCalcBackend =
                    (PriceCalculatorBackend) bem.createBackendBusinessObject(
                        BackendTypeConstants.BO_TYPE_PRICE_CALCULATOR,
                        null);
            }
            catch (BackendException ex) {
                theLocToLog.error("system.exception", new Object[] { ex.getLocalizedMessage()}, ex);
                thePriceCalcBackend = null;
                return;
            }
        }
    }

    /**
     * Initialize the backend PriceCalculator
     */
    public void init() {
        initData.setBem(bem);
        thePriceCalcBackend.setInitData(initData);
        thePriceCalcBackend.init();
    }

    public PriceCalculatorInitData createInitData() {
        return thePriceCalcBackend.createInitData();
    }

    public void setInitData(PriceCalculatorInitData initData) {
        this.initData = initData;
    }

    public PriceCalculatorInitData getInitData() {
        return initData;
    }

    /**
     * Release data. This is for IPC to be notified that the used document can
     * can be removed.
     */
    public void release() {
        thePriceCalcBackend.release();
    }

    /**
     * release data when garbage collecting
     */
    protected void finalize() throws Throwable {
        release();
        super.finalize();
    }

    public Prices getPrices(WebCatItem item) {
        // call the backend specific implementation
        return thePriceCalcBackend.getPrices(new ItemTransferRequest(item));
    }

    public Prices[] getPrices(WebCatItem[] items) {
        // call the backend specific implementation
        ItemTransferRequest[] pItems = new ItemTransferRequest[items.length];
        for (int i = 0; i < items.length; i++)
            pItems[i] = new ItemTransferRequest(items[i]);

        return thePriceCalcBackend.getPrices(pItems);
    }

    public Prices getPrices(ItemPriceRequest item) {
        return thePriceCalcBackend.getPrices(item);
    }

    public Prices[] getPrices(ItemPriceRequest[] items) {
        // call the backend specific implementation
        return thePriceCalcBackend.getPrices(items);
    }

    public void setIpcDate(Date aDate) {
        thePriceCalcBackend.setIpcDate(aDate);
    }

}