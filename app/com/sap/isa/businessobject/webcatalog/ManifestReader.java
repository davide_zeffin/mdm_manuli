package com.sap.isa.businessobject.webcatalog;

/**
 * @author: Author Company: SAP AG
 * 
 */

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Iterator;
import java.util.Map;
import java.util.jar.Attributes;
import java.util.jar.JarFile;
import java.util.jar.Manifest;

import com.sap.isa.core.logging.IsaLocation;

public class ManifestReader {

	public static void logManifest(IsaLocation log, Class theClass) {
		
		if (!log.isDebugEnabled()) return;

		String className = theClass.getName();
		className = className.replace('.','/'); 
		className+=".class";

		URL url = theClass.getClassLoader().getResource(className);
		
		String path = url.toString();
		
		if (path.indexOf("file:") == -1) {
			log.debug("We have got an unknown URL "+path+" (No \"file://\") Returning ...");
			return;
		}
		
		path = path.substring(path.indexOf("file:")+"file:".length());
		if (path.indexOf("!") == -1) {
			log.debug("We have got an unknown URL "+path+" (No \"!\") Returning ...");
			return;
		}
		
		path = path.substring(0,path.indexOf("!"));
		log.debug("The path toward: "+theClass.getName()+" is "+path);
		
		File file = new File(path);
		
		if (file == null) {
			log.debug("Although the path "+path+" looks fine, we could'n associate it with a file");
			return;
		}
		JarFile jar = null;
		try {
			jar = new JarFile(file);
		} catch (IOException ex) {
			log.debug("No JarFile can be created from the file",ex);
			return;
		}
		
		if (jar == null) {
			log.debug("No JarFile can be created from the file");
			return;
		}

		Manifest manifest = null;
		try {
			manifest = jar.getManifest();
		} catch (IOException ex) {
			log.debug("No Manifest can be found in the JarFile",ex);
			return;
		}

		if (manifest == null) {
			log.debug("No Manifest can be found in the JarFile");
			return;
		}
		
		Map map = manifest.getEntries();
		
		Iterator keys = map.keySet().iterator();
		StringBuffer buff = new StringBuffer("The content of your Manifest is: \r\n\t\t\t\t\t");
		while (keys.hasNext()) {
			String key = (String)keys.next();
			buff.append("Section name: ").append(key).append("\r\n\t\t\t\t\t");
			if (map.get(key) != null) {
				Attributes attribs = (Attributes)map.get(key);
				if (attribs.keySet() != null) {
					Iterator it = attribs.keySet().iterator();
					while (it.hasNext()) {
						Attributes.Name attrKey = (Attributes.Name)it.next();
						String value = attribs.getValue(attrKey);
						buff.append(attrKey.toString()).append(" : ").append(value).append("\r\n\t\t\t\t\t");
					}
				}
				
			}
		}
		log.debug(buff.toString());
		try {
			jar.close();
		} catch (IOException ex) {
			log.debug(ex.getMessage());
		}
	}
}
