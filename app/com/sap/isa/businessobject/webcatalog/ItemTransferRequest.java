/*****************************************************************************
    Interface:    ItemTransferRequest
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Created:      09.04.2001
    Version:      1.0

    $Revision: #7 $
    $Date: 2001/07/25 $
*****************************************************************************/

package com.sap.isa.businessobject.webcatalog;

import com.sap.isa.backend.boi.ipc.ItemConfigurationReferenceData;
import com.sap.isa.backend.boi.webcatalog.pricing.ItemPriceRequest;
import com.sap.isa.businessobject.contract.ContractReference;
import com.sap.isa.businessobject.item.BasketTransferItem;
import com.sap.isa.businessobject.item.ContractDuration;
import com.sap.isa.catalog.boi.WebCatItemData;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.catalog.webcatalog.WebCatSubItem;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.spc.remote.client.object.IPCException;

/**
 * Object to transfer item data from product catalog to shopping basket,
 * order etc. and to retrieve pricing information.
 * Internally stores the WebCatItem and transforms the method calls.
 */
public class ItemTransferRequest implements BasketTransferItem, ItemPriceRequest {

    private boolean isRedemptionItem;
    private boolean isBuyPtsItem;
	private WebCatItem item;
    private static IsaLocation log = IsaLocation.getInstance(ItemTransferRequest.class.getName());

    public ItemTransferRequest(WebCatItem item) {
        this.item = item;
    }

    public ItemTransferRequest(WebCatItemData item) {
        this.item = (WebCatItem) item;
    }
    public String getProductKey() {
        //if product variant was selected from configuration, return variant
        //instead of original product
        String productKey = null;
        if (item.getConfigItemReference() != null) {
            try {
                productKey = item.getConfigItemReference().getProductGUID();
            }
            catch (IPCException e) {
                log.error("system.exception", e);
            }
        }
        if (productKey == null)
            productKey = item.getProductID();
        return productKey;
    }

    /**
     * Returns the external configuration xml encoded with basis 64,
     * that can be embedded in html.
     */
    public String getExtConfigXmlEncodedB64() {
        return item.getExtConfigXmlEncodedB64();
    }

    /**
     * Returns the external configuration as xml.
     * If the item has no configuration, an empty string "" is returned.
     */
    public String getExtConfigXml() {
        return item.getExtConfigXml();
    }

    public String getProductId() {
        
        //if product variant was selected from configuration, return variant
        //instead of original product
        String productId = null;
        if (item.getConfigItemReference() != null) {
            try {
                productId = item.getConfigItemReference().getProductId();
            }
            catch (IPCException e) {
                log.error("system.exception", e);
            }
        }
        if (productId == null)
            productId = item.getProduct();
        return productId;
    }

    public String getProductCategory() {
        return item.getProductCategory();
    }

    public String getContractKey() {
        ContractReference contractRef = item.getContractRef();
        if (contractRef != null) {
            return contractRef.getContractKey().getIdAsString();
        }
        else {
            return null;
        }
    }

    public String getContractItemKey() {
        ContractReference contractRef = item.getContractRef();
        if (contractRef != null) {
            return contractRef.getItemKey().getIdAsString();
        }
        else {
            return null;
        }
    }

    public String getContractId() {
        ContractReference contractRef = item.getContractRef();
        if (contractRef != null) {
            return contractRef.getContractId();
        }
        else {
            return null;
        }
    }

    public String getContractItemId() {
        ContractReference contractRef = item.getContractRef();
        if (contractRef != null) {
            return contractRef.getItemID();
        }
        else {
            return null;
        }
    }

    public String getContractConfigFilter() {
        ContractReference contractRef = item.getContractRef();
        if (contractRef != null) {
            return contractRef.getConfigFilter();
        }
        else {
            return null;
        }
    }

    public ItemConfigurationReferenceData getConfigurationReference() {
        ContractReference contractRef = item.getContractRef();
        if (contractRef != null) {
            return contractRef.getConfigurationReference();
        }
        else {
            return null;
        }
    }

    public Object getBasketItemToReplace() {
        return null;
    }

    public String getCatalog() {
        return item.getCatalogID();
    }

    public WebCatInfo getCatalogInfo() {
        return item.getItemKey().getParentCatalog();
    }

    public String getCatalogCategory() {
        return item.getAreaLangIndepName();
    }

    public String getCustomerProductNumber() {
        return null;
    }

    public String getVariant() {
        return null;
    }

    public String getReqDeliveryDate() {
        return null;
    }

    public Object getConfigurationItem() {
        return item.getConfigItemReference();
    }

    public String getQuantity() {
        return item.getQuantity();
    }

    public String getUnit() {
        return item.getUnit();
    }

    public WebCatItem getCatItem() {
        return item;
    }

    public String toString() {

        StringBuffer buf = new StringBuffer("ItemTransferRequest:");
        buf.append("Product=").append(getProductId());
        buf.append(",ProductKey=").append(getProductKey());
        buf.append(",ProductCategory=").append(getProductCategory());
        buf.append(",Quantity=").append(getQuantity());
        buf.append(",Unit=").append(getUnit());
        buf.append(",Catalog=").append(getCatalog());
        buf.append(",CatalogCategory=").append(getCatalogCategory());
        buf.append(",CatalogInfo=").append(getCatalogInfo());
        buf.append(",CatalogItem=").append(getCatItem());
        buf.append(",ConfigItem=").append(getConfigurationItem());
        buf.append(",ConfigRef=").append(getConfigurationReference());
        buf.append(",Variant=").append(getVariant());
        buf.append(",ContractKey=").append(getContractKey());
        buf.append(",ContractItemKey=").append(getContractItemKey());
        buf.append(",ContractFilter=").append(getContractConfigFilter());

        return buf.toString();
    }

    /**
     * return the desccription of the item
     * @return description
     */
    public String getDescription() {
        return item.getDescription();
    }

    /**
         * Returns the <code>TeckKey</code> of the parent of this TransferItem.
         * null if no parent.
         * 
         * @return parentTechKey
         */
    public String getParentTechKey() {
        return item.getParentTechKey().getIdAsString();
    }

    /**
     * Returns the <code>TeckKey</code> of this TransferItem.
     * 
     * @return parentTechKey
     */
    public String getTechKey() {
        return item.getTechKey().getIdAsString();
    }

    /**
     * returns 
     * true if the package is exploded
     * false if the package is not exploded or if the basketTransferItem is not a package
     * 
     * @return isExploded
     */
    public boolean isExploded() {
        return item.isExploded();
    }

    /**
     * if the basketTransferItem is a package, returns true is this item is main product, false otherwise
     * 
     * @return isMain
     */
    public boolean isMainItem() {
        return item.isMainItem();
    }

    /**
     * if the basketTransferItem is a package component, returns true if the type of interlinkage is dependent component
     * @return
     */
    public boolean isDependentComponent() {
        return item.isDependentComponent();
    }

    /**
     * if the basketTransferItem is a package component, returns true if the type of interlinkage is sale component
     * @return
     */
    public boolean isSalesComponent() {
        return item.isSalesComponent();
    }

    /**
     * if the basketTransferItem is a package component, returns true if the type of interlinkage is rate plan combination
     * @return
     */
    public boolean isRatePlanCombination() {
        return item.isRatePlanCombination();
    }

    /**
     * if the basketTransferItem is a package component, returns the author flag
     * @return
     */
    public String getSCSelection() {
        log.entering("getSCSelection()");
        String response = ((WebCatSubItem) item).getAuthor();
        log.debug("returned value:" + response);
        return response;
    }

    /**
     * if the basketTransferItem is a package component, returns the group
     * @return
     */
    public String getGroup() {
        log.entering("getGroup()");
        String response = ((WebCatSubItem) item).getGroupKey();
        log.debug("returned value:" + response);
        return response;
    }

    /**
     * returns the selected contract duration
     * 
     * @return ContractDuration, the selected contract duration, null if there is no default contract duration  
     */
    public ContractDuration getSelectedContractDuration() {
        log.entering("getSelectedContractDuration()");
        ContractDuration response = item.getSelectedContractDuration();
        log.debug("returned value:" + response);
        return response;
    }

    /**
     * Returns the information, if this <code>WebCatSubItem</code> is a selected by the 
     * solution configurator.
     * The Method implements ProductBaseData.
     * 
     * @return <code>true</code>, if the item is selected by the solution configurator
     *         <code>false</code>, if not
     * 
     */
    public boolean isScSelected() {
        log.entering("isScSelected");

        boolean response = item.isScSelected();
        log.debug("returned value:" + response);
        return response;
    }

    /**
     * Returns the scDocumentGuid
     * 
     * @return TechKey scDocumentGuid, the scDocumentGuid
     */
    public TechKey getScDocumentGuid() {

        log.entering("getScDocumentGuid()");

        TechKey scDocumentGuid = null;

        if (item != null && item.getSolutionConfigurator() != null) {
            scDocumentGuid = item.getSolutionConfigurator().getScDocGuid();
        }
        else {
            if (log.isDebugEnabled()) {
                log.debug("item is null, or solutionConfiguartor is null: " + item);
            }
        }

        log.exiting();

        return scDocumentGuid;
    }

	/* (non-Javadoc)
	 * @see com.sap.isa.businessobject.item.BasketTransferItem#isPtsItem()
	 */
	public boolean isPtsItem() {
		return isRedemptionItem;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.businessobject.item.BasketTransferItem#setPtsItem(boolean)
	 */
	public void setPtsItem(boolean isRedemption) {
		isRedemptionItem = isRedemption;
	}
	/**
	 * @return
	 */
	public boolean isBuyPtsItem() {
		return isBuyPtsItem;
	}

	/**
	 * @param b
	 */
	public void setBuyPtsItem(boolean b) {
		isBuyPtsItem = b;
	}

}
