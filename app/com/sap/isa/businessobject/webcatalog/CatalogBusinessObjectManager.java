/*****************************************************************************
    Class:        CatalogBusinessObjectManager
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Created:      23.07.2001
    Version:      1.0

    $Revision: #4 $
    $Date: 2001/07/25 $
*****************************************************************************/

package com.sap.isa.businessobject.webcatalog;

import java.util.Locale;

import com.sap.isa.businessobject.IsaClient;
import com.sap.isa.businessobject.IsaServer;
import com.sap.isa.businessobject.webcatalog.atp.Atp;
import com.sap.isa.businessobject.webcatalog.contract.ContractReferenceReader;
import com.sap.isa.businessobject.webcatalog.exchproducts.ExchProdAttributes;
import com.sap.isa.businessobject.webcatalog.pricing.PriceCalculator;
import com.sap.isa.businessobject.webcatalog.views.View;
import com.sap.isa.catalog.boi.IClient;
import com.sap.isa.catalog.boi.IServer;
import com.sap.isa.catalog.webcatalog.WebCatBusinessObjectBase;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.businessobject.event.BusinessObjectCreationEvent;
import com.sap.isa.core.businessobject.event.BusinessObjectRemovalEvent;
import com.sap.isa.core.businessobject.management.BusinessObjectManagerBase;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.core.init.InitializationEnvironment;
import com.sap.isa.core.logging.IsaLocation;

/**
 * CatalogBusinessObjectManager is the catalog specific business object manager.
 * Central point to create and access Objects of the business logic. After
 * a request for a specific object, BusinessObjectManager checks if there is already
 * an object created. If so, a reference to this object is returned, otherwise
 * a new object is created and a reference to the new object is returned.
 * The reason for this class is, that the construction process of the businnes
 * logic objects may be quite complicated (because of some contextual
 * information) and most objects should only exist once per session.
 * To achieve this the <code>BusinessObjectManager</code>
 * is used. If you store a reference to this
 * object (for examle in a session context) you can get access to all other
 * business obejects using it. <br>
 * If you want to extend an existing business object, you might extend this class
 * and overwrite the new&lt;BusinessObject&gt;() method. In this case all 
 * standard beahviour like catalog browsing will continue without further
 * modification, you only need to cast from the class used here to your extended 
 * class to be able to access the extensions where they are explicitly needed.
 * @version 1.0
 */
public class CatalogBusinessObjectManager
        extends BusinessObjectManagerBase
        implements BackendAware {

    /*
     * Name of this BOM
    */
    public static final String CATALOG_BOM = "CATALOG-BOM";

    protected BackendObjectManager bem;

    // References to the business objects
    protected WebCatInfo catalog=null;
    protected PriceCalculator priceCalc=null;
    protected Atp atp=null;
    protected ExchProdAttributes exchprodAttrib=null; 
    protected ContractReferenceReader contractRefReader=null;
    protected View view = null;
    
    static final private IsaLocation log = IsaLocation.getInstance(CatalogBusinessObjectManager.class.getName());

    /**
     * Create a new instance of the object.
     */
    public CatalogBusinessObjectManager() {
      super();
      log.debug("CatalogBusinessObjectManager created.");
    }

    /**
     * Creates a new catalog info object. If such an object was already created, a
     * reference to the old object is returned and no new object is created.
     *
     * The real catalog instance can be requested via the method <code>getCatalog</code>
     * of the info object.
     *
     * @param locale The locale the catalog has to work with
     * @param catalogGuid The guid that identifies the one and only catalog to use
     * @param views Tha valid views for the user/catalog-combination
     * @return reference to a newly created or already existend catalog info object
     */
    public synchronized WebCatInfo createCatalog(IClient aClient,
                                                 IServer aServer,
                                                 InitializationEnvironment environment) {
        catalog = newWebCatInfo();
        setBEM(catalog);
        try
        {
          catalog.init(aClient, aServer, environment);
          creationNotification(new BusinessObjectCreationEvent(catalog));
          log.debug("BO Catalog created");
        }
        catch (BackendException e)
        {
          log.error("catalog.notAvailable",e);
          // do not return catalog object if error occured!
          catalog = null;
        }
        return catalog;
    }

    /**
     * Creates a new catalog info object. If such an object was already created, a
     * reference to the old object is returned and no new object is created.
     *
     * The real catalog instance can be requested via the method <code>getCatalog</code>
     * of the info object.
     *
     * @param language The locale the catalog has to work with
     * @param catalogKey The guid that identifies the one and only catalog to use
     * @param views Tha valid views for the user/catalog-combination
     * @return reference to a newly created or already existend catalog info object
     */
    public synchronized WebCatInfo createCatalog(String language,String catalogKey,
                                                 String[] views,
                                                 InitializationEnvironment environment) {
        if (catalog == null) {
            catalog = newWebCatInfo();
            setBEM(catalog);

            // get some data out of the shop needed to initialize the catalog
            // create the IsaClient- and IsaServer-objects for this first
            IsaClient   isaClient   = new IsaClient();
            IsaServer   isaServer   = new IsaServer();

            // set the values in the IsaClient and -Server
            isaClient.setLocale( new Locale(language.toLowerCase(),"") );

            if ( views != null ) {
                for ( int i = 0; i < views.length; i++ ) {
                    isaClient.addView( views[i] );
                }
            }

            // the catalogGuid contains the Catalogname + the variant
            // for the IMS server.
            isaServer.setCatalogGuid( catalogKey );

            try {
                catalog.init(isaClient, isaServer, environment);
                if (log.isDebugEnabled()) log.debug("Catalog initialized");
            }
            catch (Exception ex) {
                if (log.isDebugEnabled()) log.debug("Error init catalog");
                catalog = null;
            }
            removalNotification(new BusinessObjectCreationEvent(catalog));
        }
        log.debug("BO Catalog created");
        return catalog;
    }

    /**
     * Returns a reference to an existing catalog info object.
     * The real catalog instance can be requested via the method <code>getCatalog</code>
     * of the info object.
     *
     * @return reference to catalog info object or null if no object is present
     */
    public WebCatInfo getCatalog() {
        if (log.isDebugEnabled())
        {
          if (catalog == null)
            log.debug("BO Catalog requested, but not available");
          else
            log.debug("BO Catalog requested");
        }
        return catalog;
    }

    /**
     * Release the references to created WebCatInfo object.
     */
    public synchronized void releaseCatalog() {
        if (catalog != null) {
            removalNotification(new BusinessObjectRemovalEvent(catalog));
            callbackDestroy(catalog);
            catalog = null;
            log.debug("BO Catalog released");
        }
    }

    /**
     * Creates a new atp info object. If such an object was already created, a
     * reference to the old object is returned and no new object is created.
     *
     * @return reference to a newly created or already existing atp info object
     */
    public synchronized Atp createAtp() {
        if (atp == null) {
            atp = newAtp();
            setBEM(atp);
            creationNotification(new BusinessObjectCreationEvent(atp));
        }
        log.debug("BO Atp created");
        return atp;
    }

    /**
     * Returns a reference to existing atp information.
     *
     * @return reference to atp object or null if no object is present
     */
    public Atp getAtp() {
        if (log.isDebugEnabled())
        {
          if (atp == null)
            log.debug("BO Atp requested, but not available");
          else
            log.debug("BO Atp requested");
        }
        return atp;
    }

    /**
     * Release the references to created atp object.
     */
    public synchronized void releaseAtp() {
        if (atp != null) {
            removalNotification(new BusinessObjectRemovalEvent(atp));
            callbackDestroy(atp);
            atp = null;
            log.debug("BO Atp released");
        }
    }
    
	/**
		* Creates a new exchprodattrib info object. If such an object was already created, a
		* reference to the old object is returned and no new object is created.
		*
		* @return reference to a newly created or already existing exchprodattrib info object
		*/
	   public synchronized ExchProdAttributes createExchprodAttrib() {
		   if (exchprodAttrib == null) {
			   exchprodAttrib = newExchProductAttributes();
			   setBEM(exchprodAttrib);
			   creationNotification(new BusinessObjectCreationEvent(exchprodAttrib));
		   }
		   log.debug("BO ExchProdAttributes created");
		   return exchprodAttrib;
	   }

	   /**
		* Returns a reference to existing exchprodattrib information.
		*
		* @return reference to exchprodattrib object or null if no object is present
		*/
	   public ExchProdAttributes getExchprodAttrib() {
		   if (log.isDebugEnabled())
		   {
			 if (exchprodAttrib == null)
			   log.debug("BO ExchProdAttrib requested, but not available");
			 else
			   log.debug("BO ExchProdAttrib requested");
		   }
		   return exchprodAttrib;
	   }

	   /**
		* Release the references to created exchprodattrib object.
		*/
	   public synchronized void releaseExchprodAttributes() {
		   if (exchprodAttrib != null) {
			   removalNotification(new BusinessObjectRemovalEvent(exchprodAttrib));
			   callbackDestroy(exchprodAttrib);
			exchprodAttrib = null;
			   log.debug("BO ExchProdAttrib released");
		   }
	   }

    /**
     * Creates a new price calculator object. If such an object was already created, a
     * reference to the old object is returned and no new object is created.
     *
     * @return reference to a newly created or already existing price calculator object
     */
    public synchronized PriceCalculator createPriceCalculator() {
        if (priceCalc == null) {
            priceCalc = newPriceCalculator();
            setBEM(priceCalc);
            creationNotification(new BusinessObjectCreationEvent(priceCalc));
        }
        return priceCalc;
    }

    /**
     * Returns a reference to an existing price calculator object.
     *
     * @return reference to price calculator object or null if no object is present
     */
    public PriceCalculator getPriceCalculator() {
        if (log.isDebugEnabled())
        {
          if (priceCalc == null)
            log.debug("BO PriceCalculator requested, but not available");
          else
            log.debug("BO PriceCalculator requested");
        }
        return priceCalc;
    }

    /**
     * Release the references to created PriceCalculator object.
     */
    public synchronized void releasePriceCalculator() {
        if (priceCalc != null) {
            removalNotification(new BusinessObjectRemovalEvent(priceCalc));
            priceCalc.release();
            priceCalc = null;
            log.debug("BO PriceCalculator released");
        }
    }

    /**
     * Creates a new view object. If such an object was already created, a
     * reference to the old object is returned and no new object is created.
     *
     * @return reference to a newly created or already existing view object
     */
    public synchronized View createView() {
        if (view == null) {
            view = newView();
            setBEM(view);
            creationNotification(new BusinessObjectCreationEvent(view));
        }
        return view;
    }

    /**
     * Returns a reference to an existing view object.
     *
     * @return reference to view object or null if no object is present
     */
    public View getView() {
        if (log.isDebugEnabled())
        {
          if (view == null)
            log.debug("BO View requested, but not available");
          else
            log.debug("BO View requested");
        }
        return view;
    }

    /**
     * Release the references to created PriceCalculator object.
     */
    public synchronized void releaseView() {
        if (view != null) {
            removalNotification(new BusinessObjectRemovalEvent(view));
            callbackDestroy(view);
            view = null;
            log.debug("BO View released");
        }
    }

    /**
     *
     */
    public synchronized ContractReferenceReader createContractReferenceReader() {
        if (contractRefReader == null) {
            contractRefReader = newContractReferenceReader();
            setBEM(contractRefReader);
            creationNotification(new BusinessObjectCreationEvent(contractRefReader));
        }
        return contractRefReader;
    }

    /**
     * Returns a reference to an existing price calculator object.
     *
     * @return reference to price calculator object or null if no object is present
     */
    public ContractReferenceReader getContractReferenceReader() {
        if (log.isDebugEnabled())
        {
          if (contractRefReader == null)
            log.debug("BO ContractReferenceReader requested, but not available");
          else
            log.debug("BO ContractReferenceReader requested");
        }
        return contractRefReader;
    }

    /**
     * Release the references to created PriceCalculator object.
     */
    public synchronized void releaseContractReferenceReader() {
        if (contractRefReader != null) {
            removalNotification(new BusinessObjectRemovalEvent(contractRefReader));
            callbackDestroy(contractRefReader);
            contractRefReader = null;
            log.debug("BO ContractReferenceReader released");
        }
    }



    /**
     * Utility method internally used to set the BackendObjectManager
     * only for objects that implement BackendAware.
     *
     * @see BackendAware BackendAware
     */
    private void setBEM(Object obj) {
        if ((obj instanceof BackendAware) && (obj != null)) {
            ((BackendAware) obj).setBackendObjectManager(bem);
        }
    }


    /**
     * Each BOM who requires the BackendObjectManager will be informed
     * from the MBOM about the correct one.
     *
     * @see BackendAware com.sap.core.businessobject.BackendAware
     */
    public void setBackendObjectManager(BackendObjectManager bem) {
        this.bem = bem;
    }

    /**
     * Is called from the MBOM whenever the BOM has to be released
     * (new session or logoff, or timeout)
     *
     * @see BOManager com.sap.core.businessobject.management.BOManager
     */
    public void release() {

      releaseCatalog();
      releaseView();
      releasePriceCalculator();
      releaseContractReferenceReader();
      releaseAtp();

    }

    protected void callbackDestroy(Object o) {
        if (o instanceof WebCatBusinessObjectBase) {
            ((WebCatBusinessObjectBase)o).release();
        }
    }
    
    //-------------------- Start of customer exits -----------------------------
    

    /**
     * create the business object instance without parameters.
     * This method is called in createCatalog methods.
     * If you want to extend the WebCatInfo class and use the extended class
     * in the whole scenario, you can overwrite this method and return a new
     * instance of the extended class.
     * @return WebCatInfo new instance
     */
    protected WebCatInfo newWebCatInfo()
    {
      return new WebCatInfo();
    }

    /**
     * create the business object instance without parameters.
     * This method is called in createPriceCalculator methods.
     * If you want to extend the PriceCalculator class and use the extended class
     * in the whole scenario, you can overwrite this method and return a new
     * instance of the extended class.
     * @return PriceCalculator new instance
     */
    protected PriceCalculator newPriceCalculator()
    {
      return new PriceCalculator();
    }

    /**
     * create the business object instance without parameters.
     * This method is called in createView methods.
     * If you want to extend the View class and use the extended 
     * class in the whole scenario, you can overwrite this method and return a 
     * new instance of the extended class.
     * @return View new instance
     */
    protected View newView()
    {
      return new View();
    }

    /**
     * create the business object instance without parameters.
     * This method is called in createContractReferenceReader methods.
     * If you want to extend the ContractReferenceReader class and use the extended 
     * class in the whole scenario, you can overwrite this method and return a 
     * new instance of the extended class.
     * @return ContractReferenceReader new instance
     */
    protected ContractReferenceReader newContractReferenceReader()
    {
      return new ContractReferenceReader();
    }

    /**
     * create the business object instance without parameters.
     * This method is called in createAtp methods.
     * If you want to extend the Atp class and use the extended class
     * in the whole scenario, you can overwrite this method and return a new
     * instance of the extended class.
     * @return Atp new instance
     */
    protected Atp newAtp()
    {
      return new Atp();
    }
    
	/**
	 * create the business object instance without parameters.
	 * This method is called in createExchProdAttributes methods.
	 * If you want to extend the ExchProdAttributes class and use the extended class
	 * in the whole scenario, you can overwrite this method and return a new
	 * instance of the extended class.
	 * @return ExchProdAttributes new instance
	 */
	protected ExchProdAttributes newExchProductAttributes()
	{
	  return new ExchProdAttributes();
	}
        
	
}
