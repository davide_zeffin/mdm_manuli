/*****************************************************************************
    Class:        SolutionConfigurator
    Copyright (c) 2006, SAP AG, All rights reserved.
    Author:
    Created:      27.02.2006
    Version:      1.0

    $Revision$
    $Date$
*****************************************************************************/
package com.sap.isa.businessobject.webcatalog.solutionconfigurator;

import com.sap.isa.backend.BackendTypeConstants;
import com.sap.isa.backend.boi.webcatalog.solutionconfigurator.SolutionConfiguratorBackend;
import com.sap.isa.backend.boi.webcatalog.solutionconfigurator.SolutionConfiguratorData;
import com.sap.isa.businessobject.BusinessObjectHelper;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.catalog.boi.WebCatItemData;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.core.logging.IsaLocation;

/**
 * This class is used to explode a WebCatItem for abundle or interlinkaged products, etc. .
 * Objects of these class should be stored in the soluctionConfigurator attribute of the
 * belonging WebCatItem object.
 */
public class SolutionConfigurator implements SolutionConfiguratorData, BackendAware {
    
    private static IsaLocation log = IsaLocation.getInstance(SolutionConfigurator.class.getName());

    protected WebCatItem webCatItem = null;
    protected boolean considerSubitemsInExplosion = false;
    protected BackendObjectManager bem = null;
    protected SolutionConfiguratorBackend backendService = null;
    protected TechKey scDocGuid = null;
    
    /**
     * Constructor that also sets the belonging webCatItem,
     * which should be exploded
     * 
     * @param webCatItem the belonging WebCatItem object
     */
    public SolutionConfigurator(WebCatItem webCatItem) {
        super();
        setWebCatItem(webCatItem);
    }

    /**
     * Returns the flag, if items in the WebCatSubItemList of the
     * related WebCatItemData object should be taken into account
     * for an explosion.
     * 
     * @return true if sub items should be taken into account for the explosion
     *         false otherwise
     */
    public boolean considerSubitemsInExplosion() {
        return considerSubitemsInExplosion;
    }

    /**
     * Return the related backend object. Create one if not yet present
     * 
     * @return SolutionConfiguratorBackend the related backend object
     */
    protected SolutionConfiguratorBackend getBackendService()
            throws BackendException {

        synchronized (this) {
            if (backendService == null) {
                backendService = (SolutionConfiguratorBackend)
                                  bem.createBackendBusinessObject(BackendTypeConstants.BO_TYPE_SOLUTION_CONFIGURATOR, null);
            }
        }
        
        return backendService;
    }
    
    /**
     * Returns the related WebCatItemData object
     * 
     * @return the related WebCatItemData object
     */
    public WebCatItemData getWebCatItemData() {
        return webCatItem;
    }
    
    /**
     * Sets the BackendObjectManager for the basket object. This method is used
     * by the object manager to allow the user object interaction with
     * the backend logic. This method is normally not called
     * by classes other than BusinessObjectManager.
     *
     * @param bem BackendObjectManager to be used
     */
    public void setBackendObjectManager(BackendObjectManager bem) {
        this.bem = bem;
    }
    
    /**
     * Sets the flag, if items in the WebCatSubItemList of the
     * related WebCatItemData object should be taken into account
     * for an explosion.
     * 
     * @param true if sub items should be taken into account for the explosion
     *        false otherwise
     */
    public void setConsiderSubitemsInExplosion(boolean considerSubitemsInExplosion) {
        this.considerSubitemsInExplosion =  considerSubitemsInExplosion;
    }
    
    /**
     * Sets the related WebCatItem object
     * 
     * @param the related WebCatItem object
     */
    public void setWebCatItem(WebCatItem webCatItem) {
        this.webCatItem = webCatItem;
    }

    /**
     * This methods executes an explosion on the Solution Configurators WebCatItem object, if possible. 
     * If the explosion was successful, the determined sub item are attached as WebCatSubItemList to
     * the exploded webCatItem.
     * 
     * @param boolean storeScDoc boolean flag, to indicate, if the sc document used to execute the explosion 
     *                          should be kept in the backend or not. The document must be kept, if the explosion 
     *                          tree should be copied by refrence, e.g. when the main item is added to the basket.
     * 
     * @return int value to indicate the result of the explosion
     *         SalesConfiguratorData.NOT_ELIGIBLE if the WebCatItem is not eligible for explosion or is not set
     *         SalesConfiguratorData.EXPLOSION_ERROR if the explosion failed
     *         SalesConfiguratorData.EXPLODED if the explosion succeeded succesfully
     */
    public int explode(boolean storeScDoc) throws CommunicationException {
        
        log.entering("explode(boolean storeScDoc)");
        
        if (log.isDebugEnabled()) {
            log.debug("storeScDoc = " + storeScDoc);
        }
        
        int retVal = NOT_ELIGIBLE;
        
        if (webCatItem != null && webCatItem.isRelevantForExplosion()) {
            
            try {
                log.debug("start explosion in backend");
                retVal = getBackendService().explodeInBackend(this, storeScDoc);
            }
            catch (BackendException ex) {
                BusinessObjectHelper.splitException(ex);
            }
            finally {
                log.exiting();
            }
        }
        
        if (log.isDebugEnabled()) {
            log.debug("explosion result: " + retVal);
        }
        
        log.exiting();
        
        return retVal;
    }
    
    /**
     * This methods executes an explosion on the Solution Configurators WebCatItem object, if possible. 
     * If the explosion was successful, the determined sub item are attached as WebCatSubItemList to
     * the exploded webCatItem. The underlying SC document will not be kept.
     * 
     * @return int value to indicate the result of the explosion
     *         SalesConfiguratorData.NOT_ELIGIBLE if the WebCatItem is not eligible for explosion or is not set
     *         SalesConfiguratorData.EXPLOSION_ERROR if the explosion failed
     *         SalesConfiguratorData.EXPLODED if the explosion succeeded succesfully
     */
    public int explode() throws CommunicationException {
        
        log.entering("explode()");
        
        log.exiting();
        
        return explode(false);
    }
    
    /**
     * Return the SC document Guid for the solution configurator
     * 
     * @return TechKey the SC document Guid
     */
    public TechKey getScDocGuid() {
        log.entering("getScDocGuid()");
        log.exiting();
        return scDocGuid;
    }

    /**
     * Set the SC document Guid for the solution configurator
     * 
     * @param TechKey the SC document Guid
     */
    public void setScDocGuid(TechKey scDocGuid) {
        log.entering("setScDocGuid()");
        this.scDocGuid = scDocGuid;
        log.exiting();
    }
}
