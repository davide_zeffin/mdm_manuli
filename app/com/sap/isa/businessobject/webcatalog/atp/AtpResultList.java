/*****************************************************************************
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      20 April 2001

  $Revision: #2 $
  $Date: 2001/07/25 $
*****************************************************************************/
package com.sap.isa.businessobject.webcatalog.atp;

import java.util.ArrayList;
import java.util.Iterator;

import com.sap.isa.backend.boi.webcatalog.atp.IAtpResult;
import com.sap.isa.backend.boi.webcatalog.atp.IAtpResultList;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.core.Iterable;

/**
 * Represents a list of <code>Atp</code>information
 */
public class AtpResultList
    extends BusinessObjectBase
    implements IAtpResultList, Iterable {
    private ArrayList atpResults;

    /**
     * Creates a new <code>AtpProductList</code> object.
     */
    public AtpResultList () {
        atpResults = new ArrayList();
    }

    /**
     * Adds a new <code>AtpResult</code> to the list.
     */
    public void add (AtpResult atpResult) {
        atpResults.add(atpResult);
    }

    /**
     * Adds a new <code>IAtpResult</code> object to the list.
     */
    public void add (IAtpResult atpResult) {
        if (atpResult instanceof AtpResult) {
            atpResults.add((AtpResult) atpResult);
        }
    }

    /**
     * Returns the <code>AtpResult</code> at the specified position in
     * this list.
     */
    public IAtpResult get (int index) {
        return (IAtpResult) atpResults.get(index);
    }

    /**
     * Returns the number of <code>AtpResult</code>s in this list.
     */
    public int size () {
        return atpResults.size();
    }

    /**
     * Returns true if this list contains no <code>AtpResult</code>
     */
    public boolean isEmpty () {
        return atpResults.isEmpty();
    }

    /**
     * Returns an iterator over the <code>atpResults</code> contained
     * in the list
     */
    public Iterator iterator () {
        return atpResults.iterator();
    }

    public ArrayList getResultList() {
        return atpResults;
    }

}
