/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Created:      7 April 2001

    $Revision: #2 $
    $Date: 2001/07/25 $
*****************************************************************************/
package com.sap.isa.businessobject.webcatalog.atp;

import com.sap.isa.backend.boi.webcatalog.atp.IAtpObjectFactoryBackend;
import com.sap.isa.backend.boi.webcatalog.atp.IAtpResult;
import com.sap.isa.backend.boi.webcatalog.atp.IAtpResultList;

/**
 * Class used for the creation of iatp product objects within the BE.
 */
public class AtpObjectFactory
       implements IAtpObjectFactoryBackend {

    /**
     * Creates a new <code>IAtpProduct</code> object.
     *
     * @return The newly created object
     */
    public IAtpResult createIAtpResult() {
        return (IAtpResult)(new AtpResult());
    }

    /**
     * Creates a new <code>IAtpProductList</code> object.
     *
     * @return The newly created object
     */
    public IAtpResultList createIAtpResultList() {
        return (IAtpResultList)(new AtpResultList());
    }
}