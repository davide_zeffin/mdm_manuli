/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Created:      06 April 2001

    $Revision: #5 $
    $Date: 2001/08/03 $
*****************************************************************************/
package com.sap.isa.businessobject.webcatalog.atp;

import java.util.Date;

import com.sap.isa.backend.BackendTypeConstants;
import com.sap.isa.backend.boi.webcatalog.atp.IAtpBackend;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.mw.jco.JCO;

/**
 * The Atp class makes (time/quantity) information of a user accessible
 * for display in the B2B Internet Sales scenario.
 */
public class Atp extends BusinessObjectBase
                      implements BackendAware,
                                 BackendBusinessObjectParams {
    private static final AtpObjectFactory
        ATP_OBJECT_FACTORY = new AtpObjectFactory();
    private BackendObjectManager bem;
    private IAtpBackend iAtpBackend;

    /**
     * Constructor.
     */
    public Atp() {
        super();
    }

    /**
     * Sets the BackendObjectManager for the atp set. This method is used
     * by the object manager to allow the user object interaction with
     * the backend logic. This method is normally not called
     * by classes other than BusinessObjectManager.
     *
     * @param bem BackendObjectManager to be used
     */
    public void setBackendObjectManager(BackendObjectManager bem) {
        this.bem = bem;
    }


	/**
	* Read the atp info list for a given matnr from the backend.
	* @param checkProfile ?
	* @param shop The shop the contract item list is read in.
	* @param matnr Materialnumber
	* @param uom Unit of Measurement
    * @deprecated
	*/
	 public AtpResult readAtpResult(
					 String shop,
					 WebCatItem item,
					 Date  requestedDate,
					 String cat,
					 String variant) throws CommunicationException {

		   AtpResult atpResult = null;

	   // read atpResult info from backend
	   try {
		   atpResult = (AtpResult) getIAtpBackend().readAtpResult(
				   shop,
				   item,
				   requestedDate,
				   cat,
				   variant);
	   }
	   catch (BackendException ex) {
//			 BusinessObjectHelper.splitException(ex);
	   log.debug("Encountered exception on ATP process", ex);
	   atpResult = null;
	   }
	   catch (JCO.AbapException abapEx) {
	   log.debug("Encountered exception on ATP process", abapEx);
	   atpResult = null;
	   }
	  return atpResult;
   }

   /**
   * Read the atp info list for a given matnr from the backend.
   * @param checkProfile ?
   * @param shop The shop the contract item list is read in.
   * @param country Country of the shop
   * @param matnr Materialnumber
   * @param uom Unit of Measurement
   * @deprecated
   */
    public AtpResult readAtpResult(
                    String shop,
                    String country,
                    WebCatItem item,
                    Date  requestedDate,
                    String cat,
                    String variant) throws CommunicationException {

          AtpResult atpResult = null;

      // read atpResult info from backend
      try {
          atpResult = (AtpResult) getIAtpBackend().readAtpResult(
                  shop,
                  country,
                  item,
                  requestedDate,
                  cat,
                  variant);
      }
      catch (BackendException ex) {
//          BusinessObjectHelper.splitException(ex);
      log.debug("Encountered exception on ATP process", ex);
      atpResult = null;
      }
      catch (JCO.AbapException abapEx) {
      log.debug("Encountered exception on ATP process", abapEx);
      atpResult = null;
      }
     return atpResult;
  }

    /**
     * Read the atp info list for a given matnr from the backend.
     * @param shop The shop the contract item list is read in.
     * @param country Country of the shop
     * @param item WebCatItem
     * @param matNr Material Number
     * @param requestedDate concrete date
     * @param cat catalog
     * @param variant catalog variant
     */
    public AtpResult readAtpResult(
                   String shop,
                   String country,
                   WebCatItem item,
                   String matNr,
                   Date  requestedDate,
                   String cat,
                   String variant) throws CommunicationException {

        AtpResult atpResult = null;

        // read atpResult info from backend
        try {
            atpResult = (AtpResult) getIAtpBackend().readAtpResult(
                 shop,
                 country,
                 item,
                 matNr,
                 requestedDate,
                 cat,
                 variant);
        }
        catch (BackendException ex) {
            log.debug("Encountered exception on ATP process", ex);
            atpResult = null;
        }
        catch (JCO.AbapException abapEx) {
            log.debug("Encountered exception on ATP process", abapEx);
            atpResult = null;
        }
        return atpResult;
    }

	/**
	* Read the atp info list for a given matnr from the backend.
	* @param checkProfile ?
	* @param shop The shop the contract item list is read in.
	* @param matnr Materialnumber
	* @param uom Unit of Measurement
    * @deprecated
	*/
	   public AtpResultList readAtpResultList(
									   String shop,
									   WebCatItem item,
									   Date  requestedDate,
									   String cat,
									   String variant) 
								throws CommunicationException {

			 AtpResultList atpResultList = null;

		 // read atpResult info from backend
		 try {
			 atpResultList = (AtpResultList) getIAtpBackend().readAtpResultList(
					 shop,
					 item,
					 requestedDate,
					 cat,
					 variant);
		 }
		 catch (BackendException ex) {
//			   BusinessObjectHelper.splitException(ex);
		 log.debug("Encountered exception on ATP process", ex);
		 atpResultList = null;
		 }
		 catch (JCO.AbapException abapEx) {
		 log.debug("Encountered exception on ATP process", abapEx);
		 atpResultList = null;
		 }
		return atpResultList;
	 }

     /**
     * Read the atp info list for a given matnr from the backend.
     * @param shop The shop id the contract item list is read in.
     * @param country Country of the shop for formatting reasons 
     * @param matnr Materialnumber
     * @param uom Unit of Measurement
     * @deprecated
     */
        public AtpResultList readAtpResultList(
                                        String shop,
                                        String country,
                                        WebCatItem item,
                                        Date  requestedDate,
                                        String cat,
                                        String variant) 
                                 throws CommunicationException {

              AtpResultList atpResultList = null;

          // read atpResult info from backend
          try {
              atpResultList = (AtpResultList) getIAtpBackend().readAtpResultList(
                      shop,
                      country,
                      item,
                      requestedDate,
                      cat,
                      variant);
          }
          catch (BackendException ex) {
          log.debug("Encountered exception on ATP process", ex);
          atpResultList = null;
          }
          catch (JCO.AbapException abapEx) {
          log.debug("Encountered exception on ATP process", abapEx);
          atpResultList = null;
          }
         return atpResultList;
      }

    /**
     * Read the atp info list for a given matnr from the backend.
     * @param shop The shop id the contract item list is read in.
     * @param country Country of the shop for formatting reasons 
     * @param item WebCatItem
     * @param matNr Materialnumber
     * @param requestedDate concrete date
     * @param cat catalog
     * @param variant catalog variant
     */
    public AtpResultList readAtpResultList(
                                         String shop,
                                         String country,
                                         WebCatItem item,
                                         String matNr,
                                         Date  requestedDate,
                                         String cat,
                                         String variant) 
                                  throws CommunicationException {

        AtpResultList atpResultList = null;

        // read atpResult info from backend
        try {
            atpResultList = (AtpResultList) getIAtpBackend().readAtpResultList(
                       shop,
                       country,
                       item,
                       matNr,
                       requestedDate,
                       cat,
                       variant);
        }
        catch (BackendException ex) {
            log.debug("Encountered exception on ATP process", ex);
            atpResultList = null;
        }
        catch (JCO.AbapException abapEx) {
            log.debug("Encountered exception on ATP process", abapEx);
            atpResultList = null;
        }
        return atpResultList;
    }

    // get the atpBackend, if necessary
    private IAtpBackend getIAtpBackend()
            throws BackendException {
        if (iAtpBackend == null) {
           iAtpBackend =
               (IAtpBackend)bem.createBackendBusinessObject(BackendTypeConstants.BO_TYPE_ATP,
                                                            ATP_OBJECT_FACTORY);
        }
        return iAtpBackend;
    }
}
