/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Created:      04 April 2001

    $Revision: #2 $
    $Date: 2001/07/25 $
*****************************************************************************/
package com.sap.isa.businessobject.webcatalog.atp;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.sap.isa.backend.boi.webcatalog.atp.IAtpResult;
import com.sap.isa.businessobject.BusinessObjectBase;

/**
 * Represents a reference to AtpProduct information
 */
public class AtpResult extends BusinessObjectBase
                               implements IAtpResult{
    private String matNr;
    private String uom;
    private Date   requestedDate;
    private String requestedQuantity;
    private Date   committedDate;
    private String committedQuantity;
    private String requestedDateStr="";
    private String committedDateStr="";

    /**
     * Retrieves the matNr of the referenced atp result
     */
    public String getMatNr() {
        return matNr;
    }

    /**
     * Sets the matNr of the referenced atp result
     */
    public void setMatNr(String matNr) {
        this.matNr = matNr;
    }

    /**
     * Retrieves the unit of measurement the referenced atp result
     */
    public String getUom() {
        return uom;
    }

    /**
     * Sets the unit of measurement of the referenced atp result
     */
    public void setUom(String uom) {
        this.uom = uom;
    }


    /**
     * Retrieves the requestedDate of the referenced atp result
     */
    public Date getRequestedDate() {
        return requestedDate;
    }

    /**
     * Sets the requestedDate of the referenced atp result
     */
    public void setRequestedDate(Date requestedDate) {
        this.requestedDate = requestedDate;
    }

    /**
     * Retrieves the requestedQuantity of the referenced atp result
     */
    public String getRequestedQuantity() {
        return requestedQuantity;
    }

    /**
     * Sets the requestedQuantity of the referenced atp result
     */
    public void setRequestedQuantity(String requestedQuantity) {
        this.requestedQuantity = requestedQuantity;
    }

    /**
     * Retrieves the committed Date of the referenced atp result
     */
    public Date getCommittedDate() {
        return committedDate;
    }

    /**
     * Sets the committed Date of the referenced atp result
     */
     public void setCommittedDate(Date committedDate) {
        this.committedDate = committedDate;
    }

        /**
     * Retrieves the committed Quantity of the referenced atp result
     */
    public String getCommittedQuantity() {
        return committedQuantity;
    }

    /**
     * Sets the committed Quantity of the referenced atp result
     */
     public void setCommittedQuantity(String committedQuantity) {
        this.committedQuantity = committedQuantity;
    }

    private String convertDate(Date date, Locale locale) {
      if (date != null)
         return DateFormat.getDateInstance(DateFormat.SHORT,locale).format(date);
        else return "";
    }

    public String getCommittedDateStr() {
      return committedDateStr;
    }

    public void setDatesToStr(Locale locale) {
        this.committedDateStr = convertDate(getCommittedDate(),locale);
        this.requestedDateStr = convertDate(getRequestedDate(),locale);
    }
    
	/**
     * Prepares the date to be retrieved by jsp using the given dateFormat.
	 * In general it should be used with the shop.getDateFormat()
	*/
    public void setDatesToStr(String dateFormat){
    	//Y is not allowed for year --> y should be used
    	//D means the Day in Year --> we want the Day in Month, also d
    	dateFormat = dateFormat.replaceAll("Y","y").replaceAll("D","d");
		SimpleDateFormat fmt = new SimpleDateFormat(dateFormat);
		this.committedDateStr = fmt.format(getCommittedDate());
		this.requestedDateStr = fmt.format(getRequestedDate());   
    }

    public String getRequestedDateStr() {
      return requestedDateStr;
    }

}
