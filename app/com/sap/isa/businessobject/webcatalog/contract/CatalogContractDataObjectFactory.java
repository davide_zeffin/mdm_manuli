/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Created:      3 April 2001

    $Revision: #3 $
    $Date: 2001/07/31 $
*****************************************************************************/
package com.sap.isa.businessobject.webcatalog.contract;

import com.sap.isa.backend.boi.isacore.contract.ContractAttributeData;
import com.sap.isa.backend.boi.isacore.contract.ContractAttributeListData;
import com.sap.isa.backend.boi.isacore.contract.ContractAttributeValueData;
import com.sap.isa.backend.boi.isacore.contract.ContractAttributeValueListData;
import com.sap.isa.backend.boi.isacore.contract.ContractAttributeValueMapData;
import com.sap.isa.backend.boi.isacore.contract.ContractItemConfigurationReferenceData;
import com.sap.isa.backend.boi.isacore.contract.ContractReferenceData;
import com.sap.isa.backend.boi.isacore.contract.ContractReferenceListData;
import com.sap.isa.backend.boi.isacore.contract.ContractReferenceMapData;
import com.sap.isa.backend.boi.webcatalog.contract.CatalogContractDataObjectFactoryBackend;
import com.sap.isa.businessobject.contract.ContractAttribute;
import com.sap.isa.businessobject.contract.ContractAttributeList;
import com.sap.isa.businessobject.contract.ContractAttributeValue;
import com.sap.isa.businessobject.contract.ContractAttributeValueList;
import com.sap.isa.businessobject.contract.ContractAttributeValueMap;
import com.sap.isa.businessobject.contract.ContractItemConfigurationReference;
import com.sap.isa.businessobject.contract.ContractReference;
import com.sap.isa.businessobject.contract.ContractReferenceList;
import com.sap.isa.businessobject.contract.ContractReferenceMap;

/**
 * Class used for the creation of contract data objects within the BE.
 */
public class CatalogContractDataObjectFactory
       implements CatalogContractDataObjectFactoryBackend {

    /**
     * Creates a new <code>ContractReferenceData</code> object.
     *
     * @return The newly created object
     */
    public ContractReferenceData createContractReferenceData() {
        return (ContractReferenceData)(new ContractReference());
    }

    /**
     * Creates a new <code>ContractReferenceListData</code> object.
     *
     * @return The newly created object
     */
    public ContractReferenceListData createContractReferenceListData() {
        return (ContractReferenceListData)(new ContractReferenceList());
    }

    /**
     * Creates a new <code>ContractReferenceMapData</code> object.
     *
     * @return The newly created object
     */
    public ContractReferenceMapData createContractReferenceMapData(){
        return (ContractReferenceMapData)(new ContractReferenceMap());
    }

    /**
     * Creates a new <code>ContractAttributeData</code> object.
     *
     * @return The newly created object
     */
    public ContractAttributeData createContractAttributeData() {
        return (ContractAttributeData)(new ContractAttribute());
    }

    /**
     * Creates a new <code>ContractAttributeListData</code> object.
     *
     * @return The newly created object
     */
    public ContractAttributeListData createContractAttributeListData() {
        return (ContractAttributeListData)(new ContractAttributeList());
    }

    /**
     * Creates a new <code>ContractAttributeValueData</code> object.
     *
     * @return The newly created object
     */
    public ContractAttributeValueData createContractAttributeValueData() {
        return (ContractAttributeValueData)(new ContractAttributeValue());
    }

    /**
     * Creates a new <code>ContractAttributeValueListData</code> object.
     *
     * @return The newly created object
     */
    public ContractAttributeValueListData
            createContractAttributeValueListData() {
        return (ContractAttributeValueListData)(
            new ContractAttributeValueList());
    }

    /**
     * Creates a new <code>ContractAttributeValueMapData</code> object.
     *
     * @return The newly created object
     */
    public ContractAttributeValueMapData createContractAttributeValueMapData(){
        return (ContractAttributeValueMapData)(new ContractAttributeValueMap());
    }

    /**
     * Creates a new <code>ContractItemConfigurationReferenceData</code> object.
     *
     * @return The newly created object
     */
    public ContractItemConfigurationReferenceData
            createContractItemConfigurationReferenceData() {
        return (ContractItemConfigurationReferenceData)
            (new ContractItemConfigurationReference());
    }
}