package com.sap.isa.businessobject.webcatalog.contract;

import com.sap.isa.backend.boi.webcatalog.contract.ContractReferenceRequestBackend;
import com.sap.isa.catalog.webcatalog.WebCatBusinessObjectBase;
import com.sap.isa.catalog.webcatalog.WebCatItem;

/**
 * Title:        Internet Sales, dev branch
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @author
 * @version 1.0
 */

public class ContractReferenceRequest
       extends WebCatBusinessObjectBase
       implements ContractReferenceRequestBackend {

  private String product              = "";
  private String productGuid          = "";
  private String catalogId            = "";
  private String userId               = "";
  private String purchaseOrderNo      = "";
  private String soldToId             = "";
  private String salesOrg             = "";
  private String distChannel          = "";
  private String language             = "";
  private WebCatItem item             = null;

  public ContractReferenceRequest() {
  }

  public void setProduct(String product) {
    this.product = product;
  }

  public String getProduct() {
    return product;
  }

  public void setProductGuid(String productGuid) {
    this.productGuid = productGuid;
  }

  public String getProductGuid() {
    return productGuid;
  }

  public void setCatalogId(String catalogId) {
    this.catalogId = catalogId;
  }

  public String getCatalogId() {
    return catalogId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getUserId() {
    return userId;
  }

  public void setPurchaseOrderNo(String purchaseOrderNo) {
    this.purchaseOrderNo = purchaseOrderNo;
  }

  public String getPurchaseOrderNo() {
    return purchaseOrderNo;
  }

  public void setSoldToId(String soldToId) {
    this.soldToId = soldToId;
  }

  public String getSoldToId() {
    return soldToId;
  }

  public void setSalesOrg(String salesOrg) {
    this.salesOrg = salesOrg;
  }

  public String getSalesOrg() {
    return salesOrg;
  }

  public void setDistChannel(String distChannel) {
    this.distChannel = distChannel;
  }

  public String getDistChannel() {
    return distChannel;
  }

  public void setLanguage(String language) {
    this.language = language;
  }

  public String getLanguage() {
    return language;
  }

  public void setItem(WebCatItem item) {
    this.item = item;
  }

  public WebCatItem getItem() {
    return item;
  }

  public String toString() {
    StringBuffer buf = new StringBuffer("ContractReferenceRequest: ");
    buf.append("Product=").append(getProduct());
    buf.append(",ProductGUID=").append(getProductGuid());
    buf.append(",PO-No=").append(getPurchaseOrderNo());
    buf.append(",user=").append(getUserId());
    buf.append(",soldTo=").append(getSoldToId());
    buf.append(",salesOrg=").append(getSalesOrg());
    buf.append(",distChannel=").append(getDistChannel());
    buf.append(",language=").append(getLanguage());
    buf.append(",item=").append(getItem());
    return buf.toString();
  }

}
