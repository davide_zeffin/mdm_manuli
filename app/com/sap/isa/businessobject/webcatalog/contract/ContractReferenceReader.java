/*****************************************************************************
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      20 March 2000

    $Revision: #11 $
    $Date: 2001/08/01 $
*****************************************************************************/
package com.sap.isa.businessobject.webcatalog.contract;

import java.util.HashMap;

import com.sap.isa.backend.BackendTypeConstants;
import com.sap.isa.backend.boi.isacore.contract.ContractView;
import com.sap.isa.backend.boi.webcatalog.contract.CatalogContractDataObjectFactoryBackend;
import com.sap.isa.backend.boi.webcatalog.contract.ContractReferenceReaderBackend;
import com.sap.isa.backend.boi.webcatalog.contract.ContractReferenceRequestBackend;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.contract.ContractAttributeList;
import com.sap.isa.businessobject.contract.ContractReferenceMap;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;

/**
 * The Contract class makes (value/quantity) contracts of a user accessible
 * for browsing and ordering including contract call off in the B2B Internet
 * Sales scenario.
 */
public class ContractReferenceReader extends BusinessObjectBase
                                     implements BackendAware,
                                                BackendBusinessObjectParams {

    protected static final CatalogContractDataObjectFactoryBackend
        CONTRACT_DATA_OBJECT_FACTORY = new CatalogContractDataObjectFactory();
    protected BackendObjectManager bem;
    protected ContractReferenceReaderBackend contractReferenceReaderBackend;

    /**
     * Constructor.
     */
    public ContractReferenceReader() {
        super();
        contractAttributes = new ContractAttributes();
    }

    // map of contract attributes
    private class ContractAttributes {
        private HashMap attributeMap;
        ContractAttributes() {
            attributeMap = new HashMap();
        }
        public void put(ContractView view,
                ContractAttributeList attributeList) {
            attributeMap.put(view, attributeList);
        }
        public ContractAttributeList get(ContractView view) {
            return (ContractAttributeList)attributeMap.get(view);
        }
        public boolean containsKey(ContractView view) {
            return attributeMap.containsKey(view);
        }
    }
    private ContractAttributes contractAttributes;

    /**
     * Sets the BackendObjectManager for the attribute set. This method is used
     * by the object manager to allow the user object interaction with
     * the backend logic. This method is normally not called
     * by classes other than BusinessObjectManager.
     *
     * @param bem BackendObjectManager to be used
     */
    public void setBackendObjectManager(BackendObjectManager bem) {
        this.bem = bem;
    }

    /**
     * Read the contract references for a given list of products from the
     * backend.
     * @param productKeySet A list of product keys.
     * @param soldTo The sold to party the contract list is searched for.
     * @param shop The shop the contract item list is read in.
     * @param view Desired view onto the contract data (product list, detail or
     * contract list).
     * @return Contract references for the products in form of a
     * <code>ContractReferenceMap</code>.
     */
    public ContractReferenceMap readReferences(
            ContractReferenceRequestBackend[] requests,
            ContractView view)
            throws CommunicationException {
        ContractReferenceMap contractReferenceMap = null;

        // read contract references from backend
        try {
            contractReferenceMap =
            (ContractReferenceMap)getContractReferenceReaderBackend().readReferences(
                requests,
                view);
        }
        catch (BackendException ex) {
            log.error("system.eai.message",ex);
            return null;
        }

        // store attributes for subsequent reads
        if (!contractAttributes.containsKey(view)) {
            contractAttributes.put(view, contractReferenceMap.getAttributes());
        }
        return contractReferenceMap;
    }

    /**
     * Read the contract attributes for a given shop & view, if necessary from
     * the backend.
     * @param shop The shop the contract item list is read in.
     * @param view Desired view onto the contract data (product list, detail or
     * contract list).
     * @return Contract attributes in form of a
     * <code>ContractAttributeList</code>.
     */
    public ContractAttributeList readAttributes(
            String language,
            ContractView view)
            throws CommunicationException {
        ContractAttributeList contractAttributeList = null;

        // if not already done so, read from backend
        if (!contractAttributes.containsKey(view)) {
            try {
                contractAttributeList =
                (ContractAttributeList)getContractReferenceReaderBackend().readAttributes(
                    language,
                    view);
                contractAttributes.put(view, contractAttributeList);
            }
            catch (BackendException ex) {
                log.error("system.eai.message",ex);
                return null;
            }
        }
        else {
            contractAttributeList = contractAttributes.get(view);
        }
        return contractAttributeList;
    }

    // get the contractReferenceReaderBackend, if necessary
    private ContractReferenceReaderBackend getContractReferenceReaderBackend()
            throws BackendException {
        if (contractReferenceReaderBackend == null) {
            contractReferenceReaderBackend =
                (ContractReferenceReaderBackend) bem.createBackendBusinessObject(
                    BackendTypeConstants.BO_TYPE_CONTRACT_REFERENCE,
                    CONTRACT_DATA_OBJECT_FACTORY);
        }
        return contractReferenceReaderBackend;
    }

}
