/*****************************************************************************
    Class:        ExtendedStatusListEntry
    Copyright (c) 2002, SAP AG, Germany, All rights reserved.
    Author:

    $Revision: #1 $
    $DateTime: 2003/09/25 18:19:45 $ (Last changed)
    $Change: 150323 $ (changelist)

*****************************************************************************/

package com.sap.isa.businessobject;

import com.sap.isa.backend.boi.isacore.*;
import com.sap.isa.core.TechKey;

/**
 *
 * Class descripts a status of an object. Either this is a status which could be set
 * or the current status of the object.
 * Main status handling is located in class <code>ExtendedStatus</code>.
 * @version 1.0
 */
public class ExtendedStatusListEntry extends BusinessObjectBase
             implements ExtendedStatusListEntryData {

    private String entryPosition;
    private String description;
    private String description_short;
    private String businessProcess;
    private boolean active;

    /**
     * Contructor; allow to create this object in one step
     * @param entryPosition     Position this entry in the list
     * @param description       Language depending description of the status
     * @param description_short Language depending short description of the status
     * @param businessProcess   Business process which will be triggered if status will be set
     * @param active            Is status active or not <code>true</code> / <code>false</code>
     *
     */
    public ExtendedStatusListEntry(TechKey statusKey,
                                   String entryPos,
                                   String descript,
                                   String descript_short,
                                   String busprocess) {
        this.techKey = statusKey;
        this.entryPosition = entryPos;
        this.description = descript;
        this.description_short = descript_short;
        this.businessProcess = busprocess;
        this.active = false;

    }
    /**
     * Get position of status in status profile
     */
     public String getEntryPosition() {
         return this.entryPosition;
     }
    /**
     * Get language depending status description in long format.
     */
     public String getDescription() {
         return this.description;
     }
    /**
     * Get lanugage depending status description in short format
     */
     public String getDescriptionShort() {
         return this.description_short;
     }
    /**
     * Get Businessprocess assigned to status.
     */
     public String getBusinessProcess() {
         return this.businessProcess;
     }
     /**
      * Is status current status
      */
     public boolean isCurrentStatus() {
         return this.active;
     }
     /**
      * Mark status object as current status
      */
     public void setCurrentStatus(boolean flag) {
         this.active = flag;
     }
     /**
      * Object related <code>toString()</code> method
      */
     public String toString() {
         String finalString = this.techKey.getIdAsString();
         finalString.concat(this.entryPosition);
         finalString.concat(this.description);
         finalString.concat(this.description_short);
         finalString.concat(this.businessProcess);
         finalString.concat((this.active ? "X" : ""));
         return finalString;
     }
}
