/*****************************************************************************
    Class:        ShipTo
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP AG
    Created:      19.3.2001
    Version:      1.0

    $Revision: #2 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.businessobject;

import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.backend.boi.isacore.ShipToData;
import com.sap.isa.core.TechKey;

/**
 * Ship to
 *
 * @author Thomas Smits
 * @version 1.0
 */
public class ShipTo extends BusinessObjectBase implements ShipToData {

    /**
     * A canonical shipto that contains no special information. This
     * object can be used in calls to methods that require a shipto but
     * where the caller has no shipto information available. In the cases
     * described, do not create a new shipto with <code>new ShipTo()</code>
     * but use this reference.
     */
    public static final ShipTo SHIPTO_INITIAL = new ShipTo(TechKey.EMPTY_KEY);

    private Address address;
    private String shortAddress;
    private String id;
    private boolean manualAddress = false;
    private String status = ShipToData.ACTIVE;
    private String parent;
    private boolean posflag = false;
    /**
     * Creates a new instance of the ship to, without providing any
     * initialization data.
     */
    public ShipTo() {
    }

    private ShipTo(TechKey techKey) {
        this.techKey = techKey;
    }

    /**
     * Sets the fully qualified address for the ship to
     *
     * @param address The address to be set
     */
    public void setAddress(AddressData address) {
        this.address = (Address) address;
    }

    /**
     * Sets the fully qualified address for the ship to
     *
     * @param address The address to be set
     */
    public void setAddress(Address address) {
        this.address = address;
    }

    /**
     * Sets the status of the ship to,
     * e.g. if it's activ or not acitv
     * @param status The statusto be set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * Returns the address for the ship to
     *
     * @return Address
     */
    public Address getAddress() {
        return address;
    }


    /**
     * Returns the address for the ship to
     *
     * @return Address
     */
    public AddressData getAddressData() {
        return address;
    }


    /**
     * Returns the short address for the ship to
     *
     * @return The short address
     */
    public String getShortAddress() {
        return shortAddress;
    }

    /**
     * Returns the status of the ship to
     *
     * @return The stauts
     *
     */
    public String getStatus() {
        return status;
    }

    /**
     * Creates a new Address object.
     *
     * @return Newly created AddressData object
     */
    public AddressData createAddress() {
        return new Address();
    }

    /**
     * Sets the short address for the ship to. The short address is used
     * in dialogs to select a ship to from a list of ship tos
     *
     * @param shortAddress The short address for the ship to
     */
    public void setShortAddress(String shortAddress) {
        this.shortAddress = shortAddress;
    }

    /**
     * Get a string representation of the object.
     *
     * @return string representation
     */
    public String toString() {
        return "ShipTo [techKey=\"" + techKey + "\", shortAddress=\"" +
                shortAddress + "\", status=\"" + status + "\"]";
    }

   /**
    * Returns true if the shipto status is activ;
    * returns false if the shipto status is not acitv.
    *
    * @return boolean indicates if the shipto is activ or not
    */
    public boolean isActive() {
        if (ShipToData.ACTIVE.equals(this.status)) {
           return true;
        } else {
           return false; }
    }


    /**
     * Sets the business partner id of the ship to
     *
     * @param id  The id to be set
     */
    public void setId(String id) {
        this.id = id;
    }


     /**
     * Returns the id of the the ship to
     *
     * @return id
     */
    public String getId() {
        return id;
    }


    /**
     * Marks if the shipto address is manually
     * maintained or if it its a orginal master data
     * address
     *
     */
    public void setManualAddress() {
        this.manualAddress = true;
    }


    /**
     * Marks if the shipto address is manually
     * maintained or if it its a orginal master data
     * address
     *
     * @param manualAddress flag to set
     */
    public void setManualAddress(boolean manualAddress) {
        this.manualAddress = manualAddress;
    }


    /**
     * Return if the shipto addresss is manual maintained
     *
     * @return true if the shipto has a manual address;
     *         false if it is the shipto orginal master data address
     */
    public boolean hasManualAddress() {
        return manualAddress;
    }


    /**
     * Clones a new shipto from the given shipto object.
     *
     * @result the cloned shipto object
     *
     */
     public Object clone() {

       ShipTo shiptoClone = new ShipTo();

       shiptoClone.setTechKey(this.techKey);
       shiptoClone.setId(this.id);
       shiptoClone.setManualAddress(this.manualAddress);
       shiptoClone.setShortAddress(this.shortAddress);
       shiptoClone.setStatus(this.status);
       Address shiptoCloneAddress = (Address) this.address.clone();
       shiptoClone.setAddress(shiptoCloneAddress);

       return shiptoClone;
     }

	/**
	  * Returns the key of the parent object, e.g. item or header
	  * @return String
	  */
	public String getParent() {
		return parent;
	}
	
	/**
	 * Sets the parent key of the ship to, e.g. item key or header key
	 */
	public void setParent(String parent) {
		this.parent = parent;
	}

	/**
	 * compares two ship to's. 
	 * @return true, if they are equals and 
	 * 		   false, if they aren't equals   
	 */
	public boolean compare(ShipToData toCompareShipTo) {
		if (this.getId().equals(toCompareShipTo.getId())) {
			AddressData currentAddress = this.getAddressData();
			AddressData toCompareAddress = toCompareShipTo.getAddressData();
			if (currentAddress != null &&  toCompareAddress != null) {
				if (!currentAddress.getLastName().equals(toCompareAddress.getLastName())){
					return false;				
				}
				if (!currentAddress.getFirstName().equals(toCompareAddress.getFirstName())){
					return false;				
				}
				if (!currentAddress.getCountry().equals(toCompareAddress.getCountry())){
					return false;				
				}
				if (!currentAddress.getRegion().equals(toCompareAddress.getRegion())){
					return false;				
				}
				if (!currentAddress.getEMail().equals(toCompareAddress.getEMail())){
					return false;				
				}
				if (!currentAddress.getFaxNumber().equals(toCompareAddress.getFaxNumber())){
					return false;				
				}
				if (!currentAddress.getTel1Numbr().equals(toCompareAddress.getTel1Numbr())){
					return false;				
				}
				if (!currentAddress.getHouseNo().equals(toCompareAddress.getHouseNo())){
					return false;				
				}
				if (!currentAddress.getPostlCod1().equals(toCompareAddress.getPostlCod1())){
					return false;				
				}
				if (!currentAddress.getStreet().equals(toCompareAddress.getStreet())){
					return false;				
				}
				if (!currentAddress.getCity().equals(toCompareAddress.getCity())){
					return false;				
				}
				return true;
			} else {
				// if toCompareAddress is null and the shipto's have the same ID
				// they must be the same
				if (!this.getShortAddress().equals(toCompareShipTo.getShortAddress())) {
					return false;
				}
				return true;
			}
		}
		return false;
	}
	
	/**
	 * gets the indicator for position ship to
	 */
	public boolean isPosflag() {
		return posflag;
	}

	/**
	 * sets the flag, which indicates that the shipto belongs
	 * to a position
	 */
	public void setPosflag(boolean b) {
		this.posflag = b;
	}

}