/*****************************************************************************
    Class:        CommunicationException
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Thomas Smits
    Created:      28.3.2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/06/26 $
*****************************************************************************/

package com.sap.isa.businessobject;

import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.MessageList;

/**
 * Represents a general superclass for all exceptions of the backend layer
 * caused by communication problems (network failures etc.).
 *
 * @author Thomas Smits
 * @version 1.0
 */
public class CommunicationException extends BusinessObjectException {

    /**
     * Constructor
     *
     * @param msg Message for the Excepetion
     * @param msgList List of the messages added to the exception
     */
    public CommunicationException(String msg, MessageList msgList) {
        super(msg);
        this.msgList = msgList;
    }


    /**
     * Constructor
     *
     * @param msg Message for the Excepetion
     * @param message message added to the exception
     */
    public CommunicationException(String msg, Message message) {
        super(msg);
        this.msgList = new MessageList();
        msgList.add(message);
    }


    /**
     * Constructor
     *
     * @param msg Message for the Excepetion
     */
    public CommunicationException(String msg) {
        super(msg);
    }

    /**
     * Constructor
     */
    public CommunicationException() {
        super();
    }


}