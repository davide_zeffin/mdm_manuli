/*****************************************************************************

    Class:        BatchCharVals
    Copyright (c) 2001, SAP AG, Germany, All rights reserved.
    Author:       Daniel Seise
    Created:      22.11.2002
    Version:      1.0

    $Revision: #3 $
    $Date: 2003/10/13 $
*****************************************************************************/

package com.sap.isa.businessobject;

import java.util.ArrayList;

import com.sap.isa.backend.boi.isacore.BatchCharValsData;
import com.sap.isa.core.TechKey;



/**
 * Class representing one character and its values of a batch. 
 *
 * @author Daniel Seise
 * @version 1.0
 */
public class BatchCharVals
	extends BusinessObjectBase
	implements BatchCharValsData{
	
		
	private String charName;
	private String charTxt;
	private String charDataType; 
	private String charUnit;
	private String charUnitTExt;
	private String charAddValInput;
	private String charRangeFrom;
	private String charRangeTo;
	private boolean charAddVal;
	private int charNumberDigit;
	private int charNumberDec;
	
	private int tableNum;
	
	private ArrayList charValName = new ArrayList();
	private ArrayList charVal = new ArrayList();
	private ArrayList charValTxt = new ArrayList();
	private ArrayList charValMax = new ArrayList();
	private ArrayList charValRangeBorders = new ArrayList();
	private ArrayList charValCode = new ArrayList();
	
	
	
	/**
     * Default constructor for the Element
     */
	public BatchCharVals() {
    }

    
    /**
     * Constructor for the object directly taking the technical
     * key for the object
     * if the technical key is empty an unique handle will be generate
     *
     * @param techKey Technical key for the object
     */
    public BatchCharVals(TechKey techKey) {
        super(techKey);
        if (techKey == null || techKey.isInitial() || techKey.getIdAsString().length()<1) {
            // create a handle for the new item!
            createUniqueHandle();
        }
    }
	
	
	/**
     * Set the name of the element
     *
     * @param charNAME the String to be set
     */	
	public void setCharName(String charNAME){
		this.charName = charNAME;
	}
	
	/**
     * Get the name of the element. 
     *
     * @return charName name of the element
     */
	public String getCharName(){
		return charName;
	}
	
	
	/**
     * Set the Text of the element
     *
     * @param charTXT the String to be set
     */	
	public void setCharTxt(String charTXT){
		this.charTxt = charTXT;
	}
	
	/**
     * Get the text of the element. 
     *
     * @return charTxt text of the element
     */
	public String getCharTxt(){
		return charTxt;
	}
	
	
	/**
     * Set the data type of the element
     *
     * @param charDataTYP the String to be set
     */	
	public void setCharDataType(String charDataTYPE){
		this.charDataType = charDataTYPE;
	}
	
	/**
     * Get the data type of the element. 
     *
     * @return charDataTyp data type of the element
     */
	public String getCharDataType(){
		return charDataType;
	}
	
	
	/**
     * Set a boolean value name of the element for additional values
     *
     * @param addVAL <code>true</code> if the user is allowed to set additonal values
     */	
	public void setCharAddValue(boolean charAddVAL){
		this.charAddVal = charAddVAL;
	}
	
	
	/**
     * Get a boolean value of the element. 
     *
     * @return addVal <code>true</code> additional valsues are allowed <br>
     * 					<code>false</code> additional values are not allowed
     */
	public boolean getCharAddVal(){
		return charAddVal;
	}
	
	
	/**
     * Set the unit of the element
     *
     * @param charUNIT the String to be set
     */	
	public void setCharUnit(String charUNIT){
		this.charUnit = charUNIT;
	}
	
	/**
     * Get the unit of the element. 
     *
     * @return charUnit unit of the element
     */
	public String getCharUnitTExt(){
		return charUnitTExt;
	}
	
	/**
     * Set the unit of the element
     *
     * @param charUNIT the String to be set
     */	
	public void setCharUnitTExt(String charUnitTEXT){
		this.charUnitTExt = charUnitTEXT;
	}
	
	/**
     * Get the unit of the element. 
     *
     * @return charUnit unit of the element
     */
	public String getCharUnit(){
		return charUnit;
	}
	
	
	
	
	/**
     * Set the values of the characterisitcs of an element.
     *
     * @param charVAL the String to be added to the charVal arraylist
     */	
	public void setCharValue(String charVAL){
		this.charVal.add(charVAL);
	}
	
	/**
	 * Set the values of the characterisitcs of an element.
	 *
	 * @param charVAL the String to be added to the charVal arraylist
	 */	
	public void setCharValue(int i, String charVAL){
		this.charVal.set(i, charVal);
	}
	
	/**
     * Return the characteristic of the element. 
     *
     * @param i index of the characteristic that should return
     * @return charVal String of characteristics value the element
     */
	public String getCharVal(int i){
		return charVal.get(i).toString();
	} 
	
	
	/**
     * Set the text values of the characterisitcs of an element.
     *
     * @param charValTXT the String to be added to the charValTxt arraylist
     */
	public void setCharValTxt(String charValTXT){
		this.charValTxt.add(charValTXT);
	}
	
	/**
     * Return the characteristic of the element. 
     *
     * @param i index of the characteristic that should return
     * @return charValTxt String of characteristics text the element
     */
	public String getCharValTxt(int i){
		return charValTxt.get(i).toString();
	}
	
	/**
     * Return the amount of characteristic of the element. 
     *
     * @return int Amount of characteristics in the element
     * @deprecated
     */
	public int getCharValTxtNum(){
		return charVal.size();
	}
	
	/**
     * Return the amount of characteristic of the element. 
     *
     * @return int Amount of characteristics in the element
     */
	public int getCharValNum(){
		return charVal.size();
	}
	
	
	/**
     * Set the max values of the characterisitcs of an element.
     *
     * @param charValMAX the String to be added to the charValMax arraylist
     */
	public void setCharValMax(String charValMAX){
		this.charValMax.add(charValMAX);
	}
	
	/**
	 * Set the max values of the characterisitcs of an element.
	 *
	 * @param charValMAX the String to be added to the charValMax arraylist
	 */
	public void setCharValMax(int i, String charValMAX){
		this.charValMax.set(i, charValMAX);
	}
	
	/**
     * Return the characteristic of the element. 
     *
     * @param i index of the characteristic that should return
     * @return charValMax String of characteristics maximum value the element
     */
	public String getCharValMax(int i){
		return charValMax.get(i).toString();
	}
	
	/**
     * Set the range values of the characterisitcs of an element.
     *
     * @param charValRangeBORDERS the String to be added to the charValRangeBORDER arraylist
     */
	public void setCharValRangeBorders(String charValRangeBORDERS){
		this.charValMax.add(charValRangeBORDERS);
	}
	
	/**
     * Return the characteristic of the element. 
     *
     * @param i index of the characteristic that should return
     * @return charValRangeBorders String of characteristics range the element
     */
	public String getCharValRangeBorders(int i){
		return charValRangeBorders.get(i).toString();
	}
	
	/**
     * Set the range values of the characterisitcs of an element.
     *
     * @param charValRangeBORDERS the String to be added to the charValRangeBORDER arraylist
     */
	public void setCharValName(String charValNAME){
		this.charValName.add(charValNAME);
	}
	
	/**
     * Return the characteristic of the element. 
     *
     * @param i index of the characteristic that should return
     * @return charValRangeBorders String of characteristics range the element
     */
	public String getCharValName(int i){
		return charValName.get(i).toString();
	}
	
	/**
     * Set the range values of the characterisitcs of an element.
     *
     * @param charValRangeBORDERS the String to be added to the charValRangeBORDER arraylist
     */
	public void setCharValCode(String charValCODE){
		this.charValCode.add(charValCODE);
	}
	
	/**
     * Return the characteristic of the element. 
     *
     * @param i index of the characteristic that should return
     * @return charValRangeBorders String of characteristics range the element
     */
	public String getCharValCode(int i){
		return charValCode.get(i).toString();
	}
	

	/**
	 * Returns the charNumberDec.
	 * @return int
	 */
	public int getCharNumberDec() {
		return charNumberDec;
	}

	/**
	 * Returns the charNumberDigit.
	 * @return int
	 */
	public int getCharNumberDigit() {
		return charNumberDigit;
	}

	/**
	 * Sets the charNumberDec.
	 * @param charNumberDec The charNumberDec to set
	 */
	public void setCharNumberDec(int charNumberDec) {
		this.charNumberDec = charNumberDec;
	}

	/**
	 * Sets the charNumberDigit.
	 * @param charNumberDigit The charNumberDigit to set
	 */
	public void setCharNumberDigit(int charNumberDigit) {
		this.charNumberDigit = charNumberDigit;
	}

	/**
	 * Returns the charAddValInput.
	 * @return String
	 */
	public String getCharAddValInput() {
		return charAddValInput;
	}

	/**
	 * Returns the charRangeFrom.
	 * @return String
	 */
	public String getCharRangeFrom() {
		return charRangeFrom;
	}

	/**
	 * Returns the charRangeTo.
	 * @return String
	 */
	public String getCharRangeTo() {
		return charRangeTo;
	}

	/**
	 * Sets the charAddValInput.
	 * @param charAddValInput The charAddValInput to set
	 */
	public void setCharAddValInput(String charAddValInput) {
		this.charAddValInput = charAddValInput;
	}

	/**
	 * Sets the charRangeFrom.
	 * @param charRangeFrom The charRangeFrom to set
	 */
	public void setCharRangeFrom(String charRangeFrom) {
		this.charRangeFrom = charRangeFrom;
	}

	/**
	 * Sets the charRangeTo.
	 * @param charRangeTo The charRangeTo to set
	 */
	public void setCharRangeTo(String charRangeTo) {
		this.charRangeTo = charRangeTo;
	}
	
	public void removeElement(int i){
		charVal.remove(i);
		charValTxt.remove(i);
		charValMax.remove(i);
	}

}