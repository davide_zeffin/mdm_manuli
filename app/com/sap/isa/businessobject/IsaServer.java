/*****************************************************************************
    Class:        IsaServer
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Alexander Staff
    Created:      26.3.2001
    Version:      1.0

    $Revision: #2 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.businessobject;


import java.util.Properties;

import com.sap.isa.catalog.boi.IServer;

/**
 * Class representing the server for a catalog object.
 * Needed to instantiate the WebCatInfo-object in the bom
 *
 * @author Alexander Staff
 * @version 1.0
 */
public final class IsaServer implements IServer {

    private String      catalogGuid = null;
    private int         port        = 0;
    private Properties  properties  = null;
    private String      urlString   = null;

    /**
     * Creates a new instance
     *
     */
    public IsaServer() {
    }

    /**
     * Returns the guid of the catalog. If the server needs in any
     * case an unique identifier for the catalog
     *
     * @return catalogGuid
     */
    public String getCatalogGuid() {
        return this.catalogGuid;
    }

    /**
     * Sets the catalog's guid
     *
     * @param catalogGuid The Guid of the catalog
     */
    public void setCatalogGuid( String catalogGuid ) {
        this.catalogGuid = catalogGuid;
    }

    /**
     * Returns the port of the "serverURL" as int
     *
     * @return port
     */
    public int getPort() {
        return this.port;
    }

    /**
     * Sets the port of the "serverURL" as int
     *
     * @param port The port part of the ServerURL
     */
    public void setPort( int port ) {
        this.port = port;
    }

    /**
     * Returns the set of key value pairs the current implementation
     * needs in addition to make a connection to a specific server/catalog.
     *
     * @return properties
     */
    public Properties getProperties() {
        return this.properties;
    }

    /**
     * Sets the set of key value pairs the current implementation
     * needs in addition to make a connection to a specific server/catalog.
     *
     * @param properties The key/value pairs
     */
    public void setProperties( Properties properties ) {
        this.properties = properties;
    }

    /**
     * Returns the server part of the "serverURL" as String
     *
     * @return urlString The serverURL as String
     */
    public String getURLString() {
        return this.urlString;
    }

    /**
     * Sets the server part of the "serverURL" as String
     *
     * @param urlString The serverURL as String
     */
    public void setURLString( String urlString ) {
        this.urlString = urlString;
    }

    public String getGuid()
    {
      return null;
    }

}
