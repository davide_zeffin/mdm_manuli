package com.sap.isa.businessobject;

/*****************************************************************************
	Copyright (c) 2004, SAP Labs Europe GmbH, Germany, All rights reserved.
*****************************************************************************/

import com.sap.isa.backend.BackendTypeConstants;
import com.sap.isa.backend.boi.isacore.CurrencyConverterBackend;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;

/**
 * This is used to convert the Currency code from ISO format to SAP format
 * and vice versa
 * 
 */
public class CurrencyConverter extends BusinessObjectBase
implements BackendAware,
	BackendBusinessObjectParams {
	protected CurrencyConverterBackend currencyConverterBackend;
	protected BackendObjectManager bem;
    
	/**
	 * Constructor.
	 */
	public CurrencyConverter() {
		super();
	}
    
	/**
	 * Sets the BackendObjectManager for the attribute set. This method is used
	 * by the object manager to allow the user object interaction with
	 * the backend logic. This method is normally not called
	 * by classes other than BusinessObjectManager.
	 *
	 * @param bem BackendObjectManager to be used
	 */
	public void setBackendObjectManager(BackendObjectManager bem) {
		this.bem = bem;
	}
    
	// get the UOMConverterBackend, if necessary
	protected CurrencyConverterBackend getCurrencyConverterBackend()
		throws BackendException {
		if (currencyConverterBackend== null) {
			currencyConverterBackend = (CurrencyConverterBackend)
			bem.createBackendBusinessObject(BackendTypeConstants.BO_TYPE_CURRENCYCONVERTER, null);
		}
		return currencyConverterBackend;
	}
    
	/**
	*
	* Convert the order to PDF using its techkey and the language
	*
	*/
	public String convertCurrency(boolean saptoiso,String uom) throws CommunicationException {
		// read oci lines from backend
		final String METHOD = "convertCurrency()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("saptoiso="+saptoiso+", uom="+uom);
        
		String str=null;
		try {
				str = getCurrencyConverterBackend().convertCurrency(saptoiso,uom);
			return str;
		}
		catch (BackendException ex) {
			BusinessObjectHelper.splitException(ex);
		}
		finally{
			log.exiting();
		}
		return str;
	}
    
}
