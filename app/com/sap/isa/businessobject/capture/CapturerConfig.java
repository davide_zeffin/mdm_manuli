/*****************************************************************************
	Class         CapturerConfig
	Copyright (c) 2004, SAP Labs LLC, PaloAlto All rights reserved.
	Author:       
	Created:      Created on Jan 6, 2004
	Version:      1.0

	$Revision$
	$Date$
*****************************************************************************/
package com.sap.isa.businessobject.capture;

import java.util.Vector;

import com.sap.isa.backend.BackendTypeConstants;
import com.sap.isa.backend.boi.isacore.capture.CapturerBackend;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.isacore.TealeafInitHandler;

/**
 * Business Object implementation for Capturer configuraiton
 * Contains the list of event to be captured by tealeaf or any other
 * event capturer 
 */
public class CapturerConfig extends BusinessObjectBase implements BackendAware {

	/**
	 * XCM scenario to which the configuration is bound to
	 */
	private String scenario; 
	
	/**
	 * List of event to be captured
	 */
	private Vector eventList;
	/**
	 * Event map file fetched from backend
	 */
	private String cachedMappingfile;
	/**
	 * Backed object manager bound to this BO
	 */
	private BackendObjectManager bem;
	private CapturerBackend backendService;

	/**
	 * If an attempt is made to fetch the event configuration from backend Eg. BW
	 */
	private boolean isConfigFetchAttempted = false; 
	
	

	/**
	 * Adds an event to the list of events to be captured
	 */
	public void setEventName(String eventName) {
		if (eventList == null)
			eventList = new Vector();
		if (!eventList.contains(eventName)) //add only once
			eventList.add(eventName);
	}

	/**
	 * Returns true, if the event is to be captured. i.e., appropriate mapping is maintaiend in the BW.
	 * @param eventName name of the event
	 * @return true - if the event is to be captured
	 * 			false - if the event should not be captured
	 */
	public boolean isEventTobeCaptured(String eventName) {
		//if the eventList doesn't exist means no configuration downloaded from BW
		//so capture everything, otherwise capture only the events configured in BW
		return (eventList == null) ? true : eventList.contains(eventName);
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.businessobject.BackendAware#setBackendObjectManager(com.sap.isa.core.eai.BackendObjectManager)
	 */
	public void setBackendObjectManager(BackendObjectManager bom) {
		this.bem = bom;
	}

	/**
	 * Get the BackendService, if necessary
	 */
	private CapturerBackend getBackendService() throws BackendException {

		if (backendService == null) {
			try {
				backendService =
					(CapturerBackend) bem.createBackendBusinessObject(
						BackendTypeConstants.BO_TYPE_CAPTURER,
						null);
			} catch (BackendException ex) {
				log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, 
					"system.exception",
					new Object[] { ex.getLocalizedMessage()},
					ex);
				backendService = null;
			}
		}
		return backendService;
	}

	/**
	 * Loads the configuration from BW
	 */
	public void loadConfiguration(String scenario) {
		final String METHOD = "loadConfiguration()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("load event configuration for scenario="+scenario);
		try {
			
			try {
				setScenario(scenario);
				getBackendService();
			} catch (BackendException e) {
				if (log.isDebugEnabled()) {
					log.debug("Error occured while contacting backend for Capturer BO", e);
					log.debug("All events will be captured");
				}	
				return;
			}
			
			backendService.loadConfiguration(getFileName(), this);
		}finally{
			log.exiting();
		}
	}

	/**
	 * Returns whether configuration fetch is attempted or not
	 * @return true - if the configuration fetch is attempted
	 * 			false - otherwise
	 */
	public boolean isConfigFetchAttempted() {
		return isConfigFetchAttempted;
	}

	/**
	 * Sets <b> isConfigFetchAttempted</b>
	 * @param b
	 */
	public void setConfigFetchAttempted(boolean b) {
		isConfigFetchAttempted = b;
	}

	/**
	 * Returns the file name based on Cache directory and scenario.
	 * Cache directory can be set though the param <b>eventMapFileStorageDir</b>
	 * of InitAction  <b> TealeafInitHandler</b> in init-config.xml
	 * 
	 * @return  file name based on cache directory and scenario
	 */
	public String getFileName(){
		return TealeafInitHandler.getCacheDir() + "/EVENTMAP_"+scenario+".xml";
	}

	/**
	 * Sets the scenario bound to the BO
	 * @param scenarioName
	 */
	public void setScenario(String scenarioName) {
		this.scenario = scenarioName;
		
	}

}
