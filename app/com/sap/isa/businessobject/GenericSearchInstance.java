/*****************************************************************************
    Class:        GenericSearchInstance
    Copyright (c) 2006, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      25.04.2006
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.businessobject;

import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.maintenanceobject.businessobject.GSPropertyGroup;

//TODO docu
/**
 * The class GenericSearchInstance hold all information of one search screen instance. <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class GenericSearchInstance extends BusinessObjectBase {

	
	/**
	 * Search Result: Counted docs
	 */
	protected ResultData countedDocuments;
	
	/**
	 * Search Result: Found docs
	 */
	protected ResultData documents;
	
	/**
	 * Flag, if there are no select options available
	 */
	protected String noSelectOptionsAvailable = "false";
	
	/**
	 * Name of the screen. 
	 */
	protected String screenName = "";

	/**
	 * Flag, if the search should be started
	 */
	protected String startSearch = "false";
		
	/**
	 * Flag for the full screen mode.
	 */
	protected String fullScreenMode = "false"; 
	
	/**
	 * Property Group which is used for search. <br>
	 */
	protected GSPropertyGroup searchGroup; 

	/**
	 * Name of the result list; 
	 */
	protected String resultListName;
	
	
	/**
	 * Search Result: requested fields. <br>
	 */
	protected ResultData requestedFields;

	/**
	 * QueryString from request
	 */
	protected String queryString;
    
    /**
     * list of messages from backend
     */
    protected ResultData backendMessages;
	
	/**
	 * Return the property {@link #countedDocuments}. <br> 
	 *
	 * @return Returns the {@link #countedDocuments}.
	 */
	public ResultData getCountedDocuments() {
		return countedDocuments;
	}
	
	/**
	 * Set the property {@link #countedDocuments}. <br>
	 * 
	 * @param countedDocuments The {@link #countedDocuments} to set.
	 */
	public void setCountedDocuments(ResultData countedDocuments) {
		this.countedDocuments = countedDocuments;
	}
	
	/**
	 * Return the property {@link #documents}. <br> 
	 *
	 * @return Returns the {@link #documents}.
	 */
	public ResultData getDocuments() {
		return documents;
	}
	
	/**
	 * Set the property {@link #documents}. <br>
	 * 
	 * @param documents The {@link #documents} to set.
	 */
	public void setDocuments(ResultData documents) {
		this.documents = documents;
	}
	
	/**
	 * Return the property {@link #fullScreenMode}. <br> 
	 *
	 * @return Returns the {@link #fullScreenMode}.
	 */
	public String getFullScreenMode() {
		return fullScreenMode;
	}
	
	/**
	 * Set the property {@link #fullScreenMode}. <br>
	 * 
	 * @param fullScreenMode The {@link #fullScreenMode} to set.
	 */
	public void setFullScreenMode(String fullScreenMode) {
		this.fullScreenMode = fullScreenMode;
	}
	
	/**
	 * Return the property {@link #noSelectOptionsAvailable}. <br> 
	 *
	 * @return Returns the {@link #noSelectOptionsAvailable}.
	 */
	public String getNoSelectOptionsAvailable() {
		return noSelectOptionsAvailable;
	}
	
	/**
	 * Set the property {@link #noSelectOptionsAvailable}. <br>
	 * 
	 * @param noSelectOptionsAvailable The {@link #noSelectOptionsAvailable} to set.
	 */
	public void setNoSelectOptionsAvailable(String noSelectOptionsAvailable) {
		this.noSelectOptionsAvailable = noSelectOptionsAvailable;
	}
	
	/**
	 * Return the property {@link #screenName}. <br> 
	 *
	 * @return Returns the {@link #screenName}.
	 */
	public String getScreenName() {
		return screenName;
	}	
	
	/**
	 * Set the property {@link #screenName}. <br>
	 * 
	 * @param screenName The {@link #screenName} to set.
	 */
	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}
		
	/**
	 * Return the property {@link #searchGroup}. <br> 
	 *
	 * @return Returns the {@link #searchGroup}.
	 */
	public GSPropertyGroup getSearchGroup() {
		return searchGroup;
	}
	
	/**
	 * Set the property {@link #searchGroup}. <br>
	 * 
	 * @param searchGroup The {@link #searchGroup} to set.
	 */
	public void setSearchGroup(GSPropertyGroup searchGroup) {
		this.searchGroup = searchGroup;
	}
	
	/**
	 * Return the property {@link #startSearch}. <br> 
	 *
	 * @return Returns the {@link #startSearch}.
	 */
	public String getStartSearch() {
		return startSearch;
	}
	
	/**
	 * Set the property {@link #startSearch}. <br>
	 * 
	 * @param startSearch The {@link #startSearch} to set.
	 */
	public void setStartSearch(String startSearch) {
		this.startSearch = startSearch;
	}
	
	/**
	 * Return the property {@link #resultListName}. <br> 
	 *
	 * @return Returns the {@link #resultListName}.
	 */
	public String getResultListName() {
		return resultListName;
	}
	
	/**
	 * Set the property {@link #resultListName}. <br>
	 * 
	 * @param resultListName The {@link #resultListName} to set.
	 */
	public void setResultListName(String resultListName) {
		this.resultListName = resultListName;
	}
	
	/**
	 * Return the property {@link #queryString}.
	 * 
	 * @return Returns the {@link #queryString}.
	 */
	public String getQueryString() {
		return queryString;
	}
	/**
	 * Set the property {@link #queryString}.
	 * 
	 * @param queryString The {@link #queryString} to set
	 */
	public void setQueryString(String queryString) {
		this.queryString = queryString;
	}
	
	/**
	 * Return the property {@link #requestedFields}. <br> 
	 *
	 * @return Returns the {@link #requestedFields}.
	 */
	public ResultData getRequestedFields() {
		return requestedFields;
	}
	
	/**
	 * Set the property {@link #requestedFields}. <br>
	 * 
	 * @param requestedFields The {@link #requestedFields} to set.
	 */
	public void setRequestedFields(ResultData requestedFields) {
		this.requestedFields = requestedFields;
	}
    
    /**
     * gets the occured messages in the backend.
     * @return the list of occured messages in the backend
     */
	public ResultData getBackendMessages() {
		return backendMessages;
	}

    /**
     * sets the occured messages in the backend.
     * @param list of occured messages in the backend.
     */
	public void setBackendMessages(ResultData data) {
		backendMessages = data;
	}

}
