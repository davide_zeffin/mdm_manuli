package com.sap.isa.isacore;

/**
 * Title:        
 * Description:  
 * Copyright:    Copyright (c) 2002
 * Company:      
 * @author 
 * @version 1.0
 */
 /**
  * Currently holds all the orders contained within the mass download orders
  * it is similiar to the shopping cart for the orders that needs to be downloaded
  * 
  */

import java.util.HashMap;

import com.sap.isa.businessobject.DocumentState;
public class MassOrderDownloadBean   implements DocumentState{
  HashMap sdsMap = new HashMap();
  public MassOrderDownloadBean() {
  }
  int state;
  /**
   * Adds the lean sales document information into the bean
   */
  public void addSalesDocument(String guid,String sdnum,String state,String desc,String orderDate) {
    if(guid ==null) throw new IllegalArgumentException("Guid cannot be null");
    /**
     * Do nothing if already exist
     */
    if(sdsMap.containsKey(guid)) return;
    LeanSDObject lsdo = new LeanSDObject(guid,sdnum,state,desc,orderDate);
    sdsMap.put(guid,lsdo);
  }
  /**
   * removes the lean sales document information from the bean
   */
  public void removeSalesDocument(String guid) {
    /**
     * Do nothing if already exist
     */
     sdsMap.remove(guid);
  }
  /**
   * removes the lean sales document information from the bean
   */
  public void removeAllSalesDocuments() {
    /**
     * Do nothing if already exist
     */
     sdsMap.clear();
  }


  public void setState(int state) {
    this.state = state;
  }
  public int getState() {
    return this.state;
  }
  /**
   * Inner Sales document class that represents the lean order object
   * We can also use the order object as such
   */
  public class LeanSDObject {
    String sdNum;
    String status;
    String desc;
    String guid;
    String orderDate;
    
    public String getGUID(){
      return guid;
    }
    public String getSDNum(){
      return sdNum;
    }
    public String getStatus(){
      return status;
    }
    public String getDesc(){
      return nonNullString(desc);
    }
    public String getDate(){
      return nonNullString(orderDate);
    }
    public LeanSDObject(String guid,String sdNum,String status,String desc,String orderDate) {
      this.guid = guid;
      this.sdNum = sdNum;
      this.status = status;
      this.desc = desc;
      this.orderDate = orderDate;
    }
  }
 /**
  * Get all the orders contained within this bean
  */
 public  HashMap getSalesOrders() {
  return sdsMap;
 }
 private String nonNullString(String str) {
 	if(str!=null) return str;
 	return "";
 }
}