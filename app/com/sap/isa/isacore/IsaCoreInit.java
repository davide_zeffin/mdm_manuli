/*****************************************************************************
  Copyright (c) 2000, SAP AG, Germany, All rights reserved.
  Creator:      SAP
  Created:      03 April 2001

  $Id:
  $Revision: #1 $
  $Change: 22942 $
  $DateTime: 2001/06/26 10:29:54 $
  $Author: SAP $
*****************************************************************************/
package com.sap.isa.isacore;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import org.apache.struts.util.MessageResources;

import com.sap.isa.core.init.InitializationEnvironment;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;


/**
 * This class holds some attributes and properties needed for running the isacore-components
 * of the webapp
 *
 * @author      Alexander Staff
 * @version     1.0
 */
public class IsaCoreInit {
    // the init-class itself
    private static IsaCoreInit ici = null;

    // some messages
    private static MessageResources theMessageResources;

    // the logging instance
    private IsaLocation log;

    // the attribute list
    private List attributeExclusionList = null;

    // the multiple attribute search list
    private HashMap multipleAttrSearchMap = null;
    
    // the maximum default value for quantities in the java basket
    private static final String DEF_MAX_VALUE = "99999999";
     
    // the maximum value for quantities in the java basket
    private String maxQuantJavaBasket = DEF_MAX_VALUE; 

    /**
     * Default constructor for the singleton instance.
     *
     * @param messResources the message resources from the environment
     * @param aProps        the properties from the initialization environment
     */
    private IsaCoreInit(InitializationEnvironment env, Properties props) {
        // initialize the logging instance
        log = IsaLocation.getInstance(this.getClass().getName());

        if (props != null) {
            attributeExclusionList     = new ArrayList();
            multipleAttrSearchMap      = new HashMap();

            // read all the properties
            Enumeration e             = props.propertyNames();
            String      key;
            String      exclPrefix    = "attr.exclude";
            int         exclPrefixLen = exclPrefix.length();

            String      multipleAttrPrefix = "iqs.multiplecol";

            while (e.hasMoreElements()) {
                key = (String) e.nextElement();

                if (key.startsWith(exclPrefix) == true) {
                    attributeExclusionList.add(props.getProperty(key));

                    if (log.isDebugEnabled()) {
                        log.debug("added attribute : " + props.getProperty(key)
                            + " to the attrExclusionList");
                    }
                }
                else if (key.startsWith(multipleAttrPrefix) == true) {
                    String attribute = key.substring(key.lastIndexOf('.') + 1);
                    multipleAttrSearchMap.put(attribute, props.getProperty(key));

                    if (log.isDebugEnabled()) {
                        log.debug("added attribute : " + attribute
                            + " with attriblist : " + props.getProperty(key)
                            + " to the multipleAttrList");
                    }
                }
                else if (key.equals("max.basket.input.quantity")) {
                    setMaxQuantJavaBasket(props.getProperty(key));
                    if (log.isDebugEnabled()) {
                        log.debug("read parameter max.basket.input.quantity from init-config. value is : + " + props.getProperty(key));
                    }
                }
            }
        }
    }

    /**
     * Returns the singleton instance of the isaCoreInit-thing.
     *
     * @return the singleton instance or null
     */
    public static IsaCoreInit getInstance() {
        if (ici == null) {
            if (theMessageResources != null) {
                // some error ?
            }
        }

        return ici;
    }

    /**
     * Reads the initialization properties from the given properties and configures
     * the IsaCoreInit-thing instance accordingly.
     *
     * @param env    the calling environment
     * @param props  the properties from the initalization process
     */
    public static boolean initialize(InitializationEnvironment env,
        Properties props) {
        if (ici == null) {
            ici = new IsaCoreInit(env, props);
        }
        else {
            // error
            return false;
        }

        return true;
    }

    /**
     * Returns the exclusionlist created from the attributes in the ini-file
     *
     * @return The exclusionlist
     */
    public List getAttributeExclusionList() {
        return attributeExclusionList;
    }

    /**
     * Returns the list of attributes, that are configured to search in multiple
     * columns of the index
     *
     * @return The multipleAttrSearchList
     */
    public HashMap getMultipleAttrSearchMap() {
        return multipleAttrSearchMap;
    }
    
    /**
     * returns the maximum value for quantities in the java-basket as a String
     * 
     * @return The value of the attribute max.basket.input.quantity in the file init-config.xml
     */
    public String getMaxQuantJavaBasket() {
        return maxQuantJavaBasket; 
    }
    /**
     * sets the value for the maximum quantity in the java basket. checks for several errors
     * or invalid values
     * @param string
     */
    public void setMaxQuantJavaBasket(String string) {
        double value;
        
        if (log.isDebugEnabled()) {
            log.debug("try to set parameter maxQuantJavaBasket to value : " + string);
        }
        if (string != null && string.trim().length() > 0) {
            try {
                value = Double.parseDouble(string);
                maxQuantJavaBasket = string;
            }
            catch (NumberFormatException nfex) {
                log.error(LogUtil.APPS_COMMON_CONFIGURATION, "invalid value in init-config.xml : attribute max.basket.input.quantity = " + string);
                maxQuantJavaBasket = DEF_MAX_VALUE;
            }
        }
        else {
            maxQuantJavaBasket = DEF_MAX_VALUE;
        }
    }
}