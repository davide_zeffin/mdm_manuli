/*****************************************************************************
    Class:        ContextBase
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      27.02.2004
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.isacore;

import javax.servlet.http.HttpServletRequest;

import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.StartupParameter;
import com.sap.isa.helpvalues.HelpValuesContextSupport;

/**
 * The class ContextBase offers an easy access to all page context 
 * relevant objects . <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class ContextBase implements HelpValuesContextSupport {

	/**
	 * Reference to the IsaLocation. <br>
	 *
	 * @see com.sap.isa.core.logging.IsaLocation
	 */
	static protected IsaLocation log = IsaLocation.
		  getInstance(ContextBase.class.getName());

	/** 
	 * Http request. <br>
	 */
	protected HttpServletRequest request;


	/**
	 * user session Data object
	 */
	protected UserSessionData userSessionData;

	/**
	 * startup parameter
	 */
	protected StartupParameter startupParameter;


	/**
	 * Reference to the MetaBom object; 
	 */
	protected MetaBusinessObjectManager mbom;


    /**
     * 
     */
    public ContextBase() {
        super();
        // TODO Auto-generated constructor stub
    }


	/**
	 * Overwrites the method . <br>
	 * 
	 * @param pageContext
	 * 
	 * 
	 * @see com.sap.isa.helpvalues.HelpValuesContextSupport#initContext(javax.servlet.jsp.PageContext)
	 */
	public void initContext(HttpServletRequest request) {

		// get the request
		this.request = request;
	   
		// get user session data object
		userSessionData =  UserSessionData.getUserSessionData(request.getSession());
       
       
  	 	startupParameter = (StartupParameter)userSessionData.getAttribute(SessionConst.STARTUP_PARAMETER);

		mbom = userSessionData.getMBOM();

	}

	
    /**
     * Return the property {@link #request}. <br>
     * 
     * @return {@link #request}
     */
    public HttpServletRequest getRequest() {
        return request;
    }



    /**
     * Return the property {@link #mbom}. <br>
     * 
     * @return {@link #mbom}
     */
    public MetaBusinessObjectManager getMbom() {
        return mbom;
    }


    /**
     * Return the property {@link #startupParameter}. <br>
     * 
     * @return {@link #startupParameter}
     */
    public StartupParameter getStartupParameter() {
        return startupParameter;
    }



	/**
	   * Return the property {@link #userSessionData}. <br>
	   * 
	   * @return {@link #userSessionData}
	   */
	public UserSessionData getUserSessionData() {
		return userSessionData;
	}

	
	

}
