/*****************************************************************************
    Class:        AddressConfigurationBase
    Copyright (c) 2006, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      28.06.2006
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.isacore;

import java.util.HashMap;
import java.util.Map;

import com.sap.isa.backend.boi.isacore.AddressConfiguration;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.core.util.table.Table;


//TODO docu
/**
 * The class AddressConfigurationBase . <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
abstract public class AddressConfigurationBase {

	/**
	  * use this for logging
	  */
	 static protected IsaLocation log = IsaLocation.
										getInstance(AddressConfigurationBase.class.getName());
	
	protected int addressFormat;
	
	protected String defaultCountry;
 	
	protected ResultData countryList;
	protected ResultData titleList;
	
	protected Map countryDescriptions = new HashMap();
	
	protected Map regionTableMap = new HashMap();
	protected Map personTitleKeys = new HashMap();

		    
	/**
	 * Set the property addressFormat
	 *
	 * @param addressFormat
	 *
	 */
	public void setAddressFormat(int addressFormat) {
		this.addressFormat = addressFormat;
	}


	/**
	 * Returns the property addressFormat
	 *
	 * @return addressFormat
	 *
	 */
	public int getAddressFormat() {
	   return addressFormat;
	}

	/**
	 * Set the property defaultCountry
	 *
	 * @param defaultCountry
	 */
	public void setDefaultCountry(String defaultCountry) {
		this.defaultCountry = defaultCountry;
	}


	/**
	 * Returns the property defaultCountry
	 *
	 * @return defaultCountry
	 */
	public String getDefaultCountry() {
		return defaultCountry;
	}


	/**
	  * Create the property countryList
	  *
	  * @param countryTable
	  */
	 public void createCountryList(Table countryTable) {
		 final String METHOD = "createCountryList()";
		 log.entering(METHOD);
		 this.countryList = new ResultData(countryTable);

		 countryList.beforeFirst();

		 while (countryList.next()) {
			 countryDescriptions.put(countryList.getString("ID"),
									 countryList.getString("DESCRIPTION"));
		 }

		 countryList.beforeFirst();
		 log.exiting();
	 }


	/**
	 * add the regionTable for the country <code>country</code> to regionTableMap
	 *
	 * @param regionTable
	 * @param country to which the regions belong
	 */
	public void addRegionList(Table regionTable, String country) {
		regionTableMap.put(country,new ResultData(regionTable));
	}


	/**
	 * Returns the regionList for a given country
	 *
	 * @param country country for which the region list should return
	 *
	 * @return regionList the <code>ResultData regionList</code> has two entries
	 *          <ul>
	 *          <li>ID id of the region </li>
	 *          <li>DESCRIPTION description of the region </li>
	 *          </ul>
	 *          <i>The name of columns are defined as constants in ShopData class </i>
	 */
	protected abstract ResultData readRegionList(String country) throws CommunicationException;


	/**
	 * Returns the regionList for a given country
	 *
	 * @param country country for which the region list should return
	 *
	 * @return regionList the <code>ResultData regionList</code> has two entries
	 *          <ul>
	 *          <li>ID id of the region </li>
	 *          <li>DESCRIPTION description of the region </li>
	 *          </ul>
	 *          <i>The name of columns are defined as constants in ShopData class </i>
	 */
	public ResultData getRegionList(String country)
			throws CommunicationException {
		final String METHOD = "getRegionList()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("country = "+country);
       
       
	   ResultData regionList = (ResultData)regionTableMap.get(country);
	   if (regionList == null) {
	   		regionList = readRegionList(country);
		    regionTableMap.put(country,regionList);
	   }
	   log.exiting();
	   return regionList;
	}



	/**
	 * Create the property titleList
	 *
	 * @param titleTable
	 */
	public void createTitleList(Table titleTable) {
		final String METHOD = "createTitleList()";
		log.entering(METHOD);
		this.titleList = new ResultData(titleTable);

		// Get the titleTable entries that describe a person
		// and store them in a map for simple retrival of the
		// data
		// Reset the result set
		titleList.beforeFirst();

		while (titleList.next()) {
			if (titleList.getBoolean(AddressConfiguration.FOR_PERSON)) {
				personTitleKeys.put(
					titleList.getString(AddressConfiguration.ID),
					titleList.getString(AddressConfiguration.DESCRIPTION));
			}
		}

		// Reset the result set
		titleList.beforeFirst();
		log.exiting();
	}



	/**
	 * Returns the property titleList
	 *
	 ** @return titleList  the <code>ResultData titleList</code> has four entries
	 *          <ul>
	 *          <li>ID id of the region </li>
	 *          <li>DESCRIPTION description of the country </li>
	 *          <li>FOR_PERSON title is used for persons </li>
	 *          <li>FOR_ORGANISATION title is used for organisation </li>
	 *          </ul>
	 *          <i>The name of columns are defined as constants in ShopData class </i>
	 */
	public ResultData getTitleList() {
		return titleList;
	}

	


}
