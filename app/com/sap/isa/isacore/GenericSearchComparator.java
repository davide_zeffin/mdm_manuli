 /*****************************************************************************
    Class:        GenericSearchComparator
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author        SAP
*****************************************************************************/
package com.sap.isa.isacore;

import java.util.Comparator;
import java.util.Locale;
import java.util.StringTokenizer;

import com.sap.isa.core.util.WebUtil;

/**
 * Comparator for the Generic Search Framework
 * <p>
 * Part of the generic search framework.
 * </p>
*/
public class GenericSearchComparator implements  Comparator {
    
    /**
     * Request an acending sort sequence
     */
    public static final int ASCENDING = 1;
    /**
     * Request an descending sort sequence
     */
    public static final int DESCENDING = -1;
    /**
     * Key to set and retrieve the translation prefix (XML file)
     */
    public static final String TRANSLATEPREFIX = "gssort.translate.prefix";
    // Sorting sequence to be used
    protected int sortSeq = ASCENDING;
    protected Locale locale = null;
    protected String translationPrefix = "";
    // Type of the data which should be sorted
    protected String objType = "";
    protected String dateTmp = "";
    protected String separator = "";
    protected char separatorToRemove = ' ';
    /**
     * Constructor
     * @param int Sorting sequence ascending or descending
     */
    public GenericSearchComparator(int sortSequence) {
        sortSeq = sortSequence;
    }
    /**
     * Constructor for datas of a certain type (i.e date)
     * @param int Sorting sequence ascending or descending
     * @param String object type
     * @param String Template representing the format of the date (i.e yyyy.mm.dd)
     * @param String Template representing the format of the numbers (i.e #,###,###.##)
     * @param String language in which the string (object type 'translate') should be translated (in conjunction the translation prefix)
     * @param String Translation prefix (object type 'translate' only)
     */
    public GenericSearchComparator(int sortSequence, 
                                   String objType,
                                   String dateTemplate,
                                   String numberTemplate,
                                   String language,
                                   String translationPrefix) {
        this.objType = objType;
        this.sortSeq = sortSequence;
        if (language != null) {
            locale = new Locale(language.toLowerCase());
        } else {
            locale = new Locale("en");
        }
        this.translationPrefix = translationPrefix;
        if ("date".equals(objType)            ||
            "date_schedlines".equals(objType)  ) {     
            dateTmp = dateTemplate.toUpperCase();
            int sepIdx = dateTmp.indexOf(".");
            if (sepIdx > -1) {
                separator = ".";
            }
            sepIdx = dateTmp.indexOf("-");
            if (sepIdx > -1) {
                separator = "-";
            }
            sepIdx = dateTmp.indexOf("/"); 
            if (sepIdx > -1) {
                separator = "/";
            }
        } 
        if ("number".equals(objType)) {
            int comLoc = numberTemplate.indexOf(",");
            int pointLoc = numberTemplate.indexOf(".");
            if (comLoc < pointLoc) {
                separatorToRemove = ',';
            } else {
                separatorToRemove = '.';
            }
        }
        
    }
    /**
     * Compare method
     */
    public int compare(Object o1, Object o2) 
      throws CanNotCompareException {
        int ret = 0;
        String s1 = "";
        String s2 = "";
        Float f1 = null;
        Float f2 = null;
        
        if ( !(o1 instanceof String)  ||
             !(o2 instanceof String) ) {
            throw new CanNotCompareException("One of the objects is not a String");
        }
        if ("date".equals(objType)) {
            // To Compare dates, first convert to yyyymmdd
            s1 = formatDateL((String)o1);
            s2 = formatDateL((String)o2);
        
        }else if ("number".equals(objType)) {
            // To Compare number
            f1 = getFloat((String)o1);
            f2 = getFloat((String)o2);
            
        } else if ("date_schedlines".equals(objType)) {
            // To Compare dates, first convert to yyyymmdd
            s1 = formatDateL(getFirstSchedlineDate((String)o1));
            s2 = formatDateL(getFirstSchedlineDate((String)o2));

        } else if ("translate".equals(objType)) {
            s1 = WebUtil.translate(locale, (translationPrefix + (String)o1), null);
            s2 = WebUtil.translate(locale, (translationPrefix + (String)o2), null);
        } else  {
            s1 = (String)o1;
            s2 = (String)o2;
        }
        if ( ! "number".equals(objType)) {
            ret = s1.toLowerCase().compareTo(s2.toLowerCase());
        } else {
            ret = f1.compareTo(f2);
        }

        
        return (ret * sortSeq);
    }
    /**
     * Returns the first date within multiple scheduling lines. i.g. backend delivers as one
     * string in format: <br>
     * 50 ST - 08.10.2005 &lt;br/&gt;20 ST - 10.10.2005 &lt;br/&gt;...
     * @param String including the backend given string
     * @return String including a date
     */
    private String getFirstSchedlineDate(String inStr) {
		String retVal = "";
		if (inStr != null && inStr.length() > 0) {
			retVal = inStr.substring( (inStr.indexOf("-") + 1), (inStr.indexOf("<br/>") -1) );
		}
        return retVal;
    }
    /**
     * Convert given number String into float object
     * @param String number String
     * @return Float from number String
     */
    protected Float getFloat(String inNum) {
        String newNum = inNum.replace(separatorToRemove, '0');
        Float retVal = null;
        if (separatorToRemove == '.' ) { 
            // To convert to Float decimal separator must be '.'.
            newNum = newNum.replace(',', '.');
        }
        try {
            retVal = new Float(newNum);
        } catch(NumberFormatException numEx) {
            retVal = new Float(0);
        }
        return retVal;
    }
    /**
     * Convert given Date String into format yyyymmdd
     * @param String date string
     * @return String date string in format yyyymmdd
     */
    protected String formatDateL(String inDate) {
        StringTokenizer inST = new StringTokenizer(inDate, separator);
        StringTokenizer tmST = new StringTokenizer(dateTmp, separator);
        String dd   = "",
               mm   = "",
               yyyy = "";
        if (inDate.indexOf(separator) > 0) {
            // Convert only if given date actually contains the separator
            // given from the template (could be different if user settings
            // and shop settings doesn't fit together).
            while (tmST.hasMoreElements()) {
              String tokTM = (String)tmST.nextElement();
              String tokIN = (String)inST.nextElement();
              if ("DD".equals(tokTM)) {
                  if (tokIN.length() == 1) {
                      dd = "0" + tokIN;
                  } else {
                      dd = tokIN;
                  }
              }
              if ("MM".equals(tokTM)) {
                  if (tokIN.length() == 1) {
                      mm = "0" + tokIN;
                  } else {
                      mm = tokIN;
                  }
              }
              if ("YYYY".equals(tokTM)) {
                  yyyy = tokIN;
              }

            }
        }
        String outDate = yyyy + mm + dd;
        return outDate;
    }
    
    /**
     * Exception thrown if the two objects could not be compare because of
     * unknown object types
     *
     */
    public static class CanNotCompareException extends RuntimeException {
        public CanNotCompareException() {
            super();
        }

        public CanNotCompareException(String msg) {
            super(msg);
        }
    }
    
}