/**
 *  Class:        HistoryItem
 *  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
 *  Author:       Franz-Dieter Berger
 *  Created:      25.05.2001
 *  Version:      1.0
 *  $Revision: #2 $
 *  $Date: 2001/07/31 $
 */

package com.sap.isa.isacore;


/**
 * History items are the elements of the history. They store information, which
 * can be displayed in the history bar and know the target of the hyperlink.
 * @see History
 * @see ManagedDocument
 *@author     Franz-Dieter Berger
 *@created    3. Dezember 2001
 *@version    1.0
 */


public class HistoryItem {

    /**
     *  Type of the document, e.g. "order", "quotation", "order template".
     */
    protected String docType;

    /**
     *  Number of the document, given by the backend.
     */
    protected String docNumber;

    /**
     *  Reference number; the number the customer can assign to the document.
     */
    protected String refNumber;

    /**
     *  Reference name; the name the customer can assign to the document.
     */
    protected String refName;

    /**
     *  Date of the document; this date can be a creation date, a
     *  validity date or another useful date.
     */
    protected String docDate;

    /**
     *  href for displaying the document. See getHref().
     */
    protected String href;

    /**
     *  hrefparameter for displaying the document. See getHrefParameter().
     */
    protected String hrefParameter;


    /**
     *  Constructor
     *
     *@param  docType        Type of the document, e.g. "order", "quotation", "order template".
     *@param  docNumber      Number of the document, given by the backend.
     *@param  refNumber      Reference number; the number the customer can assign to the document.
     *@param  refName        Reference name; the name the customer can assign to the document.
     *@param  docDate        Date of the document; this can be a creation date, a validity date or another useful date.
     *@param  href           href for displaying the document. See getHref().
     *@param  hrefParameter  hrefparameter for displaying the document. See GetHrefParameter().
     */
    public HistoryItem(String docType, String docNumber, String refNumber,
            String refName, String docDate, String href, String hrefParameter) {
        this.docType = docType;
        this.docNumber = docNumber;
        this.refNumber = refNumber;
        this.refName = refName;
        this.docDate = docDate;
        this.href = href;
        this.hrefParameter = hrefParameter;
    }


    /**
     *  Returns the type of the document, e.g. "order", "quotation", "order template".
     *
     *@return    type of the document as a String
     *  <code>null</code>, if the document type is not set
     */
    public String getDocType() {
        return docType;
    }


    /**
     *  Returns the number of the document, given by the backend.
     *
     *@return    number of the document as a String
     *  <code>null</code>, if the number is not set
     */
    public String getDocNumber() {
        return docNumber;
    }


    /**
     *  Returns the reference number; the number the customer can assign to the document.
     *
     *@return    reference number of the document as a String
     *  <code>null</code>, if the reference number is not set
     */
    public String getRefNumber() {
        return refNumber;
    }


    /**
     *  Returns the reference name; the name the customer can assign to the document.
     *
     *@return    reference name of the document as a String
     *  <code>null</code>, if the reference name is not set
     */
    public String getRefName() {
        return refName;
    }


    /**
     *  Returns the date of the document; this date can be a creation date, a
     *  validity date or another useful date, which can be displayed in the
     *  history.
     *
     *@return    date of the document as a String
     *  <code>null</code>, if the date is not set
     */
    public String getDate() {
        return docDate;
    }


    /**
     *  When the history is displayed, the entries will be put in anchor tags.
     *  This function returns exactly the string, which is put into the href variable
     *  of the anchor tag. It should be an action with optionally some parameters,
     *  which creates and display the clicked document.
     *
     *@return    href for displaying the document
     *  <code>null</code>, if the href is not set. Then this document will not be
     *  clickable in the history list.
     */
    public String getHref() {
        return href;
    }


    /**
     *  This function returns exactly the string, which represents parameters like techkey
     *  objectid etc. which will be unique for each document.
     *  These parameters concated with the String of the getHref() method
     *  refer to the clickable document
     *
     *@return    hrefparameter for displaying the document
     *  <code>null</code>, if the href is not set. Then this document will not be
     *  clickable in the history list.
     */
    public String getHrefParameter() {
        return hrefParameter;
    }


    /**
     *  For the history two items are equal, if they have the same docType and the
     *  same docNumber. All other attributes may differ.
     *
     *@param  item  Description of Parameter
     *@return       Description of the Returned Value
     */
    public boolean equals(HistoryItem item) {
        if (item == null) {
            return false;
        }
        //a nonexisting item is unequal to an existing
        if (docType == null && docNumber == null) {
            if (item.getDocType() == null && item.getDocNumber() == null) {
                return true;
            }
            else {
                return false;
            }
        }
        else if (docType != null && docNumber == null) {
            if (item.getDocType() != null && docType.equals(item.getDocType()) && item.getDocNumber() == null) {
                return true;
            }
            else {
                return false;
            }
        }
        else if (docType == null && docNumber != null) {
            if (item.getDocType() == null && item.getDocNumber() != null && docNumber.equals(item.getDocNumber())) {
                return true;
            }
            else {
                return false;
            }
        }
        else if (docType != null && docNumber != null) {
            if (item.getDocType() != null && docType.equals(item.getDocType()) && item.getDocNumber() != null && docNumber.equals(item.getDocNumber())) {
                return true;
            }
            else {
                return false;
            }
        }
        return false;
    }
}
