/*****************************************************************************
    Class:        TabStripHelper
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP
    Created:      August 2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/08/01 $
*****************************************************************************/

package com.sap.isa.isacore;

import java.util.ArrayList;
import java.util.List;


/**
 * The TabStripHelper class is a helper class to display a tab strip
 * on a JSP. <br>
 *
 * All buttons which will be used in the tab strip will be added with the
 * {@link #addTabButton(String, String, String)} method.
 * Each button consists of an unique key, the resource key for
 * the text on the button and a link, which should be called if the button is 
 * pressed. <br>
 * The tab which is currently activated could be set with the 
 * {@link #setActiveButton(String)} method. 
 *
 * <p><b>Example</b>
 * <pre>TabStripHelper tabStrip = new TabStripHelper();
 *
 * &lt;%-- Add all buttons, which will be used --%&gt;
 *
 * &lt;%--Details Tab--&gt;
 * tabStrip.addTabButton("details",   // key 
 *                       "b2b.order.details.linkdetails", // resource key
 *                       "javacript:showDetails()");
 *
 * &lt;%--Notice Tab--&gt;
 * tabStrip.addTabButton("notes",
 *                       "b2b.order.details.linknote",
 *                        "javacript:showNotes()");
 *
 * &lt;%--set the active button with the key--%&gt;
 * tabStrip.setActiveButton("details");
 * </pre></p>
 *
 * To render the tab strip you use the include <code>\appbase\tabstrip.inc.jps</code>.
 * <p><b>Example</b>
 * <pre>
 * &lt;%@ include file="/appbase/tabstrip.inc.jsp" %&gt;
 * 	&lt;/ul&gt; &lt;%-- closing open brackets from the include --%&gt;
 * &lt;/div&gt;
 * </pre>
 * 
 * <p>It is also possible to render the tab strip manualy:<br>
 * With the {@link #hasNext} and {@link #next} methods it is possible to iterate 
 * over the buttons. 
 *
 * In the loop the following the methods 
 * <ul>
 * <li>{@link #isFirstButton} </li>
 * <li>{@link #isLastButton}</li>
 * <li>{@link #isActiveButton} </li>
 * <li>{@link #isNextButtonActive} </li>
 * <li>{@link #isPreviousButtonActive} </li>
 * </ul>
 * can be used to get information over the position within the strip to adjust 
 * the output.
 * </p>
 * <p><b>Example</b>
 * <pre>
 * TabStripHelper.TabButton tabButton;
 *
 * while (tabStrip.hasNext()) {
 *
 *   tabButton = tabStrip.next();
 * 
 *   // active button 
 *   if (tabStrip.isActiveButton()) {
 *		 
 *     // last button 	
 *     if (tabStrip.isLastButton()) {
 *
 *   ...
 *
 * }
 * </pre> </p>
 *
 * <i> see b2b/organizer_menu.jsp.inc for a full example </i>
 *
 * @author  SAP
 * @version 1.0
 *
 */

public class TabStripHelper {

    /**
     * A inner class of TabStripHelper to manage the buttons on the TabStrip
     *
     */
    public class TabButton{
        private String key     = "";
        private String text    = "";
        private String link    = "";
        private String custom1 = "";
        private String custom2 = "";


        public TabButton(String key, String text, String link) {
            this.key  = key;
            this.link = link;
            this.text = text;
        }

        /**
         * Set the property custom1
         *
         * @param custom1
         *
         */
        public void setCustom1(String custom1) {
            this.custom1 = custom1;
        }


        /**
         * Returns the property custom1
         *
         * @return custom1
         *
         */
        public String getCustom1() {
           return this.custom1;
        }


        /**
         * Set the property custom2
         *
         * @param custom2
         *
         */
        public void setCustom2(String custom2) {
            this.custom2 = custom2;
        }


        /**
         * Returns the property custom2
         *
         * @return custom2
         *
         */
        public String getCustom2() {
           return this.custom2;
        }


        /**
         * Set the property key
         *
         * @param key
         *
         */
        public void setKey(String key) {
            this.key = key;
        }


        /**
         * Returns the property key
         *
         * @return key
         *
         */
        public String getKey() {
           return this.key;
        }


        /**
         * Set the property link
         *
         * @param link
         *
         */
        public void setLink(String link) {
            this.link = link;
        }


        /**
         * Returns the property link
         *
         * @return link
         *
         */
        public String getLink() {
           return this.link;
        }


        /**
         * Set the property text
         *
         * @param text
         *
         */
        public void setText(String text) {
            this.text = text;
        }


        /**
         * Returns the property text
         *
         * @return text
         *
         */
        public String getText() {
           return this.text;
        }

    }


    private int index = -1;
    private List list = new ArrayList(10);
    private int activeIndex = 0;


    /**
     * Returns the next Button in the TabStrip
     * All imformations refer to this button.
     *
     * @return the next TabButton
     *
     */
    public TabButton next( ) {
        if (index <  list.size() - 1) {
            index++;
            return (TabButton) list.get(index);
        }
        else {
            return null;
        }
    }


    /**
     * Return the next Button as a preview.
     *
     * @return the next TabButton
     *
     */
    public TabButton previewNextButton( ) {
        if (index <  list.size() - 1) {
            return (TabButton) list.get(index+1);
        }
        else {
            return null;
        }
    }


    /**
     * returns if there is another button
     *
     * @return <code>true</code> if there is a button
     *
     */
    public boolean hasNext( ) {

        return index <  list.size() - 1 ? true : false;
    }



    /**
     * check if the current button is the last
     *
     * return if the current TabButton is the last one
     *
     * @return
     *
     */
    public boolean isLastButton( ) {

        return index == list.size() - 1 ? true : false;

    }



    /**
     * check if the current button is the first
     *
     * return <code>true</code> if the current TabButton is the first one
     *
     * @return
     *
     */
    public boolean isFirstButton( ) {

        return index == 0 ? true : false;

    }

    /**
     * check if the next button is the active one
     *
     * return if <code>true</code> the next TabButton is the active one
     *
     * @return
     *
     */
    public boolean isNextButtonActive( ) {

        return index + 1 == activeIndex ? true : false;

    }


    /**
     * check if the previous button is the active one
     *
     * return if <code>true</code> the previous TabButton is the active one
     *
     * @return
     *
     */
    public boolean isPreviousButtonActive( ) {

        return index - 1 == activeIndex ? true : false;

    }


    /**
     * check if the current button is the active one
     *
     *
     * @return if <code>true</code> the current TabButton is the active one
     *
     */
    public boolean isActiveButton( ) {

        return index == activeIndex ? true : false;

    }

 
   /**
     * Add a button to the TabStrip.
     *
     * @param tabButton  button to be added
     *
     */
    public void addTabButton(TabButton tabButton) {
        list.add(tabButton);
    }


    /**
     * add a button to the TabStrip
     *
     * @param key  unique key to identify the button
     * @param text text which should appear on the button
     * @param link should you in the href-tag to give a refernce
     *
     */
    public void addTabButton(String key, String text, String link) {
        list.add(createTabButton(key,text,link));
    }


   /**
     * Create a button. 
     *
     * @param key  unique key to identify the button
     * @param text text which should appear on the button
     * @param link should you in the href-tag to give a refernce
     *
     */
    public TabButton createTabButton(String key, String text, String link) {
        TabButton tabButton = new TabButton(key, text, link);
        return tabButton;
    }


    /**
     * Set the active button.
     *
     * @param key key of the active button
     *
     */
    public void setActiveButton(String key) {

        int size = list.size();
        activeIndex = 0;

        for (int i = 0; i < size; i++) {
            TabButton tabButton = (TabButton) list.get(i);
            if (tabButton.getKey().equals(key)) {
                activeIndex = i;
                return;
            }
        }

    }

	public int size()
	{
		return list.size();
	}



}
