/*****************************************************************************
  Copyright (c) 2000, SAP AG, All rights reserved.
  Author:       SAP AG
  Created:      21 March 2001

  $Revision: #2 $
  $Date: 2003/10/23 $
*****************************************************************************/

package com.sap.isa.isacore.actionform.b2c;

import org.apache.struts.action.ActionForm;

/**
 * FormBean that takes input data for User Authentification. Using the struts
 * framework every HTML Form is accompanied by a dedicated ActionForm bean to
 * store the data typed into the form. This Bean is used to communicate between
 * the Action-Servlet and the Action class of that form.<br>
 * The ActionForm Beans have to follow JavaBeans conventions (no argument
 * constructor, get/set-Methods).
 *
 * @author Biju Raj
 * @version 0.1
 */
public class LoginForm extends ActionForm {

  private String userId;
  private String password;
  private String language;

  /**
   * Set the user id.
   *
   * @param userId Id of the user who wants to log into the system
   */
  public void setUserId(String userId) {
    this.userId = userId;
  }

  /**
   * Read the user id.
   *
   * @return Id of the user who wants to log into the system
   */
  public String getUserId() {
    return userId;
  }

  /**
   * Set the user's password.
   *
   * @param passwort The passwort of the user
   */
  public void setPassword(String password) {
    this.password = password;
  }

  /**
   * Read the user's password
   *
   * @return Password of the user.
   */
  public String getPassword() {
    return password;
  }

  /**
   * Set the user's password.
   *
   * @param passwort The passwort of the user
   */
  public void setNolog_password(String password) {
	this.password = password;
  }

  /**
   * Read the user's password
   *
   * @return Password of the user.
   */
  public String getNolog_password() {
	return password;
  }

  /**
   * Set the language the user wants to use during the web application.
   *
   * @param language Language to be used. Use the well known locale strings
   *                 as defined in ISO-639.
   * @see Application Constants defined in class Application
   */
  public void setLanguage(String language) {
    this.language = language;
  }

  /**
   * Returns the language the user wants to use during the use of the
   * web application.
   *
   * @return Language to be used coded as defined in ISO-639.
   * @see Application Constants defined in class Application
   */
  public String getLanguage() {
    return language;
  }

  public LoginForm() {
  }

}