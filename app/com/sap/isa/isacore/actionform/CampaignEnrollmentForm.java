package com.sap.isa.isacore.actionform;

import org.apache.struts.action.ActionForm;

/**
 * Title:        CampagnEnrollmentForm
 * Description:  ActionForm containing input field data of one enrollment line to be updated
 *               from HTML form.
 *               For enrollment line tables this is enclosed in a CampaignEnrollmentPageForm
 * Copyright:    Copyright (c) 2006
 * Company:      SAP AG
 * @version 1.0
 */

public class CampaignEnrollmentForm extends ActionForm {

    /**
     * unique ID of enrollee, used to identify the business partner to be enrolled
     */
    private String enrolleeGUID;

    /**
     * flag whether the business partner is enrolled.
     */
    private String isEnrolled;

    /**
     * initialization
     */
    public CampaignEnrollmentForm() {
        this.enrolleeGUID = "";
        this.isEnrolled = "";
    }

    /**
     * Sets the enrolleeGUID 
     * @param newEnrolleeGUID as String
     */
    public void setEnrolleeGUID(String newEnrolleeGUID) {
        this.enrolleeGUID = newEnrolleeGUID;
    }

    /**
     * Returns the enrolleeGUID of the business partner.
     * @return enrolleeGUID as String
     */
    public String getEnrolleeGUID() {
        return this.enrolleeGUID;
    }

    /**
     * Sets the isEnrolled flag
     * @param newIsEnrolled as String
     */
    public void setIsEnrolled(String newIsEnrolled) {
        this.isEnrolled = newIsEnrolled;
    }

    /**
     * Returns the information if the business partner is enrolled.
     * @return isEnrolled as String
     */
    public String getIsEnrolled() {
        return this.isEnrolled;
    }

}