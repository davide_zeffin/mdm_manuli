/*****************************************************************************
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      28 March 2001

    $Revision: #1 $
    $Date: 2001/06/26 $
*****************************************************************************/
package com.sap.isa.isacore.actionform.b2b.contract;

import org.apache.struts.action.ActionForm;

import com.sap.isa.core.util.FormattedDate;

/**
 * FormBean that contains input data for selection of contracts within the struts
 * framework.
 */
public class ContractSearchForm extends ActionForm {
    private String attribute;
    private String attributeValue;
    private String validitySelection;
    private String period;
    private String month;
    private String year;
    private String status;
    private boolean selectRequested;
    public final static String[] MONTH = FormattedDate.MONTH;
    public final String[] YEAR = new String[5];

    /**
     * Constant indicating that no attribute has been specified.
     */
    public static final String NOT_SPECIFIED = "NULL";

    /**
     * Attribute of contract id.
     */
    public static final String ID = "A";

    /**
     * Attribute of external reference.
     */
    public static final String EXTERNAL_REF_NO = "B";

    /**
     * Attribute of contract description.
     */
    public static final String DESCRIPTION = "C";

    /**
     * Period mode of validity selection.
     */
    public static final String PERIOD = "period";

    /**
     * Date mode of validity selection.
     */
    public static final String DATE = "date";

    /**
     * Time span until end of day.
     */
    public final static String TODAY = "A";

    /**
     * Time span of 7 days from today.
     */
    public final static String NEXT_WEEK = "B";

    /**
     * Time span of 30 days from today.
     */
    public final static String NEXT_MONTH = "C";

    /**
     * Time span of 1 year from today.
     */
    public final static String NEXT_YEAR = "D";
    
    /**
     * Status of the contract.
     */
    public static final String ALL_STATUS = "all";
    
     /**
     * Status of the contract.
     */
    public static final String STATUS_IN_PROCESS = "inProcess";
    
    /**
     * Status of the contract.
     */
    public static final String STATUS_REJECTED = "rejected";
    
    /**
     * Status of the contract.
     */
    public static final String STATUS_QUOTATION = "quotation";  
    
        /**
     * Status of the contract.
     */
    public static final String STATUS_SENT = "inquirySent";  
    
    
    /**
     * Status of the contract.
     */
    public static final String STATUS_RELEASED = "released";   
    
    /**
     * Status of the contract.
     */
    public static final String STATUS_ACCEPTED = "accepted";             
    

    /**
     * Constructor. Sets all properties to their default values.
     */
    public ContractSearchForm() {
        setDefaultValues();
    }

    /**
     * Retrieves the attribute selection criterion.
     */
    public String getAttribute() {
        return attribute;
    }

    /**
     * Sets the attribute selection criterion.
     */
    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    /**
     * Indicates if given attribute has been chosen.
     */
    public boolean isAttribute(String attribute) {
        return this.attribute.equalsIgnoreCase(attribute);
    }

    /**
     * Retrieves the attribute selection criterion.
     */
    public String getAttributeValue() {
        return attributeValue;
    }

    /**
     * Sets the attribute selection criterion.
     */
    public void setAttributeValue(String attributeValue) {
        this.attributeValue = attributeValue;
    }

    /**
     * Retrieves the filter criterion for the contract id.
     */
    public String getId() {
        return attribute.equalsIgnoreCase(ID) ? attributeValue : "";
    }

    /**
     * Retrieves the filter criterion for the external reference number.
     */
    public String getExternalRefNo() {
        return attribute.equalsIgnoreCase(EXTERNAL_REF_NO) ?
            attributeValue : "";
    }

    /**
     * Retrieves the filter criterion for the language dependent description.
     */
    public String getDescription() {
        return attribute.equalsIgnoreCase(DESCRIPTION) ?
            attributeValue : "";
    }

    /**
     * Specifies if user has already requested a selection.
     */
    public void setSelectRequested(boolean selectRequested) {
        this.selectRequested = selectRequested;
    }

    /**
     * Retrieves the mode for validity selection.
     */
    public String getValiditySelection() {
        return validitySelection;
    }

    /**
     * Indicates if given validity selection mode has been chosen.
     */
    public boolean isValiditySelection(String validitySelection) {
        return this.validitySelection.equalsIgnoreCase(validitySelection);
    }

    /**
     * Sets the mode for validity selection.
     */
    public void setValiditySelection(String validitySelection) {
        this.validitySelection = validitySelection;
    }

    /**
     * Retrieves the mode for validity selection.
     */
    public String getPeriod() {
        return period;
    }

    /**
     * Indicates if given period has been chosen.
     */
    public boolean isPeriod(String period) {
        return this.period.equalsIgnoreCase(period);
    }

    /**
     * Sets the mode for validity selection.
     */
    public void setPeriod(String period) {
        this.period = period;
    }

    /**
     * Retrieves the mode for validity selection.
     */
    public String getMonth() {
        return month;
    }

    /**
     * Indicates if given month has been chosen.
     */
    public boolean isMonth(String month) {
        return this.month.equalsIgnoreCase(month);
    }

    /**
     * Sets the mode for validity selection.
     */
    public void setMonth(String month) {
        this.month = month;
    }

    /**
     * Retrieves the mode for validity selection.
     */
    public String getYear() {
        return year;
    }

    /**
     * Indicates if given year has been chosen.
     */
    public boolean isYear(String year) {
        return this.year.equalsIgnoreCase(year);
    }

    /**
     * Sets the mode for validity selection.
     */
    public void setYear(String year) {
        this.year = year;
    }

    /**
     * Indicates if user has already requested a selection.
     */
    public boolean isSelectRequested() {
        return selectRequested;
    }

    /**
     * Sets all property fields to their default values.
     */
    public void setDefaultValues() {
        FormattedDate date = new FormattedDate();

        attribute = NOT_SPECIFIED;
        attributeValue = "";
        status = ALL_STATUS;
        validitySelection = PERIOD;
        period = TODAY;
        month = date.getMonth();
        year = date.getYear();
        selectRequested = false;
        YEAR[0] = year;
        for (int i = 1; i < YEAR.length; i++) {
            date.addYears(1);
            YEAR[i] = date.getYear();
        }
    }

   /**
     * Indicates if given status has been chosen.
     */
    public boolean isStatus(String status) {
        return this.status.equalsIgnoreCase(status);
    }
    
    
	/**
	 * Returns the status.
	 * @return String
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 * @param status The status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

}