package com.sap.isa.isacore.actionform.ccms;

import com.sap.isa.core.ccms.HeartbeatCustomizingForm;
import com.sap.isa.core.xcm.XCMTestProperties;

/**
 * Form for CCMS Heartbeat customzing for b2b and b2b 
 * This form has an additional parameter shopid, which is required for the IMSTest
 *
 *  @see com.sap.isa.core.ccms.HeartbeatCustomizingForm
 */
public class B2xHeartbeatCustomizingForm extends HeartbeatCustomizingForm {
	final static public String IMS_COMPONENTID="IMS";
	
	class B2xScenario extends HeartbeatCustomizingForm.Scenario
	{
		private boolean imsTestSelected=false;
		B2xScenario(String name )
		{
			super(name);
		}
		boolean getImsTestSelected()
		{
			return imsTestSelected;
		}
		
		void setImsTestSelected(boolean newValue)
		{
			imsTestSelected=newValue;
		}
		/**
		 * Append IMS Test to list of tests for a given scenario
		 * @return XCMTestProperties[]
		 */
		protected XCMTestProperties[] addScenarioTests() {
			if(imsTestSelected)
			{
				XCMTestProperties[] tests = new XCMTestProperties[1];
				tests[0]=new XCMTestProperties();
				tests[0].setComponentID(IMS_COMPONENTID);
				return tests;
			}
			return null;
		}
	};
	/**
	 * Constructor for B2xHeartbeatCustomizingForm.
	 */
	public B2xHeartbeatCustomizingForm() {
		super();
	}
	/**
	 * Sets shopId for the given index
	 */
    public void setImsTestSelected(int idx, String newValue)
    {
		boolean value=( newValue.length() != 0);
       ((B2xScenario)scenarios.get(idx)).setImsTestSelected(value);
    }

	public String getImsTestSelected(int idx)
	{
		if(((B2xScenario)scenarios.get(idx)).getImsTestSelected())
		{
			return "on";
		}
		return null;
	}
	/**
	 * Adds a B2xScenario instead of a Scenario
	 * The b2xScenarion has an additional attribute shopId
	 */
    public void setScenariosName(int idx, String newValue)
    {
   	  scenarios.add( idx,new B2xScenario(newValue));
	}

}
