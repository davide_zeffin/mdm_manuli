/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Alexander Staff
  Class         QuickSearchForm
  Description:  The ActionForm to validate and pre-purge the input in the
                QuickSearch mask

  Created:      16 March 2001
  Version:      0.1

  $Revision: #1 $
  $Date: 2001/06/26 $
*****************************************************************************/

package com.sap.isa.isacore.actionform;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * FormBean that takes input data for the QuickSearch's input fields. Using the struts
 * framework every HTML Form can be accompanied by a dedicated ActionForm bean to
 * store the data typed into the form. This Bean is used to communicate between
 * the Action-Servlet and the Action class of that form.<br>
 * The ActionForm Beans have to follow JavaBeans conventions (no argument
 * constructor, get/set-Methods).
 *
 * @author Alexander Staff
 * @version 0.1
 */
public class QuickSearchForm extends ActionForm {

    private String      attribute;
    private String      searchexpr;
    private int         displayHits;  // number of hits to display on one page
    private int         maxHits;

    /**
     *  Constructor comes first
     */
    public QuickSearchForm() {
        attribute       = null;
        searchexpr      = null;
        displayHits     = -1;  // -1 means error, zero means "display all items"
        maxHits         = -1;  // -1 means error, zero means "return all items"
    }

    /**
     * Read the attribute.
     *
     * @return attribute the selected attribute(s)
     */
    public String getAttribute() {
        return this.attribute;
    }

    /**
     * Set the attribute from the select box.
     * The input listbox for the list of attributes can hold only one selected item.
     *
     * @param attribute. the selected item
     */
    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    /**
     * Read the number of hits to display.
     *
     * @return displayHits. The number of hits to display on one page
     */
    public int getDisplayHits() {
        return this.displayHits;
    }

    /**
     * Set the maximum of hits to display with this property
     *
     * @param displayHits. Maximum Number of hits to display on one page
     */
    public void setDisplayHits( int displayHits ) {
        this.displayHits = displayHits;
    }

    /**
     * Read the number of hits to read from the catalog engine.
     *
     * @return maxHits. The number of hits to read
     */
    public int getMaxHits() {
        return this.maxHits;
    }

    /**
     * Set the maximum of hits to read from the catalog engine with this property
     * TODO:start Some time in the future we should provide the check against a
     * useful maximum count of maxHits, provided from some catalog configuration file
     * or something like this
     * Until now this value is set in the JSP calling this form : organizer-content-product-search.jsp
     * TODO:end
     *
     * @param maxHits. Maximum Number of hits to read
     */
    public void setMaxHits( int maxHits ) {
        this.maxHits = maxHits;
    }

    /**
     * Get the searchexpr entered in the appropriate input field
     *
     * @return searchexpr. The searchexpr
     */
    public String getSearchexpr() {
        return this.searchexpr;
    }

    /**
     * Set the searchexpr
     *
     * @param searchexpr. The expression to search for
     */
    public void setSearchexpr( String searchexpr ) {
        this.searchexpr = searchexpr;
    }

    /**
     * The validation for the ActionForm
     *
     * @return errors that occurred during the validation
     */
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

        ActionErrors errors = new ActionErrors();
        return errors;

        // don't use this right now, maybe later
        // I check the input params in the Action
        // this simplifies the handling of invlid parameters
/*        if ( displayHits <= 0 ) {
            errors.add("displayHits", new ActionError( "a", "displayHits"));
        }

        if ( maxHits <= 0 ) {
            errors.add("maxHits", new ActionError("b", "maxHits"));
        }

        if ((attribute == null) || (attribute.length() < 1) ) {
            errors.add("attribute", new ActionError("c", "attribute"));
        }
        // a little bit hacky here
        // I should try to find a more elegant solution
        else if( attribute.equalsIgnoreCase("NULL") == true || attribute.equalsIgnoreCase("ohne_Merkmal") == true ) {
            errors.add("attribute", new ActionError("c", "attribute"));
        }

        if ((searchexpr == null) || (searchexpr.length() < 1)) {
            errors.add("searchexpr", new ActionError("d", "searchexpr"));
        }

        return errors; */
    }

}