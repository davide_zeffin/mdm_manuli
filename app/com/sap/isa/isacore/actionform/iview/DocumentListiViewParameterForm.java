/*****************************************************************************
    Class:        DocumentListiViewParameterForm
    Copyright (c) 2002, SAP AG, Germany, All rights reserved.
    Author        Timo Kohr
    @version      0.1
    Created:      13 August 02

*****************************************************************************/

package com.sap.isa.isacore.actionform.iview;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.order.DocumentListFilter;

/**
 * FormBean that takes input data for document list selection. Using the struts
 * framework every HTML Form is accompanied by a dedicated ActionForm bean to
 * store the data typed into the form. This Bean is used to communicate between
 * the Action-Servlet and the Action class of that form.<br>
 * The ActionForm Beans have to follow JavaBeans conventions (no argument
 * constructor, get/set-Methods).
 *
 * @author Timo Kohr
 * @version 0.1
 */
public class DocumentListiViewParameterForm extends ActionForm implements Cloneable {
  /**
   * Order
   */
  public static final String ORDER = DocumentListFilter.SALESDOCUMENT_TYPE_ORDER;
  /**
   * OrderTemplate
   */
  public static final String ORDERTEMPLATE = DocumentListFilter.SALESDOCUMENT_TYPE_ORDERTEMPLATE;
  /**
   * Quotation
   */
  public static final String QUOTATION = DocumentListFilter.SALESDOCUMENT_TYPE_QUOTATION;
  /**
   * Invoice
   */
  public static final String INVOICE = DocumentListFilter.BILLINGDOCUMENT_TYPE_INVOICE;
  /**
   * DownPayment
   */
  public static final String DOWNPAYMENT = DocumentListFilter.BILLINGDOCUMENT_TYPE_DOWNPAYMENT;
  /**
   * CreditMemo
   */
  public static final String CREDITMEMO = DocumentListFilter.BILLINGDOCUMENT_TYPE_CREDITMEMO;
  /**
   * ServiceRequest
   */
  public static final String SERVICEREQUEST = "S1";
  /**
   * InfoRequest
   */
  public static final String INFOREQUEST = "S3";
  /**
   * Complaints
   */
  public static final String COMPLAINTS = "CRMC";
  /**
   * ServiceContract
   */
  public static final String SERVICECONTRACT = "SC";


  /**
   * Name of FORM with the Document selection criteria
   */
  public static final String DOCUMENTIVIEWFORM = "dociviewform";
  /**
   * Name of default portal DocumentType
   */
  public static final String DOCUMENT_TYPE = "doc_type";

  /**
   * Name of default iView DocumentType
   */
  public static final String IVIEW_DOCUMENT_TYPE = "iviewDocType";

  /**
   * Name of default Portal Shop URL
   */
  public static final String PORTAL_SHOP_URL = "portalurl";
  /**
   * Name of default Portal Service URL
   */
  public static final String PORTAL_SERVICE_URL = "portalurlservice";
  /**
   * Name of default Portal Billing URL
   */
  public static final String PORTAL_BILLING_URL = "portalurlbilling";


  /**
   * Name of order ID
   */
  public static final String ORDER_ID = "orderID";
  /**
   * Name of order status
   */
  public static final String ORDER_STATE = "orderState";
  /**
   * Name of order attribute type
   */
  public static final String ORDER_ATTRIBUTE_TYPE = "orderAttributeType";
  /**
   * Name of order attribute value
   */
  public static final String ORDER_ATTRIBUTE_VALUE = "orderAttributeValue";
  /**
   * Name of order period
   */
  public static final String ORDER_PERIOD = "orderPeriod";


  /**
   * Name of orderTemplate ID
   */
  public static final String ORDERTEMPLATE_ID = "orderTemplateID";
  /**
   * Name of orderTemplate attribute type
   */
  public static final String ORDERTEMPLATE_ATTRIBUTE_TYPE = "orderTemplateAttributeType";
  /**
   * Name of orderTemplate attribute value
   */
  public static final String ORDERTEMPLATE_ATTRIBUTE_VALUE = "orderTemplateAttributeValue";
  /**
   * Name of orderTemplate period
   */
  public static final String ORDERTEMPLATE_PERIOD = "orderTemplatePeriod";



  /**
   * Name of quotation ID
   */
  public static final String QUOTATION_ID = "quotationID";
  /**
   * Name of quotation status
   */
  public static final String QUOTATION_STATE = "quotationState";
  /**
   * Name of quotation attribute type
   */
  public static final String QUOTATION_ATTRIBUTE_TYPE = "quotationAttributeType";
  /**
   * Name of quotation attribute value
   */
  public static final String QUOTATION_ATTRIBUTE_VALUE = "quotationAttributeValue";
  /**
   * Name of quotation period
   */
  public static final String QUOTATION_PERIOD = "quotationPeriod";


  /**
   * Name of invoice ID
   */
  public static final String INVOICE_ID = "invoiceID";
  /**
   * Name of invoice attribute type
   */
  public static final String INVOICE_ATTRIBUTE_TYPE = "invoiceAttributeType";
  /**
   * Name of invoice attribute value
   */
  public static final String INVOICE_ATTRIBUTE_VALUE = "invoiceAttributeValue";
  /**
   * Name of invoice period
   */
  public static final String INVOICE_PERIOD = "invoicePeriod";


  /**
   * Name of creditMemo ID
   */
  public static final String CREDITMEMO_ID = "creditMemoID";
  /**
   * Name of creditmemo attribute type
   */
  public static final String CREDITMEMO_ATTRIBUTE_TYPE = "creditMemoAttributeType";
  /**
   * Name of invoice attribute value
   */
  public static final String CREDITMEMO_ATTRIBUTE_VALUE = "creditMemoAttributeValue";
  /**
   * Name of invoice period
   */
  public static final String CREDITMEMO_PERIOD = "creditMemoPeriod";


  /**
   * Name of downPayment ID
   */
  public static final String DOWNPAYMENT_ID = "downPaymentID";
  /**
   * Name of downpayment attribute type
   */
  public static final String DOWNPAYMENT_ATTRIBUTE_TYPE = "downPaymentAttributeType";
  /**
   * Name of downpayment attribute value
   */
  public static final String DOWNPAYMENT_ATTRIBUTE_VALUE = "downPaymentAttributeValue";
  /**
   * Name of downpayment period
   */
  public static final String DOWNPAYMENT_PERIOD = "downPaymentPeriod";


  /**
   * Name of serviceRequest ID
   */
  public static final String SERVICEREQUEST_ID = "serviceRequestID";
  /**
   * Name of servicerequest status
   */
  public static final String SERVICEREQUEST_ATTRIBUTE_TYPE = "serviceRequestStatus";
  /**
   * Name of servicerequest period
   */
  public static final String SERVICEREQUEST_PERIOD = "serviceRequestPeriod";


  /**
   * Name of infoRequest ID
   */
  public static final String INFOREQUEST_ID = "infoRequestID";
  /**
   * Name of inforequest status
   */
  public static final String INFOREQUEST_ATTRIBUTE_TYPE = "infoRequestStatus";
  /**
   * Name of inforequest period
   */
  public static final String INFOREQUEST_PERIOD = "infoRequestPeriod";


  /**
   * Name of complaints ID
   */
  public static final String COMPLAINTS_ID = "complaintsID";
  /**
   * Name of complaints status
   */
  public static final String COMPALINTS_ATTRIBUTE_TYPE = "complaintsStatus";
  /**
   * Name of complaints period
   */
  public static final String COMPLAINTS_PERIOD = "complaintsPeriod";


  /**
   * Name of serviceContract ID
   */
  public static final String SERVICECONTRACT_ID = "serviceContractID";
  /**
   * Name of servicecontract status
   */
  public static final String SERVICECONTRACT_ATTRIBUTE_TYPE = "serviceContractStatus";
  /**
   * Name of servicecontract period
   */
  public static final String SERVICECONTRACT_PERIOD = "serviceContractPeriod";

  private String allMonth    = "JanFebMarAprMayJunJulAugSepOctNovDec";
  private String allMonthNum = "01 02 03 04 05 06 07 08 09 10 11 12";

  private String mDocumentType = null;
  private String mPortalShopUrl = "";
  private String mPortalServiceUrl = "";
  private String mPortalBillingUrl = "";

  private String mOrderID = "";
  private String mOrderState = null;
  private String mOrderAttributeType = null;
  private String mOrderAttributeValue = null;
  private String mOrderPeriod = null;

  private String mOrderTemplateID = "";
  private String mOrderTemplateAttributeType = null;
  private String mOrderTemplateAttributeValue = null;
  private String mOrderTemplatePeriod = null;

  private String mQuotationID = "";
  private String mQuotationState = null;
  private String mQuotationAttributeType = null;
  private String mQuotationAttributeValue = null;
  private String mQuotationPeriod = null;

  private String mInvoiceID = "";
  private String mInvoiceAttributeType = null;
  private String mInvoiceAttributeValue = null;
  private String mInvoicePeriod = null;

  private String mCreditMemoID = "";
  private String mCreditMemoAttributeType = null;
  private String mCreditMemoAttributeValue = null;
  private String mCreditMemoPeriod = null;

  private String mDownPaymentID = "";
  private String mDownPaymentAttributeType = null;
  private String mDownPaymentAttributeValue = null;
  private String mDownPaymentPeriod = null;

  private String mServiceRequestID = "";
  private String mServiceRequestState = null;
  private String mServiceRequestPeriod = null;

  private String mInfoRequestID = "";
  private String mInfoRequestState = null;
  private String mInfoRequestPeriod = null;

  private String mComplaintsID = "";
  private String mComplaintsState = null;
  private String mComplaintsPeriod = null;

  private String mServiceContractID = "";
  private String mServiceContractState = null;
  private String mServiceContractPeriod = null;

  // add CRM Object Types for portal navigation
  private String mCrmObjectTypeQuotation = null;
  private String mCrmObjectTypeOrder = null;
  private String mCrmObjectTypeOrderTemplate = null;
  private String mCrmObjectTypeInvoice = null;
  private String mCrmObjectTypeCreditMemo = null;
  private String mCrmObjectTypeDownPayment = null;
  private String mCrmObjectTypeServiceRequest = null;
  private String mCrmObjectTypeInfoRequest = null;
  private String mCrmObjectTypeComplaints =  null;
  private String mCrmObjectTypeServiceContract =  null;

  // no argument Constructor
  public DocumentListiViewParameterForm() {
  }


  /**
   * Set the document type.
   */
  public void setDoc_type(String docType) {
    mDocumentType = docType;
  }

  /**
   * Get DocumentType
   */
  public String getDoc_type() {
    return mDocumentType;
  }


  /**
   * Set the CRM Object Type Quotation
   */
  public void setCrmObjectTypeQuotation(String crmObjectTypeQuotation) {
    mCrmObjectTypeQuotation = crmObjectTypeQuotation;
  }

  /**
   * Get CRM Object Type Quotation
   */
  public String getCrmObjectTypeQuotation() {
    if (mCrmObjectTypeQuotation == null || mCrmObjectTypeQuotation.equals("")) {
    	mCrmObjectTypeQuotation = "SALESQUOTATIONCRM";
    }
    return mCrmObjectTypeQuotation;
  }

  /**
   * Set the CRM Object Type Order
   */
  public void setCrmObjectTypeOrder(String crmObjectTypeOrder) {
    mCrmObjectTypeOrder = crmObjectTypeOrder;
  }

  /**
   * Get CRM Object Type Order
   */
  public String getCrmObjectTypeOrder() {
    if (mCrmObjectTypeOrder == null || mCrmObjectTypeOrder.equals("")) {
    	mCrmObjectTypeOrder = "SALESORDERCRM";
    }
    return mCrmObjectTypeOrder;
  }

  /**
   * Set the CRM Object Type OrderTemplate
   */
  public void setCrmObjectTypeOrderTemplate(String crmObjectTypeOrderTemplate) {
    mCrmObjectTypeOrderTemplate = crmObjectTypeOrderTemplate;
  }

  /**
   * Get CRM Object Type OrderTemplate
   */
  public String getCrmObjectTypeOrderTemplate() {
    if (mCrmObjectTypeOrderTemplate == null || mCrmObjectTypeOrderTemplate.equals("")) {
    	mCrmObjectTypeOrderTemplate = "SALESTRANSACTIONCRM";
    }

    return mCrmObjectTypeOrderTemplate;
  }

  /**
   * Set the CRM Object Type Invoice
   */
  public void setCrmObjectTypeInvoice(String crmObjectTypeInvoice) {
    mCrmObjectTypeInvoice = crmObjectTypeInvoice;
  }

  /**
   * Get CRM Object Type Invoice
   */
  public String getCrmObjectTypeInvoice() {
    if (mCrmObjectTypeInvoice == null || mCrmObjectTypeInvoice.equals("")) {
    	mCrmObjectTypeInvoice = "BILLING";
    }

    return mCrmObjectTypeInvoice;
  }

  /**
   * Set the CRM Object Type CreditMemo
   */
  public void setCrmObjectTypeCreditMemo(String crmObjectTypeCreditMemo) {
    mCrmObjectTypeCreditMemo = crmObjectTypeCreditMemo;
  }

  /**
   * Get CRM Object Type CreditMemo
   */
  public String getCrmObjectTypeCreditMemo() {
    if (mCrmObjectTypeCreditMemo == null || mCrmObjectTypeCreditMemo.equals("")) {
    	mCrmObjectTypeCreditMemo = "SALESTRANSACTIONCRM";
    }

    return mCrmObjectTypeCreditMemo;
  }

  /**
   * Set the CRM Object Type DownPayment
   */
  public void setCrmObjectTypeDownPayment(String crmObjectTypeDownPayment) {
    mCrmObjectTypeDownPayment = crmObjectTypeDownPayment;
  }

  /**
   * Get CRM Object Type DownPayment
   */
  public String getCrmObjectTypeDownPayment() {
    if (mCrmObjectTypeDownPayment == null || mCrmObjectTypeDownPayment.equals("")) {
    	mCrmObjectTypeDownPayment = "SALESTRANSACTIONCRM";
    }

    return mCrmObjectTypeDownPayment;
  }

  /**
   * Set the CRM Object Type ServiceRequest
   */
  public void setCrmObjectTypeServiceRequest(String crmObjectTypeServiceRequest) {
    mCrmObjectTypeServiceRequest = crmObjectTypeServiceRequest;
  }

  /**
   * Get CRM Object Type ServiceRequest
   */
  public String getCrmObjectTypeServiceRequest() {
    if (mCrmObjectTypeServiceRequest == null || mCrmObjectTypeServiceRequest.equals("")) {
    	mCrmObjectTypeServiceRequest = "SERVICEPROCESSCRM";
    }

    return mCrmObjectTypeServiceRequest;
  }

  /**
   * Set the CRM Object Type InfoRequest
   */
  public void setCrmObjectTypeInfoRequest(String crmObjectTypeInfoRequest) {
    mCrmObjectTypeInfoRequest = crmObjectTypeInfoRequest;
  }

  /**
   * Get CRM Object Type InfoRequest
   */
  public String getCrmObjectTypeInfoRequest() {
    if (mCrmObjectTypeInfoRequest == null || mCrmObjectTypeInfoRequest.equals("")) {
    	mCrmObjectTypeInfoRequest = "SERVICEPROCESSCRM";
    }

    return mCrmObjectTypeInfoRequest;
  }

  /**
   * Set the CRM Object Type Complaints
   */
  public void setCrmObjectTypeComplaints(String crmObjectTypeComplaints) {
    mCrmObjectTypeComplaints = crmObjectTypeComplaints;
  }

  /**
   * Get CRM Object Type Complaints
   */
  public String getCrmObjectTypeComplaints() {
    if (mCrmObjectTypeComplaints == null || mCrmObjectTypeComplaints.equals("")) {
    	mCrmObjectTypeComplaints = "COMPLAINTCRM";
    }

    return mCrmObjectTypeComplaints;
  }

  /**
   * Set the CRM Object Type ServiceContract
   */
  public void setCrmObjectTypeServiceContract(String crmObjectTypeServiceContract) {
    mCrmObjectTypeServiceContract = crmObjectTypeServiceContract;
  }

  /**
   * Get CRM Object Type ServiceContract
   */
  public String getCrmObjectTypeServiceContract() {
    if (mCrmObjectTypeServiceContract == null || mCrmObjectTypeServiceContract.equals("")) {
    	mCrmObjectTypeServiceContract = "SERVICECONTRACTCRM";
    }

    return mCrmObjectTypeServiceContract;
  }


  /**
   * Set the Portal Shop URL
   */
  public void setPortalurl(String portalurl) {
    mPortalShopUrl = portalurl;
  }

  /**
   * Get Shop Portal URL
   */
  public String getPortalurl() {
    return mPortalShopUrl;
  }

  /**
   * Set the Portal Service URL
   */
  public void setPortalurlservice(String portalurlservice) {
    mPortalServiceUrl = portalurlservice;
  }

  /**
   * Get Service Portal URL
   */
  public String getPortalurlservice() {
    return mPortalServiceUrl;
  }

  /**
   * Set the Portal Billing URL
   */
  public void setPortalurlbilling(String portalurlbilling) {
    mPortalBillingUrl = portalurlbilling;
  }

  /**
   * Get Billing Portal URL
   */
  public String getPortalurlbilling() {
    return mPortalBillingUrl;
  }


  /**
   * Set the orderID.
   */
  public void setOrderID(String orderID) {
    mOrderID = orderID;
  }

  /**
   * Get orderID
   */
  public String getOrderID() {
    return mOrderID;
  }

  /**
   * Set the orderState.
   */
  public void setOrderState(String orderState) {
    mOrderState = orderState;
  }

  /**
   * Get orderState
   */
  public String getOrderState() {
    return mOrderState;
  }

  /**
   * Set the orderAttributeType
   */
  public void setOrderAttributeType(String orderAttributeType) {
    mOrderAttributeType = orderAttributeType;
  }

  /**
   * Get orderAttributeType
   */
  public String getOrderAttributeType() {
    return mOrderAttributeType;
  }

  /**
   * Set the orderAttributeValue
   */
  public void setOrderAttributeValue(String orderAttributeValue) {
    mOrderAttributeValue = orderAttributeValue;
  }

  /**
   * Get orderAttributeValue
   */
  public String getOrderAttributeValue() {
    return mOrderAttributeValue;
  }

  /**
   * Set the orderPeriod
   */
  public void setOrderPeriod(String orderPeriod) {
    mOrderPeriod = orderPeriod;
  }

  /**
   * Get orderPeriod
   */
  public String getOrderPeriod() {
    return mOrderPeriod;
  }



  /**
   * Set the orderTemplateID.
   */
  public void setOrderTemplateID(String orderTemplateID) {
    mOrderTemplateID = orderTemplateID;
  }

  /**
   * Get orderTemplateID
   */
  public String getOrderTemplateID() {
    return mOrderTemplateID;
  }

  /**
   * Set the orderTemplateAttributeType
   */
  public void setOrderTemplateAttributeType(String orderTemplateAttributeType) {
    mOrderTemplateAttributeType = orderTemplateAttributeType;
  }

  /**
   * Get orderTemplateAttributeType
   */
  public String getOrderTemplateAttributeType() {
    return mOrderTemplateAttributeType;
  }

  /**
   * Set the orderTemplateAttributeValue
   */
  public void setOrderTemplateAttributeValue(String orderTemplateAttributeValue) {
    mOrderTemplateAttributeValue = orderTemplateAttributeValue;
  }

  /**
   * Get orderTemplateAttributeValue
   */
  public String getOrderTemplateAttributeValue() {
    return mOrderTemplateAttributeValue;
  }

  /**
   * Set the orderTemplatePeriod
   */
  public void setOrderTemplatePeriod(String orderTemplatePeriod) {
    mOrderTemplatePeriod = orderTemplatePeriod;
  }

  /**
   * Get orderTemplatePeriod
   */
  public String getOrderTemplatePeriod() {
    return mOrderTemplatePeriod;
  }



  /**
   * Set the quotationID.
   */
  public void setQuotationID(String quotationID) {
    mQuotationID = quotationID;
  }

  /**
   * Get quotationID
   */
  public String getQuotationID() {
    return mQuotationID;
  }

  /**
   * Set the quotationState.
   */
  public void setQuotationState(String quotationState) {
    mQuotationState = quotationState;
  }

  /**
   * Get quotationState
   */
  public String getQuotationState() {
    return mQuotationState;
  }

  /**
   * Set the quotationAttributeType
   */
  public void setQuotationAttributeType(String quotationAttributeType) {
    mQuotationAttributeType = quotationAttributeType;
  }

  /**
   * Get quotationAttributeType
   */
  public String getQuotationAttributeType() {
    return mQuotationAttributeType;
  }

  /**
   * Set the quotationAttributeValue
   */
  public void setQuotationAttributeValue(String quotationAttributeValue) {
    mQuotationAttributeValue = quotationAttributeValue;
  }

  /**
   * Get quotationAttributeValue
   */
  public String getQuotationAttributeValue() {
    return mQuotationAttributeValue;
  }

  /**
   * Set the quotationPeriod
   */
  public void setQuotationPeriod(String quotationPeriod) {
    mQuotationPeriod = quotationPeriod;
  }

  /**
   * Get quotationPeriod
   */
  public String getQuotationPeriod() {
    return mQuotationPeriod;
  }


  /**
   * Set the invoiceID.
   */
  public void setInvoiceID(String invoiceID) {
    mInvoiceID = invoiceID;
  }

  /**
   * Get invoiceID
   */
  public String getInvoiceID() {
    return mInvoiceID;
  }

  /**
   * Set the invoiceAttributeType
   */
  public void setInvoiceAttributeType(String invoiceAttributeType) {
    mInvoiceAttributeType = invoiceAttributeType;
  }

  /**
   * Get invoiceAttributeType
   */
  public String getInvoiceAttributeType() {
    return mInvoiceAttributeType;
  }

  /**
   * Set the invoiceAttributeValue
   */
  public void setInvoiceAttributeValue(String invoiceAttributeValue) {
    mInvoiceAttributeValue = invoiceAttributeValue;
  }

  /**
   * Get invoiceAttributeValue
   */
  public String getInvoiceAttributeValue() {
    return mInvoiceAttributeValue;
  }

  /**
   * Set the invoicePeriod
   */
  public void setInvoicePeriod(String invoicePeriod) {
    mInvoicePeriod = invoicePeriod;
  }

  /**
   * Get invoicePeriod
   */
  public String getInvoicePeriod() {
    return mInvoicePeriod;
  }


  /**
   * Set the creditMemoID.
   */
  public void setCreditMemoID(String creditMemoID) {
    mCreditMemoID = creditMemoID;
  }

  /**
   * Get creditMemoID
   */
  public String getCreditMemoID() {
    return mCreditMemoID;
  }

  /**
   * Set the creditmemoAttributeType
   */
  public void setCreditMemoAttributeType(String creditmemoAttributeType) {
    mCreditMemoAttributeType = creditmemoAttributeType;
  }

  /**
   * Get creditmemoAttributeType
   */
  public String getCreditMemoAttributeType() {
    return mCreditMemoAttributeType;
  }

  /**
   * Set the creditmemoAttributeValue
   */
  public void setCreditMemoAttributeValue(String creditmemoAttributeValue) {
    mCreditMemoAttributeValue = creditmemoAttributeValue;
  }

  /**
   * Get creditmemoAttributeValue
   */
  public String getCreditMemoAttributeValue() {
    return mCreditMemoAttributeValue;
  }

  /**
   * Set the creditmemoPeriod
   */
  public void setCreditMemoPeriod(String creditmemoPeriod) {
    mCreditMemoPeriod = creditmemoPeriod;
  }

  /**
   * Get creditmemoPeriod
   */
  public String getCreditMemoPeriod() {
    return mCreditMemoPeriod;
  }


  /**
   * Set the downPaymentID.
   */
  public void setDownPaymentID(String downPaymentID) {
    mDownPaymentID = downPaymentID;
  }

  /**
   * Get downPaymentID
   */
  public String getDownPaymentID() {
    return mDownPaymentID;
  }

  /**
   * Set the downPaymentAttributeType
   */
  public void setDownPaymentAttributeType(String downPaymentAttributeType) {
    mDownPaymentAttributeType = downPaymentAttributeType;
  }

  /**
   * Get downPaymentAttributeType
   */
  public String getDownPaymentMemoAttributeType() {
    return mDownPaymentAttributeType;
  }

  /**
   * Set the downPaymentAttributeValue
   */
  public void setDownPaymentAttributeValue(String downPaymentAttributeValue) {
    mDownPaymentAttributeValue = downPaymentAttributeValue;
  }

  /**
   * Get downPaymentAttributeValue
   */
  public String getDownPaymentAttributeValue() {
    return mDownPaymentAttributeValue;
  }

  /**
   * Set the downPaymentPeriod
   */
  public void setDownPaymentPeriod(String downPaymentPeriod) {
    mDownPaymentPeriod = downPaymentPeriod;
  }

  /**
   * Get downPaymentPeriod
   */
  public String getDownPaymentPeriod() {
    return mDownPaymentPeriod;
  }


  /**
   * Set the serviceRequestID.
   */
  public void setServiceRequestID(String serviceRequestID) {
    mServiceRequestID = serviceRequestID;
  }

  /**
   * Get serviceRequestID
   */
  public String getServiceRequestID() {
    return mServiceRequestID;
  }

  /**
   * Set the serviceRequestState.
   */
  public void setServiceRequestState(String serviceRequestState) {
    mServiceRequestState = serviceRequestState;
  }

  /**
   * Get serviceRequestState
   */
  public String getServiceRequestState() {
    return mServiceRequestState;
  }

  /**
   * Set the serviceRequestPeriod
   */
  public void setServiceRequestPeriod(String serviceRequestPeriod) {
    mServiceRequestPeriod = serviceRequestPeriod;
  }

  /**
   * Get serviceRequestPeriod
   */
  public String getServiceRequestPeriod() {
    return mServiceRequestPeriod;
  }


  /**
   * Set the infoRequestID.
   */
  public void setInfoRequestID(String infoRequestID) {
    mInfoRequestID = infoRequestID;
  }

  /**
   * Get infoRequestID
   */
  public String getInfoRequestID() {
    return mInfoRequestID;
  }

  /**
   * Set the infoRequestState.
   */
  public void setInfoRequestState(String infoRequestState) {
    mInfoRequestState = infoRequestState;
  }

  /**
   * Get infoRequestState
   */
  public String getInfoRequestState() {
    return mInfoRequestState;
  }

  /**
   * Set the infoRequestPeriod
   */
  public void setInfoRequestPeriod(String infoRequestPeriod) {
    mInfoRequestPeriod = infoRequestPeriod;
  }

  /**
   * Get infoRequestPeriod
   */
  public String getInfoRequestPeriod() {
    return mInfoRequestPeriod;
  }


  /**
   * Set the complaintsID.
   */
  public void setComplaintsID(String complaintsID) {
    mComplaintsID = complaintsID;
  }

  /**
   * Get complaintsID
   */
  public String getComplaintsID() {
    return mComplaintsID;
  }

  /**
   * Set the complaintsState.
   */
  public void setComplaintsState(String complaintsState) {
    mComplaintsState = complaintsState;
  }

  /**
   * Get complaintsState
   */
  public String getComplaintsState() {
    return mComplaintsState;
  }

  /**
   * Set the complaintsPeriod
   */
  public void setComplaintsPeriod(String complaintsPeriod) {
    mComplaintsPeriod = complaintsPeriod;
  }

  /**
   * Get complaintsPeriod
   */
  public String getComplaintsPeriod() {
    return mComplaintsPeriod;
  }


  /**
   * Set the serviceContractID.
   */
  public void setServiceContractID(String serviceContractID) {
    mServiceContractID = serviceContractID;
  }

  /**
   * Get serviceContractID
   */
  public String getServiceContractID() {
    return mServiceContractID;
  }

  /**
   * Set the serviceContractState.
   */
  public void setServiceContractState(String serviceContractState) {
    mServiceContractState = serviceContractState;
  }

  /**
   * Get serviceContractState
   */
  public String getServiceContractState() {
    return mServiceContractState;
  }

  /**
   * Set the serviceContractPeriod
   */
  public void setServiceContractPeriod(String serviceContractPeriod) {
    mServiceContractPeriod = serviceContractPeriod;
  }

  /**
   * Get serviceContractPeriod
   */
  public String getServiceContractPeriod() {
    return mServiceContractPeriod;
  }




  public ActionErrors validate(ActionMapping ac, HttpServletRequest hsr) {
      return new ActionErrors();
  }

  /**
   * Get order DocumentListFilter
   */
  public DocumentListFilter getOrderListFilter() {
    DocumentListFilter orderListFilter = new DocumentListFilter();
    // set the type of the document
    orderListFilter.setTypeORDER();

    // set the state of the order document
    if ((mOrderState == null) || mOrderState.equals("all")
        || mOrderState.equals("")) {
      orderListFilter.setStatusALL();
    }
    if ("open".equals(mOrderState)) {
      orderListFilter.setStatusOPEN();
    }
    if ("completed".equals(mOrderState)) {
      orderListFilter.setStatusCOMPLETED();
    }
    if ("cancelled".equals(mOrderState)) {
      orderListFilter.setStatusCANCELLED();
    }

    // set the attribute type of the order document
    if ((mOrderAttributeType != null) && !mOrderAttributeType.equals("none")
        && !mOrderAttributeType.equals("")) {

        if (mOrderAttributeType.equals("description")) {
          orderListFilter.setDescription(mOrderAttributeValue);
        }
        if (mOrderAttributeType.equals("referencenr")) {
          orderListFilter.setExternalRefNo(mOrderAttributeValue);
        }
        if (mOrderAttributeType.equals("objectid")) {
          orderListFilter.setId(mOrderAttributeValue);
        }
    }

    if (mOrderPeriod != null) {
      long LdateFrom = 0l;
      long LdateToday = new Date().getTime();

      Long lngPeriod = new Long(mOrderPeriod);
      if (lngPeriod.longValue() > 0 ) {
          Date date = new Date();
          LdateFrom = LdateToday - (lngPeriod.longValue() * 24l * 60l * 60l * 1000l);                      // One Week in Milliseconds
          date.setTime(LdateFrom);
          orderListFilter.setChangedDate(convDateToFormatYYYYMMDD(date.toString()));
      }
    }

    return orderListFilter;
  }

  /**
   * Get orderTemplate DocumentListFilter
   */
  public DocumentListFilter getOrderTemplateListFilter() {
    DocumentListFilter orderTemplateListFilter = new DocumentListFilter();
    // set the type of the document
    orderTemplateListFilter.setTypeORDERTEMPLATE();

    // set the attribute type of the order document
    if ((mOrderTemplateAttributeType != null) && !mOrderTemplateAttributeType.equals("none")
        && !mOrderTemplateAttributeType.equals("")) {

        if (mOrderTemplateAttributeType.equals("description")) {
          orderTemplateListFilter.setDescription(mOrderTemplateAttributeValue);
        }
        if (mOrderTemplateAttributeType.equals("referencenr")) {
          orderTemplateListFilter.setExternalRefNo(mOrderTemplateAttributeValue);
        }
        if (mOrderTemplateAttributeType.equals("objectid")) {
          orderTemplateListFilter.setId(mOrderTemplateAttributeValue);
        }
    }

    if (mOrderTemplatePeriod != null) {
      long LdateFrom = 0l;
      long LdateToday = new Date().getTime();

      Long lngPeriod = new Long(mOrderTemplatePeriod);

      if (lngPeriod.longValue() > 0 ) {
          Date date = new Date();
          LdateFrom = LdateToday - (lngPeriod.longValue() * 24l * 60l * 60l * 1000l);                      // One Week in Milliseconds
          date.setTime(LdateFrom);
          orderTemplateListFilter.setChangedDate(convDateToFormatYYYYMMDD(date.toString()));
      }
    }

    return orderTemplateListFilter;
  }


  /**
   * Get quotation DocumentListFilter
   */
  public DocumentListFilter getQuotationListFilter() {
    DocumentListFilter quotationListFilter = new DocumentListFilter();
    // set the type of the document
    quotationListFilter.setTypeQUOTA();

    // set the state of the order document
    if ((mQuotationState == null) || mQuotationState.equals("all")
        || mQuotationState.equals("")) {
      quotationListFilter.setStatusALL();
    }
    if ("open".equals(mQuotationState)) {
      quotationListFilter.setStatusOPEN();
    }
    if ("completed".equals(mQuotationState)) {
      quotationListFilter.setStatusCOMPLETED();
    }
    if ("cancelled".equals(mQuotationState)) {
      quotationListFilter.setStatusCANCELLED();
    }

    // set the attribute type of the order document
    if ((mQuotationAttributeType != null) && !mQuotationAttributeType.equals("none")
        && !mQuotationAttributeType.equals("")) {

        if (mQuotationAttributeType.equals("description")) {
          quotationListFilter.setDescription(mQuotationAttributeValue);
        }
        if (mQuotationAttributeType.equals("referencenr")) {
          quotationListFilter.setExternalRefNo(mQuotationAttributeValue);
        }
        if (mQuotationAttributeType.equals("objectid")) {
          quotationListFilter.setId(mQuotationAttributeValue);
        }
    }

    if (mQuotationPeriod != null) {
      long LdateFrom = 0l;
      long LdateToday = new Date().getTime();

      Long lngPeriod = new Long(mQuotationPeriod);

      if (lngPeriod.longValue() > 0 ) {
          Date date = new Date();
          LdateFrom = LdateToday - (lngPeriod.longValue() * 24l * 60l * 60l * 1000l);                      // One Week in Milliseconds
          date.setTime(LdateFrom);
          quotationListFilter.setChangedDate(convDateToFormatYYYYMMDD(date.toString()));
      }
    }

    return quotationListFilter;
  }

  /**
   * Get invoice DocumentListFilter
   */
  public DocumentListFilter getInvoiceListFilter() {
    DocumentListFilter invoiceListFilter = new DocumentListFilter();
    // set the type of the document
    invoiceListFilter.setTypeINVOICE();

    // set the attribute type of the order document
    if ((mInvoiceAttributeType != null) && !mInvoiceAttributeType.equals("none")
        && !mInvoiceAttributeType.equals("")) {

        if (mInvoiceAttributeType.equals("referencenr")) {
          invoiceListFilter.setExternalRefNo(mInvoiceAttributeValue);
        }
        if (mInvoiceAttributeType.equals("objectid")) {
          invoiceListFilter.setId(mInvoiceAttributeValue);
        }
    }

    if (mInvoicePeriod != null) {
      Long lngPeriod = new Long(mInvoicePeriod);

       if (lngPeriod.longValue() > 0 ) {
          long LdateFrom = 0l;
          long LdateToday = new Date().getTime();
          Date date = new Date();

          // Set Date to (2002.02.28) and and date (2002.01.01)
          invoiceListFilter.setChangedToDate(convDateToFormatYYYYMMDD(date.toString()));
          LdateFrom = LdateToday - (lngPeriod.longValue() * 24l * 60l * 60l * 1000l);   // Selection period in days
          date.setTime(LdateFrom);
          invoiceListFilter.setChangedDate(convDateToFormatYYYYMMDD(date.toString()));
      }
      else {
          // Set Date to (2002.02.28) and and date (2002.01.01)
          invoiceListFilter.setChangedToDate(convDateToFormatYYYYMMDD(new Date().toString()));
          Date date = new Date();
          date.setTime(0); //  01.01.1970
          invoiceListFilter.setChangedDate(convDateToFormatYYYYMMDD(date.toString()));
      }
    }

    return invoiceListFilter;
  }

  /**
   * Get creditmemo DocumentListFilter
   */
  public DocumentListFilter getCreditMemoListFilter() {
    DocumentListFilter creditMemoListFilter = new DocumentListFilter();
    // set the type of the document
    creditMemoListFilter.setTypeCREDITMEMO();

    // set the attribute type of the order document
    if ((mCreditMemoAttributeType != null) && !mCreditMemoAttributeType.equals("none")
        && !mCreditMemoAttributeType.equals("")){

        if (mCreditMemoAttributeType.equals("referencenr")) {
          creditMemoListFilter.setExternalRefNo(mInvoiceAttributeValue);
        }
        if (mCreditMemoAttributeType.equals("objectid")) {
          creditMemoListFilter.setId(mInvoiceAttributeValue);
        }
    }

    if (mCreditMemoPeriod != null) {
       Long lngPeriod = new Long(mCreditMemoPeriod);
       if (lngPeriod.longValue() > 0 ) {
           long LdateFrom = 0l;
           long LdateToday = new Date().getTime();
           Date date = new Date();
           // Set Date to (2002.02.28) and and date (2002.01.01)
           creditMemoListFilter.setChangedToDate(convDateToFormatYYYYMMDD(date.toString()));
           LdateFrom = LdateToday - (lngPeriod.longValue() * 24l * 60l * 60l * 1000l);   // Selection period in days
           date.setTime(LdateFrom);
           creditMemoListFilter.setChangedDate(convDateToFormatYYYYMMDD(date.toString()));
      }
      else {
          // Set Date to (2002.02.28) and and date (2002.01.01)
          creditMemoListFilter.setChangedToDate(convDateToFormatYYYYMMDD(new Date().toString()));
          Date date = new Date();
          date.setTime(0); //  01.01.1970
          creditMemoListFilter.setChangedDate(convDateToFormatYYYYMMDD(date.toString()));
      }
    }

    return creditMemoListFilter;
  }


  /**
   * Get downPayment DocumentListFilter
   */
  public DocumentListFilter getDownPaymentListFilter() {
    DocumentListFilter downPaymentListFilter = new DocumentListFilter();
    // set the type of the document
    downPaymentListFilter.setTypeDOWNPAYMENT();

    // set the attribute type of the order document
    if ((mDownPaymentAttributeType != null) && !mDownPaymentAttributeType.equals("none")
        && !mDownPaymentAttributeType.equals("")) {

        if (mDownPaymentAttributeType.equals("referencenr")) {
          downPaymentListFilter.setExternalRefNo(mDownPaymentAttributeValue);
        }
        if (mDownPaymentAttributeType.equals("objectid")) {
          downPaymentListFilter.setId(mDownPaymentAttributeValue);
        }
    }

    if (mDownPaymentPeriod != null) {
       Long lngPeriod = new Long(mDownPaymentPeriod);
       if (lngPeriod.longValue() > 0 ) {
           long LdateFrom = 0l;
           long LdateToday = new Date().getTime();
           Date date = new Date();
           // Set Date to (2002.02.28) and and date (2002.01.01)
           downPaymentListFilter.setChangedToDate(convDateToFormatYYYYMMDD(date.toString()));
           LdateFrom = LdateToday - (lngPeriod.longValue() * 24l * 60l * 60l * 1000l);   // Selection period in days
           date.setTime(LdateFrom);
           downPaymentListFilter.setChangedDate(convDateToFormatYYYYMMDD(date.toString()));
      }
      else {
          // Set Date to (2002.02.28) and and date (2002.01.01)
          downPaymentListFilter.setChangedToDate(convDateToFormatYYYYMMDD(new Date().toString()));
          Date date = new Date();
          date.setTime(0); //  01.01.1970
          downPaymentListFilter.setChangedDate(convDateToFormatYYYYMMDD(date.toString()));
      }
    }
    return downPaymentListFilter;
  }


  /**
   * Get serviceRequest DocumentListFilter
   */
  public DocumentListFilter getServiceRequestListFilter() {
    DocumentListFilter serviceRequestListFilter = new DocumentListFilter();
    // set the type of the document
    serviceRequestListFilter.setTypeService("BUS2000116");

    // set the state of the service request document
    if ((mServiceRequestState == null) || mServiceRequestState.equals("all")
        || mServiceRequestState.equals("")) {
      //serviceRequestListFilter.setStatusALL();
    }
    else {
        serviceRequestListFilter.addServiceStatus(mServiceRequestState);
    }

    if (mServiceRequestPeriod != null) {
      Long lngPeriod = new Long(mServiceRequestPeriod);
      if (lngPeriod.longValue() > 0) {
          long LdateFrom = 0l;
          long LdateToday = new Date().getTime();
          Date date = new Date();
          // Set Date to (2002.02.28) and and date (2002.01.01)
          serviceRequestListFilter.setChangedToDate(convDateToFormatYYYYMMDD(date.toString()));
          LdateFrom = LdateToday - (lngPeriod.longValue() * 24l * 60l * 60l * 1000l);   // Selection period in days
          date.setTime(LdateFrom);
          serviceRequestListFilter.setChangedDate(convDateToFormatYYYYMMDD(date.toString()));
      }
      else {
          // Set Date to (2002.02.28) and and date (2002.01.01)
          serviceRequestListFilter.setChangedToDate(convDateToFormatYYYYMMDD(new Date().toString()));
          Date date = new Date();
          date.setTime(0); //  01.01.1970
          serviceRequestListFilter.setChangedDate(convDateToFormatYYYYMMDD(date.toString()));
      }
    }

    return serviceRequestListFilter;
  }

  /**
   * Get infoRequest DocumentListFilter
   */
  public DocumentListFilter getInfoRequestListFilter() {
    DocumentListFilter infoRequestListFilter = new DocumentListFilter();
    // set the type of the document
    infoRequestListFilter.setTypeService("BUS2000116");

    // set the state of the info request document
    if ((mInfoRequestState == null) || mInfoRequestState.equals("all")
        || mInfoRequestState.equals("")) {
      //serviceRequestListFilter.setStatusALL();
    }
    else {
        infoRequestListFilter.addServiceStatus(mInfoRequestState);
    }

    if (mInfoRequestPeriod != null) {
      Long lngPeriod = new Long(mInfoRequestPeriod);
      if (lngPeriod.longValue() > 0) {
          long LdateFrom = 0l;
          long LdateToday = new Date().getTime();
          Date date = new Date();
          // Set Date to (2002.02.28) and and date (2002.01.01)
          infoRequestListFilter.setChangedToDate(convDateToFormatYYYYMMDD(date.toString()));
          LdateFrom = LdateToday - (lngPeriod.longValue() * 24l * 60l * 60l * 1000l);   // Selection period in days
          date.setTime(LdateFrom);
          infoRequestListFilter.setChangedDate(convDateToFormatYYYYMMDD(date.toString()));
      }
      else {
          // Set Date to (2002.02.28) and and date (2002.01.01)
          infoRequestListFilter.setChangedToDate(convDateToFormatYYYYMMDD(new Date().toString()));
          Date date = new Date();
          date.setTime(0); //  01.01.1970
          infoRequestListFilter.setChangedDate(convDateToFormatYYYYMMDD(date.toString()));
      }
    }
    return infoRequestListFilter;
  }


  /**
   * Get complaints DocumentListFilter
   */
  public DocumentListFilter getComplaintsListFilter() {
    DocumentListFilter complaintsListFilter = new DocumentListFilter();
    // set the type of the document
    complaintsListFilter.setTypeService("BUS2000120");

    // set the state of the complaints document
    if ((mComplaintsState == null) || mComplaintsState.equals("all")
        || mComplaintsState.equals("")) {
      //serviceRequestListFilter.setStatusALL();
    }
    else {
        complaintsListFilter.addServiceStatus(mComplaintsState);
    }

    if (mComplaintsPeriod != null) {
      Long lngPeriod = new Long(mComplaintsPeriod);
      if (lngPeriod.longValue() > 0) {
          long LdateFrom = 0l;
          long LdateToday = new Date().getTime();
          Date date = new Date();
          // Set Date to (2002.02.28) and and date (2002.01.01)
          complaintsListFilter.setChangedToDate(convDateToFormatYYYYMMDD(date.toString()));
          LdateFrom = LdateToday - (lngPeriod.longValue() * 24l * 60l * 60l * 1000l);   // Selection period in days
          date.setTime(LdateFrom);
          complaintsListFilter.setChangedDate(convDateToFormatYYYYMMDD(date.toString()));
      }
      else {
          // Set Date to (2002.02.28) and and date (2002.01.01)
          complaintsListFilter.setChangedToDate(convDateToFormatYYYYMMDD(new Date().toString()));
          Date date = new Date();
          date.setTime(0); //  01.01.1970
          complaintsListFilter.setChangedDate(convDateToFormatYYYYMMDD(date.toString()));
      }
    }
    return complaintsListFilter;
  }


  /**
   * Get serviceContract DocumentListFilter
   */
  public DocumentListFilter getServiceContractListFilter() {
    DocumentListFilter serviceContractListFilter = new DocumentListFilter();
    // set the type of the document
    serviceContractListFilter.setTypeService("BUS2000112");

    // set the state of the service contract document
    if ((mServiceContractState == null) || mServiceContractState.equals("all")
        || mServiceContractState.equals("")) {
      //serviceRequestListFilter.setStatusALL();
    }
    else {
        serviceContractListFilter.addServiceStatus(mServiceContractState);
    }

    if (mServiceContractPeriod != null) {
      Long lngPeriod = new Long(mServiceContractPeriod);
      if (lngPeriod.longValue() > 0) {
          long LdateFrom = 0l;
          long LdateToday = new Date().getTime();
          Date date = new Date();
          // Set Date to (2002.02.28) and and date (2002.01.01)
          serviceContractListFilter.setChangedToDate(convDateToFormatYYYYMMDD(date.toString()));
          LdateFrom = LdateToday - (lngPeriod.longValue() * 24l * 60l * 60l * 1000l);   // Selection period in days
          date.setTime(LdateFrom);
          serviceContractListFilter.setChangedDate(convDateToFormatYYYYMMDD(date.toString()));
      }
      else {
          // Set Date to (2002.02.28) and and date (2002.01.01)
          serviceContractListFilter.setChangedToDate(convDateToFormatYYYYMMDD(new Date().toString()));
          Date date = new Date();
          date.setTime(0); //  01.01.1970
          serviceContractListFilter.setChangedDate(convDateToFormatYYYYMMDD(date.toString()));
      }
    }
    return serviceContractListFilter;
  }


  /**
   * Get the entered Selectiondate in a backend conform format
   */
  private String convDateToFormatYYYYMMDD(String inDate) {
      String outDate = new String();

      outDate = outDate.concat(inDate.substring(inDate.length()-4, inDate.length()));     // YYYY
      outDate = outDate.concat(allMonthNum.substring( allMonth.indexOf(inDate.substring(4,7)) , allMonth.indexOf(inDate.substring(4,7)) + 2 ));
      outDate = outDate.concat(inDate.substring(8,10));
      return outDate;
  }
}