package com.sap.isa.isacore.actionform;

import java.util.HashMap;
import java.util.Iterator;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionFormBean;

/**
 * Title:        CampaignEnrollmentPageForm
 * Description:
 * Copyright:    Copyright (c) 2006
 * Company:      SAP AG
 * @version 1.0
 */

public class CampaignEnrollmentPageForm extends ActionForm {

  HashMap enrollmList;
  private static Class enrollmFormClass = null;
  // forward
  private String forward = null;
  // campaign page
  private String campaignpage = null;
  // enroll all flag
  private boolean enrollall = false;
  // flag indicating if the enroll all flag was changed
  private boolean enrollallchanged = false;

  public CampaignEnrollmentPageForm() {
    enrollmList = new HashMap();
  }

  public CampaignEnrollmentForm getEnrollm(int index) {
    // Determination of enrollmFormClass needs to be here, as reference to servlet
    // is not yet set in constructor!
    if (enrollmFormClass == null) {
      enrollmFormClass = determineEnrollmentFormClass();
    }
    String key = String.valueOf(index);
    if (enrollmList.containsKey(key)) {
      return (CampaignEnrollmentForm) enrollmList.get(key);
    }
    else {
      CampaignEnrollmentForm enrollmForm = createCampaignEnrollmentForm();
      enrollmList.put(key, enrollmForm);
      return enrollmForm;
    }
  }

  protected Class determineEnrollmentFormClass()
  {
    Class enrollmFormClass = null;
    ActionFormBean formBean = null;
    if (getServlet() != null)
      formBean = super.getServlet().findFormBean("CampaignEnrollmentForm");
    if (formBean != null) {
      try {
        enrollmFormClass = Class.forName(formBean.getType());
      }
      catch (ClassNotFoundException e) {
        enrollmFormClass = null;
      }
    }
    if (enrollmFormClass == null) {
      enrollmFormClass = CampaignEnrollmentForm.class;
    }
    return enrollmFormClass;
  }

  protected CampaignEnrollmentForm createCampaignEnrollmentForm() {
    CampaignEnrollmentForm enrollmForm = null;
    try {
      enrollmForm = (CampaignEnrollmentForm) enrollmFormClass.newInstance();
    }
    catch (Exception e) {
      enrollmForm = new CampaignEnrollmentForm();
    }
    return enrollmForm;
  }

  public Iterator enrollmIterator() {
    return enrollmList.values().iterator();
  }

  public int size() {
    return enrollmList.size();
  }

  /**
   * Sets the forward 
   * @param newForward as String
   */
  public void setForward(String newForward) {
      this.forward = newForward;
  }

  /**
   * Returns the forward.
   * @return forward as String
   */
  public String getForward() {
      return this.forward;
  }

  /**
   * Sets the campaign page 
   * @param newPage as String
   */
  public void setCampaignpage(String newPage) {
      this.campaignpage = newPage;
  }

  /**
   * Returns the campaign page.
   * @return campaign page as String
   */
  public String getCampaignpage() {
      return this.campaignpage;
  }

  /**
   * Sets the enroll all flag 
   * @param enrollflag as boolean
   */
  public void setEnrollall(boolean newEnrollAll) {
      this.enrollall = newEnrollAll;
  }

  /**
   * Returns the enroll all flag .
   * @return enroll all flag  as boolean
   */
  public boolean isEnrollall() {
      return this.enrollall;
  }

  /**
   * Sets the flag if the enroll all flag was changed 
   * @param isChanged as boolean
   */
  public void setEnrollallchanged(boolean isChanged) {
      this.enrollallchanged = isChanged;
  }

  /**
   * Returns the flag if the enroll all flag was changed
   * @return changed flag  as boolean
   */
  public boolean isEnrollallchanged() {
      return this.enrollallchanged;
  }

}
