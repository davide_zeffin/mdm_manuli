/*****************************************************************************
    Class:        DocumentListSelectorForm
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author        Ralf Witt
    @version      0.1
    Created:      26 March 2001

*****************************************************************************/

package com.sap.isa.isacore.actionform.order;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.order.DocumentListFilter;
import com.sap.isa.core.logging.IsaLocation;

/**
 * FormBean that takes input data for document list selection. Using the struts
 * framework every HTML Form is accompanied by a dedicated ActionForm bean to
 * store the data typed into the form. This Bean is used to communicate between
 * the Action-Servlet and the Action class of that form.<br>
 * The ActionForm Beans have to follow JavaBeans conventions (no argument
 * constructor, get/set-Methods).
 *
 * @author Ralf Witt
 * @version 0.1
 */
public class DocumentListSelectorForm extends ActionForm implements Cloneable {

    /**
     * Name of FORM with the Document selection criteria
     */
    public static final String DOCUMENTSELECTFORM = "docselectform";
    /**
     * Name of DocumentType Selectbox
     */
    public static final String DOCUMENT_TYPE = "documentType";
    /**
     * Name of DocumentStatus SelectBox
     */
    public static final String DOCUMENT_STATUS = "documentStatus";
    /**
     * Name of Attribute Selectbox (documentId, documentName, purchaseOrder)
     */
    public static final String ATTRIBUTE = "attribute";
    /**
     * Name of AttributeValue Selectbox
     */
    public static final String ATTRIBUTE_VALUE = "attributeValue";
    /**
     * Name of Period Selectbox
     */
    public static final String PERIOD = "period";
    /**
     * Name of Month Selectbox
     */
    public static final String MONTH = "month";
    /**
     * Name of Year Inputfield
     */
    public static final String YEAR = "year";
    /**
     * Name of first ValiditySelection RadioButtonGroup
     */
    public static final String VALIDITYSELECTION = "validitySelection";
    /**
     * Name of soldto input field
     */
    public static final String SOLDTO = "soldto";
    /**
     * Constant indicating that no attribute has been specified
     */
    public static final String NOT_SPECIFIED = "null";
    /**
     * Order
     */
    public static final String ORDER = DocumentListFilter.SALESDOCUMENT_TYPE_ORDER;
    /**
     * OrderTemplate
     */
    public static final String ORDERTEMPLATE = DocumentListFilter.SALESDOCUMENT_TYPE_ORDERTEMPLATE;
    /**
     * Quotation
     */
    public static final String QUOTATION = DocumentListFilter.SALESDOCUMENT_TYPE_QUOTATION;
    /**
     * Invoice
     */
    public static final String INVOICE = DocumentListFilter.BILLINGDOCUMENT_TYPE_INVOICE;
    /**
     * DownPayment
     */
    public static final String DOWNPAYMENT = DocumentListFilter.BILLINGDOCUMENT_TYPE_DOWNPAYMENT;
    /**
     * CreditMemo
     */
    public static final String CREDITMEMO = DocumentListFilter.BILLINGDOCUMENT_TYPE_CREDITMEMO;
    /**
     * Contract
     */
    public static final String CONTRACT = "contract";
    /**
     * Auction
     */
    public static final String AUCTION = "auction";
    /**
     * Collective order
     */
    public static final String COLLECTIVE_ORDER = DocumentListFilter.SALESDOCUMENT_TYPE_COLLECTIVE_ORDER;
    /**
     * Status ACCEPTED (for quotations)
     */
    public static final String ACCEPTED = DocumentListFilter.SALESDOCUMENT_STATUS_ACCEPTED;
    /**
     * Status OPEN
     */
    public static final String OPEN = DocumentListFilter.SALESDOCUMENT_STATUS_OPEN;
    /**
     * Status COMPLETED
     */
    public static final String COMPLETED = DocumentListFilter.SALESDOCUMENT_STATUS_COMPLETED;
    /**
     * Status PARTLYDELIVERED
     */
    public static final String PARTLYDELIVERED = "partlydelivered";
    /**
     * Status RELEASED (for quotations)
     */
    public static final String RELEASED = DocumentListFilter.SALESDOCUMENT_STATUS_RELEASED;
    /**
     * Status READYTOPICKUP (for orders)
     */
    public static final String READYTOPICKUP = DocumentListFilter.SALESDOCUMENT_STATUS_READYTOPICKUP;
    /**
     * Status ALL
     */
    public static final String ALL = DocumentListFilter.SALESDOCUMENT_STATUS_ALL;
    /**
     * Attribute Value ObjectID
     */
    public static final String OBJECTID = "objectid";
    /**
     * Attribute Value Document Description
     */
    public static final String DESCRIPTION = "description";
    /**
     * Attribute Value PurchaseOrder
     */
    public static final String PURCHASEORDERNUMBER = "purchaseordernumber";
    /**
     * Attribute Value ProductID
     */
    public static final String PRODUCTID = "productid";
    /**
     * Attribute Value ReferenceDocumentNo.
     */
    public static final String REFERENCEDOC = "referencedoc";
    /**
     * Period TODAY
     */
    public static final String TODAY = "today";
    /**
     * Period SINCE_YESTERDAY
     */
    public static final String SINCE_YESTERDAY = "sinceyesterday";
    /**
     * Period LAST_WEEK
     */
    public static final String LAST_WEEK = "week";
    /**
     * Period LAST_MONTH
     */
    public static final String LAST_MONTH = "month";
    /**
     * Period LAST_YEAR
     */
    public static final String LAST_YEAR = "year";
    /**
     * Only by setting this Flag a Selection of SALES documents in a
     * Backend System will be performed
     */
    public static final String SETSALESSELECTIONSTART = "salesSelectionStart";
    /**
     * Only by setting this Flag a Selection of BILLING documents in a
     * Backend System will be performed
     */
    public static final String SETBILLINGSELECTIONSTART = "billingSelectionStart";
    /**
     * BusinessObject which carries all select criteria
     */
    private DocumentListFilter documentListFilter;
    private String attribute = " ";
    private String attributeValue = " ";
    private boolean salesSelectionStart = false;
    private boolean billingSelectionStart = false;
    private String period = " ";
    private String year = " ";
    private String month = " ";
    private String validitySelection = " ";
    private String soldto = " ";

    private String allMonth    = "JanFebMarAprMayJunJulAugSepOctNovDec";
    private String allMonthNum = "01 02 03 04 05 06 07 08 09 10 11 12";
    private int monthNum = 0,
                           yearNum = 0;
    private long gLdateFrom = 0l;

    private static final IsaLocation log =
    IsaLocation.getInstance("com.sap.isa.isacore.actionform.order.DocumenListSelectorForm");

    // no argument Constructor
    public DocumentListSelectorForm() {
        // Set default Values for DocumentListFilter
        documentListFilter = new DocumentListFilter();
        documentListFilter.setTypeORDER();
        documentListFilter.setStatusOPEN();
        this.attribute = NOT_SPECIFIED;
        this.validitySelection = PERIOD;
        this.period = LAST_WEEK;
        // set ChangedToDate on today
        documentListFilter.setChangedToDate(convDateToFormatYYYYMMDD(new Date().toString()));

        if(log.isDebugEnabled()) {
            log.debug("instance created");
        }
    }

    /**
     * Set Flag for SalesSelectionStart.
     * Corresponds to method isSalesSelectionStartTrue
     */
    public void setSalesSelectionStart(boolean selectionStart) {
        this.salesSelectionStart = selectionStart;
    }
    /**
     * Only start Selection in ...Action if this returns true.
     * Corresponds to method setSalesSelectionStartTrue
     */
    public boolean isSalesSelectionStartTrue() {
        return salesSelectionStart;
    }
    /**
     * Set Flag for BillingSelectionStart.
     * Corresponds to method isBillingSelectionStartTrue
     */
    public void setBillingSelectionStart(boolean selectionStart) {
        this.billingSelectionStart = selectionStart;
    }
    /**
     * Only start Selection in ...Action if this returns true.
     * Corresponds to method setBillingSelectionStartTrue
     */
    public boolean isBillingSelectionStartTrue() {
        return billingSelectionStart;
    }

    /**
     * Set the document type.
     */
    public void setDocumentType(String docType) {
        if(docType.equals(ORDER))
            documentListFilter.setTypeORDER();
        if(docType.equals(ORDERTEMPLATE)) {
            documentListFilter.setTypeORDERTEMPLATE();
            documentListFilter.setStatusALL();
        }
        if(docType.equals(QUOTATION))
            documentListFilter.setTypeQUOTA();
        if(docType.equals(INVOICE))
            documentListFilter.setTypeINVOICE();
        if(docType.equals(CREDITMEMO))
            documentListFilter.setTypeCREDITMEMO();
        if(docType.equals(DOWNPAYMENT))
            documentListFilter.setTypeDOWNPAYMENT();
    }
    /**
     * Set the document status
     */
    public void setDocumentStatus(String docStatus) {
        if(docStatus.equals(OPEN)) {
            documentListFilter.setStatusOPEN();
        } else if(docStatus.equals(ALL) || docStatus.equals(NOT_SPECIFIED)) {
            documentListFilter.setStatusALL();
        } else if(docStatus.equals(COMPLETED)) {
            documentListFilter.setStatusCOMPLETED();
        } else if (docStatus.equals(RELEASED)) {
            documentListFilter.setStatusRELEASED();
        } else if (docStatus.equals(ACCEPTED)) {
            documentListFilter.setStatusACCEPTED();
        } else if (docStatus.equals(READYTOPICKUP)) {
            documentListFilter.setStatusREADYTOPICKUP();
        } else {
            // NO system status set, set user status
            documentListFilter.SetStatusUSERSTATUS(docStatus);
        }

    }
    /**
     * Set Attribute (documentId, documentName, purchaseOrder)
     * See also method AttributeValue, only both can determine the right selection criteria
     */
    public void setAttribute(String attribute) {
        // Save for resending to JSP
        this.attribute = attribute;
    }
    /**
     * Get Attribute (documentId, documentName, purchaseOrder)
     */
    public String getAttribute() {
        return attribute;
    }
    /**
     * Get AttributeValue
     */
    public String getAttributeValue() {
        return this.attributeValue;
    }
    /**
     * Indicates if given attribute has been chosen.
     */
    public boolean isAttribute(String attribute) {
        return this.attribute.equalsIgnoreCase(attribute);
    }
    /**
     * Indicates if given DocumentType has been chosen.
     */
    public boolean isDocumentType(String docType) {
        return this.documentListFilter.getType().equalsIgnoreCase(docType);
    }
    /**
     * Indicates if given DocumentStatus has been chosen.
     */
    public boolean isDocumentStatus(String docStatus) {
        return this.documentListFilter.getStatus().equalsIgnoreCase(docStatus);
    }
    /**
     * Set AttributeValue
     * See also method Attribute, only both can determine the right selection criteria
     */
    public void setAttributeValue(String attributeValue) {
        // Save for resending to JSP
        this.attributeValue = attributeValue;
    }
    /**
     * Set Period. Period can also be number of days ("14" = for the last 14
     * days)
     * @param String Period can be like DocumentListSelectorForm.TODAY or a
     * number representing number of days to select.
     */
    public void setPeriod(String period) {
        // Save period for reprinting on JSP
        this.period = period;
        Integer xPeriod;

		// Set changedDate in DocumentListFilter for Selection
		Date date = new Date();
		long LdateTo = 0l,
              delta  = 0l;
		LdateTo = date.getTime();                                                 // dateTo is Today in Milliseconds
		
		try {
            // First check passed period is a number			
			xPeriod = new Integer(period);
			delta = (xPeriod.longValue() * 24l * 60l * 60l * 1000l);
		}
		catch(NumberFormatException ex) {
            // Not, check it is a valid selection period

            if(period.equals(TODAY)) {                                                // NO delta = today
            }
            if(period.equals(SINCE_YESTERDAY)) {
                delta = (1l* 24l * 60l * 60l * 1000l);                                // ONE Day in Milliseconds
            }
            if(period.equals(LAST_YEAR)) {
                delta = (365l* 24l * 60l * 60l * 1000l);                              // ONE Year in Milliseconds
            }
            if(period.equals(LAST_MONTH)) {
                delta = (30l* 24l * 60l * 60l * 1000l);                               // ONE Month in Milliseconds
            }
            if(period.equals(LAST_WEEK)) {
                delta = (7l* 24l * 60l * 60l * 1000l);                                // ONE Week in Milliseconds
            }
            if(period.equals(NOT_SPECIFIED)) {
                delta = (50l * 365l* 24l * 60l * 60l * 1000l);                        // FIFTY Years in Milliseconds
            }

		}
		gLdateFrom = LdateTo - delta;
    }
    /**
     * Get Period
     */
    public String getPeriod() {
        return period;
    }
    /**
     * Indicates if given Period has been chosen.
     */
    public boolean isPeriod(String period) {
        return this.period.equalsIgnoreCase(period);
    }
    /**
     * Set Month
     */
    public void setMonth(String month) {
        // Save month for reprinting on JSP
        this.month = month;
    }
    /**
     * Get Month
     */
    public String getMonth() {
        return month;
    }
    /**
     * Indicates if given Month has been chosen.
     */
    public boolean isMonth(String month) {
        return this.month.equalsIgnoreCase(month);
    }
    /**
     * Set Year
     */
    public void setYear(String year) {
        // Save year for reprinting on JSP
        this.year = year;
    }
    /**
     * Get Year
     */
    public String getYear() {
        return year;
    }
    /**
     * Indicates if given Year has been chosen.
     */
    public boolean isYear(String year) {
        return this.year.equalsIgnoreCase(year);
    }
    /**
     * Set ValiditySelection
     */
    public void setValiditySelection(String valSel) {
        this.validitySelection = valSel;
    }
    /**
     * Get ValiditySelection
     */
    public String getValiditySelection() {
        return this.validitySelection;
    }
    /**
     * Indicates if given validitySelection has been chosen.
     */
    public boolean isValiditySelection(String valSel) {
        return this.validitySelection.equalsIgnoreCase(valSel);
    }
    /**
     * Get DocumentListFilter
     */
    public DocumentListFilter getDocumentListFilter() {
        return this.documentListFilter;
    }

    /**
     * Set the property soldto
     *
     * @param soldto
     *
     */
    public void setSoldto(String soldto) {
        this.soldto = soldto;
    }


    /**
     * Returns the property soldto
     *
     * @return soldto
     *
     */
    public String getSoldto() {
        return this.soldto;
    }


    /**
     * Get DocumentType
     */
    public String getDocumentType() {
        return this.documentListFilter.getType();
    }

    public ActionErrors validate(ActionMapping ac, HttpServletRequest hsr) {
        if(month.equals(NOT_SPECIFIED) || month.equals("00")) {
            this.monthNum = 1;                                                   // January
        }
        else {
            try {
                this.monthNum = Integer.parseInt(month);
            }
            catch(NumberFormatException ex) {
                this.monthNum = 1;
            }
        }

        // Set changedDate in DocumentListFilter for Selection
        try {
            yearNum = Integer.parseInt(year);
        }
        catch(NumberFormatException ex) {
            yearNum = 1990;
        }
        // WE ONLY LOOK BACK FOR THE LAST 10 YEAR!!!!
        if(yearNum < 1990) {
            yearNum = 1990;
        }

        documentListFilter.setChangedToDate(convDateToFormatYYYYMMDD(new Date().toString()));
        if(this.validitySelection.equals(PERIOD)) {
            Date date = new Date();
            date.setTime(gLdateFrom);
            documentListFilter.setChangedDate(convDateToFormatYYYYMMDD(date.toString()));
        }
        else {
            if(monthNum < 10) {
                documentListFilter.setChangedDate((Integer.toString(yearNum) + "0" + Integer.toString(monthNum) + "01"));
            }
            else {
                documentListFilter.setChangedDate((Integer.toString(yearNum) + Integer.toString(monthNum) + "01"));
            }
        }

        // Reset AttributeValue
        documentListFilter.setDescription(null);
        documentListFilter.setExternalRefNo(null);
        documentListFilter.setId(null);
        documentListFilter.setProduct(null);
        if(attribute != null && !(attributeValue.trim().equals(""))) {
            // Fill DocumentListFilter
            if(DESCRIPTION.equals(attribute))
                documentListFilter.setDescription(attributeValue);
            if(PURCHASEORDERNUMBER.equals(attribute) || REFERENCEDOC.equals(attribute))
                documentListFilter.setExternalRefNo(attributeValue);
            if(OBJECTID.equals(attribute))
                documentListFilter.setId(attributeValue);
            if(PRODUCTID.equals(attribute))
                documentListFilter.setProduct(attributeValue);
        }

        return new ActionErrors();
    }
    /**
     * Get the entered Selectiondate in a backend conform format
     */
    private String convDateToFormatYYYYMMDD(String inDate) {
        String outDate = new String();

        outDate = outDate.concat(inDate.substring(inDate.length()-4, inDate.length()));     // YYYY
        outDate = outDate.concat(allMonthNum.substring( allMonth.indexOf(inDate.substring(4,7)) , allMonth.indexOf(inDate.substring(4,7)) + 2 ));
        outDate = outDate.concat(inDate.substring(8,10));
        return outDate;
    }

    /**
     * Convert the content of this object into a string.
     *
     * @return String representation of this object
     */
    public String toString() {
        StringBuffer buf = new StringBuffer(300);

        buf.append("DocumentListSelectorForm [")
        .append("attribute='")         .append(getAttribute()).append("', ")
        .append("attributeValue='")    .append(getAttributeValue()).append("', ")
        .append("documentType='")      .append(getDocumentType()).append("', ")
        .append("month='")             .append(getMonth()).append("', ")
        .append("period='")            .append(getPeriod()).append("', ")
        .append("validiySection='")    .append(getValiditySelection()).append("', ")
        .append("year='")              .append(getYear()).append("', ")
        .append("soldto='")            .append(getSoldto())
        .append("documentListFilter='").append(getDocumentListFilter())
        .append(']');

        return buf.toString();
    }

    /**
     * Returns a shllow copy of this object. Because of the fact that this
     * class only contains immutuable objects like String and primitive
     * types, the shallow copy produced by this method behaves like a deep copy.
     * For the encapsulated DocumentListFilter an extra deep copy operation
     * is performed.
     *
     * @return shallow copy of the object
     */
    public Object clone() {
        try {
            DocumentListSelectorForm copy =
            (DocumentListSelectorForm) super.clone();

            // special deep copy of the documentListFilter
            copy.documentListFilter = (DocumentListFilter) documentListFilter.clone();

            return copy;
        }
        catch(CloneNotSupportedException e) {
            return null;
        }
    }
}