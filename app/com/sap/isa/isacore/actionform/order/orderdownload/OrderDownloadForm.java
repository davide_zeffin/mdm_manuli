package com.sap.isa.isacore.actionform.order.orderdownload;

import java.util.ArrayList;
import java.util.StringTokenizer;

import org.apache.struts.action.ActionForm;
import com.sap.isa.isacore.action.ActionConstantsBase;

/**
 * FormBean that takes input data for document list selection. Using the struts
 * framework every HTML Form is accompanied by a dedicated ActionForm bean to
 * store the data typed into the form. This Bean is used to communicate between
 * the Action-Servlet and the Action class of that form.<br>
 * The ActionForm Beans have to follow JavaBeans conventions (no argument
 * constructor, get/set-Methods).
 *
 */
public class OrderDownloadForm extends ActionForm implements Cloneable {
  public String IDDELIMITER = ";";
  public OrderDownloadForm() {
  }
  public static final String ORDERGUIDSPARAM = "orderGUIDS";
  public static final String ORDERIDSPARAM = "orderIDS";
  String startDate;
  String endDate;
  String format;
  String orderId;
  ArrayList orderIds = new ArrayList();
  ArrayList orderGuids = new ArrayList();

  boolean idsSet =false;
  boolean isOrderDetail = false;

  /**
   * Constant for the XML format for the Order
   */
  public static final String XML_FORMAT_STR = ActionConstantsBase.ORDDOWN_XML_FORMAT_STR;
  /**
   * Constant for the PDF format for the Order
   */
  public static final String PDF_FORMAT_STR = ActionConstantsBase.ORDDOWN_PDF_FORMAT_STR;

  /**
   * Constant for the CSV format for the Order
   */
  public static final String CSV_FORMAT_STR = ActionConstantsBase.ORDDOWN_CSV_FORMAT_STR;
  
 
  public String getStartDate(){
    return startDate;
  }

  public void setStartDate(String val){
    this.startDate= val;
  }

  public String getEndDate(){
    return startDate;
  }

  public void setEndDate(String val){
    this.endDate= val;
  }
  
  public String getFormat(){
    return format;
  }

  public void setFormat(String val){
    this.format = val;
  }
  public String getOrderID(){
    return orderId;
  }
  public void setOrderID(String val){
    this.orderId = val;
  }

  public boolean areIDSset() {
    return idsSet;
  }

  public void  setOrderDetail(String val) {
    isOrderDetail = true;
  }
  
  public boolean  isOrderDetail() {
    return isOrderDetail;
  }  
  public void setOrderIDS(String val){
    idsSet = true;
    if(val==null) return;
    int i=0;
    String str=null;
    StringTokenizer strTok = new StringTokenizer(val,IDDELIMITER);
    while(strTok.hasMoreElements()) {
        str = (String)strTok.nextElement();
        orderIds.add(i,str);
	i++;
    }
  }

  public void setOrderGUIDS(String val){
    if(val==null) return;
    int i=0;
    String str=null;
    StringTokenizer strTok = new StringTokenizer(val,IDDELIMITER);
    while(strTok.hasMoreElements()) {
        str = (String)strTok.nextElement();
        orderGuids.add(i,str);
	i++;
    }
  }  
  public ArrayList getOrderIDS(){
      return orderIds;
  }

  public ArrayList getOrderGUIDS(){
      return orderGuids;
  }

  public String getOrderGuid(int i){
      return (String)orderGuids.get(i);
  }

  public   static void  main(String[] args ) 
  {
    StringTokenizer strTok = new StringTokenizer("123$guid1;456$guid2",";");
    String str=null;
    while(strTok.hasMoreElements()) {
      str = (String)strTok.nextElement();
      int index = str.indexOf("$");
      if(index!=-1) {
        System.out.println(" id is " + str.substring(0,index));
        System.out.println("guid is " + str.substring(index+1));
      }
    }
  }
}