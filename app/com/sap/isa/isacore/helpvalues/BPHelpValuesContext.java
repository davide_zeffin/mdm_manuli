/*****************************************************************************
    Class:        BPHelpValuesContext
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      22.03.2004
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.isacore.helpvalues;

import javax.servlet.http.HttpServletRequest;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businesspartner.helpvalues.BPHelpValuesContextBase;

/**
 * The class BPHelpValuesContext is the isa core implementation of the 
 * {@link com.sap.isa.businesspartner.helpvalues.BPHelpValuesContextBase} class . <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class BPHelpValuesContext extends BPHelpValuesContextBase {

	/**
	 * Reference to business object manager. <br>
	 */
	protected BusinessObjectManager bom;
	
	/**
	 * Reference to shop object. <br>
	 */
	protected Shop shop;


    /**
     * Initialize the context . <br>
     * Determine shop and bom.
     * 
     * @param request http request.
     * 
     * @see com.sap.isa.helpvalues.HelpValuesContextSupport#initContext(javax.servlet.http.HttpServletRequest)
     */
    public void initContext(HttpServletRequest request) {
		super.initContext(request);
		
		// get BOM
		bom = (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
		
		shop = bom.getShop();
	}


	/**
	 * Return if the business partner hierarchy should be used. <br>
	 * This implementation returns the value in the shop setting.
	 * 
	 * @return <code>true</code> by default.
	 */	
	protected boolean useHierarchy() {
		return shop.isBpHierarchyEnable();								 	
	}


}
