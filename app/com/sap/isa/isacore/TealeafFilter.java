package com.sap.isa.isacore;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.ServletException;

import com.sap.isa.core.logging.IsaLocation;

/**
 * Wrapper for tealeaf filter to prevent instantiation when 
 * tealeaf jar is not in classpath. Thus prevent the application initialization 
 * failure
 * uses filter init parameter tealeaf-filter to get the tealeaf filter class name
 * Allowed values <b>com.tealeaf.capture.CaptureFilter</b>,<b> com.tealeaf.capture.LiteFilter</b>
 * 
 */
public final class TealeafFilter implements Filter {
	
	private TealeafFilterConfig filterConfig;
	private final static IsaLocation loc =
		IsaLocation.getInstance(TealeafFilter.class.getName());
	private boolean initialized = false; // reflects whether actuail tealeaf filter is initialized
										// to prevent multiple initializations 
		
	private Filter tealeafFilter = null; //instance of tealeaf filter based on init-param tealeaf-filter
	
	public FilterConfig getFilterConfig() {
		return filterConfig;
	}

	public void setFilterConfig(FilterConfig filterConfig) {
		this.filterConfig = new TealeafFilterConfig(filterConfig);
	}

	/**
	 * Process the tealeaf filter, if it exists
	 * In the end give the control to next in the chain
	 */
	public void doFilter(
		ServletRequest request,
		ServletResponse response,
		FilterChain chain)
		throws IOException, ServletException {
			

		if(!initialized){ //initialize tealeaf filter if not initialized
			if(loc.isDebugEnabled())
				loc.debug("Initializing the tealeaf filter");
			initializeTealeafFilter();
		}
		//call appropriate tealeaf filter's doFilter method
		if(tealeafFilter != null){
			if(loc.isDebugEnabled())
				loc.debug("invoking doFilter method of "+tealeafFilter);
			try {
				tealeafFilter.doFilter(request,response,chain);
			} catch (IOException e) {
				// ignore exception
				if(loc.isDebugEnabled())
					loc.debug("exception occured while processing servlet request "+e);
			} catch (ServletException e) {
				// ignore exception
				if(loc.isDebugEnabled())
					loc.debug("exception occured while processing servlet request "+e);
			} catch (Throwable t){//catch runtime exceptions
				if(loc.isDebugEnabled())
					loc.debug("exception occured while processing servlet request "+t);
			}
		}else{ //if no filter exist, pass it to next one in chain
			chain.doFilter(request,response);
		}
			
	}

	/* 
	 * Instantiate and init tealeaf filter if it exists in classpath
	 * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
	 */
	public void initializeTealeafFilter() throws ServletException {
		this.initialized = true;
		String filterClassName = filterConfig.getInitParameter("tealeaf-filter");
		if(filterClassName != null){
			Class tealeafFilterClass;
			try {
				tealeafFilterClass = Class.forName(filterClassName);
				tealeafFilter = (Filter) tealeafFilterClass.newInstance();
				if(loc.isDebugEnabled())
					loc.debug("Tealeaf filter is initialized with filter class set to "+ filterClassName);
			} catch (ClassNotFoundException e) {
				loc.debug("wec.tealeaf.checklib",e);
				loc.error(e.toString()); //don't print the whole stack trace, clutters the logs
			} catch (InstantiationException e) {
				loc.debug("wec.tealeaf.checklib",e);
				loc.error(e.toString()); //don't print the whole stack trace, clutters the logs
			} catch (IllegalAccessException e) {
				loc.debug("wec.tealeaf.checklib",e);
				loc.error(e.toString()); //don't print the whole stack trace, clutters the logs
			}
			
		}
		else{
			if(loc.isInfoEnabled())
				loc.info("Tealeaf filter is not initialized as tealeaf-filter parameter is not passed in");
		}
		//init appropriate tealeaf filter
		if(tealeafFilter != null){
			if(loc.isDebugEnabled())
				loc.debug("invoking init method of "+tealeafFilter);
			try {
				tealeafFilter.init(this.filterConfig);
			} catch (ServletException e) {
				loc.debug("wec.tealeaf.checkconfig",e);
				loc.error(e.toString()); //don't print the whole stack trace, clutters the logs
				tealeafFilter = null; //act as if tealeaf filter is not defined
			} catch (Throwable t){
				loc.debug("wec.tealeaf.checkconfig",t);
				loc.error(t.toString()); //don't print the whole stack trace, clutters the logs
				tealeafFilter = null; //act as if tealeaf filter is not defined
			}
		}

		if(tealeafFilter == null)
			loc.error("wec.tealeaf.checkconfig"); 
	}


	/* 
	 * Call destroy of tealeaf filter if it exists
	 * @see javax.servlet.Filter#destroy()
	 */
	public void destroy() {
		//destroy the configured tealeaf filter
		if(tealeafFilter != null){
			if(loc.isDebugEnabled())
				loc.debug("invoking destroy method of "+tealeafFilter);
			tealeafFilter.destroy();	
		}
	}

	/* (non-Javadoc)
	 * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
	 */
	public void init(FilterConfig afilterConfig) throws ServletException {
		this.setFilterConfig(afilterConfig);
		//postpone actual Tealeaf filter initialization to doFilter method
		//Temporary fix for bug in SAP implementation of servlet engine 
	}

}

