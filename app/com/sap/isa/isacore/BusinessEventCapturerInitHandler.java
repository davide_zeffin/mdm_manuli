/*****************************************************************************
    Class         BusinessEventCapturerInitHandler
    Copyright (c)
    Author:       Pavan Bayyapu
    Created:      May 2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2002/10/25 $
*****************************************************************************/
package com.sap.isa.isacore;

import java.lang.reflect.Method;
import java.util.Properties;

import com.sap.isa.core.init.Initializable;
import com.sap.isa.core.init.InitializationEnvironment;
import com.sap.isa.core.init.InitializeException;
import com.sap.isa.core.logging.IsaLocation;


/**
 * This class set the correct property file and class for the BusinessEventTealeafCapturer
 * If you want to use your own capture you can can extend your class from
 * <code>BusinessEventCapturerInitHandler</code> and change the property
 * event-handler to your own class.
 *
 * @see com.sap.isa.isacore.BusinessEventTealeafCapturer
 *
 */
public class BusinessEventCapturerInitHandler implements Initializable {

    // instance of the BusinessEventCapturerImpl
    private static BusinessEventCapturer instance;
    private static boolean activated = false;
	private static final IsaLocation location = IsaLocation.getInstance(BusinessEventCapturerInitHandler.class.getName());
    /**
    * Initialize the implementing component.
    *
    * @param env the calling environment
    * @param props   component specific key/value pairs
    * @exception InitializeException at initialization error
    */
    public void initialize(InitializationEnvironment env, Properties props)
             throws InitializeException {
        try {
        	if (location.isDebugEnabled())
        		location.debug("Initializing business event capturer with properties " + props);
        	
            String propertyFile=env.getRealPath(props.getProperty("property-file"));
            String className=props.getProperty("event-handler");
            String activateStr=props.getProperty("activate");

            if("true".equalsIgnoreCase(activateStr)){
              activated = true;
            }
            else
              activated = false;

            if (className == null || className.length() == 0) {
                className = "com.sap.isa.isacore.BusinessEventCapturerImpl";
            }

            // take the handler class from init file
            Class handlerClass = Class.forName(className);

            // now get the instance from the handle
            instance = (BusinessEventCapturer) handlerClass.newInstance();

            String availabilityCheck = props.getProperty("availibiltyCheckAvailable");

            if (availabilityCheck!= null && availabilityCheck.equals("true")) {

                // get the init method from the class
                Method method = handlerClass.getMethod("setAvailibiltyCheckAvailable",
                                                       new Class[]{String.class});

                // invoke the method to initialize the handler
                method.invoke(null, new Object[]{new Boolean(true)});
            }

			if (location.isDebugEnabled())
				location.debug("Finished initializing business event capturer");
        } catch (Exception ex) {
          new InitializeException(ex.toString());

        }
        catch (Throwable t){
			new InitializeException(t.toString());
        }
    }


    /**
     * Returns the instance of the singelton BusinessEventCapturer
     *
     * @return instance
     *
     */
    public static BusinessEventCapturer getInstance() {
       return instance;
    }

    public static boolean isActivated(){
      return activated;
    }

    /**
    * Terminate the component.
    */
    public void terminate() {
    }


}
