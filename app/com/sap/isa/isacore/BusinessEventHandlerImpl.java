/*****************************************************************************
    Class         BusinessEventHandlerImpl
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      May 2001
    Version:      1.0

    $Revision: #2 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.isacore;

import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.businessevent.AddToDocumentEvent;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businessobject.businessevent.CreateDocumentEvent;
import com.sap.isa.businessobject.businessevent.DropDocumentEvent;
import com.sap.isa.businessobject.businessevent.DropFromDocumentEvent;
import com.sap.isa.businessobject.businessevent.LoginEvent;
import com.sap.isa.businessobject.businessevent.ModifyDocumentItemEvent;
import com.sap.isa.businessobject.businessevent.PlaceOrderEvent;
import com.sap.isa.businessobject.businessevent.RegisterEvent;
import com.sap.isa.businessobject.businessevent.SearchEvent;
import com.sap.isa.businessobject.businessevent.ViewCategoryEvent;
import com.sap.isa.businessobject.businessevent.ViewDocumentEvent;
import com.sap.isa.businessobject.businessevent.ViewProductEvent;
import com.sap.isa.businessobject.webcatalog.CatalogBusinessObjectManager;
import com.sap.isa.core.businessobject.event.BusinessEvent;
import com.sap.isa.core.businessobject.event.BusinessEventHandlerBaseImpl;


/**
 * This class handles the business events, and capture the events with
 * the BusinessCapturer which logs Business Objects
 *
 * @see BusinessEventManager
 * @see BusinessEventLogger
 * @see BusinessEvent
 *
 */
public class BusinessEventHandlerImpl extends    BusinessEventHandlerBaseImpl
                                      implements BusinessEventHandler {

    private BusinessObjectManager bom;
    private CatalogBusinessObjectManager cbom;

    /**
     * Standard Constructor
     */
    public BusinessEventHandlerImpl() {
    }


    /**
     * Constructs a new object already providing a reference to the
     * bom and the request.
     *
     * @param bom A reference to the business object manager
     * @param request A reference to the request
     */
    public BusinessEventHandlerImpl(BusinessObjectManager bom,
                                    CatalogBusinessObjectManager cbom,
                                    HttpServletRequest request) {
        this.bom = bom;
        this.cbom = cbom;
        this.request = request;
    }

    /**
     * Set the property bom
     *
     * @param bom
     *
     */
    public void setBom(BusinessObjectManager bom) {
        this.bom = bom;
    }


    /**
     * Set the property catalog bom
     *
     * @param bom
     *
     */
    public void setCatalogBom(CatalogBusinessObjectManager cbom) {
        this.cbom = cbom;
    }

    /**
     * Returns the property bom
     *
     * @return bom
     *
     */
    public BusinessObjectManager getBom() {
       return bom;
    }

    /**
     * Returns the property catalog bom
     *
     * @return cbom
     *
     */
    public CatalogBusinessObjectManager getCatalogBom() {
       return cbom;
    }


    /**
     * Fired a general business event
     *
     */
    public void fireBusinessEvent(BusinessEvent event) {
        if (log.isDebugEnabled()) log.debug("fireBusinessEvent called");
        Iterator i = listener.iterator();
        while (i.hasNext()) {
            BusinessEventCapturer eventCapturer = (BusinessEventCapturer)i.next();
            eventCapturer.captureBusinessEvent(event, bom, request);
        }
		if (log.isDebugEnabled()) log.debug("fireBusinessEvent finished");
    }


    /**
     * Fired a login event
     *
     */
    public void fireLoginEvent(LoginEvent event) {
		if (log.isDebugEnabled()) log.debug("firing LoginEvent ");
        Iterator i = listener.iterator();
        while (i.hasNext()) {
            BusinessEventCapturer eventCapturer = (BusinessEventCapturer)i.next();
            eventCapturer.captureLoginEvent(event, bom, request);
        }
		if (log.isDebugEnabled()) log.debug("LoginEvent fired");
    }


    /**
     * Fired a register event
     *
     */
    public void fireRegisterEvent(RegisterEvent event) {
		if (log.isDebugEnabled()) log.debug("Firing RegisterEvent ");
        Iterator i = listener.iterator();
        while (i.hasNext()) {
            BusinessEventCapturer eventCapturer = (BusinessEventCapturer)i.next();
            eventCapturer.captureRegisterEvent(event, bom, request);
        }
		if (log.isDebugEnabled()) log.debug("RegisterEvent fired");
    }


    /**
     * Fire a Search event
     *
     */
    public void fireSearchEvent(SearchEvent event) {
		if (log.isDebugEnabled()) log.debug("Firing search event ");
        Iterator i = listener.iterator();
        while (i.hasNext()) {
            BusinessEventCapturer eventCapturer = (BusinessEventCapturer)i.next();
            eventCapturer.captureSearchEvent(event, bom, request);
        }
		if (log.isDebugEnabled()) log.debug("search event fired");
    }


    /**
     * Fire a CreateDocument event
     *
     */
    public void fireCreateDocumentEvent(CreateDocumentEvent event) {
		if (log.isDebugEnabled()) log.debug("Firing CreateDocument event ");
        Iterator i = listener.iterator();
        while (i.hasNext()) {
            BusinessEventCapturer eventCapturer = (BusinessEventCapturer)i.next();
            eventCapturer.captureCreateDocumentEvent(event, bom, request);
        }
		if (log.isDebugEnabled()) log.debug("CreateDocument event fired");
    }


    /**
     * Fire a ViewDocument event
     *
     */
    public void fireViewDocumentEvent(ViewDocumentEvent event) {
		if (log.isDebugEnabled()) log.debug("Firing view document event ");
        Iterator i = listener.iterator();
        while (i.hasNext()) {
            BusinessEventCapturer eventCapturer = (BusinessEventCapturer)i.next();
            eventCapturer.captureViewDocumentEvent(event, bom, request);
        }
		if (log.isDebugEnabled()) log.debug("view document event fired");
    }

    /**
     * Fired an AddToDocument event
     *
     */
    public void fireAddToDocumentEvent(AddToDocumentEvent event) {
		if (log.isDebugEnabled()) log.debug("firing add to document event ");
        Iterator i = listener.iterator();
        while (i.hasNext()) {
            BusinessEventCapturer eventCapturer = (BusinessEventCapturer)i.next();
            eventCapturer.captureAddToDocumentEvent(event, bom, cbom, request);
        }
		if (log.isDebugEnabled()) log.debug("add to document event fired");
    }


    /**
     * Fired an ModifyDocumentItem event
     *
     */
    public void fireModifyDocumentItemEvent(ModifyDocumentItemEvent event) {
		if (log.isDebugEnabled()) log.debug("Firing modify document event ");
        Iterator i = listener.iterator();
        while (i.hasNext()) {
            BusinessEventCapturer eventCapturer = (BusinessEventCapturer)i.next();
            eventCapturer.captureModifyDocumentItemEvent(event, bom, cbom, request);
        }
		if (log.isDebugEnabled()) log.debug("Fired modify document event ");
    }


    /**
     * Fire a DropFromDocument event
     *
     */
    public void fireDropFromDocumentEvent(DropFromDocumentEvent event) {
		if (log.isDebugEnabled()) log.debug("Firing drop from document event ");
        Iterator i = listener.iterator();
        while (i.hasNext()) {
            BusinessEventCapturer eventCapturer = (BusinessEventCapturer)i.next();
            eventCapturer.captureDropFromDocumentEvent(event, bom, cbom, request);
        }
		if (log.isDebugEnabled()) log.debug("Fired drop from document event ");
    }


    /**
     * Fire a DropDocument event
     *
     */
    public void fireDropDocumentEvent(DropDocumentEvent event) {
		if (log.isDebugEnabled()) log.debug("firing drop document event ");
        Iterator i = listener.iterator();
        while (i.hasNext()) {
            BusinessEventCapturer eventCapturer = (BusinessEventCapturer)i.next();
            eventCapturer.captureDropDocumentEvent(event, bom, request);
        }
		if (log.isDebugEnabled()) log.debug("fired drop document event ");
    }


    /**
     * Fire a PlaceOrder event
     *
     */
    public void firePlaceOrderEvent(PlaceOrderEvent event) {
		if (log.isDebugEnabled()) log.debug("Firing place order event ");
        Iterator i = listener.iterator();
        while (i.hasNext()) {
            BusinessEventCapturer eventCapturer = (BusinessEventCapturer)i.next();
            eventCapturer.capturePlaceOrderEvent(event, bom, request);
        }
		if (log.isDebugEnabled()) log.debug("Fired place order event ");
    }


    /**
     * Fire a ViewCategory event
     *
     */
    public void fireViewCategoryEvent(ViewCategoryEvent event) {
		if (log.isDebugEnabled()) log.debug("firing View Category event");
        Iterator i = listener.iterator();
        while (i.hasNext()) {
            BusinessEventCapturer eventCapturer = (BusinessEventCapturer)i.next();
            eventCapturer.captureViewCategoryEvent(event, bom, request);
        }
		if (log.isDebugEnabled()) log.debug("fired View Category event");
    }


    /**
     * Fire a ViewProduct event
     *
     */
    public void fireViewProductEvent(ViewProductEvent event) {
		if (log.isDebugEnabled()) log.debug("firing View product event");
        Iterator i = listener.iterator();
        while (i.hasNext()) {
            BusinessEventCapturer eventCapturer = (BusinessEventCapturer)i.next();
            eventCapturer.captureViewProductEvent(event, bom, cbom, request);
        }
		if (log.isDebugEnabled()) log.debug("fired View product event");
    }


}
