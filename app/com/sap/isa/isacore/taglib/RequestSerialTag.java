/*****************************************************************************
    Class:        RequestSerialTag
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Thomas Smits
    Created:      29.8.2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/06/26 $
*****************************************************************************/

package com.sap.isa.isacore.taglib;

import javax.servlet.jsp.tagext.TagSupport;
import javax.servlet.jsp.*;
import java.io.*;

/**
 * <p>
 * This tag allows you, to mark every request with an unique request id. This
 * id can be used by the <code>EComBaseAction</code> to identify multiple
 * invocations on the same action. This effect may be caused by impatient
 * users hitting the submit button twice or more times.<br><br>
 * You can use this tag in two different modes for GET and POST requests. For
 * a GET request, the tag generates a name value pair, that you have to
 * include in your request URL. For example:
 * <pre>
 *    &lt;a href="someaction.do?param1=66&amp;<b>&lt;isacore:requestSerial mode="GET"/&gt;</b>&amp;bla=blupp"&gt;
 *       Don't click me
 *    &lt;/a&gt;
 * </pre>
 * For the usage in POST requests and forms (even is they are submitted using the
 * GET method, you simply add the tag to the form you
 * want to detect multiple invocations on. For example:
 * <pre>
 *    &lt;form action="someaction.do" method="POST"&gt;
 *        &lt;isacore:requestSerial mode="POST"/&gt;
 *    &lt;/form&gt;
 * </pre>
 * If you have added the tag using one of the two methods, the
 * <code>IsaCoreBaseAction</code> will automatically detect the multiple
 * invocations on your action and set the corresponding parameter of the
 * <code>isaPerform</code> method.
 *
 * @author SAP
 * @version 1.0
 */
public class RequestSerialTag extends TagSupport {

    /**
     * Name of the request parameter used to detect multiple invocations
     * of an action. This name is reserved for the internal mechanism
     * and should not be used by any programmer.
     */
    public static final String RC_REQUESTSERIAL = "requestserial";


    private static final int MODE_GET = 1;
    private static final int MODE_POST = 2;

    private static int serialCounter = 0;

    private int transferMode;

    /**
     * Creates a new instance of the tag handler.
     */
    public RequestSerialTag() {
    }

    /**
     * Sets the mode of the tag. Possible values are <code>"GET"</code>
     * and <code>"POST"</code>.
     *
     * @param mode the mode to be set
     */
    public void setMode(String mode) {
        if (mode.equalsIgnoreCase("get")) {
            transferMode = MODE_GET;
        }
        else if (mode.equalsIgnoreCase("post")) {
            transferMode = MODE_POST;
        }
    }


    /**
     * @exception JspException if a JSP exception has occurred
     */
    public int doStartTag() throws JspException {
        return SKIP_BODY;
    }

    /**
     *
     * @exception JspException if a JSP exception has occurred
     */
    public int doEndTag() throws JspException {

        // increment the serial number. According to the JLS operations
        // on 32bit integral data types are performed atomically and are
        // guranteed to be synchronized.
        int myCounter = serialCounter++;

        StringBuffer output = new StringBuffer();

        if (transferMode == MODE_GET) {
            output.append(RC_REQUESTSERIAL)
                  .append("=")
                  .append(myCounter);
        }
        else if (transferMode == MODE_POST) {
            output.append("<input type=\"hidden\" name=\"")
                  .append(RC_REQUESTSERIAL)
                  .append("\" value=\"")
                  .append(myCounter)
                  .append("\"/>");
        }

        try {
            JspWriter out = pageContext.getOut();
            out.print(output);
        } catch (IOException e) {
            throw new JspException();
        }


        // Continue processing this page
        return EVAL_PAGE;
    }

    /**
     * Drop the state of this tag handler. This method is called by the container
     * if reuse of tag handlers is implemented.
     */
    public void release() {
        transferMode = 0;
    }
}
