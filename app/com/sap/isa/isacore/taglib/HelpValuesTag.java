/*****************************************************************************
    Class:        HelpValuesTag
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP
    Created:      March 2004
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/06/26 $
*****************************************************************************/

package com.sap.isa.isacore.taglib;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyTagSupport;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.helpvalues.HelpValues;
import com.sap.isa.helpvalues.HelpValuesSearchManager;

/**
 * <p>
 * Search the help values for the help values search with the given name and 
 * Iterate over the result elements.  
 * </p>
 //TODO docu ueberarbeiten 
 * <p>
 * The use of this tag is quite simple, it searches for a instance of the
 * object to iterate over in page, request, session and application scope
 * (exactly in this order).<br>
 * The name of the object searched for is given using the <code>name</code>
 * parameter. If the object is found a scripting variable with the name set by
 * the <code>id</code> parameter is brought into the scripting context.<br>
 * <b>Note - </b> This variable is only available between the
 * <code>iterate</code> tags.
 * <p>
 * To set the type of the scripting variable correct (this prevents a lot
 * of casts) the <code>type</code> parameter is used. The given type and the
 * objects returned by the iterator must be compatible, otherwise the tag
 * will throw a JspException.
 * </p>
 * <p>
 * It is recommended to use the <code>ResultData</code> class for modelling
 * in memory result sets and to specifiy exactly this type in the
 * <code>type</code> parameter of the tag. The advantage of this approach is
 * that you don't have to write try/catch statements - the in memory
 * representation can hardly throw any <code>SQLExceptions.</code>
 * </p>
 * <p>
 * <b>Example: iterating over an implementor of Iterable</b>
 * <pre>
 *   &lt;table border="1"&gt;
 *     <b>&lt;isa:iterate id="soldto"
 *                  name="&lt;%= Application.RK_SOLD_TO_LIST %&gt;"
 *                  type="com.sap.isa.businessobject.SoldTo"&gt;</b>
 *       &lt;tr&gt;
 *         &lt;td&gt;<b>&lt;%= soldto.getName() %&gt;</b>&lt;/td&gt;
 *       &lt;/tr&gt;
 *     <b>&lt;/isa:iterate&gt;</b>
 *   &lt;/table&gt;
 * </pre>
 * </p>
 * <p>
 * <p>
 * This example shows how to use the iterate tag to create a table.
 * A request scope bean with
 * the name found in the constant <code>Application.RK_SOLD_TO_LIST</code>
 * is used and a scripting variable named <code>soldto</code> is created.
 * The type of the scriping variable is set to
 * <code>com.sap.isa.businessobject.SoldTo</code>. This variable can
 * be used in the code embraced by the iterate tag.
 * </p>
 * <b>Example: iterating over an array of String</b>
 * <pre>
 * &lt;%
 *    request.setAttribute("animalarray",new String[] { "Dog", "Mouse", "Horse", "Bigfoot" });
 * %&gt;
 *
 * &lt;isa:iterate id="animal"
 *     name="animalarray"
 *     type="java.lang.String"&gt;
 *     &lt;%= animal %&gt;&lt;br&gt;
 *  &lt;/isa:iterate&gt;
 * </pre>
 * </p>
 * <p>
 * If you want the tag to die silently if the object specified by the
 * <code>name</code> parameter is not found, set the optional
 * <code>ignorNull</code> parameter to <code>"on"</code> or <code>"true"</code>.
 * </p>
 * <p>
 * To reset a <code>ResultSet</code> to the first element, set the
 * <code>resetCursor</code> parameter to <code>"true"</code> or
 * <code>"on"</code>. The default value is <code>"false"</code>.
 * For all other types of iterable objects, this setting has no meaning.
 * </p>
 *
 * @author SAP
 * @version 1.0
 *
 * @see com.sap.isa.core.Iterable Iterable
 */
public class HelpValuesTag extends BodyTagSupport {

    private String name;    // Name of help values search that contains data
    private String id;      // Scripting variable to create
    private HelpValues helpValues;


    /**
     * Creates a new instance of the tag handler.
     */
    public HelpValuesTag() {
    }

    /**
     * Sets the name of the scripting variable created by this tag to
     * access the iterating data.
     *
     * @param id Name of the scripting variable
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Gets the name of the scripting variable created by this tag to
     * access the iterating data.
     *
     * @return Name of the scripting variable
     */
    public String getId() {
        return id;
    }


    /**
     * Sets the name of the request, session or application scope bean used
     * to iterate over.
     *
     * @param name The name of the bean
     */
    public void setName(String name) {
        this.name = name;
    }


    /**
     * Construct an iterator for the specified instance of <em>Iterable</em>,
     * and begin looping through the body once per element.
     *
     * @exception JspException if a JSP exception has occurred
     */
    public int doStartTag() throws JspException {


		UserSessionData userSessionData =
				UserSessionData.getUserSessionData(pageContext.getSession());

		if (userSessionData == null) {
			return SKIP_BODY;
		}

		helpValues = 
				HelpValuesSearchManager.getHelpValues(name, 
				               						  userSessionData, 
													  (HttpServletRequest)pageContext.getRequest());


        if (helpValues == null) {
            return SKIP_BODY;
        }

	
        helpValues.getValues().beforeFirst();

        if (helpValues.getValues().next()) {
           pageContext.setAttribute(id, helpValues);

           return EVAL_BODY_TAG;
        }
        else {
          return SKIP_BODY;
        }
    }


    /**
     * Make the next collection element available and loop, or
     * finish the iterations if there are no more elements.
     *
     * @exception JspException if a JSP exception has occurred
     */
    public int doAfterBody() throws JspException {

		if (helpValues.getValues().next()) {
           try {
               pageContext.setAttribute(id, helpValues);
               getPreviousOut().print(bodyContent.getString());
               bodyContent.clear();
           }
           catch (Exception e) {
             e.printStackTrace();
             throw new JspException("Incompatible Type");
           }

           return EVAL_BODY_TAG;
        }
        else {
          return SKIP_BODY;
        }
    }

    /**
     * Clean up after processing this enumeration.
     *
     * @exception JspException if a JSP exception has occurred
     */
    public int doEndTag() throws JspException {

        // Render any previously accumulated body content
        if (bodyContent != null) {

            try {
                JspWriter out = getPreviousOut();
                out.print(bodyContent.getString());
            } catch (IOException e) {
                e.printStackTrace();
                throw new JspException();
            }

        }

        // Continue processing this page
        return EVAL_PAGE;
    }

    /**
     * Drop the state of this tag handler. This method is called by the container
     * if reuse of tag handlers is implemented.
     */
    public void release() {
        super.release();
        name = null;
        id = null;
        helpValues = null;
    }

}