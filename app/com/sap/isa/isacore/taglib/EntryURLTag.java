/*****************************************************************************
    Class:        EntryURLTag
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      October 2001

    $Revision: #1 $
    $Date: 2001/08/12 $
*****************************************************************************/

package com.sap.isa.isacore.taglib;

import com.sap.isa.core.taglib.ReEntryURLTag;

/**
 * <p>
 * The tag insert the entry name of the application.
 * You can maintain an URL in web.xml with the flag <code>ContextConst.IC_ENTRY_URL</code>.
 * If no url is maintained the standard url is used with the parameter given
 * to the application.
 * <p>
 *
 * @depricated use ReEntryURLTag directly instead EntryURLTag
 *
 * @author Wolfgang Sattler
 * @version 1.0
 */
public class EntryURLTag extends ReEntryURLTag {


    /**
     * Create a new instance of the tag handler.
     */
    public EntryURLTag() {
    }


}
