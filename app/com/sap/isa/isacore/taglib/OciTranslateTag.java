/*****************************************************************************
    Class:        OciTranslateTag
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      12 August 2001

    $Revision: #1 $
    $Date: 2001/08/12 $
*****************************************************************************/

package com.sap.isa.isacore.taglib;

import javax.servlet.jsp.JspException;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.taglib.TranslateTag;
import com.sap.isa.isacore.action.IsaCoreInitAction;

/**
 * <p>
 * This Tag decorates the well-known <em>TranslateTag</em> and modifies its
 * functionality only in the following respect:
 * <p>
 * If an OCI transfer of baskets is required (i.e. the hook URL is set in the
 * calling URL), then the key passed is appended a suffix ".oci", and the
 * message for the resulting key is written into the JSP.
 * <p>
 *
 * @see com.sap.isa.core.taglib.TranslateTag TranslateTag
 */
public class OciTranslateTag extends TranslateTag {

    /**
     * Construct a new instance of this tag.
     */
    public OciTranslateTag() {
    	super();
    }

    /**
     * Process the start tag.
     *
     * @exception JspException if a JSP exception has occurred
     */
    public int doStartTag() throws JspException {

        // get user session data object
        UserSessionData userSessionData =
                UserSessionData.getUserSessionData(pageContext.getSession());

        // check if oci transfer is required
        boolean ociTransfer =
            (((IsaCoreInitAction.StartupParameter)userSessionData.
                getAttribute(IsaCoreInitAction.SC_STARTUP_PARAMETER)).
                getHookUrl().length() > 0);

        // determine right oci key from the original one
        if (ociTransfer) {
/*
            String originalKey = getKey();

            // some late minute workarounds
            if (originalKey.equals("b2b.docnav.order")) {
                setKey("b2b.docnav.basket.oci");
            }
            else if (originalKey.equals("b2b.cookie.order")) {
                setKey("b2b.cookie.basket.oci");
            }
            else {
                setKey(originalKey.concat(".oci"));
            }
*/
            setKey(getKey().concat(".oci"));
        }

        // call doStartTag on parent
        return super.doStartTag();
    }
}
