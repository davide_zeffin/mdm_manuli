/*****************************************************************************
    Class:        ShopFilterTag
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      July 2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/08/01 $
*****************************************************************************/

package com.sap.isa.isacore.taglib;

import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.lang.reflect.Method;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyTagSupport;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;

/**
 * <p>
 * The tag checks, if the body should be used, depending of properties of
 * the <code>shop</code> object. Therefore you must give a property and the
 * value for which the body should be used.
 * The shop is taken from he session context over the userData and must not exist
 * in the request context.
 * </p>
 * <b>Example</b>
 * <pre>
 *   &lt;isacore:shopfilter property="bestsellerAvailable" value = "true" &gt; <br>
 *     &lt;a href="&lt;isa:webappsURL name="b2b/bestseller.do"/&gt /%&gt; <br>
 *   &lt;/isa:message&gt;
 * </pre>
 *
 * <p>
 *
 * @author Wolfgang Sattler
 * @version 1.0
 *
 * @see com.sap.isa.businessobject.BusinessObjectBase BusinessObjectBase
 */
 public class ShopFilterTag extends BodyTagSupport {

    private String property ;
    private String value;
    private boolean ignoreNull; // Ignore null values in page context

    /**
     * Create a new instance of the tag handler.
     */
    public ShopFilterTag() {
    }


    /**
     * Sets the property of the shop object which must be compare with value
     *
     * @param property Name of property
     */
    public void setProperty(String property) {
        this.property = property;
    }


    /**
     * Set the property value
     *
     * @param value
     *
     */
    public void setValue(String value) {
        this.value = value;
    }


    /**
     *
     * @exception JspException if a JSP exception has occurred
     */
    public int doStartTag() throws JspException {

        // first I must read the shop.

        // get user session data object
        UserSessionData userSessionData =
                UserSessionData.getUserSessionData(pageContext.getSession());

        // check for missing context
        if (userSessionData == null) {
          return SKIP_BODY;
        }

       // get the MBOM
        MetaBusinessObjectManager mbom = userSessionData.getMBOM();

        // get BOM
        BusinessObjectManager bom =
             (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);


        // now I can read the shop
        Shop shop = bom.getShop();
        if (shop == null) {
            return SKIP_BODY;
        }

        try {
            PropertyDescriptor propertyDescriptor = new PropertyDescriptor(property,shop.getClass());

            Method method = propertyDescriptor.getReadMethod();

            Object valueObject = method.invoke(shop,null);

            String actualValue = valueObject.toString();

            if (actualValue.equals(value)) {
                return EVAL_BODY_TAG;
            }
        }
        catch (Exception ex) {
            System.out.println(ex);
            return SKIP_BODY;
        }

        return SKIP_BODY;

    }

    /**
     *
     * Nothing to do here
     *
     * @exception JspException if a JSP exception has occurred
     */
    public int doAfterBody() throws JspException {

          return SKIP_BODY;
    }

    /**
     * Clean up after processing this enumeration.
     *
     * @exception JspException if a JSP exception has occurred
     */
    public int doEndTag() throws JspException {

        // Render any previously accumulated body content
        if (bodyContent != null) {
            try {
                JspWriter out = getPreviousOut();
                out.print(bodyContent.getString());
            } catch (IOException e) {
                throw new JspException();
            }
        }
        // Continue processing this page
        return EVAL_PAGE;
    }

    /**
     * Drop the state of this tag handler. This method is called by the
     * container
     * if reuse of tag handlers is implemented.
     */
    public void release() {
        super.release();
        property = null;
        value = null;
        ignoreNull = false;
    }
}