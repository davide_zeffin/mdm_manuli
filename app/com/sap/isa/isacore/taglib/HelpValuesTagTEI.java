/*****************************************************************************
    Class:        HelpValuesTagTEI
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP
    Created:      March 2004
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/06/26 $
*****************************************************************************/

package com.sap.isa.isacore.taglib;

import javax.servlet.jsp.tagext.TagData;
import javax.servlet.jsp.tagext.TagExtraInfo;
import javax.servlet.jsp.tagext.VariableInfo;

import com.sap.isa.helpvalues.HelpValues;

/**
 * Utility class used only by the JSP-container to use <code>HelpValuesTag</code>.
 * This class is not used by the application developer and does not contain
 * any valuable documentation. Its only purpose is to provide some extra info
 * for the JSP-container.
 *
 * @author SAP
 * @version 1.0
 *
 * @see HelpValuesTag help values tag for more information.
 */
public final class HelpValuesTagTEI extends TagExtraInfo {

    /**
     * Look at the documentation of the superclass for more details.
     */
    public VariableInfo[] getVariableInfo(TagData data) {

	    return new VariableInfo[] {
	      new VariableInfo(data.getAttributeString("id"),
	                       HelpValues.class.getName(),
	                       true,
	                       VariableInfo.NESTED)
	    };
    }
}