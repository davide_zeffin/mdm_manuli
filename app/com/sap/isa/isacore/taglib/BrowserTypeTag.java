/*****************************************************************************
    Class:        BrowserTypeTag
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP
    Created:      10.10.2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/08/01 $
*****************************************************************************/

package com.sap.isa.isacore.taglib;

import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.isacore.action.DetermineBrowserVersionAction;

/**
 * This tag inserts a new variable with the name <code>browserType</code>
 * of the type <code>DetermineBrowserVersionAction.BrowserType</code> into
 * your JSP scripting context. You may use the methods of this variable
 * to determine the Version of the browser you are actual using.
 *
 * @author SAP
 * @version 1.0
 */
public class BrowserTypeTag extends TagSupport {

    /**
     * Creates a new instance of this class.
     */
    public BrowserTypeTag() {
    }

    /**
     * Callback for the container to notify the handler
     * that the opening tag is reached.
     *
     * @return value telling the container what to do next
     */
    public int doStartTag() throws JspException {

        // Get the session
        HttpSession session = pageContext.getSession();

        // Create the user session data
        UserSessionData userSessionData =
                UserSessionData.getUserSessionData(session);

        // get the browser information
        DetermineBrowserVersionAction.BrowserVersion browserType =
                (DetermineBrowserVersionAction.BrowserVersion)
                         userSessionData.getAttribute(DetermineBrowserVersionAction.SC_BROWSER_VERSION);

        if (browserType == null) {
            browserType = DetermineBrowserVersionAction.UNKNOWN_BROWSER;
        }
        pageContext.setAttribute("browserType", browserType);

        return SKIP_BODY;
    }

    /**
     * Callback for the container to notify the handler
     * that the closing tag is reached.
     *
     * @return value telling the container what to do next
     */
    public int doEndTag() throws JspException {
        return EVAL_PAGE;
    }

    /**
     * Releases the internal state of the tag handler. This
     * method is called by the container if it wants to
     * reuse the handler without creating a new instance.
     */
    public void release() {
    }

}
