/*****************************************************************************
    Class:        BrowserTypeTagTEI
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Thomas Smits
    Created:      10.10.2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/08/01 $
*****************************************************************************/

package com.sap.isa.isacore.taglib;

import javax.servlet.jsp.tagext.*;

/**
 * Info to the <code>browserType</code> tag.
 *
 * @author Thomas Smits
 * @version 1.0
 */
public final class BrowserTypeTagTEI extends TagExtraInfo {

    /**
     * Creates a new instance of this class.
     */
    public BrowserTypeTagTEI() {
    }

    public VariableInfo[] getVariableInfo(TagData data) {

        return new VariableInfo[] {
                new VariableInfo("browserType",
                        "com.sap.isa.isacore.action.DetermineBrowserVersionAction.BrowserVersion",
                        true,
                        VariableInfo.AT_END)
    };
    }
}
