/**
 *  Class:        ManagedDocument
 *  Copyright (c) 2001, SAP AG, All rights reserved.
 *  Author:       SAP AG
 *  Created:      29.05.2001
 *  Version:      1.0
 *  $Revision: #2 $
 *  $Date: 2002/10/29 $
 */

package com.sap.isa.isacore;

import com.sap.isa.businessobject.DocumentState;

/**
 * The ManagedDocument contains all data, which are needed for the control of
 * the UI infrastructure.
 * 
 * @see DocumentHandler
 * @see History
 * @see com.sap.isa.isacore.action.b2b.UpdateDocumentViewAction
 * 
 *@author     SAP AG
 *@created    6. Dezember 2001
 *@version    1.0
 */
public class ManagedDocument
         implements DocumentState {

    /**
     *  Type  of the document, e.g. "order", "quotation", "order template". The
     * history uses this string as part of a translation key ("b2b.
     * cookie."+docType).
     */
    protected String docType;

    /**
     *  Number of the document, given by the backend.
     */
    protected String docNumber;

    /**
     *  The reference number; the number the customer can assign to the document.
     */
    protected String refNumber;

    /**
     *  The reference name; the name the customer can assign to the document.
     */
    protected String refName;

    /**
     *  Date  of the document, this date can be a creation date, a  validity
     * date or another useful date, which can be displayed in the  history.
     */
    protected String docDate;

    /**
     *  String  for the logical forward mapping in the struts framework. The
     * UpdateDocumentViewAction uses this string as return value.
     * For this string an entry must exists in the config.xml, else you probably
     * get a runtime error.
     * 
     * @see com.sap.isa.isacore.action.b2b.UpdateDocumentViewAction
     */
    protected String forward;

    /**
     *  Href for displaying the document.<br>
     * When the history is displayed, the entries will be put in anchor tags.
     * Every entry in the history is a link to a document.  With href and
     * hrefParameter the anchor of the link is built: href+"?"+hrefParameter.
     * The result must be the action with the parameters for displaying the
     * wanted document.<br>
     * <b>If <code>href</code> is <code>null</code>, then this ManagedDocument
     * will not be added to the history</b>. So, with this parameter it is
     * possible to control, if the ManagedDocument is added to the history or
     * not.
     * 
     * @see #hrefParameter
     * @see DocumentHandler#add(ManagedDocument)
     * @see History#add(ManagedDocument)
     * @see #doc
     */
    protected String href;

    /**
     *  Additional parameters for the String href.
     * 
     * @see #href
     */
    protected String hrefParameter;

    /**
     *  Reference to the corresponding business object.<br>
     */
    protected DocumentState doc = null;

    /**
     * Reference to another ManagedDocument.<br>
     * This can be useful, if you haven't finished the editing of a document but
     * have to display another screen, for example with selection possibilities
     * or a simulation of pricing. In this case, you would create a new
     * ManagedDocument for the screen with the simulation, link the old
     * ManagedDocument to the new with this parameter and add the new
     * ManagedDocument to the DocumentHandler. Then, if the simulation is
     * finished, you can retrieve the old ManagedDocument from the new, add it
     * to the DocumentHandler and display the original document again.
     * 
     * @see DocumentHandler#add(ManagedDocument)
     */
    protected ManagedDocument associatedDoc = null;


    /**
     *  Constructor
     *
     *@param  doc            Reference off the corresponding business object.
     *@param  docType        Type of the document, e.g. "order", "quotation", "order template".
     *@param  docNumber      Number of the document, given by the backend.
     *@param  refNumber      The reference number; the number the customer can assign to the document.
     *@param  refName        The reference name; the name the customer can assign to the document.
     *@param  docDate        Date of the document.
     *@param  forward        String for the logical forward mapping in the struts framework.
     *@param  href           Href for displaying the document
     *@param  hrefParameter  Additional parameters for the String href.
     */
    public ManagedDocument(DocumentState doc, String docType, String docNumber, String refNumber,
            String refName, String docDate, String forward, String href, String hrefParameter) {
        this.doc = doc;
        this.docType = docType;
        this.docNumber = docNumber;
        this.refNumber = refNumber;
        this.refName = refName;
        this.docDate = docDate;
        this.forward = forward;
        this.href = href;
        this.hrefParameter = hrefParameter;
    }


    /**
     *  Sets    the state of the document.<br>
     * If the state is not of type TARGET_DOCUMENT or VIEW_DOCUMENT, the
     * ManagedDocument will not be added to the DocumentHandler. So it is
     * possible to add a ManagedDocument to the history and not to the
     * DocumentHandler.
     *
     * @param  state  the state as described by the constants of this interface
     *
     * @see com.sap.isa.businessobject.DocumentState
     * @see DocumentHandler#add(ManagedDocument)
     */
    public void setState(int state) {
        if (doc != null) {
            doc.setState(state);
        }
    }


    /**
     *  Gets the state of the document. 
     *
     *@return    the state as described by the constants of this interface
     *
     *@see com.sap.isa.businessobject.DocumentState
     */
    public int getState() {
        if (doc != null) {
            return doc.getState();
        }
        else {
            return DocumentState.UNDEFINED;
        }
    }

    /**
     *  Gets the associated document of this ManagedDocument.
     *
     *@return  ManagedDocument  the ManagedDocument, that is associated to this
     *ManagedDocument
     *
     *@see #associatedDoc
     */
    public ManagedDocument getAssociatedDocument() {
        return associatedDoc;
    }


    /**
     *  Sets the associated document of this ManagedDocument.
     *
     *@param  ManagedDocument  the MangedDocument, that is associated to this ManagedDocument
     *
     *@see #associatedDoc
     */
    public void setAssociatedDocument(ManagedDocument associatedDoc) {
        this.associatedDoc = associatedDoc;
    }


    /**
     *  Returns the reference on the business object.
     *  <code>null</code>, if there is no business object
     *
     *@return    Reference on the business object.
     *
     */
    public DocumentState getDocument() {
        return doc;
    }


    /**
     *  Returns the type of the document, e.g. "order", "quotation", "order template".
     *
     *@return    type of the document as a String
     *  <code>null</code>, if the document type is not set.
     * 
     * @see #docType
     */
    public String getDocType() {
        return docType;
    }


    /**
     *  Returns the number of the document, given by the backend.
     *
     *@return    number of the document as a String
     *  <code>null</code>, if the number is not set
     */
    public String getDocNumber() {
        return docNumber;
    }


    /**
     *  Returns the reference number; the number the customer can assign to the document.
     *
     *@return    reference number of the document as a String
     *  <code>null</code>, if the reference number is not set
     */
    public String getRefNumber() {
        return refNumber;
    }


    /**
     *  Returns the reference name; the name the customer can assign to the document.
     *
     *@return    reference name of the document as a String
     *  <code>null</code>, if the reference name is not set
     */
    public String getRefName() {
        return refName;
    }


    /**
     *  Returns  the date of the document.
     *
     *@return    date of the document as a String
     *  <code>null</code>, if the date is not set
     * 
     * @see #docDate
     */
    public String getDate() {
        return docDate;
    }


    /**
     * 	Href for displaying the document.
     *
     *@return    href for displaying the document
     *  <code>null</code>, if the href is not set.
     * 
     * @see #href
     */
    public String getHref() {
        return href;
    }


    /**
     *  Parameter for href.
     *
     *@return    The parameters for the action.
     *  <code>null</code>, if the hrefParameter is not set.
     * 
     * @see #hrefParameter
     */
    public String getHrefParameter() {
        return hrefParameter;
    }


    /**
     *  Returns the string for the logical forward mapping in the struts framework.
     *
     * @return    The ForwardMapping value
     *
     * @see #forward
     */
    public String getForwardMapping() {
        return forward;
    }

}
