/*****************************************************************************
    Class         ItemHierarchy
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Thomas Smits
    Created:      02.10.2001
    Version:      1.0

    $Revision: #9 $
    $Date: 2001/09/28 $
*****************************************************************************/

package com.sap.isa.isacore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.backend.boi.isacore.order.ItemListData;
import com.sap.isa.businessobject.item.ItemList;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.core.TechKey;

/**
 * Class able to group the flat list of items into a hierarchical
 * structure. This version is used to maintain a special view on the
 * items of an document without changing the data of the items
 * itself.
 * <p>
 * To use this class, create an instance giving the list of items you want to
 * control with as a parameter of the constructor. After that you
 * may ask for the status of an item by using the appropiate
 * method.
 * </p>
 * <pre>
 *   ItemHierarchy itemHierarchy = new ItemHierarchy(items);
 *   if (itemHierarchy.hasSubItems(item)) {
 *      // do something useful
 *   }
 * </pre>
 * <p>
 * <b>Note -</b> This implementation has two uses: 
 *		1. in case of product substitution, checks for the property
 *               itmUsage for . If this property is <code>ATP</code>
 *               the item is considered as an subitem in all other
 *               cases it is not an subitem. 
 *		2. in case of multi-level configurable materials, checks for matches of 
 *				 item.parentId and parent.itemguid, ie. the current item is 
 *				 sub-item to an existing node if its parentId matches the node's
 *				 item-guid
 *				 The assignment of the subitems to the main item is done using
 *               the order of the items in the item list. Therefore the
 *               order in the list is vital for the functionality of this
 *               class.
 * </p>
 *
 * @author Thomas Smits
 * @version 1.0
 */
public class ItemHierarchy {

    // NOTE: Some parts of the code are commented out, because they are not
    //       used in the current JSPs. Therefor I could not extensively test
    //       this code and removed the methods.
    private Node rootNode = new Node(null, null);
    private Map nodeCache = new HashMap();

	//used to find the next and previous first level main item
	protected static int NEXT 		= 1;
	protected static int PREVIOUS 	= 0;
	
    /**
     * Inner class for the creation of a tree list.
     */
    private class Node {
        private List subNodes = new ArrayList();
        private Map subKeys = new HashMap();
        private Node parent;
        private ItemSalesDoc referredItem;
        private TechKey itemGuid;
        private int level = 0;
        private boolean isRootConfigurable = false;
		private boolean isParentConfigurable = false;

        public Node(Node parent, ItemSalesDoc referredItem) {
            this.parent = parent;
            this.referredItem = referredItem;

            if (referredItem != null) {

                itemGuid = referredItem.getTechKey();
                level = parent.getLevel() + 1;

                if (referredItem.getParentId() == null ||  referredItem.getParentId().isInitial()) { // header level?
                    isRootConfigurable = referredItem.isConfigurable();
                    // retrieve isConfig property from referred item
                } else {
                    isRootConfigurable = parent.isRootConfigurable();
                    isParentConfigurable = parent.referredItem.isConfigurable();
                    // sub-item is marked configurable if header item is configable
                }
            } else { // root node
                level = 0;
                itemGuid = new TechKey(null);
            }
        }

        public boolean isRootConfigurable() {
            return isRootConfigurable;
        }
        
		public boolean isParentConfigurable() {
			return isParentConfigurable;
		}

        public int getLevel() {
            return level;
        }

        public void addNode(Node node) {
            subNodes.add(node);

            if (!node.itemGuid.isInitial()) {
                subKeys.put(node.itemGuid, node);
            }
        }

        public Node findSubNode(TechKey techKey) {
            Node result = (Node) subKeys.get(techKey);

            if (result == null) {
                int size = subNodes.size();
                for (int i = 0; i < size; i++) {
                    result = ((Node) subNodes.get(i)).findSubNode(techKey);
                    if (result != null) {
                        break;
                    }
                }
            }
            return result;
        }

        public boolean isParent(ItemSalesDoc item) {
            return (itemGuid.equals(item.getParentId()));
        }

        /*
                public boolean isLeaf() {
                    return subNodes.size() == 0;
                }
        
                public boolean isRoot() {
                    return parent == null;
                }
        
                public Iterator getSubNodeIterator() {
                    return subNodes.iterator();
                }
        */
                public Node[] getSubNodes() {
                    int size = subNodes.size();
                    Node[] retVal = new Node[size];
                    subNodes.toArray(retVal);
                    return retVal;
                }
        
        public Node getSubNode(int i) {
            return (Node) subNodes.get(i);
        }
        /*
                public ItemSalesDoc[] getSubItems() {
                    int size = subNodes.size();
                    ItemSalesDoc[] retVal = new ItemSalesDoc[size];
        
                    for (int i = 0; i < size; i++) {
                        retVal[i] = ((Node) subNodes.get(i)).referredItem;
                    }
        
                    return retVal;
                }
        */
        public Node getParent() {
            return parent;
        }

        public int getNumSubNodes() {
            return subNodes.size();
        }

        public boolean hasSubNodes() {
            return subNodes.size() > 0;
        }
    }

    /*
        public static class SubItemIterator {
    
            private int pos;
            private int size;
            private Node node;
    
            private SubItemIterator(Node node) {
                this.node = node;
                pos = 0;
                size = node.getNumSubNodes();
            }
    
            public boolean hasNext() {
                return (pos < size);
            }
    
            public ItemSalesDoc next() {
                if (pos < size) {
                    return node.getSubNode(pos++).referredItem;
                }
                else {
                    return null;
                }
            }
    
            public boolean isLast() {
                return (pos == size - 1);
            }
        }
    */

    /**
     * Creates a new object using a list of items. The created object represents
     * the hierarchy of the items.
     *
     * @param itemList the list of items used to construct the internal
     *        hierarchy
     */
    public ItemHierarchy(ItemList itemList) {
        Iterator it = itemList.iterator();
        Node lastNode = rootNode;
        Node subNode = null;

        while (it.hasNext()) {
            ItemSalesDoc item = (ItemSalesDoc) it.next();

            if (item.getParentId() == null ||  item.getParentId().isInitial()) { // main position
                lastNode = new Node(rootNode, item);
                rootNode.addNode(lastNode);
            } else { // sub position

                while (lastNode != null && !lastNode.isParent(item)) {
                    lastNode = lastNode.getParent();
                }
                if (lastNode != null) {
                    subNode = new Node(lastNode, item);
                    lastNode.addNode(subNode);
                    lastNode = subNode;
                } else {
                    // Maybe the subpositions are not defined subsequently
                    // try to find the parent
                    lastNode = getNode(item.getParentId());
                    if (lastNode != null) {
                        subNode = new Node(lastNode, item);
                        lastNode.addNode(subNode);
                        lastNode = subNode;
                    } else {
                        // tree inconsistant
                        System.out.println("!!! Tree Inconsistant !!!");
                        break;
                    }
                }
            }
        }
    }
    
    /**
     * Find the node for the given TechKey
     *
     * @param TechKey the key of the node to search for
     * @return the node for the given TechKey or null if not found
     */
    protected Node getNode(TechKey itemKey) {
        //  Try the cache
        Node found = (Node) nodeCache.get(itemKey);

        if (found == null) {
            found = rootNode.findSubNode(itemKey);
            nodeCache.put(itemKey, found);
        }

        return found;
    }

    /*
        public boolean hasSubItems(ItemSalesDoc item) {
            TechKey techKey = item.getTechKey();
    
            if (techKey == null) {
                return false;
            }
            else {
                Node subNode = (Node) techkeys.get(techKey);
    
                if (subNode != null) {
                    return subNode.hasSubNodes();
                }
                else {
                    return false;
                }
            }
        }
    */
    /**
     * Determines wether or not the given item is a subitem.
     *
     * @param item The item to answer the question for
     * @return <code>true</code> if the given item is not a top level
     *         item but an subitem; otherwise <code>false</code> is returned.
     */
    public boolean isSubItem(ItemSalesDoc item) {

        Node found;
        TechKey techkey = item.getTechKey();

        // Try the cache
        found = (Node) nodeCache.get(techkey);

        if (found == null) {
            found = rootNode.findSubNode(techkey);
            nodeCache.put(techkey, found);
        }

        if (found != null) {
            return found.parent != rootNode;
        } else {
            return false;
        }
    }
    /**
     * Determines wether or not the given item has any subitems.
     *
     * @param item The item to answer the question for
     * @return <code>true</code> if the given item has subitems;
     *         otherwise <code>false</code> is returned.
     */
    public boolean hasSubItems(ItemSalesDoc item) {
        Node found;
        TechKey techkey = item.getTechKey();

        // Try the cache
        found = (Node) nodeCache.get(techkey);

        if (found == null) {
            found = rootNode.findSubNode(techkey);
            nodeCache.put(techkey, found);
        }

        if (found != null) {
            return found.hasSubNodes();
        } else {
            return false;
        }
    }
    
    /**
     * Returns the first subitem or null if not present.
     *
     * @param item The item to answer the question for
     * @return item the first subitem if any exists
     *         null if no subitem exists
     */
    public ItemSalesDoc getFirstSubItem(ItemSalesDoc item) {
        
        Node found;
        TechKey techkey = item.getTechKey();

        // Try the cache
        found = (Node) nodeCache.get(techkey);

        if (found == null) {
            found = rootNode.findSubNode(techkey);
            nodeCache.put(techkey, found);
        }

        if (found != null && found.hasSubNodes()) {
            return found.getSubNode(0).referredItem;
        }
        else {
            return null;
        }
    }

    /**
     * Determines wether or not the given item has subitems of item usage ATP.
     *
     * @param item The item to answer the question for
     * @return <code>true</code> if the given item is a top level
     *         item an has subitem of item usage ATP; otherwise <code>false</code> is returned.
     */
    public boolean hasATPSubItem(ItemSalesDoc item) {
        
        ItemSalesDoc subItem = getFirstSubItem(item);

        return (subItem != null) ? subItem.isItemUsageATP() : false;
    }

	/**
	 * Checks if an item has a direct sub item which comes from 
	 * an exclusive free good condition.
	 * @param item the main item to check
	 * @return is there any free good related sub item (type exclusive?)
	 */    
    public boolean hasExclusiveFreeGoodSubitem(ItemSalesDoc item){
		ItemList subItems = getFirstLevelSubItems(item);
		
		for (int i = 0;i<subItems.size();i++){
			ItemSalesDoc currentSubItem = subItems.get(i);
			if (currentSubItem.getItmUsage() != null && currentSubItem.getItmUsage().equals(ItemSalesDoc.ITEM_USAGE_FREE_GOOD_EXCL)){
				return true;
			}
		}
    	
    	return false;
    }
    /**
     * Determines wether or not the given item has subitems of item usage ATP and
     * if at least one of those subitems has a differnt product than the main item.
     *
     * @param item The item to answer the question for
     * @return <code>true</code> if the given item is a top level
     *         item an has subitem of item usage ATP witha differing product; otherwise <code>false</code> is returned.
     */
    public boolean hasDifferingProductATPSubItems(ItemSalesDoc item) {
        
        boolean hasDiffATPSubItems = false;
        
        ItemList subItems = getFirstLevelSubItems(item);
        ItemSalesDoc subItem = null;
        
        for (int i=0; i < subItems.size(); i++) {
            subItem = subItems.get(i);
            if (subItem.isItemUsageATP()) {
                if (!subItem.getProductId().getIdAsString().equals(item.getProductId().getIdAsString())) {
                    hasDiffATPSubItems = true;
                    break;
                }
            }
            else {
                break;
            }
        }

        return hasDiffATPSubItems;
    }
      
    /**
     * Determines wether or not the given item has subitems of item usage kit.
     *
     * @param item The item to answer the question for
     * @return <code>true</code> if the given item is a top level
     *         item an has subitem of item usage Kit; otherwise <code>false</code> is returned.
     */
    public boolean hasKitSubItem(ItemSalesDoc item) {
        
        ItemSalesDoc subItem = getFirstSubItem(item);

        return (subItem != null) ? subItem.isItemUsageKit() : false;
    }
    
    /**
     * Determines wether or not the given item has subitems of item usage 1 to n substitution.
     *
     * @param item The item to answer the question for
     * @return <code>true</code> if the given item is a top level
     *         item an has subitem of item usage 1 to n substitution; otherwise <code>false</code> is returned.
     */
    public boolean has1ToNSubstSubItem(ItemSalesDoc item) {
        
        ItemSalesDoc subItem = getFirstSubItem(item);
        
        return (subItem != null) ? subItem.isItemUsage1ToNSubst() : false;
    }
    
    /**
     * Determines wether or not the given item has subitems of item usage BOM.
     *
     * @param item The item to answer the question for
     * @return <code>true</code> if the given item is a top level
     *         item an has subitem of item usage BOM; otherwise <code>false</code> is returned.
     */
    public boolean hasBOMSubItem(ItemSalesDoc item) {
        
        ItemSalesDoc subItem = getFirstSubItem(item);

        return (subItem != null) ? subItem.isItemUsageBOM() : false;
    }

	/**
	 * Determines wether or not the given item has subitems of item usage solution configuration.
	 *
	 * @param item The item to answer the question for
	 * @return <code>true</code> if the given item is a top level
	 *         item an has subitem of item usage SC; otherwise <code>false</code> is returned.
	 */
	public boolean hasSCSubItem(ItemSalesDoc item) {
        
		ItemSalesDoc subItem = getFirstSubItem(item);
				
		if (subItem != null && (subItem.isItemUsageScDependentComponent() || 
		    subItem.isItemUsageScRatePlanCombinationComponent() ||
		    subItem.isItemUsageScSalesComponent() )) {
		    return true;
		} else {
			return false;
		}

	}
    /**
     * Determines wether or not the given item is the last item of a
     * a list of subitems.
     *
     * @param item The item to answer the question for
     * @return <code>true</code> if the given item is the last subitem;
     *         otherwise <code>false</code> is returned.
     */
    public boolean isLastSubItem(ItemSalesDoc item) {
        Node found;
        TechKey techkey = item.getTechKey();

        // Try the cache
        found = (Node) nodeCache.get(techkey);

        if (found == null) {
            found = rootNode.findSubNode(techkey);
            nodeCache.put(techkey, found);
        }

        Node parent = found.parent;

        return found == parent.getSubNode(parent.getNumSubNodes() - 1);
    }

    /*
        public ItemSalesDoc[] getAllSubItems(ItemSalesDoc item) {
            TechKey techKey = item.getTechKey();
    
            if (techKey == null) {
                return new ItemSalesDoc[] {};
            }
    
            Node itemNode = rootNode.findSubNode(techKey);
    
            return itemNode.getSubItems();
        }
    
        public SubItemIterator getSubItemIterator(ItemSalesDoc item) {
            TechKey techKey = item.getTechKey();
    
            if (techKey == null) {
                return null;
            }
    
            return new SubItemIterator(rootNode.findSubNode(techKey));
        }
    */

    /**
     * Determines the hierarchy level of a given item.
     *
     * @param item The item to answer the question for
     * @return <code>level</code> if the given item is in the hierarchy;
     *          otherwise <code>0</code> is returned.
     */

    public int getLevel(ItemSalesDoc item) {
        Node found;
        TechKey techkey = item.getTechKey();

        // Try the cache
        found = (Node) nodeCache.get(techkey);

        if (found == null) {
            found = rootNode.findSubNode(techkey);
            nodeCache.put(techkey, found);
        }

        if (found != null) {
            return found.getLevel();
        } else {
            return 0;
        }
    }

    /**
     * Determines whether the root node of a given item is conf'able.
     *
     * @param item The item to answer the question for
     * @return <code>true</code> if the root item of a given item is conf'able;
     *          otherwise <code>false</code> is returned.
     */

    public boolean isRootConfigurable(ItemSalesDoc item) {
        return isRootConfigurable((ItemData) item);
    }
    
	/**
	 * Determines whether the root node of a given item is conf'able.
	 *
	 * @param item The item to answer the question for
	 * @return <code>true</code> if the root item of a given item is conf'able;
	 *          otherwise <code>false</code> is returned.
	 */

	public boolean isRootConfigurable(ItemData item) {
		Node found;
		TechKey techkey = item.getTechKey();

		// Try the cache
		found = (Node) nodeCache.get(techkey);

		if (found == null) {
			found = rootNode.findSubNode(techkey);
			nodeCache.put(techkey, found);
		}

		if (found != null) {
			return found.isRootConfigurable();
		} else {
			return false;
		}
	}
	
	/**
	 * Determines whether the porent node of a given item is conf'able.
	 *
	 * @param item The item to answer the question for
	 * @return <code>true</code> if the parent item of a given item is conf'able;
	 *          otherwise <code>false</code> is returned.
	 */

	public boolean isParentConfigurable(ItemData item) {
		Node found;
		TechKey techkey = item.getTechKey();

		// Try the cache
		found = (Node) nodeCache.get(techkey);

		if (found == null) {
			found = rootNode.findSubNode(techkey);
			nodeCache.put(techkey, found);
		}

		if (found != null) {
			return found.isParentConfigurable();
		} 
		else {
			return false;
		}
	}
    
    /**
     * Returns the number of subItems or null if no subitems are present
     *
     * @param item The item to answer the question for
     * @return number of subitems for the given item
     *         or 0 if no subitems are present.
     */
    public int getNoOfSubItems(ItemSalesDoc item) {
        Node found;
        TechKey techkey = item.getTechKey();

        // Try the cache
        found = (Node) nodeCache.get(techkey);

        if (found == null) {
            found = rootNode.findSubNode(techkey);
            nodeCache.put(techkey, found);
        }

        return found.getNumSubNodes();
    }
    
    /**
     * Returns the number of subItems or null if no subitems are present
     *
     * @param item The item to answer the question for
     * @return number of subitems for the given item
     *         or 0 if no subitems are present.
     */
    public ItemList getFirstLevelSubItems(ItemSalesDoc item) {
        
        ItemList subItems = new ItemList();
        
        Node found;
        TechKey techkey = item.getTechKey();

        // Try the cache
        found = (Node) nodeCache.get(techkey);

        if (found == null) {
            found = rootNode.findSubNode(techkey);
            nodeCache.put(techkey, found);
        }
        
		for (int i = 0; i < getNoOfSubItems(item); i++) {
	        subItems.add(found.getSubNode(i).referredItem);  
        }

        return subItems;
    }
    
	/**
	 * Returns all subItems or null if no subitems are present
	 *
	 * @param item The item to answer the question for
	 * @return list of subitems for the given item
	 *         or 0 if no subitems are present.
	 */
	public ItemList getAllLevelSubItems(ItemSalesDoc item) {
        
		ItemList subItems = new ItemList();
        
		Node found;
		TechKey techkey = item.getTechKey();

		// Try the cache
		found = (Node) nodeCache.get(techkey);

		if (found == null) {
			found = rootNode.findSubNode(techkey);
			nodeCache.put(techkey, found);
		}
        
		addSubItems(found, subItems);  

		return subItems;
	}
	
	/**
	 * Add subitesm to given list, if there are any
	 * 
	 * @param node the node to look for subitems
	 * @return subitems the changed subuitem list
	 */
	protected void addSubItems(Node node, ItemList subItems) {
		if (node.hasSubNodes()) { 
			for (int i=0; i < node.getNumSubNodes(); i++) {
				subItems.add(node.getSubNode(i).referredItem);
				addSubItems(node.getSubNode(i), subItems);
			}
		}
	}

	/**
	 * Returns the position of the Main item relative to the 
	 * item heirarchy maintained
	 * 
	 * @param mainItem  Only the mainItem or so called first Level item 
	 * 					of the item Hierarchy 
	 * @return  		the position of the main item is returned
	 */
	protected int getMainItemPosition(ItemSalesDoc mainItem) {
		
		int result = 0;
		TechKey techKey = mainItem.getTechKey();
		
		Node[] allNodes = rootNode.getSubNodes();
		
		for (int i=0; i < allNodes.length;i++){
			ItemSalesDoc currentItem = allNodes[i].referredItem;
			
			if (techKey.equals(currentItem.getTechKey())) {
				result = i;
				break;	
			}
		}
		return result; 
	}
	
	/**
	 * Returns the Next or Previous first level  Main Item from the Item Hierarchy tree
	 * 
	 * @param mainItem    Only the mainItem or so called first Level item 
	 * 					   of the item Hierarchy 
	 * @param direction	  <code>0</code> then PREVIOUS Main Item is returned
	 * 					  <code>1</code> then NEXT Main Item is returned
	 * @param gridProduct <code>true</code> then PREV or NEXT Main GRID Item is returned
	 * 					  <code>false</code> then PREV or NEXT Main Item is returned
	 * 
	 * @return	Next or Previous Main Item from the Item Hierarchy tree is returned
	 */
	protected ItemSalesDoc getPrevOrNextFirstLevelMainItem(ItemSalesDoc mainItem,
												  int direction,
												  boolean gridProduct){
		ItemSalesDoc result = null;
		
		Node[] allNodes = rootNode.getSubNodes();
		int noOfNodes = allNodes.length;
		
		int currentPosition = getMainItemPosition(mainItem);
		
		//If PREVIOUS Main Item is required
		if (direction == PREVIOUS){
			if (currentPosition == 0){
				return null;
			}
			for(int i=currentPosition-1; i>=0 ;i--){
				ItemSalesDoc prevItem = allNodes[i].referredItem;
				if (gridProduct){
					if (prevItem.isConfigurable() && 
						 prevItem.getConfigType().equals(ItemSalesDoc.ITEM_CONFIGTYPE_GRID)){
						result = prevItem;
						break;
					}
				}
				else{
					break;
				}
			}		
				
		}
		//If NEXT Main Item is required
		if (direction == NEXT){
			if (currentPosition == allNodes.length-1){
				return null;
			}
			for(int i=currentPosition+1; i<allNodes.length;i++){
				ItemSalesDoc nextItem = allNodes[i].referredItem;
				if (gridProduct){
					if (nextItem.isConfigurable() && 
						 nextItem.getConfigType().equals(ItemSalesDoc.ITEM_CONFIGTYPE_GRID)){
						result = nextItem;
						break;
					}
				}
				else{
					break;
				}
			}				
		}
		return result;										  	
	}
	
	/**
	 * Returns the Next first level Main Item from the Item Hierarchy tree 
	 * @param mainItem 		Only the mainItem or so called first Level item 
	 * 						of the item Hierarchy
	 * @param gridProduct	<code>true</code> then NEXT Main GRID Item is returned
	 * 						<code>false</code> then NEXT Main Item is returned
	 * 
	 * @return  the Next first level  Main Grid Item if found, else Null
	 */
	public ItemSalesDoc getNextFirstLevelMainItem (ItemSalesDoc mainItem,
													boolean gridProduct) {
		return getPrevOrNextFirstLevelMainItem(mainItem, NEXT, gridProduct);
	}
	/**
	 * Returns the Previous first level Main Item from the Item Hierarchy tree 
	 * @param mainItem 		Only the mainItem or so called first Level item 
	 * 						of the item Hierarchy
	 * @param gridProduct	<code>true</code> then PREVIOUS Main GRID Item is returned
	 * 						<code>false</code> then PREVIOUS Main non-grid Item is returned 
	 * 
	 * @return  the Previous first level main item if found, else Null
	 */
	public ItemSalesDoc getPreviousFirstLevelMainItem (ItemSalesDoc mainItem,
														boolean gridProduct) {						
		return getPrevOrNextFirstLevelMainItem(mainItem, PREVIOUS, gridProduct);
	}
}
