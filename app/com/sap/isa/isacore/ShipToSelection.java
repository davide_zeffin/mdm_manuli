/*****************************************************************************
    Class         ShipToSelection
    Copyright (c) 2002, SAP AG, Germany, All rights reserved.
    Author:       SAP
    Created:      November 2002
    Version:      1.0

    $Revision: #2 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.isacore;

import java.util.List;

import com.sap.isa.businessobject.Address;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.ShipTo;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.SoldFrom;
import com.sap.isa.core.TechKey;


/**
 * The class allowed to regard a sales partner by the ShipTo selection.<br>
 * The <code>ShipToSelection</code> will be create with list of possible shiptos
 * and optional with a sales partner.<br>
 * The class offers also a static method to add the sales partner into the list,
 * if the sales partner address doesn't exist in the list.
 *
 * @author SAP
 * @version 1.0
 *
 */
public class ShipToSelection {

    private static final int NO_PARTNER_FOUND_INDEX = -1;

    /* list of ship to's */
    private List shipToList;

    private BusinessPartner salesPartner;

    private String shippingRecipient = SoldFrom.SHIPTO_SOLDTO;
    
    private TechKey actualShipToKey;
    
    /**
     * Adds the sales partner to the sales document.<br>
     *
     * @param salesDocument document to add the ship to
     * @param salesPartner partner to add as ship to
     *
     */
    public static void addPartnerToShipTos(SalesDocument salesDocument,
                                          BusinessPartner salesPartner)
            throws CommunicationException {

        Address address = salesPartner.getAddress();
        //ShipTo shipTo = (ShipTo)salesDocument.createShipTo();
        ShipTo shipTo = new ShipTo();
        
        shipTo.setAddress(address);
        shipTo.setId(salesPartner.getId());
        shipTo.setManualAddress();

        salesDocument.addNewShipTo(shipTo);
    }


    /**
     * Returns if the given salesPartner is assigned as shipTo the the give shipTo list
     *
     */
    public static boolean isPartnerShipTo(List shipToList, BusinessPartner salesPartner) {

        int i = getPartnerShipToIndex(shipToList, salesPartner);

        return (i != NO_PARTNER_FOUND_INDEX);

    }


    /**
     * Constructor to create the selection without Sales Partner List
     *
     * @param shipTos array with ship tos
     **/
    public ShipToSelection(List shipToList) {
        this.shipToList = shipToList;
    }


    /**
     * Constructor to create the Selcteion without Sales Partner List
     *
     * @param shipTos array with ship tos
     * @param salesPartner sales partner
     */
    public ShipToSelection(List shipToList, BusinessPartner salesPartner) {
        this.shipToList = shipToList;
        this.salesPartner = salesPartner;
    }



    /**
     * Returns the partnerAvailable.
     * @return boolean
     */
    public boolean isPartnerAvailable() {

        int i = getPartnerShipToIndex(shipToList, salesPartner);

        return (i != NO_PARTNER_FOUND_INDEX);
    }


    /**
     * Returns the actualShipToKey<br>
     * This key marks the shipto, which is selected in the list of shiptos without 
     * the sales partner.
     * 
     * @return TechKey
     */
    public TechKey getActualShipToKey() {
        return actualShipToKey;
    }

    /**
     * Sets the actualShipToKey see {@link #getActualShipToKey}.
     * 
     * @param actualShipToKey The actualShipToKey to set
     */
    public void setActualShipToKey(TechKey actualShipToKey) {
        this.actualShipToKey = actualShipToKey;
    }


    /**
     * Returns the actualShipToKey<br>
     * This key marks the shipto, which is selected in the list of shiptos without 
     * the sales partner.
     * 
     * @return TechKey
     */
    public ShipTo getActualShipTo() {
        return getShipTo(actualShipToKey);
    }


    /**
     * Returns the salesPartner.
     * @return BusinessPartner
     */
    public BusinessPartner getSalesPartner() {
        return salesPartner;
    }

    /**
     * Sets the salesPartner.
     * @param salesPartner The salesPartner to set
     */
    public void setSalesPartner(BusinessPartner salesPartner) {
        this.salesPartner = salesPartner;
    }


    /**
     * Return the complete array of shipTos
     */
    public ShipTo[] getShipTos() {

        ShipTo[] shipTos = new ShipTo[shipToList.size()];
        shipToList.toArray(shipTos);
        return shipTos;

    }


    /**
     * Return the array of shipTos without the sales partner
     *
     */
    public ShipTo[] getShipTosWithoutPartner() {

        ShipTo[] shipTos = new ShipTo[shipToList.size()];

        return shipTos;
    }


    /**
     * Returns the shipto which is assigned to the sales partner
     *
     * @return partner as shipto
     */

    public ShipTo getPartnerShipTo() {

        ShipTo partnerShipTo = null;

        int i = getPartnerShipToIndex(shipToList, salesPartner);

        if(i != NO_PARTNER_FOUND_INDEX) {
            partnerShipTo = (ShipTo)shipToList.get(i);
        }
        return partnerShipTo;
    }


    /**
     * Returns the shipto which is assigned to the given shipToKey
     *
     * @param shipToKey key of the shipTo
     * @return partner as shipto
     */

    public ShipTo getShipTo(TechKey shipToKey) {

		for (int i = 0; i < shipToList.size(); i++) {        
        	ShipTo shipTo = (ShipTo)shipToList.get(i); 
			if (shipTo.getTechKey().equals(shipToKey)) {
			    return shipTo;         	
			}        	    
        }	
        
        return null;
    }


	/**
	 * Checks if the given shipTo is the sales partner shipto entry.
     * 
	 * @param shipTo shipTo to test
	 * @return boolean flag if the given shipTo is the sales partner;
	 */
    public boolean isPartnerShipTo(ShipTo shipTo) {

        ShipTo partnerShipTo = getPartnerShipTo();
        if (partnerShipTo != null && shipTo != null
            && partnerShipTo.getTechKey().equals(shipTo.getTechKey())) {       
            return true;                
        }        
        
        return false;        
    }    


    /*
     * Returns the index of the shipto which is assigned to the sales partner
     *
     * @return index of shipTo
     */

    private static int getPartnerShipToIndex(List shipToList, BusinessPartner salesPartner) {

        if(salesPartner != null) {
            for(int i = 0; i < shipToList.size(); i++) {

                if (isPartnerShipTo((ShipTo)shipToList.get(i), salesPartner)) {
                    return i;
                }
            }
        }
        return NO_PARTNER_FOUND_INDEX;
    }



    private static boolean isPartnerShipTo (ShipTo shipTo, BusinessPartner salesPartner) {

        String id = shipTo.getId();
        if(id != null && id.equals(salesPartner.getId())) {
            return true;
        }

        return false;
    }


	/**
	 * Returns the possible shipping recipient(s).
     * 
	 * @return String 
     * <ul>
     *   <li>{@link com.sap.isa.businesspartner.businessobject.SoldFrom#SHIPTO_SOLDFROM}</li>          
     *   <li>{@link com.sap.isa.businesspartner.businessobject.SoldFrom#SHIPTO_SOLDFROM_OR_SOLDTO}</li>          
     *   <li>{@link com.sap.isa.businesspartner.businessobject.SoldFrom#SHIPTO_SOLDTO}</li>          
     * </ul>
     * 
     * @see com.sap.isa.businesspartner.businessobject.SoldFrom#getShipRecipient
	 */
	public String getShippingRecipient() {
		return shippingRecipient;
	}


	/**
     * Sets the possible shipping recipient(s).
     * 
	 * @param shippingRecipient_ The shipping recipient(s) to set. Allowed values are
     * <ul>
     *   <li>{@link com.sap.isa.businesspartner.businessobject.SoldFrom#SHIPTO_SOLDFROM}</li>          
     *   <li>{@link com.sap.isa.businesspartner.businessobject.SoldFrom#SHIPTO_SOLDFROM_OR_SOLDTO}</li>          
     *   <li>{@link com.sap.isa.businesspartner.businessobject.SoldFrom#SHIPTO_SOLDTO}</li>          
     * </ul>
     * 	 
     */
	public void setShippingRecipient(String shippingRecipient) {
		this.shippingRecipient = shippingRecipient;
	}



}
