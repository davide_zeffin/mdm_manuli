/*****************************************************************************
    Class:        AddressConstants
    Copyright (c) 2006, SAP AG, Germany, All rights reserved.
    Created:      June 2006
    Version:      1.0

    $Revision: #2 $
    $Date: 2001/07/03 $
*****************************************************************************/

package com.sap.isa.isacore;

/**
 * this class defines some constants for request attributes (RA) which are used
 * in actions to store and retrieve data from request context.
 */
public interface AddressConstants {

  
  public final static String RC_MESSAGES             = "messages";

  public final static String RC_ADDRESS_FORMULAR     = "addressFormular";

  public final static String RC_ADDRESS              = "address";

  /**
   * Name of the countryList attribute stored in the request context.
   */
  public static final String RC_COUNTRY_LIST = "countryList";

  /**
   * Name of the regionList attribute stored in the request context.
   */
  public static final String RC_REGION_LIST          = "regionList";

  /**
   * Name of the regionList attribute stored in the request context.
   */
  public static final String RC_TAXCODE_LIST         = "taxCodeList";

  /**
   * Name of the titleList attribute stored in the request context.
   */
  public static final String RC_TITLE_LIST           = "titleList";

  /**
   * Name of the forms of address attribute stored in the request context.
   */
  public static final String RC_FORMS_OF_ADDRESS     = "formsofaddress";

  /**
   * Name of the forms of address attribute stored in the request context.
   */
  public static final String RC_POSSIBLE_COUNTIES    = "possiblecounties";


}