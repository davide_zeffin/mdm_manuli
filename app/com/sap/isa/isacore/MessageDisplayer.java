/*****************************************************************************
    Class:        MessageDisplayer
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      July 2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/08/01 $
*****************************************************************************/

package com.sap.isa.isacore;

import javax.servlet.http.HttpServletRequest;

import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.core.util.MessageListDisplayer;


/**
 * The MessageDisplayer class is a helper class to display a message or a list
 * of messages with the corresponding message.jsp.
 *
 * @author  Wolfgang Sattler
 * @version 1.0
 *
 * @see com.sap.isa.core.util.Message
 * @see com.sap.isa.core.util.MessageList
 * @see com.sap.isa.core.util.MessageListHolder
 *
 */
public class MessageDisplayer extends MessageListDisplayer {
	
    /**
     * Name to stored a BusinessObject in the request context.
     */
    // public final static String CONTEXT_NAME = "DisplayMessage.isacore.isa.sap.com" ;

	private boolean logoffWithMessage  = false;

    /**
     * standard constructor
     */
    public MessageDisplayer() {
    }


    /**
     * Constructor for MessageDisplayer
     *
     * @param forward logical forward which is used if you leave the screen.
     *                if an empty forward is definied a back-button will be offered.
     * @param loginAvailable if <code>true</code> a link to the login will be display
     *
     */
    public MessageDisplayer(String forward, boolean loginAvailable) {
        super( forward, loginAvailable);
        }
    /**
     * Return the property logoffWithMessage. <br>
     * 
     * @return logoffWithMessage
     */
    public boolean isLogoffWithMessage() {
        return logoffWithMessage;
    }

    /**
     * Set the property logoffWithMessage. <br>
     * 
     * @param logoffWithMessage
     */
    public void setLogoffWithMessage(boolean logoffWithMessage) {
        this.logoffWithMessage = logoffWithMessage;
    }


    /**
     * set the object and an BusinessObjectBaseObject in the request context
     *
     * @param request
     * @param bob a reference to an Business Object (for error page)
     *
     */
    public void addToRequest(HttpServletRequest request,BusinessObjectBase bob) {
        request.setAttribute(CONTEXT_NAME,this);
        request.setAttribute(BusinessObjectBase.CONTEXT_NAME,bob);
    }
}




