/*
 * 
 */
package com.sap.isa.isacore;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.naming.NamingException;
import javax.sql.DataSource;

import com.sap.isa.core.db.DBHelper;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.services.schedulerservice.TypedTask;
import com.sap.isa.services.schedulerservice.context.JobContext;
import com.sap.isa.services.schedulerservice.exceptions.JobExecutionException;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;

/**
 * Task to delete the business event content after the content is uploaded into the 
 * business information warehouse(BIW).
 * Note this task does not ensure the success of upload into the BW.
 * Configure this task only when you are confident that the upload will be complete at
 * the execution time of the task.
 * Two alternatives are provided to cleanup the data:
 * Deletion based on days (Data older than specified days)
 * Deletion based on date and time (Data older than specified date and time).
 * Deletion based on days will have precedence over the other option 
 */
public class BusinessEventDBCleanupTask implements TypedTask {
	private static Map parameterTypeMap = null;
	
	private static Map parameterDescMap = null;
	
	private static ArrayList props;
	private static String TO_DATE = "TODATE" ;
	private static String TO_TIME = "TOTIME" ;
	private static String OLDER_THAN_DAYS = "OLDERTHANDAYS" ;
	private static long DAYINMILLISECS = 24L*60L*60L*1000L;
	
	protected static final Category logger = LogUtil.APPS_BUSINESS_LOGIC;
	protected static Location tracer =
			Location.getLocation(BusinessEventDBCleanupTask.class); 
	static
	{
	  props = new ArrayList();
	  props.add(OLDER_THAN_DAYS);
	  props.add(TO_DATE);
	  props.add(TO_TIME);
	  
	}

	/* 
	 * @see com.sapmarkets.isa.services.schedulerservice.TypedTask#getParameterTypes()
	 */
	public Map getParameterTypes() {
		synchronized(this.getClass()){
		 if(parameterTypeMap==null) {
			parameterTypeMap = new HashMap();
			parameterTypeMap.put(TO_DATE,String.class);
			parameterTypeMap.put(TO_TIME,String.class);
			parameterTypeMap.put(OLDER_THAN_DAYS,Integer.class);
			
		 }
		}
		return parameterTypeMap;
	}
	
	/* 
	 * @see com.sapmarkets.isa.services.schedulerservice.TypedTask#getParameterDescriptions()
	 */
	public Map getParameterDescriptions(Locale locale) {
		synchronized( this.getClass() ){
		 if(parameterDescMap==null) {
			parameterDescMap = new HashMap();
			parameterDescMap.put(OLDER_THAN_DAYS, "Enter the number of day(s). Records older than the specified date will be deleted. This parameter takes precedence when the date and time are specified. Parameter is ignored if it is less than or equal to 0.");
			
			parameterDescMap.put(TO_DATE, "Enter the date YYYYMMDD (All business event data before this date will be deleted)");
			parameterDescMap.put(TO_TIME, "Enter the time HHMMSS(All business event data before this time will be deleted).");
			
		 }
		}
		return parameterDescMap;
	}


	/* 
	 * Deletes the business events from the table CRM_ISA_WEC_EVENT based on the conditions passed
	 * If old_than_days param is specified, all the data older than the specified days will be deleted
	 * Fractional days Eg: 0.5 etc are also supported
	 * If date and(or) time is specified, data older than the specified date and time will be deleted.
	 *  
	 * @see com.sap.isa.services.schedulerservice.Task#run(com.sap.isa.services.schedulerservice.context.JobContext)
	 */
	public void run(JobContext jc) throws JobExecutionException {
		try {
			String date = jc.getJob().getJobPropertiesMap().getString(TO_DATE);
			String time = jc.getJob().getJobPropertiesMap().getString(TO_TIME);
			int numDays = jc.getJob().getJobPropertiesMap().getInt(OLDER_THAN_DAYS);
			int numConditions = 3; //number of conditions in the query
			
			Connection connection = null;
			
			boolean isSuccess  = false;
			String sql ;
			logger.infoT(tracer, "Running the BusinessEventDBCleanupTask");
			jc.getLogRecorder().addMessage("Executing the BusinessEventDBCleanupTask with params date:" + date 
						+ " time :"+ time + "numdays:" + numDays , true )	;				
			//sample: delete from crm_isa_wec_event where ( calday < '20050115' ) or  (calday = '20050115' and event_time < '091030')
			if (numDays > 0){ // ignore the date and time
				Calendar calendar = Calendar.getInstance();
				long currentTime = calendar.getTimeInMillis();
				calendar.setTimeInMillis(currentTime - ( numDays * DAYINMILLISECS ));
				
				//note 1267621
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");	
				date = dateFormat.format(calendar.getTime());  //format date
				
				dateFormat.applyLocalizedPattern("HHmmss");
				time = dateFormat.format(calendar.getTime());  //format time
				//end note

				sql = "DELETE FROM CRM_ISA_WEC_EVENT WHERE ( CALDAY < ? ) OR ( CALDAY = ?  AND EVENT_TIME < ? )";			
			}else{
				if (date == null ){
					throw new JobExecutionException(new Exception(" No date passed. Task aborted ")); 
				}else{
					date = date.trim();
					if (date.length() != 8 )
						throw new JobExecutionException(new Exception(" Date length incorrect. Task aborted "));
					if (time != null)
						time = time.trim();
						
					if ( (time != null) && (time.length() != 0) ) {
						if (time.length() != 6)
							throw new JobExecutionException(new Exception(" Time length incorrect. Task aborted "));
						//valid date and time
						sql = "DELETE FROM CRM_ISA_WEC_EVENT WHERE ( CALDAY < ? ) OR ( CALDAY = ?  AND EVENT_TIME < ? )";
					}else{ //valid date
						sql = "DELETE FROM CRM_ISA_WEC_EVENT WHERE ( CALDAY < ? )";
						numConditions = 1;
					}
					
				}
			}
			
			try {
				DataSource ds = DBHelper.getApplicationSpecificDataSource(null);
				connection = ds.getConnection();
				connection.setAutoCommit(false);
				PreparedStatement stmt = connection.prepareStatement(sql);										
				
				stmt.setString(1, date) ;  
				if(numConditions == 3){
					stmt.setString(2, date) ;
					stmt.setString(3, time);
				}
				
				logger.infoT(tracer, "Executing the query " + sql);	
				jc.getLogRecorder().addMessage("Executing the query " + sql, true);
				isSuccess = stmt.execute();
				connection.commit();
				jc.getLogRecorder().addMessage("Task finished successfully ", true);
			} catch (NamingException e) {
				jc.getLogRecorder().addMessage("Error occured, correct the configuration and run again :" + e, true);
				throw new JobExecutionException(e);
			} catch (SQLException e) {
				jc.getLogRecorder().addMessage("Error occured, correct the configuration and run again :" + e, true);
				throw new JobExecutionException(e);
			} 
			 finally{
				try {
					if ((connection != null ) && ( ! connection.isClosed()))
						connection.close();
				} catch (SQLException e1) {
                     jc.getLogRecorder().addMessage("Error occured :" + e1, true);
				}
			}
		} catch (JobExecutionException e) {
			jc.getLogRecorder().addMessage("Error occured, correct the configuration and run again :" + e, true);
			throw e;
		} catch (RuntimeException e){
			jc.getLogRecorder().addMessage("Error occured, correct the configuration and run again :" + e, true);
			throw new JobExecutionException(e);
		}

	}
	
	/* 
	 * @see com.sap.isa.services.schedulerservice.Task#getPropertyNames()
	 */
	public Collection getPropertyNames() {
		return props;
	}

}
