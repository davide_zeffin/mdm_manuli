/**
 *  Class:        DocumentHandler
 *  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
 *  Author:       SAP AG
 *  Created:      20.2.2001
 *  Version:      1.0
 *  $Revision: #6 $
 *  $Date: 2001/08/01 $
 */

package com.sap.isa.isacore;

import java.util.HashMap;
import java.util.Map;

import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;

/**
 *  The DocumentHandler stores the status of the GUI for the documents and the catalog.
 *  It knows if the catalog, a document or a default page must be displayed. It
 *  also knows, which documents are instantiated in the business object layer.<br>
 *  There is exactly one DocumentHandler object for every session. It is created in the
 *  InitB2BAction and stored in the session context.
 *  @see ManagedDocument
 *  @see History
 *  @see com.sap.isa.isacore.action.b2b.InitB2BAction
 *
 *@author    SAP AG
 *@created    26. November 2001
 *@version   1.0
 */
public class DocumentHandler {

    /**
     *  Reference to the History object.
     */
    protected History history = new History();

    /**
     *  Reference to the editable document.  <code> null </code>  if there is no editable document.
     */
    protected DocumentState edoc = null;
    // the editable document

    /**
     *  Reference to the viewable document.  <code> null </code>  if there is no viewable document.
     */
    protected DocumentState vdoc = null;
    // the viewable document

    /**
     *  Reference to the document, located on the top. If there is only document,
     *  then this document lies automatically on top. <code>null</code> if there is no document.
     */
    protected DocumentState onTop = null;
    // the topmost document

    /**
     *  Indicates if the catalog is displayed.<br>
     *  <code>true</code> the catalog is displayed.<br>
     *  <code>false</code> something else is displayed.
     */
    protected boolean catalogOnTop = false;

    /**
     *  If the catalog is displayed, then this string determines, which part of the catalog
     *  is displayed.
     */
    protected String catalogType = "entry";


    /**
     * Contain the logical key for the organizer, which is actual displayed.
     */
    protected String actualOrganizer = "docsearch";
    

	/**
	 * Contain the logical key for the organizer, which is actual displayed.
	 */
	protected String lastOrganizer = "docsearch";

    /**
     *  If no catalog and no document is displayed then this flag indicates if the
     *  welcome page is displayed or the shorter selection page.<br>
     *  <code>true</code> the welcome page is displayed.<br>
     *  <code>false</code> the selection page is displayed.
     */
    protected boolean welcome = true;

    /**
     * Stores a reference to a MessageDocument object.
     * S
     */
    protected MessageDocument messageDocument = null;
    
    /**
     * storing information displayed in minibasket
     */
    protected String miniBsktNetValue = "";
    protected String miniBsktDoctype="";
    protected String miniBsktItemSize="";
    protected String miniBsktCurrency="";



    protected static IsaLocation log;
    String logPrefix = null;
    boolean isDebugEnabled;

    /**
     * Parameter to be used by the organizer
     */
    protected Map organizerParameter = new HashMap();
    /**
     *  Creates a new instance of the DocumentHandler
     */
    public DocumentHandler() {
		log = IsaLocation.getInstance(this.getClass().getName());
		isDebugEnabled = log.isDebugEnabled();
    }
    /**
     *  Creates a new instance of the DocumentHandler
     * @param History set a history
     */
    public DocumentHandler(History history) {
        this.history = history;
    }
    /**
     * Returns the logical of the actual organizer page.
     * This logical key is also used as logical forwards in the 
     * OrganizerContent and OrganizerNavigator actions.
     * 
     * @return String
     * 
     * @see com.sap.isa.isacore.action.b2b.OrganizerContentAction
     * @see com.sap.isa.isacore.action.b2b.OrganizerNavigatorAction
     */
    public String getActualOrganizer() {
        return actualOrganizer;
    }

    /**
     * Sets the logical key of the actual Organizer.
     * @param actualOrganizer The logical key of the actual organizer to set
     */
    public void setActualOrganizer(String actualOrganizer) {
        this.actualOrganizer = actualOrganizer;
    }

    /**
     * Set a parameter for the organizer
     * @param String parameter Name
     * @param String parameter Value
     */
    public void setOrganizerParameter(String paramName, String paramValue) {
        this.organizerParameter.put(paramName, paramValue);
    }

    /**
     * Returns a Organizer Parameter
     * @param String Parameter name
     * @return String Parameter value
     */
    public String getOrganizerParameter(String paramName) {
        String retVal = null;
        if (organizerParameter.get(paramName) != null) {
            retVal = (String)organizerParameter.get(paramName);
        }
        return retVal;
    }

    /**
     *  Tells the DocumentHandler that the catalog lays on top of the workarea or not.
     *
     *	@param onTop <code>true</code> if the catalog should be on top of the working area,
     * <code>false</code> else.
     */
    public void setCatalogOnTop(boolean onTop) {
		final String METHOD = "setCatalogOnTop()";
		log.entering(METHOD);

        // write some debugging info
        if (log.isDebugEnabled()) {
            log.debug("setCatalogOnTop(): onTop = " + onTop);
        }

		if (onTop != catalogOnTop) {
			catalogOnTop = onTop;
			if (catalogOnTop) {
				lastOrganizer = actualOrganizer;
				actualOrganizer = "catalog";
			} 
			else {
				actualOrganizer = lastOrganizer;
			}
		}
        welcome = false;
        log.exiting();
    }


    /**
     *  Tells the DocumentHandler that the catalog lays on top of the workarea or not.
     *  And tells him, which part of the catalog is vsible now.
     *
     *@param  onTop <code>true</code> if the catalog should be on top of the working area,
     * <code>false</code> else.
     *@param  catalogType  Part of the catalog, which is visible.
     * See b2b/catalog/refresher.jsp for possible types.
     */
    public void setCatalogOnTop(boolean onTop, String catalogType) {
		final String METHOD = "setCatalogOnTop()";
		log.entering(METHOD);
        // write some debugging info
        if (log.isDebugEnabled()) {
            log.debug("setCatalogOnTop(): onTop = " + onTop + " ,catalogType = " + catalogType);
        }
		setCatalogOnTop(onTop);
		this.catalogType = catalogType;
        log.exiting();
    }


    /**
     *  Set the document on top. This means that the document lies now above the other
     *  document, in case that there is another. This affects not the display of the catalog.<br>
     *  A document can only be set on top, if it is already handled by the DocumentHandler.
     *  This means, a corresponding ManagedDocument has been already added.
     *
     *@param  rdoc  The document, which should be on top.
     *@return       <code>true</code>, if the document could be set on top,
     * <code>false</code> otherwise.
     */
    public boolean setOnTop(DocumentState rdoc) {
        boolean ret = false;
        // write some debugging info
        if (log.isDebugEnabled()) {
            log.debug("setOnTop()");
        }
        if (rdoc != null) {
            if (edoc != null && edoc instanceof ManagedDocument) {
                ManagedDocument mdoc = (ManagedDocument) edoc;
                if (rdoc == mdoc.getDocument()) {
                    onTop = edoc;
                    ret = true;
                    if (log.isDebugEnabled()) {
                        log.debug("setOnTop(): editable doc on top");
                    }
                }
            }
            if (vdoc != null && vdoc instanceof ManagedDocument) {
                ManagedDocument mdoc = (ManagedDocument) vdoc;
                if (rdoc == mdoc.getDocument()) {
                    onTop = vdoc;
                    ret = true;
                    if (log.isDebugEnabled()) {
                        log.debug("setOnTop(): viewable doc on top");
                    }
                }
            }
        }
        if (ret == false) {
            log.warn(LogUtil.APPS_USER_INTERFACE, "b2b.dochandler.warn.notontop");
        }

        return ret;
    }


    /**
     *  Get the editable or viewable document.
     *
     *@param  state  The state to be searched for, as described in the interface
     *  <code>DocumentState</code>.
     *
     *@return        The document with the desired state or <code>null</code> if
     * no matching document is found.
     */
    public DocumentState getDocument(int state) {
		final String METHOD = "getDocument()";
		log.entering(METHOD);
        // write some debugging info
        if (log.isDebugEnabled()) {
            log.debug("getDocument(): state=" + state);
        }

        if (edoc != null && edoc.getState() == state) {
            if (edoc instanceof ManagedDocument) {
                ManagedDocument mdoc = (ManagedDocument) edoc;
                if (log.isDebugEnabled()) {
                    log.debug("getDocument() returns the editable document");
                }
                log.exiting();
                return mdoc.getDocument();
            }
        }
        if (vdoc != null && vdoc.getState() == state) {
            if (vdoc instanceof ManagedDocument) {
                ManagedDocument mdoc = (ManagedDocument) vdoc;
                if (log.isDebugEnabled()) {
                    log.debug("getDocument() returns the viewable document");
                }
                log.exiting();
                return mdoc.getDocument();
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("getDocument() returns null");
        }
        log.exiting();
        return null;
    }


    /**
     *  Get the <code>ManagedDocument</code> of the editable or viewable document.
     *
     *@param  state  The state to be searched for, as described in the interface
     *  <code>DocumentState</code>.
     *
     *@return        The corresponding <code>ManagedDocument</code> of the document
     * with the desired state or <code>null</code> if no matching document is found.
     */
    public ManagedDocument getManagedDocument(int state) {
		final String METHOD = "getManagedDocument()";
		log.entering(METHOD);
        // write some debugging info
        if (log.isDebugEnabled()) {
            log.debug("getManagedDocument(): state=" + state);
        }

        if (edoc != null && edoc.getState() == state) {
            if (edoc instanceof ManagedDocument) {
                ManagedDocument mdoc = (ManagedDocument) edoc;
                if (log.isDebugEnabled()) {
                    log.debug("getManagedDocument() returns the ManagedDocument of the editable document");
                }
                log.exiting();
                return mdoc;
            }
        }
        if (vdoc != null && vdoc.getState() == state) {
            if (vdoc instanceof ManagedDocument) {
                ManagedDocument mdoc = (ManagedDocument) vdoc;
                if (log.isDebugEnabled()) {
                    log.debug("getManagedDocument() returns the ManagedDocument of the viewable document");
                }
                log.exiting();
                return mdoc;
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("getManagedDocument() returns null");
        }
        log.exiting();
        return null;
    }


    /**
     *  Returns the number of documents, which are currently handled (editable or viewable).
     *
     *@return    0, 1, or 2
     */
    public int getNumberOfDocuments() {
		final String METHOD = "getNumberOfDocuments()";
		log.entering(METHOD);
        // write some debugging info
  
        int ret = 0;
        if (edoc != null) {
            ret++;
        }
        if (vdoc != null) {
            ret++;
        }

        if (log.isDebugEnabled()) {
            log.debug("getNumberOfDocument() returns: " + ret);
        }
        log.exiting();
        return ret;
    }


    /**
     *  Returns the document, which is on top of the workarea
     *
     *@return    The document, or <code>null</code> if no document is handled.
     */
    public DocumentState getDocumentOnTop() {
		final String METHOD = "getDocumentOnTop()";
		log.entering(METHOD);

        if (onTop != null && onTop instanceof ManagedDocument) {
            ManagedDocument mdoc = (ManagedDocument) onTop;
            if (log.isDebugEnabled()) {
                log.debug("getDocumentOnTop() returns a document");
            }
            log.exiting();
            return mdoc.getDocument();
        }
        if (log.isDebugEnabled()) {
            log.debug("getDocumentOnTop() returns null");
        }
        log.exiting();
        return null;
    }


    /**
     *  Returns the <code>ManagedDocument</code> which corresponds to the document, which is on top of the workarea
     *
     *@return    The <code>ManagedDocument</code>, or <code>null</code>
     * if no document is handled.
     */
    public ManagedDocument getManagedDocumentOnTop() {
		final String METHOD = "getManagedDocumentOnTop()";
		log.entering(METHOD);
 
        if (onTop != null && onTop instanceof ManagedDocument) {
            ManagedDocument mdoc = (ManagedDocument) onTop;
            if (log.isDebugEnabled()) {
                log.debug("getManagedDocumentOnTop() returns a ManagedDocument");
            }
            log.exiting();
            return mdoc;
        }
        if (log.isDebugEnabled()) {
            log.debug("getManagedDocumentOnTop() returns null");
        }
        log.exiting();
        return null;
    }


    /**
     *  Returns if the catalog is located on top or not.
     *
     *
     *@return    <code>true</code> if the catalog is on top.<br>
     *           <code>false</code> else.
     */
    public boolean isCatalogOnTop() {
        if (log.isDebugEnabled()) {
            log.debug("isCatalogOnTop(): returns" + catalogOnTop);
        }
        return catalogOnTop;
    }


    /**
     *  Returns the part of the catalog, which is visible now. If the type of the
     *  catalog is not set, it returns an empty String. <code>null</code> is never
     *  returned.
     *
     *@return    The CatalogType value
     */
    public String getCatalogType() {
        if (catalogType == null) {
            return "";
        }
        else {
            return catalogType;
        }
    }


    /**
     *  Returns the reference on the history.
     *
     *@return    The History value
     */
    public History getHistory() {
        return history;
    }


    /**
     *  If the user has just logged in, a friendly welcome page is displayed.
     *  Later if the user has worked with some documents or the catalog only a
     *  selection page should be displayed, if nothing is on top.
     *
     *@return    <code>true</code> if welcome page should be displayed.<br>
     *           <code>false</code> else.
     */
    public boolean showWelcome() {
        return welcome;
    }


    /**
     *  If two documents are open, then one document is in the background, the
     *  other one in the foreground. This functions put the document from the
     *  background into the foreground and vice versa.
     *
     *@return    <code>true</code> if the documents could be switched,<br>
     *           <code>false</code> if the documents cold not be switched, only
     *           the case if there are less than two documents handled by the DocumentHandler.
     */
    public boolean switchDocumentsOnTop() {
        boolean ret = false;
        if (edoc != null && vdoc != null) {
            if (onTop == edoc) {
                onTop = vdoc;
            }
            else {
                onTop = edoc;
            }
            ret = true;
        }
        return ret;
    }


    /**
     *  Adds a ManagedDocument to the DocumentHandler and to the history.
     *  The ManagedDocument will only be shown on the tabs of the workarea
     *  if documentstate is TARGET_DOCUMENT or VIEW_DOCUMENT (see DocumentState)
     *  It will only be displayed in the history if doc.getForwardMapping() != null.
     *  With that you can manage, if the document will be shown only on the tabs or only in the history or in both.
     *  Look also at history.add() and ManagedDocument.
     *  If the DocumentHandler already has a reference to a document with the
     *  DocumentState TARGET_DOCUMENT, the old document will be substituted with the new one.
     *  This is also valid for VIEW_DOCUMENT.
     *
     *@param  doc the ManagedDocument, which should be displayed in the workarea or the history.
     *@return      <code>true</code>, if the document could be added,<br>
     *             <code>false</code> otherwise.
     */
    public boolean add(ManagedDocument doc) {
		final String METHOD = "add()";
		log.entering(METHOD);
       
        boolean ret = false;
        history.add(doc);

        if (doc != null) {
            if (doc.getState() == DocumentState.TARGET_DOCUMENT) {
                edoc = doc;
                if (onTop == null) {
                    onTop = edoc;
                }
                ret = true;
                if (isDebugEnabled) {
                    log.debug("add(): ManagedDocument.status=TARGET_DOCUMENT; successfully added");
                }
            }
            else if (doc.getState() == DocumentState.VIEW_DOCUMENT) {
                vdoc = doc;
                if (onTop == null) {
                    onTop = vdoc;
                }
                ret = true;
                if (isDebugEnabled) {
                    log.debug("add(): ManagedDocument.status=VIEW_DOCUMENT; successfully added ");
                }
            }
        }
        if (ret) {
            welcome = false;
        }
        else if (isDebugEnabled) {
            log.debug("add(): ManagedDocument was NOT added, because it has not the status VIEW_DOCUMENT or TARGET_DOCUMENT");
        }
        log.exiting();
        return ret;
    }


    /**
     *  Release the document from being displayed. The history is not affected.
     *  Returns true, if the document could be released, false otherwise.
     *
     *@param        rdoc Document, which should be released
     *@return       <code>true</code>, if the document was successfully released,<br>
     *              <code>false</code>, if the document was not handled by the DocumentHandler.
     */
    public boolean release(DocumentState rdoc) {
		final String METHOD = "release()";
		log.entering(METHOD);
  
        boolean ret = false;
        if (rdoc != null) {
            if (edoc != null && edoc instanceof ManagedDocument) {
                ManagedDocument mdoc = (ManagedDocument) edoc;
                if (rdoc == mdoc.getDocument()) {
                    if (onTop == edoc) {
                        onTop = vdoc;
                    }
                    edoc = null;
                    ret = true;
                    if (log.isDebugEnabled()) {
                        log.debug("release(): successfully released the TARGET_DOCUMENT");
                    }

                }
            }
            if (vdoc != null && vdoc instanceof ManagedDocument) {
                ManagedDocument mdoc = (ManagedDocument) vdoc;
                if (rdoc == mdoc.getDocument()) {
                    if (onTop == vdoc) {
                        onTop = edoc;
                    }
                    vdoc = null;
                    ret = true;
                    if (log.isDebugEnabled()) {
                        log.debug("release(): successfully released the VIEW_DOCUMENT");
                    }
                }
            }
        }
        log.exiting();
        return ret;
    }
    
    /**
     * Adds a MessageDocument.
     * 
     * @see MessageDocument
     */    
    public boolean add(MessageDocument message) {
		final String METHOD = "add()";
		log.entering(METHOD);
        if (log.isDebugEnabled()) {
            log.debug("message="+message);
        }
        boolean success = true;   
        if (message != null) {      
            messageDocument = message;                   
        } else
        	success = false;
        log.exiting();
        return success;
        
    }
    
    /**
     * Get the MessageDocument.
     * 
     * @see MessageDocument
     */
    public MessageDocument getMessageDocument() {
        return messageDocument;
    }
    
    /**
     * Release the MessageDocument.
     * 
     * @see MessageDocument
     */
    public void releaseMessageDocument() {
        messageDocument = null;
    }
    
    /**
     * Returns the currency for the Minibasket
     */
    public String getMiniBsktCurrency() {
        return miniBsktCurrency;
    }

    /**
     * Returns the doctype for the Minibasket
     */
    public String getMiniBsktDoctype() {
        return miniBsktDoctype;
    }

    /**
     * Returns the itemsize for the Minibasket
     */
    public String getMiniBsktItemSize() {
        return miniBsktItemSize;
    }

    /**
     * Returns the net value for the Minibasket
     */
    public String getMiniBsktNetValue() {
        return miniBsktNetValue;
    }

    /**
     * Sets the currency for the Minibasket
     */
    public void setMiniBsktCurrency(String miniBsktCurrency) {
        this.miniBsktCurrency = miniBsktCurrency;
    }

    /**
     * Sets the doctype for the Minibasket
     */
    public void setMiniBsktDoctype(String miniBsktDoctype) {
        this.miniBsktDoctype = miniBsktDoctype;
    }

    /**
     * Sets the itemsize for the Minibasket
     */
    public void setMiniBsktItemSize(String miniBsktItemSize) {
        this.miniBsktItemSize = miniBsktItemSize;
    }

    /**
     * Sets the net value for the Minibasket
     */
    public void setMiniBsktNetValue(String miniBsktNetValue) {
        this.miniBsktNetValue = miniBsktNetValue;
    }

}