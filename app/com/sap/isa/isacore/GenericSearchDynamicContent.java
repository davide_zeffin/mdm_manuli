/*****************************************************************************
    Class:        GenericSearchDynamicContent
    Copyright (c) 2004, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author        Ralf Witt

    $Revision: #8 $
    $DateTime: 2004/06/02 11:02:37 $ (Last changed)
    $Change: 189493 $ (changelist)

*****************************************************************************/
package com.sap.isa.isacore;

import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.GenericSearchSelectOptionLine;
import com.sap.isa.businessobject.GenericSearchSelectOptions;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.businesspartner.businessobject.HierarchyKey;
import com.sap.isa.businesspartner.businessobject.PartnerFunctionBase;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.interaction.InteractionConfigContainer;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.maintenanceobject.businessobject.GSProperty;
import com.sap.isa.maintenanceobject.businessobject.GSPropertyGroup;
import com.sap.isa.ui.uiclass.genericsearch.GenericSearchUIData;


/**
 * This class contains all methods used to fill dynamic content in the Generic Search
 * Framework. <b>Important</b>: Init method will be called by the framework first!
 */
public class GenericSearchDynamicContent {
             
  protected UserSessionData            userSessionData;
  protected HttpServletRequest         request;
  protected BusinessObjectManager      bom;
  protected GenericSearchSelectOptions selOpt;
  protected GSProperty                 pty;
  protected int                        handle;
  
  /** 
	*  The IsaLocation of the current Action.
	*  This will be instantiated during perform and is always present.
	*/
   protected IsaLocation log = IsaLocation.getInstance(GenericSearchDynamicContent.class.getName());  
  /**
   * Constructor with no arguments for reflection api
   *
   */
  public GenericSearchDynamicContent() {
  }
  
  /**
   * Init Method
   * @param  UserSessionData            session object
   * @param  HttpServletRequest         request object 
   * @param  BusinessObjectManager      bom object
   * @param  GenericSearchSelectOptions selOpt object which should be extended with the 
   *                                    dynamic content
   * @param  String                     Handle binding the select options
   * @param  GSProperty                 Property (must be last argument, ALWAYS)

   */
  public void init(UserSessionData            userSessionData,
                   HttpServletRequest         request,
                   BusinessObjectManager      bom,
                   GenericSearchSelectOptions selOpt,
                   String                     handle,
                   GSProperty                 pty) {
      this.userSessionData = userSessionData;
      this.request = (HttpServletRequest)request;
      this.bom     = bom;
      this.selOpt  = selOpt;
      this.pty     = pty;
      try {
		this.handle  = Integer.parseInt(handle);
      } catch (NumberFormatException ex) {
		if (log.isDebugEnabled()) {
			log.debug("Error ", ex);
		}
      }
      
  }
  /**
   * Use this generic method to add the following attributes of the current "dark" property
   * as a select option line.<br>
   * &lt;property
   *  name="..."
   *  entityType="..." 
   *  parameterType="..."
   *  tokenType="..."
   *  value="..."
   *  /&gt;
   */
  public void addDarkAttributeStatic() {
      selOpt.addSelectOptionLine( 
        new GenericSearchSelectOptionLine(
            handle,
            pty.getName(),
            (pty.getEntityType() != null ? pty.getEntityType() : ""),
            "",
            (pty.getParameterType() != null ? pty.getParameterType() : ""),
            pty.getTokenType(),
            "I",
            "EQ",
            (String)pty.getValue(),
            ""));
  }
  /**
   * Adds the SoldTo ID to the select options
   */
  public void addSoldToID() {
	  // get SoldTo from BuPaMa
	  BusinessPartnerManager buPaMa = bom.createBUPAManager();
	
	  BusinessPartner buPa = buPaMa.getDefaultBusinessPartner(PartnerFunctionBase.SOLDTO);
	
	  // Found
	  if (buPa != null) {
	  	  buPa.getTechKey().getIdAsString();
		  selOpt.addSelectOptionLine( 
		    new GenericSearchSelectOptionLine(
			    handle,
                pty.getName(),
                pty.getEntityType(),
                "",
                pty.getParameterType(),
                pty.getTokenType(),
                "I",
                "EQ",
                buPa.getId(),
                ""));
	  }
  }
  /**
   * Adds the PROCESS TYPE to the select options. Process type comes from the shop
   * property <code>getSelectionProcessType()</code>.
   */
  public void addSelectionProcessTypeHOM() {
      selOpt.addSelectOptionLine( 
        new GenericSearchSelectOptionLine(
            handle,
            pty.getName(),
            pty.getEntityType(),
            "",
            "",
            pty.getTokenType(),
            "I",
            "EQ",
            bom.getShop().getSelectionProcessType(),
            ""));
  }
  /**
   * Adds the FLAG (status selection on item level) to the select options. The flag will only be set
   * if request parameter <code>STAT(1)</code> is NOT set (@see method GenericSearchUIDynamicContent.getSelectionStatusHOM)
   * to BLANK or "I1002". 
   */
  public void addStatusSelectOnItemHOM() {
      String rpSTAT = request.getParameter("rc_status_item");
      if ( rpSTAT != null  &&  ! "".equals(rpSTAT)  &&  ! "I1002".equals(rpSTAT)) {
          selOpt.addSelectOptionLine( 
            new GenericSearchSelectOptionLine(
                handle,
                pty.getName(),
                pty.getEntityType(),
                "",
                "",
                "",
                "I",
                "EQ",
                "X",
                ""));
      }
  }
  /**
   * Depending on Shop settings, the location of the Billing Application, in which the search will 
   * be performed, will be added.<br> 
   * This could be <code>R3</code>, <code>CRM</code> or <code>R3CRM</code>.
   */
  public void addBillingApplicationLocation() {
      String billingSelect = (bom.getShop().isBillingSelectionR3() ? "R3" : "");
      billingSelect = (bom.getShop().isBillingSelectionCRM() ? "CRM" : billingSelect);
      billingSelect = (bom.getShop().isBillingSelectionR3CRM() ? "R3CRM" : billingSelect);
      selOpt.addSelectOptionLine( 
        new GenericSearchSelectOptionLine(
            handle,
            pty.getName(),
            pty.getEntityType(),
            "",
            pty.getParameterType(),
            pty.getTokenType(),
            "I",
            "EQ",
            billingSelect,
            ""));
      
  }
  /**
   * Add Shops country setting, which is dependent from the Sales Organization 
   */
  public void addCountry() {
      selOpt.addSelectOptionLine( 
        new GenericSearchSelectOptionLine(
            handle,
            pty.getName(),
            "",
            "",
            "",
            "",
            "",
            "",
            bom.getShop().getCountry().toUpperCase(),
            ""));
      
  }
  /**
   * Add DB specific settings to the select options for document type <code>ORDERTMP</code>.
   * These are<br>
   * - SOLDTOSELECTABLE (shop dependent)<br>
   * - SHOPGUID<br>
   * - DATEFORMAT (from shop)<br>
   */
  public void addDBSettings() {
      if ( ! "ORDERTMP".equals(request.getParameter("rc_documenttypes"))) {
          return;
      }
      selOpt.addSelectOptionLine( 
        new GenericSearchSelectOptionLine(
            handle,
            "SHOPGUID", "", "", "", "", "", "", bom.getShop().getTechKey().getIdAsString(), ""));
      selOpt.addSelectOptionLine( 
        new GenericSearchSelectOptionLine(
            handle,
            "DATEFORMAT", "", "", "", "", "", "", bom.getShop().getDateFormat(), ""));

      if (bom.getShop().isSoldtoSelectable()) {
          String partnerFunction = bom.getShop().getCompanyPartnerFunction();
          BusinessPartner buPa = bom.getBUPAManager().getDefaultBusinessPartner(partnerFunction);

          selOpt.addSelectOptionLine( 
            new GenericSearchSelectOptionLine(
                handle,
                "SOLDTOSELECTABLE", "", "", "", "", "", "", "X", ""));
          selOpt.addSelectOptionLine( 
            new GenericSearchSelectOptionLine(
                handle,
                "PARTNER_NO/2", "", partnerFunction, "GUID", "", "", "", buPa.getTechKey().getIdAsString(), ""));
          // Get soldTo from request
          String soldToID = request.getParameter("rc_soldtoId");
          if (soldToID != null  &&  soldToID.length() > 0) {
              buPa = bom.getBUPAManager().getBusinessPartner(soldToID);
              String soldToGUID = "00000000000000000000000000000000"; // Will return no documents for unknown BP!
              if (buPa != null) {
                  soldToGUID = buPa.getTechKey().getIdAsString();   
              }
              selOpt.addSelectOptionLine( 
                new GenericSearchSelectOptionLine(
                    handle,
                    "PARTNER_NO/1", "", PartnerFunctionData.SOLDTO, "GUID", "", "", "", soldToGUID, ""));
          }
      }
      
  }
  /**
   * Add Sales partner dependent from the shop customizing. Partner function type (e.g. 0001 for Sold in CRM)  
   * must be set in attribute "parameterType" of the property.
   */
  public void addSalesPartner() {
      String partnerFunction = bom.getShop().getCompanyPartnerFunction();
    
      BusinessPartner buPa = bom.getBUPAManager().getDefaultBusinessPartner(partnerFunction);
      // add sales partner to the partner list
      GenericSearchSelectOptionLine line = null;
      if (buPa != null) {
          line = new GenericSearchSelectOptionLine(handle, pty.getName(),  pty.getEntityType(), "", pty.getParameterType(),
                                                          pty.getTokenType() , "I", "EQ", buPa.getId(), "");
      } else {
          // For security reason, if no Business partner could be determined add a dummy one!              
          line = new GenericSearchSelectOptionLine(handle, pty.getName(),  pty.getEntityType(), "", pty.getParameterType(),
                                                          pty.getTokenType() , "I", "EQ", "0000000000", "");
      }
      selOpt.addSelectOptionLine(line);
  }
  /**
   * Hide the result list field "PO_NUMBER_SOLD" if not used as select criteria (there
   * it is "rc_po_number_uc" !)
   */
  public void hidePoNumberSold() {
      String poNo = (String)request.getParameter("rc_po_number_uc");
      if (poNo == null  ||  poNo.length() < 1) {
          setListFieldInvisible("PO_NUMBER_SOLD");
      }
  }
  /**
   * Adds the BPartners from the Hierarchy to the select options. This method replaces the addDealer one. 
   */
  public void addPartnersFromHierarchy() {

      boolean partnerAdded = false;

      BusinessPartnerManager buPaMa = bom.createBUPAManager();
      BusinessPartner buPa = buPaMa.getDefaultBusinessPartner(PartnerFunctionData.SOLDTO);

      // Check selected partner (rc_partner_range must correspond to generic-searchbackend-config.xml
      // partner parameter)
      String partnerRange = (String)request.getParameter("rc_partner_range");
      String partnerId     = (String)request.getParameter("rc_partnerid");
      partnerId = (partnerId != null ? partnerId : "");
      
      if ("MYSELF".equals(partnerRange)) {
          // Show only documents of myself
          selOpt.addSelectOptionLine( 
            new GenericSearchSelectOptionLine(
                handle,
                pty.getName(),
                pty.getEntityType(),
                "",
                pty.getParameterType(),
                pty.getTokenType(),
                "I",
                "EQ",
                buPa.getId(),
                ""));
          setListFieldInvisible("SOLD_TO_PARTY"); // 1order: Fieldname must correspond to XML file !!
          setListFieldInvisible("PAYER"); // Billing Engine Application (BEA)
          partnerAdded = true;
      } else if ("ALL".equals(partnerRange)) {
          // Selection should be performed for ALL dealers
          ResultData dealerFamilyList = null;
          if (buPa != null && bom.getShop().isBpHierarchyEnable()) {
            try { 
                HierarchyKey hierarchy = buPaMa.getPartnerHierachy();
                dealerFamilyList = buPa.getPartnerFromHierachy(hierarchy);
            } catch (CommunicationException comEx){
				if (log.isDebugEnabled()) {
					log.debug("Error ", comEx);
				}
            }
          }
          // Add entry representing dealer itself (myself is not in the dealerFamilyList !!)
          selOpt.addSelectOptionLine( 
            new GenericSearchSelectOptionLine(
                handle,
                pty.getName(),
                pty.getEntityType(),
                "",
                pty.getParameterType(),
                pty.getTokenType(),
                "I",
                "EQ",
                buPa.getId(),
                ""));
          if (dealerFamilyList != null) {
              dealerFamilyList.beforeFirst();
              while(!dealerFamilyList.isLast()) {
                  dealerFamilyList.next();
                  partnerId = dealerFamilyList.getString("SOLDTO");
                  // Add each dealer to the select options
                  selOpt.addSelectOptionLine( 
                    new GenericSearchSelectOptionLine(
                        handle,
                        pty.getName(),
                        pty.getEntityType(),
                        "",
                        pty.getParameterType(),
                        pty.getTokenType(),
                        "I",
                        "EQ",
                        partnerId,
                        ""));
              }
          } // ENDIF dealerFamilyList
          partnerAdded = true;
      } else if ("OTHER".equals(partnerRange)) { 
          // Dealer has been specified explicitly
          ResultData dealerFamilyList = null;
          if (buPa != null && bom.getShop().isBpHierarchyEnable()) {
            try { 
                HierarchyKey hierarchy = buPaMa.getPartnerHierachy();
                String hierarchyType = hierarchy.getHierarchyType();
                String hierarchyName = hierarchy.getHierarchyName(); 
                dealerFamilyList = buPa.getPartnerFromHierachy(hierarchyType, hierarchyName);
            } catch (CommunicationException comEx){
				if (log.isDebugEnabled()) {
					log.debug("Error ", comEx);
				}
            }
          }
          // Dealer itself is not contained in the dealerlist
          if (partnerId.equalsIgnoreCase(buPa.getId())) {
              selOpt.addSelectOptionLine(
               new GenericSearchSelectOptionLine(handle, pty.getName(), pty.getEntityType(), "", pty.getParameterType(),
                                                pty.getTokenType(), "I", "EQ", buPa.getId(), ""));
              partnerAdded = true;
          } else {
              if (partnerId.indexOf("*") > -1 ) {
                  String compVal01 = partnerId.substring(0, partnerId.indexOf("*"));
                  String compVal02 = (buPa.getId().length() <=   partnerId.indexOf("*") 
                                    ? buPa.getId() : buPa.getId().substring(0, partnerId.indexOf("*")));
                  if (compVal01.equalsIgnoreCase(compVal02)) {
                      selOpt.addSelectOptionLine( 
                        new GenericSearchSelectOptionLine(handle, pty.getName(), pty.getEntityType(), "", pty.getParameterType(),
                                                          pty.getTokenType(), "I", "EQ", buPa.getId(), ""));
                        partnerAdded = true;
                  }
              }
          }
          // Now work on the dealerlist
          if (dealerFamilyList != null) {
              dealerFamilyList.beforeFirst();
              String partnerIdForSearch = null;
              while(!dealerFamilyList.isLast()) {
                  dealerFamilyList.next();
                  if (partnerId.indexOf("*") < 0 ) {
                      // Exact search
                      if (partnerId.equalsIgnoreCase(dealerFamilyList.getString("SOLDTO")) ) {
                          selOpt.addSelectOptionLine( 
                            new GenericSearchSelectOptionLine(handle, pty.getName(), pty.getEntityType(), "", pty.getParameterType(),
                                                              pty.getTokenType(), "I", "EQ", partnerId, ""));
                            partnerAdded = true;
                            break;
                      }
                  } else {
                      // Wildcard search => add all dealers matching wildcard pattern
                      String compVal01 = partnerId.substring(0, partnerId.indexOf("*"));
                      String compVal02 = (dealerFamilyList.getString("SOLDTO").length() <= partnerId.indexOf("*") 
                                        ? dealerFamilyList.getString("SOLDTO") : dealerFamilyList.getString("SOLDTO").substring(0, partnerId.indexOf("*")));
                      if (compVal01.equalsIgnoreCase(compVal02)) {
                          selOpt.addSelectOptionLine( 
                            new GenericSearchSelectOptionLine(handle, pty.getName(), pty.getEntityType(), "", pty.getParameterType(),
                                                              pty.getTokenType(), "I", "EQ", dealerFamilyList.getString("SOLDTO"), ""));
                            partnerAdded = true;
                      }
                  }
              }
          }
      } else if (partnerRange == null) {               // ENDIF 'OTHER'
          // Without Partner Hierarchy add sold-to party
          setListFieldInvisible("SOLD_TO_PARTY"); // 1order: Fieldname must correspond to XML file !!
          setListFieldInvisible("PAYER"); // Billing Engine Application (BEA)
          selOpt.addSelectOptionLine( 
          new GenericSearchSelectOptionLine(handle, pty.getName(), pty.getEntityType(), "",
                                            pty.getParameterType(), pty.getTokenType(), "I", "EQ",
                                            buPa.getId(), ""));
      }
      
//    Security check will be done in the backend implementation. If the above logic can't determine a business partner,
//    because the entered value doesn't exists at all, and the default sold-to party is added, the result list is
//    confusing for the web user. Unfortuneatley Generic Search Framework doesn't support error messages for 
//    search criteria's!

//      // For security reason add partner itself if no partner could be added !
//      if (partnerAdded == false) {
//          setListFieldInvisible("SOLD_TO_PARTY"); // 1order: Fieldname must correspond to XML file !!
//          setListFieldInvisible("PAYER"); // Billing Engine Application (BEA)
//          selOpt.addSelectOptionLine( 
//            new GenericSearchSelectOptionLine(handle, pty.getName(), pty.getEntityType(), "",
//                                              pty.getParameterType(), pty.getTokenType(), "I", "EQ",
//                                              buPa.getId(), ""));
//      }
  }
  /**
   * Set Field given field in result list invisible. <br>i.e. This can be used in case the
   * result list should include only documents of myself. So don't show the field partner.
   * @param String Fieldname which should be set invisible
   */
  protected void setListFieldInvisible(String fieldname) {
      // Get Properties describing the result list
      String searchToDisplay = this.getScreenName(request, userSessionData);
      GSPropertyGroup resultListDescription = (GSPropertyGroup)request.getAttribute(GenericSearchUIData.RC_RESULTLIST_PTYGRP + searchToDisplay); 
      if (resultListDescription == null) {
          // Check in Session
          resultListDescription = 
             (GSPropertyGroup)userSessionData.getAttribute(
                 (GenericSearchUIData.RC_RESULTLIST_PTYGRP + searchToDisplay) );
      }
      if (resultListDescription != null) {
          Iterator itPTY = resultListDescription.iterator();
          while (itPTY.hasNext()) {
              GSProperty pty = (GSProperty)itPTY.next();
              if (fieldname.equals(pty.getName())) {
                  pty.setType("hidden");
              }
          }
      }
      
  }
  /**
   * Return the Screen name which describes the search criteria in the XML file
   * @return String screen name
   */
  protected String getScreenName(HttpServletRequest request, UserSessionData userSessionData) {
      String searchToDisplay = request.getParameter(GenericSearchUIData.RC_SEARCHCRITERASCREENNAME);
      if (searchToDisplay == null  ||  searchToDisplay.length() <= 0) {
          searchToDisplay = (String)request.getAttribute(GenericSearchUIData.RC_SEARCHCRITERASCREENNAME);
      }
      if (searchToDisplay == null  ||  searchToDisplay.length() <= 0) {
          DocumentHandler documentHandler = (DocumentHandler)userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
          if (documentHandler != null) {
              searchToDisplay = documentHandler.getOrganizerParameter(GenericSearchUIData.RC_SEARCHCRITERASCREENNAME);
          }
      }
      return searchToDisplay;
  }
  /**
   * For contracts add status information to the select options. This is depending on the shop setting
   * <code>isContractNegotiationAllowed()</code>. If true, the status <code>INPROCESS</code> will be
   * converted into "I1076" and "INQUIRY_IN_PROCESS_BY_CUSTOMER".<br>
   * If false only "RELEASED" ("I1004") contracts will be selected.
   */
  public void addContractStatus() {
      if (bom.getShop().isContractNegotiationAllowed()) {
          String status = (String)request.getParameter("rc_status");
          if ("INPROCESS".equals(status)) {
              selOpt.addSelectOptionLine( 
                new GenericSearchSelectOptionLine(
                    handle, pty.getName().substring(0, pty.getName().indexOf("(")), pty.getEntityType(),
                    "", "", pty.getTokenType(), "I", "EQ", "I1076", ""));
              selOpt.addSelectOptionLine( 
                new GenericSearchSelectOptionLine(
                    handle, "INQUIRY_IN_PROCESS_BY_CUSTOMER", "NOT_FOR_RF",
                    "", "", pty.getTokenType(), "I", "EQ", "X", ""));
          }
      } else {
          // For contracts status field will be set to invisible
          setListFieldInvisible("STATUS_SYSTEM");
          selOpt.addSelectOptionLine( 
            new GenericSearchSelectOptionLine( 
                handle, pty.getName().substring(0, pty.getName().indexOf("(")) , pty.getEntityType(),
                "", "", pty.getTokenType(), "I", "EQ", "I1004", ""));
      }
  }
  
  /**
   * Add the Reseller information to the select options.
   */
  public void addResellerID() {
	  
	  BusinessPartnerManager bupaManager = bom.createBUPAManager();
	  BusinessPartner partner = null;
	  
	  if(bom.getShop().isHomActivated()){
		partner = bupaManager.getDefaultBusinessPartner(PartnerFunctionData.SOLDFROM);
	  } else {
		partner = bupaManager.getDefaultBusinessPartner(PartnerFunctionData.RESELLER);
	  }
	  
	  
	  selOpt.addSelectOptionLine( 
		new GenericSearchSelectOptionLine(
			handle,
			pty.getName(),
			"",
			"",
			"",
			"",
			"",
			"",
			partner.getId(),
			""));
    }
    	    
    
	/**
	   * Add Sales Organization 
	   */
	  public void addSalesOrg() {
		  selOpt.addSelectOptionLine( 
			new GenericSearchSelectOptionLine(
				handle,
				pty.getName(),
				"",
				"",
				"",
				"",
				"",
				"",
				bom.getShop().getSalesOrganisation(),
				""));
      
	  }
	/**
	   * Add Distribution Channel 
	   */
	  public void addDistChan() {
		  selOpt.addSelectOptionLine( 
			new GenericSearchSelectOptionLine(
				handle,
				pty.getName(),
				"",
				"",
				"",
				"",
				"",
				"",
				bom.getShop().getDistributionChannel(),
				""));
      
	  }
	/**
	   * Add Division 
	   */
	  public void addDivision() {
		  selOpt.addSelectOptionLine( 
			new GenericSearchSelectOptionLine(
				handle,
				pty.getName(),
				"",
				"",
				"",
				"",
				"",
				"",
				bom.getShop().getDivision(),
				""));
      
	  }
	/**
		   * Add Agent for agent scenario in ERP 
		   */	  
	  public void addAgentID() {	  
		  BusinessPartnerManager bupaManager = bom.createBUPAManager();
		  BusinessPartner agent = bupaManager.getDefaultBusinessPartner(PartnerFunctionData.AGENT);
	  
		  selOpt.addSelectOptionLine( 
			new GenericSearchSelectOptionLine(
				handle,
				pty.getName(),
				"",
				"",
				"",
				"",
				"",
				"",
				agent.getId(),
				""));
		}

    /**
     * Add DB specific partner settings to the select options for document type <code>ORDERTMP</code>.
     * These are<br>
     * - PARTNER_NO/1, SOLDTO, ID<br>
     * - PARTNER_NO/1, SOLDTO, GUID<br>
     */
    public void addSoldToDB() {
        if ( ! "ORDERTMP".equals(request.getParameter("rc_documenttypes"))) {
            return;
        }
        BusinessPartnerManager bupaManager = bom.createBUPAManager();
        BusinessPartner soldTo = bupaManager.getDefaultBusinessPartner(PartnerFunctionData.SOLDTO);

        selOpt.addSelectOptionLine( 
          new GenericSearchSelectOptionLine(
              handle, "PARTNER_NO/1", "", PartnerFunctionData.SOLDTO, "ID",
              "", "I", "EQ", soldTo.getId(), ""));
        selOpt.addSelectOptionLine( 
          new GenericSearchSelectOptionLine(
              handle, "PARTNER_NO/1", "", PartnerFunctionData.SOLDTO, "GUID",
              "TOKEN", "I", "EQ", soldTo.getTechKey().getIdAsString(), ""));
    }
	/**
     * Handles all select options for the java basket in the B2C scenario. This is required since
     * in the B2C scenario no selection screen is presented but just a link. So it is not possible
     * to use the dependencies in the XML file. The following options will be added.<br>
     * - PARTNER_NO/1, SOLDTO,   ID<br>
     * - PARTNER_NO/1, SOLDTO,   GUID<br>
     * - SHOPGUID,<br>
     * - DATEFORMAT<br>
	 *
	 */
    public void addSelectOptionsJavaBasketB2C() {
        if ( ! "SAVEDBASKETS".equals(request.getParameter("rc_documenttypes")) &&
             ! "SAVEDTEMPLATES".equals(request.getParameter("rc_documenttypes"))  ) {
            return;
        }
        BusinessPartnerManager bupaManager = bom.createBUPAManager();
        BusinessPartner soldTo = bupaManager.getDefaultBusinessPartner(PartnerFunctionData.SOLDTO);

        selOpt.addSelectOptionLine( 
          new GenericSearchSelectOptionLine(
              handle, "PARTNER_NO/1", "", PartnerFunctionData.SOLDTO, "ID",
              "", "I", "EQ", soldTo.getId(), ""));
        selOpt.addSelectOptionLine( 
          new GenericSearchSelectOptionLine(
              handle, "PARTNER_NO/1", "", PartnerFunctionData.SOLDTO, "GUID",
              "TOKEN", "I", "EQ", soldTo.getTechKey().getIdAsString(), ""));
        selOpt.addSelectOptionLine( 
          new GenericSearchSelectOptionLine(
              handle, "SHOPGUID", "", "", "",
              "", "I", "EQ", bom.getShop().getTechKey().getIdAsString(), ""));
        selOpt.addSelectOptionLine( 
          new GenericSearchSelectOptionLine(
              handle, "DATEFORMAT", "", "", "",
              "", "I", "EQ", bom.getShop().getDateFormat(), ""));
    }
    /**
     * Add shop settings controlling complexity of the quotations search. Following
     * options could be added.<br> In case of "OPEN" extended quotations set field
     * "VALID_TO" invisible (since validity period is not yet determined for open quots!)
     * - QUOTATION_TYPE     LEAN (only lean quotations should be selected)<br>
     * - QUOTATION_TYPE     EXTLEAN (select ext. and lean quotations)<br>
     */
    public void addQuotationType() {
        if ( ! ("QUOTATION".equals(request.getParameter("rc_documenttypes")) ||
		        "QUOTITM".equals(request.getParameter("rc_documenttypes"))) ) {
            return;
        }
        if ( "OPEN".equals(request.getParameter("rc_status_head2")) && 
            !"QUOTITM".equals(request.getParameter("rc_documenttypes")) ) {
            setListFieldInvisible("VALID_TO");
        }
        if ( ! bom.getShop().isQuotationExtended()) {
            selOpt.addSelectOptionLine( 
              new GenericSearchSelectOptionLine(
                  handle, "QUOTATION_TYPE", "", "", "",
                  "", "", "", "LEAN", ""));        
        } else {
            if (bom.getShop().isQuotationSearchAllTypesRequested()) {
                selOpt.addSelectOptionLine( 
                  new GenericSearchSelectOptionLine(
                      handle, "QUOTATION_TYPE", "", "", "",
                      "", "", "", "EXTLEAN", ""));        
            }
        }
    }
    
	/**
	 * Adds the SoldTo ID to the select options
	 */
	public void addPartnerFunction() {
	  selOpt.addSelectOptionLine( 
		new GenericSearchSelectOptionLine(
			handle,
			pty.getName(),
			"",
			"",
			"",
			"",
			"",
			"",
			bom.getShop().getResellerPartnerFunction(),
			""));
	}   
	/**
	 * 
	 */
	public void selectFieldObjectIdCRMBasketB2C() { 
	/*	if (ExtendedConfigInitHandler.isActive()) {
			InteractionConfigContainer interactionConfigData = getInteractionConfig(request);
			if ("true".equals(interactionConfigData.getConfig("basket").getParams().get("enableSaveBasket"))) {
				if (bom.getUser().isUserLogged() && bom.getBasket()!= null) {
					setListFieldInvisible("OBJECT_ID");
				} else {
					setListFieldInvisible("OBJECT_ID_NO_LINK");
				}
			} else {
				setListFieldInvisible("OBJECT_ID_NO_LINK");
			}
		}
	*/	
	}
	
	/**
	 * Returns configuration valid for the current session. This feature
	 * is only available if Extended Configuration Management is turned on.
	 * If no configuration data is available <code>null</code> is returned
	 * @param request the HTTP request
	 * @return configuration data
	 */
	protected InteractionConfigContainer getInteractionConfig(HttpServletRequest request) {
		UserSessionData userData =
				UserSessionData.getUserSessionData(request.getSession());
		if (userData == null) {
			return null;		
		}

	
		return (InteractionConfigContainer)(userData.getAttribute(SessionConst.INTERACTION_CONFIG));
	}
}