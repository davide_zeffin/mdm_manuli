/*****************************************************************************
    Class         TealeafInitHandler
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      May 2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/07/06 $
*****************************************************************************/
package com.sap.isa.isacore;

import java.lang.reflect.Method;
import java.util.Properties;

import com.sap.isa.core.FrameworkConfigManager;
import com.sap.isa.core.init.Initializable;
import com.sap.isa.core.init.InitializationEnvironment;
import com.sap.isa.core.init.InitializeException;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.xcm.config.ComponentConfig;
import com.sap.isa.core.xcm.config.ComponentConfigContainer;
import com.sap.isa.core.xcm.init.ExtendedConfigInitHandler;


/**
 * This class initialize the business event capturer. <br>
 * 
 * <i>For historical reason the class is called "TealeafInitHandler", but there are 
 * no dependencies of the tealeaf api. The tealeaf calls are encapsulated within the
 * Capturer Framework. And any other capturer could be used also.</i><br>
 *
 * If you want to use your own capture, you can can extend your class from
 * <code>BusinessEventTealeafCapturer</code> and change the property
 * tealeaf-handler to your own class. <br>
 * 
 * The following properties will be used by this class
 * <ul>
 * 	<li><b>tealeaf-handler</b> name of the business event capturer class
 *  <li><b>availibiltyCheckAvailable</b> switch to activate availibily information 
 *      within catalog product information
 *  <li><b>useAlwaysSalesDocItem</b> switch to force the usage of item sales doc item within 
 *      document events (AddToDocument, ModifyDocumentItem, DropFromDocument).<br>
 *      <b>Note:</b> The switch readMissingInfosFromBackend in the BusinessEventHandler should be set to true. 
 *      See <code>BusinessEventHandlerBase</code> for details. 
 *  <li><b>removeEmptyValues</b> switch to force the capturer event to filter empty output. 
 *      See <code>CaptureEvent</code>  for details.
 *  <li><b>decimalPointFormat</b> defines the number format which is used with the event output   
 * </ul>
 *
 * @see com.sap.isa.isacore.BusinessEventTealeafCapturer
 * @see com.sap.isa.core.businessobject.event.BusinessEventHandlerBase
 * @see com.sap.isa.core.businessobject.event.capturer.CapturerEvent
 *
 */

public class TealeafInitHandler implements Initializable {

    // instance of the BusinessEventTealCapturer
    private static BusinessEventCapturer instance;
	//location of cache directory to store event mapping files from BW
	private static String cacheDir;
	
	private final IsaLocation log = IsaLocation.getInstance(TealeafInitHandler.class.getName());
	
    /**
    * Initialize the implementing component.
    *
    * @param env the calling environment
    * @param props   component specific key/value pairs
    * @exception InitializeException at initialization error
    */
    public void initialize(InitializationEnvironment env, Properties props)
             throws InitializeException {
        try {
			log.debug("Initializing TealeafInitHandler");
			ComponentConfigContainer ccc = FrameworkConfigManager.XCM.getApplicationScopeConfig();
			ComponentConfig compContainer = ccc.getComponentConfig("wec", "wecconfig");
							
			String isWecEnabled = compContainer.getParamConfig().getProperty("enable.BEventCapturing", "false");
			if("false".equalsIgnoreCase(isWecEnabled)){
				log.debug("TealeafInitHandler initialization aborted as capturing disabled.");
				return;
			}
			
        	if (log.isDebugEnabled())
        		log.debug("Initializing the TealeafInitHandler with properties " + props);
        		
            String className=props.getProperty("tealeaf-handler");

            if (className == null || className.length() == 0) {
                className = "com.sap.isa.isacore.BusinessEventTealeafCapturer";
            }

            // take the handler class from init file
            Class handlerClass = Class.forName(className);

            // now get the instance from the handle
            instance = (BusinessEventCapturer) handlerClass.newInstance();

            String availabilityCheck = props.getProperty("availibiltyCheckAvailable");

            if (availabilityCheck!= null && availabilityCheck.equals("true")) {

                // get the init method from the class
                Method method = handlerClass.getMethod("setAvailibiltyCheckAvailable",
                                                       new Class[]{Boolean.TYPE});

                // invoke the method to initialize the handler
                method.invoke(null, new Object[]{new Boolean(true)});
            }

            String useAlwaysSalesDocItem = props.getProperty("useAlwaysSalesDocItem");

            if (useAlwaysSalesDocItem!= null && useAlwaysSalesDocItem.equals("true")) {

                // get the init method from the class
                Method method = handlerClass.getMethod("setUseAlwaysSalesDocItem",
                                                       new Class[]{Boolean.TYPE});

                // invoke the method to initialize the handler
                method.invoke(null, new Object[]{new Boolean(true)});
            }

			String removeEmptyValues = props.getProperty("removeEmptyValues");

			if (removeEmptyValues!= null && removeEmptyValues.equals("true")) {

				// get the init method from the class
				Method method = handlerClass.getMethod("setRemoveEmptyValues",
													   new Class[]{Boolean.TYPE});

				// invoke the method to initialize the handler
				method.invoke(null, new Object[]{new Boolean(true)});
			}

            String decimalPointFormat = props.getProperty("decimalPointFormat");

            if (decimalPointFormat!= null && decimalPointFormat.length() > 0) {

                // get the init method from the class
                Method method = handlerClass.getMethod("setNumberFormat",
                                                       new Class[]{String.class});

                // invoke the method to initialize the handler
                method.invoke(null, new Object[]{decimalPointFormat});
            }
			if (log.isDebugEnabled())
				log.debug("Finished initializing the TealeafInitHandler ");
        } catch (Exception ex) {
          new InitializeException(ex.toString());
        }
        
		String cacheDir = props.getProperty("eventMapFileStorageDir");
		if( (cacheDir == null) || !(cacheDir.length() > 0) ){
				cacheDir =ExtendedConfigInitHandler.getXCMCacheDir();
		}
		log.debug("BW Event map file storage location set to "+ cacheDir);
		setCacheDir(cacheDir);
    }


	/**
     * Returns the instance of the singelton BusinessEventCapturer
     *
     * @return instance
     *
     */
    public static BusinessEventCapturer getInstance() {
       return instance;
    }



    /**
    * Terminate the component.
    */
    public void terminate() {
    }
    
	/**
	 * Returns the cache directory where the files obtained from BW are stored
	 * @return cachedirectory
	 */
	public static String getCacheDir() {
		return cacheDir;
	}

	/**
	 * Sets the directory where BW files can be stored
	 * @param dir 
	 */
	public static void setCacheDir(String dir) {
		cacheDir = dir;
	}


}
