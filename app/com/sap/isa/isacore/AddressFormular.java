/*****************************************************************************
    Class:        AddressFormular
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Created:      August 2001
    Version:      1.0

    $Revision: #2 $
    $Date: 2002/10/29 $
*****************************************************************************/

package com.sap.isa.isacore;

import javax.servlet.http.HttpServletRequest;

import com.sap.isa.backend.boi.isacore.AddressConfiguration;
import com.sap.isa.businessobject.Address;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.core.util.table.TableRow;

/**
 * This class helps you to handle an address object on a jsp.
 *
 * @author SAP
 * @version 1.0
 */
public class AddressFormular extends AddressFormularBase {


    /**
     * Easy constructor.
     */
    public AddressFormular(){
    }

    /**
     * Constructor.
     *
     * @param addressFormatId
     */
    public AddressFormular(int addressFormatId) {
        super(addressFormatId);
    }

    /**
     * Create the object parsing the request.
     *
     * @param requestParser
     */
    public AddressFormular(RequestParser requestParser) {
        super(requestParser);
    }

    /**
     * Create the object parsing the request.
     * @param requestParser
     * @param address
     */
    public AddressFormular(RequestParser requestParser, Address address) {
        super(requestParser,address);
    }

    /**
     * Constructor.
     * @param address
     * @param addressFormatId
     */
    public AddressFormular(Address address, int addressFormatId) {
        super(address, addressFormatId);
    }

    /**
     * Constructor.
     * @param address
     * @param addressFormatId
     * @param isEditable
     */
    public AddressFormular(Address address, int addressFormatId, boolean isEditable) {
        super(address, addressFormatId,isEditable);
    }


    /**
     * Set all relvant object in the request context.
     *
     * @param request
     * @param shop the shop object would be used to get the country and region list
     *
     */
    public void addToRequest(HttpServletRequest request, Shop shop)
            throws CommunicationException {

        addToRequest(request, (AddressConfiguration) shop);
    }


    /**
     * Set all relvant object in the request context.
     *
     * @param request
     * @param configuration the configuration object would be used to get the country and region list
     *
     */
    public void addToRequest(HttpServletRequest request,
                             AddressConfiguration configuration)
            throws CommunicationException {


        Shop shop = null;

        if (configuration instanceof Shop) {
            shop = (Shop)configuration;
        }

        request.setAttribute(AddressConstants.RC_ADDRESS_FORMULAR,this);

        if(address == null) {
            address = new Address();
        }

        request.setAttribute(AddressConstants.RC_ADDRESS,address);

        countryList = configuration.getCountryList();
        regionList = null;

        if (address.getCountry().length() > 0) {

            // Put the country of the address to the country list
            // if it does not exists in the list.
            // To avoid JSP corrections this is done here
            if (shop != null && shop.getCountryDescription(address.getCountry()) == null) {

                                String countryDescription = shop.readCountryDescription(address.getCountry());
                if ((countryDescription != null) && (countryDescription.length() > 0)) {

                    // the country list is a ResultData which is only readable
                    // so a new ResultData object must be created
                    // (for a better performance the access to a table object
                    //  for the country list in the configuration will be better!!!)
                    Table countryTable = new Table("Countries");

                    countryTable.addColumn(Table.TYPE_STRING,AddressConfiguration.ID);
                    countryTable.addColumn(Table.TYPE_STRING,AddressConfiguration.DESCRIPTION);
                    countryTable.addColumn(Table.TYPE_BOOLEAN,AddressConfiguration.REGION_FLAG);

                    int numEntries = countryList.getNumRows();
                    countryList.first();
                    TableRow row = null;

                    for(int i = 1; i <= numEntries; i++) {

                        row = countryTable.insertRow();

                        row.setRowKey(new TechKey(countryList.getString(AddressConfiguration.ID)));
                        row.getField(AddressConfiguration.ID).setValue(countryList.getString(AddressConfiguration.ID));
                        row.getField(AddressConfiguration.DESCRIPTION).setValue(countryList.getString(AddressConfiguration.DESCRIPTION));
                        row.getField(AddressConfiguration.REGION_FLAG).setValue(countryList.getBoolean(AddressConfiguration.REGION_FLAG));
                        countryList.next();
                    }

                    row = countryTable.insertRow();
                    row.setRowKey(new TechKey(address.getCountry()));
                    row.getField(AddressConfiguration.ID).setValue(address.getCountry());
                    row.getField(AddressConfiguration.DESCRIPTION).setValue(shop.readCountryDescription(address.getCountry()));
                    //if there is a region in the address set the region flag
                    //and add the region into the region list later on
                    
					String region = address.getRegion();
					if (region != null && region.length()> 0) {
                        row.getField(AddressConfiguration.REGION_FLAG).setValue(true);
                    }
                    else {
                        row.getField(AddressConfiguration.REGION_FLAG).setValue(false);
                    }

                    countryList = new ResultData(countryTable);

					Table regionTable = new Table("Regions");
					regionTable.addColumn(Table.TYPE_STRING,AddressConfiguration.ID);
					regionTable.addColumn(Table.TYPE_STRING,AddressConfiguration.DESCRIPTION);

					if(region != null && region.length()> 0) {
						// if the country is not in the country list there is
						// no region flag set and so there is no region list
						// prepare a region list if a region exists in the address
						row = regionTable.insertRow();
						row.setRowKey(new TechKey(address.getRegion()));
						row.getField(AddressConfiguration.ID).setValue(address.getRegion());
						row.getField(AddressConfiguration.DESCRIPTION).setValue(shop.readRegionDescription(address.getRegion(), address.getCountry()));
					}
					regionList = new ResultData(regionTable);
					
                }
            }
            else {
                regionList = configuration.getRegionList(address.getCountry());
            }
        }

        else {
            countryList.first();
            regionList = configuration.getRegionList(countryList.getString(AddressConfiguration.ID));
        }

        //countryList.beforeFirst();
		countryList.first();
        request.setAttribute(AddressConstants.RC_COUNTRY_LIST,countryList);

        log.debug(countryList.toString());
        if (countryList.first() == true) {        
			log.debug(countryList.getObject("REGION_FLAG"));
        }
        if (regionList!= null){
             regionList.beforeFirst();
        }
        
        request.setAttribute(AddressConstants.RC_REGION_LIST,regionList);

        if (address.getCountry().length() > 0 && address.getRegion().length() > 0
                && address.getPostlCod1().length() > 0 && address.getCity().length() > 0) {
            taxCodeList = configuration.getTaxCodeList(address.getCountry(),
                                                       address.getRegion(),
                                                       address.getPostlCod1(),
                                                       address.getCity());
            if(taxCodeList != null && taxCodeList.getNumRows() > 0) {
                request.setAttribute(AddressConstants.RC_TAXCODE_LIST, taxCodeList);
            }
        }

        request.setAttribute(AddressConstants.RC_TITLE_LIST,
                             configuration.getTitleList());

        log.debug("addToRequest: " + configuration.getTitleList().getNumRows() +
                  " entries found in title list");

    }



}
