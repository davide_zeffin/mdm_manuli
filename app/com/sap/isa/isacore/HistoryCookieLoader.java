/**
 * Class:        HistoryCookieLoader 
 * Copyright:    (c) 2001, SAP AG. All rights reserved. 
 * Author:       SAP AG
 * Created: 	 20.06.2001 
 * Version: 	 1.0 
 * $Revision: #4 $ 
 * $Date: 2001/07/18 $
 */

package com.sap.isa.isacore;

import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.JspUtil;



/**
 *  Loads and saves the items of the history in a cookie.
 *
 *@author SAP AG
 *@created 3. Dezember 2001
 *@see History
 *@version 1.0
 */

public class HistoryCookieLoader
         extends HistoryLoader {
    /**
     *  Request, needed because we get the cookie from it.
     */
    protected HttpServletRequest request = null;

    /**
     *  Response, needed because we set the cookie in it.
     */
    protected HttpServletResponse response = null;

    /**
     *  Character used for seperating the items of the history in the cokie string.
     */
    protected char itemdelimiter = '#';

    /**
     *  Character used for seperating the attributes of the items of the history in the cokie string.
     */
    protected char attributedelimiter = '$';

    private static final IsaLocation log = IsaLocation.getInstance(HistoryCookieLoader.class.getName());

    private boolean isDebugEnabled = log.isDebugEnabled();

    /**
     *  Maximal age of the cookie: <b>100 days</b>. After that time, the cookie is deleted.
     */
    public final static int COOKIE_MAX_AGE = 100 * 24 * 60 * 60;

    /**
     *  Maximal number of history items, which are stored in the cookie: <b>15</b>.
     */
    public final static int MAX_ITEMS = 15;

    /**
     *  Sets the sessioncontext
     *
     *@param  req  Request context.
     *@param  res  Response context.
     */
    public void setContext(HttpServletRequest req, HttpServletResponse res) {
        request = req;
        response = res;
    }

    /**
     *  Loads the elements of the history out of the cookie
     *  and unescapes and decodes them. It loads not more than MAX_ITEMS items.
     *
     *@param  userid  Unambiguous ID of the user.
     *@param  shopid  Unambiguous ID of the shop.
     *@return         <code>true</code>, if the cookie could be loaded,<br>
     *                <code>false</code>, else.
     */
    public boolean load(String userid, String shopid) {
		final String METHOD = "load()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("userId="+userid+", shopId="+shopid);
        int index = 0;
        String cookiename;
        String content = "";
        String readhistoryitem[] = new String[MAX_ITEMS];
        String docnumbers[] = new String[MAX_ITEMS];
        String doctypes[] = new String[MAX_ITEMS];
        String hrefs[] = new String[MAX_ITEMS];
        String hrefparameters[] = new String[MAX_ITEMS];
        String docdates[] = new String[MAX_ITEMS];
        String refnames[] = new String[MAX_ITEMS];
        String refnumbers[] = new String[MAX_ITEMS];
        String decdocnumbers[] = new String[MAX_ITEMS];
        String decdoctypes[] = new String[MAX_ITEMS];
        String dechrefs[] = new String[MAX_ITEMS];
        String dechrefparameters[] = new String[MAX_ITEMS];
        String decdocdates[] = new String[MAX_ITEMS];
        String decrefnames[] = new String[MAX_ITEMS];
        String decrefnumbers[] = new String[MAX_ITEMS];
        String unesdocnumbers[] = new String[MAX_ITEMS];
        String unesdoctypes[] = new String[MAX_ITEMS];
        String uneshrefs[] = new String[MAX_ITEMS];
        String uneshrefparameters[] = new String[MAX_ITEMS];
        String unesdocdates[] = new String[MAX_ITEMS];
        String unesrefnames[] = new String[MAX_ITEMS];
        String unesrefnumbers[] = new String[MAX_ITEMS];

        if (isDebugEnabled) {
            log.debug("load(): userid = " + userid + " shopid = " + shopid);
        }
		try {
		
	        if (request == null) {
	            log.warn(LogUtil.APPS_USER_INTERFACE, "b2b.cookie.warn.norequest");
	            return false;
	            // without request, we cannot do anything
	        }
	
	        Cookie[] cookies = request.getCookies();
	
	        if (cookies == null) {
	            if (isDebugEnabled) {
	                log.debug("load(): There are no cookies!");
	            }
	            return false;
	        }
	        
	        // Determine the name of the cookie.
	        // It is the concatenation from userid and shopid. Because not all characters are allowed
	        // we must encode it.
	        cookiename = JspUtil.encodeURL(userid + shopid);
	        if (isDebugEnabled) {
	            log.debug("load(): name of the cookie: " + cookiename);
	        }
	
	        // Search through all the cookies, if there is one with the desired name.
	        for (int i = 0; i < cookies.length; i++) {
	            if (cookiename.equals(cookies[i].getName())) {
	                content = cookies[i].getValue();
	                break;
	            }
	        }
	
	        // if there was a cookie with the desired name, its content is now in the variable content
	        if ((content == null)) {
	            if (isDebugEnabled) {
	                log.debug("load(): There is no cookie with name: " + cookiename);
	            }
	            return false;
	        }
	
	        if (content.length() < 12) {
	            // 12 is somehow arbitrary, but there are 7 attributes seperated by special characters
	            if (isDebugEnabled) {
	                log.debug("load(): Found a cookie with the right name: " + cookiename + ", but the content is corrupted! I cannot load it.");
	            }
	            return false;
	        }
	
	        // now we have a cookie content, which is worth for being parsed.
	        // parse the cookie content and seperate all items, but read not more than MAX_ITEMS
	        // put the items in the array readhistoryitem
	        JspUtil.SimpleEscapeSafeStringTokenizer readtoken = new JspUtil.SimpleEscapeSafeStringTokenizer(content, itemdelimiter);
	        index = 0;
	
	        while (readtoken.hasMoreTokens() && index < MAX_ITEMS) {
	            readhistoryitem[index] = readtoken.nextToken();
	            index++;
	        }
	
	        if (index == 0) {
	            if (isDebugEnabled) {
	                log.debug("load(): The cookie: " + cookiename + ", does not contain any valid items.");
	            }
	            return false;
	        }
	
	        // parse the items and try to seperate the attributes
	        for (int x = 0; x < index; x++) { // loop over all items
	
	            JspUtil.SimpleEscapeSafeStringTokenizer itemtoken = new JspUtil.SimpleEscapeSafeStringTokenizer(readhistoryitem[x], attributedelimiter);
	
	            if (itemtoken.getNumTokens() == 7) { // are there 7 tokens for the 7 itemattributes?
	                docnumbers[x] = itemtoken.nextToken();
	                doctypes[x] = itemtoken.nextToken();
	                hrefs[x] = itemtoken.nextToken();
	                hrefparameters[x] = itemtoken.nextToken();
	                refnames[x] = itemtoken.nextToken();
	                refnumbers[x] = itemtoken.nextToken();
	                docdates[x] = itemtoken.nextToken();
	            }
	            else {
	                if (isDebugEnabled) {
	                    log.debug("load(): There is an error while parsing the attributes of item " + x + " of the cookie: " + cookiename + ", but I proceed.");
	                }
	            }
	        } // end for
	
	        // Looping over all itemattributes and decoding and unescaping them
	        for (int x = 0; x < index; x++) {
	
	            unesdoctypes[x] = JspUtil.unescapeCharacter(doctypes[x], attributedelimiter);
	            decdoctypes[x] = JspUtil.unescapeCharacter(unesdoctypes[x], itemdelimiter);
	            doctypes[x] = JspUtil.decodeURL(decdoctypes[x]);
	
	            unesdocnumbers[x] = JspUtil.unescapeCharacter(docnumbers[x], attributedelimiter);
	            decdocnumbers[x] = JspUtil.unescapeCharacter(unesdocnumbers[x], itemdelimiter);
	            docnumbers[x] = JspUtil.decodeURL(decdocnumbers[x]);
	
	            unesrefnumbers[x] = JspUtil.unescapeCharacter(refnumbers[x], attributedelimiter);
	            decrefnumbers[x] = JspUtil.unescapeCharacter(unesrefnumbers[x], itemdelimiter);
	            refnumbers[x] = JspUtil.decodeURL(decrefnumbers[x]);
	
	            unesrefnames[x] = JspUtil.unescapeCharacter(refnames[x], attributedelimiter);
	            decrefnames[x] = JspUtil.unescapeCharacter(unesrefnames[x], itemdelimiter);
	            refnames[x] = JspUtil.decodeURL(decrefnames[x]);
	
	            unesdocdates[x] = JspUtil.unescapeCharacter(docdates[x], attributedelimiter);
	            decdocdates[x] = JspUtil.unescapeCharacter(unesdocdates[x], itemdelimiter);
	            docdates[x] = JspUtil.decodeURL(decdocdates[x]);
	
	            uneshrefs[x] = JspUtil.unescapeCharacter(hrefs[x], attributedelimiter);
	            dechrefs[x] = JspUtil.unescapeCharacter(uneshrefs[x], itemdelimiter);
	            hrefs[x] = JspUtil.decodeURL(dechrefs[x]);
	
	            uneshrefparameters[x] = JspUtil.unescapeCharacter(hrefparameters[x], attributedelimiter);
	            dechrefparameters[x] = JspUtil.unescapeCharacter(uneshrefparameters[x], itemdelimiter);
	            hrefparameters[x] = JspUtil.decodeURL(dechrefparameters[x]);
	        }
	        // end for
	
	        // Out of the strings of the cookie, a ManagedDocument will be created
	        // and added to the history list
	        for (int x = index; x >= 0; x--) {
	            // for loop to store the items in the right sequence to get the items in chronological order
	            ManagedDocument item = new ManagedDocument(null, doctypes[x], docnumbers[x], refnumbers[x], refnames[x], docdates[x], "", hrefs[x], hrefparameters[x]);
	            history.add(item);
	        }
	        if (isDebugEnabled) {
	            log.debug("load(): Everything seems ok. return true");
	        }
	        return true;
		} finally {
			log.exiting();
		}
    }


    /**
     *  Saves the first MAX_ITEMS history items in a cookie (for preventing the
     *  cookie getting to big).
     *  The name of the cookie is the
     *  URL encoded concatenation of userid and shopid. So there is a cookie for every
     *  shop user combination.
     *
     *@param  userid  Unambiguous ID of the user.
     *@param  shopid  Unambiguous ID of the shop.
     *@return         <code>true</code>, if the cookie could be saved,<br>
     *                <code>false</code>, else.
     *@see HistoryCookieLoader#MAX_ITEMS
     */
    public boolean save(String userid, String shopid) {
		final String METHOD = "save()";
		log.entering(METHOD);
		
		String cookiename;
        String listofitems = "";
        String itemvalue = "";
        String hrefparameter = "";
        String result = "";
        String docnumber = "";
        String doctype = "";
        String href = "";
        String docdate = "";
        String refnumber = "";
        String refname = "";
        String encdocnumber = "";
        String encdoctype = "";
        String enchref = "";
        String encdocdate = "";
        String encrefnumber = "";
        String encrefname = "";
        String enchrefparameter = "";
        List itemlist = null;
        Cookie cookie;

        if (isDebugEnabled) {
            log.debug("save(): userid= " + userid + " ; shopid= " + shopid);
        }
        try {
        
	        if (response == null) {
	            log.warn(LogUtil.APPS_USER_INTERFACE, "b2b.cookie.warn.noresponse");
	            return false;
	        }
	        if (history == null) {
	            log.warn(LogUtil.APPS_USER_INTERFACE, "b2b.cookie.warn.nohistory");
	            return false;
	        }
	
	        itemlist = history.getHistoryList();
	
	        if (itemlist == null) {
	            log.warn(LogUtil.APPS_USER_INTERFACE, "b2b.cookie.warn.nohistorylist");
	            return false;
	            // nix zum speichern da
	        }
	        if (itemlist.size()<=0) {
	            if (isDebugEnabled) {
	                log.debug("save(): There are no history items to save.");
	            }
	            return false;
	            // nix zum speichern da
	        }
	
	        // Determine the name of the cookie.
	        // It is the concatenation from userid and shopid. Because not all characters are allowed
	        // we must encode it.
	        cookiename = JspUtil.encodeURL(userid + shopid);
	
	        // Create the cookie with the name
	        try {
	            cookie = new Cookie(cookiename, "");
	        }
	        catch (java.lang.IllegalArgumentException e) {
	            log.warn(LogUtil.APPS_USER_INTERFACE, "b2b.cookie.warn.wrongname");
	            if (isDebugEnabled) {
	                log.debug("save(): name of the cookie: " + cookiename);
	            }
	            return false;
	        }
	
	        // The content of a cookie is only one string.
	        // For this we must use a special character (itemdelimiter) to seperate the items of the history
	        // and a special character (attributedelimiter) to seperate the attributes of each historyitem.
	        // Because the special characters can occur in the attributes of the history items, they must
	        // escaped. This is done by doubling the special characters.
	
	        // We store only the first MAX_ITEMS entries of the history, because we don't want
	        // a cookie, which is to big.
	        for (int i = 0; i < MAX_ITEMS && i < itemlist.size(); i++) {
	            // Read the items out of the historylist and make shure,
	            // that their attributes are at least one character long.
	            HistoryItem hisitem = (HistoryItem) itemlist.get(i);
	
	            if ((hisitem.getDocNumber() != null) && (hisitem.getDocNumber().length() > 1)) {
	                docnumber = hisitem.getDocNumber();
	            }
	            else {
	                docnumber = " ";
	            }
	
	            if ((hisitem.getDocType() != null) && (hisitem.getDocType().length() > 1)) {
	                doctype = hisitem.getDocType();
	            }
	            else {
	                doctype = " ";
	            }
	
	            if ((hisitem.getHref() != null) && (hisitem.getHref().length() > 1)) {
	                href = hisitem.getHref();
	            }
	            else {
	                href = " ";
	            }
	
	            if ((hisitem.getRefName() != null) && (hisitem.getRefName().length() > 1)) {
	                refname = hisitem.getRefName();
	            }
	            else {
	                refname = " ";
	            }
	
	            if ((hisitem.getRefNumber() != null) && (hisitem.getRefNumber().length() > 1)) {
	                refnumber = hisitem.getRefNumber();
	            }
	            else {
	                refnumber = " ";
	            }
	
	            if ((hisitem.getDate() != null) && (hisitem.getDate().length() > 1)) {
	                docdate = hisitem.getDate();
	            } 
	            else {
	                docdate = " ";
	            }
	
	            // Delete the spaces out of the hrefparameter String
	            if ((hisitem.getHrefParameter() != null) && (hisitem.getHrefParameter().length() > 1)) {
	
	                StringTokenizer token = new StringTokenizer(hisitem.getHrefParameter());
	                hrefparameter="";
	                while (token.hasMoreElements()) {
	                    result = token.nextToken();
	                    hrefparameter = hrefparameter.concat(result);
	                }
	            }
	            else {
	                hrefparameter = " ";
	            }
	
	            // Concatenate the attributes of the item in the itemvalue and add the itemvalue to the itemlist
	
	            // Because there are not all characters allowed in a cookie content string,
	            // we must encode the attributes of an history item.
	            encdocnumber = JspUtil.encodeURL(docnumber);
	            encdoctype = JspUtil.encodeURL(doctype);
	            enchref = JspUtil.encodeURL(href);
	            enchrefparameter = JspUtil.encodeURL(hrefparameter);
	            encrefname = JspUtil.encodeURL(refname);
	            encrefnumber = JspUtil.encodeURL(refnumber);
	            encdocdate = JspUtil.encodeURL(docdate);
	
	            // Escape the special characters used for seperating the attributes
	            encdocnumber = JspUtil.escapeCharacter(encdocnumber, attributedelimiter);
	            encdoctype = JspUtil.escapeCharacter(encdoctype, attributedelimiter);
	            enchref = JspUtil.escapeCharacter(enchref, attributedelimiter);
	            enchrefparameter = JspUtil.escapeCharacter(enchrefparameter, attributedelimiter);
	            encrefname = JspUtil.escapeCharacter(encrefname, attributedelimiter);
	            encrefnumber = JspUtil.escapeCharacter(encrefnumber, attributedelimiter);
	            encdocdate = JspUtil.escapeCharacter(encdocdate, attributedelimiter);
	
	            // Escape the special characters used for seperating the items of the list
	            docnumber = JspUtil.escapeCharacter(encdocnumber, itemdelimiter);
	            doctype = JspUtil.escapeCharacter(encdoctype, itemdelimiter);
	            href = JspUtil.escapeCharacter(enchref, itemdelimiter);
	            hrefparameter = JspUtil.escapeCharacter(enchrefparameter, itemdelimiter);
	            refname = JspUtil.escapeCharacter(encrefname, itemdelimiter);
	            refnumber = JspUtil.escapeCharacter(encrefnumber, itemdelimiter);
	            docdate = JspUtil.escapeCharacter(encdocdate, itemdelimiter);
	
	            // build the string for one historyiten
	            itemvalue = docnumber + attributedelimiter
	                     + doctype + attributedelimiter
	                     + href + attributedelimiter
	                     + hrefparameter + attributedelimiter
	                     + refname + attributedelimiter
	                     + refnumber + attributedelimiter
	                     + docdate + itemdelimiter;
	
	            // build the cookie content string
	            listofitems = listofitems.concat(itemvalue);
	        } // end for loop of items
	
	        // remove the last item seperator.
	        listofitems = listofitems.substring(0, listofitems.length() - 1);
	
	
			// find cookiePath to set from request object 
			String reqURI = request.getRequestURI();
			if (reqURI == null || reqURI.equals("")) {
				if (isDebugEnabled) {
	              log.debug("trying to find requestURI for cookie path - requestURI null or empty: no cookie path can be specified.");
	              log.debug("=> PATH parameter of cookie not set; default value set by J2EE engine.");
	            }
			}
			else {
			    int index = reqURI.lastIndexOf("/");
			    reqURI = reqURI.substring(0,index+1);
			    // set cookie path - cookie only valid for the subpath '/b2b/history' of the requesting servlet
			    cookie.setPath(reqURI);
			}	
	        
	        // set the attributes of the cookie and put it the response context
	        cookie.setValue(listofitems);
	        cookie.setMaxAge(COOKIE_MAX_AGE);
	
	        response.addCookie(cookie);
	
	        if (isDebugEnabled) {
	            log.debug("save(): Saved the cookie - everything seems ok");
	        }
	        return true;
        } finally {
        	log.exiting();
        }
  
    } // end of function

} // end of class
