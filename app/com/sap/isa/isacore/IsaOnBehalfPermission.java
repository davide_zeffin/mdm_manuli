/*
 * Created on 21.06.2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.isacore;

import com.sap.isa.user.permission.ECoValuePermission;

/**
 * This class provides the checks for the Onbehalf working on documents
 * We need a ValuePermission class because the ActionPermisison  for the
 * same document types are covered by the normal user permission checks.
 * This class is mapped to a different backend authorization object
 * in the <@link>Permission-Config.xml</link> file. (For CRM its 'ECO_DOC_BH'
 */
public class IsaOnBehalfPermission extends ECoValuePermission {

	/**
	 * @param doc_type The documenty type in the Java world
 	 * @param action The kind of action permission required
	 */
	public IsaOnBehalfPermission(String doc_type, String action) {
		super(doc_type, action);		
	}

}
