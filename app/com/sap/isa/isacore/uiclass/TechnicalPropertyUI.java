/*****************************************************************************
    Class:        TechnicalPropertyUI
    Copyright (c) SAP AG
    Author:       D023061
    Created:      Januray 2006
    Version:      1.0

*****************************************************************************/
package com.sap.isa.isacore.uiclass;

import java.util.Iterator;

import javax.servlet.jsp.PageContext;

import com.sap.isa.businessobject.item.ItemBase;
import com.sap.isa.businessobject.order.TechnicalProperty;
import com.sap.isa.businessobject.order.TechnicalPropertyMaintenanceObject;
import com.sap.isa.ui.uiclass.MaintenanceObjectUI;

/**
 * The class describes a technical property UI object.
 *
 * @author D023061
 * @version 1.0
 */

public class TechnicalPropertyUI extends MaintenanceObjectUI {

    /**
    * ensures that the attribute the MaintenanceObject)is set.
    * 
    */
    protected void determineMaintenanceObject() {
        //setObject is used instead
    }

    public TechnicalPropertyUI(PageContext pageContext) {
        initContext(pageContext);
    }

    public TechnicalPropertyUI() {
        super();
    }

    /**
    * ensures that the readOnly Flag is set. The Flag depens on current used JSP.
    * 
    */
    protected void determineReadOnly() {
        //  --> setReadOnly is used instead

    }

    /**
    * ensures that the attribute formName is set.
    * 
    */
    protected void determineFormName() {
        // setForNname is used  instead

    }

    /* (non-Javadoc)
     * @see com.sap.isa.ui.uiclass.MaintenanceObjectUI#determineHelpAction()
     */
    protected void determineHelpAction() {

    }

    /**
    * Sets the MaintenanceObject for the current item.
    * @param ItemBase item
    */
    public void setObject(ItemBase item) {

        // create maintenance Object for TechnicalPropertyGroup
        TechnicalPropertyMaintenanceObject maintenanceObject = new TechnicalPropertyMaintenanceObject();

        // add assigned PropertyGroup of the item to the maintemance Object
        maintenanceObject.addPropertyGroup(item.getAssignedTechnicalProperties());

        object = maintenanceObject;

    }

    /**
    * Sets all proprties of the maintenaceObject to OutputOnly.
    * Boolen will remain as boolean Type
    * @param ItemBase item
    */
    public void setAllOutputOnly() {

        Iterator it = this.object.propertyIterator();

        while (it.hasNext()) {
            TechnicalProperty prop = (TechnicalProperty) it.next();
            if (!prop.getType().equals("Boolean")) {
                prop.setReadOnly(true);
            }
        }
    }

    public boolean isTechnicalDataAssigned(ItemBase item) {

        return (item.getAssignedTechnicalProperties().getNumberOfVisibleProperty() > 0);
    }

}
