package com.sap.isa.isacore.uiclass;

 
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.jsp.PageContext;

import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.util.MessageList;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.action.PaymentActions;
import com.sap.isa.isacore.action.order.DocumentStatusOrderQuotAction;
import com.sap.isa.maintenanceobject.businessobject.UIElementBase;
import com.sap.isa.payment.backend.boi.PaymentBaseData;
import com.sap.isa.payment.backend.boi.PaymentBaseTypeData;
import com.sap.isa.payment.backend.boi.PaymentCCardData;
import com.sap.isa.payment.backend.boi.PaymentMethodData;
import com.sap.isa.payment.businessobject.PaymentBaseType;
import com.sap.isa.payment.businessobject.PaymentCCard;
import com.sap.isa.payment.businessobject.PaymentMethod;
import com.sap.isa.ui.uicontrol.UIController;

/**
 * UI class to handle payment data on a JSP. <br>
 * 
 * 
 * <strong>Note</strong> A lot of information is available as public properties
 * of the class.
 *
 * @author SAP
 * @version 1.0
 **/
public class PaymentUI extends IsaBaseUI {

	private static final int NEW_LINES = 3;
	private static final int ONE_LINE  = 1;

	// JSP-UI elements
	public static final String PAYMENT_TYPE     = "payment.type";
	// multiple cards
	public static final String CARD_TYPE        = "payment.card.type";
	public static final String CARD_NUMBER      = "payment.card.number";
	public static final String CARD_HOLDER      = "payment.card.holder";
	public static final String CARD_CVV         = "payment.card.cvv";
	public static final String CARD_SUFFIX      = "payment.card.number.suffix";
	public static final String CARD_VALIDITY    = "payment.card.validity";
	public static final String CARD_MONTH       = "payment.card.expd.month";
	public static final String CARD_YEAR        = "payment.card.expd.year";
	public static final String CARD_LIMITFLAG   = "payment.card.limit.flag";
	public static final String CARD_LIMITAMOUNT = "payment.card.limit.amount";
	public static final String CARD_DELETEFLAG  = "payment.card.chckb.delete";
	public static final String CARD_AUTHSTATUS  = "payment.card.auth.status";
	// one card
	public static final String ONECARD_TYPE        = "payment.one.card.type";
	public static final String ONECARD_NUMBER      = "payment.one.card.number";
	public static final String ONECARD_HOLDER      = "payment.one.card.holder";
	public static final String ONECARD_CVV         = "payment.one.card.cvv";
	public static final String ONECARD_SUFFIX      = "payment.one.card.suffix";
	public static final String ONECARD_VALIDITY    = "payment.one.card.validity";
	public static final String ONECARD_MONTH       = "payment.one.card.expd.month";
	public static final String ONECARD_YEAR        = "payment.one.card.expd.year";
	public static final String ONECARD_LIMITFLAG   = "payment.one.card.limit.flag";
	public static final String ONECARD_LIMITAMOUNT = "payment.one.card.limit.amount";
	public static final String ONECARD_DELETEFLAG  = "payment.one.card.chckb.delete";
	public static final String ONECARD_AUTHSTATUS  = "payment.one.card.auth.status";

  
	public PaymentBaseData payment;
	public PaymentCCardData currentPaymentCard;
	public PaymentBaseTypeData paymentTypes;
	public int line;
	public int oneLine = 0;

	public boolean oneCard = false;
	public boolean multipleCards = false;	
	public boolean enableMaintenance = false;
    public boolean directPaymentAllowed = false;
	 
	/**
	 * Reference to list of message, whic are assigned to the document. <br>  
	 */
	public MessageList docMessages;
	
	/**
	 * String, which contains all doc messages in one string. <br>
	 * See {@link # getMessageString} for details.
	 */
	public String docMessageString = "";
	
	/**
	 * Separator to separate message concatenate by the {@link # getMessageString} 
	 * method. <br> The default value is &lt;br&gt;.
	 */	
	protected String messageSeparator = "<br>";	
	
	protected String numberOfCards;
	protected boolean disableAll = false;
	protected boolean backendR3   = false;
	protected boolean backendR3PI = false;	
	protected boolean showCVVAsStars = false;  
	
	protected HeaderSalesDocument header;
	protected SalesDocument       salesDoc;
	 
 
	/**
	 * Constructor for OrderUI.
	 *
	 * @param request
	 */
	public PaymentUI(PageContext pageContext) {

		super(pageContext);
		
	    
		// if the number is set to visible in the UIControll 
		// it will be assumed that the payment maintenance
		// is available!
		UIController uiController = UIController.getController(userSessionData);
		UIElementBase multipleCardsElements = uiController.getUIElement("payment.card.number");
		UIElementBase oneCardElements = uiController.getUIElement("payment.one.card.number");
	
		if (multipleCardsElements != null && !multipleCardsElements.isHidden()) {
		   numberOfCards = PaymentActions.MULTIPLE_CARDS;
		   multipleCards = true;
		   oneCard = false;
		} else if (oneCardElements != null && !oneCardElements.isHidden()) {
			numberOfCards = PaymentActions.ONE_CARD;
			multipleCards = false;
			oneCard = true;
		}
		
		 	       
		paymentTypes = (PaymentBaseTypeData) request.getAttribute(PaymentActions.PAYMENT_TYPES);   
		payment = (PaymentBaseData) request.getAttribute(PaymentActions.PAYMENT); 
		line = 0;
	    
		determineMessages();
		docMessageString = getMessageString(docMessages);
	    
	    
		String shopBackend = shop.getBackend();

		backendR3PI = shopBackend.equals(Shop.BACKEND_R3_PI);

		backendR3 = shopBackend.equals(Shop.BACKEND_R3) || shopBackend.equals(Shop.BACKEND_R3_40) ||
							shopBackend.equals(Shop.BACKEND_R3_45) || shopBackend.equals(Shop.BACKEND_R3_46) ||
							backendR3PI;
		
		directPaymentAllowed = shop.isDirectPaymentMaintenanceAllowed();
		
							
		if (paymentTypes != null) {
		  enableMaintenance = (oneCard || multipleCards) && (paymentTypes.isCardAvailable() || paymentTypes.isCODAvailable());	 				
		}
	}

	public PaymentUI(PageContext pageContext, boolean disableAll) {
		this(pageContext);
		this.disableAll = disableAll;
	}


	public PaymentUI(PageContext pageContext, HeaderSalesDocument header, boolean disableAll) {
		this(pageContext);
		initPaymentUI(header);
		this.disableAll = disableAll;   
	}

	public PaymentUI(PageContext pageContext, HeaderSalesDocument header) {
		this(pageContext, header, false);
		this.header = header;
	}
    
	public PaymentUI(PageContext pageContext, SalesDocument salesDoc) {
		this(pageContext, salesDoc.getHeader(), false);
		this.header = salesDoc.getHeader();
		this.salesDoc = salesDoc;
	}
	
	public void initPaymentUI(HeaderSalesDocument header) {
		this.header = header;		
	}
	
	/**
	 * Determine the Messages which should be displayed. <br>
	 * The messages are taken from the request parameter 
	 * {@link com.sap.isa.isacore.action.order.MaintainOrderBaseAction.RC_MESSAGES}
	 * 
	 */
	protected void determineMessages() {
		this.docMessages = (MessageList) request.getAttribute(PaymentActions.DOC_MESSAGES);
	}
	
		
	/**
	 * Build one string from the given messagelist. <br>
	 * The message will be concatenated and separated with the {@link #messageSeparator}.
	 * 
	 * @param  messageList list with message.
	 * @return All message concatenated and separated with the {@link #messageSeparator}
	 *  
	 */
	protected String getMessageString(MessageList messageList) {
        
		String messageString = "";
		
		if (messageList != null) {
			for (int i = 0; i < messageList.size(); i++) {
				messageString += messageList.get(i).getDescription();
				if (i < messageList.size() - 1) {
					messageString += messageSeparator;
				}
			}
		}
        
		return messageString;
	}	
	
	/**
	 * 
	 */
	public void setPaymentCard(PaymentCCardData card) {        
    
	   this.currentPaymentCard = card;
    
	}
	
	/**
	 * 
	 */ 
	public int addLine() {
		return line++;
	}

	/**
	 * 
	 *
	 */
	public void initLine() {
	   this.line = 0;
	}

	/**
	 * 
	 */
	public int getNewLines() {
		
		int newLines; 
		
		if (request.getAttribute(PaymentActions.NEWLINES) != null) {
		  newLines = Integer.parseInt(request.getAttribute(PaymentActions.NEWLINES).toString());
		} else {
		  if (multipleCards) {
			newLines = NEW_LINES;
		  } else {
			newLines = ONE_LINE;  
		  }
		}
		
		request.setAttribute(PaymentActions.NEWLINES, Integer.toString(newLines));
		
		return newLines;
	}	    								
		
	public int getMinThresholdNewLines() {	
	List payCards = new ArrayList();	
	   if (payment != null && payment.getPaymentMethods() != null && payment.getPaymentMethods().size() > 0) {
		List paymentMethods = payment.getPaymentMethods();
		Iterator iter = paymentMethods.iterator();			
			while (iter.hasNext()){					
				PaymentMethod payMethod = (PaymentMethod) iter.next();			
				if (payMethod != null){
				// if the payment method is Credit card, fill the import table accordingly
					if(payMethod.getPayTypeTechKey().equals(PaymentBaseTypeData.CARD_TECHKEY )) {                    		
						PaymentCCard payCCard = (PaymentCCard) payMethod;							
						payCards.add(payCCard);
					}
				}
			}
	   }
	   if (payCards.size() > 0)	{	   	 	   	 	   
		 return payCards.size();	   	 
	   } else {
		 return this.line;
	   }
	}
	
	
	public int getMaxThresholdNewLines() {
		List payCards = new ArrayList();    	
		if (payment != null && payment.getPaymentMethods() != null && payment.getPaymentMethods().size() > 0) {
			List paymentMethods = payment.getPaymentMethods();
			Iterator iter = paymentMethods.iterator();		   		
			while (iter.hasNext()){					
			   PaymentMethod payMethod = (PaymentMethod) iter.next();			
			   if (payMethod != null){
			   // if the payment method is Credit card, fill the import table accordingly
				   if(payMethod.getPayTypeTechKey().equals(PaymentBaseTypeData.CARD_TECHKEY )) {                    		
					   PaymentCCard payCCard = (PaymentCCard) payMethod;							
					   payCards.add(payCCard);
				   }
			   }
			}
		}
		if (payCards.size() > 0) {  
		  return (payCards.size() + getNewLines());
		} else {
		  return getNewLines();									
		}
	}
    
    
    
	/**
	 * 
	 */
	public String getCardTypeSelected(String cardType) {
	   String selected = "";
		
	   if (multipleCards) {	
		 if (cardType.equals(currentPaymentCard.getTypeTechKey().getIdAsString())) {
		   selected = "selected='selected'";
		  } 
	   } else if (oneCard) {
			if (payment != null && payment.getPaymentMethods() != null && payment.getPaymentMethods().size() > 0) {
				if (((PaymentMethodData) (payment.getPaymentMethods().get(0))) instanceof PaymentCCard) {
					PaymentCCardData payCard = (PaymentCCard) (payment.getPaymentMethods().get(0));				
					if (payCard.getTypeTechKey().getIdAsString().equals(cardType)) {		 	
						selected = "selected='selected'";
					}
				} 	    
			}
	   } 	
		return selected;    
	}

	/**
	 * 
	 */
	public String getCardMonthSelected(String cardMonth) {
		String selected = "";
    	
		if (multipleCards) {
		  if (cardMonth.equals(currentPaymentCard.getExpDateMonth())) {
			selected = "selected='selected'";
		  }
		} else if (oneCard) {
			if (payment != null && payment.getPaymentMethods() != null && payment.getPaymentMethods().size() > 0 ) {
				if (((PaymentMethodData) (payment.getPaymentMethods().get(0))) instanceof PaymentCCard) {
					PaymentCCardData payCard = (PaymentCCard) (payment.getPaymentMethods().get(0));				
					if (payCard.getExpDateMonth().equals(cardMonth)) {		 	
						selected = "selected='selected'";
					}
				} 	    
			}    		  
		}    	
		return selected;
	}
    
	/**
	 * 
	 */
	public void resetSalesDocLastEnteredCVVS() {
		if (salesDoc != null) {
			salesDoc.getLastEnteredCVVS().clear();
			log.debug("LastEntered CVSS of salesDoc reset");
		}
		else {
			log.debug("Could not reset LastEntered CVSS no salesDoc available");
		}
	}
    
	/**
	 * 
	 */
	public void addSalesDocLastEnteredCVVS(int idx, String curVCC) {
		if (salesDoc != null) {
			salesDoc.getLastEnteredCVVS().add(idx, curVCC);
			log.debug("LastEntered CVSS added");
		}
		else {
			log.debug("Could not add new LastEntered CVSS no salesDoc available");
		}
	}
       
	/**
	 * 
	 */
	public String getCardYearSelected(String cardYear) {
		String selected = "";
    	
		if (multipleCards) {
		  if (cardYear.equals(currentPaymentCard.getExpDateYear())) {
			selected = "selected='selected'";
		  }
		} else if (oneCard) {
			if (payment != null && payment.getPaymentMethods() != null && payment.getPaymentMethods().size() > 0) {
				if (((PaymentMethodData) (payment.getPaymentMethods().get(0))) instanceof PaymentCCard) {
					PaymentCCardData payCard = (PaymentCCard) (payment.getPaymentMethods().get(0));				
					if (payCard.getExpDateYear().equals(cardYear)) {		 	
						selected = "selected='selected'";
					}
				} 	    
			}    		  
		}    	
		return selected;
	}    
     	 
     	  		
	/**
	 * 
	 */
	public void disableAll() {
		this.disableAll = true;
	}
    
    
	/**
	 * 
	 */
	public void enableAll() {
		this.disableAll = false;
	}
    	  
   
	/**
	 * 
	 */ 	  
	public String getAllDisabled() {
	  String disabled = "";
      
	  if (disableAll) {
		disabled = "disabled='disabled'";
	  }
      
	  return disabled;
	}  
       
      
	/**
	 * 
	 */ 	  		
	public String getFieldDisabled (String elementName) { 	
		String disabled ="";
        
		if (disableAll) {
		   disabled = "disabled='disabled'";
		} else {
		  if ((elementName.equals("nolog_cardno") ||
			  elementName.equals("nolog_cardtype"))) {
			disabled = "disabled='disabled'"; 		
		 } else if (elementName.equals("authlimited") ||
			 elementName.equals("authlimit")) {
		   disabled = "";   
		 } else if (elementName.equals("deleteflag") &&
					currentPaymentCard != null &&
					currentPaymentCard.getAuthStatus() != null &&
					currentPaymentCard.getAuthStatus().equals(PaymentCCardData.AUTH_USED)) {
			disabled = "disabled='disabled'";       	  
		 } else if (elementName.equals("deleteflag")) {
			disabled = "";	       	  
		 } else {
		  if (currentPaymentCard != null &&
			   currentPaymentCard.getAuthStatus() != null &&
			  (currentPaymentCard.getAuthStatus().equals(PaymentCCardData.AUTH_USED) ||
			   currentPaymentCard.getAuthStatus().equals(PaymentCCardData.AUTH_EXIST)||
			   currentPaymentCard.getAuthStatus().equals(PaymentCCardData.AUTH_ERROR))) {
			disabled = "disabled='disabled'";  
		  }		
		 }		
		}
        
		return disabled;	
	}
    
	public void setAllDisabledByPayType () {
		boolean enable = false;
		if (paymentTypes != null) {
			if (payment != null && payment.getPaymentMethods() != null && payment.getPaymentMethods().size() > 0) {
				List paymentMethods = payment.getPaymentMethods();
				Iterator iter = paymentMethods.iterator();		   		
				while (iter.hasNext() && (enable == false)){					
					PaymentMethod payMethod = (PaymentMethod) iter.next();			
					if (payMethod != null){
					// if the payment method is Credit card, fill the import table accordingly
						if(payMethod.getPayTypeTechKey().equals(PaymentBaseTypeData.CARD_TECHKEY )) {                    		
							enable = true;															
						}
					}
				}
				if (enable == true)
					enableAll();
				else 
					disableAll();
			} else {
				if (paymentTypes.getDefault().equals(PaymentBaseTypeData.CARD_TECHKEY)) 
					enableAll();     
				 else
					disableAll();
		   }
		 } else {
			enableAll();
		 }		  
	}

	public void setAllDisabledIfLargeDocumentCommands () {
		
		if (isOrderLargeDocument() && !isHeaderChangeInLargeDocument()) {
		  disableAll();	
		 } 		  
	}    
       
	public boolean isMaintainable(String elementName) {
		boolean maintainable = true;
        
	  if (multipleCards) {
		 if (elementName.equals(CARD_LIMITFLAG) ||
			 elementName.equals(CARD_LIMITAMOUNT)) {
		   maintainable = true;   
		 } else if (elementName.equals(CARD_DELETEFLAG) &&
					currentPaymentCard != null &&
					currentPaymentCard.getAuthStatus() != null &&
					currentPaymentCard.getAuthStatus().equals(PaymentCCardData.AUTH_USED)) {
			maintainable = false;
		 } else if (elementName.equals(CARD_DELETEFLAG)) {
			maintainable = true;       	  
		 } else {
		  if (currentPaymentCard != null &&
			  currentPaymentCard.getAuthStatus() != null &&
			  (currentPaymentCard.getAuthStatus().equals(PaymentCCardData.AUTH_USED) ||
			  currentPaymentCard.getAuthStatus().equals(PaymentCCardData.AUTH_EXIST) ||
			  currentPaymentCard.getAuthStatus().equals(PaymentCCardData.AUTH_ERROR))) {
			maintainable = false;  
		  }		
		 }		
	  }
         
	  return maintainable;	    
    
	}
	
    /**
     * this method was implemented during Lean Order development
     * the limit amount field in new lines for credit cards in LORD case
     * should be always disabled
     */
	public String getFieldReadOnlyNewLine (String elementName) {
		String readonly = "";
		if (multipleCards) {
			if (elementName.equals(CARD_LIMITFLAG) ||
				elementName.equals(CARD_LIMITAMOUNT) && (shop.isDirectPaymentMaintenanceAllowed() && backendR3PI )) { 
				readonly = "readonly='readonly'";
			} else {
				getFieldReadOnly(elementName);
			}
		} 
		return readonly;    	    	
	}
	
	public String getFieldReadOnly(String elementName) {
		String readonly ="";
        
	  if (multipleCards) {  
		if (elementName.equals(CARD_LIMITFLAG) ||
			elementName.equals(CARD_LIMITAMOUNT)) {
			readonly = "";   
		 } else if (elementName.equals(CARD_DELETEFLAG) &&
					currentPaymentCard != null &&
					currentPaymentCard.getAuthStatus() != null &&
					currentPaymentCard.getAuthStatus().equals(PaymentCCardData.AUTH_USED)) {
			readonly = "readonly='readonly'";  
		 } else if	(elementName.equals(CARD_DELETEFLAG)) {
			readonly = "";       	  
		 } else {
		  if (currentPaymentCard != null &&
			  currentPaymentCard.getAuthStatus() != null &&
			  (currentPaymentCard.getAuthStatus().equals(PaymentCCardData.AUTH_USED) ||
			   currentPaymentCard.getAuthStatus().equals(PaymentCCardData.AUTH_EXIST) ||
			   currentPaymentCard.getAuthStatus().equals(PaymentCCardData.AUTH_ERROR))) {
			readonly = "readonly='readonly'";  
		  }		
		 }		
	  } 
	  
	  return readonly;	    	
	}
    
    
	/**
	 * 
	 * @return
	 */
	public boolean paymentCardsExist() {
	  boolean card = false;
	  if (payment == null) {
		return false;
	  } else if (payment != null) {
		List paymentMethods = payment.getPaymentMethods();
		if (paymentMethods != null &&  paymentMethods.size() > 0) {		
			Iterator iter = paymentMethods.iterator();
			if (paymentMethods != null)	{	   		
				while (iter.hasNext() && (card == false)){					
					PaymentMethod payMethod = (PaymentMethod) iter.next();			
					if (payMethod != null){
					// if the payment method is Credit card, fill the import table accordingly
						if(payMethod.getPayTypeTechKey().equals(PaymentBaseTypeData.CARD_TECHKEY )) {                    		
							card = true;															
						}
					}
				}
			}
	  	}
	  }	
	  if (card == true) {          
	   return true;
	  } else {
	   return false;
	  }
                        	
	}
    
    
//	/**
//	 * 
//	 * @return
//	 */
//	public boolean authErrorExist() {
//    	
//		if (payment == null) {
//		  return false;	
//		} else {
//		  return payment.getCardAuthError();
//		} 
//    	
//	}


	public void initTypes(boolean invoice, boolean COD, boolean card, TechKey defaultType) {
    	
		if (paymentTypes == null) {
		  paymentTypes = new PaymentBaseType();
    	  
		}  
		setInvoiceAvailable(invoice);
		setCODAvailable(COD);
		setCardAvailable(card);
		paymentTypes.setDefault(defaultType);
	}

	public void setInvoiceAvailable(boolean bValue) {
		if (paymentTypes != null) {
		  paymentTypes.setCardAvailable(bValue);
		}
	}

	public void setCODAvailable(boolean bValue) {
		if (paymentTypes != null) {
		  paymentTypes.setCODAvailable(bValue);
		}
	}

	public void setCardAvailable(boolean bValue) {
		if (paymentTypes != null) {
		  paymentTypes.setCardAvailable(bValue);	
		}
	}

	public boolean isInvoiceAvailable() {
		if (paymentTypes != null) {
		  return paymentTypes.isInvoiceAvailable();
		} else {
		  return false;
		}
	}
 
    
	public boolean isCODAvailable() {
	   if (paymentTypes != null) {
		 return paymentTypes.isCODAvailable();
	   } else {
		 return false;
	   }
	}
 
    
	public boolean isCardAvailable() {
	   if (paymentTypes != null) {
		  return paymentTypes.isCardAvailable();
	   } else {
		  return false;
	   }
	}
 
    
	public TechKey getDefaultType() {
		if (paymentTypes != null) {
		  return paymentTypes.getDefault();	
		} else {
		  return null;}
	}
 
    	  	
	public int getNumberOfTypes() {
	   int number = 0;
       
	   if (isInvoiceAvailable()) {
		  number++;
	   }
	   if (isCODAvailable()) {
		  number++;
	   }
	   if (isCardAvailable()) {
		  number++;	  	  	  	  		     
	   }
       
	   return number;
	}
    
    
	 public PaymentBaseTypeData getTypes() {
		return paymentTypes;
	 }
     
	 public String getInvoiceTypeAsString() {
		 if (paymentTypes != null) {
		   return getInvoiceType().getIdAsString();
		 } else {
		   return "";
		 }
	 }
     
	 public String getCODTypeAsString() {
		 if (paymentTypes != null) {
		   return getCODType().getIdAsString();
		 } else {
		   return "";
		 }
	 }
     
	public String getCardTypeAsString() {
	   if (paymentTypes != null) {
		 return getCardType().getIdAsString();
	   } else {
		 return "";
	   }  
	} 
	         
	 public TechKey getCardType() {
		if (paymentTypes != null) {
		  return paymentTypes.getCard();
		} else {
		  return null;
		}  
	 }
     
	public TechKey getInvoiceType() {
		if (paymentTypes != null) {
		  return paymentTypes.getInvoice();
		} else {
		  return null;
		}
	}
     
	public TechKey getCODType() {
		if (paymentTypes != null) {
		  return paymentTypes.getCOD();
		} else {
		  return null;
		}
	}
     
	public String getChecked(TechKey payType) {

	  if (paymentTypes != null) {
		if (payment != null && payment.getPaymentMethods() != null && payment.getPaymentMethods().size() > 0 && payment.getPaymentMethods().get(0) != null) {
		  if (payType.equals(((PaymentMethodData)(payment.getPaymentMethods().get(0))).getPayTypeTechKey())) {
			return "checked='checked'";
		  } else { 		  
			return "";
		  }	
		} else {
		  if (paymentTypes.getDefault().equals(payType)) 
			return "checked='checked'";     
		  else
			return "";
		}
	  } else {
		return "";
	  }
	}
    
	public int getNumberOfLines() {
		List payCard = new ArrayList();
		if (payment != null && payment.getPaymentMethods() != null && (payment.getPaymentMethods().size() > 0)) {
			List paymentMethods = payment.getPaymentMethods();
			Iterator iter = paymentMethods.iterator();
			if (paymentMethods != null)	{	   		
				while (iter.hasNext()){					
					PaymentMethod payMethod = (PaymentMethod) iter.next();			
					if (payMethod != null){
					// if the payment method is Credit card, fill the import table accordingly
						if(payMethod instanceof PaymentCCard) {
							payCard.add(payMethod);
						}
					}
				}
			}
		}	        
		if (payCard != null && payCard.size() > 0) {
		  return (payCard.size() + getNewLines());
		} else {
		  return getNewLines();									
		}      
	}
       
	/**
	 * Return Set the property {@link #backendR3}. <br>
	 * 
	 * @return {@link #backendR3}
	 *  
	 */
	public boolean isBackendR3() {
		
		// Workaround since the UIControl is 
		// not flexible as needed and
		// there is not payment maintenance for 
		// R3 in the OrderChange but in BasketCreate
		// Otherwise additional fields in Control 
		// needed.
		if (backendR3 || backendR3PI) {
		  return true;
		}
		return false;
	}      
	

   public String getYearShort(String year) {
       
	   if (year.length() == 4) {
		 StringBuffer yearBuffer = new StringBuffer(year);
		 return yearBuffer.substring(2);
	   } else {
		return year;
	   }
   
   }
   
   
   /**
	* Convert the amount value from internal format
	* ####.## to the decimal point format valid for 
	* the shop.
	* 
	* @param amount value in internal format
	* @return amount value in external format
	*/
   public String convertToOutputFormat(String amount) {
     
	 if (shop != null) {
	   NumberFormat outputFormat = shop.getNumberFormat();
	   String test = outputFormat.format(amount);
	   return amount.replace('.', shop.getDecimalSeparator().charAt(0));
	 } 
     
	 return amount;
   }
   
   
   /**
	* Returns the current payment process to distinguish 
	* the order button text in the payment maintenance
	* 
	* @return payment process as string
	*/
   public String getPaymentProcess() {
   	   
  	   
	   if (request.getAttribute(PaymentActions.PAYMENT_PROCESS) != null) {
		return request.getAttribute(PaymentActions.PAYMENT_PROCESS).toString();   
	   } else if (userSessionData.getAttribute(PaymentActions.PAYMENT_PROCESS) != null) {
		return userSessionData.getAttribute(PaymentActions.PAYMENT_PROCESS).toString();
	   } else {
		return  PaymentActions.BASKET_CREATION;
	   }   
   }
   
   /**
	* Returns if the current payment process
	* is order simulation
	* 
	* @return true if current payment process is
	*         order simulation
	*/
   public boolean isOrderSimulation() {
	  if (getPaymentProcess().equals(PaymentActions.BASKET_SIMULATION)) 
		return true;
	  else
		return false;
   }
   
   /**
	* Returns if the current payment process
	* is quotation ordering
	* 
	* @return true if current payment process is
	*         quotation ordering
	*/
   public boolean isQuotationOrdering() {
	  if (getPaymentProcess().equals(PaymentActions.QUOT_ORDERING)) 
		return true;
	  else
		return false;
   }
   
   
   public HeaderSalesDocument getHeader() {
	 HeaderSalesDocument header = null;
	 
	 if (isOrderSimulation()) {
	   SalesDocument order = bom.getOrder();
	   if (order != null) {
		header = order.getHeader();
	   } 
	 } else if (isQuotationOrdering()) {  
		SalesDocument order = bom.getOrder(DocumentStatusOrderQuotAction.ORDER_INDEX);
		if (order != null) {
		  header = order.getHeader();
		}
	 } else {
		SalesDocument order = bom.getOrder();
		if (order != null) {
		 header = order.getHeader();
		} 
	 }
   	 
	 return header;
   }
   
   public boolean isGrossValueAvailable() {
   	
	if (isOrderSimulation()) {
	  SalesDocument order = bom.getOrder();
	  if (order != null) {
	   return order.isGrossValueAvailable();
	  } 
	  else {
	   return false;
	  }
	} else if (isQuotationOrdering()) {  
	   SalesDocument order = bom.getOrder(DocumentStatusOrderQuotAction.ORDER_INDEX);
	   if (order != null) {
		 return order.isGrossValueAvailable();
	   }
	   else {
		return false;
	   }
	 }
	 
	 return false; 
   }
   
   
   public boolean isManagedDocType() {
   	   
	   DocumentHandler documentHandler = (DocumentHandler) userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
	   if (documentHandler != null ) {	
		  ManagedDocument mdoc = documentHandler.getManagedDocumentOnTop();
		  if (mdoc != null) {
			 String docType = removeNull(mdoc.getDocType());
			if ( ( (docType.equals(HeaderData.DOCUMENT_TYPE_BASKET) ||             
				   docType.indexOf(HeaderData.DOCUMENT_TYPE_BASKET) == 0) &&      
				   ! shop.isDocTypeLateDecision() ) ||
				  docType.equals(HeaderData.DOCUMENT_TYPE_ORDER)) {

			  return true;	
			} else {
			  return false; 		  	
			}
		  } else {
		   return false;		
		  } 
	   } else {
		return false;
	   }
	   	
   }
   
   
   public boolean isSalesDocValid() {
   	
	if (isOrderSimulation()) {
	  SalesDocument order = bom.getOrder();
	  if (order != null) {
	   return order.isValid();
	  } 
	  else {
	   return false;
	  }
	} else if (isQuotationOrdering()) {  
	   SalesDocument order = bom.getOrder(DocumentStatusOrderQuotAction.ORDER_INDEX);
	   if (order != null) {
		 return order.isValid();
	   }
	   else {
		return false;
	   }
	 }
	 
	 return false; 
   }
  
   
   public boolean isOrderLargeDocument() {
   
	  if (isQuotationOrdering()) {  
	   SalesDocument order = bom.getOrder(DocumentStatusOrderQuotAction.ORDER_INDEX);
	   if (order != null) {
		 return order.isLargeDocument(bom.getShop().getLargeDocNoOfItemsThreshold());
	   }
	   else {
		return false;
	   }
	  } else {
	   SalesDocument order = bom.getOrder();
	   if (order != null) {
		return order.isLargeDocument(bom.getShop().getLargeDocNoOfItemsThreshold())  ;
	   } 
	   else {
		return false;
	   }
	  } 
        
   }
 
   
   public boolean isOrderHeaderChangeable() {
   
	HeaderSalesDocument header = getHeader();
	if (header != null){
	  return header.isChangeable();	 
	}
	else {
	  return false;
	}
   
  }  
   
   public boolean isHeaderChangeInLargeDocument() {
   	
	if (isQuotationOrdering()) {  
	 SalesDocument order = bom.getOrder(DocumentStatusOrderQuotAction.ORDER_INDEX);
	 if (order != null) {
	   return order.isHeaderChangeInLargeDocument(bom.getShop().getLargeDocNoOfItemsThreshold());
	 }
	 else {
	  return false;
	 }
	} else {
	 SalesDocument order = bom.getOrder();
	 if (order != null) {
	  return order.isHeaderChangeInLargeDocument(bom.getShop().getLargeDocNoOfItemsThreshold());
	 } 
	 else {
	  return false;
	 }
	}   	
   }
   
   public boolean isPaymentMaintenanceAvailable() {
		
	if (enableMaintenance && (isQuotationOrdering()|| isOrderSimulation())) {
	 return true;
	} else {
	 return false;
	}
   }  
    
	/**
	 * @return
	 */
	public boolean isShowCVVAsStars() {
		return showCVVAsStars;
	}

	/**
	 * @param b
	 */
	public void setShowCVVAsStars(boolean showCVVAsStars) {
		this.showCVVAsStars = showCVVAsStars;
	}
        
	public String getCvvVal(PaymentCCard card) {
		if (showCVVAsStars) {
			return card.getCVVAsStars();
		}
		else {
			return card.getCVV();
		}
	}
	
	/**
	 * returns a boolean, which defines if the payment data can be directly 
	 * maintained in the order or not  
	 */
	public boolean isDirectPaymentAllowed() {
		return this.directPaymentAllowed;
	}

    /**
     * returns a boolean, which defines if the message list is filled  
     * @return hasMessages as boolean, true if the message list containes at least one messages   
     */
    public boolean hasMessages() {
        boolean hasMessages = !this.docMessages.isEmpty();
        return hasMessages;
    }   
    

}




