package com.sap.isa.isacore.uiclass;

import javax.servlet.jsp.PageContext;

import com.sap.isa.businessobject.BusinessObjectManager;

public class CatalogStageListUI extends ShopListUI {
    
    /** 
     * The business object manager isn't visible on the JSP. <br>
     */ 
    protected BusinessObjectManager bom;
    
    public CatalogStageListUI(PageContext pageContext) {
        super(pageContext);
        log.entering("CatalogStageListUI constructor");
        log.exiting();
    }
    
    /**
      * Returns the bom.
      * @return BusinessObjectManager
      */
    public BusinessObjectManager getBom() {
        if (bom == null && userSessionData != null) {
            // get BOM
            bom = (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
        }   
        
        return bom;
    }
    
    /**
      * Returns check if IPCStagingDate should be shown
      * Also see equivalent method in ReadInactiveCatalogAction
      * 
      * @return true if so, false else
      */
    public boolean  showCatalogueStagingIPCDate() {
        boolean retVal = false;
        
        if (getBom() != null && bom.getShop() != null) {
            if (bom.getShop().isIPCPriceUsed() || !bom.getShop().isListPriceUsed()) {
                retVal = true;
            }
        }   
        
        return retVal;
    }

}
