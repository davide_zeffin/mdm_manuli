package com.sap.isa.isacore.uiclass;

import javax.servlet.jsp.PageContext;

import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.businessobject.webcatalog.CatalogBusinessObjectManager;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.util.MessageList;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.user.permission.ECoActionPermission;

/**
 * The class UIBaseIsa is a base class for ISA ui classes. <br>
 *  
 * The class provide the bom and the shop in all pages. 
 *
 * @author SAP
 * @version 1.0
 **/
public class IsaBaseUI extends InlineConfigBaseUI {

    protected BusinessObjectManager bom;
    protected Shop shop;

    /**
     * Constructor for IsaBaseUI. <br>
     * @param pageContext
     */
    public IsaBaseUI(PageContext pageContext) {
		super(pageContext);
    }


	/**
	 * Constructor for IsaBaseUI. <br>
	 * @param pageContext
	 */
	public IsaBaseUI() {
	}

	/**
	  * Initialize UI clasee . <br>
	  * 
	  * @param pageContext current page context
	  * 
	  * 
	  * @see com.sap.isa.ui.uiclass.BaseUI#initContext(javax.servlet.jsp.PageContext)
	  */
	 public void initContext(PageContext pageContext) {
        
	 	super.initContext(pageContext);
		
		// check for missing context
		if (userSessionData != null) {

			// get BOM
			bom = (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
	
	
			// now I can read the shop
			shop = bom.getShop();
		}
	 }	

    /**
     * Returns the bom. <br>
     * 
     * @return BusinessObjectManager
     */
    public BusinessObjectManager getBom() {
        return bom;
    }

    /**
     * Returns the shop. <br>
     * @return Shop
     */
    public Shop getShop() {
        return shop;
    }

	protected String getMessageString(MessageList messageList) {
	    
        String messageString = "";
        for (int i = 0; i < messageList.size(); i++) {
            messageString += messageList.get(i).getDescription() + "<br>";
        }

		return messageString;
	}
    
    /**
     * Returns true, if the company partner function is reseller
     * false else 
     */
    public boolean isCompPFReseller() {
        if (shop != null) {
            return shop.getCompanyPartnerFunction().equalsIgnoreCase(PartnerFunctionData.RESELLER);
        }
        else {
            return false;
        }
    }
    
	/**
	 * Check for the user Permission for the Creation of different Documents. <br>
	 * @param User for whom the permissions are required 
	 * @param String document for which permission is asked.
	 * @return Boolean True/False.
	 */
	public Boolean[] checkPermissions(User user, String[] docs, String[] actions) throws CommunicationException  {
		ECoActionPermission[] docPermissions = new ECoActionPermission[docs.length];
		for( int cnt = 0; cnt<docs.length; cnt++){
		docPermissions[cnt] = new ECoActionPermission(docs[cnt], actions[cnt]);	    
		}
		return user.hasPermissions(docPermissions);	    
	}	
    
    public String getLastVisited() {
        if (userSessionData != null) {
             CatalogBusinessObjectManager bom =
                       (CatalogBusinessObjectManager)userSessionData.getBOM(CatalogBusinessObjectManager.CATALOG_BOM);
             if (bom.getCatalog()!= null ) {
                 return bom.getCatalog().getLastVisited();
             }
        }
        return null;
    }
    
    /**
     * Return if the prices are visible in the catalogue and the marekting area. 
     * <br>
     * The authority check will be done in the catalogue area and only in the B2B
     * application. 
     * 
     * @return <code>true</code> if the price should be displayed.
     */
    public boolean isPriceVisible() {
        
        if (shop.getApplication().equals(ShopData.B2B)) {
            return super.isElementVisible("catalog.prices");
        }

        return true;        
    }
    
    /**
     * returns true if the catalog should be displayed
     * @return true, if the catalog should be displayed
     */
    public boolean showCatalog() {
        final String METHOD_NAME = "showCatalog()";
        log.entering(METHOD_NAME);
        
        boolean returnedValue = false;

        DocumentHandler documentHandler = (DocumentHandler)
                userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);

        if (documentHandler.isCatalogOnTop()) {
            returnedValue = true;
        }

        log.exiting();
        return returnedValue;
    }
    
}
