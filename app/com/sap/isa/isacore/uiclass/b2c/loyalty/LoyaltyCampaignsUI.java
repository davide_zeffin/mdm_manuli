package com.sap.isa.isacore.uiclass.b2c.loyalty;

import javax.servlet.jsp.PageContext;

import com.sap.ecommerce.businessobject.MarketingBusinessObjectManager;
import com.sap.ecommerce.businessobject.campaign.Campaign;
import com.sap.ecommerce.businessobject.campaign.MarketingCampaignList;
import com.sap.ecommerce.businessobject.loyalty.LoyaltyMembership;
import com.sap.isa.core.util.MessageList;

/**
 * UI class to handle the loyalty campaign in E-Commerce B2C application.
 *
 * @date  06/03/2008
 * @author D046485
 * @version 1.0
 */
public class LoyaltyCampaignsUI extends LoyaltyBaseUI {

	protected MarketingCampaignList campaignList;
    
	/**
	 * Constants to identify the transaction list in the request. <br>
	 */
	  public static final String LOY_CAMPAIGN_LIST = "campaignlist";
  
    
	public LoyaltyCampaignsUI(PageContext pageContext) {
		super(pageContext);
			
		if (userSessionData != null) {
			MarketingBusinessObjectManager marketingBom = (MarketingBusinessObjectManager) userSessionData.getBOM(MarketingBusinessObjectManager.MARKETING_BOM);
			LoyaltyMembership loyaltyMembership = marketingBom.getLoyaltyMembership();
			if (marketingBom != null) {
				campaignList = (MarketingCampaignList)loyaltyMembership.getLoyaltyCampaigns();
			}
		}
		
		pageContext.setAttribute(LOY_CAMPAIGN_LIST, campaignList);
	}
	
	/**
	 * return the list of loyalty campaigns
	 * @return marketingCampaignList, the list of loyalty Campaigns
	 */
	public MarketingCampaignList getLoyaltyCampaigns() {
		log.entering("getLoyaltyCampaigns");
		log.exiting();
		return campaignList;
	}
	
	/**
	 * returns the size of loyalty campaign list
	 * @return int, the loyalty campaign list size
	 */
	public int getLoyaltyCampaignsSize(){
		log.entering("getLoyaltyCampaignsSize");
		log.exiting();
		if(campaignList != null){
			return campaignList.size();
		}
		else {
			return 0;
		}
	}
	
	/**
	 * returns a Campaign identified by the index
	 * @param index
	 * @return Campaign identifiex by the given index
	 */
	public Campaign getLoyaltyCampaign(int index){
		log.entering("LoyaltyTransactionUI.getLoyaltyCampaign(int index)");
		
		Campaign returnCampaign = null;
		
		if(campaignList != null && index <= campaignList.size() ) {
			returnCampaign = (Campaign)campaignList.getData(index);
		}

		log.exiting();
		return returnCampaign;
	}
	
	/**
	 * returns the message list
	 * @return messageList, the message list associated to the loyalty campaign list
	 */
	public MessageList getMessageList(){
		log.entering("getMessageList");
		log.exiting();
		if(campaignList != null) {
			return campaignList.getMessageList();
		}
		else {
			return null;
		}
	}
	
	/**
	 * returns the size of the loyalty campaign list
	 * @return int, the size of the loyalty campaign list
	 */
	public int getMessageListSize(){
		log.entering("getMessageListSize");
		log.exiting();
		if(campaignList != null) {
			return campaignList.getMessageList().size();
		}
		else {
			return 0;
		}
	}
    
    /**
     * returns the planned start date of the loyalty campaign 
     * @return startDate, the start date of the loyalty campaign 
     */
    public String getPlannedStartDate(Campaign camp){
        log.entering("getPlannedStartDate");
        String startDate = "";
        if (camp != null &&
            camp.getHeader() != null &&
            camp.getHeader().getPlannedStartDate() != null ) {
            startDate = camp.getHeader().getPlannedStartDate();
        }
        if (log.isDebugEnabled()) {
            log.debug("camp: " + camp.getHeader().getCampaignID() + " - plannedStartDate = " + startDate);
        }
        log.exiting();
        return startDate;
    }

    /**
     * returns the planned end date of the loyalty campaign 
     * @return endDate, the end date of the loyalty campaign 
     */
    public String getPlannedEndDate(Campaign camp){
        log.entering("getPlannedEndDate");
        String endDate = "";
        if (camp != null &&
            camp.getHeader() != null &&
            camp.getHeader().getPlannedEndDate() != null ) {
            endDate = camp.getHeader().getPlannedEndDate();
        }
        if (log.isDebugEnabled()) {
            log.debug("camp: " + camp.getHeader().getCampaignID() + " - plannedEndDate = " + endDate);
        }
        log.exiting();
        return endDate;
    }

}

