/**
 * Created on Apr 7, 2008
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.isacore.uiclass.b2c.loyalty;

import javax.servlet.jsp.PageContext;

import com.sap.ecommerce.businessobject.MarketingBusinessObjectManager;
import com.sap.ecommerce.businessobject.loyalty.LoyaltyMembership;
import com.sap.isa.ui.uiclass.BaseUI;

/**
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class LoyaltyBaseUI extends BaseUI {

	public LoyaltyBaseUI(PageContext pageContext) {
		initContext(pageContext);
	}

    /**
     * Returns the points unit for the current price product
     * @return ptsUnit, points unit as String
     */
    public String getPointsUnit() {
        log.entering("getPointsUnit()");
        String ptsUnit = null;

        MarketingBusinessObjectManager mktBom = (MarketingBusinessObjectManager) userSessionData.getBOM(MarketingBusinessObjectManager.MARKETING_BOM);
        LoyaltyMembership loyMembership = mktBom.getLoyaltyMembership();
        if (loyMembership != null                   &&
            loyMembership.getPointAccount() != null) {  
            ptsUnit = loyMembership.getPointAccount().getPointCodeId();  
        }
        log.exiting();
        return ptsUnit;
    }

    /**
     * Returns the language dependen description of the points unit for the current price product
     * @return ptsUnitDescr, description of points unit as String
     */
    public String getPointsUnitDescr() {
        log.entering("getPointsUnitDescr()");
        String ptsUnitDescr = null;

        MarketingBusinessObjectManager mktBom = (MarketingBusinessObjectManager) userSessionData.getBOM(MarketingBusinessObjectManager.MARKETING_BOM);
        LoyaltyMembership loyMembership = mktBom.getLoyaltyMembership();
        if (loyMembership != null &&
            loyMembership.getPointAccount() != null) {  
            ptsUnitDescr = loyMembership.getPointAccount().getDescr();  
        }  
        log.exiting();
        return ptsUnitDescr;
    }
}
