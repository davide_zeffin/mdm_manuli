/*
 * Created on Apr 7, 2008
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.isacore.uiclass.b2c.loyalty;

import java.math.BigDecimal;
import java.util.Iterator;

import javax.servlet.jsp.PageContext;

import com.sap.ecommerce.businessobject.MarketingBusinessObjectManager;
import com.sap.ecommerce.businessobject.loyalty.LoyaltyMembership;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.backend.boi.webcatalog.pricing.PriceInfo;
import com.sap.isa.businessobject.Product;
import com.sap.isa.businessobject.ProductBaseData;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.item.ItemList;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.businessobject.order.OrderStatus;
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.ICatalog;
import com.sap.isa.catalog.boi.IFilter;
import com.sap.isa.catalog.boi.IQuery;
import com.sap.isa.catalog.boi.IQueryStatement;
import com.sap.isa.catalog.filter.CatalogFilterFactory;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.catalog.webcatalog.WebCatItemList;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.Message;
import com.sap.isa.isacore.uiclass.PricingBaseUI;
import com.sap.isa.ui.uiclass.BaseUI;

/**
 * @author d038645
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class LoyaltyBasketUI extends BaseUI {

	private ShopData _shop;
	private WebCatInfo _salesDocWebCat;
	private ItemData _item;
	private boolean followOnInstance;
	private HeaderData _header = null;
	private PricingBaseUI priceUi;
	private LoyaltyMembershipUI membershipUI;
	private MarketingBusinessObjectManager mktBom;
	private LoyaltyMembership loyMembership;

	public LoyaltyBasketUI(PageContext pageContext) {
		initContext(pageContext);
		priceUi = new PricingBaseUI(pageContext);
		membershipUI = new LoyaltyMembershipUI(pageContext);
		mktBom = (MarketingBusinessObjectManager) userSessionData.getBOM(MarketingBusinessObjectManager.MARKETING_BOM);
		loyMembership = mktBom.getLoyaltyMembership();
	}

	public LoyaltyBasketUI(PageContext pageContext, boolean isFollowOnInstance) {
		if (isFollowOnInstance) {
			followOnInstance = true;
		}
	}
	
	public LoyaltyBasketUI setHeader(HeaderData header) {
		initHeader(header);
		return this;
	}
	
	public LoyaltyBasketUI setHeader(HeaderData header, WebCatInfo webCat, ShopData shop) {
		_shop = shop;
		_salesDocWebCat = webCat;
		initHeader(header);
		return this;
	}
	
	public LoyaltyBasketUI setItem(ItemData item) {
		_item = item;
		return this;
	}

	/**
	 * @param header
	 */
	private void initHeader(HeaderData header) {
		_header = header;
	}
	
    public boolean isRedemptionBasket() {
        return (_header.getLoyaltyType() == null) ? false : _header.getLoyaltyType().equals(HeaderData.LOYALTY_TYPE_REDEMTPION); 
    }

    public boolean isBuyPointsBasket() {
        return (_header.getLoyaltyType() == null) ? false : _header.getLoyaltyType().equals(HeaderData.LOYALTY_TYPE_BUYPOINTS); 
    }

	public String getTotalAmount() {
		return _header.getTotalRedemptionValue();
	}
	
	private double getTotalAmountValue(ItemList items) {
		int n = (items == null) ? 0 : items.size();
		double sum = 0;
		String itemPrice = "";
		
		for (int i=0; i<n; i++) {
			setItem(items.get(i));
			itemPrice = getItemPoints();
			double d =  Double.parseDouble((itemPrice == null) ? "0.00" : itemPrice);
			sum += d;
		}
		return sum;
	}
	
	public String getTotalAmount(ItemList items) {
		return DecimalToCurreny(new BigDecimal(getTotalAmountValue(items)));
	}
	
	public String getPointCode() {
		log.entering("getPointCode()");
		String ptsUnitDescr = null;

		if (loyMembership != null &&
			loyMembership.getPointAccount() != null) {  
			ptsUnitDescr = loyMembership.getPointAccount().getDescr();  
		}  
		log.exiting();
		return ptsUnitDescr;
	}
	
	public double missingPoints(ItemList items) {
		double missingPts = -1;
		double pointsAvailable = 0;
		
		if (membershipUI != null) {
			loyMembership.getLoyaltyPointAccountBalanceFromBackend(loyMembership.getPointAccount().getPointCodeId(), loyMembership.getPointAccount().getDescr());
			pointsAvailable = Double.parseDouble(membershipUI.getPointAccountBalance()); 
			missingPts =  pointsAvailable - getTotalAmountValue(items);
			if (missingPts > 0) { // Enough points available 
				missingPts = 0;
			}
		}
		
		return missingPts;
	}
	
	/** 
	 * Retrieves the amount of redemption points the item costs
	 * @return
	 */
	public String getItemPoints() {
		String pts = null;
		ProductBaseData priceProd =  _item.getCatalogProduct();
		
		if (priceProd == null && _salesDocWebCat != null) {
			WebCatItem webCatItem = searchItemInCatalog();
			if (webCatItem == null) {
				return "";
			}
			_item.setCatalogProduct(new Product(webCatItem));
			priceProd =  _item.getCatalogProduct();
		}
		priceUi.setPriceProd(priceProd);
		PriceInfo[] prices = priceUi.getPricesForPriceProduct();

		if (loyMembership != null                   &&
			loyMembership.getMembershipId() != null &&
			loyMembership.getLoyaltyProgram() != null      &&
			loyMembership.getPointAccount() != null) {  
			String loyProg = loyMembership.getLoyaltyProgram().getProgramId();
			if (loyProg != null && loyProg.length() > 0) {
				String ptCodeId = loyMembership.getPointAccount().getPointCodeId();
				
				for (int i=0; i< prices.length; i++) {
					if (prices[i].getCurrency().equalsIgnoreCase(ptCodeId)) {
						pts = prices[i].getPrice();
						break;  
					}
				}
			}
		}

		log.exiting();
		return Convert(pts, _item.getQuantity());
	}
	
	/**
	 * @param pts
	 * @param string
	 * @return
	 */
	private String Convert(String pts, String quantity) {
		BigDecimal price = CurrencyToDouble(pts);
		BigDecimal quant = QuantityToDouble(quantity);
		BigDecimal amount = price.multiply(quant);

		return DecimalToCurreny(amount);
	}

	/**
	 * @param amount
	 * @return
	 */
	private String DecimalToCurreny(BigDecimal amount) {
		BigDecimal pts2 = amount.setScale(2, BigDecimal.ROUND_HALF_UP);

		return pts2.toString();
	}

	/**
	 * @param quantity
	 * @return
	 */
	private BigDecimal QuantityToDouble(String quantity) {
		BigDecimal qty = new BigDecimal(quantity);
		return qty;
	}

	/**
	 * @param pts
	 * @return
	 */
	private BigDecimal CurrencyToDouble(String pts) {
		if (pts == null) {
			pts="0";
		}
		
		BigDecimal points = new BigDecimal(pts);
		return points;
	}

	public String getLoyRedemptionValue() {
		return _item.getLoyRedemptionValue();
	}

	/**
	 * Searches for the given item in the Catalog and retrieves the related
	 * WebCatitem or null of no item was found. 
	 *
	 * @param item         The item, to search for
	 * @param theShop      The shop
	 * 
	 * @return WebCatItem  The found Catalog Item, or null if nothing was found
	 */
	public WebCatItem searchItemInCatalog() {
		HeaderData header = _header;
		ItemSalesDoc item = (ItemSalesDoc) _item;
		ShopData shop = _shop;
		OrderStatus orderStat;
		
		final String METHOD = "searchItemInCatalog()";
		log.entering(METHOD);
		WebCatItem      webCatItem = null;
		WebCatItemList  webCatItemList = null;
		String          requestedProduct = null;
		ICatalog        theCatalog = null;
		boolean         prodNameSearch = false;
		boolean closevalue = false;
        
        if (_header == null || _shop==null || _item==null) 
        	return null;
        	
		CatalogFilterFactory  myFactory = CatalogFilterFactory.getInstance();

		String backend = shop.getBackend();

		boolean isBackendR3 =
			backend.equals(Shop.BACKEND_R3)
				|| backend.equals(Shop.BACKEND_R3_40)
				|| backend.equals(Shop.BACKEND_R3_45)
				|| backend.equals(Shop.BACKEND_R3_46)
				|| backend.equals(Shop.BACKEND_R3_PI);

		try {
			if (shop.isInternalCatalogAvailable()) {
				// get the catalog
				theCatalog = _salesDocWebCat.getCatalog();
			}
			else {
				log.exiting();
				return webCatItem;
			}
	        
	        
			try {
				IQueryStatement queryStmt =
					theCatalog.createQueryStatement();
				IFilter search = null;
				// first check if we can search by the techkey
				if (null != item.getProductId()
					&& item.getProductId().toString().length() > 0) {
					// don't forget to copy the OBJECT_ID to the item in this case
					search =
						myFactory.createAttrEqualValue(
							"OBJECT_GUID",
							item.getProductId().toString());
					// store productname, in case nothing is found for the given productguid
					requestedProduct = item.getProduct();
				} else {
					if (log.isDebugEnabled()) {
						log.debug("Search for product name: " + item.getProduct());
					}
					log.debug("Search for product name");
					prodNameSearch = true;
					// search for the product name, explicitly specify what fields to search in
					search =
						myFactory.createAttrEqualValue(
							"OBJECT_ID",
							item.getProduct());
				}
	            
				if (log.isDebugEnabled()) {
					log.debug("Search for redemption item.");
				}
				
				if(header.getShop() != null)
				{	// only if RedemptionShop set these attributes
					String loyRedemptionOrderType = header.getShop().getRedemptionOrderType();
					
					if (loyRedemptionOrderType != null && loyRedemptionOrderType.length()>0) {
						if (item.isPtsItem()) {
							search = myFactory.createAnd(search, myFactory.createAttrEqualValue("IS_PTS_ITEM", "X"));
						}
						else {
							search = myFactory.createAnd(search, myFactory.createNot( myFactory.createAttrEqualValue("IS_PTS_ITEM", "X")));
						}
					}
				}
	            
	            
				queryStmt.setStatement(search, null, null);    
				IQuery query = theCatalog.createQuery(queryStmt);
				if (log.isDebugEnabled()) {
					log.debug(
						"searchItemInCatalog: Query Stmt="
							+ queryStmt.toString());
				}
				webCatItemList =
					new WebCatItemList(_salesDocWebCat, query, true, false, false);
				// yes, we do want the accessories
			} 
			catch (CatalogException e) {
				log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "system.backend.exception", e);
				return webCatItem;
			} // try
	    
			if (webCatItemList.size() == 0) { // not found
				// have we searche for ProductGuid and a product name was also given, but we
				// found nothing, so show also the unknown prodduct name, to make the error message
				// and the postion easier to interprt for the user
				if (requestedProduct != null) { // only set if searched via productId
					item.setProduct(requestedProduct);
					// to generate a not found error message after the next 
					// refresh the unknown productId must be deleted
					item.setProductId(null);
				} 
				else {
					// this part intentionally left empty
				}
	                            
				item.addMessage(new Message(Message.ERROR, 
									   "b2b.order.populitems.nonefound", 
									   new String[]  { item.getProduct()}, ""));
                                       
				return webCatItem;
			} 
			else if (
				webCatItemList.size() > 1) { // more than one found
				// log it and take the first one
				if (log.isDebugEnabled()) {
					log.debug(
						"more than one item found in searchItemInCatalog");
				}
			}
	    
			// retrieve data for order item
			if (isBackendR3) {
				Iterator webCatItemListIter = webCatItemList.iterator();
				String theProductId = item.getProduct();
				if (theProductId == null || theProductId.equals("")) {
					theProductId = item.getProductId().getIdAsString();
					if (theProductId == null || theProductId.equals("")) {
						log.error("Couldn't get ID of SalesDocItem, populate will fail!");
					}
				}
            
				// see longer comment below
				WebCatItem savedItem = null;
				WebCatItem savedItemClose = null;
				// check if we are using the quicksearch entry and if we have more than one result
				if (item.getDescription() == null && webCatItemList.size() > 1) {
					// we are in quicksearch, check if one of the retrieved product ids equals the entred one
					String enteredProdId = theProductId.toUpperCase();
				while (webCatItemListIter.hasNext()) {
					WebCatItem theWebCatItem = (WebCatItem) webCatItemListIter.next();
						if (theWebCatItem.getProductID().toUpperCase().equals(enteredProdId)) {
							return theWebCatItem;
						}
					}
					// no product has been found that exactly matches the Product id entered by the user
					// in this case we return the first found product from the webCatItemList
					return webCatItemList.getItem(0);
				} 
				else {
					while (webCatItemListIter.hasNext()) {
						WebCatItem theWebCatItem = (WebCatItem) webCatItemListIter.next();
                        
						// if the item comes from the catalog we have a description and an unit
						// they must be the same as the description and the unit in the 
						// webCatItem to ensure that we really have the correct item
						String webCatItemDescription = theWebCatItem.getDescription();
						String webCatItemUnit = theWebCatItem.getUnit();
                
						String itemDescription = item.getDescription();
						String itemUnit = item.getUnit();
                
						// to check if the item was put into the basket
						// from the catalog or by quick order entry 
						// we compare the description and the unit of the item also
						// BUT: if we want to copy an item from a recovered 
						// orderTemplate, meanwhile the description of the item can 
						// have changed due to typos or a more detailed description
						// so we save the first found webCatItem here to return this if
						// we don't find a matching description/unit
						savedItem = theWebCatItem;
						if (itemUnit.equals(webCatItemUnit)) {   
							// Abhishek:Taking the close value after matching productid and Unit
							closevalue = true;
							savedItemClose = theWebCatItem;
							// If the item description is null then return this value
							if (itemDescription == null ||
								itemDescription.trim().length() < 1 ||
								itemDescription.equals(webCatItemDescription)) {
							return theWebCatItem;
						}
								}
				}  // endwhile
				}
            
				//Abhishek : return the close value with the matching
				// productid , UOM and description in case it is not null
				// if the description is null then also return the close
				// catalog item with the matching productid and UOM
				if (closevalue) {
					return savedItemClose;
				}
				//   if we reach this point, we didn't find a suitable item
				// so check if we found a sufficient good looking one and return this
				else {
				if (savedItem != null) {
					return savedItem;
				}
				}
            
				// no product found with the given material ID
				if (requestedProduct != null) {
					// only set if searched via productId
					item.setProduct(requestedProduct);
					// to generate a not found error message after the next 
					// refresh the unknown productId must be deleted
					item.setProductId(null);
				}

				item.addMessage(new Message(Message.ERROR,  "b2b.order.populitems.nonefound", new String[] { item.getProduct()}, ""));
                    
				return webCatItem;
			}
			else { //CRM
				if (webCatItemList.size() > 1 && prodNameSearch) {
					log.debug("search best match in found products");
                    
					Iterator webCatItemListIter = webCatItemList.iterator();
					String theProductId = item.getProduct();
					boolean found = false;
            
					// see longer comment below
					WebCatItem savedItem = null;
					while (webCatItemListIter.hasNext()) {
						WebCatItem theWebCatItem = (WebCatItem) webCatItemListIter.next();
                        
						if (theProductId.equals(theWebCatItem.getProduct())) {
							webCatItem = theWebCatItem;
							found = true;
							break;
						}
					}
                    
					if (!found) {
						webCatItem = webCatItemList.getItem(0);
					}
				}
				else {
					webCatItem = webCatItemList.getItem(0);
				}
                
			} // if (isBackendR3)
		}
		finally {
			log.exiting();
		}
        
		return webCatItem;
	}
    
}
