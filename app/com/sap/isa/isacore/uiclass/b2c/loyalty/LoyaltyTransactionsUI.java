package com.sap.isa.isacore.uiclass.b2c.loyalty;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.jsp.PageContext;

import com.sap.ecommerce.businessobject.MarketingBusinessObjectManager;
import com.sap.ecommerce.businessobject.loyalty.LoyaltyMembership;
import com.sap.ecommerce.businessobject.loyalty.LoyaltyTransaction;
import com.sap.ecommerce.businessobject.loyalty.LoyaltyTransactionList;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.core.util.MessageList;

/**
 * UI class to handle the loyalty transaction in E-Commerce B2C application.
 *
 * @date  29/02/2008
 * @author D046485
 * @version 1.0
 */
public class LoyaltyTransactionsUI extends LoyaltyBaseUI {

    protected LoyaltyTransactionList transactionList;
    
    private Shop shop = null;
    
	/**
	 *  Constants to identify the transaction list in the request. <br>
	 */
	public static final String LOY_TRANSACTION_LIST = "transactionlist";

    // Transaction types for which resources are defined in crm~b2c~resources.properties
    private static final String TTYPE_ACCR = "ACCR";   
    private static final String TTYPE_CANC = "CANC";   
    private static final String TTYPE_CONS = "CONS";   
    private static final String TTYPE_EXPR = "EXPR";   
    private static final String TTYPE_RETN = "RETN";   
    
	public LoyaltyTransactionsUI(PageContext pageContext) {
		super(pageContext);
			
		if (userSessionData != null) {
			MarketingBusinessObjectManager marketingBom = (MarketingBusinessObjectManager) userSessionData.getBOM(MarketingBusinessObjectManager.MARKETING_BOM);
			LoyaltyMembership loyaltyMembership = marketingBom.getLoyaltyMembership();

			if (loyaltyMembership != null) {
				transactionList = (LoyaltyTransactionList)loyaltyMembership.getLoyaltyTransactions();
			}	
		//sets the shop variable

		// get BOM
			BusinessObjectManager bom =	(BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
			shop = bom.getShop();
		}		
		pageContext.setAttribute(LOY_TRANSACTION_LIST, transactionList);
	}

	/**
	 * Return the date format. <br>
	 * The Format is taken from the salesDocConfiguration.
	 * This is the format, that must be used for the new calendar control.
	 * 
	 * @return the date format taken from the salesDocConfiguration
	 */
	public String getDefaultDateFormat() {
		log.entering("getDefaultDateFormat");
		String dateFormat = null;
		// check for missing context
		if(shop != null){
			dateFormat = shop.getDateFormat();
			dateFormat = dateFormat.replace('Y', 'y');
			dateFormat = dateFormat.replace('D', 'd');
		}
		log.exiting();
		return dateFormat;
	}
	
	/**
	 * returns the loyalty transaction list
	 * @return loyaltytransactionlist, the list of loyalty transactions
	 */
	public LoyaltyTransactionList getLoyaltyTransactions() {
		log.entering("getLoyaltyTransactions");
		log.exiting();
		return transactionList;
	}
	
	/**
	 * returns the size of the loyalty transaction list
	 * @return int, the size of the loyalty transaction list
	 */
	public int getLoyaltyTransactionsSize(){
		log.entering("getLoyaltyTransactionsSize");
		log.exiting();
		if(transactionList != null){
			return transactionList.size();
		}
		else {
			return 0;
		}
	}
	
	/**
	 * returns the loyalty transaction from a given index
	 * @param index, the index from the loyalty transaction that will be returned
	 * @return loyalty transaction, the loyalty transaction from a given index
	 */
	public LoyaltyTransaction getLoyaltyTransaction(int index){
		log.entering("LoyaltyTransactionUI.getLoyaltyTransaction");
		
		LoyaltyTransaction returnTransaction = null;
		
		if(transactionList != null && index <= transactionList.size() ) {
			returnTransaction = (LoyaltyTransaction)transactionList.getData(index);
		}

		log.exiting();
		return returnTransaction;
	}
	
	/**
	 * returns the message list
	 * @return messageList, the message list
	 */
	public MessageList getMessageList(){
		log.entering("getMessageList");
		log.exiting();
		if(transactionList != null) {
			return transactionList.getMessageList();
		}
		else {
			return null;
		}
	}
	
	/**
	 * returns the size of the message list
	 * @return int, the size of the message list
	 */
	public int getMessageListSize(){
		log.entering("getMessageListSize");
		log.exiting();
		if(transactionList != null) {
			return transactionList.getMessageList().size();
		}
		else {
			return 0;
		}
	}
	
	/**
	 * returns the transaction From Date
	 * @return string, represents the transaction From Date
	 */
	public String getFromTransactionDate(){
		log.entering("getFromTransactionDate()");
		String returnValue = new String();
		if(transactionList != null && transactionList.getFromTransactionDate() != null){
			returnValue = transactionList.getFromTransactionDate();
		}
		else{
			SimpleDateFormat fmt = new SimpleDateFormat(getDefaultDateFormat());
			Date date = new Date();
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			cal.add(Calendar.DAY_OF_MONTH,-30);
			date = cal.getTime();
			returnValue = fmt.format(date);
		}
		log.exiting();
		return returnValue;
	}

	/**
	 * returns the transaction To Date
	 * @return string, represents the transaction To Date
	 */
	public String getToTransactionDate(){
		log.entering("getToTransactionDate()");
		String returnValue = new String();
		if(transactionList != null && transactionList.getToTransactionDate() != null){
			returnValue = transactionList.getToTransactionDate();
		}
		else{
			SimpleDateFormat fmt = new SimpleDateFormat(getDefaultDateFormat());
			Date date = new Date();
			returnValue = fmt.format(date);
		}
		log.exiting();
		return returnValue;
	}
    
    /**
     * Checks if a transaction type can be converted to string representation. <br>
     * @param  transactionType as String to be checked
     * @return isSupported as boolean, true if transaction type can be converted, false else
     * @deprecated
     */
    public boolean isTransactionTypeSupported(String transactionType) {
        log.entering("isTransactionTypeSupported");
        boolean isSupported = false; 
        if ( transactionType != null &&
             ( transactionType.equals(TTYPE_ACCR) ||
               transactionType.equals(TTYPE_CANC) ||
               transactionType.equals(TTYPE_CONS) ||
               transactionType.equals(TTYPE_EXPR) ||
               transactionType.equals(TTYPE_RETN) )) {
            isSupported = true;
        }
        else {
            if (log.isDebugEnabled()) {
                log.debug("no resource property defined for the transaction type: " + transactionType);
            }
        }
        log.exiting();
        return isSupported;
    }

}
