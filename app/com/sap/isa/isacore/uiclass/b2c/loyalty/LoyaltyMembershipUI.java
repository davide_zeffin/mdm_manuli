package com.sap.isa.isacore.uiclass.b2c.loyalty;

import javax.servlet.jsp.PageContext;

import com.sap.ecommerce.businessobject.MarketingBusinessObjectManager;
import com.sap.ecommerce.businessobject.loyalty.LoyaltyMembership;
import com.sap.ecommerce.businessobject.loyalty.LoyaltyPointAccount;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.isacore.action.loyalty.LoyaltyUtilities;


/**
 * UI class to handle the loyalty membership in E-Commerce B2C application.
 *
 * @date  29/02/2008
 * @author D046485
 * @version 1.0
 */
public class LoyaltyMembershipUI extends LoyaltyBaseUI {
	
	private LoyaltyMembership loyaltyMembership = null;
	private Shop shop = null;

	public LoyaltyMembershipUI(PageContext pageContext) {
		super(pageContext);
		if (userSessionData != null) {
			MarketingBusinessObjectManager marketingBom = (MarketingBusinessObjectManager) userSessionData.getBOM(MarketingBusinessObjectManager.MARKETING_BOM);
			loyaltyMembership = marketingBom.getLoyaltyMembership();
			
			BusinessObjectManager bom = (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
			shop = bom.getShop();
		}
	}

	public String getMembershipID() {
		log.entering("getMembershipID");
		String returnValue = null;
		if(loyaltyMembership != null){
			returnValue = loyaltyMembership.getMembershipId();
		}
		
		if(log.isDebugEnabled()){
			log.debug("returnedValue:"+returnValue);
		}
		
		log.exiting();
		
		return returnValue;
	}

	public String getPointAccountBalance() {
		log.entering("getPointAccountBalance");
		String returnValue = null;
		if(loyaltyMembership != null){
			LoyaltyPointAccount loyaltyPointAccount = loyaltyMembership.getPointAccount();
			if(loyaltyPointAccount != null){
				returnValue = loyaltyPointAccount.getBalance();
			}
		}
		log.exiting();
	
		return returnValue;
	}

	public String getCheckedValue(String uiFlag) {
	   log.entering("getCheckedValue");
	   
	   String checkedValue  = "";
	   
	   if (userSessionData != null &&
	       userSessionData.getAttribute(uiFlag) != null && 
	       userSessionData.getAttribute(uiFlag).toString().equals("true")) {
	     checkedValue = "checked=\"checked\"";
	   }
       log.exiting();
	   
	   return checkedValue;
	}
	
	public boolean isLoyaltyProgramMember()  {
		log.entering("isLoyaltyProgramMember");
				
		log.exiting();
		if(loyaltyMembership != null)
		  return loyaltyMembership.exists();
		else  
		  return false;
	}
	
	public boolean isRequestToJoinLoyaltyProgram() {
		log.entering("isRequestToJoinLoyaltyProgram");
		
		log.exiting();
		return LoyaltyUtilities.isJoiningLoyaltyProgram(userSessionData); 
	    
	}
	
	public String getPromotionCode() {
		log.entering("getPromotionCode");
		
		String promoCode = "";
		
	    if (userSessionData != null) {
		  MarketingBusinessObjectManager marketingBom = (MarketingBusinessObjectManager) userSessionData.getBOM(MarketingBusinessObjectManager.MARKETING_BOM);
		  if (marketingBom.getLoyaltyCampaign() != null && 
		      marketingBom.getLoyaltyCampaign().getHeader() != null &&
		      marketingBom.getLoyaltyCampaign().getHeader().getCampaignID() != null &&
		      marketingBom.getLoyaltyCampaign().getHeader().getCampaignID().length() > 0) { 
		    promoCode = marketingBom.getLoyaltyCampaign().getHeader().getCampaignID();
		  } 
		  else if (userSessionData.getAttribute("loyaltyPromotionCode") != null &&
				   userSessionData.getAttribute("loyaltyPromotionCode").toString().length() > 0){
		    promoCode = userSessionData.getAttribute("loyaltyPromotionCode").toString();
		  }
		} 
		    
		log.exiting();    
		return promoCode;    
	}
	
	
	public boolean isErrorOccurred() {
		log.entering("isErrorOccurred");
		
        if (userSessionData != null) {
		  LoyaltyMembership loyMembership = LoyaltyUtilities.getLoyaltyMembership(userSessionData);
	      if (loyMembership != null && loyMembership.hasMessages()) {
	    	request.setAttribute("loyaltyMessages", loyMembership.getMessageList());
	    	log.exiting();  
	    	return true;
	       } else {
	    	log.exiting();     
	    	return false;
	       }	 
	    }
        
        log.exiting();  
	    return false;
	}

	
	public String getUrl(PageContext pageContext, String uiFlag) {
		log.entering("getUrl");
		
		String url = null;
		String urlpre = "";
		String urlpost = "";
		String target = null;
		String key = null;
		String args[] = new String[2];
		
		if (uiFlag.equals("joinloyaltyprogramflag")) {
		   target = "b2c/loyalty/loyaltyInformation.jsp";	
		   key = "b2c.loyalty.program.join.ckbx";
		   url = WebUtil.getAppsURL(pageContext, null, target, null, null, false);       
		   urlpre = "<a href=\"#\" onclick=\"window.open(\'" + url + "\',\'\','width=400,height=400,scrollbars=yes,resizable=yes\'); return false\">";
           urlpost = "</a>";		
           args[0] = urlpre; args[1] = urlpost;
           
           log.exiting();  
  		   return WebUtil.translate(pageContext, key, args);
  		   
		} else if (uiFlag.equals("acceptprogramtermsflag")){
			target = "b2c/order/terms_and_conditions.jsp";
			key = "b2c.loyalty.program.terms.ckbx";
			url = WebUtil.getAppsURL(pageContext, null, target, null, null, false);       
			urlpre = "<a href=\"#\" onclick=\"window.open(\'" + url + "\',\'\','width=400,height=400,scrollbars=yes,resizable=yes\'); return false\">";
	        urlpost = "</a>";
	        args[0] = urlpre; args[1] = urlpost;
	        
	        log.exiting();  
			return WebUtil.translate(pageContext, key, args);
		}
		
		log.exiting();  
		return "";
		 
				                   	
	}		
	
	
	/**
	 * returns the point code description from the shop
	 * @return pointCodeDescription, the point code description from the shop
	 */
	public String getPointCodeDescription() {
		log.entering("getPointCodeDescription");
		
		if (userSessionData != null) {
			BusinessObjectManager bom = (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
			Shop shop = bom.getShop();
			
			log.exiting();
		    return shop.getPointCodeDescription();
		}
		
		log.exiting();
		return "";
	}
	
	
	/**
	 * returns the point code from the shop
	 * @return pointCode, the point code from the shop
	 */
	public String getPointCode() {
		log.entering("getPointCode");
		
		String returnedValue = new String();
		if(shop!=null){
			returnedValue = shop.getPointCode();
		}
		if(log.isDebugEnabled()){
			log.debug("returnedValue:"+returnedValue);
		}
		
		log.exiting();
		return returnedValue;
	}
    
    /**
     * Returns the language dependend description of the loyalty program
     * @return loyProgDescr, description of the loyalty program as String
     */
    public String getLoyProgDescr() {
        log.entering("getLoyProgDescr()");
        String loyProgDescr = shop.getLoyProgDescr();
        
        log.exiting();
        return loyProgDescr;
    }

    
    /** 
     * returns true if the <b>Join Now</b> should be rendered
     * @return boolean value indicating if the loyalty registration link is visible or not
     */
	public boolean showLoyaltyLink(){
		log.entering("showLoyaltyLink");
		
		boolean showLoyaltyLink = false;
		
		if (shop.isEnabledLoyaltyProgram()) {
		  showLoyaltyLink = true;
		  
		  if (userSessionData != null) {
             
            MarketingBusinessObjectManager marketingBom = null;
            try {
                marketingBom = (MarketingBusinessObjectManager) userSessionData.getBOM(MarketingBusinessObjectManager.MARKETING_BOM);
            }
            catch (Exception e) {
                log.error("Exception occurred " +  e.getMessage());
            }
			
            
            if (marketingBom != null) {
                LoyaltyMembership loyaltyMembership = marketingBom.getLoyaltyMembership();
             
                if (loyaltyMembership != null && loyaltyMembership.exists()) {
                  showLoyaltyLink = false; 
                }
            }
            else {
                showLoyaltyLink = false;
            }
		  }	   	
		}
		
		log.exiting();
		return showLoyaltyLink;
	}
}
