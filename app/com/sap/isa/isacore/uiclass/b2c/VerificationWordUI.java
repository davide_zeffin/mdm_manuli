package com.sap.isa.isacore.uiclass.b2c;

import java.util.List;

import javax.servlet.jsp.PageContext;

import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.Identification;
import com.sap.isa.core.FrameworkConfigManager;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.isacore.uiclass.IsaBaseUI;

public class VerificationWordUI extends IsaBaseUI {

    private boolean showVerification = false;
    /**
     *
     */

    public VerificationWordUI(PageContext pageContext) {
        super(pageContext);
        setVerificationMode();
    }

    /**
     *
     */

    public VerificationWordUI() {
        super();
        setVerificationMode();
    }

    private void setVerificationMode () {
        showVerification = false;
        try {
            // get the Verification Word's Diplay Status (if there's any)
            showVerification = (boolean)  (
            (String)FrameworkConfigManager.Servlet.getInteractionConfigParameter(
                request.getSession(),
                "ui",
                "ShowVerificationWord",
                ""
                )
            ).equals("true");
        }
        catch (Exception ex) {
            if (log.isDebugEnabled()) {
                log.debug("Verification word not found in XCM: " + ex.getMessage());
            }
        }
    }
    
    public boolean useVerificationWord () {
        return showVerification;
    }
    
    public String getVerificationWord(BusinessPartner partner) {
        String vWord = WebUtil.translate(this.request.getSession().getServletContext(), this.request.getSession(), "b2c.callcentermode.novword", null);
        List list = null;

        try {
            vWord = WebUtil.translate(this.request.getSession().getServletContext(), this.request.getSession(), "b2c.callcentermode.vwordukn", null);            
            if (useVerificationWord()) {
                String strType = shop.getIdTypeForVerification();
       
                list = partner.getIdentificationsOfType(strType);
                if (list.size() == 1) {
                    Identification id = (Identification) list.get(0);
                
                    vWord = id.getNumber();
                }
            }
        }
        catch (Exception ex) {
            log.debug("Could not read vWord.");
        }
        
        return vWord;
    }
    
}
