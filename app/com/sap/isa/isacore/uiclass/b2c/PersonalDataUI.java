package com.sap.isa.isacore.uiclass.b2c;

import java.util.Iterator;

import javax.servlet.jsp.PageContext;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.User;
import com.sap.isa.core.util.Message;
import com.sap.isa.ui.uiclass.BaseUI;

/**
 * UI class to handle the personal data in E-Commerce B2C application.
 *
 * @author D046485
 * @version 1.0
 */
public class PersonalDataUI extends BaseUI{
	
	public PersonalDataUI(PageContext pageContext) {
		initContext(pageContext);
	}
	
	public void removeMessage(String propertyKeyToRemove){
		
		BusinessObjectManager bom =	(BusinessObjectManager) userSessionData.getBOM(
								BusinessObjectManager.ISACORE_BOM);
		User user = bom.getUser();
				
		Iterator msgList = user.getMessageList().iterator();
		user.clearMessages();
		while (msgList.hasNext()) {
		  Message message = (Message) msgList.next();
		  if (!message.getProperty().equals(propertyKeyToRemove)) {
			  user.addMessage(message);
		  }
		}
	}
}