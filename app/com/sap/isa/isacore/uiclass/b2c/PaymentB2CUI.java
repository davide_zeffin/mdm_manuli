/**
 * Class:        PaymentB2CUI  
 * Author:       SAP AG Germany, Copyright (c) 2006, All rights reserved.  
 * Created:      01.04.2006 
 * Version:      1.0
 */
package com.sap.isa.isacore.uiclass.b2c;

import java.util.List;

import javax.servlet.jsp.PageContext;

import com.sap.isa.core.TechKey;
import com.sap.isa.isacore.uiclass.IsaBaseUI;
import com.sap.isa.payment.backend.boi.PaymentBaseTypeData;
import com.sap.isa.payment.businessobject.BankTransfer;
import com.sap.isa.payment.businessobject.PaymentBase;
import com.sap.isa.payment.businessobject.PaymentBaseType;
import com.sap.isa.payment.businessobject.PaymentCCard;
import com.sap.isa.payment.businessobject.PaymentMethod;


/**
 * UI class to handle payment data in E-Commerce B2C application.
 *
 * @author D038380
 * @version 1.0
 */
public class PaymentB2CUI extends IsaBaseUI {

	private PaymentBaseType paymentType;
    private PaymentBase payment;
    private int paymentIndex;

    /**
     * Constructor for a new PaymentB2CUI object.
     *
     * @param pageContext JSP page context
     * @param payType {@link com.sap.isa.payment.businessobject.PaymentBaseType} object 
     * @param pay {@link com.sap.isa.payment.businessobject.PaymentBase} object
     * @param payIndex Index used to differentiate between more payment types (represented by PaymentBaseType) on a 
     *                 same page. If only one payment type is involved, set it for example to &quot;0&quot;. 
     */
    public PaymentB2CUI(PageContext pageContext, PaymentBaseType payType, PaymentBase pay, int payIndex) {
        super(pageContext);
        paymentType = payType;
        payment = pay;
        paymentIndex = payIndex;
    }

    /**
     * Returns {@link com.sap.isa.payment.businessobject.PaymentBase} object.
     *
     * @return PaymentBase object
     */
    public PaymentBase getPaymentBase() {
        return payment;
    }

    /**
     * Returns {@link com.sap.isa.payment.businessobject.PaymentBaseType} object.
     *
     * @return PaymentBaseType object
     */
    public PaymentBaseType getPaymentBaseType() {
        return paymentType;
    }

    /**
     * Returns index of the payment types represented by this class.
     * @see #PaymentB2CUI(PageContext, PaymentBaseType, PaymentBase, int)
     *
     * @return index
     */
    public int getIndex() {
        return paymentIndex;
    }

    /**
     * Returns number of available payment types.<br /><br />
     * Considered will be:
     * <ul>
     *   <li>Invoice</li>
     *   <li>COD</li>
     *   <li>Credit Card</li>
     *   <li>Bank Transfer</li>
     * </ul>  
     *
     * @return number of payment types
     */
    public int getNumberOfAvailPaytypes() {
        int numberOfAvailPaytypes = 0;

        if (paymentType.isInvoiceAvailable()) {
            numberOfAvailPaytypes++;
        }

        if (paymentType.isCODAvailable()) {
            numberOfAvailPaytypes++;
        }

        if (paymentType.isCardAvailable()) {
            numberOfAvailPaytypes++;
        }

        if (paymentType.isBankTransferAvailable()) {
            numberOfAvailPaytypes++;
        }

        return numberOfAvailPaytypes;
    }

    /**
     * Returns selected payment type.<br />
     * The information about which payment types are available and which payment type is checked, will be represented by
     * {@link com.sap.isa.payment.businessobject.PaymentBaseType} class.
     *
     * @return selected payment type (represented by a TechKey)
     * @see com.sap.isa.payment.businessobject.PaymentBaseType
     */
    public TechKey getCheckedPayType() {
        TechKey checkedPaytype = null;

        if (paymentType.getCheckedPaytypeTechKey() != null) {
            checkedPaytype = paymentType.getCheckedPaytypeTechKey();
        }
        else {
            checkedPaytype = paymentType.getDefault();
        }

        return checkedPaytype;
    }

    /**
     * Returns selected payment method.<br />
     * The payment method will be represented by {@link com.sap.isa.payment.businessobject.PaymentMethod} class.<br /><br />
     * If no payment method could be determined or more payment methods are available with the same payment type 
     * (shouldn't be possible in B2C), this method returns <code>null</code>.
     *
     * @return PaymentMethod object 
     */
    public PaymentMethod getCheckedPayMethod() {
        PaymentMethod pMethod = null;
        List payMethods = null;

        if (isPayTypeInvoiceChecked()) {
            payMethods = payment.getPaymentMethodsByPaymentBaseTypeRef(paymentType.getTechKey().getIdAsString(),
                                                                       PaymentBaseTypeData.INVOICE_TECHKEY.getIdAsString());
        }
        else if (isPayTypeCODChecked()) {
            payMethods = payment.getPaymentMethodsByPaymentBaseTypeRef(paymentType.getTechKey().getIdAsString(),
                                                                       PaymentBaseTypeData.COD_TECHKEY.getIdAsString());
        }
        else if (isPayTypeCardChecked()) {
            payMethods = payment.getPaymentMethodsByPaymentBaseTypeRef(paymentType.getTechKey().getIdAsString(),
                                                                       PaymentBaseTypeData.CARD_TECHKEY.getIdAsString());
        }
        else if (isPayTypeBanktransferChecked()) {
            payMethods = payment.getPaymentMethodsByPaymentBaseTypeRef(paymentType.getTechKey().getIdAsString(),
                                                                       PaymentBaseTypeData.BANK_TECHKEY.getIdAsString());
        }

        if (payMethods.size() == 0) {
            log.debug("no payment method object found in PaymentBase instance");
        }
        else if (payMethods.size() > 1) {
            log.debug("more than one payment method object found in PaymentBase instance (currently shouldn't be possible in B2C scenario!)");
        }
        else {
            pMethod = (PaymentMethod) payMethods.get(0);
        }

        return pMethod;
    }

    /**
     * Checks if payment type <i>invoice</i> is selected.
     *
     * @return TRUE if <i>invoice</i> is selected
     */
    public boolean isPayTypeInvoiceChecked() {
        if ((getCheckedPayType() != null) && paymentType.getInvoice().equals(getCheckedPayType())) {
            return true;
        }

        return false;
    }

    /**
     * Checks if payment type <i>COD</i> is selected.
     *
     * @return TRUE if <i>COD</i> is selected
     */
    public boolean isPayTypeCODChecked() {
        if ((getCheckedPayType() != null) && paymentType.getCOD().equals(getCheckedPayType())) {
            return true;
        }

        return false;
    }

    /**
     * Checks if payment type <i>credit card</i> is selected.
     *
     * @return TRUE if <i>credit card</i> is selected
     */
    public boolean isPayTypeCardChecked() {
        if ((getCheckedPayType() != null) && paymentType.getCard().equals(getCheckedPayType())) {
            return true;
        }

        return false;
    }

    /**
     * Checks if payment type <i>bank transfer</i> is selected.
     *
     * @return TRUE if <i>bank transfer</i> is selected
     */
    public boolean isPayTypeBanktransferChecked() {
        if ((getCheckedPayType() != null) && paymentType.getBankTransfer().equals(getCheckedPayType())) {
            return true;
        }

        return false;
    }

    /**
     * Returns selected credit card type (e.g. keys for VISA, Master Card...).
     *
     * @return key of credit card type
     */
    public String getSelectedCardTypeKey() {
        String selectedCardType = "";

        PaymentCCard ccard = getCCard();

        if ((ccard != null) && (ccard.getTypeTechKey() != null)) {
            selectedCardType = ccard.getTypeTechKey().getIdAsString();
        }

        return selectedCardType;
    }

    /**
     * Returns {@link PaymentCCard} object, if it's available in {@link PaymentBase}.
     * If no PaymentCCard could be determined or more PaymentCCard objects are available 
     * (shouldn't be possible in B2C), this method returns <code>null</code>.
     * 
     * @return PaymentCCard or <code>null</code>
     */
    public PaymentCCard getCCard() {
        PaymentCCard ccard = null;

        List payMethods = payment.getPaymentMethodsByPaymentBaseTypeRef(paymentType.getTechKey().getIdAsString(),
                                                                        PaymentBaseTypeData.CARD_TECHKEY.getIdAsString());

        if (payMethods.size() == 0) {
            log.debug("no payment card object found in PaymentBase instance");
        }
        else if (payMethods.size() > 1) {
            log.debug("more than one card object found in PaymentBase instance (currently shouldn't be available in B2C scenario!)");
        }
        else {
            ccard = (PaymentCCard) payMethods.get(0);
        }

        return ccard;
    }

    /**
     * Returns {@link BankTransfer} object, if it's available in {@link PaymentBase}.
     * If no BankTransfer could be determined or more BankTransfer objects are available 
     * (shouldn't be possible in B2C), this method returns <code>null</code>.
     * 
     * @return PaymentCCard or <code>null</code>
     */
    public BankTransfer getBankTransfer() {
        BankTransfer bankTransfer = null;

        List payMethods = payment.getPaymentMethodsByPaymentBaseTypeRef(paymentType.getTechKey().getIdAsString(),
                                                                        PaymentBaseTypeData.BANK_TECHKEY.getIdAsString());

        if (payMethods.size() == 0) {
            log.debug("no bank transfer object found in PaymentBase instance");
        }
        else if (payMethods.size() > 1) {
            log.debug("more than one bank transfer object found in PaymentBase instance (currently shouldn't be available in B2C scenario!)");
        }
        else {
            bankTransfer = (BankTransfer) payMethods.get(0);
        }

        return bankTransfer;
    }
}
