/*
 * Created on Dec 20, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.isacore.uiclass.b2c;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.ui.accessibility.AccessKeysForApplication;
import com.sap.isa.ui.uiclass.AccessKeyUI;

/**
 * @author i800855
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class AccessKeyB2CUI extends AccessKeyUI
{
	static final private IsaLocation log = IsaLocation.getInstance(AccessKeyB2CUI.class.getName());
	
	protected void initAccessKeys()
	{
		final String METHOD_NAME = "initAccessKeys()";
		log.entering(METHOD_NAME);
		AccessKeysForApplication app = addApplication("b2c.catalog");
		app.addAccessKey("b2c.cat.compareitems.access", "b2c.cat.compareitems.access.inf");
		app.addAccessKey("b2c.cat.b2cprodheadarea.access", "b2c.cat.prodheadarea.access.inf");
		app.addAccessKey("b2c.cat.b2ccatarea.access", "b2c.cat.b2ccatarea.access.inf");
		app.addAccessKey("b2c.cat.b2cpatharea.access", "b2c.cat.b2cpatharea.access.inf");
		app.addAccessKey("b2c.cat.beginpage.access", "b2c.cat.beginpage.access.inf");
		app.addAccessKey("b2c.cat.lead.jsp.btnarea.access", "b2c.cat.lead.btnarea.access.inf");
		app.addAccessKey("b2c.cat.endpage.access", "b2c.cat.endpage.access.inf");
		app.addAccessKey("b2c.cat.lead.jsp.main.access", "b2c.cat.lead.jsp.main.access.inf");
		app.addAccessKey("b2c.cat.b2cbtnarea.access", "b2c.cat.b2cbtnarea.access.inf");
		app.addAccessKey("b2c.cat.blockview.begin.access", "b2c.cat.blockview.begin.access.inf");
		app.addAccessKey("b2c.cat.blockview.end.access", "b2c.cat.blockview.end.access.inf");

		app = addApplication("b2c.core");

		app = addApplication("b2c.ipc");
		app.addAccessKey("ipc.key.crm.buttons", "ipc.area.buttons");
		app.addAccessKey("ipc.key.crm.cstics", "ipc.area.cstics");
		app.addAccessKey("ipc.key.crm.buttons", "ipc.area.groups");
		app.addAccessKey("ipc.key.crm.instances", "ipc.area.instances");
		app.addAccessKey("ipc.key.crm.grouplist", "ipc.area.grouplist");
		app.addAccessKey("ipc.key.crm.mfa", "ipc.area.mfa");
		app.addAccessKey("ipc.key.crm.search", "ipc.area.search");
		app.addAccessKey("ipc.key.crm.accept", "ipc.key.info.button.accept");
		app.addAccessKey("ipc.key.crm.cancel", "ipc.key.info.button.cancel");
		app.addAccessKey("ipc.key.crm.reset", "ipc.key.info.button.reset");
		app.addAccessKey("ipc.key.crm.lastfocused", "ipc.key.info.element.lastfocused");
		log.exiting();
	}
}
