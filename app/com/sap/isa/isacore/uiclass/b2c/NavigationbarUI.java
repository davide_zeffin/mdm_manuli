/*****************************************************************************
    Class:        NavigationbarUI
    Copyright (c) 2007, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      11.01.2007
    Version:      1.0

    $Revision: #1 $
    $Date: 2007/01/11 $ 
*****************************************************************************/

package com.sap.isa.isacore.uiclass.b2c;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.PageContext;

import com.sap.ecommerce.businessobject.MarketingBusinessObjectManager;
import com.sap.ecommerce.businessobject.loyalty.LoyaltyMembership;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.User;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.util.SecureHandler;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.isacore.uiclass.IsaBaseUI;

/**
 * The class NavigationbarUI. <br>
 *  
 * The class provide the secure flag. 
 *
 * @author SAP
 * @version 1.0
 **/
public class NavigationbarUI extends IsaBaseUI {
       
    /**
     * @param pageContext
     */
    public NavigationbarUI(PageContext pageContext) {
        
        initContext(pageContext);
        
    }

    /**
      * Initialize UI clasee . <br>
      * 
      * @param pageContext current page context
      * 
      * 
      * @see com.sap.isa.ui.uiclass.BaseUI#initContext(javax.servlet.jsp.PageContext)
      */
     public void initContext(PageContext pageContext) {
        
        super.initContext(pageContext);
        
     }    
    
   /**
     * Returns the application-URL with the correct scheme (http or https)
     * 
     *@param  pageContext  the current PageContext
     *@param  name         part of the URL
     *@param  params       a sequence of name=value pairs separated with an '&'. 
     * 
     */
    public static String getWebappsURL(PageContext pageContext, String name, String params, String anchor, boolean completeURL) {
        
        UserSessionData userSessionData = UserSessionData.getUserSessionData(pageContext.getSession());
        
        String appsUrl = "";
        boolean secure = SecureHandler.isRequestSecure((HttpServletRequest)pageContext.getRequest());
        
        if (secure && SecureHandler.isSSLSwitchEnabled((HttpServletRequest)pageContext.getRequest()) && userSessionData != null) { 
                BusinessObjectManager bom = (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
                User user = bom.getUser();
                if (user == null || !user.isUserLogged()) {
                    secure = false;
                }
                appsUrl = WebUtil.getAppsURL(pageContext, new Boolean(secure), name, params, anchor, completeURL);
        }
        else {
            appsUrl = WebUtil.getAppsURL(pageContext, null, name, params, anchor, completeURL);
        }      
        
        return appsUrl;
    }
    
    /**
      * Returns a request parameter if the search should be done in the reward catalog 
      * 
      * @return reqParam as String, true if the search should be done in the reward catalog, false otherwise 
      * 
      */
     public String getSearchRewardCatalog() {
        String reqParam = "";
        if (request.getParameter("searchRewardCatalog") != null && 
            ((String)request.getParameter("searchRewardCatalog")).equals("X")) {
                reqParam = "?searchRewardCatalog=X"; 
        } 
        return reqParam;
     }
     
    /**
      * Returns a flag indicating if the advanced search should be performed  
      * That's only the case if a Loyalty Program is acitve for the shop an the user has a membership.
      * 
      * @return isAdvSearch as boolean, true if the search should be done as advanced search
      * 
      */
     public boolean isAdvSearch() {
         boolean isAdvSearch = shop.showCatQuickSearchInNavigationBar();
          
         if (shop.isEnabledLoyaltyProgram()){
             // reward catalog settings only if user has a Loyalty Membership
             MarketingBusinessObjectManager mktBom = (MarketingBusinessObjectManager) userSessionData.getBOM(MarketingBusinessObjectManager.MARKETING_BOM);
             LoyaltyMembership loyaltyMembership = mktBom.getLoyaltyMembership();

             if (loyaltyMembership != null && loyaltyMembership.exists()) {
                 // display catalog selection 
                 isAdvSearch = true;
             }
         }
         return(isAdvSearch);    
     }
     
    /**
     * Returns the language dependend description of the loyalty program
     * @return loyProgDescr, description of the loyalty program as String
     */
    public String getLoyProgDescr() {
        log.entering("getLoyProgDescr()");
        String loyProgDescr = shop.getLoyProgDescr();
        
        log.exiting();
        return loyProgDescr;
    }

}
