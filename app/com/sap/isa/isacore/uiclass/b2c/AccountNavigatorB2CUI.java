/*****************************************************************************
    Class:        AccountNavigatorB2CUI
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      07.03.2008
    Version:      1.0

    $Revision: 
    $Date:  
*****************************************************************************/

package com.sap.isa.isacore.uiclass.b2c;

import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.jsp.PageContext;

import com.sap.ecommerce.businessobject.MarketingBusinessObjectManager;
import com.sap.ecommerce.businessobject.loyalty.LoyaltyMembership;
import com.sap.isa.backend.boi.webcatalog.CatalogConfiguration;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.catalog.actions.ActionConstants;
import com.sap.isa.catalog.webcatalog.WebCatWeightedArea;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.ui.uiclass.BaseUI;

/**
 * UI class is used in accountNavigator.inc.jsp
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class AccountNavigatorB2CUI extends BaseUI
{
	static final private IsaLocation log = IsaLocation.getInstance(AccountNavigatorB2CUI.class.getName());
	
    /**
     * Constructor
     * @param pageContext
     */
    public AccountNavigatorB2CUI(PageContext pageContext) {
        super(pageContext);
    }
    
    /**
     * Checks if the Loyalty Program Information should be displayed
     * @return flag, if loyalty program will be displayed
     */
	public boolean isLoyaltyProgramDisplayed() {
		final String METHOD_NAME = "isLoyaltyProgramDisplayed()";
		log.entering(METHOD_NAME);
        
        boolean isLoyProgdisplayed = false;

        BusinessObjectManager bom = (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
        CatalogConfiguration catalogConfig = bom.getCatalogConfiguration();
        
        if (catalogConfig.isEnabledLoyaltyProgram()){
            MarketingBusinessObjectManager mktBom = (MarketingBusinessObjectManager) userSessionData.getBOM(MarketingBusinessObjectManager.MARKETING_BOM);
            
            if (mktBom != null) {
                LoyaltyMembership membShip = mktBom.getLoyaltyMembership();

			    // check if the user has a loyalty membership
			    if (membShip != null && membShip.exists()) { 
                    isLoyProgdisplayed = true;
			    }
            }
        }        
		log.exiting();
        return (isLoyProgdisplayed );
	}
    
    /**
     * Checks if the Reward catalog contains catalog area with area type 'LORC'.
     * @return flag, if reward catalog will be displayed
     */
    public boolean isRewardCatalogDisplayed() {
        final String METHOD_NAME = "isRewardCatalogDisplayed()";
        log.entering(METHOD_NAME);
        
        boolean isRCdisplayed = false;
        if (request.getAttribute(ActionConstants.RA_AREATREE) != null) {
            ArrayList areaTree = (ArrayList) request.getAttribute(ActionConstants.RA_AREATREE);
            Iterator theAreas = areaTree.iterator();
            int i=-1;
            while (theAreas.hasNext() && isRCdisplayed == false) {
                WebCatWeightedArea theWeightedArea = (WebCatWeightedArea) theAreas.next();
                if (theWeightedArea.getArea().isRewardCategory()) {
                    isRCdisplayed = true;
                }
            }
        }
        
        log.exiting();
        return (isRCdisplayed);
    }

    /**
     * Returns the language dependend description of the points unit for the current price product
     * @return ptsUnitDescr, description of points unit as String
     */
    public String getPointsUnitDescr() {
        log.entering("getPointsUnitDescr()");
        String ptsUnitDescr = "";

        MarketingBusinessObjectManager mktBom = (MarketingBusinessObjectManager) userSessionData.getBOM(MarketingBusinessObjectManager.MARKETING_BOM);
        LoyaltyMembership loyMembership = mktBom.getLoyaltyMembership();
        if (loyMembership != null &&
            loyMembership.getPointAccount() != null) {  
            ptsUnitDescr = loyMembership.getPointAccount().getDescr();  
        }  
        log.exiting();
        return ptsUnitDescr;
    }

    /**
     * Returns the language dependend description of the loyalty program
     * @return loyProgDescr, description of the loyalty program as String
     */
    public String getLoyProgDescr() {
        log.entering("getLoyProgDescr()");
        String loyProgDescr = "";

        BusinessObjectManager bom = (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
        Shop shop = bom.getShop();
        
        if (shop != null){
            loyProgDescr = shop.getLoyProgDescr();
        }        
        log.exiting();
        return loyProgDescr;
    }

}
