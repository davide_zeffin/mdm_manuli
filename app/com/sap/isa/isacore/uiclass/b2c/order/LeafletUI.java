package com.sap.isa.isacore.uiclass.b2c.order;

import javax.servlet.jsp.PageContext;

import com.sap.ecommerce.businessobject.MarketingBusinessObjectManager;
import com.sap.ecommerce.businessobject.loyalty.LoyaltyMembership;
import com.sap.isa.businessobject.Product;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.isacore.action.GetTransferItemFromProductAction;
import com.sap.isa.isacore.uiclass.PricingBaseUI;

public class LeafletUI extends PricingBaseUI {

    public static IsaLocation log =
		IsaLocation.getInstance(LeafletUI.class.getName());

	public LeafletUI(PageContext pageContext) {
		super(pageContext);
		log.entering("LeafletUI: constructor");
		log.exiting();
	}

	public String getAddToBasketLink(Product product) {
		log.entering("getAddToBasketLink");
		String itemToBasket =
			"b2c/leafletToBasket.do"
				+ GetTransferItemFromProductAction.createAddRequest(product, 1);
		String toBasketURL =
			WebUtil.getAppsURL(
				pageContext,
				new Boolean(request.isSecure()),
				itemToBasket,
				null,
				null,
				false);
		log.exiting();
		return toBasketURL;
	}

	public String removeProductLink(Product product) {
		log.entering("removeProductLink");

		String removeItem =
			"b2c/removeLeafletItem.do?techKey="
				+ product.getTechKey().getIdAsString();
		String removeURL =
			WebUtil.getAppsURL(
				pageContext,
				new Boolean(request.isSecure()),
				removeItem,
				null,
				null,
				false);
		log.exiting();
		return removeURL;
	}

    /**
     * Returns the points unit for the current price product
     * @return ptsUnit, points unit as String
     */
    public String getPointsUnit() {
        log.entering("getPointsUnit()");
        String ptsUnit = null;

        MarketingBusinessObjectManager mktBom = (MarketingBusinessObjectManager) userSessionData.getBOM(MarketingBusinessObjectManager.MARKETING_BOM);
        LoyaltyMembership loyMembership = mktBom.getLoyaltyMembership();
        if (loyMembership != null &&
            loyMembership.getPointAccount() != null) {  
            ptsUnit = loyMembership.getPointAccount().getPointCodeId();  
        }
        log.exiting();
        return ptsUnit;
    }

    /**
     * Returns the language dependen description of the points unit for the current price product
     * @return ptsUnitDescr, description of points unit as String
     */
    public String getPointsUnitDescr() {
        log.entering("getPointsUnitDescr()");
        String ptsUnitDescr = null;

        MarketingBusinessObjectManager mktBom = (MarketingBusinessObjectManager) userSessionData.getBOM(MarketingBusinessObjectManager.MARKETING_BOM);
        LoyaltyMembership loyMembership = mktBom.getLoyaltyMembership();
        if (loyMembership != null &&
            loyMembership.getPointAccount() != null) {  
            ptsUnitDescr = loyMembership.getPointAccount().getDescr();  
        }
        log.exiting();
        return ptsUnitDescr;
    }

}
