/*****************************************************************************
    Class:        PrintOrderB2CUI
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      15.12.2004
    Version:      1.0

    $Revision: #1$
    $Date: 2004/12/15 $ 
*****************************************************************************/
package com.sap.isa.isacore.uiclass.b2c.order;

import javax.servlet.jsp.PageContext;

import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.item.ItemList;
import com.sap.isa.isacore.action.b2c.B2cConstants;
import com.sap.isa.isacore.action.b2c.order.MaintainCheckoutB2CBaseAction;
import com.sap.isa.isacore.uiclass.b2b.order.SalesDocumentUI;

/**
 * The class PrintOrderB2CUI . <br>
 *
 * @author  SAP AG
 * @version 1.0
 *
 */
public class PrintOrderB2CUI  extends SalesDocumentUI  {

	/**
	 * @param pageContext
	 */
	public PrintOrderB2CUI(PageContext pageContext) {
		
		super(pageContext);
		
	}
	
	/**
	 * Overwrites the method determineHeader. <br>
	 * 
	 * Determine the header from the page context.
	 * The header is taken from the request parameter 
	 * {@link com.sap.isa.isacore.action.b2c.B2cConstants.RK_BASKET_HEADER}
	 */
	protected void determineHeader() {    	
		this.header = (HeaderSalesDocument) request.getAttribute(MaintainCheckoutB2CBaseAction.RC_HEADER);        
	}
	
	/**
	 * Overwrites the method determineItems. <br>
	 * 
	 * Determine the item list which should be displayed. <br>
	 * The items are taken from the request parameter. 
	 * 
	 * {@link com.sap.isa.isacore.action.b2c.B2cConstants.RK_BASKET_ITEM_LIST}
	 * 
	 */
	protected void determineItems() {
		this.items = (ItemList) request.getAttribute(B2cConstants.RK_BASKET_ITEM_LIST);	    
	}
	
	/**
	 * Overwrites the method determineSalesDocument. <br>
	 * 
	 * The method determine the used sales document. The sales document will be
	 * taken from the BusinessObjectManager. <br> 
	 * 
	 * @see com.sap.isa.isacore.uiclass.SalesDocumentBaseUI#determineSalesDocument()
	 * 
	 */
	protected void determineSalesDocument() {
		if (getBom() != null) {
			this.salesDoc = (SalesDocument) bom.getOrder();
		}

	}
	
	/**
	 * Overwrites the method determineMessages. <br>
	 * 
	 * Determine the Messages which should be displayed. <br>
	 * The messages are taken from the SalesDocument 
	 * 
	 */
	protected void determineMessages() {
		if (getBom() != null) {
			this.docMessages = bom.getOrder().getMessageList();
		}
	}
	/** Returns true, if at least one item of the item table contains a value for the 
	 * schedule lines 
	 * 
	 * @return boolean
	 * @param  String for attribute
	 * the param is concatanated within the listItemContainsValue with "get"
	 * and delivers so the method name of ItemBase       
	 */
   
	public boolean listItemContainsSchedlineValue () {
		return this.items.listItemContainsValue("ScheduleLines");
	}  
		

	/** Returns true if at least one item of the item table contains usage scope
	 * imported with the parameter 
	 * 
	 * @return boolean
	 * @param  String for method name 
	 * the param is concatanated within the listItemContainsValue with "get"
	 * and delivers so the method name of ItemBase       
	 */
	public boolean listItemContainsUsageScope () {
		return this.items.listItemContainsValue("UsageScope");
	}

}
