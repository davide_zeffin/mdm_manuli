/*****************************************************************************
    Class:        ConfirmB2CUI
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      03.12.2004
    Version:      1.0

    $Revision: #1$
    $Date: 2004/12/03 $ 
*****************************************************************************/
package com.sap.isa.isacore.uiclass.b2c.order;

import javax.servlet.jsp.PageContext;

import com.sap.isa.backend.boi.isacore.order.ItemBaseData;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.item.ItemList;
import com.sap.isa.isacore.action.b2c.B2cConstants;
import com.sap.isa.isacore.action.b2c.order.MaintainCheckoutB2CBaseAction;
import com.sap.isa.isacore.uiclass.b2b.order.SalesDocumentUI;

/**
 * The class ConfirmB2CUI . <br>
 *
 * @author  SAP AG
 * @version 1.0
 *
 */
public class ConfirmB2CUI extends SalesDocumentUI  {

	/**
	 * @param pageContext
	 */
	public ConfirmB2CUI(PageContext pageContext) {
		
		super(pageContext);
		
	}

	/**
	 * Overwrites the method determineHeader. <br>
	 * 
	 * Determine the header from the page context.
	 * The header is taken from the request parameter 
	 * {@link com.sap.isa.isacore.action.b2c.B2cConstants.RK_BASKET_HEADER}
	 */
	protected void determineHeader() {    	
		this.header = (HeaderSalesDocument) request.getAttribute(MaintainCheckoutB2CBaseAction.RC_HEADER);        
	}
	
	/**
	 * Overwrites the method determineItems. <br>
	 * 
	 * Determine the item list which should be displayed. <br>
	 * The items are taken from the request parameter. 
	 * 
	 * {@link com.sap.isa.isacore.action.b2c.B2cConstants.RK_BASKET_ITEM_LIST}
	 * 
	 */
	protected void determineItems() {
		this.items = (ItemList) request.getAttribute(B2cConstants.RK_BASKET_ITEM_LIST);	    
	}
	
	/**
	 * Overwrites the method determineSalesDocument. <br>
	 * 
	 * The method determine the used sales document. The sales document will be
	 * taken from the BusinessObjectManager. <br> 
	 * 
	 * @see com.sap.isa.isacore.uiclass.SalesDocumentBaseUI#determineSalesDocument()
	 * 
	 */
	protected void determineSalesDocument() {
		if (getBom() != null) {
			this.salesDoc = (SalesDocument) bom.getBasket();
		}

	}
	
	/**
	 * Overwrites the method determineMessages. <br>
	 * 
	 * Determine the Messages which should be displayed. <br>
	 * The messages are taken from the SalesDocument 
	 * 
	 */
	protected void determineMessages() {
		if (getBom() != null) {
			this.docMessages = bom.getOrder().getMessageList();
		}
	}
	
	/**
	 * Returns the delivery priority Id.
	 * 
     * @return String
     */
    public String getDeliveryPriorityId() {
		String retId = "";
		if (getBom() != null) {
			retId = bom.getOrder().getHeader().getDeliveryPriority();
		}
		return retId;				
	}
    
    /**
     * Returns TRUE if in the item list at least one item with usage scope 
     * {@link ItemBaseData.USAGE_SCOPE_DEFAULT} exists.
     * 
     * @return boolean
     */
    public boolean itemListContainsUsageScopeRatePlan() {
    	return this.items.listItemContainsUsageScope(ItemBaseData.USAGE_SCOPE_DEFAULT);
    }

    /**
     * Returns TRUE if in the item list at least one item with usage scope 
     * {@link ItemBaseData.USAGE_SCOPE_DIFFERENT} exists.
     * 
     * @return boolean
     */
    public boolean itemListContainsUsageScopeGoods() {
    	return this.items.listItemContainsUsageScope(ItemBaseData.USAGE_SCOPE_DIFFERENT);
    }

    /**
     * Returns TRUE if in the item list at least one item without any usage scope 
     * exists. For instance, possible usage scopes could be {@link ItemBaseData.USAGE_SCOPE_DIFFERENT}
     * or {@link ItemBaseData.USAGE_SCOPE_DEFAULT}.
     * 
     * @return boolean
     */
    public boolean itemListContainsNotDefinedUsageScope() {
    	return this.items.listItemContainsUsageScope("");
    }
    
    /** Returns true, if at least one item of the item table contains a value for the 
     * schedule lines 
     * 
     * @return boolean
     * @param  String for attribute
     * the param is concatanated within the listItemContainsValue with "get"
     * and delivers so the method name of ItemBase       
     */
   
	public boolean listItemContainsSchedlineValue () {
		return this.items.listItemContainsValue("ScheduleLines");
	}  
	
	
	/** Returns true if at least one item of the item table contains usage scope
	 * imported with the parameter 
	 * 
	 * @return boolean
	 * @param  String for method name 
     * the param is concatanated within the listItemContainsValue with "get"
     * and delivers so the method name of ItemBase       
	 */
	public boolean listItemContainsUsageScope () {
		return this.items.listItemContainsValue("UsageScope");
	}	  
	 
	 /**
	  * returns the column which gets the style last in the item table
	  * at checkout.inc.jsp, confirm.inc.jsp, printorder.jsp
	  * @param is the page name, from where the method is called
	  * @return the column name
	  * e.g. cat: Category; quant: Quantity etc.
	  */
	public String getNameOfItemListLastColumn (String pagename) {
		String column = "";
		Object shop = request.getAttribute("Shop");
		boolean isBrandOwnerScenario = ((Shop) shop).isPartnerBasket();
		if (pagename.equals("checkout.inc.jsp")){
			if (this.listItemContainsUsageScope()){
				column = "cat";
			}
			else {
				column = "quant";
			}
			return column;
		}
		else if (pagename.equals("confirm.inc.jsp")){
			if (isBrandOwnerScenario || (!isBrandOwnerScenario && !this.listItemContainsSchedlineValue())){
				column = "price";
				return column; 
			}
			else if (isBrandOwnerScenario && this.listItemContainsSchedlineValue()){
				column = "delivQuant";
				return column;
		    }						
		}
		else {
			// TODO what if printorder
			return column;
		}
		return column;
						
	}
}
