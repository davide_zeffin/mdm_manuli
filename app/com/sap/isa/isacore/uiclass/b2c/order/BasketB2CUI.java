/*****************************************************************************
    Class:        BasketB2CUI
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      01.12.2004
    Version:      1.0

    $Revision: #1$
    $Date: 2004/12/01 $ 
*****************************************************************************/
package com.sap.isa.isacore.uiclass.b2c.order;

import javax.servlet.jsp.PageContext;

import com.sap.isa.backend.boi.isacore.order.ItemBaseData;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.item.ItemList;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.isacore.action.b2c.B2cConstants;
import com.sap.isa.isacore.uiclass.b2b.order.SalesDocumentUI;


/**
 * The class BasketB2CUI . <br>
 *
 * @author  SAP AG
 * @version 1.0
 *
 */
public class BasketB2CUI extends SalesDocumentUI {

	/**
	 * @param pageContext
	 */
	public BasketB2CUI(PageContext pageContext) {
		
		super(pageContext);
		
	}
	
	/**
     * Overwrites the method determineHeader. <br>
	 * 
	 * Determine the header from the page context.
	 * The header is taken from the request parameter 
	 * {@link com.sap.isa.isacore.action.b2c.B2cConstants.RK_BASKET_HEADER}
	 */
	protected void determineHeader() {    	
		this.header = (HeaderSalesDocument) request.getAttribute(B2cConstants.RK_BASKET_HEADER);        
	}
	
	/**
     * Overwrites the method determineItems. <br>
	 * 
	 * Determine the item list which should be displayed. <br>
	 * The items are taken from the request parameter. 
	 * 
	 * {@link com.sap.isa.isacore.action.b2c.B2cConstants.RK_BASKET_ITEM_LIST}
	 * 
	 */
	protected void determineItems() {
		this.items = (ItemList) request.getAttribute(B2cConstants.RK_BASKET_ITEM_LIST);	    
	}
	
	/**
	 * Overwrites the method determineSalesDocument. <br>
	 * 
	 * The method determine the used sales document. The sales document will be
	 * taken from the BusinessObjectManager. <br> 
	 * 
	 * @see com.sap.isa.isacore.uiclass.SalesDocumentBaseUI#determineSalesDocument()
	 * 
	 */
	protected void determineSalesDocument() {
		if (getBom() != null) {
			salesDoc = (SalesDocument) bom.getBasket();
		}

	}
	
	/**
	 * Overwrites the method determineMessages. <br>
	 * 
	 * Determine the Messages which should be displayed. <br>
	 * The messages are taken from the SalesDocument 
	 * 
	 */
	protected void determineMessages() {
		if (salesDoc != null) {
			this.docMessages = salesDoc.getMessageList();
		}
	}				

	/**
	 * Overwrites the method isSoldToVisible
	 * 
	 * @return <code>false</code>
	 *          
	 */
	public boolean isSoldToVisible() {
		return false;
	}
	
	/**
	 * Returns true if the campaign has affect on one item
	 * in the shopping basket. <br>
	 * 
     * @return <code>true</code> or <code>false</code>
     */
    public boolean isAnyItemAffected() {
		boolean retVal = false;
		for (int i = 0; i < this.items.size(); i++) {
			if (items.get(i).getAssignedCampaigns() != null && 
			     items.get(i).getAssignedCampaigns().size() > 0) {
			     	return true;
			     }
		}
		
		return retVal;
	}
	
	/**
	 * Returns <code>true</code> if the item is affected from
	 * the campaign.
	 * 
     * @param idx
     * @return boolean
     */
    public boolean isItemAffected(int idx) {
    	if (isManualCampaignEntryAllowed() || isHeaderCampaignListSet()) {
			if (items.get(idx).getAssignedCampaigns() != null &&
				 items.get(idx).getAssignedCampaigns().size() > 0) {
					return true;
			}
    	}

		return false;
	}
	
	/**
	 * Returns <code>true</code> if the basket is empty
	 * 
	 * @return boolean
	 */
	public boolean isBasketEmpty() {
	  return items.isEmpty();	
	}	

	/**
	 * Returns <code>true</code> if the item is changeable via the solution configurator
	 * 
	 * @return boolean
	 */
	public boolean isItemScChangeable() {
	  if (this.header.getScDocumentGuid() != null && !this.header.getScDocumentGuid().isInitial() &&
	     ( (itemHierarchy.hasSCSubItem(item) && !isScSubItem())
            || (isMainProductFromPackage() && !itemHierarchy.hasSCSubItem(item))
	        || (isScSubItem() && item.isScAlternativeProductAvailable())
	        || (isScSubItem() && item.isDeletable()))) {
	  	return true;	
	  } else {
	  	return false;
	  }
	}	

    
	/**
	 * returns <code>true</code> if the item is subitem of a package component (Telco)
	 * or true if the item is not a package component AND the root element
	 * 
	 */
	public boolean isItemQuantityChangeable(ItemSalesDoc item) {
	   boolean isChangeable = true;
       if (itemHasAuctionRelation(item)) {
           isChangeable = false;
       }
       else {
           if (item.getProductRole() != null && item.getProductRole().length() > 0) {
            isChangeable = !(item.getProductRole().equals(ItemBaseData.PRODUCT_ROLE_SALES_PACKAGE) || item.getProductRole().equalsIgnoreCase(ItemBaseData.PRODUCT_ROLE_COMBINED_RATE_PLAN) || item.getProductRole().equalsIgnoreCase(ItemBaseData.PRODUCT_ROLE_RATE_PLAN));
           }
       }

	   return isChangeable;
	}  
    
    /**
     * returns <code>true</code> if the item is subitem of a package component (Telco)
     * or true if the item is not a package component AND the root element
     * 
     */
    public boolean isItemConfigurable(ItemSalesDoc item) {
       return !itemHasAuctionRelation(item);
    }
    
    /**
     * returns <code>true</code> if the item is subitem of a package component (Telco)
     * or true if the item is not a package component AND the root element
     * 
     */
    public boolean itemHasAuctionRelation(ItemSalesDoc item) {
       return (item.getAuctionGuid() != null && item.getAuctionGuid().getIdAsString().length() > 0);
    }
    
}
