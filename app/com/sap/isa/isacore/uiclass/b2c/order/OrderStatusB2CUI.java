/*****************************************************************************
    Class:        OrderStatusB2CUI
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      15.12.2004
    Version:      1.0

    $Revision: #1$
    $Date: 2004/12/15 $ 
*****************************************************************************/
package com.sap.isa.isacore.uiclass.b2c.order;

import javax.servlet.jsp.PageContext;

import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.order.DocumentListFilterData;
import com.sap.isa.backend.boi.isacore.order.ItemBaseData;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.businessobject.order.OrderStatus;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.isacore.action.b2c.order.DocumentStatusDetailAction;
import com.sap.isa.isacore.uiclass.SalesDocumentDataUI;
import com.sap.isa.isacore.uiclass.SalesDocumentStatusBaseUI;

/**
 * The class OrderStatusB2CUI . <br>
 *
 * @author  SAP AG
 * @version 1.0
 *
 */

public class OrderStatusB2CUI  extends SalesDocumentDataUI {
	
	/** 
	 * The switch for R/3 backend. <br>
	 */	
	protected boolean backendR3   = false;

	
	/** 
	 * The switch for R/3 backend with plug in
	 */	
	protected boolean backendR3PI = false;
	

	protected OrderStatus orderStatus = null;
	
	protected Shop shop;
	
	protected BusinessObjectManager bom;	
	static final private IsaLocation log = IsaLocation.getInstance(OrderStatusB2CUI.class.getName());
	
	/**
	 * Constructor for OrderStatusUI. <br>
	 * 
	 * @param pageContext
	 */
	public OrderStatusB2CUI(PageContext pageContext) {
		initContext(pageContext);
	}

	/**
	 * Initialize the the object from the page context. <br>
	 * 
	 * @param pageContext page context
	 */
	public void initContext(PageContext pageContext) {
		final String METHOD_NAME = "initContext()";
		log.entering(METHOD_NAME);
		super.initContext(pageContext);
		initDocument();
		log.exiting();
	}
	
	/**
	 * Returns the shop.
	 * @return Shop
	 */
	public Shop getShop() {
		return shop;
	}	
		
	/**
	 * Only dummy because there is no salesDocument involved . <br>
	 * 
	 * @see com.sap.isa.isacore.uiclass.SalesDocumentBaseUI#determineSalesDocument()
	 */
	protected void determineSalesDocument() {
	}
	
	/**
	 * Determine the message list from the page context.
	 * The message list is taken salesDocStatus which is provided by the DocumentHandler (getDocumentOnTop())
	 * @see com.sap.isa.isacore.uiclass.SalesDocumentBaseUI#determineMessages()
	 */
	protected void determineMessages() {
		this.docMessages = orderStatus.getMessageList();
	}	
	
	/**
	 * Determine the item list from the page context.
	 * The item list is taken from salesDocStatus which is provided by the DocumentHandler (getDocumentOnTop()) 
	 * @see com.sap.isa.isacore.uiclass.SalesDocumentBaseUI#determineItems()
	 */
	protected void determineItems() {
		this.items = orderStatus.getItemList();
	}
	
	/**
	 * Determine the header from the salesDocStatus which is provided by the DocumentHandler (getDocumentOnTop())
	 * @see com.sap.isa.isacore.uiclass.SalesDocumentBaseUI#determineHeader()
	 */
	protected void determineHeader() {		
		this.header = orderStatus.getOrderHeader();
	}
	
	/**
	 * Init Document
	 */
	protected void initDocument() {
		orderStatus = (OrderStatus) request.getAttribute(DocumentStatusDetailAction.RK_ORDER_STATUS_DETAIL);
	}	
	
	/**
	 * Set the shop object in the salesDocConfiguration property. <br>
	 * 
	 * @see com.sap.isa.isacore.uiclass.order.OrderDataUI#setSalesDocConfiguration()
	 */
	protected void setSalesDocConfiguration() {
    	
		// check for missing context
		if (userSessionData != null) {
			// get BOM
			bom = (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
	
			shop = bom.getShop();
			
			// now I can read the shop
			salesDocConfiguration = bom.getShop();
		}		
	}

	/**
	 * Returns the header campaign description and campaign type description for the given index
	 * 
	 * @param intger idx the idx of the header campaign to look for
	 * @return String returns the header campaign description and campaign type description for the given index
	 */
	public String getHeaderCampaignDescs(int idx) {
		String descs = "";
		
		if (header != null && header.getAssignedCampaigns() != null && 
			header.getAssignedCampaigns().getCampaign(idx) != null) {
			String campGuid = header.
			                  getAssignedCampaigns().
			                  getCampaign(idx).
			                  getCampaignGUID().
			                  getIdAsString();
			String campTypeId = header.getAssignedCampaigns().getCampaign(idx).getCampaignTypeId();
			descs = (String) orderStatus.getCampaignDescriptions().get(campGuid);
			if (orderStatus.getCampaignTypeDescriptions().get(campTypeId) != null &&
			    ((String) orderStatus.getCampaignTypeDescriptions().get(campTypeId)).length() > 0) {
				descs = descs + " (" + orderStatus.getCampaignTypeDescriptions().get(campTypeId) + ")";
			}
		}
		
		return descs;
	}
	
	/**
	 * Only dummy
	 * 
	 * Set the property {@link #processTypeInfo}. 
	 */
	protected void setProcessTypeInfo() {
	}
	
	
	/**
	 * Only dummy
	 * 
	 * Return if the sold to should be displayed on the page. <br>
	 * 
	 * @return <code>true</code>, if the sold to should be displayed on the 
	 *          JSP
	 */
	public boolean isSoldToVisible() {
		return false;
	}

	/**
	 * Only dummy
	 * 
	 * Return the description of the substitution reason for the given key. <br>
	 * 
	 * @param key key for the substitution reason
	 * @return description of the substitution reason
	 */
	protected  String getSubstitutionReasonDesc(String key) {
		return null;
	}

	/** 
	 * Only dummy
	 * 
	 * Return the business partner object for the given sold to key. <br>
	 * 
	 * @param  soldToKey
	 * @return business partner object, for the given business partner key 
	 */
	protected BusinessPartner getSoldTo(TechKey soldToKey) {
		return null; 
	}
	
	/**
	 * Returns true if the Delivery Priority should be displayed.
	 * 
     * @return boolean
     */
    public boolean isDeliveryPriorityVisible() {
		boolean retVal = false;
		
		String shopBackend = shop.getBackend();

		backendR3PI = shopBackend.equals(Shop.BACKEND_R3_PI);

		backendR3 = shopBackend.equals(Shop.BACKEND_R3) || shopBackend.equals(Shop.BACKEND_R3_40) ||
							shopBackend.equals(Shop.BACKEND_R3_45) || shopBackend.equals(Shop.BACKEND_R3_46) ||
							backendR3PI;
		
		if (!backendR3 && ! shop.isPartnerBasket()) {
			if (isElementVisible("order.deliveryPriority") &&
				header.getDeliveryPriority() != null && 
				header.getDeliveryPriority().length() > 0) {
					retVal = true;
			}
		}
		
		return retVal;
	}
	
	/**
	 * Returns true if the select box should be displayed.
	 * 
	 * @return boolean
	 */
	public boolean showSelectBox(ItemSalesDoc orderitem, String selectedOrder) {	
		String ZERO = "00000000000000000000000000000000";
		
		if ( !(shop.getScenario().equalsIgnoreCase(ShopData.TELCO_CUSOMER_SELF_SERVICE) && (selectedOrder.equalsIgnoreCase("COMPLETED_ORDERS") || orderitem.getStatus().equals(DocumentListFilterData.SALESDOCUMENT_STATUS_CANCELLED) || orderitem.getStatus().equals(DocumentListFilterData.SALESDOCUMENT_STATUS_COMPLETED)))  
	   		&& ( (orderitem.getProductId() != null  &&  ! orderitem.getProductId().equals(ZERO)) &&  (!itemHierarchy.isSubItem(orderitem) || !(itemHierarchy.isRootConfigurable(orderitem) || orderitem.isItemUsageBOM() || isScSubItem())))) { 
	   		return true;
	   	} else
	   		return false;
		}
		
	/**
	 * returns <code>true</code> if the itemlist contains at leats one rate plan or 
	 * combined rate plan product (Telco)
	 */
	public boolean showContractStartDate() {
        
	   boolean showStartDate = false;
	   ItemSalesDoc item = null;
       
	   for (int i = 0; i < items.size(); i++) {
		   item = items.get(i);
		   if (item.getProductRole() != null && item.getProductRole().length() > 0 && 
			   (item.getProductRole().equalsIgnoreCase(ItemBaseData.PRODUCT_ROLE_COMBINED_RATE_PLAN) || 
				item.getProductRole().equalsIgnoreCase(ItemBaseData.PRODUCT_ROLE_RATE_PLAN))) {
			   showStartDate = true;
			   break;
		   }
	   }

	   return showStartDate;
	} 
	
	/**
	 * returns <code>true</code> if there is a header text to be displayed.
	 */
	public boolean showHeaderText() {
        
	   boolean showHeaderText = false;
       
	   showHeaderText = header.getText() != null && header.getText().getText() != null && header.getText().getText().length() > 0;

	   return showHeaderText;
	}      
 
}
