package com.sap.isa.isacore.uiclass.b2c.order;

import javax.servlet.jsp.PageContext;

import com.sap.isa.backend.boi.isacore.order.ItemBaseData;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.item.ItemList;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.isacore.action.b2c.B2cConstants;
import com.sap.isa.ui.uiclass.BaseUI;

public class MiniBasketB2CUI extends BaseUI {

	private static IsaLocation log =
		IsaLocation.getInstance(MiniBasketB2CUI.class.getName());

    /**
     * Hold the the header inforamtion. <br>
     */
    public HeaderSalesDocument header;  
    
    /**
     * Hold the items.
     */
    public ItemList items;
    
	public MiniBasketB2CUI(PageContext pageContext) {
		super(pageContext);
		log.entering("MiniBasketB2CUI: constructor");
		log.exiting();
	}
	
    /**
     * Initialize the object from the page context. <br />
     * 
     * @param pageContext page context
     */
    public void initContext(PageContext pageContext) {
        final String METHOD_NAME = "initContext()";
        log.entering(METHOD_NAME);
        super.initContext(pageContext);

        determineHeader();
        determineItems();
    }

    /**
     * Determine the item list which should be displayed. <br>
     * The items are taken from the request parameter.
     * 
     * {@link com.sap.isa.isacore.action.b2c.B2cConstants.RK_BASKET_ITEM_LIST}
     * 
     */
    protected void determineItems() {
        this.items = (ItemList) request.getAttribute(B2cConstants.RK_BASKET_ITEM_LIST);
    }
    
    /**
     * Overwrites the method determineHeader. <br>
     * 
     * Determine the header from the page context. The header is taken from the
     * request parameter
     * {@link com.sap.isa.isacore.action.b2c.B2cConstants.RK_BASKET_HEADER}
     */
    protected void determineHeader() {
        this.header = (HeaderSalesDocument) request.getAttribute(B2cConstants.RK_BASKET_HEADER);
    }
    
	/**
	 * return true if this item should be inserted in the mini basket. 
	 * Telco case: dependent component and rate plan combination shouldn't be inserted in the miniBasket
	 */
	public boolean shouldBeDisplayedInTheMiniBasket(ItemSalesDoc item) {
		log.entering("shouldBeDisplayedInTheMiniBasket");
		boolean response = true;
		if (item.getItmUsage() != null) {

			if (item.getItmUsage().equals(ItemBaseData.ITEM_USAGE_SC_DEPENDENT_COMPONENT)
				|| item.getItmUsage().equals(ItemBaseData.ITEM_USAGE_SC_RATE_PLAN_COMBINATION_COMPONENT)) {
				response = false;
			}
		}
		log.debug("return: " + response);
		log.exiting();
		return response;
	}
    
    /**
     * Returns the number of positions in the itemlist.<br/ >
     *
     * @return the number of positions in the itemlist.
     */
    public int getNumBasketItems() {
     
        int numItems = 0;
        if (this.items != null) {
            numItems = this.items.size();
        }
        return numItems;
    }

    /**
     * Returns the number of main positions in the itemlist.<br/ >
     * This is a wrapper of the method {@link ItemList#numberOfMainPositions()}.<br /><br />
     * In contrast to method {@link #getNumBasketItems()} item positions that are sublevel<br>
     * positions to an assigned top level item are not counted.
     *
     * @return the number of main (toplevel) positions in the itemlist.
     */
    public int getNumBasketMainItems() {
        
        int numItems = 0;
        if (this.items != null) {
            numItems = this.items.numberOfMainPositions();
        }
        return numItems;
    }
}
