package com.sap.isa.isacore.uiclass.b2c;

import java.util.Iterator;

import javax.servlet.jsp.PageContext;

import com.sap.isa.businessobject.header.HeaderBase;
import com.sap.isa.businessobject.item.ItemBase;
import com.sap.isa.businessobject.item.ItemList;
import com.sap.isa.ui.uiclass.BaseUI;

public class RecurringPriceUI extends BaseUI {
    
    private boolean isRecurring = false;
    private boolean isNonRecurring = false;
    private String recurringTimeUnit = null;
    private String recurringTimeUnitPl = null;
    private String recurringTimeUnitAbr = null;
    private String recurringTaxValue = null;
    private String recurringGrossValue = null;
    private String recurringNetValue = null;
    private String recurringDuration = null;
    private String nonRecurringTaxValue = null;
    private String nonRecurringNetValue = null;
    private String nonRecurringGrossValue = null;
    private boolean followOnInstance = false;

    public RecurringPriceUI(PageContext pageContext) {
        initContext(pageContext);
    }

    public RecurringPriceUI(PageContext pageContext, boolean isFollowOnInstance) {
    	if (isFollowOnInstance) {
    		followOnInstance = true;
    	}
    }
    
    public RecurringPriceUI setHeader(HeaderBase headerBase) {
        initHeader(headerBase);
        return this;
    }
    
    private String Trimmer(String item) {
        return (item == null) ? "" : item.trim();
    }
    
    private int CountChar(String src, char testCh) {
        int retVal = 0;
        
        for (int i=0; i < src.length(); i++) {
            if (src.charAt(i) == testCh)
                retVal++;
        }
        
        return retVal;
    }
    

    private double parseDouble(String value) {
        double dbl = 0.0;
        String orgValue = value;
        
        value = (value == null) ? "0" : value.trim();
        
        try {
            if (value.length() > 0) {
                int cntCol = CountChar(value, ',');
                int cntDot = CountChar(value, '.');
                String decimalSep="";
                String thousendSep="";
                
                if (!( (cntCol > 0) && (cntDot > 0))) {
                    // only one kind
                    int comp = cntCol - cntDot;
                    
                    switch (comp) {
                        case 0: // no Seps at all
                            decimalSep = "";
                            break;
                        case 1: // only one Sep, could be thsnd or decimal
                            decimalSep = ","; // Let's guess
                            break;
                        case -1: // only one Sep, could be thsnd or decimal
                            decimalSep = "."; // Let's guess
                            break;

                        default: // more than one, must be thsnd sep!
                            if (comp > 0) {
                                thousendSep = ","; 
                            }
                            else {
                                thousendSep = "\\.";
                            }
                    }
                }
                else {
                    // that's easy, the right most must be the decimal
                    int colPos = value.indexOf(',');
                    int pntPos = value.indexOf('.');
                    
                    if (colPos < pntPos) {
                        decimalSep = ".";
                        thousendSep = ","; 
                    }
                    else {
                        decimalSep = ",";
                        thousendSep = "\\.";
                    }
                }
                         
                if (thousendSep.length() > 0 ) {
                    value = value.replaceAll(thousendSep,"");
                }
                
                if (decimalSep.equals(",")) {
                    value = value.replaceFirst(decimalSep,".");
                }
                else if (decimalSep.length() == 0) {
                    value = value + ".0"; // no decimal Separator, simulate one
                }
                
                dbl = Double.parseDouble(value);
            }
        }
        catch (NumberFormatException ex) {
            if (log.isDebugEnabled()) {
                log.debug("Error ", ex);
            }
        }
        
        return dbl;
    }

    private int parseInt(String value) {
        int n = 0;
        
        value = (value == null) ? "0" : value.trim();
        
        try {
            if (value.length() > 0) {
                n = Integer.parseInt(value);
            }
        }
        catch (NumberFormatException ex) {
			if (log.isDebugEnabled()) {
				log.debug("Error ", ex);
			}
        }
        
        return n;
    }
    
    
    /*
     * If nonrecurring values are supplied they have to be used instead of the 'normal' values.
     * Combination of 'normal' and recurring values is not allowed.
     */
    private void initHeader(HeaderBase headerBase) {
        double recurringGross = 0.0;
        double nonRecurringGross = 0.0;
        double grossValue = 0.0;
        int recDuration = 0;

        nonRecurringGrossValue = Trimmer(headerBase.getNonRecurringGrossValue());
        nonRecurringGross = parseDouble( nonRecurringGrossValue );

        recurringGrossValue = Trimmer(headerBase.getRecurringGrossValue());
        recurringGross = parseDouble( recurringGrossValue );

        grossValue = parseDouble( Trimmer(headerBase.getGrossValue()) );
       
		recurringDuration = Trimmer(headerBase.getRecurringDuration());
        recDuration = parseInt(recurringDuration);

        if (nonRecurringGrossValue.length() > 0 && nonRecurringGross != 0.0 || recDuration != 0 && recurringGross != 0.0) {
            /* header has true nonrecurring part, ignore "old" gross value */	
            isNonRecurring = true;
            /* has header recurring part ? */ 
            isRecurring = (recDuration > 0) && recurringGross != 0.0;
            if (!isRecurring) {
                ClearRecurring(); 
            }
            else {
                recurringGrossValue = Trimmer(headerBase.getRecurringGrossValue());
                recurringTimeUnit = Trimmer(headerBase.getRecurringTimeUnit());
                recurringTimeUnitPl = Trimmer(headerBase.getRecurringTimeUnitPlural());
                recurringTimeUnitAbr = Trimmer(headerBase.getRecurringTimeUnitAbr());
                recurringTaxValue = Trimmer(headerBase.getRecurringTaxValue());
                recurringNetValue = Trimmer(headerBase.getRecurringNetValue());
            }
        
            nonRecurringTaxValue = Trimmer(headerBase.getNonRecurringTaxValue());
            nonRecurringNetValue = Trimmer(headerBase.getNonRecurringNetValue());
            nonRecurringGrossValue = Trimmer(headerBase.getNonRecurringGrossValue());
        }
        else { /* header has only valid gross value, rec. and nonrec. are invalid */
            isRecurring = false;
            isNonRecurring = false;
            ClearRecurring(); 
            nonRecurringTaxValue = Trimmer(headerBase.getTaxValue());
            nonRecurringNetValue = Trimmer(headerBase.getNetValue());
            nonRecurringGrossValue = Trimmer(headerBase.getGrossValue());
        }
        
    }

    private void ClearRecurring() {
        recurringTimeUnit = "";
        recurringTimeUnitPl = "";
        recurringTimeUnitAbr = "";
        recurringTaxValue = "";
        recurringGrossValue = "";
        recurringNetValue = "";
        recurringDuration = "";
    }

    private void ClearNonRecurring() {
        nonRecurringTaxValue = "";
        nonRecurringNetValue = "";
        nonRecurringGrossValue = "";
    }

    /**
     * Initilizes ui class for use with items
     * @return the object itself 
     */
    public RecurringPriceUI setItem(ItemBase itemBase) {
        initItem(itemBase);
        return this;
    }

    private void initItem(ItemBase itemBase) {
        double recurringGross = 0.0;
        int recDuration = 0;
                
        nonRecurringGrossValue = Trimmer(itemBase.getGrossValue());
        recurringGrossValue = Trimmer(itemBase.getRecurringGrossValue());
        

   		recurringDuration = Trimmer(itemBase.getRecurringDuration());
        recDuration = parseInt(recurringDuration);
        
        recurringGross = parseDouble( recurringGrossValue );

		isRecurring = recDuration > 0 && recurringGross != 0.0;
        /* item is nonrecurring or recurring - never both */    
        isNonRecurring = !isRecurring;
        
        if (!isRecurring) {
            ClearRecurring(); 
            nonRecurringTaxValue = Trimmer(itemBase.getTaxValue());
            nonRecurringNetValue = Trimmer(itemBase.getNetValue());
            nonRecurringGrossValue = Trimmer(itemBase.getGrossValue());
        }
        else {
            ClearNonRecurring(); 
            recurringGrossValue = Trimmer(itemBase.getRecurringGrossValue());
            recurringTimeUnit = Trimmer(itemBase.getRecurringTimeUnit());
            recurringTimeUnitPl = Trimmer(itemBase.getRecurringTimeUnitPlural());
            recurringTimeUnitAbr = Trimmer(itemBase.getRecurringTimeUnitAbr());
            recurringTaxValue = Trimmer(itemBase.getRecurringTaxValue());
            recurringNetValue = Trimmer(itemBase.getRecurringNetValue());
            recurringDuration = Trimmer(itemBase.getRecurringDuration());
        }
        
    }

    public void initContext(PageContext pageContext) {
        super.initContext(pageContext);
    }
    
    /**
     * Checks for recurring price information on the items.  
     * @param header which should be checked
     * @param itemList which should be checked
     * @return true if one of the items 
     */
    public boolean isRecurringPriceIncluded(HeaderBase header, ItemList itemList) {
    	// In case of a item cancellation the header values for recurring price information will
    	// be empty. So it has to be checked on item level too for recurring price information
		boolean retVal = false;
    	if ( ! followOnInstance) {
    		// To not destroy the current content of class instance originally created from the JSP,
    		// create a new one for checking purposses
    		RecurringPriceUI rp_ui = new RecurringPriceUI(pageContext, true);
    		retVal = rp_ui.isRecurringPriceIncluded(header, itemList);
    	} else {
			initHeader(header);
			if (isRecurring) {
				retVal = true;
			} else {
				Iterator itemIT = itemList.iterator();
				while (itemIT.hasNext()  &&  !retVal) {
					initItem((ItemBase)itemIT.next());
					if (isRecurring) {
						retVal = true;
					}
				}
			}
		}
    	return retVal;
    } 
    
    public boolean isRecurringPrice() {
        return isRecurring;
    }
    
    /*
     * items: returns always false
     * header: returns true if nonrecurrent price is not used
     */
    public boolean isNormalPrice() {
        return !isNonRecurring;
    }
    
    /**
     * Returns the non recurring Gross value
     * @return the non recurring Gross value
     */
    public String getNonRecurringGrossValue() {
        return nonRecurringGrossValue;
    }

    /**
     * Returns the non recurring Net value
     * @return the non recurring Net value
     */
    public String getNonRecurringNetValue() {
        return nonRecurringNetValue;
    }

    /**
     * Returns the non recurring Tax value
     * @return the non recurring Tax value
     */
    public String getNonRecurringTaxValue() {
        return nonRecurringTaxValue;
    }

    /**
     * Returns the recurring Duration
     * @return the recurring Duration
     */
    public String getRecurringDuration() {
        return recurringDuration;
    }
    
    /**
     * Returns the recurring Net value
     * @return the recurring Net value
     */
    public String getRecurringNetValue() {
        return recurringNetValue;
    }

    /**
     * Returns the recurring Gross value
     * @return the recurring Gross value
     */
    public String getRecurringGrossValue() {
        return recurringGrossValue;
    }

    /**
     * Returns the  recurring Tax value
     * @return the recurring Tax value
     */
    public String getRecurringTaxValue() {
        return recurringTaxValue;
    }

    /**
     * Returns the recurring Time unit 
     * @return the recurring Time unit 
     */
    public String getRecurringTimeUnit() {
        return recurringTimeUnit;
    }

    /**
     * Returns the recurring Time unit plural
     * @return the recurring Time unit 
     */
    public String getRecurringTimeUnitPl() {
        return recurringTimeUnitPl;
    }

    /**
     * Returns the recurring Time unit short term 
     * @return the recurring Time unit 
     */
    public String getRecurringTimeUnitAbr() {
        return recurringTimeUnitAbr;
    }

    /**
     * Convenience method for reading the complete billing frequency,
     * e.g. "/ 2 months" or "per yr"
     * @param Should short time unit be used?
     * @param the divisor has to be set by the caller as it is translation relevant
     * @param e.g a separating blank charakter
     * @return the complete billing frequency 
     */
    public String getRecurringTimeUnitComp(boolean shortTerm, String divisor, String separator) {
		String timeBuf = "";
		
    	try {
			timeBuf = ((divisor == null) ? "" : divisor) + ((separator == null) ? "" : separator);
			boolean isPlural = (Trimmer(getRecurringDuration()).length()==0) ? false : Integer.parseInt(getRecurringDuration().trim()) > 1 ; 
        
			if (shortTerm)
				timeBuf += getRecurringTimeUnitAbr();
			else
				timeBuf += isPlural ? getRecurringTimeUnitPl() : getRecurringTimeUnit();  
    	}
    	catch (Exception ex) {
			if (log.isDebugEnabled()) {
				log.debug("Error ", ex);
			}
    	}
        
        return  timeBuf;
    }


}
