/*****************************************************************************
    Class:        ProductListUI
    Copyright (c) 2005, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      03.01.2005
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.isacore.uiclass;

import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.jsp.PageContext;

import com.sap.ecommerce.businessobject.MarketingBusinessObjectManager;
import com.sap.ecommerce.businessobject.loyalty.LoyaltyMembership;
import com.sap.isa.backend.boi.isacore.marketing.MarketingBusinessObjectsAware;
import com.sap.isa.backend.boi.isacore.marketing.MarketingConfiguration;
import com.sap.isa.backend.boi.webcatalog.pricing.Prices;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.Product;
import com.sap.isa.businessobject.ProductList;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.webcatalog.CatalogBusinessObjectManager;
import com.sap.isa.businessobject.webcatalog.pricing.PriceCalculator;
import com.sap.isa.catalog.actions.ActionConstants;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.catalog.webcatalog.WebCatItemPrice;
import com.sap.isa.core.interaction.InteractionConfig;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.JspUtil;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.MessageList;

/**
 * The class ProductListUI allows to display a 
 * {@link com.sap.isa.businessobject.ProductList} object. <br>
 * This is an abstract class, which will be used e.g. by the marketing objects
 * {@link com.sap.isa.businessobject.marketing.Bestseller} or 
 * {@link com.sap.isa.businessobject.marketing.PersonalizedRecommendation}.
 *
 * @author  SAP AG
 * @version 1.0
 */
public abstract class ProductListUI extends PricingBaseUI {
    
    static public IsaLocation log = IsaLocation.getInstance(ProductListUI.class.getName());
    
    /**
     * Constants to identify the product list in the request. <br>
     */
    public static final String PC_PRODUCT_LIST = "productlist";
    
    /**
     * Reference to the used product list. <br> 
     */
    protected ProductList productList;

    /**
     * Referenz to the current product object. <br>
     */ 
    protected Product product;
    
    /**
     * Are we on catalog jsps. <br>
     */
    protected boolean calledFromCatalog = false;
    
    /**
     * Switch to show list price. <br>
     */
    protected boolean listPriceAvailable = false;

    /**
     * Switch to show customer specific prices. <br>
     */
    protected boolean customerSpecificPriceAvailable = false; 

    /**
     * Switch to show product list as block view. <br>
     */
    protected boolean showAsBlockView = true;

    /**
     * Switch to control if page header and page footer are displayed in block view. <br>
     */
    protected boolean showPage = true;

    /**
     * Switch to control if compare is allowed in block view. <br>
     */
    protected boolean isCompareAllowed = true;

    /**
    * Number of Rows displayed in block view <br>
    */
   protected int numMaxRows = 1;

   /**
    * Number of Columns displayed in block view <br>
    */
   protected int numMaxCols = 2;

    /**
     * page size displayed in block view <br>
     */
    protected int pageSize = 0;

    /**
     * the suffix for the form
     */
    protected String formSuffix = "";

    /**
     * Number of current page <br>
     */
    protected int page;

    /**
     * Item Index counter <br>
     */
    protected int itemIndexCounter = -1;
    
    /**
     * The marketing configuration object <br>
     */
    protected MarketingConfiguration marketingConfiguration = null;
    /**
     * Show Select Product instead of addToBasketIcon
     */
    protected boolean showSelectProduct = false;

    protected String isQuery = null;
    
    /**
     * Standard constructor. <br>
     * 
     * @param pageContext
     */
    public ProductListUI(PageContext pageContext) {
        initContext(pageContext);        
    }

    /**
     * Standard constructor. <br>
     * 
     * @param pageContext
     */
    public ProductListUI() {
    }

    /**
     * Initialize UI clasee . <br>
     * 
     * @param pageContext current page context
     * 
     * 
     * @see com.sap.isa.ui.uiclass.BaseUI#initContext(javax.servlet.jsp.PageContext)
     */
    public void initContext(PageContext pageContext) {
        
        super.initContext(pageContext);
        
        determineProductList();
        
        pageContext.setAttribute(PC_PRODUCT_LIST, productList);

        // visiblity of prices.
        if (isPriceVisible()) {
            if (priceConfig.isListPriceUsed()) {
                listPriceAvailable = true;
            }
    
            if (priceConfig.isIPCPriceUsed()) {
                customerSpecificPriceAvailable = true;
            }
        }

        MarketingBusinessObjectsAware mkbom = (MarketingBusinessObjectsAware) userSessionData.getMBOM().getBOMByType(MarketingBusinessObjectsAware.class);
        if (mkbom != null) {
            marketingConfiguration = mkbom.getMarketingConfiguration();
            
            showSelectProduct = marketingConfiguration.isStandaloneCatalog();
        }

        isQuery = request.getParameter(ActionConstants.RA_IS_QUERY);

        if (isQuery == null && request.getAttribute(ActionConstants.RA_IS_QUERY) != null) {
            isQuery = request.getAttribute(ActionConstants.RA_IS_QUERY).toString();
        }

        if (isQuery != null) {
            isQuery = JspUtil.encodeHtml(isQuery);
        }
        else {
            isQuery = "";
        }


        // Set UI parameter
        setUIParameter();
    }
    
    /**
     * Returns true, if contract data should be displayed
     * 
     * @return true   if contract data should be displayed,
     *         false  otherwise
     */
    public boolean showSelectedProduct() {
        log.entering("showSelectedProduct");

        if (log.isDebugEnabled()) {
            log.debug("showSelectedProduct" + showSelectProduct);
        }
        log.exiting();
        
        return showSelectProduct;
    }

    /**
     * Set the current product. <br>
     * 
     * @param product {@link #product}
     */
    public void setProduct(Product product) {
        this.product = product;     
    }

    /**
     * Return if the list price should be displayed. <br>
     * 
     * @return <code>true</code> if the price should be displayed.  
     */
    public boolean isListPriceAvailable() {
         return listPriceAvailable;
    }

    
    /**
     * Return if the customer specific price should be displayed. <br>
     * 
     * @return <code>true</code> if the price should be displayed.  
     */
    public boolean isCustomerSpecificPriceAvailable() {
        return customerSpecificPriceAvailable;
    }


    /**
     * Determine the product list which shoul be used. <br>
     *
     */
    public abstract void determineProductList();
    
    /**
     * 
     * Return a title for the product list. <br>
     * 
     * @return title for the list.
     */
    public abstract String getTitle();
      
      
    /**
     * Return no products found message. <br>
     * 
     * @return message text if no products are found.
     */
    public abstract String getNotFoundMessage();


    /**
     * Return "more products found" message. <br>
     * 
     * @return message text if more products as displayed are found.
     */
    public abstract String getMoreFoundMessage();
      

    /**
     * Return "more products found" action name. <br>
     * 
     * @return action to display all products,
     *  if more products as displayed are found.
     */
    public abstract String getMoreFoundAction();
    
    
    /**
     * Return the scenario for the product detail action <br>
     * 
     * @return scenario name;
     */
    public abstract String getScenario();

      
    /**
     * Return the product list {@link #productList}. <br>
     * 
     * @return {@link #productList}
     */
    public ProductList getProductList() {
        log.entering("getProductList");
        log.exiting();
        return productList;
    }
    
    /**
     * Return the product list size
     */
    public int getProductListSize() {
        log.entering("getProductListSize");
        log.exiting();
        if(productList != null) {
            return productList.size();
        }
        else {
            return 0;
        }
        
    }

    public boolean isItMoreThanOnePageToDisplay() {
        return page != 1 && page != 0;
    }
    /**
     * Return the flag if data are displayed in block view. <br>
     * 
     * @return boolean value
     */
    public boolean showAsBlockView() {
        return this.showAsBlockView;
    }

    /**
     * Return the flag if page link is displayed in block view. <br>
     * 
     * @return boolean value
     */
    public boolean showPage() {
        String method = "showPage()";
        log.entering(method);
        if (log.isDebugEnabled()) {
            log.debug(method + ": showPage = " + this.showPage);
        }
        log.exiting();
        return (this.showPage);
    }

    /**
     * Return the number of columns displayed in block view. <br>
     * 
     * @return number of columns
     */
    public int getMaxCols() {
        return this.numMaxCols;
    }

    /**
     * Return the number of rows displayed in block view. <br>
     * 
     * @return number of rows
     */
    public int getMaxRows() {
        return this.numMaxRows;
    }

    /**
     * Return the current page number . <br>
     * 
     * @return page number 
     */
    public int getPage() {
        return this.page;
    }

    /**
     * Return the size of the list. <br>
     * 
     * @return size of the list
     */
    public int size() {
        return this.productList.size();
    }

    /**
     * Return the number of items which could be displayed in block view. <br>
     * 
     * @return number of items
     */
    public int pageSize() {
        String method = "pageSize()";
        log.entering(method);
        if (log.isDebugEnabled()) {
            log.debug(method + ": pageSize = " + (this.pageSize));
        }
        log.exiting();
        return this.pageSize;
    }

    /**
     * Set the flag if the page should be displayed in general. <br>
     * 
     * @param flag {@link #boolean}
     */
    public void setShowPageFlag(boolean flag) {
        String method = "setShowPageFlag()";
        log.entering(method);
        if (log.isDebugEnabled()) {
            log.debug(method + ": showPage = " + flag);
        }
        log.exiting();
        this.showPage = flag;
    }

    /**
     * Set the flag if the product comparison should be allowed in the block view. <br>
     * 
     * @param flag {@link #boolean}
     */
    public void setIsCompareAllowed(boolean flag) {
        String method = "setIsCompareAllowed()";
        log.entering(method);
        if (log.isDebugEnabled()) {
            log.debug(method + ": is product comparison allowed = " + flag);
        }
        log.exiting();
        this.isCompareAllowed = flag;
    }

    /**
     * Return the flag if the product comparison is allowed in block view. <br>
     * 
     * @return boolean value
     */
    public boolean isCompareAllowed() {
        String method = "isCompareAllowed()";
        log.entering(method);
        boolean retVal = this.isCompareAllowed && productList.size() > 1;
        if (log.isDebugEnabled()) {
            log.debug(method + ": isCompareAllowed = " + retVal);
        }
        log.exiting();
        return (retVal);
    }

    public String getDetailScenario() {
        String detailScenario = request.getParameter(ActionConstants.RA_DETAILSCENARIO);
        if (null!=request.getAttribute(ActionConstants.RA_DETAILSCENARIO)) {
           detailScenario = (String) request.getAttribute(ActionConstants.RA_DETAILSCENARIO);
        } else {
            detailScenario = "";
        }        
        if (null!=detailScenario) {
           detailScenario = JspUtil.encodeHtml(detailScenario);
        }   
        return detailScenario;
    }

    /**
     * Set UI parameter for max columns. <br>
     * 
     */
    public void setUIParameter() {
        InteractionConfig uiConf = getInteractionConfig().getConfig(ActionConstants.IN_UI_CONFIG_ID);
        if ( uiConf != null) {
          String maxCols = uiConf.getValue(ActionConstants.IN_UI_CAT_MAXCOLS);
          if (maxCols != null) {
            try {
              this.numMaxCols = Integer.parseInt(maxCols);
            }
            catch (NumberFormatException e) {
              log.warn("invalid Catalog.BlockView.MaxColumns "+maxCols);
            }
          }
        }
    }

    /**
     * Set UI parameter for Catalog Area. <br>
     * 
     */
    public void setUIParameterCatArea() { 
        InteractionConfig uiConf = getInteractionConfig().getConfig(ActionConstants.IN_UI_CONFIG_ID) ;
      
        if (uiConf != null) {
            String blockView = uiConf.getValue(ActionConstants.IN_UI_CATAREA_BLOCKVIEW);
            if (blockView != null) {
              showAsBlockView = blockView.equals("true");
            }
            else {
            	showAsBlockView = false;
            }
          
            String maxRows = uiConf.getValue(ActionConstants.IN_UI_CATAREA_MAXROWS);
            if (maxRows != null) {
              try {
                numMaxRows = Integer.parseInt(maxRows);
                setPageSize(numMaxRows * numMaxCols);
              }
              catch (NumberFormatException e) {
                log.warn("invalid CatalogArea.BlockView.MaxRows "+maxRows);
              }
            }
        }
    }

    /**
     * Set UI parameter for Catalog Search. <br>
     * 
     */
    public void setUIParameterCatSearch() { 
        InteractionConfig uiConf = getInteractionConfig().getConfig(ActionConstants.IN_UI_CONFIG_ID) ;
      
        if (uiConf != null) {
            String blockView = uiConf.getValue(ActionConstants.IN_UI_CATSEARCH_BLOCKVIEW);
            if (blockView != null) {
              showAsBlockView = blockView.equals("true");
            }
            else {
            	showAsBlockView = false;
            }
          
            String maxRows = uiConf.getValue(ActionConstants.IN_UI_CATSEARCH_MAXROWS);
            if (maxRows != null) {
              try {
                numMaxRows = Integer.parseInt(maxRows);
                setPageSize(numMaxRows * numMaxCols);
              }
              catch (NumberFormatException e) {
                log.warn("invalid CatalogSearch.BlockView.MaxRows "+maxRows);
              }
            }
        }
    }

    /**
     * Set UI parameter for Recommendations. <br>
     * 
     */
    public void setUIParameterRecommendations() { 
        InteractionConfig uiConf = getInteractionConfig().getConfig(ActionConstants.IN_UI_CONFIG_ID) ;
      
        if (uiConf != null) {
            String blockView = uiConf.getValue(ActionConstants.IN_UI_RECOM_BLOCKVIEW);
            if (blockView != null) {
              showAsBlockView = blockView.equals("true");
            }
            else {
            	showAsBlockView = false;
            }
            
         
            String maxRows = uiConf.getValue(ActionConstants.IN_UI_RECOM_MAXROWS);
            if (maxRows != null) {
              try {
                numMaxRows = Integer.parseInt(maxRows);
                setPageSize(numMaxRows * numMaxCols);
              }
              catch (NumberFormatException e) {
                log.warn("invalid PersonalRecommendations.BlockView.MaxRows "+maxRows);
              }
            }
        }
    }

    /**
     * Set UI parameter for Bestseller. <br>
     * 
     */
    public void setUIParameterBestseller() { 
        InteractionConfig uiConf = getInteractionConfig().getConfig(ActionConstants.IN_UI_CONFIG_ID) ;
      
        if (uiConf != null) {
            String blockView = uiConf.getValue(ActionConstants.IN_UI_GLOBAL_BLOCKVIEW);
            if (blockView != null) {
              showAsBlockView = blockView.equals("true");
            }
            else {
            	showAsBlockView = false;
            }
            
          
            String maxRows = uiConf.getValue(ActionConstants.IN_UI_GLOBAL_MAXROWS);
            if (maxRows != null) {
              try {
                numMaxRows = Integer.parseInt(maxRows);
                setPageSize(numMaxRows * numMaxCols);
              }
              catch (NumberFormatException e) {
                log.warn("invalid GlobalRecommendations.BlockView.MaxRows "+maxRows);
              }
            }
        }
    }

    /**
     * Returns the flag, if we were called from catalog jsps
     */
    public boolean isCalledFromCatalog() {
        return calledFromCatalog;
    }

    /**
     * Sets the flag, if we were called from catalog jsps
     */
    public void setCalledFromCatalog(boolean calledFromCatalog) {
        this.calledFromCatalog = calledFromCatalog;
    }

    /**
     * Dependent of the product the quantity is changeable or not
     * 
     * @param  product to check
     * 
     * @return boolean value
     */
    public boolean isQuantityChangeable(Product product) {
        String method = "isQuantityChangeable(Product product)";
        log.entering(method);
        
        boolean isChangeable = !(product.isSalesPackage() || product.isCombinedRatePlan() || product.isRatePlan()) ;
        
        if (log.isDebugEnabled()) {
            log.debug(method + ": product = " + product.getDescription() + " / quantity is changeable = " + isChangeable);
        }
        log.exiting();
        return isChangeable;
    }
    
    /**
     * returns the suffix to be used for the form
     */
    public String getFormSuffix() {
        return formSuffix;
    }
    
    /**
     * sets the suffix to be used for the form
     */
    public void setFormSuffix(String formSuffix) {
        this.formSuffix = formSuffix;
    }

    /**
     * Get page size
     * 
     * @return page size
     */
    public int getPageSize() {
        return this.pageSize;
    }

    /**
     * Set the page size 
     * 
     * @param pageSize
     */
    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }
    
    /**
     * Please comment 
     * 
     */
    public boolean shouldThisUnitBeSelected(Product product, int j) {
        log.entering("shouldThisUnitBeSelected");
        boolean response = false;
        WebCatItem anItem=null;
        if( (anItem = product.getCatalogItem()) != null) {
            response = anItem.getUnitsOfMeasurement()[j].trim().equalsIgnoreCase(anItem.getUnit().trim());
        }
        else {
            response = product.getUnitsOfMeasurement()[j].trim().equalsIgnoreCase(product.getUnit().trim());
        }
        if (log.isDebugEnabled()) {
            log.debug("returned value: "+response);
        }
        log.exiting();
        return response;
    }

    /**
     * Initialize the item index counter with a start value 
     * 
     * @param itemIndexStartCounter, indicates where the list starts
     */
    public void setItemIndexCounter(int itemIndexStartCounter) {
        this.itemIndexCounter = itemIndexStartCounter;
    }

    /**
     * Get the item index which will be increased before 
     * 
     * @param increaseIndex as boolean value, indicating if the value should be increased
     * 
     * @return item index as int value
     */
    public int getItemIndex(boolean increaseIndex) {
        int itemIndex = -1;
        if (this.itemIndexCounter >= 0) {
            // increase itemIndexCounter by 1  
            itemIndex = this.itemIndexCounter;
            if (increaseIndex) {
                this.itemIndexCounter += 1;
            }
        }
        return itemIndex;
    }
    
    /**
     * return the messages for a product
     *
     * @return messages as String
     */
    public String getMessage(Product product) {
       return getMessage(product.getCatalogItem());
    }

    /**
     * return the messages for an WebCatItem
     *
     * @return messages as String
     */
    public String getMessage(WebCatItem item) {
       String message = null;
       MessageList messageList = null;
       if( ( messageList = item.getMessageList()) != null) {
           Iterator it = messageList.iterator();
           while(it.hasNext()){
              Message msg = (Message)it.next();
              if(message != null){
                 message = message +", "+msg.getDescription();
              }
              else {
                 message = msg.getDescription();
              }
            }
            item.clearMessages();
        }
        return message;
    }
    
    
    /**
     * Reprices given ArrayList of WebCatItems.
     * 
     * @param bom CatalogBusinessObjectManager to get access to business objects.
     * @param items ArrayList of WebCatItems
     */
    protected boolean repriceItems(CatalogBusinessObjectManager bom, ArrayList items) {

        if(items.size() == 0) {
            return false;
        }

        PriceCalculator priceCalc = bom.getPriceCalculator();
        if(priceCalc == null) {
            return false;
        }

        WebCatItem[] itemArray = (WebCatItem[]) items.toArray(new WebCatItem[items.size()]);
        
        // set all prices to null to enforce repricing
        for (int i = 0; i < itemArray.length; i++) {
            itemArray[i].setItemPrice(null);
        }

        // start repricing
        Prices[] priceList = priceCalc.getPrices(itemArray);
        if ((priceList == null) || (itemArray.length != priceList.length)) {
            for (int i = 0; i < itemArray.length; i++) {
                itemArray[i].setItemPrice(new WebCatItemPrice());
            }
            return false;
        }
        else {
            for (int i = 0; i < itemArray.length; i++) {
                itemArray[i].setItemPrice(new WebCatItemPrice(priceList[i]));
            }
            return true;
        }

    }

    public String getHookUrl () {
    	String hookURL = "";
    	
    	if (userSessionData != null ) {
			CatalogBusinessObjectManager cbom =(CatalogBusinessObjectManager)userSessionData.getBOM(CatalogBusinessObjectManager.CATALOG_BOM);
			if (cbom !=null && cbom.getCatalog() != null) {
				hookURL = cbom.getCatalog().getHookUrl();
			}
    	}
    	
    	return hookURL;
    }

    /**
     * Reset to default constellation of a sales package or combined rate plan.
     * 
     */
    public void resetToDefCmp() {
        if (userSessionData != null) {
            CatalogBusinessObjectManager bom =
                                      (CatalogBusinessObjectManager)userSessionData.getBOM(CatalogBusinessObjectManager.CATALOG_BOM);
            if (bom != null && 
                bom.getCatalog()!= null && 
                bom.getCatalog().getCurrentItemList() != null &&
                !bom.getCatalog().getLastVisited().equals("itemDetails")) {
                ArrayList itemsForRepricing = new ArrayList();
                bom.getCatalog().getCurrentItemList().resetToDefCmp(itemsForRepricing);
                repriceItems(bom, itemsForRepricing);
            }
        }
    }

    /**
     * Returns true, if the subTitel attribute of the item is set to true
     *
     * @param product the item to check
     *
     * @return true   if the subtitle attribute is set to "true" false  otherwise
     */
    public boolean isSubTitleTrue(Product product) {
        if (log.isDebugEnabled()) {
            log.debug( "test if subtitle attribute is set to true for the product "
                + product.getId()
                + " - value : "
                + product.getSubtitle());
        }

        return product.getSubtitle().equalsIgnoreCase("true");
    }
    
    /**
     * Returns the amount of selected products in the productlist which are not displayed on the current page.
     * The method stops checking the product list if atleast 2 products are selected. 
     *
     * @return counter as int, 0 if no products are selected
     */
    public int getAmountOfSelectedItems(int pageNo) {
        log.entering("getAmountOfSelectedItems()");
        log.exiting();
        return 0;
    }
    
    /**
     * Flag to indicate, if add to leaflet is allowed
     *
     * @return boolean true if add to leaflet is allowed,
     *                 false else
     */
    public boolean isAddToLeafletAllowed() {
        log.entering("isAddToLeafletAllowed()");
        boolean isAllowed = marketingConfiguration.showCatAddoLeaflet();
        
        // add to leaflet button is only displayed if it's no query and the
        // area is an buy points area or reward area
        if (isAllowed &&
            !isCatalogQuery() &&
            (this.isBuyPointsArea() || this.isRewardArea())) {
                isAllowed = false;
        }
        
        if (log.isDebugEnabled()) {
            log.debug("display add to leaflet button = " + isAllowed);
        }

        log.exiting();
        return isAllowed;
    }
    
    /**
     * Returns flag if personal recommendations are supported<br>
     * 
     * @return boolean true if personal recommendations are supported,
     *                 false else.
     */
    public boolean supportPersRec() {
        log.entering("supportPersRec");
        log.exiting();
        return marketingConfiguration.showCatPersRecommend();
    }
    
    /**
     * Returns flag if personal recommendations should be shown<br>
     * 
     * @return boolean true if personal recommendations should be shown,
     *                 false else.
     */
    public boolean showPersRec() {
        log.entering("showPersRec");
        log.exiting();
        return marketingConfiguration.isPersonalRecommendationAvailable() && supportPersRec();
    }
    
    /**
     * returns true if the personal recommendation login link should be displayed.
     * 
     * @return true, if personal recommendation login link is displayed.
     */
    public boolean showPersRecLoginLink(){
		log.entering("showPersRecLoginLink");
		BusinessObjectManager bom =	(BusinessObjectManager) userSessionData.getBOM(
								BusinessObjectManager.ISACORE_BOM);

		Shop shop = bom.getShop();
		boolean returnValue =  !shop.getScenario().equals(Shop.B2B) && !shop.getScenario().equals(Shop.B2BC) && supportPersRec(); //TODO: Check against shop.scenario is not sufficient in case of Shop.B2BC
		log.exiting();
		return returnValue;
    }
    
	/**
	* Returns flag if personal recommendations are supported<br>
	* 
	* @return boolean true if personal recommendations are supported,
	*                 fasle else.
	*/
	public boolean supportSpecialOffers() {
	    log.entering("supportSpecialOffers");
	    log.exiting();
	    return marketingConfiguration.showCatSpecialOffers();
	}
    
    /**
    * Returns flag if personal recommendations should be shown<br>
    * 
    * @return boolean true if personal recommendations should be shown,
    *                 fasle else.
    */
    public boolean showSpecialOffers() {
        log.entering("showSpecialOffers");
        log.exiting();
        return marketingConfiguration.isBestsellerAvailable() && supportSpecialOffers();
    }

    /**
     * Returns the points unit for the current price product
     * @return ptsUnit, points unit as String
     */
    public String getPointsUnit() {
        log.entering("getPointsUnit()");
        String ptsUnit = null;

        MarketingBusinessObjectManager mktBom = (MarketingBusinessObjectManager) userSessionData.getBOM(MarketingBusinessObjectManager.MARKETING_BOM);
        LoyaltyMembership loyMembership = mktBom.getLoyaltyMembership();
        if (loyMembership != null                   &&
            loyMembership.getPointAccount() != null) {  
            ptsUnit = loyMembership.getPointAccount().getPointCodeId();  
        }
        log.exiting();
        return ptsUnit;
    }

    /**
     * Returns the language dependen description of the points unit for the current price product
     * @return ptsUnitDescr, description of points unit as String
     */
    public String getPointsUnitDescr() {
        log.entering("getPointsUnitDescr()");
        String ptsUnitDescr = null;

        MarketingBusinessObjectManager mktBom = (MarketingBusinessObjectManager) userSessionData.getBOM(MarketingBusinessObjectManager.MARKETING_BOM);
        LoyaltyMembership loyMembership = mktBom.getLoyaltyMembership();
        if (loyMembership != null &&
            loyMembership.getPointAccount() != null) {  
            ptsUnitDescr = loyMembership.getPointAccount().getDescr();  
        }
        log.exiting();
        return ptsUnitDescr;
    }

    /**
     * return true if the Accessories Link should be displayed in the productList
     * @return true, if the accessories link should be displayed in the productList
     *         false, if the accessories link should not be displayed in the productList
     */
    public boolean showAccessoriesLink(WebCatItem item){
    	log.entering("showAccessoriesLink");
    	boolean show = (item.getAttribute("ROLE") != null) && item.getAttribute("ROLE").equals("A"); 
    	
		if (log.isDebugEnabled()) {
		   log.debug( "showAccessoriesLink: "+show);
        }
        log.exiting();
    	return show;
    }	

    /**
     * Returns the flag if the list is build by a query 
     *
     * @return flag, if catalog query
     */
    public boolean isCatalogQuery() {
        log.entering("isCatalogQuery()");
        log.exiting();
        return (isQuery != null && isQuery.equals("yes"));
    }


}