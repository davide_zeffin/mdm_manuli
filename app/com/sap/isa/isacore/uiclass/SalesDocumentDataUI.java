package com.sap.isa.isacore.uiclass;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.jsp.PageContext;

import com.sap.isa.backend.boi.isacore.ConnectedDocumentItemData;
import com.sap.isa.backend.boi.isacore.SalesDocumentConfiguration;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.ConnectedDocument;
import com.sap.isa.businessobject.ExtendedStatusListEntry;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.item.ItemList;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.businessobject.order.ExtRefObjectList;
import com.sap.isa.businessobject.order.PartnerListEntry;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.JspUtil;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.MessageList;
import com.sap.isa.helpvalues.HelpValues;
import com.sap.isa.helpvalues.HelpValuesSearch;
import com.sap.isa.isacore.ItemHierarchy;
import com.sap.isa.payment.backend.boi.PaymentBaseTypeData;
import com.sap.isa.payment.businessobject.PaymentCCard;
import com.sap.isa.payment.businessobject.PaymentMethod;
import com.sap.isa.ui.uiclass.BaseUI;
import com.sap.spc.remote.client.object.IPCItem;

/**
 * UI class to handle an object, which consists of a <code>HeaderSalesDocument</code>
 * object and some <code>ItemSalesDoc</code> items on a JSP. <br>
 * That means that this class could be used for all classes extend <code>SalesDocument</code> 
 * (in ISaCore package) but also classes extend <code>SalesDocumentStatus</code>.
 * 
 * <strong>Note</strong> Some informations are available as public properties
 * of the class.
 *
 * @author SAP AG
 * @version 1.0
 **/
abstract public class SalesDocumentDataUI extends SalesDocumentBaseUI {

	final public String HEADER = "header";

	final public String ITEM = "item";

    /**
	 * Hold the the header inforamtion. <br>
	 */
	public HeaderSalesDocument header;	
	
	/**
	 * Hold the items. <br>
	 */
	public ItemList items;
	
	/**
	 * Itemhierarchy build up with the itemlist. <br>
	 */
    public ItemHierarchy itemHierarchy;

	/**
	 * Hold the item currently used. The item must be set with the {@link #setItem}
	 * method. <br>
	 */	
	protected ItemSalesDoc item;
	
	/** 
	 * The shop isn't visible on the JSP. <br>
	 */	
	protected SalesDocumentConfiguration salesDocConfiguration;	
		
	/**
	 * Flag for OCI scenario.
	 */
	protected boolean ociTransfer = false;

	/**
	 * Hold the resource key for the document status 
	 */
	protected String documentStatusKey = "";
    
    /**
     * Resource key for the document delivery status. <br>
     */
    protected String documentDeliveryStatusKey = "";

	/**
	 * Resource key for the document type. <br>
	 */
	protected String documentTypeKey = "";
   
	/**
	 * Sold to ID. <br>
	 */
	protected String soldToId = null;
	
	/**
	 * Name of the sold to. <br>
	 */
	protected String soldToName = "";
    
	/**
	 * Sold to key. <br>
	 */
    protected TechKey soldToKey;

	/**
	 * Level of the item hierarchy for the current icon. <br>
	 */
	protected int itemLevel;
	
	/**
	 * Margin in the table of the item hierarchy for the current icon. <br>
	 */
	protected int itemMargin;
		
	/**
	 * These field hold the quantity of the last main item,
	 * if we are processing subItems. <br>
	 */
	protected String mainQuantity;

	/**
	 * These field hold the unit of the last main item,
	 * if we are processing subItems. <br>
	 */
	protected String mainUnit;

	/**
	 * These field hold product id of the last main item,
	 * if we are processing subItems. <br>
	 */
	protected String mainProduct;

	/**
	 * These field holds TechKey of the main item,
	 * if we are processing subItems. <br>
	 */
	protected TechKey mainItemGuid;
	
	/**
	 * These field hold teh requested delivery date of the last main item,
	 * if we are processing subItems. <br>
	 */
	protected String mainReqDeliveryDate;
	
	/**
	 * Variable to store the css class of for the item table. <br>
	 */
	protected String itemCSSClass;

	/**
	 * Flag for productSubstitution. <br>
	 */
	protected boolean prodSubstitution = false;
    
    /**
	 * Flag: item holds exclusive free good sub items.
	 */
	protected boolean freeGoodExclusive = false;
	
	/**
	 * Flag: an inclusive free good condition has been found
	 * for this item (i.e. free quantity is >0)
	 */
	protected boolean freeGoodInclusive = false;
	
    /**
     * Flag if product of at least one ATP product differs from product of 
     * main position. <br>
     */
    protected boolean differATPProdsSubpos = true;
    
    /**
     * Flag for items that have a single subposition. <br>
     */
    protected boolean singleSubpos = false;
    
    /**
     * Flag for Kit product. <br>
     */
    protected boolean prodKit = false;
    
    /**
     * Flag for BOM product. <br>
     */
    protected boolean prodBOM = false;

	/**
	 * Flag for extended quotation. <br>
	 */
	protected boolean quotationExtended = false;

	/**
	 * Flag, if at least one item belongs to a contract. <br>
	 */
	protected boolean isContract = false;    

	/**
	 * Description of the current used process type. <br>
	 */
	protected String processTypeDescription = "";
    
    /** 
     * Flag for service recall
    */
    protected boolean isServiceRecall = false;
    
    /** 
	 * Flag for error(s) on sub items
	*/
	protected boolean erroneousBOMSubItem = false;
    
    /** 
     * String, to hold all position number of subpositions as a String
     */
    protected String subPositionsPosNo = "";
    
	/** 
	 * String, to hold all position product IDs of subpositions as a String
	 */
	protected String subPositionsProductId = "";
    
	static final protected IsaLocation log = IsaLocation.getInstance(SalesDocumentDataUI.class.getName());

    /**
     * Constructor for SalesDocumentDataUI. <br>
     *
     * @param request
     */
    public SalesDocumentDataUI(PageContext pageContext) {

		initContext(pageContext);	
    }
    
	/**
	 * Constructor for SalesDocumentDataUI. <br>
	 *
	 */
	public SalesDocumentDataUI() {
	}

	/**
	 * Initialize the object from the page context. <br>
	 * Some abstract methods are called within this method:
	 *
     * setSalesDocConfiguration();  {@link #setSalesDocConfiguration}
	 * setProcessTypes();           {@link #setProcessTypes}            
	 * determineHeader();         {@link #determineHeader}         
	 * 
	 * @param pageContext page context
	 */
	public void initContext(PageContext pageContext) {

        super.initContext(pageContext);

		setSalesDocConfiguration();
       	
        setHeader(this.header);

        // Check for contracts
        if (items != null) {
	        Iterator it = items.iterator();
	
	        while (it.hasNext()) {
	          	ItemSalesDoc item = (ItemSalesDoc) it.next();
	          	if (item.getContractId() != null && item.getContractId().length() > 0) {
	            	isContract = true;
	            	break;
	          	}
	        }        	
	       	itemHierarchy = new ItemHierarchy(items);
        }

		setProcessTypeInfo();         
				
		// if the sold to should be displayed on JSP 
		// the sold to details must be determined.
		if (isSoldToVisible()) {
			determineSoldToInformations();
		}	
        if ((header instanceof HeaderSalesDocument) && (header.getDeliveryStatus() != null)) {
            documentDeliveryStatusKey = "status.sales.status.".concat(header.getDeliveryStatus());
        }  	

		noOfItemsThreshold = salesDocConfiguration.getLargeDocNoOfItemsThreshold();		
    }

	/**
     * Method setItem.
     * @param item
     */
    public void setItem(ItemSalesDoc item) {
	   
        this.item = item;
        prodSubstitution = false;
        freeGoodExclusive = false;
        freeGoodInclusive = false;
        erroneousBOMSubItem = false;
        differATPProdsSubpos = true;
        singleSubpos = false;
        prodKit = false;
        prodBOM = false;
        subPositionsPosNo = "";
		subPositionsProductId = "";
        itemMargin = 0;
		MessageList messagelist = new MessageList();
        
        initItem(item);
        
        ItemSalesDoc predItem = item;
		while (predItem.getParentId() != null && !predItem.getParentId().isInitial()) {
			predItem = items.get(predItem.getParentId());
		}
		mainItemGuid = predItem.getTechKey();
		            
        if (itemHierarchy.hasSubItems(item)) {
            mainQuantity = item.getQuantity();
            mainUnit     = item.getUnit();
            mainProduct  = getProduct();
            mainReqDeliveryDate = item.getReqDeliveryDate();

          
            ItemList subItems = new ItemList(); 
          
            subItems = itemHierarchy.getFirstLevelSubItems(item);
            singleSubpos = subItems.size() < 2; 
          
            if (itemHierarchy.hasATPSubItem(item)) {
                prodSubstitution = true; 
                differATPProdsSubpos = itemHierarchy.hasDifferingProductATPSubItems(item);
            }
            else if (itemHierarchy.has1ToNSubstSubItem(item)) {
              prodSubstitution = true; 
            }
            else if (itemHierarchy.hasKitSubItem(item)) {
                prodKit = true;
            }
            else if (itemHierarchy.hasBOMSubItem(item)) {
                prodBOM = true;
            }
            
			freeGoodExclusive = (itemHierarchy.hasExclusiveFreeGoodSubitem(item));
            
            for (int i=0; i < subItems.size(); i++) {
            	if ( prodKit || prodBOM ) {
					//messagelist = subItems.get(i).getMessageList();
					//if (( messagelist != null) && (messagelist.size() > 0) ) {
					//	erroneousBOMSubItem = true;
					//}
					if ( !subItems.get(i).isValid() ) {
						erroneousBOMSubItem = true;
					}
            	}
                subPositionsPosNo = subPositionsPosNo + subItems.get(i).getNumberInt();
                if (i < subItems.size()- 1) { 
                    subPositionsPosNo = subPositionsPosNo + ", ";
                }
				subPositionsProductId = subPositionsProductId + subItems.get(i).getProduct();
				if (i < subItems.size()- 1) { 
					subPositionsProductId = subPositionsProductId + ", ";
            }
        } 
        } 

        //check on free quantity
		freeGoodInclusive = item.getFreeQuantity()!=null && (!item.getFreeQuantity().equals(""));
		

        if (!itemHierarchy.isSubItem(item) ) {
             even_odd = alternator.next(); // "even" or "odd" 
        }
        else { // if sub-level, insert subpos symbols 
           itemLevel = itemHierarchy.getLevel (item);

           itemMargin = 20 * (itemLevel - 1);
        }

        determineItemCSSClass();

	}

	/**
	 * Return if at least one item belongs to a contract. <br>
	 * 
	 * @return <code>true</code> if at least one itme belongs to a contract.
	 */
	public boolean isContractInfoAvailable() {
		return isContract;
	}

	/**
	 * Return Set the property {@link #documentStatusKey}. <br>
	 * 
	 * @return {@link #documentStatusKey}
	 */
	public String getDocumentStatusKey() {
		return documentStatusKey;
	}

	public String getExtStatusValue(ExtendedStatusListEntry eStatus) {
        return eStatus.isCurrentStatus() ? "" : (eStatus.getTechKey().getIdAsString()) + "/" + eStatus.getBusinessProcess();
	}

	/**
	 * Return the header description fields. <br>
	 * 
	 * The header description consist of the document type and 
	 * the process type description, if this is avaiable in
	 * {@link #processTypeDescription} property. <br> 
	 * 
	 * <em>The method returns the translate string and no resource key.</em><br>
	 * 
	 * @return String with the header description.
	 */
	public String getHeaderDescription() {
		
        String ret = "";

        if (("basket".equals(docType) && salesDocConfiguration.isDocTypeLateDecision()) ||
             "basket.b2r".equals(docType)) {
            ret = translate("b2b.docnav.basket.b2r");
        }
        else {
			String key = "b2b.docnav." + docType;
        
			if (ociTransfer) {
				key = key.concat(".oci");
			}

            if (processTypeDescription.length() == 0) {
                ret = translate(key);
            }
            else {
                ret = translate(key) + " (" + processTypeDescription + ")";
            }
        }

        return ret;		
	}

	/**
	 * Returns, if the field validTo could be displayed on JSP.
	 * 
	 * @return <code>true</code> if property validTo could be displayed on JSP
	 */
	public boolean isHeaderValidToAvailable () {
		
		if (header.getDocumentType().equals(HeaderSalesDocument.DOCUMENT_TYPE_QUOTATION) &&
				(header.getStatus().equals(HeaderSalesDocument.DOCUMENT_COMPLETION_STATUS_OPEN) 
				&& header.isQuotationExtended()
			   	|| header.getStatus().equals(HeaderSalesDocument.DOCUMENT_COMPLETION_STATUS_RELEASED) 
			   	&& header.isQuotationExtended())) {
			   	
			return true; 
		    }		   

		return false;
	}
    
    /**
     * Returns true if the current item is a main item
     */
    public boolean isItemMainItem() {
        return (item != null && (item.getParentId() == null || item.getParentId().getIdAsString().length() == 0));
    }

	/**
	 * Return the predecessor of the current item. <br>
	 * 
	 * @return ConnectedDocumentItemData predecessor
	 */
	public ConnectedDocumentItemData getPredecessor() {
		 return item.getPredecessor();
	}	 

	/**
	 * Return the sucessor of the current item. <br>
	 * 
	 * @return ConnectedDocumentItemData sucessor
	 */
	public ConnectedDocumentItemData getSucessor() {
		 return item.getSucessor();
	}	 

	/**
	 * Return the predecessor of the current item. <br>
	 * 
	 * @return ConnectedDocumentItemData predecessor
	 */
	public ConnectedDocumentItemData getConnectedItem() {
		 return item.getPredecessor();
	}	 

	/**
	 * Return the date format. <br>
	 * The Format is taken from the salesDocConfiguration and convert to lower 
	 * case.   
	 * 
	 * @return the date format taken from the salesDocConfiguration in lower 
	 *          case.
	 */	
	public String getDateFormat() {
		return salesDocConfiguration.getDateFormat().toLowerCase();		
	}
    
    /**
     * Return the date format. <br>
     * The Format is taken from the salesDocConfiguration.
     * This is the format, that must be used for the new calendar control.
     * 
     * @return the date format taken from the salesDocConfiguration
     */ 
    public String getDefaultDateFormat() {
        String dateFormat = salesDocConfiguration.getDateFormat();
        dateFormat = dateFormat.replace('Y','y');
        dateFormat = dateFormat.replace('D','d');
        return dateFormat;     
    }

	/**
	 * Returns the value of validTo field. If the validTo is empty (equals "00000000")
	 * the fix text with the resource key "status.sales.detail.validTo.noend" is used.
	 *   
	 * @return the field validTo for the header.
	 */
	public String getHeaderValidTo() {
		
		if (header.getValidTo() != null && !header.getValidTo().equals("00000000")) { 
				return header.getValidTo();
		}
		String key = "status.sales.detail.validTo.noen" ;
		return translate(key); 	  	
		
	}

  	/**
  	 * Return the property {@link #itemCSSClass}. <br>
  	 * 
  	 * @return {@link #itemCSSClass}
  	 */
  	public String getItemCSSClass() {
  		
		return itemCSSClass;
    }

    /**
     * Set the property {@link #itemCSSClass}. <br>
     * 
     * @param itemCSSClass
     */
    public void setItemCSSClass(String itemCSSClass) {
        this.itemCSSClass = itemCSSClass;
    }

    /**
     * Return the property {@link #itemLevel}. <br>
     * 
     * @return {@link #itemLevel}
     */
    public int getItemLevel() {
        return itemLevel;
    }

    /**
     * Return the property {@link #itemMargin}. <br>
     * 
     * @return {@link #itemMargin}
     */
    public int getItemMargin() {
        return itemMargin;
    }

	/**
	 * Return the item status as a string. <br>
	 * 
	 * @return
	 */
	public String getItemStatus() {
		if (item.getStatus() != null && !isZeroProductId) {
			String itemStatusKey = "status.sales.status.".concat(item.getStatus());
			return translate(itemStatusKey);
		}
		return "&nbsp;";	
	}

	/**
	 * Returns the description for the substitution reason using the abstract 
	 * method {@link #getSubstitutionReasonDesc}, which have to provide the
	 * description. <br>
	 * 
	 * @return the description for the substitution reason.
	 */
	public String getSubstitutionReason() {

		if (salesDocConfiguration.isProductDeterminationInfoDisplayed()) {
			String substResDesc = getSubstitutionReasonDesc(item.getSubstitutionReasonId());
			if (substResDesc.length() > 0) {
				return "<br>" + translate("b2b.proddsubst.desc") + JspUtil.encodeHtml(substResDesc);
			}
		}

		return "";
	}

	/**
	 * Return if product determination info is available, which have to be
	 * displayed. <br>
	 * 
	 * @return <code>true</code>, if the information is available.
	 */
	public boolean isProductDeterminationInfoAvailable() {
		
		return salesDocConfiguration.isProductDeterminationInfoDisplayed() 
				&& item.getSystemProductId() != null 
				&& item.getSystemProductId().length() > 0 
				&& item.getProduct() != null  
				&& !item.getProduct().equals(item.getSystemProductId())
				&& !item.isItemUsageATP()
				&& !item.isItemUsage1ToNSubst()
				&& !item.isItemUsageKit();
	}

	/**
	 * Return product number<br>
	 * 
	 * @return product number if the item.
	 */
	public String getProduct() {
		
		if (item.isItemUsageATP() || item.isItemUsage1ToNSubst() || item.isItemUsageKit()) {
			return item.getSystemProductId();
		} else {
			return item.getProduct();
		}
	}

	/**
	 * Return the property {@link #soldToId}. <br>
	 * 
	 * @return
	 */
	public String getSoldToId() {
		return soldToId;
	}

	/**
	 * Return the property {@link #soldToKey}. <br>
	 * 
	 * @return
	 */
	public TechKey getSoldToKey() {
		return soldToKey;
	}

	/**
	 * Return the property {@link #soldToName}. <br>
	 * 
	 * @return
	 */
	public String getSoldToName() {
		return soldToName;
	}

	/**
	 * Return if the sold to should be displayed on the page. <br>
	 * 
	 * @return <code>true</code>, if the sold to should be displayed on the 
	 *          JSP
	 */
	abstract public boolean isSoldToVisible();


	/**
	 * Return the description of the substitution reason for the given key. <br>
	 * 
	 * @param key key for the substitution reason
	 * @return description of the substitution reason
	 */
	abstract protected  String getSubstitutionReasonDesc(String key);


	/**
	 * Set the property {@link #processTypeInfo}. 
	 */
	abstract protected void setProcessTypeInfo();


	/**
	 * Set the property {@link #salesDocConfiguration}. 
	 */
	abstract protected void setSalesDocConfiguration();

	/**
	 * Return the business partner object for the given sold to key. <br>
	 * 
	 * @param  soldToKey
	 * @return business partner object, for the given business partner key 
	 */
	abstract protected BusinessPartner getSoldTo(TechKey soldToKey); 

	/** 
	 * Determine the css class for the item. <br>
	 *
	 * <pre>
     * if ((!itemHierarchy.isSubItem(item)) && (!prodSubstitution)) {
	 *     // normal product or konf product Mainposition
	 *     itemCSSClass = "poslist-mainpos-" + even_odd;
	 * }
	 * else if ((!itemHierarchy.isSubItem(item)) && (prodSubstitution)) { 
	 *     // ProductSub MainPosition
	 *     itemCSSClass = "poslist-" + even_odd;
	 * }
	 * else if ((itemHierarchy.isSubItem(item)) && (prodSubstitution)) { 
	 *     // ProductSub SubPosition
	 *     itemCSSClass = "poslist-subpos-" + even_odd;
	 * }
	 * else { 
	 *     // konf product subposition
	 *     itemCSSClass = "poslist-" + even_odd;
	 * }
     * </pre>
	 *  
	 */
	protected void determineItemCSSClass() {
	    
	    if ((!itemHierarchy.isSubItem(item)) && (!prodSubstitution)) {
	        // normal product or konf product Mainposition
	        itemCSSClass = "poslist-mainpos-" + even_odd;
	    }
	    else if ((!itemHierarchy.isSubItem(item)) && (prodSubstitution)) { 
	        // ProductSub MainPosition
	        itemCSSClass = "poslist-" + even_odd;
	    }
	    else if ((itemHierarchy.isSubItem(item)) && (prodSubstitution)) { 
	        // ProductSub SubPosition
	        itemCSSClass = "poslist-subpos-" + even_odd;
	    }
	    else { 
	        // konf product subposition
	        itemCSSClass = "poslist-" + even_odd;
	    }
	}

	/**
	 * Determine information for the soldto like {@link #soldToName }. <br> 
	 * The method is only called if the method {@link #isSoldVisible()} 
	 * returns <code>true</code>. 
	 */
	protected void determineSoldToInformations() {
		
	    PartnerListEntry soldToEntry = header.getPartnerList().getSoldTo();
	    
	    soldToId = (soldToEntry != null) ? soldToEntry.getPartnerId(): "";
	    soldToName = "";
	    soldToKey = (soldToEntry != null) ? soldToEntry.getPartnerTechKey():null;
	    
	    if (soldToKey != null && soldToKey.getIdAsString().length() > 0) {
	        com.sap.isa.businesspartner.businessobject.BusinessPartner businessPartner = getSoldTo(soldToKey);
	        soldToName = businessPartner.getAddress().getName()  + " " + businessPartner.getAddress().getFirstName();
	    }
	} 

	/**
	 * Set the header object and adjust all header dependent attributes. <br>
	 *  
	 * @param header
	 */
	protected void setHeader(HeaderSalesDocument header) {

		if (header != null)  {
		    this.header = header;
		    
		    documentTypeKey = "status.sales.dt.".concat(header.getDocumentType());
		    documentStatusKey = "";
		    // If a shop has only lean quotes, header status will only be open and completed
		    // For shops with ext. quotes, header status for lean quotes will be confirmed / expired
	        if (quotationExtended  &&  // extended quotations allowed?
		        header.isDocumentTypeQuotation()) { 
		        if (!header.isQuotationExtended()) {
		            documentStatusKey = "status.sales.status.l.".concat(header.getStatus());
		        } else {
		            documentStatusKey = "status.sales.status.x.".concat(header.getStatus());
		        }
		    } else if (!header.isDocumentTypeOrderTemplate()) {
		    	if (header.getStatus() != null) {		    
		       	 	documentStatusKey = "status.sales.status.".concat(header.getStatus());
		    	} else {
					header.setStatus("");
		       	 	documentStatusKey = "status.sales.status.".concat(header.getStatus());		       	 
		       	}
		    }
	
		    // isOrderDownloadRequired =  header.isDocumentTypeOrder();
		} 
	}

	/**
	 * Product of the last main item
	 * 
	 * @return Product of the last main item
	 */
	public String getMainProduct() {
		return mainProduct;
	}
	
	/**
	 * Quantity of the last main item
	 * 
	 * @return Quantity of the last main item
	 */
	public String getMainQuantity() {
		return mainQuantity;
	}

	/**
	 * TechKey of the main item
	 * 
	 * @return TechKey of the main item
	 */
	public TechKey getMainItemGuid() {
		return mainItemGuid;
	}
	

		
	/**
	 * Returns the free quantity. Only relevant for items
	 * for which an inclusive free good condition is valid.
	 * 
	 * @return the free quantity
	 */
	public String getFreeQuantity(){
		return item.getFreeQuantity();
	}
	
	/**
	 * Returns the current item unit. 
	 * @return the item unit
	 */
	public String getUnit(){
		return item.getUnit();
	}
	
	/**
	 * ReqDeliveryDate of the last main item
	 * 
	 * @return ReqDeliveryDate of the last main item
	 */
	public String getMainReqDeliveryDate() {
		return mainReqDeliveryDate;
	}
	
	/**
	 * Unit of the last main item
	 * 
	 * @return Unit of the last main item
	 */
	public String getMainUnit() {
		return mainUnit;
	}
	
	/**
	 * Returns the flag, if there are ATP product substitutions available for 
	 * the current item. <br>
	 * 
	 * @return <code>true</code>, if there are ATP product substitutions available
	 *         <code>false</code> else
	 */
	public boolean isProdSubstitution() {
		return prodSubstitution;
	}
    
    /**
	 * Check on exclusive free good sub items. 
	 * @return Is there any sub item that is result from a free good condition 
	 * of type 'exclusive'?
	 */
	public boolean isFreeGoodExclusive(){
		return freeGoodExclusive;
	}
	
	/**
	 * Check on inclusive free good sub items. 
	 * @return Does the item carry a free quantity (i.e. in backend an inclusive
	 * free good subitem was found?)  
	 */
	public boolean isFreeGoodInclusive(){
		return freeGoodInclusive;
	}	
    
    /**
     * Returns the flag, if there are ATP product substitutions available for 
     * the current item, with a differing rpoduct. <br>
     * 
     * @return <code>true</code>, if there are ATP product substitutions available
     *                            with differing products
     *         <code>false</code> else
     */
    public boolean hasDifferingProductATPSubpos() {
        return differATPProdsSubpos;
    }
    
 	/**
 	 * Returns the resource file key of back order items depending on 
 	 * order due or delivery due.
 	 * @return resource file key  of backorder item
 	**/
 	public String getItemBackOrderKey() {
 
 		String backOrderKey = "";
 
 		if (item.isBackordered()) {
 			backOrderKey = "b2b.ordr.item.backorder";
 		}
 
 		return backOrderKey;
 	}        
    
    /**
     * Returns the items campaign id for the given index
     * 
     * @param intger idx the idx of the items campaign to look for
     * @return String returns the header items id for the given index
     */
    public String getItemCampaignId(int idx) {
        String id = "";
        
        if (item != null && item.getAssignedCampaigns() != null && 
        item.getAssignedCampaigns().getCampaign(idx) != null) {
            id = item.getAssignedCampaigns().getCampaign(idx).getCampaignId();
        }
        
        return id;
    }
    
    /**
     * Returns the number of campaigns entries for the item
     * 
     * @return int the number of campaigns assigned to the item
     */
    public int getNoItemCampaignEntries() {
        int noCampaings = 0;
        
        if (item != null && item.getAssignedCampaigns() != null) {
            noCampaings = item.getAssignedCampaigns().size();
        }
        
        return noCampaings;
    }
    
    /**
     * Returns the Colspan value for the item details table for all entries despite campaigns<br>
     *  
     * @return the Colspan value for the item details table for all entries despite campaigns
     */
    public int getNonCampItemDetailColspan() {
        return 3;
    }
    
    /**
     * Returns a flag, to indicate, if the Toggle Icon for multiple campaign 
     * information should be shown for the current item 
     * 
     * @return false always, should be overwritten by the subclasses if necessary
     */
    public boolean showCurrentItemToggleMultipleCampaignData() {
        
       return false;
    }
    
    /**
     * Returns the item campaign guid for the given index
     * 
     * @param intger idx the idx of the item campaign to look for
     * @return String returns the item campaign guid for the given index
     */
    public String getItemCampaignGuid(int idx) {
        String id = "";
    
        if (item != null && item.getAssignedCampaigns() != null && 
            item.getAssignedCampaigns().getCampaign(idx) != null &&
            item.getAssignedCampaigns().getCampaign(idx).getCampaignGUID() != null) {
            id = item.getAssignedCampaigns().getCampaign(idx).getCampaignGUID().getIdAsString();
        }
    
        return id;
    }
    
    /**
     * Returns true if the campaign list of the current item is not empty
     * 
     * @return true if the campaign list of the current item is not empty
     */
    public boolean isItemCampaignListSet() {
        boolean isSet = false;
        
        if (item != null && item.getAssignedCampaigns() != null && 
            item.getAssignedCampaigns().size() > 0) {
                isSet = true;
        }
        
        return isSet;
    }
    
    /**
     * Returns true if the items campaign for the given index was entered manually
     * 
     * @param intger idx the idx of the items campaign to look for
     * @return boolean returns true if the items campaign for the given index was entered manually
     */
    public boolean isItemCampaignManuEntered(int idx) {
        boolean isManEntered = false;
        
        if (item != null && item.getAssignedCampaigns() != null && 
            item.getAssignedCampaigns().getCampaign(idx) != null &&
            item.getAssignedCampaigns().getCampaign(idx).getCampaignGUID() != null &&
            item.getAssignedCampaigns().getCampaign(idx).getCampaignGUID().getIdAsString().length() > 0) {
            isManEntered = item.getAssignedCampaigns().getCampaign(idx).isManuallyEntered();
        }
        
        return isManEntered;
    }
    
    /**
     * Returns true if the items campaign for the given index is valid
     * 
     * @param intger idx the idx of the items campaign to look for
     * @return boolean returns true if the items campaign for the given index is valid
     */
    public boolean isItemCampaignValid(int idx) {
        boolean isValid = true;
        
        if (item != null && item.getAssignedCampaigns() != null && 
            item.getAssignedCampaigns().getCampaign(idx) != null) {
                isValid = item.getAssignedCampaigns().getCampaign(idx).isCampaignValid();
        }
        
        return isValid;
    }
    
    /**
     * Returns true if the items campaign for the given index is know,
     * which means its TechKey is not null or initial
     * 
     * @param intger idx the idx of the items campaign to look for
     * @return boolean returns true if the items campaign for the given index is known
     */
    public boolean isItemCampaignKnown(int idx) {
        boolean isValid = true;
        
        if (item != null && item.getAssignedCampaigns() != null && 
            item.getAssignedCampaigns().getCampaign(idx) != null) {
            TechKey guid = item.getAssignedCampaigns().getCampaign(idx).getCampaignGUID();
                
            if (guid == null || guid.getIdAsString().length() == 0) { 
                isValid = false;
            }
        }
        
        return isValid;
    }
    
    /**
     * Returns the number of invalid campaigns for the current item
     * 
     * @return the number of invalid campaigns for the current item
     */
    public int getNoInvalidItemCampaigns() {
        int noInvalidCamps = 0;
        
        if (item != null && item.getAssignedCampaigns() != null && 
            item.getAssignedCampaigns().size() > 0) {
                for (int i = 0; i < item.getAssignedCampaigns().size(); i++) {
                    if (!item.getAssignedCampaigns().getCampaign(i).isCampaignValid()) {
                        noInvalidCamps++;
                    }   
                }
        }
        
        return noInvalidCamps;
    }
    
    /**
     * Returns the id of the first invalid campaign for current item
     * 
     * @return the id of the first invalid campaign for the current item
     */
    public String getFirstInvalidItemCampaignId() {
        String campId = "";
        
        if (item != null && item.getAssignedCampaigns() != null && 
            item.getAssignedCampaigns().size() > 0) {
                for (int i = 0; i < item.getAssignedCampaigns().size(); i++) {
                    if (!item.getAssignedCampaigns().getCampaign(i).isCampaignValid()) {
                        campId = item.getAssignedCampaigns().getCampaign(i).getCampaignId();
                        break;
                    }   
                }
        }
        
        return campId;
    }
    
    /**
     * Returns true if any of the items campaign was assigned automatically to the item
     * 
     * @return boolean returns true if any of the items campaign was assigned automatically to the item
     */
    public boolean isItemCampaignAssignedAuto() {
        boolean isAutAssigned = false;
        
        if (item != null && item.getAssignedCampaigns() != null) {
            for (int i = 0; i < item.getAssignedCampaigns().size(); i++) {
                isAutAssigned = isAutAssigned || !item.getAssignedCampaigns().getCampaign(i).isManuallyEntered();
            }
        }
        
        return isAutAssigned;
    }
    
    /**
     * Returns the header campaign id for the given index
     * 
     * @param intger idx the idx of the header campaign to look for
     * @return String returns the header campaign id for the given index
     */
    public String getHeaderCampaignId(int idx) {
        String id = "";
        
        if (header != null && header.getAssignedCampaigns() != null && 
            header.getAssignedCampaigns().getCampaign(idx) != null) {
            id = header.getAssignedCampaigns().getCampaign(idx).getCampaignId();
        }
        
        return id;
    }
    
    /**
     * Returns the header campaign guid for the given index
     * 
     * @param intger idx the idx of the header campaign to look for
     * @return String returns the header campaign guid for the given index
     */
    public String getHeaderCampaignGuid(int idx) {
        String id = "";
        
        if (header != null && header.getAssignedCampaigns() != null && 
            header.getAssignedCampaigns().getCampaign(idx) != null &&
            header.getAssignedCampaigns().getCampaign(idx).getCampaignGUID() != null) {
            id = header.getAssignedCampaigns().getCampaign(idx).getCampaignGUID().getIdAsString();
        }
        
        return id;
    }
    
    /**
     * Returns true if the header campaign for the given index is valid
     * 
     * @param intger idx the idx of the items campaign to look for
     * @return boolean returns true if the header campaign for the given index is valid
     */
    public boolean isHeaderCampaignValid(int idx) {
        boolean isValid = true;
        
        if (header != null && header.getAssignedCampaigns() != null && 
            header.getAssignedCampaigns().getCampaign(idx) != null) {
            isValid = header.getAssignedCampaigns().getCampaign(idx).isCampaignValid();
        }
        
        return isValid;
    }
    
    /**
     * Returns true if the header campaign for the given index is known,
     * which means its TechKey is not null or initial.
     * 
     * @param intger idx the idx of the items campaign to look for
     * @return boolean returns true if the header campaign for the given index is know
     */
    public boolean isHeaderCampaignKnown(int idx) {
        boolean isValid = true;
        
        if (header != null && header.getAssignedCampaigns() != null && 
            header.getAssignedCampaigns().getCampaign(idx) != null) {
            TechKey guid = header.getAssignedCampaigns().getCampaign(idx).getCampaignGUID();
            
            if (guid == null || guid.getIdAsString().length() == 0) { 
                isValid = false;
            }
        }
        
        return isValid;
    }
    
    /**
     * Returns true if for the document header follow-on or preceding documents are available  
     *
     * @return boolean true, if documents are available
     */
    
    public boolean isHeaderDocFlowAvailable() {
    	boolean isHeaderDocFlowAvailable = false;
		List predecessorList = header.getPredecessorList();
		List successorList = header.getSuccessorList();
		ConnectedDocument predecessor = null;
		ConnectedDocument successor = null;
		int i;
		if (predecessorList != null && predecessorList.size() > 0)  {
			i = 0;
			while( i < predecessorList.size()) {
			   predecessor = (ConnectedDocument)predecessorList.get(i);
			   if (predecessor.getDocType() != null &&
                   isElementVisible("order.pred.".concat(predecessor.getDocType()))) {
				  return true;
			   }
			   i++;
			}			   	 
    	}
	    if (successorList != null && successorList.size() > 0) { 
			i = 0;
			while (i < successorList.size()) {
				successor = (ConnectedDocument)successorList.get(i);
			  	if (successor.getDocType() != null &&
                    isElementVisible("order.succ.".concat(successor.getDocType()))) {
					return true;
			  	}
			  	i++;
			}
	    }   			   
    	return false;
    }
    
    /**
     * Returns the number of invalid campaigns on header level
     * 
     * @return the number of invalid campaigns on header level
     */
    public int getNoInvalidHeaderCampaigns() {
        int noInvalidCamps = 0;
        
        if (header != null && header.getAssignedCampaigns() != null && 
            header.getAssignedCampaigns().size() > 0) {
                for (int i = 0; i < header.getAssignedCampaigns().size(); i++) {
                    if (!header.getAssignedCampaigns().getCampaign(i).isCampaignValid()) {
                        noInvalidCamps++;
                    }   
                }
        }
        
        return noInvalidCamps;
    }
    
    /**
     * Returns the id of the first invalid campaign on header level
     * 
     * @return the id of the first invalid campaign on header level
     */
    public String getFirstInvalidHeaderCampaignId() {
        String campId = "";
        
        if (header != null && header.getAssignedCampaigns() != null && 
            header.getAssignedCampaigns().size() > 0) {
                for (int i = 0; i < header.getAssignedCampaigns().size(); i++) {
                    if (!header.getAssignedCampaigns().getCampaign(i).isCampaignValid()) {
                        campId = header.getAssignedCampaigns().getCampaign(i).getCampaignId();
                        break;
                    }   
                }
        }
        
        return campId;
    }
    
    /**
     * Returns true if the campaign list on header level is not empty
     * 
     * @return true if the campaign list on header level is not empty
     */
    public boolean isHeaderCampaignListSet() {
        boolean isSet = false;
        
        if (header != null && header.getAssignedCampaigns() != null && 
            header.getAssignedCampaigns().size() > 0) {
                isSet = true;
        }
        
        return isSet;
    }
    
    /**
     * Returns the number of campaigns entries for the header
     * 
     * @return int the number of campaigns assigned to the header
     */
    public int getNoHeaderCampaignEntries() {
        int noCampaings = 0;
        
        if (header != null && header.getAssignedCampaigns() != null) {
            noCampaings = header.getAssignedCampaigns().size();
        }
        
        return noCampaings;
    }
    
    /**
     * Returns true if current item is a configurable sub item
     * 
     * @return true if current item is a configurable sub item
     */
    public boolean isItemConfigurableSubItem() {
        boolean isConfSubItem = false;
        
        if (item != null && !isItemMainItem() && 
            (item.isItemUsageConfiguration() || item.getItmUsage() == null || item.getItmUsage().length() ==0)) {
            isConfSubItem = true;
        }
        
        return isConfSubItem;
    }
    
	/**
	  * Returns true, if the configuration of the item is complete
	  * @return true, it the configuration of the item is complete 
	  *
	  */
	 public boolean isConfigurationComplete() {
		 IPCItem ipcItem = (IPCItem)item.getExternalItem();	 
		 if (ipcItem == null) {
			 return false;
		 } else {
			 if (ipcItem.getConfiguration() != null && ipcItem.getConfiguration().isAvailable()) {
				 return (ipcItem.getConfiguration().isComplete() && ipcItem.getConfiguration().isConsistent());
			 } else {
				 return false;
			 }
		 }
	 }        
    
    /**
	 * Returns true if current item is a BOM sub item
	 * 
	 * @return true if current item is a BOM sub item
	 */
	public boolean isItemBOMSubItem() {
		boolean isBOMSubItem = false;
        
		if (item != null && !isItemMainItem() && (item.isItemUsageBOM() )) {
			isBOMSubItem = true;
		}
        
		return isBOMSubItem;
	}
 
	/**
	 * Return true if a sub item has errors
	 * 
	 * @return true if a sub item has errors
	 */
	public boolean isBOMSubItemErroneous() {
		return erroneousBOMSubItem;
	}	
 
	/**
	 * Returns true if current item is a FreeGoodExclusive sub item
	 * 
	 * @return true if current item is a FreeGoodExclusive sub item
	 */
	public boolean isItemFreeGoodExclusiveSubItem() {
		boolean isFreeGoodExclusiveSubItem = false;
        
		if (item != null && !isItemMainItem() && (item.isItemUsageFreeGoodExcl() )) {
			isFreeGoodExclusiveSubItem = true;
		}
        
		return isFreeGoodExclusiveSubItem;
	}
	   
    /**
     * Returns true if the detail button for an item must be shown
     */
    public boolean showItemDetailButton() {
        boolean retVal = isToggleSupported();
        if (isToggleSupported()) {  // no toogle support -> no further check
            if ((isItemBOMSubItem() || isItemFreeGoodExclusiveSubItem() || isItemConfigurableSubItem())) {
                // it is an subitem, now check XCM setting (component UI -> ui.fields.order.subitemDetailView)
                if (isElementVisible("order.subitemDetailView")) {
                    retVal = true;
                } else {
                    retVal = false;
                }
            } else {
                retVal = true;
            }
        }
        return retVal;
    }
    
    /**
     * Returns true if the ATP info for the current item must be shown
     */
    public boolean showItemATPInfo() {
        boolean showATPInfo = false;
        
        if (item != null && item.isATPRelevant() && 
            (!itemHierarchy.hasSubItems(item) || itemHierarchy.hasExclusiveFreeGoodSubitem(item) || item.isConfigurable())) {
            showATPInfo = true;
        }
        
        return showATPInfo;
    }
    
    /**
	 * Returns true if the sub items of a bill of material should
	 * not be displayed  
     * @return true if the sub items of a bill of material should
     * not be displayed
	 */
	public boolean isBOMSubItemToBeSuppressed() {
		return  isItemBOMSubItem() && !isElementVisible("order.bomExplosion"); 
	}
    
    /**
     * Returns the flag, if there are BOM or Kit sub components available for 
     * the current item. <br>
     * 
     * @return <code>true</code>, if there are BOM or Kit sub components available
     *         <code>false</code> else
     */
    public boolean isProdKitBOM() {
        return prodKit || prodBOM;
    }

    /**
     * Returns the String that holds all position number of subpositions as a comma separated list
     * 
     * @return String all position number of subpositions as a comma separated list
     */
    public String getSubPositionsPosNo() {
        return subPositionsPosNo;
    }
    
    /**
	 * Returns the String that holds all position product IDs of subpositions as a comma separated list
	 * 
	 * @return String all position product IDs of subpositions as a comma separated list
	 */
	public String getSubPositionsProductId() {
		return subPositionsProductId;
	}    
    
    /**
     * Returns key that shall be used for  substitution messages
     */
    public String getSubstMessage() {
        return (singleSubpos) ? "b2b.prodreplacement.msg3" : "b2b.productreplacement.message2"; 
    }
    
    /**
     * Returns key that shall be used for kit and structurec product messages
     */
    public String getKitProdMessage() {
        String prod = (prodKit) ? "kit" : "struct";
        return (singleSubpos) ? "b2b." + prod + ".msg.single" : "b2b." + prod + ".msg"; 
    }   
	
	/**
	 * Generates all messages for accessibility.
	 */
	public void generateAccMessages() {

		MessageList messagelist = header.getMessageList();
			
		for(int i=0; i<messagelist.size(); i++){
			Message message = messagelist.get(i);
			message.setPageLocation(HEADER);
		}
		
		addAccessibilityMessages(messagelist);

		
		//Read Document Messages
        if (docMessages != null) {
            for (int i=0; i<docMessages.size(); i++){
                Message message = docMessages.get(i); 
                message.setPageLocation(HEADER);
            }
        }
        
		addAccessibilityMessages(docMessages);
	
		//Read all items with their messages
		if(items != null & items.size() > 0){
			for(int i=0; i<items.size(); i++){
				messagelist = items.get(i).getMessageList();
				for(int j=0; j<messagelist.size(); j++){
					Message message = messagelist.get(j);
					message.setPageLocation(ITEM);
					message.setPosition(items.get(i).getNumberInt());
				}
				//add messages to Accessibility messages
				addAccessibilityMessages(messagelist);
			}
		}
	}
		
	/**
	 * Returns the delivery priority description for a given dlv prio key
	 * from the help value search object.
	 * 
	 * @param deliveryPriorityId
	 * @return String
	 */
	public String getDeliveryPriorityDescription(String deliveryPriorityId) {
		String description = "";
		if ((deliveryPriorityId != null) && (!deliveryPriorityId.equals("00"))) {
			HelpValuesSearch search = getHelpValuesSearch("DeliveryPriority");
			if (search != null) {
				try {
				HelpValues values = search.callHelpValuesMethod(userSessionData.getMBOM(), request);
				if (values.search("deliveryPriority",deliveryPriorityId)) {
					description = values.getValueDescription("deliveryPriority");
				}				
				}
				catch (CommunicationException ex){
					log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.exception",ex);		
				}
			}			
		}    	
		return description;
	}

    /**
     * Get Resource key regarding document delivery status
     */
    public String getDocumentDeliveryStatusKey() {
        return documentDeliveryStatusKey;
    }

    protected boolean isLargeDocument = false;

    /**
     * Determines if the document is a large document or not
     */
    public boolean isLargeDocument() {
        return isLargeDocument;
    }	


	/**
	 * Returns the external reference objects as string separated by ';' 
	 * @return string
	*/	
	public String getExtRefObjListAsString(ExtRefObjectList extRefObjects) {
		String objects = "";
		if ( extRefObjects != null && extRefObjects.size() > 0) {
			for (int j=0; j < extRefObjects.size(); j++) {
				objects = objects + extRefObjects.getExtRefObject(j).getData() + ";";
			}
		}
		return objects;	
	}
 
	/**
	 * Returns the value of an entry of an external reference object list
	 * @param  ExtRefObjectList list of external reference objects
	 * @param  int the index of the required entry
	 * @return string 
	*/	  
	public String getExtRefObjValue(ExtRefObjectList extRefObjects, int i) {
		String value = "";
		if (extRefObjects != null && i < extRefObjects.size()) {
			value =  extRefObjects.getExtRefObject(i).getData();		
		}
		return value;
	} 
    
    /**
     * Returns true, if net price info is available for the current item and should be shown,
     * false else
     * 
     * @return true, if net price info is available for the current item 
     *         false, else
     */
    public boolean showNetPriceInfo() {
        
       return item != null &&
              ((item.getNetPrice() != null && item.getNetPrice().length() > 0) ||
               (item.getNetQuantPriceUnit() != null && item.getNetQuantPriceUnit().length() > 0)
              ) ;
    }
    
	/**
	 * Returns true, if payment terms are available for the header,
	 * false else
	 * 
	 * @return true, if payment terms are available for the header 
	 *         false, else
	 */
	public boolean showHeaderPaymentTerms() {
        
	   return (header.getPaymentTermsDesc() != null && header.getPaymentTermsDesc().length() > 0);
	}
	
	/**
	 * Returns true, if payment terms are available for the current item and different from the header,
	 * false else
	 * 
	 * @return true, if payment terms are available for the item 
	 *         false, else
	 */
	public boolean showItemPaymentTerms() {
        
		if ((item.getPaymentTermsDesc() != null && item.getPaymentTermsDesc().length() > 0) &&
			 (!showHeaderPaymentTerms() || (showHeaderPaymentTerms() && !header.getPaymentTermsDesc().equalsIgnoreCase(item.getPaymentTermsDesc()) ))) {
	   		return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Returns true, if the document is a service recall
	 */
	public boolean isServiceRecall() {
		return isServiceRecall;
	}  
	
	/**
	 * Returns true, if item status should be shown,
	 * false else
	 * 
	 * @return true, if item status should be shown
	 *         false, else
	 */
	public boolean showItemStatus() {
		return ( ! header.isDocumentTypeOrderTemplate() &&
				! (header.isDocumentTypeQuotation()  && ! header.isQuotationExtended())); 
	   	   
	}    
	
	/**
	 * Returns true, if item quantity to deliver should be shown,
	 * false else
	 * 
	 * @return true, if item quantity to deliver should be shown
	 *         false, else
	 */
	public boolean showQtyToDeliver() {
		return (!header.isDocumentTypeOrderTemplate() && ! header.isDocumentTypeQuotation() ); 
	   	   
	}   

	/**
	 * Returns true if the current item is subitem of a sales package. <br>
	 * 
	 * @return <code>true</code> if the current item is subitem of a sales package.
	 */	
	public boolean isScSubItem() {
	   return (item.isItemUsageScSalesComponent()|| item.isItemUsageScRatePlanCombinationComponent() || item.isItemUsageScDependentComponent());
	}     
		
        
    public boolean isMainProductFromPackage(){
        return item.isMainProductFromPackage();
    }
    
	public boolean hideItemDetailButton(ItemSalesDoc orderitem) {
		 boolean hide = true;
		 
		 if (isElementVisible("order.itemdetailgroup1")) {		 	
		 	hide = false;
		 	return hide;
         }
     
		 if (((orderitem.getPredecessorList() != null && orderitem.getPredecessorList().size() > 0) || 
		      (orderitem.getSuccessorList() != null && orderitem.getSuccessorList().size() > 0)) &&
		      (isElementVisible("order.itemdetailgroup2"))) { 
		    hide = false;
		    return hide; 	
		 }
		 
		 return hide;
	 } 

     /**
      * returns <code>true</code> if the item is subitem of a package component (Telco)
      * or true if the item is not a package component
      * 
      */
     public boolean shouldTheInlineConfigurationBeDisplayed(ItemSalesDoc item, ItemHierarchy itemHierachy) {
        boolean response = true;
        response = isScSubItem() || !itemHierarchy.isSubItem(item);
        return response;
     }
    
    /**
     * 
     * @return a List of payment cards
     */ 
	public List getPaymentCardList(){
		List payCards = new ArrayList();
		   if (header != null && header.getPaymentData() != null && header.getPaymentData().getPaymentMethods() != null && header.getPaymentData().getPaymentMethods().size() > 0) {
			   List paymentMethods = header.getPaymentData().getPaymentMethods();
			   Iterator iter = paymentMethods.iterator();			
			   while (iter.hasNext()){					
				   PaymentMethod payMethod = (PaymentMethod) iter.next();			
				   if (payMethod != null){
				   // if the payment method is Credit card, fill the import table accordingly
						if(payMethod.getPayTypeTechKey().equals(PaymentBaseTypeData.CARD_TECHKEY )) {                    		
						   PaymentCCard payCCard = (PaymentCCard) payMethod;							
						   payCards.add(payCCard);
						}
				   }
			   }
		   }
		return payCards;	   
	}
	
	/**
	 * Return if the UIElement with the given name is enabled. <br>
	 * 
	 * @param fieldName name of the ui field
	 * @return <code>true</code> if the element exist and is enabled. 
	 */
	public boolean isElementEnabled(String fieldName) {
		// In the order screens  the UI element of the header data are not referenced 
		// by the document techKey. 
		// Therefore the techKey is added here in order to be able to handle multiple documents.  
	
		return isElementEnabled(fieldName, header.getTechKey().toString());
		
	}
	
	/**
	 * Return if the UIElement with the given name and key is visible. <br>
	 * 
	 * @param fieldName name of the ui field
	 * @return <code>true</code> if the element exist and is visible. 
	 */
	public boolean isElementVisible(String fieldName) {
		// In the order screens  the UI element of the header data are not referenced 
		// by the document techKey. 
		// Therefore the techKey is added here in order to be able to handle multiple documents.  
	
		return isElementVisible(fieldName, header.getTechKey().toString());	
	}
        
}
