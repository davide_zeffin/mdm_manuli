/*****************************************************************************
    Class         PartnerLocatorUI
    Copyright (c) 2001, SAP AG, Germany, All rights reserved.
    Description:  Action to display an dealer list
    Author:       SAP
    Created:      July 2004
    Version:      1.0

    $Revision: #3 $
    $Date: 2004/07/27 $
*****************************************************************************/
package com.sap.isa.isacore.uiclass;

import java.util.List;

import javax.servlet.jsp.PageContext;

import com.sap.isa.core.ContextConst;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.JspUtil;
import com.sap.isa.core.util.MessageList;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.dealerlocator.action.ActionConstants;
import com.sap.isa.dealerlocator.action.DealerLocatorActionHelperData;
import com.sap.isa.dealerlocator.backend.boi.DealerLocatorConfiguration;
import com.sap.isa.dealerlocator.backend.boi.DealerLocatorData;
import com.sap.isa.dealerlocator.businessobject.Dealer;
import com.sap.isa.dealerlocator.businessobject.DealerLocator;
import com.sap.isa.isacore.AddressFormularBase;
import com.sap.isa.ui.uiclass.BaseUI;

/**
 * PartnerLocator UI class ... . <br>
 * 
 * @author SAP
 * @version 1.0
 */
public class PartnerLocatorUI extends BaseUI {
	
	protected DealerLocatorConfiguration configuration;	
	
	protected DealerLocatorActionHelperData actionData;
	
	protected DealerLocator dealerLocator;
	
	protected AddressFormularBase addressFormular;
	
	/**
	 * Separator to separate message concatenate by the {@link # getMessageString} 
	 * method. <br> The default value is &lt;br&gt;.
	 */
	protected String messageSeparator = "<br>";
	static final private IsaLocation log = IsaLocation.getInstance(PartnerLocatorUI.class.getName());
	
	
	/**
	 * Constructor for PartnerLocatorUI. <br>
	 *
	 * @param pageContext
	 */
	public PartnerLocatorUI(PageContext pageContext) {
		
		initContext(pageContext);
		
	}
		
	/**
	 * Standard constructor for PartnerLocatorUI. <br>
	 *
	 */
	public PartnerLocatorUI() {

	}

	/**
	 * Initialize the the object from the page context. <br>
	 * 
	 * @param pageContext page context
	 */
	public void initContext(PageContext pageContext) {
		final String METHOD_NAME = "init()";
		log.entering(METHOD_NAME);
		super.initContext(pageContext);
		getConfiguration();
		getActionData();
		getDealerLocator();
		getAddressFormular();
		log.exiting();
	}


	/**
	 * Returns the DealerLocatorConfiguration
	 * 
	 * @return configuration
	 */
	protected DealerLocatorConfiguration getConfiguration() {
		this.configuration = (DealerLocatorConfiguration) request.getAttribute(ActionConstants.RC_CONFIGURATION); 
		return configuration;
	}
	
	/**
	 * Returns actionData
	 * 
	 * @return actionData
	 */
	protected DealerLocatorActionHelperData getActionData() {
		this.actionData = (DealerLocatorActionHelperData) request.getAttribute(ActionConstants.RC_ACTION_HELPER);
		return actionData;
	}
	
	/**
	 * The address data are contained within the address formular. 
	 * This object should never be 'null'.
	 * 
	 * @return AddressFormularBase addressFormular
	 */
	public AddressFormularBase getAddressFormular() {
		this.addressFormular = (AddressFormularBase) request.getAttribute(ActionConstants.RC_ADDRESS_FORMULAR);
		return addressFormular;
	}
	
	/**
	 * Returns the Dealer Locator object
	 * 
	 * @return DealerLocator dealerLocator
	 */
	public DealerLocator getDealerLocator() {
		this.dealerLocator = (DealerLocator) request.getAttribute(ActionConstants.RC_DEALER_LOCATOR);	
		return dealerLocator;
	}
	
	/**
	 * Returns the possibleRegions
	 * 
	 * @return ResultData possibleRegions
	 */
	public ResultData getPossibleRegions() {
		return (ResultData) request.getAttribute(ActionConstants.RC_POSSIBLE_REGIONS);
	}
	
	/**
	 * Returns the possibleCountries
	 * 
	 * @return ResultData possibleCoutries
	 */
	public ResultData getPossibleCoutries() {
		return (ResultData) request.getAttribute(ActionConstants.RC_POSSIBLE_COUNTRIES);
	}
	
	/**
	 * Returns the property collaborativeShowroom true or false
	 * <b>Only for Collaborative Showroom.</b>
	 * 
	 * @return boolean isCollaborativeShowroom
	 */
	public boolean isCollaborativeShowroom() {
		return actionData.isCollaborativeShowroom();
	}
	
	/**
	 * Returns a list of dealers
	 * 
	 * @return ResultData dealerList
	 */
	public ResultData getDealerList() {
		return (ResultData) request.getAttribute(ActionConstants.RC_DEALER_LIST);
	}
	
	/**
	 * Returns a reference to a dealer object
	 * 
	 * @return dealer
	 */
	public Dealer getDealer() {
		return (Dealer) request.getAttribute(ActionConstants.RC_DEALER);
	}

	/**
	 * Returns the property ociBasketForwarding<br>
	 * <b>Only for Collaborative Showroom.</b>
	 * 
	 * @return boolean ociBasketForwarding
	 */
	public boolean isOciBasketForwarding() {
		return configuration.isOciBasketForwarding();
	}
	
	
	/**
	 * Returns the property partnerAvailabilityInfoAvailable<br>
	 * <b>Only for Collaborative Showroom.</b>
	 * 
	 * @return boolean partnerAvailabilityInfoAvailable
	 */
	public boolean isPartnerAvailabilityInfoAvailable() {
		return configuration.isPartnerAvailabilityInfoAvailable();
	}

	/**
	 * Get the name of the traffic light image for the product availablity<br>
	 * <b>Only for Collaborative Showroom.</b>
	 *
	 * @param scheduleLines schedule lines
	 * @return name of traffic light image and message. If the schedule lines are emtpy
	 *         <code>TL_EMPTY</code> will be returned
	 */	
	public String[] getTrafficLight(List scheduleLines) {
		return configuration.getTrafficLight(scheduleLines);	
	}
	
	/**
	 * Returns the property showPartnerOnLineItem.<br>
	 * <b>Only for Collaborative Showroom.</b>
	 * 
	 * @return boolean showPartnerOnLineItem
	 */
	public boolean isShowPartnerOnLineItem() {
		return configuration.isShowPartnerOnLineItem();
	}
	
	/**
	 * Returns the property relationshipSelectable
	 * true: there are more than one RELTYPE
	 * false: there is only one RELTYPE
	 * 
	 * @return boolean
	 */
	public boolean isRelationshipSelectable() {
		boolean relationshipSelectable = false;
		if (dealerLocator.getRelationShips() != null &&
			dealerLocator.getRelationShips().getNumRows() > 1) {
			    relationshipSelectable = true;
		}
		return relationshipSelectable;
	}
	
	/**
	 * Returns the property usage
	 * 
	 * @param usage
	 * @return String
	 */
	public String getUsage(String usage) {
		if (configuration.getRelationshipGroup() != null && configuration.getRelationshipGroup().length() > 0) {
			return usage;
		}
		return translate(usage);
	}
	
	/**
	 * Returns true if a title is available.
	 * @param dealer
	 * @return boolean
	 */
	public boolean isTitleAvailable(ResultData dealer) {
		return (dealer.getString(DealerLocatorData.TITLE) != null && dealer.getString(DealerLocatorData.TITLE).length() > 0);
	}
	
	/**
	 * Returns the title.
	 * @param dealer
	 * @return String
	 */	
	public String getTitle(ResultData dealer) {
		return (JspUtil.encodeHtml(dealer.getString(DealerLocatorData.TITLE)) + "&nbsp;");
	}

	/**
	 * Returns true if the partner represent a person.
	 * @param dealer
	 * @return boolean
	 */	
	public boolean isPerson(ResultData dealer) {
		boolean person = false;
		if ((dealer.getString(DealerLocatorData.FIRSTNAME) != null && dealer.getString(DealerLocatorData.FIRSTNAME).length() > 0) ||
			 (dealer.getString(DealerLocatorData.LASTNAME) != null && dealer.getString(DealerLocatorData.LASTNAME).length() > 0)) {
			person = true;
		}
		return person;
	}

	/**
	 * Returns the person name.
	 * @param dealer
	 * @return String
	 */	
	public String getPersonName(ResultData dealer) {
		String name = "";
		if (dealer.getString(DealerLocatorData.FIRSTNAME) != null && dealer.getString(DealerLocatorData.FIRSTNAME).length() > 0) {
			name = JspUtil.encodeHtml(dealer.getString(DealerLocatorData.FIRSTNAME)).trim() + "&nbsp;";
		}
		if (dealer.getString(DealerLocatorData.LASTNAME) != null && dealer.getString(DealerLocatorData.LASTNAME).length() > 0) {
			name = name + JspUtil.encodeHtml(dealer.getString(DealerLocatorData.LASTNAME)).trim();
		}
		return name;
	}

	/**
	 * Returns true if the partner represent a company.
	 * @param dealer
	 * @return boolean
	 */	
	public boolean isCompany(ResultData dealer) {
		boolean company = false;
		if ((dealer.getString(DealerLocatorData.NAME1) != null && dealer.getString(DealerLocatorData.NAME1).length() > 0) ||
			 (dealer.getString(DealerLocatorData.NAME2) != null && dealer.getString(DealerLocatorData.NAME2).length() > 0) ||
			  (dealer.getString(DealerLocatorData.NAME3) != null && dealer.getString(DealerLocatorData.NAME3).length() > 0) ||
			   (dealer.getString(DealerLocatorData.NAME4) != null && dealer.getString(DealerLocatorData.NAME4).length() > 0)) {
			   	company = true;
			   }
		return company;
	}

	/**
	 * Returns the company name.
	 * @param dealer
	 * @return String
	 */	
	public String getCompanyName(ResultData dealer) {
		String name = "";
		if (dealer.getString(DealerLocatorData.NAME1) != null && dealer.getString(DealerLocatorData.NAME1).length() > 0) {
			name = JspUtil.encodeHtml(dealer.getString(DealerLocatorData.NAME1)).trim() + "&nbsp;";
		}
		if (dealer.getString(DealerLocatorData.NAME2) != null && dealer.getString(DealerLocatorData.NAME2).length() > 0) {
			name = name + JspUtil.encodeHtml(dealer.getString(DealerLocatorData.NAME2)).trim() + "&nbsp;";
		}
		if (dealer.getString(DealerLocatorData.NAME3) != null && dealer.getString(DealerLocatorData.NAME3).length() > 0) {
			name = name + JspUtil.encodeHtml(dealer.getString(DealerLocatorData.NAME3)).trim() + "&nbsp;";
		}
		if (dealer.getString(DealerLocatorData.NAME4) != null && dealer.getString(DealerLocatorData.NAME4).length() > 0) {
			name = name + JspUtil.encodeHtml(dealer.getString(DealerLocatorData.NAME4)).trim() + "&nbsp;";
		}
		return name;
	}

	/**
	 * Returns true if a street is available.
	 * @param dealer
	 * @return boolean
	 */	
	public boolean isStreetAvailable(ResultData dealer) {
		return (dealer.getString(DealerLocatorData.STREET) != null && dealer.getString(DealerLocatorData.STREET).length() > 0);
	}

	/**
	 * Returns the name of the street.
	 * @param dealer
	 * @return String
	 */	
	public String getStreet(ResultData dealer) {
		return JspUtil.encodeHtml(dealer.getString(DealerLocator.STREET)).trim() + "&nbsp;";
	}
	
	/**
	 * Returns true if a house nr. is available.
	 * @param dealer
	 * @return boolean
	 */	
	public boolean isHouseNrAvailable(ResultData dealer) {
		return (dealer.getString(DealerLocatorData.HOUSENUMBER) != null && dealer.getString(DealerLocatorData.HOUSENUMBER).length() > 0);
	}

	/**
	 * Returns the house nr.
	 * @param dealer
	 * @return String
	 */	
	public String getHouseNr(ResultData dealer) {
		return JspUtil.encodeHtml(dealer.getString(DealerLocatorData.HOUSENUMBER)).trim();
	}
	
	/**
	 * Returns true if the zip code is available.
	 * @param dealer
	 * @return boolean
	 */	
	public boolean isZipCodeAvailable(ResultData dealer) {
		return (dealer.getString(DealerLocatorData.ZIPCODE) != null && dealer.getString(DealerLocatorData.ZIPCODE).length() > 0);
	}

	/**
	 * Returns the zip code.
	 * @param dealer
	 * @return String
	 */	
	public String getZipCode(ResultData dealer) {
		return JspUtil.encodeHtml(dealer.getString(DealerLocatorData.ZIPCODE)).trim() + "&nbsp;";
	}
	
	/**
	 * Returns true if the city is available.
	 * @param dealer
	 * @return boolean
	 */	
	public boolean isCityAvailable(ResultData dealer) {
		return (dealer.getString(DealerLocatorData.CITY) != null && dealer.getString(DealerLocatorData.CITY).length() > 0);
	}

	/**
	 * Returns the name of the city.
	 * @param dealer
	 * @return String
	 */	
	public String getCity(ResultData dealer) {
		return JspUtil.encodeHtml(dealer.getString(DealerLocatorData.CITY)).trim() + "&nbsp;";
	}
	
	/**
	 * Returns true if the country text is available.
	 * @param dealer
	 * @return boolean
	 */
	public boolean isCountryAvailable(ResultData dealer) {
		return (dealer.getString(DealerLocatorData.COUNTRYTEXT) != null && dealer.getString(DealerLocatorData.COUNTRYTEXT).length() > 0);
	}
	
	/**
	 * Returns the country text.
	 * @param dealer
	 * @return String
	 */
	public String getCountry(ResultData dealer) {
		return JspUtil.encodeHtml(dealer.getString(DealerLocatorData.COUNTRYTEXT)).trim();
	}

	/**
	 * Returns true if the region text is available.
	 * @param dealer
	 * @return boolean
	 */	
	public boolean isRegionTextAvailable(ResultData dealer) {
		return (dealer.getString(DealerLocatorData.REGIONTEXT) != null && dealer.getString(DealerLocatorData.REGIONTEXT).length() > 0);
	}

	/**
	 * Returns the region text.
	 * @param dealer
	 * @return String
	 */	
	public String getRegionText(ResultData dealer) {
		return JspUtil.encodeHtml(dealer.getString(DealerLocatorData.REGIONTEXT)).trim() + "&nbsp;";
	}
	
	/**
	 * Returns true if a phone nr. is available.
	 * @param dealer
	 * @return boolean
	 */	
	public boolean isPhoneAvailable(ResultData dealer) {
		return (dealer.getString(DealerLocatorData.PHONE) != null && dealer.getString(DealerLocatorData.PHONE).length() > 0);
	}

	/**
	 * Returns the phone nr.
	 * @param dealer
	 * @return String
	 */	
	public String getPhone(ResultData dealer) {
		return JspUtil.encodeHtml(dealer.getString(DealerLocatorData.PHONE)).trim() + "&nbsp;";
	}
	
	/**
	 * Returns true if a phone extension nr. is available.
	 * @param dealer
	 * @return boolean
	 */
	public boolean isPhoneExtensionAvailable(ResultData dealer) {
		return (dealer.getString(DealerLocatorData.PHONE_EXTENSION) != null && dealer.getString(DealerLocatorData.PHONE_EXTENSION).length() > 0);
	}

	/**
	 * Returns the phone extension nr.
	 * @param dealer
	 * @return String
	 */	
	public String getPhoneExtension(ResultData dealer) {
		return JspUtil.encodeHtml(dealer.getString(DealerLocatorData.PHONE_EXTENSION)).trim();
	}

	/**
	 * Returns true if a fax nr. is available.
	 * @param dealer
	 * @return boolean
	 */	
	public boolean isFaxAvailable(ResultData dealer) {
		return (dealer.getString(DealerLocatorData.FAX) != null && dealer.getString(DealerLocatorData.FAX).length() > 0);
	}

	/**
	 * Returns the fax nr.
	 * @param dealer
	 * @return String
	 */	
	public String getFax(ResultData dealer) {
		return JspUtil.encodeHtml(dealer.getString(DealerLocatorData.FAX)).trim() + "&nbsp;";
	}

	/**
	 * Returns true if a fax extension nr. is available.
	 * @param dealer
	 * @return boolean
	 */	
	public boolean isFaxExtensionAvailable(ResultData dealer) {
		return (dealer.getString(DealerLocatorData.FAX_EXTENSION) != null && dealer.getString(DealerLocatorData.FAX_EXTENSION).length() > 0);
	}

	/**
	 * Returns the fax extension nr.
	 * @param dealer
	 * @return String
	 */	
	public String getFaxExtension(ResultData dealer) {
		return 	JspUtil.encodeHtml(dealer.getString(DealerLocatorData.FAX_EXTENSION)).trim();
	}

	/**
	 * Returns true if a E-Mail is available.
	 * @param dealer
	 * @return boolean
	 */	
	public boolean isEmailAvailable(ResultData dealer) {
		return (dealer.getString(DealerLocatorData.EMAIL) != null && dealer.getString(DealerLocatorData.EMAIL).length() > 0);
	}

	/**
	 * Returns the E-Mail address.
	 * @param dealer
	 * @return String
	 */	
	public String getEmail(ResultData dealer) {
		return JspUtil.encodeHtml(dealer.getString(DealerLocatorData.EMAIL)).trim();
	}

	/**
	 * Returns true if a home page URL is available.
	 * @param dealer
	 * @return boolean
	 */	
	public boolean isURLHomePageAvailable(ResultData dealer) {
		return (dealer.getString(DealerLocatorData.URL_HOMEPAGE1) != null && dealer.getString(DealerLocatorData.URL_HOMEPAGE1).length() > 0);
	}

	/**
	 * Returns the home page URL.
	 * @param dealer
	 * @return String
	 */	
	public String getURLHomePage(ResultData dealer) {
		String url = "";
		if (!dealer.getString(DealerLocatorData.URL_HOMEPAGE1).startsWith("http")) {
			url = "http://";
		}
		url = url + dealer.getString(DealerLocatorData.URL_HOMEPAGE1).trim() +
					dealer.getString(DealerLocatorData.URL_HOMEPAGE2).trim() +
					dealer.getString(DealerLocatorData.URL_HOMEPAGE3).trim() +
					dealer.getString(DealerLocatorData.URL_HOMEPAGE4).trim();
		return url;
	}

	/**
	 * Returns true if a map URL is available.
	 * @param dealer
	 * @return boolean
	 */	
	public boolean isURLMapAvailable(ResultData dealer) {
		return (dealer.getString(DealerLocatorData.URL_MAP1) != null && dealer.getString(DealerLocatorData.URL_MAP1).length() > 0);
	}

	/**
	 * Returns the map URL.
	 * @param dealer
	 * @return String
	 */	
	public String getURLMap(ResultData dealer) {
		String url = "";
		if (!dealer.getString(DealerLocatorData.URL_MAP1).startsWith("http")) {
			url = "http://";
		}
		url = url + dealer.getString(DealerLocatorData.URL_MAP1).trim() +
					dealer.getString(DealerLocatorData.URL_MAP2).trim() +
					dealer.getString(DealerLocatorData.URL_MAP3).trim() +
					dealer.getString(DealerLocatorData.URL_MAP4).trim();
		return url;		
	}
	
	/**
	 * Returns true if we use a popup
	 * @return boolean
	 */
	public boolean isPopupWindow() {
		return dealerLocator.isPopupWindow();	
	}
	
	/**
	 * Returns the usage for the given key
	 * @param key
	 * @return String
	 */
	public String getUsageDescr(String key) {
		String retval = "";
		if (key != null && key.length() > 0) {
			retval = translate(key);
		}
		return retval;
	}
	
	/**
	 * Returns true if the application is B2B
	 * @return boolean
	 */
	public boolean isB2B() {
		boolean applic = false;
		String application = pageContext.getServletContext().getInitParameter(ContextConst.IC_SCENARIO);
		if (application != null && application.equals("B2B")) {
			applic = true;
		}
		return applic;	
	}
	
	/**
	 * Returns true if the scoll buttons are visible.
	 * @return boolean
	 */
	public boolean isScrollBar() {
		boolean scrollBar = false;
		if ((dealerLocator.getHasMoreDealer().equalsIgnoreCase("X")) && (dealerLocator.getStartIndex() == 0)
			 || (dealerLocator.getHasMoreDealer().equalsIgnoreCase("X")) && (dealerLocator.getStartIndex() > 0) && (dealerLocator.getStartIndex() == dealerLocator.getMaxHitsPerPage())
			  || (dealerLocator.getHasMoreDealer().equalsIgnoreCase("X")) && (dealerLocator.getStartIndex() > 0) && (dealerLocator.getStartIndex() > dealerLocator.getMaxHitsPerPage())
			   || (dealerLocator.getHasMoreDealer().equals("")) && (dealerLocator.getStartIndex() > dealerLocator.getMaxHitsPerPage())
			    || (dealerLocator.getHasMoreDealer().equals("")) && (dealerLocator.getStartIndex() == dealerLocator.getMaxHitsPerPage())) {
					
					scrollBar = true;
		}
		return scrollBar;	
	}
	
	/**
	 * Returns true if we search for Partner with Sales Org as search criteria.
	 * @return boolean
	 */
	public boolean isPartnerSearch() {
		boolean partnerSearch = false;
		if (configuration.getPartnerLocatorBusinessPartnerId() != null &&
			configuration.getPartnerLocatorBusinessPartnerId().length() == 0) {
				partnerSearch = true;
			}
		return partnerSearch;	
	}
	
	/**
	 * Returns true if we search a partner for a product
	 * @return boolean
	 */
	public boolean isProductSearch() {
		boolean productSearch = false;
		if (dealerLocator.getProduct() != null && dealerLocator.getProduct().length() > 0) {
			productSearch = true;
		}
		return productSearch;	
	}
	/**
	 * Returns the product
	 * @return String
	 */
	public String getProduct() {
		String product = "";
		if (isProductSearch()) {
			product = dealerLocator.getProduct();
		}
		return product;
	}
	
	/**
	 * Returns the message for the given Message Type.
	 * 
     * @param messageType
     * 
     * @return String
     */
    public String getMessageText(int messageType) {
		String messageText = "";
		MessageList messageList = dealerLocator.getMessageList();
		
		if (messageList != null) {
			MessageList subMessageList = messageList.subList(messageType);
			if (subMessageList != null) {
				for (int i = 0; i < subMessageList.size(); i++) {
					messageText += subMessageList.get(i).getDescription();
					if (i < subMessageList.size() - 1) {
						messageText += messageSeparator;
					}
				}				
			}

		}				
		return messageText;
	}	
}
