/*****************************************************************************
    Class:        BillingDocumentStatusBaseUI
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      01.03.2004
    Version:      1.0

    $Revision: #9 $
    $Date: 2004/08/10 $ 
*****************************************************************************/

package com.sap.isa.isacore.uiclass;

import java.util.Iterator;

import javax.servlet.jsp.PageContext;

import com.sap.isa.businessobject.Address;
import com.sap.isa.businessobject.ConnectedDocument;
import com.sap.isa.businessobject.ConnectedDocumentItem;
import com.sap.isa.businessobject.billing.BillingStatusDocument;
import com.sap.isa.businessobject.header.HeaderBillingDocument;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.item.ItemBillingDoc;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.isacore.ManagedDocumentLargeDoc;
import com.sap.isa.isacore.action.ActionConstantsBase;
import com.sap.isa.portalurlgenerator.util.AnchorAttributes;


/**
 * This UI class is used on billing status detail page. <br>
 * Because the class is abstract the GenericFactory will provide an implementation
 * on the JSP. <br> 
 * The class is introduced for a reuse of the page in <strong>Internet Sales</strong>
 * and <strong>Claims and Returns </strong> scenario, which only use different implementation
 * for this abstract class. <br>.
 *
 * @author  SAP AG
 * @version 1.0
 */
abstract public class BillingDocumentStatusBaseUI extends SalesDocumentStatusBaseUI {
	
    protected BillingStatusDocument salesDocStatusLocal = null;
	static final private IsaLocation log = IsaLocation.getInstance(BillingDocumentStatusBaseUI.class.getName());
	public boolean isCheckBoxRequested = false;
    
    /**
     * Constructor for BillingDocumentStatusBaseUI. <br>
     * 
     * @param pageContext
     */
    public BillingDocumentStatusBaseUI(PageContext pageContext) {
        initContext(pageContext);
    }
    
    /**
     * Constructor for BillingDocumentStatusBaseUI. <br>
     *
     */
    public BillingDocumentStatusBaseUI() {

    }
    /**
     * Initialize the the object from the page context. <br>
     * 
     * @param pageContext page context
     */
    public void initContext(PageContext pageContext) {
		final String METHOD_NAME = "initContext()";
		log.entering(METHOD_NAME);
        super.initContext(pageContext);
        isLargeDocument = salesDocStatusLocal.isLargeDocument(salesDocConfiguration.getLargeDocNoOfItemsThreshold());
        noOfOriginalItems = salesDocStatusLocal.getNoOfOriginalItems();
		if (mdoc instanceof ManagedDocumentLargeDoc                                                                         &&
		    salesDocStatusLocal != null                                                                                     &&
		    salesDocStatusLocal.getItemList().size() > 1                                                                    &&
			! isLargeDocument                                                                                               &&
            ManagedDocumentLargeDoc.ITEM_PROPERTY_NUMBERINT.equals(((ManagedDocumentLargeDoc)mdoc).getItemSearchProperty()) &&
			((ManagedDocumentLargeDoc)mdoc).getItemSearchPropertyLowValue() != null                                         &&
			((ManagedDocumentLargeDoc)mdoc).getItemSearchPropertyLowValue().length() > 0) {
			itemNoToShow = ((ManagedDocumentLargeDoc)mdoc).getItemSearchPropertyLowValue();
		}
        
    	log.exiting();
    }            
    /**
     * Determine the header from the salesDocStatus which is provided by the DocumentHandler (getDocumentOnTop())
     * @see com.sap.isa.isacore.uiclass.SalesDocumentBaseUI#determineHeader()
     */
    protected void determineHeader() {
        this.header = salesDocStatusLocal.getDocumentHeader();
    }

    /**
     * Init Document
     */
    protected void initDocument() {
		final String METHOD_NAME = "initDocument()";
		log.entering(METHOD_NAME);
        salesDocStatusLocal = (BillingStatusDocument)mdoc.getDocument();
        log.exiting();
    }
    /**
     * Determine the item list from the page context.
     * The item list is taken from salesDocStatus which is provided by the DocumentHandler (getDocumentOnTop()) 
     * @see com.sap.isa.isacore.uiclass.SalesDocumentBaseUI#determineItems()
     */
    protected void determineItems() {
        this.items = salesDocStatusLocal.getItemList();
        this.accNumOfItems = items.size();
    }
    /**
     * Determine the message list from the page context.
     * The message list is taken salesDocStatus which is provided by the DocumentHandler (getDocumentOnTop())
     * @see com.sap.isa.isacore.uiclass.SalesDocumentBaseUI#determineMessages()
     */
    protected void determineMessages() {
        //salesDocumentStatus os = (SalesDocumentStatus)request.getAttribute(ActionConstantsBase.RC_SALES_STATUS_DETAIL);
        this.docMessages = salesDocStatusLocal.getMessageList();
    }
    /**
     * Returns true if there is no item in the table of the selected items.
     * (Used for reading large documents).
     */
    public boolean isSelectedItemGuidTableEmpty() {
        return (salesDocStatusLocal.getSelectedItemGuids().isEmpty());
    }
    /**
     * Set the header object and adjust all header dependent attributes. <br>
     *  
     * @param header
     */
    protected void setHeader(HeaderSalesDocument header) {
        
        documentTypeKey = "status.sales.dt.".concat(((HeaderBillingDocument)header).getDocumentType());
        documentStatusKey = "";
        
    }
    /**
     * Set the items into the HTTP request to be iterated.
     */
    public void setItemsToIterate() {
        request.setAttribute(ActionConstantsBase.RK_BILLING_DOC_STATUS_DETAIL_ITEMITERATOR, items);
    }
    /**
     * Get number of found Items from the ManagedDocumentLargeDoc
     */
    public int getNumberOfItemsFound() {
        int numberOfItemsFound = super.getNumberOfItemsFound();
        if ("standalone".equals((String)request.getParameter("billingmode"))) {
            Integer noif = ((Integer)request.getAttribute("numberofitemsfound"));
            if (noif != null) {
                numberOfItemsFound = noif.intValue();
            } else {
                numberOfItemsFound = ManagedDocumentLargeDoc.NO_OF_FOUND_ITEMS_UNDEFINED;
            }
        }
        return numberOfItemsFound;
    }
   /**
    * Returns the document and item number which is claimable in a formated 
    * String of kind "documentno / itemno". Returns <code><b>null</b></code> if none exists.
    * @param ItemBillingDoc billing document item
    * @return String formated e.g 9000030 / 10.  
    */
   public String getClaimableInfo(ItemBillingDoc item) {
       String retVal = null;
       Iterator conIT = header.getPredecessorList().iterator();
       while (conIT.hasNext()) {
           ConnectedDocument conD = (ConnectedDocument)conIT.next();
           if (conD.getRefGuid().equals(item.getTechKey().getIdAsString())  
            && "Z".equals(conD.getTransferUpdateType())) {
                retVal = conD.getDocNumber() + " / " + conD.getDocItemNumber();
           }
       }
       return retVal; 
   }
   /**
    * Returns the message explaining which item can be claimed instead of this one. If no
    * message can be issues it will return an <b>empty</b> String.
    * @param ItemBillingDoc billing document item.
    * @return String including the message
    */
   public String getUnclaimableMessage(ItemBillingDoc item) {
       String retVal = "";
       String docNo = getClaimableInfo(item);
       String[] args = new String[1];
       if (docNo != null) {
           args[0] = docNo;
           retVal = WebUtil.translate(pageContext, "status.bill.item.n.clm.wr", args);
       } else {
           retVal = WebUtil.translate(pageContext, "status.bill.item.n.clm.nr", null);
       }
       return retVal;
   }
    /**
     * Return the Address in a country specific formated way
     * @param Address to be converted
     * @return String representing address infos
     */
    public String getPrintableAddress(Address adr) {
        String out = "";
        if ( ! "US".equals(adr.getCountryISO())) {

            out = adr.getName1() + "<br />" +
                  adr.getStreet() + "&nbsp;" + adr.getHouseNo() + "<br />" +
                  adr.getPostlCod1() + "&nbsp;" + adr.getCity(); 
        } else {
            out = adr.getName1() + "<br />" +
                  adr.getStreet() + "&nbsp;" + adr.getHouseNo() + "<br />" +
                  adr.getCity() + "&nbsp;" + adr.getRegion() + ",&nbsp;" + adr.getPostlCod1();
        }
        return out;
    }
    /**
     * Return the Address in a country specific formated way
     * @param BusinessPartner
     * @param Address to be converted
     * @return String representing address infos
     */
    public String getPrintableAddress(BusinessPartner bp, Address adr) {
        String out = "";
        if (bp == null  ||  adr == null) {
            return out;
        }
        if ( ! "US".equals(adr.getCountryISO())) {

            out = adr.getName1() +  " (" + bp.getId() + ")"+ "<br />" +
                  adr.getStreet() + "&nbsp;" + adr.getHouseNo() + "<br />" +
                  adr.getPostlCod1() + "&nbsp;" + adr.getCity(); 
        } else {
            out = adr.getName1() + " (" + bp.getId() + ")" + "<br />" +
                  adr.getStreet() + "&nbsp;" + adr.getHouseNo() + "<br />" +
                  adr.getCity() + "&nbsp;" + adr.getRegion() + ",&nbsp;" + adr.getPostlCod1();
        }
        return out;
    }
    public String getRefDocumentLinkAttributes(ItemSalesDoc item) {
        //techkey=<%= billingitem.getSalesRefDocTechKey() %>&object_id=<%= billingitem.getSalesRefDocNo() %>&objects_origin=&objecttype=order&billing=<%=billingFlag%>
        //((ItemBillingDoc)item)
        return null;
    }
    /**
     * Get Businesspartner of partner function from header
     * @param String  partner function
     * @return Businesspartner 
     */
    abstract public BusinessPartner getHeaderBusinessPartner(String partnerFunction);
    /**
     * Get Businesspartner of partner function from item
     * @param ItemSalesDoc item
     * @param String  partner function
     * @return Businesspartner 
     */
    abstract public BusinessPartner getItemBusinessPartner(ItemSalesDoc item, String partnerFunction);
    /**
     * Method returns the AnchorAttribute object which contains the information to do Portal 
     * navigation.
     * @return AnchorAttributes 
     */
    abstract public AnchorAttributes getPortalAnchorAttributesForClaimCreation();
    /**
     * Method returns the AnchorAttribute object which contains the information to do Portal 
     * navigation. The navigation will point to Internet Sales web application.
     * @return AnchorAttributes
     */
    abstract public AnchorAttributes getPortalAnchorAttributesForSalesDocDisplay();
    /**
     * Write out JScript onClick event depending on the existens of a sales document reference.<br>
     * Furthermore links will only be created for know document types. Currently this is<br>
     * - order<br>
     * @param ItemBillingDoc item for which the onClick event should be created
     * @param ConnectedDocument where the link might point to
     * @return String like 'onclick="salesDocDisplay('dockey', 'docno', 'doctype');"
     **/
    abstract public String writeItemOnClickEvent(ItemBillingDoc item, ConnectedDocumentItem cd);
}