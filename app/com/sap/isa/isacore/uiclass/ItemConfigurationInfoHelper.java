/*****************************************************************************
    Class:        ItemConfigurationInfoHelper
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      29.11.2004
    Version:      1.0

    $Revision: #13 $
    $Date: 2004/11/29 $ 
*****************************************************************************/
package com.sap.isa.isacore.uiclass;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.ui.uiclass.BaseUI;
import com.sap.spc.remote.client.object.Characteristic;
import com.sap.spc.remote.client.object.CharacteristicGroup;
import com.sap.spc.remote.client.object.CharacteristicValue;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItem;

/**
 * Class ItemConfigurationInfoUI is a helper class for retrieving product configuration information. <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class ItemConfigurationInfoHelper extends BaseUI {

	private IPCItem ipcItem;
	static final private IsaLocation log = IsaLocation.getInstance(ItemConfigurationInfoHelper.class.getName());

	public void setItem(ItemSalesDoc item) {
		ipcItem = null;
		if (item.isConfigurable() && item.getExternalItem() != null) {
			ipcItem = (IPCItem) item.getExternalItem();
		}
	}

	public void setItem(IPCItem ipcItem) {
		this.ipcItem = null;
		if (ipcItem.isConfigurable()) {
			this.ipcItem = ipcItem;
		}
	}

	/**
	 * Returns true, if the configuration of the item is available
	 * @return true, it the configuration of the item is available 
	 *
	 */
	public boolean isConfigurationAvailable() {
		boolean isConfigAvailable = false;
		try {
			if (ipcItem != null && ipcItem.getConfiguration() != null) {
				isConfigAvailable = (ipcItem.getConfiguration().isAvailable());
			}
		} catch (IPCException ex) {
			log.error(ex.getMessage());
		}
		return isConfigAvailable;
	}
	/**
	 * Determines the values of all characteristic of the IPC item regarding the view
	 * (e.g. order view, detail view). 
	 * The values will be returned in one string separated by ','.
	 * 
	 * @param view The view of the application 
	 * @return The values of the charactieristics 
	 *
	 */
	public String getItemConfigValues(char view) {
		log.entering("getItemConfigValues");
		List values = null;
		Iterator valuesIterat = null;
		CharacteristicValue chsticVal = null;
		String configValues = "";
		Characteristic chstic = null;

		if (ipcItem == null) {
			// return a message that configurator is not available 
			log.debug("IPC Configuration is not available");
			return ("Configuration is not available");
		}
		try {
			if (ipcItem.getConfiguration() != null) {
				List chsticList = ipcItem.getConfiguration().getRootInstance().getCharacteristics(false, view);
				Iterator chsticIterat = chsticList.iterator();
				while (chsticIterat.hasNext()) {
					chstic = (Characteristic) chsticIterat.next();
					values = chstic.getValues();
					valuesIterat = values.iterator();

					while (valuesIterat.hasNext()) {
						chsticVal = (CharacteristicValue) valuesIterat.next();
						if (chsticVal.isAssigned() && chsticVal.isVisible()) {
							if (configValues.length() > 0) {
								configValues = configValues.concat(", ");
							}
							configValues = configValues.concat(chsticVal.getLanguageDependentName());
						}
					}
				}
			}
		} catch (IPCException ex) {
			log.error(LogUtil.APPS_BUSINESS_LOGIC, ex.getMessage());
		} catch (Exception ex2) {
			log.error(LogUtil.APPS_BUSINESS_LOGIC, ex2.getMessage());
		}
		log.exiting();
		return configValues;
	}

	/*
	 *  Returns the list of characteristic groups of the item.
	 *  @param view The view of the application
	 *  @return The list (ArrayList) of characteristic groups
	 */
	public ArrayList getItemConfigGroups(char view) {
		ArrayList charGroupList = null;
		ArrayList resultCharGroups = new ArrayList();
		CharacteristicGroup charGroup = null;
		boolean assigned;
		//String configValues = "";
		boolean isCsticSet = false;
		if (ipcItem.getConfiguration() != null
			&& ipcItem.getConfiguration().getRootInstance() != null) {
			charGroupList =	(ArrayList) ipcItem.getConfiguration().getRootInstance().getCharacteristicGroups();
		}
		for (int i = 0; i < charGroupList.size(); i++) {
			charGroup = (CharacteristicGroup) charGroupList.get(i);
			isCsticSet = false;
			if (getCharacteristics(charGroup, view) == null	|| getCharacteristics(charGroup, view).size() == 0) {
				continue;
			} else {
				//check, if any characteristics are set
				ArrayList chstics = getCharacteristics(charGroup, view);
				for (int j = 0; j < chstics.size(); j++) {
					List values = ((Characteristic) chstics.get(j)).getValues();
					Iterator valuesIterat = values.iterator();
					CharacteristicValue chsticVal;
					while (valuesIterat.hasNext()) {
						chsticVal = (CharacteristicValue) valuesIterat.next();
						if (chsticVal.isAssigned() && chsticVal.isVisible()) {
							isCsticSet = true;
							break;
						}
					}
					if (isCsticSet) {
						break;
					}
				}
				if (isCsticSet) {
					assigned = resultCharGroups.add(charGroup);
				}
			}
		}
		return resultCharGroups;
	}

	/*
	 *  Returns the name of a characteristic group
	 *  @param charGroup The characteristic group
	 *  @return The name of the characteristic group
	 */
	public String getCharacteristicGroupName(CharacteristicGroup charGroup) {
		return charGroup.getName();
	}

	/*
	 *  Returns the list of characteristics of a characteristic group
	 *  @param charGroup The characteristic group
	 *  @return  The List of characteristics of the characteristic group
	 */
	public ArrayList getCharacteristics(CharacteristicGroup charGroup,	char view) {
		ArrayList characteristics = null;
		characteristics = (ArrayList) charGroup.getCharacteristics(false, view);
		return characteristics;
	}

	/*
	 *  Returns the name of a characteristic 
	 *  @param characteristic The characteristic 
	 *  @return The name of the characteristic
	 */
	public String getCharacteristicName(Characteristic characteristic) {
		return characteristic.getLanguageDependentName();
	}

	/*
	 *  Returns the characteristic values separated by ','
	 *  @param characteristic The characteristic 
	 *  @return  The characteristics values
	 */
	public String getCharacteristicValues(Characteristic characteristic) {
		String configValues = "";
		List values = characteristic.getValues();
		Iterator valuesIterat = values.iterator();
		CharacteristicValue chsticVal;
		while (valuesIterat.hasNext()) {
			chsticVal = (CharacteristicValue) valuesIterat.next();
			if (chsticVal.isAssigned() && chsticVal.isVisible()) {
				if (configValues.length() > 0) {
					configValues = configValues.concat(", ");
				}
				configValues = configValues.concat(chsticVal.getLanguageDependentName());
			}
		}
		return configValues;
	}
}
