package com.sap.isa.isacore.uiclass;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.jsp.PageContext;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.catalog.boi.IServerEngine;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.ui.uiclass.BaseUI;

public class ShopListUI extends BaseUI {

	private String title;
	private String tableTitle;
	private ResultData tableItems;
	private String[] tableRowTitle;
	private boolean displayShopList;
    
	public static IsaLocation log =
		IsaLocation.getInstance(ShopListUI.class.getName());

	public ShopListUI(PageContext pageContext) {
		super(pageContext);
		log.entering("ShopListUI constructor");
		log.exiting();
	}

	public String getTitle() {
		log.entering("getTitle: " + title);
		log.exiting();
		return title;
	}

	public void setTitle(String title) {
		log.entering("setTitle:" + title);
		this.title = title;
		log.exiting();
	}

	public ResultData getTableItems() {
		log.entering("getTableItems" + tableItems);
		log.exiting();
		return tableItems;
	}

	public String getTableTitle() {
		log.entering("getTableTitle: " + tableTitle);
		log.exiting();
		return tableTitle;
	}

	public void setTableItems(ResultData data) {
		log.entering("setTableItems");
		tableItems = data;
		log.exiting();
	}

	public void setTableTitle(String tableTitle) {
		log.entering("setTableTitle: " + tableTitle);
		this.tableTitle = tableTitle;
		log.exiting();
	}

	public String getTableRowTitle(int index) {
		log.entering("getTableRowTitle: " + index);
		String response = new String();
		if (tableRowTitle != null) {
			if (tableRowTitle.length > index) {
				response = tableRowTitle[index];
			}
		}
		log.debug("returned value: " + response);
		log.exiting();
		return response;
	}

	public void setTableRowTitle(String[] tableRowTitle) {
		log.entering("setTableRowTitle");
		this.tableRowTitle = tableRowTitle;
		log.exiting();
	}
    
    public int getTableTitleSize() {
        log.entering("getTableTitleSize");
        int response = 0;
        if(tableRowTitle != null) {
            response = tableRowTitle.length;
        }
        log.exiting();
        return response;
    }

	public boolean isDisplayShopList() {
		return displayShopList;
	}

	public void setDisplayShopList(boolean displayShopList) {
		log.entering("setDisplayShopList");
		this.displayShopList = displayShopList;
		log.exiting();
	}

	/**
	 * Return the date format. <br>
	 * The Format is taken from the salesDocConfiguration.
	 * This is the format, that must be used for the new calendar control.
	 * 
	 * @return the date format taken from the salesDocConfiguration
	 */
	public String getDefaultDateFormat() {
		log.entering("getDefaultDateFormat");
		String dateFormat = null;
		// check for missing context
		if (userSessionData != null) {
			// get BOM
			BusinessObjectManager bom =	(BusinessObjectManager) userSessionData.getBOM(
            			BusinessObjectManager.ISACORE_BOM);

			// now I can read the shop
			Shop shop = bom.getShop();
			dateFormat = shop.getDateFormat();
			dateFormat = dateFormat.replace('Y', 'y');
			dateFormat = dateFormat.replace('D', 'd');
		}
		log.exiting();
		return dateFormat;
	}
    
    public String getToday(){
        log.entering("today");
        SimpleDateFormat fmt = new SimpleDateFormat(getDefaultDateFormat());
        Date date = new Date();
        log.exiting();
        return fmt.format(date);
   
    }
    
    /**
     * Return the information if a link for the catalog should be displayed.
     * Only for active an inactive catalogs a link is displayed. 
     * @param  replStatus  as String value, replication status of the catalog
     * @return displayLink as boolean value, if a link shall be displayed 
     */
    public boolean isDisplayLink(String replStatus) {
        log.entering("isDisplayLink");
        if (log.isDebugEnabled()) {
            log.debug("Replication status = " + replStatus);
        }

        boolean displayLink = replStatus.equals(IServerEngine.STAGING_TYPE_ACTIVE)  ||
                              replStatus.equals(IServerEngine.STAGING_TYPE_INACTIVE) ||
                              replStatus.equals(IServerEngine.STAGING_TYPE_OBSOLETE);

        if (log.isDebugEnabled()) {
            log.debug("is Link displayed = " + displayLink);
        }
        log.exiting();
        return displayLink;
    }
    
    /**
      * Always returns false
      */
    public boolean  showCatalogueStagingIPCDate() {
        return false;
    }

}
