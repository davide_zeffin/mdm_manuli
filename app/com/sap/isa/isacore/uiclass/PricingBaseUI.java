/**
 * Class:        PricingBaseUI Copyright (c) 2006, SAP AG, All rights reserved.
 * Author:       SAP AG
 * Created:      18.04.2006
 * Version:      1.0 
 * $Revision$ 
 * $Date$
 */
package com.sap.isa.isacore.uiclass;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;

import javax.servlet.jsp.PageContext;

import com.sap.isa.backend.boi.PricingBusinessObjectsAware;
import com.sap.isa.backend.boi.PricingConfiguration;
import com.sap.isa.backend.boi.isacore.BaseConfiguration;
import com.sap.isa.backend.boi.webcatalog.pricing.PriceInfo;
import com.sap.isa.backend.boi.webcatalog.pricing.PriceInfoComparator;
import com.sap.isa.backend.boi.webcatalog.pricing.PriceType;
import com.sap.isa.businessobject.ProductBaseData;
import com.sap.isa.businessobject.webcatalog.CatalogBusinessObjectManager;
import com.sap.isa.catalog.actions.ActionConstants;
import com.sap.isa.catalog.webcatalog.WebCatArea;
import com.sap.isa.catalog.webcatalog.WebCatItemPrice;
import com.sap.isa.core.interaction.InteractionConfig;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.imp.rfc.RFCIPCItemReference;

/**
 * This class contains basic catalog relevant UI class methods in regards to pricing
 */
public class PricingBaseUI extends InlineConfigBaseUI {

    static public IsaLocation log = IsaLocation.getInstance(PricingBaseUI.class.getName());
    
    final public String DEFAULT_PRICE_DIV_NAME = "cat-prc-detail";
    protected int scalePriceDivId = 0; // used to generate an unique name for scale price div containers
    protected boolean displayEyeCatcherText = true;
    protected String priceDivName = DEFAULT_PRICE_DIV_NAME;
    protected String titleTextKey = null;
    protected String titleClass = null;
    protected boolean hidePromotionalPrices = false;
    
    protected ProductBaseData priceProd = null;
    protected int showPriceInfo = 0;
    protected boolean showLabelAsPostfix = true;
    
    private boolean displayPriceInDetailsPage = false;
    
    protected PricingConfiguration priceConfig = null;

    /**
     * Standard constructor. <br>
     * 
     * @param pageContext
     */
    public PricingBaseUI() {
        log.entering("PricingBaseUI()");
        
        // check for missing context
        if (userSessionData != null) {
        
            PricingBusinessObjectsAware priceBom = (PricingBusinessObjectsAware) userSessionData.getMBOM().getBOMByType(PricingBusinessObjectsAware.class);
        
            // now I can read the shop
            priceConfig = priceBom.getPricingConfiguration();
        }
        
        log.exiting();
    }

    /**
     * Create new Instance
     *
     * @param pageContext DOCUMENT ME!
     */
    public PricingBaseUI(PageContext pageContext) {
        super(pageContext);
        
        // check for missing context
        if (userSessionData != null) {
        
            PricingBusinessObjectsAware priceBom = (PricingBusinessObjectsAware) userSessionData.getMBOM().getBOMByType(PricingBusinessObjectsAware.class);
        
            // now I can read the shop
            priceConfig = priceBom.getPricingConfiguration();
        }
        
        log.entering("PricingBaseUI(PageContext pageContext)");
 
        log.exiting();
    }

    /**
     * Calculates and returns the next id, to be used for scalePriceDivs,
     * by adding 1 to the current value
     *
     * @return int the next id, to be used for scalePriceDivs
     */
    public int createNextScalePriceDivId() {
        log.entering("createNextScalePriceDivId");
        scalePriceDivId++;
        log.exiting();
        return scalePriceDivId;
    }

    
    /**
     * Returns true, if RCFItemReference for the priceInfo object, if present
     *
     * @param priceInfo object to determine the IPCItemRefernce
     *
     * @return RFCIPCItemReference of the priceInfo object, if available null, if not
     */
    public RFCIPCItemReference getIPCItemRef(PriceInfo priceInfo) {

        log.entering("getIPCItemRef(PriceInfo priceInfo)");

        RFCIPCItemReference ipcItemRef = null;
 
        if (log.isDebugEnabled()) {
            log.debug("determine IPCPItemRefernce for priceInfo " + priceInfo);
        }

        IPCItem priceRef = null;

        if (priceInfo != null) {
            priceRef = (IPCItem) priceInfo.getPricingItemReference();
            if (priceRef != null) {
                ipcItemRef = (RFCIPCItemReference) priceRef.getItemReference();
            }
            else {
                log.debug("priceInfo object has no IPCItem");
            }
        }
        else {
            log.debug("priceInfo object is null");
        }

        if (log.isDebugEnabled()) {
            log.debug("determine IPCPItemRefernce for priceInfo " + priceInfo + " : " + ipcItemRef);
        }
        
        log.exiting();

        return ipcItemRef;
    }

    /**
     * Returns colspan for non cscale price
     * 
     * @param PriceInfo[] prices all prices that will be displayed prices.
     */
    public String getNonScalePriceColSpan(PriceInfo[] prices) {

        log.entering("getNonScalePriceColSpan()");

        String retVal = "1";

        // scales prices contained?
        if (prices != null) {
            for (int i = 0; i < prices.length; i++) {
                if (prices[i].isScalePrice()) {
                    retVal = "4";
                    break;
                }
            }
        }

        if (log.isDebugEnabled()) {
            log.debug("getNonScalePriceColSpan =" + retVal);
        }

        log.exiting();

        return retVal;
    }

    /**
     * Sets all price relevant infos at one glance
     */
    public void setPriceRelevantInfo(ProductBaseData priceProd, boolean isScRelevant, boolean showLabelAsPostfix) {
        log.entering("setPriceRelevantInfo(ProductBaseData priceProd, boolean scRelevant, boolean showLabelAsPostfix)");
        
        int priceInfo = WebCatItemPrice.SHOW_ALL_PRICES;
        if (isScRelevant) {
            log.debug("isScRelevant");
            
            if (priceProd.hasSubItems()) {
                priceInfo = WebCatItemPrice.SHOW_SUMS_ONLY;
            }
            else {
                log.debug("Items has no subitems, so no sums will be available, show all prices instead");
            }
        }
        else {
            log.debug("Not scRelevant");
        }

        setPriceRelevantInfo(priceProd, priceInfo , showLabelAsPostfix);

        log.exiting();
    }

    /**
     * Sets all price relevant infos at one glance
     */
    public void setPriceRelevantInfo(ProductBaseData priceProd, int showPriceInfo, boolean showLabelAsPostfix) {
        log.entering("setPriceRelevantInfo(ProductBaseData priceProd, int showPriceInfo, boolean showLabelAsPostfix)");
        this.priceProd = priceProd;
        this.showPriceInfo = showPriceInfo;
        this.showLabelAsPostfix = showLabelAsPostfix;

        setDisplayEyeCatcherText(true);
        setPriceDivName(DEFAULT_PRICE_DIV_NAME);
        setTitleClass(null);
        setTitleTextKey(null);
        hidePromotionalPrices = false;
        
        if (log.isDebugEnabled()) {
            log.debug("priceProd = " + priceProd + " showPriceInfo = " + showPriceInfo + " showLabelAsPostfix = " + showLabelAsPostfix);
        }
        
        log.exiting();
    }

    /**
     * Returns the page Context
     * index <code>start</code>.
     */
    public PageContext getPageContext() {
        log.entering("getPageContext()");
        log.exiting();
        return pageContext;
    }

    /**
     * Returns the size of a scale price group, that begins at the
     * index <code>start</code>.
     */
    public int getScalePriceGroupSize(PriceInfo[] prices, int start) {

        log.entering("getScalePriceGroupSize()");

        int retVal = 1;

        // scale price
        if (prices[start].isScalePrice()) {
            for (int i = start + 1; i < prices.length; i++) {
                if (!isSameScalePriceGroup(prices[start], prices[i])) {
                    break;
                }
                retVal++;
            }
        }
        
        if (log.isDebugEnabled()) {
            log.debug("getScalePriceGroupSize()=" + retVal);
        }
        
        log.exiting();

        return retVal;
    }

	/**
	 *  Returns true, if there is at least one recurrent price.
	 * 
	 * @param prices
	 * @return
	 */
    public boolean hasRecurrentPrices(PriceInfo[] prices) {

        boolean retVal = false;
        
        if (prices != null) {
            for (int i = 0; i < prices.length; i++) {
                if (prices[i].getPeriodType() != PriceInfo.ONE_TIME) {
                    retVal = true;
                    break;
                }
            }
        }

        return retVal;
    }

    /**
     * Returns true, if the price info of the given index is the first entry
     * of a scale price group.
     */
    public boolean isScalePriceBegin(PriceInfo[] prices, int i) {

        log.entering("isScalePriceBegin()");

        // no scale price
        if (!prices[i].isScalePrice()) {
            return false;
        }
        // first entry
        if (i == 0) {
            return true;
        }

        // The entry before this must not have the same type
        boolean ret = isSameScalePriceGroup(prices[i], prices[i - 1]);

        if (log.isDebugEnabled()) {
            log.debug("isScalePriceBegin()=" + ret);
        }
        
        log.exiting();

        return !ret;
    }

    /**
     * Returns true, if the price info of the given index is the last entry
     * of a scale price group.
     */
    public boolean isScalePriceEnd(PriceInfo[] prices, int i) {

        log.entering("isScalePriceEnd()");

        boolean retVal = false;

        // no scale price
        if (prices[i].isScalePrice() || i != 0) {
            // last entry
            if (i == prices.length - 1) {
                retVal = true;
            }
            else {
                // The entry after this must not have the same type
                retVal = !isSameScalePriceGroup(prices[i], prices[i + 1]);
            }
        }

        if (log.isDebugEnabled()) {
            log.debug("isScalePriceEnd()=" + retVal);
        }
        
        log.exiting();

        return retVal;
    }
    
    /**
     * Returns true, if the price info of the given index is the scale price
     * matching the given quantity
     */
    public boolean isScalePriceForQuantity(PriceInfo[] prices, int i) {

        log.entering("isScalePriceForQuantity()" );
        
        if (log.isDebugEnabled()) {
            log.debug("Price index i=" + i);
        }

        boolean retVal = false;
        
        String decSeparator = priceConfig.getDecimalSeparator();
        String groupSeparator = priceConfig.getGroupingSeparator();

        // no scale price
        if (!prices[i].isScalePrice()) {
            log.debug("no scale price");
            return false;
        }
        
        BigDecimal scaleQuantity = null;
        
        BigDecimal compareQuantity = convertToBigDec(priceProd.getQuantityAsStr(), decSeparator, groupSeparator);
        
        if ("*99999999,99900".equals(prices[i].getQuantity())) {  // this means to infite for to scale types values
            log.debug("scaleQuantity is *99999999,99900");
            scaleQuantity = compareQuantity;
        }
        else {
            scaleQuantity = convertToBigDec(prices[i].getQuantity(), decSeparator, groupSeparator);
        }
        
        if (log.isDebugEnabled()) {
            log.debug("quantity to check =" + priceProd.getQuantityAsStr() +" as BigDecimal =" + compareQuantity);
            log.debug("scale quantity  =" + prices[i].getQuantity() + " as BigDecimal =" + scaleQuantity);
            log.debug("Scale type = " + prices[i].getScaletype());
        }
        
        if (prices[i].getScaletype().equals(PriceInfo.SCALE_TYPE_FROM)) {
            log.debug("Scale Type From");
            retVal = compareQuantity.compareTo(scaleQuantity) > -1;
            if (retVal && prices.length > i && 
                isSameScalePriceGroup(prices[i], prices[i + 1]) && 
                prices[i].isPayablePrice() == prices[i +1].isPayablePrice()) {
                // is there a upper limit we have to check ?
                BigDecimal nextScaleQuantity = convertToBigDec(prices[i + 1].getQuantity(), decSeparator, groupSeparator);
                if (log.isDebugEnabled()) {
                    log.debug("next scale quantity  =" + prices[i + 1].getQuantity() + " as BigDecimal =" + scaleQuantity);
                } 
                retVal =compareQuantity.compareTo(nextScaleQuantity) < 0;   
            }
            else {
                log.debug("Scale price End");
            }  
        }
        else {
            log.debug("Scale Type To");
            retVal = compareQuantity.compareTo(scaleQuantity) < 1;
            if (retVal && i > 0 && 
                isSameScalePriceGroup(prices[i - 1], prices[i]) && 
                prices[i - 1].isPayablePrice() == prices[i].isPayablePrice()) {
                // is there a lower limit we have to check ?
                BigDecimal prevScaleQuantity = convertToBigDec(prices[i - 1].getQuantity(), decSeparator, groupSeparator);
                if (log.isDebugEnabled()) {
                    log.debug("previous scale quantity  =" + prices[i - 1].getQuantity() + " as BigDecimal =" + prevScaleQuantity);
               }
               retVal = compareQuantity.compareTo(prevScaleQuantity) > -1;
            }
            else {
                log.debug("Scale price Begin");    
            }
        }

        if (log.isDebugEnabled()) {
            log.debug("isScalePriceForQuantity()=" + retVal);
        }
        
        log.exiting();

        return retVal;
    }
    
    /**
     * Converts a String, representing a figure (e.g 12,345.67), into a BigDecimal object. The <code>
     * initData</code> object delivers the information about grouping separator and decimal 
     * separator.<br> If content of figure can not be convert a BigDecimal object with value '0'
     * will be returned.
     * @param figure which should be converted (e.g 12,345.67)  
     * @param initData including information about grouping and decimal separator
     * @return BigDecimal object
     */
    protected static BigDecimal convertToBigDec(String figure, String decSeparator, String groupSeparator) {
        log.entering("convertToBigDec()");
        char[] figCharsOld = figure.toCharArray();
        char[] figCharsNew = new char[figCharsOld.length];
        StringBuffer figNew = new StringBuffer();

        // Remove grouping Separator
        for (int i = 0; i < figCharsOld.length; i++) {
            if (figCharsOld[i] != (groupSeparator.toCharArray())[0]) {
                figCharsNew[i] = figCharsOld[i];
            }
        }
        if (!".".equals(decSeparator)) {
            // Decimal Separator is not ".", so convert it
            for (int i = 0; i < figCharsNew.length; i++) {
                if (figCharsNew[i] == (decSeparator.toCharArray())[0]) {
                    figCharsNew[i] = '.';
                }
            }
        }
        for (int i = 0; i < figCharsNew.length; i++) {
            if (figCharsNew[i] != '\u0000') {
                figNew.append(figCharsNew[i]);
            }
        }
        BigDecimal retVal;
        try {
            retVal = new BigDecimal(figNew.toString());
        }
        catch (NumberFormatException nfx) {
            if (log.isDebugEnabled()) {
                log.debug("PriceCalulatorCRMIPC.convertToBigDec could not convert >" + figNew + "<");
            }
            retVal = new BigDecimal("0");
        }
        log.exiting();
        return retVal;
    }

    /**
     * Returns true, if both price info objects are belonging to the same
     * scale price group.
     */
    public boolean isSameScalePriceGroup(PriceInfo price1, PriceInfo price2) {
        log.entering("isSameScalePriceGroup()");
        log.exiting();
        return price1.getScalePriceGroup() == price2.getScalePriceGroup();
    }

    /**
     * Returns true, if the price analysis link for the priceInfo should be dsiplayed
     *
     * @param product the item to check
     *
     * @return true   if the price analysis link should be shown false  otherwise
     */
    public boolean showPriceAnalysisLink(PriceInfo priceInfo) {

        log.entering("showPriceAnalysisLink(PriceInfo priceInfo)");

        boolean showAnalysisLink = false;

        if (priceInfo != null) {
            showAnalysisLink = priceConfig.isPricingCondsAvailable() && priceInfo.getPricingItemReference() != null;
            if (log.isDebugEnabled()) {
                log.debug("test if price analysis link should be shown for the priceInfo " + priceInfo + " : " + showAnalysisLink);
            }  
        }
        else {
            log.debug("priceInfo is null");
        }

        log.exiting();

        return showAnalysisLink;
    }

    /**
     * Returns true, if both price info objects are belonging to the same
     * scale price group.
     */
    protected boolean showPrices() {
        log.entering("showPrices()");
        boolean showPrices = true;

        if (priceProd != null && priceProd.isRelevantForExplosion() && !isDisplayPriceInDetailsPage()) {
            showPrices = showPricesForSCRelItemsInListView();
        }

        if (log.isDebugEnabled()) {
            log.debug("showPrices()=" + showPrices);
        }

        log.exiting();
        return showPrices;
    }
    
    /**
     * Returns true, if no prices at all should be displayed
     */
    public boolean isNoPriceUsed() {
        log.entering("isNoPriceUsed()");

        if (log.isDebugEnabled()) {
            log.debug("isNoPriceUsed()=" + priceConfig.isNoPriceUsed());
        }

        log.exiting();
        return priceConfig.isNoPriceUsed();
    }

    /**
     * Determine from the interaction config Conatiner, if prices should be shown for SC relevant items.
     */
    protected boolean showPricesForSCRelItemsInListView() {
        log.entering("showPricesForSCRelItemsInListView()");

        boolean showPrices = true;
        String showPricesParam = null;

        InteractionConfig uiConf = getInteractionConfig().getConfig(ActionConstants.IN_UI_CONFIG_ID);

        if (uiConf != null) {
            showPricesParam = uiConf.getValue(ActionConstants.IN_LISTVIEW_SHOW_SC_PRICES);
            if (showPricesParam != null) {
                if (log.isDebugEnabled()) {
                    log.debug("ActionConstants.IN_LISTVIEW_SHOW_SC_PRICES =" + showPricesParam);
                }
                showPrices = showPricesParam.equals("true");
            }
            else {
                log.debug("ActionConstants.IN_LISTVIEW_SHOW_SC_PRICES not set");
            }
        }
        else {
            log.debug("No Interaction Config conatiner found");
        }

        log.exiting();

        return showPrices;
    }

    /**
     * Returns the price info object to display for the current price product.
     * 
     * @return PriceInfo[] the list of PriceInfo objects, to display 
     */
    public PriceInfo[] getPricesForPriceProduct() {

        PriceInfo[] priceInfos = new PriceInfo[0];

        PriceType[] priceTypeArray = null;
        int[] showPriceInfoArray = null;

        if (priceProd != null) {

            if (showPrices()) {
                PriceType priceType = null;

                if (priceProd.getProductPrice() != null
                    && priceProd.getProductPrice().getPriceInfo() != null
                    && priceProd.getProductPrice().getPriceInfo().getPriceType() != null) {

                    priceType = priceProd.getProductPrice().getPriceInfo().getPriceType();
                    if (log.isDebugEnabled()) {
                        log.debug("PriceType= " + priceType);
                    }

                    if (priceType.equals(PriceType.LIST_VALUE) || priceType.equals(PriceType.SCALE_VALUE)) {
                        priceTypeArray = new PriceType[] { PriceType.LIST_VALUE, PriceType.SCALE_VALUE };
                        showPriceInfoArray = new int[] { showPriceInfo, WebCatItemPrice.SHOW_ALL_PRICES };
                        log.debug("List and scale");
                    }
                    else {
                        priceTypeArray = new PriceType[] { priceType };
                        showPriceInfoArray = new int[] { showPriceInfo };
                    }

                    if (log.isDebugEnabled()) {
                        log.debug("Determine price infos for Price Type(s) = " + priceType + " showPriceInfo = " + showPriceInfo);
                    }

                    WebCatItemPrice webCatItemPrice = priceProd.getProductPrice();

                    if (webCatItemPrice != null) {
                        priceInfos = webCatItemPrice.getPriceInfosForPriceTypes(priceTypeArray, showPriceInfoArray, true);
                        
                        if (priceInfos != null && priceInfos.length > 1) {
                            log.debug("Start sorting price infos");
                            ArrayList sortArray = new ArrayList(priceInfos.length);
                            for (int i = 0; i < priceInfos.length; i++) {
                                sortArray.add(priceInfos[i]);
                            }
                            Collections.sort(sortArray, new PriceInfoComparator());
                        
                            for (int i = 0; i < sortArray.size(); i++) {
                                priceInfos[i] = (PriceInfo) sortArray.get(i);
                            }
                            log.debug("Finished Sorting price infos");
                        }
                    }
                    else {
                        log.debug("webCatItemPrice is null");
                    }
                }
                else {
                    log.debug("Could not determine price Type");
                }
            }
        }
        else {
            log.debug("priceProd is null");
        }
        
        log.exiting();

        return priceInfos;
    }

    /**
     * Returns the Price Eye Catcher text for the current price product
     * if present, null else
     */
    public String getPriceEyeCatcherText() {
        log.entering("getPriceEyeCatcherText()");
        String text = null;

        if (priceProd != null) {
            if (priceProd.getParentTechKey() == null || priceProd.getParentTechKey().isInitial()) {
                text = priceProd.getPriceEyeCatcherText();
            }
            else {
                log.debug("No price Eye Catcher returned, because item is subItem");
            }
        }

        log.exiting();
        return text;
    }

    /**
     * Returns the Product, prices to be displayed for
     * 
     * @return ProductBaseData the product
     */
    public ProductBaseData getPriceProd() {
        log.entering("getPriceProd()");
        log.exiting();
        return priceProd;
    }

    /**
     * Returns the current ScalePriceDivId
     */
    public int getScalePriceDivId() {
        log.entering("getScalePriceDivId()");
        log.exiting();
        return scalePriceDivId;
    }

    /**
     * Returns the flag, if price labels should be shown as postfix
     */
    public boolean isShowLabelAsPostfix() {
        log.entering("getPriceProd()");
        log.exiting();
        return showLabelAsPostfix;
    }

    /**
     * Returns the flag, if only sums should be shown
     */
    public boolean isSumsOnly() {
        log.entering("isSumsOnly()");
        log.exiting();
        return showPriceInfo == WebCatItemPrice.SHOW_SUMS_ONLY;
    }

    /**
     * Returns <code>true</code>, if no scale prices should be displayed.
     */
    public boolean noScalePrice() {
        
        log.entering("noScalePrice()");
        if (getPriceProd() == null) {
            log.debug("priceProd is null, returning false");
            log.exiting();
            return false;
        }
        
        if (getPriceProd().isSubComponent()) {
            log.debug("noScalePrice=true");
            log.exiting();
            return true;
        }
        
        log.debug("noScalePrice=false");
        log.exiting();
        return false;
    }
    
    /**
     * Sets the Product, prices to be displayed for
     */
    public void setPriceProd(ProductBaseData priceProd) {
        log.entering("setPriceProd()");
        this.priceProd = priceProd;
        log.exiting();
    }

    /**
     * Sets the flag, if price labels should be shown as postfix
     */
    public void setShowLabelAsPostfix(boolean showLabelAsPostfix) {
        log.entering("setShowLabelAsPostfix()");
        this.showLabelAsPostfix = showLabelAsPostfix;
        log.exiting();
    }

    /**
     * Sets the flag, if only sums should be shown
     */
    public void setSumsOnly(boolean sumsOnly) {
        log.entering("setSumsOnly()");
        this.showPriceInfo = WebCatItemPrice.SHOW_SUMS_ONLY;
        log.exiting();
    }

    /**
     * Returns <code>true</code> if the eye catcher text should be displayed.
     */
    public boolean displayEyeCatcherText() {
        return displayEyeCatcherText;
    }

    /**
     * Controls the display of the ey catcher text.
     */
    public void setDisplayEyeCatcherText(boolean displayEyeCatcherText) {
        this.displayEyeCatcherText = displayEyeCatcherText;
    }

    /**
     * Returns the name of the div tag to display the prices.
     */
    public String getPriceDivName() {
        return priceDivName;
    }
    
    /**
     * Sets the name of the div tag that is used to display the prices in the
     * current context. Default is "b2c-prc-detail".
     */
    public void setPriceDivName(String priceDivName) {
        this.priceDivName = priceDivName;
    }

    public String getTitleClass() {
        return titleClass;
    }

    public String getTitleTextKey() {
        return titleTextKey;
    }

    public void setTitleClass(String string) {
        titleClass = string;
    }

    public void setTitleTextKey(String string) {
        titleTextKey = string;
    }
    
    public void setHidePromotionalPrices(boolean hidePromotionalPrices) {
        this.hidePromotionalPrices = hidePromotionalPrices;
    }

    public boolean getHidePromotionalPrices() {
       return hidePromotionalPrices;
    }
    
	public boolean isDisplayPriceInDetailsPage() {
		return displayPriceInDetailsPage;
	}

	public void setDisplayPriceInDetailsPage(boolean displayPriceInDetailsPage) {
		this.displayPriceInDetailsPage = displayPriceInDetailsPage;
	}
    
    /**
     * Return if the prices are visible in the catalogue and the marekting area. 
     * <br>
     * The authority check will be done in the catalogue area and only in the B2B
     * application. 
     * 
     * @return <code>true</code> if the price should be displayed.
     */
    public boolean isPriceVisible() {
        
        if (priceConfig.getApplication().equals(BaseConfiguration.B2B)) {
            return super.isElementVisible("catalog.prices");
        }

        return true;        
    }
    
    public String getLastVisited() {
        if (userSessionData != null) {
             CatalogBusinessObjectManager bom =
                       (CatalogBusinessObjectManager)userSessionData.getBOM(CatalogBusinessObjectManager.CATALOG_BOM);
             if (bom.getCatalog()!= null ) {
                 return bom.getCatalog().getLastVisited();
             }
        }
        return null;
    }
    
    /**
     * Initialize UI clasee . <br>
     * 
     * @param pageContext current page context
     * 
     * 
     * @see com.sap.isa.ui.uiclass.BaseUI#initContext(javax.servlet.jsp.PageContext)
     */
    public void initContext(PageContext pageContext) {
        
        super.initContext(pageContext);
        
        // check for missing context
        if (userSessionData != null) {
        
            PricingBusinessObjectsAware priceBom = (PricingBusinessObjectsAware) userSessionData.getMBOM().getBOMByType(PricingBusinessObjectsAware.class);
        
            // now I can read the shop
            priceConfig = priceBom.getPricingConfiguration();
        }
    }

    /**
     * Return if the points should be displayed instead of prices. 
     * 
     * @return <code>true</code> if the points should be displayed.
     */
    public boolean displayPoints() {
        log.entering("displayPoints()");

        boolean displayPts = false;
                
        if (userSessionData != null) {
            CatalogBusinessObjectManager bom =
                  (CatalogBusinessObjectManager)userSessionData.getBOM(CatalogBusinessObjectManager.CATALOG_BOM);
            if (bom.getCatalog()!= null) {
                WebCatArea catArea = bom.getCatalog().getCurrentArea();
                if (catArea != null) {
                    displayPts = (catArea.isRewardCategory());
                }
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("display points = " + displayPts);
        }

        log.exiting();        
        return (displayPts);        
    }

    /**
     * Returns the points unit for the current price product
     * By default always null
     * The method will be implemented by sub classes
     */
    public String getPointsUnit() {
        return null;
    }

    /**
     * Returns the language dependen description of the points unit for the current price product
     * By default always null
     * The method will be implemented by sub classes
     */
    public String getPointsUnitDescr() {
        return null;
    }

    /**
     * Returns the language dependen description of the points unit for the current price product
     * By default always null
     * The method will be implemented by sub classes
     */
    public boolean isWrongPtUnit(String ptUnit) {
        boolean isWrongPtUnit = true;
        if (ptUnit != null && ptUnit.length() > 0) {
            if (getPointsUnit() != null) {
                isWrongPtUnit = !ptUnit.equals(getPointsUnit());                
            }
        }
        return isWrongPtUnit;
    }

    /**
     * Return if the buy points area should be displayed. 
     * 
     * @return <code>true</code> if the buy points area should be displayed.
     */
    public boolean isBuyPointsArea() {
        log.entering("isBuyPointsArea()");

        boolean isBuyPts = false;
                
        if (userSessionData != null) {
            CatalogBusinessObjectManager bom =
                  (CatalogBusinessObjectManager)userSessionData.getBOM(CatalogBusinessObjectManager.CATALOG_BOM);
            if (bom.getCatalog()!= null) {
                WebCatArea catArea = bom.getCatalog().getCurrentArea();
                if (catArea != null) {
                    isBuyPts = (catArea.isBuyPointsCategory());
                }
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("is buy points area = " + isBuyPts);
        }

        log.exiting();        
        return (isBuyPts);        
    }

    /**
     * Return if the reward area should be displayed. 
     * 
     * @return <code>true</code> if the reward area should be displayed.
     */
    public boolean isRewardArea() {
        log.entering("isRewardArea()");

        boolean isReward = false;
                
        if (userSessionData != null) {
            CatalogBusinessObjectManager bom =
                  (CatalogBusinessObjectManager)userSessionData.getBOM(CatalogBusinessObjectManager.CATALOG_BOM);
            if (bom.getCatalog()!= null) {
                WebCatArea catArea = bom.getCatalog().getCurrentArea();
                if (catArea != null) {
                    isReward= (catArea.isRewardCategory());
                }
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("is reward area = " + isReward);
        }

        log.exiting();        
        return (isReward);        
    }

    /**
     * Return if the qunatities are visible in the catalogue and the marekting area. 
     * 
     * @return <code>true</code> if the quantity should be displayed.
     */
    public boolean isQuantityVisible(ProductBaseData product) {
        log.entering("isQuantityVisible(" + product.getId() + ")");
        boolean isVisible = true;
        
        if (product.isBuyPtsItem()) {
            isVisible = false;
        }
        
        if (log.isDebugEnabled()) {
            log.debug("is quantity visible = " + isVisible);
        }

        log.exiting();        
        return isVisible;        
    }
    
    /**
     * Return if the quantity column is visible in the catalogue . 
     * By default always return true
     * The method will be implemented by sub classes
     * @return <code>true</code> if the quantity column should be displayed.
     */
    public boolean isQuantityVisible() {
        return true;
    }

}
