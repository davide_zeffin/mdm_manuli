package com.sap.isa.isacore.uiclass;

import javax.servlet.jsp.PageContext;

import com.sap.isa.businessobject.item.ItemBase;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.interaction.InteractionConfig;
import com.sap.isa.core.interaction.InteractionConfigContainer;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.JspUtil;
import com.sap.isa.core.util.MessageList;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.ManagedDocumentLargeDoc;

/**
 * Base UI class to handle a sales document base object. <br>
 * 
 * <strong>Note</strong> Some informations are available as public properties
 * of the class.
 * 
 *
 * @author SAP
 * @version 1.0
 */

public abstract class SalesDocumentBaseUI extends InlineConfigBaseUI {


	/**
	 * Alternator to to differ between even and odd lines. <br>
	 */
	public JspUtil.Alternator alternator;


	/**
	 * Reference to list of message, whic are assigned to the document. <br>  
	 */
	public MessageList docMessages;
	
	/**
	 * String, which contains all doc messages in one string. <br>
	 * See {@link # getMessageString} for details.
	 */
	public String docMessageString = "";
	
	/**
	 * Number of lines to display for new positions. <br>
	 */
	public int newpos = 0;
    
    /**
     * Initial Number of lines to display for new positions. Set in the UI component<br>
     */
    public int initialNewpos = 0;
	
	/**
	 * Current line number. <br>
	 */
	public int line = 0;


	/**
	 * Array with the units of measurement of the current line item. <br> 
	 */	
	public String[] units;
	
	/**
	 * String, which contains all item messages in one string. <br>
	 * See {@link # getMessageString} for details.
	 */
	public String itemMessageString = "";
	
	/**
	 * String to differ between "even" and "odd" lines. <br>
	 */
	public String even_odd = "";
	
	/**
	 * Threshold for large documents. <br>
	 */
	public int noOfItemsThreshold = 0; 

	/**
	 * This property is used by the batches 
	 */	
	public int batchCountTotal = 0;

	/**
	 * This property is used by the batches 
	 */	
	public int batchCountCheckBox = 0;


	/**
	 * Flag, if the product tech key is zero. <br>
	 * If <code>true</code> teh product id doesn't exist in the Backend.  
	 */
	public boolean isZeroProductId;
	
	
	/**
	 * document date taken from ManagedDocument. <br>
	 */ 
	protected String docDate = "";
	
	/**
	 * document number taken from ManagedDocument. <br>
	 */ 
	protected String docNumber = "";
	
	/**
	 * document type taken from ManagedDocument. <br>
	 */ 
	protected String docType = "";

	/**
	 * Reference to current document handler. <br>
	 */
	protected DocumentHandler documentHandler; 

	
	/**
	 * corresponding managed document. <br>   
	 */ 	
	protected ManagedDocument mdoc = null;
    
    /**
     * Separator to separate message concatenate by the {@link # getMessageString} 
     * method. <br> The default value is &lt;br&gt;.
     */
	protected String messageSeparator = "<br>";
	static final protected IsaLocation log = IsaLocation.getInstance(SalesDocumentBaseUI.class.getName());
	
    
    /**
     * Constructor for SalesDocumentUI. <br>
     *
     * @param pageContext
     */
    public SalesDocumentBaseUI(PageContext pageContext) {

		initContext(pageContext);	
    }


	/**
	 * Constructor for SalesDocumentUI. <br>
	 *
	 */
	public SalesDocumentBaseUI() {

	}


	/**
	 * Initialize the object from the page context. <br>
	 * Some abstract methods are called within this method:
	 *
     * determineSalesDocument();  {@link #determineSalesDocument}
	 * initDocument();            {@link #initDocument}            
	 * determineHeader();         {@link #determineHeader}         
	 * determineItems();          {@link #determineItems}          
	 * determineNewPos();         {@link #determineNewPos}         
	 * determineMessages();       {@link #determineMessages}       
	 * 
	 * @param pageContext page context
	 */
	public void initContext(PageContext pageContext) {
		final String METHOD_NAME = "initContext()";
		log.entering(METHOD_NAME);
		super.initContext(pageContext);

		DocumentHandler documentHandler = getDocumentHandler(userSessionData);
		
		if (documentHandler !=null ) {	
			mdoc = documentHandler.getManagedDocumentOnTop();
		
			if (mdoc != null) {
				docDate = removeNull(mdoc.getDate());
				docNumber = removeNull(mdoc.getDocNumber());
				docType = removeNull(mdoc.getDocType());
			}		
		}

		determineSalesDocument();
		
		initDocument();
        
		determineHeader();

		determineItems();

		determineNewPos();

		determineMessages();

		docMessageString = getMessageString(docMessages);
        
		alternator = new JspUtil.Alternator(new String[] {"odd", "even" });
		log.exiting();
	}


    /**
     * Returns date of the document taken form the managed document.
     * 
     * @return String != null 
     */
    public String getDocDate() {
        return docDate;
    }

    /**
     * Returns document number taken form the managed document.
     * 
     * @return String != null
     */
    public String getDocNumber() {
        return docNumber;
    }


    /**
     * Returns type of the document taken form the managed document.
     * 
     * @return String != null
     */
    public String getDocType() {
        return docType;
    }


	/**
	 * Initialize all item relvant fields.
	 * 
	 * @param item current item position
	 */
	public void initItem(ItemBase item) {
		final String METHOD_NAME = "initItem()";
		log.entering(METHOD_NAME);
		MessageList messages = item.getMessageList();
        
		itemMessageString = getMessageString(messages);

		// itemMessageString = "";
		// for (int i = 0; i < messages.size(); i++) {
		//  itemMessageString += messages.get(i).getDescription();
		// }

		line++;

		units = item.getPossibleUnits();
        
		if (units == null || units.length == 0) {
			units = new String[] {item.getUnit()}; 
		}

		TechKey techKey = item.getProductId();
		String prodId = (techKey != null) ? techKey.getIdAsString() : "";
        
		// Test for not existing product-ID in Backend => ProductID consists of zeros 
        // or productId is not set at all
		isZeroProductId = ("".equals(prodId)? true : false);
		char[] prodIdArray = prodId.toCharArray();
		for (int arrayPos = 0; arrayPos < prodIdArray.length; arrayPos++) {
			if (prodIdArray[arrayPos] == '0') {
				isZeroProductId = true;
			}
			else {
				isZeroProductId = false;
				break;
			}
		}
		log.exiting(); 

	}


	/**
	 * Return the property {@link #isZeroProductId}. <br>
	 * 
	 * @return
	 */
	public boolean isZeroProductId() {
		return isZeroProductId;
	}


	/**
	 * Determine the sales document from the page context. <br>
	 * 
	 */
	abstract protected void determineSalesDocument();


	/**
	 * Determine the header from the page context. <br>
	 * The source for the header object could be different for different pages.
	 */
	abstract protected void determineHeader();


	/**
	 * Determine the items from the page context. <br>
	 * The source for the header object could be different for different pages.
	 */
	abstract protected void determineItems();


	/**
	 * Determine the document messages from the page context. <br>
	 * The source for the document messages could be different for different pages.
	 */
	abstract protected void determineMessages();


	/**
	 * Determine the number of lines, which should be displayed  for new positions . <br>
	 * The default value is 5, if not oversteered in the UI component.
	 */
	protected void determineNewPos() {
		this.newpos = getInitialNewPos();
	}


	/**
	 * Build one string from the given messagelist. <br>
	 * The message will be concatenated and separated with the {@link #messageSeparator}.
	 * 
	 * @param  messageList list with message.
	 * @return All message concatenated and separated with the {@link #messageSeparator}
	 *  
	 */
	protected String getMessageString(MessageList messageList) {
        
		String messageString = "";
		
		if (messageList != null) {
			for (int i = 0; i < messageList.size(); i++) {
				messageString += JspUtil.encodeHtml(messageList.get(i).getDescription());
				if (i < messageList.size() - 1) {
				    messageString += messageSeparator;
				}
			}
		}
        
		return messageString;
	}


	/**
	 * Method, which allows some initialization in inherit classes. <br>
	 */
	protected void initDocument() {
	}


    /**
     * Method to retrieve the DocumentHandler
     * @return DocumentHandler docHandler
     */
    protected DocumentHandler getDocumentHandler(UserSessionData userSessionData) {
        return ( (DocumentHandler) userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER) );
    }
    
    /**
     * Returns the value set in the ui component for the initial number of empty items
     * to be created in a document.
     * 
     * @return initial number of empty items
     */
    protected int getInitialNewPos() {
        
        if (initialNewpos == 0) {
            InteractionConfigContainer icc= com.sap.isa.core.FrameworkConfigManager.Servlet.getInteractionConfig(
                request.getSession());
            
            InteractionConfig ic= icc.getConfig("ui");
        
            if( ic != null) {
                
                try {
                    initialNewpos = Integer.parseInt(ic.getValue("initial.newpos"));
                } catch(Exception ex) {
                    initialNewpos = 5;
                    log.warn(LogUtil.APPS_USER_INTERFACE, "UI parameter initial.newpos not set properly, 5 taken as default");
                }
            } 
        }
        
        return initialNewpos;
    }
    
    /** 
     * Method allows to compare two strings which are supposed to contain integer values.
     * Leading zeros will not taken into account. 
     * @param strNo1 (e.g 00010)
     * @param strNo2 (e.g 010)
     * @return true since leading zeros are not relevant
     */
	static public boolean areStringNumbersTheSame(String strNo1, String strNo2) {
		boolean retVal = false;
		try {
			int no1 = Integer.parseInt(strNo1);
			int no2 = Integer.parseInt(strNo2);
			if (no1 == no2) {
				retVal = true;
			}
		} catch(NumberFormatException noEx) {
			log.debug(noEx.getMessage());
		}
		return retVal;
	}

}
