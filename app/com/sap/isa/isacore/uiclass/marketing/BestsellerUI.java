/*****************************************************************************
    Class:        BestsellerUI
    Copyright (c) 2005, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      03.01.2005
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.isacore.uiclass.marketing;

import javax.servlet.jsp.PageContext;

import com.sap.isa.businessobject.ProductList;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.isacore.action.ActionConstants;
import com.sap.isa.isacore.uiclass.ProductListUI;

/**
 * The class BestsellerUI helps to render the 
 * {@link com.sap.isa.businessobject.marketing.Bestseller}. <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class BestsellerUI extends ProductListUI {
    
    private static IsaLocation log = IsaLocation.getInstance(BestsellerUI.class.getName());

    /**
     * Standard constructor. <br>
     * 
     * @param pageContext
     */
    public BestsellerUI(PageContext pageContext) {
        initContext(pageContext);
		setUIParameterBestseller();
        setItemIndexCounter(0);
    }

	/**
	 * Standard constructor. <br>
	 */
	public BestsellerUI() {
	}

    /**
     * Overwrites the method determineProductList. <br>
     * 
     * @see com.sap.isa.isacore.uiclass.ProductListUI#determineProductList()
     */
    public void determineProductList() {
        log.entering("determineProductList");
        resetToDefCmp();
		productList = (ProductList) request.getAttribute(ActionConstants.DS_BESTSELLER);
        log.exiting();
    }

    /**
     * Returns the title for the bestseller . <br>
     * 
     * @return <code>translate("bestseller.jsp.header");</code>
     * 
     * 
     * @see com.sap.isa.isacore.uiclass.ProductListUI#getTitle()
     */
    public String getTitle() {
        log.entering("getTitle");
        log.exiting();
        return translate("bestseller.jsp.header");
    }

    /**
     * Returns a message text if no product is found. <br>
     * 
     * @return
     * 
     * 
     * @see com.sap.isa.isacore.uiclass.ProductListUI#getNotFoundMessage()
     */
    public String getNotFoundMessage() {
    	log.entering("getNotFoundMessage");
    	StringBuffer ret = new StringBuffer(translate("bestseller.jsp.notFound"));
    	
    	ret.append("<br/>").append(translate("bestseller.jsp.notFound2"));
    	log.exiting();
        return ret.toString();
    }

	/**
	 * Returns "more products found" message. <br>
	 * 
	 * @return message text if more products as displayed are found.
	 */
	public String getMoreFoundMessage() {
        log.entering("getMoreFoundMessage");
        log.exiting();
		return translate("catalogentry.jsp.moreBestseller");
	}
	  

	/**
	 * Returns "more products found" action name. <br>
	 * 
	 * @return action to display all products,
	 *  if more products as displayed are found.
	 */
	public String getMoreFoundAction() {
        log.entering("getMoreFoundAction");
        log.exiting();
		return  "bestsellerlist.do";
	}


	/**
	 * Returns the scenario for the product detail action <br>
	 * 
	 * @return scenario name;
	 */
	public String getScenario() {
        log.entering("getScenario");
        log.exiting();
		return ActionConstants.DS_BESTSELLER;
	}
	
}
