/*****************************************************************************
    Class:        RecommendationUI
    Copyright (c) 2005, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      03.01.2005
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.isacore.uiclass.marketing;

import javax.servlet.jsp.PageContext;

import com.sap.isa.businessobject.ProductList;
import com.sap.isa.catalog.actions.ActionConstants;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.isacore.uiclass.ProductListUI;

/**
 * The class RecommendationUI helps to render the
 * {@link com.sap.isa.businessobject.marketing.PersonalizedRecommendation} object. <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class RecommendationUI extends ProductListUI {

    private static IsaLocation log = IsaLocation.getInstance(RecommendationUI.class.getName());
	/**
	 * Standard constructor. <br>
	 * 
	 * @param pageContext
	 */
	public RecommendationUI(PageContext pageContext) {
		initContext(pageContext);
		setUIParameterRecommendations();
        setItemIndexCounter(0);
	}

	/**
	 * Standard constructor. <br>
	 * 
	 * @param pageContext
	 */
	public RecommendationUI() {
	}

	/**
	 * Overwrites the method determineProductList. <br>
	 * 
	 * @see com.sap.isa.isacore.uiclass.ProductListUI#determineProductList()
	 */
	public void determineProductList() {
        log.entering("determineProductList");
        log.exiting();
        resetToDefCmp();
		productList = (ProductList) request.getAttribute(ActionConstants.DS_RECOMMENDATION);
	}

	/**
	 * Returns the title for the recommendation . <br>
	 * 
	 * @return <code>translate("recommendation.jsp.header");</code>
	 * 
	 * 
	 * @see com.sap.isa.isacore.uiclass.ProductListUI#getTitle()
	 */
	public String getTitle() {
        log.entering("getTitle");
        log.exiting();
		return translate("recommendation.jsp.header");
	}

	/**
	 * Return a message text if no product is found. <br>
	 * 
	 * @return
	 * 
	 * 
	 * @see com.sap.isa.isacore.uiclass.ProductListUI#getNotFoundMessage()
	 */
	public String getNotFoundMessage() {
    	log.entering("getNotFoundMessage");
		StringBuffer ret = new StringBuffer(translate("recommendation.jsp.notFound"));
    	
		ret.append("<br/>").append(translate("recommendation.jsp.notFound2"));
    	log.exiting();
		return ret.toString();
	}

	/**
	 * Return "more products found" message. <br>
	 * 
	 * @return message text if more products as displayed are found.
	 */
	public String getMoreFoundMessage() {
        log.entering("getMoreFoundMessage");
        log.exiting();
		return translate("catalogentry.jsp.moreRecommend");
	}
	  

	/**
	 * Return "more products found" action name. <br>
	 * 
	 * @return action to display all products,
	 *  if more products as displayed are found.
	 */
	public String getMoreFoundAction() {
        log.entering("getMoreFoundAction");
        log.exiting();
		return  "recommendationlist.do";
	}

	/**
	 * Return the scenario for the product detail action <br>
	 * 
	 * @return scenario name;
	 */
	public String getScenario() {
        log.entering("getScenario");
        log.exiting();
		return ActionConstants.DS_RECOMMENDATION;
	}
    
}
