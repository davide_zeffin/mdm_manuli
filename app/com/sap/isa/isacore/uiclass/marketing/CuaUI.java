/*****************************************************************************
    Class:        CuaUI
    Copyright (c) 2005, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      03.01.2005
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.isacore.uiclass.marketing;

import javax.servlet.jsp.PageContext;

import com.sap.isa.businessobject.ProductList;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.isacore.action.marketing.ShowCUABaseAction;
import com.sap.isa.isacore.uiclass.ProductListUI;

/**
 * The class CuaUI helps to render the
 * {@link com.sap.isa.businessobject.marketing.CUAList} object. <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class CuaUI extends ProductListUI {

    static public IsaLocation log = IsaLocation.getInstance(CuaUI.class.getName());

	/**
	 * Product will be added to the document via javascript. <br>
	 */
	public static final String METHOD_JAVASCRIPT = "javascript";

	/**
	 * Product will be added to the document via struts action. <br>
	 */
	public static final String METHOD_ACTION = "action";

	/**
	 * Product will be replaced in the document via javascript. <br>
	 */
	public static final String METHOD_SIMPLE_REPLACE = "replace";

	/**
	 * Product will be replaced in the document via struts action. <br>
	 */
	public static final String METHOD_ACTION_REPLACE = "replaceAction";

	/**
	 * Reference to the display, which shoul be used. <br> 
	 */
	protected String displayType;

	/**
	 * Method to transfer the product to the document. <br>
	 */
	protected String method;

	/**
	 * Resource key for the title description. <br>
	 */
	protected String title = "cualist.jsp.title";

	/**
	 * target to use on JSP. <br>
	 */
	protected String target = "";
	
	/**
	 * Standard constructor. <br>
	 * 
	 * @param pageContext
	 */
	public CuaUI(PageContext pageContext) {
		initContext(pageContext);
	}

	/**
	 * Standard constructor. <br>
	 * 
	 * @param pageContext
	 */
	public CuaUI() {
	}

	/**
	 * Overwrites the method determineProductList. <br>
	 * 
	 * @see com.sap.isa.isacore.uiclass.ProductListUI#determineProductList()
	 */
	public void determineProductList() {
        log.entering("determineProductList");
		productList =
			(ProductList) request.getAttribute(ShowCUABaseAction.RC_CUALIST);
        log.exiting();
	}

	/**
	 * Overwrites the method setUIParameter. <br>
	 * 
	 */
	public void setUIParameter() {
	}

	/**
	 * Returns the title for the cua . <br>
	 * 
	 * @return <code>translate(title);</code>
	 * 
	 * 
	 * @see com.sap.isa.isacore.uiclass.ProductListUI#getTitle()
	 */
	public String getTitle() {
        log.entering("getTitle");
        log.exiting();
		return translate(title);
	}

	/**
	 * Set the property {@link #title}. <br>
	 * 
	 * @param title
	 */
	public void setTitle(String title) {
        log.entering("setTitle: "+title);
		this.title = title;
        log.exiting();
	}

	/**
	 * Return a message text if no product is found. <br>
	 * 
	 * @return <code>cua.jsp.notFound</code>
	 * 
	 * 
	 * @see com.sap.isa.isacore.uiclass.ProductListUI#getNotFoundMessage()
	 */
	public String getNotFoundMessage() {

		return "cua.jsp.notFound";
	}

	/**
	 * Return the property {@link #displayType}. <br>
	 * 
	 * @return {@link #displayType}
	 */
	public String getDisplayType() {
		return displayType;
	}

	/**
	 * Set the property {@link #displayType}. <br>
	 * 
	 * @param displayType {@link #displayType}
	 */
	public void setDisplayType(String displayType) {
		this.displayType = displayType;
	}

	/**
	 * Return the property {@link #method}. <br>
	 * 
	 * @return {@link #method}
	 */
	public String getMethod() {
		return method;
	}

	/**
	 * Set the property {@link #method}. <br>
	 * 
	 * @param method {@link #method}
	 */
	public void setMethod(String method) {
		this.method = method;
	}

	/**
	 * Set the {@link #METHOD_ACTION_REPLACE} or {@link #METHOD_ACTION_REPLACE}  
	 * depending if the replaced item is filled in the request parameter
	 * <code>cuareplaceditem</code>. 
	 *
	 */
	public void setReplaceActionMethod() {
		String replacedItem = request.getParameter("cuareplaceditem");

		if (replacedItem != null && replacedItem.length() > 0) {
			setMethod(CuaUI.METHOD_ACTION_REPLACE);
		} else {
			setMethod(CuaUI.METHOD_ACTION);
		}
	}

	/**
	 * Return "more products found" message. <br>
	 * 
	 * @return message text if more products as displayed are found.
	 */
	public String getMoreFoundMessage() {
		return "";
	}

	/**
	 * Return "more products found" action name. <br>
	 * 
	 * @return action to display all products,
	 *  if more products as displayed are found.
	 */
	public String getMoreFoundAction() {
		return "cua.do";
	}

	/**
	 * Return the scenario for the product detail action <br>
	 * 
	 * @return scenario name;
	 */
	public String getScenario() {
		return "";
	}

	public WebCatItem getItem() {
		log.entering("getItem()");
		WebCatItem item = (WebCatItem) request.getAttribute("currentItem");
		log.exiting();
		return item;
	}

	/**
	 * Return the property {@link #target}. <br> 
	 *
	 * @return Returns the {@link #target}.
	 */
	public String getTarget() {
		return target;
	}

	/**
	 * Return the target attribute for href. <br> 
	 *
	 * @return Returns the target attribute for href.
	 */
	public String getTargetAttribute() {
		if (target!=null && target.length() >0) {
			return "target=\"" + target + "\"";
		}
		return "";
	}

	/**
	 * Set the property {@link #target}. <br>
	 * 
	 * @param target The {@link #target} to set.
	 */
	public void setTarget(String target) {
		this.target = target;
	}

	public boolean IsItemListQuery() {
		log.entering("IsItemListQuery()");
		boolean response = false;
		WebCatItem item = getItem();
		if (item != null) {
			response = item.getItemKey().getParentCatalog().getCurrentItemList() != null
					&& item.getItemKey().getParentCatalog().getCurrentItemList().getQuery() != null;
		}
		log.exiting();

		return response;
	}

	public boolean IsItemListAreaQuery() {
		log.entering("IsItemListAreaQuery()");
		log.exiting();

		return getItem().getItemKey().getParentCatalog().getCurrentItemList().getAreaQuery() != null;
	}

}
