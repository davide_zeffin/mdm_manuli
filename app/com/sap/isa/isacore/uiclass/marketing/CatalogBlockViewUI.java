/*****************************************************************************
    Class:        CatalogBlockViewUI
    Copyright (c) 2005, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      13.02.2006
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.isacore.uiclass.marketing;

import javax.servlet.jsp.PageContext;

import com.sap.isa.businessobject.ProductList;
import com.sap.isa.businessobject.webcatalog.CatalogBusinessObjectManager;
import com.sap.isa.catalog.actions.ActionConstants;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.catalog.webcatalog.WebCatItemList;
import com.sap.isa.isacore.action.marketing.ShowRecommendationAction;
import com.sap.isa.isacore.uiclass.ProductListUI;

/**
 * The class CatalogBlockViewUI helps to render the
 * {@link com.sap.isa.businessobject.marketing.CatalogArea} object. <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class CatalogBlockViewUI extends ProductListUI {

	/**
	 * Standard constructor. <br>
	 * 
	 * @param pageContext
	 */
	public CatalogBlockViewUI(PageContext pageContext) {
		initContext(pageContext);
	}

	/**
	 * Standard constructor. <br>
	 * 
	 * @param pageContext, 
     * @param isCatQuery, flag if catalog query
	 */
	public CatalogBlockViewUI(PageContext pageContext, boolean isCatQuery) {
		initContext(pageContext);
		setUIParameter(isCatQuery);
	}

	/**
	 * Standard constructor. <br>
	 * 
	 * @param pageContext
	 */
	public CatalogBlockViewUI() {
	}

	/**
	 * Overwrites the method determineProductList. <br>
	 * 
	 * @see com.sap.isa.isacore.uiclass.ProductListUI#determineProductList()
	 */

	public void determineProductList() {
        determineProductListFromItemPage();
	}
    
    public void determineProductListFromItemPage() {
		String scenario = getDetailScenario();
		
        if (userSessionData != null) {
            CatalogBusinessObjectManager bom =
                                      (CatalogBusinessObjectManager)userSessionData.getBOM(CatalogBusinessObjectManager.CATALOG_BOM);
            
            //cuaList?
            if(ActionConstants.DS_CUA.equals(scenario)){
	            if (bom.getCatalog()!= null && bom.getCatalog().getCurrentCUAList() != null){
	            	//no current page?
	            	if(bom.getCatalog().getCurrentCUAList().getCurrentItemPage() == null) {
	            		//call to init
	            		bom.getCatalog().getCurrentCUAList().getItemPage(1, WebCatItemList.CV_LISTTYPE_CUA);
	            	}
	                //resetToDefCmp();
	                productList = new ProductList(bom.getCatalog().getCurrentCUAList().getCurrentItemPage().getItemsAsWebCatItemList());
	            }
            }else{
                if (bom.getCatalog()!= null && bom.getCatalog().getCurrentItemList() != null && 
                        bom.getCatalog().getCurrentItemList().getCurrentItemPage() != null) {
                        resetToDefCmp();
                        productList = new ProductList(bom.getCatalog().getCurrentItemList().getCurrentItemPage().getItemsAsWebCatItemList());
                    }

            }
        }
    }

	/**
	 * set the UI parameter depending on the scenario. <br>
	 * 
     * @param isCatQuery
	 */
	public void setUIParameter(boolean isCatQuery) { 
		String scenario = getDetailScenario();

		if (scenario.equals(ActionConstants.DS_BESTSELLER)) {
			setUIParameterBestseller();
		} else {	 
		        if (scenario.equals(ActionConstants.DS_RECOMMENDATION)) {
					setUIParameterRecommendations();
				} else { 
                    determineProductListFromItemPage();
					if (isCatQuery) {
						setUIParameterCatSearch();
					} else {
 					    setUIParameterCatArea();
					}
		        }
		}
        
        if (userSessionData != null) {
            CatalogBusinessObjectManager bom =
                                      (CatalogBusinessObjectManager)userSessionData.getBOM(CatalogBusinessObjectManager.CATALOG_BOM);
            if (bom.getCatalog()!= null && bom.getCatalog().getCurrentItemList() != null) {
                setPageSize(bom.getCatalog().getPageSize(bom.getCatalog().getCurrentItemList().getListType()));
            }
        }
        
        // compare is not allowed if it's no query and 
        // the area is an buy points area
        this.setIsCompareAllowed(isCatQuery || !this.isBuyPointsArea());
	}
	
	/**
	 * Returns the title for the CatalogArea . <br>
	 * 
	 * @return <code>translate("CatalogArea.jsp.header");</code>
	 * 
	 * 
	 * @see com.sap.isa.isacore.uiclass.ProductListUI#getTitle()
	 */
	
	public String getTitle() {
		return translate("CatalogBlockView.jsp.header");
	}

	/**
	 * Return a message text if no product is found. <br>
	 * 
	 * @return
	 * 
	 * 
	 * @see com.sap.isa.isacore.uiclass.ProductListUI#getNotFoundMessage()
	 */
	public String getNotFoundMessage() {
    	
		StringBuffer ret = new StringBuffer(translate("CatalogBlockView.jsp.notFound"));
    	
		ret.append("<br/>").append(translate("CatalogBlockView.jsp.notFound2"));
    	
		return ret.toString();
	}

	/**
	 * Return "more products found" message. <br>
	 * 
	 * @return message text if more products as displayed are found.
	 */
	public String getMoreFoundMessage() {
		return translate("catalogentry.jsp.blockview");
	}
	  

	/**
	 * Return "more products found" action name. <br>
	 * 
	 * @return action to display all products,
	 *  if more products as displayed are found.
	 */
	public String getMoreFoundAction() {
		return  "CatalogProductListlist.do";
	}

	/**
	 * Return the scenario for the product detail action <br>
	 * 
	 * @return scenario name;
	 */
	public String getScenario() {
		return  "CatalogProductList";
	}
    
    /**
     * Returns the amount of selected items in the itemlist which are not displayed on the current page.
     * The method stops checking the itemlist if atleast 2 items are selected. 
     *
     * @param  pageNo as int, number of the current page
     * @return counter as int, 0 if no item is selected
     *                         1 if one item is selected
     *                         2 if atleast 2 items are selected
     */
    public int getAmountOfSelectedItems(int pageNo) {
        final String METHOD = "getAmountOfSelectedItems()";
        log.entering(METHOD);
        int counter = 0;

        if (userSessionData == null) {
            log.exiting();
            return 0;
        }
        
        CatalogBusinessObjectManager bom =
                       (CatalogBusinessObjectManager)userSessionData.getBOM(CatalogBusinessObjectManager.CATALOG_BOM);
                       
        if (bom.getCatalog()!= null && bom.getCatalog().getCurrentItemList() != null) {
            
            WebCatItemList itemList = bom.getCatalog().getCurrentItemList();

            int firstRow = (pageNo - 1) * pageSize() + 1;
            int lastRow = (pageNo - 1) * pageSize() + productList.size();
            int counter2 = 0;
            WebCatItem item;

            // check pages before the current page         
            for (int i = 0; i < firstRow-1 && counter < 2; i++) {
                if (itemList.getItem(i).isSelected()) {
                    counter++;
                }
            }
            if (log.isDebugEnabled()) {
                int j = firstRow - 1;
                counter2 = counter;
                log.debug(METHOD + ": check item number 0 to " + j +
                          " / selected items = " + counter);
            }

            // check pages after the current page         
            for (int i = lastRow; i < itemList.size() && counter < 2; i++) {
                if (itemList.getItem(i).isSelected()) {
                    counter++;
                }
            }
            if (log.isDebugEnabled()) {
                int j1 = lastRow+1;
                int j2 = itemList.size()-1;
                counter2 -= counter;
                log.debug(METHOD + ": check item number " + j1 +
                          " to " + j2 +
                          " / selected items = " + counter2 +
                          " / total selected items (max 2) = " + counter);
            }
        }
        log.exiting();
        return counter;
    }

}
