/*****************************************************************************
    Class:        OrderStatusBaseUI
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      01.03.2004
    Version:      1.0

    $Revision: #18 $
    $Date: 2004/08/10 $ 
*****************************************************************************/

package com.sap.isa.isacore.uiclass;


import javax.servlet.ServletContext;
import javax.servlet.jsp.PageContext;

import com.sap.isa.businessobject.ConnectedDocument;
import com.sap.isa.businessobject.ConnectedDocumentItem;
import com.sap.isa.businessobject.SalesDocumentBase;
import com.sap.isa.businessobject.SalesDocumentStatus;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.businessobject.order.ExtRefObjectList;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.isacore.ManagedDocumentLargeDoc;
import com.sap.isa.isacore.action.ActionConstantsBase;
import com.sap.isa.portalurlgenerator.util.AnchorAttributes;


/**
 * This UI class is used on order status detail page. <br>
 * Because the class is abstract the GenericFactory will provide an implementation
 * on the JSP. <br> 
 * The class is introduced for a reuse of the page in <strong>Internet Sales</strong>
 * and <strong>Claims and Returns </strong> scenario, which only use different implementation
 * for this abstract class. <br>.
 *
 * @author  SAP AG
 * @version 1.0
 */
abstract public class SalesDocumentStatusBaseUI extends SalesDocumentDataUI {

    protected SalesDocumentStatus salesDocStatus = null;
    protected SalesDocumentBase salesDocDataContainer = null;
    protected int noOfProcessTypes = 0;
    protected String processTypeToOrder = "";
    public boolean showIPCpricing = false;
    public Table navButtons;
    public Table jScriptFileNames;
    public int noOfOriginalItems = 0;
    protected ResultData sucProcTypeAllowedList = null;
    public boolean showConfirmedDeliveryDate = false;
    public boolean quotationExtended = false;
    public boolean showInternalCatalog = false;
    public boolean backendR3 = false;
    public int accNumOfItems = 0;
    public boolean existingProcess = false; 
	static final protected IsaLocation log = IsaLocation.getInstance(SalesDocumentStatusBaseUI.class.getName());
	/**
	 * Item which should be presented only. 
	 */
	protected String itemNoToShow = null;
	
    /**
     * Constructor for SalesDocumentStatusBaseUI. <br>
     * 
     * @param pageContext
     */
    public SalesDocumentStatusBaseUI(PageContext pageContext) {
        initContext(pageContext);
    }
    
    /**
     * Constructor for SalesDocumentStatusBaseUI. <br>
     *
     */
    public SalesDocumentStatusBaseUI() {

    }

    /**
     * Initialize the the object from the page context. <br>
     * 
     * @param pageContext page context
     */
    public void initContext(PageContext pageContext) {
		final String METHOD_NAME = "initContext()";
		log.entering(METHOD_NAME);
        super.initContext(pageContext);
                
        showIPCpricing = salesDocConfiguration.isPricingCondsAvailable();
        if (salesDocStatus != null) {
            isLargeDocument = salesDocStatus.isLargeDocument(salesDocConfiguration.getLargeDocNoOfItemsThreshold());
            noOfOriginalItems = salesDocStatus.getNoOfOriginalItems();
            // Set ShipTo to Session
            userSessionData.setAttribute(ActionConstantsBase.SC_SHIPTOS, salesDocStatus.getShipTos());        
            this.salesDocDataContainer = salesDocStatus.getSalesDocumentDataContainer();
        }      
        // Initialize Application specific JScriptfiles and Buttons        
        jScriptFileNames = createJScriptFileNameTable();
        fillJScriptFileNameTable(jScriptFileNames);
        
        navButtons = createNavigationButtonTable();
        fillNavigationButtonTable(navButtons);
        log.exiting();
        
		if (mdoc instanceof ManagedDocumentLargeDoc                                                                         &&
		    salesDocStatus != null                                                                                          &&
		    salesDocStatus.getItemList().size() > 1                                                                         &&
		    ! isLargeDocument                                                                                               &&
		    ManagedDocumentLargeDoc.ITEM_PROPERTY_NUMBERINT.equals(((ManagedDocumentLargeDoc)mdoc).getItemSearchProperty()) &&
			((ManagedDocumentLargeDoc)mdoc).getItemSearchPropertyLowValue() != null                                         &&
			((ManagedDocumentLargeDoc)mdoc).getItemSearchPropertyLowValue().length() > 0) {
			itemNoToShow = ((ManagedDocumentLargeDoc)mdoc).getItemSearchPropertyLowValue();
		}

    }

    /**
     * Returns true if there is no item in the table of the selected items.
     * (Used for reading large documents).
     */
    public boolean isSelectedItemGuidTableEmpty() {
        return (salesDocStatus.getSelectedItemGuids().isEmpty());
    }
    /**
     * Only dummy because there is no salesDocument involved . <br>
     * 
     * @see com.sap.isa.isacore.uiclass.SalesDocumentBaseUI#determineSalesDocument()
     */
    protected void determineSalesDocument() {
    }

    /**
     * Determine the header from the salesDocStatus which is provided by the DocumentHandler (getDocumentOnTop())
     * @see com.sap.isa.isacore.uiclass.SalesDocumentBaseUI#determineHeader()
     */
    protected void determineHeader() {
        this.header = salesDocStatus.getOrderHeader();
    }

    /**
     * Determine the item list from the page context.
     * The item list is taken from salesDocStatus which is provided by the DocumentHandler (getDocumentOnTop()) 
     * @see com.sap.isa.isacore.uiclass.SalesDocumentBaseUI#determineItems()
     */
    protected void determineItems() {
        this.items = salesDocStatus.getItemList();
        this.accNumOfItems = items.size();
    }

    /**
     * Determine the message list from the page context.
     * The message list is taken salesDocStatus which is provided by the DocumentHandler (getDocumentOnTop())
     * @see com.sap.isa.isacore.uiclass.SalesDocumentBaseUI#determineMessages()
     */
    protected void determineMessages() {
        //salesDocumentStatus os = (SalesDocumentStatus)request.getAttribute(ActionConstantsBase.RC_SALES_STATUS_DETAIL);
        this.docMessages = salesDocStatus.getMessageList();
    }
    
    /**
     * Init Document
     */
    protected void initDocument() {
        //salesDocStatus = (SalesDocumentStatus)request.getAttribute(ActionConstantsBase.RC_SALES_STATUS_DETAIL);
        salesDocStatus = (SalesDocumentStatus)mdoc.getDocument();
    }
    /**
     * Create a table containing following columns:<br>
     * - ALIGN          (Type String) Alignment of the button LEFT or RIGHT<br>
     * - NAME           (Type String) Name of the Button (translation key)<br>
     * - ID             (Type String) HTML element ID (to be used by jScript)<br>
     * - DISABLED       (Type String) Set HTML element to be disabled (true / false (default))<br>
     * - TITLE          (Type String) Title of the Button (translation key)<br>
     * - HREF           (Type String)<br>
     * - ONCLICK        (Type String)<br>
     * - CSSCLASS       (Type String)<br>
     * - LINKED_ELEMENT (Type String)<br>
     */
    protected Table createNavigationButtonTable() {
        Table tab = new Table("NavigationButtons");
        tab.addColumn(Table.TYPE_STRING, "ALIGN");
        tab.addColumn(Table.TYPE_STRING, "NAME");
        tab.addColumn(Table.TYPE_STRING, "ID");
        tab.addColumn(Table.TYPE_STRING, "DISABLED");
        tab.addColumn(Table.TYPE_STRING, "TITLE");
        tab.addColumn(Table.TYPE_STRING, "HREF");
        tab.addColumn(Table.TYPE_STRING, "ONCLICK");
        tab.addColumn(Table.TYPE_STRING, "CSSCLASS");
        tab.addColumn(Table.TYPE_STRING, "LINKED_ELEMENT");
        return tab;
    }
    
    /**
     * Create a table containing following columns:<br>
     * - FILENAME (Type String)
     */
    protected Table createJScriptFileNameTable() {
        Table tab = new Table("JScriptFileNames");
        tab.addColumn(Table.TYPE_STRING, "FILENAME");
        return tab;
    }
    
    /**
     * Get number of found Items from the ManagedDocumentLargeDoc
     */
    public int getNumberOfItemsFound() {
        int numberOfItemsFound = 0;
        if (mdoc != null  &&  mdoc instanceof ManagedDocumentLargeDoc) {
            numberOfItemsFound = ((ManagedDocumentLargeDoc)mdoc).getNumberofItemsFound();
        }
        return numberOfItemsFound;
    }
    
    /**
     * Returns the correct value for the Item, wich is NetValueWOfreight 
     * 
     * @return the correct value for the item
     */
    public String getItemValue() {

        return item.getNetValueWOFreight();
    } 
    
    /**
     * Returns the correct value for the header, wich is NetValueWOfreight 
     * 
     * @return the correct value for the header
     */
    public String getHeaderValue() {

        return header.getNetValueWOFreight();
    } 
    
	/**
	 * Check if there is a header text.
	 * 
	 * @return true, if there is a header text
	 *         false, otherwise
	 */
	public boolean hasHeaderText() {
		return header.hasText();
	}
    
    /**
     * Set the items into the HTTP request to be iterated.
     */
    public void setItemsToIterate() {
        request.setAttribute(ActionConstantsBase.RK_SALES_DOC_STATUS_DETAIL_ITEMITERATOR, items);
    }
    
    /**
     * Abstract method to fill Table NavigationButton
     */
    abstract protected void fillNavigationButtonTable(Table navButtons);
    
    /**
     * Abstract method to fill Table JScriptFilenames
     */
    abstract protected void fillJScriptFileNameTable(Table tab);
    
    /**
     * Return <code>true</code> if order download is required
     */
    abstract public boolean isOrderDownloadRequired();    
    
    /**
     * @return Returns true if the hosted order management is activated.
     */
    abstract public boolean isHomActivated();
    
    /**
     * @return Returns true if the soldTo information should be visible
     * @see com.sap.isa.isacore.uiclass.order.OrderDataUI#isSoldVisible()
     */
    abstract public boolean isSoldToVisible();
    
    /** 
     * Return the list of allowed process types when ordering a quotation
     */
    abstract public ResultData getSucProcTypeAllowedList();
    
    /**
     * Get threshold for large Documents as String
     */
    public String getLargeDocThreshold() {
        return String.valueOf(salesDocConfiguration.getLargeDocNoOfItemsThreshold());
    }

    /**
     * Returns the header campaign description and campaign type description for the given index
     * 
     * @param intger idx the idx of the header campaign to look for
     * @return String returns the header campaign description and campaign type description for the given index
     */
    public String getHeaderCampaignDescs(int idx) {
        String descs = "";
        
        if (header != null && header.getAssignedCampaigns() != null && 
            header.getAssignedCampaigns().getCampaign(idx) != null) {
            String campGuid = header.getAssignedCampaigns().getCampaign(idx).getCampaignGUID().getIdAsString();
            String campTypeId = header.getAssignedCampaigns().getCampaign(idx).getCampaignTypeId();
            descs = (String) salesDocStatus.getCampaignDescriptions().get(campGuid);
            if (salesDocStatus.getCampaignTypeDescriptions().get(campTypeId) != null &&
                salesDocStatus.getCampaignTypeDescriptions().get(campTypeId).toString().length() > 0) {
            	descs = descs + " (" + salesDocStatus.getCampaignTypeDescriptions().get(campTypeId).toString() + ")";
            }
        }
        
        return descs;
    }
    
    /**
     * Returns the items campaign description and campaign type description for the given index
     * 
     * @param intger idx the idx of the items campaign to look for
     * @return String returns the items campaign description and campaign type description for the given index
     */
    public String getItemCampaignDescs(int idx) {
        String descs = "";
        
        if (item != null && item.getAssignedCampaigns() != null && 
            item.getAssignedCampaigns().getCampaign(idx) != null) {
            String campGuid = item.getAssignedCampaigns().getCampaign(idx).getCampaignGUID().getIdAsString();
            String campTypeId = item.getAssignedCampaigns().getCampaign(idx).getCampaignTypeId();
            descs = (String) salesDocStatus.getCampaignDescriptions().get(campGuid);
			if (salesDocStatus.getCampaignTypeDescriptions().get(campTypeId) != null &&
                salesDocStatus.getCampaignTypeDescriptions().get(campTypeId).toString().length() > 0) {
				descs = descs + " (" + salesDocStatus.getCampaignTypeDescriptions().get(campTypeId).toString() + ")";
			}            
        }
        
        return descs;
    }
    
    /**
     * Returns the search criteria for the item status, in case of large documents
     * 
     * @return String returns the search criteria for the item status, empty string otherwise
     */
    public String getSearchItemStatus() {
        String stat = "";
        
        if (isLargeDocument && mdoc != null) {
            stat = ((ManagedDocumentLargeDoc) mdoc).getItemSearchStatus();
        }
        
        return stat;
    }
    
    /**
     * Returns the search criteria for the item property, in case of large documents
     * 
     * @return String returns the search criteria for the item property, empty string otherwise
     */
    public String getSearchItemProperty() {
        String prop = "";
        
        if (isLargeDocument && mdoc != null) {
            prop = ((ManagedDocumentLargeDoc) mdoc).getItemSearchProperty();
        }
        
        return prop;
    }
    
    /**
     * Returns the search criteria for the item property low value, in case of large documents
     * 
     * @return String returns the search criteria for the item property low value, empty string otherwise
     */
    public String getSearchItemPropertyLowValue() {
        String propVal = "";
        
        if (isLargeDocument && mdoc != null) {
            propVal = ((ManagedDocumentLargeDoc) mdoc).getItemSearchPropertyLowValue();
        }
        
        return propVal;
    }
    
    /**
     * Returns the search criteria for the item property high value, in case of large documents
     * 
     * @return String returns the search criteria for the item property high value, empty string otherwise
     */
    public String getSearchItemPropertyHighValue() {
        String propVal = "";
        
        if (isLargeDocument && mdoc != null) {
            propVal = ((ManagedDocumentLargeDoc) mdoc).getItemSearchPropertyHighValue();
        }
        
        return propVal;
    }
    
    /**
     * Returns the remaining quantity to deliver depending on item type and status.
     */
    public String getItemQuantityToDeliver() {
        String retVal = "&nbsp;";
        if ( ! header.isDocumentTypeQuotation() ) {
            if ( ! item.isBusinessObjectTypeService()) {   // Service positions does not have a remaining qty yet! 
                if ( ! item.isStatusCompleted()) {
                    retVal =  item.getQuantityToDeliver();
                } else {
                    retVal = "--";
                }
            } else {
                retVal = WebUtil.translate(pageContext, "status.sales.no.remain.qty1", null) 
                         + "<br />" 
                         + WebUtil.translate(pageContext, "status.sales.no.remain.qty2", null);
            }
        }
        return retVal;
    }
    
    /**
     * Method returns the AnchorAttribute object which contains the information to do Portal 
     * navigation. The navigation will point to Internet Sales web application (separate billing
     * part).
     * @return AnchorAttribute 
     */
    abstract public AnchorAttributes getPortalAnchorAttributesForBillingDocDisplay();
    
    /**
     * Write out JScript onClick event depending on the existens of a referenced doc.
     * @return String like 'onclick="documentDisplay('dockey', 'docno', 'doctype');"
     */
    abstract public String writeHeaderRefOnClickEvent(ConnectedDocument conDoc);

	/**
	 * Write out JScript onClick event depending on the existens of a referenced doc.
	 * @return String like 'onclick="documentDisplay('dockey', 'docno', 'doctype');"
	 */
	abstract public String writeItemRefOnClickEvent(ConnectedDocumentItem conDocItem);   
	 
	/**
	 * Returns the external reference objects as string separated by ';' 
	*/	
	public String getExtRefObjListAsString(ExtRefObjectList extRefObjects) {
		String objects = "";
		if ( extRefObjects != null && extRefObjects.size() > 0) {
			for (int j=0; j < extRefObjects.size(); j++) {
				objects = objects + extRefObjects.getExtRefObject(j) + ";";
			}
		}
		return objects;	
	}
    /**
     * Returns the threshold set in the salesDocConfiguration (e.g. shop) to define a large document.
     * @return int number of items
     */
    public int getShopLargeDocNoOfItemsThreshold() {
        return salesDocConfiguration.getLargeDocNoOfItemsThreshold();
    }

	/**
	 * Use this method to check if the item should be shown on the web page
	 * @param itemNo
	 */
	public boolean isItemNoToShow(String itemNo) {
		boolean retVal = true;
		if (itemNoToShow == null) {
			// No specific item should be show, so show all (=> true for all)
			retVal = true;
		} else {
			retVal = areStringNumbersTheSame(itemNo, itemNoToShow);
		}
		return retVal;
	}
    
	/**
	 * Use this method to get the item which should be shown on the web page
	 * @param itemNo or null if not set
	 */
	public String getItemNoToShow() {
		return this.itemNoToShow;
	}

    // START INSERTION 967900
    /**
     * Check for the availability of the freight value (shipping costs).
     * @return true if available
     */
    public boolean isFreightValueAvailable() {
        boolean retVal = false;
        // Has the backend filled this price element at all? For example an JavaBasket based order template
        // without IPC pricing will not deliver this price element
        if (salesDocDataContainer != null && salesDocDataContainer.isFreightValueAvailable()) {
            if (backendR3) {
                retVal = true;;
            } else  {
                if (! header.isChangeable()  &&  
                     (header.getFreightValue() == null  ||  header.getFreightValue().equals(header.getNetValueWOFreight())) ) {
                     // In case of R/3 created documents (the check for that, doc not changeable, freight and netVal WO freigth are 0,00 
                     // might not be the best but easiest way), no freight and net value without freight are available !!!!
                    retVal = false;
                } else {
                    retVal = true;;
                }
            }
        } else {
            retVal = false;  // price element is not supported
        }
        return retVal;
    }
    //  END INSERTION 967900
    
    /**
     * Returns corresponding property from the sales document container.<br>
     * 
     * @return
     */
    public boolean isGrossValueAvailable() {
        return salesDocDataContainer!=null?salesDocDataContainer.isGrossValueAvailable():false;
    }

    /**
     * Returns corresponding property from the sales document container.<br>
     * 
     * @return
     */
    public boolean isNetValueAvailable() {
        return salesDocDataContainer!=null?salesDocDataContainer.isNetValueAvailable():false;
    }

    /**
     * Returns corresponding property from the sales document container.<br>
     * 
     * @return
     */
    public boolean isTaxValueAvailable() {
        return salesDocDataContainer!=null?salesDocDataContainer.isTaxValueAvailable():false;
    }

    
    /**
     * only display checkboxes for external products in cr_b2b
     */
    public boolean displayCheckboxes(ItemSalesDoc item){
    	boolean response = true;
    	
    	if(item.getProductId().toString().equals("00000000000000000000000000000000")) {
		   ServletContext sc = (ServletContext)pageContext.getAttribute("javax.servlet.jsp.jspAppContext");
		   String applicationName = sc.getInitParameter("application.name");
		   if (!applicationName.equals("cr_b2b")){
			 response = false;
           }
		}		
		return response;
    }
	
}