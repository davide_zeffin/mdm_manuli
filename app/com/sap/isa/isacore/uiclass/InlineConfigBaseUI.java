/*
 * Created on 15.03.2006
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.isacore.uiclass;

import javax.servlet.jsp.PageContext;

import com.sap.isa.core.ContextConst;
import com.sap.isa.core.interaction.InteractionConfig;
import com.sap.isa.core.interaction.InteractionConfigContainer;
import com.sap.isa.ui.uiclass.BaseUI;

/**
 * @author d046485
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public abstract class InlineConfigBaseUI extends BaseUI {

	public InlineConfigBaseUI(PageContext pageContext) {
		super(pageContext);
	}

	public InlineConfigBaseUI() {
	}

	/** 
	 * Helper object to enable displaying of configration as inline string
	 */
	protected ItemConfigurationInfoHelper itemConfigInfoHelper =
		new ItemConfigurationInfoHelper();

	/**
	 * Returns the ItemConfigurationInfoHelper object, used to generate 
	 * strings for inline configuration display
	 * 
	 * @return ItemConfigurationInfoHelper the helper object
	 */
	public ItemConfigurationInfoHelper getItemConfigInfoHelper() {
		return itemConfigInfoHelper;
	}

	/**
	 * Determines from XCM the application view on the product configuration, 
	 * which should be used for display in the item list of the order screen. 
	 * 
	 * 
	 * @return The view represented by a single character
	 *
	 */
	public char getOrderView() {
        return getConfigView(ContextConst.IC_ITEM_CONFIG_ORDER_VIEW);
	}

	/**
	 * Determines from XCM the application view on the product configuration, 
	 * which should be used for display in the item list of the order
	 *  confirmation screen (detailed view). 
	 * 
	 * @return The view represented by a single character
	 *
	 */
	public char getOrderDetailView() {
        return getConfigView(ContextConst.IC_ITEM_CONFIG_ORDER_DETAIL_VIEW);
    }

	/**
	 * Determines from XCM the application view on the product configuration, 
	 * which should be used for display in the item list of the catalog screen. 
	 * 
	 * 
	 * @return The view represented by a single character
	 *
	 */
	public char getCatalogConfigView() {
        return getConfigView(ContextConst.IC_ITEM_CONFIG_CATALOG_VIEW);
	}

    /**
     * gets the char value defined for the view identified by viewName
     * calls from getOrderView, getOrderDetailView...
     * 
     * @param viewName
     * @return
     */
    private char getConfigView(String viewName) {
        log.debug("getConfigView: viewName:" + viewName);
        char view = ' ';
        InteractionConfigContainer icc = getInteractionConfig();
        InteractionConfig confview = icc.getConfig("ui");
        String iccView = confview.getValue(viewName);
        if (iccView != null && iccView.length() > 0) {
           view = iccView.charAt(0);
        }
        log.debug("getConfigView: viewValue:" + view);
        return view; 
    }

}
