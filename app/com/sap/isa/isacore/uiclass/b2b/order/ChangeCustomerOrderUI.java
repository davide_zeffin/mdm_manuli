package com.sap.isa.isacore.uiclass.b2b.order;


import java.util.Iterator;

import javax.servlet.jsp.PageContext;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.ExtendedStatusListEntry;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.ShipTo;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.TechKey;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ShipToSelection;
import com.sap.isa.isacore.action.order.MaintainOrderBaseAction;

/**
 * The class ProcessCustomerOrderUI.java .... .
 *
 * @author SAP
 * @version 1.0
 **/
public class ChangeCustomerOrderUI extends CustomerOrderUI {

    public TechKey shipToPartnerKey;

	/**
	 * Flag, if the status of a position is open. <br>
	 */
	public boolean statusOpen = false;


    /**
     * Constructor for ProcessCustomerOrderUI.
     * @param pageContext
     */
    public ChangeCustomerOrderUI(PageContext pageContext) {
        
        super(pageContext);
        
		determineKeyValues();
        
        DocumentHandler documentHandler = (DocumentHandler)
                                          userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);

        SalesDocument salesDocument = (SalesDocument) documentHandler.getDocumentOnTop();

        
        TechKey soldFromKey = header.getPartnerList().getSoldFrom().getPartnerTechKey();
        BusinessPartner soldFromPartner = bom.createBUPAManager().getBusinessPartner(soldFromKey);
        
        // add shipto to selection
        shipToSelection.setSalesPartner(soldFromPartner);
		determineSoldToInformations();


        // add sales partner as ship to if neccesary
        if (!shipToSelection.isPartnerAvailable()) {
            try {
                
                ShipToSelection.addPartnerToShipTos(salesDocument, soldFromPartner);
                userSessionData.setAttribute(
                                MaintainOrderBaseAction.SC_SHIPTOS, shipToSelection.getShipTos());
                                
            }
            catch (CommunicationException e) {
                shipToPartnerKey = TechKey.EMPTY_KEY;
                return;
            }
        }
        shipToPartnerKey = shipToSelection.getPartnerShipTo().getTechKey();                                

    }
    

	/**
     * The Method check, if the given shipTo is the ship to address of the
     * soldfrom.
     * 
     * @param shipTo
     * @return short address or fi text for collection
     */
    public String getShipToDescription(ShipTo shipTo) {
	    
	    if (shipTo.getTechKey().equals(shipToPartnerKey)) {
            return translate("hom.jsp.CollectionFromVend");
	    
	    } else {
            return shipTo.getShortAddress();
	    }
	    
	}
     

    /**
     * @see com.sap.isa.isacore.uiclass.b2b.order.OrderUI#determineNewPos()
     */
    protected void determineNewPos() {
        
        //default value
        newpos = 0;

        if ((userSessionData.getAttribute("newpos") != null) &&
            (((String)userSessionData.getAttribute("newpos")).length() > 0 )){
          newpos = Integer.parseInt(((String)userSessionData.getAttribute("newpos")));
          userSessionData.removeAttribute("newpos");
        }
    }


	/**
	 * Determines the keys for the send, confirm and cancel buttons
	 */
	protected void determineKeyValues() {
		
		confirmKey = "b2b.order.change.confirmtext";
		cancelKey = "b2b.order.change.cancel";

		alertBasketEmpty = "b2b.ordr.alr." + docType + ".empty";

		sendKey = "b2b.order.change.confirmtext";
	}


    /**
     * Overwrites the method setItem. <br>
     * Additional the status open will be set.
     * 
     * @param item order item
     * 
     * @see com.sap.isa.isacore.uiclass.SalesDocumentDataUI#setItem(com.sap.isa.businessobject.item.ItemSalesDoc)
     */
    public void setItem(ItemSalesDoc item) {
        super.setItem(item);
	
		Iterator extStatus = (Iterator)item.getExtendedStatus().getStatusIterator();

        while (extStatus.hasNext()) {
            ExtendedStatusListEntry eStatus =
                (ExtendedStatusListEntry) extStatus.next();

            if (eStatus.isCurrentStatus()) {
                if (eStatus.getTechKey().getIdAsString().equals("E0001")) {
                    statusOpen = true;
                }
                else {
                    statusOpen = false;
                }
            }
        }

    }

}
