/*****************************************************************************
    Class:        SimulationUI
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      20.07.2004
    Version:      1.0

    $Revision: #1$
    $Date: 2004/07/20 $ 
*****************************************************************************/
package com.sap.isa.isacore.uiclass.b2b.order;

import javax.servlet.jsp.PageContext;

import com.sap.isa.core.logging.IsaLocation;

/**
 * The class SimulationUI . <br>
 *
 * @author  SAP AG
 * @version 1.0
 *
 */
public class SimulationUI extends SalesDocumentUI {
    
    protected String headKey1 = "b2b.order.simulate.order";
    protected String headKey2 = "b2b.order.simulate.description";
    protected String sendKey  = "b2b.order.simulate.button.send";
    protected String accSendKey  = "b2b.order.sim.button.acc.send";
    protected String confirmKey = "b2b.ord.conf.order.plain";
    public boolean showIPCpricing = false;
	static final private IsaLocation log = IsaLocation.getInstance(SimulationUI.class.getName());
	
    /**
     * @param pageContext
     */
    public SimulationUI(PageContext pageContext) {
        
        super(pageContext);
        
        determineKeyValues();
    }
    
    /**
     * Initialize the the object from the page context. <br>
     * 
     * @param pageContext page context
     */
    public void initContext(PageContext pageContext) {
		final String METHOD_NAME = "initContext()";
		log.entering(METHOD_NAME);
        super.initContext(pageContext);
                
        showIPCpricing = salesDocConfiguration.isPricingCondsAvailable();
   		log.exiting();
    }
    
    /**
     * Determines the keys
     */
    protected void determineKeyValues() {
 
        if (isLateDecision) {
           if (bom.getOrder() != null) {
               headKey1 = "b2b.order.simulate.order.b2r";
               headKey2 = "b2b.order.simul.order.desc.b2r";
           } 
           else if (bom.getQuotation() != null)  {
               headKey1 = "b2b.order.simulate.quotation.b2r";
               headKey2 = "b2b.order.simul.quot.desc.b2r";
               sendKey = "b2b.order.simul.button.sendquot";
               accSendKey = "b2b.order.sim.bttn.acc.sendquot";
               confirmKey = "b2b.ord.conf.quotation.plain";
           } 
           else {
               headKey1 = "b2b.order.simul.templ.b2r";
               headKey2 = "b2b.ord.simul.tmpl.dsc.b2r";
               sendKey = "b2b.order.simul.button.sendquot";
               accSendKey = "b2b.order.sim.bttn.acc.sendquot";
               confirmKey = "b2b.ord.conf.ordertemplate.plain";
           }
        }
    }
    
    /**
     * Return the header description fields. <br>
     * 
     * The header description consist of the document type and 
     * the process type description, if this is avaiable in
     * {@link #processTypeDescription} property. <br> 
     * 
     * <em>The method returns the translate string and no resource key.</em><br>
     * 
     * @return String with the header description.
     */
    public String getHeaderDescription() {
        
        if (processTypeDescription != null && processTypeDescription.length() >0) {
            return translate(headKey1) + " (" + processTypeDescription + ")";
        }
        else {
            return translate(headKey1);     
        }
    }
    
    /**
     * Return the header description fields for the second line. <br>
     * 
     * <em>The method returns the translate string and no resource key.</em><br>
     * 
     * @return String with the header description for the second line.
     */
    public String getHeaderDescription2() {
        
        return translate(headKey2);     
    }
    
    /**
     * Returns the Colspan value for the items table<br>
     *  
     * @return the Colspan value for the items table
     */
    public String getItemsColspan() {
        String subinfos_colspan = "8";
        if (isContractInfoAvailable()) {
            subinfos_colspan="9";
        }
        if (header.isDocumentTypeOrderTemplate() || header.isDocumentTypeQuotation()) {
            subinfos_colspan="6";
        }
        
        return subinfos_colspan;
    }
    
    /**
     * Returns the sendKey value<br>
     *  
     * @return the sendKey value
     */
    public String getSendKey() {
        return sendKey;
    }
    
    /**
     * Returns the accesibility sendKey<br>
     *  
     * @return the accesibility sendKey value
     */
    public String getAccSendKey() {
        return accSendKey;
    } 
    
    /**
     * Returns the sendKey value<br>
     *  
     * @return the sendKey value
     */
    public String getConfirmKey() {
        return confirmKey;
    }
    
}
