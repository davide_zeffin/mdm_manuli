/*
 * Created on 20.07.2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.isacore.uiclass.b2b.order;

import java.util.ArrayList;

import com.sap.isa.businessobject.BatchCharList;
import com.sap.isa.businessobject.BatchCharVals;
import com.sap.isa.businessobject.ProductBatch;
import com.sap.isa.businessobject.item.ItemSalesDoc;

/**
 * @author d031177
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class BatchUIInfoContainer {
	
	boolean orderChange = false;
	
	private int noOfCharacteristicsVals = 0;
	
	//set characteristic information
	private ArrayList charName = new ArrayList(1); 			//Name of a characteristic, e.g. SIZE 
	private ArrayList charTxt = new ArrayList(1); 			//Language dependent text of a characteristic
	private ArrayList charAddValSet = new ArrayList(1); 	//Contains the information, if an additional self defined value is allowed
	private ArrayList charAddVal = new ArrayList(1);		//Contains the content of the additional value, in the case it was set in the previous run
	private ArrayList charUnit = new ArrayList(1);			//Unit for range values, such as 'Length' or 'Weight'
	private ArrayList charDataType = new ArrayList(1);      //For usability reason, does the user have to enter char or num values
	private ArrayList charNumberDigit = new ArrayList(1);	//Length of the value, up to the decimal separater
	private ArrayList charNumberDec = new ArrayList(1);		//Length of the value, after to the decimal separater
	private ArrayList preselection = new ArrayList(1);		//Holds concatenated strings that provide an aggregated view of the previous selection  
	
	//set characteristic values, the characteristic name is taken as key 
	private ArrayList charNameKey = new ArrayList(1); 		//Key to get the link between data and header table
	private ArrayList charVal = new ArrayList(1); 			//Each entry represents 1 value of 1 characteristic
	private ArrayList charValName = new ArrayList(1);		//Name of a value
	private ArrayList charValTxt = new ArrayList(1);		//Language dependent text of a value
	private ArrayList charValMax = new ArrayList(1);		//Represents the maximum of a value, that is demanded by the user; used for ranges
	private ArrayList charValRangeMin = new ArrayList(1);	//In case that a batch characteristic has a minimum range, only valid for ranges
	private ArrayList charValRangeMax = new ArrayList(1);	//In case that a batch characteristic has a maximum range, only valid for ranges
	private ArrayList charValCode = new ArrayList(1);		//Type of the possible selection option, e.g. '1' single select (checkbox)
	private ArrayList selected = new ArrayList(1);			//Indicator to show if a single select was already set
	
	
	/*This object works as a kind of Bean for batch characteristics of a certain product!
	 * It is organized as "tables" where each coloumn represented by an ArrayList. The
	 * tables finally represent the combined information of what batch characteristics 
	 * exist for a product and which characteristics had been previously selected.
	 * 
	 * Example:
	 * --------------------------------------------------------------------------------------------------------------------------------
	 * | CharName | CharText | CharAddValSel | CharAddVal | CharUnit | CharDataType | CharNumberDigit | CharNumberDec | Preselection  |
	 * |------------------------------------------------------------------------------------------------------------------------------|
	 * | Color    | Farbe    | X             | S          |          | CHAR         | 1               | 0             | R,G,B         |
	 * | Length   | Länge    |               | 130        | cm       | NUM          | 5               | 3             | 170mm - 180mm |
	 * --------------------------------------------------------------------------------------------------------------------------------
	 * 
	 * ----------------------------------------------------------------------------------------------------------------
	 * | CharNameKey | CharVal | CharValTxt | CharValMax | CharValRangeMin | CharValRangeMax | CharValCode | Selected |
	 * |--------------------------------------------------------------------------------------------------------------|
	 * | Color       | Red     | Rot        |            |                 |                 | 1           |          |
	 * | Color       | Blue    | Blau       |            |                 |                 | 1           | X        |
	 * | Color       | Green   | Grün       |            |                 |                 | 1           | X        |
	 * | Length      | 170     |            | 180        | 120             | 200             | 3           |          |
	 * | Length      | 180     |            |            |                 |                 | 3           |          |
	 * ----------------------------------------------------------------------------------------------------------------
	 * 
	 * The merge process of product batch info and item batch info is done in the same step as the table creation.
	 * The structure of both is:
	 * 
	 * item/product
	 *   |
	 *   |
	 *   |_BatchList
	 * 			|_Batch#1 (Color)
	 * 			|	|_CharVal (S)
	 * 			|	|_CharVal (Green)
	 * 			|	|_CharVal (Red)
	 * 			|
	 * 			|_Batch#2 (Length)
	 * 				|_CharVal (120)
	 * 				|_CharVal (170 - 180)
	 * 
	 * Generally the item batch info is a subset of the product batch info!
	 */
	
	
	public BatchUIInfoContainer(ProductBatch productbatch, ItemSalesDoc item, boolean orderChange){
		
		this.orderChange = orderChange;
		
		BatchCharList batchs = productbatch.getElements();
		BatchCharList itemBatch = new BatchCharList();
		itemBatch = item.getBatchCharListJSP();
		
		// create "tables" that contain all infos about batch characteristics and their values
		// there for we are looping through all batch characteristics of a product 
		for(int i=0; i<batchs.size(); i++){
			BatchCharVals currBatch = batchs.get(i);
			
			// set all 'header' data for each characteristic ==> main table
			charName.add(i, currBatch.getCharName());
			charTxt.add(i, currBatch.getCharTxt());
			charAddValSet.add(i, currBatch.getCharAddVal() == true ? "X" : "");
			charAddVal.add("");
			if(currBatch.getCharUnitTExt() != null && currBatch.getCharUnitTExt().length()>0) charUnit.add(i, currBatch.getCharUnitTExt());
			else if(currBatch.getCharUnit() != null && currBatch.getCharUnit().length()>0) charUnit.add(i, currBatch.getCharUnit());
			else charUnit.add("");
			charDataType.add(i, currBatch.getCharDataType());
			charNumberDigit.add(i,String.valueOf(currBatch.getCharNumberDigit()));
			charNumberDec.add(i,String.valueOf(currBatch.getCharNumberDec()));
			preselection.add("");
			
			String currentPreSel = "";
			
			// prepare some parameters that are used for the merge process
			boolean found = false;
			int itemBatchIndex = 0;
			String addVal= "";
						
			// loop through the characteristic values of product batch characteristic ==> value table
			for(int j=0; j<currBatch.getCharValNum(); j++){
				
				// initalize Strings for the no item batch info exisits or simply does not match
				String value = currBatch.getCharVal(j);
				String txt = currBatch.getCharValTxt(j);
				String max = "";
				String select = "";
				
				// these are some kind of 'header' information for each characteristic value
				charNameKey.add(noOfCharacteristicsVals, currBatch.getCharName());
				charValRangeMin.add(noOfCharacteristicsVals, currBatch.getCharVal(j));
				charValRangeMax.add(noOfCharacteristicsVals, currBatch.getCharValMax(j));
				charValCode.add(noOfCharacteristicsVals, currBatch.getCharValCode(j));
								
				// check if item batch info exists
				if(itemBatch != null && itemBatch.size()>0){
					
					// see 2 lines beneath
					if(!found){
						
						// get the corresponding item batch characteristic to the product batch characteristic
						// to do this only 1 time for each product batch characteristic, we also set the found boolean field 
						for(int k=0; k<itemBatch.size(); k++){
							if(itemBatch.get(k).getCharName().equals(currBatch.getCharName())){
								found = true;
								itemBatchIndex = k;
								
								// set additional value
								if(currBatch.getCharAddVal() && itemBatch != null && itemBatch.size()>0){
									addVal = addValCheck(currBatch, itemBatch.get(itemBatchIndex), i);
									if(!addVal.equals(""))currentPreSel = addVal + ";" + currentPreSel;
								}
								break;
							}
						}
					}
					
					BatchCharVals itemBVals = new BatchCharVals();
					itemBVals = itemBatch.get(itemBatchIndex);
					
					// loop through the characteristic values of the item 
					// to reduce the amount of loops and to prevent that values are read multiple times, we remove each
					// matching value 
					for(int k=0; k<itemBVals.getCharValNum(); k++){
						switch ((currBatch.getCharValCode(j)).charAt(0)){
							case '1': // checkbox case
								if(itemBVals.getCharVal(k).toString().equals(currBatch.getCharVal(j))){
									select = "X";
									currentPreSel = currentPreSel + txt + ";";
									itemBVals.removeElement(k);
								}
								break;
							case '3': // input fields case
								if(!select.equals("X")){
										value = itemBVals.getCharVal(k); 
										txt = itemBVals.getCharValTxt(k);
										max = itemBVals.getCharValMax(k);
										select = "X";
										currentPreSel = currentPreSel + txt + ";";
										itemBVals.removeElement(k);
								}
								break;
						}
					}
				}
				
				// now set the rest of the values of the value table
				charVal.add(noOfCharacteristicsVals, value);
				charValTxt.add(noOfCharacteristicsVals, txt);
				charValMax.add(noOfCharacteristicsVals, max); 
				selected.add(noOfCharacteristicsVals, select);
				
				noOfCharacteristicsVals++;
			}
						
			// cut off the last char since it allways ends with an unnecessary ';'
			// and set the overview of the previous selection
			if (currentPreSel.length()>1)currentPreSel = currentPreSel.substring(0,currentPreSel.length()-1);
			preselection.set(i, currentPreSel);
		}
	}
	
	
	/*
	 * ***********************
	 *  set additional value
	 * **********************
	 * 
	 * Therefore we always assume that the first value is the additional one. since we have no real information if
	 * an additional value exists or not. The Backend does not return any information about that
	 */
	private String addValCheck(BatchCharVals pVals, BatchCharVals iVals, int currBatchCount){
		String result = "";
		// boolean match= false;
		
		// compare the first characterirstic value from the item with those of the product
		// if we found any match of the predefined product characteristic value, or if the 
		// maximum value is not empty, then it can't be the additional one
		for(int i=0; i<pVals.getCharValNum(); i++){
			switch (pVals.getCharValCode(i).charAt(0)){	
				case '1':
					if(iVals.getCharVal(0).equals(pVals.getCharVal(i))){ 
						return result;
					}
					break;
				case '3': 
					for(int lv_ivals=0; lv_ivals<iVals.getCharValNum(); lv_ivals++) {
						if (iVals.getCharValCode(lv_ivals).charAt(0) == '1') {
							charAddVal.set(currBatchCount, iVals.getCharVal(lv_ivals));
							iVals.removeElement(lv_ivals);
							return charAddVal.get(currBatchCount).toString();
						}
					}
					return result;
			}
		}
		// check if any matching value was found

		charAddVal.set(currBatchCount, iVals.getCharVal(0));
		iVals.removeElement(0);
		return charAddVal.get(currBatchCount).toString();
	}
	
	
	/*
	 * ***********************************
	 *  getter methods for general part
	 * ***********************************
	 */
	public String isOrderChange(){
		return orderChange == true ? "X" : "";
	}
	
	
	/*
	 * ***********************************
	 *  getter methods for the main table
	 * ***********************************
	 */
	
	public String getCharName(int index){
		return (String)charName.get(index);
	}
	
	public String getCharTxt(int index){
			return (String)charTxt.get(index);
	}
	
	public String getCharAddValSet(int index){
			return (String)charAddValSet.get(index);
	}
	
	public String getCharAddVal(int index){
			return (String)charAddVal.get(index);
	}
	
	public String getCharUnit(int index){
			return (String)charUnit.get(index);
	}
	
	public String getCharDataType(int index){
			return (String)charDataType.get(index);
	}
	
	public String getCharNumberDigit(int index){
			return (String)charNumberDigit.get(index);
	}
	
	public String getCharNumberDec(int index){
			return (String)charNumberDec.get(index);
	}
	
	public String getPreselection(int index){
			return (String)preselection.get(index);
	}
	
	/*
	 * ************************************
	 *  getter methods for the value table
	 * ************************************
	 */
		 
	public int getNoOfharacteristicsVals(){
		return noOfCharacteristicsVals;
	}
	public String getCharNameKey(int index){
			return (String)charNameKey.get(index);
	}
		
	public String getCharVal(int index){
			return (String)charVal.get(index);
	}
	
	public String getCharValName(int index){
			return (String)charValName.get(index);
	}
		
	public String getCharValTxt(int index){
			return (String)charValTxt.get(index);
	}
		
	public String getCharValMax(int index){
			return (String)charValMax.get(index);
	}
	
	public String getCharValRangeMin(int index){
			return (String)charValRangeMin.get(index);
	}
		
	public String getCharValRangeMax(int index){
			return (String)charValRangeMax.get(index);
	}
	
	public String getCharValCode(int index){
			return (String)charValCode.get(index);
	}
			
	public String getSelected(int index){
			return (String)selected.get(index);
	}

}
