
/*****************************************************************************
    Class:        OrderStatusUI
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      01.03.2004
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.isacore.uiclass.b2b.order;

import javax.servlet.jsp.PageContext;

import com.sap.isa.backend.boi.isacore.order.DocumentListFilterData;
import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.ConnectedDocument;
import com.sap.isa.businessobject.ConnectedDocumentItem;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.businessobject.order.OrderStatus;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.core.ContextConst;
import com.sap.isa.core.FrameworkConfigManager;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.core.util.table.TableRow;
import com.sap.isa.core.xcm.init.ExtendedConfigInitHandler;
import com.sap.isa.isacore.action.IsaCoreInitAction;
import com.sap.isa.isacore.action.ShopReadAction;
import com.sap.isa.isacore.action.IsaCoreInitAction.StartupParameter;
import com.sap.isa.isacore.uiclass.SalesDocumentStatusBaseUI;
import com.sap.isa.portalurlgenerator.businessobject.PortalUrlGenerator;
import com.sap.isa.portalurlgenerator.util.AnchorAttributes;
import com.sap.isa.portalurlgenerator.util.UrlParameters;


/**
 * The class OrderStatusUI is the ISACore implementation for the abstract
 * SalesDocumentStatusBaseUI UI class used on the order status detail page.  <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class OrderStatusUI extends SalesDocumentStatusBaseUI {

	protected Shop shop;
	
	protected BusinessObjectManager bom;

    protected StartupParameter startupParam;

    protected AnchorAttributes anchorAttrSales = null;
	static final private IsaLocation log = IsaLocation.getInstance(OrderStatusUI.class.getName());
	
    
    /**
     * Constructor for OrderStatusUI. <br>
     * 
     * @param pageContext
     */
    public OrderStatusUI(PageContext pageContext) {
        super(pageContext);
    }
	/**
	 * Constructor for OrderStatusUI. <br>
	 * 
	 * @param pageContext
	 */
	public OrderStatusUI() { 
	}

    /**
     * Initialize the the object from the page context. <br>
     * 
     * @param pageContext page context
     */
    public void initContext(PageContext pageContext) {
		final String METHOD_NAME = "initContext()";
		log.entering(METHOD_NAME);
        super.initContext(pageContext);

        startupParam = (StartupParameter)userSessionData.getAttribute(IsaCoreInitAction.SC_STARTUP_PARAMETER);
        showConfirmedDeliveryDate = shop.isShowConfirmedDeliveryDateRequested();
        quotationExtended = shop.isQuotationExtended();
        showInternalCatalog = shop.isInternalCatalogAvailable();
        backendR3 = (shop.getBackend().startsWith(Shop.BACKEND_R3) ? true : false);
        if (header.isDocumentTypeQuotation() && header.isQuotationExtended()) {
            try {
                // Successor document types only necessary for extended quotations
            sucProcTypeAllowedList = shop.getSucProcTypeAllowedList(header.getProcessType());
            } catch (CommunicationException comEx) {
                log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "Error reading allowed successor process types from backend for process type :" + header.getProcessType());
            }
        }        
        if (header.isDocumentTypeQuotation()) {
			// Rebuild navigation button table since sucessor process types are shop depending and
			// during processing in SalesDocumentStatusBaseUI there is no shop !!
			navButtons = createNavigationButtonTable();
			fillNavigationButtonTable(navButtons);
            }
      	log.exiting();
     }   
     
          
    /**
     * Return <code>true</code> if order download is required
     */
    public boolean isOrderDownloadRequired() {
        boolean isOrderDownloadRequired =  header.isDocumentTypeOrder();

		String flag= FrameworkConfigManager.Servlet.getInteractionConfigParameter(request.getSession(),"ui", "enable.orderdownload", ContextConst.BO_ORDERDOWNLOAD_ENABLED);
		boolean isEnabled = (flag != null && flag.equals("true"));

        if (!isHomActivated() || !isEnabled) {
            isOrderDownloadRequired = false;
        }
        return isOrderDownloadRequired;
    }


    /** 
     * Return the list of allowed process types when ordering a quotation
     */
    public ResultData getSucProcTypeAllowedList() {
        return sucProcTypeAllowedList;
    }

    /**
     * Overwrites the method . <br>
     * 
     * @see com.sap.isa.isacore.uiclass.order.OrderDataUI#setProcessTypes()
     */
    protected void setProcessTypeInfo() {
		// get description of process type
		ResultData processTypes = null;
		
		processTypeDescription = "";
		if ("quotation".equals(docType) && shop.isQuotationExtended()) {
		   processTypes = (ResultData)userSessionData.getAttribute(ShopReadAction.QUOTATION_PROCESS_TYPES);
		} else {
		   processTypes = (ResultData)userSessionData.getAttribute(ShopReadAction.PROCESS_TYPES);
		}
		if (processTypes != null) {
            noOfProcessTypes = processTypes.getNumRows();
            if (noOfProcessTypes == 1) {
                processTypes.first();
                processTypeToOrder = processTypes.getString(1);
            }
			if (processTypes.first()) {
				do  {
				   if (processTypes.getString(1).equalsIgnoreCase(header.getProcessType())) {
					  processTypeDescription = processTypes.getString(2);
                      isServiceRecall = processTypes.getString(3).equalsIgnoreCase(HeaderData.DOCUMENT_USAGE_RECALL_ORDER); 
					  break;
				   }
				} while(processTypes.next());
			}
		}
    }


    /**
     * Set the shop object in the salesDocConfiguration property. <br>
     * 
     * @see com.sap.isa.isacore.uiclass.order.OrderDataUI#setSalesDocConfiguration()
     */
    protected void setSalesDocConfiguration() {
    	
		// check for missing context
		if (userSessionData != null) {

			// get BOM
			bom = (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
	
			shop = bom.getShop();
			
			// now I can read the shop
			salesDocConfiguration = bom.getShop();
			
			quotationExtended = shop.isQuotationExtended();
		}		
    }


    /**
     * Overwrites the method . <br>
     * 
     * @param key
     * @return
     * 
     * 
     * @see com.sap.isa.isacore.uiclass.order.OrderDataUI#getSubstitutionReasonDesc(java.lang.String)
     */
    protected String getSubstitutionReasonDesc(String key) {
        return shop.getSubstitutionReasonDesc(key);
    }

    /**
     * @return Returns true if the soldTo information should be visible
     * @see com.sap.isa.isacore.uiclass.order.OrderDataUI#isSoldVisible()
     */
    public boolean isSoldToVisible() {
		boolean isVisible = shop.isSoldtoSelectable();
		
		// check also if the soldto is not the default soldto
		if (!isVisible) {
			BusinessPartner defaultSoldTo =  bom.getBUPAManager().getDefaultBusinessPartner(PartnerFunctionData.SOLDTO);
			if (defaultSoldTo != null) {
				String soldToId = header.getPartnerId(PartnerFunctionData.SOLDTO);
				if (soldToId != null && ! soldToId.equals(defaultSoldTo.getId())) {
					isVisible = true;	
				}
			}	
		}
		
		return isVisible;
    }

    /**
     * @param soldToKey TechKey representing the SoldTo key
     * @return BusinessPartner of the SoldTo
     * @see com.sap.isa.isacore.uiclass.order.OrderDataUI#getSoldTo(com.sap.isa.core.TechKey)
     */
    protected BusinessPartner getSoldTo(TechKey soldToKey) {
        
		return  bom.createBUPAManager().getBusinessPartner(soldToKey);
    }
    /**
     * @return Returns true if the hosted order management is activated.
     */
    public boolean isHomActivated() {
        return shop.isHomActivated();
    }
    /**
     * Returns true, if the document is a service recall
     */
    public boolean isServiceRecall() {
        return isServiceRecall;
    }
    /**
     * Implemented method to fill Table JScriptFilenames
     */
    protected void fillJScriptFileNameTable(Table tab) {
        TableRow row = tab.insertRow();
        row.setValue("FILENAME","appbase/jscript/ipc_pricinganalysis.jsp");
        
        row = tab.insertRow();
        row.setValue("FILENAME","b2b/jscript/frames.js");

        row = tab.insertRow();
        row.setValue("FILENAME","appbase/jscript/getElementsByType.js");

        row = tab.insertRow();
        row.setValue("FILENAME","b2b/jscript/send_confirm.js");

        row = tab.insertRow();
        row.setValue("FILENAME","ecombase/jscript/js_orderstatusdetail.js.jsp");
        
        row = tab.insertRow();
        row.setValue("FILENAME","appbase/jscript/portalnavigation.js.jsp");

        row = tab.insertRow();
        row.setValue("FILENAME","iviews/jscript/crm_isa_epcm.js");
    }
    /**
     * Implemented method to fill Table NavigationButton. This method includes also all checks
     * whether a button should be diplayed or not. Following buttons exist:<br>
     * - Transfer to Basket<br>
     * - - not in a HOM scenario<br>
     * - Print<br>
     * - Close Document<br>
     * - Change<br>
     * - - for large documents are these Change Header and Change Items<br>
     * - Order Quotation<br>
     * - - only for released quotations<br>
     * - - Drop Down Box for process type selection
     * - OCI Transfer<br>
     * - - only if Shop has been started with &hookurl=....<br>
     */
    protected void fillNavigationButtonTable(Table navButtons) {
		User user = bom.getUser();
        // Check if oci transfer is required
        boolean ociTransfer = 
            (((IsaCoreInitAction.StartupParameter)userSessionData.
                getAttribute(IsaCoreInitAction.SC_STARTUP_PARAMETER)).
                getHookUrl().length() > 0);
        // Check further OCI requirements which means only Released Ext.Quots or   
        // Open Lean Quots (which is the same!) and Order Templates can be tranferred.
        if (ociTransfer) {
            if (header.isDocumentTypeQuotation() && 
               ( header.isStatusReleased() || ! header.isQuotationExtended() && header.isStatusOpen()) ||
                header.isDocumentTypeOrderTemplate()) {
                    ociTransfer = true;
            } else {
                    ociTransfer = false;
            }                 
        }
        // Check Quotation can be ordered. Only Open Lean Quots and Released Ext. Quots.
        boolean orderQuotpossible = false;
        if (header.isDocumentTypeQuotation() && 
           ( header.isStatusReleased() || ! header.isQuotationExtended() && header.isStatusOpen())) {
				Boolean perm = new Boolean(false);
				try{			 
					perm = user.hasPermission(DocumentListFilterData.SALESDOCUMENT_TYPE_ORDER,
												 DocumentListFilterData.ACTION_CREATE);
				}
				catch (CommunicationException ex){
					log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "Failed to connect to the backend", ex);		
				}
				if (!perm.booleanValue()){
					log.error(LogUtil.APPS_COMMON_SECURITY, "User has no permission to create " + DocumentListFilterData.SALESDOCUMENT_TYPE_ORDER);
				}
				else{
                orderQuotpossible = true;
				}
        }                 
        boolean changeable = false;
        if ((docType.equals(DocumentListFilterData.SALESDOCUMENT_TYPE_ORDER) ||
            docType.equals(DocumentListFilterData.SALESDOCUMENT_TYPE_COLLECTIVE_ORDER) ||
            docType.equals(DocumentListFilterData.SALESDOCUMENT_TYPE_ORDERTEMPLATE) ||
            ( docType.equals(DocumentListFilterData.SALESDOCUMENT_TYPE_QUOTATION) &&
                 (header.isQuotationExtended() &&
                 (header.isStatusOpen() ||
                  header.isStatusReleased()))))
             &&  header.isChangeable() ) {
				  //check for the document change permission for the user
				
				  Boolean perm = new Boolean(false);
				  try{			 
					perm = user.hasPermission(docType, DocumentListFilterData.ACTION_CHANGE);
				  }
				  catch (CommunicationException ex){
					  log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "Failed to connect to the backend", ex);		
				  }
				  if (!perm.booleanValue()){
				   	log.error(LogUtil.APPS_COMMON_SECURITY, "User has no permission to change " + docType);
				  }
				  else{ 
                  	changeable = true;
				  }
           }
        // we have to avoid that completed and cancelled orders can be re-opened or changed
        // the functionality is given in CRM online, but makes no sense for webusers  
        if ((changeable == true) && (docType.equals(DocumentListFilterData.SALESDOCUMENT_TYPE_ORDER)) &&
             (header.isStatusCompleted() || header.isStatusCancelled()) ) {
				changeable = false;
        }
        
        // we have to avoid that auction orders can be re-opend or changed
//        if (shop.isEAuctionUsed() && shop.getAuctionOrderType().equals(header.getProcessType())) {
//            changeable = false;
//        }
        // Fill table with allowed buttons
        TableRow row = navButtons.insertRow();
        /* TRANSFER BUTTON */
        if (! shop.isHomActivated()) {
            row.setValue("ALIGN","LEFT");
            row.setValue("NAME", "status.sales.detail.btn.transfer");
            row.setValue("ID", "transferbutton");
            row.setValue("DISABLED", "true");
            row.setValue("TITLE", "status.btn.transfer.tit");
            row.setValue("HREF", "#");
            row.setValue("ONCLICK", "transferToBasket();");
            row.setValue("CSSCLASS", "");
            row.setValue("LINKED_ELEMENT", "");
        }
        /* PRINT BUTTON */
        row = navButtons.insertRow();
        row.setValue("ALIGN","LEFT");
        row.setValue("NAME", "status.sales.detail.button.print");
        row.setValue("TITLE", "status.sales.detail.button.print");
        row.setValue("HREF", "#");
        row.setValue("ONCLICK", "print();");
        row.setValue("CSSCLASS", "");
        row.setValue("LINKED_ELEMENT", "");
        /* OCI BUTTON */
        if (ociTransfer) {
            row = navButtons.insertRow();
            row.setValue("ALIGN","RIGHT");
            if (header.isDocumentTypeOrderTemplate()) {
                row.setValue("NAME", "status.sales.detail.button.otmp");
            } else {
                row.setValue("NAME", "status.sales.detail.button.squot");
            }
            if (header.isDocumentTypeOrderTemplate()) {
                row.setValue("TITLE", "status.sales.detail.button.otmp");
            } else {
                row.setValue("TITLE", "status.sales.detail.button.squot");
            }
            row.setValue("HREF", "#");
            row.setValue("ONCLICK", "sendOci('" + header.getTechKey().getIdAsString() + "');");
            row.setValue("CSSCLASS", "");
            row.setValue("LINKED_ELEMENT", "");
        }
        /* ORDER QUOTATION BUTTON */
        if (orderQuotpossible) {
            if (header.isQuotationExtended()  &&  getSucProcTypeAllowedList() != null) {
            row = navButtons.insertRow();
            row.setValue("ALIGN","RIGHT");
            if (getSucProcTypeAllowedList().getNumRows() == 1) {
				getSucProcTypeAllowedList().first();
				String processTypeToOrder = getSucProcTypeAllowedList().getString(1);
                // With only one Process type which will be used to order
                row.setValue("NAME", "status.sales.detail.btn.quot");
                row.setValue("TITLE", "status.sales.detail.btn.quot");
                row.setValue("HREF", "#");
                row.setValue("ONCLICK", "orderQuotation('" + processTypeToOrder + "');");
                row.setValue("CSSCLASS", "");
                row.setValue("LINKED_ELEMENT", "");
            } else {
                // With a select box to choose a process type which will be ordered
                row.setValue("NAME", "b2b.docnav.ok");
                row.setValue("TITLE", "b2b.docnav.ok");
                row.setValue("HREF", "#");
                row.setValue("ONCLICK", "orderQuotation();");
                row.setValue("CSSCLASS", "");
                row.setValue("LINKED_ELEMENT", "PROCESSTYPESELECTION");
            }
            } else {
                row = navButtons.insertRow();
                row.setValue("ALIGN","RIGHT");
                row.setValue("NAME", "status.sales.detail.btn.quot");
                row.setValue("TITLE", "status.sales.detail.btn.quot");
                row.setValue("HREF", "#");
                // Since lean quotations will not be copied we don't need as real process type
                // as successor type.
                row.setValue("ONCLICK", "orderQuotation('" + header.getProcessType() + "');");
                row.setValue("CSSCLASS", "");
                row.setValue("LINKED_ELEMENT", "");
        }
        }
        /* CHANGE BUTTON */
        if (changeable &&  ! ociTransfer) {
            if (isLargeDocument) {
                // Large document 1. Button Change Header
                row = navButtons.insertRow();
                row.setValue("ALIGN","RIGHT");
                row.setValue("NAME", "status.sales.det.chghead");
                row.setValue("TITLE", "status.sales.det.chghead");
                row.setValue("HREF", "#");
                row.setValue("ONCLICK", "changeDocument('HEADONLY');");
                row.setValue("CSSCLASS", "");
                row.setValue("LINKED_ELEMENT", "");
                // 2. Button Change Items
                row = navButtons.insertRow();
                row.setValue("ALIGN","RIGHT");
                row.setValue("NAME", "status.sales.det.chgit");
                row.setValue("TITLE", "status.sales.det.chgit");
                row.setValue("HREF", "#");
                row.setValue("ONCLICK", "transferToOrderChange()");
                row.setValue("CSSCLASS", "");
                row.setValue("LINKED_ELEMENT", "");
            } else {
                row = navButtons.insertRow();
                row.setValue("ALIGN","RIGHT");
                row.setValue("NAME", "status.sales.detail.btn.change");
                row.setValue("TITLE", "status.sales.detail.btn.change");
                row.setValue("HREF", "#");
                row.setValue("ONCLICK", "changeDocument()");
                row.setValue("CSSCLASS", "");
                row.setValue("LINKED_ELEMENT", "");
            }
        }
        /* CLOSE BUTTON */
        row = navButtons.insertRow();
        row.setValue("ALIGN","RIGHT");
        row.setValue("NAME", "status.sales.detail.button.close");
        row.setValue("TITLE", "status.sales.detail.button.close");
        row.setValue("HREF", "#");
        row.setValue("ONCLICK", "closeDocument();");
        row.setValue("CSSCLASS", "");
        row.setValue("LINKED_ELEMENT", "");
    }
    /**
     * Method returns the AnchorAttribute object which contains the information to do Portal 
     * navigation. The navigation will point to Internet Sales web application (separate billing
     * part). The action forward used for claim creation is <b>cedocumentstatusdetail</b>.
     * @return AnchorAttribute 
     */
    public AnchorAttributes getPortalAnchorAttributesForBillingDocDisplay() {
        if (anchorAttrSales == null) {
            if (ExtendedConfigInitHandler.isActive()  &&  startupParam.isPortal() 
                &&  ! startupParam.isBillingIncluded()) {
                UrlParameters urlPars = new UrlParameters();
                PortalUrlGenerator urlGen = bom.createPortalUrlGenerator();
                urlPars.add("nextaction", "cedocumentstatusdetail");
           
                try {
                    anchorAttrSales = urlGen.readPageURL("BILLING","","","DEFAULT","",urlPars, request);
                } catch (CommunicationException comEx) {
                	log.debug(comEx.getMessage());
                }
            }
        }
        return anchorAttrSales;
    }
    /**
     * Write out JScript onClick event depending on the existens of a referenced doc.
     * @return String like 'onclick="documentDisplay('dockey', 'docno', 'doctype');"
     */
    public String writeHeaderRefOnClickEvent(ConnectedDocument conDoc) {
        String retVal = "";
        if (shop.isBillingSelectionNoDoc()) {
            // Shop doesn't allow Billing docs, so don't show any link and just the doc.number
            return retVal;
        }
        
        if ( !(( HeaderData.DOCUMENT_TYPE_BILLING.equals(conDoc.getDocType())) ||
              ( HeaderData.DOCUMENT_TYPE_NEGOTIATED_CONTRACT.equals(conDoc.getDocType())) ||
		      ( HeaderData.DOCUMENT_TYPE_QUOTATION.equals(conDoc.getDocType())) || 
		      ( HeaderData.DOCUMENT_TYPE_ORDER.equals(conDoc.getDocType())) ||
		      ("INVOI".equals(conDoc.getDocType())))) {
        	return retVal;
        }
        
        String isBillingDoc = ("CRMBE".equals(conDoc.getAppTyp()) ||  "BILL".equals(conDoc.getAppTyp())
                              ? "X" : "");
		                             
        String objOrigin = conDoc.getDocumentsOrigin();
        if (getPortalAnchorAttributesForBillingDocDisplay() != null   
            && startupParam.isPortal() &&  ! startupParam.isBillingIncluded() && "X".equals(isBillingDoc)) {
            retVal = "onclick=\"documentDisplay(" + ("'PORTAL'," + "'"+conDoc.getTechKey().getIdAsString()+"',")
                     + ("'"+conDoc.getDocNumber()+"',") + "'"+conDoc.getDocType()+"',"  
                     + ("'"+isBillingDoc+"',") + ("'"+objOrigin+"'") +");\"";   
        }

        if (startupParam.isBillingIncluded()) {
            retVal = "onclick=\"documentDisplay(" + ("'SHOP'," + "'"+conDoc.getTechKey().getIdAsString()+"',")
                     + ("'"+conDoc.getDocNumber()+"',") + "'"+conDoc.getDocType()+"',"  
                     + ("'"+isBillingDoc+"',") +("'"+objOrigin+"'") +");\"";   
        }
        return retVal;
    }
    
    /**
     * Returns the correct Value for the Item, wich is NetValueWOfreight 
     * for CRM backends and NetValue else.
     * 
     * @return the correct value for the item, depending on the backend
     */
    public String getItemValue() {

        if (backendR3 || ((OrderStatus) mdoc.getDocument()).getOrder().isDocumentRecoverable()) {
            return item.getNetValue();
        }
        else  {
            // START INSERTION 967900
            if (! header.isChangeable()  &&  
                 (header.getFreightValue() == null  ||  header.getFreightValue().equals(header.getNetValueWOFreight())) ) {
                 // In case of R/3 created documents (the check for that, doc not changeable, freight and netVal WO freigth are 0,00 
                 // might not be the best but easiest way), no freight and net value without freight are available !!!!
                return item.getNetValue();
            } else {
            // END INSERTION 967900
                return item.getNetValueWOFreight();
            }
        }
    }
    /**
     * Returns the correct Value for the header, wich is NetValueWOfreight 
     * for CRM backends and NetValue else.
     * 
     * @return the correct value for the header, depending on the backend
     */
    public String getHeaderValue() {
        if (backendR3 || ((OrderStatus) mdoc.getDocument()).getOrder().isDocumentRecoverable()) {
            return header.getNetValue();
        } else  {
            // START INSERTION 967900
            if (! header.isChangeable()  &&  
                 (header.getFreightValue() == null  ||  header.getFreightValue().equals(header.getNetValueWOFreight())) ) {
                // In case of R/3 created documents (the check for that, doc not changeable, freight and netVal WO freigth are 0,00 
                // might not be the best but easiest way), no freight and net value without freight are available !!!!
                return header.getNetValue();
            } else {
            // END INSERTION 967900
                return header.getNetValueWOFreight();
            }
        }
    }

	/**
	 * Write out JScript onClick event depending on the existens of a referenced doc.
	 * @return String like 'onclick="documentDisplay('dockey', 'docno', 'doctype');"
	 */
	public String writeItemRefOnClickEvent(ConnectedDocumentItem conDocItem) {
		String retVal = "";		
			   if (shop.isBillingSelectionNoDoc()) {
				   // Shop doesn't allow Billing docs, so don't show any link and just the doc.number
				   return retVal;
			   }
        
			   if ( !(( HeaderData.DOCUMENT_TYPE_BILLING.equals(conDocItem.getDocType())) ||
					 ( HeaderData.DOCUMENT_TYPE_NEGOTIATED_CONTRACT.equals(conDocItem.getDocType())) ||
					 ( HeaderData.DOCUMENT_TYPE_QUOTATION.equals(conDocItem.getDocType())) || 
		             ( HeaderData.DOCUMENT_TYPE_ORDER.equals(conDocItem.getDocType())) ||
					 ("INVOI".equals(conDocItem.getDocType())))) {
				   return retVal;
			   }
        
			   String isBillingDoc = ("CRMBE".equals(conDocItem.getAppTyp()) ||  "BILL".equals(conDocItem.getAppTyp())
									 ? "X" : "");
		                             
			   String objOrigin = conDocItem.getDocOrigin();
			   if (getPortalAnchorAttributesForBillingDocDisplay() != null   
				   && startupParam.isPortal() &&  ! startupParam.isBillingIncluded() && "X".equals(isBillingDoc)) {
				   retVal = "onclick=\"documentDisplay(" + ("'PORTAL'," + "'"+conDocItem.getHeaderKey()+"',")
							+ ("'"+conDocItem.getDocNumber()+"',") + "'"+conDocItem.getDocType()+"',"  
							+ ("'"+isBillingDoc+"',") + ("'"+objOrigin+"'") +");\"";   
			   }

			   if (startupParam.isBillingIncluded()) {
				   retVal = "onclick=\"documentDisplay(" + ("'SHOP'," + "'"+conDocItem.getHeaderKey()+"',")
							+ ("'"+conDocItem.getDocNumber()+"',") + "'"+conDocItem.getDocType()+"',"  
							+ ("'"+isBillingDoc+"',") +("'"+objOrigin+"'") +");\"";   
			   }
			   return retVal;
	}		

}