/*****************************************************************************
    Class:        BasketUI
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      02.03.2004
    Version:      1.0

    $Revision: #1$
    $Date: 2004/03/02 $ 
*****************************************************************************/
package com.sap.isa.isacore.uiclass.b2b.order;

import javax.servlet.jsp.PageContext;

import com.sap.spc.remote.client.object.IPCItem;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.isacore.action.order.MaintainBasketBaseAction;
import com.sap.isa.backend.boi.isacore.order.DocumentListFilterData;
import com.sap.isa.core.PanicException;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.logging.LogUtil;

/**
 * The class BasketUI . <br>
 *
 * @author  SAP AG
 * @version 1.0
 *
 */
public class BasketUI extends SalesDocumentUI {		
	
    final String SCENARIO_ERPCRM = "B2BERPCRM";
    
	/**
	 * @param pageContext
	 */
	public BasketUI(PageContext pageContext) {
		
		super(pageContext);
		
		determineKeyValues();
	}
	
	/**
	 * Determines the keys for the send, confirm and cancel buttons
	 */
	protected void determineKeyValues() {
		
		confirmKey = "b2b.ord.conf." + docType;
		cancelKey = "";
		
		if (ociTransfer && "basket".equals(docType)) {
			cancelKey  = "b2b.ord.cncl.conf." + docType + ".oci";
			confirmKey += ".oci";
		}
		else {
			cancelKey  = "b2b.ord.cncl.conf." + docType;
			confirmKey += ".plain";
		}
		alertBasketEmpty = "b2b.ordr.alr." + docType + ".empty";

		sendKey = "b2b.ord.sub.send." + docType;
	}
	
	
	/**
	 * Determine the number of ne positions to be displayes
	 * The value is taken from the session parameter <code>"newpos"</code>.
	 */
	protected void determineNewPos() {
		super.determineNewPos();
		
        if (isAuction) {
        	newpos = 0;
        }
	}
	
	/**
	 * Returns the soldToName if it is not empty otherwise the SoldToId is returned
	 * 
	 * @return String soldToname if not empty, SoldToId otherwise
	 */
	public String getSoldToInfo() {
		if (soldToName != null && soldToName.trim().length() > 0) {
		    return soldToName; 
		 } 
		 else {
			return soldToId;
		}
		
	}
	
	/**
	 * Returns true if the document is an OrderTemplate
	 * 
	 * @return boolean true if the document is an OrderTemplate
	 */
	public boolean isOrderTemplate() {
		return MaintainBasketBaseAction.DOCTYPE_ORDERTEMPLATE.equals(getDocType());
	}
	
	/**
	 * Returns true if the document is a Quotation
	 * 
	 * @return boolean true if the document is a Quotation
	 */
	public boolean isQuotation() {
		return MaintainBasketBaseAction.DOCTYPE_QUOTATION.equals(getDocType());
	}
	
	/**
	 * Returns true if the backend is R3PI
	 * 
	 * @return boolean true if backend is R3PI
	 */
	public boolean isBackendR3PI() {
		return backendR3PI;
	}
	
	/**
	 * Returns the id that is used to access the related IPCItem for an Item
	 * 
	 * @return String id of an IPCItem
	 */
	public String getIPCItemId(ItemSalesDoc item){
		String ipcItemId = "";
		
		if (header.getIpcDocumentId() != null ) { 
			if (item.getExternalItem() != null) {
				ipcItemId = ((IPCItem) item.getExternalItem()).getItemId(); 
			}
			else {
				ipcItemId = item.getTechKey().getIdAsString();
			}
		}
		
		return ipcItemId;
	}
	
	
	/**
	 * Returns the number of ShipTos<br>
	 *  
	 * @return the number of ShipTos
	 */
	public int getNumShipTos() {
		return salesDoc.getNumShipTos();
	}
	
	/**
	 * Returns true, if the CUA link can he shown<br>
	 *  
	 * @return true, if the CUA lionk can he shown
	 */
	public boolean isCUAAvailable() {
	   //available for R3 since Release 4.6C
	   return  super.isCUAAvailable();
	} 
	
	/**
	 * Returns the Colspan value for the items table<br>
	 *  
	 * @return the Colspan value for the items table
	 */
	public int getItemsColspan() {
		return (isContract) ? 11 : 10;
	}
	
	/**
	 * Returns the is externalToOrder flag of the salesDocument. <br>
	 *  
	 * @return is externalToOrder flag of the salesDocument
	 */
	public boolean isExternalToOrder() {
	    return salesDoc.isExternalToOrder();
	}
	
    /**
     * Checks if user has permission for creating orders. <br>
     * @return is create order permission flag.
     */
	public boolean hasCreateOrderPermission() {	
		Boolean perm = Boolean.FALSE;
		try {
			perm = bom.getUser().hasPermission(DocumentListFilterData.SALESDOCUMENT_TYPE_ORDER,
											 DocumentListFilterData.ACTION_CREATE);
		}
		catch (CommunicationException e) {
            log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "Failed to connect to backend", e);
        }
		return perm.booleanValue();
		
	}
	
	/**
     * Checks if user has permission for creating quotations. <br>
     * @return is create quotation permission flag.
     */
	public boolean hasCreateQuotPermission() {		
		Boolean perm = Boolean.FALSE;
		try {
			perm = bom.getUser().hasPermission(DocumentListFilterData.SALESDOCUMENT_TYPE_QUOTATION,
											 DocumentListFilterData.ACTION_CREATE);
		}
		catch (CommunicationException e) {
            log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "Failed to connect to backend", e);
        }
		return perm.booleanValue();
	}
	
	/**
     * Checks if user has permission for creating ordertemplates. <br>
     * @return is create ordertemplate permission flag.
     */
	public boolean hasCreateTempPermission() {
		Boolean perm = Boolean.FALSE;
		try {
			perm = bom.getUser().hasPermission(DocumentListFilterData.SALESDOCUMENT_TYPE_ORDERTEMPLATE,
											 DocumentListFilterData.ACTION_CREATE);
		}
		catch (CommunicationException e) {
            log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "Failed to connect to backend", e);
        }
		return perm.booleanValue();		
	}

    /**
     * Returns true if the availability information should be displayed
     * @return boolean true if availability information is displayed
     */
    public boolean isATPDisplayed() {
        boolean isATPDisplayed = !isLateDecision && !isOrderTemplate();
        
        // display availability information for ERP CRM shop
        if (isATPDisplayed && isBackendR3()) {
            String scen = getShop().getScenario();
            isATPDisplayed = scen.equals(SCENARIO_ERPCRM);
        }
        return isATPDisplayed;
    }
	
}
