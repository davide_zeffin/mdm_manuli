/*
 * Created on 20.07.2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.isacore.uiclass.b2b.order;

import javax.servlet.jsp.PageContext;

import com.sap.isa.businessobject.BatchCharList;
import com.sap.isa.businessobject.BatchCharVals;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.core.util.JspUtil;
import com.sap.isa.isacore.action.order.MaintainBasketBaseAction;
import com.sap.isa.ui.uiclass.BaseUI;

/**
 * @author d031177
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class BatchUI extends BaseUI {
	
	protected BatchUIInfoContainer batchBean = null;
	protected String itemGuid = "";
	protected String productId = "";
	protected String product = "";
	protected BatchCharList batches = null;
	protected ItemSalesDoc item = null;
	
	public BatchUI(PageContext pageContext){
		super.initContext(pageContext);
		
		if (userSessionData.getAttribute("batchproductid") != null ){
		  productId = (String)userSessionData.getAttribute("batchproductid");
		}
		
		if (userSessionData.getAttribute("batchselection") != null) {
			itemGuid = (String)userSessionData.getAttribute("batchselection");
		}

		if (userSessionData.getAttribute("batchproduct") != null) {
		  product = (String)userSessionData.getAttribute("batchproduct");
		}
		
		batches = (BatchCharList) request.getAttribute(MaintainBasketBaseAction.RC_BATCHS);
		
		item = (ItemSalesDoc)request.getAttribute(MaintainBasketBaseAction.RC_SINGLE_ITEM);
		
		batchBean = (BatchUIInfoContainer)userSessionData.getAttribute("batchbean");
	}
	
	
	/*
	 * ***********************************
	 *  getter methods for general part
	 * ***********************************
	 */
	public String isOrderChange(){
		return batchBean.isOrderChange();
	}
	
	public int getNoOfEntries(){
		return batchBean.getNoOfharacteristicsVals();
	}
	
	
	/*
	 * ***********************************
	 *  getter methods for the main table
	 * ***********************************
	 */
	
	public String getCharName(int i){
		return batchBean.getCharName(i);
	}
	
	public String getCharTxt(int i){
		return batchBean.getCharTxt(i);
	}
	
	public String getCharAddValSet(int i){
		return batchBean.getCharAddValSet(i);
	}
	
	public String getCharAddVal(int i){
		return batchBean.getCharAddVal(i);
	}
		
	public String getCharUnit(int i){
		return batchBean.getCharUnit(i);
	}
	
	public String getCharDataType(int i){
		return batchBean.getCharDataType(i);
	}

	public String getCharNumberDigit(int i){
		return batchBean.getCharNumberDigit(i);
	}

	public String getCharNumberDec(int i){
		return batchBean.getCharNumberDec(i);
	}
	
	public String getPreselection(int i){
		return batchBean.getPreselection(i);
	}
	
	
	/*
	 * ************************************
	 *  getter methods for the value table
	 * ************************************
	 */
	 
	public String getCharNameKey(int i){
		return batchBean.getCharNameKey(i);
	}
	
	public String getCharVal(int i){
		return batchBean.getCharVal(i);
	}
	
	public String getCharValTxt(int i) {
		return batchBean.getCharValTxt(i);
	}
	
	public String getCharValMax(int i){
		return batchBean.getCharValMax(i);
	}
	
	public String getCharValRangeMin(int i){
		return batchBean.getCharValRangeMin(i);
	}
	
	public String getCharValRangeMax(int i){
		return batchBean.getCharValRangeMax(i);
	}
	
	public String getSelected(int i){
		if(batchBean.getSelected(i).equals("X")){
			return "checked";
		}
		return "";
	}
	
	/**
	 * 
	 * @param int i attribute index
	 * @return int i value for different input type
	 */
	public char getInputType(int i){
		
		return batchBean.getCharValCode(i).charAt(0);
	}
	
	/**
	 * Returns itemGuid
	 * 
	 * @return
	 */
	public String getItemGuid() {
		return itemGuid;
	}
	
	/**
	 * Returns the product id of the related product
	 * 
	 * @return
	 */
	public String getProductId() {
		return productId;
	}
	
	/**
	 * Returns the product of the related product
	 * 
	 * @return
	 */
	public String getProduct() {
		return product;
	}
	
	/**
	 * Returns the batches 
	 * 
	 * @return
	 */
	public BatchCharList getBatches() {
		return batches;
	}
	
	/**
	 * Returns the batches size
	 * 
	 * @return
	 */
	public int getBatchesSize() {
		return batches != null ? batches.size() : 0;
	}
	
	/**
	 * Returns the item 
	 * 
	 * @return
	 */
	public ItemSalesDoc getItem() {
		return item;
	}
	
	/**
	 * Returns the Batch Attribute description 
	 * 
	 * @param int i attribute index
	 * @return String the batch description
	 */
	public String getBatchDesc(int i) {
		return batchBean.getCharTxt(i) != "" ? batchBean.getCharTxt(i) : batchBean.getCharName(i);
	}
	
	/**
	 * Returns the Batch Attribute description 
	 * 
	 * @param int i attribute index
	 * @return String the batch description
	 */
	public String getBatchValDesc(int i) {
		return batchBean.getCharValTxt(i);
	}
	
	/**
	 * Returns the concatenated String of Attribute characteristics, selected for the item
	 * 
	 * @param int i attribute index
	 * @return String the selected values for the Batch attribute description
	 */
	public String getSelectedBatchValues(int i) {
		
		String valueList = "";
		BatchCharVals charVal = item.getBatchCharListJSP().get(i);
		
		for (int j=0; j < charVal.getCharValNum(); j++) {
			if(charVal.getCharValMax(j).equals("")) {
				valueList = valueList + JspUtil.encodeHtml(charVal.getCharVal(j)) + ";";
			}
			else{
				valueList = valueList + JspUtil.encodeHtml(charVal.getCharVal(j)) + "-" + JspUtil.encodeHtml(charVal.getCharValMax(j));
			}
		}
		
		return valueList;
	 }
	
}
