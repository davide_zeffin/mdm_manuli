/*****************************************************************************
    Class:        ConfirmationUI
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      02.03.2004
    Version:      1.0

    $Revision: #1$
    $Date: 2004/03/02 $ 
*****************************************************************************/
package com.sap.isa.isacore.uiclass.b2b.order;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.jsp.PageContext;

import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.SalesDocumentStatus;
import com.sap.isa.businessobject.Schedline;
import com.sap.isa.businessobject.ShipTo;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.PartnerFunctionBase;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.isacore.action.order.DocumentStatusDetailPrintAction;
import com.sap.isa.isacore.action.order.MaintainBasketBaseAction;
import com.sap.isa.isacore.action.order.MaintainOrderBaseAction;
import com.sap.isa.isacore.action.order.ShowOrderConfirmedAction;
import com.sap.isa.isacore.actionform.order.DocumentListSelectorForm;

/**
 * The class ConfirmationUI. UI class for the confirmation.jsp.<br>
 *
 * @author  SAP AG
 * @version 1.0
 *
 */
public class ConfirmationUI extends SalesDocumentUI {
	
	protected HashMap campDescs = null;
	protected HashMap campTypeDescs = null;
    protected HashMap configInfoMap = null;
    protected HashMap configInfoDetailMap = null;
	public boolean isPrintOrderStatus = false;
    public boolean isDocumentRecoverable = false;
	protected SalesDocumentStatus salesDocStat = null;
    protected boolean isChangeMode = false;
    
    protected boolean isGrossValueAvailable = true;
    protected boolean isNetValueAvailable = true;
    protected boolean isFreightValueAvailable = true;
    protected boolean isTaxValueAvailable = true;
    
	
	/**
	 * Flag if the current order document is a hosted order. <br>
	 */
	protected boolean isCustomerDocument = false;
	
	/**
	 * Flag if the ship to is equals the sold from. (Only HOM). <br>
	 */
	protected boolean isPickupFromVendor = false;
	
	
	/**
	 * @param pageContext
	 */
	public ConfirmationUI(PageContext pageContext) {
        
		super(pageContext);
		
		campDescs = (HashMap) request.getAttribute(MaintainOrderBaseAction.RC_CAMPDESC);
		campTypeDescs = (HashMap) request.getAttribute(MaintainOrderBaseAction.RC_CAMPTYPEDESC);
        
        configInfoMap = (HashMap) request.getAttribute(MaintainOrderBaseAction.RC_CONFIG_INFO);
        configInfoDetailMap = (HashMap) request.getAttribute(MaintainOrderBaseAction.RC_CONFIG_INFO_DETAIL);
		
		if (request.getAttribute(DocumentStatusDetailPrintAction.RC_PRINTORDERSTATUS) != null &&
           ((String) request.getAttribute(DocumentStatusDetailPrintAction.RC_PRINTORDERSTATUS)).length() > 0 ) {
			isPrintOrderStatus = true;
        }
        else {
            docType = (String) request.getAttribute(ShowOrderConfirmedAction.RC_CONFIRMED_DOCTYPE);
            docNumber = removeNull(header.getSalesDocNumber());
            //docDate = removeNull(header.getChangedAt()); DEL 978390 changed at date is not consistent to other
                                                          // application parts.
        }
        
        setProcessTypeInfo();
        
        if (request.getAttribute(MaintainOrderBaseAction.RC_DOC_RECOVERABLE) != null &&
            ((String) request.getAttribute(MaintainOrderBaseAction.RC_DOC_RECOVERABLE)).length() > 0 ) {
            isDocumentRecoverable = true;
        }
        
        if (request.getAttribute(MaintainOrderBaseAction.RC_IS_CHANGE_MODE) != null &&
            "Y".equals(((String) request.getAttribute(MaintainOrderBaseAction.RC_IS_CHANGE_MODE)))) {
            isChangeMode = true;
        }
        
        if (request.getAttribute(MaintainOrderBaseAction.RC_IS_LARGE_DOC) != null &&
            "Y".equals(((String) request.getAttribute(MaintainOrderBaseAction.RC_IS_LARGE_DOC)))) {
            isLargeDocument = true;
        }
        
		if (request.getAttribute(DocumentStatusDetailPrintAction.RC_CUSTOMERORDERSTATUS) != null &&
		   		((String) request.getAttribute(DocumentStatusDetailPrintAction.RC_CUSTOMERORDERSTATUS)).equals("true")) {
			isCustomerDocument = true;
			ShipTo shipTo = header.getShipTo();
			if (shipTo != null) {
				BusinessPartner soldFromPartner = bom.createBUPAManager().getDefaultBusinessPartner(PartnerFunctionBase.SOLDFROM);
				isPickupFromVendor = shipTo.getId().equals(soldFromPartner.getId());     	
			}
		}
        
        Object obj = request.getAttribute(SalesDocument.IS_GROSS_VALUE_AVAILABLE); 
        if (obj != null) {
            Boolean flag = (Boolean) obj;
            setGrossValueAvailable(flag.booleanValue());
        }
        
        obj = request.getAttribute(SalesDocument.IS_NET_VALUE_AVAILABLE); 
        if (obj != null) {
            Boolean flag = (Boolean) obj;
            setNetValueAvailable(flag.booleanValue());
        }
        
        obj = request.getAttribute(SalesDocument.IS_TAX_VALUE_AVAILABLE); 
        if (obj != null) {
            Boolean flag = (Boolean) obj;
            setTaxValueAvailable(flag.booleanValue());
        }
        
        obj = request.getAttribute(SalesDocument.IS_FREIGHT_VALUE_AVAILABLE); 
        if (obj != null) {
            Boolean flag = (Boolean) obj;
            setFreightValueAvailable(flag.booleanValue());
        }
        
	}   
	
	/**
	 * Returns true if the document is an OrderTemplate
	 * 
	 * @return boolean true if the document is an OrderTemplate
	 */
	public boolean isOrderTemplate() {
		return MaintainBasketBaseAction.DOCTYPE_ORDERTEMPLATE.equals(getDocType());
	}
	
	/**
	 * Returns true if the document is a Quotation
	 * 
	 * @return boolean true if the document is a Quotation
	 */
	public boolean isQuotation() {
		return MaintainBasketBaseAction.DOCTYPE_QUOTATION.equals(getDocType());
	}
	
	/**
	 * Returns true if the backend is R3PI
	 * 
	 * @return boolean true if backend is R3PI
	 */
	public boolean isBackendR3PI() {
		return backendR3PI;
	}
	
	/**
	 * Returns true, if the CUA lionk can he shown<br>
	 *  
	 * @return true, if the CUA lionk can he shown
	 */
	public boolean isCUAAvailable() {
	   return  super.isCUAAvailable() && !backendR3;
	}
    
    /**
     * Returns true, if the status column for items should be shown.<br>
     *  
     * @return true, if the status column for items should be shown
     *         false, else
     */     
    public boolean showStatusColumn() {
       return  !header.isDocumentTypeOrderTemplate() &&
               !(header.isDocumentTypeQuotation() && !header.isQuotationExtended()) ;
    }  
    
    /**
     * Returns the numbner of columns in the items table
     * 
     * @return String returns number of columns in the item table
     */
    public String getItemsColspan() {
        
        return (showStatusColumn())? "5" : "4";
    }
	
	/**
	 * Returns the items campaign description and campaign type description for the given index
	 * 
	 * @param intger idx the idx of the items campaign to look for
	 * @return String returns the items campaign description and campaign type description for the given index
	 */
	public String getItemCampaignDescs(int idx) {
		String descs = "";
		
        if (item != null && item.getAssignedCampaigns() != null && 
            item.getAssignedCampaigns().getCampaign(idx) != null) {
            String campGuid = item.getAssignedCampaigns().getCampaign(idx).getCampaignGUID().getIdAsString();
            String campTypeId = item.getAssignedCampaigns().getCampaign(idx).getCampaignTypeId();
            if (campGuid != null && campGuid != "") { // is the campaign known ? if not, do nothing
                descs = campDescs.get(campGuid).toString();
                if (campTypeDescs.get(campTypeId).toString().length() > 0) {
                    descs = descs + " (" + campTypeDescs.get(campTypeId).toString() + ")";
                }
            }
        }
		
		return descs;
	}
	
	/**
	 * Returns the header campaign description and campaign type description for the given index
	 * 
	 * @param intger idx the idx of the header campaign to look for
	 * @return String returns the header campaign description and campaign type description for the given index
	 */
	public String getHeaderCampaignDescs(int idx) {
		String descs = "";
		
		if (header != null && header.getAssignedCampaigns() != null && 
			header.getAssignedCampaigns().getCampaign(idx) != null) {
			String campGuid = header.getAssignedCampaigns().getCampaign(idx).getCampaignGUID().getIdAsString();
			String campTypeId = header.getAssignedCampaigns().getCampaign(idx).getCampaignTypeId();
			descs = campDescs.get(campGuid).toString();
			if (campTypeDescs.get(campTypeId).toString().length() > 0) {
				descs = descs + " (" + campTypeDescs.get(campTypeId).toString() + ")";
			}
		}
		
		return descs;
	}
    
    /**
     * Returns a flag, to indicate, if the Toggle Icon for multiple campaign 
     * information should be shown for the current item 
     * 
     * @return false always, should be overwritten by the subclasses if necessary
     */
    public boolean showCurrentItemToggleMultipleCampaignData() {
        
       return false;
    }
    
    /**
     * Returns the correct Value for the Item, wich is NetValueWOfreight 
     * for CRM backends and NetValue else.
     * 
     * @return the correct value for the item, depending on the backend
     */
    public String getItemValue() {

        if (isBackendR3() || isDocumentRecoverable) {
            return item.getNetValue();
        }
        else  {
            return item.getNetValueWOFreight();
        }
    }
    
    /**
     * Returns the correct Value for the header, wich is NetValueWOfreight 
     * for CRM backends and NetValue else.
     * 
     * @return the correct value for the header, depending on the backend
     */
    public String getHeaderValue() {

        if (isBackendR3() || isDocumentRecoverable) {
            return header.getNetValue();
        }
        else  {
            return header.getNetValueWOFreight();
        }
    }
	
	/**
	 * Returns true, if the system productId must be shown for the current item<br>
	 *  
	 * @return true, if the system productId must be shown for the current item
	 */
	public boolean showItemSysProductId() {
	   boolean showSysProdId = false;
	   
	   if (item != null &&
	       shop.isProductDeterminationInfoDisplayed() && 
	       item.getSystemProductId() != null &&
		   item.getSystemProductId().length() > 0 && 
		   item.getProduct() != null  && 
		   !item.getProduct().equals(item.getSystemProductId()) ) {
		   showSysProdId = true;
	   }

	   return  showSysProdId;
	} 
	
	/* (non-Javadoc)
	 * @see com.sap.isa.isacore.uiclass.SalesDocumentUI#determineSalesDocument()
	 */
	protected void determineSalesDocument() {
		if (mdoc != null) {
			if (mdoc.getDocument() instanceof SalesDocument) {
			    salesDoc = (SalesDocument) mdoc.getDocument();
			}
			else if (mdoc.getDocument() instanceof SalesDocumentStatus) {
				salesDocStat = (SalesDocumentStatus) mdoc.getDocument();
			}
		}
	}
	
	/**
	 * Returns the number of ShipTos<br>
	 *  
	 * @return the number of ShipTos
	 */
	public int getNumShipTos() {
		int num = 0;
		if (salesDoc != null) {
			num = salesDoc.getNumShipTos();
		}
		else {
			num = salesDocStat.getShipTos().length;
		}
		return num;
	}
	
	/**
	 * Returns the is externalToOrder flag of the salesDocument. <br>
	 *  
	 * @return is externalToOrder flag of the salesDocument
	 */
	public boolean isExternalToOrder() {
		boolean ext = true;
		
		if (salesDoc != null) {
			ext = salesDoc.isExternalToOrder();
		}
		return ext;
	}
	
    /**
     * Returns true, if items should be shown on the UI.
     *  
     * @return boolean true if items should be shown, 
     *                 false otherwise
     */
	public boolean showItems() {
        
		boolean showItems = true;
		
		if (salesDoc != null) {
            showItems = !salesDoc.isHeaderChangeInLargeDocument(noOfItemsThreshold) || salesDoc.getSelectedItemGuids().size() > 0;
		}

		return showItems;
	}
    
    /**
     * Returns a string containing all scheduline informations 
     *  
     * @return string containing all scheduline informations
     */
    public String getSchedulineData() {
        
       String sched = "";
       //String availString = "; " + WebUtil.translate(pageContext, "status.sales.detail.confDelDate", null) + ": ";  note 936716
       String onString = " " + WebUtil.translate(pageContext, "status.sales.detail.confDelDate", null) + " ";

       ArrayList aList = item.getScheduleLines();
       
       if (showItemATPInfo()) {
           for (int i = 0; i < aList.size(); i++) {
               Schedline scheduleLine = (Schedline) aList.get(i);
               sched = sched + scheduleLine.getCommittedQuantity() + onString + scheduleLine.getCommittedDate() + "; ";
           } 
       }
       
       if (sched.length() > 0) {
           sched = "; " + sched.substring(0, sched.length() - 2);   // note 936716
       }
       
       return sched;
    }
    	
    /**
     * Returns true, if validTo info should be shown <br>
     *  
     * @return true if valid to info should be shown
     */
    public boolean showValidTo() {
        return (docType.equals(DocumentListSelectorForm.QUOTATION)
                && (header.isStatusOpen() && !header.isQuotationExtended() || header.isStatusReleased() && header.isQuotationExtended()));
    }
   
    /**
     * Returns true, if the company partner function is reseller <br>
     *  
     * @return true if the company partner function is reseller
     *         false else
     */
    public boolean isCompPFReseller() {   
        return shop.getCompanyPartnerFunction().equalsIgnoreCase(PartnerFunctionData.RESELLER);
    }
    
    /**
     * Returns true, if confirmation was called for a document in change mode
     *         false, if it was called for a newly creted document.
     * 
     * @return true, if confirmation was called for a document in change mode
     *         false, if it was called for a newly creted document
     */
    public boolean isChangeMode() {
        return isChangeMode;
    }
    
    /**
     * Returns true, if the div-container "header-misc" should be displayed.
     * 
     * @return boolean
     */
    public boolean isHeaderMiscDisplayed() {
    	boolean disp = false;
		if ((header.getIncoTerms1Desc() != null && header.getIncoTerms1Desc().length() > 0) ||
		    (isHeaderCampaignListSet() && !isOciTransfer()) ||
		    (header.getRecallId() != null && header.getRecallId().length() > 0) ||
		    (header.getAssignedExtRefObjects() != null && !header.getAssignedExtRefObjects().isEmpty()) ||
		    (header.getAssignedExternalReferences() != null && !header.getAssignedExternalReferences().isEmpty())) {
		    	disp = true; 
		    }
    	return disp;
    }
    
    /**
     * Returns true, if the div-container "header-payment" should be displayed.
     * 
     * @return boolean
     */
    public boolean isPaymentDataDisplayed() {
    	boolean disp = false;
    	if (header.getPaymentData() != null &&
		   (isElementVisible("payment.type") ||
		    isElementVisible("payment.card.number") ||
		    isElementVisible("payment.one.card.number"))) {
    	    	disp = true;
    	    }
    	return disp;
    }

	/**
	 * Returns true, if the div-container "header-message" should be displayed.
	 * 
     * @return boolean
     */
    public boolean isHeaderMessageDisplayed() {
		boolean disp = false;
		if ((header.getText() != null && header.getText().getText().length() > 0) && !isOciTransfer()) {
			disp = true;
		}
		return disp;
	}
		
    /**
     * Return the property {@link #isCustomerDocument}. <br>
     * 
     * @return
     */
    public boolean isCustomerDocument() {
        return isCustomerDocument;
    }

    /**
     * Return the property {@link #isPickupFromVendor}. <br>
     * 
     * @return
     */
    public boolean isPickupFromVendor() {
        return isPickupFromVendor;
    }


	/**
	 * Return if the ship to information on header level should be displayed. <br>
	 * Only valid if {@link #isCustomerDocument} is <code>true</code>. 
	 * 
	 * @return
	 */
	public boolean showHeaderShipTo() {
		return isCustomerDocument && header.getShipTo() != null &&
			(!"".equals(header.getShipTo().getShortAddress()) || isPickupFromVendor);
			 
	}


	/**
	 * Return if the ship to information on item level should be displayed. <br>
	 * Only valid if {@link #isCustomerDocument} is <code>false</code>. 
	 * 
	 * @return
	 */
	public boolean showItemShipTo() {
		return !isCustomerDocument && item.getShipTo() != null &&
			!"".equals(item.getShipTo().getShortAddress()) ;
			 
	}


    /**
     * @return
     */
    public boolean isFreightValueAvailable() {
        return isFreightValueAvailable;
    }

    /**
     * @return
     */
    public boolean isGrossValueAvailable() {
        return isGrossValueAvailable;
    }

    /**
     * @return
     */
    public boolean isNetValueAvailable() {
        return isNetValueAvailable;
    }

    /**
     * @return
     */
    public boolean isTaxValueAvailable() {
        return isTaxValueAvailable;
    }

    /**
     * @param b
     */
    public void setFreightValueAvailable(boolean b) {
        isFreightValueAvailable = b;
    }

    /**
     * @param b
     */
    public void setGrossValueAvailable(boolean b) {
        isGrossValueAvailable = b;
    }

    /**
     * @param b
     */
    public void setNetValueAvailable(boolean b) {
        isNetValueAvailable = b;
    }

    /**
     * @param b
     */
    public void setTaxValueAvailable(boolean b) {
        isTaxValueAvailable = b;
    }
    
    /**
     * Retunrs the configInfo String if available for the current item
     * 
     * @return String the config info, if available else null
     */
    public String getItemConfigInfo() {
        String configInfo = null;
        if (item != null && configInfoMap != null) {
            configInfo = (String) configInfoMap.get(item.getTechKey().getIdAsString());
        }
        
        return configInfo;
    }
    
    /**
     * Retunrs the config info characteristics if available for the current item
     * 
     * @return ArrayList the config info characteristics, if available else null
     */
    public ArrayList getItemConfigInfoDetail() {
        ArrayList configInfoDetail = null;
        if (item != null && configInfoMap != null) {
            configInfoDetail = (ArrayList) configInfoDetailMap.get(item.getTechKey().getIdAsString());
        }
        
        return configInfoDetail;
    }

}
