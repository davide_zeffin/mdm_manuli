package com.sap.isa.isacore.uiclass.b2b.order;

import javax.servlet.jsp.PageContext;

import com.sap.isa.backend.boi.isacore.ConnectedDocumentItemData;
import com.sap.isa.backend.boi.isacore.order.DocumentListFilterData;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.ShipTo;
import com.sap.isa.businessobject.User;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.PartnerFunctionBase;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.ShipToSelection;
import com.sap.isa.payment.backend.boi.PaymentMethodData;
import com.sap.isa.payment.businessobject.PaymentBase;
import com.sap.isa.payment.businessobject.PaymentBaseType;
import com.sap.isa.user.permission.ECoActionPermission;


/**
 * UI class to handle the customer order status object. <br>
 *
 * @author SAP
 * @version 1.0
 **/
public class CustomerOrderStatusUI extends OrderStatusUI {

	static final private IsaLocation log 
		= IsaLocation.getInstance(CustomerOrderStatusUI.class.getName());

    public boolean showIPCpricing = false;
    public boolean showConfirmedDeliveryDate = false;


	public boolean isB2BSupported = false;

	public PaymentBase payment;
	public String  paymentKey = "";
	public boolean paytypeIsCard = false;

	public boolean isPickupFromVendor = false;

	public boolean hasDispOrderPermission = false;
	protected boolean hasChangeCustOrderPermission = false;
	protected boolean hasProcessCustOrderPermission = false;
	
	public ConnectedDocumentItemData connectedItem;

	protected ShipToSelection shipToSelection;


    /**
     * Constructor for OrderUI.
     *
     * @param request
     */
    public CustomerOrderStatusUI(PageContext pageContext) {
		initContext(pageContext);
    }

	/**
	 * Constructor for OrderUI.
	 *
	 * @param request
	 */
	public void initContext(PageContext pageContext) {
		
		super.initContext(pageContext);
       	
		showIPCpricing = shop.isPricingCondsAvailable();
		showConfirmedDeliveryDate = shop.isShowConfirmedDeliveryDateRequested();
       	
		determineAuthority();
        
		ShipTo shipTo = ((HeaderSalesDocument)header).getShipTo();
		if (shipTo != null) {
			BusinessPartner soldFromPartner = bom.createBUPAManager().getDefaultBusinessPartner(PartnerFunctionBase.SOLDFROM);
			isPickupFromVendor = shipTo.getId().equals(soldFromPartner.getId());     	
		}
 
		// Payment
		payment = (PaymentBase) ((HeaderSalesDocument)header).getPaymentData();
		if (payment.getPaymentMethods() != null && payment.getPaymentMethods().size() > 0) {
			TechKey payTypeKey = ( (PaymentMethodData) payment.getPaymentMethods().get(0)).getPayTypeTechKey();

			if (payTypeKey.equals(PaymentBaseType.INVOICE_TECHKEY)) {
				paymentKey = "b2b.payment.invoice";
			}
			else if (payTypeKey.equals(PaymentBaseType.COD_TECHKEY)) {
				paymentKey = "b2b.payment.cod";
			}
			else if (payTypeKey.equals(PaymentBaseType.CARD_TECHKEY)) {
				paymentKey = "b2b.payment.card";
				paytypeIsCard = true;
			}
		}

	}

	/**
	 * @see com.sap.isa.isacore.uiclass.b2b.order.OrderUI#setItem(com.sap.isa.businessobject.item.ItemSalesDoc)
	 */
	public void setItem(ItemSalesDoc item) {

		super.setItem(item);
        
		connectedItem = item.getSucessor();
	}


	/**
	 * Determine authority for the user. <br>
	 */
    protected void determineAuthority() {
        // get the user first
        User user = bom.getUser();
        
        //Check if user has required permissions
        String[] docs =
            new String[] {
                DocumentListFilterData.SALESDOCUMENT_TYPE_ORDER,
                DocumentListFilterData.SALESDOCUMENT_TYPE_HOSTORDER,
                DocumentListFilterData.SALESDOCUMENT_TYPE_HOSTORDER };
        String[] actions =
            new String[] {
                DocumentListFilterData.ACTION_READ,
                DocumentListFilterData.ACTION_PROCESS,
                DocumentListFilterData.ACTION_CHANGE };
        ECoActionPermission[] docPermissions = new ECoActionPermission[docs.length];
        for (int cnt = 0; cnt < docs.length; cnt++) {
            docPermissions[cnt] =
                new ECoActionPermission(docs[cnt], actions[cnt]);
        }
        
        try {
            Boolean perm[] = user.hasPermissions(docPermissions);
            hasDispOrderPermission = perm[0].booleanValue();
			hasProcessCustOrderPermission = perm[1].booleanValue();
			hasChangeCustOrderPermission = perm[2].booleanValue();
        }
        catch (CommunicationException ex) {
        	log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "Failed to connect to the backend", ex);
        } 
    }


	/**
	 * Return if the sold to inforamtion should be displayed . <br>
	 * 
	 * @return always <code>true</code>
	 * 
	 * 
	 * @see com.sap.isa.isacore.uiclass.SalesDocumentDataUI#isSoldToVisible()
	 */
	public boolean isSoldToVisible() {
		return true;
	}


	/**
	 * Returns if the "Processs" Button is available. <br>
	 * 
	 * @return <code>true</code> if the button should be displayed. <br>
	 */
	public boolean isProcessButtonAvailable() {
		return hasProcessCustOrderPermission;
	}


	/**
	 * Returns if the "Change Data" Button is available. <br>
	 * 
	 * @return <code>true</code> if the button should be displayed. <br>
	 */
	public boolean isChangeDataButtonAvailable() {
		return hasChangeCustOrderPermission;
	}

	
	/**
	 * Returns if the Collective order is the successor document. <br>
	 * 
     * @return <code>true</code> if the Collective order is the successor document. <br>
     */
    public boolean isCollectiveOrderActive() {
    	// get the editable document
		ManagedDocument manDoc = getDocumentHandler(userSessionData).getManagedDocument(DocumentState.TARGET_DOCUMENT);
		if (manDoc != null) {
		    if (this.connectedItem.getDocNumber().equals(manDoc.getDocNumber()) &&
		        manDoc.getDocType().equals(DocumentListFilterData.SALESDOCUMENT_TYPE_COLLECTIVE_ORDER)) {
		    	return true;
		    }
		}
		return false;
	}
}
