package com.sap.isa.isacore.uiclass.b2b.order;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.ArrayList;

import javax.servlet.jsp.PageContext;

import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.backend.boi.isacore.order.ItemBaseData;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.ConnectedDocument;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.ShipTo;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.item.ItemList;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.businessobject.order.Basket;
import com.sap.isa.businessobject.order.PartnerListEntry;
import com.sap.isa.businesspartner.action.GetPartnerHierarchyAction;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.JspUtil;
import com.sap.isa.core.util.MessageList;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.helpvalues.HelpValuesSearch;
import com.sap.isa.isacore.action.IsaCoreInitAction;
import com.sap.isa.isacore.action.ShopReadAction;
import com.sap.isa.isacore.action.order.MaintainBasketBaseAction;
import com.sap.isa.isacore.action.order.MaintainOrderBaseAction;
import com.sap.isa.isacore.uiclass.SalesDocumentDataUI;
import com.sap.isa.isacore.action.SalesDocumentParser;

/**
 * UI class to handle an <strong>editable</strong> sales document on a JSP. <br>
 * The class is the base for the basket and the order change JSP. <br>
 * 
 * <strong>Note</strong> A lot of information is available as public properties
 * of the class.
 *
 * @author SAP
 * @version 1.0
 **/
public class SalesDocumentUI extends SalesDocumentDataUI {
    
    /**
     * Name of the F4 Customer search.
     */
   public static final String HELP_VALUE_CUSTOMER_SEARCH = "Customer";
    

	/**
	 * Flag for the late descision scenario. <br>
	 */
	public boolean isLateDecision = false;
	
	/** 
	 * The business object manager isn't visible on the JSP. <br>
	 */	
	protected BusinessObjectManager bom;
	
	/** 
	 * The shop isn't visible on the JSP. <br>
	 */	
	protected Shop shop;
	
	/** 
	 * The sales document isn't visible on the JSP
	 */	
	protected SalesDocument salesDoc;

	/** 
	 * The switch for R/3 backend. <br>
	 */	
	protected boolean backendR3   = false;

	/** 
	 * The switch for R/3 backend with plug in
	 */	
	protected boolean backendR3PI = false;

	/** 
	 * Flag for auctions
	*/
	protected boolean isAuction = false;

	/**
	 * Flag for OCI scenario.
	 */
	protected boolean ociTransfer = false;

	/**
	 * Flag for extended quotation. <br>
	 */
	protected boolean quotationExtended = false;
	
	/**
	 * Keys for the send, confirm and cancel buttons and message for empty basket
	 */
	protected String sendKey = null;
	protected String confirmKey = null;
	protected String cancelKey = null;
	protected String alertBasketEmpty = null;	

	/**
	 * Switch, if the help values search for products should be available. <br>
	 */	
	protected boolean productValuesSearchAvailable = false;
    
    /**
     * No of the first erroneous item in the itemlist or 0 if 
     * there are no erroneous items
     */
    protected int firstErroneousPos = -1;
    
    /**
     * Flag, if ordering of non catalog products is allowed. <br>
     */
    protected boolean isOrderingOfNonCatalogProductsAllowed = true;  
	static final protected IsaLocation log = IsaLocation.getInstance(SalesDocumentUI.class.getName());
	
	/**
	 * Constructor for SalesDocumentUI.
	 *
	 * @param request
	 */
	public SalesDocumentUI(PageContext pageContext) {

		initContext(pageContext);	
	}
	
	/**
	 * Constructor for SalesDocumentUI.
	 *
	 * @param request
	 */
	public SalesDocumentUI() {
	}	
	
	public void initContext(PageContext pageContext) {	

		super.initContext(pageContext);
        
		// Check if oci transfer is required
		ociTransfer =
			(((IsaCoreInitAction.StartupParameter)userSessionData.
				getAttribute(IsaCoreInitAction.SC_STARTUP_PARAMETER)).
				getHookUrl().length() > 0);

		String shopBackend = shop.getBackend();

		backendR3PI = shopBackend.equals(Shop.BACKEND_R3_PI);

		backendR3 = shopBackend.equals(Shop.BACKEND_R3) || shopBackend.equals(Shop.BACKEND_R3_40) ||
							shopBackend.equals(Shop.BACKEND_R3_45) || shopBackend.equals(Shop.BACKEND_R3_46) ||
							backendR3PI;

		isAuction = (userSessionData.getAttribute("EAUCTION_PARAMID") != null) ||
					(request.getSession().getAttribute("EAUCTION_PARAMID") != null) ||
					(request.getAttribute("EAUCTION_PARAMID") != null);
					
		if ( isAuction == true ) {
			salesDoc.setChangeHeaderOnly(true);		
		}
					
		//if (shop.getCompanyPartnerFunction().equalsIgnoreCase(PartnerFunctionData.RESELLER)) {
		// 	compPFisReseller = true;
		//}

		isLateDecision = shop.isDocTypeLateDecision();		
		
		// initialize the product help values search.
		HelpValuesSearch productSearch = getHelpValuesSearch("Product");
		if (productSearch != null  && !shop.isInternalCatalogAvailable()) {
			productValuesSearchAvailable = true; 
			HelpValuesSearch.Parameter salesOrg = productSearch.getParameter("salesOrg");
			if (salesOrg != null) {
				salesOrg.setValue(shop.getSalesOrganisation());
			}
		}
        
        if(userSessionData.getAttribute(ShopReadAction.NON_CATALOG_PRODUCTS_ALLOWED) != null) {
            isOrderingOfNonCatalogProductsAllowed = ((Boolean) userSessionData.getAttribute(ShopReadAction.NON_CATALOG_PRODUCTS_ALLOWED)).booleanValue();
        }
	}

	/**
	  * Returns the bom.
	  * @return BusinessObjectManager
	  */
	public BusinessObjectManager getBom() {
		if (bom == null && userSessionData != null) {
			// get BOM
			bom = (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
		}	
		return bom;
	}

	/**
	 * Sets the Shop as SalesDocumentConfiguration. <br>
	 * See also {@link #salesDocConfiguration}.
	 */
	protected void setSalesDocConfiguration() {
		
		// check for missing context
		if (getBom() != null) {
			// now I can read the shop
			shop = bom.getShop();
			salesDocConfiguration = shop;
			quotationExtended = shop.isQuotationExtended();
		}		
	}

	/**
	 * Determine the {@link #processTypeDescription} property. <br>  
	 */
	protected void setProcessTypeInfo() {
    
		// get description of process type
		ResultData processTypes = null;
		
		processTypeDescription = "";
		if ("quotation".equals(docType) && shop.isQuotationExtended()) {
		   processTypes = (ResultData)userSessionData.getAttribute(ShopReadAction.QUOTATION_PROCESS_TYPES);
		   
		} else {
			if ("ncontract".equals(docType)) {
				processTypes = (ResultData)userSessionData.getAttribute(ShopReadAction.CONTRACT_NEGOTIATION_PROCESS_TYPES);  
			} else {	
		   		processTypes = (ResultData)userSessionData.getAttribute(ShopReadAction.PROCESS_TYPES);
			}
		}
		if (processTypes != null) {
			if (processTypes.first()) {
				do  {
				   if (processTypes.getString(1).equalsIgnoreCase(header.getProcessType())) {
					  if (processTypes.getNumRows() > 1) {
					  	// Display of description, if more than one process type exists
						  processTypeDescription = processTypes.getString(2); 
					  }
					  isServiceRecall = processTypes.getString(3).equalsIgnoreCase(HeaderData.DOCUMENT_USAGE_RECALL_ORDER);
					  determineNewPos();
					  break;
				   }
				} while(processTypes.next());
			}
		}
	}
    
	/**
	 * Determine the number of Process types for the order
	 * 
	 * @return number of Process types for the order
	 */
	public int getNumProcessTypesOrder() {
    	
		int noProcTypes = 0;
		
		if (shop.getProcessTypes() != null) {
			noProcTypes = shop.getProcessTypes().getNumRows();
		}
		
		return noProcTypes;
	}
    
	/**
	 * Determine the Process types for the order
	 * 
	 * @return the Process types for the order
	 */
	public String getProcessTypeOrder() {
    	
		int numRowsQuotation = 0;
		String processTypeOrder = null;
		ResultData processTypesOrder = shop.getProcessTypes();
		
		if (processTypesOrder != null) {
			numRowsQuotation =  processTypesOrder.getNumRows();
				if (numRowsQuotation == 1) {
				  processTypesOrder.first(); 
				  processTypeOrder = processTypesOrder.getString(1);
				}
		}
		
		return processTypeOrder;
	}
    
	/**
	 * Determine the number of Process types for the quotation
	 * 
	 * @return number of Process types for the quotation
	 */
	public int getNumProcessTypesQuotation() {
    	
		int noProcTypes = 0;
		
        if (shop.isQuotationExtended()) {
		if (shop.getQuotationProcessTypes() != null) {
			noProcTypes = shop.getQuotationProcessTypes().getNumRows();
		}
        }
        else {
            noProcTypes = getNumProcessTypesOrder();
        }
		
		return noProcTypes;
	}
    
    /**
     * Returns the String " selected" if the given newpos value was last selected br>
     *  
     * @return " selected" if the given shipCond is the header shipCond
     */
    public String getNewPosSelected(int newpos) {
        
        String selected = "";
        
        if (this.newpos == newpos) {
            selected = " selected=\"selected\"";
        } 
        
        return selected;
    }
	
	/**
	 * Determine the Process types for the quotation
	 * 
	 * @return the Process types for the quotation
	 */
	public String getProcessTypeQuotation() {
    	
		int numRowsQuotation = 0;
		String processTypeQuotation = null;
		ResultData processTypesQuotation = shop.getQuotationProcessTypes();
		
        if (shop.isQuotationExtended()) {
		if (processTypesQuotation != null) {
			numRowsQuotation =  processTypesQuotation.getNumRows();
				if (numRowsQuotation == 1) {
				  processTypesQuotation.first(); 
				  processTypeQuotation = processTypesQuotation.getString(1);
				}
		}
        }
        else {
            processTypeQuotation = getProcessTypeOrder();
        }
		
		return processTypeQuotation;
	}
	
	/**
	 * Determine the number of Process types for the contract
	 * 
	 * @return number of Process types for the quotacontracttion
	 */
	public int getNumProcessTypesContract() {
    	
		int noProcTypes = 0;
		
		if (shop.getContractNegotiationProcessTypes() != null) {
			noProcTypes = shop.getContractNegotiationProcessTypes().getNumRows();
		}
		
		return noProcTypes;
	}

	public void setEmptyItem() {
		line++;
		even_odd = alternator.next();
		itemCSSClass = "poslist-mainpos-"+ even_odd;
		item = null;
	}	

    /** 
     * Returns only the name of the catalog, not the catalog key (which is 
     * a concatenation of catalog name and variant name)
     */
	public String getCatalog() {
        
        // see also class ItemSalesDoc ->constructor-> ItemSalesDoc(BasketTransferItem transferItm)
        
        String retVal = shop.getCatalog();
        if (retVal != null) {
            int length = retVal.length();
            if (length > 30) {
                // wrong behaviour, catalog name and variant are concated
                // and stored both in the catalog property.
                retVal = shop.getCatalog().substring(0, 30).trim();
            }
        }
        
		return retVal;
	}
   
	/**
	 * Returns the item text.
	 * 
	 * @return the item text
	 */
	public String getItemText() {
		return item.getText().getText();
	}
		 
	/**
	 * Return Set the property {@link #backendR3}. <br>
	 * 
	 * @return {@link #backendR3}
	 * @depracted use UI coontroller instead.
	 */
	public boolean isBackendR3() {
		return backendR3;
	}
    
	/**
	 * Return Set the property {@link #}. <br>
	 * 
	 * @return
	 */
	public boolean isOciTransfer() {
		return ociTransfer;
	}

	 /**
	  * Returns the shop.
	  * @return Shop
	  */
	 public Shop getShop() {
		 return shop;
	 }

	/**
	 * Returns corresponding property from the sales document.<br>
	 *  
	 * @return 
	 */
	public boolean isFreightValueAvailable() {
		return salesDoc!=null?salesDoc.isFreightValueAvailable():false;
	}

	/**
	 * Returns corresponding property from the sales document.<br>
	 * 
	 * @return
	 */
	public boolean isGrossValueAvailable() {
		return salesDoc!=null?salesDoc.isGrossValueAvailable():false;
	}

	/**
	 * Returns corresponding property from the sales document.<br>
	 * 
	 * @return
	 */
	public boolean isNetValueAvailable() {
		return salesDoc!=null?salesDoc.isNetValueAvailable():false;
	}

	/**
	 * Returns corresponding property from the sales document.<br>
	 * 
	 * @return
	 */
	public boolean isTaxValueAvailable() {
		return salesDoc!=null?salesDoc.isTaxValueAvailable():false;
	}

	/**
	 * Create hidden fields to store the Business Partner in the header. <br>
	 * the Request parameter businessPartnerRole[], businessPartnerTechKey[] 
	 * and businessPartnerId[] will be filled. <br>
	 * The field are of the following form:
	 * <pre>
	 * &lt;input type="hidden" name="businessPartnerRole[1]" value="SOLDTO"&gt;
	 * &lt;input type="hidden" name="businessPartnerTechKey[1]" value="4X9234798792UZZU4536"&gt;
	 * &lt;input type="hidden" name="businessPartnerId[1]" value="4711"&gt;
	 * </pre>  
	 * 
	 * @return
	 */
	public String storeBusinessPartners() {
		StringBuffer ret = new StringBuffer(); 
		
		Iterator bpIt = header.getPartnerList().getList().entrySet().iterator();
		Map.Entry partnerEntry = null;
		ArrayList relevantPartnerFunctions = SalesDocumentParser.getRelevantPartnerFunctions(userSessionData);
		
		int bpCount = 1;

		while (bpIt.hasNext()) {
			partnerEntry = (Map.Entry) bpIt.next();
			
			if (!relevantPartnerFunctions.contains(partnerEntry.getKey())) {
			 ret.append("<input type=\"hidden\" name=\"businessPartnerRole[").append(bpCount).append("]\" value=\"").append(partnerEntry.getKey()).append("\"/>").append(CRLF)
				 .append("<input type=\"hidden\" name=\"businessPartnerTechKey[").append(bpCount).append("]\" value=\"").append(JspUtil.removeNull(((PartnerListEntry) partnerEntry.getValue()).getPartnerTechKey())).append("\"/>").append(CRLF)
				 .append("<input type=\"hidden\" name=\"businessPartnerId[").append(bpCount).append("]\" value=\"").append(JspUtil.encodeHtml(removeNull(((PartnerListEntry) partnerEntry.getValue()).getPartnerId()))).append("\"/>").append(CRLF);
				 
			  bpCount++;
			} 
		   } 		
			
		return ret.toString(); 	
	}

	/**
	 * Return the if only the header is changable only. <br>
	 * The flag is used for large documents.
	 *  
	 * @return boolean
	 */
	public boolean isHeaderChangeOnly() {
		return salesDoc.isChangeHeaderOnly();
	}
	
	/**
     * Returns true, if items should be shown on the UI.
     *  
     * @return boolean true if items should be shown, 
     *                 false otherwise
     */
    public boolean showItems() {
        return !salesDoc.isHeaderChangeInLargeDocument(noOfItemsThreshold) || salesDoc.getSelectedItemGuids().size() > 0;
    }
	
	/**
	 * Returns true if the document is a Basket
	 * 
	 * @return boolean true if the document is a Basket
	 */
	public boolean isBasket() {
        return (docType != null && docType.indexOf(MaintainBasketBaseAction.DOCTYPE_BASKET) == 0);
	}	

	/**
	 * Returns if the CUA icon should be displayed for the current item. <br>
	 * 
	 * @return <code>true</code> if CUA icon should be displayed.
	 */	
	public boolean isCUAAvailable() {
	   return shop.isCuaAvailable() && !item.getProductId().isInitial() 
			  && !isZeroProductId;
	}   

	/**
	 * Returns the key for the basket empty alert
	 * 
	 * @return key for the basket empty alert
	 */
	public String getAlertBasketEmpty() {
		return alertBasketEmpty;
	}
	
	/**
	 * Returns the key for the cancel button
	 * 
	 * @return key for the cancel button
	 */
	public String getCancelKey() {
		return cancelKey;
	}

	/**
	 * Returns the key for the confirm button
	 * 
	 * @return key for the confirm button
	 */
	public String getConfirmKey() {
		return confirmKey;
	}
	
	/**
	 * Returns the text on header level or an empty String if the header text is null
	 * 
	 * @return String text on header level or empty String
	 */
	public String getHeaderText(){
		return (header != null && header.getText() != null && header.getText().getText() != null) ? header.getText().getText():"";
	}

	/**
	 * Returns the key for the send button
	 * 
	 * @return key for the send button
	 */
	public String getSendKey() {
		return sendKey;
	}	
	
	/**
	 * Return HTML read only attribute, if the item is read only. <br>
	 * 
	 * @return <code>readonly="readonly"</code> if the item is read only. 
	 */
	public String getItemReadOnly() {

		return !isZeroProductId() ?"readonly=\"readonly\"":"";
	}

    /**
     * Returns the description for the reason of a product substitution for the 
     * given key which idenity the reason. <br>
     * 
     * @param key logical for the product substitution reason
     * @return description which stored in the shop. See  {@link Shop#getSubstitutionReasonDesc(String)}.
     * 
     * @see com.sap.isa.isacore.uiclass.SalesDocumentDataUI#getSubstitutionReasonDesc(java.lang.String)
     */
    protected String getSubstitutionReasonDesc(String key) {
		return shop.getSubstitutionReasonDesc(key);
	}

	/**
	 * Returns the replacement type of the current item. <br>
	 * 
	 * @return possible values are: <br>
	 * 			<code>productchangeable</code> <br>
	 *          <code>deletable</code>         <br>
	 *          <code>cancelable</code>        <br>
	 *          empty string. 
	 */
	public String getItemReplaceType() {

		String replaceType = "";

		if (item.isDeletable()) {
			replaceType = item.isProductChangeable() ? "productchangeable" : "deletable";
		}
		else if (item.isCancelable()) {
			replaceType = "cancelable";
		}

		return replaceType;
	}
	
	/**
	 * Returns the resource file key of back order items 
	 * @return resource file key  of backorder item
	 */
	public String getItemBackOrderKey() {

		String backOrderKey = "";

		if (item.isBackordered() ) {
			backOrderKey = "b2b.ordr.item.backorder";
		}

		return backOrderKey;
	}    
    
	/**
     * Returns the correct Value for the Item, wich is NetValueWOfreight 
     * for CRM backends and NetValue else.
     * 
     * @return the correct value for the item, depending on the backend
     */
    public String getItemValue() {

        if (isBackendR3() || (salesDoc != null && salesDoc.isDocumentRecoverable())) {
            return item.getNetValue();
        }
        else  {
            return item.getNetValueWOFreight();
        }
    } 
    
    /**
     * Returns the correct Value for the header, wich is NetValueWOfreight 
     * for CRM backends and NetValue else.
     * 
     * @return the correct value for the header, depending on the backend
     */
    public String getHeaderValue() {

        if (isBackendR3() || salesDoc.isDocumentRecoverable()) {
            return header.getNetValue();
        }
        else  {
            return header.getNetValueWOFreight();
        }
    }  
      
	/**
	 * Return if the sold to should be displayed on the page. <br>
	 * The sold to is diplay if either the shop property isSoldtoSelectable is set or
	 * if the sold to of the sales document is not the default sold to provided
	 * from the business object manager.
	 * Additional the sold to will be disable, if a sold to selection is displayed on
	 * create order page.  
	 * 
	 * @return <code>true</code>, if the sold to should be displayed on the 
	 *          JSP
	 */
	public boolean isSoldToVisible() {
				
		boolean isVisible = shop.isSoldtoSelectable();
		
		// check also if the soldto is not the default soldto
		if (!isVisible) {
			BusinessPartner defaultSoldTo =  bom.getBUPAManager().getDefaultBusinessPartner(PartnerFunctionData.SOLDTO);
			if (defaultSoldTo != null) {
				String soldToId = header.getPartnerId(PartnerFunctionData.SOLDTO);
				if (soldToId != null) {
					if (!soldToId.equals(defaultSoldTo.getId())) {
						isVisible = true;	
					} 
					else {
						// check, if a sold to selection is displayed on the create document page.
						if (shop.isBpHierarchyEnable()) {
							try {
								ResultData bp = GetPartnerHierarchyAction.getPartner(userSessionData, userSessionData.getMBOM());
								if (bp != null && bp.getNumRows() > 0) {
									isVisible = true;	
								}                            									 
							}
							catch (Exception e) {
								// log the exception 
								log.error(LogUtil.APPS_USER_INTERFACE, "Exception in getting partner", e);
							}
						}										 	
					}
				}
			}	
		}
				
		return isVisible;
	}

	/**
	 * Return if the sold to could be changed on the page. <br>
	 * The sold to is changable if the shop property isSoldtoSelectable is set to 
	 * <code>true</code>.
	 * 
	 * @return <code>true</code>, if the sold to could be changed on the 
	 *          JSP
	 */
	public boolean isSoldToChangeable() {
		return shop.isSoldtoSelectable();
	}
		
	/**
	 * Return if the catalog key should transfer as a request parameter. <br>
	 *  
	 * @return the inverison of the parameter 
	 *  {@link com.sap.isa.isacore.action.ShopReadAction.NON_CATALOG_PRODUCTS_ALLOWED} 
	 * from the user session data
	 */
	public boolean isTransferCatalogKey() {
		
		boolean orderingOfNonCatalogProductsAllowed = ((Boolean)userSessionData.getAttribute(ShopReadAction.NON_CATALOG_PRODUCTS_ALLOWED)).booleanValue();
		
		return !orderingOfNonCatalogProductsAllowed;
	}

	/**
	 * Determine the Messages which should be displayed. <br>
	 * The messages are taken from the request parameter 
	 * {@link com.sap.isa.isacore.action.order.MaintainOrderBaseAction.RC_MESSAGES}
	 * 
	 */
	protected void determineMessages() {
		this.docMessages = (MessageList) request.getAttribute(MaintainOrderBaseAction.RC_MESSAGES);
	}

	/**
	 * Determine the item list which should be displayed. <br>
	 * The items are taken from the request parameter 
	 * {@link com.sap.isa.isacore.action.order.MaintainOrderBaseAction.RC_ITEMS}
	 * 
	 */
	protected void determineItems() {
		this.items = (ItemList) request.getAttribute(MaintainOrderBaseAction.RC_ITEMS);
        
        for (int i = 0; i < items.size(); i++) {
            if (!items.get(i).isValid()) {
                firstErroneousPos = i;
                break; 
            }
        }	    
	}

	/**
	 * Determine the header from the page context.
	 * The header is taken from the request parameter 
	 * {@link com.sap.isa.isacore.action.order.MaintainOrderBaseAction.RC_HEADER}
	 */
	protected void determineHeader() {    	
		this.header = (HeaderSalesDocument) request.getAttribute(MaintainOrderBaseAction.RC_HEADER);        
	}

	/**
	 * Determine the number of ne positions to be displayes
	 * The value is taken from the session parameter <code>"newpos"</code>.
	 */
	protected void determineNewPos() {
        
		newpos = (isServiceRecall)? 0 : getInitialNewPos();
		if (userSessionData.getAttribute("newpos") != null &&
			((String)userSessionData.getAttribute("newpos")).length() > 0) {
			newpos = Integer.parseInt((String)userSessionData.getAttribute("newpos"));
		}
	}

    /**
     * Overwrites the method determineSalesDocument. <br>
     * 
     * The method determine the used sales document. The sales document will be
     * taken from the managed document. <br> 
     * 
     * @see com.sap.isa.isacore.uiclass.SalesDocumentBaseUI#determineSalesDocument()
     */
	protected void determineSalesDocument() {
		if (mdoc != null) {
			salesDoc = (SalesDocument) mdoc.getDocument();
		}
	}
	
    /**
     * Implement the abstract method getSoldTo. <br>
     * The method provide the business partner for the given business partner key. 
     * 
     * @param soldToKey technical key of the business partner. 
     * 
     * @return business partner object.
     * 
     * 
     * @see com.sap.isa.isacore.uiclass.SalesDocumentDataUI#getSoldTo(com.sap.isa.core.TechKey)
     */
	protected  BusinessPartner getSoldTo(TechKey soldToKey) { 
		return  bom.createBUPAManager().getBusinessPartner(soldToKey);
	}
		
	/**
	 * Set the header object and adjust all header dependent attributes. <br>
	 *  
	 * @param header
	 */
	protected void setHeader(HeaderSalesDocument header) {

		this.header = header;
	    
		if (header.getDocumentType() != null) {
			documentTypeKey = "status.sales.dt.".concat(header.getDocumentType());
		}
		else {
			if (salesDoc instanceof Basket) {
				documentTypeKey = "status.sales.dt.".concat(HeaderSalesDocument.DOCUMENT_TYPE_BASKET);
			}
		}
		documentStatusKey = "";

		quotationExtended = shop.isQuotationExtended();

		if (quotationExtended &&
				header.isDocumentTypeQuotation() &&
				("completed".equals(header.getStatus()) ||
		         "open".equals(header.getStatus()))) {
			if (!header.isQuotationExtended()) {
				documentStatusKey = "status.sales.status.l.".concat(header.getStatus());
			} else {
				documentStatusKey = "status.sales.status.x.".concat(header.getStatus());
			}	
		} 
		else {
			if (header.isDocumentTypeNegotiatedContract()) {
				documentStatusKey = "b2b.nct.st.".concat(header.getStatus());
			} else if (!header.isDocumentTypeOrderTemplate()) {
				if (header.getStatus() != null) {
					documentStatusKey = "status.sales.status.".concat(header.getStatus());
				}
			}
		}

		// isOrderDownloadRequired =  header.isDocumentTypeOrder();
	    
	}
	
	
	
	
	
	
	
	
	
	/**
	 * Returns the String " selected" if the given shipTo is the header ShipTo.<br>
	 *  
	 * @return " selected" if the given shipTo is the header ShipTo
	 */
	public String getShipToSelected(ShipTo shipTo) {
		
		String selected = "";
		
		if (shipTo.equals(header.getShipTo()) && shipTo.getShortAddress().equals(header.getShipTo().getShortAddress())) {
			   selected = " selected=\"selected\"";
		} 
		
		return selected;
	}
	
	/**
	 * Returns the String " selected" if the given shipCond is the header ShipCond.<br>
	 *  
	 * @return " selected" if the given shipCond is the header shipCond
	 */
	public String getShipCondSelected(String shipCond) {
		
		String selected = "";
		
		if (shipCond.equals(header.getShipCond())) {
			   selected = " selected";
		} 
		
		return selected;
	}
    
    /**
     * Returns the Colspan value for the header details table for all entries despite campaigns<br>
     *  
     * @return the Colspan value for the header details table for all entries despite campaigns
     */
    public int getNonCampHeaderDetailColspan() {
        return 3;
    }
	
	/**
	 * Returns the String " selected" if the given Unit is the unit of the currentItem.<br>
	 *  
	 * @return " selected" if the given Unit is the unit of the currentItem
	 */
	public String getItemUnitSelected(String unit) {
		
		String selected = "";
		
		if (item != null && item.getUnit()!= null && item.getUnit().equals(unit)) {
			   selected = " selected=\"selected\"";
		} 
		
		return selected;
	}
	
	/**
	 * Returns the String " selected" if the given shipTo is the shipTo of the currentItem.<br>
	 *  
	 * @return " selected" if the given shipTo is the shipTo of the currentItem
	 */
	public String getItemShipToSelected(ShipTo shipTo) {
		
		String selected = "";
		if (item.getShipTo() != null) {		
			if (shipTo.equals(item.getShipTo()) && shipTo.getShortAddress().equals(item.getShipTo().getShortAddress())) {
			   selected = " selected=\"selected\"";
			}
		} else {
			if (shipTo.equals(header.getShipTo()) && shipTo.getShortAddress().equals(header.getShipTo().getShortAddress())) {
				selected = " selected=\"selected\"";
			}
		}
		
		return selected;
	}
	
	/**
	 * @return Returns if the hosted order management is activated.
	 */
	public boolean isHomActivated() {
		return shop.isHomActivated();
	}
		
	/**
	 * @return Returns ture if we are in the auction scenario
	 */
	public boolean isAuction() {
		return isAuction;
	}
    
	/**
	 * @return Returns ture if campaign related fields should be always shown
	 */
	public boolean isManualCampaignEntryAllowed() {
		return shop.isManualCampainEntryAllowed();
	}
    
    /**
     * @return Returns ture if multiple campaigns are allowed
     */
    public boolean isMultipleCampaignsAllowed() {
        return shop.isMultipleCampaignsAllowed();
    }
	
	/**
	 * Returns the header campaign description and campaign type description for the given index
	 * 
	 * @param intger idx the idx of the header campaign to look for
	 * @return String returns the header campaign description and campaign type description for the given index
	 */
	public String getHeaderCampaignDescs(int idx) {
		String descs = "";
		
		if (header != null && header.getAssignedCampaigns() != null && 
			header.getAssignedCampaigns().getCampaign(idx) != null) {
			String campGuid = header.getAssignedCampaigns().getCampaign(idx).getCampaignGUID().getIdAsString();
			String campTypeId = header.getAssignedCampaigns().getCampaign(idx).getCampaignTypeId();
			descs = (String) salesDoc.getCampaignDescriptions().get(campGuid);
			if (salesDoc.getCampaignTypeDescriptions().get(campTypeId) != null &&
                salesDoc.getCampaignTypeDescriptions().get(campTypeId).toString().length() > 0) {
				descs = descs + " (" + salesDoc.getCampaignTypeDescriptions().get(campTypeId).toString() + ")";
			}
		}
		
		return descs;
	}
    
	/**
	 * Returns the items campaign description and campaign type description for the given index
	 * 
	 * @param intger idx the idx of the items campaign to look for
	 * @return String returns the items campaign description and campaign type description for the given index
	 */
	public String getItemCampaignDescs(int idx) {
		String descs = "";
        
		if (item != null && item.getAssignedCampaigns() != null && 
			item.getAssignedCampaigns().getCampaign(idx) != null) {
			String campGuid = item.getAssignedCampaigns().getCampaign(idx).getCampaignGUID().getIdAsString();
			String campTypeId = item.getAssignedCampaigns().getCampaign(idx).getCampaignTypeId();
			descs = (String) salesDoc.getCampaignDescriptions().get(campGuid);
			if (salesDoc.getCampaignTypeDescriptions().get(campTypeId) != null &&
                salesDoc.getCampaignTypeDescriptions().get(campTypeId).toString().length() > 0) {
				descs = descs + " (" + salesDoc.getCampaignTypeDescriptions().get(campTypeId).toString() + ")";
			}			
		}
        
		return descs;
	}
    
    /**
     * Returns the number of campaigns entries to be shown for the item
     * 
     * @return int the number of campaigns to be shown for the item
     */
    public int getDisplayNoItemCampaignEntries() {
        
        int noDispCampaings = 0;
        
        if (shop.isManualCampainEntryAllowed()) {
            noDispCampaings = 5;
        }
        
        if (item != null && item.getAssignedCampaigns() != null) {
            noDispCampaings = noDispCampaings + item.getAssignedCampaigns().size();
        }
        
        return noDispCampaings;
    }
	
	/**
	 * Returns true if the campaign list on header level is not empty
	 * 
	 * @return true if the campaign list on header level is not empty
	 */
	public boolean isHeaderCampaignListSet() {
		boolean isSet = false;
		
		if (header != null && header.getAssignedCampaigns() != null && 
			header.getAssignedCampaigns().size() > 0) {
				isSet = true;
		}
		
		return isSet;
	}
     
    /**
     * Returns the number of campaigns entries to be shown for the header
     * 
     * @return int the number of campaigns entries to be shown for the header
     */
    public int getDisplayNoHeaderCampaignEntries() {
        int noDispCampaings = 0;
        
        if (shop.isManualCampainEntryAllowed()) {
            noDispCampaings = 5;
        }
        
        if (header != null && header.getAssignedCampaigns() != null) {
            noDispCampaings = noDispCampaings + header.getAssignedCampaigns().size();
        }
        
        return noDispCampaings;
    }
	
	/**
	 * Returns 1 if alternativ product information for the current item must be shown, 0 else
	 * 
	 * @return integer 1 if alternativ product information for the current item must be shown, 0 else
	 */
	public int rowSpanItemSystProductId() {
		int rowSpan = 0;
		
		if(item != null && shop.isProductDeterminationInfoDisplayed() && item.getSystemProductId() != null &&
		   item.getSystemProductId().length() > 0 && item.getProduct() != null  &&
		   !item.getProduct().equals(item.getSystemProductId())) {
			rowSpan = 1;
		}
		return rowSpan;
	}
	
	/**
	 * Returns the processtype description or an empty string if not available
	 * 
	 * @return String the processtype description or an empty string if not available
	 */
	public String getProcessTypeDesc() {	
		return (processTypeDescription == null) ? "" : processTypeDescription;
	}
    
    /**
     * Return the property {@link #productValuesSearchAvailable}. <br>
     * 
     * @return
     */
    public boolean isProductValuesSearchAvailable() {
        return productValuesSearchAvailable;
    }
    
    /**
     * Returns true if ordering of non catalog products is allowed. <br>
     * 
     * @return true if ordering of non catalog products is allowed
     *         false else
     */
    public boolean isOrderingOfNonCatalogProductsAllowed() {
        return isOrderingOfNonCatalogProductsAllowed;
    }
    
    /**
     * Returns the value of the element in the UserSessionData
     * 
     * @param element 	for which the value is obtained from UserSessionData
     * 
     * @return result	actual value of the element is returned
     */
	public String getDataFromUserSessionElement(String element){
		
		String result = "";
		if (userSessionData.getAttribute(element) != null){
			result = userSessionData.getAttribute(element).toString();
		}
		return result;
	}
    
	/**
	 * Returns the value that enable the grid product related  
	 * checkboxes to be set or not.
	 * 
	 * @param element for which the value is obtained from UserSessionData
	 * 
	 * @return result <code>checked</code> if checkbox is to be set
	 */
	public String setCheckBoxToMarked(String element){
		
		String result = "";
		String elementValue = getDataFromUserSessionElement(element);
		if (elementValue.length() > 0)
			result = "checked";
		return result;
	}

    /**
     * Write out JScript onClick event depending on the existens of a referenced doc.
     * @return String like 'onclick="documentDisplay('dockey', 'docno', 'doctype');"
     */
    public String writeHeaderRefOnClickEvent(ConnectedDocument conDoc) {
        return "";    
    }
 
    /**
     * Returns the number of the first erroneous positions in the itemlist
     * 
     * @return number of the first erroneous positions in the itemlist
     *         -1 else
     */
    public int getFirstErroneousPos() {
        return firstErroneousPos;
    }
    

    /**
     * Indicates, if the are positions in the itemlist with errors
     * 
     * @return true, if there are erroneous positions in the itemlist
     *         false else
     */
    public boolean hasErroneousPos() {
        return firstErroneousPos != -1;
    }  

	/**
	 * Check if there is a header text.
	 * 
	 * @return true, if there is a header text
	 *         false, otherwise
	 */
	public boolean hasHeaderText() {
		return header.hasText();
	}
    
	/**
	 * Check if there is a iem text.
	 * 
	 * @return true, if there is a item text
	 *         false, otherwise
	 */
	public boolean hasItemText() {
		return item.hasText();
	}
    
    /**
     * Returns true, if incoterms information should be shown,
     * false else
     * 
     * @return true, if incoterms information should be shown
     *         false, else
     */
    public boolean showIncoterms() {
        
       return !isBackendR3() && !isSoldToChangeable();
    }
    
    /**
     * Returns true, if campaign information should be shown on header level,
     * false else
     * 
     * @return true, if campaign information should be shown on header level
     *         false, else
     */
    public boolean showCampaignData() {
        
       return isElementVisible("order.campaign") && !isServiceRecall() && (isManualCampaignEntryAllowed() || isHeaderCampaignListSet());
    }
    
	/**
	 * Returns true, if multiple campaign information should be shown on header level,
	 * false else
	 * 
	 * @return true, if the Toggle Icon for multiple campaigns should be shown on header level
	 *         false, else
	 */
	public boolean showHeaderToggleMultipleCampaignData() {
        
	   return showCampaignData() && 
              ((shop.isMultipleCampaignsAllowed() && shop.isManualCampainEntryAllowed() && isElementEnabled("order.campaign")) ||  
                getNoHeaderCampaignEntries() > 1);
	}
    
	/**
	 * Returns true, if campaign information should be shown for the current item,
	 * false else
	 * 
	 * @return true, if campaign information should be shown for the current item
	 *         false, else
	 */
	public boolean showCurrentItemCampaignData() {
        
	   return isElementVisible("order.item.campaign") && !isServiceRecall() && ((isItemMainItem() && isManualCampaignEntryAllowed()) || isItemCampaignListSet());
	}
	
	/**
	 * Returns a flag, to indicate, if the Toggle Icon for multiple campaigns 
     * should be shown for the current item 
	 * 
	 * @return true, if toggle icon for multiple campaign information should be 
     *               shown for the current item
	 *         false, else
	 */
	public boolean showCurrentItemToggleMultipleCampaignData() {
        
	   return  showCurrentItemCampaignData() && 
               ((shop.isMultipleCampaignsAllowed() && shop.isManualCampainEntryAllowed() && isElementEnabled("order.item.campaign", item.getTechKey().getIdAsString())) ||  
                 getNoItemCampaignEntries() > 1);
	}
    
    /**
     * Returns true, if extref object information should be shown on header level,
     * false else
     * 
     * @return true, if extref object information should be shown on header level
     *         false, else
     */
    public boolean showExtRefData() {
    
       return (header.getAssignedExternalReferences() != null && header.getAssignedExternalReferences().size() > 0)
              || (header.getExtRefObjectType() != null && header.getExtRefObjectType().length() > 0);
    }
    
    /**
     * Returns the Mapping needed for the Customer Help Value search, to set the selected 
     * customer id on in the right filed in the welcome.jsp
     *
     * @param fieldName name of the form field the select customer id should be set in
     * @return HashMap Mapping from the Help Values Customer Search field to the fieldName
     */
    public Map getCustomerSearchMapping(String fieldName) {
        HelpValuesSearch helpValuesSearch = getHelpValuesSearch(HELP_VALUE_CUSTOMER_SEARCH);
        Map parameterMapping = new HashMap();
        if (helpValuesSearch!=null) {
            parameterMapping.put(helpValuesSearch.getParameter().getName(),fieldName);
        }
       return parameterMapping;
    }
    
	/**
	 * returns <code>true</code> if the itemlist contains at leats one rate plan or 
	 * combined rate plan product (Telco)
	 */
	public boolean showContractStartDate() {
        
	   boolean showStartDate = false;
	   ItemSalesDoc item = null;
       
	   for (int i = 0; i < items.size(); i++) {
		   item = items.get(i);
		   if (item.getProductRole() != null && item.getProductRole().length() > 0 && 
			   (item.getProductRole().equalsIgnoreCase(ItemBaseData.PRODUCT_ROLE_COMBINED_RATE_PLAN) || 
				item.getProductRole().equalsIgnoreCase(ItemBaseData.PRODUCT_ROLE_RATE_PLAN))) {
			   showStartDate = true;
			   break;
		   }
	   }

	   return showStartDate;
	}     
        
    /**
     * @return
     */
    public SalesDocument getSalesDoc() {
        return salesDoc;
    }

}
