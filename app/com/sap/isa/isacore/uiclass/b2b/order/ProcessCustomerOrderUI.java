package com.sap.isa.isacore.uiclass.b2b.order;

import java.util.Iterator;

import javax.servlet.jsp.PageContext;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.ExtendedStatusListEntry;
import com.sap.isa.businessobject.Search;
import com.sap.isa.businessobject.SearchResult;
import com.sap.isa.businessobject.ShipTo;
import com.sap.isa.businessobject.UserStatusProfileSearchCommand;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.businessobject.order.OrderChange;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.isacore.action.order.hom.DarkMaintainShowCollectiveOrderAction;


/**
 * The class ProcessCustomerOrderUI.java .... .
 *
 * @author SAP
 * @version 1.0
 **/
public class ProcessCustomerOrderUI extends CustomerOrderUI {

	public static final String PROCESS_REORDER = "PCHS";
    public static final String PROCESS_FORWARD = "FWRD";
    
    public static final String RC_USERSTATUSPROFILE = "userstatusprofile";

    public OrderChange collectiveOrder = null;
    public ShipTo[] shipTosCollectiveOrder;
    public String shipToCustomerIndex;
    public ItemSalesDoc collectiveOrderItem;
    public String collectiveOrderItemMessage = null;

    public boolean showcollectiveOrder = false;
    public boolean showcollectiveOrderIcon = false;
    public boolean showForwardIcon = false;
    public boolean showReorderIcon = false;
    public boolean isReorder = true;
	public boolean isSubItem = false;


    protected ExtendedStatusListEntry currentStatus = null;
    protected ExtendedStatusListEntry reorderStatus = null;
    protected ExtendedStatusListEntry forwardStatus = null;     


    /**
     * Constructor for ProcessCustomerOrderUI.
     * @param pageContext
     */
    public ProcessCustomerOrderUI(PageContext pageContext) {
        
        super(pageContext);

        collectiveOrder = (OrderChange) request.getAttribute(DarkMaintainShowCollectiveOrderAction.RC_COLLECTIVE_ORDER);

        shipTosCollectiveOrder = (ShipTo[])request.getAttribute(DarkMaintainShowCollectiveOrderAction.RC_SHIPTOS);

        shipToCustomerIndex = (String)request.getAttribute(DarkMaintainShowCollectiveOrderAction.RC_SHIPTO_CUSTOMER);
   	}
        

    /**
     * @see com.sap.isa.isacore.uiclass.b2b.order.OrderUI#setItem(com.sap.isa.businessobject.item.ItemSalesDoc)
     */
    public void setItem(ItemSalesDoc item) {

        super.setItem(item);
        
        collectiveOrderItem = null;
        
        collectiveOrderItemMessage = "";
        
        showcollectiveOrder = false;
        showcollectiveOrderIcon = false;
        showForwardIcon = false;
        showReorderIcon = false;
        isReorder = true;  
		isSubItem = itemHierarchy.isSubItem(item);              
        
        if (connectedItem == null) {
         	/* use item data for collective order */
          	collectiveOrderItem = item;
          	// System.out.println("no item connected");
          
          	// check if "reorder" and "forward" are allowed for this item 
          	Iterator extStatus = item.getExtendedStatus().getStatusIterator();
          
          	while (extStatus.hasNext()) {
              	ExtendedStatusListEntry element = (ExtendedStatusListEntry) extStatus.next();
              	if (element.getBusinessProcess().equals(PROCESS_REORDER)) {
                  showReorderIcon = true;
                  reorderStatus = element;      
              	}
              	if (element.getBusinessProcess().equals(PROCESS_FORWARD) && !isCollectionFromVendor) {
                  	showForwardIcon = true; 
                  	forwardStatus = element;   
              	}
              	if (element.isCurrentStatus()) {
                    currentStatus = element;    	
              	}
          	}
          
          	if (showForwardIcon || showReorderIcon) {
          		showcollectiveOrderIcon = true;
          	}
        }
        else {
            /*
            System.out.println("documentKey: " + connectedItem.getDocumentKey().getIdAsString());
            System.out.println("ui.collectiveOrder: " + collectiveOrder.getTechKey().getIdAsString());
            */

            currentStatus = (ExtendedStatusListEntry)(item.getExtendedStatus().getCurrentStatus());
            
            if (currentStatus != null && currentStatus.getBusinessProcess().equals(PROCESS_FORWARD)) {
                isReorder = false;
            }
			
            // connected item is in the actual collective order
            if (connectedItem.getDocumentKey().equals(collectiveOrder.getTechKey())){
              	collectiveOrderItem = collectiveOrder.getItem(connectedItem.getTechKey()); /* Eventuell gibt es diese Routine noch nicht */
              	if (collectiveOrderItem.getReqDeliveryDate().length() == 0 && collectiveOrderItem.getConfirmedDeliveryDate().length() >0) {
              		collectiveOrderItem.setReqDeliveryDate(collectiveOrderItem.getConfirmedDeliveryDate());
              	}
                showcollectiveOrder = true;
              	// System.out.println("collective order item connected");
                collectiveOrderItemMessage = getMessageString(collectiveOrderItem.getMessageList()); 
            }    
        }

    }
    
    public boolean isAllowedStatus(ExtendedStatusListEntry status) {
         
         String process = status.getBusinessProcess();
         
         if (!showcollectiveOrder && connectedItem != null) {
             return true;
         } else {
             return !(process.equals(PROCESS_FORWARD) || process.equals(PROCESS_REORDER));
         }
         
    }


	public String getCurrentStatusDescription() {
	    
        if (currentStatus != null) {
            return currentStatus.getDescription();
        }    
        
		return "";    	    
	}


    public String getCurrentStatusValue() {
	    
        if (currentStatus != null) {
            return getExtStatusValue(currentStatus);
        }    
        
        return "";    	    
    }

    public String getReorderStatusDescription() {
	    
        if (reorderStatus != null) {
            return reorderStatus.getDescription();
        }    
        
        return "";    	    
    }


    public String getReorderStatusValue() {
	    
        if (reorderStatus != null) {
            return getExtStatusValue(reorderStatus);
        }    
        
        return "";    	    
    }


    public String getForwardStatusDescription() {
	    
        if (forwardStatus != null) {
            return forwardStatus.getDescription();
        }    
        
        return "";    	    
    }


    public String getForwardStatusValue() {
	    
        if (forwardStatus != null) {
            return getExtStatusValue(forwardStatus);
        }    
        
        return "";    	    
    }


	/**
	 * Stores an iterator over all user status in the request context. 
	 * See {@link #RC_USERSTATUSPROFILE}.
	 */
	public void setExtendStatusIterator () {
		// Get User Status profile for given Procedure and add to request
		Search search = bom.createSearch();
		SearchResult resultStatusProfile = null;
		try {
            resultStatusProfile = search.performSearch(new UserStatusProfileSearchCommand(shop.getLanguage()));
        }
        catch (CommunicationException e) {
        	log.debug(e.getMessage());
        }
		ResultData result =  new ResultData(resultStatusProfile.getTable());
		request.setAttribute(RC_USERSTATUSPROFILE, result);
		
	}

}
