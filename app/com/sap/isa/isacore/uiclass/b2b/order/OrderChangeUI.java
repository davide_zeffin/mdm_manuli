/*****************************************************************************
    Class:        OrderChangeUI
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      02.02.2004
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.isacore.uiclass.b2b.order;

import javax.servlet.jsp.PageContext;

import com.sap.isa.backend.boi.isacore.ConnectedDocumentItemData;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.item.ItemSalesDoc;

/**
 * The class OrderChangeUI . <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class OrderChangeUI extends SalesDocumentUI {
	
	/**
	 * @param pageContext
	 */
	public OrderChangeUI(PageContext pageContext) {
		super(pageContext);
		determineKeyValues();
		// TODO Auto-generated constructor stub
	}

	public ConnectedDocumentItemData connectedItem;

   
	/**
	 * Determines the keys for the send, confirm and cancel buttons
	 */
	protected void determineKeyValues() {
		
		confirmKey = "b2b.order.change.confirmtext";
		cancelKey = "b2b.order.change.cancel";

		alertBasketEmpty = "b2b.ordr.alr." + docType + ".empty";

		sendKey = "b2b.order.change.confirmtext";
	}

	/**
	 * Returns the Colspan value for the items table<br>
	 *  
	 * @return the Colspan value for the items table
	 */
	public int getItemsColspan() {
		return (isContract) ? 14 : 13;
	}
	
		
	/**
	 * Returns true if the document is a collective order
	 * 
	 * @return boolean true if the document is a collective order
	 */
	public boolean isCollectiveOrder() {
		return HeaderSalesDocument.DOCUMENT_TYPE_COLLECTIVE_ORDER.equals(getDocType());
	}
	
    /**
     * Overwrites the method . <br>
     * 
     * @param item
     * 
     * 
     * @see com.sap.isa.isacore.uiclass.b2b.order.OrderUI#setItem(com.sap.isa.businessobject.item.ItemSalesDoc)
     */
    public void setItem(ItemSalesDoc item) {
    	
        super.setItem(item);
        
		connectedItem = item.getPredecessor();
    }


}
