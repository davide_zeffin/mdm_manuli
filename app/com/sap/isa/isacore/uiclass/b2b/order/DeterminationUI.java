/*****************************************************************************
    Class:        DeterminationUI
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      12.03.2004
    Version:      1.0

    $Revision: #1$
    $Date: 2004/03/12 $ 
*****************************************************************************/
package com.sap.isa.isacore.uiclass.b2b.order;

import java.util.HashSet;

import javax.servlet.jsp.PageContext;

import com.sap.isa.businessobject.ManagedDocumentDetermination;
import com.sap.isa.businessobject.item.AlternativProduct;
import com.sap.isa.isacore.action.ShowDeterminationAction;
import com.sap.isa.isacore.action.order.MaintainBasketBaseAction;

/**
 * The class DeterminationUI . <br>
 *
 * @author  SAP AG
 * @version 1.0
 *
 */
public class DeterminationUI extends SalesDocumentUI {
	
	protected String shiptoAddr = null;
    
	protected String shipCondDesc = null;
    
	protected HashSet selectedNone = new HashSet();
    
	protected String docMode = null;
	

	/**
	 * @param pageContext
	 */
	public DeterminationUI(PageContext pageContext) {
		
		super(pageContext);
		
		docType = (String) userSessionData.getAttribute(MaintainBasketBaseAction.SC_DOCTYPE);
		shiptoAddr = (String) request.getAttribute(ShowDeterminationAction.RC_SHIPTO_ADR);
		shipCondDesc = (String) request.getAttribute(ShowDeterminationAction.RC_SHIPCOND_DESC);
		selectedNone = (HashSet) request.getAttribute(ShowDeterminationAction.RC_SELECTED_NONE);
		docMode = (String) request.getAttribute(ShowDeterminationAction.RC_DOC_MODE);
	}	
	
	/**
	 * Determine the number of ne positions to be displayes
	 * The value is taken from the session parameter <code>"newpos"</code>.
	 */
	protected void determineNewPos() {
	}
	
	/**
	 * Returns true if the documentype is quotation
	 */
	public boolean isQuotation() {
		return MaintainBasketBaseAction.DOCTYPE_QUOTATION.equals(docType);
	}
	
	/**
	 * Returns true if the documentype is quotation
	 */
	public boolean isOrderTemplate() {
		return (docType != null && docType.indexOf(MaintainBasketBaseAction.DOCTYPE_ORDERTEMPLATE) == 0);
	}

	/**
	 * Returns true if the documentype is quotation
	 */
	public boolean isBasket() {
        return (docType != null && docType.indexOf(MaintainBasketBaseAction.DOCTYPE_BASKET) == 0);
	}
	
	/**
	 * Returns true if the current item needs manual determination
	 */
	public boolean isDeterminationNecessary() {	
		return item != null &&
			   (item.isProductAliasAvailable() || item.isDeterminedCampaignAvailable()) &&
			   (item.getParentId() == null || item.getParentId().getIdAsString().length() == 0) &&
			   !selectedNone.contains(item.getTechKey().getIdAsString())
			   ;
	}
	
	/**
	 * Returns true if the current item eithet needs manual product determination or product substitution
	 */
	public boolean isProdDetSubst() {	
		return item != null && item.isProductAliasAvailable();				   
	}
	
	/**
	 * Returns true if the current item needs manual product determination
	 */
	public boolean isProdDet() {	
		return isProdDetSubst() && item.getAlternativProductList().isDeterminationProductList();				   
	}
	
	/**
	 * Returns true if the current item needs manual product substitution
	 */
	public boolean isProdSubst() {	
			return isProdDetSubst() && item.getAlternativProductList().isSubstituteProductList();	   
	}
	/**
	 * Returns true if the current item needs manual campaign substitution
	 */
	public boolean isCampDet() {	
			return item != null && item.isDeterminedCampaignAvailable();	   
	}
	
	/**
	 * Returns size of the list that needs determination for the current item
	 */
	public int getDetListSize() {
		int size = 0;
		if (isProdDetSubst()) {
			size = item.getAlternativProductList().size(); 	
		}
		else if (this.isCampDet()) {
			size = item.getDeterminedCampaigns().size();
		}
		return size;	   
	}
	
	/**
	 * Returns size of the list that needs determination for the current item
	 * 
	 * returns the alternative product 
	 */
	public String getAltProdSystemProductId(int idx) {
		String id = "";
		
		if (isProdSubst()) {
			id = item.getAlternativProductList().getAlternativProduct(idx).getSystemProductId(); 	
		}

		return id;	   
	}
	
	/**
	 * Return the document mode
	 * 
	 * @return String document mode
	 */
	public String getDocMode() {
		return docMode;
	}

	/**
	 * Returns the list thhat holds informations about positions where a choice 
	 * was made, to make no choice 
	 * 
	 * @return HasSet of positions wher the option no choice wa selected
	 */
	public HashSet getSelectedNone() {
		return selectedNone;
	}

	/**
	 * Returns the shipping condition desc
	 * 
	 * @return String the shipping condition desc
	 */
	public String getShipCondDesc() {
		return shipCondDesc;
	}

	/**
	 * Returns the shipto address
	 * 
	 * @return String the shipto address
	 */
	public String getShiptoAddr() {
		return shiptoAddr;
	}
	
	/**
	 * Returns the substitution reason description for the entry j of the altProductList of the
	 * current item
	 * 
	 * @return String the substitution reason description for the alternative product 
	 */
	public String getSubstitutionReasonDesc(int j) {
		String desc = "";
		
		if (isProdSubst()) {
			AlternativProduct altProd = (AlternativProduct) item.getAlternativProductList().getList().get(j);
			desc = shop.getSubstitutionReasonDesc(altProd.getSubstitutionReasonId());
		}
		return desc;
	}
	
	/**
	 * Returns the description for the entry j of the altProductList of the
	 * current item
	 * 
	 * @return String the description for the alternative product 
	 */
	public String getDescription(int j) {
		String desc = "";
		
		if (isProdSubst()) {
			AlternativProduct altProd = (AlternativProduct) item.getAlternativProductList().getList().get(j);
			desc = altProd.getDescription();
		}
		return desc;
	}
	
	/**
	 * Returns the SystemProductId for the entry j of the altProductList of the
	 * current item
	 * 
	 * @return String the SystemProductId 
	 */
	public String getSystemProductId(int j) {
		String systemProductId = "";
		
		if (isProdSubst()) {
			AlternativProduct altProd = (AlternativProduct) item.getAlternativProductList().getList().get(j);
			systemProductId = altProd.getSystemProductId();
		}
		return systemProductId;
	}
	
	/**
	 * Returns the productIdType description for the entry j of the altProductList of the
	 * current item
	 * 
	 * @return String the productIdType description
	 */
	public String getproductIdTypeDesc(int j) {
		String productIdTypeDesc = "";
		
		if (isProdDet()) {
			AlternativProduct altProd = (AlternativProduct) item.getAlternativProductList().getList().get(j);
			productIdTypeDesc = shop.getEnteredProductIdDesc(altProd.getEnteredProductIdType());
		}
		return productIdTypeDesc;
	}
	
	/**
	 * Return the Id and description for the entry j of the altProductList of the
	 * current item
	 * 
	 * @return String the productIdType description
	 */
	public String getIdAndDesc(int j) {
		String idAndDesc = "";
		
		if (isProdDet()) {
			AlternativProduct altProd = (AlternativProduct) item.getAlternativProductList().getList().get(j);
			idAndDesc = altProd.getSystemProductId() + " (" + altProd.getDescription() + ")";
		}
		return idAndDesc;
	}
	
	/**
	 * get the campaign id for the entry idx in the determinedCampaigns of the current item 
	 * 
	 * returns the campaign id for the entry idx in the determinedCampaigns of the current item 
	 */
	public String getItemDetCampaignId(int idx) {
		String id = "";
		
		if (isCampDet()) {
			id = id = item.getDeterminedCampaigns().getCampaign(idx).getCampaignId(); 	
		}

		return id;	   
	}
	
	/**
	 * get the campaign description for the entry idx in the determinedCampaigns of the current item 
	 * 
	 * returns the campaign description for the entry idx in the determinedCampaigns of the current item 
	 */
	public String getItemDetCampaignDesc(int idx) {
		String desc = "";
		
		if (isCampDet() && salesDoc.getCampaignDescriptions().size() > 0) {
            String campGuid = item.getDeterminedCampaigns().getCampaign(idx).getCampaignGUID().getIdAsString();
			desc = (String) salesDoc.getCampaignDescriptions().get(campGuid);	
		}

		return desc;	   
	}
	
	/**
	 * get the campaign type description for the entry idx in the determinedCampaigns of the current item 
	 * 
	 * returns the campaign type description for the entry idx in the determinedCampaigns of the current item 
	 */
	public String getItemDetCampaignTypeDesc(int idx) {
		String desc = "";
		
		if (isCampDet() && salesDoc.getCampaignTypeDescriptions().size() > 0) {
			String typeId = item.getDeterminedCampaigns().getCampaign(idx).getCampaignTypeId();
			desc = (String) salesDoc.getCampaignTypeDescriptions().get(typeId);	
		}

		return desc;	   
	}
    
    /**
     * Returns true if option  to postpone the selection should be shown
     */
    public boolean showPostPoneOption() {
        return !ManagedDocumentDetermination.DOC_MODE_SIMULATE.equals(docMode);
    }

}
