package com.sap.isa.isacore.uiclass.b2b.order;

import javax.servlet.jsp.PageContext;

import com.sap.isa.backend.boi.isacore.ConnectedDocumentItemData;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.core.TechKey;
import com.sap.isa.isacore.ShipToSelection;
import com.sap.isa.isacore.action.ActionConstants;
import com.sap.isa.payment.backend.boi.PaymentMethodData;
import com.sap.isa.payment.businessobject.PaymentBase;
import com.sap.isa.payment.businessobject.PaymentBaseType;

/**
 * The class CustomerOrderUI.java .... .
 *
 * @author SAP
 * @version 1.0
 **/
public class CustomerOrderUI extends SalesDocumentUI {

    public boolean isB2BSupported = false;

    public PaymentBase payment;
    public String  paymentKey = "";
    public boolean paytypeIsCard = false;
    public boolean isCollectionFromVendor = false;
    
    public ConnectedDocumentItemData connectedItem;


	protected ShipToSelection shipToSelection;

    /**
     * Constructor for CustomerOrderUI.
     * @param pageContext
     */
    public CustomerOrderUI(PageContext pageContext) {
        super(pageContext);

        // Payment
        payment = (PaymentBase) ((HeaderSalesDocument)header).getPaymentData();
		if (payment.getPaymentMethods() != null && payment.getPaymentMethods().size() > 0) {
        	TechKey payTypeKey = ( (PaymentMethodData) payment.getPaymentMethods().get(0)).getPayTypeTechKey();

       		if (payTypeKey.equals(PaymentBaseType.INVOICE_TECHKEY)) {
          	  paymentKey = "b2b.payment.invoice";
       		}
        	else if (payTypeKey.equals(PaymentBaseType.COD_TECHKEY)) {
            	paymentKey = "b2b.payment.cod";
        	}
        	else if (payTypeKey.equals(PaymentBaseType.CARD_TECHKEY)) {
            	paymentKey = "b2b.payment.card";
            	paytypeIsCard = true;
        	}
		}

        shipToSelection  = (ShipToSelection)request.getAttribute(ActionConstants.RC_SHIPTOSELECTION);
        if (shipToSelection != null) {
            isCollectionFromVendor = shipToSelection.isPartnerShipTo(((HeaderSalesDocument)header).getShipTo());
        }
                
		determineKeyValues();
    }


    /**
     * @see com.sap.isa.isacore.uiclass.b2b.order.OrderUI#setItem(com.sap.isa.businessobject.item.ItemSalesDoc)
     */
    public void setItem(ItemSalesDoc item) {

        super.setItem(item);
        
        connectedItem = item.getSucessor();
    }


	/**
	 * Determines the keys for the send, confirm and cancel buttons
	 */
	protected void determineKeyValues() {
		
		alertBasketEmpty = "";
		confirmKey = "b2b.order.change.confirmtext";
		cancelKey = "b2b.order.change.cancel";
		sendKey = "b2b.order.change.confirmtext";
	}


    /**
     * Return if the sold to inforamtion should be displayed . <br>
     * 
     * @return always <code>true</code>
     * 
     * 
     * @see com.sap.isa.isacore.uiclass.SalesDocumentDataUI#isSoldToVisible()
     */
    public boolean isSoldToVisible() {
        return true;
    }

}
