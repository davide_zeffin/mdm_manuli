/*****************************************************************************
	Class:        ExternalReferenceObjectUI
	Copyright (c) 2005, SAP AG, Germany, All rights reserved.
	Author:       SAP AG
	Created:      19.01.2005
	Version:      1.0

	$Revision: #1$
	$Date: 2005/01/19 $ 
*****************************************************************************/
package com.sap.isa.isacore.uiclass.b2b.order;

import javax.servlet.jsp.PageContext;

import com.sap.isa.businessobject.order.ExtRefObjectList;
import com.sap.isa.core.TechKey;




/**
 * The class ExternalReferenceObjectUI . <br>
 *
 * @author  SAP AG
 * @version 1.0
 *
 *
 */
public class ExternalReferenceObjectUI extends SalesDocumentUI {
	
	private ExtRefObjectList extRefObjects;
	private int listSize = 0;

	/**
	 * Constructor for ExternalReferenceObjectUI
	 * @param pageContext
	 */
	public ExternalReferenceObjectUI(PageContext pageContext) {
	
		super(pageContext);
		extRefObjects = (ExtRefObjectList)userSessionData.getAttribute("extRefObjects[]");
		if (extRefObjects != null) {
			listSize = extRefObjects.size();
		}		
	}
			
	public boolean isHeader() {
		return request.getAttribute("level").toString().equals("1");
	}
	
	public String getHeader() {
		if (isHeader()) {
			return "1";
		}
		return "0";
	}
	
	public int getCounter() {
		return Integer.parseInt(request.getAttribute("vincounter").toString());
	}
	
	public boolean isNewPos() {
		boolean newPos = false;
		if (request.getAttribute("addNewPos").toString().equals("true")) {
			newPos = true;
		}
		return newPos;
	}
	
	public String getVinValue(int idx) {
		String value = "";
		String param = "";
		
		if (isNewPos()) {
			param = "vinNumber_".concat(String.valueOf(idx));
			if (request.getAttribute(param) != null && 
				  request.getAttribute(param).toString().length() > 0) {
				value = request.getAttribute(param).toString();
			}
		} else {
			param = "VIN_".concat(String.valueOf(idx));
			if ((String) header.getExtensionData(param) != null) {
				value = (String) header.getExtensionData(param);
			}
		}
		
		return value;
	}
	
	public String getVinValue(int idx, String id) {
		String value = "";
		String param = "";
		TechKey techKey = new TechKey(id);
		param = "VIN_".concat(String.valueOf(idx));
		if (items.get(techKey) != null) {
			value = (String) items.get(techKey).getExtensionData(param);
		}
		return value;
	}
	
	public int getVinHits() {
		return Integer.parseInt(request.getAttribute("vinhits").toString());
	}
	
	public int getPosNum() {
		return Integer.parseInt(request.getAttribute("linenum").toString());
	}
	
	public String getItemKeyValue() {
		String itemKey = "";
		if (!isHeader()) {
			itemKey = request.getAttribute("itemkey").toString();
		}
		return itemKey;
	}	

	
	public String getExtRefObjValue(int i) {
		String value = "";
		if (i < listSize) {
			value =  extRefObjects.getExtRefObject(i).getData();		
		}
		return value;
	}
	
	public String getExtRefObjNoOfEntries() {
		return String.valueOf(listSize);		
	}
	
	public String getHead() {
		if (!isHeader()) {
			return "b2b.extrefobj.title.item";
		}
		else {
			return "b2b.extrefobj.title.header";
		}
	}
	
	
	/**
	 * Returns the external reference objects as string separated by ';' 
	*/	
	public String getExtRefObjListAsString(ExtRefObjectList extRefObjects) {
		String objects = "";
		if ( extRefObjects != null && extRefObjects.size() > 0) {
			for (int j=0; j < extRefObjects.size(); j++) {
				objects = objects + extRefObjects.getExtRefObject(j).getData() + ";";
			}
		}
		return objects;	
	}
	
	
		
}