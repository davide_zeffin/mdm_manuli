/*****************************************************************************
	Class:        CampaignDetailUI
	Copyright (c) 2006, SAP AG, Germany, All rights reserved.

	$Revision: #1 $
	$DateTime: 2006/11/27 $ 
    
*****************************************************************************/

package com.sap.isa.isacore.uiclass.b2b;

import java.util.Iterator;

import javax.servlet.jsp.PageContext;

import com.sap.ecommerce.businessobject.campaign.Campaign;
import com.sap.ecommerce.businessobject.campaign.CampaignEnrollment;
import com.sap.ecommerce.businessobject.campaign.CampaignEnrollmentList;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.MessageList;
import com.sap.isa.isacore.action.ActionConstants;
import com.sap.isa.isacore.action.campaign.CampaignDisplayAction;
import com.sap.isa.isacore.uiclass.IsaBaseUI;

/**
 * The class CampaignDetailUI.java processes the campaignDetail.jsp .
 *
 * @author SAP
 * @version 1.0
 **/
public class CampaignDetailUI extends IsaBaseUI {
	
    // Logging location
    public static IsaLocation log = IsaLocation.getInstance(CampaignDetailUI.class.getName());
    
    protected Campaign campaign = null;
    protected CampaignEnrollmentList enrollmList = null;
    protected CampaignEnrollmentList enrollmListPage = null;
    protected boolean isChangeable = false;
     
    // paging for enrollment
    protected int currentPage = 1;   // current page number 
    protected int maxPages = 0;      // maximum number of pages 
    protected int maxColsEnr  = 5;   // maximum number of columns in enrollment table 
    protected int maxPageLinks  = 5; // maximum number of page links

    /**
    * List of messages
    */
    public final static String MESSAGE_LIST = "MessageList";

	/**
	 * Constructor for CampaignDetailUI
	 * @param pageContext
	 */
	public CampaignDetailUI(PageContext pageContext) {
	
		super(pageContext);
        
        final String METHOD = "CampaignDetailUI()";
        log.entering(METHOD);
        // Sets the campaign into the request

        // campaign
        this.campaign = (Campaign) request.getAttribute(ActionConstants.RC_CAMPAIGN);
        if (campaign == null) {
            log.error(METHOD + ": campaign = null");
            return;
        } 
 
        String currPageStr = (String) request.getAttribute(ActionConstants.RC_CAMPAIGN_PAGE);
        if (currPageStr != null) {
            this.currentPage = Integer.valueOf(currPageStr).intValue();
        }
           
        // Get the enrollment list page
        this.enrollmListPage = (CampaignEnrollmentList) request.getAttribute(ActionConstants.RC_CAMPAIGN_ENROLLM);
        
        // Get the changeable flag 
        String isChangeableStr = (String) request.getAttribute("enrollment_changeable");
        isChangeable = (isChangeableStr != null && isChangeableStr.equals("true"));

        enrollmList = this.campaign.getCampaignEnrollmentList();  
        if (enrollmList != null) {
            this.maxPages = enrollmList.size() / CampaignDisplayAction.MAX_ENROLLM_LINES ;
            int zeroOrOne = enrollmList.size() % CampaignDisplayAction.MAX_ENROLLM_LINES ;
            if (zeroOrOne > 0) {
                this.maxPages += 1; 
            }
            if (this.maxPages < this.maxPageLinks) {
                this.maxPageLinks = this.maxPages;
            }
            if (log.isDebugEnabled()) {
                log.debug(METHOD + " / maximum number of pages = " + this.maxPages);
            }
        }
      
        if (log.isDebugEnabled()) {
           if (enrollmListPage != null) {
               log.debug(METHOD + ": number of entries in enrollmListPage = " + enrollmListPage.size());
           } 
           else {
               log.debug(METHOD + ": enrollmListPage = null");
           }
           if (enrollmList != null) {
               log.debug(METHOD + ": number of entries in enrollmList = " + enrollmList.size());
           } 
           else {
               log.debug(METHOD + ": enrollmList = null");
           }
        }
        
        MessageList msgList = getCampMessageList();
        if (msgList != null) { 
            pageContext.setAttribute(MESSAGE_LIST, msgList); 
        }

        log.exiting();
	}	
    
    /**
     * Returns the campaign  
     *
     * @return campaign from request attribute 
     */
    public Campaign getCampaign() {
        final String METHOD = "getCampaign()";
        log.entering(METHOD);

        log.exiting();
        return this.campaign;
    }

    /**
     * Returns the Id of the campaign  
     * @return campaignId as String
     */
    public String getCampaignId() {
        final String METHOD = "getCampaignId()";
        String campId = "";
        log.entering(METHOD);

        if (this.campaign != null &&
            this.campaign.getHeader() != null) {
          campId = this.campaign.getHeader().getCampaignID();
        }
        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": campaignId = " + campId);
        }
        log.exiting();
        return campId;
    }
    
    /**
     * Returns the information if the enrollment list of the campaign contains only one entry 
     * @return isSingleEnrollm as boolean, true if only one entry in the list, false otherwise
     */
    public boolean isSingleEnrollment() {
        final String METHOD = "isSingleEnrollment()";
        log.entering(METHOD);
        boolean isSingleEnrollm = true;

        if (this.enrollmList != null) {
                int size = this.enrollmList.size();
                isSingleEnrollm = (size == 0 || size == 1);
        }
        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": only one enrollment entry = " + isSingleEnrollm);
        }
        log.exiting();
        return isSingleEnrollm;
    }
    
    /**
     * Returns the description of the campaign type  
     * @return descr as String, description of the type
     */
    public String getCampTypeDescr() {
        final String METHOD = "getCampTypeDescr()";
        log.entering(METHOD);
        String descr = "";

        if (this.campaign != null &&
            this.campaign.getHeader() != null) {
            descr = this.campaign.getHeader().getTypeDescription();
        }
        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": campaign type description = " + descr);
        }
        log.exiting();
        return descr;
    }
    
    /**
     * Returns the planned start date of the campaign   
     * @return date as String, planned start date of the campaign
     */
    public String getCampPlannedStartDate() {
        final String METHOD = "getCampPlannedStartDate()";
        log.entering(METHOD);
        String date = "";

        if (this.campaign != null &&
            this.campaign.getHeader() != null) {
            date = this.campaign.getHeader().getPlannedStartDate();
        }
        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": campaign planned start date = " + date);
        }
        log.exiting();
        return date;
    }
    
    /**
     * Returns the planned end date of the campaign   
     * @return date as String, planned end date of the campaign
     */
    public String getCampPlannedEndDate() {
        final String METHOD = "getCampPlannedEndDate()";
        log.entering(METHOD);
        String date = "";

        if (this.campaign != null &&
            this.campaign.getHeader() != null) {
            date = this.campaign.getHeader().getPlannedEndDate();
        }
        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": campaign planned end date = " + date);
        }
        log.exiting();
        return date;
    }
    
    /**
     * Returns the enrollment start date of the campaign   
     * @return date as String, enrollment start date of the campaign
     */
    public String getCampEnrollmStartDate() {
        final String METHOD = "getCampEnrollmStartDate()";
        log.entering(METHOD);
        String date = "";

        if (this.campaign != null &&
            this.campaign.getHeader() != null) {
            date = this.campaign.getHeader().getEnrollmStartDate();
        }
        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": campaign enrollment start date = " + date);
        }
        log.exiting();
        return date;
    }
    
    /**
     * Returns the enrollment end date of the campaign   
     * @return date as String, enrollment end date of the campaign
     */
    public String getCampEnrollmEndDate() {
        final String METHOD = "getCampEnrollmEndDate()";
        log.entering(METHOD);
        String date = "";

        if (this.campaign != null &&
            this.campaign.getHeader() != null) {
            date = this.campaign.getHeader().getEnrollmEndDate();
        }
        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": campaign enrollment end date = " + date);
        }
        log.exiting();
        return date;
    }
    
    /**
     * Returns the short description of the campaign   
     * @return descr as String, short description of the campaign
     */
    public String getCampShortDescr() {
        final String METHOD = "getCampShortDescr()";
        log.entering(METHOD);
        String descr = "";

        if (this.campaign != null &&
            this.campaign.getHeader() != null) {
            descr = this.campaign.getHeader().getDescription();
        }
        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": campaign short description = " + descr);
        }
        log.exiting();
        return descr;
    }
    
    /**
     * Returns the long description of the campaign   
     * @return descr as String, long description of the campaign
     */
    public String getCampLongDescr() {
        final String METHOD = "getCampLongDescr()";
        log.entering(METHOD);
        String descr = "";

        if (this.campaign != null &&
            this.campaign.getHeader() != null) {
            descr = this.campaign.getHeader().getCampaignText();
            if (descr == null) {
                descr = "";
            }
        }
        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": campaign long description = " + descr);
        }
        log.exiting();
        return descr;
    }
    
    /**
     * Returns the an enrollment information
     * @return enrollm of type CampaignEnrollment 
     */
    public CampaignEnrollment getEnrollment() {
        final String METHOD = "getEnrollment()";
        log.entering(METHOD);
        CampaignEnrollment enrollm = null;

        Iterator iter = enrollmList.iterator();
         
        if (iter.hasNext()) {
            enrollm = (CampaignEnrollment) iter.next();
        }
        if (log.isDebugEnabled()) {
            if (enrollm != null) {
                log.debug(METHOD + ": enrollee id = " + enrollm.getEnrolleeID());
            }
            else {
                log.debug(METHOD + ": enrollment not found");
            }
        }
        log.exiting();
        return enrollm;
    }
    
    /**
     * Returns the information if enrollment data is available
     * @return flag as boolean, 'true' if enrollment data is available, 'false' otherwise
     */
    public boolean isEnrollmentAvailable() {
        final String METHOD = "isEnrollmentAvailable()";
        log.entering(METHOD);
        boolean retval = false;

        if (this.enrollmListPage != null) {
          retval = (this.enrollmListPage.size() > 0);
        }
        
        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": number of enrollment entries = " + enrollmListPage.size()
                      + " / is enrollment available = " + retval);
        }
        log.exiting();
        return retval;
    }
    
    /**
     * Returns the information if the enrollment button is visible
     * @return flag as boolean, 'true' if enrollment button is visible, 'false' otherwise
     */
    public boolean isEnrollmentButtonVisible() {
        final String METHOD = "isEnrollmentButtonVisible()";
        log.entering(METHOD);
        boolean retval = false;

        if (this.enrollmListPage != null) {
          retval = (this.enrollmListPage.size() > 0) && this.isChangeable;
        }
        
        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": number of enrollment entries = " + enrollmListPage.size() +
                      " / isChangeable = " + this.isChangeable +
                      " / is enrollment button visible = " + retval);
        }
        log.exiting();
        return retval;
    }
    
    /**
     * Returns the information if the enrollment button is disabled
     * @return flag as boolean, 'true' if enrollment button is disabled, 'false' otherwise
     */
    public boolean isEnrollmentButtonDisabled() {
        final String METHOD = "isEnrollmentButtonDisabled()";
        log.entering(METHOD);
        boolean disabled = false;
        
        if (this.enrollmList != null) { 
          if (this.enrollmList.size() == 1) {
              if (log.isDebugEnabled()) {
                  log.debug(METHOD + ": only one enrollment item, therefore the return value is always false");
              }
          }
          else {
              boolean isChanged = this.enrollmList.isEnrollmListChanged();
              disabled = !isChanged;
              if (log.isDebugEnabled()) {
                  log.debug(METHOD + ": at least one enrollment is change in the list = " + isChanged);
              }
          }
        }
        
        log.exiting();
        return disabled;
    }
    
    /**
     * Sets the currrent page number of the enrollment list
     * 
     * @param page as int, number of the page to be set 
     */
    public void setPage(int page) {
        final String METHOD = "setPage()";
        log.entering(METHOD);
        
        if (this.campaign != null &&
            this.campaign.getCampaignEnrollmentList() != null) {
            if (page > 0 && page <= this.maxPages) {
                this.currentPage = page;
                if (log.isDebugEnabled()) {
                    log.debug(METHOD + ": page number was set to = " + this.currentPage);
                }
            }
        }
        log.exiting();
    }
    
    /**
     * Sets the first page
     */
    public void setFirstPage() {
        final String METHOD = "setFirstPage()";
        log.entering(METHOD);
        setPage(1);
        log.exiting();
    }
    
    /**
     * Sets the last page
     */
    public void setLastPage() {
        final String METHOD = "setLastPage()";
        log.entering(METHOD);
        setPage(this.maxPages);
        log.exiting();
    }
    
    /**
     * Sets the next page
     */
    public void setNextPage() {
        final String METHOD = "setNextPage()";
        log.entering(METHOD);
        setPage(this.currentPage + 1);
        log.exiting();
    }
    
    /**
     * Sets the previous page
     */
    public void setPreviousPage() {
        final String METHOD = "setPreviousPage()";
        log.entering(METHOD);
        setPage(this.currentPage - 1);
        log.exiting();
    }
    
    /**
     * Get the current page number
     * 
     * @return currentPage as int, current page number
     */
    public int getCurrPage() {
        final String METHOD = "getCurrPage()";
        log.entering(METHOD);
        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": current page number = " + this.currentPage);
        }
        log.exiting();
        return (this.currentPage);
    }
    
    /**
     * Get the first index of the business partner
     * 
     * @return firstIndex as int, number of first business partner on the current page
     */
    public int getFirstBPIndex() {
        final String METHOD = "getFirstBPIndex()";
        log.entering(METHOD);
        
        int firstIndex = (this.currentPage - 1) * CampaignDisplayAction.MAX_ENROLLM_LINES + 0;
        
        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": first index of business partner = " + firstIndex
                      + " on page = " + this.currentPage);
        }
        log.exiting();
        return (firstIndex);
    }
    
    /**
     * Get the last index of the business partner
     * 
     * @return lastIndex as int, number of last business partner on the current page
     */
    public int getLastBPIndex() {
        final String METHOD = "getLastBPIndex()";
        log.entering(METHOD);
        
        int lastIndex = this.currentPage * CampaignDisplayAction.MAX_ENROLLM_LINES - 1;
        
        if (lastIndex > CampaignDisplayAction.MAX_ENROLLM_LINES) {
            lastIndex = CampaignDisplayAction.MAX_ENROLLM_LINES;
        }

        int listSize = this.campaign.getCampaignEnrollmentList().size();
        if (lastIndex > listSize) {        
            lastIndex = listSize;
        }
        
        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": list size = " + listSize
                      + ": last index of business partner = " + lastIndex
                      + " on page = " + this.currentPage);
        }
        log.exiting();
        return (lastIndex);
    }
    
    /**
     * Returns the number of rows of the current enrollment table
     * 
     * @return currRowNumber as int, current number of rows in the table
     */
    public int getRowNumber() {
        final String METHOD = "getRowNumber()";
        log.entering(METHOD);
        
        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": current number of rows = " + this.enrollmListPage.size());
        }
        log.exiting();
        return (this.enrollmListPage.size());
    }
    
    /**
     * Returns the number of columns of the enrollment table
     * 
     * @return maxColNumber as int, maximum number of columns in the table
     */
    public int getColNumber() {
        final String METHOD = "getColNumber()";
        log.entering(METHOD);
        
        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": max number of columns = " + this.maxColsEnr);
        }
        log.exiting();
        return (this.maxColsEnr);
    }
    
    /**
     * Returns the enrollment list
     * 
     * @return enrollmTable as CampaignEnrollmentList
     */
    public CampaignEnrollmentList getEnrollmList() {
        final String METHOD = "getEnrollmList()";
        log.entering(METHOD);
        
        log.exiting();
        return (this.enrollmList);
    }
    
    /**
     * Checks if the business partner is enrolled for the campaign
     * This check returns only true, if only one enrollment entry was selected.
     * 
     * @return retval as boolean, true if business partner is enrolled
     */
    public boolean isBpEnrolled() {
        final String METHOD = "isBpEnrolled()";
        log.entering(METHOD);
        
        boolean retval = false;
        
        if (this.enrollmList != null) {
            CampaignEnrollment enrollm = getEnrollment();
            retval = enrollm.isEnrolled();
        }
        
        log.exiting();
        return (retval);
    }

    /**
     * Checks if the page link should be displayed
     * This check returns true, if more than one page
     * 
     * @return display as boolean, true if page link is displayed
     */
    public boolean isPageLinkDisplayed() {
        final String METHOD = "isPageLinkDisplayed()";
        log.entering(METHOD);
        
        boolean display = false;
        
        if (this.maxPages > 1) {
            display = true;
        }
        
        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": is page link displayed = " + display);
        }
        log.exiting();
        return (display);
    }

    /**
     * Returns true, if the current page is not the last page
     * 
     * @return true if the current page is not the last page 
     *         false if there are pages available after the current page. 
     */
    public boolean showNextLink() {
        return this.currentPage < this.maxPages;
    }
    
    /**
     * determines the information if the current page isn't the first page.
     *
     * @return boolean value is true if it isn't the 1st page
     */
    public boolean showPrevLink() {
        return this.currentPage > 1;
    }

    /**
     * Returns the pagination link consisting of an array of page numbers
     *
     * @return array of page numbers
     */
    public int[] getPaginationLinks() {
        
        log.debug("PaginationLinks called");
        
        int paginationArray[];   //Array to hold all the pagination numbers to be displayed

        // check for odd or even nuber of pages to be displayed
        int zeroOrOne = maxPageLinks % 2;

        int middlePage = this.currentPage;        //Current Page
        
        // if the page to be displayed is not at least the middle of the 
        // first chunk of pages to be displayed, set it to the middle
        if (middlePage <= maxPageLinks / 2) {
            middlePage = maxPageLinks / 2 + zeroOrOne;
        }
        
        // if the current page would exceed the total number of pages
        // set it to the middle of the last page chunk
        if (middlePage > maxPages - (maxPageLinks / 2)) {
            middlePage = maxPages - (maxPageLinks / 2);
        }

        //Calculate the first and last pagination number that should be displayed.
        int firstLinkNumber = middlePage - ((maxPageLinks / 2) + zeroOrOne) + 1 ;
        int lastLinkNumber = middlePage + (maxPageLinks / 2);
        
        // no make sure we do notstart with an index < 1
        if (firstLinkNumber < 1) {
            firstLinkNumber = 1;
        }

        //Create the array to hold the pagination numbers that will be displayed on the page
        paginationArray = new int[maxPageLinks];
        
        try {
            for (int y = firstLinkNumber; y <= lastLinkNumber; y++) {
                paginationArray[y - firstLinkNumber] = y;
            }
        }
        catch (IndexOutOfBoundsException e) {
        	log.debug(e.getMessage());
        }

        return paginationArray;
    }

    /**
     * Returns the message list for campaign enrollment 
     * 
     * @return msgList of type MessageList
     */
    public MessageList getCampMessageList() {

        final String METHOD = "getCampMessageList()";
        log.entering(METHOD);
        MessageList msgList = null;
        
        if (campaign != null && campaign.hasMessages()) {
            msgList = campaign.getMessageList();
            if (log.isDebugEnabled()) {
                log.debug(METHOD + ": number of messages =" + msgList.size());
            }
        }
        else {
            if (log.isDebugEnabled()) {
                log.debug(METHOD + ": no messages found");
            }
        }
        log.exiting();
        return msgList;
    }

    /**
     * Returns the information if all business partners are enrolled
     * 
     * @return flag as boolean, true if all business partners are enrolled
     */
    public boolean areAllBpEnrolled() {
        final String METHOD = "areAllBpEnrolled()";
        log.entering(METHOD);
        
        boolean retval = this.enrollmList.areAllBpEnrolled();
        
        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": all business partner are enrolled = " + retval);
        }
        
        log.exiting();
        return (retval);
    }
    
    /**
     * Returns the information if all business partners on the current page are enrolled
     * 
     * @return flag as boolean, true if all business partners on the current page are enrolled
     */
    public boolean areAllBpOnPageEnrolled() {
        final String METHOD = "areAllBpOnPageEnrolled()";
        log.entering(METHOD);
        
        boolean retval = this.enrollmListPage.areAllBpEnrolled();
        
        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": all business partner on the current page are enrolled = " + retval);
        }
        
        log.exiting();
        return (retval);
    }
    
    /**
     * Returns the information if the enrollment flags are not changeable
     * 
     * @return flag as boolean, true if the enrollment data are not changeable
     */
    public boolean isDisplayOnly() {
        final String METHOD = "isDisplayOnly()";
        log.entering(METHOD);
        
        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": enrollment is display only = " + !this.isChangeable);
        }
        
        log.exiting();
        return (!this.isChangeable);
    }

}
