/*****************************************************************************
    Class:        BillingStatusUI
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      01.03.2004
    Version:      1.0

    $Revision: #12 $
    $Date: 2004/08/10 $ 
*****************************************************************************/

package com.sap.isa.isacore.uiclass.b2b.billing;

import javax.servlet.jsp.PageContext;

import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.ConnectedDocument;
import com.sap.isa.businessobject.ConnectedDocumentItem;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.item.ItemBillingDoc;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.businessobject.order.PartnerListEntry;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.interaction.InteractionConfig;
import com.sap.isa.core.interaction.InteractionConfigContainer;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.core.util.table.TableRow;
import com.sap.isa.core.xcm.init.ExtendedConfigInitHandler;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.action.ActionConstantsBase;
import com.sap.isa.isacore.action.IsaCoreInitAction;
import com.sap.isa.isacore.action.IsaCoreInitAction.StartupParameter;
import com.sap.isa.isacore.uiclass.BillingDocumentStatusBaseUI;
import com.sap.isa.portalurlgenerator.businessobject.PortalUrlGenerator;
import com.sap.isa.portalurlgenerator.util.AnchorAttributes;
import com.sap.isa.portalurlgenerator.util.UrlParameters;


/**
 * The class BillingStatusUI is the ISACore implementation for the abstract
 * BillingDocumentStatusBaseUI UI class used on the billing status detail page.  <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class BillingStatusUI extends BillingDocumentStatusBaseUI {

    protected Shop shop;
    
    protected BusinessObjectManager bom;

    protected StartupParameter startupParam;

    protected AnchorAttributes anchorAttrSales = null;

    protected AnchorAttributes anchorAttrSalesDocDisplay = null;
    
	/** 
	  *  The IsaLocation of the current Action.
	  *  This will be instantiated during perform and is always present.
	  */
	 protected IsaLocation log = IsaLocation.getInstance(BillingStatusUI.class.getName());
    
    /**
     * Constructor for OrderStatusUI. <br>
     * 
     * @param pageContext
     */
    public BillingStatusUI(PageContext pageContext) {
        super(pageContext);
    }
    /**
     * Constructor for OrderStatusUI. <br>
     * 
     * @param pageContext
     */
    public BillingStatusUI() { 
    }

    /**
     * Initialize the the object from the page context. <br>
     * 
     * @param pageContext page context
     */
    public void initContext(PageContext pageContext) {
        super.initContext(pageContext);
    }

    /**
     * Method to retrieve the DocumentHandler. If billing docuemnts run outside ISales frames 
     * (isBillingIncluded = false), then the appropriated document handler has to be returned.
     * @return DocumentHandler docHandler
     */
    protected DocumentHandler getDocumentHandler(UserSessionData userSessionData) {
        startupParam = (StartupParameter)userSessionData.getAttribute(IsaCoreInitAction.SC_STARTUP_PARAMETER);
        DocumentHandler docHandler = null;
        if (startupParam.isBillingIncluded()) {
            docHandler = (DocumentHandler) userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
        } else {
            docHandler = (DocumentHandler) userSessionData.getAttribute(ActionConstantsBase.DOCUMENT_HANDLER_BILL);
        }
        return docHandler;
    }

    /**
     * Return allways <code>false</code>. Only implemented by the needs of
     * @see SalesDocuemntStatusBaseUI.
     */
    public boolean isOrderDownloadRequired() {
        return false;
    }

    /** 
     * Return allways <code>null</code>. Only implemented by the needs of
     * @see SalesDocuemntStatusBaseUI.
     */
    public ResultData getSucProcTypeAllowedList() {
        return null;
    }

    /**
     * Not used. Only implemented by the needs of
     * @see SalesDocuemntStatusBaseUI.
     */
    protected void setProcessTypeInfo() {
    }


    /**
     * Set the shop object in the salesDocConfiguration property. <br>
     * 
     * @see com.sap.isa.isacore.uiclass.order.OrderDataUI#setSalesDocConfiguration()
     */
    protected void setSalesDocConfiguration() {
        
        // check for missing context
        if (userSessionData != null) {

            // get BOM
            bom = (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
    
            shop = bom.getShop();
            
            // now I can read the shop
            salesDocConfiguration = bom.getShop();
        }       
    }


    /**
     * Only implemented by the needs of @see SalesDocuemntStatusBaseUI.
     */
    protected String getSubstitutionReasonDesc(String key) {
        return null;
    }

    /**
     * @return Returns true if the soldTo information should be visible
     * @see com.sap.isa.isacore.uiclass.order.OrderDataUI#isSoldVisible()
     */
    public boolean isSoldToVisible() {
        boolean isVisible = shop.isSoldtoSelectable();
        
        // check also if the soldto is not the default soldto
/*      if (!isVisible) {
            BusinessPartner defaultSoldTo =  bom.getBUPAManager().getDefaultBusinessPartner(PartnerFunctionData.SOLDTO);
            if (defaultSoldTo != null) {
                String soldToId = header.getPartnerId(PartnerFunctionData.SOLDTO);
                if (!soldToId.equals(defaultSoldTo.getId())) {
                    isVisible = true;   
                }
            }   
        }
*/      
        return isVisible;
    }

    /**
     * @param soldToKey TechKey representing the SoldTo key
     * @return BusinessPartner of the SoldTo
     * @see com.sap.isa.isacore.uiclass.order.OrderDataUI#getSoldTo(com.sap.isa.core.TechKey)
     */
    protected BusinessPartner getSoldTo(TechKey soldToKey) {
        
        return  bom.createBUPAManager().getBusinessPartner(soldToKey);
    }

    /**
     * Get Businesspartner of partner function from header
     * @param String  partner function
     * @return Businesspartner 
     */
    public BusinessPartner getHeaderBusinessPartner(String partnerFunction) {
        PartnerListEntry ple = header.getPartnerList().getPartner(partnerFunction);
        BusinessPartner bp = null;
        if ( ple != null) {
            bp = bom.getBUPAManager().getBusinessPartner(ple.getPartnerTechKey());
        }
        return bp;
    }

    /**
     * Get Businesspartner of partner function from item
     * @param ItemSalesDoc item
     * @param String  partner function
     * @return Businesspartner 
     */
    public BusinessPartner getItemBusinessPartner(ItemSalesDoc item, String partnerFunction) {
        PartnerListEntry ple = item.getPartnerList().getPartner(partnerFunction);
        BusinessPartner bp = null;
        if ( ple != null) {
            bp = bom.getBUPAManager().getBusinessPartner(ple.getPartnerTechKey());
        }
        return bp;
}

    /**
     * @return Returns true if the hosted order management is activated.
     */
    public boolean isHomActivated() {
        return shop.isHomActivated();
    }
    /**
     * Implemented method to fill Table JScriptFilenames
     */
    protected void fillJScriptFileNameTable(Table tab) {
        TableRow row = tab.insertRow();
        row.setValue("FILENAME","b2b/jscript/frames.js");

        row = tab.insertRow();
        row.setValue("FILENAME","b2b/jscript/billingbuttons.jsp");

        row = tab.insertRow();
        row.setValue("FILENAME","appbase/jscript/portalnavigation.js.jsp");

        row = tab.insertRow();
        row.setValue("FILENAME","iviews/jscript/crm_isa_epcm.js");
    }
    /**
     * Implemented method to fill Table NavigationButton. This method includes also all checks
     * whether a button should be diplayed or not. Following buttons exist:<br>
     * - Close Document<br>
     * - Create Claim<br>
     */
    protected void fillNavigationButtonTable(Table navButtons) {
        // Fill table with allowed buttons
        TableRow row = navButtons.insertRow();

        // CREATE CLAIM BUTTON 
        AnchorAttributes anchor = this.getPortalAnchorAttributesForClaimCreation();
        if (anchor != null  &&  anchor.getURI() != null  && anchor.getURI().length() > 0) {
            row.setValue("ALIGN","RIGHT");
            row.setValue("NAME", "ecm.stat.bill.but.crtcl");
            row.setValue("TITLE", "ecm.acc.bill.but.crtcl");
            row.setValue("HREF", "#");
            row.setValue("ONCLICK", "createClaim();");
            row.setValue("CSSCLASS", "");
            row.setValue("LINKED_ELEMENT", "");

            // Add row for next button!
            row = navButtons.insertRow();
            // Set flag to signal JSP to render a checkbox
            isCheckBoxRequested = true;
        }

        // CLOSE BUTTON 
        row.setValue("ALIGN","RIGHT");
        row.setValue("NAME", "status.sales.detail.button.close");
        row.setValue("TITLE", "ecm.acc.bill.but.close");
        row.setValue("HREF", "#");
        row.setValue("ONCLICK", "closeDocument();");
        row.setValue("CSSCLASS", "");
        row.setValue("LINKED_ELEMENT", "");
    }
    /**
     * Method returns the AnchorAttribute object which contains the information to do Portal 
     * navigation. The navigation will point to the claims & returns web application.
     * The action forward used for claim creation is <b>createclaimfrominv</b>.
     * @return AnchorAttribute 
     */
    public AnchorAttributes getPortalAnchorAttributesForClaimCreation() {
        if (anchorAttrSales == null) {
            if (ExtendedConfigInitHandler.isActive()  &&  startupParam.isPortal()) {
                InteractionConfigContainer interactionConfigData = getInteractionConfig();
                InteractionConfig iac = interactionConfigData.getConfig("shop");
                boolean claimingEnabled = false;
                if (iac != null) {
                    claimingEnabled = ("true".equalsIgnoreCase(iac.getValue("portal.enable.claiming.from.invoice")) ? true : false);
                }
                if (claimingEnabled) {            
                    UrlParameters urlPars = new UrlParameters();
                    PortalUrlGenerator urlGen = bom.createPortalUrlGenerator();
                    urlPars.add("nextaction", "createclaimfrominv");
                
                    try {
                        anchorAttrSales = urlGen.readPageURL("COMPLAINTCRM","","","DEFAULT","",urlPars, request);
                    } catch (CommunicationException comEx) {
						if (log.isDebugEnabled()) {
							log.debug("Error ", comEx);
						}
                    }

                }
            }
        }
        return anchorAttrSales;
    }
    /**
     * Method returns the AnchorAttribute object which contains the information to do Portal 
     * navigation. The navigation will point to Internet Sales web application.<br>
     * <b>The anchor will only be created if the billing screen running separatly (billing.included=NO)</b><br>
     * The action forward used for sales document display is <b>cedocumentstatusdetail</b>.
     * @return AnchorAttribute 
     */
    public AnchorAttributes getPortalAnchorAttributesForSalesDocDisplay() {
        if (anchorAttrSalesDocDisplay == null) {
            if (ExtendedConfigInitHandler.isActive()  &&  startupParam.isPortal()  
                &&  ! startupParam.isBillingIncluded()) {
                UrlParameters urlPars = new UrlParameters();
                PortalUrlGenerator urlGen = bom.createPortalUrlGenerator();
                urlPars.add("nextaction", "cedocumentstatusdetail");
                
                try {
                    anchorAttrSalesDocDisplay = urlGen.readPageURL("SALESORDERCRM","","","DEFAULT","",urlPars, request);
                } catch (CommunicationException comEx) {
					if (log.isDebugEnabled()) {
						log.debug("Error ", comEx);
					}
                }
            }
        }
        return anchorAttrSalesDocDisplay;
    }
    /**
     * Write out JScript onClick event depending on the existens of a sales document reference.<br>
     * Furthermore links will only be created for known document. 
     * @param ItemBillingDoc item for which the onClick event should be created
     * @param ConnectedDocument where the link might point to
     * @return String like 'onclick="docDisplay('DocKey', 'DocNo', 'DocType', 'DocOrigin');"
     */
    public String writeItemOnClickEvent(ItemBillingDoc item, ConnectedDocumentItem cd) {
        String retVal = "";
        // Check link should be created or not
        if (HeaderData.DOCUMENT_TYPE_DELIVERY.equals(cd.getDocType())
         || HeaderData.DOCUMENT_TYPE_RETURNS.equals(cd.getDocType())
         || HeaderData.DOCUMENT_TYPE_SERVICECONTRACT.equals(cd.getDocType())
		 || HeaderData.DOCUMENT_TYPE_SERVICEORDER.equals(cd.getDocType())
		 || HeaderData.DOCUMENT_TYPE_CONFIRMATION.equals(cd.getDocType())		          
         || HeaderData.DOCUMENT_TYPE_COMPLAINT.equals(cd.getDocType())) {
            return retVal;         // => NO LINK
        }
        String docKey = ("CRMB".equals(cd.getAppTyp())) 
                     ? cd.getDocNumber() : cd.getTechKey().getIdAsString();
        if (docKey == null  ||  "".equals(docKey)) {
            // Without a document key no link can be provided
            return retVal;
        }
        String docID = cd.getDocNumber();
        String docType = ("CRMB".equals(cd.getAppTyp()) ? "billing" : "order");
        String docOrigin = ("CRMB".equals(cd.getAppTyp()) ? "CRM" : cd.getDocOrigin());
        String navType = (! startupParam.isBillingIncluded() 
                      &&  startupParam.isPortal()
                      &&  ! "billing".equals(docType) 
                      &&  getPortalAnchorAttributesForSalesDocDisplay() != null
                      ? "PORTAL" : "SHOP");  
        retVal = ("onclick=\"docDisplay(") + ("'"+navType+ "',") + ("'"+docKey+"',")
                 + ("'"+docID+"',") + ("'"+docType+"',") + ("'"+docOrigin+"'") + ");\"";   
        return retVal;
    }
    /**
     * <b>NOT IMPLEMENTED returns allways null</b><br>
     * Method returns the AnchorAttribute object which contains the information to do Portal 
     * navigation. The navigation will point to Internet Sales web application (separate billing
     * part).
     * @return AnchorAttribute 
     */
    public AnchorAttributes getPortalAnchorAttributesForBillingDocDisplay() {
         return null;
    }
    /**
     * 
     * Write out JScript onClick event depending on the existens of a referenced doc.
     * @return String like 'onclick="documentDisplay('dockey', 'docno', 'doctype');"
     */
    public String writeHeaderRefOnClickEvent(ConnectedDocument conDoc) {
		String retVal = "";
			// Check link should be created or not
			if (HeaderData.DOCUMENT_TYPE_DELIVERY.equals(conDoc.getDocType())
			 || "DLVY".equals(conDoc.getDocType())
			 || HeaderData.DOCUMENT_TYPE_RETURNS.equals(conDoc.getDocType())
			 || HeaderData.DOCUMENT_TYPE_SERVICECONTRACT.equals(conDoc.getDocType())
			 || HeaderData.DOCUMENT_TYPE_SERVICEORDER.equals(conDoc.getDocType())
			 || HeaderData.DOCUMENT_TYPE_CONFIRMATION.equals(conDoc.getDocType())		          
			 || HeaderData.DOCUMENT_TYPE_COMPLAINT.equals(conDoc.getDocType())) {
				return retVal;         // => NO LINK
			}
			
			String docKey = ("CRMB".equals(conDoc.getAppTyp())) 
						 ? conDoc.getDocNumber() : conDoc.getTechKey().getIdAsString();
			if (docKey == null  ||  "".equals(docKey)) {
				// Without a document key no link can be provided
				return retVal;
			}
			String docID = conDoc.getDocNumber();
			String docType = ("CRMB".equals(conDoc.getAppTyp()) ? "billing" : "order");
			String docOrigin = ("CRMB".equals(conDoc.getAppTyp()) ? "CRM" : conDoc.getDocumentsOrigin());
			String navType = (! startupParam.isBillingIncluded() 
						  &&  startupParam.isPortal()
						  &&  ! "billing".equals(docType) 
						  &&  getPortalAnchorAttributesForSalesDocDisplay() != null
						  ? "PORTAL" : "SHOP");  
			retVal = ("onclick=\"docDisplay(") + ("'"+navType+ "',") + ("'"+docKey+"',")
					 + ("'"+docID+"',") + ("'"+docType+"',") + ("'"+docOrigin+"'") + ");\"";   
			return retVal;
    }

   /**
    * <b>NOT IMPLEMENTED returns allways empty String</b><br>
    * Write out JScript onClick event depending on the existens of a referenced doc.
    * @return String like 'onclick="documentDisplay('dockey', 'docno', 'doctype');"
   */
    public String writeItemRefOnClickEvent(ConnectedDocumentItem conDocItem) {
	     return "";
    }    
}