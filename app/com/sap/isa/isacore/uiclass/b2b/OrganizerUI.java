/*
 * Created on 12.05.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.isacore.uiclass.b2b;

import javax.servlet.jsp.PageContext;

import com.sap.isa.businessobject.IsaQuickSearch;
import com.sap.isa.isacore.action.InitQuickSearchAction;
import com.sap.isa.isacore.uiclass.IsaBaseUI;

/**
 * @author d033969
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class OrganizerUI extends IsaBaseUI {
	
	private IsaQuickSearch iqs = null;
	
	public OrganizerUI(PageContext pageContext) {
		super(pageContext);
			
		this.iqs = (IsaQuickSearch) request.getAttribute(InitQuickSearchAction.QS_OBJECT);
	}

	public IsaQuickSearch getIqs(){
		
		return this.iqs;
	}

}// End of Class
