/*
 * Created on 24.05.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.isacore.uiclass.b2b;

import javax.servlet.jsp.PageContext;

import com.sap.isa.isacore.action.IsaCoreInitAction;
import com.sap.isa.isacore.action.b2b.WorkareaNavAction;
import com.sap.isa.isacore.uiclass.IsaBaseUI;


/**
 * @author d033969
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class DocNavUI extends IsaBaseUI{

	private boolean quotationExtended = false;
	private boolean backendR3 = true;
	private boolean homActivated = false;
	private String itemSize = "";
	private String basketItemSize = ""; // contains data for displaying
	private String netValue = "";
	private String basketNetValue = "";
	private String currency = "";
	private String basketCurrency = "";
	
	
	public DocNavUI(PageContext pageContext) {
		super(pageContext);
		
		itemSize = (String) request.getAttribute(WorkareaNavAction.RK_MB_ITEMSIZE);
		netValue = (String) request.getAttribute(WorkareaNavAction.RK_MB_NETVALUE);
		currency = (String) request.getAttribute(WorkareaNavAction.RK_MB_CURRENCY);
	}

	public void setQuotationExtended(boolean prop){
		quotationExtended = prop;
	}

	public void setBackendR3(boolean prop){
		backendR3 = prop;
	}

	public void setHomActivated(boolean prop){
		homActivated = prop;
	}

	public boolean getOciTransfer() {
		int plength = 0;
		plength = ((IsaCoreInitAction.StartupParameter)userSessionData.getAttribute(IsaCoreInitAction.SC_STARTUP_PARAMETER)).getHookUrl().length();
		if(plength > 0)	
			return true;
		else
			return false;
	}

	public String getItemSize(){
		return this.itemSize;
	}

	public String getBasketItemSize(){
		return this.basketItemSize;
	}

	public String getNetValue(){
		return this.netValue;
	}

	public String getBasketNetValue(){
		return this.basketNetValue;
	}

	public String getCurrency(){
		return this.currency;
	}

	public String getBasketCurrency(){
		return this.basketCurrency;
	}

	public String getMbDoctype(){
		return (String) request.getAttribute(WorkareaNavAction.RK_MB_DOCTYPE);
	}

	public String getBasketLinkText(){
		String retvalue;
		
		if (getMbDoctype().equals("negotiatedContract")){
			//note: when a 'negotiated contract' is edited only itemsize should be displayed in minibasket
			retvalue = "b2b.header.minibasket.ncontract";
			this.basketItemSize = this.itemSize;
		} 
		else {
			//note: used for all sales documents except 'negotiated contracts'
			if((getItemSize().equals("0")) && (getNetValue().equals("0"))){
				//true when no editable document is available or editable document is no sales document
				//path is never be used in HOM scenario
				retvalue = "b2b.header.minibasket.default";
			}
			else {
				if ((getItemSize().equals("0")) && (getNetValue().equals("0"))) {
					//different minibasket descriptions for hom and standard 
					if(this.homActivated){
						retvalue = "b2b.header.hom.minibasket.empty";
					}
					else{
						retvalue = "b2b.header.minibasket.empty";
					}
				}
				else{
					//same description for standard b2b and hom scenario
					retvalue = "b2b.header.minibasket.l1";
					this.basketItemSize = this.itemSize;
					this.basketNetValue = this.netValue;
					this.basketCurrency = this.currency;
				}
			} 
		}
		
		return retvalue;
	}


} //end of class
