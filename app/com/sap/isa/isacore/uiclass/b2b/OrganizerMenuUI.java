
package com.sap.isa.isacore.uiclass.b2b;

import javax.servlet.jsp.PageContext;

import com.sap.isa.core.util.WebUtil;
import com.sap.isa.isacore.TabStripHelper;
import com.sap.isa.isacore.TabStripHelper.TabButton;
import com.sap.isa.isacore.uiclass.IsaBaseUI;

/**
 * @author d033969
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class OrganizerMenuUI extends IsaBaseUI {
	
	
	private boolean customerdocsearch 		= true;
	private boolean docsearch         		= false;
	private boolean genericdocsearch  		= false;
	private boolean contractsearch          = false;
	private boolean productsearch     		= true;
	private boolean customersearch    		= true;
	private boolean catalog           		= true;
	private boolean catalogonly				= false;
	private boolean externalCatalogVisible	= false;
	
	private TabStripHelper tabStrip;
	private TabStripHelper.TabButton tabButton;
	
	private String activeView = "";
	
	/**
	 * 
	 * @param pageContext
	 * Constructor
	 */ 
	public OrganizerMenuUI(PageContext pageContext) {
		super(pageContext);
		
		tabStrip = new TabStripHelper();
	}


	public TabStripHelper getTabStrip(String activeview){
		setActiveView(activeview);
		buildTabStrip();
		
		return tabStrip;
	}

	private void buildTabStrip(){
		
		/* All menu entries are defined within this section */

		String appsUrl = null;

		/* first add all buttons, which will be used */
		/* Customer Document Search ******************************************************************* */
		if (customerdocsearch) {
			appsUrl = WebUtil.getAppsURL(pageContext, null, "b2b/organizer-nav-customer-doc-search.jsp", null, null, false);

			tabButton = tabStrip.createTabButton("customerdocsearch",
				"b2b.org_nav_custdocsearch.l1",
				 appsUrl+ "\" onclick=\"load_customerdocSearch();");
			/* tabButton.setCustom1("b2b.org_nav_custdocsearch.l2"); */
			tabButton.setCustom2("document");
			tabStrip.addTabButton(tabButton);
		}

		/* Document Search ******************************************************************* */
		if (docsearch) {
			appsUrl = WebUtil.getAppsURL(pageContext, null, "b2b/organizer-nav-doc-search.jsp", null, null, false);

			tabButton = tabStrip.createTabButton("docsearch",
				"b2b.org_nav_docsearch.l2",
				appsUrl+ "\" onclick=\"load_docSearch();");
			/* tabButton.setCustom1("b2b.org_nav_docsearch.l2"); */
			tabButton.setCustom2("document");
			tabStrip.addTabButton(tabButton);
		}
		
		/* Contract Search ******************************************************************* */
		if (contractsearch ) {
			appsUrl = WebUtil.getAppsURL(pageContext, null, "b2b/organizer-nav-doc-search.jsp", null, null, false);

			tabButton = tabStrip.createTabButton("contractsearch",
				"b2b.org_nav_docsearch.l2",
				appsUrl+ "\" onclick=\"load_contractSearch();");
			/* tabButton.setCustom1("b2b.org_nav_docsearch.l2"); */
			tabButton.setCustom2("document");
			tabStrip.addTabButton(tabButton);
		}		

		/* Generic Document Search ************************************************************ */
		if (genericdocsearch ) {
			appsUrl = WebUtil.getAppsURL(pageContext, null, "b2b/organizer-nav-doc-search.jsp", null, null, false);

			tabButton = tabStrip.createTabButton("genericdocsearch",
				"b2b.org_nav_docsearch.l2",
				appsUrl+ "\" onclick=\"load_genericdocSearch();");
			/* tabButton.setCustom1("b2b.org_nav_docsearch.l2"); */
			tabButton.setCustom2("document");
			tabStrip.addTabButton(tabButton);
		}

		/* Customer Search *******************************************************************  */
		if (customersearch) {
			appsUrl = WebUtil.getAppsURL(pageContext, null, "/b2b/organizer-nav-customer-search.jsp", null, null, false);

			tabButton = tabStrip.createTabButton("customersearch",
				"b2b.org_nav_customersearch.l1",
				appsUrl+ "\" onclick=\"load_CustomerSearch();");
			/* tabButton.setCustom1("b2b.org_nav_customersearch.l2"); */
			tabButton.setCustom2("document");
			tabStrip.addTabButton(tabButton);
		}

		/* Product Search *******************************************************************  */
		if (productsearch) {
			appsUrl = WebUtil.getAppsURL(pageContext, null, "/b2b/organizer-nav-product-search.jsp", null, null, false);

			tabButton = tabStrip.createTabButton("productsearch",
				"b2b.org_nav_productsearch.l1",
				appsUrl+ "\" onclick=\"load_ProductSearch();");
			/* tabButton.setCustom1("b2b.org_nav_productsearch.link2"); */
			tabButton.setCustom2("document");
			tabStrip.addTabButton(tabButton);
		}

		/* Catalog Tab *******************************************************************  */
		if (catalog) {
			appsUrl = WebUtil.getAppsURL(pageContext, null, "/b2b/organizer-nav-catalog.jsp", null, null, false);

			tabButton = tabStrip.createTabButton("catalog",
				"b2b.organizer_nav_catalog.link1",
				appsUrl+ "\" onclick=\"load_catalog();");
			tabButton.setCustom1("b2b.organizer_nav_catalog.link2");
			tabButton.setCustom2("catalog");
			tabStrip.addTabButton(tabButton);
		}
	}


	private void setActiveView(String aview){
		this.activeView = aview;
		
		if ((activeView.equals("genericdocsearch")) || (activeView.equals("docsearch")) || (activeView.equals("contractsearch")) || (activeView.equals("productsearch")) || (activeView.equals("customerdocsearch")) || (activeView.equals("customersearch"))) {
			catalog = false;
	  	}
  
		/* disable all Buttons in catalog view - show only catalog header */
		if (activeView.equals("catalog")) {
			catalogonly = true;
			customerdocsearch = false;
			docsearch = false;
			genericdocsearch = false;
			contractsearch = false;
			productsearch = false;
			customersearch = false;
			catalog = false;
		}

	}
	
	public TabButton getTabButton(){
		return tabButton;
	}

	/* Get and ste methods for boolean variables */

	public void setCustomerdocsearch(boolean cdsearch){
		this.customerdocsearch = cdsearch;
	}

	public boolean getCustomerdocsearch(){
		return this.customerdocsearch;
	}

	public void setDocsearch(boolean dsearch){
		this.docsearch = dsearch;
	}

	public boolean getDocsearch(){
		return this.docsearch;
	}
	
	public void setContractsearch(boolean cntsearch){
		this.contractsearch = cntsearch;
	}

	public boolean getContractsearch(){
		return this.contractsearch;
	}
	
	public void setGenericdocsearch(boolean gendocsearch){
		this.genericdocsearch = gendocsearch;
	}
	
	public boolean setGenericdocsearch(){
		return this.genericdocsearch;
	}
	
	public void setProductsearch(boolean psearch){
		this.productsearch = psearch;
	}
	
	public boolean getProductsearch(){
		return this.productsearch;
	}
	
	public void setCustomersearch(boolean custsearch){
		this.customersearch = custsearch;
	}
	
	public boolean getCustomersearch(){
		return this.customersearch;
	}
	
	public void setCatalog(boolean catalog){
		this.catalog = catalog;
	}
	
	public boolean getCatalog(){
		return this.catalog;
	}
	
	public void setCatalogonly(boolean catonly){
		this.catalogonly = catonly;
	}
	
	public boolean getCatalogonly(){
		return this.catalogonly;
	}
	
	public void setExternalCatalogVisible(boolean ecatvisible){
		this.externalCatalogVisible = ecatvisible;
	}
	
	public boolean getExternalCatalogVisible(){
		return this.externalCatalogVisible;
	}
}
