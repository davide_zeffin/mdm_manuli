/*
 * Created on 08.06.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.isacore.uiclass.b2b;

import javax.servlet.jsp.PageContext;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.isacore.uiclass.IsaBaseUI;

/**
 * @author d033969
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class LoginUI extends IsaBaseUI {
	
	public LoginUI(PageContext pageContext){
		super(pageContext);
	}
	
	/** This method retreives the maximum password length which is important for password change
	* default length: 8
	* @param user
	* @return maximum password length
	* @throws CommunicationException 
	*/
	
	public int getPasswordLength() {
		int maxLength = 8;
		try {
			maxLength = this.bom.getUser().getPasswordLength();
		} catch (CommunicationException e) {
			log.error("Something went wrong determining the maximum password length. Set the default max length to 8 characters.", e);
		}
		return maxLength;
	}

}
