package com.sap.isa.isacore.uiclass.b2b;

import javax.servlet.jsp.PageContext;

import com.sap.isa.backend.boi.isacore.order.DocumentListFilterData;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.User;
import com.sap.isa.isacore.uiclass.IsaBaseUI;

/**
 * The class HeaderUI.java .... .
 *
 * @author SAP
 * @version 1.0
 **/
public class HeaderUI extends IsaBaseUI {
	
	public boolean homActivated = false; 
	public boolean internalCatalogVisible = true;
	public boolean ociEnable = false;
	public String externalCatalogURL = "";
	public String catalogLink = "";
	
	protected boolean hasCollOrderPermission = false;

	public String enableHipbone = "";

	/**
	 * Constructor for HeaderUI.
	 * @param pageContext
	 */	
	public HeaderUI(PageContext pageContext) {
		
		super(pageContext);
		
		
		homActivated = shop.isHomActivated();
		
		ociEnable = shop.isOciAllowed();
		internalCatalogVisible = shop.isInternalCatalogAvailable();
		externalCatalogURL = shop.getExternalCatalogURL();
		
		enableHipbone = pageContext.getServletContext().getInitParameter("enablehipbone.cic.isa.sap.com");
		
		if ((internalCatalogVisible) && (ociEnable)) {
			catalogLink = "b2b.header.catalogs";
		} else {
			catalogLink = "b2b.header.catalog";
		}
		
		//check for all create permissions
		User user = bom.getUser();
		
		String[] documents = new String[]{DocumentListFilterData.SALESDOCUMENT_TYPE_ORDER,
										  DocumentListFilterData.SALESDOCUMENT_TYPE_ORDER,};
		String[] actions   = new String[]{DocumentListFilterData.ACTION_CREATE,
										  DocumentListFilterData.ACTION_CHANGE };											
	  try{    
		Boolean[] checkResults = checkPermissions(user, documents, actions);	    									
		hasCollOrderPermission = checkResults[0].booleanValue() && checkResults[1].booleanValue();
		}
	  catch(CommunicationException Ex) {
	  	log.debug(Ex.getMessage());
		}
			
	}
	
	/**
	 * Returns if the mini basket should be displayed.
	 * 
	 */
	public boolean isMiniBasketAvailable() {
		return !homActivated || hasCollOrderPermission;	
	}
	
	/**
	 * Returns the property storeLocatorAvailable
	 * @return boolean
	 */
	public boolean isStoreLocatorAvailable() {
		return shop.isPartnerLocatorAvailable();
	}
	
}
