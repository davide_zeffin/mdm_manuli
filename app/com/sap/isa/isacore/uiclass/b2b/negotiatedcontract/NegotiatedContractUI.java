/*****************************************************************************
    Class:        NegotiatedContractUI
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      01.10.2004
    Version:      1.0

    $Revision: #1$
    $Date: 2004/10/01 $ 
*****************************************************************************/
package com.sap.isa.isacore.uiclass.b2b.negotiatedcontract;

import javax.servlet.jsp.PageContext;

import com.sap.isa.isacore.action.negotiatedcontract.NegotiatedContractConstants;
import com.sap.isa.businessobject.header.*;
import com.sap.isa.isacore.uiclass.b2b.order.SalesDocumentUI;
import com.sap.isa.businessobject.item.*;
import com.sap.isa.core.util.table.ResultData;



public class NegotiatedContractUI extends SalesDocumentUI {

	/** 
	 * Flag for type of contract (quantity related or value related contract)
	*/
	public String contractType = null;
	
	/** 
	 *  Header of the contract
	 */
	public HeaderNegotiatedContract header = null;
	
	/** 
	 *  Items  of the contract
	 */
	public ItemList items = null;
	
	/** 
	 *  Table of available condition types for price agreements
	 */	
	public ResultData condTypes = null;
	
	/**
	 * Hold the item currently used. The item must be set with the {@link #setItem}
	 * method. <br>
	 */	
	protected ItemNegotiatedContract item;
	
	/**
	 * Is true, if an calculated price of the item currently used is available
	 * method. <br>
	 */	
	protected boolean itemCalculatedPriceExists;
	
	/**
	 * Is true, if an offered price of the item currently used is available
	 * method. <br>
	 */	
	protected boolean itemOfferedPriceExists;
	
	/**
	 * Is true, if the normal price of the item currently used could be determined
	 * method. <br>
	 */	
	protected boolean itemNormalPriceExists;
	
	
		
	/**
	 * Constructor for NegotiatedContractUI.
	 *
	 * @param pageContext
	 */
	public NegotiatedContractUI(PageContext pageContext) {
	
		super(pageContext);
		header	= (HeaderNegotiatedContract) request.getAttribute(NegotiatedContractConstants.RC_HEADER);	 
		contractType = (String) request.getAttribute(NegotiatedContractConstants.RC_CONTRACT_TYPE);

		items	= (ItemList) request.getAttribute(NegotiatedContractConstants.RC_ITEMS);		
		
	}
	
	
	/**
	 * Sets and initialize the current item data
	 * 
	 * @param item The current item to be set
	 */
	public void setItem(ItemNegotiatedContract item) {
	    super.setItem(item);
	    this.item = item;
	    condTypes = item.getCondValues();
	    if  (item.getCondIdInquired() != null && item.getCondIdInquired().length() > 0 && item.getPriceInquired().length() > 0 && item.getUnitPriceInquired().length() > 0 ) {											
			itemCalculatedPriceExists = true;
	    } else {
			itemCalculatedPriceExists = false;
	    }
		if (item.getCondIdOffered() != null && item.getCondIdOffered().length() > 0 && item.getPriceOffered().length() > 0 && item.getUnitPriceOffered().length() > 0) {											
			itemOfferedPriceExists = true;
		} else {
			itemOfferedPriceExists = false;		
		}  
		if (item.getUnitPriceNormal().length() > 0) {
			itemNormalPriceExists = true;
		} else {
			itemNormalPriceExists = false;		
		}
	}

    /**
     * Returns the Colspan value for the items table<br>
     *  
     * @return the Colspan value for the items table
     */
    public int getItemsColspan() {
        return 11;
    }

	/**
	 * Sets the string value " selected",  if the condition type (discount) was selected for the current item
	 * 
	 * @param condType The current condType of the select box
	 * @return the selected string
	 */	
	public String getItemCondTypeSelected (ResultData condType) {
		String selected ="";
		String condKey =  condType.getString(ItemNegotiatedContract.CONDITION_TYPE); 
		 if ( condKey.equals((item.getCondTypeInquired())) ) {
			selected = " selected";
		 } 
		 return selected;
	}

	/**
	 * Returns the description of the condition type (discount) 
	 * 
	 * @param condType The condition type 
	 * @return the condition type description
	 */		
	public String getCondTypeDescription (ResultData condType) {

		String condValue =  condType.getString(ItemNegotiatedContract.DESCRIPTION); 
		return condValue;
	}

	/**
	 * Returns the type of value of the condition type (discount): 
	 * - discount in percent: %
	 * - value discount: the currency of the item
	 * 
	 * @param condType The current condition type of the select box
	 * @return the type of the value of the condition type 
	 */	
	public String getCondValueType (ResultData condType) {

		String condValueType; 
		if ( condType.getString(ItemNegotiatedContract.CONDITION_CLASS).equalsIgnoreCase(ItemNegotiatedContract.CONDITION_CLASS_REBATE )) {
		   condValueType = "%";
		}
		else {
		   condValueType = item.getCurrency();
		}  		
		return condValueType;
	}

	/**
	 * Returns the key of condition type 
	 * 
	 * @param condType The condition type
	 * @return the key of the condition type
	 */		
	public String getCondTypeKey (ResultData condType) {

		String condKey =  condType.getString(ItemNegotiatedContract.CONDITION_TYPE); 
		return condKey;
	}
	
	/**
	 * Returns the string " selected" if the unit was selected for the condition  
	 * 
	 * @param condUnit The unit of the condition type
	 * @return the string selected 
	 */			
	public String getItemCondUnitSelected (String condUnit) {
		String selected = "";
		if (condUnit.equals(item.getCondUnitInquired())) {
		  selected = " selected";
		}
		return selected;
	}
	
	/**
	 * Returns true if an calculated price on base of an inquired discount is available
	 * 
	 * @return
	 */
	public boolean itemCalculatedPriceExists () {
		return itemCalculatedPriceExists;
	}

    /**
     * Returns true, if an offered price on base of an offered discount is available
     * 
     * @return
     */	
	public boolean itemOfferedPriceExists () {
		return itemOfferedPriceExists;
	}
	
	/**
	 * Returns true, if the normal price of the item product is available
	 * 
	 * @return
	 */	
	public boolean itemNormalPriceExists () {
		return itemNormalPriceExists;
	}
	
	
}
