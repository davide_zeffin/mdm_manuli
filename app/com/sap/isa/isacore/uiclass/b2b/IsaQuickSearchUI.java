/*
 * Created on 22.09.2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.isacore.uiclass.b2b;

import javax.servlet.jsp.PageContext;

import com.sap.isa.businessobject.Shop;
import com.sap.isa.isacore.uiclass.IsaBaseUI;

/**
 * @author d025715
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class IsaQuickSearchUI extends IsaBaseUI {

    /**
     * Switch to show list price. <br>
     */
    protected boolean listPriceAvailable = false;

    /**
     * Switch to show customer specific prices. <br>
     */
    protected boolean customerSpecificPriceAvailable = false; 

    /**
     * Standard constructor. <br>
     * 
     * @param pageContext
     */
    public IsaQuickSearchUI(PageContext pageContext) {
        initContext(pageContext);        
    }

    
    /**
     * Standard constructor. <br>
     * 
     * @param pageContext
     */
    public IsaQuickSearchUI() {
    }

    
    /**
     * Initialize UI clasee . <br>
     * 
     * @param pageContext current page context
     * 
     * 
     * @see com.sap.isa.ui.uiclass.BaseUI#initContext(javax.servlet.jsp.PageContext)
     */
    public void initContext(PageContext pageContext) {
        
        super.initContext(pageContext);
        
        // visiblity of prices.
        if (isPriceVisible()) {
            if (shop.isListPriceUsed()) {
                listPriceAvailable = true;
            }
    
            if (shop.isIPCPriceUsed()) {
                customerSpecificPriceAvailable = true;
            }
        }
    }

    /**
     * @return
     */
    public boolean isCustomerSpecificPriceAvailable() {
        return customerSpecificPriceAvailable;
    }

    /**
     * @return
     */
    public boolean isListPriceAvailable() {
        return listPriceAvailable;
    }

    /**
     * @return isBackendR3 as boolean, shopbackend as string
     */
    public boolean isBackendR3() {
        String shopBackend = shop.getBackend();
        boolean isBackendR3 = shopBackend.equals(Shop.BACKEND_R3) || shopBackend.equals(Shop.BACKEND_R3_40) ||
                              shopBackend.equals(Shop.BACKEND_R3_45) || shopBackend.equals(Shop.BACKEND_R3_46) ||
                              shopBackend.equals(Shop.BACKEND_R3_PI);
        return isBackendR3;
    }

}


