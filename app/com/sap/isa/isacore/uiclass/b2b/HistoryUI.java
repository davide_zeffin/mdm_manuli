/*
 * Created on 04.05.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.isacore.uiclass.b2b;

import com.sap.isa.core.util.WebUtil;
import com.sap.isa.isacore.uiclass.IsaBaseUI;
import javax.servlet.jsp.PageContext;
import com.sap.isa.isacore.HistoryItem;

/**
 * @author d033969
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class HistoryUI extends IsaBaseUI{
	
	private HistoryItem hitem = null;
	private String refNo = "";
	private String refName = "";
	
	public HistoryUI(PageContext pageContext) {
		super(pageContext);
			
	}

	public void setHistoryItem(HistoryItem hi){
		this.hitem = hi;
		this.refNo = hitem.getRefNumber();
		this.refName = hitem.getRefName();
	}
	
	public String getHref(){
		String href = "";
		
		if (hitem.getHref() != null) {
			href = hitem.getHref();
		}
		
		if (hitem.getHrefParameter() != null) {
			href = href + "?" + hitem.getHrefParameter();
		}
		return href;
	}
	
	public String getRefNo(){
		if (refNo == null) 
			refNo="";
		
		refNo = refNo.trim();
		
		//	cut when string is to long for history frame
		if (refNo.length() > 9) {
			refNo = refNo.substring(0,9);
			refNo = refNo + "...";
		}
		
		return refNo;
	}
	
	public String getRefName(){
		if (refName == null) 
			refName="";
			
		refName = refName.trim();
					
		// cut when string is to long for history frame
		if (refName.length() > 9) {
			refName = refName.substring(0,9);
			refName = refName + "...";
		}
	
		return refName;
	}
	
	
	public String getTooltipRefNo(){
		String tooltipRefNo = "";
		
		if (refNo == null  ||  refNo.trim().length()==0) {
			//reference_number not existing
			tooltipRefNo = WebUtil.translate(pageContext,"b2b.history.item.ref.none",null) + "\n";
		}
		else {
			tooltipRefNo = refNo + "\n";
		}
		
		return tooltipRefNo;
	}
	
	
	public String getTooltipRefName(){
		String tooltipRefName = "";
		
		if (refName == null  ||  refName.trim().length()==0) {
			//reference_name not existing
			tooltipRefName = WebUtil.translate(pageContext,"b2b.history.item.ref.none",null);
		}
		else {
			tooltipRefName = refName;
		}
		
		return tooltipRefName;
	}
	
	public String getDocNumber(){
		if(hitem.getDocNumber() != null)
			return hitem.getDocNumber();
		else
			return "";
	}
	
	public String getDate(){
		if(hitem.getDate() != null)
			return hitem.getDate();
		else
			return "";
	}
	
	public String getDocType(){
		if(hitem.getDocType() != null)
			return hitem.getDocType();
		else
			return "";
	}
	
	public String getKey(){
		if(!getDocType().equals("")){
			String key = "b2b.cookie." + this.getDocType();
			return key;
		}
		else
			return "";
	}
	
} //End of class
