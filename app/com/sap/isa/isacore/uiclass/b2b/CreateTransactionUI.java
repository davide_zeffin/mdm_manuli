/*****************************************************************************
	Class:        CreateTransactionUI
	Copyright (c) 2004, SAP AG, Germany, All rights reserved.

	$Revision: #1 $
	$DateTime: 2004/01/09 10:09:38 $ (Last changed)
    
*****************************************************************************/

package com.sap.isa.isacore.uiclass.b2b;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.jsp.PageContext;

import com.sap.ecommerce.businessobject.campaign.Campaign;
import com.sap.ecommerce.businessobject.campaign.CampaignBusinessObjectManager;
import com.sap.ecommerce.businessobject.campaign.CampaignSearchCriteria;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.order.DocumentListFilterData;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.User;
import com.sap.isa.businesspartner.action.GetPartnerHierarchyAction;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.businesspartner.businessobject.PartnerFunctionBase;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.MessageList;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.helpvalues.HelpValuesSearch;
import com.sap.isa.helpvalues.uiclass.FieldSupport;
import com.sap.isa.isacore.IsaOnBehalfPermission;
import com.sap.isa.isacore.action.IsaCoreInitAction;
import com.sap.isa.isacore.action.b2b.GetDealerFamilyAction;
import com.sap.isa.isacore.uiclass.IsaBaseUI;

/**
 * The class CreateTransactionUI.java .... .
 *
 * @author SAP
 * @version 1.0
 **/
public class CreateTransactionUI extends IsaBaseUI {
	
	public boolean quotationExtended = false;
	public boolean backendR3 = true;
    public boolean isBackendR3AgentScenario = false;
	public boolean ociEnable = false;
	public boolean internalCatalogVisible = false;
	
	public boolean ociTransfer = false;
	public String catalogUrl;
	
	public ResultData soldToList = null;
	
	public boolean lateDecision = false;
    public boolean isSoldToSelectable = false;
    public boolean isEnrollmentAllowed = false;
	public boolean templateAllowed = false;
	public boolean quotationAllowed = false;
    public boolean quotationOrderingAllowed = false;
	public boolean contractNegotiationAllowed = false;
	public boolean welcome = false;
	public boolean minibasket = false;
	//flags for document create permissions
	public boolean hasCreateOrderPermission = false;
	public boolean hasCreateContPermission = false;
	public boolean hasCreateTempPermission = false;
	public boolean hasCreateQuotPermission = false;
	public boolean hasDispQuotPermission = false;
	public boolean hasCollOrderPermission = false;
    public boolean hasDispEnrollmPermission = false;
    public boolean hasChangeEnrollmPermission = false;
	//
	//flags for document create on behalf permissions
  	public boolean hasCreateOrderOBPermission = false;
  	public boolean hasCreateContOBPermission = false;
  	public boolean hasCreateTempOBPermission = false;
  	public boolean hasCreateQuotOBPermission = false;
    public boolean hasEnrollmentOBPermission = false;
	
	public ResultData processTypes = null;
	public ResultData quotationProcessTypes = null;
	public ResultData contractNegotiationProcessTypes = null;
	
	public int numRowsOrder = 0;
	public String lastProcessTypeOrder = null;
	public int numRowsQuotation = 0;
	public String lastProcessTypeQuotation = null; 
	public int numRowsContract = 0;
	public String lastProcessTypeContract = null;

    // campaign enrollment
    protected Campaign campaign = null;
	
	/**
	 * Name of the process types property.
	 */
	public static final String PROCESS_TYPES = "processTypes";
    
	/**
	 * Name of the quotation process types property.
	 */
	public static final String QUOTATION_PROCESS_TYPES = "quotationProcessTypes";
	
	/**
	 * Name of the contract negotiation process types property.
	 */
	public static final String CONTRACT_NEGOTIATION_PROCESS_TYPES = "contractNegotiationProcessTypes";  
      
    /**
     * Name of the F4 Customer search.
     */
    public static final String HELP_VALUE_CUSTOMER_SEARCH = "Customer";

    protected static final String RC_CAMPAIGN = "campaign";

    /**
    * List of messages
    */
    public final static String MESSAGE_LIST = "MessageList";

    // Selection BP hierarchy
    public static final String SELBP_MY    = "MY";
    public static final String SELBP_ALL   = "ALL";
    public static final String SELBP_SEL   = "SEL";
	
	/**
	 * Constructor for CreateTransactionUI
	 * @param pageContext
	 */
	public CreateTransactionUI(PageContext pageContext) {
	
		super(pageContext);
		
		// Get the shop properties
		quotationExtended = shop.isQuotationExtended();
		if (shop.getBackend().equals(ShopData.BACKEND_CRM)) {
			backendR3 = false;
		}
		//check for all create permissions
		  User user = bom.getUser();
		  boolean hasDispTempPermission = false;
		  boolean hasChangeOrderPermission = false;		 
		  String[] documents = new String[]{DocumentListFilterData.SALESDOCUMENT_TYPE_ORDER,
											DocumentListFilterData.DOCUMENT_TYPE_CONTRACT,
											DocumentListFilterData.SALESDOCUMENT_TYPE_ORDERTEMPLATE,
											DocumentListFilterData.SALESDOCUMENT_TYPE_QUOTATION,
                                            DocumentListFilterData.DOCUMENT_TYPE_CAMPAIGN,
											DocumentListFilterData.SALESDOCUMENT_TYPE_ORDERTEMPLATE,
											DocumentListFilterData.SALESDOCUMENT_TYPE_QUOTATION,
											DocumentListFilterData.SALESDOCUMENT_TYPE_ORDER,
                                            DocumentListFilterData.DOCUMENT_TYPE_CAMPAIGN};
		  String[] actions   = new String[]{DocumentListFilterData.ACTION_CREATE,
											DocumentListFilterData.ACTION_CREATE,
											DocumentListFilterData.ACTION_CREATE,
											DocumentListFilterData.ACTION_CREATE,
                                            DocumentListFilterData.ACTION_READ,
                                            DocumentListFilterData.ACTION_READ,
											DocumentListFilterData.ACTION_READ,
                                            DocumentListFilterData.ACTION_CHANGE,                                           
											DocumentListFilterData.ACTION_CHANGE };											
	    try{    
		  Boolean[] checkResults = checkPermissions(user, documents, actions);	    									
		  hasCreateOrderPermission = checkResults[0].booleanValue();
		  hasCreateContPermission = checkResults[1].booleanValue();
		  hasCreateTempPermission = checkResults[2].booleanValue();
		  hasCreateQuotPermission = checkResults[3].booleanValue();
          hasDispEnrollmPermission = checkResults[4].booleanValue();
		  hasDispTempPermission   = checkResults[5].booleanValue();
		  hasDispQuotPermission   = checkResults[6].booleanValue();
		  hasChangeOrderPermission = checkResults[7].booleanValue();
          hasChangeEnrollmPermission = checkResults[8].booleanValue();
		  }
		catch(CommunicationException Ex) {
			log.debug(Ex.getMessage());
		  }
		templateAllowed = shop.isTemplateAllowed() && hasDispTempPermission;
		quotationAllowed = shop.isQuotationAllowed()&& hasCreateQuotPermission;
        quotationOrderingAllowed = shop.isQuotationOrderingAllowed() && hasDispQuotPermission;
		contractNegotiationAllowed = shop.isContractNegotiationAllowed() &&
														hasCreateContPermission ;
 		hasCollOrderPermission = hasCreateOrderPermission && hasChangeOrderPermission;
		ociEnable = shop.isOciAllowed();
		internalCatalogVisible = shop.isInternalCatalogAvailable();
		catalogUrl = shop.getExternalCatalogURL();
		lateDecision = shop.isDocTypeLateDecision();
        isSoldToSelectable = shop.isSoldtoSelectable();
        isEnrollmentAllowed = shop.isEnrollmentAllowed();
		
		// Check if oci transfer is required
		ociTransfer =
			(((IsaCoreInitAction.StartupParameter)userSessionData.
				getAttribute(IsaCoreInitAction.SC_STARTUP_PARAMETER)).
				getHookUrl().length() > 0);
		
		// Get dealer family list
		soldToList = (ResultData) request.getAttribute(GetDealerFamilyAction.DEALER_FAMILY_LIST);
		
		//In case there are is a soldtolist found,Check for all required on behalf permissions
		if ( isSoldToListVisible() ){		
            try {
                documents = new String[]{DocumentListFilterData.SALESDOCUMENT_TYPE_ORDER,
                                         DocumentListFilterData.DOCUMENT_TYPE_CONTRACT,
                                         DocumentListFilterData.SALESDOCUMENT_TYPE_ORDERTEMPLATE,
                                         DocumentListFilterData.SALESDOCUMENT_TYPE_QUOTATION,
                                         DocumentListFilterData.DOCUMENT_TYPE_CAMPAIGN};
                String[] actvt = new String[]{DocumentListFilterData.ACTVT_CREATE,
                                              DocumentListFilterData.ACTVT_CREATE,
                                              DocumentListFilterData.ACTVT_CREATE,
                                              DocumentListFilterData.ACTVT_CREATE,
                                              DocumentListFilterData.ACTVT_DISPLAY};                                           
			    Boolean[] resultsOnBehalf = checkPermissionsOB(user, documents, actvt);
			    hasCreateOrderOBPermission = resultsOnBehalf[0].booleanValue() && isSoldToListVisible();
			    hasCreateContOBPermission = resultsOnBehalf[1].booleanValue() && isSoldToListVisible();
			    hasCreateTempOBPermission = resultsOnBehalf[2].booleanValue() && isSoldToListVisible();
			    hasCreateQuotOBPermission = resultsOnBehalf[3].booleanValue() && isSoldToListVisible();
                hasEnrollmentOBPermission = resultsOnBehalf[4].booleanValue() && isSoldToListVisible();
		    }
		    catch(CommunicationException Ex) {
				log.debug(Ex.getMessage());
		    }		
		}
		// Determination of order process types
		processTypes = shop.getProcessTypes();
		if (processTypes != null) {
			numRowsOrder = processTypes.getNumRows();
			if (numRowsOrder > 1) {
				lastProcessTypeOrder = getLastProcessTyp(processTypes);
			}
		}
		
		// Determination of quotation process types
		if (shop.isQuotationExtended()) {
			quotationProcessTypes = shop.getQuotationProcessTypes();
			if(quotationProcessTypes != null) {
				numRowsQuotation = quotationProcessTypes.getNumRows();
				if (numRowsQuotation > 1) {
					lastProcessTypeQuotation = getLastProcessTyp(quotationProcessTypes); 
				}
			}
		}
        else {
            quotationProcessTypes =processTypes;
            numRowsQuotation = numRowsOrder;
            lastProcessTypeQuotation = lastProcessTypeOrder; 
        }
		
		// Determination of contract process types
		if (contractNegotiationAllowed) {
			contractNegotiationProcessTypes = shop.getContractNegotiationProcessTypes();
			if (contractNegotiationProcessTypes != null) {
				numRowsContract = contractNegotiationProcessTypes.getNumRows();
				if (numRowsContract > 1) {
					lastProcessTypeContract = getLastProcessTyp(contractNegotiationProcessTypes);
				}
			}
		}
		
		// Display welcome text
		if (request.getParameter("welcome") != null && request.getParameter("welcome").equals("1")) {
			welcome = true;
		}
		
		// Are we coming from catalog or minibasket
		if (request.getAttribute("reqsource") != null 
		    && request.getAttribute("reqsource").equals("minibasket")) {
	  		minibasket = true;
		}
        // Check for R/3 agent scenario
        if (backendR3) {
            BusinessPartnerManager bupaManager = bom.createBUPAManager();
            if (bupaManager.getDefaultBusinessPartner(PartnerFunctionData.AGENT) != null) {
                isBackendR3AgentScenario = true;
            }
        }

        // get Campaign BOM
        CampaignBusinessObjectManager campaignBom = (CampaignBusinessObjectManager)userSessionData.getBOM(CampaignBusinessObjectManager.CAMPAIGN_BOM);
        if (campaignBom != null) {
            campaign = (Campaign)campaignBom.getCampaign();
        }
	}	
	
	/**
	 * D041773 Message 993816 2006
	 * Returns <code>true</code> if Quotation or 'Quotation ordering' is switched on in the shop configuration and the user has the 
	 * persmission to create Orders. Otherwise <code>false</code>. This is needed by the welcome page for the
	 * switch to show the Quotation link or not. 
	 * @return Returns <code>true</code> if Quotation or 'Quotation ordering' is switched on in the shop configuration  
	 *       and the user has the persmission to create Orders. Otherwise <code>false</code>. 
	 */
	public boolean isQuotationAllowed() {
		return (quotationAllowed || quotationOrderingAllowed) && hasCreateOrderPermission && hasDispQuotPermission;
	}
	
	/**
	 * Returns the Resource Keys for createdocument.jsp
	 * @return String[]
	 */
	public String[] getInfoKeys() {
		if (this.minibasket) {
			return new String[] {"b2b.noDocument.info2", "b2b.noDocument.infoType2"};
		}
		return new String[] {"b2b.noDocument.info", "b2b.noDocument.infoType"};
	}
	
	/**
	 * Return the process typ if only one is in the ResultSet
	 * @param types
	 * @return String
	 */
	public String getFirstProcessTyp(ResultData procTypes) {
		procTypes.first();
		return procTypes.getString(1);
	}
	
	/**
	 * Return the last process typ in the ResultSet
	 * @param types
	 * @return String
	 */	
	public String getLastProcessTyp(ResultData procTypes) {
		procTypes.last();
		return procTypes.getString(1);
	}


	/**
	 * Return if list of possible soldto's should be displayed.  <br>
	 * 
	 * @return
	 */
	public boolean isSoldToListVisible() {
		return soldToList != null && soldToList.getNumRows() > 0;			
	}
		
	
	/**
	 * Return the name of the default sold to. <br>
	 * 
	 * @return String Name of the default sold to party or an empty string.
	 */
	public String getDefaultSoldToName() {
		
		String soldToName = "";
		BusinessPartnerManager buPaMa = bom.getBUPAManager();
		
		BusinessPartner soldTo = buPaMa.getDefaultBusinessPartner(PartnerFunctionBase.SOLDTO);
		
		if (soldTo != null) {
			soldToName = soldTo.getAddress().getName();
		}
		
		return soldToName;		
	}

	
	/**
	 * Check for the user Permission for the Creation of different Documents. <br>
	 * @param User for whom the permissions are required 
	 * @param String document for which permission is asked.
     * @param String activities which are checked
	 * @return Boolean True/False.
	 */
	public Boolean[] checkPermissionsOB(User user, String[] docs, String[] actvt) throws CommunicationException  {
		IsaOnBehalfPermission[] docPermissions = new IsaOnBehalfPermission[docs.length];
		for (int cnt = 0; cnt<docs.length; cnt++){
		    docPermissions[cnt] = new IsaOnBehalfPermission(docs[cnt], actvt[cnt]); 	    
		}
		return user.hasPermissions(docPermissions);	    
	}


	/**
	 * Returns true if the manualCampaignEntryAllowed flag
	 * is set in the shop management
	 * 
     * @return boolean
     */
    public boolean isManualCampaignEntryAllowed() {
		return shop.isManualCampainEntryAllowed();
	}
	
    /**
     * Return a field support field to select the sold to party. <br>
     * 
     * @param valueName name of the input field
     * @param formName name of the http form
     * @return FieldSupport
     */
    public FieldSupport getSoldToField(String valueName, String formName) {

        FieldSupport field = getSoldToFieldFromList(valueName, formName,
                                                    "b2b.makenewdoc.dealer", 
                                                    "b2b.makenewdoc.myself");
        
        return field;
    }
	
	/**
     * Return a field support field to select the sold to party. <br>
     * 
     * @param valueName name of the input field
     * @param formName name of the http form
     * @param label for the input field
     * @param myPrefix prefix for my business partner in the list
     * @return FieldSupport
     */
    public FieldSupport getSoldToFieldFromList(String valueName, String formName,
                                               String label, String myPrefix) {

		FieldSupport  field = new FieldSupport(pageContext,
											   "PartnerHierarchy",
											   formName,
											   valueName,
											   label,
											   new String[]{"name"} );
											  
		StringBuffer description = new StringBuffer();
		
		description.append(translate(myPrefix))
		           .append(" ")
		           .append(getDefaultSoldToName());
		        

		try {
            ResultData partners = GetPartnerHierarchyAction.getPartner(userSessionData, userSessionData.getMBOM());
            if (partners != null) {
            	field.setMaxSize(partners.getNumRows());
            }
        }
        catch (CommunicationException e) {
            log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "Failed to connect to backend", e);
        }		
		                   
		field.setFieldName(valueName);
		field.addAdditionalValue("", description.toString());
		
		return field;
	}
    
    /**
     * Return true if any work list is available. Currently following work lists are checked<br>
     * - shop.isBackorderSelectionAllowed()<br>
     * @return boolean true or false
     */
    public boolean isWorklistAvailable() {
        boolean retVal = false;
        if (shop.isBackorderSelectionAllowed()) {
            retVal = true;
        }
        return retVal;
    }
    /** 
     * Return true if shop allows backorder selection.
     * @return boolean true or false
     */ 
    public boolean isBackorderSelectionAllowed() {
        return shop.isBackorderSelectionAllowed();
    }
    /**
     * Return the backend and Scenario dependent search screen name for the generic framework
     * @param String document type according to generic-searchbackend-config.xml file
     * @return String search screen name
     */
    public String getDependentSearchScreenName(String docType) {
        String retVal = "SearchCriteria_B2B_Sales";
        if (isSoldToSelectable) {
            retVal = "SearchCriteria_BOB_Sales";
        }
        if (backendR3) {
            if ("ORDERTMP".equals(docType)) {
                retVal = "SearchCriteria_B2B_Ordertemplates";
            }
        }
        return retVal;
    }


    /**
     * Return the backend and Scenario dependent result list name for the generic framework
     * @param String document type according to generic-searchbackend-config.xml file 
     * @return String search screen name
     */
    public String getDependentResultListName(String docType) {
        String retVal = "";
        if (backendR3) {
            if ("ORDERTMP".equals(docType)) {
                retVal = "ordertmp_resultlist_B2B_DB_pre";
            }
            if ("QUOTATION".equals(docType)) {
                if (ociTransfer) {
                    retVal = "quot_resultlist_B2B_pre_oci";
                } else {
                    retVal = "quot_resultlist_B2B_pre";
                }
            }
        } else {
            if ("ORDERTMP".equals(docType)) {
                retVal = "ordertmp_resultlist_B2B_pre";
            }
            if ("QUOTATION".equals(docType)) {
                if (ociTransfer) {
                    retVal = "quot_resultlist_B2B_pre_oci";
                } else {
                    retVal = "quot_resultlist_B2B_pre";
                }
            }
        }
        return retVal;
    }
    
    /**
     * Returns the Mapping needed for the Customer Help Value search, to set the selected 
     * customer id on in the right field in the welcome.jsp
     *
     * @param fieldName name of the form field the select customer id should be set in
     * @return HashMap Mapping from the Help Values Customer Search field to the fieldName
     */
    public Map getCustomerSearchMapping(String fieldName) {
        HelpValuesSearch helpValuesSearch = getHelpValuesSearch(HELP_VALUE_CUSTOMER_SEARCH);
        Map parameterMapping = new HashMap();
        if (helpValuesSearch!=null) {
            parameterMapping.put(helpValuesSearch.getParameter().getName(),fieldName);
        }
       return parameterMapping;
    }
    
    /**
     * Returns <code>true</code> if Enrollment for Marketing Programs is allowed 
     * Otherwise <code>false</code>. This is needed by the welcome page for the
     * switch to show the Enrollment link or not. 
     * @return Returns <code>true</code> if enrollment for Marketing Programs is allwoed Otherwise <code>false</code>. 
     */
    public boolean isEnrollmentAllowed() {
        boolean isAllowed = isEnrollmentAllowed && hasDispEnrollmPermission;
        return isAllowed;
    }
    
    /**
     * Returns <code>true</code> if the business partner field should be visible
     * Otherwise <code>false</code>. This is needed by the welcome page for the
     * the selection of business partners for the enrollment of campaigns
     * @return Returns <code>true</code> if an entry in the business partner field is allowed Otherwise <code>false</code>. 
     */
    public boolean isBpDisplayed() {
        boolean isDisplayed = hasEnrollmentOBPermission && hasDispEnrollmPermission;
        return isDisplayed;
    }
    
    /**
     * Return a field support field to select a value from drop down list box. <br>
     * 
     * @param valueName name of the input field
     * @param formName name of the http form
     * @param label for the input field
     * @param myPrefix prefix for my business partner in the list
     * @return FieldSupport
     */
    public FieldSupport getSelectionFieldFromList(String valueName, String formName,
                                                  String label, String myPrefix) {

        FieldSupport  field = new FieldSupport(pageContext,
                                               "PartnerSelection",
                                               formName,
                                               valueName,
                                               label,
                                               new String[]{"name"} );
                                              
        StringBuffer description = new StringBuffer();
        
        description.append(translate(myPrefix))
                   .append(" ")
                   .append(getDefaultSoldToName());
                

        try {
            ResultData partners = GetPartnerHierarchyAction.getPartner(userSessionData, userSessionData.getMBOM());
            if (partners != null) {
                field.setMaxSize(partners.getNumRows());
            }
        }
        catch (CommunicationException e) {
            log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "Failed to connect to backend", e);
        }       
                           
        field.setFieldName(valueName);
        field.addAdditionalValue("", description.toString());
        
        return field;
    }
 
    /**
     * returns the campaign id for campaign enrollment
     * 
     * @return campaignId as String  Id of the searched campaign 
     */
    public String getCampaignId() {

        final String METHOD = "getCampaignId()";
        log.entering(METHOD);
        String campaignId = "";
        
        if (campaign != null && campaign.getSearchCriteria() != null) {
            campaignId = campaign.getSearchCriteria().getCampaignID();
        }
        
        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": campaignId=" + campaignId);
        }
        log.exiting();
        return campaignId;
    }
    
    /**
     * returns the id of the enrollee for campaign enrollment 
     * 
     * @return enrolleeId as String Id of the enrollee
     */
    public String getEnrolleeId() {

        final String METHOD = "getEnrolleeId()";
        log.entering(METHOD);
        String enrolleeId = "";
        
        if (campaign != null && campaign.getSearchCriteria() != null) {
            enrolleeId = campaign.getSearchCriteria().getEnrolleeId();
            if (enrolleeId == null) {
                enrolleeId = "";
            }
        }
        
        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": enrolleeId=" + enrolleeId);
        }
        log.exiting();
        return enrolleeId;
    }

    /**
     * Method returns the search flag for business partner hierarchy for campaign enrollment 
     * 
     * @return flag as boolean
     */
    public boolean isSearchForBpHier(String selForBpHier) {

        final String METHOD = "isSearchForBpHier()";
        log.entering(METHOD);
        boolean retval = false;
        
        if (campaign != null && campaign.getSearchCriteria() != null) {
            CampaignSearchCriteria searchCriteria = campaign.getSearchCriteria();
            boolean isSearchForBpHier = searchCriteria.getSearchForBpHier();
            String  enrolleeId = campaign.getSearchCriteria().getEnrolleeId();
            String  enrollerId = campaign.getSearchCriteria().getEnrollerId();
            
            if (log.isDebugEnabled()) {
                log.debug(METHOD + ": isSearchForBpHier=" + isSearchForBpHier +
                          " / selForBpHier=" + selForBpHier );
            }
            if (selForBpHier != null && enrolleeId != null) {
                if (selForBpHier.equals(CreateTransactionUI.SELBP_MY)) {
                    retval = !isSearchForBpHier && enrolleeId.equals(enrollerId);
                }
                else if (selForBpHier.equals(CreateTransactionUI.SELBP_ALL)) {
                    retval = isSearchForBpHier;
                }
                else if (selForBpHier.equals(CreateTransactionUI.SELBP_SEL)) {
                    retval = !isSearchForBpHier && !enrolleeId.equals(enrollerId);
                }
            }
            if (log.isDebugEnabled()) {
                String campaignId = campaign.getSearchCriteria().getCampaignID();
                log.debug(METHOD + ": campaignId=" + campaignId + " / isSearchForBpHier=" + isSearchForBpHier
                                 + " / enrolleeId=" + enrolleeId);
            }
        }
        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": retval=" + retval );
        }
        log.exiting();
        return retval;
    }
    
    /**
     * Returns the message list for campaign enrollment 
     * 
     * @return msgList of type MessageList
     */
    public MessageList getCampMessageList() {

        final String METHOD = "getCampMessageList()";
        log.entering(METHOD);
        MessageList msgList = null;
        
        if (campaign != null && campaign.hasMessages()) {
            msgList = campaign.getMessageList();
            if (log.isDebugEnabled()) {
                log.debug(METHOD + ": number of messages =" + msgList.size());
            }
        }
        else {
            if (log.isDebugEnabled()) {
                log.debug(METHOD + ": no messages found");
            }
        }
        log.exiting();
        return msgList;
    }


}
