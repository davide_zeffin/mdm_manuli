/*
 * Created on Dec 20, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.isacore.uiclass.b2b;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.ui.accessibility.AccessKeysForApplication;
import com.sap.isa.ui.uiclass.AccessKeyUI;

/**
 * @author i800855
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class AccessKeyB2BUI extends AccessKeyUI
{
	private static final IsaLocation log =
			IsaLocation.getInstance(AccessKeyB2BUI.class.getName());
	protected void initAccessKeys()
	{
		final String METHOD_NAME = "initAccessKeys()";
		log.entering(METHOD_NAME);
		AccessKeysForApplication app = addApplication("b2b.catalog");
		app.addAccessKey("b2b.cat.bigSearchHead.access","b2b.cat.header.access.inf");
		app.addAccessKey("b2b.cat.bigSearchBegin.access","b2b.cat.search.access.inf");
		app.addAccessKey("b2b.cat.categoriesBrowse.access","b2b.cat.category.access.inf");
		app.addAccessKey("b2b.cat.CompareItems.buttons.accesss","b2b.cat.buttons.access.inf");
		app.addAccessKey("b2b.cat.CompareItems.access","b2b.cat.compareItems.access.inf");
		app.addAccessKey("b2b.cat.proddetails.access","b2b.cat.itemdetails.access.inf");
		app.addAccessKey("b2b.cat.lead.jsp.main.access","b2b.cat.leadmain.access.inf");
		app.addAccessKey("b2b.cat.lead.jsp.btnarea.access","buttons Area:leads page");
		
		app = addApplication("b2b.core");
        app.addAccessKey("b2b.ord.firstpos.key","b2b.ord.key.firstpos.desc");

		app = addApplication("b2b.ipc");
		app.addAccessKey("ipc.key.crm.buttons", "ipc.area.buttons");
		app.addAccessKey("ipc.key.crm.cstics", "ipc.area.cstics");
		app.addAccessKey("ipc.key.crm.buttons", "ipc.area.groups");
		app.addAccessKey("ipc.key.crm.instances", "ipc.area.instances");
		app.addAccessKey("ipc.key.crm.grouplist", "ipc.area.grouplist");
		app.addAccessKey("ipc.key.crm.mfa", "ipc.area.mfa");
		app.addAccessKey("ipc.key.crm.search", "ipc.area.search");
		app.addAccessKey("ipc.key.crm.accept", "ipc.key.info.button.accept");
		app.addAccessKey("ipc.key.crm.cancel", "ipc.key.info.button.cancel");
		app.addAccessKey("ipc.key.crm.reset", "ipc.key.info.button.reset");
		app.addAccessKey("ipc.key.crm.lastfocused", "ipc.key.info.element.lastfocused");
		log.exiting();
	}
}
