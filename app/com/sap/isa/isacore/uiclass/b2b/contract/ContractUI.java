/*****************************************************************************
    Class:        ContractUI
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      08.09.2004
    Version:      1.0

    $Revision: #1$
    $Date: 2004/09/08 $ 
*****************************************************************************/
package com.sap.isa.isacore.uiclass.b2b.contract;

import javax.servlet.jsp.PageContext;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.contract.ContractAttributeList;
import com.sap.isa.businessobject.contract.ContractAttributeValueMap;
import com.sap.isa.businessobject.contract.ContractHeader;
import com.sap.isa.businessobject.contract.ContractItem;
import com.sap.isa.businessobject.contract.ContractItemList;
import com.sap.isa.businessobject.contract.ContractItemPrice;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.util.JspUtil;
import com.sap.isa.isacore.action.b2b.contract.ContractReadAction;
import com.sap.isa.isacore.uiclass.SalesDocumentDataUI;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.IPCItemReference;

/**
 * The class ContractUI . <br>
 *
 * @author  SAP AG
 * @version 1.0
 *
 */
public class ContractUI extends SalesDocumentDataUI {

	protected Shop shop;
	
	protected BusinessObjectManager bom;	
	
	/**
	 * Variable to store the css class of for the item table. <br>
	 */
	protected String itemCSSClass;
	
	/**
	 * String to differ between "even" and "odd" lines. <br>
	 */
	public String even_odd = "";
	
	/**
	 * Holds the attribute values of the items. <br>
	 */
	public ContractAttributeValueMap attributeValueMap = null;
	
	/**
	 * Holds the the item list<br>
	 */
	public ContractItemList contractItemList = null;
	
	/**
	 * Holds the header inforamtion. <br>
	 */
	public ContractHeader header = null;
	 
	
	/**
	 * Holds the  attriubute list of the contract. <br>
	 */	
	public ContractAttributeList attributeList = null;
	
	/**
	 * Holds the item currently used. The item must be set with the {@link #setItem}
	 * method. <br>
	 */	
	public ContractItem item = null;
	
	/**
	 * Current line number. <br>
	 */
	public int line = 0;	
	
	
	/**
	 * Flag, which determines, if the item is configurable
	 */
	public boolean configurable = false;
	
		
	/**
	 * Holds the Reference to the IPCItem of the contract item
	 */
	public IPCItemReference ipcItemReference = null;
	
	/**
	 * Alternator to to differ between even and odd lines. <br>
	 */
	public JspUtil.Alternator alternator;
	
	
		
	/**
	 * @param pageContext
	 */
	public ContractUI(PageContext pageContext) {
		
		super(pageContext);
		
		determineAttributeValueMap();
		
		determineContractItemList();
		
		header = contractItemList.getContractHeader();
		
		// super.initContext(pageContext);
		
		alternator = new JspUtil.Alternator(new String[] {"odd", "even" });		
		
	}	

	/**
	 * Determine the value map of the contract attributes from the page context.
	 * The map is taken from the request parameter 
	 * {@link com.sap.isa.isacore.action.b2b.contract.ContractReadAction.CONTRACT_ATTRIBUTE_VALUE_MAP}
	 */
	protected void determineAttributeValueMap() {    	
		this.attributeValueMap  =
		(ContractAttributeValueMap)request.getAttribute(
			ContractReadAction.CONTRACT_ATTRIBUTE_VALUE_MAP);;        
	}

	/**
	 * Determine the item list of the contract from the page context.
	 * The list is taken from the request parameter 
	 * {@link com.sap.isa.isacore.action.b2b.contract.ContractReadAction.CONTRACT_ITEM_LIST}
	 */
	protected void determineContractItemList() {   	
	    this.contractItemList =
			(ContractItemList)request.getAttribute(ContractReadAction.CONTRACT_ITEM_LIST);
	}

	/**
	 * Returns true, if the contract item is configurable
	 *  
	 * @return true, if the contract item is configurable
	 */
	protected void setConfigurable(ContractItem item) {
		 
		IPCItem ipcItem = null;
		ContractItemPrice contractItemPrice = null;
		boolean isConfigurable = false;
		
	/*	contractItemPrice = item.getItemPrice(); 
		if  (contractItemPrice != null) {                                  
			ipcItem = contractItemPrice.getConfigItemReference();
	    	if (ipcItem != null) { 
				ipcItemReference = ipcItem.getItemReference();
				if (ipcItemReference != null && 
			   	     (item.getConfigurationType().length() > 0 || item.isVariant())) { 			
		       		configurable =  true;
				}
	    	}
		}
		*/
		if (item.isIpcConfigurable() && item.getExternalItem() != null) {
			configurable = true;
		} else {
		    configurable = false;
		}
	}
	
	/**
	 * Returns the contract item price as String
	 *  
	 * @return String the contract item price
	 */	
	public String getItemPrice (ContractItem item) {
		String itemPrice = "";
 
		ContractItemPrice contractItemPrice = item.getItemPrice();
		if (item.getPrice() != null) { 
			itemPrice = itemPrice.concat(item.getPrice()+ ' ' + item.getCurrency() + ' ' + " / "  + ' ' + item.getPriceUnit() + ' ' + item.getPriceQuantUnit());		
		} else {
			if (contractItemPrice != null) {
				ContractItemPrice.PriceAsString priceAsString = contractItemPrice.getPrice();	
				itemPrice = priceAsString.getValue();
			}
		}	
		return itemPrice;	
	
	}
	
	/**
	 * Returns the contract item currency and unit as String
	 *  
	 * @return String the contract item currency and unit
	 */	
	protected String getItemCurrencyAndUnit(ContractItem item) {
		
		ContractItemPrice contractItemPrice = item.getItemPrice();
		ContractItemPrice.PriceAsString priceAsString = contractItemPrice.getPrice();	
		return priceAsString.getCurrencyAndUnit();	
	
	}
	
	/**
	 * Returns the IpcItemReference of the contract item 
	 *  
	 * @return IPCItemReference of the contract item
	 */	
	public IPCItemReference getIPCItemRef(ContractItem item) {
		IPCItemReference ipcItemRef = null;
		ContractItemPrice contractItemPrice = item.getItemPrice();
		if (contractItemPrice != null) {
			IPCItem ipcItem = contractItemPrice.getConfigItemReference();
			if (ipcItem != null) {
				ipcItemRef = ipcItem.getItemReference();
			}
		}
		return ipcItemRef;

	}	
	
	/**
	 * Returns the key of the contract item as string
	 *  
	 * @return String of the contract item
	 */	
	public String getItemKeyAsString (ContractItem contractItem) {
		String itemKey = "";
		if (contractItem != null) {
			itemKey = contractItem.getContractItemTechKey().getItemKey().getIdAsString();
		}
				return itemKey;
	}		
	
	/**
	 * Sets the item actually used.
	 * @param ContractItem item
	 */
	public void setItem(ContractItem item) {
	   
		this.item = item;
		line++;
		itemCSSClass = "poslist-" + even_odd;
		setConfigurable(item);
		if (item.isNewMainItem() ) {
			 even_odd = alternator.next(); // "even" or "odd" 
		}

	}
	
	/**
	 * Returns true if the detail button for an item must be shown
	 */
	public boolean showItemDetailButton() {
		return isToggleSupported(); 
	}
	
	/**
 	 * Only dummy
 	 * 
 	 * Set the property {@link #processTypeInfo}. 
 	 */
	protected void setProcessTypeInfo() {
	}
		
	/**
 	 * Only dummy
 	 * 
 	 * Return if the sold to should be displayed on the page. <br>
 	 * 
 	 * @return <code>true</code>, if the sold to should be displayed on the 
 	 *          JSP
 	 */
	public boolean isSoldToVisible() {
		return false;
	}

	/**
 	 * Only dummy
 	 * 
 	 * Return the description of the substitution reason for the given key. <br>
 	 * 
 	 * @param key key for the substitution reason
 	 * @return description of the substitution reason
 	 */
	protected  String getSubstitutionReasonDesc(String key) {
		return null;
	}

	/** 
 	 * Only dummy
 	 * 
 	 * Return the business partner object for the given sold to key. <br>
 	 * 
 	 * @param  soldToKey
 	 * @return business partner object, for the given business partner key 
 	 */
	protected BusinessPartner getSoldTo(TechKey soldToKey) {
		return null; 
	}

	/**
 	 * Only dummy because there is no salesDocument involved . <br>
 	 * 
 	 * @see com.sap.isa.isacore.uiclass.SalesDocumentBaseUI#determineSalesDocument()
 	 */
	protected void determineSalesDocument() {
	}
	
	/**
 	 * Only dummy 
	 * 
 	 * Determine the message list from the page context.
 	 * The message list is taken salesDocStatus which is provided by the DocumentHandler (getDocumentOnTop())
 	 * @see com.sap.isa.isacore.uiclass.SalesDocumentBaseUI#determineMessages()
 	 */
	protected void determineMessages() {
	}	
	
	/**
	 * Only dummy
	 * 
	 * The item list is taken from salesDocStatus which is provided by the DocumentHandler (getDocumentOnTop()) 
	 * @see com.sap.isa.isacore.uiclass.SalesDocumentBaseUI#determineItems()
	 */
	protected void determineItems() {
	}
		
	/**
	 *  
	 * Determine the contractHeader from the contractItemList 
	 * @see com.sap.isa.isacore.uiclass.SalesDocumentBaseUI#determineHeader()
	 */
	protected void determineHeader() {
		this.header = contractItemList.getContractHeader();		
	}

	/**
	 * Only dummy
	 * 
	 * Set the shop object in the salesDocConfiguration property. <br>
	 * @see com.sap.isa.isacore.uiclass.order.OrderDataUI#setSalesDocConfiguration()
	 */
	protected void setSalesDocConfiguration() {
    	
		// check for missing context
		if (userSessionData != null) {
			// get BOM
			bom = (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
	
			shop = bom.getShop();
			
			// now I can read the shop
			salesDocConfiguration = bom.getShop();
		}				
	}	
	
	/**
	 * Init Document <br>
	 * The contractItemList is set at least
	 */
	protected void initDocument() {
		this.contractItemList =
					(ContractItemList)request.getAttribute(ContractReadAction.CONTRACT_ITEM_LIST);		
	}	
	
}
