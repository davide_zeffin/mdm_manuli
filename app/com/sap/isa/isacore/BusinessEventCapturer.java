/*****************************************************************************
    Class         BusinessEventCapturer
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      May 2001
    Version:      1.0

    $Revision: #2 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.isacore;

import javax.servlet.http.HttpServletRequest;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.businessevent.AddToDocumentEvent;
import com.sap.isa.businessobject.businessevent.CreateDocumentEvent;
import com.sap.isa.businessobject.businessevent.DropDocumentEvent;
import com.sap.isa.businessobject.businessevent.DropFromDocumentEvent;
import com.sap.isa.businessobject.businessevent.LoginEvent;
import com.sap.isa.businessobject.businessevent.ModifyDocumentItemEvent;
import com.sap.isa.businessobject.businessevent.PlaceOrderEvent;
import com.sap.isa.businessobject.businessevent.RegisterEvent;
import com.sap.isa.businessobject.businessevent.SearchEvent;
import com.sap.isa.businessobject.businessevent.ViewCategoryEvent;
import com.sap.isa.businessobject.businessevent.ViewDocumentEvent;
import com.sap.isa.businessobject.businessevent.ViewProductEvent;
import com.sap.isa.businessobject.webcatalog.CatalogBusinessObjectManager;
import com.sap.isa.core.businessobject.event.BusinessEvent;
import com.sap.isa.core.businessobject.event.BusinessEventCapturerBase;


/**
 * Classes which would be listen to Business Events must implement this interface
 * and the register on <code>BusinessEventHandleImpl</code>
 *
 * @see BusinessEventHandler
 * @see BusinessEvent
 *
 */
public interface BusinessEventCapturer extends BusinessEventCapturerBase  {


    /**
     * Fired a general business event
     *
     */
    public void captureBusinessEvent(BusinessEvent event,
                                     BusinessObjectManager bom,
                                     HttpServletRequest request);


    /**
     * Fired a login event
     *
     */
    public void captureLoginEvent(LoginEvent event,
                                  BusinessObjectManager bom,
                                  HttpServletRequest request);


    /**
     * Fired a register event
     *
     */
    public void captureRegisterEvent(RegisterEvent event,
                                     BusinessObjectManager bom,
                                     HttpServletRequest request);

    /**
     * Capture a Search event
     *
     */
    public void captureSearchEvent(SearchEvent event,
                                   BusinessObjectManager bom,
                                   HttpServletRequest request);


    /**
     * Capture a CreateDocument event
     *
     */
    public void captureCreateDocumentEvent(CreateDocumentEvent event,
                                           BusinessObjectManager bom,
                                           HttpServletRequest request);


    /**
     * Capture a ViewDocument event
     *
     */
    public void captureViewDocumentEvent(ViewDocumentEvent event,
                                         BusinessObjectManager bom,
                                         HttpServletRequest request);


    /**
     * Fired an AddToDocument event
     *
     */
    public void captureAddToDocumentEvent(AddToDocumentEvent event,
                                          BusinessObjectManager bom,
                                          CatalogBusinessObjectManager cbom,
                                          HttpServletRequest request);
    /**
     * Fired an ModifyDocumentItem event
     *
     */
    public void captureModifyDocumentItemEvent(ModifyDocumentItemEvent event,
                                               BusinessObjectManager bom,
                                               CatalogBusinessObjectManager cbom,
                                               HttpServletRequest request);

    /**
     * Capture a DropFromDocument event
     *
     */
    public void captureDropFromDocumentEvent(DropFromDocumentEvent event,
                                    BusinessObjectManager bom,
                                    CatalogBusinessObjectManager cbom,
                                    HttpServletRequest request);

    /**
     * Capture a DropDocument event
     *
     */
    public void captureDropDocumentEvent(DropDocumentEvent event,
                                    BusinessObjectManager bom,
                                    HttpServletRequest request);

    /**
     * Capture a PlaceOrder event
     *
     */
    public void capturePlaceOrderEvent(PlaceOrderEvent event,
                                    BusinessObjectManager bom,
                                    HttpServletRequest request);

    /**
     * Capture a ViewCategory event
     *
     */
    public void captureViewCategoryEvent(ViewCategoryEvent event,
                                    BusinessObjectManager bom,
                                    HttpServletRequest request);


    /**
     * Capture a ViewProduct event
     *
     */
    public void captureViewProductEvent(ViewProductEvent event,
                                    BusinessObjectManager bom,
                                    CatalogBusinessObjectManager cbom,
                                    HttpServletRequest request);

}
