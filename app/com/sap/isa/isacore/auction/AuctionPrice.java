/*
 * Created on Jul 10, 2007
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.isacore.auction;


/**
 * @author d038645
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class AuctionPrice {
    /**
     * @param decimal
     * @param string
     */
    private String currency;
    private String price;
    
    public AuctionPrice(String price, String currency) {
        this.price = price;
        this.currency = currency;
    }
        
    public String getPrice() {
        return price;
    }
    
    public String getCurrency() {
        return currency;
    }
    

}
