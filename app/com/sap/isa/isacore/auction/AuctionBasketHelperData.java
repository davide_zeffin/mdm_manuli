/*
 * Created on 05.07.2007
 *
 * Interface, to access auction functionalities in SalesDocument\SalesDocumentBackends
 */
package com.sap.isa.isacore.auction;

import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;

/**
 * @author SAP AG
 *
 * Interface, to access auction functionalities in SalesDocument\SalesDocumentBackends
 */
public interface AuctionBasketHelperData {

	/**
	 * Method to retrieve pricing related auction data
	 * 
	 * @param auctionGuid Guid of the auction
	 */
	public AuctionPrice getAuctionPrice(TechKey auctionGuid, String soldToId);

	
	/**
	 * Method to check if the given product for the given auction is valid for checkout.
	 * That means the product is part of the auction and the auction is not yet checked out.
	 * 
	 * Either productGuid or productId, or both have to be provided.
	 * 
	 * @param soldToId Id of the soldTo
     * @param auctionGuid Guid of the auction
	 * @param productGuid Guid of the product
	 * @param productId Id of teh product
	 * @return boolean true if valid, false esle
	 */
	public boolean isAuctionItemValid(String soldToId, TechKey auctionGuid, TechKey productGuid, String productId);
    
    public String getTotalPriceCondition();
    
    public void setInvocationContext(UserSessionData userData);
    
    public void releaseInvocationContext();

    public String getRebatePriceCondition();
}
