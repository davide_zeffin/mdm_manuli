/**
 *  Class:        RecoverBasketCookieHandler
 *  Copyright (c) 2002, SAPMarkets Europe GmbH, Germany, All rights reserved.
 *  Author:       Michael Dietrich
 *  Created:      12.03.2002
 *  Version:      1.0
 *  $Revision: #1 $
 *  $Date: 2002/03/12 $
 */

package com.sap.isa.isacore;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sap.isa.businessobject.Shop;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;

/**
 *  Creates and maintains a cookie that stores information about the actually
 *
 *
 *@author     Michael Dietrich
 *@created    12.03.2002
 *
 *@version    1.0
 */

public class RecoverBasketCookieHandler {

    /**
     * Session context constant for the RecoverBasketCookieHandler
     */
    public static final String SC_RECO_BASKET_COOKIE_HDLR = "com.sap.isa.isacore.RecoverBasketCookieHandler";

    /**
     * Prefix for the name of the cookie which stores the leaflet data.
     * <p>
     * The complete name is built from the prefix and the techKey of the shop.
     * </p>
     */
    private static final String RECOVERBASKET_COOKIE_NAME_PREFIX = "recoverbasket";

    /**
     *  Maximal age of the cookie: <b>100 days</b>. After that time, the cookie is deleted.
     */
    private static final int COOKIE_MAX_AGE = 100 * 24 * 60 * 60;
    private static final int COOKIE_DELETE = 0;

    private IsaLocation log = IsaLocation.getInstance(this.getClass().getName());
    private boolean isDebugEnabled = log.isDebugEnabled();

    /**
     *  Name of the cookie
     */
    protected String cookiename  = "recoverbasket";;

    /**
     *  GUID of the Basket, stored in the cookie
     */
    protected TechKey basketGUID;

    /**
     *  Shop the basket was created in
     */
    protected Shop shop;

    /**
     *  request to search the cookie in
     */
    protected HttpServletRequest request;

    /**
     *  response to store the cookie in
     */
    protected HttpServletResponse response;

    /**
     * Constructor
     * @param  request  Request context.
     * @param  response  Response context.
     */
    public RecoverBasketCookieHandler(HttpServletRequest request, HttpServletResponse response) {

        if (request == null) {
            log.warn(LogUtil.APPS_USER_INTERFACE, "No request specified, unable to retrieve cookie");
            return;
            // without request, we cannot do anything
        }

        this.request = request;

         if (response == null) {
            log.warn(LogUtil.APPS_USER_INTERFACE, "No response specified, cookie can't be stored");
            return;
            // without request, we cannot do anything
        }

        this.response = response;
    }

    /**
     * tries to read the cookie for the given shop from the given request
     *
     * @param shop     Reference to the shop business object
     * @return         <code>true</code>, if the cookie could be loaded,<br>
     *                <code>false</code>, else.
     */
    public boolean read(Shop shop) {

        if (!setShop(shop)) {
            log.warn(LogUtil.APPS_USER_INTERFACE, "read(): Error setting Shop specified, unable to retrieve cookie value");
            return false;
            // without a shop, we can do anything
        }

        if (isDebugEnabled) {
            log.debug("read: shopid = " + shop.getTechKey().getIdAsString());
        }

        Cookie[] cookies = request.getCookies();

        if (cookies == null) {
            if (isDebugEnabled) {
                log.debug("read(): request conatins no cookies!");
            }
            return false;
        }

        if (isDebugEnabled) {
            log.debug("read(): name of the cookie: " + cookiename);
        }

        // Search through all the cookies, if there is one with the desired name.
        for (int i = 0; i < cookies.length; i++) {
            if (cookiename.equals(cookies[i].getName())) {
                if (!fill(cookies[i])) {
                    // no valid value found
                    return false;
                }
                break;
            }
        }

        if (isDebugEnabled) {
            log.debug("read(): cookie found, with value:" + basketGUID.getIdAsString());
        }
        return true;
    }


    /**
     * Saves the cookie in the response. If the cookie already
     * exists, it is updated with the current value.
     *
     * @param shop       Reference to the shop business object
     * @param basketGUID guid of the basket
     * @return         <code>true</code>, if the cookie could be loaded,<br>
     *                 <code>false</code>, else.
     */
    public boolean save(Shop shop, TechKey basketGUID) {

        Cookie cookie;

        if (!setShop(shop)) {
            log.warn(LogUtil.APPS_USER_INTERFACE,  "save(): Error setting shop, unable to save cookie");
            return false;
            // without a shop, we can do anything
        }

        if (basketGUID == null) {
            log.warn(LogUtil.APPS_USER_INTERFACE,  "save(): No basketGUID specified, unable to save cookie value");
            return false;
            // without a shop, we can do anything
        }

        this.basketGUID = basketGUID;

        // Create the cookie with the name
        try {
            cookie = new Cookie(cookiename, "");
            cookie.setPath("/");
        }
        catch (java.lang.IllegalArgumentException e) {
            log.warn(LogUtil.APPS_USER_INTERFACE, "save(): error creating cookie" + e.toString());
            if (isDebugEnabled) {
                log.debug("save(): name of the cookie: " + cookiename);
            }
            return false;
        }

        // The content of a cookie is the basketGUID as string.
        // set the attributes of the cookie and put it the response context
        cookie.setValue(basketGUID.toString());
        cookie.setMaxAge(COOKIE_MAX_AGE);

        response.addCookie(cookie);

        if (isDebugEnabled) {
            log.debug("save(): Saved the cookie with" + basketGUID.toString() );
        }
        return true;
    }

    /**
     * Deletes the cookie
     *
     * *@param shop     Reference to the shop business object
     */
    public boolean delete(Shop shop) {

        if (!setShop(shop)) {
            log.warn(LogUtil.APPS_USER_INTERFACE, "delete(): Error setting Shop, unable to retrieve cookie value");
            return false;
            // without a shop, we can do anything
        }

        Cookie cookie = new Cookie(cookiename, "");

        cookie.setMaxAge(COOKIE_DELETE);
        cookie.setPath("/");

        response.addCookie(cookie);

        return true;
    }

    /**
     * sets the shop information
     *
     * @param shop     Reference to the shop business object
     * @return         <code>true</code>, if the shop could be set <br>
     *                 <code>false</code>, else.
     */
    public boolean setShop(Shop shop) {

        if (shop == null) {
            log.warn(LogUtil.APPS_USER_INTERFACE, "read(): No Shop specified, unable to retrieve cookie value");
            return false;
            // without a shop, we can do anything
        }

        this.shop = shop;
        cookiename = buildCookieName(shop);

        return true;
    }

    /**
     * gets the shop information
     *
     * @return Shop     Reference to the shop business object
     */
    public Shop getShop() {

        return shop;
    }

    /**
     * gets the basketGUID information
     *
     * @return TechKey   GUID of the basket, stored in the cookie
     */
    public TechKey getBasketGUID() {

        return basketGUID;
    }

    /**
     * Builds the name of the cookie which stores the basketGUID. The name
     * is a concatenation of a constant prefix and the technical key of the
     * shop.
     */
    public String buildCookieName(Shop shop) {
        String name = null;

        if (shop != null && shop.getTechKey() != null) {
            name = RECOVERBASKET_COOKIE_NAME_PREFIX + shop.getTechKey().getIdAsString();
        }

        return name;
    }

    /**
     * Tries to extract the basketGUID from the Cookie.
     *
     * @param shop     Reference to the shop business object
     * @return         <code>true</code>, if a valid value was found <br>
     *                 <code>false</code>, else.
     */
    public boolean fill(Cookie cookie) {

        String value;

        value = cookie.getValue();

        // is there a valid value
        if ((value == null)) {
            if (isDebugEnabled) {
                log.debug("fill(): no value in cookie: " + cookiename);
            }
            return false;
        }

        basketGUID = new TechKey(value);

        return true;
    }
    
    public void setResponse(HttpServletResponse response) {
        this.response = response;
    }

} // end of class
