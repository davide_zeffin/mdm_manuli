package com.sap.isa.isacore;


import java.util.HashMap;

/**
 * The MessageDocument represents a message that is displayed in the
 * form_input frame. The message stay on top until the user reacts, 
 * e.g. presses cancel.
 */
public class MessageDocument {

	/**
 	 * Generic forward to the refresher jsp, 
 	 * that contains the jsp with the actual message.
	 */
	String forward = "generic_message";

	/**
 	 * Name of the jsp with the message.
 	 */
	String messageJSP;


	/**
	 * Hashtable to store name value pairs, that are needed on the jsp
	 */
	HashMap map;


	public void setForward(String forward) {
		this.forward = forward;		
	}

	public String getForward() {
		return forward;
	}
	
	public void setMessageJSP(String messageJSP) {
		this.messageJSP = messageJSP;
	}
	
	public String getMessageJSP() {
		return messageJSP;
	}		
	
	/**
	 * Stores a Name/Value pair in a hashmap, that can be used e.g. on a 
	 * jsp later on
	 */	
	public void setValue(String name, String value) {
		
		if (map == null) {
			map = new HashMap();
		}
		
		map.put(name, value);
	
	}
	
	/**
	 * Returns the value for the specified name
	 */	
	public String getValue(String name) {
		
		if (map == null) {
			map = new HashMap();
		}

		return (String) map.get(name);
	
	}
	
	
	
	
	/**
	 * Overwrite this method to set other data in the request context.
	 */
    public void preDisplayHook() {
     
    }


}
