
package com.sap.isa.isacore;

import java.util.Enumeration;
import java.util.Vector;

import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;

import com.sap.isa.core.FrameworkConfigManager;
import com.sap.isa.core.util.MiscUtil;
import com.sap.isa.core.xcm.config.ComponentConfig;
import com.sap.isa.core.xcm.config.ComponentConfigContainer;

/**
 * Wrapper arounf FilterConfig to encapsulate servlet context parameters
 * in Filter init params.
 * This is implemented to specify the tealeaf configuration as a servlet
 * context parameter so that the configuration file can be placed outside
 * web application deployed foder (to ease the upgrades)
 */
public class TealeafFilterConfig implements FilterConfig {

	private FilterConfig filterConfig;
	private final String TEALEAF_CONFIGFILE_CONTEXT_PARAM ="configFile";
	private final String TEALEAF_CONFIGFILE_SAP_CONTEXT_PARAM ="configfile.tealeaf";
	private Vector initParams ;
	
	/**
	 * Add configFile to init parameter list
	 * @param config
	 */
	public TealeafFilterConfig(FilterConfig config){
		filterConfig = config;
		initParams = new Vector();
		Enumeration enum = filterConfig.getInitParameterNames();
		while(enum.hasMoreElements()){
			 initParams.add(enum.nextElement());
		}
		initParams.add(TEALEAF_CONFIGFILE_CONTEXT_PARAM); //add tealeaf config file context param
	}
	
	/* (non-Javadoc)
	 * @see javax.servlet.FilterConfig#getFilterName()
	 */
	public String getFilterName() {
		return filterConfig.getFilterName();
	}

	/* (non-Javadoc)
	 * @see javax.servlet.FilterConfig#getServletContext()
	 */
	public ServletContext getServletContext() {
		return filterConfig.getServletContext();
	}

	/* 
	 * Return the value of the init-param passed in the filter configuration
	 * of web.xml. If the param is not the list of init-params of the 
	 * Filter configuration, SetvletContext params will be searched.
	 * If there is a parameter in the servlet context then retun the param value
	 * corresponding to the context parameter.
	 * @see javax.servlet.FilterConfig#getInitParameter(java.lang.String)
	 */
	public String getInitParameter(String paramName) {
		String paramValue = null;
		paramValue = filterConfig.getInitParameter(paramName);
		if(paramValue == null){//if tealeaf filter queries filter config with "configFile"
							//search servletcontext with "configfile.tealeaf.isa.sap.com"
			if(TEALEAF_CONFIGFILE_CONTEXT_PARAM.equalsIgnoreCase(paramName)){
				//paramValue =  filterConfig.getServletContext().getInitParameter(TEALEAF_CONFIGFILE_SAP_CONTEXT_PARAM);
				//commented as part of XCM migration of web.xml params
				ComponentConfigContainer ccc = FrameworkConfigManager.XCM.getApplicationScopeConfig();
				ComponentConfig compContainer = ccc.getComponentConfig("wec", "wecconfig");
				paramValue = compContainer.getParamConfig().getProperty(TEALEAF_CONFIGFILE_SAP_CONTEXT_PARAM);
				paramValue = MiscUtil.replaceSystemProperty(paramValue);
			}
			else {
				paramValue =  filterConfig.getServletContext().getInitParameter(paramName);
			}
		}
		return paramValue;
	}

	/* 
	 * Return init params passed in the filter configuration mentioned in web.xml
	 * <b> NOTE :</b> This method do not return the context params 
	 * @see javax.servlet.FilterConfig#getInitParameterNames()
	 */
	public Enumeration getInitParameterNames() {
		return initParams.elements();
	}
}
