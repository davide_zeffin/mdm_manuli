/*****************************************************************************
    Class:        AddressFormularBase
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Created:      August 2001
    Version:      1.0

    $Revision: #2 $
    $Date: 2002/10/29 $
*****************************************************************************/

package com.sap.isa.isacore;

import javax.servlet.http.HttpServletRequest;

import com.sap.isa.backend.boi.isacore.AddressConfiguration;
import com.sap.isa.businessobject.Address;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ResultData;


/**
 * This class helps you to handle an address object on a jsp.
 *
 * @author SAP
 * @version 1.0
 */
public class AddressFormularBase {

    /**
     * use this for logging
     */
    static protected IsaLocation log = IsaLocation.
                                       getInstance(AddressFormularBase.class.getName());

    private boolean isEditable = true;

    private boolean isPerson = false;
    private boolean isRegionRequired = false;

    protected Address address = null;
    protected int addressFormatId = 0;

	protected ResultData countryList;
	protected ResultData regionList;
	protected ResultData taxCodeList;


    /**
     * Easy constructor.
     */
    public AddressFormularBase(){
    }

    /**
     * Constructor.
     *
     * @param addressFormatId
     */
    public AddressFormularBase(int addressFormatId) {
        this.addressFormatId = addressFormatId;
    }

    /**
     * Create the object parsing the request.
     *
     * @param requestParser
     */
    public AddressFormularBase(RequestParser requestParser) {
        address = new Address();
        parseRequest(requestParser);
    }

    /**
     * Create the object parsing the request.
     * @param requestParser
     * @param address
     */
    public AddressFormularBase(RequestParser requestParser, Address address) {
        this.address = address;
        parseRequest(requestParser);
    }

    /**
     * Constructor.
     * @param address
     * @param addressFormatId
     */
    public AddressFormularBase(Address address, int addressFormatId) {
        this.address = address;
        this.addressFormatId = addressFormatId;
    }

    /**
     * Constructor.
     * @param address
     * @param addressFormatId
     * @param isEditable
     */
    public AddressFormularBase(Address address, int addressFormatId, boolean isEditable) {
        this.address = address;
        this.addressFormatId = addressFormatId;
        this.isEditable = isEditable;
    }


    /**
     * Set all relvant object in the request context.
     *
     * @param request
     * @param configuration the configuration object would be used to get the country and region list
     *
     */
    public void addToRequest(HttpServletRequest request,
                             AddressConfiguration configuration)
            throws CommunicationException {

        request.setAttribute(AddressConstants.RC_ADDRESS_FORMULAR,this);

        if(address == null) {
            address = new Address();
        }

        request.setAttribute(AddressConstants.RC_ADDRESS,address);

        countryList = configuration.getCountryList();
        regionList = null;

        if(address.getCountry().length() > 0) {
        	
// the following is not valid because AddressConfiguration miss the methods 
// readCountryDescription and readRegionDescription         	

//            // Put the country of the address to the country list
//            // if it does not exists in the list.
//            // To avoid JSP corrections this is done here
//            if (configuration.getCountryDescription(address.getCountry()) == null) {
//
//         		  String countryDescription = shop.readCountryDescription(address.getCountry());
//			      if ((countryDescription != null) && (countryDescription.length() > 0)) {
//
//                    // the country list is a ResultData which is only readable
//                    // so a new ResultData object must be created
//                    // (for a better performance the access to a table object
//                    //  for the country list in the configuration will be better!!!)
//                    Table countryTable = new Table("Countries");
//
//                    countryTable.addColumn(Table.TYPE_STRING,AddressConfiguration.ID);
//                    countryTable.addColumn(Table.TYPE_STRING,AddressConfiguration.DESCRIPTION);
//                    countryTable.addColumn(Table.TYPE_BOOLEAN,AddressConfiguration.REGION_FLAG);
//
//                    int numEntries = countryList.getNumRows();
//                    countryList.first();
//                    TableRow row = null;
//
//                    for(int i = 1; i <= numEntries; i++) {
//
//                        row = countryTable.insertRow();
//
//                        row.setRowKey(new TechKey(countryList.getString(AddressConfiguration.ID)));
//                        row.getField(AddressConfiguration.ID).setValue(countryList.getString(AddressConfiguration.ID));
//                        row.getField(AddressConfiguration.DESCRIPTION).setValue(countryList.getString(AddressConfiguration.DESCRIPTION));
//                        row.getField(AddressConfiguration.REGION_FLAG).setValue(countryList.getBoolean(AddressConfiguration.REGION_FLAG));
//                        countryList.next();
//                    }
//
//                    row = countryTable.insertRow();
//                    row.setRowKey(new TechKey(address.getCountry()));
//                    row.getField(AddressConfiguration.ID).setValue(address.getCountry());
//                    row.getField(AddressConfiguration.DESCRIPTION).setValue(configuration.readCountryDescription(address.getCountry()));
//                    //if there is a region in the address set the region flag
//                    //and add the region into the region list later on
//                    if(address.getRegion() != null && address.getRegion() != "") {
//                        row.getField(AddressConfiguration.REGION_FLAG).setValue(true);
//                    }
//                    else {
//                        row.getField(AddressConfiguration.REGION_FLAG).setValue(false);
//                    }
//
//                    countryList = new ResultData(countryTable);
//
//                    if(address.getRegion().length() > 0) {
//                        // if the country is not in the country list there is
//                        // no region flag set and so there is no region list
//                        // prepare a region list if a region exists in the address
//                        Table regionTable = new Table("Regions");
//
//                        regionTable.addColumn(Table.TYPE_STRING,AddressConfiguration.ID);
//                        regionTable.addColumn(Table.TYPE_STRING,AddressConfiguration.DESCRIPTION);
//
//                        numEntries = 1;
//
//                        for(int i = 0; i < numEntries; i++) {
//
//                            row = regionTable.insertRow();
//
//                            row.setRowKey(new TechKey(address.getRegion()));
//                            row.getField(AddressConfiguration.ID).setValue(address.getRegion());
//                            row.getField(AddressConfiguration.DESCRIPTION).setValue(configuration.readRegionDescription(address.getRegion(), address.getCountry()));
//                        }
//
//                        regionList = new ResultData(regionTable);
//                    }
//                }
//            }
//            else {
                regionList = configuration.getRegionList(address.getCountry());
//           }
        }

        else {
            countryList.first();
            regionList = configuration.getRegionList(countryList.getString(AddressConfiguration.ID));
        }

        countryList.beforeFirst();
        request.setAttribute(AddressConstants.RC_COUNTRY_LIST,countryList);

        log.debug(countryList.toString());

        regionList.beforeFirst();
        request.setAttribute(AddressConstants.RC_REGION_LIST,regionList);

        if (address.getCountry().length() > 0 && address.getRegion().length() > 0
                && address.getPostlCod1().length() > 0 && address.getCity().length() > 0) {
            taxCodeList = configuration.getTaxCodeList(address.getCountry(),
                                                       address.getRegion(),
                                                       address.getPostlCod1(),
                                                       address.getCity());
            if(taxCodeList != null && taxCodeList.getNumRows() > 0) {
                request.setAttribute(AddressConstants.RC_TAXCODE_LIST, taxCodeList);
            }
        }

        request.setAttribute(AddressConstants.RC_TITLE_LIST,
                             configuration.getTitleList());

        log.debug("addToRequest: " + configuration.getTitleList().getNumRows() +
                  " entries found in title list");

    }

    /**
     * Method setAddress.
     * @param address
     */
    public void setAddress(Address address) {
        this.address = address;
    }

    /**
     * Method getAddress.
     * @return Address
     */
    public Address getAddress() {
        return address;
    }

    /**
     * Method setEditable.
     * @param editable
     */
    public void setEditable(boolean editable) {
        this.isEditable = editable;
    }

    /**
     * Method isEditable.
     * @return boolean
     */
    public boolean isEditable() {
        return isEditable;
    }

    /**
     * Method setPerson.
     * @param isPerson
     */
    public void setPerson(boolean isPerson) {
        this.isPerson = isPerson;
    }

    /**
     * Method isPerson.
     * @return boolean
     */
    public boolean isPerson() {
        return isPerson;
    }

    /**
     * Method setRegionRequired.
     * @param isRegionRequired
     */
    public void setRegionRequired(boolean isRegionRequired) {
        this.isRegionRequired = isRegionRequired;
    }

    /**
     * Method isRegionRequired.
     * @return boolean
     */
    public boolean isRegionRequired() {
        return isRegionRequired;
    }

    /**
     * Method setAddressFormatId.
     * @param addressFormatId
     */
    public void setAddressFormatId(int addressFormatId) {
        this.addressFormatId = addressFormatId;
    }

    /**
     * Method getAddressFormatId.
     * @return int
     */
    public int getAddressFormatId() {
        return addressFormatId;
    }


    /**
     * Set the property countryList
     *
     * @param countryList
     *
     */
    public void setCountryList(ResultData countryList) {
        this.countryList = countryList;
    }


    /**
     * Returns the property countryList
     *
     * @return countryList
     *
     */
    public ResultData getCountryList() {
        return this.countryList;
    }


    /**
     * Set the property regionList
     *
     * @param regionList
     *
     */
    public void setRegionList(ResultData regionList) {
        this.regionList = regionList;
    }


    /**
     * Returns the property regionList
     *
     * @return regionList
     *
     */
    public ResultData getRegionList() {
        return this.regionList;
    }


    /**
     * Set the property taxCodeList
     *
     * @param taxCodeList
     *
     */
    public void setTaxCodeList(ResultData taxCodeList) {
        this.taxCodeList = taxCodeList;
    }


    /**
     * Returns the property taxCodeList
     *
     * @return taxCodeList
     *
     */
    public ResultData getTaxCodeList() {
        return this.taxCodeList;
    }


    // helper methods

    private void parseRequest(RequestParser requestParser) {
        String techKeyString = (String)requestParser.getParameter("addrtechkey").getValue().getString();
        address.setTechKey(new TechKey(techKeyString));
        address.setTitleKey(requestParser.getParameter("titleKey").getValue().getString());
        address.setFirstName(requestParser.getParameter("firstName").getValue().getString());
        address.setLastName(requestParser.getParameter("lastName").getValue().getString());
        address.setName1(requestParser.getParameter("name1").getValue().getString());
        address.setName2(requestParser.getParameter("name2").getValue().getString());
        address.setStreet(requestParser.getParameter("street").getValue().getString());
        address.setHouseNo(requestParser.getParameter("houseNumber").getValue().getString());
        address.setPostlCod1(requestParser.getParameter("postalCode").getValue().getString());
        address.setCity(requestParser.getParameter("city").getValue().getString());
        address.setCountry(requestParser.getParameter("country").getValue().getString());
        address.setTaxJurCode(requestParser.getParameter("taxJurisdictionCode").getValue().getString());
        address.setRegion(requestParser.getParameter("region").getValue().getString());
        address.setAddressPartner(requestParser.getParameter("addresspartner").getValue().getString());
        address.setEMail(requestParser.getParameter("email").getValue().getString());
        address.setTel1Numbr(requestParser.getParameter("telephoneNumber").getValue().getString());
        address.setFaxNumber(requestParser.getParameter("faxNumber").getValue().getString());

        // additional possible fields
        address.setTitle(requestParser.getParameter("title").getValue().getString());
        address.setTitleAca1Key(requestParser.getParameter("titleAca1Key").getValue().getString());
        address.setTitleAca1(requestParser.getParameter("titleAca1").getValue().getString());
        address.setBirthName(requestParser.getParameter("birthName").getValue().getString());
        address.setSecondName(requestParser.getParameter("secondName").getValue().getString());
        address.setMiddleName(requestParser.getParameter("middleName").getValue().getString());
        address.setNickName(requestParser.getParameter("nickName").getValue().getString());
        address.setInitials(requestParser.getParameter("initials").getValue().getString());
        address.setName3(requestParser.getParameter("name3").getValue().getString());
        address.setName4(requestParser.getParameter("name4").getValue().getString());
        address.setCoName(requestParser.getParameter("coName").getValue().getString());
        //In the moment the district is not transfered from the JSP because the HTML county select box
        //only sends the tax jurisdiction code as a request parameter for the selected entry.
        //But the district could be filled from the backend. Since it is not changed a mismatch of
        //tax jurisdiction code related data can exist which leads to problems.
        //Therefore the statement is removed here.
        //address.setDistrict(requestParser.getParameter("district").getValue().getString());
        address.setPostlCod2(requestParser.getParameter("postlCod2").getValue().getString());
        address.setPostlCod3(requestParser.getParameter("postlCod3").getValue().getString());
        address.setPcode1Ext(requestParser.getParameter("pcode1Ext").getValue().getString());
        address.setPcode2Ext(requestParser.getParameter("pcode2Ext").getValue().getString());
        address.setPcode3Ext(requestParser.getParameter("pcode3Ext").getValue().getString());
        address.setPoBox(requestParser.getParameter("poBox").getValue().getString());
        address.setPoWoNo(requestParser.getParameter("poWoNo").getValue().getString());
        address.setPoBoxCit(requestParser.getParameter("poBoxCit").getValue().getString());
        address.setPoBoxReg(requestParser.getParameter("poBoxReg").getValue().getString());
        address.setPoBoxCtry(requestParser.getParameter("poBoxCtry").getValue().getString());
        address.setPoCtryISO(requestParser.getParameter("poCtryISO").getValue().getString());
        address.setStrSuppl1(requestParser.getParameter("strSuppl1").getValue().getString());
        address.setStrSuppl2(requestParser.getParameter("strSuppl2").getValue().getString());
        address.setStrSuppl3(requestParser.getParameter("strSuppl3").getValue().getString());
        address.setLocation(requestParser.getParameter("Location").getValue().getString());
        address.setHouseNo2(requestParser.getParameter("houseNo2").getValue().getString());
        address.setHouseNo3(requestParser.getParameter("houseNo3").getValue().getString());
        address.setBuilding(requestParser.getParameter("buidling").getValue().getString());
        address.setFloor(requestParser.getParameter("floor").getValue().getString());
        address.setRoomNo(requestParser.getParameter("roomNo").getValue().getString());
        address.setCountryISO(requestParser.getParameter("countryISO").getValue().getString());
        address.setRegion(requestParser.getParameter("region").getValue().getString());
        address.setHomeCity(requestParser.getParameter("homeCity").getValue().getString());
        address.setTel1Ext(requestParser.getParameter("tels1Ext").getValue().getString());
        address.setFaxExtens(requestParser.getParameter("faxExtens").getValue().getString());


        isPerson = requestParser.getParameter("person").getValue().getBoolean();
        isRegionRequired = requestParser.getParameter("regionRequired").getValue().getBoolean();
    }
}
