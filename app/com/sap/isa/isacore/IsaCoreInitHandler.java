/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Creator:      Alexander Staff, stolen from Roland Huecking's CatalogCacheInitHandler
  Created:      June, 07 2001

  $Id:
  $Revision:
  $Change:
  $DateTime:
  $Author:
*****************************************************************************/

package com.sap.isa.isacore;

// core imports
import java.util.Properties;

import com.sap.isa.core.init.Initializable;
import com.sap.isa.core.init.InitializationEnvironment;
import com.sap.isa.core.init.InitializeException;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;

/**
 * This class is called from the initialization environment to setup the
 * isacore webapp.
 * Currently we only create a list of searchable attributes for the quicksearch-functionality here.
 * The attributes also contain if they are searchable or not.
 * We only create a "do not search this" list, the default is that all attributes are searchable.
 * @author      Alexander Staff
 * @version     1.0
 */
public class IsaCoreInitHandler implements Initializable
{

    // the logging instance
    private static IsaLocation log;

    // the instance
    private IsaCoreInit ici = null;

    /**
     * Default constructor.
     */
    public IsaCoreInitHandler()
    {
        // initialize the logging instance
        log = IsaLocation.getInstance(this.getClass().getName());
    }

    /**
     * Is called by our init-handler. Sets up the exclusionlist of attributes for
     * the quicksearch.
     *
     * @param env The initialization environment
     * @param props The properties set in the init-config-file
     */
    public void initialize(InitializationEnvironment env, Properties props)
            throws InitializeException
    {
        // initialization of the IsaCoreInit-thing, write a warning to the log if it
        // was already done
        if( !IsaCoreInit.initialize(env, props) ) {
            log.warn(LogUtil.APPS_COMMON_CONFIGURATION, "some error");
        }

        // Get a reference to the IsaCoreInit-thing so that the garbage collector does not
        // remove the instance
        ici = IsaCoreInit.getInstance();
        if( ici == null) {
            throw new InitializeException("some error");
        }
    }

    /**
     * Now the application is terminated so the garbage collector is allowed
     * to remove the instance
     *
     */
    public void terminate()
    {
        // Now the application is terminated so the garbage collector is allowed
        // to remove the instance
        ici = null;
    }

}