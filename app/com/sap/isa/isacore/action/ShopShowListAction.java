/*****************************************************************************
    Class         ShopShowListAction
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Action to display an shop list
    Author:       Wolfgang Sattler
    Created:      March 2001
    Version:      0.1

    $Revision: #3 $
    $Date: 2001/07/04 $
*****************************************************************************/

package com.sap.isa.isacore.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.SearchResult;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.interaction.InteractionConfig;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.isacore.action.b2c.B2cConstants;
/**
 * Display the list of shop's
 *
 * <br> Note that all Action classes have to provide a non argument constructor
 * because they were instantiated automatically by the action servlet.
 * <p>
 * This action retrieves a list of the available shops from the system and
 * puts the data in the request context to allow a JSP the rendering of the
 * data. If only one shop is found or auto entry is requested (use first shop of shoplist),
 *  the control is directly forwarded to the action reading the shop data.
 * </p>
 * <p>
 * <em>Input:</em>
 * <ul>
 *   <li><b><code>none</code></b>
 * </ul>
 * <br>
 * <em>Output:</em>
 * <ul>
 *   <li><b><code>Request-Attribute->RK_SHOP_LIST</code></b> - List of the
 *       shops found as a <code>ResultData</code> object.
 *   <li><b><code>Request-Attribute->PARAM_SHOPID</code></b> - Id of the shop
 *       if only one shop was found. In this case the control is directly
 *       forwarded to the action, which reads the shop.
 * </ul>
 *
 * @author Wolfgang Sattler
 * @version 1.0
 *
 */
public class ShopShowListAction extends ShopReadListBaseAction {

    private static final String FORWARD_READSHOP = "readshop";
    private static final String FORWARD_SHOPLIST = "shoplist";
	private static final String FORWARD_AUTOENTRYERROR = "autoentryerror";
	


	/**
	 * Initialize the action.
	 * In B2C this action could be used by anonymous user.
	 * 
	 * @see com.sap.isa.isacore.action.IsaCoreBaseAction#initialize()
	 */
	public void initialize() {
		if (isB2C()) {
			this.checkUserIsLoggedIn = false;
		}	
	}

	
    /**
     * Overriden <em>isaPerform</em> method of <em>IsaCoreBaseAction</em>.
     */
    public ActionForward isaPerform(ActionMapping mapping,
                ActionForm form,
                HttpServletRequest request,
                HttpServletResponse response,
                UserSessionData userSessionData,
                RequestParser requestParser,
                BusinessObjectManager bom,
                IsaLocation log)
                        throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        // Page that should be displayed next.
        String forwardTo = null;

        // Logging
        boolean isDebugEnabled = log.isDebugEnabled();
		String logId = getLoggingPrefix(request);
        // First check, if a shop already exists
        Shop shop = bom.getShop();

        if (shop != null) {
            request.setAttribute(PARAM_SHOPID,shop.getId());
            return mapping.findForward(FORWARD_READSHOP);
        }

        // read user for the language
        User user = bom.getUser();

        // check if a shop is given as an startupParameter

        IsaCoreInitAction.StartupParameter startupParameter =
                (IsaCoreInitAction.StartupParameter)userSessionData.getAttribute(IsaCoreInitAction.SC_STARTUP_PARAMETER);

        String shopId = null;
		SearchResult result = null;
        if (startupParameter != null) {

            shopId = startupParameter.getShop();
            if (shopId.length() > 0) {

                if (isDebugEnabled) {
                    log.debug(logId + "shop provided as startup parameter: " + shopId);
                }

                // Save shopId in request
                request.setAttribute(PARAM_SHOPID, shopId);
                return mapping.findForward(FORWARD_READSHOP);
            }
        }

		// get the scenario form the XCM.
		InteractionConfig interactionConfig = getInteractionConfig(request).getConfig("shop");
		String scenario = interactionConfig.getValue("shopscenario");
		if (isDebugEnabled) {
			log.debug("using scenario: " + scenario);
		}

		// try to read shop from XCM
		shopId = interactionConfig.getValue("defaultShopId");

        if (shopId != null && shopId.length() > 0) {

            if (isDebugEnabled) {
                log.debug(logId + "shop provided from XCM: " + shopId);
            }

            // Save shopId in request
            request.setAttribute(PARAM_SHOPID, shopId);
            log.exiting();
            return mapping.findForward(FORWARD_READSHOP);
        }

		
		String sEnableWebCrawling = enableWebCrowlersAndOthers(
						request,
						userSessionData,
						log,
						scenario);
        // Use the bom to retrieve a reference to the search object
		// read the result set to diplay it on the jsp
		result = readShopList(
							userSessionData,
							bom,
							log,
							scenario);
		ResultData shopList = new ResultData(result.getTable());


        if (result.getNumberOfHits() <= 0) {

            if (isDebugEnabled) {
                log.debug(logId + "no shop found, bailing out!");
            }

            // found no shop, this can't happen - panic!
            log.exiting();
            throw new PanicException("panic.shoplist.zeroelements");
        }
        else if (result.getNumberOfHits() == 1) {

            // move to first row
            shopList.absolute(1);

            // Save shopId in request
            request.setAttribute(PARAM_SHOPID, shopList.getRowKey());

            if (isDebugEnabled) {
                log.debug(logId + "found exactly one shop, using it: " + shopList.getRowKey());
            }

            forwardTo = FORWARD_READSHOP;
        }
        else {
			if (startupParameter.getAutoEntry().equals("YES")) {
				// Found more than one shop but autoentry requested (currently used from
				// Portal iViews). Since no shoplist can be presented, this situation
				// will lead to errorpage
				request.setAttribute(RK_AUTOENTRYERROR, "shoperror");
				request.setAttribute(RK_AUTOENTRYADDINFO, "shop:".concat( (shop != null ? shop.getId() : "no shop id given" )));
				return mapping.findForward(FORWARD_AUTOENTRYERROR);
			}

            // Store list of shops in the request context
            request.setAttribute(RK_SHOP_LIST, shopList);
            boolean blnFlg = adjustNumofHits(
							request,
							userSessionData,
							log,
							result,
							logId);
			if(blnFlg){
				log.debug(logId + "trying to redirect to webshop");
				forwardTo = FORWARD_READSHOP;
			}
			else{
				log.debug(logId + "found more than one shop");
				userSessionData.setAttribute(B2cConstants.BASKET_LOADED_DIRECT,"true");
				forwardTo = FORWARD_SHOPLIST;
			}
            
        }
        log.exiting();
        return mapping.findForward(forwardTo);
    }
}
