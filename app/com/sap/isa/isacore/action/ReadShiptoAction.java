package com.sap.isa.isacore.action;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.Address;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.ShipTo;
import com.sap.isa.businessobject.ShiptoSearch;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.businessobject.order.Order;
import com.sap.isa.businessobject.ordertemplate.OrderTemplate;
import com.sap.isa.businessobject.quotation.Quotation;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;



public class ReadShiptoAction extends IsaCoreBaseAction {

    /**
     * Name of the request parameter directly setting the shipTo.
     */
    public static final String PARAM_SHIPTO = "BPARTNER";

    /**
     * Name of the request parameter directly setting the shipToTechKey.
     */
    public static final String PARAM_SHIPTO_TECHKEY = 		"BPARTNER_GUID";
	public static final String PARAM_SHIPTO_SHORT_ADDR = 	"ADDRESS_SHORT";
	public static final String PARAM_SHIPTO_NAME1 = 			"NAME1";
	public static final String PARAM_SHIPTO_NAME2 = 			"NAME2";
	public static final String PARAM_SHIPTO_FIRSTNAME = 		"FIRSTNAME";
	public static final String PARAM_SHIPTO_LASTNAME = 		"LASTNAME";
	public static final String PARAM_SHIPTO_ADDR_NR = 		"ADDR_NR";
	public static final String PARAM_SHIPTO_STREET = 		"STREET";
	public static final String PARAM_SHIPTO_POSTL_COD1 = 	"POSTL_COD1";
	public static final String PARAM_SHIPTO_COUNTRY = 		"COUNTRY";
	public static final String PARAM_SHIPTO_HOUSE_NO = 		"HOUSE_NO";
	public static final String PARAM_SHIPTO_CITY = 			"CITY";
	public static final String PARAM_SHIPTO_EMAIL = 			"E_MAIL";
	public static final String PARAM_SHIPTO_ADDR_TYPE = 		"ADDR_TYPE";
	public static final String PARAM_SHIPTO_ADDR_ORIGIN = 	"ADDR_ORIGIN";
	public static final String PARAM_SHIPTO_ADDR_NP = 		"ADDR_NP";

    public static final String FORWARD_CANCEL = "cancel";
    public static final String FORWARD_UPDATEDOCUMENTVIEW = "updatedocumentview";
    public static final String FORWARD_NOTECHKEY = "notechkey";

    /** creates the parameter list for the shipTo **/
    public static String createRequest(ResultData shipToList){

        return "?" + PARAM_SHIPTO_TECHKEY + "=" + shipToList.getString(PARAM_SHIPTO_TECHKEY) +
               	"&" + PARAM_SHIPTO + "=" + shipToList.getString(PARAM_SHIPTO) +
				"&" + PARAM_SHIPTO_NAME1 + "=" + shipToList.getString(PARAM_SHIPTO_NAME1) +
				"&" + PARAM_SHIPTO_NAME2 + "=" + shipToList.getString(PARAM_SHIPTO_NAME2) +
				"&" + PARAM_SHIPTO_FIRSTNAME + "=" + shipToList.getString(PARAM_SHIPTO_FIRSTNAME) +
				"&" + PARAM_SHIPTO_LASTNAME + "=" + shipToList.getString(PARAM_SHIPTO_LASTNAME) +
				"&" + PARAM_SHIPTO_STREET + "=" + shipToList.getString(PARAM_SHIPTO_STREET) +
				"&" + PARAM_SHIPTO_POSTL_COD1 + "=" + shipToList.getString(PARAM_SHIPTO_POSTL_COD1) +
				"&" + PARAM_SHIPTO_COUNTRY + "=" + shipToList.getString(PARAM_SHIPTO_COUNTRY) +
				"&" + PARAM_SHIPTO_HOUSE_NO + "=" + shipToList.getString(PARAM_SHIPTO_HOUSE_NO) +
				"&" + PARAM_SHIPTO_CITY + "=" + shipToList.getString(PARAM_SHIPTO_CITY) +
				"&" + PARAM_SHIPTO_EMAIL + "=" + shipToList.getString(PARAM_SHIPTO_EMAIL) +
				"&" + PARAM_SHIPTO_SHORT_ADDR + "=" + shipToList.getString(PARAM_SHIPTO_SHORT_ADDR) +
				"&" + PARAM_SHIPTO_ADDR_NR + "=" + shipToList.getString(PARAM_SHIPTO_ADDR_NR) +
				"&" + PARAM_SHIPTO_ADDR_TYPE + "=" + shipToList.getString(PARAM_SHIPTO_ADDR_TYPE) +
				"&" + PARAM_SHIPTO_ADDR_ORIGIN + "=" + shipToList.getString(PARAM_SHIPTO_ADDR_ORIGIN) +
				"&" + PARAM_SHIPTO_ADDR_NP + "=" + shipToList.getString(PARAM_SHIPTO_ADDR_NP);
				
    }

    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            BusinessObjectManager bom,
            IsaLocation log)
            throws CommunicationException {

        // Page that should be displayed next.
        String name = null;
        String forwardTo = null;
		BusinessPartnerManager bupama = null;
		bupama = bom.getBUPAManager();

        // get pre-order document on top from document handler
        DocumentHandler documentHandler = (DocumentHandler)
            userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
		String itemKey = (String)
			userSessionData.getAttribute("SEARCHSHIPTOITEMKEY");
            
        if (documentHandler == null) {
            throw new PanicException("No document handler found");
        }
        DocumentState targetDocument =
            documentHandler.getDocument(ManagedDocument.TARGET_DOCUMENT);

        // get the current pre order sales document - basket is assumed as
        // default
        SalesDocument preOrderSalesDocument = null;
        if (targetDocument instanceof Quotation) {
            preOrderSalesDocument = bom.getQuotation();
        }
        else if  (targetDocument instanceof Order) {
                    preOrderSalesDocument = bom.getOrder();
        }
        else if (targetDocument instanceof OrderTemplate) {
            preOrderSalesDocument = bom.getOrderTemplate();
        }
        else {
            preOrderSalesDocument = bom.getBasket();

        }
        if (preOrderSalesDocument == null) {
            throw new PanicException("No sales document returned from backend");
        }


        User user           = bom.getUser();
        Shop shop           = bom.getShop();

        TechKey preOrderSalesDocumentKey   = preOrderSalesDocument.getTechKey();
        TechKey soldtoKey   = bupama.getDefaultBusinessPartner(PartnerFunctionData.SOLDTO).getTechKey();
        TechKey shopKey     = shop.getTechKey();

        ShiptoSearch search = (ShiptoSearch) bom.getShiptoSearch();

       //which button was pressed?
       if (requestParser.getParameter(FORWARD_CANCEL).isSet()) {
           forwardTo = FORWARD_UPDATEDOCUMENTVIEW;
           return mapping.findForward(forwardTo);
       }
        if ( request.getParameter(PARAM_SHIPTO_TECHKEY) == null ||
             request.getParameter(PARAM_SHIPTO_TECHKEY).length() == 0) {

           return mapping.findForward(FORWARD_NOTECHKEY);
        }
        		
		ShipTo shipto = (ShipTo)preOrderSalesDocument.createShipTo();
		shipto.setTechKey(new TechKey(request.getParameter(PARAM_SHIPTO_TECHKEY)));
		shipto.setId(new String(request.getParameter(PARAM_SHIPTO)));
		shipto.setShortAddress(new String(request.getParameter(PARAM_SHIPTO_SHORT_ADDR)));		
		Address address = (Address)shipto.createAddress();

		address.setName1(new String(request.getParameter(PARAM_SHIPTO_NAME1)));
		address.setName2(new String(request.getParameter(PARAM_SHIPTO_NAME2)));
		address.setFirstName(new String(request.getParameter(PARAM_SHIPTO_FIRSTNAME)));
		address.setLastName(new String(request.getParameter(PARAM_SHIPTO_LASTNAME)));
		address.setStreet(new String(request.getParameter(PARAM_SHIPTO_STREET)));
		address.setHouseNo(new String(request.getParameter(PARAM_SHIPTO_HOUSE_NO)));
		address.setCity(new String(request.getParameter(PARAM_SHIPTO_CITY)));
		address.setEMail(new String(request.getParameter(PARAM_SHIPTO_EMAIL)));
		address.setCountry(new String(request.getParameter(PARAM_SHIPTO_COUNTRY)));
		address.setPostlCod1(new String(request.getParameter(PARAM_SHIPTO_POSTL_COD1)));
		address.setId(new String(request.getParameter(PARAM_SHIPTO_ADDR_NR)));
		address.setType(new String(request.getParameter(PARAM_SHIPTO_ADDR_TYPE)));
		address.setOrigin(new String(request.getParameter(PARAM_SHIPTO_ADDR_ORIGIN)));
		address.setPersonNumber(new String(request.getParameter(PARAM_SHIPTO_ADDR_NP)));
		
		shipto.setAddress(address);
		if (!preOrderSalesDocument.getShipToList().contains(shipto)) {
			preOrderSalesDocument.addShipTo(shipto);
		}
		
		if ( (itemKey != null) && (itemKey.length() != 0) && !itemKey.equals("undefined") ) {
		  	preOrderSalesDocument.getItem(new TechKey(itemKey)).setShipTo(shipto);
		} 
		else {
			preOrderSalesDocument.getHeader().setShipTo(shipto);	
		}
		preOrderSalesDocument.update(bupama);
		userSessionData.removeAttribute("SEARCHSHIPTOITEMKEY");

        return mapping.findForward(FORWARD_UPDATEDOCUMENTVIEW);
        }
}