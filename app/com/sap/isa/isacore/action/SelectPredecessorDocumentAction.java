/*****************************************************************************
    Class         SelectPredecessorDocumentAction
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP AG
    Created:      08.06.2001
    Version:      1.0

    $Revision: #2 $
    $Date: 2001/07/03 $
*****************************************************************************/

package com.sap.isa.isacore.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.backend.boi.isacore.order.DocumentListFilterData;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.businessobject.order.DocumentListFilter;
import com.sap.isa.businessobject.order.OrderDataContainer;
import com.sap.isa.businessobject.order.OrderStatus;
import com.sap.isa.businessobject.order.PartnerList;
import com.sap.isa.businessobject.order.PartnerListEntry;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.businesspartner.businessobject.PartnerFunctionBase;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.order.CreateSalesDocumentBase;

/**
 * Retrieves a list of predecessor documents (OrderTemplates, Quotations) from
 * the backend and displays the list to allow the user a selection
 * of such a document.
 *
 * @author SAP AG
 * @version 1.0
 *
 */
public class SelectPredecessorDocumentAction extends IsaCoreBaseAction {

    private static final String FORWARD_SUCCESS = "showdocs";

    /**
     * List of sales documents stored in request scope.
     */
    public static final String RK_ORDER_LIST = "orderlist";

    /**
     * Number of read document headers
     */
    public static final String RK_DOCUMENT_COUNT = "documentcount";

    /**
     * Type of the source document
     */
    public static final String RK_SOURCE = "source";

    /**
     * Type of the target document
     */
    public static final String RK_TARGET = "target";
    
    /**
     * Sessionc context variable to store last soldToId for document search
     */
    public static final String SC_OOB_SOLDTODID = "oobSoldToId";

	/**
	  * Process type of the target document
	  */
	 public static final String RK_PROCESS_TYPE = "processtype";

    /**
     * Overriden <em>isaPerform</em> method of <em>IsaCoreBaseAction</em>.
     */
    public ActionForward isaPerform(ActionMapping mapping,
                ActionForm form,
                HttpServletRequest request,
                HttpServletResponse response,
                UserSessionData userSessionData,
                RequestParser requestParser,
                BusinessObjectManager bom,
                IsaLocation log)
                        throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        // Get the type of document to create
        String target = requestParser.getParameter("target").getValue().getString();
        String source = requestParser.getParameter("source").getValue().getString();
        String oobSoldToId = requestParser.getParameter(SC_OOB_SOLDTODID).getValue().getString();
       
        RequestParser.Parameter processType = requestParser.getParameter("processtype");
        String processTypeValue = "";
        if (processType != null) {
           processTypeValue = processType.getValue().getString();
        }

        if (target.equals("order")) {
            // Do something order specific
        }
        else if (target.equals("quotation")) {
            // Do something quotation specific
        }
        else {
            // don't allow unknown values
            log.exiting();
            throw new PanicException("unknown value for parameter 'target' provided");
        }

		BusinessPartnerManager buPaMa = bom.createBUPAManager(); 

		Shop shop = bom.getShop();

		// check the given sold to id. <br>
		if (!CreateSalesDocumentBase.checkSoldToParameter(buPaMa,requestParser,userSessionData,shop)) {
			return mapping.findForward("message");
		}	

         // Page that should be displayed next.
        String forwardTo = null;

        // Ask the business object manager for a user. If this object is not
        // already present, the the next action will to login
        User user = bom.getUser();

        if (user == null) {
        	log.exiting();
            return mapping.findForward("login");
        }

        DocumentListFilter filter = new DocumentListFilter();

        if (source.equals("ordertemplate")) {
			//Check if user has permission for displaying OrderTemplate
			  Boolean perm = user.hasPermission(DocumentListFilterData.SALESDOCUMENT_TYPE_ORDERTEMPLATE,
												DocumentListFilterData.ACTION_READ);
			  if (!perm.booleanValue()){
			  		log.exiting();
				  throw new CommunicationException ("User has no permission to display " 
				  									+ DocumentListFilterData.SALESDOCUMENT_TYPE_ORDERTEMPLATE);
			  }
            filter.setTypeORDERTEMPLATE();
        }
        else if (source.equals("quotation")) {
			//Check if user has permission for displaying Quotation
			Boolean perm = user.hasPermission(DocumentListFilterData.SALESDOCUMENT_TYPE_QUOTATION,
											  DocumentListFilterData.ACTION_READ);
			if (!perm.booleanValue()){
				log.exiting();
				throw new CommunicationException ("User has no permission to display " + 
													DocumentListFilterData.SALESDOCUMENT_TYPE_QUOTATION);
			}
            filter.setTypeQUOTA();
        }
        else {
            // don't allow unknown values
            log.exiting();
            throw new PanicException("unknown value for parameter 'source' provided");
        }

        OrderStatus orderList = bom.getOrderStatus();

        if (orderList == null) {
            orderList = bom.createOrderStatus(new OrderDataContainer());
        }
        TechKey soldToKey = (buPaMa.getDefaultBusinessPartner(PartnerFunctionBase.SOLDTO) != null 
                             ? buPaMa.getDefaultBusinessPartner(PartnerFunctionBase.SOLDTO).getTechKey()
                             : new TechKey(""));
        
        if (shop.isSoldtoSelectable()) {
            // take aware of reseller sceanrio
            orderList.setMultiPartnerScenario(true);
    
            // handle the partner list in this case
            PartnerList partnerList = orderList.getPartnerList();
            
            partnerList.clearList();
    
            // get reseller from BuPaMa
            String partnerFunction = shop.getCompanyPartnerFunction();
    
            BusinessPartner buPa = buPaMa.getDefaultBusinessPartner(partnerFunction);
    
            // add reseller to the partner list
            if (buPa != null) {
                PartnerListEntry partner = new PartnerListEntry();
                partner.setPartnerId(buPa.getId());
                partner.setPartnerTechKey(buPa.getTechKey());
                partnerList.setPartnerData(partnerFunction, partner);
            }
            
            userSessionData.removeAttribute(SC_OOB_SOLDTODID);
            
            if (oobSoldToId != null && oobSoldToId.length() > 0) {
                PartnerListEntry partner = new PartnerListEntry();
                partner.setPartnerId(oobSoldToId);
                partnerList.setPartnerData(PartnerFunctionBase.SOLDTO, partner);
                
                userSessionData.setAttribute(SC_OOB_SOLDTODID, oobSoldToId);
            }
    
            orderList.setRequestedPartnerFunctions(new String[]{PartnerFunctionBase.SOLDTO});
            
            // Get Salesdocuments headers
            orderList.readOrderHeaders(soldToKey, shop, filter);
        }
        else {
            soldToKey = null;

            if (oobSoldToId != null && oobSoldToId.length() > 0) {
				BusinessPartner buPa = buPaMa.getBusinessPartner(oobSoldToId);
                soldToKey = buPa.getTechKey();    
            }
            else {  
                // get sold to from BuPaMa
                BusinessPartner buPa = buPaMa.getDefaultBusinessPartner(PartnerFunctionBase.SOLDTO);
                soldToKey = buPa.getTechKey();
            }

            // Get Salesdocuments headers
            orderList.readOrderHeaders(soldToKey, shop, filter);    
        }

        request.setAttribute(RK_DOCUMENT_COUNT, new Integer(orderList.getOrderCount()).toString());
        request.setAttribute(RK_ORDER_LIST, orderList);
		request.setAttribute(RK_PROCESS_TYPE, processTypeValue);

        forwardTo = FORWARD_SUCCESS;
		log.exiting();
        return mapping.findForward(forwardTo);
    }

}
