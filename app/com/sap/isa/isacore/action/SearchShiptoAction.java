package com.sap.isa.isacore.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.SearchShiptoCommand;
import com.sap.isa.businessobject.ShiptoSearch;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ResultData;



public class SearchShiptoAction extends IsaCoreBaseAction {

    public static final String SHIPTORESULT     			= "SHIPTORESULT";
    public static final String FORWARD_RESULT   			= "showshiptoresult";
    public static final String FORWARD_SEARCH   			= "showshiptosearch";
    public static final String FORWARD_CANCEL   			= "cancel";
    public static final String PARAM_PARTNER    			= "partner";
    public static final String PARAM_SHIPTO_BPARTNERID  	= "partnerId";    
    public static final String PARAM_NAME1      			= "name1";
    public static final String PARAM_NAME2      			= "name2";
    public static final String PARAM_CITY       			= "city";
    public static final String PARAM_ZIPCODE    			= "zipCode";
    public static final String PARAM_COUNTRY    			= "country";
    public static final String PARAM_COUNTRYISO 			= "countryiso";
    public static final String PARAM_REGION     			= "region";
    public static final String PARAM_STREET     			= "street";
    public static final String PARAM_HOUSENO    			= "houseNo";
    public static final String PARAM_SEARCH     			= "search";
    public static final String PARAM_CANCEL     			= "cancel";
    public static final String PARAM_SHIPTOSEARCH 			= "search";
    public static final String PARAM_NOENTRY    			= "noentry";



    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
//     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
//     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            BusinessObjectManager bom,
            IsaLocation log)
            throws CommunicationException {

        // Page that should be displayed next.
        String forwardTo 	= null;
		Shop shop 			= bom.getShop();
		User user			= bom.getUser();
		BusinessPartnerManager bupama = (BusinessPartnerManager) bom.getBUPAManager();
		
        //which button was pressed?
       	if (requestParser.getParameter(PARAM_CANCEL).isSet()) {
       	    forwardTo = FORWARD_CANCEL;
        	return mapping.findForward(forwardTo);
      	 }

        ShiptoSearch search = bom.createShiptoSearch();

        if (search == null) {
            throw new PanicException("ShiptoSearch.notFound");
        }

        SearchShiptoCommand cmd = new SearchShiptoCommand();

//      fill the search command object
		cmd.setPartner(bupama.getDefaultBusinessPartner(PartnerFunctionData.SOLDTO).getId());

		cmd.setShiptoBPartnerId(requestParser.getParameter(PARAM_SHIPTO_BPARTNERID).getValue().getString());
        cmd.setName1((String) request.getParameter(PARAM_NAME1));
        cmd.setName2((String) request.getParameter(PARAM_NAME2));
        cmd.setCity(requestParser.getParameter(PARAM_CITY).getValue().getString());
        cmd.setZipCode(requestParser.getParameter(PARAM_ZIPCODE).getValue().getString());
        cmd.setCountry(requestParser.getParameter(PARAM_COUNTRY).getValue().getString());
//      cmd.setCountryiso(requestParser.getParameter(PARAM_COUNTRYISO).getValue().getString());
//      cmd.setRegion(requestParser.getParameter(PARAM_REGION).getValue().getString());
        cmd.setStreet(requestParser.getParameter(PARAM_STREET).getValue().getString());
        cmd.setHouseNo(requestParser.getParameter(PARAM_HOUSENO).getValue().getString());

        ResultData resultData = (ResultData) search.searchShipto(cmd, shop);

      if (resultData.getNumRows() == 0) {
           // forward zur�ck zum suchbild
           forwardTo = FORWARD_SEARCH;
      }
      else {
          // forward zur Ergebniss
          forwardTo = FORWARD_RESULT;
      }

        request.setAttribute(PARAM_SHIPTOSEARCH,search);
        request.setAttribute(SHIPTORESULT,resultData);


        return mapping.findForward(forwardTo);

    }

}