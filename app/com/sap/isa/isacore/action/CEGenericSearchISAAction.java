/*****************************************************************************
    Class:        CEGenericSearchISAAction
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author        SAP AG
*****************************************************************************/
package com.sap.isa.isacore.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.ui.uiclass.genericsearch.GenericSearchUIData;

/**
 * ISA specific part to allow a crossentry for the generic search framework 
 */
public class CEGenericSearchISAAction extends GenericSearchISAAction {
	
	public ActionForward ecomPerform(ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response,
			UserSessionData userSessionData,
			RequestParser requestParser,
			MetaBusinessObjectManager mbom,
			boolean multipleInvocation,
			boolean browserBack)
			throws CommunicationException {
				
			final String METHOD_NAME = "ecomPerform()";
			log.entering(METHOD_NAME);
		   BusinessObjectManager bom = (BusinessObjectManager)mbom.getBOMbyName("ISACORE-BOM");
		           
		   request.setAttribute(GenericSearchUIData.RC_DATEFORMAT, bom.getShop().getDateFormat().toLowerCase());
		   request.setAttribute(GenericSearchUIData.RC_NUMBERFORMAT, bom.getShop().getDecimalPointFormat());       
		   userSessionData.setAttribute("genericsearch.start", "true");
			   
		   super.ecomPerform(mapping, form, request, response, userSessionData,
		                     requestParser, mbom, multipleInvocation, browserBack);
		   log.exiting();
		   return mapping.findForward("default");
    }
}