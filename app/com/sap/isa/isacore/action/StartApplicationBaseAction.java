/*****************************************************************************
    Class:        StartApplicationBaseAction
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      20.09.2004
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.isacore.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.RequestProcessor;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.ui.context.ReInvokeContainer;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.StartupParameter;

//TODO docu
/**
 * The class StartApplicationBaseAction handles the reinvoke of an invalid session. <br>
 * The reinvoke is necessary to support bookmarks. <br>
 * The reinvoke is taken from the {@link com.sap.isa.core.ui.context.ReInvokeContainer} 
 * object.
  *
 * @author  SAP AG
 * @version 1.0
 */
abstract public class StartApplicationBaseAction extends EComBaseAction {

    /**
     * Overwrite this method to start your application. <br>
     * 
     * @param mapping struts action mapping
     * @param form struts form
     * @param request http request
     * @param response http response
     * @param userSessionData user Session Data 
     * @param requestParser
     * @param mbom meta business object manager
     * @return logical forward 
     * 
     * @throws IOException
     * @throws ServletException
     * 
     * 
     * @see com.sap.isa.core.BaseAction#doPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    abstract public ActionForward doStartApplication(ActionMapping mapping, 
                                                       ActionForm form,
                                                       HttpServletRequest request, 
                                                       HttpServletResponse response,
                                                       UserSessionData userSessionData,
                                                       MetaBusinessObjectManager mbom,
                                                       StartupParameter startupParameter)
    		throws IOException, ServletException, CommunicationException;
    
    
    /**
     * Action to start the application. <br>
     * First the abstract method {@link #doStartApplication(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)}
     * is called.
     * Afterwards the method checks if a ReInvoke Container could be found in the 
     * {@link com.sap.isa.core.UserSessionData} and call then the corresponding 
     * action. <br>
     * 
     * @param mapping struts action mapping
     * @param form struts form
     * @param request http request
     * @param response http response
     * @param userSessionData user Session Data 
     * @param requestParser
     * @param mbom meta business object manager
     * @param multipleInvocation
     * @param browserBack
     * @return forward given from <code>doStartApplication</code> or the action 
     *  taken from the reinvoke container.  
     * @throws IOException
     * @throws ServletException
     * @throws CommunicationException
     * 
     * 
     * @see com.sap.isa.isacore.action.EComBaseAction#ecomPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.sap.isa.core.UserSessionData, com.sap.isa.core.util.RequestParser, com.sap.isa.core.businessobject.management.MetaBusinessObjectManager, boolean, boolean)
     */
    public ActionForward ecomPerform(ActionMapping mapping, 
            						  ActionForm form,
            						  HttpServletRequest request, 
            						  HttpServletResponse response,
            						  UserSessionData userSessionData, 
            						  RequestParser requestParser,
            						  MetaBusinessObjectManager mbom, 
            						  boolean multipleInvocation,
            						  boolean browserBack) 
    		throws IOException, ServletException, CommunicationException {
		final String METHOD_NAME = "ecomPerform()";
		log.entering(METHOD_NAME);
        StartupParameter startupParameter = (StartupParameter)userSessionData.getAttribute(SessionConst.STARTUP_PARAMETER);
        
        ActionForward forward = doStartApplication(mapping,
                                                   form,
                                                   request,
                                                   response,
                                                   userSessionData,
                                                   mbom,
                                                   startupParameter);

        ReInvokeContainer reInvokeContainer = (ReInvokeContainer)userSessionData.getAttribute(SessionConst.REINVOKE_CONTAINER);
        if (reInvokeContainer != null) {
        	
        	String path = reInvokeContainer.reInvoke(request);

			forward = new ActionForward(path);
			forward.setRedirect(RequestProcessor.getRedirectForReinvoke());
			forward.setName("redirect");

		    userSessionData.removeAttribute(SessionConst.REINVOKE_CONTAINER);	
        }

	    userSessionData.setInitialized(true);
		log.exiting();
        return forward;
    }

}
