/*****************************************************************************
    Class:        DocumentListAction
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author        Thomas Smits
    Created:      5.7.2001

    $Revision: #1 $
    $Date: 2001/07/05 $
*****************************************************************************/

package com.sap.isa.isacore.action.test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;


public class ThrowExceptionAction extends IsaCoreBaseAction {
	
    public ThrowExceptionAction() {
    }

    public ActionForward isaPerform(ActionMapping mapping,
        ActionForm form,
        HttpServletRequest request,
        HttpServletResponse response,
        UserSessionData userSessionData,
        RequestParser requestParser,
        BusinessObjectManager bom,
        IsaLocation log)
        throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        String type = request.getParameter("type");

        if (type.equals("communicationexception")) {
			log.exiting();
            throw new CommunicationException(request.getParameter("message"));
        }
        else if (type.equals("runtimeexception")) {
			log.exiting();
            throw new RuntimeException(request.getParameter("message"));
        }
        else if (type.equals("panicexception")) {
			log.exiting();
            throw new PanicException(request.getParameter("message"));
        }
		log.exiting();
        return mapping.findForward("");
    }
}