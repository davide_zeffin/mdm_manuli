/*****************************************************************************
    Class         ShowDeterminationAction
    Copyright (c) 2003, SAP AG, All rights reserved.
    Description:  Action to show the product determination screen
    Author:
    Created:      15. Mai 2003
    Version:      1
*****************************************************************************/

package com.sap.isa.isacore.action;

import java.util.HashSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.ManagedDocumentDetermination;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.ShipTo;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.order.Basket;
import com.sap.isa.businessobject.order.CollectiveOrder;
import com.sap.isa.businessobject.order.CustomerOrder;
import com.sap.isa.businessobject.order.Order;
import com.sap.isa.businessobject.ordertemplate.OrderTemplate;
import com.sap.isa.businessobject.quotation.Quotation;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.eai.BackendRuntimeException;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.MessageDisplayer;
import com.sap.isa.isacore.action.order.MaintainBasketBaseAction;
import com.sap.isa.isacore.action.order.MaintainOrderBaseAction;

/**
 * Read and display the determination jsp.
 *
 * @author SAP AG
 * @version 1
 */

public class ShowDeterminationAction extends IsaCoreBaseAction {
    
    /**
     * Request context constant for storage of the shiptoaddress
     */
    public static final String RC_SHIPTO_ADR = "detshiptoadr";
    
    /**
     * Request context constant for storage of the shiptping condidtion
     */
    public static final String RC_SHIPCOND_DESC = "detshipconddesc";
    
    /**
     * Request context constant to store for which items the user has choosen,
     * to not select an alternative product
     */
    public static final String RC_SELECTED_NONE = "detselnone";
    
    /**
     * Request context constant to store the mode of the document
     */
    public static final String RC_DOC_MODE = "detdocmode";
	
    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser parser,
            BusinessObjectManager bom,
            IsaLocation log) throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        // Page that should be displayed next.
        String forwardTo = "showdet";
        
        DocumentHandler documentHandler = (DocumentHandler)
        userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
        ManagedDocument mandoc = documentHandler.getManagedDocumentOnTop();
        
        if (mandoc == null) { // haben wir eins?
                   // use global forward to show valid frame
                   // this if statement is also reached, if the readForUpdate below failed
                   // and the message.jsp was displayed and the user clicks on the back icon
                   // of the browser, because in this case the last executed action, which is this action,
                   // is recalled. But in that case, the managed document was already deleted
            log.exiting();
            return mapping.findForward("updatedocumentview");
        }
        
        String docMode = ((ManagedDocumentDetermination) mandoc).getDocMode();
        HashSet selectedNone = ((ManagedDocumentDetermination) mandoc).getSelectedNone();
        
        SalesDocument salesDoc = (SalesDocument) mandoc.getDocument();
        String docType;
        
        
        try {

            if (salesDoc instanceof Order) {
               ((Order) salesDoc).readForUpdate(bom.getShop());
               docType = HeaderData.DOCUMENT_TYPE_ORDER;
            }
            else if (salesDoc instanceof OrderTemplate) {
                if (docMode.equals(ManagedDocumentDetermination.DOC_MODE_NEW)) {
                   ((OrderTemplate) salesDoc).readForUpdate(bom.getShop());
                }
                else {
                   salesDoc.read();  
                }
                docType = HeaderData.DOCUMENT_TYPE_ORDERTEMPLATE;
            } else if (salesDoc instanceof Basket) {
                salesDoc.read();
                docType = HeaderData.DOCUMENT_TYPE_BASKET;
            } else if (salesDoc instanceof Quotation) {
                salesDoc.read();
                docType = HeaderData.DOCUMENT_TYPE_QUOTATION;
            }
            else {
            	log.exiting();
                throw new PanicException("No valid document returned from document handler");
            }
        }
        catch (BackendRuntimeException ex) {

          log.debug("BackendRuntimeException", ex);
            
          documentHandler.release(salesDoc);

          // create a Message Displayer for the error page!
          MessageDisplayer messageDisplayer = new MessageDisplayer();

          messageDisplayer.addToRequest(request);
          messageDisplayer.copyMessages(salesDoc);

          // the action which is to call after the message
          if (salesDoc instanceof CustomerOrder) {
              messageDisplayer.setAction("customerdocumentstatusdetailprepare.do");
          }
          else if (salesDoc instanceof CollectiveOrder) {
              messageDisplayer.setAction("collectivedocumentstatusdetailprepare.do");
          }
          else {
              messageDisplayer.setAction("documentstatusdetailprepare.do");
          }

          messageDisplayer.setActionParameter("?techkey="+salesDoc.getTechKey().getIdAsString()
                                               +"&objecttype="+mandoc.getDocType()
                                               +"&objects_origin=&object_id=" );
		  log.exiting();
          return mapping.findForward("message");
        }
        
        String shiptoAddr = "";
        String shipCondDesc = "";
        HeaderSalesDocument header = salesDoc.getHeader();
        
        ShipTo[] shipTos = salesDoc.getShipTos();
        
        for (int i = 0; i < shipTos.length; i++) {
            if (shipTos[i].equals(header.getShipTo())) {
                shiptoAddr = shipTos[i].getShortAddress();
            }
        }
        
        ResultData shipConds = salesDoc.readShipCond(bom.getShop().getLanguage());
        
        shipConds.beforeFirst();
        
        while(shipConds.next()) {
            if (shipConds.getRowKey().toString().equals(header.getShipCond())) {
                shipCondDesc = shipConds.getString(1);
            }
        }
        
        userSessionData.setAttribute(MaintainBasketBaseAction.SC_DOCTYPE, docType);
        request.setAttribute(RC_SHIPTO_ADR, shiptoAddr);
        request.setAttribute(RC_SHIPCOND_DESC, shipCondDesc);
        request.setAttribute(RC_SELECTED_NONE, selectedNone);
        request.setAttribute(RC_DOC_MODE, docMode);
        request.setAttribute(MaintainOrderBaseAction.RC_HEADER, salesDoc.getHeader());
        request.setAttribute(MaintainOrderBaseAction.RC_ITEMS, salesDoc.getItems());
        log.exiting();
        return mapping.findForward(forwardTo);
    }

}