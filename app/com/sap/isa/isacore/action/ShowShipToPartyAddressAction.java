package com.sap.isa.isacore.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.Address;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.ShipTo;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.AddressConstants;
import com.sap.isa.isacore.CRAddressFormular;

/*****************************************************************************
		Class         ShowShipToAction
		Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
		Description:  Action for the Profile Maintenance
		Author:       SAPMarkets
		Created:      January 2007
		Version:      1.0
****************************************************************************/
/**
	 * Displays a ship to address. There are three different ways to provide the
	 * ship to address, which is to use.
	 * <ol>
	 * <li>The parameter/attribute <code>PARAM_SHIPTO</code> is set to
	 * <code>VALUE_SHIPTO_FROM_RC</code>. The address is taken from
	 * parameter/attribute <code>ActionConstants.RC_ADDRESS</code>
	 * </li>
	 * <li>The parameter <code>PARAM_SHIPTO_INDEX</code> is set. The address is
	 * taken from the ship to array <code>MaintainBasketBaseAction.SC_SHIPTOS</code>
	 * </li>
	 * <li>The parameter <code>PARAM_SHIPTO_TECHKEY</code> is set. The address is
	 * taken from the ship to array <code>MaintainBasketBaseAction.SC_SHIPTOS</code>
	 * </li>
	 * </ol>.
	 * <b>Note:</b> The action checks the possibilities in this order!
	 *
	 * @author SAP
	 * @version 1.0
	 *
	 */
public class ShowShipToPartyAddressAction extends EComBaseAction {

	/**
	 * Name of the "disabled" parameter stored in the request context.
	 */
	public static final String RC_DISABLED = "disabled";

	/**
	 * Name of the itemGuid parameter stored in the request context.
	 */
	public static final String RC_SHIPTO_KEY = "shipToKey";

	/**
	 * Name of the itemGuid parameter stored in the request context.
	 */
	public static final String RC_ITEMKEY = "itemKey";

	/**
	 * Name of the request parameter for the shipto index.
	 */
	public static final String PARAM_SHIPTO_INDEX = "shipToIndex";

	/**
	 * Name of the request parameter for the shiptolinekey.
	 */
	public static final String PARAM_SHIPTO_TECHKEY = "shipToTechKey";

	/**
	 * Name of the request parameter for the shipto index.
	 */
	public static final String PARAM_SHIPTO_KEY = "shipToKey";

	/**
	 * Name of the request parameter for the shipto index.
	 */
	public static final String PARAM_ITEMKEY = "itemKey";

	/**
	 * Name of the request parameter for the shipto source
	 */
	public static final String PARAM_SHIPTO_SOURCE = "shipToSource";

	/**
	 * Value of the request parameter indicating that the shipto should be
	 * taken from the request context
	 */
	public static final String VALUE_SHIPTO_FROM_RC = "requestContext";

	/**
	 * Session context constant for storage of the list of available ship tos
	 */
	public static final String SC_SHIPTOS =
		"com.sap.isa.isacore.action.order.MaintainBasketAction.shiptos";

	/**
	 * Implement this method to add functionality to your action.
	 *
	 * @param form              The <code>FormBean</code> specified in the
	 *                          config.xml file for this action
	 * @param request           The request object
	 * @param response          The response object
	 * @param userSessionData   Object wrapping the session
	 * @param requestParser     Parser to simple retrieve data from the request
	 * @param bom               Reference to the BusinessObjectManager
	 * @param log               Reference to the IsaLocation, needed for logging
	 * @return Forward to another action or page
	 */
	public ActionForward ecomPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack)
		throws IOException, ServletException, CommunicationException {

		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);

		CRAddressFormular addressFormular = new CRAddressFormular(0);
		// the adress format can be read from a shop: shop.getAddressFormat().

		Address address = null;

		/* specify in a request parameter from where the ship to addres should be taken */
		if (requestParser.getParameter(PARAM_SHIPTO_SOURCE).isSet()) {
			String shipToSource =
				requestParser
					.getParameter(PARAM_SHIPTO_SOURCE)
					.getValue()
					.getString();

			/* take address from request context */
			if (VALUE_SHIPTO_FROM_RC.equals(shipToSource)) {
				address =
					(Address) request.getAttribute(AddressConstants.RC_ADDRESS);
				if (requestParser
					.getParameter(RC_DISABLED)
					.getValue()
					.getString()
					.equals("true")) {
					addressFormular.setEditable(false);
				}
			}
		}

		/* specify in a request attribute from where the ship to addres should be taken */
		else if (requestParser.getAttribute(PARAM_SHIPTO_SOURCE).isSet()) {
			String shipToSource =
				(String) request.getAttribute(PARAM_SHIPTO_SOURCE);

			/* take address from request context */
			if (VALUE_SHIPTO_FROM_RC.equals(shipToSource)) {
				address =
					(Address) request.getAttribute(AddressConstants.RC_ADDRESS);
				if (requestParser
					.getAttribute(RC_DISABLED)
					.getValue()
					.getString()
					.equals("true")) {
					addressFormular.setEditable(false);
				}
			}
		} else if (requestParser.getParameter(PARAM_SHIPTO_INDEX).isSet()) {

			int index =
				requestParser
					.getParameter(PARAM_SHIPTO_INDEX)
					.getValue()
					.getInt();

			ShipTo[] shipTos =
				(ShipTo[]) userSessionData.getAttribute(SC_SHIPTOS);

			address = shipTos[index].getAddress();

			addressFormular.setEditable(false);

			request.setAttribute(
				RC_SHIPTO_KEY,
				shipTos[index].getTechKey().getIdAsString());

		} else if (requestParser.getParameter(PARAM_SHIPTO_TECHKEY).isSet()) {

			String lineKey =
				requestParser
					.getParameter(PARAM_SHIPTO_TECHKEY)
					.getValue()
					.getString();

			ShipTo[] shipTos =
				(ShipTo[]) userSessionData.getAttribute(SC_SHIPTOS);

			for (int i = 0; i < shipTos.length; i++) {
				if (shipTos[i] != null) {
					if (lineKey
						.equals(shipTos[i].getTechKey().getIdAsString())) {
						address = shipTos[i].getAddress();
						request.setAttribute(
							RC_SHIPTO_KEY,
							shipTos[i].getTechKey().getIdAsString());
						break;
					}
				}
			}

			addressFormular.setEditable(false);

		}

		if (requestParser.getParameter(PARAM_ITEMKEY).isSet()) {
			request.setAttribute(
				RC_ITEMKEY,
				requestParser
					.getParameter(PARAM_ITEMKEY)
					.getValue()
					.getString());
		}

		if (requestParser.getParameter(PARAM_SHIPTO_KEY).isSet()) {
			request.setAttribute(
				RC_SHIPTO_KEY,
				requestParser
					.getParameter(PARAM_SHIPTO_KEY)
					.getValue()
					.getString());
		}

		addressFormular.setAddress(address);
		request.setAttribute(AddressConstants.RC_ADDRESS_FORMULAR, addressFormular);
		request.setAttribute(AddressConstants.RC_ADDRESS, address);
		log.exiting();
		return mapping.findForward("showshipto");

	}

}
