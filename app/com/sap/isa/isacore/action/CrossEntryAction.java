/*****************************************************************************
    Class:        CrossEntryAction
    Copyright (c) 2002, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author        Ralf Witt

    $Revision: #4 $
    $DateTime: 2004/04/30 12:35:01 $ (Last changed)
    $Change: 185020 $ (changelist)

*****************************************************************************/
package com.sap.isa.isacore.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.webcatalog.CatalogBusinessObjectManager;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;

/**
 *
 * Checks where the call comes from, and routes to the specific action
 * provided in parameter <b>nextaction</b>
 */
public class CrossEntryAction extends IsaCoreBaseAction {

    // Constants
    public static final String RP_NEXT_ACTION = "nextaction";
    /**
     * The value passed thru with this URL parameter will be set into session
     * context and can be check later on in the application.
     */
    public static final String RP_APPL_FORWARD = "applforward";
    
	public static final String RP_OBJECT_ID = "CRM_OBJECT_ID";
	
    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            BusinessObjectManager bom,
            IsaLocation log)
            throws CommunicationException {
            	
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        String nextAction = request.getParameter(RP_NEXT_ACTION);
        String applForward = request.getParameter(RP_APPL_FORWARD);
        if (applForward != null  &&  applForward.length() > 0) {
            userSessionData.setAttribute(RP_APPL_FORWARD, applForward);
        } else {
            userSessionData.removeAttribute(RP_APPL_FORWARD);
        }

		Shop shop = bom.getShop();

		// First check application is initialized correctly. Shop and soldto / reseller
        // have been selected
        if (shop == null) {
	        // Shop is not yet present, forward to shop selection
	        if (log.isDebugEnabled()) log.debug("Shop not found => shopselection");
	        return mapping.findForward("shopselection");
        } else {
	        // SoldTo / Reseller is not yet present, forward to SoldTo selection 
	        BusinessPartner soldToEntry   = bom.getBUPAManager().getDefaultBusinessPartner(PartnerFunctionData.SOLDTO);
	        BusinessPartner resellerEntry = bom.getBUPAManager().getDefaultBusinessPartner(PartnerFunctionData.RESELLER);
	        BusinessPartner agentEntry = bom.getBUPAManager().getDefaultBusinessPartner(PartnerFunctionData.AGENT);
	        
            if (soldToEntry == null  &&  resellerEntry == null && agentEntry == null) {
                if (log.isDebugEnabled()) log.debug("No Businesspartner found => soldtoselection");         
	            return mapping.findForward("soldtoselection");
	        } else {
				CatalogBusinessObjectManager cbom =
						getCatalogBusinessObjectManager(userSessionData);
			    if (cbom.getCatalog() == null && shop.isInternalCatalogAvailable()) {
			    	if (shop.isCatalogDeterminationActived()) {
					    if (log.isDebugEnabled()) log.debug("Catalog determination is activated => catalogselection");
						return mapping.findForward("catalogselection");
			    	} else {
						throw new PanicException("CrossentryAction: Catalog not available in context!" );
			    	}
			    }
	        }
        }	

        // Application is completely initialized
        if (log.isDebugEnabled()) log.debug("Request Parameter nextaction:" + nextAction);
		
		// correction for interaction history navigation, BEGIN
		// as next action parametre cannot be passed, nextaction == null and object_id is passed is the
		// criterion for setting the nextaction to display the document details
		String objectID = request.getParameter(RP_OBJECT_ID);		
		if (nextAction == null  ||  (nextAction.equals("")) ) {
			if (objectID != null  &&  (! objectID.equals("")) ) {	
				nextAction = "cedocumentdetails";
				request.setAttribute("doc_key", objectID);				
				request.setAttribute("doc_type", "order");
				
				if (log.isDebugEnabled()) log.debug("Forward:" + nextAction);
				log.exiting();
				return mapping.findForward(nextAction);
			}
		}
		// correction for interaction history navigation, END

        if (nextAction != null  &&  (! nextAction.equals("")) ) {
            if (log.isDebugEnabled()) log.debug("Forward:" + nextAction);
	        log.exiting();
	        return mapping.findForward(nextAction);
        } else {
	        if (log.isDebugEnabled()) log.debug("Forward default");
	        log.exiting();
	        return mapping.findForward("default");
        }

    }
}