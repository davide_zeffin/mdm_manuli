/*****************************************************************************
  Copyright (c) 2000, SAP AG, All rights reserved.
  Author:       SAP
  Created:      17 April 2001

  $Revision: #5 $
  $Date: 2003/10/23 $
*****************************************************************************/


package com.sap.isa.isacore.action.b2c;

// framework dependencies
import javax.servlet.http.HttpServletRequest;

import com.sap.isa.businessobject.Address;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.isacore.AddressFormular;


/**
 * Prepares the displaying of the sold-to address data by setting all those needed
 * data as request attributes.
 */
public class DisplayCurrentAddressAction extends DisplayAddressBaseAction {
	
	/**
	 * Return the address which should be used. <br>
	 * The address will be taken from the addressFormular which is store in the 
	 * request. If the address formular can't be found in the request the 
	 * address will be read from business object.
	 * 
	 * @param request current request 
	 * @param partner business partner, which address should be changed.
	 * @return
	 */
	protected Address getAddress(HttpServletRequest request,
								  UserSessionData userSessionData,
								  BusinessObjectManager bom) {


        AddressFormular addressFormular = (AddressFormular) request.getAttribute(B2cConstants.ADDRESS_FORMULAR);
        
        Address address = null;
        
        if (addressFormular != null) {
        	address =  addressFormular.getAddress();
        }
        
        return address;
    }
}
