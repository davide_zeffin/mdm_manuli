/**
 * Class         SoldToReadActionB2C Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
 * Description:  Action to display the SoldToList Created:      February 2002 Version:      0.1 $Revision: #1 $ $Date:
 * 2001/06/26 $
 */
package com.sap.isa.isacore.action.b2c;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.order.Basket;
import com.sap.isa.businessobject.order.PartnerList;
import com.sap.isa.businessobject.order.PartnerListEntry;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.businesspartner.businessobject.SoldTo;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Read the Sold Tos for the a given conatct person. <br>
 */
public class ReadSoldToActionB2C extends IsaCoreBaseAction {
    public static final String RK_SOLDTO_LIST = "soldtolist";

    /** Name of the request parameter directly setting the soldTo. */
    public static final String PARAM_SOLDTO = "soldTo";

    /** Name of the request parameter directly setting the soldToTechKey. */
    public static final String PARAM_SOLDTO_TECHKEY = "soldToTechKey";

    /**
     * creates the parameter list for the soldTo
     *
     * @param soldToList DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public static String createRequest(ResultData soldToList) {
        return "?" + PARAM_SOLDTO_TECHKEY + "=" + soldToList.getString("SOLDTO_TECHKEY") + "&" + PARAM_SOLDTO + "=" +
               soldToList.getString("SOLDTO");
    }

    /**
     * Implement this method to add functionality to your action.
     *
     * @param mapping DOCUMENT ME!
     * @param form The <code>FormBean</code> specified in the config.xml file for this action
     * @param request The request object
     * @param response The response object
     * @param userSessionData Object wrapping the session
     * @param requestParser Parser to simple retrieve data from the request
     * @param bom Reference to the BusinessObjectManager
     * @param log Reference to the IsaLocation, needed for logging
     *
     * @return Forward to another action or page
     *
     * @throws CommunicationException DOCUMENT ME!
     */
    public ActionForward isaPerform(ActionMapping         mapping,
                                    ActionForm            form,
                                    HttpServletRequest    request,
                                    HttpServletResponse   response,
                                    UserSessionData       userSessionData,
                                    RequestParser         requestParser,
                                    BusinessObjectManager bom,
                                    IsaLocation           log)
                             throws CommunicationException {
        final String METHOD_NAME = "isaPerform()";
        log.entering(METHOD_NAME);
        
        String forward = "success";

        RequestParser.Value soldToValue = requestParser.getParameter(PARAM_SOLDTO).getValue();
        RequestParser.Value soldToTechKeyValue = requestParser.getParameter(PARAM_SOLDTO_TECHKEY).getValue();

        if (!soldToValue.isSet()) {
            // No request parameter, try the request attribute
            soldToValue = requestParser.getAttribute(PARAM_SOLDTO).getValue();
            soldToTechKeyValue = requestParser.getAttribute(PARAM_SOLDTO_TECHKEY).getValue();
        }

        // check if the soldTo parameter exist
        if (soldToTechKeyValue.isSet()) {
            readSoldToB2C(bom, soldToTechKeyValue.getString());
        }
        else {
            forward = "failure";
        }

        log.exiting();
        return mapping.findForward(forward);
    }

    /**
     * DOCUMENT ME!
     *
     * @param bom DOCUMENT ME!
     * @param soldToTechKeyValue DOCUMENT ME!
     *
     * @throws CommunicationException DOCUMENT ME!
     */
    public void readSoldToB2C(BusinessObjectManager bom,
                              String                soldToTechKeyValue)
                       throws CommunicationException {
                           
        TechKey soldToTechKey = new TechKey(soldToTechKeyValue);

        BusinessPartnerManager buPaMa = bom.createBUPAManager();
        BusinessPartner partner = buPaMa.createBusinessPartner(soldToTechKey, null, null);

        SoldTo soldTo = new SoldTo();
        partner.addPartnerFunction(soldTo);
        buPaMa.setDefaultBusinessPartner(partner.getTechKey(), soldTo.getName());

        Basket basket = bom.getBasket();

        if (basket != null) {
            PartnerList pList = basket.getHeader().getPartnerList();
            BusinessPartner bpSoldTo = bom.createBUPAManager().getDefaultBusinessPartner(PartnerFunctionData.SOLDTO);
            pList.setSoldTo(new PartnerListEntry(bpSoldTo.getTechKey(), bpSoldTo.getId()));
            basket.updateHeader(bom.createBUPAManager());
        }
    }
}
