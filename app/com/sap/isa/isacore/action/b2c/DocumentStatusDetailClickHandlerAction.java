/*****************************************************************************
    Class:        DocumentStatusDetailClickHandlerAction
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP AG
    Created:      12 October 2001

    $Revision: #4 $
    $Date: 2002/08/05 $
*****************************************************************************/
package com.sap.isa.isacore.action.b2c;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.businessobject.ipc.IPCBOManager;
import com.sap.isa.businessobject.item.BasketTransferItemImpl;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.businessobject.order.DocumentListFilter;
import com.sap.isa.businessobject.order.Order;
import com.sap.isa.businessobject.order.OrderChange;
import com.sap.isa.businessobject.order.OrderStatus;
import com.sap.isa.businessobject.ordertemplate.OrderTemplate;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.MessageList;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.action.order.AddToBasketAction;
import com.sap.isa.isacore.action.order.DocumentStatusDetailAction;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.imp.IPCClientObjectFactory;
import com.sap.spc.remote.client.object.imp.rfc.RFCIPCItemReference;


/**
 * Retrievs items from HTTP request.
 *   Possible processes are: - delete item(s) from saved basket (order template)
 *                           - cancel item(s) from order
 *                           - transfer item(s) to basket (shopping cart)
 */
public class DocumentStatusDetailClickHandlerAction extends IsaCoreBaseAction {
	
    /**
     * Overriden <em>isaPerform</em> method of <code>IsaCoreBaseAction</code>.
     */
    public ActionForward isaPerform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response, UserSessionData userSessionData, RequestParser requestParser, BusinessObjectManager bom, IsaLocation log) throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        List                   transferItems = null;
        BasketTransferItemImpl transferItem = null;
        OrderStatus            orderStatus = null;
        ItemSalesDoc           itemSalesDoc = null;

        // read the shop data
        Shop shop = bom.getShop();

        if (shop == null) {
        	log.exiting();
            throw new PanicException("shop.notFound");
        }

        // get user
        User user = bom.getUser();

        if (user == null) {
        	log.exiting();
            return mapping.findForward("login");
        }

        // get OrderStatus
        orderStatus = bom.getOrderStatus();

        if (orderStatus == null) {
        	log.exiting();
            throw new PanicException("status.message.nostatusobject");
        }

        // create a new list of basket transfer items
        transferItems = new ArrayList();

        RequestParser.Parameter checkbox = requestParser.getParameter((DocumentStatusDetailAction.RK_CHECKBOXNAME + "[]"));
        RequestParser.Parameter checkboxhidden = requestParser.getParameter((DocumentStatusDetailAction.RK_CHECKBOXHIDDEN + "[]"));

        TechKey                 markedItems[] = new TechKey[checkboxhidden.getNumValues()];
        int                     cntItemsMarked = 0;

        if (checkbox != null) {
            String on;
            String techKeyValue;

            if (log.isDebugEnabled()) {
                log.debug("--------------------------------------------------------");
            }

            if ((request.getParameter("cancelpressed").length() > 0) || (request.getParameter("deletepressed").length() > 0)) {
                // BUTTON cancel OR delete PRESSED
                if (log.isDebugEnabled()) {
                    log.debug("Button CANCEL or DELETED pressed");
                }
            }

            for (int i = 0; i < checkboxhidden.getNumValues(); i++) {
                // Because it is a simulated index which is overtaken from JSP it starts with 1 (ONE)!!
                on               = checkbox.getValue(i + 1).getString();
                techKeyValue     = checkboxhidden.getValue(Integer.toString(i + 1)).getString();

                if (!(on.equals(""))) { // i'th checkbox marked

                    if (log.isDebugEnabled()) {
                        log.debug("CheckBox (" + i + ") " + on + "  TechKey:" + techKeyValue);
                    }

                    if ((request.getParameter("cancelpressed").length() > 0) || (request.getParameter("deletepressed").length() > 0)) {
                        // BUTTON cancel OR delete PRESSED
                        // Does status allow processing at all, otherwise don't even send item to Backend
                        if (orderStatus.getItemByTechKey(new TechKey(techKeyValue.trim())).getStatus() != DocumentListFilter.SALESDOCUMENT_STATUS_CANCELLED  &&  
                            orderStatus.getItemByTechKey(new TechKey(techKeyValue.trim())).getStatus() != DocumentListFilter.SALESDOCUMENT_STATUS_COMPLETED) {
                            markedItems[cntItemsMarked++] = new TechKey(techKeyValue.trim());
                        }
                    }
                    else {
                        // BUTTON transfer PRESSED
                        // CREATE TransferItem object
                        transferItem     = new BasketTransferItemImpl();

                        // Get item from the orderstatus
                        itemSalesDoc = orderStatus.getItemByTechKey(new TechKey(techKeyValue.trim()));

                        // FILL Transferitem object
                        if (!(itemSalesDoc.getContractKey().getIdAsString().equals(""))) {
                            transferItem.setContractKey(itemSalesDoc.getContractKey().getIdAsString());
                            transferItem.setContractItemKey(itemSalesDoc.getContractItemKey().getIdAsString());
                            transferItem.setContractConfigFilter(itemSalesDoc.getContractConfigFilter());
                        }

                        transferItem.setProductKey(itemSalesDoc.getProductId().toString());
                        transferItem.setProductId(itemSalesDoc.getProduct());
                        transferItem.setQuantity(itemSalesDoc.getQuantity());
                        transferItem.setUnit(itemSalesDoc.getUnit());
                        transferItem.setPtsItem(itemSalesDoc.isPtsItem());
                        transferItem.setBuyPtsItem(itemSalesDoc.isBuyPtsItem());

                        if (itemSalesDoc.isConfigurable()) {
                            IPCItem ipcItem = null;

                            if (itemSalesDoc.getExternalItem() != null) {
                                ipcItem = (IPCItem) itemSalesDoc.getExternalItem();
                            }
                            else {
								RFCIPCItemReference ipcItemReference = (RFCIPCItemReference)IPCClientObjectFactory.getInstance().newIPCItemReference();
                                ipcItemReference.setItemId(itemSalesDoc.getTechKey().getIdAsString());
                                ipcItemReference.setDocumentId(orderStatus.getOrderHeader().getIpcDocumentId().toString());
                                ipcItemReference.setConnectionKey(orderStatus.getOrderHeader().getIpcConnectionKey());

                                IPCBOManager ibom = this.getIPCBusinessObjectManager(userSessionData);

                                /*
                                InteractionConfigContainer data = getInteractionConfig(request);
                                */

                                //String client = orderStatus.getOrderHeader().getIpcClient();                
                                try {
                                	String connectionKey = orderStatus.getOrderHeader().getIpcConnectionKey();
                                    IPCClient ipcClient = ibom.createIPCClientUsingConnection(connectionKey);
                                    ipcItem = ipcClient.getIPCItem(ipcItemReference);
                                }
                                catch (IPCException ex) {
                                    log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "Create IPCClient failed! - No IPC client available!");
                                    log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, ex.getMessage());
                                }
                            }

                            if (ipcItem != null) {
                                transferItem.setConfigurationItem(ipcItem);
                            }
                        }

                        transferItems.add(transferItem);
                    }
                }
                else {
                    if (log.isDebugEnabled()) {
                        log.debug("CheckBox (" + i + ") off");
                    }
                }
            }
             // ENDFOR

            if (log.isDebugEnabled()) {
                log.debug("--------------------------------------------------------");
            }
        }

        // Empty lines in toProcessItems would result in cancelling or deleting ALL items
        TechKey toProcessItems[] = new TechKey[cntItemsMarked];

        for (int i = 0; i < cntItemsMarked; i++) {
            toProcessItems[i] = markedItems[i];
        }

        if (request.getParameter("cancelpressed").length() > 0) {
            // BUTTON cancel PRESSED
            if (cntItemsMarked > 0) {
                Order       order = bom.createOrder(8);
                OrderChange orderChange = new OrderChange(order);
                orderChange.setTechKey(orderStatus.getTechKey());
                order.getHeader().setShop(shop); // Set shop to avoid NullPointer in WrapperCrmIsa.crmIsaBasketGetItems ->
                                                 // checkRedemptionProcessType(salesDoc)

                try {
                    orderChange.readForUpdate();
                    orderChange.cancelItems(toProcessItems);
                    orderChange.saveChanges();
                    MessageList msgL = orderChange.getMessageList();
                    for (int i = 0; i < msgL.size(); i++) {
                        orderStatus.getOrder().addMessage(msgL.get(i));
                    }
                }
                catch (Exception ex) {
                    MessageList msgL = orderChange.getMessageList();

                    for (int i = 0; i < msgL.size(); i++) {
                        orderStatus.getOrder().addMessage(msgL.get(i));
                    }

                    //                    return mapping.findForward("orderstatusdetail");
                }

                bom.releaseOrder(8);
                orderStatus.reReadOrderStatus();
            }
        }
        else if (request.getParameter("deletepressed").length() > 0) {
            // BUTTON delete PRESSED
            if (cntItemsMarked > 0) {
                /*
                Order order = bom.createOrder();
                OrderChange orderChange = new OrderChange(order);
                orderChange.setTechKey(orderStatus.getTechKey());
                order.setBasketId(orderStatus.getTechKey());
                orderChange.readForUpdate(user);
                orderChange.deleteItems(toProcessItems);
                orderChange.saveOrderChange();
                */
                OrderTemplate orderTemplate = bom.createOrderTemplate();
                
                // set the GData here because for the DB-basket we need a better 
                // initialized SalesDocument. Otherwise we receive NullpointerException
                // when we want to accesss the WebCatalog
                orderTemplate.setGData(shop, getCatalogBusinessObjectManager(userSessionData).getCatalog());
                
                orderTemplate.setTechKey(orderStatus.getTechKey());
                orderTemplate.setHeader(orderStatus.getOrderHeader());
                orderTemplate.read();
                orderTemplate.deleteItems(toProcessItems);
                bom.releaseOrderTemplate();
                orderStatus.reReadOrderStatus();
            }
        }
        else {
            // BUTTON transfer PRESSED
            // prepare call of AddToBasketAction
            if (!transferItems.isEmpty()) {
                request.setAttribute(AddToBasketAction.RC_TRANSFER_ITEM_LIST, transferItems);
				log.exiting();
                // Forward to addtodocument which will return to the given Action
                return mapping.findForward("addtodocument");
            }
			
			log.exiting();
            // Forward to refresh minibasket while nothing has been selected, but 'target' had been set
            return mapping.findForward("minibasketrefresh");
        }

        //}
        log.exiting();
        return mapping.findForward("orderstatusdetail");
    }
     // End of Method
}
