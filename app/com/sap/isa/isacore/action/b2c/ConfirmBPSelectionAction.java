package com.sap.isa.isacore.action.b2c;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.Address;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businessobject.businessevent.LoginEvent;
import com.sap.isa.businessobject.order.Basket;
import com.sap.isa.businessobject.order.PartnerList;
import com.sap.isa.businessobject.order.PartnerListEntry;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.businesspartner.businessobject.Contact;
import com.sap.isa.businesspartner.businessobject.SoldTo;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.action.IsaCoreInitAction;
import com.sap.isa.user.util.LoginStatus;

public class ConfirmBPSelectionAction extends IsaCoreBaseAction {

	private boolean CheckCCAgentAuthorization(	UserSessionData userSessionData, 
												MetaBusinessObjectManager mbom, 
												BusinessObjectManager bom) {
		
		if (checkPreConditions(userSessionData, mbom)==null) {  
			User user = bom.getUser();	
			return user.IsCallCenterAgent();
		}
		
		return false;
	}


	public ActionForward isaPerform(ActionMapping mapping,
									 ActionForm form,
									 HttpServletRequest request,
									 HttpServletResponse response,
									 UserSessionData userSessionData,
									 RequestParser requestParser,
									 BusinessObjectManager bom,
									 IsaLocation log,
									 IsaCoreInitAction.StartupParameter startupParameter,
									 BusinessEventHandler eventHandler)
			throws CommunicationException {

		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
		// Page that should be displayed next.
		String forwardTo = null;
		HttpSession session = request.getSession();
		ServletContext context = getServlet().getServletContext();
		
		LoginStatus i = LoginStatus.NOT_OK;
		String welcomeText = "";


		MetaBusinessObjectManager mbom = userSessionData.getMBOM();
		checkPreConditions(userSessionData, mbom);

		if (CheckCCAgentAuthorization(userSessionData, mbom, bom) == true) {
			String confirmKey = WebUtil.translate(context, session, "b2c.callcentermode.confirm", null);
			String cancelKey = WebUtil.translate(context, session, "b2c.callcentermode.cancel", null);
			
			String confirm = request.getParameter(confirmKey);
			 
			if (confirm != null && confirm.length()>0) {
				BusinessPartnerManager buPaMan = bom.getBUPAManager();
				String bPartnerID = request.getParameter("BPID");
				BusinessPartner bPartner = buPaMan.getBusinessPartner(bPartnerID);
				User user = bom.getUser();
				user.setBusinessPartner(bPartner.getTechKey());
                user.resetMarketingSettingsCCM();
				
				setBusinessPartner(userSessionData, bPartner.getTechKey(), buPaMan);

				Address address = bPartner.getAddress();
			
				if (address != null && !address.getLastName().equals("")) {
					String title = address.getTitle();
					String lastName = address.getName();
					welcomeText = title + " " + lastName;
				}


				// set session attributes for the welcome-text (navigationbar.jsp)
				request.getSession().setAttribute(B2cConstants.WELCOME_TEXT, welcomeText);

				// set user session data
				userSessionData.setAttribute(B2cConstants.FRAME_BEFORE_LOGIN,(String) userSessionData.getAttribute(B2cConstants.FRAME_BEFORE_LOGIN));
				userSessionData.setAttribute(B2cConstants.FRAME_NAME, LoginAction.LOGIN);
				userSessionData.setAttribute(B2cConstants.LOGIN_STATUS, LoginAction.LOGIN_SUCCESS);

				fireLoginEvent(eventHandler, buPaMan, user);

				boolean auctionRelated = false;
				String aucTransId =  startupParameter.getForward();
				if (aucTransId != null){
					if (aucTransId.equalsIgnoreCase("auctionChkout")){
						auctionRelated = true;
					}
				}
				handleBasket(userSessionData, bom, user, auctionRelated);
/*
				if (loginForm.getUserId().equals(SPECIAL_NAME_AS_USERID)) {
					forwardTo = "ssoSuccess";
				}
				else if ( userSessionData.getAttribute(B2cConstants.EASY_B2B).toString().equals("true") ) {
					forwardTo = "successEasyB2B";
				} 
				else if(isDarklogin) {
					forwardTo = "darklogin";
				}
				else 
*/
				{
					forwardTo = "success";
				}
			}
			else {
				forwardTo="error";
			} 
			log.exiting();
		}
		else {
			// no message required
			// TODO log unauthorized access
			forwardTo="logoff";
		}
		return mapping.findForward(forwardTo);
	}

	/**
	 * Fires the login event see {@link #LoginEvent}. <br>
	 * 
	 * @param eventHandler business event handler
	 * @param buPaMa business partner manager 
	 * @param user current user
	 */
	protected void fireLoginEvent(BusinessEventHandler eventHandler,
									BusinessPartnerManager buPaMa,
									User user) {
		// fire a login event
		LoginEvent event = new LoginEvent(user);
		eventHandler.fireLoginEvent(event);
	}

	/**
	 * Set the business partner in the business object manager depending of the
	 * scenario. <br>
	 * 
	 * @param userSessionData user session data
	 * @param businessPartnerKey key of the business partner 
	 * @param buPaMa
	 * @return
	 */
	static protected BusinessPartner setBusinessPartner(UserSessionData userSessionData,
												  TechKey businessPartnerKey,
												  BusinessPartnerManager buPaMa) {
		BusinessPartner partner = buPaMa.createBusinessPartner(businessPartnerKey,null, null );

		if ( userSessionData.getAttribute(B2cConstants.EASY_B2B).toString().equals("true") ) {
			// easyB2B
        
			Contact contact = new Contact();
			partner.addPartnerFunction(contact);
			buPaMa.setDefaultBusinessPartner(partner.getTechKey(),contact.getName());
            
		} else {
        
			SoldTo soldTo = new SoldTo();
			partner.addPartnerFunction(soldTo);
			buPaMa.setDefaultBusinessPartner(partner.getTechKey(),soldTo.getName());
        
		}
        
		return partner;
	}

	/**
	 * Handles the basket recovery. <br>
	 * 
	 * @param userSessionData user session data
	 * @param bom business object manager
	 * @param user current user
	 * @throws CommunicationException
	 */
	protected void handleBasket(UserSessionData userSessionData,
								 BusinessObjectManager bom,
								 User user,
								 boolean auctionRelated)
			throws CommunicationException {

		// check if there is a basket for the user, that must be recovered
		if (bom.getBasket() == null) {
            
			// get business objects
			Shop shop = bom.getShop();
            
			// check for dark login                
			if(shop != null) {
        		                
				WebCatInfo catalog =
					getCatalogBusinessObjectManager(userSessionData).getCatalog();
        
				// create an read the basket
				Basket basket = bom.createBasket();
				basket.setAuctionBasket(auctionRelated);
				// to know if the basket supports recovery, we must initialize it
				basket.init(shop, bom.createBUPAManager(), null, catalog);
        
				if (basket.isDocumentRecoverable()) {
					// a new empty entry in the database was created, if the basket is recoverable
					// that entry must be deleted
					basket.destroyContent();
                    
                    if (shop.getAuctionBasketHelper() != null) {
                        shop.getAuctionBasketHelper().setInvocationContext(userSessionData);
                    }
        
					if (!basket.recoverUsingShopAndUser(shop, user.getTechKey(), catalog)) {
						// there is no document to be recovered, delete the basket again
						bom.releaseBasket();
					}
                    
                    if (shop.getAuctionBasketHelper() != null) {
                        shop.getAuctionBasketHelper().releaseInvocationContext();
                    }
				} 
                else {
					bom.releaseBasket();
				}
			}    
		}
		else { 
        	
			if ( !(userSessionData.getAttribute(B2cConstants.EASY_B2B).toString().equals("true")) ) {
            	
				Basket basket = bom.getBasket();
				basket.setAuctionBasket(auctionRelated);
				PartnerList pList = basket.getHeader().getPartnerList();
        
				BusinessPartner soldTo = bom.createBUPAManager().getDefaultBusinessPartner(PartnerFunctionData.SOLDTO);
                
				if (soldTo != null) {
					pList.setSoldTo(new PartnerListEntry(soldTo.getTechKey(),soldTo.getId()));
					basket.updateHeader(bom.createBUPAManager());
				}
        
			} 
		}
	}//end of isaPerform method

}

