/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Biju Raj
  Created:      3rd April 2001

  $Revision: #2 $
  $Date: 2001/06/29 $
*****************************************************************************/

package com.sap.isa.isacore.action.b2c;

// framework dependencies
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 * Defines that the <i>Contact</i> JSP should be displayed next by setting
 * an appropriate <code>UserSessionData</code> attribute.
 */
public class ContactForwardAction extends IsaCoreBaseAction {

    /**
     * Overriden <em>isaPerform</em> method of <code>IsaCoreBaseAction</code>.
     */
    public ActionForward isaPerform(ActionMapping mapping,
                                    ActionForm form,
                                    HttpServletRequest request,
                                    HttpServletResponse response,
                                    UserSessionData userSessionData,
                                    RequestParser requestParser,
                                    BusinessObjectManager bom,
                                    IsaLocation log) {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        // The frame name attribute will be evaluated during the processing
        // of the catalogFS JSP.
        userSessionData.setAttribute(B2cConstants.FRAME_NAME,"CONTACT");
        log.exiting();
        return mapping.findForward("success");
    }
}
