/*****************************************************************************
  Copyright (c) 2000, SAP AG, Germany, All rights reserved.
  Author:       SAP AG
  Created:      22nd April 2001
*****************************************************************************/

package com.sap.isa.isacore.action.b2c;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.businessobject.order.DocumentListFilter;
import com.sap.isa.businessobject.order.OrderDataContainer;
import com.sap.isa.businessobject.order.OrderStatus;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.businesspartner.businessobject.PartnerFunctionBase;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 * Prepares the displaying of the open orders.
 * @deprecated This class will be removed with CRM 6.0 and be replaced with the generic
 *             search framework (10.12.2004).
*/
 public class ShowOpenOrdersAction extends IsaCoreBaseAction {

    /**
     * Overriden <em>isaPerform</em> method of <code>IsaCoreBaseAction</code>.
     */
     public ActionForward isaPerform(ActionMapping mapping,
                ActionForm form,
                HttpServletRequest request,
                HttpServletResponse response,
                UserSessionData userSessionData,
                RequestParser requestParser,
                BusinessObjectManager bom,
                IsaLocation log)
                        throws CommunicationException {


       // Page that should be displayed next.
	   final String METHOD_NAME = "isaPerform()";
	   log.entering(METHOD_NAME);
       String forwardTo = null;
       // all parameters required to get all open orders set in the doument filter
       // and passed to the backend using the orderstatus
       DocumentListFilter documentListFilter = new DocumentListFilter();
       documentListFilter.setTypeORDER();
       documentListFilter.setStatusOPEN();
       documentListFilter.setChangedDate(B2cConstants.RK_DATE_FROM);
       request.setAttribute(B2cConstants.RK_FILTER,documentListFilter);

       // read the shop data
       Shop shop = bom.getShop();

       // Make Shop available for JSP (to check for Contract and quotation allowance)
       request.setAttribute(B2cConstants.RK_DLA_SHOP, shop);

       // Ask the business object manager for a user. If this object is not
        // already present, a new one will be created.
       User user = bom.getUser();
	   if (user == null  ||  ! user.isUserLogged()) {
	   		log.exiting();
		   return mapping.findForward("login");
	   }

       OrderStatus orderList = bom.getOrderStatus();
       if (orderList == null) {
           orderList = bom.createOrderStatus(new OrderDataContainer());
       }
       BusinessPartnerManager buPaMa = bom.createBUPAManager(); 
       TechKey soldToKey = (buPaMa.getDefaultBusinessPartner(PartnerFunctionBase.SOLDTO) != null 
                            ? buPaMa.getDefaultBusinessPartner(PartnerFunctionBase.SOLDTO).getTechKey()
                            : new TechKey(""));

       orderList.readOrderHeaders(soldToKey, shop, documentListFilter);

       request.setAttribute(B2cConstants.RK_DOCUMENT_COUNT, new Integer(orderList.getOrderCount()).toString());
       request.setAttribute(B2cConstants.RK_ORDER_LIST, orderList);
       request.setAttribute(B2cConstants.LINK_SELECTED,"OPEN_ORDERS");
       forwardTo = "orderlist";
		log.exiting();
       return mapping.findForward(forwardTo);
    }//end of isaPerform method

}// end of ShowOpenOrdersAction class

