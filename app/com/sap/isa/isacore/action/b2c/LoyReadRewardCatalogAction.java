/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       SAP
  Created:      3rd April 2001

  $Revision: #2 $
  $Date: 2001/07/23 $
*****************************************************************************/

package com.sap.isa.isacore.action.b2c;

// framework dependencies
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.User;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;


/**
 * Checks whether the user is already logged into the system. <br> 
 * If yes, the <i>success</i> will be returned. <br>
 * If no, the logical forward <i>failure</i> will be processed. 
 */
public class LoyReadRewardCatalogAction extends IsaCoreBaseAction {
	
	/**
	 * Initialize the action.
	 * In B2C this action could be used by anonymous user.
	 * 
	 * @see com.sap.isa.isacore.action.IsaCoreBaseAction#initialize()
	 */
	public void initialize() {
		if (isB2C()) {
			this.checkUserIsLoggedIn = false;
		}	
	}


    /**
     * Overriden <em>isaPerform</em> method of <code>IsaCoreBaseAction</code>.
     */
    public ActionForward isaPerform(ActionMapping mapping,
                                    ActionForm form,
                                    HttpServletRequest request,
                                    HttpServletResponse response,
                                    UserSessionData userSessionData,
                                    RequestParser requestParser,
                                    BusinessObjectManager bom,
                                    IsaLocation log)
                                    throws CommunicationException {

		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        // Page that should be displayed next.
        String forwardTo = null;

        // get the instance of the user business object
        User user = bom.getUser();

        // Check if there exist an instance of user business object and 
        // if so is the user logged into the system
        // The method user.isUserLogged returns true if the user is logged
        // into the system.
        if ((user != null) && (user.isUserLogged())){
            forwardTo = "success";
        }
        else {
            // forward to login.jsp
            forwardTo = "login";
        }
		log.exiting();
        return mapping.findForward(forwardTo);
    }
}

