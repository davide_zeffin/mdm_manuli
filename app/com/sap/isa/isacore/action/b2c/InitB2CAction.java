/*****************************************************************************
    Class         InitB2CAction
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  B2C specific initializations
    Author:
    Created:      23.04.2001
    Version:      1.0

    $Revision: #4 $
    $Date: 2002/03/21 $
*****************************************************************************/

package com.sap.isa.isacore.action.b2c;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.Address;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.UnloggedUserInfo;
import com.sap.isa.businessobject.User;
import com.sap.isa.businessobject.marketing.MktPartner;
import com.sap.isa.businessobject.order.Basket;
import com.sap.isa.businessobject.order.Leaflet;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.Constants;
import com.sap.isa.core.RequestProcessor;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.ui.context.GlobalContextManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.StartupParameter;
import com.sap.isa.isacore.RecoverBasketCookieHandler;
import com.sap.isa.isacore.action.CheckOciReceiveAction;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.action.IsaCoreInitAction;
import com.sap.isa.user.util.UserUnSecureConnectionEventHandler;


/**
 * Performs all initializations specific to the B2C scenario.
 */
public class InitB2CAction extends IsaCoreBaseAction {
		
	/**
	 * This action could be used by anonymous user!
	 * 
	 * @see com.sap.isa.isacore.action.IsaCoreBaseAction#initialize()
	 */
	public void initialize() {
		this.checkUserIsLoggedIn = false;

		RequestProcessor.getSecureConnectionChecker().registerEventHandler(new UserUnSecureConnectionEventHandler());
	}

	/**
	 * Initialize Context values. <br>
	 */
	static public void initContextValues() {
		GlobalContextManager.registerValue(Constants.CV_DOCUMENT_KEY, true, false);
		GlobalContextManager.registerValue(Constants.CV_DOCUMENT_TYPE, true, false);
        GlobalContextManager.registerValue(Constants.CV_BASKET_ANCHOR_KEY, false, false);
        GlobalContextManager.registerValue(Constants.CV_ANCHOR_IS_CFG, false, false);
        GlobalContextManager.registerValue(Constants.CV_1STSCCALL, false, false);
	}	

    /**
     * Performs all initializations specific to the B2C scenario.
     *
     */
    public ActionForward isaPerform(ActionMapping mapping,
                ActionForm form,
                HttpServletRequest request,
                HttpServletResponse response,
                UserSessionData userSessionData,
                RequestParser requestParser,
                BusinessObjectManager bom,
                IsaLocation log) throws CommunicationException {

        // next page to be displayed
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        String forwardTo = null;

        User user = bom.getUser();
        if (user == null) {
            // create an instance of the user
            user = bom.createUser();
        }


		BusinessPartnerManager buPaMa = bom.createBUPAManager();
		BusinessPartner soldTo = buPaMa.getDefaultBusinessPartner(PartnerFunctionData.SOLDTO);
		BusinessPartner contact = buPaMa.getDefaultBusinessPartner(PartnerFunctionData.CONTACT);

		Address address = null;
		String welcomeText= null;

        // Determine whether the user is a person or represents an organisation.
        // The user represents a person if the lastname of the soldTo is
        // non-empty, otherwise it's an organization
        if (contact != null &&	contact.getAddress() != null &&
            !contact.getAddress().getName().equals("")) {
            address = contact.getAddress();	
        } else if (soldTo != null && soldTo.getAddress() != null &&
			!soldTo.getAddress().getName().equals("")) {
			address = soldTo.getAddress();	
        }
        
        if (address != null) {
			String title = address.getTitle();
			String lastName = address.getName();
			welcomeText = title + " " + lastName;
        }
        else if (user.getSalutationText()!=null && user.getSalutationText().length() > 0) {
			welcomeText = user.getSalutationText();
        }

		if (welcomeText != null) {
			// Note 1010868
			//request.getSession().setAttribute(B2cConstants.WELCOME_TEXT, user.getSalutationText());
			request.getSession().setAttribute(B2cConstants.WELCOME_TEXT, welcomeText);
		}

        if (userSessionData.getAttribute (B2cConstants.LOGIN_STATUS) == null ) {
            userSessionData.setAttribute (B2cConstants.LOGIN_STATUS ,"BEFORE_LOGIN");
        }


        if (!user.isUserLogged()) {
            // Set the markting user as unknown user  
            user.getMktPartner().setKnown(false);
        }    

        // get business objects
        Shop shop = bom.getShop();
        WebCatInfo catalog =
                getCatalogBusinessObjectManager(userSessionData).getCatalog();

        // create an instance of the leaflet
        Leaflet leaflet = bom.createLeaflet();
        leaflet.init(shop, catalog);
        
        shop.setShowCatAddoLeaflet(true);
        shop.setShowCatPersRecommend(true);
		shop.setShowCatSpecialOffers(true);
        shop.setCatUseReloadOneTime(true);
        shop.setSuppCatEventCapturing(true);
        
        // no recovery in case of oci
        StartupParameter startupParameter = (StartupParameter)userSessionData.getAttribute(SessionConst.STARTUP_PARAMETER);
        boolean ocireceive = false;
        // check if a ocireceive forward is given as an startupParameter
        if (startupParameter != null) {
             String forward = startupParameter.getParameterValue(IsaCoreInitAction.PARAM_FORWARD);
             if (forward.length() > 0 && forward.equals(CheckOciReceiveAction.OCI_RECEIVE)) {
                 ocireceive = true;
             }
        }

        RecoverBasketCookieHandler recBasketCookieHdlr = new RecoverBasketCookieHandler(request, response);

        // initialize leaflet with cookie data, create unlogged user info and try
        // to find RecoverBasketCookie
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (int i=0; i< cookies.length; i++) {
                String cookieName = cookies[i].getName();

                if (cookieName.equals(leaflet.getCookieName())) {
                    leaflet.fill(cookies[i].getValue());
                }
                else if (cookieName.equals(CreateOrUpdateUserCookieAction.COOKIE_NAME)) {
                    if (!user.isUserLogged()){    
                        createUnloggedUserInfo(user, cookies[i], userSessionData);                   
                    }
                } 
                else if (cookieName.equals(UnknownUserCookieAction.COOKIE_NAME)) {
                    if (!user.isUserLogged()){    
                        createUnknownUserInfo(user,cookies[i]);                   
                    }
                } 
                // Search for RecoverBasketCookie
                else if (cookieName.equals(recBasketCookieHdlr.buildCookieName(shop))) {
                    recBasketCookieHdlr.fill(cookies[i]);
                }
            }
        }

        // try to recover basket, if a valid RecoverBasketCookie was found
        if (recBasketCookieHdlr.getBasketGUID() != null && bom.getBasket() == null && !ocireceive) {

            boolean debugEnabled = log.isDebugEnabled();

            if (debugEnabled) {
                log.debug("Try to recover basket Techkey:" + recBasketCookieHdlr.getBasketGUID());
            }
            // create an read the basket
            Basket basket = bom.createBasket();            
            if (basket.checkRecovery()) {
                if(!basket.recoverUsingTechKey(shop, user.getTechKey(), catalog, recBasketCookieHdlr.getBasketGUID())) {
                    // document couldn't be recovered, delete Basket again
                    bom.releaseBasket();
                    // delete the cookie, so we wont try to read the document again next time
                    recBasketCookieHdlr.delete(shop);
                    if (debugEnabled) {
                        log.debug("Recovery of basket Techkey:" + recBasketCookieHdlr.getBasketGUID() + " failed");
                    }
                } 
                else {
                   userSessionData.setAttribute(RecoverBasketCookieHandler.SC_RECO_BASKET_COOKIE_HDLR, recBasketCookieHdlr);
                   if (debugEnabled) {
                         log.debug("Recovery of basket Techkey:" + recBasketCookieHdlr.getBasketGUID() + " succeeded");
                   }
                }
            }
            else {
            	bom.releaseBasket();
            }
        }
         
       
		// check, if save and load basket function is supported by the backend
		if (shop.isSaveBasketAllowed()) {
			Basket basket = bom.getBasket();
			boolean releaseBasket = false;
			if (basket == null) {
				basket = bom.createBasket();
				releaseBasket = true;
			}

			if (!basket.checkSaveBaskets()) {
			   shop.setSaveBasketAllowed(false);
			   shop.setTemplateAllowed(true);
			   log.error("Save and load of baskets is not supported by basket backend. Use of templates enabled instead.");
			}
			
			if (releaseBasket) {
				bom.releaseBasket();
			}
			
		}

        // set the frame combination in the mainFS frame set.
        userSessionData.setAttribute ( B2cConstants.FRAME_NAME ,"INITIAL");
        
        // forward to the navigation page
        forwardTo = "main";
		log.exiting();
        return mapping.findForward(forwardTo);
    }


    private void createUnloggedUserInfo(User user, Cookie userCookie, UserSessionData userSessionData) {

        // get the cookie content
        String cookieContent = CreateCookieBaseAction.getCookieContent(userCookie);

        // error reading cookie?
        if (cookieContent == null) {
            return;
        }

        // create the sold to technical key from the cookie contents
        TechKey soldToTechKey = new TechKey(cookieContent);
        // create an unlogged user info object and put it into the user session data
        UnloggedUserInfo unloggedUserInfo = new UnloggedUserInfo(soldToTechKey);
        userSessionData.setAttribute(B2cConstants.UNLOGGED_USER_INFO, unloggedUserInfo);

        MktPartner mktPartner = user.getMktPartner();        
        mktPartner.setTechKey(soldToTechKey);
        mktPartner.setKnown(true);
    }


    private void createUnknownUserInfo(User user, Cookie userCookie) {

        // get the cookie content
        String cookieContent = CreateCookieBaseAction.getCookieContent(userCookie);

        // error reading cookie?
        if (cookieContent == null) {
            return;
        }

        MktPartner mktPartner = user.getMktPartner();
        
        if (mktPartner.getTechKey().isInitial()) {      
            // set unknown user info, only if the marketing partner is really unknown
            mktPartner.setTechKey(new TechKey(cookieContent));
            mktPartner.setKnown(false);
        }    
    }


}
