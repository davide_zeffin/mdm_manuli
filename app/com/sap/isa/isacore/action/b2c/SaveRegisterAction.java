/*****************************************************************************
  Copyright (c) 2000, SAP AG, All rights reserved.
  Author:       SAP AG
  Created:      21 March 2001

  $Revision: #10 $
  $Date: 2004/02/10 $
****************************************************************************/

package com.sap.isa.isacore.action.b2c;

// framework dependencies
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.ecommerce.businessobject.MarketingBusinessObjectManager;
import com.sap.ecommerce.businessobject.campaign.CampaignHeader;
import com.sap.ecommerce.businessobject.loyalty.LoyaltyCampaign;
import com.sap.ecommerce.businessobject.loyalty.LoyaltyMembership;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businessobject.businessevent.RegisterEvent;
import com.sap.isa.businessobject.marketing.MktAttributeSet;
import com.sap.isa.businessobject.marketing.MktPartner;
import com.sap.isa.businessobject.marketing.MktProfile;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.businesspartner.businessobject.Identification;
import com.sap.isa.businesspartner.businessobject.SoldTo;
import com.sap.isa.core.FrameworkConfigManager;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.isacore.AddressFormular;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.action.IsaCoreInitAction;
import com.sap.isa.isacore.action.loyalty.LoyaltyUtilities;
import com.sap.isa.user.util.RegisterStatus;


/**
 * Registers a user into the backend system. This is done by using the appropiate
 * business object. There could some things be happened before the tries to save
 * his register data. This action class handles those cases appropriate: Change
 * of title, change of country, selection of counties.
 */
public class SaveRegisterAction extends IsaCoreBaseAction {
	
	/**
	 * Parameter name for password.
	 */
	public static final String PN_PASSWORD = "nolog_password";

	/**
	 * Parameter name for password verification.
	 */
	public static final String PN_PASSWORD_VERIFY = "nolog_passwordVerify";

    /**
     * Parameter name for userId. It will be needed if login via SU01 user ID or SU01 user Alias is customized.
     */
    public static final String PN_USERID = "userId";
	
    
    protected class ForwardStorage {
    	
    	private String forward = null;
    	
    	ForwardStorage(String forward) {
    	   this.forward = forward;	
    	}
    	
    	void setForward(String forward) {
    	  this.forward = forward;
    	}
    
    	String getForward() {
    		return this.forward;
    	}
    }
    
    
	/**
	* Initialize the action.
	* In B2C this action could be used by anonymous user.
	*/
	public void initialize() {
		if (isB2C()) {
			this.checkUserIsLoggedIn = false;
		}	
	}

    /**
     * Overriden <em>isaPerform</em> method of <code>IsaCoreBaseAction</code>.
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            BusinessObjectManager bom,
            IsaLocation log,
            IsaCoreInitAction.StartupParameter startupParameter,
            BusinessEventHandler eventHandler,
            boolean multipleInvocation,
            boolean browserBack)
            throws CommunicationException {

        // Page that should be displayed next.
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
         
     
        User user = bom.createUser();
        
        if (!user.isUserLogged()) {
        	log.exiting();
        	return doRegistration(mapping,
                                  form,
                                  request,
                                  response,
                                  userSessionData,
                                  requestParser,
                                  bom,
                                  log,
                                  startupParameter,
                                  eventHandler,
                                  multipleInvocation,
                                  browserBack);	
        } else { 
        	log.exiting();
        	return doLoyaltyRegistration(mapping,
                                         form,
                                         request,
                                         response,
                                         userSessionData,
                                         requestParser,
                                         bom,
                                         log,
                                         startupParameter,
                                         eventHandler,
                                         multipleInvocation,
                                         browserBack);	
        }
        
        
        
		
         
    }//end of isaPerform method

    
    
	/**
	 * Sets data into the request object which will be needed to show the register.inc.jsp again.<br />
	 * This method will be needed if for instance while processing of the data an error occured and 
	 * the flow should returns back to the register.inc.jsp.
	 * 
	 * @param request HttpServletRequest
	 * @param shop Shop data
	 * @param addressFormular Address formular with entered adrres data
	 * @param password Entered password
	 * @param passwordVerify Entered password verification
	 * @throws CommunicationException
	 */
	protected void setDisplayDetailData(HttpServletRequest request, 
										Shop shop, 
										AddressFormular addressFormular, 
										String password, 
										String passwordVerify) 
			throws CommunicationException {
        
		ResultData formsOfAddress = shop.getTitleList();
		ResultData listOfCountries = shop.getCountryList();
		if (addressFormular.isRegionRequired()){
			ResultData listOfRegions = shop.getRegionList(addressFormular.getAddress().getCountry());
			if (listOfRegions != null) {
				request.setAttribute(B2cConstants.POSSIBLE_REGIONS, listOfRegions);
			}
		}

		request.setAttribute(B2cConstants.FORMS_OF_ADDRESS,formsOfAddress);
		request.setAttribute(B2cConstants.POSSIBLE_COUNTRIES,listOfCountries);
		request.setAttribute(B2cConstants.ADDRESS_FORMULAR, addressFormular);
		request.setAttribute(PN_PASSWORD, password);
		request.setAttribute(PN_PASSWORD_VERIFY, passwordVerify);

        // will only be available if login via SU01 user ID or SU01 user Alias is enabled
        request.setAttribute(PN_USERID, request.getParameter(PN_USERID));
	}
    
    private Identification checkVWord(HttpServletRequest request, Shop shop, User user  ) {
        Identification vWord = null;
        boolean showVerification = false;
        
        try {
            // get the Verification Word (if there's any)
            showVerification = (boolean)  (
            (String)FrameworkConfigManager.Servlet.getInteractionConfigParameter(
                request.getSession(),
                "ui",
                "ShowVerificationWord",
                ""
                )
            ).equals("true");
        }
        catch (Exception ex) {
            if (log.isDebugEnabled()) {
                log.debug("Verification word not found in XCM: " + ex.getMessage());
            }
        }
                    
        if (showVerification && shop.getIdTypeForVerification().length() > 0 ) {
          user.getMessageList().remove(new String[] {"b2c.verificationword.missing", "b2c.verificationword.tolong" });
          vWord = new Identification();
          // Identification vWord = (Identification) request.getParameter(B2cConstants.PARTNER_IDENTIFICATION);
          vWord.setType(shop.getIdTypeForVerification());
          vWord.setNumber(request.getParameter(B2cConstants.VERIFICATION_WORD));
                      
          if (vWord.getNumber()== null || vWord.getNumber().length() == 0) {
            // missing verification word
            if (log.isDebugEnabled()) {
                log.debug("Verification word missing.");
            }
            user.addMessage(new Message(Message.ERROR, "b2c.verificationword.missing", null, B2cConstants.VERIFICATION_WORD));
            request.setAttribute(B2cConstants.MESSAGE_LIST, user.getMessageList());
          }
          else if (vWord.getNumber().length() > 60){
              // vWord.setNumber(vWord.getNumber().substring(0, 60));
              user.addMessage(new Message(Message.ERROR, "b2c.verificationword.tolong", null, B2cConstants.VERIFICATION_WORD));
              request.setAttribute(B2cConstants.MESSAGE_LIST, user.getMessageList());
          }
          
        }
        return vWord;
    }
    
    
    
    
//    protected RegisterStatus  register(HttpServletRequest request, 
//                                       UserSessionData userSessionData,
//                                       BusinessObjectManager bom,
//                                       RequestParser parser,
//                                       User user,
//                                       Shop shop,
//                                       AddressFormular addressFormular, 
//                                       String password,
//                                       String passwordVerify,
//                                       ForwardStorage actionForward) 
//              throws CommunicationException {
//    	
//      RegisterStatus status = RegisterStatus.NOT_OK;
//      boolean doNormalRegistration = true;	
//      String forwardTo = "success";
//      
//  	  if (shop.getLoyaltyProgramID() != null && shop.getLoyaltyProgramID().length() > 0) {
//  		
//      	 RequestParser.Parameter joinProgramFlag   = LoyaltyUtilities.handleRequestAttributes(parser, userSessionData, "joinloyaltyprogramflag");
//    	 RequestParser.Parameter acceptProgramFlag = LoyaltyUtilities.handleRequestAttributes(parser, userSessionData, "acceptprogramtermsflag");
//     	    	 
//  		 LoyaltyMembership loyMembership = LoyaltyUtilities.createLoyaltyMembership(parser, userSessionData, bom);  
//  		
//         // both flags are set - the user wants to join
//  		 if (joinProgramFlag.getValue().isSet() && acceptProgramFlag.getValue().isSet()) {
//           // register and join the loyalty program
//  		   status = user.registerAndJoinLoyaltyProgram(shop.getId(), loyMembership, addressFormular.getAddress(), password, passwordVerify);
//     	   // get the point account informtion
//  		   if (loyMembership.exists()){
//      		 loyMembership.getLoyaltyPointAccountFromBackend(shop.getPointCode(), shop.getPointCodeDescription());   
//      	   }
//  		   forwardTo = "loyaltyInfoAndPostSteps";
//  		   doNormalRegistration = false;
//  		 
//  		 // the joining flag is set but the user has forgoten to set the acceptance flag
//  		 } else if (joinProgramFlag.getValue().isSet() && !acceptProgramFlag.getValue().isSet()) {
//  			// he must accept the terms and conditions => error message 
//    		loyMembership.addMessage(new Message(Message.ERROR, "b2c.loyalty.program.noacception", null, "loyaltyRegistration")); 
//    		status = RegisterStatus.NOT_OK;
//    	    doNormalRegistration = false;
//    	    forwardTo = "failure";
//    	    
//     	 // the joining flag is not set but the user has set the acceptance flag
//  		 } else if (!joinProgramFlag.getValue().isSet() && acceptProgramFlag.getValue().isSet()) {
//  			// he must set the join flag as well => error message 
//    		loyMembership.addMessage(new Message(Message.ERROR, "b2c.loyalty.program.nojoinflag", null, "loyaltyRegistration")); 
//    		status = RegisterStatus.NOT_OK;
//    	    doNormalRegistration = false;
//    	    forwardTo = "failure"; 
//    	    
//    	 // nothing was selected so only do the normal registration   
//    	 } else {
//  	       doNormalRegistration = true;		
//  		 } 
//  		 
//  	    request.setAttribute("loyaltyMessages", loyMembership.getMessageList());   
//
//  	  } 	
//  	
//  	   if (doNormalRegistration) {
//    	status = user.register(shop.getId(), addressFormular.getAddress(), password, passwordVerify); 	
//  	   }
//  	   
//  	   actionForward.setForward(forwardTo);
//  	   return status;
//    }
      
    
    
    
    
    protected ActionForward doRegistration(ActionMapping mapping,
                                        ActionForm form,
                                        HttpServletRequest request,
                                        HttpServletResponse response,
                                        UserSessionData userSessionData,
                                        RequestParser requestParser,
                                        BusinessObjectManager bom,
                                        IsaLocation log,
                                        IsaCoreInitAction.StartupParameter startupParameter,
                                        BusinessEventHandler eventHandler,
                                        boolean multipleInvocation,
                                        boolean browserBack)
            throws CommunicationException {

        // Page that should be displayed next.
		final String METHOD_NAME = "doRegistration()";
		log.entering(METHOD_NAME);
        String forwardTo = null;
        ForwardStorage successForward = new  ForwardStorage("success");

        // list of countries
        ResultData listOfCountries;
        // list of regions
        ResultData listOfRegions = null;
        // list of forms of address
        ResultData formsOfAddress;

        // ask the business object manager for the shop
        Shop shop = bom.getShop();

        // create a new address formular
        AddressFormular addressFormular = new AddressFormular(requestParser);
        // address format id
        int addressFormatId = shop.getAddressFormat();
        // the next line is nessassary in case of a reconstruction of the register JSP
        addressFormular.setAddressFormatId(addressFormatId);

        // get the user's password and repeated password input
        String password = requestParser.getParameter(PN_PASSWORD).getValue().getString();
        String passwordVerify = requestParser.getParameter(PN_PASSWORD_VERIFY).getValue().getString();

        // check for possible cases
        // case A: if the user changed the title type. Possible title types are
        // "FOR_PERSON" and "FOR_ORGANISATION"
        if (request.getParameter("titleChange").length() > 0) {
            if (addressFormular.isPerson()) {
                 addressFormular.setPerson(false);
            }
            else{
                addressFormular.setPerson(true);
            }

            // build up the detail data needed for the register-JSP
            setDisplayDetailData(request, shop, addressFormular, password, passwordVerify);

            forwardTo = "titleChange";
            
            }
        // case B: if the user changes the default country
        else if(request.getParameter("countryChange").length() > 0) {

            // build up the detail data needed for the register-JSP
            setDisplayDetailData(request, shop, addressFormular, password, passwordVerify);

            forwardTo = "countryChange";
            }
        // case C: if the user changes the default country
        else if( request.getParameter("countiesRefresh") != null && request.getParameter("countiesRefresh").length() > 0) {

            log.debug("Refresh counties");

            // build up the detail data needed for the register-JSP
            setDisplayDetailData(request, shop, addressFormular, password, passwordVerify);                    
                    
            // read list of counties
            ResultData listOfCounties = shop.getTaxCodeList(addressFormular.getAddress().getCountry(),
                                                            addressFormular.getAddress().getRegion(),
                                                            addressFormular.getAddress().getPostlCod1(),
                                                            addressFormular.getAddress().getCity());

            request.setAttribute(B2cConstants.POSSIBLE_COUNTIES, listOfCounties);

            forwardTo = "countiesRefresh";
            }
        // case D: if the user decided to cancel
        else if(request.getParameter("cancelClicked").length() > 0) {

            LoyaltyMembership loyMembership = LoyaltyUtilities.getLoyaltyMembership(userSessionData);
            if (loyMembership != null) {
                loyMembership.clearMessages();
            }
            userSessionData.removeAttribute("joinloyaltyprogramflag");
            userSessionData.removeAttribute("acceptprogramtermsflag");
            if (userSessionData.getAttribute("loyaltyPromotionCode") != null) {
                userSessionData.removeAttribute("loyaltyPromotionCode");}
            forwardTo = "cancel";
        } 
        // case E: If the user decided to save the registration details
        else {
            // Ask the business object manager for a user. If this object is not
            // already present, a new one will be created.
            User user = bom.createUser();
            user.clearMessages();
            
            // in case of login via SU01 user ID or SU01 user Alias, an user Id value should be in request
            user.setUserId(requestParser.getParameter(PN_USERID).getValue().getString());
            
            Identification vWord = checkVWord(request, shop, user);

            if (vWord != null && (vWord.getNumber().length() == 0 || vWord.getNumber().length() > 60)) { 
                formsOfAddress = shop.getTitleList();
                listOfCountries = shop.getCountryList();
                request.setAttribute(B2cConstants.FORMS_OF_ADDRESS,formsOfAddress);
                request.setAttribute(B2cConstants.POSSIBLE_COUNTRIES,listOfCountries);
                if (listOfRegions != null) {
                    request.setAttribute(B2cConstants.POSSIBLE_REGIONS, listOfRegions);
                }
                request.setAttribute(B2cConstants.ADDRESS_FORMULAR, addressFormular);
                request.setAttribute(PN_PASSWORD, password);
                request.setAttribute(PN_PASSWORD_VERIFY, passwordVerify);
                request.setAttribute(B2cConstants.MESSAGE_LIST, user.getMessageList());
                request.setAttribute(B2cConstants.VERIFICATION_WORD, request.getParameter(B2cConstants.VERIFICATION_WORD));

                userSessionData.setAttribute(B2cConstants.FRAME_NAME,"REGISTER");
                forwardTo = "failure";
                return mapping.findForward(forwardTo);
            }

            
            // copy the marketing from a existing unknown user
            MktPartner mktPartner = user.getMktPartner();
            MktProfile tmpProfile = null;
            
            if (!mktPartner.isKnown() && !mktPartner.getTechKey().isInitial()) {
                MktAttributeSet mktAttributeSet = bom.createMktAttributeSet();
                
                // read first the attribute set
                mktAttributeSet.read(shop);               
                // copy the profile of the unknown user
                tmpProfile = user.readMktProfile(mktAttributeSet.getTechKey(),shop);
            }        

            // Note:
            // Either firstName/lastName or name1/nam2 must be set equal to ""
            // because of the kind of the JSP's construction. With other words,
            // the values of the input fields that disappear after a title change,
            // are not forgotten. They will be displayed again after another
            // title change.
            if (addressFormular.isPerson()) {
                addressFormular.getAddress().setName1("");
                addressFormular.getAddress().setName2("");
            }
            else {
                addressFormular.getAddress().setFirstName("");
                addressFormular.getAddress().setLastName("");
            }

            RegisterStatus registerStatus;

            if (multipleInvocation) {
                registerStatus = user.getRegisterStatus();
            }
            else {
//                registerStatus = user.register(shop.getId(),
//                        addressFormular.getAddress(),
//                        password,
//                        passwordVerify);
            	  registerStatus = register(request, 
                                            userSessionData,
                                            bom,
                                            requestParser,
                                            user,
                                            shop,
                                            addressFormular, 
                                            password,
                                            passwordVerify,
                                            successForward); 
            }

            switch (registerStatus.toInteger()) {
                case 0:
                    // registration was successful
                        log.debug("User is successfully registrated.");

                    // update welcome-text
                    String welcomeText="";
                    if (addressFormular.isPerson()) {
                        // read title value over formsOfAddress
						formsOfAddress = shop.getTitleList();
                        String titleKey = addressFormular.getAddress().getTitleKey();
                        String title = "";
                        formsOfAddress.first();
                        if(formsOfAddress.searchRowByColumn(ShopData.ID, titleKey, 1, -1)) {
							// after search the rowid will be set to the right row
							// so we can now access to the right row with the right title name value
							title = formsOfAddress.getString(ShopData.DESCRIPTION);
						}
                        // read name                
                        String lastName = addressFormular.getAddress().getName();
                        // Should the shop description also be part of the welcome text?
                        // String shopDescription = shop.getDescription();
                        // if (shopDescription != null) { ... }
                        welcomeText = title + " " + lastName;
                    }

                    // get the current session
                    HttpSession session = request.getSession();
                    // set session attributes for the welcome-text (navigationbar.jsp)
                    session.setAttribute(B2cConstants.WELCOME_TEXT, welcomeText);

                    // set user session data
                    userSessionData.setAttribute(B2cConstants.LOGIN_STATUS,"LOGIN_SUCCESS");
                    forwardTo = successForward.getForward();  //"success";

                    // set default businesspartner
                    BusinessPartnerManager buPaMa = bom.createBUPAManager();

                    BusinessPartner partner = buPaMa.createBusinessPartner(user.getBusinessPartner(),
                                                                           null,
                                                                           null );

                    SoldTo soldTo = new SoldTo();
                    partner.addPartnerFunction(soldTo);
                    buPaMa.setDefaultBusinessPartner(partner.getTechKey(),soldTo.getName());
                    

                    if (vWord != null) {
                          partner.addIdentification(vWord); 
                    }
                    

                    // fire a register event
                    RegisterEvent event = new RegisterEvent(user, addressFormular.getAddress());
                    eventHandler.fireRegisterEvent(event);

                    // copy the marketing from a existing unknown user
                        
                    if (tmpProfile != null) {
                        user.saveMktProfile(tmpProfile,shop);                    
                    }    
            
                                        
                    break;

                case 1:
                    // registration error
                        log.debug("User registration failed.");

                    // if there is an error message with property 'password' or
                    // 'passwordVerify' then the entered password(s) should
                    // not be redisplayed
                    if (user.getMessageList().contains(Message.ERROR, "password") ||
                        user.getMessageList().contains(Message.ERROR, "passwordVerify")) {

                      password = "";
                      passwordVerify = "";
                    }

                    // build up the detail data needed for the register-JSP
                    setDisplayDetailData(request, shop, addressFormular, password, passwordVerify);
                    
                    request.setAttribute(B2cConstants.MESSAGE_LIST, user.getMessageList());
                    request.setAttribute(B2cConstants.VERIFICATION_WORD, request.getParameter(B2cConstants.VERIFICATION_WORD));

                    forwardTo = "failure";
                    break;

                case 2:
                    // county required
                        log.debug("User registration failed, a county entry is required.");

                    // build up the detail data needed for the register-JSP
                    setDisplayDetailData(request, shop, addressFormular, password, passwordVerify);                    

                    // list of counties
                    ResultData listOfCounties = shop.getTaxCodeList(addressFormular.getAddress().getCountry(),
                                                                    addressFormular.getAddress().getRegion(),
                                                                    addressFormular.getAddress().getPostlCod1(),
                                                                    addressFormular.getAddress().getCity());

                    request.setAttribute(B2cConstants.POSSIBLE_COUNTIES, listOfCounties);
                    request.setAttribute(B2cConstants.MESSAGE_LIST, user.getMessageList());

                    forwardTo = "countyRequired" ;
            }
        }// end of if block to decide further processing
		log.exiting();
        return mapping.findForward(forwardTo);
    }//end of doRegistration method
    
 
    
//    protected ActionForward doLoyaltyRegistration(ActionMapping mapping,
//                                               ActionForm form,
//                                               HttpServletRequest request,
//                                               HttpServletResponse response,
//                                               UserSessionData userSessionData,
//                                               RequestParser requestParser,
//                                               BusinessObjectManager bom,
//                                               IsaLocation log,
//                                               IsaCoreInitAction.StartupParameter startupParameter,
//                                               BusinessEventHandler eventHandler,
//                                               boolean multipleInvocation,
//                                               boolean browserBack)
//                 throws CommunicationException {
//
//        // Page that should be displayed next.
//        final String METHOD_NAME = "doLoyaltyRegistration()";
//        log.entering(METHOD_NAME);
//        String forwardTo = "loyaltyInfo";  
//        
//        // if the user decided to cancel
//        if(request.getParameter("cancelClicked").length() > 0) {
//
//          forwardTo = "cancelLoyalty";
//        } 
//        // If the user decided to save the registration details
//        else {
//                              
//            Shop shop = bom.getShop();
// 
//            if (multipleInvocation) {
//                 
//            }
//            else if (shop.getLoyaltyProgramID() != null && shop.getLoyaltyProgramID().length() > 0) {
//              		  
//            	RequestParser.Parameter joinProgramFlag   = LoyaltyUtilities.handleRequestAttributes(requestParser, userSessionData, "joinloyaltyprogramflag");
//           	    RequestParser.Parameter acceptProgramFlag = LoyaltyUtilities.handleRequestAttributes(requestParser, userSessionData, "acceptprogramtermsflag");
//           	             	
//            	LoyaltyMembership loyMembership = LoyaltyUtilities.createLoyaltyMembership(requestParser, userSessionData, bom);  	 
//            	            	
//            	// both flags are set - the user wants to join
//            	if (joinProgramFlag.getValue().isSet() && acceptProgramFlag.getValue().isSet()) {
//            	   // create a new membership for the known customer	
//            	   BusinessPartnerManager buPaMa = bom.createBUPAManager();           	   
//            	   loyMembership.createNewMembership(buPaMa.getDefaultBusinessPartner((new SoldTo()).getName()));
//            	   // get the point account informtion
//            	   if (loyMembership.exists()) {
//            		 loyMembership.getLoyaltyPointAccountFromBackend(shop.getPointCode(), shop.getPointCodeDescription());   
//            	   }
//            	   
//            	// the joining flag is set but the user has forgoten to set the acceptance flag            	   
//            	}  else if (joinProgramFlag.getValue().isSet() && !acceptProgramFlag.getValue().isSet()) {
//            		// he must accept the terms and conditions => error message 
//            		loyMembership.addMessage(new Message(Message.ERROR, "b2c.loyalty.program.noacception", null, "loyaltyRegistration"));            
//            	    forwardTo = "failureLoyalty";
//            	
//            	 // the joining flag is not set but the user has set the acceptance flag
//     		     } else if (!joinProgramFlag.getValue().isSet() && acceptProgramFlag.getValue().isSet()) {
//     			    // he must set the join flag as well => error message 
//       		        loyMembership.addMessage(new Message(Message.ERROR, "b2c.loyalty.program.nojoinflag", null, "loyaltyRegistration")); 
//       	            forwardTo = "failureLoyalty";   	
//     		     }
//       	         
//            	request.setAttribute("loyaltyMessages", loyMembership.getMessageList());
//          		
//            }
//        }// end of if block to decide further processing
//		log.exiting();
//        return mapping.findForward(forwardTo);        
//              
//    }
    
    
    protected boolean handlePromoCode(HttpServletRequest request, UserSessionData userSessionData, RequestParser requestParser, LoyaltyMembership loyMembership)
       throws CommunicationException {
	
	  RequestParser.Parameter promoCode = LoyaltyUtilities.handleRequestAttributes(requestParser, userSessionData, "loyaltyPromotionCode");
	
	  if (promoCode.getValue().isSet() && promoCode.getValue().getString().length() > 0) {
	
	    MarketingBusinessObjectManager marketingBom = (MarketingBusinessObjectManager) userSessionData.getBOM(MarketingBusinessObjectManager.MARKETING_BOM);
	  
	    if (marketingBom.getLoyaltyCampaign() != null && 
	        marketingBom.getLoyaltyCampaign().getHeader() != null &&
	        marketingBom.getLoyaltyCampaign().getHeader().getCampaignID() != null &&
	        marketingBom.getLoyaltyCampaign().getHeader().getCampaignID().length() > 0 &&
	        marketingBom.getLoyaltyCampaign().getHeader().getCampaignID().equals(promoCode.getValue().getString())) { 
	      loyMembership.setLoyaltyPromotion(marketingBom.getLoyaltyCampaign());
	    } else {
		  marketingBom.releaseLoyaltyCampaign();
          LoyaltyCampaign loyCampaign = marketingBom.createLoyaltyCampaign();
          CampaignHeader campaignHeader = (CampaignHeader) loyCampaign.createHeader();

          campaignHeader.setCampaignID(promoCode.getValue().getString());
          loyCampaign.setHeader(campaignHeader);
          loyCampaign.readCampaignHeader();
       
          if (loyCampaign.hasMessages()){
     	    //request.setAttribute("loyaltyMessages", loyCampaign.getMessageList());
     	    loyMembership.copyMessages(loyCampaign);
     	    marketingBom.releaseLoyaltyCampaign();
            return false;  
          } else if (loyCampaign.getHeader().getTechKey() != null &&
        		     loyCampaign.getHeader().getTechKey().getIdAsString().length() > 0) {
            loyMembership.setLoyaltyPromotion(loyCampaign);	  
          } else {
            loyMembership.addMessage(new Message(Message.ERROR, "b2c.loyalty.promotion.notvalid", null, "loyaltyRegistration"));  
            marketingBom.releaseLoyaltyCampaign();
            return false;
          }
        }
	  }  
       
	  return true;
    }    

    
    
    protected boolean handleFlags(RequestParser.Parameter joinProgramFlag, RequestParser.Parameter acceptProgramFlag, LoyaltyMembership loyMembership) {
          
	   // the joining flag is set but the user has forgoten to set the acceptance flag
       if (joinProgramFlag.getValue().isSet() && !acceptProgramFlag.getValue().isSet()) {
	     // he must accept the terms and conditions => error message
	     loyMembership.addMessage(new Message(Message.ERROR, "b2c.loyalty.program.noacception", null, "loyaltyRegistration"));
	     return false;
	     
	     // the joining flag is not set but the user has set the acceptance flag
       } else if (!joinProgramFlag.getValue().isSet() && acceptProgramFlag.getValue().isSet()) {
	     // he must set the join flag as well => error message
	     loyMembership.addMessage(new Message(Message.ERROR, "b2c.loyalty.program.nojoinflag", null, "loyaltyRegistration"));
	     return false;
	    
	   // no flag was set or both flags are set => handled in calling method!!!  
       } else {
    	 return true;
       }
    	  
	 
    }
 
     


    protected ActionForward doLoyaltyRegistration(ActionMapping mapping,
			                                      ActionForm form, 
			                                      HttpServletRequest request,
			                                      HttpServletResponse response,
			                                      UserSessionData userSessionData,
			                                      RequestParser requestParser,
			                                      BusinessObjectManager bom,
			                                      IsaLocation log,
			                                      IsaCoreInitAction.StartupParameter startupParameter,
			                                      BusinessEventHandler eventHandler,
			                                      boolean multipleInvocation,
			                                      boolean browserBack)
              throws CommunicationException {

		// Page that should be displayed next.
		final String METHOD_NAME = "doLoyaltyRegistration()";
		log.entering(METHOD_NAME);
		String forwardTo = "loyaltyInfo";

		// if the user decided to cancel
		if (request.getParameter("cancelClicked").length() > 0) {

            LoyaltyMembership loyMembership = LoyaltyUtilities.getLoyaltyMembership(userSessionData);
            if (loyMembership != null) {
                loyMembership.clearMessages();
            }
            userSessionData.removeAttribute("joinloyaltyprogramflag");
            userSessionData.removeAttribute("acceptprogramtermsflag");
            if (userSessionData.getAttribute("loyaltyPromotionCode") != null) {
              userSessionData.removeAttribute("loyaltyPromotionCode");}
			forwardTo = "cancelLoyalty";
		}
		// If the user decided to save the registration details
		else {
			
			Shop shop = bom.getShop();

			if (multipleInvocation) {

			} else if (shop.getLoyaltyProgramID() != null && shop.getLoyaltyProgramID().length() > 0) {

				RequestParser.Parameter joinProgramFlag = LoyaltyUtilities.handleRequestAttributes(requestParser, userSessionData, "joinloyaltyprogramflag");
				RequestParser.Parameter acceptProgramFlag = LoyaltyUtilities.handleRequestAttributes(requestParser, userSessionData, "acceptprogramtermsflag");

				// both flags are not set - the user doesn't want to join
				if (!joinProgramFlag.getValue().isSet() && !acceptProgramFlag.getValue().isSet()) {
				  forwardTo = "loyaltyInfo";;
				} else {
					LoyaltyMembership loyMembership = LoyaltyUtilities.createLoyaltyMembership(requestParser, userSessionData, bom);
					
					boolean doCreation = handleFlags(joinProgramFlag, acceptProgramFlag,loyMembership) &&  handlePromoCode(request, userSessionData, requestParser, loyMembership); 
                    
					if (doCreation) {
		              // create a new membership for the known customer	
		              BusinessPartnerManager buPaMa = bom.createBUPAManager();           	   
		              loyMembership.createNewMembership(buPaMa.getDefaultBusinessPartner((new SoldTo()).getName()));
		              
		              // get the point account information
//                      if (loyMembership.exists() &&
//                          loyMembership.getLoyaltyProgram() != null) {
//                        loyMembership.getLoyaltyPointAccountFromBackend(loyMembership.getLoyaltyProgram().getTechKey().getIdAsString(), 
//                                                                        loyMembership.getLoyaltyProgram().getProgramId(), 
//                                                                        shop.getPointCode());   
//                      } 						
					} else {
					 forwardTo = "failureLoyalty";	
					}	
					
					request.setAttribute("loyaltyMessages", loyMembership.getMessageList());
				}		
					
			}
		}// end of if block to decide further processing
		log.exiting();
		return mapping.findForward(forwardTo);

	}    

    
    protected RegisterStatus  register(HttpServletRequest request, 
                                       UserSessionData userSessionData,
                                       BusinessObjectManager bom,
                                       RequestParser parser,
                                       User user,
                                       Shop shop,
                                       AddressFormular addressFormular, 
                                       String password,
                                       String passwordVerify,
                                       ForwardStorage actionForward) 
              throws CommunicationException {

        RegisterStatus status = RegisterStatus.NOT_OK;
		boolean doNormalRegistration = true;
		String forwardTo = "success";

		if (shop.getLoyaltyProgramID() != null && shop.getLoyaltyProgramID().length() > 0) {
        
			RequestParser.Parameter joinProgramFlag = LoyaltyUtilities.handleRequestAttributes(parser, userSessionData, "joinloyaltyprogramflag");
			RequestParser.Parameter acceptProgramFlag = LoyaltyUtilities.handleRequestAttributes(parser, userSessionData, "acceptprogramtermsflag");

			LoyaltyMembership loyMembership = LoyaltyUtilities.createLoyaltyMembership(parser, userSessionData, bom);

			// both flags are not set - the user doesn't want to join
			if (!joinProgramFlag.getValue().isSet() && !acceptProgramFlag.getValue().isSet()) {
			  doNormalRegistration = true;
			} else {
				loyMembership = LoyaltyUtilities.createLoyaltyMembership(parser, userSessionData, bom);
				
				boolean doCreation = handleFlags(joinProgramFlag, acceptProgramFlag,loyMembership) &&  handlePromoCode(request, userSessionData, parser, loyMembership); 
                
				if (doCreation) {
				  // register and join the loyalty program
				  status = user.registerAndJoinLoyaltyProgram(shop.getId(), shop.getPointCode(), loyMembership, addressFormular.getAddress(), password, passwordVerify);
				  // get the point account informtion
//                  if (loyMembership.exists() &&
//                      loyMembership.getLoyaltyProgram() != null) {
//                      loyMembership.getLoyaltyPointAccountFromBackend(loyMembership.getLoyaltyProgram().getTechKey().getIdAsString(), 
//                                                                      loyMembership.getLoyaltyProgram().getProgramId(), 
//                                                                      shop.getPointCode());   
//				   }
				  forwardTo = "loyaltyInfoAndPostSteps";
				  doNormalRegistration = false;					
				} else {
				  status = RegisterStatus.NOT_OK;
				  doNormalRegistration = false;
				  forwardTo = "failure";
				}	
				
				request.setAttribute("loyaltyMessages", loyMembership.getMessageList());
			}	
		}

		if (doNormalRegistration) {
			status = user.register(shop.getId(), addressFormular.getAddress(), password, passwordVerify);
		}

		actionForward.setForward(forwardTo);
		return status;
	}     
    
    
    
}// end of SaveRegistrationAction class


