/*****************************************************************************
  Copyright (c) 2000, SAP AG, All rights reserved.
  Author:       SAP
  Created:      17 April 2001

  $Revision: #5 $
  $Date: 2003/10/23 $
*****************************************************************************/


package com.sap.isa.isacore.action.b2c;

// framework dependencies
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.Address;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.Identification;
import com.sap.isa.core.FrameworkConfigManager;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.isacore.AddressFormular;
import com.sap.isa.isacore.action.IsaCoreBaseAction;


/**
 * Prepares the displaying of the sold-to address data by setting all those needed
 * data as request attributes.
 */
abstract public class DisplayAddressBaseAction extends IsaCoreBaseAction {
	BusinessPartner partner = null;
	
	/**
	 * Initialize the action.
	 * In B2C this action could be used by anonymous user.
	 * 
	 * @see com.sap.isa.isacore.action.IsaCoreBaseAction#initialize()
	 */
	public void initialize() {
		if (isB2C()) {
			this.checkUserIsLoggedIn = false;
		}	
	}

    /**
     * Overriden <em>isaPerform</em> method of <code>IsaCoreBaseAction</code>.
     */
    public ActionForward isaPerform(ActionMapping mapping,
                                    ActionForm form,
                                    HttpServletRequest request,
                                    HttpServletResponse response,
                                    UserSessionData userSessionData,
                                    RequestParser requestParser,
                                    BusinessObjectManager bom,
                                    IsaLocation log) throws CommunicationException {

		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
				
        //get the shop details to which the user intends to login.
        Shop shop = bom.getShop();        
        // use the bom to get the user business object
        User user = bom.getUser();
        
        if (!user.isUserLogged()) {
            userSessionData.setAttribute(B2cConstants.FRAME_BEFORE_LOGIN,"accounts");
            request.setAttribute(B2cConstants.MESSAGE_LIST, user.getMessageList());
            log.exiting();
            return mapping.findForward("login");
        }
      
        
        Address address = getAddress(request,userSessionData,bom);
        
        if (address == null) {
        	request.setAttribute(B2cConstants.MESSAGE_LIST, user.getMessageList());
        	return mapping.findForward("failure");	
        }
        getUserId(shop, request);

        // get the forms of address supported in the shop from the backend.
        ResultData formsOfAddress = shop.getTitleList();
        // get the list of countries from the shop business object
        ResultData listOfCountries = shop.getCountryList();
        //set the default country to the value already in the user's SoldToAddress
        String defaultCountryId = address.getCountry();
        // flag that indicates whether a region is needed to complete an address
        boolean isRegionRequired = false;
        // list of regions
        ResultData listOfRegions = null;

        // check whether the default country has a needs region to complete the address.
        listOfCountries.beforeFirst();
        while (listOfCountries.next()){
            if (listOfCountries.getString("ID").equalsIgnoreCase(defaultCountryId)){
                isRegionRequired = listOfCountries.getBoolean("REGION_FLAG");
            }
        }

        // if region is needed then use Shop to get the list of regions
        if (isRegionRequired){
            listOfRegions = shop.getRegionList(defaultCountryId);
        }

        // address format id
        int addressFormatId = shop.getAddressFormat();
        // create a new AddressFormular instance
        AddressFormular addressFormular = new AddressFormular(address, addressFormatId);
        // this section sets the 'isPerson'-value and 'isRegionRequired'-value accordingly
        if(shop.isTitleKeyForPerson(address.getTitleKey())) {
            addressFormular.setPerson(true);
        }
        else {
            addressFormular.setPerson(false);
        }
        addressFormular.setRegionRequired(isRegionRequired);


        // set all the required data as request attributes and pass them to 
        // the soldToAddress.jsp
        // -----------------------------------------------------------------
        // the attributes will only be set if the same attribute 
        // IS NOT ALREADY SET in the request object
        // (-> this code will will be performed as well if within the
        // SavepersoalDetails action a return to the soldToAddress.jsp will
        // be initiated and in this case the already set data shouldn't be
        // overwritten)  
        // -----------------------------------------------------------------
        if(request.getAttribute(B2cConstants.FORMS_OF_ADDRESS) == null) {
            request.setAttribute(B2cConstants.FORMS_OF_ADDRESS, formsOfAddress);
        }
        if(request.getAttribute(B2cConstants.POSSIBLE_COUNTRIES) == null) {
            request.setAttribute(B2cConstants.POSSIBLE_COUNTRIES,listOfCountries);
        }
        if (isRegionRequired &&
                request.getAttribute(B2cConstants.POSSIBLE_REGIONS) == null) {

            request.setAttribute(B2cConstants.POSSIBLE_REGIONS, listOfRegions);
        }
        if(request.getAttribute(B2cConstants.ADDRESS_FORMULAR) == null) {
            request.setAttribute(B2cConstants.ADDRESS_FORMULAR, addressFormular);
        }
        
        //get the list of counties if available
        ResultData possibleCounties = shop.getTaxCodeList(address.getCountry(),
                                                          address.getRegion(),
                                                          address.getPostlCod1(),
                                                          address.getCity());
        
        if(request.getAttribute(B2cConstants.POSSIBLE_COUNTIES) == null) {
            request.setAttribute(B2cConstants.POSSIBLE_COUNTIES, possibleCounties);
        }
        
        request.setAttribute(B2cConstants.MESSAGE_LIST, user.getMessageList());

		log.exiting();
        return mapping.findForward("success");
    }

	/* 
	 * List must contain exactly one entry, if it doesn't add an empty one,
	 * so the user can fill in a value in DisplayPersonalData 
	 * */
	private Identification getId(BusinessPartner partner, String idType) {
        Identification returnID = null;
        if(partner != null) {
		   List idList = partner.getIdentificationsOfType(idType);
		
           if (idList.isEmpty()) {
			  returnID = new Identification();
			  returnID.setType(idType);
		   } 
		   else {
			  Iterator itIds = idList.iterator();
			  Identification idTmp = null;
			
			  if (itIds.hasNext()) {
                 returnID = (Identification) itIds.next();
				 if (itIds.hasNext()) {
					log.exiting();
					throw new PanicException("To many identifications of type '" + idType + "'.");
				 }
			  }
		   }
        }
		return returnID;
	}

	// getAddress must be called first in order to initialize partner.
	private boolean getUserId(Shop shop, HttpServletRequest request) {
		boolean retval = false; // no ID to be displayed
		boolean showVerification = false;
		
		try {
			// get the Verification Word's Diplay Status (if there's any)
			showVerification = (boolean)  (
			(String)FrameworkConfigManager.Servlet.getInteractionConfigParameter(
				request.getSession(),
				"ui",
				"ShowVerificationWord",
				""
				)
			).equals("true");
		}
		catch (Exception ex) {
			if (log.isDebugEnabled()) {
				log.debug("Verification word not found in XCM: " + ex.getMessage());
			}
		}

		if (showVerification) {
			String idType = shop.getIdTypeForVerification();
		 		
			if (idType.length() > 0) {

				Identification vWord = getId(partner, idType);
                if(vWord != null) {
                    retval = true;
                    request.setAttribute( B2cConstants.VERIFICATION_WORD, vWord.getNumber());
                    request.setAttribute( B2cConstants.VERIFICATION_WORD_TK, vWord.getTechKey().getIdAsString());
                }
			}
		}
		return retval;
	}

	
	/**
	 * Return the address which should be used. <br>
	 * 
	 * @param request current request 
	 * @param partner business partner, which address should be changed.
	 * @return
	 */
    abstract protected Address getAddress(HttpServletRequest request,
											UserSessionData userSessionData,
											BusinessObjectManager bom);

}        						  