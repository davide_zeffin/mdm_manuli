/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Biju Raj
  Created:      3rd April 2001

  $Revision: #2 $
  $Date: 2001/07/23 $
*****************************************************************************/

package com.sap.isa.isacore.action.b2c;

// framework dependencies
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.ecommerce.businessobject.MarketingBusinessObjectManager;
import com.sap.ecommerce.businessobject.loyalty.LoyaltyMembership;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.webcatalog.CatalogBusinessObjectManager;
import com.sap.isa.catalog.webcatalog.WebCatArea;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 * Checks if the user has a loyalty membership. If yes, the
 * <i>Redeem Points</i> area of the web shop will be displayed next. 
 */
public class AccountStartAction extends IsaCoreBaseAction {
	
	/**
	 * Initialize the action.
	 * In B2C this action could be used by anonymous user.
	 * 
	 * @see com.sap.isa.isacore.action.IsaCoreBaseAction#initialize()
	 */
	public void initialize() {
		if (isB2C()) {
			this.checkUserIsLoggedIn = false;
		}	
	}
	
    /**
     * Overriden <em>isaPerform</em> method of <code>IsaCoreBaseAction</code>.
     */
    public ActionForward isaPerform(ActionMapping mapping,
                                    ActionForm form,
                                    HttpServletRequest request,
                                    HttpServletResponse response,
                                    UserSessionData userSessionData,
                                    RequestParser requestParser,
                                    BusinessObjectManager bom,
                                    IsaLocation log)
                                    throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        
        // Page that should be displayed next.
        String forward = "success";

        // Check if a membership exists for the user and
        // if catalog was read 
        MarketingBusinessObjectManager mktBom = (MarketingBusinessObjectManager)userSessionData.getBOM(MarketingBusinessObjectManager.MARKETING_BOM);
        LoyaltyMembership loyMembership = mktBom.getLoyaltyMembership();
        
        if (loyMembership != null &&
            loyMembership.exists() &&
            request.getAttribute("subCategories") == null) {
            CatalogBusinessObjectManager catbom = (CatalogBusinessObjectManager)userSessionData.getBOM(CatalogBusinessObjectManager.CATALOG_BOM);
            if (catbom !=null && catbom.getCatalog() != null) {
                WebCatArea area = catbom.getCatalog().getCurrentArea();
                if ( area == null || 
                     !(area.isBuyPointsCategory() ||
                       area.isRewardCategory()) ) {
                    catbom.getCatalog().setCurrentArea(WebCatArea.ROOT_AREA);
                }
            }
            forward = "readcatalog";
        }
        
		log.exiting();
        return mapping.findForward(forward);
    }
}

