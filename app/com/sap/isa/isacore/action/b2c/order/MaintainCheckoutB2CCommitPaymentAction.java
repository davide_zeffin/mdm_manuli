package com.sap.isa.isacore.action.b2c.order;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businessobject.order.Basket;
import com.sap.isa.businessobject.order.Order;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.businesspartner.businessobject.PartnerFunctionBase;
import com.sap.isa.businesspartner.businessobject.SoldTo;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreInitAction;
import com.sap.isa.payment.businessobject.PaymentBase;
//test end

             
public class MaintainCheckoutB2CCommitPaymentAction extends MaintainCheckoutB2CBaseAction {

	public static final String CHECKOUT_ERROR_MESSAGES = "MaintainCheckoutB2CCommitPaymentAction.ErrorMessages";
	private static final String FORWARD_SAVEORDER     = "saveorder_now";
	private static final String FORWARD_PAYMENTERROR  = "paymenterror";
	
	public MaintainCheckoutB2CCommitPaymentAction() {
	}

	
	public String checkOutPerform (HttpServletRequest request,
		HttpServletResponse response,
		HttpSession session,
		UserSessionData userSessionData,
		RequestParser parser,
		BusinessObjectManager bom,
		IsaLocation log,	    
	    IsaCoreInitAction.StartupParameter startupParameter,
		boolean multipleInvocation,
		boolean browserBack,
		BusinessEventHandler eventHandler,
		Shop shop,
		Basket basket,
		Order order) throws CommunicationException {
				
		final String METHOD_NAME = "checkoutCommitPayment()";
		log.entering(METHOD_NAME);
		BusinessPartnerManager buPaMa = bom.createBUPAManager();
		BusinessPartner buPa = buPaMa.getDefaultBusinessPartner(PartnerFunctionBase.SOLDTO);
        SoldTo soldTo = (SoldTo)buPa.getPartnerFunctionObject(PartnerFunctionBase.SOLDTO);

/* Attention: If the soldto.createPayment() method is called in the MaintainCheckoutB2CAction an additional   */
/*            business agreement is created  if the user chooses the BACK button on the 'Check Order' page.   */
/*            To avoid this the call of the method is moved to this action.                                   */  
/*            Thus it would possible to merge the calls of the methods soldTo.createPayment() and             */
/*            soldTo.commitPayment() together in one RFC call!                                                */
/*                                                                                                            */ 
//*corr#start                                                                                                            
        PaymentBase payment = (PaymentBase) order.getPayment();	 
            
        if (shop.isPaymAtBpEnabled() == true) {	                    
	      List buAgList = soldTo.createPayment(buPa, payment);        
	      if ( buAgList != null && buAgList.size()>0){        
		     soldTo.setBusinessAgreement(buAgList);                
//		List buAgList = soldTo.getBusinessAgreement();
//corr#end		
		     soldTo.commitPayment();		  	
//corr#start
		  } else if (payment.getError() == true) {
		    return FORWARD_PAYMENTERROR;
	      }     
		}
//corr#end					 
		log.exiting();
		return FORWARD_SAVEORDER;
	}	
}	