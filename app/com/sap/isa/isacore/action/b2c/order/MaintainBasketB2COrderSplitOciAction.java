/*****************************************************************************
  Class:        MaintainBasketB2COrderSplitOciAction
  Copyright (c) 2002, SAP AG, Germany, All rights reserved.
  Author:
  Created:      08.10.2002
  Version:      1.0

  $Revision: #1 $
  $Date: 2002/11/07 $
*****************************************************************************/

package com.sap.isa.isacore.action.b2c.order;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.item.ItemList;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.businessobject.order.Basket;
import com.sap.isa.businessobject.order.BasketSplitPartnerEntry;
import com.sap.isa.businessobject.order.Leaflet;
import com.sap.isa.businessobject.order.PartnerListEntry;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreInitAction;
import com.sap.isa.isacore.action.b2c.B2cConstants;

/**
 * Action to create a list of soldFrom partners with their related items from a multisoldFrom basket,
 * if the basket is going to be forwarded to the partner shop via oci
 * <p>
 * The following forwards are used by this action:
 * </p>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>success</td><td>a list of partners with related items was created an can be displayed</td></tr>
 *   <tr><td>showbasket</td><td>show the basket again</td></tr>
 * </table>
 *
 * @author
 * @version 1.0
 */
public class MaintainBasketB2COrderSplitOciAction extends MaintainBasketB2CBaseAction {

    public static String FORWARD_SHOWBASKET = "showbasket";
    public static String FORWARD_BASKETEMPTY = "basketEmpty";
    public static String FORWARD_CHECKOUTPARTNER = "checkoutpartner";
	
    public MaintainBasketB2COrderSplitOciAction() {
    }
    
    public void initialize() {
        this.checkUserIsLoggedIn = false;
    }
    
    protected String basketPerform(
                HttpServletRequest request,
                HttpServletResponse response,
                HttpSession session,
                UserSessionData userSessionData,
                RequestParser parser,
                BusinessObjectManager bom,
                IsaLocation log,
                IsaCoreInitAction.StartupParameter startupParameter,
                boolean multipleInvocation,
                boolean browserBack,
                Shop shop,
                Basket basket,
                Leaflet leaflet)
                    throws CommunicationException {

		final String METHOD_NAME = "basketPerform()";
		log.entering(METHOD_NAME);
		// update basket with request data
		updateBasket(parser, shop, basket, bom, log, userSessionData, request);

        ItemList items = basket.getItems();
        int noItems = items.size();

        if (noItems == 0) {
        	log.exiting();
            return FORWARD_BASKETEMPTY;
        }

        ArrayList soldFromListOci;
        PartnerListEntry soldFrom;
        HashMap keyMap = new HashMap();

        // first check, if the action is entered the first time
        soldFromListOci = (ArrayList) userSessionData.getAttribute(SOLDFROMLISTOCI);

        if (soldFromListOci == null ||
           !((BasketSplitPartnerEntry) soldFromListOci.get(0)).getBasketGuid().getIdAsString().equals(basket.getTechKey().getIdAsString())
            ) { // new soldFromList must be created
            soldFromListOci = new ArrayList();
        }

        // delete all entries in the list, where no order was placed. Than add all soldfroms
        // in the basket, that are in the basket and not in the list. So we get rid off
        // soldfroms that are not needed anymore, because all related Items have been deleted

        Iterator soldFromListOciIter = soldFromListOci.iterator();
        BasketSplitPartnerEntry soldFromOciIterEntry;

        while (soldFromListOciIter.hasNext()) {
           soldFromOciIterEntry = (BasketSplitPartnerEntry) soldFromListOciIter.next();
           if (!soldFromOciIterEntry.isBasketForwarded()) {
               soldFromListOciIter.remove();
           }
           else {// remember the the TechKeys of the soldFroms that are already in the list
               keyMap.put(soldFromOciIterEntry.getSoldFromTechKey().getIdAsString(),
                          soldFromOciIterEntry);
               // delete the itemList, because the numnber of items might have changed
               soldFromOciIterEntry.getItemList().clear();
           }
        }

        String soldFromKeyStr = "";
        String soldFromNameAndCity = null;
        String soldFromName = null;
        String soldFromCity = null;
        BasketSplitPartnerEntry soldFromOciEntry = null;
        int colIdx = 0;
        ItemSalesDoc item;

        for (int i = 0; i < noItems; i++) {
            item = items.get(i);
            // only items that are available and assigned to a soldfrom are regarded
            soldFrom = item.getPartnerList().getSoldFrom();
            if (soldFrom == null || soldFrom.getPartnerTechKey() == null ||
                soldFrom.getPartnerTechKey().getIdAsString().length() == 0 ||
                item.isAvailableInPartnerCatalog() == ItemData.NOT_AVAILABLE_AT_PARTNER) {
                continue;
            }
            // check if soldfrom is already in the list
            soldFromKeyStr = soldFrom.getPartnerTechKey().getIdAsString();
            if (!keyMap.containsKey(soldFromKeyStr)) {
                soldFromNameAndCity = soldFrom.getPartnerNameAndCity(bom.createBUPAManager());
                colIdx = soldFromNameAndCity.indexOf(",");
                soldFromName = soldFromNameAndCity.substring(0, colIdx);
                soldFromCity = soldFromNameAndCity.substring(colIdx + 2);
                soldFromOciEntry = new BasketSplitPartnerEntry(basket.getTechKey(),
                                                              new TechKey(soldFromKeyStr),
                                                              soldFrom.getPartnerId(),
                                                              soldFromName,
                                                              soldFromCity);
                // add the item
                soldFromOciEntry.getItemList().add(item);
                // add new entry to the list
                soldFromListOci.add(soldFromOciEntry);
                // remember TechKey and entry
                keyMap.put(soldFromKeyStr, soldFromOciEntry);
            }
            else {
                // add the item to the itemlist of the reseller
                ((BasketSplitPartnerEntry) keyMap.get(soldFromKeyStr)).getItemList().add(item);
            }
        }

        // finally delete all soldfroms with no items
        soldFromListOciIter = soldFromListOci.iterator();

        while (soldFromListOciIter.hasNext()) {
           soldFromOciIterEntry = (BasketSplitPartnerEntry) soldFromListOciIter.next();
           if (soldFromOciIterEntry.getItemList().size() == 0) {
               soldFromListOciIter.remove();
           }
        }

        // no resellers to forward to
        if (soldFromListOci.size() == 0) {
        	log.exiting();
            return FORWARD_SHOWBASKET;
        }

        // store the list in the session context
        userSessionData.setAttribute(SOLDFROMLISTOCI, soldFromListOci);
        // store item list in request context
        request.setAttribute(B2cConstants.RK_BASKET_ITEM_LIST, items);
        // store header in request context
        request.setAttribute(B2cConstants.RK_BASKET_HEADER, basket.getHeader());
        // store basket in request context
        request.setAttribute(B2cConstants.RK_BASKET, basket);
		log.exiting();
        return FORWARD_CHECKOUTPARTNER;
    }
}