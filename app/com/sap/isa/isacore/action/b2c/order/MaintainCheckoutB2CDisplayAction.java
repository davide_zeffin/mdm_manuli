/*****************************************************************************
  Class:        MaintainCheckoutB2CDisplayAction
  Copyright (c) 2001, SAP AG, All rights reserved.
  Author:       SAP AG
  Created:      25.03.2001
  Version:      1.0

  $Revision: #5 $
  $Date: 2002/11/27 $
*****************************************************************************/

package com.sap.isa.isacore.action.b2c.order;

import java.util.Iterator;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sap.isa.businessobject.Address;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.ProductList;
import com.sap.isa.businessobject.ShipTo;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businessobject.item.ItemList;
import com.sap.isa.businessobject.order.Basket;
import com.sap.isa.businessobject.order.Order;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.businesspartner.businessobject.SoldFrom;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.isacore.ShipToSelection;
import com.sap.isa.isacore.action.IsaCoreInitAction;
import com.sap.isa.isacore.action.b2c.B2cConstants;
import com.sap.isa.payment.backend.boi.PaymentBaseData;
import com.sap.isa.payment.backend.boi.PaymentMethodData;
import com.sap.isa.payment.businessobject.BankTransfer;
import com.sap.isa.payment.businessobject.PaymentBase;
import com.sap.isa.payment.businessobject.PaymentBaseType;

/**
 * Action to display the checkout screen showing all data and offer the user
 * the oportunity to enter payment and shipping information.
 * <p>
 * The following forwards are used by this action:
 * </p>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>FORWARD_PRINTOUT</td><td>go to a screen summarizing the order data and allow the printout of that information</td></tr>
 *   <tr><td>FORWARD_CHECKOUT</td><td>go to the checkout screen</td></tr>
 * </table>
 *
 * @author Thomas Smits
 * @version 1.0
 */
public class MaintainCheckoutB2CDisplayAction extends MaintainCheckoutB2CBaseAction {
	
    public MaintainCheckoutB2CDisplayAction() {
    }

    
    /**
     * Read the order data to display it on the Checkout page
     *
     * @param request           request context
     * @param response          response context
     * @param session           unwrapped session context
     * @param userSessionData   object wrapping the session
     * @param requestParser     parser to simple retrieve data from the request
     * @param bom               reference to the BusinessObjectManager
     * @param log               reference to the IsaLocation, needed for logging
     * @param startupParameter  object containing the startup parameters
     * @param eventHandler      object to capture events with
     * @param multipleInvocation flag indicating, that a multiple invocation occured
     * @param browserBack       flag indicating a browser back
     * @param eventHandler      reference to the business event handler
     * @param user              user business object
     * @param shop              shop business object
     * @param basket            the current sales document edited
     * @param order             the order created
     *
     * @return logical key for a forward to another action or jsp
     *
	 * @see com.sap.isa.isacore.action.b2c.order.MaintainCheckoutB2CBaseAction#checkOutPerform(HttpServletRequest, HttpServletResponse, HttpSession, UserSessionData, RequestParser, BusinessObjectManager, IsaLocation, StartupParameter, boolean, boolean, BusinessEventHandler, User, Shop, Basket, Order)
	 */
    public String checkOutPerform(HttpServletRequest request,
                HttpServletResponse response,
                HttpSession session,
                UserSessionData userSessionData,
                RequestParser parser,
                BusinessObjectManager bom,
                IsaLocation log,
                IsaCoreInitAction.StartupParameter startupParameter,
                boolean multipleInvocation,
                boolean browserBack,
                BusinessEventHandler eventHandler,
                Shop shop,
                Basket basket,
                Order order) throws CommunicationException {

		final String METHOD_NAME = "checkOutPerform()";
		log.entering(METHOD_NAME);
        String forwardTo;

        // get the list of countries from the shop Object
        ResultData listOfCountries = shop.getCountryList();
        // get the default country for the given shop
        String defaultCountryId = shop.getDefaultCountry();
        // flag that indicates whether a region is needed to complete an address

		RequestParser.Parameter printDisplay =
			parser.getParameter("printDisplay");

		if (!printDisplay.isSet()) {
			printDisplay = parser.getAttribute("printDisplay");
		}

		// if the order is already saved (basket is null)
		// and if it is not the printout return the FORWARD_SAVED
		if (basket == null && !printDisplay.isSet()) {
			return FORWARD_SAVED;
		}	

        // clearMessages() as creditcard error messages can still exist, int. CSN 4505052
        order.clearMessages();
        // Don't read the order again for print display only
        if (!printDisplay.isSet()) { 
			order.readHeader();
			order.readAllItems(); 
        }

        // get sold-to address
        Address soldToAddress = getSoldToAddress(bom.createBUPAManager(), order.getHeader());    // may throw CommunicationException

        // put data in request context
        request.setAttribute(RC_HEADER, order.getHeader());
        request.setAttribute(RC_SOLDTO_ADDRESS, soldToAddress);
        // retrieve

        Address shipToAddress = null;
        
        ShipToSelection shipToSelection = (ShipToSelection)request.getSession().getAttribute(B2cConstants.SC_SHIPTO_SELECTION);           
        ShipTo shipTo = shipToSelection.getActualShipTo(); 

        if (shipTo == null) {
            shipTo = order.getHeader().getShipTo();
        }
        if (shipTo != null) {
            shipToAddress = shipTo.getAddress();
        }
        else {    
            
            if (log.isDebugEnabled()) {
                log.debug("isaPerform(): shipTo is null");
            }
            shipToAddress = soldToAddress;
        }


        if (shop.isPartnerBasket()) {
            // Create the order for a sales partner
		
            request.setAttribute(RC_PARTNER_IS_SHIPTO, shipToSelection.isPartnerShipTo(order.getHeader().getShipTo())?"true":"false");		

            String msgText = null;
            Message msg = null;
            // get matching description
            ServletContext context = getServlet().getServletContext();

            BusinessPartnerManager bupama = bom.createBUPAManager();
            TechKey soldFromKey = order.getHeader().getPartnerList().getSoldFrom().getPartnerTechKey();
            BusinessPartner soldFromPartner = bupama.getBusinessPartner(soldFromKey);
            SoldFrom soldFrom = (SoldFrom) soldFromPartner.getPartnerFunctionObject(PartnerFunctionData.SOLDFROM);
            if (soldFrom == null) {
                soldFrom = new SoldFrom();
                bupama.addPartnerFunction(soldFromPartner, soldFrom);
                soldFrom.readData(soldFromKey, shop.getLanguage(), null, null, null);
            }

            PaymentBase paymentBase = order.getPayment();
        	if(paymentBase.getPaymentTypes().size() == 0) {
        		PaymentBaseType payType1 = (PaymentBaseType)soldFrom.getPaymentType();
        		if (payType1 != null) {
        		    payType1.setTechKey(new TechKey(RC_PAYTYPE1));
        		    paymentBase.addPaymentType(payType1);
                    PaymentBaseType payType2 = (PaymentBaseType)payType1.clone();
                    payType2.setTechKey(new TechKey(RC_PAYTYPE2));
                    paymentBase.addPaymentType(payType2);
        		}
        		else {
                    msgText = WebUtil.translate(context, session, "b2c.order.partner.nopayment", null);
                    msg = new Message(Message.ERROR, "b2c.order.partner.nopayment");
                    msg.setDescription(msgText);
                    order.addMessage(msg);        			
        		}
        	}
            request.setAttribute(RC_PAYTYPE1, paymentBase.getPaymentTypeByTechnicalKey(RC_PAYTYPE1));
            request.setAttribute(RC_PAYTYPE2, paymentBase.getPaymentTypeByTechnicalKey(RC_PAYTYPE2));        	
            request.setAttribute(RC_CARDTYPE, soldFrom.getCardType());

            request.setAttribute(RC_SHIPCOND, soldFrom.getShippingCond());
            if (soldFrom.getShippingCond().getNumRows() == 0) {
               msgText = WebUtil.translate(context, session, "b2c.order.partner.noshiconds", null);
               msg = new Message(Message.ERROR, "b2c.order.partner.noshiconds");
               msg.setDescription(msgText);
               order.addMessage(msg);
            }

            // add address of the soldfrom to the context
            Address soldFromAddress = soldFromPartner.getAddress();
            request.setAttribute(RC_SOLDFROM_ADDRESS, soldFromAddress);
  
            String soldFromCountryDescription = shop.getCountryDescription(soldFromAddress.getCountry());           
            request.setAttribute(RC_SOLDFROM_COUNTRY_DESCRIPTION, soldFromCountryDescription);
          
        }
        else {
            PaymentBase paymentBase = order.getPayment();
        	if(paymentBase.getPaymentTypes().size() == 0) {
        		
        		// we're in initial state 
        		// -> read payment types from backend and store them in PaymentBase class 
        		PaymentBaseType payType1 = (PaymentBaseType)order.readPaymentType();
        		PaymentBaseType payType2 = (PaymentBaseType)payType1.clone();
        		payType1.setTechKey(new TechKey(RC_PAYTYPE1));
        		paymentBase.addPaymentType(payType1);
        		payType2.setTechKey(new TechKey(RC_PAYTYPE2));
        		paymentBase.addPaymentType(payType2); 
        	}
            
            request.setAttribute(RC_PAYTYPE1, paymentBase.getPaymentTypeByTechnicalKey(RC_PAYTYPE1));
            request.setAttribute(RC_PAYTYPE2, paymentBase.getPaymentTypeByTechnicalKey(RC_PAYTYPE2));

            request.setAttribute(RC_CARDTYPE, order.readCardType());
            request.setAttribute(RC_SHIPCOND, order.readShipCond(userSessionData.getSAPLanguage()));
        }

		request.setAttribute(RC_DLV_PRIO, order.getHeader().getDeliveryPriority());
        request.setAttribute(RC_SHIPTO_ADDRESS, shipToAddress);

        request.setAttribute(RC_CARDMONTH, CARD_MONTHS);
        request.setAttribute(RC_CARDYEARS, CARD_YEARS);
        request.setAttribute(RC_MESSAGELIST, order.getMessageList());
		request.setAttribute(RC_PAYMENT, order.getPayment());
        request.setAttribute(RC_SHOP, shop);
        request.setAttribute(B2cConstants.RK_BASKET_ITEM_LIST, order.getItems());
        request.setAttribute("paytypeDescription", getPaymentTypeDescr(request, order));
        request.setAttribute("shipcondDescription", getShipCondDescr(order, shop));
		request.setAttribute(B2cConstants.POSSIBLE_COUNTRIES,listOfCountries);
		request.setAttribute(B2cConstants.DEFAULT_COUNTRY, defaultCountryId);

        String soldToCountryDescription = shop.getCountryDescription(soldToAddress.getCountry());
        String shipToCountryDescription = null;
        
        if (shipToAddress != null) {
            shipToCountryDescription = shop.getCountryDescription(shipToAddress.getCountry());
        }
        if (soldToCountryDescription != null) {
            request.setAttribute(RC_SOLDTO_COUNTRY_DESCRIPTION, soldToCountryDescription);
        }
        if (shipToCountryDescription != null) {
            request.setAttribute(RC_SHIPTO_COUNTRY_DESCRIPTION, shipToCountryDescription);
        }
        
		PaymentBaseData payment = order.getPayment();
		Iterator iter = payment.getPaymentMethods().iterator();
		String bankTransferCountryID = "";
		while (iter.hasNext() && (bankTransferCountryID.equals(""))) {
			PaymentMethodData payMethod = (PaymentMethodData) iter.next();
			if (payMethod instanceof BankTransfer) {
				bankTransferCountryID = ((BankTransfer) payMethod).getCountry();
			}
		}
		request.setAttribute(RC_BANK_TRANSFER_COUNTRY_DESCRIPTION, shop.getCountryDescription(bankTransferCountryID));
        
        if (printDisplay.isSet()) {
            forwardTo = FORWARD_PRINTOUT;
        }
        else {
            forwardTo = FORWARD_CHECKOUT;
        }
        
        // Get the product information to add it to the item list
        WebCatInfo catalog = getCatalogBusinessObjectManager(userSessionData).getCatalog();
        ItemList items = order.getItems();
        ProductList.enhance(catalog, items.toArray());
        
		log.exiting();
        return forwardTo;
    }
}