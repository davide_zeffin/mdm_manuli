/*****************************************************************************
  Class:        MaintainBasketB2CEmptyAction
  Copyright (c) 2001, SAP AG, All rights reserved.
  Author:       SAP AG
  Created:      20.09.2001
  Version:      1.0

  $Revision: #5 $
  $Date: 2003/02/11 $
*****************************************************************************/

package com.sap.isa.isacore.action.b2c.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.order.Basket;
import com.sap.isa.businessobject.order.Leaflet;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.RecoverBasketCookieHandler;
import com.sap.isa.isacore.action.IsaCoreInitAction;

/**
 * Action to remove the whole content of the basket.
 * <p>
 * The following forwards are used by this action:
 * </p>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>displayBasket</td><td>the basket is displayed (forward normally not used)</td></tr>
 *   <tr><td>basketEmpty</td><td>the basket is empty and a special screen reporting this is shown</td></tr>
 *   <tr><td>displayProducts</td><td>display product page </td></tr>
 * </table>
 *
 * @author SAP AG
 * @version 1.0
 */
public class MaintainBasketB2CEmptyAction extends MaintainBasketB2CBaseAction {
    
    // Forward to display products page
    public static final String FORWARD_DISPLAYPRODUCTS = "displayProducts";

    public MaintainBasketB2CEmptyAction() {
    }
    
    public void initialize() {
        this.checkUserIsLoggedIn = false;
    }
    
    protected String basketPerform(
                HttpServletRequest request,
                HttpServletResponse response,
                HttpSession session,
                UserSessionData userSessionData,
                RequestParser parser,
                BusinessObjectManager bom,
                IsaLocation log,
                IsaCoreInitAction.StartupParameter startupParameter,
                boolean multipleInvocation,
                boolean browserBack,
                Shop shop,
                Basket basket,
                Leaflet leaflet)
                    throws CommunicationException {
         
		final String METHOD_NAME = "basketPerform()";
		log.entering(METHOD_NAME);               
        RequestParser.Parameter showProducts =
            parser.getParameter("showproducts");

        // read catalog
        WebCatInfo webCat =
                getCatalogBusinessObjectManager(userSessionData).getCatalog();

        basket.emptyContent(shop, bom.createBUPAManager(), webCat);
        bom.releaseBasket();
        
        if (basket.isDocumentRecoverable()) {
            RecoverBasketCookieHandler recoBasketCookieHdlr = (RecoverBasketCookieHandler) userSessionData.getAttribute(RecoverBasketCookieHandler.SC_RECO_BASKET_COOKIE_HDLR);
            if (basket.getTechKey() == null && recoBasketCookieHdlr != null) {
                log.debug("Delete recovery Cookie for emptied basket");
                recoBasketCookieHdlr.setResponse(response);
                recoBasketCookieHdlr.delete(shop);
            }
            else {
                if (log.isDebugEnabled()) {
                    log.debug("Set/Rewrite recovery Cookie for emptied basket with TechKey " + basket.getTechKey());
                }
            
                if (recoBasketCookieHdlr == null) {
                    recoBasketCookieHdlr = new RecoverBasketCookieHandler(request, response);
                    recoBasketCookieHdlr.save(shop, basket.getTechKey());
                    userSessionData.setAttribute(RecoverBasketCookieHandler.SC_RECO_BASKET_COOKIE_HDLR, recoBasketCookieHdlr);
                }
                else {
                    recoBasketCookieHdlr.setResponse(response);
                    recoBasketCookieHdlr.delete(shop);
                    recoBasketCookieHdlr.save(shop, basket.getTechKey());
                }
            }
        }
             
        // delete some session variable variables, that may be present
        userSessionData.removeAttribute(SOLDFROMLIST);
        userSessionData.removeAttribute(SOLDFROMLIST_IDX);
        userSessionData.removeAttribute(SOLDFROMLISTOCI);
        
        if (!showProducts.isSet() || showProducts.getValue().getString().length() == 0) {
            // aeb: because after this action the class 
            // com.sap.isa.isacore.action.b2c.order.MaintainBasketB2CDisplayAction
            // will be called, the displayBasket method must not be called twice
            // forwardTo = displayBasket(request, user, shop, basket, bom, leaflet, log, userSessionData);
           log.exiting();
           return "displayBasket";
        }
        else {
        	log.exiting();
            return FORWARD_DISPLAYPRODUCTS;
        }
    }
}