/*****************************************************************************
    Class         DisplayMiniBasketB2CAction
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Displays the mini basket screen in the B2C scenario
    Author:       Sabine Heider
    Created:      13.03.2001
    Version:      1.0
*****************************************************************************/
package com.sap.isa.isacore.action.b2c.order;

// framework dependencies
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.item.ItemList;
import com.sap.isa.businessobject.order.Basket;
import com.sap.isa.businessobject.order.Leaflet;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.action.b2c.B2cConstants;

/**
 * Displays the mini basket screen in the B2C scenario.
 * The mini basket screen consists of a mini basket and a mini leaflet part.
 *
 * @author Sabine Heider
 * @version 1.0
 *
 */
public class DisplayMiniBasketB2CAction extends IsaCoreBaseAction {

	/**
	 * Initialize the action.
	 * In B2C this action could be used by anonymous user.
	 * 
	 * @see com.sap.isa.isacore.action.IsaCoreBaseAction#initialize()
	 */
	public void initialize() {
		if (isB2C()) {
			this.checkUserIsLoggedIn = false;
		}	
	}


    /**
     * Reads the items in the basket and leaflet and forwards them to the
     * corresponding JSP.
     * The target must be maintained in the <em>config.xml</em> file using the
     * alias <code>displayMiniBasket</code>.
     *
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            BusinessObjectManager bom,
            IsaLocation log) throws CommunicationException {

        // page to be displayed next
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        String forwardTo = null;

        ItemList items = null;

        boolean isDebugEnabled = log.isDebugEnabled();

        // get business objects
        // basket is created when the first item is added
        Basket basket = bom.getBasket();

        Leaflet leaflet = bom.getLeaflet();
        HeaderSalesDocument header = null;

        if (basket != null) {
            // read the current basket header data from the backend
            basket.readHeader();
            // get the current basket header data
            header = basket.getHeader();

            // get basket items
            basket.readAllItems();    
            items = basket.getItems();
        }
        else {
            if (isDebugEnabled) {
                log.debug("No basket found, creating empty item list.");
            }
            items = new ItemList();
        }

        // store basket items in request context
        request.setAttribute(B2cConstants.RK_BASKET_ITEM_LIST, items);

        // store basket header in request conetext
        if (header != null) {
            request.setAttribute(B2cConstants.RK_BASKET_HEADER, header);
        }

        // store number of leaflet items in request context
        int numItems = 0;
        if (leaflet != null) {
        	numItems = leaflet.getNumItems();
        }
        request.setAttribute(B2cConstants.NUM_LEAFLET_ITEMS, new Integer(numItems));

        forwardTo = "displayMiniBasket";
		log.exiting();
        return mapping.findForward(forwardTo);
    }
}