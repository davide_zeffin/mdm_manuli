/*****************************************************************************
  Class:        MaintainBasketB2CCheckoutAction
  Copyright (c) 2001, SAP AG, All rights reserved.
  Author:
  Created:      20.09.2001
  Version:      1.0

  $Revision: #7 $
  $Date: 2002/10/21 $
*****************************************************************************/

package com.sap.isa.isacore.action.b2c.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.businessobject.order.Basket;
import com.sap.isa.businessobject.order.Leaflet;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreInitAction;
import com.sap.isa.isacore.action.b2c.B2cConstants;

/**
 * Action to continue to the checkout.
 * <p>
 * The following forwards are used by this action:
 * </p>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>error</td><td>something went wrong</td></tr>
 *   <tr><td>checkout</td><td>the user is logged in and can continue to the checkout</td></tr>
 * </table>
 *
 * @author SAP AG
 * @version 1.0
 */
public class MaintainBasketB2CCheckoutAction extends MaintainBasketB2CBaseAction {

    public void initialize() {
        this.checkUserIsLoggedIn = false;
    }
        
    public MaintainBasketB2CCheckoutAction() {
    }

    protected String basketPerform(
                HttpServletRequest request,
                HttpServletResponse response,
                HttpSession session,
                UserSessionData userSessionData,
                RequestParser parser,
                BusinessObjectManager bom,
                IsaLocation log,
                IsaCoreInitAction.StartupParameter startupParameter,
                boolean multipleInvocation,
                boolean browserBack,
                Shop shop,
                Basket basket,
                Leaflet leaflet)
                    throws CommunicationException {

		final String METHOD_NAME = "basketPerform()";
		log.entering(METHOD_NAME);
        String forwardTo = null;
		
		User user = bom.getUser();
		
		// at leat one item in the basket ?
		if (basket.getItems().size() < 1) {
           forwardTo = "basketEmpty";
        }
        // if already logged on, create order
        else if (user != null && user.isUserLogged()) {
            if (shop.isPartnerBasket()) { // are we are in the channel commerce hub scenario ?
                forwardTo = "ordersplit";
            }
            else {
                forwardTo = createOrder(request, shop, basket, bom, log, userSessionData);
            }
        }
        else {
            userSessionData.setAttribute(B2cConstants.FRAME_BEFORE_LOGIN,"checkout");
            forwardTo = "login";
        }
		log.exiting();
        return forwardTo;
    }
}