/*****************************************************************************
    Class         DisplayLeafletB2CAction
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Displays the leaflet (B2C scenario)
    Author:       Sabine Heider
    Created:      06.04.2001
    Version:      1.0
*****************************************************************************/
package com.sap.isa.isacore.action.b2c.order;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.ProductMap;
import com.sap.isa.businessobject.order.Leaflet;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.action.b2c.B2cConstants;

/**
 * Displays the leaflet.
 * If the leaflet doesn't contain any items, a message is displayed instead.
 *
 * @author Sabine Heider
 * @version 1.0
 *
 */
public class DisplayLeafletB2CAction extends IsaCoreBaseAction {

	/**
	 * Initialize the action.
	 * In B2C this action could be used by anonymous user.
	 * 
	 * @see com.sap.isa.isacore.action.IsaCoreBaseAction#initialize()
	 */
	public void initialize() {
		if (isB2C()) {
			this.checkUserIsLoggedIn = false;
		}	
	}

    /**
     * Reads the items from the <code>Leaflet</code> business object
     * and forwards them to the corresponding JSP. The target must be
     * maintained in the <em>config.xml</em> file using the alias
     * <code>displayLeaflet</code>.
     * <p>
     * If the leaflet doesn't contain any items, the request is forwarded
     * to a page displaying a message instead. This page is maintained in
     * the <em>config.xml</em> file using the alias <code>leafletEmpty</code>.
     * </p><p>
     * The information provided when the item is added to the leaflet from the
     * cookie (at the beginning of the user's session) or the catalog is not
     * sufficient for the display screen. Therefore, the items have to be
     * enhanced with information from the catalog before they are displayed for
     * the first time.
     * </p>
     *
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            BusinessObjectManager bom,
            IsaLocation log) {

        // page to be displayed next
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        String forwardTo = null;

        // ask leaflet from BOM
        Leaflet leaflet = bom.getLeaflet();

        // get enhanced items from leaflet
        ProductMap items = leaflet.readItems();

        // if leaflet is empty, display a leaflet-empty-message
        if (items.size() == 0) {
            forwardTo = "leafletEmpty";
        }
        else {
            // store item list in request context
            request.setAttribute(B2cConstants.RK_LEAFLET_ITEM_LIST, items);
            forwardTo = "displayLeaflet";
        }
		log.exiting();
        return mapping.findForward(forwardTo);
    }

}