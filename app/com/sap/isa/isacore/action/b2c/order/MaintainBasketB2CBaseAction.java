/**
 *  Class: MaintainBasketB2CBaseAction
 *  Copyright (c) 2001, SAP AG, All rights reserved.
 *  Author:  SAP AG
 *  Created: 10.09.2001
 *  Version: 1.0
 *
 *  $Revision: #18 $
 *  $Date: 2003/03/13 $
 */

package com.sap.isa.isacore.action.b2c.order;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.ecommerce.businessobject.loyalty.LoyaltyMembership;
import com.sap.isa.businessobject.Address;
import com.sap.isa.businessobject.BORuntimeException;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.ProductList;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.ShipTo;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.item.ItemList;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.businessobject.order.Basket;
import com.sap.isa.businessobject.order.Leaflet;
import com.sap.isa.businessobject.order.Order;
import com.sap.isa.businessobject.order.OrderCreate;
import com.sap.isa.businessobject.order.PartnerList;
import com.sap.isa.businessobject.order.PartnerListEntry;
import com.sap.isa.businessobject.order.TechnicalProperty;
import com.sap.isa.businessobject.order.TechnicalPropertyGroup;
import com.sap.isa.businessobject.ordertemplate.OrderTemplateCreate;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.businesspartner.businessobject.PartnerFunctionBase;
import com.sap.isa.businesspartner.businessobject.SoldFrom;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.JspUtil;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.MessageList;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.MessageDisplayer;
import com.sap.isa.isacore.ShipToSelection;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.action.IsaCoreInitAction;
import com.sap.isa.isacore.action.b2c.B2cConstants;
import com.sap.isa.isacore.action.campaign.AddCampaignToCatalogueAction;
import com.sap.isa.isacore.action.campaign.GetCampaignAction;
import com.sap.isa.isacore.action.loyalty.LoyaltyUtilities;

/**
 *  Base action for all actions providing functionality to manage the shopping
 *  basket. <br>
 *  <p>
 *
 *  This action implements a template pattern, to keep the specialized actions
 *  as simple as possible. All necessary data is retrieved by this class and
 *  offered to the subclasses using a template design pattern. <br>
 *  Subclasses are supposed to overwrite the abstract <code>basketPerform
 *  </code>method and they normally do not touch the <code>isaPerform</code>
 *  method. <br>
 *  <br>
 *  </p><p>
 *
 *  The following forwards are used by this action:</p>
 *  <table border="1" cellspacing="0" cellpadding="2">
 *
 *    <tr>
 *
 *      <td>
 *        <b>forward</b>
 *      </td>
 *
 *      <td>
 *        <b>description</b>
 *      </td>
 *
 *      <tr>
 *
 *        <tr>
 *
 *          <td>
 *            basketEmpty
 *          </td>
 *
 *          <td>
 *            forward used if something goes wrong to allow application to reach
 *            an consitent state again
 *          </td>
 *
 *        </tr>
 *
 *      </table>
 *
 *
 *@author
 *@created    19. M�rz 2002
 *@version    1.0
 */
public abstract class MaintainBasketB2CBaseAction extends IsaCoreBaseAction {

    // session attributes, to store information for basket split in the channel commerce hub scenario
    public static String SOLDFROMLIST =
        "com.sap.isa.isacore.action.b2c.order.MaintainBasketB2COrderSplitAction.soldfromlist";
    public static String SOLDFROMLIST_IDX =
        "com.sap.isa.isacore.action.b2c.order.MaintainBasketB2COrderSplitAction.soldfromlistidx";

    // session attribute, to store information for basket forwarding in the channel commerce hub scenario
    public static String SOLDFROMLISTOCI =
        "com.sap.isa.isacore.action.b2c.order.MaintainBasketB2COrderSplitOciAction.soldfromlistoci";

    // messages of the template, that is created, when the basket is saved 
    public static String RC_SAVEORDERMESSAGES = "saveOrderMessages";

    /**
     * Set to "X" if the order object should be be released from the BOM
     */
    // To allow customers passing extension data to the order at the
    // time of document creation (e.g. in CRM this is CRM_ISA_BASKET_CREATE)
    public static String RC_DONTRELEASEORDER = "dontreleaseorder"; // INS 903622

    /**
     *  Constructor for the MaintainBasketB2CBaseAction object
     */
    public MaintainBasketB2CBaseAction() {
    }
    

    /**
     *  Description of the Method
     *
     *@param  mapping                     Description of Parameter
     *@param  form                        Description of Parameter
     *@param  request                     Description of Parameter
     *@param  response                    Description of Parameter
     *@param  userSessionData             Description of Parameter
     *@param  requestParser               Description of Parameter
     *@param  bom                         Description of Parameter
     *@param  log                         Description of Parameter
     *@param  startupParameter            Description of Parameter
     *@param  eventHandler                Description of Parameter
     *@param  multipleInvocation          Description of Parameter
     *@param  browserBack                 Description of Parameter
     *@return                             Description of the Returned Value
     *@exception  CommunicationException  Description of Exception
     */
    public ActionForward isaPerform(
        ActionMapping mapping,
        ActionForm form,
        HttpServletRequest request,
        HttpServletResponse response,
        UserSessionData userSessionData,
        RequestParser requestParser,
        BusinessObjectManager bom,
        IsaLocation log,
        IsaCoreInitAction.StartupParameter startupParameter,
        BusinessEventHandler eventHandler,
        boolean multipleInvocation,
        boolean browserBack)
        throws CommunicationException {

        final String METHOD_NAME = "isaPerform()";
        log.entering(METHOD_NAME);
        // Page that should be displayed next.
        String forwardTo = null;

        // get business objects
        Basket basket = bom.getBasket();

        Shop shop = bom.getShop();
        Leaflet leaflet = bom.getLeaflet();

        HttpSession session = request.getSession();

        forwardTo =
            basketPerform(
                    request,
                    response,
                    session,
                    userSessionData,
                    requestParser,
                    bom,
                    log,
                    startupParameter,
                    multipleInvocation,
                    browserBack,
                    shop,
                    basket,
                    leaflet);

        log.exiting();
        return mapping.findForward(forwardTo);
    }

    /**
     *  Fills the basket business object with the data provided in the request.
     *  Note that this doesn't add the items on the backend.
     *
     *@param  parser  Description of Parameter
     *@param  basket  Description of Parameter
     *@param  log     Description of Parameter
     */
    protected void parseHeader(RequestParser parser, Basket basket, IsaLocation log) {

        RequestParser.Parameter description = parser.getParameter("description");

        //RequestParser.Parameter customerNumber =
        //        parser.getParameter("customerNumber");

        // get the businesspartner fields from the request
        RequestParser.Parameter businessPartnerRole = parser.getParameter("businessPartnerRole[]");

        RequestParser.Parameter businessPartnerTechKey = parser.getParameter("businessPartnerTechKey[]");

        RequestParser.Parameter businessPartnerId = parser.getParameter("businessPartnerId[]");

        RequestParser.Parameter deliveryPriority = parser.getParameter("deliveryPriority");

        RequestParser.Parameter headCampId = parser.getParameter("headCampId");

		RequestParser.Parameter reqDeliveryDate = parser.getParameter("headerReqDeliveryDate");
        
        RequestParser.Parameter contractStartDate = parser.getParameter("contractStartDate");
        
        if (!basket.getAuctionBasket()) {
            basket.clearHeader();
        }

        // create header information and assign header to basket
        HeaderSalesDocument header = new HeaderSalesDocument();
        header.setDescription(JspUtil.replaceSpecialCharacters(description.getValue().getString()));
        header.setShipCond("");
        if (deliveryPriority.getValue().getString().length() > 0) {
            header.setDeliveryPriority(deliveryPriority.getValue().getString());
        }

        PartnerList bpList = header.getPartnerList();

        for (int i = 1; i <= businessPartnerRole.getNumValues(); i++) {
            PartnerListEntry bpListEntry =
                new PartnerListEntry(
                    new TechKey(businessPartnerTechKey.getValue(i).getString()),
                    businessPartnerId.getValue(i).getString());
            bpList.setPartner(businessPartnerRole.getValue(i).getString(), bpListEntry);
        }

        if (headCampId != null && headCampId.getValue().getString().length() > 0) {
            header.getAssignedCampaigns().addCampaign(headCampId.getValue().getString(), TechKey.EMPTY_KEY, "", false, true);
        }

		if (reqDeliveryDate != null && reqDeliveryDate.getValue().getString().length() > 0) {
			header.setReqDeliveryDate(reqDeliveryDate.getValue().getString());
		}
        
        if (contractStartDate != null && contractStartDate.getValue().getString().length() > 0) {
            header.setContractStartDate(contractStartDate.getValue().getString());
        }
				     
        if (!basket.getAuctionBasket()) {
            basket.setHeader(header);
        }
    }

    /**
     *  Fills the basket business object with the data provided in the request.
     *  Note that this doesn't add the items on the backend.
     *
     *@param  parser  Description of Parameter
     *@param  basket  Description of Parameter
     *@param  log     Description of Parameter
     */
    protected void parseItems(RequestParser parser, Basket basket, IsaLocation log) {

        //        String parentId;
        PartnerList bpList;

        RequestParser.Parameter techKey = parser.getParameter("techKey[]");
        RequestParser.Parameter pcat = parser.getParameter("pcat[]");
		RequestParser.Parameter pcatVariant = parser.getParameter("pcatVariant[]");
		RequestParser.Parameter pcatArea = parser.getParameter("pcatArea[]");
        //        RequestParser.Parameter contractKey =
        //                parser.getParameter("contractKey[]");
        //        RequestParser.Parameter contractItemKey =
        //                parser.getParameter("contractItemKey[]");
        //        RequestParser.Parameter ipcDocumentId =
        //                parser.getParameter("ipcDocumentId[]");
        //        RequestParser.Parameter ipcItemId =
        //                parser.getParameter("ipcItemId[]");
        RequestParser.Parameter productId = parser.getParameter("productId[]");
        RequestParser.Parameter quantity = parser.getParameter("quantity[]");
        RequestParser.Parameter oldQuantity = parser.getParameter("oldquantity[]");
        RequestParser.Parameter unit = parser.getParameter("unit[]");
        RequestParser.Parameter product = parser.getParameter("product[]");
        //        RequestParser.Parameter description =
        //                parser.getParameter("description[]");
        RequestParser.Parameter parent = parser.getParameter("parentId[]");
		RequestParser.Parameter campId = parser.getParameter("campId[]");
        RequestParser.Parameter deliveryPriority = parser.getParameter("deliveryPriority[]");

        RequestParser.Parameter contractDuration = parser.getParameter("contractDuration[]");
        RequestParser.Parameter contractDurationUnit = parser.getParameter("contractDurationUnit[]");
        RequestParser.Parameter auctionGuid = parser.getParameter("auctionGuid[]");
        
        // items businesspartner fields from the request
        RequestParser.Parameter itemBusinessPartnerRole;
        RequestParser.Parameter itemBusinessPartnerTechKey;
        RequestParser.Parameter itemBusinessPartnerId;

        //Technical property Fields from the request
        RequestParser.Parameter itemTechPropertyCompGuid;
        RequestParser.Parameter itemTechPropertyAttrId;
        RequestParser.Parameter itemTechPropertyValue;
        RequestParser.Parameter itemTechPropertyDataType;

        basket.clearItems();

        for (int i = 0; i < techKey.getNumValues(); i++) {
            if (techKey.getValue(i).getString().length() > 0) {
                ItemSalesDoc itm = new ItemSalesDoc();
                itm.setTechKey(new TechKey(techKey.getValue(i).getString()));
                itm.setChangeable(true);
                itm.setProductId(new TechKey(productId.getValue(i).getString()));
                itm.setQuantity(quantity.getValue(i).getString());
                itm.setOldQuantity(oldQuantity.getValue(i).getString());
                itm.setUnit(unit.getValue(i).getString());
                itm.setProduct(product.getValue(i).getString());
                itm.setPcat(new TechKey(pcat.getValue(i).getString()));
				itm.setPcatVariant(pcatVariant.getValue(i).getString());
				itm.setPcatArea(pcatArea.getValue(i).getString());
                //                itm.setContractKey(new TechKey(contractKey.getValue(i).getString()));
                //                itm.setContractItemKey(new TechKey(contractItemKey.getValue(i).getString()));

                // set delivery priority
                itm.setDeliveryPriority(deliveryPriority.getValue(i).getString());
                
                itm.setContractDuration(contractDuration.getValue(i).getString());
                itm.setContractDurationUnit(contractDurationUnit.getValue(i).getString());
                
				if (campId.getValue(i).getString().length() > 0) {
					itm.getAssignedCampaigns().addCampaign(campId.getValue(i).getString(), new TechKey(""), "", false, true);
				}
                
                if (auctionGuid.getValue(i) != null && auctionGuid.getValue(i).getString().length() > 0) {
                    itm.setAuctionGuid(new TechKey(auctionGuid.getValue(i).getString()));
                }
                
                ShipTo shipTo = new ShipTo();
                shipTo.setTechKey(TechKey.EMPTY_KEY);
                itm.setShipTo(shipTo);
                if (parent.getValue(i).getString().equals("")) {
                    itm.setParentId(new TechKey(null));
                }
                else {
                    itm.setParentId(new TechKey(parent.getValue(i).getString()));
                }
                //determine items businesspartners from the request
                itemBusinessPartnerRole = parser.getParameter("itemBusinessPartnerRole[" + i + "][]");
                itemBusinessPartnerTechKey = parser.getParameter("itemBusinessPartnerTechKey[" + i + "][]");
                itemBusinessPartnerId = parser.getParameter("itemBusinessPartnerId[" + i + "][]");

                bpList = itm.getPartnerList();

                for (int j = 0; j < itemBusinessPartnerRole.getNumValues(); j++) {
                    PartnerListEntry bpListEntry =
                        new PartnerListEntry(
                            new TechKey(itemBusinessPartnerTechKey.getValue(j).getString()),
                            itemBusinessPartnerId.getValue(j).getString());
                    bpList.setPartner(itemBusinessPartnerRole.getValue(j).getString(), bpListEntry);
                }

                // determine technicalProperties from the request
                itemTechPropertyCompGuid = parser.getParameter("itemTechPropertyCompGuid[" + i + "][]");
                itemTechPropertyAttrId = parser.getParameter("itemTechPropertyAttrId[" + i + "][]");
                itemTechPropertyValue = parser.getParameter("itemTechPropertyValue[" + i + "][]");
                itemTechPropertyDataType = parser.getParameter("itemTechPropertyDataType[" + i + "][]");

                TechnicalPropertyGroup propGroup = new TechnicalPropertyGroup();

                //			  get all technical Properties for the current item
                for (int j = 0; j < itemTechPropertyCompGuid.getNumValues(); j++) {

                    TechnicalProperty prop = new TechnicalProperty();

                    prop.setAttributeGuid(itemTechPropertyCompGuid.getValue(j).getString());
                    prop.setAttributeId(itemTechPropertyAttrId.getValue(j).getString());
                    prop.setValue(itemTechPropertyValue.getValue(j).getString());
                    prop.setType(itemTechPropertyDataType.getValue(j).getString());

                    propGroup.addProperty(prop);
                }

                itm.setAssignedTechnicalProperties(propGroup);

              //adopt request delivery date from header
              itm.setReqDeliveryDate(basket.getHeader().getReqDeliveryDate());

                // delete Items that have quantity 0 and are main positions
                if (!parent.getValue(i).getString().equals("") || !itm.getQuantity().equals("0")) {

                    basket.addItem(itm);
                }
                else {
                    try {
                        basket.removeItem(itm);
                    }
                    catch (CommunicationException excp) {
                        log.debug(excp.toString());
                    }
                }
            }
        }
    }

    /**
     *  Fills the basket business object with the data provided in the request.
     *  Note that this doesn't add the items on the backend.
     *
     *@param  parser  Description of Parameter
     *@param  basket  Description of Parameter
     *@param  log     Description of Parameter
     */
    protected void parse(RequestParser parser, Basket basket, IsaLocation log) {

        basket.clearMessages();

        parseHeader(parser, basket, log);
        parseItems(parser, basket, log);
    }

    /**
     *  Performs the necessary steps to display the basket.
     *
     *@param  request                     Description of Parameter
     *@param  shop                        Description of Parameter
     *@param  basket                      Description of Parameter
     *@param  bom                         Description of Parameter
     *@param  leaflet                     Description of Parameter
     *@param  log                         Description of Parameter
     *@param  userSessionData             Description of Parameter
     *@return                             Page that should be displayed next.
     *@exception  CommunicationException  Description of Exception
     */
    protected String displayBasket(
        HttpServletRequest request,
        Shop shop,
        Basket basket,
        BusinessObjectManager bom,
        Leaflet leaflet,
        IsaLocation log,
        UserSessionData userSessionData)
        throws CommunicationException {

        String forwardTo = null;

        if (basket == null) {
            return "basketEmpty";
        }

        WebCatInfo catalog = getCatalogBusinessObjectManager(userSessionData).getCatalog();

        // get items in the basket
        basket.readHeader();
        // may throw CommunicationException
        basket.readAllItems();
        // may throw CommunicationException

        /*
         * if (user.isUserLogged()) {
         * basket.readAllItems(user);      // may throw CommunicationException
         * }
         * else {
         * basket.readAllItems();
         * }
         */
        ItemList items = basket.getItems();

        // if basket is empty, display a basket-empty-message
        if (items.size() == 0 && !shop.isSaveBasketAllowed()) {
            forwardTo = "basketEmpty";
        }
        else {
            // Get the product information to add it to the item list
            ProductList.enhance(catalog, items.toArray());

            // store catalog in request context
            request.setAttribute(B2cConstants.CATALOG, catalog);

            // store item list in request context
            request.setAttribute(B2cConstants.RK_BASKET, basket);

            // store item list in request context
            request.setAttribute(B2cConstants.RK_BASKET_ITEM_LIST, items);

            // store header in request context
            request.setAttribute(B2cConstants.RK_BASKET_HEADER, basket.getHeader());

            // store number of leaflet items in request context
            request.setAttribute(B2cConstants.NUM_LEAFLET_ITEMS, new Integer(leaflet.getNumItems()));

            // store all availabilty symbols in request context, if we are in a CCH scenario
            // and the availabilty infos are displayed
            if (shop.isPartnerBasket() && shop.isPartnerAvailabilityInfoAvailable()) {
                request.setAttribute(B2cConstants.RK_CCH_TRAFFICLIGHTS, shop.getTrafficLights());
            }

            // set the campaign Id in the catalog
            if (shop.isManualCampainEntryAllowed() && !shop.isSoldtoSelectable()) {
                if (basket.getHeader().getAssignedCampaigns() != null
                    && basket.getHeader().getAssignedCampaigns().size() > 0) {

                    if (shop.isInternalCatalogAvailable() && catalog == null) {
                        throw (new IllegalArgumentException("Paramter catalog can not be null!"));
                    }

                    // we have to set the Id in the catalog
                    GetCampaignAction.readCampaign(
                        basket.getHeader().getAssignedCampaigns().getCampaign(0).getCampaignId(),
                        userSessionData,
                        request,
                        log);
                    AddCampaignToCatalogueAction.addCampaignToCatalogue(catalog, userSessionData, bom, log);
                }
            }

            forwardTo = "displayBasket";
        }

        return forwardTo;
    }

    /**
     *  Performs the necessary steps to create an order from the basket.
     *
     *@param  request                     Description of Parameter
     *@param  shop                        Description of Parameter
     *@param  basket                      Description of Parameter
     *@param  bom                         Description of Parameter
     *@param  log                         Description of Parameter
     *@param  userSessionData             Description of Parameter
     *@return                             Page that should be displayed next.
     *@exception  CommunicationException  Description of Exception
     */
    protected String createOrder(
        HttpServletRequest request,
        Shop shop,
        Basket basket,
        BusinessObjectManager bom,
        IsaLocation log,
        UserSessionData userSessionData)
        throws CommunicationException {

        String forwardTo = null;
        boolean isDebugEnabled = log.isDebugEnabled();

		LoyaltyMembership loyMemShip = LoyaltyUtilities.getLoyaltyMembership(userSessionData);                
		basket.getHeader().setMemberShip(loyMemShip);

        // clear the Ship-Tos list because the list will be read via basket.readAllItems() again
        basket.clearShipTos();
        // additionally reset the default Ship-To because it will be set in basket.readAllItems() as well
        basket.getHeader().setShipTo(null);
        
        // get items in the basket
        basket.readAllItems();

        // set point code 
        if (loyMemShip != null && loyMemShip.exists() && loyMemShip.getPointAccount() != null) {
            ItemList items = basket.getItems();   
            for (int i=0; i < items.size(); i++) {
                ItemSalesDoc item = items.get(i);
                item.setLoyPointCodeId(loyMemShip.getPointAccount().getPointCodeId());
            }
        }

        // if an old order exists, release it
        if (!"X".equals(request.getParameter(RC_DONTRELEASEORDER))) { // INS 903622
			//RELEASE ORDER IN BACKEND
			//NOTE 1268984
			//START==>
			Order currentBAOrder = bom.getOrder();
			if( currentBAOrder != null )
				{if( log.isDebugEnabled() )
					log.debug("BackendOrderObject "+currentBAOrder.getTechKey()+" will be destroyed.");
					currentBAOrder.destroyContent();
				}
			//<==END
            bom.releaseOrder();
        } // INS 903622
        // create order
        OrderCreate order = new OrderCreate(bom.createOrder());

        // read in web-xml: location of basket
        // ServletContext ctx = getServlet().getServletConfig().getServletContext();
        // String basketloc = ctx.getInitParameter("basketlocation.core.isa.sap.com");

        // External preordersalesdocument or not
        if (basket.isExternalToOrder()) {
            // if (basketloc.equalsIgnoreCase("IPC")) {
            // create order in backend

            // read catalog
            WebCatInfo webCat = getCatalogBusinessObjectManager(userSessionData).getCatalog();
            
            if (shop.getAuctionBasketHelper() != null && isAuctionItemInBasket(basket)) {
                shop.getAuctionBasketHelper().setInvocationContext(userSessionData);
            }

            try {
                order.createFromExternalBasket(basket, shop, bom.createBUPAManager(), webCat);
            } catch (BORuntimeException BORex) {
                // Fatal error -> show error page with thrown messages
                createErrorMessage(request, "", "", (order.getSalesDocument()) );
                return "error";
            }
                        
            // Use Ship-To which has been stored in the session (in case the ship-to has been changed before)
            Address shipToAddress = (Address) request.getSession().getAttribute(B2cConstants.SHIP_TO_ADDRESS);
            if (shipToAddress != null)
            {
            	BusinessPartnerManager buPaMa = bom.createBUPAManager();
            	BusinessPartner soldTo = buPaMa.getDefaultBusinessPartner(PartnerFunctionData.SOLDTO);
            	order.getSalesDocument().addNewShipTo(shipToAddress, soldTo.getTechKey(), shop.getTechKey());
            }

            if (shop.getAuctionBasketHelper() != null && isAuctionItemInBasket(basket)) {
                shop.getAuctionBasketHelper().releaseInvocationContext();
            }
            
            if (isDebugEnabled) {
                log.debug("Call createFromExternalBasket");
            }
        }
        else {
            // fill order with basket data
            order.copyFromDocument(basket);
            
            // Use Ship-To which has been stored in the session (in case the ship-to has been changed before)
            Address shipToAddress = (Address) request.getSession().getAttribute(B2cConstants.SHIP_TO_ADDRESS);
            if (shipToAddress != null)
            {
            	BusinessPartnerManager buPaMa = bom.createBUPAManager();
            	BusinessPartner soldTo = buPaMa.getDefaultBusinessPartner(PartnerFunctionData.SOLDTO);
            	order.getSalesDocument().addNewShipTo(shipToAddress, soldTo.getTechKey(), shop.getTechKey());
            }

            // update order in the backend
            order.update(bom.createBUPAManager(), shop);
            // may throw CommunicationException
            if (isDebugEnabled) {
                log.debug("Call copyFromDocument");
            }
        }

        ShipToSelection shipToSelection = new ShipToSelection(bom.getOrder().getShipToList());

        shipToSelection.setShippingRecipient(SoldFrom.SHIPTO_SOLDTO);

        ShipTo shipTo = bom.getOrder().getHeader().getShipTo();
        if (shipTo != null) {
            shipToSelection.setActualShipToKey(shipTo.getTechKey());
        }

        request.getSession().setAttribute(B2cConstants.SC_SHIPTO_SELECTION, shipToSelection);

        //	if simulation is needed, call it now
        if (shop.isOrderSimulateAvailable()) {
            if (log.isDebugEnabled()) {
                log.debug("order simulate called");
            }
            bom.getOrder().simulate();
        }

        forwardTo = "checkout";

        return forwardTo;
    }

    /**
     *  Performs the necessary steps to create an order from the basket for
     *  all items of a soldFrom
     *
     *@param  request                     Description of Parameter
     *@param  shop                        Description of Parameter
     *@param  basket                      Description of Parameter
     *@param  soldFromTechKey             TechKey of the soldFrom partner
     *@param  soldFromId                  Id of the soldFrom partner
     *@param  bom                         Description of Parameter
     *@param  log                         Description of Parameter
     *@param  userSessionData             Description of Parameter
     *@return                             Page that should be displayed next.
     *@exception  CommunicationException  Description of Exception
     */
    protected String createOrderForSoldFrom(
        HttpServletRequest request,
        Shop shop,
        Basket basket,
        TechKey soldFromTechKey,
        String soldFromId,
        BusinessObjectManager bom,
        IsaLocation log,
        UserSessionData userSessionData)
        throws CommunicationException {

        String forwardTo = null;

        // get items in the basket
        basket.readAllItems();
        // may throw CommunicationException

        // if an old order exists, release it
        bom.releaseOrder();

        // create order
        Order newOrder = bom.createOrder();
        newOrder.setCustomerDocument(true);
        OrderCreate order = new OrderCreate(newOrder);

        BusinessPartnerManager bupama = bom.createBUPAManager();

        // check if there is a valid relationship between the reseller and the soldto, if not
        // try to create one
        //BusinessPartner soldToBuPa = bupama.getBusinessPartner(soldToEntry.getPartnerTechKey());
        BusinessPartner soldFromPartner = bupama.getBusinessPartner(soldFromTechKey);

        // read catalog
        WebCatInfo webCat = getCatalogBusinessObjectManager(userSessionData).getCatalog();

        order.createFromBasketForSoldFrom(basket, shop, soldFromTechKey, soldFromId, bupama, webCat);
        //order.createFromExternalBasket(basket, shop, user, bom.createBUPAManager(), webCat);

        // update order in the backend
        order.update(bupama, shop);

        // Handling the shipTo information

        // First get the info's about the soldfrom
        SoldFrom soldFrom = (SoldFrom) soldFromPartner.getPartnerFunctionObject(PartnerFunctionBase.SOLDFROM);
        if (soldFrom == null) {
            soldFrom = new SoldFrom();
            soldFromPartner.addPartnerFunction(soldFrom);
            soldFrom.readData(soldFromPartner.getTechKey(), shop.getLanguage(), null, null, null);
        }

        // create a shipTo selection object
        ShipToSelection shipToSelection = new ShipToSelection(bom.getOrder().getShipToList());

        ShipTo shipTo = bom.getOrder().getHeader().getShipTo();
        if (shipTo != null && shipTo.getTechKey() != null) {
            shipToSelection.setActualShipToKey(shipTo.getTechKey());
        }

        String shippingRecipient = soldFrom.getShipRecipient();

        if (!shippingRecipient.equals(SoldFrom.SHIPTO_SOLDTO)) { // no shipping only to end customer

            // add shipto to selection
            shipToSelection.setSalesPartner(soldFromPartner);

            // add sales partner as ship to if neccesary
            if (!shipToSelection.isPartnerAvailable()) {
                ShipToSelection.addPartnerToShipTos(bom.getOrder(), soldFromPartner);
            }
        }

        shipToSelection.setShippingRecipient(shippingRecipient);

        request.getSession().setAttribute(B2cConstants.SC_SHIPTO_SELECTION, shipToSelection);

        forwardTo = "checkout";

        return forwardTo;
    }

    /**
     *  Performs the necessary steps to create an order template from the basket.
     *
     *@param  request                     Description of Parameter
     *@param  response                    current HTTP response
     *@param  user                        Description of Parameter
     *@param  shop                        Description of Parameter
     *@param  basket                      Description of Parameter
     *@param  bom                         Description of Parameter
     *@param  log                         Description of Parameter
     *@param  userSessionData             Description of Parameter
     *@return                             Page that should be displayed next.
     *@exception  CommunicationException  Description of Exception
     */
    protected String createOrderTemplate(
        HttpServletRequest request,
        HttpServletResponse response,
        Shop shop,
        Basket basket,
        BusinessObjectManager bom,
        IsaLocation log,
        UserSessionData userSessionData)
        throws CommunicationException {

        String forwardTo = null;

        // get items in the basket
        basket.readAllItems();
        // may throw CommunicationException

        bom.releaseOrderTemplate();

        // create order
        OrderTemplateCreate orderTmpl = new OrderTemplateCreate(bom.createOrderTemplate());

        // read catalog
        WebCatInfo webCat = getCatalogBusinessObjectManager(userSessionData).getCatalog();

        // orderTemplate is treated as copy of an external basket:
        // otherwise you wont have a new representation(guid) in backend
        orderTmpl.createFromExternalBasket(basket, shop, bom.createBUPAManager(), webCat);
        // read the itemList, to prevent doubling of items by later update
        orderTmpl.readAllItems();

        // add description
        String description = JspUtil.replaceSpecialCharacters( (String) request.getParameter("description"));

        if (description == null || description.length() == 0) {
            // look up in the client's session, may be we will find something ...
            description = JspUtil.replaceSpecialCharacters((String) request.getSession().getAttribute(B2cConstants.SAVE_BASKET_DESCRIPTION));
            if (description == null || description.length() == 0) {
                // no, we have not found anything
                description = "";
            }
            else {
                // remove the save basket description from the client's session
                request.getSession().removeAttribute(B2cConstants.SAVE_BASKET_DESCRIPTION);
                // set the save basket description into the request
                request.setAttribute(B2cConstants.SAVE_BASKET_DESCRIPTION, description);
            }
        }
        else {
            // set the save basket description into the request
            request.setAttribute(B2cConstants.SAVE_BASKET_DESCRIPTION, description);
        }

        // set the description of the orderTemplate
        orderTmpl.getHeader().setDescription(description);

        // update order template in the backend:
        // description has to be added
        orderTmpl.update(bom.createBUPAManager());
        // may throw CommunicationException

        // save order template
        orderTmpl.saveOrderTemplateCreate();

        // get order number
        orderTmpl.readOrderTemplateHeader();

        MessageList orderTmplMessageList = orderTmpl.getMessageList();
        String orderNumber = orderTmpl.getSalesDocNumber();

        if (orderNumber != null && orderNumber.trim().length() > 0 && !orderNumber.trim().equals("0")) {
        	orderTmplMessageList.add(new Message(Message.INFO, "b2c.order.basket.message.saved", new String[] {orderNumber}, ""));
        }
        else {
        	orderTmplMessageList.add(new Message(Message.ERROR, "b2c.order.basket.msg.notsa"));
        }

		// we must set it into session because before basket.inc.jsp will be shown, a redirect will 
		// be performed and then request data would get lost
        request.getSession().setAttribute(RC_SAVEORDERMESSAGES, orderTmplMessageList);
        
        forwardTo = "basket";

        return forwardTo;
    }
    
    protected boolean isAuctionItemInBasket(Basket basket) {
        
        boolean retVal = false;
        
        ItemList items = basket.getItems();
        
        if (items != null) {
            for (int i = 0; i < items.size(); i++) {
                if (items.get(i).hasAuctionRelation()) {
                    retVal = true;
                    break;
                }
            }
        }
        
        if (log.isDebugEnabled()) {
            log.debug("Check if auction related items are in the basket = " + retVal);
        }
        
        return retVal;
    }

    /**
     * This method calls the parse and update method.
     * Additionaly it will be check if the campaign Id has been 
     * modified. If so, we set the Id in the catalog.
     * 
     * @param parser					Description of Parameter
     * @param shop						Description of Parameter
     * @param basket					Description of Parameter
     * @param bom						Description of Parameter
     * @param log						Description of Parameter
     * @param userSessionData			Description of Parameter
     * @throws CommunicationException	Description of Exception
     */
    protected void updateBasket(
        RequestParser parser,
        Shop shop,
        Basket basket,
        BusinessObjectManager bom,
        IsaLocation log,
        UserSessionData userSessionData,
        HttpServletRequest request)
        throws CommunicationException {

        String oldCampaign = "";
        String newCampaign = "";
        // read the old campaign Id
        if (shop.isManualCampainEntryAllowed() && !shop.isSoldtoSelectable()) {
            if (basket.getHeader().getAssignedCampaigns() != null && basket.getHeader().getAssignedCampaigns().size() > 0) {
                oldCampaign = basket.getHeader().getAssignedCampaigns().getCampaign(0).getCampaignId();
            }
        }

        // update basket with request data
        parse(parser, basket, log);
        
		LoyaltyMembership loyMemShip = LoyaltyUtilities.getLoyaltyMembership(userSessionData);    
		basket.getHeader().setMemberShip(loyMemShip);
        if (loyMemShip != null && loyMemShip.exists() && loyMemShip.getPointAccount() != null) {
            ItemList items = basket.getItems();   
            for (int i=0; i < items.size(); i++) {
                ItemSalesDoc item = items.get(i);
                item.setLoyPointCodeId(loyMemShip.getPointAccount().getPointCodeId());
            }
        }

        // read the new campaign Id
        if (shop.isManualCampainEntryAllowed() && !shop.isSoldtoSelectable()) {
            if (basket.getHeader().getAssignedCampaigns() != null && basket.getHeader().getAssignedCampaigns().size() > 0) {
                newCampaign = basket.getHeader().getAssignedCampaigns().getCampaign(0).getCampaignId();
            }

            // check if the Id has changed
            if (newCampaign != null && !newCampaign.trim().equals(oldCampaign.trim())) {

                // get the catalog
                WebCatInfo webCat = getCatalogBusinessObjectManager(userSessionData).getCatalog();

                if (shop.isInternalCatalogAvailable() && webCat == null) {
                    throw (new IllegalArgumentException("Parameter webCat can not be null!"));
                }

                // we have to set the Id in the catalog
                GetCampaignAction.readCampaign(newCampaign, userSessionData, request, log);
                AddCampaignToCatalogueAction.addCampaignToCatalogue(webCat, userSessionData, bom, log);
            }
        }

        // may throw CommunicationException
        basket.update(bom.createBUPAManager());

    }
    
    /**
     * Sets the error messages from the sales document into the request. Be aware that the forward to the message page
     * must be set separately!
     * @param request             HTTP request
     * @param nextAction          next Struts action which should be called after message page
     * @param nextActionParameter Parameters for the next Struts action
     * @param salesDocument       the involved sales document
     */
    static protected void createErrorMessage(
        HttpServletRequest request,
        String nextAction,
        String nextActionParameter,
        SalesDocument salesDocument) {
  
        // create a Message Displayer for the error page!
        MessageDisplayer messageDisplayer = new MessageDisplayer();
        
        messageDisplayer.addToRequest(request);
        messageDisplayer.copyMessages(salesDocument);
        
        // the action which is to call after the message
        messageDisplayer.setAction(nextAction);
        
        messageDisplayer.setActionParameter(nextActionParameter);
    }

    /**
     *  Description of the Method
     *
     *@param  request                     Description of Parameter
     *@param  response                    Description of Parameter
     *@param  session                     Description of Parameter
     *@param  userSessionData             Description of Parameter
     *@param  parser                      Description of Parameter
     *@param  bom                         Description of Parameter
     *@param  log                         Description of Parameter
     *@param  startupParameter            Description of Parameter
     *@param  multipleInvocation          Description of Parameter
     *@param  browserBack                 Description of Parameter
     *@param  user                        Description of Parameter
     *@param  shop                        Description of Parameter
     *@param  basket                      Description of Parameter
     *@param  leaflet                     Description of Parameter
     *@return                             Description of the Returned Value
     *@exception  CommunicationException  Description of Exception
     */
    protected abstract String basketPerform(
        HttpServletRequest request,
        HttpServletResponse response,
        HttpSession session,
        UserSessionData userSessionData,
        RequestParser parser,
        BusinessObjectManager bom,
        IsaLocation log,
        IsaCoreInitAction.StartupParameter startupParameter,
        boolean multipleInvocation,
        boolean browserBack,
        Shop shop,
        Basket basket,
        Leaflet leaflet)
        throws CommunicationException;

}
