/*****************************************************************************
  Class:        MaintainCheckoutB2CDispatcherAction
  Copyright (c) 2001, SAP AG, All rights reserved.
  Author:       SAP AG
  Created:      25.03.2001
  Version:      1.0

  $Revision: #2 $
  $Date: 2002/11/27 $
*****************************************************************************/

package com.sap.isa.isacore.action.b2c.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.ShipTo;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businessobject.order.Basket;
import com.sap.isa.businessobject.order.Order;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.ShipToSelection;
import com.sap.isa.isacore.action.IsaCoreInitAction.StartupParameter;
import com.sap.isa.isacore.action.b2c.B2cConstants;

/**
 * Action to dispatch all requests for the checkout process to
 * different (specialized) actions. The decision to which action the control
 * is forwarded to depends on the request parameters set.<br><br>
 *
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>Request parameter</b></td>
 *     <td><b>Value of parameter</b></td>
 *     <td><b>Logical forward used</b></td>
 *   <tr>
 *   <tr><td>confirm</td><td>!= ""</td><td>confirm</td><tr>
 *   <tr><td>changeShipTo</td><td>!= ""</td><td>shipto</td></tr>
 *   <tr><td>saveOrder</td><td>!= ""</td><td>saveorder</td></tr>
 *   <tr><td>readOrder</td><td>!= ""</td><td>saveorder</td></tr>
 *   <tr><td colspan="2"><i>none of the above</i></td><td>display</td></tr>
 * </table>
 * <p>
 * According to the described parameters the corresponding forwards are used.
 * </p>
 *
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>confirm</td><td>data was entered and is going to be checked before a confirmation is displayed</td></tr>
 *   <tr><td>shipto</td><td>the shipto is going to be changed</td></tr>
 *   <tr><td>saveorder</td><td>save the order</td></tr>
 *   <tr><td>readorder</td><td>read in an order, that should be displayed for printing</td></tr>
 *   <tr><td>display</td><td>display the checkout information</td></tr>
 * </table>
 *
 *
 * @author SAP AG
 * @version 1.0
 */
public class MaintainCheckoutB2CDispatcherAction extends MaintainCheckoutB2CBaseAction {

    private static final String FORWARD_CONFIRM       = "confirm";
    private static final String FORWARD_CHANGE_SHIPTO = "shipto";
    private static final String FORWARD_SAVEORDER     = "saveorder";
    private static final String FORWARD_READORDER     = "readorder";
    private static final String FORWARD_DISPLAY       = "display";
    private static final String FORWARD_REFRESH_BANK  = "refreshbank";

    public MaintainCheckoutB2CDispatcherAction() {
    }


    /**
     * @see com.sap.isa.isacore.action.b2c.order.MaintainCheckoutB2CBaseAction#checkOutPerform(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, javax.servlet.http.HttpSession, com.sap.isa.core.UserSessionData, com.sap.isa.core.util.RequestParser, com.sap.isa.businessobject.BusinessObjectManager, com.sap.isa.core.logging.IsaLocation, com.sap.isa.isacore.action.IsaCoreInitAction.StartupParameter, boolean, boolean, com.sap.isa.businessobject.businessevent.BusinessEventHandler, com.sap.isa.businessobject.User, com.sap.isa.businessobject.Shop, com.sap.isa.businessobject.order.Basket, com.sap.isa.businessobject.order.Order)
     */
    public String checkOutPerform(
        HttpServletRequest request,
        HttpServletResponse response,
        HttpSession session,
        UserSessionData userSessionData,
        RequestParser parser,
        BusinessObjectManager bom,
        IsaLocation log,
        StartupParameter startupParameter,
        boolean multipleInvocation,
        boolean browserBack,
        BusinessEventHandler eventHandler,
        Shop shop,
        Basket basket,
        Order order)
        	throws CommunicationException {

		final String METHOD_NAME = "checkOutPerform()";
		log.entering(METHOD_NAME);
        boolean isDebugEnabled = log.isDebugEnabled();

        // Page that should be displayed next.
        String forwardTo = null;    

        // parse the selected shipTo		
        RequestParser.Parameter shipTo = parser.getParameter("shipto");
    
        if (shipTo.isSet()) {        
            ShipToSelection shipToSelection = (ShipToSelection)request.getSession().getAttribute(B2cConstants.SC_SHIPTO_SELECTION);
        
        	ShipTo actShipTo = null;
        	
            if (shipTo.getValue().getString().equals("soldto")) {
                actShipTo = shipToSelection.getActualShipTo();    
            } 
            else {
                actShipTo = shipToSelection.getPartnerShipTo();                    
            }
            order.getHeader().setShipTo(actShipTo);                   
        }
   

        // check for possible cases
        RequestParser.Parameter confirm      = parser.getParameter("confirm");
        RequestParser.Parameter changeShipTo = parser.getParameter("changeShipTo");
        RequestParser.Parameter saveOrder    = parser.getParameter("saveOrder");
        RequestParser.Parameter displayOrderSplit  = parser.getAttribute("displayOrderSplit");
        RequestParser.Parameter readOrder    = parser.getParameter("readOrder");
		RequestParser.Parameter printDisplay = parser.getParameter("printDisplay");
		RequestParser.Parameter refreshBank = parser.getParameter("refreshBank");


        if (confirm.isSet() && confirm.getValue().getString().length() > 0) {
            // -------------- case: confirm ---------------------

            forwardTo = FORWARD_CONFIRM;

            if (isDebugEnabled) {
                log.debug("selected action 'confirm case', forward to '" + forwardTo + "'.");
            }

        }
        else if (changeShipTo.isSet() && changeShipTo.getValue().getString().length() > 0) {
            // -------------- case: changeShipTo ----------------

            forwardTo = FORWARD_CHANGE_SHIPTO;

            if (isDebugEnabled) {
                log.debug("selected action 'change ship to case', forward to '" + forwardTo + "'.");
            }
        }
        else if (refreshBank.isSet() && refreshBank.getValue().getString().length() > 0) {
        	forwardTo = FORWARD_REFRESH_BANK;			
				log.debug("selected action 'refresh bank', forward to '" + forwardTo + "'.");			              
        }
        else if ((saveOrder.isSet() && saveOrder.getValue().getString().length() > 0)
                 && !(displayOrderSplit.isSet() && displayOrderSplit.getValue().getString().length() > 0)) {
            // -------------- case: saveOrder ---------------------

            forwardTo = FORWARD_SAVEORDER;
			
            if (isDebugEnabled) {
                log.debug("selected action 'save order case', forward to '" + forwardTo + "'.");
            }
        }
        else if (readOrder.isSet() && readOrder.getValue().getString().length() > 0 ) {
            // -------------- case: readOrder ---------------------

            forwardTo = FORWARD_READORDER;

            if (isDebugEnabled) {
                log.debug("selected action 'read order case', forward to '" + forwardTo + "'.");
            }
        }
		else if (printDisplay.isSet() && printDisplay.getValue().getString().length() > 0 ) {
			// -------------- case: printOrder ---------------------

			forwardTo = FORWARD_PRINTOUT;

			if (isDebugEnabled) {
				log.debug("selected action 'print order order case', forward to '" + forwardTo + "'.");
			}
		}
        else {
            // -------------- case: display ---------------------

            forwardTo = FORWARD_DISPLAY;

            if (isDebugEnabled) {
                log.debug("selected action 'display case', forward to '" + forwardTo + "'.");
            }

        }
        log.exiting();
        return forwardTo;

    }

}