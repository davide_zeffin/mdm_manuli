/*****************************************************************************
  Class:        MaintainBasketB2CRemoveAction
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Thomas Smits
  Created:      20.09.2001
  Version:      1.0

  $Revision: #2 $
  $Date: 2001/09/25 $
*****************************************************************************/

package com.sap.isa.isacore.action.b2c.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.order.Basket;
import com.sap.isa.businessobject.order.Leaflet;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreInitAction;

/**
 * Action to remove an item from the basket.
 * <p>
 * The following forwards are used by this action:
 * </p>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>displayBasket</td><td>the basket is displayed</td></tr>
 *   <tr><td>basketEmpty</td><td>the basket is empty and a special screen reporting this is shown</td></tr>
 * </table>
 *
 * @author Thomas Smits
 * @version 1.0
 */
public class MaintainBasketB2CRemoveAction extends MaintainBasketB2CBaseAction {

    public MaintainBasketB2CRemoveAction() {
    }

    public void initialize() {
       this.checkUserIsLoggedIn = false;
    }
        
    protected String basketPerform(
                HttpServletRequest request,
                HttpServletResponse response,
                HttpSession session,
                UserSessionData userSessionData,
                RequestParser parser,
                BusinessObjectManager bom,
                IsaLocation log,
                IsaCoreInitAction.StartupParameter startupParameter,
                boolean multipleInvocation,
                boolean browserBack,
                Shop shop,
                Basket basket,
                Leaflet leaflet)
                    throws CommunicationException {

		final String METHOD_NAME = "basketPerform()";
		log.entering(METHOD_NAME);
        String forwardTo = null;
        
        if (basket == null) {
        	log.debug("Browser Back pressed and basket is empty - forward to empty basket");
			forwardTo = "basketEmpty";
        }
        else {
			// get techKey of the item to be removed
			String techKeyAsString = request.getParameter("remove");
			if (techKeyAsString != null) {
				TechKey techKey = new TechKey(techKeyAsString);
				// remove item from basket
				basket.removeItem(techKey);
			}

			// aeb: because after this action the class 
			// com.sap.isa.isacore.action.b2c.order.MaintainBasketB2CDisplayAction
			// will be called, the displayBasket method must not be called twice
			// forwardTo = displayBasket(request, user, shop, basket, bom, leaflet, log, userSessionData);
			forwardTo = "displayBasket";

			// overwrite forward if provided
			String newForward = request.getParameter("forward_to");
			if (newForward != null && newForward.trim().length() != 0) {
				forwardTo = newForward;
			}
        }

		log.exiting();
        return forwardTo;
    }
}