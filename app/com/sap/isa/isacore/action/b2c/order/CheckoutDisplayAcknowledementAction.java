/*****************************************************************************
  Class:        MaintainCheckoutB2CDisplayAction
  Copyright (c) 2001, SAP AG, All rights reserved.
  Author:       SAP AG
  Created:      25.03.2001
  Version:      1.0

  $Revision: #5 $
  $Date: 2002/11/27 $
*****************************************************************************/

package com.sap.isa.isacore.action.b2c.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businessobject.order.Basket;
import com.sap.isa.businessobject.order.Order;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreInitAction;

/**
 * Action to display the checkout screen showing all data and offer the user
 * the oportunity to enter payment and shipping information.
 * <p>
 * The following forwards are used by this action:
 * </p>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>FORWARD_PRINTOUT</td><td>go to a screen summarizing the order data and allow the printout of that information</td></tr>
 *   <tr><td>FORWARD_CHECKOUT</td><td>go to the checkout screen</td></tr>
 * </table>
 *
 * @author Thomas Smits
 * @version 1.0
 */
public class CheckoutDisplayAcknowledementAction extends MaintainCheckoutB2CBaseAction {
	
    public CheckoutDisplayAcknowledementAction() {
    }

    
    /**
     * Read the order data to display it on the Checkout page
     *
     * @param request           request context
     * @param response          response context
     * @param session           unwrapped session context
     * @param userSessionData   object wrapping the session
     * @param requestParser     parser to simple retrieve data from the request
     * @param bom               reference to the BusinessObjectManager
     * @param log               reference to the IsaLocation, needed for logging
     * @param startupParameter  object containing the startup parameters
     * @param eventHandler      object to capture events with
     * @param multipleInvocation flag indicating, that a multiple invocation occured
     * @param browserBack       flag indicating a browser back
     * @param eventHandler      reference to the business event handler
     * @param user              user business object
     * @param shop              shop business object
     * @param basket            the current sales document edited
     * @param order             the order created
     *
     * @return logical key for a forward to another action or jsp
     *
	 * @see com.sap.isa.isacore.action.b2c.order.MaintainCheckoutB2CBaseAction#checkOutPerform(HttpServletRequest, HttpServletResponse, HttpSession, UserSessionData, RequestParser, BusinessObjectManager, IsaLocation, StartupParameter, boolean, boolean, BusinessEventHandler, User, Shop, Basket, Order)
	 */
    public String checkOutPerform(HttpServletRequest request,
                HttpServletResponse response,
                HttpSession session,
                UserSessionData userSessionData,
                RequestParser parser,
                BusinessObjectManager bom,
                IsaLocation log,
                IsaCoreInitAction.StartupParameter startupParameter,
                boolean multipleInvocation,
                boolean browserBack,
                BusinessEventHandler eventHandler,
                Shop shop,
                Basket basket,
                Order order) throws CommunicationException {

		final String METHOD_NAME = "checkOutPerform()";
		
		log.entering(METHOD_NAME);

        // clearMessages() as creditcard error messages can still exist, int. CSN 4505052
        order.clearMessages();
        order.readHeader();
        
        return "success";
    }
}