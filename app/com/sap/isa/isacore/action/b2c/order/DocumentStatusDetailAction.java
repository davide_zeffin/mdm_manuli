/*****************************************************************************
    Class:        DocumentStatusDetailAction
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author        Ralf Witt
    @version      0.1
    Created:      27 March 2001

    $Revision: #1 $
    $Date: 2001/06/26 $
*****************************************************************************/

package com.sap.isa.isacore.action.b2c.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.ecommerce.businessobject.loyalty.LoyaltyMembership;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.order.OrderStatus;
import com.sap.isa.businessobject.order.PartnerListEntry;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.businesspartner.businessobject.PartnerFunctionBase;
import com.sap.isa.core.Constants;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.MessageListDisplayer;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.action.loyalty.LoyaltyUtilities;
import com.sap.isa.isacore.action.order.MaintainOrderBaseAction;

/**
 * Show detail of an order document
 *
 * <br> Note that all Action classes have to provide a non argument constructor
 * because they were instantiated automatically by the action servlet.
 *
 */
public class DocumentStatusDetailAction extends IsaCoreBaseAction {

    /**
    * document stored in request scope.
    */
    public static final String RK_ORDER_STATUS_DETAIL = "orderstatusdetail";
    public static final String RK_ORDER_STATUS_DETAIL_ITEMITERATOR = "itemiterator";
    public static final String RK_BILLING_STATUS_DETAIL = "billingstatusdetail";
    public static final String RK_BILLING_STATUS_DETAIL_ITEMITERATOR = "itemiterator";
    public static final String RK_SOLDTO_NAME = "soldtoname";
    public static final String RK_SOLDTO_TECHKEY = "soldtotechkey";
    public static final String RK_CHECKBOXNAME = "itemcheckbox";
    public static final String RK_CHECKBOXHIDDEN = "itemcheckboxhidden";
	// constants for order check
	private static char CHECK_ERROR = 'E';
	private static char CHECK_OK = 'O';

    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
                                    ActionForm form,
                                    HttpServletRequest request,
                                    HttpServletResponse response,
                                    UserSessionData userSessionData,
                                    RequestParser requestParser,
                                    BusinessObjectManager bom,
                                    IsaLocation log)
            throws CommunicationException {

		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);


		OrderStatus orderStatusDetail = bom.getOrderStatus();

		// getting context value		
		String documentType = getContextValue(request,Constants.CV_DOCUMENT_TYPE);
		String documentKeyValue = getContextValue(request,Constants.CV_DOCUMENT_KEY);
		TechKey documentKey = null;
		 
		if (documentKeyValue!= null) { 
			documentKey = new TechKey(documentKeyValue);
			setContextValue(request,Constants.CV_DOCUMENT_KEY, documentKeyValue);
			setContextValue(request,Constants.CV_DOCUMENT_TYPE, documentType);
        }
        else if (orderStatusDetail == null) {
			// no  context value and no orderdetail object: We have a serious problem!		
			log.exiting();
			return mapping.findForward("error");
		}
        		
		LoyaltyMembership loyMemShip = LoyaltyUtilities.getLoyaltyMembership(userSessionData);                
		
		if (orderStatusDetail == null) {
			// prepare order status detail object if not already available.			
			DocumentStatusDetailPrepareAction.prepareOrderStatus(bom, 
			                                                     bom.getShop(), 
			                                                     documentKey, 
			                                                     "", 
			                                                     "", 
																 documentType,
																 loyMemShip);
			orderStatusDetail = bom.getOrderStatus();																 
		}


        if (orderStatusDetail.getTechKey().equals(documentKey)) {
            // Backend call has been performed in DocumentStatusPrepareAction before,
            // but refresh it. Prevents from data inconsistency.
            orderStatusDetail.readOrderStatus(null, null, null, null, loyMemShip);
        }
        else {    
			orderStatusDetail.readOrderStatus(documentKey, null, null, documentType, loyMemShip);
        }
        
		//Note 1107074                                                     
		// Is user allowed to see the document
		boolean userIsOwnerOfOrder = false;
		userIsOwnerOfOrder = preConditionsCheck(userSessionData, bom, orderStatusDetail);

		//user tried to open an order that is not his own                  
		if(!userIsOwnerOfOrder){                                           
			log.exiting();                                                 
			MessageListDisplayer messageDisplayer = new MessageListDisplayer();                                                                    
			messageDisplayer.addToRequest(request);                        
			messageDisplayer.setOnlyBack();                                
			// message = Authorization missing to show this document
			messageDisplayer.addMessage(new Message(Message.INFO,"b2b.showDoc.userNotAllowed"));  
			return mapping.findForward("message");  
		} 
        //end Note 1107074  
		   
        // When details requested, only ONE salesdocument is in access
        request.setAttribute(RK_ORDER_STATUS_DETAIL_ITEMITERATOR, orderStatusDetail.getItemsIterator());
        request.setAttribute(RK_ORDER_STATUS_DETAIL, orderStatusDetail);
        // set the delivery priority key in the request
        request.setAttribute(MaintainCheckoutB2CBaseAction.RC_DLV_PRIO, orderStatusDetail.getOrderHeader().getDeliveryPriority());
		
        userSessionData.setAttribute(MaintainOrderBaseAction.SC_SHIPTOS, orderStatusDetail.getShipTos());
		log.exiting();
        return mapping.findForward("orderdetail");
	}
	/**
	 * Following precondition checks will be performed<br>
	 * - is logged on user granted to see this document<br>
	 * @param userSessionData
	 * @param bom BusinessObjectManager
	 * @param orderStatus which should be checked
	 */
	protected boolean preConditionsCheck( UserSessionData userSessionData,
										  BusinessObjectManager bom,
										  OrderStatus orderStatus) {
		boolean retVal = false;
		char _check = checkPartnerFunction(bom.getBUPAManager(), PartnerFunctionBase.SOLDTO, orderStatus.getOrderHeader());
		if (_check == CHECK_OK) {
			retVal = true;
		}
		return retVal;
	}
	/**
	 * Check, if the partner, which is the default in the buisness object manager
	 * for the given partner function is the same as found in the header . <br>
	 * 
	 * @param buPaMa
	 * @param partnerFunction
	 * @param header
	 * @return <code>CHECK_ERROR</code>, if the partner don't match.
	 *          <code>CHECK_OK</code>, if the partner do match.
	 */
	protected char checkPartnerFunction(BusinessPartnerManager buPaMa,
										String partnerFunction, 
										HeaderSalesDocument header) {
        
		char ret = CHECK_ERROR;
            
		// take the company Partner Function from header    
		PartnerListEntry partnerEntry = header.getPartnerList().getPartner(partnerFunction);
        
		// get the default partner fromthe bu pa ma
		BusinessPartner defaultPartner =
			 buPaMa.getDefaultBusinessPartner(partnerFunction);
			 
		if (log.isDebugEnabled()){
			StringBuffer debugOutput = new StringBuffer("\ncheckPartnerFunction"); 
			debugOutput.append("\npartner function & document partner: "+ partnerFunction+", "+partnerEntry);
			if (partnerEntry != null){
				debugOutput.append("\ndocument partner key: "+ partnerEntry.getPartnerTechKey());
			}
			if (defaultPartner != null){
				debugOutput.append("\ndefault partner: "+defaultPartner.getTechKey());
			}
			log.debug(debugOutput.toString());
		}

		TechKey partnerKey =
			 (partnerEntry != null) ? partnerEntry.getPartnerTechKey() : null;

             

		// check if the partner of the order is the the same as in the buPaMa
		if (partnerKey != null && defaultPartner != null
			 && partnerKey.equals(defaultPartner.getTechKey()) ) {
         
			 ret =  CHECK_OK;           
		}
        
		return ret;
	}
    
}