/*****************************************************************************
    Class         EmptyLeafletB2CAction
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Removes all items from the leaflet (B2C scenario)
    Author:       Sabine Heider
    Created:      19.04.2001
    Version:      1.0
*****************************************************************************/
package com.sap.isa.isacore.action.b2c.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.order.Leaflet;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;


/**
 * Removes all items from the leaflet.
 *
 * @author Sabine Heider
 * @version 1.0
 *
 */
public class EmptyLeafletB2CAction extends IsaCoreBaseAction {
	
    public void initialize() {
        this.checkUserIsLoggedIn = false;
    }

    /**
     * Removes all items from the leaflet.
     * <p>
     * The request is forwarded to the action preparing the display of the
     * mini basket or of the basket. It must be maintained in the 
     * <em>config.xml</em> file using the alias <strong>miniBasket</strong>
     * or <strong>basket</strong>.
     * </p>
     *
     */
    public ActionForward isaPerform(ActionMapping mapping,
                ActionForm form,
                HttpServletRequest request,
                HttpServletResponse response,
                UserSessionData userSessionData,
                RequestParser requestParser,
                BusinessObjectManager bom,
                IsaLocation log) {

        // next page to be displayed
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        String forwardTo = null;

        // get leaflet from BOM
        Leaflet leaflet = bom.getLeaflet();

        // empty leaflet
        leaflet.removeAllItems();

        // update the cookie
        leaflet.addCookieToResponse(response);

        // read the value of "forward"-variable from the request-context 
        String forwardVal = (String)request.getParameter("forward");

        // if the forward is "successmaxi" then show the complete basket
        // else show only the minibasket
        // aeb:
        // "successmaxi" will be needed if Leaflet will be opened in a new window
        // within frame-based B2C shop. Frameless version includes Leaflet within 
        // the main application and therefore only the forward "miniBasket" is
        // necessary 
        if (forwardVal.equals("successmaxi")) {
          forwardTo = "basket";
        } else {
          // generate leaflet screen
          forwardTo = "miniBasket";
        }	
		log.exiting();
        return mapping.findForward(forwardTo);
    }

}