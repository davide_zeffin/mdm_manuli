/*****************************************************************************
  Class:        MaintainBasketB2CDispatcherAction
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:
  Created:      19.09.2001
  Version:      1.0

  $Revision: #4 $
  $Date: 2002/11/08 $
*****************************************************************************/

package com.sap.isa.isacore.action.b2c.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businessobject.order.Basket;
import com.sap.isa.businessobject.order.Leaflet;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.action.IsaCoreInitAction;

/**
 * Action to dispatch all requests coming from the <code>order.jsp</code> to
 * different (specialized) actions. The decision to which action the control
 * is forwarded to depends on the request parameters set.<br><br>
 *
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>Request parameter</b></td>
 *     <td><b>Value of parameter</b></td>
 *     <td><b>Logical forward used</b></td>
 *   <tr>
 *   <tr><td>display</td><td>!= ""</td><td>showonly</td><tr>
 *   <tr><td>update</td><td>!= ""</td><td>update</td></tr>
 *   <tr><td>empty</td><td>!= ""</td><td>empty</td></tr>
 *   <tr><td>removeallitems</td><td>!= ""</td><td>removeallitems</td></tr>
 *   <tr><td>save</td><td>!= ""</td><td>save</td></tr>
 *   <tr><td>createnew</td><td>!= ""</td><td>createnew</td></tr>
 *   <tr><td>remove</td><td>!= ""</td><td>remove</td></tr>
 *   <tr><td>checkout</td><td>!= ""</td><td>checkout</td></tr>
 *   <tr><td>order</td><td>!= ""</td><td>order</td></tr>
 *   <tr><td>savebasket</td><td>!= ""</td><td>savebasket</td></tr>
 *   <tr><td>returnconfig</td><td>!= ""</td><td>returnconfig</td></tr>
 *   <tr><td>exitconfig</td><td>!= null</td><td>exitconfig</td></tr>
 * </table>
 * <p>
 * According to the described parameters the corresponding forwards are used.
 * </p>
 *
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>display</td><td>the content of the document is displayed again</td></tr>
 *   <tr><td>update</td><td>the data of the document is send to the backend and displayed again</td></tr>
 *   <tr><td>empty</td><td>the document is cleared</td></tr>
 *   <tr><td>save</td><td></td>the document is saved for later use and closed<td></tr>
 *   <tr><td>createnew</td><td>the basket is closed and a new basket created</td></tr>* 
 *   <tr><td>remove</td><td>an item in the document is deleted</td></tr>
 *	 <tr><td>removeallitems</td><td>all items of the document are deleted</td></tr>
 *   <tr><td>checkout</td><td>the user is finished and wants to go to the checkout</td></tr>
 *   <tr><td>order</td><td>???</td></tr>
 *   <tr><td>savebasket</td><td>the basket is copied and saved in an order template</td></tr>
 *   <tr><td>returnconfig</td><td>return from the configuration, use the config data</td></tr>
 *   <tr><td>exitconfig</td><td>exit from the configuration, all config data is discarded</td></tr>
 * </table>
 *
 *
 * @author Thomas Smits
 * @version 1.0
 */
 public class MaintainBasketB2CDispatcherAction extends MaintainBasketB2CBaseAction {

    private static final String FORWARD_DISPLAY        = "display";
    private static final String FORWARD_UPDATE         = "update";
    private static final String FORWARD_EMPTY          = "empty";
	private static final String FORWARD_CREATENEW      = "createnew";   
	private static final String FORWARD_REMOVEALLITEMS = "removeallitems";   
    private static final String FORWARD_REMOVE         = "remove";
    private static final String FORWARD_CHECKOUT       = "checkout";
    private static final String FORWARD_ORDER          = "order";
    private static final String FORWARD_ORDERSPLIT     = "ordersplit";
    private static final String FORWARD_ORDERSPLIT_OCI = "ordersplitoci";
    private static final String FORWARD_SAVEBASKET     = "savebasket";
    private static final String FORWARD_ITEMCONFIG     = "itemconfig";
    private static final String FORWARD_RETURNCONFIG   = "returnconfig";
    private static final String FORWARD_EXITCONFIG     = "exitconfig";
    private static final String FORWARD_PRODUCTDETAILS = "productdetails";
    


    public MaintainBasketB2CDispatcherAction() {
    }
    
    public void initialize() {
        this.checkUserIsLoggedIn = false;
    }

    /**
     * Overriden <em>isaPerform</em> method of <em>IsaCoreBaseAction</em>.
     */
    protected String basketPerform(
            HttpServletRequest request,
            HttpServletResponse response,
            HttpSession session,
            UserSessionData userSessionData,
            RequestParser parser,
            BusinessObjectManager bom,
            IsaLocation log,
            IsaCoreInitAction.StartupParameter startupParameter,
            boolean multipleInvocation,
            boolean browserBack,
            Shop shop,
            Basket basket,
            Leaflet leaflet)
                throws CommunicationException {

		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        String action = null;
        
        boolean performBasketUpdate = false;
        boolean checkBasketIsValid = false;

        RequestParser.Parameter paramDisplay =
                parser.getParameter("display");
        RequestParser.Parameter paramUpdate =
                parser.getParameter("update");
        RequestParser.Parameter paramEmpty =
                parser.getParameter("empty");
		RequestParser.Parameter paramCreateNew =
				parser.getParameter("createnew");                
		RequestParser.Parameter paramRemoveAllItems =
				parser.getParameter("removeallitems"); 
        RequestParser.Parameter paramRemove =
                parser.getParameter("remove");
        RequestParser.Parameter paramCheckout =
                parser.getParameter("checkout");
        RequestParser.Parameter paramCheckoutOci =
                parser.getParameter("checkoutoci");
        RequestParser.Parameter paramOrder =
                parser.getParameter("order");              
        RequestParser.Parameter paramSaveBasket =
                parser.getParameter("savebasket");               
        RequestParser.Parameter paramItemConfig =
            parser.getParameter("itemconfig");
        RequestParser.Parameter paramReturnConfig =
                parser.getParameter("returnconfig");
        RequestParser.Parameter paramExitConfig =
                parser.getParameter("exitconfig");
        RequestParser.Parameter paramFallback =
                parser.getParameter("fallback");
        RequestParser.Parameter paramShowProductDetails =
            parser.getParameter("showProductDetails");
        
        RequestParser.Parameter attrDisplay =
                parser.getAttribute("display");
        RequestParser.Parameter attrUpdate =
                parser.getAttribute("update");
        RequestParser.Parameter attrEmpty =
                parser.getAttribute("empty");
				RequestParser.Parameter attrCreateNew =
				parser.getAttribute("createnew");                
		RequestParser.Parameter attrRemoveAllItems =
				parser.getAttribute("removeallitems");               
        RequestParser.Parameter attrRemove =
                parser.getAttribute("remove");
        RequestParser.Parameter attrCheckout =
                parser.getAttribute("checkout");
        RequestParser.Parameter attrCheckoutOci =
                parser.getAttribute("checkoutoci");
        RequestParser.Parameter attrOrder =
                parser.getAttribute("order");
        RequestParser.Parameter attrSaveBasket =
                parser.getAttribute("savebasket");
        RequestParser.Parameter attrItemConfig =
                parser.getAttribute("itemconfig");
        RequestParser.Parameter attrReturnConfig =
                parser.getAttribute("returnconfig");
        RequestParser.Parameter attrExitConfig =
                parser.getAttribute("exitconfig");
        RequestParser.Parameter attrFallback =
                parser.getAttribute("fallback");
        RequestParser.Parameter attrShowProductDetails =
            parser.getAttribute("showProductDetails");
        
        if (paramDisplay.isSet() || attrDisplay.isSet()) {
            action = FORWARD_DISPLAY;
        }
        else if (paramUpdate.isSet() || attrUpdate.isSet()) {
            action = FORWARD_UPDATE;
        }
        else if (paramEmpty.isSet() || attrEmpty.isSet()) {
            action = FORWARD_EMPTY;
        }
		else if (paramCreateNew.isSet() || attrEmpty.isSet()) {
			action = FORWARD_CREATENEW;
		}		
		else if (paramRemoveAllItems.isSet() || attrRemoveAllItems.isSet()) {
			action = FORWARD_REMOVEALLITEMS;
		}        	
        else if (paramRemove.isSet() || attrRemove.isSet()) {
            action = FORWARD_REMOVE;
        }
        else if (paramCheckout.isSet() || attrCheckout.isSet()) {
            action = FORWARD_CHECKOUT;
            performBasketUpdate = true;
            checkBasketIsValid = true;
        }
        else if (paramCheckoutOci.isSet() || attrCheckoutOci.isSet()) {
            action = FORWARD_ORDERSPLIT_OCI;
        }
        else if (paramOrder.isSet() || attrOrder.isSet()) {
            if (bom.getShop().isPartnerBasket()) { // channel commerce hub scenario ?
                action = FORWARD_ORDERSPLIT;
            }
            else {
                action = FORWARD_ORDER;
            }
        }
        else if (paramSaveBasket.isSet() || attrSaveBasket.isSet()) {
            action = FORWARD_SAVEBASKET;
        }
        else if (paramReturnConfig.isSet() || attrReturnConfig.isSet()) {
            action = FORWARD_RETURNCONFIG;
        }
        else if (paramExitConfig.isSet() || attrExitConfig.isSet()) {
            action = FORWARD_EXITCONFIG;
        }
        else if (paramItemConfig.isSet() || attrItemConfig.isSet()) {
            action = FORWARD_ITEMCONFIG;
            performBasketUpdate = true;
        }
        else if (paramShowProductDetails.isSet() || attrShowProductDetails.isSet()) {
            action = FORWARD_PRODUCTDETAILS;
            performBasketUpdate = true;
        }
        
        // the next if is necessary to handle the case that return 
        // is pressed in the basket.jsp and the first available
        // submit button doesn't fire
        // In the basket.jsp the fallback parameter is always set,
        // so this test will always resolve to true
        // !! Due to this fact, the following else if must always
        // be the last statement in the else if cascade !!
        else if (paramFallback.isSet() || attrFallback.isSet()) {
            action = FORWARD_UPDATE;
        }
        
        if(performBasketUpdate) {
            // update basket with request data
    		updateBasket(parser, shop, basket, bom, log, userSessionData, request);
    		basket.readHeader();
    		basket.readAllItems();
        }
        if(checkBasketIsValid) {
    		if(!basket.isValid()) {
            	action = FORWARD_DISPLAY;
            }        	
        }
        
		log.exiting();
        return action;
    }
}