/*****************************************************************************
  Class:        MaintainBasketB2CAddSoldFromAction.java
  Copyright (c) 2002, SAP, Germany, All rights reserved.
  Author:       SAP
  Created:      01.10.2002
  Version:      1.0

  $Revision: #1 $
  $Date: 2002/10/01 $
*****************************************************************************/

package com.sap.isa.isacore.action.b2c.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.item.ItemList;
import com.sap.isa.businessobject.order.Basket;
import com.sap.isa.businessobject.order.Leaflet;
import com.sap.isa.businessobject.order.PartnerListEntry;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreInitAction;

/**
 * Action to and a sold from to the basket or an item and update and re-display the basket.
 * If no itemTechkey is found, it is assumed, that the sold from should be set on document level.
 * <p>
 * The following forwards are used by this action:
 * </p>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>displayBasket</td><td>the basket is displayed</td></tr>
 *   <tr><td>basketEmpty</td><td>the basket is empty and a special screen reporting this is shown</td></tr>
 * </table>
 *
 * @author SAP
 * @version 1.0
 */
public class MaintainBasketB2CAddSoldFromAction extends MaintainBasketB2CBaseAction {

    public static String RC_SOLDFROM_TECHKEY = "com.sap.isa.isacore.action.b2c.orderMaintainBasket.B2CAddSoldFromAction.soldFromTechKey";
    public static String RC_ITEM_TECHKEY = "com.sap.isa.isacore.action.b2c.orderMaintainBasket.B2CAddSoldFromAction.itemTechKey";

    public void initialize() {
       this.checkUserIsLoggedIn = false;
    } 

    public MaintainBasketB2CAddSoldFromAction() {
    }

    protected String basketPerform(
                HttpServletRequest request,
                HttpServletResponse response,
                HttpSession session,
                UserSessionData userSessionData,
                RequestParser parser,
                BusinessObjectManager bom,
                IsaLocation log,
                IsaCoreInitAction.StartupParameter startupParameter,
                boolean multipleInvocation,
                boolean browserBack,
//                User user,
                Shop shop,
                Basket basket,
                Leaflet leaflet)
                    throws CommunicationException {

		final String METHOD_NAME = "basketPerform()";
		log.entering(METHOD_NAME);
        RequestParser.Parameter soldFromTechKeyPar =
                parser.getParameter(RC_SOLDFROM_TECHKEY);

        RequestParser.Parameter itemTechKeyPar =
                parser.getParameter(RC_ITEM_TECHKEY);

        if (soldFromTechKeyPar.isSet()) {

            TechKey soldFromTechKey = new TechKey(soldFromTechKeyPar.getValue().getString());
            BusinessPartner soldFrom = bom.createBUPAManager().getBusinessPartner(soldFromTechKey);
            soldFrom.getAddress(); // this is necessary to force a read from the backend
            String soldFromId = soldFrom.getId();

            if (soldFromId == null || soldFromId.length() == 0) {
                log.debug("No soldFrom found for TechKey: " + soldFromTechKey);
            }
            else {
                // if the salesdoc has just one item the soldto is set fro the header and the item
                if (itemTechKeyPar.isSet() && basket.getItems().size() > 1) { // set soldFrom only for an special item

                    TechKey itemTechKey = new TechKey(itemTechKeyPar.getValue().getString());

                    basket.getItem(itemTechKey).getPartnerList().setSoldFrom(new PartnerListEntry(soldFromTechKey, soldFromId));

                    log.debug("SoldFrom added to item: Item TechKey " + itemTechKey + " SoldFrom TechKey " + soldFromTechKey + " Id: " + soldFromId);
                }
                else { // set soldFrom for header and all items

                    // header
                    basket.getHeader().getPartnerList().setSoldFrom(new PartnerListEntry(soldFromTechKey, soldFromId));
                    log.debug("SoldFrom added to header and items : Reseller TechKey " + soldFromTechKey + " Id: " + soldFromId);

                    // items
                    ItemList items = basket.getItems();

                    for (int i = 0; i < items.size(); i++) {
                        items.get(i).getPartnerList().setSoldFrom(new PartnerListEntry(soldFromTechKey, soldFromId));
                    }
                }

                basket.update(bom.createBUPAManager());                // may throw CommunicationException
            }
        }

		// aeb: because after this action the class 
		// com.sap.isa.isacore.action.b2c.order.MaintainBasketB2CDisplayAction
		// will be called, the displayBasket method must not be called twice
		// return displayBasket(request, shop, basket, bom, leaflet, log, userSessionData);
		log.exiting();
		return "displayBasket";

    }
}