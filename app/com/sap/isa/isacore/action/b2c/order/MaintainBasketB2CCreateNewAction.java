/*****************************************************************************
  Class:        MaintainBasketB2CCreateNewAction
  Copyright (c) 2006, SAP AG, Germany, All rights reserved.
  Author:       SAP AG
  Created:      13.02.2006
  Version:      1.0

*****************************************************************************/

package com.sap.isa.isacore.action.b2c.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.businessobject.order.Basket;
import com.sap.isa.businessobject.order.CampaignListEntry;
import com.sap.isa.businessobject.order.Leaflet;
import com.sap.isa.businessobject.order.PartnerList;
import com.sap.isa.businessobject.order.PartnerListEntry;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreInitAction;

/**
 * Action to create a new basket. Close the current basket, if not empty.
 * <p>
 * The following forwards are used by this action:
 * </p>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr><td>displayBasket</td><td>the basket has to be displayed</td></tr>
 * </table>
 *
 * @author SAP AG
 * @version 1.0
 */
public class MaintainBasketB2CCreateNewAction extends MaintainBasketB2CBaseAction {

    public MaintainBasketB2CCreateNewAction() {
    }
    
	public void initialize() {
		   this.checkUserIsLoggedIn = false;
		}

	/**
	 * Implement this method to add functionality to your action.
	 *
	 * @param form              The <code>FormBean</code> specified in the
	 *                          config.xml file for this action
	 * @param request           The request object
	 * @param response          The response object
	 * @param userSessionData   Object wrapping the session
	 * @param requestParser     Parser to simple retrieve data from the request
	 * @param bom               Reference to the BusinessObjectManager
	 * @param log               Reference to the IsaLocation, needed for logging
	 * @return Forward to another action or page
	 */
	protected String basketPerform(
				HttpServletRequest request,
				HttpServletResponse response,
				HttpSession session,
				UserSessionData userSessionData,
				RequestParser parser,
				BusinessObjectManager bom,
				IsaLocation log,
				IsaCoreInitAction.StartupParameter startupParameter,
				boolean multipleInvocation,
				boolean browserBack,
				Shop shop,
				Basket basket,
				Leaflet leaflet)
					throws CommunicationException {

		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
		TechKey documentKey = null;

        if (shop == null) {
			log.exiting();
			throw new PanicException("Shop not found in BOM");        	
        }
		User user  = bom.getUser();

		if (user == null) {
			log.exiting();
			throw new PanicException("User not found in BOM");
		}        
    
		String forwardTo = null;
        if (basket != null) {
        	// close the current basket
        //	basket.dequeue();
        	basket.destroyContent();
        	bom.releaseBasket();
        }
		// create new basket
		basket = bom.createBasket();
		// read catalog
		WebCatInfo webCat =
					 getCatalogBusinessObjectManager(userSessionData).getCatalog();
		// Setting of the campaignKey if existing
		CampaignListEntry campaignListEntry = null;
		 if ((startupParameter != null) && (startupParameter.getCampaignKey().length() > 0)) {
			 String campaignKey = startupParameter.getCampaignKey();
			 basket.setCampaignKey(campaignKey);
			 basket.setCampaignObjectType(startupParameter.getCampaignObjectType());

			 campaignListEntry = basket.getHeader().getAssignedCampaigns().createCampaign();
			 campaignListEntry.setCampaignGUID(new TechKey(campaignKey));
			 campaignListEntry.setCampaignTypeId(startupParameter.getCampaignObjectType());
		 }

		 if (shop.isManualCampainEntryAllowed() && !shop.isSoldtoSelectable()) {
			 if (shop.isInternalCatalogAvailable() && (webCat == null)) {
				 throw (new IllegalArgumentException("Paramter webCat can not be null!"));
			 }
			 // check the catalog if a campaign Id exists
			 if ((webCat != null) && ((webCat.getCampaignId() != null) && (webCat.getCampaignId().length() > 0))) {
				 // we use the campaign Id of the catalog
				 campaignListEntry = null;
				 // set the campaign Id
				 campaignListEntry = basket.getHeader().getAssignedCampaigns().createCampaign(webCat.getCampaignId(),
						 TechKey.EMPTY_KEY, "", false, true);
			 }
		 }	
		 
		 if (user.isUserLogged()) {		 
		 	PartnerList pList = new PartnerList();
		 	BusinessPartnerManager bupama = bom.createBUPAManager();
		 	BusinessPartner buPa;

		 	String compPartnerFunction = shop.getCompanyPartnerFunction();

		 	if ((compPartnerFunction == null) || (compPartnerFunction.length() == 0)) {
			 	compPartnerFunction = PartnerFunctionData.SOLDTO;
		 	}

		 	if (compPartnerFunction.equals(PartnerFunctionData.SOLDTO) ||
				 compPartnerFunction.equals(PartnerFunctionData.RESELLER) ||
				 compPartnerFunction.equals(PartnerFunctionData.AGENT)) {
			 	buPa = bupama.getDefaultBusinessPartner(compPartnerFunction);

			 	if (buPa != null) {
					pList.setPartner(compPartnerFunction, new PartnerListEntry(buPa.getTechKey(), buPa.getId()));
			 	}
		 	}

		 	// set contact info, but only, if we are not in the reseller scenario
		 	buPa = bupama.getDefaultBusinessPartner(PartnerFunctionData.CONTACT);

		 	if ((!compPartnerFunction.equals(PartnerFunctionData.RESELLER)) &&
				 (!compPartnerFunction.equals(PartnerFunctionData.AGENT)) &&
				 (buPa != null)) {
				pList.setContact(new PartnerListEntry(buPa.getTechKey(), buPa.getId()));
		 	}

		 	// add responsible at partner in reseller scenario
		 	if (compPartnerFunction.equals(PartnerFunctionData.RESELLER)) {
			 	BusinessPartner bupa1 = bupama.getDefaultBusinessPartner(PartnerFunctionData.RESP_AT_PARTNER);
			 	pList.setPartner(PartnerFunctionData.RESP_AT_PARTNER,
				new PartnerListEntry(bupa1.getTechKey(), bupa1.getId()));
		 	}

		 	// initialisation of the basket
		 	basket.init(shop, bupama, pList, webCat, campaignListEntry); // may throw CommunicationException		 
		 	
		 } else {
		 	basket.init(shop, webCat, campaignListEntry);
		 }
		 			 
		forwardTo = "displayBasket";          	
        
		return forwardTo;
	}
}