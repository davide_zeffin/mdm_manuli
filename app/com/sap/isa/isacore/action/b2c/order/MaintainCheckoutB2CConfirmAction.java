/*****************************************************************************
  Class:        MaintainCheckoutB2CConfirmAction
  Copyright (c) 2001, SAP AG, All rights reserved.
  Author:       SAP AG
  Created:      25.03.2001
  Version:      1.0

  $Revision: #6 $
  $Date: 2003/03/13 $
*****************************************************************************/

package com.sap.isa.isacore.action.b2c.order;

import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businessobject.item.ItemList;
import com.sap.isa.businessobject.order.Basket;
import com.sap.isa.businessobject.order.Order;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreInitAction;
import com.sap.isa.payment.backend.boi.PaymentMethodData;
import com.sap.isa.payment.businessobject.BankTransfer;
import com.sap.isa.payment.businessobject.PaymentBase;



/**
 * Action to finally check the data entered into the checkout form - the payment
 * data - and display the confirmation screen, if the data was ok.
 * Otherwise the checkout screen is displayed again.
 * <p>
 * The following forwards are used by this action:
 * </p>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>FORWARD_PAYMENTERROR</td><td>problems with the entered payment data ocurred</td></tr>
 *   <tr><td>FORWARD_CONFIRM</td><td>payment data was ok, proceed to confirmation screen</td></tr>
 * </table>
 *
 * @author SAP AG
 * @version 1.0
 */
public class MaintainCheckoutB2CConfirmAction extends MaintainCheckoutB2CBaseAction {
	
    public MaintainCheckoutB2CConfirmAction() {
    }

    public String checkOutPerform(HttpServletRequest request,
                HttpServletResponse response,
                HttpSession session,
                UserSessionData userSessionData,
                RequestParser parser,
                BusinessObjectManager bom,
                IsaLocation log,
                IsaCoreInitAction.StartupParameter startupParameter,
                boolean multipleInvocation,
                boolean browserBack,
                BusinessEventHandler eventHandler,
                Shop shop,
                Basket basket,
                Order order) throws CommunicationException {

		final String METHOD_NAME = "checkOutPerform()";
		log.entering(METHOD_NAME);
        String forwardTo = null;
        
        if (basket == null) {
        	forwardTo = FORWARD_DISPLAY_BASKET;
        	log.exiting();
        	return forwardTo;
        }

        parseRequest(parser, order, shop, log);
        
        List paymentMethods = order.getPayment().getPaymentMethods();
        if (paymentMethods != null && paymentMethods.size() > 0) {        
        	Iterator iter = paymentMethods.iterator();	
        	if ((shop.isPayMask4CollAuthEnabled()== true)) {	        
		   		while (iter.hasNext()){
              		PaymentMethodData payMethod = (PaymentMethodData) iter.next();							
              		if (payMethod instanceof BankTransfer){
                  		if( ((BankTransfer)payMethod).getDebitAuthority() == null) {

        		      		PaymentBase payment = order.getPayment();
        			  		payment.addMessage(new Message(Message.ERROR, "b2c.checkout.debitauth.missing"));		
        		      		payment.setError(true);	
        		      		log.exiting();
        		      		return FORWARD_CONFIRM;					
                  		}
              		}			
          		}
        	}
        }       

/* Attention: If the soldto.createPayment() method is called here an additional business agreement is created  */
/*            if the user chooses the BACK button on the 'Check Order' page.                                   */
/*            To avoid this the call of the method is moved to the MaintainCheckoutB2CCommitPaymentAction.     */  
/*            Thus it would possible to merge the calls of the methods soldTo.createPayment89 and              */
/*            soldTo.commitPayment() together in one RFC call!                                                 */
/*                                                                                                             */ 
/*            Correction marked with corr#s(tart) and corr#e(nd)                                               */
/*                                                                                                             */

//corr#s    
        // The parameter for BuAg - Payment creation on BP Side should be chacked firstly:
  
//        BusinessPartnerManager buPaMa = bom.createBUPAManager();
//        BusinessPartner buPa = buPaMa.getDefaultBusinessPartner(PartnerFunctionBase.SOLDTO);
//        SoldTo soldTo = (SoldTo)buPa.getPartnerFunctionObject(PartnerFunctionBase.SOLDTO);
//corr#e
 	    
        // 1. if needed by the shop the payment information is created for Business Partner
        // 2. if needed the payment information is assigned to an agreement	    
   //     PaymentBase payment = (PaymentBase) order.getPayment();	 
        // 5. check errors
        
	//	if  (payment.getError() == true){
	//		log.exiting();
	//		return FORWARD_CONFIRM;
	//	}     
		      
        // 4. the agreement is added to SoldTo            
//corr#s             
//       if (shop.isPaymAtBpEnabled() == true) {	                  	     
//           List buAgList = soldTo.createPayment(buPa, payment);        
//           if ( buAgList != null && buAgList.size()>0){        
//                soldTo.setBusinessAgreement(buAgList);               
//           }            
//       }
//        else {
        if (shop.isPaymAtBpEnabled() == false) {
//corr#e
		   try {
			   order.createPayment();
			   }
			catch (CommunicationException ex) {
			   forwardTo = FORWARD_PAYMENTERROR;  
			   log.exiting();
			   return forwardTo; 	
			}
        }
       
        
        // In case of R/3 backend we must set the delivery priority on item level        
        setDeliveryPriorityOnItemLevel(shop, parser, order, log);
        // In case that credit card was re-entered update and read is necessary to check the document's validity
        order.updateHeader(bom.createBUPAManager());	
        forwardTo = FORWARD_CONFIRM;

        log.exiting();
        return forwardTo;
    }
    

    /**
     * This method set the delivery priority on item level in case of R/3 backend
     * and the delivery priority is set.
     *  
     * @param shop
     * @param parser
     * @param order
     * @param log
     * 
     * @throws CommunicationException
     */
    protected void setDeliveryPriorityOnItemLevel(Shop shop, 
                                                  RequestParser parser, 
                                                  Order order,
	                                              IsaLocation log) throws CommunicationException {
                                                    	
		if ( shop.getBackend().equals(Shop.BACKEND_R3) || 
             shop.getBackend().equals(Shop.BACKEND_R3_40) ||
			 shop.getBackend().equals(Shop.BACKEND_R3_46) || 
             shop.getBackend().equals(Shop.BACKEND_R3_PI) ) {
			  
			RequestParser.Parameter deliveryPriority =
						parser.getParameter("deliveryPriority");
			if (deliveryPriority.isSet()) {
				ItemList itemList = order.getItems();
				for (int i = 0; i < itemList.size(); i++) {
					itemList.getItemData(i).setDeliveryPriority(deliveryPriority.getValue().getString());
				}
			}
		} 
    }
    												  
}