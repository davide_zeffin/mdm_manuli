/*****************************************************************************
  Class:        MaintainCheckoutB2CChangeShipToAction
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       SAP AG
  Created:      25.03.2001
  Version:      1.0

  $Revision: #2 $
  $Date: 2001/09/26 $
*****************************************************************************/

package com.sap.isa.isacore.action.b2c.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.ShipTo;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businessobject.order.Basket;
import com.sap.isa.businessobject.order.Order;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.ShipToSelection;
import com.sap.isa.isacore.action.IsaCoreInitAction;
import com.sap.isa.isacore.action.b2c.B2cConstants;

/**
 * Action to handle the change of the ship to information during the checkout
 * process in the b2c scenatio.
 * <p>
 * The following forwards are used by this action:
 * </p>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>FORWARD_SHOW_SHIPTO</td><td>display the screen to enter the ship to information</td></tr>
 * </table>
 *
 * @author SAP AG
 * @version 1.0
 */
public class MaintainCheckoutB2CChangeShipToAction extends MaintainCheckoutB2CBaseAction {

    public MaintainCheckoutB2CChangeShipToAction() {
    }

    public String checkOutPerform(HttpServletRequest request,
                HttpServletResponse response,
                HttpSession session,
                UserSessionData userSessionData,
                RequestParser parser,
                BusinessObjectManager bom,
                IsaLocation log,
                IsaCoreInitAction.StartupParameter startupParameter,
                boolean multipleInvocation,
                boolean browserBack,
                BusinessEventHandler eventHandler,
                Shop shop,
                Basket basket,
                Order order) throws CommunicationException {

		final String METHOD_NAME = "checkOutPerform()";
		log.entering(METHOD_NAME);
        parseRequest(parser, order, shop, log);
        order.update(bom.createBUPAManager());

        order.readHeader();
        order.readAllItems(); 

        ShipToSelection shipToSelection = (ShipToSelection)request.getSession().getAttribute(B2cConstants.SC_SHIPTO_SELECTION);           

        ShipTo shipTo = null; 

        // get ship-to address
        if (shipToSelection.getActualShipToKey() != null) {            
            shipTo = shipToSelection.getShipTo(shipToSelection.getActualShipToKey());
        }    


        if (shipTo != null) {
            request.setAttribute(B2cConstants.SHIP_TO_ADDRESS, shipTo.getAddress());
        }
		log.exiting();
        return FORWARD_SHOW_SHIPTO;
    }
}