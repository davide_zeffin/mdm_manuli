package com.sap.isa.isacore.action.b2c.order;


import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businessobject.order.Basket;
import com.sap.isa.businessobject.order.Order;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreInitAction;
import com.sap.isa.payment.backend.boi.PaymentMethodData;
import com.sap.isa.payment.businessobject.BankTransfer;

             
public class MaintainCheckoutB2CRefreshBankAction extends MaintainCheckoutB2CBaseAction {

	public static final String CHECKOUT_ERROR_MESSAGES = "MaintainCheckoutB2CRefreshBankAction.ErrorMessages";
	
	
	public MaintainCheckoutB2CRefreshBankAction() {
	}

	
	public String checkOutPerform (HttpServletRequest request,
		HttpServletResponse response,
		HttpSession session,
		UserSessionData userSessionData,
		RequestParser parser,
		BusinessObjectManager bom,
		IsaLocation log,	    
		IsaCoreInitAction.StartupParameter startupParameter,
		boolean multipleInvocation,
		boolean browserBack,
		BusinessEventHandler eventHandler,
		Shop shop,
		Basket basket,
		Order order) throws CommunicationException {
				
		final String METHOD_NAME = "checkoutRefreshBank()";
		log.entering(METHOD_NAME);				
		
		parseRequest(parser, order, shop, log);
		
        if (order.getPayment() != null) {											     
            if (order.getPayment().getPaymentMethods() != null) { 							
                List paymentMethods = order.getPayment().getPaymentMethods();
                Iterator iter = paymentMethods.iterator();
                while ((iter.hasNext())){
                    PaymentMethodData payMethod = (PaymentMethodData) iter.next();							
                    if (payMethod instanceof BankTransfer){
                        BankTransfer bankTransfer = (BankTransfer) payMethod; 
                        order.readBankName(bankTransfer);
                        if (bankTransfer.getMessageList() != null && (bankTransfer.getMessageList().size() > 0)){
                            order.getPayment().setError(true);
                            order.getPayment().copyMessages(bankTransfer);
                                bankTransfer.clearMessages();
                        }							
                    }	
                }						             
            }
        }
		
        log.exiting();	
        return FORWARD_CHECKOUT;
	}	
}	