/*****************************************************************************
  Class:        LoadBasketB2CAction
  Copyright (c) 2006, SAP AG, Germany, All rights reserved.
  Author:       SAP AG
  Created:      06.02.2006
  Version:      1.0

*****************************************************************************/

package com.sap.isa.isacore.action.b2c.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.businessobject.order.Basket;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.eai.BackendRuntimeException;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.MessageList;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.MessageDisplayer;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.action.b2c.B2cConstants;

/**
 * Action to load a saved basket.
 * <p>
 * The following forwards are used by this action:
 * </p>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>closeBasket</td><td>the current basket has to be closes</td></tr>
 *   <tr><td>displayBasket</td><td>the basket has to be displayed</td></tr>
 *   <tr><td>basketEmpty</td><td>the basket is empty and a special screen reporting this is shown</td></tr>
 * </table>
 *
 * @author SAP AG
 * @version 1.0
 */
public class LoadBasketB2CAction extends IsaCoreBaseAction {

    public LoadBasketB2CAction() {
    }

	/**
	 * Implement this method to add functionality to your action.
	 *
	 * @param form              The <code>FormBean</code> specified in the
	 *                          config.xml file for this action
	 * @param request           The request object
	 * @param response          The response object
	 * @param userSessionData   Object wrapping the session
	 * @param requestParser     Parser to simple retrieve data from the request
	 * @param bom               Reference to the BusinessObjectManager
	 * @param log               Reference to the IsaLocation, needed for logging
	 * @return Forward to another action or page
	 */
	public ActionForward isaPerform(ActionMapping mapping,
									ActionForm form,
									HttpServletRequest request,
									HttpServletResponse response,
									UserSessionData userSessionData,
									RequestParser requestParser,
									BusinessObjectManager bom,
									IsaLocation log)
	throws CommunicationException {

		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
		TechKey documentKey = null;
        boolean basketValid = true;

		if( (request.getParameter("techkey")) != null) {
			documentKey = new TechKey(request.getParameter("techkey"));
		}
		String documentId = (request.getParameter("objectid") != null ? request.getParameter("object_id") : "");
		String documentsOrigin = (request.getParameter("objects_origin") != null ? request.getParameter("objects_origin") : "");
		String documentType = (request.getParameter("objecttype") != null ? request.getParameter("objecttype") : "");

        Shop shop = bom.getShop();
        if (shop == null) {
			log.exiting();
			throw new PanicException("Shop not found in BOM");        	
        }
		User user  = bom.getUser();

		if (user == null) {
			log.exiting();
			throw new PanicException("User not found in BOM");
		}        
     
		String forwardTo = null;
        Basket basket = bom.getBasket();
        if (basket != null) {
        	// close the current basket
        //	basket.dequeue();
        	basket.destroyContent();
        	bom.releaseBasket();
        }
		// read basket in update mode
		basket = bom.createBasket();
		// set shop
		basket.getHeader().setShop(shop);
		// read catalog
		WebCatInfo webCat =
					 getCatalogBusinessObjectManager(userSessionData).getCatalog();			
		basket.setTechKey(documentKey);
		basket.setGData(shop,webCat);
		
		if (!basket.isValid()) {
			basketValid = false;
			request.setAttribute(B2cConstants.MESSAGE_LIST, basket.getMessageList());		
		} else {
			   
			try {                                                             
				basket.readForUpdate();
			}
			catch (BackendRuntimeException ex) {
				// create a Message Displayer for the error page!			
		    	String msg =  ex.getMessage();
		    	Message message = new Message(2, msg);
				MessageDisplayer messageDisplayer = new MessageDisplayer();
				messageDisplayer.addToRequest(request);
				messageDisplayer.addMessage(message);
				messageDisplayer.setBackAvailable(false);
				messageDisplayer.setOnlyDisplayMessages();
			}

        	String docType = basket.getHeader().getDocumentType();
        	if(!docType.equals(HeaderData.DOCUMENT_TYPE_ORDERTEMPLATE)) {
            	basket.addMessage(new Message(Message.ERROR, "b2c.loadbskt.noordrposbl"));
            	basketValid = false;
            	request.setAttribute(B2cConstants.MESSAGE_LIST, basket.getMessageList());
        	}
		}
        
        if(!basketValid) {
            basket.destroyContent();
            bom.releaseBasket();
        }
        
		forwardTo = "displaybasket";
       	
        
		return mapping.findForward(forwardTo);
	}
}