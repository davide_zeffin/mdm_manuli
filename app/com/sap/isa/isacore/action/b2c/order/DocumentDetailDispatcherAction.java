/*****************************************************************************
	Class:        DocumentDetailDispatcherAction
	Copyright (c) 2006, SAP AG, Germany, All rights reserved.
	Author        SAP
	Created:      February 2006

	$Revision: $
	$Date: $
*****************************************************************************/

package com.sap.isa.isacore.action.b2c.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 * Action to dispatch the request coming from the generic search of documents.
 * In general the found document is forwarded for display to the DocumentStatusDetailPrepareAction. 
 * For saved baskets, which should be loaded for further maintenance, the forward will be dispatched to
 * the LoadBasketB2CAction
 *
 *
 */
public class DocumentDetailDispatcherAction extends IsaCoreBaseAction {
	
	private static final String FORWARD_ORDERDETAIL      = "orderdetail";
	private static final String FORWARD_LOADBASKET       = "loadbasket";	
	
	/**
	 * Implement this method to add functionality to your action.
	 *
	 * @param form              The <code>FormBean</code> specified in the
	 *                          config.xml file for this action
	 * @param request           The request object
	 * @param response          The response object
	 * @param userSessionData   Object wrapping the session
	 * @param requestParser     Parser to simple retrieve data from the request
	 * @param bom               Reference to the BusinessObjectManager
	 * @param log               Reference to the IsaLocation, needed for logging
	 * @return Forward to another action or page
	 */
	public ActionForward isaPerform(ActionMapping mapping,
									ActionForm form,
									HttpServletRequest request,
									HttpServletResponse response,
									UserSessionData userSessionData,
									RequestParser requestParser,
									BusinessObjectManager bom,
									IsaLocation log)
	throws CommunicationException {
		
        String forward = null;
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
		
		Shop shop = bom.getShop();
		if (shop == null) {
			log.exiting();
			throw new PanicException("Shop not found in BOM");
		}
		
	//	String documentType = (request.getParameter("objecttype") != null ? request.getParameter("objecttype") : "");	
		
	//	if (shop.isSaveBasketAllowed() && documentType != null && documentType.equalsIgnoreCase("basket")) {
		if (shop.isSaveBasketAllowed()) {
			forward = FORWARD_LOADBASKET;
		} else {
			forward = FORWARD_ORDERDETAIL;
		}
		 
		log.exiting();
		return mapping.findForward(forward);
	}
}
