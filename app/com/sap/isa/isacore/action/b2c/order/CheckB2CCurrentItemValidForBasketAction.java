/*****************************************************************************
  Class:        CheckB2CCurrentItemValidForBasketAction
  Copyright (c) 2006, SAP AG, Germany, All rights reserved.
  Created:      06.04.2006
  Version:      1.0

*****************************************************************************/

package com.sap.isa.isacore.action.b2c.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.order.Basket;
import com.sap.isa.businessobject.webcatalog.CatalogBusinessObjectManager;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 * Action to ensure that the basket item is still valid.
 * This action is called when a basket item is displayed in the catalogs detail page.
 * If the basket item is not valid, the SC document GUID will be removed and a initial 
 * explosion of the item will be performed.
 *
 * <p>
 * The following forwards are used by this action:
 * </p>
 * <table border="1" cellspacing="0" cellpadding="2">
 *
 *   <tr><td>FOLLOWUPACTION</td><td>the action to be called</td></tr>
 * </table>
 *
 * @author SAP AG
 * @version 1.0
 */
public class CheckB2CCurrentItemValidForBasketAction extends IsaCoreBaseAction {

	
	/**
	 * Initialize the action.
	 * In B2C this action could be used by anonymous user.
	 * 
	 * @see com.sap.isa.isacore.action.IsaCoreBaseAction#initialize()
	 */
	public void initialize() {
		if (isB2C()) {
			this.checkUserIsLoggedIn = false;
		}	
	}


	/**
	 * Determines the basket and sets the dirty flag. 
	 * The forward will be provided by the request parameter followUpAction * corresponding JSP.
	 *
	 */
	public ActionForward isaPerform(ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response,
			UserSessionData userSessionData,
			RequestParser requestParser,
			BusinessObjectManager bom,
			IsaLocation log) throws CommunicationException {

		boolean isItemValid = false;
		
		// page to be displayed next
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
		String forwardTo = "success";
		
		
		CatalogBusinessObjectManager catBom = (CatalogBusinessObjectManager)userSessionData.getBOM(CatalogBusinessObjectManager.CATALOG_BOM);
		WebCatInfo catalog = catBom.getCatalog();
		WebCatItem webCatItem = catalog.getCurrentItem();
		if (webCatItem == null) {
			if (log.isDebugEnabled()) {
				log.debug("WebCatItem is empty.");
			}
		} else {
    		if (webCatItem.getTechKey() == null) {
				if (log.isDebugEnabled()) {
					log.debug("WebCatItem Key is empty.");
				} 
    		} else {    			
	    		// get the current basket
				Basket basket = bom.getBasket();
				if (basket == null) {
					if (log.isDebugEnabled()) {
						log.debug("Basket not found.");
					}			
				} else {
					// check if item is valid for the basket
					basket.readAllItems();
			    	if (basket.getItem(webCatItem.getTechKey()) != null	) {
			    		isItemValid = true;
			    	}
				}
    		}				
		}
		if (!isItemValid) {
			// reset the SC document and technical key (item key)
			webCatItem.setSCOrderDocumentGuid(null);
			webCatItem.setTechKey(null);
			// perform a new explosion of the solution configuration
			webCatItem.explode();			
		}
		
		log.exiting();
		return mapping.findForward(forwardTo);
		
		
	}
	
	
}