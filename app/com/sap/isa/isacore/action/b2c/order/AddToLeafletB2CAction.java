/*****************************************************************************
    Class         AddToLeafletB2CAction
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Adds an item to the leaflet
    Author:       Sabine Heider
    Created:      05.04.2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/06/26 $
*****************************************************************************/
package com.sap.isa.isacore.action.b2c.order;

// framework dependencies
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.Product;
import com.sap.isa.businessobject.ProductList;
import com.sap.isa.businessobject.item.BasketTransferItem;
import com.sap.isa.businessobject.order.Leaflet;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.ContextConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.action.order.AddToBasketAction;

/**
 * Adds an item to the leaflet.
 *
 * @author Sabine Heider
 * @version 1.0
 *
 */
public class AddToLeafletB2CAction extends IsaCoreBaseAction {


	/**
	 * Initialize the action.
	 * In B2C this action could be used by anonymous user.
	 * 
	 * @see com.sap.isa.isacore.action.IsaCoreBaseAction#initialize()
	 */
	public void initialize() {
		if (isB2C()) {
			this.checkUserIsLoggedIn = false;
		}	
	}


    /**
     * Adds an item to the Leaflet.
     * The item must be available as a request attribute with the name
     * <code>TransferItemB2CAction.RK_TRANSFER_ITEM</code>.
     *
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            BusinessObjectManager bom,
            IsaLocation log) {

        // page to be displayed next
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        String forwardTo = null;

        // get leaflet from BOM
        Leaflet leaflet = bom.getLeaflet();

        // get transfer item from request context
        BasketTransferItem transferItem =
                (BasketTransferItem) request.getAttribute(AddToBasketAction.RC_TRANSFER_ITEM);

        if (transferItem == null) {
            if (log.isDebugEnabled()) {
                log.debug("AddToLeafletB2CAction: transferItem == null");
            }
        }
        else {
            // create product
            Product product = new Product(transferItem);

            // add product to leaflet
            leaflet.addItem(product);
        }

        // get transfer item list from request context
        List transferItemList =
                (List) request.getAttribute(AddToBasketAction.RC_TRANSFER_ITEM_LIST);

        if (transferItemList == null) {
            if (log.isDebugEnabled()) {
                log.debug("AddToLeafletB2CAction: transferItemList == null");
            }
        }
        else {
            // create product list
            ProductList productList = createProductList(transferItemList);

            // add product to leaflet
            leaflet.addItem(productList);
        }


        // update the cookie
        leaflet.addCookieToResponse(response);

        // generate mini basket screen
        forwardTo = "miniBasket";
        
		// determine forward
		RequestParser.Value forwardValue =
			requestParser.getAttribute("addtoleafletforward").getValue();

		// then check request parameter
		if (!forwardValue.isSet()) {
			forwardValue =
				requestParser.getParameter("addtoleafletforward").getValue();
		}
		
		if (forwardValue.isSet()) {
			WebCatInfo webCat = getCatalogBusinessObjectManager(userSessionData).getCatalog();
			if(getInitParameter(ContextConst.IC_SCENARIO).equals("B2C") ) {
			   if (webCat.getCurrentItemList().getQuery() != null &&
				   webCat.getCurrentItemList().getAreaQuery() == null) {
				  forwardTo = "showCatalogQuery";
			   } else {
				 forwardTo = forwardValue.getString();
				 }
			}
			else {
			   forwardTo = forwardValue.getString();
			}
		}
        
		log.exiting();
        return mapping.findForward(forwardTo);
    }


    /**
     * Creates a ProductList from a list of BasketTransferItems
     */
    private ProductList createProductList(List transferItemList) {
        ProductList productList = new ProductList();

        Iterator it = transferItemList.iterator();
        while(it.hasNext()) {
            BasketTransferItem basketTransferItem = (BasketTransferItem) it.next();
            Product product = new Product(basketTransferItem);
            productList.addProduct(product);
        }

        return productList;
    }

}