/*****************************************************************************
    Class         RemoveLeafletItemB2CAction
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Removes an item from the leaflet (B2C scenario)
    Author:       Sabine Heider
    Created:      06.04.2001
    Version:      1.0

    $Revision: #2 $
    $Date: 2001/07/04 $
*****************************************************************************/

package com.sap.isa.isacore.action.b2c.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.order.Leaflet;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.ActionConstants;
import com.sap.isa.isacore.action.IsaCoreBaseAction;


/**
 * Removes an item from the leaflet.
 *
 * @author Sabine Heider
 * @version 1.0
 *
 */
public class RemoveLeafletItemB2CAction extends IsaCoreBaseAction {

    private static final String FORWARD_SUCCESS = "minibasket";

    public void initialize() {
        this.checkUserIsLoggedIn = false;
    }
    
    /**
     * Removes an item from the leaflet.
     * <p>
     * The techKey of the item to be removed is passed by the request in the
     * URL in the form ...?techKey=&lt;techKey&gt;. The specified item is
     * removed from the leaflet. If no techKey is provided, nothing is done.
     * </p> <p>
     * The request is forwarded to the action preparing the display of the
     * mini basket. It must be maintained in the <em>config.xml</em> file using
     * the alias <strong>miniBasket</strong>.
     * </p>
     *
     */
    public ActionForward isaPerform(ActionMapping mapping,
                ActionForm form,
                HttpServletRequest request,
                HttpServletResponse response,
                UserSessionData userSessionData,
                RequestParser requestParser,
                BusinessObjectManager bom,
                IsaLocation log) {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        // next page to be displayed
        String forwardTo = null;

        // get techKey of the item to be removed
        RequestParser.Value techKeyValue =
                requestParser.getParameter("techKey").getValue();

        RequestParser.Value forwardValue =
                requestParser.getAttribute(ActionConstants.RA_FORWARD).getValue();

        if (!forwardValue.isSet()) {
            forwardValue =
                requestParser.getParameter(ActionConstants.RA_FORWARD).getValue();
        }

        if (techKeyValue.isSet()) {
            TechKey techKey = new TechKey(techKeyValue.getString());

            // get leaflet from BOM
            Leaflet leaflet = bom.getLeaflet();

            // remove item from leaflet
            if(leaflet.containsItem(techKey)) {
                leaflet.removeItem(techKey);
            }

            // update the cookie
            leaflet.addCookieToResponse(response);
        }
        else {    // request parameter not set
        	log.exiting();
            throw new PanicException("request parameter 'techKey' not set!");
        }

        if (forwardValue.isSet()) {
            forwardTo = forwardValue.getString();
        }
        else {
            forwardTo = FORWARD_SUCCESS;
        }
		log.exiting();
        return mapping.findForward(forwardTo);
    }

}