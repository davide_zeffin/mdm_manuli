/*****************************************************************************
  Class:        MaintainBasketB2CBasketForwardAction
  Copyright (c) 2002, SAP AG, Germany, All rights reserved.
  Author:
  Created:      08.10.2002
  Version:      1.0

  $Revision: #2 $
  $Date: 2002/11/22 $
*****************************************************************************/

package com.sap.isa.isacore.action.b2c.order;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.order.Basket;
import com.sap.isa.businessobject.order.BasketSplitPartnerEntry;
import com.sap.isa.businessobject.order.Leaflet;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.businesspartner.businessobject.SoldFrom;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreInitAction;
import com.sap.isa.isacore.action.b2c.B2cConstants;

/**
 * Action to forward the items to be orderd from a of soldFrom partners to his shop
 * if the basket is going to be forwarded to the partner shop via oci
 * <p>
 * The following forwards are used by this action:
 * </p>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>forwardbasket</td><td>show the partnershops jsp again and forward the sold from items the partnet shop </td></tr>
 * </table>
 *
 * @author
 * @version 1.0
 */
public class MaintainBasketB2CBasketForwardAction extends MaintainBasketB2CBaseAction {

    // Index of the soldfrom a basket forward should be made
    public static String RC_SOLDFROMOCI_FWD_IDX = "MaintainBasketB2CBasketForwardAction.soldfromocifwdidx";
    // shop url plus to forward to
    public static String RC_SOLDFROMOCI_FWD_URL = "MaintainBasketB2CBasketForwardAction.soldfromocifwdurl";
    // basket as oci
    public static String RC_SOLDFROMOCI_OCI_BSKT = "MaintainBasketB2CBasketForwardAction.soldfromocibskt";
    // if url is invalid this parameter is set
    public static String RC_SOLDFROMOCI_FWD_URL_INVALID = "MaintainBasketB2CBasketForwardAction.soldfromocifwdurlinvalid";

    public static String FORWARD_FORWARDBASKET = "forwardbasket";

    // default OCI version
    public static String OCI_DEFAULT_VERSION = "3.5";

    // default OCI conversion type
    public static String OCI_DEFAULT_TYPE = "HTML";

    public void initialize() {
        this.checkUserIsLoggedIn = false;
    }

    public MaintainBasketB2CBasketForwardAction() {
    }

    protected String basketPerform(
                HttpServletRequest request,
                HttpServletResponse response,
                HttpSession session,
                UserSessionData userSessionData,
                RequestParser parser,
                BusinessObjectManager bom,
                IsaLocation log,
                IsaCoreInitAction.StartupParameter startupParameter,
                boolean multipleInvocation,
                boolean browserBack,
                Shop shop,
                Basket basket,
                Leaflet leaflet)
                    throws CommunicationException {

		final String METHOD_NAME = "basketPerform()";
		log.entering(METHOD_NAME);
        String forwardUrl = "";
        String basketAsOCI = "";
        int forwardIdx = -1;
        ArrayList soldFromListOci;

        RequestParser.Parameter forwadIdxParm = parser.getParameter(RC_SOLDFROMOCI_FWD_IDX);

        if (forwadIdxParm.isSet()) {
            forwardIdx = forwadIdxParm.getValue().getInt();
        }

        // first check, if the action is entered the first time
        soldFromListOci = (ArrayList) userSessionData.getAttribute(SOLDFROMLISTOCI);

        BasketSplitPartnerEntry soldFromOciEntry;

        if (!(forwardIdx < 0 || soldFromListOci == null || forwardIdx > soldFromListOci.size())) {
           soldFromOciEntry = (BasketSplitPartnerEntry) soldFromListOci.get(forwardIdx);
           BusinessPartnerManager bupama = bom.createBUPAManager();
           TechKey soldFromKey = soldFromOciEntry.getSoldFromTechKey();
           BusinessPartner soldFromPartner = bupama.getBusinessPartner(soldFromKey);
           SoldFrom soldFrom = new SoldFrom();
           bupama.addPartnerFunction(soldFromPartner, soldFrom);
           soldFrom.readData(soldFromKey, shop.getLanguage(), null, null, null);
           forwardUrl = soldFrom.getShopURL();

           if (forwardUrl != null && forwardUrl.length() > 0) {

               if (forwardUrl.trim().toLowerCase().indexOf("http://") < 0) {
                    forwardUrl = "http://" + forwardUrl;
               }

               // does the partner except the basket as OCI string ?
               if (soldFrom.getXOCI()) {
                   //convert partner items to OCI message
                   // XML conversion - basketAsOCI = bom.createOCIGenerator().generateXML(shop, basket, OCI_DEFAULT_VERSION, soldFromOciEntry.getSoldFromId());
                   // XML conversion - basketAsOCI =  OCIGenerator.SAP_encode_b64(basketAsOCI);
                   basketAsOCI = bom.createOCIGenerator().generateHTML(shop, basket, OCI_DEFAULT_VERSION, soldFromOciEntry.getSoldFromId());
               }
               soldFromOciEntry.setBasketForwarded(true);

               if (log.isDebugEnabled()) {
                   log.debug("Forward Url: "+ forwardUrl);
                   log.debug("Forward Basket: "+ basketAsOCI);
               }
           }
           else {
               // store shopurl + oci basket of partner in the request
               request.setAttribute(RC_SOLDFROMOCI_FWD_URL_INVALID, "true");
               if (log.isDebugEnabled()) {
                   log.debug("Invalid Url for Partner " + soldFromOciEntry.getSoldFromName());
               }
           }
        }

        // store item list in request context
        request.setAttribute(B2cConstants.RK_BASKET_ITEM_LIST, basket.getItems());
        // store header in request context
        request.setAttribute(B2cConstants.RK_BASKET_HEADER, basket.getHeader());
        // store basket in request context
        request.setAttribute(B2cConstants.RK_BASKET, basket);

        // store shopurl of partner in the request
        request.setAttribute(RC_SOLDFROMOCI_FWD_URL, forwardUrl);

        // store oci basket of partner in the request
        request.setAttribute(RC_SOLDFROMOCI_OCI_BSKT, basketAsOCI);
		log.exiting();
        return FORWARD_FORWARDBASKET;
    }

}