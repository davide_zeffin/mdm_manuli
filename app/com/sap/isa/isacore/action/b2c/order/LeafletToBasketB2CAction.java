/*****************************************************************************
    Class         LeafletToBasketB2CAction
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Moves an item from the leaflet to the basket
    Author:       Sabine Heider
    Created:      26.04.2001
    Version:      1.0
*****************************************************************************/
package com.sap.isa.isacore.action.b2c.order;

// framework dependencies
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.Product;
import com.sap.isa.businessobject.order.Leaflet;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 * Moves an item from the leaflet to the basket.
 *
 * @author Sabine Heider
 * @version 1.0
 *
 */
public class LeafletToBasketB2CAction extends IsaCoreBaseAction {

    /**
     * Constant which will be used to add a specific product from the leaflet
     * to the request for further processing (add to basket).
     */
    public static final String PRODUCT_CONFIGURATION_FROM_LEAFLET = "com.sap.isa.b2c.productConfigurationFromLeaflet";

    public void initialize() {
        this.checkUserIsLoggedIn = false;
    }

    /**
     * Moves an item from the leaflet to the Basket.
     * The item must be available as a request attribute with the name
     * <code>TransferItemB2CAction.RK_TRANSFER_ITEM</code>.
     *
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            BusinessObjectManager bom,
            IsaLocation log) {

        // page to be displayed next
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        String forwardTo = null;

        // get techKey of the item to be moved
        RequestParser.Value techKeyValue =
                requestParser.getParameter("productguid").getValue();

        if (techKeyValue.isSet()) {
            TechKey techKey = new TechKey(techKeyValue.getString());

            Leaflet leaflet = bom.getLeaflet();

            // get the item from the leaflet
            Product product = leaflet.getItem(techKey);
            if (product == null) {
                if (log.isDebugEnabled()) {
                    log.debug("LeafletToBasketB2CAction: item == null");
                }
                forwardTo = "error";
            }
            else {
                // add the configuration of the currently regarded product to the request
                // (from which it will be taken and added to the basket) if there is
                // a configuration at all
                if (product.getProductConfiguration() != null) {
                    request.setAttribute(PRODUCT_CONFIGURATION_FROM_LEAFLET, product.getProductConfiguration());
                }

                // remove item from leaflet
                leaflet.removeItem(techKey);

                // update the cookie
                leaflet.addCookieToResponse(response);

                // forward to the action creating a BasketTransferItem
                forwardTo = "transferitem";

            }
        }
		log.exiting();
        return mapping.findForward(forwardTo);
    }
}