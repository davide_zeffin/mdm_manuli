/*****************************************************************************
  Class:        MaintainCheckoutB2CSaveOrderAction
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       SAP Markets
  Created:      25.03.2001
  Version:      1.0

  $Revision: #9 $
  $Date: 2002/11/26 $
*****************************************************************************/

package com.sap.isa.isacore.action.b2c.order;

import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businessobject.businessevent.PlaceOrderEvent;
import com.sap.isa.businessobject.item.ItemList;
import com.sap.isa.businessobject.order.Basket;
import com.sap.isa.businessobject.order.BasketSplitPartnerEntry;
import com.sap.isa.businessobject.order.Order;
import com.sap.isa.businessobject.order.PartnerListEntry;
import com.sap.isa.businesspartner.backend.boi.BusinessPartnerData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.RecoverBasketCookieHandler;
import com.sap.isa.isacore.action.IsaCoreInitAction;
import com.sap.isa.isacore.action.b2c.B2cConstants;

/**
 * Action to finally save the order to the backend. If the ueser does not
 * confirm the AGBs the flow is forwarded back to the checkout screen.
 * <p>
 * The following forwards are used by this action:
 * </p>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>FORWARD_SAVED</td><td>order was saved successfully</td></tr>
 *   <tr><td>FORWARD_CONFIRM</td><td>AGBs were not accepted, go back to the summary screen</td></tr>
 * </table>
 *
 * @author SAP AG
 * @version 1.0
 */
public class MaintainCheckoutB2CSaveOrderAction extends MaintainCheckoutB2CBaseAction {

    public static final String CHECKOUT_ERROR_MESSAGES = "MaintainCheckoutB2CSaveOrderAction.Order.ErrorMessages";

    public MaintainCheckoutB2CSaveOrderAction() {
    }

    public String checkOutPerform(HttpServletRequest request,
                HttpServletResponse response,
                HttpSession session,
                UserSessionData userSessionData,
                RequestParser parser,
                BusinessObjectManager bom,
                IsaLocation log,
                IsaCoreInitAction.StartupParameter startupParameter,
                boolean multipleInvocation,
                boolean browserBack,
                BusinessEventHandler eventHandler,
                Shop shop,
                Basket basket,
                Order order) throws CommunicationException {

		final String METHOD_NAME = "checkOutPerform()";
		log.entering(METHOD_NAME);
        String forwardTo;

        boolean isDebugEnabled = log.isDebugEnabled();
        
        // forward to the basket if the user has pressed the browser back button
        // and press the button "order" again.
        if (basket == null) {
        	forwardTo = FORWARD_DISPLAY_BASKET;
        	log.exiting();
        	return forwardTo;
        }

        RequestParser.Parameter agreement = parser.getParameter("agreement");

        if (agreement.isSet() && agreement.getValue().getString().equals("accept")) {

            // check for existing relationsship between SoldTo and SoldFrom
            if (shop.isPartnerBasket()) {
                checkSoldFromSoldToRelation(bom, log, order);
            }

            // save basket in own sales backend
            int returnCode;                                    
            // if agreements are used for the TELCO scenario, the agreement will be assigned within this
            // order.createPayment
            // in all another cases the paymenet was created within the MaintainCheckoutB2CConfirmAction
			if (shop.isPaymAtBpEnabled() == true) {
               try {
				   order.createPayment();
               }
			   catch (CommunicationException ex) {
				   forwardTo = FORWARD_PAYMENTERROR;  
				   log.exiting();
				   return forwardTo; 	
			   }
			   
			   
			   if (!order.isValid()){
				forwardTo = FORWARD_PAYMENTERROR;  
				log.exiting();
				return forwardTo; 
			   }
			   
			}
			/*
            if(!order.createPayment()) {
				displayConfirmation(request, bom, shop, order, bom.getBUPAManager(), log);
				forwardTo = FORWARD_PAYMENTERROR;  
				log.exiting();
				return forwardTo;          	
            }
            */	
            returnCode = order.saveOrderAndCommit();       // may throw CommunicationException
            
            if (returnCode == 0) {
            
            	// was there a payment data error -- ONLY FOR R3 BACKENDS
            	if ( (shop.getBackend().equals(Shop.BACKEND_R3) ||
                	  shop.getBackend().equals(Shop.BACKEND_R3_40) ||
                 	  shop.getBackend().equals(Shop.BACKEND_R3_46) ||
                 	  shop.getBackend().equals(Shop.BACKEND_R3_PI) ) &&
                 	 order.getHeader().getPaymentData().getError()) {
                	request.setAttribute(CHECKOUT_ERROR_MESSAGES, order.getMessageList());
                	displayConfirmation(request, bom, shop, order, bom.getBUPAManager(), log);
                	forwardTo = FORWARD_CONFIRM;

            	} else {
            		//Remove stored Ship-To address
            		request.getSession().removeAttribute(B2cConstants.SHIP_TO_ADDRESS);

                	order.readHeader(); // may throw CommunicationException
                	// order.readAllItems(user); // that gets the shipto information, too

                	String ordernumber = new String(order.getHeader().getSalesDocNumber());
//                	request.setAttribute("ordernumber", ordernumber);
					userSessionData.removeAttribute("ordernumber");
					userSessionData.setAttribute("ordernumber", ordernumber);
                	
					if (shop.isManualCampainEntryAllowed()) {
						String descs = "";
						if (order.getHeader() != null && 
							order.getHeader().getAssignedCampaigns() != null && 
							order.getHeader().getAssignedCampaigns().size() > 0) {
								String campGuid = order.getHeader().getAssignedCampaigns().getCampaign(0).getCampaignGUID().getIdAsString();
								String campTypeId = order.getHeader().getAssignedCampaigns().getCampaign(0).getCampaignTypeId();
								descs = (String) basket.getCampaignDescriptions().get(campGuid) ;
								if (campTypeId != null && basket.getCampaignTypeDescriptions().get(campTypeId) != null &&
                                    ((String) basket.getCampaignTypeDescriptions().get(campTypeId)).length() > 0) {
									descs = descs + " (" + basket.getCampaignTypeDescriptions().get(campTypeId) + ")";
								}
						}
//						request.setAttribute("campaign", descs);
						userSessionData.setAttribute("campaign", descs);						
					}

                	// fire event
                	PlaceOrderEvent event = new PlaceOrderEvent(order,bom.getUser());
                	eventHandler.firePlaceOrderEvent(event);

                	// if we are in the channel commerce hub scenario we must delete the items of
                	// the order from the basket and maintain the soldFrom list informations
                	if (updateBasketForOrderSplit(userSessionData, bom, log, shop, basket, order)) {
                    	request.setAttribute("displayOrderSplit", "true");
                    	forwardTo = FORWARD_ORDERSPLIT;
                	}
                	else {
                    	if (shop.isPartnerBasket()) {
	                        userSessionData.setAttribute("partnerBasketNumber", new String(basket.getHeader().getSalesDocNumber()));
    	                }

           	         if (basket!=null && basket.isDocumentRecoverable()) {
            	        // if the basket supports recovery detroy the basket in the backend
                	        if (userSessionData.getAttribute(RecoverBasketCookieHandler.SC_RECO_BASKET_COOKIE_HDLR) != null) {
                    	       // if there is a cookie that stores the basketGUID, destroy it also
                        	   if (isDebugEnabled) {
                            	   log.debug("MaintainCheckoutB2CSaveOrderAction - Delete RecoverBasketCookie");
                           	}
                           	// the Cookie handler has to be created newly, because the response has changed since its creation
                           	RecoverBasketCookieHandler recBasketCookieHdlr = new RecoverBasketCookieHandler(request, response);

                           	recBasketCookieHdlr.delete(shop);
                           	userSessionData.removeAttribute(RecoverBasketCookieHandler.SC_RECO_BASKET_COOKIE_HDLR);
                        	}
                        	// finally delete the basket in the backend
                        	basket.destroyContent();
                    	}

                    	// release basket
                    	bom.releaseBasket();

                    	forwardTo = FORWARD_SAVED;
                	}
            	}
            }
            else {
				request.setAttribute("no_authorization","true");
				displayConfirmation(request, bom, shop, order, bom.getBUPAManager(), log);
				forwardTo = FORWARD_PAYMENTERROR;            	
            }
        }
        else {
            // error message on confirmation page
            request.setAttribute("ack_missing", "true");
            displayConfirmation(request, bom, shop, order, bom.getBUPAManager(), log);
            forwardTo = FORWARD_CONFIRM;
        }
		log.exiting();
        return forwardTo;
    }

    /**
     * Checks if the order was created by an order split. If so, the items in the order
     * are removed from the basket and the SoldFrom informatiosn in the usersession are
     * maintained.
     * @return boolean true if another Split necessary, because there are still unprocessed items
     *                      in the basket
     *                 false if the basket can be deleted
     */
    protected boolean updateBasketForOrderSplit(
                UserSessionData userSessionData,
                BusinessObjectManager bom,
                IsaLocation log,
                Shop shop,
                Basket basket,
                Order order) {
                	
        // determine if we are in the channel commerce hub scenario
        if (!shop.isPartnerBasket()) {
            return false;
        }

        ArrayList soldFromList;
        int soldFromListIdx = 0;

        //determine SOLDFROM infos from usersession
        soldFromList = (ArrayList) userSessionData.getAttribute(MaintainBasketB2COrderSplitAction.SOLDFROMLIST);
        if (userSessionData.getAttribute(MaintainBasketB2COrderSplitAction.SOLDFROMLIST_IDX) != null) {
            soldFromListIdx = ((Integer) userSessionData.getAttribute(MaintainBasketB2COrderSplitAction.SOLDFROMLIST_IDX)).intValue();
        }
        BasketSplitPartnerEntry curSoldFrom =
        (BasketSplitPartnerEntry) soldFromList.get(soldFromListIdx);

        curSoldFrom.setCreatedOrderNumber(order.getHeader().getSalesDocNumber());
        curSoldFrom.setCreatedOrderGUID(order.getTechKey());

        // if we have processed the last SoldFromPartner, we are finished, the
        // basket might be deleted
        if (soldFromListIdx == soldFromList.size() - 1) {
            return false;
        }

        // there are still soldfroms to be processed delete the items of
        // the actual soldfrom in the basket and update SOLDFROMLIST_IDX

        ItemList items = basket.getItems();
        int noOfItems = items.size();
        TechKey[] deleteKeys = new TechKey[noOfItems];
        int deleteIdx = 0;
        TechKey itemSoldFromKey = null;

        for (int i = 0; i < noOfItems; i++) {
            itemSoldFromKey = items.get(i).getPartnerList().getSoldFrom().getPartnerTechKey();
            // if the item belongs to the current SoldFrom it muts be deleted
            if (itemSoldFromKey.getIdAsString().equals(curSoldFrom.getSoldFromTechKey().getIdAsString())) {
                deleteKeys[deleteIdx++] = items.get(i).getTechKey();
            }
        }

        try {
            basket.deleteItems(deleteKeys);
            basket.update(bom.createBUPAManager(), shop);
            basket.read();
        }
        catch (CommunicationException e) {
            if (log.isDebugEnabled()) {
                log.debug(e.toString());
            }
        }

        soldFromListIdx++;
        userSessionData.setAttribute(MaintainBasketB2COrderSplitAction.SOLDFROMLIST_IDX, new Integer(soldFromListIdx));

        return true;
    }

    /** check for existing relationsship between SoldTo and SoldFrom and try to create
     *  it, if not present
     */
    protected void checkSoldFromSoldToRelation(
                BusinessObjectManager bom,
                IsaLocation log,
                Order order) {

        BusinessPartnerManager bupama = bom.createBUPAManager();

        PartnerListEntry soldToEntry = order.getHeader().getPartnerList().getSoldTo();
        PartnerListEntry soldFromEntry = order.getHeader().getPartnerList().getSoldFrom();

        // check if there is a valid relationship between the reseller and the soldto, if not
        // try to create one
        BusinessPartner soldToBuPa = bupama.getBusinessPartner(soldToEntry.getPartnerTechKey());
        BusinessPartner soldFromBuPa = bupama.getBusinessPartner(soldFromEntry.getPartnerTechKey());

        if (!soldToBuPa.checkRelationExists(BusinessPartnerData.SOLDFROM, soldFromBuPa)) {
            if (log.isDebugEnabled()) {
                log.debug("try ro create relationship between soldfrom and soldto");
            }
            // create the relation
            String ret = soldToBuPa.createRelation(soldFromBuPa, BusinessPartnerData.SOLDFROM);
            if (!ret.equalsIgnoreCase("0")) {
                Iterator msgIter = soldToBuPa.getMessageList().iterator();
                while (msgIter.hasNext()) {
                    log.debug((Message) msgIter.next());
                }
            }
        }
    }
}