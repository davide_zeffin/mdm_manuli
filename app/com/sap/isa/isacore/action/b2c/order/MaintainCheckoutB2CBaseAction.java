
/*****************************************************************************
  Class:        MaintainCheckoutB2CBaseAction
  Copyright (c) 2001, SAP AG, All rights reserved.
  Author:
  Created:      22.03.2001
  Version:      1.0

  $Revision: #3 $
  $Date: 2003/03/13 $
*****************************************************************************/

package com.sap.isa.isacore.action.b2c.order;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.Address;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.ProductList;
import com.sap.isa.businessobject.ShipTo;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.item.ItemList;
import com.sap.isa.businessobject.order.Basket;
import com.sap.isa.businessobject.order.Order;
import com.sap.isa.businessobject.order.PartnerListEntry;
import com.sap.isa.businessobject.order.Text;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.businesspartner.businessobject.SoldFrom;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.isacore.ShipToSelection;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.action.IsaCoreInitAction;
import com.sap.isa.isacore.action.b2c.B2cConstants;
import com.sap.isa.maintenanceobject.businessobject.UIElement;
import com.sap.isa.payment.backend.boi.PaymentBaseData;
import com.sap.isa.payment.backend.boi.PaymentMethodData;
import com.sap.isa.payment.businessobject.BankTransfer;
import com.sap.isa.payment.businessobject.COD;
import com.sap.isa.payment.businessobject.Invoice;
import com.sap.isa.payment.businessobject.PaymentBase;
import com.sap.isa.payment.businessobject.PaymentBaseType;
import com.sap.isa.payment.businessobject.PaymentCCard;
import com.sap.isa.payment.businessobject.PaymentMethod;
import com.sap.isa.ui.uicontrol.UIController;
/**
 * Base class for the checkout process in the B2C scenario.
 *
 * @author SAP AG
 * @version 1.0
 */
public abstract class MaintainCheckoutB2CBaseAction extends IsaCoreBaseAction {
	/**
     * Request context constant for storage of the header of the document
     */
    public static final String RC_HEADER         = "header";

    /**
     * Request context constant for storage of the sold to address
     */
    public static final String RC_SOLDTO_ADDRESS = "soldToAddress";

    /**
     * Request context constant for storage of the shipto address
     */
    public static final String RC_SHIPTO_ADDRESS = "shipToAddress";


    /**
     * Request context constant for storage of the shipto address
     */
    public static final String RC_PARTNER_IS_SHIPTO = "isPartnerShipTo";

	/**
	 * Payment object.
	 */
	public static final String RC_PAYMENT = "payment";
    /**
     * Request context constant for storage of the payment type
     */
    public static final String RC_PAYTYPE        = "paytype";
    public static final String RC_PAYMENT_DISABLED = "pay_disabled";
    public static final String RC_PAYTYPE1       = "paytype1";
    public static final String RC_PAYTYPE2       = "paytype2";
/*
    public static final String RC_PAYMETHOD1KEY_CCARD = "paymethod1key_ccard";
    public static final String RC_PAYMETHOD2KEY_CCARD = "paymethod2key_ccard";
    public static final String RC_PAYMETHOD1KEY_BANKTRANSFER = "paymethod1key_banktransfer";
    public static final String RC_PAYMETHOD2KEY_BANKTRANSFER = "paymethod2key_banktransfer";
*/    
    
    /**
     * Request context constant for storage of the card type
     */
    public static final String RC_CARDTYPE       = "cardtype";

    /**
     * Request context constant for storage of the possible months for the card
     */
    public static final String RC_CARDMONTH      = "cardmonths";

    /**
     * Request context constant for storage of the possible years for the card
     */
    public static final String RC_CARDYEARS      = "cardyears";

    
    /**
     * Request context constant for storage of the shipping condition information
     */
    public static final String RC_SHIPCOND       = "shipcond";

    /**
     * Request context constant for storage of a reference to the shop
     */
    public static final String RC_SHOP           = "shop";

    /**
     * Request context constant for storage of the list of messages
     */
    public static final String RC_MESSAGELIST    = "messagelist";

    /**
     * Request context constant for storage of the contry descriptions for the sold to
     */
    public static final String RC_SOLDTO_COUNTRY_DESCRIPTION = "soldToCountryDescription";

    /**
     * Request context constant for storage of the contry descriptions for the ship to
     */
    public static final String RC_SHIPTO_COUNTRY_DESCRIPTION = "shipToCountryDescription";

    /**
     * Request context constant for storage of the contry descriptions for the ship to
     */
    public static final String RC_SOLDFROM_COUNTRY_DESCRIPTION = "soldFromCountryDescription";

    /**
     * Request context constant for storage of the reseller address
     */
    public static final String RC_SOLDFROM_ADDRESS = "soldFromAddress";

     /**
     * Request context constant for storage of the soldfrom terms and conditions
     */
    public static final String RC_SOLDFROM_TERMS_AND_CONDS = "soldFromTermsAndConds";

    /**
     * Constant for available forwards, value is &quot;checkout&quot;.
     */
    protected static final String FORWARD_CHECKOUT     = "checkout";

    /**
     * Constant for available forwards, value is &quot;printout&quot;.
     */
    protected static final String FORWARD_PRINTOUT     = "printout";

    /**
     * Constant for available forwards, value is &quot;confirm&quot;.
     */
    protected static final String FORWARD_CONFIRM      = "confirm";

    /**
     * Constant for available forwards, value is &quot;showshipto&quot;.
     */
    protected static final String FORWARD_SHOW_SHIPTO  = "showshipto";

    /**
     * Constant for available forwards, value is &quot;saved&quot;.
     */
    protected static final String FORWARD_SAVED        = "saved";

    /**
     * Constant for available forwards, value is &quot;ordersplit&quot;.
     */
    protected static final String FORWARD_ORDERSPLIT        = "ordersplit";

    /**
     * Constant for available forwards, value is &quot;paymenterror&quot;.
     */
    protected static final String FORWARD_PAYMENTERROR = "paymenterror";
    
	/**  
	 * Constant for available forwards, value is &quot;invaliddoc&quot;.
	 */
	protected static final String FORWARD_INVALIDDOC = "invaliddoc";

	/**  
	 * Constant for available forwards, value is &quot;displaybasket&quot;.
	 */
	protected static final String FORWARD_DISPLAY_BASKET = "displaybasket";
	
	/**
	 * Request context constant for storage of the delivery priority information
	 */
	public static final String RC_DLV_PRIO       = "dlvprio";	
	
	/**
	  * Request context constant for storage of the contry descriptions for bank transfer
	  */
	 public static final String RC_BANK_TRANSFER_COUNTRY_DESCRIPTION = "bankTransferCountryDescription";
	

    public static final List CARD_MONTHS = new ArrayList();
    public static final List CARD_YEARS = new ArrayList();

    static {
        CARD_MONTHS.add("01");
        CARD_MONTHS.add("02");
        CARD_MONTHS.add("03");
        CARD_MONTHS.add("04");
        CARD_MONTHS.add("05");
        CARD_MONTHS.add("06");
        CARD_MONTHS.add("07");
        CARD_MONTHS.add("08");
        CARD_MONTHS.add("09");
        CARD_MONTHS.add("10");
        CARD_MONTHS.add("11");
        CARD_MONTHS.add("12");

        Calendar cal = Calendar.getInstance();
        int thisYear = cal.get(Calendar.YEAR);

        // offer next 12 years
        for (int year = thisYear; year < thisYear + 12; year++) {
            CARD_YEARS.add("" + year);
        }
    }

    /**
     * Create a new instance.
     */
    public MaintainCheckoutB2CBaseAction() {
    }

    /**
     * Template method to be overwritten by the subclasses of this class.
     * Normally you should overwrite this method and you do not touch the
     * <code>isaPreform</code> method.
     *
     * @param request           request context
     * @param response          response context
     * @param session           unwrapped session context
     * @param userSessionData   object wrapping the session
     * @param requestParser     parser to simple retrieve data from the request
     * @param bom               reference to the BusinessObjectManager
     * @param log               reference to the IsaLocation, needed for logging
     * @param startupParameter  object containing the startup parameters
     * @param eventHandler      object to capture events with
     * @param multipleInvocation flag indicating, that a multiple invocation occured
     * @param browserBack       flag indicating a browser back
     * @param eventHandler      reference to the business event handler
     * @param user              user business object
     * @param shop              shop business object
     * @param basket            the current sales document edited
     * @param order             the order created
     *
     * @return logical key for a forward to another action or jsp
     */
    public abstract String checkOutPerform(HttpServletRequest request,
                HttpServletResponse response,
                HttpSession session,
                UserSessionData userSessionData,
                RequestParser parser,
                BusinessObjectManager bom,
                IsaLocation log,
                IsaCoreInitAction.StartupParameter startupParameter,
                boolean multipleInvocation,
                boolean browserBack,
                BusinessEventHandler eventHandler,
                Shop shop,
                Basket basket,
                Order order)throws CommunicationException;

    /**
     * Overwritten method from the base class.
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            BusinessObjectManager bom,
            IsaLocation log,
            IsaCoreInitAction.StartupParameter startupParameter,
            BusinessEventHandler eventHandler,
            boolean multipleInvocation,
            boolean browserBack) throws CommunicationException {

		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        HttpSession session = request.getSession();

        // Page that should be displayed next.
        String forwardTo = null;

        // get business objects
        Shop shop = bom.getShop();
        Order order = bom.getOrder();
        Basket basket = bom.getBasket();

		// Display basket if no order is available.
		if (order == null) {
			log.exiting();
			return mapping.findForward(FORWARD_DISPLAY_BASKET);			
		}

        forwardTo = checkOutPerform(request, response, session,
                    userSessionData, requestParser, bom,
                    log, startupParameter, multipleInvocation, browserBack,
                    eventHandler, shop, basket, order);
		log.exiting();
        return mapping.findForward(forwardTo);
    }

    /**
     * Performs the necessary steps to display the confirmation page. <br>
     * The sold to address is taken from the soldto in header partner list.
     *
     * @return Page that should be displayed next.
     */
    protected void displayConfirmation(HttpServletRequest request,
			BusinessObjectManager bom,
            Shop shop,
            Order order,
            BusinessPartnerManager bupama,
            IsaLocation log)
                    throws CommunicationException {

		
        order.readShipTos();    // may throw CommunicationException

        HeaderSalesDocument header = order.getHeader();

        Address soldToAddress = getSoldToAddress(bupama, header);

        // get ship-to address
        ShipTo shipTo = header.getShipTo();
        Address shipToAddress = null;

        if (shipTo != null) {
            shipToAddress = shipTo.getAddress();
        }
        else {
            log.debug("shipTo == null");
        }

        // put data in request context
        request.setAttribute(RC_HEADER, header);
        request.setAttribute(RC_SOLDTO_ADDRESS, soldToAddress);
        request.setAttribute(RC_SHIPTO_ADDRESS, shipToAddress);
        request.setAttribute(RC_MESSAGELIST, order.getMessageList());
        UserSessionData sessionData = UserSessionData.getUserSessionData(request.getSession());
        request.setAttribute(RC_SHIPCOND, order.readShipCond(sessionData.getSAPLanguage()));
        request.setAttribute(RC_SHOP, shop);
        request.setAttribute(RC_DLV_PRIO, order.getHeader().getDeliveryPriority());

        //brandowner scenario stuff
        if (shop.isPartnerBasket()) {

            PartnerListEntry soldFromEntry = order.getHeader().getPartnerList().getSoldFrom();
            Address soldFromAddress = null;
            if (soldFromEntry != null) {
                soldFromAddress = soldFromEntry.getPartnerAddress(bupama);
                request.setAttribute(RC_SOLDFROM_ADDRESS, soldFromAddress);
            }

            String soldFromCountryDescription = null;
            if (soldFromAddress != null) {
                soldFromCountryDescription = shop.getCountryDescription(soldFromAddress.getCountry());
            }
            if (soldFromCountryDescription != null) {
                request.setAttribute(RC_SOLDFROM_COUNTRY_DESCRIPTION, soldFromCountryDescription);
            }

            TechKey soldFromKey = soldFromEntry.getPartnerTechKey();
            BusinessPartner soldFromPartner = bupama.getBusinessPartner(soldFromKey);

            SoldFrom soldFrom = (SoldFrom) soldFromPartner.getPartnerFunctionObject(PartnerFunctionData.SOLDFROM);
            if (soldFrom == null) {
                soldFrom = new SoldFrom();
                bupama.addPartnerFunction(soldFromPartner, soldFrom);
                soldFrom.readData(soldFromKey, shop.getLanguage(), null, null, null);
            }
            String soldFromTermsAndConds = soldFrom.getAGB();
            if (soldFromTermsAndConds == null) {
                soldFromTermsAndConds = "";
            }

            request.setAttribute(RC_SOLDFROM_TERMS_AND_CONDS, WebUtil.encodeURL(soldFromTermsAndConds,request));
        }

        String soldToCountryDescription = shop.getCountryDescription(soldToAddress.getCountry());
        String shipToCountryDescription = null;
        if (shipToAddress != null) {
            shipToCountryDescription = shop.getCountryDescription(shipToAddress.getCountry());
        }

        if (soldToCountryDescription != null) {
            request.setAttribute(RC_SOLDTO_COUNTRY_DESCRIPTION, soldToCountryDescription);
        }

        if (shipToCountryDescription != null) {
            request.setAttribute(RC_SHIPTO_COUNTRY_DESCRIPTION, shipToCountryDescription);
        }

        request.setAttribute(B2cConstants.RK_BASKET_ITEM_LIST, order.getItems());
        request.setAttribute("shipcondDescription", getShipCondDescr(order, shop));
        request.setAttribute("commentHeader", header.getText().getText());
        
        // PAYMENT
        // -------------------------------------------------------------------
        PaymentBase paymentBase = order.getPayment();
        
        request.setAttribute(RC_PAYMENT, order.getPayment());
        request.setAttribute(RC_PAYTYPE1, paymentBase.getPaymentTypeByTechnicalKey(RC_PAYTYPE1));
        request.setAttribute(RC_PAYTYPE2, paymentBase.getPaymentTypeByTechnicalKey(RC_PAYTYPE2));        
        request.setAttribute(RC_CARDTYPE, order.readCardType());        
        request.setAttribute(RC_CARDMONTH, CARD_MONTHS);
        request.setAttribute(RC_CARDYEARS, CARD_YEARS);
//        request.setAttribute("paytypeDescription", getPaymentTypeDescr(request, order));
        
        Iterator iter = paymentBase.getPaymentMethods().iterator();
        String bankTransferCountryID = "";
        while (iter.hasNext() && (bankTransferCountryID.equals(""))) {
        	PaymentMethodData payMethod = (PaymentMethodData) iter.next();
        	if (payMethod instanceof BankTransfer) {
        		BankTransfer bankTransfer = (BankTransfer)payMethod;
        		bankTransferCountryID = bankTransfer.getCountry();
        		if(bankTransferCountryID != null) {
        			bankTransfer.setCountryDescription(shop.getCountryDescription(bankTransferCountryID));
        		}
        	}
        }
        
        // Get the product information to add it to the item list
        WebCatInfo catalog = getCatalogBusinessObjectManager(UserSessionData.getUserSessionData(request.getSession())).getCatalog();
        ItemList items = order.getItems();
        ProductList.enhance(catalog, items.toArray());
    }


	/**
	 * Return the address for the sold to found in the partnerlist of the
	 * given document header. <br>
	 * 
	 * @param bupama business object manager to read the business partner infos
	 * @param header document header of the order object.
	 * 
	 * @return business partner address.
	 */
    protected Address getSoldToAddress(BusinessPartnerManager bupama,
        								HeaderSalesDocument header) {
        
        // get sold-to address
        TechKey soldToKey = header.getPartnerList().getSoldTo().getPartnerTechKey();
        
        BusinessPartner soldTo = bupama.getBusinessPartner(soldToKey);
        
        Address soldToAddress = null;
        if (soldTo != null) {
        	soldToAddress = soldTo.getAddress();
        }
        
        return soldToAddress;
    }


    /**
     * Performs the necessary steps to display the confirmation page.
     *
     * @return Page that should be displayed next.
     */
    protected void setRequestCheckout(HttpServletRequest request,
            HttpSession session,
			BusinessObjectManager bom,
            Shop shop,
            Order order,
            BusinessPartnerManager bupama,
            IsaLocation log)
                    throws CommunicationException {

						
        boolean isDebugEnabled = log.isDebugEnabled();

        order.readShipTos();

        // get sold-to address
        Address soldToAddress = getSoldToAddress(bupama,order.getHeader());   

        // get ship-to address
        ShipTo shipTo = order.getHeader().getShipTo();
        Address shipToAddress = null;
		// get the list of countries from the shop Object
		ResultData listOfCountries = shop.getCountryList();
		 // get the default country for the given shop
		String defaultCountryId = shop.getDefaultCountry();
		 // flag that indicates whether a region is needed to complete an address
        
        if (shipTo != null) {
            shipToAddress = shipTo.getAddress();
        }
        else {
            if (isDebugEnabled) {
                log.debug("isaPerform(): shipTo is null");
            }
        }

        // put data in request context
        request.setAttribute(RC_HEADER, order.getHeader());
        request.setAttribute(RC_SOLDTO_ADDRESS, soldToAddress);
        request.setAttribute(RC_SHIPTO_ADDRESS, shipToAddress);

        if (shop.isPartnerBasket()) {
            String msgText = null;
            Message msg = null;
            // get matching description
            ServletContext context = getServlet().getServletContext();
            
			ShipToSelection shipToSelection = (ShipToSelection) session.getAttribute(B2cConstants.SC_SHIPTO_SELECTION);
			request.setAttribute(RC_PARTNER_IS_SHIPTO, shipToSelection.isPartnerShipTo(order.getHeader().getShipTo())?"true":"false");

            TechKey soldFromKey = order.getHeader().getPartnerList().getSoldFrom().getPartnerTechKey();
            BusinessPartner soldFromPartner = bupama.getBusinessPartner(soldFromKey);
            SoldFrom soldFrom = (SoldFrom) soldFromPartner.getPartnerFunctionObject(PartnerFunctionData.SOLDFROM);
            if (soldFrom == null) {
                soldFrom = new SoldFrom();
                bupama.addPartnerFunction(soldFromPartner, soldFrom);
                soldFrom.readData(soldFromKey, shop.getLanguage(), null, null, null);
            }

            if (soldFrom.getPaymentType() != null) {
                PaymentBaseType payType1 = (PaymentBaseType)soldFrom.getPaymentType();
                PaymentBaseType payType2 = (PaymentBaseType)payType1.clone();
            	request.setAttribute(RC_PAYTYPE1, (PaymentBaseType)soldFrom.getPaymentType());
                request.setAttribute(RC_PAYTYPE2, payType2);
            }
            else {
               msgText = WebUtil.translate(context, session, "b2c.order.partner.nopayment", null);
               msg = new Message(Message.ERROR, "b2c.order.partner.nopayment");
               msg.setDescription(msgText);
               order.addMessage(msg);
            }
            request.setAttribute(RC_CARDTYPE,soldFrom.getCardType());

            request.setAttribute(RC_SHIPCOND, soldFrom.getShippingCond());
            if (soldFrom.getShippingCond().getNumRows() == 0) {
               msgText = WebUtil.translate(context, session, "b2c.order.partner.noshiconds", null);
               msg = new Message(Message.ERROR, "b2c.order.partner.noshiconds");
               msg.setDescription(msgText);
               order.addMessage(msg);
            }
        }
        else {
        	PaymentBase paymentBase = order.getPayment();
        	
            request.setAttribute(RC_PAYTYPE1, paymentBase.getPaymentTypeByTechnicalKey(RC_PAYTYPE1));
            request.setAttribute(RC_PAYTYPE2, paymentBase.getPaymentTypeByTechnicalKey(RC_PAYTYPE2));

            request.setAttribute(RC_CARDTYPE, order.readCardType());
            UserSessionData sessionData = UserSessionData.getUserSessionData(request.getSession());
            request.setAttribute(RC_SHIPCOND, order.readShipCond(sessionData.getSAPLanguage()));
        }
 

		request.setAttribute(RC_MESSAGELIST, order.getMessageList());
        request.setAttribute(RC_PAYMENT, order.getPayment());
        request.setAttribute(RC_CARDMONTH, CARD_MONTHS);
        request.setAttribute(RC_CARDYEARS, CARD_YEARS);
        request.setAttribute(RC_SHOP, shop);
        request.setAttribute(B2cConstants.RK_BASKET_ITEM_LIST, order.getItems());
//         request.setAttribute("paytypeDescription", getPaymentTypeDescr(request, order));
        request.setAttribute("shipcondDescription", getShipCondDescr(order, shop));
		request.setAttribute(B2cConstants.POSSIBLE_COUNTRIES,listOfCountries);
		request.setAttribute(B2cConstants.DEFAULT_COUNTRY, defaultCountryId);
        
        String soldToCountryDescription = shop.getCountryDescription(soldToAddress.getCountry());
        String shipToCountryDescription = shop.getCountryDescription(shipToAddress.getCountry());

        if (soldToCountryDescription != null) {
            request.setAttribute(RC_SOLDTO_COUNTRY_DESCRIPTION, soldToCountryDescription);
        }
        if (shipToCountryDescription != null) {
            request.setAttribute(RC_SHIPTO_COUNTRY_DESCRIPTION, shipToCountryDescription);
        }

        //brandowner scenario stuff
        if (shop.isPartnerBasket()) {

            PartnerListEntry soldFromEntry = order.getHeader().getPartnerList().getSoldFrom();
            Address soldFromAddress = null;
            if (soldFromEntry != null) {
                soldFromAddress = soldFromEntry.getPartnerAddress(bupama);
                request.setAttribute(RC_SOLDFROM_ADDRESS, soldFromAddress);
            }

            String soldFromCountryDescription = null;
            if (soldFromAddress != null) {
                soldFromCountryDescription = shop.getCountryDescription(soldFromAddress.getCountry());
            }
            if (soldFromCountryDescription != null) {
                request.setAttribute(RC_SOLDFROM_COUNTRY_DESCRIPTION, soldFromCountryDescription);
            }
        }
		 PaymentBaseData payment = order.getPayment();
		 Iterator iter = payment.getPaymentMethods().iterator();
		 String bankTransferCountryID = "";
		 while (iter.hasNext() && (bankTransferCountryID.equals(""))) {
			 PaymentMethodData payMethod = (PaymentMethodData) iter.next();
			 if (payMethod instanceof BankTransfer) {
				 bankTransferCountryID = ((BankTransfer) payMethod).getCountry();
			 }
		 }
		 request.setAttribute(RC_BANK_TRANSFER_COUNTRY_DESCRIPTION, shop.getCountryDescription(bankTransferCountryID));

        // Get the product information to add it to the item list
        WebCatInfo catalog = getCatalogBusinessObjectManager(UserSessionData.getUserSessionData(session)).getCatalog();
        ItemList items = order.getItems();
        ProductList.enhance(catalog, items.toArray());
    }

    /**
     * Helper method to parse the request.
     * <p>
     * The following fields are taken from the request and used by this method:
     * </p>
     * <table border="1" cellspacing="0" cellpadding="2">
     *   <tr>
     *     <td><b>field</b></td>
     *     <td><b>use</b></td>
     *   </tr>
     *   <tr>
     *     <td>paytype</td>
     *     <td>type of payment choosen (e.g. credit card)</td>
     *   </tr>
     *   <tr>
     *     <td>nolog_cardtype</td>
     *     <td>type of the credit card selected</td>
     *   </tr>
     *   <tr>
     *     <td>nolog_cardno</td>
     *     <td>number of the credit card</td>
     *   </tr>
     *   <tr>
     *     <td>nolog_cardholder</td>
     *     <td>holder of the credit card</td>
     *   </tr>
     *   <tr>
     *     <td>nolog_cardmonth</td>
     *     <td>month of the valid to date of the credit card</td>
     *   </tr>
     *   <tr>
     *     <td>nolog_cardyear</td>
     *     <td>year of the valid to date of the credit card</td>
     *   </tr>
     *   <tr>
     *     <td>shipcond</td>
     *     <td>selected shipping condition</td>
     *   </tr>
     *   <tr>
     *     <td>comment</td>
     *     <td>comments entered by the user</td>
     *   </tr>
     * </table>
     * <br><br>
     *
     * @param parser The request, wrapped into a parser object
     * @param order the order
     * @param shop the shop
     * @param log reference to the logging context
     */
    protected void parseRequest(RequestParser parser,
                                  Order order,
                                  Shop shop,
                                  IsaLocation log) throws CommunicationException {

      
        RequestParser.Parameter parsepaymreqdata =
                parser.getParameter("parsepaymreqdata[]");
        RequestParser.Parameter paytypekey =
                parser.getParameter("paytypekey[]");
        RequestParser.Parameter paytype =
                parser.getParameter("paytype[]");
		RequestParser.Parameter paymethodtechkey =
				parser.getParameter("paymethodtechkey[]");
        RequestParser.Parameter usagescope =
                parser.getParameter("usagescope[]");
        RequestParser.Parameter cardtype =
                parser.getParameter("nolog_cardtype[]");
        RequestParser.Parameter nolog_cardno =
                parser.getParameter("nolog_cardno[]");
        RequestParser.Parameter nolog_cardcvv =
                parser.getParameter("nolog_cardcvv[]");
        RequestParser.Parameter nolog_cardno_suffix =
                parser.getParameter("nolog_cardno_suffix[]");
        RequestParser.Parameter cardholder =
                parser.getParameter("nolog_cardholder[]");
        RequestParser.Parameter cardmonth =
                parser.getParameter("nolog_cardmonth[]");
        RequestParser.Parameter cardyear =
                parser.getParameter("nolog_cardyear[]");
        RequestParser.Parameter shipcond =
                parser.getParameter("shipcond");
        RequestParser.Parameter comment =
                parser.getParameter("comment");
        RequestParser.Parameter deliveryPriority =
        		parser.getParameter("deliveryPriority");        
        // bank transfer details		
		RequestParser.Parameter debitauthority =
				parser.getParameter("debitauthority[]");
		RequestParser.Parameter nolog_accountholder =
				parser.getParameter("nolog_accountholder[]");
		RequestParser.Parameter nolog_accountnumber = 
		        parser.getParameter("nolog_accountnumber[]");
		RequestParser.Parameter routingname = 
				parser.getParameter("routingname[]");
		RequestParser.Parameter bankname = 
				parser.getParameter("bankname[]");		
		RequestParser.Parameter country = 
				parser.getParameter("country[]");										
       
        // update the payment data
        PaymentBase payment = order.getPayment();
        payment.setError(false);
        payment.clearMessages();        
        
        for (int i = 0; i < paytypekey.getNumValues(); i++) {

            String pTypeKeyStr = paytypekey.getValue(i).getString();
            String pTypeStr = paytype.getValue(i).getString();  

            // If usagescope is set to "pay_disapled", this payment data shouldn't be parsed
            // Additionally, if payment data was already maintained for this pTypeKeyStr, this
            // data should be removed as well
            if(usagescope.getValue(i).getString().length() > 0 &&
                    usagescope.getValue(i).getString().equalsIgnoreCase(RC_PAYMENT_DISABLED)) {
                
                payment.removePaymentMethodsByPaymentBaseTypeRef(pTypeKeyStr);
                continue;
            }

            // If the payment methods shouldn't be paresed, they shouldn't be removed from the
            // list of  payment methods in the PaymentBase object.
            if(parsepaymreqdata.getValue(i).getString().length() > 0 &&
                    parsepaymreqdata.getValue(i).getString().equalsIgnoreCase("false")) {
        
                // maybe the usage scope was changed and it need to be overwriten within the 
                // already stored paymentMethod
                PaymentMethod paymentMethod = payment.getPaymentMethodByTechnicalKey(paymethodtechkey.getValue(i).getString());
                paymentMethod.setUsageScope(usagescope.getValue(i).getString());
                continue;
            }

            // If the payment methods should be paresed, they should be removed from the
            // list of  payment methods in the PaymentBase object. Thereafter the request data should 
            // be used to create new PaymentMethod objects 
            if(parsepaymreqdata.getValue(i).getString().length() > 0 &&
                    parsepaymreqdata.getValue(i).getString().equalsIgnoreCase("true")) {
        
                payment.removePaymentMethodsByPaymentBaseTypeRef(pTypeKeyStr);
            }

        	PaymentBaseType pType = payment.getPaymentTypeByTechnicalKey(pTypeKeyStr);
        	if(pType != null) {
            	if (paytype.getValue(i).getString().length() > 0) {
            		pType.setCheckedPaytypeTechKey(new TechKey(pTypeStr));
                }       		
        	}
        	
            PaymentMethod paymentMethod = payment.createPaymentMethodByPaymentType(pTypeStr);
            paymentMethod.setPayTypeBaseTechKey(new TechKey(pTypeKeyStr));
            paymentMethod.setUsageScope(usagescope.getValue(i).getString());

            if (paymentMethod instanceof PaymentCCard){ 
                
            	PaymentCCard paymentCCard = (PaymentCCard)paymentMethod;
                
                if (cardtype.getValue(i).getString().length() == 0){
					payment.addMessage(new Message(Message.ERROR, "b2c.checkout.cctype.missing"));
                	payment.setError(true);
                }
                else {
                    paymentCCard.setTypeTechKey(new TechKey(cardtype.getValue(i).getString()));
                }
                
                if (nolog_cardno.getValue(i).getString().length() == 0){
					payment.addMessage(new Message(Message.ERROR, "b2c.checkout.ccno.missing"));
										payment.setError(true);                	
                }
                else {
                    paymentCCard.setNumber(nolog_cardno.getValue(i).getString());
                }
                
                if (nolog_cardcvv.getValue(i).getString().length() > 0) {
                    paymentCCard.setCVV(nolog_cardcvv.getValue(i).getString());
                }
                
                if (nolog_cardno_suffix.getValue(i).getString().length() > 0) {
                    paymentCCard.setNumberSuffix(nolog_cardno_suffix.getValue(i).getString());
                }
                
                if (cardholder.getValue(i).getString().length() == 0){
                	// The card holder should be checked by the order and a corresponding
                	// message should come from the order as well. In the order message
                	// log customizing it could be declared it the message should have severity 
                	// error or warning. 
					//payment.addMessage(new Message(Message.ERROR, "b2c.checkout.ccholder.missing"));
					//payment.setError(true);                	
                }
                else {
                    paymentCCard.setHolder(cardholder.getValue(i).getString());
                }
                
                if (cardmonth.getValue(i).getString().length() > 0) {
                    paymentCCard.setExpDateMonth(cardmonth.getValue(i).getString());
                }
                
                if (cardyear.getValue(i).getString().length() > 0) {
                    paymentCCard.setExpDateYear(cardyear.getValue(i).getString());
                }

                payment.addPaymentMethod(paymentMethod);
            }		 
            else if (paymentMethod instanceof BankTransfer) {
                
            	BankTransfer bankTransfer = (BankTransfer) paymentMethod;
             
                if (debitauthority.getValue(i).getString().length() > 0) {
                    bankTransfer.setDebitAuthority(debitauthority.getValue(i).getString());
                }
                
                if (nolog_accountholder.getValue(i).getString().length() == 0){
					payment.addMessage(new Message(Message.ERROR, "b2c.checkout.accholder.missing"));
					payment.setError(true);                         	
                }
                else  {
                    bankTransfer.setAccountHolder(nolog_accountholder.getValue(i).getString());
                }
                
                if (nolog_accountnumber.getValue(i).getString().length() == 0){
					payment.addMessage(new Message(Message.ERROR, "b2c.checkout.accno.missing"));
					payment.setError(true);                 	
                }
                else  {
                    bankTransfer.setAccountNumber(nolog_accountnumber.getValue(i).getString());
                }
                
                if (routingname.getValue(i).getString().length() == 0){
					payment.addMessage(new Message(Message.ERROR, "b2c.checkout.accrout.missing"));
					payment.setError(true); 
                }
                else  {
                    bankTransfer.setRoutingName(routingname.getValue(i).getString());
                }
                if (bankname.getValue(i).getString().length() > 0) {
                    bankTransfer.setBankName(bankname.getValue(i).getString());
                }
                if (country.getValue(i).getString().length() > 0) {
                    bankTransfer.setCountry(country.getValue(i).getString());
                    bankTransfer.setCountryDescription(shop.getCountryDescription(country.getValue(i).getString()));
                }		
   		   
                payment.addPaymentMethod(paymentMethod);
            }			 
            else if (paymentMethod instanceof COD) {
            	
                payment.addPaymentMethod(paymentMethod);
            }	
            else if (paymentMethod instanceof Invoice) {

                payment.addPaymentMethod(paymentMethod);
            }	
        }
        
		HeaderSalesDocument header = order.getHeader();  

        // is used in parallel with
        header.setPaymentData((PaymentBase)payment);
          
        // update shipping condition
        if (shipcond.isSet()) {
            header.setShipCond(shipcond.getValue().getString());
        }

        // update text
        if (comment.isSet()) {
            header.setText(new Text("1000", comment.getValue().getString()));
        }
        
        // delivery priority
        if (deliveryPriority.isSet()) {
        	header.setDeliveryPriority(deliveryPriority.getValue().getString());
        }

        order.setHeader(header);
    }


    /**
     * Returns the description for the selected payment type.
     */
    protected String getPaymentTypeDescr(HttpServletRequest request,
                                       Order order)
                                       throws CommunicationException {
      
        String description = "";

        // get possible payment types
        PaymentBaseType paytype = (PaymentBaseType) order.readPaymentType();  // may throw CommunicationException

        // get selected payment type
        PaymentBase paymentHeader = (PaymentBase) order.getHeader().getPaymentData();
        if (paymentHeader == null) {
            paymentHeader = new PaymentBase();
        }
        if ( paymentHeader.getPaymentMethods() != null && paymentHeader.getPaymentMethods().size() > 0 ) {        
         PaymentMethodData payMethod = (PaymentMethod) paymentHeader.getPaymentMethods().get(0);        
         if (payMethod != null) {        
        	// get techkey for selected paytype
        	TechKey paytypeTechKey = payMethod.getPayTypeTechKey();
        	
        	if (paytypeTechKey == null) {
            	paytypeTechKey = paytype.getDefault();
        	}

       		 // get matching description
        	ServletContext context = super.getServlet().getServletContext();
        	HttpSession session = request.getSession();

        	if (paytypeTechKey.equals(paytype.getInvoice())) {
            	description = WebUtil.translate(context, session,
               	     "b2c.order.confirm.value.invoice", null);
        	}
        	else if (paytypeTechKey.equals(paytype.getCOD())) {
           	 description = WebUtil.translate(context, session,
            	        "b2c.order.confirm.value.cod", null);
        	}
        	else if (paytypeTechKey.equals(paytype.getCard())) {
           	 description = WebUtil.translate(context, session,
                	    "b2c.order.confirm.value.card", null);
        	}
        	else if (paytypeTechKey.equals(paytype.getBankTransfer())) {
				description = WebUtil.translate(context, session,
								"b2c.checkout.withBankTransfer", null);
        	}
        	else {
           		 description = WebUtil.translate(context, session,
                    "b2c.order.confirm.value.unknown", null);
        	}
        }
        } 
        return description;
    }


    /**
     * Returns the description for the selected shipping condition.
     */
    protected String getShipCondDescr(Order order, Shop shop)
            throws CommunicationException {
        // list of possible shipping conditions
        ResultData shipconds = order.readShipCond(shop.getLanguage());  // may throw CommunicationException

        // key of selected shipping condition
        String shipcondKey = order.getHeader().getShipCond();


        // get matching description
        String description = null;
        if (shipcondKey != null) {
            if ("".equals(shipcondKey)) {
                description = "Description not found!";
            }
            else {
                shipconds.beforeFirst();
                while (shipconds.next()) {
                    if (shipcondKey.equals(shipconds.getRowKey().toString())) {
                        description = shipconds.getString(1);
                        break;
                    }
                }
            }
        }

        return description;
    }
    
    
	/**
	 * Return if the UIElement with the given name is visible. <br>
	 * 
	 * @param fieldName name of the ui field
	 * @param userSessionData
	 * @return <code>true</code> if the element exist and is visible. 
	 */
    protected boolean isElementVisible(String fieldName, UserSessionData userSessionData) {
		
	    UIController uiController = UIController.getController(userSessionData);	 	
        UIElement uiElement = uiController.getUIElement(fieldName);

		
	    if (uiElement != null && !uiElement.isHidden()) {
		     return true;
	    }
		
	    return false;
   }
    
}