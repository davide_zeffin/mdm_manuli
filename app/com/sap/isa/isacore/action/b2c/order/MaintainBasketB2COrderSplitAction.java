/*****************************************************************************
  Class:        MaintainBasketB2COrderSplitAction
  Copyright (c) 2002, SAP AG, Germany, All rights reserved.
  Author:
  Created:      08.10.2002
  Version:      1.0

  $Revision: #2 $
  $Date: 2002/11/21 $
*****************************************************************************/

package com.sap.isa.isacore.action.b2c.order;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.item.ItemList;
import com.sap.isa.businessobject.order.Basket;
import com.sap.isa.businessobject.order.BasketSplitPartnerEntry;
import com.sap.isa.businessobject.order.Leaflet;
import com.sap.isa.businessobject.order.PartnerListEntry;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreInitAction;

/**
 * Action to checkout a multisoldFrom basket, by spltting the basket into several orders
 * <p>
 * The following forwards are used by this action:
 * </p>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>checkout</td><td>the user is logged in and can continue to the checkout</td></tr>
 * </table>
 *
 * @author
 * @version 1.0
 */
public class MaintainBasketB2COrderSplitAction extends MaintainBasketB2CBaseAction {
	
   public MaintainBasketB2COrderSplitAction() {
    }

    protected String basketPerform(
                HttpServletRequest request,
                HttpServletResponse response,
                HttpSession session,
                UserSessionData userSessionData,
                RequestParser parser,
                BusinessObjectManager bom,
                IsaLocation log,
                IsaCoreInitAction.StartupParameter startupParameter,
                boolean multipleInvocation,
                boolean browserBack,
                Shop shop,
                Basket basket,
                Leaflet leaflet)
                    throws CommunicationException {

		final String METHOD_NAME = "basketPerform()";
		log.entering(METHOD_NAME);
        String forwardTo = null;
        ArrayList soldFromList;
        int soldFromListIdx = 0;

        // first check, if the action is entered the first time
        soldFromList = (ArrayList) userSessionData.getAttribute(SOLDFROMLIST);
        if (userSessionData.getAttribute(SOLDFROMLIST_IDX) != null) {
            soldFromListIdx = ((Integer) userSessionData.getAttribute(SOLDFROMLIST_IDX)).intValue();
        }

        if (soldFromList == null ||
           !((BasketSplitPartnerEntry) soldFromList.get(0)).getBasketGuid().getIdAsString().equals(basket.getTechKey().getIdAsString())
            ) { // new soldFromList must be created
            soldFromList = new ArrayList();
            soldFromListIdx = 0;
        }
        else {
            // we have to check for new soldFroms or soldFroms that were already
            // checked out and got new items meanwhile, we also have to take care about
            // soldFroms that are not needed anymore, because all items have benn deleted
            // without ordering
            // so delete all SoldFroms from the SoldFrom list, where not order was created
            // and add all soldfroms from teh basket again
            Iterator soldFromListIter = soldFromList.iterator();
            BasketSplitPartnerEntry soldFromIterEntry;

            while (soldFromListIter.hasNext()) {
               soldFromIterEntry = (BasketSplitPartnerEntry) soldFromListIter.next();
               if (soldFromIterEntry.getCreatedOrderNumber() == null ||
                   soldFromIterEntry.getCreatedOrderNumber().trim().length() == 0) {
                   soldFromListIter.remove();
               }
            }
        }

        ItemList items = basket.getItems();
        PartnerListEntry soldFrom;
        String soldFromNameAndCity = null;
        String soldFromName = null;
        String soldFromCity = null;
        String soldFromKeyStr = null;
        int colIdx = 0;
        HashSet soldFromSet = new HashSet(); // this is used to quickly find out, if the soldfrom was already added

        for (int i = 0; i<items.size(); i++) {
            soldFrom = items.get(i).getPartnerList().getSoldFrom();
            soldFromKeyStr = soldFrom.getPartnerTechKey().getIdAsString();
            if (!soldFromSet.contains(soldFromKeyStr)) {
                // new soldFrom found
                soldFromSet.add(soldFromKeyStr);
                soldFromNameAndCity = soldFrom.getPartnerNameAndCity(bom.createBUPAManager());
                colIdx = soldFromNameAndCity.indexOf(",");
                soldFromName = soldFromNameAndCity.substring(0, colIdx);
                soldFromCity = soldFromNameAndCity.substring(colIdx + 2);
                soldFromList.add(new BasketSplitPartnerEntry(basket.getTechKey(),
                                                             new TechKey(soldFromKeyStr),
                                                             soldFrom.getPartnerId(),
                                                             soldFromName,
                                                             soldFromCity));
            }
        }

        userSessionData.setAttribute(SOLDFROMLIST, soldFromList);
        userSessionData.setAttribute(SOLDFROMLIST_IDX, new Integer(soldFromListIdx));

        BasketSplitPartnerEntry soldFromEntry = (BasketSplitPartnerEntry) soldFromList.get(soldFromListIdx);

        forwardTo = createOrderForSoldFrom(request, shop, basket, soldFromEntry.getSoldFromTechKey(),
                                           soldFromEntry.getSoldFromId(), bom, log, userSessionData);
		log.exiting();
        return forwardTo;
    }

}