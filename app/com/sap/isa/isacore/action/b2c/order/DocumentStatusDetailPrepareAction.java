/*****************************************************************************
    Class:        DocumentStatusDetailPrepareBaseAction
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author        SAP
    Created:      May 2001

    $Revision: #3 $
    $Date: 2002/11/22 $
*****************************************************************************/

package com.sap.isa.isacore.action.b2c.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.ecommerce.businessobject.loyalty.LoyaltyMembership;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.order.OrderStatus;
import com.sap.isa.core.Constants;
import com.sap.isa.core.ContextConst;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.action.loyalty.LoyaltyUtilities;

/**
 * Prepares to show a documents detail
 *
 * <br> Note that all Action classes have to provide a non argument constructor
 * because they were instantiated automatically by the action servlet.
 *
 */
public class DocumentStatusDetailPrepareAction extends IsaCoreBaseAction {

    /**
    * sales document stored in request scope.
    */
    public static final String RK_ORDER_STATUS_DETAIL = "orderstatusdetail";
    public static final String RK_ORDER_STATUS_DETAIL_ITEMITERATOR = "itemiterator";
    public static final String RK_SOLDTO_NAME = "soldtoname";
    public static final String RK_SOLDTO_TECHKEY = "soldtotechkey";
    public static final String RK_DOCUMENTID = "documentid";


    /**
     *
     * creates the Request Parameter which must be given to this action
     *
     * @param orderHeader OrderHeader
     *
     * @return String with the request parameter
     *
     */
    public static String createParameter(HeaderSalesDocument orderheader) {

        StringBuffer parameter = new StringBuffer();

        parameter.append('?').append("techkey=").append(orderheader.getTechKey())
        .append('&').append("object_id=").append(orderheader.getSalesDocNumber())
        .append('&').append("objects_origin=").append(orderheader.getSalesDocumentsOrigin())
        .append('&').append("objecttype=").append(orderheader.getDocumentType());

        return parameter.toString();
    }


    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
                                    ActionForm form,
                                    HttpServletRequest request,
                                    HttpServletResponse response,
                                    UserSessionData userSessionData,
                                    RequestParser requestParser,
                                    BusinessObjectManager bom,
                                    IsaLocation log)
    throws CommunicationException {

		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        TechKey documentKey = null;
        if( (request.getParameter("techkey")) != null) {
            documentKey = new TechKey(request.getParameter("techkey"));
        }
        String documentId = (request.getParameter("object_id") != null ? request.getParameter("object_id") : "");
        String documentsOrigin = (request.getParameter("objects_origin") != null ? request.getParameter("objects_origin") : "");
        String documentType = (request.getParameter("objecttype") != null ? request.getParameter("objecttype") : "");

        String billing = request.getParameter("billing");

        // Called in crossentry sequence
        String externalForward = null;
        if( (request.getParameter("techkey")) == null) {
            // check request attributes
            documentKey = new TechKey((String)request.getAttribute("techkey"));
            documentId = (String)request.getAttribute("object_id");
            documentType = (String)request.getAttribute("objecttype");
            billing = (String)request.getAttribute("billing");
            externalForward = (String)request.getAttribute("externalforward");
        }


        Shop shop = bom.getShop();

        // Check can be removed lateron
        if(documentType == null) {
            documentType = "";
        }

		changeContextValue(request,Constants.CV_DOCUMENT_KEY, documentKey.getIdAsString());
		changeContextValue(request,Constants.CV_DOCUMENT_TYPE, documentType);


        if(log.isDebugEnabled()) {
            log.debug("--------------------------------------------------------");
            log.debug("techkey:        " + documentKey.getIdAsString().trim());
            log.debug("object_id:      " + documentId.trim());
            log.debug("objects_origin: " + documentsOrigin.trim());
            log.debug("objecttype:     " + documentType.trim());
            log.debug("billing:        " + billing);
            log.debug("--------------------------------------------------------");
        }

        if(documentKey == null) {
            if(log.isDebugEnabled()) {
                log.debug("--------------------------dockeynull------------------------------");
            }
            // No techkey given, than only Id AND Origin identifing a object uniquely
            if( (documentsOrigin == null || documentsOrigin.equalsIgnoreCase(""))
                || (documentId == null || documentId.equalsIgnoreCase(""))) {
                throw new PanicException("status.message.parameter.missing");

            }

        }

		// get the user first
		User user = bom.getUser();
		if (user == null  ||  ! user.isUserLogged()) {
			log.exiting();
			return mapping.findForward("loggin");
		}

		LoyaltyMembership loyMemShip = LoyaltyUtilities.getLoyaltyMembership(userSessionData);                
        prepareOrderStatus(bom, shop, documentKey, documentId, documentsOrigin,
            			   documentType, loyMemShip);

		HeaderSalesDocument headerSalesDoc = bom.getOrderStatus().getOrderHeader();

		// eAuction integration: Get auction process type
		String auctionProcessType = getInitParameter(ContextConst.BO_EAUCTION_PROCESSTYPE);
		// eAuction integration: Do not allow documents created via eAuctions to be changed!
		if( auctionProcessType != null &&
			auctionProcessType.equalsIgnoreCase(headerSalesDoc.getProcessType())) {
			headerSalesDoc.setChangeable("");
		}

        // Another forward provided ??
        if(externalForward != null  &&  (! externalForward.equals("")) ) {
            log.exiting();
            return mapping.findForward(externalForward);
        }
        log.exiting();
        return mapping.findForward("success");

    }


    static public void prepareOrderStatus(BusinessObjectManager bom,
		                                    Shop shop,
		                                    TechKey documentKey,
                                            String documentId,
                                            String documentsOrigin,
                                            String documentType,
											LoyaltyMembership loyMemShip)
        		throws CommunicationException {
        			
        OrderStatus orderStatusDetail = bom.createOrderStatus();
        
        // Is Shop still available
        if( orderStatusDetail.getShop() == null) {
            orderStatusDetail.setShop(shop);
        }
        
        // We use OrderStatus and DocumentListFilter as data container for
        // transport purposes between muliple Actions
        
        // Set attributes given by request
        orderStatusDetail.setTechKey(documentKey);
        if (documentId!= null) {
			orderStatusDetail.setSalesDocumentNumber(documentId);
        }
		if (documentsOrigin!= null) {
        	orderStatusDetail.setSalesDocumentsOrigin(documentsOrigin);
		}	
        
        // Get Salesdocument status details
        orderStatusDetail.readOrderStatus(documentKey, documentId, documentsOrigin, documentType, loyMemShip);
        
//        HeaderSalesDocument headerSalesDoc = orderStatusDetail.getOrderHeader();
//
//		// eAuction integration: Get auction process type
//		String auctionProcessType = getInitParameter(ContextConst.BO_EAUCTION_PROCESSTYPE);
//		// eAuction integration: Do not allow documents created via eAuctions to be changed!
//		if( auctionProcessType != null &&
//			auctionProcessType.equalsIgnoreCase(headerSalesDoc.getProcessType())) {
//			headerSalesDoc.setChangeable("");
//		}
//        
        
    }


}
