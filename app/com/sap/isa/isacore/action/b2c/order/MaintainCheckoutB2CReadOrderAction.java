/*****************************************************************************
  Class:        MaintainCheckoutB2CReadOrderAction
  Copyright (c) 2001, SAP AG, All rights reserved.
  Author:       SAP AG
  Created:      27.11.2002
  Version:      1.0

  $Revision: #51 $
  $Date: 2002/11/27 $
*****************************************************************************/

package com.sap.isa.isacore.action.b2c.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businessobject.order.Basket;
import com.sap.isa.businessobject.order.Order;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreInitAction;

/**
 * Action to read in an exiting order, that has been created during
 * a basketsplit in the channel commerce hub scenario. This action is
 * called, whenever a order, created through a basketsplit, should be printed
 * for confirmation purposes.
 * <p>
 * The following forwards are used by this action:
 * </p>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>FORWARD_DISPLAY</td><td>display order for printing</td></tr>
 *   <tr><td>FORWARD_ERROR</td><td>no order techkey found</td></tr>
 * </table>
 *
 * @author SAP AG
 * @version 1.0
 */
public class MaintainCheckoutB2CReadOrderAction extends MaintainCheckoutB2CBaseAction {

    public static final String RC_ORDER_GUID = "MaintainCheckoutB2CReadOrderAction.orderGuid";

    public static final String FORWARD_DISPLAY = "display";
    public static final String FORWARD_ERROR = "error";

    public MaintainCheckoutB2CReadOrderAction() {
    }

    public String checkOutPerform(HttpServletRequest request,
                HttpServletResponse response,
                HttpSession session,
                UserSessionData userSessionData,
                RequestParser parser,
                BusinessObjectManager bom,
                IsaLocation log,
                IsaCoreInitAction.StartupParameter startupParameter,
                boolean multipleInvocation,
                boolean browserBack,
                BusinessEventHandler eventHandler,
                Shop shop,
                Basket basket,
                Order order) throws CommunicationException {

		final String METHOD_NAME = "checkOutPerform()";
		log.entering(METHOD_NAME);
        String forwardTo = FORWARD_DISPLAY;

        bom.releaseOrder();
        order = bom.createOrder();

        if (order == null) {
        	log.exiting();
            throw new PanicException("Order could not be created in BOM");
        }

        RequestParser.Parameter orderGuidParam = parser.getParameter(RC_ORDER_GUID);
        String orderGuid = null;

        if (orderGuidParam.isSet()) {
            orderGuid = orderGuidParam.getValue().getString();
        }

        if (orderGuid == null || orderGuid.length() == 0) {
            if (log.isDebugEnabled()) {
                log.debug("No TechKey found in Request");
            }
        }
        else {
            // read catalog
            WebCatInfo webCat =
                getCatalogBusinessObjectManager(userSessionData).getCatalog();

            if (log.isDebugEnabled()) {
                log.debug("Read oder with TechKey: " + orderGuid);
            }
            order.setTechKey(new TechKey(orderGuid));
			order.readHeader();
			order.readAllItems();             
            order.setGData(shop, webCat);
        }
		log.exiting();
        return forwardTo;
    }

}