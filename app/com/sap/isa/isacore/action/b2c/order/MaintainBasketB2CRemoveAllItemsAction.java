/*****************************************************************************
  Class:        MaintainBasketB2CRemoveAllItemsAction
  Copyright (c) 2006, SAP AG, Germany, All rights reserved.
  Created:      13.02.2006
  Version:      1.0
*****************************************************************************/

package com.sap.isa.isacore.action.b2c.order;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.businessobject.order.Basket;
import com.sap.isa.businessobject.order.Leaflet;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreInitAction;

/**
 * Action to remove all items from the basket.
 * <p>
 * The following forwards are used by this action:
 * </p>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>displayBasket</td><td>the basket is displayed</td></tr>
 * </table>
 *
 */
public class MaintainBasketB2CRemoveAllItemsAction extends MaintainBasketB2CBaseAction {

    public MaintainBasketB2CRemoveAllItemsAction() {
    }

    public void initialize() {
       this.checkUserIsLoggedIn = false;
    }
        
    protected String basketPerform(
                HttpServletRequest request,
                HttpServletResponse response,
                HttpSession session,
                UserSessionData userSessionData,
                RequestParser parser,
                BusinessObjectManager bom,
                IsaLocation log,
                IsaCoreInitAction.StartupParameter startupParameter,
                boolean multipleInvocation,
                boolean browserBack,
                Shop shop,
                Basket basket,
                Leaflet leaflet)
                    throws CommunicationException {

		TechKey[] itemKeys = new TechKey[1];
		
		final String METHOD_NAME = "basketPerform()";
		log.entering(METHOD_NAME);
        String forwardTo = null;

        // remove all items from basket
		List deleteItems = new ArrayList(basket.getItems().size());
		Iterator itemIterator = basket.getItems().iterator();
		for ( Iterator iter = basket.getItems().iterator(); iter.hasNext(); )
		{
		   TechKey key = new TechKey( ((ItemSalesDoc)iter.next()).getTechKey().toString());
		   deleteItems.add(key);
		}
		
		if (deleteItems.size() > 0) {
			basket.deleteItems((TechKey[])deleteItems.toArray(itemKeys));
		}

         forwardTo = "displayBasket";

        // overwrite forward if provided
        String newForward = request.getParameter("forward_to");
        if (newForward != null && newForward.trim().length() != 0) {
            forwardTo = newForward;
        }
		log.exiting();
        return forwardTo;
    }
}