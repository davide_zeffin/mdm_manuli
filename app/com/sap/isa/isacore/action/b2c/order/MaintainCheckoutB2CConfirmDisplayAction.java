/*****************************************************************************
  Class:        MaintainCheckoutB2CConfirmDisplayAction
  Copyright (c) 2001, SAP AG, All rights reserved.
  Author:       SAP AG
  Created:      25.03.2001
  Version:      1.0

  $Revision: #6 $
  $Date: 2003/03/13 $
*****************************************************************************/

package com.sap.isa.isacore.action.b2c.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.order.Basket;
import com.sap.isa.businessobject.order.Order;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreInitAction;


/**
 * Action to finally check the data entered into the checkout form - the payment
 * data - and display the confirmation screen, if the data was ok.
 * Otherwise the checkout screen is displayed again.
 * <p>
 * The following forwards are used by this action:
 * </p>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>FORWARD_PAYMENTERROR</td><td>problems with the entered payment data ocurred</td></tr>
 *   <tr><td>FORWARD_CONFIRM</td><td>payment data was ok, proceed to confirmation screen</td></tr>
 * </table>
 *
 * @author SAP AG
 * @version 1.0
 */
public class MaintainCheckoutB2CConfirmDisplayAction extends MaintainCheckoutB2CBaseAction {
	
    public MaintainCheckoutB2CConfirmDisplayAction() {
    }

    public String checkOutPerform(HttpServletRequest request,
                HttpServletResponse response,
                HttpSession session,
                UserSessionData userSessionData,
                RequestParser parser,
                BusinessObjectManager bom,
                IsaLocation log,
                IsaCoreInitAction.StartupParameter startupParameter,
                boolean multipleInvocation,
                boolean browserBack,
                BusinessEventHandler eventHandler,
                Shop shop,
                Basket basket,
                Order order) throws CommunicationException {

        final String METHOD_NAME = "checkOutPerform()";
        log.entering(METHOD_NAME);
        String forwardTo = null;

		
        // if the order is already saved (basket is null)
        // and if it is not the printout return the FORWARD_SAVED
        if (basket == null) {
            return FORWARD_SAVED;
        }
        
        order.clearMessages();
        order.read();
        HeaderSalesDocument header = order.getHeader();
      

        if (!order.isValid()) {
            forwardTo = FORWARD_INVALIDDOC;
            setRequestCheckout(request, session, bom, shop, order, bom.createBUPAManager(), log);
        }
        else if ((isElementVisible("order.shippingCondition", userSessionData) && 
                     (header.getShipCond() == null || header.getShipCond().length() == 0)) || 
                 (isElementVisible("order.deliveryPriority", userSessionData) && 
                     (header.getDeliveryPriority() == null || header.getDeliveryPriority().length() == 0))) {
                    	
            forwardTo = FORWARD_PAYMENTERROR;
            setRequestCheckout(request, session, bom, shop, order, bom.createBUPAManager(), log);
        }     
        else if (order.getPayment().getError() == true) {
            forwardTo = FORWARD_PAYMENTERROR;
            setRequestCheckout(request, session, bom, shop, order, bom.createBUPAManager(), log);           
        }
        else {
            order.clearMessages();
            // if simulation is needed, call it now
            if (shop.isOrderSimulateAvailable()) {
                log.debug("order simulate called");
                order.simulate();
            }
            
            // a change on header level(for example shipping conditions
            // impact also the prices of the items; so let's read them again
            // from backend
            order.readAllItems();                

            // Are the payment infos ok?
            if ( order.getPaymentData().getError() == true) {
                // there are payment errors
                forwardTo = FORWARD_PAYMENTERROR;
                setRequestCheckout(request, session, bom, shop, order, bom.createBUPAManager(), log);
            }
            else {
                // everthing seems to be OK, let us build the confirmation screen
                displayConfirmation(request, bom, shop, order, bom.createBUPAManager(), log);
                forwardTo = FORWARD_CONFIRM;
            }
        }   
				
		log.exiting();
        return forwardTo;
    }

}