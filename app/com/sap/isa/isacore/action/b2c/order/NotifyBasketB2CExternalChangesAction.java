/*****************************************************************************
  Class:        NotifyBasketB2CExternalChangesAction
  Copyright (c) 2006, SAP AG, Germany, All rights reserved.
  Created:      06.04.2006
  Version:      1.0

*****************************************************************************/

package com.sap.isa.isacore.action.b2c.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.order.Basket;
import com.sap.isa.catalog.actions.ActionConstants;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 * Action to ensure that the basket will be refreshed after it was changed by an external 
 * component.
 * In the catalog the solution configuration of an order item can be changed. The changes
 * will be transferred by to the backend (CRM) by calling an RFC.  
*
 * <p>
 * The following forwards are used by this action:
 * </p>
 * <table border="1" cellspacing="0" cellpadding="2">
 *
 *   <tr><td>FOLLOWUPACTION</td><td>the action to be called</td></tr>
 * </table>
 *
 * @author SAP AG
 * @version 1.0
 */
public class NotifyBasketB2CExternalChangesAction extends IsaCoreBaseAction {

	
	/**
	 * Initialize the action.
	 * In B2C this action could be used by anonymous user.
	 * 
	 * @see com.sap.isa.isacore.action.IsaCoreBaseAction#initialize()
	 */
	public void initialize() {
		if (isB2C()) {
			this.checkUserIsLoggedIn = false;
		}	
	}


	/**
	 * Determines the basket and sets the dirty flag. 
	 * The forward will be provided by the request parameter followUpAction * corresponding JSP.
	 *
	 */
	public ActionForward isaPerform(ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response,
			UserSessionData userSessionData,
			RequestParser requestParser,
			BusinessObjectManager bom,
			IsaLocation log) throws CommunicationException {

		// page to be displayed next
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
		String forwardTo = null;
		
		// get the current basket
		Basket basket = bom.getBasket();
		if (basket == null) {
			if (log.isDebugEnabled()) {
				log.debug("No basket found.");
			}			
		} else {
		  basket.setDirty(true);	
		}
	
		// the forward should be set by the caller
		if (requestParser.getAttribute(ActionConstants.RA_BAKET_FOLLOW_UP_ACTION).isSet()) {
			forwardTo = requestParser.getAttribute(ActionConstants.RA_BAKET_FOLLOW_UP_ACTION).getValue().getString();
		} else {
			log.debug("Request attribute for follow up action is not set.");
		}
		
		log.exiting();
		return mapping.findForward(forwardTo);
		
		
	}
	
	
}