/*****************************************************************************
    Class         ItemConfigurationB2CAction
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Dendl
    Created:      19.09.2001
    Version:      1.0

    $Revision: #15 $
    $Date: 2001/08/01 $
*****************************************************************************/
package com.sap.isa.isacore.action.b2c.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.backend.boi.isacore.order.DocumentListFilterData;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.item.ItemList;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.businessobject.order.Basket;
import com.sap.isa.businessobject.order.OrderStatus;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.order.ItemConfigurationBaseAction;
import com.sap.spc.remote.client.object.OriginalRequestParameterConstants;


/**
 * Action to configure an item from the basket.
 *
 * @author Stefan Dendl
 * @version 1.0
 */
public class ItemConfigurationB2CAction extends ItemConfigurationBaseAction {
    private static final String FORWARD_IPC_CONFIGURATION = "success";
    public static final String  SC_CONFIGURATION_ACTIVE = "com.sap.isa.isacore.action.order.ItemConfigurationAction.configactive";
    public static final String  SC_ITEM_IN_CONFIG = "com.sap.isa.isacore.action.order.ItemConfigurationAction.iteminconfig";
    public static final String  CONFIG_ACTIVE_TRUE = "TRUE";
    public static final String  CONFIG_ACTIVE_FALSE = "FALSE";

    /**
     * Name of the request parameter for the shipto index.
     */
    public static final String PARAM_ITEMID = "itemId";
    public static final String PARAM_B2C_BASKET = "b2cBasket";
	
    /**
     * Overriden <em>isaPerform</em> method of <em>IsaCoreBaseAction</em>.
     */
    public ActionForward isaPerform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response, UserSessionData userSessionData, RequestParser parser, BusinessObjectManager bom, IsaLocation log) throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        ItemList            itemList;
        HeaderSalesDocument header;
        String              caller;
        String              forward = FORWARD_IPC_CONFIGURATION;

        TechKey             techKey = null;
        String              b2cBasket;
        SalesDocument       salesDoc = null;

        // Configuration of an item from the basket
        if (parser.getAttribute("itemId").isSet()) {
            techKey = new TechKey(parser.getAttribute("itemId").getValue().getString());
        }
        else {
            techKey = new TechKey(parser.getParameter("itemId").getValue().getString());
        }

        // determine if basket or orderstatus called configuration
        if (parser.getAttribute("b2cBasket").isSet()) {
            b2cBasket = parser.getAttribute("b2cBasket").getValue().getString();
        }
        else {
            b2cBasket = parser.getParameter("b2cBasket").getValue().getString();
        }

        boolean deleteExternalItem = true;

        // Basket or OrderStatus
        if ((b2cBasket.equals("false")) == true) {
            // OrderStatus
            deleteExternalItem = false;

            OrderStatus orderStatus = bom.getOrderStatus();
			orderStatus.getItemConfig(DocumentListFilterData.SALESDOCUMENT_TYPE_ORDER, techKey);
            header       = orderStatus.getOrderHeader();
            itemList     = orderStatus.getItemList();
            caller       = OriginalRequestParameterConstants.CALLER_B2C_ORDERSTATUS;
        }
        else {
            Basket basket = bom.getBasket();
            salesDoc = basket;
            basket.getItemConfig(techKey);
            itemList     = basket.getItems();
            header       = basket.getHeader();
            caller       = OriginalRequestParameterConstants.CALLER_B2C_BASKET;
        }

        if (itemList == null) {
        	log.exiting();
            throw new PanicException("****ItemConfigurationAction: Itemlist is empty");
        }

        userSessionData.setAttribute(SC_ITEM_IN_CONFIG, techKey);

        // Find the item for the given technical key
        ItemSalesDoc item = itemList.get(techKey);

        if (isItemConfigurableInIPC(item)) {
            setConfigurationParams(caller, item, header, salesDoc, deleteExternalItem, userSessionData, request);
        }
        else {
            forward = "backtoorder";
        }

		log.exiting();
        return mapping.findForward(forward);
    }

    /**
     * DOCUMENT ME!
     */
    public void initialize() {
        this.checkUserIsLoggedIn = false;
    }
}
