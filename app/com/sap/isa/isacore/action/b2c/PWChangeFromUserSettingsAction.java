/*****************************************************************************
    Class         PWChangeFromUserSettingsAction
    Copyright (c) 2000, SAP AG, All rights reserved.
    Description:  Action for the Profile Maintenance
    Author:       SAP AG
    Created:      27 Februar 2001
    Version:      0.1

    $Revision: #3 $
    $Date: 2003/10/23 $
*****************************************************************************/

package com.sap.isa.isacore.action.b2c;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.User;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 *  This action sets only the request attribute <em>FORWARD_NAME</em> for the
 *  <i>Password Change</i> JSP.
 */
public class PWChangeFromUserSettingsAction extends IsaCoreBaseAction {

    /**
     * Overriden <em>isaPerform</em> method of <code>IsaCoreBaseAction</code>.
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            BusinessObjectManager bom,
            IsaLocation log)
            throws CommunicationException {

		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        String actionName = (String)request.getAttribute(PWChangeAction.ACTION_NAME);

        User user = bom.getUser();

        request.setAttribute(BusinessObjectBase.CONTEXT_NAME, user);

        /*
         * Check commented out, because this action will be used within the 
         * "B2B for occasional users" scenario, when the user has an expired 
         * password while login process. 
         * In this case the user isn't logged in!
         */
        /*
        if (!user.isUserLogged()) {
            userSessionData.setAttribute(B2cConstants.FRAME_BEFORE_LOGIN,"accounts");
            return mapping.findForward("login");
        }
        */

        /*
         * The attributes musn't be changed, if password change within the
         * "B2B for occasional users" scenario login process will be called 
         * (when the user has an expired password) 
         */
        if(actionName == null || actionName.equals("")) {
            request.setAttribute(PWChangeAction.ACTION_NAME,PWChangeAction.ACTION_PW);
            request.setAttribute(PWChangeAction.FORWARD_NAME,PWChangeAction.FORWARD_NAME_PWCHANGE);
        }
		log.exiting();
        return mapping.findForward("success");
    }
}

