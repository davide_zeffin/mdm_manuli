/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Biju Raj
  Created:      3rd April 2001

  $Revision: #4 $
  $Date: 2001/07/24 $
*****************************************************************************/


package com.sap.isa.isacore.action.b2c;

// framework dependencies
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.User;
import com.sap.isa.catalog.webcatalog.WebCatArea;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.ActionConstants;
import com.sap.isa.isacore.action.IsaCoreBaseAction;


/**
 * Defines that the <i>Catalog</i> should be displayed next by setting
 * an appropriate <code>UserSessionData</code> attribute. And tells the catalog
 * whether the user is logged in.
 */
 public class CatalogForwardAction extends IsaCoreBaseAction {
	
	/**
	 * Initialize the action.
	 * In B2C this action could be used by anonymous user.
	 * 
	 * @see com.sap.isa.isacore.action.IsaCoreBaseAction#initialize()
	 */
	public void initialize() {
		if (isB2C()) {
			this.checkUserIsLoggedIn = false;
		}	
	}

  /**
   * Overriden <em>doPerform</em> method of <em>BaseAction</em>.
   */
     public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            BusinessObjectManager bom,
            IsaLocation log) {

		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);

        // Page that should be displayed next - default entry
        String forwardTo = "success";

        User user = bom.getUser();
        if (user == null) {
        	log.exiting();
            return mapping.findForward("error");
        }

        WebCatInfo catalog =
                getCatalogBusinessObjectManager(userSessionData).getCatalog();

        if (catalog == null) {
        	log.exiting();
            throw new PanicException("catalog.notFound");
        }
        // determine if the user is already known (not necessary logged in)
        boolean isUserKnown = false;
        if (userSessionData.getAttribute(B2cConstants.UNLOGGED_USER_INFO) != null ||
            user.isUserLogged()) {
            isUserKnown = true;
        }

        // Do not show recommendation, if user is not known
        if (!isUserKnown && catalog.getLastVisited().equals("recommendations")) {
            catalog.setLastVisited("initial");
        }

        // new forward for initial application entry when user is unknown
        if ((!isUserKnown) && ((catalog.getLastVisited().equals(WebCatInfo.INITIAL)) || (catalog.getLastVisited().equals(WebCatInfo.AREADETAILS)))) {
            // initial application entry - show default catalog view when user is not known 
            forwardTo = "initial";
        }
        // new forward for initial application entry when user is already known
        else if ((isUserKnown) && ((catalog.getLastVisited().equals(WebCatInfo.INITIAL)) || (catalog.getLastVisited().equals(WebCatInfo.AREADETAILS)))) {
            // show recommendations and bestsellers (as initial page), if the user is known
            catalog.resetCurrentArea();
            catalog.setLastVisited(ActionConstants.DS_ENTRY);
            forwardTo = "initial";
        }
        else if (isUserKnown &&
                 request.getParameter(ActionConstants.DS_ENTRY) != null &&
                 request.getParameter(ActionConstants.DS_ENTRY).equals("true")) {
            // show recommendations and bestsellers, if the user is known and an
            // appropriate homepage link was clicked
			catalog.setCurrentArea(WebCatArea.ROOT_AREA); 
            catalog.resetCurrentArea();
            catalog.setLastVisited(ActionConstants.DS_ENTRY);
            
            // update context value (this value will be read e.g. in 
            // com.sap.isa.catalog.actions.GetCategoriesB2CAction (display part 
            // following after this action))
            changeContextValue(request,
                    com.sap.isa.catalog.actions.ActionConstants.CV_CURRENT_AREA,
                    WebCatArea.ROOT_AREA);
            
			forwardTo = "initial";
        }
        else if (isUserKnown && user.getMktProfile() != null) {
            // show recommendations and bestsellers, if the user has maintained
            // his profile
            catalog.resetCurrentArea();
            catalog.setLastVisited(ActionConstants.DS_ENTRY);
			forwardTo = "initial";
        }
        else if (isUserKnown &&
                 ( catalog.getLastVisited().equals(WebCatInfo.ITEMLIST) ||
                   catalog.getLastVisited().equals(WebCatInfo.ITEMDETAILS) ) &&
                 catalog.getCurrentArea() != null &&
                 catalog.getCurrentArea().getAreaType() != null &&
                 ( catalog.getCurrentArea().getAreaType().equals(WebCatArea.AREATYPE_LOY_REWARD_CATALOG) ||
                   catalog.getCurrentArea().getAreaType().equals(WebCatArea.AREATYPE_LOY_BUY_POINTS) )) {
            // show MyAccount if the current catalog area is of type reward catalog or buy points 
            if ( catalog.getLastVisited().equals(WebCatInfo.ITEMLIST)) {
                catalog.setLastVisited(ActionConstants.DS_ACCITEMLIST);
            }
            else {
                catalog.setLastVisited(ActionConstants.DS_ACCITEMDET);
            }
        }
        else if (userSessionData.getAttribute(B2cConstants.FRAME_NAME) != null &&
                 userSessionData.getAttribute(B2cConstants.FRAME_NAME).equals(LoginAction.LOGIN)) {
            // show recommendations and bestsellers, if the last user action was
            // a successful login or registration
            catalog.resetCurrentArea();
            catalog.setLastVisited(ActionConstants.DS_ENTRY);
			forwardTo = "initial";
        }
        else if (!isUserKnown &&
                 request.getParameter(ActionConstants.DS_ENTRY) != null &&
                 request.getParameter(ActionConstants.DS_ENTRY).equals("true")) {
            // show only bestsellers, if the user is not known and an
            // appropriate homepage link was clicked
			catalog.setCurrentArea(WebCatArea.ROOT_AREA); 
            catalog.resetCurrentArea();
            catalog.setLastVisited(ActionConstants.DS_ENTRY);
			forwardTo = "initial";
        }
        else if (catalog.getLastVisited().equals(WebCatInfo.ITEMDETAILS) && 
                 catalog.getCurrentItem().getSCOrderDocumentGuid() != null && 
				 catalog.getCurrentItem().getSCOrderDocumentGuid().getIdAsString().length() > 0) {
			// don't show the details for an item coming from the basket
			// in case continue shopping is selected. 
			catalog.setCurrentItem(null);
			catalog.setCurrentArea(WebCatArea.ROOT_AREA);
			catalog.resetCurrentArea();
			catalog.setLastVisited(ActionConstants.DS_ENTRY);
			forwardTo = "initial";	
        }

        // B2cConstants.Frame_NAME variable is later used in CatalogFS.jsp
        // to decide the frame to be displayed
        userSessionData.setAttribute(B2cConstants.FRAME_NAME,"CATALOG");
                
        log.exiting();
        return mapping.findForward(forwardTo);
    }//end of isaPerform method

}// end of CatalogForwardAction class


