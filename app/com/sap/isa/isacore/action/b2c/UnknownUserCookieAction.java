/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Thorsten Mohr
  Created:      December 2001

*****************************************************************************/

package com.sap.isa.isacore.action.b2c;

// framework dependencies
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.User;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;

/**
 * This Action is used in the B2C scenario to save the technical key which was
 * generate for an unknown user.
 * If the user reenter the shop (in future) but is not logged in at this time. With
 * the information contained in the Cookie created or updated by this action the user is
 * "known" although she/he is not logged in or registered. On such a way e.g. the personal
 * recommendations of the (last logged in) user can directly be displayed.
 */
public class UnknownUserCookieAction extends CreateCookieBaseAction {

    /**
     * The name of the cookie which stores the user's details
     */
    public static final String COOKIE_NAME = "rec_unknown_user";

	/**
	 * Initialize the action.
	 * In B2C this action could be used by anonymous user.
	 * 
	 * @see com.sap.isa.isacore.action.IsaCoreBaseAction#initialize()
	 */
	public void initialize() {
		if (isB2C()) {
			this.checkUserIsLoggedIn = false;
		}	
	}



    /**
     * Overriden <em>isaPerform</em> method of <code>IsaCoreBaseAction</code>.
     * The method creates a cookie with appropriate user details or updates the
     * cookie if it already exists.
     */
    public ActionForward isaPerform(ActionMapping mapping,
                                    ActionForm form,
                                    HttpServletRequest request,
                                    HttpServletResponse response,
                                    UserSessionData userSessionData,
                                    RequestParser requestParser,
                                    BusinessObjectManager bom,
                                    IsaLocation log)
                                    throws CommunicationException {

		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        User user = bom.getUser();

        // the contents of the cookie is the user's technical key (as a string)
        String cookieContents = user.getMktPartner().getTechKey().getIdAsString();

        createOrUpdateCookie(cookieContents, COOKIE_NAME,request, response);
		log.exiting();
        return mapping.findForward("success");
    }
}