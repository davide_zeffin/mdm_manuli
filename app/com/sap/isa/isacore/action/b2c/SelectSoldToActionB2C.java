/**
 * Class         SelectSoldToActionB2C Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
 * Description:  Action to display the SoldToList Created:      February 2002 Version:      0.1 $Revision: #3 $ $Date:
 * 2001/08/03 $
 */
package com.sap.isa.isacore.action.b2c;

import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.action.IsaCoreInitAction;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Display the list of SoldTos <br>
 * Note that all Action classes have to provide a non argument constructor because they were instantiated
 * automatically by the action servlet. Auto entry integration, where if requested by startup parameter
 * <code>autoentry</code>, first sold to of sold to list will be used.
 *
 * @author Marting Schley
 * @version 1.0
 */
public class SelectSoldToActionB2C extends IsaCoreBaseAction {
    /** Name of the lists of marketing attributes stored in the request context. */
    public static final String RK_SOLDTO_LIST = "soldtolist";

    /**
     * Implement this method to add functionality to your action.
     *
     * @param mapping DOCUMENT ME!
     * @param form The <code>FormBean</code> specified in the config.xml file for this action
     * @param request The request object
     * @param response The response object
     * @param userSessionData Object wrapping the session
     * @param requestParser Parser to simple retrieve data from the request
     * @param bom Reference to the BusinessObjectManager
     * @param log Reference to the IsaLocation, needed for logging
     *
     * @return Forward to another action or page
     *
     * @throws CommunicationException DOCUMENT ME!
     */
    public ActionForward isaPerform(ActionMapping         mapping,
                                    ActionForm            form,
                                    HttpServletRequest    request,
                                    HttpServletResponse   response,
                                    UserSessionData       userSessionData,
                                    RequestParser         requestParser,
                                    BusinessObjectManager bom,
                                    IsaLocation           log)
                             throws CommunicationException {
        final String METHOD_NAME = "isaPerform()";
        log.entering(METHOD_NAME);

        boolean isDebugEnabled = log.isDebugEnabled();

        String forward = null;

        forward = selectSoldTo(request, bom, userSessionData, log);

        log.exiting();

        return mapping.findForward(forward);
    }

    /**
     * DOCUMENT ME!
     *
     * @param request DOCUMENT ME!
     * @param userSessionData DOCUMENT ME!
     * @param bom DOCUMENT ME!
     * @param log DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     *
     * @throws CommunicationException DOCUMENT ME!
     */
    public String selectSoldTo(HttpServletRequest    request,
                               BusinessObjectManager bom,
                               UserSessionData       userSessionData,
                               IsaLocation           log)
                        throws CommunicationException {
        String forward = null;
        boolean isDebugEnabled = log.isDebugEnabled();

        User user = bom.createUser();
        Shop shop = bom.getShop();

        BusinessPartnerManager buPaMa = bom.createBUPAManager();
        BusinessPartner contact = buPaMa.getDefaultBusinessPartner(PartnerFunctionData.CONTACT);
        ResultData result = buPaMa.getSoldTosBySalesArea(contact, shop.getId());

        IsaCoreInitAction.StartupParameter startupParameter = (IsaCoreInitAction.StartupParameter) userSessionData.getAttribute(IsaCoreInitAction.SC_STARTUP_PARAMETER);

        if ((result == null) || (result.getNumRows() <= 0)) { // fm 19.06.01

            if (isDebugEnabled) {
                log.debug("found no sold to, bailing out.");
            }

            log.error(LogUtil.APPS_USER_INTERFACE, "no soldtos found!");

            user.addMessage(new Message(Message.ERROR, "user.noSoldTo"));

            // user.addMessage(new Message(Message.ERROR,"user.noSoldTo",null,null));
            request.setAttribute(BusinessObjectBase.CONTEXT_NAME, user);

            forward = "error";
        }
        else if ((result.getNumRows() == 1) || startupParameter.getAutoEntry().equals("X")) { // Autoentry integration

            // found exactly one sold to, or AutoEntry requested, where first sold to will be used
            if (isDebugEnabled && startupParameter.getAutoEntry().equals("X")) {
                result.absolute(1);
                log.debug("Autoentry requested, using soldto:" + result.getString("SOLDTO"));
            }

            // move to first row
            result.absolute(1);

            if (isDebugEnabled && !startupParameter.getAutoEntry().equals("X")) {
                log.debug("found exactly one sold to, using it.");
            }

            // Save sold techKey in request
            request.setAttribute(ReadSoldToActionB2C.PARAM_SOLDTO, result.getString("SOLDTO"));
            request.setAttribute(ReadSoldToActionB2C.PARAM_SOLDTO_TECHKEY, result.getString("SOLDTO_TECHKEY"));
            forward = "readsoldto";
        }
        else {
            if (isDebugEnabled) {
                log.debug("found more than one sold to.");
            }

            request.setAttribute(RK_SOLDTO_LIST, result);
            forward = "selectsoldto";
        }

        return forward;
    }
}
