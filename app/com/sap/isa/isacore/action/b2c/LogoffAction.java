/*****************************************************************************
  Copyright (c) 2000, SAP AG, All rights reserved.
  Author:       SAP AG
  Created:      08 August 2001

  $Revision: #4 $
  $Date: 2003/10/23 $
*****************************************************************************/

package com.sap.isa.isacore.action.b2c;

// framework dependencies
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.RecoverBasketCookieHandler;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 * The <code>LogoffAction</code> is used to provide to <i>Logoff</i> JSP with
 * the id of the currently visited shop. This information can be used to enable a reentering
 * of the same shop without any prior shoplist selection.
 */
public class LogoffAction extends IsaCoreBaseAction {

    /**
     * constant that should be used as a key considering the shop identification
     */
    public static final String SHOP_ID = "shopId";

    /**
     * Overriden <em>isaPerform</em> method of <code>IsaCoreBaseAction</code>.
     */
    public ActionForward isaPerform(ActionMapping mapping,
        ActionForm form,
        HttpServletRequest request,
        HttpServletResponse response,
        UserSessionData userSessionData,
        RequestParser requestParser,
        BusinessObjectManager bom,
        IsaLocation log)
        throws CommunicationException {

		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        // if the current salesdocument supports recovery all information that was generated
        // to recover the document must be destroyed
        SalesDocument salesDoc = bom.getBasket();

        if (salesDoc != null && salesDoc.isDocumentRecoverable()) {
            // if the basket supports recovery detroy the basket in the backend
                if (userSessionData.getAttribute(RecoverBasketCookieHandler.SC_RECO_BASKET_COOKIE_HDLR) != null) {
                   // if there is a cookie that stores the basketGUID, destroy it also
                   if (log.isDebugEnabled()) {
                       log.debug("LogoffAction - Delete RecoverBasketCookie");
                   }
                   // the Cookie handler has to be created newly, because the response has changed since its creation
                   RecoverBasketCookieHandler recBasketCookieHdlr = new RecoverBasketCookieHandler(request, response);

                   recBasketCookieHdlr.delete(bom.getShop());
                   userSessionData.removeAttribute(RecoverBasketCookieHandler.SC_RECO_BASKET_COOKIE_HDLR);
                }
                if (log.isDebugEnabled()) {
                   log.debug("LogoffAction - Destroy Content of Sales Document");
                }
                // finally delete the basket in the backend
                salesDoc.destroyContent();
        }
		log.exiting();
        return mapping.findForward("logoff");
  }
}