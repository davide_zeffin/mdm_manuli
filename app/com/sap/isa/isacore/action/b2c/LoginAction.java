/*****************************************************************************
  Class:        LoginAction
  Copyright (c) 2000, SAP Germany, All rights reserved.
  Author:       SAP
  Created:      21 March 2001
  Version:      1.0

  $Revision: #11 $
  $Date: 2004/04/26 $
*****************************************************************************/
package com.sap.isa.isacore.action.b2c;

// framework dependencies
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.ecommerce.businessobject.loyalty.LoyaltyMembership;
import com.sap.isa.businessobject.Address;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businessobject.businessevent.LoginEvent;
import com.sap.isa.businessobject.item.ItemList;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.businessobject.order.Basket;
import com.sap.isa.businessobject.order.PartnerList;
import com.sap.isa.businessobject.order.PartnerListEntry;
import com.sap.isa.businessobject.webcatalog.CatalogBusinessObjectManager;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.businesspartner.businessobject.Contact;
import com.sap.isa.businesspartner.businessobject.SoldTo;
import com.sap.isa.catalog.actions.ActionConstants;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.FrameworkConfigManager;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.action.IsaCoreInitAction;
import com.sap.isa.isacore.action.loyalty.LoyaltyUtilities;
import com.sap.isa.isacore.actionform.b2c.LoginForm;
import com.sap.isa.user.util.LoginStatus;


/**
 * Logs user into the backend system. This is done by using the appropiate
 * business object. If the login fails an error message is displayed and
 * the user has another chance to log in. After the login is successful,
 * a welcome message text will be displayed on the navigation bar.
 */
public class LoginAction extends IsaCoreBaseAction {
    /**
     * Constant that represents a possible value of the user's login status. The value
     * indicates a successful login. It will be stored within the user session data
     * and is accessible over the following key: <code>B2cConstants.LOGIN_STATUS</code>.
     */
    public static final String LOGIN_SUCCESS = "LOGIN_SUCCESS";
    /**
     * Constant that represents the value of the user's login status. The value
     * indicates a login failure. It will be stored within the user session data
     * and is accessible over the following key: <code>B2cConstants.LOGIN_STATUS</code>.
     */
    public static final String LOGIN_FAILURE = "LOGIN_FAILURE";
    /**
     * Constant that represents a value for the current frame name attribute which
     * will be stored within the user session data. This value will
     * later be used to decide what frame combination should be displayed on screen.
     * It is accessible over the following key: <code>B2cConstants.FRAME_NAME</code>
     */
    public static final String LOGIN = "LOGIN";
	/**
	 * Flag that represents the password change request from the backend in the  
	 * EasyB2B Scenario. This flag will be used on the <code>pwchange.jsp</code>
	 * to handle the frame targets.
	 */
	public static final String PASSWORD_CHANGE_EASYB2B = "PASSWORD_CHANGE_EASYB2B";
    /**
     * Parameter name for userId.
     */
    public static final String PN_USERID = "userId";
    /**
     * Parameter name for password.
     */
    public static final String PN_PASSWORD = "nolog_password";
    /**
     * Parameter name for password change.
     */
    public static final String PN_PASSWORD_CHANGE = "changePassword";
    /**
     * Parameter name for password change.
     */
    public static final String PN_LOGIN = "login";
	/**
     * Flag for sso ticket login in user id field.
     */
    private static final String SPECIAL_NAME_AS_USERID = "$MYSAPSSO2$";
    /**
     * Flag indicationg that a loyalty program membership shoul be created
     */
    public static final String PN_JOIN_LOYALTY = "joinLoyaltyProgram";
    /**
     * Flag inidcating that the loyalty program membership should be checked
     */
    public static final String PN_CHECK_LOYALTY = "checkLoyaltyProgram";
    
	/**
	 * Set the business partner in the business object manager depending of the
	 * scenario. <br>
	 * 
	 * @param userSessionData user session data
	 * @param businessPartnerKey key of the business partner 
	 * @param buPaMa
	 * @return
	 */
	static protected BusinessPartner setBusinessPartner(UserSessionData userSessionData,
												  TechKey businessPartnerKey,
												  BusinessPartnerManager buPaMa) {
		BusinessPartner partner = buPaMa.createBusinessPartner(businessPartnerKey,null, null );

		if ( userSessionData.getAttribute(B2cConstants.EASY_B2B).toString().equals("true") ) {
			// easyB2B
        
			Contact contact = new Contact();
			partner.addPartnerFunction(contact);
			buPaMa.setDefaultBusinessPartner(partner.getTechKey(),contact.getName());
            
		} else {
        
			SoldTo soldTo = new SoldTo();
			partner.addPartnerFunction(soldTo);
			buPaMa.setDefaultBusinessPartner(partner.getTechKey(),soldTo.getName());
        
		}
        
		return partner;
	}


	/**
	 * Initialize the action.
	 * In B2C this action could be used by anonymous user.
	 * 
	 * @see com.sap.isa.isacore.action.IsaCoreBaseAction#initialize()
	 */
	public void initialize() {
		if (isB2C()) {
			this.checkUserIsLoggedIn = false;
		}	
	}

    /**
     * Overriden <em>isaPerform</em> method of <code>IsaCoreBaseAction</code>.
     */
    public ActionForward isaPerform(ActionMapping mapping,
                                     ActionForm form,
                                     HttpServletRequest request,
                                     HttpServletResponse response,
                                     UserSessionData userSessionData,
                                     RequestParser requestParser,
                                     BusinessObjectManager bom,
                                     IsaLocation log,
                                     IsaCoreInitAction.StartupParameter startupParameter,
                                     BusinessEventHandler eventHandler)
        	throws CommunicationException {

		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        // Page that should be displayed next.
        String forwardTo = null;
        LoginStatus i = LoginStatus.NOT_OK;
        String welcomeText = "";
        boolean isDarklogin = false;
        boolean updateCatalogueCampaign = false;
        
        request.removeAttribute(ActionConstants.RA_LOGIN_CAMPAIGN_CHECK_FWD);

        // Check, if the given ActionForm object is really a LoginForm.
        // This must not be true because someone may have broken the action
        // mappings described in the action.xml file. In this case return
        // null to tell the action servlet that something went totally wrong.
        if (!(form instanceof LoginForm)) {
        	log.exiting();
            return null;
        }

        // Cast the form reference to a reference of type LoginActionFrom.
        // This is safe because of the test with the instanceof operator we have
        // done above.
        LoginForm loginForm = (LoginForm) form;

        if (requestParser.getAttribute(com.sap.isa.isacore.action.b2b.LoginAction.PN_USERID).getValue().isSet()) {
            // darklogin after coreinit!!
            isDarklogin = true;
            loginForm.setUserId(requestParser.getAttribute(com.sap.isa.isacore.action.b2b.LoginAction.PN_USERID).getValue().getString());
            loginForm.setPassword(requestParser.getAttribute(com.sap.isa.isacore.action.b2b.LoginAction.PN_PASSWORD).getValue().getString());
            // this should only be done for the darklogin not for the case of a sso ticket login in the easyB2B
            // but this would be only a workaround?!
            if (!loginForm.getUserId().equals(SPECIAL_NAME_AS_USERID)) {
                userSessionData.setAttribute(B2cConstants.FRAME_BEFORE_LOGIN,"shoplist");
            }
        } 

        if (loginForm.getUserId() == null || loginForm.getUserId().length()== 0) {
			HttpSession session = request.getSession();
			User user = bom.createUser();
        	user.addMessage(new Message(Message.ERROR,"b2c.login.wrongLogin"));

			request.setAttribute(BusinessObjectBase.CONTEXT_NAME, user);
			session.setAttribute(B2cConstants.USER,user);
			session.setAttribute(B2cConstants.FRAME_NAME, LOGIN);
			session.setAttribute(B2cConstants.LOGIN_STATUS, LOGIN_FAILURE);

        	log.exiting();
        	return mapping.findForward("failure");
        }
        

        // Ask the business object manager for a user. If this object is not
        // already present, a new one will be created.
        User user = bom.createUser();

        if (user == null) {
        	log.exiting();
            throw new PanicException("user.notFound");
        }

        // get the current session
        HttpSession session = request.getSession();

        // Set username and password for the user object. The data for this is
        // stored in the LoginForm object. This object was filled with the forms
        // data by the action servlet.
        // Then call the login
        // method to tell the user object that it should perform a login.
        // The login method will return true, if the login was successful or
        // otherwise false. If the login will be unsuccessful return to the
        // login screen and show an error message.
        user.setUserId(loginForm.getUserId());
        user.setPassword(loginForm.getPassword());

		BusinessPartnerManager buPaMa = bom.createBUPAManager();

		boolean auctionRelated = false;
        boolean callCenterMode = false; 

		String aucTransId =  startupParameter.getForward();
		if (aucTransId != null){
			if (aucTransId.equalsIgnoreCase("auctionChkout")){
				auctionRelated = true;
			}
		}

        try {
            String cm = FrameworkConfigManager.Servlet.getInteractionConfigParameter(
                request.getSession(), "user", "CallCenterMode", "");
                
            if (cm != null) { 
                callCenterMode = cm.equalsIgnoreCase("true");
            }
            // if CallCenterMode is not set in XCM then let's pretend the user has no CC Agent authorization
            if (callCenterMode == false) {
                user.setCallCenterAgent("");
            }
            else {
                user.setCallCenterAgent("Y"); // inform login of call center mode
            }
        } catch (Exception ex) { 
            user.setCallCenterAgent("");
        }
        

        // Login the user using the login() method of the user business object.
        // If the login fails, false is returned, otherwise true.
        //if ((i = user.login()) == LoginStatus.OK) {
                
		i = login(request, response, requestParser, userSessionData, bom, user);
                
        if (i == LoginStatus.OK) {
        
            // The user has successfully logged into the system.

			// if CallCenterMode is not set in XCM then let's pretend the user has no CC Agent authorization
            // even if they were found in the backend
			if (callCenterMode == false) {
				user.setCallCenterAgent("");
			}

			if (!(user.IsCallCenterAgent())) {
				BusinessPartner partner = setBusinessPartner(userSessionData, 
															 user.getBusinessPartner(), 
															 buPaMa);
			
				Address address = partner.getAddress();
			
				if (address != null && !address.getLastName().equals("")) {
					String title = address.getTitle();
					String lastName = address.getName();
					welcomeText = title + " " + lastName;
				}


				// set session attributes for the welcome-text (navigationbar.jsp)
				session.setAttribute(B2cConstants.WELCOME_TEXT, welcomeText);

				// set user session data
				userSessionData.setAttribute(B2cConstants.FRAME_BEFORE_LOGIN,(String) userSessionData.getAttribute(B2cConstants.FRAME_BEFORE_LOGIN));
				userSessionData.setAttribute(B2cConstants.FRAME_NAME, LOGIN);
				userSessionData.setAttribute(B2cConstants.LOGIN_STATUS, LOGIN_SUCCESS);

				fireLoginEvent(eventHandler, buPaMa, user);

				handleBasket(userSessionData, session, bom, user, auctionRelated);

				if (loginForm.getUserId().equals(SPECIAL_NAME_AS_USERID)) {
					forwardTo = "ssoSuccess";
				}
				else if ( userSessionData.getAttribute(B2cConstants.EASY_B2B).toString().equals("true") ) {
					forwardTo = "successEasyB2B";
	            } 
	            else if(isDarklogin) {
	                forwardTo = "darklogin";
	            }
	            else {
	                forwardTo = determineForward(userSessionData, bom); //"success";
				}
                
                forwardTo = checkCatalogueCampaignUpdate(userSessionData, forwardTo, request);
			}
			else { // TODO check what parts of "set user session ..." are to be copied/modified !!
				forwardTo = "callcenteragent";		
				userSessionData.setAttribute(B2cConstants.FRAME_BEFORE_LOGIN,(String) userSessionData.getAttribute(B2cConstants.FRAME_BEFORE_LOGIN));
				userSessionData.setAttribute(B2cConstants.FRAME_NAME, LOGIN);
				userSessionData.setAttribute(B2cConstants.LOGIN_STATUS, LOGIN_SUCCESS);
			}

        }
        
        // insert easyB2B begin
        else if (i==LoginStatus.NOT_OK_NEW_PASSWORD) {

            // set parameter to set the correct forward for pwchange action
            request.setAttribute(PWChangeAction.FORWARD_NAME,PWChangeAction.FORWARD_NAME_SUCCESS);
            request.setAttribute(PWChangeAction.ACTION_NAME,PWChangeAction.ACTION_US);

			request.setAttribute(BusinessObjectBase.CONTEXT_NAME, user);
            
            if ( userSessionData.getAttribute(B2cConstants.EASY_B2B).toString().equals("true") ) {

			    // will be used on the pwchange.jsp to handle the frame targets.
			    request.setAttribute(PASSWORD_CHANGE_EASYB2B, "true");
				request.setAttribute(PWChangeAction.FORWARD_NAME,PWChangeAction.FORWARD_NAME_SELECTSOLDTO);

            } 

            // set user session data
            userSessionData.setAttribute(B2cConstants.FRAME_BEFORE_LOGIN,(String) userSessionData.getAttribute(B2cConstants.FRAME_BEFORE_LOGIN));
            userSessionData.setAttribute(B2cConstants.FRAME_NAME, LOGIN);
            
			fireLoginEvent(eventHandler, buPaMa, user);

			handleBasket(userSessionData, session, bom, user, auctionRelated);

            if(isDarklogin) {
                forwardTo = "darkloginFailure";
            } 
            else {
                forwardTo="pwchange";
            }
            
            forwardTo = checkCatalogueCampaignUpdate(userSessionData, forwardTo, request);
        }

        else if (i==LoginStatus.NOT_OK_PASSWORD_EXPIRED) {

            // set parameter to set the correct forward for pwchange action
            request.setAttribute(PWChangeAction.FORWARD_NAME,PWChangeAction.FORWARD_NAME_SUCCESS);
            request.setAttribute(PWChangeAction.ACTION_NAME,PWChangeAction.ACTION_EP);
            
			request.setAttribute(BusinessObjectBase.CONTEXT_NAME, user);

            if ( userSessionData.getAttribute(B2cConstants.EASY_B2B).toString().equals("true") ) {

			    // will be used on the pwchange.jsp to handle the frame targets.
			    request.setAttribute(PASSWORD_CHANGE_EASYB2B, "true");
				request.setAttribute(PWChangeAction.FORWARD_NAME,PWChangeAction.FORWARD_NAME_SELECTSOLDTO);
              
            } 

            // set user session data
            userSessionData.setAttribute(B2cConstants.FRAME_BEFORE_LOGIN,(String) userSessionData.getAttribute(B2cConstants.FRAME_BEFORE_LOGIN));
            userSessionData.setAttribute(B2cConstants.FRAME_NAME, LOGIN);

			fireLoginEvent(eventHandler, buPaMa, user);

			handleBasket(userSessionData, session, bom, user, auctionRelated);

            if(isDarklogin) {
                forwardTo = "darkloginFailure";
            } 
            else {
                forwardTo="pwchange";
            }
            
            forwardTo = checkCatalogueCampaignUpdate(userSessionData, forwardTo, request);
        }
        // insert easyB2B end

        else{
            // In case of login failure....display appropriate error message.
            user = bom.getUser();
            session.setAttribute(B2cConstants.USER,user);
            userSessionData.setAttribute(B2cConstants.FRAME_NAME, LOGIN);
            userSessionData.setAttribute(B2cConstants.LOGIN_STATUS, LOGIN_FAILURE);

            if (loginForm.getUserId().equals(SPECIAL_NAME_AS_USERID)) {
                if (isCallCenterMode(request)) {
                    forwardTo = "ssoFailureCCM";
                }
                else {
                    forwardTo = "ssoFailure";
                }
            }
            else if(isDarklogin) {
                    forwardTo = "darkloginFailure";
            } 
            else {
                forwardTo = "failure";
            }
        }
        
        if (loginForm.getUserId().equals(SPECIAL_NAME_AS_USERID)) {
        	request.setAttribute(com.sap.isa.isacore.action.b2b.LoginAction.PN_USERID, "");
        }	
		log.exiting();
        return mapping.findForward(forwardTo);
    }

    /**
     * Checks if there is a catalogue camapign, that must be updated afer a succesful login
     */
    private String checkCatalogueCampaignUpdate(UserSessionData userSessionData, String forwardTo, HttpServletRequest request) {
        // reapply catalogue campaign after succesful login, to check for private campaign eligibilty
        CatalogBusinessObjectManager cbom = (CatalogBusinessObjectManager) userSessionData.getMBOM().getBOMbyName(CatalogBusinessObjectManager.CATALOG_BOM);
        if (cbom != null &&  cbom.getCatalog() != null && 
            cbom.getCatalog().getCampaignId() != null &&
            cbom.getCatalog().getCampaignId().length() > 0 &&
            cbom.getCatalog().isSoldToEligible() == false) {
            log.debug("Catalogue uampaign update necessary");
            request.setAttribute(ActionConstants.RA_LOGIN_CAMPAIGN_CHECK_FWD , "log" + forwardTo);
            return "updateCatCampaign";
        }
        else {
            log.debug("No catalogue campaign update necessary");
            return forwardTo;
        }
    }

    private boolean isCallCenterMode (HttpServletRequest request) {
    {
        boolean callCenterMode = false; 
        String cm = FrameworkConfigManager.Servlet.getInteractionConfigParameter(
            request.getSession(), "user", "CallCenterMode", "");
                
        if (cm != null) { 
            callCenterMode = cm.equalsIgnoreCase("true");
        }
        return callCenterMode;
    }
}

	/**
	 * Fires the login event see {@link #LoginEvent}. <br>
	 * 
	 * @param eventHandler business event handler
	 * @param buPaMa business partner manager 
	 * @param user current user
	 */
    protected void fireLoginEvent(BusinessEventHandler eventHandler,
								    BusinessPartnerManager buPaMa,
                                    User user) {
        // fire a login event
        LoginEvent event = new LoginEvent(user);
        eventHandler.fireLoginEvent(event);
    }


	/**
	 * Handles the basket recovery. <br>
	 * 
	 * @param userSessionData user session data
	 * @param bom business object manager
	 * @param user current user
	 * @throws CommunicationException
	 */
    protected void handleBasket(UserSessionData userSessionData,
                                HttpSession session,
                                BusinessObjectManager bom,
                                User user,
                                boolean auctionRelated)
        	throws CommunicationException {

        // check if there is a basket for the user, that must be recovered
        if (bom.getBasket() == null) {
            
            // get business objects
            Shop shop = bom.getShop();
            
        	// check for dark login                
        	if(shop != null) {
        		                
                WebCatInfo catalog =
                    getCatalogBusinessObjectManager(userSessionData).getCatalog();
        
                // create an read the basket
                Basket basket = bom.createBasket();
                basket.setAuctionBasket(auctionRelated);
                // to know if the basket supports recovery, we must initialize it


				LoyaltyMembership loyMemShip = LoyaltyUtilities.getLoyaltyMembership(userSessionData);
				basket.getHeader().setMemberShip(loyMemShip);
                if (loyMemShip != null && loyMemShip.exists() && loyMemShip.getPointAccount() != null) {
                    ItemList items = basket.getItems();   
                    for (int i=0; i < items.size(); i++) {
                        ItemSalesDoc item = items.get(i);
                        item.setLoyPointCodeId(loyMemShip.getPointAccount().getPointCodeId());
                    }
                }
				                
                basket.init(shop, bom.createBUPAManager(), null, catalog);
        
                if (basket.isDocumentRecoverable()) {
                    // a new empty entry in the database was created, if the basket is recoverable
                    // that entry must be deleted
                    basket.destroyContent();
                    
                    if (shop.getAuctionBasketHelper() != null) {
                        shop.getAuctionBasketHelper().setInvocationContext(userSessionData);
                    }
        
                    if (!basket.recoverUsingShopAndUser(shop, user.getTechKey(), catalog)) {
                        // there is no document to be recovered, delete the basket again
                        bom.releaseBasket();
                    }
                    
                    if (shop.getAuctionBasketHelper() != null) {
                        shop.getAuctionBasketHelper().releaseInvocationContext();
                    }
                } 
                else {
                    bom.releaseBasket();
                }
            }    
        }
        else { 
        	
            if ( !(userSessionData.getAttribute(B2cConstants.EASY_B2B).toString().equals("true")) ) {
            	
                Basket basket = bom.getBasket();
                basket.setAuctionBasket(auctionRelated);
                PartnerList pList = basket.getHeader().getPartnerList();
        
                BusinessPartner soldTo = bom.createBUPAManager().getDefaultBusinessPartner(PartnerFunctionData.SOLDTO);
                
        		if (soldTo != null) {
					pList.setSoldTo(new PartnerListEntry(soldTo.getTechKey(),soldTo.getId()));
					basket.updateHeader(bom.createBUPAManager());
        		}
        
            } 
        }
    }//end of isaPerform method
    
    
    /**
     * Login to the webshop using the normal login functionality or
     * the login that supports loyalty programs if the webshop 
     * contains a loyalty program.
     * 
     * @param usersissiondata
     * @param business object manager
     * @param user data
     * 
     * 
     */
    protected LoginStatus login(HttpServletRequest request,
								HttpServletResponse response,
								RequestParser requestParser,
								UserSessionData userSessionData, 
								BusinessObjectManager bom, 
								User user)
              throws CommunicationException {
    
      LoginStatus status = LoginStatus.NOT_OK;
      boolean doNormalLogin = true;
	  Shop shop = bom.getShop();
      
	  if (shop != null) {
	    // login supporting loyalty program 
	    if (shop.isEnabledLoyaltyProgram()) {
	 	  LoyaltyMembership loyMembership = LoyaltyUtilities.createLoyaltyMembership(requestParser, userSessionData, bom);
		  // login and check loyalty program	
		  status = user.loginAndCheckLoyaltyMembership(loyMembership);
		  doNormalLogin = false;
	    } 
	  }
	  
	  if (doNormalLogin) {
		  status = user.login();  	  
	  }
	  	  
       return status;
    }   

    
    
    protected String determineForward (UserSessionData userSessionData, BusinessObjectManager bom) {
    	
    	String forwardTo = "success";
    	
    	Shop shop = bom.getShop();
        if (shop != null) {
          if (shop.isEnabledLoyaltyProgram() &&
        	  // joining loyalty should be possible everytime therefore remove this check here	
        	  // if it is available the normal logon procedure appears pushing the "Log On" button
        	  //LoyaltyUtilities.isJoiningLoyaltyProgram(userSessionData) &&
        	  !LoyaltyUtilities.getLoyaltyMembership(userSessionData).exists()) {
        	forwardTo = "successLoyalty";  
          }
        } 
        
        return forwardTo;
    }
}// end of LoginAction class


