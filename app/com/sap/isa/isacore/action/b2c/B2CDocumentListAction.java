/*****************************************************************************
    Class:        B2CDocumentListAction
    Copyright (c) 2002, SAP AG, All rights reserved.
    Author        SAP AG
    Created:      03.09.2002

    $Revision: #3 $
    $Date: 2002/11/25 $
*****************************************************************************/

package com.sap.isa.isacore.action.b2c;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.businessobject.order.DocumentListFilter;
import com.sap.isa.businessobject.order.OrderDataContainer;
import com.sap.isa.businessobject.order.OrderStatus;
import com.sap.isa.businessobject.order.PartnerList;
import com.sap.isa.businessobject.order.PartnerListEntry;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.businesspartner.businessobject.PartnerFunctionBase;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.actionform.b2c.order.B2CDocumentListSelectorForm;

/**
 * Read document headers from a backend
 * @deprecated This class will be removed with CRM 6.0 and be replaced with the generic
 *             search framework (10.12.2004).
 *
 * @author  SAP AG
 * @version 1.0
 */
public class B2CDocumentListAction extends IsaCoreBaseAction {
	
  /**
   * Number of read document headers
   */
  public static final String RK_DOCUMENT_COUNT = "documentcount";

    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            BusinessObjectManager bom,
            IsaLocation log)
            throws CommunicationException {

		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        // Page that should be displayed next.
        String forwardTo = null;
        HttpSession session = request.getSession();


        // get / check the form
        if (form != null && !(form instanceof B2CDocumentListSelectorForm)) {
            log.exiting();
            throw new PanicException("status.message.noListActionForm");
        }

        B2CDocumentListSelectorForm documentListSelectorForm =
                (B2CDocumentListSelectorForm) form;

        // Store the from in the session context to allow a refreshment of
        // the list with already set search parameter.
        session.setAttribute("B2CDocumentListSelectorForm", (B2CDocumentListSelectorForm) documentListSelectorForm);


        // No documentListSelectorForm means this was the inital call for the Action WITHOUT any selection
        //    so just prepare selection JSP


        // get user
        User user = bom.getUser();
        if (user == null  ||  ! user.isUserLogged()) {
        	log.exiting();
			return mapping.findForward("login");
        }

        DocumentListFilter filter = documentListSelectorForm.getDocumentListFilter();

        // read the shop data
        Shop shop = bom.getShop();

        // Make Shop available for JSP (to check for Contract and quotation allowance)
        request.setAttribute(B2cConstants.RK_DLA_SHOP, shop);

        OrderStatus orderList = bom.getOrderStatus();

        if (orderList == null) {
//			Order order = new Order();
            OrderDataContainer order = new OrderDataContainer();
            orderList = bom.createOrderStatus(order);
        }

        // to fill the partnerlist in the orderstatus headers the following parameters
        // must be set, regardless if we search for a certain soldFrom or not.
        orderList.setMultiPartnerScenario(true);

        PartnerList partnerList = orderList.getPartnerList();
        partnerList.removePartnerData(PartnerFunctionBase.SOLDTO);
        partnerList.removePartnerData(PartnerFunctionBase.SOLDFROM);
        // add soldto to the partner list
        PartnerListEntry partner = new PartnerListEntry();
        partner.setPartnerId(bom.createBUPAManager().getDefaultBusinessPartner(PartnerFunctionBase.SOLDTO).getId());
        partnerList.setPartnerData(PartnerFunctionBase.SOLDTO, partner);

        orderList.setRequestedPartnerFunctions(new String[]{PartnerFunctionBase.SOLDFROM});

        BusinessPartnerManager buPaMa = bom.createBUPAManager();    
        TechKey soldToKey = (buPaMa.getDefaultBusinessPartner(PartnerFunctionBase.SOLDTO) != null 
                             ? buPaMa.getDefaultBusinessPartner(PartnerFunctionBase.SOLDTO).getTechKey()
                             : new TechKey(""));

        if (documentListSelectorForm .getAttribute().equals(B2CDocumentListSelectorForm.SOLDFROM)) {
            // add soldFrom to the partner list
            String soldFrom = documentListSelectorForm.getAttributeValue().trim();

            if (soldFrom != null && soldFrom.length() > 0) {
                partner = new PartnerListEntry();
                partner.setPartnerId(soldFrom);
                partnerList.setPartnerData(PartnerFunctionBase.SOLDFROM, partner);
             }

            if (log.isDebugEnabled()) {
               log.debug("Search for SoldFrom ID:        " + documentListSelectorForm.getAttributeValue().trim());
            }
       }

       if (log.isDebugEnabled()) {
            log.debug("Shop ID:        " + shop.getId().trim());
            log.debug("SoldTo GUID:    " + soldToKey.getIdAsString());
            log.debug("DocStatus:      " + filter.getStatus().trim());
            log.debug("DocDate:        " + filter.getChangedDate().trim());
            log.debug("ExtRefNo:       " + filter.getExternalRefNo());
            log.debug("DocId   :       " + filter.getId());
            log.debug("SoldFrom   :    " + documentListSelectorForm.getAttributeValue());
        }

       // Get Salesdocuments headers
       if (!documentListSelectorForm.isFirstCall()) {
           orderList.readOrderHeaders(soldToKey, shop, filter);
       }
       /*
       else {
           documentListSelectorForm.setFirstCall(false);
       }
       */

       request.setAttribute(B2cConstants.RK_DOCUMENT_COUNT, new Integer(orderList.getOrderCount()).toString());
       request.setAttribute(B2cConstants.RK_ORDER_LIST, orderList);
       request.setAttribute(B2cConstants.LINK_SELECTED,"OPEN_ORDERS");
		log.exiting();
       return mapping.findForward("orderlist");
    }
}