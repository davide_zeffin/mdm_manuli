/*****************************************************************************
  Copyright (c) 2000, SAP AG, All rights reserved.
  Author:       SAP AG
  Created:      17 May 2001

  $Revision: #6 $
  $Date: 2003/10/23 $
****************************************************************************/

package com.sap.isa.isacore.action.b2c;

// framework dependencies
import java.util.HashMap;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.businesspartner.businessobject.Identification;
import com.sap.isa.core.FrameworkConfigManager;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.isacore.AddressFormular;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.user.util.RegisterStatus;

/**
 * Looks what the user wants to do with his personal data: Change the title,
 * change a country, save the changes or nothing.
 */
public class SavePersonalDetailsAction extends IsaCoreBaseAction {
	
	/**
	* Initialize the action.
	* In B2C this action could be used by anonymous user.
	*/
	public void initialize() {
		if (isB2C()) {
			this.checkUserIsLoggedIn = false;
		}	
	}
	
    /**
     * Main method for the execution of this Struts Action<br />.
     * This method will be called by IsaCoreBaseAction class.
     * 
     * @param mapping           The ActionMapping used to select this instance
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * 
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
                                    ActionForm form,
                                    HttpServletRequest request,
                                    HttpServletResponse response,
                                    UserSessionData userSessionData,
                                    RequestParser requestParser,
                                    BusinessObjectManager bom,
                                    IsaLocation log) throws CommunicationException {

        // page that should be displayed next
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        String forwardTo = null;

        // ask the business object manager for user and shop
        User user = bom.getUser();
        Shop shop = bom.getShop();

        HashMap verifyUserExistData = new HashMap();
        boolean userExist = false;
        boolean pwChangeRequired = false;

        // create a new address formular
        AddressFormular addressFormular = new AddressFormular(requestParser);
        // address format id
        int addressFormatId = shop.getAddressFormat();
        // the next line is nessassary in case of a reconstruction of the soldToAddress JSP
        addressFormular.setAddressFormatId(addressFormatId);

        // check for possible cases
        // case A: if the user changes the country....
        if (request.getParameter("countryChange").length() > 0) {

            // build up the detail data needed for the soldToAddress-JSP
			prepareDisplayDetailData(mapping, form, request, response, userSessionData, requestParser, bom, log, addressFormular);

            request.setAttribute(B2cConstants.MESSAGE_LIST, user.getMessageList());
            log.exiting();
            return mapping.findForward("countryChange");
        }
        // case B: if the user changes the default country
        else if(request.getParameter("countiesRefresh").length() > 0) {

            log.debug("Refresh counties");

            // build up the detail data needed for the soldToAddress-JSP
            prepareDisplayDetailData(mapping, form, request, response, userSessionData, requestParser, bom, log, addressFormular);

            // list of counties
            ResultData listOfCounties =
                shop.getTaxCodeList(
                    addressFormular.getAddress().getCountry(),
                    addressFormular.getAddress().getRegion(),
                    addressFormular.getAddress().getPostlCod1(),
                    addressFormular.getAddress().getCity());

            request.setAttribute(B2cConstants.POSSIBLE_COUNTIES, listOfCounties);
            request.setAttribute(B2cConstants.MESSAGE_LIST, user.getMessageList());

            log.exiting();
            return mapping.findForward("countiesRefresh");
        } 
        // case C: if the user decided to cancel
        else if (request.getParameter("cancelClicked").length() > 0) {
			user.clearMessages();			
            log.exiting();
            return mapping.findForward("cancel");
        } 
        // case D: the user decided to save the personal details
        else {

            // Note:
            // Either firstName/lastName or name1/name2 is equal to "" within
            // the address-object which associated to the addressFormular-object
            // because of the kind of the JSP's construction. A title change is
            // not allowed while displaying the personal details

            // WORKAROUND because of the behavior of the CRM backend related to
            // address changes of addresses with existing tax jurisdiction code.
            // However, this involves the county selection problem which must be
            // fixed later.

            // if no 'new' tax juridiction code was given by selecting a county
            // then take the 'old' under the following conditions:
            // The values for postal code, city, country and region are unchanged.

            //B2C email check
            String oldId = user.getUserId();
            String oldPassword = user.getPassword();
            
            String newId = request.getParameter("email");
            String oldPass = request.getParameter("nolog_oldpassword");
            String password = request.getParameter("nolog_password");
            String passwordVerify = request.getParameter("nolog_passwordVerify");

			BusinessPartnerManager buPaMa =  bom.createBUPAManager();
			BusinessPartner userPartner = buPaMa.getDefaultBusinessPartner(PartnerFunctionData.SOLDTO);

//TODO: Attention: the check newId.equals(oldId.toUpperCase()) only works with email authentication!
            //check if the email id has been changed or not
            // if yes, check if user exists with the same e-mail address 	
            if ( !userSessionData.getAttribute(B2cConstants.EASY_B2B).toString().equals("true") &&
                 ((!user.IsCallCenterAgent() && !newId.toUpperCase().equals(oldId.toUpperCase())) ||
                  (user.IsCallCenterAgent() && !newId.equalsIgnoreCase(userPartner.getAddress().getEMail()))) )  {
                if (password != null) {
                    // if password != null, the user was already asked to change his password 
                    // I.e. this action will be executed for second time and while the first
                    // execution the request parameter "actionpwchange" was set to "pwchange" 
                    verifyUserExistData = user.verifyUserExistenceExtended(newId, password);
                }
                else {
                    // this code will be executed if the action will be called for the first time:
                    verifyUserExistData = user.verifyUserExistenceExtended(newId, oldPassword);
                }

                userExist = ((Boolean)verifyUserExistData.get("USEREXIST")).booleanValue();
                pwChangeRequired = ((Boolean)verifyUserExistData.get("PWCHANGEREQUIRED")).booleanValue();
            
                log.debug("Result of user existence check: userExists: " + userExist +
                          " pwChangeRequired: " + pwChangeRequired);
            }

            // an user account with the same email address (user id) exists already		 
            if (userExist) {
                
                if(pwChangeRequired) {
                    // ask the user to choose another password (because two user accounts with the same email 
                    // address and the same password exist (typically relevant for non-unique email scenarios))
                    user.addMessage(new Message(Message.ERROR, "b2c.persdet.newpw"));
                    request.setAttribute("actionpwchange", "pwchange"); // new fileds for pwchange will be displayed                     
                }
                else {
                    // if we're running in a scenario where the login type allows only unique email addresses (="4"),
                    // we show only an error message that another email address should be used
                    user.addMessage(new Message(Message.ERROR, "b2c.persdet.emailnotvalid", new String[] {newId}, null));
                }

                // build up the detail data needed for the soldToAddress-JSP
                prepareDisplayDetailData(mapping, form, request, response, userSessionData, requestParser, bom, log, addressFormular);
        
                request.setAttribute(B2cConstants.MESSAGE_LIST, user.getMessageList());
                log.exiting();
                return mapping.findForward("failure");
            }

            // this condition will be executed when the old password is incorrect	
            //change password
            if (!oldId.equals("") || oldId != "") {
                    
                if (oldPass != null && !oldPassword.equals(oldPass)) {
                    
                    // the entered old password doesn't match with the stored password in the user object
                    user.addMessage(new Message(Message.ERROR, "b2c.persdet.oldpwdnotcorrect"));
                    request.setAttribute("actionpwchange", "pwchange");

                    // build up the detail data needed for the soldToAddress-JSP
                    prepareDisplayDetailData(mapping, form, request, response, userSessionData, requestParser, bom, log, addressFormular);
        
                    request.setAttribute(B2cConstants.MESSAGE_LIST, user.getMessageList());
                    log.exiting();
                    return mapping.findForward("failure");
                }
                                
                if (password != null && passwordVerify != null
                        && !password.equals(passwordVerify) ) {
                    
                    // the entered new password and new password verification don't match
                    user.addMessage(new Message(Message.ERROR, "b2c.persdet.newpwdsnotmatch"));
                    request.setAttribute("actionpwchange", "pwchange");
                                
                    // build up the detail data needed for the soldToAddress-JSP
                    prepareDisplayDetailData(mapping, form, request, response, userSessionData, requestParser, bom, log, addressFormular);
        
                    request.setAttribute(B2cConstants.MESSAGE_LIST, user.getMessageList());
                    log.exiting();
                    return mapping.findForward("failure");
                }

                if (password != null) {
                    user.changePassword(password, passwordVerify);
                }
            }

            if (forwardTo == null
                    || newId.toUpperCase().equals(oldId.toUpperCase())) {
                    
                // save the changes to the backend...saveStatus returns the status of the process
                RegisterStatus saveStatus =
                        user.changeCustomer(shop.getId(), addressFormular.getAddress());

                switch (saveStatus.toInteger()) {
                    case 0 :
                        // the user is successful in saving the personal details

                        buPaMa =  bom.createBUPAManager();
                        BusinessPartner partner;

                        if (userSessionData.getAttribute(B2cConstants.EASY_B2B).toString().equals("true")) {
                            partner = buPaMa.getDefaultBusinessPartner(PartnerFunctionData.CONTACT);                             
                        }
                        else {
                            partner = buPaMa.getDefaultBusinessPartner(PartnerFunctionData.SOLDTO);
				    		// set new address in BusinessPartner area
			     			// enforce reread the partner address instead of overtake of changed address
							// to get enhanced address data from the backend
							//partner.setAddress(addressFormular.getAddress());
				    		partner.setAddress(null);
                        }

                        // update welcome-text
                        String welcomeText = "";
                        if (addressFormular.isPerson()) {
                            String title = partner.getAddress().getTitle();
                            String lastName = partner.getAddress().getName();
                            // Should the shop description also be part of the welcome text?
                            // String shopDescription = shop.getDescription();
                            // if (shopDescription != null) { ... }
                            welcomeText = title + " " + lastName;
                        }

                        // get the current session
                        HttpSession session = request.getSession();
                        // set session attributes for the welcome-text (navigationbar.jsp)
                        session.setAttribute(B2cConstants.WELCOME_TEXT, welcomeText);

                        forwardTo = "success";

                        if (!checkVerificationWord(partner, shop, request)) {
							if (partner.hasMessages()){
								Iterator it = partner.getMessageList().iterator();
								Message msg = null;
								while (it.hasNext()){
									msg = (Message)it.next();
									msg.setProperty(B2cConstants.VERIFICATION_WORD);
									user.addMessage(msg);
								}
							}
							else {
                                String vWord = request.getParameter(B2cConstants.VERIFICATION_WORD);
                                if (vWord != null && vWord.length() > 60) {
                                    user.addMessage(new Message(Message.ERROR, "b2c.verificationword.tolong", null, B2cConstants.VERIFICATION_WORD));
                                }
                                else {
                                    user.addMessage(new Message(Message.ERROR, "b2c.verificationword.missing", null, B2cConstants.VERIFICATION_WORD));
                                }
							} 
                            forwardTo = "failure";
                        }
                        break;

                    case 1 :
                        // registration error

                        // build up the detail data needed for the soldToAddress-JSP
                    prepareDisplayDetailData(mapping, form, request, response, userSessionData, requestParser, bom, log, addressFormular);

						setExtendedRequestAttribute(request, B2cConstants.MESSAGE_LIST, user.getMessageList());

                        forwardTo = "failure";
                        break;

                    case 2 :
                        // county required ...

                        // build up the detail data needed for the soldToAddress-JSP
                    prepareDisplayDetailData(mapping, form, request, response, userSessionData, requestParser, bom, log, addressFormular);

                        // list of counties
                        ResultData listOfCounties =
                                shop.getTaxCodeList(
                                    addressFormular.getAddress().getCountry(),
                                    addressFormular.getAddress().getRegion(),
                                    addressFormular.getAddress().getPostlCod1(),
                                    addressFormular.getAddress().getCity());

       			    	request.setAttribute(B2cConstants.POSSIBLE_COUNTIES, listOfCounties);
				    	request.setAttribute(B2cConstants.MESSAGE_LIST, user.getMessageList());

                        forwardTo = "countyRequired";
                        break;
                } // end: switch
            } // end of if block to decide further processing
        }

        // build up the detail data needed for the soldToAddress-JSP
        prepareDisplayDetailData(mapping, form, request, response, userSessionData, requestParser, bom, log, addressFormular);

        request.setAttribute(B2cConstants.MESSAGE_LIST, user.getMessageList());
		log.exiting();
        return mapping.findForward(forwardTo);

    }//end of isaPerform method

	/**
	 * Sets data into the request object which will be needed to show the soldToAddress.inc.jsp again.<br />
	 * This method was used in older versions and is deprecated. Please use instead 
     * {@link #prepareDisplayDetailData(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse, UserSessionData, RequestParser, BusinessObjectManager, IsaLocation, AddressFormular)}
	 * 
	 * @param request HttpServletRequest
	 * @param shop Shop data
	 * @param addressFormular Address formular with entered adrres data
	 * @throws CommunicationException
     * 
     * @deprecated Please use instead {@link #prepareDisplayDetailData(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse, UserSessionData, RequestParser, BusinessObjectManager, IsaLocation, AddressFormular)}
     *             This method will be removed earliest in the E-Commerce release 6.0.
	 */
	protected void setDisplayDetailData(HttpServletRequest request, 
										Shop shop, 
										AddressFormular addressFormular) 
			throws CommunicationException {
       
	}

    /**
     * Sets data into the request object which will be needed to show the soldToAddress.inc.jsp again.<br />
     * This method will be needed if for instance while processing of the data an error occured and 
     * the flow should returns back to the soldToAddress.inc.jsp.
     * 
     * @param mapping           The ActionMapping used to select this instance
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @param addressFormular   Address formular with entered adrres data
     * 
     * @throws CommunicationException
     */    
    protected void prepareDisplayDetailData(ActionMapping mapping,
                                            ActionForm form,
                                            HttpServletRequest request,
                                            HttpServletResponse response,
                                            UserSessionData userSessionData,
                                            RequestParser requestParser,
                                            BusinessObjectManager bom,
                                            IsaLocation log,
                                            AddressFormular addressFormular)
            throws CommunicationException {
                                            
        Shop shop = bom.getShop();
        ResultData formsOfAddress = shop.getTitleList();
        ResultData listOfCountries = shop.getCountryList();
        if (addressFormular.isRegionRequired()) {
            ResultData listOfRegions = shop.getRegionList(addressFormular.getAddress().getCountry());
            if (listOfRegions != null) {
                request.setAttribute(B2cConstants.POSSIBLE_REGIONS, listOfRegions);
            }
        }
        request.setAttribute(B2cConstants.FORMS_OF_ADDRESS, formsOfAddress);
        request.setAttribute(B2cConstants.POSSIBLE_COUNTRIES, listOfCountries);
        request.setAttribute(B2cConstants.ADDRESS_FORMULAR, addressFormular);
        setExtendedRequestAttribute(request, B2cConstants.ADDRESS_FORMULAR, addressFormular);

        // Verification Word
        String vWordTechKey = request.getParameter(B2cConstants.VERIFICATION_WORD_TK);
        String vWord = request.getParameter(B2cConstants.VERIFICATION_WORD);

        request.setAttribute(B2cConstants.VERIFICATION_WORD_TK, vWordTechKey);
        request.setAttribute(B2cConstants.VERIFICATION_WORD, vWord);
        
        // Call deprecated method to avoid incompatible changes
        // (should be removed earliest in ECO 6.0)
        setDisplayDetailData(request, shop, addressFormular);
    }

	private boolean checkVerificationWord(BusinessPartner partner, Shop shop, HttpServletRequest request) {
		boolean showVerification = false;
		boolean retval=false; // ID not changed

		try {
			// get the Verification Word's Diplay Status (if there's any)
			showVerification = (boolean)  (
			(String)FrameworkConfigManager.Servlet.getInteractionConfigParameter(
				request.getSession(),
				"ui",
				"ShowVerificationWord",
				""
				)
			).equals("true");
		}
		catch (Exception ex) {
				log.debug("Verification word not found in XCM: " + ex.getMessage());
			}

		if (showVerification) {
			String strType = shop.getIdTypeForVerification();
			String strNewNumber = request.getParameter(B2cConstants.VERIFICATION_WORD);  		
			Identification vWord_old = null; 
			String strTechKey = request.getParameter(B2cConstants.VERIFICATION_WORD_TK);
		
			if (strNewNumber.length() == 0) {
				retval = false; // no ID is not allowed!
			}
			else { // that's the way - ID must be supplied
                if (strNewNumber.length() > 60 ) {
                    retval = false;
                }
                else {
                    vWord_old = partner.getIdentification(strTechKey);
        
                    if (vWord_old != null) {
                        if (!vWord_old.getNumber().equalsIgnoreCase(strNewNumber)) {
                            if (partner.removeIdentification(strTechKey)) {
                                // preserve all other fields, only change ID
                                vWord_old.setNumber(strNewNumber);
                                partner.addIdentification(vWord_old);
								retval = true;
                            }
                            else {
								//it was not possible to remove the identification - probably no authorization.
								//display a message.
								retval = false;
                            }
                        } // it's ok not to change the ID
                
                        retval = true;
                    }
                    else if (vWord_old == null) {
                        Identification vWord = new Identification();
                        vWord.setType(strType);
                        vWord.setNumber(strNewNumber);

                        retval = partner.addIdentification(vWord);
                    }
                }
			}
		}
		else {
			// no verification word displayed hence no change required
			retval = true;
		}
		
		return retval;
	}

}// end of SavePersonalDetailsAction class


