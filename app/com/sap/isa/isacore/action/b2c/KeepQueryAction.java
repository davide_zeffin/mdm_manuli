/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Thorsten Mohr
  Created:      21 November 2001

*****************************************************************************/

package com.sap.isa.isacore.action.b2c;

// framework dependencies
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 * The <code>KeepQueryAction</code> is used to write a given query string into the
 * session context. This is a slight work-around to keep the query string in mind for
 * a product search which was entered by the user until it will be used
 * by an appropriate action.
 */
public class KeepQueryAction extends IsaCoreBaseAction {
	
    /**
     * Constant that should be used as a key corresponding to query string value
     * entered by the user (on the navigationbar-JSP).
     */
    public static final String QUERY_STRING = "queryString";
    /*
     * Name of the query string input field on the navigationbar-JSP.
     */
    public static final String QUERY = "query";


	/**
	 * Initialize the action.
	 * In B2C this action could be used by anonymous user.
	 * 
	 * @see com.sap.isa.isacore.action.IsaCoreBaseAction#initialize()
	 */
	public void initialize() {
		if (isB2C()) {
			this.checkUserIsLoggedIn = false;
		}	
	}



    /**
     * Overriden <em>isaPerform</em> method of <code>IsaCoreBaseAction</code>.
     */
    public ActionForward isaPerform(ActionMapping mapping,
        ActionForm form,
        HttpServletRequest request,
        HttpServletResponse response,
        UserSessionData userSessionData,
        RequestParser requestParser,
        BusinessObjectManager bom,
        IsaLocation log)
        throws CommunicationException {
        	
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
		
        String queryString = request.getParameter(QUERY);
        request.getSession().setAttribute(QUERY_STRING, queryString);
		log.exiting();
        return mapping.findForward("continue");
  }
}