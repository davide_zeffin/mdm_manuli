/*****************************************************************************
  Copyright (c) 2000, SAP AG, All rights reserved.
  Author:       SAP
  Created:      17 April 2001

  $Revision: #5 $
  $Date: 2003/10/23 $
*****************************************************************************/

package com.sap.isa.isacore.action.b2c;

import javax.servlet.http.HttpServletRequest;

import com.sap.isa.businessobject.Address;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.core.UserSessionData;

/**
 * Prepares the displaying of the sold-to address data by setting all those needed
 * data as request attributes.
 */
public class DisplayPersonalDetailsAction extends DisplayAddressBaseAction {

	/**
	 * Return the address which should be used. <br>
	 * The address will be read from business object.
	 * 
	 * @param request current request 
	 * @param partner business partner, which address should be changed.
	 * @return
	 */
	protected Address getAddress(
		HttpServletRequest request,
		UserSessionData userSessionData,
		BusinessObjectManager bom) {
		BusinessPartnerManager buPaMa = bom.createBUPAManager();

		// BusinessPartner partner;

		if (userSessionData.getAttribute(B2cConstants.EASY_B2B).toString().equals("true")) {
			partner = buPaMa.getDefaultBusinessPartner(PartnerFunctionData.CONTACT);
		} else {
			partner = buPaMa.getDefaultBusinessPartner(PartnerFunctionData.SOLDTO);
		}

		if (partner != null) {
			//          use the user business object to get the soldToAddress
			return partner.getAddress();
		} else {
			return null;
		}
	}

}