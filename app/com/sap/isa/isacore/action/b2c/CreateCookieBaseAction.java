/*****************************************************************************
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP
    Created:      November 2002
    Version:      1.0

    $Revision: #11 $
    $Date: 2002/08/05 $

*****************************************************************************/

package com.sap.isa.isacore.action.b2c;

// framework dependencies
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sap.isa.core.util.CookieUtil;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 * This base class could be used from Actions in the B2C scenario to create or
 * update a cookie.<br>
 * Use {@link #createOrUpdateCookie} to create or update the cookie
 * 
 */
public class CreateCookieBaseAction extends IsaCoreBaseAction {

    /**
     * Character used to separate the (different) content strings of the cookie.
     */

    public static final String COOKIE_CONTENTS_SEPARATOR = "&";


    /**
     * This method should be used to determine the check sum of the cookie's contents.
     *
     * @param cookieContents The contents of the cookie is the input (string) of the check sum
     * algorithm.
     * @return The computed check sum as a string.
     * @deprecated use {@link com.sap.isa.core.util.CookieUtil#getCheckSum}
     *  instead.
     */
    public static String getCheckSum(String cookieContents) {

        return CookieUtil.getCheckSum(cookieContents);
    }

    /**
     * Returns the content for a given Cookie
     * 
     * @param cookie cookie to use
     * @return content of the cookie or <code>null</code> if an error occured.
     */ 
    public static String getCookieContent(Cookie cookie) {
       
        return CookieUtil.getCookieContent(cookie);
    }

    /**
     * Create or update a cookie.
     *
     * @param cookieContent content of the cookie
     * @param cookieName    name of the cookie
     * @param request       http request
     * @param respons       http response
     */
    protected void createOrUpdateCookie(String cookieContent,
                                        String cookieName,
                                        HttpServletRequest request,
                                        HttpServletResponse response) {

		CookieUtil.createOrUpdateCookie(cookieContent, cookieName,"/", request, response);
    }


}