/*****************************************************************************
  Copyright (c) 2000, SAP AG, All rights reserved.
  Author:       SAP
  Created:      22nd April 2001

  $Revision: #4 $

  $Date: 2003/10/23 $
*****************************************************************************/

package com.sap.isa.isacore.action.b2c;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.ecommerce.businessobject.loyalty.LoyaltyMembership;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.action.loyalty.LoyaltyUtilities;
import com.sap.isa.user.action.UserActions;

/**
 * This action changes a password, it can use either for password change on
 * the logon screen or for user switch.
 * Therefore the caller of the jsp must set the parameter <em>ACTION_NAME</em> to
 * the correct value.
 *
 * Note that all Action classes have to provide a non argument constructor
 * because they were instantiated automatically by the action servlet.
 */
public class PWChangeAction extends IsaCoreBaseAction {
	
    // Name for request parameter used in jsp to get the forward string
    public static final String FORWARD_NAME  = "forward";

    // Define the forward after the password is changed
    public static final String FORWARD_NAME_PWCHANGE  = "showpwchange";

    public static final String FORWARD_NAME_SELECTSOLDTO = "selectsoldto";

    // Define the forward after the password is changed
    public static final String FORWARD_NAME_SUCCESS  = "success";

    // Name for request parameter used in jsp to get the action string
    public static final String ACTION_NAME  = "actionpwchange";

    // normal password change
    public static final String ACTION_PW    = "pwchange";

    // user switch
    public static final String ACTION_US    = "userswitch";

       // expired password
    public static final String ACTION_EP    = "expired_password";

    /** Parameter name for password */
    public static final String PN_PASSWORD    = "nolog_password";

    /** Parameter name for password verify*/
    public static final String PN_PASSWORD_VERIFY    = "nolog_passwordVerify";

	/**
	* Initialize the action.
	* In B2C this action could be used by anonymous user.
	* Check for logged user will be disable here because while changing of an 
	* initial password no user is logged in.
	*/
	public void initialize() {
		if (isB2C()) {
			this.checkUserIsLoggedIn = false;
		}	
	}
	
    /**
     * Overriden <em>isaPerform</em> method of <code>IsaCoreBaseAction</code>.
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            BusinessObjectManager bom,
            IsaLocation log)
            throws CommunicationException {

		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        String forwardTo = null;
		String welcomeText = "";
		// get the current session
		HttpSession session = request.getSession();
		// get user object
		User user = bom.getUser();
		// Page that should be displayed next
        RequestParser.Parameter forwardParameter = requestParser.getParameter(FORWARD_NAME);

		// change user password
		ActionForward forward = UserActions.performPwChange(mapping,
                                                            request,
                                                            requestParser,
                                                            user);
        
        String forwardName = forward.getName();
        
        if(forwardName.equalsIgnoreCase(forwardParameter.getValue().getString()) ||
		   forwardName.equalsIgnoreCase("success")) {
		   	
			BusinessPartnerManager buPaMa = bom.createBUPAManager();
			BusinessPartner partner = 
					LoginAction.setBusinessPartner(userSessionData,
												   user.getBusinessPartner(),
												   buPaMa);
            
			// update welcome-text
			if ( userSessionData.getAttribute(B2cConstants.EASY_B2B).toString().equals("true") ) {
				partner = buPaMa.getDefaultBusinessPartner(PartnerFunctionData.CONTACT);
			}
			else {
				partner = buPaMa.getDefaultBusinessPartner(PartnerFunctionData.SOLDTO);
			}


			if (partner != null && partner.getAddress() != null && 
					!partner.getAddress().getLastName().equals("")) {
				String title = partner.getAddress().getTitle();
				String lastName = partner.getAddress().getName();
				welcomeText = title + " " + lastName;
			}
			
			userSessionData.setAttribute(B2cConstants.LOGIN_STATUS, "LOGIN_SUCCESS");
			session.setAttribute(B2cConstants.WELCOME_TEXT, welcomeText);
			
			// get loyalty membership data if loyalty is enabled 
			// and business partner is loyalty member 
			Shop shop = bom.getShop();      
			if (shop != null) {
			  if (shop.isEnabledLoyaltyProgram()) {
			    LoyaltyMembership loyMembership = LoyaltyUtilities.createLoyaltyMembership(requestParser, userSessionData, bom);
				loyMembership.getMembershipData(partner);
			  }
			}	  
		}
        
		log.exiting();
        return forward;
    }
}

