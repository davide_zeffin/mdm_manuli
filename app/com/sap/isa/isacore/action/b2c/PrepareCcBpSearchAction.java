package com.sap.isa.isacore.action.b2c;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businesspartner.action.BupaConstants;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.StartupParameter;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.action.IsaCoreInitAction;

public class PrepareCcBpSearchAction extends IsaCoreBaseAction {
	public ActionForward isaPerform(ActionMapping mapping,
									 ActionForm form,
									 HttpServletRequest request,
									 HttpServletResponse response,
									 UserSessionData userSessionData,
									 RequestParser requestParser,
									 BusinessObjectManager bom,
									 IsaLocation log,
									 IsaCoreInitAction.StartupParameter startupParameter,
									 BusinessEventHandler eventHandler)
			throws CommunicationException {

		// TODO if SSO and BP available call confirmation page
		//	return mapping.findForward("ccbpconfirmation");
		// TODO else call CcBpSearchPage
		//	return mapping.findForward("ccbpsearch");
        String bupaId;
        
        BusinessPartnerManager buPaMan = bom.createBUPAManager(); 
        request.setAttribute(BupaConstants.MESSAGE_LIST, buPaMan.getMessageList());

        UserSessionData userData = UserSessionData.getUserSessionData(request.getSession()); // createUserSessionData(request.getSession(), false);
        StartupParameter sP = (StartupParameter) userData.getAttribute(SessionConst.STARTUP_PARAMETER);
        bupaId = sP.getParameterValue("bupaid");
        request.setAttribute("bupaid", bupaId);

						return mapping.findForward("success");
					}

}
