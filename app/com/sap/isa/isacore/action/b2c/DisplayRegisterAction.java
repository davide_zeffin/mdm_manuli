/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Biju Raj
  Created:     05 April 2001

  $Revision: #4 $
  $Date: 2001/07/30 $
*****************************************************************************/

package com.sap.isa.isacore.action.b2c;

// framework dependencies
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.Address;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.isacore.AddressFormular;
import com.sap.isa.isacore.action.IsaCoreBaseAction;


/**
 * Prepares the data necessary to display the register formular.
 */
public class DisplayRegisterAction extends IsaCoreBaseAction {

    /**
     * Flag: login via SU01 user ID or SU01 user Alias is enabled / disabled.
     */
    public static final String LOGIN_VIA_USER_ID = "loginViaUserId";
	
	/**
	 * Initialize the action.
	 * In B2C this action could be used by anonymous user.
	 * 
	 * @see com.sap.isa.isacore.action.IsaCoreBaseAction#initialize()
	 */
	public void initialize() {
		if (isB2C()) {
			this.checkUserIsLoggedIn = false;
		}	
	}

    /**
     * Overriden <em>isaPerform</em> method of <code>IsaCoreBaseAction</code>.
     */
    public ActionForward isaPerform(ActionMapping mapping,
                                    ActionForm form,
                                    HttpServletRequest request,
                                    HttpServletResponse response,
                                    UserSessionData userSessionData,
                                    RequestParser requestParser,
                                    BusinessObjectManager bom,
                                    IsaLocation log) throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        
        // get user to check if already logged in
        User user = bom.getUser();
        if(user.isUserLogged()) {
        	// user is logged in but wants to register for loyalty program
        	if (userSessionData.getAttribute("joinLoyaltyProgram") != null &&
        		userSessionData.getAttribute("joinLoyaltyProgram").toString() != null &&
        		userSessionData.getAttribute("joinLoyaltyProgram").toString().equals("true")) {		
        		return mapping.findForward("success");	
        	}
        	
            return mapping.findForward("loggedin");            
        }

        // get the shop details to which the user intends to login
        Shop shop = bom.getShop();
        
        boolean isBackendR3 = (shop.getBackend().startsWith(Shop.BACKEND_R3) ? true : false);
        // if login with user ID enabled:
        if(!isBackendR3 && (user.getLoginType().equals(User.LT_CRM_USERALIAS) || user.getLoginType().equals(User.LT_CRM_USERID) )) {
            request.setAttribute(LOGIN_VIA_USER_ID, "true");  
        }
        
		/* address formular exist already in the context, e.g. country or region change */
		AddressFormular addressFormular = (AddressFormular) request.getAttribute(B2cConstants.ADDRESS_FORMULAR);
		if (addressFormular!=null) {
			log.exiting();
			return mapping.findForward("success");
		}

        // get the forms of address supported in the shop from the backend
        ResultData formsOfAddress = shop.getTitleList();
        // get the list of countries from the shop Object
        ResultData listOfCountries = shop.getCountryList();
        // get the default country for the given shop
        String defaultCountryId = shop.getDefaultCountry();
        // flag that indicates whether a region is needed to complete an address
        boolean isRegionRequired = false;
        // list of regions
        ResultData listOfRegions = null;

        // used to find out if the default country needs regions to complete the address
        listOfCountries.beforeFirst();
        while (listOfCountries.next()){
            if (listOfCountries.getString("ID").equalsIgnoreCase(defaultCountryId)){
                isRegionRequired = listOfCountries.getBoolean("REGION_FLAG");
            }
        }

        // get the list of regions in case the region flag is set
        if (isRegionRequired){
            listOfRegions = shop.getRegionList(defaultCountryId);
        }

        // address format id
        int addressFormatId = shop.getAddressFormat();
        // create an address formular object to transfer some default settings
        // to the register JSP
        Address address = (Address)userSessionData.getAttribute("auctionaddress");
        if(userSessionData.getAttribute("RegistrationAuctionRequired") != null
        	&& userSessionData.getAttribute("RegistrationAuctionRequired").equals("true"))  {
			addressFormular = new AddressFormular(address, addressFormatId);
        }  else  {
			addressFormular = new AddressFormular(new Address(), addressFormatId);	
        }
        

        // set address formular values
        // the first found form of address for a person should be marked as default,
        // if there is such a form of address at all
        formsOfAddress.beforeFirst();
        while(formsOfAddress.next()){
            if(formsOfAddress.getBoolean("FOR_PERSON")){
                addressFormular.getAddress().setTitleKey(formsOfAddress.getString("ID"));
                addressFormular.setPerson(true);
                break;
            }
        }
        // default country
        addressFormular.getAddress().setCountry(defaultCountryId);
        addressFormular.setRegionRequired(isRegionRequired);

        // set all the required data as request attributes and pass it to the register JSP
        request.setAttribute(B2cConstants.FORMS_OF_ADDRESS, formsOfAddress);
        request.setAttribute(B2cConstants.POSSIBLE_COUNTRIES,listOfCountries);
        request.setAttribute(B2cConstants.ADDRESS_FORMULAR, addressFormular);
        if (isRegionRequired) {
            request.setAttribute(B2cConstants.POSSIBLE_REGIONS, listOfRegions);
        }
		log.exiting();
        return mapping.findForward("success");
    }//end of isaPerform method
}// end of DisplayRegisterAction class