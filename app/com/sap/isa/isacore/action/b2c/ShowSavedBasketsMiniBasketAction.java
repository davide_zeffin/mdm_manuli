/*****************************************************************************
    Class         ShowSavedBasketsMiniBasketAction
    Copyright (c) 2006, SAP AG, Germany, All rights reserved.
    Description:  Action to check prerequisites for displaying the saved basket list
    Created:      08.02.06
    Version:      1.0

*****************************************************************************/

package com.sap.isa.isacore.action.b2c;

// framework dependencies
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 * This action is called to check if the save basket function is enabled and 
 * the user is logged on. These are the prerequisites for the display of the saved
 * baskets within the mini basket area.
 */
public class ShowSavedBasketsMiniBasketAction extends IsaCoreBaseAction {

	public void initialize() {
	   this.checkUserIsLoggedIn = false;
   }
   /**
     * Overriden <em>isaPerform</em> method of <code>IsaCoreBaseAction</code>.
     */
    public ActionForward isaPerform(ActionMapping mapping,
                                    ActionForm form,
                                    HttpServletRequest request,
                                    HttpServletResponse response,
                                    UserSessionData userSessionData,
                                    RequestParser requestParser,
                                    BusinessObjectManager bom,
                                    IsaLocation log)
                                    throws CommunicationException {

		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        String forward = "nolist";
        
        // get the shop
        Shop shop = bom.getShop();
		if (shop == null) {
			log.exiting();
			throw new PanicException("Shop not found in BOM");        	
		}       
        // get the user
        User user = bom.getUser();
		if (user == null) {
			log.exiting();
			throw new PanicException("User not found in BOM");
		}   
		//       
        if (shop.isSaveBasketAllowed() && user.isUserLogged()) {
        	forward = "resultlist";        	
        }

		log.exiting();
        return mapping.findForward(forward);
    }
}

