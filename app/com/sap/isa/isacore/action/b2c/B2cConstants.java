/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Biju Raj
  Created:      3rd April 2001

  $Revision: #2 $
  $Date: 2003/01/21 $
*****************************************************************************/

package com.sap.isa.isacore.action.b2c;

/**
 * The class <code>B2cConstants</code> is the place where constants should be
 * defined which may be used anywhere in the B2C application. It is e.g. recommended
 * to use these constants instead of normal strings if a binding name for an
 * object has to be choosen.<p>
 * <b>Example</b><p>
 * recommended usage
 * <pre>
 * request.setAttribute(B2cConstants.SHOP_DESCRIPTION, shopDescription);
 * ...
 * String shopDescription = (String) request.getAttribute(B2cConstants.SHOP_DESCRIPTION);
 * </pre>
 * <p>
 * instead of
 * <pre>
 * request.setAttribute("shop description", shopDescription);
 * ...
 * String shopDescription = (String) request.getAttribute("shop description");
 * </pre>
 */

public class B2cConstants {

    /**
     * Title to be stored in request scope.
     */
    public static final String TITLE = "title";

    /**
     * Constant for the last name to be stored in request scope.
    */
    public static final String LAST_NAME = "last name";

    /**
     * Constant for the shop descirption to be stored in request scope.
     */
    public static final String SHOP_DESCRIPTION = "shop description";

    /**
     * Name under which the welcome text is bound in the session context. The
     * welcome text should be displayed after the user has successfully logged in.
     */
    public static final String WELCOME_TEXT = "welcome text";

    /**
     * This constant is used to store if the user has successfully logged into the shop.
     */
    public static final String LOGIN_SUCCESS = "Login Successful";

    /**
     * This constant is used to store a login failure.
     */
    public static final String LOGIN_FAILURE = "Login Failed";

    /**
     * This constant is used to store the login status.
     */
    public static final String LOGIN_STATUS = "Login Status";

    /**
     * This constant is used to store the frame name to be used by the frameset
     * to decide on the combination of frames to be displayed.
     */
    public static final String FRAME_NAME = "frameName";

    /**
     * This constant is used to store the frame from which the login JSP was
     * called so that control could be passed back to the appropriate frameset.
     */
    public static final String FRAME_BEFORE_LOGIN = "frameBeforeLogin";
    /**
     * This constant is used to store the resultset showing the forms of address
     * to be used e.g. in the registration form.
     */
    public static final String FORMS_OF_ADDRESS = "formsOfAddress";

    /**
     * This constant is used to store the resultset showing the list of possible
     * countries to be used e.g. in the registration form.
     */
    public static final String DEFAULT_COUNTRY = "defaultCountries";

    /**
     * This constant is used to store the resultset showing the list of possible
     * countries to be used e.g. in the registration form.
     */
    public static final String POSSIBLE_COUNTRIES = "possibleCountries";

    /**
     * This constant is used to store the resultset showing the list of
     * possible regions to be used e.g. in the registrtion form.
     */
    public static final String POSSIBLE_REGIONS = "possibleRegions";

    /**
     * This constant is used to store the resultset showing the list of
     * possible counties to be used e.g. in the registrtion form.
     */
    public static final String POSSIBLE_COUNTIES = "possibleCounties";

    /**
     * Name of the basket stored in the request context.
     */
    public static final String RK_BASKET = "basket";

    /**
     * Name of the basket header stored in the request context.
     */
    public static final String RK_BASKET_HEADER = "basketHeader";

    /**
     * Name of the list of items stored in the request context.
     */
    public static final String RK_BASKET_ITEM_LIST = "basketItems";

    /**
     * Name of the list of leaflet items stored in the request context.
     */
    public static final String RK_LEAFLET_ITEM_LIST = "leafletItems";

    /**
     * Name used to store the number of leaflet items in the request context.
     */
    public static final String NUM_LEAFLET_ITEMS = "numLeafletItems";

    /**
     * Name used to bind an <code>AddressFormular</code> object.
     */
    public static final String ADDRESS_FORMULAR = "addressFormular";

    /**
     * Name used to bind an <code>MessageList</code> object.
     */
    public static final String MESSAGE_LIST = "messageList";

    /**
     * Name used to bind the ship-to address.
     */
    public static final String SHIP_TO_ADDRESS = "shipToAddress";

    /**
     * Name used to bind the password of the user.
     */
    public static final String PASSWORD = "password";

    /**
     * Name used to bind the bupaid in callcenter mode bupa search.
     */
    public static final String CCM_BUPAID = "soldtoId";

    
    /**
     * Name used to bind the repeated password of the user.
     */
    public static final String PASSWORD_VERIFY = "passwordVerify";

    /**
     * Name used to bind an <code>User</code> object.
     */
    public static final String USER = "user";

    /**
     * Name of the attribute, which describes the current action in "MyAccount" frame set.
     */
    public static final String RC_USED_ACCOUNT_ACTION = "usedAccountAction";

    /**
     * Number of read document headers.
     */
    public static final String RK_DOCUMENT_COUNT = "documentcount";

    /**
     * List of sales documents stored in request scope.
     */
    public static final String RK_ORDER_LIST = "orderlist";

    /**
     * Date from user in the Document filter
     */
    public static final String RK_DATE_FROM = "19000101";

    /**
     * Shop instance
     */
    public static final String RK_DLA_SHOP = "shopinstance";

    /**
     * Document filter to be passed as request parameter.
     */
    public static final String RK_FILTER = "documentListFilter";

    /**
     * Link selected to be passed as request parameter.
     */
    public static final String LINK_SELECTED = "linkselected";

    /**
     * Link selected to be passed as request parameter.
     */
    public static final String CATALOG = "catalog";

    /**
     * Request parameter value for the leaflet
     */
    public static final String RK_VALUE_LEAFLET = "leaflet";

    /**
     * Description of the basket that should be saved.
     */
    public static final String SAVE_BASKET_DESCRIPTION =
        "saveBasketDescription";

    /**
     * Key to retrieve an <code>UnloggedUserInfo</code> object from the user
     * session data (if there is such an object at all).
     */
    public static final String UNLOGGED_USER_INFO = "unloggedUserInfo";

    /**
     * Switch on/off B2C as easyB2B
     *
     */
    public static final String EASY_B2B = "easyB2B";

    /**
    * Terms and conditions for partner in Channel Commerce Hub scenario
    *
    */
    public static final String TERMS_AND_CONDS = "termsAndConds";

    /**
     * Session context constant for storage of the shipto selection object
     */
    public static final String SC_SHIPTO_SELECTION = "shipToSelection";

    /**
     * List of all availabilty symbols in a cch scenario stored in request scope.
     */
    public static final String RK_CCH_TRAFFICLIGHTS = "cchTrafficlights";
    
	/**
	 *   Request parameter passed to enable direct addToBasket functionality for web crawler 
	 *   enabled static HTML pages accessing the shop  
	 *	 
	 */

	public final static String ADD_BASKET_DIRECT = "addToBasket";
	
	/**
	 *   Request parameter that contains the Item Id which has to be 
	 *   added to the basket.  
	 *	 
	 */

	public final static String ADD_ITMID_DIRECT = "itemId";
	
	/**
	 *   Request parameter that contains the Tech Key of Verification Word  
	 *   for Telco scenarios.  
	 *	 
	 */

	public final static String VERIFICATION_WORD_TK = "verificationWordTk";

	/**
	 *   Request parameter that contains the Verification Word only (fka Service Password) 
	 *   for Telco scenarios.  
	 *	 
	 */

	public final static String VERIFICATION_WORD = "verificationWord";

	/**
	 *   Request parameter that contains the Shop Id which has to be 
	 *   displayed directly
	 *	 
	 */
		
	public final static String SHOPID_DIRECT = "strshopid";
	
	/**
	 *   This variable is set to true when the minibasket loads for the    
	 *   first time. Subsequently it is set to false to prevent the recurrent
	 *	 java script execution in minibasket.jsp. 
	 */		
	public final static String ALLOW_MINIBASKET_ONLOAD = "strAllowCallinMiniBasket";

	/**
	 * Constant used to store the information if basket has been loaded directly 
	 * when request came from static HTML page generated to enable web crawlers search
	 * for products on the site 
	 *
	 */	
	 
	public final static String BASKET_LOADED_DIRECT = "basketLoadOver"; 


	/**
	  *  ENABLE_WEB_CRAWLERS  is initially set in the XCM
	  *         and may be overwritten by customers to enable or disable web crawlers.
	 */
	 public final static String ENABLE_WEB_CRAWLERS = "crawlerEnabled";
    
}