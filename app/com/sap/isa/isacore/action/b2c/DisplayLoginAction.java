/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Biju Raj
  Created:      3rd April 2001

  $Revision: #2 $
  $Date: 2004/09/27 $
*****************************************************************************/

package com.sap.isa.isacore.action.b2c;

// framework dependencies
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 * Prepares the login procedure by setting an appropriate
 * <code>UserSessionData</code> attribute. This attribute will indicate later
 * that the user has forced the login procedure and not the application itself.
 */
public class DisplayLoginAction extends IsaCoreBaseAction {
	
	/**
	 * Initialize the action.
	 * In B2C this action could be used by anonymous user.
	 * 
	 * @see com.sap.isa.isacore.action.IsaCoreBaseAction#initialize()
	 */
	public void initialize() {
		if (isB2C()) {
			this.checkUserIsLoggedIn = false;
		}	
	}


    /**
     * Overriden <em>isaPerform</em> method of <code>IsaCoreBaseAction</code>.
     */
    public ActionForward isaPerform(ActionMapping mapping,
                                    ActionForm form,
                                    HttpServletRequest request,
                                    HttpServletResponse response,
                                    UserSessionData userSessionData,
                                    RequestParser requestParser,
                                    BusinessObjectManager bom,
                                    IsaLocation log) {

        /* The frame before login attribute will be used during the processing
         * of the ArrangeFramesMainfsAction.
         * 
         * aeb: 
         * this attribute will only be nedded, if B2C is running as frame application
         */
		 final String METHOD_NAME = "isaPerform()";
		 log.entering(METHOD_NAME);
        userSessionData.setAttribute(B2cConstants.FRAME_BEFORE_LOGIN,"INITIAL");
        userSessionData.setAttribute("joinLoyaltyProgram", "false");
        log.exiting();
        return mapping.findForward("success");
    }
}
