/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Biju Raj
  Created:      22nd April 2001

  $Revision: #2 $

  $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.isacore.action.b2c;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 * Prepares the displaying of the product order status.
 */
 public class PrepareProductDetailAction extends IsaCoreBaseAction {

    /**
     * Overriden <em>isaPerform</em> method of <code>IsaCoreBaseAction</code>.
     */
    public ActionForward isaPerform(ActionMapping mapping,
                ActionForm form,
                HttpServletRequest request,
                HttpServletResponse response,
                UserSessionData userSessionData,
                RequestParser requestParser,
                BusinessObjectManager bom,
                IsaLocation log)
                        throws CommunicationException {


       // Page that should be displayed next.
	   final String METHOD_NAME = "isaPerform()";
	   log.entering(METHOD_NAME);
       String forwardTo = null;
       // all parameters required to get all open orders set in the doument filter
       // and passed to the backend using the orderstatus

       WebCatInfo catalog =
            getCatalogBusinessObjectManager(userSessionData).getCatalog();

       request.setAttribute(B2cConstants.CATALOG,catalog);

       forwardTo = "orderstatusdetail";
	   log.exiting();
       return mapping.findForward(forwardTo);
    }//end of isaPerform method

}// end of ShowOpenOrdersAction class

