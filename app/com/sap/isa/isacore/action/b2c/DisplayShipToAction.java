/*****************************************************************************
    Class         DisplayShipToAction
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Action for the displaying the ship-to address data
    Author:       Thorsten Mohr
    Created:      12.09.01
    Version:      1.0

    $Revision: #2 $
    $Date: 2001/07/18 $
*****************************************************************************/

package com.sap.isa.isacore.action.b2c;

// framework dependencies
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.Address;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.isacore.AddressFormular;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 * Prepares the displaying of the ship-to address data of an order. It is supposed
 * that the ship-to address will be available within the request. All other
 * needed request attributes to build up the <i>Ship-to</i> JSP will also be set.
 */
public class DisplayShipToAction extends IsaCoreBaseAction {

    /**
     * Overriden <em>isaPerform</em> method of <code>IsaCoreBaseAction</code>.
     */
    public ActionForward isaPerform(ActionMapping mapping,
                                    ActionForm form,
                                    HttpServletRequest request,
                                    HttpServletResponse response,
                                    UserSessionData userSessionData,
                                    RequestParser requestParser,
                                    BusinessObjectManager bom,
                                    IsaLocation log)
                                    throws CommunicationException {

		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        // get the shop
        Shop shop = bom.getShop();

        // get the ship-to address
        Address address = (Address) request.getAttribute(B2cConstants.SHIP_TO_ADDRESS);

        // get the forms of address supported in the shop from the backend.
        ResultData formsOfAddress = shop.getTitleList();
        // get the list of countries from the shop
        ResultData listOfCountries = shop.getCountryList();
        //set the default country to the value already in the ship-to's address
        String defaultCountryId = address.getCountry();
        // flag that indicates whether a region is needed to complete an address
        boolean isRegionRequired = false;
        // list of regions
        ResultData listOfRegions = null;

        // check whether the default country has a needs region to complete the address.
        listOfCountries.beforeFirst();
        while (listOfCountries.next()){
            if (listOfCountries.getString("ID").equalsIgnoreCase(defaultCountryId)){
                isRegionRequired = listOfCountries.getBoolean("REGION_FLAG");
            }
        }

        // if region is needed then use Shop to get the list of regions
        if (isRegionRequired){
            listOfRegions = shop.getRegionList(defaultCountryId);
        }

        // address format id
        int addressFormatId = shop.getAddressFormat();
        // create a new AddressFormular instance
        AddressFormular addressFormular = new AddressFormular(address, addressFormatId);
        // this section sets the 'isPerson'-value and 'isRegionRequired'-value accordingly
        if(shop.isTitleKeyForPerson(address.getTitleKey())) {
            addressFormular.setPerson(true);
        }
        else {
            addressFormular.setPerson(false);
        }
        addressFormular.setRegionRequired(isRegionRequired);

        // set all the required data as request attributes and pass them to the shipTo JSP
        request.setAttribute(B2cConstants.FORMS_OF_ADDRESS, formsOfAddress);
        request.setAttribute(B2cConstants.POSSIBLE_COUNTRIES,listOfCountries);
        if (isRegionRequired) {
            request.setAttribute(B2cConstants.POSSIBLE_REGIONS, listOfRegions);
        }
        request.setAttribute(B2cConstants.ADDRESS_FORMULAR, addressFormular);

        //get the list of counties if available
        ResultData possibleCounties = shop.getTaxCodeList(address.getCountry(),
                                                          address.getRegion(),
                                                          address.getPostlCod1(),
                                                          address.getCity());
        request.setAttribute(B2cConstants.POSSIBLE_COUNTIES, possibleCounties);

		log.exiting();
        return mapping.findForward("showshipto");
    }
}

