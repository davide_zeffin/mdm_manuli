/*****************************************************************************
    Class         MaintainShipToAction
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Action for the Ship-to Maintenance
    Author:       Wolfgang Sattler, Thorsten Mohr
    Created:      April 20001
    Version:      1.0

    $Revision: #8 $
    $Date: 2001/12/19 $
*****************************************************************************/

package com.sap.isa.isacore.action.b2c;
// framework dependencies
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.ShipTo;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.isacore.AddressFormular;
import com.sap.isa.isacore.ShipToSelection;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 * Creates a new ship-to address for the order, if the user has decided to change
 * the current ship-to address.
 */
public class MaintainB2CShipToAction extends IsaCoreBaseAction {

     /**
     * Overriden <em>isaPerform</em> method of <code>IsaCoreBaseAction</code>.
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            BusinessObjectManager bom,
            IsaLocation log)
            throws CommunicationException {

		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        // Page that should be displayed next.
        String forwardTo = null;
        // indicates whether the try to save the ship-to address data was
        // successfull (= 0) or not
        int saveStatus = 1;

        // list of countries
        ResultData listOfCountries;
        // list of regions
        ResultData listOfRegions = null;
        // list of forms of address
        ResultData formsOfAddress;

        // ask the business object manager for the shop
        Shop shop = bom.getShop();

        // create a new address formular
        AddressFormular addressFormular = new AddressFormular(requestParser);
        // address format id
        int addressFormatId = shop.getAddressFormat();
        // the next line is nessassary in case of a reconstruction of the register JSP
        addressFormular.setAddressFormatId(addressFormatId);

        if (request.getParameter("titleChange").length() > 0) {

            if (addressFormular.isPerson()) {
                 addressFormular.setPerson(false);
            }
            else{
                addressFormular.setPerson(true);
            }

            // build up the detail data needed for the ship-to JSP
            setDisplayDetailData(request, shop, addressFormular);

            forwardTo = "shipto";
        }
        else if(request.getParameter("countryChange").length() > 0) {

            // build up the detail data needed for the ship-to JSP
            setDisplayDetailData(request, shop, addressFormular);

            forwardTo = "shipto";
        }
        else if(request.getParameter("countiesRefresh").length() > 0) {

            log.debug("Refresh counties");

            // build up the detail data needed for the soldToAddress-JSP
            setDisplayDetailData(request, shop, addressFormular);

            request.setAttribute(B2cConstants.MESSAGE_LIST, addressFormular.getAddress().getMessageList());

            forwardTo = "shipto";
        }    
        else if(request.getParameter("cancelClicked").length() > 0) {
            
            forwardTo = "canceled";
        }
        else {
            // save ship-to address data

            // Note:
            // Either firstName/lastName or name1/nam2 must be set equal to ""
            // because of the kind of the JSP's construction. With other words,
            // the values of the input fields that disappear after a title change,
            // are not forgotten. They will be displayed again after another
            // title change.
            if (addressFormular.isPerson()) {
                addressFormular.getAddress().setName1("");
                addressFormular.getAddress().setName2("");
            }
            else {
                addressFormular.getAddress().setFirstName("");
                addressFormular.getAddress().setLastName("");
            }

            // save the ship-to address
            SalesDocument document = bom.getOrder();
            if (document == null) {
              document = bom.getBasket();
            }

            document.clearMessages();

			BusinessPartnerManager buPaMa = bom.createBUPAManager();
			BusinessPartner soldTo = buPaMa.getDefaultBusinessPartner(PartnerFunctionData.SOLDTO);

            saveStatus = document.addNewShipTo(addressFormular.getAddress(),
										soldTo.getTechKey(),
                                        shop.getTechKey());

            if (saveStatus == 0) {
                
                ShipToSelection shipToSelection = (ShipToSelection)request.getSession().getAttribute(B2cConstants.SC_SHIPTO_SELECTION);           

                if (shipToSelection != null) {
                    
                    ShipTo shipTo = document.getHeader().getShipTo();

                    shipToSelection.setActualShipToKey(shipTo.getTechKey());
                }
                
                //Store the address in the session. It will be used if the order is canceled and created again
                request.getSession().setAttribute(B2cConstants.SHIP_TO_ADDRESS, addressFormular.getAddress());
            }
            else {    
                // an error occurred, maybe a county will be required

                // build up the detail data needed for the ship-to JSP
                setDisplayDetailData(request, shop, addressFormular);

                request.setAttribute(B2cConstants.MESSAGE_LIST, addressFormular.getAddress().getMessageList());

                forwardTo = "shipto";
            }
            
        }

        if (saveStatus == 0) {
        	log.exiting();
            return mapping.findForward("success");
        }
        else {
        	log.exiting();
            return mapping.findForward(forwardTo);
        }

    }
    
    /**
     * Sets data into the request object which will be needed to show the shipto.inc.jsp again.<br />
     * This method will be needed if for instance while processing of the data an error occured and 
     * the flow should returns back to the shipto.inc.jsp.
     *  
     * @param request HttpServletRequest
     * @param shop Shop data
     * @param addressFormular Address formular with entered adrres data
     * @throws CommunicationException
     */
    protected void setDisplayDetailData(HttpServletRequest request, 
                                        Shop shop, 
                                        AddressFormular addressFormular) 
            throws CommunicationException {
       
        ResultData formsOfAddress = shop.getTitleList();
        ResultData listOfCountries = shop.getCountryList();
        if (addressFormular.isRegionRequired()){
            ResultData listOfRegions = shop.getRegionList(addressFormular.getAddress().getCountry());
            if (listOfRegions != null) {
                request.setAttribute(B2cConstants.POSSIBLE_REGIONS, listOfRegions);
            }            
        }
        request.setAttribute(B2cConstants.FORMS_OF_ADDRESS,formsOfAddress);
        request.setAttribute(B2cConstants.POSSIBLE_COUNTRIES,listOfCountries);
        request.setAttribute(B2cConstants.ADDRESS_FORMULAR, addressFormular);
        
        // determine counties
        if (addressFormular.getAddress().getCountry().length()   > 0 &&
            addressFormular.getAddress().getCity().length()      > 0 &&
            addressFormular.getAddress().getRegion().length()    > 0 &&
            addressFormular.getAddress().getPostlCod1().length() > 0) {
                    
            ResultData listOfCounties = shop.getTaxCodeList(addressFormular.getAddress().getCountry(),
                                                            addressFormular.getAddress().getRegion(),
                                                            addressFormular.getAddress().getPostlCod1(),
                                                            addressFormular.getAddress().getCity());
            if (listOfCounties != null) {
                request.setAttribute(B2cConstants.POSSIBLE_COUNTIES, listOfCounties);
            }
        }
    }
}

