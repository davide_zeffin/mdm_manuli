/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Biju Raj
  Created:      3rd April 2001

  $Revision: #1 $
  $Date: 2001/06/26 $
*****************************************************************************/

package com.sap.isa.isacore.action.b2c;

// framework dependencies
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
// implementation dependencies

/**
 * Defines the arrangement of the frames within the B2C scenario after a successful
 * login of the user. The decision which steps should be performed next, will be
 * based on the values of several attributes of the <code>UserSessionData</code>.
 * I.e. the <code>UserSessionData</code> object holds the state of the application
 * before a possibly delayed login occurred.
 */
public class ArrangeFramesMainfsAction extends IsaCoreBaseAction {

	/**
	 * Initialize the action.
	 * In B2C this action could be used by anonymous user.
	 * 
	 * @see com.sap.isa.isacore.action.IsaCoreBaseAction#initialize()
	 */
	public void initialize() {
		if (isB2C()) {
			this.checkUserIsLoggedIn = false;
		}	
	}


    /**
     * Overriden <em>isaPerform</em> method of <code>IsaCoreBaseAction</code>.
     */
    public ActionForward isaPerform(ActionMapping mapping,
                                    ActionForm form,
                                    HttpServletRequest request,
                                    HttpServletResponse response,
                                    UserSessionData userSessionData,
                                    RequestParser requestParser,
                                    BusinessObjectManager bom,
                                    IsaLocation log)
                                    throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        // Page that should be displayed next.
        String forwardTo ="initial";
        String frameCombination =(String) userSessionData.getAttribute(B2cConstants.FRAME_NAME);
        String loginStatus =(String) userSessionData.getAttribute(B2cConstants.LOGIN_STATUS);
        String frameBeforeLogin =(String) userSessionData.getAttribute(B2cConstants.FRAME_BEFORE_LOGIN);

		userSessionData.setAttribute("SSLEnabled", getInteractionConfig(request).getConfig("shop").getValue("SSLEnabled"));
			
        if (frameCombination.equals("LOGIN")){
        // in case the mainFs_inner.jsp is called indirectly from the login.jsp
            if (loginStatus.equals("LOGIN_SUCCESS")){
                forwardTo = frameBeforeLogin.toLowerCase();
            }else if (loginStatus.equals("LOGIN_FAILURE")){
                forwardTo = frameCombination.toLowerCase();
            }else if (loginStatus.equals("BEFORE_LOGIN")){
				forwardTo = "login"; // for SSL switch
            }else if (frameBeforeLogin.equals("checkout"))
            	forwardTo = "checkout"; // for SSL switch
        }else  if (frameCombination.equals("REGISTER")){
        // in case the mainFs_inner.jsp is loaded indirectly by the register.jsp
            forwardTo = frameCombination.toLowerCase();
        }else  if (frameCombination.equals("CATALOG")){
            forwardTo = frameCombination.toLowerCase();
        }else if (frameCombination.equals("ACCOUNTS")){
			forwardTo = frameCombination.toLowerCase(); // for SSL switch
        }else if (frameCombination.equals("BASKET")){
			forwardTo = frameCombination.toLowerCase(); // for SSL switch
        }else if (frameCombination.equals("QUERY")){
			forwardTo = frameCombination.toLowerCase(); // for SSL switch
        }else if (frameCombination.equals("CHECKOUT")){
			forwardTo = frameCombination.toLowerCase(); // for SSL switch
        }else if (frameCombination.equals("PROFILE")){
			forwardTo = frameCombination.toLowerCase(); // for SSL switch
	}
		log.exiting();
        return mapping.findForward(forwardTo);
    }
}