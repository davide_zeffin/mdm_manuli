package com.sap.isa.isacore.action.b2c;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.User;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businesspartner.action.BupaConstants;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.action.IsaCoreInitAction;

public class CcBpSearchAction extends IsaCoreBaseAction {
	/** Request parameter name for email address in call center mode */
	public static final String PN_EMAILADDRESS = "email";

	/** Request parameter name for bpid in call center mode */
	public static final String PN_BUPAID = "soldtoId";


	private boolean CheckCCAgentAuthorization(	UserSessionData userSessionData, 
												MetaBusinessObjectManager mbom, 
												BusinessObjectManager bom) {
		
		if (checkPreConditions(userSessionData, mbom)==null) {  
			User user = bom.getUser();	
			return user.IsCallCenterAgent();
		}
		
		return false;
	}

	/**
	 * Overriden <em>isaPerform</em> method of <code>IsaCoreBaseAction</code>.
	 */
	public ActionForward isaPerform(ActionMapping mapping,
									 ActionForm form,
									 HttpServletRequest request,
									 HttpServletResponse response,
									 UserSessionData userSessionData,
									 RequestParser requestParser,
									 BusinessObjectManager bom,
									 IsaLocation log,
									 IsaCoreInitAction.StartupParameter startupParameter,
									 BusinessEventHandler eventHandler)
			throws CommunicationException {

		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
		// Page that should be displayed next.
		String forwardTo = null;
		
		User user = bom.getUser();
		
		BusinessPartnerManager buPaMan = bom.createBUPAManager(); 
		MetaBusinessObjectManager mbom = userSessionData.getMBOM();
		HttpSession session = request.getSession();
		ServletContext context = getServlet().getServletContext();

		checkPreConditions(userSessionData, mbom);

		if (CheckCCAgentAuthorization(userSessionData, mbom, bom) == true) {
			String okKey = WebUtil.translate(context, session, "b2c.callcentermode.ok", null);
			String cancelKey = WebUtil.translate(context, session, "b2c.callcentermode.cancel", null);
			buPaMan.getMessageList().clear();
			String ok = request.getParameter(okKey);
			boolean cancel = (request.getParameter(cancelKey)!= null) 
							? request.getParameter(cancelKey).length() > 0 : false;

			if (!cancel) {
				if ( ok != null && ok.length()>0 && CheckBpId(buPaMan, user, request, context) == true) {
					forwardTo="success";
				}
				else {
					request.setAttribute(BupaConstants.MESSAGE_LIST, buPaMan.getMessageList());
					forwardTo="error";
				} 
			}
			else {
				forwardTo="cancel";
			}
			 
			log.exiting();
		}
		else {
			// no message required
			forwardTo="logoff";
		}
		return mapping.findForward(forwardTo);
	}

	private boolean CheckBpId(BusinessPartnerManager buPaMan, User user, HttpServletRequest request, ServletContext context) {
		String userEmail = null;
		String userID = null;
		BusinessPartner bPartner = null;
		boolean retVal = false;

		userEmail = request.getParameter(PN_EMAILADDRESS);
		userID = request.getParameter(PN_BUPAID);
		bPartner = getBupa(buPaMan, userID, userEmail, request.getSession(), context); // via BAPI_BUPA_SEARCH
		
		if (bPartner != null) {
			user.setBusinessPartner(new TechKey(bPartner.getId()));
            user.setCallCenterUserId(getUserId(buPaMan, bPartner));
			retVal = true;
		}
        else {
            boolean msgFound = buPaMan.getMessageList().size() > 0;
            
            for (int n = 0; n < buPaMan.getMessageList().size(); n++) {
                Message msg = buPaMan.getMessageList().get(n);
                if (msg.getDescription().equalsIgnoreCase("b2c.callcentermode.tomanybp")) {
                    msgFound = true;
                    buPaMan.getMessageList().remove(msg.getResourceKey());
                    buPaMan.addMessage(new Message(Message.ERROR,  
                                                    "b2c.callcentermode.tomanybp",
                                                    null,
                                                    B2cConstants.CCM_BUPAID));
                }
            }
            
            if (!msgFound) {
                buPaMan.addMessage(new Message(Message.ERROR,  
                                                "b2c.callcentermode.bpnotfound",
                                                null,
                                                B2cConstants.CCM_BUPAID));
            }
        }

        if (!retVal) {		
            request.setAttribute(PN_EMAILADDRESS, userEmail );
            request.setAttribute(PN_BUPAID, userID );
        }
		return retVal;
	}

	private BusinessPartner getBupa(BusinessPartnerManager buPaMan, String userID, String userEmail, HttpSession session, ServletContext context) {
		BusinessPartner bPartner = null;
		String bupaRole = "CRM006";

		try {
			if (userEmail.length() > 0) {
				bPartner = buPaMan.getBusinessPartnerFromEmailAddress(userEmail, bupaRole);
			} else {
				String eMail;

				bPartner = buPaMan.getBusinessPartner(userID);
				eMail = bPartner.getAddress().getEMail();
				if (eMail == null || eMail.length()==0) {
                    buPaMan.addMessage(new Message(Message.ERROR, 
                                                   "b2c.callcentermode.bpnotvalid",
                                                   null,
                                                   B2cConstants.CCM_BUPAID));
					bPartner = null;
				}
				else {
					bPartner = buPaMan.getBusinessPartnerFromEmailAddress(eMail, bupaRole);
				}
			}
		}
		catch(Exception ex) {
			log.error(ex.getMessage()== null ? "null pointer" : ex.getMessage());
		}
		
		return bPartner;
	}
    
    private String getUserId(BusinessPartnerManager buPaMan, BusinessPartner bPartner) {
        String bpId = bPartner.getId();
        String userId = "";
        
        bpId = "0000000000" + bpId.trim();
        bpId = bpId.substring(bpId.length() - 10);
        
        userId = buPaMan.getUserIdFromBpId(bpId);
        
        return userId;
    }
}
