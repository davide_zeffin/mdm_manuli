/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Thorsten Mohr
  Created:      December 2001

*****************************************************************************/

package com.sap.isa.isacore.action.b2c;

// framework dependencies
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;

/**
 * This Action is used in the B2C scenario to save details (e.g. the technical key
 * of the sold-to) of an logged in user with the objective to reuse these informations
 * if the user reenter the shop (in future) but is not logged in at this time. With
 * the information contained in the Cookie created or updated by this action the user is
 * known although she/he is not logged in. On such a way e.g. the personal recommendations
 * of the (last logged in) user can directly be displayed.
 */
public class CreateOrUpdateUserCookieAction extends CreateCookieBaseAction {
	
    /**
     * The name of the cookie which stores the user's details
     */
    public static final String COOKIE_NAME = "rec_user";


    /**
     * Overriden <em>isaPerform</em> method of <code>IsaCoreBaseAction</code>.
     * The method creates a cookie with appropriate user details or updates the
     * cookie if it already exists.
     */
    public ActionForward isaPerform(ActionMapping mapping,
                                    ActionForm form,
                                    HttpServletRequest request,
                                    HttpServletResponse response,
                                    UserSessionData userSessionData,
                                    RequestParser requestParser,
                                    BusinessObjectManager bom,
                                    IsaLocation log)
                                    throws CommunicationException {

		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        // remove unlogged user info if there is such a info at all
        userSessionData.removeAttribute("unloggedUserInfo");

		BusinessPartnerManager buPaMa = bom.createBUPAManager();
		BusinessPartner soldTo = buPaMa.getDefaultBusinessPartner(PartnerFunctionData.SOLDTO);
		
        // the contents of the cookie is the user's technical key (as a string)
        String cookieContents = soldTo.getTechKey().getIdAsString();
        
        createOrUpdateCookie(cookieContents, COOKIE_NAME,request, response);       

        // determine forward
        String forwardTo = null;
        if (request.getParameter("nextStep") != null &&
            request.getParameter("nextStep").equals("shoplist")) {
            forwardTo = "shoplist";
        }
        else {
            forwardTo = "success";
        }
		log.exiting();
        return mapping.findForward(forwardTo);
    }
}