/*****************************************************************************
    Class:        EComCheckPortalAction
    Copyright (c) 2007, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      31.08.2007
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.isacore.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.Constants;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.StartupParameter;

/**
 * //TODO docu
 * The class EComCheckPortalAction checks if the current session is running in the portal 
 * using the portal startup parameter. <br>
 * 
 * <h4>The following forwards are used by this action</h4>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <th>forward</th>
 *     <th>description</th>
 *   <tr>
 *   <tr><td><em>no_portal</em></td><td><em>no portal</em></td></tr>
 *   <tr><td><em>portal</em></td><td><em>portal</em></td></tr>
 * </table>
*
 * @author  SAP AG
 * @version 1.0
 */
public class EComCheckPortalAction extends EComBaseAction {

	/**
	 * Overwrites the method ecomPerform. <br>
	 * 
	 * @param mapping            The ActionMapping used to select this instance
	 * @param form               The <code>FormBean</code> specified in the
	 *                              config.xml file for this action
	 * @param request            The request object
	 * @param response           The response object
	 * @param userSessionData    Object wrapping the session
	 * @param requestParser      Parser to simple retrieve data from the request
	 * @param mbom               Reference to the MetaBusinessObjectManager
	 * @param multipleInvocation Flag indicating, that a multiple invocation occured
	 * @param browserBack        Flag indicating a browser back
	 * @return Forward to another action or page
	 * 
	 * @throws IOException
	 * @throws ServletException
	 * @throws CommunicationException
	 * 
	 * @see com.sap.isa.isacore.action.EComBaseAction#ecomPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.sap.isa.core.UserSessionData, com.sap.isa.core.util.RequestParser, com.sap.isa.core.businessobject.management.MetaBusinessObjectManager, boolean, boolean)
	 */
	public ActionForward ecomPerform(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response,
			UserSessionData userSessionData,
			RequestParser requestParser,
			MetaBusinessObjectManager mbom,
			boolean multipleInvocation,
			boolean browserBack)
				throws IOException, ServletException, CommunicationException {
		final String METHOD_NAME = "ecomPerform()";
		log.entering(METHOD_NAME);		
		
		boolean isPortal = false;
		if (userSessionData != null) {
			StartupParameter startupParameter = (StartupParameter)userSessionData
					.getAttribute(SessionConst.STARTUP_PARAMETER);
			
			if (startupParameter.getParameterValue(Constants.PORTAL).equals("YES")) {
				isPortal = true;             
			}
		}
		
		log.exiting();
		return isPortal?mapping.findForward("portal"):mapping.findForward("no_portal");
	}

}
