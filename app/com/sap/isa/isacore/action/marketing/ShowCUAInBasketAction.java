/*****************************************************************************
    Class         ShowCUAInBasketAction
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Action to display the Bestseller
    Author:       Wolfgang Sattler
    Created:      May 2001
    Version:      1.0

    $Revision: #4 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.isacore.action.marketing;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.businessobject.marketing.CUAManager;
import com.sap.isa.businessobject.webcatalog.CatalogBusinessObjectManager;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 * Reading and display the CUA products for all products in the basket.<p>
 * <h4>Overview over inport parameters and attributes</h4>
 * No parameters will read within this Action. The basket is taken from the BOM
 *
 * <h4>The following forwards are used by this action</h4>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>success</td><td>standard forward</td></tr>
 * </table>
 *
 *
 */
public class ShowCUAInBasketAction extends IsaCoreBaseAction {

	/**
	 * Initialize the action.
	 * In B2C this action could be used by anonymous user.
	 * 
	 * @see com.sap.isa.isacore.action.IsaCoreBaseAction#initialize()
	 */
	public void initialize() {
		if (isB2C()) {
			this.checkUserIsLoggedIn = false;
		}	
	}


    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            BusinessObjectManager bom,
            IsaLocation log)
            throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        Shop shop = bom.getShop();

        if (shop == null) {
        	log.exiting();
            throw new PanicException("shop.notFound");
        }

        CatalogBusinessObjectManager cbom =
                getCatalogBusinessObjectManager(userSessionData);

        WebCatInfo catalog = cbom.getCatalog();

        if (catalog == null) {
        	log.exiting();
            throw new PanicException("catalog.notFound");
        }

        User user = bom.getUser();

        if (user == null) {
        	log.exiting();
            throw new PanicException("user.notFound");
        }

        if (shop.isCuaAvailable()) {
            // read the cua product for the items in the basket
            CUAManager cUAManager = bom.createCUAManager();
            boolean cuaFound = cUAManager.readAllCUAList(bom.getBasket(),
                                                         user,
                                                         shop,
                                                         catalog);

            // store cuaFound in request context
            request.setAttribute("cuafound",new Boolean(cuaFound));
        }
		log.exiting();
        return mapping.findForward("success");
    }

}
