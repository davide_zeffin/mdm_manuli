/*****************************************************************************
    Class:        ActionConstants
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Created:      Jan 2008
    Version:      1.0

    $Revision: #2 $
    $Date: 2008/01/17 $
*****************************************************************************/
package com.sap.isa.isacore.action.marketing;

/**
 * @author SAP AG
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */

public class MarketingActionConstants {
    
    public final static String DS_BESTSELLER           = "bestseller";

    public final static String DS_CUA                  = "cuaList";
    
    public final static String DS_ENTRY                = "catalogEntry";

    public final static String DS_RECOMMENDATION       = "recommendations";
    
    public final static String DS_IN_WINDOW            = "detailInWindow";
    
    public final static String RC_WEBCATITEM           = "currentItem";
    
    /**
     * Name used to bind an <code>User</code> object.
     */
    public static final String USER = "user";
    
    /**
     * This constant is used to store the frame from which the login JSP was
     * called so that control could be passed back to the appropriate frameset.
     */
    public static final String FRAME_BEFORE_LOGIN = "frameBeforeLogin";
    
    /**
     * Key to retrieve an <code>UnloggedUserInfo</code> object from the user
     * session data (if there is such an object at all).
     */
    public static final String UNLOGGED_USER_INFO = "unloggedUserInfo";
}
