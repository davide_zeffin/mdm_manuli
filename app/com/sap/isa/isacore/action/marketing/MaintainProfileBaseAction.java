/*****************************************************************************
    Class         MaintainProfileBaseAction
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Action for the Profile Maintenance
    Author:       SAP AG
    Created:      27 Februar 2001
    Version:      1.0

    $Revision: #2 $
    $Date: 2001/06/27 $

*****************************************************************************/
package com.sap.isa.isacore.action.marketing;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.backend.boi.isacore.marketing.MarketingBusinessObjectsAware;
import com.sap.isa.backend.boi.isacore.marketing.MarketingConfiguration;
import com.sap.isa.backend.boi.isacore.marketing.MarketingUser;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.marketing.MktChosenValues;
import com.sap.isa.businessobject.marketing.MktProfile;
import com.sap.isa.businessobject.marketing.MktValue;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;

/**
 * Save a profile, after he user had answered the questionary
 *
 * <p>
 * <h4>Overview over inport parameters and attributes</h4>
 * <table border="1" cellspacing="0" cellpadding="4">
 *   <tr>
 *     <td><b>name</b></td>
 *     <td><b>parameter</b></td>
 *     <td><b>attribute</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr>
 *      <td>AttributeSetKey</td><td>X</td><td>&nbsp</td>
 *      <td>techKey of the attribute set</td>
 *   </tr>
 *   <tr>
 *      <td>Attribute[]</td><td>X</td><td>&nbsp</td>
 *      <td>techkeys of the used attributes</td>
 *   </tr>
 *   <tr>
 *      <td>ChosenValues[]</td><td>X</td><td>&nbsp</td>
 *      <td>values, which are chosen from the user</td>
 *   </tr>
 *   <tr>
 *      <td>DataType[]</td><td>X</td><td>&nbsp</td>
 *      <td>data type of the attribute</td>
 *   </tr>
 *   <tr>
 *      <td>CancelMktProfile</td><td>X</td><td>&nbsp</td>
 *      <td>user has pressed the cancel button</td>
 *   </tr>
 *   <tr>
 *      <td>SaveMktProfile</td><td>X</td><td>&nbsp</td>
 *      <td>user has pressed the save button</td>
 *   </tr>
 *   <tr>
 *      <td>"More"+[Attribute[]]></td><td>X</td><td>&nbsp</td>
 *      <td>user has pressed the "more entry" button for the attribute
 *          with the key [Attribute[]] </td>
 *   </tr>
 * </table>
 *
 * <h4>The following forwards are used by this action</h4>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>canceled</td><td>forward used, if the user cancels the maintenance</td></tr>
 *   <tr><td>success</td><td>forward used, if the profile is saved for the user</td></tr>
 *   <tr><td>failure</td>
 *       <td>forward used, if an error occured while saving the profile</td>
 *   </tr>
 *   <tr><td>showprofile</td>
 *       <td>forward used, if the maintenance is going on. e.g.
 *           if the user needs more input fields
 *       </td>
 *   </tr>
 *   <tr><td>recommendation</td>
 *       <td>forward used, if the maintenance is called from the recommendation
 *           screen
 *       </td>
 *   </tr>
 *   <tr><td>unknowuser</td>
 *       <td>forward used, if the profile is saved for a unknown user. 
 *           In this case the technical key which was generate in the backend
 *           shoul be stored in a cookie.
 *       </td>
 *   </tr>
 * </table>
 * <h4>Overview over attributes set in the request context</h4>
 * <ul>
 * <li><code>BusinessObjectBase.CONTEXT_NAME</code></li>
 * </ul>
 *
 * @author SAP AG
 * @version 1.0
 *
 */
abstract public class MaintainProfileBaseAction extends MarketingBaseAction {

    /**
     * Name of profile stored in the request context.
     */
    public static final String RC_PROFILE = "MktProfile";

    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward marketingPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            MetaBusinessObjectManager mbom,
            MarketingConfiguration configuration,
            MarketingBusinessObjectsAware marketBom,
            IsaLocation log)
            throws CommunicationException {

		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        // Page that should be displayed next.
        String forwardTo = "failure";

        // Ask the business object manager for a user. If this object is not
        //  present, it is an error
        MarketingUser user = marketBom.getMarketingUser();
        if (user == null) {
        	log.exiting();
            return mapping.findForward("error");
        }

        if (configuration == null) {
        	log.exiting();
            throw new PanicException("configuration.notFound");
        }

        MktProfile profile = new MktProfile();

        String attributeSetKey;
		
        if (requestParser.getParameter("CancelMktProfile").isSet() && 
             requestParser.getParameter("CancelMktProfile").getValue().getString().length() > 0) {
        	forwardTo = "canceled";
            forwardTo = cancelMaintenance(userSessionData, requestParser, user, forwardTo);
			log.exiting();
            return mapping.findForward(forwardTo);
        }

        attributeSetKey = requestParser.getParameter("AttributeSetKey").getValue().getString();
        if (attributeSetKey != null) {
            profile.setAttributeSetKey(new TechKey(attributeSetKey));
        } else {
        	log.exiting();
            return mapping.findForward("showprofile");
        }

        String profileKey = requestParser.getParameter("ProfileKey").getValue().getString();
        if (profileKey != null) {
            profile.setTechKey(new TechKey(profileKey));
        }

        RequestParser.Parameter attributeParameter = requestParser.getParameter("Attribute[]");
        RequestParser.Parameter chosenParameter = requestParser.getParameter("ChosenValues[]");
        RequestParser.Parameter dataTypeParameter  = requestParser.getParameter("DataType[]");

        MktChosenValues chosenValues;

        for (int i = 0; i < attributeParameter.getNumValues(); i++ ) {

            String attributeKey = attributeParameter.getValue(i+1).getString();

            int dataType = dataTypeParameter.getValue(i+1).getInt();

            String chosenValuesKey = chosenParameter.getValue(i+1).getString();
            chosenValues = new MktChosenValues(new TechKey(chosenValuesKey),
                                               new TechKey(attributeKey),
                                               dataType );

            Map valueMap = requestParser.getParameter(attributeKey).getValueMap();
            int numValues = requestParser.getParameter(attributeKey).getNumValues();

            MktValue mktValue;

            int entry = 0;

            for (int h = 0; h < numValues; h++) {
                String value = (String)valueMap.get("" + h);
                if (value.trim().length() == 0) entry++;
                mktValue = new MktValue(new TechKey(value),"");
                chosenValues.addValue(mktValue);
            }

            if (requestParser.getParameter("More"+attributeKey).isSet() && 
			     requestParser.getParameter("More"+attributeKey).getValue().getString().length() > 0) {
                // user want more entry fields for this attribute!!
                mktValue = new MktValue(TechKey.EMPTY_KEY,"");
                // add tree new entries
                for (int k=0; k<requestParser.getParameter("CountofFields"+attributeKey).getValue().getInt();k++) {
                    // add maximal four new entries
                    if (entry > 3) {
                        k = requestParser.getParameter("CountofFields"+attributeKey).getValue().getInt();
                    }
                    else {
                        chosenValues.addValue(mktValue);
                    }
                    entry++;
                }
                forwardTo = "showprofile";
            }

            profile.addChosenValues(chosenValues);

        } //for

        request.setAttribute(BusinessObjectBase.CONTEXT_NAME, profile);

        forwardTo = setProfile(userSessionData, requestParser, forwardTo, user, configuration, profile);
        
        if (!profile.isValid()) {
            forwardTo = "failure";
        }
        
		log.exiting();
        return mapping.findForward(forwardTo);
    }


    /**
     * Exit to adjust user if the maintenance is canceled. <br>
     * 
     * @param userSessionData
     * @param requestParser
     * @param user
     * @param forwardTo
     * @return
     */
    protected String cancelMaintenance(UserSessionData userSessionData,
                                        RequestParser requestParser, 
    								    MarketingUser user,
									    String forwardTo) {
		return forwardTo;									  
	}


	/**
	 * Store the profile. <br>
	 * Check if the request parameter SaveMktProfile to decide if to store the
	 * profile.
	 * 
	 * @param requestParser parser
	 * @param forwardTo logical forward
	 * @param user current user object
	 * @param configuration configuration object
	 * @param profile profile object
	 * @return the logical forward which should be used; 
	 * @throws CommunicationException
	 */
    abstract protected String setProfile(UserSessionData userSessionData,
                                           RequestParser requestParser,
        					               String forwardTo,
        					               MarketingUser user,
        					               MarketingConfiguration configuration,
        					               MktProfile profile)         					   
        	throws CommunicationException; 

}

