/*****************************************************************************
    Class         ShowProfileBaseAction
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Action which display the profile maintenance screen
    Author:       SAP AG
    Created:      20 Februar 2001
    Version:      1.0

    $Revision: #2 $
    $Date: 2001/06/27 $

*****************************************************************************/

package com.sap.isa.isacore.action.marketing;

import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.backend.boi.isacore.marketing.MarketingBusinessObjectsAware;
import com.sap.isa.backend.boi.isacore.marketing.MarketingConfiguration;
import com.sap.isa.backend.boi.isacore.marketing.MarketingUser;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.marketing.MktAttribute;
import com.sap.isa.businessobject.marketing.MktAttributeSet;
import com.sap.isa.businessobject.marketing.MktProfile;
import com.sap.isa.businessobject.marketing.MktQuestion;
import com.sap.isa.businessobject.marketing.MktQuestionary;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.RequestParser;


/**
 * Display the questionary to maintain the profile of an User.
 *
 * <h4>Overview over inport parameters and attributes</h4>
 * <table border="1" cellspacing="0" cellpadding="4">
 *   <tr>
 *     <td><b>name</b></td>
 *     <td><b>parameter</b></td>
 *     <td><b>attribute</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr>
 *      <td>SaveMktProfile</td><td>X</td><td>&nbsp</td>
 *      <td>user has pressed the save button</td>
 *   </tr>
 * </table>
 *
 * <h4>The following forwards are used by this action</h4>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>success</td><td>standard forward to display profile</td></tr>
 * </table>
 * <h4>Overview over attributes set in the request context</h4>
 * <ul>
 * <li><code>BusinessObjectBase.CONTEXT_NAME</code></li>
 * <li><code>RK_MKTATTRIBUTE_LIST</code></li>
 * <li>questionary</li>
 * </ul>
 *
 * @author SAP AG
 * @version 1.0
 *
 */
abstract public class ShowProfileBaseAction extends MarketingBaseAction {

    /**
     * Name of the lists of marketing attributes stored in the request context.
     */
    public static final String RK_MKTATTRIBUTE_LIST = "mktattribute";


    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     *
     */
    public ActionForward marketingPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            MetaBusinessObjectManager mbom,
            MarketingConfiguration configuration,
            MarketingBusinessObjectsAware marketBom,
            IsaLocation log)
            throws CommunicationException{

		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        // Ask the business object manager for a user. If this object is not
        // already present, it's an error
        MarketingUser user = marketBom.getMarketingUser();

        if (configuration == null) {
        	log.exiting();
            throw new PanicException("MarketingConfiguration.notFound");
        }

        boolean needLogin = true;
        
        if (user.isUserLogged() || 
            ( configuration.isMarketingForUnknownUserAllowed() && !user.getMktPartner().isKnown()) ) {
           needLogin= false;     
        }               

        // determine whether the user is unknown
        if (needLogin) {
            user.addMessage(new Message(Message.INFO,"marketing.profile.needLogin"));
            request.getSession().setAttribute(MarketingActionConstants.USER,user);
            // this variable is used by an action called in mainFs.jsp to decide the frame to be displayed
            userSessionData.setAttribute(MarketingActionConstants.FRAME_BEFORE_LOGIN,"profile");
            log.exiting();
            return mapping.findForward("login");
        }


        // Ask the business object manager for the attribute set
        MktAttributeSet attributeSet = marketBom.createMktAttributeSet();

        request.setAttribute(BusinessObjectBase.CONTEXT_NAME, attributeSet);

        readAttributeSet(configuration, attributeSet);
        
        if (!attributeSet.isValid()) {
            request.setAttribute(BusinessObjectBase.CONTEXT_NAME, attributeSet);
            log.exiting();
            return mapping.findForward("error");
        }

        MktProfile profile = getProfile(user, configuration, attributeSet);

		if (profile != null && !profile.isValid()) {
			request.setAttribute(BusinessObjectBase.CONTEXT_NAME, profile);
		}

        MktQuestionary questionary = new MktQuestionary(attributeSet,profile);

        questionary.setSaved(false);
        if (requestParser.getParameter("SaveMktProfile").getValue().getString().length() > 0 && questionary.isValid()) {
            questionary.setSaved(true);
        }

        // create all info message for the questions
        Iterator i = questionary.iterator();

        Message info;
        MktQuestion question;
        MktAttribute attribute;

        while (i.hasNext()) {

            question = (MktQuestion)i.next();

            attribute = question.getAttribute();

            // create the corresponding info message for the attribute
            if (attribute.isMultiple()) {
                info = new Message(Message.INFO,"marketing.info.selectMultiple",null,"");
            }
            else {
                info = new Message(Message.INFO,"marketing.info.selectOne",null,"");
            }
            question.addMessage(info);

            if (!attribute.hasAllowedValues()) {
                // free entry
                String[] args = new String[2];
                args[0]= "";
                args[1]= "";

                if (attribute.isRangeAllowed()) {
                    info = new Message(Message.INFO,"marketing.info.intervalAllowed",null,"");
                    question.addMessage(info);
                }

                switch (attribute.getDataType()) {
                case MktAttribute.DTNUMC:
                case MktAttribute.DTCURR:

                    args[0] = "" + ( attribute.getLength() - attribute.getDecimals());

                    if (attribute.getDecimals() == 0) {
                        info = new Message(Message.INFO,"marketing.info.noDecimals",args,"");
                    }
                    else {
                        args[1] = "" + attribute.getDecimals();
                        info = new Message(Message.INFO,"marketing.info.Decimals",args,"");
                        question.addMessage(info);
                        if (configuration.getDecimalSeparator().equals(".")) {
                            info = new Message(Message.INFO,"marketing.info.Point",null,"");
                        }
                        else{
                            info = new Message(Message.INFO,"marketing.info.Comma",null,"");
                        }
                    }

                    question.addMessage(info);

                    if (!attribute.isSigned()) {
                        info = new Message(Message.INFO,"marketing.info.noNegatives",null,"");
                        question.addMessage(info);
                    }
                    break;
                case MktAttribute.DTDATE:
                    String dateFormat = configuration.getDateFormat();
                    if (dateFormat.equals(MarketingConfiguration.DATE_FORMAT1)) {
                        info = new Message(Message.INFO,"marketing.info.dateFormat1",null,"");
                    }
                    else if (dateFormat.equals(MarketingConfiguration.DATE_FORMAT2)) {
                        info = new Message(Message.INFO,"marketing.info.dateFormat2",null,"");
                    }
                    else if (dateFormat.equals(MarketingConfiguration.DATE_FORMAT3)) {
                        info = new Message(Message.INFO,"marketing.info.dateFormat3",null,"");
                    }
                    else if (dateFormat.equals(MarketingConfiguration.DATE_FORMAT4)) {
                        info = new Message(Message.INFO,"marketing.info.dateFormat4",null,"");
                    }
                    else if (dateFormat.equals(MarketingConfiguration.DATE_FORMAT5)) {
                        info = new Message(Message.INFO,"marketing.info.dateFormat5",null,"");
                    }
                    else if (dateFormat.equals(MarketingConfiguration.DATE_FORMAT6)) {
                        info = new Message(Message.INFO,"marketing.info.dateFormat6",null,"");
                    }
                    question.addMessage(info);
                    break;
                case MktAttribute.DTTIME:
                    info = new Message(Message.INFO,"marketing.info.timeFormat",null,"");
                    question.addMessage(info);
                    break;
                }
            }

            if (attribute.getUnit().length() > 0) {
                String args[] = new String[1];
                args[0]= attribute.getUnit();
                info = new Message(Message.INFO,"marketing.info.unit",args,"Unit");
                question.addMessage(info);
            }

        }


        // store the questionary in page context for the iteration tag
        request.setAttribute("questionary", questionary);
        request.setAttribute(RK_MKTATTRIBUTE_LIST, questionary);

        // release Attribute from BOM
        marketBom.releaseMktAttributeSet();
		log.exiting();
        return mapping.findForward("success");
    }


    /**
     * Return the profile found for the given attribute set. <br>
     * 
     * @param user current user
     * @param configuration configuration object
     * @param attributeSet given attribute set.
     * @return
     * @throws CommunicationException
     */
    abstract protected MktProfile getProfile(MarketingUser user,
                                               MarketingConfiguration configuration,
                                               MktAttributeSet attributeSet)
        	throws CommunicationException;

	
    /**
     * Read the attribute set. <br>
     * 
     * @param configuration configuration object 
     * @param attributeSet attribute set to read
     * @throws CommunicationException
     */
    abstract protected void readAttributeSet(MarketingConfiguration configuration, 
                                                MktAttributeSet attributeSet)
        	throws CommunicationException;

}
