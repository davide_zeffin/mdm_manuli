/*****************************************************************************
    Class         MarketingBaseAction
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Author:       SAP
    Created:      17.01.2008
    Version:      1.0

    $Revision: #1 $
    $Date: 2008/01/17 $
*****************************************************************************/
package com.sap.isa.isacore.action.marketing;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.backend.boi.isacore.BaseConfiguration;
import com.sap.isa.backend.boi.isacore.marketing.MarketingBusinessObjectsAware;
import com.sap.isa.backend.boi.isacore.marketing.MarketingConfiguration;
import com.sap.isa.backend.boi.isacore.marketing.MarketingUser;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.webcatalog.CatalogBusinessObjectManager;
import com.sap.isa.core.Constants;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.EComBaseAction;

/**
 * <p>
 * Base class that takes care of the save retrievement of the BOM and
 * session context. Another feature is that default catch statements for
 * all common exceptions are put into this class. The developer of a
 * an action can rely on this class to catch all Exceptions and display
 * corresponding pages.
 * If the correct log/tracing level (DEBUG) is set, all contextual data is
 * written do the log file. To provide a security feature, the value of
 * request parameters starting with <code>nolog_</code> are not written to
 * the logfile.<br>
 * You should for example protect credit card information with this
 * method.
 * </p>
 * <p>
 * <b>Note - In this version of the class every request coming from
 * one client is handled synchronized. This is done to prevent unpredictable
 * exceptions coming from the underlying middleware (JCo). This approach
 * may not be sufficient for high performance installations and it may
 * be necessary to switch to another model of synchronization later.</b>
 * </p>
 *
 * @author SAP
 * @version 1.0
 */

public abstract class MarketingBaseAction extends EComBaseAction {
    
    /**
     * Switch to activated the check, if the user is logged, in the 
     * <code>checkPreConditions</code> method. <br>
     * The switch is <code>true</code> for the b2b application otherwise it is <code>false</code> by default. <br>
     * <b>Overwrite this Value in the constructor or in the {@link #initialize}
     * method  to change the default behaviour. </b>
     * 
     * @see #checkUserIsLoggedIn(User)
     * @see #checkPreConditions
     */
    protected boolean checkUserIsLoggedIn = true; 
    
    /**
     * Switch to actived the check if the bom exist. <br>
     */
    protected boolean checkBom = true;
    
    /**
     * Template method to be overwritten by the subclasses of this class.
     * Normally you should overwrite this method and you do not touch the
     * <code>isaPreform</code> method.
     *
     * @param request           request context
     * @param response          response context
     * @param userSessionData   object wrapping the session
     * @param requestParser     parser to simple retrieve data from the request
     * @param configuration     configuration business object
     *
     * @return logical key for a forward to another action or jsp
     */
    protected abstract ActionForward marketingPerform(ActionMapping mapping,
                                               ActionForm form,
                                               HttpServletRequest request,
                                               HttpServletResponse response,
                                               UserSessionData userSessionData,
                                               RequestParser requestParser,
                                               MetaBusinessObjectManager mbom,
                                               MarketingConfiguration configuration,
                                               MarketingBusinessObjectsAware marketBom,
                                               IsaLocation log)
                    throws CommunicationException;
                    
    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward ecomPerform(ActionMapping mapping,
                                     ActionForm form,
                                     HttpServletRequest request,
                                     HttpServletResponse response,
                                     UserSessionData userSessionData,
                                     RequestParser requestParser,
                                     MetaBusinessObjectManager mbom,
                                     boolean multipleInvocation,
                                     boolean browserBack)
                        throws CommunicationException {
                            
        final String METHOD_NAME = "ecomPerform()";
        log.entering(METHOD_NAME);

        ActionForward forwardTo = marketingPerform(
                                      mapping,
                                      form,
                                      request,
                                      response,
                                      userSessionData,
                                      requestParser,
                                      mbom,
                                      getMarketingConfiguration(userSessionData),
                                      getMarketingBom(userSessionData),
                                      log);
                                      
        log.exiting();
        
        return forwardTo;
    }
    
    public MarketingBusinessObjectsAware getMarketingBom(UserSessionData userSessionData) {
       
        return (MarketingBusinessObjectsAware) userSessionData.getMBOM().getBOMByType(MarketingBusinessObjectsAware.class);
    }
    
    public MarketingConfiguration getMarketingConfiguration(UserSessionData userSessionData) {
        
        MarketingConfiguration marketingConfiguration = null;
        
        MarketingBusinessObjectsAware mkbom =  getMarketingBom(userSessionData);
        if (mkbom != null) {
            marketingConfiguration = mkbom.getMarketingConfiguration();
        }
        
        return marketingConfiguration;
    }
    
    /**
     * Convenience method to retrieve the business object manager for the
     * catalog from the session.
     *
     * @param userSessionData the user session data for this session
     * @return the bom capable of creating business objects for the catalog
     */
    protected CatalogBusinessObjectManager getCatalogBusinessObjectManager(UserSessionData userSessionData) {
        return (CatalogBusinessObjectManager) userSessionData.getBOM(CatalogBusinessObjectManager.CATALOG_BOM);
    }
    
    /**
     * Returns if the action runs in the B2C application.
     * 
     * @param application name of the application (taken from Scenario
     *         parameter in web.xml (see {@link com.sap.isa.core.ContextConst#IC_SCENARIO}).
     *
     * @return <code>true</code>, if the application is B2C, else <code>false</code>.
     */
    protected boolean checkLogin() {
        return getApplication().equals(BaseConfiguration.B2C);
    }
    
    public void setCheckLogin() {
        if (checkLogin()) {
            this.checkUserIsLoggedIn = false;
        } 
    }
    
    /**
     * Checks the precondition before the actions will be performed. <br>
     * Overwrite this method to extends or to reduce the checks. <br>
     * If an error occurrs the  <code>PanicException</code> will be thrown or a logical
     * forward will be returned to allow a proper error handling.
     * 
     * @param application name of the application (taken from Scenario
     *         parameter in web.xml (see {@link com.sap.isa.core.ContextConst#IC_SCENARIO}).
     * @param userSessionData user session data
     * @param bom current business object manager
     * @return name of the logical forward to handle the error situation or <code>null</code> 
     * if no error occured.
     * 
     * @throws PanicException
     * 
     * @see com.sap.isa.core.PanicException
     */
    protected String preConditionsCheck(String application,
                                        UserSessionData userSessionData,
                                        MarketingBusinessObjectsAware marketBom)
            throws PanicException {
            
        MarketingUser user = marketBom.getMarketingUser();  
                
        checkObjectExist(user); 

        boolean check = application.equals(BaseConfiguration.B2C) || application.equals(BaseConfiguration.B2B) ;              
                    
        if (check && checkUserIsLoggedIn) {
            return checkUserIsLoggedIn(user);
        }
        return null;
    }
    
    /**
     * Checks, if the given user is logged in. <br>
     * If not a <code>{@link Constants#USER_NO_LOGIN} </code> will be returned to 
     * found an appropriated forward.
     * 
     * @param user the user, which should be checked
     * @return {@link Constants#USER_NO_LOGIN} if the user is not logged in. 
     */
    protected String checkUserIsLoggedIn(MarketingUser user) {

        if (!user.isUserLogged()) {     
            return Constants.USER_NO_LOGIN;
        }
        
        return null;
    }
    
    /**
     * Generic method to check pre conditions. <br>
     * Use the method {@link #checkPreConditions(String, UserSessionData, 
     * BusinessObjectManager)} to overwrite or extend the default behaviour. 
     * 
     * @param userSessionData user session data
     * @param mbom meta business object manager
     * @return name of the logical forward to handle the error situation or <code>null</code> 
     * if no error occured.
     * 
     * @throws PanicException
     * 
     * @see com.sap.isa.core.PanicException
     */
    protected final String checkPreConditions(UserSessionData userSessionData,
                                              MetaBusinessObjectManager mbom) 
            throws PanicException {
        
        String application = getApplication();

        if (checkBom) {
            checkPreConditions(application, userSessionData, getMarketingBom(userSessionData));
            return preConditionsCheck(application, userSessionData, getMarketingBom(userSessionData));
        }
        
        return null;
    }
    
    /**
     * Checks the precondition before the actions will be performed. <br>
     * Overwrite this method to extends or to reduce the checks. <br>
     * If an error occurrs the  <code>PanicException</code> will thrown.
     * 
     * @param application name of the application (taken from Scenario
     *         parameter in web.xml (see {@link com.sap.isa.core.ContextConst#IC_SCENARIO}).
     * @param userSessionData user session data
     * @param bom current business object manager
     * @throws PanicException
     * 
     * @deprecated use {@link IsaCoreBaseAction#preConditionsCheck(String, UserSessionData, BusinessObjectManager)}
     * 
     * @see com.sap.isa.core.PanicException
     */
    protected void checkPreConditions(String application,
                                      UserSessionData userSessionData,
                                      MarketingBusinessObjectsAware marketBom)
            throws PanicException {
            
    }
  
}
