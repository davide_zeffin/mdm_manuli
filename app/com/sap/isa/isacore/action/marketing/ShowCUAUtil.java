/*****************************************************************************
    Class         ShowCUABaseAction
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Action to display the Bestseller
    Author:       Wolfgang Sattler
    Created:      May 2001
    Version:      1.0

    $Revision: #4 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.isacore.action.marketing;

import com.sap.isa.backend.boi.isacore.marketing.CUADisplayTypes;

/**
 * Base class for classes which display cua products
 *
 * At the moment we make no difference between upselling and downselling.
 *
 * @see com.sap.isa.isacore.action.marketing.ShowCUAAction
 * @see com.sap.isa.isacore.action.marketing.ShowCUAInCatalogAction
 *
 */
public abstract class ShowCUAUtil implements CUADisplayTypes, CUAActionConstants {


	/**
	 * Returns if a product of the given <code>productType</code> should be displayed
	 * on a cua list with the given <code>displayType</code>
	 *
	 * param productType: cuaType of product
	 * param displayType: describe with types should be displayed.
	 *
	 * return <code>true</code> if the product have the correct type
	 */
	public static boolean isProductVisible(String productType, String displayType) {

		boolean isVisible = false;
        
		if (displayType.length() == 0 ) {
			isVisible= true;
		}         
		else if (displayType.indexOf(productType) > -1){
			isVisible = true;        
		}

		return isVisible;

	 }



}
