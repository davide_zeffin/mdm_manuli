/*****************************************************************************
    Class:        CUAActionConstants
    Copyright (c) 2005, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      13.04.2005
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.isacore.action.marketing;

/**
 * The interface CUAActionConstants contains constants for actions taking care of
 * CUA and other related products . <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public interface CUAActionConstants {
	
	/**
	 * Name of the cua list stored in the request context.
	 */
	public static final String RC_CUALIST = "cuaList";

	public static final String RC_CUATABLE = "cuaTable";

	/**
	 * Name of the cua type stored in the request context.
	 */
	public static final String RC_CUATYPE = "cuaType";


	/**
	 * Name of the request parameter to the product fields:<br>
	 * type of cua product should be displayed
	 */
	public static final String PN_CUATYP   = "cuaproducttype";


}
