/*****************************************************************************
    Class         ShowCUAAction
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Action to display the Bestseller
    Author:       Wolfgang Sattler
    Created:      May 2001
    Version:      1.0

    $Revision: #4 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.isacore.action.marketing;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sap.isa.backend.boi.isacore.marketing.MarketingConfiguration;
import com.sap.isa.backend.boi.isacore.marketing.MarketingUser;
import com.sap.isa.backend.boi.webcatalog.CatalogBusinessObjectsAware;
import com.sap.isa.backend.boi.webcatalog.CatalogConfiguration;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.marketing.CUAList;
import com.sap.isa.businessobject.marketing.CUAManager;
import com.sap.isa.businessobject.webcatalog.CatalogBusinessObjectManager;
import com.sap.isa.catalog.actions.ActionConstants;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.ShowProductListHelper;

/**
 * Reading and display the CUA products for a given product. The product is given with
 * the request attribute <code>ActionConstants.RC_WEBCATITEM</code> (<i>currentItem</i>).<p>
 *
 * You can use the Parameter PN_CUATYP(<i>cuaproducttype</i>) to display only Accessories or Recommendation.
 * Possible values are the constants ALL, ACCESSORY CROSSSELLING, CROSSUPSELLING or UPSELLING.
 * With the method <code>shouldProductdisplayed</code> one can check on JSP, if a product should
 * display depending the type.<br>
 * <i>Note:</i> If the type is set to ACCESSORY the accessories will be read from the
 * catalog. <p>
 * There are two posibilities to display the cua products. You can use the marketing template with
 * short informations or a product list (equivalent to the products in categories).
 * Therefore the <code>ShowProductListHelper</code> is used. To use the short
 * information you must provide the parameter <i>inFrame</i> with the value <i>yes</i>.
 * <p>
 * You can give a logical forward within the request parameter <i>forward</i>. <br>
 * <i>Note:</i> Don't use this forward, if you want diplay cua products in a product list.
 * <h4>Overview over inport parameters and attributes</h4>
 * <table border="1" cellspacing="0" cellpadding="4">
 *   <tr>
 *     <td><b>name</b></td>
 *     <td><b>parameter</b></td>
 *     <td><b>attribute</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr>
 *      <td>cuaproducttype</td><td>X</td><td>&nbsp</td>
 *      <td>cua type which should be displayed</td>
 *   </tr>
 *   <tr>
 *      <td>currentItem</td><td>&nbsp</td><td>X</td>
 *      <td>webCatItem for which cua product should be displayed</td>
 *   </tr>
 *   <tr>
 *      <td>inFrame</td><td>X</td><td>&nbsp</td>
 *      <td>cua products should be displayed in the product detail screen</td>
 *   </tr>
 *   <tr>
 *      <td>forward</td><td>&nbsp</td><td>X</td>
 *      <td>logical forward, which should be used if the action is successful</td>
 *   </tr>
 *
 * </table>
 * <h4>The following forwards are used by this action</h4>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>showcua</td><td>forward used if the cua products are displayed in product detail
 *                           or if no products could be found</td></tr>
 *   <tr><td>showcualist</td><td>forward used if the cua products should be displayed in product list</td></tr>
 *   <tr><td>error</td><td>forward used if the request attribute <code>ActionConstants.RC_WEBCATITEM</code>
                           is missing</td></tr>
 * </table>
 * <h4>Overview of attributes set in the request context</h4>
 * <ul>
 * <li><code>RC_CUALIST</code></li>
 * <li><code>RC_CUATYPE</code></li>
 * </ul>
 *
 * @see com.sap.isa.isacore.action.ShowProductListHelper
 */
public class ShowCUAInCatalogAction extends ShowCUABaseAction {

	/**
	 * Template method to be overwritten by the subclasses of this class.
	 * Normally you should overwrite this method and you do not touch the
	 * <code>isaPreform</code> method.
	 *
	 * @param request           request context
	 * @param response          response context
	 * @param userSessionData   object wrapping the session
	 * @param requestParser     parser to simple retrieve data from the request
	 * @param bom               reference to the BusinessObjectManager
	 * @param cbom              reference to the CatalogBusinessObjectManager
	 * @param catalog           reference to the catalog
	 * @param log               reference to the IsaLocation, needed for logging
	 * @param cUAManager        cUAManager business object
	 * @param user              user business object
	 * @param configuration              configuration business object
	 * @param forward           logical forward which is given to the action
	 *
	 * @return logical key for a forward to another action or jsp
	 */
	protected String cuaPerform(
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser parser,
		CatalogBusinessObjectManager cbom,
		WebCatInfo catalog,
		IsaLocation log,
		CUAManager cUAManager,
		MarketingUser user,
		MarketingConfiguration configuration,
		String cUAType,
		String forward)
		throws CommunicationException {
		final String METHOD_NAME = "cuaPerform()";
		log.entering(METHOD_NAME);
		CUAList cUAList = null;

		String inFrame = parser.getParameter("inFrame").getValue().getString();
		//default if necessary
		if("yes".equals(inFrame) && (cUAType==null || "".equals(cUAType))){
			cUAType=ACCESSORY;
		}
        
        String detailScenario = parser.getParameter(ActionConstants.RA_DETAILSCENARIO).getValue().getString();
        if (detailScenario == null || detailScenario.length() == 0) {
        	detailScenario = parser.getAttribute(ActionConstants.RA_DETAILSCENARIO).getValue().getString();
        }
        if (detailScenario != null) {
            request.setAttribute(ActionConstants.RA_DETAILSCENARIO, detailScenario);
        }
        String oldDetailScenario = parser.getParameter(ActionConstants.RA_OLDDETAILSCENARIO).getValue().getString();
        if (oldDetailScenario != null) {
            request.setAttribute(ActionConstants.RA_OLDDETAILSCENARIO, oldDetailScenario);
        }
        String cuaRelatedProduct = parser.getParameter(ActionConstants.RA_CUA_RELATED_PRODUCT).getValue().getString();
        if (cuaRelatedProduct != null) {
            request.setAttribute(ActionConstants.RA_CUA_RELATED_PRODUCT, cuaRelatedProduct);
        }

		if (cUAType.equals("AL")) {
			cUAType = ACCESSORY;
			forward = "showcualist";
		} else {
			if (cUAType.equals("CL")) {
				cUAType = CROSSSELLING;
				forward = "showcualist";
			} else {
				if (cUAType != null
				    && !forward.equals("CompareUpSelling")
					&& (cUAType.equals(ShowCUABaseAction.CROSSSELLING)
					|| cUAType.equals(ShowCUABaseAction.ALTERNATIVES)
					|| cUAType.equals(ShowCUABaseAction.ACCESSORY)
					|| cUAType.equals(ShowCUABaseAction.REMANNEW))) {
					forward = "showcua";
					//sets the productdetaillist type from the catalog.
					if (cUAType.equals(ACCESSORY)) {
						catalog.setProductDetailListType(
							WebCatInfo.PROD_DET_LIST_ACCESSORIES);
					} else {
						if (cUAType.equals(ALTERNATIVES)) {
							catalog.setProductDetailListType(
								WebCatInfo.PROD_DET_LIST_ALTERNATIVES);
						} else {
							if (cUAType.equals(REMANNEW)) {
								catalog.setProductDetailListType(
									WebCatInfo.PROD_DET_LIST_EXCH_PROD);
							} else {
								if (cUAType.equals(CROSSSELLING)) {
									catalog.setProductDetailListType(
										WebCatInfo.PROD_DET_LIST_REL_PROD);
								}
							}
						}
					}
				}
			}
		}

		if (cUAType != null
			&& cUAType.equals("")
			&& catalog.getCuaType() != null
			&& !catalog.getCuaType().equals("")) {
			cUAType = catalog.getCuaType();
		}

		if (request.getAttribute(MarketingActionConstants.RC_WEBCATITEM)
			!= null) {

			WebCatItem webCatItem =
				(WebCatItem) request.getAttribute(
					MarketingActionConstants.RC_WEBCATITEM);

			// create the cuaList object for webCatItem
			if (cUAType.equals(ACCESSORY)) {
				cUAList = cUAManager.readAccessoryList(webCatItem);
			} else {
				cUAList =
					cUAManager.readCUAList(
						webCatItem,
						user,
						configuration,
						catalog,
						getCUATypes(cUAType));
			}
			// set the product id in the cuaList
			cUAList.setProductId(webCatItem.getProduct());
			if (!inFrame.equals("yes"))
				catalog.setCurrentCUAList(cUAList, cUAType);
		} else {
			log.debug("No product key for CUA Manager");
			log.exiting();
			return "error";
		}

		setRequestAttributes(request, cUAList, cUAType);

		String forwardTo = null;
		if (forward.length() > 0) {
			forwardTo = forward;
		}

		// if the cuaList is diplayed in product detail of the catalog, forward to showcua
		if (inFrame.equals("yes")) {
			forwardTo = "showcua";
		}
		
		if(forwardTo.equals("showcua")){
			CatalogBusinessObjectsAware cataConfBom = (CatalogBusinessObjectsAware) userSessionData.getMBOM().getBOMByType(CatalogBusinessObjectsAware.class);
			if (cataConfBom != null) {
				CatalogConfiguration catConfig = cataConfBom.getCatalogConfiguration();
				if(!catConfig.catUseReloadOneTime() ){
					//we should set the layout
					forwardTo = "showcualayout";
				}
			}
		}
		
		if(forwardTo.equals("CompareUpSelling")){
			request.setAttribute("compareCUA","true");
		}

		if (forwardTo != null && !forwardTo.equals("showcualist")) {
			log.exiting();
			return forwardTo;
		}

		forwardTo = "showcualist";
		CatalogBusinessObjectsAware cataConfBom = (CatalogBusinessObjectsAware) userSessionData.getMBOM().getBOMByType(CatalogBusinessObjectsAware.class);
		if (cataConfBom != null) {
			CatalogConfiguration catConfig = cataConfBom.getCatalogConfiguration();
			if(!catConfig.catUseReloadOneTime() ){
				forwardTo = "showcualistLayout";
			}
		}

		if (ShowProductListHelper
			.prepareProductList(
				catalog,
				request,
				cUAList,
				MarketingActionConstants.DS_CUA)) {
			log.exiting();
			return forwardTo;
		}
		log.exiting();
		return "showcualistempty";

	}

}
