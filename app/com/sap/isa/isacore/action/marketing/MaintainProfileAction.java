/*****************************************************************************
    Class         MaintainProfileAction
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Action for the Profile Maintenance
    Author:       SAP AG
    Created:      27 Februar 2001
    Version:      1.0

    $Revision: #2 $
    $Date: 2001/06/27 $

*****************************************************************************/
package com.sap.isa.isacore.action.marketing;

import com.sap.isa.backend.boi.isacore.marketing.MarketingConfiguration;
import com.sap.isa.backend.boi.isacore.marketing.MarketingUser;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.marketing.MktProfile;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.util.RequestParser;

/**
 * Maintain the profile of an User, after he had answered the questionary. <br>
 * see {@link com.sap.isa.isacore.action.marketing.MaintainProfileBaseAction}
 * for details.
 *
 * @author SAP AG
 * @version 1.0
 * @see com.sap.isa.isacore.action.marketing.MaintainProfileBaseAction
 *
 */
public class MaintainProfileAction extends MaintainProfileBaseAction {

    /**
     * This constant is used to indicate that at least one time during the current
     * session a successfull saving of the customer's profile occured.
     */
    public static final String PROFILE_SAVED_DURING_CURRENT_SESSION = "ProfileSavedDuringCurrentSession";
	

	/**
	 * Initialize the action.
	 * In B2C this action could be used by anonymous user.
	 * 
	 * @see com.sap.isa.isacore.action.IsaCoreBaseAction#initialize()
	 */
	public void initialize() {
        setCheckLogin();
	}

    
	/**
	 * Store the profile. <br>
	 * Check if the request parameter SaveMktProfile to decide if to store the
	 * profile.
	 * 
	 * @param requestParser parser
	 * @param forwardTo logical forward
	 * @param user current user object
	 * @param configuration configuration object
	 * @param profile profile object
	 * @return the logical forward which should be used; 
	 * @throws CommunicationException
	 */
	protected String setProfile(UserSessionData userSessionData,
								 RequestParser requestParser,
							     String forwardTo,
							     MarketingUser user,
							     MarketingConfiguration configuration,
							     MktProfile profile)         					   
			throws CommunicationException {
        
		if (requestParser.getParameter("SaveMktProfile").isSet() && 
		     requestParser.getParameter("SaveMktProfile").getValue().getString().length() > 0) {
			// user has pressed the save button
			user.saveMktProfile(profile, configuration);
			forwardTo = "success";
			// determine what should be displayed next
			if (requestParser.getParameter("recommendation").isSet()) {
				forwardTo = "recommendation";
			}
			if (profile.isValid()) {
				// the profile was successfully saved
				userSessionData.setAttribute(PROFILE_SAVED_DURING_CURRENT_SESSION, "true");
            
				// if the user is unknown. The Partner guid is filled in the backend
				// with a technical key for the unknown user.
				// This info should be stored in a cookie now. 
				if (!user.getMktPartner().isKnown()) {
					forwardTo = "unknownuser";    
				}    
			}
			else {
				forwardTo = "failure";
			}
			
		}
		else {
			user.setMktProfile(profile);
		}
        
		return forwardTo;
	}

	/**
	 * Exit to adjust user if the maintenance is canceled. <br>
	 * 
	 * @param userSessionData
	 * @param requestParser
	 * @param user
	 * @param forwardTo
	 * @return
	 */
	protected String cancelMaintenance(UserSessionData userSessionData,
									   RequestParser requestParser, 
									   MarketingUser user,
									   String forwardTo) {
		// user has pressed the cancel button
		user.setMktProfile(null);
        
		if (requestParser.getParameter("recommendation").isSet()) {
			forwardTo = "recommendation";
		}

		return forwardTo;
	}
    

}

