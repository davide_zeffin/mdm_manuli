/*****************************************************************************
    Class         MaintainNewsletterProfileAction
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Action for the Profile Maintenance
    Author:       SAP AG
    Created:      27 Februar 2001
    Version:      1.0

    $Revision: #2 $
    $Date: 2001/06/27 $

*****************************************************************************/
package com.sap.isa.isacore.action.marketing;

import com.sap.isa.backend.boi.isacore.marketing.MarketingConfiguration;
import com.sap.isa.backend.boi.isacore.marketing.MarketingUser;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.marketing.MktProfile;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.util.RequestParser;

/**
 * Maintain the profile of an MarketingUser, after he had answered the questionary. <br>
 * see {@link com.sap.isa.isacore.action.marketing.MaintainProfileBaseAction}
 * for details.
 *
 * @author SAP AG
 * @version 1.0
 * @see com.sap.isa.isacore.action.marketing.MaintainProfileBaseAction
 *
 */
public class MaintainNewsletterProfileAction extends MaintainProfileBaseAction {


	/**
	 * Store the profile. <br>
	 * Check if the request parameter SaveMktProfile to decide if to store the
	 * profile.
	 * 
	 * @param requestParser parser
	 * @param forwardTo logical forward
	 * @param user current user object
	 * @param configuration configuration object
	 * @param profile profile object
	 * @return the logical forward which should be used; 
	 * @throws CommunicationException
	 */
	protected String setProfile(UserSessionData userSessionData,
								 RequestParser requestParser,
							     String forwardTo,
							     MarketingUser user,
							     MarketingConfiguration configuration,
							     MktProfile profile)         					   
			throws CommunicationException {
        
		if (requestParser.getParameter("SaveMktProfile").isSet() && 
			 requestParser.getParameter("SaveMktProfile").getValue().getString().length() > 0) {
			// user has pressed the save button
			user.saveProfile(profile, configuration);
			forwardTo = "success";
		}

		user.setNewsletterProfile(profile);
        
		return forwardTo;
	}

	/**
	 * Exit to adjust user if the maintenance is canceled. <br>
	 * 
	 * @param userSessionData
	 * @param requestParser
	 * @param user
	 * @param forwardTo
	 * @return
	 */
	protected String cancelMaintenance(UserSessionData userSessionData,
										RequestParser requestParser, 
										MarketingUser user,
										String forwardTo) {

		user.setNewsletterProfile(null);
        
		return forwardTo;
	}
    

}

