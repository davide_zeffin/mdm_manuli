/*****************************************************************************
    Class         ShowBestsellerAction
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Action to display the Bestseller
    Author:       Wolfgang Sattler
    Created:      May 2001
    Version:      1.0

    $Revision: #5 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.isacore.action.marketing;

import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.backend.boi.isacore.marketing.MarketingBusinessObjectsAware;
import com.sap.isa.backend.boi.isacore.marketing.MarketingConfiguration;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Product;
import com.sap.isa.businessobject.marketing.Bestseller;
import com.sap.isa.businessobject.webcatalog.CatalogBusinessObjectManager;
import com.sap.isa.catalog.actions.ActionConstants;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.ShowProductListHelper;


/**
 * Reading and display the actual bestsellers.
 *
 * There are two posibilities to display the bestsellers. You can use the marketing template with
 * short informations or a product list (equivalent to the products in categories).
 * Therefore the <code>ShowProductListHelper</code> is used.
 * <p>
 * <h4>Overview over inport parameters and attributes</h4>
 * No import parameter and attributes are used.
 *
 * <h4>The following forwards are used by this action</h4>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>showbestseller</td><td>forward used if no products could be found</td></tr>
 *   <tr><td>showbestsellerlist</td><td>standard forward when the bestsellers contain at least one product</td></tr>
 * </table>
 * <h4>Overview over of attributes set in the request context</h4>
 * <ul>
 * <li><code>{@link com.sap.isa.catalog.actions.ActionConstants#DS_BESTSELLER}</code></li>
 * </ul>
 *
 * @see com.sap.isa.isacore.action.ShowProductListHelper
 */

public class ShowBestsellerAction extends MarketingBaseAction {

    /**
     * Name of the bestseller stored in the request context.
     * @deprecated Please use {@link com.sap.isa.catalog.actions.ActionConstants#DS_BESTSELLER} instead!
     */
    public static final String RC_BESTSELLER = "bestseller";

	/**
	 * Initialize the action.
	 * In B2C this action could be used by anonymous user.
	 * 
	 * @see com.sap.isa.isacore.action.IsaCoreBaseAction#initialize()
	 */
	public void initialize() {
        setCheckLogin();
	}


    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward marketingPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            MetaBusinessObjectManager mbom,
            MarketingConfiguration configuration,
            MarketingBusinessObjectsAware marketBom,
            IsaLocation log)
            throws CommunicationException {

		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        CatalogBusinessObjectManager cbom =
                getCatalogBusinessObjectManager(userSessionData);

        if (configuration == null) {
        	log.exiting();
            throw new PanicException("configuration.notFound");
        }

        WebCatInfo catalog = cbom.getCatalog();

        if (catalog == null) {
        	log.exiting();
            throw new PanicException("catalog.notFound");
        }
        
		String lastVisited = (String)request.getAttribute("lastVisited");


        // create the Bestseller object
        Bestseller bestseller = marketBom.createBestseller(configuration,
                (String)userSessionData.getAttribute(SessionConst.XCM_CONF_KEY));

        if (bestseller.isValid()) {
        //check if there are products with configuration. If yes, the configuration should be resetted.
        
		    boolean shouldBeRepriced = false; 
		    Product prod;
		    
		    for (Iterator prodIter = bestseller.iterator(); prodIter.hasNext();) {
		    	prod = (Product) prodIter.next();
                    
				// reset configuration
				if (prod.isConfigurable() && prod.getCatalogItem() != null && 
					prod.getCatalogItem().getConfigItemReference() != null &&
				    (lastVisited != null && lastVisited.equals(WebCatInfo.AREADETAILS) || lastVisited != null && lastVisited.equals(WebCatInfo.ITEMDETAILS) ) ) {
					   prod.getCatalogItem().setConfigItemReference(null);
					   shouldBeRepriced = true;
				}
			}
        
        
            if (!bestseller.isEnhanced() || shouldBeRepriced) {
                // enhance and check the bestseller productlist against the catalog
                bestseller.enhance(catalog, cbom.getPriceCalculator(), false);
            }

        }

        request.setAttribute(ActionConstants.DS_BESTSELLER, bestseller);
        
        request.setAttribute(ActionConstants.LAST_VISITED,ActionConstants.DS_BESTSELLER);
        catalog.setLastVisited(ActionConstants.DS_BESTSELLER);
        
        // navigation on catalog pages
        String forwardTo = "showbestseller";


        if (ShowProductListHelper.prepareProductList(catalog,
                                                     request,
                                                     bestseller,
                                                     MarketingActionConstants.DS_BESTSELLER)) {
            forwardTo = "showbestsellerlist";
        }
		log.exiting();
        return mapping.findForward(forwardTo);
    }

}
