/*****************************************************************************
    Class         ShowCatalogEntryAction
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Action to display the Bestseller
    Author:       Wolfgang Sattler
    Created:      May 2001
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.isacore.action.marketing;

import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.backend.boi.isacore.marketing.MarketingBusinessObjectsAware;
import com.sap.isa.backend.boi.isacore.marketing.MarketingConfiguration;
import com.sap.isa.backend.boi.isacore.marketing.MarketingUser;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Product;
import com.sap.isa.businessobject.marketing.Bestseller;
import com.sap.isa.businessobject.marketing.PersonalizedRecommendation;
import com.sap.isa.businessobject.webcatalog.CatalogBusinessObjectManager;
import com.sap.isa.catalog.actions.ActionConstants;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.catalog.webcatalog.WebCatItemList;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;

/**
 * Reading and display the Personalized product recommendation for an user
 *
 * <br> Note that all Action classes have to provide a non argument constructor
 * because they were instantiated automatically by the action servlet.
 *
 */
/**
 * Reading bestseller and personalized product recommendations for an users
 * to display them in the catalog entry page.
 *
 * <h4>Overview over inport parameters and attributes</h4>
 * No import parameters and attributes are used.
 *
 * <h4>The following forwards are used by this action</h4>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>showcatalogentry</td><td>standard</td></tr>
 * </table>
 * <h4>Overview of attributes set in the request context</h4>
 * <ul>
 * <li><code>ShowRecommendationAction.RC_RECOMMENDATION</code></li>
 * <li><code>ShowBestsellerAction.RC_BESTSELLER</code></li>
 * <li><code>ActionConstants.RA_DISPLAYSCENARIO</code></li>
 * </ul>
 *
 * @see com.sap.isa.isacore.action.marketing.ShowBestsellerAction
 * @see com.sap.isa.isacore.action.marketing.ShowRecommendationAction
 * @see com.sap.isa.isacore.action.ActionConstants
 *
 */

public class ShowCatalogEntryAction extends MarketingBaseAction {

	/**
	 * Initialize the action.
	 * In B2C this action could be used by anonymous user.
	 * 
	 * @see com.sap.isa.isacore.action.IsaCoreBaseAction#initialize()
	 */
	public void initialize() {
        setCheckLogin();
	}

    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward marketingPerform(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response,
                                          UserSessionData userSessionData,
                                          RequestParser requestParser,
                                          MetaBusinessObjectManager mbom,
                                          MarketingConfiguration configuration,
                                          MarketingBusinessObjectsAware marketBom,
                                          IsaLocation log)
            throws CommunicationException {

		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        CatalogBusinessObjectManager cbom =
        getCatalogBusinessObjectManager(userSessionData);

        if (configuration == null) {
        	log.exiting();
            throw new PanicException("configuration.notFound");
        }

        WebCatInfo catalog = cbom.getCatalog();

        if (catalog == null) {
        	log.exiting();
            throw new PanicException("catalog.notFound");
        }
        
		String lastVisited = (String)request.getAttribute("lastVisited");
	

        MarketingUser user = marketBom.getMarketingUser();
        if (user == null) {
        	log.exiting();
            return mapping.findForward("error");
        }

        WebCatItemList currentItemList = new WebCatItemList(catalog);
        Iterator iter;

        if (configuration.isPersonalRecommendationAvailable() && !user.getMktPartner().getTechKey().isInitial()
            && user.getMktPartner().isKnown()) {
            // read the recommendation from the user
            PersonalizedRecommendation recommendation = null;

            //if(user.isUserLogged()) {
                recommendation = user.readPersonalizedRecommendation(configuration);
            //}
            
//if the user has entered a campaign code, the CUA item prices should be repriced. 
//the function catalog.getCUACampaignId returns the campaignId from the last CUA prices.
		    boolean shouldBeRepriced = false;
		    if ( catalog.getCampaignId() != null ) {
		    	 shouldBeRepriced = !catalog.getCampaignId().equals(catalog.getRecommendationCampaignId());
		    }
		                               
			if (shouldBeRepriced) {
				catalog.setRecommendationCampaignId(catalog.getCampaignId());
			}
				
            if (recommendation.isValid()) {
            	
				Product prod;
				
                
				for (Iterator prodIter = recommendation.iterator(); prodIter.hasNext();) {
				   prod = (Product) prodIter.next();
                    
				   // reset configuration
				   if (prod.isConfigurable() && prod.getCatalogItem() != null && 
					   prod.getCatalogItem().getConfigItemReference() != null && 
                    (lastVisited != null && (lastVisited.equals(WebCatInfo.AREADETAILS) || lastVisited.equals(WebCatInfo.ITEMDETAILS)))
                   ) {	  prod.getCatalogItem().setConfigItemReference(null);
						  shouldBeRepriced = true;
				   }
				}
                if(!recommendation.isEnhanced() || shouldBeRepriced) {
                    // enhance and check the productlist against the catalog
                    recommendation.enhance(catalog, cbom.getPriceCalculator(), false);
                }
            }

            request.setAttribute(ActionConstants.DS_RECOMMENDATION, recommendation);

            if (recommendation != null && recommendation.size() > 0) {
                currentItemList.setListType(ActionConstants.CV_LISTTYPE_RECOMMENDATIONS);
                iter=recommendation.iterator();
                while(iter.hasNext()) {
                    WebCatItem theItem = ((Product)iter.next()).getCatalogItem();
                    currentItemList.addItem(theItem);
                }
            }
        }

        if (configuration.isBestsellerAvailable()) {
            // create the Bestseller object
            Bestseller bestseller = marketBom.createBestseller(configuration,(String)userSessionData.getAttribute(SessionConst.XCM_CONF_KEY));
//			if the user has entered a campaign code, the CUA item prices should be repriced. 
//			the function catalog.getCUACampaignId returns the campaignId from the last CUA prices.
		    boolean shouldBeRepriced = false;
		    if( catalog.getCampaignId() != null ) {
		    	shouldBeRepriced = !catalog.getCampaignId().equals(catalog.getBestsellerCampaignId());
		    }
		                               
			if (shouldBeRepriced) {
			   catalog.setBestsellerCampaignId(catalog.getCampaignId());
			}
					  
            if(bestseller.isValid()) {
                
                Product prod;
                
                for (Iterator prodIter = bestseller.iterator(); prodIter.hasNext();) {
                    prod = (Product) prodIter.next();
                    
                    // reset configuration
                    if (prod.isConfigurable() && prod.getCatalogItem() != null && 
                        prod.getCatalogItem().getConfigItemReference() != null &&
                        (lastVisited != null && (lastVisited.equals(WebCatInfo.AREADETAILS) || lastVisited.equals(WebCatInfo.ITEMDETAILS)))
                        ) {                           
                           prod.getCatalogItem().setConfigItemReference(null);
                           shouldBeRepriced = true;
                    }
                }
                
                if(!bestseller.isEnhanced() || shouldBeRepriced) {
                    // enhance and check the bestseller productlist against the catalog
                    bestseller.enhance(catalog, cbom.getPriceCalculator(), false);
                }
            }

            request.setAttribute(ActionConstants.DS_BESTSELLER, bestseller);

            if (bestseller != null && bestseller.size() != 0) {
                currentItemList.setListType(ActionConstants.CV_LISTTYPE_BESTSELLER);
                iter=bestseller.iterator();
                while(iter.hasNext()) {
                    WebCatItem theItem = ((Product)iter.next()).getCatalogItem();
                    currentItemList.addItem(theItem);
                }
            }
        }

        catalog.setCurrentItemList(currentItemList);

        request.setAttribute(ActionConstants.RA_DETAILSCENARIO, ActionConstants.DS_ENTRY);

        // needed for B2C
        catalog.setLastVisited(MarketingActionConstants.DS_ENTRY);
        catalog.setLastUsedDetailScenario(ActionConstants.DS_ENTRY);

		log.exiting();
        return mapping.findForward("showcatalogentry");

    }

}
