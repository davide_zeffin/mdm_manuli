/*****************************************************************************
    Class         ShowNewsletterProfileAction
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Action which display the profile maintenance screen
    Author:       SAP AG
    Created:      20 Februar 2001
    Version:      1.0

    $Revision: #2 $
    $Date: 2001/06/27 $

*****************************************************************************/

package com.sap.isa.isacore.action.marketing;

import com.sap.isa.backend.boi.isacore.marketing.MarketingConfiguration;
import com.sap.isa.backend.boi.isacore.marketing.MarketingUser;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.marketing.MktAttributeSet;
import com.sap.isa.businessobject.marketing.MktProfile;


/**
 * Display the questionary to maintain the profile of an MarketingUser.
 * see {@link com.sap.isa.isacore.action.marketing.ShowProfileBaseAction}
 * for details.
 *
 * @author SAP AG
 * @version 1.0
 * @see com.sap.isa.isacore.action.marketing.ShowProfileBaseAction
 *
 */
public class ShowNewsletterProfileAction extends ShowProfileBaseAction {

	/** 
	 * usage which is set in the attribute set. <br> 
	 */
	public static final String ATTRIBUTE_SET_USAGE = "newsletter";
	
    /**
	 * Return the profile found for the given attribute set. <br>
	 * 
	 * @param user current user
	 * @param configuration configuration object
	 * @param attributeSet given attribute set.
	 * @return
	 * @throws CommunicationException
	 */
	protected MktProfile getProfile(MarketingUser user,
									MarketingConfiguration configuration,
									MktAttributeSet attributeSet)
			throws CommunicationException {
        		
		MktProfile profile = user.getNewsletterProfile();
        
		if (profile == null) {
			profile = user.readProfile(attributeSet.getTechKey(), configuration);
		}
		return profile;
	}


	
	/**
	 * Read the attribute set. <br>
	 * 
	 * @param configuration configuration object 
	 * @param attributeSet attribute set to read
	 * @throws CommunicationException
	 */
	protected void readAttributeSet(MarketingConfiguration configuration, 
									  MktAttributeSet attributeSet)
			throws CommunicationException {
				
		attributeSet.read(configuration.getNewsletterAttributeSetId(), configuration);
		attributeSet.setUsage(ATTRIBUTE_SET_USAGE);
        
	}

    

}
