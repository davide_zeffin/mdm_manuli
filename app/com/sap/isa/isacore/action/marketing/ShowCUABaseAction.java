/*****************************************************************************
    Class         ShowCUABaseAction
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Action to related products like CUA.
    Author:       SAP AG
    Created:      May 2001
    Version:      1.0

    $Revision: #4 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.isacore.action.marketing;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.backend.boi.isacore.marketing.CUADisplayTypes;
import com.sap.isa.backend.boi.isacore.marketing.MarketingBusinessObjectsAware;
import com.sap.isa.backend.boi.isacore.marketing.MarketingConfiguration;
import com.sap.isa.backend.boi.isacore.marketing.MarketingUser;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.marketing.CUAList;
import com.sap.isa.businessobject.marketing.CUAManager;
import com.sap.isa.businessobject.webcatalog.CatalogBusinessObjectManager;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;

/**
 * Base class for classes which display cua products. <br>
 *
 * At the moment we make no difference between upselling and downselling. <br>
 * <strong>For extensions: Please ensure that the constants refers to the 
 * corresponding constants of the {@link CUAProductData} interface. </strong> 
 *
 * @see com.sap.isa.isacore.action.marketing.ShowCUAAction
 * @see com.sap.isa.isacore.action.marketing.ShowCUAInCatalogAction
 *
 */
public abstract class ShowCUABaseAction extends MarketingBaseAction
		implements CUADisplayTypes, CUAActionConstants {

    /**
     * Name of the cua list stored in the request context.
     */
    public static final String RC_CUALIST = "cuaList";

    public static final String RC_CUATABLE = "cuaTable";

    /**
     * Name of the cua type stored in the request context.
     */
    public static final String RC_CUATYPE = "cuaType";


    /**
     * Name of the request parameter to the product fields:<br>
     * type of cua product should be displayed
     */
    public static final String PN_CUATYP   = "cuaproducttype";


	/**
	 * Initialize the action.
	 * In B2C this action could be used by anonymous user.
	 * 
	 * @see com.sap.isa.isacore.action.IsaCoreBaseAction#initialize()
	 */
	public void initialize() {
        setCheckLogin();
	}


    /**
     * Returns if a product of the given <code>productType</code> should be displayed
     * on a cua list with the given <code>displayType</code>
     *
     * param productType: cuaType of product
     * param displayType: describe with types should be displayed.
     *
     * return <code>true</code> if the product have the correct type
     */
  	public static boolean isProductVisible(String productType, String displayType) {

		return ShowCUAUtil.isProductVisible(productType, displayType);
     }


    /**
     * Give all cuatypes (business object) which should be displayed with
     * the cUAType used in the action (see contants). <br>
     * 
     * <strong>Since 5.0 the method works generic and interpret each char 
     * in the given <code>cUAType</code> as a type. For an empty string
     * <code>null</code> will returned<strong>
     *
     * @param cUAType the type of cua products should be displayed
     *
     * @return a string array with all used cua types or <code>null</code>
     */
    public String[] getCUATypes(String cUAType) {
		
		List types = new ArrayList();
		
        String[] retArray = null;

        if (cUAType.length() != 0 ) {

			for (int i = 0; i < cUAType.length(); i++) {
				types.add("" + cUAType.charAt(i));            	    
            } 
			
			retArray = (String[])types.toArray( new String[] { "dummy" });			
        }

        return retArray;
    }


     /**
      * Template method to be overwritten by the subclasses of this class.
      * Normally you should overwrite this method and you do not touch the
      * <code>isaPreform</code> method.
      *
      * @param request           request context
      * @param response          response context
      * @param userSessionData   object wrapping the session
      * @param requestParser     parser to simple retrieve data from the request
      * @param bom               reference to the BusinessObjectManager
      * @param cbom              reference to the CatalogBusinessObjectManager
      * @param catalog           reference to the Catalog
      * @param log               reference to the IsaLocation, needed for logging
      * @param cUAManager        cUAManager business object
      * @param user              user business object
      * @param configuration     configuration business object
      * @param cUAType           type of the cua Product
      * @param forward           logical forward which is given to the action
      *
      * @return logical key for a forward to another action or jsp
      */
     protected abstract String cuaPerform(HttpServletRequest request,
                 HttpServletResponse response,
                 UserSessionData userSessionData,
                 RequestParser parser,
                 CatalogBusinessObjectManager cbom,
                 WebCatInfo catalog,
                 IsaLocation log,
                 CUAManager cUAManager,
                 MarketingUser user,
                 MarketingConfiguration configuration,
                 String cUAType,
                 String forward)
                     throws CommunicationException;


    /**
     *  set the cuaList and type in the request context
     *
     * @param request to set the data
     * @param cUAList reference to an cuaList
     * @param cUAType type of the list
     *
     */
    public void setRequestAttributes(HttpServletRequest request, CUAList cUAList, String cUAType) {

        request.setAttribute(RC_CUALIST, cUAList);
        request.setAttribute(RC_CUATYPE, cUAType);

    }


    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward marketingPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            MetaBusinessObjectManager mbom,
            MarketingConfiguration configuration,
            MarketingBusinessObjectsAware marketBom,
            IsaLocation log)
            throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);

        if (configuration == null) {
        	log.exiting();
            throw new PanicException("configuration.notFound");
        }

        CatalogBusinessObjectManager cbom =
                getCatalogBusinessObjectManager(userSessionData);

        WebCatInfo catalog = cbom.getCatalog();

        if (catalog == null) {
        	log.exiting();
            throw new PanicException("catalog.notFound");
        }

        MarketingUser user = marketBom.getMarketingUser();

        String forward = requestParser.getParameter("forward").getValue().getString();

        String cUAType = requestParser.getParameter(PN_CUATYP).getValue().getString();

        CUAManager cUAManager = marketBom.createCUAManager();

        String forwardTo = cuaPerform(request,
                                      response,
                                      userSessionData,
                                      requestParser,
                                      cbom,
                                      catalog,
                                      log,
                                      cUAManager,
                                      user,
                                      configuration,
                                      cUAType,
                                      forward);
		log.exiting();
        return mapping.findForward(forwardTo);
    }

}
