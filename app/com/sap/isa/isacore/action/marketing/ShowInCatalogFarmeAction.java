/*****************************************************************************
    Class         ShowInCatalogFarmeAction
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Action to set the "type" for the right catalog refresher
    Author:       Wolfgang Sattler
    Created:      May 2001
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.isacore.action.marketing;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.backend.boi.isacore.marketing.MarketingBusinessObjectsAware;
import com.sap.isa.backend.boi.isacore.marketing.MarketingConfiguration;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;

/**
 * Action to set the right "type" for the catalog refresher to display marketing
 * objects.
 *
 * <h4>Overview over inport parameters and attributes</h4>
 * <table border="1" cellspacing="0" cellpadding="4">
 *   <tr>
 *     <td><b>name</b></td>
 *     <td><b>parameter</b></td>
 *     <td><b>attribute</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr>
 *      <td>type</td><td>X</td><td>&nbsp</td>
 *      <td>type which should be displayed</td>
 *   </tr>
 *
 * </table>
 * <h4>The following forwards are used by this action</h4>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>success</td><td>standard</td></tr>
 * </table>
 * <h4>Overview of attributes set in the request context</h4>
 * <ul>
 * <li>type</li>
 * </ul>
 *
 */

public class ShowInCatalogFarmeAction extends MarketingBaseAction {

	/**
	 * Initialize the action.
	 * In B2C this action could be used by anonymous user.
	 * 
	 * @see com.sap.isa.isacore.action.IsaCoreBaseAction#initialize()
	 */
	public void initialize() {
        setCheckLogin();
	}


    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward marketingPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            MetaBusinessObjectManager mbom,
            MarketingConfiguration configuration,
            MarketingBusinessObjectsAware marketBom,
            IsaLocation log)
            throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        WebCatInfo catalog =
                getCatalogBusinessObjectManager(userSessionData).getCatalog();

        if (catalog == null) {
        	log.exiting();
            throw new PanicException("catalog.notFound");
        }

        String type = requestParser.getParameter("type").getValue().getString();
        request.setAttribute("type",type);

        // navigation on catalog pages
        catalog.setLastVisited(type);
		log.exiting();
        return mapping.findForward("success");
    }

}
