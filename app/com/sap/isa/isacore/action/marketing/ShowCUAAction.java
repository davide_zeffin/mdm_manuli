/*****************************************************************************
    Class         ShowCUAAction
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Action to display the Bestseller
    Author:       Wolfgang Sattler
    Created:      May 2001
    Version:      1.0

    $Revision: #4 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.isacore.action.marketing;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sap.isa.businessobject.AppBaseBusinessObjectManager;
import com.sap.isa.backend.boi.isacore.marketing.MarketingConfiguration;
import com.sap.isa.backend.boi.isacore.marketing.MarketingUser;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.marketing.CUAList;
import com.sap.isa.businessobject.marketing.CUAManager;
import com.sap.isa.businessobject.webcatalog.CatalogBusinessObjectManager;
import com.sap.isa.businessobject.IdMapper;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;


/**
 *
 * Reading and display the CUA products for a given product. The product is given with
 * the request parameter <code>PN_CUAPRODUCT</code> (<i>cuaproductkey</i>).<br>
 * <i>Note: </i>This action is only used in B2B scenario.
 * <p>
 *
 * You can use the Parameter PN_CUATYP(<i>cuaproducttype</i>) to display only Accessories or Recommendation.
 * Possible values are the constants ALL, ACCESSORY CROSSSELLING, CROSSUPSELLING or UPSELLING.
 * With the method <code>shouldProductdisplayed</code> one can check on JSP, if a product should
 * display depending the type.
 * <p>
 * You can give a logical forward within the request parameter <i>forward</i>.
 * <h4>Overview over inport parameters and attributes</h4>
 * <table border="1" cellspacing="0" cellpadding="4">
 *   <tr>
 *     <td><b>name</b></td>
 *     <td><b>parameter</b></td>
 *     <td><b>attribute</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr>
 *      <td>cuaproducttype</td><td>X</td><td>&nbsp</td>
 *      <td>cua type which should be displayed</td>
 *   </tr>
 *   <tr>
 *      <td>cuaproductkey</td><td>X</td><td>&nbsp</td>
 *      <td>webCatItem for which cua product should be displayed</td>
 *   </tr>
 *   <tr>
 *      <td>forward</td><td>&nbsp</td><td>X</td>
 *      <td>logical forward, which should be used if the action is successful</td>
 *   </tr>
 *
 * </table>
 * <h4>The following forwards are used by this action</h4>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>showcua</td><td>standard forward if all is alright</td></tr>
 *   <tr><td>error</td><td>forward used if the request parameter <code>PN_CUAPRODUCT</code>
 *                         is missing</td></tr>
 * </table>
 *
 */
public class ShowCUAAction extends ShowCUABaseAction {


    /**
     * Name of the request parameter to the product fields:<br>
     * key of product for which a cua list should be displayed
     */
    public static final String PN_CUAPRODUCT = "cuaproductkey";
    
    /**
     * Name of the request parameter to hold the documenttype
     * the item belongs to
     */
    public static final String PN_CUADOCTYPE = "cuadocumenttype";
	

    /** creates the parameter list for a product, which can use in jsp's to
     *  display cua product list
     */
    public static String createRequest(String productKey, String type){

        return "?" + PN_CUAPRODUCT + "=" + productKey +
               "&" + PN_CUATYP + "=" + type;
    }

	
    /**
     * Template method to be overwritten by the subclasses of this class.
     * Normally you should overwrite this method and you do not touch the
     * <code>isaPreform</code> method.
     *
     * @param request           request context
     * @param response          response context
     * @param userSessionData   object wrapping the session
     * @param requestParser     parser to simple retrieve data from the request
     * @param bom               reference to the BusinessObjectManager
     * @param cbom              reference to the CatalogBusinessObjectManager
     * @param catalog           reference to the catalog
     * @param log               reference to the IsaLocation, needed for logging
     * @param cUAManager        cUAManager business object
     * @param user              user business object
     * @param configuration     configuration object
     * @param forward           logical forward which is given to the action
     *
     * @return logical key for a forward to another action or jsp
     */
    protected String cuaPerform(HttpServletRequest request,
                                HttpServletResponse response,
                                UserSessionData userSessionData,
                                RequestParser parser,
                                CatalogBusinessObjectManager cbom,
                                WebCatInfo catalog,
                                IsaLocation log,
                                CUAManager cUAManager,
                                MarketingUser user,
                                MarketingConfiguration configuration,
                                String cUAType,
                                String forward)
                                    throws CommunicationException {
		final String METHOD_NAME = "cuaPerform()";
		log.entering(METHOD_NAME);
        CUAList cUAList = null;

        // read parameter from request parser
        if (!parser.getParameter(PN_CUAPRODUCT).getValue().isSet()) {
            log.debug("No product key!");
            log.exiting();
            return "error";
        }
        
        String mappingSource = IdMapper.BASKET;
        
        // read parameter from request parser
        if (parser.getParameter(PN_CUADOCTYPE).getValue().isSet()) {

            mappingSource = parser.getParameter(PN_CUADOCTYPE).getValue().getString();
            if (log.isDebugEnabled()) {
                log.debug("Doctype " + mappingSource + " is given!");
            }
        }

        // here in special scenarios an id mapping is needed
        //TechKey productKey = new TechKey(parser.getParameter(PN_CUAPRODUCT).getValue().getString());
		AppBaseBusinessObjectManager appBaseBom = (AppBaseBusinessObjectManager) userSessionData.getBOM(AppBaseBusinessObjectManager.APPBASE_BOM);
		IdMapper idMapper = appBaseBom.createIdMapper();
		String productKeyString = idMapper.mapIdentifier(IdMapper.PRODUCT, 
                                                         mappingSource, 
		                                                 IdMapper.CATALOG,
										                 parser.getParameter(PN_CUAPRODUCT).getValue().getString());
		 TechKey productKey = new TechKey(productKeyString);										                 

        if (productKey.getIdAsString().length() > 0) {
            // create the cuaList object
            cUAList = cUAManager.readCUAList(productKey,
                                             user,
                                             configuration,
                                             catalog,
                                             getCUATypes(cUAType));

            cUAList.determinePrices(cbom.getPriceCalculator());
        }
        else {
            // create only a empty list for an empty key
            cUAList = new CUAList(productKey);
        }


        setRequestAttributes(request,cUAList,cUAType);

        // catalog.setCurrentCUAList(cUAList,cUAType);

        if (forward.length()>0) {
        	log.exiting();
            return forward;
        }
		log.exiting();
        return "showcua";
    }

}
