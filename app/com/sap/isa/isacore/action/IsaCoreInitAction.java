/**
 * Class         IsaCoreInitAction Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved. Author:
 * SAP Created:      22.05.2001 Version:      1.0 $Revision: #15 $ $Date: 2001/07/26 $
 */
package com.sap.isa.isacore.action;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.ecommerce.businessobject.MarketingBusinessObjectManager;
import com.sap.ecommerce.businessobject.campaign.Campaign;
import com.sap.ecommerce.businessobject.campaign.CampaignBusinessObjectManager;
import com.sap.ecommerce.businessobject.campaign.CampaignHeader;
import com.sap.ecommerce.businessobject.loyalty.LoyaltyCampaign;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.User;
import com.sap.isa.businessobject.webcatalog.CatalogBusinessObjectManager;
import com.sap.isa.core.BaseAction;
import com.sap.isa.core.Constants;
import com.sap.isa.core.ContextConst;
import com.sap.isa.core.FrameworkConfigManager;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.event.BusinessEventSourceSetup;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.interaction.InteractionConfigContainer;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.xcm.config.ComponentConfig;
import com.sap.isa.core.xcm.config.ComponentConfigContainer;
import com.sap.isa.core.xcm.init.ExtendedConfigInitHandler;
import com.sap.isa.isacore.BusinessEventCapturerInitHandler;
import com.sap.isa.isacore.BusinessEventHandlerImpl;
import com.sap.isa.isacore.TealeafInitHandler;
import com.sap.isa.user.action.UserActions;

/**
 * <p>
 * Action class providing the special feature to store all data passed to it in the request as parameters into the
 * session context and make them aviable for all other actions.
 * </p>
 * 
 * <p>
 * This feature is implemented to allow other applications to jump directly to the internet sales application and
 * provide all information neccessary to initialize the application.<br>
 * A second feature is, that this action allows internet sales to its own initialisation after the init action has
 * run.
 * </p>
 * 
 * <p>
 * The following parameters are processed by this class:<br>
 * <br>
 * <br>
 * <code>shop          </code> - Shop to be used <br>
 * <code>language      </code> - Language to be used <br>
 * <code>userid        </code> - The id of the user that should be logged in <br>
 * <code>password      </code> - The password of the user that should be logged in <br>
 * <code>forward       </code> - The page the LoginAction should forward to <br>
 * <code>hook_url      </code> - Url for the outside page to return to <br>
 * <code>oci_version   </code> - Version of the oci-interface <br>
 * <code>opportunity_id</code> - id of the auctions opportunity <br>
 * <code>doc_type      </code> - Document Type of the document key to be displayed in detail <br>
 * <code>doc_key       </code> - Document Key of the document Type to be displayed in detail <br>
 * <code>portal        </code> - Marks if the application runs in a portal. Possible values: <code>yes, no</code><br>
 * <code>autoentry     </code> - If marked, sold to selection will skiped selecting the first sold to party if more
 * than one exists, and shop selection will be skiped selecting the first shop if more than one exists and not startup
 * parameter <code>shop</code> ist provided. Possible value <code>YES</code><br>
 * <code>billing.included</code>- Billing documents will be displayed as other document types. Otherwise they are not
 * included in the select box for the document types (<code>NO</code>(default: YES)).
 * </p>
 *
 * @author SAP
 * @version 1.0
 */
public class IsaCoreInitAction extends BaseAction {
    /** String constant specifying the session attribute the startup parameters are stored in. */
    public static final String SC_STARTUP_PARAMETER = "IsaCoreInitAction.action.isacore.isa.sap.com";

    /** String constant specifying if auctions are enabled. */
    public static final String SC_AUCTION_ENABLED = "auctionEnabled.IsaCoreInitAction.action.isacore.isa.sap.com";
    private static final String PARAM_SHOP = "shop";
    private static final String PARAM_SOLDTO = "soldto";
    private static final String PARAM_CATALOG = "catalog";
    private static final String PARAM_LANGUAGE = "language";
    private static final String PARAM_USERID = "userid";
    private static final String PARAM_PASSWORD = "password";

    /** Request parameter name for a logical forward. */
    public static final String PARAM_FORWARD = Constants.FORWARD;
    private static final String PARAM_HOOK_URL = "hook_url";
    private static final String PARAM_HOOK_URL_UPPER = "HOOK_URL";
    private static final String PARAM_OCI_VERSION = "oci_version";
    private static final String PARAM_OCI_VERSION_UPPER = "OCI_VERSION";
    private static final String PARAM_OPP_ID = "_auctionId";
    private static final String PARAM_CAMPAIGN_KEY = "MIG";
    private static final String PARAM_CAMPAIGN_OBJ = "MIG_OBJ";
    private static final String PARAM_URL_KEY = "URLGUIDE";
    private static final String PARAM_DOC_TYPE = "doc_type";
    private static final String PARAM_DOC_KEY = "doc_key";
    private static final String PARAM_AUTOENTRY = "autoentry";
    private static final String FORWARD = "success";
    private static final String PARAM_BILLING_INCLUDED = "billing.included";
	/**
	 * Names for caller, target and okcode parameter constants of an oci client
	 * stored in the HOOK_URL.
	 */
	private static final String OCI_OKCODE = "~OkCode";
	private static final String OCI_TARGET = "~target";
	private static final String OCI_CALLER = "~caller";

    // Easy-B2B
    private static final String EASY_B2B = "easyB2B";
    private static final String INTERACTION_CONFID_USER = "user";
    private static final String INTERACTION_CONFVALUE_EASYB2B = "easyB2B";
    public static final String AUCTIONTRANSID = "transId";

    /**
     * Creates a new IsaCoreInitAction object.
     */
    public IsaCoreInitAction() {
    }

    /**
     * rebuild the parameter language, shop, userid, forward to reenter a crashed application
     *
     * @param session
     *
     * @return parameter list
     *
     * @deprecated Use the method rebuildParameter from them core class com.sap.isa.core.util.StartupParameter
     */
    public static String rebuildParameter(HttpSession session) {
        // get user session data object
        UserSessionData userSessionData = UserSessionData.getUserSessionData(session);

        StringBuffer returnString = new StringBuffer();

        if (userSessionData != null) {
            // Retrieve the startup parameters
            IsaCoreInitAction.StartupParameter startupParameter = (IsaCoreInitAction.StartupParameter) userSessionData.getAttribute(IsaCoreInitAction.SC_STARTUP_PARAMETER);

            boolean parameterFound = false;

            String parameter = startupParameter.getLanguage();

            if (parameter.length() > 0) {
                if (parameterFound) {
                    returnString.append("&");
                }
                else {
                    returnString.append("?");
                    parameterFound = true;
                }

                returnString.append(PARAM_LANGUAGE);
                returnString.append("=");
                returnString.append(parameter);
            }

            parameter = startupParameter.getShop();

            if (parameter.length() > 0) {
                if (parameterFound) {
                    returnString.append("&");
                }
                else {
                    returnString.append("?");
                    parameterFound = true;
                }

                returnString.append(PARAM_SHOP);
                returnString.append("=");
                returnString.append(parameter);
            }

            parameter = startupParameter.getSoldTo();

            if (parameter.length() > 0) {
                if (parameterFound) {
                    returnString.append("&");
                }
                else {
                    returnString.append("?");
                    parameterFound = true;
                }

                returnString.append(PARAM_SOLDTO);
                returnString.append("=");
                returnString.append(parameter);
            }

            parameter = startupParameter.getCatalog();

            if (parameter.length() > 0) {
                if (parameterFound) {
                    returnString.append("&");
                }
                else {
                    returnString.append("?");
                    parameterFound = true;
                }

                returnString.append(PARAM_CATALOG);
                returnString.append("=");
                returnString.append(parameter);
            }

            parameter = startupParameter.getForward();

            if (parameter.length() > 0) {
                if (parameterFound) {
                    returnString.append("&");
                }
                else {
                    returnString.append("?");
                    parameterFound = true;
                }

                returnString.append(PARAM_FORWARD);
                returnString.append("=");
                returnString.append(parameter);
            }

            parameter = startupParameter.getUserId();

            if (parameter.length() > 0) {
                if (parameterFound) {
                    returnString.append("&");
                }
                else {
                    returnString.append("?");
                    parameterFound = true;
                }

                returnString.append(PARAM_USERID);
                returnString.append("=");
                returnString.append(parameter);
            }
        }

        return returnString.toString();
    }

    /**
     * Implemented doPerform method used by ActionBase. This method does some useful stuff and then forwards
     *
     * @param mapping DOCUMENT ME!
     * @param form DOCUMENT ME!
     * @param request DOCUMENT ME!
     * @param response DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     *
     * @throws IOException DOCUMENT ME!
     * @throws ServletException DOCUMENT ME!
     */
    public final ActionForward doPerform(ActionMapping mapping, ActionForm form, HttpServletRequest request,
        HttpServletResponse response)
        throws IOException, ServletException {
        final String METHOD_NAME = "doPerform()";
        log.entering(METHOD_NAME);

        // check if logging is enabled
        boolean isDebugEnabled = log.isDebugEnabled();

        // get user session data object
        UserSessionData userSessionData = UserSessionData.getUserSessionData(request.getSession());

        // get the MBOM
        MetaBusinessObjectManager mbom = userSessionData.getMBOM();

        // set UI Controller in the backend
        mbom.setBackendUIController(com.sap.isa.ui.uicontrol.UIController.getController(userSessionData));
        
        // get BOM
        BusinessObjectManager bom = (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);

        // get BOM
        CatalogBusinessObjectManager cbom = (CatalogBusinessObjectManager) userSessionData.getBOM(CatalogBusinessObjectManager.CATALOG_BOM);

        // Get the data from the request context
        String shop = request.getParameter(PARAM_SHOP);
        String soldTo = request.getParameter(PARAM_SOLDTO);
        String pCatalog = request.getParameter(PARAM_CATALOG);
        String userId = request.getParameter(PARAM_USERID);
        String password = request.getParameter(PARAM_PASSWORD);
        String forward = request.getParameter(PARAM_FORWARD);

        String hookUrl = request.getParameter(PARAM_HOOK_URL);

        if (hookUrl == null) {
            //Read the parameter hook_url in upper case letter.
            hookUrl = request.getParameter(PARAM_HOOK_URL_UPPER);
        }
        
        // Add caller, target and OkCode to hookUrl
        if (hookUrl != null) {
        	if (request.getParameter(OCI_OKCODE) != null && request.getParameter(OCI_OKCODE).length() > 0) {
				hookUrl += "&" + OCI_OKCODE + "=" + request.getParameter(OCI_OKCODE);
        	}
			if (request.getParameter(OCI_TARGET) != null && request.getParameter(OCI_TARGET).length() > 0) {
				hookUrl += "&" + OCI_TARGET + "=" + request.getParameter(OCI_TARGET);
			}
			if (request.getParameter(OCI_CALLER) != null && request.getParameter(OCI_CALLER).length() > 0) {
				hookUrl += "&" + OCI_CALLER + "=" + request.getParameter(OCI_CALLER);
			}
        	    
        }

        String ociVersion = request.getParameter(PARAM_OCI_VERSION);

        if (ociVersion == null) {
            //Read the parameter ociVersion in upper case letter.
            ociVersion = request.getParameter(PARAM_OCI_VERSION_UPPER);
        }

        String opportunityId = request.getParameter(PARAM_OPP_ID);
        String campaignKey = "";
        String campaignObjectType = "";
        String docKey = request.getParameter(PARAM_DOC_KEY);
        String docType = request.getParameter(PARAM_DOC_TYPE);
        String autoEntry = request.getParameter(PARAM_AUTOENTRY);
        String billingIncluded = request.getParameter(PARAM_BILLING_INCLUDED);

        // Convert the shop parameter string to upper cases, because it should
        // be transparent for the user to enter lower or upper cases by specifying the
        // shop Id.
        if ((shop != null) && !shop.equals("")) {
            shop = shop.toUpperCase();
        }

        // Make uniform AUTOENTRY parameter values
        if ((autoEntry != null) && !autoEntry.equals("")) {
            if (autoEntry.toUpperCase().equals("YES") || autoEntry.toUpperCase().equals("X")) {
                autoEntry = "YES";
            }
            else {
                autoEntry = "NO";
            }
        }
        else {
            autoEntry = "NO";
        }

        // init user
        // Locale was set in core.InitAction
        String language = null;
        Locale loc = userSessionData.getLocale();

        if (loc != null) {
            language = loc.getLanguage();
        }

        if ((language == null) || (language.trim().length() < 1)) {
            log.error(LogUtil.APPS_USER_INTERFACE, "core.language.not.found");
        }
        User user = initUser(userSessionData, bom, language);
        String mailIdentifier = request.getParameter(PARAM_CAMPAIGN_KEY);

        if (mailIdentifier != null) {
            try {
                String urlKey = request.getParameter(PARAM_URL_KEY);

                campaignKey = user.getCampaignKeyFromMailIdentifier(mailIdentifier, (urlKey != null) ? urlKey : "");

                if ((campaignKey != null) && (campaignKey.length() > 0) && (user.getSalutationText() != null)) {
                    if (user.getSalutationText() != null) {
                        String salutation = user.getSalutationText();

                        String args[] = { salutation, "" };
                        user.addMessage(new Message(Message.INFO, "user.mig.welcome", args, ""));
                        request.setAttribute(UserActions.RC_USER, user);
                    }

                    campaignObjectType = user.getCampaginObjectType();
 
                    if (user.isLoyaltyCampaign(campaignKey)){
                     MarketingBusinessObjectManager marketingBom = (MarketingBusinessObjectManager) userSessionData.getBOM(MarketingBusinessObjectManager.MARKETING_BOM);

                     LoyaltyCampaign loyCampaign = marketingBom.createLoyaltyCampaign();
                     CampaignHeader campaignHeader = (CampaignHeader) loyCampaign.createHeader();

                     campaignHeader.setTechKey(new TechKey(campaignKey));
                     loyCampaign.setHeader(campaignHeader);

                     loyCampaign.readCampaignHeader();	
                     
                     campaignKey = "";
                     
                    } else { 
                     CampaignBusinessObjectManager campaignBom = (CampaignBusinessObjectManager) userSessionData.getBOM(CampaignBusinessObjectManager.CAMPAIGN_BOM);

                     Campaign campaign = campaignBom.createCampaign();
                     CampaignHeader campaignHeader = (CampaignHeader) campaign.createHeader();

                     campaignHeader.setTechKey(new TechKey(campaignKey));
                     campaign.setHeader(campaignHeader);

                     campaign.readCampaignHeader();
                    }
                    
//                    CampaignData campaign;
//                    if (user.isLoyaltyCampaign(campaignKey)){
//                        MarketingBusinessObjectManager marketingBom = (MarketingBusinessObjectManager) userSessionData.getBOM(MarketingBusinessObjectManager.MARKETING_BOM);
//                        campaign = (CampaignData) marketingBom.createLoyaltyCampaign();  
//                    } else { 
//                        CampaignBusinessObjectManager campaignBom = (CampaignBusinessObjectManager) userSessionData.getBOM(CampaignBusinessObjectManager.CAMPAIGN_BOM);
//                        campaign = (CampaignData) campaignBom.createCampaign();       
//                    }
// 
//                     CampaignHeader campaignHeader = (CampaignHeader) campaign.createHeader();
//
//                     campaignHeader.setTechKey(new TechKey(campaignKey));
//                     campaign.setHeader(campaignHeader);
//
//                     campaign.readCampaignHeader();
                        
                }
            }
            catch (CommunicationException ex) {
                user.logMessage(new Message(Message.ERROR, "user.migError", new String[] { ex.toString() }, ""));
            }
        }


        // get the core Startup Parameter
        com.sap.isa.core.util.StartupParameter startupParameterCore = (com.sap.isa.core.util.StartupParameter) userSessionData.getAttribute(SessionConst.STARTUP_PARAMETER);

        // Store data in StartupParameter
        StartupParameter startupParameter = new StartupParameter(startupParameterCore, shop, soldTo, pCatalog,
                userSessionData.getSAPLanguage(), userId, password, forward, hookUrl, ociVersion, opportunityId, campaignKey,
                campaignObjectType, docType, docKey, autoEntry, billingIncluded);

        userSessionData.setAttribute(SC_STARTUP_PARAMETER, startupParameter);

        /* needed for adapting the unit-tests
        Iterator startIter = startupParameterCore.iterator();

        while (startIter.hasNext()) {
            Parameter parameter = (Parameter) startIter.next();
            int i = 0;
            String value = parameter.getValue();

            if (value == null) {
                value = "Null";
            }
            else if (value.trim().length() < 1) {
                value = "Emtpy";
            }

            log.debug("startupParameterCore --> " + parameter.getName() + " : " + value + "reentry : " +
                parameter.isReentry());
        }

        log.debug("StartupParameter : " + startupParameter.toString());
        */

        // Create the BEH and store it in session context
        // read in web-xml, if capturing is activated
        //ServletContext ctx = getServlet().getServletConfig().getServletContext();
        //String captureEvents = ctx.getInitParameter(ContextConst.IC_ENABLE_TEALEAF_CAPTURING);
        String captureEvents = "false";
        boolean isCapturingActivated = false;
        
        BusinessEventHandlerImpl beh = (BusinessEventHandlerImpl)userSessionData.getBusinessEventHandler();
        if (beh == null) {
            new PanicException("No BusinessEventHandlerImpl is available.");
        }
        beh.setCatalogBom(cbom);
        beh.setBom(bom);

        ComponentConfigContainer ccc = FrameworkConfigManager.XCM.getApplicationScopeConfig();
        ComponentConfig compContainer = ccc.getComponentConfig("wec", "wecconfig");
        if (compContainer!=null) {    
            captureEvents = compContainer.getParamConfig().getProperty("enable.BEventCapturing");
            if (captureEvents != null && captureEvents.equals("true")) {
                    isCapturingActivated = true;
            }
		} 
        else {
            //ignore exception and disable business event capturing
			log.debug("Business Event capturing disabled");
        }
        

        if (isCapturingActivated) {
            beh.register(TealeafInitHandler.getInstance());

	        if (BusinessEventCapturerInitHandler.isActivated()) {
	            beh.register(BusinessEventCapturerInitHandler.getInstance());
	        }
        }

        // Create the BESS object and add it to the BOM
        BusinessEventSourceSetup bess = new BusinessEventSourceSetup(beh);
        mbom.addBusinessObjectCreationListener(bess);

        //Auction checkout startup parameters
        String transId = request.getParameter(AUCTIONTRANSID);

        if (transId != null) {
            startupParameterCore.addParameter(AUCTIONTRANSID, transId, false);
        }

        // are eauctions allowed? - determine it from web.xml
        String enableEauctions = getServlet().getServletConfig().getServletContext().getInitParameter(ContextConst.BO_EAUCTION_ENABLED);

        if ((enableEauctions != null) && (enableEauctions.trim().equalsIgnoreCase("true"))) {
            userSessionData.setAttribute(SC_AUCTION_ENABLED, Boolean.TRUE);
        }
        else {
            userSessionData.setAttribute(SC_AUCTION_ENABLED, Boolean.FALSE);
        }

        if (isDebugEnabled) {
            log.debug("enable eauctions parameter: " + enableEauctions);
        }

        // set the easyB2B attribute
        if (userSessionData.getAttribute(EASY_B2B) == null) {
            // get InteractionConfigContainer
            InteractionConfigContainer interactionConfigData = getInteractionConfig(request);

            // XCM activ?
            if (ExtendedConfigInitHandler.isActive() &&
                    (interactionConfigData.getConfig(INTERACTION_CONFID_USER) != null)) {
                if (log.isDebugEnabled()) {
                    log.debug("XCM is active ...");
                }

                String easyB2B = interactionConfigData.getConfig(INTERACTION_CONFID_USER).getValue(INTERACTION_CONFVALUE_EASYB2B);

                if (log.isDebugEnabled()) {
                    log.debug("EASYB2B-State: " + easyB2B);
                }

                startupParameterCore.addParameter(EASY_B2B, easyB2B, true);

                if ((easyB2B == null) ||
                        (easyB2B.length() == 0) ||
                        !(easyB2B.equals("true") ||
                        easyB2B.equals("false"))) {
                    // get easyB2B from web.xml:
                    easyB2B = getServlet().getServletConfig().getServletContext().getInitParameter("easyb2b.core.isa.sap.com");

                    if ((easyB2B == null) ||
                            (easyB2B.length() == 0) ||
                            !(easyB2B.equals("true") ||
                            easyB2B.equals("false"))) {
                        userSessionData.setAttribute(EASY_B2B, "");
                    }
                    else {
                        userSessionData.setAttribute(EASY_B2B, easyB2B);
                    }
                }
                else {
                    userSessionData.setAttribute(EASY_B2B, easyB2B);
                }
            }
        }

        log.exiting();

        return mapping.findForward(FORWARD);
    }

    /**
     * DOCUMENT ME!
     *
     * @param userSessionData DOCUMENT ME!
     * @param startupParameterCore DOCUMENT ME!
     * @param shop DOCUMENT ME!
     * @param soldTo DOCUMENT ME!
     * @param pCatalog DOCUMENT ME!
     * @param language DOCUMENT ME!
     * @param userId DOCUMENT ME!
     * @param password DOCUMENT ME!
     * @param forward DOCUMENT ME!
     * @param hookUrl DOCUMENT ME!
     * @param ociVersion DOCUMENT ME!
     * @param opportunityId DOCUMENT ME!
     * @param campaignKey DOCUMENT ME!
     * @param campaignObjectType DOCUMENT ME!
     * @param docType DOCUMENT ME!
     * @param docKey DOCUMENT ME!
     * @param autoEntry DOCUMENT ME!
     * @param billingIncluded DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public static StartupParameter getStartupParameter(UserSessionData userSessionData,
        com.sap.isa.core.util.StartupParameter startupParameterCore, String shop, String soldTo, String pCatalog,
        String language, String userId, String password, String forward, String hookUrl, String ociVersion,
        String opportunityId, String campaignKey, String campaignObjectType, String docType, String docKey,
        String autoEntry, String billingIncluded) {
            
        StartupParameter retVal = null;

        retVal = (StartupParameter) userSessionData.getAttribute(SC_STARTUP_PARAMETER);

        if (retVal == null) {
            retVal = new StartupParameter(startupParameterCore, shop, soldTo, pCatalog, language, 
                                            userId, password, forward, hookUrl, ociVersion, opportunityId, 
                                            campaignKey, campaignObjectType, docType, docKey, autoEntry, 
                                            billingIncluded);
        }

        return retVal;
    }

    /**
     * Inner class representing the data captured from the request parameters and stored in the session environment. <br>
     * <br>
     * <b>Note -</b> All fields are guaranteed to be not <code>null</code>. In case a parameter was not present the
     * corresponding property is set to <code>""</code>.
     *
     * @author SAP
     * @version 1.0
     */
    public static final class StartupParameter {
        private com.sap.isa.core.util.StartupParameter parameter;

        /**
         * Constructs a new object containing startup parameters.
         *
         * @param parameter DOCUMENT ME!
         * @param shop DOCUMENT ME!
         * @param soldTo DOCUMENT ME!
         * @param pCatalog DOCUMENT ME!
         * @param language DOCUMENT ME!
         * @param userId DOCUMENT ME!
         * @param password DOCUMENT ME!
         * @param forward DOCUMENT ME!
         * @param hookUrl DOCUMENT ME!
         * @param ociVersion DOCUMENT ME!
         * @param opportunityId DOCUMENT ME!
         * @param campaignKey DOCUMENT ME!
         * @param campaignObjectType DOCUMENT ME!
         * @param docType DOCUMENT ME!
         * @param docKey DOCUMENT ME!
         * @param autoEntry DOCUMENT ME!
         * @param billingIncluded DOCUMENT ME!
         */
        private StartupParameter(com.sap.isa.core.util.StartupParameter parameter, String shop, String soldTo,
            String pCatalog, String language, String userId, String password, String forward, String hookUrl,
            String ociVersion, String opportunityId, String campaignKey, String campaignObjectType, String docType,
            String docKey, String autoEntry, String billingIncluded) {
            this.parameter = parameter;

            parameter.addParameter(PARAM_SHOP, shop, true);
            parameter.addParameter(PARAM_SOLDTO, soldTo, true);
            parameter.addParameter(PARAM_CATALOG, pCatalog, true);
            parameter.addParameter(PARAM_LANGUAGE, language, true);
            parameter.addParameter(PARAM_USERID, userId, true);
            parameter.addParameter(PARAM_PASSWORD, password, true);
            parameter.addParameter(PARAM_FORWARD, forward, true);
            parameter.addParameter(PARAM_HOOK_URL, hookUrl, true);
            parameter.addParameter(PARAM_OCI_VERSION, ociVersion, true);
            parameter.addParameter(PARAM_OPP_ID, opportunityId, false);
            parameter.addParameter(PARAM_CAMPAIGN_KEY, campaignKey, false);
            parameter.addParameter(PARAM_CAMPAIGN_OBJ, campaignObjectType, false);
            parameter.addParameter(PARAM_DOC_KEY, docKey, true);
            parameter.addParameter(PARAM_DOC_TYPE, docType, true);
            parameter.addParameter(PARAM_AUTOENTRY, autoEntry, false);
            parameter.addParameter(PARAM_BILLING_INCLUDED, billingIncluded, true);
        }

        /**
         * Returns the short name of the shop selected using a request parameter.
         *
         * @return short name of shop
         */
        public String getShop() {
            return parameter.getParameterValue(PARAM_SHOP);
        }

        /**
         * Returns the short name of the soldto selected using a request parameter.
         *
         * @return name of soldto
         */
        public String getSoldTo() {
            return parameter.getParameterValue(PARAM_SOLDTO);
        }

        /**
         * Returns the short name of the catalog selected using a request parameter.
         *
         * @return name of catalog
         */
        public String getCatalog() {
            return parameter.getParameterValue(PARAM_CATALOG);
        }

        /**
         * Returns the language selected using a request parameter.
         *
         * @return language
         */
        public String getLanguage() {
            return parameter.getParameterValue(PARAM_LANGUAGE);
        }

        /**
         * Returns the userid selected using a request parameter.
         *
         * @return userId
         */
        public String getUserId() {
            return parameter.getParameterValue(PARAM_USERID);
        }

        /**
         * Returns the forward selected using a request parameter.
         *
         * @return forward
         */
        public String getForward() {
            return parameter.getParameterValue(PARAM_FORWARD);
        }

        /**
         * Returns the hookUrl selected using a request parameter.
         *
         * @return short name of shop
         */
        public String getHookUrl() {
            return parameter.getParameterValue(PARAM_HOOK_URL);
        }

        /**
         * Returns the oci version selected using a request parameter.
         *
         * @return oci version
         */
        public String getOciVersion() {
            return parameter.getParameterValue(PARAM_OCI_VERSION);
        }

        /**
         * Returns the opportunity id selected using a request parameter.
         *
         * @return opportunity id
         */
        public String getOpportunityId() {
            return parameter.getParameterValue(PARAM_OPP_ID);
        }

        /**
         * Returns the property campaignKey
         *
         * @return campaignKey
         */
        public String getCampaignKey() {
            return parameter.getParameterValue(PARAM_CAMPAIGN_KEY);
        }

        /**
         * Returns the property campaignObjectType
         *
         * @return campaignObjectType
         */
        public String getCampaignObjectType() {
            return parameter.getParameterValue(PARAM_CAMPAIGN_OBJ);
        }

        /**
         * Returns the property docType
         *
         * @return docType
         */
        public String getDocType() {
            return parameter.getParameterValue(PARAM_DOC_TYPE);
        }

        /**
         * Returns the property docKey
         *
         * @return docKey
         */
        public String getDocKey() {
            return parameter.getParameterValue(PARAM_DOC_KEY);
        }

        /**
         * Returns the property portal
         *
         * @return portal
         */
        public String getPortal() {
            return parameter.getParameterValue(Constants.PORTAL);
        }

        /**
         * Returns if the application runs in the portal
         *
         * @return portal
         */
        public boolean isPortal() {
            String portal = parameter.getParameterValue(Constants.PORTAL);

            return portal.equalsIgnoreCase("YES");
        }

        /**
         * Returns the property autoEntry
         *
         * @return autoEntry
         */
        public String getAutoEntry() {
			String returnValue = parameter.getParameterValue(PARAM_AUTOENTRY);
			//default value is NO, YES if set from Portal
			if(returnValue==null) 
			  returnValue= "NO";        	
			return returnValue;
        }

        /**
         * Returns the property billing.included
         *
         * @return boolean billing included
         */
        public boolean isBillingIncluded() {
            String billIncl = ((parameter.getParameterValue(PARAM_BILLING_INCLUDED) != null)
                ? parameter.getParameterValue(PARAM_BILLING_INCLUDED) : "YES");
            boolean retVal = true;

            if (billIncl.equalsIgnoreCase("NO")) {
                retVal = false;
            }

            return retVal;
        }

        /**
         * Overridden method from object providing some useful information about the object.
         *
         * @return String representation of the object
         */
        public String toString() {
            return (new StringBuffer()).append("StartupParameter [shop=").append(parameter.getParameterValue(PARAM_SHOP))
                    .append(", language=").append(parameter.getParameterValue(PARAM_LANGUAGE)).append(", soldto=")
                    .append(parameter.getParameterValue(PARAM_SOLDTO)).append(", catalog=")
                    .append(parameter.getParameterValue(PARAM_CATALOG)).append(", userid=")
                    .append(parameter.getParameterValue(PARAM_USERID)).append(", forward=")
                    .append(parameter.getParameterValue(PARAM_FORWARD)).append(", hook_url=")
                    .append(parameter.getParameterValue(PARAM_HOOK_URL)).append(", OCI_VERSION=")
                    .append(parameter.getParameterValue(PARAM_OCI_VERSION)).append(", opportunity_id=")
                    .append(parameter.getParameterValue(PARAM_OPP_ID)).append(", campaignKey=")
                    .append(parameter.getParameterValue(PARAM_CAMPAIGN_KEY)).append(", docKey=")
                    .append(parameter.getParameterValue(PARAM_DOC_KEY)).append(", docType=")
                    .append(parameter.getParameterValue(PARAM_DOC_TYPE)).append(", portal=")
                    .append(parameter.getParameterValue(Constants.PORTAL)).append(", autoEntry=")
                    .append(parameter.getParameterValue(PARAM_AUTOENTRY)).append(", billingIncluded=")
                    .append(parameter.getParameterValue(PARAM_BILLING_INCLUDED)).append("]").toString();
        }
    }

    public User initUser(UserSessionData userSessionData, BusinessObjectManager bom, String language) {
        User user = bom.createUser();

        // Locale was set in core.InitAction
        Locale loc = userSessionData.getLocale();

        if (loc != null) {
			user.setLanguage(loc);
        }
        else {
            log.error(LogUtil.APPS_USER_INTERFACE, "core.language.not.found");
        }

        return user;
    }
      
}
