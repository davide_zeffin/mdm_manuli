/*
 * Created on Mar 8, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.isacore.action.iview;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.backend.boi.isacore.order.DocumentListFilterData;
import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.backend.boi.isacore.order.ItemListData;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.lead.Lead;
import com.sap.isa.businessobject.order.OrderDataContainer;
import com.sap.isa.businessobject.order.OrderStatus;
import com.sap.isa.businessobject.order.PartnerList;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 * @author d028980
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public abstract class OneOrderDoc2BasketBaseAction extends IsaCoreBaseAction {

	protected abstract String oneOrder2BasketPerform(HttpServletRequest request,
	HttpServletResponse response,
	DocumentHandler documentHandler,																								
	IsaLocation log) throws CommunicationException;
    


	public ActionForward isaPerform(ActionMapping mapping,
										ActionForm form,
										HttpServletRequest request,
										HttpServletResponse response,
										UserSessionData userSessionData,
										RequestParser requestParser,
										BusinessObjectManager bom,
										IsaLocation log) throws CommunicationException {
											
	final String METHOD_NAME = "isaPerform()";
	log.entering(METHOD_NAME);
	String guid = request.getParameter("CRM_OBJECT_ID");
    	
	TechKey key1 = new TechKey(guid);
        
	Lead lead = bom.createLead();
	lead.setTechKey(key1);

	String type = DocumentListFilterData.SALESDOCUMENT_TYPE_ORDER;
    
	DocumentHandler documentHandler = (DocumentHandler)
					userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
            	
	//if ((guid != null) && (guid.length() > 0)) {
    		
    		
		TechKey key = new TechKey(guid);
    		
		// read lead data with orderstatus object
  		// check if orderstatus object is already in use, 		
		OrderStatus orderStatus = bom.getOrderStatus();
            
			if (orderStatus == null) {
				orderStatus = bom.createOrderStatus(new OrderDataContainer());
            
			} else {
            	            	
				//release orderstatus
				//DocumentHandler documentHandler = (DocumentHandler)
				//userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);

				OrderStatus os = bom.getOrderStatus();
																										                 
				documentHandler.release(os);
				bom.releaseOrderStatus();
                                
				//create a new object
				orderStatus = bom.createOrderStatus(new OrderDataContainer());
				//BillingStatus bs = bom.getBillingStatus();
				//if (bs != null)
				//   documentHandler.release(bs);
				// release Status objects from BOM                 
				//bom.releaseBillingStatus();
                                            	
			}
            
            
		Shop shop = bom.getShop();
            
	 	orderStatus.setShop(shop);
                    
        orderStatus.setTechKey(key);
                                   
		// read data    		
		orderStatus.readOrderStatus(key, "","",type,null);
			  		    		
		// get prospect information from lead
		// PartnerList partnerList = orderStatus.getPartnerList();
    		 
		PartnerList partnerList = orderStatus.getOrderHeader().getPartnerList(); 
    		    		    		    		    		    	    		    		    		
		orderStatus.getItemList();
		ItemListData itemList = orderStatus.getOrder().getItemListData();
		// Reset the req. delivery date 
		ItemData item;
		for (int i = 0; i < itemList.size(); i++) {
			item = itemList.getItemData(i);
			if(item.getReqDeliveryDate() != null & item.getReqDeliveryDate().length() > 0) {
				item.setReqDeliveryDate("");
			}
		}
	    	    	
		String forwardTo = oneOrder2BasketPerform(request, response, documentHandler, log);
	
	    //String forwardTo = "test";
	    log.exiting();
		return mapping.findForward(forwardTo);

  
	}
}


