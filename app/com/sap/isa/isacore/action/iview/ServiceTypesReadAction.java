/*****************************************************************************
    Class:        ServiceTypesReadAction
    Copyright (c) 2002, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author        Ralf Witt

    $Revision: #2 $
    $DateTime: 2003/10/01 16:07:42 $ (Last changed)
    $Change: 151875 $ (changelist)

*****************************************************************************/
package com.sap.isa.isacore.action.iview;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Search;
import com.sap.isa.businessobject.SearchResult;
import com.sap.isa.businessobject.ServiceTypesSearchCommand;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 *  Reads all defined service types
 *
 */
public class ServiceTypesReadAction extends IsaCoreBaseAction {

    // CONSTANTS
    /**
     * Defines request object for service types. Returns an object of type
     * <code>String[][]</code> from request context.
     */
    public final static String RK_SERVICETYPES = "servicetypes";
	
    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            BusinessObjectManager bom,
            IsaLocation log)
            throws CommunicationException {

		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        Search search = bom.createSearch();
        SearchResult result = null;
        ResultData serviceTypesTable = null;

        result = search.performSearch(new ServiceTypesSearchCommand(bom.getUser().getLanguage()));

        serviceTypesTable = new ResultData(result.getTable());

        // Set cursor before the first row
        serviceTypesTable.beforeFirst();
        String sta[][] = new String[result.getTable().getNumRows()][3];
        int aC = 0;
        while( ! serviceTypesTable.isLast() ) {
            serviceTypesTable.next();
            sta[aC][0] = serviceTypesTable.getString("PROCESSTYPE");
            sta[aC][1] = serviceTypesTable.getString("BOTYPE");
            sta[aC][2] = serviceTypesTable.getString("DESCRIPTION");
            // Next row
            aC++;
         }

        request.setAttribute(RK_SERVICETYPES, sta);
		log.exiting();
        return mapping.findForward("documentlist");
    }
}