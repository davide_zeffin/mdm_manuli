/*****************************************************************************
    Class:        DocumentOverviewListAction
    Copyright (c) 2002, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author        Ralf Witt

    $Revision: #3 $
    $DateTime: 2004/02/19 10:34:20 $ (Last changed)
    $Change: 171408 $ (changelist)

*****************************************************************************/
package com.sap.isa.isacore.action.iview;

import java.util.Date;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Search;
import com.sap.isa.businessobject.SearchResult;
import com.sap.isa.businessobject.ServiceSearchCommand;
import com.sap.isa.businessobject.billing.BillingStatus;
import com.sap.isa.businessobject.header.HeaderBillingDocument;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.order.DocumentListFilter;
import com.sap.isa.businessobject.order.OrderDataContainer;
import com.sap.isa.businessobject.order.OrderStatus;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.businesspartner.businessobject.PartnerFunctionBase;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.core.util.table.TableRow;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.action.order.DocumentListAction;
import com.sap.isa.isacore.actionform.order.DocumentListSelectorForm;

/**
 *  Document overview
 *
 */
public class DocumentOverviewListAction extends IsaCoreBaseAction {

    // CONSTANTS
    /**
     * Defines session object for document type
     */
    public final static String SC_DOCTYPE = "iviewDO_doc_type";
    /**
     * Defines session object for fast refresh
     */
    public final static String SC_IVIEW_FASTREFRESH_LIST = "iviewDO_fastrefresh";
    public final static String SC_SELECTPERIOD = "iviewDO_select_period";
    public final static String SC_PORTALURL = "iview_portalurl";
    public final static String SC_PORTALURLSERVICE = "iview_portalurlservice";
    public final static String SC_PORTALURLBILLING = "iview_portalurlbilling";
           final static String RC_IVIEW_DOCTYPE = "doc_type";
           final static String RC_IVIEW_SELECTPERIOD = "select_period";
           final static String RC_IVIEW_PORTALURL = "portalurl";
           final static String RC_IVIEW_PORTALURLSERVICE = "portalurlservice";
           final static String RC_IVIEW_PORTALURLBILLING = "portalurlbilling";
    public final static String RC_SERVICEBUSINESSTYPE = "servicebusinesstype";
    public final static String RC_DOCSORIGIN = "documentsorigin";
    public final static String RK_DOCLIST = "documentoverviewresultlist";

    private boolean fastRefresh = false;
    private ResultData sessionResult = null;

    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            BusinessObjectManager bom,
            IsaLocation log)
            throws CommunicationException {

		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        // Are any info in session context available
        long selectionPeriod;
        String docType;
        String serviceBusinessType = null;
        fastRefresh = false;
        if (userSessionData.getAttribute(SC_DOCTYPE) == null) {
            // NOTHING in session context, so first call

            // Get document type provided from portal call
            docType = request.getParameter(RC_IVIEW_DOCTYPE);
            // Map document type to service document type is necessary (must be dynamically later on)
            if (docType.equals("servicerequest"))  docType = "S1";
            if (docType.equals("inforequest"))     docType = "S3";
            if (docType.equals("servicecomplain")) docType = "CRCM";
            if (docType.equals("servicecontract")) docType = "SC";
            String portalURL = request.getParameter(RC_IVIEW_PORTALURL);
            String portalURLService = request.getParameter(RC_IVIEW_PORTALURLSERVICE);
            String portalURLBilling = request.getParameter(RC_IVIEW_PORTALURLBILLING);
            try {
                selectionPeriod = Long.parseLong( (String)request.getParameter(RC_IVIEW_SELECTPERIOD) );
            } catch (NumberFormatException NFEx) {
                selectionPeriod = 7l;                               // Set to one week
            }
            // Put selection Period into Session context
            userSessionData.setAttribute(SC_SELECTPERIOD, String.valueOf(selectionPeriod));
            userSessionData.setAttribute(SC_PORTALURL, portalURL);
            userSessionData.setAttribute(SC_PORTALURLSERVICE, portalURLService);
            userSessionData.setAttribute(SC_PORTALURLBILLING, portalURLBilling);
        } else {
             // Follow up call from selection JSP

            // Get Selectquery from Paramter
            docType = request.getParameter(DocumentListSelectorForm.DOCUMENT_TYPE);
            serviceBusinessType = request.getParameter(DocumentOverviewListAction.RC_SERVICEBUSINESSTYPE);
            if (docType == null  ||  docType.equals("")) {
                // On a refresh from portal, do not use send document type, since portal uses
                // the default value, instead use doccument type stored in session.
                docType = (String)userSessionData.getAttribute(SC_DOCTYPE);
                fastRefresh = true;
                // Use ResultData from Session context for faster refresh
                sessionResult = (ResultData) userSessionData.getAttribute(SC_IVIEW_FASTREFRESH_LIST);
            }

            try {
                selectionPeriod = Long.parseLong( (String)userSessionData.getAttribute(SC_SELECTPERIOD));
            } catch (NumberFormatException NFEx) {
                selectionPeriod = 7l;
            }
        }

        if (docType == null  ||  docType.equals("")) {
            return mapping.findForward("invalidparameter");
        }
        if (serviceBusinessType == null) {
            /* One of the service documents was selected as default document type and
               with that defined for first selection. In that case set default values
               (Must be replaced with dynamically read values */
            if (docType.equals("S1") || docType.equals("S3")) {    // Service and Info request
                serviceBusinessType = "BUS2000116";  // not unique
            }
            if (docType.equals("SC")) {                    // Service Contracts
                serviceBusinessType = "BUS2000112";
            }
            if (docType.equals("CRMC")) {                  // Service Complains
                serviceBusinessType = "BUS2000120";
            }
        }

        //  Put document type into Session context
        userSessionData.setAttribute(SC_DOCTYPE, docType);


        // FOR WHAT DOCUMENT TYPE DO WE RUN
        if (docType.equals(DocumentListSelectorForm.ORDER)         ||
            docType.equals(DocumentListSelectorForm.ORDERTEMPLATE) ||
            docType.equals(DocumentListSelectorForm.QUOTATION))    {

            // ISA Sales documents
            log.exiting();
            return IsaSalesSelection(mapping,
                         form,
                         request,
                         response,
                         userSessionData,
                         requestParser,
                         bom,
                         log,
                         docType,
                         selectionPeriod);
        } else if (docType.equals(DocumentListSelectorForm.INVOICE)       ||
                   docType.equals(DocumentListSelectorForm.CREDITMEMO)    ||
                   docType.equals(DocumentListSelectorForm.DOWNPAYMENT))  {
			log.exiting();
            // ISA Billing documents
            return IsaBillingSelection(mapping,
                         form,
                         request,
                         response,
                         userSessionData,
                         requestParser,
                         bom,
                         log,
                         docType,
                         selectionPeriod);


        } else {
			log.exiting();
            // ICSS documents
            return IsaServiceSelection(mapping,
                         form,
                         request,
                         response,
                         userSessionData,
                         requestParser,
                         bom,
                         log,
                         docType,         // docType is serviceProcessType
                         serviceBusinessType,
                         selectionPeriod);
        }
    }
    /**
     *
     */
    private ActionForward IsaSalesSelection(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            BusinessObjectManager bom,
            IsaLocation log,
            String docType,
            long selectionPeriod)
            throws CommunicationException {

        ResultData salesDocTable = null;

        if (! fastRefresh) {
            // Set select filter
            DocumentListFilter filter = new DocumentListFilter();
            filter.setStatusALL();
            if (docType.equals(DocumentListSelectorForm.ORDER)) {
                 filter.setTypeORDER();
            }
            if (docType.equals(DocumentListSelectorForm.ORDERTEMPLATE)) {
                filter.setTypeORDERTEMPLATE();
            }
            if (docType.equals(DocumentListSelectorForm.QUOTATION)) {
                filter.setTypeQUOTA();
            }
            long LdateFrom = 0l;
            long LdateToday = new Date().getTime();
            Date date = new Date();
            LdateFrom = LdateToday - (selectionPeriod * 24l * 60l * 60l * 1000l);                      // One Week in Milliseconds
            date.setTime(LdateFrom);
            filter.setChangedDate(convDateToFormatYYYYMMDD(date.toString()));

            OrderStatus orderStatus = bom.createOrderStatus(new OrderDataContainer());
            BusinessPartnerManager buPaMa = bom.createBUPAManager();    
            TechKey soldToKey = (buPaMa.getDefaultBusinessPartner(PartnerFunctionBase.SOLDTO) != null 
                                 ? buPaMa.getDefaultBusinessPartner(PartnerFunctionBase.SOLDTO).getTechKey()
                                 : new TechKey(""));

            // Perform selection
            orderStatus.readOrderHeaders(soldToKey, bom.getShop(), filter);

            // Transform list into Resultset to be conform to ICSS documents
            Table docTab = new Table("doctab");
            docTab.addColumn(Table.TYPE_STRING,"STATUS");
            docTab.addColumn(Table.TYPE_STRING,"DATE");
            docTab.addColumn(Table.TYPE_STRING,"DOCID");
            docTab.addColumn(Table.TYPE_STRING,"DESCRIPTION");

            for (Iterator it = orderStatus.iterator(); it.hasNext(); ) {
                // Get a header
                HeaderSalesDocument headerSalesDoc = (HeaderSalesDocument)it.next();
                // Put into Table
                TableRow docRow = docTab.insertRow();
                docRow.setRowKey(headerSalesDoc.getTechKey());
                docRow.getField(1).setValue((headerSalesDoc.getStatus() != null ? headerSalesDoc.getStatus() : ""));
                docRow.getField(2).setValue(headerSalesDoc.getChangedAt());
                docRow.getField(3).setValue(headerSalesDoc.getSalesDocNumber());
                if ( ! headerSalesDoc.getPurchaseOrderExt().equals("")) {
                    docRow.getField(4).setValue(headerSalesDoc.getPurchaseOrderExt());
                } else {
                    docRow.getField(4).setValue(headerSalesDoc.getDescription());
                }
            }
            salesDocTable = new ResultData(docTab);
        } else {  // Fast refresh
            salesDocTable = sessionResult;
        }

        // Set data to request
        request.setAttribute(DocumentListAction.RK_DOCUMENT_COUNT, new Integer(salesDocTable.getNumRows()).toString());
        request.setAttribute(DocumentListAction.RK_DLA_SHOP, bom.getShop());
        request.setAttribute(RK_DOCLIST, salesDocTable);
        // Set data to session
        userSessionData.setAttribute(SC_IVIEW_FASTREFRESH_LIST, salesDocTable);

        return mapping.findForward("documentlist");
    }
    /**
     *
     */
    private ActionForward IsaBillingSelection(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            BusinessObjectManager bom,
            IsaLocation log,
            String docType,
            long selectionPeriod)
            throws CommunicationException {

      ResultData billDocTable = null;
      String documentsOrigin = null;

       if (! fastRefresh) {
           // Set select filter
           DocumentListFilter filter = new DocumentListFilter();

           if (docType.equals(DocumentListSelectorForm.INVOICE)) {
               filter.setTypeINVOICE();
           }
           if (docType.equals(DocumentListSelectorForm.CREDITMEMO)) {
               filter.setTypeCREDITMEMO();
           }
           if (docType.equals(DocumentListSelectorForm.DOWNPAYMENT)) {
               filter.setTypeDOWNPAYMENT();
           }
           long LdateFrom = 0l;
           long LdateToday = new Date().getTime();
           Date date = new Date();
           // Set Date to (2002.02.28) and and date (2002.01.01)
           filter.setChangedToDate(convDateToFormatYYYYMMDD(date.toString()));
           LdateFrom = LdateToday - (selectionPeriod * 24l * 60l * 60l * 1000l);   // Selection period in days
           date.setTime(LdateFrom);
           filter.setChangedDate(convDateToFormatYYYYMMDD(date.toString()));

           BillingStatus billingStatus = bom.createBillingStatus();

           BusinessPartnerManager buPaMa = bom.createBUPAManager();    
           TechKey soldToKey = (buPaMa.getDefaultBusinessPartner(PartnerFunctionBase.SOLDTO) != null 
                                ? buPaMa.getDefaultBusinessPartner(PartnerFunctionBase.SOLDTO).getTechKey()
                                : new TechKey(""));
           // Perform selection
           billingStatus.readBillingHeaders(soldToKey ,bom.getShop(), filter);

           // Transform list into Resultset to be conform to ICSS documents
           Table docTab = new Table("doctab");
//           docTab.addColumn(Table.TYPE_STRING,"STATUS");
           docTab.addColumn(Table.TYPE_STRING,"DATE");
           docTab.addColumn(Table.TYPE_STRING,"DUEDATE");
           docTab.addColumn(Table.TYPE_STRING,"DOCID");
           docTab.addColumn(Table.TYPE_STRING,"GROSSVALUE");
           docTab.addColumn(Table.TYPE_STRING,"CURRENCY");

           for (Iterator it = billingStatus.iterator(); it.hasNext(); ) {
               // Get a header
               HeaderBillingDocument headerBillDoc = (HeaderBillingDocument)it.next();
               // Put into Table
               TableRow docRow = docTab.insertRow();
               docRow.setRowKey(new TechKey(headerBillDoc.getBillingDocNo()));
               //               docRow.getField(1).setValue(headerBillDoc.getStatus());           Not yes supported
               docRow.getField(1).setValue(headerBillDoc.getCreatedAt());
               docRow.getField(2).setValue(headerBillDoc.getDueAt());
               docRow.getField(3).setValue(headerBillDoc.getBillingDocNo());
               docRow.getField(4).setValue(headerBillDoc.getGrossValue());
               docRow.getField(5).setValue(headerBillDoc.getCurrency());
               if (  headerBillDoc.getBillingDocumentsOrigin() != null  &&
                   ! headerBillDoc.getBillingDocumentsOrigin().equals("")) {
                   documentsOrigin = headerBillDoc.getBillingDocumentsOrigin();
               }
           }
           billDocTable = new ResultData(docTab);
        } else {  // from Fast refresh
           billDocTable = sessionResult;
        }

        // Set data to request
        request.setAttribute(DocumentListAction.RK_DOCUMENT_COUNT, new Integer(billDocTable.getNumRows()).toString());
        request.setAttribute(DocumentListAction.RK_DLA_SHOP, bom.getShop());
        request.setAttribute(RK_DOCLIST, billDocTable);
        request.setAttribute(DocumentOverviewListAction.RC_DOCSORIGIN, documentsOrigin);
        // Set data to session
        userSessionData.setAttribute(SC_IVIEW_FASTREFRESH_LIST, billDocTable);

        return mapping.findForward("documentlist");
    }
    /**
     *
     */
    private ActionForward IsaServiceSelection(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            BusinessObjectManager bom,
            IsaLocation log,
            String serviceProcessType,
            String serviceBusinessType,
            long selectionPeriod)
            throws CommunicationException {

        ResultData serviceTable = null;

        if (!fastRefresh) {
           DocumentListFilter filter = new DocumentListFilter();

           // DocumentType is Businessobjecttype
           filter.setTypeService(serviceBusinessType);

           long LdateFrom = 0l;
           long LdateToday = new Date().getTime();
           Date date = new Date();
           // Set Date to (2002.02.28) and and date (2002.01.01)
           filter.setChangedToDate(convDateToFormatYYYYMMDD(date.toString()));
           LdateFrom = LdateToday - (selectionPeriod * 24l * 60l * 60l * 1000l);   // Selection period in days
           date.setTime(LdateFrom);
           filter.setChangedDate(convDateToFormatYYYYMMDD(date.toString()));
            // Use the bom to retrieve a reference to the search object
            Search search = bom.createSearch();
            SearchResult result = null;
            BusinessPartnerManager buPaMa = bom.createBUPAManager();    
            TechKey soldToKey = (buPaMa.getDefaultBusinessPartner(PartnerFunctionBase.SOLDTO) != null 
                                 ? buPaMa.getDefaultBusinessPartner(PartnerFunctionBase.SOLDTO).getTechKey()
                                 : new TechKey(""));

            result = search.performSearch(new ServiceSearchCommand(soldToKey,
                                                                   filter,
                                                                   serviceProcessType,
                                                                   bom.getUser().getLanguage()));

            serviceTable = new ResultData(result.getTable());


        } else {  // from Fast refresh
            serviceTable = sessionResult;
        }

        request.setAttribute(DocumentListAction.RK_DOCUMENT_COUNT, new Integer(serviceTable.getNumRows()).toString());
        request.setAttribute(DocumentListAction.RK_DLA_SHOP, bom.getShop());
        request.setAttribute(RK_DOCLIST, serviceTable);
        // Set data to session
        userSessionData.setAttribute(SC_IVIEW_FASTREFRESH_LIST, serviceTable);

        return mapping.findForward("documentlist");
    }
    /**
     * Get the entered Selectiondate in a backend conform format
     */
    private String convDateToFormatYYYYMMDD(String inDate) {
        String allMonth    = "JanFebMarAprMayJunJulAugSepOctNovDec";
        String allMonthNum = "01 02 03 04 05 06 07 08 09 10 11 12";
        String outDate = new String();

        outDate = outDate.concat(inDate.substring(inDate.length()-4, inDate.length()));     // YYYY
        outDate = outDate.concat(allMonthNum.substring( allMonth.indexOf(inDate.substring(4,7)) , allMonth.indexOf(inDate.substring(4,7)) + 2 ));
        outDate = outDate.concat(inDate.substring(8,10));
        return outDate;
    }
}