package com.sap.isa.isacore.action.iview;

// framework dependencies
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.User;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.MessageDocument;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.action.IsaCoreInitAction;

/**
 * Cross-Entry Action that creates a basket when called from UIU
 */
public class CrossBasketAction extends IsaCoreBaseAction {

    public ActionForward isaPerform(
        ActionMapping mapping,
        ActionForm form,
        HttpServletRequest request,
        HttpServletResponse response,
        UserSessionData userSessionData,
        RequestParser requestParser,
        BusinessObjectManager bom,
        IsaLocation log,
        IsaCoreInitAction.StartupParameter startupParameter,
        BusinessEventHandler eventHandler,
        boolean multipleInvocation,
        boolean browserBack)
        throws CommunicationException {

        final String METHOD_NAME = "isaPerform()";
        log.entering(METHOD_NAME);
        String forwardTo = null;

        request.setAttribute("BasketCrossEntry", "X");
        forwardTo = "newBasket";
        log.exiting();
        return mapping.findForward(forwardTo);

    }

}