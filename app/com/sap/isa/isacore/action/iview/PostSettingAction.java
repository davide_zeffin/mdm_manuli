/*****************************************************************************
	Class:        PrepareSettingAction
	Copyright (c) 2002, SAPLabs, Palo Alto, All rights reserved.
	Author        SAP

	$Revision: #1 $

*****************************************************************************/

package com.sap.isa.isacore.action.iview;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.User;
import com.sap.isa.core.Constants;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.StartupParameter.Parameter;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.action.b2b.ReadCatalogAction;
import com.sap.isa.isacore.action.b2b.ReadSoldToAction;
/**
 * Post processing the portal setting JSP page, set
 * the Webshop, catalog and soldTo as parameter to 
 * the init action to restart the application.
 */
public class PostSettingAction extends IsaCoreBaseAction {

	private static final String PARAM_SHOP          = "shop";
    private static final String PARAM_SOLDTO          = "soldto";
	private static final String PARAM_CATALOG          = "catalog";
	public static final String ATTR_BRANCH_OUT    = "branchout";
	/**
	 * Implement this method to add functionality to your action.
	 *
	 * @param form              The <code>FormBean</code> specified in the
	 *                          config.xml file for this action
	 * @param request           The request object
	 * @param response          The response object
	 * @param userSessionData   Object wrapping the session
	 * @param requestParser     Parser to simple retrieve data from the request
	 * @param bom               Reference to the BusinessObjectManager
	 * @param log               Reference to the IsaLocation, needed for logging
	 * @return Forward to another action or page
	 */
	public ActionForward isaPerform(ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response,
			UserSessionData userSessionData,
			RequestParser requestParser,
			BusinessObjectManager bom,
			IsaLocation log)
			throws CommunicationException {

		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
		String forwardTo = "success";
		boolean isDebugEnabled = log.isDebugEnabled();
		if (cancelMaintenance(userSessionData,
								requestParser,
								bom)){
			forwardTo = "cancel";
			log.exiting();
			return mapping.findForward(forwardTo);						
		}

		// check if the saving button is clicked 
		if (saveSetting(userSessionData,
							requestParser,
							bom	)){
			forwardTo = "success";
            boolean portalRefresh = false;
            
			// Find out if we run in a portal.		
			// get all the information from 
			User user = bom.getUser();
			//Shop shop = bom.getShop();
			//get soldTo and catalog information and set to request.
			com.sap.isa.core.util.StartupParameter startupParameterCore =
				(com.sap.isa.core.util.StartupParameter)
				userSessionData.getAttribute(SessionConst.STARTUP_PARAMETER);
			RequestParser.Parameter soldToP =  requestParser.getParameter("SoldTo");
			if (soldToP != null){
				String soldTo = soldToP.getValue().getString();
				Parameter soldToPar = 
					startupParameterCore.getParameter(PARAM_SOLDTO);
				soldToPar.setValue(soldTo);		
				startupParameterCore.addParameter(soldToPar);
				request.setAttribute(Constants.PARAM_SOLDTO, soldTo);	
                portalRefresh = true;
			}
		
            RequestParser.Parameter viewList =  requestParser.getIndexedParameter("ViewIds[]");
            if (viewList != null && viewList.getNumValues() > 0){
                String views[] = (String[]) viewList.getValueStrings();
                String allViews = "";
                
                for (int i=0; i < views.length; i++) {
                    allViews = allViews + views[i];
                    if (i < views.length -1 ) {
                        allViews += Constants.PARAM_SEPARATOR; // group separator
                    }
                }
                request.setAttribute(ReadCatalogAction.PARAM_VIEW, views);

                Parameter viewsPar = 
                    startupParameterCore.getParameter(Constants.PARAM_VIEWS);
                viewsPar.setValue(allViews);     
                startupParameterCore.addParameter(viewsPar);
                portalRefresh = true;
            }
            else { // as views are not necessarily there we must remove the old ones from the parameter storage
                Parameter viewsPar = 
                    startupParameterCore.getParameter(Constants.PARAM_VIEWS);
                viewsPar.setValue("");     
                startupParameterCore.addParameter(viewsPar);
            }
        

			RequestParser.Parameter shopP =  requestParser.getParameter("WebShop");
			if (shopP != null){
				String shopId = shopP.getValue().getString();
				// reset the shop, soldto and catalog
				Parameter shopPar = 
					startupParameterCore.getParameter(PARAM_SHOP);
				shopPar.setValue(shopId);
				request.setAttribute(Constants.PARAM_SHOP, shopId);
                portalRefresh = true;
			}
		
			RequestParser.Parameter catalogP =  requestParser.getParameter("WebCatalog");

			if (catalogP != null){
				String catalogId = catalogP.getValue().getString();
				Parameter catalogPar = 
					startupParameterCore.getParameter(PARAM_CATALOG);
				catalogPar.setValue(catalogId);
				startupParameterCore.addParameter(catalogPar);
				request.setAttribute(Constants.PARAM_CATALOG, catalogId);			
                portalRefresh = true;
			}
            if (portalRefresh == true) {
                request.setAttribute(Constants.PARAM_PORTAL_REFRESH, "yes");   
            }
			// before send to the initilization action class set those
			// attributes		
			user.clearMessages();
			request.setAttribute(BusinessObjectBase.CONTEXT_NAME,user);
			userSessionData.setAttribute(ATTR_BRANCH_OUT, "YES");
			//set all the Shop, SoldTo and Webcatalog back to startparameter
			log.exiting();
			return mapping.findForward(forwardTo);			
		}
		
		//check if the Webshop value is changed
		if (changeWebShop(userSessionData,
							requestParser,
							bom)){
			forwardTo = "shopChanged";
			RequestParser.Parameter shopP =  requestParser.getParameter("WebShop");
			if (shopP != null){
				String shopId = shopP.getValue().getString();
				request.setAttribute(Constants.PARAM_SHOP, shopId);
			}else{
				// should not happen
				forwardTo = "cancel";
			}
		}
		
		return mapping.findForward(forwardTo);	

	}
	
	
	/**
	 * Exit to adjust user if the maintenance is canceled. <br>
	 * 
	 * @param userSessionData
	 * @param requestParser
	 * @param user
	 * @param forwardTo
	 * @return
	 */
	protected boolean cancelMaintenance(UserSessionData userSessionData,
										RequestParser requestParser, 
										BusinessObjectManager bom) {
		// user has pressed the cancel button
        boolean isCancel = false;
		if (requestParser.getParameter("CancelSetting").isSet()) {
			//set all the information to request
			isCancel = true;
		}

		return isCancel;
	}


	/**
	 * Exit to adjust user if the maintenance is saved. <br>
	 * 
	 * @param userSessionData
	 * @param requestParser
	 * @return
	 */
	protected boolean saveSetting(UserSessionData userSessionData,
										RequestParser requestParser, 
										BusinessObjectManager bom) {
		// user has pressed the cancel button
		boolean isSaved = false;
		if (requestParser.getParameter("SaveSetting").isSet()) {
			//set all the information to request
			isSaved = true;
		}
		return isSaved;
	}	
	/**
	 * Exit to adjust user if the WebShop is changed. <br>
	 * 
	 * @param userSessionData
	 * @param requestParser
	 * @param user
	 * @param forwardTo
	 * @return
	 */
	protected boolean changeWebShop(UserSessionData userSessionData,
										RequestParser requestParser, 
										BusinessObjectManager bom) {
		// user has pressed the cancel button
		boolean isChanged = false;
		if (requestParser.getParameter("WebShop").isSet()) {
			//set all the information to request
			RequestParser.Parameter shopP =  requestParser.getParameter("WebShop");
			if (shopP != null){
				String shopId = shopP.getValue().getString();
				String shopFromBOM = bom.getShop().getId();
				if (shopFromBOM.equalsIgnoreCase(shopId)){
					isChanged = true;
				}
			}
		}
		if (requestParser.getParameter("SoldTo").isSet()) {
			isChanged = true;	
		}
		if (requestParser.getParameter("WebCatalog").isSet()) {
			isChanged = true;	
		}	
		return isChanged;
	}
	
}
