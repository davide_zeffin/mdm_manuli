package com.sap.isa.isacore.action.iview;

// framework dependencies
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.MessageDocument;


/**
 * Cross-Entry Action that creates a basket based on lead data 
 * when "order" Button is pressed in the lead portal.
 */
public class Lead2BasketAction extends OneOrderDoc2BasketBaseAction {

	
//	public ActionForward isaPerform(ActionMapping mapping,
//                                    ActionForm form,
//                                    HttpServletRequest request,
//                                    HttpServletResponse response,
//                                    UserSessionData userSessionData,
//                                    RequestParser requestParser,
//                                    BusinessObjectManager bom,
//                                    IsaLocation log) throws CommunicationException {
    
    
//    	String forwardTo = "";
//    	    	    	    	
//    	//get lead guid from request
//    	//String guid = request.getParameter("TechKey");
//    	String guid = request.getParameter("CRM_OBJECT_ID");
//    	
//        TechKey key1 = new TechKey(guid);
//        
//        Lead lead = bom.createLead();
//        lead.setTechKey(key1);
//
//        String type = DocumentListFilterData.SALESDOCUMENT_TYPE_ORDER;
//            	
//    	if ((guid != null) && (guid.length() > 0)) {
//    		
//    		
//    		TechKey key = new TechKey(guid);
//    		
//    		// read lead data with orderstatus object
//    		// check if orderstatus object is already in use, 		
//    		OrderStatus orderStatus = bom.getOrderStatus();
//            
//            if (orderStatus == null) {
//            	            	            
//                Order order = new Order();
//                orderStatus = bom.createOrderStatus(order);
//            
//            } else {
//            	            	
//            	//release orderstatus!!!!
//            	DocumentHandler documentHandler = (DocumentHandler)
//                userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
//
//                OrderStatus os = bom.getOrderStatus();
//																										                 
//                documentHandler.release(os);
//                bom.releaseOrderStatus();
//                                
//                //create a new object
//                Order order = new Order();
//                orderStatus = bom.createOrderStatus(order);
//                //BillingStatus bs = bom.getBillingStatus();
//                //if (bs != null)
//                //   documentHandler.release(bs);
//                // release Status objects from BOM                 
//                //bom.releaseBillingStatus();
//                                            	
//            }
//            
//            
//            Shop shop = bom.getShop();
//            
//            orderStatus.setShop(shop);
//                                   
//            // set data in orderstatus object: is techkey enough?
//            orderStatus.setTechKey(key);
//                		
//    		// read data    		
//   			User user = bom.getUser();
//			
//    		orderStatus.readOrderStatus(key, "","",type, user);
//			  		    		
//    		// get prospect information from lead
//    		// PartnerList partnerList = orderStatus.getPartnerList();
//    		 
//    		PartnerList partnerList = orderStatus.getOrderHeader().getPartnerList(); 
//    		    		    		    		    		    	    		    		    		
//       		orderStatus.getItemList();
//    		ItemListData itemList = orderStatus.getOrder().getItemListData();
//    		    		       		    		    		    	    		
//    		// does an editable document exist?    		
//    	    DocumentHandler documentHandler = (DocumentHandler)
//                userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
    		
    		
		   public String oneOrder2BasketPerform(												
												HttpServletRequest request,
												HttpServletResponse response,
												DocumentHandler documentHandler,																								
												IsaLocation log) throws CommunicationException {
    		
			final String METHOD_NAME = "oneOrder2BasketPerform()";
			log.entering(METHOD_NAME);
    		String forwardTo = null;
    		String objType = null;
    		
			objType = request.getParameter("CRM_OBJECT_TYPE");
    		
    		if (documentHandler.getDocument(DocumentState.TARGET_DOCUMENT) == null) {	
    		    
    		    if (objType.equalsIgnoreCase("BUS2000108")) {
					request.setAttribute("lead2Basket","X");
    		    }
    		    
				if (objType.equalsIgnoreCase("BUS2000111")) {
					request.setAttribute("opp2Basket","X");
				}
    		    
    		    request.setAttribute("BasketCrossEntry", "X");                                          	                                          	                                          	                                          	              
                forwardTo = "newBasket"; 		       		        		            	      		
    			    			
    			    			    			
    		} else {
    			
    			//create new message Document and set the forwardJsp
    			MessageDocument messageDoc = new MessageDocument();
    			messageDoc.setMessageJSP("/iviews/lead2basket/lead2Basket.jsp");    			    			    		    
    			documentHandler.add(messageDoc);
    			messageDoc.setValue("nextAction", "/b2b/lead2BasketDialogue.do");    			
    			forwardTo = "showMessage";
    			
    		}
    		    		    	
    	
        log.exiting();
     	return forwardTo;
     	
    }

}
