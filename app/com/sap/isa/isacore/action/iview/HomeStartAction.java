package com.sap.isa.isacore.action.iview;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.GenericSearch;
import com.sap.isa.businessobject.GenericSearchControlOptionLine;
import com.sap.isa.businessobject.GenericSearchReturn;
import com.sap.isa.businessobject.GenericSearchSearchCommand;
import com.sap.isa.businessobject.GenericSearchSelectOptionLine;
import com.sap.isa.businessobject.GenericSearchSelectOptions;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.businesspartner.businessobject.PartnerFunctionBase;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.isacore.action.GenericSearchBaseAction;
import com.sap.isa.isacore.action.IsaCoreBaseAction;




public class HomeStartAction extends IsaCoreBaseAction {
 
  public final static String SC_PORTALURLBILLING = "iview_portalurlbilling";
  public final static String SC_PORTALURL = "iview_portalurl";
  final static String RC_IVIEW_PORTALURLBILLING = "portalurlbilling";
  final static String RC_IVIEW_PORTALURL = "portalurl";

//  public static final String RK_ORDER_LIST = "orderlist";

  /**
   * Shopinstance
   */
  public static final String RK_DLA_SHOP = "shopinstance";


  public static final String RK_OPEN_ORDERS = "openorderscount";
  public static final String RK_DONE_ORDERS = "doneorderscount";
  public static final String RK_OPEN_QUOTA = "openquotacount";
  public static final String RK_DONE_QUOTA = "donequotacount";
  public static final String RK_SUM_QUOTA = "sumquotacount";
  public static final String RK_SUM_ORDERS = "sumorderscount";
  
  
  public static final String RK_RELEASED_QUOTA = "releasedquotacount";
  public static final String RK_ACCEPTED_QUOTA = "acceptedquotacount";
  public static final String RK_COMPLETED_QUOTA = "completededquotacount";
  
  public static final String RK_NEW_EXTENDED_QUOTA = "newextendedquota";
  public static final String RK_EXPIRED_QUOTA = "expiredquota";
  public static final String RK_INREQUEST_QUOTA = "inrequestquota";
  public static final String RK_CONFIRMED_QUOTA = "confirmedquota";
    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            BusinessObjectManager bom,
            IsaLocation log)
            throws CommunicationException {

		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        // Page that should be displayed next.

		Shop shop = bom.getShop();
		if (shop == null) {
			log.exiting();
			throw new PanicException("Shop object null");
		}
		request.setAttribute(RK_DLA_SHOP, shop);

        
        
        String forwardTo = null;
        HttpSession session = request.getSession();

		//generic search framework integration

		// determine scenario dependent parameters
		String function_category ="0001";
		String partner_selparam = "PARTNER_NO/1";
		if (shop.getCompanyPartnerFunction().equals(PartnerFunctionBase.RESELLER)) {
			function_category ="0010";
			partner_selparam = "PARTNER_NO/2";			
		}			
		
		BusinessPartnerManager bupaMa = bom.createBUPAManager();		
		BusinessPartner bupa = bupaMa.getDefaultBusinessPartner(shop.getCompanyPartnerFunction());

		
		GenericSearch searchRequestHandler = bom.createGenericSearch();
		
		//orders, created within the last 7 days handle = 0		
		GenericSearchSelectOptions selOpt = new GenericSearchSelectOptions();
		GenericSearchSelectOptionLine selOptLine = new GenericSearchSelectOptionLine(0,
																				"LIST_FUNCTION",
																				null,
																				null,
																				null,
																				null,
																				"I",
																				"EQ",
																				"ORDER",
																				"document_types");
		selOpt.addSelectOptionLine(selOptLine);		
		GenericSearchSelectOptionLine selOptLine1 = new GenericSearchSelectOptionLine(0,
																				"STAT",
																				"CL_CRM_REPORT_SET_STATUS",
																				null,
																				null,
																				"RAN",
																				"I",
																				"EQ",
																				"ALL",
																				null);
		selOpt.addSelectOptionLine(selOptLine1);																		
		GenericSearchSelectOptionLine selOptLine2 = new GenericSearchSelectOptionLine(0,
																				"CREATED_AT",
																				"CL_CRM_REPORT_EXT_ORDERADM_H",
																				"datetoken",
																				null,
																				"RAN",
																				"I",
																				"EQ",
																				"last_week",
																				null);
		selOpt.addSelectOptionLine(selOptLine2);
		
		//set business partner information
										
			GenericSearchSelectOptionLine selOptLine3 = new GenericSearchSelectOptionLine(0,
																				partner_selparam,
																				"CL_CRM_REPORT_SET_PARTNER",
																				null,
																				function_category,
																				"RAN",
																				"I",
																				"EQ",
																				bupa.getId(),
																				null);
			selOpt.addSelectOptionLine(selOptLine3);
//		}
		
		
		//set Search Control options -> search only number of Documents 
		GenericSearchControlOptionLine line = new GenericSearchControlOptionLine(0,true,0);
		selOpt.setControlOptionLine(line);
		
		//completed orders, created within the last 7 days handle = 1		
		GenericSearchSelectOptionLine selOptLine4 = new GenericSearchSelectOptionLine(1,
																				"LIST_FUNCTION",
																				null,
																				null,
																				null,
																				null,
																				"I",
																				"EQ",
																				"ORDER",
																				"document_types");
		selOpt.addSelectOptionLine(selOptLine4);		
		GenericSearchSelectOptionLine selOptLine5 = new GenericSearchSelectOptionLine(1,
																				"STAT",
																				"CL_CRM_REPORT_SET_STATUS",
																				null,
																				null,
																				"RAN",
																				"I",
																				"EQ",
																				"I1005",
																				null);
		selOpt.addSelectOptionLine(selOptLine5);																		
		GenericSearchSelectOptionLine selOptLine6 = new GenericSearchSelectOptionLine(1,
																				"CREATED_AT",
																				"CL_CRM_REPORT_EXT_ORDERADM_H",
																				"datetoken",
																				null,
																				"RAN",
																				"I",
																				"EQ",
																				"last_week",
																				null);
		selOpt.addSelectOptionLine(selOptLine6);
		
		//set business partner information						
		
			GenericSearchSelectOptionLine selOptLine7 = new GenericSearchSelectOptionLine(1,
																				partner_selparam,
																				"CL_CRM_REPORT_SET_PARTNER",
																				null,
																				function_category,
																				"RAN",
																				"I",
																				"EQ",
																				bupa.getId(),
																				null);
			selOpt.addSelectOptionLine(selOptLine7);						
		
		
		//set Search Control options -> search only number of Documents 
		GenericSearchControlOptionLine line1 = new GenericSearchControlOptionLine(1,true,0);
		selOpt.setControlOptionLine(line1);

		//open orders, independent from the creation date, handle = 2		
		GenericSearchSelectOptionLine selOptLine8 = new GenericSearchSelectOptionLine(2,
																				"LIST_FUNCTION",
																				null,
																				null,
																				null,
																				null,
																				"I",
																				"EQ",
																				"ORDER",
																				"document_types");
		selOpt.addSelectOptionLine(selOptLine8);		
		GenericSearchSelectOptionLine selOptLine9 = new GenericSearchSelectOptionLine(2,
																				"STAT",
																				"CL_CRM_REPORT_SET_STATUS",
																				null,
																				null,
																				"RAN",
																				"I",
																				"EQ",
																				"I1002",
																				null);
		selOpt.addSelectOptionLine(selOptLine9);																		

		GenericSearchSelectOptionLine selOptLine10 = new GenericSearchSelectOptionLine(2,
																				"CREATED_AT",
																				"CL_CRM_REPORT_EXT_ORDERADM_H",
																				"rdatetoken",
																				null,
																				"RAN",
																				"I",
																				"BT",
																				"19000101",
																				"99991230");
		selOpt.addSelectOptionLine(selOptLine10);																		
		
		//set business partner information											
		GenericSearchSelectOptionLine selOptLine11 = new GenericSearchSelectOptionLine(2,
																				partner_selparam,
																				"CL_CRM_REPORT_SET_PARTNER",
																				null,
																				function_category,
																				"RAN",
																				"I",
																				"EQ",
																				bupa.getId(),
																				null);
		selOpt.addSelectOptionLine(selOptLine11);
		
			
		//set Search Control options -> search only number of Documents 
		GenericSearchControlOptionLine line2 = new GenericSearchControlOptionLine(2,true,0);
		selOpt.setControlOptionLine(line2);
						
		//search quotations
		if (shop.isQuotationAllowed()) {
			// count additional status released and accepted
			
			//quoations, created within the last 7 days handle = 3		
			
			GenericSearchSelectOptionLine selOptLine12 = new GenericSearchSelectOptionLine(3,
																					"LIST_FUNCTION",
																					null,
																					null,
																					null,
																					null,
																					"I",
																					"EQ",
																					"QUOTATION",
																					"document_types");
			selOpt.addSelectOptionLine(selOptLine12);		
			GenericSearchSelectOptionLine selOptLine13 = new GenericSearchSelectOptionLine(3,
																					"STAT",
																					"CL_CRM_REPORT_SET_STATUS",
																					null,
																					null,
																					"RAN",
																					"I",
																					"EQ",
																					"LEANALL",
																					null);
			selOpt.addSelectOptionLine(selOptLine13);																		
			GenericSearchSelectOptionLine selOptLine14 = new GenericSearchSelectOptionLine(3,
																					"CREATED_AT",
																					"CL_CRM_REPORT_EXT_ORDERADM_H",
																					"datetoken",
																					null,
																					"RAN",
																					"I",
																					"EQ",
																					"last_week",
																					null);
			selOpt.addSelectOptionLine(selOptLine14);
			
			GenericSearchSelectOptionLine selOptLine31 = new GenericSearchSelectOptionLine(3,
																					"QUOTATION_TYPE",
																					null,
																					null,
																					null,
																					null,
																					null,
																					null,
																					"LEAN",
																					null);
			selOpt.addSelectOptionLine(selOptLine31);

			
			//set business partner information							
			GenericSearchSelectOptionLine selOptLine15 = new GenericSearchSelectOptionLine(3,
																					partner_selparam,
																					"CL_CRM_REPORT_SET_PARTNER",
																					null,
																					function_category,
																					"RAN",
																					"I",
																					"EQ",
																					bupa.getId(),
																					null);
			selOpt.addSelectOptionLine(selOptLine15);
		
			//set Search Control options -> search only number of Documents 
			GenericSearchControlOptionLine line3 = new GenericSearchControlOptionLine(3,true,0);
			selOpt.setControlOptionLine(line3);

			//completed quoations, created within the last 7 days handle = 4		
			
			GenericSearchSelectOptionLine selOptLine16 = new GenericSearchSelectOptionLine(4,
																					"LIST_FUNCTION",
																					null,
																					null,
																					null,
																					null,
																					"I",
																					"EQ",
																					"QUOTATION",
																					"document_types");
			selOpt.addSelectOptionLine(selOptLine16);		
			GenericSearchSelectOptionLine selOptLine17 = new GenericSearchSelectOptionLine(4,
																					"STAT",
																					"CL_CRM_REPORT_SET_STATUS",
																					null,
																					null,
																					"RAN",
																					"I",
																					"EQ",
																					"I1005",
																					null);
			selOpt.addSelectOptionLine(selOptLine17);																		
			GenericSearchSelectOptionLine selOptLine18 = new GenericSearchSelectOptionLine(4,
																					"CREATED_AT",
																					"CL_CRM_REPORT_EXT_ORDERADM_H",
																					"datetoken",
																					null,
																					"RAN",
																					"I",
																					"EQ",
																					"last_week",
																					null);
			selOpt.addSelectOptionLine(selOptLine18);
			
			GenericSearchSelectOptionLine selOptLine32 = new GenericSearchSelectOptionLine(4,
																					"QUOTATION_TYPE",
																					null,
																					null,
																					null,
																					null,
																					null,
																					null,
																					"LEAN",
																					null);
			selOpt.addSelectOptionLine(selOptLine32);		
			//set business partner information							
			GenericSearchSelectOptionLine selOptLine19 = new GenericSearchSelectOptionLine(4,
																					partner_selparam,
																					"CL_CRM_REPORT_SET_PARTNER",
																					null,
																					function_category,
																					"RAN",
																					"I",
																					"EQ",
																					bupa.getId(),
																					null);
			selOpt.addSelectOptionLine(selOptLine19);
		
			//set Search Control options -> search only number of Documents 
			GenericSearchControlOptionLine line4 = new GenericSearchControlOptionLine(4,true,0);
			selOpt.setControlOptionLine(line4);

			//open quoations, handle = 5		
			
			GenericSearchSelectOptionLine selOptLine20 = new GenericSearchSelectOptionLine(5,
																					"LIST_FUNCTION",
																					null,
																					null,
																					null,
																					null,
																					"I",
																					"EQ",
																					"QUOTATION",
																					"document_types");
			selOpt.addSelectOptionLine(selOptLine20);		
			GenericSearchSelectOptionLine selOptLine21 = new GenericSearchSelectOptionLine(5,
																					"STAT",
																					"CL_CRM_REPORT_SET_STATUS",
																					null,
																					null,
																					"RAN",
																					"I",
																					"EQ",
																					"I1002",
																					null);
			selOpt.addSelectOptionLine(selOptLine21);																		
			GenericSearchSelectOptionLine selOptLine22 = new GenericSearchSelectOptionLine(5,
																					"CREATED_AT",
																					"CL_CRM_REPORT_EXT_ORDERADM_H",
																					"rdatetoken",
																					null,
																					"RAN",
																					"I",
																					"BT",
																					"19000101",
																					"99991230");
			selOpt.addSelectOptionLine(selOptLine22);
			
			GenericSearchSelectOptionLine selOptLine33 = new GenericSearchSelectOptionLine(5,
																					"QUOTATION_TYPE",
																					null,
																					null,
																					null,
																					null,
																					null,
																					null,
																					"LEAN",
																					null);
			selOpt.addSelectOptionLine(selOptLine33);
					
			//set business partner information							
			GenericSearchSelectOptionLine selOptLine23 = new GenericSearchSelectOptionLine(5,
																					partner_selparam,
																					"CL_CRM_REPORT_SET_PARTNER",
																					null,
																					function_category,
																					"RAN",
																					"I",
																					"EQ",
																					bupa.getId(),
																					null);
			selOpt.addSelectOptionLine(selOptLine23);
		
			//set Search Control options -> search only number of Documents 
			GenericSearchControlOptionLine line5 = new GenericSearchControlOptionLine(5,true,0);
			selOpt.setControlOptionLine(line5);
				
		if (shop.isQuotationExtended()) {
		
			//new during the last 7 days, handle= 6		
			
			GenericSearchSelectOptionLine selOptLine24 = new GenericSearchSelectOptionLine(6,
																					"LIST_FUNCTION",
																					null,
																					null,
																					null,
																					null,
																					"I",
																					"EQ",
																					"QUOTATION",
																					"document_types");
			selOpt.addSelectOptionLine(selOptLine24);		
			
																					
			GenericSearchSelectOptionLine selOptLine26 = new GenericSearchSelectOptionLine(6,
																					"CREATED_AT",
																					"CL_CRM_REPORT_EXT_ORDERADM_H",
																					"datetoken",
																					null,
																					"RAN",
																					"I",
																					"EQ",
																					"last_week",
																					null);
			selOpt.addSelectOptionLine(selOptLine26);
					
			//set business partner information							
			GenericSearchSelectOptionLine selOptLine27 = new GenericSearchSelectOptionLine(6,
																					partner_selparam,
																					"CL_CRM_REPORT_SET_PARTNER",
																					null,
																					function_category,
																					"RAN",
																					"I",
																					"EQ",
																					bupa.getId(),
																					null);
			selOpt.addSelectOptionLine(selOptLine27);
		
			//set Search Control options -> search only number of Documents 
			GenericSearchControlOptionLine line7 = new GenericSearchControlOptionLine(6,true,0);
			selOpt.setControlOptionLine(line7);
		
			
			
			//of which expired, created within the last 7 days handle = 7		
			
			GenericSearchSelectOptionLine selOptLine28 = new GenericSearchSelectOptionLine(7,
																					"LIST_FUNCTION",
																					null,
																					null,
																					null,
																					null,
																					"I",
																					"EQ",
																					"QUOTATION",
																					"document_types");
			selOpt.addSelectOptionLine(selOptLine28);		
			GenericSearchSelectOptionLine selOptLine29 = new GenericSearchSelectOptionLine(7,
																					"STAT",
																					"CL_CRM_REPORT_SET_STATUS",
																					null,
																					null,
																					"RAN",
																					"I",
																					"EQ",
																					"COMPLETED",
																					null);
			selOpt.addSelectOptionLine(selOptLine29);																		
			GenericSearchSelectOptionLine selOptLine30 = new GenericSearchSelectOptionLine(7,
																					"CREATED_AT",
																					"CL_CRM_REPORT_EXT_ORDERADM_H",
																					"datetoken",
																					null,
																					"RAN",
																					"I",
																					"EQ",
																					"last_week",
																					null);
			selOpt.addSelectOptionLine(selOptLine30);
		
			//set business partner information							
			GenericSearchSelectOptionLine selOptLine35 = new GenericSearchSelectOptionLine(7,
																					partner_selparam,
																					"CL_CRM_REPORT_SET_PARTNER",
																					null,
																					function_category,
																					"RAN",
																					"I",
																					"EQ",
																					bupa.getId(),
																					null);
			selOpt.addSelectOptionLine(selOptLine35);
		
			//set Search Control options -> search only number of Documents 
			GenericSearchControlOptionLine line8 = new GenericSearchControlOptionLine(7,true,0);
			selOpt.setControlOptionLine(line8);
			
//			in request, handle = 8		
			
					  GenericSearchSelectOptionLine selOptLine36 = new GenericSearchSelectOptionLine(8,
																							  "LIST_FUNCTION",
																							  null,
																							  null,
																							  null,
																							  null,
																							  "I",
																							  "EQ",
																							  "QUOTATION",
																							  "document_types");
					  selOpt.addSelectOptionLine(selOptLine36);		
					  GenericSearchSelectOptionLine selOptLine37 = new GenericSearchSelectOptionLine(8,
																							  "STAT",
																							  "CL_CRM_REPORT_SET_STATUS",
																							  null,
																							  null,
																							  "RAN",
																							  "I",
																							  "EQ",
																							  "OPEN",
																							  null);
					  selOpt.addSelectOptionLine(selOptLine37);																		
					  GenericSearchSelectOptionLine selOptLine38 = new GenericSearchSelectOptionLine(8,
																							  "CREATED_AT",
																							  "CL_CRM_REPORT_EXT_ORDERADM_H",
																							  "rdatetoken",
																							  null,
																							  "RAN",
																							  "I",
																							  "BT",
																							  "19000101",
																							  "99991230");
					  selOpt.addSelectOptionLine(selOptLine38);
		
					  //set business partner information							
					  GenericSearchSelectOptionLine selOptLine39 = new GenericSearchSelectOptionLine(8,
																							  partner_selparam,
																							  "CL_CRM_REPORT_SET_PARTNER",
																							  null,
																							  function_category,
																							  "RAN",
																							  "I",
																							  "EQ",
																							  bupa.getId(),
																							  null);
					  selOpt.addSelectOptionLine(selOptLine39);
		
					  //set Search Control options -> search only number of Documents 
					  GenericSearchControlOptionLine line9 = new GenericSearchControlOptionLine(8,true,0);
					  selOpt.setControlOptionLine(line9);

//			Confirmed, handle = 9		
			
					  GenericSearchSelectOptionLine selOptLine40 = new GenericSearchSelectOptionLine(9,
																							  "LIST_FUNCTION",
																							  null,
																							  null,
																							  null,
																							  null,
																							  "I",
																							  "EQ",
																							  "QUOTATION",
																							  "document_types");
					  selOpt.addSelectOptionLine(selOptLine40);		
					  GenericSearchSelectOptionLine selOptLine41 = new GenericSearchSelectOptionLine(9,
																							  "STAT",
																							  "CL_CRM_REPORT_SET_STATUS",
																							  null,
																							  null,
																							  "RAN",
																							  "I",
																							  "EQ",
																							  "RELEASED",
																							  null);
					  selOpt.addSelectOptionLine(selOptLine41);																		
					  GenericSearchSelectOptionLine selOptLine42 = new GenericSearchSelectOptionLine(9,
																							  "CREATED_AT",
																							  "CL_CRM_REPORT_EXT_ORDERADM_H",
																							  "rdatetoken",
																							  null,
																							  "RAN",
																							  "I",
																							  "BT",
																							  "19000101",
																							  "99991230");
					  selOpt.addSelectOptionLine(selOptLine42);
		
					  //set business partner information							
					  GenericSearchSelectOptionLine selOptLine43 = new GenericSearchSelectOptionLine(9,
																							  partner_selparam,
																							  "CL_CRM_REPORT_SET_PARTNER",
																							  null,
																							  function_category,
																							  "RAN",
																							  "I",
																							  "EQ",
																							  bupa.getId(),
																							  null);
					  selOpt.addSelectOptionLine(selOptLine43);
		
					  //set Search Control options -> search only number of Documents 
					  GenericSearchControlOptionLine line10 = new GenericSearchControlOptionLine(9,true,0);
					  selOpt.setControlOptionLine(line10);
		
		
		}	
		
		
		} 
			
						
		//perform the search		
		GenericSearchSearchCommand searchCommand = new GenericSearchSearchCommand(selOpt);
		GenericSearchBaseAction gsba = new GenericSearchBaseAction();
		searchCommand.setBackendImplementation("ORDER");				
		GenericSearchReturn returnData = searchRequestHandler.performGenericSearch(searchCommand);
		
		// set the found numbers as request parameters
		
		int neworders = 0;
		int completedorders = 0;
		int openorders = 0;
		
		
		int newquota = 0;
        int completedquota = 0;
        int openquota = 0;		
		
		
		int newextendedquota = 0;
		int expiredquota = 0;
		int inrequestquota = 0;
		int confirmedquota = 0;
		//String sumquota   = (String)request.getAttribute(HomeStartAction.RK_SUM_QUOTA);

		
		if (returnData.getCountedDocuments() != null) {
			ResultData tab = new ResultData(returnData.getCountedDocuments());
				 								
		int compHandle = 0;
		tab.beforeFirst();
		// define strings to pass number of documents to request
								
		while (tab.next()) {
			compHandle = tab.getInt("HANDLE");
			
			switch(compHandle) {
				case 0: neworders = tab.getInt("NUM_DOC_SEL"); break;
				case 1: completedorders = tab.getInt("NUM_DOC_SEL"); break;
				case 2: openorders = tab.getInt("NUM_DOC_SEL"); break;
				case 3: newquota = tab.getInt("NUM_DOC_SEL"); break;
				case 4: completedquota = tab.getInt("NUM_DOC_SEL"); break;
				case 5: openquota = tab.getInt("NUM_DOC_SEL"); break;
				case 6: newextendedquota = tab.getInt("NUM_DOC_SEL"); break;																																 				
				case 7: expiredquota = tab.getInt("NUM_DOC_SEL"); break;
				case 8: inrequestquota = tab.getInt("NUM_DOC_SEL"); break;
				case 9: confirmedquota = tab.getInt("NUM_DOC_SEL"); break;
			}
			
		}

		} // countedDocs!=null
		
		// set numbers as request parameters
		request.setAttribute(RK_SUM_ORDERS, Integer.toString(neworders));
		request.setAttribute(RK_DONE_ORDERS, Integer.toString(completedorders));
		request.setAttribute(RK_OPEN_ORDERS, Integer.toString(openorders));

		request.setAttribute(RK_SUM_QUOTA, Integer.toString(newquota));
		request.setAttribute(RK_DONE_QUOTA, Integer.toString(completedquota));
		request.setAttribute(RK_OPEN_QUOTA, Integer.toString(openquota));
		
		
		request.setAttribute(RK_NEW_EXTENDED_QUOTA, Integer.toString(newextendedquota));
		request.setAttribute(RK_EXPIRED_QUOTA, Integer.toString(expiredquota));
		request.setAttribute(RK_INREQUEST_QUOTA, Integer.toString(inrequestquota));
		request.setAttribute(RK_CONFIRMED_QUOTA, Integer.toString(confirmedquota));
		
		
		
//		request.setAttribute(RK_RELEASED_QUOTA, Integer.toString(releasedquota));
//		request.setAttribute(RK_ACCEPTED_QUOTA, Integer.toString(acceptedquota));

		return mapping.findForward("orderlist");
		
		//int noOfFoundDocs = returnData.getCountedDocuments().getRow(1).getField("NUM_DOC_SEL").getInt();
		
		
		
		//int noOfFoundDocs = returnData.getCountedDocuments().getRow(1).getField("NUM_DOC_SEL").getInt();
                      
//        // open orders
//        documentListSelectorForm.setDocumentType(DocumentListFilter.SALESDOCUMENT_TYPE_ORDER);
//        documentListSelectorForm.setDocumentStatus(DocumentListFilter.SALESDOCUMENT_STATUS_OPEN);        
//        //documentListSelectorForm.setValiditySelection(documentListSelectorForm.PERIOD);
//        //documentListSelectorForm.setPeriod(documentListSelectorForm.LAST_WEEK);
//        documentListSelectorForm.setSalesSelectionStart(true);
//        
//        // imitate request because form bean is used  
//        documentListSelectorForm.validate(null, null);
//
//        DocumentListFilter filter = documentListSelectorForm.getDocumentListFilter();
//
//
//        OrderStatus orderList = bom.getOrderStatus();
//        if (orderList == null) {
//          orderList = bom.createOrderStatus(new OrderDataContainer());
//        }
//        BusinessPartnerManager buPaMa = bom.createBUPAManager();    
//        TechKey soldToKey = (buPaMa.getDefaultBusinessPartner(PartnerFunctionBase.SOLDTO) != null 
//                             ? buPaMa.getDefaultBusinessPartner(PartnerFunctionBase.SOLDTO).getTechKey()
//                             : new TechKey(""));
//
//        //if (shop.getCompanyPartnerFunction().equals(PartnerFunctionBase.RESELLER)) {
//        if (shop.isSoldtoSelectable()){
//          // take aware of reseller sceanrio
//          orderList.setMultiPartnerScenario(true);
//
//          // handle the partner list in this case
//          PartnerList partnerList = orderList.getPartnerList();
//
//          // get reseller from BuPaMa
//          BusinessPartner buPa = buPaMa.getDefaultBusinessPartner(shop.getCompanyPartnerFunction());
//
//          // add reseller to the partner list
//          if (buPa != null) {
//            PartnerListEntry partner = new PartnerListEntry();
//            partner.setPartnerId(buPa.getId());
//            partner.setPartnerTechKey(buPa.getTechKey());
//            partnerList.setPartnerData(shop.getCompanyPartnerFunction(), partner);
//          }
//
//          // add soldto to the partner list
//          String soldto = documentListSelectorForm.getSoldto().trim();
//
//          if (soldto != null && soldto.length() > 0) {
//            PartnerListEntry partner = new PartnerListEntry();
//            partner.setPartnerId(soldto);
//            partnerList.setPartnerData(PartnerFunctionBase.SOLDTO, partner);
//          }
//
//        }
//
//
//        // Get Salesdocuments headers (for calculation of openorders)
//        orderList.readOrderHeaders(soldToKey, shop, filter);
//        int openorders = orderList.getOrderCount();
//        request.setAttribute(RK_OPEN_ORDERS, Integer.toString(openorders));



//        // Calculation of doneorders
//        documentListSelectorForm.setDocumentType(DocumentListFilter.SALESDOCUMENT_TYPE_ORDER);
//        documentListSelectorForm.setDocumentStatus(DocumentListFilter.SALESDOCUMENT_STATUS_COMPLETED);
//        documentListSelectorForm.setValiditySelection(DocumentListSelectorForm.PERIOD);
//        documentListSelectorForm.setPeriod(DocumentListSelectorForm.LAST_WEEK);
//        documentListSelectorForm.setSalesSelectionStart(true);
//        
//        // imitate request because form bean is used  
//        documentListSelectorForm.validate(null, null);
//        
//        filter = documentListSelectorForm.getDocumentListFilter();
//        orderList.readOrderHeaders(soldToKey, shop, filter);
//        int doneorders = orderList.getOrderCount();
//        request.setAttribute(RK_DONE_ORDERS, Integer.toString(doneorders));
//
//		
//		// new orders
//        documentListSelectorForm.setDocumentType(DocumentListFilter.SALESDOCUMENT_TYPE_ORDER);
//        documentListSelectorForm.setDocumentStatus("null");
//        documentListSelectorForm.setValiditySelection(DocumentListSelectorForm.PERIOD);
//        documentListSelectorForm.setPeriod(DocumentListSelectorForm.LAST_WEEK);
//        documentListSelectorForm.setSalesSelectionStart(true);
//        
//        // imitate request because form bean is used  
//        documentListSelectorForm.validate(null, null);
//        
//        filter = documentListSelectorForm.getDocumentListFilter();
//        orderList.readOrderHeaders(soldToKey, shop, filter);
//        int sumorders = orderList.getOrderCount();
//        request.setAttribute(RK_SUM_ORDERS, Integer.toString(sumorders));
//		
//		
//		
//	
//		
//		//selOpt.addSelectOptionLine()
//		
//		
//		//GenericSearchReturn returnData = searchRequestHandler.performGenericSearch(selOpt);
//		
//		//GenericSearchSearchCommand itemSearchCommand = new GenericSearchSearchCommand(itemSelOpt);
//		
//		//GenericSearch searchRequest = bom.createGenericSearch();
//		//GenericSearchSelectOptions selOpt;
//		//GenericSearchSearchCommand searchOptions = new GenericSearchSearchCommand(selOpt);
//		
//			
//		//GenericSearchReturn returnData = searchRequest.performSearch(searchOptions);
//		
//		
//		//quotations, take new quotation concept into account
//		
//		// declarations for variables 
//		int releasedquota = 0;
//		int acceptedquota = 0;
//		int completedquota = 0;
//		int donequota = 0;
//		
//        // calculation of openquota (the same for old and new quotes)
//        documentListSelectorForm.setDocumentType(DocumentListFilter.SALESDOCUMENT_TYPE_QUOTATION);
//        documentListSelectorForm.setDocumentStatus(DocumentListFilter.SALESDOCUMENT_STATUS_OPEN);
//        documentListSelectorForm.setValiditySelection(DocumentListSelectorForm.PERIOD);
//        documentListSelectorForm.setPeriod(DocumentListSelectorForm.NOT_SPECIFIED);
//        documentListSelectorForm.setSalesSelectionStart(true);
//        
//        // imitate request because form bean is used  
//        documentListSelectorForm.validate(null, null);
//
//        
//        filter = documentListSelectorForm.getDocumentListFilter();
//        orderList.readOrderHeaders(soldToKey, shop, filter);
//        int openquota = orderList.getOrderCount();
//        request.setAttribute(RK_OPEN_QUOTA, Integer.toString(openquota));
//        
//        
//        // is shop param for new quotation concept set?
//        if (shop.isQuotationExtended()) {
//									        	
//        	// calculate released quotations
//			documentListSelectorForm.setDocumentType(DocumentListFilter.SALESDOCUMENT_TYPE_QUOTATION);
//	        documentListSelectorForm.setDocumentStatus(DocumentListFilter.SALESDOCUMENT_STATUS_RELEASED);
//    	    documentListSelectorForm.setValiditySelection(DocumentListSelectorForm.PERIOD);
//        	documentListSelectorForm.setPeriod(DocumentListSelectorForm.NOT_SPECIFIED);
//	        documentListSelectorForm.setSalesSelectionStart(true);
//
//	        // imitate request because form bean is used  
//    	    documentListSelectorForm.validate(null, null);
//
//        	filter = documentListSelectorForm.getDocumentListFilter();
//
//	        orderList.readOrderHeaders(soldToKey, shop, filter);
//    	    releasedquota = orderList.getOrderCount();
//        	request.setAttribute(RK_RELEASED_QUOTA, Integer.toString(releasedquota));
//
//			// not needed any more
//
////        	// calculate accepted quotations
////			documentListSelectorForm.setDocumentType(DocumentListFilter.SALESDOCUMENT_TYPE_QUOTATION);
////	        documentListSelectorForm.setDocumentStatus(DocumentListFilter.SALESDOCUMENT_STATUS_ACCEPTED);
////    	    documentListSelectorForm.setValiditySelection(documentListSelectorForm.PERIOD);
////        	documentListSelectorForm.setPeriod(documentListSelectorForm.LAST_WEEK);
////	        documentListSelectorForm.setSalesSelectionStart(true);
////
////        	filter = documentListSelectorForm.getDocumentListFilter();
////
////	        orderList.readOrderHeaders(user, shop, filter);
////    	    acceptedquota = orderList.getOrderCount();
////        	request.setAttribute(RK_ACCEPTED_QUOTA, Integer.toString(acceptedquota));
//
//
//			// done below, same for both
////        	// calculate completed quotations
////			documentListSelectorForm.setDocumentType(DocumentListFilter.SALESDOCUMENT_TYPE_QUOTATION);
////	        documentListSelectorForm.setDocumentStatus(DocumentListFilter.SALESDOCUMENT_STATUS_COMPLETED);
////    	    documentListSelectorForm.setValiditySelection(documentListSelectorForm.PERIOD);
////        	documentListSelectorForm.setPeriod(documentListSelectorForm.LAST_WEEK);
////	        documentListSelectorForm.setSalesSelectionStart(true);
////
////	        // imitate request because form bean is used  
////    	    documentListSelectorForm.validate(null, null);
////
////        	filter = documentListSelectorForm.getDocumentListFilter();
////
////	        orderList.readOrderHeaders(user, shop, filter);
////    	    donequota = orderList.getOrderCount();
////        	//request.setAttribute(RK_COMPLETED_QUOTA, Integer.toString(completedquota));
////            request.setAttribute(RK_DONE_QUOTA, Integer.toString(donequota));
//                
//        } //else {
//                       
//        	// calculation of donequota
//        	documentListSelectorForm.setDocumentType(DocumentListFilter.SALESDOCUMENT_TYPE_QUOTATION);
//        	documentListSelectorForm.setDocumentStatus(DocumentListFilter.SALESDOCUMENT_STATUS_COMPLETED);
//        	documentListSelectorForm.setValiditySelection(DocumentListSelectorForm.PERIOD);
//        	documentListSelectorForm.setPeriod(DocumentListSelectorForm.LAST_WEEK);
//        	documentListSelectorForm.setSalesSelectionStart(true);
//
//	        // imitate request because form bean is used  
//    	    documentListSelectorForm.validate(null, null);
//
//        	filter = documentListSelectorForm.getDocumentListFilter();
//
//        	orderList.readOrderHeaders(soldToKey, shop, filter);
//        	donequota = orderList.getOrderCount();
//        	request.setAttribute(RK_DONE_QUOTA, Integer.toString(donequota));
//        		
//      //  }
//        
//	// calculation of sum for quotations
//	
////        int sumorders = openorders + doneorders;	
////        request.setAttribute(RK_SUM_ORDERS, Integer.toString(sumorders));
//        
//        
//        
//        //if (shop.isQuotationExtended()) {
//        	
//        	//int sumquota = openquota + releasedquota + acceptedquota + donequota;	
//        	//request.setAttribute(RK_SUM_QUOTA, Integer.toString(sumquota));
//        	
//        	//new quotations
//        	
//        	
//        	
//        	
//        //} else {
//        	
//		// new quotations
//        documentListSelectorForm.setDocumentType(DocumentListFilter.SALESDOCUMENT_TYPE_QUOTATION);
//        documentListSelectorForm.setDocumentStatus("null");
//        documentListSelectorForm.setValiditySelection(DocumentListSelectorForm.PERIOD);
//        documentListSelectorForm.setPeriod(DocumentListSelectorForm.LAST_WEEK);
//        documentListSelectorForm.setSalesSelectionStart(true);
//        
//        // imitate request because form bean is used  
//        documentListSelectorForm.validate(null, null);
//        
//        filter = documentListSelectorForm.getDocumentListFilter();
//        orderList.readOrderHeaders(soldToKey, shop, filter);
//        int sumquota = orderList.getOrderCount();
//        request.setAttribute(RK_SUM_QUOTA, Integer.toString(sumquota));
//        	                        
//        //}
//        
//        //store docListSelector in session
//        session.setAttribute("DocumentListSelectorFormFA", (DocumentListSelectorForm) documentListSelectorForm.clone());
//        
//        log.exiting();
//        return mapping.findForward("orderlist");


      }

}
