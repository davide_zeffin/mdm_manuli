/*****************************************************************************
	Class:        CCHSetupStartAction
	Copyright (c) 2002, SAP AG, Germany, All rights reserved.
	Author        SAP

	$Revision: #2 $
	$DateTime: 2003/10/01 16:07:42 $ (Last changed)
	$Change: 151875 $ (changelist)
    
*****************************************************************************/
package com.sap.isa.isacore.action.iview;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.businessobject.order.DocumentListFilter;
import com.sap.isa.businessobject.order.OrderDataContainer;
import com.sap.isa.businessobject.order.OrderStatus;
import com.sap.isa.businessobject.order.PartnerList;
import com.sap.isa.businessobject.order.PartnerListEntry;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.businesspartner.businessobject.PartnerFunctionBase;
import com.sap.isa.businesspartner.businessobject.SoldFrom;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.actionform.order.DocumentListSelectorForm;


public class CCHSetupStartAction extends IsaCoreBaseAction {

    /**
     * Key to read sales partner from request
     */
    public static final String RK_CCHSG_SALESPARTNER = "cchsg_salespartner";
    /**
     * Key to read amount of open orders from request 
     */
    public static final String RK_CCHSG_OPENORDERS   = "cchsg_openorders";
	/**
	 * Key to read amount of all orders from last seven days from request 
	 */
	public static final String RK_CCHSG_ALLORDERSOFLAST7DAYS   = "cchsg_allorderoflast7days";
    /**
     * Key to read Portal Url from UserSession
     */
    public static final String SC_PORTALURL = "cchsg_portalurl";
	/**
	 * Key to read Navigation model from UserSession.<br>
     * -<code>page</code>   Send via request<br>
     * - <code>portal</code> Default and will not being send via request
	 */
	public static final String SC_NAVMODELVERSION = "cchsg_navmodelversion";
    /**
     * Key to read the Error Code.<br>
     * -<code>1000</code>   Partner isn't of type Sold From party<br> -
     * -<code>2000</code>   Shop forwading AND order hosting in
     * parallel is designated / activated (not supported yet)
     */
	public static final String RK_CCHSG_ERRORCODE = "cchsg_errorcode";
    
    
    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            BusinessObjectManager bom,
            IsaLocation log)
            throws CommunicationException {

        // Page that should be displayed next.
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        String forwardTo = null;
        
        // Take attributes from request and set to userSession
        if (request.getParameter("portalurl") != null) {
            userSessionData.setAttribute(SC_PORTALURL , ((String)request.getParameter("portalurl")));
		}
        if (request.getParameter("navmodelversion") != null) {
        	userSessionData.setAttribute(SC_NAVMODELVERSION, request.getParameter("navmodelversion"));
        }

        Shop shop = bom.getShop();

        // get Sales Partner (Reseller, Dealer, SoldFrom, ...) from BuPaMa
        BusinessPartnerManager buPaMa = bom.createBUPAManager();
        BusinessPartner buPa = buPaMa.getDefaultBusinessPartner(PartnerFunctionBase.SOLDFROM);

        String shopCall = null;
        String orderHosting = null;
        SoldFrom soldFrom = null;

        if (buPa != null) {
            soldFrom = (SoldFrom)buPa.getPartnerFunctionObject(PartnerFunctionBase.SOLDFROM);
            soldFrom.readData(buPa.getTechKey(),
                              shop.getLanguage(), shop.getSalesOrganisation(), 
                              shop.getDistributionChannel(), shop.getDivision());

            // Separate interesting status flags and convert to integer for easier analysing
            
            shopCall = ( soldFrom.getShopCall() != null  &&  !soldFrom.getShopCall().equals("") )  
                         ? soldFrom.getShopCall() : "0";
            orderHosting = ( soldFrom.getOrderHosting() != null  &&  !soldFrom.getOrderHosting().equals("") ) 
                         ? soldFrom.getOrderHosting() : "0";
		} else {
			shopCall = "0";
			orderHosting = "0";
        }
        // shopCall = (request.getParameter("sc") != null ? request.getParameter("sc") : shopCall);  
        // orderHosting = (request.getParameter("oh") != null ? request.getParameter("oh") : orderHosting);
        if ( (shopCall.equals("1") && orderHosting.equals("1")) ||
             (shopCall.equals("1") && orderHosting.equals("2")) ||
             (shopCall.equals("2") && orderHosting.equals("1")) ||
             (shopCall.equals("2") && orderHosting.equals("2")) ) {
            // a combination of ShopCall (Basket Forwarding) and OrderHosting isn't
            // possible yet.
			request.setAttribute(RK_CCHSG_ERRORCODE, "2000");
			forwardTo = "errorpage";
        }
        if (forwardTo == null) {
            // illegal combinations has been excluded, now check which page has to be shown
            if (shopCall.equals("0") && orderHosting.equals("0")) {
                forwardTo = "entry";
            }
            if (shopCall.equals("1")    ||
                shopCall.equals("2")    ||
                orderHosting.equals("1")  ) {
                forwardTo = "designetedactivated";
            }
            if (orderHosting.equals("2")) {
                int notCompletedOrders = readNotCompletedOrders(buPa, bom);
                int allOrdersOfLast7days = readAllOrdersOfLastXDays(buPa, bom, 7);
                request.setAttribute(RK_CCHSG_OPENORDERS, Integer.toString(notCompletedOrders));
                request.setAttribute(RK_CCHSG_ALLORDERSOFLAST7DAYS, Integer.toString(allOrdersOfLast7days));
                forwardTo = "oh_activated";
            }
        }
        // Set Sales Partner to request
        request.setAttribute(RK_CCHSG_SALESPARTNER, soldFrom);
		log.exiting();
        return mapping.findForward(forwardTo);
    }
    /**
     * For the primitiv document overview screen get all not completed orders
     *
     * @param bupa   for which the research should be performed
     */
     private int readNotCompletedOrders(BusinessPartner buPa,
                                        BusinessObjectManager bom)
                    throws CommunicationException {
        // read the shop data
        Shop shop = bom.getShop();
        if (shop == null) {
            throw new PanicException("shop.notFound");
        }
        // get sold to
        User user = bom.getUser();

        // Form-Bean creates the DocumentListFilter
        DocumentListSelectorForm
        	documentListSelectorForm = new DocumentListSelectorForm();

        documentListSelectorForm.setDocumentType(DocumentListFilter.SALESDOCUMENT_TYPE_ORDER);
        documentListSelectorForm.setDocumentStatus(DocumentListFilter.SALESDOCUMENT_STATUS_OPEN);
        documentListSelectorForm.setValiditySelection(DocumentListSelectorForm.PERIOD);
        documentListSelectorForm.setPeriod(DocumentListSelectorForm.NOT_SPECIFIED);
        documentListSelectorForm.setSalesSelectionStart(true);

        documentListSelectorForm.validate(null, null);

        DocumentListFilter filter = documentListSelectorForm.getDocumentListFilter();

        OrderStatus orderList = bom.getOrderStatus();
        if (orderList == null) {
          orderList = bom.createOrderStatus(new OrderDataContainer()); 
        }


        // take aware of reseller sceanrio
        orderList.setMultiPartnerScenario(true);

        // handle the partner list in this case
        PartnerList partnerList = orderList.getPartnerList();

        // add Sales Partner (Reseller, Dealer, SoldFrom, ... to the partner list
        if (buPa != null) {
            PartnerListEntry partner = new PartnerListEntry();
            partner.setPartnerId(buPa.getId());
            partner.setPartnerTechKey(buPa.getTechKey());
            partnerList.setPartnerData(PartnerFunctionBase.SOLDFROM, partner);
        }

        // Get Salesdocuments headers (for calculation of openorders)
        orderList.readOrderHeaders(new TechKey(""), shop, filter);  // NO SOLDTO !!
        int openorders = orderList.getOrderCount();
         return openorders;
     }
	/**
	 * For the primitiv document overview screen get all orders created in the
	 * last X days
	 *
	 * @param bupa   for which the research should be performed
	 */
	 private int readAllOrdersOfLastXDays(BusinessPartner buPa,
							  	            BusinessObjectManager bom,
							  	            int daysToSelect)
					throws CommunicationException {
		// read the shop data
		Shop shop = bom.getShop();
		if (shop == null) {
			throw new PanicException("shop.notFound");
		}
		// get sold to
		User user = bom.getUser();

		// Form-Bean creates the DocumentListFilter
		DocumentListSelectorForm
			documentListSelectorForm = new DocumentListSelectorForm();

		documentListSelectorForm.setDocumentType(DocumentListFilter.SALESDOCUMENT_TYPE_ORDER);
		documentListSelectorForm.setDocumentStatus(DocumentListFilter.SALESDOCUMENT_STATUS_ALL);
		documentListSelectorForm.setValiditySelection(DocumentListSelectorForm.PERIOD);
		documentListSelectorForm.setPeriod(Integer.toString(daysToSelect));
		documentListSelectorForm.setSalesSelectionStart(true);

		documentListSelectorForm.validate(null, null);

		DocumentListFilter filter = documentListSelectorForm.getDocumentListFilter();

		OrderStatus orderList = bom.getOrderStatus();
		if (orderList == null) {
		  orderList = bom.createOrderStatus(new OrderDataContainer());
		}


		// take aware of reseller sceanrio
		orderList.setMultiPartnerScenario(true);

		// handle the partner list in this case
		PartnerList partnerList = orderList.getPartnerList();

		// add Sales Partner (Reseller, Dealer, SoldFrom, ... to the partner list
		if (buPa != null) {
			PartnerListEntry partner = new PartnerListEntry();
			partner.setPartnerId(buPa.getId());
			partner.setPartnerTechKey(buPa.getTechKey());
			partnerList.setPartnerData(PartnerFunctionBase.SOLDFROM, partner);
		}

		// Get Salesdocuments headers (for calculation of openorders)
		orderList.readOrderHeaders(new TechKey(""), shop, filter);  // NO SOLDTO !!
		 return orderList.getOrderCount();
	 }
}

