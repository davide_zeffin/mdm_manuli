/*****************************************************************************
	Class:        CCHSalesPartnerStatusChangeAction
	Copyright (c) 2002, SAP AG, Germany, All rights reserved.
	Author        SAP

	$Revision: #2 $
	$DateTime: 2003/10/01 16:07:42 $ (Last changed)
	$Change: 151875 $ (changelist)
    
*****************************************************************************/
package com.sap.isa.isacore.action.iview;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.businesspartner.businessobject.PartnerFunctionBase;
import com.sap.isa.businesspartner.businessobject.SoldFrom;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 * Action to set a business partner (in role SoldFrom) active for Shop Call
 * (Basket forwarding) or Order Hosting process.
 */
public class CCHSalesPartnerStatusChangeAction extends IsaCoreBaseAction {

    public static final String RK_CCHSC_ERROROCCURED = "cchsc_erroroccured";

    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            BusinessObjectManager bom,
            IsaLocation log)
            throws CommunicationException {

        // Page that should be displayed next.
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        String forwardTo = null;

        // get Sales Partner (Reseller, Dealer, SoldFrom, ...) from BuPaMa
        BusinessPartnerManager buPaMa = bom.createBUPAManager();
        BusinessPartner buPa = buPaMa.getDefaultBusinessPartner(PartnerFunctionBase.SOLDFROM);

        SoldFrom soldFrom = (SoldFrom)buPa.getPartnerFunctionObject(PartnerFunctionBase.SOLDFROM);

        Shop shop = bom.getShop();

        String returncode = null;
        if (soldFrom.getShopCall() != null  &&
            soldFrom.getShopCall().equals("1")) {
            // Activate Business Partner
            returncode = soldFrom.activateForShopCall(buPa.getTechKey(),
                                         shop.getSalesOrganisation(),
                                         shop.getDistributionChannel(),
                                         shop.getDivision());
        }
        if (soldFrom.getOrderHosting() != null  &&
            soldFrom.getOrderHosting().equals("1")) {
            // Activate Business Partner
            returncode = soldFrom.activateForOrderHosting(buPa.getTechKey(),
                                         shop.getSalesOrganisation(),
                                         shop.getDistributionChannel(),
                                         shop.getDivision());
        }
        // Check returncode
        if (returncode != null) {
            request.setAttribute(RK_CCHSC_ERROROCCURED, returncode);
        }
        log.exiting();
        return mapping.findForward("shownextpage");
    }
}

