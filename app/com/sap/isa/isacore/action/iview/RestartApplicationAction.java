package com.sap.isa.isacore.action.iview;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.businessobject.webcatalog.CatalogBusinessObjectManager;
import com.sap.isa.businesspartner.backend.boi.BusinessPartnerData;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.Agent;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.businesspartner.businessobject.PartnerFunctionBase;
import com.sap.isa.businesspartner.businessobject.Reseller;
import com.sap.isa.businesspartner.businessobject.ResponsibleAtPartner;
import com.sap.isa.businesspartner.businessobject.SoldFrom;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.Constants;
import com.sap.isa.core.ContextConst;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.interaction.InteractionConfig;
import com.sap.isa.core.interaction.InteractionConfigContainer;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.StartupParameter;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.isacore.MessageDisplayer;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.action.IsaCoreInitAction;
import com.sap.isa.isacore.action.ShopReadAction;
import com.sap.isa.isacore.action.b2b.ReadCatalogAction;
import com.sap.isa.isacore.action.b2b.ReadSoldToAction;
import com.sap.isa.isacore.action.oci.OciGetCatalogURLAction;
/**
 * This class simulates the whole start up process after following
 * parameters are reset: (Shop, SoldTo and catalog
 **/
public class RestartApplicationAction extends IsaCoreBaseAction {
	/**
	 * Implement this method to add functionality to your action.
	 *
	 * @param form              The <code>FormBean</code> specified in the
	 *                          config.xml file for this action
	 * @param request           The request object
	 * @param response          The response object
	 * @param userSessionData   Object wrapping the session
	 * @param requestParser     Parser to simple retrieve data from the request
	 * @param bom               Reference to the BusinessObjectManager
	 * @param log               Reference to the IsaLocation, needed for logging
	 * @return Forward to another action or page
	 */
	public ActionForward isaPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		BusinessObjectManager bom,
		IsaLocation log)
		throws CommunicationException {

		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
		String forwardTo = "success";
		boolean isDebugEnabled = log.isDebugEnabled();
		// here will simulate all the startup process
		// Create TechKey for the selected shop
		String shopId = (String) request.getAttribute(Constants.PARAM_SHOP);
		com.sap.isa.core.util.StartupParameter startupParameterCore =
			(
				com
					.sap
					.isa
					.core
					.util
					.StartupParameter) userSessionData
					.getAttribute(
				SessionConst.STARTUP_PARAMETER);

		if (shopId != null && !shopId.equals("")) {
			shopId = shopId.toUpperCase();
		}

		String soldTo = (String) request.getAttribute(Constants.PARAM_SOLDTO);
		//if (soldTo != null && !soldTo.equals("")) {
		//}

		String catalogId =
			(String) request.getAttribute(Constants.PARAM_CATALOG);

		TechKey shopKey = new TechKey(shopId);
		Shop shop = null;
		bom.releaseShop();

		//			if (shop != null) {
		//				if (shopKey.equals(shop.getTechKey())) {
		//					log.exiting();
		//					return mapping.findForward(FORWARD_SUCCESS);
		//				}
		//				else {
		//					MessageDisplayer messageDisplayer = new MessageDisplayer("shopexist",true);
		//					messageDisplayer.addMessage(new Message(Message.ERROR,"shop.existAlready"));
		//					messageDisplayer.addToRequest(request);
		//					log.exiting();
		//					return mapping.findForward("shopexist");
		//				}
		//			}

		// Ask the bom to create a shop with the given key
		shop = bom.createShop(shopKey); // May throw CommunicationException

		// check if shop is null
		if (shop == null) {
			// create invalid shop
			shop = new Shop();
			shop.addMessage(new Message(Message.ERROR, "shop.notFound"));
		}

		if (!shop.isValid()) {
			MessageDisplayer messageDisplayer =
				createShopErrorMessage(request, userSessionData, shopKey);
			messageDisplayer.copyMessages(shop);
			return mapping.findForward("message");
		}

		// get the scenario form the XCM.
		InteractionConfigContainer icc = this.getInteractionConfig(request);
		InteractionConfig interactionConfig = icc.getConfig("shop");

		InteractionConfig uiInteractionConfig = null;
		if (null != icc)
			uiInteractionConfig = icc.getConfig("ui");

		String scenario = interactionConfig.getValue("shopscenario");

		if (!checkScenario(shop, scenario)) {
			MessageDisplayer messageDisplayer =
				createShopErrorMessage(request, userSessionData, shopKey);
			messageDisplayer.addMessage(
				new Message(Message.ERROR, "shop.wrongScenario"));
			log.exiting();
			return mapping.findForward("message");
		}

		// debug:start
		if (log.isDebugEnabled()) {
			log.debug(shop);
		}
		// debug:end

		// set web application
		// the parameter in the web.xml calls scenario of historical reasons
		shop.setApplication(getInitParameter(ContextConst.IC_SCENARIO));

		// can products which are not in the catalog, but known in the backend be ordered?
		String orderingOfNonCataloProductsAllowed = null;
		if (null != uiInteractionConfig)
			orderingOfNonCataloProductsAllowed =
				uiInteractionConfig.getValue(
					ContextConst.IC_ENABLE_NON_CATALOG_PRODUCTS);

		if (orderingOfNonCataloProductsAllowed != null
			&& orderingOfNonCataloProductsAllowed.equalsIgnoreCase("true")) {
			shop.setOrderingOfNonCatalogProductsAllowed(true);
		} else {
			shop.setOrderingOfNonCatalogProductsAllowed(false);
		}

		userSessionData.setAttribute(
			ShopReadAction.NON_CATALOG_PRODUCTS_ALLOWED,
			new Boolean(shop.isOrderingOfNonCatalogProductsAllowed()));
		// EAuction is not supported
		//shop.setEAuctionUsed(false);
		String pricingConditions = "";
		if (null != uiInteractionConfig)
			pricingConditions =
				uiInteractionConfig.getValue(
					ContextConst.IC_ENABLE_PRICE_ANALYSIS);

		if (pricingConditions != null
			&& pricingConditions.equalsIgnoreCase("true")) {
			shop.setPricingCondsAvailable(true);
		} else {
			shop.setPricingCondsAvailable(false);
		}

		// are contracts allowed?
		userSessionData.setAttribute(
			ShopReadAction.CONTRACT_NOT_ALLOWED,
			new Boolean(!shop.isContractAllowed()));

		// are contract negotiations allowed?
		userSessionData.setAttribute(
			ShopReadAction.CONTRACT_NEGOTIATION_NOT_ALLOWED,
			new Boolean(!shop.isContractNegotiationAllowed()));
		// process types for contract negotiations    
		/*          moved to InitB2BAction
					if (shop.isContractNegotiationAllowed()) 
						userSessionData.setAttribute(CONTRACT_NEGOTIATION_PROCESS_TYPES, shop.getContractNegotiationProcessTypes());    
		*/
		// are quotations allowed?
		userSessionData.setAttribute(
			ShopReadAction.QUOTATION_NOT_ALLOWED,
			new Boolean(!shop.isQuotationAllowed()));

		// are templates allowed?
		userSessionData.setAttribute(
			ShopReadAction.TEMPLATE_NOT_ALLOWED,
			new Boolean(!shop.isTemplateAllowed()));

		IsaCoreInitAction.StartupParameter startupParameter =
			(IsaCoreInitAction.StartupParameter) userSessionData.getAttribute(
				IsaCoreInitAction.SC_STARTUP_PARAMETER);

		if (startupParameter.getPortal().equals("YES")) {
			// no profile maintenance in the portal
			shop.setUserProfileAvailable(false);
		}

		shop.setCampaignKey(startupParameter.getCampaignKey());

		/// end up reading Shop information, now read some catalog parameter stuff
		// get all the stuff form the XCM.
		//InteractionConfig interactionConfig = getInteractionConfig(request).getConfig("oci");
		String ociUrl = null;
		String enable = interactionConfig.getValue("ociEnable");
		String oci_readAllProductData = "";
		String oci_allProductsInMaterialMaster = "";

		if ((enable != null) && (enable.equalsIgnoreCase("true"))) {

			String externalCatalogUrl =
				interactionConfig.getValue("ociCatalogURL");
			String oci_target = interactionConfig.getValue("ociTarget");
			String oci_version = interactionConfig.getValue("ociVersion");
			String oci_form = interactionConfig.getValue("ociForm");

			oci_readAllProductData =
				interactionConfig.getValue("ociReadAllProductData");
			oci_allProductsInMaterialMaster =
				interactionConfig.getValue("ociAllProductsInMaterialMaster");

			if (isDebugEnabled) {
				log.debug(
					"external catalog URL in XCM: "
						+ externalCatalogUrl
						+ " ociTarget: "
						+ oci_target
						+ " ociVersion: "
						+ oci_version
						+ " ociForm: "
						+ oci_form
						+ "  ### and the other parameters ociReadAllProductData: "
						+ oci_readAllProductData
						+ " ociAllProductsInMaterialMaster: "
						+ oci_allProductsInMaterialMaster);
			}

			// construct the catalog-url-suffix from the arguments
			String completeURL =
				WebUtil.getAppsURL(
					getServlet().getServletContext(),
					request,
					response,
                    new Boolean(request.isSecure()),
					OciGetCatalogURLAction.oci_hook_action,
					null,
					null,
					true);
			ociUrl =
				externalCatalogUrl
					+ "?"
					+ OciGetCatalogURLAction.hook_action_name
					+ "="
					+ completeURL;

			ociUrl += "&"
				+ OciGetCatalogURLAction.target_name
				+ "="
				+ oci_target;
			ociUrl += "&"
				+ OciGetCatalogURLAction.version_name
				+ "="
				+ oci_version;
			ociUrl += "&" + OciGetCatalogURLAction.form_name + "=" + oci_form;
			ociUrl += "&"
				+ OciGetCatalogURLAction.caller_name
				+ "="
				+ OciGetCatalogURLAction.oci_caller;
			ociUrl += "&"
				+ OciGetCatalogURLAction.okcode_name
				+ "="
				+ OciGetCatalogURLAction.oci_okcode;

		}

		// user exit, to modify the OCI catalog URL, if necessary
		String catalogUrl = ociUrl;
		//customerExitGetOCIURL(requestParser, request, userSessionData, bom, ociUrl);

		// user exit, to modify forward, if necessary
		//customerForwardTo = customerExitDispatcher(requestParser, request, userSessionData, bom, catalogUrl);
		//forwardTo = (customerForwardTo == null) ? forwardTo : customerForwardTo;

		if (shop != null) {

			if ((enable != null) && enable.equalsIgnoreCase("true")) {

				shop.setOciAllowed(true);

				if (catalogUrl != null) {
					shop.setExternalCatalogURL(catalogUrl);
				}

				if ((oci_readAllProductData != null)
					&& (oci_readAllProductData.equalsIgnoreCase("true"))) {
					shop.setProductInfoFromExternalCatalogAvailable(true);
				}

				if ((oci_allProductsInMaterialMaster != null)
					&& (oci_allProductsInMaterialMaster
						.equalsIgnoreCase("true"))) {
					shop.setAllProductInMaterialMaster(true);
				}

			}

		}
		// not get soldTo stuff
		User user = bom.getUser();

		BusinessPartnerManager buPaMa = bom.createBUPAManager();

		String soldToTechKeyValue = null;
		// actual user data until all getSoldTo calls are erased
		// get the soldto techkey
		BusinessPartner bpSoldTo = buPaMa.getBusinessPartner(soldTo);
		
		TechKey soldToTechKey = bpSoldTo.getTechKey();
		ResultData soldToList = null;

		if (soldToList == null) {
			soldToList =
				(ResultData) userSessionData.getAttribute(
					ReadSoldToAction.RK_SOLDTO_LIST);
		}
		if (soldToList == null) {
			soldToList =
				(ResultData) request.getAttribute(
					ReadSoldToAction.RK_SOLDTO_LIST);
		}

		soldToList.first();
		if (soldToList
			.searchRowByColumn(
				BusinessPartnerData.SOLDTO_TECHKEY,
				soldToTechKey.getIdAsString())) {
			if (soldToList
				.getString(BusinessPartnerData.SOLDTO)
				.equals(soldTo)) {

				BusinessPartner partner =
					buPaMa.createBusinessPartner(soldToTechKey, soldTo, null);

				PartnerFunctionBase partnerFunction = null;

				if (shop
					.getCompanyPartnerFunction()
					.equals(PartnerFunctionBase.RESELLER)) {
					partnerFunction = new Reseller();

					BusinessPartner respPartner =
						buPaMa.getDefaultBusinessPartner(
							PartnerFunctionData.CONTACT);

					// contact should not be transfered to the order
					buPaMa.resetDefaultBusinessPartner(
						PartnerFunctionBase.CONTACT);

					ResponsibleAtPartner responsibleAtPartnerPF =
						new ResponsibleAtPartner();
					respPartner.addPartnerFunction(responsibleAtPartnerPF);

					buPaMa.setDefaultBusinessPartner(
						respPartner.getTechKey(),
						responsibleAtPartnerPF.getName());

				}
				//check if it is an agent (relevant for BOB scenario)
				else if (
					shop.getCompanyPartnerFunction().equals(
						PartnerFunctionBase.AGENT)) {
					partnerFunction = new Agent();

					// contact should not be transfered to the order
					buPaMa.resetDefaultBusinessPartner(
						PartnerFunctionBase.CONTACT);
				} else {
					partnerFunction =
						new com.sap.isa.businesspartner.businessobject.SoldTo();
					//LoginEvent event = new LoginEvent(user);
					//eventHandler.fireLoginEvent(event);
				}

				partner.addPartnerFunction(partnerFunction);
				buPaMa.setDefaultBusinessPartner(
					partner.getTechKey(),
					partnerFunction.getName());

				if (shop.isHomActivated()) {
					// In the pom scenario: add also the partner function soldfrom
					partnerFunction = new SoldFrom();
					partner.addPartnerFunction(partnerFunction);
					buPaMa.setDefaultBusinessPartner(
						partner.getTechKey(),
						partnerFunction.getName());
				}

				// set the partner as alternative partner in marketing functions
				user.getMktPartner().setAlternativePartner(soldToTechKey);
			}
		} //////////////////////////////////////////////////////////////////

        String[] views = null;

        // set the catalog views
        RequestParser.Parameter viewParameter =
            requestParser.getParameter(Constants.PARAM_VIEWS);
        if (viewParameter.isSet()) {
            views = new String[viewParameter.getNumValues()];
            for (int i = 0; i < viewParameter.getNumValues(); i++) {
                views[i] = viewParameter.getValue(i).getString();
            }
        } else {
            views =
                (String[]) request.getAttribute(
                    ReadCatalogAction.PARAM_VIEW);
        }

        if (log.isDebugEnabled()) {
            if (views == null) {
                log.debug("no views available.");
            }
            else {
                log.debug("available views: ");
                for (int i = 0; i < views.length; i++) {
                    log.debug(views[i]);
                }
            }
        }
        shop.setViews(views);

		/// now read catalog stuff
		if (shop.isCatalogDeterminationActived()) {
			CatalogBusinessObjectManager cbom =
				getCatalogBusinessObjectManager(userSessionData);

			// check if the catalog parameter exist
			//		if (catalogValue.isSet()) {
			String catalogKey = catalogId;
            
            WebCatInfo theCatalog = cbom.getCatalog();

			if (theCatalog != null) {
				if (catalogKey.equals(theCatalog.getCatalog().getGuid())) {
					log.exiting();
					return mapping.findForward("success");
				} else {
					MessageDisplayer messageDisplayer =
						new MessageDisplayer("catalogexist", true);
					messageDisplayer.addMessage(
						new Message(Message.ERROR, "b2b.catalog.existAlready"));
					messageDisplayer.addToRequest(request);
                    forwardTo = "catalogexist";
				}
			}

			shop.readOrgData(catalogKey);

		}
		////
		
		log.exiting();
		return mapping.findForward(forwardTo);
	}

	private boolean checkScenario(Shop shop, String scenario) {

		boolean check = shop.getScenario().equals(scenario);

		if (!check
			&& shop.getScenario().equals("B2BC")
			&& (scenario.equals("B2B") || scenario.equals("B2C"))) {
			check = true;
		}
		return check;
	}

	private MessageDisplayer createShopErrorMessage(
		HttpServletRequest request,
		UserSessionData userSessionData,
		TechKey shopKey) {

		StartupParameter startupParameter =
			(StartupParameter) userSessionData.getAttribute(
				SessionConst.STARTUP_PARAMETER);

		startupParameter.getParameter("shop").setReentry(false);
		String args[] = { shopKey.getIdAsString()};
		MessageDisplayer messageDisplayer = new MessageDisplayer();
		messageDisplayer.addToRequest(request);
		messageDisplayer.setOnlyLogin();
		messageDisplayer.addMessage(
			new Message(Message.ERROR, "shop.loadError", args, null));
		return messageDisplayer;
	}

}
