/*****************************************************************************
	Class:        PrepareSettingAction
	Copyright (c) 2002, SAPLabs, Palo Alto, All rights reserved.
	Author        SAP

	$Revision: #1 $

*****************************************************************************/

package com.sap.isa.isacore.action.iview;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.core.Constants;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.StartupParameter.Parameter;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.action.IsaCoreInitAction;
import com.sap.isa.isacore.action.b2b.ReadCatalogAction;
/**
 * This class retrieves the default WebShop, SoldTo and catalog and
 * Set to http request
 */
public class PrepareSettingAction extends IsaCoreBaseAction {
	/**
	 * Implement this method to add functionality to your action.
	 *
	 * @param form              The <code>FormBean</code> specified in the
	 *                          config.xml file for this action
	 * @param request           The request object
	 * @param response          The response object
	 * @param userSessionData   Object wrapping the session
	 * @param requestParser     Parser to simple retrieve data from the request
	 * @param bom               Reference to the BusinessObjectManager
	 * @param log               Reference to the IsaLocation, needed for logging
	 * @return Forward to another action or page
	 */
	public static final String RQ_SOLDTO = "soldTo";
	public static final String RQ_CATALOG = "webCatalog";
	public static final String RQ_WEBSHOP = "webShopId";
	public static final String RQ_ALL_WEBSHOPS = "allShops";
	public static final String RQ_ALL_SOLDTOS = "allsoldTos";
	public static final String RQ_ALL_CATALOGS = "allCatalogs";
	public static final String RQ_DISPLAY_MODE = "display";
	public ActionForward isaPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		BusinessObjectManager bom,
		IsaLocation log)
		throws CommunicationException {
			
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
		String forwardTo = "success";
		String soldTo = "";
		String webCatalog = "";
		String shopId = "";
		boolean isDebugEnabled = log.isDebugEnabled();

		// Find out if we run in a portal.
		IsaCoreInitAction.StartupParameter startupParameter =
			(IsaCoreInitAction.StartupParameter) userSessionData.getAttribute(
				IsaCoreInitAction.SC_STARTUP_PARAMETER);

		String displayOnly = requestParser.getParameter(RQ_DISPLAY_MODE).
									getValue().getString();

		User user = bom.getUser();
		Shop shop = bom.getShop();
		shopId = (String)request.getAttribute(Constants.PARAM_SHOP);
		if ((shopId == null) || (shopId.length()<1)){
			shopId = shop.getId();
		}
		com.sap.isa.core.util.StartupParameter startupParameterCore =
			(com.sap.isa.core.util.StartupParameter)
			userSessionData.getAttribute(SessionConst.STARTUP_PARAMETER);
		Parameter soldToPar = 
				startupParameterCore.getParameter(Constants.PARAM_SOLDTO);
		if(soldToPar != null){		
			request.setAttribute(Constants.PARAM_SOLDTO, soldToPar.getValue());	
		}
		
		Parameter catalogPar = 
				startupParameterCore.getParameter(Constants.PARAM_CATALOG);
		if(catalogPar != null){		
			request.setAttribute(Constants.PARAM_CATALOG, catalogPar.getValue());	
		}
        Parameter viewsPar = 
                startupParameterCore.getParameter(Constants.PARAM_VIEWS);
        if(viewsPar != null && viewsPar.getValue()!=null && viewsPar.getValue().length() != 0){
            String viewsList[] = split(viewsPar.getValue(), Constants.PARAM_SEPARATOR);     
            request.setAttribute(ReadCatalogAction.PARAM_VIEW, viewsList);   
        }
		//get soldTo and catalog information and set to request.
//		soldTo =
//			bom
//				.getBUPAManager()
//				.getBusinessPartner(PartnerFunctionData.SOLDTO)
//				.getId();
//		CatalogBusinessObjectManager catalogBOM =
//			(CatalogBusinessObjectManager) userSessionData.getBOM(
//				CatalogBusinessObjectManager.CATALOG_BOM);
		user.clearMessages();
	
		request.setAttribute(BusinessObjectBase.CONTEXT_NAME, user);
		//request.setAttribute(RQ_SOLDTO, soldTo);
		request.setAttribute(Constants.PARAM_SHOP, shopId);
		request.setAttribute(RQ_DISPLAY_MODE, displayOnly);
		log.exiting();
		return mapping.findForward(forwardTo);
	}
    /**
     * @param string
     * @return
     */
    private String[] split(String string, String separator) {
        return string.split(separator);
    }

}
