/*****************************************************************************
    Class:        DocumentOverviewListParameterAction
    Copyright (c) 2002, SAP AG, Germany, All rights reserved.
    Author        Timo Kohr

    $DateTime: 2002/10/23 20:30:00 $ (Last changed)
    $Change: 89788 $ (changelist)

*****************************************************************************/
package com.sap.isa.isacore.action.iview;

import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Search;
import com.sap.isa.businessobject.SearchResult;
import com.sap.isa.businessobject.ServiceSearchCommand;
import com.sap.isa.businessobject.billing.BillingStatus;
import com.sap.isa.businessobject.header.HeaderBillingDocument;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.order.DocumentListFilter;
import com.sap.isa.businessobject.order.OrderDataContainer;
import com.sap.isa.businessobject.order.OrderStatus;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.businesspartner.businessobject.PartnerFunctionBase;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.core.util.table.TableRow;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.action.order.DocumentListAction;
import com.sap.isa.isacore.actionform.iview.DocumentListiViewParameterForm;

/**
 *  Document overview
 *
 */
public class DocumentOverviewListParameterAction extends IsaCoreBaseAction {

    // CONSTANTS
    /**
     * Defines session object for document type
     */
    public final static String SC_DOCTYPE = "iviewDO_doc_type";
    /**
     * Defines session object for fast refresh
     */
    public final static String SC_IVIEW_FASTREFRESH_LIST = "iviewDO_fastrefresh";
    public final static String RC_DOCSORIGIN = "documentsorigin";
    public final static String RK_DOCLIST = "documentoverviewresultlist";
    public final static String RC_SERVICEBUSINESSTYPE = "servicebusinesstype";

    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            BusinessObjectManager bom,
            IsaLocation log)
            throws CommunicationException {

		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        // Get Selectquery from Paramter
        String docType = request.getParameter(DocumentListiViewParameterForm.IVIEW_DOCUMENT_TYPE);
        if (docType == null || docType.equals("")) {
          docType = (String) userSessionData.getAttribute(ReadiViewParameterAction.SC_DOCTYPE);
        }


        // FOR WHAT DOCUMENT TYPE DO WE RUN
        if (docType.equals(DocumentListiViewParameterForm.ORDER)         ||
            docType.equals(DocumentListiViewParameterForm.ORDERTEMPLATE) ||
            docType.equals(DocumentListiViewParameterForm.QUOTATION))    {
                // Sales documents
                IsaSalesSelection(request, response, bom, userSessionData, docType);
        }
        else if (docType.equals(DocumentListiViewParameterForm.INVOICE)         ||
                 docType.equals(DocumentListiViewParameterForm.DOWNPAYMENT) ||
                 docType.equals(DocumentListiViewParameterForm.CREDITMEMO))    {
                // Billing documents
                IsaBillingSelection(request, response, bom, userSessionData, docType);
        }
        else if (docType.equals(DocumentListiViewParameterForm.COMPLAINTS)   ||
                 docType.equals(DocumentListiViewParameterForm.SERVICECONTRACT)   ||
                 docType.equals(DocumentListiViewParameterForm.SERVICEREQUEST)   ||
                 docType.equals(DocumentListiViewParameterForm.INFOREQUEST)) {
                // service documents
                IsaServiceSelection(request, response, bom, userSessionData, getServiceBusinessType(docType));
        }
		log.exiting();
        return mapping.findForward("documentlist");

    }

    /**
     *
     */
    private void IsaSalesSelection(
            HttpServletRequest request,
            HttpServletResponse response,
            BusinessObjectManager bom,
            UserSessionData userSessionData,
            String docType)
            throws CommunicationException {


        ResultData salesDocTable;

        if  (( userSessionData.getAttribute(ReadiViewParameterAction.SC_SALES_REFRESH).equals("true")) ||
            (! docType.equals(userSessionData.getAttribute(ReadiViewParameterAction.SC_DOCTYPE)))) {
            // set the actual document type to the session
            userSessionData.setAttribute(ReadiViewParameterAction.SC_DOCTYPE, docType);
            userSessionData.setAttribute(ReadiViewParameterAction.SC_SALES_REFRESH, "false");

            DocumentListFilter salesDocListFilter = null;
            if (docType.equals("order")) {
                salesDocListFilter = (DocumentListFilter) userSessionData.getAttribute(ReadiViewParameterAction.SC_ORDER_FILTER);
            }
            else if (docType.equals("quotation")) {
                salesDocListFilter = (DocumentListFilter) userSessionData.getAttribute(ReadiViewParameterAction.SC_QUOTATION_FILTER);
            }
            else if (docType.equals("ordertemplate")) {
                salesDocListFilter = (DocumentListFilter) userSessionData.getAttribute(ReadiViewParameterAction.SC_ORDERTEMPLATE_FILTER);
            }

            OrderStatus orderStatus = bom.createOrderStatus(new OrderDataContainer());
            // Perform selection
            BusinessPartnerManager buPaMa = bom.createBUPAManager();    
            TechKey soldToKey = (buPaMa.getDefaultBusinessPartner(PartnerFunctionBase.SOLDTO) != null 
                                 ? buPaMa.getDefaultBusinessPartner(PartnerFunctionBase.SOLDTO).getTechKey()
                                 : new TechKey(""));
            orderStatus.readOrderHeaders(soldToKey, bom.getShop(), salesDocListFilter);

            // Transform list into Resultset to be conform to ICSS documents
            Table docTab = new Table("doctab");
            docTab.addColumn(Table.TYPE_STRING,"STATUS");
            docTab.addColumn(Table.TYPE_STRING,"DATE");
            docTab.addColumn(Table.TYPE_STRING,"DOCID");
            docTab.addColumn(Table.TYPE_STRING,"DESCRIPTION");

            for (Iterator it = orderStatus.iterator(); it.hasNext(); ) {
                // Get a header
                HeaderSalesDocument headerSalesDoc = (HeaderSalesDocument)it.next();
                // Put into Table
                TableRow docRow = docTab.insertRow();
                docRow.setRowKey(headerSalesDoc.getTechKey());
                docRow.getField(1).setValue((headerSalesDoc.getStatus() != null ? headerSalesDoc.getStatus() : ""));
                docRow.getField(2).setValue(headerSalesDoc.getChangedAt());
                docRow.getField(3).setValue(headerSalesDoc.getSalesDocNumber());
                if ( ! headerSalesDoc.getPurchaseOrderExt().equals("")) {
                    docRow.getField(4).setValue(headerSalesDoc.getPurchaseOrderExt());
                } else {
                    docRow.getField(4).setValue(headerSalesDoc.getDescription());
                }
            }
            salesDocTable = new ResultData(docTab);
        }
        else {
            salesDocTable = (ResultData) userSessionData.getAttribute(SC_IVIEW_FASTREFRESH_LIST);
        }

        // Set data to request
        request.setAttribute(DocumentListAction.RK_DOCUMENT_COUNT, new Integer(salesDocTable.getNumRows()).toString());
        request.setAttribute(DocumentListAction.RK_DLA_SHOP, bom.getShop());
        request.setAttribute(RK_DOCLIST, salesDocTable);
        // Set data to session
        userSessionData.setAttribute(SC_IVIEW_FASTREFRESH_LIST, salesDocTable);

    }

    /**
     *
     */
    private void IsaBillingSelection(
            HttpServletRequest request,
            HttpServletResponse response,
            BusinessObjectManager bom,
            UserSessionData userSessionData,
            String docType)
            throws CommunicationException {

      String documentsOrigin = null;
      ResultData billingDocTable;

      if  (( userSessionData.getAttribute(ReadiViewParameterAction.SC_BILLING_REFRESH).equals("true")) ||
          (! docType.equals(userSessionData.getAttribute(ReadiViewParameterAction.SC_DOCTYPE)))) {
            // set the actual document type to the session
            userSessionData.setAttribute(ReadiViewParameterAction.SC_DOCTYPE, docType);
            userSessionData.setAttribute(ReadiViewParameterAction.SC_BILLING_REFRESH, "false");

            DocumentListFilter billingDocListFilter = null;
            if (docType.equals("invoice")) {
                billingDocListFilter = (DocumentListFilter) userSessionData.getAttribute(ReadiViewParameterAction.SC_INVOICE_FILTER);
            }
            else if (docType.equals("creditmemo")) {
                billingDocListFilter = (DocumentListFilter) userSessionData.getAttribute(ReadiViewParameterAction.SC_CREDITMEMO_FILTER);
            }
            else if (docType.equals("downpayment")) {
                billingDocListFilter = (DocumentListFilter) userSessionData.getAttribute(ReadiViewParameterAction.SC_DOWNPAYMENT_FILTER);
            }

           BillingStatus billingStatus = bom.createBillingStatus();
           BusinessPartnerManager buPaMa = bom.createBUPAManager();    
           TechKey soldToKey = (buPaMa.getDefaultBusinessPartner(PartnerFunctionBase.SOLDTO) != null 
                                ? buPaMa.getDefaultBusinessPartner(PartnerFunctionBase.SOLDTO).getTechKey()
                                : new TechKey(""));
           // Perform selection
           billingStatus.readBillingHeaders(soldToKey, bom.getShop(), billingDocListFilter);

           // Transform list into Resultset to be conform to ICSS documents
           Table docTab = new Table("doctab");
  //           docTab.addColumn(Table.TYPE_STRING,"STATUS");
           docTab.addColumn(Table.TYPE_STRING,"DATE");
           docTab.addColumn(Table.TYPE_STRING,"DUEDATE");
           docTab.addColumn(Table.TYPE_STRING,"DOCID");
           docTab.addColumn(Table.TYPE_STRING,"GROSSVALUE");
           docTab.addColumn(Table.TYPE_STRING,"CURRENCY");
		   docTab.addColumn(Table.TYPE_STRING,"COMPANYCODE");

           for (Iterator it = billingStatus.iterator(); it.hasNext(); ) {
               // Get a header
               HeaderBillingDocument headerBillDoc = (HeaderBillingDocument)it.next();
               // Put into Table
               TableRow docRow = docTab.insertRow();
               docRow.setRowKey(new TechKey(headerBillDoc.getBillingDocNo()));
               //               docRow.getField(1).setValue(headerBillDoc.getStatus());           Not yes supported
               docRow.getField(1).setValue(headerBillDoc.getCreatedAt());
               docRow.getField(2).setValue(headerBillDoc.getDueAt());
               docRow.getField(3).setValue(headerBillDoc.getBillingDocNo());
               docRow.getField(4).setValue(headerBillDoc.getGrossValue());
               docRow.getField(5).setValue(headerBillDoc.getCurrency());
			   docRow.getField(6).setValue(headerBillDoc.getCompanyCode());
               if (  headerBillDoc.getBillingDocumentsOrigin() != null  &&
                   ! headerBillDoc.getBillingDocumentsOrigin().equals("")) {
                   documentsOrigin = headerBillDoc.getBillingDocumentsOrigin();
               }
           }
           billingDocTable = new ResultData(docTab);
        }
        else {
           billingDocTable = (ResultData) userSessionData.getAttribute(SC_IVIEW_FASTREFRESH_LIST);
        }

        // Set data to request
        request.setAttribute(DocumentListAction.RK_DOCUMENT_COUNT, new Integer(billingDocTable.getNumRows()).toString());
        request.setAttribute(DocumentListAction.RK_DLA_SHOP, bom.getShop());
        request.setAttribute(RK_DOCLIST, billingDocTable);
        request.setAttribute(RC_DOCSORIGIN, documentsOrigin);
        // Set data to session
        userSessionData.setAttribute(SC_IVIEW_FASTREFRESH_LIST, billingDocTable);


    }


    /**
     *
     */
    private void IsaServiceSelection(
            HttpServletRequest request,
            HttpServletResponse response,
            BusinessObjectManager bom,
            UserSessionData userSessionData,
            String docType)
            throws CommunicationException {


        ResultData serviceDocTable;
        //String serviceProcessType =  request.getParameter(RC_SERVICEBUSINESSTYPE);
        String serviceProcessType;

        if (( userSessionData.getAttribute(ReadiViewParameterAction.SC_SERVICE_REFRESH).equals("true")) ||
            (! docType.equals(userSessionData.getAttribute(ReadiViewParameterAction.SC_DOCTYPE)))) {
            // set the actual document type to the session
            userSessionData.setAttribute(ReadiViewParameterAction.SC_DOCTYPE, docType);
            userSessionData.setAttribute(ReadiViewParameterAction.SC_SERVICE_REFRESH, "false");

            DocumentListFilter serviceDocListFilter = null;
            if (docType.equals("S1")) {
                serviceDocListFilter = (DocumentListFilter) userSessionData.getAttribute(ReadiViewParameterAction.SC_SERVICEREQUEST_FILTER);
                //serviceProcessType="BUS2000116";
            }
            else if (docType.equals("S3")) {
                serviceDocListFilter = (DocumentListFilter) userSessionData.getAttribute(ReadiViewParameterAction.SC_INFOREQUEST_FILTER);
                //serviceProcessType="BUS2000116";
            }
            else if (docType.equals("CRMC")) {
                serviceDocListFilter = (DocumentListFilter) userSessionData.getAttribute(ReadiViewParameterAction.SC_COMPLAINTS_FILTER);
                //serviceProcessType="BUS2000120";
            }
            else if (docType.equals("SC")) {
                serviceDocListFilter = (DocumentListFilter) userSessionData.getAttribute(ReadiViewParameterAction.SC_SERVICECONTRACT_FILTER);
                //serviceProcessType="BUS2000112";
            }

            // Use the bom to retrieve a reference to the search object
            Search search = bom.createSearch();
            SearchResult result = null;
            BusinessPartnerManager buPaMa = bom.createBUPAManager();    
            TechKey soldToKey = (buPaMa.getDefaultBusinessPartner(PartnerFunctionBase.SOLDTO) != null 
                                 ? buPaMa.getDefaultBusinessPartner(PartnerFunctionBase.SOLDTO).getTechKey()
                                 : new TechKey(""));
            result = search.performSearch(new ServiceSearchCommand(soldToKey,
                                                                   serviceDocListFilter,
                                                                   docType,
                                                                   bom.getUser().getLanguage()));

            serviceDocTable = new ResultData(result.getTable());

        } else {  // from Fast refresh
            serviceDocTable = (ResultData) userSessionData.getAttribute(SC_IVIEW_FASTREFRESH_LIST);
        }

        request.setAttribute(DocumentListAction.RK_DOCUMENT_COUNT, new Integer(serviceDocTable.getNumRows()).toString());
        request.setAttribute(DocumentListAction.RK_DLA_SHOP, bom.getShop());
        request.setAttribute(RK_DOCLIST, serviceDocTable);
        // Set data to session
        userSessionData.setAttribute(SC_IVIEW_FASTREFRESH_LIST, serviceDocTable);

    }

    /**
     *
     */
    private String getServiceBusinessType(String docType) {

      if (docType.equals("SC")) {
        return DocumentListiViewParameterForm.SERVICECONTRACT;
      }
      if (docType.equals("S1")) {
        return DocumentListiViewParameterForm.SERVICEREQUEST;
      }
      if (docType.equals("S3")) {
        return DocumentListiViewParameterForm.INFOREQUEST;
      }
      if (docType.equals("CRMC")) {
        return DocumentListiViewParameterForm.COMPLAINTS;
      }

      return docType;
    }

}