package com.sap.isa.isacore.action.iview;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.MessageDisplayer;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.action.IsaCoreInitAction;

/**
 * 
 * This action class checks whether there is an open sales document
 * if so, the setting action will be aborted, else the resetting
 * web shop, soldto and catalog will be carried on.
 */
public class CheckOpenDocumentAction extends IsaCoreBaseAction {
	/**
	 * Implement this method to add functionality to your action.
	 *
	 * @param form              The <code>FormBean</code> specified in the
	 *                          config.xml file for this action
	 * @param request           The request object
	 * @param response          The response object
	 * @param userSessionData   Object wrapping the session
	 * @param requestParser     Parser to simple retrieve data from the request
	 * @param bom               Reference to the BusinessObjectManager
	 * @param log               Reference to the IsaLocation, needed for logging
	 * @return Forward to another action or page
	 */
	public ActionForward isaPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		BusinessObjectManager bom,
		IsaLocation log)
		throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
		String forwardTo = "success";
		boolean isDebugEnabled = log.isDebugEnabled();
		boolean isOpenDocument = false;
		// Find out if we run in a portal.
		IsaCoreInitAction.StartupParameter startupParameter =
			(IsaCoreInitAction.StartupParameter) userSessionData.getAttribute(
				IsaCoreInitAction.SC_STARTUP_PARAMETER);

		DocumentHandler documentHandler =
			(DocumentHandler) userSessionData.getAttribute(
				SessionConst.DOCUMENT_HANDLER);
		
		ManagedDocument	openDocument = (documentHandler == null) ? null : documentHandler.getManagedDocument(1);
		if (openDocument == null){ 
			//means no open document, then the action should continue
			return mapping.findForward("failure");
		}else{
			if ((openDocument.getDocNumber() == null) && 
				(openDocument.getDocNumber().equalsIgnoreCase(""))){
					return mapping.findForward("failure");			
				}
		}
		MessageDisplayer messageDisplayer = new MessageDisplayer("openDocument",true);
		messageDisplayer.addMessage(new Message(Message.ERROR,"portal.openDoc"));
		messageDisplayer.addToRequest(request);
		log.exiting();
		return mapping.findForward(forwardTo);
	}
}
