package com.sap.isa.isacore.action.iview;

// framework dependencies
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.User;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.MessageDocument;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.action.IsaCoreInitAction;

/**
 * Cross-Entry Action that creates a basket when "order" Button is pressed 
 * on the Account BSP in the partner portal.
 */
public class Account2BasketAction extends IsaCoreBaseAction {

     public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            BusinessObjectManager bom,
            IsaLocation log,
            IsaCoreInitAction.StartupParameter startupParameter,
            BusinessEventHandler eventHandler,
            boolean multipleInvocation,
            boolean browserBack)
            throws CommunicationException {

			final String METHOD_NAME = "isaPerform()";
			log.entering(METHOD_NAME);
          String forwardTo = null;
		  
		  // Request param techKey is the bupa id
          //String currentBupa = request.getParameter("TechKey");
          String currentBupa = request.getParameter("CRM_OBJECT_ID");


          if (currentBupa != null) {
            // get reference to bupa-manager
            BusinessPartnerManager bupaManager = bom.createBUPAManager();
            
            //get bupa            
            //BusinessPartner partner = bupaManager.getBusinessPartner(currentBupa);
            TechKey techkey = new TechKey(currentBupa);
			BusinessPartner partner = bupaManager.getBusinessPartner(techkey);
            partner.getAddress();

            User user = bom.getUser();
            
            DocumentHandler documentHandler = (DocumentHandler)
                userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
    		
            // does an editable document already exist?
            if (documentHandler.getDocument(DocumentState.TARGET_DOCUMENT) == null) {	
              //create a new basket

              //set request attribute, so that CreateBasketAction knows that the soldto shall be filled
              request.setAttribute("soldToIsSelectable","X");
              request.setAttribute("BupaKey", partner.getTechKey().getIdAsString());
              request.setAttribute("Account2Order", "X");
              request.setAttribute("BasketCrossEntry", "X");                                          	                                          	                                          	                                          	
              forwardTo = "newBasket";

            } else {
            	
              	//create new message Document and set the forwardJsp
    			
    			MessageDocument messageDoc =  new MessageDocument();
    			    			    			
    			messageDoc.setMessageJSP("/iviews/lead2basket/lead2Basket.jsp");    			    			    		    
    			documentHandler.add(messageDoc);
    			    			
    			messageDoc.setValue("nextAction", "/b2b/account2BasketDialogue.do");
				messageDoc.setValue("BupaKey", partner.getTechKey().getIdAsString());    			
    			    			    			
    			forwardTo = "showMessage";
    			    			            			
                                      		      				
            } //basket == null

          } // currentBupa != null
		  log.exiting();       
          return mapping.findForward(forwardTo);

	}

}