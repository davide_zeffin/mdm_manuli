/*
 * Created on 07.10.2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.isacore.action.iview;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.MessageDocument;

/**
 * @author d028980
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class Activity2BasketAction extends OneOrderDoc2BasketBaseAction {

	/* (non-Javadoc)
	 * @see com.sap.isa.isacore.action.iview.OneOrderDoc2BasketBaseAction#oneOrder2BasketPerform(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.sap.isa.isacore.DocumentHandler, com.sap.isa.core.logging.IsaLocation)
	 */
	protected String oneOrder2BasketPerform(
		HttpServletRequest request,
		HttpServletResponse response,
		DocumentHandler documentHandler,
		IsaLocation log)
		throws CommunicationException {
		// TODO Auto-generated method stub
		
		final String METHOD_NAME = "oneOrder2BasketPerform()";
		log.entering(METHOD_NAME);
		String forwardTo = null;
    		
		if (documentHandler.getDocument(DocumentState.TARGET_DOCUMENT) == null) {	
    		    
			request.setAttribute("activity2Basket","X");  
			request.setAttribute("BasketCrossEntry", "X");                                          	                                          	                                          	                                          	              
			forwardTo = "newBasket"; 		       		        		            	      		
    			    			
    			    			    			
		} else {
    			
			//create new message Document and set the forwardJsp
			MessageDocument messageDoc = new MessageDocument();
			messageDoc.setMessageJSP("/iviews/lead2basket/lead2Basket.jsp");    			    			    		    
			documentHandler.add(messageDoc);
			messageDoc.setValue("nextAction", "/b2b/lead2BasketDialogue.do");    			
			forwardTo = "showMessage";
    			
		}
    		    		    	
    	
		log.exiting();
		return forwardTo;
		
		
		
		
	}

}
