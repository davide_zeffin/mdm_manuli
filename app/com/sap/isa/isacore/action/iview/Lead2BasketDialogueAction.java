package com.sap.isa.isacore.action.iview;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.businessobject.order.Basket;
import com.sap.isa.businessobject.order.Order;
import com.sap.isa.businessobject.ordertemplate.OrderTemplate;
import com.sap.isa.businessobject.quotation.Quotation;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.MessageDocument;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 * Reacts on input on message jsp, either keep existing editable document, or
 * continue lead2Basket process.
 */
public class Lead2BasketDialogueAction extends IsaCoreBaseAction {

	public ActionForward isaPerform(ActionMapping mapping,
                                    ActionForm form,
                                    HttpServletRequest request,
                                    HttpServletResponse response,
                                    UserSessionData userSessionData,
                                    RequestParser requestParser,
                                    BusinessObjectManager bom,
                                    IsaLocation log) throws CommunicationException {

		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
	    String forwardTo = "";

        String createNewDoc = (String) request.getParameter("createDoc");

		// get Document Handler
		DocumentHandler documentHandler = (DocumentHandler)
                userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
        if (documentHandler == null) {
        	log.exiting();
            throw new PanicException("No document handler found");
        }

		//get user
		User user = bom.getUser();
				        
        if ((createNewDoc == null) || (createNewDoc.length() == 0)) {

			// delete message in document handler
            documentHandler.releaseMessageDocument();
            forwardTo = "cancel";

        } else {
        	
        
        forwardTo = "createLead";
        request.setAttribute("lead2Basket","X");
				     
        MessageDocument messageDoc = documentHandler.getMessageDocument();
        
        
        DocumentState targetDocument =
            	documentHandler.getDocument(ManagedDocument.TARGET_DOCUMENT);

	    // get the current pre order sales document - basket is assumed as
    	// default
        SalesDocument preOrderSalesDocument = null;

	    if (targetDocument instanceof Quotation) {
    	        preOrderSalesDocument = bom.getQuotation();
            
        } else if (targetDocument instanceof OrderTemplate) {
            	preOrderSalesDocument = bom.getOrderTemplate();
         
	    } else if (targetDocument instanceof Order) {
            	preOrderSalesDocument = bom.getOrder();
         
        } else {
            	preOrderSalesDocument = bom.getBasket();         
	    }

       
	    Shop shop      = bom.getShop();

	  
    	preOrderSalesDocument.clearMessages();
		
		
        preOrderSalesDocument.destroyContent();       // may throw CommunicationException
        	
        if (preOrderSalesDocument instanceof Quotation) {
            	documentHandler.release((Quotation)preOrderSalesDocument);
            	bom.releaseQuotation();
        }
        else if (preOrderSalesDocument instanceof OrderTemplate) {
            	documentHandler.release((OrderTemplate)preOrderSalesDocument);
            	bom.releaseOrderTemplate();
        }
        	
        else if (preOrderSalesDocument instanceof Order) {
            	documentHandler.release((Order)preOrderSalesDocument);
            	bom.releaseOrder();
        }
        	
        else {
            	documentHandler.release((Basket)preOrderSalesDocument);
   		        bom.releaseBasket();
        }

		
		documentHandler.releaseMessageDocument();
		
        
       }
       log.exiting();           		    	
	   return mapping.findForward(forwardTo);
	    	

    }


}
