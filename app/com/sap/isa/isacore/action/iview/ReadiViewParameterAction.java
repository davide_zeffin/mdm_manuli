/*****************************************************************************
    Class:        ReadiViewParameterAction
    Copyright (c) 2002, SAP AG, Germany, All rights reserved.
    Author        Timo Kohr

    $DateTime: 2002/10/23 20:30:00 $ (Last changed)
    $Change: 89788 $ (changelist)

*****************************************************************************/
package com.sap.isa.isacore.action.iview;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.actionform.iview.DocumentListiViewParameterForm;

/**
 *  Document overview
 *
 */
public class ReadiViewParameterAction extends IsaCoreBaseAction {
	
    // CONSTANTS
    public final static String SC_DOCTYPE = "iview_doctype";
    public final static String SC_PORTALURL = "iview_portalurl";
    public final static String SC_PORTALURLSERVICE = "iview_portalurlservice";
    public final static String SC_PORTALURLBILLING = "iview_portalurlbilling";
    public final static String SC_ORDER_FILTER = "iview_orderfilter";
    public final static String SC_QUOTATION_FILTER = "iview_quotationfilter";
    public final static String SC_ORDERTEMPLATE_FILTER = "iview_ordertemplatefilter";
    public final static String SC_INVOICE_FILTER = "iview_invoicefilter";
    public final static String SC_CREDITMEMO_FILTER = "iview_creditmemofilter";
    public final static String SC_DOWNPAYMENT_FILTER = "iview_downpaymentfilter";
    public final static String SC_SERVICEREQUEST_FILTER = "iview_servicerequestfilter";
    public final static String SC_INFOREQUEST_FILTER = "iview_inforequestfilter";
    public final static String SC_COMPLAINTS_FILTER = "iview_complaintsfilter";
    public final static String SC_SERVICECONTRACT_FILTER = "iview_servicecontractfilter";

    public final static String SC_ORDER_ID = "iview_orderid";
    public final static String SC_QUOTATION_ID = "iview_quotationid";
    public final static String SC_ORDERTEMPLATE_ID = "iview_ordertemplateid";
    public final static String SC_INVOICE_ID = "iview_invoiceid";
    public final static String SC_CREDITMEMO_ID = "iview_creditmemoid";
    public final static String SC_DOWNPAYMENT_ID = "iview_downpaymentid";
    public final static String SC_SERVICEREQUEST_ID = "iview_servicerequestid";
    public final static String SC_INFOREQUEST_ID = "iview_inforequestid";
    public final static String SC_COMPLAINTS_ID = "iview_complaintsid";
    public final static String SC_SERVICECONTRACT_ID = "iview_servicecontractid";

    public final static String SC_SALES_REFRESH = "iview_sales_refresh";
    public final static String SC_BILLING_REFRESH = "iview_billing_refresh";
    public final static String SC_SERVICE_REFRESH = "iview_service_refresh";

    public final static String SC_CRMOBJECTTYPE_QUOTATION = "iview_objecttype_quotation";
    public final static String SC_CRMOBJECTTYPE_ORDER = "iview_objecttype_order";
    public final static String SC_CRMOBJECTTYPE_ORDERTEMPLATE = "iview_objecttype_ordertemplate";
    public final static String SC_CRMOBJECTTYPE_INVOICE = "iview_objecttype_invoice";
    public final static String SC_CRMOBJECTTYPE_DOWNPAYMENT = "iview_objecttype_downpayment";
    public final static String SC_CRMOBJECTTYPE_CREDITMEMO = "iview_objecttype_creditmemo";
    public final static String SC_CRMOBJECTTYPE_SERVICEREQUEST = "iview_objecttype_servicerequest";
    public final static String SC_CRMOBJECTTYPE_INFOREQUEST = "iview_objecttype_inforequest";
    public final static String SC_CRMOBJECTTYPE_COMPLAINTS = "iview_objecttype_complaints";
    public final static String SC_CRMOBJECTTYPE_SERVICECONTRACT = "iview_objecttype_servicecontract";



    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            BusinessObjectManager bom,
            IsaLocation log)
            throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        DocumentListiViewParameterForm docListiViewForm = (DocumentListiViewParameterForm) form;

        // set the refresh state of the components
        userSessionData.setAttribute(SC_SALES_REFRESH, "true");
        userSessionData.setAttribute(SC_BILLING_REFRESH, "true");
        userSessionData.setAttribute(SC_SERVICE_REFRESH, "true");

        // Put selection Period into Session context
        if ((userSessionData.getAttribute(SC_DOCTYPE) == null) || (userSessionData.getAttribute(SC_DOCTYPE).equals(""))) {
          userSessionData.setAttribute(SC_DOCTYPE, docListiViewForm.getDoc_type());
        }


        userSessionData.setAttribute(SC_PORTALURL, docListiViewForm.getPortalurl());
        userSessionData.setAttribute(SC_PORTALURLSERVICE, docListiViewForm.getPortalurlservice());
        userSessionData.setAttribute(SC_PORTALURLBILLING, docListiViewForm.getPortalurlbilling());

        // set the CRM Object Types
        userSessionData.setAttribute(SC_CRMOBJECTTYPE_QUOTATION, docListiViewForm.getCrmObjectTypeQuotation());
        userSessionData.setAttribute(SC_CRMOBJECTTYPE_ORDER, docListiViewForm.getCrmObjectTypeOrder());
        userSessionData.setAttribute(SC_CRMOBJECTTYPE_ORDERTEMPLATE, docListiViewForm.getCrmObjectTypeOrderTemplate());
        userSessionData.setAttribute(SC_CRMOBJECTTYPE_INVOICE, docListiViewForm.getCrmObjectTypeInvoice());
        userSessionData.setAttribute(SC_CRMOBJECTTYPE_DOWNPAYMENT, docListiViewForm.getCrmObjectTypeDownPayment());
        userSessionData.setAttribute(SC_CRMOBJECTTYPE_CREDITMEMO, docListiViewForm.getCrmObjectTypeCreditMemo());
        userSessionData.setAttribute(SC_CRMOBJECTTYPE_SERVICEREQUEST, docListiViewForm.getCrmObjectTypeServiceRequest());
        userSessionData.setAttribute(SC_CRMOBJECTTYPE_INFOREQUEST, docListiViewForm.getCrmObjectTypeInfoRequest());
        userSessionData.setAttribute(SC_CRMOBJECTTYPE_COMPLAINTS, docListiViewForm.getCrmObjectTypeComplaints());
        userSessionData.setAttribute(SC_CRMOBJECTTYPE_SERVICECONTRACT, docListiViewForm.getCrmObjectTypeServiceContract());

        userSessionData.setAttribute(SC_ORDER_ID, docListiViewForm.getOrderID());
        userSessionData.setAttribute(SC_ORDER_FILTER, docListiViewForm.getOrderListFilter());

        userSessionData.setAttribute(SC_QUOTATION_ID, docListiViewForm.getQuotationID());
        userSessionData.setAttribute(SC_QUOTATION_FILTER, docListiViewForm.getQuotationListFilter());

        userSessionData.setAttribute(SC_ORDERTEMPLATE_ID, docListiViewForm.getOrderTemplateID());
        userSessionData.setAttribute(SC_ORDERTEMPLATE_FILTER, docListiViewForm.getOrderTemplateListFilter());

        userSessionData.setAttribute(SC_INVOICE_ID, docListiViewForm.getInvoiceID());
        userSessionData.setAttribute(SC_INVOICE_FILTER, docListiViewForm.getInvoiceListFilter());

        userSessionData.setAttribute(SC_CREDITMEMO_ID, docListiViewForm.getCreditMemoID());
        userSessionData.setAttribute(SC_CREDITMEMO_FILTER, docListiViewForm.getCreditMemoListFilter());

        userSessionData.setAttribute(SC_DOWNPAYMENT_ID, docListiViewForm.getDownPaymentID());
        userSessionData.setAttribute(SC_DOWNPAYMENT_FILTER, docListiViewForm.getDownPaymentListFilter());

        userSessionData.setAttribute(SC_SERVICEREQUEST_ID, docListiViewForm.getServiceRequestID());
        userSessionData.setAttribute(SC_SERVICEREQUEST_FILTER, docListiViewForm.getServiceRequestListFilter());

        userSessionData.setAttribute(SC_INFOREQUEST_ID, docListiViewForm.getInfoRequestID());
        userSessionData.setAttribute(SC_INFOREQUEST_FILTER, docListiViewForm.getInfoRequestListFilter());

        userSessionData.setAttribute(SC_COMPLAINTS_ID, docListiViewForm.getComplaintsID());
        userSessionData.setAttribute(SC_COMPLAINTS_FILTER, docListiViewForm.getComplaintsListFilter());

        userSessionData.setAttribute(SC_SERVICECONTRACT_ID, docListiViewForm.getServiceContractID());
        userSessionData.setAttribute(SC_SERVICECONTRACT_FILTER, docListiViewForm.getServiceContractListFilter());
		log.exiting();
        return mapping.findForward("documentlistparameter");
    }


}