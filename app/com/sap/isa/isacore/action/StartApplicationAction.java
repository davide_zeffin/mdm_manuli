/*****************************************************************************
    Class         StartApplicationAction
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Action for the Profile Maintenance
    Author:       SAP AG
    Created:      27 Februar 2001
    Version:      0.1

    $Revision: #1 $
    $Date: 2001/06/26 $

*****************************************************************************/

package com.sap.isa.isacore.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.StartupParameter;

/**
 *  This action start's the application. <br>
 *  With the global parameter: {@link IsaCoreInitAction#PARAM_FORWARD}
 *  you can control the flow of the application.
 */
public class StartApplicationAction extends StartApplicationBaseAction {

    public static final String DATE_FORMAT = "dateformat";
    

    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward doStartApplication(ActionMapping mapping,
            								 ActionForm form,
            								 HttpServletRequest request,
            								 HttpServletResponse response,
            								 UserSessionData userSessionData,
            								 MetaBusinessObjectManager mbom,
            								 StartupParameter startupParameter
            								 )
            throws CommunicationException {
		final String METHOD_NAME = "doStartApplication()";
		log.entering(METHOD_NAME);
        BusinessObjectManager bom = (BusinessObjectManager)mbom.getBOMbyName(BusinessObjectManager.ISACORE_BOM);
        
        User user = bom.getUser();
        Shop shop = bom.getShop();
        HttpSession session = request.getSession();

        if (user == null) {
        	log.exiting();
            throw new PanicException("user.notFound");
        }

        if (shop == null) {
        	log.exiting();
            throw new PanicException("shop.notFound");
        }

        String shopdate = shop.getDateFormat().toLowerCase();
        session.setAttribute(DATE_FORMAT,shopdate);

        // check if a logical forward is given as an startupParameter
        if (startupParameter != null) {
            String forward = startupParameter.getParameterValue(IsaCoreInitAction.PARAM_FORWARD);
            if (forward.length() > 0) {
                if (log.isDebugEnabled()) {
                    log.debug("Forward to: " + forward);
                }
                log.exiting();
                return mapping.findForward(forward);
            }
        }
        
        if (ActionConstants.APPLAREA_SEPBILL.equals(userSessionData.getAttribute(CrossEntryAction.RP_APPL_FORWARD))) {
            log.exiting();
            return mapping.findForward("billingframe");
        }
        
        // if the Application was started with areaID and productID as request parameters, these context values will be
        // set, by the DirectToDetailsAction, to indicate, that we should directly forward to the details of an item,
        // or the prodcuts of an area
        String areaID = getContextValue(request, com.sap.isa.catalog.actions.ActionConstants.CV_CURRENT_AREA);
        String productID = getContextValue(request, com.sap.isa.catalog.actions.ActionConstants.CV_CURRENT_ITEM);
        
        
		if (areaID != null) {
			if (productID != null) {
				log.debug("forward to productdetail for areaID=" + areaID + " productID=" + productID);
				log.exiting();
				return mapping.findForward("directdetail");
			}
			else {
				log.debug("forward to area products for areaID=" + areaID);
				log.exiting();
				return mapping.findForward("directarea");
			}
		}

        // Set application area (see also fs_portalbillingstatus.jsp)
        userSessionData.setAttribute(ActionConstants.SC_APPLAREA, ActionConstants.APPLAREA_SHOP);
		log.exiting();
        return mapping.findForward("success");
    }
}

