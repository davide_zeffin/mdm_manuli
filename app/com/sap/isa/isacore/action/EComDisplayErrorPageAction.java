/*****************************************************************************
    Class:        EComDisplayErrorPageAction
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      20.04.2004
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.isacore.action;

import java.io.IOException;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.businessobject.BusinessObjectException;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.MessageListDisplayer;
import com.sap.isa.core.util.MessageListHolder;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.MessageDisplayer;

/**
 * The class EComDisplayErrorPageAction prepares a MessageDisplayer to 
 * display an error page . <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class EComDisplayErrorPageAction extends EComBaseAction {

	public final static String RC_MESSAGES  = "messagelist";
	
    /**
     * Overwrites the method . <br>
     * 
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @param userSessionData
     * @param requestParser
     * @param mbom
     * @param multipleInvocation
     * @param browserBack
     * @return
     * @throws IOException
     * @throws ServletException
     * @throws CommunicationException
     * 
     * 
     * @see com.sap.isa.isacore.action.EComBaseAction#ecomPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.sap.isa.core.UserSessionData, com.sap.isa.core.util.RequestParser, com.sap.isa.core.businessobject.management.MetaBusinessObjectManager, boolean, boolean)
     */
    public ActionForward ecomPerform(
	        ActionMapping mapping,
	        ActionForm form,
	        HttpServletRequest request,
	        HttpServletResponse response,
	        UserSessionData userSessionData,
	        RequestParser requestParser,
	        MetaBusinessObjectManager mbom,
	        boolean multipleInvocation,
	        boolean browserBack)
	        throws IOException, ServletException, CommunicationException {
		
		final String METHOD_NAME = "ecomPerform()";
		log.entering(METHOD_NAME);
		
		MessageListDisplayer messageDisplayer = (MessageListDisplayer)request.getAttribute(MessageListDisplayer.CONTEXT_NAME);
		
		if (messageDisplayer == null) {
			messageDisplayer = new MessageDisplayer();
			messageDisplayer.setApplicationError(true);
			messageDisplayer.setOnlyBack();
			request.setAttribute(MessageListDisplayer.CONTEXT_NAME,messageDisplayer);
		} else {
			messageDisplayer.setErrorPage(true);
		}
		
		// add messages from the exception object if possible
		Exception ex = (Exception)request.getAttribute("Exception");

		if (ex != null && ex instanceof BusinessObjectException) { 
			messageDisplayer.copyMessages((BusinessObjectException)ex);
		}


		// add messages from a messageList, if possible
		MessageListHolder messageList = (MessageListHolder)request.getAttribute(RC_MESSAGES);
		 if (messageList != null) {
			messageDisplayer.copyMessages(messageList);			 	 
		 	
		 }

		// add messages from the business object if possible
		BusinessObjectBase bo = (BusinessObjectBase)request.getAttribute(BusinessObjectBase.CONTEXT_NAME);

		if (bo != null){ 
			messageDisplayer.copyMessages(bo);	
		   // read all subobject of the business object to display the errors
		   // of subobjects 
		   Iterator i = bo.getSubObjectIterator();

		   while (i.hasNext()) {
			messageDisplayer.copyMessages((BusinessObjectBase)i.next());	
		   }
		}		    
		log.exiting();
		return mapping.findForward("message");
    }

}
