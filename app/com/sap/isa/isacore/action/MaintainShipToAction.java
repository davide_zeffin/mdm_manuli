/*****************************************************************************
    Class         MaintainShipToAction
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Action for the Profile Maintenance
    Author:       SAP
    Created:      April 20001
    Version:      1.0

    $Revision: #6 $
    $Date: 2002/08/05 $
*****************************************************************************/

package com.sap.isa.isacore.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.Address;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.order.PartnerListEntry;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.AddressFormular;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.action.order.ChangeOrderAction;
import com.sap.isa.isacore.action.order.MaintainBasketNewShiptoAction;
import com.sap.isa.isacore.action.order.ManagedDocumentProcessCustomerOrder;

/**
 * Create a new ShipTo address for the order
 *
 * @author SAP
 * @version 1.0
 *
 */
public class MaintainShipToAction extends IsaCoreBaseAction {

    /**
     * Implement this method to add further parse functionality to this action.
     *
     * @param parser            Parser to simple retrieve data from the request
     * @param data              Object wrapping the session
     * @param addressFormular   Object wrapping the address object
     *
     */
    public void customerExitParseRequest (RequestParser   parser,
                                          UserSessionData data,
                                          AddressFormular addressFormular) {

    }


    /**
     * Action to maintain the shipto.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            BusinessObjectManager bom,
            IsaLocation log)
            throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        int ret = 1;

        Shop shop = bom.getShop();

        if (shop == null) {
        	log.exiting();
            throw new PanicException("shop.notFound");
        }

        if (requestParser.getParameter("cancel").getValue().getString().length() > 0) {
        	log.exiting();
            return mapping.findForward("canceled");
        }

        AddressFormular addressFormular = new AddressFormular(requestParser);

        Address address = addressFormular.getAddress();

        // set the correct address format
        addressFormular.setAddressFormatId(shop.getAddressFormat());


		if (log.isDebugEnabled()) {
	        log.debug("Country: " + address.getCountry());	
	        log.debug("RegionList: " + requestParser.getParameter("RegionList").getValue().getString());
		}	


        String itemKey = requestParser.getParameter(ShowShipToAction.PARAM_ITEMKEY).getValue().getString();
        String shipToKey = requestParser.getParameter(ShowShipToAction.PARAM_SHIPTO_KEY).getValue().getString();
        String shipToHandle = requestParser.getParameter(MaintainBasketNewShiptoAction.SHIPTO_HANDLE).getValue().getString();
        
        if (requestParser.getParameter(ShowShipToAction.PARAM_ITEMKEY).isSet()) {
            request.setAttribute(ShowShipToAction.RC_ITEMKEY, itemKey);
            request.setAttribute(ShowShipToAction.RC_SHIPTO_KEY, shipToKey);
        }

        // call the customer exit
        customerExitParseRequest(requestParser,
                                 userSessionData,
                                 addressFormular);

        // search for existing target document on the document handler
        DocumentHandler documentHandler = (DocumentHandler)
                userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
        // get panic if it's null
        if (documentHandler == null) {
        	log.exiting();
            throw new PanicException("No document handler found");
        }


		if (requestParser.getParameter("save").getValue().getString().length() > 0) {
            // save the ship to in the basket object
            SalesDocument document = null;
            
            ManagedDocument managedDocument = 
            	documentHandler.getManagedDocument(ManagedDocument.TARGET_DOCUMENT);
            
            if (managedDocument instanceof ManagedDocumentProcessCustomerOrder) {
                // if we process a customer order the shipto is add to the collective 
                // order
                document = bom.getCollectiveOrder();
                
            } 
            else {
            
	            // get the actual open document from the handler
	            DocumentState targetDocument =
	                documentHandler.getDocument(ManagedDocument.TARGET_DOCUMENT);
	
	            if (targetDocument == null) {
					log.exiting();
	              throw new PanicException("No sales document returned from document handler");
	            }
	                
	            if (targetDocument instanceof SalesDocument) {
					log.exiting();
	                document = (SalesDocument) targetDocument;
	            }
            }
            
            if (document == null) {
				log.exiting();
                throw new PanicException("No sales document returned from backend");
            }

            document.clearMessages();
            
			PartnerListEntry soldToEntry = document.getHeader().getPartnerList().getSoldTo();

			if (soldToEntry != null) {
	            if (itemKey.length() == 0) {
/*@TODO d034021
 * check, if this implementation doesn't impact old functionality
 * ship to handle is used for SalesDocumentR3Lrd
 */	            	
	            	if (shipToHandle != null && !shipToHandle.equals("")) {
						ret = document.addNewShipTo(address,
													soldToEntry.getPartnerTechKey(),
													shop.getTechKey(),
													shipToHandle);
	            	} else {	            	
	                	ret = document.addNewShipTo(address,
										        soldToEntry.getPartnerTechKey(),
	                                            shop.getTechKey());
	            	}
	            }
	            else {
	            	if (shipToHandle != null && !shipToHandle.equals("")) {
						ret = document.addNewShipTo(new TechKey(itemKey),
																  new TechKey(shipToKey),
																  address,
																  soldToEntry.getPartnerTechKey(),
																  shop.getTechKey(),
																  shipToHandle);
	            	} else {			            	
	                	ret = document.addNewShipTo(new TechKey(itemKey),
	                                      new TechKey(shipToKey),
	                                      address,
										  soldToEntry.getPartnerTechKey(),
	                                      shop.getTechKey());
	            	}
	            }
			}

        }

        addressFormular.addToRequest(request,shop);

        if (ret == 0 && address.isValid()) {
            
            ManagedDocument managedDocument = documentHandler.getManagedDocumentOnTop();
            
            if (managedDocument instanceof ManagedDocumentProcessCustomerOrder) {
				log.exiting();
                return mapping.findForward("processcustomerorder");
            }      
            // decide, if we are coming from the orde_change.jsp
            else if (managedDocument.getForwardMapping().equalsIgnoreCase(ChangeOrderAction.ORDER_CHANGE)) {
				log.exiting();
                return mapping.findForward("successorderchange");
            }
            else {
				log.exiting();
                return mapping.findForward("success");
            }
        }
        else {
			log.exiting();
			if (shipToHandle != null && !shipToHandle.equals("")) {
				request.setAttribute(MaintainBasketNewShiptoAction.SHIPTO_HANDLE, shipToHandle);
			}
            return mapping.findForward("showshipto");
        }

    }

}

