/*****************************************************************************
    Class:        ActionConstantsBase
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Created:      May 2001
    Version:      1.0

    $Revision: #2 $
    $Date: 2001/07/03 $
*****************************************************************************/

package com.sap.isa.isacore.action;

import com.sap.isa.isacore.AddressConstants;

/**
 * this class defines some constants for request attributes (RA) which are used
 * in actions to store and retrieve data from request context.
 */
public class ActionConstantsBase implements AddressConstants, EComConstantsBase {

  /**
   * key to store item key in request context
   */
  public final static String RA_FORWARD              = "forward";

  public final static String RC_WEBCATITEM           = "currentItem";

  /**
   * Name of the SalesDocumentStatus details stored in the request context
   */
  public static final String RC_SALES_STATUS_DETAIL = "salesdocumentstatusdetail";
  /**
   * Name of the shiptolinekey storted in the request context
   */
  public static final String PARAM_SHIPTO_TECHKEY = "shipToTechKey";
  /**
   * Name of the item id stored in the request context
   */
  public static final String PARAM_ITEMID = "itemId";
  /**
   * Name of the item check box in the request context
   */
  public static final String RK_CHECKBOXNAME = "itemcheckbox";
  /**
   * Name of the hidden item check box in the request context
   */
  public static final String RK_CHECKBOXHIDDEN = "itemcheckboxhidden";
  /**
   * Name of the Sales Document Status Detail's item iterator in the request context
   */
  public static final String RK_SALES_DOC_STATUS_DETAIL_ITEMITERATOR = "itemiterator";
  /**
   * Name of the Billing Document Status Detail's item iterator in the request context
   */
  public static final String RK_BILLING_DOC_STATUS_DETAIL_ITEMITERATOR = "itemiterator";
  /**
   * Constant to define request parameter, that will be set,
   * if no valid search criteria is specified in the Large Document Scenario of the Sales
   * Document Status
   */
  public static final String RC_NO_SEARCH_CRITERIA = "nosearchcriteria";
    
  /**
   * Constant to define request parameter, that will be set,
   * if no items were found in the Large Document Scenario of the Sales
   * Document Status
   */
  public static final String RC_NO_ITEMS_FOUND = "noitemsfound";
  
  /**
   * Constant to define request parameter, that will be set,
   * if no the low value is invalid
   */
  public static final String RC_LOW_VAL_INVALID = "lowvalinvalid";
    
  /**
   * Constant to define request parameter, that will be set,
   * if no the high value is invalid
   */
  public static final String RC_HIGH_VAL_INVALID = "highvalinvalid";
    
  /**
   * Constant to define request parameter, that will hold the
   * techkey of the salesdocument in the Large Document Scenario of the Sales
   * Document Status
   */
  public static final String RC_TECHKEY = "techkey";
  /**
   * String constant to store the document handler for the separated billing
   * document display in the session context.
   */
  public static final String DOCUMENT_HANDLER_BILL =
                                 "DocumentHandler.bill.isacore.isa.sap.com";
    
  /**
   * Constant to define request parameter, that will hold the
   * objecttype of the salesdocument in the Large Document Scenario of the Sales
   * Document Status
   */
  public static final String RC_OBJECT_TYPE = "objecttype";
  /**
   * Constant for the XML format for the Order download
   */
  public static final String ORDDOWN_XML_FORMAT_STR = "XML";
  /**
   * Constant for the PDF format for the Order download
   */
  public static final String ORDDOWN_PDF_FORMAT_STR = "PDF";

  /**
   * Constant for the CSV format for the Order download
   */
  public static final String ORDDOWN_CSV_FORMAT_STR = "CSV";
  /**
   * Session context constant for storage of the list of available ship tos
   */
  public static final String SC_SHIPTOS = "com.sap.isa.isacore.action.order.MaintainBasketAction.shiptos";

  /**
   * Name of the payment cards object stored in the request context
   */
  public static final String RC_PAYMENT_CARDS = "paymentCards";
  
  /**
   * Payment card authorization status value
   */
  public static final String CARD_AUTH_ERROR = "04";
    
  
}