/*****************************************************************************
    Class         GetForwardAction
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Action to forward to the another action. The forward will
                  be taken from the request
    Author:       SAP
    Created:      July 2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/08/01 $
*****************************************************************************/

package com.sap.isa.isacore.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;

/**
 * Action to read a logical forward from the context to forward to this.
 *
 * The action take the logical forward from the parameter forward.
 *
 * @author SAP AG
 * @version 1.0
 *
 */
public class GetForwardAction extends IsaCoreBaseAction {

    /**
     * Name of the request attributes to the transfer object
     */
    public static final String FORWARD_NAME  = ActionConstants.RA_FORWARD;

    private static final String FORWARD_SUCCESS = "success";

	/**
	 * Initialize the action.
	 * In B2C this action could be used by anonymous user.
	 * 
	 * @see com.sap.isa.isacore.action.IsaCoreBaseAction#initialize()
	 */
	public void initialize() {
		if (isB2C()) {
			this.checkUserIsLoggedIn = false;
		}	
	}

    /**
     * Overriden <em>isaPerform</em> method of <em>IsaCoreBaseAction</em>.
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            BusinessObjectManager bom,
            IsaLocation log)
            throws IOException, ServletException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        // page to be displayed next
        String forwardTo = null;

        RequestParser.Value forwardValue = requestParser.getParameter(FORWARD_NAME).getValue();

        if (forwardValue.isSet()) {
            forwardTo = forwardValue.getString();
            if (log.isDebugEnabled()) log.debug("forward to: " + forwardTo);
        }
        else {
            forwardTo = FORWARD_SUCCESS;
        }
		log.exiting();
        return mapping.findForward(forwardTo);
    }


}