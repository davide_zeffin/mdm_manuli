/*****************************************************************************
    Class         CreateOrderTemplateAction
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       
    Created:      08.06.2001
    Version:      1.0

    $Revision: #5 $
    $Date: 2002/10/01 $
*****************************************************************************/

package com.sap.isa.isacore.action.ordertemplate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.backend.boi.isacore.order.DocumentListFilterData;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.order.CampaignListEntry;
import com.sap.isa.businessobject.order.PartnerList;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.action.ActionConstants;
import com.sap.isa.isacore.action.order.CreateSalesDocumentBase;
import com.sap.isa.isacore.action.order.MaintainBasketBaseAction;

/**
 * Create an <code>OrderTemplate</code> and forward to a page displaying
 * and maintaining that object.
 *
 * @author Thomas Smits
 * @version 1.0
 *
 */
public class CreateOrderTemplateAction extends CreateSalesDocumentBase {

    private static final String FORWARD_SUCCESS = "showpreordersalesdoc";
    private static final String FORWARD_ORDER_TEMPLATE = "ordertemplate";

    /**
     * Overriden <em>isaPerform</em> method of <em>IsaCoreBaseAction</em>.
     */
    public ActionForward isaPerform(ActionMapping mapping,
                ActionForm form,
                HttpServletRequest request,
                HttpServletResponse response,
                UserSessionData userSessionData,
                RequestParser requestParser,
                BusinessObjectManager bom,
                IsaLocation log)
                        throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
		// Page that should be displayed next.
        String forwardTo = null;

        // Ask the business object manager for a user. If this object is not
        // already present, the the next action will to login
        User user = bom.getUser();
        Shop shop = bom.getShop();
        BusinessPartnerManager bupama = bom.createBUPAManager();

        if (user == null) {
        	log.exiting();
            return mapping.findForward("login");
        }
		//Check if user has permission for OrderTemplate Creation
		  Boolean perm = user.hasPermission(DocumentListFilterData.SALESDOCUMENT_TYPE_ORDERTEMPLATE,
		  									DocumentListFilterData.ACTION_CREATE);
		  if (!perm.booleanValue()){
				log.exiting();
			  throw new CommunicationException ("User has no permission to Create " 
			  									+ DocumentListFilterData.SALESDOCUMENT_TYPE_ORDERTEMPLATE);
		  }

        bom.releaseOrderTemplate();
        // Ask the business object manager to create an order template
        SalesDocument orderTemplate = bom.createOrderTemplate();

        orderTemplate.clearMessages();

        // read catalog
        WebCatInfo webCat =
                getCatalogBusinessObjectManager(userSessionData).getCatalog();

		// check the given sold to id. <br>
		if (!checkSoldToParameter(bupama,requestParser,userSessionData,shop)) {
			return mapping.findForward("message"); 
		}	

		// initialisation of the partner list
		PartnerList pList = createPartnerList(bupama, shop.getCompanyPartnerFunction(), requestParser);

		// set the Id in the CampaignListEntry if allowed and existing
		CampaignListEntry campaignListEntry = null;
		campaignListEntry = setCampaign(webCat, orderTemplate.getHeader(), campaignListEntry, shop);
		        
        orderTemplate.init(shop, bupama, pList, webCat ,shop.getTemplateProcessType(), campaignListEntry);    // may throw CommunicationException
        if (orderTemplate.getTechKey().getIdAsString().replaceAll("0","").trim().length()==0) {
            // This can happen, if there are missing authorizations
            log.error("Have got no TechKey for OrderTemplate!");
            if (orderTemplate.getMessageList() != null) {
                for (int i = 0; i < orderTemplate.getMessageList().size(); i++) {
                    Message msg = orderTemplate.getMessageList().get(i);
                    log.error(msg.getDescription());
                }
            }
            throw new CommunicationException ("Severe errors during order template initialization.");
        }   
                
        orderTemplate.readHeader();   
        orderTemplate.readAllItems(); 
        orderTemplate.setState(DocumentState.TARGET_DOCUMENT);

        // Store the list of ShipTos in session context, to allow all frames access to
        // it
        userSessionData.setAttribute(MaintainBasketBaseAction.SC_SHIPTOS, orderTemplate.getShipTos());

        // Satisfy the document handler
        HeaderSalesDocument header = orderTemplate.getHeader();

        ManagedDocument managedDocument = new ManagedDocument(
            orderTemplate,
            "ordertemplate",  // part of ressource key for b2b.docnav.quotation
            header.getSalesDocNumber(),  // id
            header.getPurchaseOrderExt(),
            header.getDescription(),
            header.getCreatedAt(),
            FORWARD_ORDER_TEMPLATE,
            null,
            null);

        DocumentHandler documentHandler = (DocumentHandler)
            userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);

        documentHandler.add(managedDocument);
        documentHandler.setOnTop(orderTemplate);

        // search for a given forward in request context
        RequestParser.Value forwardValue =
                requestParser.getAttribute(ActionConstants.RA_FORWARD).getValue();

        userSessionData.setAttribute(MaintainBasketBaseAction.SC_DOCTYPE, MaintainBasketBaseAction.DOCTYPE_ORDERTEMPLATE);

        if (forwardValue.isSet()) {
            forwardTo = forwardValue.getString();
        }
        else {
            forwardTo = FORWARD_SUCCESS;
        }
		log.exiting();
        return mapping.findForward(forwardTo);
    }

}