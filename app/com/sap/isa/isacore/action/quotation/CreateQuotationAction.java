/*****************************************************************************
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       
  Created:      07.06.2001

  $Revision: #6 $
  $Date: 2002/10/01 $
*****************************************************************************/
package com.sap.isa.isacore.action.quotation;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.backend.boi.isacore.order.DocumentListFilterData;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.businessobject.order.CampaignListEntry;
import com.sap.isa.businessobject.order.PartnerList;
import com.sap.isa.businessobject.quotation.Quotation;
import com.sap.isa.businessobject.quotation.QuotationCreate;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.action.ActionConstants;
import com.sap.isa.isacore.action.order.CreateSalesDocumentBase;
import com.sap.isa.isacore.action.order.MaintainBasketBaseAction;

/**
 * Create a quotation.
 */
public class CreateQuotationAction extends CreateSalesDocumentBase {

    private static final String FORWARD_SUCCESS = "createquotation";
    private static final String FORWARD_QUOTATION = "quotation";

    /**
     * Overriden <em>isaPerform</em> method of <em>IsaCoreBaseAction</em>.
     */
    public ActionForward isaPerform(ActionMapping mapping,
                ActionForm form,
                HttpServletRequest request,
                HttpServletResponse response,
                UserSessionData userSessionData,
                RequestParser requestParser,
                BusinessObjectManager bom,
                IsaLocation log) throws CommunicationException {
                	
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
		// Page that should be displayed next.
        String forwardTo = null;
        
		// Transaction type used for creating the quotation
		RequestParser.Parameter processType = null;
		String processTypeValue;    
        
        // read the shop data
        Shop shop = bom.getShop();
        if (shop == null) {
			log.exiting();
            throw new PanicException("shop.notFound");
        }
        BusinessPartnerManager bupama = bom.createBUPAManager();

        // get user
        User user = bom.getUser();
        if (user == null) {
			log.exiting();
            return mapping.findForward("login");
        }
		//Check if user has permission for Quotation Creation
		Boolean perm = user.hasPermission(DocumentListFilterData.SALESDOCUMENT_TYPE_QUOTATION,
										  DocumentListFilterData.ACTION_CREATE);
		if (!perm.booleanValue()){
			log.exiting();
			throw new CommunicationException ("User has no permission to Create " 
												+ DocumentListFilterData.SALESDOCUMENT_TYPE_QUOTATION);
		}
        
        bom.releaseQuotation();
        // Ask the business object manager to create a quotation.
        Quotation quotation = bom.createQuotation();
        QuotationCreate quotationCreate = new QuotationCreate(quotation);

        // initialisation of the quotation
        quotation.clearMessages();

        // read catalog
        WebCatInfo webCat =
                getCatalogBusinessObjectManager(userSessionData).getCatalog();

		// check the given sold to id. <br>
		if (!checkSoldToParameter(bupama,requestParser,userSessionData,shop)) {
			return mapping.findForward("message");
		}	

		// initialisation of the partner list
		PartnerList pList = createPartnerList(bupama, shop.getCompanyPartnerFunction(), requestParser);

	   //determine the transaction type
		processType = requestParser.getParameter("processtype");
		if (!processType.isSet()){
			processType = requestParser.getAttribute("processtype");
		}
		processTypeValue = processType.isSet() ? processType.getValue().getString() : "";        
		if (log.isDebugEnabled()) { log.debug("Quotation process type: " + processTypeValue); }			

		// set the Id in the CampaignListEntry if allowed and existing
		CampaignListEntry campaignListEntry = null;
		campaignListEntry = setCampaign(webCat, quotation.getHeader(), campaignListEntry, shop);

        quotationCreate.create(shop, bupama, pList, webCat, 
        					   processTypeValue, campaignListEntry);  // may throw CommunicationException

        userSessionData.setAttribute(
            MaintainBasketBaseAction.SC_SHIPTOS,
            quotation.getShipTos());

        // add new quotation to the document handler
        quotation.readHeader();
        quotation.readAllItems();   // may throw CommunicationException
        quotation.setState(DocumentState.TARGET_DOCUMENT);

        ManagedDocument managedDocument = new ManagedDocument(
            quotation,
            "quotation",  // part of ressource key for b2b.docnav.quotation
            "new", 
            null,
            null,
            null,
            FORWARD_QUOTATION,
            null,
            null);
        DocumentHandler documentHandler = (DocumentHandler)
            userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
        documentHandler.add(managedDocument);
        documentHandler.setOnTop(quotation);

        // search for a given forward in request context
        RequestParser.Value forwardValue =
                requestParser.getAttribute(ActionConstants.RA_FORWARD).getValue();

        userSessionData.setAttribute(MaintainBasketBaseAction.SC_DOCTYPE, MaintainBasketBaseAction.DOCTYPE_QUOTATION);

        if (forwardValue.isSet()) {
            forwardTo = forwardValue.getString();
        }
        else {
            forwardTo = FORWARD_SUCCESS;
        }
		log.exiting();
        return mapping.findForward(forwardTo);
        

    }

}