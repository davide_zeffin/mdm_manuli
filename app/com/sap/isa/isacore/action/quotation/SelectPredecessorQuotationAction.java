/*****************************************************************************
    Class         SelectPredecessorQuotationAction
    Copyright (c) 2001, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      29.11.2001
    Version:      1.0
*****************************************************************************/

package com.sap.isa.isacore.action.quotation;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.backend.boi.isacore.order.DocumentListFilterData;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.businessobject.order.DocumentListFilter;
import com.sap.isa.businessobject.order.OrderDataContainer;
import com.sap.isa.businessobject.order.OrderStatus;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.businesspartner.businessobject.PartnerFunctionBase;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.action.order.CreateSalesDocumentBase;

/**
 * Retrieves a list of predecessor quotations from the backend
 * and displays them to allow the user to order a selected quotation.
 *
 * @author SAP AG
  */
public class SelectPredecessorQuotationAction extends IsaCoreBaseAction {

    private static final String FORWARD_SUCCESS = "showquots";

    /**
     * List of quotations stored in request scope.
     */
    public static final String RK_QUOTE_LIST = "quotelist";

    /**
     * Number of read quotations
     */
    public static final String RK_QUOTE_COUNT = "quotecount";
    
	/**
	  * Process type of the target document
	  */
	 public static final String RK_PROCESS_TYPE = "processtype";    


    /**
     * Overriden <em>isaPerform</em> method of <em>IsaCoreBaseAction</em>.
     */
    public ActionForward isaPerform(ActionMapping mapping,
                ActionForm form,
                HttpServletRequest request,
                HttpServletResponse response,
                UserSessionData userSessionData,
                RequestParser requestParser,
                BusinessObjectManager bom,
                IsaLocation log)
                        throws CommunicationException {        
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        // Page that should be displayed next.
        String forwardTo = null;
        
		String processType = requestParser.getParameter("processtype").getValue().getString();        

        // Ask the business object manager for a user. If this object is not
        // already present, the the next action will to login
        User user = bom.getUser();

        if (user == null) {
        	log.exiting();
            return mapping.findForward("login");
        }
		//Check if user has permission for displaying Quotation
		Boolean perm = user.hasPermission(DocumentListFilterData.SALESDOCUMENT_TYPE_QUOTATION,
										  DocumentListFilterData.ACTION_READ);
		if (!perm.booleanValue()){
			log.exiting();
			throw new CommunicationException ("User has no permission to display " 
												+ DocumentListFilterData.SALESDOCUMENT_TYPE_QUOTATION);
		}

        Shop shop = bom.getShop();

        // create a filter for quotations
        DocumentListFilter filter = new DocumentListFilter();
        filter.setTypeQUOTA();
        if (shop.isQuotationExtended()) {
            filter.setStatusRELEASED();
        }
        else {
            filter.setStatusOPEN();
        }

        // get an orderlist
        OrderStatus orderList = bom.getOrderStatus();
        if (orderList == null) {
            orderList = bom.createOrderStatus(new OrderDataContainer());
        }
        BusinessPartnerManager buPaMa = bom.createBUPAManager(); 

		String soldToId = requestParser.getParameter("soldToId").getValue().getString();
		// check the given sold to id. <br>
		if (!CreateSalesDocumentBase.checkSoldToParameter(buPaMa,requestParser,userSessionData,shop)) {
			return mapping.findForward("message");
		}	

        TechKey soldToKey = null;
		if (soldToId != null && soldToId.length() > 0) {
			BusinessPartner buPa = buPaMa.getBusinessPartner(soldToId);
			soldToKey = buPa.getTechKey();    
		}
		else {  
			// get sold to from BuPaMa
			soldToKey = (buPaMa.getDefaultBusinessPartner(PartnerFunctionBase.SOLDTO) != null 
                             ? buPaMa.getDefaultBusinessPartner(PartnerFunctionBase.SOLDTO).getTechKey()
                             : new TechKey(""));
		}                             

        // Get Salesdocuments headers
        orderList.readOrderHeaders(soldToKey, shop, filter);

        request.setAttribute(RK_QUOTE_COUNT, new Integer(orderList.getOrderCount()).toString());
        request.setAttribute(RK_QUOTE_LIST, orderList);
		request.setAttribute(RK_PROCESS_TYPE, processType);

        forwardTo = FORWARD_SUCCESS;
		log.exiting();
        return mapping.findForward(forwardTo);
    }

}