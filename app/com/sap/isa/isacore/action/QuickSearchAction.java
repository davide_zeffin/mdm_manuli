/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Alexander Staff
  Class         QuickSearchAction
  Description:  Action to handle the input in the QuickSearch mask in the product tab
                of the order entry screen
  Created:      19 March 2001
  Version:      0.1

  $Revision: #2 $
  $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.isacore.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.IsaQuickSearch;
import com.sap.isa.businessobject.ProductList;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businessobject.businessevent.SearchEvent;
import com.sap.isa.businessobject.webcatalog.CatalogBusinessObjectManager;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.actionform.QuickSearchForm;

/**
 * Performs the quicksearch in the product catalog
 *
 * <br> Note that all Action classes have to provide a non argument constructor
 * because they were instantiated automatically by the action servlet.
 *
 * @author Alexander Staff
 * @version 0.1
 *
 */
public class QuickSearchAction extends IsaCoreBaseAction {

    /**
     * Name of the list of common attributes in the request
     */
    public static final String QS_RESULT_LIST   = "QuickSearchResults";

    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @param startupParameter  Object containing the startup parameters
     * @param eventHandler      Object to capture events with
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            BusinessObjectManager bom,
            IsaLocation log,
            IsaCoreInitAction.StartupParameter startupParameter,
            BusinessEventHandler eventHandler) {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        // Page that should be displayed next.
        String forwardTo = null;

        // get the ActionForm
        QuickSearchForm qsf;
        if ( form instanceof QuickSearchForm ) {
            qsf = (QuickSearchForm) form;
        }
        else {
        	log.exiting();
            return mapping.findForward("error");
        }

        // retrieve the attributes, resp. their ids/guids/whatever from the QuickSearchForm
        // and pass them to the IsaQuickSearch-Object that handles all the other stuff
        IsaQuickSearch isaQuickSearch = bom.getIsaQuickSearch();
        if ( isaQuickSearch == null ) {
            // error, must be there
            log.exiting();
            return mapping.findForward("error");
        }

        CatalogBusinessObjectManager cbom =
                getCatalogBusinessObjectManager(userSessionData);

        // clear all error messages, if any
        isaQuickSearch.clearMessages();

        // init the IsaQuickSearch-object and perform the search
        // after checking the input parameters
        String attribute  = qsf.getAttribute();
        String searchexpr = qsf.getSearchexpr();
        int displayHits   = qsf.getDisplayHits();
        int maxHits       = qsf.getMaxHits();

        if ( displayHits <= 0 ) {
            isaQuickSearch.addMessage(new Message(Message.ERROR, "iqs.error.displayHits", null, ""));
            isaQuickSearch.setInitialData();
            isaQuickSearch.setIsValid(false);
            forwardTo = "failed";
        }

        if (maxHits <= 0 ) {
            isaQuickSearch.addMessage(new Message(Message.ERROR, "iqs.error.maxHits", null, ""));
            isaQuickSearch.setInitialData();
            isaQuickSearch.setIsValid(false);
            forwardTo = "failed";
        }

        if ((attribute == null) || (attribute.length() < 1) ) {
            isaQuickSearch.addMessage(new Message(Message.ERROR, "iqs.error.attribute", null, ""));
            isaQuickSearch.setInitialData();
            isaQuickSearch.setIsValid(false);
            forwardTo = "failed";
        }
        // a little bit hacky here
        // I should try to find a more elegant solution
        else if( attribute.equalsIgnoreCase("NULL") == true || attribute.equalsIgnoreCase("ohne_Merkmal") == true ) {
            isaQuickSearch.addMessage(new Message(Message.ERROR, "iqs.error.attribute", null, ""));
            isaQuickSearch.setInitialData();
            isaQuickSearch.setIsValid(false);
            forwardTo = "failed";
        }

        if ((searchexpr == null) || (searchexpr.length() < 1) || searchexpr.trim().length() == 0) {
            isaQuickSearch.addMessage(new Message(Message.ERROR, "iqs.error.searchexpr", null, ""));
            isaQuickSearch.setInitialData();
            isaQuickSearch.setIsValid(false);
            forwardTo = "failed";
        }

        if ( forwardTo == null || "success".equals(forwardTo) ) {

            isaQuickSearch.setSearchAttribute( attribute );
            isaQuickSearch.setSearchexpr( searchexpr );
            isaQuickSearch.setDisplayHits( displayHits );
            isaQuickSearch.setMaxHits( maxHits );

            // do we have a result at all ?
            // getProdItems checked, ob neue Suche ausgef�hrt werden soll oder altes Ergebnis angezeigt wird
            ProductList items = isaQuickSearch.getProdItems();
            if ( items == null ) {
                if ( isaQuickSearch.getRetCount() == 0 ) {
                    isaQuickSearch.addMessage(new Message( Message.INFO, "iqs.result.nothing", null, ""));
                }
                else {
                    isaQuickSearch.addMessage(new Message( Message.INFO, "iqs.result.too_much", null, ""));
                }
                //no items found (something went wrong) => invalidate stored hitlist
                //isaQuickSearch.setInitialData();
                isaQuickSearch.setIsValid(false);
                forwardTo = "failed";
            }
            else {
                // ok, we found something, store it in the request
                // after calculating the prices
                items.determinePrices(cbom.getPriceCalculator() );
                request.setAttribute(QS_RESULT_LIST, items);
                forwardTo = "success";
            }

            SearchEvent event = new SearchEvent(isaQuickSearch);
            eventHandler.fireSearchEvent(event);

        }  // endif ( forwardTo == null || "success".equals(forwardTo) )

        // store some objects in the request
        request.setAttribute(InitQuickSearchAction.QS_OBJECT, isaQuickSearch);
        request.setAttribute(InitQuickSearchAction.QS_COMMONATTRIBUTES_LIST, isaQuickSearch.getAttributes());

        // everything is fine, be happy
        log.exiting();
        return mapping.findForward( forwardTo );
    }
}