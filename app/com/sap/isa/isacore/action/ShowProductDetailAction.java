/*****************************************************************************
    Class         ShowProductDetail
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Action to show product detail screen
    Author:       SAP AG
    Created:      May 2001
    Version:      1.0

    $Revision: #5 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.isacore.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Product;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.businessobject.order.Basket;
import com.sap.isa.businessobject.webcatalog.ItemDetailRequest;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.JspUtil;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.MessageDisplayer;

/**
 * Show's the product detail data from a product<p>
 * There are two possibilities: call with<br> 
 * &nbsp;&nbsp;productkey and scenario, or 
 * &nbsp;&nbsp;productguid, productarea and scenario.
 *
 * <h4>Overview over request parameters and attributes</h4>
 *
 * <table border="1" cellspacing="0" cellpadding="4">
 *   <tr>
 *     <th>name</th>
 *     <th>parameter</th>
 *     <th>attribute</th>
 *     <th>description</th>
 *   <tr>
 *   <tr>
 *      <td>productkey</td><td>X</td><td>&nbsp;</td>
 *      <td>Contains                  <code>the Product key</code>the unique key
 * in the backend system. </td>
 *   </tr>
 *   <tr>
 *      <td>productguid</td><td>X</td><td>&nbsp;</td>
 *      <td>product key in catalog</td>
 *   </tr>
 *   <tr>
 *      <td>productarea</td><td>X</td><td>&nbsp;</td>
 *      <td>Determine the catalog area.</td>
 *   </tr>
 *   <tr>
 *      <td>scenario</td><td>X</td><td>&nbsp;</td>
 *      <td>Determines the return point for the catalog.</td>
 *   </tr></table>
 *
 * <h4>The following forwards are used by this action</h4>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <th>forward</th>
 *     <th>description</th>
 *   <tr>
 *   <tr><td>success</td><td>no other logical forward is definied</td></tr>
 *   <tr><td>message</td><td>product is not in catalog</td></tr>
 *   <tr><td><i>locial   forward given by the <b>forward</b>
 * attribute</i></td><td>see config.xml</td></tr>
 * </table>
 * <h4>Overview over attributes which will be set in the request context</h4>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <th>attribute</th>
 *     <th>description</th>
 *   <tr>
 * <tr><td>forward</td><td>logical forward which, will be called after displaying a
 *    message.  The value is <i>success</i> by default</td></tr>
 * <tr><td>itemRequest</td><td>Item details which should be displayed.</td></tr>
 * <tr><td>message</td><td>The message displayed, if a product is not
 * in catalog.</td></tr>
 * </table>
 *
 *
 * @author SAP AG
 * @version 1.0
 *
 */

public class ShowProductDetailAction extends IsaCoreBaseAction {

    private static final String FORWARD_SUCCESS = "success";

    /**
     * Name of the request parameter to the product fields: product key
     */
    public static final String PRODUCT_KEY     = "productkey";
    /**
     * Name of the request parameter to the product fields: productguid
     */
    public static final String PRODUCT_GUID     = "productguid";
    /**
     * Name of the request parameter to the product fields: productarea
     */
    public static final String PRODUCT_AREA     = "productarea";
    /**
     * Name of the request parameter to the product fields: scenario
     */
    public static final String PRODUCT_SCENARIO = "scenario";
	/**
	 * Name of the request parameter for the item (main item of a package): itemkey
	 */
	public static final String SC_ITEM_KEY     = "basketitemkey";	

    /** 
     * creates the parameter list for a product, which can use in jsp's to
     *  display product details.
     *  Attention the values here won't be encoded! Please use {@link JspUtil#encodeURL(String)} before calling this method.
     */
    public static String createDetailRequest(Product product, String detailScenario){
//    	The coding below is very slow! I changed it to a string buffer that is 
//    	multiple times faster!
//        return "?" + PRODUCT_GUID + "=" + product.getItemId() +
//               "&" + PRODUCT_AREA + "=" + product.getArea() +
//               "&" + PRODUCT_SCENARIO + "=" + scenario+
//			   "&" + ActionConstants.RA_DETAILSCENARIO+ "=" + scenario;
    	return createDetailRequest(product.getItemId(), product.getArea(), detailScenario);
    }

    /** 
     * creates the parameter list for a product, which can use in jsp's to
     *  display product details.
     *  Attention the values here won't be encoded! Please use {@link JspUtil#encodeURL(String)} before calling this method.
     */
    public static String createDetailRequest(Product product, String detailsScenario, int itemIndex){
//    	The coding below is very slow! I changed it to a string buffer that is 
//    	multiple times faster!
//        return "?" + PRODUCT_GUID + "=" + product.getItemId() +
//               "&" + PRODUCT_AREA + "=" + product.getArea() +
//               "&" + PRODUCT_SCENARIO + "=" + scenario +
//               "&" + ActionConstants.RA_DETAILSCENARIO + "=" + scenario +
//               "&" + ActionConstants.RA_PRODUCT_INDEX + "=" + itemIndex;
    	StringBuffer str = new StringBuffer();
    	str.append(createDetailRequest(product, detailsScenario));
    	str.append("&");
    	str.append(ActionConstants.RA_PRODUCT_INDEX);
    	str.append("=");
    	str.append(itemIndex);
    	return str.toString();
    }

    /** 
     * Creates the parameter list for a product, which can use in jsp's to
     * display product details.
     * Attention the values here won't be encoded! Please use {@link JspUtil#encodeURL(String)} before calling this method.
     */
      public static String createDetailRequest(String itemId, String areaId, String detailScenario) {
//        	The coding below is very slow! I changed it to a string buffer that is 
//        	multiple times faster!
//            return "?" + PRODUCT_GUID + "=" + itemId +
//                   "&" + PRODUCT_AREA + "=" + areaId +
//                   "&" + PRODUCT_SCENARIO + "=" + detailScenario+
//                   "&" + ActionConstants.RA_DETAILSCENARIO + "=" + detailScenario;
        	StringBuffer str = new StringBuffer("?");
        	str.append(PRODUCT_GUID);
        	str.append("=");
        	str.append(itemId);
        	str.append("&");
        	str.append(PRODUCT_AREA);
        	str.append("=");
        	str.append(areaId);
        	str.append("&");
        	str.append(PRODUCT_SCENARIO);
        	str.append("=");
        	str.append(detailScenario);
        	str.append("&");
        	str.append(ActionConstants.RA_DETAILSCENARIO);
        	str.append("=");
        	str.append(detailScenario);
        	return str.toString();
    }

	/** 
	 * Creates	the parameter list for a product, which can use in jsp's to
	 * display product details.
     * Attention the values here won't be encoded! Please use {@link JspUtil#encodeURL(String)} before calling this method.
	 */
	public static String createDetailRequest(TechKey productKey, WebCatInfo catalog, String scenario) {

		// get info's about itemId and areaId

		Product product = new Product(productKey);

		if (product.enhance(catalog)) {
			return createDetailRequest(product,scenario);
		}

		return "";
	}
    
	/** 
	 * Creates	the parameter list for a product, which can use in jsp's to
	 * display product details.
	 */
	public static String createDetailRequest(Product product, WebCatInfo catalog, String scenario) {

		if (product.enhance(catalog)) {
			return createDetailRequest(product,scenario);
		}

		return "";
	}

	/** 
	 * Creates	the parameter list for a product, which can use in jsp's to
	 * display product details.
	 */
	public static String createDetailRequest(Product product, TechKey productKey, String scenario) {
		if (product == null)
			return createDetailRequest(productKey, scenario);
		else
			return createDetailRequest(product, scenario);
	}
    

	/** 
	 * Creates	the parameter list for a product, which can use in jsp's to
	 * display product details.
	 * Attention the values here won't be encoded! Please use {@link JspUtil#encodeURL(String)} before calling this method.
	 */
	public static String createDetailRequest(TechKey productKey, String detailScenario) {
//    	The coding below is very slow! I changed it to a string buffer that is 
//    	multiple times faster!
//		return "?" + PRODUCT_KEY + "=" + productKey.getIdAsString() +
//			   "&" + PRODUCT_SCENARIO + "=" + detailScenario+
//			   "&" + ActionConstants.RA_DETAILSCENARIO + "=" + detailScenario;
		StringBuffer str = new StringBuffer("?");
    	str.append(PRODUCT_KEY);
    	str.append("=");
    	str.append(productKey.getIdAsString());
    	str.append("&");
    	str.append(PRODUCT_SCENARIO);
    	str.append("=");
    	str.append(detailScenario);
    	str.append("&");
    	str.append(ActionConstants.RA_DETAILSCENARIO);
    	str.append("=");
    	str.append(detailScenario);
    	return str.toString();
	}

	/** 
	 * Creates	the parameter list for a basket item, which can be uses in the
	 * basket to change solution configuration of the item.
	 * Attention the values here won't be encoded! Please use {@link JspUtil#encodeURL(String)} before calling this method.
	 */
	public static String createScDetailRequest(String ScItemKey, String detailScenario) {
//    	The coding below is very slow! I changed it to a string buffer that is 
//    	multiple times faster!
//		return "?" + SC_ITEM_KEY + "=" + ScItemKey +
//			   "&" + PRODUCT_SCENARIO + "=" + detailScenario+
//			   "&" + ActionConstants.RA_DETAILSCENARIO + "=" + detailScenario;
		StringBuffer str = new StringBuffer("?");
    	str.append(SC_ITEM_KEY);
    	str.append("=");
    	str.append(ScItemKey);
    	str.append("&");
    	str.append(PRODUCT_SCENARIO);
    	str.append("=");
    	str.append(detailScenario);
    	str.append("&");
    	str.append(ActionConstants.RA_DETAILSCENARIO);
    	str.append("=");
    	str.append(detailScenario);
    	return str.toString();
	}
	

	/** 
	 * Creates	the parameter list for a basket item, which can be uses in the
	 * basket to change solution configuration of the item.
	 * Attention the values here won't be encoded! Please use {@link JspUtil#encodeURL(String)} before calling this method.
	 */
	public static String createScDetailRequest(String ScItemKey, String ScAnchorKey,  String scenario) {
//    	The coding below is very slow! I changed it to a string buffer that is 
//    	multiple times faster!
//		return "?" + SC_ITEM_KEY + "=" + ScItemKey +
//			   "&" + ActionConstantsBase.RC_ANCHOR_KEY + "=" + ScAnchorKey +
//			   "&" + PRODUCT_SCENARIO + "=" + scenario+
//			   "&" + ActionConstants.RA_DETAILSCENARIO + "=" + scenario;
		StringBuffer str = new StringBuffer();
    	str.append(createScDetailRequest(ScItemKey, scenario));
    	str.append("&");
    	str.append(ActionConstantsBase.RC_ANCHOR_KEY);
    	str.append("=");
    	str.append(ScAnchorKey);
    	return str.toString();
	}	
		
	/** 
	 * Creates	the parameter list for a basket item, which can be uses in the
	 * basket to change solution configuration of the item.
	 * Attention the values here won't be encoded! Please use {@link JspUtil#encodeURL(String)} before calling this method.
	 */
	public static String createScDetailRequest(String ScItemKey, String ScAnchorKey, String anchorIsCfg, String scenario) {
//    	The coding below is very slow! I changed it to a string buffer that is 
//    	multiple times faster!
//		return "?" + SC_ITEM_KEY + "=" + ScItemKey +
//		       "&" + ActionConstantsBase.RC_ANCHOR_KEY + "=" + ScAnchorKey +
//		       "&" + ActionConstantsBase.RC_ANCHOR_IS_CFG + "=" +  +
//			   "&" + PRODUCT_SCENARIO + "=" + scenario+
//			   "&" + ActionConstants.RA_DETAILSCENARIO + "=" + scenario;
		StringBuffer str = new StringBuffer();
    	str.append(createScDetailRequest(ScItemKey, ScAnchorKey, scenario));
    	str.append("&");
    	str.append(ActionConstantsBase.RC_ANCHOR_IS_CFG);
    	str.append("=");
    	str.append(anchorIsCfg);
    	return str.toString();
	}	

    /* define an inner class to implement the <code>ItemDetailRequest</code> object */
    private class DetailRequest implements ItemDetailRequest {

        private String id;
        private String areaId;
        private String catalog = "";
        private String scenario = "";
        private int index = -1;


        public DetailRequest (String id, String areaId, String scenario) {
            this.id       = id;
            this.areaId   = areaId;
            this.scenario = scenario;
        }

        public DetailRequest (String id, String areaId, String scenario, int index) {
            this.id       = id;
            this.areaId   = areaId;
            this.scenario = scenario;
            this.index    = index;
        }

        /**
         * The ID of the item! to be requested
         */
        public String getProductID() {
            return id;
        };


        /**
         * Reference to IPC-Item containing configuration
         */
        public Object getConfigurationItem() {
            return null;
        }


        /**
         * Key for catalog this item was taken from
         */
        public String getCatalogID() {
            return catalog;
        }

        public String getDetailScenario() {
            return scenario;
        }

        /**
         * Key for catalog area this item was taken from
         */
        public String getAreaID() {
            return areaId;
        }

        /**
         * index of the item
         */
        public int getIndex() {
            return this.index;
        }

    }


	/**
	 * Initialize the action.
	 * In B2C this action could be used by anonymous user.
	 * 
	 * @see com.sap.isa.isacore.action.IsaCoreBaseAction#initialize()
	 */
	public void initialize() {
		if (isB2C()) {
			this.checkUserIsLoggedIn = false;
		}	
	}


    /**
     * Overriden <em>isaPerform</em> method of <em>IsaCoreBaseAction</em>.
     */
    public ActionForward isaPerform(ActionMapping mapping,
                ActionForm form,
                HttpServletRequest request,
                HttpServletResponse response,
                UserSessionData userSessionData,
                RequestParser requestParser,
                BusinessObjectManager bom,
                IsaLocation log)
                        throws CommunicationException {
                        	
		Basket basket = null;
		String basketItemGuid = "";
		String productId = "";

		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
		ShowProductDetailAction.DetailRequest  detailRequest = null;

		WebCatInfo catalog =
				getCatalogBusinessObjectManager(userSessionData).getCatalog();

        if (catalog != null) {		

			if (requestParser.getParameter(SC_ITEM_KEY).isSet()) {

				basket = bom.getBasket();
				
				if (basket == null) {
					MessageDisplayer messageDisplayer = new MessageDisplayer();
					messageDisplayer.addMessage(new Message(Message.ERROR,"basket.not.found"));
					messageDisplayer.setOnlyBack();
					messageDisplayer.addToRequest(request);
					log.exiting();
					return mapping.findForward("message");
				}
				// set the dirty flag in order to assure that the basket will be refreshed after the return
				basket.setDirty(true);
				basketItemGuid = requestParser.getParameter(SC_ITEM_KEY).getValue().getString();

				ItemSalesDoc item = basket.getItem(new TechKey(basketItemGuid));
				if (item != null) {
					productId = item.getProductId().getIdAsString();
				}
			}		    	
			    	
			if (requestParser.getParameter(PRODUCT_KEY).isSet() || (productId.length() > 0)) {
				
				if (productId.length() == 0) {
					productId = requestParser.getParameter(PRODUCT_KEY).getValue().getString();
				}
				Product product = 
					new Product( new TechKey(productId));
	
				if (product.enhance(catalog)) {
								
					detailRequest = new ShowProductDetailAction.DetailRequest
					(product.getItemId(),
					 product.getArea(),
					 requestParser.getParameter(PRODUCT_SCENARIO).getValue().getString());
				} 
                else {
					 
					MessageDisplayer messageDisplayer = new MessageDisplayer();
					messageDisplayer.addMessage(new Message(Message.ERROR,"product.not.in.catalog"));
					messageDisplayer.setOnlyBack();
					messageDisplayer.addToRequest(request);
					return mapping.findForward("message");
				}
					
            } 
            else {
	        	   detailRequest = new ShowProductDetailAction.DetailRequest
	                   (requestParser.getParameter(PRODUCT_GUID).getValue().getString(),
	                    requestParser.getParameter(PRODUCT_AREA).getValue().getString(),
	                    requestParser.getParameter(PRODUCT_SCENARIO).getValue().getString(),
                        requestParser.getParameter(ActionConstants.RA_PRODUCT_INDEX).getValue().getInt());
			}
	
	        request.setAttribute("itemRequest",detailRequest);	
			request.setAttribute(ActionConstants.RA_DETAILSCENARIO, requestParser.getParameter(PRODUCT_SCENARIO).getValue().getString());	

	        if (log.isDebugEnabled()) {
	            log.debug("Before getCatalogBusinessObjectManager()");
	        }
	        
            WebCatItem item = null; 
            // search for existing WebCatItem in current itemlist of catalog
            if (catalog.getCurrentItemList() != null && 
                catalog.getCurrentItemList().size() > 0 &&
                detailRequest.getIndex() >= 0) {
                item = catalog.getCurrentItemList().getItem(detailRequest.getIndex());
                // check Product Guid  
                if (!item.equals(detailRequest.getProductID())) {
                    item = null;  
                }
            }
            
            // create new WebCatItem
            if (item == null) {
	            item = catalog.getItem(detailRequest.getAreaID(), detailRequest.getProductID());
            }
            
	        if (item != null) {
                
                if (bom.getShop() != null && ShopData.TELCO_CUSOMER_SELF_SERVICE.equals(bom.getShop().getScenario())) {
                    if (basket != null) {
                        item.setTechKey(new TechKey(basketItemGuid));
                        item.setSCOrderDocumentGuid(basket.getHeader().getScDocumentGuid());                    
                        if (item.isRelevantForExplosion() && item.getSolutionConfigurator() == null) {
                            if (basket.getHeader().getScDocumentGuid() == null || basket.getHeader().getScDocumentGuid().isInitial()) {
                                log.debug("SC Document ID is missing");
                            } else {
                                item.explode(); 
                            }
                        }
                    }
                }
                else {
                    if (log.isDebugEnabled()) {
                        log.debug("No explosion becuase Shop is null or not TelcoScenario. Scenario = " + ((bom.getShop() != null)? bom.getShop().getScenario() : "shop null"));
                    } 
                }

	            request.setAttribute(ActionConstants.RC_WEBCATITEM, item);
	        }
	        log.exiting();
            return mapping.findForward(FORWARD_SUCCESS);
        }
        else {
            MessageDisplayer messageDisplayer = new MessageDisplayer();
            messageDisplayer.addMessage(new Message(Message.ERROR,"product.not.in.catalog"));
            messageDisplayer.setOnlyBack();
            messageDisplayer.addToRequest(request);
            log.exiting();
            return mapping.findForward("message");
        }   
    }
}