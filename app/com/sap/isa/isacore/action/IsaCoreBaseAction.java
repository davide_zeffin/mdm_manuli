/*****************************************************************************
    Class         IsaCoreBaseAction
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP
    Created:      05.04.2001
    Version:      1.0

    $Revision: #12 $
    $Date: 2003/09/03 $
*****************************************************************************/

package com.sap.isa.isacore.action;

import java.io.IOException;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.businessobject.AppBaseBusinessObjectManager;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.User;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businessobject.ipc.IPCBOManager;
import com.sap.isa.businessobject.item.ItemList;
import com.sap.isa.businessobject.webcatalog.CatalogBusinessObjectManager;
import com.sap.isa.core.Constants;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.MessageList;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.StartupParameter;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.isacore.BusinessEventHandlerImpl;

/**
 * <p>
 * Base class that takes care of the save retrievement of the BOM and
 * session context. Another feature is that default catch statements for
 * all common exceptions are put into this class. The developer of a
 * an action can rely on this class to catch all Exceptions and display
 * corresponding pages.
 * If the correct log/tracing level (DEBUG) is set, all contextual data is
 * written do the log file. To provide a security feature, the value of
 * request parameters starting with <code>nolog_</code> are not written to
 * the logfile.<br>
 * You should for example protect credit card information with this
 * method.
 * </p>
 * <p>
 * <b>Note - In this version of the class every request coming from
 * one client is handled synchronized. This is done to prevent unpredictable
 * exceptions coming from the underlying middleware (JCo). This approach
 * may not be sufficient for high performance installations and it may
 * be necessary to switch to another model of synchronization later.</b>
 * </p>
 *
 * @author SAP
 * @version 1.0
 */
public abstract class IsaCoreBaseAction extends EComBaseAction {


	/**
	 * Switch to activated the check, if the user is logged, in the 
	 * <code>checkPreConditions</code> method. <br>
	 * The switch is <code>true</code> for the b2b application otherwise it is <code>false</code> by default. <br>
	 * <b>Overwrite this Value in the constructor or in the {@link #initialize}
	 * method  to change the default behaviour. </b>
	 * 
	 * @see #checkUserIsLoggedIn(User)
	 * @see #checkPreConditions
	 */
	protected boolean checkUserIsLoggedIn = true; 
	
	/**
	 * Switch to actived the check if the bom exist. <br>
	 */
	protected boolean checkBom = true;
	
	/**
	 * Create a new instance of this class.
	 */
	public IsaCoreBaseAction() {
		if (log.isDebugEnabled()) {
			log.debug("New instance created.");
		}
	}


	/**
	 * Returns the value of the Startupparameter with the given name. <br>
	 * If the parameter does not exists, the methods returns an empty string.
	 * 
	 * @param userSessionData current user session, 
	 * @param name name of the parameter.
	 * 
	 * @return value of the Startup parameter.
	 */
	public String getStartupParameterValue(UserSessionData userSessionData,
			String name) {
	
		StartupParameter startupParameter = (StartupParameter)userSessionData
				.getAttribute(SessionConst.STARTUP_PARAMETER);

		String parameterValue = "";
		
		if (startupParameter != null) {
			parameterValue = startupParameter.getParameterValue(name);
		}
		
		return parameterValue;
	}

    
    /**
     * Call this method, if your preconditions are to complex, to check
     * them
     * @deprecated is not used, will be removed!
     */
    protected void preconditionErrorDetected(String messageKey) {
    }


	/**
	 * Checks, if the given user is logged in. <br>
	 * If not a <code>{@link Constants#USER_NO_LOGIN} </code> will be returned to 
	 * found an appropriated forward.
	 * 
     * @param user the user, which should be checked
     * @return {@link Constants#USER_NO_LOGIN} if the user is not logged in. 
     */
    protected String checkUserIsLoggedIn(User user) {

		if (!user.isUserLogged()) {		
			return Constants.USER_NO_LOGIN;
		}
		
		return null;
	}


	/**
	 * Checks the precondition before the actions will be performed. <br>
	 * Overwrite this method to extends or to reduce the checks. <br>
	 * If an error occurrs the  <code>PanicException</code> will thrown.
	 * 
     * @param application name of the application (taken from Scenario
     *         parameter in web.xml (see {@link com.sap.isa.core.ContextConst#IC_SCENARIO}).
     * @param userSessionData user session data
     * @param bom current business object manager
     * @throws PanicException
     * 
     * @deprecated use {@link IsaCoreBaseAction#preConditionsCheck(String, UserSessionData, BusinessObjectManager)}
     * 
     * @see com.sap.isa.core.PanicException
     */
    protected void checkPreConditions(String application,
									  UserSessionData userSessionData,
									  BusinessObjectManager bom)
			throws PanicException {
			
	}

	/**
	 * Checks the precondition before the actions will be performed. <br>
	 * Overwrite this method to extends or to reduce the checks. <br>
	 * If an error occurrs the  <code>PanicException</code> will be thrown or a logical
	 * forward will be returned to allow a proper error handling.
	 * 
	 * @param application name of the application (taken from Scenario
	 *         parameter in web.xml (see {@link com.sap.isa.core.ContextConst#IC_SCENARIO}).
	 * @param userSessionData user session data
	 * @param bom current business object manager
	 * @return name of the logical forward to handle the error situation or <code>null</code> 
	 * if no error occured.
	 * 
	 * @throws PanicException
	 * 
	 * @see com.sap.isa.core.PanicException
	 */
	protected String preConditionsCheck(String application,
									  UserSessionData userSessionData,
									  BusinessObjectManager bom)
			throws PanicException {
			
		User user = bom.getUser();	
				
		checkObjectExist(user);	

		boolean check = application.equals(ShopData.B2C) || application.equals(ShopData.B2B) ;				
					
		if (check && checkUserIsLoggedIn) {
			return checkUserIsLoggedIn(user);
		}
		return null;
	}
	
		
	/**
	 * Generic method to check pre conditions. <br>
	 * Use the method {@link #checkPreConditions(String, UserSessionData, 
	 * BusinessObjectManager)} to overwrite or extend the default behaviour. 
	 * 
     * @param userSessionData user session data
     * @param mbom meta business object manager
	 * @return name of the logical forward to handle the error situation or <code>null</code> 
	 * if no error occured.
	 * 
	 * @throws PanicException
     * 
     * @see com.sap.isa.core.PanicException
     */
    protected final String checkPreConditions(UserSessionData userSessionData,
									MetaBusinessObjectManager mbom) 
			throws PanicException {
		
        String application = getApplication();

		if (checkBom) {
			BusinessObjectManager bom = (BusinessObjectManager) 
					userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
		    checkPreConditions(application, userSessionData, bom);
			return preConditionsCheck(application, userSessionData, bom);
		}
		
		return null;
	}


	
	/**
	 * Returns if the action runs in the B2C application.
	 * 
     * @param application name of the application (taken from Scenario
     *         parameter in web.xml (see {@link com.sap.isa.core.ContextConst#IC_SCENARIO}).
	 *
	 * @return <code>true</code>, if the application is B2C, else <code>false</code>.
	 */
	protected boolean isB2C() {
		return getApplication().equals(ShopData.B2C);	
	}


	/**
	 * Returns if the action runs in the B2B application.
	 * 
	 * @param application name of the application (taken from Scenario
	 *         parameter in web.xml (see {@link com.sap.isa.core.ContextConst#IC_SCENARIO}).
	 *
	 * @return <code>true</code>, if the application is B2B, else <code>false</code>.
	 */
	protected boolean isB2B() {
		return getApplication().equals(ShopData.B2B);	
	}


    /**
     * Returns an identifier for the session that can be used in all
     * calls to the logging API to provide session information for
     * the trace-file.
     *
     * @param request The current request (used to get the session)
     * @return an identifier for the session
     * @deprecated automatically support from logging framework.
     */
    protected String getLoggingPrefix(HttpServletRequest request) {
        // Code removed, because logging Framwork can now
        return "";
    }

    /**
     * Implemented doPerform method used by ActionBase.
     * This method does some useful stuff and then calls isaPerform.
     * 
     * @param mapping            The ActionMapping used to select this instance
     * @param form               The <code>FormBean</code> specified in the
     *                              config.xml file for this action
     * @param request            The request object
     * @param response           The response object
     * @param userSessionData    Object wrapping the session
     * @param requestParser      Parser to simple retrieve data from the request
     * @param mbom               Reference to the MetaBusinessObjectManager
     * @param multipleInvocation Flag indicating, that a multiple invocation occured
     * @param browserBack        Flag indicating a browser back
     * @return Forward to another action or page
     */
	public ActionForward ecomPerform(ActionMapping mapping,
			                          ActionForm form,
			                          HttpServletRequest request,
			                          HttpServletResponse response,
			                          UserSessionData userSessionData,
			                          RequestParser requestParser,
			                          MetaBusinessObjectManager mbom,
			                          boolean multipleInvocation,
			                          boolean browserBack)
			throws IOException, ServletException,
				   CommunicationException {
				   	
		final String METHOD_NAME = "ecomPerform()";
		log.entering(METHOD_NAME);
        // get BOM
        BusinessObjectManager bom =
             (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);

        // no BOM = no session
        if (bom == null) {
            if (log.isDebugEnabled()) {
                log.debug("No bom found! Forward to SESSION_NOT_VALID");
            }
            log.exiting();
            return mapping.findForward(Constants.SESSION_NOT_VALID);
        }
                
        BusinessEventHandlerImpl beHandler = null;
        
        // Retrieve the BusinessEventHandler
        beHandler = (BusinessEventHandlerImpl)userSessionData.getBusinessEventHandler();

        // and set bom and request
        beHandler.setBom(bom);
        beHandler.setRequest(request);

        // Retrieve the startup parameters
        IsaCoreInitAction.StartupParameter startupParameter =
                (IsaCoreInitAction.StartupParameter)
                        userSessionData.
                            getAttribute(IsaCoreInitAction.SC_STARTUP_PARAMETER);

		log.exiting();
        return isaPerform(mapping, form, request, response, userSessionData,
                             requestParser, bom, log, startupParameter, beHandler, multipleInvocation, browserBack);
    }

    /**
     * Overwrite this method, if you want to be notified of business events and
     * want to get a simpler access to the initialization parameters.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @param documentHandler   DocumentHandler to be used by the action
     * @param startupParameter  Object containing the startup parameters
     * @param eventHandler      Object to capture events with
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            BusinessObjectManager bom,
            IsaLocation log,
            IsaCoreInitAction.StartupParameter startupParameter,
            BusinessEventHandler eventHandler)
            throws IOException, ServletException,
                   CommunicationException {

        return isaPerform(mapping, form, request, response, userSessionData,
            requestParser, bom, log);
    }

    /**
     * Overwrite this method, if you want to be notified of business events and
     * want to get a simpler access to the initialization parameters. As an
     * additional parameter you will get a flag indicating, whether or not
     * a multiple invocation occured.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @param documentHandler   DocumentHandler to be used by the action
     * @param startupParameter  Object containing the startup parameters
     * @param eventHandler      Object to capture events with
     * @param multipleInvocation Flag indicating, that a multiple invocation occured
     * @param browserBack       Flag indicating a browser back
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            BusinessObjectManager bom,
            IsaLocation log,
            IsaCoreInitAction.StartupParameter startupParameter,
            BusinessEventHandler eventHandler,
            boolean multipleInvocation,
            boolean browserBack)
            throws IOException, ServletException,
                   CommunicationException {

        return isaPerform(mapping, form, request, response, userSessionData,
            requestParser, bom, log, startupParameter, eventHandler);
    }
    /**
     * Overwrite this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @param documentHandler   DocumentHandler to be used by the action
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            BusinessObjectManager bom,
            IsaLocation log)
            throws IOException, ServletException,
                   CommunicationException {
        return null;
    }

    public void finalize() throws Throwable{
        if (log.isDebugEnabled()) {
            log.debug("Instance removed by the garbage collector.");
        }
        super.finalize();
    }

    /**
     * Convenience method to retrieve the business object manager for the
     * catalog from the session.
     *
     * @param userSessionData the user session data for this session
     * @return the bom capable of creating business objects for the catalog
     */
    protected CatalogBusinessObjectManager getCatalogBusinessObjectManager(UserSessionData userSessionData) {
        return (CatalogBusinessObjectManager) userSessionData.getBOM(CatalogBusinessObjectManager.CATALOG_BOM);
    }

    
    /**
     * Convenience method to retrieve the business object manager for the
     * application base from the session.
     *
     * @param userSessionData the user session data for this session
     * @return the bom capable of creating business objects out of the application base
     */
    protected AppBaseBusinessObjectManager getAppBaseBusinessObjectManager(UserSessionData userSessionData) {
        return (AppBaseBusinessObjectManager) userSessionData.getBOM(AppBaseBusinessObjectManager.APPBASE_BOM);
    }
    
    /**
     * Convenience method to retrieve the business object manager for the
     * IPC from the session.
     *
     * @param userSessionData the user session data for this session
     * @return the bom capable of creating business objects for the IPC
     */
    protected IPCBOManager getIPCBusinessObjectManager(UserSessionData userSessionData) {
        // this is only a temporary hack until we get the name in some constant from the IPC-guys
        return (IPCBOManager) userSessionData.getBOM(IPCBOManager.NAME);
    }
    
	/**
	 * Check if a string is filled with only 'value'  
	 * @param s The String to check
	 * @param value The value to test on
	 * @return true if the string is filled with character value
	 */    
	protected boolean stringIsFilledWith(String s, char value) {
		boolean retVal = true;
		for (int i = 0; i < s.length() && retVal == true; i++) {
			if (s.charAt(i) != value) {
				retVal = false;
			}
		}
		return retVal;
	}
    
    /**
     * completes the message description for the sales document
     * only a resource key
     *
     * @param session the actual HTTP session
     * @param preOrderSalesDocument the document to store the data in
     * @param log reference to the logging context
     * @param isDebugEnabled flag indicating wheter or not logging output
     *        is written
     */
    protected void resolveMessageKeys(
            HttpSession session,
            SalesDocument preOrderSalesDocument,
            IsaLocation log,
            boolean isDebugEnabled)
                        throws CommunicationException {

          if (preOrderSalesDocument == null) {
             return;
          }

          translateMessageList(session,
                               preOrderSalesDocument.getMessageList(),
                               log,
                               isDebugEnabled);
          if (preOrderSalesDocument.getHeader() != null) {
              translateMessageList(session,
                               preOrderSalesDocument.getHeader().getMessageList(),
                               log,
                               isDebugEnabled);
          }

          ItemList items = preOrderSalesDocument.getItems();
          if (items != null) {
              for (int i = 0; i < items.size(); i++) {
                        translateMessageList(session,
                                   items.get(i).getMessageList(),
                                   log,
                                   isDebugEnabled);
              }
          }
    }

    /**
     * Retrieves the description from the resource files for messages containing
     * only a resource key
     *
     * @param session the actual HTTP session
     * @param msglist list of messages to process
     * @param log reference to the logging context
     * @param isDebugEnabled flag indicating wheter or not logging output
     *        is written
     */
    protected void translateMessageList(
            HttpSession session,
            MessageList msgList,
            IsaLocation log,
            boolean isDebugEnabled)
                        throws CommunicationException {

          Iterator it;

           if (msgList != null) {
               it = msgList.iterator();

               while (it.hasNext()) {

                Message message = (Message) it.next();

                String description = message.getDescription();
                String ressourceKey    = message.getResourceKey();

                    if ((description == null || description.length() == 0)
                         && ressourceKey != null) {
                        // Messagedescription is empty but message contains resource key
                        // retrieve the description
                        message.setDescription(WebUtil.translate(getServlet().getServletContext(),
                                           session,
                                           ressourceKey,
                                           message.getResourceArgs()));
                    }
                }
           }
    }

    /**
     * sets the flags for the available prices on header level of a salesDocument
     * in the http-request
     *
     * @param salesDoc The salesDocument
     * @param request The http-request
     */
    public void setAvailableValueFlagsForJSP(SalesDocument salesDoc, HttpServletRequest request) {
        
        Boolean isGrossValueAvailable;
        Boolean isNetValueAvailable;
        Boolean isTaxValueAvailable;
        Boolean isFreightValueAvailable;
        
        if (salesDoc != null) {
            isGrossValueAvailable = new Boolean(salesDoc.isGrossValueAvailable());
            isNetValueAvailable = new Boolean(salesDoc.isNetValueAvailable());
            isTaxValueAvailable = new Boolean(salesDoc.isTaxValueAvailable());
            isFreightValueAvailable = new Boolean(salesDoc.isFreightValueAvailable());
        }
        else {
            isGrossValueAvailable = new Boolean(true);
            isNetValueAvailable = new Boolean(true);
            isTaxValueAvailable = new Boolean(true);
            isFreightValueAvailable = new Boolean(true);
        }
        request.setAttribute(SalesDocument.IS_GROSS_VALUE_AVAILABLE,   isGrossValueAvailable);
        request.setAttribute(SalesDocument.IS_NET_VALUE_AVAILABLE,     isNetValueAvailable);
        request.setAttribute(SalesDocument.IS_TAX_VALUE_AVAILABLE,     isTaxValueAvailable);
        request.setAttribute(SalesDocument.IS_FREIGHT_VALUE_AVAILABLE, isFreightValueAvailable);
        
    }
}