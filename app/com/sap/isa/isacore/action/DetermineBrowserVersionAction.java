/*****************************************************************************
  Class:        DetermineBrowserVersionAction
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       SAP
  Created:      09.10.2001
  Version:      1.0

  $Revision: #5 $
  $Date: 2001/08/03 $
*****************************************************************************/

package com.sap.isa.isacore.action;

import java.io.IOException;

import com.sap.isa.core.*;
import com.sap.isa.core.util.*;
import org.apache.struts.action.*;

import javax.servlet.ServletException;
import javax.servlet.http.*;

/**
 * This action gets information about the browser used and stores the data
 * in an object. This object of type <code>BrowserVersion</code> is put
 * into the session context, to allow JSPs browser specific behaviour. The code
 * of this action is based on the browsersniff JavaScript by Netscape
 * Communications.
 *
 * @author SAP
 * @version 1.0
 */
public class DetermineBrowserVersionAction extends BaseAction {

    public static final String SC_BROWSER_VERSION = "com.sap.isa.isacore.action.DetermineBrowserVersionAction.browserVersion";
    public static final BrowserVersion UNKNOWN_BROWSER = new BrowserVersion();
	
    public static class BrowserVersion {

        private boolean nav;
        private boolean nav2;
        private boolean nav3;
        private boolean nav4;
        private boolean nav4up;
        private boolean navonly;
        private boolean nav6;
        private boolean nav6up;
        private boolean gecko;
        private boolean ie;
        private boolean ie3;
        private boolean ie4;
        private boolean ie4up;
        private boolean ie5;
        private boolean ie5_5;
        private boolean ie5up;
        private boolean ie5_5up;
        private boolean ie6;
        private boolean ie6up;
        private boolean aol;
        private boolean aol3;
        private boolean aol4;
        private boolean aol5;
        private boolean aol6;
        private boolean opera;
        private boolean opera2;
        private boolean opera3;
        private boolean opera4;
        private boolean opera5;
        private boolean opera5up;
        private boolean opera6;
        private boolean opera7;
        private boolean opera7up;
        private boolean webtv;
        private boolean hotjava;
        private boolean hotjava3;
        private boolean hotjava3up;

        private String  browserName;
        private int     browserMajor;
        private double  browserMinor;

        private double  javaScriptVersion;

        public BrowserVersion() {
        }

        public boolean isUnknown() {
            return this == UNKNOWN_BROWSER;
        }

        public boolean isNav() {
            return nav;
        }

        public boolean isNav4up() {
            return nav4up;
        }

        public boolean isNav4() {
            return nav4;
        }

        public boolean isNav6() {
            return nav6;
        }

        public boolean isNav6up() {
            return nav6up;
        }

        public boolean isIe() {
            return ie;
        }

        public boolean isIe4() {
            return ie4;
        }

        public boolean isIe4up() {
            return ie4up;
        }

        public boolean isIe5() {
            return ie5;
        }

        public boolean isIe5up() {
            return ie5up;
        }

        public boolean isIe55() {
            return ie5_5;
        }

        public boolean isIe55up() {
            return ie5_5up;
        }

        public boolean isIe6() {
            return ie6;
        }

        public boolean isIe6up() {
            return ie6up;
        }

        public boolean isOpera() {
            return opera;
        }

        public boolean isOpera7up() {
            return opera7up;
        }

        public String getBrowserName() {
            return browserName;
        }

        public int getBrowserMajor() {
            return browserMajor;
        }

        public double getBrowserMinor() {
            return browserMinor;
        }

        public double getJavaScriptVersion() {
            return javaScriptVersion;
        }

    }
    
    public DetermineBrowserVersionAction() {
    }

    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @param startupParameter  Object containing the startup parameters
     * @param eventHandler      Object to capture events with
     * @return Forward to another action or page
     */
	public ActionForward doPerform(ActionMapping mapping,
		                           ActionForm form,
		                           HttpServletRequest request,
		                           HttpServletResponse response)
			throws IOException, ServletException {
		final String METHOD_NAME = "doPerform()";
		log.entering(METHOD_NAME);
		RequestParser requestParser = new RequestParser(request);

        // Get the browser information
        String browserName  = requestParser.getParameter("browsername").getValue().getString();
        int    browserMajor = requestParser.getParameter("browsermajor").getValue().getInt();
        double browserMinor = requestParser.getParameter("browserminor").getValue().getDouble();

        if (log.isDebugEnabled()) {
            log.debug("Got browser information. Agent=" + browserName + ", major=" + browserMajor + ", minor=" + browserMinor);
        }

        BrowserVersion browserVersion = new BrowserVersion();

        browserVersion.browserMajor = browserMajor;
        browserVersion.browserName = browserName;
        browserVersion.browserMinor = browserMinor;

        // Note: Opera and WebTV spoof Navigator.  We do strict client detection.
        // If you want to allow spoofing, take out the tests for opera and webtv.
        browserVersion.nav  = ((browserName.indexOf("mozilla")!=-1) && (browserName.indexOf("spoofer")==-1)
                            && (browserName.indexOf("compatible") == -1) && (browserName.indexOf("opera")==-1)
                            && (browserName.indexOf("webtv")==-1) && (browserName.indexOf("hotjava")==-1));
        browserVersion.nav2 = (browserVersion.nav && (browserMajor == 2));
        browserVersion.nav3 = (browserVersion.nav && (browserMajor == 3));
        browserVersion.nav4 = (browserVersion.nav && (browserMajor == 4));
        browserVersion.nav4up = (browserVersion.nav && (browserMajor >= 4));
        browserVersion.navonly      = (browserVersion.nav && ((browserName.indexOf(";nav") != -1) ||
                              (browserName.indexOf("; nav") != -1)) );
        browserVersion.nav6 = (browserVersion.nav && (browserMajor == 5));
        browserVersion.nav6up = (browserVersion.nav && (browserMajor >= 5));
        browserVersion.gecko = (browserName.indexOf("gecko") != -1);


        browserVersion.ie     = ((browserName.indexOf("msie") != -1) && (browserName.indexOf("opera") == -1));
        browserVersion.ie3    = (browserVersion.ie && (browserMajor < 4));
        browserVersion.ie4    = (browserVersion.ie && (browserMajor == 4) && (browserName.indexOf("msie 4")!=-1) );
        browserVersion.ie4up  = (browserVersion.ie && (browserMajor >= 4));
        browserVersion.ie5    = (browserVersion.ie && (browserMajor == 4) && (browserName.indexOf("msie 5.0")!=-1) );
        browserVersion.ie5_5  = (browserVersion.ie && (browserMajor == 4) && (browserName.indexOf("msie 5.5") !=-1));
        browserVersion.ie5up  = (browserVersion.ie && !browserVersion.ie3 && !browserVersion.ie4);
        browserVersion.ie5_5up =(browserVersion.ie && !browserVersion.ie3 && !browserVersion.ie4 && !browserVersion.ie5);
        browserVersion.ie6    = (browserVersion.ie && (browserMajor == 4) && (browserName.indexOf("msie 6.")!=-1) );
        browserVersion.ie6up  = (browserVersion.ie && !browserVersion.ie3 && !browserVersion.ie4 && !browserVersion.ie5 && !browserVersion.ie5_5);

        // KNOWN BUG: On AOL4, returns false if IE3 is embedded browser
        // or if this is the first browser window opened.  Thus the
        // variables browserVersion.aol, browserVersion.aol3, and browserVersion.aol4 aren"t 100% reliable.
        browserVersion.aol   = (browserName.indexOf("aol") != -1);
        browserVersion.aol3  = (browserVersion.aol && browserVersion.ie3);
        browserVersion.aol4  = (browserVersion.aol && browserVersion.ie4);
        browserVersion.aol5  = (browserName.indexOf("aol 5") != -1);
        browserVersion.aol6  = (browserName.indexOf("aol 6") != -1);

        browserVersion.opera = (browserName.indexOf("opera") != -1);
        browserVersion.opera2 = (browserName.indexOf("opera 2") != -1 || browserName.indexOf("opera/2") != -1);
        browserVersion.opera3 = (browserName.indexOf("opera 3") != -1 || browserName.indexOf("opera/3") != -1);
        browserVersion.opera4 = (browserName.indexOf("opera 4") != -1 || browserName.indexOf("opera/4") != -1);
        browserVersion.opera5 = (browserName.indexOf("opera 5") != -1 || browserName.indexOf("opera/5") != -1);
        browserVersion.opera5up = (browserVersion.opera 
                                    && !browserVersion.opera2 
                                    && !browserVersion.opera3 
                                    && !browserVersion.opera4);

        browserVersion.opera6 = (browserName.indexOf("opera 6") != -1 || browserName.indexOf("opera/6") != -1);
        browserVersion.opera7 = (browserName.indexOf("opera 7") != -1 || browserName.indexOf("opera/7") != -1);
        browserVersion.opera7up = (browserVersion.opera 
        							&& !browserVersion.opera2 
        							&& !browserVersion.opera3 
        							&& !browserVersion.opera4
                        			&& !browserVersion.opera5
                        			&& !browserVersion.opera6);

        browserVersion.webtv = (browserName.indexOf("webtv") != -1);

        browserVersion.hotjava = (browserName.indexOf("hotjava") != -1);
        browserVersion.hotjava3 = (browserVersion.hotjava && (browserMajor == 3));
        browserVersion.hotjava3up = (browserVersion.hotjava && (browserMajor >= 3));

        if (browserVersion.nav2 || browserVersion.ie3) {
            browserVersion.javaScriptVersion = 1.0;
        }
        else if (browserVersion.nav3) {
            browserVersion.javaScriptVersion = 1.1;
        }
        else if (browserVersion.opera5up) {
            browserVersion.javaScriptVersion = 1.3;
        }
        else if (browserVersion.opera) {
            browserVersion.javaScriptVersion = 1.1;
        }
        else if ((browserVersion.nav4 && (browserVersion.browserMinor <= 4.05)) || browserVersion.ie4) {
            browserVersion.javaScriptVersion = 1.2;
        }
        else if ((browserVersion.nav4 && (browserVersion.browserMinor > 4.05)) || browserVersion.ie5) {
            browserVersion.javaScriptVersion = 1.3;
        }
        else if (browserVersion.hotjava3up) {
            browserVersion.javaScriptVersion = 1.4;
        }
        else if (browserVersion.nav6 || browserVersion.gecko) {
            browserVersion.javaScriptVersion = 1.5;
        }
        else if (browserVersion.nav6up) {
            // NOTE: In the future, update this code when newer versions of JS
            // are released. For now, we try to provide some upward compatibility
            // so that future versions of Nav and IE will show they are at
            // *least* JS 1.x capable. Always check for JS version compatibility
            // with > or >=.
            browserVersion.javaScriptVersion = 1.5;
        }
        else if (browserVersion.ie5up) {
            // NOTE: ie5up on mac is 1.4
            browserVersion.javaScriptVersion = 1.3;
        }
        else {
            browserVersion.javaScriptVersion = 0.0;
        }


		UserSessionData userSessionData =
				UserSessionData.getUserSessionData(request.getSession());

		if (userSessionData != null) {
			userSessionData.setAttribute(SC_BROWSER_VERSION, browserVersion);
		}
		log.exiting();
        return mapping.findForward("next");
    }

}
