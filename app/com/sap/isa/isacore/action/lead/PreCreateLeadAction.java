package com.sap.isa.isacore.action.lead;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.webcatalog.CatalogBusinessObjectManager;
import com.sap.isa.catalog.actions.ActionConstants;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.core.BaseAction;
import com.sap.isa.core.UserSessionData;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public class PreCreateLeadAction extends BaseAction {
	
  public PreCreateLeadAction() {
  }
  public ActionForward doPerform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws javax.servlet.ServletException, java.io.IOException {
	final String METHOD_NAME = "doPerform()";
	log.entering(METHOD_NAME);
    UserSessionData userSessionData =
            UserSessionData.getUserSessionData(request.getSession());

    CatalogBusinessObjectManager cbom =
                  (CatalogBusinessObjectManager)userSessionData.getBOM(CatalogBusinessObjectManager.CATALOG_BOM);


    WebCatItem currentItem = cbom.getCatalog().getCurrentItem();
    request.setAttribute(ActionConstants.RC_WEBCATITEM, currentItem);
	log.exiting();
    return mapping.findForward("success");
  }
}