package com.sap.isa.isacore.action.lead;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.lead.Lead;
import com.sap.isa.businessobject.webcatalog.CatalogBusinessObjectManager;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.core.BaseAction;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.util.WebUtil;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public class CreateLeadAction extends BaseAction {
	
  public CreateLeadAction() {
  }
  public ActionForward doPerform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws javax.servlet.ServletException, java.io.IOException {
    //System.out.println("CreateLeadAction:started");
	final String METHOD_NAME = "doPerform()";
	log.entering(METHOD_NAME);
    if(request.getParameter("Save") != null) {
      try {
        String leadText = request.getParameter("leadText");

        HttpSession session = request.getSession();
        UserSessionData userData = UserSessionData.getUserSessionData(session);

        CatalogBusinessObjectManager cbom =
                  (CatalogBusinessObjectManager)userData.getBOM(CatalogBusinessObjectManager.CATALOG_BOM);
        WebCatItem currentItem = cbom.getCatalog().getCurrentItem();

        BusinessObjectManager bom =
         (BusinessObjectManager) userData.getBOM(BusinessObjectManager.ISACORE_BOM);
        Lead lead = getLead(bom);

        LeadSavingThread thread = new LeadSavingThread(session, leadText,
                currentItem, lead);
        thread.start();
      }
      catch(Exception ex) {
        ex.printStackTrace();
      } finally {
	  		log.exiting();
  		}
    } 
 
    return mapping.findForward("success");
  }

  	private Lead getLead (BusinessObjectManager bom){
	 	BusinessPartner partner = null;
		BusinessPartnerManager buPaMa =  bom.createBUPAManager();
		partner = buPaMa.getDefaultBusinessPartner(PartnerFunctionData.SOLDTO);
      	
      	Lead lead = bom.createLead();
      	lead.setBPID(partner.getId());
      	return lead;
  	}


  private class LeadSavingThread extends Thread {
    public LeadSavingThread(HttpSession session, String text,
        WebCatItem item, Lead lead) {
      super();
      this.session = session;
      this.text = text;
      this.item = item;
      this.lead = lead;
    }

    HttpSession session;
    String text;
    WebCatItem item;
    Lead lead;

    public void run() {
    	
      try {
       synchronized(lead) {
        UserSessionData userData = UserSessionData.getUserSessionData(session);
        String language = userData.getLocale().getLanguage();
        if(language == null)
          language = "EN";
        language = language.toUpperCase();

        lead.setBSPAppl("LEAD GENERATION");
        lead.setBSPView("CONSUMER");

        String productID = item.getProductID();
        //System.out.println("productID=" + productID);
        String quantity = item.getQuantity();
        String productDescription= item.getProduct();
        String productUnit = item.getUnit();
        text=WebUtil.translate(userData.getLocale(), "lead.generated.explicit", null)
           + "\r\n" + text;
        lead.addProduct(productID, quantity, productUnit, productDescription, text);

        //String guidOfLead = getLeadIDFromSession(session);
        String guidOfLead = lead.getLeadID();
        if( guidOfLead == null){
          lead.setDescription(WebUtil.translate(userData.getLocale(),
                                "lead.generated.title", null));

          String guid =lead.createLead(language);
          // setLeadIDToSession(session, guid);
        }
        else
          lead.modifyLead();
        //java.lang.Thread.sleep(2000);
       }
      } catch(Exception ex) {
        ex.printStackTrace();
      }
    }
  }

}