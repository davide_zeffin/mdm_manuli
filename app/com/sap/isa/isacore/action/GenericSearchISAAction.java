/*****************************************************************************
    Class:        GenericSearchISAAction
    Copyright (c) 2004, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author        Ralf Witt

    $Revision: #7 $
    $DateTime: 2004/03/26 09:03:45 $ (Last changed)
    $Change: 179868 $ (changelist)

*****************************************************************************/
package com.sap.isa.isacore.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.GenericSearch;
import com.sap.isa.businessobject.GenericSearchReturn;
import com.sap.isa.businessobject.GenericSearchSearchCommand;
import com.sap.isa.businessobject.GenericSearchSelectOptions;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.ui.uiclass.genericsearch.GenericSearchUIData;
import com.sap.isa.user.action.EComExtendedBaseAction;

/**
 *
 * ISA specific part of performing the selection request 
 */
public class GenericSearchISAAction extends GenericSearchBaseAction {

	public ActionForward ecomPerform(ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response,
			UserSessionData userSessionData,
			RequestParser requestParser,
			MetaBusinessObjectManager mbom,
			boolean multipleInvocation,
			boolean browserBack)
			throws CommunicationException {
		final String METHOD_NAME = "ecomPerform()";
		log.entering(METHOD_NAME);
        BusinessObjectManager bom = (BusinessObjectManager)mbom.getBOMbyName("ISACORE-BOM");

        int handle = 1;    // This action creates only ONE request
        
        
        // check if the user is logged in.
        String forward = EComExtendedBaseAction.userCheck(userSessionData, mbom);
        if (forward != null) {
			log.exiting();
			return mapping.findForward(forward);	
        }
        
        // Add administration data to request
        addAdminDataToRequest(request, bom);
        // Use helper method for parsing
		GenericSearchSelectOptions selOpt = buildSelectOptions(request, userSessionData, handle);
		
		request.setAttribute("genericsearch.debug.selopt", selOpt);
		
		handleDarkAttributes(userSessionData, handle, selOpt, request, mbom);

		GenericSearch searchRequest = bom.createGenericSearch();

        GenericSearchSearchCommand searchOptions = new GenericSearchSearchCommand(selOpt);

        searchOptions.setBackendImplementation(getBackendImplemenation(handle, selOpt));

		GenericSearchReturn returnData = searchRequest.performGenericSearch(searchOptions);

        // Make return data available for UI presentation
        makeReturnDataAvailableForUI(userSessionData, request, returnData);
		log.exiting();
        return mapping.findForward("resultlist");
    }
    /**
     * Add some adminiatrative data to the request attributes.
     * @param request
     * @param bom
     */
    private void addAdminDataToRequest(HttpServletRequest request, BusinessObjectManager bom) {
        String field = request.getParameter(GenericSearchUIData.RC_DATEFORMAT);
        if (field == null || field.length() <= 0) {
            request.setAttribute(GenericSearchUIData.RC_DATEFORMAT, bom.getShop().getDateFormat());
        }
        field = request.getParameter(GenericSearchUIData.RC_NUMBERFORMAT);
        if (field == null || field.length() <= 0) {
            request.setAttribute(GenericSearchUIData.RC_NUMBERFORMAT, bom.getShop().getDecimalPointFormat());
        }
        field = request.getParameter(GenericSearchUIData.RC_LANGUAGE);
        if (field == null || field.length() <= 0) {
            request.setAttribute(GenericSearchUIData.RC_LANGUAGE, bom.getShop().getLanguageIso());
        }
    }
    /**
     * Overwrites the method . <br>
     * 
     * @param userSessionData
     * @param request
     * @param mbom
     * @param objs
     * @param sig
     * 
     * 
     * @see com.sap.isa.isacore.action.GenericSearchBaseAction#prepareDarkAttributes(com.sap.isa.core.UserSessionData, javax.servlet.http.HttpServletRequest, com.sap.isa.core.businessobject.management.MetaBusinessObjectManager, java.lang.Object[], java.lang.Class[])
     */
    public void handleDarkAttributes(UserSessionData userSessionData,
	                                  int handle,
									  GenericSearchSelectOptions selOpt,
        							  HttpServletRequest request,
        							  MetaBusinessObjectManager mbom) {
        if (isRePerformRequested(request) == true) {
            // Re-Performing of the last search has been requested
            return;
        }
		BusinessObjectManager bom = (BusinessObjectManager)mbom.getBOMbyName("ISACORE-BOM");

		Object objs[] = new Object[5];
		Class sig[] = new Class[5];

		// Use helper method for processing properties of type 'dark'
		// Build object list which should be used
		objs[0] = userSessionData;
		objs[1] = request;
		objs[2] = bom;
		objs[3] = selOpt;
		objs[4] = Integer.toString(handle);
		sig[0] = userSessionData.getClass();
		sig[1] = findInterface(request, "javax.servlet.http.HttpServletRequest");
		sig[2] = BusinessObjectManager.class;
		sig[3] = selOpt.getClass();
		sig[4] = Integer.toString(handle).getClass();

		processDarkAttributes(userSessionData, request, objs, sig);

    }

}