/*****************************************************************************
    Class         CreateFromPredecessorAction
    Copyright (c) 2001, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      08.06.2001
    Version:      1.0

    $Revision: #6 $
    $Date: 2002/11/05 $
*****************************************************************************/
package com.sap.isa.isacore.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.ipc.IPCBOManager;
import com.sap.isa.businessobject.item.ItemList;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.businessobject.order.PartnerListEntry;
import com.sap.isa.businessobject.ordertemplate.OrderTemplate;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.MessageDisplayer;
import com.sap.isa.isacore.action.order.MaintainBasketBaseAction;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.IPCItemReference;
import com.sap.spc.remote.client.object.imp.IPCClientObjectFactory;
import com.sap.spc.remote.client.object.imp.rfc.RFCIPCItemReference;


/**
 * Creates a document using another one.
 *
 * @author Thomas Smits
 * @version 1.0
 *
 */
public class CreateFromPredecessorAction extends IsaCoreBaseAction {
    private static final String FORWARD_SUCCESS = "editdoc";
    private static final String FORWARD_ORDER_TEMPLATE = "basket";

    /**
     * List of sales documents stored in request scope.
     */
    public static final String RK_ORDER_LIST = "orderlist";

    /**
     * Number of read document headers
     */
    public static final String RK_DOCUMENT_COUNT = "documentcount";
    
	/**
	  * Process Type of the target document
	  */
	public static final String RK_PROCESS_TYPE = "processtype";    
	
    /**
     * Check result: Check passed.
     */
    private static final char CHECK_OK = 'O';
    
    /**
     * Check result: Check failed.
     */
    private static final char CHECK_ERROR = 'E';

    /**
     * Check, if the user is allowed to see the order and if the order should be 
     * be displayed as customer order.
     * 
     * @param orderStatus
     * @param buPaMa
     * @param companyPartnerFunction partner function of the company
     * @param isHomAcitvated within hom there is one additional check.
     */
    protected char checkOrder(SalesDocument sourceDoc, BusinessPartnerManager buPaMa, String companyPartnerFunction) {
        
        HeaderSalesDocument header = sourceDoc.getHeader();

        char ret = CHECK_ERROR;
    	
        // take the company Partner Function from header	
        PartnerListEntry partnerEntry = header.getPartnerList().getPartner(companyPartnerFunction);
        
        TechKey partnerKey =
        	 (partnerEntry != null) ? partnerEntry.getPartnerTechKey() : null;

		                
        // get the default partner fromthe bu pa ma
        BusinessPartner defaultPartner =
        	 buPaMa.getDefaultBusinessPartner(companyPartnerFunction);

			 if (log.isDebugEnabled()) {
						 log.debug("Check if default partner equals header partner");				 
						 log.debug("partner function: " + companyPartnerFunction.toString());
						 log.debug("header partner: " + ((partnerKey != null) ? partnerKey.toString() : null));		 
						 log.debug("default partner: " + defaultPartner.getTechKey().toString() + ";" + defaultPartner.getTechKey());
					 }


        // check if the partner of the order is the the same as in the buPaMa
        if (partnerKey != null && defaultPartner != null
        	 && partnerKey.equals(defaultPartner.getTechKey()) ) {
         
        	 ret =  CHECK_OK;		    
        
        }
        
		if (log.isDebugEnabled()) {
				if(ret == CHECK_ERROR){
					log.debug("Partner function check failed");				 
				} else {
					log.debug("Partner function check ok");
				}
		}
        
        return ret;
    }
    
    /**
     * Overriden <em>isaPerform</em> method of <em>IsaCoreBaseAction</em>.
     */
    public ActionForward isaPerform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response, UserSessionData userSessionData, RequestParser requestParser, BusinessObjectManager bom, IsaLocation log) throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        // Page that should be displayed next.
        String        forwardTo = null;

        String source = requestParser.getParameter("source").getValue().getString();
        String target = requestParser.getParameter("target").getValue().getString();
        String techkey = requestParser.getParameter("techkey").getValue().getString();
		String processType = requestParser.getParameter(RK_PROCESS_TYPE).getValue().getString();

        SalesDocument sourceDoc = null;
        SalesDocument targetDoc = null;

        if (source.equalsIgnoreCase("ordertemplate")) {
            bom.releaseOrderTemplate();
            sourceDoc = bom.createOrderTemplate();
        }
        else if (source.equalsIgnoreCase("quotation")) {
            bom.releaseQuotation();
            sourceDoc = bom.createQuotation();
        }
        else {
        	log.exiting();
            throw new PanicException("unknown document:" + source);
        }

        if (target.equalsIgnoreCase("basket") || target.equalsIgnoreCase("order")) {
            bom.releaseBasket();
            targetDoc = bom.createBasket();
        }
        else if (target.equalsIgnoreCase("quotation")) {
            bom.releaseQuotation();
            targetDoc = bom.createQuotation();
        }
        else {
        	log.exiting();
            throw new PanicException("unknown document:" + target);
        }

        User user = bom.getUser();
        Shop shop = bom.getShop();

        //      read catalog
        WebCatInfo webCat = getCatalogBusinessObjectManager(userSessionData).getCatalog();

        TechKey    sourceKey = new TechKey(techkey);
        sourceDoc.setTechKey(sourceKey);

        if (user.isUserLogged()) {
            if (source.equalsIgnoreCase("ordertemplate")) {
                ((OrderTemplate) sourceDoc).setGData(shop, webCat);
            }

            sourceDoc.read();
        }
        else {
            sourceDoc.read();
        }

        //Check if the user is authorized to open the source document
        char check = checkOrder(sourceDoc, 
				bom.getBUPAManager(),
				shop.getCompanyPartnerFunction());
        
        if (check == CHECK_ERROR) {
            
            // create a Message Displayer for the error page!
            MessageDisplayer messageDisplayer = new MessageDisplayer();

            messageDisplayer.addToRequest(request);
            messageDisplayer.addMessage(new Message(Message.ERROR,"b2b.showDoc.userNotAllowed"));

            messageDisplayer.setAction("b2b/updatedocumentview.do");

            return mapping.findForward("message");                
        } 
        
        // get configuration of configurable products
        ItemList items = sourceDoc.getItems();

        for (int i = 0; i < items.size(); i++) {
            ItemSalesDoc itemSalesDoc = items.get(i);
            Object externalItem = itemSalesDoc.getExternalItem();
            if (itemSalesDoc.isConfigurable() && (externalItem == null || !(externalItem instanceof IPCItem)) ) {
                sourceDoc.getItemConfig(itemSalesDoc.getTechKey());

                IPCItemReference ipcItemReference = (IPCItemReference)IPCClientObjectFactory.getInstance().newIPCItemReference();
                ipcItemReference.setItemId(itemSalesDoc.getTechKey().getIdAsString());
                ipcItemReference.setDocumentId(sourceDoc.getHeader().getIpcDocumentId().toString());

                IPCBOManager               ibom = this.getIPCBusinessObjectManager(userSessionData);
                // InteractionConfigContainer data = getInteractionConfig(request);
                // String                     client = data.getConfig("ipc_scenario").getValue("client");

                try {
                	
					IPCBOManager ipcBoManager =
						(IPCBOManager) userSessionData.getBOM(IPCBOManager.NAME);
					String connectionKey = sourceDoc.getHeader().getIpcConnectionKey();
                    IPCClient ipcClient = ipcBoManager.createIPCClientUsingConnection(connectionKey);

                    IPCItem   ipcItem = ipcClient.getIPCItem(ipcItemReference);

                    if (ipcItem != null) {
                        itemSalesDoc.setExternalItem(ipcItem);
                    }
                }
                catch (IPCException ex) {
                    log.error(LogUtil.APPS_USER_INTERFACE, "Create IPCClient failed! - No IPC client available!");
                    log.error(LogUtil.APPS_USER_INTERFACE, ex.getMessage());
                }
//                catch (BackendException bex) {
//                    log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "Create IPCClient failed! - No IPC client available!");
//                    log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, bex.getMessage());
//                }
            }

            // Delete Requested Delivery Date to avoid copying (makes no sence to copy)
            itemSalesDoc.setReqDeliveryDate("");
        }
         // ENDFOR

        if (source.equalsIgnoreCase("ordertemplate")) {                        //INS 960133
            // Delete Requested Delivery Date to avoid copying (makes no sence //INS 960133
            //  to copy from order templates)                                  //INS 960133
            sourceDoc.getHeader().setReqDeliveryDate("");                      //INS 960133
        }
        
        targetDoc.setIdMapper(getAppBaseBusinessObjectManager(userSessionData).createIdMapper());
        targetDoc.init(sourceDoc, shop, bom.createBUPAManager(), webCat, processType);
        targetDoc.setState(DocumentState.TARGET_DOCUMENT);
        //targetDoc.read(user);
//        targetDoc.readHeader();
//        targetDoc.readAllItemsForUpdate();
        targetDoc.read();
        sourceDoc.destroyContent();

        // Store the list of ShipTos in session context, to allow all frames access to
        // it
        // HttpSession session = request.getSession();
        userSessionData.setAttribute(MaintainBasketBaseAction.SC_SHIPTOS, targetDoc.getShipTos());

        // Satisfy the document handler
        HeaderSalesDocument header = targetDoc.getHeader();

        ManagedDocument     managedDocument = null;

        if (target.equalsIgnoreCase("basket") || target.equalsIgnoreCase("order")) {
            managedDocument = new ManagedDocument(targetDoc, "basket", // part of ressource key for b2b.docnav.quotation
                    "new", //header.getSalesDocNumber(),  // id
                    header.getPurchaseOrderExt(), header.getDescription(), header.getCreatedAt(), FORWARD_ORDER_TEMPLATE, null, null);
            userSessionData.setAttribute(MaintainBasketBaseAction.SC_DOCTYPE, MaintainBasketBaseAction.DOCTYPE_BASKET);
        }
        else if (target.equalsIgnoreCase("quotation")) {
            managedDocument = new ManagedDocument(targetDoc, "quotation", // part of ressource key for b2b.docnav.quotation
                    "new", //header.getSalesDocNumber(),  // id
                    header.getPurchaseOrderExt(), header.getDescription(), header.getCreatedAt(), FORWARD_ORDER_TEMPLATE, null, null);
            userSessionData.setAttribute(MaintainBasketBaseAction.SC_DOCTYPE, MaintainBasketBaseAction.DOCTYPE_QUOTATION);
        }

        DocumentHandler documentHandler = (DocumentHandler) userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
        // Release GenericSearch as "document on top". This is necessary for the case, the ordering
        // process will be cancelled. Now the links on the result list could not be created correctly due
        // due missing source and target information.
        ManagedDocument mDoc = documentHandler.getManagedDocumentOnTop();
        documentHandler.release(mDoc.getDocument());

        documentHandler.add(managedDocument);
        documentHandler.setOnTop(targetDoc);

        forwardTo = FORWARD_SUCCESS;
		log.exiting();
        return mapping.findForward(forwardTo);
    }

}
