/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Alexander Staff
  Class         PageQuickSearchAction
  Description:  Action to handle the leaf through actions in the result list of the quicksearch

  Created:      27 April 2001
  Version:      1.0

  $Revision: #2 $
  $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.isacore.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.IsaQuickSearch;
import com.sap.isa.businessobject.ProductList;
import com.sap.isa.businessobject.webcatalog.CatalogBusinessObjectManager;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;

/**
 * Handle the user requests to leaf through the result list of the quicksearch.
 * We use the same action for the forward- and backward-events, as well as the
 * events that directly access a specific page.
 * The parameter defining the action to do has the name "QuickSearchPageCommand" and can
 * have the values "forward" or "backward", to scroll forward or backward, and # which
 * is a placeholder for the number of the page to display.
 * If the parameter does not exist in the request, "forward" is the default.
 *
 * <br> Note that all Action classes have to provide a non argument constructor
 * because they were instantiated automatically by the action servlet.
 *
 * @author Alexander Staff
 * @version 0.1
 *
 */
public class PageQuickSearchAction extends IsaCoreBaseAction {

    private static final int PAGE = 1;   // go to page #
    private static final int LEAF = 2;   // leaf through the search result
    private static final int SORT = 3;   // sort the search result

    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            BusinessObjectManager bom,
            IsaLocation log) {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        // Page that should be displayed next.
        String forwardTo = "success";

        // the actions to do in the search
        String direction    = "forward";    // some reasonable default-value
        int searchaction    = LEAF;         // some reasonable default-value
        int page            = 1;            // some reasonable default-value
        int curPage         = 1;            // some reasonable default-value
        int sortCol         = 0;            // some reasonable default-value

        CatalogBusinessObjectManager cbom =
                getCatalogBusinessObjectManager(userSessionData);

        // get the isaQuickSearch-Object from the bom and set some properties
        // to init it for the next search- or leaf through-command
        IsaQuickSearch isaQuickSearch = bom.getIsaQuickSearch();
        if ( isaQuickSearch == null ) {
            // error, must be there
            log.exiting();
            return mapping.findForward("error");
        }

        // get the action to do
        RequestParser.Parameter rparam = requestParser.getParameter("QuickSearchPageCommand");
        if (rparam.isSet()) {
            if (rparam.getValue().isInt()) {
                page         = rparam.getValue().getInt();
                searchaction = PAGE;
            }
            else {

                if ("SORT".equalsIgnoreCase(rparam.getValue().getString())) {
                    searchaction = SORT;

                    // use the request's parameter "sortCol" to set the current page
                    rparam = requestParser.getParameter("sortCol");
                    if (rparam.isSet()) {
                        if (rparam.getValue().isInt()) {
                            // must be int !!
                            sortCol = rparam.getValue().getInt();
                        }
                        else {
                            forwardTo = "error";
                        }
                    }
                    else {
                        forwardTo = "error";
                    }
                }
                else {
                    searchaction = LEAF;
                    direction    = rparam.getValue().getString();
                    if ( !direction.equalsIgnoreCase("forward") && !direction.equalsIgnoreCase("backward")) {
                        direction = "forward";
                    }

                    // use the request's parameter "curPage" to set the current page
                    rparam = requestParser.getParameter("curPage");
                    if (rparam.isSet()) {
                        if (rparam.getValue().isInt()) {
                            // must be int !!
                            curPage = rparam.getValue().getInt();

                            // set the current page here, just to be sure that the isaQuickSearch knows where to
                            // start with leafing through the search results
                            isaQuickSearch.setCurPage(curPage);
                        }
                        else {
                            forwardTo = "error";
                        }
                    }
                    else {
                        forwardTo = "error";
                    }
                }
            }
        }
        else {
            // nothing set
            // use the defaults, see above, variable declaration
        }

        // continue ??
        if ("success".equals(forwardTo)) {

            ProductList items = null;

            // decide what to do
            switch (searchaction) {
            case PAGE:
                items = isaQuickSearch.gotoPage( page );
                items.determinePrices( cbom.getPriceCalculator() );
                request.setAttribute(QuickSearchAction.QS_RESULT_LIST, items);
                break;
            case LEAF:
                items = isaQuickSearch.leafThrough( direction.equalsIgnoreCase("forward") ? 1 : -1 );
                items.determinePrices( cbom.getPriceCalculator() );
                request.setAttribute(QuickSearchAction.QS_RESULT_LIST, items);
                break;
            default:
                // searchaction == SORT
                items = isaQuickSearch.sort( sortCol );
                items.determinePrices( cbom.getPriceCalculator() );
                request.setAttribute(QuickSearchAction.QS_RESULT_LIST, items);
                break;
            } // endswitch(searchaction)

            // three objects are stored in the request
            // the productlist itself, the isaQuickSearch-object and the list of common attributes
            request.setAttribute(InitQuickSearchAction.QS_OBJECT, isaQuickSearch);
            request.setAttribute(InitQuickSearchAction.QS_COMMONATTRIBUTES_LIST, isaQuickSearch.getAttributes());
        }

        // everything is fine, be happy
        log.exiting();
        return mapping.findForward(forwardTo);
    }
}
