/*****************************************************************************
    Class:        GenericSearchCloseAction
    Copyright (c) 2005, SAP AG, Germany, All rights reserved.
*****************************************************************************/

package com.sap.isa.isacore.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;


/**
 * Handling to close a document in status display mode
 */
public class GenericSearchCloseAction extends EComBaseAction {

    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param mbom              Reference to the MetaBusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward ecomPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            MetaBusinessObjectManager mbom,
            boolean multipleInvocation,
            boolean browserBack)
            throws CommunicationException {

        final String METHOD_NAME = "ecomPerform()";
        log.entering(METHOD_NAME);
        DocumentHandler documentHandler = (DocumentHandler)
                userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);

        if (documentHandler != null  &&  documentHandler.getManagedDocumentOnTop() != null) {
            // Might be null if WebUser clicks the Close-Button multiple times!
            ManagedDocument mDoc = documentHandler.getManagedDocumentOnTop();
            documentHandler.release(mDoc.getDocument());
        }
        log.exiting();
        return mapping.findForward("success");
    }
        

}