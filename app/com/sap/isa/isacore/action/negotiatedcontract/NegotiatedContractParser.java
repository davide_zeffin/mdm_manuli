/*****************************************************************************
    Class         NegotiatedContractParser
    Copyright (c) 2004, SAP AG, All rights reserved.
    Description:  Standard parser methods for negotiatet contracts
    Author:       SAP AG
    Created:      July 2004
    Version:      1.0

    $Revision: #1 $
    $Date: 2004/07/29 $

*****************************************************************************/

package com.sap.isa.isacore.action.negotiatedcontract;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.isacore.action.SalesDocumentParser;
import com.sap.isa.businessobject.order.Text;
import com.sap.isa.businessobject.negotiatedcontract.NegotiatedContract;
import com.sap.isa.businessobject.header.HeaderNegotiatedContract;
import com.sap.isa.businessobject.item.ItemNegotiatedContract;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.UserSessionData;

/**
 *  This class is the default parser for standard negotiated contract
 *  and might be used for parsing the ncontract.jsp and 
 *  ncontractchange.jsp.
 */

public class NegotiatedContractParser extends SalesDocumentParser {
	
	/**
	  * Template method to be overwritten by the subclasses of this class.
	  * This method provides the possibilty, to easily extend the parsing
	  * process for the header of a negotiated contract, accordingly to special
	  * customer requirements. By default this method is empty.
	  * <code>customerExitParseRequestHeader</code> method.
	  *
	  * @param requestParser        parser to simple retrieve data from the request
	  * @param userSessionData      object wrapping the session
	  * @param header               the current header
	  * @param negotiatedContract   the current negotiated contract
	  *
	  */
	  public void customerExitParseRequestHeader(
				 RequestParser parser,
				 UserSessionData userSessionData,
				 HeaderNegotiatedContract header,
				 NegotiatedContract negotiatedContract) {
	  }
     
	/**
	 * Template method to be overwritten by the subclasses of this class.
	 * This method provides the possibilty, to easily extend the parsing
	 * process for the items of a negotiated contract, accordingly to special
	 * customer requirements. By default this method is empty.
	 * <code>customerExitParseRequestHeader</code> method.
	 *
	 * @param requestParser            parser to simple retrieve data from the request
	 * @param userSessionData          object wrapping the session
	 * @param itemNegotiatedContract   the current item edited
	 * @param itemIdx                  the index of item related fields in the request
	 * @param negotiatedContract       the current negotiated contract
	 *
	 */
	 public void customerExitParseRequestItem(
				RequestParser parser,
				UserSessionData userSessionData,
	            ItemNegotiatedContract itemNegotiatedContract,
				int itemIdx,
	            NegotiatedContract negotiatedContract) {
	 }	
	
	/**
	  * Helper method to parse the request and construct the header of
	  * a <code>NegotiatedContractDocument</code> form the
	  * request's data.
	  * <p>
	  * The following fields are taken from the request and used to
	  * construct the header:
	  * </p>
	  * <table border="1" cellspacing="0" cellpadding="2">
	  *   <tr>
	  *     <td><b>field</b></td>
	  *     <td><b>use</b></td>
	  *   </tr>
	  *   <tr>
	  *     <td>reference</td>
	  *     <td>contract number the customer entered</td>
	  *   </tr>
	  *   <tr>
	  *     <td>description</td>
	  *     <td>description the customer entered for the contract</td>
	  *   </tr>
	  *   <tr>
	  *     <td>contractStartDate</td>
	  *     <td>start date for the contract</td>
	  *   </tr>
	  *   <tr>
	  *     <td>contractEndDate</td>
	  *     <td>start date for the contract</td>
	  *   </tr>
	  *   <tr>
	  *     <td>shipCond</td>
	  *     <td>shipping conditions for the document</td>
	  *   </tr>
	  *   <tr>
	  *     <td>newmessages</td>
	  *     <td>new messages entered by the customer</td>
	  *   </tr>
	  * </table>
	  * <br><br>
	  *
	  * @param parser The request, wrapped into a parser object
	  * @param userSessionData   object wrapping the session
	  * @param header the header to fill from the request
	  * @param negotiatedContract the current negotiated contract 
	  * @param log reference to the logging context
	  * @param isDebugEnabled flag indicating wheter or not logging output
	  *        is written
	  */
	 public void parseHeader(RequestParser parser,
							 UserSessionData userSessionData,
							 HeaderNegotiatedContract header,
							 NegotiatedContract negotiatedContract,
							 IsaLocation log,
							 boolean isDebugEnabled)
						 throws CommunicationException {
						 	
	     Text text = new Text(); 
		
		 // get header fields from the request		
	 	 RequestParser.Parameter reference = parser.getParameter("reference");
		 RequestParser.Parameter description = parser.getParameter("description");
		 RequestParser.Parameter contractStartDate = parser.getParameter("contractStartDate");
		 RequestParser.Parameter contractEndDate = parser.getParameter("contractEndDate");
		 RequestParser.Parameter shipcondition = parser.getParameter("shipCond");
		 RequestParser.Parameter newmessages = parser.getParameter("newmessages");

		 text.setText(newmessages.getValue().getString());

	     // Fill the data from the JSP into the header object.
		 header.setDescription(description.getValue().getString());
		 header.setPurchaseOrderExt(reference.getValue().getString());
		 header.setContractStart(contractStartDate.getValue().getString());
		 header.setContractEnd(contractEndDate.getValue().getString());
		 header.setShipCond(shipcondition.getValue().getString());
		 header.setText(text);
		 
		 if (isDebugEnabled) {
			  log.debug("Parsed header - " + header);
		  }          
	 }	

	/**
	 * Helper method to parse the request and construct items
	 * for the <code>SalesDocument</code> form the
	 * request's data.
	 * This method can be used for order.jsp and orde_change.jsp.
	 * If the parameter isOrderChange is set to true, 
	 * <p>
	 * The following fields are taken from the request and used to
	 * construct the item:
	 * </p>
	 * <table border="1" cellspacing="0" cellpadding="2">
	 *   <tr>
	 *     <td><b>field</b></td>
	 *     <td><b>use</b></td>
	 *   </tr>
	 *   <tr>
	 *     <td>itemTechKey[]</td>
	 *     <td>technical key of the item, only relevant for items already added to the document</td>
	 *   </tr>
	 *   <tr>
	 *     <td>product[]</td>
	 *     <td>product number</td>
	 *   </tr>
	 *   <tr>
	 *     <td>targetquantity[]</td>
	 *     <td>target quantity of the product</td>
	 *   </tr>
	 *   <tr>
	 *     <td>targetvalue[]</td>
	 *     <td>target value of the product</td>
	 *   </tr>
	 * 	 <tr>
	 *     <td>target unit[]</td>
	 *     <td>the unit of the product</td>
	 *   </tr>
	 *   <tr>
	 *     <td>units[]</td>
	 *     <td>the unit of the product</td>
	 *   </tr>
	 *   <tr>
	 *     <td>delete[]</td>
	 *     <td>items to be deleted from the order</td>
	 *   </tr>
	 *   <tr>
	 *     <td>cancel[]</td>
	 *     <td>items to be cancelled from the order</td>
	 *   </tr>
	 *   <tr>
	 *     <td>comment[]</td>
	 *     <td>addtitional comments for the item entered by the customer</td>
	 *   </tr>
	 *   <tr>
	 *     <td>conditionType[]</td>
	 *     <td>condition type for the price/rebate of the item</td>
	 *   </tr>
	 *   <tr>
	 *     <td>conditionValue[]</td>
	 *     <td>condition value (price/rebate) of the item</td>
	 *   </tr>
	 *   <tr>
	 *     <td>conditionQuantity[]</td>
	 *     <td>quantity of condition of the product (e.g. price per 10 pc)</td>
	 *   </tr>
	 *   <tr>
	 *     <td>conditionUnit[]</td>
	 *     <td>unit of the condition quantity of the item</td>
	 *   </tr>
	 *   <tr>
	 *     <td>conditionId[]</td>
	 *     <td>identifier of the condition</td>
	 *   </tr>
	 * </table>
	 * <br><br>
	 * @param parser The request, wrapped into a parser object
	 * @param userSessionData   object wrapping the session
	 * @param negotiatedContract the current negotiated contract
	 * @param log reference to the logging context
	 * @param isDebugEnabled flag indicating whether or not logging output
	 *        is written
	 */
	public void parseItems (RequestParser parser,
							UserSessionData userSessionData,
							NegotiatedContract negotiatedContract,
							IsaLocation log,
							boolean isDebugEnabled) {
		Text text = null;
							
		negotiatedContract.clearItems();
		
	 	// Read all positions from the JSP
	 	RequestParser.Parameter itemTechKey        = parser.getParameter("itemTechKey[]");
	 	RequestParser.Parameter product            = parser.getParameter("product[]");
		RequestParser.Parameter quantity           = parser.getParameter("targetquantity[]");
		RequestParser.Parameter targetValue        = parser.getParameter("targetvalue[]");        
		RequestParser.Parameter unit               = parser.getParameter("targetunit[]");
	 	RequestParser.Parameter delete    		   = parser.getParameter("delete[]");
	 	RequestParser.Parameter cancel     		   = parser.getParameter("cancel[]");
	 	RequestParser.Parameter comment            = parser.getParameter("comment[]");
	 	RequestParser.Parameter conditionType      = parser.getParameter("conditionType[]");
	 	RequestParser.Parameter conditionValue     = parser.getParameter("conditionValue[]");
	 	RequestParser.Parameter conditionQuantity  = parser.getParameter("conditionQuantity[]");
	 	RequestParser.Parameter conditionUnit      = parser.getParameter("conditionUnit[]");
	 	RequestParser.Parameter conditionId        = parser.getParameter("conditionId[]");

	 	// Fill the data from the JSP in the item objects
	 	int numLines = product.getNumValues();

	 	for (int i=0 ; i < numLines; i++) {
	   		if ( product.getValue(i).getString().length() > 0) {
		 		if ( !delete.getValue(i).isSet()) {
		   			ItemNegotiatedContract itm = new ItemNegotiatedContract( new TechKey( itemTechKey.getValue(i).getString()));
		   			itm.setProduct(product.getValue(i).getString());
		   			itm.setQuantity(quantity.getValue(i).getString());
		   			itm.setUnit(unit.getValue(i).getString().toUpperCase());
		   			itm.setTargetValue(targetValue.getValue(i).getString());
		   			itm.setCondIdInquired(conditionId.getValue(i).getString());
		   			itm.setCondPriceUnitInquired(conditionQuantity.getValue(i).getString());
		   			itm.setCondRateInquired(conditionValue.getValue(i).getString());
		   			itm.setCondUnitInquired(conditionUnit.getValue(i).getString());
		   			itm.setCondTypeInquired(conditionType.getValue(i).getString());
		   			if (cancel.isSet()) {
			 			itm.setCancelable(true);
					}			 			
		   			if (comment.isSet()) {
//			 			itm.setText( new Text ("SU99", comment.getValue(i).toString()));
                        text = new Text();
                        text.setText(comment.getValue(i).toString());          
                        itm.setText(new Text());
           			}
		   			customerExitParseRequestItem(parser, userSessionData, itm, i, negotiatedContract);		   
		   			negotiatedContract.addItem(itm);	
					if (isDebugEnabled) {
						 log.debug("Added item to document - " + itm);
					 }
		 		}
	   		}
	 	}	
	}						
}
