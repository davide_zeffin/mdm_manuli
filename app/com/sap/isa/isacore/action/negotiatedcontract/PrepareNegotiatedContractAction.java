/*****************************************************************************
    Class         		PrepareNegotiatedContractAction
    Copyright (c)	2002, SAP AG, All rights reserved.
    Description:
    Author:			SAP AG
    Created:      	14. November 2002
    Version:      	1.0
*****************************************************************************/

package com.sap.isa.isacore.action.negotiatedcontract;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.businessobject.header.HeaderNegotiatedContract;
import com.sap.isa.businessobject.negotiatedcontract.ManagedDocumentNegContract;
import com.sap.isa.businessobject.negotiatedcontract.NegotiatedContract;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.History;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 *
 * @author  SAP
 * @version 1.0
 *
 */
public class PrepareNegotiatedContractAction extends IsaCoreBaseAction {

    private static final String FORWARD_NEGOTIATEDCONTRACT = "negotiatedcontract_display";
    private static final String FORWARD_CONTRACT_NOT_FOUND = "contractnotfound";
  
    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser parser,
            BusinessObjectManager bom,
            IsaLocation log) throws CommunicationException {

		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
		boolean isDebugEnabled = log.isDebugEnabled();
		
        RequestParser.Parameter contractSelected = null;
        String contractSelectedValue;
        TechKey contractKey = null;
        
        // read the shop data...
        Shop shop = bom.getShop();
        if (shop == null) {
        	log.exiting();
            throw new PanicException("shop.notFound");
        }

        // are contracts allowed?
        if (!shop.isContractAllowed()) {
        	log.exiting();
            return mapping.findForward(FORWARD_CONTRACT_NOT_FOUND);
        }

        // get user
        User user = bom.getUser();
        if (user == null) {
        	log.exiting();
            return mapping.findForward("login");
        }

        // read catalog
        WebCatInfo webCat =
                getCatalogBusinessObjectManager(userSessionData).getCatalog();
        // Ask the business object manager to create a negotiatedcontract.
        NegotiatedContract negotiatedcontract = bom.createNegotiatedContractStatus(); // TODO Fehlerbehandlung
        if (isDebugEnabled) {
			log.debug(" + Create a NegotiatedContactStatus. *");
        	if(bom.getNegotiatedContractStatus()==null) { log.debug(" + BOM has no NegotiatedContractStatus. *"); }
        	else  { log.debug(" + BOM holds a NegotiatedContractStatus. *"); } 
        }
        //NegotiatedContractCreate negotiatedcontractCreate = new NegotiatedContractCreate(negotiatedcontract);

        // initialisation of the negotiatedcontract
        negotiatedcontract.clearMessages();
        

        
        // determine selected contract from release context
        contractSelected = parser.getParameter(NegotiatedContractConstants.CONTRACT_SELECTED);
        contractSelectedValue = contractSelected.isSet() ?
            contractSelected.getValue().getString() : "";
        contractKey = new TechKey(contractSelectedValue);
        
        // set the ID of the contract in the header
        HeaderNegotiatedContract negotiatedcontractHeader = (HeaderNegotiatedContract) negotiatedcontract.getHeader();
        negotiatedcontract.setTechKey(contractKey);
        negotiatedcontractHeader.setTechKey(contractKey);
        negotiatedcontract.setGData(shop, webCat);  
		negotiatedcontract.readHeader();
		// TODO: Fehlerbehandlung, was ist, wennn der Kontrakt nicht gelesen werden konnte 
		
		DocumentHandler documentHandler = (DocumentHandler)
			 userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
        
        if (negotiatedcontract.getHeader().isStatusReleased()) {
        // status is already released -> forward display of released contract
        
		// remove from history
			History history = documentHandler.getHistory();
			history.remove("ncontract", negotiatedcontract.getHeader().getSalesDocNumber());
           
        	bom.releaseNegotiatedContractStatus();
     	
			return mapping.findForward("contract");       	
        }
        negotiatedcontract.setState(DocumentState.VIEW_DOCUMENT);      
        ManagedDocumentNegContract managedDocument = new ManagedDocumentNegContract(
            negotiatedcontract,
            "ncontract",  // part of ressource key for b2b.docnav.quotation
			negotiatedcontractHeader.getSalesDocNumber(),
			negotiatedcontractHeader.getPurchaseOrderExt(),
			negotiatedcontractHeader.getDescription(),
			negotiatedcontractHeader.getContractStart(),
            FORWARD_NEGOTIATEDCONTRACT,
			"b2b/preparenegotiatedcontract.do",
			NegotiatedContractConstants.CONTRACT_SELECTED + "=" + negotiatedcontract.getTechKey().getIdAsString());
        managedDocument.setChangeMode(false); //contract should be editable
        documentHandler.add(managedDocument);
        documentHandler.setOnTop(negotiatedcontract); 
		log.exiting();
        return mapping.findForward("success");
    }

}