/*****************************************************************************
    Class         ChangeNegotiatedContractStatusAction
    Copyright (c) 2002, SAP AG, Germany, All rights reserved.
    Description:
    Author:       SAP AG
    Created:      21. Januar 2003
    Version:      1.0
*****************************************************************************/

package com.sap.isa.isacore.action.negotiatedcontract;

import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.header.HeaderNegotiatedContract;
import com.sap.isa.businessobject.negotiatedcontract.ManagedDocumentNegContract;
import com.sap.isa.businessobject.negotiatedcontract.NegotiatedContract;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.MessageList;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 * Changes the status of  a negotiated contract to <i>accepted</i>,
 * <i>rejected</i> or put it in <i>change</i> mode.
 * <p>
 * <bf>Precondition:</bf><br>The contract must be the document on top. For
 * changing to <i>Accept</i> or <i>Reject</i> it must have the status
 * <i>Quotation</i>, for putting it into change mode it must be changeable.
 * <p>
 * The action must be called with one of the request parameters
 * <code>accept=true, reject=true, change=true</code>.
 * <p>
 * The following forwards are used by this action:
 * </p>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   </tr>
 *   <tr>
 * 	<td>display</td>
 * 	<td>Forward				 used if the status cannot be changed. The contract
 * is displayed again with an error message.</td>
 *   </tr>
 *   <tr>
 * 	<td>NegotiatedContractConstants.	FORWARD_UPDATEDOCUMENT
 * ("updatedocumentview")</td>
 * 	<td>Forward					 used if an error occurs, which forces the
 * action to stop.</td>
 *   </tr>
 *   <tr>
 * 	<td>NegotiatedContractConstants.FORWARD_CLOSE ("close")</td>
 * 	<td>Forward							 used if the status could be changed to
 * <i>Accepted</i> or <i>Rejected</i>.
 * </td>
 *   </tr>
 *   <tr>
 * 	<td>NegotiatedContractConstants.FORWARD_CHANGE ("change")</td>
 * 	<td>Forward											 used if the status
 * could be put into change mode.
 * </td>
 *   </tr>
 * </table>
 * 
 * @author SAP AG
 * @version 1.0
 *
 */

public class ChangeNegotiatedContractStatusAction extends IsaCoreBaseAction {
    
    // Forward for the created ManagedDocument, if the contract is put to change mode
    private static final String FORWARD_NEGOTIATEDCONTRACT = "negotiatedcontract_change"; 
	private static final String FORWARD_DISPLAY = "display"; 

    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser parser,
            BusinessObjectManager bom,
            IsaLocation log) throws CommunicationException {
                
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);   
		boolean isDebugEnabled = log.isDebugEnabled();

		// ------------------- some basic error checking  ------------------- 
        // Page that should be displayed next.
        String forwardTo = null;

        // Get the shop from the BOM
        Shop shop = bom.getShop();
        if (shop == null) {
        	log.exiting();
          	throw new PanicException("No shop returned from bom");
        }       
        
        DocumentHandler documentHandler = (DocumentHandler)userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
		if (documentHandler == null) {
			log.exiting();
			throw new PanicException("No document handler found.");
		}

        ManagedDocument mdoc = documentHandler.getManagedDocumentOnTop();

        if (mdoc == null) {
           // use global forward to show valid frame
		   if (isDebugEnabled) { log.debug("The DocumentHandler has no ManagedDocument on top."); }
		   log.exiting();
           return mapping.findForward(NegotiatedContractConstants.FORWARD_UPDATEDOCUMENT);
        }        
        
        // Ask the business object manager for the negotiatedcontract in display mode.
        NegotiatedContract negotiatedcontractstatus = bom.getNegotiatedContractStatus();
        if (negotiatedcontractstatus == null) {
        	if (isDebugEnabled) { log.debug("The bom cannot give me a NegotiatedContractStatus."); }
			documentHandler.release(mdoc.getDocument());       
			log.exiting();
			return mapping.findForward(NegotiatedContractConstants.FORWARD_UPDATEDOCUMENT);
        }          

		HeaderNegotiatedContract header = (HeaderNegotiatedContract) negotiatedcontractstatus.getHeader();
		if (header == null) {
			if (isDebugEnabled) { log.debug("The NeotiatedContract has no header. This looks like a serious problem"); }
			documentHandler.release(mdoc.getDocument());        
			log.exiting();
			return mapping.findForward(NegotiatedContractConstants.FORWARD_UPDATEDOCUMENT);
		}

		// Find out if the document on top is a managed document.
		if (mdoc.getDocument() != negotiatedcontractstatus) {
			if (isDebugEnabled) { log.debug("The document on top is not a negotiated contract status, but it should be."); }
			documentHandler.release(mdoc.getDocument());        
			log.exiting();
			return mapping.findForward(NegotiatedContractConstants.FORWARD_UPDATEDOCUMENT);
		}          

		// ------------------- Accept a contract  ------------------- 
        if ((request.getParameter("accept") != null) && (request.getParameter("accept").length() > 0)) {
			if (isDebugEnabled) { log.debug("The contract should be accepted ."); }
            if (header.isStatusQuotation()) {
				negotiatedcontractstatus.readForUpdate();
                if (negotiatedcontractstatus.isValid() == true) {
    				negotiatedcontractstatus.save(NegotiatedContractConstants.PROCESSTYPE_ACCEPT, shop);
	   			    forwardTo = NegotiatedContractConstants.FORWARD_CLOSE;
                } else {
                    forwardTo = FORWARD_DISPLAY;
                }
            }
            else {
				if (isDebugEnabled) { log.debug("The negotiated contract may not be accepted ."); }
				// display the document again, with an error message, that this operation is not allowed
				request.setAttribute(NegotiatedContractConstants.RC_HEADERMESSAGE, "b2b.nct.error.accept");
             	forwardTo = FORWARD_DISPLAY;
            }
        }

		// ------------------- Reject a contract  ------------------- 
        else if ((request.getParameter("reject") != null) && (request.getParameter("reject").length() > 0)) {
			if (isDebugEnabled) { log.debug("The contract should be rejected ."); }
            if (header.isStatusQuotation()) {
				negotiatedcontractstatus.readForUpdate();
                if (negotiatedcontractstatus.isValid() == true) {
				    negotiatedcontractstatus.save(NegotiatedContractConstants.PROCESSTYPE_REJECT, shop);
				    forwardTo = NegotiatedContractConstants.FORWARD_CLOSE;
                } 
                else {
                     forwardTo = NegotiatedContractConstants.FORWARD_SUCCESS;
                }
            }
            else {
				if (isDebugEnabled) { log.debug("The negotiated contract may not be rejected ."); }
				// display the document again, with an error message, that this operation is not allowed
				request.setAttribute(NegotiatedContractConstants.RC_HEADERMESSAGE, "b2b.nct.error.reject");
				forwardTo = FORWARD_DISPLAY;
            }
        } 
        
		// ------------------- put the contract in change mode  ------------------- 
		else if ((request.getParameter("change") != null) && (request.getParameter("change").length() > 0)) {
			if (isDebugEnabled) { log.debug("The contract should be changed."); }
			

			if (header.isStatusQuotation()){
				negotiatedcontractstatus.setStatusInquiry(); 
                if (negotiatedcontractstatus.isValid() == false) {
                   forwardTo = NegotiatedContractConstants.FORWARD_SUCCESS;                   
                }                 
            }
      		forwardTo = NegotiatedContractConstants.FORWARD_CHANGE;

			// We must create a new editable contract with the BOM
			// and new new ManagedDocument for the editable contract.
			// After initializing the new objects, we release the old ones.
				
			// Create a new negotiatedcontract for editing.
			NegotiatedContract negotiatedcontract = bom.createNegotiatedContract();
			if (negotiatedcontract == null) {
			    if (isDebugEnabled) { log.debug("The bom cannot create a NegotiatedContract object."); }
				   throw new PanicException("BOM cannot create a NegotiatedContract object.");
			}          
			    // Copy the Techkey
			negotiatedcontract.setTechKey(negotiatedcontractstatus.getTechKey());
            // set dirty flag in order to force to read the contract from backend
            negotiatedcontract.setDirty();
                
            negotiatedcontract.readForUpdate();  
            if (!negotiatedcontract.getHeader().isChangeable()) {
                    
                if (log.isDebugEnabled()) { log.debug("The contract is not changeable."); }
               
                // display the document , with an error message, that this operation is not allowed
                request.setAttribute(NegotiatedContractConstants.RC_HEADERMESSAGE, "b2b.nct.error.change"); 
                MessageList messageList = negotiatedcontract.getMessageList();
                 
                // read the contract in display mode and add messages
                negotiatedcontractstatus.readHeader();
                Iterator i = messageList.iterator();
                while (i.hasNext()) {
                   negotiatedcontractstatus.addMessage((Message)i.next());
                }

                negotiatedcontractstatus.readAllItems();                                  
                forwardTo = NegotiatedContractConstants.FORWARD_DISPLAY;                   

                bom.releaseNegotiatedContract();
             }
                else {
	               // create a new ManagedDocument
	               negotiatedcontract.setState(DocumentState.TARGET_DOCUMENT);
	               ManagedDocumentNegContract managedDocument = new ManagedDocumentNegContract(
	                    negotiatedcontract,
	                    "ncontract",  // part of ressource key for b2b.docnav.quotation
					   header.getSalesDocNumber(),
					   header.getPurchaseOrderExt(),
					   header.getDescription(),
					   header.getContractStart(),
	                   FORWARD_NEGOTIATEDCONTRACT,
	                   null,
	                   null);

				    if (managedDocument == null) {
					   if (isDebugEnabled) { log.debug("Cannot create a new ManagedDocument."); }
					   throw new PanicException("Cannot create a new ManagedDocument.");
				    }
				          
	               managedDocument.setChangeMode(true); //contract should be editable
	
				    // Clean up the old objects and register the new
				    documentHandler.release(mdoc.getDocument());        
	
	               documentHandler.add(managedDocument);
	               documentHandler.setOnTop(negotiatedcontract);            
	
				   bom.releaseNegotiatedContractStatus();        
                }
        	//}
		}
		log.exiting();
        return mapping.findForward(forwardTo);       
    }

}
