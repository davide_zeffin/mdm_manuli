/*****************************************************************************
  Class:        NegotiatedContractConstants
  Copyright:    (c) 2001, SAP AG, Germany, All rights reserved.
  Author:
  Created:      6.12.2002
  Version:      1.0

  $Revision: #8 $
  $Date: 2002/08/21 $
*****************************************************************************/

package com.sap.isa.isacore.action.negotiatedcontract;


public class NegotiatedContractConstants {

     /**
     * Request context constant for storage of the items of the document, value
     * is &quot;items&quot;
     */
    public static final String RC_ITEMS      = "items";

     /**
     * Request context constant for storage of the header of the document, value
     * is &quot;hdr&quot;
     */
    public static final String RC_HEADER     = "hdr";

    /**
     * Request context constant for storage of the available shipping
     * conditions, value is &quot;shipcond&quot;
     */
    public static final String RC_SHIPCOND   = "shipcond";
    
    /**
     * Request context constant for transaction type of the contract, value is
     * &quot;ncontracttransactiontype&quot;.
     */
    public static final String RC_TRANSACTION_TYPE = "ncontracttransactiontype";
    
    /**
     * Request context constant for storage of the available contract types,
     * value is &quot;contracttype&quot;
     */
    public static final String RC_CONTRACT_TYPE     = "contracttype";    

	/**
	 * Request context constant for storage of errors and warnings, value is
	 * &quot;messages&quot;
	 */
	public final static String RC_MESSAGES   = "messages";

	/**
	 * Request context constant for an error message, which is displayed in the
	 * header, value is &quot;headermessage&quot;.
	 */
	public final static String RC_HEADERMESSAGE   = "headermessage";

    /**
    * Request context constant for techkey, value is &quot;
    * techkey&quot;
    */
    public static final String TECH_KEY      = "techkey";

    /**
     * Constant for available forwards, value is &quot;updatedocumentview&quot;.
     */
    public static final String FORWARD_UPDATEDOCUMENT     = "updatedocumentview";

    /**
     * Constant for available forwards, value is &quot;error&quot;.
     */
    public static final String FORWARD_ERROR     = "error";

    /**
     * Constant for available forwards, value is &quot;success&quot;.
     */
    public static final String FORWARD_SUCCESS     = "success";

    /**
     * Constant for available forwards, value is &quot;save&quot;.
     */
    public static final String FORWARD_SAVE     = "save";

	/**
	 * Constant for available forwards, value is &quot;update&quot;.
	 */
	public static final String FORWARD_UPDATE     = "update";

	/**
	 * Constant for available forwards, value is &quot;display&quot;.
	 */
	public static final String FORWARD_DISPLAY     = "display";


    /**
     * Constant for available forwards, value is &quot;saveconfirm&quot;.
     */
    public static final String FORWARD_SAVECONFIRM     = "saveconfirm";

    /**
     * Constant for available forwards, value is &quot;sendconfirm&quot;.
     */
    public static final String FORWARD_SENDCONFIRM     = "sendconfirm";
    
    /**
     * Constant for available forwards, value is &quot;change&quot;.
     */
    public static final String FORWARD_CHANGE     = "change";
    
    /**
     * Constant for available forwards, value is &quot;close&quot;.
     */
    public static final String FORWARD_CLOSE      = "close";  
    
    /**
     * Constant for available forwards, value is &quot;buttons&quot;.
     */
    public static final String FORWARD_BUTTONS      = "buttons";   

    /**
     * Constant for process type: save a negotiated contract for processing.
     * Value is 1.
     */
    public static final int PROCESSTYPE_SEND     = 1;

    /**
     * Constant for process type: save a negotiated contract without processing.
     * Value is 2.
     */
    public static final int PROCESSTYPE_SAVE     = 2;
    
    /**
     * Constant for process type: save a negotiated contract for accepting.
     * Value is 3.
     */
    public static final int PROCESSTYPE_ACCEPT     = 3;
    
    /**
     * Constant for process type: save a negotiated contract for rejecting.
     * Value is 4.
     */
    public static final int PROCESSTYPE_REJECT     = 4;    

	/**
	 * Name of the parameter for the contract id for the
	 * PrepareNegotiatedContractAction, value is &quot;contractSelected&quot;.
	 */
	public static final String CONTRACT_SELECTED = "contractSelected";

}
