/*****************************************************************************
    Class         ShowNegotiatedContractAction
    Copyright (c) 2002, SAP AG, Germany, All rights reserved.
    Description:
    Author:       SAP AG
    Created:      23. October 2002
    Version:      1.0
*****************************************************************************/

package com.sap.isa.isacore.action.negotiatedcontract;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.header.HeaderNegotiatedContract;
import com.sap.isa.businessobject.negotiatedcontract.ManagedDocumentNegContract;
import com.sap.isa.businessobject.negotiatedcontract.NegotiatedContract;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.MessageDisplayer;
import com.sap.isa.isacore.action.ActionConstants;
import com.sap.isa.isacore.action.IsaCoreBaseAction;


/**
 * Asks the DocumentHandler for the document on top and if this document
 * is a NegotiatedContract, it reads the contract data from the backend
 * and puts it in the request context.
 * <p>
 * It also looks for a request parameter <code>modus</code>. If this parameter
 * is <code>display</code> it reads header and item data from the backend; if
 * the value is <code>buttons</code> only the header data will be read. If the
 * action is called without this parameter, the contract is read for update
 * (locked in the backend).
 * <p>
 * The following forwards are used by this action:
 * </p>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td width="50%"><b>forward</b></td>
 *     <td width="50%"><b>description</b></td>
 *   </tr>
 *   <tr>
 * 	<td>NegotiatedContractConstants.	FORWARD_UPDATEDOCUMENT
 * ("updatedocumentview")</td>
 * 	<td>Forward			 used if something goes wrong to allow application to
 * reach an consistent state again.</td>
 *   </tr>
 *   <tr>
 * 	<td>NegotiatedContractConstants.FORWARD_DISPLAY ("display")</td>
 * 	<td>Forward			 used if everthing is ok and parameter
 * <code>modus=display</code> was set.</td>
 *   </tr>
 *   <tr>
 * 	<td>NegotiatedContractConstants.FORWARD_BUTTONS ("buttons")</td>
 * 	<td>Forward			 used if everthing is ok and parameter
 * <code>modus=buttons</code> was set.</td>
 *   </tr>
 *   <tr>
 * 	<td>NegotiatedContractConstants.FORWARD_CHANGE ("change")</td>
 * 	<td>Forward			 used if everthing is ok and no parameter
 * <code>modus</code> was set.</td>
 *   </tr>
 *   <tr>
 * 	<td>NegotiatedContractConstants.FORWARD_ERROR ("error")</td>
 * 	<td>Forward			 used if contract should be displayed in change mode but
 * is not changeable, because meanwhile it was locked in the backend.</td>
 *   </tr>
 * </table>
 *
 * @author SAP AG
 * @version 1.0
 *
 */
public class ShowNegotiatedContractAction extends IsaCoreBaseAction {

    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser parser,
            BusinessObjectManager bom,
            IsaLocation log) throws CommunicationException {

		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        boolean isDebugEnabled = log.isDebugEnabled();

        // ------------------- some basic error checking  ------------------- 

        // Page that should be displayed next.
        String forwardTo = null;
		String contractType = ""; 
		String processType;  
        

        Shop shp = bom.getShop();
        if (shp == null) {
        	log.exiting();
          	throw new PanicException("No shop returned from bom");
        }

        DocumentHandler documentHandler = (DocumentHandler) userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
        if (documentHandler == null) {
        	log.exiting();
            throw new PanicException("No document handler found.");
        }

		// Check if there is a document on top.
        ManagedDocument mdoc = documentHandler.getManagedDocumentOnTop();
        if (mdoc == null) {
           // use global forward to show valid frame
           if (isDebugEnabled) { log.debug("The DocumentHandler has no ManagedDocument on top."); }
           log.exiting();
           return mapping.findForward(NegotiatedContractConstants.FORWARD_UPDATEDOCUMENT);
        }        

        // Check if the ManagedDocument on top is ManagedDocumentNegContract
        ManagedDocumentNegContract mandoc;
        if (!(mdoc instanceof ManagedDocumentNegContract)) {
            if (isDebugEnabled) { log.debug("The ManagedDocument is not of type ManagedDocumentNegContract."); }
            documentHandler.release(mdoc.getDocument());
            log.exiting();
            return mapping.findForward(NegotiatedContractConstants.FORWARD_UPDATEDOCUMENT);
        }
        mandoc = (ManagedDocumentNegContract) mdoc;

		// Check if the ManagedDocument holds a NegotiatedContract
        NegotiatedContract negotiatedcontract;
        if (!(mandoc.getDocument() instanceof NegotiatedContract)) {
            if (isDebugEnabled) { log.debug("The document on top is not a NegotiatedContract. So we are in the wrong action."); }
            // Perhaps we can go further on, if we release the document at the document handler,
            // but a problem could be, that we couldn't release it at the bom.			
            documentHandler.release(mandoc.getDocument());
            log.exiting();
            return mapping.findForward(NegotiatedContractConstants.FORWARD_UPDATEDOCUMENT);
        }
        negotiatedcontract = (NegotiatedContract) mandoc.getDocument();	


        // ------------------- Main  ------------------- 

        // find out in which mode we are called and determine the according forward.
        RequestParser.Parameter modus = parser.getParameter("modus");
        if (isDebugEnabled) { log.debug("RequestParameter modus: " + modus.getValue().getString()); }

        if (modus.getValue().getString().equals("display")) {
            forwardTo = NegotiatedContractConstants.FORWARD_DISPLAY;
        }
        else if (modus.getValue().getString().equals("buttons")) {
            forwardTo = NegotiatedContractConstants.FORWARD_BUTTONS;
        }
        else {
            forwardTo = NegotiatedContractConstants.FORWARD_CHANGE;
        }
        
		// read required data from backend        
        if ( modus.getValue().getString().equals("buttons")) {
            // for displaying the right buttons (like [accept] and [reject]), we need some header data
            negotiatedcontract.readHeader();
        } 
        else  if (mandoc.getChangeMode() == true) {
            // the negotiated contract is changeable
            negotiatedcontract.setDirty();   
            negotiatedcontract.readForUpdate();
        }
        else {
            // the negotiated contract will only be displayed, not changed
            negotiatedcontract.readHeader();
            negotiatedcontract.readAllItems();
        }
        
        negotiatedcontract.getHeader().setDocumentTypeNegotiatedContract();

		// determine the contract type (like value contract or quantity contract) from request context
		processType = negotiatedcontract.getHeaderData().getProcessType();

		ResultData processTypes = shp.getContractNegotiationProcessTypes();
		if (processTypes.first()) {
		   if ( (processTypes.searchRowByColumn(1, processType)) == true) {
			  contractType = processTypes.getString(3); 
		   }
		}		
		if (contractType.length() > 0) {
			negotiatedcontract.getHeaderData().setProcessTypeUsage(contractType);
		}        
        
        if (( mandoc.getChangeMode() == true) && !negotiatedcontract.getHeader().isChangeable()){
			// Actually the negotiated contract should be changeable, 
			// but if someone locks the contract in the backend 
			// just in the period between generating the [change] button and the click of that button
			// then we have this case and show an error             
            MessageDisplayer messageDisplayer = new MessageDisplayer(NegotiatedContractConstants.FORWARD_CLOSE, false);
            //messageDisplayer.setOnlyLogin();
            messageDisplayer.setAction("/b2b/closenegotiatedcontract.do?history=true");
			messageDisplayer.copyMessages(negotiatedcontract);
            request.setAttribute(ActionConstants.RC_MESSAGES, negotiatedcontract.getMessageList());                 
            messageDisplayer.addToRequest(request);               
            forwardTo = NegotiatedContractConstants.FORWARD_ERROR;
        }
        else {
            request.setAttribute(NegotiatedContractConstants.RC_HEADER, negotiatedcontract.getHeader());
            request.setAttribute(NegotiatedContractConstants.RC_ITEMS, negotiatedcontract.getItems());
            request.setAttribute(NegotiatedContractConstants.RC_CONTRACT_TYPE, ((HeaderNegotiatedContract)negotiatedcontract.getHeader()).getContractType(shp)); 
            request.setAttribute(NegotiatedContractConstants.TECH_KEY, negotiatedcontract.getTechKey());
            request.setAttribute(NegotiatedContractConstants.RC_SHIPCOND, negotiatedcontract.readShipCond(shp.getLanguage()));
            request.setAttribute(NegotiatedContractConstants.RC_MESSAGES, negotiatedcontract.getMessageList());
        }
        
        resolveMessageKeys(request.getSession(), negotiatedcontract, log, log.isDebugEnabled());
		log.exiting();
        return mapping.findForward(forwardTo);

    }

}