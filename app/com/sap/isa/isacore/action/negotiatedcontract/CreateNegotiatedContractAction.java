/*****************************************************************************
	Class: 				CreateNegotiatedContractAction
    Copyright: 		(c) 2002, SAP AG, Germany, All rights reserved.
    Description:
    Author:       	SAP AG
    Created:      	21.10.2002
    Version:      	1.0
*****************************************************************************/
package com.sap.isa.isacore.action.negotiatedcontract;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.negotiatedcontract.ManagedDocumentNegContract;
import com.sap.isa.businessobject.negotiatedcontract.NegotiatedContract;
import com.sap.isa.businessobject.order.PartnerList;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.action.order.CreateSalesDocumentBase;
import com.sap.isa.isacore.action.order.MaintainBasketBaseAction;

/**
 * Creates a new negotiated contract with a corresponding ManagedDocument of
 * type <i>ManagedDocumentNegContract</i> and adds it to the DocumentHandler.
 * The action can be called with the request parameter
 * <code>ncontracttransactiontype</code> and a valid transaction type for
 * negotiated contracts from the CRM.
 * <p>
 * The following forwards are used by this action:
 * </p>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   </tr>
 *   <tr>
 * 	<td>NegotiatedContractConstants.	FORWARD_UPDATEDOCUMENT
 * ("updatedocumentview")</td>
 * 	<td>Forward					 used if an error occurs, which forces the
 * action to stop.</td>
 *   </tr>
 *   <tr>
 * 	<td>FORWARD_SUCCESS ("success")</td>
 * 	<td>Forward	used if everything works just fine.</td>
 *   </tr>
 * </table>
 * 
 * @author SAP AG
 * @version 1.0
 *
 */

public class CreateNegotiatedContractAction extends CreateSalesDocumentBase {

	/**
	 * Forward used, if everthing seems to be ok, value is &quot;success&quot;.
	 */
    private static final String FORWARD_SUCCESS = "success";

	/**
	 * Forward used in the newly created ManagedDocument, value is &quot;
	 * negotiatedcontract_change&quot;.
	 */
    private static final String FORWARD_NEGOTIATEDCONTRACT = "negotiatedcontract_change";


	/**
	 * Implement this method to add functionality to your action.
	 *
	 * @param form              The <code>FormBean</code> specified in the
	 *                          config.xml file for this action
	 * @param request           The request object
	 * @param response          The response object
	 * @param userSessionData   Object wrapping the session
	 * @param requestParser     Parser to simple retrieve data from the request
	 * @param bom               Reference to the BusinessObjectManager
	 * @param log               Reference to the IsaLocation, needed for logging
	 * @return Forward to another action or page
	 */
    public ActionForward isaPerform(ActionMapping mapping,
                ActionForm form,
                HttpServletRequest request,
                HttpServletResponse response,
                UserSessionData userSessionData,
                RequestParser requestParser,
                BusinessObjectManager bom,
                IsaLocation log) throws CommunicationException {

		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        RequestParser.Parameter transactionType = null;
        String transactionTypeValue;    
		String contractType = ""; 
        boolean isDebugEnabled = log.isDebugEnabled();
    
		// ------------------- some basic error checking  ------------------- 
		// Page that should be displayed next.
		String forwardTo = null;

		// Get the shop from the BOM
		Shop shop = bom.getShop();
		if (shop == null) {
			log.exiting();
		  	throw new PanicException("No shop returned from bom");
		}       
        
		DocumentHandler documentHandler = (DocumentHandler)userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
		if (documentHandler == null) {
			log.exiting();
			throw new PanicException("No document handler found.");
		}

		// Create the Manager for BusinessPartner objects
        BusinessPartnerManager bupama = bom.createBUPAManager();
        if (bupama == null) {
            if (isDebugEnabled) { log.debug("The bom cannot cannot create a BusinessPartnerManager. This looks like a serious problem."); }
            log.exiting();
            return mapping.findForward(NegotiatedContractConstants.FORWARD_UPDATEDOCUMENT);
        }          

        // Ask the business object manager to create a negotiatedcontract.
        NegotiatedContract negotiatedcontract = bom.createNegotiatedContract();
        if (negotiatedcontract == null) {
            if (isDebugEnabled) { log.debug("The bom cannot create a NegotiatedContract. This looks like a serious problem."); }
            log.exiting();
            return mapping.findForward(NegotiatedContractConstants.FORWARD_UPDATEDOCUMENT);
        }          

        // get a catalog from the catalog bom
        WebCatInfo webCat = getCatalogBusinessObjectManager(userSessionData).getCatalog();

		// check the given sold to id. 
		if (!checkSoldToParameter(bupama,requestParser,userSessionData,shop)) {
			return mapping.findForward("message");
		}	
		
        // now we should have all needed objects
        // ------------------- main part  ------------------- 
        // initialisation of the negotiatedcontract
        negotiatedcontract.clearMessages();

		
        String compPartnerFunction = shop.getCompanyPartnerFunction();

		// initialisation of the basket
		PartnerList pList = createPartnerList(bupama, compPartnerFunction, requestParser);

        // determine the transaction type (like value contract or quantity contract) from request context
        transactionType = requestParser.getParameter(NegotiatedContractConstants.RC_TRANSACTION_TYPE);
        transactionTypeValue = transactionType.isSet() ? transactionType.getValue().getString() : "";        
        if (isDebugEnabled) { log.debug("Transaction type:" + transactionTypeValue); }
		
		ResultData processTypes = shop.getContractNegotiationProcessTypes();
		if (processTypes.first()) {
		   if ( (processTypes.searchRowByColumn(1, transactionTypeValue)) == true) {
			  contractType = processTypes.getString(3); 
		   }
		}
		
		if (contractType.length() > 0) {
			negotiatedcontract.getHeaderData().setProcessTypeUsage(contractType);
		}
		negotiatedcontract.init(shop, bupama, pList, webCat, transactionTypeValue);
		negotiatedcontract.setState(DocumentState.TARGET_DOCUMENT);

        

        // store list of ShipTos in session context
        userSessionData.setAttribute(
            MaintainBasketBaseAction.SC_SHIPTOS,
            negotiatedcontract.getShipTos());

        // add new negotiatedcontract to the document handler
        negotiatedcontract.readForUpdate();
        negotiatedcontract.setState(DocumentState.TARGET_DOCUMENT);
        ManagedDocumentNegContract managedDocument = new ManagedDocumentNegContract(
            negotiatedcontract,
            "ncontract",  // part of ressource key for b2b.docnav.quotation
            "new",
            null,
            null,
            null,
            FORWARD_NEGOTIATEDCONTRACT,
            null,
            null);
        managedDocument.setChangeMode(true); //contract should be editable

        documentHandler.add(managedDocument);
        documentHandler.setOnTop(negotiatedcontract);

        forwardTo = FORWARD_SUCCESS;
		log.exiting();
        return mapping.findForward(forwardTo);
    }

}