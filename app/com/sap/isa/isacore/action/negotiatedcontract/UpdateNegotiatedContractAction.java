/*****************************************************************************
    Class         UpdateNegotiatedContractAction
    Copyright (c) 2002, SAP AG, Germany, All rights reserved.
    Description:
    Author:       SAP AG
    Created:      8. November 2002
    Version:      1.0
*****************************************************************************/

package com.sap.isa.isacore.action.negotiatedcontract;

import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectHelper;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.header.HeaderNegotiatedContract;
import com.sap.isa.businessobject.negotiatedcontract.NegotiatedContract;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.GenericFactory;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 * Read and display the negotiated contract for change purposes.
 *
 * @author SAP AG
 * @version 1.0
 *
 */
public class UpdateNegotiatedContractAction extends IsaCoreBaseAction {
	
	/**
	  * Parser to parse the request
	  */
	protected static final NegotiatedContractParser STANDARD_PARSER = (NegotiatedContractParser) GenericFactory.getInstance("negotiatedContractStandardParser");	
	
    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser parser,
            BusinessObjectManager bom,
            IsaLocation log) throws CommunicationException {
            	
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
		boolean isDebugEnabled = log.isDebugEnabled();
		
        // Page that should be displayed next - initial value, hopefully will be changed
        String forward = NegotiatedContractConstants.FORWARD_ERROR;

        Shop shop = bom.getShop();
        if (shop == null) {
        	log.exiting();
          throw new PanicException("No shop returned from bom");
        }

        DocumentHandler documentHandler = (DocumentHandler)
        userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
        ManagedDocument mandoc = documentHandler.getManagedDocumentOnTop();

        if (mandoc == null) {
           // use global forward to show valid frame
           // this if statement is also reached, if the readForUpdate below failed
           // and the message.jsp was displayed and the user clicks on the back icon
           // of the browser, because in this case the last executed action, which is this action,
           // is recalled. But in that case, the managed document was already deleted
            log.exiting();
            return mapping.findForward("updatedocumentview");
        }

        DocumentState docstate = mandoc.getDocument();
        if (docstate == null) {
			if (isDebugEnabled) {
				log.debug("document is null");
			}
        }
        NegotiatedContract negotiatedcontract = (NegotiatedContract) docstate;


        // Read header data from the JSP
        HeaderNegotiatedContract header = (HeaderNegotiatedContract)negotiatedcontract.getHeader();
        if ( header != null ) {
        	STANDARD_PARSER.parseHeader(parser,userSessionData, header, negotiatedcontract,log, isDebugEnabled );      
        }      
        else { 
			if (isDebugEnabled) {
            	log.debug("Null Header");
			}
        }
        
        //clear message buffer
        negotiatedcontract.clearMessages();
        // get ItemData
        STANDARD_PARSER.parseItems(parser,userSessionData,negotiatedcontract,log,isDebugEnabled);
        
        // get deleted items
        RequestParser.Parameter delete = parser.getParameter("delete[]");

        // delete items with the delete flag set
		if (delete.getNumValues() > 0) {

			Iterator deletedIter = delete.iterator();

 	        TechKey[] deletedItemsTechKeys = new TechKey[delete.getNumValues()];

  	    	int k = 0;

        	while (deletedIter.hasNext()) {
				deletedItemsTechKeys[k++] = new TechKey(((RequestParser.Value) deletedIter.next()).getString());
        	}

        	if (deletedItemsTechKeys.length > 0) {
            	if (log.isDebugEnabled()) {
                  log.debug("deleting items");
           		}
           		// delete the items in the backend
           		negotiatedcontract.deleteItems(deletedItemsTechKeys);
        	}
		}
        // update the new data in the backend
        try {
        	negotiatedcontract.update(shop);
        } catch (BackendException ex) {
                BusinessObjectHelper.splitException(ex);
        }

        // determine the forward
        // Are there errors? TODO Errors vom Backend lesen
        boolean errors = false;

        if (errors) {
            forward = NegotiatedContractConstants.FORWARD_ERROR;
        }
        else if (((request.getParameter("save") != null) && (request.getParameter("save").length() > 0))
                    || ((request.getParameter("send") != null) && (request.getParameter("send").length() > 0))) {
            forward = NegotiatedContractConstants.FORWARD_SAVE; // the [save] or [send] button on the JSP was pressed
        }
        else {
            forward = NegotiatedContractConstants.FORWARD_UPDATE; // the [update] button on the JSP was pressed
        }
		log.exiting();
        return mapping.findForward(forward);
    } // end of isa-perform
}
