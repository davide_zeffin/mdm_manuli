/*****************************************************************************
    Class         SaveNegotiatedContractAction
    Copyright (c) 2002, SAP AG, Germany, All rights reserved.
    Description:
    Author:       SAP AG
    Created:      13. November 2002
    Version:      1.0
*****************************************************************************/

package com.sap.isa.isacore.action.negotiatedcontract;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.businessobject.header.HeaderNegotiatedContract;
import com.sap.isa.businessobject.negotiatedcontract.NegotiatedContract;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 * save the negotiated contract.
 *
 * @author SAP AG
 * @version 1.0
 *
 */
public class SaveNegotiatedContractAction extends IsaCoreBaseAction {

    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser parser,
            BusinessObjectManager bom,
            IsaLocation log) throws CommunicationException {

		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        // Page that should be displayed next.
        String forwardTo = null;

        Shop shop = bom.getShop();
        if (shop == null) {
        	log.exiting();
          throw new PanicException("No shop returned from bom");
        }


        // get user
        User user = bom.getUser();
        if (user == null) {
        	log.exiting();
            return mapping.findForward("login");
        }

        // Ask the business object manager to create a negotiatedcontract.
        NegotiatedContract negotiatedcontract = bom.getNegotiatedContract();
        //NegotiatedContractChange negotiatedcontractchange = new NegotiatedContractChange(negotiatedcontract);

        // TODO : Fehlerhandling
        DocumentHandler documentHandler = (DocumentHandler) userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
        ManagedDocument mandoc = documentHandler.getManagedDocumentOnTop();

        if (mandoc == null) {
           // use global forward to show valid frame
           // this if statement is also reached, if the readForUpdate below failed
           // and the message.jsp was displayed and the user clicks on the back icon
           // of the browser, because in this case the last executed action, which is this action,
           // is recalled. But in that case, the managed document was already deleted
            log.exiting();
            return mapping.findForward(NegotiatedContractConstants.FORWARD_UPDATEDOCUMENT);
        }

          // create a Message Displayer for the error page!
          /*
          MessageDisplayer messageDisplayer = new MessageDisplayer();

          messageDisplayer.addToRequest(request);
          messageDisplayer.copyMessages(salesDoc);

          // the action which is to call after the message
          messageDisplayer.setAction("documentstatusdetailprepare.do");
          messageDisplayer.setActionParameter("?techkey="+salesDoc.getTechKey().getIdAsString()
                                               +"&objecttype="+mandoc.getDocType()
                                               +"&objects_origin=&object_id=" );
          */
          //return mapping.findForward("message");
        //}
        // TODO : Fehlerbehandlung
        if ((request.getParameter("save") != null) && (request.getParameter("save").length() > 0)) {
            negotiatedcontract.save(NegotiatedContractConstants.PROCESSTYPE_SAVE, shop);
            negotiatedcontract.readHeader();
			request.setAttribute(NegotiatedContractConstants.RC_HEADER, negotiatedcontract.getHeader());
            forwardTo = NegotiatedContractConstants.FORWARD_SAVECONFIRM; // the [save]-button on the JSP was pressed
        }
        else if ((request.getParameter("send") != null) && (request.getParameter("send").length() > 0)) {
            negotiatedcontract.save(NegotiatedContractConstants.PROCESSTYPE_SEND, shop);
            forwardTo = NegotiatedContractConstants.FORWARD_SENDCONFIRM; // the [send]-button on the JSP was pressed
            // Put the negotiated contract into the history, then kill it 
            // This must be removed, if a confirmation with contract information should be displayed
			negotiatedcontract.readHeader();
            HeaderNegotiatedContract header = (HeaderNegotiatedContract) negotiatedcontract.getHeader();
			request.setAttribute(NegotiatedContractConstants.RC_HEADER, header);
            ManagedDocument historydoc = new ManagedDocument(
			negotiatedcontract,
			mandoc.getDocType(), // part of ressource key for b2b.cookie.ncontract
			header.getSalesDocNumber(),
			header.getPurchaseOrderExt(),
			header.getDescription(),
			header.getContractStart(),
			NegotiatedContractConstants.FORWARD_ERROR, // this forward should never be used, because this doc is only used in the history
			"b2b/preparenegotiatedcontract.do",
			NegotiatedContractConstants.CONTRACT_SELECTED + "=" + negotiatedcontract.getTechKey().getIdAsString());
			historydoc.setState(DocumentState.UNDEFINED); // because of state UNDEFINED the document will only be added to the history,
			documentHandler.add(historydoc);			  // not to the documenthandler
			documentHandler.release(mandoc.getDocument());
			bom.releaseNegotiatedContract();
        }
        else {
            forwardTo = NegotiatedContractConstants.FORWARD_ERROR;
        }
        log.exiting();
        return mapping.findForward(forwardTo);
    }

}
