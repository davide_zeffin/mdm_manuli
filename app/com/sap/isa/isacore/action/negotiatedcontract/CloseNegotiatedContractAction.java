/*****************************************************************************
    Class         CloseNegotiatedContractAction
    Copyright (c) 2002, SAP AG, Germany, All rights reserved.
    Description:
    Author:       SAP AG
    Created:      14. November 2002
    Version:      1.0
*****************************************************************************/

package com.sap.isa.isacore.action.negotiatedcontract;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.header.HeaderNegotiatedContract;
import com.sap.isa.businessobject.negotiatedcontract.NegotiatedContract;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 * Asks the DocumentHandler for the document on top and if this document
 * is a NegotiatedContract, it releases it at the Bom and at the
 * DocumentHandler. The NegotiatedContract may be in display or in edit mode.
 * <p>
 * It also looks for a request parameter <code>history</code>. If this
 * parameter is <code>true</code> and the negotiated contract was already saved
 * in the backend, it puts an entry for this document in the history.
 * <p>
 * The following forwards are used by this action:
 * </p>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   </tr>
 *   <tr>
 * 	<td>NegotiatedContractConstants.FORWARD_UPDATEDOCUMENT</td>
 * 	<td>forward	 used if something goes wrong to allow application to reach an
 * consistent state again</td>
 *   </tr>
 *   <tr>
 * 	<td>NegotiatedContractConstants.FORWARD_SUCCESS</td>
 * 	<td>forward used if everthing is ok</td>
 *   </tr>
 * </table>
 *
 * @author SAP AG
 * @version 1.0
 *
 */
public class CloseNegotiatedContractAction extends IsaCoreBaseAction {
	
    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser parser,
            BusinessObjectManager bom,
            IsaLocation log) throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
		boolean isDebugEnabled = log.isDebugEnabled();
				
		// ------------------- some basic error checking  ------------------- 
        DocumentHandler documentHandler = (DocumentHandler)userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
		if (documentHandler == null) {
			log.exiting();
			throw new PanicException("No document handler found.");
		}

        ManagedDocument mandoc = documentHandler.getManagedDocumentOnTop();

        if (mandoc == null) {
			if (isDebugEnabled) { log.debug("There is no ManagedDocument."); }
           // use global forward to show valid frame
            log.exiting();
            return mapping.findForward(NegotiatedContractConstants.FORWARD_UPDATEDOCUMENT);
        }

		NegotiatedContract negotiatedcontract;
		if (mandoc.getDocument() instanceof NegotiatedContract) {
			negotiatedcontract = (NegotiatedContract) mandoc.getDocument();	
		}
		else {
			if (isDebugEnabled) { log.debug("The document on top is not a NegotiatedContract. So we are in the wrong action."); }
			// Perhaps we can go further on, if we release the document at the document handler,
			// but a problem could be, that we couldn't release it at the bom.			
			documentHandler.release(mandoc.getDocument());
			log.exiting();
			return mapping.findForward(NegotiatedContractConstants.FORWARD_UPDATEDOCUMENT);
		}


		// ------------------- Find out, if the contract should be placed in the history  ------------------- 
		RequestParser.Parameter history = parser.getParameter("history");
        
		if (history.getValue().getString().equals("true")) {
			if (isDebugEnabled) { log.debug("Found request parameter history=true. -> Try to put an entry into the history."); }

			HeaderNegotiatedContract header;
			if (negotiatedcontract.getHeader() != null) {
				header = (HeaderNegotiatedContract) negotiatedcontract.getHeader();
				String docnum;
				try {
					negotiatedcontract.readHeader(); // may throw a communication exception
					docnum = header.getSalesDocNumber();
					if (docnum!=null && docnum.length() > 2) {
						if (isDebugEnabled) { log.debug("contract number:" + docnum); }
						// we put the contract in the history, only if it was saved in the backend
						// that is, only if it has a number
						ManagedDocument historydoc = new ManagedDocument(
						negotiatedcontract,
						mandoc.getDocType(), // part of ressource key for b2b.cookie.ncontract
						header.getSalesDocNumber(),
						header.getPurchaseOrderExt(),
						header.getDescription(),
						header.getContractStart(),
						NegotiatedContractConstants.FORWARD_ERROR, // this forward should never be used, because this doc is only used in the history
						"b2b/preparenegotiatedcontract.do",
						NegotiatedContractConstants.CONTRACT_SELECTED + "=" + negotiatedcontract.getTechKey().getIdAsString());
						historydoc.setState(DocumentState.UNDEFINED); // because of state UNDEFINED the document will only be added to the history,
						documentHandler.add(historydoc);			  // not to the documenthandler
					}
					else {
						if (isDebugEnabled) { log.debug("There is no internal document number, therefore I cannot put an entry into the history"); }
					}
				}
				catch (CommunicationException e) {
					if (isDebugEnabled) { log.debug("Could not read the header, therefore I cannot put an entry into the history"); }
				}
			}
			else {
				if (isDebugEnabled) { log.debug("There is no header, therefore I cannot put an entry into the history"); }
			}
		}

		// ------------------- Finally, release the negotiated contract ------------------- 

        negotiatedcontract.dequeue();      // release lock of the order
        negotiatedcontract.destroyContent();  //initialize order in backend  
        negotiatedcontract.clearData();; 
 

        documentHandler.release(mandoc.getDocument());
        // find out which contract should be released and release it
        if (negotiatedcontract==bom.getNegotiatedContract()) {
			if (isDebugEnabled) { log.debug("Calling: bom.releaseNegotiatedContract();"); }
			bom.releaseNegotiatedContract();
        }
        else if (negotiatedcontract==bom.getNegotiatedContractStatus()){
			if (isDebugEnabled) { log.debug("Calling: bom.releaseNegotiatedContractStatus();"); }
			bom.releaseNegotiatedContractStatus();
        }
        else {
			if (isDebugEnabled) { log.debug("Found nothing to release at the bom. This is should not happen!"); }
        }
		log.exiting();
        return mapping.findForward(NegotiatedContractConstants.FORWARD_SUCCESS);
    }

}
