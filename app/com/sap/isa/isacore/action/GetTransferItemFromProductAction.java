/*****************************************************************************
    Class         GetTransferItemFromProductAction
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Action add a product to the basket
    Author:       Wolfgang Sattler
    Created:      May 2001
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/16 $
*****************************************************************************/

package com.sap.isa.isacore.action;

// framework dependencies
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Product;
import com.sap.isa.businessobject.item.BasketTransferItemImpl;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.businessobject.webcatalog.ItemTransferRequest;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.catalog.webcatalog.WebCatItemList;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.b2c.order.LeafletToBasketB2CAction;
import com.sap.isa.isacore.action.order.AddToBasketAction;
import com.sap.isa.catalog.actions.ActionConstants;

/**
 * Create a transfer item for product and set it in the request context
 *
 * @author Wolfgang Sattler
 * @version 1.0
 *
 */
public class GetTransferItemFromProductAction extends IsaCoreBaseAction {

	public static final String FORWARD_FAILURE = "failure";
	public static final String FORWARD_SUCCESS = "success";
	public static final String FORWARD_NAME = "target";

	/**
	 * Name of the request parameter to the product fields
	 */
	public static final String PRODUCT_GUID = "productguid";
	public static final String PRODUCT_ID = "productid";
	public static final String ITEM_GUID = "itemguid";
	public static final String QUANTITY = "quantity";
	public static final String UNIT = "unit";
	public static final String AREA = "area";

	
	/**
	 * used to specify the transfer item source
	 */
	protected static final String TRANSFER_ITEM_SOURCE = "catalog";
	
	/** creates the parameter list for a product, which can use in jsp's to
	 *  display product details.
	 */
	public static String createAddRequest(Product product, int quantity) {

		StringBuffer str = new StringBuffer("?");
    	str.append(PRODUCT_GUID);
    	str.append("=");
    	str.append(product.getTechKey().getIdAsString());
    	str.append("&");
    	str.append(UNIT);
    	str.append("=");
    	str.append(product.getUnit());
    	str.append("&");
    	str.append(QUANTITY);
    	str.append("=");
    	str.append(quantity);
    	str.append("&");
    	str.append(AREA);
    	str.append("=");
    	str.append(product.getArea());
    	return str.toString();

	}

	/** creates the parameter list for a product, which can use in jsp's to
	 *  display product details.
	 */
	public static String createReplaceRequest(
		Product product,
		String itemKey,
		int quantity) {

		StringBuffer str = new StringBuffer();
    	str.append(createAddRequest(product, quantity));
    	str.append("&");
    	str.append(ITEM_GUID);
    	str.append("=");
    	str.append(itemKey);
    	return str.toString();
	}

	/** creates the parameter list for a product, which can use in jsp's to
	 *  display product details.
	 */
	public static String createReplaceRequest(
		Product product,
		String itemKey,
		int quantity,
		String forward) {

		StringBuffer str = new StringBuffer();
    	str.append(createReplaceRequest(product, itemKey, quantity));
    	str.append("&");
    	str.append(FORWARD_NAME);
    	str.append("=");
    	str.append(forward);
    	return str.toString();
	}

	/** creates the parameter list for a product, which can use in jsp's to
	 *  display product details.
	 */
	public static String createAddRequest(
		Product product,
		int quantity,
		String forward) {

		StringBuffer str = new StringBuffer();
    	str.append(createAddRequest(product, quantity));
    	str.append("&");
    	str.append(FORWARD_NAME);
    	str.append("=");
    	str.append(forward);
    	return str.toString();
	}

	/** creates the parameter list for a product, which can use in jsp's to
	 *  display product details.
	 */
	public static String createAddRequest(
		String productKey,
		String unit,
		int quantity) {

		StringBuffer str = new StringBuffer("?");
    	str.append(PRODUCT_GUID);
    	str.append("=");
    	str.append(productKey);
    	str.append("&");
    	str.append(UNIT);
    	str.append("=");
    	str.append(unit);
    	str.append("&");
    	str.append(QUANTITY);
    	str.append("=");
    	str.append(quantity);
    	str.append("&");
    	return str.toString();
	}

	/**
	 * Initialize the action.
	 * In B2C this action could be used by anonymous user.
	 * 
	 * @see com.sap.isa.isacore.action.IsaCoreBaseAction#initialize()
	 */
	public void initialize() {
		if (isB2C()) {
			this.checkUserIsLoggedIn = false;
		}
	}

	/**
	 * Overriden <em>isaPerform</em> method of <em>IsaCoreBaseAction</em>.
	 */
	public ActionForward isaPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		BusinessObjectManager bom,
		IsaLocation log)
		throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);

		WebCatInfo catalog =
			getCatalogBusinessObjectManager(userSessionData).getCatalog();
		//find the webCatItem
		WebCatItemList currentWebCatItemList = catalog.getCurrentItemList();
		WebCatItem anItem = null;
		if (currentWebCatItemList != null) {
			Iterator currentListIterator = currentWebCatItemList.iteratorOnlyPopulated();
			while (currentListIterator.hasNext()) {
				anItem = (WebCatItem) currentListIterator.next();
				if (anItem.getCatalogItem().getProductGuid().equals(
                       requestParser.getParameter(PRODUCT_GUID).getValue().getString())) {
					break;
				} 
                else {
					anItem = null;
				}

			}
		}

		if ((anItem != null && anItem.getSolutionConfigurator() == null)
			|| anItem == null) { // in the case of a non package product
			// create the transfer item!
			BasketTransferItemImpl item = new BasketTransferItemImpl();
			item.setSource(TRANSFER_ITEM_SOURCE);

			// only used  within the b2c scenario
			Object productConfiguration = request.getAttribute(LeafletToBasketB2CAction.PRODUCT_CONFIGURATION_FROM_LEAFLET);
			if (productConfiguration != null) {
				item.setConfigurationItem(productConfiguration);
			}

			item.setProductKey(requestParser.getParameter(PRODUCT_GUID).getValue().getString());
			item.setQuantity(requestParser.getParameter(QUANTITY).getValue().getString());
			item.setUnit(requestParser.getParameter(UNIT).getValue().getString());
                
            if (anItem != null) {
                item.setTechKey(anItem.getTechKey().getIdAsString());
            }

			if (requestParser.getParameter(ITEM_GUID).getValue().isSet()) {
				ItemSalesDoc basketItemToReplace =
					new ItemSalesDoc(new TechKey(requestParser.getParameter(ITEM_GUID).getValue().getString()));
				item.setBasketItemToReplace(basketItemToReplace);
			}

			AddToBasketAction.setRequestAttribute(request, item);
		} 
        else {
			ArrayList basketItems = new ArrayList();
			//in the case of a package
			//we should add all package components
			Iterator itemIterator = null;
			//only the webcatitem with getItemKey.isSelected()==true should be inserted 
			itemIterator = anItem.iteratorOnlySubItemSelected();
			//add the main product

			basketItems.add(new ItemTransferRequest(anItem));

			while (itemIterator.hasNext()) {
				anItem = (WebCatItem) itemIterator.next();
				basketItems.add(new ItemTransferRequest(anItem));
			}
			request.setAttribute(ActionConstants.RA_ITEMLIST_ISA, basketItems);
		}

		// determine forward: provided as request parameter or attribute
		String forwardTo = FORWARD_SUCCESS;
		RequestParser.Value forwardValue =
			requestParser.getParameter(FORWARD_NAME).getValue();
		if (forwardValue.isSet()) {
			forwardTo = forwardValue.getString();
		} 
        else {
			forwardValue = requestParser.getAttribute(FORWARD_NAME).getValue();
			if (forwardValue.isSet()) {
				forwardTo = forwardValue.getString();
			}
		}

		if (log.isDebugEnabled()) {
			log.debug("forward to: " + forwardTo);
		}
		log.exiting();
		return mapping.findForward(forwardTo);
	}
}