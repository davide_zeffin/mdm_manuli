/*****************************************************************************
	Class:        CatalogReadListAction
	Copyright (c) 2004, SAPLabs, Palo Alto, All rights reserved.
	Author        SAP

	$Revision: #1 $

*****************************************************************************/

package com.sap.isa.isacore.action.b2b;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.backend.boi.isacore.UserData;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.core.util.table.TableRow;
import com.sap.isa.isacore.MessageDisplayer;
import com.sap.isa.isacore.action.IsaCoreInitAction;

public class CatalogReadListAction extends CatalogReadListBaseAction {
	private static final String FORWARD_AUTOENTRYERROR = "autoentryerror";

	/**
	 * Implement this method to add functionality to your action.
	 *
	 * @param form              The <code>FormBean</code> specified in the
	 *                          config.xml file for this action
	 * @param request           The request object
	 * @param response          The response object
	 * @param userSessionData   Object wrapping the session
	 * @param requestParser     Parser to simple retrieve data from the request
	 * @param bom               Reference to the BusinessObjectManager
	 * @param log               Reference to the IsaLocation, needed for logging
	 * @return Forward to another action or page
	 */
	public ActionForward isaPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		BusinessObjectManager bom,
		IsaLocation log)
		throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
		// Page that should be displayed next.
		//		  String forwardTo = null;
		IsaCoreInitAction.StartupParameter startupParameter =
			(IsaCoreInitAction.StartupParameter) userSessionData.getAttribute(
				IsaCoreInitAction.SC_STARTUP_PARAMETER);
		ResultData result = null;
		User user = bom.createUser();
		String shopId = null;
		String soldToId = null;
		RequestParser.Parameter shopP =  requestParser.getParameter("WebShop");
		if (shopP != null){
			shopId = shopP.getValue().getString();
			if (shopId.equalsIgnoreCase("")){
				shopId = bom.getShop().getId();
			}/// see if the shop is changed
		}else{
			shopId = bom.getShop().getId();
		}
		RequestParser.Parameter soldToP =  requestParser.getParameter("SoldTo");
		if (soldToP != null){
			soldToId = soldToP.getValue().getString();
		}
		Shop shop = bom.getShop();
        
		if (shop == null) {
			log.exiting();
			throw new PanicException("shop.notFound");
		}
		/// 
		if (shopId.equalsIgnoreCase(shop.getId())){
			if (!shop.isCatalogDeterminationActived()) {
				request.setAttribute(
						ReadCatalogAction.PARAM_CATALOG,
						shop.getCatalog());
				// construct a ResultData and set to request
				result = constructTableFromRow(shop.getCatalog());
				// move to first row
				result.absolute(1);
				request.setAttribute(RK_CATALOG_LIST, result);
                request.setAttribute(ReadCatalogAction.PARAM_VIEW, null);
				log.exiting();
				return mapping.findForward("readcatalog");
			}
			try {
				result = readCatalogList(shopId, bom, soldToId);
			} catch (Exception ex) {
				log.exiting();
				return mapping.findForward("error");
			}
			if (result == null || result.getNumRows() <= 0) {

				MessageDisplayer messageDisplayer = new MessageDisplayer();
				messageDisplayer.setOnlyLogin();
				messageDisplayer.addMessage(
					new Message(Message.ERROR, "user.noCatalogs", null, null));
				messageDisplayer.addToRequest(request);
				log.exiting();
				return mapping.findForward("message");
			} else if (result.getNumRows() == 1) {

				log.debug(result.toString());
				// found exactly one shop, use it

				// move to first row
				result.absolute(1);

				// Save catalog in request
				request.setAttribute(
					ReadCatalogAction.PARAM_CATALOG,
					result.getString(UserData.RD_CATALOG_KEY));

				request.setAttribute(
					ReadCatalogAction.PARAM_VIEW,
					result.getObject(UserData.RD_CATALOG_VIEWS));
				request.setAttribute(RK_CATALOG_LIST, result);
				log.exiting();
				return mapping.findForward("readcatalog");
			} else {
//				if (startupParameter.getAutoEntry().equals("YES")) {
//					// Found more than one sold-to parties but autoentry requested (currently
//					// used from Portal iViews). Since no shoplist can be presented, this
//					// situation will lead to errorpage
//					request.setAttribute(RK_AUTOENTRYERROR, "catdetermineerror");
//					return mapping.findForward(FORWARD_AUTOENTRYERROR);
//				}
				log.debug(result.toString());
				request.setAttribute(RK_CATALOG_LIST, result);
                // move to first row, as all catalogs here must belong to the same shop we have
                // the same views for all catalogs
                result.absolute(1);
                request.setAttribute(ReadCatalogAction.PARAM_VIEW, result.getObject(UserData.RD_CATALOG_VIEWS));

			}
			log.exiting();
			return mapping.findForward("selectcatalog");
			
		}else{
			// read the shop and see it is catalogdeterminationactived
			// Create TechKey for the selected shop
			 TechKey shopKey = new TechKey(shopId);
			// Ask the bom to create a shop with the given key
			Shop newShop = bom.getShop(shopKey); // May throw CommunicationException

			if (newShop == null) {
				// create invalid shop
				newShop = new Shop();
				newShop.addMessage(new Message(Message.ERROR,"shop.notFound"));
			}

			if (!newShop.isValid()){
				String args[]= {shopKey.getIdAsString()};
 				MessageDisplayer messageDisplayer = new MessageDisplayer();
 				messageDisplayer.addToRequest(request);
 				messageDisplayer.setOnlyLogin();
 				messageDisplayer.addMessage(new Message(Message.ERROR,"shop.loadError",args,null));
				messageDisplayer.copyMessages(newShop);
				return mapping.findForward("readcatalog");
			}
			if (!newShop.isCatalogDeterminationActived()) {
				request.setAttribute(
						ReadCatalogAction.PARAM_CATALOG,
						newShop.getCatalog());
				// construct a ResultData and set to request
				result = constructTableFromRow(newShop.getCatalog());
				// move to first row
				result.absolute(1);
				request.setAttribute(RK_CATALOG_LIST, result);
                request.setAttribute(ReadCatalogAction.PARAM_VIEW, null);
				log.exiting();
				return mapping.findForward("readcatalog");
			}
			try {
				result = readCatalogList(shopId, bom, soldToId);
			} catch (Exception ex) {
				log.exiting();
				return mapping.findForward("error");
			}
			if (result == null || result.getNumRows() <= 0) {

				MessageDisplayer messageDisplayer = new MessageDisplayer();
				messageDisplayer.setOnlyLogin();
				messageDisplayer.addMessage(
					new Message(Message.ERROR, "user.noCatalogs", null, null));
				messageDisplayer.addToRequest(request);
				log.exiting();
				return mapping.findForward("message");
			} else if (result.getNumRows() == 1) {

				log.debug(result.toString());
				// found exactly one shop, use it

				// move to first row
				result.absolute(1);

				// Save catalog in request
				request.setAttribute(
					ReadCatalogAction.PARAM_CATALOG,
					result.getString(UserData.RD_CATALOG_KEY));

				request.setAttribute(
					ReadCatalogAction.PARAM_VIEW,
					result.getObject(UserData.RD_CATALOG_VIEWS));
				request.setAttribute(RK_CATALOG_LIST, result);
				log.exiting();
				return mapping.findForward("readcatalog");
			} else {
//				if (startupParameter.getAutoEntry().equals("YES")) {
//					// Found more than one sold-to parties but autoentry requested (currently
//					// used from Portal iViews). Since no shoplist can be presented, this
//					// situation will lead to errorpage
//					request.setAttribute(RK_AUTOENTRYERROR, "catdetermineerror");
//					return mapping.findForward(FORWARD_AUTOENTRYERROR);
//				}
				log.debug(result.toString());
				request.setAttribute(RK_CATALOG_LIST, result);

                // move to first row, as all catalogs here must belong to the same shop we have
                // the same views for all catalogs
                result.absolute(1);
                request.setAttribute(ReadCatalogAction.PARAM_VIEW, result.getObject(UserData.RD_CATALOG_VIEWS));
			}
			log.exiting();
			return mapping.findForward("selectcatalog");
	 
		}
	}
	

    private ResultData constructTableFromRow(String catalogId){
		Table pcatsTable = new Table("Catalogs");
		pcatsTable.addColumn(Table.TYPE_STRING, UserData.RD_CATALOG_KEY);
		TableRow   pcatsRow = pcatsTable.insertRow();
		// columns for the catalog data
		pcatsRow.getField(UserData.RD_CATALOG_KEY).setValue(catalogId);		
		return (pcatsTable != null) ? new ResultData(pcatsTable) : null;
	}

}
