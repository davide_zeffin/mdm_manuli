/*****************************************************************************
	Class         ShowProductInCatalogAction
	Copyright (c) 2002, SAP AG, Germany, All rights reserved.
	Description:  This action start's the catalog with an specific item
	Author:       SAP AG
	Created:      13.08.2003
	Version:      0.1

	$Revision: #1 $
	$Date: 2003/08/13 $

*****************************************************************************/

package com.sap.isa.isacore.action.b2b;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.AppBaseBusinessObjectManager;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.IdMapper;
import com.sap.isa.businessobject.Product;
import com.sap.isa.catalog.actions.ActionConstants;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.order.OrderStatus;
import com.sap.isa.catalog.webcatalog.WebCatArea;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.MessageDisplayer;
import com.sap.isa.isacore.action.IsaCoreBaseAction;


/**
 *  This action start's the catalog with an specific item:
 */
public class ShowProductInCatalogAction extends IsaCoreBaseAction {


	/**
	 * String constant specifying if auctions are enabled.
	 */
	private static final String PARAM_FORWARD           = "success";
	private static final String PARAM_PRODUCT_ID        = "productId";
	private static final String PARAM_PRODUCT_AREA      = "productArea";
	private static final String PARAM_PRODUCT_SCENARIO  = "productScenario";
	private static final String PARAM_CATALOG_REFRESH   = "showProduct";

	  /**
	 * Implement this method to add functionality to your action.
	 *
	 * @param form              The <code>FormBean</code> specified in the
	 *                          config.xml file for this action
	 * @param request           The request object
	 * @param response          The response object
	 * @param userSessionData   Object wrapping the session
	 * @param requestParser     Parser to simple retrieve data from the request
	 * @param bom               Reference to the BusinessObjectManager
	 * @param log               Reference to the IsaLocation, needed for logging
	 * @return Forward to another action or page
	 */
	public ActionForward isaPerform(ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response,
			UserSessionData userSessionData,
			RequestParser requestParser,
			BusinessObjectManager bom,
			IsaLocation log)
			throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
		HttpSession session = request.getSession();

		// create a webcatitemlist, because it is need in catalog actions
		WebCatInfo catalog =
				getCatalogBusinessObjectManager(userSessionData).getCatalog();

		if (catalog == null) {
			log.exiting();
			throw new PanicException("catalog.notFound");
		}

		String itemId = request.getParameter(PARAM_PRODUCT_ID);
        
		if (itemId == null) {
			itemId = (String)request.getAttribute(PARAM_PRODUCT_ID);
		}	
        
		if (itemId != null && !itemId.equals("")) {
			// here in special scenarios id mapping is needed
			AppBaseBusinessObjectManager appBaseBom = (AppBaseBusinessObjectManager) userSessionData.getBOM(AppBaseBusinessObjectManager.APPBASE_BOM);
			IdMapper idMapper = appBaseBom.createIdMapper();
			// get the documentHandler to get the document on top
			DocumentHandler documentHandler = (DocumentHandler) userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
            HeaderSalesDocument header = null;
            if (documentHandler.getDocumentOnTop() instanceof SalesDocument) {
                header = ((SalesDocument)documentHandler.getDocumentOnTop()).getHeader();
            } else if (documentHandler.getDocumentOnTop() instanceof OrderStatus) {
                header = ((OrderStatus)documentHandler.getDocumentOnTop()).getOrderHeader();
            }
            
			String mappedItemId = null;
            try{
				 mappedItemId = idMapper.mapIdentifier(IdMapper.PRODUCT,
															 header.getDocumentType(),
															 IdMapper.CATALOG,
															 itemId);
            }
            catch(CommunicationException exc){
				MessageDisplayer messageDisplayer = new MessageDisplayer();
				messageDisplayer.addMessage(new Message(Message.ERROR,"product.not.in.catalog"));
				messageDisplayer.setAction("/b2b/updatedocumentview.do");
				messageDisplayer.addToRequest(request);
				log.exiting();
				return mapping.findForward("message");
            }

            // set area type			
            String areaType = null;
            String areaId = (String)request.getAttribute(PARAM_PRODUCT_AREA);
            if (areaId != null) {
                WebCatArea area = catalog.getArea(areaId);
                if ( area.isRewardCategory() ) {
                    areaType = WebCatArea.AREATYPE_LOY_REWARD_CATALOG; 
                }
                if ( area.isBuyPointsCategory() ) {
                    areaType = WebCatArea.AREATYPE_LOY_BUY_POINTS; 
                }
            }   
        
			Product product = new Product( new TechKey(mappedItemId));
				
			if (product.enhance(catalog, true, areaType)) {
				session.setAttribute(PARAM_PRODUCT_ID,product.getItemId().toString());
				session.setAttribute(PARAM_PRODUCT_AREA, product.getArea().toString());
				session.setAttribute(PARAM_PRODUCT_SCENARIO, "ItemFromBasket");
				session.setAttribute(ActionConstants.CV_DETAILSCENARIO, "ItemFromBasket");
				
				// set the catalog area
				catalog.resetCurrentArea();
				catalog.setCurrentArea(product.getArea().toString());
				//set the forward for the catalog
				catalog.setLastVisited(PARAM_CATALOG_REFRESH);
				catalog.setLastUsedDetailScenario(WebCatInfo.ITEMDETAILS);
				
				// set the documentHandler to get the catalog on top
				 documentHandler = (DocumentHandler)
					userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
				documentHandler.setCatalogOnTop(true);
				     				 
			} 
			else {
				
				MessageDisplayer messageDisplayer = new MessageDisplayer();
				messageDisplayer.addMessage(new Message(Message.ERROR,"product.not.in.catalog"));
				messageDisplayer.setAction("/b2b/updatedocumentview.do");
				messageDisplayer.addToRequest(request);
				log.exiting();
				return mapping.findForward("message");

			}	
		}

		log.exiting();
		return mapping.findForward(PARAM_FORWARD);

	}

}
