/*****************************************************************************
    Class         StartApplicationAction
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Action for the Profile Maintenance
    Author:       SAP AG
    Created:      27 Februar 2001
    Version:      0.1

    $Revision: #1 $
    $Date: 2001/06/26 $

*****************************************************************************/

package com.sap.isa.isacore.action.b2b;


/**
 * This action start's the application. <br>
 * With the global parameter: {@link IsaCoreInitAction#SC_STARTUP_PARAMETER}
 * you can control the flow of the application.
 * 
 * @deprecated Please use {@link com.sap.isa.isacore.action.StartApplicationAction}
 */
public class StartApplicationAction extends com.sap.isa.isacore.action.StartApplicationAction {


}

