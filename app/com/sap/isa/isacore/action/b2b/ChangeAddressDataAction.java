/*****************************************************************************
Class         ChangeAddressDataAction
Copyright (c) 2002, SAP AG, Germany, All rights reserved.
Description:  Saves user's address data changes.
Author:       Adam Ebert
Created:      Aug 2002
Version:      1.0

$Revision 3 $
$Date 18.06.2004 $
*****************************************************************************/
package com.sap.isa.isacore.action.b2b;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.isacore.AddressFormular;
import com.sap.isa.isacore.action.ActionConstants;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.user.action.UserActions;
import com.sap.isa.user.businessobject.IsaUserBase;

/**
* Saves user's address data changes.  
*
* @author Adam Ebert
* @version 1.0
*
*/
public class ChangeAddressDataAction extends IsaCoreBaseAction {
	
/**
* simple constructor
*/
public ChangeAddressDataAction() {
}

/**
 * Implement this method to add functionality to your action.
 *
 * @param form              The <code>FormBean</code> specified in the
 *                          config.xml file for this action
 * @param request           The request object
 * @param response          The response object
 * @param userSessionData   Object wrapping the session
 * @param requestParser     Parser to simple retrieve data from the request
 * @param bom               Reference to the BusinessObjectManager
 * @param log               Reference to the IsaLocation, needed for logging
 * @return Forward to another action or page
 */
public ActionForward isaPerform(ActionMapping mapping,
        ActionForm form,
        HttpServletRequest request,
        HttpServletResponse response,
        UserSessionData userSessionData,
        RequestParser requestParser,
        BusinessObjectManager bom,
        IsaLocation log)
        throws CommunicationException {
	final String METHOD_NAME = "isaPerform()";
	log.entering(METHOD_NAME);	                    	
    // page that should be displayed next
    String forwardTo = null;
    boolean isDebugEnabled = log.isDebugEnabled();
    ActionForward forwardBase = null;

    // list of countries
    ResultData listOfCountries;
    // list of regions
    ResultData listOfRegions = null;
    // list of forms of address
    ResultData formsOfAddress;

    IsaUserBase user = bom.getUser();
    user.clearMessages();
    request.setAttribute(BusinessObjectBase.CONTEXT_NAME, user);

    Shop shop = bom.getShop();
    if (shop == null) {
		log.exiting();
        throw new PanicException("shop.notFound");
    }
    
    BusinessPartnerManager bupama = bom.getBUPAManager();

    // create a new address formular
    AddressFormular addressFormular = new AddressFormular(requestParser);
    // address format id
    int addressFormatId = shop.getAddressFormat();
    // the next line is nessassary in case of a reconstruction of the soldToAddress JSP
    addressFormular.setAddressFormatId(addressFormatId);

    // read SoldTos
    BusinessPartner contact = bupama.getDefaultBusinessPartner(PartnerFunctionData.CONTACT);
    ResultData result = bupama.getSoldTosBySalesArea(contact, shop.getId());

    if ((result == null) || (result.getNumRows() <= 0)) {  

        log.error(LogUtil.APPS_USER_INTERFACE, "no sold tos found!");
        user.addMessage(new Message(Message.ERROR,"user.noSoldTo"));
		log.exiting();
        return mapping.findForward("failure");
    }
    else {
        if (isDebugEnabled) log.debug("soldtos found - num: " + result.getNumRows());
        request.setAttribute(ReadAddressDataAction.RK_SOLDTO_LIST, result);
    }

    BusinessPartner bupa = null;
    
    bupa = bupama.getBusinessPartner(addressFormular.getAddress().getAddressPartner());
    if(bupa != null){
        String shortAddress = bupa.getAddressesShort().getTable().getRow(1).getField("DESC").getString();
        request.setAttribute(ReadAddressDataAction.RK_SHORTADDRESS, shortAddress);
    }
    else{
        user.addMessage(new Message(Message.ERROR,"user.noSoldTo"));
		log.exiting();
        return mapping.findForward("failure");
    }

    // check for possible cases
    // case A: if the user changes the country....
    //         This case is only relevant if addresses will not be stored in 
    //         the relationship to a sold-to.  
    if(request.getParameter("countryChange").length() > 0) {

        // build up the detail data needed for the soldToAddress-JSP
        formsOfAddress = shop.getTitleList();
        listOfCountries = shop.getCountryList();
        if (addressFormular.isRegionRequired()){
            listOfRegions = shop.getRegionList(addressFormular.getAddress().getCountry());
        }

        // set request attributes
        request.setAttribute(ActionConstants.RC_FORMS_OF_ADDRESS, formsOfAddress);
        request.setAttribute(ActionConstants.RC_COUNTRY_LIST, listOfCountries);
        if (listOfRegions != null) {
            request.setAttribute(ActionConstants.RC_REGION_LIST, listOfRegions);
        }
        request.setAttribute(ActionConstants.RC_ADDRESS_FORMULAR, addressFormular);
    	forwardTo = "countryChange";
    }
    // case B: if the user changes the SoldTo ...
    //         This case is only relevant if addresses will be stored in 
    //         the relationship to a sold-to.  
    else if(request.getParameter("soldtoChange").length() > 0) {

        formsOfAddress = shop.getTitleList();
        request.setAttribute(ActionConstants.RC_FORMS_OF_ADDRESS, formsOfAddress);

        // get Soldto and the ShortAddress:
        request.setAttribute(ReadAddressDataAction.RK_ADDRESSPARTNER, requestParser.getParameter("addresspartner").getValue().getString());
    
        bupa = bupama.getBusinessPartner(requestParser.getParameter("addresspartner").getValue().getString());
        if(bupa != null){
            //String shortAddress = bupa.getAddressesShort().getTable().getRow(1).getField("DESC").getString();
            //request.setAttribute(ReadAddressDataAction.RK_SHORTADDRESS, shortAddress);
        	if(log.isDebugEnabled()) log.debug("BusinessPartner as AddressPartner found");
        	// read all short addresses for the selected sold-to
        	ResultData shortAddresses = bupa.getAddressesShort();
            if(log.isDebugEnabled()) log.debug("AddressPartner�s shortaddresses: " + shortAddresses.toString());
            request.setAttribute(ReadAddressDataAction.RK_SHORTADDRESS, shortAddresses);
        }
        else{
            user.addMessage(new Message(Message.ERROR,"user.noSoldTo"));
			log.exiting();
            return mapping.findForward("failure");
        }
        request.setAttribute(ActionConstants.RC_ADDRESS_FORMULAR, addressFormular);
        forwardTo = "soldtoChange";
    }
    // case C: if the user decided to cancel
    else if(request.getParameter("cancelClicked").length() > 0) {
		log.exiting();
    	return mapping.findForward("cancel");
    }
    // case D: the user decided to save the personal details
    else {

        // Note:
        // Either firstName/lastName or name1/name2 is equal to "" within
        // the address-object which associated to the addressFormular-object
        // because of the kind of the JSP's construction. A title change is
        // not allowed while displaying the personal details

        // WORKAROUND because of the behavior of the CRM backend related to
        // address changes of addresses with existing tax jurisdiction code.
        // However, this involves the county selection problem which must be
        // fixed later.

        // if no 'new' tax juridiction code was given by selecting a county
        // then take the 'old' under the following conditions:
        // The values for postal code, city, country and region are unchanged.
        /*if (addressFormular.getAddress().getTaxJurCode().equals("") &&
            addressFormular.getAddress().getCity().equalsIgnoreCase(user.getSoldToAddress().getCity()) &&
            addressFormular.getAddress().getPostlCod1().equalsIgnoreCase(user.getSoldToAddress().getPostlCod1()) &&
            addressFormular.getAddress().getCountry().equalsIgnoreCase(user.getSoldToAddress().getCountry()) &&
            addressFormular.getAddress().getRegion().equalsIgnoreCase(user.getSoldToAddress().getRegion())) {
            addressFormular.getAddress().setTaxJurCode(user.getSoldToAddress().getTaxJurCode());
        }*/

        // save the changes to the backend...saveStatus returns the status of the process
        forwardBase = 
                UserActions.performChangeAddressData(mapping,
                                                     request,
                                                     requestParser,
                                                     user,
                                                     addressFormular.getAddress());


    }// end of if block to decide further processing

    if(forwardBase == null){ // i.e. "countryChange", "soldtoChange" was requested
		log.exiting();
    	return mapping.findForward(forwardTo);	
    }
    else {
    
        // TODO (?): update welcome-text when successful
        
    	if( !forwardBase.getName().equals("success")){
        	formsOfAddress = shop.getTitleList();
            listOfCountries = shop.getCountryList();

            if (addressFormular.isRegionRequired()){
                listOfRegions = shop.getRegionList(addressFormular.getAddress().getCountry());
            }

            // set request attributes
            request.setAttribute(ActionConstants.RC_FORMS_OF_ADDRESS, formsOfAddress);
            request.setAttribute(ActionConstants.RC_COUNTRY_LIST, listOfCountries);
            if (listOfRegions !=null) {
                request.setAttribute(ActionConstants.RC_REGION_LIST, listOfRegions);
            }
            request.setAttribute(ActionConstants.RC_ADDRESS_FORMULAR, addressFormular);
            
            // get Soldto and the ShortAddress:
            request.setAttribute(ReadAddressDataAction.RK_ADDRESSPARTNER, requestParser.getParameter("addresspartner").getValue().getString());
        
            bupa = bupama.getBusinessPartner(requestParser.getParameter("addresspartner").getValue().getString());
            if(bupa != null){
                //String shortAddress = bupa.getAddressesShort().getTable().getRow(1).getField("DESC").getString();
                //request.setAttribute(ReadAddressDataAction.RK_SHORTADDRESS, shortAddress);
            	if(log.isDebugEnabled()) log.debug("BusinessPartner as AddressPartner found");
            	// read all short addresses for the selected sold-to
            	ResultData shortAddresses = bupa.getAddressesShort();
                if(log.isDebugEnabled()) log.debug("AddressPartner�s shortaddresses: " + shortAddresses.toString());
                request.setAttribute(ReadAddressDataAction.RK_SHORTADDRESS, shortAddresses);
            }
            else{
                user.addMessage(new Message(Message.ERROR,"user.noSoldTo"));
				log.exiting();
                return mapping.findForward("failure");
            }
            
            // set selected address
            request.setAttribute(ReadAddressDataAction.ADDR_TECHKEY, requestParser.getParameter(ReadAddressDataAction.ADDR_TECHKEY).getValue().getString());
    	}
        
    	if(forwardBase.getName().equals("failure")){
            // nothing to do additionally
    		// all needed data will be read in "if( !forwardBase.getName().equals("success"))"
        }
        else if(forwardBase.getName().equals("countyRequired")){
        
            // additionally read list of counties
            ResultData listOfCounties = shop.getTaxCodeList(addressFormular.getAddress().getCountry(),
                                                            addressFormular.getAddress().getRegion(),
                                                            addressFormular.getAddress().getPostlCod1(),
                                                            addressFormular.getAddress().getCity());
            // set request attributes
            request.setAttribute(ActionConstants.RC_POSSIBLE_COUNTIES, listOfCounties);
        } 
		log.exiting();
        return forwardBase;
    }
}
}
