/**
 * Class         SoldToReadListAction Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
 * Description:  Action to display the SoldToList    Created:      March 2001 Version: 0.1
 */
package com.sap.isa.isacore.action.b2b;

import com.sap.isa.businessobject.AppBaseBusinessObjectManager;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.businessobject.IdMapper;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businessobject.businessevent.LoginEvent;
import com.sap.isa.businesspartner.backend.boi.BusinessPartnerData;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.Agent;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.businesspartner.businessobject.PartnerFunctionBase;
import com.sap.isa.businesspartner.businessobject.Reseller;
import com.sap.isa.businesspartner.businessobject.ResponsibleAtPartner;
import com.sap.isa.businesspartner.businessobject.SoldFrom;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.action.IsaCoreInitAction;


import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Read the SolTos for the User <br>
 * Note that all Action classes have to provide a non argument constructor because they were instantiated
 * automatically by the action servlet.
 */
public class ReadSoldToAction extends IsaCoreBaseAction {
    
    public static final String RK_SOLDTO_LIST = "soldtolist";

    /** Name of the request parameter directly setting the soldTo. */
    public static final String PARAM_SOLDTO = "soldTo";

    /** Name of the request parameter directly setting the soldToTechKey. */
    public static final String PARAM_SOLDTO_TECHKEY = "soldToTechKey";

    /**
     * creates the parameter list for the soldTo
     *
     * @param soldToList DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public static String createRequest(ResultData soldToList) {
        return "?" + PARAM_SOLDTO_TECHKEY + "=" + soldToList.getString("SOLDTO_TECHKEY") + "&" + PARAM_SOLDTO + "=" +
               soldToList.getString("SOLDTO");
    }

    /**
     * Implement this method to add functionality to your action.
     *
     * @param mapping DOCUMENT ME!
     * @param form The <code>FormBean</code> specified in the config.xml file for this action
     * @param request The request object
     * @param response The response object
     * @param userSessionData Object wrapping the session
     * @param requestParser Parser to simple retrieve data from the request
     * @param bom Reference to the BusinessObjectManager
     * @param log Reference to the IsaLocation, needed for logging
     * @param startupParameter DOCUMENT ME!
     * @param eventHandler DOCUMENT ME!
     *
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping                      mapping,
                                    ActionForm                         form,
                                    HttpServletRequest                 request,
                                    HttpServletResponse                response,
                                    UserSessionData                    userSessionData,
                                    RequestParser                      requestParser,
                                    BusinessObjectManager              bom,
                                    IsaLocation                        log,
                                    IsaCoreInitAction.StartupParameter startupParameter,
                                    BusinessEventHandler               eventHandler)
              throws CommunicationException {
        String forward = "success";

        final String METHOD_NAME = "isaPerform()";
        log.entering(METHOD_NAME);

        RequestParser.Value soldToValue = requestParser.getParameter(PARAM_SOLDTO).getValue();
        RequestParser.Value soldToTechKeyValue = requestParser.getParameter(PARAM_SOLDTO_TECHKEY).getValue();

        if (!soldToValue.isSet()) {
            // No request parameter, try the request attribute
            soldToValue = requestParser.getAttribute(PARAM_SOLDTO).getValue();
            soldToTechKeyValue = requestParser.getAttribute(PARAM_SOLDTO_TECHKEY).getValue();
        }

        // check if the soldTo parameter exist
        if (soldToTechKeyValue.isSet()) {
            // actual user data until all getSoldTo calls are erased
            TechKey soldToTechKey = new TechKey(soldToTechKeyValue.getString());
            ResultData soldToList = null;

            if (soldToList == null) {
                soldToList = (ResultData) userSessionData.getAttribute(RK_SOLDTO_LIST);
            }

            if (soldToList == null) {
                soldToList = (ResultData) request.getAttribute(RK_SOLDTO_LIST);
            }

            soldToList.first();

            //forward = readSoldTo(bom, eventHandler, soldToList, soldToTechKey, soldToValue.getString());
			forward = readSoldTo(userSessionData, bom, eventHandler, soldToList, soldToTechKey, soldToValue.getString());
        }
        else {
            forward = "failure";
        }

        log.exiting();

        return mapping.findForward(forward);
    }

    /**
     * DOCUMENT ME!
     *
     * @param bom DOCUMENT ME!
     * @param eventHandler DOCUMENT ME!
     * @param soldToList DOCUMENT ME!
     * @param soldToTechKey DOCUMENT ME!
     * @param soldTo DOCUMENT ME!
     *
     * @return the forward to go to. "success" if ok, "failure" if not.
     */
    public String readSoldTo(BusinessObjectManager bom,
                             BusinessEventHandler  eventHandler,
                             ResultData            soldToList,
                             TechKey               soldToTechKey,
                             String                soldTo) {
        String forward = "failure";

        User user = bom.getUser();
        BusinessPartnerManager buPaMa = bom.createBUPAManager();
        Shop shop = bom.getShop();

        if (soldToList.searchRowByColumn(BusinessPartnerData.SOLDTO_TECHKEY, soldToTechKey.getIdAsString())) {
            if (soldToList.getString(BusinessPartnerData.SOLDTO).equals(soldTo)) {
                BusinessPartner partner = buPaMa.createBusinessPartner(soldToTechKey, soldTo, null);

                PartnerFunctionBase partnerFunction = null;

                if (shop.getCompanyPartnerFunction().equals(PartnerFunctionBase.RESELLER)) {
                    partnerFunction = new Reseller();

                    BusinessPartner respPartner = buPaMa.getDefaultBusinessPartner(PartnerFunctionData.CONTACT);

                    // contact should not be transfered to the order
                    // correction: don't delete partner function contact
                    //             if it should not be transfered to the order 
                    //             don't put it into the partnerlist.
                    //buPaMa.resetDefaultBusinessPartner(PartnerFunctionBase.CONTACT);

                    ResponsibleAtPartner responsibleAtPartnerPF = new ResponsibleAtPartner();
                    respPartner.addPartnerFunction(responsibleAtPartnerPF);

                    buPaMa.setDefaultBusinessPartner(respPartner.getTechKey(), responsibleAtPartnerPF.getName());
                }

                //check if it is an agent (relevant for BOB scenario)
                else if (shop.getCompanyPartnerFunction().equals(PartnerFunctionBase.AGENT)) {
                    partnerFunction = new Agent();

                    // contact should not be transfered to the order
                    // correction: don't delete partner function contact
					//             if it should not be transfered to the order 
					//             don't put it into the partnerlist.
                    //buPaMa.resetDefaultBusinessPartner(PartnerFunctionBase.CONTACT);
                }
                else {
                    partnerFunction = new com.sap.isa.businesspartner.businessobject.SoldTo();

                    if (eventHandler != null) {
                        LoginEvent event = new LoginEvent(user);
                        eventHandler.fireLoginEvent(event);
                    }
                }

                partner.addPartnerFunction(partnerFunction);
                buPaMa.setDefaultBusinessPartner(partner.getTechKey(), partnerFunction.getName());

                if (shop.isHomActivated()) {
                    // In the pom scenario: add also the partner function soldfrom
                    partnerFunction = new SoldFrom();
                    partner.addPartnerFunction(partnerFunction);
                    buPaMa.setDefaultBusinessPartner(partner.getTechKey(), partnerFunction.getName());
                }

                // set the partner as alternative partner in marketing functions
                user.getMktPartner().setAlternativePartner(soldToTechKey);
                forward = "success"; // important!!
            }
        }

        log.exiting();

        return forward;
    }
    
    
	/**
	 * Create a business partner for the selected soldto party.
	 * Put it into the business partner manager. 
	 * Add scenario specific partner functions to the soldto.
	 * 	  
	 * @param bom DOCUMENT ME!
	 * @param eventHandler DOCUMENT ME!
	 * @param soldToList DOCUMENT ME!
	 * @param soldToTechKey DOCUMENT ME!
	 * @param soldTo DOCUMENT ME!
	 * @param userSessionData
	 *
	 * @return the forward to go to. "success" if ok, "failure" if not.
	 */
	public String readSoldTo(UserSessionData       userSessionData,
	                         BusinessObjectManager bom,
							 BusinessEventHandler  eventHandler,
							 ResultData            soldToList,
							 TechKey               soldToTechKey,
							 String                soldTo) 
	        throws CommunicationException {
		String forward = "failure";

		User user = bom.getUser();
		BusinessPartnerManager buPaMa = bom.createBUPAManager();
		Shop shop = bom.getShop();

		if (soldToList.searchRowByColumn(BusinessPartnerData.SOLDTO_TECHKEY, soldToTechKey.getIdAsString())) {
			if (soldToList.getString(BusinessPartnerData.SOLDTO).equals(soldTo)) {
				BusinessPartner partner = buPaMa.createBusinessPartner(soldToTechKey, soldTo, null);

				PartnerFunctionBase partnerFunction = null;

				if (shop.getCompanyPartnerFunction().equals(PartnerFunctionBase.RESELLER)) {
					partnerFunction = new Reseller();

					BusinessPartner respPartner = buPaMa.getDefaultBusinessPartner(PartnerFunctionData.CONTACT);

					// contact should not be transfered to the order
					// correction: don't delete partner function contact
					//             if it should not be transfered to the order 
					//             don't put it into the partnerlist.
					//buPaMa.resetDefaultBusinessPartner(PartnerFunctionBase.CONTACT);

					ResponsibleAtPartner responsibleAtPartnerPF = new ResponsibleAtPartner();
					respPartner.addPartnerFunction(responsibleAtPartnerPF);

					buPaMa.setDefaultBusinessPartner(respPartner.getTechKey(), responsibleAtPartnerPF.getName());
				}

				//check if it is an agent (relevant for BOB scenario)
				else if (shop.getCompanyPartnerFunction().equals(PartnerFunctionBase.AGENT)) {
					partnerFunction = new Agent();

					// contact should not be transfered to the order
					// correction: don't delete partner function contact
					//             if it should not be transfered to the order 
					//             don't put it into the partnerlist.
					//buPaMa.resetDefaultBusinessPartner(PartnerFunctionBase.CONTACT);
				}
				else {
					partnerFunction = new com.sap.isa.businesspartner.businessobject.SoldTo();

					if (eventHandler != null) {
						LoginEvent event = new LoginEvent(user);
						eventHandler.fireLoginEvent(event);
					}
				}

				partner.addPartnerFunction(partnerFunction);
				buPaMa.setDefaultBusinessPartner(partner.getTechKey(), partnerFunction.getName());

				if (shop.isHomActivated()) {
					// In the pom scenario: add also the partner function soldfrom
					partnerFunction = new SoldFrom();
					partner.addPartnerFunction(partnerFunction);
					buPaMa.setDefaultBusinessPartner(partner.getTechKey(), partnerFunction.getName());
				}

				// set the partner as alternative partner in marketing functions
				// in special scenarios an id mapping is needed
				AppBaseBusinessObjectManager appBaseBom = (AppBaseBusinessObjectManager) userSessionData.getBOM(AppBaseBusinessObjectManager.APPBASE_BOM);
				IdMapper idMapper = appBaseBom.createIdMapper();
				String mappedSoldToTechKey = idMapper.mapIdentifier(IdMapper.SOLDTO, IdMapper.USER, IdMapper.MARKETING, soldToTechKey.toString());
				user.getMktPartner().setAlternativePartner(new TechKey(mappedSoldToTechKey));
				 
				
				forward = "success"; // important!!
			}
		}

		log.exiting();

		return forward;
	}    
}
