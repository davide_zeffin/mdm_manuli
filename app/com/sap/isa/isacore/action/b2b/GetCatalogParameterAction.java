package com.sap.isa.isacore.action.b2b;

//struts imports
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.webcatalog.ItemDetailRequest;
import com.sap.isa.core.BaseAction;
import com.sap.isa.core.util.Message;
import com.sap.isa.isacore.MessageDisplayer;


public class GetCatalogParameterAction extends BaseAction {

  /**
   * String constant specifying if auctions are enabled.
   */
  private static final String PARAM_PRODUCT_ID        = "productId";
  private static final String PARAM_PRODUCT_AREA      = "productArea";
  private static final String PARAM_PRODUCT_SCENARIO  = "productScenario";
  private static final String NO_PRODUCT_FOUND 	    = "noProductFound";
 
  /* define an inner class to implement the <code>ItemDetailRequest</code> object */
  private class DetailRequest implements ItemDetailRequest {

      private String id;
      private String areaId;
      private String catalog = "";
      private String scenario = "";

      public DetailRequest (String id, String areaId, String scenario) {
          this.id       = id;
          this.areaId   = areaId;
          this.scenario = scenario;
      }

      /**
       * The ID of the item! to be requested
       */
      public String getProductID() {
          return id;
      };


      /**
       * Reference to IPC-Item containing configuration
       */
      public Object getConfigurationItem() {
          return null;
      }


      /**
       * Key for catalog this item was taken from
       */
      public String getCatalogID() {
          return catalog;
      }

      public String getDetailScenario() {
          return scenario;
      }

      /**
       * Key for catalog area this item was taken from
       */
      public String getAreaID() {
          return areaId;
      }

  }

    /**
     * Process the specified HTTP request, and create the corresponding HTTP
     * response (or forward to another web component that will create it).
     * Return an <code>ActionForward</code> instance describing where and how
     * control should be forwarded, or <code>null</code> if the response has
     * already been completed.
     *
     * @param mapping The ActionMapping used to select this instance
     * @param actionForm The optional ActionForm bean for this request (if any)
     * @param request The HTTP request we are processing
     * @param response The HTTP response we are creating
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     */

  public ActionForward doPerform( ActionMapping mapping,
                                  ActionForm form,
                                  HttpServletRequest request,
                                  HttpServletResponse response)
  throws ServletException
  {
	final String METHOD_NAME = "doPerform()";
	log.entering(METHOD_NAME);
  	String forward = "success";
    HttpSession session = request.getSession();

    if (session.getAttribute(PARAM_PRODUCT_SCENARIO).toString().equals(NO_PRODUCT_FOUND)) {
		MessageDisplayer messageDisplayer = new MessageDisplayer();
						messageDisplayer.addMessage(new Message(Message.ERROR,"product.not.in.catalog"));
						messageDisplayer.setOnlyDisplayMessages();
						messageDisplayer.addToRequest(request);
						forward = "message";
    	
    } else {
		// get the appropriate product informations from the session
		GetCatalogParameterAction.DetailRequest detailRequest =
		  new GetCatalogParameterAction.DetailRequest
					 ((String) session.getAttribute(PARAM_PRODUCT_ID),
					 (String) session.getAttribute(PARAM_PRODUCT_AREA),
					 (String) session.getAttribute(PARAM_PRODUCT_SCENARIO));
		
		request.setAttribute("itemRequest",detailRequest);
    	
    }

	// remove the session attributes
	session.removeAttribute(PARAM_PRODUCT_ID);
	session.removeAttribute(PARAM_PRODUCT_AREA);
	session.removeAttribute(PARAM_PRODUCT_SCENARIO);
	
    log.exiting();
    return mapping.findForward(forward);
  }
}
