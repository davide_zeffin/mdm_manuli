/**
 *  Class:        SwitcDocumentsOnTopAction
 *  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
 *  Author:       Franz-Dieter Berger
 *  Created:      17.05.2001
 *  Version:      1.0
 *  $Revision: #4 $
 *  $Date: 2001/07/24 $
 */

package com.sap.isa.isacore.action.b2b;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 *  Takes the document from the background and puts it into the foregroud.
 *  It also removes the catalog.
 *  <br>
 *  The only possible forward is the constant FORWARD_SUCCESS.
 *
 *@author     Franz-Dieter Berger
 *@created    12. Dezember 2001
 *@version    1.0
 */
public class SwitchDocumentsOnTopAction extends IsaCoreBaseAction {

    /**
     * Constant for available forwards, value is &quot;success&quot;.
     */
    protected final static String FORWARD_SUCCESS = "success";
	
    /**
     *  Overriden <em>isaPerform</em> method of <em>IsaCoreBaseAction</em>.
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser parser,
            BusinessObjectManager bom,
            IsaLocation log)
             throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        // Page that should be displayed next.
        String forwardTo = null;
        HttpSession session = request.getSession();

        DocumentHandler documentHandler = (DocumentHandler)
                userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
        if (documentHandler == null) {
			log.exiting();
            throw new PanicException("No document handler found.");
        }

        documentHandler.switchDocumentsOnTop();
        documentHandler.setCatalogOnTop(false);

        forwardTo = FORWARD_SUCCESS;
		log.exiting();
        return mapping.findForward(forwardTo);
    }
}
