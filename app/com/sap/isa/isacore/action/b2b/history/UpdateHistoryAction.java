/*******************************************************************
   Class:		UpdateHistoryAction
   Copyright (c) 2001, SAP AG, All rights reserved.  
   Author:	SAP AG
   Created:   29.05.2001  Version:      1.0  
   $Revision: #5 $
   $Date: 2001/12/17 $
******************************************************************/

package com.sap.isa.isacore.action.b2b.history;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.History;
import com.sap.isa.isacore.HistoryCookieLoader;
import com.sap.isa.isacore.HistoryLoader;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 *  Puts the history in the request context and saves the history.
 *  If the action is called for the first time in a session, it loads the
 *  history data into the history object, in subsequent calls is gets the data
 *  from the history object.<br>
 *  For loading and saving the history data, a HistoryLoader is used. It is instantiated
 *  in the IsaCoreBaseAction. Up to now, we have only the HistoryCookieLoader.
 * <br>
 * The following forwards are used by this action:
 * </p>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>FORWARD_SUCCESS</td><td>forward used if everything works just fine.</td></tr>
 *   <tr><td>FORWARD_FAILURE</td><td>forward used if the history could not be set into the request context.</td></tr>
 * </table>
 *
 *@see com.sap.isa.isacore.History
 *
 *@author     SAP AG
 *@created    6. Dezember 2001
 *@version    1.0
 */
public class UpdateHistoryAction extends IsaCoreBaseAction {

    /**
     *  Session context constant, value is &quot;first&quot;. It marks, if the action is called for the first
     *  time in this session. Only then, the cookie is loaded and parsed. In subsequent calls,
     *  we get the hstory directly fron the history object.
     */
    public final static String FIRST = "first";

    /**
     * Request context constant for storage of the history, value is &quot;historylist&quot;.
     */
    public final static String HISTORY_LIST = "historylist";

    /**
     * Constant for available forwards, value is &quot;success&quot;.
     */
    protected final static String FORWARD_SUCCESS = "success";

    /**
     * Constant for available forwards, value is &quot;failure&quot;.
     */
    protected final static String FORWARD_FAILURE = "failure";
	
    /**
     *  Overriden <em>isaPerform</em> method of <em>IsaCoreBaseAction</em>.
     *
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser parser,
            BusinessObjectManager bom,
            IsaLocation log)
             throws CommunicationException {
             	
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        String logPrefix = null;
        boolean isDebugEnabled = log.isDebugEnabled();
        Shop shop = bom.getShop();
        User user = bom.getUser();

		HttpSession session = request.getSession();

        DocumentHandler docHandler = (DocumentHandler)
                userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);

        if (isDebugEnabled) {
            logPrefix = "isaPerform(): ";
        }

        // Lets see, if we have all needed objects
        if (docHandler == null) {
            log.debug(logPrefix + "No DocumentHandler in the session context. This is a serious error! But i will proceed with forward=failure");
            log.exiting();
            return mapping.findForward(FORWARD_FAILURE);
        }
        if (shop == null) {
            log.debug(logPrefix + "Did not get a shop from the BOM. This is a serious error! But i will proceed with forward=failure");
            log.exiting();
            return mapping.findForward(FORWARD_FAILURE);
        }
        if (user == null) {
            log.debug(logPrefix + "Did not get a user from the BOM. This is a serious error! But i will proceed with forward=failure");
            log.exiting();
            return mapping.findForward(FORWARD_FAILURE);
        }
        
		//	get the partner directly assigned to the user.
	 	TechKey partnerKey = user.getBusinessPartner();

        if (partnerKey == null) {
            log.debug(logPrefix + "The user is not assigned to a businesspartner. This is a serious error! But i will proceed with forward=failure");
            log.exiting();
            return mapping.findForward(FORWARD_FAILURE);
        }
        
		String userid = partnerKey.getIdAsString(); 

		// get the additional partner (e.g SoldTo, Reseller or Agent) from the
		// marketing user object.
		// The alternative partner will be set in the ReadSoldToAction.        
		partnerKey = user.getMktPartner().getAlternativePartner();
        
        if (partnerKey == null) {
			log.debug(logPrefix + "The user is not assigned to an additional partner. This is a serious error! But i will proceed with forward=failure");
			log.exiting();
			return mapping.findForward(FORWARD_FAILURE);
		}

		userid = userid + partnerKey.getIdAsString();

        String shopid = shop.getId();

        History history = docHandler.getHistory();
        if (history == null) {
            log.debug(logPrefix + "There is no history object. This is a serious error! But i will proceed with forward=failure");
			log.exiting();
            return mapping.findForward(FORWARD_FAILURE);
        }

        HistoryLoader loader = history.getLoader();
        if (loader == null) {
            log.debug(logPrefix + "There is no history loader. This is a serious error! But i will proceed with forward=failure");
			log.exiting();
            return mapping.findForward(FORWARD_FAILURE);
        }
        if (loader instanceof HistoryCookieLoader) {
            // the Cookieloader gets its data from the request and puts the cookie in the response
            HistoryCookieLoader cLoader = (HistoryCookieLoader) loader;
            cLoader.setContext(request, response);
        }

        if (session.getAttribute(FIRST) != "false") {
            // if session is new =>load history
            history.load(userid, shopid);
        }

        List itemlist = history.getHistoryList();
        request.setAttribute(HISTORY_LIST, itemlist);

        if (isDebugEnabled) {
            log.debug(logPrefix + "is session new ? : " + session.getAttribute(FIRST));
            log.debug(logPrefix + "set itemlist in request.Number of items : " + itemlist.size());
            log.debug(logPrefix + "sessionid : " + session.getId());
        }

        history.save(userid, shopid);
        session.setAttribute(FIRST, "false");
		log.exiting();
        return mapping.findForward(FORWARD_SUCCESS);
    }
}
