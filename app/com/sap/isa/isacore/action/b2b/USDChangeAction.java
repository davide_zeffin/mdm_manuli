/*
 * Created on 25.01.2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.isacore.action.b2b;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.User;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 * @author d031177
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class USDChangeAction extends IsaCoreBaseAction {
	
	public static final String UIMODE = "uimode";
	public static final String PRICE_DISPLAY = "P";
	public static final String STOCK_DISPLAY = "S";
	public static final String PRICE_STOCK_DISPLAY = "C";
	public static final String GRID_DISPLAY = "G";
	public static final String GRID_PRICE = "gridprice";
	public static final String GRID_STOCK = "gridstock";
	public static final String CHECKED = "X";
	
	/**
		 * Implement this method to add functionality to your action.
		 *
		 * @param form              The <code>FormBean</code> specified in the
		 *                          config.xml file for this action
		 * @param request           The request object
		 * @param response          The response object
		 * @param userSessionData   Object wrapping the session
		 * @param requestParser     Parser to simple retrieve data from the request
		 * @param bom               Reference to the BusinessObjectManager
		 * @param log               Reference to the IsaLocation, needed for logging
		 * @return Forward to another action or page
		 */
		public ActionForward isaPerform(ActionMapping mapping,
				ActionForm form,
				HttpServletRequest request,
				HttpServletResponse response,
				UserSessionData userSessionData,
				RequestParser requestParser,
				BusinessObjectManager bom,
				IsaLocation log)
				throws CommunicationException {
			final String METHOD_NAME = "USDChangeAction - isaPerform()";
			log.entering(METHOD_NAME);
			User user = bom.getUser();

			user.clearMessages();
			request.setAttribute(BusinessObjectBase.CONTEXT_NAME, user);

			//if Cancel button clicked
			if ( request.getParameter("cancel") == null && 
			     request.getParameter("cancel").equals(CHECKED)) {
				 // user has cancel the passwordchange
				 log.exiting();
				 return mapping.findForward("success"); //even this goes to start page of MY details
			}
		
			if(request.getParameter(GRID_PRICE).equals(CHECKED) && 
				request.getParameter(GRID_STOCK).equals(CHECKED)){
					userSessionData.setAttribute(UIMODE, PRICE_STOCK_DISPLAY);
			}
			else if(request.getParameter(GRID_PRICE).equals(CHECKED)){
				userSessionData.setAttribute(UIMODE, PRICE_DISPLAY);	
			}
			else if(request.getParameter(GRID_STOCK).equals(CHECKED)){
				userSessionData.setAttribute(UIMODE, STOCK_DISPLAY);
			}
			else{
				userSessionData.setAttribute(UIMODE, GRID_DISPLAY);
			}
		
			log.exiting();
			return mapping.findForward("success");
		}

}
