/*****************************************************************************
  Class:        LogoffAction
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Franz-Dieter Berger
  Created:      21.05.2001
  Version:      1.0

  $Revision: #1 $
  $Date: 2001/06/26 $
*****************************************************************************/

package com.sap.isa.isacore.action.b2b;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.action.IsaCoreInitAction;
import com.sap.isa.isacore.action.oci.OciLinesSendAction;

/**
 * Logs off, by invalidating the session.
 *
 * @author Franz-Dieter Berger
 * @version 1.0
 */
public class LogoffAction extends IsaCoreBaseAction {

    private static final String FORWARD_SUCCESS           = "success";
    private static final String FORWARD_OCI_SEND_BASKET   = "ocisendbasket";
	
    /**
     * Overriden <em>isaPerform</em> method of <em>IsaCoreBaseAction</em>.
     */
    public ActionForward isaPerform(
                ActionMapping mapping,
                ActionForm form,
                HttpServletRequest request,
                HttpServletResponse response,
                UserSessionData userSessionData,
                RequestParser parser,
                BusinessObjectManager bom,
                IsaLocation log,
                IsaCoreInitAction.StartupParameter startupParameter,
                BusinessEventHandler eventHandler)
                        throws CommunicationException {
	final String METHOD_NAME = "isaPerform()";
	log.entering(METHOD_NAME);

      // Page that should be displayed next.
      String forwardTo = null;


      // check for oci transfer
      boolean ociTransfer = startupParameter != null &&
              startupParameter.getHookUrl().length() > 0;

      if (!ociTransfer) {
          forwardTo = FORWARD_SUCCESS;
      }
      else {

          // in oci case, remove sales doc to transfer
          userSessionData.removeAttribute(
                OciLinesSendAction.SALES_DOCUMENT_KEY);
          forwardTo = FORWARD_OCI_SEND_BASKET;
      }
      
      // if the current salesdocument supports recovery all information that was generated
      // to recover the document must be destroyed
      SalesDocument salesDoc = bom.getBasket();

      if (salesDoc != null && salesDoc.isDocumentRecoverable()) {

          if (log.isDebugEnabled()) {
              log.debug("LogoffAction - Destroy Content of Sales Document Basket");
          }
              // finally delete the basket in the backend
              salesDoc.destroyContent();
      }
      
      salesDoc = bom.getOrderTemplate();

      if (salesDoc != null && salesDoc.isDocumentRecoverable()) {

          if (log.isDebugEnabled()) {
              log.debug("LogoffAction - Destroy Content of Sales Document Ordertemplate");
          }
              // finally delete the basket in the backend
              salesDoc.destroyContent();
      }
    
	  log.exiting();
      return mapping.findForward(forwardTo);
  }
}
