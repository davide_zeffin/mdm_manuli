/*****************************************************************************
	Class:        SoldToReadListBaseAction
	Copyright (c) 2004, SAPLabs, Palo Alto, All rights reserved.
	Author        SAP

	$Revision: #1 $

*****************************************************************************/

package com.sap.isa.isacore.action.b2b;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

public class SoldToReadListBaseAction extends IsaCoreBaseAction {
	/**
	 * Name of the lists of marketing attributes stored in the request context.
	 */
	public static final String RK_SOLDTO_LIST = "soldtolist";
	/**
	 * Flag to signal problems during auto entry process
	 */
	public static final String RK_AUTOENTRYERROR = "autoentryerror";
	/**
	 * Name of the request attribute to get additional infos about the problem
	 */
	public static final String RK_AUTOENTRYADDINFO = "autoentryaddinfo";

	public ResultData readSoldToList(
		User user,
		String shopId,
		BusinessObjectManager bom)
		throws CommunicationException {
		final String METHOD_NAME = "readSoldToList()";
		log.entering(METHOD_NAME);
		BusinessPartnerManager buPaMa = bom.createBUPAManager();

		BusinessPartner contact =
			buPaMa.getDefaultBusinessPartner(PartnerFunctionData.CONTACT);
		ResultData result = buPaMa.getSoldTosBySalesArea(contact, shopId);

		return result;
	}

}
