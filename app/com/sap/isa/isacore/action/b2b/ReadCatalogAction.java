/*****************************************************************************
    Class         ReadCatalogAction
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Action to read the selected catalog
    Author:       Martin Schley
    Created:      March 2001
    Version:      0.1

    $Revision: #3 $
    $Date: 2001/07/26 $

*****************************************************************************/

package com.sap.isa.isacore.action.b2b;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.businessobject.webcatalog.CatalogBusinessObjectManager;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.JspUtil;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.isacore.MessageDisplayer;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 * Set the give catalog key in the shop data. 
 *
 * <b>The action doesn't read the catalog (Sorry for the mistakable name)</b>. <br>
 * 
 * Within this action the catalogkey and the views are taken from the request
 * and will be set within the actual shop object. Beyond the org data for the given catalog
 * will be read. <br>
 * 
 * First the action checks, if catalog determination is activated. If not
 * the forward <code>success</code> is set directly because all data are still set
 * in shop in this case. 
 *
 */
public class ReadCatalogAction extends IsaCoreBaseAction {
	
    public static final String RK_SOLDTO_LIST = "cataloglist";

    /**
     * Name of the request parameter for the catalog key.
     */
    public static final String PARAM_CATALOG = "catalogKey";

    /**
     * Name of the request parameter for the catalog view.
     */
    public static final String PARAM_VIEW = "catalogViews[]";


    /** 
     * creates the parameter list for the given catalog 
     * 
     * @param catalog key and views for a catalog
     */
    public static String createRequest(ResultData catalogList){

        StringBuffer parameter = new StringBuffer(30);

        parameter.append('?').append(PARAM_CATALOG).append('=')
                 .append(JspUtil.encodeURL(JspUtil.encodeHtml(catalogList.getString(User.RD_CATALOG_KEY).trim())));

        // add all views
        String[] views = (String[])catalogList.getObject(User.RD_CATALOG_VIEWS);

        for (int i=0;i<views.length;i++) {
            parameter.append('&').append(PARAM_VIEW).append('=').append(JspUtil.encodeHtml(views[i]));
        }

        // return JspUtil.encodeURL(parameter.toString());
        return parameter.toString();
    }


    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            BusinessObjectManager bom,
            IsaLocation log)
            throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        Shop shop = bom.getShop();

        if (shop == null) {
			log.exiting();
            throw new PanicException("shop.notFound");
        }


        if (!shop.isCatalogDeterminationActived()) {
			log.exiting();
            return mapping.findForward("success");
        }

        CatalogBusinessObjectManager cbom =
                getCatalogBusinessObjectManager(userSessionData);

        RequestParser.Value catalogValue = requestParser.getParameter(PARAM_CATALOG).getValue();

        if (!catalogValue.isSet()) {
            // No request parameter, try the request attribute
            catalogValue = requestParser.getAttribute(PARAM_CATALOG).getValue();
        }


       // check if the catalog parameter exist
        if (catalogValue.isSet()) {
            String catalogKey = catalogValue.getString();

            if (cbom.getCatalog() != null) {
                if (catalogKey.equals(shop.getCatalog()) ) {
					log.exiting();
                    return mapping.findForward("success");
                }
                else {
                    MessageDisplayer messageDisplayer = new MessageDisplayer("catalogexist",true);
                    messageDisplayer.addMessage(new Message(Message.ERROR,"b2b.catalog.existAlready"));
                    messageDisplayer.addToRequest(request);
					log.exiting();
                    return mapping.findForward("catalogexist");
                }
            }

            shop.readOrgData(catalogKey);

            String[] views;

            // set the catalog views
            RequestParser.Parameter viewParameter = requestParser.getParameter(PARAM_VIEW);
            if (viewParameter.isSet()) {
                views = new String[viewParameter.getNumValues()];
                for (int i = 0; i < viewParameter.getNumValues(); i++) {
                    views[i] = viewParameter.getValue(i).getString();
                }
            }
            else {
                views = (String[])request.getAttribute(PARAM_VIEW);
            }


            if (log.isDebugEnabled()){
                log.debug("avaibable views: ");
                for (int i=0;i<views.length;i++) {
                    log.debug(views[i]);
                }
            }


            shop.setViews(views);
        }
        else {
			log.exiting();
            return mapping.findForward("failure");
        }

		log.exiting();
        return mapping.findForward("success");

    }

}