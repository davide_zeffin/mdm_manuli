/*****************************************************************************
  Class:        LoginAction
  Copyright (c) 2004, SAP AG, All rights reserved.
  Author:       SAP AG
  Created:      2004
  Version:      1.0

  $Revision: #1 $
  $Date: 2004/11/09 $
*****************************************************************************/
package com.sap.isa.isacore.action.b2b;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.TechKey;
import com.sap.isa.businessobject.AppBaseBusinessObjectManager;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.User;
import com.sap.isa.businessobject.IdMapper;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.businesspartner.businessobject.Contact;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.action.IsaCoreInitAction;

/**
 * Performs steps after successful user login in ISA B2B shop.<br />
 * This Struts Action will be called in connection with the E-Commerce Logon 
 * Module that provides the logon functionality. In this Action the 
 * initialization of Business Partner Manager with the Business Partner 
 * &qout;Contact Person&qout; will be fulfilled.
 *
 * @author D038380
 * @version 1.0
 *
 */
public class PostLoginAction extends IsaCoreBaseAction {

    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @param startupParameter  Object containing the startup parameters
     * @param eventHandler      Object to capture events with
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            BusinessObjectManager bom,
            IsaLocation log,
            IsaCoreInitAction.StartupParameter startupParameter,
            BusinessEventHandler eventHandler)
                    throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        // Page that should be displayed next.
        String forwardTo = "success";

        User user = bom.getUser();
        BusinessPartnerManager buPaMa = bom.createBUPAManager();

        // after the user was logged in successfully the data of 
        // bussiness partner manager shall be filled
        BusinessPartner partner = buPaMa.getBusinessPartner(user.getBusinessPartner());

        Contact contact = new Contact();
        partner.addPartnerFunction(contact);
        buPaMa.setDefaultBusinessPartner(partner.getTechKey(),contact.getName());
        
        // in special scenarios an id mapping is needed
		AppBaseBusinessObjectManager appBaseBom = (AppBaseBusinessObjectManager) userSessionData.getBOM(AppBaseBusinessObjectManager.APPBASE_BOM);
		IdMapper idMapper = appBaseBom.createIdMapper();
		String mappedContactTechKey = idMapper.mapIdentifier(IdMapper.CONTACT, IdMapper.USER, IdMapper.MARKETING, user.getBusinessPartner().toString());
        user.resetMarketingSettings(new TechKey(mappedContactTechKey));
        
		log.exiting();
        return mapping.findForward(forwardTo);
    }
}