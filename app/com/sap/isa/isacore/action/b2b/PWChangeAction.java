/*****************************************************************************
	Class         PWChangeAction
	Copyright (c) 2001, SAP Germany, All rights reserved.
	Author:       SAP
	Version:      1.0

	$Revision: #7 $
	$Date: 2004/10/01 $
 *****************************************************************************/
package com.sap.isa.isacore.action.b2b;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.User;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.businesspartner.businessobject.Contact;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.user.action.UserActions;
import com.sap.isa.user.businessobject.UserBase;
import com.sap.isa.user.util.LoginStatus;
import com.sap.isa.user.util.UmeWrapper; 

/**
 * This action changes a password, it can use either for password change on
 * the logon screen or for user switch.
 * Therefore the caller of the jsp must set the parameter ACTION_NAME to
 * the correct value.
 *
 * Note that all Action classes have to provide a non argument constructor
 * because they were instantiated automatically by the action servlet.
 *
 * @author SAP
 * @version 1.0
 *
 */
public class PWChangeAction extends IsaCoreBaseAction {

    /** Name for request parameter used in jsp to get the forward string. */
    public static final String FORWARD_NAME          = "forward";
    /** Define the forward after the password is changed. */
    public static final String FORWARD_NAME_PWCHANGE = "showpwchange";
    /** Define the forward after the password is changed. */
    public static final String FORWARD_NAME_SUCCESS  = "success";
    /** Name for request parameter used in jsp to get the action string. */
    public static final String ACTION_NAME           = "actionpwchange";
    /** normal password change. */
    public static final String ACTION_PW             = "pwchange";
    /** user switch. */
    public static final String ACTION_US             = "userswitch";
    /** expired password. */
    public static final String ACTION_EP             = "expired_password";
    /** user switch. */
    public static final String ACTION_CANCEL         = "cancel";
    /** Parameter name for old password. */
    public static final String PN_OLDPASSWORD        = "nolog_oldpassword"; 
    /** Parameter name for password. */
    public static final String PN_PASSWORD           = "nolog_password";
    /** Parameter name for password verify. */
    public static final String PN_PASSWORD_VERIFY    = "nolog_passwordVerify";
    /** Parameter name for back action. */
    public static final String PN_ACTION_BACK        = "back";
    /** Parameter name for forward action. */
    public static final String PN_ACTION_CONTINUE    = "continue";
	
	/**
	* Security Check. 
	* Check for logged user will be disable here because while changing of an 
	* initial password no user is logged in.
	*/
	public void initialize() {
		if (isB2B()) {
			this.checkUserIsLoggedIn = false;
		}
	}

    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            BusinessObjectManager bom,
            IsaLocation log)
            throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        // Page that should be displayed next.
        String forwardTo = null;

        RequestParser.Parameter forwardParameter = requestParser.getParameter(FORWARD_NAME);

        if (forwardParameter.isSet()) {
            // set the forward again in  context for error handling
            request.setAttribute(FORWARD_NAME,forwardParameter.getValue().getString());
        }

        log.debug("start change password");

		BusinessPartnerManager buPaMa = bom.createBUPAManager();

        User user = bom.getUser();
        user.clearMessages();

        request.setAttribute(BusinessObjectBase.CONTEXT_NAME, user);

        String oldpassword = request.getParameter(PN_OLDPASSWORD);
        String password = request.getParameter(PN_PASSWORD);
        String passwordVerify = request.getParameter(PN_PASSWORD_VERIFY);

        //If no password is entered, an errormessage is displayed

        if (requestParser.getParameter(ACTION_CANCEL).isSet()) {
            // user has cancel the passwordchange
			log.exiting();
            return mapping.findForward("canceled");
        }

        if (requestParser.getParameter(PN_ACTION_BACK).isSet()) {
			log.exiting();
            // user has pushed the back button
            return mapping.findForward("back");
        }

        if (requestParser.getParameter(PN_ACTION_CONTINUE).isSet() && user.isUserLogged()) {
			log.exiting();
            // user has pushed the continue button
            return mapping.findForward("continue");
        }
        else if (requestParser.getParameter(PN_ACTION_CONTINUE).isSet() && !user.isUserLogged()) {
			log.exiting();
            return mapping.findForward("back");
        }


        // reset action in request
        if (requestParser.getParameter(ACTION_PW).isSet()) {
            // normal password change
            request.setAttribute(ACTION_NAME,ACTION_PW);
        }
        else if (requestParser.getParameter(ACTION_US).isSet()) {
            // userswitch
            request.setAttribute(ACTION_NAME,ACTION_US);
        }
        else if (requestParser.getParameter(ACTION_EP).isSet()) {
            // expired password
            request.setAttribute(ACTION_NAME, ACTION_EP);
        }


        // if password equals passwordverify the passord of the user is changed
        if ( (password.equals(passwordVerify)) && 
                (password.length()>0) &&
                (oldpassword.equals(user.getPassword())) ) {
            
            // The password was changed successfully
            forwardTo = "success";

            if (requestParser.getParameter(ACTION_PW).isSet()) {
                // password change
                // ===============
                // In the case if UME logon functionality is enabled, the password
                // must be changed only in the UME user storage. Because for the 
                // logon area UME provides its own application, the request for password
                // change can't come from the login area (normally this can only
                // come from the "my data" area).
                // Therefore we don't need to attend this for userswitch (US) and
                // expired password (EP) cases (see below).
                if(UserActions.isUmeLogonEnabled(request, 
                                                 userSessionData, 
                                                 (UserBase)user)) {
                                                        
                    if(!UmeWrapper.setUMEUserPassword((UserBase)user, 
                                                      user.getUserId(), 
                                                      password)) {
						log.exiting();                           
                        return mapping.findForward("failure");
                    }                    
                }
                else {
                    if(!user.changePassword(password, passwordVerify)) {
						log.exiting();
                        return mapping.findForward("failure");
                    }
                }
            }
            if (requestParser.getParameter(ACTION_US).isSet()) {
                // userswitch
                // ==========
                if (user.createNewInternetUser(password) != LoginStatus.OK) {
					log.exiting();
                    return mapping.findForward("failure");
                }
            }
            if (requestParser.getParameter(ACTION_EP).isSet()) {
                // expired password
                // change the old password to the new one
                if (!user.changeExpiredPassword(oldpassword, password)) {
					log.exiting();
                    return mapping.findForward("failure");
                }
            }

			BusinessPartner partner = buPaMa.createBusinessPartner(user.getBusinessPartner(),
																   null,
																   null );

			Contact contact = new Contact();
			partner.addPartnerFunction(contact);
			buPaMa.setDefaultBusinessPartner(partner.getTechKey(),contact.getName());

            user.addMessage(new Message(Message.INFO,"b2b.login.pwchange.confirm",null,""));

            if (forwardParameter.isSet()) {
                forwardTo=forwardParameter.getValue().getString();
            }
        }
        else {
            // bring the appropriate message
            if (password.length()==0) {
                user.addMessage(new Message(Message.ERROR,"b2b.login.pwchange.noentry",null,""));
            }
            else if (!password.equals(passwordVerify)) {
                user.addMessage(new Message(Message.ERROR,"b2b.login.pwchange.error",null,""));
            }
            else if (!oldpassword.equals(user.getPassword())) {
                user.addMessage(new Message(Message.ERROR,"b2b.login.pwchange.oldpwd.error",null,""));	
            }
            else {
                user.addMessage(new Message(Message.ERROR,"b2b.login.pwchange.nopwchange",null,""));	
            }

            forwardTo="failure";
        }
		log.exiting();
        return mapping.findForward(forwardTo);
    }
}
