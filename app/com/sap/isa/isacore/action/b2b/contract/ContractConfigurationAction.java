/*****************************************************************************
    Copyright (c) 2000, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      8 July 2001

    $Revision: #10 $
    $Date: 2001/08/03 $
*****************************************************************************/
package com.sap.isa.isacore.action.b2b.contract;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.contract.Contract;
import com.sap.isa.businessobject.contract.ContractHeader;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.interaction.InteractionConfig;
import com.sap.isa.core.interaction.InteractionConfigContainer;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.ipc.ui.jsp.action.ActionForwardConstants;
import com.sap.isa.ipc.ui.jsp.action.InternalRequestParameterConstants;
import com.sap.isa.ipc.ui.jsp.action.RequestParameterConstants;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.OriginalRequestParameterConstants;
import com.sap.spc.remote.client.object.imp.IPCClientObjectFactory;
import com.sap.spc.remote.client.object.imp.rfc.RFCIPCItemReference;


/**
 * Starts configuration for a contract item.
 */
public class ContractConfigurationAction extends IsaCoreBaseAction {
    private static final String FORWARD_START_CONFIGURATION = "contractStartConfiguration";

    /**
     * Attribute containing the item to be configured.
     */
    public static final String CONTRACT_CONFIGURE_ITEM_KEY = "contractConfigureItemKey";
	
	/**
	 * Attribute containing the configuration filter of the item
	 */   
	public static final String CONTRACT_CONFIG_FILTER_ITEM = "contractConfigFilter";
	
    /**
     * Overriden <em>doPerform</em> method of <em>BaseAction</em>.
     */
    public ActionForward isaPerform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response, UserSessionData userSessionData, RequestParser requestParser, BusinessObjectManager bom, IsaLocation log) throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        Contract                   contract = null;

        // get configuration information from the Contract business object
        contract = bom.getContract();

        if (contract == null) {
            // no contract? => exit
            log.exiting();
            return mapping.findForward(ActionForwardConstants.FORWARD_CLOSE_CONFIGURATION);
        }

        // get key of item to be configured
        String contractConfigureItemKeyValue = "";
        if (userSessionData.getAttribute(CONTRACT_CONFIGURE_ITEM_KEY) != null) {
			contractConfigureItemKeyValue = userSessionData.getAttribute(CONTRACT_CONFIGURE_ITEM_KEY).toString();
        }
		if (contractConfigureItemKeyValue.length() == 0) {
			// no item? => exit
			 log.warn(LogUtil.APPS_USER_INTERFACE, "b2b.contract.configuration.nullItemKey");
			 log.exiting();
			 return mapping.findForward(ActionForwardConstants.FORWARD_CLOSE_CONFIGURATION);			
		}
		
		// get configuration filter
		String configFilter = "";
		if (userSessionData.getAttribute(CONTRACT_CONFIG_FILTER_ITEM) != null) {
			configFilter = userSessionData.getAttribute(CONTRACT_CONFIG_FILTER_ITEM).toString();
		}
        // put caller (post-configuration forward) into request context
        request.setAttribute(RequestParameterConstants.CALLER, RequestParameterConstants.CALLER_CLOSE_CONFIGURATION);

        // put ipc item reference into request context
        IPCItem           ipcItem = null;
		RFCIPCItemReference  ipcItemReference = null;
        
		ipcItemReference = (RFCIPCItemReference)IPCClientObjectFactory.getInstance().newIPCItemReference();
		
		// try to get it from contract
		ContractHeader selectedContract = contract.getSelectedContract();
		if (selectedContract == null) {
			log.exiting();
			throw new PanicException("b2b.contract.message.notFound");
		}
				
        if (selectedContract.getIpcDocumentId() != null) {
			ipcItemReference.setDocumentId(selectedContract.getIpcDocumentId().toString());
        }
        
		if (selectedContract.getIpcConnectionKey() != null) {
			ipcItemReference.setConnectionKey(selectedContract.getIpcConnectionKey());
		}
	    ipcItemReference.setItemId(contractConfigureItemKeyValue);

        if (ipcItemReference == null) {
            // no IPC item? => exit
            log.warn(LogUtil.APPS_USER_INTERFACE, "b2b.contract.configuration.nullIpcItem");
			log.exiting();
            return mapping.findForward(ActionForwardConstants.FORWARD_CLOSE_CONFIGURATION);
        }

        request.setAttribute(RequestParameterConstants.IPC_ITEM_REFERENCE, ipcItemReference);


        if (log.isDebugEnabled()) {
            log.debug("DocumentId: " + ipcItemReference.getDocumentId());
            log.debug("ItemId: " + ipcItemReference.getItemId());
            log.debug("configItemReference: " + ipcItemReference);
        }

        // put contract filter into request context (if existing)
        String contractFilter = "";

        if ((contractFilter != null) && (contractFilter.length() > 0) && !stringIsFilledWith(contractFilter, '0')) {
            request.setAttribute("restrictId", contractFilter);
            request.setAttribute("ownerType", "CONTRACT");
            request.setAttribute("repositoryType", "IBASE");

            if (contract.getTechKey() != null) {
                request.setAttribute("ownerId", contract.getTechKey().getIdAsString());
            }
        }

        // put display only mode into request context
        request.setAttribute(RequestParameterConstants.DISPLAY_MODE, RequestParameterConstants.T);
        request.setAttribute(RequestParameterConstants.ENABLE_VARIANT_SEARCH, "F");

        // put caller message into request context
        ServletContext context = super.getServlet().getServletContext();
        HttpSession    session = request.getSession();

        // message "configuration can only be changed in basket"
        String description = WebUtil.translate(context, session, "b2b.contract.message.confDOnly", null);
        request.setAttribute("caller_message", description);

		// set scenario
		request.setAttribute(OriginalRequestParameterConstants.IPC_SCENARIO, "ISA");
		request.setAttribute(OriginalRequestParameterConstants.DISPLAY_MODE, RequestParameterConstants.T);
		
		InteractionConfigContainer interactionConfigContainer = getInteractionConfig(request);
		InteractionConfig          interactionConfig = interactionConfigContainer.getConfig("ipc_scenario");

		if (interactionConfig != null) {
			String ipcScenario = interactionConfig.getValue("scenario.basket");

			if (ipcScenario == null) {
				log.warn(LogUtil.APPS_COMMON_CONFIGURATION, "XCM Scenario " + "scenario.basket" + " for IPC Product Configuration not found");
			}

			request.setAttribute(InternalRequestParameterConstants.IPC_XCM_SCENARIO, ipcScenario);
		}
		else {
			log.warn(LogUtil.APPS_COMMON_CONFIGURATION, "Configuration for ipc_scenario not found in interaction-config.xml");
		}	
		request.setAttribute(RequestParameterConstants.IPC_ITEM_REFERENCE, ipcItemReference);


		log.exiting();
        // call configuration UI
        return mapping.findForward(FORWARD_START_CONFIGURATION);
    }
}