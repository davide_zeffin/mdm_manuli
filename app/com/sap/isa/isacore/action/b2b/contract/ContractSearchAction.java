/*****************************************************************************
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      27 March 2001

    $Revision: #5 $
    $Date: 2001/07/15 $
*****************************************************************************/
package com.sap.isa.isacore.action.b2b.contract;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.businessobject.contract.Contract;
import com.sap.isa.businessobject.contract.ContractHeaderList;
import com.sap.isa.businessobject.contract.ContractSearchCriteria;
import com.sap.isa.businessobject.order.DocumentListFilter;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.FormattedDate;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.actionform.b2b.contract.ContractSearchForm;

/**
 * Select contracts corresponding to given criteria.
 */
public class ContractSearchAction extends IsaCoreBaseAction {

    /**
     * Name of the contract header list stored in the request context.
     */
    public static final String CONTRACT_HEADER_LIST = "contractHeaderList";

    /**
     * Name of the contract select button passed in the request context.
     */
    public static final String CONTRACT_SEARCH_BUTTON = "contractSearchButton";

    /**
     * Name of the contract select attribute stored in the request context.
     */
    public static final String CONTRACT_SELECTED = "contractSelected";

    /**
     * Name of the id of the selected contract stored in the request context.
     */
    public static final String CONTRACT_SELECTED_ID = "contractSelectedId";

    /**
     * Name of the document type attribute stored in the request context.
     */
    public final static String DOCUMENT_TYPE = "documentType";

    /**
     * Constants for document types in the request context.
     */
    public final static String CONTRACT = "contract";
    public static final String ORDER =
        DocumentListFilter.SALESDOCUMENT_TYPE_ORDER;
    public static final String ORDERTEMPLATE =
        DocumentListFilter.SALESDOCUMENT_TYPE_ORDERTEMPLATE;
    public static final String QUOTATION =
        DocumentListFilter.SALESDOCUMENT_TYPE_QUOTATION;
    public static final String INVOICE =
        DocumentListFilter.BILLINGDOCUMENT_TYPE_INVOICE;
    public static final String DOWNPAYMENT =
        DocumentListFilter.BILLINGDOCUMENT_TYPE_DOWNPAYMENT;
    public static final String CREDITMEMO =
        DocumentListFilter.BILLINGDOCUMENT_TYPE_CREDITMEMO;
    public static final String AUCTION =
        DocumentListFilter.DOCUMENT_TYPE_AUCTION;

    private static final String FORWARD_CONTRACT_SEARCH = "contractsearch";
    private static final String FORWARD_DOCUMENT_SEARCH = "documentsearch";
    private static final String FORWARD_BILLING_SEARCH = "billingsearch";
    private static final String FORWARD_AUCTION_SEARCH = "auctionsearch";

    /**
     * Overriden <em>isaPerform</em> method of <em>IsaCoreBaseAction</em>.
     * This method is called by the action servlet after the form data
     * was evaluated and stored in the ContractSearchForm bean.
     *
     * @see ContractSearchForm ContractSearchForm bean
     */
    public ActionForward isaPerform(
                ActionMapping mapping,
                ActionForm form,
                HttpServletRequest request,
                HttpServletResponse response,
                UserSessionData userSessionData,
                RequestParser requestParser,
                BusinessObjectManager bom,
                IsaLocation log)
                throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        Shop shop = null;
        String docType = null;
        User user = null;
		DocumentHandler documentHandler = (DocumentHandler)
				userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);

        // get / check the form
        if (!(form instanceof ContractSearchForm)) {
			log.exiting();
            throw new PanicException("b2b.contract.message.noActForm");
        }
        ContractSearchForm contractSearchForm = (ContractSearchForm) form;

        // other doc type requested?
        docType = request.getParameter(DOCUMENT_TYPE);
        if (docType != null && !docType.equalsIgnoreCase(CONTRACT)) {
            if (docType.equalsIgnoreCase(INVOICE) ||
                docType.equalsIgnoreCase(DOWNPAYMENT) ||
                docType.equalsIgnoreCase(CREDITMEMO)) {
				log.exiting();
                return mapping.findForward(FORWARD_BILLING_SEARCH);
            }
            else if (docType.equalsIgnoreCase(AUCTION)) {
				log.exiting();
                return mapping.findForward(FORWARD_AUCTION_SEARCH);
            }
            else {
				log.exiting();
                return mapping.findForward(FORWARD_DOCUMENT_SEARCH);
            }
        }

        // read the shop data
        shop = bom.getShop();
        if (shop == null) {
			log.exiting();
            throw new PanicException("shop.notFound");
        }

        // are contracts allowed?
        if (!shop.isContractAllowed()) {
			log.exiting();
            return mapping.findForward(FORWARD_DOCUMENT_SEARCH);
        }

        // get / create the Contract business object
        Contract contract = bom.getContract();
        if (contract == null) {
            contract = bom.createContract();
        }

        // is there any selection requested ?
        if ((request.getParameter(CONTRACT_SEARCH_BUTTON) != null) || 
            (contractSearchForm.isSelectRequested())) {
            contractSearchForm.setSelectRequested(true);

            // get user
            user = bom.getUser();
            if (user == null) {
				log.exiting();
                return mapping.findForward("login");
            }
            ContractHeaderList headerList;
			BusinessPartnerManager buPaMa = bom.createBUPAManager();
			BusinessPartner partner = buPaMa.getDefaultBusinessPartner(PartnerFunctionData.SOLDTO);

            if (shop.isContractNegotiationAllowed()) {
            // use the Search business object to select contract headers
                headerList =
                    contract.readHeaders(
				    partner.getTechKey(),
                    shop,
                    getSearchCriteria(contractSearchForm));
                // might throw communicationException
            } 
            else {
                //since no contract negotiation are allowed, select only contracts in status released
                ContractSearchCriteria contractSearchCriteria = getSearchCriteria(contractSearchForm);
                contractSearchCriteria.setStatusReleased();
                headerList =
                    contract.readHeaders(
				    partner.getTechKey(),
                    shop,
                    contractSearchCriteria);
            }
            

            // pass values to the JSP via request context
            request.setAttribute(CONTRACT_HEADER_LIST, headerList);
        }
        else {
            contractSearchForm.setSelectRequested(false);
        }
		documentHandler.setActualOrganizer("contractsearch");
		log.exiting();
        return mapping.findForward(FORWARD_CONTRACT_SEARCH);
    }

    // extract selection criteria from the selection form
    private ContractSearchCriteria getSearchCriteria(
            ContractSearchForm form) {
        String validFrom = "";
        String validTo = "";
        FormattedDate date = null;

        // extract dates
        date = new FormattedDate();
        validFrom = date.getFormattedDate(FormattedDate.Format.YYYYMMDD);
        if (form.isValiditySelection(ContractSearchForm.PERIOD)) {
            if (form.isPeriod(ContractSearchForm.NEXT_WEEK)) {
                date.addDays(7);
            }
            else if (form.isPeriod(ContractSearchForm.NEXT_MONTH)) {
                date.addDays(30);
            }
            else if (form.isPeriod(ContractSearchForm.NEXT_YEAR)) {
                date.addYears(1);
            }
        }
        else {

            // get end of month date
            date.setDay(1);
            date.setMonth(Integer.parseInt(form.getMonth()));
            date.setYear(Integer.parseInt(form.getYear()));
            date.addMonths(1);
            date.addDays(-1);
        }
        validTo = date.getFormattedDate(FormattedDate.Format.YYYYMMDD);

        // get/create search criteria (resides in page context)
        return new ContractSearchCriteria(
                    form.getId(),
                    form.getExternalRefNo(),
                    form.getDescription(),
                    validFrom,
                    validTo,
                    form.getStatus() );
    }
}