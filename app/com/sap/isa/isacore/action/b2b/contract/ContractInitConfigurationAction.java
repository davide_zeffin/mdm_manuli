/*****************************************************************************
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      18 August 2001

    $Revision: #0 $
    $Date: 2001/08/18 $
*****************************************************************************/
package com.sap.isa.isacore.action.b2b.contract;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 * Action for initializing the configuration
 * Creates a managed document for the configuration ui & forwards to the
 * <code>ContractConfigurationAction</code> via the
 * <code>UpdateDocumentViewAction</code>.
 *
 * @see com.sap.isa.isacore.action.b2b.contract.ContractConfigurationAction ContractConfigurationAction
 * @see com.sap.isa.isacore.action.b2b.contract.ContractConfigurationAction ContractConfigurationAction
 */
public class ContractInitConfigurationAction extends IsaCoreBaseAction {

    private static final String FORWARD_SUCCESS = "success";
	
    /**
     * Overriden <em>isaPerform</em> method of <em>IsaCoreBaseAction</em>.
     */
    public ActionForward isaPerform(ActionMapping mapping,
                ActionForm form,
                HttpServletRequest request,
                HttpServletResponse response,
                UserSessionData userSessionData,
                RequestParser parser,
                BusinessObjectManager bom,
                IsaLocation log) throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        // get contract document on top from document handler
        DocumentHandler documentHandler = (DocumentHandler)
        userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
        if (documentHandler == null) {
			log.exiting();
           throw new PanicException("No document handler found");
        }

        // get a copy of it that only differs by the forward
        ManagedDocument onTopDoc = documentHandler.getManagedDocumentOnTop();
        if (onTopDoc == null) {
			log.exiting();
           throw new PanicException("No document found on top");
        }
        ManagedDocument configuratorDoc =
            new ManagedDocument(
                onTopDoc.getDocument(),
                onTopDoc.getDocType(),
                onTopDoc.getDocNumber(),
                onTopDoc.getRefNumber(),
                onTopDoc.getRefName(),
                onTopDoc.getDate(),
                "contractconfiguration",
                onTopDoc.getHref(),
                onTopDoc.getHrefParameter());

        // document handler enshures that configuration document stays on top
        // during refresh - the previous contract document will be replaced
        documentHandler.add(configuratorDoc);
        documentHandler.setOnTop(configuratorDoc.getDocument());

        // we have to put the itemKey into the sesseion context
        String itemKey = "";
        if (parser.getAttribute(
                ContractConfigurationAction.CONTRACT_CONFIGURE_ITEM_KEY).
                isSet()) {
            itemKey = parser.getAttribute(
                ContractConfigurationAction.CONTRACT_CONFIGURE_ITEM_KEY).
                getValue().getString();
        }
        else {
            itemKey = parser.getParameter(
                ContractConfigurationAction.CONTRACT_CONFIGURE_ITEM_KEY).
                getValue().getString();
        }
        userSessionData.setAttribute(
            ContractConfigurationAction.CONTRACT_CONFIGURE_ITEM_KEY, itemKey);
		log.exiting();
        return mapping.findForward(FORWARD_SUCCESS);
    }
}