/*****************************************************************************
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      29 March 2001

    $Revision: #2 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.isacore.action.b2b.contract;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.contract.Contract;
import com.sap.isa.businessobject.contract.ContractAttributeList;
import com.sap.isa.businessobject.contract.ContractAttributeValueList;
import com.sap.isa.businessobject.contract.ContractItem;
import com.sap.isa.businessobject.contract.ContractItemList;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 * Displays detail data for a contract item for release order.
 */
public class DetailContractReadAction extends IsaCoreBaseAction {

    /**
     * Name of the contract key attribute stored in the request context.
     */
    public static final String DETAIL_CONTRACT_KEY = "detailContractKey";

    /**
     * Name of the contract item key attribute stored in the request context.
     */
    public static final String DETAIL_CONTRACT_ITEM_KEY =
            "detailContractItemKey";

    /**
     * Name of the contract item in the request context.
     */
    public static final String DETAIL_CONTRACT_ITEM =
            "detailContractItem";

    /**
     * Name of the contract in the request context.
     */
    public static final String DETAIL_CONTRACT_ID =
            "detailContractId";

    /**
     * Name of the list of contract attributes in the session context.
     */
    public static final String DETAIL_CONTRACT_ATTRIBUTE_LIST =
            "detailContractAttributes";

    /**
     * Name of the list of contract attribute values in the request context.
     */
    public static final String DETAIL_CONTRACT_ATTRIBUTE_VALUE_LIST =
            "detailContractAttributeValues";

    private static final String FORWARD_DETAIL_CONTRACT_READ =
            "detailcontractread";
	
    /**
     * Overriden <em>doPerform</em> method of <em>BaseAction</em>.
     */
    public ActionForward isaPerform(
                ActionMapping mapping,
                ActionForm form,
                HttpServletRequest request,
                HttpServletResponse response,
                UserSessionData userSessionData,
                RequestParser requestParser,
                BusinessObjectManager bom,
                IsaLocation log)
                throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        ContractItem contractItem = null;
        ContractItemList contractItemList = null;
        ContractAttributeList contractAttributeList = null;
        ContractAttributeValueList contractAttributeValueList = null;
        TechKey contractKey = null;
        TechKey contractItemKey = null;

        // get / create the Contract business object
        Contract contract = bom.getContract();
        if (contract == null) {
            contract = bom.createContract();
        }

        // get contract key from form parameter
        RequestParser.Value contractDetailKeyValue = requestParser.
            getParameter(DETAIL_CONTRACT_KEY).getValue();
        if (contractDetailKeyValue.isSet() &&
            !(contractDetailKeyValue.getString().equalsIgnoreCase("") ||
              contractDetailKeyValue.getString().equalsIgnoreCase("null")) ) {
            contractKey = new TechKey(contractDetailKeyValue.getString());
        }
        RequestParser.Value contractDetailItemKeyValue = requestParser.
            getParameter(DETAIL_CONTRACT_ITEM_KEY).
                getValue();
        if (contractDetailItemKeyValue.isSet() &&
            !(contractDetailItemKeyValue.getString().equalsIgnoreCase("") ||
              contractDetailItemKeyValue.getString().equalsIgnoreCase("null"))
            ) {
            contractItemKey = new TechKey(contractDetailItemKeyValue.getString());
        }

        // read the fantastic shop data
        Shop shop = bom.getShop();
        if (shop == null) {
        	log.exiting();
            throw new PanicException("shop.notFound");
        }

        // use the Contract business object to read contract items
        if (contractKey != null && contractItemKey != null) {
            contractItemList = contract.readItem(
                    contractKey,
                    contractItemKey,
                    shop);
                    // might throw communicationException
        }
        else {
            contractItemList = new ContractItemList();
        }

        // get item
        if (!contractItemList.isEmpty()) {
            contractItem = contractItemList.get(0);
            contractAttributeList = contractItemList.getAttributes();
            contractAttributeValueList = contractItemList.getAttributeValues().
                get(contractItem.getContractItemTechKey().getItemKey());
        }
        else {
            contractItem = new ContractItem();
            contractAttributeList = new ContractAttributeList();
            contractAttributeValueList = new ContractAttributeValueList();
        }

        // pass values to the JSP via request context
        request.setAttribute(DETAIL_CONTRACT_ITEM, contractItem);
        request.setAttribute(
            DETAIL_CONTRACT_ATTRIBUTE_LIST,
            contractAttributeList);
        request.setAttribute(
            DETAIL_CONTRACT_ATTRIBUTE_VALUE_LIST,
            contractAttributeValueList);
		log.exiting();
        return mapping.findForward(FORWARD_DETAIL_CONTRACT_READ);
    }
}