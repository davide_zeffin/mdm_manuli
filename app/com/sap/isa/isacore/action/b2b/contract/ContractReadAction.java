/*****************************************************************************
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      29 March 2001

    $Revision: #7 $
    $Date: 2001/08/01 $
*****************************************************************************/
package com.sap.isa.isacore.action.b2b.contract;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.backend.boi.webcatalog.pricing.PriceInfo;
import com.sap.isa.backend.boi.webcatalog.pricing.Prices;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.contract.Contract;
import com.sap.isa.businessobject.contract.ContractHeader;
import com.sap.isa.businessobject.contract.ContractItem;
import com.sap.isa.businessobject.contract.ContractItemList;
import com.sap.isa.businessobject.contract.ContractItemPrice;
import com.sap.isa.businessobject.contract.ContractItemPriceConfigMap;
import com.sap.isa.businessobject.contract.ContractItemPriceRequest;
import com.sap.isa.businessobject.contract.ContractItemQuantityMap;
import com.sap.isa.businessobject.contract.TechKeySet;
import com.sap.isa.businessobject.webcatalog.pricing.PriceCalculator;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 * Displays a contract for release order.
 */
public class ContractReadAction extends IsaCoreBaseAction {

    /**
     * Name of the contract key attribute stored in the request context.
     */
    public static final String CONTRACT_KEY = "contractKey";

    /**
     * Name of the list of items passed in the request context.
     */
    public static final String CONTRACT_ITEM_LIST = "contractItemList";

    /**
     * Name of the set of expanded items passed in the request context.
     */
    public static final String CONTRACT_EXPANDED_ITEM_SET = "expandedItemSet";

    /**
     * Name of the current page attribute passed in the request context.
     */
    public static final String CONTRACT_CURRENT_PAGE_NO =
            "contractCurrentPageNo";

    /**
     * Name of the current page attribute passed in the request context.
     */
    public static final String CONTRACT_PAGE_SIZE = "contractPageSize";

    /**
     * Constant for default page size.
     */
    public static final int DEFAULT_PAGE_SIZE = Contract.DEFAULT_PAGE_SIZE;

    /**
     * Name of the list of item attribute value map passed in the request context.
     */
    public static final String CONTRACT_ATTRIBUTE_VALUE_MAP =
            "contractAttributeValueMap";

    /**
     * Name of the list of item attribute value list in the request context.
     */
    public static final String CONTRACT_ATTRIBUTE_VALUE_LIST =
            "contractAttributeValueList";

    /**
     * Name of the last page attribute passed in the request context.
     */
    public static final String CONTRACT_LAST_PAGE_NO = "contractLastPageNo";

    /**
     * Name of the first page command passed in the request context.
     */
    public static final String CONTRACT_FIRST_PAGE = "contractFirstPage";

    /**
     * Name of the previous page command passed in the request context.
     */
    public static final String CONTRACT_PREV_PAGE = "contractPrevPage";

    /**
     * Name of the next page command passed in the request context.
     */
    public static final String CONTRACT_NEXT_PAGE = "contractNextPage";

    /**
     * Name of the last page command passed in the request context.
     */
    public static final String CONTRACT_LAST_PAGE = "contractLastPage";

    /**
     * Name of the update button passed in the request context.
     */
    public static final String CONTRACT_UPDATE_BUTTON = "contractUpdateButton";

    /**
     * Name of the page command field passed in the request context.
     */
    public static final String CONTRACT_PAGE_COMMAND = "contractPageCommand";

    /**
     * Name of the item to collapse passed in the request context.
     */
    public static final String CONTRACT_COLLAPSE_ITEM = "contractCollapseItem";

    /**
     * Name of the item to expand passed in the request context.
     */
    public static final String CONTRACT_EXPAND_ITEM = "contractExpandItem";

    /**
     * Name of the item to expand passed in the request context.
     */
    public static final String CONTRACT_RELEASE_ITEM = "contractReleaseItem";

    /**
     * Name of the item to configurate passed in the request context.
     */
    public static final String CONTRACT_CONFIGURATE_ITEM =
            "contractConfigurateItem";

    /**
     * Name of the release command passed in the request context.
     */
    public static final String CONTRACT_RELEASE_COMMAND =
        "contractReleaseCommand";

    /**
     * Command to create a release order from the contract.
     */
    public static final String CONTRACT_RELEASE = "contractRelease";

    /**
     * Command to close the contract.
     */
    public static final String CONTRACT_CLOSE = "contractClose";

    private static final String FORWARD_CONTRACT_READ = "contractread";
    private static final String FORWARD_RELEASE_ORDER = "contractreleaseorder";
    private static final String FORWARD_CONTRACT_NOT_FOUND = "contractnotfound";
    private static final List EMPTY_PRICES = new ArrayList(0);

    /**
     * Overriden <em>doPerform</em> method of <em>BaseAction</em>.
     */
    public ActionForward isaPerform(
                ActionMapping mapping,
                ActionForm form,
                HttpServletRequest request,
                HttpServletResponse response,
                UserSessionData userSessionData,
                RequestParser requestParser,
                BusinessObjectManager bom,
                IsaLocation log)
                throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
		ContractItemList contractItemList = null;
        TechKey contractKey = null;
        String forward = null;
        RequestParser.Parameter releaseCommand = null;
        Set releasedItemsSet = null;

        // get shop data
        Shop shop = bom.getShop();
        if (shop == null) {
        	log.exiting();
            throw new PanicException("shop.notFound");
        }

        // are contracts allowed?
        if (!shop.isContractAllowed()) {
			log.exiting();
            return mapping.findForward(FORWARD_CONTRACT_NOT_FOUND);
        }

        // get / create the Contract business object
        Contract contract = bom.getContract();
        if (contract == null) {
            contract = bom.createContract();
        }

        // get contract key from form parameter
        RequestParser.Value contractSelectedValue = requestParser.
            getParameter(ContractSelectAction.CONTRACT_SELECTED).getValue();
        if (contractSelectedValue.isSet() &&
            !(contractSelectedValue.getString().equalsIgnoreCase("") ||
              contractSelectedValue.getString().equalsIgnoreCase("null")) ) {
            contractKey = new TechKey(contractSelectedValue.getString());
        }
        else {

            // try to get it from contract
            ContractHeader selectedContract = contract.getSelectedContract();
            if (selectedContract != null) {
                contractKey = selectedContract.getTechKey();
            }
            else {
				log.exiting();
                throw new PanicException("b2b.contract.message.notFound");
            }
        }

        // get expanded items from the request
        TechKeySet expandedItemSet =
            new ExpandedItems(requestParser).getExpandedItems(contract);

        // get release quantities from the request except already released ones
        // & store them on contract level
        releasedItemsSet = (Set)request.getAttribute(
                ContractReleaseOrderAction.CONTRACT_ITEMS_RELEASED);
        if (releasedItemsSet == null) {
            releasedItemsSet = (Set) new HashSet();
        }
        ContractItemQuantityMap releaseQuantities =
            new ReleaseQuantities(requestParser).getReleaseQuantities(
            releasedItemsSet);

        // use the Contract business object to read contract items
        try {
            contractItemList = contract.readItems(
                    contractKey,
                    shop,
                    getPageNumber(contract, requestParser),
                    getPageSize(contract, requestParser),
                    getVisibleItem(requestParser),
                    expandedItemSet,
                    releaseQuantities);    // might throw communicationException
        }

        // if called with an unknown key, gently redirect to the contract
        // selection
        catch (Contract.UnknownKeyException unknownKeyException) {
            DocumentHandler documentHandler = (DocumentHandler)
                userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
            ContractHeader selectedContract = contract.getSelectedContract();
            if (selectedContract != null) {
                documentHandler.release(selectedContract);
            }
			log.exiting();
            return mapping.findForward(FORWARD_CONTRACT_NOT_FOUND);
        }

        // pass values to the JSP via request context
        request.setAttribute(CONTRACT_ITEM_LIST, contractItemList);
        request.setAttribute(CONTRACT_EXPANDED_ITEM_SET, expandedItemSet);
        request.setAttribute(CONTRACT_ATTRIBUTE_VALUE_MAP,
            contractItemList.getAttributeValues());

        // determine forward
        forward = FORWARD_CONTRACT_READ;

        // release order?
        releaseCommand = requestParser.getParameter(CONTRACT_RELEASE_COMMAND);
        if (releaseCommand.isSet()) {
            if (releaseCommand.getValue().getString().
                equalsIgnoreCase(CONTRACT_RELEASE)) {

                // enshure no release has happened in the same request (loops!)
                if (!requestParser.getAttribute(
                        ContractReleaseOrderAction.CONTRACT_ITEMS_RELEASED).
                        isSet()) {
                    forward = FORWARD_RELEASE_ORDER;
                }
            }
        }
		log.exiting();
        return mapping.findForward(forward);
    }



    // determine the page to display from request attributes
    private int getPageNumber(Contract contract, RequestParser requestParser) {
        RequestParser.Parameter itemPageParameter;

        // get current / last page
        int currentPage = contract.getPageNumber();
        itemPageParameter = requestParser.getParameter(CONTRACT_CURRENT_PAGE_NO);
        if (itemPageParameter.isSet()) {
            currentPage = itemPageParameter.getValue().getInt();
        }
        int lastPage = contract.getLastPageNumber();
        itemPageParameter = requestParser.getParameter(CONTRACT_LAST_PAGE_NO);
        if (itemPageParameter.isSet()) {
            lastPage = itemPageParameter.getValue().getInt();
        }

        // react to paging requests
        if (requestParser.getParameter(CONTRACT_PAGE_COMMAND).isSet()) {
            String command= requestParser.getParameter(CONTRACT_PAGE_COMMAND).
                    getValue().getString();
            if (command.equalsIgnoreCase(CONTRACT_FIRST_PAGE)) {
                return 1;
            }
            if (command.equalsIgnoreCase(CONTRACT_PREV_PAGE)) {
                return currentPage > 1 ? --currentPage :  1;
            }
            if (command.equalsIgnoreCase(CONTRACT_NEXT_PAGE)) {
                return currentPage < lastPage ? ++currentPage :  lastPage;
            }
            if (command.equalsIgnoreCase(CONTRACT_LAST_PAGE)) {
                return lastPage;
            }
        }
        return currentPage;
    }

    // determine the page size to display from request attributes
    private int getPageSize(Contract contract, RequestParser requestParser) {
        RequestParser.Parameter pageSizeParameter =
            requestParser.getParameter(CONTRACT_PAGE_SIZE);
        if (pageSizeParameter.isSet()) {
            return pageSizeParameter.getValue().getInt();
        }
        else {
            return contract.getPageSize();
        }
    }

    // if expand/collapse is requested expanded/collapsed item should be visible
    private TechKey getVisibleItem(RequestParser requestParser) {

        // on collapse, previosly visible item should stay visible
        RequestParser.Parameter collapseItem =
            requestParser.getParameter(
                ContractReadAction.CONTRACT_COLLAPSE_ITEM);
        if (collapseItem.isSet()) {
            return new TechKey(collapseItem.getValue().getString());
        }
        return TechKey.EMPTY_KEY;
    }
}
