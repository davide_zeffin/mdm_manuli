/*****************************************************************************
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      15 May 2001

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.isacore.action.b2b.contract;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.backend.boi.isacore.contract.ContractView;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.contract.Contract;
import com.sap.isa.businessobject.contract.ContractAttributeList;
import com.sap.isa.businessobject.contract.ContractHeader;
import com.sap.isa.businessobject.contract.ContractHeaderList;
import com.sap.isa.businessobject.contract.ContractSearchCriteria;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 * Selects a contract for release order.
 */
public class ContractSelectAction extends IsaCoreBaseAction {

    /**
     * Name of the contract select attribute stored in the request context.
     */
    public static final String CONTRACT_SELECTED = "contractSelected";

    /**
     * Name of the list of contract attributes in the session context.
     */
    public static final String CONTRACT_ATTRIBUTE_LIST =
            "contractAttributeList";

    private static final String FORWARD_CONTRACT_READ = "contractread";
    private static final String FORWARD_CONTRACT = "contract";
    private static final String FORWARD_CONTRACT_NOT_FOUND = "contractnotfound";

    /**
     * Overriden <em>isaPerform</em> method of <em>IsaCoreBaseAction</em>.
     */
    public ActionForward isaPerform(
                ActionMapping mapping,
                ActionForm form,
                HttpServletRequest request,
                HttpServletResponse response,
                UserSessionData userSessionData,
                RequestParser requestParser,
                BusinessObjectManager bom,
                IsaLocation log)
                throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        Contract contract = null;
        ContractAttributeList contractAttributeList = null;
        ContractHeader contractHeader = null;
        DocumentHandler documentHandler = (DocumentHandler)
                userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
        RequestParser.Parameter contractSelected = null;
        String contractSelectedValue;
        TechKey contractKey = null;

        // read the shop data...
        Shop shop = bom.getShop();
        if (shop == null) {
			log.exiting();
            throw new PanicException("shop.notFound");
        }

        // are contracts allowed?
        if (!shop.isContractAllowed()) {
			log.exiting();
            return mapping.findForward(FORWARD_CONTRACT_NOT_FOUND);
        }

        // get / create the Contract business object
        contract = bom.getContract();
        if (contract == null) {
            contract = bom.createContract();
        }

        // determine selected contract from release context
        contractSelected = requestParser.getParameter(CONTRACT_SELECTED);
        contractSelectedValue = contractSelected.isSet() ?
            contractSelected.getValue().getString() : "";
        contractKey = new TechKey(contractSelectedValue);

        // remove previous contract from document handler
        contractHeader = contract.getSelectedContract();
        if (contractHeader != null &&
            !contractKey.equals(contractHeader.getTechKey())) {
            documentHandler.release(contractHeader);
        }

        // select contract
        try {
            contract.setSelectedContract(contractKey);
        }

        // if called with a stale key - perhaps from history -
        // read all valid contracts from the backend
        catch (Contract.UnknownKeyException outerUnknownKeyException) {

            // get user...
			BusinessPartnerManager buPaMa = bom.createBUPAManager();
			BusinessPartner partner = buPaMa.getDefaultBusinessPartner(PartnerFunctionData.SOLDTO);

			ContractSearchCriteria contractSearchCriteria = new ContractSearchCriteria();
			contractSearchCriteria.setTechKey(contractKey);
			contractSearchCriteria.setStatusReleased();
            // select contract headers with empty criteria...
            ContractHeaderList headerList =
                contract.readHeader(
                    shop,
                    contractSearchCriteria);
                // might throw communicationException

            // ...& try it again
            try {
                contract.setSelectedContract(contractKey);
            }

            // hm. looks like the contract isn't valid anymore
            catch (Contract.UnknownKeyException innerUnknownKeyException) {
				log.exiting();
                return mapping.findForward(FORWARD_CONTRACT_NOT_FOUND);
            }
        }

        // add (new) contract to the document handler
        contractHeader = contract.getSelectedContract();
        String href_action = "b2b/contractselect.do";
        String href_parameter =
            (ContractSelectAction.CONTRACT_SELECTED.concat("=").
                concat(contractHeader.getKeyAsString()));
        ManagedDocument managedDocument = new ManagedDocument(
            contractHeader,
            "contract",  // part of ressource key for b2b.docnav.contract
            contractHeader.getId(),
            contractHeader.getExternalRefNo(),
            contractHeader.getDescription(),
            contractHeader.getValidToDate(),
            FORWARD_CONTRACT,
            href_action,
            href_parameter);
        documentHandler.add(managedDocument);
        documentHandler.setOnTop(contractHeader);
        // needed if the catalog is displayed and a document gets selected from the history
        documentHandler.setCatalogOnTop(false);

        // put contract attributes into the session context
        contractAttributeList = contract.readAttributes(
                shop,
                ContractView.CONTRACT_LIST);
                // might throw communicationException
        request.getSession().
            setAttribute(CONTRACT_ATTRIBUTE_LIST, contractAttributeList);
		log.exiting();
        return mapping.findForward(FORWARD_CONTRACT_READ);
    }
}