/*****************************************************************************
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      2 May 2001

    $Revision: #3 $
    $Date: 2001/08/01 $
*****************************************************************************/
package com.sap.isa.isacore.action.b2b.contract;

import java.util.Iterator;

import com.sap.isa.businessobject.contract.Contract;
import com.sap.isa.businessobject.contract.TechKeySet;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.util.RequestParser;

/*
 * Helper class to extract expanded items from the request context.
 */
class ExpandedItems {
    private static TechKeySet expandedItems;
    private RequestParser requestParser;

    // static initializer
    static {
        expandedItems = new TechKeySet();
    }

    // constructor
    public ExpandedItems(RequestParser requestParser) {
        this.requestParser = requestParser;
    }

    // parse the map of release Quantities out of the request
    public TechKeySet getExpandedItems(Contract contract) {

        // first rebuild map from the request
        expandedItems.clear();
        RequestParser.Parameter expandedItemValues =
            requestParser.getParameter("expandedItems[]");
        if (expandedItemValues.isSet()) {

            // put quantities into techKeySet
            for (int i = 0; i < expandedItemValues.getNumValues(); i++) {
                expandedItems.add(
                    new TechKey(expandedItemValues.getValue(i).getString()));
            }
        }
        else {
            expandedItems.clear();
            Iterator itemKeyIterator = contract.getExpandedItems().iterator();
            while (itemKeyIterator.hasNext()) {
                expandedItems.add((TechKey)itemKeyIterator.next());
            }
        }

        // add expanded item to the set if requested
        RequestParser.Parameter expandItem =
            requestParser.getParameter(ContractReadAction.CONTRACT_EXPAND_ITEM);
        if (expandItem.isSet()) {
            expandedItems.add(new TechKey(expandItem.getValue().getString()));
        }

        // remove collapsed item from the set if requested
        RequestParser.Parameter collapseItem =
            requestParser.getParameter(
                ContractReadAction.CONTRACT_COLLAPSE_ITEM);
        if (collapseItem.isSet()) {
            expandedItems.remove(
                new TechKey(collapseItem.getValue().getString()));
        }
        return expandedItems;
    }
}