/*****************************************************************************
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      15 May 2001

    $Revision: #1 $
    $Date: 2001/06/26 $
*****************************************************************************/
package com.sap.isa.isacore.action.b2b.contract;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.contract.Contract;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 * Displays a contract for release order.
 */
public class ContractCloseAction extends IsaCoreBaseAction {
    private static final String FORWARD_CONTRACT_CLOSED = "contractclosed";
	
    /**
     * Overriden <em>doPerform</em> method of <em>BaseAction</em>.
     */
    public ActionForward isaPerform(
                ActionMapping mapping,
                ActionForm form,
                HttpServletRequest request,
                HttpServletResponse response,
                UserSessionData userSessionData,
                RequestParser requestParser,
                BusinessObjectManager bom,
                IsaLocation log)
                throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        Contract contract = null;
        DocumentHandler documentHandler = (DocumentHandler)
            userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);

        // get / create the Contract business object
        contract = bom.getContract();
        if (contract == null) {

            // no contract? => fine
            log.exiting();
            return mapping.findForward(FORWARD_CONTRACT_CLOSED);
        }

        // remove selected contract
        documentHandler.release(contract.getSelectedContract());
        contract.removeSelectedContract();
        log.exiting();
        return mapping.findForward(FORWARD_CONTRACT_CLOSED);
    }
}