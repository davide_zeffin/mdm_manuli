/*****************************************************************************
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      15 May 2001

    $Revision: #2 $
    $Date: 2001/07/10 $
*****************************************************************************/
package com.sap.isa.isacore.action.b2b.contract;

import com.sap.isa.businessobject.contract.ContractItemTechKey;

/*
 * Helper class to extract contract item techkeys from the request context.
 */
class ContractItemTechKeys {

    /**
     * Splits a concatenated String representation of a
     * <code>ContractItemKey</code> into item and product key
     */
    public static ContractItemTechKey getContractItemKey(String key) {
        int index = key.indexOf('.');
        if (index < 0 || key.lastIndexOf('.',index) > index) {
            throw new IllegalArgumentException();
        }
        return new ContractItemTechKey(key.substring(0,index),
                key.substring(index+1,key.length()));
    }
}
