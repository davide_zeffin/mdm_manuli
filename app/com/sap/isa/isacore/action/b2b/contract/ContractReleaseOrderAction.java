/*****************************************************************************
    Copyright (c) 2000, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      3 May 2001

    $Revision: #6 $
    $Date: 2002/10/17 $
*****************************************************************************/
package com.sap.isa.isacore.action.b2b.contract;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.TreeMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.businessobject.contract.Contract;
import com.sap.isa.businessobject.contract.ContractHeader;
import com.sap.isa.businessobject.contract.ContractItem;
import com.sap.isa.businessobject.contract.ContractItemPriceConfigMap;
import com.sap.isa.businessobject.contract.ContractItemQuantityMap;
import com.sap.isa.businessobject.contract.ContractItemTechKey;
import com.sap.isa.businessobject.item.BasketTransferItemImpl;
import com.sap.isa.businessobject.order.Basket;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.action.order.AddToBasketAction;

/**
 * Displays a contract for release order.
 */
public class ContractReleaseOrderAction extends IsaCoreBaseAction {
    private static final String FORWARD_ADD_TO_DOCUMENT = "addtodocument";
    private static final String FORWARD_CONTRACT_READ = "contractread";

    /**
     * Attribute containing a set of items that have been released in the
     * actual request.
     */
    public static final String CONTRACT_ITEMS_RELEASED =
        "contractItemsReleased";

    
    /**
     * constant used to specify transfer item source
     */
    protected static final String TRANSFER_ITEM_SOURCE = "contract";
    
    
    /**
     * Overriden <em>doPerform</em> method of <em>BaseAction</em>.
     */
    public ActionForward isaPerform(
                ActionMapping mapping,
                ActionForm form,
                HttpServletRequest request,
                HttpServletResponse response,
                UserSessionData userSessionData,
                RequestParser requestParser,
                BusinessObjectManager bom,
                IsaLocation log)
                throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        List transferItems = null;
        Basket basket = null;
        BasketTransferItemImpl transferItem = null;
        Contract contract = null;
        ContractHeader contractHeader = null;
        ContractItem contractItem = null;
        ContractItemQuantityMap releaseQuantities = null;
        ContractItemPriceConfigMap releasePriceConfigs = null;
        ContractItemTechKey contractItemKey = null;
        HashSet releasedItemsSet = new HashSet();
        RequestParser.Parameter releaseItem = null;
        Shop shop = null;
        String forward = null;
        String quantity = null;
        String releaseItemValue = null;
        String unit = null;
        String contractKey = null;
        String contractId = null;
        String itemKey = null;
        String itemId = null;
        String productKey = null;
        User user = null;

        // read the shop data
        shop = bom.getShop();
        if (shop == null) {
			log.exiting();
            throw new PanicException("shop.notFound");
        }

        // get / create the Contract business object
        contract = bom.getContract();
        if (contract == null) {
			log.exiting();
            // no contract? => let readAction handle this
            return mapping.findForward(FORWARD_CONTRACT_READ);
        }

        // get selected contract
        contractHeader = contract.getSelectedContract();
        if (contractHeader == null) {
			log.exiting();
            // no contract header? => let readAction handle this
            return mapping.findForward(FORWARD_CONTRACT_READ);
        }
        contractKey = contractHeader.getTechKey().getIdAsString();
        contractId = contractHeader.getId();

        // get relelase quantities
        releaseQuantities = contract.getReleaseQuantities();
        if (releaseQuantities == null) {
			log.exiting();
            // no quantities? => back to readAction
            return mapping.findForward(FORWARD_CONTRACT_READ);
        }

        // get price / config data
        releasePriceConfigs = contract.getReleasePriceConfigs();

        // get user
        user = bom.getUser();
        if (user == null) {
			log.exiting();
            return mapping.findForward("login");
        }

        // create a new list of basket transfer items
        transferItems = new ArrayList();

        // determine if only a single item is requested
        releaseItem = requestParser.getParameter(
                ContractReadAction.CONTRACT_RELEASE_ITEM);

        // release a single item only
        releaseItemValue = releaseItem.getValue().getString();
        if (releaseItem.isSet() &&
            !(releaseItemValue.equalsIgnoreCase("") ||
              releaseItemValue.equalsIgnoreCase("null")) ) {
            contractItemKey = ContractItemTechKeys.getContractItemKey(
                releaseItemValue);
            if (releaseQuantities.containsKey(contractItemKey)) {
                itemKey = contractItemKey.getItemKey().getIdAsString();
                contractItem = contract.readItem(new TechKey(contractKey), contractItemKey.getItemKey(), shop).get(0);
                itemId = contractItem.getId();
                productKey = contractItemKey.getProductKey().getIdAsString();
                quantity = releaseQuantities.getQuantity(contractItemKey);
                unit = releaseQuantities.getUnit(contractItemKey);
                transferItem = new BasketTransferItemImpl();
                transferItem.setSource(TRANSFER_ITEM_SOURCE);
                transferItem.setContractKey(contractKey);
                transferItem.setContractId(contractId);
                transferItem.setContractItemKey(itemKey);
                transferItem.setContractItemId(itemId);
                transferItem.setProductKey(productKey);
                transferItem.setQuantity(quantity);
                transferItem.setUnit(unit);
                if (releasePriceConfigs != null &&
                	releasePriceConfigs.getConfigurationType(contractItemKey) != null &&
                    releasePriceConfigs.getConfigurationType(contractItemKey).
                        length() > 0) {
                    // as of release 3.0, config of contract items is
                    // display-only => no config item will be passed
/*
                    transferItem.setConfigurationItem(
                        releasePriceConfigs.
                        getConfigItemReference(contractItemKey));
*/
                    transferItem.setContractConfigFilter(
                        releasePriceConfigs.getConfigFilter(contractItemKey));
                }
                transferItems.add(transferItem);

                // remove quantity from the contract object
                releaseQuantities.remove(contractItemKey);

                // mark them as released for the remainder of the request
                releasedItemsSet.add(contractItemKey);
            }
        }

        // release all requested items
        else {

            // loop at requested products
			TreeMap treeMap = new TreeMap();            
            Iterator itemIterator = releaseQuantities.getItemKeyIterator();
            while (itemIterator.hasNext()) {
                contractItemKey = (ContractItemTechKey)itemIterator.next();
                itemKey = contractItemKey.getItemKey().getIdAsString();
                contractItem = contract.readItem(new TechKey(contractKey), contractItemKey.getItemKey(), shop).get(0);
                itemId = contractItem.getId();
                productKey = contractItemKey.getProductKey().getIdAsString();
                quantity = releaseQuantities.getQuantity(contractItemKey);
                unit = releaseQuantities.getUnit(contractItemKey);
                transferItem = new BasketTransferItemImpl();
                transferItem.setSource(TRANSFER_ITEM_SOURCE);
                transferItem.setContractKey(contractKey);
                transferItem.setContractId(contractId);
                transferItem.setContractItemKey(itemKey);
                transferItem.setContractItemId(itemId);
                transferItem.setProductKey(productKey);
                transferItem.setQuantity(quantity);
                transferItem.setUnit(unit);
                // as of release 3.0, config of contract items is display-only
                // => no config item will be passed
                if (releasePriceConfigs != null) {
                    transferItem.setContractConfigFilter(
                        releasePriceConfigs.getConfigFilter(contractItemKey));
                }
				// In the case of Partner/Product Ranges (PPR) in the contract, the Item ID may                                                  
				// not be unique.                                        
				Integer mapKey = new Integer(itemId);                    
				if (!treeMap.containsKey(mapKey))                        
				{                                                        
					treeMap.put(mapKey, new LinkedList());               
				}                                                        
				List itemList = (List) treeMap.get(mapKey);              
				itemList.add(transferItem);                              
				// remove quantity from the contract object              
				itemIterator.remove();                                   


				// mark them as released for the remainder of the request
				releasedItemsSet.add(contractItemKey);
            }
			// the order of the items taken from the contract has to be taken into 
			// account!
			Iterator sortedMapIterator = treeMap.entrySet().iterator();  
			while (sortedMapIterator.hasNext()) {                        
				Map.Entry entry = (Map.Entry)sortedMapIterator.next();   
				List itemList = (List) entry.getValue();                 
				Iterator itemListIterator = itemList.iterator();         
				while (itemListIterator.hasNext())                       
				{                                                        
					transferItem = (BasketTransferItemImpl)itemListIterator.next();                                                              
					transferItems.add(transferItem);                     
				}                                                        
			}                                                            
		}                                                 

        // flag which contract items have been released for read action
        request.setAttribute(CONTRACT_ITEMS_RELEASED, releasedItemsSet);

        // determine forward
        forward = FORWARD_CONTRACT_READ;

        // prepare call of AddToBasketAction
        if (!transferItems.isEmpty()) {
            request.setAttribute(
                AddToBasketAction.RC_TRANSFER_ITEM_LIST,transferItems);
            request.setAttribute(AddToBasketAction.FORWARD_NAME,forward);
            forward = FORWARD_ADD_TO_DOCUMENT;
        }
		log.exiting();
        // always return to contract read action
        return mapping.findForward(forward);
    }
}