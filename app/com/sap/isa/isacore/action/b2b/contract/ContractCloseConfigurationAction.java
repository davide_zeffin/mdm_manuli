/*****************************************************************************
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Stefan Hunsicker
  Created:      18.08.2001

  $Revision: #0 $
  $Date: 2001/08/18 $
*****************************************************************************/
package com.sap.isa.isacore.action.b2b.contract;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.action.IsaCoreBaseAction;


/**
 * This action re-replaces the configuration ui for the contract with the
 * prevoiusly visible contract.
 *
 */
public class ContractCloseConfigurationAction extends IsaCoreBaseAction {


    private static final String FORWARD_UPDATEDOCUMENT = "updatedocumentview";
	
     /**
     * Overriden <em>isaPerform</em> method of <em>IsaCoreBaseAction</em>.
     */
    public ActionForward isaPerform(ActionMapping mapping,
                ActionForm form,
                HttpServletRequest request,
                HttpServletResponse response,
                UserSessionData userSessionData,
                RequestParser parser,
                BusinessObjectManager bom,
                IsaLocation log) throws CommunicationException {

		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
         // Page that should be displayed next.
        String forwardTo = null;

        HttpSession session = request.getSession();

        String sessionId = session.getId();
        boolean isDebugEnabled = log.isDebugEnabled();

        String logPrefix = null;

        if (isDebugEnabled) {
            logPrefix = sessionId + " ~ isaPerform(): ";
        }

        // get document handler
        DocumentHandler documentHandler = (DocumentHandler)
                userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
        if (documentHandler == null) {
        	log.exiting();
            throw new PanicException("No document handler found");
        }

        // replace configuration document on top by the prevoius contract
        // document
        ManagedDocument onTopDoc = documentHandler.getManagedDocumentOnTop();
        if (onTopDoc == null) {
        	log.exiting();
           throw new PanicException("No document found on top");
        }
        ManagedDocument contractDoc =
            new ManagedDocument(
                onTopDoc.getDocument(),
                onTopDoc.getDocType(),
                onTopDoc.getDocNumber(),
                onTopDoc.getRefNumber(),
                onTopDoc.getRefName(),
                onTopDoc.getDate(),
                "contract",
                onTopDoc.getHref(),
                onTopDoc.getHrefParameter());
        documentHandler.add(contractDoc);
        documentHandler.setOnTop(contractDoc.getDocument());
        forwardTo = FORWARD_UPDATEDOCUMENT;

        // remove config item key from session context
        userSessionData.removeAttribute(
                ContractConfigurationAction.CONTRACT_CONFIGURE_ITEM_KEY);
		log.exiting();
        return mapping.findForward(forwardTo);
    }
}