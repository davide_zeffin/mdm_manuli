/*****************************************************************************
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      2 May 2001

    $Revision: #2 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.isacore.action.b2b.contract;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.sap.isa.businessobject.contract.ContractItemQuantityMap;
import com.sap.isa.core.util.RequestParser;


/*
 * Helper class to extract item release quantities from the request context.
 */
class ReleaseQuantities {
    private static ContractItemQuantityMap releaseQuantities;
    private RequestParser requestParser;

    // static initializer
    static {
        releaseQuantities = new ContractItemQuantityMap();
    }

    // constructor
    public ReleaseQuantities(RequestParser requestParser) {
        this.requestParser = requestParser;
    }

    // parse the map of release Quantities out of the request
    public ContractItemQuantityMap getReleaseQuantities(
            Set releasedItemsSet) {
        Iterator valueIterator = null;
        Map quantityValueMap = null;
        RequestParser.Parameter quantityParameter = null;
        RequestParser.Parameter requestedParameter = null;
        RequestParser.Parameter unitParameter = null;
        String key = null;
        String quantity = null;
        String unit = null;
        boolean requested = false;

        // reset release quantities
        releaseQuantities.clear();

        // if new quantites are requested, extract them from request context
        quantityParameter = requestParser.getParameter("quantity[]");
        if (quantityParameter.isSet()) {
            requestedParameter = requestParser.getParameter("requested[]");
            unitParameter = requestParser.getParameter("unit[]");

            // put quantities into ContractItemQuantityMap
            quantityValueMap = quantityParameter.getValueMap();
            valueIterator = quantityValueMap.keySet().iterator();
            while (valueIterator.hasNext()) {
                key = (String)valueIterator.next();
                requested = requestedParameter.getValue(key).getString().
                    length() > 0 ? true : false;
                quantity = (String)quantityValueMap.get(key);

                // add only quantities that have not been released in the same
                // request
                if (!releasedItemsSet.contains(
                        ContractItemTechKeys.getContractItemKey(key)) &&
                        requested && quantity.length() > 0) {
                    unit = unitParameter.getValue(key).getString();
                    releaseQuantities.put(
                        ContractItemTechKeys.getContractItemKey(key),
                        quantity,
                        unit);
                }

                // quantities with empty data will be deleted from contract
                else {
                    releaseQuantities.put(
                        ContractItemTechKeys.getContractItemKey(key), "", "");
                }
            }
        }
        return releaseQuantities;
    }
}
