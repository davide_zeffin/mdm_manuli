/*****************************************************************************
	Class         PrepareLoginAction
	Copyright (c) 2001, SAP Germany, All rights reserved.
	Author:       SAP
	Created:      09.10.2001
	Version:      1.0

	$Revision: #2 $
	$Date: 2003/10/16 $
*****************************************************************************/


package com.sap.isa.isacore.action.b2b;

// framework dependencies
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.User;
import com.sap.isa.core.Constants;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.interaction.InteractionConfigContainer;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.StartupParameter;
import com.sap.isa.isacore.action.ActionConstants;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.security.api.IPrincipal;
import com.sap.security.api.IUser;
import com.sap.security.api.UMFactory;
import com.sap.security.api.logon.ILoginConstants;

public class PrepareLoginAction extends IsaCoreBaseAction {

    private static final String FAILURE                   = "failure";
    private static final String SUCCESS                   = "success";
    private static final String MYSAPSSO2                 = "MYSAPSSO2";
    private static final String SPECIAL_NAME_AS_USERID    = "$MYSAPSSO2$";

    private static final String PARAM_USERID              = "userid";
    private static final String PARAM_PASSWORD            = "password";

    private static final String UME_USER                  = "j_user";
    private static final String UME_PASSWORD              = "j_password";
  
	/**
	* Security Check.
	*/
    public void initialize() {
    	if (isB2B()) {
    		this.checkUserIsLoggedIn = false;
    	}
    }

    public ActionForward isaPerform(ActionMapping mapping,
                                    ActionForm form,
                                    HttpServletRequest request,
                                    HttpServletResponse response,
                                    UserSessionData userSessionData,
                                    RequestParser requestParser,
                                    BusinessObjectManager bom,
                                    IsaLocation log)
                                    throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        // get InteractionConfigContainer
        InteractionConfigContainer interactionConfigData = getInteractionConfig(request);

        String userId   = request.getParameter(PARAM_USERID);
        String password = request.getParameter(PARAM_PASSWORD);
        User user = bom.getUser(); 

        // get StartupParameter to check if UME login should be provided
        StartupParameter startupParameter =
                (StartupParameter)userSessionData.getAttribute(SessionConst.STARTUP_PARAMETER);
        if (startupParameter != null) {
            String umeloginParameter = startupParameter.getParameterValue(Constants.UMELOGIN);
            if (umeloginParameter.length() > 0 && umeloginParameter.equals("YES")) {
                // use UME ? 
                String umeEnabled = interactionConfigData.getConfig(ActionConstants.UME_INTERACTION_CONFID).getValue(ActionConstants.UME_INTERACTION_CONFVALUE);
                if(log.isDebugEnabled()) log.debug("UME-Enabling: " + umeEnabled);
                if(umeEnabled != null && umeEnabled.equals("true")){
                    request.setAttribute(UME_USER, userId);	
                    request.setAttribute(UME_PASSWORD, password);
                    request.setAttribute("schema", ActionConstants.UME_LOGON_SCHEMA_B2B);
                    IUser userUME = UMFactory.getAuthenticator().forceLoggedInUser(request, response);
                    if(userUME == null){
                        return mapping.findForward("umelogin");
                    }
                    else{
                        user.setUserUME(userUME); 
                        if (log.isDebugEnabled()) log.debug("UME-Uid = " + userUME.getUid());
                    }
                }
            }
        }                
        
        // check for darklogin
		log.debug("Checking for dark login.");
        if ((userId != null) && (userId.length() > 0 )) {

            // if a user is given check if also the password is given
            if ((password != null) && (password.length() > 0)) {
                // set both to request context and call the login action
				log.debug("user and password are given, call the login action directly.");
                request.setAttribute(LoginAction.PN_USERID,userId);
                request.setAttribute(LoginAction.PN_PASSWORD,password);
                return mapping.findForward(SUCCESS);
            }
            else {
				log.debug("Only the user id is given, go to the standard login action.");
                user.setUserId(userId);
                request.setAttribute(BusinessObjectBase.CONTEXT_NAME,user);
            }
        }

		log.debug("check whether there is an MYSAPSSO2-Cookie");
        // check whether there is an MYSAPSSO2-Cookie
        Cookie[] cookies = request.getCookies();
        Cookie mySAPSSO2Cookie = null;
        // iterate over all cookies, if there are cookies at all
		log.debug("iterate over all cookies, if there are cookies at all");
        if (cookies != null) {
            for (int i=0; mySAPSSO2Cookie == null && i < cookies.length; i++) {
                if (cookies[i].getName().equals(MYSAPSSO2)) {
                    // cookie found -> get cookie
                    mySAPSSO2Cookie = cookies[i];

                    if (log.isDebugEnabled()) {
                        log.debug("MYSAPSSO2 Cookie was found!");
                    }
                }
            }
        }
        // if the desired cookie was not found, prompt the login JSP
        if (mySAPSSO2Cookie == null) {
			if (log.isDebugEnabled()) log.debug("the desired cookie was not found, check for certificate login before calling the login JSP");
			// it is possible that the login were performed with a certificate.
			// in this case the UMEuser object needs to be checked for the
			// SSO ticket string.
			IUser userUME = user.getUserUME();
			if (userUME != null) {
				// toString methods works also with a null-value.
				Object ticketObject =  userUME.getTransientAttribute(IPrincipal.DEFAULT_NAMESPACE, 
																	 ILoginConstants.SSOTICKET_USER_ATTRIBUTE_PURE);
				if(ticketObject != null) {
					if (log.isDebugEnabled()) log.debug("SSO Ticket string is present.");
					password = ticketObject.toString();
				} else {
					if (log.isDebugEnabled()) log.debug("SSO Ticket string is NOT present. Calling the login JSP");
					log.exiting();
					return mapping.findForward(FAILURE);
				}
			} else {
				if (log.isDebugEnabled()) log.debug("No UME user could be found. Calling the login JSP");
				log.exiting();
				return mapping.findForward(FAILURE);
			}
		} else {
			if (log.isDebugEnabled()) log.debug("Gathering the Cookie value.");
			password = mySAPSSO2Cookie.getValue();
		}

		// set a special user id indicating that a SSO2 ticket will be used
		request.setAttribute(LoginAction.PN_USERID, SPECIAL_NAME_AS_USERID);
		// set the cookie's value (=ticket) as password
		request.setAttribute(LoginAction.PN_PASSWORD, password);
		log.exiting();
        return mapping.findForward(SUCCESS);
    }
}
