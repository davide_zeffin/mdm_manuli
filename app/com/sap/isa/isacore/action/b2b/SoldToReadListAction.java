/*****************************************************************************
	Class:        SoldToReadListAction
	Copyright (c) 2004, SAPLabs, Palo Alto, All rights reserved.
	Author        SAP

	$Revision: #1 $

*****************************************************************************/

package com.sap.isa.isacore.action.b2b;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.core.Constants;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.isacore.action.IsaCoreInitAction;

/**
 * This action class is ued in Portal enviroment as an iView to 
 * Change setting
 **/
public class SoldToReadListAction extends SoldToReadListBaseAction {
	private static final String FORWARD_AUTOENTRYERROR = "autoentryerror";
	/**
	 * Implement this method to add functionality to your action.
	 *
	 * @param form              The <code>FormBean</code> specified in the
	 *                          config.xml file for this action
	 * @param request           The request object
	 * @param response          The response object
	 * @param userSessionData   Object wrapping the session
	 * @param requestParser     Parser to simple retrieve data from the request
	 * @param bom               Reference to the BusinessObjectManager
	 * @param log               Reference to the IsaLocation, needed for logging
	 * @return Forward to another action or page
	 */
	public ActionForward isaPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		BusinessObjectManager bom,
		IsaLocation log)
		throws CommunicationException {

		// Page that should be displayed next.
		String forwardTo = null;

		boolean isDebugEnabled = log.isDebugEnabled();

		User user = bom.createUser();
		Shop shop = bom.getShop();
		String shopId = null;
		RequestParser.Parameter shopP =  requestParser.getParameter("WebShop");
		if (shopP != null){
			shopId = shopP.getValue().getString();
		}else{
			shopId = bom.getShop().getId();
		}		
		ResultData result = null;
		result = readSoldToList(user, shopId, bom);

		IsaCoreInitAction.StartupParameter startupParameter =
			(IsaCoreInitAction.StartupParameter) userSessionData.getAttribute(
				IsaCoreInitAction.SC_STARTUP_PARAMETER);
		if ((result == null) || (result.getNumRows() <= 0)) { // fm 19.06.01

			if (isDebugEnabled) {
				log.debug("found no sold to, bailing out.");
			}

			log.error(LogUtil.APPS_USER_INTERFACE, "no sold tos found!");

			user.addMessage(new Message(Message.ERROR, "user.noSoldTo"));

			// user.addMessage(new Message(Message.ERROR,"user.noSoldTo",null,null));
			request.setAttribute(BusinessObjectBase.CONTEXT_NAME, user);
			log.exiting();
			return mapping.findForward("error");
//		} else if (result.getNumRows() == 1) {
//
//			// move to first row
//			result.absolute(1);
//
//			if (isDebugEnabled) {
//				log.debug(
//					"found exactly one sold to, using it - SOLDTO: "
//						+ result.getString("SOLDTO")
//						+ " - SOLDTO-TECHKEY: "
//						+ result.getString("SOLDTO_TECHKEY"));
//			}
//
//			// Save sold techKey in request
//			request.setAttribute(
//				ReadSoldToAction.PARAM_SOLDTO,
//				result.getString("SOLDTO"));
//			request.setAttribute(
//				ReadSoldToAction.PARAM_SOLDTO_TECHKEY,
//				result.getString("SOLDTO_TECHKEY"));
//
//			request.setAttribute(RK_SOLDTO_LIST, result);
//			userSessionData.setAttribute(RK_SOLDTO_LIST, result);
//			log.exiting();
//			return mapping.findForward("readsoldto");
		} else {

//			if (startupParameter.getAutoEntry().equals("YES")) {
//				// Found more than one sold-to parties but autoentry requested (currently
//				// used from Portal iViews). Since no shoplist can be presented, this
//				// situation will lead to errorpage
//				request.setAttribute(RK_AUTOENTRYERROR, "soldtoerror");
//				request.setAttribute(
//					RK_AUTOENTRYADDINFO,
//					"Contact:".concat(user.getUserId()));
//				return mapping.findForward(FORWARD_AUTOENTRYERROR);
//			}
			result.absolute(1);
			// Save sold techKey in request
			if (isDebugEnabled) {
				log.debug(
					"found one sold to, using the first one it - SOLDTO: "
						+ result.getString("SOLDTO")
						+ " - SOLDTO-TECHKEY: "
						+ result.getString("SOLDTO_TECHKEY"));
			}
			String soldToId = (String)request.
							getAttribute(Constants.PARAM_SOLDTO);
			if ((soldToId == null) || (soldToId.length()== 0)){
				request.setAttribute(
					ReadSoldToAction.PARAM_SOLDTO,
					result.getString("SOLDTO"));
				request.setAttribute(
					ReadSoldToAction.PARAM_SOLDTO_TECHKEY,
					result.getString("SOLDTO_TECHKEY"));
				request.setAttribute(
					Constants.PARAM_SOLDTO,
					result.getString("SOLDTO"));
			}
			if (isDebugEnabled) {
				log.debug("found one or more than one sold to, used the first one");
			}
			request.setAttribute(RK_SOLDTO_LIST, result);
			userSessionData.setAttribute(RK_SOLDTO_LIST, result);
		}
		log.exiting();
		return mapping.findForward("selectsoldto");

	}

}
