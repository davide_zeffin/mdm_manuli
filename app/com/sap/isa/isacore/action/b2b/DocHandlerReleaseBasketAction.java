/*****************************************************************************
    Class         DocHandlerReleaseBasketAction
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Action for the Profile Maintenance
    Author:       Steffen M�ller
    Created:      Juni 20001
    Version:      1.0

    $Revision: #4 $
    $Date: 2001/10/29 $

*****************************************************************************/
package com.sap.isa.isacore.action.b2b;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.order.Basket;
import com.sap.isa.businessobject.order.OrderStatus;
import com.sap.isa.businessobject.ordertemplate.OrderTemplate;
import com.sap.isa.businessobject.quotation.Quotation;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 * Displays a confirmation of the order
 *
 * @author Steffen M�ller
 * @version 1.0
 *
 */


public class DocHandlerReleaseBasketAction extends IsaCoreBaseAction {

    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            BusinessObjectManager bom,
            IsaLocation log)
            throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        // Page that should be displayed next.
        String forwardTo = null;

        // get pre-order document on top from document handler
        DocumentHandler documentHandler = (DocumentHandler)
                userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
//        DocumentState docstate =
//            documentHandler.getDocument(ManagedDocument.TARGET_DOCUMENT);
          ManagedDocument mDoc = documentHandler.getManagedDocumentOnTop();
          DocumentState docstate = mDoc.getDocument();

        // get the current pre order sales document - basket is assumed as
        // default
        SalesDocument preOrderSalesDocument = null;
        if (docstate instanceof Quotation) {
            preOrderSalesDocument = bom.getQuotation();
            bom.releaseQuotation();
            documentHandler.release(preOrderSalesDocument);
        }
        else if (docstate instanceof OrderTemplate) {
            preOrderSalesDocument = bom.getOrderTemplate();
            bom.releaseOrderTemplate();
            documentHandler.release(preOrderSalesDocument);
        }
        else if (docstate instanceof Basket){
            preOrderSalesDocument = bom.getBasket();
            bom.releaseBasket();
            documentHandler.release(preOrderSalesDocument);
        }
        else if (docstate instanceof OrderStatus) {
            OrderStatus os = bom.getOrderStatus();
            if (os != null) {
                documentHandler.release(os);
                bom.releaseOrderStatus();
            }
        }
		log.exiting();
        return mapping.findForward("updatedocumentview");

    }

}

