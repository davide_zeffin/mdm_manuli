/**
 *  Class:        WorkareaNavAction
 *  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
 *  Author:       Franz-Dieter Berger
 *  Created:      09.05.2001
 *  Version:      1.0
 *  $Revision: #5 $
 *  $Date: 2001/08/01 $
 */

package com.sap.isa.isacore.action.b2b;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.item.ItemList;
import com.sap.isa.businessobject.negotiatedcontract.NegotiatedContract;
import com.sap.isa.businessobject.order.CollectiveOrder;
import com.sap.isa.businessobject.order.Order;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.eai.BackendRuntimeException;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.action.order.GetCollectiveOrderBaseAction;

/**
 *  Determines which JSP should be displayed in the <i>documents</i> frame
 *  (the tabstrips for documents) and puts the neccessary data in the request context.
 *
 *  The following forwards are used by this action:
 *  </p>
 *  <table border="1" cellspacing="0" cellpadding="2">
 *  <tr>
 *  <td><b>forward</b></td>
 *  <td><b>description</b></td>
 *  <tr>
 *  <tr><td>FORWARD_NODOC</td><td>No document is active.</td></tr>
 *  <tr><td>FORWARD_ONEDOC</td><td>One document is active.</td></tr>
 *  <tr><td>FORWARD_TWODOC</td><td>Two documents are active.</td></tr>
 *  <tr><td>FORWARD_FAILURE</td><td>Something went wrong.</td></tr>
 *  </table>
 *
 *  @author     Franz-Dieter Berger
 *  @created    7. Dezember 2001
 *  @version    1.0
 */
public class WorkareaNavAction extends GetCollectiveOrderBaseAction {
	
    /**
     *  Constant in the request context marking if the catalog is on top or not,
     *  value is &quot;catontop&quot;
     */
    public final static String CATONTOP = "catontop";
    
    
    // --------------------------- all you need for minibasket information
    /**
     *  Constant in the request context for storing the netValue of the current editable document
     *  value is &quot;netvalue&quot;
     */
    public final static String RK_MB_NETVALUE = "mb_netvalue";
    
    /**
     *  Constant in the request context for storing the current number of positions in the current editable document
     *  value is &quot;itemsize&quot;
     */
    public final static String RK_MB_ITEMSIZE = "mb_itemsize";
    
    /**
     *  Constant in the request context for storing the currency of the current editable document (used in minibasket)
     *  value is &quot;currency&quot;
     */
    public final static String RK_MB_CURRENCY = "mb_currency";
    
    /**
     *  Constant in the request context for storing the documentype of the current editable document (used in minibasket)
     *  value is &quot;currency&quot;
     */
    public final static String RK_MB_DOCTYPE = "mb_doctype";
    
    
    // --------------------------- all you need for the rendering of tabstrip 1
    /**
     *  Constant in the request context containing the document type on the left tab strip,
     *  value is &quot;doctype1&quot;
     */
    public final static String DOCTYPE1 = "doctype1";

    /**
     *  Constant in the request context containing the number of the document on the left tab strip,
     *  value is &quot;docnumber1&quot;
     */
    public final static String DOCNUMBER1 = "docnumber1";

    /**
     *  Constant in the request context containing the date of the document on the left tab strip,
     *  value is &quot;docdate1&quot;
     */
    public final static String DOCDATE1 = "docdate1";

    /**
     *  Constant in the request context marking if the document on the left tab strip is edtable or not,
     *  value is &quot;editable1&quot;
     */
    public final static String EDITABLE1 = "editable1";

    /**
     *  Constant in the request context marking if the document on the left tab strip is on top of the other or not,
     *  value is &quot;ontop1&quot;
     */
    public final static String ONTOP1 = "ontop1";

    // --------------------------- all you need for the rendering of tabstrip 2
    /**
     *  Constant in the request context containing the document type on the right tab strip,
     *  value is &quot;doctype2&quot;
     */
    public final static String DOCTYPE2 = "doctype2";

    /**
     *  Constant in the request context containing the number of the document on the right tab strip,
     *  value is &quot;docnumber2&quot;
     */
    public final static String DOCNUMBER2 = "docnumber2";

    /**
     *  Constant in the request context containing the date of the document on the right tab strip,
     *  value is &quot;docdate2&quot;
     */
    public final static String DOCDATE2 = "docdate2";

    /**
     *  Constant in the request context marking if the document on the right tab strip is edtable or not,
     *  value is &quot;editable2&quot;
     */
    public final static String EDITABLE2 = "editable2";

    /**
     *  Constant in the request context marking if the document on the right tab strip is on top of the other or not,
     *  value is &quot;ontop2&quot;
     */
    public final static String ONTOP2 = "ontop2";

    /**
     * Constant for available forwards, value is &quot;nodoc&quot;.
     */
    protected final static String FORWARD_NODOC = "nodoc";

    /**
     * Constant for available forwards, value is &quot;onedoc&quot;.
     */
    protected final static String FORWARD_ONEDOC = "onedoc";

    /**
     * Constant for available forwards, value is &quot;twodoc&quot;.
     */
    protected final static String FORWARD_TWODOC = "twodoc";

    /**
     * Constant for available forwards, value is &quot;failure&quot;.
     */
    protected final static String FORWARD_FAILURE = "failure";
    
    /**
     * Constant for available forwards, value is &quot;failure&quot;.
     */
    protected final static String FORWARD_BACK_TO_CAT = "backToCatalog";


    /**
     *  Overriden <em>isaPerform</em> method of <em>IsaCoreBaseAction</em>.
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser parser,
            BusinessObjectManager bom,
            IsaLocation log)
             throws CommunicationException {
             	
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        String forwardTo = null;            // Page that should be displayed next.
        String itemSize = "";               // info for minibasket: number of positions in editable salesdoc
        String netValue = "";               // info for minibasket: netvalue in editable salesdoc
        DocumentState doc;

        int numberOfDocs = 0;               // number of active documents

        String logPrefix = null;
        boolean isDebugEnabled = log.isDebugEnabled();
        if (isDebugEnabled) {
            logPrefix = "isaPerform(): ";
        }

        
        User user = bom.getUser();
        
        Shop shop = bom.getShop();

        DocumentHandler documentHandler = (DocumentHandler)
                userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);

        if (documentHandler == null) {
        	log.exiting();
            throw new PanicException("No document handler found.");
        }

        // writes the information if the catalog is on top or not into the request context
        if (documentHandler.isCatalogOnTop()) {
            request.setAttribute(CATONTOP, "1");
        }
        else {
            request.setAttribute(CATONTOP, "0");
        }

        // how many documents must we handle
        numberOfDocs = documentHandler.getNumberOfDocuments();
        if (isDebugEnabled) {
            log.debug(logPrefix + "#Docs: " + numberOfDocs);
        }

        if (shop.isHomActivated()) {
        // if we are in HOM scenario the minibasket represents the collective order
        // only information about collective order are displayed on minibasket
            try {
				CollectiveOrder collectiveOrder = getCollectiveOrder(userSessionData, request, bom,user,shop,true);
			   	//single point for setting up minibasket information for collective order
			   	setMiniBasketInformation(request,(SalesDocument)collectiveOrder,shop);
           }
           catch (PanicException exception) {
           	log.debug(exception.getMessage());
           }
        }    	

        switch (numberOfDocs) {
        // Note: when HOM scenario is selected, no minibasket information is updated in this
        //       section because in this scenario the collective order is represented by minibasket
            case 0:

            	if (!shop.isHomActivated()) {
            		
	                // no document active => minibasket displays "document in processing"
	                // set netValue and itemSize to default value
	                netValue="-1";
	                itemSize="-1";
	                
	                request.setAttribute(RK_MB_NETVALUE, netValue); 
	                request.setAttribute(RK_MB_ITEMSIZE, itemSize);   
	                request.setAttribute(RK_MB_DOCTYPE, "nodoc");   
            	}    
                forwardTo = FORWARD_NODOC;
                break;
                
            case 1:
                
                
                doc = documentHandler.getManagedDocumentOnTop();

                if (setRequestDocData(doc, request, DOCTYPE1, DOCNUMBER1, DOCDATE1, EDITABLE1)) {
                    request.setAttribute(ONTOP1, "1");
                   
                    // only one document active => read from backend only when document is editable
                    if (doc.getState() == DocumentState.TARGET_DOCUMENT) {
                          
                        if (!shop.isHomActivated()) {	
                            //find current miniBasket information and write into request context
                            setMiniBasketInformation(documentHandler, request, netValue, itemSize, user, shop);
                        }
                    }
                    else {
                    // only one document active and document not editable => no info for minibasket necessary                        	
                        if (!shop.isHomActivated()) {
                        // if HOM scenario is active minibasket displays collective order => no change for standard documents neccessary 	
                    	    netValue="-1";
                            itemSize="-1";
                            request.setAttribute(RK_MB_NETVALUE, netValue); 
                            request.setAttribute(RK_MB_ITEMSIZE, itemSize); 
                            request.setAttribute(RK_MB_DOCTYPE, "no_info");                              
                        }
                    }
                    forwardTo = FORWARD_ONEDOC;
                }
                else {
                    forwardTo = FORWARD_FAILURE;
                }
                
                break;
                
            case 2:
                DocumentState docleft = documentHandler.getManagedDocument(DocumentState.TARGET_DOCUMENT);
                DocumentState docright = documentHandler.getManagedDocument(DocumentState.VIEW_DOCUMENT);

                //note 1246817 minibasket information not updated correctly after ordering an item
                DocumentState docdefault = documentHandler.getManagedDocument(DocumentState.UNDEFINED);
                if(docright == null && docdefault != null){
                	docright = docdefault;
                }//end note

                if (setRequestDocData(docleft, request, DOCTYPE1, DOCNUMBER1, DOCDATE1, EDITABLE1) &&
                        setRequestDocData(docright, request, DOCTYPE2, DOCNUMBER2, DOCDATE2, EDITABLE2)) {
                    // which of the documents lays on top of the other
                    if (docleft == documentHandler.getManagedDocumentOnTop()) {
                        request.setAttribute(ONTOP1, "1");
                    }
                    else {
                        request.setAttribute(ONTOP1, "0");
                    }

                    if (docright == documentHandler.getManagedDocumentOnTop()) {
                        request.setAttribute(ONTOP2, "1");
                    }
                    else {
                        request.setAttribute(ONTOP2, "0");
                    }
                    
                    // determine basket information from editable document for minibasket
                    // two documents active => docleft is always the editable one
                    if (docleft != null) {
                    	
                        if (!shop.isHomActivated()) {	
	                    	//find current miniBasket information and write into request context
    	                    setMiniBasketInformation(documentHandler, request, netValue, itemSize, user, shop);
                        }    
                    }	                     

                    forwardTo = FORWARD_TWODOC;
                }
                else {
                    forwardTo = FORWARD_FAILURE;
                }
                break;
            default:
                // Well, this is a serious error. This should not happen.
                forwardTo = FORWARD_FAILURE;
                if (isDebugEnabled) {
                    log.debug(logPrefix + "ERROR in  WorkareaNavAction! Number of active documents (allowed are 0, 1, 2): " + numberOfDocs);
                }
                // Attention: there is nothing in the request context.
        }
        
        if ("true".equals(request.getParameter(FORWARD_BACK_TO_CAT))) {
            forwardTo = FORWARD_BACK_TO_CAT;
        }
        
        documentHandler.setMiniBsktNetValue((String) request.getAttribute(RK_MB_NETVALUE));
        documentHandler.setMiniBsktCurrency((String) request.getAttribute(RK_MB_CURRENCY));
        documentHandler.setMiniBsktItemSize((String) request.getAttribute(RK_MB_ITEMSIZE));
        documentHandler.setMiniBsktDoctype((String) request.getAttribute(RK_MB_DOCTYPE));
        
        log.exiting();
        return mapping.findForward(forwardTo);
    }


    /**
     *  setRequestDocData writes the type, number and date of the document in the request context.
     *  It also writes the information, if the document is editable or not.
     *
     *@param  doc        Reference on the ManagedDocument, which should be displayed on a tab strip.
     *@param  request    Request context
     *@param  docType    Name of the constant in the request context for the document type.
     *@param  docNumber  Name of the constant in the request context for the document number.
     *@param  docDate    Name of the constant in the request context for the document date.
     *@param  editable   Name of the constant in the request context for maring the document as editable.
     *@return            <code>true</code>, if the data could be put in the request context, <code>false</code> else.
     */
    protected boolean setRequestDocData(DocumentState doc, HttpServletRequest request,
            String docType, String docNumber, String docDate, String editable) {
        boolean ret = false;
        String logPrefix = null;
        String rdocType = "error";
        String rdocNumber = "error";
        String rdocDate = "00.00.0000 00:00";
        String reditable = "0";
        boolean isDebugEnabled = log.isDebugEnabled();
        if (isDebugEnabled) {
            logPrefix = "setRequestDocData(): ";
        }

        if (doc == null) {
            ret = false;
            if (isDebugEnabled) {
                log.debug(logPrefix + "doc = null, cannot put any data in the request context.");
            }

        }
        else {
            // findout if doc is editable or not
            if (doc.getState() == DocumentState.TARGET_DOCUMENT) {
                reditable = "1";
            }
            else {
                reditable = "0";
            }
            if (doc instanceof ManagedDocument) {
                ManagedDocument mdoc = (ManagedDocument) doc;
                rdocType = mdoc.getDocType();
                rdocNumber = mdoc.getDocNumber();
                rdocDate = mdoc.getDate();
                ret = true;
            }
            else {
                // schlimmer Fehler
                if (isDebugEnabled) {
                    log.debug(logPrefix + "Must handle a strange document type, but cannot! ");
                }

                ret = false;
            }

        }
        if (rdocDate == null) {
            rdocDate = " ";
        }
        if (rdocType == null) {
            rdocType = " ";
        }
        if (rdocNumber == null) {
            rdocNumber = " ";
        }

        request.setAttribute(docType, rdocType);
        request.setAttribute(docNumber, rdocNumber);
        request.setAttribute(docDate, rdocDate);
        request.setAttribute(editable, reditable);

        return ret;
    }
    
    
    
    /**
     *  setMinBasketInformation reads up to date header and item information for editable sales documents 
     *  from backend and writes netValue and number of item positions of the editable sales document in 
     *  the request context. This information is used for up to date minibasket information.
     *
     *  @param  documentHandler   Reference on the DocumentHandler
     *  @param  request           Request context
     *  @param  netValue          Variable for writing the netValue into the request context
     *  @param  itemSize          Variable for writing the number of positions into the request context
     *  @param  user              User!?
     */
    protected void setMiniBasketInformation(DocumentHandler documentHandler, HttpServletRequest request,
                                               String netValue, String itemSize, User user) throws CommunicationException {
    	
    	DocumentState targetDoc = documentHandler.getDocument(DocumentState.TARGET_DOCUMENT);
                        
        if (targetDoc instanceof SalesDocument) {
            // casting to salesDocument in order to get netValue and itemSize information
            SalesDocument salesDoc = (SalesDocument) targetDoc;
        
            setMiniBasketInformation(request, user, salesDoc);
        }
        else {
            // document is no salesDocument => minibasket displays "document in processing"
            // set netValue and itemSize to default value
            netValue="-1";
            itemSize="-1";
            request.setAttribute(RK_MB_NETVALUE, netValue); 
            request.setAttribute(RK_MB_ITEMSIZE, itemSize);
            request.setAttribute(RK_MB_DOCTYPE, "no_info");
        }
    }

	/**
	 *  setMinBasketInformation reads up to date header and item information for editable sales documents 
	 *  from backend and writes netValue and number of item positions of the editable sales document in 
	 *  the request context. This information is used for up to date minibasket information.
	 *
	 *  @param  documentHandler   Reference on the DocumentHandler
	 *  @param  request           Request context
	 *  @param  netValue          Variable for writing the netValue into the request context
	 *  @param  itemSize          Variable for writing the number of positions into the request context
	 *  @param  user              User!?
	 *  @param  shop              Shop
	 */
	protected void setMiniBasketInformation(DocumentHandler documentHandler, HttpServletRequest request,
											   String netValue, String itemSize, User user, Shop shop) throws CommunicationException {
    	
		DocumentState targetDoc = documentHandler.getDocument(DocumentState.TARGET_DOCUMENT);
                        
		if (targetDoc instanceof SalesDocument) {
			// casting to salesDocument in order to get netValue and itemSize information
			SalesDocument salesDoc = (SalesDocument) targetDoc;
        
			setMiniBasketInformation(request, salesDoc, shop);
		}
		else {
			// document is no salesDocument => minibasket displays "document in processing"
			// set netValue and itemSize to default value
			netValue="-1";
			itemSize="-1";
			request.setAttribute(RK_MB_NETVALUE, netValue); 
			request.setAttribute(RK_MB_ITEMSIZE, itemSize);
			request.setAttribute(RK_MB_DOCTYPE, "no_info");
		}
	}


    /**
     *  setMinBasketInformation reads up to date header and item information for editable sales documents 
     *  from backend and writes netValue and number of item positions of the editable sales document in 
     *  the request context. This information is used for up to date minibasket information.
     *
     *  @param  request           Request context
     *  @param  user              User             
     *  @param  salesDocument     document with price informations
     */
    protected void setMiniBasketInformation(HttpServletRequest request, 
    										User user, 
    										SalesDocument salesDoc)
		        throws CommunicationException {
		            
        //constant that stores the document type of the current sales document
        String doctype="";
        
        //constants used for storing information displayed in minibasket
        String netValue="";
        String itemSize="";
        String currency="";
        
        // backend read for up to date backend object data (salesdoc readForUpdate for order instances)
        if (salesDoc instanceof Order) {
        	//reading complete document (header and items) for update
        	try {
		        ((Order)salesDoc).readForUpdate();
        	}
        	catch (BackendRuntimeException ex) {		
                 log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "The required document is locked - BackendRuntimeException", ex);
        	}
        }
        else {
        	// if salesdoc is no instance of order we have to read header and items separately
	        // don't move reading the header elsewhere, because some backends are having problems
	        // if the items and shiptos are read and the header wasn't read before.
	        salesDoc.readHeader();
	        // get itemSize information
	        salesDoc.readAllItemsForUpdate();
        }        

		HeaderSalesDocument header = salesDoc.getHeader();
		ItemList items = salesDoc.getItems();
		
		// get item size of main positions
		itemSize = ""+items.numberOfMainPositions();
	
        if (salesDoc instanceof NegotiatedContract) {
            //salesdocument is a 'negotiated contract' => needed infos for minibasket: only itemsize relevant
            doctype = "negotiatedContract";
        }
        else {
        	//salesdocument is no 'negotiated contract' => needed infos for minibasket: itemsize, netvalue, currency
            doctype = "general_salesdoc";
            
            //when there is no position in salesdoc, no value and currency info is diplayed in minibasket
            if (itemSize.equals("0")) {
        	    // itemSize and netValue = 0 => minibasket displays standard text "no items in basket"
        	    netValue="0"; 
            }
            else {
            	//document is salesDocument and there is at least one item in minibasket => read header for minibasket info
                if (header != null) {
                    netValue = header.getNetValue();
                    currency = header.getCurrency();
                }
                else {
                    netValue = "0";
                }
            }
        }
        
        request.setAttribute(RK_MB_NETVALUE, netValue);
        request.setAttribute(RK_MB_CURRENCY, currency);
        request.setAttribute(RK_MB_ITEMSIZE, itemSize);
        request.setAttribute(RK_MB_DOCTYPE, doctype);
    }
   
	/**
	 *  setMinBasketInformation reads up to date header and item information for editable sales documents 
	 *  from backend and writes netValue and number of item positions of the editable sales document in 
	 *  the request context. This information is used for up to date minibasket information.
	 *
	 *  @param  request           Request context
	 *  @param  salesDocument     document with price informations
	 *  @param  shop              Shop
	 */
	protected void setMiniBasketInformation(HttpServletRequest request, 
											  SalesDocument salesDoc,
											  Shop shop)
				throws CommunicationException {
	            
		//constant that stores the document type of the current sales document
		String doctype="";
    
		//constants used for storing information displayed in minibasket
		String netValue="";
		String itemSize="";
		String currency="";
    
		// backend read for up to date backend object data
      
		// don't move reading the header elsewhere, because some backends are having problems
		// if the items and shiptos are read and the header wasn't read before.
        if (shop.getLargeDocNoOfItemsThreshold() == Shop.INFINITE_NO_OF_ITEMS) {
            // No large document handling read document at once
            salesDoc.read();
        } else {
            // Large document handling
            salesDoc.readHeader();
            // get itemSize information
            if (!salesDoc.isChangeHeaderOnly() || salesDoc.getSelectedItemGuids().size() > 0 ) {
               salesDoc.readAllItemsForUpdate();
            }
        }
        HeaderSalesDocument header = salesDoc.getHeader();
		ItemList items = salesDoc.getItems();
		if (salesDoc.isLargeDocument(shop.getLargeDocNoOfItemsThreshold())) {
			itemSize = "" + salesDoc.getCurrentNoOfPositions();
		}
		else {
			itemSize = "" + items.numberOfMainPositions();
		}

		if (salesDoc instanceof NegotiatedContract) {
			//salesdocument is a 'negotiated contract' => needed infos for minibasket: only itemsize relevant
			doctype = "negotiatedContract";
		}
		else {
			//salesdocument is no 'negotiated contract' => needed infos for minibasket: itemsize, netvalue, currency
			doctype = "general_salesdoc";
        
			//when there is no position in salesdoc, no value and currency info is diplayed in minibasket
			if (itemSize.equals("0")) {
				// itemSize and netValue = 0 => minibasket displays standard text "no items in basket"
				netValue="0"; 
			}
			else {
				//document is salesDocument and there is at least one item in minibasket => read header for minibasket info
				if (header != null) {
					netValue = header.getNetValue();
					currency = header.getCurrency();
				}
				else {
					netValue = "0";
				}
			}
		}
		request.setAttribute(RK_MB_NETVALUE, netValue);
		request.setAttribute(RK_MB_CURRENCY, currency);
		request.setAttribute(RK_MB_ITEMSIZE, itemSize);
		request.setAttribute(RK_MB_DOCTYPE, doctype);
	}
}
