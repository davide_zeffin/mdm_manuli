/*****************************************************************************
  Class:        CatalogEndAction
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Franz-Dieter Berger
  Created:      18.05.2001
  Version:      1.0

  $Revision: #2 $
  $Date: 2001/07/25 $
*****************************************************************************/

package com.sap.isa.isacore.action.b2b;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 * Ends displaying the catalog.
 *
 * @author Franz-Dieter Berger
 * @version 1.0
 */
public class CatalogEndAction extends IsaCoreBaseAction {

    private static final String FORWARD_SUCCESS = "success";
	
    /**
     * Overriden <em>isaPerform</em> method of <em>IsaCoreBaseAction</em>.
     */
    public ActionForward isaPerform(ActionMapping mapping,
                ActionForm form,
                HttpServletRequest request,
                HttpServletResponse response,
                UserSessionData userSessionData,
                RequestParser parser,
                BusinessObjectManager bom,
                IsaLocation log)
                        throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
      // Page that should be displayed next.
      String forwardTo = null;

      DocumentHandler documentHandler = (DocumentHandler)
                userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);

      documentHandler.setCatalogOnTop(false);

      forwardTo = FORWARD_SUCCESS;
	  log.exiting();
      return mapping.findForward(forwardTo);
  }
}