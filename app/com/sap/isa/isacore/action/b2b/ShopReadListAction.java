/*****************************************************************************
	Class:        ShopReadListAction
	Copyright (c) 2004, SAPLabs, Palo Alto, All rights reserved.
	Author        SAP

	$Revision: #1 $

*****************************************************************************/


package com.sap.isa.isacore.action.b2b;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.SearchResult;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.core.Constants;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.interaction.InteractionConfig;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.core.util.table.RowKey;
import com.sap.isa.isacore.action.ShopReadListBaseAction;
import com.sap.isa.isacore.action.b2c.B2cConstants;

public class ShopReadListAction extends ShopReadListBaseAction {
	private static final String FORWARD_SHOPLIST = "shoplist";
	/**
	 * Overriden <em>isaPerform</em> method of <em>IsaCoreBaseAction</em>.
	 */
	public ActionForward isaPerform(ActionMapping mapping,
				ActionForm form,
				HttpServletRequest request,
				HttpServletResponse response,
				UserSessionData userSessionData,
				RequestParser requestParser,
				BusinessObjectManager bom,
				IsaLocation log)
						throws CommunicationException {

		// Page that should be displayed next.
		// Logging
		boolean isDebugEnabled = log.isDebugEnabled();
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
		String forwardTo = null;
		Shop shop = bom.getShop();

		if (shop != null) {
			request.setAttribute(PARAM_SHOPID,shop.getId());
		}
		String shopId = null;
		SearchResult result = null;
		// get the scenario form the XCM.
		InteractionConfig interactionConfig = getInteractionConfig(request).getConfig("shop");
		String scenario = interactionConfig.getValue("shopscenario");
		if (isDebugEnabled) {
			log.debug("using scenario: " + scenario);
		}
		// Use the bom to retrieve a reference to the search object
		// read the result set to diplay it on the jsp
		result = readShopList(
							userSessionData,
							bom,
							log,
							scenario);
		ResultData shopList = new ResultData(result.getTable());
		int numOfRows = shopList.getNumRows();
		
		shopList.first();
		for (int i = 1; i <= numOfRows; i++){
			RowKey shop1 = shopList.getRowKey();
			shop1.toString();
			shopList.next();
		}
		if (result.getNumberOfHits() == 1) {

			// move to first row
			shopList.absolute(1);

			// Save shopId in request
			request.setAttribute(Constants.PARAM_SHOP, shopList.getRowKey());

			log.debug("found exactly one shop, using it: " + shopList.getRowKey());
		}
		else {
			boolean blnFlg = adjustNumofHits(
							request,
							userSessionData,
							log,
							result,
							"");
			if(blnFlg){
				log.debug("trying to redirect to webshop");
			}
			else{
				log.debug("found more than one shop");
				userSessionData.setAttribute(B2cConstants.BASKET_LOADED_DIRECT,"true");
			}           
		}	
		// Store list of shops in the request context
		log.exiting();
		request.setAttribute(RK_SHOP_LIST, shopList);
		return mapping.findForward(FORWARD_SHOPLIST);
	}

}
