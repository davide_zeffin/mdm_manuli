/*****************************************************************************
Class         ReadAddressDataAction
Copyright (c) 2002, SAP AG, Germany, All rights reserved.
Description:  Read user's addressdata for address change.
Created:      Aug 2002
Version:      1.0

$Revision 4 $
$Date: 2004/06/17 $
*****************************************************************************/
package com.sap.isa.isacore.action.b2b;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.Address;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.isacore.AddressFormular;
import com.sap.isa.isacore.action.ActionConstants;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.user.action.UserActions;
import com.sap.isa.user.businessobject.IsaUserBase;

/**
* Read user's addressdata for address change. 
*
* @author Adam Ebert
* @version 1.0
*
*/
public class ReadAddressDataAction extends IsaCoreBaseAction {

	public static final String RK_SOLDTO_LIST    = "soldtolist";
	public static final String RK_ADDRESSPARTNER = "addresspartner";
	public static final String RK_SHORTADDRESS   = "shortaddress";
	public static final String USER_SOLDTO       = "usersoldto";
	public static final String ADDR_TECHKEY      = "addrtechkey";

	/**
	* simple constructor
	*/
	public ReadAddressDataAction() {
	}
	
	/**
	 * Implement this method to add functionality to your action.
	 *
	 * @param form              The <code>FormBean</code> specified in the
	 *                          config.xml file for this action
	 * @param request           The request object
	 * @param response          The response object
	 * @param userSessionData   Object wrapping the session
	 * @param requestParser     Parser to simple retrieve data from the request
	 * @param bom               Reference to the BusinessObjectManager
	 * @param log               Reference to the IsaLocation, needed for logging
	 * @return Forward to another action or page
	 */
	public ActionForward isaPerform(ActionMapping mapping,
	        ActionForm form,
	        HttpServletRequest request,
	        HttpServletResponse response,
	        UserSessionData userSessionData,
	        RequestParser requestParser,
	        BusinessObjectManager bom,
	        IsaLocation log)
	        throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);     	                    	
	    boolean isDebugEnabled = log.isDebugEnabled();
	    
	    IsaUserBase user = bom.getUser();
	    user.clearMessages();
	    request.setAttribute(BusinessObjectBase.CONTEXT_NAME, user);
	
	    Shop shop = bom.getShop();
	    if (shop == null) {
			log.exiting();
	        throw new PanicException("shop.notFound");
	    }
	
	    BusinessPartnerManager bupama = bom.getBUPAManager();
	    BusinessPartner bupa = null;
	
	    // read SoldTos
        BusinessPartner contact = bupama.getDefaultBusinessPartner(PartnerFunctionData.CONTACT);
        ResultData result = bupama.getSoldTosBySalesArea(contact, shop.getId());

	    if ((result == null) || (result.getNumRows() <= 0)) {  
	        
	    	log.error(LogUtil.APPS_USER_INTERFACE, "no sold tos found!");
	        user.addMessage(new Message(Message.ERROR,"user.noSoldTo"));
			log.exiting();
	        return mapping.findForward("failure");
	    }
	    else {
	        if (isDebugEnabled) log.debug("soldtos found - num: " + result.getNumRows());
	        request.setAttribute(RK_SOLDTO_LIST, result);
	    }
	
	    // read user address
	    if(log.isDebugEnabled()) log.debug("read address from user " + user.getUserId());
	
	    ActionForward forwardBase = 
	            UserActions.performReadAddressData(mapping,
	                                               request,
	                                               requestParser,
	                                               user);
	                                              
	    Address address = (Address)request.getAttribute(UserActions.RC_ADDRESS);
		if (address == null) {  
	        
			log.error(LogUtil.APPS_USER_INTERFACE, "no address found!");
			user.addMessage(new Message(Message.ERROR,"userSettings.adrchange.readerr"));
			log.exiting();
			return mapping.findForward("failure");
		}
		
	    request.setAttribute(ADDR_TECHKEY, address.getTechKey().getIdAsString());
	    
	    // this is not necessary since the number is comming without leading zeros from the backend
	    /*
	    String address_mod = address.getAddressPartner();
	    // delete leading "0"
	    String endstring = "";
	    boolean parseEnd = false;
	    for(int i=0; i < address_mod.length(); i++){
	      
	        if( (address_mod.substring(i, i+1).equals("0")) && (!parseEnd)) {
	            endstring = address_mod.substring(i+1);	
	        }	
	        else{
	            parseEnd = true;
	        }
	    }
	    
	    // get Soldto and the ShortAddress:
	    request.setAttribute(RK_ADDRESSPARTNER, endstring);
	    */
	
	    request.setAttribute(RK_ADDRESSPARTNER, address.getAddressPartner());
	
	    if(log.isDebugEnabled()) log.debug("user-address-GUID: " + address.getTechKey().getIdAsString());
	    if(log.isDebugEnabled()) log.debug("user-address-addressPartner: " + address.getAddressPartner());
	    
	    bupa = bupama.getBusinessPartner(address.getAddressPartner());
	    if(bupa != null){
	    	if(log.isDebugEnabled()) log.debug("BusinessPartner as AddressPartner found");
	    	// read all short addresses for the selected sold-to
	    	ResultData shortAddresses = bupa.getAddressesShort();
	        if(log.isDebugEnabled()) log.debug("AddressPartner�s shortaddresses: " + shortAddresses.toString());
	        request.setAttribute(RK_SHORTADDRESS, shortAddresses);
	    }
	    else{
	        request.setAttribute(RK_SHORTADDRESS, null);
	        if(log.isDebugEnabled()) log.debug("NO Businesspartner for AddressPartner found! -> old addressformat?");
	    }
	
	    // get the forms of address supported in the shop from the backend.
	    ResultData formsOfAddress = shop.getTitleList();
	    // get the list of countries from the shop business object
	    ResultData listOfCountries = shop.getCountryList();
	    //set the default country to the value already in the user's SoldToAddress
	    String defaultCountryId = (address.getCountry());
	    // flag that indicates whether a region is needed to complete an address
	    boolean isRegionRequired = false;
	    // list of regions
	    ResultData listOfRegions = null;
	
	    // check whether the default country has a needs region to complete the address.
	    listOfCountries.beforeFirst();
	    while (listOfCountries.next()){
	        if (listOfCountries.getString("ID").equalsIgnoreCase(defaultCountryId)){
	            isRegionRequired = listOfCountries.getBoolean("REGION_FLAG");
	        }
	    }
	
	    // if region is needed then use Shop to get the list of regions
	    if (isRegionRequired){
	        listOfRegions = shop.getRegionList(defaultCountryId);
	    }
	
	    // address format id
	    int addressFormatId = shop.getAddressFormat();
	    // create a new AddressFormular instance
	    AddressFormular addressFormular = new AddressFormular((Address)request.getAttribute(UserActions.RC_ADDRESS), addressFormatId);
	    // this section sets the 'isPerson'-value and 'isRegionRequired'-value accordingly
	    if(shop.isTitleKeyForPerson(address.getTitleKey())) {
	        addressFormular.setPerson(true);
	    }
	    else {
	        addressFormular.setPerson(false);
	    }
	    addressFormular.setRegionRequired(isRegionRequired);
	
	    // set all the required data as request attributes and pass them to the addresschange.jsp
	    request.setAttribute(ActionConstants.RC_FORMS_OF_ADDRESS, formsOfAddress);
	    request.setAttribute(ActionConstants.RC_COUNTRY_LIST, listOfCountries);
	    if (isRegionRequired) {
	        request.setAttribute(ActionConstants.RC_REGION_LIST, listOfRegions);
	    }
	    request.setAttribute(ActionConstants.RC_ADDRESS_FORMULAR, addressFormular);
	
	    //get the list of counties if available
	    ResultData possibleCounties = shop.getTaxCodeList(address.getCountry(),
	                                                      address.getRegion(),
	                                                      address.getPostlCod1(),
	                                                      address.getCity());
	    request.setAttribute(ActionConstants.RC_POSSIBLE_COUNTIES, possibleCounties);
		log.exiting();
	    return forwardBase;
	    
	}
}
