/*****************************************************************************
    Class         SelectCatalogAction
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Action to display the Catalog List
    Author:       SAP
    Created:      April 2001
    Version:      1.0

    $Revision: #4 $
    $Date: 2001/07/26 $

*****************************************************************************/

package com.sap.isa.isacore.action.b2b;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.UserData;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.isacore.MessageDisplayer;
import com.sap.isa.isacore.action.IsaCoreInitAction;

/**
 * Display the list of Catalogs.
 *
 * If no catalog determination is activated, the forward readcatalog is used. 
 *
 *
 */
public class SelectCatalogAction extends CatalogReadListBaseAction {
	
    /**
     * Name of the lists of marketing attributes stored in the request context.
     */
    public static final String RK_CATALOG_LIST = "cataloglist";
	/**
	 * Flag to signal problems during auto entry process
	 */
	public static final String RK_AUTOENTRYERROR = "autoentryerror";

	private static final String FORWARD_AUTOENTRYERROR = "autoentryerror";

    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            BusinessObjectManager bom,
            IsaLocation log) {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        // Page that should be displayed next.
//        String forwardTo = null;
		  IsaCoreInitAction.StartupParameter startupParameter =
				  (IsaCoreInitAction.StartupParameter)userSessionData.getAttribute(IsaCoreInitAction.SC_STARTUP_PARAMETER);

        User user = bom.createUser();
        Shop shop = bom.getShop();
        BusinessPartnerManager buPaMa = bom.getBUPAManager();

        if (shop == null) {
			log.exiting();
            throw new PanicException("shop.notFound");
        }

        if (!shop.isCatalogDeterminationActived()) {
			log.exiting();		
            return mapping.findForward("readcatalog");
        }
        
//        BusinessPartner contact = buPaMa.getDefaultBusinessPartner(PartnerFunctionData.CONTACT);
//        BusinessPartner soldTo = buPaMa.getDefaultBusinessPartner(PartnerFunctionData.SOLDTO);
// 	upport of note 1062281

		BusinessPartner contact = null;                               
		BusinessPartner soldTo = null;                                
		if ( shop.getScenario().equals(ShopData.ORDER_ON_BEHALF) ) {  
	  		contact = buPaMa.getDefaultBusinessPartner(                 
							PartnerFunctionData.RESP_AT_PARTNER); 
	  		soldTo  = buPaMa.getDefaultBusinessPartner(                 
							PartnerFunctionData.RESELLER);        
		} else {                                                      
	  		contact = buPaMa.getDefaultBusinessPartner(                 
							PartnerFunctionData.CONTACT);         
	  		soldTo  = buPaMa.getDefaultBusinessPartner(                 
							PartnerFunctionData.SOLDTO);          
		}                                                             


        ResultData result = null;
        try {
             result = user.getCatalogs(shop.getTechKey().getIdAsString(), soldTo.getTechKey(), contact.getTechKey());
        }
        catch (Exception ex) {
			log.exiting();
          	return mapping.findForward("error");
        }

        if (result == null || result.getNumRows() <= 0) {

            MessageDisplayer messageDisplayer = new MessageDisplayer();
            messageDisplayer.setOnlyLogin();
            messageDisplayer.addMessage(new Message(Message.ERROR,"user.noCatalogs",null,null));
            messageDisplayer.addToRequest(request);
			log.exiting();
            return mapping.findForward("message");
        }
        else if (result.getNumRows() == 1) {

            log.debug(result.toString());
            // found exactly one shop, use it

            // move to first row
            result.absolute(1);

            // Save sold techKey in request
            request.setAttribute(ReadCatalogAction.PARAM_CATALOG, result.getString(UserData.RD_CATALOG_KEY));

            request.setAttribute(ReadCatalogAction.PARAM_VIEW, result.getObject(UserData.RD_CATALOG_VIEWS));

			log.exiting();
            return mapping.findForward("readcatalog");
        }
        else {
			if (startupParameter.getAutoEntry().equals("YES")) {
				// get the catalog ID from the startupParameter and use that ID 
				String catalogId = startupParameter.getCatalog();
				if ((catalogId != null) && (catalogId.length() > 0)) {
					for (int i = 1; i<= result.getNumRows(); i++){
						result.absolute(i);
						if (result.getString(UserData.RD_CATALOG_KEY).
									equalsIgnoreCase(catalogId)){
							request.setAttribute(
								ReadCatalogAction.PARAM_CATALOG,
								result.getString(UserData.RD_CATALOG_KEY));

							request.setAttribute(
								ReadCatalogAction.PARAM_VIEW,
								result.getObject(UserData.RD_CATALOG_VIEWS));

							log.exiting();
							return mapping.findForward("readcatalog");					
						}
					}
				} else { // no catalog ID used the first one as default
					// move to first row
					result.absolute(1);
					// Save sold techKey in request
					request.setAttribute(
						ReadCatalogAction.PARAM_CATALOG,
						result.getString(UserData.RD_CATALOG_KEY));

					request.setAttribute(
						ReadCatalogAction.PARAM_VIEW,
						result.getObject(UserData.RD_CATALOG_VIEWS));

					log.exiting();
					return mapping.findForward("readcatalog");

				}
				
				// Found more than one sold-to parties but autoentry requested (currently
				// used from Portal iViews). Since no shoplist can be presented, this
				// situation will lead to errorpage, should not happen here
				request.setAttribute(RK_AUTOENTRYERROR, "catdetermineerror");
				return mapping.findForward(FORWARD_AUTOENTRYERROR);
			}
            log.debug(result.toString());
            request.setAttribute(RK_CATALOG_LIST, result);
        }
		log.exiting();
        return mapping.findForward("selectcatalog");


   }

}
