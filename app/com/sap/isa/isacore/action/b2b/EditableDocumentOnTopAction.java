/*****************************************************************************
  Class:        EditableDocumentOnTopAction
  Copyright (c) 2002, SAP AG, All rights reserved.
  Author:
  Created:      25.11.2002
  Version:      1.0

  $Revision: #1 $
  $Date: 2002/11/25 $
*****************************************************************************/

package com.sap.isa.isacore.action.b2b;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.action.IsaCoreBaseAction;


/**
 * Action is used when minibasket button is clicked. The action brings the
 * editable document within the form input frame to front. If no editable 
 * document exists, a selection screen for document creation is displayed.
 * <p>
 * The following forwards are used by this action:
 * </p>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>FORWARD_SUCCESS ("success")</b></td>
 *     <td><b>Standard forward; used when editable document already exists</b></td>
 *   </tr>
 *   <tr>
 *     <td><b>FORWARD_NOEDITDOC ("noeditdoc")</b></td>
 *     <td><b>Used when not editable document exists and selection screen for 
 *            document creation should be displayed</b></td>
 *   </tr>
 *   <tr>
 *     <td><b>FORWARD_CREATEBASKET ("createBasket")</b></td>
 *     <td><b>Used in 'late decision' scenario where only document type 'basket' can be created. 
 *            Forward represents direct link to create basket action.</b></td>
 *   </tr>
 * </table>
 * 
 * @author SAP AG
 * @version 1.0
 *
 */



public class EditableDocumentOnTopAction extends IsaCoreBaseAction {
	
    /**
	 * Forward used, if editable document already exists, value is &quot;success&quot;.
	 */
    private static final String FORWARD_SUCCESS = "success";
    
    /**
	 * Forward used, if no editable document exists, value is &quot;noeditdoc&quot;.
	 */
    private static final String FORWARD_NOEDITDOC = "noeditdoc";
    
    /**
	 * Forward used, if no editable document exists and 'late decision' scenario is used.
	 * Value is &quot;createBasket&quot;.
	 */
    private static final String FORWARD_CREATEBASKET = "createBasket";
    
    /**
	 * Implement this method to add functionality to your action.
	 *
	 * @param form              The <code>FormBean</code> specified in the
	 *                           config.xml file for this action
	 * @param request           The request object
	 * @param response          The response object
	 * @param userSessionData   Object wrapping the session
	 * @param parser            RequestParser to simple retrieve data from the request
	 * @param bom               Reference to the BusinessObjectManager
	 * @param log               Reference to the IsaLocation, needed for logging
	 * @return Forward to another action or page
	 */
    public ActionForward isaPerform(ActionMapping mapping,
                ActionForm form,
                HttpServletRequest request,
                HttpServletResponse response,
                UserSessionData userSessionData,
                RequestParser parser,
                BusinessObjectManager bom,
                IsaLocation log)
                        throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
      // Page that should be displayed next.
      String forwardTo = null;
      
      boolean isDebugEnabled = log.isDebugEnabled();
      
      
      Shop shop = bom.getShop();
      if (shop == null) {
		log.exiting();
		  throw new PanicException("Error - No shop returned from bom");
		}
      
      DocumentHandler documentHandler = (DocumentHandler)
                userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
      if (documentHandler == null) {
			log.exiting();
			throw new PanicException("Error - No document handler found.");
		}

	  DocumentState editDoc = documentHandler.getDocument(DocumentState.TARGET_DOCUMENT);
	  
	  //if catalog on top => close catalog
	  if (documentHandler.isCatalogOnTop()) {
	  	documentHandler.setCatalogOnTop(false);
	  }
	  
	  if (documentHandler.setOnTop(editDoc)) {
	    forwardTo = FORWARD_SUCCESS;  	
	  }
	  else {
	  	//no editable document to display
	  	if (isDebugEnabled) { log.debug("Info: no editable document present - new document will be created."); }
        
        if (shop.isDocTypeLateDecision()) {
          // in the Late decision scenario, only a basket can be created so
          // forward directly to the CreateBasketAction
          forwardTo = FORWARD_CREATEBASKET;
        }
        else {
          // display selection for document creation
          // hier muss Abfrageparamter noch in SessionContext geschrieben werden
          request.setAttribute("reqsource", "minibasket");
          forwardTo = FORWARD_NOEDITDOC;
        }
	  }
	  log.exiting();
      return mapping.findForward(forwardTo);
  }
}