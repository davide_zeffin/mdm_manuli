/*****************************************************************************
  Class:        OrganizerContentAction
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       SAP
  Created:      12.06.2001
  Version:      1.0

  $Revision: #1 $
  $Date: 2001/06/26 $
*****************************************************************************/

package com.sap.isa.isacore.action.b2b;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.webcatalog.CatalogBusinessObjectManager;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 * Handles the display of the navigator frame in the organizer.
 * The actual organizer is stored within the DocumentHandler.
 * The return of the <code>getActualOrganizer<code> method is used 
 * as logical forward. 
 * See inlude organizer_menu.jsp.inc and config.xml for existing values.
 * 
 * @author SAP
 * @version 1.0
 * 
 * @see com.sap.isa.isacore.action.DocumentHandler
 */
public class OrganizerContentAction extends IsaCoreBaseAction {
    
    public static final String FWD_EXT_CAT_SEARCH = "catalogExtSearch";

    /**
     * Overriden <em>isaPerform</em> method of <em>IsaCoreBaseAction</em>.
     */
    public ActionForward isaPerform(ActionMapping mapping,
                ActionForm form,
                HttpServletRequest request,
                HttpServletResponse response,
                UserSessionData userSessionData,
                RequestParser parser,
                BusinessObjectManager bom,
                IsaLocation log)
                        throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);

         // Page that should be displayed next.
        String forwardTo = null;
        boolean extSearch = false;

        DocumentHandler documentHandler = (DocumentHandler)
                userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);

        // if catalog is on top, display the catalog tabstrip
        // else we try to display a document
        // if there is no document, we show a default page
        if (documentHandler.isCatalogOnTop()) {
            request.setAttribute("type", documentHandler.getCatalogType());
          
            CatalogBusinessObjectManager cbom = (CatalogBusinessObjectManager) userSessionData.getMBOM().getBOMbyName(CatalogBusinessObjectManager.CATALOG_BOM);

            WebCatInfo theCatalog = cbom.getCatalog();
          
            extSearch = theCatalog.isExtendedSearch();
        }
        
        if (extSearch) {
            forwardTo = FWD_EXT_CAT_SEARCH;
        }
        else {
            forwardTo = documentHandler.getActualOrganizer();
        }
      
		log.exiting();
        return mapping.findForward(forwardTo);
    }
}
