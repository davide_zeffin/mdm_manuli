/*****************************************************************************
    Class         GetDealerFamilyAction
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Description:  This action reads the list of related dealers from the dealer family.
    Author:       SAP AG
    Created:      08.01.2004
    Version:      0.1

    $Revision: #1 $
    $Date: 2004/01/08 $

*****************************************************************************/
package com.sap.isa.isacore.action.b2b;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businesspartner.action.GetPartnerHierarchyAction;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.action.IsaCoreBaseAction;


/**
 *  This action reads the list of related dealers from the dealer family.
 */
public class GetDealerFamilyAction extends IsaCoreBaseAction {
    /**
     * welcome.jsp should be displayed next
     */
    private static final String FORWARD_WELCOME = "welcome";

    /**
     * createdocument.jsp should be displayed next
     */
    private static final String FORWARD_CREATE_DOC = "createdoc";
    
    /**
     * createdocument.inc.jsp should be displayed next
     */
    private static final String FORWARD_CREATE_DOC_FROM_CAT = "createdocFromCat";

    /**
     * Name of the dealerFamily property in the request.
     */
    public static final String DEALER_FAMILY_LIST = "dealerFamilyList";

    /**
     * hom_welcome.jsp should be displayed next
     */
    private static final String FORWARD_HOM_WELCOME = "homWelcome";

    /**
    * Implement this method to add functionality to your action.
    *
    * @param form              The <code>FormBean</code> specified in the
    *                          config.xml file for this action
    * @param request           The request object
    * @param response          The response object
    * @param userSessionData   Object wrapping the session
    * @param requestParser     Parser to simple retrieve data from the request
    * @param bom               Reference to the BusinessObjectManager
    * @param log               Reference to the IsaLocation, needed for logging
    * @return Forward to another action or page
    */
    public ActionForward isaPerform(ActionMapping mapping, ActionForm form, HttpServletRequest request,
        HttpServletResponse response, UserSessionData userSessionData, RequestParser requestParser,
        BusinessObjectManager bom, IsaLocation log)
        throws CommunicationException {
        final String METHOD_NAME = "isaPerform()";
        log.entering(METHOD_NAME);

        // page that should be displayed next
        String PARAM_FORWARD = null;

        Shop shop = bom.getShop();

        if (shop == null) {
            log.exiting();
            throw new PanicException("shop.notFound");
        }

        ResultData dealerFamilyList = null;

        // forward to the hom_welcome.jsp
        if (shop.isHomActivated()) {
            PARAM_FORWARD = FORWARD_HOM_WELCOME;
        }
        else {
            if (shop.isBpHierarchyEnable()) {
                dealerFamilyList = GetPartnerHierarchyAction.getPartner(userSessionData, userSessionData.getMBOM());
            }

            request.setAttribute(DEALER_FAMILY_LIST, dealerFamilyList);

            // page that should be displayed next	  
            String next = requestParser.getParameter("next").getValue().getString();
            String source = requestParser.getAttribute("reqsource").getValue().getString();
            String forward = requestParser.getAttribute("forward").getValue().getString();

            if (((next != null) && next.equals("addToBasket")) ||
                    ((source != null) && source.equals("minibasket")) ||
                    ((forward != null) && forward.equals("orderstatusdetail")) ||
                    ((forward != null) && forward.equals("addToBasket")) ||
                    ((forward != null) && forward.equals("contractread"))) {
                if (isCatalogonTop(userSessionData)) {
                    PARAM_FORWARD = FORWARD_CREATE_DOC_FROM_CAT;
                }
                else {
                    PARAM_FORWARD = FORWARD_CREATE_DOC;
                }
            }
            else {
                PARAM_FORWARD = FORWARD_WELCOME;
            }
        }

        log.exiting();

        return mapping.findForward(PARAM_FORWARD);
    }
    
    /**
     * returns true if the catalog should be displayed
     * @return true, if the catalog should be displayed
     */
    protected boolean isCatalogonTop(UserSessionData userSessionData) {
        final String METHOD_NAME = "showCatalog()";
        log.entering(METHOD_NAME);
        
        boolean returnedValue = false;
        
        Shop shop = null;
        
        if (userSessionData != null) {

            // get BOM
            BusinessObjectManager bom = (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
    
    
            // now I can read the shop
            shop = bom.getShop();
        }

        DocumentHandler documentHandler = (DocumentHandler)
                userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);

        if (documentHandler.isCatalogOnTop()) {
            returnedValue = true;
        }

        log.exiting();
        return returnedValue;
    }
}
