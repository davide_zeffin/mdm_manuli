/*****************************************************************************
    Class         CEShowProductDetailAction
    Copyright (c) 2002, SAP AG, Germany, All rights reserved.
    Description:  Action for the Profile Maintenance
    Author:       Timo Kohr
    Created:      01 M�rz 2002
    Version:      0.1

    $Revision: #2 $
    $Date: 2002/03/19 $

*****************************************************************************/

package com.sap.isa.isacore.action.b2b;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Product;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItemList;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.action.ActionConstants;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 *  This action start's the catalog with an specific item:
 */
public class CEShowProductDetailAction extends IsaCoreBaseAction {

    /**
     * String constant specifying if auctions are enabled.
     */
    private static final String PARAM_FORWARD           = "success";
    private static final String PARAM_PRODUCT_ID        = "productId";
    private static final String PARAM_PRODUCT        = "product";
    private static final String PARAM_PRODUCT_AREA      = "productArea";
    private static final String PARAM_PRT_OBJECT_ID	  = "CRM_OBJECT_ID";
    private static final String PARAM_PRODUCT_SCENARIO  = "productScenario";
	private static final String NO_PRODUCT_FOUND 	  = "noProductFound";
    private static final String PARAM_CATALOG_REFRESH   = "showProduct";
	

      /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            BusinessObjectManager bom,
            IsaLocation log)
            throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        HttpSession session = request.getSession();

        // create a webcatitemlist, because it is need in catalog actions
        WebCatInfo catalog =
                getCatalogBusinessObjectManager(userSessionData).getCatalog();

        if (catalog == null) {
			log.exiting();
            throw new PanicException("catalog.notFound");
        }

        String areaId = request.getParameter(PARAM_PRODUCT_AREA);
        String itemId = request.getParameter(PARAM_PRODUCT_ID);
        String item = request.getParameter(PARAM_PRODUCT);
       	String prtObjectId = request.getParameter(PARAM_PRT_OBJECT_ID);
        
        if (areaId != null && !areaId.equals("") ) {
			// set the appropriate attributes to get
			// a product detail from the catalog
			session.setAttribute(PARAM_PRODUCT_ID,itemId);
			session.setAttribute(PARAM_PRODUCT_AREA, areaId);
			session.setAttribute(PARAM_PRODUCT_SCENARIO, ActionConstants.DS_IVIEW);        	
        }
        else { 
            // Enhancement for Portal KM iView to find a product with only an ID (e.g. HT-1010)
            if (item != null && item.length() > 0) {
                String[] itemIDs = new String[1];
                itemIDs[0] = item;
                
                WebCatItemList itemList = catalog.getItemListFromID(itemIDs );
                
                if (itemList.size() > 0) {
                    prtObjectId = itemList.getItem(0).getProductID(); 
                }
            }
            if (prtObjectId != null && !prtObjectId.equals("")) {
                Product product = 
                    new Product( new TechKey(prtObjectId));
                
                if (product.enhance(catalog)) {
                    session.setAttribute(PARAM_PRODUCT_ID,product.getItemId().toString());
                    session.setAttribute(PARAM_PRODUCT_AREA, product.getArea().toString());
                    session.setAttribute(PARAM_PRODUCT_SCENARIO, ActionConstants.DS_IVIEW);                      
                } 
                else {
                    session.setAttribute(PARAM_PRODUCT_SCENARIO, NO_PRODUCT_FOUND);
                }   
            }
        }

        // set the forward for the catalog
        catalog.setLastVisited(PARAM_CATALOG_REFRESH);

        // set the documentHandler to get the catalog on top
        DocumentHandler documentHandler = (DocumentHandler)
            userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
        documentHandler.setCatalogOnTop(true);
		log.exiting();
        return mapping.findForward(PARAM_FORWARD);

    }
}