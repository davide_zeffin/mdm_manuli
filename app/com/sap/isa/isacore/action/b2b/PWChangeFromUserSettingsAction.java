/*****************************************************************************
    Class         PWChangeFromUserSettingsAction
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Action for the Profile Maintenance
    Author:       Wolfgang Sattler
    Created:      27 Februar 2001
    Version:      0.1

    $Revision: #3 $
    $Date: 2003/01/10 $

*****************************************************************************/

package com.sap.isa.isacore.action.b2b;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.User;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 *  This action set only the request attribute FORWARD_NAME for the pwchange.jsp
 */
public class PWChangeFromUserSettingsAction extends IsaCoreBaseAction {
	
    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            BusinessObjectManager bom,
            IsaLocation log)
            throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        User user = bom.getUser();

        user.clearMessages();
        request.setAttribute(BusinessObjectBase.CONTEXT_NAME, user);

        request.setAttribute(PWChangeAction.ACTION_NAME,PWChangeAction.ACTION_PW);
        request.setAttribute(PWChangeAction.FORWARD_NAME,PWChangeAction.FORWARD_NAME_SUCCESS);
		log.exiting();
        return mapping.findForward("success");
    }
}

