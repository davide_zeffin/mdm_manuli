/*****************************************************************************
    Class         InitB2BAction
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  B2B specific initializations
    Author:
    Created:      23.04.2001
    Version:      1.0

    $Revision: #4 $
    $Date: 2002/03/21 $
*****************************************************************************/
package com.sap.isa.isacore.action.b2b;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.order.CollectiveOrder;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.businesspartner.businessobject.PartnerFunctionBase;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.interaction.InteractionConfig;
import com.sap.isa.core.interaction.InteractionConfigContainer;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.StartupParameter;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.core.util.table.TableRow;
import com.sap.isa.core.xcm.init.ExtendedConfigInitHandler;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.History;
import com.sap.isa.isacore.HistoryCookieLoader;
import com.sap.isa.isacore.MessageDisplayer;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.action.ShopReadAction;
import com.sap.isa.isacore.action.order.hom.HomSessionListner;


/**
 * Performs all initializations specific to the B2B application.
 */
public class InitB2BAction extends IsaCoreBaseAction {
	
    /**
     * Performs all initializations specific to the B2B application.
     *
     */
    public ActionForward isaPerform(ActionMapping mapping,
                ActionForm form,
                HttpServletRequest request,
                HttpServletResponse response,
                UserSessionData userSessionData,
                RequestParser requestParser,
                BusinessObjectManager bom,
                IsaLocation log) throws CommunicationException {

		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
		// Check for the existence of the Document handler and create it, if
		// not present
        DocumentHandler documentHandler = (DocumentHandler)
                userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);

        if (documentHandler == null) {
            documentHandler = new DocumentHandler();
            userSessionData.setAttribute(SessionConst.DOCUMENT_HANDLER,
                    documentHandler);
            // the history will be stored in a cookie
            History history = documentHandler.getHistory();
            if (history != null) {
              HistoryCookieLoader historyLoader = new HistoryCookieLoader();
              history.setLoader(historyLoader);
            }
        }
        
        //do we need to display the auction page?
		StartupParameter startupParameter = (StartupParameter) userSessionData.getAttribute(SessionConst.STARTUP_PARAMETER);
		if(startupParameter != null &&
		   startupParameter.getParameter("_auctionId") != null &&
		   !startupParameter.getParameterValue("_auctionId").equals("") && 
		   documentHandler != null)  {
		   	 documentHandler.setCatalogOnTop(true);
		}
								   
        Shop shop = bom.getShop();
        if (shop == null) {
			log.exiting();
            throw new PanicException("shop.notFound");
        }
        
        shop.setShowCatATP(true);
        shop.setShowCatAVWLink(true);
        shop.setShowCatCompToSim(true);
        shop.setShowCatContracts(true);
        shop.setShowCatCUAButtons(true);
        shop.setShowCatExchProds(true);
        shop.setShowCatMultiSelect(true);
        shop.setShowCatProdCateg(true);
        shop.setShowCatPersRecommend(true);
		shop.setShowCatSpecialOffers(true);
        shop.setShowCatListCUALink(true);
        shop.setShowCatQuickSearchInCat(true);
        shop.setSuppCatEventCapturing(true);
        
        // get partner function for order
        BusinessPartnerManager buPaMa = bom.getBUPAManager();

		String soldtoId = "";
		if (buPaMa.getDefaultBusinessPartner(PartnerFunctionBase.SOLDTO) != null) {
			soldtoId = buPaMa.getDefaultBusinessPartner(PartnerFunctionBase.SOLDTO).getId();        
		}

		// read customizing for contract negotiation
        if (shop.isContractNegotiationAllowed()) {
 			// read the process types for contract negotiations from the backend       	
 			
 			shop.readContractNegotiationProcessTypes(soldtoId);
            userSessionData.setAttribute(ShopReadAction.CONTRACT_NEGOTIATION_PROCESS_TYPES, shop.getContractNegotiationProcessTypes());    
        }
        
        // read customizing for quotations
        if (shop.isQuotationExtended()) {
        	if (shop.isQuotationGroupSelected()) {
        		// read the process types for quotations from backend
        		shop.readQuotationProcessTypes(soldtoId);
        	} else {
        		// read the quotation process type and store it in the ResultSet
        		if (shop.getQuotationProcessType() != null) {
					Table table = new Table("quotationProcessTypeGroup");
					table.addColumn(Table.TYPE_STRING, ShopData.ID);
					table.addColumn(Table.TYPE_STRING, ShopData.DESCRIPTION);
					table.addColumn(Table.TYPE_STRING, ShopData.CONTRACT_TYPE);
					TableRow row = table.insertRow();	
					row.setStringValues(
							new String[] { shop.getQuotationProcessType(), "", "" });
					shop.setQuotationProcessTypes(new ResultData(table));	
        		}		
        	}
        	userSessionData.setAttribute(ShopReadAction.QUOTATION_PROCESS_TYPES, shop.getQuotationProcessTypes());
        }
        
        if (shop.isOrderGroupSelected()) {
			// read the process types from backend
			shop.readProcessTypes(soldtoId);
        } else {
        	// read the process type and store it in the ResultSet
        	if (shop.getProcessType() != null) {
				Table table = new Table("orderProcessTypeGroup");
				table.addColumn(Table.TYPE_STRING, ShopData.ID);
				table.addColumn(Table.TYPE_STRING, ShopData.DESCRIPTION);
				table.addColumn(Table.TYPE_STRING, ShopData.CONTRACT_TYPE);
				TableRow row = table.insertRow();	
				row.setStringValues(
						new String[] { shop.getProcessType(), "", "" });
				shop.setProcessTypes(new ResultData(table));
        	} 
        }
		userSessionData.setAttribute(ShopReadAction.PROCESS_TYPES, shop.getProcessTypes());

        // By default set old document search
        documentHandler.setOrganizerParameter("docsearchversion", "OLD");
		String genericSearchName = null;
        if (ExtendedConfigInitHandler.isActive()) {
            InteractionConfigContainer interactionConfigData = getInteractionConfig(request);
            InteractionConfig iac = interactionConfigData.getConfig("shop");
            if (iac != null) {
                genericSearchName = iac.getValue("documentsearch.name");
            }
            if (genericSearchName != null) {            
                // set new document search
                documentHandler.setOrganizerParameter("docsearchversion", "NEW");
                documentHandler.setOrganizerParameter("documentsearch.name", genericSearchName);
                documentHandler.setOrganizerParameter("initdocsearch.name", genericSearchName);
                documentHandler.setOrganizerParameter("genericsearch.name", genericSearchName);
                documentHandler.setActualOrganizer("genericdocsearch");
            }
            
            // set customer search
            String genericCustomerSearchName = null;
			if (iac != null) {
				genericCustomerSearchName = iac.getValue("customersearch.name");
			}
			if (genericCustomerSearchName != null) {            											
				documentHandler.setOrganizerParameter("genericcustsearch.name", genericCustomerSearchName);				
			}
			                
        }
		           
        if (shop.isHomActivated()) {
            documentHandler.setActualOrganizer("customerdocsearch");
            if (genericSearchName == null) {
				genericSearchName = "SearchCriteria_HOM_Sales";
				// set new document search
				documentHandler.setOrganizerParameter("docsearchversion", "NEW");
				documentHandler.setOrganizerParameter("documentsearch.name", genericSearchName);
				documentHandler.setOrganizerParameter("initdocsearch.name", genericSearchName);
				documentHandler.setOrganizerParameter("genericsearch.name", genericSearchName);
				documentHandler.setActualOrganizer("genericdocsearch");
            }
 
            // get the contact person
            TechKey contact = buPaMa.getDefaultBusinessPartner(PartnerFunctionBase.CONTACT).getTechKey();
            
            TechKey soldTo = buPaMa.getDefaultBusinessPartner(PartnerFunctionBase.SOLDTO).getTechKey();

            CollectiveOrder collectiveOrder = bom.createCollectiveOrder();

            collectiveOrder.enqueue(contact, soldTo);
            
            if (!collectiveOrder.isValid()) {
              
                MessageDisplayer messageDisplayer = new MessageDisplayer();
                messageDisplayer.setOnlyLogin();
                messageDisplayer.addMessage(new Message(Message.ERROR,"hom.user.loggedinAlready"));
                messageDisplayer.addToRequest(request);
				log.exiting();
                return mapping.findForward("message");
            } 
                        
            HomSessionListner sessionListner = new HomSessionListner(bom, contact, soldTo);

            // register a listner to unlock object if the session time out
            request.getSession().setAttribute(HomSessionListner.class.getName(),sessionListner);

        }
        String branchout = (String)userSessionData.getAttribute("branchout");
        if (branchout != null){
        	if(branchout.equalsIgnoreCase("YES")){
        		userSessionData.setAttribute("branchout", "");
				log.exiting();
				return mapping.findForward("branchout");
        	}
        }
		log.exiting();
        return mapping.findForward("success");
    }


}