/**
 *  Class:        UpdateDocumentViewAction
 *  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
 *  Author:       Franz-Dieter Berger
 *  Created:      09.05.2001
 *  Version:      1.0
 *  $Revision: #4 $
 *  $Date: 2001/08/01 $
 */

package com.sap.isa.isacore.action.b2b;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.order.Basket;
import com.sap.isa.businessobject.order.Order;
import com.sap.isa.businessobject.order.OrderChange;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.StartupParameter;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.MessageDocument;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.action.order.MaintainGridBaseAction;
/**
 *  The UpdateDocumentViewAction is responsible for displaying the <em>form_input</em>,
 *  the <em>documents</em> and the <em>_history</em> frame. It uses the DocumentHandler
 *  for to determine what should be displayed.
 * The following forwards are used by this action:
 * </p>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>FORWARD_NODOC</td><td>If neither the catalog nor a document should be displayed.</td></tr>
 *   <tr><td>FORWARD_CATALOG</td><td>If the catalg should be displayed.</td></tr>
 *   <tr><td>FORWARD_FAILURE</td><td>If a document should be displayed, but the corresponding ManagedDocument has no forward.</td></tr>
 * </table>
 * If a document should be displayed it uses the forward attribute of the corresponding ManagedDocument.
 *
 * @see DocumentHandler
 * @see ManagedDocument
 *
 *@author     Franz-Dieter Berger
 *@created    6. Dezember 2001
 *@version    1.0
 */
public class UpdateDocumentViewAction extends IsaCoreBaseAction {

    private final static String CAT_TYPE = "type";

    /**
     * Constant for available forwards, value is &quot;nodoc&quot;.
     */
    protected final static String FORWARD_NODOC = "nodoc";

    /**
     * Constant for available forwards, value is &quot;catalog&quot;.
     */
    protected final static String FORWARD_CATALOG = "catalog";
	
	/**
	 * Constant to identify a grid product when clicked on grid link in catalog
	 */
	protected static final String IS_GRID_ITEM = "isGridItem";
    /**
     * Constant for available forwards, value is &quot;failure&quot;.
     */
    protected final static String FORWARD_FAILURE = "failure";

    /**
     * Request context constant, value is &quot;welcome&quot;. Is put in the request context,
     * if the forward is FORWARD_NODOC and the welcomepage should be shown instead of the default page.
     */
    public final static String WELCOME = "welcome";
	/**
	 * If the application is logged in with auctionId, then show the auction details 
	 * instead of the welcome.jsp 
	 */
	protected final static String AUCTION	= "auction"; 
    /**
     *  Overriden  <em> isaPerform </em>  method of  <em> IsaCoreBaseAction </em>.
     *
     *
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser parser,
            BusinessObjectManager bom,
            IsaLocation log)
             throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        String forwardTo = null;

        String logPrefix = null;
        boolean isDebugEnabled = log.isDebugEnabled();
        if (isDebugEnabled) {
            logPrefix = "isaPerform(): ";
        }
        
        //Is the item clicked is a Grid item and clicked from Catalog
		String isGridItem = (String)request.getParameter(IS_GRID_ITEM);
		
			
        DocumentHandler documentHandler = (DocumentHandler)
                userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);

        if (documentHandler == null) {
			log.exiting();
            throw new PanicException("No document handler found.");
        }

		
		// if the DocumentHandler contains a MessageDocument, the message is always displayed on top,
		// otherwise: previous process
		
		if (documentHandler.getMessageDocument() != null) {
        			
			MessageDocument messageDocument = documentHandler.getMessageDocument();            
            request.setAttribute("messageJsp", messageDocument.getMessageJSP());
			messageDocument.preDisplayHook();
			
			log.exiting();
			return mapping.findForward(messageDocument.getForward());

		} else {  // no message document, previous process

        // if catalog is on top, we display the catalog
        // else we try to display a document
        // if there is no document, we show a default page
       
        //for GRID PRODUCTS: need to display IPC Grid screen instead of displaying the catalog
        if (documentHandler.isCatalogOnTop() &&
			(isGridItem == null || isGridItem.length() <= 0 ) ) {
				
            request.setAttribute(CAT_TYPE, documentHandler.getCatalogType());
            forwardTo = FORWARD_CATALOG;
            if (isDebugEnabled) {
                log.debug(logPrefix + "Display the catalog, forward: " + forwardTo);
            }
        }
        else {
            ManagedDocument mdoc = documentHandler.getManagedDocumentOnTop();           
            if (mdoc != null) { 		           	
                forwardTo = mdoc.getForwardMapping();
                if (forwardTo == null) {
                    // this means, that the document will not be displayed
                    forwardTo = FORWARD_FAILURE;
                    if (isDebugEnabled) {
                        log.debug(logPrefix + "Tried to display a document, but there is no forward in the ManagedDocument. For this forward: " + forwardTo);
                    }
                }
                else {
					//If it is a grid product and one of the buttons(prev, grid, newprod)
					//is clicked in the grid screen, then forward to grid related process
					if ( ( request.getParameter(MaintainGridBaseAction.GRID_PREV) != null &&
				   		   request.getParameter(MaintainGridBaseAction.GRID_PREV).length() > 0 ) ||
				 		 ( request.getParameter(MaintainGridBaseAction.GRID_NEXT) != null &&
				   		   request.getParameter(MaintainGridBaseAction.GRID_NEXT).length() > 0 ) ||
				 	   	 ( request.getParameter(MaintainGridBaseAction.GRID_NEW_PROD) != null &&
				   		   request.getParameter(MaintainGridBaseAction.GRID_NEW_PROD).length() > 0 ) ) {
				   		
				   		//required to avoid infinite loop
				   		if (request.getAttribute("gridnav") != null) {
				   			request.removeAttribute("gridnav");
				   		}else{
				   			forwardTo = (MaintainGridBaseAction.GRID).concat(forwardTo);
				   		}
                	}
                	
                	//required for Multiple Shipto functionality
                	if (request.getParameter("multipleshipto") != null &&
					    request.getParameter("multipleshipto").length() > 0){
					    	
					    userSessionData.setAttribute("itemshipto", request.getParameter("multipleshipto"));
						forwardTo = "multiplegridfunc";
					}
					//required for Multiple Requested Delivery Date functionality
					if (request.getParameter("multiplereqdlvdate") != null &&
					    request.getParameter("multiplereqdlvdate").length() > 0){
					    
						userSessionData.setAttribute("itemreqdlvdate", request.getParameter("multiplereqdlvdate"));
						forwardTo = "multiplegridfunc";
					}
					
                    if (isDebugEnabled) {
                        log.debug(logPrefix + "Display a document, forward: " + forwardTo);
                    }
                }
            }
            else {
				//If the application is logged in with auctionId as the parameter
				//Show the auction details
				//This parameter will be removed from the startup object after the first
				//time, it has been shown to the user
				  StartupParameter startupParameter = (StartupParameter)
						  userSessionData.getAttribute(SessionConst.STARTUP_PARAMETER);
				  if(startupParameter != null 
						  && startupParameter.getParameter("_auctionId") != null
						  && !startupParameter.getParameterValue("_auctionId").equals(""))  {
					  return mapping.findForward(AUCTION);			
				  }
                // there is no document for displaying
                if (documentHandler.showWelcome()) {
                    request.setAttribute(WELCOME, "1");
                    if (isDebugEnabled) {
                        log.debug(logPrefix + "Default page is the welcome page.");
                    }
                }
                forwardTo = FORWARD_NODOC;
                if (isDebugEnabled) {
                    log.debug(logPrefix + "Display a default page, forward: " + forwardTo);
                }
            }
        }
        
        if ( forwardTo.equals("order_change") || forwardTo.equals("order_status")){
        	if(userSessionData.getAttribute("auctionFromCat") != null && userSessionData.getAttribute("auctionFromCat").equals("true")){
        		userSessionData.setAttribute("auctionFromCat","false");
	        	forwardTo = forwardTo + "_from_cat";
			}
        }
      
       } //if (documentHandler.getMessageDocument() != null) 
        
        
	   log.exiting();
        return mapping.findForward(forwardTo);
    }
}
