/**
 * Class:        LoginAction Copyright (c) 2001, SAP AG, All rights reserved. Author:       SAP AG Created:
 * 22.03.2001 Version:      1.0 $Revision: #6 $ $Date: 2003/10/16 $
 */
package com.sap.isa.isacore.action.b2b;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.User;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.businesspartner.businessobject.Contact;
import com.sap.isa.core.Constants;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.interaction.InteractionConfigContainer;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.StartupParameter;
import com.sap.isa.core.xcm.init.ExtendedConfigInitHandler;
import com.sap.isa.isacore.action.ActionConstants;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.action.IsaCoreInitAction;
import com.sap.isa.user.util.LoginStatus;
import com.sap.security.api.IUser;
import com.sap.security.api.UMFactory;


/**
 * Logs user into the backend system. This is done using the appropiate business object. If the login fails an error
 * message is displayed and the user has another chance to log in. After the login is successful, a list of sold to
 * parties is displayed, if more than one party is present.<br>
 * Note that all Action classes have to provide a non argument constructor because they were instantiated
 * automatically by the action servlet.
 *
 * @author SAP
 * @version 1.0
 */
public class LoginAction extends IsaCoreBaseAction {
    /** Parameter name for userId. */
    public static final String PN_USERID = "UserId";

    /** Parameter name for password. */
    public static final String PN_PASSWORD = "nolog_password";

    /** Parameter name for password change. */
    public static final String PN_PASSWORD_CHANGE = "changePassword";

    /** Parameter name for password change. */
    public static final String PN_LOGIN = "login";

    /**
     * Security Check.
     */
    public void initialize() {
        if (isB2B()) {
            this.checkUserIsLoggedIn = false;
        }
    }

    /**
     * Implement this method to add functionality to your action.
     *
     * @param mapping DOCUMENT ME!
     * @param form The <code>FormBean</code> specified in the config.xml file for this action
     * @param request The request object
     * @param response The response object
     * @param userSessionData Object wrapping the session
     * @param requestParser Parser to simple retrieve data from the request
     * @param bom Reference to the BusinessObjectManager
     * @param log Reference to the IsaLocation, needed for logging
     * @param startupParameter Object containing the startup parameters
     * @param eventHandler Object to capture events with
     *
     * @return Forward to another action or page
     *
     * @throws CommunicationException DOCUMENT ME!
     */
    public ActionForward isaPerform(ActionMapping                      mapping,
                                    ActionForm                         form,
                                    HttpServletRequest                 request,
                                    HttpServletResponse                response,
                                    UserSessionData                    userSessionData,
                                    RequestParser                      requestParser,
                                    BusinessObjectManager              bom,
                                    IsaLocation                        log,
                                    IsaCoreInitAction.StartupParameter startupParameter,
                                    BusinessEventHandler               eventHandler)
                             throws CommunicationException {
                                 
        final String METHOD_NAME = "isaPerform()";
        log.entering(METHOD_NAME);

        // Page that should be displayed next.
        String forwardTo = null;

        LoginStatus i = LoginStatus.NOT_OK;

        if (log.isDebugEnabled()) {
            log.debug("start");
        }

        // Set username and password for the user object. The data for this is
        // stored in the LoginForm object. This object was filled with the forms
        // data by the action servlet.
        // Then call the login
        // method to tell the user object that it should perform a login.
        // The login method will return true, if the login was successful or
        // otherwise false. If the login will be unsuccessful return to the
        // login screen and show an error message.
        String userId = requestParser.getParameter(PN_USERID).getValue().getString();

        if (!requestParser.getParameter(PN_USERID).getValue().isSet()) {
            // if userId isn't given as parameter, try attribute
            userId = requestParser.getAttribute(PN_USERID).getValue().getString();
        }

        String password = requestParser.getParameter(PN_PASSWORD).getValue().getString();

        if (!requestParser.getParameter(PN_PASSWORD).getValue().isSet()) {
            // if password isn't given as parameter, try attribute
            password = requestParser.getAttribute(PN_PASSWORD).getValue().getString();
        }

        if (log.isDebugEnabled()) {
            log.debug(userId);
        }

        User user = bom.createUser();

        BusinessPartnerManager buPaMa = bom.createBUPAManager();

        i = user.login(userId, password);

        request.setAttribute(BusinessObjectBase.CONTEXT_NAME, user);

        if (log.isDebugEnabled()) {
            log.debug(String.valueOf(i));
        }

        if (i == LoginStatus.OK) {
            // The user was successfully logged into the system.
            if (log.isDebugEnabled()) {
                log.debug("userok");
            }

            BusinessPartner partner = buPaMa.createBusinessPartner(user.getBusinessPartner(), null, null);

            Contact contact = new Contact();
            partner.addPartnerFunction(contact);
            buPaMa.setDefaultBusinessPartner(partner.getTechKey(), contact.getName());

            if (requestParser.getParameter(PN_PASSWORD_CHANGE).isSet()) {
                request.setAttribute(PWChangeAction.FORWARD_NAME, PWChangeAction.FORWARD_NAME_SUCCESS);
                request.setAttribute(PWChangeAction.ACTION_NAME, PWChangeAction.ACTION_PW);
                forwardTo = "pwchange";
            }
            else {
                // Read the shoplist and display it. Later on we will
                // display a personalized shoplist.
                forwardTo = "success";
            }
        }

        else if (i == LoginStatus.NOT_OK) {
            if (log.isDebugEnabled()) {
                log.debug("user not ok");
            }

            forwardTo = "failure";

            // get StartupParameter to check if UME login should be provided
            StartupParameter startupCoreParameter = (StartupParameter) userSessionData.getAttribute(SessionConst.STARTUP_PARAMETER);

            if (startupCoreParameter != null) {
                String umeloginParameter = startupCoreParameter.getParameterValue(Constants.UMELOGIN);

                if ((umeloginParameter.length() > 0) && umeloginParameter.equals("YES")) {
                    // XCM ?
                    if (ExtendedConfigInitHandler.isActive()) {
                        if (log.isDebugEnabled()) {
                            log.debug("XCM is active ...");
                        }

                        // use UME ? 
                        // get InteractionConfigContainer
                        InteractionConfigContainer interactionConfigData = getInteractionConfig(request);
                        String umeEnabled = interactionConfigData.getConfig(ActionConstants.UME_INTERACTION_CONFID)
                                                                 .getValue(ActionConstants.UME_INTERACTION_CONFVALUE);

                        if (log.isDebugEnabled()) {
                            log.debug("UME-Enabling: " + umeEnabled);
                        }

                        if ((umeEnabled != null) && umeEnabled.equals("true")) {
                            IUser userUME = user.getUserUME();

                            if (userUME != null) {
                                UMFactory.getAuthenticator().logout(request, response);

                                if (log.isDebugEnabled()) {
                                    log.debug("logout UME user");
                                }
                            }

                            forwardTo = "failureUME";
                        }
                    }
                }
            }
        }

        else if (i == LoginStatus.NOT_OK_NEW_PASSWORD) {
            if (log.isDebugEnabled()) {
                log.debug("user switch");
            }

            // set parameter to set the correct forward for pwchange action
            request.setAttribute(PWChangeAction.FORWARD_NAME, PWChangeAction.FORWARD_NAME_SUCCESS);
            request.setAttribute(PWChangeAction.ACTION_NAME, PWChangeAction.ACTION_US);
            forwardTo = "pwchange";
        }

        else if (i == LoginStatus.NOT_OK_PASSWORD_EXPIRED) {
            if (log.isDebugEnabled()) {
                log.debug("expired password");
            }

            // set parameter to set the correct forward for pwchange action
            request.setAttribute(PWChangeAction.FORWARD_NAME, PWChangeAction.FORWARD_NAME_SUCCESS);
            request.setAttribute(PWChangeAction.ACTION_NAME, PWChangeAction.ACTION_EP);
            forwardTo = "pwchange";
        }

        else {
            if (log.isDebugEnabled()) {
                log.debug("i error");
            }

            forwardTo = "error";
        }

        if (log.isDebugEnabled()) {
            log.debug(forwardTo);
            log.debug("finish");
        }

        log.exiting();

        return mapping.findForward(forwardTo);
    }
}
