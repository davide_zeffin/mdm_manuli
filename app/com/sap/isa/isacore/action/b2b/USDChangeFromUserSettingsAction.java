/*
 * Created on 20.01.2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.isacore.action.b2b;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.User;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;


/**
 * @author d031177
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class USDChangeFromUserSettingsAction extends IsaCoreBaseAction {
	

	/**
	 * Implement this method to add functionality to your action.
	 *
	 * @param form              The <code>FormBean</code> specified in the
	 *                          config.xml file for this action
	 * @param request           The request object
	 * @param response          The response object
	 * @param userSessionData   Object wrapping the session
	 * @param requestParser     Parser to simple retrieve data from the request
	 * @param bom               Reference to the BusinessObjectManager
	 * @param log               Reference to the IsaLocation, needed for logging
	 * @return Forward to another action or page
	 */
	public ActionForward isaPerform(ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response,
			UserSessionData userSessionData,
			RequestParser requestParser,
			BusinessObjectManager bom,
			IsaLocation log)
			throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
		User user = bom.getUser();

		user.clearMessages();
		request.setAttribute(BusinessObjectBase.CONTEXT_NAME, user);
		
		String uiMode = "";
		if ((userSessionData.getAttribute(USDChangeAction.UIMODE) != null && 
			userSessionData.getAttribute(USDChangeAction.UIMODE).toString().length() > 0)){
				uiMode = userSessionData.getAttribute(USDChangeAction.UIMODE).toString();	
		}
		
		
		request.setAttribute(USDChangeAction.GRID_PRICE, "");
		request.setAttribute(USDChangeAction.GRID_STOCK, "");
		
		userSessionData.setAttribute(USDChangeAction.GRID_PRICE, "");
		userSessionData.setAttribute(USDChangeAction.GRID_STOCK, "");
		
		
		if(uiMode.equals(USDChangeAction.PRICE_DISPLAY) ||uiMode.equals(USDChangeAction.PRICE_STOCK_DISPLAY)){
			request.setAttribute(USDChangeAction.GRID_PRICE, USDChangeAction.CHECKED);
			userSessionData.setAttribute(USDChangeAction.GRID_PRICE, USDChangeAction.CHECKED);
		}
		if (uiMode.equals(USDChangeAction.STOCK_DISPLAY) || uiMode.equals(USDChangeAction.PRICE_STOCK_DISPLAY)){
			request.setAttribute(USDChangeAction.GRID_STOCK, USDChangeAction.CHECKED);
			userSessionData.setAttribute(USDChangeAction.GRID_STOCK, USDChangeAction.CHECKED);
		}
		
		log.exiting();
		return mapping.findForward("success");
	}
}
