/**
 * Class         SelectSoldToAction Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
 * Description:  Action to display the SoldToList Author:       SAPMarkets Created:      March 2001 Version:      0.1
 * $Revision: #3 $ $Date: 2001/08/03 $
 */
package com.sap.isa.isacore.action.b2b;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.isacore.action.IsaCoreInitAction;


/**
 * Display the list of SoldTos <br>
 * Note that all Action classes have to provide a non argument constructor because they were instantiated
 * automatically by the action servlet. Auto entry integration, where if requested by startup parameter
 * <code>autoentry</code>, first sold to of sold to list will be used.
 *
 * @author SAP
 * @version 1.0
 */
public class SelectSoldToAction extends SoldToReadListBaseAction {
    /** Name of the lists of marketing attributes stored in the request context. */
    public static final String RK_SOLDTO_LIST = "soldtolist";

    /** Flag to signal problems during auto entry process */
    public static final String RK_AUTOENTRYERROR = "autoentryerror";

    /** Name of the request attribute to get additional infos about the problem */
    public static final String RK_AUTOENTRYADDINFO = "autoentryaddinfo";
    private static final String FORWARD_AUTOENTRYERROR = "autoentryerror";

    /**
     * Implement this method to add functionality to your action.
     *
     * @param mapping DOCUMENT ME!
     * @param form The <code>FormBean</code> specified in the config.xml file for this action
     * @param request The request object
     * @param response The response object
     * @param userSessionData Object wrapping the session
     * @param requestParser Parser to simple retrieve data from the request
     * @param bom Reference to the BusinessObjectManager
     * @param log Reference to the IsaLocation, needed for logging
     *
     * @return Forward to another action or page
     *
     * @throws CommunicationException DOCUMENT ME!
     */
    public ActionForward isaPerform(ActionMapping         mapping,
                                    ActionForm            form,
                                    HttpServletRequest    request,
                                    HttpServletResponse   response,
                                    UserSessionData       userSessionData,
                                    RequestParser         requestParser,
                                    BusinessObjectManager bom,
                                    IsaLocation           log)
                             throws CommunicationException {
        final String METHOD_NAME = "isaPerform()";
        log.entering(METHOD_NAME);

        // Page that should be displayed next.
        String forwardTo = "selectsoldto";

        User user = bom.createUser();
        Shop shop = bom.getShop();

        //why is this here? => performance!!
        //String addrTest = user.getAddress().getTitle();
        //String addrTest2 = user.getAddress().getName();

        forwardTo = selectSoldTo(request, bom, userSessionData, log);

        log.exiting();

        return mapping.findForward(forwardTo);
    }

    /**
     * DOCUMENT ME!
     *
     * @param request DOCUMENT ME!
     * @param bom DOCUMENT ME!
     * @param userSessionData DOCUMENT ME!
     * @param log DOCUMENT ME!
     *
     * @return the forward to forward to.
     *
     * @throws CommunicationException DOCUMENT ME!
     */
    public String selectSoldTo(HttpServletRequest    request,
                               BusinessObjectManager bom,
                               UserSessionData       userSessionData,
                               IsaLocation           log)
                        throws CommunicationException {
        String forwardTo = null;
        boolean isDebugEnabled = log.isDebugEnabled();

        User user = bom.createUser();
        Shop shop = bom.getShop();
        BusinessPartnerManager buPaMa = bom.createBUPAManager();

        BusinessPartner contact = buPaMa.getDefaultBusinessPartner(PartnerFunctionData.CONTACT);
        ResultData result = buPaMa.getSoldTosBySalesArea(contact, shop.getId());

        IsaCoreInitAction.StartupParameter startupParameter = (IsaCoreInitAction.StartupParameter) userSessionData.getAttribute(IsaCoreInitAction.SC_STARTUP_PARAMETER);

        if ((result == null) || (result.getNumRows() <= 0)) { // fm 19.06.01

            if (isDebugEnabled) {
                log.debug("found no sold to, bailing out.");
            }

            log.error(LogUtil.APPS_USER_INTERFACE, "no sold tos found!");

            user.addMessage(new Message(Message.ERROR, "user.noSoldTo"));

            // user.addMessage(new Message(Message.ERROR,"user.noSoldTo",null,null));
            request.setAttribute(BusinessObjectBase.CONTEXT_NAME, user);

            forwardTo = "error";
        }
        else if (result.getNumRows() == 1) {
            // move to first row
            result.absolute(1);

            if (isDebugEnabled) {
                log.debug("found exactly one sold to, using it - SOLDTO: " + result.getString("SOLDTO") +
                          " - SOLDTO-TECHKEY: " + result.getString("SOLDTO_TECHKEY"));
            }

            // Save sold techKey in request
            request.setAttribute(ReadSoldToAction.PARAM_SOLDTO, result.getString("SOLDTO"));
            request.setAttribute(ReadSoldToAction.PARAM_SOLDTO_TECHKEY, result.getString("SOLDTO_TECHKEY"));

            request.setAttribute(RK_SOLDTO_LIST, result);
            userSessionData.setAttribute(RK_SOLDTO_LIST, result);

            forwardTo = "readsoldto";
        }
        else {
        	boolean autoEntry = startupParameter.getAutoEntry().equals("YES");
        	if (autoEntry) {
                String soldToId = startupParameter.getSoldTo();
                handleAutoEntry(request, userSessionData, log, buPaMa, soldToId, result);
            }

            if (isDebugEnabled) {
                log.debug("found more than one sold to.");
            }

            request.setAttribute(RK_SOLDTO_LIST, result);
            userSessionData.setAttribute(RK_SOLDTO_LIST, result);
            if (startupParameter.getPortal().equals("YES") && autoEntry) {
                forwardTo = "readsoldto";            }
            else {
                forwardTo = "selectsoldto";
            }
        }

        log.exiting();

        return forwardTo;
    }

    private void handleAutoEntry(HttpServletRequest     request,
                                 UserSessionData        userSessionData,
                                 IsaLocation            log,
                                 BusinessPartnerManager buPaMa,
                                 String                 soldToId,
                                 ResultData             result) {
                                     
        boolean isDebugEnabled = log.isDebugEnabled();

        // Found more than one sold-to parties but autoentry requested (currently
        // used from Portal iViews). Since no shoplist can be presented, this
        // situation will lead to errorpage
        // logic has been changed here, default to the first item
        // not do not return the error page
        //request.setAttribute(RK_AUTOENTRYERROR, "soldtoerror");
        //request.setAttribute(RK_AUTOENTRYADDINFO, "Contact:".concat(user.getUserId()));
        //return mapping.findForward(FORWARD_AUTOENTRYERROR);
        // move to first row
        if ((soldToId != null) && (soldToId.length() > 0)) {
            if (isDebugEnabled) {
                log.debug("soldToId provided as startup parameter: " + soldToId);
            }

            // Save soldToId in request
            request.setAttribute(ReadSoldToAction.PARAM_SOLDTO, soldToId);

            // get TeckKey 
            BusinessPartner soldTo = null;

            if ((soldToId != null) && (!soldToId.equalsIgnoreCase(""))) {
                soldTo = buPaMa.getBusinessPartner(soldToId);
                request.setAttribute(ReadSoldToAction.PARAM_SOLDTO_TECHKEY, soldTo.getTechKey().getIdAsString());
            }
            else {
                if (isDebugEnabled) {
                    log.debug("found no sold to in startupParameter, bailing out.");
                }

                request.setAttribute(ReadSoldToAction.PARAM_SOLDTO_TECHKEY, result.getString("SOLDTO_TECHKEY"));
            }
        }
        else {
            result.absolute(1);

            if (isDebugEnabled) {
                log.debug("found more than one sold to, using the first one - SOLDTO: " + result.getString("SOLDTO") +
                          " - SOLDTO-TECHKEY: " + result.getString("SOLDTO_TECHKEY"));
            }

            // Save soldto + techkey techKey in request
            request.setAttribute(ReadSoldToAction.PARAM_SOLDTO, result.getString("SOLDTO"));
            request.setAttribute(ReadSoldToAction.PARAM_SOLDTO_TECHKEY, result.getString("SOLDTO_TECHKEY"));
        }

        // do not need to be changed
        request.setAttribute(RK_SOLDTO_LIST, result);
        userSessionData.setAttribute(RK_SOLDTO_LIST, result);
    }
}
