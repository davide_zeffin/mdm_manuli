/*****************************************************************************
	Class:        CatalogReadListBaseAction 
	Copyright (c) 2004, SAPLabs, Palo Alto, All rights reserved.
	Author        SAP

	$Revision: #1 $

*****************************************************************************/


package com.sap.isa.isacore.action.b2b;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.User;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

public class CatalogReadListBaseAction extends IsaCoreBaseAction {
	/**
	 * Name of the lists of marketing attributes stored in the request context.
	 */
	public static final String RK_CATALOG_LIST = "cataloglist";
	/**
	 * Flag to signal problems during auto entry process
	 */
	public static final String RK_AUTOENTRYERROR = "autoentryerror";

	protected ResultData readCatalogList(String shopId,
										BusinessObjectManager bom,
										String bpId)
						throws CommunicationException {
							
		final String METHOD_NAME = "readCatalogList()";
		log.entering(METHOD_NAME);
		ResultData result = null;
		User user = bom.createUser();
		BusinessPartnerManager buPaMa = bom.getBUPAManager();
		BusinessPartner contact = buPaMa.getDefaultBusinessPartner(PartnerFunctionData.CONTACT);
		BusinessPartner soldTo = null;
		if ((bpId != null) && (!bpId.equalsIgnoreCase(""))){
			soldTo = buPaMa.getBusinessPartner(bpId);		
		}else{
			soldTo = buPaMa.getDefaultBusinessPartner(PartnerFunctionData.SOLDTO);
		}	
		result = user.getCatalogs(shopId, 
						soldTo.getTechKey(), contact.getTechKey());
		log.exiting();
		return result;
	}

}
