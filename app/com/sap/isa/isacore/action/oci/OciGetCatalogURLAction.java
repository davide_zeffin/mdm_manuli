/*****************************************************************************
	Class         OciGetCatalogURLAction
	Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
	Description:  Action which get the URL from the external catalog. 
	Author:       SAP
	Created:      February 2003
	Version:      1.0

	$Revision: #0 $
	$Date: 2003/02/18 $
*****************************************************************************/

package com.sap.isa.isacore.action.oci;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.interaction.InteractionConfig;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.isacore.action.IsaAppBaseAction;
/**
 * Returns the URL for a external catalog. 
 * 
 * <h4>Overview over request parameters and attributes</h4>
 *
 * <table border="1" cellspacing="0" cellpadding="4">
 *   <tr>
 *     <th>name</th>
 *     <th>parameter</th>
 *     <th>attribute</th>
 *     <th>description</th>
 *   <tr>
 *   <tr>
 *      <td>nextPage</td><td>X</td><td>&nbsp;</td>
 *      <td>Contains the name of JSP that should displayed next.</td>
 *   </tr></table>
 *
 * <h4>The following forwards are used by this action</h4>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <th>forward</th>
 *     <th>description</th>
 *   <tr>
 *   <tr><td>welcome</td><td>the <b>welcome.jsp</b> is displayed next.</td></tr>
 *   <tr><td>defaultworkarea</td><td>the
 * <b>default_workarea.jsp</b> is displayed next.
 * </td></tr>
 *   <tr><td><i>locial   forward given by the <b>forward</b>
 * attribute</i></td><td>see config.xml</td></tr>
 * </table>
 * <h4>Overview over attributes which will be set in the request context</h4>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <th>attribute</th>
 *     <th>description</th>
 *   <tr>
 * <tr><td>forward</td><td>logical forward which JSP, will be called. The value
 * is <i>welcome or defaultworkarea</i> by default</td></tr>
 * <tr><td>externalCatalogURL</td>
 * <td>A new window opens using this URL to show a external catalog.
 * </td></tr>
 * </table>
 *
 *
 * @author SAP AG
 * @version 1.0
 *
 */
public class OciGetCatalogURLAction extends IsaAppBaseAction {
	
	/**
	 * Name of the request parameter for storage the forward to next page.
	 */
	private static final String FORWARD_SUCCESS = "success";
	private static final String FORWARD_FAILURE = "failure";
	
	public static   String hook_action_name = "HOOK_URL";
	public static   String target_name      = "~TARGET";
	public static   String version_name     = "OCI_VERSION";
	public static   String form_name        = "~FORM";
	public static   String caller_name      = "~CALLER";
	public static   String okcode_name      = "~OkCode";

	public static   String oci_hook_action  = "b2b/ocireceive.do";
	public static   String oci_caller       = "CTLG";
	public static   String oci_okcode       = "ADDI";

	/**
	 * Overriden <em>isaPerform</em> method of <em>IsaCoreBaseAction</em>.
	 */
	public ActionForward performAction(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom)
		throws CommunicationException {
			
		final String METHOD_NAME = "performAction()";
		log.entering(METHOD_NAME);
		// first ask the mbom for the used bom
		BusinessObjectManager bom = (BusinessObjectManager)
				mbom.getBOMbyName(BusinessObjectManager.ISACORE_BOM);
			
		// Page that should be displayed next.
		String forwardTo = FORWARD_SUCCESS;
		
		// if customer likes to override forward, this string is used
		String customerForwardTo = null;
		
		boolean isDebugEnabled = log.isDebugEnabled();
		
		String ociUrl = null;
		
		Shop shop = bom.getShop();
		
		// get all the stuff form the XCM.
		InteractionConfig interactionConfig = getInteractionConfig(request).getConfig("oci");
		
		String enable = interactionConfig.getValue("ociEnable");
		String oci_readAllProductData = "";
		String oci_allProductsInMaterialMaster = "";
		
		if ((enable != null) && (enable.equalsIgnoreCase("true"))) {
		
			String externalCatalogUrl       = interactionConfig.getValue("ociCatalogURL");
			String oci_target       		= interactionConfig.getValue("ociTarget");
			String oci_version     			= interactionConfig.getValue("ociVersion");
			String oci_form         		= interactionConfig.getValue("ociForm");
			
			oci_readAllProductData   		= interactionConfig.getValue("ociReadAllProductData");
			oci_allProductsInMaterialMaster = interactionConfig.getValue("ociAllProductsInMaterialMaster");
			
			if (isDebugEnabled) {
				log.debug("external catalog URL in XCM: " + externalCatalogUrl +
						  " ociTarget: " + oci_target +
						  " ociVersion: " + oci_version +
						  " ociForm: " + oci_form +
						  "  ### and the other parameters ociReadAllProductData: "+ oci_readAllProductData + 
						  " ociAllProductsInMaterialMaster: " + oci_allProductsInMaterialMaster);
			}			
			
			// construct the catalog-url-suffix from the arguments
			String completeURL = WebUtil.getAppsURL(getServlet().getServletContext(), request, response, null, oci_hook_action, null, null, true);
			ociUrl = externalCatalogUrl + "?" + hook_action_name + "=" + completeURL;
			
			ociUrl += "&" + target_name + "=" + oci_target;
			ociUrl += "&" + version_name + "=" + oci_version;
			ociUrl += "&" + form_name + "=" + oci_form;
			ociUrl += "&" + caller_name + "=" + oci_caller;
			ociUrl += "&" + okcode_name + "=" + oci_okcode;
			
		}
		
		// user exit, to modify the OCI catalog URL, if necessary
		String catalogUrl = customerExitGetOCIURL(requestParser, request, userSessionData, bom, ociUrl);
		
		// user exit, to modify forward, if necessary
		customerForwardTo = customerExitDispatcher(requestParser, request, userSessionData, bom, catalogUrl);
		forwardTo = (customerForwardTo == null) ? forwardTo : customerForwardTo;
		
		if (shop != null) {
			
			if ((enable != null) && enable.equalsIgnoreCase("true")) {
				
				shop.setOciAllowed(true);
				
				if (catalogUrl != null) {
					shop.setExternalCatalogURL(catalogUrl);
				}
				
				if ((oci_readAllProductData != null) && (oci_readAllProductData.equalsIgnoreCase("true"))) {
					shop.setProductInfoFromExternalCatalogAvailable(true);
				}
				
				if ((oci_allProductsInMaterialMaster != null) && (oci_allProductsInMaterialMaster.equalsIgnoreCase("true"))) {
					shop.setAllProductInMaterialMaster(true);
				}
				
			}

		}
		log.exiting();	
		return mapping.findForward(forwardTo);
		
	}
	
	/**
	* Template method to be overwritten by the subclasses of this class.
	* This method provides the possibilty, to easily change the forward
	* of an action, accordingly to customer requirements. if this method
	* returns nothing, the default forward is used.<br>
	* By default this method returns <b>null</b>.<br>
	* <code>customerExitDispatcher</code> method.
	*
	* @param requestParser     parser to simple retrieve data from the request
	* @param request			request context
	* @param userSessionData   object wrapping the session
	* @param bom			    reference to the BusinessObjectManager
	* @param ociUrl      		the default catalog URL
	* 
	* @return logical key for a forward to another action or jsp
	*
	*/
	public String customerExitDispatcher(
			RequestParser requestParser,
			HttpServletRequest request,
			UserSessionData userSessionData,
			BusinessObjectManager bom,
			String catalogUrl) {
				
			   return null;
	}
	
	/**
	* Template method to be overwritten by the subclasses of this class.
	* This method provides the possibilty, to easily change the URL of an
	* externel catalog, accordingly to customer requirements.
	* By default this method returns the <b>default catalog URL</b>.<br>
	* <code>customerExitGetOCIURL</code> method.
	*
	* @param requestParser     parser to simple retrieve data from the request
	* @param request			request context
	* @param userSessionData   object wrapping the session
	* @param bom			    reference to the BusinessObjectManager
	* @param ociUrl      		the default catalog URL
	* 
	* @return ociUrl			the modified catalog URL, if necessary
	*
	*/
	public String customerExitGetOCIURL(
			RequestParser requestParser,
			HttpServletRequest request,
			UserSessionData userSessionData,
			BusinessObjectManager bom,
			String ociUrl) {
				
			   return ociUrl;
	}

}
