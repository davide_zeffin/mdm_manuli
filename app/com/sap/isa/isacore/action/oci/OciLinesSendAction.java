/*****************************************************************************
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP AG
    Created:      29 May 2001

    $Revision: #2 $
    $Date: 2001/07/30 $
*****************************************************************************/
package com.sap.isa.isacore.action.oci;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectException;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businessobject.oci.HookUrlParser;
import com.sap.isa.businessobject.oci.OciLineList;
import com.sap.isa.businessobject.oci.OciServer;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.interaction.InteractionConfig;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.MessageList;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.action.IsaCoreInitAction;

/**
 * Displays a contract for release order.
 */
public class OciLinesSendAction extends IsaCoreBaseAction {

    /**
     * Name of the sales document key attribute stored in the request context.
     */
    public static final String SALES_DOCUMENT_KEY = "salesDocumentKey";

    /**
     * Name of the oci line list attribute stored in the request context.
     */
    public static final String OCI_LINE_LIST = "ociLineList";

    /**
     * Name of the oci line list attribute stored in the request context.
     */
    public static final String OCI_HOOK_URL = "ociHookUrl";

    /**
     * Names for caller, target and okcode parameter constants of an oci client
     * stored in the request context.
     */
    public static final String OCI_OKCODE = "~OkCode";
    public static final String OCI_TARGET = "~target";
    public static final String OCI_CALLER = "~caller";

    /**
     * Name of the oci line list attribute stored in the request context.
     */
    public static final String OCI_VERSION = "ociVersion";

    private static final String FORWARD_OCI_LINES_SEND = "ocilinessend";
    private static final String FORWARD_ERROR = "error";

    /**
     * Overriden <em>doPerform</em> method of <em>BaseAction</em>.
     */
    public ActionForward isaPerform(
                ActionMapping mapping,
                ActionForm form,
                HttpServletRequest request,
                HttpServletResponse response,
                UserSessionData userSessionData,
                RequestParser requestParser,
                BusinessObjectManager bom,
                IsaLocation log,
                IsaCoreInitAction.StartupParameter startupParameter,
                BusinessEventHandler eventHandler)
                throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        HookUrlParser hup = null;
        HookUrlParser.Parameter hupParameter = null;
        MessageList messageList = new MessageList();
        OciLineList ociLineList = null;
        OciServer ociServer = null;
        String[] ressourceDummy = { "", "" };
        TechKey salesDocumentKey = null;

        // prepare logging
        boolean isDebugEnabled = log.isDebugEnabled();
        String logPrefix = null;
        if (isDebugEnabled) {
            logPrefix = request.getSession().getId() + " ~ isaPerform(): ";
        }

        // get shop data
        Shop shop = bom.getShop();
        if (shop == null) {
        	log.exiting();
            throw new PanicException("shop.notFound");
        }

        // get / create the Contract business object
        ociServer = bom.getOciServer();
        if (ociServer == null) {
            ociServer = bom.createOciServer();
        }

        // get hook url as startup parameters
        if (startupParameter != null) {
            String hookUrl = startupParameter.getHookUrl();
            if (hookUrl.length() > 0) {
                ociServer.setHookUrl(hookUrl);
            }
            else {
                messageList.add(
                    new Message(
                        Message.ERROR,
                        "oci.noHookUrl",
                        ressourceDummy,
                        ""));
                request.setAttribute(
                    "Exception",
                    new BusinessObjectException("oci.noHookUrl", messageList));
                log.warn("oci.noHookUrl");
                log.exiting();
                return mapping.findForward(FORWARD_ERROR);
            }
            if (shop.isPartnerBasket()) {
	            // get oci version from xcm
				InteractionConfig interactionConfig = getInteractionConfig(request).getConfig("oci");
	            ociServer.setOciVersion(interactionConfig.getValue("ociVersion"));
			}
			else {
				// get oci version from startup-parameters
				ociServer.setOciVersion(startupParameter.getOciVersion());
			}
        }
        else {
        	log.exiting();
            return mapping.findForward(FORWARD_ERROR);
        }

        // try to get sales document key ...
        salesDocumentKey = null;

        // ... either from request param
        RequestParser.Value salesDocumentKeyValue = requestParser.
            getParameter(SALES_DOCUMENT_KEY).getValue();
        if (salesDocumentKeyValue.isSet() &&
            !(salesDocumentKeyValue.getString().equalsIgnoreCase("") ||
              salesDocumentKeyValue.getString().equalsIgnoreCase("null")) ) {
            salesDocumentKey = new TechKey(salesDocumentKeyValue.getString());
        }

        // ... or from user session data
        else {
            String salesDocumentKeyString =
                    (String)userSessionData.getAttribute(SALES_DOCUMENT_KEY);
            if (salesDocumentKeyString != null &&
                !(salesDocumentKeyString.equalsIgnoreCase("") ||
                  salesDocumentKeyString.equalsIgnoreCase("null"))) {
                salesDocumentKey = new TechKey(salesDocumentKeyString);
            }
        }

        if (salesDocumentKey != null) {

            // read oci lines from the be
            ociLineList = ociServer.readOciLines(
                salesDocumentKey,
                shop.getLanguage());    // might throw communicationException
            if (isDebugEnabled) {
                log.debug(logPrefix + " [ociLineList] " + ociLineList.toString());
            }
            if (ociLineList.getMessageList().size() > 0) {
                messageList.add(
                    new Message(
                        Message.ERROR,
                        "oci.xmlTransferError",
                        ressourceDummy,
                        ""));
                request.setAttribute(
                    "Exception",
                    new BusinessObjectException(
                        "oci.xmlTransferError",
                        messageList));
                if (isDebugEnabled) {
                    log.debug(logPrefix + " [ociLineList.MessageList] " +
                        ociLineList.getMessageList().toString());
                }
                log.exiting();
                return mapping.findForward(FORWARD_ERROR);
            }
        }
        else {

            // empty key means logoff without basket or manipulation - reason
            // enough to exit with an empty list
            ociLineList = new OciLineList();
        }

        // pass values to the JSP via request context
        String hookUrl = ociServer.getHookUrl();
        request.setAttribute(OCI_HOOK_URL, hookUrl);
        request.setAttribute(OCI_VERSION, ociServer.getOciVersion());
        request.setAttribute(OCI_LINE_LIST, ociLineList);
        hup = new HookUrlParser(hookUrl);
        hupParameter = hup.getParameter(OCI_OKCODE);
        if (hupParameter.isSet()) {
            request.setAttribute(OCI_OKCODE, hupParameter.getValue());
        }
        hupParameter = hup.getParameter(OCI_TARGET);
        if (hupParameter.isSet()) {
            request.setAttribute(OCI_TARGET, hupParameter.getValue());
        }
        else {
            request.setAttribute(OCI_TARGET, "_top"); // default value
        }
        hupParameter = hup.getParameter(OCI_CALLER);
        if (hupParameter.isSet()) {
            request.setAttribute(OCI_CALLER, hupParameter.getValue());
        }
		log.exiting();
        return mapping.findForward(FORWARD_OCI_LINES_SEND);
    }
}