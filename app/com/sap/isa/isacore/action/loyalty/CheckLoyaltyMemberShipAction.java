/*****************************************************************************
   Class         CheckLoyaltyMemberShipAction
   Copyright (c) 2008, SAP AG, Germany, All rights reserved.
   Description:  Action to check if membership to loyalty program exist
   Created:      05.08.2008
   Version:      1.0
*****************************************************************************/
 
package com.sap.isa.isacore.action.loyalty;
 
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
 
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.ecommerce.businessobject.loyalty.LoyaltyMembership;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.action.loyalty.LoyaltyUtilities;
import com.sap.isa.businessobject.Shop;
 
public class CheckLoyaltyMemberShipAction extends IsaCoreBaseAction {
 
 public ActionForward isaPerform(
  ActionMapping mapping,
  ActionForm form,
  HttpServletRequest request,
  HttpServletResponse response,
  UserSessionData userSessionData,
  RequestParser requestParser,
  BusinessObjectManager bom,
  IsaLocation log)
  throws CommunicationException {
 
  final String METHOD_NAME = "isaPerform()";
  log.entering(METHOD_NAME);
  String forward = "noMembershipExist";        
  Shop shop = bom.getShop();
  
  if (shop.isEnabledLoyaltyProgram() && shop.getLoyaltyProgramID() != null) {
      LoyaltyMembership loyMembership = LoyaltyUtilities.getLoyaltyMembership(userSessionData);
      if (loyMembership != null && loyMembership.exists()) {
        forward = "membershipExist";
      }
  }
  
  log.exiting();
  return mapping.findForward(forward);
 }
} 
