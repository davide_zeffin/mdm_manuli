package com.sap.isa.isacore.action.loyalty;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.ecommerce.businessobject.MarketingBusinessObjectManager;
import com.sap.ecommerce.businessobject.campaign.MarketingCampaignList;
import com.sap.ecommerce.businessobject.loyalty.LoyaltyMembership;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.User;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/*****************************************************************************
			Class         ShowLoyaltyCampaignAction
			Copyright (c) 2008, SAP AG, Germany, All rights reserved.
			Description:  Action to check prerequisites for displaying the loyalty campaign list
			Created:      29.02.08
			Version:      1.0
*****************************************************************************/

public class ShowLoyaltyCampaignAction extends IsaCoreBaseAction {

	public ActionForward isaPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		BusinessObjectManager bom,
		IsaLocation log)
		throws CommunicationException {

		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
		String forward = "success";

		MarketingBusinessObjectManager marketingBom = (MarketingBusinessObjectManager) userSessionData.getBOM(MarketingBusinessObjectManager.MARKETING_BOM);
		LoyaltyMembership loyaltyMembership = marketingBom.getLoyaltyMembership();

		if (loyaltyMembership == null) {
			loyaltyMembership = marketingBom.createLoyaltyMembership();
		} else {
			loyaltyMembership.clearMessages();
		}
		
		//gets the date format from the shop
		String format = bom.getShop().getDateFormat();
		format = format.replace('Y', 'y');
		format = format.replace('D', 'd');
		DateFormat dateFormat = new SimpleDateFormat(format);
		dateFormat.setLenient(false);

		//get the loyalty campaigns
		MarketingCampaignList campaignList = loyaltyMembership.getLoyaltyCampaigns();
		
		boolean reReadCampaignList = false;
		
		//check if the user wants to enroll for a campaign
		if(campaignList != null && userSessionData.getAttribute("reReadCampaign") != null && userSessionData.getAttribute("reReadCampaign").toString().equals("true")){
			reReadCampaignList = true;
		}

		if (campaignList == null || campaignList.size() == 0 || reReadCampaignList) {
			//campaigns will be read from backend.
			String campaignTextId = new String();
			User user = bom.getUser();
			String memberGuid = user.getBusinessPartner().toString();
			campaignList = loyaltyMembership.getLoyaltyCampaignsFromBackend(loyaltyMembership.getMembershipId(), memberGuid, campaignTextId, dateFormat);
			
			loyaltyMembership.setLoyaltyCampaignList(campaignList);
		}

		//maybe the user clicks on the sort link?
		if (campaignList != null && campaignList.size() > 0) {
			//maybe the user clicks on the sort link?
			if (userSessionData.getAttribute("sort") != null) {
				String sortBy = userSessionData.getAttribute("sort").toString();
				if (!sortBy.equals("")) {
					campaignList.order(dateFormat, sortBy);
					loyaltyMembership.setLoyaltyCampaignList(campaignList);
				}
			}
		}

		log.exiting();
		return mapping.findForward(forward);
	}

}