/*****************************************************************************
			Class         LoyaltyUtilities
			Copyright (c) 2008, SAP AG, Germany, All rights reserved.
			Description:  
			Created:      01.04.2008
			Version:      1.0
*****************************************************************************/

package com.sap.isa.isacore.action.loyalty;

import com.sap.ecommerce.businessobject.MarketingBusinessObjectManager;
import com.sap.ecommerce.businessobject.loyalty.LoyaltyMembership;
import com.sap.ecommerce.businessobject.loyalty.LoyaltyPointAccount;
import com.sap.ecommerce.businessobject.loyalty.LoyaltyProgram;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.util.RequestParser;
 


public class LoyaltyUtilities {

    /**
     * Flag indicationg that a loyalty program membership shoul be created
     */
    public static final String PN_JOIN_LOYALTY = "joinLoyaltyProgram";

    
    
    /** 
     * returns true if the join loyalty program is part of the 
     * user session data object and false if not
     * 
     * @param userSessionData
     * @return boolean value indicating if the join loyalty 
     *         attrubute is contained with value true in the 
     *         user sessiond data
     */
    public static boolean isJoiningLoyaltyProgram(UserSessionData userSessionData) {
    	  	
    	if (userSessionData.getAttribute(PN_JOIN_LOYALTY) != null &&
    		userSessionData.getAttribute(PN_JOIN_LOYALTY).toString() != null &&
    		userSessionData.getAttribute(PN_JOIN_LOYALTY).toString().equals("true")) {
    	 return true;	
    	} else {
    	 return false;	
    	}
    } 

    
    
    public static LoyaltyMembership createLoyaltyMembership(RequestParser requestParser, UserSessionData userSessionData, BusinessObjectManager bom) 
          throws CommunicationException{
    	MarketingBusinessObjectManager marketingBom = (MarketingBusinessObjectManager) userSessionData.getBOM(MarketingBusinessObjectManager.MARKETING_BOM); 
	 	LoyaltyMembership loyMembership = marketingBom.getLoyaltyMembership();
    	
	 	if (loyMembership == null) {
    	  LoyaltyProgram loyProgram = marketingBom.createLoyaltyProgram(bom.getShop().getLoyaltyProgramGUID(), bom.getShop().getLoyaltyProgramID().toString());    	
    	  loyMembership = marketingBom.createLoyaltyMembership();
    	  loyMembership.setLoyaltyProgram(loyProgram);
          loyMembership.setPointAccount(new LoyaltyPointAccount(bom.getShop().getPointCode(), null));
    	
//    	  RequestParser.Parameter loyaltyPromotionCode = requestParser.getAttribute("loyaltyPromotionCode");
//      	  if (loyaltyPromotionCode.isSet()) {
//    	    LoyaltyCampaign loyPromotion = marketingBom.createLoyaltyCampaign();
//    	    CampaignHeader campaignHeader = (CampaignHeader) loyPromotion.createHeader();
//            campaignHeader.setCampaignID(loyaltyPromotionCode.toString());
//            loyPromotion.setHeader(campaignHeader);
//            loyPromotion.readCampaignHeader();
//            loyMembership.setLoyaltyPromotion(loyPromotion);
//    	   }
    	}
    	
	 	loyMembership.clearMessages();
	 	
		return loyMembership;
    }    
    
    
    public static LoyaltyMembership getLoyaltyMembership(UserSessionData userSessionData) {
    	MarketingBusinessObjectManager marketingBom = (MarketingBusinessObjectManager) userSessionData.getBOM(MarketingBusinessObjectManager.MARKETING_BOM); 	
    	return marketingBom.getLoyaltyMembership();   	  
    }    
    
    
    public static RequestParser.Parameter handleRequestAttributes(RequestParser requestParser, UserSessionData userSessionData, String parameterName) {
    	
    	// get the parameter from the request
    	RequestParser.Parameter parameter = requestParser.getParameter(parameterName);
    	
 		// put it back for later accesses
		userSessionData.setAttribute(parameterName, parameter.getValue().getString());
		
		return parameter;
    }
    
    
}
