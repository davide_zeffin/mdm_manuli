package com.sap.isa.isacore.action.loyalty;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.ecommerce.boi.loyalty.LoyaltyTransactionListData;
import com.sap.ecommerce.businessobject.MarketingBusinessObjectManager;
import com.sap.ecommerce.businessobject.loyalty.LoyaltyMembership;
import com.sap.ecommerce.businessobject.loyalty.LoyaltyTransactionList;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

public class SearchForLoyaltyTransactionsAction extends IsaCoreBaseAction {

	public void initialize() {
	}

	public ActionForward isaPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		BusinessObjectManager bom,
		IsaLocation log)
		throws CommunicationException {

		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
		String forward = "success";

		MarketingBusinessObjectManager marketingBom =
			(MarketingBusinessObjectManager) userSessionData.getBOM(MarketingBusinessObjectManager.MARKETING_BOM);

		LoyaltyMembership loyaltyMembership =
			marketingBom.getLoyaltyMembership();

		if (loyaltyMembership == null) {
			loyaltyMembership = marketingBom.createLoyaltyMembership();
		} else {
			loyaltyMembership.clearMessages();
		}

		//get the loyalty transactions
		LoyaltyTransactionListData loyaltyTransactions = loyaltyMembership.getLoyaltyTransactions();

		if (loyaltyTransactions == null) {
			loyaltyTransactions = new LoyaltyTransactionList();
		} else {
			loyaltyTransactions.clearMessages();
		}

		//gets the shop date format.
		String format = bom.getShop().getDateFormat();
		format = format.replace('Y', 'y');
		format = format.replace('D', 'd');
		DateFormat dateFormat = new SimpleDateFormat(format);
		dateFormat.setLenient(false);

		//maybe the user clicks on the sort link?
		boolean searchRequired = true;
		if (loyaltyTransactions.size() > 0) {
			RequestParser.Parameter order = requestParser.getParameter("order");
			if (order != null) {
				String sortBy = order.getValue().getString();
				if (!sortBy.equals("")) {
					loyaltyTransactions.order(dateFormat, sortBy);
					loyaltyMembership.setLoyaltyTransactionList(loyaltyTransactions);
					searchRequired = false;
				}
			}
		}

		if (searchRequired == true) {
			//if the user only clicks on the search link, it is not necessary to reread the 
			//loyalty transactions from the backend and to check the given dates

			//check the transactionFrom and transactionTo parameter
			//is the syntax correct?
			//Check if the date are well formed
			
			String transactionFromDate = null;
			String transactionToDate = null;

			if(request.getAttribute("transactionFrom")!=null){
				transactionFromDate = request.getAttribute("transactionFrom").toString();
			}
			else{
				RequestParser.Parameter transactionFrom = requestParser.getParameter("transactionFrom");
				transactionFromDate = transactionFrom.getValue().getString();
			}
			
			if(request.getAttribute("transactionTo")!=null){
				transactionToDate = request.getAttribute("transactionTo").toString();
			}
			else{
				RequestParser.Parameter transactionTo =	requestParser.getParameter("transactionTo");
				transactionToDate = transactionTo.getValue().getString();
			}
			
			if (transactionFromDate == null) {
				Message msg = new Message(Message.ERROR, "b2c.loyalty.noTransactionFromDate", null, "");
				String messageText = WebUtil.translate(userSessionData.getLocale(),	msg.getResourceKey(), null);
				msg.setDescription(messageText);
				loyaltyTransactions.addMessage(msg);
				forward = "message";
			} 
			if (transactionToDate == null){
				Message msg = new Message(Message.ERROR, "b2c.loyalty.noTransactionToDate",	null, "");
				String messageText = WebUtil.translate(userSessionData.getLocale(),msg.getResourceKey(),null);
				msg.setDescription(messageText);
				loyaltyTransactions.addMessage(msg);
				forward = "message";
			}

			Date fromDate = null;
			Date toDate = null;

			// do not accept dates like 34.03.2001
			if (transactionFromDate != null) {
				try {
					fromDate = dateFormat.parse(transactionFromDate);
				} catch (Exception e) {
					if (log.isDebugEnabled()) {
						log.debug("transaction From Date is invalid:"+ transactionFromDate);
					}
					Message msg = new Message(Message.ERROR,"b2c.loyalty.incorrectTransactionFromDate",	null, "");
					String messageText = WebUtil.translate(userSessionData.getLocale(),	msg.getResourceKey(), null);
					msg.setDescription(messageText);
					loyaltyTransactions.addMessage(msg);
				}
			}
			if (transactionToDate != null) {
				try {
					toDate = dateFormat.parse(transactionToDate);
				} catch (Exception e) {
					if (log.isDebugEnabled()) {
						log.debug("transaction To Date is invalid: "+ transactionToDate);
					}
					Message msg = new Message(Message.ERROR,"b2c.loyalty.incorrectTransactionToDate", null,"");
					String messageText = WebUtil.translate(userSessionData.getLocale(), msg.getResourceKey(),null);
					msg.setDescription(messageText);
					loyaltyTransactions.addMessage(msg);
				}
			}
			if (fromDate != null && toDate != null) {
				if (fromDate.after(toDate)) {
					Message msg = new Message(Message.ERROR, "b2c.loyalty.fromDateAfterToDate", null, "");
					String messageText = WebUtil.translate(userSessionData.getLocale(), msg.getResourceKey(), null);
					msg.setDescription(messageText);
					loyaltyTransactions.addMessage(msg);
				}
			}

			if (!loyaltyTransactions.getMessageList().isEmpty()) {
				forward = "message";
			} else {
				//sets the loyalty transaction dates
				loyaltyTransactions.setFromTransactionDate(transactionFromDate);
				loyaltyTransactions.setToTransactionDate(transactionToDate);
				String pointAccount = new String();
				if(loyaltyMembership.getPointAccount() != null){
					pointAccount = loyaltyMembership.getPointAccount().getTechKey().toString();
				}
				loyaltyTransactions.addLoyaltyTransactionList(
					loyaltyMembership.getLoyaltyTransactionsFromBackend(
						loyaltyMembership.getMembershipId(),
						pointAccount,
						dateFormat,
						fromDate,
						toDate));
			}
		}
		loyaltyMembership.setLoyaltyTransactionList(loyaltyTransactions);

		log.exiting();
		return mapping.findForward(forward);
	}
}
