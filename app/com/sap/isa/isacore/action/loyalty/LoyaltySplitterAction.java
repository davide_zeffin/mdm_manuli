/*****************************************************************************
			Class         LoyaltySplitterAction
			Copyright (c) 2008, SAP AG, Germany, All rights reserved.
			Description:  Action to register for a loyalty campaign in the backend
			Created:      27.03.2008
			Version:      1.0
*****************************************************************************/

package com.sap.isa.isacore.action.loyalty;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.ecommerce.businessobject.loyalty.LoyaltyMembership;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

public class LoyaltySplitterAction extends IsaCoreBaseAction {

	public ActionForward isaPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		BusinessObjectManager bom,
		IsaLocation log)
		throws CommunicationException {

		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
		String forward = "normal";
        
		Shop shop = bom.getShop();
		
		if (shop.isEnabledLoyaltyProgram() && shop.getLoyaltyProgramID() != null) {
//			we need to show if the user has already registered for the loyalty program.
//			If not, return "loyalty"
		   LoyaltyMembership loyMembership = LoyaltyUtilities.getLoyaltyMembership(userSessionData);
		   if (loyMembership != null && loyMembership.exists()) {
			   forward = "normal";
			}
		  else{
		       forward="loyalty";
		  }
		}
		
		log.exiting();
		return mapping.findForward(forward);
	}
} 