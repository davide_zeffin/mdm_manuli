package com.sap.isa.isacore.action.loyalty;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.ecommerce.businessobject.MarketingBusinessObjectManager;
import com.sap.ecommerce.businessobject.loyalty.LoyaltyMembership;
import com.sap.ecommerce.businessobject.loyalty.LoyaltyPointAccount;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/*****************************************************************************
		Class         ShowLoyaltyTransactionAction
		Copyright (c) 2008, SAP AG, Germany, All rights reserved.
		Description:  Action to check prerequisites for displaying the loyalty transaction list
		Created:      29.02.08
		Version:      1.0

*****************************************************************************/
public class ShowLoyaltyTransactionAction extends IsaCoreBaseAction {

	public ActionForward isaPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		BusinessObjectManager bom,
		IsaLocation log)
		throws CommunicationException {

		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
		String forward = "success";

		MarketingBusinessObjectManager marketingBom = (MarketingBusinessObjectManager) userSessionData.getBOM(
				MarketingBusinessObjectManager.MARKETING_BOM);
		LoyaltyMembership loyaltyMembership = marketingBom.getLoyaltyMembership();

		if (loyaltyMembership == null) {
			loyaltyMembership = marketingBom.createLoyaltyMembership();
		} else {
			loyaltyMembership.clearMessages();
		}

		//get the loyalty point account
		LoyaltyPointAccount loyaltyPointAccount = loyaltyMembership.getPointAccount();
		
		//gets the point code from the shop.
		Shop shop = bom.getShop();

		//loyalty point account will be read from the backend.
		loyaltyPointAccount = loyaltyMembership.getLoyaltyPointAccountBalanceFromBackend(
		                                           shop.getPointCode(), 
		                                           shop.getPointCodeDescription());
		loyaltyMembership.setPointAccount(loyaltyPointAccount);
		
		if(loyaltyMembership.getLoyaltyTransactions() == null){
//			now we search for the transaction for the last 30 days
			String dateFormat = null;
			// check for missing context
			if (shop != null) {
				dateFormat = shop.getDateFormat();
				dateFormat = dateFormat.replace('Y', 'y');
				dateFormat = dateFormat.replace('D', 'd');
				
				SimpleDateFormat fmt = new SimpleDateFormat(dateFormat);
				Date date = new Date();
				String fromDate = new String();
				String toDate = new String();
				Calendar cal = Calendar.getInstance();
				cal.setTime(date);
				date = cal.getTime();
				toDate = fmt.format(date);
				cal.add(Calendar.DAY_OF_MONTH,-30);
				date = cal.getTime();
				fromDate = fmt.format(date);
				
				request.setAttribute("transactionFrom",fromDate);
				request.setAttribute("transactionTo",toDate);
				forward="searchTr";
			}
		}
		
		log.exiting();
		return mapping.findForward(forward);

	}

}
