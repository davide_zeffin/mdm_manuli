package com.sap.isa.isacore.action.loyalty;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.ecommerce.businessobject.MarketingBusinessObjectManager;
import com.sap.ecommerce.businessobject.loyalty.LoyaltyMembership;
import com.sap.ecommerce.businessobject.loyalty.LoyaltyPointAccount;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/*****************************************************************************
		Class         InitLoyaltyTransactionAction
		Copyright (c) 2008, SAP AG, Germany, All rights reserved.
		Description:  Action to check prerequisites for displaying the loyalty transaction list
		Created:      05.08.2008
		Version:      1.0

*****************************************************************************/
public class InitLoyaltyTransactionAction extends IsaCoreBaseAction {

	public ActionForward isaPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		BusinessObjectManager bom,
		IsaLocation log)
		throws CommunicationException {

		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
		String forward = "success";

		MarketingBusinessObjectManager marketingBom = (MarketingBusinessObjectManager) userSessionData.getBOM(
				MarketingBusinessObjectManager.MARKETING_BOM);
		LoyaltyMembership loyaltyMembership = marketingBom.getLoyaltyMembership();

		if (loyaltyMembership == null) {
			loyaltyMembership = marketingBom.createLoyaltyMembership();
		} else {
			loyaltyMembership.clearMessages();
		}

//      refresh loyalty transactions
		if (loyaltyMembership.getLoyaltyTransactions() != null) {
            loyaltyMembership.setLoyaltyTransactionList(null);
		}
		
		log.exiting();
		return mapping.findForward(forward);

	}

}
