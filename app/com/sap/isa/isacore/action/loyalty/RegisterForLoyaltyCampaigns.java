package com.sap.isa.isacore.action.loyalty;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.ecommerce.businessobject.MarketingBusinessObjectManager;
import com.sap.ecommerce.businessobject.loyalty.LoyaltyMembership;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/*****************************************************************************
			Class         RegisterForLoyaltyCampaigns
			Copyright (c) 2008, SAP AG, Germany, All rights reserved.
			Description:  Action to register for a loyalty campaign in the backend
			Created:      07.03.2008
			Version:      1.0
*****************************************************************************/
public class RegisterForLoyaltyCampaigns extends IsaCoreBaseAction {

	public ActionForward isaPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		BusinessObjectManager bom,
		IsaLocation log)
		throws CommunicationException {

		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
		String forward = "success";

		MarketingBusinessObjectManager marketingBom = 
		    (MarketingBusinessObjectManager) userSessionData.getBOM(MarketingBusinessObjectManager.MARKETING_BOM);
		LoyaltyMembership loyaltyMembership = marketingBom.getLoyaltyMembership();

		if (loyaltyMembership == null) {
			loyaltyMembership = marketingBom.createLoyaltyMembership();
		} else {
			loyaltyMembership.clearMessages();
		}

		//check if the user want only to sort the loyalty campaign table.	
		RequestParser.Parameter order = requestParser.getParameter("order");
		if (order != null) {
			String sortBy = order.getValue().getString();
			if (!sortBy.equals("")) {
				forward = "sort";
				userSessionData.setAttribute("sort", sortBy);
			}
		}

		RequestParser.Parameter enroll = requestParser.getParameter("enroll");
		if (enroll != null) {
			if (enroll.getValue().getString().equals("true")) {

				//read the campaign Guid that should be used for the registration
				RequestParser.Parameter campaignGuidP = requestParser.getParameter("campaignGuid");
				String campaignGuid = new String();
				if (campaignGuidP != null) {
					campaignGuid = campaignGuidP.getValue().getString();

					if (!campaignGuid.equals("")) {
						//check if the user wants to enroll for a campaign
						String memberGuid = bom.getUser().getBusinessPartner().toString();
						boolean enrollmentIsOk = loyaltyMembership.enrollLoyaltyCampaignInBackend(campaignGuid, memberGuid);
						if (enrollmentIsOk) {
							userSessionData.setAttribute("reReadCampaign","true");
						}
					}
				}
			}
		}

		log.exiting();
		return mapping.findForward(forward);
	}
}
