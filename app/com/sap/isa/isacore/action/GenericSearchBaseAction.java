/*****************************************************************************
    Class:        GenericSearchBaseAction
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author        SAP

    $Revision: #22 $
    $DateTime: 2004/07/08 14:52:11 $ (Last changed)
    $Change: 195223 $ (changelist)

*****************************************************************************/
package com.sap.isa.isacore.action;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.GenericSearchBuilder;
import com.sap.isa.businessobject.GenericSearchControlOptionLine;
import com.sap.isa.businessobject.GenericSearchInstance;
import com.sap.isa.businessobject.GenericSearchRequestedFieldLine;
import com.sap.isa.businessobject.GenericSearchReturn;
import com.sap.isa.businessobject.GenericSearchSelectOptionLine;
import com.sap.isa.businessobject.GenericSearchSelectOptions;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.core.xcm.ConfigContainer;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.GenericSearchComparator;
import com.sap.isa.maintenanceobject.backend.GenericSearchBuilderHelper;
import com.sap.isa.maintenanceobject.backend.MaintenanceObjectHelperException;
import com.sap.isa.maintenanceobject.businessobject.GSAllowedValue;
import com.sap.isa.maintenanceobject.businessobject.GSProperty;
import com.sap.isa.maintenanceobject.businessobject.GSPropertyGroup;
import com.sap.isa.ui.uiclass.genericsearch.GenericSearchUIData;

/**
 * Base Action including methods to parse the HTTP request and build up 
 * the select options object. 
 * Class itself can not be used as action, but only as a helper. 
 * <br><b>Please use only as public declared methods !! </b><br>
 * <p>The proper sequence of method usage is<br>
 * - buildSelectOptions<br>
 * - handleDarkAttributes<br>
 * - getBackendImplementation has to be used before calling <code>performGenericSearch<code>
 * - makeReturnDataAvailableForUI<br><br>
 * </p> 
 * <p>
 * Part of the generic search framework.
 * </p>
*/
public class GenericSearchBaseAction extends EComBaseAction {

    /**
     * Name of the instance stored in the area context. <br> 
     */
    public static final String SC_GENERICSEARCH_INSTANCE = "genericsearch.instance";

    /**
     * Name of the return table "requested fields" in the session context 
     */
    public static final String SC_GENERICSEARCH_RET_FIELDS = "genericsearch.return.fields";

    /**
     * Name of the return table "counted documents" in the session context 
     */
    public static final String SC_GENERICSEARCH_RET_COUNTED_DOCS = "genericsearch.return.counted.docs";
    /**
     * Name of the return table "selected documents" in the session context 
     */
    public static final String SC_GENERICSEARCH_RET_DOCUMENTS = "genericsearch.return.documents";
	
    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param mbom              Reference to the MetaBusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward ecomPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            MetaBusinessObjectManager mbom,
            boolean multipleInvocation,
            boolean browserBack)
            throws CommunicationException {
        // Empty method to fulfill Base Class requirements
        return null;
    }

    /**
     * Use this method to build your GenericSearchSelectOptions object initialy.
     * It will first parse the request for the parameters defined in the (XML file) and
     * then add the requested fields.
     * @param handle                       Handle binding this select options together
     * @param request                      The HTTP request object 
     * @param userSessionData              Object wrapping the session
     * @return GenericSearchSelectOptions  A Select Options Object
     */
    public GenericSearchSelectOptions buildSelectOptions(
                             HttpServletRequest request,
                             UserSessionData userSessionData,
                             int handle) {
        if (isRePerformRequested(request) == true) {
            // Re-Performing of the last search has been requested 
            return lastSelectOptions(userSessionData);
        }
        Map srm = null;
        
        // Reference to the GenericSearchBuilder class instance
        GenericSearchBuilder genSrBuilder = getGenericSearchBuilder(userSessionData);
        // Name of the requested search
        String requestedSearch = getScreenName(request, userSessionData);
        
        // All information which is relvant for the JSP should be stored in the searchInstance. 
        GenericSearchInstance searchInstance = new GenericSearchInstance();
        searchInstance.setScreenName(requestedSearch);
        
        // set resultlist name
        if (request.getParameter(GenericSearchUIData.RC_RESULTLISTNAME) != null) {
        	searchInstance.setResultListName(request.getParameter(GenericSearchUIData.RC_RESULTLISTNAME));
        }	
        // set full screen mode 
        if (request.getParameter(GenericSearchUIData.RC_UI_NOFULLSCREENMODE) !=  null) {
        	searchInstance.setFullScreenMode(request.getParameter(GenericSearchUIData.RC_UI_NOFULLSCREENMODE));
        }
        
        // set select options flag 
        if (request.getParameter(GenericSearchUIData.RC_UI_NOSELECTOPTIONS) !=  null) {
        	searchInstance.setNoSelectOptionsAvailable(request.getParameter(GenericSearchUIData.RC_UI_NOSELECTOPTIONS));
        }
                

		getAreaContext(request).setAttribute(SC_GENERICSEARCH_INSTANCE,searchInstance);
        
        // Clear Search Request Memory
        if (genSrBuilder.getSearchCriteriaDescription(requestedSearch).isUseSearchRequestMemory()) {
            srm = getSearchRequestMemory(userSessionData, requestedSearch);
            if (srm != null) {
                srm.clear();
            }
        }
        // Create a new Select Option object
        GenericSearchSelectOptions selOpt = new GenericSearchSelectOptions();
        parseParameters(handle, selOpt, request, userSessionData, genSrBuilder, 
                        srm, requestedSearch);
        
        addRequestedFields(handle, selOpt, userSessionData, request, genSrBuilder, requestedSearch);
        setControlOptions(handle, selOpt, userSessionData, request, genSrBuilder, requestedSearch);
       
        // TODO use only search instance to store data.
        saveLastSelectOptions(userSessionData, selOpt);
        return selOpt;
    }
    
    /**
     * Returns the last select option object from the user session context
     * @param userSessionData
     * @return GenericSearchSelectOptions
     */
    protected GenericSearchSelectOptions lastSelectOptions(UserSessionData userSessionData) {
        return (GenericSearchSelectOptions)userSessionData.getAttribute(GenericSearchUIData.SC_LASTSELECTOPTIONS);
    }

    /**
     * Saves the current select option in the user session context 
     * @param UserSessionData
     * @param GenericSearchSelectOptions
     */
    protected void saveLastSelectOptions(UserSessionData userSessionData, GenericSearchSelectOptions selOpt) {
        userSessionData.setAttribute(GenericSearchUIData.SC_LASTSELECTOPTIONS, selOpt);
    }

    /**
     * Returns true if the last performed search requested should be repeated.
     * @param HttpServletRequest
     * @return
     */
    protected boolean isRePerformRequested(HttpServletRequest request) {
        if ("true".equals(request.getParameter(GenericSearchUIData.RC_REDOLASTSEARCH))) {
            return true;
        }
        return false;
    }

    /**
     * Use this method to parse the standard parameters.
     * <br><p>This method can not parse objects of type 'DARK'...
     * @param handle          Handle binding this select options together
     * @param selOpt          The Select Option object
     * @param request         The request Object
     * @param userSessionData Object wrapping the session
     */
    protected void parseParameters(int handle,
                                   GenericSearchSelectOptions selOpt,
                                   HttpServletRequest request,
                                   UserSessionData userSessionData,
                                   GenericSearchBuilder genSrBuilder,
                                   Map searchRequestMemory,
                                   String requestedSearch) {

    	if (requestedSearch == null  ||  requestedSearch.length() < 1) {
    		// Try to get searchname from the request attributes
    		getScreenName(request,userSessionData);
        }
        GSPropertyGroup ptyGrp = genSrBuilder.getSearchCriteriaDescription(requestedSearch);
        
        Iterator itPG = ptyGrp.iterator();
        // Loop over list of Properties. This will avoid taking parameters from
        // Request which are not meant to be for the search

        while (itPG.hasNext()) {
            GSProperty pty = (GSProperty)itPG.next();
            
            if ("dark".equals(pty.getType())  ||
                "text".equals(pty.getType())  ||
                "unknown".equals(pty.getType()) ) {
                continue;  // This type can't be processed here => skip this property
            }
                
            String select_param     = "";
            String entityType       = "";
            String param_function   = "";
            String param_func_type  = "";
            String token            = "";
            String sign             = "";
            String option           = "";
            String low              = "";
            String high             = ""; 

            // Check Property is of type RANGE or not
            String ptyReqName = pty.getRequestParameterName();
            String ptyReqNameHigh = "";
            if ( ptyReqName == null ) {
                // Property which had been defined just for UI control => skip it
                continue;
            }
            if ( ! pty.getType().startsWith("daterange")) {
                low = request.getParameter(ptyReqName);
                if (low == null  ||  low.length() == 0) {
                    // request parameter not found, look for request attribute
                    // of this name
                    low = (request.getAttribute(ptyReqName) != null ? (String)request.getAttribute(ptyReqName) : "");
                    // add Attribute to Search Request Memory
                    if (searchRequestMemory != null) {
                        searchRequestMemory.put(ptyReqName, low);
                    }
                }
            } else {
                // Property is defined as of type 'daterange' (LOW and HIGH value)
                ptyReqName = ptyReqName + "_low";
                ptyReqNameHigh = pty.getRequestParameterName() + "_high";
                low = request.getParameter(ptyReqName);
                high = request.getParameter(ptyReqNameHigh);
                if (low == null  ||  low.length() == 0) {
                    low = high;
                }
                if (high == null || high.length() == 0) {
                    high = low;
                }
                if (low == null  ||  low.length() == 0) {
                    // request parameter not found, look for request attribute
                    // of this name
                    low = (request.getAttribute(ptyReqName) != null ? (String)request.getAttribute(ptyReqName) : "");
                    high = (request.getAttribute(ptyReqNameHigh) != null ? (String)request.getAttribute(ptyReqNameHigh): "");
                }
            }
            // add Attribute to Search Request Memory
            if (low != null  &&  searchRequestMemory != null) {
                searchRequestMemory.put(ptyReqName, low);
                if ( high != null) {
                    // add Attribute to Search Request Memory
                    searchRequestMemory.put(ptyReqNameHigh, high);
                }
            }
             if ( low == null  ||  low.length() == 0) {
                // Attribute wasn't selected => skip it
                continue;
            }
            if (pty.getType().endsWith("UI")) {
                // Don't process attributes meant for UI (ie. boxUI)
                // Those were just meant UI control => So add it to search request memory,
                // but not to the search options!
                continue;
            }
            boolean dateRangeHighFormatting = true;
            if (pty.getType().startsWith("date")) {
                // Dates have to be converted from local format to internal format YYYYMMDD
                String dateTemplate = (String)request.getParameter(GenericSearchUIData.RC_DATEFORMAT);
                low = formatDateL(dateTemplate, low);
                if (pty.getType().startsWith("daterange") &&
                    high.length() < 1) {
                    // High not filled => high = low
                    high = low;
                    dateRangeHighFormatting = false;
                }
                if (high.length() > 0) {
                    // Formatting necessary
                    if (dateRangeHighFormatting) {
                        high = formatDateL(dateTemplate, high);
                    }
                    try {
                        Integer lowI = new Integer(low);
                        Integer highI = new Integer(high);
                        if ( (lowI.compareTo(highI) > 0)) {
                            // Low greather high => switch positions
                            low = highI.toString();
                            high = lowI.toString();
                        }
                    } catch (NumberFormatException numEx) { 
                        log.error(LogUtil.APPS_USER_INTERFACE, "Dates not valid: Low = " + low + " / High = " + high);
                    }
                }
            }
            if (isSubTypeToken(pty.getType())) {
                // Type has a subtype containing tokens
                param_function = getSubType(pty.getType());
            }
            // Handle Implementation Filter
            if ( ! pty.isImplemenationFilter()) {
                select_param = getActuallName(pty.getName());
            } else {
                select_param = "IMPLEMENTATION_FILTER";
                high = pty.getName();
            }
            entityType = pty.getEntityType();
            param_func_type = pty.getParameterType();
            token = pty.getTokenType();
            sign = "I";          // FIX 'I'ncluded
            if ( pty.getType().startsWith("daterange")) {
                option = "BT";  // BETWEEN
            } else if (low.indexOf("*") > -1 ) {
                option = "CP";  // CONTAINS PATTERN
            } else {
                option = "EQ";  // EQUALS
            }
            // Create new Select Option line and add it to the Select Options
            GenericSearchSelectOptionLine selOptLine = new GenericSearchSelectOptionLine (
                     handle,
                     select_param,
                     entityType,
                     param_function,
                     param_func_type,
                     token,
                     sign,
                     option,
                     low,
                     high);
             selOpt.addSelectOptionLine(selOptLine);
        } // END WHILE
      
    }
    
   /**
    * Use this method to make the return data as <code>ResultData</code> available
    * for the UI (JSP).
    * @param request   The request object
    * @param retData    Generic search return data object
    */
    public void makeReturnDataAvailableForUI(UserSessionData userSessionData,
                                             HttpServletRequest request,
                                             GenericSearchReturn returnData) {
    	
    	// TODO Only use search instance    	
    	GenericSearchInstance searchInstance = (GenericSearchInstance)getAreaContext(request).getAttribute(SC_GENERICSEARCH_INSTANCE);
    	if (searchInstance == null) {
    		log.debug("search Instance is not available");
//    		 TODO remove this coding after consolidation
    		searchInstance = new GenericSearchInstance();
    	}

        GenericSearchBuilder genSrBuilder = getGenericSearchBuilder(userSessionData);
        // Name of the requested search
        String requestedSearch = requestedSearch = getScreenName(request, userSessionData);
        GSPropertyGroup ptyGrp = genSrBuilder.getSearchCriteriaDescription(requestedSearch);
        
        addReturnDataToInstance(searchInstance, returnData);
        searchInstance.setQueryString(request.getQueryString());

        GSPropertyGroup resultlistDescription = searchInstance.getSearchGroup();

    	// TODO Only use search instance    	
        if (ptyGrp.isUseSearchRequestMemory()) {
            // Session
            addReturnDataToSession(userSessionData, searchInstance, requestedSearch);
            if (resultlistDescription == null) {
	            resultlistDescription = (GSPropertyGroup)userSessionData.getAttribute(
	                 GenericSearchUIData.RC_RESULTLIST_PTYGRP + requestedSearch);
            }    
        } else {
            // Request
            addReturnDataToRequest(request, searchInstance, requestedSearch);
            if (resultlistDescription == null) {
            	resultlistDescription = (GSPropertyGroup)request.getAttribute(
                 GenericSearchUIData.RC_RESULTLIST_PTYGRP + requestedSearch);
            }	
        }
        
        ResultData documents = searchInstance.getDocuments();
        
        // Sorting
        preSortResultData(userSessionData, request, ptyGrp ,resultlistDescription, documents);            
    }
    
    /**
     * Use this method to sort the result data according to the default declaration in the 
     * XML file: <code>defaultSortSequence="ASCENDING"</code> or <code>defaultSortSequence="DESCENDING"</code>.
     * @param UserSessionData  The session context object
     * @param request   The request object
     * @param GSPropertyGroup defining the search screen
     * @param GSPropertyGroup defining the result list
     * @param GenericSearchReturn inclduing the result data
     */
    protected void preSortResultData(UserSessionData userSessionData,
                                     HttpServletRequest request,
                                     GSPropertyGroup ptyGrpS,
                                     GSPropertyGroup ptyGrpR,
                                     ResultData documents) {
        // Write sorting information as Map into Request
        Map sortInfo = new HashMap();
        userSessionData.setAttribute(
         (GenericSearchUIData.RC_SORTINFO + getScreenName(request, userSessionData)), sortInfo);

        Iterator itPG = ptyGrpR.iterator();
        GSProperty pty = null;
        // Find Prooperty carrying the sorting information
        while (itPG.hasNext()) {
            pty = (GSProperty)itPG.next();
            if (pty.getDefaultSortSequence() != null) {
                break;
            }
        }
        if (pty.getDefaultSortSequence() == null) {
            // NO pre sorting defined
            return;
        }
        String sortSequenceStr = pty.getDefaultSortSequence().toUpperCase();
        int sortSeq = 0;
        // Define sorting sequence
        if ("ASCENDING".equals(sortSequenceStr)) {
            sortSeq = GenericSearchComparator.ASCENDING;
        }
        if ("DESCENDING".equals(sortSequenceStr)) {
            sortSeq = GenericSearchComparator.DESCENDING;
        }
        // Check object type
        String dateFormat = (String)request.getParameter(GenericSearchUIData.RC_DATEFORMAT);
        if (dateFormat == null) {
            dateFormat = (String)request.getAttribute(GenericSearchUIData.RC_DATEFORMAT);
        }
        String numberFormat = (String)request.getParameter(GenericSearchUIData.RC_NUMBERFORMAT);
        if (numberFormat == null) {
            numberFormat = (String)request.getAttribute(GenericSearchUIData.RC_NUMBERFORMAT);
        }
        String language = (String)request.getParameter(GenericSearchUIData.RC_LANGUAGE);
        if (language == null) {
            language = (String)request.getAttribute(GenericSearchUIData.RC_LANGUAGE);
        }
        GenericSearchComparator gsComp = new GenericSearchComparator(sortSeq, pty.getType(), dateFormat, numberFormat, language, pty.getTranslationPrefix());
        documents.sort(pty.getName(), gsComp);
        // Set sorting info value
        sortInfo.put(pty.getName(), Integer.toString(sortSeq));
    }
    
    /**
     * Use this method to add the return data as <code>ResultData</code> to the
     * UserSession using the contants<br>
     * <code>SC_GENERICSEARCH_RET_COUNTED_DOCS</code><br>
     * <code>SC_GENERICSEARCH_RET_DOCUMENTS</code><br>
     * <code>SC_GENERICSEARCH_RET_FIELDS</code><br>
     * suffixed by the search name
     * @param request   The request object
     * @param retData   Generic search return data object
     */
    protected void addReturnDataToInstance(GenericSearchInstance searchInstance,
                                          GenericSearchReturn retData) {
        
    	searchInstance.setCountedDocuments(new ResultData(retData.getCountedDocuments()));
    	searchInstance.setDocuments(new ResultData(retData.getDocuments()));
    	searchInstance.setRequestedFields(new ResultData(retData.getRequestedFields()));
        searchInstance.setBackendMessages(new ResultData(retData.getReturnTableMessages()));
    }	
   	

    /**
     * Use this method to add the return data as <code>ResultData</code> to the
     * UserSession using the contants<br>
     * <code>SC_GENERICSEARCH_RET_COUNTED_DOCS</code><br>
     * <code>SC_GENERICSEARCH_RET_DOCUMENTS</code><br>
     * <code>SC_GENERICSEARCH_RET_FIELDS</code><br>
     * suffixed by the search name
     * @param request   The request object
     * @param retData   Generic search return data object
     * @deprecated 
     */
    protected void addReturnDataToSession(UserSessionData session,
                                          GenericSearchReturn retData,
                                          String requestedSearch) {
        session.setAttribute(
            (GenericSearchBaseAction.SC_GENERICSEARCH_RET_COUNTED_DOCS + requestedSearch), new ResultData(retData.getCountedDocuments()));
        session.setAttribute(
            (GenericSearchBaseAction.SC_GENERICSEARCH_RET_DOCUMENTS + requestedSearch),    new ResultData(retData.getDocuments()));
        session.setAttribute(
            (GenericSearchBaseAction.SC_GENERICSEARCH_RET_FIELDS + requestedSearch),       new ResultData(retData.getRequestedFields()));
    }

    
    /**
     * Use this method to add the return data as <code>ResultData</code> to the
     * UserSession using the contants<br>
     * <code>SC_GENERICSEARCH_RET_COUNTED_DOCS</code><br>
     * <code>SC_GENERICSEARCH_RET_DOCUMENTS</code><br>
     * <code>SC_GENERICSEARCH_RET_FIELDS</code><br>
     * suffixed by the search name
     * 
     * @param request   The request object
     * @param retData   Generic search return data object
     * @deprecated 
     */
    protected void addReturnDataToSession(UserSessionData session,
                                          GenericSearchInstance instance,
                                          String requestedSearch) {
        session.setAttribute(
            (GenericSearchBaseAction.SC_GENERICSEARCH_RET_COUNTED_DOCS + requestedSearch), instance.getCountedDocuments());
        session.setAttribute(
            (GenericSearchBaseAction.SC_GENERICSEARCH_RET_DOCUMENTS + requestedSearch),    instance.getDocuments());
        session.setAttribute(
            (GenericSearchBaseAction.SC_GENERICSEARCH_RET_FIELDS + requestedSearch),       instance.getRequestedFields());
    }

    
    /**
     * Use this method to add the return data as <code>ResultData</code> to the
     * UserSession using the contants<br>
     * <code>SC_GENERICSEARCH_RET_COUNTED_DOCS</code><br>
     * <code>SC_GENERICSEARCH_RET_DOCUMENTS</code><br>
     * <code>SC_GENERICSEARCH_RET_FIELDS</code><br>
     * suffixed by the search name
     * @param request   The request object
     * @param retData   Generic search return data object
     * @deprecated 
     */
    protected void addReturnDataToRequest(HttpServletRequest request,
                                          GenericSearchReturn retData,
                                          String requestedSearch) {
        request.setAttribute(
            (GenericSearchBaseAction.SC_GENERICSEARCH_RET_COUNTED_DOCS + requestedSearch), new ResultData(retData.getCountedDocuments()));
        request.setAttribute(
            (GenericSearchBaseAction.SC_GENERICSEARCH_RET_DOCUMENTS + requestedSearch),    new ResultData(retData.getDocuments()));
        request.setAttribute(
            (GenericSearchBaseAction.SC_GENERICSEARCH_RET_FIELDS + requestedSearch),       new ResultData(retData.getRequestedFields()));
    }

    
    /**
     * Use this method to add the return data as <code>ResultData</code> to the
     * UserSession using the contants<br>
     * <code>SC_GENERICSEARCH_RET_COUNTED_DOCS</code><br>
     * <code>SC_GENERICSEARCH_RET_DOCUMENTS</code><br>
     * <code>SC_GENERICSEARCH_RET_FIELDS</code><br>
     * suffixed by the search name
     * @param request   The request object
     * @param retData   Generic search return data object
     * @deprecated 
     */
    protected void addReturnDataToRequest(HttpServletRequest request,
    									  GenericSearchInstance instance,
                                          String requestedSearch) {
        request.setAttribute(
            (GenericSearchBaseAction.SC_GENERICSEARCH_RET_COUNTED_DOCS + requestedSearch), instance.getCountedDocuments());
        request.setAttribute(
            (GenericSearchBaseAction.SC_GENERICSEARCH_RET_DOCUMENTS + requestedSearch),    instance.getDocuments());
        request.setAttribute(
            (GenericSearchBaseAction.SC_GENERICSEARCH_RET_FIELDS + requestedSearch),       instance.getRequestedFields());
    }

    /**
     * Use this method to set the control options (defined in the XML file). Following are the
     * currently known control options:<br>
     * - maxHitsToSelect<br>
     * @param handle          Handle binding this select options together
     * @param Select Options  Select Options object
     * @param Session         Session object
     * @param Request         HTTP request object
     * @param GenericSearchBuilder
     * @param requestedSearch 
     */
    protected void setControlOptions(int handle,
                                     GenericSearchSelectOptions selOpt,
                                     UserSessionData     userSessionData,
                                     HttpServletRequest  request,
                                     GenericSearchBuilder genSrBuilder,
                                     String requestedSearch) {
        String implFilterSelectedValue = getImplFilterSelectedValue(handle, selOpt);
        String implFilterPtyName = getImplFilterPtyName(handle, selOpt);
        int maxHits = genSrBuilder.getMaxHitsOption(requestedSearch, implFilterPtyName, implFilterSelectedValue);
        selOpt.setControlOptionLine(
                 new GenericSearchControlOptionLine(handle, false, maxHits));
    }
    
    
    /**
     * Use this method to add the requested fields (defined in the XML file) 
     * @param handle          Handle binding this select options together
     * @param Select Options  Select Options object
     * @param Session         Session object
     * @param Request         HTTP request object
     * @param GenericSearchBuilder
     * @param requestedSearch 
     */
    protected void addRequestedFields(int handle,
                                      GenericSearchSelectOptions selOpt,
                                      UserSessionData    userSessionData,
                                      HttpServletRequest request,
                                      GenericSearchBuilder genSrBuilder,
                                      String requestedSearch) {
        String implFilterSelectedValue = getImplFilterSelectedValue(handle, selOpt);
        String implFilterPtyName = getImplFilterPtyName(handle, selOpt);
        GSPropertyGroup ptyGrp = getResultListDescription(request, genSrBuilder, requestedSearch, implFilterPtyName, implFilterSelectedValue);
        GSPropertyGroup ptyGrpMemCheck = genSrBuilder.getSearchCriteriaDescription(requestedSearch);
        
    	// TODO Only use search instance    	
    	GenericSearchInstance searchInstance = (GenericSearchInstance)getAreaContext(request).getAttribute(SC_GENERICSEARCH_INSTANCE);
    	if (searchInstance != null) {
    		searchInstance.setSearchGroup(ptyGrp);
    	}
    	
        // Add to request to make public
        if (! ptyGrpMemCheck.isUseSearchRequestMemory()) {
            request.setAttribute((GenericSearchUIData.RC_RESULTLIST_PTYGRP + requestedSearch), ptyGrp);
        } else {
            userSessionData.setAttribute((GenericSearchUIData.RC_RESULTLIST_PTYGRP + requestedSearch), ptyGrp);
        }

        Iterator itPG = ptyGrp.iterator();
        // Loop over list of Properties. This will avoid taking parameters from
        // Request which are not meant to be for the search
        int fieldIndex = 1;
        while (itPG.hasNext()) {
            GSProperty pty = (GSProperty)itPG.next();
            String fieldName        = pty.getName();
            String attribute        = pty.getParameterType();
            GenericSearchRequestedFieldLine rqFlLine = new GenericSearchRequestedFieldLine(
                                                       handle,
                                                       fieldIndex,
                                                       fieldName,
                                                       attribute);
            selOpt.addRequestedFieldLine(rqFlLine);
            //
            fieldIndex++; 
        }
    }

    /**
     * Use this method to determine the Backend Implementation which should be used.
     * @param handle          Handle binding this select options together
     * @param Select Options
     * @return String containing the BackendImplemenation (i.e. ORDER, QUOTATION, ...) 
     */
    public String getBackendImplemenation(int handle,
                                             GenericSearchSelectOptions selOpt) {
        
        Iterator itSO = selOpt.getSelectOptionLineIterator();
        while (itSO.hasNext()) {
            GenericSearchSelectOptionLine selOptLine = (GenericSearchSelectOptionLine)itSO.next();
            if (selOptLine.getHandle() != handle) {
                continue;
            }
            if ("IMPLEMENTATION_FILTER".equals(selOptLine.getSelect_param())) {
                return selOptLine.getLow();
            }
        }
        
        return null;
    }
    /**
     * Use this method to determine if the property type has a subtype of kind <code>token</code>
     * <p>i.e.<br>
     * type = "box"                 = not of subtype token<br>
     * type = "box(doctypetoken)"   = subtype is a token (doctypetoken)
     * </p>
     * @param Property Type
     * @return True if subtype is of kind <code>token</code<
     */
    protected boolean isSubTypeToken(String ptyType) {
        if (ptyType == null) {
            return false;
        }
        if (ptyType.indexOf("token") > 0) {
            return true;
        }
        return false;
    }
    /**
     * Use this method to get the subtype of the property type
     * <p>i.e.<br>
     * type = "box"           = no sub type<br>
     * type = "box(datetoken) = sub type is datetoken</p> 
     * @param Property Type
     * @return Sub type
     */
    protected static String getSubType(String ptyType) {
        String subType = "";
        if (ptyType != null) {
            int str = ptyType.indexOf("(");
            int end = ptyType.indexOf(")");
            if (str > 0  &&  end > 0) {
                subType = ptyType.substring( (str + 1 ), end );  // Subtype only, no ( )
            }
        }
        return subType;
    }
    /**
     * Use this method to strip of index indicators from the Property name. Index indicators 
     * are suffixes at the property name's enclosed by brackets.
     * <p>i.e.<br>
     * &lt;property name="CREATED_AT(1)" ...     =&gt; CREATED_AT
     * &lt;property name="CREATED_AT(2)" ...     =&gt; CREATED_AT</p>
     * @param ptyName
     * @return String Actuall Name of the Property
     */
    protected static String getActuallName(String ptyName) {
        String actName = "";
        if (ptyName != null) {
            int str = ptyName.indexOf("(");
            int end = ptyName.indexOf(")");
            if (str > 0 && end > 0) {
                // only if index indicator is enclosed by opening and closing brackets it is one!
                actName = ptyName.substring(0,str);
            } else {
                // Contains no brackes
                actName = ptyName;
            }
        }
        return actName;
    }
    /**
     * Use this method to find a determined interface of an object.
     * @param Object which should be searched through
     * @param String full qualified name of the interface
     * @return Class
     */
    protected Class findInterface(Object obj, String iF) {
        Class retVal = null;
        Class[] clIf = null;
        if (obj instanceof Class) {
            clIf = ((Class)obj).getInterfaces();
        } else {
            clIf = obj.getClass().getInterfaces();
        }
        for (int idx = 0; idx < clIf.length; idx++) {
          if (iF.equals(clIf[idx].getName())) {
              retVal = clIf[idx];
          }
        }
        if (retVal == null &&  !"java.lang.Object".equals(obj.getClass().getSuperclass().getName())) {
           retVal = findInterface(obj.getClass().getSuperclass(), iF);
        }
        return retVal;
    }

	/**
	 * Use this method to process Properties of type 'dark'. This class must be used if the signatur
	 * of the <code>init</code> method contains interfaces.
	 * @param UserSessionData session object 
	 * @param objs Array which should include all Objets like BusinessObjectManager, UserSessionData
	 *             and so on in the right sequence of the signature of the <code>init</code> method.<br>
	 * <p>example: (
	 *   Object[] objs = new Object[4];
	 *   objs[0] = bom;
	 *   objs[1] = request;
	 *   objs[2] = selOpt;
	 *   objs[3] = handle;
	 * </p>
	 * @param sig Array which must be correspond to the objs array identifying the signature of 
	 *        the method to be called.
	 * <p>example: (
	 *   Class[] sig = new Class[4];
	 *   sig[0] = bom.getClass();
	 *   sig[1] = findInterface(request, "javax.servlet.http.HttpServletRequest");
	 *   sig[2] = ...
	 * </p>
	 */
	public void handleDarkAttributes(UserSessionData userSessionData,
									  int handle,
									  GenericSearchSelectOptions selOpt,
									  HttpServletRequest request,
									  MetaBusinessObjectManager mbom) {
     }
									  	
    /**
     * Use this method to process Properties of type 'dark'. This class must be used if the signatur
     * of the <code>init</code> method contains interfaces.
     * @param UserSessionData session object 
     * @param objs Array which should include all Objets like BusinessObjectManager, UserSessionData
     *             and so on in the right sequence of the signature of the <code>init</code> method.<br>
     * <p>example: (
     *   Object[] objs = new Object[4];
     *   objs[0] = bom;
     *   objs[1] = request;
     *   objs[2] = selOpt;
     *   objs[3] = handle;
     * </p>
     * @param sig Array which must be correspond to the objs array identifying the signature of 
     *        the method to be called.
     * <p>example: (
     *   Class[] sig = new Class[4];
     *   sig[0] = bom.getClass();
     *   sig[1] = findInterface(request, "javax.servlet.http.HttpServletRequest");
     *   sig[2] = ...
     * </p>
     */
    public void processDarkAttributes(UserSessionData userSessionData,
                                      HttpServletRequest request, 
                                      Object[] objs,
                                      Class[] sig) {
         // Reference to the GenericSearchBuilder class instance
         GenericSearchBuilder genSrBuilder = getGenericSearchBuilder(userSessionData);
         // Name of the requested search
         String requestedSearch = getScreenName(request, userSessionData);

         GSPropertyGroup ptyGrp = genSrBuilder.getSearchCriteriaDescription(requestedSearch);
         Object newObj = null;
        
         Iterator itPG = ptyGrp.iterator();

         // Loop over list of Properties to find ones of type 'dark'
         while (itPG.hasNext()) {
             GSProperty pty = (GSProperty)itPG.next();
                
             if ( ! "dark".equals(pty.getType())) {
                 continue;  // This type can't be processed here => skip this property
             }

             // Do the initialzation of the Dynamic Content class
             StringBuffer creationStep = new StringBuffer("Object could not be created: " +
                                   getDynamicContentClassForProperty(pty));
             try {
                 creationStep.append("+Instanciation");
                 String dcClassName = getDynamicContentClassForProperty(pty);
                 String dcClassMethod = getDynamicContentMethodForProperty(pty);
                 if (dcClassName != null  ||  dcClassMethod != null) {
                     newObj = (Class.forName(dcClassName)).newInstance(); 
                 }
             } catch (ClassNotFoundException ex) {
                 log.error(LogUtil.APPS_USER_INTERFACE, creationStep);
                 log.error(LogUtil.APPS_USER_INTERFACE, "", ex);
                 ex.printStackTrace();
             } catch (InstantiationException ex) {
                 log.error(LogUtil.APPS_USER_INTERFACE, creationStep);
                 log.error(LogUtil.APPS_USER_INTERFACE, "", ex);
                 ex.printStackTrace();
             } catch (IllegalAccessException ex) { 
                 log.error(LogUtil.APPS_USER_INTERFACE, creationStep);
                 log.error(LogUtil.APPS_USER_INTERFACE, "", ex);
                 ex.printStackTrace();
             }
             // Object could not be created => quit
             if (newObj == null) {
                 return;
             }

             Class cl = newObj.getClass();

             // Build class array to allow reflect api finding the right method with the
             // right signature
             Class[] classSig = null;
             Object[] objsArg = null;
             if (sig != null) {
                 classSig = new Class[(sig.length + 1)];
                 objsArg = new Object[(objs.length + 1)];
                 for(int idx = 0; idx < objs.length; idx++) {
                     classSig[idx] = sig[idx];
                     objsArg[idx] = objs[idx];
                 }
             } else {
                 classSig = new Class[(objs.length + 1)];
                 objsArg = new Object[(objs.length + 1)];
                 for(int idx = 0; idx < objs.length; idx++) {
                     classSig[idx] = objs[idx].getClass();
                     objsArg[idx] = objs[idx];
                 }
             }
             classSig[(objs.length)] = pty.getClass();     // ADD Property object as last Argument
             objsArg[(objs.length)] = pty;                 //         "             "


             try { 
                 creationStep.append("+init");
                 Method meth = cl.getMethod("init", classSig );
                 meth.invoke(newObj, objsArg);

             } catch (NoSuchMethodException ex) {
                 log.error(LogUtil.APPS_USER_INTERFACE, creationStep);
                 log.error(LogUtil.APPS_USER_INTERFACE, "", ex); 
                 ex.printStackTrace(); 
             } catch (InvocationTargetException ex) { 
                 log.error(LogUtil.APPS_USER_INTERFACE, creationStep);
                 log.error(LogUtil.APPS_USER_INTERFACE, "", ex);
                 ex.printStackTrace(); 
             } catch (IllegalAccessException ex) { 
                 log.error(LogUtil.APPS_USER_INTERFACE, creationStep);
                 log.error(LogUtil.APPS_USER_INTERFACE, "", ex);
                 ex.printStackTrace();
             }
 
             // Now invoke method defined in the attribute "contentCreateMethod" (WITHOUT arguments !)
             creationStep = new StringBuffer("Object could not be created: " +
                            getDynamicContentMethodForProperty(pty));
             try {
                 cl = newObj.getClass();
                 Method meth = cl.getMethod(getDynamicContentMethodForProperty(pty)
                                           , new Class[] {} );
                 meth.invoke(newObj, new Class[] {});

             } catch (NoSuchMethodException ex) {
                 log.error(LogUtil.APPS_USER_INTERFACE, creationStep);
                 log.error(LogUtil.APPS_USER_INTERFACE, "", ex); 
                 ex.printStackTrace(); 
             } catch (InvocationTargetException ex) { 
                 log.error(LogUtil.APPS_USER_INTERFACE, creationStep);
                 log.error(LogUtil.APPS_USER_INTERFACE, "", ex);
                 ex.printStackTrace(); 
             } catch (IllegalAccessException ex) { 
                 log.error(LogUtil.APPS_USER_INTERFACE, creationStep);
                 log.error(LogUtil.APPS_USER_INTERFACE, "", ex);
                 ex.printStackTrace();
             }
            
         }

    }
    /**
     * Use this method to process Properties of type 'dark'.
     * @param UserSessionData session object 
     * @param objs Array which should include all Objets like BusinessObjectManager, UserSessionData
     *             and so on in the right sequence of the signature of the <code>init</code> method.<br>
     * <p>example: (
     *   Object[] objs = new Object[4];
     *   objs[0] = bom;
     *   objs[1] = userSessionData;
     *   objs[2] = selOpt;
     *   objs[3] = handle;
     * </p>
     */
    public void processDarkAttributes(UserSessionData userSessionData,
                                      HttpServletRequest request, 
                                      Object[] objs) {
        processDarkAttributes(userSessionData, request, objs, null);
    }
    /**
     * Returns the class name which is defined in the AllowedValue 
     * "contentCreateClass" attribute
     * @param pty Property
     * @return String Class name or null
     */
    protected String getDynamicContentClassForProperty(GSProperty pty) {
        String className = "";
        
        Iterator itAV = pty.iterator();
        while (itAV.hasNext()) {
            GSAllowedValue av = (GSAllowedValue)itAV.next();
            if ("dynamic".equals(av.getContent())) {
                // Only Dynamic ones should have a Content class
                className = av.getContentCreateClass();
            }
        }
        
        return className;
    }
    /**
     * Returns the Method name which is defined in the AllowedValue 
     * "contentCreateClass" attribute
     * @param pty Property
     * @return String Class name or null
     */
    protected String getDynamicContentMethodForProperty(GSProperty pty) {
        String methodName = "";
        
        Iterator itAV = pty.iterator();
        while (itAV.hasNext()) {
            GSAllowedValue av = (GSAllowedValue)itAV.next();
            if ("dynamic".equals(av.getContent())) {
                // Only Dynamic ones should have a Content class method
                methodName = av.getContentCreateMethod();
            }
        }
        
        return methodName;
    }
    /**
     * Use this method to determine the value which has been selected for the 
     * Property which is marked as <b>implementationFilter</b>.
     * @param handle          Handle binding this select options together
     * @param Select Options
     * @return String containing selected Value (i.e. ORDER, QUOTATION, ...) 
     */
    protected String getImplFilterSelectedValue(int handle, GenericSearchSelectOptions selOpt) {
        Iterator itSO = selOpt.getSelectOptionLineIterator();
        while (itSO.hasNext()) {
            GenericSearchSelectOptionLine selOptLine = (GenericSearchSelectOptionLine)itSO.next();
            if (selOptLine.getHandle() != handle) {
                continue;
            }
            if ("IMPLEMENTATION_FILTER".equals(selOptLine.getSelect_param())) {
                return selOptLine.getLow();
            }
        }
        
        return null;

    }
    /**
     * Use this method to determine the name of the implementation filter property. 
     * Property which is marked as <b>implementationFilter</b> and actually used in
     * the request.<br>
     * Can only be used AFTER method <code>parseParameters</code>.
     * @param handle          Handle binding this select options together
     * @param Select Options
     * @return String containing Property Name (i.e. document_types, ...) 
     */
    protected String getImplFilterPtyName(int handle, GenericSearchSelectOptions selOpt) {
        Iterator itSO = selOpt.getSelectOptionLineIterator();
        while (itSO.hasNext()) {
            GenericSearchSelectOptionLine selOptLine = (GenericSearchSelectOptionLine)itSO.next();
            if (selOptLine.getHandle() != handle) {
                continue;
            }
            if ("IMPLEMENTATION_FILTER".equals(selOptLine.getSelect_param())) {
                return selOptLine.getHigh();
            }
        }
        
        return null;

    }
    /** 
     * Get the Search Request Memory Object from the UserSessionData. The Object is related to the 
     * used search. If it doesn't exist yet it will be created. 
     * (@see com.sap.isa.ui.uiclass.genericsearch.GenericSearchBaseUI.init())
     * @param UserSessionData   sessio object
     * @param String            Name of the search description
     * @return Map              Search request memory
     */
    protected Map getSearchRequestMemory(UserSessionData userSessionData, String requestedSearch) {
        Map srRqMm = (Map)userSessionData.getAttribute(GenericSearchUIData.SC_SEARCHREQUEST_MEMORY + requestedSearch);
        if (srRqMm == null) {
                srRqMm = new HashMap();
                userSessionData.setAttribute((GenericSearchUIData.SC_SEARCHREQUEST_MEMORY + requestedSearch), srRqMm);
        }
        return srRqMm;
    }
    /**
     * Get the GenericSearchBuilder Object from the GenericSearchBuilderHelper.
     */
    protected GenericSearchBuilder getGenericSearchBuilder(UserSessionData userSessionData) {
        GenericSearchBuilder genSrBuilder = null;
        // Initialize Helper Object
        GenericSearchBuilderHelper objectHelper = new GenericSearchBuilderHelper();
        ConfigContainer container = userSessionData.getCurrentConfigContainer();
        try {
            genSrBuilder = (GenericSearchBuilder)objectHelper.createObjectFromXCM(GenericSearchBuilder.class.getName(),container, "generic-search-config");
        } catch (MaintenanceObjectHelperException ex) {
            log.error(LogUtil.APPS_USER_INTERFACE, "", ex);
        }
        return genSrBuilder;
    }

    /**
     * Return the Screen name which describes the search criteria in the XML file
     * @return String screen name
     */
    public String getScreenName(HttpServletRequest request, UserSessionData userSessionData) {
    	
    	String searchToDisplay = null;

    	// TODO Only use search instance    	
    	GenericSearchInstance searchInstance = (GenericSearchInstance)getAreaContext(request).getAttribute(SC_GENERICSEARCH_INSTANCE);
    	if (searchInstance != null) {
    		searchToDisplay = searchInstance.getScreenName();
    	}
        if (searchToDisplay == null  ||  searchToDisplay.length() <= 0) {
        	searchToDisplay = request.getParameter(GenericSearchUIData.RC_SEARCHCRITERASCREENNAME);
        }
        if (searchToDisplay == null  ||  searchToDisplay.length() <= 0) {
            searchToDisplay = (String)request.getAttribute(GenericSearchUIData.RC_SEARCHCRITERASCREENNAME);
        }
        if (searchToDisplay == null  ||  searchToDisplay.length() <= 0) {
            DocumentHandler documentHandler = (DocumentHandler)userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
            if (documentHandler != null) {
                searchToDisplay = documentHandler.getOrganizerParameter(GenericSearchUIData.RC_SEARCHCRITERASCREENNAME);
            }
        }
        return searchToDisplay;
    }
    /** 
     * Return the Propert Group describing a result list. 
     * @param HttpServletRequest request
     * @param GenericSearchBuilder genSrBuilder
     * @param String requestedSearch
     * @param String implFilterPtyName
     * @param String implFilterSelectedValue
     * @return
     */
    protected GSPropertyGroup getResultListDescription(HttpServletRequest request, GenericSearchBuilder genSrBuilder,
                                                    String requestedSearch, String implFilterPtyName, 
                                                    String implFilterSelectedValue) {
        GSPropertyGroup ptyGrp = null;
        String resultListName = null;
    	// TODO Only use search instance
        
    	GenericSearchInstance searchInstance = (GenericSearchInstance)getAreaContext(request).getAttribute(SC_GENERICSEARCH_INSTANCE);
    	if (searchInstance != null) {
    		resultListName = searchInstance.getResultListName();
    	}
        if (resultListName == null) {
        	request.getParameter(GenericSearchUIData.RC_RESULTLISTNAME);
        }	
        
        if (resultListName != null) {
            // result list has been specified in the request
            ptyGrp = (GSPropertyGroup)genSrBuilder.getPropertyGroupByName(resultListName, GenericSearchBuilder.TYPE_RESULT);
        } else {
            ptyGrp = genSrBuilder.getResultListDescription(requestedSearch, implFilterPtyName, implFilterSelectedValue);
        } 
        return ptyGrp;
    }

    /**
     * Convert given Date String into format yyyymmdd
     * @param String date template  (i.e. dd/mm/yyyy)
     * @param String date string
     * @return String date string in format yyyymmdd
     */
    protected String formatDateL(String dateTemplate, String inDate) {
        String dateTmp = dateTemplate.toUpperCase();
        String separator = "";
        // First find separator " . / - "
        int sepIdx = dateTmp.indexOf(".");
        if (sepIdx > -1) {
            separator = ".";
        }
        sepIdx = dateTmp.indexOf("-");
        if (sepIdx > -1) {
            separator = "-";
        }
        sepIdx = dateTmp.indexOf("/"); 
        if (sepIdx > -1) {
            separator = "/";
        }
        // Split into single tokens
        StringTokenizer inST = new StringTokenizer(inDate, separator);
        StringTokenizer tmST = new StringTokenizer(dateTmp, separator);
        String dd   = "",
               mm   = "",
               yyyy = "";
        while (tmST.hasMoreElements()) {
          String tokTM = (String)tmST.nextElement();
          String tokIN = (String)inST.nextElement();
          if ("DD".equals(tokTM)) {
              if (tokIN.length() == 1) {
                  dd = "0" + tokIN;
              } else {
                  dd = tokIN;
              }
          }
          if ("MM".equals(tokTM)) {
              if (tokIN.length() == 1) {
                  mm = "0" + tokIN;
              } else {
                  mm = tokIN;
              }
          }
          if ("YYYY".equals(tokTM)) {
              yyyy = tokIN;
          }

        }
        String outDate = yyyy + mm + dd;
        return outDate;
    }
}
