/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       SAP AG
  Class         OciReceiveAction
  Description:  Action for receiving items via the OCI

  Created:      29 May 2001
  Version:      0.1

  $Revision: #1 $
  $Date: 2001/06/26 $
*****************************************************************************/

package com.sap.isa.isacore.action;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.backend.boi.isacore.oci.OciConverterData;
import com.sap.isa.businessobject.BusinessObjectException;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.oci.OciConverter;
import com.sap.isa.businessobject.oci.OciParser;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.action.order.AddToBasketAction;


/**
 *
 * Receive the request from an external catalog and read the items contained in the request.<br>
 * ~OkCode indicates if items must be placed into the shopping basket. ADDI : add to basket<br>
 *<br>
 * ~TARGET specifies the frame to which a catalog is to return in a frame-based environment.<br>
 *<br>
 * ~CALLER indicates that the data was sent by an external catalog. Must be CTLG<br>
 *<br>
 * Currently we support the parameters:<br>
 *   productKey : the SAP product master number, char40<br>
 *   quantity   : the quantity of the item, char15, max 11 digits to the left of the decimal point and exactly 3 digits to the right<br>
 *   uom        : the unit of measurement, char3, standard iso code<br>
 *<br>
 * These parameters are encoded as:<br>
 *   productKey : NEW_ITEM-MATNR[n]<br>
 *   quantity   : NEW_ITEM-QUANTITY[n]<br>
 *   uom        : NEW_ITEM-UNIT[n]<br>
 *   where n, enclosed in []s is the index of the item.<br>
 *<br>
 * Note that all Action classes have to provide a non argument constructor<br>
 * because they were instantiated automatically by the action servlet.<br>
 *<br>
 * @author Alexander Staff<br>
 * @version 1.0<br>
 *
 */
public class OciReceiveAction extends IsaCoreBaseAction {

	/**
	 * Constant to identify that items have been received via OCI
	 */
	public static final String SC_OCI_RECEIVE ="ocireceive";

	/**
	 * Initialize the action.
	 * In B2C this action could be used by anonymous user.
	 * 
	 * @see com.sap.isa.isacore.action.IsaCoreBaseAction#initialize()
	 */
	public void initialize() {
		if (isB2C()) {
			this.checkUserIsLoggedIn = false;
		}	
	}


    /**
     * Implement this method to add functionality to your action.
     *
     * @param mapping           The <code>ActionMapping</code> passed by struts
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            BusinessObjectManager bom,
            IsaLocation log) throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        // Page that should be displayed next.
        String forwardTo = "success";
        
		userSessionData.removeAttribute(OciReceiveAction.SC_OCI_RECEIVE);
        
        // get the shop
        Shop shop = bom.getShop();
        
		if (shop == null) {
			throw new PanicException("shop.notFound");
		}

		// if customer likes to override forward, this string is used
		String customerForwardTo = null;
        
        // used Version of OCI
        StringBuffer ociVersion = new StringBuffer();

        // list of BasketTransferItems
        List itemList = new ArrayList();

        // I use this to display any error message on the error.jsp
        BusinessObjectException boex = new BusinessObjectException();
        Message                 msg;
//        String[]                msgParams = new String[1];
        int                     requestCheck;

        request.setAttribute("Exception",boex);
        // init the boex, insert some header line for the error messages
        msg = new Message( Message.ERROR, "oci.errorTitle", null, "" );
        boex.addMessage(msg);

        requestCheck = CheckRequest(requestParser, request, boex, ociVersion );
        if (requestCheck == OciConverterData.OCI_RECEIVE_SUCCESS) {
            // ok, continue

            RequestParser.Parameter rparam;

            // decide which kind of form we have
            // we change to the behaviour like it is used by EBP
            // check if we have a parameter or attribute called "~xmlDocument" in
            // the request. if we have, then we do XML-parsing, if not, we assume that
            // HTML is used.
            rparam = requestParser.getParameter("~xmlDocument");
            if (!rparam.isSet()) {
                // looking for an xml document in the request attribute
                // if no parameter could be found
                rparam = requestParser.getAttribute("~xmlDocument");
            }
            
            String requestForm;
            if (rparam.isSet()) {
                requestForm = "XML";
            }
            else {
                requestForm = "HTML";
            }
            
            if (log.isDebugEnabled()) {
            	log.debug("OCI Form: " + requestForm);
            }
                
            // read the raw parameters from the request and create some BasketTransferItems with them

            // parsing is always something that can be multi-thread-sensitive
            // so create a local object for parsing the request
            OciParser ociP = new OciParser();
            ociP.setOciVersion(ociVersion.toString());
            ociP.setShop(shop);

            if ( OciParser.OCI_PARSE_SUCCESS != ociP.ParseRequest( request,
                                                                   requestParser,
                                                                   itemList,
                                                                   boex,
                                                                   requestForm)) {
                // error while parsing the request
                // display the error-page with some  messages
                forwardTo = "error";
            }
            else {
                // handle an empty list
                if (itemList.size() == 0) {
                    // same as above, ~OkCode != "ADDI"
                }
                else {
                	
                	// user exit, to modify itemList before convertOciRequest, if necessary
                	userExitModifyItemList(itemList, boex, bom, userSessionData);
                	
					// user exit, to modify itemList before convertOciRequest, if necessary
					userExitModifyItemListBeforeConvertRequest(requestParser, request, ociVersion, itemList, boex, bom, userSessionData);
                	
                    // convert the uom and number formats to the internal format
                    if (OciConverter.OCI_CONVERT_SUCCESS ==
                                convertOciRequest(itemList, boex, bom, userSessionData.getLocale())) {
                                	
                        // user exit, to modify itemList before AddToBasketAction, if necessary
                        userExitModifyItemListBeforeAddToRequest(itemList, boex, bom, userSessionData);

						// user exit, to modify itemList before AddToBasketAction, if necessary
						userExitModifyItemListBeforeAddToBasket(requestParser, request, ociVersion, itemList, boex, bom, userSessionData);
                        
						userSessionData.setAttribute(SC_OCI_RECEIVE, "true");
                        
                        // store the list in the request
                        AddToBasketAction.setRequestAttribute(request, itemList);
                        // and forward to the "AddToBasketAction"
                        forwardTo = "success";
                    }
                    else {
                        // some error, but how, use a boex again ?
                        forwardTo = "error";
                    }
                }
            }
        }
        else {
            forwardTo = "error";
        }
        
		// user exit, to modify forward, if necessary
		customerForwardTo = customerExitDispatcher(requestParser, request, userSessionData, bom, ociVersion);
		forwardTo = (customerForwardTo == null) ? forwardTo : customerForwardTo;
		
		DocumentHandler documentHandler = (DocumentHandler)
						userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);

		if (documentHandler != null && documentHandler.isCatalogOnTop()) {
			request.setAttribute(ActionConstants.RA_FORWARD, "backToCatalog");
		}
        
        log.exiting();
        return mapping.findForward( forwardTo );
    }

    /**
     * Check the passed request-attributes
     *
     * @param requestParser     Parser to simple retrieve data from the request
     * @param request           The request
     * @param boex The businessobjectexception-object that any errors will be fixed to, if any.
     */
    public int CheckRequest(RequestParser requestParser,
                            HttpServletRequest request,
                            BusinessObjectException boex,
                            StringBuffer ociVersion ) {

        // I use this to display any error message on the error.jsp
        Message                 msg;
        String[]                msgParams = new String[1];
        int                     retCode = OciConverterData.OCI_RECEIVE_SUCCESS;

        // check the ~OkCode first
        RequestParser.Parameter rparam;
        rparam = requestParser.getParameter("~OkCode");
        if (rparam.isSet()) {
            if (! "ADDI".equals(rparam.getValue().getString()) ){
                // ~OkCode != "ADDI", nothing
                // return to the page where the catalog was called from ?
                // define some "returnPage" for this case
            }
        }
        else {
            // no ~OkCode : error, but continue parsing for required value ~CALLER
            // provide some error message here
            msgParams[0] = "~OkCode";
            msg = new Message( Message.ERROR, "oci.requiredParameterMissing", msgParams, "" );
            boex.addMessage(msg);

            retCode = OciConverterData.OCI_RECEIVE_FAILED;
        }

        // check for ~CALLER now, must be "CTLG" here
        rparam = requestParser.getParameter("~CALLER");
        if (rparam.isSet()) {
            if (! "CTLG".equals(rparam.getValue().getString()) ){
                // ~OkCode != "ADDI", nothing
                // return to the page where the catalog was called from ?
                // define some "returnPage" for this case
                retCode = OciConverterData.OCI_RECEIVE_FAILED;
            }
        }
        else {
            // no ~CALLER : error, do NOT continue
            // provide some error message here
            msgParams[0] = "~CALLER";
            msg = new Message( Message.ERROR, "oci.requiredParameterMissing", msgParams, "" );
            boex.addMessage(msg);

            retCode = OciConverterData.OCI_RECEIVE_FAILED;
        }

        // check for OCI_VERSION now, must be 2.0 or 3.5 here
        rparam = requestParser.getParameter("OCI_VERSION");
        if (rparam.isSet()) {
            ociVersion.append(rparam.getValue().getString());
            if (! "2.0".equals(ociVersion.toString()) &&
                ! "3.5".equals(ociVersion.toString()) ){
                // "OCI_VERSION" is invalid
                // return to the page where the catalog was called from ?
                // define some "returnPage" for this case
                // provide some error message here
                msgParams[0] = "OCI_VERSION";
                msg = new Message( Message.ERROR, "oci.unsupportedversion", msgParams, "" );
                boex.addMessage(msg);
                
                if (log.isDebugEnabled()) {
                	log.debug("OCI_VERSION: " + ociVersion.toString());
                }

                retCode = OciConverterData.OCI_RECEIVE_FAILED;
            }
        }
        else {
            // no OCI_VERSION : error, do NOT continue
            // provide some error message here
            msgParams[0] = "OCI_VERSION";
            msg = new Message( Message.ERROR, "oci.requiredParameterMissing", msgParams, "" );
            boex.addMessage(msg);

            retCode = OciConverterData.OCI_RECEIVE_FAILED;
        }
        
		// user exit, to modify retCode, if necessary
		retCode = userExitCheckRequest(requestParser, request, boex, ociVersion, retCode);
		
		
        return retCode;
    }

    /**
     * convert the uom and the numbers in the OCI-Request to the internal format
     *
     * @param itemList List of the passed items in the OCI-Request
     * @param boex The object to tie any errors or warnings to
     * @param bom  We need the user for the isoCode, get it from the bom
     * @param locale The locale, what else
     * @return ret OCI_CONVERT_SUCCESS if successful
     */
    public int convertOciRequest(   List                    itemList,
                                    BusinessObjectException boex,
                                    BusinessObjectManager   bom,
                                    Locale                  locale) 
									throws CommunicationException {

        int             ret;
        OciConverter    ociConv;
        Shop            shop;

        ociConv = bom.createOciConverter();
        if (ociConv != null) {
            shop = bom.getShop();
            ret = ociConv.convertUnits(locale, itemList, boex, shop);
        } else {
        	ret = OciConverterData.OCI_INIT_FAILURE;
        }

        return ret;
    }


	/**
	* Template method to be overwritten by the subclasses of this class.
	* This method provides the possibilty, to easily change the forward
	* of an action, accordingly to customer requirements. if this method
	* returns nothing, the default forward is used.<br>
	* By default this method returns <b>null</b>.<br>
	* <code>customerExitDispatcher</code> method.
	*
	* @param requestParser     parser to simple retrieve data from the request
	* @param request			request context
	* @param userSessionData   object wrapping the session
	* @param bom			    reference to the BusinessObjectManager
	* @param ociVersion   		used version of OCI
	* 
	* @return logical key for a forward to another action or jsp
	*
	*/
	public String customerExitDispatcher(RequestParser requestParser,
										  HttpServletRequest request,
										  UserSessionData userSessionData,
										  BusinessObjectManager bom,
										  StringBuffer ociVersion) {
				
			   return null;
	}


	/**
	* Template method to be overwritten by the subclasses of this class.
	* This method provides the possibilty, to easily change the return
	* code, accordingly to customer requirements. By default this method returns
	* the <b>default return code</b>.<br> 
	* <code>customerExitCheckRequest</code> method.
	*
	* @param requestParser     parser to simple retrieve data from the request
	* @param request			request context
	* @param boex			    The object to tie any errors or warnings to
	* @param retCode      		the return code
	* 
	* @return retCode			return code
	*
	*/
    public int userExitCheckRequest(RequestParser requestParser,
									 HttpServletRequest request,
									 BusinessObjectException boex,
									 StringBuffer ociVersion,
									 int retCode) {
									 	
		return retCode;

	}

	/**
	 * Template method to be overwritten by the subclasses of this class.
	 * This method provides the possibilty, to easily modify the itemList
	 * before calling the method convertOciRequest, accordingly to special
	 * customer requirements. By default this method is empty.
	 * <code>customerExitModifyItemList</code> method.
	 *
	 * @param itemList		    List of items
	 * @param boex			    The object to tie any errors or warnings to
	 * @param bom			    reference to the BusinessObjectManager
	 * @param userSessionData  object wrapping the session
	 *
	 * @deprecated use method #userExitModifyItemListBeforeConvertRequest
	 * instead
	 */
	public void userExitModifyItemList(List itemList,
										BusinessObjectException boex,
										BusinessObjectManager bom,
										UserSessionData userSessionData) {
															   									 
	}

	/**
	 * Template method to be overwritten by the subclasses of this class.
	 * This method provides the possibilty, to easily modify the itemList
	 * before calling the method convertOciRequest, accordingly to special
	 * customer requirements. By default this method is empty.
	 * <code>customerExitModifyItemList</code> method.
	 *
	 * @param requestParser    parser to simple retrieve data from the request
	 * @param request			request context
	 * @param ociVersion   	used version of OCI
	 * @param itemList		    List of items
	 * @param boex			    The object to tie any errors or warnings to
	 * @param bom			    reference to the BusinessObjectManager
	 * @param userSessionData  object wrapping the session
	 *
	 */
	public void userExitModifyItemListBeforeConvertRequest(RequestParser requestParser,
															HttpServletRequest request,
															StringBuffer ociVersion,
															List itemList,
															BusinessObjectException boex,
															BusinessObjectManager bom,
															UserSessionData userSessionData) {
															   									 
	}
	
	/**
	 * Template method to be overwritten by the subclasses of this class.
	 * This method provides the possibilty, to easily modify the itemList
	 * before the items stored in the request, accordingly to special customer
	 * requirements. By default this method is empty.
	 * <code>customerExitItemListBeforeAddToRequest</code> method.
	 *
	 * @param itemList		    List of items
	 * @param boex			    The object to tie any errors or warnings to
	 * @param bom			    reference to the BusinessObjectManager
	 * @param userSessionData  object wrapping the session
	 *  
	 * @deprecated use method #userExitModifyItemListBeforeAddToBasket instead.
	 */
	public void userExitModifyItemListBeforeAddToRequest(List itemList,
														  BusinessObjectException boex,
														  BusinessObjectManager bom,
														  UserSessionData userSessionData) {
															   	
	}
	
	/**
	 * Template method to be overwritten by the subclasses of this class.
	 * This method provides the possibilty, to easily modify the itemList
	 * before the items stored in the request, accordingly to special customer
	 * requirements. By default this method is empty.
	 * <code>customerExitItemListBeforeAddToRequest</code> method.
	 *
	 * @param requestParser    parser to simple retrieve data from the request
	 * @param request			request context
	 * @param ociVersion   	used version of OCI
	 * @param itemList		    List of items
	 * @param boex			    The object to tie any errors or warnings to
	 * @param bom			    reference to the BusinessObjectManager
	 * @param userSessionData  object wrapping the session
	 *  
	 */
	public void userExitModifyItemListBeforeAddToBasket(RequestParser requestParser,
														 HttpServletRequest request,
														 StringBuffer ociVersion,
														 List itemList,
														 BusinessObjectException boex,
														 BusinessObjectManager bom,
														 UserSessionData userSessionData) {
															   	
	}
	
}



