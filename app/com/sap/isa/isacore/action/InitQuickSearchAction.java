/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       SAP AG
  Class         InitQuickSearchAction
  Description:  Action for the Initialisation of the QuickSearch in the product tab
                of the order entry screen
  Created:      15 March 2001
  Version:      0.1

  $Revision: #2 $
  $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.isacore.action;

import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.PageContext;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.IsaCatalogAttributes;
import com.sap.isa.businessobject.IsaQuickSearch;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.webcatalog.CatalogBusinessObjectManager;
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.ICatalog;
import com.sap.isa.catalog.impl.CatalogAttribute;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.isacore.IsaCoreInit;


/**
 * Load the attributes of a catalog's products and set up some additional
 * parameters to enable the QuickSearch.
 * If the QuickSearch has been called before, just load the IsaQuickSearch-object from the bom,
 * it already contains all the attributes from the catalog and some other things
 * already loaded from the backend/catalog.
 *
 * <br> Note that all Action classes have to provide a non argument constructor
 * because they were instantiated automatically by the action servlet.
 *
 * @author Alexander Staff
 * @version 0.1
 *
 */
public class InitQuickSearchAction extends IsaCoreBaseAction {

    /**
     * Name of the list of common attributes in the request
     */
    public static final String QS_COMMONATTRIBUTES_LIST = "commonAttributes";

    /**
     * Name of the isaQuickSearch-object
     */
    public static final String QS_OBJECT        = "IsaQuickSearch";
	
    

    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            BusinessObjectManager bom,
            IsaLocation log) throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        // Page that should be displayed next.
        String forwardTo = null;
        
        
        // first check if our IsaQuickSearch is already there - try to load from bom
        IsaQuickSearch isaQuickSearch = bom.getIsaQuickSearch();
        
        // isaQuickSearch already there => redisplay current hit list
        if (isaQuickSearch != null) {
        	if (isaQuickSearch.isValid()) {
        		// current stored hitlist is up to date so display
        		forwardTo = "redisplay";	
        	}
        	else {
        		// no valid hit list exists because last product search has been unsuccessful => display blank search screen
        		// clear all error messages, if any
	            isaQuickSearch.clearMessages();
	            // place the attributes in the request
	            // I need them in the quicksearch.jsp
	            request.setAttribute( QS_COMMONATTRIBUTES_LIST, isaQuickSearch.getAttributes());
	            request.setAttribute( QS_OBJECT, isaQuickSearch);
	            forwardTo = "nodisplay";
        	}
        }
        
        else {
        	// create new quickSearchObject
            isaQuickSearch = createIsaQuickSearch(userSessionData, bom, log);
            
            if (isaQuickSearch == null) {
	            // if isaQuickSearch is not available here
	            // some severe error must have happened
	            // vamos adiosque, wie der spanische Lateiner so sagen w�rde
	            forwardTo = "error";
	        }
	        else {
	            // clear all error messages, if any
	            isaQuickSearch.clearMessages();
	
	            // place the attributes in the request
	            // I need them in the quicksearch.jsp
	            request.setAttribute( QS_COMMONATTRIBUTES_LIST, isaQuickSearch.getAttributes());
	            request.setAttribute( QS_OBJECT, isaQuickSearch);
	            // new quickSearchObject has been created
	            forwardTo = "success";
	        }
        }
		log.exiting();
        return mapping.findForward( forwardTo );
    }

    /**
     * Creates a new IsaQuickSearch-object
     * When this method is called a new quickSearchObject has to be created.
     * Therefore we have to read some data from the catalog first.
     *
     * @param userSessionData A reference to the session-wrapper
     * @param bom A reference to the BusinessObjectManager
     * @param log Reference to the IsaLocation, needed for logging
     * @return isaQuickSearch The created IsaQuickSearch-Object
     *
     */
    private IsaQuickSearch createIsaQuickSearch( UserSessionData userSessionData,
                                               BusinessObjectManager bom,
                                               IsaLocation log)
                        throws CommunicationException {

        

  
        // get the catalog from the bom
        // WebCatInfo, the "father", holding the "pure" catalog
        WebCatInfo webCatInfo = loadWebCatInfo(userSessionData, bom, log);
        if ( webCatInfo == null ) {
            return null;
        }

        // Alway check this flag before log.debug
        if (log.isDebugEnabled()) {
            log.debug("webCatInfo available from here.");
        }

        // webCatInfo is available from here
        // get the "pure"-catalog
        // The catalog (interface) we work with ...
        ICatalog catalog = webCatInfo.getCatalog();
        if ( catalog == null ) {
            log.error(LogUtil.APPS_USER_INTERFACE, "No catalog in WebCatInfo. " + webCatInfo);
        }

        // get the common attributes
        // the Iterator for the attributes from the catalog
        Iterator                iterAttributes          = null;
        try
        {
          iterAttributes =      catalog.getAttributes();
        }
        catch (CatalogException e)
        {
          log.error(LogUtil.APPS_USER_INTERFACE, "Error in catalog attributes",e);
        }
        IsaCatalogAttributes    attributes              = new IsaCatalogAttributes();
        CatalogAttribute        catAttribute;
        IsaCoreInit             cic                     = IsaCoreInit.getInstance();
        List                    attributeExclusionList  = null;

        if (cic == null) {
            // Alway check this flag before log.debug
            if (log.isDebugEnabled()) {
                log.debug("no IsaCoreInit available, no attributes exclusion list either");
            }
        }
        else {
            attributeExclusionList = cic.getAttributeExclusionList();
        }

        while ( iterAttributes.hasNext() ) {
            catAttribute = (CatalogAttribute) iterAttributes.next();

            if (log.isDebugEnabled()) {
                log.debug("attributename : " + catAttribute.getName() );
            }

            // get the attribute-exclusion list from the IsaCoreInit-object and compare
            // the catalog's attributes names with the names in the exclusion list
            // use only the attributes that do NOT occur in the list
            // take care of wildcards.
            if ( isValidAttribute( catAttribute.getName(), attributeExclusionList)) {
                attributes.addAttribute( catAttribute );
            }
        }

        // create the QuickSearch-object
        // it is NOT BackendAware
        // we use the shop's techkey here as the quickSearch is always dependent from the shop
        // the Shop must exist now. No shop, no catalog - no catalog, no quicksearch
        IsaQuickSearch isaQuickSearch = bom.createIsaQuickSearch();

        // attributes are in our IsaAttributesObject now
        // place them in the isaQuickSearch-object
        isaQuickSearch.setAttributes(attributes);
        isaQuickSearch.setWebCatInfo(webCatInfo);

        // set the backend type
        // get the shop from the bom and read it from there
        isaQuickSearch.setBackendType(bom.getShop().getBackend());
        
        return isaQuickSearch;
            
    }

    /**
     * Loads the WebCatalog-object
     * The first try is to load it from the bom. If it is not available there,
     * create it.
     * The Web-Catalog needs some data from the shop to be created. So loading the shop is an
     * absolute prerequisite for this.
     *
     * @param userSessionData A reference to the session-wrapper
     * @param bom A reference to the BusinessObjectManager
     * @param log Reference to the IsaLocation, needed for logging
     * @return WebCatInfo The read or created Web-Catalog-Object
     *
     */
    private WebCatInfo loadWebCatInfo( UserSessionData userSessionData,
                                       BusinessObjectManager bom,
                                       IsaLocation log)
                    throws CommunicationException {

        WebCatInfo  webCatInfo;
        Shop        shop;

        CatalogBusinessObjectManager cbom =
                getCatalogBusinessObjectManager(userSessionData);

        webCatInfo = cbom.getCatalog();

        if (webCatInfo == null ) {

            log.debug( "Catalog == null. load it from backend." );

            webCatInfo = cbom.getCatalog();
        }  // endif webCatInfo == null

        // catalog available from here
        log.debug("catalog available from here");

        return webCatInfo;
    }

    /**
     * Loads the shop
     * Try to load it from the bom. If it is not available there,
     * --> error.
     *
     * @param userSessionData A reference to the session-wrapper
     * @param bom A reference to the BusinessObjectManager
     * @param log Reference to the IsaLocation, needed for logging
     * @return shop The read or created shop
     *
     */
    private Shop loadShop( UserSessionData userSessionData,
                           BusinessObjectManager bom,
                           IsaLocation log)
                throws CommunicationException {

        // The shop we need to get the catalog
        // shop "must" be available now
        // if it is not --> error
        Shop shop = bom.getShop();
        if ( shop == null ) {
            log.error(LogUtil.APPS_USER_INTERFACE, "Shop not available in bom.");
        }

        return shop;
    }

    /**
     * checks if the attributeName is in our exclusionlist. Take care of wildcards
     *
     * @param attName The name of an attribute to check
     * @param ael The list of attributes to exclude
     * @return true if attName is NOT in our exclusionlist
     *
     */
    private boolean isValidAttribute( String attName, List ael ) {

        boolean isValid = true;

        if ( ael != null ) {

            int     size    = ael.size();
            String  check   = null;

            if (log.isDebugEnabled()) {
                log.debug("isValidAttribute - attName: " + attName );
            }

            for ( int i = 0; i < size && isValid; i++  ) {
                // take care of wildcards
                check = (String) ael.get(i);
                if (log.isDebugEnabled()) {
                    log.debug("isValidAttribute - check: " + check );
                }

                if (check.endsWith("*")) {
                    // compare the check with the attName using startsWith()
                    // cut off the '*'
                    check = check.substring(0, check.length() - 1 );

                    // and compare
                    isValid = !(attName.startsWith(check));
                }
                else {
                    // compare for case-insensitive equality
                    isValid = !(attName.equalsIgnoreCase(check));
                }
            }  // endfor
        }

        if (log.isDebugEnabled()) {
            log.debug("isValidAttribute - valid ? " + isValid );
        }

        return isValid;
    }

    /**
     * returns the description of an atribute
     * first it checks if an entry in the property-file exists. if it does not it
     * tries to retrieve the description from the attribute itself. if this does not
     * exist either, return null and the attribute should not be displayed.
     *
     * @param pageContext The pageContext from the calling JSP
     * @param domain The prefix for the search in the property-file
     * @param key The key for the search in the property-file
     * @return description The description of the attribute it it exists.
     *
     */
    public static String getAttributeDescription( PageContext pageContext, String domain, CatalogAttribute attribute) {
        String description = null;
        String key = attribute.getName().toLowerCase();

        // the domain already contains the "." at the end
        description = WebUtil.translate(pageContext, domain + key, null);
        if (description == null || description.indexOf("???") == 0) {
            // no resource-key found, try to get the description from the attribute itself
            description = attribute.getDescription();
        }

        return description;
    }

}