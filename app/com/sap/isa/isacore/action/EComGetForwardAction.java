/*****************************************************************************
    Class         EComGetForwardAction
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Action to forard to a given logical forward
    Author:       SAP
    Created:      February 2002
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/04 $
*****************************************************************************/
package com.sap.isa.isacore.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;

/**
 * Action to read a logical forward from the context to forward to this.
 *
 * The action take the logical forward from the parameter "forward".
 *
 * <h4>Overview over general request parameters and attributes</h4>
 * <table border="1" cellspacing="0" cellpadding="4">
 *   <tr>
 *     <th>name</th>
 *     <th>parameter</th>
 *     <th>attribute</th>
 *     <th>description</th>
 *   <tr>
 *   <tr>
 *      <td>forward</td><td>X</td><td>X</td>
 *      <td>logical forward to be used</td>
 *   </tr>
 * </table>
 *
 * <h4>The following forwards are used by this action</h4>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <th>forward</th>
 *     <th>description</th>
 *   <tr>
 *   <tr><td><em>*</em></td><td><em>all values give with parameter "forward"></em></td></tr>
 * </table>
 * </table>
 *
 * @author SAP AG
 * @version 1.0
 *
 */
public class EComGetForwardAction extends EComBaseAction {
	

	/**
	 * Overwrites the method ecomPerform. <br>
	 * 
	 * @param mapping            The ActionMapping used to select this instance
	 * @param form               The <code>FormBean</code> specified in the
	 *                              config.xml file for this action
	 * @param request            The request object
	 * @param response           The response object
	 * @param userSessionData    Object wrapping the session
	 * @param requestParser      Parser to simple retrieve data from the request
	 * @param mbom               Reference to the MetaBusinessObjectManager
	 * @param multipleInvocation Flag indicating, that a multiple invocation occured
	 * @param browserBack        Flag indicating a browser back
	 * @return Forward to another action or page
	 * 
	 * @throws IOException
	 * @throws ServletException
	 * @throws CommunicationException
	 * 
	 * @see com.sap.isa.isacore.action.EComBaseAction#ecomPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.sap.isa.core.UserSessionData, com.sap.isa.core.util.RequestParser, com.sap.isa.core.businessobject.management.MetaBusinessObjectManager, boolean, boolean)
	 */
	public ActionForward ecomPerform(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response,
			UserSessionData userSessionData,
			RequestParser requestParser,
			MetaBusinessObjectManager mbom,
			boolean multipleInvocation,
			boolean browserBack)
				throws IOException, ServletException, CommunicationException {
		final String METHOD_NAME = "ecomPerform()";
		log.entering(METHOD_NAME);		

	    String forwardAfter= requestParser.getFromContext("forward").getValue().getString();
		log.exiting();
        return mapping.findForward(forwardAfter);

    }
}
