/*****************************************************************************
	Class:        CampaignDisplayAction
	Copyright (c) 2006, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      2006/11/29
    Version:      1.0

    $Revision: #1 $
    $Date: 2006/11/29 $
*****************************************************************************/
package com.sap.isa.isacore.action.campaign;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.ecommerce.businessobject.campaign.Campaign;
import com.sap.ecommerce.businessobject.campaign.CampaignBusinessObjectManager;
import com.sap.ecommerce.businessobject.campaign.CampaignEnrollment;
import com.sap.ecommerce.businessobject.campaign.CampaignEnrollmentList;
import com.sap.ecommerce.businessobject.campaign.CampaignHeader;
import com.sap.isa.backend.boi.isacore.order.DocumentListFilterData;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.businesspartner.businessobject.PartnerFunctionBase;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.MessageDisplayer;
import com.sap.isa.isacore.action.ActionConstants;

/**
 * Copyright (c) 2006, SAP AG, Germany, All rights reserved.
 *
 * Reads campaign data. 
 * 
 */
public class CampaignDisplayAction extends CampaignEnrollmentBaseAction {

    static final private IsaLocation log = IsaLocation.getInstance(CampaignDisplayAction.class.getName());

    /**
     * Constant for available forwards, value is &quot;success&quot;.
     */
    protected final static String FORWARD_SUCCESS = "success";

    /**
     * Constant for maximum number of rows
     */
    public final static int MAX_ENROLLM_LINES = 5;

    /**
     * Implement this method to add functionality to your action.
     * Overriden <em>isaPerform</em> method of <em>IsaCoreBaseAction</em>.
     * The action adds the parameters <code>RC_CAMPAIGN<code> and 
     * <code>RC_RETURN_TYPE_LIST<code> to the request
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
                                    ActionForm form,
                                    HttpServletRequest request,
                                    HttpServletResponse response,
                                    UserSessionData userSessionData,
                                    RequestParser requestParser,
                                    BusinessObjectManager bom,
                                    IsaLocation log)
                         throws CommunicationException {
                            
        final String METHOD = "isaPerform()";
        log.entering(METHOD);

        // Page that should be displayed next.
        String forwardTo = FORWARD_SUCCESS;

        try {        
            Shop shop = bom.getShop();
            if (shop == null) {
                log.exiting();
                throw new PanicException("shop.notFound");
            }
            if (!shop.isEnrollmentAllowed()) {
                log.exiting();
                throw new PanicException("enrollment not allowed");
            }
            
            //get commom and campaign BOM
            CampaignBusinessObjectManager campaignBom = (CampaignBusinessObjectManager)userSessionData.getBOM(CampaignBusinessObjectManager.CAMPAIGN_BOM);

            //Retrieving campaign BO        
            Campaign campaign = (Campaign)campaignBom.getCampaign();
        
            CampaignHeader campHeader = campaign.getHeader();
            String campaignId = campHeader.getCampaignID();
            CampaignEnrollmentList enrollmList = campaign.getCampaignEnrollmentList();       

            // set reference to bupa-manager
            BusinessPartnerManager buPaManager = bom.getBUPAManager();
            if (buPaManager == null) {
                buPaManager = bom.createBUPAManager();
            }
            // set enrollerTechKey in header
            BusinessPartner partner = buPaManager.getDefaultBusinessPartner(PartnerFunctionBase.SOLDTO);
            String enrollerGuid = null;
            if (partner != null) {
                enrollerGuid = partner.getTechKey().getIdAsString();
                campHeader.setEnrollerGuid(enrollerGuid);
            }

        
            // set search criteria - campaign id
            if (campaignId.equals("")) {
                log.error("no campaign key provided");
                MessageDisplayer messageDisplayer = new MessageDisplayer();
                messageDisplayer.addMessage(new Message(Message.ERROR, "ecom.cmp.displ.err.noguid"));
                messageDisplayer.setOnlyBack();
                messageDisplayer.addToRequest(request);
                return mapping.findForward(FORWARD_ERROR);
            }

            String pageStr = request.getParameter(ActionConstants.RC_CAMPAIGN_PAGE);
            if (pageStr == null) {
                pageStr = (String) request.getAttribute(ActionConstants.RC_CAMPAIGN_PAGE);
            }
            if (pageStr == null || pageStr.equals("") || pageStr.equals("null")) {
                pageStr = "1";
            }
            
            // Checks permissions
            User user = bom.getUser();
        
            String[] documents = new String[]{DocumentListFilterData.DOCUMENT_TYPE_CAMPAIGN,
                                              DocumentListFilterData.DOCUMENT_TYPE_CAMPAIGN};
            String[] actions   = new String[]{DocumentListFilterData.ACTION_READ,
                                              DocumentListFilterData.ACTION_CHANGE};                                           
            String[] actvts    = new String[]{DocumentListFilterData.ACTVT_DISPLAY,
                                              DocumentListFilterData.ACTVT_CHANGE};                                           

            Boolean[] checkResults = checkPermissions(user, documents, actions);
                                                        
            boolean hasDispEnrollmPermission   = checkResults[0].booleanValue();
            boolean hasChangeEnrollmPermission = checkResults[1].booleanValue();
            if (!hasDispEnrollmPermission) {
                // clear messages
                campaign.clearMessages();
                // No authorization to display campaign enrollment data
                campaign.addMessage(new Message(Message.ERROR, "ecom.camp.msg.noAuth.display"));
                // Sets the campaign into the request
//                request.setAttribute(ActionConstants.RC_CAMPAIGN, campaign);
                return mapping.findForward(FORWARD_ERROR);
            }
            
            boolean hasChangeEnrollmOBPermission = true;
            //In case enrollee and enroller are different check the required on behalf permissions
            if (!enrollmList.isEnrollerOnly(enrollerGuid)) {       
                try{
                    boolean hasDispEnrollmOBPermission = false;
                    Boolean[] resultsOnBehalf = checkPermissionsOB(user, documents, actvts);
                    hasDispEnrollmOBPermission   = resultsOnBehalf[0].booleanValue();
                    hasChangeEnrollmOBPermission = resultsOnBehalf[1].booleanValue();

                    if (!hasDispEnrollmOBPermission) {
                        // clear messages
                        campaign.clearMessages();
                        // No authorization to display campaign enrollment data
                        campaign.addMessage(new Message(Message.ERROR, "ecom.camp.msg.noAuthOB.display"));
                        // Sets the campaign into the request
//                        request.setAttribute(ActionConstants.RC_CAMPAIGN, campaign);
                        return mapping.findForward(FORWARD_ERROR);
                    }
                }
                catch (CommunicationException Ex) {
                    log.fatal("User has no permission to read the campaign:" + DocumentListFilterData.DOCUMENT_TYPE_CAMPAIGN);
                    log.fatal(LogUtil.APPS_COMMON_SECURITY, "esrv.com.log.err.noread", new Object[] { METHOD, DocumentListFilterData.DOCUMENT_TYPE_CAMPAIGN });
                    MessageDisplayer messageDisplayer = new MessageDisplayer();
                    messageDisplayer.addMessage(new Message(Message.ERROR, "ecom.cmp.displ.err.noguid"));
                    messageDisplayer.setOnlyBack();
                    messageDisplayer.addToRequest(request);
                    throw new PanicException("User has no permission to read campaigns");
                }       
            }

            boolean isChangeable = checkEnrollmPeriod(campaign, (hasChangeEnrollmPermission && hasChangeEnrollmOBPermission), shop);
            String isChangeableStr = (isChangeable) ? "true" : "false"; 

            // current page of enrollment data
            int page = 1;
            if (pageStr != null && !pageStr.equals("")) {
                page = Integer.valueOf(pageStr).intValue();
            }
        
            CampaignEnrollmentList enrollmListPage = null;
                   
            if (enrollmList != null) {
                enrollmListPage = new CampaignEnrollmentList();

                int lastIdx = getLastBPIndex(page, MAX_ENROLLM_LINES , enrollmList.size());
                               
                for (int i = getFirstBPIndex(page, MAX_ENROLLM_LINES ); i <= lastIdx ; i++) { 
                    CampaignEnrollment enrollm = enrollmList.get(i);
                    enrollmListPage.addEnrollment(enrollm);
                }
            
                if (log.isDebugEnabled()) {
                    log.debug(METHOD + ": size of enrollment table = " + enrollmList.size());
                }
            }
            
            // Sets the campaign into the request
            request.setAttribute(ActionConstants.RC_CAMPAIGN, campaign);
            // Sets page number into the request
            request.setAttribute(ActionConstants.RC_CAMPAIGN_PAGE, pageStr);
            // Sets the enrollment list into the request
            request.setAttribute(ActionConstants.RC_CAMPAIGN_ENROLLM, enrollmListPage);
            // Sets the changeable flag into the request
            request.setAttribute("enrollment_changeable", isChangeableStr);
        }
        finally {
            log.exiting();
        }
      
        return mapping.findForward(forwardTo);
    }

    /**
     * Get the first index of the business partner
     * 
     * @param  pageNo as int, page number 
     * @param  maxRows as int, maximum number of rows 
     * @return firstIndex as int, number of first business partner on the current page
     */
    private int getFirstBPIndex(int pageNo, int maxRows) {
        final String METHOD = "getFirstBPIndex()";
        log.entering(METHOD);
        
        int firstIndex = (pageNo - 1) * maxRows + 0;
        
        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": first index of business partner = " + firstIndex
                      + " on page = " + pageNo);
        }
        log.exiting();
        return (firstIndex);
    }
    
    /**
     * Get the last index of the business partner
     * 
     * @param  pageNo as int, page number 
     * @param  maxRows as int, maximum number of rows 
     * @return lastIndex as int, number of last business partner on the current page
     */
    private int getLastBPIndex(int pageNo, int maxRows, int size) {
        final String METHOD = "getLastBPIndex()";
        log.entering(METHOD);
        
        int lastIndex = pageNo * maxRows - 1;
        
        int lastIndexSize = size - 1;
        if (lastIndex > lastIndexSize) {        
            lastIndex = lastIndexSize;
        }
        
        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": list size = " + size
                      + ": last index of business partner = " + lastIndex
                      + " on page = " + pageNo);
        }
        log.exiting();
        return (lastIndex);
    }
    
}
