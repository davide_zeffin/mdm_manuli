/*****************************************************************************
    Class:        CampaignCloseAction
    Copyright (c) 2006, SAP AG, Germany, All rights reserved.
*****************************************************************************/

package com.sap.isa.isacore.action.campaign;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.ecommerce.businessobject.campaign.Campaign;
import com.sap.ecommerce.businessobject.campaign.CampaignBusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.action.EComBaseAction;


/**
 * Handling to close a document in status display mode
 */
public class CampaignCloseAction extends EComBaseAction {

    // request parameter indicating to delete the message list of the campaign
    // parameter is set only in campaignDetail.jsp
    public final static String RC_DELETE_MSGS = "deleteMsgs";

    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param mbom              Reference to the MetaBusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward ecomPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            MetaBusinessObjectManager mbom,
            boolean multipleInvocation,
            boolean browserBack)
            throws CommunicationException {

        final String METHOD_NAME = "ecomPerform()";
        log.entering(METHOD_NAME);
        
        // delete message list of the campaign if necessary
        String deleteMsgs = request.getParameter(RC_DELETE_MSGS);
        if (deleteMsgs != null && deleteMsgs.equals("true")) {
            //get commom and campaign BOM
            CampaignBusinessObjectManager campaignBom = (CampaignBusinessObjectManager)userSessionData.getBOM(CampaignBusinessObjectManager.CAMPAIGN_BOM);
            //Retrieving campaign BO        
            Campaign campaign = (Campaign)campaignBom.getCampaign();
            //Delete message list
            if (campaign != null) {
                campaign.clearMessages();
                if (log.isDebugEnabled()) {
                    log.debug(METHOD_NAME + ": message list cleared");
                }
            }
        }

        DocumentHandler documentHandler = (DocumentHandler)
                userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);

        ManagedDocument mDoc = documentHandler.getManagedDocumentOnTop();
        // release document
        if (mDoc != null) {
            documentHandler.release(mDoc.getDocument());
        }
        
        // show welcome page
        documentHandler.showWelcome();
        
        // Sets the campaign into the request
//        request.setAttribute(ActionConstants.RC_CAMPAIGN, campaign);

        log.exiting();
        return mapping.findForward("success");
    }
        

}