/*****************************************************************************
    Class         GetCampaignAction
    Author:       SAP AG
    Created:      April 2005
    Version:      1.0

    $Revision: #4 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.isacore.action.campaign;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.ecommerce.businessobject.campaign.Campaign;
import com.sap.ecommerce.businessobject.campaign.CampaignBusinessObjectManager;
import com.sap.ecommerce.businessobject.campaign.CampaignHeader;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 * This action reads the campaign with the id provided with {@link #RA_CAMPAIGN_CODE}
 * and store it in the campaign business object manager. <br>
 */
public class GetCampaignAction extends IsaCoreBaseAction {

	/**
	 * key to store the campaign code in request context
	 */
	public final static String RA_CAMPAIGN_CODE        = "campaigncode";


	/**
	 * Initialize the action.
	 * In B2C this action could be used by anonymous user.
	 * 
	 * @see com.sap.isa.isacore.action.IsaCoreBaseAction#initialize()
	 */
	public void initialize() {
		if (isB2C()) {
			this.checkUserIsLoggedIn = false;
		}	
	}


    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            BusinessObjectManager bom,
            IsaLocation log)
            throws CommunicationException {

		final String METHOD_NAME = "GetCampaignAction - isaPerform()";
		log.entering(METHOD_NAME);

		String campaignCode = new String();
		campaignCode = request.getParameter(RA_CAMPAIGN_CODE);
		if (campaignCode == null && request.getAttribute(RA_CAMPAIGN_CODE) != null) {
			campaignCode = request.getAttribute(RA_CAMPAIGN_CODE).toString();
		}		
		campaignCode.trim();
		
		log.debug("CampaignCode: " + campaignCode);
		
		readCampaign(campaignCode, userSessionData, request, log);
		
		log.exiting();
		
        return mapping.findForward("success");
    }


	/**
	 * Read the campaign with the given campaignCode. <br>
	 * 
	 * @param campaignCode
	 * @param theCatalog
	 * @param userSessionData
	 * @param request
	 * @param bom
	 * @throws CommunicationException
	 */
	static public Campaign readCampaign(String campaignCode,
	                                    UserSessionData userSessionData,
	                                    HttpServletRequest request,
	                                    IsaLocation log)
				throws CommunicationException {

		CampaignBusinessObjectManager campaignBom = (CampaignBusinessObjectManager) 
				userSessionData.getBOM(CampaignBusinessObjectManager.CAMPAIGN_BOM);

		campaignBom.releaseCampaign();
		
		Campaign campaign = campaignBom.createCampaign();
		CampaignHeader campaignHeader = (CampaignHeader)campaign.createHeader();

		campaignHeader.setCampaignID(campaignCode);
		campaign.setHeader(campaignHeader);
		
		if (campaignCode != null && !campaignCode.equals("")) {
			log.debug("Read Campaign Header for: " + campaignCode);
			campaign.readCampaignHeader();
		}	
		return campaign;	
	}
}
