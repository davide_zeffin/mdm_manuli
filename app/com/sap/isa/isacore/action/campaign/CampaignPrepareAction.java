/*****************************************************************************
	Class:        CampaignPrepareAction
	Copyright (c) 2006, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      2006/11/29
    Version:      1.0

    $Revision: #1 $
    $Date: 2006/11/29 $
*****************************************************************************/
package com.sap.isa.isacore.action.campaign;

import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.ecommerce.businessobject.campaign.Campaign;
import com.sap.ecommerce.businessobject.campaign.CampaignBusinessObjectManager;
import com.sap.ecommerce.businessobject.campaign.CampaignEnrollment;
import com.sap.ecommerce.businessobject.campaign.CampaignEnrollmentList;
import com.sap.ecommerce.businessobject.campaign.CampaignHeader;
import com.sap.ecommerce.businessobject.campaign.CampaignSearchCriteria;
import com.sap.isa.backend.boi.isacore.order.DocumentListFilterData;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.businesspartner.businessobject.PartnerFunctionBase;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.MessageDisplayer;
import com.sap.isa.isacore.action.ActionConstants;

/**
 * Copyright (c) 2006, SAP AG, Germany, All rights reserved.
 *
 * Reads campaign data. 
 * 
 */
public class CampaignPrepareAction extends CampaignEnrollmentBaseAction {

    private final static IsaLocation log = IsaLocation.getInstance(CampaignPrepareAction.class.getName());

    /**
     * Constant for available forwards, value is &quot;success&quot;.
     */
    protected static final String FORWARD_CAMPAIGN = "displaycampaign";

    /**
     * Implement this method to add functionality to your action.
     * Overriden <em>isaPerform</em> method of <em>IsaCoreBaseAction</em>.
     * The action adds the parameters <code>RC_CAMPAIGN<code> and 
     * <code>RC_RETURN_TYPE_LIST<code> to the request
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
                                    ActionForm form,
                                    HttpServletRequest request,
                                    HttpServletResponse response,
                                    UserSessionData userSessionData,
                                    RequestParser requestParser,
                                    BusinessObjectManager bom,
                                    IsaLocation log)
                         throws CommunicationException {
                            
        final String METHOD = "isaPerform()";
        log.entering(METHOD);

        // Page that should be displayed next.
        String forwardTo = FORWARD_CAMPAIGN;

        try {        
            // get document handler
            DocumentHandler documentHandler = (DocumentHandler)
                            userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
            if (documentHandler == null)
                throw new PanicException("No document handler found");

            //get commom and campaign BOM
            CampaignBusinessObjectManager campaignBom = (CampaignBusinessObjectManager)userSessionData.getBOM(CampaignBusinessObjectManager.CAMPAIGN_BOM);

            Shop shop = bom.getShop();
            if (shop == null) {
                log.exiting();
                throw new PanicException("shop.notFound");
            }
            if (!shop.isEnrollmentAllowed()) {
                log.exiting();
                throw new PanicException("enrollment not allowed");
            }
            
            CampaignSearchCriteria searchCriteria = new CampaignSearchCriteria();

            //Retrieving campaign BO        
            Campaign campaign = (Campaign)campaignBom.getCampaign();
        
            //Create a campaign BO if it doesn't exists
            if (campaign == null) {
                campaign = (Campaign)campaignBom.createCampaign();
                if (log.isDebugEnabled()) {
                    log.debug(METHOD + ": campaign created");
                }
            }
        
            String campaignId = null;
            String searchBpHier = null;
            String enrolleeId = null;
            String page = null;
            boolean performPeriodCheck = false;
            
            if (request.getParameter(ActionConstants.RC_CAMPAIGN_ID) != null) {
                // read request parameter (1st read)
                campaignId = request.getParameter(ActionConstants.RC_CAMPAIGN_ID);
                searchBpHier = request.getParameter(ActionConstants.RC_SEARCHBPHIER);
                enrolleeId = request.getParameter(ActionConstants.RC_ENROLLEE);
                page = request.getParameter(ActionConstants.RC_CAMPAIGN_PAGE);
                performPeriodCheck = true;
            }
            else {
                // read request attributes (subsequent reads)
                campaignId = (String) request.getAttribute(ActionConstants.RC_CAMPAIGN_ID);
                searchBpHier = (String) request.getAttribute(ActionConstants.RC_SEARCHBPHIER);
                enrolleeId = (String) request.getAttribute(ActionConstants.RC_ENROLLEE);
                page = (String) request.getAttribute(ActionConstants.RC_CAMPAIGN_PAGE);
            }
        
            if (log.isDebugEnabled()) {
                log.debug(METHOD + ": request.parameters/attributes - campaignId=" + campaignId
                                 + " / searchBpHier=" + searchBpHier
                                 + " / enrolleeId=" + enrolleeId
                                 + " / page=" + page);
            }
        
            // set search criteria
            // set search criteria - campaign id
            searchCriteria.setCampaignID(campaignId);
            
            // set search criteria - searchBpHierFlag
            boolean searchBpHierFlag = (searchBpHier != null && searchBpHier.equals("true"));
            searchCriteria.setSearchForBpHier(searchBpHierFlag);

            // set search criteria - text type for campaign description + country 
            searchCriteria.setTextTypeCampDescr(shop.getTextTypeForCampDescr());
            searchCriteria.setCountry(shop.getCountry());

            // get reference to bupa-manager
            BusinessPartnerManager buPaManager = bom.getBUPAManager();
            if (buPaManager == null) {
                buPaManager = bom.createBUPAManager();
            }

            // set search criteria - enrollerTechKey and enrollerId 
            BusinessPartner partner = buPaManager.getDefaultBusinessPartner(PartnerFunctionBase.SOLDTO);
            String enrollerGuid = null;
            if (partner != null) {
                enrollerGuid = partner.getTechKey().getIdAsString();
                searchCriteria.setEnrollerGuid(enrollerGuid);
                searchCriteria.setEnrollerId(partner.getId());
            }

            // set search criteria - enrolleeTechKey and enrolleeId
            // if enrolleeId is not entered, take the enroller (= already read partner) instead
            String enrolleeGuid = null;
            if (enrolleeId != null && !enrolleeId.equals("")) {
                searchCriteria.setEnrolleeId(enrolleeId);
                partner = buPaManager.getBusinessPartner(enrolleeId);
                if (partner == null) {
                    if (log.isDebugEnabled()) {
                       log.debug(METHOD + ": enrollee (" + enrolleeId + ") is not valid");
                    }
                    // save the search criteria explicitly here
                    campaign.setSearchCriteria(searchCriteria);
                    // clear messages
                    campaign.clearMessages();
                    // message: Campaign not valid for selected Business Partner; check your selection
                    campaign.addMessage(new Message(Message.ERROR, "ecom.camp.displ.err.bpInvalid"));
                    return mapping.findForward(FORWARD_ERROR);
                }
            }
            
            if (partner != null) {
                enrolleeGuid = partner.getTechKey().getIdAsString();
                searchCriteria.setEnrolleeGuid(enrolleeGuid);
                searchCriteria.setEnrolleeId(partner.getId());
            }
            
            if (campaignId.equals("")) {
                if (log.isDebugEnabled()) {
                    log.debug(METHOD + ": no campaignId entered");
                }
                // save the search criteria explicitly here
                campaign.setSearchCriteria(searchCriteria);
                // clear messages
                campaign.clearMessages();
                // message: Invalid campaign; check your entries
                campaign.addMessage(new Message(Message.ERROR, "ecom.camp.displ.err.campInvalid"));
                return mapping.findForward(FORWARD_ERROR);
            }
            
            // page
            if (page == null || page.equals("")) {
                page = "1";
            }
          
            //Reset business object campaign
            if (campaign.getHeader() != null) {
                String campaignIdOld = campaign.getHeader().getCampaignID();
                String campaignIdNew = campaignId.toUpperCase();
                if (campaignIdOld == null ||
                   !campaignIdOld.equals(campaignIdNew)) {
                   campaign.clearMessages();
                }
            } 
            else { 
                campaign.clearMessages();
            }
            campaign.clear();
            // save search criteria
            campaign.setSearchCriteria(searchCriteria);
        
            // Checks permissions
            User user = bom.getUser();
        
            String[] documents = new String[]{DocumentListFilterData.DOCUMENT_TYPE_CAMPAIGN};
            String[] actions   = new String[]{DocumentListFilterData.ACTION_READ};                                           
            Boolean[] checkResults = checkPermissions(user, documents, actions);                                            
            boolean hasDispEnrollmPermission = checkResults[0].booleanValue();
            if (!hasDispEnrollmPermission) {
                // clear messages
                campaign.clearMessages();
                // No authorization to display campaign enrollment data
                campaign.addMessage(new Message(Message.ERROR, "ecom.camp.msg.noAuth.display"));
                return mapping.findForward(FORWARD_ERROR);
            }

            //In case enrollee and enroller are different check the required on behalf permissions
            if ( searchBpHierFlag ||
                 ( enrolleeGuid != null && !enrolleeGuid.equals(enrollerGuid))) {       
                try{
                    boolean hasEnrollmentOBPermission = false;
                    String[] actvts  = new String[]{DocumentListFilterData.ACTVT_DISPLAY};                                           

                    Boolean[] resultsOnBehalf = checkPermissionsOB(user, documents, actvts);
                    hasEnrollmentOBPermission = resultsOnBehalf[0].booleanValue();

                    if (!hasEnrollmentOBPermission) {
                        // clear messages
                        campaign.clearMessages();
                        // No authorization to display campaign enrollment data
                        campaign.addMessage(new Message(Message.ERROR, "ecom.camp.msg.noAuthOB.display"));
                        return mapping.findForward(FORWARD_ERROR);
                    }
                }
                catch (CommunicationException Ex) {
                    log.fatal("User has no permission to read the campaign:" + DocumentListFilterData.DOCUMENT_TYPE_CAMPAIGN);
                    log.fatal(LogUtil.APPS_COMMON_SECURITY, "esrv.com.log.err.noread", new Object[] { METHOD, DocumentListFilterData.DOCUMENT_TYPE_CAMPAIGN });
                    MessageDisplayer messageDisplayer = new MessageDisplayer();
                    messageDisplayer.addMessage(new Message(Message.ERROR, "ecom.cmp.displ.err.noguid"));
                    messageDisplayer.setOnlyBack();
                    messageDisplayer.addToRequest(request);
                    throw new PanicException("User has no permission to read campaigns");
                }       
            }

            if (log.isDebugEnabled()) {
                log.debug(METHOD + ": search criteria: campaignId=" + searchCriteria.getCampaignID()
                                 + " / enrolleeId=" + searchCriteria.getEnrolleeId()
                                 + " / enrollerId=" + searchCriteria.getEnrollerId()
                                 + " / searchForBpHier=" + searchCriteria.getSearchForBpHier()
                                 + " / textTypeCampDescr=" + searchCriteria.getTextTypeCampDescr()
                                 + " / country=" + searchCriteria.getCountry());
            }
        
            // start reading campaign with enrollment information
            campaign.readCampaignWithEnrollment();
        
            // check read campaign
            if (campaign.getHeader() == null || !campaign.getHeader().isValid()) {
                if (log.isDebugEnabled()) {
                    if (campaign.getHeader() == null) {
                        log.debug(METHOD + ": campaign is invalid because header is null");
                    }
                    else {
                        log.debug(METHOD + ": campaign is invalid because isInvalid = " + campaign.getHeader().isValid());
                    }
                }
                // clear messages
                campaign.clearMessages();
                // message: Invalid campaign; check your entries
                campaign.addMessage(new Message(Message.ERROR, "ecom.camp.displ.err.campInvalid"));
                return mapping.findForward(FORWARD_ERROR);
            }
            if (campaign.getCampaignEnrollmentList() == null || campaign.getCampaignEnrollmentList().size() == 0 ) {
                if (log.isDebugEnabled()) {
                   log.debug(METHOD + ": campaign is invalid because enrollment list is empty");
                }
                // clear messages
                campaign.clearMessages();
                // message: Campaign not valid for selected Business Partner; check your selection
                campaign.addMessage(new Message(Message.ERROR, "ecom.camp.displ.err.bpInvalid"));
                return mapping.findForward(FORWARD_ERROR);
            }
            
            if (log.isDebugEnabled()) {
               log.debug(METHOD + ": campaign read correctly");
            }

            campaign.setState(ManagedDocument.TARGET_DOCUMENT);
        
            // Gets enrollment data
            CampaignEnrollmentList enrollmList = campaign.getCampaignEnrollmentList();
            if (performPeriodCheck) {
                checkEnrollmPeriod(campaign, enrollmList, shop);
            }
                                                      
            // Adds document
            String href_parameter = ((ActionConstants.RC_CAMPAIGN_ID.concat("=")).concat(campaign.getHeader().getCampaignID())).concat("&");
            href_parameter = (((href_parameter.concat(ActionConstants.RC_SEARCHBPHIER)).concat("=")).concat(searchBpHier)).concat("&");
            if (enrolleeId != null) {
                href_parameter = (((href_parameter.concat(ActionConstants.RC_ENROLLEE)).concat("=")).concat(enrolleeId)).concat("&");
            }
            href_parameter = (((href_parameter.concat(ActionConstants.RC_CAMPAIGN_PAGE)).concat("=")).concat(page));
   
            ManagedDocument mDoc = documentHandler.getManagedDocumentOnTop();
            // release document
            if (mDoc != null && mDoc.getDocType().equals(DocumentListFilterData.DOCUMENT_TYPE_CAMPAIGN)) {
                documentHandler.release(mDoc.getDocument());
            }
            mDoc = new ManagedDocument(
                                (DocumentState)campaign,
                                DocumentListFilterData.DOCUMENT_TYPE_CAMPAIGN,
                                campaign.getHeader().getCampaignID(),
                                null,
                                null,
                                null,
                                FORWARD_CAMPAIGN,
                                "/b2b/searchcampaign.do",
                                href_parameter);
                                
            documentHandler.add(mDoc);
            documentHandler.setOnTop(mDoc);
        
            // Sets page number into the request
            request.setAttribute(ActionConstants.RC_CAMPAIGN_PAGE, "1");
        }
        finally {
            log.exiting();
        }
      
        return mapping.findForward(forwardTo);
    }

    /**
     * Set the campaign message and checks the enrollment period  
     * @param campaign 
     * @param enrollmList 
     * @param shop is used to determine the date format
     * @return isChangeable as boolean, indicating if the campaign enrollment is currently possible 
     */
    private void checkEnrollmPeriod(Campaign campaign, 
                                    CampaignEnrollmentList enrollmList, 
                                    Shop shop) {
        final String METHOD_NAME = "checkEnrollmPeriod()";
        log.entering(METHOD_NAME);
        
        String dateFormat = getShopDateFormat(shop);
        
        CampaignHeader campHeader = campaign.getHeader();
        
        if (campHeader != null) {
            if (campHeader.isEnrollmPeriodInPast(dateFormat)) {
                // enrollment period in the past
                if (isDeletedInternally(enrollmList)) {
                    // message: Enrollment deleted by vendor; enrollment period is over
                    campaign.addMessage(new Message(Message.INFO, "ecom.camp.msg.enrollDelPeriodOver"));
                }
                else {
                    // message: Enrollment period is over; no more changes possible
                    campaign.addMessage(new Message(Message.INFO, "ecom.camp.msg.enrollPeriodOver"));
                }
            } 
            else if (campHeader.isEnrollmPeriodInFuture(dateFormat)) {
                // enrollment period in the future
                // message: Enrollment period has not started yet
                campaign.addMessage(new Message(Message.INFO, "ecom.camp.msg.enrollPeriodFuture"));
            }
            else {
                // enrollment period currently open
                if (isDeletedInternally(enrollmList)) {
                    // message: Enrollment deleted by vendor; you can enroll again
                    campaign.addMessage(new Message(Message.INFO, "ecom.camp.msg.enrollDelPeriodOpen"));
                }
            }
        }
    }

    /**
     * checks if at least one enrollment was deleted internally  
     * @param enrollmList 
     * @return isDeletedInternally as boolean
     */
    private boolean isDeletedInternally(CampaignEnrollmentList enrollmList) {
        final String METHOD_NAME = "isDeletedInternally()";
        log.entering(METHOD_NAME);
        boolean isDeletedInternally = false;
        CampaignEnrollment enrollm = null;        
        Iterator iter = enrollmList.iterator();
        while (iter.hasNext() && !isDeletedInternally) {
            enrollm = (CampaignEnrollment) iter.next();
            if (enrollm != null) {
                // isEnrolled has already the new value !
                isDeletedInternally = enrollm.isDeletedInternally();
            } 
        }
        log.exiting();
        return (isDeletedInternally);
    }
    
}
