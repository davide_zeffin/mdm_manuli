/*****************************************************************************
	Class:        CampaignEnrollmentBaseAction
	Copyright (c) 2007, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      2007/03/27
    Version:      1.0

    $Revision: #1 $
    $Date: 2007/03/27 $
*****************************************************************************/
package com.sap.isa.isacore.action.campaign;

import com.sap.ecommerce.businessobject.campaign.Campaign;
import com.sap.ecommerce.businessobject.campaign.CampaignHeader;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.isacore.IsaOnBehalfPermission;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.user.permission.ECoActionPermission;

/**
 * Copyright (c) 2006, SAP AG, Germany, All rights reserved.
 *
 * Reads campaign data. 
 * 
 */
public class CampaignEnrollmentBaseAction extends IsaCoreBaseAction {

    static final private IsaLocation log = IsaLocation.getInstance(CampaignEnrollmentBaseAction.class.getName());

    /**
     * Constant for available forwards, value is &quot;error&quot;.
     */
    protected final static String FORWARD_ERROR = "error";

    /**
     * Check for the user Permission for the Creation of different Documents. <br>
     * @param User for whom the permissions are required 
     * @param String document for which permission is asked.
     * @return Boolean True/False.
     */
    protected Boolean[] checkPermissions(User user, String[] docs, String[] actions) throws CommunicationException  {
        ECoActionPermission[] docPermissions = new ECoActionPermission[docs.length];
        for( int cnt = 0; cnt<docs.length; cnt++){
            docPermissions[cnt] = new ECoActionPermission(docs[cnt], actions[cnt]);     
        }
        return user.hasPermissions(docPermissions);     
    }   
    
    /**
     * Check for the user Permission for the Creation of different Documents. <br>
     * @param User for whom the permissions are required 
     * @param String document for which permission is asked.
     * @param String activities which are checked
     * @return Boolean True/False.
     */
    protected Boolean[] checkPermissionsOB(User user, String[] docs, String[] actvts) throws CommunicationException  {
        IsaOnBehalfPermission[] docPermissions = new IsaOnBehalfPermission[docs.length];
        for (int cnt = 0; cnt<docs.length; cnt++){
            docPermissions[cnt] = new IsaOnBehalfPermission(docs[cnt], actvts[cnt]);         
        }
        return user.hasPermissions(docPermissions);     
    }

    /**
     * Set the campaign message and checks the enrollment period  
     * @param campaign 
     * @param hasChangeEnrollmPermission, true if the user has the right to change enrollments
     * @param shop is used to determine the date format
     * @return isChangeable as boolean, indicating if the campaign enrollment is currently possible 
     */
    protected boolean checkEnrollmPeriod(Campaign campaign, 
                                       boolean hasChangeEnrollmPermission,
                                       Shop shop) {
        final String METHOD_NAME = "checkEnrollmPeriod()";
        log.entering(METHOD_NAME);
        
        boolean isChangeable = false;
        String dateFormat = getShopDateFormat(shop);
        
        CampaignHeader campHeader = campaign.getHeader();
        
        if (campHeader != null &&
            campHeader.isEnrollmPeriodOpen()) {
            // the enrollment period is currently open
            isChangeable = hasChangeEnrollmPermission;
        }
        return (isChangeable);
    }

    /**
     * Return the date format. <br>
     * The Format is taken from the salesDocConfiguration.
     * This is the format, that must be used for the new calendar control.
     * 
     * @param shop is used to determine the date format
     * @return the date format taken from the salesDocConfiguration
     */
    protected String getShopDateFormat(Shop shop) {
        final String METHOD_NAME = "getShopDateFormat()";
        log.entering(METHOD_NAME);
        String dateFormat = null;

        // read the date format from shop
        dateFormat = shop.getDateFormat();
        dateFormat = dateFormat.replace('Y', 'y');
        dateFormat = dateFormat.replace('D', 'd');
 
        if (log.isDebugEnabled()) {
            log.debug(METHOD_NAME + ": date format = " + dateFormat); 
        }
        log.exiting();
        return dateFormat;
    }

}
