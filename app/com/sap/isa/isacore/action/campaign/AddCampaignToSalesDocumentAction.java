/*****************************************************************************
    Class         AddCampaignToCatalogueAction
    Author:       SAP AG
    Created:      April 2005
    Version:      1.0

    $Revision: #4 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.isacore.action.campaign;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.ecommerce.boi.campaign.CampaignHeaderData;
import com.sap.ecommerce.businessobject.campaign.Campaign;
import com.sap.ecommerce.businessobject.campaign.CampaignBusinessObjectManager;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.order.CampaignListData;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.order.Basket;
import com.sap.isa.businessobject.ordertemplate.OrderTemplate;
import com.sap.isa.businessobject.quotation.Quotation;
import com.sap.isa.businessobject.webcatalog.CatalogBusinessObjectManager;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.catalog.actions.ActionConstants;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 * This class adds the campaign hold in campaign business object manager to the 
 * current sales document. <br>
 *
 */
public class AddCampaignToSalesDocumentAction extends IsaCoreBaseAction {


	/**
	 * Initialize the action.
	 * In B2C this action could be used by anonymous user.
	 * 
	 * @see com.sap.isa.isacore.action.IsaCoreBaseAction#initialize()
	 */
	public void initialize() {
		if (isB2C()) {
			this.checkUserIsLoggedIn = false;
		}	
	}


    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            BusinessObjectManager bom,
            IsaLocation log)
            throws CommunicationException {

		final String METHOD_NAME = "AddCampaignToSalesDocumentAction - isaPerform()";
		log.entering(METHOD_NAME);

		Shop shop = bom.getShop();
		BusinessPartnerManager bupaManager = bom.createBUPAManager();

		CampaignBusinessObjectManager campaignBom = (CampaignBusinessObjectManager) userSessionData.getBOM(CampaignBusinessObjectManager.CAMPAIGN_BOM);
		Campaign campaign = campaignBom.getCampaign();
		
		if (campaign==null) {
			log.debug("campaign is null");
		}
		else {
			log.debug("campaignn is valid: " + campaign.isValid());
		}

		if (campaign!=null && campaign.isValid()) {

			CampaignHeaderData campaignHeader = campaign.getHeaderData();
				
			// Catalog-Sales Document Interaction
			if (shop.getApplication().equals(ShopData.B2C)){ //B2C
				log.debug("B2C scenario");
				Basket basket = bom.getBasket();
				if (basket != null ){ // B2C basket exists
					log.debug("Add campaign to basket");
                    if (basket.getHeaderData().getAssignedCampaignsData() != null && 
                        !basket.getHeaderData().getAssignedCampaignsData().isEmpty() &&
                        !shop.isMultipleCampaignsAllowed()) {
                        basket.getHeaderData().getAssignedCampaignsData().clear();
                        log.debug("AssignedCamapigns cleared");
                    }
                    else {
                        log.debug("AssignedCampaigns not cleared, because it is empty or because multiple cmapaigns are allowed");
                    }
					
					basket.getHeaderData().getAssignedCampaignsData().addCampaign(campaignHeader.getCampaignID(), campaignHeader.getTechKey(), campaignHeader.getType(), campaignHeader.isValid(), campaignHeader.isEnteredManually());
					basket.update(bupaManager);
				}
			}
			else if (shop.getApplication().equals(ShopData.B2B)){ //B2B
				log.debug("B2B scenario");
				DocumentHandler documentHandler = (DocumentHandler)userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
				if (documentHandler != null) {
					DocumentState targetDocument = documentHandler.getDocument(ManagedDocument.TARGET_DOCUMENT);
				
					SalesDocument preOrderSalesDocument = null;
					if (targetDocument instanceof Quotation) {
						log.debug("Add campaign to quotation");
						preOrderSalesDocument = bom.getQuotation();
					}
					else if (targetDocument instanceof OrderTemplate) {
						log.debug("Add campaign to template");
						preOrderSalesDocument = bom.getOrderTemplate();
					}
					else {
						log.debug("Add campaign to basket");
						preOrderSalesDocument = bom.getBasket();
					}
					if (null != preOrderSalesDocument) {	
						CampaignListData assignedCampaigns = preOrderSalesDocument.getHeaderData().getAssignedCampaignsData();
						if (assignedCampaigns != null){
							log.debug("Add campaign to assignedCampaigns list");
							if (!shop.isMultipleCampaignsAllowed() && !assignedCampaigns.isEmpty()){ // Multiple campaigns are not allowed and list contains an entry, overwrite
								assignedCampaigns.clear();
								log.debug("Clear assignedCampaigns list at first");
							}
                            else {
                                log.debug("AssignedCampaigns not cleared, because it is empty or because multiple cmapaigns are allowed");
                            }
							assignedCampaigns.addCampaign(campaignHeader.getCampaignID(), campaignHeader.getTechKey(), campaignHeader.getType(), campaignHeader.isValid(), campaignHeader.isEnteredManually());
						}
						else {
							log.debug("AssignedCamapigns list not existing - campaign not added");
						}
						preOrderSalesDocument.update(bupaManager);
					}
				}
			}
		}
		
		log.exiting();
		
		CatalogBusinessObjectManager cbom = getCatalogBusinessObjectManager(userSessionData);
		WebCatInfo theCatalog = cbom.getCatalog();
		String forward = ActionConstants.FW_SUCCESS;
		
		if(WebCatInfo.ITEMDETAILS.equals(theCatalog.getLastVisited())){
		   forward = WebCatInfo.ITEMDETAILS;	
		}

        return mapping.findForward(forward);
    }


}
