/*****************************************************************************
    Class         AddCampaignToCatalogueAction
    Author:       SAP AG
    Created:      April 2005
    Version:      1.0

    $Revision: #4 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.isacore.action.campaign;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.ecommerce.boi.campaign.CampaignHeaderData;
import com.sap.ecommerce.businessobject.campaign.Campaign;
import com.sap.ecommerce.businessobject.campaign.CampaignBusinessObjectManager;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.webcatalog.CatalogBusinessObjectManager;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.catalog.actions.ActionConstants;
import com.sap.isa.catalog.actions.CatalogBaseAction;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.catalog.webcatalog.WebCatItemList;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.MessageList;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 * This class adds the campaign hold in campaign business object manager to the 
 * catalog. <br>
 *
 */
public class AddCampaignToCatalogueAction extends IsaCoreBaseAction {


	/**
	 * Initialize the action.
	 * In B2C this action could be used by anonymous user.
	 * 
	 * @see com.sap.isa.isacore.action.IsaCoreBaseAction#initialize()
	 */
	public void initialize() {
		if (isB2C()) {
			this.checkUserIsLoggedIn = false;
		}	
	}


    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            BusinessObjectManager bom,
            IsaLocation log)
            throws CommunicationException {

		final String METHOD_NAME = "AddCampaignToCatalogueAction - isaPerform()";
		log.entering(METHOD_NAME);

        String forward = "success";

        CatalogBusinessObjectManager cbom =
                getCatalogBusinessObjectManager(userSessionData);
	
		WebCatInfo theCatalog = cbom.getCatalog();
	
		if (theCatalog == null) {
			log.exiting();
			throw new PanicException("catalog.notFound");
		}

        String rforward = addCampaignToCatalogue(theCatalog, userSessionData, bom, false, log);
        
        if (request.getAttribute(ActionConstants.RA_LOGIN_CAMPAIGN_CHECK_FWD) != null) {
            forward = (String) request.getAttribute(ActionConstants.RA_LOGIN_CAMPAIGN_CHECK_FWD);
            if (log.isDebugEnabled()) {
                log.debug("Forward received from the login Action: " + forward);
            }
        }

        if (rforward.equals(ActionConstants.FW_REPRICE)) {
            // first do a repricing than continue with normal flow
            if (forward.equals("success")) {
                request.setAttribute(ActionConstants.RA_FORWARD, ActionConstants.FW_ADDCAMP);
            }
            else {
                request.setAttribute(ActionConstants.RA_FORWARD, forward);
            }
            forward = rforward;
            if (theCatalog.getCurrentItem() != null) {
                request.setAttribute(ActionConstants.RC_WEBCATITEM, theCatalog.getCurrentItem());
            } 
            else if (theCatalog.getCurrentItemList() != null) { 
                request.setAttribute(ActionConstants.RA_ITEMLIST_EBP, theCatalog.getCurrentItemList());
            }
        }
        
		log.exiting();

        return mapping.findForward(forward);
    }
    
	/**
	 * Add the given campaign guid to the catalogue. <br>
	 * 
	 * @param campaignCode
	 * @param theCatalog
	 * @param userSessionData
	 * @param bom
	 * @throws CommunicationException
	 */
	static public String addCampaignToCatalogue(WebCatInfo theCatalog,
											  UserSessionData userSessionData,
											  BusinessObjectManager bom,
											  IsaLocation log)
				throws CommunicationException {
					
	    return addCampaignToCatalogue(theCatalog, userSessionData, bom, true, log);
    }



	/**
	 * Add the given campaign guid to the catalogue. <br>
	 * 
	 * @param campaignCode
	 * @param theCatalog
	 * @param userSessionData
	 * @param doPricing do also the pricing if necessary
	 * @param bom
	 * @throws CommunicationException
	 */
    static public String addCampaignToCatalogue(WebCatInfo theCatalog,
											    UserSessionData userSessionData,
											    BusinessObjectManager bom,
											    boolean doPricing,
	                                            IsaLocation log)
        		throws CommunicationException {

        String forward = ActionConstants.FW_SUCCESS;
		CampaignBusinessObjectManager campaignBom = (CampaignBusinessObjectManager) userSessionData.getBOM(CampaignBusinessObjectManager.CAMPAIGN_BOM);
		Campaign campaign = campaignBom.getCampaign();
		
		if (campaign == null) {
			log.debug("Campaign object null - return");
			return (forward);
		}

		CampaignHeaderData campaignHeader = campaign.getHeaderData();
		
		if (campaignHeader == null) {
			log.debug("campaignHeader object null - return");
			return (forward);
		}
		
		String campaignCode = campaignHeader.getCampaignID(); 
        
    	boolean campainWasValid = theCatalog.getCampaignGuid() != null && theCatalog.isSoldToEligible();
    	
    	log.debug("Old Campaign validity :" + campainWasValid + "  Guid != null: " + 
    	           (theCatalog.getCampaignGuid() != null) + " SoldTo Eligible:" + theCatalog.isSoldToEligible());
    
    	// setting useful defaults
    	theCatalog.setCampaignId(campaignCode);
    	theCatalog.setSoldToEligible(false);
    	theCatalog.setCampaignGuid(null);	
    
    	// repricing is true, if the campaign was valid.
    	// if the campaign wasn't invalid, it depends if the new camapign is valid
    	// this check will done below. 		
    	boolean repricing = campainWasValid;
    	
    	log.debug("CampaignId: " + campaignCode);
		log.debug("campaignHeader.isValid(): " + campaignHeader.isValid());
    
    	if (campaignCode != null && !campaignCode.equals("") && campaignHeader.isValid()) {

			BusinessPartnerManager bupaManager = bom.createBUPAManager();
			BusinessPartner soldTo = bupaManager.getDefaultBusinessPartner(PartnerFunctionData.SOLDTO);
    		
    		TechKey soldToGuid = soldTo!=null?soldTo.getTechKey():null;
    
			log.debug("campaignHeader.isPublic(): " + campaignHeader.isPublic());
			
			if (!campaignHeader.isPublic()){
				log.debug("Private Campaign");
                MessageList messages = campaignHeader.getMessageList();
                messages.remove("catalog.isa.camp.priv.logon");
				if (null != soldToGuid && !soldToGuid.isInitial()){ //Sold-to available
					Shop shop = bom.getShop();
					if ( soldToGuid != null){
						log.debug("Campaign check eligibilty");
						campaign.checkPrivateCampaignEligibility(soldToGuid,
							shop.getSalesOrganisation(), shop.getDistributionChannel(), shop.getDivision(), new Date());
						if (campaignHeader.isValid()) { // Sold-to is eligible
							log.debug("Campaign is eligible set values");
							theCatalog.setCampaignGuid(campaignHeader.getTechKey());
							theCatalog.setSoldToEligible(true);
						}
					}
				}
				else {
					log.debug("Campaign is private but no soldTo available");
                    Message msg = new Message(Message.WARNING, "catalog.isa.camp.priv.logon", new String[] {campaignHeader.getCampaignID().toUpperCase()}, "");
                    messages.add(msg); 
				}
			}
		
			if (campaignHeader.isPublic()) { 
				log.debug("Public Campaign set values");
				theCatalog.setCampaignGuid(campaignHeader.getTechKey());
				//Public Campaign, Sold-to not mandatory
				theCatalog.setSoldToEligible(true);
			}
    	}
    
    
    	// repricing is true if the campaign is valid. 		
    	if (theCatalog.getCampaignGuid() != null && theCatalog.isSoldToEligible()) {
    		repricing = true;
    		log.debug("Set repricing true");
    	}
    
    	if (repricing) {
    		
    		ArrayList repriceItems = null;
    		
			// current item list
			WebCatItemList itemList = theCatalog.getCurrentItemList();
    
			//	Items need to be re-priced 
			if (itemList != null && itemList.getItems() != null) { 
				repriceItems = new ArrayList(itemList.size() + 1);
				Iterator iter = itemList.getItems().iterator();
				while (iter.hasNext()){
					WebCatItem item = (WebCatItem)iter.next();
					item.setItemPrice(null);
					repriceItems.add(item);
				}
			}
    		
            // current item 
            WebCatItem currItem = (WebCatItem) theCatalog.getCurrentItem();
            if (currItem != null) {
            	if (repriceItems == null) {
					repriceItems = new ArrayList(1);
            	}
                currItem.setItemPrice(null);
				repriceItems.add(currItem);
            }
    		
    		if (repriceItems != null) {
    			if (doPricing) {
    				log.debug("Do direct Pricing");
					CatalogBaseAction.repriceItems(userSessionData.getMBOM(), repriceItems);
    			}
    			else {
					log.debug("Call GetPrices Action");
					forward = ActionConstants.FW_REPRICE;
    			}
    		}

    	}
    	
        return (forward);
    }
    

}
