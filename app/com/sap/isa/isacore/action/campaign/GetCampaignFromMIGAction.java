/*****************************************************************************
    Class         GetCampaignFromMIGAction
    Author:       SAP AG
    Created:      May 2005
    Version:      1.0

    $Revision: #4 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.isacore.action.campaign;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.ecommerce.businessobject.campaign.Campaign;
import com.sap.ecommerce.businessobject.campaign.CampaignBusinessObjectManager;
import com.sap.ecommerce.businessobject.campaign.CampaignHeader;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.Constants;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.action.IsaCoreInitAction;

/**
 * This action reads the campaign associate with the MIG
 * and store it in the campaign business object manager. <br>
 */
public class GetCampaignFromMIGAction extends IsaCoreBaseAction {


	/**
	 * Initialize the action.
	 * In B2C this action could be used by anonymous user.
	 * 
	 * @see com.sap.isa.isacore.action.IsaCoreBaseAction#initialize()
	 */
	public void initialize() {
		if (isB2C()) {
			this.checkUserIsLoggedIn = false;
		}	
	}


    /**
     * Implement this method to add functionality to your action.
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            BusinessObjectManager bom,
            IsaLocation log)
            throws CommunicationException {

		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);

		IsaCoreInitAction.StartupParameter startupParameter =
				(IsaCoreInitAction.StartupParameter)userSessionData.getAttribute(IsaCoreInitAction.SC_STARTUP_PARAMETER);

		// Setting of the campaignKey if existing
		if ((startupParameter != null) && (startupParameter.getCampaignKey().length() > 0)) {
			String campaignKey = startupParameter.getCampaignKey();

			CampaignBusinessObjectManager campaignBom = (CampaignBusinessObjectManager) userSessionData.getBOM(CampaignBusinessObjectManager.CAMPAIGN_BOM);
                    
			Campaign campaign = campaignBom.createCampaign();
			CampaignHeader campaignHeader = (CampaignHeader)campaign.createHeader();

			campaignHeader.setTechKey(new TechKey(campaignKey));
			campaign.setHeader(campaignHeader);
		
			campaign.readCampaignHeader();
		}
        
        String webServiceAction = request.getParameter(Constants.PARAM_WEB_SERVICE);
        if (webServiceAction != null && !"".equals(webServiceAction.trim())) {
            return mapping.findForward("catWSlogin");
        }

        return mapping.findForward("success");
    }


}
