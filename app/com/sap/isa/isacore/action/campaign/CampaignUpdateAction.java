/*****************************************************************************
    Class:        CampaignUpdateAction
    Copyright (c) 2006, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      2006/12/06
    Version:      1.0

    $Revision: #1 $
    $Date: 2006/12/06 $
*****************************************************************************/
package com.sap.isa.isacore.action.campaign;

import java.io.IOException;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.ecommerce.businessobject.campaign.Campaign;
import com.sap.ecommerce.businessobject.campaign.CampaignBusinessObjectManager;
import com.sap.ecommerce.businessobject.campaign.CampaignEnrollment;
import com.sap.ecommerce.businessobject.campaign.CampaignEnrollmentList;
import com.sap.ecommerce.businessobject.campaign.CampaignHeader;
import com.sap.ecommerce.businessobject.campaign.CampaignSearchCriteria;
import com.sap.isa.backend.boi.isacore.order.DocumentListFilterData;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.businesspartner.businessobject.PartnerFunctionBase;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.ActionConstants;

/**
 * Title:        CampaignUpdateAction<br /> 
 * Description:  Action to update a campaign with enrollment  data from current CampaignEnrollmentPage
 *               in current enrollment list of the campaign<br />
 * 
 * @version 2.0
 */
public class CampaignUpdateAction extends CampaignEnrollmentBaseAction {

    static final private IsaLocation log = IsaLocation.getInstance(CampaignUpdateAction.class.getName());

    /**
     * Constant for available forwards, value is &quot;displaycampaign&quot;.
     */
    protected final static String FORWARD_DISPLAY = "displaycampaign";

    /**
     * Constant for available forwards, value is &quot;readcampaign&quot;.
     */
    protected final static String FORWARD_REREAD = "rereadcampaign";

    /**
     * Constant for available forwards, value is &quot;updateEnrollment&quot;.
     */
    protected final static String FORWARD_ENROLL = "updateEnrollment";

    /**
     * Constant for available forwards, value is &quot;selectAllBPs&quot;.
     */
    protected final static String FORWARD_SELALL = "selectAllBPs";

    /**
     * Constant for available forwards, value is &quot;deselectAllBPs&quot;.
     */
    protected final static String FORWARD_DESELALL = "deselectAllBPs";

    /**
     * Process the specified HTTP request, and create the corresponding HTTP
     * response (or forward to another web component that will create it).
     * Return an <code>ActionForward</code> instance describing where and how
     * control should be forwarded, or <code>null</code> if the response has
     * already been completed.
     * 
     * @param mapping mapping The ActionMapping used to select this instance
     * @param form The <code>FormBean</code> specified in the Struts configuration file for this action
     * @param request The request object
     * @param response The response object
     * @param userSessionData Object wrapping the session
     * @param requestParser Parser to simple retrieve data from the request
     * @param bom BusinessObjectManager
     * @param log IsaLocation
     *
     * @return Forward to another action or JSP page
     * @throws IOException
     * @throws ServletException
     * @throws CommunicationException
     */

    public ActionForward isaPerform(ActionMapping mapping,
                                   ActionForm form,
                                   HttpServletRequest request,
                                   HttpServletResponse response,
                                   UserSessionData userSessionData,
                                   RequestParser requestParser,
                                   BusinessObjectManager bom, 
                                   IsaLocation log)
                         throws IOException, ServletException, CommunicationException {

        final String METHOD_NAME = "isaPerform()";
        log.entering(METHOD_NAME);

        Shop shop = bom.getShop();
        User user = bom.getUser();
        HttpSession session = request.getSession();

        String forward = FORWARD_DISPLAY;
        
        RequestParser.Parameter rp_forward = requestParser.getParameter("forward");
        RequestParser.Parameter rp_enrollall = requestParser.getParameter("enrollall");
        RequestParser.Parameter rp_campaignpage = requestParser.getParameter("campaignpage");
        RequestParser.Parameter rp_enrolleeGUID = requestParser.getParameter("enrolleeGUID[]");
        RequestParser.Parameter rp_isEnrolled = requestParser.getParameter("isEnrolled[]");
 
        HttpSession theSession = request.getSession();

        //get commom and campaign BOM
        CampaignBusinessObjectManager campaignBom = (CampaignBusinessObjectManager)userSessionData.getBOM(CampaignBusinessObjectManager.CAMPAIGN_BOM);
        //Retrieving campaign BO        
        Campaign campaign = (Campaign)campaignBom.getCampaign();
        if (campaign == null) {
            log.exiting();
            throw new IOException("campaign not found");
        }
        CampaignHeader campHeader = campaign.getHeader();
        if (campHeader == null) {
            log.exiting();
            throw new IOException("campaign header not defined");
        }
        
        if (log.isDebugEnabled()) {
           log.debug(METHOD_NAME + ": campaign Id =" + campaign.getHeader().getCampaignID());
        }
        
        // clear messages
        campaign.clearMessages();
        
        // Checks permissions
        String[] documents = new String[]{DocumentListFilterData.DOCUMENT_TYPE_CAMPAIGN,
                                          DocumentListFilterData.DOCUMENT_TYPE_CAMPAIGN};
        String[] actions   = new String[]{DocumentListFilterData.ACTION_READ,
                                          DocumentListFilterData.ACTION_CHANGE};                                           

        Boolean[] checkResults = checkPermissions(user, documents, actions);                                            
        boolean hasDispEnrollmPermission = checkResults[0].booleanValue();
        boolean hasChangeEnrollmPermission = checkResults[1].booleanValue();
        if (!hasDispEnrollmPermission) {
            // No authorization to display campaign enrollment data
            campaign.addMessage(new Message(Message.ERROR, "ecom.camp.msg.noAuth.display"));
            return mapping.findForward(FORWARD_ERROR);
        }

        // set reference to bupa-manager
        BusinessPartnerManager buPaManager = bom.getBUPAManager();
        if (buPaManager == null) {
            buPaManager = bom.createBUPAManager();
        }
        // set enrollerTechKey in header
        BusinessPartner partner = buPaManager.getDefaultBusinessPartner(PartnerFunctionBase.SOLDTO);
        String enrollerGuid = null;
        if (partner != null) {
            enrollerGuid = partner.getTechKey().getIdAsString();
            campHeader.setEnrollerGuid(enrollerGuid);
        }

        CampaignEnrollmentList enrollmList = campaign.getCampaignEnrollmentList();

        // read information from request parameters
        String campaignpage = rp_campaignpage.getValue().getString();
        forward = rp_forward.getValue().getString();
        
        if (log.isDebugEnabled()) {
            log.debug(METHOD_NAME + " / campaignpage = " + campaignpage);
        }

        boolean isChangable = checkEnrollmPeriod(campaign, hasChangeEnrollmPermission, shop);
        if (isChangable) {
            // change all enrollment data in business object
            // !!! this functionality is currently not active !!!
            if (forward.equals(FORWARD_SELALL) ||
                forward.equals(FORWARD_DESELALL)) {
                enrollmList.changeAllEnrollments(forward.equals(FORWARD_SELALL));
                forward = FORWARD_DISPLAY;
            }
            else {
                // take over the individual changes of the page
                // this may override the changes done before for all enrollments      
                if (enrollmList.size() > 1) {
                    // more than one business partner in the list    
                    // update enrollment data 
                    for (int i = 0; i < rp_enrolleeGUID.getNumValues(); i++) {
                        
                        if (rp_enrolleeGUID.getValue(i).getString().length() > 0) {
                            
                            CampaignEnrollment enrollm = null;
                            String enrolleeGuid = rp_enrolleeGUID.getValue(i).getString();
                            boolean newIsEnrolled = rp_isEnrolled.getValue(i).getBoolean();
            
                            // read old enrollment data from campaign
                            if (enrolleeGuid != null) {
                                if (log.isDebugEnabled()) {
                                    log.debug(METHOD_NAME + ": enrollment read from list with GUID = " + enrolleeGuid);
                                }
                                enrollm = enrollmList.get(enrolleeGuid);
                            }
            
                            // change enrollment flag
                            if (enrollm != null) {
                                boolean isNotChanged = (enrollm.isEnrolled() && newIsEnrolled) || (!enrollm.isEnrolled() && !newIsEnrolled);
                                if (!isNotChanged) {
                                    // enrollment was changed
                                    enrollm.setIsEnrolled(newIsEnrolled);
                                    enrollm.setIsChanged();
                                    if (log.isDebugEnabled()) {
                                        log.debug(METHOD_NAME + ": enrollment flag changed to = " + newIsEnrolled);
                                    }
                                }
                            }
                        }
                    } // end for
                }
                else {
                    // only one business partner in the list    
                    if (forward.equals(FORWARD_ENROLL)) {
                        // enrollment was changed to the opposite
                        CampaignEnrollment enrollm = enrollmList.get(0);
        
                        if (enrollm != null) {
                            boolean newIsEnrolled = enrollm.isEnrolled();
                            enrollm.setIsEnrolled(!newIsEnrolled);
                            enrollm.setIsChanged();
                            if (log.isDebugEnabled()) {
                                log.debug(METHOD_NAME + ": enrollment flag changed to = " + newIsEnrolled);
                            }
                        }
                    }
                }
            }
        }

        // process the forwards
        //check if there are error messages for the main item
        if (forward.equals(FORWARD_ENROLL)) {
            // save enrollment changes
            boolean errFlag = campaign.enrollBpForCampaign();
            // set the message correctly
            setMessage(campaign, enrollmList);

            CampaignSearchCriteria searchCriteria = campaign.getSearchCriteria();
            if (searchCriteria != null) {
                request.setAttribute(ActionConstants.RC_CAMPAIGN_ID, searchCriteria.getCampaignID());
                String searchBpHier = (searchCriteria.getSearchForBpHier()) ? "true" : "false";
                request.setAttribute(ActionConstants.RC_SEARCHBPHIER, searchBpHier);
                request.setAttribute(ActionConstants.RC_ENROLLEE, searchCriteria.getEnrolleeId());
            }
            forward = FORWARD_REREAD;
        } 

        // Sets the campaign into the request
//        request.setAttribute(ActionConstants.RC_CAMPAIGN, campaign);
        // Sets page number into the request
        request.setAttribute(ActionConstants.RC_CAMPAIGN_PAGE, campaignpage);

        if (log.isDebugEnabled()) {
            log.debug("Decided to forward to " + forward + "...");
        }
        return mapping.findForward(forward);
    }

    /**
     * Set the campaign message  
     * @param campaign 
     * @param enrollmList
     */
    public void setMessage(Campaign campaign, CampaignEnrollmentList enrollmList) {
        final String METHOD_NAME = "setMessage()";
        log.entering(METHOD_NAME);
        
        if (enrollmList != null &&
            enrollmList.size() > 1) {
            // more than one business partner in the list    
            // message: Selected business partner(s) sucessfully enrolled
            campaign.addMessage(new Message(Message.SUCCESS, "ecom.camp.msg.selBpEnrollSucc"));
        }
        else {
            // only one business partner in the list    
            CampaignEnrollment enrollm = null;        
            Iterator iter = enrollmList.iterator();
            if (iter.hasNext()) {
                enrollm = (CampaignEnrollment) iter.next();
            }
            if (enrollm != null) {
                // isEnrolled has already the new value !
                if (enrollm.isEnrolled()) {
                    // message: Business partner successfully enrolled
                    campaign.addMessage(new Message(Message.SUCCESS, "ecom.camp.msg.singleBpEnrollSucc"));
                }
                else {
                    // message: Enrollment of business partner successfully deleted
                    campaign.addMessage(new Message(Message.SUCCESS, "ecom.camp.msg.singleEnrollDeleted"));
                }
            } 
        }
        log.exiting();
    }

}