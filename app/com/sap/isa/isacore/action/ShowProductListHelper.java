/*****************************************************************************
    Class         ShowProductListHelper
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Helper class for displaying a productlist in the catalog environment
    Author:       Wolfgang Sattler
    Created:      August 2001
    Version:      1.0

    $Revision: #5 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.isacore.action;

import javax.servlet.http.HttpServletRequest;

import com.sap.isa.businessobject.ProductList;
import com.sap.isa.catalog.actions.ActionConstants;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItemList;
import com.sap.isa.isacore.action.marketing.MarketingActionConstants;
import com.sap.isa.core.util.JspUtil;

/**
 * This is helper class which help to prepare the displaying from a productlist in 
 * the catalog environment. Therefore all relevant attributes for the catalog will be set:
 *
 * <h4>Overview of attributes set in the request context</h4>
 * <ul>
 * <li><code>ActionConstants.RA_DISPLAYSCENARIO</code></li>
 * <li>type</li>
 * <li>isProductList</li>
 * <li>currentArea</li>
 * <li>toNext</li>
 * <li>isQuery</li>
 * <li>itemList</li>
 * </ul>
 *
 * @see com.sap.isa.isacore.action.ActionConstants
 *
 */
public class ShowProductListHelper {
	
	/**
	 * The value is used in the method {@link #getValueFromRequest(HttpServletRequest, String, int)}
	 * to not encode the value from the request.
	 */
	public static final int DO_NOT_ENCODE = -1;
	
	/**
	 * The value is used in the method {@link #getValueFromRequest(HttpServletRequest, String, int)}
	 * to encode the value to HTML from the request.
	 */
	public static final int ENCODE_TO_HTML = 1;
	
	/**
	 * The value is used in the method {@link #getValueFromRequest(HttpServletRequest, String, int)}
	 * to encode the value to JavaScript from the request.
	 */
	public static final int ENCODE_TO_JS = 2;
	
	/**
	 * The value is used in the method {@link #getValueFromRequest(HttpServletRequest, String, int)}
	 * to encode the value to an URL from the request.
	 */
	public static final int ENCODE_TO_URL = 3;
	

    /**
     * set all required attribute in the request context and in the catalog object
     *
     *
     * @param catalog to set all required properties in the catalog
     * @param request to set all required attribute in the context
     * @param productList the productList which should be displayed in catalog item list
     * @param type the type of the productlist
     *
     * @return <code>true</code> id the productlist isn't empty
     *
     */
    public static boolean prepareProductList(WebCatInfo catalog,
                                             HttpServletRequest request,
                                             ProductList productList,
                                             String type ) {

        request.setAttribute(ActionConstants.RA_SCENARIO_TYPE,type);
        catalog.setLastVisited(type);
        catalog.setLastUsedDetailScenario(type);

        request.setAttribute(ActionConstants.RA_DISPLAY_SCENARIO, type);
        request.setAttribute(ActionConstants.RA_DETAILSCENARIO, type);

        if (productList.size() > 0) {

            // set a WebCatItemList into Context to display the list in getProductsAction!
            WebCatItemList itemList = productList.createWebCatItemList(catalog);
            itemList.setListType(getListType(type));

            request.setAttribute(ActionConstants.RA_ITEMLIST_EBP, itemList);
            
            if(type.equals(ActionConstants.DS_RECOMMENDATION) || type.equals(ActionConstants.DS_BESTSELLER)) { //) || type.equals(ActionConstants.DS_CUA)) {
            	catalog.setCurrentItemList(itemList);
            }
            // Store search result in case of Back navigation
            if (catalog.getSavedItemList() == null && 
                 (catalog.getCurrentItemList() != null && catalog.getCurrentItemList().getQuery() != null)) {
                     catalog.setSavedItemList(catalog.getCurrentItemList());
            }

            request.setAttribute(ActionConstants.RA_IS_PRODUCT_LIST, type);

            request.setAttribute(ActionConstants.RA_TO_NEXT, "toItemPage");
            request.setAttribute(ActionConstants.RA_IS_QUERY,ActionConstants.RA_IS_QUERY_VALUE_YES);
            request.setAttribute(ActionConstants.RA_AREA, catalog.getCurrentArea());

            return true;
        }
        return false;
    }
    
    /**
     * sets the list type as int for the WebCatItemList 
     *
     * @param type the type of the productlist
     *
     * @return listType the type of the list as int
     *
     */
    private static int getListType(String type) {
        int listType = 0;
        
        if (type.equals(MarketingActionConstants.DS_BESTSELLER)) {
            listType = ActionConstants.CV_LISTTYPE_BESTSELLER;
        }
        else if (type.equals(MarketingActionConstants.DS_CUA)) {
            listType = ActionConstants.CV_LISTTYPE_CUA;
        }
        else if (type.equals(MarketingActionConstants.DS_RECOMMENDATION)) {
            listType = ActionConstants.CV_LISTTYPE_RECOMMENDATIONS;
        }
        else if (type.equals(MarketingActionConstants.DS_ENTRY)) {
            listType = ActionConstants.CV_LISTTYPE_CATALOG_AREA;
        }
        
        return listType;
    }
    
    /**
     * This method checks the request for an parameter with the provided key. If an
	 * attribute is not available, then the attribute is checked. If this is also
	 * null, then an empty string is returned.
	 * The value from the request will be encoded, depending on the encType parameter
	 * value. Allowed values are .
     * @param request
     * @param requestKey
     * @param encType
     * @return
     */
	public static String getValueFromRequest(HttpServletRequest request, String requestKey, int encType) {
		String toReturn = (String) request.getParameter(requestKey);
		if(toReturn == null) {
			toReturn = (String) request.getAttribute(requestKey);
		}
		
		if(toReturn == null) {
			toReturn = "";
		}
		
		switch (encType) {
			case ENCODE_TO_HTML:
				toReturn = JspUtil.encodeHtml(toReturn);
				break;
			case ENCODE_TO_JS:
				toReturn = JspUtil.escapeJavaScriptString(toReturn);
				break;
			case ENCODE_TO_URL:
				toReturn = JspUtil.encodeURL(toReturn);
				break;
			default:
				break;
		}
		
		return toReturn;
	}
}
