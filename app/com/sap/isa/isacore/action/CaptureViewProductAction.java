/*****************************************************************************
    Class         CaptureViewProductAction
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Action to fire the ViewProductEvent
    Author:       Wolfgang Sattler
    Created:      June 2001
    Version:      0.1

    $Revision: #2 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.isacore.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businessobject.businessevent.ViewProductEvent;
import com.sap.isa.businessobject.webcatalog.exchproducts.ExchProdAttributes;
import com.sap.isa.businessobject.webcatalog.exchproducts.ExchProductResult;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ResultData;

/**
 *  This action fired the view category event
 */
public class CaptureViewProductAction extends IsaCoreBaseAction {

	/**
	 * Initialize the action.
	 * In B2C this action could be used by anonymous user.
	 * 
	 * @see com.sap.isa.isacore.action.IsaCoreBaseAction#initialize()
	 */
	public void initialize() {
		if (isB2C()) {
			this.checkUserIsLoggedIn = false;
		}
	}

	protected String detailScenario;
	/**
	 * Implement this method to add functionality to your action.
	 *
	 * @param form              The <code>FormBean</code> specified in the
	 *                          config.xml file for this action
	 * @param request           The request object
	 * @param response          The response object
	 * @param userSessionData   Object wrapping the session
	 * @param requestParser     Parser to simple retrieve data from the request
	 * @param bom               Reference to the BusinessObjectManager
	 * @param log               Reference to the IsaLocation, needed for logging
	 * @param startupParameter  Object containing the startup parameters
	 * @param eventHandler      Object to capture events with
	 * @return Forward to another action or page
	 */
	public ActionForward isaPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		BusinessObjectManager bom,
		IsaLocation log,
		IsaCoreInitAction.StartupParameter startupParameter,
		BusinessEventHandler eventHandler)
		throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
		this.detailScenario = (String) request.getParameter(ActionConstants.RA_DETAILSCENARIO);
		WebCatInfo catalog = getCatalogBusinessObjectManager(userSessionData).getCatalog();

		WebCatItem item = (WebCatItem) request.getAttribute(com.sap.isa.catalog.actions.ActionConstants.RC_WEBCATITEM);

		if (item == null) { /*should never happens*/
			item = (WebCatItem) request.getAttribute(com.sap.isa.catalog.actions.ActionConstants.RA_WEBCATITEM);
		}

		if (item != null) {
			if (this.detailScenario != null) {
				if (!this.detailScenario.equalsIgnoreCase(ActionConstants.DS_IN_WINDOW)) {
					// Set the Item to Catalog Current Item only if it is not an Accessory.					
					catalog.setCurrentItem(item);
				}
			} else {
				catalog.setCurrentItem(item);
			}

			ViewProductEvent event = new ViewProductEvent(catalog, item);
			eventHandler.fireViewProductEvent(event);
		}

		String shopBackend = bom.getShop().getBackend();
		if (shopBackend.equals(Shop.BACKEND_CRM)) {
			//Exchange product result object 
			ExchProductResult exchprodResult = null;

			// create the BO ExchProdAttributes for exchange products
			ExchProdAttributes exchprodattrib =	getCatalogBusinessObjectManager(userSessionData).getExchprodAttrib();
			if (exchprodattrib == null) {
				exchprodattrib = getCatalogBusinessObjectManager(userSessionData).createExchprodAttrib();
			}

			// change the current item for the display of exchange products inthe marketing funtionality
			ResultData condPurpGroup = catalog.getCondPurpGroup();
			if (condPurpGroup != null) {
				item.setCondPurpGroup(condPurpGroup);
				try {
					exchprodResult = exchprodattrib.readExchProductAttributes(catalog);
					if (exchprodResult == null) {
						if (log.isDebugEnabled())
							log.debug("Exchange Product Result is null !! Error ! ");
					}
				} catch (CommunicationException ex) {
					log.error("Communication exception");
				} catch (java.lang.Exception ex) {
					log.error("General Exception Caught");
				}

				item.setExchProdData(exchprodResult);
			}
		}

//		String forward = requestParser.getParameter("forward").getValue().getString();
//		if (forward.length() > 0) {
//			log.exiting();
//			return mapping.findForward(forward);
//		}
		log.exiting();
		return mapping.findForward("success");
	}
}
