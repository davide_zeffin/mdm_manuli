/*****************************************************************************
    Class:        SimpleStartApplicationAction
    Copyright (c) 2006, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      22.09.2006
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.isacore.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.StartupParameter;

//TODO docu
/**
 * The class SimpleStartApplicationAction . <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class SimpleStartApplicationAction extends StartApplicationBaseAction {

	/**
	 * Overwrites the method . <br>
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @param userSessionData
	 * @param mbom
	 * @param startupParameter
	 * @return
	 * @throws IOException
	 * @throws ServletException
	 * @throws CommunicationException
	 * 
	 * 
	 * @see com.sap.isa.isacore.action.StartApplicationBaseAction#doStartApplication(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.sap.isa.core.UserSessionData, com.sap.isa.core.businessobject.management.MetaBusinessObjectManager, com.sap.isa.core.util.StartupParameter)
	 */
	public ActionForward doStartApplication(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response, UserSessionData userSessionData,
			MetaBusinessObjectManager mbom, StartupParameter startupParameter)
			throws IOException, ServletException, CommunicationException {
		
		 return mapping.findForward("success");		
	}

}
