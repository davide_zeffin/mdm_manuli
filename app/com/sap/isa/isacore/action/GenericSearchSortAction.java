 /*****************************************************************************
    Class:        GenericSearchSortAction
    Copyright (c) 2004, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author        Ralf Witt

    $Revision: #4 $
    $DateTime: 2004/06/04 14:56:10 $ (Last changed)
    $Change: 189863 $ (changelist)

*****************************************************************************/
package com.sap.isa.isacore.action;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.GenericSearchComparator;
import com.sap.isa.ui.uiclass.genericsearch.GenericSearchUIData;


/**
 * Sorts the result list  
 */
public class GenericSearchSortAction extends GenericSearchBaseAction {

    /**
     * Key to store and retrieve the column name which should be sorted into the HTTP request
     */
	public static final String RC_GENERICSEARCH_SORT_COLUMNNAME = "genericsearch.sort.columnname";
    /**
     * Key to store and retrieve the sort sequence should be sorted into the HTTP request
     */
    public static final String RC_GENERICSEARCH_SORT_SEQUENCE = "genericsearch.sort.sequence";
    /**
     * Key to store and retrieve the object type into the HTTP request
     */
    public static final String RC_GENERICSEARCH_SORT_OBJECTTYPE = "genericsearch.sort.objecttype";
    /**
     * Key to store and retrieve the date format into the HTTP request
     */
    public static final String RC_GENERICSEARCH_SORT_DATEFORMAT = "genericsearch.sort.dateformat";
    /**
     * Key to store and retrieve the number format into the HTTP request
     */
    public static final String RC_GENERICSEARCH_SORT_NUMBERFORMAT = "genericsearch.sort.numberformat";
    /**
     * Key to store and retrieve the language into the HTTP request
     */
    public static final String RC_GENERICSEARCH_SORT_LANGUAGE = "genericsearch.sort.language";
    /**
     * Method acts on following commands stored in the HTTP request.<br>
     * - columnname (name of the column which should be sorted) @see RC_GENERICSEARCH_SORT_COLUMNNAME<br>
     * - sort sequence (acsending or descending) RC_GENERICSEARCH_SORT_SEQUENCE<br>
     * <br>
     * Following Object needs to be in the user session:<br>
     * - ResultData (including the last search result)<br>
     * named with @see GenericSearchBaseAction.SC_GENERICSEARCH_RET_DOCUMENTS
	 */
    public ActionForward ecomPerform(ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response,
			UserSessionData userSessionData,
			RequestParser requestParser,
			MetaBusinessObjectManager mbom,
			boolean multipleInvocation,
			boolean browserBack)
			throws CommunicationException {
		final String METHOD_NAME = "ecomPerform()";
		log.entering(METHOD_NAME); 
        String colName = (String)request.getParameter(RC_GENERICSEARCH_SORT_COLUMNNAME);
        String objType = (String)request.getParameter(RC_GENERICSEARCH_SORT_OBJECTTYPE);
        String dateFormat = (String)request.getParameter(RC_GENERICSEARCH_SORT_DATEFORMAT);
        String numberFormat = (String)request.getParameter(RC_GENERICSEARCH_SORT_NUMBERFORMAT);
        String language = (String)request.getParameter(RC_GENERICSEARCH_SORT_LANGUAGE);
        int sortSeq = GenericSearchComparator.ASCENDING;
        try {
            sortSeq = Integer.parseInt((String)request.getParameter(RC_GENERICSEARCH_SORT_SEQUENCE));
        } catch (NumberFormatException ex) {
            log.debug("Sort request: NumberFormatEx. => Sorting sequence set to ASCENDING !");
        }
        String requestedSearch = getScreenName(request,userSessionData);
        // Get Result list to sort
        ResultData result = (ResultData)userSessionData.getAttribute( (SC_GENERICSEARCH_RET_DOCUMENTS + requestedSearch) );
        Map sortInfo = (Map)userSessionData.getAttribute((GenericSearchUIData.RC_SORTINFO + requestedSearch));
        String translationPrefix = "";
        if (sortInfo != null) {
            translationPrefix = (String)sortInfo.get(GenericSearchComparator.TRANSLATEPREFIX);
        }
        GenericSearchComparator gsComp = new GenericSearchComparator(sortSeq, objType, dateFormat, numberFormat, language, translationPrefix);
        // Now sort
        log.debug("Sort request: Fieldname = " + colName + " / sorting sequence = " + Integer.toString(sortSeq) + 
                  " / Objecttype = " + objType + " / Dateformat = " + dateFormat + " / Numberformat = " + numberFormat);
        result.sort(colName, gsComp);
        // Write sorting information as Map into Request
        sortInfo = new HashMap();
        sortInfo.put(colName, Integer.toString(sortSeq));
        userSessionData.setAttribute((GenericSearchUIData.RC_SORTINFO + requestedSearch), sortInfo);

        log.exiting();
        return mapping.findForward("resultlist");
    }
    /**
     * Return the Screen name which describes the search criteria in the XML file
     * @return String screen name
     */
    public String getScreenName(HttpServletRequest request, UserSessionData userSessionData) {
        String searchToDisplay = request.getParameter(GenericSearchUIData.RC_SEARCHCRITERASCREENNAME);
        if (searchToDisplay == null) {
            DocumentHandler documentHandler = (DocumentHandler)userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
            if (documentHandler != null) {
                searchToDisplay = documentHandler.getOrganizerParameter(GenericSearchUIData.RC_SEARCHCRITERASCREENNAME);            
            }
        }
        return searchToDisplay;
    }
}