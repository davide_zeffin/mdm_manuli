/*****************************************************************************
	Class         PaymentActions
	Copyright (c) 2004, SAP AG, All rights reserved.
	Description:  Action to prepare the determination process
	Author:
	Created:      15. Mai 2003
	Version:      1
*****************************************************************************/

package com.sap.isa.isacore.action;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.SalesDocumentStatus;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businessobject.businessevent.PlaceOrderEvent;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.order.Order;
import com.sap.isa.businessobject.order.OrderStatus;
import com.sap.isa.businessobject.ordertemplate.OrderTemplate;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.History;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.action.order.DocumentStatusOrderQuotAction;
import com.sap.isa.isacore.action.order.MaintainBasketBaseAction;
import com.sap.isa.maintenanceobject.businessobject.UIElementBase;
import com.sap.isa.payment.backend.boi.PaymentBaseData;
import com.sap.isa.payment.backend.boi.PaymentBaseTypeData;
import com.sap.isa.payment.backend.boi.PaymentCCardData;
import com.sap.isa.payment.backend.boi.PaymentMethodData;
import com.sap.isa.payment.businessobject.PaymentBase;
import com.sap.isa.payment.businessobject.PaymentCCard;
import com.sap.isa.payment.businessobject.PaymentMethod;
import com.sap.isa.ui.uicontrol.UIController;


/**
 * A collection of static methods which handles payment actions l
 *
 * @author SAP
 * @version 1.0
 *
 */
public class PaymentActions {

   // public constants

    /******************************************************************
     * Authoritzaion status values
     ******************************************************************/    
	//Authorization USED
	public static final String AUTH_USED = PaymentCCardData.AUTH_USED;

	// Authorization exist
	public static final String AUTH_EXIST = PaymentCCardData.AUTH_EXIST;
	
	// Authorization ERROR
 	public static final String AUTH_ERROR = PaymentCCardData.AUTH_ERROR;

	// Authoization doesn't exist
	public static final String NO_AUTH_MSG = PaymentCCardData.NO_AUTH;

	
	/******************************************************************
	 * Request contexts parameter values
	 ******************************************************************/
	/**
	 * Request context parameter NEWLINES
	 */
	public static final String NEWLINES = "payCardNewLines";

	/**
	 * Request context parameter CARDTYPES  
	 */
	public static final String CARDTYPES = "cardTypes";

	/**
	 *   Request context parameter CARDMONTHS
	 */
	public static final String CARDMONTHS = "cardMonths";

	/**
	 *   Request context parameter CARDYEARS
	 */
	public static final String CARDYEARS = "cardYears";

    /**
     * Request context paramenter PAYMENT
     */
    public static final String PAYMENT = "payment";
	
	/**
	 * Request context paramenter PAYMENT_TYPES
	 */
	public static final String PAYMENT_TYPES ="paymentTypes";
	
	/**
	 * Request context parameter NUMBER_OF_CARDS
	 */
	public static final String NUMBER_OF_CARDS = "numberOfCards";
	
	/**
	 * Request context paramenter document messages
	 */
	public static final String DOC_MESSAGES = "messages";
	
	/**
	 * Request context parameter for payment process
	 */
	public static final String PAYMENT_PROCESS = "process";
	
	
	
	/************************************************************* 
	 * Payment process values
	 **************************************************************/
	// Process values for basket creation
	public static final String BASKET_CREATION = "basketCreation";
	// Process value for order change
	public static final String ORDER_CHANGE = "orderChange";
	// Process value for order simulation
	public static final String BASKET_SIMULATION = "basketSimulate";
	// Process value for quotation ordering
	public static final String QUOT_ORDERING = "quotOrdering";
	// Process value for order confirmation
	public static final String ORDER_CONFIRMATION = "orderConfirm";
	

   /************************************************************************
    * Payment constants
    ************************************************************************/
	
	// Constant indicates the use of one card
	public static final String ONE_CARD = "oneCard";
	
	// Constant indicates the use of multiple cards
	public static final String MULTIPLE_CARDS = "multipleCards";
	
	
	/*****************************************************************************
	 * Payment action forwards 
	 *****************************************************************************/
    public static final String FORWARD_TO_PAYMENT_MAINTAINENCE = "maintainpayment";
    
    public static final String FORWARD_TO_PAYMENT_ERROR = "paymenterror";
    
    public static final String FORWARD_TO_UPDATE = "updatepayment";
    
    public static final String FORWARD_TO_ORDERSUMMARY_SIMULATION = "ordersummarysimulation";
    
    public static final String FORWARD_TO_ORDERSUMMARY_QUOTORDER = "ordersummaryquotorder";
    
    public static final String FORWARD_TO_CANCEL_SIMULATION = "cancelsimulation";
    
    public static final String FORWARD_TO_CANCEL_QUOTORDER = "cancelorderquot";


    /****************************************************************************
     * Button values
     ****************************************************************************/
    public static final String BUTTON = "selectedaction";
    public static final String CANCEL = "cancel";
    public static final String UPDATE = "update";
    public static final String SAVE   = "save";

    /******************************************************************************* 
     * Payment card helpvalues
     *******************************************************************************/
	public static final List CARD_MONTHS = new ArrayList();
	public static final List CARD_YEARS = new ArrayList();

	static {
		CARD_MONTHS.add("01");
		CARD_MONTHS.add("02");
		CARD_MONTHS.add("03");
		CARD_MONTHS.add("04");
		CARD_MONTHS.add("05");
		CARD_MONTHS.add("06");
		CARD_MONTHS.add("07");
		CARD_MONTHS.add("08");
		CARD_MONTHS.add("09");
		CARD_MONTHS.add("10");
		CARD_MONTHS.add("11");
		CARD_MONTHS.add("12");

		Calendar cal = Calendar.getInstance();
		int thisYear = cal.get(Calendar.YEAR);

		// offer next 12 years
		for (int year = thisYear; year < thisYear + 12; year++) {
		CARD_YEARS.add("" + year);
		}
	}

 
    
    
	// Action utility methods
	
	
	/**
	 * 
	 * @param parser
	 * @param userSessionData
	 * @param preOrderSalesDocument
	 * @param log
	 * @param isDebugEnabled
	 * @throws CommunicationException
	 */
    public static void parseRequestPayment(
		      	RequestParser parser,
			    UserSessionData userSessionData,
			    SalesDocument preOrderSalesDocument,
			    IsaLocation log,
			boolean isDebugEnabled)
						throws CommunicationException {


 
   // get request parameters
   RequestParser.Parameter paymentMaintain     = parser.getParameter("payment_maintain");
	
   if (paymentMaintain.isSet() && paymentMaintain.getValue().getString().equals("true")) 
   {
	//RequestParser.Parameter parsepaymreqdata 	= parser.getParameter("parsepaymreqdata[]");
	RequestParser.Parameter paytype 			= parser.getParameter("paytype");
	//RequestParser.Parameter paymethodtechkey 	= parser.getParameter("paymethodtechkey[]");		
	RequestParser.Parameter cardtype            = parser.getParameter("nolog_cardtype[]");
	RequestParser.Parameter nolog_cardno        = parser.getParameter("nolog_cardno[]");
	RequestParser.Parameter nolog_cardcvv       = parser.getParameter("nolog_cardcvv[]");
	RequestParser.Parameter nolog_cardno_suffix = parser.getParameter("nolog_cardno_suffix[]");
	RequestParser.Parameter cardholder          = parser.getParameter("nolog_cardholder[]");
	RequestParser.Parameter cardmonth           = parser.getParameter("nolog_cardmonth[]");
	RequestParser.Parameter cardyear            = parser.getParameter("nolog_cardyear[]");
	RequestParser.Parameter authlimit           = parser.getParameter("authlimit[]");
	RequestParser.Parameter carddeleteflag      = parser.getParameter("deleteflag[]");
    RequestParser.Parameter nolog_cardtechkey	= parser.getParameter("nolog_cardtechkey[]");
 
	// HeaderSalesDocument header = new HeaderSalesDocument();
	HeaderSalesDocument header = preOrderSalesDocument.getHeader();
				  
	// update the payment data 
	PaymentBaseData payment = header.createPaymentData();

 	if (payment.getPaymentMethods() != null && payment.getPaymentMethods().size() > 0) {
 		payment.getPaymentMethods().clear();
 	}
 	
 	if (payment.getError() == true) {
 		payment.setError(false);
 	}
 	
    List payCards = new ArrayList();        
    String pTypeKeyStr = "";	

	if (paytype.isSet()) {
		pTypeKeyStr = paytype.getValue().getString(); 
	}
// support non-selection of paytype (only invoice and credit cards are possible)
	else {	 
    	if (nolog_cardno.getNumValues() > 0) {
    		for (int i=0; i<nolog_cardno.getNumValues(); i++) {
    			if (! carddeleteflag.getValue(i).isSet() && nolog_cardno.getValue(i).getString().length() > 0){
    				pTypeKeyStr = PaymentBaseTypeData.CARD_TECHKEY.getIdAsString();  
    				break;    		  		
    			}    		
    		}
    	}
	}
	
    if (pTypeKeyStr.equals("")){	
		pTypeKeyStr = PaymentBaseTypeData.INVOICE_TECHKEY.getIdAsString();	                        
    }

	if (userSessionData.getAttribute(ActionConstantsBase.RC_PAYMENT_CARDS) != null) {
		payCards = (List)userSessionData.getAttribute(ActionConstantsBase.RC_PAYMENT_CARDS);
	}

    
// check if there already credit card data exists in backend
 	if  (pTypeKeyStr.equals(PaymentBaseTypeData.CARD_TECHKEY.getIdAsString()) || 
 	     ((!pTypeKeyStr.equals(PaymentBaseTypeData.CARD_TECHKEY.getIdAsString())) && 
			nolog_cardno.getNumValues() > 0)) {  	 
		// update old entries
		if (nolog_cardtechkey.isSet()){
			for (int i = 0; i < nolog_cardno.getNumValues(); i++) {
				if (nolog_cardno.getValue(i).getString().length() > 0) {  
					boolean found = false;
					if (nolog_cardtechkey.getValue(i) != null && nolog_cardtechkey.getValue(i).getString().length() > 0)	{							
						Iterator iter = payCards.iterator();
						PaymentCCard card = new PaymentCCard();	
						for (int l = 0; l < payCards.size(); l++) {
							 card = (PaymentCCard) iter.next();
						 	if (card.getTechKey().getIdAsString().equals(nolog_cardtechkey.getValue(i).getString())) {
						 		found = true;
					 			break;
							 }		
						}							
				
						if (found == true){
							found = false;
							if (nolog_cardtechkey.getValue(i).isSet()) {
								card.setTechKey(new TechKey (nolog_cardtechkey.getValue(i).getString()));
							}
							if (nolog_cardno.getValue(i).isSet()) {
								card.setNumber(nolog_cardno.getValue(i).getString());	 
							} else {
								card.setNumber("");	   	
							}
							if (cardtype.getValue(i).isSet()) {
								card.setTypeTechKey(new TechKey(cardtype.getValue(i).getString()));
							} else {
								card.setTypeTechKey(null);		     
							}
							if  (nolog_cardcvv.getValue(i).isSet()) {
								if (nolog_cardcvv.getValue().getString().indexOf("*") < 0) {						
									card.setCVV(nolog_cardcvv.getValue(i).getString());
								} else {
									if (preOrderSalesDocument.getLastEnteredCVVS().size() > 0 ) {							
										card.setCVV(preOrderSalesDocument.getLastEnteredCVVS().get(i).toString());
									} else {
										card.setCVV(""); 		
									}
								}
							} else {
								card.setCVV("");	
							}
							if (nolog_cardno_suffix.getValue(i).isSet()) {
								card.setNumberSuffix(nolog_cardno_suffix.getValue(i).getString());
							} else {
								card.setNumberSuffix("");	
							}
							if (cardholder.getValue(i).isSet()) {
								card.setHolder(cardholder.getValue(i).getString());
							} else {
								card.setHolder("");	
							}  	     
							if (cardmonth.getValue(i).isSet()) {
								card.setExpDateMonth(cardmonth.getValue(i).getString());
							} else {
								card.setExpDateMonth("");	
							}
							if (cardyear.getValue(i).isSet()) {
								String year = convertYear(cardyear.getValue(i).getString());	
								card.setExpDateYear(year);
							} else {
								card.setExpDateYear("");	       		   
							}
							if (authlimit.getValue(i).isSet()) { 	
								card.setAuthLimit(authlimit.getValue(i).getString().trim());
								if (card.getAuthLimit().length() > 0 &&
									(!card.getAuthLimit().equals("0") &&
									!card.getAuthLimit().equals("0.00") &&
									!card.getAuthLimit().equals("0,00")) ) {
									card.setAuthLimited(true); 	
								} else {
									card.setAuthLimited(false);
								}					  
							} else {
								card.setAuthLimit("");
								card.setAuthLimited(false);	
							}
							if (carddeleteflag.getValue(i).isSet() || !pTypeKeyStr.equals(PaymentBaseTypeData.CARD_TECHKEY.getIdAsString())) {
								card.setMode(PaymentCCardData.DELETE_MODE);					
							} else {
								card.setMode(PaymentCCardData.UPDATE_MODE);
							}
						payment.getPaymentMethods().add((PaymentMethodData) card);	 	
						} //found is true
					} else {
						PaymentMethod paymentMethod = ((PaymentBase) payment).createPaymentMethodByPaymentType(pTypeKeyStr);
						paymentMethod.setPayTypeBaseTechKey(new TechKey(pTypeKeyStr));

						if (paymentMethod instanceof PaymentCCard){
							PaymentCCard paymentCCard = (PaymentCCard) paymentMethod;
							if (cardtype.getValue(i).isSet()) {
								paymentCCard.setTypeTechKey(new TechKey(cardtype.getValue(i).getString()));
							}
							if (nolog_cardno.getValue(i).isSet()) {
								paymentCCard.setNumber(nolog_cardno.getValue(i).getString());	 
							}
							if (nolog_cardcvv.getValue(i).isSet()) {
								paymentCCard.setCVV(nolog_cardcvv.getValue(i).getString());  
							}
							if (nolog_cardno_suffix.getValue(i).isSet()) {
								paymentCCard.setNumberSuffix(nolog_cardno_suffix.getValue(i).getString());
							}
							if (cardholder.getValue(i).isSet()) {
								paymentCCard.setHolder(cardholder.getValue(i).getString());
							}
							if (cardmonth.getValue(i).isSet()) {
								paymentCCard.setExpDateMonth(cardmonth.getValue(i).getString());
							}
							if (cardyear.getValue(i).isSet()) {
								String year = convertYear(cardyear.getValue(i).getString());	
								paymentCCard.setExpDateYear(year);
							}
							if (authlimit.getValue(i).isSet()) {
								paymentCCard.setAuthLimit(authlimit.getValue(i).getString().trim());
								if (paymentCCard.getAuthLimit().length() > 0 &&
									(!paymentCCard.getAuthLimit().equals("0") &&
									!paymentCCard.getAuthLimit().equals("0.00") &&
									!paymentCCard.getAuthLimit().equals("0,00")) ) {
									paymentCCard.setAuthLimited(true); 	
								}		
								else {
									paymentCCard.setAuthLimited(false);
								}
							} else {
								paymentCCard.setAuthLimited(false);	
							} //(authlimit.getValue(i).isSet())
							if (carddeleteflag.getValue(i).isSet()) {	
								paymentCCard.setMode(PaymentCCardData.DELETE_MODE);
							} else {
								paymentCCard.setMode(PaymentCCardData.CREATE_MODE);
							}
								payment.addPaymentMethod(paymentMethod);
						} // if payment card		
					}// if techkey is set
				}// if (nolog_cardno.getValue(i).getString().length > 0 
			}// for nolog_cardno
		}// if nolog_cardtechkey is set
		else  { // create new payment cards 
			for (int i = 0; i < nolog_cardno.getNumValues(); i++) { 
				if (nolog_cardno.getValue(i).getString().length() > 0) {                 			    	 					 									
	 				PaymentMethod paymentMethod = ((PaymentBase) payment).createPaymentMethodByPaymentType(pTypeKeyStr);
	 				paymentMethod.setPayTypeBaseTechKey(new TechKey(pTypeKeyStr));
	 				
	 				if (paymentMethod instanceof PaymentCCard){
	 					PaymentCCard paymentCCard = (PaymentCCard) paymentMethod;
	 					
						if (cardtype.getValue(i).isSet()) {
							paymentCCard.setTypeTechKey(new TechKey(cardtype.getValue(i).getString()));
						}
						if (nolog_cardno.getValue(i).isSet()) {
							paymentCCard.setNumber(nolog_cardno.getValue(i).getString());	 
     					}
						if (nolog_cardcvv.getValue(i).isSet()) {
							paymentCCard.setCVV(nolog_cardcvv.getValue(i).getString());  
						}
						if (nolog_cardno_suffix.getValue(i).isSet()) {
							paymentCCard.setNumberSuffix(nolog_cardno_suffix.getValue(i).getString());
						}
						if (cardholder.getValue(i).isSet()) {
							paymentCCard.setHolder(cardholder.getValue(i).getString());
						}
						if (cardmonth.getValue(i).isSet()) {
							paymentCCard.setExpDateMonth(cardmonth.getValue(i).getString());
						}
						if (cardyear.getValue(i).isSet()) {
				  			String year = convertYear(cardyear.getValue(i).getString());	
				  			paymentCCard.setExpDateYear(year);
						}
						if (authlimit.getValue(i).isSet()) {
							paymentCCard.setAuthLimit(authlimit.getValue(i).getString().trim());
			  				if (paymentCCard.getAuthLimit().length() > 0 &&
				  				(!paymentCCard.getAuthLimit().equals("0") &&
					   			!paymentCCard.getAuthLimit().equals("0.00") &&
					   			!paymentCCard.getAuthLimit().equals("0,00")) ) {
								paymentCCard.setAuthLimited(true); 	
				  			}		
			  				else {
								paymentCCard.setAuthLimited(false);
			 				}
						} else {
							paymentCCard.setAuthLimited(false);	
			  			} //(authlimit.getValue(i).isSet())
		   				if (carddeleteflag.getValue(i).isSet()) {	
		   					paymentCCard.setMode(PaymentCCardData.DELETE_MODE);
		   				} else {
							paymentCCard.setMode(PaymentCCardData.CREATE_MODE);
			   			}
					payment.addPaymentMethod(paymentMethod);
	 				}// if payment card
	 			} // if nolog_cardno.getValue(i).getString().length() > 0
			} // for (int i = 0; i < nolog_cardno.getNumValues(); i++) 		
   		}// new cards
 	}//pTypeKeyStr.equals(PaymentBaseTypeData.CARD_TECHKEY			 	
 	else if (pTypeKeyStr.equals(PaymentBaseTypeData.INVOICE_TECHKEY.getIdAsString()) ||
			 pTypeKeyStr.equals(PaymentBaseTypeData.COD_TECHKEY.getIdAsString())) {
		PaymentMethod paymentMethod = ((PaymentBase) payment).createPaymentMethodByPaymentType(pTypeKeyStr);
		paymentMethod.setPayTypeBaseTechKey(new TechKey(pTypeKeyStr));
		payment.addPaymentMethod(paymentMethod);	 	
 	}
	 
	header.setPaymentData(payment);	 	
    }//paymentMaintain.isSet()
   }
   
   
	/**
	  * Returns a list from 1 to 12 
	  * representign the months. 
	  * 
	  * @return list of months
	  */
	 public List getCardMonths() { 
		return CARD_MONTHS;
	 }
    
	 /**
	  * Returns a list of 12 years 
	  * beginning with the current year.
	  * 
	  * @return list of the next 12 years
	  */
	 public List getCardYears() {
		 return CARD_YEARS;
	 }
	 
	public static void setPaymentAttributes (
			  UserSessionData userSessionData,
			  HttpServletRequest request,
			  SalesDocument preOrderSalesDocument) 
	   throws CommunicationException {
     

	   request.setAttribute(PAYMENT_TYPES, preOrderSalesDocument.readPaymentType());
	   request.setAttribute(CARDTYPES, preOrderSalesDocument.readCardType()); 
	   request.setAttribute(CARDMONTHS, CARD_MONTHS);
	   request.setAttribute(CARDYEARS, CARD_YEARS);
	   if (preOrderSalesDocument.getHeader().getPaymentData() != null) {
		 request.setAttribute(PAYMENT, preOrderSalesDocument.getHeader().getPaymentData());	
	
 		List paymentCards = new ArrayList();
 		if (preOrderSalesDocument.getHeader().getPaymentData() != null && preOrderSalesDocument.getHeader().getPaymentData().getPaymentMethods() != null && preOrderSalesDocument.getHeader().getPaymentData().getPaymentMethods().size() > 0) {
	 		List paymentMethods = preOrderSalesDocument.getHeader().getPaymentData().getPaymentMethods();
	 		Iterator iter = paymentMethods.iterator();			
	 		while (iter.hasNext()){					
		 	PaymentMethod payMethod = (PaymentMethod) iter.next();			
		 		if (payMethod != null){
		 		// if the payment method is Credit card, fill the import table accordingly
			 		if(payMethod.getPayTypeTechKey().equals(PaymentBaseTypeData.CARD_TECHKEY )) {                    		
				 		PaymentCCard payCCard = (PaymentCCard) payMethod;							
				 		paymentCards.add(payCCard);
			  		}
				}
	 		}
 			}
 		if (paymentCards.size() > 0) {
			request.setAttribute(ActionConstantsBase.RC_PAYMENT_CARDS, paymentCards);
			userSessionData.setAttribute(ActionConstantsBase.RC_PAYMENT_CARDS, paymentCards);				      
 		}
	   }

	   if (request.getParameter(NEWLINES) != null) {
		 request.setAttribute(NEWLINES, request.getParameter(NEWLINES));
	   }
	   request.setAttribute(NUMBER_OF_CARDS, MULTIPLE_CARDS ); // XCM Parameter auswerten!!!!
		 
	 }

  
      /**
       * Puts payment related sources into the context
       * 
       * @param request the request object
       * @param preOrderSalesDocument the current sales document
       * @throws CommunicationException
       */
      public static void setPaymentAttributes (
	            UserSessionData userSessionData,
				HttpServletRequest request,
				SalesDocument preOrderSalesDocument,
				String process) 
		 throws CommunicationException {
     
	     request.setAttribute(PAYMENT_PROCESS, process);
	     setPaymentAttributes(userSessionData, request, preOrderSalesDocument);
	     	 
       }
      
       
	/**
	 * Puts payment related sources into the context
	 * 
	 * @param request the request object
	 * @param preOrderSalesDocument the current sales document
	 * @throws CommunicationException
	 */
	public static void setPaymentAttributes (
			  UserSessionData userSessionData,
			  HttpServletRequest request,
			  SalesDocumentStatus orderSalesDocument) 
	   throws CommunicationException {
     
	   //request.setAttribute(PAYMENT_TYPES, orderSalesDocument.readPaymentType());
	   //request.setAttribute(CARDTYPES, orderSalesDocument.readCardType()); 
	   request.setAttribute(CARDMONTHS, CARD_MONTHS);
	   request.setAttribute(CARDYEARS, CARD_YEARS);
	   if (orderSalesDocument.getOrderHeader().getPaymentData() != null) {
			request.setAttribute(PAYMENT, orderSalesDocument.getOrderHeader().getPaymentData());
			List paymentCards = new ArrayList();
			if (orderSalesDocument.getOrderHeader().getPaymentData() != null && orderSalesDocument.getOrderHeader().getPaymentData().getPaymentMethods() != null && orderSalesDocument.getOrderHeader().getPaymentData().getPaymentMethods().size() > 0) {
				List paymentMethods = orderSalesDocument.getOrderHeader().getPaymentData().getPaymentMethods();
				Iterator iter = paymentMethods.iterator();			
				while (iter.hasNext()){					
					PaymentMethod payMethod = (PaymentMethod) iter.next();			
					if (payMethod != null){
					// if the payment method is Credit card, fill the import table accordingly
						if(payMethod.getPayTypeTechKey().equals(PaymentBaseTypeData.CARD_TECHKEY )) {                    		
							PaymentCCard payCCard = (PaymentCCard) payMethod;							
							paymentCards.add(payCCard);
						}
					}
				}
			
				if (paymentCards.size() > 0) {
					request.setAttribute(ActionConstantsBase.RC_PAYMENT_CARDS, paymentCards);
					userSessionData.setAttribute(ActionConstantsBase.RC_PAYMENT_CARDS, paymentCards);				      
				}
			}
	   }  
	   if (request.getParameter(NEWLINES) != null) {
		 request.setAttribute(NEWLINES, request.getParameter(NEWLINES));
	   }
	   request.setAttribute(NUMBER_OF_CARDS, MULTIPLE_CARDS ); // XCM Parameter auswerten!!!!
		 
	 }
	 
	 
	 public static ActionForward showPayment (ActionMapping mapping,
								HttpServletRequest request,
								HttpServletResponse response,
								UserSessionData userSessionData,
								RequestParser parser,
								BusinessObjectManager bom,
								IsaLocation log,
								IsaCoreInitAction.StartupParameter startupParameter,
								BusinessEventHandler eventHandler,
								boolean multipleInvocation,
								boolean browserBack)
		throws CommunicationException {

        
	DocumentHandler documentHandler = (DocumentHandler) userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
	// get the document on top  
	DocumentState doc = documentHandler.getManagedDocumentOnTop();
	if (doc == null) {
	  throw new PanicException("No document returned from document handler");
	}

	ManagedDocument mdoc = (ManagedDocument) doc;
	doc = mdoc.getDocument();
	SalesDocument salesDoc = (SalesDocument) doc;
			
	String paymentProcess = (String) userSessionData.getAttribute(PAYMENT_PROCESS);
	if (paymentProcess == null) {
	  throw new PanicException("No payment process specified");
	}
	
	salesDoc.read();
		
	PaymentActions.setPaymentAttributes (userSessionData,
						                 request,
						                 salesDoc,
				                         paymentProcess); 

	request.setAttribute(DOC_MESSAGES, salesDoc.getMessageList());


    return mapping.findForward(FORWARD_TO_PAYMENT_MAINTAINENCE);
 }

	 
	 
	 
	 public static ActionForward maintainPayment(ActionMapping mapping,
						HttpServletRequest request,
						HttpServletResponse response,
						UserSessionData userSessionData,
						RequestParser parser,
						BusinessObjectManager bom,
						IsaLocation log,
						IsaCoreInitAction.StartupParameter startupParameter,
						BusinessEventHandler eventHandler,
						boolean multipleInvocation,
						boolean browserBack)
           throws CommunicationException { 
           	
			String forwardTo = null;
			
			String paymentProcess = (String) userSessionData.getAttribute(PAYMENT_PROCESS);
			if (paymentProcess == null) {
		     throw new PanicException("No payment process specified");
			}  else {
			userSessionData.setAttribute(PAYMENT_PROCESS, paymentProcess);		
		   }
           		
			DocumentHandler documentHandler = (DocumentHandler) userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
			// get the document on top  
			DocumentState doc = documentHandler.getManagedDocumentOnTop();
			if (doc == null) {
			  throw new PanicException("No document returned from document handler");
			}

			ManagedDocument mdoc = (ManagedDocument) doc;
			doc = mdoc.getDocument();
			SalesDocument salesDoc = (SalesDocument) doc;
			
			String selectedButton = "";
			RequestParser.Parameter selButton = parser.getParameter(BUTTON);
			if (selButton.isSet()) {
			  selectedButton = selButton.getValue().getString();
			} else {
				throw new PanicException("No button selected");    	
			}

            if (selectedButton.equals(CANCEL)) {
//!!!!        Is an order deletable if a used credit card authorization exists
//!!!!        If not the existence of an used auth. must be verified             
              if (paymentProcess.equals(BASKET_SIMULATION)) {
                forwardTo = FORWARD_TO_CANCEL_SIMULATION;
                userSessionData.removeAttribute(PaymentActions.PAYMENT_PROCESS);
              } else if (paymentProcess.equals(QUOT_ORDERING)) {
				OrderStatus orderStatus = bom.getOrderStatus();
				if (orderStatus == null)
					throw new PanicException("status.message.nostatusobject");  
						               
				bom. releaseOrder(DocumentStatusOrderQuotAction.ORDER_INDEX);
				userSessionData.removeAttribute(DocumentStatusOrderQuotAction.SC_QUOT_TO_ORDER);
				documentHandler.release(salesDoc); 
				documentHandler.setOnTop(orderStatus);
              	forwardTo = FORWARD_TO_CANCEL_QUOTORDER;
              }
                           
              return mapping.findForward(forwardTo);	
            }

            //update header concerning payment
		    salesDoc.readHeader();
			salesDoc.clearMessages();

			PaymentActions.parseRequestPayment(parser,
								   userSessionData,
								   salesDoc,
								   log,
								   log.isDebugEnabled());

			salesDoc.updateHeader(bom.getBUPAManager(), bom.getShop());
			salesDoc.read();
			
			// if there are any error messages for the SalesDocument,
			// you cannot order. The user stays on the same page.
			if (salesDoc.getHeader().getPaymentData().getError() ||
			    !salesDoc.isValid()) {
			   return mapping.findForward(FORWARD_TO_PAYMENT_ERROR);  
			}	
					
			if (selectedButton.equals(SAVE)) {   
				int retCode = ((Order) salesDoc).saveOrderAndCommit();  // may throw CommunicationException

                if (retCode == 0 ) {
				  salesDoc.read();
				  userSessionData.setAttribute(
					MaintainBasketBaseAction.SC_SHIPTOS, salesDoc.getShipTos());


                  if (paymentProcess.equals(BASKET_SIMULATION)) {   
                
				    // fire event
				    eventHandler.firePlaceOrderEvent(
						new PlaceOrderEvent(bom.getOrder(),bom.getUser()));
           	
           	    
					// to get the confirmation screen working the basket must be added to the document
					// handler again, replacing the managed document created in the simulation action
					// here we only handle baskets therefore we take the original basket 
					ManagedDocument mDoc = new ManagedDocument(bom.getBasket(),
														   "basket",
														   "new",
														   null,
														   null,
														   null,
														   "basket",
														   null,
														   null);
					documentHandler.add(mDoc);
					documentHandler.setOnTop(bom.getBasket());


					// If the basket is recoverable, delete the basket in the database, it is not needed anymore
					if (bom.getBasket().isDocumentRecoverable()) {
						bom.getBasket().destroyContent();
					}
					
					forwardTo = FORWARD_TO_ORDERSUMMARY_SIMULATION;  //---> go to 
					
                   } else if (paymentProcess.equals(QUOT_ORDERING)) {
					 
					History history = documentHandler.getHistory();
					history.remove(salesDoc.getHeader().getDocumentType().trim(),salesDoc.getHeader().getSalesDocNumber());					 
					//remove orderstatusdetail from bom & document manager
					OrderStatus orderStatus = bom.getOrderStatus();
					if (orderStatus == null)
						throw new PanicException("status.message.nostatusobject");                 
					bom.releaseOrderStatus();
					documentHandler.release(orderStatus);    
					
					forwardTo = FORWARD_TO_ORDERSUMMARY_QUOTORDER;  //---> go to              
                 
                   }
                } else {
                 forwardTo = FORWARD_TO_PAYMENT_ERROR;	  
                }
			} else if (selectedButton.equals(UPDATE)) {         

		     forwardTo = FORWARD_TO_UPDATE;   // --> go to MaintainPaymentPrepareAction
		   } 

		   return mapping.findForward(forwardTo);           	
           	
           	
}	
	
	public static boolean isPaymentMaintenanceAvailable(UserSessionData userSessionData, SalesDocument salesDoc) {
		
		// if the number is set to visible in the UIControll 
		// it will be assumed that the payment maintenance
		// is available!
		UIController uiController = UIController.getController(userSessionData);
		UIElementBase multipleCards = uiController.getUIElement("payment.card.number");
		UIElementBase oneCard = uiController.getUIElement("payment.one.card.number");
	
	    try {
		 if ( (multipleCards != null && !multipleCards.isHidden() || oneCard != null && !oneCard.isHidden()) && 
		       (!(salesDoc instanceof OrderTemplate) &&
		       (salesDoc.getHeader().getDocumentType() == null && salesDoc instanceof Order || 
		          salesDoc.getHeader().getDocumentType().equals(HeaderData.DOCUMENT_TYPE_ORDER))) &&
		       salesDoc.readPaymentType().isCardAvailable()) {
			return true;
		 }
	    } catch (CommunicationException ex){ 
	    	return false;
	    }
	    
		return false;
	}


	 public static ActionForward maintainPayment2(ActionMapping mapping,
						HttpServletRequest request,
						HttpServletResponse response,
						UserSessionData userSessionData,
						RequestParser parser,
						BusinessObjectManager bom,
						IsaLocation log,
						IsaCoreInitAction.StartupParameter startupParameter,
						BusinessEventHandler eventHandler,
						boolean multipleInvocation,
						boolean browserBack)
		   throws CommunicationException { 
           	
			String forwardTo = null;
			
			String paymentProcess = (String) userSessionData.getAttribute(PAYMENT_PROCESS);
			if (paymentProcess == null) {
			 throw new PanicException("No payment process specified");
			} else {
		     userSessionData.setAttribute(PAYMENT_PROCESS, paymentProcess);		
			}
           		
			DocumentHandler documentHandler = (DocumentHandler) userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
			// get the document on top  
			DocumentState doc = documentHandler.getManagedDocumentOnTop();
			if (doc == null) {
			  throw new PanicException("No document returned from document handler");
			}

			ManagedDocument mdoc = (ManagedDocument) doc;
			doc = mdoc.getDocument();
			SalesDocument salesDoc = (SalesDocument) doc;
			
			String selectedButton = "";
			RequestParser.Parameter selButton = parser.getParameter(BUTTON);
			if (selButton.isSet()) {
			  selectedButton = selButton.getValue().getString();
			} else {
				throw new PanicException("No button selected");    	
			}

			if (selectedButton.equals(CANCEL)) {
//!!!!        Is an order deletable if a used credit card authorization exists
//!!!!        If not the existence of an used auth. must be verified             
			  if (paymentProcess.equals(BASKET_SIMULATION)) {
				forwardTo = FORWARD_TO_CANCEL_SIMULATION;
			  } else if (paymentProcess.equals(QUOT_ORDERING)) {
				OrderStatus orderStatus = bom.getOrderStatus();
				if (orderStatus == null)
					throw new PanicException("status.message.nostatusobject");  
						               
				bom. releaseOrder(DocumentStatusOrderQuotAction.ORDER_INDEX);
				documentHandler.release(salesDoc); 
				documentHandler.setOnTop(orderStatus);
				forwardTo = FORWARD_TO_CANCEL_QUOTORDER;
			  }
                           
			  return mapping.findForward(forwardTo);	
			}

			//update header concerning payment
			salesDoc.readHeader();
			salesDoc.clearMessages();

			PaymentActions.parseRequestPayment(parser,
								   userSessionData,
								   salesDoc,
								   log,
								   log.isDebugEnabled());
								   
			if (paymentProcess.equals(QUOT_ORDERING)) {
			  // Workaround to solve ACE check problem of wrong contact person
			  // in newly created order for quotation if quotation was created
			  // by another user. 
			  salesDoc.getHeader().getPartnerList().clearList();
			}	
			salesDoc.updateHeader(bom.getBUPAManager(), bom.getShop());
			salesDoc.isDirty();
			salesDoc.read();
			
			// if there are any error messages for the SalesDocument,
			// you cannot order. The user stays on the same page.
			if (salesDoc.getHeader().getPaymentData().getError() ||
			    !salesDoc.isValid() ) {
			   return mapping.findForward(FORWARD_TO_PAYMENT_ERROR);  
			}	
					
			if (selectedButton.equals(SAVE)) {   

				if (paymentProcess.equals(BASKET_SIMULATION)) {
					   
					userSessionData.setAttribute(
					  MaintainBasketBaseAction.SC_SHIPTOS, salesDoc.getShipTos());
					
					forwardTo = FORWARD_TO_ORDERSUMMARY_SIMULATION;  //---> go to 
					
				} else if (paymentProcess.equals(QUOT_ORDERING)) {
									  
				  int retCode = ((Order) salesDoc).saveOrderAndCommit();  // may throw CommunicationException

				  if (retCode == 0 ) {				  	  
					  salesDoc.read();
					  userSessionData.setAttribute(
					    MaintainBasketBaseAction.SC_SHIPTOS, salesDoc.getShipTos());
					 									 
					//remove orderstatusdetail from bom & document manager
					OrderStatus orderStatus = bom.getOrderStatus();
					if (orderStatus == null)
						throw new PanicException("status.message.nostatusobject");  
						
					History history = documentHandler.getHistory();
					history.remove(orderStatus.getOrderHeader().getDocumentType().trim(),orderStatus.getOrderHeader().getSalesDocNumber());
	               
					bom.releaseOrderStatus();
					documentHandler.release(orderStatus);    
					
					forwardTo = FORWARD_TO_ORDERSUMMARY_QUOTORDER;  //---> go to              
                 
				  }
				  else {
					salesDoc.isDirty();
					salesDoc.read();
				    forwardTo = FORWARD_TO_PAYMENT_ERROR;	  
			 	  }
				}  
			} else if (selectedButton.equals(UPDATE)) {         

			 forwardTo = FORWARD_TO_UPDATE;   // --> go to MaintainPaymentPrepareAction
		   } 

		   return mapping.findForward(forwardTo);           	
           	
           	
}	

	 private static String convertYear(String year) {
	 	  
	
        if (year.length() == 2) {
		  Calendar cal = Calendar.getInstance();
		  int thisYear = cal.get(Calendar.YEAR);
		  		  
		  int yearAsInt = Integer.parseInt(year);
		   
		  if (yearAsInt >= 0  &&
		      yearAsInt <= 99) {
			for (int i = thisYear; i < thisYear + 12; i++) {
			  String theYearAsString = Integer.toString(i);
			  String theYearAsStringPrefix = theYearAsString.substring(0,2);
			  String theYearAsStringSuffix = theYearAsString.substring(2);	
			  if (theYearAsStringSuffix.equals(year)) {
			  	return theYearAsStringPrefix + year;
			  }  		  
			} 
			return year;			    
		  } else {
		  	return year;
		  }     	  	
        } else {
         return year;
        }		
	 }
	 
	public static void removePaymentProcessAttribute(UserSessionData userSessionData) {
	
	  if (userSessionData.getAttribute(PAYMENT_PROCESS) != null) {	  
	    userSessionData.removeAttribute(PAYMENT_PROCESS);
	  }
	  if (userSessionData.getAttribute(ActionConstantsBase.RC_PAYMENT_CARDS) != null) {
	  	userSessionData.removeAttribute(ActionConstantsBase.RC_PAYMENT_CARDS);	  	
	  }
	}
}