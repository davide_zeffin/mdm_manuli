/*****************************************************************************
    Class:        EComConstantsBase
    Copyright (c) 2006, SAP AG, All rights reserved.
    Created:      July 2006
    Version:      1.0

    $Revision: #1 $
    $Date: 2006/07/19 $
*****************************************************************************/
package com.sap.isa.isacore.action;

/**
 * this class defines some constants for request attributes (RA) which are used
 * in ECom actions to store and retrieve data from request context.
 */

public interface EComConstantsBase {

    /**
     * key to store the techKey of the item that should be an anchor in the itemdetails
     */
    public final static String RC_ANCHOR_KEY           = "anchorKey";
  
    /**
     * key to store if the anchor should be set on the config link of the item
     * it shoukld be set to "X" in those cases
     */
    public final static String RC_ANCHOR_IS_CFG        = "anchorIsCfg";
	/**
	 * Name of the search item status parameter in the request context
	 */
	public static final String RK_SEARCH_ITEM_STATUS = "itemStatus";
	/**
	 * Name of the search item property parameter in the request context
	 */
	public static final String RK_SEARCH_ITEM_PROPERTY = "itemProperty";
	/**
	 * Name of the search item property low value parameter in the request context
	 */
	public static final String RK_SEARCH_ITEM_PROPERTY_LOW_VALUE = "itemPropertyLowValue";
	/**
	 * Name of the search item property high value parameter in the request context
	 */
	public static final String RK_SEARCH_ITEM_PROPERTY_HIGH_VALUE = "itemPropertyHighValue";
    
}
