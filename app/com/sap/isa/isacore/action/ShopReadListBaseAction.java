/*****************************************************************************
	Class:        ShopReadListBaseAction 
	Copyright (c) 2004, SAPLabs, Palo Alto, All rights reserved.
	Author        SAP

	$Revision: #1 $

*****************************************************************************/

package com.sap.isa.isacore.action;

import javax.servlet.http.HttpServletRequest;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Search;
import com.sap.isa.businessobject.SearchResult;
import com.sap.isa.businessobject.ShopListSearchCommand;
import com.sap.isa.businessobject.User;
import com.sap.isa.core.ContextConst;
import com.sap.isa.core.FrameworkConfigManager;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.core.xcm.config.ComponentConfig;
import com.sap.isa.core.xcm.config.ComponentConfigContainer;
import com.sap.isa.isacore.action.b2c.B2cConstants;

public class ShopReadListBaseAction extends IsaCoreBaseAction {
	/**
	 * Name of the lists of shops stored in the request context.
	 */
	public static final String RK_SHOP_LIST = "shoplist";

	/**
	 * Name of the request parameter directly setting the shop id.
	 */
	public static final String PARAM_SHOPID = "shopId";
	/**
	 * Flag to signal problems during auto entry process
	 */
	public static final String RK_AUTOENTRYERROR = "autoentryerror";
	/**
	 * Name of the request attribute to get additional infos about the problem
	 */
	public static final String RK_AUTOENTRYADDINFO = "autoentryaddinfo";

	/**
	 * Read a list of WebShop
	 * @param userSessionData
	 * @param requestParser
	 * @param bom
	 * @param log
	 * @param interactionConfig
	 * @return The list of WebShop objects
	 * @throws CommunicationException
	 */
	public SearchResult readShopList(
		UserSessionData userSessionData,
		BusinessObjectManager bom,
		IsaLocation log,
		String scenario)
		throws CommunicationException {

		ResultData shopList = null;
		boolean isDebugEnabled = log.isDebugEnabled();
		Search search = bom.createSearch();
		SearchResult result = null;
		User user = bom.getUser();
		String application = getInitParameter(ContextConst.IC_SCENARIO);
		String configuration =
			(String) userSessionData.getAttribute(SessionConst.XCM_CONF_KEY);

		ShopListSearchCommand searchCommand =
			new ShopListSearchCommand(
				application,
				scenario,
				user.getLanguage(),
				configuration);

		result = search.performSearch(searchCommand);
		if (result.getNumberOfHits() <= 0) {

			if (isDebugEnabled) {
				log.debug("In readShopList menthod, no shop found, bailing out!");
			}

			// found no shop, this can't happen - panic!
			throw new PanicException("panic.shoplist.zeroelements");
		}
		return result;
	}

	/*
	* This attribute is used to enable search engine adaptability for the shop
	*/
	public String enableWebCrowlersAndOthers(
		HttpServletRequest request,
		UserSessionData userSessionData,
		IsaLocation log,
		String scenario) {
		boolean isDebugEnabled = log.isDebugEnabled();
		String sEnableWebCrawling = null;
		if (scenario.equalsIgnoreCase("B2C")) {
			ComponentConfigContainer ccc = null;
			ccc = FrameworkConfigManager.XCM.getApplicationScopeConfig();
			ComponentConfig compContainer = null;
			if (ccc != null) {
				compContainer = ccc.getComponentConfig("b2c", "b2cconfig");
			} else {
				if (isDebugEnabled) {
					log.debug(
						"Error in XCM FrameworkConfig Manager: it's null");
				}
			}

			if (compContainer != null) {
				sEnableWebCrawling =
					compContainer.getParamConfig().getProperty(
						B2cConstants.ENABLE_WEB_CRAWLERS);
				if (isDebugEnabled) {
					log.debug(
						"got xcm value for b2cconfig: sEnableWebCrawling "
							+ sEnableWebCrawling);
				}
			} else {
				if (isDebugEnabled) {
					log.debug(
						"Error in reading configuration files for B2C it's null");
				}
			}
		}
		if (sEnableWebCrawling == null) {
			sEnableWebCrawling = "false";
		}

		if (isDebugEnabled) {
			log.debug("got the webcrawlerEnabled value " + sEnableWebCrawling);
		}

		userSessionData.setAttribute(
			B2cConstants.ENABLE_WEB_CRAWLERS,
			sEnableWebCrawling);
		/*
		* This attribute is used to add directly to the basket the first time when web crawler
		* direct access is made. Thereafter, the attribute is turned to true and normal functionality
		* of the web shop is restored.
		*/

		String sSearchShowBasketDirect =
			(String) request.getParameter(B2cConstants.ADD_BASKET_DIRECT);
		String sItemId =
			(String) request.getParameter(B2cConstants.ADD_ITMID_DIRECT);
		String sShopId =
			(String) request.getParameter(B2cConstants.SHOPID_DIRECT);
		if (sSearchShowBasketDirect != null
			&& sItemId != null
			&& sShopId != null
			&& sSearchShowBasketDirect.equalsIgnoreCase("true")
			&& sEnableWebCrawling.equalsIgnoreCase("true")) {
			userSessionData.setAttribute(
				B2cConstants.BASKET_LOADED_DIRECT,
				"false");
			userSessionData.setAttribute(
				B2cConstants.ADD_ITMID_DIRECT,
				sItemId);
			if (isDebugEnabled) {
				log.debug("request parameters passed correctly: " + sItemId);
			}
		} else {
			userSessionData.setAttribute(
				B2cConstants.BASKET_LOADED_DIRECT,
				"true");
			if (isDebugEnabled) {
				log.debug("web crawlers not enabled");
			}
		}
		return sEnableWebCrawling;
	}
	
	/**
	 * Adjust the number of hits
	 * @param request
	 * @param userSessionData
	 * @param log
	 * @param result
	 * @param logId
	 * @return
	 */
	public boolean adjustNumofHits(
									HttpServletRequest request,
									UserSessionData userSessionData,
									IsaLocation log,
									SearchResult result,
									String logId){
		String strBasketLoadOver = (String)userSessionData.getAttribute(B2cConstants.BASKET_LOADED_DIRECT);			
		int intTot = result.getNumberOfHits();
		boolean blnFlg = false;
		String strShopIdValue = "";
		boolean isDebugEnabled = log.isDebugEnabled();	
		ResultData shopList = (ResultData)request.getAttribute(RK_SHOP_LIST);
		if(strBasketLoadOver != null && strBasketLoadOver.equalsIgnoreCase("false")) {            
			String strShopIdRequest =(String) request.getParameter(B2cConstants.SHOPID_DIRECT);
			if(strShopIdRequest != null ) {								
				log.debug(logId + "strShopIdRequest" + strShopIdRequest);
				shopList.absolute(1);
				while(intTot > 0){
					shopList.next();
					log.debug(logId + "intTot" + intTot +"\""+shopList.getRow());
					strShopIdValue = shopList.getRowKey().toString();
					log.debug(logId + "strShopIdValue" + strShopIdValue);
					if(strShopIdRequest.equalsIgnoreCase(strShopIdValue)){
						blnFlg = true;
						// Save shopId in request
						request.setAttribute(PARAM_SHOPID, shopList.getRowKey());
						break;
					}
					intTot--;
				}					
			}
		}
            
		if (isDebugEnabled) {
			log.debug("found more than one shop");
		}		
		return blnFlg;
	}
	
}
