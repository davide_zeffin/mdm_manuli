/*****************************************************************************
    Class         DeterminationAction
    Copyright (c) 2003, SAP AG, All rights reserved.
    Description:  Action for the Profile Maintenance
    Author:       SAP AG
    Created:      15.05.2003
    Version:      1
*****************************************************************************/

package com.sap.isa.isacore.action;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.ManagedDocumentDetermination;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.item.AlternativProduct;
import com.sap.isa.businessobject.item.ItemList;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.businessobject.order.CampaignListEntry;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.action.PaymentActions;


/**
 * Read determination choices.
 *
 * @author SAP AG
 * @version 1
 */

public class DeterminationAction extends IsaCoreBaseAction {
    
    
    /**
     * Constant for available forwards, value is &quot;showbasketframe&quot;.
     */
    protected static final String FORWARD_SHOWBASKET  = "showbasketframe";
    
    /**
     * Constant for available forwards, value is &quot;basketsimulat&quot;.
     */
    protected static final String FORWARD_SIMULATE    = "basketsimulate";
    
    /**
     * Constant for available forwards, value is &quot;showsimulation&quot;.
     */
    protected static final String FORWARD_SHOW_SIMULATION    = "showsimulation";
    
    /**
     * Constant for available forwards, value is &quot;basketsend&quot;.
     */
    protected static final String FORWARD_BASKET_SEND    = "basketsend";
    
    /**
     * Constant for available forwards, value is &quot;showorderframe&quot;.
     */
    protected static final String FORWARD_SHOWORDER  = "showorderframe";
    
    /**
     * Constant for available forwards, value is &quot;ordersend&quot;.
     */
    protected static final String FORWARD_ORDER_SEND    = "ordersend";
    
    /**
     * Request context varaiable, to signal, that an action or jsp was
     * called from this action
     */
    public static final String RC_CALLED_FROM_DET  = "calldfromdet";
    
    /**
     * Forward to display determination 
     */
    private static final String FORWARD_FS_DET     = "fsdet";
    
    /**
     * Choice for no selection
     */
    public static final int NO_SELECTION     = -1;
    
	/**
	 * Choice for no campign assignment wanted
	 */
    public static final int ASSIGN_NO_CAMPAIGN     = -2;
    
    protected boolean isSelectionMissing = false;
    protected boolean cancelpressed = false;
    protected boolean sendpressed = false;
    
    protected String caller = "";
    protected String docMode = "";
    protected String simulatedDoc = "";
    protected HashSet selectedNone = null; 
	
    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser parser,
            BusinessObjectManager bom,
            IsaLocation log) throws CommunicationException {
            	
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);        
        // Page that should be displayed next.
        String forwardTo = "";
        isSelectionMissing = false;
        
        DocumentHandler documentHandler = (DocumentHandler)
                userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
        ManagedDocument mandoc = documentHandler.getManagedDocumentOnTop();
        
        if (mandoc == null) { // haben wir eins?
           // use global forward to show valid frame
           // this if statement is also reached, if the readForUpdate below failed
           // and the message.jsp was displayed and the user clicks on the back icon
           // of the browser, because in this case the last executed action, which is this action,
           // is recalled. But in that case, the managed document was already deleted
            log.exiting();
            return mapping.findForward("updatedocumentview");
        }
        
        caller = ((ManagedDocumentDetermination) mandoc).getCaller();
        docMode = ((ManagedDocumentDetermination) mandoc).getDocMode();
        simulatedDoc = ((ManagedDocumentDetermination) mandoc).getSimulatedDoc();
        selectedNone = ((ManagedDocumentDetermination) mandoc).getSelectedNone();
                     
        parseRequest(request,parser,userSessionData, log, log.isDebugEnabled());
        
        SalesDocument salesDoc = (SalesDocument) mandoc.getDocument();
                
        if (cancelpressed) {
            forwardTo = determineForward(userSessionData, mandoc, documentHandler, salesDoc, request, log);
            log.exiting();
            return mapping.findForward(forwardTo);
        }
           
        
        List[] retList = parseRequestItems(parser,userSessionData, salesDoc, log, log.isDebugEnabled());
        
        // !!!!!!!!!!!!! Attention don't call a simple salesDoc.update(..) without taking care of messages
        // !!!!!!!!!!!!! related to rule based ATP. This messages are only displayed once and will be
        // !!!!!!!!!!!!! deleted by a new update. Unfortunately those message must be displayed to the user
        // !!!!!!!!!!!!! to prevent for example that a basket is ordered and the user is not aware that an
        // !!!!!!!!!!!!! ATP substitution has taken place.
        
        //	any alternative products selected
        if (retList[0].size() > 0) { 
            salesDoc.replaceItemsByAlternativProduct(retList[0]);
        }
        //	any campaigns selected
		if (retList[1].size() > 0) { 
		    salesDoc.updateItemCampaignInformation(retList[1]);
		}
		log.exiting();
        return mapping.findForward(determineForward(userSessionData, mandoc, documentHandler, salesDoc, request, log));
    }
    
    /**
     * 
     * Determine where to go next
     * 
     */
    protected String determineForward(UserSessionData userSessionData, ManagedDocument mandoc, DocumentHandler documentHandler, 
                                       SalesDocument salesDoc, HttpServletRequest request, IsaLocation log) {
        
        String forward = "";
        boolean newSelectionReq = false;
       
        
        //check if new alternative products or campaigns have been found and the screen must be redisplayed
        if (salesDoc.isDeterminationRequired()) {

          try {
              salesDoc.readAllItemsForUpdate();  
          }
          catch(CommunicationException e) {
              log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, e.getMessage());
          }
          
          ItemList items = salesDoc.getItems();
          
          // Now check, if new alternativ products or campaigns were found
          for (int i = 0; i < items.size(); i++) {
              if ((!items.get(i).getAlternativProductList().isEmpty() || !items.get(i).getDeterminedCampaigns().isEmpty()) &&
                  !selectedNone.contains(items.get(i).getTechKey().getIdAsString())) {
                  newSelectionReq = true;
              }
          }
            
        }
        
        if (!cancelpressed && (isSelectionMissing || newSelectionReq)){
        	// not for every item an option was selected, or new lists where found, to select from 
            forward = FORWARD_FS_DET;
        }
        else {
            ManagedDocument assocManagedDocument = mandoc.getAssociatedDocument();
            
            if(cancelpressed && ManagedDocumentDetermination.DOC_MODE_SIMULATE.equals(docMode)) {
                // if determination was canceled for a simulated document, we even have 
                // a second layer of assoiated documents
                assocManagedDocument = assocManagedDocument.getAssociatedDocument();
            }

            assocManagedDocument.setState(DocumentState.TARGET_DOCUMENT);
            documentHandler.add(assocManagedDocument);
            documentHandler.setOnTop(assocManagedDocument.getDocument());
            
            request.setAttribute(RC_CALLED_FROM_DET,"y");
            
            if (caller.equals(ManagedDocumentDetermination.CALLER_MAINTAIN_BASKET_REFRESH)){
                forward = FORWARD_SHOWBASKET;
            }
            else if (caller.equals(ManagedDocumentDetermination.CALLER_MAINTAIN_BASKET_SIMULATE)){
                     if (cancelpressed || !selectedNone.isEmpty()) {
                         forward = FORWARD_SHOWBASKET;
                     }
                     // was determination run for the simulated document
                     else if (ManagedDocumentDetermination.DOC_MODE_SIMULATE.equals(docMode)) {
						if (PaymentActions.isPaymentMaintenanceAvailable(userSessionData, salesDoc) &&
						    salesDoc.isValid()) {			  
						  // remember where we come from
						 userSessionData.setAttribute(PaymentActions.PAYMENT_PROCESS, PaymentActions.BASKET_SIMULATION);
						 forward = PaymentActions.FORWARD_TO_PAYMENT_MAINTAINENCE;
						} else {
                         forward = FORWARD_SHOW_SIMULATION;
						}
                     }
                     // or was determination run for the basket
                     else {
                         request.setAttribute("simulatepressed", simulatedDoc);
                         forward = FORWARD_SIMULATE;
                     }     
            }
            else if (caller.equals(ManagedDocumentDetermination.CALLER_MAINTAIN_BASKET_SEND)) {
                if (cancelpressed || !selectedNone.isEmpty()) {
                    forward = FORWARD_SHOWBASKET;
                } 
                else {
                    forward = FORWARD_BASKET_SEND;
                }
            }
            else if (caller.equals(ManagedDocumentDetermination.CALLER_MAINTAIN_ORDER_REFRESH)) {
                forward = FORWARD_SHOWORDER;
            }
            else if (caller.equals(ManagedDocumentDetermination.CALLER_MAINTAIN_ORDER_SEND)) {
                if (cancelpressed || !selectedNone.isEmpty()) {
                    forward = FORWARD_SHOWORDER;
                } 
                else {
                    forward = FORWARD_ORDER_SEND;
                }

           }
        }
        
        return forward;
    }

    
    /**
     * Helper method to parse the request for some basic entries
     * 
     * @param request The request object
     * @param parser The request, wrapped into a parser object
     * @param userSessionData   object wrapping the session
     * @param log reference to the logging context
     * @param isDebugEnabled flag indicating wheter or not logging output
     * is        written
     */
    protected void parseRequest(
                HttpServletRequest request,
                RequestParser parser,
                UserSessionData userSessionData,
                IsaLocation log,
                boolean isDebugEnabled)
                throws CommunicationException {
                    
        cancelpressed = false;
        sendpressed = false;
                    
        if ((request.getParameter("cancelpressed") != null) &&
            (request.getParameter("cancelpressed").length() > 0)) {
            cancelpressed = true;
        }
        if ((request.getParameter("sendpressed") != null) &&
            (request.getParameter("sendpressed").length() > 0)) {
            sendpressed = true;
        }
    }
    
    /**
     * Helper method to parse the request and construct items
     * for the <code>SalesDocument</code> form the
     * request's data.
     * <p>
     * The following fields are taken from the request and used to
     * construct the item:
     * </p>
     * <table border="1" cellspacing="0" cellpadding="2">
     *   <tr>
     *     <td><b>field</b></td>
     *     <td><b>use</b></td>
     *   </tr>
     *   <tr>
     *     <td>itemTechKey[]</td>
     *     <td>technical key of the item, only relevant for items already added to the document</td>
     *   </tr>
     *   <tr>
     *     <td>altProd[]</td>
     *     <td>index of the alternativ product oir campaign</td>
     *   </tr>
     * </table>
     * <br><br>
     * @param parser The request, wrapped into a parser object
     * @param userSessionData   object wrapping the session
     * @param preOrderSalesDocument the document to store the data in
     * @param headerReqDeliveryDate the date set in the header for the
     *        requested delivery date. This parameter is used to provide
     *        the functionality to provide a preset date for all items
     * @param log reference to the logging context
     * @param isDebugEnabled flag indicating wheter or not logging output
     *        is written
     * @return List[] list array, containing two list, 
     *                the fisrt entry is a list of item GUIDS, for items that 
     *                must be replaced by a choosen alternative product
     *                the second entry is a list of item GUIDS, for items the 
     *                campaign information must be updated
     */
    protected List[] parseRequestItems(
            RequestParser parser,
            UserSessionData userSessionData,
            SalesDocument preOrderSalesDocument,
            IsaLocation log,
            boolean isDebugEnabled)
                        throws CommunicationException {
                            
        List itemGuidsToProcessAltProduct = new ArrayList();
		List itemGuidsToProcessCampaigns = new ArrayList();
		List retList[] = {itemGuidsToProcessAltProduct, itemGuidsToProcessCampaigns};

                            
        ItemList items = preOrderSalesDocument.getItems();

        // get the prod determination fields from the request
        RequestParser.Parameter itemTechKey =
                parser.getParameter("itemTechKey[]");

        // even this paramter is calles alt it is used for all selection lists
        // like alternative products, campaigns, ...
        RequestParser.Parameter altIdx =
                parser.getParameter("alt[]");

        if (isDebugEnabled) {
            log.debug("Found " + itemTechKey.getNumValues() + " TechKeys in request");
        }

        ItemSalesDoc item;
        AlternativProduct altProd;
        CampaignListEntry campaign;
        // noCampaign is used, to tell the backend, that no campaign assignment is wanted
		CampaignListEntry noCampaign = new CampaignListEntry("", TechKey.EMPTY_KEY, "", false, true); 
        
        isSelectionMissing = false;
		
        for (int i = 0; i < itemTechKey.getNumValues(); i++) {

            if (itemTechKey.getValue(i).getString().length() > 0) {
            	
				item = items.get(new TechKey(itemTechKey.getValue(i).getString()));
				
				altProd = null;
				campaign = null;
                
                if (altIdx.getValue(i).getString().length() > 0) {
                
                    if (altIdx.getValue(i).getInt() != NO_SELECTION) {
                    	
                    	// selection for alternative products
						if (!item.getAlternativProductList().isEmpty()) {
							altProd = item.getAlternativProductList().getAlternativProduct(altIdx.getValue(i).getInt());
                      
							item.setProductId(altProd.getSystemProductGUID());
							itemGuidsToProcessAltProduct.add(item.getTechKey());
						}
                        // selection for campaigns
						else {
							campaign = noCampaign;
							
							if (altIdx.getValue(i).getInt() != ASSIGN_NO_CAMPAIGN) {
							    campaign = item.getDeterminedCampaigns().getCampaign(altIdx.getValue(i).getInt());
							}
                      
                            item.getAssignedCampaigns().clear();
							item.getAssignedCampaigns().addCampaign(campaign);
                            itemGuidsToProcessCampaigns.add(item.getTechKey());
						}

                        if (isDebugEnabled) {
                            log.debug("Processed item  - " + item);
                        }
                    }
                    else {
                        selectedNone.add(itemTechKey.getValue(i).getString());
                    }
                }
                else {
                    isSelectionMissing = true;
                }
                
            }
        }
        
        return retList;
    }

}