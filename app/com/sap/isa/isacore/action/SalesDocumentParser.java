/*****************************************************************************
    Class         SalesDocumentParser
    Copyright (c) 2004, SAP AG, All rights reserved.
    Description:  Standard parser methods for salesdocuments
    Author:       SAP AG
    Created:      July 2004
    Version:      1.0

    $Revision: #1 $
    $Date: 2004/07/27 $

*****************************************************************************/

package com.sap.isa.isacore.action;

import java.util.StringTokenizer;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.ExtendedStatus;
import com.sap.isa.businessobject.ExtendedStatusListEntry;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.ShipTo;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.item.ItemList;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.businessobject.order.ExtRefObject;
import com.sap.isa.businessobject.order.ExtRefObjectList;
import com.sap.isa.businessobject.order.ExternalReference;
import com.sap.isa.businessobject.order.ExternalReferenceList;
import com.sap.isa.businessobject.order.PartnerList;
import com.sap.isa.businessobject.order.PartnerListEntry;
import com.sap.isa.businessobject.order.Text;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;

/**
 *  This class is the defualt parser for standard salesdocuments
 *  and might be used for parsing the order.jsp and orderchange.jsp
 */
public class SalesDocumentParser {
    
    //private int countCBs = 0;
    //private int countElements = 0;
    /**
      * Method to set the batch related item fields
      *
      * @param item The item to set the fields
      * @param countCBs   object wrapping the session
      * @param countElements the document to store the data in
      * @param batchID reference to the logging context
      */
    protected int[] setItemBatchFields(ItemSalesDoc item, int i, int countCBs, int countElements,  
                                      RequestParser.Parameter elementCount,  RequestParser.Parameter batchID, 
                                      RequestParser.Parameter batchDedicated, RequestParser.Parameter batchClassAssigned, 
                                      RequestParser.Parameter elementName, RequestParser.Parameter elementTxt,
                                      RequestParser.Parameter elementCountCBs, RequestParser.Parameter elementCharVal, RequestParser.Parameter elementCharValTxt,
                                      RequestParser.Parameter elementCharValMax) {
                                          
        int[] batchCounters = {0, 0};
        
        // Set the batch values for the item
        item.setBatchID(batchID.getValue(i).getString().toUpperCase());
        item.setBatchDedicated(batchDedicated.getValue(i).getBoolean());
        item.setBatchClassAssigned(batchClassAssigned.getValue(i).getBoolean());
                
        // Set choosen batch inputs
        item.getBatchCharListJSP().clear();
                
        for(int j=0; j<elementCount.getValue(i).getInt(); j++ ){
            item.getBatchCharListJSP().add();
            item.getBatchCharListJSP().get(j).setCharName(elementName.getValue(countElements+j).getString());
            item.getBatchCharListJSP().get(j).setCharTxt(elementTxt.getValue(countElements+j).getString());
                    
            for(int k=0; k<elementCountCBs.getValue(countElements+j).getInt(); k++){
                item.getBatchCharListJSP().get(j).setCharValue(elementCharVal.getValue(countCBs+k).getString());
                item.getBatchCharListJSP().get(j).setCharValTxt(elementCharValTxt.getValue(countCBs+k).getString());
                item.getBatchCharListJSP().get(j).setCharValMax(elementCharValMax.getValue(countCBs+k).getString());  
            }
                
            countCBs = countCBs + elementCountCBs.getValue(countElements+j).getInt();
        }
        countElements = countElements + elementCount.getValue(i).getInt();
        
        batchCounters[0] = countCBs;
        batchCounters[1] = countElements;
        
        return batchCounters;
    }
    
    /**
     * this methdos determines, if the customerNumber has changed
     * if so, it returns the new number, othrwise it returns null.
     *
     * @param parser The request, wrapped into a parser object
     */
    public String checkSoldToChange(RequestParser parser) {
        String newCustomerNumber = null;

        RequestParser.Parameter customerNumber =
                parser.getParameter("customerNumber");

        RequestParser.Parameter customerNumberOld =
                parser.getParameter("customerNumberOld");

        // the customerNumber field is only available, if the SoldTo is selectable
        // was the soldto changed by entering an new id in the customerNumber Field ?
        if (customerNumber.isSet()) {
            String customerNumberVal = customerNumber.getValue().getString();
            String customerNumberOldVal = customerNumberOld.getValue().getString();

            if (!customerNumberVal.equals(customerNumberOldVal)) {
                newCustomerNumber = customerNumberVal;
            }
        }

        return newCustomerNumber;
    }
    
    /**
     * Template method to be overwritten by the subclasses of this class.
     * This method provides the possibilty, to easily extend the parsing
     * process for the header of a sales document, accordingly to special
     * customer requirements. By default this method is empty.
     * <code>customerExitParseRequestHeader</code> method.
     *
     * @param requestParser     parser to simple retrieve data from the request
     * @param userSessionData   object wrapping the session
     * @param header            the current header
     * @param salesDoc          the current salesDocument
     *
     */
     public void customerExitParseRequestHeader(
                RequestParser parser,
                UserSessionData userSessionData,
                HeaderSalesDocument header,
                SalesDocument salesDoc) {
     }
     
   /**
    * Template method to be overwritten by the subclasses of this class.
    * This method provides the possibilty, to easily extend the parsing
    * process for the items of a sales document, accordingly to special
    * customer requirements. By default this method is empty.
    * <code>customerExitParseRequestHeader</code> method.
    *
    * @param requestParser     parser to simple retrieve data from the request
    * @param userSessionData   object wrapping the session
    * @param itemSalesDoc      the current item edited
    * @param itemIdx           the index of item related fields in the request
    * @param itemIdx           the current SalesDocument  
    *
    */
    public void customerExitParseRequestItem(
               RequestParser parser,
               UserSessionData userSessionData,
               ItemSalesDoc itemSalesDoc,
               int itemIdx,
               SalesDocument salesDoc) {
    }
    
    /**
     * Helper method to parse the request and construct the header of
     * a <code>SalesDocument</code> form the
     * request's data.
     * <p>
     * The following fields are taken from the request and used to
     * construct the header:
     * </p>
     * <table border="1" cellspacing="0" cellpadding="2">
     *   <tr>
     *     <td><b>field</b></td>
     *     <td><b>use</b></td>
     *   </tr>
     *   <tr>
     *     <td>poNumber</td>
     *     <td>purchase order number the customer entered</td>
     *   </tr>
     *   <tr>
     *     <td>headerDescription</td>
     *     <td>description the customer entered for the order</td>
     *   </tr>
     *   <tr>
     *     <td>headerReqDeliveryDate</td>
     *     <td>requested delivery date for the order; data is used to set the date on the item level</td>
     *   </tr>
     *   <tr>
     *     <td>headerShipTo</td>
     *     <td>ship to selected on the header level</td>
     *   </tr>
     *   <tr>
     *     <td>shipCond</td>
     *     <td>shipping conditions for the document</td>
     *   </tr>
     * </table>
     * <br><br>
     *
     * @param parser The request, wrapped into a parser object
     * @param userSessionData   object wrapping the session
     * @param header the header to fill from the request
     * @param shipTos array of shipTos, to search the belonging shipTo for the items
     * @param salesDoc the current SalesDocument 
     * @param isOrderChange flag to indicate, if true the items for a document in orderchange must be
     *        parsed and created or for a newly created document if false
     * @param log reference to the logging context
     * @param isDebugEnabled flag indicating wheter or not logging output
     *        is written
     */
    public void parseHeader(RequestParser parser,
                            UserSessionData userSessionData,
                            HeaderSalesDocument header,
                            SalesDocument salesDoc,
                            boolean isOrderChange,
                            IsaLocation log,
                            boolean isDebugEnabled)
                        throws CommunicationException {

        // get Header fields from the request
        RequestParser.Parameter poNumber =
                parser.getParameter("poNumber");

        RequestParser.Parameter headerDescription =
                parser.getParameter("headerDescription");

        RequestParser.Parameter headerReqDeliveryDate =
                parser.getParameter("headerReqDeliveryDate");

        RequestParser.Parameter headerShipTo =
                parser.getParameter("headerShipTo");

        RequestParser.Parameter shipCond =
                parser.getParameter("shipCond");

        // get the businesspartner fields from the request
        RequestParser.Parameter businessPartnerRole =
                parser.getParameter("businessPartnerRole[]");

        RequestParser.Parameter businessPartnerTechKey =
                parser.getParameter("businessPartnerTechKey[]");
                

        RequestParser.Parameter businessPartnerId =
                parser.getParameter("businessPartnerId[]");

        RequestParser.Parameter headerComment =
                parser.getParameter("headerComment");
                
        // campaign data
        RequestParser.Parameter headCampId =
                parser.getParameter("headCampId[]"); 
                
        RequestParser.Parameter invHeadCampId =
                parser.getParameter("invHeadCampId[]"); 
                
        // incoterms
        RequestParser.Parameter incoterms1 =
                parser.getParameter("incoterms1");
                
        RequestParser.Parameter incoterms2 = 
                parser.getParameter("incoterms2");
                
        RequestParser.Parameter headRecallId = 
                parser.getParameter("headRecallId");
                
        RequestParser.Parameter deliveryPriority = 
        		parser.getParameter("deliveryPriority");
        
		RequestParser.Parameter headerLatestDlvDate =
				parser.getParameter("headerlatestdlvdate");
				
		RequestParser.Parameter vinNumber =
				parser.getParameter("addVinNum");
				
		RequestParser.Parameter vinNum =
						parser.getParameter("vinNum");				
							
		RequestParser.Parameter vinKey =
						parser.getParameter("headerVinKeys[]");	
								
		RequestParser.Parameter extReferenceType =
		        parser.getParameter("extReferenceType[])");
		        
		RequestParser.Parameter extReferenceNumber =
				parser.getParameter("extReferenceNumber[])");
				
		RequestParser.Parameter extReferenceTechKey =
				parser.getParameter("extReferenceTechKey[])");
            
        // empty the data of the sales document
        header.clear();
        
        if (isOrderChange) {
            header.setTechKey(salesDoc.getTechKey());
        }

        header.setPurchaseOrderExt(poNumber.getValue().getString());
        header.setDescription(headerDescription.getValue().getString());
        header.setReqDeliveryDate(headerReqDeliveryDate.getValue().getString());
        header.setShipCond(shipCond.getValue().getString());
        
		header.setLatestDlvDate(headerLatestDlvDate.getValue().getString());
		
        ShipTo selectedHeaderShipTo = salesDoc.getShipTo(new TechKey(headerShipTo.getValue().getString()));
        header.setShipTo(selectedHeaderShipTo);

        if (headerComment.isSet() && headerComment.getValue().getString().length() > 0) {
            header.setText(new Text("1000", headerComment.getValue().getString()));
        }

        enhancePartnerList(userSessionData, header);
        PartnerList bpList = header.getPartnerList();

        for (int i = 1; i <= businessPartnerRole.getNumValues(); i++) {
          if (!getRelevantPartnerFunctions(userSessionData).contains(businessPartnerRole.getValue(i).getString())) {	
            PartnerListEntry bpListEntry =
                new PartnerListEntry(new TechKey(businessPartnerTechKey.getValue(i).getString()),
                                     businessPartnerId.getValue(i).getString());
            bpList.setPartner(businessPartnerRole.getValue(i).getString(), bpListEntry);
          }  
        }

		// external reference numbers
		if (extReferenceType != null && extReferenceType.getNumValues() > 0) {
			ExternalReferenceList extReferenceList = new ExternalReferenceList();
			for (int i = 0; i < extReferenceType.getNumValues(); i++) {
				ExternalReference extReference = new ExternalReference();
				extReference.setRefType(extReferenceType.getValue(i).getString());
				extReference.setData(extReferenceNumber.getValue(i).getString());
				extReference.setTechKey(new TechKey(extReferenceTechKey.getValue(i).getString()));
				extReferenceList.addExtRef(extReference);
			}
			header.setAssignedExternalReferences(extReferenceList);
		}
		
		String extRefObjectData = "";
		String  extRefObjectData_0 = "";
		ExtRefObject  extRefObject ;
		header.getAssignedExtRefObjects().clear();
        // external reference objects
		if (vinNumber != null && vinNumber.getValue().getString().length() > 0) {
			StringTokenizer st = new StringTokenizer(vinNumber.getValue().getString(), ";", false);
			int i = 0;
			while (st.hasMoreTokens()) {
				extRefObject = new ExtRefObject();
				if (vinKey != null  && vinKey.getValue(i).getString().length() > 0) {
					extRefObject.setTechKey(new TechKey(vinKey.getValue(i).getString()));		
				}
				extRefObjectData = st.nextToken();
				if (i != 0) {
					extRefObject.setData(extRefObjectData);
				} else {
					extRefObjectData_0 = vinNum.getValue().getString();
					if (extRefObjectData.equals(extRefObjectData_0)) {
						extRefObject.setData(extRefObjectData);
					} else {
						extRefObject.setData(extRefObjectData_0);
					}
				}
				if (extRefObject.getData().trim().length() > 0 || !extRefObject.getTechKey().isInitial()) {
					header.getAssignedExtRefObjects().addExtRefObject(extRefObject);	
				}
				i = i+1;
			}
		}
	
		if ((vinNumber != null && vinNumber.getValue().getString().length() == 0) &&
			  (vinNum != null && vinNum.getValue().getString().length() > 0 )){
				extRefObject = new ExtRefObject();
				extRefObject.setData(vinNum.getValue(0).getString());
				if (vinKey != null && vinKey.getValue(0).getString().length() > 0) {
					extRefObject.setTechKey(new TechKey(vinKey.getValue(0).getString()));		
				}
				header.getAssignedExtRefObjects().addExtRefObject(extRefObject);
		}
		
        // the customerNumber field is only available, if the SoldTo is selectable
        // was the soldto changed by entering an new id in the customerNumber Field ?
        String newCustomerNumber = this.checkSoldToChange(parser);
        if (newCustomerNumber != null) {
//           if (newCustomerNumber.length() > 0) {
//              // Existenz of partner is checked in the Sales Document
//              if (bpList.getSoldTo() != null) {
//                  bpList.getSoldTo().setPartnerId(newCustomerNumber);
//                  bpList.getSoldTo().setPartnerTechKey(null);
//              }
//              else {
//                  bpList.setSoldTo(new PartnerListEntry(new TechKey(null), newCustomerNumber));
//              }
//           }
//           else { // no soldTo specified, delete it in the partnerList if its still present
//              bpList.removePartner(PartnerFunctionData.SOLDTO);
//           }
           addCustomerToPartnerList(newCustomerNumber, bpList);	
        } else {
        	String oldCustomerNumber = this.getOldCustomerNumber(parser);
        	if (oldCustomerNumber != null) {
        		addCustomerToPartnerList(oldCustomerNumber, bpList);		
        	}
        }
        
        
        if (headCampId != null && headCampId.getNumValues() > 0) {
        
            boolean campValid;
            
			for (int i = 0; i < headCampId.getNumValues(); i++) {
                if (headCampId.getValue(i).getString().trim().length() > 0) {
                    
                    campValid = invHeadCampId.getValue(i).getString().equals("");
                    header.getAssignedCampaigns().addCampaign(headCampId.getValue(i).getString(), TechKey.EMPTY_KEY, "", campValid, true);
                }
			}
        }
        
        if (incoterms1.getValue() != null && incoterms1.getValue().getString().length() > 0) {
            header.setIncoTerms1(incoterms1.getValue().getString());
        }
        else { // the incoterms1 were deleted by the user
        	header.setIncoTerms1(""); 
        }
        
        if (incoterms2.getValue() != null && incoterms2.getValue().getString().length() > 0) {
            header.setIncoTerms2(incoterms2.getValue().getString());
        }
        else { // the incoterms2 were deleted by the user
        	header.setIncoTerms2("");
        }
        
        if (headRecallId.getValue().getString().length() > 0) {
            header.setRecallId(headRecallId.getValue().getString());
        }
        
        if (deliveryPriority.getValue().getString().length() > 0) {
        	header.setDeliveryPriority(deliveryPriority.getValue().getString());
        }
        
        // get payment related data from request
		PaymentActions.parseRequestPayment(parser,
				   					       userSessionData,
										   salesDoc,
										   log,
										   isDebugEnabled); 
										           
        customerExitParseRequestHeader(parser, userSessionData, header, salesDoc);

        if (isDebugEnabled) {
            log.debug("Parsed header - " + header);
        }
    }
    
    /**
     * Helper method to parse the request and construct items
     * for the <code>SalesDocument</code> form the
     * request's data.
     * This method can be used for order.jsp and orde_change.jsp.
     * If the parameter isOrderChange is set to true, 
     * <p>
     * The following fields are taken from the request and used to
     * construct the item:
     * </p>
     * <table border="1" cellspacing="0" cellpadding="2">
     *   <tr>
     *     <td><b>field</b></td>
     *     <td><b>use</b></td>
     *   </tr>
     *   <tr>
     *     <td>itemTechKey[]</td>
     *     <td>technical key of the item, only relevant for items already added to the document</td>
     *   </tr>
     *   <tr>
     *     <td>product[]</td>
     *     <td>product number</td>
     *   </tr>
     *   <tr>
     *     <td>quantity[]</td>
     *     <td>quantity of the product</td>
     *   </tr>
     *   <tr>
     *     <td>unit[]</td>
     *     <td>the unit of the product</td>
     *   </tr>
     *   <tr>
     *     <td>units[]</td>
     *     <td>the unit of the product</td>
     *   </tr>
     *   <tr>
     *     <td>delete[]</td>
     *     <td>items to be deleted from the order</td>
     *   </tr>
     *   <tr>
     *     <td>cancel[]</td>
     *     <td>items to be cancelled from the order</td>
     *   </tr>
     *   <tr>
     *     <td>comment[]</td>
     *     <td>addtitional comments for the item entered by the customer</td>
     *   </tr>
     *   <tr>
     *     <td>reqdeliverydate[]</td>
     *     <td>requested delivery date for the item</td>
     *   </tr>
     *   <tr>
     *     <td>partnerproduct[]</td>
     *     <td>customer's own product number</td>
     *   </tr>
     *   <tr>
     *     <td>quantity[]</td>
     *     <td>quantity of the product</td>
     *   </tr>
     *   <tr>
     *     <td>oldquantity[]</td>
     *     <td>old quantity of the product</td>
     *   </tr>
     *   <tr>
     *     <td>pcat[]</td>
     *     <td>catalog the item was taken from</td>
     *   </tr>
     *   <tr>
     *     <td>pcatVariant[]</td>
     *     <td>catalog variant the item was taken from</td>
     *   </tr>
     *   <tr>
     *     <td>pcatArea[]</td>
     *     <td>catalog area the item was taken from</td>
     *   </tr>
     *   <tr>
     *     <td>customer[]</td>
     *     <td>customer to deliver this special item to</td>
     *   </tr>
     * </table>
     * <br><br>
     * @param parser The request, wrapped into a parser object
     * @param userSessionData   object wrapping the session
     * @param itemList the itemList for fill the newly created items into
     * @param salesDoc the current SalesDocument
     * @param isOrderChange flag to indicate, if true the items for a document in orderchange must be
     *        parsed and created or for a newly created document if false
     * @param log reference to the logging context
     * @param isDebugEnabled flag indicating wheter or not logging output
     *        is written
     */
    public void parseItems (RequestParser parser,
                            UserSessionData userSessionData,
                            ItemList itemList,
                            SalesDocument salesDoc,
                            boolean isOrderChange,
                            IsaLocation log,
                            boolean isDebugEnabled) {

        // get the item fields from the request
        RequestParser.Parameter itemTechKey =
                parser.getParameter("itemTechKey[]");

        RequestParser.Parameter product =
                parser.getParameter("product[]");
                
		RequestParser.Parameter productKey =
				parser.getParameter("productkey[]");                               

        RequestParser.Parameter quantity =
                parser.getParameter("quantity[]");

        RequestParser.Parameter unit = null;
        if (isOrderChange) {
            unit = parser.getParameter("unit[]");
        }
        else {
            unit = parser.getParameter("units[]");
        }

        RequestParser.Parameter comment =
                parser.getParameter("comment[]");

        RequestParser.Parameter reqDeliveryDate =
                parser.getParameter("reqdeliverydate[]");

        RequestParser.Parameter shipTo =
                parser.getParameter("customer[]");

        RequestParser.Parameter parentId =
                parser.getParameter("parentid[]");
                
        RequestParser.Parameter pcat =
                parser.getParameter("pcat[]");
        
        RequestParser.Parameter batchID =
                parser.getParameter("batchID[]");
        
        RequestParser.Parameter batchDedicated =
                parser.getParameter("batchDedicated[]");
                
        RequestParser.Parameter batchClassAssigned =
                parser.getParameter("batchClassAssigned[]");
                
        RequestParser.Parameter elementCount =
                parser.getParameter("elementCount[]");
                
        RequestParser.Parameter elementName =
                parser.getParameter("elementName[]");
        
        RequestParser.Parameter elementTxt =
                parser.getParameter("elementTxt[]");
                                
        RequestParser.Parameter elementCountCBs =
                parser.getParameter("elementCountCBs[]");
                
        RequestParser.Parameter elementCharVal =
                parser.getParameter("elementCharVal[]");
                
        RequestParser.Parameter elementCharValTxt =
                parser.getParameter("elementCharValTxt[]");
                
        RequestParser.Parameter elementCharValMax =
                parser.getParameter("elementCharValMax[]");
        
        RequestParser.Parameter setExt =
                parser.getParameter("setExt[]");
        
        // fields only used in order.jsp
        RequestParser.Parameter partnerproduct =
                parser.getParameter("partnerproduct[]");

        RequestParser.Parameter oldquantity =
                parser.getParameter("oldquantity[]");

        RequestParser.Parameter pcatVariant =
                parser.getParameter("pcatVariant[]");

        RequestParser.Parameter pcatArea =
                parser.getParameter("pcatArea[]");
        
        // fields only used in order_change.jsp
        RequestParser.Parameter itemExtStatus =
                parser.getParameter("itemextstatus[]");
                
        RequestParser.Parameter deliveryPriority =
        		parser.getParameter("deliveryPriority[]");
        
		RequestParser.Parameter latestDlvDate =
				parser.getParameter("latestdlvdate[]");
				
		RequestParser.Parameter contract =
						parser.getParameter("detailContractKey[]");	
						
		RequestParser.Parameter contractItem =
										parser.getParameter("detailContractItemKey[]");	
		
		RequestParser.Parameter configurable =
										parser.getParameter("configurable[]");

			
		//If the Latest Delivery Date(cancel date) is visible, then
		//only allow it to be changed.
		boolean isLatestDlvDateChangeable = false;
		if (latestDlvDate.getNumValues() > 0){
			isLatestDlvDateChangeable = true;		
		}
		
                      
        // clear the items of the document
        itemList.clear();
        
        if(isDebugEnabled) {
            log.debug("Found " + product.getNumValues() + " items in request");
        }

        int[] batchCounters = {0, 0};
        
		ExtRefObjectList extRefObjectsItem;
		//String RC_REF_OBJECTS_ITEM;
        
        for(int i = 1; i <= product.getNumValues(); i++) {
            
            if(product.getValue(i).getString().length() > 0) {
                
                ItemSalesDoc item = salesDoc.createItemSalesDoc();
                
                item.setTechKey(new TechKey(itemTechKey.getValue(i).getString()));
                
                // Set the values into the item
                // common fields
                item.setProduct(product.getValue(i).getString().trim());
                item.setProductId(new TechKey(productKey.getValue(i).getString()));
                item.setQuantity(quantity.getValue(i).getString().toUpperCase());
                item.setUnit(unit.getValue(i).getString().toUpperCase());
                item.setReqDeliveryDate(reqDeliveryDate.getValue(i).getString());
				item.setLatestDlvDate(latestDlvDate.getValue(i).getString());
                item.setPcat(new TechKey(pcat.getValue(i).getString()));
                item.setText(new Text("1000", comment.isSet() ? comment.getValue(i).getString() : ""));
                item.setDataSetExternally(setExt.getValue(i).getBoolean());
                item.setContractKey(new TechKey(contract.getValue(i).getString()));
                item.setContractItemKey(new TechKey(contractItem.getValue(i).getString()));
				item.setConfigurable(configurable.getValue(i).getBoolean());
                ShipTo selectedShipTo = salesDoc.getShipTo(new TechKey(shipTo.getValue(i).getString()));
                item.setShipTo((selectedShipTo == null) ? ShipTo.SHIPTO_INITIAL : selectedShipTo);
                item.setParentId(new TechKey(parentId.getValue(i).getString().equals("") ? null : parentId.getValue(i).getString()));
                
                batchCounters = setItemBatchFields(item, i, batchCounters[0], batchCounters[1], 
                                   elementCount, batchID, batchDedicated, batchClassAssigned, 
                                   elementName, elementTxt, elementCountCBs, elementCharVal, 
                                   elementCharValTxt, elementCharValMax);
                
				// campaign data
				RequestParser.Parameter campId =
						parser.getParameter("campId[" + i + "][]"); 
                        
                RequestParser.Parameter delCampId =
                                        parser.getParameter("delCampId[" + i + "][]");  
                
				for (int j = 0; j < campId.getNumValues(); j++) {
				    if (campId.getValue(j).getString() != null &&
                        campId.getValue(j).getString().trim().length() > 0 && 
                        (delCampId.getValue(j) == null || !delCampId.getValue(j).isSet())) {
						item.getAssignedCampaigns().addCampaign(campId.getValue(j).getString(), TechKey.EMPTY_KEY, "", false, true);
					} 
                }
             
                if(isOrderChange) {
                    // Fields only used in order_change.jsp
                    if(itemExtStatus != null) {
                        // Fill up status object (itemExtStatus contains Status/BusinessProcess)
                        int sBP = itemExtStatus.getValue(i).getString().indexOf("/");
                        // If new status = current status JSP will send blank value (sBP == -1)
                        if(sBP != -1) {
                            ExtendedStatus eStatus = new ExtendedStatus();
                            ExtendedStatusListEntry eStatusListEntry =
                            (ExtendedStatusListEntry)eStatus.createExtendedStatusListEntry(
                                                                                          new TechKey(itemExtStatus.getValue(i).getString().substring(0, sBP)),
                                                                                          null, null, null,
                                                                                          itemExtStatus.getValue(i).getString().substring((sBP + 1),
                                                                                                                                          itemExtStatus.getValue(i).getString().length()));
                            eStatus.setSelectedStatus(eStatusListEntry);
                            item.setExtendedStatus(eStatus);

                        }
                    }
                }
                else {
                    // Fields only used in order.jsp
                    item.setChangeable(true);
                    item.setOldQuantity(oldquantity.getValue(i).getString());
                    item.setPartnerProduct(partnerproduct.getValue(i).getString());
                    item.setPcatVariant(pcatVariant.getValue(i).getString());
                    item.setPcatArea(pcatArea.getValue(i).getString());
                    
                    //if latest dlv date is editable then set the true flag
                    if (isLatestDlvDateChangeable) item.setLatestDlvDateChangeable(true);
                }
                
                if (deliveryPriority.getValue(i).getString().length() > 0) {
                	item.setDeliveryPriority(deliveryPriority.getValue(i).getString());
                }
                
                // list of external reference numbers
				RequestParser.Parameter extReferenceTypes =
						parser.getParameter("itemExtReferenceType["+i+"][]");	        
				RequestParser.Parameter extReferenceNumbers =
						parser.getParameter("itemExtReferenceNumber["+i+"][]");
				RequestParser.Parameter extReferenceTechKeys =
						parser.getParameter("itemExtReferenceTechKey["+i+"][])");						
                if ( extReferenceTypes.getNumValues() > 0) {
					ExternalReferenceList extReferenceList = new ExternalReferenceList();                	
                	for (int j = 0; j < extReferenceTypes.getNumValues(); j++) {
						ExternalReference extReference = new ExternalReference();
						extReference.setRefType(extReferenceTypes.getValue(j).getString());
						extReference.setData(extReferenceNumbers.getValue(j).getString());
						extReference.setTechKey( new TechKey(extReferenceTechKeys.getValue(j).getString()));
						extReferenceList.addExtRef(extReference);
					}
					item.setAssignedExternalReferences(extReferenceList);               		 
                }
                
                // list of external reference objects               
				String addVinNum = "addVinNum_".concat(String.valueOf(i));
				String vinNum0 = "vinNum_".concat(String.valueOf(i));
				
				RequestParser.Parameter vinNumber =
						parser.getParameter(addVinNum);
					
				RequestParser.Parameter vinNum =
						parser.getParameter(vinNum0);
						
				RequestParser.Parameter vinKey =
						parser.getParameter("itemVinKeys["+i+"][])");	
												
				extRefObjectsItem = item.getAssignedExtRefObjects();
				extRefObjectsItem.clear();			
					
				if (vinNumber != null && vinNumber.getValue().getString().length() > 0) {
					StringTokenizer st = new StringTokenizer(vinNumber.getValue().getString(), ";", false);
					int j = 0;
					while (st.hasMoreTokens()) {
						String val = st.nextToken();
						ExtRefObject extRefObject = new ExtRefObject();
						if (vinKey != null && vinKey.getValue(j).getString().length() > 0) {
							extRefObject.setTechKey(new TechKey(vinKey.getValue(j).getString()));		
						}						
						if (j != 0) {
							extRefObject.setData(val);
						} else {
							String val_0 = vinNum.getValue().getString();
							if (val.equals(val_0)) {
								extRefObject.setData(val);
							} else {
								extRefObject.setData(val_0);
							}
						}
						if (extRefObject.getData().trim().length() > 0 || !extRefObject.getTechKey().isInitial()) {
							extRefObjectsItem.addExtRefObject(extRefObject);
						}						
						j++;
					}
				}
						
				if ((vinNumber != null && vinNumber.getValue().getString().length() == 0) &&
					  (vinNum != null && vinNum.getValue().getString().length() > 0 )){
						ExtRefObject extRefObject = new ExtRefObject();
						if (vinKey != null && vinKey.getValue(0).getString().length() > 0) {
							extRefObject.setTechKey(new TechKey(vinKey.getValue(0).getString()));		
						}
						extRefObject.setData(vinNum.getValue().getString());						  	
						extRefObjectsItem.addExtRefObject(extRefObject);
				}                
                      
                customerExitParseRequestItem(parser, userSessionData, item, i, salesDoc);
                
                itemList.add(item);
                
                if (isDebugEnabled) {
                    log.debug("Added item to document - " + item);
                }
            }
        }        
    }
	/**
	 * Template method to be overwritten by the subclasses of this class.
	 * This method easily enables to set any variable for the life of
	 * the transaction.
	 * By default this method is empty.
	 * 
	 * @param request           object holding the parameter & attributes
	 * @param userSessionData   object wrapping the session
	 */
	public void customerExitParsePresetGridCheckBoxValues(
			   HttpServletRequest request,
			   UserSessionData userSessionData) {
	 }
	/**
	 * Sets the checkBoxes for Stock Indicator & Price, which influences the
	 * grid entry screen view in IPC for a grid product.
	 *
	 * @param request           object holding the parameter & attributes
	 * @param userSessionData   object wrapping the session
	 */
	public void parsePresetGridCheckBoxValues(HttpServletRequest request,
										UserSessionData userSessionData){
											
		String gridStockFlag = request.getParameter("gridstock");
		String gridPriceFlag = request.getParameter("gridprice");
										
		if (gridStockFlag != null){
			userSessionData.setAttribute("gridprice",gridPriceFlag);
		}
			
		if (gridStockFlag != null){
			userSessionData.setAttribute("gridstock",gridStockFlag);
		}
		
		customerExitParsePresetGridCheckBoxValues(request, userSessionData);
	}
	
	
	
	
	public void enhancePartnerList(UserSessionData userSessionData,
                                   HeaderSalesDocument header) {
		
		
		BusinessObjectManager bom = (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
		BusinessPartnerManager bupama = bom.createBUPAManager();
		
		BusinessPartner buPa = null;		
		PartnerList pList = header.getPartnerList();
		 		
		ArrayList relevantPartnerFunctions = SalesDocumentParser.getRelevantPartnerFunctions(userSessionData);
		for (int i=0; i<relevantPartnerFunctions.size(); i++) {
			String partnerFunction = (String) relevantPartnerFunctions.get(i);
			if (partnerFunction != null) {		
				buPa = bupama.getDefaultBusinessPartner(partnerFunction);
				if (buPa != null ) {
				  pList.setPartner(partnerFunction, new PartnerListEntry(buPa.getTechKey(),buPa.getId()));
				  buPa = null;
				}
			}	
		}
	}
	
	
	
	public static ArrayList getRelevantPartnerFunctions(UserSessionData userSessionData) {
		
		ArrayList partnerFunctions = new ArrayList();

		BusinessObjectManager bom = (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);		
		Shop shop = bom.getShop();
		
		// add main partner function Soldto to list
		partnerFunctions.add(PartnerFunctionData.SOLDTO);
	      
		// if available in shop add Reseller or Agent partner function 
	    String compPartnerFunction = shop.getCompanyPartnerFunction();		
		if (compPartnerFunction.equals(PartnerFunctionData.RESELLER)||
			compPartnerFunction.equals(PartnerFunctionData.AGENT)) {
			partnerFunctions.add(compPartnerFunction);
		}

		// add contact partner function but only, if we are not in the reseller or agent scenario
		if ((!compPartnerFunction.equals(PartnerFunctionData.RESELLER)) &&
			(!compPartnerFunction.equals(PartnerFunctionData.AGENT))) {
			partnerFunctions.add(PartnerFunctionData.CONTACT);
		}

        
		// add responsible at partner function in reseller scenario
		if (compPartnerFunction.equals(PartnerFunctionData.RESELLER)) {        			
			partnerFunctions.add(PartnerFunctionData.RESP_AT_PARTNER);			
		}
				
		// add sold from partner function if basket is partner basket
		if (shop.isPartnerBasket()) {
			partnerFunctions.add(PartnerFunctionData.SOLDFROM);
		}

		// add end customer partner function
		partnerFunctions.add(PartnerFunctionData.END_CUSTOMER);

			
		return partnerFunctions;
		
	}
	
    public String getOldCustomerNumber(RequestParser parser) {
        String oldCustomerNumber = null;

        RequestParser.Parameter customerNumberOld =
                parser.getParameter("customerNumberOld");

        // the customerNumber field is only available, if the SoldTo is selectable
        // was the soldto changed by entering an new id in the customerNumber Field ?
        if (customerNumberOld.isSet()) {
            oldCustomerNumber = customerNumberOld.getValue().getString();
        }

        return oldCustomerNumber;
    }
    
    
    public void addCustomerToPartnerList(String customerNumber, PartnerList partnerList) {
    	
        if (customerNumber != null) {
            if (customerNumber.length() > 0) {
               // Existenz of partner is checked in the Sales Document
               if (partnerList.getSoldTo() != null) {
                   partnerList.getSoldTo().setPartnerId(customerNumber);
                   partnerList.getSoldTo().setPartnerTechKey(null);
               }
               else {
                   partnerList.setSoldTo(new PartnerListEntry(new TechKey(null), customerNumber));
               }
            }
            else { // no soldTo specified, delete it in the partnerList if its still present
               partnerList.removePartner(PartnerFunctionData.SOLDTO);
            }
        }    
    }
}
