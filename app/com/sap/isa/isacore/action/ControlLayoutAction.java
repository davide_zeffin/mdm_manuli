/*****************************************************************************
    Class:        ControlLayoutAction
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      05.11.2004
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.isacore.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.ui.layout.UILayout;
import com.sap.isa.core.ui.layout.UILayoutManager;
import com.sap.isa.core.util.RequestParser;


/**
 * The class ControlLayoutAction allows to change the layout dynamically. <br>
 * The action should be the last action which is called with the
 * UILayout action attribute. See {@link com.sap.isa.core.ui.layout.UILayout}
 * for details.
 *
 * @author  SAP AG
 * @version 1.0
 */
abstract public class ControlLayoutAction extends EComBaseAction {

	/**
	 * Return Set the property {@link #}. <br>
	 * 
     * @param mapping            The ActionMapping used to select this instance
     * @param form               The <code>FormBean</code> specified in the
     *                              config.xml file for this action
     * @param request            The request object
     * @param response           The response object
     * @param userSessionData    Object wrapping the session
     * @param requestParser      Parser to simple retrieve data from the request
	 * @param layout             Current layout stored in the request.
	 * @throws IOException
	 * @throws ServletException
	 * @throws CommunicationException
	 */
	abstract public void controlLayout(ActionMapping mapping,
		                                 ActionForm form,
		                                 HttpServletRequest request,
		                                 HttpServletResponse response,
		                                 UserSessionData userSessionData,
					  		             MetaBusinessObjectManager mbom,
							             UILayout layout)
		throws IOException, ServletException, CommunicationException;
	


    /**
     * Overwrites the method ecomPerform. <br>
     * The method get the current layout object and calls then the 
     * {@link #controlLayout} method.
     * 
     * 
     * @param mapping            The ActionMapping used to select this instance
     * @param form               The <code>FormBean</code> specified in the
     *                              config.xml file for this action
     * @param request            The request object
     * @param response           The response object
     * @param userSessionData    Object wrapping the session
     * @param requestParser      Parser to simple retrieve data from the request
     * @param mbom               Reference to the MetaBusinessObjectManager
     * @param multipleInvocation Flag indicating, that a multiple invocation occured
     * @param browserBack        Flag indicating a browser back
     * @return Forward to another action or page
     * @throws IOException
     * @throws ServletException
     * @throws CommunicationException
     * 
     * 
     * @see com.sap.isa.isacore.action.EComBaseAction#ecomPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.sap.isa.core.UserSessionData, com.sap.isa.core.util.RequestParser, com.sap.isa.core.businessobject.management.MetaBusinessObjectManager, boolean, boolean)
     */
    public ActionForward ecomPerform(ActionMapping mapping,
                                      ActionForm form,
                                      HttpServletRequest request,
                                      HttpServletResponse response,
                                      UserSessionData userSessionData,
                                      RequestParser requestParser,
                                      MetaBusinessObjectManager mbom,
                                      boolean multipleInvocation,
                                      boolean browserBack)
        	throws IOException, ServletException, CommunicationException {
        		
		final String METHOD_NAME = "ecomPerform()";
		log.entering(METHOD_NAME);
		UILayoutManager uiManager = UILayoutManager.getManagerFromRequest(request);
		
		if (uiManager.getCurrentUILayout() != null) {
			controlLayout(mapping,
						  form,
						  request,
						  response,
						  userSessionData,
						  mbom,
						  uiManager.getCurrentUILayout());
		}
        		
		ActionForward retForward;
		
		retForward = new ActionForward("UIContinue");
		retForward.setRedirect(false);
		retForward.setName("success");
        log.exiting();	
        return retForward;
    }

}
