/*****************************************************************************
    Class         PrepareIsaPriceAnalysisAction
    Copyright (c) 2000, SAP AG, Germany, All rights reserved.
    Description:  Action to prepare the display of the price analysis in ISA
    Author:       SAP
    Created:      October 2005
    Version:      1.0

    $Revision: #1 $
    $Date: 2005/10/05 $
*****************************************************************************/
package com.sap.isa.isacore.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.businessobject.ipc.IPCBOManager;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.isa.ipc.ui.jsp.action.RequestParameterConstants;
import com.sap.spc.remote.client.object.imp.IPCClientObjectFactory;
import com.sap.spc.remote.client.object.imp.rfc.RFCIPCItemReference;

/**
 * Class to initialize the price analysis in the E-selling application.
 * The <code>isaPerform</code> method uses the 
 *   - connectionKey : key of the IPC connection,
 *   - IPC-documentId : ID of the IPC pricing document,
 *   - IPC-itemId : ID of the IPC prcing item
 * to instanciate a new IPCItemReference object.   
 *
 * @author SAP
 * @version 1.0
 */
public class PrepareIsaPriceAnalysisAction extends IsaCoreBaseAction {

	/**
	 * In case of B2C allow usage of action without being logged in. 
	 */
	protected void initialize() {
		if (isB2C()) {
			checkUserIsLoggedIn = false;
		}
	}

	/**
	 * Implement this method to add functionality to your action.
	 *
	 * @param mapping           The <code>ActionMapping</code> passed by struts
	 * @param form              The <code>FormBean</code> specified in the
	 *                          config.xml file for this action
	 * @param request           The request object
	 * @param response          The response object
	 * @param userSessionData   Object wrapping the session
	 * @param requestParser     Parser to simple retrieve data from the request
	 * @param bom               Reference to the BusinessObjectManager
	 * @param log               Reference to the IsaLocation, needed for logging
	 * @return Forward to another action or page
	 */
	
	public ActionForward isaPerform(ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response,
			UserSessionData userSessionData,
			RequestParser requestParser,
			BusinessObjectManager bom,
			IsaLocation log) throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
		// Page that should be displayed next.
		String forwardTo = "success";
		String itemID = "";
		String docID = "";
		String connectionKey = "";
		String mode = "";

		RFCIPCItemReference ipcItemReference = (RFCIPCItemReference)IPCClientObjectFactory.getInstance().newIPCItemReference();
		IPCBOManager ibom = getIPCBusinessObjectManager(userSessionData);

		// get the IPC item ID from the request
		if (requestParser.getAttribute("IPCitemID").isSet()) {
			itemID = requestParser.getAttribute("IPCitemID").getValue().getString();
		}
		else {
			itemID = requestParser.getParameter("IPCitemID").getValue().getString();
		}
		
		// get the IPC document ID from the request
		if (requestParser.getAttribute("IPCdocID").isSet()) {
			docID = requestParser.getAttribute("IPCdocID").getValue().getString();
		}
		else {
			docID = requestParser.getParameter("IPCdocID").getValue().getString();
		}

		// get the connection key from the request
		if (requestParser.getAttribute("conKey").isSet()) {
			connectionKey = requestParser.getAttribute("conKey").getValue().getString();
		}
		else {
			connectionKey = requestParser.getParameter("conKey").getValue().getString();
		}

		// get the display mode from the request
		if (requestParser.getAttribute("mode").isSet()) {
			mode = requestParser.getAttribute("mode").getValue().getString();
		}
		else {
			mode = requestParser.getParameter("mode").getValue().getString();
		}

		// syncronization with IPC server required!
		IPCClient ipcClient= ibom.getIPCClient();
        if (ipcClient == null) {
            ipcClient = ibom.createIPCClientUsingConnection(connectionKey);
        }
		ipcClient.getIPCSession().setCacheDirty();
		ipcClient.getIPCSession().syncWithServer();
		
		ipcItemReference.setDocumentId(docID);
		ipcItemReference.setItemId(itemID);
		ipcItemReference.setConnectionKey(connectionKey);

		// update the request: add the ipcItemReference
		request.setAttribute(RequestParameterConstants.IPC_ITEM_REFERENCE, ipcItemReference);
		request.setAttribute("MODE", mode);

		log.exiting();
		return mapping.findForward( forwardTo );

	}

}
