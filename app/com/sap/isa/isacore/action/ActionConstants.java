/*****************************************************************************
    Class:        ActionConstants
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Created:      May 2001
    Version:      1.0

    $Revision: #2 $
    $Date: 2001/07/03 $
*****************************************************************************/

package com.sap.isa.isacore.action;

/**
 * this class defines some constants for request attributes (RA) which are used
 * in actions to store and retrieve data from request context.
 */
public class ActionConstants extends com.sap.isa.catalog.actions.ActionConstants {

  /**
   * The current item in the request. The value is <b>currentItem</b>.
   */

  public final static String RC_WEBCATITEM           = "currentItem";

  /**
   * The address formular in the request. The value is <b>addressFormular</b>.
   */
  public final static String RC_ADDRESS_FORMULAR     = "addressFormular";

  /**
   * The address in the request. The value is <b>address</b>.
   */
  public final static String RC_ADDRESS              = "address";

  /**
   * A request constant for the ship to selection. The value is <b>shipToSelection</b>.
   */
  public final static String RC_SHIPTOSELECTION      = "shipToSelection";

  /**
   * Name of the countryList attribute stored in the request context. The value is <b>countryList</b>.
   */
  public static final String RC_COUNTRY_LIST = "countryList";

  /**
   * Name of the regionList attribute stored in the request context. The value is <b>regionList</b>.
   */
  public static final String RC_REGION_LIST          = "regionList";

  /**
   * Name of the regionList attribute stored in the request context. The value is <b>taxCodeList</b>.
   */
  public static final String RC_TAXCODE_LIST         = "taxCodeList";

  /**
   * Name of the titleList attribute stored in the request context. The value is <b>titleList</b>.
   */
  public static final String RC_TITLE_LIST           = "titleList";
  
  /**
   * Name of the forms of address attribute stored in the request context. The value is <b>formsofaddress</b>.
   */
  public static final String RC_FORMS_OF_ADDRESS     = "formsofaddress";

  /**
   * Name of the forms of address attribute stored in the request context. The value is <b>possiblecounties</b>.
   */
  public static final String RC_POSSIBLE_COUNTIES    = "possiblecounties";

  // public final static String DS_RECOMMENDATION_AND_BESTSELLER = "recommendationAndBestseller";

  /** 
   * Confid in XCM for UME configuration. The value is <b>user</b>.
   */
  public static final String UME_INTERACTION_CONFID    = "user";
  /** Confvalue in XCM for UME configuration. The value is <b>UseUME</b>. */
  public static final String UME_INTERACTION_CONFVALUE = "UseUME";
  /** Logon schema for ISA layout in the UME application. The value is <b>isab2b</b>. */
  public static final String UME_LOGON_SCHEMA_B2B      = "isab2b";
  /**
   * Key to store and retrieve the the current application area. The value is <b>b2b.appl.area.sap.com</b>.
   */
  public static final String SC_APPLAREA = "b2b.appl.area.sap.com";
  /**
   * Defines that the current application area is the Internet Sales <b>shop</b>.<br />
   * (NOT shopadmin !)<br />
   * The value is <b>b2b.appl.area.sap.com.shop</b>.
   */
  public static final String APPLAREA_SHOP = "b2b.appl.area.sap.com.shop";
  /**
   * Defines that the current application area is the Internet Sales <b>separate billing screen</b>.<br />
   * The value is <b>b2b.appl.area.sap.com.sbill</b>.
   */
  public static final String APPLAREA_SEPBILL = "b2b.appl.area.sap.com.sbill";
  
  /**
   * Id of the config which describes the configuration stuff for the ui,
   * including the parameters for max items for a list view below.
   */
  public static final String IN_UI_CONFIG_ID = "ui";
  
  /**
   * UI constant for steering price display in the configuration
   */
  public static final String SHOW_CONFIG_PRICES = "configuration.showPrices";
  
  /**
   * UI constant for steering displaying varaints in the configuration UI
   */
  public static final String SHOW_CONFIG_VARIANTS = "configuration.showVariants";

}