/*****************************************************************************
    Class:        QuickSearchAddToBasketAction
    Copyright (c) 2003, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      July 2003

    $Revision: #1 $
    $Date: 2003/07/01 $
*****************************************************************************/
package com.sap.isa.isacore.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businessobject.item.BasketTransferItemImpl;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.order.AddToBasketAction;

/**
 * Retrievs items from HTTP request and transfers those to the basket
 */
public class QuickSearchAddToBasketAction extends IsaCoreBaseAction  {
	
    /**
     * request parameter, to transfer product guid of the product, that
     * should be added to the document
     */
    public static final String RC_PRODUCT_GUID   = "QS_prodguid";

    /**
     * request parameter, to transfer UOM of the product, that
     * should be added to the document in case of ISA-R3
     */
    public static final String RC_PRODUCT_UNIT   = "QS_produnit";

    
    /**
     * constant to specify the source of a transfer item
     */
    protected static final String TRANSFER_ITEM_SOURCE = "catalog";
    
    
    
    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @param startupParameter  Object containing the startup parameters
     * @param eventHandler      Object to capture events with
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            BusinessObjectManager bom,
            IsaLocation log,
            IsaCoreInitAction.StartupParameter startupParameter,
            BusinessEventHandler eventHandler) {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        BasketTransferItemImpl transferItem = null;

        RequestParser.Parameter productGuid = requestParser.getParameter(RC_PRODUCT_GUID);
        
        //Abhishek: We also need to pass the Unit from Organizer-content-product-search1.jsp
        // in case of ISA-R3.
        RequestParser.Parameter unit = requestParser.getParameter(RC_PRODUCT_UNIT);

        if (productGuid.isSet() && productGuid.getValue().getString() != null &&
            productGuid.getValue().getString().length() > 0) {
            transferItem = new BasketTransferItemImpl();
            transferItem.setSource(TRANSFER_ITEM_SOURCE);
            transferItem.setProductKey(productGuid.getValue().getString());
            transferItem.setQuantity("1");
            transferItem.setUnit(unit.getValue().getString());           
            if(log.isDebugEnabled()) {
                log.debug("product GUID to be added:" + transferItem.getProductKey());
            }
        }
        else {
            if(log.isDebugEnabled()) {
                log.debug("ERROR: no productGuid specified");
            }
        }
        
		// if the user adds a product to the basket from the MyTransaction 'product quick search'
		// after he/she added a product to the basket from the catalog view previously
		// constant AddToBasketAction.SC_FORWARD_NAME needs to be adjusted, 
		// otherwise the catalog view is shown in the middle frame, instead of the basket
        
		if ("backToCatalog".equals(request.getSession().getAttribute(AddToBasketAction.SC_FORWARD_NAME))){
			request.getSession().removeAttribute(AddToBasketAction.SC_FORWARD_NAME);
		}
				 
        // prepare call of AddToBasketAction
        if(transferItem != null) {
            request.setAttribute(AddToBasketAction.RC_TRANSFER_ITEM,transferItem);
            log.exiting();
            return mapping.findForward("addtodocument");        
        }
        log.exiting();
        return mapping.findForward("error");

    }

}