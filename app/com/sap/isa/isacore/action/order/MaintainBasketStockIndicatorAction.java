/*****************************************************************************
	Class         MaintainBasketStockIndicatorAction
	Copyright (c) 2004, SAP Labs India, All rights reserved.
	Description:  Action to get Stock Indictors of grid product
	Author:
	Created:      27.09.2004
	Version:      1
*****************************************************************************/
package com.sap.isa.isacore.action.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.action.IsaCoreInitAction;

/**
 * Action to get the Stock Indicators from Backend and forwards to Grid screen.
 * <p>
 * The following forwards are used by this action:
 * </p>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>FORWARD_CONFIG_ITEM</td><td>always used</td></tr>
 * </table>
 *
 */
public class MaintainBasketStockIndicatorAction extends MaintainBasketBaseAction{

	protected String basketPerform(HttpServletRequest request,
					HttpServletResponse response,
					HttpSession session,
					UserSessionData userSessionData,
					RequestParser parser,
					BusinessObjectManager bom,
					IsaLocation log,
					IsaCoreInitAction.StartupParameter startupParameter,
					BusinessEventHandler eventHandler,
					boolean multipleInvocation,
					boolean browserBack,
					Shop shop,
					SalesDocument preOrderSalesDocument,
					boolean auction,
					DocumentState targetDocument,
					DocumentHandler documentHandler)
						throws CommunicationException {
							
		final String METHOD_NAME = "basketPerform()";
		log.entering(METHOD_NAME);
		
		TechKey techKey = null;
		
		if (parser.getAttribute("itemId").isSet()) {
					techKey = new TechKey(parser.getAttribute("itemId").getValue().getString());
				}
				else {
					techKey = new TechKey(parser.getParameter("itemId").getValue().getString());
				}
				userSessionData.setAttribute(ItemConfigurationBaseAction.SC_ITEM_IN_CONFIG, techKey.getIdAsString());
		
		if (userSessionData.getAttribute(ItemConfigurationBaseAction.SC_ITEM_IN_CONFIG) != null 
			&& userSessionData.getAttribute(ItemConfigurationBaseAction.SC_ITEM_IN_CONFIG).toString().length() > 0) {
			
			String techKeyStr = (String) userSessionData.getAttribute(ItemConfigurationBaseAction.SC_ITEM_IN_CONFIG);
			TechKey itemTechKey = new TechKey(techKeyStr) ;
			
			String[][] variantStockInd = preOrderSalesDocument.getGridStockIndicator(itemTechKey);
			
			//Get the two 1-d array and set it to userSessionData
			String[] variantArr  = variantStockInd[0];
			String[] stockIndArr = variantStockInd[1];
			
			userSessionData.setAttribute("varIds", variantArr);
			userSessionData.setAttribute("indicators", stockIndArr);
		}
		log.exiting();
		return FORWARD_CONFIG_ITEM;
	}
}