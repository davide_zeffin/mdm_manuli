package com.sap.isa.isacore.action.order.hom;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.order.CustomerOrder;
import com.sap.isa.businessobject.order.CustomerOrderStatus;
import com.sap.isa.businessobject.ordertemplate.OrderTemplate;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.action.order.MaintainOrderBaseAction;

public class HomChangeOrderAction extends IsaCoreBaseAction {

    // this is used form the updatedocumentview Action and teh Maintain shiptoAction,
    // to determine what to display next
    public static String ORDER_CHANGE = "order_change";

    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser parser,
            BusinessObjectManager bom,
            IsaLocation log) throws CommunicationException {

		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);  
        DocumentHandler documentHandler = (DocumentHandler)
                userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);

        Shop shp    = bom.getShop();

        if (shp == null) {
        	log.exiting();
           	throw new PanicException("Shop not found in BOM");
        }


        CustomerOrderStatus orderstatus = bom.getCustomerOrderStatus();

        if (orderstatus == null) {
           mapping.findForward("failure");
        }

        // Check, what documenttype is going to be changed
        HeaderSalesDocument headerSalesDoc = orderstatus.getOrderHeader();
        String doctype = orderstatus.getOrderHeader().getDocumentType();

        SalesDocument salesDoc = null;

        // read catalog
        WebCatInfo webCat =
                getCatalogBusinessObjectManager(userSessionData).getCatalog();

        if (doctype.equalsIgnoreCase(HeaderSalesDocument.DOCUMENT_TYPE_ORDER) ||
            doctype.equalsIgnoreCase(HeaderSalesDocument.DOCUMENT_TYPE_QUOTATION)) {
            // Ask the business object manager to get the order
            CustomerOrder order = bom.createCustomerOrder();

            if (order == null) {
            	log.exiting();
               throw new PanicException("Order could not be created in BOM");
            }

            order.setTechKey(orderstatus.getTechKey());
            order.setGData(shp, webCat);

            salesDoc = order;
        }
        else if (doctype.equalsIgnoreCase(HeaderSalesDocument.DOCUMENT_TYPE_ORDERTEMPLATE)) {
            // Ask the business object manager to get the order
            OrderTemplate orderTemplate = bom.createOrderTemplate();

            if (orderTemplate == null) {
            	log.exiting();
               throw new PanicException("OrderTemplate could not be created in BOM");
            }

            orderTemplate.setTechKey(orderstatus.getTechKey());
            orderTemplate.setGData(shp, webCat);

            salesDoc = orderTemplate;
        }
        else {
            mapping.findForward("failure");
        }

        salesDoc.setState(DocumentState.TARGET_DOCUMENT);

        ManagedDocument mDoc = null;

        mDoc = new ManagedDocument(salesDoc,
                                   doctype,
                                   headerSalesDoc.getSalesDocNumber(),
                                   headerSalesDoc.getPurchaseOrderExt(),
                                   headerSalesDoc.getDescription(),
                                   headerSalesDoc.getChangedAt(),
                                   "homchangeorder",
                                   null,
                                   null);

        documentHandler.add(mDoc);
        documentHandler.setOnTop(salesDoc);
        documentHandler.release(orderstatus);
        bom.releaseCustomerOrderStatus();

        userSessionData.setAttribute(
                MaintainOrderBaseAction.SC_SHIPTOS, salesDoc.getShipTos());
        log.exiting();
        return mapping.findForward("changeorder");
    }

}