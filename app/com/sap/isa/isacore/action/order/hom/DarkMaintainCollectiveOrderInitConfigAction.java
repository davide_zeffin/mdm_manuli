/*****************************************************************************
    Class         DarkMaintainCollectiveOrderInitConfigAction
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP
    Created:      16.08.2001
    Version:      1.0

    $Revision: #15 $
    $Date: 2001/08/01 $
*****************************************************************************/

package com.sap.isa.isacore.action.order.hom;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.order.CollectiveOrder;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.action.order.InitItemConfigurationBaseAction;

/**
 * Action for initializing the configuration.
 *
 * @author SAP
 * @version 1.0
 */
public class DarkMaintainCollectiveOrderInitConfigAction extends InitItemConfigurationBaseAction {

    /**
     * The method use the document on top taken from the document handler for the configuration. <br>
     * 
     * @see com.sap.isa.isacore.action.order.InitItemConfigurationBaseAction#getConfiguratorDoc(com.sap.isa.core.UserSessionData, com.sap.isa.businessobject.BusinessObjectManager)
     */
    public ManagedDocument getConfiguratorDoc(DocumentHandler documentHandler,
    										  UserSessionData userSessionData, 
    										  BusinessObjectManager bom) {

		final String METHOD_NAME = "getConfiguratorDoc()";
		log.entering(METHOD_NAME);
        CollectiveOrder collectiveOrder = bom.getCollectiveOrder();

        HeaderSalesDocument header = collectiveOrder.getHeader();
        
        collectiveOrder.setState(DocumentState.TARGET_DOCUMENT);
        
        ManagedDocument configuratorDoc = new ManagedDocument(collectiveOrder,
             header.getDocumentType(), header.getSalesDocNumber(), header.getPurchaseOrderExt(),
             header.getDescription(), header.getChangedAt(), "configuration",
             null, null);
		log.exiting();
        return configuratorDoc;
    }


}