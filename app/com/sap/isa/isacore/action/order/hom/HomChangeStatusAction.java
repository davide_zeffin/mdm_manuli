/*****************************************************************************
  Class:        MaintainOrderSendAction
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:
  Created:      18.09.2001
  Version:      1.0

  $Revision: #8 $
  $Date: 2002/08/21 $
*****************************************************************************/

package com.sap.isa.isacore.action.order.hom;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.businessobject.order.CustomerOrderChange;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 * Action to perform the final step in the processing of an document (i.e. to
 * save it in the backend).
 * <p>
 * The following forwards are used by this action:
 * </p>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>FORWARD_CHANGEORDER</td><td>something went wrong during the save process</td></tr>
 *   <tr><td>FORWARD_CONFIRMATION_SCREEN</td><td>show confirmation that save was successful</td></tr>
 * </table>
 *
 * @author
 * @version 1.0
 */
public class HomChangeStatusAction extends IsaCoreBaseAction {

    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            BusinessObjectManager bom,
            IsaLocation log) {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);

        DocumentHandler documentHandler = (DocumentHandler)
                userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);

        if (documentHandler == null) {
        	log.exiting();
            throw new PanicException("No document handler found");
        }

        DocumentState targetDocument =
            documentHandler.getDocument(ManagedDocument.TARGET_DOCUMENT);

        if (targetDocument == null) {
        	log.exiting();
            throw new PanicException("No target document returned from documentHandler");
        }

        CustomerOrderChange order;

        //order = new CustomerOrderChange(bom.getCustomerOrder());
                order = new CustomerOrderChange(bom.getCustomerOrder());

            //   userSessionData.setAttribute(SC_DOCTYPE, DOCTYPE_BASKET);
            if (order == null) {
            	log.exiting();
                throw new PanicException("No order returned from bom");
            }

        Shop shop      = bom.getShop();
        User user     = bom.getUser();

        if (shop == null) {
        	log.exiting();
            throw new PanicException("No shop returned from bom");
        }

        if (user == null) {
        	log.exiting();
            throw new PanicException("No user returned from bom");
        }

        /*String forwardTo = orderPerform(request, response, session, userSessionData,
                             parser, bom, log, startupParameter, eventHandler,
                             multipleInvocation, browserBack, user, shop,
                             order, targetDocument,
                             documentHandler);*/
		log.exiting();
        return mapping.findForward("homprocessorder");


        //return "homprocessorder";
    }
}