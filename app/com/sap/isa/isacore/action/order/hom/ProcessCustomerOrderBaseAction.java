package com.sap.isa.isacore.action.order.hom;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.ExtendedStatus;
import com.sap.isa.businessobject.ExtendedStatusListEntry;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.businessobject.order.OrderChange;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.order.MaintainOrderBaseAction;

/**
 * Base class for processing the customer order.<br>
 * At the moment only status changes are allowed on this object while processing.
 * Therefore the parse methods are overwriten in apropriate manner. 
 * 
 * @author SAP
 * @version 1.0
 *
 */
abstract public class ProcessCustomerOrderBaseAction extends MaintainOrderBaseAction {


	/**
	 * @see com.sap.isa.isacore.action.order.MaintainOrderBaseAction#parseRequestHeader(RequestParser, UserSessionData, OrderChange, IsaLocation, boolean)
	 */
	protected void parseRequestHeader(
		RequestParser parser,
		UserSessionData userSessionData,
		OrderChange order,
		IsaLocation log,
		boolean isDebugEnabled)
		  throws CommunicationException {

        customerExitParseRequestHeader(parser, userSessionData, order);

        if(isDebugEnabled) {
            log.debug("Parsed header - " + order.getHeader());
        }
            
	}

	/**
	 * @see com.sap.isa.isacore.action.order.MaintainOrderBaseAction#parseRequestItems(RequestParser, UserSessionData, OrderChange, String, IsaLocation, boolean)
	 */
	protected void parseRequestItems(
		RequestParser parser,
		UserSessionData userSessionData,
		OrderChange order,
		String headerReqDeliveryDate,
		IsaLocation log,
		boolean isDebugEnabled)
		  throws CommunicationException {
		  	
		final String METHOD_NAME = "parseRequestItems()";
		log.entering(METHOD_NAME);
	        // get the status information from the request

		        RequestParser.Parameter itemTechKey =
		        parser.getParameter("customerItemKey[]");
		
		        RequestParser.Parameter product =
		        parser.getParameter("customerProduct[]");
		
		        RequestParser.Parameter quantity =
		        parser.getParameter("customerQuantity[]");
		
		        RequestParser.Parameter unit =
		        parser.getParameter("customerUnit[]");
				
		        RequestParser.Parameter itemExtStatus =
		        parser.getParameter("itemextstatus[]");
		
				RequestParser.Parameter parentId =
				parser.getParameter("customerParentid[]");

		        // clear the items of the document
		        order.clearItems();
				
				//ItemList items = order.getItems();
		        if(isDebugEnabled) {
		            log.debug("Found " + product.getNumValues() + " items in request");
		        }

                for (int i = 1; i <= product.getNumValues(); i++) {
                    if (product.getValue(i).getString().length() > 0) {
                        //ItemSalesDoc itm = items.get(i);
                        ItemSalesDoc item = new ItemSalesDoc(new TechKey(itemTechKey.getValue(i).getString()));

                        item.setProduct(product.getValue(i).getString());
                        item.setQuantity(quantity.getValue(i).getString());
                        item.setUnit(unit.getValue(i).getString());

						if(parentId.getValue(i).getString().equals("")) {
							item.setParentId(new TechKey(null));
						}
						else {
							item.setParentId(new TechKey(parentId.getValue(i).getString()));
						}

						// sub items inherits the status from the parent item	
						if (!item.getParentId().isInitial()) {
			
							ItemSalesDoc parentItem = order.getItem(item.getParentId());
							if (parentItem != null && parentItem.getExtendedStatus() != null) {			
								ExtendedStatusListEntry xStatusEntry = (ExtendedStatusListEntry)parentItem.getExtendedStatus().getSelectedStatus();
							
								ExtendedStatus eStatus = new ExtendedStatus();
								ExtendedStatusListEntry eStatusListEntry =
										(ExtendedStatusListEntry)eStatus.createExtendedStatusListEntry(
										new TechKey(xStatusEntry.getTechKey().getIdAsString()),
										null, null, null,
										xStatusEntry.getBusinessProcess());
								eStatus.setSelectedStatus(eStatusListEntry);
								item.setExtendedStatus(eStatus);
							}	
						} 		
						// no else! Allows to overwrite the status with a status set on the JSP
						// at the moment no status is set on JSP for sub items!

                        // Fill up status object (itemExtStatus contains Status/BusinessProcess)
                        int sBP = itemExtStatus.getValue(i).getString().indexOf("/");
                        // If new status = current status JSP will send blank value (sBP == -1)
                        if (sBP != -1) {
                            ExtendedStatus eStatus = new ExtendedStatus();
                            ExtendedStatusListEntry eStatusListEntry =
                                (ExtendedStatusListEntry) eStatus.createExtendedStatusListEntry(
                                    new TechKey(itemExtStatus.getValue(i).getString().substring(0, sBP)),
                                    null, null, null,
                                    itemExtStatus.getValue(i).getString().substring((sBP + 1),itemExtStatus.getValue(i).getString().length()));
                            eStatus.setSelectedStatus(eStatusListEntry);
                            item.setExtendedStatus(eStatus);
                        }

                        customerExitParseRequestItem(parser, userSessionData, item, i);
                        order.addItem(item);
                    }
                }
                log.exiting();
                 
		  }
}
