/*****************************************************************************
  Class:        ProcessCustomerOrderSendAction
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:
  Created:      18.09.2001
  Version:      1.0

  $Revision: #8 $
  $Date: 2002/08/21 $
*****************************************************************************/

package com.sap.isa.isacore.action.order.hom;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.order.OrderChange;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.action.IsaCoreInitAction;

/**
 * Action to perform the final step in the processing of an document (i.e. to
 * save it in the backend). <br>
 * <i>Note: The order will be saved without a commit!</i>
 * <p>
 * The following forwards are used by this action:
 * </p>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>FORWARD_CHANGEORDER</td><td>something went wrong during the save process</td></tr>
 *   <tr><td>SUCCESS</td><td>display customer order in view mode</td></tr>
 * </table>
 *
 * @author
 * @version 1.0
 */
public class ProcessCustomerOrderSendAction extends ProcessCustomerOrderBaseAction {
	
    protected String orderPerform(HttpServletRequest request,
                HttpServletResponse response,
                HttpSession session,
                UserSessionData userSessionData,
                RequestParser parser,
                BusinessObjectManager bom,
                IsaLocation log,
                IsaCoreInitAction.StartupParameter startupParameter,
                BusinessEventHandler eventHandler,
                boolean multipleInvocation,
                boolean browserBack,
                Shop shop,
                OrderChange order,
                DocumentState targetDocument,
                DocumentHandler documentHandler)
                    throws CommunicationException {
		final String METHOD_NAME = "orderPerform()";
		log.entering(METHOD_NAME);
        String forwardTo = null;

        boolean isDebugEnabled = log.isDebugEnabled();

        updateOrder(parser, userSessionData, order, shop, bom.getBUPAManager(), log, request);
        
        //  order must be read first in order to check validity
        order.readForUpdate(
        );

        if (shop.isOnlyValidDocumentAllowed() && !order.isvalid()) {
            if (isDebugEnabled) {
                log.debug("document NOT valid");
                log.debug("message provided: " + order.getMessageList());
            }
            
            forwardTo = FORWARD_CHANGEORDER;
        }
        else {
           order.saveChanges();
           
       	 	SalesDocument salesDoc = (SalesDocument)targetDocument;
         	salesDoc.readHeader();
         	HeaderSalesDocument header = salesDoc.getHeader();
             
         	String docType = header.getDocumentType();
         	String techkey = salesDoc.getTechKey().getIdAsString();
                         
         	request.setAttribute("techkey", techkey);
         	request.setAttribute("object_id", docType);
         	forwardTo = "success";
        }
		log.exiting();
        return forwardTo;
    }
}