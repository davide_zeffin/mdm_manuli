/*****************************************************************************
  Class:        DarkMaintainCollectiveOrderBaseAction
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       SAP AG
  Created:      December 2002
  Version:      1.0

  $Revision: #6 $
  $Date: 2002/03/06 $
*****************************************************************************/

package com.sap.isa.isacore.action.order.hom;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.ConnectedDocumentItem;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.ExtendedStatusListEntry;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businessobject.ipc.IPCBOManager;
import com.sap.isa.businessobject.item.ItemList;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.businessobject.order.CollectiveOrder;
import com.sap.isa.businessobject.order.CollectiveOrderChange;
import com.sap.isa.businessobject.order.CustomerOrder;
import com.sap.isa.businessobject.order.CustomerOrderChange;
import com.sap.isa.businessobject.order.OrderChange;
import com.sap.isa.businessobject.ordertemplate.OrderTemplate;
import com.sap.isa.businessobject.ordertemplate.OrderTemplateChange;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.action.IsaCoreInitAction;
import com.sap.isa.isacore.action.order.MaintainOrderBaseAction;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.IPCItemReference;
import com.sap.spc.remote.client.object.imp.IPCClientObjectFactory;

/**
 * Base class to maintain a collective order additional to another document, like
 * the customer order in the partner order management.
 *
 * @author SAP
 * @version 1.0
 *
 */
abstract public class DarkMaintainCollectiveOrderBaseAction
    extends MaintainOrderBaseAction {
	
        /**
         * Template method to be overwritten by the subclasses of this class.
         * Normally you should overwrite this method and you do not touch the
         * <code>isaPreform</code> method.
         *
         * @param request           request context
         * @param response          response context
         * @param session           unwrapped session context
         * @param userSessionData   object wrapping the session
         * @param requestParser     parser to simple retrieve data from the request
         * @param bom               reference to the BusinessObjectManager
         * @param log               reference to the IsaLocation, needed for logging
         * @param startupParameter  object containing the startup parameters
         * @param eventHandler      object to capture events with
         * @param multipleInvocation flag indicating, that a multiple invocation occured
         * @param browserBack       flag indicating a browser back
         * @param shop              shop business object
         * @param buPaMa            business partner manager
         * @param collective order  the collective order object
         * @param order             the additional order object
         * @param documentHandler   document handler
         *
         * @return logical key for a forward to another action or jsp
         */
        protected abstract String orderPerform(HttpServletRequest request,
                    HttpServletResponse response,
                    HttpSession session,
                    UserSessionData userSessionData,
                    RequestParser parser,
                    BusinessObjectManager bom,
                    IsaLocation log,
                    IsaCoreInitAction.StartupParameter startupParameter,
                    BusinessEventHandler eventHandler,
                    boolean multipleInvocation,
                    boolean browserBack,
                    Shop shop,
                    BusinessPartnerManager buPaMa,
                    CollectiveOrderChange collectiveOrder,
                    OrderChange order,
                    DocumentHandler documentHandler)
                        throws CommunicationException;



    /**
     * Convenience method to update the salesdocument in the backend before
     * any further processing is done.
     *
     * @param parser The request, wrapped into a parser object
     * @param userSessionData   object wrapping the session
     * @param order              the document to store the data in
     * @param shop              shop business object
     * @param bupama            business partner manager object
     * @param log reference to the logging context
     */
    protected void updateOrder(RequestParser parser,
                               UserSessionData userSessionData,
                               CollectiveOrderChange collectiveOrder,
                               OrderChange order,
                               Shop shop,
                               BusinessPartnerManager bupama,
                               IsaLocation log) throws CommunicationException {

        parseRequest(parser, userSessionData, collectiveOrder, log);

        // get the ref item
        RequestParser.Parameter product              =  parser.getParameter("product[]");

        RequestParser.Parameter refItemsParameter    =  parser.getParameter("refItemKey[]");
        RequestParser.Parameter productKeysParameter =  parser.getParameter("productkey[]");
        RequestParser.Parameter productConfigurableParameter =  parser.getParameter("productConfigurable[]");
        
 
        // set the predecessor information
        ItemList items = collectiveOrder.getItems();

        int size = items.size();
        int toRemove[] = new int[size];
        int toRemoveSize = 0;

        String refItems[] = new String[size];
        String productKeys[] = new String[size];
        String productConfigurable[] = new String[size];

        int j= 0;
        for(int i = 1; i <= product.getNumValues(); i++) {

            if (product.getValue(i).getString().length() > 0) {

                refItems[j] = refItemsParameter.getValue(i).getString();
                productKeys[j] = productKeysParameter.getValue(i).getString();
                productConfigurable[j] = productConfigurableParameter.getValue(i).getString();
                j++;
            }
        }

        for (int i = 0; i < size; i++) {

            ItemSalesDoc item = (ItemSalesDoc)items.getItemData(i);

            // search for new entries
            if (item.getTechKey().getIdAsString().length() == 0) {

                String quantity = item.getQuantity();

                // check if item is not empty
                if (quantity == null || quantity.trim().equals("0") || quantity.length() == 0 ) {
                    toRemove[toRemoveSize++]=i;
                }
                else {

                    ConnectedDocumentItem connectedItem = new ConnectedDocumentItem();

					TechKey refItemKey = new TechKey(refItems[i]);

                    connectedItem.setTechKey(refItemKey);
                    connectedItem.setDocumentKey(order.getTechKey());
                    item.setPredecessor(connectedItem);
                    item.setProductId(new TechKey(productKeys[i]));

                    // copy the configuration of the connected item
                    if (productConfigurable[i].equals("X")) {
                        order.getItemConfig(refItemKey);
                        IPCItemReference ipcItemReference = IPCClientObjectFactory.getInstance().newIPCItemReference();
                        ipcItemReference.setItemId(refItemKey.getIdAsString());
                        ipcItemReference.setDocumentId(order.getHeader().getIpcDocumentId().toString());

                        IPCBOManager ibom = this.getIPCBusinessObjectManager(userSessionData);
                        // InteractionConfigContainer data = getInteractionConfig(parser.getRequest());
						// String client = data.getConfig("ipc_scenario").getValue("client");                
						try {
							IPCClient ipcClient = ibom.createIPCClient(shop.getLanguageIso());
                        
                            IPCItem ipcItem = ipcClient.getIPCItem(ipcItemReference);
                            if(ipcItem != null) {
                                item.setExternalItem(ipcItem);
                            }
                        }
                        catch(com.sap.spc.remote.client.object.IPCException e) {
                            log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "Create IPCClient failed! - No IPC client available!");
                            log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, e.getMessage());        
                            //  return mapping.findForward("error");
                        }
                    }

					if (item.getExtendedStatus()!=null) {
						// Handle 'Purchased' or 'Forwarded' Process. Request has been passed previously but
						// including User Status (E0001, ...). Now user status must be removed, and only
						// business process will remain.
						ExtendedStatusListEntry xStatusData = (ExtendedStatusListEntry)item.getExtendedStatus().getSelectedStatus();
						if (xStatusData.getBusinessProcess().equals("PCHS")) {
							xStatusData = (ExtendedStatusListEntry)
										  item.getExtendedStatus().createExtendedStatusListEntry(new TechKey(""),
																						 null,null,null,"PCHS");
							item.getExtendedStatus().setSelectedStatus(xStatusData);
						} else if (xStatusData.getBusinessProcess().equals("FWRD")) {
							xStatusData = (ExtendedStatusListEntry)
							item.getExtendedStatus().createExtendedStatusListEntry(new TechKey(""),
									null,null,null,"FWRD");
							item.getExtendedStatus().setSelectedStatus(xStatusData);
						}
					}
                }
            }
        }

        collectiveOrder.update(bupama,shop);
    }



        /**
         * Template method to be overwritten by the subclasses of this class.
         * Normally you should overwrite this method and you do not touch the
         * <code>isaPerform</code> method.
         *
         * @param request           request context
         * @param response          response context
         * @param session           unwrapped session context
         * @param userSessionData   object wrapping the session
         * @param requestParser     parser to simple retrieve data from the request
         * @param bom               reference to the BusinessObjectManager
         * @param log               reference to the IsaLocation, needed for logging
         * @param startupParameter  object containing the startup parameters
         * @param eventHandler      object to capture events with
         * @param multipleInvocation flag indicating, that a multiple invocation occured
         * @param browserBack       flag indicating a browser back
         * @param shop              shop business object
         * @param order             the current order edited
         * @param targetDocument    the target document
         * @param documentHandler   document handler
         *
         * @return logical key for a forward to another action or jsp
         */
        protected String orderPerform(HttpServletRequest request,
                    HttpServletResponse response,
                    HttpSession session,
                    UserSessionData userSessionData,
                    RequestParser parser,
                    BusinessObjectManager bom,
                    IsaLocation log,
                    IsaCoreInitAction.StartupParameter startupParameter,
                    BusinessEventHandler eventHandler,
                    boolean multipleInvocation,
                    boolean browserBack,
                    Shop shop,
                    OrderChange order,
                    DocumentState targetDocument,
                    DocumentHandler documentHandler)
                        throws CommunicationException {

		final String METHOD_NAME = "orderPerform()";
		log.entering(METHOD_NAME);
        // the given order is always a collective order
        CollectiveOrderChange collectiveOrder = (CollectiveOrderChange)order;

        BusinessPartnerManager buPaMa = bom.createBUPAManager();

        if(buPaMa == null) {
        	log.exiting();
            throw new PanicException("No Business Partner Manager returned from bom");
        }

        // get the additional order object from the document handler
        order = null;

        if(targetDocument != null) {
            if(targetDocument instanceof OrderTemplate) {
                order = new OrderTemplateChange(bom.getOrderTemplate());
            }
            else if(targetDocument instanceof CollectiveOrder) {
                order = new CollectiveOrderChange(bom.getCollectiveOrder());
            }
            else if(targetDocument instanceof CustomerOrder) {
                order = new CustomerOrderChange(bom.getCustomerOrder());
            }
            else {
                order = new OrderChange(bom.getOrder());
            }
        }
		log.exiting();
        // call the abstract perform method
        return  orderPerform(request, response, session, userSessionData,
                             parser, bom, log, startupParameter, eventHandler,
                             multipleInvocation, browserBack, shop, buPaMa,
                             collectiveOrder, order,
                             documentHandler);

    }


    /**
     * Overriden <em>isaPerform</em> method of <em>IsaCoreBaseAction</em>.
     */
    public ActionForward isaPerform(ActionMapping mapping,
                                    ActionForm form,
                                    HttpServletRequest request,
                                    HttpServletResponse response,
                                    UserSessionData userSessionData,
                                    RequestParser parser,
                                    BusinessObjectManager bom,
                                    IsaLocation log,
                                    IsaCoreInitAction.StartupParameter startupParameter,
                                    BusinessEventHandler eventHandler,
                                    boolean multipleInvocation,
                                    boolean browserBack)
    throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        HttpSession session = request.getSession();

        DocumentHandler documentHandler = (DocumentHandler)
                                          userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);

        if(documentHandler == null) {
        	log.exiting();
            throw new PanicException("No document handler found");
        }

        DocumentState targetDocument =
        documentHandler.getDocument(ManagedDocument.TARGET_DOCUMENT);

        if(targetDocument == null) {
        	log.exiting();
            throw new PanicException("No target document returned from documentHandler");
        }

        OrderChange order;

        order = new CollectiveOrderChange(bom.getCollectiveOrder());

        if(order == null) {
        	log.exiting();
            throw new PanicException("No order returned from bom");
        }

        Shop shop = bom.getShop();

        if(shop == null) {
        	log.exiting();
            throw new PanicException("No shop returned from bom");
        }

        String forwardTo = orderPerform(request, response, session, userSessionData,
                                        parser, bom, log, startupParameter, eventHandler,
                                        multipleInvocation, browserBack, shop,
                                        order, targetDocument,
                                        documentHandler);
		log.exiting();
        return mapping.findForward(forwardTo);
    }



    /**
     * @see com.sap.isa.isacore.action.order.MaintainOrderBaseAction#parseRequestHeader(com.sap.isa.core.util.RequestParser, com.sap.isa.businessobject.order.OrderChange, com.sap.isa.core.logging.IsaLocation, boolean)
     * @deprecated
     */
    protected void parseRequestHeader(RequestParser parser, OrderChange order, IsaLocation log, boolean isDebugEnabled)
        throws CommunicationException {

    }

    /**
     * @see com.sap.isa.isacore.action.order.MaintainOrderBaseAction#parseRequestHeader(com.sap.isa.core.util.RequestParser, com.sap.isa.core.UserSessionData, com.sap.isa.businessobject.order.OrderChange, com.sap.isa.core.logging.IsaLocation, boolean)
     */
    protected void parseRequestHeader(
        RequestParser parser,
        UserSessionData userSessionData,
        OrderChange order,
        IsaLocation log,
        boolean isDebugEnabled)
        throws CommunicationException {
    }

}
