/*****************************************************************************
    Class         HomSessionListner
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Listner for Session
    Author:       SAP
    Created:      October 2002
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/04 $
*****************************************************************************/

package com.sap.isa.isacore.action.order.hom;

import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.order.CollectiveOrder;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.logging.IsaLocation;

/**
 * The Listner unlocks the collective order.
 * The Listner prevent a shop after the session time out or is invalid.  
 * 
 * @author SAP
 * @version 1.0
 *
 */
public class HomSessionListner implements HttpSessionBindingListener {

	protected BusinessObjectManager bom;
	protected TechKey contactKey;
	protected TechKey soldToKey;

	/** 
	  *  The IsaLocation of the current Action.
	  *  This will be instantiated during perform and is always present.
	  */
	 protected IsaLocation log = IsaLocation.getInstance(HomSessionListner.class.getName());
	
	/**
	 * Constructor. 
	 * 
	 * @param  bom shop admin business object manager
	 * @param	contactKey key of the contact person
	 * @param  soldToKey key for the sold to party 
	 * 
	 */
	public HomSessionListner(BusinessObjectManager bom, TechKey contactKey, TechKey soldToKey) {
		this.bom = bom;
		this.contactKey = contactKey;
		this.soldToKey = soldToKey;
	}	 

	/**
	 * @see javax.servlet.http.HttpSessionBindingListener#valueBound(HttpSessionBindingEvent)
	 */
	public void valueBound(HttpSessionBindingEvent arg0) {
	}

	/**
	 * @see javax.servlet.http.HttpSessionBindingListener#valueUnbound(HttpSessionBindingEvent)
	 */
	public void valueUnbound(HttpSessionBindingEvent arg0) {

        CollectiveOrder collectiveOrder = bom.createCollectiveOrder();

    	try {
            collectiveOrder.dequeue(contactKey, soldToKey);
    	}
    	catch (Exception ex) {
    		log.debug(ex.getMessage());
    	}	 	

	}

}
