/*****************************************************************************
  Class:        DarkMaintainCollectiveOrderUpdateAction
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:
  Created:      18.09.2001
  Version:      1.0

  $Revision: #6 $
  $Date: 2002/08/21 $
*****************************************************************************/

package com.sap.isa.isacore.action.order.hom;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businessobject.order.CollectiveOrderChange;
import com.sap.isa.businessobject.order.OrderChange;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.action.IsaCoreInitAction;

/**
 * Action to update the collectiveorder with data come from the <code>hom_processorder.jsp</code>.
 * The action will check the request parameter and decide if an update will be performed.  
 * You can overwritten the update flag if you overwrite the <code>customerExitPerformUpdate</code> 
 * method.  
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>Request parameter</b></td>
 *     <td><b>Value of parameter</b></td>
 *     <td><b>Remark</b></td>
 *   <tr>
 *   <tr><td>cancelpressed</td><td>!= ""</td><td>no update of the document</td></tr>
 *   <tr><td>showonly</td><td>!= ""</td><td>no update of the document</td></tr>
 * </table>
 * <p>
 * </p>
 *
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>success</td><td></td></tr>
 * </table>
 *
 *
 * @author SAP
 * @version 1.0
 */
public class DarkMaintainCollectiveOrderUpdateAction extends DarkMaintainCollectiveOrderBaseAction {
	
    /**
     * Template method to be overwritten by the subclasses of this class.
     * This method provides the possibilty, to easily prevent the 
     * action, to perform an update in the backend.
     * By default this method returns true.
     *
     * @param requestParser     parser to simple retrieve data from the request
     * 
     * @return true, if the order object should be updated in the backend
     *
     */
    public boolean customerExitPerformUpdate(RequestParser parser) {
        return true;
    }


    /**
     * Template method to be overwritten by the subclasses of this class.
     * Normally you should overwrite this method and you do not touch the
     * <code>isaPreform</code> method.
     *
     * @param request           request context
     * @param response          response context
     * @param session           unwrapped session context
     * @param userSessionData   object wrapping the session
     * @param requestParser     parser to simple retrieve data from the request
     * @param bom               reference to the BusinessObjectManager
     * @param log               reference to the IsaLocation, needed for logging
     * @param startupParameter  object containing the startup parameters
     * @param eventHandler      object to capture events with
     * @param multipleInvocation flag indicating, that a multiple invocation occured
     * @param browserBack       flag indicating a browser back
     * @param shop              shop business object
     * @param buPaMa            business partner manager
     * @param collective order  the collective order object
     * @param order             the additional order object
     * @param documentHandler   document handler
     *
     * @return logical key for a forward to another action or jsp
     */
    protected String orderPerform(HttpServletRequest request,
                HttpServletResponse response,
                HttpSession session,
                UserSessionData userSessionData,
                RequestParser parser,
                BusinessObjectManager bom,
                IsaLocation log,
                IsaCoreInitAction.StartupParameter startupParameter,
                BusinessEventHandler eventHandler,
                boolean multipleInvocation,
                boolean browserBack,
                Shop shop,
                BusinessPartnerManager buPaMa,
                CollectiveOrderChange collectiveOrder,
                OrderChange order,
                DocumentHandler documentHandler)
                    throws CommunicationException {
                    	
		final String METHOD_NAME = "orderPerform()";
		log.entering(METHOD_NAME);  
        boolean isDebugEnabled = log.isDebugEnabled();
        boolean performUpdate = true;

        performUpdate = customerExitPerformUpdate(parser);        

        if ((request.getParameter("showonly") != null) &&
    		(request.getParameter("showonly").length() > 0)) {


		    if (isDebugEnabled) {
        		log.debug("Requested action 'showonly' dispatching ! No Update on document");
    		}
            performUpdate = false;
		}
		else if ((request.getParameter("cancelpressed") != null) &&
                 (request.getParameter("cancelpressed").length() > 0)){

            if (isDebugEnabled) {
                log.debug("Requested action 'cancelpressed' dispatching! No Update on document");
            }
            performUpdate = false;
        }


		if (performUpdate && !multipleInvocation) {
	        // update in the backend
	        updateOrder(parser,
	                    userSessionData, 
	                    collectiveOrder, 
	                    order,
	                    shop, 
	                    buPaMa,
	                    log);
        }
           
		log.exiting();
        return "success";
    }
}