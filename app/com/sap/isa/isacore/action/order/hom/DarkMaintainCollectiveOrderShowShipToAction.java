/*****************************************************************************
  Class:        DarkMaintainCollectiveOrderShowShipToAction
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:
  Created:      18.09.2001
  Version:      1.0

  $Revision: #3 $
  $Date: 2002/08/05 $
*****************************************************************************/

package com.sap.isa.isacore.action.order.hom;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.ShipTo;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businessobject.order.CollectiveOrderChange;
import com.sap.isa.businessobject.order.OrderChange;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.action.ActionConstants;
import com.sap.isa.isacore.action.IsaCoreInitAction;
import com.sap.isa.isacore.action.ShowShipToAction;

/**
 * Action to cancel the change process of an order; the actual document is
 * thrown away.
 * <p>
 * The following forwards are used by this action:
 * </p>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>FORWARD_UPDATEDOCUMENT</td><td>always used</td></tr>
 * </table>
 *
 * @author
 * @version 1.0
 */
public class DarkMaintainCollectiveOrderShowShipToAction extends DarkMaintainCollectiveOrderBaseAction {

    /**
     * Template method to be overwritten by the subclasses of this class.
     * Normally you should overwrite this method and you do not touch the
     * <code>isaPreform</code> method.
     *
     * @param request           request context
     * @param response          response context
     * @param session           unwrapped session context
     * @param userSessionData   object wrapping the session
     * @param requestParser     parser to simple retrieve data from the request
     * @param bom               reference to the BusinessObjectManager
     * @param log               reference to the IsaLocation, needed for logging
     * @param startupParameter  object containing the startup parameters
     * @param eventHandler      object to capture events with
     * @param multipleInvocation flag indicating, that a multiple invocation occured
     * @param browserBack       flag indicating a browser back
     * @param shop              shop business object
     * @param buPaMa            business partner manager
     * @param collective order  the collective order object
     * @param order             the additional order object
     * @param documentHandler   document handler
     *
     * @return logical key for a forward to another action or jsp
     */
    protected String orderPerform(HttpServletRequest request,
                HttpServletResponse response,
                HttpSession session,
                UserSessionData userSessionData,
                RequestParser parser,
                BusinessObjectManager bom,
                IsaLocation log,
                IsaCoreInitAction.StartupParameter startupParameter,
                BusinessEventHandler eventHandler,
                boolean multipleInvocation,
                boolean browserBack,
                Shop shop,
                BusinessPartnerManager buPaMa,
                CollectiveOrderChange collectiveOrder,
                OrderChange order,
                DocumentHandler documentHandler)
                    throws CommunicationException {

		final String METHOD_NAME = "orderPerform()";
		log.entering(METHOD_NAME);  	  		
		// get the index for the ship to		
        if (parser.getParameter(ShowShipToAction.PARAM_SHIPTO_INDEX).isSet()) {

            ShipTo[] shipTos = bom.getCollectiveOrder().getShipTos();	
			int index =  parser.getParameter(ShowShipToAction.PARAM_SHIPTO_INDEX).getValue().getInt();           
                                  
            parser.getRequest().setAttribute(ShowShipToAction.PARAM_SHIPTO_SOURCE, ShowShipToAction.VALUE_SHIPTO_FROM_RC);
            parser.getRequest().setAttribute(ActionConstants.RC_ADDRESS,shipTos[index].getAddress());
            parser.getRequest().setAttribute(ShowShipToAction.RC_DISABLED, "true");                        
        }    
   				
		log.exiting();
        return "success";
    }
}