package com.sap.isa.isacore.action.order.hom;

import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.action.order.ChangeOrderAction;
import com.sap.isa.isacore.action.order.ManagedDocumentProcessCustomerOrder;

/**
 * Display the customer order for process purposes.
 * The action take the actual document from <code>DocumentHandler</code> and
 * initiate the process customer order process.
 * 
 * @author SAP
 * @version 1.0
 *
 */
public class ProcessCustomerOrderAction extends ChangeOrderAction {
    
    static final public  String CUSTOMER_ORDER_PROCESS = "customer_order_process";
	
    public ProcessCustomerOrderAction(){
        super();
        customerOrderForward = CUSTOMER_ORDER_PROCESS;
    }    


    /**
     * Creates a apropriate manageDocument.
     * 
     * @param docType
     * @param salesDoc
     * @param headerSalesDoc
     * @param forward
     * @return ManagedDocument
     */
    protected ManagedDocument createManagedDocument(
        String docType,
        SalesDocument salesDoc,
        HeaderSalesDocument headerSalesDoc,
        String forward) {
        	
		final String METHOD_NAME = "createManagedDocument()";
		log.entering(METHOD_NAME);
        ManagedDocument mDoc = null;
        
        mDoc = new ManagedDocumentProcessCustomerOrder(salesDoc,
                                   docType,
                                   headerSalesDoc.getSalesDocNumber(),
                                   headerSalesDoc.getPurchaseOrderExt(),
                                   headerSalesDoc.getDescription(),
                                   headerSalesDoc.getChangedAt(),
                                   forward,
                                   null,
                                   null);
        log.exiting();                        
        return mDoc;
    }


}
