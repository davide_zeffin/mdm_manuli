package com.sap.isa.isacore.action.order.hom;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.item.ItemList;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.businessobject.order.Order;
import com.sap.isa.businessobject.ordertemplate.OrderTemplate;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.MessageDisplayer;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.action.order.MaintainOrderBaseAction;

public class HomShowProcessOrderAction extends IsaCoreBaseAction {
	
    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser parser,
            BusinessObjectManager bom,
            IsaLocation log) throws CommunicationException {

		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        Shop shp = bom.getShop();
        if (shp == null) {
        	log.exiting();
          	throw new PanicException("No shop returned from bom");
        }
        DocumentHandler documentHandler = (DocumentHandler)
                userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
        DocumentState docstate = documentHandler.getManagedDocumentOnTop();
         // haben wir eins?
        if (docstate == null) {
           // use global forward to show valid frame
           // this if statement is also reached, if the readForUpdate below failed
           // and the message.jsp was displayed and the user clicks on the back icon
           // of the browser, because in this case the last executed action, which is this action,
           // is recalled. But in that case, the managed document was already deleted
            log.exiting();
            return mapping.findForward("updatedocumentview");
        }
        ManagedDocument mandoc = (ManagedDocument) docstate;
        docstate = mandoc.getDocument();
        SalesDocument salesDoc = (SalesDocument) docstate;
        salesDoc.clearMessages();

        try {
           if (salesDoc instanceof Order) {
               ((Order) salesDoc).readForUpdate();
           }
           else if (salesDoc instanceof OrderTemplate) {
               ((OrderTemplate) salesDoc).readForUpdate();
           }
           else {
           		log.exiting();
               	throw new PanicException("No valid document returned from document handler");
           }
        }
        catch (Exception ex) {
          documentHandler.release(salesDoc);

          // create a Message Displayer for the error page!
          MessageDisplayer messageDisplayer = new MessageDisplayer();

          messageDisplayer.addToRequest(request);
          messageDisplayer.copyMessages(salesDoc);

          // the action which is to call after the message
          messageDisplayer.setAction("documentstatusdetailprepare.do");
          messageDisplayer.setActionParameter("?techkey="+salesDoc.getTechKey().getIdAsString()
                                               +"&objecttype="+mandoc.getDocType()
                                               +"&objects_origin=&object_id=" );

          log.exiting();
          return mapping.findForward("message");
        }

        // determine document type via document handler in order to
        // activate the possibility to delete positions in order templates (note 521056)
        String docType = mandoc.getDocType();
        // set documenttype in header

        //salesDoc.getHeader().setDocumentTypeOther(docType);
		salesDoc.getHeader().setAllValuesChangeable(false, false, false, false, false, false, false);
        // set deletable flag to true for order templates


        ItemList items = salesDoc.getItems();
        boolean deletable = true;
        ItemSalesDoc item;
        for (int i = 0; i < items.size(); i++) {
            item = (ItemSalesDoc)items.get(i);
    		if (docType.equals(HeaderSalesDocument.DOCUMENT_TYPE_ORDERTEMPLATE)) {
            	item.setDeletable(deletable);
    		}
			//item.setAllValuesChangeable(false, false, false, false, false, false, false, false, false,false, false, false, false, false, false, false, false, false, false, false, false, false);
        }

        request.setAttribute(MaintainOrderBaseAction.RC_HEADER, salesDoc.getHeader());
        request.setAttribute(MaintainOrderBaseAction.RC_ITEMS, salesDoc.getItems());
        request.setAttribute(MaintainOrderBaseAction.RC_SHIPCOND, salesDoc.readShipCond(shp.getLanguage()));
        request.setAttribute(MaintainOrderBaseAction.RC_MESSAGES, salesDoc.getMessageList());
        userSessionData.setAttribute(
                MaintainOrderBaseAction.SC_SHIPTOS, salesDoc.getShipTos());
        log.exiting();
        return mapping.findForward("showorder");
    }

}