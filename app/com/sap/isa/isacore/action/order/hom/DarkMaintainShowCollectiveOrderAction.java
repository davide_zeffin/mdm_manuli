/*****************************************************************************
    Class         DarkMaintainShowCollectiveOrderAction
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Action for the Profile Maintenance
    Author:
    Created:      10. April 2001
    Version:      0.2
*****************************************************************************/
package com.sap.isa.isacore.action.order.hom;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.ShipTo;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businessobject.order.CollectiveOrder;
import com.sap.isa.businessobject.order.CollectiveOrderChange;
import com.sap.isa.businessobject.order.CustomerOrder;
import com.sap.isa.businessobject.order.OrderChange;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.businesspartner.businessobject.PartnerFunctionBase;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.eai.BackendRuntimeException;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.MessageDisplayer;
import com.sap.isa.isacore.ShipToSelection;
import com.sap.isa.isacore.action.IsaCoreInitAction;


/**
 * Read and display the order for change purposes.
 * The action takes care of the following order types:
 * <ul>
 *  <li>{@link com.sap.isa.businessobject.order.Order}</li>
 *  <li>{@link com.sap.isa.businessobject.ordertemplate.OrderTemplate}</li>
 *  <li>{@link com.sap.isa.businessobject.order.CollectiveOrder}</li>
 *  <li>{@link com.sap.isa.businessobject.order.CustomerOrder}</li>
 * </ul>
 *
 * @author SAP
 * @version 0.1
 */
public class DarkMaintainShowCollectiveOrderAction
    extends DarkMaintainCollectiveOrderBaseAction {
    static final public String RC_COLLECTIVE_ORDER = "collective_order";
    static final public String RC_SHIPTOS = "collective_order_shiptos";
    static final public String RC_SHIPTO_CUSTOMER = "shipto_customer";

    /**
     * Template method to be overwritten by the subclasses of this class.
     * Normally you should overwrite this method and you do not touch the
     * <code>isaPreform</code> method.
     *
     * @param request           request context
     * @param response          response context
     * @param session           unwrapped session context
     * @param userSessionData   object wrapping the session
     * @param requestParser     parser to simple retrieve data from the request
     * @param bom               reference to the BusinessObjectManager
     * @param log               reference to the IsaLocation, needed for logging
     * @param startupParameter  object containing the startup parameters
     * @param eventHandler      object to capture events with
     * @param multipleInvocation flag indicating, that a multiple invocation occured
     * @param browserBack       flag indicating a browser back
     * @param shop              shop business object
     * @param buPaMa            business partner manager
     * @param collective order  the collective order object
     * @param order             the additional order object
     * @param documentHandler   document handler
     *
     * @return logical key for a forward to another action or jsp
     */
    protected String orderPerform(HttpServletRequest request, 
                                  HttpServletResponse response, 
                                  HttpSession session, 
                                  UserSessionData userSessionData, 
                                  RequestParser parser, 
                                  BusinessObjectManager bom, IsaLocation log, 
                                  IsaCoreInitAction.StartupParameter startupParameter, 
                                  BusinessEventHandler eventHandler, 
                                  boolean multipleInvocation, 
                                  boolean browserBack, 
                                  Shop shop, 
                                  BusinessPartnerManager buPaMa, 
                                  CollectiveOrderChange collectiveOrder, 
                                  OrderChange order, 
                                  DocumentHandler documentHandler)
                           throws CommunicationException {
                               
		final String METHOD_NAME = "orderPerform()";
		log.entering(METHOD_NAME);  
        // Page that should be displayed next.
        String forwardTo = "showorder";

        collectiveOrder.clearMessages();

        try {
            collectiveOrder.readForUpdate();
        }
        catch (BackendRuntimeException ex) {
            
            ManagedDocument mandoc = (ManagedDocument) documentHandler.getManagedDocumentOnTop();

            // haben wir eins?
            if (mandoc == null) {
            	log.exiting();
                return "updatedocumentview";
            }

            SalesDocument salesDoc = (SalesDocument) mandoc.getDocument();

            documentHandler.release(salesDoc);

            // create a Message Displayer for the error page!
            MessageDisplayer messageDisplayer = new MessageDisplayer();

            messageDisplayer.addToRequest(request);

            messageDisplayer.copyMessages(collectiveOrder);

            // the action which is to call after the message
            if (salesDoc instanceof CustomerOrder) {
                messageDisplayer.setAction(
                        "/b2b/customerdocumentstatusdetailprepare.do");
            }
            else if (salesDoc instanceof CollectiveOrder) {
                messageDisplayer.setAction(
                        "/b2b/collectivedocumentstatusdetailprepare.do");
            }
            else {
                messageDisplayer.setAction("/b2b/documentstatusdetailprepare.do");
            }

            messageDisplayer.setActionParameter("?techkey=" + 
                                                salesDoc.getTechKey()
                                                        .getIdAsString() + 
                                                "&objecttype=" + 
                                                mandoc.getDocType() + 
                                                "&objects_origin=&object_id=");
			log.exiting();
            return "message";
        }

        TechKey customerKey = order.getHeader()
                                   .getPartnerKey(PartnerFunctionBase.SOLDTO);

        ShipToSelection shipToSelection = new ShipToSelection(
                                                  bom.getCollectiveOrder()
                                                     .getShipToList());
                                                     
        ShipTo[] shipTos = shipToSelection.getShipTos();
        String shipToCustomer = "";

        if (customerKey != null) {
            BusinessPartner customer = buPaMa.getBusinessPartner(customerKey);

            shipToSelection.setSalesPartner(customer);

            if (!shipToSelection.isPartnerAvailable()) {
                ShipToSelection.addPartnerToShipTos(bom.getCollectiveOrder(), 
                                                    customer);
				// get the shipTos again if the customer is added                                                    
                shipTos = shipToSelection.getShipTos();                                                    
            }

            ShipTo shipTo = shipToSelection.getPartnerShipTo();
            if (shipTo == null) { // ACE Check fails
				ManagedDocument mandoc = (ManagedDocument) documentHandler.getManagedDocumentOnTop();

				// haben wir eins?
				if (mandoc == null) {
					log.exiting();
					return "updatedocumentview";
				}

				SalesDocument salesDoc = (SalesDocument) mandoc.getDocument();

				documentHandler.release(salesDoc);
            	
				// create a Message Displayer for the error page!
				MessageDisplayer messageDisplayer = new MessageDisplayer();

				messageDisplayer.addToRequest(request);
				messageDisplayer.setApplicationError(true);
				messageDisplayer.copyMessages(collectiveOrder);

				// the action which is to call after the message
				if (salesDoc instanceof CustomerOrder) {
					messageDisplayer.setAction(
							"/b2b/customerdocumentstatusdetailprepare.do");
				}
				else if (salesDoc instanceof CollectiveOrder) {
					messageDisplayer.setAction(
							"/b2b/collectivedocumentstatusdetailprepare.do");
				}
				else {
					messageDisplayer.setAction("/b2b/documentstatusdetailprepare.do");
				}

				messageDisplayer.setActionParameter("?techkey=" + 
													salesDoc.getTechKey()
															.getIdAsString() + 
													"&objecttype=" + 
													mandoc.getDocType() + 
													"&objects_origin=&object_id=");
				log.exiting();
				return "message";
            } 

            for (int i = 0; i < shipTos.length; i++) {
                if (shipTos[i].getTechKey().equals(shipTo.getTechKey())) {
                    shipToCustomer = "" + i;
                }
            }
        }

        request.setAttribute(RC_COLLECTIVE_ORDER, collectiveOrder);
        request.setAttribute(RC_SHIPTOS, shipTos);
        request.setAttribute(RC_SHIPTO_CUSTOMER, shipToCustomer);
		log.exiting();
        return forwardTo;
    }
}