package com.sap.isa.isacore.action.order.hom;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Search;
import com.sap.isa.businessobject.SearchResult;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.UserStatusProfileSearchCommand;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

public class HomCloserProcessOrderAction extends IsaCoreBaseAction {

    // some doings before closerprocessdetail
    public static final String RK_DLA_USERSTATUSPROFILE = "userstatusprofile";

    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser parser,
            BusinessObjectManager bom,
            IsaLocation log) throws CommunicationException {
            	
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        Shop shop    = bom.getShop();

        if (shop == null) {
        	log.exiting();
           throw new PanicException("Shop not found in BOM");
        }

        // Get User Status profile for given Procedure and add to request
        Search search = bom.createSearch();
        SearchResult resultStatusProfile = null;
        resultStatusProfile = search.performSearch(new UserStatusProfileSearchCommand(shop.getLanguage()));
        ResultData result =  new ResultData(resultStatusProfile.getTable());
        request.setAttribute(RK_DLA_USERSTATUSPROFILE, result);
		log.exiting();
        return mapping.findForward("success");
    }

}