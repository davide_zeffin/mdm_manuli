/*****************************************************************************
  Class:        MaintainOrderDispatcherAction
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:
  Created:      18.09.2001
  Version:      1.0

  $Revision: #6 $
  $Date: 2002/08/21 $
*****************************************************************************/

package com.sap.isa.isacore.action.order.hom;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businessobject.order.OrderChange;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.action.IsaCoreInitAction;
import com.sap.isa.isacore.action.order.ItemConfigurationBaseAction;

/**
 * Action to dispatch all requests coming from the <code>hom_processorder.jsp</code> to
 * different (specialized) actions. The decision to which action the control
 * is forwarded to depends on the request parameters set.<br><br>
 *
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>Request parameter</b></td>
 *     <td><b>Value of parameter</b></td>
 *     <td><b>Logical forward used</b></td>
 *   <tr>
 *   <tr><td>returnconfig</td><td>!= ""</td><td>returnconfig</td></tr>
 *   <tr><td>exitconfig</td><td>!= ""</td><td>exitconfig</td></tr>
 *   <tr><td>sendpressed</td><td>!= ""</td><td>send</td></tr>
 *   <tr><td>cancelpressed</td><td>!= ""</td><td>cancel</td></tr>
 *   <tr><td>replaceditem</td><td>!= ""</td><td>replaceditem</td></tr>
 *   <tr><td>refresh</td><td>!= null</td><td>refresh</td></tr>
 *   <tr><td>newpos</td><td>!= ""</td><td>refresh</td></tr>
 *   <tr><td>configitemid</td><td>!= ""</td><td>prepareitemconfig</td></tr>
 * </table>
 * <p>
 * According to the described parameters the corresponding forwards are used.
 * </p>
 *
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>returnconfig</td><td>return from the configuration, use the config data</td></tr>
 *   <tr><td>exitconfig</td><td>exit from the configuration, all config data is discarded</td></tr>
 *   <tr><td>send</td><td>the user is finished and wants to save the document</td></tr>
 *   <tr><td>newshipto</td><td>new ship tos are added to the document</td></tr>
 *   <tr><td>cancel</td><td>the document is cancelled</td></tr>
 *   <tr><td>refresh</td><td>the data of the document is send to the backend and displayed again</td></tr>
 *   <tr><td>replaceditem</td><td>an item was replaced with another one from the CUA</td></tr>
 *   <tr><td>prepareitemconfig</td><td>save all changes, before an item is configured</td></tr>
 * </table>
 *
 *
 * @author SAP
 * @version 1.0
 */
public class ProcessCustomerOrderDispatcherAction extends ProcessCustomerOrderBaseAction {


    private static final String FORWARD_SEND         = "send";
    private static final String FORWARD_CANCEL       = "cancel";
    private static final String FORWARD_REFRESH      = "refresh";
    private static final String FORWARD_UPDATEDOC    = "updatedocumentview";
    private static final String FORWARD_NEW_SHIPTO    = "newshipto";
    protected static final String FORWARD_PREPAREITEMCONFIG = "prepareitemconfig";


    /**
     * Overriden <em>isaPerform</em> method of <em>IsaCoreBaseAction</em>.
     */
   protected String orderPerform(HttpServletRequest request,
                HttpServletResponse response,
                HttpSession session,
                UserSessionData userSessionData,
                RequestParser parser,
                BusinessObjectManager bom,
                IsaLocation log,
                IsaCoreInitAction.StartupParameter startupParameter,
                BusinessEventHandler eventHandler,
                boolean multipleInvocation,
                boolean browserBack,
                Shop shop,
                OrderChange order,
                DocumentState targetDocument,
                DocumentHandler documentHandler)
                    throws CommunicationException {

		final String METHOD_NAME = "orderPerform()";
		log.entering(METHOD_NAME);
        boolean isDebugEnabled = log.isDebugEnabled();


        // Page that should be displayed next.
        String forwardTo = null;

        // if customer likes to override forward, this string is used
        String customerForwardTo = null;

        // check for possible cases
        if ((request.getParameter("showonly") != null) &&
            (request.getParameter("showonly").length() > 0)) {

            forwardTo = FORWARD_UPDATEDOC;

            if (isDebugEnabled) {
                log.debug("Requested action 'showonly' dispatching to Action '" + forwardTo + "'");
            }
        }
        else if ((request.getParameter("sendpressed") != null) &&
                 (request.getParameter("sendpressed").length() > 0)) {

            forwardTo = FORWARD_SEND;

            if (isDebugEnabled) {
                log.debug("Requested action 'sendpressed' dispatching to Action '" + forwardTo + "'");
            }

        }
        else if ((request.getParameter("cancelpressed") != null) &&
                 (request.getParameter("cancelpressed").length() > 0)){

            forwardTo = FORWARD_CANCEL;

            if (isDebugEnabled) {
                log.debug("Requested action 'cancelpressed' dispatching to Action '" + forwardTo + "'");
            }
        }
        else if ((request.getParameter("newshipto") != null) &&
                 (request.getParameter("newshipto").length() > 0)) {

            forwardTo = FORWARD_NEW_SHIPTO;

            if (isDebugEnabled) {
                log.debug("Requested action 'newshipto' dispatching to Action '" + forwardTo + "'");
            }
        }
        else if ((request.getParameter("configitemid") != null)
                      && (request.getParameter("configitemid").length() > 0)) {

             forwardTo = FORWARD_PREPAREITEMCONFIG;

            if (isDebugEnabled) {
                log.debug("Requested action 'configitemid' dispatching to Action '" + forwardTo + "'");
            }
        }
        else if ((request.getParameter("confignewitemid") != null)
                      && (request.getParameter("confignewitemid").length() > 0)) {

             forwardTo = "newitemconfig";
             
             request.setAttribute(ItemConfigurationBaseAction.PARAM_ITEMID,request.getParameter("confignewitemid"));

             if (isDebugEnabled) {
                log.debug("Requested action 'confignewitemid' dispatching to Action '" + forwardTo + "'");
             }
        }
        else if ((request.getParameter("refresh") != null) ||
                 ((request.getParameter("newpos") != null) && (request.getParameter("newpos").length() > 0))) {

            forwardTo = FORWARD_REFRESH;

            if (isDebugEnabled) {
                log.debug("Requested action 'refresh' dispatching to Action '" + forwardTo + "'");
            }
        }

        // user exit, to modify forward, if necessary
        customerForwardTo = customerExitDispatcher(parser);
        forwardTo = (customerForwardTo == null) ? forwardTo : customerForwardTo;
		log.exiting();
        return forwardTo;
    }
}