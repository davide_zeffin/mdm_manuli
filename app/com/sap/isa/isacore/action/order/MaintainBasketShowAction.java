/*****************************************************************************
  Class:        MaintainBasketShowAction
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Thomas Smits
  Created:      10.09.2001
  Version:      1.0

  $Revision: #5 $
  $Date: 2002/08/26 $
*****************************************************************************/

package com.sap.isa.isacore.action.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.action.IsaCoreInitAction;

/**
 * Action to display the basket.
 * <p>
 * The following forwards are used by this action:
 * </p>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>FORWARD_SHOWBASKET</td><td>always used</td></tr>
 * </table>
 *
 * @author Thomas Smits
 * @version 1.0
 */
public class MaintainBasketShowAction extends MaintainBasketBaseAction {
	/**
     * Creates a new instance of this class.
     */
    public MaintainBasketShowAction() {
    }


    protected String basketPerform(HttpServletRequest request,
                HttpServletResponse response,
                HttpSession session,
                UserSessionData userSessionData,
                RequestParser parser,
                BusinessObjectManager bom,
                IsaLocation log,
                IsaCoreInitAction.StartupParameter startupParameter,
                BusinessEventHandler eventHandler,
                boolean multipleInvocation,
                boolean browserBack,
                Shop shop,
                SalesDocument preOrderSalesDocument,
                boolean auction,
                DocumentState targetDocument,
                DocumentHandler documentHandler)
                    throws CommunicationException {

			final String METHOD_NAME = "basketPerform()";
			log.entering(METHOD_NAME);
            preOrderSalesDocument.readHeader();        
            preOrderSalesDocument.readAllItemsForUpdate();
            if (!preOrderSalesDocument.getItems().isEmpty()) {
                preOrderSalesDocument.readIpcInfo();    
            }

            // Test for necessary relationship between reseller and soldto
            checkSoldToResellerRelation(preOrderSalesDocument, shop, bom.createBUPAManager(), false);
			log.exiting();
            return FORWARD_SHOWBASKET;
    }


}