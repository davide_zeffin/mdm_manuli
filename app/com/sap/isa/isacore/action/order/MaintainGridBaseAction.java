/*****************************************************************************
	Class         MaintainGridBaseAction
	Copyright (c) 2004, SAP Labs India, All rights reserved.
	Description:  Action for the all common functionalities of grid product
	Author:
	Created:      23.09.2004
	Version:      1
*****************************************************************************/
package com.sap.isa.isacore.action.order;
import javax.servlet.http.HttpServletRequest;

import com.sap.isa.businessobject.item.ItemList;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.isacore.ItemHierarchy;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 * Base action for all Grid product related actions providing functionality to 
 * manage the shopping basket and sales documents similar to the basket. <br>
 * <p>
 * This action implements all common methods that are used in both Order create 
 * and Order change to make the actions as simple as possible. 
 * </p>
 *
 */
public class MaintainGridBaseAction extends IsaCoreBaseAction {

	/**
	 * Constants for different uiModes of Grid Entry Screen
	 */
	protected static final String GRID_PRICE_UIMODE 		= "P";
	
	protected static final String GRID_DEFAULT_UIMODE 		= "G";
	
	protected static final String GRID_STOCKIND_UIMODE 		= "S";
	
	protected static final String GRID_PRICE_STOCK_UIMODE 	= "C";
	
	protected static final String FORWARD_GRID_NAVIGATE 	= "gridnavigate";
	
	protected static final String FORWARD_GRID_STOCK_IND 	= "gridstockind";
	
	protected static final String FORWARD_ADD_GRID_PROD 	= "addgridprod";
	
	protected static final String UIMODE					="uimode";
	
	protected static final String CONFIG_TYPE				="configtype";
	
	public static final String GRID_NEXT 					= "gridnext";
	
	public static final String GRID_PREV 					= "gridprev";
	
	public static final String GRID_NEW_PROD 				= "gridnewprod";
	
	public static final String GRID		 					= "grid";
	
	/**
	 * Sets the <code>uiMode, configtype<code> flag to session, that is used in grid 
	 * entry screen to display the correct view.
	 * 
	 * @param request           object holding the parameter & attributes
	 * @param userSessionData   object wrapping the session
	 * 
	 * @return the uiMode to the calling Action to forward it to correct Action.
	 */
	protected void setUiModeForGrid(HttpServletRequest request, 
							 UserSessionData userSessionData) {
							 	
		String uiMode=""; 
		boolean enablePrice=false, enableStock=false;
			 
		if ((userSessionData.getAttribute(UIMODE) != null && 
			 userSessionData.getAttribute(UIMODE).toString().length() > 0)){
			uiMode = userSessionData.getAttribute(UIMODE).toString();	
		}
		if (request.getParameter("gridstock") != null &&
			request.getParameter("gridstock").length()>0){
			enableStock = true;
		}
		else if (userSessionData.getAttribute("gridstock") != null &&
					((String)userSessionData.getAttribute("gridstock")).length() > 0){
			enableStock = true;
		}
		
		if (request.getParameter("gridprice") != null &&
			request.getParameter("gridprice").length()>0){
			enablePrice = true;
		}
		else if (userSessionData.getAttribute("gridprice") != null &&
					((String)userSessionData.getAttribute("gridprice")).length() > 0){
			enablePrice = true;
		}
				
		if ((request.getParameter("configtype") != null &&
			 request.getParameter("configtype").length() > 0)){
			
			if ( enablePrice && enableStock )// Pricing and Stock Indicator enabled
				uiMode = GRID_PRICE_STOCK_UIMODE;
			else if (enablePrice) //Only Pricing enabled
				uiMode = GRID_PRICE_UIMODE;
			else if (enableStock) //only Stock Indiacator enabled
				uiMode = GRID_STOCKIND_UIMODE;
			else				  //Only Grid screen
				uiMode = GRID_DEFAULT_UIMODE;

			userSessionData.setAttribute(UIMODE,uiMode);
			userSessionData.setAttribute(CONFIG_TYPE, request.getParameter("configtype"));				
		}
	}
	
	/**
	 * Sets the PREV & NEXT button flags which are used to enable or disable these 
	 * buttons in the grid entry screen
	 * 
	 * @param request  			object holding the parameter & attributes
	 * @param items				Items of the salesDocument
	 * @param userSessionData   object wrapping the session
	 * 
	 */
	protected void setNavigationButtonsinGridScreen(HttpServletRequest request, 
													ItemList items,
													UserSessionData userSessionData) {
		
		String techKeyStr = "", prevFlag = "",nextFlag = "";
		if (request.getAttribute(ItemConfigurationBaseAction.PARAM_ITEMID) != null) {
			 	
			techKeyStr = ((Object) request.getAttribute(ItemConfigurationBaseAction.PARAM_ITEMID)).toString();
		}
			TechKey itemTechKey= new TechKey(techKeyStr);
			ItemSalesDoc forwardItem = items.get(itemTechKey);
		
		ItemHierarchy itemHierarchy = new ItemHierarchy(items);

		//This is done to set the PREV/NEXT flags in UserSessionData, so that the 
		//corresponding buttons are disabled or enabled in the screen
		
		ItemSalesDoc prevItem = itemHierarchy.getPreviousFirstLevelMainItem(forwardItem, true);
		ItemSalesDoc nextItem = itemHierarchy.getNextFirstLevelMainItem(forwardItem, true);
		
		if (prevItem != null)	prevFlag = "X";
		if (nextItem != null)	nextFlag = "X"; 

		userSessionData.setAttribute(GRID_PREV, prevFlag);
		userSessionData.setAttribute(GRID_NEXT, nextFlag);
		
	}
	
	/**
	 * Creates a new item with only Product and no other data
	 *   
	 * @param productName  name of the Product for which the ISA item to be created
	 * 
	 * @return item with only product in it. 
	 */
	protected ItemSalesDoc createNewItem(String productName){
		
		ItemSalesDoc item = new ItemSalesDoc(TechKey.EMPTY_KEY);
		item.setProduct(productName);
		item.setProductId(TechKey.EMPTY_KEY);
		item.setPcat(TechKey.EMPTY_KEY);
		item.setContractKey(TechKey.EMPTY_KEY  );
		item.setContractItemKey(TechKey.EMPTY_KEY);		
		return item;
	}
	
	/**
	 * Gets the Item for newly entered Product in Grid entry screen
	 * 
	 * @param items 	Items of the salesDocument
	 * 
	 * @return item for the newly entered product
	 */
	protected ItemSalesDoc getItemForNewlyAddedProduct(ItemList items){
		
		ItemSalesDoc result =null;
		
		if (items != null){	
			for (int i=items.size()-1;i>=0;i--){
				ItemSalesDoc item = items.get(i);
				
				if (item.getParentId() == null || ((TechKey)item.getParentId()).toString().equals("")){
					result = item;
					break;
				}
			}
		}
		return result;	
	}
	
	/**
	 * Gets the correct forward depending on the UiMODE and also
	 * sets the navigation buttons flags in session
	 * 
	 * @param forward	the actual forward of the Calling Action
	 * @param items		Items of the salesDocument
	 * @param request   object holding the parameter & attributes
	 * @param userSessionData  object wrapping the session
	 * 
	 * @return Correct forward depending on uiMode
	 */
	protected String forwardDependingOnUiMode( String forward,
											ItemList items,
											HttpServletRequest request,
											UserSessionData userSessionData){
		final String METHOD_NAME = "forwardDependingOnUiMode()";
		log.entering(METHOD_NAME);
		String forwardTo = forward;
		
		if (userSessionData.getAttribute(UIMODE) != null &&
			userSessionData.getAttribute(UIMODE).toString().length() > 0){
				
			//Set the PREV/NEXT button flags to make it enable/disable in the grid entry screen
			setNavigationButtonsinGridScreen(request, items, userSessionData);
						  
			String uiMode = userSessionData.getAttribute(UIMODE).toString();
			
			//check boxes Stock Indicator(S) or Stock Indicator & Pricing
			// forwarded to another Action
			if (uiMode.equals(GRID_STOCKIND_UIMODE) || 
				uiMode.equals(GRID_PRICE_STOCK_UIMODE) ){
			    	
			  forwardTo = FORWARD_GRID_STOCK_IND;
			}
		}
		log.exiting();
		return forwardTo;
	}
	/**
	 * Resets all the Grid related attributes from UserSessionData
	 * 
	 * @param userSessionData object wrapping the session
	 *
	 */
	protected void resetGridRelatedSessionAttributes(UserSessionData userSessionData){
				
		//remove grid attributes from userSessionData
		if (userSessionData.getAttribute(GRID_PREV) != null)
			userSessionData.removeAttribute(GRID_PREV);
			
		if (userSessionData.getAttribute(GRID_NEXT) != null)	
			userSessionData.removeAttribute(GRID_NEXT);
			
		if (userSessionData.getAttribute(UIMODE) != null)	
			userSessionData.removeAttribute(UIMODE);
			
		if (userSessionData.getAttribute(CONFIG_TYPE) != null)	
			userSessionData.removeAttribute(CONFIG_TYPE);
	}

	/**
	 * When one of the PREV/NEXT/NEW PRODUCT button is clicked in grid screen,
	 * this method is called from NavigateAction(both Basket & Order), that gets
	 * the PREV/NEXT item and returns the respective forward
	 *
	 * @param forward	the actual forward of the Calling Action
	 * @param items		Items of the salesDocument
	 * @param request   object holding the parameter & attributes
	 * @param userSessionData  object wrapping the session
	 * 
	 * @return the forward to the respective forward.
	 */
	protected String getPrevNextGridProduct(String forwardTo,
											ItemList items,
											HttpServletRequest request,
											UserSessionData userSessionData	){
		
		ItemSalesDoc forwardItem = null;
		ItemHierarchy itemHierarchy = new ItemHierarchy(items);
		
		// When one of the PREV/NEXT/NEW PRODUCT button is clicked, this IF statement is processed
		// ie: gets the correct grid product and forwards to Grid entry screen
		 
		if ( request.getParameter(GRID_PREV) != null &&
			 request.getParameter(GRID_PREV).length() > 0 ){
					
			TechKey itemTechKey = new TechKey(request.getParameter(GRID_PREV)) ;
			ItemSalesDoc currentItem = items.get(itemTechKey);
			 	
			forwardItem = itemHierarchy.getPreviousFirstLevelMainItem(currentItem, true);
		}
		else if ( request.getParameter(GRID_NEXT) != null &&	
				  request.getParameter(GRID_NEXT).length() > 0 ){
					 	
			TechKey itemTechKey = new TechKey(request.getParameter(GRID_NEXT)) ;
			ItemSalesDoc currentItem = items.get(itemTechKey);
				 	
			forwardItem = itemHierarchy.getNextFirstLevelMainItem(currentItem, true);
		}
		else if ( request.getParameter(GRID_NEW_PROD) != null &&
				  request.getParameter(GRID_NEW_PROD).length() > 0 ){
			forwardItem = null;
			forwardTo= FORWARD_ADD_GRID_PROD; //forward to AddGridProductAction
		}
		
		if (forwardItem != null){
		
			request.setAttribute(ItemConfigurationBaseAction.PARAM_ITEMID, forwardItem.getTechKey());
		
			//Get the correct forward depending on the uimode
			forwardTo= forwardDependingOnUiMode(forwardTo, items, request, userSessionData);
		}
		//required to make sure that the Navigation does not go into infinite loop.
		//This attribute is removed in UpdateDocumentViewAction.
		request.setAttribute("gridnav", "X");
		return forwardTo;
	}
}