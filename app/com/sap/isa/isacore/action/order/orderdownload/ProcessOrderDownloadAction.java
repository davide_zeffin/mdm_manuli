package com.sap.isa.isacore.action.order.orderdownload;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TimeZone;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.backend.boi.isacore.order.ItemListData;
import com.sap.isa.backend.boi.isacore.order.TextData;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CurrencyConverter;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.UOMConverter;
import com.sap.isa.businessobject.User;
import com.sap.isa.businessobject.order.OrderStatus;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.core.BaseAction;
import com.sap.isa.core.Constants;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.CodePageUtils;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.MassOrderDownloadBean;
import com.sap.isa.isacore.actionform.order.orderdownload.OrderDownloadForm;
import com.sap.isa.payment.backend.boi.PaymentBaseTypeData;
import com.sap.isa.payment.businessobject.PaymentBaseType;
import com.sap.isa.payment.businessobject.PaymentCCard;
import com.sap.isa.payment.businessobject.PaymentMethod;

public class ProcessOrderDownloadAction extends BaseAction {
	private static final String BASE_ORDER_NAME = "order";
	
	public ProcessOrderDownloadAction() {
	}

	/**
	 * Implemented doPerform method used by ActionBase.
	 * This method does some useful stuff and then calls isaPerform
	 */
	public final ActionForward doPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws IOException, ServletException {
		
		final String METHOD_NAME = "doPerform()";
		log.entering(METHOD_NAME);
		UserSessionData userSessionData =
			UserSessionData.getUserSessionData(request.getSession());
		/**
		 * This parameter will be set whenever
		 * there is successful completion of the mass download
		 * or the cancellation of the mass download basket
		 */
		String exit = request.getParameter("exit");
		if ("true".equals(exit)) {
			DocumentHandler documentHandler =
				(DocumentHandler) userSessionData.getAttribute(
					SessionConst.DOCUMENT_HANDLER);
			ManagedDocument mDoc = documentHandler.getManagedDocumentOnTop();
			/**
			 * @todo if there is already a managed document on the top
			 * which is of mass download orders type
			 * so forward to the success
			 */
			if (mDoc != null
				&& mDoc.getDocument() instanceof MassOrderDownloadBean) {
				documentHandler.release(documentHandler.getDocumentOnTop());
			}
			userSessionData.removeAttribute("downloadsuccess");
			userSessionData.removeAttribute("downloadfailure");
			userSessionData.removeAttribute("orderdownloadstatus");
			userSessionData.removeAttribute("massdownloadbean");
			userSessionData.removeAttribute("userstatusmap");
			log.exiting();
			return mapping.findForward("exitmassdownload");
		}

		String download = request.getParameter("download");
		if ("true".equals(download)) {
			OrderConversionManager.OrderDownloadStatus status =
				(
					OrderConversionManager
						.OrderDownloadStatus) userSessionData
						.getAttribute(
					"orderdownloadstatus");

			// Format the current time.
			String dateString = null;
			try {
				SimpleDateFormat formatter =
					new SimpleDateFormat("yyyyMMdd_hhmmss");
				Date currentTime_1 = new Date();
				dateString = formatter.format(currentTime_1);
			} catch (Exception neglect) {
				log.debug(neglect.getMessage());
			}
			String fileName = BASE_ORDER_NAME;
			if (dateString != null) {
				fileName = fileName + dateString;
			}
			writeToStream(status.getFormat(), status.getGeneratedDoc(),status.getGenDocBytes(), fileName, response, true);
			response.getOutputStream().flush();
			response.getOutputStream().close();
			userSessionData.removeAttribute("downloadsuccess");
			userSessionData.removeAttribute("downloadfailure");
			userSessionData.removeAttribute("orderdownloadstatus");
			log.exiting();
			return null;
		}

		/**
		 * For the case of multiple order download
		 * remove the bean which has the orders to be downloaded
		 */
		userSessionData.removeAttribute("massdownloadbean");
		// get the MBOM
		MetaBusinessObjectManager mbom = userSessionData.getMBOM();
		// get BOM
		BusinessObjectManager bom =
			(BusinessObjectManager) userSessionData.getBOM(
				BusinessObjectManager.ISACORE_BOM);

		// no BOM = no session
		if (bom == null) {
			log.exiting();
			return mapping.findForward(Constants.SESSION_NOT_VALID);
		}

		OrderDownloadForm orderDownloadForm = (OrderDownloadForm) form;
		/**
		 * That means the user has searched for some criteria and about to download some of
		 * the orders
		 */
		if (!orderDownloadForm.areIDSset()
			&& request.getParameter(OrderDownloadForm.ORDERGUIDSPARAM) != null) {
			orderDownloadForm.setOrderIDS(
				request.getParameter(OrderDownloadForm.ORDERIDSPARAM));
			orderDownloadForm.setOrderGUIDS(
				request.getParameter(OrderDownloadForm.ORDERGUIDSPARAM));
			if (request.getParameter("orderDetail") != null) {
				orderDownloadForm.setOrderDetail(
					request.getParameter("orderDetail"));
			}
		}
		if (orderDownloadForm.areIDSset()) {
			OrderConversionManager orderConversionMgr =
				OrderConversionManager.getInstance(
					orderDownloadForm.getFormat());
			String output = null;
			OrderConversionManager.OrderDownloadStatus status =
				orderConversionMgr.getNewOrderDownloadStatusInstance();
			status.setFormat(orderDownloadForm.getFormat());
			String orderDetail = request.getParameter("orderDetail");
			if (orderDetail == null /*!orderDownloadForm.isOrderDetail()*/
				) {
				orderConversionMgr.convert(
					bom,
					orderDownloadForm.getOrderGUIDS(),
					orderDownloadForm.getOrderIDS(),
					status,
					userSessionData.getLocale());
			} else {
				if (orderDownloadForm.getOrderGUIDS() != null)
					//                    output = orderConversionMgr.convert(bom,
					//                                                        (String)orderDownloadForm.getOrderGUIDS().get(0),
					//                                                        (String)orderDownloadForm.getOrderIDS().get(0));
					orderConversionMgr.convert(
						bom,
						(String) orderDownloadForm.getOrderGUIDS().get(0),
						(String) orderDownloadForm.getOrderIDS().get(0),
						status,
						userSessionData.getLocale());
				writeToStream(
					orderDownloadForm.getFormat(),
					status.getGeneratedDoc(),
					status.getGenDocBytes(),
					BASE_ORDER_NAME
						+ (String) orderDownloadForm.getOrderIDS().get(0),
					response,
					true);
				response.getOutputStream().flush();
				response.getOutputStream().close();
				log.exiting();
				return null;
			}

			userSessionData.setAttribute("orderdownloadstatus", status);
			userSessionData.setAttribute(
				"downloadsuccess",
				status.getSuccessfuldownloadOrders());
			userSessionData.setAttribute(
				"downloadfailure",
				status.getFaileddownloadOrders());

		}
		log.exiting();
		return mapping.findForward("success");
	}

	/**
	 * Now since the flow has changed this has to be the jsp page
	 * that will show the confirmation page.The confirmation can contain
	 * the orders downloaded vs failed while retreiving information
	 */

	void writeToStream(
		String format,
		String output,
		byte[] bytes,
		String fileName,
		HttpServletResponse response,
		boolean writeHeader) {
		try {
			String contentType = null;
			String fileNameExt = "";
			if (OrderDownloadForm.PDF_FORMAT_STR.equals(format)) {
				contentType = "application/pdf";
				fileNameExt = ".pdf";
			} else if (OrderDownloadForm.CSV_FORMAT_STR.equals(format)) {
				contentType = "text/csv";
				fileNameExt = ".csv";
			} else if (OrderDownloadForm.XML_FORMAT_STR.equals(format)) {
				contentType = "text/xml";
				fileNameExt = ".xml";
			}

			if (writeHeader) {
				response.setContentType(contentType);
				response.setHeader(
					"Content-Disposition",
					"attachment; filename=\"" + fileName + fileNameExt + "\"");
				response.setHeader("Cache-Control", "");
				response.setHeader("Pragma", "");
			}
			if (output != null || bytes!=null) {
				ServletOutputStream stream = response.getOutputStream();
				BufferedInputStream fif =  null;
				if(bytes==null)
					fif = new BufferedInputStream(
						new ByteArrayInputStream(output.getBytes()));
				else fif = new BufferedInputStream(new ByteArrayInputStream(bytes));

				int data;
				while ((data = fif.read()) != -1) {
					stream.write(data);
				}
				fif.close();
				stream = null;
			}
		} catch (Exception e) {
			log.debug("An exception occurred ProcessOrderDownload ", e);
		}
	}
}
/**
 * Title:
 * Description:  Order Conversion Manager for converting orders into other formats such
 *                as PDF,XML and CSV.
 * Copyright:    Copyright (c) 2002
 * Company:      SAP Labs
 * @author
 * @version 1.0
 */

/**
 * The Order conversion manager is the factory for the conversion of orders
 * into other formats such as XML,PDF and CSV.
 * The users who want to convert the order into the respective formats
 * has to obtain the relevant conversion instance through this class.
 * Note this class is a stateless conversion manager where in you have to
 * pass the order object for the conversion object.
 */
abstract class OrderConversionManager {

	public class OrderDownloadStatus {

		String genDoc = null;
		byte[] genDocBytes = null;
		String format = null;
		Set successOrderIds = new HashSet();
		Set failureOrderIds = new HashSet();
		public void setFormat(String str) {
			this.format = str;
		}
		public String getFormat() {
			return format;
		}
		public String getGeneratedDoc() {
			return genDoc;
		}
		public Set getSuccessfuldownloadOrders() {
			return successOrderIds;
		}
		public Set getFaileddownloadOrders() {
			return failureOrderIds;
		}
		public void setSuccessfulOrder(String orderid) {
			successOrderIds.add(orderid);
		}
		public void setFailedOrder(String orderid) {
			failureOrderIds.add(orderid);
		}
		public void setGenDoc(String genDoc) {
			this.genDoc = genDoc;
		}
		/**
		 * @return
		 */
		public byte[] getGenDocBytes() {
			return genDocBytes;
		}

		/**
		 * @param bs
		 */
		public void setGenDocBytes(byte[] bs) {
			genDocBytes = bs;
		}

	}

	public OrderDownloadStatus getNewOrderDownloadStatusInstance() {
		return new OrderDownloadStatus();
	}
	protected static IsaLocation log =
		IsaLocation.getInstance(OrderConversionManager.class.getName());

	static final char COMMA = ',';
	static final String FWSLASH = "/";
	static final String NEWLINE = "\n";
	static final int HDRCOLS = 51;
	static String HDR_ITEM_SEPARATOR_STRING;
	static final String DBLQUOTE = "\"";
	static final char DOT = '.';

	/**
	 * Constant for the XML format for the Order
	 */
	public static final byte XML_FORMAT = 0x01;
	/**
	 * Constant for the PDF format for the Order
	 */
	public static final byte PDF_FORMAT = 0x02;
	/**
	 * Constant for the CSV format for the Order
	 */
	public static final byte CSV_FORMAT = 0x03;

	private static final OrderToXMLConverter order2xml =
		new OrderToXMLConverter();
	private static final GenericOrderToPDFConverter order2pdf =
		new GenericOrderToPDFConverter();
	private static final OrderToCSVConverter order2csv =
		new OrderToCSVConverter();

	/**
	 * Obtain the instance of the relevant conversion manager
	 * by passing the relevent format constant
	 * @param format has to be one of the format constants defined in this class
	 * @returns OrderConversionManager the relevant orderconversionmanager for
	 *                                  the passed format
	 */
	public static OrderConversionManager getInstance(byte format) {
		if (format == XML_FORMAT) {
			return order2xml;
		}
		if (format == PDF_FORMAT) {
			return order2pdf;
		}
		if (format == CSV_FORMAT) {
			return order2csv;
		}
		throw new IllegalArgumentException(
			"Format " + format + " Not supported");
	}

	/**
	 * Call this stateless method by passing the relevant order object.
	 * @param bom the business object manager
	 * @param order the order object.
	 * @param OrderDownloadStatus the converted order object into the relevant format as a string
	 */
	public void convert(
		BusinessObjectManager bom,
		OrderStatus order,
		OrderDownloadStatus status,
		Locale locale) {
		convert(
			bom,
			order.getTechKey().getIdAsString(),
			order.getOrderHeader().getSalesDocNumber(),
			status,
			locale);

	}
	/**
	 * Call this stateless method by passing the relevant order object.
	 * @param bom the business object manager in case the order conversion happens in the
	 * backend or need to some more details.
	 * @param orderGuid the orderguid of the order
	 * @param orderid the id of the order
	 * @returns String the converted order object into the relevant format as a string
	 */
	public abstract void convert(
		BusinessObjectManager bom,
		String orderGuid,
		String orderid,
		OrderDownloadStatus status,
		Locale locale);
	/**
	 * Call this for mass download of orders.
	 * @param bom the business object manager in case the order conversion happens in the
	 * backend or need to some more details.
	 * @param orderGuids the orderguids in order
	 * @param orderids the array of the corresponding orderids in line with the orderguids
	 */
	//    public abstract String convert(BusinessObjectManager bom,
	//                                   ArrayList orderGuids , ArrayList orderIds);

	/**
	* Call this for mass download of orders.
	* @param bom the business object manager in case the order conversion happens in the
	* backend or need to some more details.
	* @param orderGuids the orderguids in order
	* @param orderids the array of the corresponding orderids in line with the orderguids
	*/
	public abstract void convert(
		BusinessObjectManager bom,
		ArrayList orderGuids,
		ArrayList orderIds,
		OrderDownloadStatus status,
		Locale locale);

	/**
	 * Given the format string get the conversion manager;This avoids
	 * the external callers to keep track of the byte codes that is being used to get
	 * the conversion manager instance.
	 * @throws IllegalArgumentException for the format other than the constant strings
	 * defined in this class
	 *
	 */
	public static OrderConversionManager getInstance(String format) {
		if (OrderDownloadForm.XML_FORMAT_STR.equals(format))
			return getInstance(XML_FORMAT);
		if (OrderDownloadForm.PDF_FORMAT_STR.equals(format))
			return getInstance(PDF_FORMAT);
		if (OrderDownloadForm.CSV_FORMAT_STR.equals(format))
			return getInstance(CSV_FORMAT);
		throw new IllegalArgumentException(
			"Format " + format + " Not supported");
	}

	/**
	 * utility method to fetch the order from the backend.
	 * @todo has to have a type of the sales document.So
	 * that we can retrieve the proper type of the document.
	 */
	protected SalesDocument fetchSalesDocument(
		String orderID,
		String guid,
		BusinessObjectManager bom) {
		User user = bom.getUser();
		if (user == null) {
			return null;
		}
		/**
		 * @todo what to do in case the order already exists in the bom
		 * One alternative is
		 * Has to take a backup of the order and then put it back to the bom
		 */
		SalesDocument order = bom.createOrder();
		TechKey orderGuid = new TechKey(guid);
		order.setTechKey(orderGuid);
		try {
			/**
			 * This call is made to set the dirty flags set to true
			 *Has to be another way of doing this
			 */
			order.clearMessages();
			order.read();
		} catch (Exception e) {
			log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, 
				"An error occurred in fetching the salesDoc from the backend",
				e);
		}

		return order;
	}

	/**
	 * utility method to fetch the order from the backend.
	 * @todo has to have a type of the sales document.So
	 * that we can retrieve the proper type of the document.
	 */
	protected OrderStatus fetchOrderStatus(
		String orderID,
		String guid,
		BusinessObjectManager bom) {
		User user = bom.getUser();
		if (user == null) {
			return null;
		}
		/**
		 * @todo what to do in case the order already exists in the bom
		 * One alternative is
		 * Has to take a backup of the order and then put it back to the bom
		 */
		OrderStatus order = bom.createOrderStatus();
		TechKey orderGuid = new TechKey(guid);
		order.setTechKey(orderGuid);
		//        order.setTechKey(orderGuid);
		Shop shop = bom.getShop();
		try {
			/**
			 * This call is made to set the dirty flags set to true
			 *Has to be another way of doing this
			 */
			/**
			 * For some reasons the shop is null.So setting explicitly the same
			 */
			order.setShop(shop);
			order.readOrderStatus(orderGuid, orderID, null, "order", null);
			//                      order.read(user);
		} catch (Exception e) {
			log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, 
				"An error occurred in fetching the salesDoc from the backend",
				e);
		}

		return order;
	}

	/**
	 * utility method to format the number according currency
	 */
	public String formatNumber(
		String price,
		char groupSeparator,
		char decimalPointSeparator) {
		try {
			StringBuffer output = new StringBuffer("");
			String outputStr = price.replace(decimalPointSeparator, '#');
			StringTokenizer strTok =
				new StringTokenizer(outputStr, "" + groupSeparator);
			while (strTok.hasMoreTokens()) {
				output.append(strTok.nextToken());
			}
			return output.toString().replace('#', '.');
		} catch (Exception e) {
			return price;
		}
	}

}

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */
class GenericOrderToPDFConverter extends OrderConversionManager {
	GenericOrderToPDFConverter() {
	}
	protected static IsaLocation log =
		IsaLocation.getInstance(GenericOrderToPDFConverter.class.getName());

	/**
	 * Call this stateless method by passing the relevant order object.
	 * @param bom the business object manager in case the order conversion happens in the
	 * backend or need to some more details.
	 * @param orderGuid the orderguid of the order
	 * @param orderid the id of the order
	 * @returns String the converted order object into the relevant format as a string
	 */
	public void convert(
		BusinessObjectManager bom,
		String orderGuid,
		String orderid,
		OrderDownloadStatus status,
		Locale locale) {

		/**
		 * this operation is backend specific.This will return null if bom is null
		 */
		if (bom == null) {
			throw new IllegalArgumentException("BOM is null");
		}
		com.sap.isa.businessobject.order.OrderToPDFConverter order2pdf =
			bom.getOrderToPDFConverter();
		if (order2pdf == null)
			order2pdf = bom.createOrderToPDFConverter();

		try {
			byte[] bytes = order2pdf.convertOrderToPDF(
				new TechKey(orderGuid),
				locale.getLanguage());
			if (bytes == null || bytes.length==0) {
				status.setFailedOrder(orderid);
			} else {
				status.setSuccessfulOrder(orderid);
				status.setGenDocBytes(bytes);
			}
		} catch (Exception e) {
			log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "An error occurred in pdf conversion of order", e);
		}
		return;
	}

	/**
	 * Call this for mass download of orders.
	 * @param bom the business object manager in case the order conversion happens in the
	 * backend or need to some more details.
	 * @param orderGuids the orderguids in order
	 * @param orderids the array of the corresponding orderids in line with the orderguids
	 */
	public void convert(
		BusinessObjectManager bom,
		ArrayList orderGuids,
		ArrayList orderIds,
		OrderDownloadStatus status,
		Locale locale) {
		/**
		 * this operation is backend specific.This will return null if bom is null
		 */
		if (bom == null) {
			throw new IllegalArgumentException("BOM is null");
		}
		Collection techKeys = new HashSet();
		for (int i = 0; i < orderGuids.size(); i++) {
			techKeys.add(new TechKey((String) orderGuids.get(i)));
		}
		com.sap.isa.businessobject.order.OrderToPDFConverter order2pdf =
			bom.getOrderToPDFConverter();
		if (order2pdf == null)
			order2pdf = bom.createOrderToPDFConverter();

		try {
			byte[] bytes = 
					order2pdf.convertOrderToPDF(
						techKeys,
						locale.getLanguage());
			/**
			 * @todo extracting out the failed order in the pdf
			 */

			for (int i = 0; i < orderIds.size(); i++) {
				if (bytes == null || bytes.length == 0) {
					status.setFailedOrder((String) orderIds.get(i));
				} else {
					status.setSuccessfulOrder((String) orderIds.get(i));
				}

			}
			status.setGenDocBytes(bytes);
		} catch (Exception e) {
			for (int i = 0; i < orderIds.size(); i++) {
				status.setFailedOrder((String) orderIds.get(i));
			}
			log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "An error occurred in pdf conversion of order", e);
		}
	}
}
class OrderToCSVConverter extends OrderConversionManager {

	OrderToCSVConverter() {
	}
	protected static IsaLocation log =
		IsaLocation.getInstance(OrderToCSVConverter.class.getName());

	static {
		StringBuffer sb = new StringBuffer("");
		for (int i = 0; i < HDRCOLS; i++) {
			sb.append(COMMA);
		}
		HDR_ITEM_SEPARATOR_STRING = sb.toString();
	}

	private static final String HEADING =
		"ORDERID,ORDERDATE,LANGUAGE,CURRENCY,"
			+ "VEN_ID,VEN_COMPANY,VEN_STREET,VEN_HOUSENUM,VEN_CITY,VEN_COUNTRY,VEN_POSTALCODE,VEN_TELEPHONE,VEN_EMAIL,"
			+ "OP_ID,OP_COMPANY,OP_STREET,OP_HOUSENUM,OP_CITY,OP_COUNTRY,OP_POSTALCODE,OP_TELEPHONE,OP_EMAIL,BP_ID,"
			+ "BP_COMPANY,BP_STREET,BP_HOUSENUM,BP_CITY,BP_COUNTRY,BP_POSTALCODE,BP_TELEPHONE,"
			+ "BP_EMAIL,SP_ID,SP_COMPANY,SP_STREET,SP_HOUSENUM,SP_CITY,SP_COUNTRY,SP_POSTALCODE,"
			+ "SP_TELEPHONE,SP_EMAIL,"
			+ "BD_PARTNER_TO_CUST_AMOUNT,BD_PARTNER_TO_CUST_CURRENCY,"
			+ "BD_PARTNER_TO_CUST_CC_HOLDER,BD_PARTNER_TO_CUST_CC_NUM,BD_PARTNER_TO_CUST_CC_ISSUER,"
			+ "BD_PARTNER_TO_CUST_CC_EXPDATE,TAX,TAX CURRENCY,FREIGHT,FREIGHTCURRENCY,ITEMID,"
			+ "ITEMQUANTITY,ITEMUOM,ITEMPRICE,ITEMPRICEBASEQUANTITY,ITEMPRICECURRENCY,ITEMCATEGORY,"
			+ "ITEMTAX,ITEMTAXCURRENCY,ITEMFREIGHT,ITEMFREIGHTCURRENCY,ITEMSHIPTOCOMPANY,ITEMSHIPTOSTREET,ITEMSHIPTOHOUSENO,"
			+ "ITEMSHIPTOCITY,ITEMSHIPTOCOUNTRY,ITEMSHIPTOPOSTALCODE,ITEMSHIPTOTELEPHONE,ITEMSHIPTOEMAIL";

	private final String[] HEADINGKEYS =
		new String[] {
			"b2b.ordrdwnld.csv.ORDERID",
			"b2b.ordrdwnld.csv.ORDERDATE",
			"b2b.ordrdwnld.csv.LANGUAGE",
			"b2b.ordrdwnld.csv.CURRENCY",
			"b2b.ordrdwnld.csv.VENID",
			"b2b.ordrdwnld.csv.VENCOMP",
			"b2b.ordrdwnld.csv.VENSTRT",
			"b2b.ordrdwnld.csv.VENHOUS",
			"b2b.ordrdwnld.csv.VENCTY",
			"b2b.ordrdwnld.csv.VENCTRY",
			"b2b.ordrdwnld.csv.VENPCODE",
			"b2b.ordrdwnld.csv.VENTEL",
			"b2b.ordrdwnld.csv.VENEMAIL",
			"b2b.ordrdwnld.csv.OP_ID",
			"b2b.ordrdwnld.csv.OP_COMPANY",
			"b2b.ordrdwnld.csv.OP_STREET",
			"b2b.ordrdwnld.csv.OP_HOUSENUM",
			"b2b.ordrdwnld.csv.OP_CITY",
			"b2b.ordrdwnld.csv.OP_COUNTRY",
			"b2b.ordrdwnld.csv.OP_POSTALCODE",
			"b2b.ordrdwnld.csv.OP_TELEPHONE",
			"b2b.ordrdwnld.csv.OP_EMAIL",
			"b2b.ordrdwnld.csv.BP_ID",
			"b2b.ordrdwnld.csv.BP_COMPANY",
			"b2b.ordrdwnld.csv.BP_STREET",
			"b2b.ordrdwnld.csv.BP_HOUSENUM",
			"b2b.ordrdwnld.csv.BP_CITY",
			"b2b.ordrdwnld.csv.BP_COUNTRY",
			"b2b.ordrdwnld.csv.BP_POSTALCODE",
			"b2b.ordrdwnld.csv.BP_TELEPHONE",
			"b2b.ordrdwnld.csv.BP_EMAIL",
			"b2b.ordrdwnld.csv.SP_ID",
			"b2b.ordrdwnld.csv.SP_COMPANY",
			"b2b.ordrdwnld.csv.SP_STREET",
			"b2b.ordrdwnld.csv.SP_HOUSENUM",
			"b2b.ordrdwnld.csv.SP_CITY",
			"b2b.ordrdwnld.csv.SP_COUNTRY",
			"b2b.ordrdwnld.csv.SP_POSTALCODE",
			"b2b.ordrdwnld.csv.SP_TELEPHONE",
			"b2b.ordrdwnld.csv.SP_EMAIL",
			"b2b.ordrdwnld.csv.BD_AMOUNT",
			"b2b.ordrdwnld.csv.BD_CURRENCY",
			"b2b.ordrdwnld.csv.BDPYMTYPE",
			"b2b.ordrdwnld.csv.BD_CC_HOLDER",
			"b2b.ordrdwnld.csv.BD__CC_NUM",
			"b2b.ordrdwnld.csv.BD_CC_ISSUER",
			"b2b.ordrdwnld.csv.BD_CC_EXPDATE",
			"b2b.ordrdwnld.csv.TAX",
			"b2b.ordrdwnld.csv.TAXCURRENCY",
			"b2b.ordrdwnld.csv.FREIGHT",
			"b2b.ordrdwnld.csv.FREIGHTCURR",
			"b2b.ordrdwnld.csv.ITEMID",
			"b2b.ordrdwnld.csv.ITEMQUANTITY",
			"b2b.ordrdwnld.csv.ITEMUOM",
			"b2b.ordrdwnld.csv.ITEMPRICE",
			"b2b.orddnld.csv.ITMPRICEBASEQTY",
			"b2b.ordrdwnld.csv.ITEMPRICECURR",
			"b2b.ordrdwnld.csv.ITEMCATEGORY",
			"b2b.ordrdwnld.csv.ITEMTAX",
			"b2b.ordrdwnld.csv.ITEMTAXCURR",
			"b2b.ordrdwnld.csv.ITEMFREIGHT",
			"b2b.ordrdwnld.csv.ITMFREIGHTCUR",
			"b2b.ordrdwnld.csv.ITMSHIPTOCOMP",
			"b2b.ordrdwnld.csv.ITEMSHIPTOSTR",
			"b2b.ordrdnld.csv.ITEMSHPTOHOUSE",
			"b2b.ordrdwnld.csv.ITEMSHIPTOCTY",
			"b2b.ordrdnld.csv.ITEMSHIPTOCTRY",
			"b2b.ordrdnld.csv.ITMSHPPOSTCODE",
			"b2b.ordrdwnld.csv.ITEMSHIPTOTEL",
			"b2b.ordrdwnld.csv.ITEMSHIPTOEML" };

	/**
	   * Call this stateless method by passing the relevant order object.
	   * @param order the order object.The type is currently set to Object to allow
	   *                  flexibility to pass orders of differnt types.
	   * @returns String the converted order object into the relevant format as a string
	   *
	  */

	public String convert(BusinessObjectManager bom, OrderStatus order) {
		return convert(bom, order, Locale.getDefault());
	}

	public String convert(
		BusinessObjectManager bom,
		OrderStatus order,
		Locale locale) {
		if (order == null)
			return null;
		StringBuffer csvStr =
			new StringBuffer(locale == null ? HEADING : getHeader(locale));
		csvStr.append(NEWLINE);
		csvStr.append(convertWithoutHeading(bom, order, locale));
		return csvStr.toString();
	}

	public void convert(
		BusinessObjectManager bom,
		OrderStatus order,
		OrderDownloadStatus status,
		Locale locale) {
		String str = convert(bom, order, locale);
		if (str == null) {
			status.setFailedOrder(order.getOrderHeader().getSalesDocNumber());
			return;
		}
		status.setSuccessfulOrder(order.getOrderHeader().getSalesDocNumber());
		status.setGenDoc(str);
	}

	/**
	 * Utility method to convert the header to the locale specific ones
	 * if available using the resource keys
	 */
	private String getHeader(Locale locale) {
		StringBuffer hdrStr = new StringBuffer("");
		for (int i = 0; i < HEADINGKEYS.length; i++) {
			hdrStr.append(
				DBLQUOTE
					+ WebUtil.translate(locale, HEADINGKEYS[i], null)
					+ DBLQUOTE);
			if (i < HEADINGKEYS.length - 1)
				hdrStr.append(COMMA);
		}
		return hdrStr.toString();
	}

	String convertWithoutHeading(
		BusinessObjectManager bom,
		OrderStatus order,
		Locale locale) {

		StringBuffer csvStr = new StringBuffer("");
		try {
			HeaderData hdrSalesDoc = order.getOrderHeader();
			csvStr.append(
				trimOrReplaceNullWithEmptyString(
					hdrSalesDoc.getSalesDocNumber()));
			csvStr.append(COMMA);
			csvStr.append(
				trimOrReplaceNullWithEmptyString(hdrSalesDoc.getChangedAt()));
			csvStr.append(COMMA);
			if (locale != null) {
				csvStr.append(
					trimOrReplaceNullWithEmptyString(locale.getLanguage()));
			}
			//          else
			//            csvStr.append(replaceNullWithEmptyString("en"));
			csvStr.append(COMMA);
			csvStr.append(
				trimOrReplaceNullWithEmptyString(hdrSalesDoc.getCurrency()));
			csvStr.append(COMMA);

			/**
			 * Vendor details?How to obtain it.so passing null ordering party and
			 * bill to party are one and the same in the ISA scenario
			 */
			csvStr.append(
				trimOrReplaceNullWithEmptyString(
					hdrSalesDoc.getPartnerListData().getSoldFromData() == null
						? null
						: hdrSalesDoc
							.getPartnerListData()
							.getSoldFromData()
							.getPartnerId()));
			csvStr.append(COMMA);
			if (bom != null
				&& hdrSalesDoc != null
				&& hdrSalesDoc.getPartnerListData() != null
				&& hdrSalesDoc.getPartnerListData().getSoldFromData() != null) {
				log.debug(
					"About to retrieve the business partner for in the CSV"
						+ hdrSalesDoc
							.getPartnerListData()
							.getSoldFromData()
							.getPartnerId());
				BusinessPartnerManager bupMa = bom.createBUPAManager();
				BusinessPartner bp =
					bupMa.getBusinessPartner(
						hdrSalesDoc
							.getPartnerListData()
							.getSoldFromData()
							.getPartnerTechKey());
				if (bp != null) {
					addAddressData(csvStr, bp.getAddress());
				}
			} else {
				log.debug("Sold from data is null");
				addAddressData(csvStr, null);
			}

			/**
			 * Ordering party details?How to obtain it.so passing null ordering party and
			 * bill to party are one and the same in the ISA scenario
			 */
			csvStr.append(
				trimOrReplaceNullWithEmptyString(
					hdrSalesDoc.getPartnerListData().getSoldToData() == null
						? null
						: hdrSalesDoc
							.getPartnerListData()
							.getSoldToData()
							.getPartnerId()));
			csvStr.append(COMMA);
			if (bom != null
				&& hdrSalesDoc != null
				&& hdrSalesDoc.getPartnerListData() != null
				&& hdrSalesDoc.getPartnerListData().getSoldToData() != null) {
				BusinessPartnerManager bupMa = bom.createBUPAManager();
				BusinessPartner bp =
					bupMa.getBusinessPartner(
						hdrSalesDoc
							.getPartnerListData()
							.getSoldToData()
							.getPartnerTechKey());
				if (bp != null) {
					addAddressData(csvStr, bp.getAddress());
				}
			} else {
				addAddressData(csvStr, null);
			}

			/**
			 * Billing party details?How to obtain it.so passing null ordering party and
			 * bill to party are one and the same in the ISA scenario
			 */
			csvStr.append(
				trimOrReplaceNullWithEmptyString(
					hdrSalesDoc.getPartnerListData().getSoldToData() == null
						? null
						: hdrSalesDoc
							.getPartnerListData()
							.getSoldToData()
							.getPartnerId()));
			csvStr.append(COMMA);
			if (bom != null
				&& hdrSalesDoc != null
				&& hdrSalesDoc.getPartnerListData() != null
				&& hdrSalesDoc.getPartnerListData().getSoldToData() != null) {
				BusinessPartnerManager bupMa = bom.createBUPAManager();
				BusinessPartner bp =
					bupMa.getBusinessPartner(
						hdrSalesDoc
							.getPartnerListData()
							.getSoldToData()
							.getPartnerTechKey());
				if (bp != null) {
					addAddressData(csvStr, bp.getAddress());
				}
			} else {
				addAddressData(csvStr, null);
			}
			/**
			   * ShipTo party details
			  */
			csvStr.append(
				trimOrReplaceNullWithEmptyString(
					hdrSalesDoc == null
						|| hdrSalesDoc.getShipToData() == null
							? null
							: hdrSalesDoc.getShipToData().getId()));
			csvStr.append(COMMA);
			addAddressData(
				csvStr,
				hdrSalesDoc.getShipToData() == null
					? null
					: hdrSalesDoc.getShipToData().getAddressData());

			/**
			 * Billing Details: BO->Partner details.
			 * Right now there is no way to obtain this.So just pass the null as such
			 */
			/**
			 * Billing Details: Partner ->Customer details.
			 * Right now credit card is assumed
			 */
			/*BD_PARTNER_TO_CUST_AMOUNT,BD_PARTNER_TO_CUST_CURRENCY,"+
			"BD_PARTNER_TO_CUST_CC_HOLDER,BD_PARTNER_TO_CUST_CC_NUM,BD_PARTNER_TO_CUST_CC_ISSUER,"+
			"BD_PARTNER_TO_CUST_CC_EXPDATE*/
			/**
			 * Currently passing the gross value as the total amount to be charged for the credit card
			 */
			csvStr.append(
				DBLQUOTE
					+ trimOrReplaceNullWithEmptyString(
						hdrSalesDoc.getGrossValue())
					+ DBLQUOTE);
			csvStr.append(COMMA);
			csvStr.append(
				trimOrReplaceNullWithEmptyString(hdrSalesDoc.getCurrency()));
			csvStr.append(COMMA);
			/**
			 * PaymentType
			 */
/*@TODO d034021
 * check payment download!!!!
 */			
			String paymentKey = null;
            
			if (hdrSalesDoc.getPaymentData() != null) {
				List paymentMethods = hdrSalesDoc.getPaymentData().getPaymentMethods();
				if (paymentMethods != null && paymentMethods.size() > 0) {				
					Iterator iter = paymentMethods.iterator();
					while (iter.hasNext()){					
						PaymentMethod payMethod = (PaymentMethod) iter.next();			
						if (payMethod != null){
						// if the payment method is Credit card, fill the import table accordingly
					   	 TechKey payTypeKey = payMethod.getPayTypeTechKey();
							if (PaymentBaseType.INVOICE_TECHKEY.equals(payTypeKey)) {
								paymentKey = "b2b.payment.invoice";
							} else if (PaymentBaseType.COD_TECHKEY.equals(payTypeKey)) {
								paymentKey = "b2b.payment.cod";
							} else if (PaymentBaseType.CARD_TECHKEY.equals(payTypeKey)) {
								paymentKey = "b2b.payment.card";
							}
							csvStr.append(
								DBLQUOTE
									+ trimOrReplaceNullWithEmptyString(
										WebUtil.translate(locale, paymentKey, null))
									+ DBLQUOTE);
							csvStr.append(COMMA);						
							if(payMethod.getPayTypeTechKey().equals(PaymentBaseTypeData.CARD_TECHKEY )) {							                    		
								PaymentCCard payCCard = (PaymentCCard) payMethod;
								csvStr.append(
									trimOrReplaceNullWithEmptyString(
									(payCCard.getHolder() == null)
											? null
											: payCCard.getHolder()));
								csvStr.append(COMMA);
								csvStr.append(
									trimOrReplaceNullWithEmptyString(
									(payCCard.getNumber() == null)
											? null
											: payCCard.getNumber()));
								csvStr.append(COMMA);	
								csvStr.append(
									trimOrReplaceNullWithEmptyString(
									(hdrSalesDoc.getPaymentData() == null) ? null : null));
									csvStr.append(COMMA);
									String expDate =
									(hdrSalesDoc.getPaymentData() == null)
											? null
											: payCCard.getExpDateMonth()
											+ FWSLASH
											+ payCCard.getExpDateYear();
								csvStr.append(trimOrReplaceNullWithEmptyString(expDate));
								csvStr.append(COMMA);																				
							}
						}
					}							
				}
			}

			csvStr.append(
				DBLQUOTE
					+ trimOrReplaceNullWithEmptyString(hdrSalesDoc.getTaxValue())
					+ DBLQUOTE);
			csvStr.append(COMMA);
			csvStr.append(
				trimOrReplaceNullWithEmptyString(hdrSalesDoc.getCurrency()));
			csvStr.append(COMMA);
			csvStr.append(
				DBLQUOTE
					+ trimOrReplaceNullWithEmptyString(
						hdrSalesDoc.getFreightValue())
					+ DBLQUOTE);
			csvStr.append(COMMA);
			csvStr.append(
				trimOrReplaceNullWithEmptyString(hdrSalesDoc.getCurrency()));
			csvStr.append(COMMA);

			/**
			 * Item details being added now
			 */
			/**  "BD_PARTNER_TO_CUST_CC_EXPDATE,TAX,TAX CURRENCY,FREIGHT,FREIGHTCURRENCY,ITEMID,"+
			  "ITEMQUANTITY,ITEMUOM,ITEMPRICE,ITEMPRICEBASEQUANTITY,ITEMPRICECURRENCY,ITEMCATEGORY,"+
			  "ITEMTAX,ITEMTAXCURRENCY,ITEMFREIGHT,ITEMFREIGHTCURRENCY,ITEMSHIPTOSTREET,ITEMSHIPTOHOUSENO,"+
			  "ITEMSHIPTOCITY,ITEMSHIPTOCOUNTRY,ITEMSHIPTOPOSTALCODE";
			  */

			ItemListData itemList = order.getItemListData();
			for (int i = 0; i < itemList.size(); i++) {
				if (itemList.getItemData(i) != null) {
					if (i > 0) {
						csvStr.append(NEWLINE);
						csvStr.append(HDR_ITEM_SEPARATOR_STRING);
					}
					csvStr.append(
						trimOrReplaceNullWithEmptyString(
							itemList.getItemData(i).getProduct()));
					csvStr.append(COMMA);
					//                  csvStr.append(trimOrReplaceNullWithEmptyString(itemList.getItemData(i).getReqDeliveryDate()));
					//                  csvStr.append(COMMA);
					csvStr.append(
						trimOrReplaceNullWithEmptyString(
							itemList.getItemData(i).getQuantity()));
					csvStr.append(COMMA);
					csvStr.append(
						trimOrReplaceNullWithEmptyString(
							itemList.getItemData(i).getUnit()));
					csvStr.append(COMMA);
					csvStr.append(
						DBLQUOTE
							+ trimOrReplaceNullWithEmptyString(
								itemList.getItemData(i).getNetPrice())
							+ DBLQUOTE);
					csvStr.append(COMMA);
					csvStr.append(
						trimOrReplaceNullWithEmptyString(
							itemList.getItemData(i).getNetPriceUnit()));
					csvStr.append(COMMA);
					csvStr.append(
						trimOrReplaceNullWithEmptyString(
							itemList.getItemData(i).getCurrency()));
					csvStr.append(COMMA);
					csvStr.append(
						trimOrReplaceNullWithEmptyString(
							itemList.getItemData(i).getPcatArea()));
					csvStr.append(COMMA);
					csvStr.append(
						DBLQUOTE
							+ trimOrReplaceNullWithEmptyString(
								itemList.getItemData(i).getTaxValue())
							+ DBLQUOTE);
					csvStr.append(COMMA);
					csvStr.append(
						trimOrReplaceNullWithEmptyString(
							itemList.getItemData(i).getCurrency()));
					csvStr.append(COMMA);
					csvStr.append(
						DBLQUOTE
							+ trimOrReplaceNullWithEmptyString(
								itemList.getItemData(i).getFreightValue())
							+ DBLQUOTE);
					csvStr.append(COMMA);
					csvStr.append(
						trimOrReplaceNullWithEmptyString(
							itemList.getItemData(i).getCurrency()));
					csvStr.append(COMMA);
					addAddressData(
						csvStr,
						itemList.getItemData(i).getShipToData() == null
							? null
							: itemList
								.getItemData(i)
								.getShipToData()
								.getAddressData());
				}
			}
		} catch (Exception e) {
			log.warn(LogUtil.APPS_USER_INTERFACE, "An error occured in the csv conversion of order ", e);
		}
		return csvStr.toString();
	}
	private String trimOrReplaceNullWithEmptyString(String str) {
		if (str == null)
			return "";
		return str.trim();
	}

	/**
	 * Adds the address data into the csv string
	 * reduces the dependencies in the code for the address data to this only one place
	 */
	private void addAddressData(StringBuffer csvStr, AddressData addr) {
		if (addr != null
			&& addr.getName1() != null
			&& !addr.getName1().equals("")) {
			csvStr.append(
				trimOrReplaceNullWithEmptyString(
					addr == null ? null : addr.getName1()));
			if (addr.getName2() != null)
				csvStr.append(" " + addr.getName2());
		} else
			csvStr.append(
				trimOrReplaceNullWithEmptyString(
					addr == null
						? null
						: addr.getFirstName() + " " + addr.getLastName()));
		csvStr.append(COMMA);
		csvStr.append(
			trimOrReplaceNullWithEmptyString(
				addr == null ? null : addr.getStreet()));
		csvStr.append(COMMA);
		csvStr.append(
			trimOrReplaceNullWithEmptyString(
				addr == null ? null : addr.getHouseNo()));
		csvStr.append(COMMA);
		csvStr.append(
			trimOrReplaceNullWithEmptyString(
				addr == null ? null : addr.getCity()));
		csvStr.append(COMMA);
		csvStr.append(
			trimOrReplaceNullWithEmptyString(
				addr == null ? null : addr.getCountry()));
		csvStr.append(COMMA);
		csvStr.append(
			trimOrReplaceNullWithEmptyString(
				addr == null ? null : addr.getPostlCod1()));
		csvStr.append(COMMA);
		csvStr.append(
			trimOrReplaceNullWithEmptyString(
				addr == null ? null : addr.getTel1Numbr()));
		csvStr.append(COMMA);
		csvStr.append(
			trimOrReplaceNullWithEmptyString(
				addr == null ? null : addr.getEMail()));
		csvStr.append(COMMA);
	}

	/**
	 * Call this for mass download of orders.
	 * @param bom the business object manager in case the order conversion happens in the
	 * backend or need to some more details.
	 * @param orderGuids the orderguids in order
	 * @param orderids the array of the corresponding orderids in line with the orderguids
	 */
	public void convert(
		BusinessObjectManager bom,
		ArrayList orderGuids,
		ArrayList orderIds,
		OrderDownloadStatus status,
		Locale locale) {
		int size = -1;
		if (orderGuids != null)
			size = orderGuids.size();
		if (orderGuids != null)
			size = orderIds.size();
		String orderGuid = null;
		String orderId = null;
		OrderStatus order = null;
		//        OrderStatus order = null;
		StringBuffer csvStr = new StringBuffer("");
		for (int i = 0; i < size; i++) {
			if (orderGuids != null)
				orderGuid = (String) orderGuids.get(i);
			if (orderIds != null)
				orderId = (String) orderIds.get(i);
			order = fetchOrderStatus(orderId, orderGuid, bom);
			if (order == null) {
				status.setFailedOrder(orderId);
				continue;
			}
			String str = null;
			if (i < 1) {
				str = convert(bom, order, locale);
				if (str != null) {
					csvStr.append(str);
					status.setSuccessfulOrder(orderId);
				} else
					status.setFailedOrder(orderId);
			} else {
				str = convertWithoutHeading(bom, order, locale);
				if (str != null) {
					status.setSuccessfulOrder(orderId);
					//                    csvStr.append(str);
					csvStr.append(NEWLINE + str);

				} else
					status.setFailedOrder(orderId);
			}
		}
		status.setGenDoc(csvStr.toString());
	}

	public void convert(
		BusinessObjectManager bom,
		String orderGuid,
		String orderid,
		OrderDownloadStatus status,
		Locale locale) {
		OrderStatus order = null;
		//        OrderStatus order = null;
		order = fetchOrderStatus(orderid, orderGuid, bom);
		if (order == null) {
			status.setFailedOrder(orderid);
			return;
		}
		convert(bom, order, status, locale);
	}
}

/**
 * @todo has to replace the creation of the XML elements with the template xml file.
 * Currently generates the XML using the builder approach.
 */
class OrderToXMLConverter extends OrderConversionManager {
	OrderToXMLConverter() {
	}
	private static final String XML_HEADER =
		"<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
	private static final String END_ORDERS_ELEMENT = "</Orders>";
	protected static IsaLocation log =
		IsaLocation.getInstance(OrderToXMLConverter.class.getName());
	protected static final String DEFAULTSCHEMA =
		"\"http://www.w3.org/2001/XMLSchema-instance\"";
	protected static final String ORDERSCHEMANAME = "\"Order.xsd\"";
	protected static final String ORDERSSCHEMANAME = "\"Orders.xsd\"";

	/**
	 * Call this stateless method by passing the relevant order object.
	 * @param order the order object.The type is currently set to Object to allow
	 *                  flexibility to pass orders of differnt types.
	 * @returns String the converted order object into the relevant format as a string
	 */

	public void convert(
		BusinessObjectManager bom,
		OrderStatus order,
		OrderDownloadStatus status,
		Locale locale) {
		String str = convert(bom, order, locale);
		//        /**
		//         * For the lack of better api support manually insert the namespace
		//         * specific string into the generated string
		//         */
		//        int index = str.indexOf("<Order>");
		//        if(index !=-1) {
		//            String temp = str.substring(index+"<Order>".length());
		//            /**
		//             * Append the namespace specific string now
		//             */
		//            String nameSpaceAwareString = "<Order  xmlns:xsi=" + DEFAULTSCHEMA +
		//                " xsi:noNamespaceSchemaLocation= " +  ORDERSCHEMANAME + ">";
		//            str = nameSpaceAwareString + temp;
		//        }
		if (str == null) {
			status.setFailedOrder(order.getOrderHeader().getSalesDocNumber());
			return;
		}
		status.setSuccessfulOrder(order.getOrderHeader().getSalesDocNumber());
		// Append the xml header
		str = XML_HEADER + str;
		status.setGenDoc(str);
	}

	public void convert(
		BusinessObjectManager bom,
		String orderguid,
		String orderid,
		OrderDownloadStatus status,
		Locale locale) {
		OrderStatus order = fetchOrderStatus(orderid, orderguid, bom);
		convert(bom, order, status, locale);

	}

	/**
	 * Call this stateless method by passing the relevant order object.
	 * @param order the order object.The type is currently set to Object to allow
	 *                  flexibility to pass orders of differnt types.
	 * @returns String the converted order object into the relevant format as a string
	 */
	public String convert(
		BusinessObjectManager bom,
		OrderStatus order,
		Locale locale) {
		/**
		 * ID,date,language,currency,vendor,orderingparty,shiptoparty,shiptoAddress,
		 */
		/**
		 * document is the root.Start from here
		 */
		try {
			if (order == null)
				return null;
			Shop shop = null;
			if (bom != null)
				shop = bom.getShop();

			HeaderData hdrSalesDoc = order.getOrderHeader();
			//order.getOrderHeader();
			Document doc =
				DocumentBuilderFactory
					.newInstance()
					.newDocumentBuilder()
					.newDocument();
			Element root = createElement(doc, doc, "Order");

			Element orderHdr = createElement(doc, root, "OrderHeader");

			Element tempElem = null;

			tempElem = createElement(doc, orderHdr, "OrderID");
			addElementValue(doc, tempElem, hdrSalesDoc.getSalesDocNumber());
			/**
			 * Doing some formatting to fit the created date into the order
			 * screen
			 */
			if (hdrSalesDoc.getCreatedAt() != null) {
				String str = null;
				//                try {
				tempElem = createElement(doc, orderHdr, "OrderDate");
				//                    SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
				// TODO May need to apply the time zone information
				//                    Date date = sdf.parse(hdrSalesDoc.getCreatedAt());
				//                    sdf = new SimpleDateFormat("yyyy-MM-dd");
				str =
					formatDate(
						hdrSalesDoc.getCreatedAt(),
						null,
						shop.getDateFormat());
				//                }
				//                catch(ParseException pe ) {
				//                    str = hdrSalesDoc.getCreatedAt();
				//                }
				addElementValue(doc, tempElem, str);
			}
			/**
			 * @todo currently hard coded needed to find the language of the order
			 * Language value not found
			 */
			tempElem = createElement(doc, orderHdr, "Language");
			if (locale != null) {
				//            csvStr.append(replaceNullWithEmptyString(bom.getShop().getLanguage()));
				addElementValue(doc, tempElem, locale.getLanguage());
			}
			//          else
			//              addElementValue(doc,tempElem,"");

			tempElem = createElement(doc, orderHdr, "Currency");
			addElementValue(doc, tempElem, hdrSalesDoc.getCurrency());

			Element tradingPartnersElem =
				createElement(doc, orderHdr, "TradingPartners");
			//addElementValue(doc,tempElem,hdrSalesDoc.getCurrency());
			/**
			 * Vendor here in the trading parter means the one who is supplying
			 * the material. Here in this case, it should be the self information
			 * of the reseller.??
			 *
			 */

			Element orderingParty =
				createElement(doc, tradingPartnersElem, "OrderingParty");
			Element shipToParty =
				createElement(doc, tradingPartnersElem, "ShipToParty");
			Element billToParty =
				createElement(doc, tradingPartnersElem, "BillToParty");

			/**
			 * Ordering party details Currently contained in the SoldTo Data
			 */
			Element identifier =
				createElement(doc, orderingParty, "Identifier");
			Element partner = createElement(doc, identifier, "PartnerID");
			Element billToPartner =
				createElement(
					doc,
					createElement(doc, billToParty, "Identifier"),
					"PartnerID");
			if (hdrSalesDoc != null)
				if (hdrSalesDoc.getPartnerListData().getSoldToData() != null) {
					addElementValue(
						doc,
						partner,
						hdrSalesDoc
							.getPartnerListData()
							.getSoldToData()
							.getPartnerId());
					addElementValue(
						doc,
						billToPartner,
						hdrSalesDoc
							.getPartnerListData()
							.getSoldToData()
							.getPartnerId());

				}
			Element addr = createElement(doc, orderingParty, "Address");
			Element billToPartyaddr =
				createElement(doc, billToParty, "Address");
			/**
			 * ?? Value not found
			 */
			if (bom != null
				&& hdrSalesDoc != null
				&& hdrSalesDoc.getPartnerListData() != null
				&& hdrSalesDoc.getPartnerListData().getSoldToData() != null) {
				BusinessPartnerManager bupMa = bom.createBUPAManager();
				BusinessPartner bp =
					bupMa.getBusinessPartner(
						hdrSalesDoc
							.getPartnerListData()
							.getSoldToData()
							.getPartnerTechKey());
				if (bp != null) {
					addAddressData(doc, addr, bp.getAddress());
					addAddressData(doc, billToPartyaddr, bp.getAddress());
				}
			} else {
				addAddressData(doc, addr, null);
				addAddressData(doc, billToPartyaddr, null);
			}

			if (hdrSalesDoc != null
				&& hdrSalesDoc.getPartnerListData().getSoldFromData() != null) {
				/**
				 * Vendor information
				 */
				Element vendor =
					createElement(doc, tradingPartnersElem, "Vendor");
				identifier = createElement(doc, vendor, "Identifier");
				partner = createElement(doc, identifier, "PartnerID");
				addElementValue(
					doc,
					partner,
					hdrSalesDoc
						.getPartnerListData()
						.getSoldFromData()
						.getPartnerId());

				addr = createElement(doc, vendor, "Address");
				/**
				 * ?? Value not found
				 */
				BusinessPartner bp =
					bom.getBUPAManager().getBusinessPartner(
						hdrSalesDoc
							.getPartnerListData()
							.getSoldFromData()
							.getPartnerTechKey());
				if (bp != null && bp.getAddress() != null) {
					addAddressData(doc, addr, bp.getAddress());
				} else
					addAddressData(doc, addr, null);
			}

			/**
			 * ShipTo Party information
			 */
			/**
			 * shipToParty Here this is supposed to contain the customers information
			 * who has purchased at the reseller.
			 */
			identifier = createElement(doc, shipToParty, "Identifier");
			partner = createElement(doc, identifier, "PartnerID");
			if (hdrSalesDoc != null && hdrSalesDoc.getShipToData() != null) {
				addElementValue(
					doc,
					partner,
					hdrSalesDoc.getShipToData().getId());
			}

			addr = createElement(doc, shipToParty, "Address");
			/**
			 * ?? Value not found
			 */

			if (hdrSalesDoc != null
				&& hdrSalesDoc.getShipToData() != null
				&& hdrSalesDoc.getShipToData().getAddressData() != null) {
				addAddressData(
					doc,
					addr,
					hdrSalesDoc.getShipToData().getAddressData());
			} else
				addAddressData(doc, addr, null);
			/**
			 * Description
			 */
			addElementValue(
				doc,
				createElement(doc, orderHdr, "Description"),
				hdrSalesDoc.getDescription());
			/**
			 * Text Element
			 */
			addTextElement(locale,doc,hdrSalesDoc.getText(),orderHdr);

			/**
			 * Procurement card to be changed as such
			 * BuyerToPartnerPaymentInstructions.BillToParty information
			 *
			 */
			Element billingDetails =
				createElement(doc, orderHdr, "BillingDetails");

			Element billingAmt =
				createElement(doc, billingDetails, "BillingAmount");

			Element billAmtVal = createElement(doc, billingAmt, "Value");
			addElementValue(
				doc,
				billAmtVal,
				formatNumber(
					hdrSalesDoc.getGrossValue(),
					shop.getGroupingSeparator().charAt(0),
					shop.getDecimalSeparator().charAt(0)));

			Element billAmtCurr = createElement(doc, billingAmt, "Currency");
			String currencyStr = null;
			try {
				CurrencyConverter currencyConverter =
					bom.getCurrencyConverter();
				if (currencyConverter == null)
					currencyConverter = bom.createCurrencyConverter();
				currencyStr =
					currencyConverter.convertCurrency(
						true,
						hdrSalesDoc.getCurrency());
				if (currencyStr == null || currencyStr.length() < 1)
					currencyStr = hdrSalesDoc.getCurrency();
			} catch (Exception e) {
				currencyStr = hdrSalesDoc.getCurrency();
				log.debug(e);
			}
			addElementValue(doc, billAmtCurr, currencyStr);

			Element p2c =
				createElement(
					doc,
					billingDetails,
					"PartnerToCustomerBillingDetails");
			/*<ProcurementCard>
			<IssueCompany>Stri</IssueCompany>
			<Number>String</Number>
			<HolderName>String</HolderName>
			<ValidityDate>1967-08*/
			/**
			 *@todo credit card exp day not found in the api
			 * so hardcdoed to 1.Needs to be changed later
			 */
/*TODO d034021
 * check payment
 */			
			if (hdrSalesDoc.getPaymentData() != null) {
				List paymentMethods = hdrSalesDoc.getPaymentData().getPaymentMethods();
				if (paymentMethods != null && paymentMethods.size() > 0) { 				
					Iterator iter = paymentMethods.iterator();
					while (iter.hasNext()){					
						PaymentMethod payMethod = (PaymentMethod) iter.next();			
						if (payMethod != null){
						// if the payment method is Credit card, fill the import table accordingly
						//if (hdrSalesDoc.getPaymentData() != null) {
							Element payTypeElem = createElement(doc, p2c, "PaymentType");
							TechKey payTypeKey = payMethod.getPayTypeTechKey();
							String paymentType = null;
							if (payTypeKey.equals(PaymentBaseType.INVOICE_TECHKEY)) {
								paymentType = "Invoice";
							} else if (payTypeKey.equals(PaymentBaseType.COD_TECHKEY)) {
								paymentType = "Cod";
							} else if (payTypeKey.equals(PaymentBaseType.CARD_TECHKEY)) {
								paymentType = "CreditCard";
							}
							addElementValue(doc, payTypeElem, paymentType);
							if (payTypeKey.equals(PaymentBaseType.CARD_TECHKEY)) {
								Element ccard = createElement(doc, p2c, "CreditCard");
					/**
					 * @todo the card type result data to be read from the order or the sold from depending
					 * upon the hom scenario
					 */
					//                  ResultData listOfCardInst =
					//                payment.determineCreditCardInstituteName(listOfCardInst);
								addElementValue(
									doc,
									createElement(doc, ccard, "IssueCompany"),
									"");
								Element creditCardNum = createElement(doc, ccard, "Number");
								Element creditCardHolderName =
									createElement(doc, ccard, "HolderName");
								Element creditCardExpDate =
									createElement(doc, ccard, "ExpirationDate");
								String number = ((PaymentCCard) payMethod).getNumber();
								String holder = ((PaymentCCard) payMethod).getHolder();
								String year = ((PaymentCCard) payMethod).getExpDateYear();	
								String month = ((PaymentCCard) payMethod).getExpDateMonth();
								addElementValue(
									doc,
									creditCardNum,
									number);
								addElementValue(
									doc,
									createElement(doc, ccard, "HolderName"),
									holder);
								addElementValue(
									doc,
									creditCardExpDate,
									year
								+ "-"
								+ month);
							}
						}
					}
				}
			}

			/**
			 * Has to  decide upon this
			 */

			//order.
			/**
			 * Freight value
			 */
			//order.
			/**
			 * Freight value
			 */
			Element hdrfreight = createElement(doc, orderHdr, "Freight");
			Element hdrfreightVal = createElement(doc, hdrfreight, "Value");
			Element hdrfreightCurr = createElement(doc, hdrfreight, "Currency");

			addElementValue(
				doc,
				hdrfreightVal,
				formatNumber(
					hdrSalesDoc.getFreightValue(),
					shop.getGroupingSeparator().charAt(0),
					shop.getDecimalSeparator().charAt(0)));
			addElementValue(doc, hdrfreightCurr, currencyStr);

			Element hdrTax = createElement(doc, orderHdr, "Tax");
			Element hdrTaxVal = createElement(doc, hdrTax, "Value");
			Element hdrTaxCurr = createElement(doc, hdrTax, "Currency");
			/**
			 * @todo assuming the grouping separator and decimal separator are of 1 char wide
			 *
			 */
			addElementValue(
				doc,
				hdrTaxVal,
				formatNumber(
					hdrSalesDoc.getTaxValue(),
					shop.getGroupingSeparator().charAt(0),
					shop.getDecimalSeparator().charAt(0)));
			addElementValue(doc, hdrTaxCurr, currencyStr);
			ItemListData itemList = order.getItemListData();
			for (int i = 0; i < itemList.size(); i++) {
				Element orderItem = createElement(doc, root, "OrderItem");
				if (itemList.getItemData(i) != null) {
					addElementValue(
						doc,
						createElement(doc, orderItem, "OrderItemID"),
						itemList.getItemData(i).getNumberInt());

					/**
					 * Order line item level;category,currency,price,basisquantity
					 */
					//Hopefully the parentid of the product
					if (itemList.getItemData(i).getConfirmedDeliveryDate()
						!= null) {
						if (!itemList
							.getItemData(i)
							.getConfirmedDeliveryDate()
							.trim()
							.equals(""))
							addElementValue(
								doc,
								createElement(doc, orderItem, "DeliveryDate"),
								formatDate(
									itemList
										.getItemData(i)
										.getConfirmedDeliveryDate(),
									null,
									shop.getDateFormat()));

					}

					/*<Quantity>
					<Value>0</Value>
					<UoM>Str</UoM>
					</Quantity>*/
					Element quantity =
						createElement(doc, orderItem, "Quantity");
					addElementValue(
						doc,
						createElement(doc, quantity, "Value"),
						formatNumber(
							itemList.getItemData(i).getQuantity(),
							shop.getGroupingSeparator().charAt(0),
							shop.getDecimalSeparator().charAt(0)));
					/**
					 * Format this UOM value
					 */
					String uomStr = null;
					currencyStr = null;
					if (bom != null) {
						try {
							UOMConverter uomConv = bom.getUOMConverter();
							if (uomConv == null)
								uomConv = bom.createUOMConverter();
							uomStr =
								uomConv.convertUOM(
									true,
									itemList.getItemData(i).getUnit());
							if (uomStr == null || uomStr.length() < 1)
								uomStr = itemList.getItemData(i).getUnit();
						} catch (Exception e) {
							uomStr = itemList.getItemData(i).getUnit();
						}
						try {
							CurrencyConverter currencyConverter =
								bom.getCurrencyConverter();
							if (currencyConverter == null)
								currencyConverter =
									bom.createCurrencyConverter();
							currencyStr =
								currencyConverter.convertCurrency(
									true,
									itemList.getItemData(i).getCurrency());
							if (currencyStr == null
								|| currencyStr.length() < 1)
								currencyStr =
									itemList.getItemData(i).getCurrency();
						} catch (Exception e) {
							currencyStr = itemList.getItemData(i).getCurrency();
							log.debug(e);
						}

					}
					addElementValue(
						doc,
						createElement(doc, quantity, "UoM"),
						uomStr);

					Element price = createElement(doc, orderItem, "Price");
					addElementValue(
						doc,
						createElement(doc, price, "Value"),
						formatNumber(
							itemList.getItemData(i).getNetPrice(),
							shop.getGroupingSeparator().charAt(0),
							shop.getDecimalSeparator().charAt(0)));

					addElementValue(
						doc,
						createElement(doc, price, "Currency"),
						currencyStr);

					addElementValue(
						doc,
						createElement(doc, price, "PriceBasisQuantity"),
						(itemList.getItemData(i).getNetQuantPriceUnit()
							== null)
							? "1"
							: itemList.getItemData(i).getNetQuantPriceUnit());

					/**
					 *
					 <Product>
					          <Type>Material</Type>
					          <Identifier>
					                  <ProductID>String</ProductID>
					                  <SystemID>String</SystemID>
					                  <ProductIDVendorAssigned>String</ProductIDVendorAssigned>
					                  <ManufacturerID>String</ManufacturerID>
					                  <ProductIDManufacturerAssigned>String</ProductIDManufacturerAssigned>
					                  <GTIN>String</GTIN>
					          </Identifier>
					          <Description>String</Description>
					  </Product>
					
					 */
					Element product = createElement(doc, orderItem, "Product");

					/**
					 * Currently the type of the product is always "material"
					 * @todo so currently  hardcoded as "Material"
					 */
					addElementValue(
						doc,
						createElement(doc, product, "Type"),
						"Material");
					Element identifier1 =
						createElement(doc, product, "Identifier");
					addElementValue(
						doc,
						createElement(doc, identifier1, "ProductID"),
						itemList.getItemData(i).getProduct());

					addElementValue(
						doc,
						createElement(doc, product, "Description"),
						itemList.getItemData(i).getDescription());

					/**
					 * @todo SystemID currently set same as the productid
					 */
					//                    addElementValue(doc,
					//                                    createElement(doc,
					//                                                  identifier1,
					//                                                  "SystemID"),
					//                                    itemList.getItemData(i).getProduct());


					// Text Node
					addTextElement(locale, doc, itemList.getItemData(i).getText(), orderItem);

					/**
					 * item level details,quantity,uom,price
					  * shipTo information and tax level information
					 *
					 */

					Element freight = createElement(doc, orderItem, "Freight");
					Element freightVal = createElement(doc, freight, "Value");
					Element freightCurr =
						createElement(doc, freight, "Currency");

					addElementValue(
						doc,
						freightVal,
						formatNumber(
							itemList.getItemData(i).getFreightValue(),
							shop.getGroupingSeparator().charAt(0),
							shop.getDecimalSeparator().charAt(0)));
					addElementValue(
						doc,
						freightCurr,
						itemList.getItemData(i).getCurrency());

					Element tax = createElement(doc, orderItem, "Tax");
					Element taxVal = createElement(doc, tax, "Value");
					Element taxCurr = createElement(doc, tax, "Currency");
					/**
					 * @todo assuming the grouping separator and decimal separator are of 1 char wide
					 *
					 */
					addElementValue(
						doc,
						taxVal,
						formatNumber(
							itemList.getItemData(i).getTaxValue(),
							shop.getGroupingSeparator().charAt(0),
							shop.getDecimalSeparator().charAt(0)));
					addElementValue(doc, taxCurr, currencyStr);

					/**
					 * Add the shipTo Address information which contains basically
					 * the shipTo as well as the shipToAddress information
					 */
					if (itemList.getItemData(i).getShipToData() != null) {
						Element shipToElem =
							createElement(doc, orderItem, "ShipToParty");
						identifier =
							createElement(doc, shipToElem, "Identifier");
						partner = createElement(doc, identifier, "PartnerID");
						addElementValue(
							doc,
							partner,
							itemList.getItemData(i).getShipToData().getId());

						addr = createElement(doc, shipToElem, "Address");
						/**
						 * ?? Value not found
						 */
						if (itemList.getItemData(i).getShipToData() != null
							&& itemList
								.getItemData(i)
								.getShipToData()
								.getAddressData()
								!= null) {
							addAddressData(
								doc,
								addr,
								itemList
									.getItemData(i)
									.getShipToData()
									.getAddressData());
						} else
							addAddressData(doc, addr, null);
					}

				}
			}
			String str = root.toString();
			if (str != null) {
				String sapLang =
					CodePageUtils.getSapLangForJavaLanguage(
						locale.getLanguage());
				String sapCp = CodePageUtils.getSapCpForSapLanguage(sapLang);
				String srcEncoding =
					CodePageUtils.getJavaEncForSapCpNonUnicode(sapCp);
				// Hardcoded TO Be Removed Later
				String targetEncoding = "UTF-8";
				if (!srcEncoding.equals(targetEncoding) && str != null) {
					try {
						String encodedStr =
							new String(str.getBytes(), srcEncoding);
						str = new String(encodedStr.getBytes(targetEncoding));
					} catch (UnsupportedEncodingException e) {
						log.debug(e.getMessage());
					}
				}
			}
			return str;
		} catch (Exception e) {
			log.warn(LogUtil.APPS_USER_INTERFACE, "An error occurred in xml conversion of order", e);
		}
		return null;
	}

	private void addTextElement(
		Locale locale,
		Document doc,
		TextData textData,
		Element orderItem) {
		if (textData!=null) {
			if (textData.getText() != null
				&& textData.getText().length() > 0) {
				Element textElem =
					createElement(doc, orderItem, "Text");
				Element contentElem =
					createElement(doc, textElem, "Content");
				Element languageElem =
					createElement(doc, textElem, "Language");
				addElementValue(
					doc,
					contentElem,
					textData.getText());
				//TODO temporarily added the locale language
				/// have to confirm that this indeed is the case
				addElementValue(
					doc,
					languageElem,
					locale.getLanguage());
			}
		}
	}

	/**
	 * Utility method to create a element within the parentElement
	 * @param doc the document of the XML
	 * @param parentElm the parent element
	 * @param elementName the element name of the child to be created
	 */
	private Element createElement(
		Document doc,
		Node parentElm,
		String elementName) {
		Element newElem = doc.createElement(elementName);
		parentElm.appendChild(newElem);
		return newElem;
	}
	/**
	 * Utility method to add value to an element.Basically creates a text node
	 * within the element
	 * @param doc the document of the XML
	 * @param elem the element for which value needs to be added
	 * @param val the value of the element
	 */
	private void addElementValue(Document doc, Node elem, String val) {

		if (val != null || val != "")
			elem.appendChild(doc.createTextNode(val));
	}

	/**
	 * Utility method to create attribute and sets value to
	 * within an element.
	 * @param doc the document of the XML
	 * @param elm the element for which attribute needs to be added
	 * @param val the value of the element in string
	 * @param attrName the name of the attribute to be created
	 * @param value the value of the attribute
	 */
	private Attr addAttribute(
		Document doc,
		Element elm,
		String attrName,
		String val) {
		Attr newAttr = doc.createAttribute(attrName);
		if (val != null)
			newAttr.setNodeValue(val);
		((Node) elm).appendChild(newAttr);
		return newAttr;
	}
	public void addAddressData(
		Document doc,
		Element addr,
		AddressData addressData) {

		if (addressData != null) {
			if (addressData.getName1() != null
				&& !addressData.getName1().equals("")) {
				Element comp = createElement(doc, addr, "CompanyName1");
				String name = addressData.getName1();
				addElementValue(doc, comp, name);
				if (addressData.getName2() != null
					&& !addressData.getName2().equals("")) {
					addElementValue(
						doc,
						createElement(doc, addr, "CompanyName2"),
						addressData.getName2());
				}

			} else if (addressData.getFirstName() != null) {
				Element comp = createElement(doc, addr, "FirstName");
				addElementValue(doc, comp, addressData.getFirstName());

				comp = createElement(doc, addr, "LastName");
				addElementValue(doc, comp, addressData.getLastName());

			}
			if (addressData.getStreet() != null) {
				Element street = createElement(doc, addr, "Street");
				addElementValue(doc, street, addressData.getStreet());
			}
			if (addressData.getHouseNo() != null) {
				Element houseNo = createElement(doc, addr, "HouseNumber");
				addElementValue(doc, houseNo, addressData.getHouseNo());
			}
		}
		Element city = createElement(doc, addr, "City");
		Element postalCode = createElement(doc, addr, "PostalCode");
		Element country = createElement(doc, addr, "Country");
		if (addressData != null) {
			addElementValue(doc, city, addressData.getCity());
			addElementValue(doc, postalCode, addressData.getPostlCod1());
			if (addressData.getCountryISO() == null
				|| addressData.getCountryISO().trim().length() == 0)
				addElementValue(doc, country, addressData.getCountry());
			else
				addElementValue(doc, country, addressData.getCountryISO());

		}
	}
	/**
	 * Call this for mass download of orders.
	 * @param bom the business object manager in case the order conversion happens in the
	 * backend or need to some more details.
	 * @param orderGuids the orderguids in order
	 * @param orderids the array of the corresponding orderids in line with the orderguids
	 */
	public void convert(
		BusinessObjectManager bom,
		ArrayList orderGuids,
		ArrayList orderIds,
		OrderDownloadStatus status,
		Locale locale) {
		int size = -1;
		if (orderGuids != null)
			size = orderGuids.size();
		if (orderGuids != null)
			size = orderIds.size();
		String orderGuid = null;
		String orderId = null;
		OrderStatus order = null;
		//		StringBuffer output =  new StringBuffer("<Orders xmlns:xsi=" + DEFAULTSCHEMA + " xsi:noNamespaceSchemaLocation= " + ORDERSSCHEMANAME + ">");
		StringBuffer output = new StringBuffer("<Orders>");
		for (int i = 0; i < size; i++) {
			if (orderGuids != null)
				orderGuid = (String) orderGuids.get(i);
			if (orderIds != null)
				orderId = (String) orderIds.get(i);
			order = fetchOrderStatus(orderId, orderGuid, bom);
			if (order == null) {
				status.setFailedOrder(orderId);
				continue;
			}
			String str = null;
			str = convert(bom, order, locale);
			if (str != null) {
				output.append(str);
				status.setSuccessfulOrder(orderId);
			} else {
				status.setFailedOrder(orderId);
			}
		}
		output.append(NEWLINE + END_ORDERS_ELEMENT);
		status.setGenDoc(XML_HEADER + output.toString());
	}
	/**
	 * This function will try to format the date using one of the fixed formats
	 * like 02/23/2004 into CCYY-MM-DD
	 * @param date
	 * @return
	 */
	private String formatDate(String date, TimeZone tz, String format) {
		//    	SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

		try {
			// Change the shop date format to the standard date format
			// i.e. change the DD ->dd
			// change the YYYY - > yyyy
			String str = format.replace('Y', 'y');
			str = str.replace('D', 'd');
			SimpleDateFormat sdf = new SimpleDateFormat(str);
			Date returnDate = sdf.parse(date);
			// TODO Has to convert this date to the GMT probably 
			sdf = new SimpleDateFormat("yyyy-MM-dd");
			return sdf.format(returnDate);
		} catch (Exception e) {
			log.debug(e);
			return date;
		}
	}
}
