package com.sap.isa.isacore.action.order.orderdownload;

/**
 * Title:        
 * Description:  
 * Copyright:    Copyright (c) 2002
 * Company:      
 * @author 
 * @version 1.0
 */
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.BaseAction;
/**
 * Right now there are no request specific attributes
 * only session specific attributes
 * So just forward to the success page
 */
public class ShowMassDownloadOrdersAction extends BaseAction {

  public ShowMassDownloadOrdersAction() {
  }
   /**
     * Implemented doPerform method used by ActionBase.
     * This method does some useful stuff and then calls isaPerform
     */
    public final ActionForward doPerform(ActionMapping mapping,
                                         ActionForm form,
                                         HttpServletRequest request,
                                         HttpServletResponse response)
        throws IOException, ServletException {
          return mapping.findForward("success");
	}  
}