package com.sap.isa.isacore.action.order.orderdownload;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */
import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.Search;
import com.sap.isa.businessobject.SearchResult;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.UserStatusProfileSearchCommand;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.MassOrderDownloadBean;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

public class AddOrderToMassDownloadListAction extends IsaCoreBaseAction{
	
    public AddOrderToMassDownloadListAction() {
    }

    protected String href_action = "/b2b/order/addOrderToMassDownloadList.do";

    /**
     * Overwrite this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @param documentHandler   DocumentHandler to be used by the action
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
                                    ActionForm form,
                                    HttpServletRequest request,
                                    HttpServletResponse response,
                                    UserSessionData userSessionData,
                                    RequestParser requestParser,
                                    BusinessObjectManager bom,
                                    IsaLocation log)
        throws IOException, ServletException,
        CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        MassOrderDownloadBean mdbean =(MassOrderDownloadBean)userSessionData.getAttribute("massdownloadbean");
        /**
         * Only i care about the guid,id,status and desc
         * Let us care of adding the single bean right now
         * we can take care of adding multiple ones later
         */
        RequestParser.Parameter orderguidParam = requestParser.getParameter("orderguid");
        RequestParser.Parameter orderidParam = requestParser.getParameter("orderid");
        RequestParser.Parameter statusParam = requestParser.getParameter("status");
        RequestParser.Parameter descParam = requestParser.getParameter("desc");
        RequestParser.Parameter orderDateParam = requestParser.getParameter("orderdate");
        RequestParser.Parameter operationParam = requestParser.getParameter("operation");
        String orderid = null;
        String status = null;
        String desc = null;
        String operation = null;
        String orderguid = null;
        String orderDate = null;

        if(operationParam.isSet())
            operation = operationParam.getValue().getString();
        // Get the document Handler object
        DocumentHandler documentHandler = (DocumentHandler)
        userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);

        if(mdbean == null) {
            mdbean = new MassOrderDownloadBean();
            userSessionData.setAttribute("massdownloadbean",mdbean);
        }
        Shop shop = bom.getShop();
        if(shop.isHomActivated()) {
        	// Put the user status map for the ui to display correct statuses
			// Get User Status profile for given Procedure and add to request
			if(userSessionData.getAttribute("userstatusmap")== null) {
				Search search = bom.createSearch();
				SearchResult resultStatusProfile = null;
				resultStatusProfile = search.performSearch(new UserStatusProfileSearchCommand(shop.getLanguage()));
				ResultData result =  new ResultData(resultStatusProfile.getTable());
				HashMap map = new HashMap();
				while(result.next()) {
					map.put(result.getRowKey().toString(),result.getString("DESCRIPTION_LONG"));
				}
				userSessionData.setAttribute("userstatusmap",map);
			}
        }

        if("addall".equals(operation)) {
            orderguidParam = requestParser.getParameter("orderguid[]");
            orderidParam = requestParser.getParameter("orderid[]");
            statusParam = requestParser.getParameter("status[]");
            descParam = requestParser.getParameter("desc[]");
            orderDateParam = requestParser.getParameter("orderdate[]");
            String [] orderguids=null;
            String [] orderids=null;
            String [] descs=null;
            String [] statuses=null;
            String [] orderDates = null;
            if(orderguidParam.isSet())
                orderguids = orderguidParam.getValueStrings();
            if(orderidParam.isSet())
                orderids = orderidParam.getValueStrings();
            if(statusParam.isSet())
                statuses = statusParam.getValueStrings();
            if(descParam.isSet())
                descs = descParam.getValueStrings();
            if(orderDateParam.isSet())
                orderDates = orderDateParam.getValueStrings();

            /**
             * All the data contained in the above fields will contain
             * multiple values delimited by the ";"
             */
            //         ArrayList orderids = getValues(";",orderid);
            //         ArrayList orderguids = getValues(";",orderguid);
            //         ArrayList descriptions = getValues(";",desc);
            //         ArrayList statuses = getValues(";",status);
            if(orderguids!=null) {
                for(int i=0;i<orderguids.length;i++) {
                    orderguid = orderguids[i];
                    if(orderids!=null)
                        orderid = orderids[i];
                    if(statuses!=null)
                        status = statuses[i];
                    if(descs!=null)
                        desc = descs[i];
                    if(orderDates!=null)
                        orderDate = orderDates[i];
                    mdbean.addSalesDocument(orderguid,orderid,status
                                            ,desc,orderDate);
                }
            }
        }
        if("removeall".equals(operation))
            mdbean.removeAllSalesDocuments();
        else if("add".equals(operation)) {
            if(orderidParam.isSet())
                orderid = orderidParam.getValue().getString();
            if(orderguidParam.isSet())
                orderguid = orderguidParam.getValue().getString();
            if(descParam.isSet())
                desc = descParam.getValue().getString();
            if(orderDateParam.isSet())
                orderDate = orderDateParam.getValue().getString();
            if(statusParam.isSet())
                status = statusParam.getValue().getString();
            mdbean.addSalesDocument(orderguid,orderid,status,desc,orderDate);
        }
        else if("remove".equals(operation)) {
            //         if(orderguidParam.isSet())
            //          orderguid = orderguidParam.getValue().getString();
            orderguidParam = requestParser.getParameter("select_[]");
            String [] orderguids =null;
            if(orderguidParam.isSet())
                orderguids = orderguidParam.getValueStrings();
            if(orderguids!=null) {
                for(int i=0;i<orderguids.length;i++) {
                    orderguid = orderguids[i];
                    mdbean.removeSalesDocument(orderguid);
                }
            }
        }
        mdbean.setState(DocumentState.VIEW_DOCUMENT);

        ManagedDocument mDoc =documentHandler.getManagedDocumentOnTop();
        /**
         * @todo if there is already a managed document on the top
         * which is of mass download orders type
         * so forward to the success
         */
        if(mDoc!=null && mDoc.getDocType().equals("massdwnldorders")) {
        	log.exiting();
            return (mapping.findForward("success"));
        }

//        String href_parameter = "operation=view";
        // create the document of mass download type for the tabs
        mDoc = new ManagedDocument(mdbean,
                                   "massdwnldorders",
                                   " ",
                                   "",
                                   "",
                                   "",
                                   "massdownloadorders",
                                   href_action,
                                   null);
        documentHandler.add(mDoc);
        documentHandler.setOnTop(mdbean);
        documentHandler.setCatalogOnTop(false);
        log.exiting();
        return (mapping.findForward("success"));
    }

//    private  ArrayList getValues(String del,String val) {
//        ArrayList vals = new ArrayList();
//        if(val==null) return vals;
//        StringTokenizer strTok = new StringTokenizer(val,del);
//
//        /**
//         * Has more tokens
//         */
//        int i=0;
//        while(strTok.hasMoreTokens()) {
//            Object o = strTok.nextElement();
//            if(o!=null)
//                vals.add(i,o);
//            i++;
//        }
//        return vals;
//    }

    /**
   * utility method to fetch the order from the backend.
   * @todo has to have a type of the sales document.So
   * that we can retrieve the proper type of the document.
   */
    protected  SalesDocument fetchSalesDocument(String orderID,
                                                String guid,
                                                BusinessObjectManager bom) {
         //TODO what to do in case the order already exists in the bom
         // One alternative is
         // Has to take a backup of the order and then put it back to the bom
         
        SalesDocument order = bom.getOrder();
        if(order == null)
            order = bom.createOrder();
        TechKey orderGuid = new TechKey(guid);
        order.setTechKey(orderGuid);
        try {
            order.read();
        }
        catch(Exception e) {
            log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "An error occurred in fetching the salesDoc from the backend",e);
        }
        return order;
    }
}
