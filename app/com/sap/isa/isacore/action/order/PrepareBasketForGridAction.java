/*
 * Created on 25.11.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.isacore.action.order;

// framework dependencies
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.action.IsaCoreInitAction;

/**
 * @author d031177
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class PrepareBasketForGridAction extends MaintainBasketBaseAction {

	 protected String basketPerform(HttpServletRequest request,
                HttpServletResponse response,
                HttpSession session,
                UserSessionData userSessionData,
                RequestParser parser,
                BusinessObjectManager bom,
                IsaLocation log,
                IsaCoreInitAction.StartupParameter startupParameter,
                BusinessEventHandler eventHandler,
                boolean multipleInvocation,
                boolean browserBack,
                Shop shop,
                SalesDocument preOrderSalesDocument,
                boolean auction,
                DocumentState targetDocument,
                DocumentHandler documentHandler)
                    throws CommunicationException {

			final String METHOD_NAME = "basketPerform()";
			log.entering(METHOD_NAME);
			//updateSalesDocument(parser, userSessionData, preOrderSalesDocument, shop, bom.createBUPAManager(), log);
			preOrderSalesDocument.readAllItemsForUpdate();
			ItemSalesDoc lastItem = preOrderSalesDocument.getItems().get(preOrderSalesDocument.getItems().size()-1);
			request.setAttribute(ItemConfigurationBaseAction.PARAM_ITEMID, lastItem.getTechKey());
			
			String forwardTo = FORWARD_CONFIG_ITEM;
			
			//Set the correct uiMode to display the grid Screen or Configuration screen.
			setUiModeForGrid(request, userSessionData);
		
			//Get the correct forward depending on the uimode
			forwardTo= forwardDependingOnUiMode(forwardTo, 
												  preOrderSalesDocument.getItems(), 
												  request, 
												  userSessionData);			
			log.exiting();
			return forwardTo;
		}
}
