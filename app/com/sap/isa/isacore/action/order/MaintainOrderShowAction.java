/*****************************************************************************
  Class:        MaintainOrderShowAction
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       SAP
  Created:      December 2002
  Version:      1.0

  $Revision: #6 $
  $Date: 2002/03/06 $
*****************************************************************************/

package com.sap.isa.isacore.action.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businessobject.order.CustomerOrderChange;
import com.sap.isa.businessobject.order.OrderChange;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.action.IsaCoreInitAction;

/**
 * Action to display the order maintanence again.
 * <p>
 * The following forwards are used by this action:
 * </p>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>FORWARD_CHANGEORDER</td><td>used for standard order change</td></tr>
 *   <tr><td>FORWARD_CHANGECUSTOMERORDER</td><td>used for customer order change</td></tr>
 * </table>
 *
 * @author SAP
 * @version 1.0
 */
public class MaintainOrderShowAction extends MaintainOrderBaseAction {

    protected String orderPerform(HttpServletRequest request,
                                  HttpServletResponse response,
                                  HttpSession session,
                                  UserSessionData userSessionData,
                                  RequestParser parser,
                                  BusinessObjectManager bom,
                                  IsaLocation log,
                                  IsaCoreInitAction.StartupParameter startupParameter,
                                  BusinessEventHandler eventHandler,
                                  boolean multipleInvocation,
                                  boolean browserBack,
                                  Shop shop,
                                  OrderChange order,
                                  DocumentState targetDocument,
                                  DocumentHandler documentHandler)
                throws CommunicationException {
		final String METHOD_NAME = "orderPerform()";
		log.entering(METHOD_NAME);
        // take care of customer order
        if(order instanceof CustomerOrderChange) {
			log.exiting();
            return FORWARD_CHANGECUSTOMERORDER;
        }
		log.exiting();
        return FORWARD_CHANGEORDER;
    }
}