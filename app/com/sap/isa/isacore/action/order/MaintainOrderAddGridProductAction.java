/*****************************************************************************
	Class         MaintainOrderGridProductAction
	Copyright (c) 2004, SAP Labs India, All rights reserved.
	Description:  Action for the all common functionalities of grid product
	Author:
	Created:      23.09.2004
	Version:      1
*****************************************************************************/
package com.sap.isa.isacore.action.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.ManagedDocumentDetermination;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.businessobject.order.OrderChange;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.action.IsaCoreInitAction;
import com.sap.isa.isacore.action.PrepareDeterminationAction;

/**
 * Action to add a new Product to the Order, and forwards to the respective
 * forward depending on the grid/non-grid product
 * <p>
 * The following forwards are used by this action:
 * </p>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>FORWARD_CONFIG_ITEM</td><td>always used</td></tr>
 * </table>
 *
 */
public class MaintainOrderAddGridProductAction extends MaintainOrderBaseAction{
	
	protected String orderPerform(HttpServletRequest request,
				HttpServletResponse response,
				HttpSession session,
				UserSessionData userSessionData,
				RequestParser parser,
				BusinessObjectManager bom,
				IsaLocation log,
				IsaCoreInitAction.StartupParameter startupParameter,
				BusinessEventHandler eventHandler,
				boolean multipleInvocation,
				boolean browserBack,
				Shop shop,
				OrderChange order,
				DocumentState targetDocument,
				DocumentHandler documentHandler)
					throws CommunicationException {

		final String METHOD_NAME = "orderPerform()";
		log.entering(METHOD_NAME);
		String forwardTo = FORWARD_CONFIG_ITEM, newGridProduct = "";

		//Get the product from request
		if ( request.getParameter(GRID_NEW_PROD) != null &&
			 request.getParameter(GRID_NEW_PROD).length() > 0 ){

			newGridProduct = request.getParameter(GRID_NEW_PROD);

			//Create an ISA item out of the obtained item above
			ItemSalesDoc item = createNewItem(newGridProduct);

			//Add the item to the SalesDocument
			order.addItem(item);

			//Update the SalesDocument
			order.update(bom.createBUPAManager(), shop);

			//Read the SalesDocument from Backend
			order.readForUpdate();

			//get the item for the NEW PRODUCT added
			ItemSalesDoc newItem = getItemForNewlyAddedProduct(order.getItems());

			//check the item is grid product or not
			if (newItem != null){

				//If Grid Product
				if ( newItem.isConfigurable() &&
					 newItem.getConfigType().equals(ItemSalesDoc.ITEM_CONFIGTYPE_GRID)){

					//set the techKey for IPC 
					request.setAttribute(ItemConfigurationBaseAction.PARAM_ITEMID, newItem.getTechKey());
					
					//get the correct forward 					  
					forwardTo = forwardDependingOnUiMode(forwardTo,
														order.getItems(),
														request,
														userSessionData);
				}
				else {
					//Not a grid product, but alternative products available
					if (order.isDeterminationRequired()) {
						forwardTo = FORWARD_PRODDET;
            			request.setAttribute(PrepareDeterminationAction.RC_DET_CALLER, ManagedDocumentDetermination.CALLER_MAINTAIN_ORDER_REFRESH);
            			request.setAttribute(PrepareDeterminationAction.RC_DET_DOC_MODE, ManagedDocumentDetermination.DOC_MODE_CHANGE);					}
					else{
						//Not a grid product, so forward to Order
						ManagedDocument onTopDoc = documentHandler.getManagedDocumentOnTop();

						ManagedDocument orderDoc = new ManagedDocument(onTopDoc.getDocument(),
						onTopDoc.getDocType(), onTopDoc.getDocNumber(), onTopDoc.getRefNumber(),
						onTopDoc.getRefName(), onTopDoc.getDate(), "order_change",
						onTopDoc.getHref(), onTopDoc.getHrefParameter());

						documentHandler.add(orderDoc);
						documentHandler.setOnTop(orderDoc.getDocument());
						forwardTo = FORWARD_UPDATEDOCUMENT;
					}
				}
			}
		}
		request.removeAttribute(GRID_NEW_PROD);
		log.exiting();
		return forwardTo;
	}
}