/*****************************************************************************
    Class         ChangeOrderAction
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:
    Created:      02. April 2001
    Version:      3.0
*****************************************************************************/
package com.sap.isa.isacore.action.order;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.backend.boi.isacore.ShipToData;
import com.sap.isa.businessobject.BORuntimeException;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.ShipTo;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.businessobject.item.ItemList;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.businessobject.order.CollectiveOrder;
import com.sap.isa.businessobject.order.CustomerOrder;
import com.sap.isa.businessobject.order.Order;
import com.sap.isa.businessobject.order.OrderStatus;
import com.sap.isa.businessobject.ordertemplate.OrderTemplate;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.eai.BackendRuntimeException;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ItemHierarchy;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.MessageDisplayer;


/**
 * Display the order for change purposes.
 * The action take the actual document from <code>DocumentHandler</code> and
 * initiate the change order process.
 *
 * @author SAP
 * @version 2.0
 *
 */
public class ChangeOrderAction extends ChangeOrderBaseAction {
	/**
	 * Request parameter to be set, if only the header should be
	 * changed for a large order.
	 */
	public static final String RC_CHANGE_HEAD_ONLY = "changeHeadOnly";

    protected String customerOrderForward = CUSTOMER_ORDER_CHANGE;       

    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser parser,
            BusinessObjectManager bom,
            IsaLocation log) throws CommunicationException {

		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
		Shop shp = bom.getShop();
		 if (shp == null) {
			log.exiting();
		   	throw new PanicException("No shop returned from bom");
		 }
        DocumentHandler documentHandler = (DocumentHandler)
                userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);

        ManagedDocument mdoc = documentHandler.getManagedDocumentOnTop();
        
        DocumentState document = mdoc.getDocument();

        OrderStatus orderStatus;

        if (document instanceof OrderStatus) {         
            orderStatus = (OrderStatus)document; 
        }    
        else {
			log.exiting();
           	return mapping.findForward("failure");
        }

        SalesDocument salesDocument = getSalesDocument(document,bom);

        salesDocument.setTechKey(orderStatus.getTechKey());  
        salesDocument.getHeader().setSalesDocNumber(orderStatus.getSalesDocumentNumber());
        salesDocument.getHeader().setProcessType(orderStatus.getOrderHeader().getProcessType());

        String forward = ORDER_CHANGE;
        if (salesDocument instanceof CustomerOrder) {
			forward = customerOrderForward;
		}
		
		// check if we are dealing with a large document, that must be
		// treated specially                 
		if (orderStatus.isLargeDocument(bom.getShop().getLargeDocNoOfItemsThreshold())) {
			if (log.isDebugEnabled()) {
				log.debug("Processig large Document");
			}
			RequestParser.Parameter changeHeadOnly = parser.getParameter(RC_CHANGE_HEAD_ONLY);
			if (changeHeadOnly.isSet() && changeHeadOnly.getValue().getString().equals("true")) {
				// only the header must be changed
				salesDocument.setSelectedItemGuids(new ArrayList());
                salesDocument.setChangeHeaderOnly(true);
				if (log.isDebugEnabled()) {
					log.debug("Only header of large document should be changed");
				}
			}
			else {
                salesDocument.setChangeHeaderOnly(false);
				// there must be items marked to be changed, set these guids in the header
				salesDocument.setSelectedItemGuids(checkLargeDocumentItemsToBeChanged(request, parser, orderStatus, log));
				if (salesDocument.getSelectedItemGuids().size() == 0) {
					if (log.isDebugEnabled()) {
						log.debug("List of items of large document to be changed is empty");
					}
					
					return mapping.findForward("noitemsselected"); 
				}
			}
			salesDocument.setInitialSizeSelectedItems(salesDocument.getSelectedItemGuids().size());
			salesDocument.setNoOfOriginalItems(orderStatus.getNoOfOriginalItems());
		} 

        // we have to avoid that in auction orders only the header is changeable
        if (shp.isEAuctionUsed() && shp.getAuctionOrderType().equals(salesDocument.getHeader().getProcessType())) {
            salesDocument.setChangeHeaderOnly(true);
        }
        

		// Initialize the salesdocument in the backend and add it to the document handler  
        try {       
            setSalesDocument(mdoc.getDocType(),
                             salesDocument,
                             orderStatus.getOrderHeader(),
                             bom,userSessionData,
                             documentHandler,
                             forward);
        } catch (BORuntimeException BORex) {
            // already invalid after initialization
            createErrorMessage(request, documentHandler, mdoc, salesDocument);
            log.exiting();
            return mapping.findForward("message");
        }

		if (!orderStatus.isLargeDocument(bom.getShop().getLargeDocNoOfItemsThreshold())){
			// release the old order status object                          
			DocumentStatusDetailCloseAction.releaseOrderStatus(orderStatus, 
															   documentHandler, 
															   bom,
															   userSessionData);   
        }
        
		try {                                                             
			if (salesDocument instanceof Order) {
			   ((Order) salesDocument).readForUpdate(shp);
			}
			else if (salesDocument instanceof OrderTemplate) {
			  ((OrderTemplate) salesDocument).readForUpdate(shp);
			}
		}
		catch (BackendRuntimeException ex) {
			if (!salesDocument.getHeader().isChangeable()) {
				createErrorMessage(request, documentHandler, mdoc, salesDocument);
				log.exiting();
				return mapping.findForward("message");
			}
		}
        
        // set campaign in catalog after recovery if necessary
        // but not, if multiple campaigns are allowed
        if (!shp.isSoldtoSelectable() && !shp.isMultipleCampaignsAllowed()
            && salesDocument.getHeader().getAssignedCampaigns().size() > 0) {
				// read catalog
				WebCatInfo webCat =
							 getCatalogBusinessObjectManager(userSessionData).getCatalog();
        
        	if (webCat != null) {
				webCat.setCampaignId(salesDocument.getHeader().getAssignedCampaigns().getCampaign(0).getCampaignId());  
        	}
        }
        
//		correction: if reseller's ship to adress has to be added manually to the dropdown box,        
		Shop shop = bom.getShop();
		String compPartnerFunction = shop.getCompanyPartnerFunction();
		if (compPartnerFunction.equals(PartnerFunctionData.RESELLER)){
			BusinessPartnerManager bupama = bom.createBUPAManager();
			User user = bom.getUser();
			BusinessPartner reseller = bupama.getDefaultBusinessPartner(PartnerFunctionData.RESELLER);
			reseller.getAddress();
			boolean add = true;

//			is reseller already in list?	
				  for (int i=0; i < salesDocument.getShipToList().size(); i++) {         
					  ShipTo shipTo = new ShipTo();
					  shipTo = (ShipTo) salesDocument.getShipToList().get(i);
					  if (reseller.getId().equals(shipTo.getId())) {		 	
						  add = false;		 	
					  }						
				  }
			
			if (add){			
						ShipToData shipto = salesDocument.createShipTo();
						shipto.setTechKey(new TechKey(reseller.getId()));
						shipto.setId(reseller.getId());
						shipto.setAddress(reseller.getAddress());
						shipto.setManualAddress();
  						salesDocument.addNewShipTo((ShipTo) shipto);
				
					}				  				        		                
		}
		
		log.exiting();
        return mapping.findForward("changeorder");
    }
    
	protected void createErrorMessage(
		HttpServletRequest request,
		DocumentHandler documentHandler,
		ManagedDocument mdoc,
		SalesDocument salesDocument) {
		documentHandler.release(salesDocument);  
		// create a Message Displayer for the error page!
		MessageDisplayer messageDisplayer = new MessageDisplayer();
		
		messageDisplayer.addToRequest(request);
		messageDisplayer.copyMessages(salesDocument);
		
		// the action which is to call after the message
		if (salesDocument instanceof CustomerOrder) {
			messageDisplayer.setAction("/b2b/customerdocumentstatusdetailprepare.do");
		}
		else if (salesDocument instanceof CollectiveOrder) {
			messageDisplayer.setAction("/b2b/collectivedocumentstatusdetailprepare.do");
		}
		else {
			messageDisplayer.setAction("/b2b/documentstatusdetailprepare.do");
		}
		
		messageDisplayer.setActionParameter("?techkey="+salesDocument.getTechKey().getIdAsString()
											 +"&objecttype="+mdoc.getDocType()
											 +"&objects_origin=&object_id=" );
		
	}
    
    /**
     * For large documents this method tries to determine the items that
     * are marked to be changed.
     * 
     * @param request     The request object
     * @param parser      Parser to simple retrieve data from the request
     * @param orderStatus The orderStatus object 
     * @param log         Reference to the IsaLocation, needed for logging
     * @return ArrayList  TechKeys of all items that are marked to be changed
     */
    protected ArrayList checkLargeDocumentItemsToBeChanged(HttpServletRequest request,
	                                          RequestParser parser,
	                                          OrderStatus orderStatus,
	                                          IsaLocation log) {
    	
    	ArrayList itemsToChange = new ArrayList();
		ItemList itemList = orderStatus.getItemList();
		ItemHierarchy itemHierarchy = new ItemHierarchy(itemList);
		ItemSalesDoc item = null;
		TechKey itemTechKey = null;

    	// are there items marked to be changed
		RequestParser.Parameter checkbox = parser.getParameter((DocumentStatusDetailAction.RK_CHECKBOXNAME + "[]"));
		RequestParser.Parameter checkboxhidden = parser.getParameter((DocumentStatusDetailAction.RK_CHECKBOXHIDDEN + "[]"));

		if (checkbox != null) {
			String on, techKeyValue;
			if (log.isDebugEnabled()) log.debug("--------------------------------------------------------");
			for (int i = 0; i < checkboxhidden.getNumValues(); i++) {
				// Because it is a simulated index which is overtaken from JSP it starts with 1 (ONE)!!
				on = checkbox.getValue(i + 1).getString();
				techKeyValue = checkboxhidden.getValue(Integer.toString(i + 1)).getString();
				
				if (!(on.equals(""))) {            // i'th checkbox marked
					if (log.isDebugEnabled()) log.debug("CheckBox (" + i + ") " + on + "  TechKey:" + techKeyValue);
					itemTechKey = new TechKey(techKeyValue);
					item = itemList.get(itemTechKey);
					if (item != null) {
						// if the item is a sub item transfer key of main position
						while (itemHierarchy.isSubItem(item)) {
							item = itemList.get(item.getParentId());
						}
						itemsToChange.add(item.getTechKey());
					}
					else {
						if (log.isDebugEnabled()) {
							log.debug("checkLargeDocumentItemsToBeChanged - No item found for Techkey: " + techKeyValue);
						}
					}
				}
			}
		}
		
		return itemsToChange;
    }

}