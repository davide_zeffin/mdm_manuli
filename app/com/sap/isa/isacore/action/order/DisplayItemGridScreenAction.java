/*****************************************************************************
  Class:        ExitItemConfigurationAction
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Thomas Smits
  Created:      18.09.2001
  Version:      1.0

  $Revision: #3 $
  $Date: 2001/09/25 $
*****************************************************************************/

package com.sap.isa.isacore.action.order;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.ActionConstantsBase;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 * Action to Display the Grid screen from Order Status screen in display mode.
 * Only the default grid uimode is supported.
 * <br><br>
 * The following forwards are used by this action:
 * </p>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>FORWARD_SUCCESS</td><td>always used</td></tr>
 * </table>
 *
 * @author SAP
 * @version 1.0
 */
public class DisplayItemGridScreenAction extends IsaCoreBaseAction {
    
	protected String FORWARD_SUCCESS  = "success";

    
	/**
	 * @see com.sap.isa.isacore.action.IsaCoreBaseAction#isaPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.sap.isa.core.UserSessionData, com.sap.isa.core.util.RequestParser, com.sap.isa.businessobject.BusinessObjectManager, com.sap.isa.core.logging.IsaLocation)
	 */
	public ActionForward isaPerform(ActionMapping mapping,
									ActionForm form,
									HttpServletRequest request,
									HttpServletResponse response,
									UserSessionData userSessionData,
									RequestParser requestParser,
									BusinessObjectManager bom,
									IsaLocation log)
			throws IOException, ServletException, CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
		
		if (request.getParameter(ActionConstantsBase.PARAM_ITEMID) != null){
			request.setAttribute("itemId", request.getParameter(ActionConstantsBase.PARAM_ITEMID));
		}
		if (request.getParameter("configtype") != null){
			userSessionData.setAttribute("uimode",request.getParameter("configtype"));
			userSessionData.setAttribute("configtype", request.getParameter("configtype"));
		}
		return mapping.findForward(FORWARD_SUCCESS);
	}

}