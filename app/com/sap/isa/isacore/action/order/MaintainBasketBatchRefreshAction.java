/*****************************************************************************
  Class:        MaintainBasketBatchRefreshAction
  Copyright (c) 2001, SAP AG, Germany, All rights reserved.
  Author:       Daniel Seise
  Created:      25.11.2002
  Version:      1.0

  $Revision: #8 $
  $Date: 2002/11/25 $
*****************************************************************************/

package com.sap.isa.isacore.action.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.action.IsaCoreInitAction;

/**
 * Action to display the basket again.
 * <p>
 * The following forwards are used by this action:
 * </p>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>FORWARD_SHOWBASKET</td><td>always used</td></tr>
 * </table>
 *
 * @author Daniel Seise
 * @version 1.0
 */
public class MaintainBasketBatchRefreshAction
	extends MaintainBasketBaseAction {

	public MaintainBasketBatchRefreshAction() {
    }


    protected String basketPerform(HttpServletRequest request,
                HttpServletResponse response,
                HttpSession session,
                UserSessionData userSessionData,
                RequestParser parser,
                BusinessObjectManager bom,
                IsaLocation log,
                IsaCoreInitAction.StartupParameter startupParameter,
                BusinessEventHandler eventHandler,
                boolean multipleInvocation,
                boolean browserBack,
                Shop shop,
                SalesDocument preOrderSalesDocument,
                boolean auction,
                DocumentState targetDocument,
                DocumentHandler documentHandler)
                    throws CommunicationException {

		final String METHOD_NAME = "basketPerform()";
		log.entering(METHOD_NAME);
        if (!multipleInvocation) {

            updateSalesDocument(parser, userSessionData, preOrderSalesDocument, shop, bom.createBUPAManager(), log);

        }
		log.exiting();

        return FORWARD_SHOWBASKET;
    }

}
