/*****************************************************************************
  Class:        MaintainBasketNavigateAction
  Copyright (c) 2004, SAP Labs India, All rights reserved.
  Author:       
  Created:      
  Version:      1.0

  $Revision: #0 $
  $Date:  $
*****************************************************************************/

package com.sap.isa.isacore.action.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.action.IsaCoreInitAction;

/**
 * Action to navigate from one grid product to another with in
 * the grid entry screen
 * <p>
 * The following forwards are used by this action:
 * </p>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>FORWARD_CONFIG_ITEM</td><td>when clicked on PREV / NEXT</td></tr>
 * 	 <tr><td>FORWARD_ADD_GRID_PROD</td><td>when clicked on NEW PRODUCT</td></tr>
 * </table>
 *
 * @author 
 * @version 1.0
 */
public class MaintainBasketNavigateAction extends MaintainBasketBaseAction {

	/**
	 * Creates a new instance of this class.
	 */
	public MaintainBasketNavigateAction() {
	}


	protected String basketPerform(HttpServletRequest request,
				HttpServletResponse response,
				HttpSession session,
				UserSessionData userSessionData,
				RequestParser parser,
				BusinessObjectManager bom,
				IsaLocation log,
				IsaCoreInitAction.StartupParameter startupParameter,
				BusinessEventHandler eventHandler,
				boolean multipleInvocation,
				boolean browserBack,
				Shop shop,
				SalesDocument preOrderSalesDocument,
				boolean auction,
				DocumentState targetDocument,
				DocumentHandler documentHandler)
					throws CommunicationException {
		final String METHOD_NAME = "basketPerform()";
		log.entering(METHOD_NAME);
		//is this required???
		preOrderSalesDocument.readHeader();
		preOrderSalesDocument.readAllItemsForUpdate();
		log.exiting();
		return getPrevNextGridProduct(FORWARD_CONFIG_ITEM,
												preOrderSalesDocument.getItems(),
												request,
												userSessionData);
	}
}