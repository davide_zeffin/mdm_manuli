/*****************************************************************************
    Class         ShowBasketAction
    Copyright (c) 2000, SAP AG, All rights reserved.
    Description:  Action for the Profile Maintenance
    Author:       SAP AG
    Created:      24.4.2001
    Version:      1.0

    $Revision: #10 $
    $Date: 2003/03/13 $
*****************************************************************************/

package com.sap.isa.isacore.action.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.ShipTo;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.ordertemplate.OrderTemplate;
import com.sap.isa.businessobject.quotation.Quotation;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.action.PaymentActions;

/**
 * Reread the data for the basket from the backend to allow
 * a frame tolerant access for the JSP.
 *
 * @author Thomas Smits
 * @version 1.0
 *
 */
public class ShowBasketAction extends IsaCoreBaseAction {

    public static final String SC_SHIPTOS = "com.sap.isa.isacore.order.ShowBasketAction.shiptos";
    public static final String SC_EXIT_CONFIG = "com.sap.isa.isacore.action.order.MaintainBasketAction.exitconfig";
    public static final String FORWARD_SUCCESS = "showbasket";

    /**
     * Overriden <em>isaPerform</em> method of <em>IsaCoreBaseAction</em>.
     */
    public ActionForward isaPerform(ActionMapping mapping,
                ActionForm form,
                HttpServletRequest request,
                HttpServletResponse response,
                UserSessionData userSessionData,
                RequestParser requestParser,
                BusinessObjectManager bom,
                IsaLocation log)
                        throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        // Page that should be displayed next.
        String forwardTo = null;

        String exitConfig = (String) userSessionData.getAttribute(SC_EXIT_CONFIG);
        if (exitConfig != null) {
        //
            userSessionData.removeAttribute(SC_EXIT_CONFIG);
            request.setAttribute(MaintainBasketBaseAction.RC_EXITCONFIG, "true");
            forwardTo = FORWARD_SUCCESS;
			log.exiting();
            return mapping.findForward(forwardTo);
        }


        HttpSession session = request.getSession();

        // Ask the business object manager for a user. If this object is not
        // already present, the the next action will to login
        Shop shp = bom.getShop();

        // get pre-order document on top from document handler
        DocumentHandler documentHandler = (DocumentHandler)
            userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
        if (documentHandler == null) {
			log.exiting();
            throw new PanicException("No document handler found");
        }
        DocumentState targetDocument =
            documentHandler.getDocument(ManagedDocument.TARGET_DOCUMENT);

        // get the current pre order sales document - basket is assumed as
        // default
        SalesDocument preOrderSalesDocument = null;
        if (targetDocument instanceof Quotation) {
            preOrderSalesDocument = bom.getQuotation();
            userSessionData.setAttribute(
                MaintainBasketBaseAction.SC_DOCTYPE,
                MaintainBasketBaseAction.DOCTYPE_QUOTATION);
        }
        else if (targetDocument instanceof OrderTemplate) {
            preOrderSalesDocument = bom.getOrderTemplate();
            userSessionData.setAttribute(
                MaintainBasketBaseAction.SC_DOCTYPE,
                MaintainBasketBaseAction.DOCTYPE_ORDERTEMPLATE);
        }
        else {
            preOrderSalesDocument = bom.getBasket();
            userSessionData.setAttribute(
                MaintainBasketBaseAction.SC_DOCTYPE,
                MaintainBasketBaseAction.DOCTYPE_BASKET);
        }
        if (preOrderSalesDocument == null) {
			log.exiting();
            throw new PanicException("No sales document returned from backend");
        }

        preOrderSalesDocument.read();

        if (!preOrderSalesDocument.getItems().isEmpty()) {
            preOrderSalesDocument.readIpcInfo();
            
            //note 1180044
            //if there is an error in the entered material number the shipTo might be 
            //reseted unintentional            
            int numItems = preOrderSalesDocument.getItems().size();
            for(int i=0;i<numItems; i++){
            	ShipTo itemShipTo = preOrderSalesDocument.getItems().get(i).getShipTo();
            	if(itemShipTo==null){
            		preOrderSalesDocument.getItems().get(i).setShipTo(preOrderSalesDocument.getHeader().getShipTo());
        }
            }//end note
        }
        
        // add Message for failed simulation if necessary
        if (userSessionData.getAttribute(MaintainBasketSimulateAction.NO_SOLDTO_MSG) != null) {
           Message msg = new Message(Message.ERROR, "b2b.order.simulate.nosoldto", null, "");
           preOrderSalesDocument.addMessage(msg);
           userSessionData.removeAttribute(MaintainBasketSimulateAction.NO_SOLDTO_MSG);
        }
        
        // Test for necessary relationship between reseller and soldto
		MaintainBasketBaseAction.checkSoldToResellerRelation(preOrderSalesDocument, shp, bom.createBUPAManager(), false);
        
        // check if message from lead/opportunity2basket process has to be displayed.
        // The message is raised         
        if (userSessionData.getAttribute("preDocError") != null) {           
		    Message msg = new Message(Message.ERROR,"b2b.order.simulate.predoce", null, "");
		    preOrderSalesDocument.addMessage(msg);
		    userSessionData.removeAttribute("preDocError");
        }
        
        if (userSessionData.getAttribute(MaintainBasketSendAction.SC_ATPSUBST) != null) {
            Message msg = new Message(Message.WARNING, "b2b.order.atp.subst");
            preOrderSalesDocument.addMessage(msg);
            userSessionData.removeAttribute(MaintainBasketSendAction.SC_ATPSUBST);
        }

		//remove grid attributes from userSessionData
		if (userSessionData.getAttribute("prev") != null)
			userSessionData.removeAttribute("prev");
			
		if (userSessionData.getAttribute("next") != null)	
			userSessionData.removeAttribute("next");
			
		/*
		if (userSessionData.getAttribute("uimode") != null)	
			userSessionData.removeAttribute("uimode");
		*/
			
		if (userSessionData.getAttribute("configtype") != null)	
			userSessionData.removeAttribute("configtype");
		
        request.setAttribute(
                MaintainBasketBaseAction.RC_BASKET, preOrderSalesDocument);

        request.setAttribute(
                MaintainBasketBaseAction.RK_HEADER, preOrderSalesDocument.getHeader());

        request.setAttribute(
                MaintainBasketBaseAction.RK_ITEMS, preOrderSalesDocument.getItems());

        request.setAttribute(
                MaintainBasketBaseAction.RC_SHIPCOND, preOrderSalesDocument.readShipCond(shp.getLanguage()));

        request.setAttribute(
                MaintainBasketBaseAction.RK_MESSAGES, preOrderSalesDocument.getMessageList());

        userSessionData.setAttribute(
                MaintainBasketBaseAction.SC_SHIPTOS, preOrderSalesDocument.getShipTos());

		// put payment related values into the context
		PaymentActions.setPaymentAttributes(userSessionData, request, preOrderSalesDocument); 
 
        forwardTo = FORWARD_SUCCESS;

        resolveMessageKeys(session, preOrderSalesDocument, log, log.isDebugEnabled());
		log.exiting();
        return mapping.findForward(forwardTo);
    }

}