/*****************************************************************************
    Class:        DocumentStatusDetailPrepareAction
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author        Ralf Witt
    Created:      May 2001

    $Revision: #2 $
    $Date: 2001/11/20 $
*****************************************************************************/

package com.sap.isa.isacore.action.order;

import javax.servlet.http.HttpServletRequest;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.order.OrderDataContainer;
import com.sap.isa.businessobject.order.OrderStatus;


/**
 * Prepares to show a documents detail
 *
 * <br> Note that all Action classes have to provide a non argument constructor
 * because they were instantiated automatically by the action servlet.
 *
 * @author  Ralf Witt
 * @version 0.1
 *
 */
public class DocumentStatusDetailPrepareAction extends DocumentStatusDetailPrepareBaseAction {



	/**
	 * Constructor.
	 * Overwrites <code>href_action</code> and <code>forward</code> 
	 */
	public DocumentStatusDetailPrepareAction() {
		
   		href_action = "b2b/documentstatusdetailprepare.do";
 	  	forward = "order_status";	

	}	


	/**
	 * Returns a new order status object. 
	 * 
	 * @see com.sap.isa.isacore.action.order.DocumentStatusDetailPrepareBaseAction#getSalesDocumentStatus(BusinessObjectManager)
	 */
	protected OrderStatus getSalesDocumentStatus(HttpServletRequest request, BusinessObjectManager bom) {
        // In case items are already selected (large document handling), return object from
        // the bom, otherwise create new one
        OrderStatus orderStatusDetail = null;
        boolean itemSearchPerformed = 
             (request.getAttribute(DocumentStatusSearchItemsAction.RC_ITEMSEARCHPERFORMED) != null ?
                ((Boolean)request.getAttribute(DocumentStatusSearchItemsAction.RC_ITEMSEARCHPERFORMED)).booleanValue() : false);
        if (bom.getOrderStatus() == null  ||  itemSearchPerformed == false) {
            bom.releaseOrderStatus();
            OrderDataContainer order = new OrderDataContainer();
            orderStatusDetail = bom.createOrderStatus(order);
        } else {
            orderStatusDetail = bom.getOrderStatus();
        }
        
        return orderStatusDetail;
	}
	
	/**
	 * @see com.sap.isa.isacore.action.order.DocumentStatusDetailPrepareBaseAction#getSalesDocumentType(HeaderSalesDocument)
	 */
	protected String getSalesDocumentType(HeaderSalesDocument header) {
		return header.getDocumentType();
	}

}
