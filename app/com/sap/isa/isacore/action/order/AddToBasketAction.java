/*****************************************************************************
    Class         AddToBasketAction
    Copyright (c) 2001, SAP AG, All rights reserved.
    Description:  Action add a product to the salesDocument
    Author:       SAP AG
    Created:      May 2001
    Version:      1.0

    $Revision: #25 $
    $Date: 2002/11/05 $
*****************************************************************************/
package com.sap.isa.isacore.action.order;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.ecommerce.businessobject.loyalty.LoyaltyMembership;
import com.sap.isa.businessobject.AppBaseBusinessObjectManager;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.IdMapper;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businessobject.item.BasketTransferItem;
import com.sap.isa.businessobject.item.BasketTransferItemImpl;
import com.sap.isa.businessobject.item.ItemList;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.businessobject.negotiatedcontract.NegotiatedContract;
import com.sap.isa.businessobject.order.CampaignListEntry;
import com.sap.isa.businessobject.order.Order;
import com.sap.isa.businessobject.order.PartnerList;
import com.sap.isa.businessobject.order.PartnerListEntry;
import com.sap.isa.businessobject.ordertemplate.OrderTemplate;
import com.sap.isa.businessobject.quotation.Quotation;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.core.ContextConst;
import com.sap.isa.core.FrameworkConfigManager;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.interaction.InteractionConfig;
import com.sap.isa.core.interaction.InteractionConfigContainer;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.DecimalPointFormat;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.RecoverBasketCookieHandler;
import com.sap.isa.isacore.action.ActionConstants;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.action.IsaCoreInitAction;
import com.sap.isa.isacore.action.OciReceiveAction;
import com.sap.isa.isacore.action.loyalty.LoyaltyUtilities;
import com.sap.spc.remote.client.object.IPCItem;



/**
 * Add a product or a list of products to the active sales document.<p>
 *
 * The action take the transferItem(s) from the Request Context and add the item
 * or the list of items to the active document.<br>
 *
 * In B2C always the basket is used. If the basket doesn't exists it will be
 * create within this action.<br>
 * In B2B the active document is used. If not active document exists the forward
 * <b>noDocument</b> is set. If the docTypeLateDecision property is set, the forward
 * <b>createBasket</b> is set.<br>
 *
 * <i>Note:</i> The scenario is take from context parameter ContextConst.IC_SCENARIO
 *
 * <h4>Overview over request parameters and attributes</h4>
 *
 * <table border="1" cellspacing="0" cellpadding="4">
 *   <tr>
 *     <th>name</th>
 *     <th>parameter</th>
 *     <th>attribute</th>
 *     <th>description</th>
 *   <tr>
 *   <tr>
 *      <td>baskettransferitem</td><td>&nbsp</td><td>X</td>
 *      <td>contains a <code>BasketTransferItem</code> object with the product to add</td>
 *   </tr>
 *   <tr>
 *      <td>baskettransferitemlist</td><td>&nbsp</td><td>X</td>
 *      <td>contains a list of <code>BasketTransferItem</code> objects</td>
 *   </tr>
 *   <tr>
 *      <td>forward</td><td>&nbsp</td><td>X</td>
 *      <td>logical forward, which is to call after the add to basket sequence</td>
 *   </tr>
 * </table>
 *
 * <h4>The following forwards are used by this action</h4>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <th>forward</th>
 *     <th>description</th>
 *   <tr>
 *   <tr><td>success</td><td>no other logical forward is definied</td></tr>
 *   <tr><td>noDocument</td><td>no active document could be found</td></tr>
 *   <tr><td>createBasket</td><td>a basket should be created</td></tr>
 *   <tr><td>error</td><td>an application error occurs</td></tr>
 *   <tr><td><i>locial forward given by the <b>forward</b> attribute</i></td><td>see config.xml</td></tr>
 * </table>
 * <h4>Overview over attributes which will be set in the request context</h4>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <th>attribute</th>
 *     <th>description</th>
 *   <tr>
 * <tr><td>forward</td><td>logical forward which, will be called after displaying a
 *    message.  The value is <i>success</i> by default</td></tr>
 * </table>
 * <h4>Overview over attributes which will be set in the session context</h4>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <th>attribute</th>
 *     <th>description</th>
 *   <tr>
 * <tr><td><i>AddToBasketAction.ANIMATION</i></td><td>flag for displayin an animation.</td></tr>
 * <tr><td><i>AddToBasketAction.SC_TRANSFER_ITEM_LIST</i></td><td>basket transfer items store
 *   in session context while the selection of the document type</td></tr>
 * <tr><td><i>AddToBasketAction.SC_TRANSFER_ITEM</i></td><td>basket transfer item stores
 *   in session context while the selection of the document type</td></tr>
 * <tr><td><i>AddToBasketAction.SC_FORWARD_NAME</i></td><td>logical forward stores
 *   in session context while the selection of the document type</td></tr>
 * </table>
 *

 *
 * @author SAP
 * @version 1.0
 *
 */
public class AddToBasketAction extends IsaCoreBaseAction {
    /**
     * Used request attribute: actual value <i>baskettransferitem</i>
     */
    public static final String RC_TRANSFER_ITEM = "baskettransferitem";

    /**
     * Used request attribute: actual value <i>baskettransferitemlist</i>
     */
    public static final String RC_TRANSFER_ITEM_LIST = "baskettransferitemlist";

    /**
     * Used request attribute: actual value <code>ActionConstants.RA_FORWARD</code>
     */
    public static final String FORWARD_NAME = ActionConstants.RA_FORWARD;

    /**
     * Used session attribute: actual value <i>animation</i>
     */
    public static final String ANIMATION = "animation";

    /**
     * Used session attribute: actual value <i>addtobaskettransferitem</i>
     */
    public static final String SC_TRANSFER_ITEM = "addtobaskettransferitem";

    /**
     * Used session attribute: actual value <i>addtobaskettransferitemlist</i>
     */
    public static final String SC_TRANSFER_ITEM_LIST = "addtobaskettransferitemlist";

    /**
     * Used session attribute: actual value <i>addtobasketforward</i>
     */
    public static final String SC_FORWARD_NAME = "addtobasketforward";
    public static final String IS_GRID_ITEM = "isGridItem";

    // is used/set in web.xml
    // possible values: see CreateDocumentAction.FORWARD_BASKET,
    //                      CreateDocumentAction.FORWARD_QUOTATION and
    //                      CreateDocumentAction.FORWARD_ORDERTEMPLATE
    public static final String DEF_DOCUMENT_TYPE = "defaultDocumentType.isacore.isa.sap.com";
    private static final String FORWARD_SUCCESS = "success";
    private static final String FORWARD_CREATE_BASKET_FOR_GRID = "createBasketForGrid";

    /**
     * set the flag which documenttype to create in the request
     * @param request request context
     * @param documentType Type of document to create. See CreateDocumentAction for possible constants
     */
    public static void setDefaultDocumentType(HttpServletRequest request, String documentType) {
        request.setAttribute(DEF_DOCUMENT_TYPE, documentType);
    }

    /**
     * get the flag which documenttype to create in the request
     * @param request request context
     * @return documentType Type of document to create. See CreateDocumentAction for possible values
     */
    public static String getDefaultDocumentType(HttpServletRequest request) {
        return (String) request.getAttribute(DEF_DOCUMENT_TYPE);
    }

    /**
     * set the tansfer item in the request context
     *
     * @param request request context
     * @param transferItem item to set in the request context
     *
     */
    public static void setRequestAttribute(HttpServletRequest request, BasketTransferItem transferItem) {
        request.setAttribute(RC_TRANSFER_ITEM, transferItem);
    }

    /**
     * set the tansfer item in the request context
     *
     * @param request request context
     * @param transferItem item to set in the request context
     * @param forward name of the forward mapping
     *
     */
    public static void setRequestAttribute(HttpServletRequest request, BasketTransferItem transferItem, String forward) {
        request.setAttribute(RC_TRANSFER_ITEM, transferItem);
        request.setAttribute(FORWARD_NAME, forward);
    }

    /**
     * set a the list tansfer item in the request context
     *
     * @param request request context
     * @param transferItemList list of transfer item to set in the request context
     *
     */
    public static void setRequestAttribute(HttpServletRequest request, List transferItemList) {
        request.setAttribute(RC_TRANSFER_ITEM_LIST, transferItemList);
    }

    /**
     * set a the list tansfer item in the request context
     *
     * @param request request context
     * @param transferItemList list of transfer item to set in the request context
     * @param forward name of the forward mapping
     *
     */
    public static void setRequestAttribute(HttpServletRequest request, List transferItemList, String forward) {
        request.setAttribute(RC_TRANSFER_ITEM_LIST, transferItemList);
        request.setAttribute(FORWARD_NAME, forward);
    }

    /**
     * DOCUMENT ME!
     */
    public void initialize() {
        if (isB2C()) {
            this.checkUserIsLoggedIn = false;
        }
    }

    /**
     * Overriden <em>isaPerform</em> method of <em>IsaCoreBaseAction</em>.
     */
    public ActionForward isaPerform(ActionMapping mapping, ActionForm form, HttpServletRequest request,
        HttpServletResponse response, UserSessionData userSessionData, RequestParser requestParser,
        BusinessObjectManager bom, IsaLocation log, IsaCoreInitAction.StartupParameter startupParameter,
        BusinessEventHandler eventHandler)
        throws IOException, ServletException, CommunicationException {
        final String METHOD_NAME = "isaPerform()";
        log.entering(METHOD_NAME);

        // flag indicating whether the sales document was freshly created
        // or not, default is new
        boolean initialSalesDocument = true;
		String forward = "nodocument";        

        Shop shop = bom.getShop();
        User user = bom.getUser();
        HttpSession session = request.getSession();

        SalesDocument salesDocument = null;
		if (userSessionData.getAttribute("EAUCTION_PARAMID") != null) {
			forward = "eauction";
			return mapping.findForward(forward);
		}

        BasketTransferItem transferItem = (BasketTransferItem) request.getAttribute(RC_TRANSFER_ITEM);
        List transferItemList = (List) request.getAttribute(RC_TRANSFER_ITEM_LIST);

        //is set to request attribute, to make use while returning from grid IPC screen to Catalog
        //This flag is used in ExitItemConfigurationAction and ReturnItemConfigurationAction
        //and removed from request object
        String isGridItem = (String) request.getParameter(IS_GRID_ITEM);

        if ((isGridItem != null) && (isGridItem.length() > 0)) {
            request.getSession().setAttribute("gridcatalog", "X");
        }

        String scenario = getInitParameter(ContextConst.IC_SCENARIO);

        // check if identical products should be merged to one basket
        // entry or not.
        // String merge = getInitParameter(ContextConst.IC_MERGE_IDENTICAL_PRODUCTS);
        InteractionConfigContainer icc = FrameworkConfigManager.Servlet.getInteractionConfig(request);
        InteractionConfig uiInteractionConfig = null;
        String merge = null;

        if (null != icc) {
            uiInteractionConfig = icc.getConfig("ui");
            merge = uiInteractionConfig.getValue(ContextConst.IC_MERGE_IDENTICAL_PRODUCTS);
        }

        if (log.isDebugEnabled()) {
            log.debug("merge parameter is set  to: " + merge);
        }

        boolean mergeIdenticalProducts = ("true".equals(merge) || "on".equals(merge));

        if (log.isDebugEnabled()) {
            log.debug("mergeidenticalproducts is set to: " + mergeIdenticalProducts);
        }

        // check for oci transfer
        boolean ociTransfer = (startupParameter != null) && (startupParameter.getHookUrl().length() > 0);

        String anim = "false";

        if (log.isDebugEnabled()) {
            log.debug("scenario: " + scenario);
        }

        // read catalog
        WebCatInfo webCat = getCatalogBusinessObjectManager(userSessionData).getCatalog();

        CampaignListEntry campaignListEntry = null;
        
		DocumentHandler documentHandler = null;

        if (scenario.equals("B2C")) {
            initialSalesDocument = (bom.getBasket() == null);

            salesDocument = bom.createBasket();

            // Setting of the campaignKey if existing
            if ((startupParameter != null) && (startupParameter.getCampaignKey().length() > 0)) {
                String campaignKey = startupParameter.getCampaignKey();
                salesDocument.setCampaignKey(campaignKey);
                salesDocument.setCampaignObjectType(startupParameter.getCampaignObjectType());

                campaignListEntry = salesDocument.getHeader().getAssignedCampaigns().createCampaign();
                campaignListEntry.setCampaignGUID(new TechKey(campaignKey));
                campaignListEntry.setCampaignTypeId(startupParameter.getCampaignObjectType());
            }

            if (initialSalesDocument && shop.isManualCampainEntryAllowed() && !shop.isSoldtoSelectable()) {
                if (shop.isInternalCatalogAvailable() && (webCat == null)) {
                    throw (new IllegalArgumentException("Paramter webCat can not be null!"));
                }

                // check the catalog if a campaign Id exists
                if ((webCat != null) && ((webCat.getCampaignId() != null) && (webCat.getCampaignId().length() > 0))) {
                    // we use the campaign Id of the catalog
                    campaignListEntry = null;

                    // set the campaign Id
                    campaignListEntry = salesDocument.getHeader().getAssignedCampaigns().createCampaign(webCat.getCampaignId(),
                            TechKey.EMPTY_KEY, "", false, true);
                }
            }
        }
        else {
            // b2b case - several type of target documents are possible
            // search for existing target document on the document handler
            documentHandler = (DocumentHandler) userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);

            if (documentHandler == null) {
                log.exiting();
                throw new PanicException("No document handler found");
            }

            ManagedDocument managedDoc = documentHandler.getManagedDocument(ManagedDocument.TARGET_DOCUMENT);

            DocumentState targetDocument = null;

            // if there is an associated ManagedDocument,
            // than the associated document should be the target for the insert
            if (managedDoc != null) {
                if (managedDoc.getAssociatedDocument() == null) {
                    targetDocument = managedDoc.getDocument();
                }
                else {
                    targetDocument = managedDoc.getAssociatedDocument().getDocument();
                    targetDocument.setState(ManagedDocument.TARGET_DOCUMENT);
                    documentHandler.add(managedDoc.getAssociatedDocument());
                    documentHandler.setOnTop(targetDocument);
                }
            }

            // no target doc so far -> create one first
            if (targetDocument == null) {
                // the objects must be stored, because a new request will follow for the creating of the document
                if (transferItemList != null) {
                    request.getSession().setAttribute(SC_TRANSFER_ITEM_LIST, transferItemList);
                }
                else {
                    request.getSession().removeAttribute(SC_TRANSFER_ITEM_LIST);
                }

                if (transferItem != null) {
                    request.getSession().setAttribute(SC_TRANSFER_ITEM, transferItem);
                }
                else {
                    request.getSession().removeAttribute(SC_TRANSFER_ITEM);
                }

                Object obj = request.getAttribute(FORWARD_NAME);

                if (obj != null) {
                    request.getSession().setAttribute(SC_FORWARD_NAME, obj);
                }

                // is there any default document type to create ?
                // get it from the web.xml
                // this can enable this action to directly create the document without
                // paining the user to select it manually
                // especially helpful for OCI item transfer into ISales
                String defDocumentType = getInitParameter(AddToBasketAction.DEF_DOCUMENT_TYPE);

                if (defDocumentType != null) {
                    if (CreateDocumentAction.FORWARD_BASKET.equals(defDocumentType) ||
                            CreateDocumentAction.FORWARD_QUOTATION.equals(defDocumentType) ||
                            CreateDocumentAction.FORWARD_ORDERTEMPLATE.equals(defDocumentType)) {
                        // set the forward for the creation actions
                        request.setAttribute(ActionConstants.RA_FORWARD, "addtodocument");

                        forward = defDocumentType;
                    }
                    else {
                        if (log.isDebugEnabled()) {
                            log.debug(AddToBasketAction.DEF_DOCUMENT_TYPE + " found but invalid content : " +
                                defDocumentType);
                        }
                    }
                }
                else if (shop.isDocTypeLateDecision()) {
                    // in the Late decision scenario, only a basket can be created so
                    // forward directly to the CreateBasketAction
                    // set the forward for the creation actions
                    request.setAttribute(ActionConstants.RA_FORWARD, "addtodocument");
                    request.setAttribute("orderType", "order");
                    forward = "createBasket";
                }
                else {
                    forward = "nodocument";
                    // set this flag to indicate proper forwarding in the GetDealerFamilyAction
                    request.setAttribute(ActionConstants.RA_FORWARD, "addToBasket");

                    if ((isGridItem != null) && (isGridItem.length() > 0)) {
                        request.setAttribute(ActionConstants.RA_FORWARD, "addtodocument");
                        request.setAttribute("orderType", "order");
                        documentHandler.setCatalogOnTop(false);
                        forward = "createBasket";
                    }
                }

                log.exiting();

                return mapping.findForward(forward);
            }
            else {
                // get the existing target document from the bom
                if (targetDocument instanceof Quotation) {
                    salesDocument = bom.getQuotation();
                }
                else if (targetDocument instanceof OrderTemplate) {
                    salesDocument = bom.getOrderTemplate();
                }
                else if (targetDocument instanceof Order) {
                    salesDocument = bom.getOrder();
                }
                else if (targetDocument instanceof NegotiatedContract) {
                    salesDocument = bom.getNegotiatedContract();
                }
                else {
                    salesDocument = bom.getBasket();
                }

                if (salesDocument == null) {
                    log.exiting();
                    throw new PanicException("No sales document returned from backend");
                }

                initialSalesDocument = false;
            }
        }

        // check if user is logged in
        boolean loggedIn = user.isUserLogged();

        // initialize salesDocument if its a new one
        if (loggedIn) {
            if (log.isDebugEnabled()) {
                log.debug("AddToBasketAction: initBasket(shop, user)");
            }

            if (initialSalesDocument) {
                PartnerList pList = new PartnerList();
                BusinessPartnerManager bupama = bom.createBUPAManager();
                BusinessPartner buPa;

                String compPartnerFunction = shop.getCompanyPartnerFunction();

                if ((compPartnerFunction == null) || (compPartnerFunction.length() == 0)) {
                    compPartnerFunction = PartnerFunctionData.SOLDTO;
                }

                if (compPartnerFunction.equals(PartnerFunctionData.SOLDTO) ||
                        compPartnerFunction.equals(PartnerFunctionData.RESELLER) ||
                        compPartnerFunction.equals(PartnerFunctionData.AGENT)) {
                    buPa = bupama.getDefaultBusinessPartner(compPartnerFunction);

                    if (buPa != null) {
                        pList.setPartner(compPartnerFunction, new PartnerListEntry(buPa.getTechKey(), buPa.getId()));
                    }
                }

                // set contact info, but only, if we are not in the reseller scenario
                buPa = bupama.getDefaultBusinessPartner(PartnerFunctionData.CONTACT);

                if ((!compPartnerFunction.equals(PartnerFunctionData.RESELLER)) &&
                        (!compPartnerFunction.equals(PartnerFunctionData.AGENT)) &&
                        (buPa != null)) {
                    pList.setContact(new PartnerListEntry(buPa.getTechKey(), buPa.getId()));
                }

                // add responsible at partner in reseller scenario
                if (compPartnerFunction.equals(PartnerFunctionData.RESELLER)) {
                    BusinessPartner bupa1 = bupama.getDefaultBusinessPartner(PartnerFunctionData.RESP_AT_PARTNER);
                    pList.setPartner(PartnerFunctionData.RESP_AT_PARTNER,
                        new PartnerListEntry(bupa1.getTechKey(), bupa1.getId()));
                }

                // initialisation of the basket
				LoyaltyMembership loyMemShip = LoyaltyUtilities.getLoyaltyMembership(userSessionData);                
				salesDocument.init(shop, bupama, pList, webCat, campaignListEntry, loyMemShip); // may throw CommunicationException
            }
            else if (shop.getLoyaltyProgramGUID() != null && shop.getLoyaltyProgramGUID().getIdAsString().length() > 0) {
				LoyaltyMembership loyMemShip = LoyaltyUtilities.getLoyaltyMembership(userSessionData);
				salesDocument.getHeader().setMemberShip(loyMemShip);
            }
        }
        else {
            if (log.isDebugEnabled()) {
                log.debug("AddToBasketAction: initBasket(shop)");
            }

            if (initialSalesDocument) {
                salesDocument.init(shop, webCat, campaignListEntry); // may throw CommunicationException
            }
        }
        //	sd: clearMessages sets the dirty flag always to true, due to performance reasons 
		//      the call has been encommented  
        //salesDocument.clearMessages();

        if ((transferItem == null) && (transferItemList == null)) {
            log.exiting();

            return mapping.findForward("error");
        }

        // Store the list of ShipTos in session context, to allow all frames access to
        userSessionData.setAttribute(MaintainBasketBaseAction.SC_SHIPTOS, salesDocument.getShipTos());

        // first the salesDocument must read from backend
        salesDocument.readHeader();

        salesDocument.readAllItems();

        // care for header req. delivery date
        String headerReqDeliveryDate = salesDocument.getHeader().getReqDeliveryDate();

        if (transferItem != null) {
            // adopt the quantity from an existing item
            if (transferItem.getBasketItemToReplace() != null) {
                ItemSalesDoc itemToReplace = salesDocument.getItem(((ItemSalesDoc) transferItem.getBasketItemToReplace()).getTechKey());

                if ((itemToReplace != null) && transferItem instanceof BasketTransferItemImpl) {
                    ((BasketTransferItemImpl) transferItem).setQuantity(itemToReplace.getQuantity());
                }
            }

            // add item
            salesDocument.addTransferItem(transferItem,
                                          headerReqDeliveryDate,
                                          new TechKey(mapProductId(userSessionData, salesDocument, transferItem)));
        }

        int shipToHandle = 0;

        if (transferItemList != null) {
            Iterator i = transferItemList.iterator();

            BasketTransferItem basketTransferItem;

            while (i.hasNext()) {
                basketTransferItem = (BasketTransferItem) i.next();

                // adopt the quantity from an existing item
                if (basketTransferItem.getBasketItemToReplace() != null) {
                    ItemSalesDoc itemToReplace = salesDocument.getItem(((ItemSalesDoc) basketTransferItem.getBasketItemToReplace()).getTechKey());

                    if ((itemToReplace != null) && basketTransferItem instanceof BasketTransferItemImpl) {
                        ((BasketTransferItemImpl) basketTransferItem).setQuantity(itemToReplace.getQuantity());
                    }
                }


                // Set handle to add a new ShipTo Party lateron (after update!!)
                if (basketTransferItem instanceof BasketTransferItemImpl &&
                        (((BasketTransferItemImpl) basketTransferItem).getShipTo() != null)) {
                    salesDocument.addTransferItem(basketTransferItem, 
                                                  headerReqDeliveryDate,
                                                  Integer.toString(shipToHandle),
                                                  new TechKey(mapProductId(userSessionData, salesDocument, basketTransferItem)));
                    ((BasketTransferItemImpl) basketTransferItem).setHandle(Integer.toString(shipToHandle));
                    shipToHandle++;
                }
                else {
					salesDocument.addTransferItem(basketTransferItem, 
												  headerReqDeliveryDate, 
												  new TechKey(mapProductId(userSessionData, salesDocument, basketTransferItem)));
                }
                
            }
        }

        // merge together identical products in the b2c scenario
        if (scenario.equals("B2C") && mergeIdenticalProducts) {
            // need to retrieve product guid if OCI is used and products should be merged
            // and product guid is not filled
            if (log.isDebugEnabled()) {
                log.debug("Merge Products: ociTransfer=" + ociTransfer + " SC_OCI_RECEIVE=" +
                    (String) userSessionData.getAttribute(OciReceiveAction.SC_OCI_RECEIVE));
            }

            if (ociTransfer || (userSessionData.getAttribute(OciReceiveAction.SC_OCI_RECEIVE) != null)) {
                WebCatItem webCatItem = null;
                ItemSalesDoc item;
                ItemList items = salesDocument.getItems();

                for (int i = 0; i < items.size(); i++) {
                    item = items.get(i);

                    if ((item.getProductId() == null) || (item.getProductId().getIdAsString().length() == 0)) {
                        webCatItem = salesDocument.searchItemInCatalog(item, shop);

                        if (webCatItem != null) {
                            item.setProductId(new TechKey(webCatItem.getProductID()));
                        }
                        else {
                            if (log.isDebugEnabled()) {
                                log.debug("No WebcatItem foudn for OCI transferred product " + item.toString());
                            }
                        }
                    }
                }
            }
            // If the user is not logged in use the shop to get the decimalPointformat
            // otherwise a Stateful connection will be created; reason memory consumption
            DecimalPointFormat decimalPointFormat;
            if (user.isUserLogged()){
            	decimalPointFormat = user.getDecimalPointFormat();
            }
            else {
            	decimalPointFormat = 
            	       DecimalPointFormat.createInstance(shop.getDecimalSeparator().trim().charAt(0), 
            	                                          shop.getGroupingSeparator().trim().charAt(0));
            }
            salesDocument.mergeIdenticalProducts(decimalPointFormat);
        }

        userSessionData.removeAttribute(OciReceiveAction.SC_OCI_RECEIVE);

        salesDocument.update(bom.createBUPAManager(), shop); // may throw CommunicationException
		//Note:1059235 -If the backend is CRM then Copy IPC sub-items for grid item
		//when copied from another document(Order / template /etc.)
		if (shop != null && (shop.getBackend() != null && shop.getBackend().equals("CRM"))){
	   		
			ItemList items = salesDocument.getItems();
			for (int i = 0; i < items.size(); i++) {
				ItemSalesDoc itm = items.get(i);
				if (itm.getConfigType() != null &&
					itm.getConfigType().equals(ItemSalesDoc.ITEM_CONFIGTYPE_GRID)){
					IPCItem mainItem = (IPCItem) itm.getExternalItem();
			 		 	
					// if IpcItem techKey and mainISA item's techKey are not same,
					// then ipc sub-Items need to be created. 
					if (mainItem != null && !mainItem.getItemId().equals(itm.getTechKey().toString())){
					   salesDocument.copyIPCItem(itm.getTechKey());
					}
				}
			}
		}		
        if (transferItem != null) {
            // check if an old item should be replaced by the new one
            ItemSalesDoc itemToReplace = (ItemSalesDoc) transferItem.getBasketItemToReplace();

            if (itemToReplace != null) {
                // delete item from salesDocument
                salesDocument.removeItem(itemToReplace.getTechKey()); // may throw CommunicationException
            }
        }

        if (transferItemList != null) {
            Iterator i = transferItemList.iterator();

            while (i.hasNext()) {
                ItemSalesDoc itemToReplace = (ItemSalesDoc) ((BasketTransferItem) i.next()).getBasketItemToReplace();

                if (itemToReplace != null) {
                    // delete item from salesDocument
                    salesDocument.removeItem(itemToReplace.getTechKey()); // may throw CommunicationException
                }
            }
        }

        // After item had been written to backend it has a techkey and Shiptos can be added
        if ((transferItemList != null) && (shipToHandle > 0) && !scenario.equals("B2C")) {
            Iterator i = transferItemList.iterator();

            // Assign handle to item's techkey. Necessary since handle will be lost after readAllItems()
            while (i.hasNext()) {
                BasketTransferItem basketTransferItem = (BasketTransferItem) i.next();

                if (basketTransferItem instanceof BasketTransferItemImpl &&
                        (((BasketTransferItemImpl) basketTransferItem).getHandle() != null)) {
                    ItemSalesDoc itm = salesDocument.getFirstItemWithHandle(((BasketTransferItemImpl) basketTransferItem).getHandle());
                    ((BasketTransferItemImpl) basketTransferItem).setHandle(itm.getTechKey().getIdAsString());
                }
            }

            // first the salesDocument must be read from backend
            salesDocument.readHeader();
            salesDocument.readAllItems();

            i = transferItemList.iterator();

            PartnerListEntry soldToEntry = salesDocument.getHeader().getPartnerList().getSoldTo();

            while (i.hasNext()) {
                BasketTransferItem basketTransferItem = (BasketTransferItem) i.next();

                if (basketTransferItem instanceof BasketTransferItemImpl &&
                        (((BasketTransferItemImpl) basketTransferItem).getHandle() != null)) {
                    ItemSalesDoc itm = salesDocument.getItem(new TechKey(
                                ((BasketTransferItemImpl) basketTransferItem).getHandle()));

                    if (soldToEntry != null) {
                        salesDocument.addNewShipTo(itm.getTechKey(), null,
                            ((BasketTransferItemImpl) basketTransferItem).getShipTo().getAddress(),
                            soldToEntry.getPartnerTechKey(), shop.getTechKey());
                    }
                }
            }
        }
        
        // set minibasket info in very special case
		if (shop.isDocTypeLateDecision() && documentHandler != null && salesDocument.getItems().size() == 1) {
			documentHandler.setMiniBsktNetValue(salesDocument.getHeader().getNetValue());                                             
			documentHandler.setMiniBsktCurrency(salesDocument.getHeader().getCurrency());                                 
			documentHandler.setMiniBsktItemSize(String.valueOf(salesDocument.getItems().size()));                                 
			documentHandler.setMiniBsktDoctype(salesDocument.getHeader().getDocumentType());
		}

        // page to be displayed next
        String forwardTo = null;

        // determine forward
        // first check value SC_FORWARD_NAME, which will be set in
        // the create document action
        RequestParser.Value forwardValue = requestParser.getAttribute(SC_FORWARD_NAME).getValue();

        // then check request parameter
        // added for ocireceive
        if (!forwardValue.isSet()) {
            forwardValue = requestParser.getParameter(SC_FORWARD_NAME).getValue();
        }

        // then check request parameter
        if (!forwardValue.isSet()) {
            forwardValue = requestParser.getParameter(FORWARD_NAME).getValue();
        }

        // then check request attribute
        if (!forwardValue.isSet()) {
            forwardValue = requestParser.getAttribute(FORWARD_NAME).getValue();
        }

        // then check request parameter
        if (!forwardValue.isSet()) {
            forwardValue = requestParser.getParameter(FORWARD_NAME).getValue();
        }

        if (forwardValue.isSet()) {
			if(scenario.equals("B2C") ) {
			   if (webCat != null && webCat.getCurrentItemList() != null &&
                   webCat.getCurrentItemList().getQuery() != null &&
			       webCat.getCurrentItemList().getAreaQuery() == null) {
			      forwardTo = "showCatalogQuery";
			   } else {
			     forwardTo = forwardValue.getString();
			     }
			}
        	else {
               forwardTo = forwardValue.getString();
        	}
        }
        else {
            Object obj = request.getSession().getAttribute(AddToBasketAction.SC_FORWARD_NAME);

            if (obj != null) {
                forwardTo = (String) obj;
                request.getSession().removeAttribute(AddToBasketAction.SC_FORWARD_NAME);
            }
            else {
                forwardTo = FORWARD_SUCCESS;
            }
        }

        // the set forward will also set in the request context and could so
        // be reused in AfterAddToBasketAction which is call after the confirmation or
        // error message
        request.setAttribute(FORWARD_NAME, forwardTo);

        anim = "true";
        session.setAttribute(ANIMATION, anim);

        // Handle all errors on the document page!!!!!
        //   if (!salesDocument.isValid() && salesDocument.hasMessages()) {
        //       // create a Message Displayer for the error page!
        //       MessageDisplayer messageDisplayer = new MessageDisplayer(forwardTo,true);
        //       messageDisplayer.setApplicationError(false);
        //       messageDisplayer.addToRequest(request,salesDocument);
        //       // the action which is to call after the message
        //       messageDisplayer.setAction("afteraddtodocument.do");
        //       forwardTo = FORWARD_ERROR;
        //   }
        // clear business object
        salesDocument.clearData();   

        // if its bB2Cand the basket supports recovery, store the basketGUID in a cookie if not already done
        if (scenario.equals("B2C") &&
                salesDocument.isDocumentRecoverable() &&
                (userSessionData.getAttribute(RecoverBasketCookieHandler.SC_RECO_BASKET_COOKIE_HDLR) == null)) {
            RecoverBasketCookieHandler recoBasketCookieHdlr = new RecoverBasketCookieHandler(request, response);

            recoBasketCookieHdlr.save(shop, salesDocument.getTechKey());
            userSessionData.setAttribute(RecoverBasketCookieHandler.SC_RECO_BASKET_COOKIE_HDLR, recoBasketCookieHdlr);
        }

        log.exiting();

        return mapping.findForward(forwardTo);
    }
    
    
    /**
     * Get the product techkey that corresponds
     * to the target document type.
     * 
     */
    protected String mapProductId(UserSessionData userSessionData, 
                                  SalesDocument salesDocument,
                                  BasketTransferItem basketTransferItem) 
            throws CommunicationException {
       
 	   AppBaseBusinessObjectManager appBaseBom = (AppBaseBusinessObjectManager) userSessionData.getBOM(AppBaseBusinessObjectManager.APPBASE_BOM);
	   IdMapper idMapper = appBaseBom.createIdMapper();
	   
       if (basketTransferItem instanceof BasketTransferItemImpl )  {  
       	    // TODO in case of order templates mapping is required  
//            return basketTransferItem.getProductKey();
            
 		   return idMapper.mapIdentifier(IdMapper.PRODUCT,                                            // object to map id for 
			                             ((BasketTransferItemImpl)basketTransferItem).getSource(),    // source  
					                     salesDocument.getHeader().getDocumentType(),                 // target 
					                     basketTransferItem.getProductKey());
       }
       else {
	   
		   return idMapper.mapIdentifier(IdMapper.PRODUCT,                             // object to map id for 
			   	   			             IdMapper.CATALOG,                             // source  
										 salesDocument.getHeader().getDocumentType(),  // target 
										 basketTransferItem.getProductKey(),
										 basketTransferItem.getProductId());
	   }      
    }	
    
}


