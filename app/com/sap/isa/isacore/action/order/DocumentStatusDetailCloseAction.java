/*****************************************************************************
    Class:        DocumentStatusDetailCloseAction
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author        Ralf Witt
    Created:      May 2001

    $Revision: #1 $
    $Date: 2001/06/26 $
*****************************************************************************/

package com.sap.isa.isacore.action.order;

import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.billing.BillingStatus;
import com.sap.isa.businessobject.order.CollectiveOrderStatus;
import com.sap.isa.businessobject.order.CustomerOrderStatus;
import com.sap.isa.businessobject.order.OrderStatus;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.ui.uicontrol.UIController;


/**
 * Handling to close a document in status display mode
 *
 * <br> Note that all Action classes have to provide a non argument constructor
 * because they were instantiated automatically by the action servlet.
 *
 * @author  Ralf Witt
 * @version 0.1
 *
 */
public class DocumentStatusDetailCloseAction extends DocumentStatusBaseAction {

	/**
	 * Release the Order status. <br>
     * The method release the document from the given document handle and 
     * from the given bom. The method works for the following classes
     * <ul>
     *  <li>{@link com.sap.isa.businessobject.order.OrderStatus} </li>
     *  <li>{@link com.sap.isa.businessobject.order.CollectiveOrderStatus} </li>
     *  <li>{@link com.sap.isa.businessobject.order.CustomerOrderStatus} </li>
     * </ul>
     * 
	 * @param orderStatus order status doccument
	 * @param documentHandler the actual document handler
	 * @param bom the business object manager
	 */
    public static void releaseOrderStatus(OrderStatus orderStatus, 
                                          DocumentHandler documentHandler,
                                          BusinessObjectManager bom,
										  UserSessionData userSessionData) {

		// delete individual UI elements 
	    UIController uiController = UIController.getController(userSessionData);
	    if (uiController != null) {
		   // delete UI elements of header
		   uiController.deleteUIElementsInGroup("order.header",orderStatus.getOrder().getHeaderData().getTechKey().getIdAsString());
		   Iterator itemsIterator = orderStatus.getItemListData().iterator();
		   // delete UI elements of items
		   while (itemsIterator.hasNext()) {
			   ItemData itemData = (ItemData) itemsIterator.next();
			   uiController.deleteUIElementsInGroup("order.item", itemData.getTechKey().toString());
		   }			
	    }  
	    
        ManagedDocument preMDoc = documentHandler.getManagedDocumentOnTop().getAssociatedDocument();
        documentHandler.release(orderStatus);
        documentHandler.add(preMDoc);

        // release Status objects from BOM
        if (orderStatus instanceof CollectiveOrderStatus) {
            bom.releaseCollectiveOrderStatus();
        } 
        else if (orderStatus instanceof CustomerOrderStatus) {
            bom.releaseCustomerOrderStatus();
        }
        else {    
            bom.releaseOrderStatus();
        }    
    
    }                                
        

	/**
	 * @see com.sap.isa.isacore.action.order.DocumentStatusBaseAction#performBillingStatus(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse, UserSessionData, RequestParser, BusinessObjectManager, BillingStatus, IsaLocation)
	 */
	public ActionForward performBillingStatus(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		BusinessObjectManager bom,
		BillingStatus billingStatus,
		IsaLocation log)
		throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        DocumentHandler documentHandler = (DocumentHandler)
                userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);

        documentHandler.release(billingStatus);
        
        bom.releaseBillingStatus();
        
        if (request.getParameter("externalforward") == null) {
			log.exiting();
            return mapping.findForward("success");
        } else {
			log.exiting();
            return mapping.findForward(request.getParameter("externalforward"));
        }

	}

	/**
	 * @see com.sap.isa.isacore.action.order.DocumentStatusBaseAction#performOrderStatus(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse, UserSessionData, RequestParser, BusinessObjectManager, OrderStatus, IsaLocation)
	 */
	public ActionForward performOrderStatus(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		BusinessObjectManager bom,
		OrderStatus orderStatus,
		IsaLocation log)
		throws CommunicationException {
		final String METHOD_NAME = "performOrderStatus()";
		log.entering(METHOD_NAME);
        DocumentHandler documentHandler = (DocumentHandler)
                userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);

		 
		releaseOrderStatus(orderStatus, documentHandler, bom, userSessionData);     
        
        log.exiting();
        return mapping.findForward("success");
	}

}