/*****************************************************************************
    Class         InitItemConfigurationBaseAction
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP
    Created:      16.08.2001
    Version:      1.0

    $Revision: #15 $
    $Date: 2001/08/01 $
*****************************************************************************/

package com.sap.isa.isacore.action.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 * Base class for initializing the configuration.
 * This class is abstract. Implement the <code>getConfigurationDoc</code> method to
 * use the class.
 * The <code>isaPerform</code> method will taken the key of the item to be configure from
 * the request context and store it in the userSessionData. 
 *
 * @author SAP
 * @version 1.0
 */
public abstract class InitItemConfigurationBaseAction extends IsaCoreBaseAction {

    private static final String FORWARD_SUCCESS = "success";

	public abstract ManagedDocument getConfiguratorDoc(DocumentHandler documentHandler,
													   UserSessionData userSessionData,
    												   BusinessObjectManager bom);

    /**
     * Overriden <em>isaPerform</em> method of <em>IsaCoreBaseAction</em>.
     */
    public ActionForward isaPerform(ActionMapping mapping,
                ActionForm form,
                HttpServletRequest request,
                HttpServletResponse response,
                UserSessionData userSessionData,
                RequestParser parser,
                BusinessObjectManager bom,
                IsaLocation log)
                        throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        DocumentHandler documentHandler = (DocumentHandler)
        userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
        if (documentHandler == null) {
			log.exiting();
            throw new PanicException("No document handler found");
        }
        
        ManagedDocument onTopDoc = documentHandler.getManagedDocumentOnTop();        

      	ManagedDocument configuratorDoc = getConfiguratorDoc(documentHandler, userSessionData, bom); 
   
		// store the actual document in the Managed Document for Configuration	       
  	 	configuratorDoc.setAssociatedDocument(documentHandler.getManagedDocumentOnTop()); 
                
  	 	documentHandler.add(configuratorDoc);

  	 	documentHandler.setOnTop(configuratorDoc.getDocument());
 
  		TechKey techKey = null;

      	if (parser.getAttribute("itemId").isSet()) {
            techKey = new TechKey(parser.getAttribute("itemId").getValue().getString());
        }
        else {
            techKey = new TechKey(parser.getParameter("itemId").getValue().getString());
        }
      	userSessionData.setAttribute(ItemConfigurationBaseAction.SC_ITEM_IN_CONFIG, techKey.getIdAsString());
		log.exiting();
      	return mapping.findForward(FORWARD_SUCCESS);
	}
}