/*****************************************************************************
    Class:        DocumentStatusBaseAction
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author        SAP
    Created:      November 2002

    $Revision: #1 $
    $Date: 2001/06/26 $
*****************************************************************************/

package com.sap.isa.isacore.action.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.billing.BillingStatus;
import com.sap.isa.businessobject.order.OrderStatus;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.action.ActionConstants;
import com.sap.isa.isacore.action.ActionConstantsBase;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 * Base action for Document Status Actions.
 * The base class determine the correct document type from 
 * the document handler.
 *
 */
abstract public class DocumentStatusBaseAction extends IsaCoreBaseAction {

   /**
   * document stored in request scope.
   */
  public static final String RK_ORDER_STATUS_DETAIL = "orderstatusdetail";
  public static final String RK_ORDER_STATUS_DETAIL_ITEMITERATOR = "itemiterator";
  public static final String RK_BILLING_STATUS_DETAIL = "billingstatusdetail";
  public static final String RK_BILLING_STATUS_DETAIL_ITEMITERATOR = "itemiterator";
  public static final String RK_SOLDTO_NAME = "soldtoname";
  public static final String RK_SOLDTO_TECHKEY = "soldtotechkey";
  public static final String RK_CHECKBOXNAME = "itemcheckbox";
  public static final String RK_CHECKBOXHIDDEN = "itemcheckboxhidden";
 
    /**
     * Perform order status.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param orderStatus       The order status object
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    abstract public ActionForward performOrderStatus(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            BusinessObjectManager bom,
            OrderStatus orderStatus,
            IsaLocation log)
                    throws CommunicationException;


    /**
     * Perform order status.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param orderStatus       The order status object
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward performBillingStatus(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            BusinessObjectManager bom,
            BillingStatus billingStatus,
            IsaLocation log)
            throws CommunicationException {
        return mapping.findForward("error");                
     }             


    /**
     * Handles the action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            BusinessObjectManager bom,
            IsaLocation log)
            throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        DocumentHandler documentHandler = getDocumentHandler(userSessionData);
            
        ManagedDocument mdoc = documentHandler.getManagedDocumentOnTop();
        
        DocumentState document = mdoc.getDocument();
       
        if (document instanceof BillingStatus) {
            BillingStatus billingStatus = (BillingStatus) document;
			log.exiting();
            return performBillingStatus(mapping,
                                        form,
                                        request,
                                        response,
                                        userSessionData,
                                        requestParser,
                                        bom,
                                        billingStatus,
                                        log);           
        } 
        else if (document instanceof OrderStatus) {
            OrderStatus orderStatus = (OrderStatus) document; 
			log.exiting();   
            return performOrderStatus(mapping,
                                      form,
                                      request,
                                      response,
                                      userSessionData,
                                      requestParser,
                                      bom,
                                      orderStatus,
                                      log);           
        }       
        else {    
			log.exiting();
            return mapping.findForward("error");
        }     
    }
    /**
     * Method to retrieve the DocumentHandler. If billing docuemnts run outside ISales frames 
     * (isBillingIncluded = false), then the appropriated document handler has to be returned.
     * @return DocumentHandler docHandler
     */
    protected DocumentHandler getDocumentHandler(UserSessionData userSessionData) {
        String applArea = (String)userSessionData.getAttribute(ActionConstants.SC_APPLAREA);
        DocumentHandler docHandler = null;
        if ( ActionConstants.APPLAREA_SEPBILL.equals(applArea)) {
            docHandler = (DocumentHandler) userSessionData.getAttribute(ActionConstantsBase.DOCUMENT_HANDLER_BILL);
        } else {
            docHandler = (DocumentHandler) userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
        }
        return docHandler;
    }
    /**
     * Return <code>true</code> if the current Application area is the separate billing screen.
     * @return boolean
     */
    protected boolean isSeparateBilling(UserSessionData userSessionData) {
        return (ActionConstants.APPLAREA_SEPBILL.equals(userSessionData.getAttribute(ActionConstants.SC_APPLAREA)));
    }
}