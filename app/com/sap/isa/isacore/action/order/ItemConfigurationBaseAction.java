/*****************************************************************************
    Class         ItemConfigurationBaseAction
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP AG
    Created:      19.09.2001
    Version:      1.0

    $Revision: #15 $
    $Date: 2001/08/01 $
*****************************************************************************/
package com.sap.isa.isacore.action.order;

import javax.servlet.http.HttpServletRequest;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.ipc.IPCBOManager;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.core.FrameworkConfigManager;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.interaction.InteractionConfig;
import com.sap.isa.core.interaction.InteractionConfigContainer;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.Message;
import com.sap.isa.ipc.ui.jsp.action.InternalRequestParameterConstants;
import com.sap.isa.ipc.ui.jsp.action.RequestParameterConstants;
import com.sap.isa.isacore.action.ActionConstants;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.maintenanceobject.businessobject.UIElement;
import com.sap.isa.ui.uicontrol.UIController;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.OriginalRequestParameterConstants;
import com.sap.spc.remote.client.object.imp.IPCClientObjectFactory;
import com.sap.spc.remote.client.object.imp.rfc.RFCIPCItemReference;


/**
 * Class to provide parameter for ipc-call and forward to IPC
 *
 * @author Stefan Dendl
 * @version 1.0
 */
public class ItemConfigurationBaseAction extends IsaCoreBaseAction {
    public static final String FORWARD_IPC_CONFIGURATION = "success";
    public static final String SC_CONFIGURATION_ACTIVE = "com.sap.isa.isacore.action.order.ItemConfigurationAction.configactive";
    public static final String SC_ITEM_IN_CONFIG = "com.sap.isa.isacore.action.order.ItemConfigurationAction.iteminconfig";
    public static final String CONFIG_ACTIVE_TRUE = "TRUE";
    public static final String CONFIG_ACTIVE_FALSE = "FALSE";

    /**
     * Name of the request parameter
     */
    public static final String PARAM_ITEMID = "itemId";
    public static final String PARAM_B2C_BASKET = "b2cBasket";

    /**
     * Overriden <em>isaPerform</em> method of <em>IsaCoreBaseAction</em>.
     *
     * @deprecated please use setConfigurationParams(String caller, ItemSalesDoc item,
     *                                    HeaderSalesDocument header,
     *                                    boolean deleteExternalItem,
     *                                    UserSessionData userSessionData,
     *                                    HttpServletRequest request)
     *
     * @param caller DOCUMENT ME!
     * @param item DOCUMENT ME!
     * @param header DOCUMENT ME!
     * @param userSessionData DOCUMENT ME!
     * @param request DOCUMENT ME!
     */
    protected void setConfigurationParams(String caller, ItemSalesDoc item, HeaderSalesDocument header, UserSessionData userSessionData, HttpServletRequest request) throws CommunicationException {
		final String METHOD_NAME = "setConfigurationParams()";
		log.entering(METHOD_NAME);
        setConfigurationParams(caller, item, header, true, userSessionData, request);
		log.exiting();
    }
    
    /**
     * Sets some parameters needed to initialize the configuration
     * 
     * @deprecated please use setConfigurationParams(String caller,
     *                                    ItemSalesDoc item,
     *                                    HeaderSalesDocument header,
     *                                    SalesDocument salesDoc,
     *                                    boolean deleteExternalItem,
     *                                    UserSessionData userSessionData,
     *                                    HttpServletRequest request)
     *
     * @param caller DOCUMENT ME!
     * @param item DOCUMENT ME!
     * @param header DOCUMENT ME!
     * @param deleteExternalItem should the externalItem in the itemSalesDoc be set to null
     * @param userSessionData DOCUMENT ME!
     * @param request DOCUMENT ME!
     */
     protected void setConfigurationParams(String caller, ItemSalesDoc item, HeaderSalesDocument header, boolean deleteExternalItem, UserSessionData userSessionData, HttpServletRequest request) throws CommunicationException {
         final String METHOD_NAME = "setConfigurationParams()";
         log.entering(METHOD_NAME);
         setConfigurationParams(caller, item, header, null, deleteExternalItem, userSessionData, request);
         log.exiting();                 
     }

    /**
    * Sets some parameters needed to initialize the configuration
    *
    * @param caller DOCUMENT ME!
    * @param item DOCUMENT ME!
    * @param header DOCUMENT ME!
    * @param salesDoc DOCUMENT ME!
    * @param deleteExternalItem should the externalItem in the itemSalesDoc be set to null
    * @param userSessionData DOCUMENT ME!
    * @param request DOCUMENT ME!
    */
    protected void setConfigurationParams(String caller, ItemSalesDoc item, HeaderSalesDocument header, SalesDocument salesDoc,  boolean deleteExternalItem, UserSessionData userSessionData, HttpServletRequest request) throws CommunicationException {
        RFCIPCItemReference ipcItemReference;
        boolean isConfigurationChangeable = false;

        if (item == null) {
            throw new PanicException("Empty item provided");
        }
        
        UIController uiController = null;
		uiController = UIController.getController(userSessionData);
		UIElement uiField = uiController.getUIElement("order.item.configuration", item.getTechKey().getIdAsString());
		
		if (!item.hasAuctionRelation()) {
            if (uiField != null && !uiField.isDisabled()) {
                isConfigurationChangeable = true;
            }
		}
        else {
            log.debug("Item is auction related so configuration is shown in display only mode");
        }
        
        // caller
        request.setAttribute("caller", caller);

        // set display mode
        if (isConfigurationChangeable == false) {
            request.setAttribute(OriginalRequestParameterConstants.DISPLAY_MODE, RequestParameterConstants.T);
        }
        else {
            request.setAttribute(OriginalRequestParameterConstants.DISPLAY_MODE, RequestParameterConstants.F);
        }
        
		// set price display
		boolean showConfigPrices = true;
        boolean showConfigVariants = true;
        
		if (getInteractionConfig(request) != null &&
			getInteractionConfig(request).getConfig(ActionConstants.IN_UI_CONFIG_ID) != null) {
			InteractionConfig uiConf = getInteractionConfig(request).getConfig(ActionConstants.IN_UI_CONFIG_ID);
        
			String showConfigPricesStr = uiConf.getValue(ActionConstants.SHOW_CONFIG_PRICES);
			if (showConfigPricesStr != null) {
				showConfigPrices = !showConfigPricesStr.equals("false");
			}
            

			if (!isConfigurationChangeable) {
				showConfigVariants = false;
			}
			else {
				String showConfigVariantsStr = uiConf.getValue(ActionConstants.SHOW_CONFIG_VARIANTS);
				if (showConfigVariantsStr != null || !isConfigurationChangeable) {
					showConfigVariants = !showConfigVariantsStr.equals("false");
				}
			}
		}
        
		if (!showConfigPrices) {
			log.debug("Don't show prices for configurable products");
			request.setAttribute(OriginalRequestParameterConstants.HIDE_PRICES, RequestParameterConstants.T);
		}
        
        // set ipcItemReference
        Object obj = item.getExternalItem();

        // set attribute only_var_find
        IPCItem ipcItem = null;
        boolean changeableVariant = false;
       
        if (obj != null && obj instanceof IPCItem) {
            ipcItem = (IPCItem) obj;
            changeableVariant = ipcItem.getItemProperties().isChangeableProductVariant();
        }
        
        if (changeableVariant) {
            log.debug("Don't show variants in Config UI for changeable variant");
            request.setAttribute(OriginalRequestParameterConstants.ENABLE_VARIANT_SEARCH, RequestParameterConstants.F);
        }
        else if (!showConfigVariants) {
            log.debug("XCM - Don't show variants in Config UI for configurable products");
            request.setAttribute(OriginalRequestParameterConstants.ENABLE_VARIANT_SEARCH, RequestParameterConstants.F);
        }
        else {
            log.debug("Show variants in Config UI for configurable products");
            request.setAttribute(OriginalRequestParameterConstants.ENABLE_VARIANT_SEARCH, RequestParameterConstants.T);
        }
        
        // activate variantsearch
        //request.setAttribute(OriginalRequestParameterConstants.ENABLE_VARIANT_SEARCH, RequestParameterConstants.T);

        // request.setAttribute(RequestParameterConstants.ENABLE_AUTOMATIC_SEARCH, RequestParameterConstants.T);

        // set scenario
        request.setAttribute(OriginalRequestParameterConstants.IPC_SCENARIO, "ISA");

        InteractionConfigContainer interactionConfigContainer = getInteractionConfig(request);
        InteractionConfig          interactionConfig = interactionConfigContainer.getConfig("ipc_scenario");

        if (interactionConfig != null) {
            String ipcScenario = interactionConfig.getValue("scenario.basket");

            if (ipcScenario == null) {
                log.warn(LogUtil.APPS_COMMON_CONFIGURATION, "XCM Scenario " + "scenario.basket" + " for IPC Product Configuration not found");
            }

            request.setAttribute(InternalRequestParameterConstants.IPC_XCM_SCENARIO, ipcScenario);
        }
        else {
            log.warn(LogUtil.APPS_COMMON_CONFIGURATION, "Configuration for ipc_scenario not found in interaction-config.xml");
        }
       
        if (item.getOnlyVarFind() == true) {
            request.setAttribute(OriginalRequestParameterConstants.ONLY_VAR_FIND, RequestParameterConstants.T);
        }
        else {
            request.setAttribute(OriginalRequestParameterConstants.ONLY_VAR_FIND, RequestParameterConstants.F);
        }


		String connectionKey = header.getIpcConnectionKey();
        if (obj != null) {
            if (obj instanceof IPCItem) {
                ipcItem = (IPCItem) obj;
                ipcItemReference = (RFCIPCItemReference)ipcItem.getItemReference();
				ipcItemReference.setConnectionKey(connectionKey);

                if (log.isDebugEnabled()) {
                    log.debug(ipcItemReference.toString());
                }
            }
            else {   
                ipcItemReference = (RFCIPCItemReference) item.getExternalItem();
            }

            if (deleteExternalItem) {
                item.setExternalItem(null); //reset external item
                // to ensure that that the external item will be set
                // after the return/cancellation of the configuration
                if (salesDoc != null) {
                    salesDoc.setDirty(true); 
                }                     
            }
        }
        else {
            ipcItemReference = (RFCIPCItemReference)IPCClientObjectFactory.getInstance().newIPCItemReference();

            if (header.getIpcDocumentId() != null) {
                ipcItemReference.setDocumentId(header.getIpcDocumentId().getIdAsString());
            }

            ipcItemReference.setItemId(item.getTechKey().getIdAsString());
            ipcItemReference.setConnectionKey(header.getIpcConnectionKey());
        }
        

        // get contract filter (if existing)
        String contractFilter = item.getContractConfigFilter();

        if ((contractFilter != null) && (contractFilter.length() > 0) && !stringIsFilledWith(contractFilter, '0')) {
            request.setAttribute("restrictId", contractFilter);
            request.setAttribute("ownerType", "CONTRACT");
            request.setAttribute("repositoryType", "IBASE");
            request.setAttribute("ownerId", item.getContractKey().getIdAsString());
        }

        IPCBOManager ibom = this.getIPCBusinessObjectManager(userSessionData);

        request.setAttribute(RequestParameterConstants.IPC_ITEM_REFERENCE, ipcItemReference);

        //    String configOnlineEval = servletContext.getInitParameter("configOnlineEvaluate.isa.sap.com");
        //		Parameter configOnlineEvaluate moved from web.xml to xcm-configuration (4.0 SP06)
        String attributeName = "configOnlineEvaluate";
        String configOnlineEval = FrameworkConfigManager.Servlet.getInteractionConfigParameter(request.getSession(), "ui", attributeName, attributeName + ".isa.sap.com");

        if ((configOnlineEval != null) && (configOnlineEval.equalsIgnoreCase("true"))) {
            request.setAttribute(RequestParameterConstants.ONLINE_EVALUATE, RequestParameterConstants.T);
        }
        else {
            request.setAttribute(RequestParameterConstants.ONLINE_EVALUATE, RequestParameterConstants.F);
        }
		//required for Grid products for pricing at Item / sub-item level
		request.setAttribute("pricing.statistical", item.getStatisticPrice());

    }

    /**
    * Checks if the item has a knowledge base in the IPC
    *
    * @param header The salesdocument's header
    * @param item The ItemSalesDoc to check
    */
    protected boolean isItemConfigurableInIPC(ItemSalesDoc item) {
        boolean retVal = false;

        if (item.isConfigurable()) {
            retVal = true;

            Object externalItem = item.getExternalItem();

            if ((externalItem != null) && externalItem instanceof IPCItem) {
                IPCItem ipcItem = (IPCItem) externalItem;

                if (!ipcItem.isConfigurable()) {
                    Message msg = new Message(Message.ERROR, "ordr.item.no.kb");
                    item.getMessageList().clear();
                    item.addMessage(msg);

                    retVal = false;
                }
            }
        }

        return retVal;
    }

    /**
     * DOCUMENT ME!
     */
    public void initialize() {
        if (isB2C()) {
            this.checkUserIsLoggedIn = false;
        }
    }
}
