/*****************************************************************************
    Class         ChangeOrderAction
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:
    Created:      02. April 2001
    Version:      3.0
*****************************************************************************/
package com.sap.isa.isacore.action.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.order.CollectiveOrder;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;

/**
 * Display the collective order for change purposes.
 * The action take the actual document from the bom and
 * initiate the change order process.
 *
 * @author SAP
 * @version 2.0
 *
 */
public class ChangeCollectiveOrderAction extends ChangeOrderBaseAction {


    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser parser,
            BusinessObjectManager bom,
            IsaLocation log) throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        DocumentHandler documentHandler = (DocumentHandler)
                userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);

        CollectiveOrder collectiveOrder = bom.getCollectiveOrder();

        if (collectiveOrder == null) {
			log.exiting();
           return mapping.findForward("failure");
        }

        setSalesDocument(HeaderSalesDocument.DOCUMENT_TYPE_COLLECTIVE_ORDER,
                         collectiveOrder,
                         collectiveOrder.getHeader(),
                         bom,userSessionData,
                         documentHandler,
                         ORDER_CHANGE);
		log.exiting();
        return mapping.findForward("changeorder");
    }

}