/*****************************************************************************
  Class:        MaintainGridCatalogCancelAction
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Anil Krishnamurthy
  Created:      16.02.2005
  Version:      1.0

  $Revision: #3 $
  $Date:  $
*****************************************************************************/

package com.sap.isa.isacore.action.order;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.businessobject.negotiatedcontract.NegotiatedContract;
import com.sap.isa.businessobject.order.Order;
import com.sap.isa.businessobject.ordertemplate.OrderTemplate;
import com.sap.isa.businessobject.quotation.Quotation;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 * Action to delete the grid item from the basket, when cancel is clicked in 
 * IPC grid screen(if comes from catalog) and returns to catalog 
 * <br><br>
 * The following forwards are used by this action:
 * </p>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>FORWARD_UPDATEDOCUMENT</td><td>always used</td></tr>
 * </table>
 *
 * @author SAP
 * @version 1.0
 */
public class MaintainGridCatalogCancelAction extends IsaCoreBaseAction {
    
	protected String FORWARD_SUCCESS  = "success";

    
	/**
	 * @see com.sap.isa.isacore.action.IsaCoreBaseAction#isaPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.sap.isa.core.UserSessionData, com.sap.isa.core.util.RequestParser, com.sap.isa.businessobject.BusinessObjectManager, com.sap.isa.core.logging.IsaLocation)
	 */
	public ActionForward isaPerform(ActionMapping mapping,
									ActionForm form,
									HttpServletRequest request,
									HttpServletResponse response,
									UserSessionData userSessionData,
									RequestParser requestParser,
									BusinessObjectManager bom,
									IsaLocation log)
			throws IOException, ServletException, CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
		DocumentHandler documentHandler = (DocumentHandler)
		userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
		if (documentHandler == null) {
			log.exiting();
			throw new PanicException("No document handler found");
		}
		SalesDocument salesDocument = null;

		ManagedDocument onTopDoc = documentHandler.getManagedDocumentOnTop();

		// todo: kein docontop da
		DocumentState targetDocument = onTopDoc.getDocument(); 

//		get the existing target document from the bom
		if (targetDocument instanceof Quotation) {
			 salesDocument = bom.getQuotation();
		}
		else if (targetDocument instanceof OrderTemplate) {
			 salesDocument = bom.getOrderTemplate();
		}
		else if (targetDocument instanceof Order) {
			 salesDocument = bom.getOrder();
		}
		else if (targetDocument instanceof NegotiatedContract) {
			 salesDocument = bom.getNegotiatedContract();
		}
		else {
			 salesDocument = bom.getBasket();
		}
		if (salesDocument == null) {
			log.exiting();
			throw new PanicException("No sales document returned from backend");
		}		
        
		salesDocument.readAllItemsForUpdate();
		ItemSalesDoc itemToDelete = salesDocument.getItems().get(salesDocument.getItems().size()-1);
		
		TechKey[] itemTechKey = new TechKey[1];
		itemTechKey[0]= itemToDelete.getTechKey();
		salesDocument.deleteItems(itemTechKey);

		log.exiting();
		return mapping.findForward(FORWARD_SUCCESS);
	}

}