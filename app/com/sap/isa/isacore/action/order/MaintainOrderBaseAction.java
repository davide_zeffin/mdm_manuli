/*****************************************************************************
  Class:        MaintainOrderBaseAction
  Copyright (c) 2001, SAP AG, All rights reserved.
  Author:
  Created:      18.09.2001
  Version:      1.0

  $Revision: #9 $
  $Date: 2003/02/12 $
*****************************************************************************/

package com.sap.isa.isacore.action.order;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businessobject.item.ItemList;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.businessobject.order.CollectiveOrder;
import com.sap.isa.businessobject.order.CollectiveOrderChange;
import com.sap.isa.businessobject.order.CustomerOrder;
import com.sap.isa.businessobject.order.CustomerOrderChange;
import com.sap.isa.businessobject.order.Order;
import com.sap.isa.businessobject.order.OrderChange;
import com.sap.isa.businessobject.ordertemplate.OrderTemplate;
import com.sap.isa.businessobject.ordertemplate.OrderTemplateChange;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.GenericFactory;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.MessageList;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.action.IsaCoreInitAction;
import com.sap.isa.isacore.action.SalesDocumentParser;
import com.sap.isa.isacore.action.campaign.AddCampaignToCatalogueAction;
import com.sap.isa.isacore.action.campaign.GetCampaignAction;

/**
 * Base action for all actions providing functionality to manage the shopping
 * basket and sales documents similar to the basket. <br>
 * <p>
 * This action implements a template pattern, to keep the specialized
 * actions as simple as possible. All necessary data is retrieved by this
 * class and offered to the subclasses using a template design pattern.<br>
 * Subclasses are supposed to overwrite the abstract
 * <code>basketPerform</code> method and they normally do not touch the
 * <code>isaPerform</code> method.<br><br>
 * </p>
 * <p>
 * The following forwards are used by this action:
 * </p>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>FORWARD_UPDATEDOCUMENT</td><td>forward used if something goes wrong to allow application to reach an consitent state again</td></tr>
 * </table>

 * @author
 * @version 1.0
 */
public abstract class MaintainOrderBaseAction extends MaintainGridBaseAction {

    /**
     * Session context constant for the type of the currently edited document
     */
    public static final String SC_DOCTYPE = "com.sap.isa.isacore.action.order.MaintainBasketAction.doctype";
   
    /**
     * Session context constant for storage of the list of available ship tos
     */
    public static final String SC_SHIPTOS = "com.sap.isa.isacore.action.order.MaintainBasketAction.shiptos";

    /**
     * Request context constant for storage of the items of the document
     */
    public static final String RC_ITEMS      = "items";


	public static final String RC_SINGLE_ITEM      = "item";
	
    /**
     * Request context constant for storage of the header of the document
     */
    public static final String RC_HEADER     = "hdr";

    /**
     * Request context constant for storage of messages attached to the document
     */
    public static final String RC_MESSAGES   = "messages";

    /**
     * Request context constant for storage of the whole document
     */
    public static final String RC_ORDER     = "order";

    /**
     * Request context constant for storage of the available shipping conditions
     */
    public static final String RC_SHIPCOND   = "shipcond";
    
	/**
	 * Request context constant for storage of campaign descriptions
	 */
	public static final String RC_CAMPDESC   = "campdesc";
	
	/**
	 * Request context constant for storage of campaign type descriptions
	 */
	public static final String RC_CAMPTYPEDESC   = "camptypedesc";
    
    /**
     * Request context constant for the recoverability of the current document
     */
    public static final String RC_DOC_RECOVERABLE = "docrecov";
    
    /**
     * Request context constant to store if the document is in change mode document
     */
    public static final String RC_IS_CHANGE_MODE = "changemode";
    
    /**
     * Request context constant to store if the document is a large document
     */
    public static final String RC_IS_LARGE_DOC = "largedoc";

    /**
     * Request context constant to indicate an exit from the configuration
     */
    public static final String RC_EXITCONFIG = "exitconfig";
    
    /**
     * Request context constant to store the configuration info
     */
    public static final String RC_CONFIG_INFO = "configinfo";
    
    /**
     * Request context constant to store the configuration detail info
     */
    public static final String RC_CONFIG_INFO_DETAIL = "configinfodetail";
    
    /**
	* Document type constant to indicate a order
	*/
   	public static final String DOCTYPE_ORDER        = "order";
	
    /**
     * Constant for available forwards, value is &quot;newshipto&quot;.
     */
    protected static final String FORWARD_NEW_SHIPTO         = "newshipto";
    /**
    /**
     * Constant for available forwards, value is &quot;updatedocumentview&quot;.
     */
    protected static final String FORWARD_UPDATEDOCUMENT     = "updatedocumentview";

    /**
     * Constant for available forwards, value is &quot;closeconfig&quot;.
     */
    protected static final String FORWARD_CLOSE_CONFIG       = "closeconfig";

    /**
     * Constant for available forwards, value is &quot;changeorder&quot;.
     */
    protected static final String FORWARD_CHANGEORDER = "changeorder";


    /**
     * Constant for available forwards, value is &quot;changeorder&quot;.
     */
    protected static final String FORWARD_CHANGECUSTOMERORDER = "changecustomerorder";


    /**
     * Constant for available forwards, value is &quot;confirmationscreen&quot;.
     */
    protected static final String FORWARD_CONFIRMATION_SCREEN = "confirmationscreen";

    /**
     * Constant for available forwards, value is &quot;leaveorder&quot;.
     */
    protected static final String FORWARD_LEAVE = "leaveorder";

    /**
     * Constant for available forwards, value is &quot;configitem&quot;.
     */
    protected static final String FORWARD_CONFIG_ITEM         = "configitem";
    
    /**
     * Constant for available forwards, value is &quot;showbatchframe&quot;.
     */
    
    protected static final String FORWARD_SHOWBATCH			= "itembatch";

    /**
     * Constant for available forwards, value is &quot;proddet&quot;.
     */
    protected static final String FORWARD_PRODDET         = "proddet";

    /**
     Request context constant for storage of the card type
     */
    public static final String RC_CARDTYPE   = "cardtype";
    
    /**
     * Parser to parse the request
     */
    protected static final SalesDocumentParser STANDARD_PARSER = (SalesDocumentParser )GenericFactory.getInstance("salesDocumentStandardParser");

    /**
    * Some dummy constants for testing purposes
    */
    public static final String DUMMY_CONDITIONS    = "dummyconditions";

    /**
    * Some dummy constants for testing purposes
    */
    public static final String DUMMY_DATES         = "dummydates";

    /**
    * Some dummy constants for testing purposes
    */
    public static final String DUMMY_ITEMS         = "dummyitems";

    /**
     * Some dummy constants for testing purposes
    */
    public static final String MORE_DUMMY_DATES    = "moredummydates";

    /**
    * Some dummy constants for testing purposes
    */
    public static final String TECH_KEY         = "techkey";
	
    /**
     * This Method returns the correct OrderChange decorator for a given sales document.
     * 
     * @param order order Document for which a decorator is needed 
     * 
     * @return OrderChange decorator of the correct type
     */
    public static OrderChange getOrderChangeDecorator(SalesDocument order) {
        
        OrderChange orderChange = null;
    
               // get necessary business objects
        if(order instanceof OrderTemplate) {
            orderChange = new OrderTemplateChange((OrderTemplate)order);
        }
        else if(order instanceof CollectiveOrder) {
            orderChange = new CollectiveOrderChange((CollectiveOrder)order);
        }
        else if(order instanceof CustomerOrder) {
            orderChange = new CustomerOrderChange((CustomerOrder)order);
        }
        else if(order instanceof Order) {
            orderChange = new OrderChange((Order)order);
            
        }
        return orderChange;
    }



    /**
     * Helper method to parse the request and construct items
     * for the <code>SalesDocument</code> form the
     * request's data.
     * Note: If this method is called the userSessionData parameter in the
     * customerExitParseRequestItems method is null and therefore can't be used.
     * <p>
     * The following fields are taken from the request and used to
     * construct the item:
     * </p>
     * <table border="1" cellspacing="0" cellpadding="2">
     *   <tr>
     *     <td><b>field</b></td>
     *     <td><b>use</b></td>
     *   </tr>
     *   <tr>
     *     <td>itemTechKey[]</td>
     *     <td>technical key of the item, only relevant for items already added to the document</td>
     *   </tr>
     *   <tr>
     *     <td>product[]</td>
     *     <td>product number</td>
     *   </tr>
     *   <tr>
     *     <td>quantity[]</td>
     *     <td>quantity of the product</td>
     *   </tr>
     *   <tr>
     *     <td>unit[]</td>
     *     <td>the unit of the product</td>
     *   </tr>
     *   <tr>
     *     <td>delete[]</td>
     *     <td>items to be deleted from the order</td>
     *   </tr>
     *   <tr>
     *     <td>cancel[]</td>
     *     <td>items to be cancelled from the order</td>
     *   </tr>
     *   <tr>
     *     <td>comment[]</td>
     *     <td>addtitional comments for the item entered by the customer</td>
     *   </tr>
     *   <tr>
     *     <td>reqdeliverydate[]</td>
     *     <td>requested delivery date for the item</td>
     *   </tr>
     *   <tr>
     *     <td>itemExtStatus[]</td>
     *     <td>status to set for the item</td>
     *   </tr>
     *   <tr>
     *     <td>batchID[]</td>
     *     <td>the ID of a batch element</td>
     *   </tr>
     *   <tr>
     *     <td>batchDedicated[]</td>
     *     <td>true if batches are maintained for the material</td>
     *   </tr>
     *   <tr>
     *     <td>batchAssigned[]</td>
     *     <td>true if characteristics are avaible to set for a batch element</td>
     *   </tr>
     *   <tr>
     *     <td>elementCount[]</td>
     *     <td>the amount of batch selections that exists</td>
     *   </tr>
     *   <tr>
     *     <td>elementName[]</td>
     *     <td>name of the chosen element</td>
     *   </tr>
     *   <tr>
     *     <td>elementTxt[]</td>
     *     <td>language dependend name of the element</td>
     *   </tr>
     *   <tr>
     *     <td>elementCountBs[]</td>
     *     <td>count checkboxes per element</td>
     *   </tr>
     *   <tr>
     *     <td>elementCharValue[]</td>
     *     <td>value of checkboxes from the element</td>
     *   </tr>
     *   <tr>
     *     <td>elementCharValTxt[]</td>
     *     <td>language dependend value of checkboxes from the element</td>
     *   </tr>
     *   <tr>
     *     <td>elementCharValMax[]</td>
     *     <td>language depended unit of the batch element</td>
     *   </tr>
     * </table>
     * <br><br>
     * @param parser The request, wrapped into a parser object
     * @param order the document to store the data in
     * @param headerReqDeliveryDate the date set in the header for the
     *        requested delivery date. This parameter is used to provide
     *        the functionality to provide a preset date for all items
     * @param log reference to the logging context
     * @param isDebugEnabled flag indicating wheter or not logging output
     *        is written
     *
     * @deprecated This method will be replaced by
     *    <code>parseRequestItems(
     *                  RequestParser parser,
     *                  UserSessionData userSessionData,
     *                  OrderChange order,
     *                  headerReqDeliveryDate,
     *                  IsaLocation log,
     *                  boolean isDebugEnabled)</code>
     * @see #parseRequestItems(
     *                  RequestParser parser,
     *                  UserSessionData userSessionData,
     *                  OrderChange order,
     *                  headerReqDeliveryDate,
     *                  IsaLocation log,
     *                  boolean isDebugEnabled)
     */
    protected void parseRequestItems(
                                    RequestParser parser,
                                    OrderChange order,
                                    String headerReqDeliveryDate,
                                    IsaLocation log,
                                    boolean isDebugEnabled)
    throws CommunicationException {

        UserSessionData usersSessionData = null;

        parseRequestItems(parser, usersSessionData, order, headerReqDeliveryDate, log, isDebugEnabled);
    }

    /**
     * Helper method to parse the request and construct items
     * for the <code>SalesDocument</code> form the
     * request's data.
     * <p>
     * The following fields are taken from the request and used to
     * construct the item:
     * </p>
     * <table border="1" cellspacing="0" cellpadding="2">
     *   <tr>
     *     <td><b>field</b></td>
     *     <td><b>use</b></td>
     *   </tr>
     *   <tr>
     *     <td>itemTechKey[]</td>
     *     <td>technical key of the item, only relevant for items already added to the document</td>
     *   </tr>
     *   <tr>
     *     <td>product[]</td>
     *     <td>product number</td>
     *   </tr>
     *   <tr>
     *     <td>quantity[]</td>
     *     <td>quantity of the product</td>
     *   </tr>
     *   <tr>
     *     <td>unit[]</td>
     *     <td>the unit of the product</td>
     *   </tr>
     *   <tr>
     *     <td>delete[]</td>
     *     <td>items to be deleted from the order</td>
     *   </tr>
     *   <tr>
     *     <td>cancel[]</td>
     *     <td>items to be cancelled from the order</td>
     *   </tr>
     *   <tr>
     *     <td>comment[]</td>
     *     <td>addtitional comments for the item entered by the customer</td>
     *   </tr>
     *   <tr>
     *     <td>reqdeliverydate[]</td>
     *     <td>requested delivery date for the item</td>
     *   </tr>
     * </table>
     * <br><br>
     * @param parser The request, wrapped into a parser object
     * @param userSessionData   object wrapping the session
     * @param order the document to store the data in
     * @param headerReqDeliveryDate the date set in the header for the
     *        requested delivery date. This parameter is used to provide
     *        the functionality to provide a preset date for all items
     * @param log reference to the logging context
     * @param isDebugEnabled flag indicating wheter or not logging output
     *        is written
     */
    protected void parseRequestItems(
                                    RequestParser parser,
                                    UserSessionData userSessionData,
                                    OrderChange order,
                                    String headerReqDeliveryDate,
                                    IsaLocation log,
                                    boolean isDebugEnabled)
    throws CommunicationException {

        ItemList items = order.getItems();
        
        STANDARD_PARSER.parseItems(parser, userSessionData, items, order.getSalesDocument(),
                                   true, log, isDebugEnabled);
        
        for (int i = 0; i < items.size(); i++) {
            customerExitParseRequestItem(parser, userSessionData, items.get(i));
            customerExitParseRequestItem(parser, userSessionData, items.get(i), i + 1);
        }
    }

    /**
     * Convenience method for developers who want to call
     * <code>parseRequestItems</code> and <code>parseRequestHeader</code> one
     * after the other. This method calls the other two and gains some extra
     * data to keep the call simple.
     * Note: If this method is called the userSessionData parameter in the
     * customerExitParseRequest.. methods is null and therefore can't be used.
     *
     * @param parser The request, wrapped into a parser object
     * @param order the document to store the data in
     * @param log reference to the logging context
     *
     * @deprecated This method will be replaced by
     *    <code>parseRequest(
     *                  RequestParser parser,
     *                  UserSessionData userSessionData,
     *                  OrderChange order,
     *                  IsaLocation log)</code>
     * @see #parseRequest(
     *                  RequestParser parser,
     *                  UserSessionData userSessionData,
     *                  OrderChange order,
     *                  IsaLocation log)
     */
    protected void parseRequest(
                               RequestParser parser,
                               OrderChange order,
                               IsaLocation log) throws CommunicationException {

        parseRequest(parser, null, order, log);
    }

    /**
     * Convenience method for developers who want to call
     * <code>parseRequestItems</code> and <code>parseRequestHeader</code> one
     * after the other. This method calls the other two and gains some extra
     * data to keep the call simple.
     *
     * @param parser The request, wrapped into a parser object
     * @param userSessionData   object wrapping the session
     * @param order the document to store the data in
     * @param log reference to the logging context
     */
    protected void parseRequest(
                               RequestParser parser,
                               UserSessionData userSessionData,
                               OrderChange order,
                               IsaLocation log) throws CommunicationException {

        boolean isDebugEnabled = log.isDebugEnabled();

        order.clearMessages();

        parseRequestHeader(parser, userSessionData, order, log,
                           isDebugEnabled);

        String headerReqDeliveryDate = order.getHeader().getReqDeliveryDate();

        parseRequestItems(parser, userSessionData, order,
                          headerReqDeliveryDate, log,
                          isDebugEnabled);
    }

    /**
     * Helper method to parse the request and construct the header of
     * a <code>SalesDocument</code> form the
     * request's data.
     * Note: If this method is called the userSessionData parameter in the
     * customerExitParseRequestHeader method is null and therefore can't be used.
     * <p>
     * The following fields are taken from the request and used to
     * construct the header:
     * </p>
     * <table border="1" cellspacing="0" cellpadding="2">
     *   <tr>
     *     <td><b>field</b></td>
     *     <td><b>use</b></td>
     *   </tr>
     *   <tr>
     *     <td>poNumber</td>
     *     <td>purchase order number the customer entered</td>
     *   </tr>
     *   <tr>
     *     <td>headerDescription</td>
     *     <td>description the customer entered for the order</td>
     *   </tr>
     *   <tr>
     *     <td>headerReqDeliveryDate</td>
     *     <td>requested delivery date for the order; data is used to set the date on the item level</td>
     *   </tr>
     *   <tr>
     *     <td>headerShipTo</td>
     *     <td>ship to selected on the header level</td>
     *   </tr>
     *   <tr>
     *     <td>shipCond</td>
     *     <td>shipping conditions for the document</td>
     *   </tr>
     *   <tr>
     *     <td>headerShipTo</td>
     *     <td>ship to selected on the header level</td>
     *   </tr>
     * </table>
     * <br><br>
     *
     * @param parser The request, wrapped into a parser object
     * @param preOrderSalesDocument the document to store the data in
     * @param log reference to the logging context
     * @param isDebugEnabled flag indicating wheter or not logging output
     *        is written
     *
     * @deprecated This method will be replaced by
     *    <code>parseRequestHeader(
     *                  RequestParser parser,
     *                  UserSessionData userSessionData,
     *                  OrderChange order,
     *                  IsaLocation log,
     *                  boolean isDebugEnabled)</code>
     * @see #parseRequestHeader(
     *                  RequestParser parser,
     *                  UserSessionData userSessionData,
     *                  OrderChange order,
     *                  IsaLocation log,
     *                  boolean isDebugEnabled)
     */
    protected void parseRequestHeader(
                                     RequestParser parser,
                                     OrderChange order,
                                     IsaLocation log,
                                     boolean isDebugEnabled)
            throws CommunicationException {

        UserSessionData usersSessionData = null;

        parseRequestHeader(parser, usersSessionData, order, log, isDebugEnabled);
    }

    /**
     * Helper method to parse the request and construct the header of
     * a <code>SalesDocument</code> form the
     * request's data.
     * <p>
     * The following fields are taken from the request and used to
     * construct the header:
     * </p>
     * <table border="1" cellspacing="0" cellpadding="2">
     *   <tr>
     *     <td><b>field</b></td>
     *     <td><b>use</b></td>
     *   </tr>
     *   <tr>
     *     <td>poNumber</td>
     *     <td>purchase order number the customer entered</td>
     *   </tr>
     *   <tr>
     *     <td>headerDescription</td>
     *     <td>description the customer entered for the order</td>
     *   </tr>
     *   <tr>
     *     <td>headerReqDeliveryDate</td>
     *     <td>requested delivery date for the order; data is used to set the date on the item level</td>
     *   </tr>
     *   <tr>
     *     <td>headerShipTo</td>
     *     <td>ship to selected on the header level</td>
     *   </tr>
     *   <tr>
     *     <td>shipCond</td>
     *     <td>shipping conditions for the document</td>
     *   </tr>
     *   <tr>
     *     <td>headerShipTo</td>
     *     <td>ship to selected on the header level</td>
     *   </tr>
     * </table>
     * <br><br>
     *
     * @param parser The request, wrapped into a parser object
     * @param userSessionData   object wrapping the session
     * @param preOrderSalesDocument the document to store the data in
     * @param log reference to the logging context
     * @param isDebugEnabled flag indicating wheter or not logging output
     *        is written
     */
    protected void parseRequestHeader(
                                     RequestParser parser,
                                     UserSessionData userSessionData,
                                     OrderChange order,
                                     IsaLocation log,
                                     boolean isDebugEnabled)
            throws CommunicationException {
        
        STANDARD_PARSER.parseHeader(parser, userSessionData, order.getHeader(), 
                                    order.getSalesDocument(), false, log, isDebugEnabled);
                                                    
        customerExitParseRequestHeader(parser, userSessionData, order);
    }

    /**
     * Convenience method to update the salesdocument in the backend before
     * any further processing is done.
     *
     * @param parser The request, wrapped into a parser object
     * @param userSessionData   object wrapping the session
     * @param order              the document to store the data in
     * @param user              user business object
     * @param shop              shop business object
     * @param log reference to the logging context
     *
     * @deprecated This method will be replaced by
     * <code>updateOrder(RequestParser parser,
     *                            UserSessionData userSessionData,
     *                            OrderChange order,
     *                            User user,
     *                            Shop shop,
     *                            BusinessPartnerManager bupama,
     *                            IsaLocation log) </code>
     * @see #updateOrder(RequestParser parser,
     *                            UserSessionData userSessionData,
     *                            OrderChange order,
     *                            User user,
     *                            Shop shop,
     *                            BusinessPartnerManager bupama,
     *                            IsaLocation log)
     */
    protected void updateOrder(RequestParser parser,
                                 UserSessionData userSessionData,
                                 OrderChange order,
                                 Shop shop,
                                 IsaLocation log,
								 HttpServletRequest request) throws CommunicationException {
        BusinessPartnerManager bupama = null;
        updateOrder(parser, userSessionData, order, shop, bupama, log, request);
    }

    /**
     * Convenience method to prepare the update the salesdocument in the backend
     * before any further processing is done.
     *
     * @param parser The request, wrapped into a parser object
     * @param userSessionData   object wrapping the session
     * @param order              the document to store the data in
     * @param user              user business object
     * @param shop              shop business object
     * @param bupama            business partner manager object
     * @param log reference to the logging context
     */
    /*
    protected void prepareUpdateOrder(RequestParser parser,
                                      UserSessionData userSessionData,
                                      OrderChange order,
                                      User user,
                                      Shop shop,
                                      BusinessPartnerManager bupama,
                                      IsaLocation log ,
    								  TechKey[] canceledItemsTechKeys,
    								  TechKey[] deletedItemsTechKeys
    								  )    								  
            throws CommunicationException {
*/
    /**
     * Convenience method to update the salesdocument in the backend before
     * any further processing is done.
     *
     * @param parser The request, wrapped into a parser object
     * @param userSessionData   object wrapping the session
     * @param order              the document to store the data in
     * @param shop              shop business object
     * @param bupama            business partner manager object
     * @param log reference to the logging context
     */
    protected void updateOrder(RequestParser parser,
                               UserSessionData userSessionData,
                               OrderChange order,
                               Shop shop,
                               BusinessPartnerManager bupama,
                               IsaLocation log,
							   HttpServletRequest request) throws CommunicationException {

		String oldCampaign = "";
		String newCampaign = "";
		// read the old campaign Id
        // but don'tt transfer a changed campaign to the catalogue, is multiple cmapaigns are allowed
		if (!shop.isMultipleCampaignsAllowed() && shop.isManualCampainEntryAllowed() && !shop.isSoldtoSelectable()) {
			if (order.getHeader().getAssignedCampaigns().getCampaign(0) != null) {
				oldCampaign = order.getHeader().getAssignedCampaigns().getCampaign(0).getCampaignId();				
			}
		}        
        
        parseRequest(parser, userSessionData, order, log);

        // possible customer exit, to parse request
        customerExitParseRequest(parser, userSessionData, order);

		// read the new campaign Id
		if (!shop.isMultipleCampaignsAllowed() && shop.isManualCampainEntryAllowed() && !shop.isSoldtoSelectable()) {
			if (order.getHeader().getAssignedCampaigns().getCampaign(0) != null) {
				newCampaign = order.getHeader().getAssignedCampaigns().getCampaign(0).getCampaignId();				
			}
			
			// check if the Id has changed
			if (newCampaign != null && !newCampaign.trim().equals(oldCampaign.trim())) {
			
				// get the catalog
				WebCatInfo webCat =
						getCatalogBusinessObjectManager(userSessionData).getCatalog();
					
				if (shop.isInternalCatalogAvailable() && webCat == null) {
					throw( new IllegalArgumentException("Paramter webCat can not be null!"));
				}					
				// get the bom
				BusinessObjectManager bom = (BusinessObjectManager)
						userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);

				// we have to set the Id in the catalog
				GetCampaignAction.readCampaign(newCampaign, userSessionData, request, log);
 				   AddCampaignToCatalogueAction.addCampaignToCatalogue(webCat, userSessionData, bom, log);
                }
			}			
		

        RequestParser.Parameter delete =
        parser.getParameter("delete[]");

        RequestParser.Parameter cancel =
        parser.getParameter("cancel[]");

        RequestParser.Parameter replacetype =
        parser.getParameter("replacetype[]");

        int numLines = parser.getParameter("product[]").getNumValues();

        // dont' save new items, with quantity 0 and
        // delete main position where the quantity is 0  and all their subpositions
        ItemList items =order.getItems();

        String quantity = null;
        TechKey techKey = null;
        TechKey parentId = null;

        int newItemsToDelete[] = new int[items.size()];

        HashSet deletedItems = new HashSet();
        HashSet canceldItems = new HashSet();

        // check for main positions where the quantity is 0 and cancel or
        // delete them - the subpositions should be deleted automatically
        for(int i = 0; i < items.size(); i++) {
            quantity = items.get(i).getQuantity();
            techKey = items.get(i).getTechKey();
            parentId = items.get(i).getParentId();

            if((techKey == null || techKey.getIdAsString().length() == 0) &&
               quantity != null && quantity.trim().equals("0")) {
                // new item with quantity 0, don't save it
                newItemsToDelete[i] = 1;
            }
            else if(techKey != null && techKey.getIdAsString().length() > 0 &&
                    quantity != null && quantity.trim().equals("0") &&
                    (parentId == null || parentId.getIdAsString().length() == 0)) {
                // numbering for request parameters starts with 1 despite if the item list, that
                // starts with 0, so we have to add 1 to the index to get the correct parameter
                if(replacetype.getValue(i + 1).getString().equalsIgnoreCase("cancelable")) {
                    canceldItems.add(techKey.getIdAsString());
                    items.get(i).setQuantity(items.get(i).getOldQuantity());
                    if(log.isDebugEnabled()) {
                        log.debug("Resetting Quantity for main Item " + items.get(i).getProduct() + " to avoid problems with invalid quantity");
                    }
                }
                else {
                    deletedItems.add(techKey.getIdAsString());
                }
                if(log.isDebugEnabled()) {
                    log.debug("Main Item " + items.get(i).getProduct() + " will be canceled or deleted, because quantity is 0");
                }
            }
        }

        Iterator itemsIter = items.iterator();

        int k= 0;

        // because there is not method to remove an item directly from
        // the itemlist we have to use the iterator
        while(itemsIter.hasNext()) {
            itemsIter.next();
            if(newItemsToDelete[k] == 1) {
                if(log.isDebugEnabled()) {
                    log.debug("Deleting new Item " + items.get(k).getProduct() + " before update, because quantity is 0");
                }
                itemsIter.remove();
            }
            k++;
        }

        for(int i = 1; i <= numLines; i++) {
            if(cancel.getValue(i).isSet()) {
                canceldItems.add(cancel.getValue(i).getString());
            }
        }

        for(int i = 1; i <= numLines; i++) {
            if(delete.getValue(i).isSet()) {
                deletedItems.add(delete.getValue(i).getString());
            }
        }

        TechKey[] canceledItemsTechKeys = new TechKey[canceldItems.size()];
        TechKey[] deletedItemsTechKeys = new TechKey[deletedItems.size()];

        Iterator canceldIter = canceldItems.iterator();
        Iterator deletedIter = deletedItems.iterator();

        k = 0;

        while(canceldIter.hasNext()) {
            canceledItemsTechKeys[k++]   = new TechKey((String) canceldIter.next());
        }

        k = 0;

        while(deletedIter.hasNext()) {
            deletedItemsTechKeys[k++]   = new TechKey((String) deletedIter.next());
        }

		//Required to delete /  cancel the Sub-items of grid main item when cancelled /deleted
		ItemList tempItemList = order.getItems();
		//to collect all the cancelled and deleted sub-items
		HashSet deletedSubItems = new HashSet();
		HashSet canceldSubItems = new HashSet();
	
		if ( deletedItemsTechKeys.length > 0 ||
			 canceledItemsTechKeys.length > 0 ){
				
			for (int i=0; i<tempItemList.size();i++){
				ItemSalesDoc subItem = tempItemList.get(i); 
				if (subItem.getParentId() != null &&
					subItem.getParentId().toString().length() > 0){
				    	
						String parentKey = subItem.getParentId().toString();
					
						//Fill the SubItems that need to be cancelled
						for (int j=0; j<canceledItemsTechKeys.length;j++){
							if (parentKey.toString().equals(canceledItemsTechKeys[j].toString())){
									canceldSubItems.add(subItem.getTechKey().getIdAsString());  	// Note 1373196
							}
						}
						//Fill the SubItems that need to be deleted
						for (int j=0; j<deletedItemsTechKeys.length;j++){
							if (parentKey.toString().equals(deletedItemsTechKeys[j].toString())){
									deletedSubItems.add(subItem.getTechKey().getIdAsString());  	// Note 1373196									
							}
						}
				}
			}
		}
		// Note 1373196
		TechKey[] subItemsCancelTechKeys = new TechKey[canceldSubItems.size()];
		TechKey[] subItemsDeleteTechKeys = new TechKey[deletedSubItems.size()];
		Iterator canceldSubIter = canceldSubItems.iterator();
		Iterator deletedSubIter = deletedSubItems.iterator();
		
		k = 0;
		while(canceldSubIter.hasNext()) {
			subItemsCancelTechKeys[k++]   = new TechKey((String) canceldSubIter.next());
		}
		k = 0;
		while(deletedSubIter.hasNext()) {
			subItemsDeleteTechKeys[k++]   = new TechKey((String) deletedSubIter.next());
		}
		// Note 1373196
	
//cancel the items and subitems				
        if(canceledItemsTechKeys != null && canceledItemsTechKeys.length > 0) {
            if(log.isDebugEnabled()) {
                log.debug("canceling items");
            }
            order.cancelItems(canceledItemsTechKeys);
            //Cancel the subItems
            if(subItemsCancelTechKeys != null && subItemsCancelTechKeys.length > 0 ){
			order.cancelItems(subItemsCancelTechKeys);			
        }
        }

//delete the items and subitems
        if(deletedItemsTechKeys != null && deletedItemsTechKeys.length > 0) {
            if(log.isDebugEnabled()) {
                log.debug("deleting items");
            }
            order.deleteItems(deletedItemsTechKeys);
            ItemList itemList = order.getItems();
            for (int i = 0; i < deletedItemsTechKeys.length; i++) {
                itemList.remove(deletedItemsTechKeys[i]);
            }
            
			if (subItemsDeleteTechKeys != null && subItemsDeleteTechKeys.length > 0){
			   	order.deleteItems(subItemsDeleteTechKeys);
            //Delete the subItems
            for(int i= 0; i < subItemsDeleteTechKeys.length; i++){
            	if (subItemsDeleteTechKeys[i] != null) {
            		itemList.remove(subItemsDeleteTechKeys[i]);
        }
            }
        }
        }


        order.update(bupama, shop);
        order.readForUpdate(shop);

    }



    /**
     * Method to activate the delete flag for positions in order templates
     *
     * @param parser The request, wrapped into a parser object
     * @param order the document to store the data in
     * @param log reference to the logging context
     */
    protected void determineItemsDeletable(OrderChange order)
                throws CommunicationException {

        // activate the possibility to delete positions in order templates (note 521056)

        if(order instanceof OrderTemplateChange) {
            ItemList items = order.getItems();
            boolean deletable = true;
            ItemSalesDoc item;
            for(int i = 0; i < items.size(); i++) {
                item = (ItemSalesDoc)items.get(i);
                // set deletable flag to true
                item.setDeletable(deletable);
            }
        }
    }


    /**
     * Template method to be overwritten by the subclasses of this class.
     * Normally you should overwrite this method and you do not touch the
     * <code>isaPreform</code> method.
     *
     * @param request           request context
     * @param response          response context
     * @param session           unwrapped session context
     * @param userSessionData   object wrapping the session
     * @param requestParser     parser to simple retrieve data from the request
     * @param bom               reference to the BusinessObjectManager
     * @param log               reference to the IsaLocation, needed for logging
     * @param startupParameter  object containing the startup parameters
     * @param eventHandler      object to capture events with
     * @param multipleInvocation flag indicating, that a multiple invocation occured
     * @param browserBack       flag indicating a browser back
     * @param shop              shop business object
     * @param order             the current order edited
     * @param targetDocument    the target document
     * @param documentHan       document handler
     *
     * @return logical key for a forward to another action or jsp
     */
    protected abstract String orderPerform(HttpServletRequest request,
                                           HttpServletResponse response,
                                           HttpSession session,
                                           UserSessionData userSessionData,
                                           RequestParser parser,
                                           BusinessObjectManager bom,
                                           IsaLocation log,
                                           IsaCoreInitAction.StartupParameter startupParameter,
                                           BusinessEventHandler eventHandler,
                                           boolean multipleInvocation,
                                           boolean browserBack,
                                           Shop shop,
                                           OrderChange order,
                                           DocumentState targetDocument,
                                           DocumentHandler documentHandler)
            throws CommunicationException;


    /**
     * Overriden <em>isaPerform</em> method of <em>IsaCoreBaseAction</em>.
     */
    public ActionForward isaPerform(ActionMapping mapping,
                                    ActionForm form,
                                    HttpServletRequest request,
                                    HttpServletResponse response,
                                    UserSessionData userSessionData,
                                    RequestParser parser,
                                    BusinessObjectManager bom,
                                    IsaLocation log,
                                    IsaCoreInitAction.StartupParameter startupParameter,
                                    BusinessEventHandler eventHandler,
                                    boolean multipleInvocation,
                                    boolean browserBack)
            throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        HttpSession session = request.getSession();

        DocumentHandler documentHandler = (DocumentHandler)
                                          userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);

        if(documentHandler == null) {
			log.exiting();
            throw new PanicException("No document handler found");
        }

        DocumentState targetDocument =
        documentHandler.getDocument(ManagedDocument.TARGET_DOCUMENT);

        if(targetDocument == null) {
			log.exiting();
            throw new PanicException("No target document returned from documentHandler");
        }

		if (targetDocument instanceof Order) {			
			userSessionData.setAttribute(SC_DOCTYPE, DOCTYPE_ORDER);
		}
		
        OrderChange orderChange = getOrderChangeDecorator((SalesDocument)targetDocument);

        if (orderChange == null) {
			log.exiting();
            throw new PanicException("No order returned from bom");
        }

        Shop shop   = bom.getShop();

        if(shop == null) {
			log.exiting();
            throw new PanicException("No shop returned from bom");
        }

        String forwardTo = orderPerform(request, response, session, userSessionData,
                                        parser, bom, log, startupParameter, eventHandler,
                                        multipleInvocation, browserBack, shop,
                						orderChange, targetDocument,
                                        documentHandler);
                                        
        resolveMessageKeys(session, orderChange, log, log.isDebugEnabled());

		log.exiting();
        return mapping.findForward(forwardTo);
    }

    
    /**
     * Template method to be overwritten by the subclasses of this class.
     * This method provides the possibilty, to easily extend the parsing
     * process for a sales document, accordingly to special
     * customer requirements. By default this method is empty.
     * <code>customerExitParseRequestHeader</code> method.
     *
     * @param requestParser     parser to simple retrieve data from the request
     * @param userSessionData   object wrapping the session
     * @param order             the current sales OrderChange object edited
     *
     */
    public void customerExitParseRequest(RequestParser parser,
                                         UserSessionData userSessionData,
                                         OrderChange order) {
    }

    /**
     * Template method to be overwritten by the subclasses of this class.
     * This method provides the possibilty, to easily extend the parsing
     * process for the header of a sales document, accordingly to special
     * customer requirements. By default this method is empty.
     * <code>customerExitParseRequestHeader</code> method.
     *
     * @param requestParser     parser to simple retrieve data from the request
     * @param userSessionData   object wrapping the session
     * @param order             the current sales OrderChange object edited
     *
     * @deprecated This method will be replaced by <code>customerExitParseRequestHeader(
     *                                                           RequestParser parser,
     *                                                           UserSessionData userSessionData,
     *                                                           HeaderSalesDocument header,
     *                                                           SalesDocument salesDoc)</code>
     *             of the class <code>SaleDocumentParser</code> or one of its subclasses
     * @see #SaleDocumentParser.customerExitParseRequestHeader(
     *                                  RequestParser parser,
     *                                  UserSessionData userSessionData,
     *                                  HeaderSalesDocument header,
     *                                  SalesDocument salesDoc)
     */
    public void customerExitParseRequestHeader(RequestParser parser,
                                               UserSessionData userSessionData,
                                               OrderChange order) {
    }

    /**
    * Template method to be overwritten by the subclasses of this class.
    * This method provides the possibilty, to easily extend the parsing
    * process for the items of a sales document, accordingly to special
    * customer requirements. By default this method is empty.
    * <code>customerExitParseRequestHeader</code> method.
    *
    * @param requestParser     parser to simple retrieve data from the request
    * @param userSessionData   object wrapping the session
    * @param itemSalesDoc      the current item edited
    * 
    * @deprecated This method will be replaced by 
    *    <code>customerExitParseRequestItem(
    *          RequestParser    parser,
    *          UserSessionData  userSessionData, 
    *          ItemSalesDoc     itemSalesDoc,
    *          int              itemIdx)</code>
    * @see #customerExitParseRequestItem(          
    *          RequestParser    parser,
    *          UserSessionData  userSessionData, 
    *          ItemSalesDoc     itemSalesDoc,
    *          int              itemIdx)
    */
    public void customerExitParseRequestItem(
               RequestParser parser,
               UserSessionData userSessionData,
               ItemSalesDoc itemSalesDoc) {
    }
     
   /**
    * Template method to be overwritten by the subclasses of this class.
    * This method provides the possibilty, to easily extend the parsing
    * process for the items of a sales document, accordingly to special
    * customer requirements. By default this method is empty.
    * <code>customerExitParseRequestHeader</code> method.
    *
    * @param requestParser     parser to simple retrieve data from the request
    * @param userSessionData   object wrapping the session
    * @param itemSalesDoc      the current item edited
    * @param itemIdx           the index of item related fields in the request 
    * 
    * @deprecated This method will be replaced by <code>customerExitParseRequestItem(
    *                                                   RequestParser parser,
    *                                                   UserSessionData userSessionData,
    *                                                   ItemSalesDoc itemSalesDoc,
    *                                                   int itemIdx,
    *                                                   SalesDocument salesDoc)</code>
    *             of the class <code>SaleDocumentParser</code> or one of its subclasses
    * @see #SaleDocumentParser.customerExitParseRequestItem(
    *                                                   RequestParser parser,
    *                                                   UserSessionData userSessionData,
    *                                                   ItemSalesDoc itemSalesDoc,
    *                                                   int itemIdx,
    *                                                   SalesDocument salesDoc)
    */
    public void customerExitParseRequestItem(
               RequestParser parser,
               UserSessionData userSessionData,
               ItemSalesDoc itemSalesDoc,
               int itemIdx) {
    }


    /**
     * Template method to be overwritten by the subclasses of this class.
     * This method provides the possibilty, to easily change the forward
     * of an action, accordingly to customer requirements. if this method
     * returns nothing, the default forward is used
     * By default this method returns null.
     * <code>customerExitDispatcher</code> method.
     *
     * @param requestParser     parser to simple retrieve data from the request
     * @param userSessionData   object wrapping the session
     * @param itemSalesDoc      the current item edited
     *
     */
    public String customerExitDispatcher(RequestParser parser) {
        return null;
    }
    
    /**
     * completes the message description for the sales document
     * only a resource key
     *
     * @param session the actual HTTP session
     * @param preOrderSalesDocument the document to store the data in
     * @param log reference to the logging context
     * @param isDebugEnabled flag indicating wheter or not logging output
     *        is written
     */
    protected void resolveMessageKeys(
            HttpSession session,
            OrderChange preOrderSalesDocument,
            IsaLocation log,
            boolean isDebugEnabled)
                        throws CommunicationException {

          if (preOrderSalesDocument == null) {
             return;
          }

          translateMessageList(session,
                               preOrderSalesDocument.getMessageList(),
                               log,
                               isDebugEnabled);
          if (preOrderSalesDocument.getHeader() != null) {
              translateMessageList(session,
                               preOrderSalesDocument.getHeader().getMessageList(),
                               log,
                               isDebugEnabled);
          }

          ItemList items = preOrderSalesDocument.getItems();
          if (items != null) {
              for (int i = 0; i < items.size(); i++) {
                        translateMessageList(session,
                                   items.get(i).getMessageList(),
                                   log,
                                   isDebugEnabled);
              }
          }
    }
    
    /**
     * Retrieves the description from the resource files for messages containing
     * only a resource key
     *
     * @param session the actual HTTP session
     * @param msglist list of messages to process
     * @param log reference to the logging context
     * @param isDebugEnabled flag indicating wheter or not logging output
     *        is written
     */
    protected void translateMessageList(
            HttpSession session,
            MessageList msgList,
            IsaLocation log,
            boolean isDebugEnabled)
                        throws CommunicationException {

          Iterator it;

           if (msgList != null) {
               it = msgList.iterator();

               while (it.hasNext()) {

                Message message = (Message) it.next();

                String description = message.getDescription();
                String ressourceKey    = message.getResourceKey();

                    if ((description == null || description.length() == 0)
                         && ressourceKey != null) {
                        // Messagedescription is empty but message contains resource key
                        // retrieve the description
                        message.setDescription(WebUtil.translate(getServlet().getServletContext(),
                                           session,
                                           ressourceKey,
                                           message.getResourceArgs()));
                    }
                }
           }
    }

}