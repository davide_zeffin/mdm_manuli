/*****************************************************************************
    Class:        DocumentStatusDetailAction
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author        SAP
    Created:      27 March 2001

    $Revision: #9 $
    $Date: 2004/05/05 $
*****************************************************************************/

package com.sap.isa.isacore.action.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.billing.BillingStatus;
import com.sap.isa.businessobject.order.CustomerOrderStatus;
import com.sap.isa.businessobject.order.OrderStatus;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.SoldFrom;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.ActionConstantsBase;
import com.sap.isa.isacore.action.PaymentActions;

/**
 * Display the details of a document status object.
 *
 */
public class DocumentStatusDetailAction extends DocumentStatusBaseAction {

    /**
    * document stored in request scope.
    */
    public static final String RK_ORDER_STATUS_DETAIL_ITEMITERATOR = "itemiterator";
    public static final String RK_BILLING_STATUS_DETAIL = "billingstatusdetail";
    public static final String RK_BILLING_STATUS_DETAIL_ITEMITERATOR = "itemiterator";
    public static final String RK_SOLDTO_NAME = "soldtoname";
    public static final String RK_SOLDTO_TECHKEY = "soldtotechkey";
    public static final String RK_CHECKBOXNAME = "itemcheckbox";
    public static final String RK_CHECKBOXHIDDEN = "itemcheckboxhidden";
	
    /**
    * Request context constant for storage of the card type
    */
    public static final String RC_CARDTYPE       = "cardtype";


    /**
     * @see com.sap.isa.isacore.action.order.DocumentStatusBaseAction#performBillingStatus(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse, UserSessionData, RequestParser, BusinessObjectManager, BillingStatus, IsaLocation)
     */
    public ActionForward performBillingStatus(ActionMapping mapping,
                                              ActionForm form,
                                              HttpServletRequest request,
                                              HttpServletResponse response,
                                              UserSessionData userSessionData,
                                              RequestParser requestParser,
                                              BusinessObjectManager bom,
                                              BillingStatus billingStatus,
                                              IsaLocation log)
            throws CommunicationException {
		final String METHOD_NAME = "performBillingStatus()";
		log.entering(METHOD_NAME);
        // Backend call has been performed in DocumentStatusPrepareAction before,
        // but refresh it. Prevents from data inconsistency.
        billingStatus.readBillingStatus(null, null, (Shop)null, (BusinessPartner) null);

        // When details requested, only ONE salesdocument is in access
        request.setAttribute(RK_BILLING_STATUS_DETAIL_ITEMITERATOR, billingStatus.getItemsIterator());
        request.setAttribute(RK_BILLING_STATUS_DETAIL, billingStatus);
		log.exiting();
        return mapping.findForward("billingdetail");

    }

    /**
     * @see com.sap.isa.isacore.action.order.DocumentStatusBaseAction#performOrderStatus(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse, UserSessionData, RequestParser, BusinessObjectManager, OrderStatus, IsaLocation)
     */
    public ActionForward performOrderStatus(ActionMapping mapping,
                                            ActionForm form,
                                            HttpServletRequest request,
                                            HttpServletResponse response,
                                            UserSessionData userSessionData,
                                            RequestParser requestParser,
                                            BusinessObjectManager bom,
                                            OrderStatus orderStatus,
                                            IsaLocation log)
            throws CommunicationException {

        // Backend call has been performed in DocumentStatusPrepareAction before,
        // but refresh it. Prevents from data inconsistency.
        orderStatus.readOrderStatus(null, null, null, null, null);

        // When details requested, only ONE salesdocument is in access
        request.setAttribute(RK_ORDER_STATUS_DETAIL_ITEMITERATOR, orderStatus.getItemsIterator());
        request.setAttribute(ActionConstantsBase.RC_SALES_STATUS_DETAIL, orderStatus);

        if (orderStatus instanceof CustomerOrderStatus) {
            TechKey soldFromKey = orderStatus.getOrderHeader().getPartnerList().getSoldFrom().getPartnerTechKey();
            BusinessPartner soldFromPartner = bom.createBUPAManager().getBusinessPartner(soldFromKey);
            SoldFrom soldFrom = new SoldFrom();
            bom.createBUPAManager().addPartnerFunction(soldFromPartner, soldFrom);
            soldFrom.readData(soldFromKey, bom.getShop().getLanguage(), null, null, null);

            request.setAttribute(RC_CARDTYPE,soldFrom.getCardType());
			userSessionData.setAttribute(MaintainBasketBaseAction.SC_SHIPTOS, orderStatus.getShipTos());
        }

		// put payment related values into the context
	    PaymentActions.setPaymentAttributes(userSessionData, request, orderStatus); 
      
        return mapping.findForward("orderdetail");
    }

}