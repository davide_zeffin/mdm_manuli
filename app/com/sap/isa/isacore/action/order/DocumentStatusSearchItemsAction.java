/*****************************************************************************
    Class:        DocumentStatusSearchItemsAction
    Copyright (c) 2000, SAP AG, All rights reserved.
    Description:  search for items of a large document, based on the parameters
                  specified in orderstatusdetail.jsp
    Author:       SAP AG
    Created:      Jan 2004

    $Revision: #1 $
    $Date: 2004/01/13 $
*****************************************************************************/

package com.sap.isa.isacore.action.order;

import java.sql.SQLException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.GenericSearch;
import com.sap.isa.businessobject.GenericSearchBuilder;
import com.sap.isa.businessobject.GenericSearchReturn;
import com.sap.isa.businessobject.GenericSearchSearchCommand;
import com.sap.isa.businessobject.GenericSearchSelectOptionLine;
import com.sap.isa.businessobject.GenericSearchSelectOptions;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.billing.BillingStatus;
import com.sap.isa.businessobject.order.OrderStatus;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ExtendedResultSet;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.GenericSearchComparator;
import com.sap.isa.isacore.ManagedDocumentLargeDoc;
import com.sap.isa.isacore.action.ActionConstantsBase;
import com.sap.isa.isacore.action.GenericSearchBaseAction;
import com.sap.isa.isacore.action.IsaCoreInitAction;
import com.sap.isa.isacore.action.IsaCoreInitAction.StartupParameter;
import com.sap.isa.ui.uiclass.genericsearch.GenericSearchUIData;

/**
 * 
 * @author SAP AG
 *
 * Search for items of a large document, based on the parameters specified 
 * in orderstatusdetail.jsp. The list of retrieved items will be 
 * displayed in the orderstatus screen.
 */
public class DocumentStatusSearchItemsAction extends DocumentStatusBaseAction {
   /**
     * Constant to define request parameter, that will be set,
     * if no valid search criteria is specified
     */
    public static final String RC_NO_SEARCH_CRITERIA = "nosearchcriteria";
    
    /**
     * Constant to define request parameter, that will be set,
     * if no items were found
     */
    public static final String RC_NO_ITEMS_FOUND = "noitemsfound";
    
    /**
     * Constant to define request parameter, that will be set,
     * if no the low value is invalid
     */
    public static final String RC_LOW_VAL_INVALID = "lowvalinvalid";
    
    /**
     * Constant to define request parameter, that will be set,
     * if no the high value is invalid
     */
    public static final String RC_HIGH_VAL_INVALID = "highvalinvalid";
    
    /**
     * Constant to define request parameter, that will hold the
     * techkey of the salesdocument
     */
    public static final String RC_TECHKEY = "techkey";

    /**
     * Constant to define request parameter, that will hold the
     * flag signaling an item search had been performed (large doc.handling)
     */
    public static final String RC_ITEMSEARCHPERFORMED = "itemsearchperformed";
    
    /**
     * Constant to define request parameter, that will hold the
     * objecttype of the salesdocument
     */
    public static final String RC_OBJECT_TYPE = "objecttype";

    /**
     * Perform order status.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param orderStatus       The order status object
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward performOrderStatus(
        ActionMapping mapping,
        ActionForm form,
        HttpServletRequest request,
        HttpServletResponse response,
        UserSessionData userSessionData,
        RequestParser requestParser,
        BusinessObjectManager bom,
        OrderStatus orderStatus,
        IsaLocation log)
        throws CommunicationException {
        	
		final String METHOD_NAME = "performOrderStatus()";
		log.entering(METHOD_NAME);
        // determine search criteria
        RequestParser.Parameter itemStatus = requestParser.getParameter("itemStatus");
        RequestParser.Parameter itemProperty = requestParser.getParameter("itemProperty");
        RequestParser.Parameter itemPropertyLowValue = requestParser.getParameter("itemPropertyLowValue");
        RequestParser.Parameter itemPropertyHighValue = requestParser.getParameter("itemPropertyHighValue");
        RequestParser.Parameter searchToDisplay = requestParser.getParameter(GenericSearchUIData.RC_SEARCHCRITERASCREENNAME);        
        boolean isBillingScreen = ("SearchCriteria_B2B_Billing_BillingStatus_Items".equals(searchToDisplay.getValue().getString()) ? true : false);
        boolean isStatusNotSpecified = (!itemStatus.isSet() || itemStatus.getValue().getString().equals(ManagedDocumentLargeDoc.ITEM_STATUS_NOT_SPECIFIED));
                                        
        boolean isPropertyNotSpecified = (!itemProperty.isSet() || itemProperty.getValue().getString().equals(ManagedDocumentLargeDoc.ITEM_PROPERTY_NONE));
        
        boolean isPropertyValueNotSpecified = (!itemPropertyLowValue.isSet() || itemPropertyLowValue.getValue().getString().equals("")) &&
                                              (!itemPropertyHighValue.isSet() || itemPropertyHighValue.getValue().getString().equals(""));

        boolean isPropertyNotBackorder =  (isPropertyNotSpecified || 
             !(itemProperty.getValue().getString().equals(ManagedDocumentLargeDoc.ITEM_PROPERTY_BACKORDER)));      
        DocumentHandler documentHandler = getDocumentHandler(userSessionData);
        ManagedDocumentLargeDoc lmdoc =  (ManagedDocumentLargeDoc) documentHandler.getManagedDocumentOnTop();
        
        String itemSearchStatus = itemStatus.getValue().getString();
        String itemSearchProperty = itemProperty.getValue().getString();
        String itemSearchPropertyLowValue = itemPropertyLowValue.getValue().getString();
        String itemSearchPropertyHighValue = itemPropertyHighValue.getValue().getString();

        if (log.isDebugEnabled()) {
            log.debug("entered search criteria: itemStatus=" + itemSearchStatus + " itemProperty=" + itemSearchProperty + " itemPropertyLowValue=" + itemSearchPropertyLowValue + " itemPropertyHighValue=" + itemSearchPropertyHighValue);
        }
        
        lmdoc.setItemSearchStatus(itemSearchStatus);
        lmdoc.setItemSearchProperty(itemSearchProperty);
        lmdoc.setItemSearchPropertyLowValue(itemSearchPropertyLowValue);
        lmdoc.setItemSearchPropertyHighValue(itemSearchPropertyHighValue);

        // any search criteria set
        if ((isStatusNotSpecified && ((isPropertyNotSpecified || isPropertyValueNotSpecified))) 
            && isPropertyNotBackorder) {
        
            if (log.isDebugEnabled()) {
                log.debug("No search criteria entered!");
            }
            
            request.setAttribute(RC_NO_SEARCH_CRITERIA, "true");
            /*
            lmdoc.setItemSearchStatus(ManagedDocumentLargeDoc.ITEM_STATUS_NOT_SPECIFIED);
            lmdoc.setItemSearchProperty(ManagedDocumentLargeDoc.ITEM_PROPERTY_NONE);
            lmdoc.setitemSearchPropertyLowValue("");
            */
			log.exiting();
            return mapping.findForward("nocriteria"); 
        }
        
        // search items, only deliver items 
        int maxItems = bom.getShop().getLargeDocNoOfItemsThreshold();
        
        //Search for items with specified criteria
        // START GENERIC SEARCH FRAMEWORK INTEGRATION ------------------
        // Since Generic Search Framework expects to have each Attribute specified 
        // as its own, re format. Regarding names of the request attributes check xcm file
        // generic-searchbackend-config.xml
        GenericSearchBuilder genBuild = new GenericSearchBuilder();
        if (itemSearchProperty.equals(ManagedDocumentLargeDoc.ITEM_PROPERTY_PRODUCT)) {
            request.setAttribute("rc_product_id", itemSearchPropertyLowValue);
        }
        if (itemSearchProperty.equals(ManagedDocumentLargeDoc.ITEM_PROPERTY_DESC)) {
            request.setAttribute("rc_description_uc", itemSearchPropertyLowValue);
        }
        if (!ManagedDocumentLargeDoc.ITEM_STATUS_NOT_SPECIFIED.equals(itemSearchStatus)) {
            request.setAttribute("rc_status_item", itemSearchStatus);
        }
        if (itemSearchProperty.equals(ManagedDocumentLargeDoc.ITEM_PROPERTY_BACKORDER)) {
            request.setAttribute("rc_backorder", "X");
        }       
        // Set Document ID
        request.setAttribute("rc_object_id", bom.getOrderStatus().getOrderHeader().getSalesDocNumber());
        // Now specify Implementation filter
        request.setAttribute("rc_dummy_implfilter","1O_ITEMS");
        // Create a new Select Option object
        int handle = 1;  // only one selection request
        GenericSearchBaseAction gsba = new GenericSearchBaseAction();
        GenericSearchSelectOptions itemSelOpt = gsba.buildSelectOptions(request, userSessionData, handle);
        // handle select option for internal position number
        if (itemSearchProperty.equals(ManagedDocumentLargeDoc.ITEM_PROPERTY_NUMBERINT)) {
            boolean valuesValid = processLowAndHighValue("1ORD", itemSearchPropertyLowValue, itemSearchPropertyHighValue, 
                                                         lmdoc, itemSelOpt, handle, bom.getShop(), request);
                                                    
            if (!valuesValid) {
				log.exiting();
                return mapping.findForward("nocriteria");
        	}
        }
        GenericSearch searchRequestHandler = bom.createGenericSearch();
        // Use helper method for parsing (Handle = 1 => only one selection request !)
        GenericSearchSearchCommand itemSearchCommand = new GenericSearchSearchCommand(itemSelOpt);
        itemSearchCommand.setBackendImplementation(gsba.getBackendImplemenation(handle, itemSelOpt));
        GenericSearchReturn returnData = searchRequestHandler.performGenericSearch(itemSearchCommand);
        ExtendedResultSet searchResult = new ResultData(returnData.getDocuments());
        int noOfFoundItems = returnData.getCountedDocuments().getRow(1).getField("NUM_DOC_SEL").getInt();
        // END GENERIC SEARCH FRAMEWORK INTEGRATION ------------------      

		// Sort result list by item number
		GenericSearchComparator comparator = new GenericSearchComparator(GenericSearchComparator.ASCENDING,"number", null,
																		 bom.getShop().getDecimalPointFormat() ,null, null);
		searchResult.sort("NUMBER_INT", comparator);  
		  		
        ArrayList foundItems = new ArrayList(0);
        try {
            
            int rowsInArray = (maxItems < searchResult.getNumRows())? maxItems : searchResult.getNumRows();
            foundItems = new ArrayList(rowsInArray);
            
            searchResult.first();
            
            while (rowsInArray > 0) {
                foundItems.add(searchResult.getRowKey());
                searchResult.next();
                rowsInArray--;
            }

        }
        catch (SQLException ex) {
			if (log.isDebugEnabled()) {
				log.debug("Error ", ex);
			}
        }

        if (foundItems.size() == 0) {
            if (log.isDebugEnabled()) {
                log.debug("No items found!");
            }

            request.setAttribute(RC_NO_ITEMS_FOUND, "true");
            lmdoc.setNumberofItemsFound(ManagedDocumentLargeDoc.NO_OF_FOUND_ITEMS_UNDEFINED);
            
            orderStatus.getItemList().clear();
            orderStatus.getSelectedItemGuids().clear();
            
            return mapping.findForward("noitemsfound"); 
        }
        else {
            lmdoc.setNumberofItemsFound(noOfFoundItems);
            if (log.isDebugEnabled()) {
                log.debug(String.valueOf(noOfFoundItems) + " items found!");
            }
        }

        // add the returned guids to orderStatus
        orderStatus.setSelectedItemGuids(foundItems);
        
        request.setAttribute(RC_ITEMSEARCHPERFORMED, Boolean.TRUE);
        request.setAttribute(RC_TECHKEY, orderStatus.getTechKey().getIdAsString());
        request.setAttribute(RC_OBJECT_TYPE, orderStatus.getOrderHeader().getDocumentType());
		log.exiting();
        return mapping.findForward("orderstatusdetail");
    }

    /**
     * Perform billing status.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param billingStatus     The billing status object
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward performBillingStatus(
             ActionMapping mapping,
             ActionForm form,
             HttpServletRequest request,
             HttpServletResponse response,
             UserSessionData userSessionData,
             RequestParser requestParser,
             BusinessObjectManager bom,
             BillingStatus billingStatus,
             IsaLocation log)
               throws CommunicationException {
        StartupParameter startupParameter = (StartupParameter)userSessionData.getAttribute(IsaCoreInitAction.SC_STARTUP_PARAMETER);
        // Even Billing documents run in the same session as Shop, it will be displayed
        // in a separat Portal iView
        DocumentHandler documentHandler = getDocumentHandler(userSessionData);
        ManagedDocumentLargeDoc lmdoc =  (ManagedDocumentLargeDoc) documentHandler.getManagedDocumentOnTop();

        // determine search criteria
        RequestParser.Parameter itemProperty = requestParser.getParameter(ActionConstantsBase.RK_SEARCH_ITEM_PROPERTY);
        RequestParser.Parameter itemPropertyLowValue = requestParser.getParameter(ActionConstantsBase.RK_SEARCH_ITEM_PROPERTY_LOW_VALUE);
        RequestParser.Parameter itemPropertyHighValue = requestParser.getParameter(ActionConstantsBase.RK_SEARCH_ITEM_PROPERTY_HIGH_VALUE);
        
        boolean isPropertyNotSpecified = (!itemProperty.isSet() ||  itemProperty.getValue().getString().equals(ManagedDocumentLargeDoc.ITEM_PROPERTY_NONE));
        boolean isPropertyValueNotSpecified = (!itemPropertyLowValue.isSet() || itemPropertyLowValue.getValue().getString().equals(""))&&
                                              (!itemPropertyHighValue.isSet() || itemPropertyHighValue.getValue().getString().equals(""));

        String itemSearchProperty = itemProperty.getValue().getString();
        String itemSearchPropertyLowValue = itemPropertyLowValue.getValue().getString();
        String itemSearchPropertyHighValue = itemPropertyHighValue.getValue().getString();

        if (lmdoc != null) {
            lmdoc.setItemSearchProperty(itemSearchProperty);
            lmdoc.setItemSearchPropertyLowValue(itemSearchPropertyLowValue);
            lmdoc.setItemSearchPropertyHighValue(itemSearchPropertyHighValue);
        }
        
        // any search criteria set
        if (isPropertyNotSpecified || isPropertyValueNotSpecified) {
            if (log.isDebugEnabled()) {
                log.debug("No search criteria entered!");
            }
            request.setAttribute(RC_NO_SEARCH_CRITERIA, "true");
            return mapping.findForward("bill_nocriteria"); 
        }
        
        // search items, only deliver items 
        int maxItems = bom.getShop().getLargeDocNoOfItemsThreshold();

        //Search for items with specified criteria
        // START GENERIC SEARCH FRAMEWORK INTEGRATION ------------------
        // Since Generic Search Framework expects to have each Attribute specified 
        // as its own, re format. Regarding names of the request attributes check xcm file
        // generic-searchbackend-config.xml
        GenericSearchBuilder genBuild = new GenericSearchBuilder();
        if (itemSearchProperty.equals(ManagedDocumentLargeDoc.ITEM_PROPERTY_PRODUCT)) {
            request.setAttribute("rc_product_id", itemSearchPropertyLowValue);
        }
        // Set Document ID
        request.setAttribute("rc_headnoext", billingStatus.getDocumentHeader().getBillingDocNo());
        request.setAttribute("rc_appllocation", billingStatus.getBillingDocumentsOrigin());
        // Now specify Implementation filter
        request.setAttribute("rc_dummy_implfilter","BILLINGITM");
        // Create a new Select Option object
        int handle = 1;  // only one selection request
        GenericSearchBaseAction gsba = new GenericSearchBaseAction();
        GenericSearchSelectOptions itemSelOpt = gsba.buildSelectOptions(request, userSessionData, handle);
        // For security reason, add business partner
        addBusinessPartnerBilling(handle, bom, itemSelOpt);
        // handle select option for internal position number
        if (itemSearchProperty.equals(ManagedDocumentLargeDoc.ITEM_PROPERTY_NUMBERINT)) {
            boolean valuesValid = processLowAndHighValue("CRMB", itemSearchPropertyLowValue, itemSearchPropertyHighValue, 
                                                    lmdoc, itemSelOpt, handle, bom.getShop(), request);
            
            if (!valuesValid) {
                return mapping.findForward("bill_nocriteria");
            }
            }
            
        GenericSearch searchRequestHandler = bom.createGenericSearch();
        // Use helper method for parsing (Handle = 1 => only one selection request !)
        GenericSearchSearchCommand itemSearchCommand = new GenericSearchSearchCommand(itemSelOpt);
        itemSearchCommand.setBackendImplementation(gsba.getBackendImplemenation(handle, itemSelOpt));
        GenericSearchReturn returnData = searchRequestHandler.performGenericSearch(itemSearchCommand);
        ExtendedResultSet searchResult = new ResultData(returnData.getDocuments());
        int noOfFoundItems = returnData.getCountedDocuments().getRow(1).getField("NUM_DOC_SEL").getInt();
        // END GENERIC SEARCH FRAMEWORK INTEGRATION ------------------      

		// Sort result list by item number
		GenericSearchComparator gensearchcomp = new GenericSearchComparator(GenericSearchComparator.ASCENDING, "number", null,
																			bom.getShop().getDecimalPointFormat(),null, null);
		searchResult.sort("ITEMNO_EXT", gensearchcomp);
		
        ArrayList foundItems = new ArrayList(0);
        try {
            
            int rowsInArray = (maxItems < searchResult.getNumRows())? maxItems : searchResult.getNumRows();
            foundItems = new ArrayList(rowsInArray);
            
            searchResult.first();
            
            while (rowsInArray > 0) {
                foundItems.add(searchResult.getRowKey());
                searchResult.next();
                rowsInArray--;
            }

        }
        catch (SQLException ex) {
			if (log.isDebugEnabled()) {
				log.debug("Error ", ex);
			}        	
        }

        if (foundItems.size() == 0) {
            if (log.isDebugEnabled()) {
                log.debug("No items found!");
            }

            request.setAttribute(RC_NO_ITEMS_FOUND, "true");
            if (lmdoc != null) {
                lmdoc.setNumberofItemsFound(ManagedDocumentLargeDoc.NO_OF_FOUND_ITEMS_UNDEFINED);
            } else {
                request.setAttribute("numberofitemsfound", new Integer(ManagedDocumentLargeDoc.NO_OF_FOUND_ITEMS_UNDEFINED));
            }
            
            billingStatus.getItemList().clear();
            billingStatus.getSelectedItemGuids().clear();
            
            return mapping.findForward("bill_noitemsfound"); 
        }
        else {
            if (log.isDebugEnabled()) {
                log.debug(String.valueOf(noOfFoundItems) + " items found!");
            }
            if (lmdoc != null) {
                lmdoc.setNumberofItemsFound(noOfFoundItems);
            } else {
                request.setAttribute("numberofitemsfound", new Integer(noOfFoundItems));
            }
        }

        // add the returned guids to orderStatus
        billingStatus.setSelectedItemGuids(foundItems);
        
        request.setAttribute(RC_TECHKEY, billingStatus.getTechKey().getIdAsString());
        request.setAttribute(RC_OBJECT_TYPE, billingStatus.getDocumentHeader().getDocumentType());
        
        // Set flag to process billing documents
        request.setAttribute("objects_origin", billingStatus.getBillingDocumentsOrigin());
        request.setAttribute("billing", "X");
        
        return mapping.findForward("orderstatusdetail");
    }
    
    /**
     * Add business partner to select options
     * @param handle
     * @param bom  Reference to the BusinessPartnerManager object
     * @param itemSelOpt Reference to the current select options
     */
    protected void addBusinessPartnerBilling(
                 int                        handle,
                 BusinessObjectManager      bom,
                 GenericSearchSelectOptions itemSelOpt) {
        itemSelOpt.addSelectOptionLine(
           new GenericSearchSelectOptionLine(handle, "IRT_BDH_PAYER", "", "", "", "NOP", "", "",
                                             bom.getBUPAManager().getDefaultBusinessPartner(PartnerFunctionData.SOLDTO).getId(), ""));
    }

    /**
     * Converts the given string to a number
     * 
     * @param numberString the number as string
     * @param shop the shop
     * @return a Number object if the string is is a valid number, null else
     */
    private Number convertToNumber(String numberString, Shop shop) {
        
        Number number = null;
        
        NumberFormat numberFormat = shop.getNumberFormat();

        try {
            number = numberFormat.parse(numberString);
        }
        catch (ParseException ex) {
            if (log.isDebugEnabled()) {
                log.debug("String is not a parseable number: " + numberString);
            }
        }

        return number;
    }
    /**
     * Method to handle low and high value data, in case search via internal posno is choosen.
     * Returns null if everything is o.k., else a String to forward to
     * 
     * @param searchAppl                  application where the search will be performed
     * @param itemSearchPropertyLowValue  low value property
     * @param itemSearchPropertyHighValue high value property
     * @param lmdoc                       managed doculemnt for large docs
     * @param itemSelOpt                  generic serach slect options
     * @param handle                      generic search framework handle
     * @param shop                        The shop
     * @param request                     The request object
     * 
     * @return boolean true if everything is o.k., false if the values are invalid
     */
    private boolean  processLowAndHighValue(String searchAppl,
                                       String itemSearchPropertyLowValue, 
                                       String itemSearchPropertyHighValue, 
                                       ManagedDocumentLargeDoc lmdoc,
                                       GenericSearchSelectOptions itemSelOpt,
                                       int handle,
                                       Shop shop, 
                                       HttpServletRequest request) {

        //check for correctness of value entries
        int lowValInt = 0;
        int highValInt = 0;
        String lowValStr = "0";
        String highValStr = "";
        boolean lowValueValid = true;
        boolean highValueValid = true;
        boolean valuesValid = true;
            
        if (itemSearchPropertyLowValue != null && !"".equals(itemSearchPropertyLowValue)) {
            Number lowValNumber = convertToNumber(itemSearchPropertyLowValue, shop);
            if (lowValNumber != null) {
                lowValInt = lowValNumber.intValue();
                lowValStr = Integer.toString(lowValInt); 
                if (lowValInt < 0) {
                    lowValueValid = false;
                    request.setAttribute(RC_LOW_VAL_INVALID, "true");
    }
            }
            else {
                lowValueValid = false;
                request.setAttribute(RC_LOW_VAL_INVALID, "true");
            }
        }

        if (itemSearchPropertyHighValue != null && !"".equals(itemSearchPropertyHighValue)) {
            Number highValNumber = convertToNumber(itemSearchPropertyHighValue, shop);
            if (highValNumber != null) {
                highValInt = highValNumber.intValue();
                highValStr = Integer.toString(highValInt); 
                if (highValInt < 0) {
                    highValueValid = false;
                    request.setAttribute(RC_HIGH_VAL_INVALID, "true");
}            }
            else {
                highValueValid = false;
                request.setAttribute(RC_HIGH_VAL_INVALID, "true");
            }
        }
            
        if (!lowValueValid || !highValueValid) {
            valuesValid = false;
        }
        else {
            // if only a high value is set, set low value to 0
            if ((itemSearchPropertyLowValue == null || "".equals(itemSearchPropertyLowValue)) && 
                !"".equals(highValStr)) {
                lmdoc.setItemSearchPropertyLowValue("0");
            }
            
            // check for lowVal < highVal
            if (!"".equals(highValStr) && highValInt < lowValInt) {
                String tempVal = lmdoc.getItemSearchPropertyLowValue();
                lmdoc.setItemSearchPropertyLowValue(lmdoc.getItemSearchPropertyHighValue());
                lmdoc.setItemSearchPropertyHighValue(tempVal);
                tempVal = lowValStr;
                lowValStr = highValStr;
                highValStr = tempVal;
            }   
            
            //if high value is not set set it to low value
            if ("".equals(highValStr)) {
                highValStr = lowValStr;
            }
            if ("1ORD".equals(searchAppl)) {
                itemSelOpt.addSelectOptionLine(new GenericSearchSelectOptionLine(
                  handle,
                  "NUMBER_INT",
                  "CL_CRM_REPORT_EXT_ORDERADM_I",
                  "",
                  "",
                  "RAN",
                  "I",
                  "BT",
                  lowValStr,
                  highValStr));
            }
            if ("CRMB".equals(searchAppl)) {
                itemSelOpt.addSelectOptionLine(new GenericSearchSelectOptionLine(
                  handle,
                  "IRT_BDI_ITEMNO_EXT",
                  "BEART_ITEMNO_EXT",
                  "",
                  "",
                  "EXP",
                  "I",
                  "BT",
                  lowValStr,
                  highValStr));
            }
        }
         
        return valuesValid;
    }
    


}