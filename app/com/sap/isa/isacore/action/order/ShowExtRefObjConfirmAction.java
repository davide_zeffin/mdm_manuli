/*****************************************************************************
	Class         ShowExtRefObjConfirmAction
	Copyright (c) 2004, SAP AG, Germany, All rights reserved.
	Description:  Action for the Profile Maintenance
	Author:       SAP
	Created:      21 Januar 2005
	Version:      0.1

	$Revision: #1 $
	$Date: 2005/01/21 $

*****************************************************************************/

package com.sap.isa.isacore.action.order;

import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 *  This action forward to the displayextrefobjconfirm.jsp
 */
public class ShowExtRefObjConfirmAction extends IsaCoreBaseAction  {
	
	public ActionForward isaPerform(ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response,
			UserSessionData userSessionData,
			RequestParser parser,
			BusinessObjectManager bom,
			IsaLocation log) throws CommunicationException {

		// Page that should be displayed next.
		String forwardTo = "success";
		
		
		int vinHits = 0;
		int z = 0;
		
		StringTokenizer st = new StringTokenizer(
								parser.getParameter("savedvins").getValue().getString(),
								";",
								false);
			
			
		while (st.hasMoreTokens()){
			String field = "vinNumber_".concat(String.valueOf(z));
			request.setAttribute(field, st.nextToken());
			z++;
		}
		vinHits = z;
		request.setAttribute("vinhits", String.valueOf(vinHits));
		
		
		if (parser.getParameter("level").getValue().getString().equals("0")) {
			// on item level
			request.setAttribute("level", "0");
		} else {
			// on header level
			request.setAttribute("level", "1");			
		}

		return mapping.findForward(forwardTo);
	}	
}