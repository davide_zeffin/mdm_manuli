/*****************************************************************************
  Class:        MaintainOrderCancelAction
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:
  Created:      18.09.2001
  Version:      1.0

  $Revision: #3 $
  $Date: 2002/08/05 $
*****************************************************************************/

package com.sap.isa.isacore.action.order;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businessobject.order.OrderChange;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.History;
import com.sap.isa.isacore.HistoryItem;
import com.sap.isa.isacore.action.IsaCoreInitAction;

/**
 * Action to cancel the change process of an order; the actual document is
 * thrown away.
 * <p>
 * The following forwards are used by this action:
 * </p>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>FORWARD_UPDATEDOCUMENT</td><td>always used</td></tr>
 * </table>
 *
 * @author
 * @version 1.0
 */
public class MaintainOrderCancelAction extends MaintainOrderBaseAction {

    protected String orderPerform(HttpServletRequest request,
                HttpServletResponse response,
                HttpSession session,
                UserSessionData userSessionData,
                RequestParser parser,
                BusinessObjectManager bom,
                IsaLocation log,
                IsaCoreInitAction.StartupParameter startupParameter,
                BusinessEventHandler eventHandler,
                boolean multipleInvocation,
                boolean browserBack,
                Shop shop,
                OrderChange order,
                DocumentState targetDocument,
                DocumentHandler documentHandler)
                    throws CommunicationException {
                    	
			final String METHOD_NAME = "orderPerform()";
			log.entering(METHOD_NAME);
            String forwardTo = null;
            // The order in CRM is dequeued implicitly with the intitialisation by the 
            // destroyContent call
            if (!shop.getBackend().equalsIgnoreCase(Shop.BACKEND_CRM)) {
            	order.dequeueOrder();   // release lock of the order
            }
            order.destroyContent(); //initialize order in backend
            order.clearData();
            bom.releaseOrder();

            if ( userSessionData.getAttribute("EAUCTION_PARAMID") != null ) {
            	History history = documentHandler.getHistory();
            	List historyList = history.getHistoryList();
            	HistoryItem hiItem = (HistoryItem)historyList.get(0);
            	history.remove(hiItem.getDocType(), hiItem.getDocNumber());
				userSessionData.removeAttribute("EAUCTION_PARAMID");
            }
			documentHandler.release(documentHandler.getDocumentOnTop());
            
            forwardTo = FORWARD_UPDATEDOCUMENT;
			log.exiting();
            return forwardTo;
    }
}