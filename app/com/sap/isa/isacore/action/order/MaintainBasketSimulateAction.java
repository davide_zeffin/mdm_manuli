/*****************************************************************************
  Class:        MaintainBasketSimulateAction
  Copyright (c) 2001, SAP AG, All rights reserved.
  Author:
  Created:      21.02.2002
  Version:      1.0

  $Revision: #8 $
  $Date: 2002/10/29 $
*****************************************************************************/

package com.sap.isa.isacore.action.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sap.isa.backend.boi.isacore.order.DocumentListFilterData;
import com.sap.isa.businessobject.AppBaseBusinessObjectManager;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.ManagedDocumentDetermination;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.IdMapper;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.order.Order;
import com.sap.isa.businessobject.order.OrderCreate;
import com.sap.isa.businessobject.order.PartnerListEntry;
import com.sap.isa.businessobject.ordertemplate.OrderTemplate;
import com.sap.isa.businessobject.quotation.Quotation;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.action.DeterminationAction;
import com.sap.isa.isacore.action.IsaCoreInitAction;
import com.sap.isa.isacore.action.PaymentActions;
import com.sap.isa.isacore.action.PrepareDeterminationAction;

/**
 * Action to simulate an oder in the Backend. Only used in case of R3 order and
 * Java Basket Backends.
 * The following forwards are used by this action:
 * </p>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>FORWARD_OCI_SUMMARY_BASKET</td><td>show a summary of the ordered products and offer an transfer in OCI format to EBP system</td></tr>
 *   <tr><td>FORWARD_OCI_SEND_BASKET</td><td>transfer basket in OCI format to EBP system</td></tr>
 *   <tr><td>FORWARD_EAUCTION</td><td>go to the special eAuction checkout process</td></tr>
 *   <tr><td>FORWARD_SUMMARY_BASKET</td><td>show a summary of the ordered products and offer a printout</td></tr>
 * </table>
 *
 * @author
 * @version 1.0
 */
public class MaintainBasketSimulateAction extends MaintainBasketBaseAction {

// Forward for UpdateDocumentViewAction
private static final String FORWARD_SIMULATE = "simulate";

/*
 * set if no soldto is specified
 */
public static final String NO_SOLDTO_MSG ="simulatenosoldto";

    /**
     * Creates a new instance of this class.
     */
    public MaintainBasketSimulateAction() {
    }


    protected String basketPerform(HttpServletRequest request,
                HttpServletResponse response,
                HttpSession session,
                UserSessionData userSessionData,
                RequestParser parser,
                BusinessObjectManager bom,
                IsaLocation log,
                IsaCoreInitAction.StartupParameter startupParameter,
                BusinessEventHandler eventHandler,
                boolean multipleInvocation,
                boolean browserBack,
                Shop shop,
                SalesDocument preOrderSalesDocument,
                boolean auction,
                DocumentState targetDocument,
                DocumentHandler documentHandler)
                    throws CommunicationException {

		final String METHOD_NAME = "basketPerform()";
		log.entering(METHOD_NAME);
        String forwardTo = null;

        boolean isDebugEnabled = log.isDebugEnabled();
        
		// process type used for creating the basket
		RequestParser.Parameter processType = null;
		String processTypeValue;  
		//determine the process type
		processType = parser.getParameter("processtype");
		processTypeValue = processType.isSet() ? processType.getValue().getString() : "";        
		if (log.isDebugEnabled()) { log.debug("Process type: " + processTypeValue); }	
		 
        // determine, what object to create
        String docType = request.getParameter("simulatepressed");
        
        RequestParser.Parameter calledFromDetermination =
                        parser.getParameter(DeterminationAction.RC_CALLED_FROM_DET);
        
        // In case the action is called from the product determination action there is nothing
        // to be parsed
        if (!calledFromDetermination.isSet()) {

            updateSalesDocument(parser, userSessionData, preOrderSalesDocument, shop, bom.createBUPAManager(), log);
 
            processCustomerNumberFields(parser, preOrderSalesDocument, shop, bom.createBUPAManager());
        }
        
        if (preOrderSalesDocument.isDeterminationRequired()) {
            request.setAttribute(PrepareDeterminationAction.RC_DET_CALLER, ManagedDocumentDetermination.CALLER_MAINTAIN_BASKET_SIMULATE);
            request.setAttribute(PrepareDeterminationAction.RC_DET_DOC_MODE, ManagedDocumentDetermination.DOC_MODE_NEW);
            request.setAttribute(PrepareDeterminationAction.RC_DET_SIMULATED_DOC, docType);
			log.exiting();
            return FORWARD_PRODDET;
        }

        // if there are any error messages for the preOrderSalesDocument or
        // items, you cannot order. The user stays on the same page.
        if (!preOrderSalesDocument.isValid()) {

            if (isDebugEnabled) {
                log.debug("Document NOT valid");
                log.debug("Message provided: " + preOrderSalesDocument.getMessageList());
            }
			log.exiting();
            return FORWARD_SHOWBASKET;
        }

        // is SoldTo set ? otherwise no simulation is possible
        TechKey soldToTechKey = null;

        PartnerListEntry soldToEntry = preOrderSalesDocument.getHeader().getPartnerList().getPartner(PartnerFunctionData.SOLDTO);

        if (soldToEntry != null) {
            soldToTechKey = soldToEntry.getPartnerTechKey();
        }

        if (soldToTechKey == null || soldToTechKey.getIdAsString().length() == 0) {
            userSessionData.setAttribute(NO_SOLDTO_MSG,"y");
			log.exiting();
            return FORWARD_SHOWBASKET;
        }

        // reread the basket, to synchronize all informations
        preOrderSalesDocument.readShipTos();
        preOrderSalesDocument.read();

        if (preOrderSalesDocument.getItems().size() == 0) {
			log.exiting();
            return FORWARD_SHOWBASKET;
        }

        // read catalog
        WebCatInfo webCat =
                getCatalogBusinessObjectManager(userSessionData).getCatalog();
		//Before Starting to create the documents check authorization of user
        //Check if user has permission for document Creation
		  Boolean perm = bom.getUser().hasPermission(docType,
		  									DocumentListFilterData.ACTION_CREATE);
		  if (!perm.booleanValue()){
			log.exiting();
			  throw new CommunicationException ("User has no permission to Create " 
			  									+ docType);
		  }
        SalesDocument simulatedDoc = null;
        String simulatedDocType = null;
        boolean execCampDet = true;
		// campaign/contract determination must be suppressed, when basket and order 
		//      have the same backend
		if (!preOrderSalesDocument.isExternalToOrder()) {
			execCampDet = false;
		}
        if (docType.equals(HeaderSalesDocument.DOCUMENT_TYPE_ORDER)) {

            bom.releaseOrder();
            Order order = bom.createOrder();
            order.setIdMapper(getIdMapper(userSessionData));
            OrderCreate orderCreate = new OrderCreate(order);

            if (isDebugEnabled) {
                log.debug("Simulate Basket - Create an Order");
            }

            // we always use copyFromExternalBasket, to avoid problems with the basket,
            // when deleting the order later.
            orderCreate.createFromExternalBasket(preOrderSalesDocument, shop, bom.createBUPAManager(), webCat, processTypeValue, execCampDet);

            orderCreate.simulate();

            simulatedDoc = order;
            simulatedDocType = HeaderSalesDocument.DOCUMENT_TYPE_ORDER;
            simulatedDoc.getHeader().setDocumentTypeOrder();

        } else if (docType.equals(HeaderSalesDocument.DOCUMENT_TYPE_QUOTATION)) {
            
           bom.releaseQuotation();
           Quotation quotation = bom.createQuotation();
           quotation.setIdMapper(getIdMapper(userSessionData));
           //now create the quotation and copy all data from the basket
           quotation.init(preOrderSalesDocument, shop, bom.createBUPAManager(), webCat, processTypeValue, execCampDet);

           quotation.simulate();

           simulatedDoc = quotation;
           simulatedDocType = HeaderSalesDocument.DOCUMENT_TYPE_QUOTATION;
           simulatedDoc.getHeader().setDocumentTypeQuotation();
           
        } else if (docType.equals(HeaderSalesDocument.DOCUMENT_TYPE_ORDERTEMPLATE)) {
            
           bom.releaseOrderTemplate();
           OrderTemplate template = bom.createOrderTemplate();
           template.setIdMapper(getIdMapper(userSessionData));
		   //now create the order template and copy all data from the basket
		   if (shop.isDocTypeLateDecision()) {
			   processTypeValue = shop.getTemplateProcessType();     
			   template.init(preOrderSalesDocument, shop, bom.createBUPAManager(), webCat, processTypeValue, execCampDet);                  
		   } 
		   else {
			   template.init(preOrderSalesDocument, shop, bom.createBUPAManager(), webCat, execCampDet);
		   }
    
           template.simulate();
    
           simulatedDoc = template;
           simulatedDocType = HeaderSalesDocument.DOCUMENT_TYPE_ORDERTEMPLATE;
           simulatedDoc.getHeader().setDocumentTypeOrderTemplate();
         }
        
        String basketKey = null;
        
        if (shop.isDocTypeLateDecision()) {
            basketKey = "basket.b2r";
        }
        else {
            basketKey = "basket";
        }

        simulatedDoc.setState(DocumentState.TARGET_DOCUMENT);
        ManagedDocument managedDocument = new ManagedDocument(
            simulatedDoc,
            basketKey, // used to determine
            "new",  // id
            null,
            null,
            null,
            FORWARD_SIMULATE,
            null,
            null);

        managedDocument.setAssociatedDocument(documentHandler.getManagedDocumentOnTop());

        documentHandler.add(managedDocument);
        documentHandler.setOnTop(simulatedDoc);
        
        // is determintion necessary for the simualted document ?
        if (simulatedDoc.isDeterminationRequired()) {
            request.setAttribute(PrepareDeterminationAction.RC_DET_CALLER, ManagedDocumentDetermination.CALLER_MAINTAIN_BASKET_SIMULATE);
            request.setAttribute(PrepareDeterminationAction.RC_DET_DOC_MODE, ManagedDocumentDetermination.DOC_MODE_SIMULATE);
            request.setAttribute(PrepareDeterminationAction.RC_DET_SIMULATED_DOC, simulatedDocType);
			log.exiting();
            return FORWARD_PRODDET;
        }

		if (PaymentActions.isPaymentMaintenanceAvailable(userSessionData, simulatedDoc) &&
		    docType.equals(HeaderSalesDocument.DOCUMENT_TYPE_ORDER) &&
		    simulatedDoc.isValid()) {			  
		  // remember where we come from
		 userSessionData.setAttribute(PaymentActions.PAYMENT_PROCESS, PaymentActions.BASKET_SIMULATION);
		 forwardTo = PaymentActions.FORWARD_TO_PAYMENT_MAINTAINENCE;
		} else {
         forwardTo = FORWARD_SIMULATE_BASKET;
		}
		log.exiting();
        return forwardTo;
    }
    
    
    protected IdMapper getIdMapper(UserSessionData userSessionData) {
    	 AppBaseBusinessObjectManager appBaseBom = (AppBaseBusinessObjectManager) userSessionData.getBOM(AppBaseBusinessObjectManager.APPBASE_BOM);
    	 return appBaseBom.createIdMapper();
    }
}