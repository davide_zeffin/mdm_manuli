/*****************************************************************************
  Class:        MaintainBatchBaseAction
  Copyright (c) 2001, SAP AG, Germany, All rights reserved.
  Author:       Daniel Seise
  Created:      25.11.2002
  Version:      1.0

  $Revision: #8 $
  $Date: 2002/11/25 $
*****************************************************************************/

package com.sap.isa.isacore.action.order;


import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 * Base action for all actions providing functionality to manage the batches. <br>
 * <p>
 * This action implements a template pattern, to keep the specialized
 * actions as simple as possible. All necessary data is retrieved by this
 * class and offered to the subclasses using a template design pattern.<br>
 * Subclasses are supposed to overwrite the abstract
 * <code>basketPerform</code> method and they normally do not touch the
 * <code>isaPerform</code> method.<br><br>
 * </p>
 * 
 * @author Daniel Seise
 * @version 1.0
 */
public class MaintainBatchBaseAction extends IsaCoreBaseAction {

	/*
	 * The result of the parse input fields is ver much the same structure that was used to set up the batch.jsp
	 * 
	 * Again some kind of tables are created, containing the following information:
	 * ----------------------------------------------------------
	 * | CharName | CharTxt | CharVal | CharValTxt | CharValMax |
	 * |--------------------------------------------------------|
	 * | Color    | Farbe   | Red     | Rot        |            |
	 * | Color    | Farbe   |         |            |            |
	 * | Color    | Farbe   | Green   | Grün       |            |
	 * | Length   | Länge   | 170     |            | 180        |
	 * | Length   | Länge   |         |            |            |
	 * ----------------------------------------------------------
	 * 
	 * ---------------------------
	 * | CharNameAddVal | AddVal |
	 * |-------------------------|
	 * | Color          | S      |
	 * | Length         | 120    |
	 * ---------------------------
	 * 
	 * This information is mapped to the structure of an item. But the item batch only holds those characteristic for a which at least
	 * one value was set:
	 * 
	 * item
	 *   |
	 *   |
	 *   |_BatchList
	 * 			|_Batch#1 (Color)
	 * 			|	|_CharVal (S)
	 * 			|	|_CharVal (Green)
	 * 			|	|_CharVal (Red)
	 * 			|
	 * 			|_Batch#2 (Length)
	 * 				|_CharVal (120)
	 * 				|_CharVal (170 - 180) 
	 * 
	 * One problem is, that the backend deals with additional values as with pre-defined one! Therefore some additional effort was necessary!
	 * 
	 */
	
	protected static void parseRequestItems(
			RequestParser parser,
			UserSessionData userSessionData,
			SalesDocument preOrderSalesDocument,
			IsaLocation log,
			boolean isDebugEnabled)
						throws CommunicationException {

		final String METHOD_NAME = "parseRequestItems()";
		log.entering(METHOD_NAME);
		
		// parse table like structured input fields from JSP
		
		RequestParser.Parameter itemGuid =
						parser.getParameter("itemGuid");
						
		RequestParser.Parameter batchElements =
						parser.getParameter("batchElement[]");
		
		RequestParser.Parameter char_name =
						parser.getParameter("char_name[]");
						
		RequestParser.Parameter char_txt =
						parser.getParameter("char_txt[]");
						
		RequestParser.Parameter add_value =
						parser.getParameter("add_value[]");
						
		RequestParser.Parameter char_name_add_value =
						parser.getParameter("char_name_add_value[]");
						
		RequestParser.Parameter values =
						parser.getParameter("values[]");
													
		RequestParser.Parameter values_txt =
						parser.getParameter("values_txt[]");
						
		RequestParser.Parameter values_to =
						parser.getParameter("values_to[]");
						
		
		// prepare item and some supportive parameters for mapping to item structure
		ItemSalesDoc item = preOrderSalesDocument.getItem(new TechKey(itemGuid.getValue().getString()));
		
		item.getBatchCharListJSP().clear();		// delete information that habe might have been set in a previous run
		String currentCharacteristic = "";		// since our table like structure has no uniquie characteristic key, we use this String to hold information about the current one
		int currentCharCounter = -1;			// counter to get the characteistic level in the item mapping 
		int addValCounter = 0;					// counter that saves the information which line of the add value belongs to which row of the value table
		boolean addCharacteristic = false;		// does the current row represent a new characteristic? if 'true' we have to add this to the item
		boolean addValue = false;				// do we have to deal with an additional value	
		
		
		// loop through the value table row-by-row
		for(int i=0; i<char_name.getNumValues(); i++){
			
			// check if the current row deals with an existing characteristic or a new one
			// this if clause sets the 'header' information and the addional value information, this is only required once per characteristic 
			if(!(char_name.getValue(i).getString()).equals(currentCharacteristic)){
				
				// set current character equal to new characteristic  
				currentCharacteristic = char_name.getValue(i).getString();
				
				// check if any value was set for, if not, we do not need to add this characteristic to the item batch
				if((values.getValue(i) != null && !values.getValue(i).getString().equals(""))
					|| values_to.getValue(i) != null && !values_to.getValue(i).getString().equals("")){
					addCharacteristic = true;
				}
				
				// check if an additoinal value was maintained for the current characteristic
				for(int j=0; j<char_name_add_value.getNumValues(); j++){
					if((currentCharacteristic.equals(char_name_add_value.getValue(j).getString()))
						&& (add_value.getValue(j) != null && !add_value.getValue(j).getString().equals(""))){
						addCharacteristic = true;
						addValue = true;
						addValCounter = j; // hold the current row	
					}
				}
				
				// add characteristic if predefined or an additional value was set
				if(addCharacteristic){
					currentCharCounter++;
					item.getBatchCharListJSP().add();
					item.getBatchCharListJSP().get(currentCharCounter).setCharName(char_name.getValue(i).getString());
					item.getBatchCharListJSP().get(currentCharCounter).setCharTxt(char_txt.getValue(i).getString());
				}
				
				// add the additional value, in case it was set
				if(addValue){
					item.getBatchCharListJSP().get(currentCharCounter).setCharValue(add_value.getValue(addValCounter).getString());	
					item.getBatchCharListJSP().get(currentCharCounter).setCharValTxt("");
					item.getBatchCharListJSP().get(currentCharCounter).setCharValMax("");
				}
				
				// no value was set and we did not add this characteristic, than reset the current characteristic
				if(!addCharacteristic) currentCharacteristic = "";
				addCharacteristic = false; addValue = false;
			}
			
			// add the predefined selected values to the current characteristics
			if((values.getValue(i) != null && !values.getValue(i).getString().equals(""))
					|| values_to.getValue(i) != null && !values_to.getValue(i).getString().equals("")){		
				item.getBatchCharListJSP().get(currentCharCounter).setCharValue(values.getValue(i).getString());	
				item.getBatchCharListJSP().get(currentCharCounter).setCharValTxt("");
				item.getBatchCharListJSP().get(currentCharCounter).setCharValMax(values_to.getValue(i).getString());
			}
		}
		log.exiting();
	}
}
