/*****************************************************************************
  Class:        MaintainBasketBatchAction
  Copyright (c) 2001, SAP AG, Germany, All rights reserved.
  Author:       Daniel Seise
  Created:      25.11.2002
  Version:      1.0

  $Revision: #8 $
  $Date: 2002/11/25 $
*****************************************************************************/

package com.sap.isa.isacore.action.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.action.IsaCoreInitAction;

/**
 * Action to display the batches for a material.
 * <p>
 * The following forwards are used by this action:
 * </p>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 * 	 <tr><td>FORWARD_SHOWBATCH</td><td>used when the document is valid</td></tr>
 *   <tr><td>FORWARD_SHOWBASKET</td><td>used when the document is not valid</td></tr>
 * </table>
 *
 * @author Daniel Seise
 * @version 1.0
 */
public class MaintainBasketBatchAction extends MaintainBasketBaseAction {

    /**
     * Creates a new instance of this class.
     */
    public MaintainBasketBatchAction() {
    }


    protected String basketPerform(HttpServletRequest request,
                HttpServletResponse response,
                HttpSession session,
                UserSessionData userSessionData,
                RequestParser parser,
                BusinessObjectManager bom,
                IsaLocation log,
                IsaCoreInitAction.StartupParameter startupParameter,
                BusinessEventHandler eventHandler,
                boolean multipleInvocation,
                boolean browserBack,
                Shop shop,
                SalesDocument preOrderSalesDocument,
                boolean auction,
                DocumentState targetDocument,
                DocumentHandler documentHandler)
                    throws CommunicationException {


			final String METHOD_NAME = "basketPerform()";
			log.entering(METHOD_NAME);
            updateSalesDocument(parser, userSessionData, preOrderSalesDocument, shop, bom.createBUPAManager(), log);

            processCustomerNumberFields(parser, preOrderSalesDocument, shop, bom.createBUPAManager());
			boolean isDebugEnabled = log.isDebugEnabled();
			
			
			if (!preOrderSalesDocument.isValid()) {

            	if (isDebugEnabled) {
                	log.debug("Document NOT valid");
                	log.debug("Message provided: " + preOrderSalesDocument.getMessageList());
            	}
				log.exiting();
           		return FORWARD_SHOWBASKET;
        	}
        	else{
       
        		
            	userSessionData.setAttribute("batchselection", request.getParameter("batchselection"));
            	if (preOrderSalesDocument.getItem(new TechKey((String)userSessionData.getAttribute("batchselection"))) != null){
					userSessionData.setAttribute("batchproductid", request.getParameter("batchproductid"));
					userSessionData.setAttribute("batchproduct", request.getParameter("batchproduct"));
					log.exiting();
					return FORWARD_SHOWBATCH;
            	}
            	else{
					log.exiting();
            		return FORWARD_SHOWBASKET;
            	}
        	}



        
    }
}