/*****************************************************************************
    Class         ShowOrderConfirmedAction
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Action for the Profile Maintenance
    Author:       
    Created:      05. Juni 2001
    Version:      0.1
*****************************************************************************/

package com.sap.isa.isacore.action.order;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.ConnectedDocument;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.businessobject.lead.Lead;
import com.sap.isa.businessobject.order.Basket;
import com.sap.isa.businessobject.order.DocumentListFilter;
import com.sap.isa.businessobject.order.Order;
import com.sap.isa.businessobject.ordertemplate.OrderTemplate;
import com.sap.isa.businessobject.quotation.Quotation;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.ContextConst;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.interaction.InteractionConfig;
import com.sap.isa.core.interaction.InteractionConfigContainer;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.MessageList;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ItemHierarchy;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.action.PaymentActions;
import com.sap.isa.isacore.uiclass.ItemConfigurationInfoHelper;

/**
 * Read and display the confirmed order.
 *
 * @author Steffen Mueller
 * @version 0.1
 *
 */
public class ShowOrderConfirmedAction extends IsaCoreBaseAction {
    
    public static String RC_CONFIRMED_DOCTYPE = "confirmed.doctype";

    /**
     * Signal to JSP avoid reseting mini basket content.<br>
     * Positive value <code>TRUE</code>.
     */
    public static final String RK_RESET_MINBASKET = "resetminibasket";

    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser parser,
            BusinessObjectManager bom,
            IsaLocation log) throws CommunicationException {

		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        Shop shp = bom.getShop();

        // For ordering a quotation a different index could be used for bom order creation
        boolean confirmQuotToOrderFlag = false;
        if (userSessionData.getAttribute(
               DocumentStatusOrderQuotAction.SC_QUOT_TO_ORDER) != null && 
            userSessionData.getAttribute(
			   DocumentStatusOrderQuotAction.SC_QUOT_TO_ORDER).equals("TRUE") ) {
            confirmQuotToOrderFlag = true;
        }

        // get pre-order document on top from document handler
        DocumentHandler documentHandler = (DocumentHandler)
            userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
        if (documentHandler == null) {
			log.exiting();
            throw new PanicException("No document handler found");
        }
        DocumentState targetDocument =
            documentHandler.getDocument(ManagedDocument.TARGET_DOCUMENT);

        String href_action = "b2b/documentstatusdetailprepare.do";

        // get the current pre order sales document - basket is assumed as
        // default
        SalesDocument preOrderSalesDocument = null;
		// To avoid conflicts with the currently in change mode document, use a order with another Index
		if (confirmQuotToOrderFlag) {
			// Ask the business object manager to get the order created from
			// basket
 			Order order = bom.getOrder(DocumentStatusOrderQuotAction.ORDER_INDEX);
            
            setRequestAttributes(order, shp, DocumentListFilter.SALESDOCUMENT_TYPE_ORDER,request);

			// add payment data to the request
			PaymentActions.setPaymentAttributes(userSessionData,request,order);
            // note upport 1294268
			documentHandler.release(order);
			setManagedDocument(order, documentHandler, DocumentListFilter.SALESDOCUMENT_TYPE_ORDER, href_action);

			List predDocList = order.getHeader().getPredecessorList();
			ConnectedDocument predDoc;
			TechKey leadKey = null;
			
			for (int i = 0; i < predDocList.size(); i++) {
				predDoc = (ConnectedDocument) predDocList.get(i);
				if (predDoc.getDocType().equals(HeaderData.DOCUMENT_TYPE_LEAD))  {					
					leadKey = predDoc.getTechKey();
					break;
				}
			}
                        
			bom.releaseOrder(DocumentStatusOrderQuotAction.ORDER_INDEX);
            
			userSessionData.removeAttribute(DocumentStatusOrderQuotAction.SC_QUOT_TO_ORDER);
			PaymentActions.removePaymentProcessAttribute(userSessionData);
			request.setAttribute(RK_RESET_MINBASKET,"TRUE");
			if (leadKey != null && leadKey.getIdAsString().length() > 0) {					
					setLeadStatus(leadKey, userSessionData, bom, shp );					            
			}	      
			log.exiting();
			return mapping.findForward("showorder");
                
		} else if (targetDocument instanceof Quotation) {
            preOrderSalesDocument = bom.getQuotation();
            ((Quotation)preOrderSalesDocument).readHeaderStatus();
            
            setRequestAttributes(preOrderSalesDocument, shp, DocumentListFilter.SALESDOCUMENT_TYPE_QUOTATION, request);
            // note upport 1294268
            // the confirmation is only a temporary screen, if other screens are displayed the confirmation is lost
            // for this we must clean up our objects
            // release from DocumentHandler
            documentHandler.release(preOrderSalesDocument);
			setManagedDocument(preOrderSalesDocument, documentHandler, DocumentListFilter.SALESDOCUMENT_TYPE_QUOTATION, href_action);

            
            // release from bom
            bom.releaseQuotation();                   
			log.exiting();
            return mapping.findForward("showquotation");
        }
        else if (targetDocument instanceof OrderTemplate) {
            preOrderSalesDocument = bom.getOrderTemplate();
            
            setRequestAttributes(preOrderSalesDocument, shp, DocumentListFilter.SALESDOCUMENT_TYPE_ORDERTEMPLATE, request);
            
            // depending on the setting of the forceIPCPricing flag in the Jasva-Basket
            // the set of available prices must be determined
            setAvailableValueFlagsForJSP(preOrderSalesDocument, request);
            
			// note upport 1294268
            // the confirmation is only a temporary screen, if other screens are displayed the confirmation is lost
            // for this we must clean up our objects
            // release from DocumentHandler
            documentHandler.release(preOrderSalesDocument);
			setManagedDocument(preOrderSalesDocument, documentHandler, DocumentListFilter.SALESDOCUMENT_TYPE_ORDERTEMPLATE, href_action);
            
            
            // release from bom
            bom.releaseOrderTemplate();
			log.exiting();    			
            return mapping.findForward("showordertemplate");
        }
        else if (targetDocument instanceof Basket){
            // the confirmation is only a temporary screen, if other screens are displayed the confirmation is lost
            // for this we must clean up our objects
            // In the order.MaintainBasketBasketAction we created a new order for the basket,
            // now we know that we don't need the basket anymore.
            // release from DocumentHandler and BOM
            Basket basket = bom.getBasket();
            bom.releaseBasket();
            documentHandler.release(basket);
            // Ask the business object manager to get the order created from
            // basket
            Order order = bom.getOrder();
            
            setRequestAttributes(order, shp, DocumentListFilter.SALESDOCUMENT_TYPE_ORDER, request);

			// add payment data to the request
			PaymentActions.setPaymentAttributes(userSessionData,request,order);
			PaymentActions.removePaymentProcessAttribute(userSessionData);
            
            setManagedDocument(order, documentHandler, DocumentListFilter.SALESDOCUMENT_TYPE_ORDER, href_action);
            // the confirmation is only a temporary screen, if other screens are displayed the confirmation is lost
            // for this we must clean up our objects
            // release from DocumentHandler
            documentHandler.release(order);
			List predDocList = order.getHeader().getPredecessorList();
			ConnectedDocument predDoc;
			TechKey leadKey = null;
			
			for (int i = 0; i < predDocList.size(); i++) {
				predDoc = (ConnectedDocument) predDocList.get(i);
				if (HeaderData.DOCUMENT_TYPE_LEAD.equals(predDoc.getDocType()))  {					
					leadKey = predDoc.getTechKey();
					break;
				}
			}
			if (leadKey != null && leadKey.getIdAsString().length() > 0) {
					setLeadStatus(leadKey, userSessionData, bom, shp );          
			}

            bom.releaseOrder();                
			log.exiting();
            return mapping.findForward("showorder");
        }

		log.exiting();
        
        return mapping.findForward("error");
    }
    
    /**
     * Method to set the request attributes
     * @author SAP
     *
     * @param salesDoc the salesDocument to retrieve the request attributes from
     * @param shp the shop
     * @param request the HTTP request, to set the attributes in
     */
    protected void setRequestAttributes(SalesDocument salesDoc, Shop shp, String documentType, HttpServletRequest request)
            throws CommunicationException {
        request.setAttribute(MaintainOrderBaseAction.RC_HEADER, salesDoc.getHeader());
        request.setAttribute(MaintainOrderBaseAction.RC_DOC_RECOVERABLE, (salesDoc.isDocumentRecoverable())? "Y":"");
        request.setAttribute(MaintainOrderBaseAction.RC_ITEMS, salesDoc.getItems());
        // determine inline config info, before document is destroyed
        ItemConfigurationInfoHelper itemConfigInfo = new ItemConfigurationInfoHelper();
        ItemHierarchy itemHierarchy = new ItemHierarchy(salesDoc.getItems());
        char configInfoView = getOrderView(request); 
        char configInfoDetailView = getOrderDetailView(request);
        HashMap configInfoMap = new HashMap();
        HashMap configInfoDetailMap = new HashMap();
        
        if (configInfoView != ' ' || configInfoDetailView != ' ') {
            for (int i=0; i < salesDoc.getItems().size(); i++) {
                ItemSalesDoc item = salesDoc.getItems().get(i);
                if (item.isConfigurable()) { 
                      itemConfigInfo.setItem(item); 
                      if (itemConfigInfo.isConfigurationAvailable()) {
                          if (configInfoView != ' ' && !(itemHierarchy.isSubItem(item) && item.isItemUsageConfiguration())) {
                              configInfoMap.put(item.getTechKey().getIdAsString(), itemConfigInfo.getItemConfigValues(configInfoView));
                          }
                          if (configInfoDetailView != ' ') {
                              configInfoDetailMap.put(item.getTechKey().getIdAsString(), itemConfigInfo.getItemConfigGroups(configInfoDetailView));
                          }
                      }
                }
            }
        }
        
        request.setAttribute(MaintainOrderBaseAction.RC_CONFIG_INFO, configInfoMap);
        request.setAttribute(MaintainOrderBaseAction.RC_CONFIG_INFO_DETAIL, configInfoDetailMap);
        
        request.setAttribute(MaintainOrderBaseAction.RC_SHIPCOND, salesDoc.readShipCond(shp.getLanguage()));
        request.setAttribute(MaintainOrderBaseAction.RC_CAMPDESC, salesDoc.getCampaignDescriptions());
        request.setAttribute(MaintainOrderBaseAction.RC_CAMPTYPEDESC, salesDoc.getCampaignTypeDescriptions());
        request.setAttribute(MaintainOrderBaseAction.RC_MESSAGES, salesDoc.getMessageList());
        request.setAttribute(RC_CONFIRMED_DOCTYPE, documentType);
    }
    
    /**
     * Method to set the managed document
     * @author SAP
     *
     * @param salesDoc the salesDocument to retrieve the request attributes from
     * @param documentHandler The document handler
     * @param documenType string that defines the documenttype
     * @param href_action the action to use for the manged document
     */
    protected void setManagedDocument(SalesDocument salesDoc, DocumentHandler documentHandler,
                                      String documentType, String href_action) {
                                                                                 
        // Build href for ManagedDocument Class
        HeaderSalesDocument preOrderSalesDocHeader = salesDoc.getHeader();
        String href_parameter = ("techkey=".concat(salesDoc.getTechKey().getIdAsString())).concat("&");
        href_parameter = ((href_parameter.concat("object_id=")).concat(preOrderSalesDocHeader.getSalesDocNumber())).concat("&");
        href_parameter = href_parameter.concat("objects_origin=");
        href_parameter = (href_parameter.concat("&objecttype=")).concat(documentType);

        // add the document to the history
        ManagedDocument mDoc = new ManagedDocument(salesDoc,
                                                   documentType,
                                                   preOrderSalesDocHeader.getSalesDocNumber(),
                                                   preOrderSalesDocHeader.getPurchaseOrderExt(),
                                                   preOrderSalesDocHeader.getDescription(),
                                                   //preOrderSalesDocHeader.getChangedAt(),  CHN 978390 changed at date is not consistent to other
                                                   preOrderSalesDocHeader.getCreatedAt(), // application parts (see also class: ChangeOrderBaseAction)
                                                   null,
                                                   href_action,
                                                   href_parameter);
		mDoc.setState(DocumentState.VIEW_DOCUMENT);  
        documentHandler.add(mDoc);
		documentHandler.setOnTop(salesDoc);
    }
    
    /**
     * Returns configuration valid for the current session. This feature
     * is only available if Extended Configuration Management is turned on.
     * If no configuration data is available <code>null</code> is returned
     * @return configuration data
     */
    public InteractionConfigContainer getInteractionConfig(HttpServletRequest request) {
        UserSessionData userData =
                 UserSessionData.getUserSessionData(request.getSession());
        if (userData == null) {
            return null;
        }
   
        return (InteractionConfigContainer)(userData.getAttribute(SessionConst.INTERACTION_CONFIG));
     }
    
    /**
     * Determines from XCM the application view on the product configuration, 
     * which should be used for display in the item list of the order screen. 
     * 
     * 
     * @return The view represented by a single character
     *
     */ 
    public char getOrderView(HttpServletRequest request) {
        char view = ' ';
        InteractionConfigContainer icc = getInteractionConfig(request);
        InteractionConfig confview = icc.getConfig("ui");
        String iccView = confview.getValue(ContextConst.IC_ITEM_CONFIG_ORDER_VIEW);
        if (iccView != null && iccView.length() > 0) {
            view = iccView.charAt(0);
        }
        return view;
    }
    
    /**
     * Determines from XCM the application view on the product configuration, 
     * which should be used for display in the item list of the order
     *  confirmation screen (detailed view). 
     * 
     * @return The view represented by a single character
     *
     */ 
    public char getOrderDetailView(HttpServletRequest request) {
        char view = ' ';
        InteractionConfigContainer icc = getInteractionConfig(request);
        InteractionConfig confview = icc.getConfig("ui");
        String iccView = confview.getValue(ContextConst.IC_ITEM_CONFIG_ORDER_DETAIL_VIEW);
        if (iccView != null && iccView.length() > 0) {
            view = iccView.charAt(0);
        }       
        return view;
    }
     
	private void setLeadStatus(TechKey leadKey, UserSessionData userSessionData, BusinessObjectManager bom, Shop shp) throws CommunicationException  {
			// read catalog
			WebCatInfo webCat =
				getCatalogBusinessObjectManager(userSessionData).getCatalog();

			Lead lead = bom.createLead();
			lead.setTechKey(leadKey);
		  //lead.init(shop, user, webCat);
		  //lead.setGData(shop,webCat);
		  lead.readForUpdate();
            
			// is lead valid?            
			//if (lead.isValid()) {
                        
			  lead.changeStatusToWon();
			  lead.update(bom.createBUPAManager(), shp);
			  lead.read();
			  MessageList msgList = lead.getMessageList();
			  if (msgList.size() > 0) {
				  for (int i = 0; i < msgList.size(); i++) {
					  log.debug(msgList.get(i).getDescription());
				  }
			  }
    	        
			  lead.save();
			  bom.releaseLead();
            
//			  } else {
//            	
//				//display error messages for lead
//				  // get error messages
//				  MessageDisplayer messageDisplayer = new MessageDisplayer();
//				
//				messageDisplayer.addToRequest(request);
//				messageDisplayer.copyMessages(lead);
//				messageDisplayer.setAction("updatedocumentview.do");
//
//				forwardTo = "leadNotValid";
//            
//			  }
	}
    
}