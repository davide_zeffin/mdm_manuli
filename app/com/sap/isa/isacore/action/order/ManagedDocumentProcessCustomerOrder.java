package com.sap.isa.isacore.action.order;

import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.isacore.ManagedDocument;

/**
 * This class is used to flag that a customer is processing.
 * The flag is used in the MaintainShipToAction to add a new shipTo to the
 * collective order instead to the active document.
 * 
 * @see com.sap.isa.isacore.action.MaintainShipToAction
 *
 */
public class ManagedDocumentProcessCustomerOrder extends ManagedDocument {
    
    /**
     *  Constructor
     *
     *@param  doc            Reference off the corresponding business object.
     *@param  docType        Type of the document, e.g. "order", "quotation", "order template".
     *@param  docNumber      Number of the document, given by the backend.
     *@param  refNumber      The reference number; the number the customer can assign to the document.
     *@param  refName        The reference name; the name the customer can assign to the document.
     *@param  docDate        Date of the document.
     *@param  forward        String for the logical forward mapping in the struts framework.
     *@param  href           Href for displaying the document
     *@param  hrefParameter  Additional parameters for the String href.
     */
    public ManagedDocumentProcessCustomerOrder(DocumentState doc, String docType, String docNumber, String refNumber,
            String refName, String docDate, String forward, String href, String hrefParameter) {

            super(doc, docType, docNumber, refNumber, refName, docDate, forward, href, hrefParameter);
    }
  

}
