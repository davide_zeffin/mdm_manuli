/*****************************************************************************
	Class:        CEDocumentListAction
	Copyright (c) 2003, SAP AG, Germany, All rights reserved.
	Author        SAP

	$Revision: #3 $
	$DateTime: 2003/07/08 11:50:18 $ (Last changed)
	$Change: 137030 $ (changelist)
    
*****************************************************************************/
package com.sap.isa.isacore.action.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.actionform.order.DocumentListSelectorForm;

/**
 * Action to enable a cross entry call to the DocumentListAction. Allowed
 * parameters are referenced from <code>DocumentListSelectorForm</code>.
 */
public class CEDocumentListAction extends IsaCoreBaseAction {

    /**
     * Use this request parameter to pass a specific forward. List type should
     * be a Documentlist or a CustomerDocumentList (HOM)).
     */
    public static final String RK_LISTTYPE = "cedsls_listtype";

    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            BusinessObjectManager bom,
            IsaLocation log)
            throws CommunicationException {

		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        DocumentListSelectorForm dlsf = new DocumentListSelectorForm();
        
        // Handle Document Type
        String param = request.getParameter(DocumentListSelectorForm.DOCUMENT_TYPE);
        if (param != null) {
            dlsf.setDocumentType(param);
        }
		// Handle Document Status
		param = request.getParameter(DocumentListSelectorForm.DOCUMENT_STATUS);
		if (param != null) {
			dlsf.setDocumentStatus(param);
		}
		// Handle Validity Selection (Week, Month   OR 20030714)
		param = request.getParameter(DocumentListSelectorForm.VALIDITYSELECTION);
		if (param != null) {
			dlsf.setValiditySelection(param);
		}
		// Handle Period
		param = request.getParameter(DocumentListSelectorForm.PERIOD);
		if (param != null) {
			dlsf.setPeriod(param);
		}

		// Start selection immediately
        dlsf.setSalesSelectionStart(true);
        
        dlsf.validate(mapping, request);
        
        userSessionData.setAttribute(DocumentListBaseAction.SC_CROSSENTRYCALL, "true");
        // Store Bean in session. By adding 'crossentry' to session standard
        // action accepts FormBean from session!!
        userSessionData.setAttribute(DocumentListBaseAction.SC_FORMBEAN, dlsf);
        
        String forward = request.getParameter(RK_LISTTYPE);  
        if (forward == null) {
            forward = "standarddoclist";
        }
        // Write some useful debug infos
        if (log.isDebugEnabled()) log.debug("Specified forward: " + forward);
		if (log.isDebugEnabled()) log.debug("DocumentListSelectorForm: " + dlsf.toString());
		log.exiting();
        return mapping.findForward(forward);

    }
}

