/*****************************************************************************
  Class:        CreateMultipleShiptoItemsAction
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       SAP
  Created:      March 2005
  Version:      1.0

  *****************************************************************************/

package com.sap.isa.isacore.action.order;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.ShipTo;
import com.sap.isa.businessobject.item.ItemList;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.spc.remote.client.object.IPCItem;

public class CreateMultipleShiptoItemsAction extends IsaCoreBaseAction {
        
	protected String FORWARD_SUCCESS  = "success";
	
	/**
	  * @see com.sap.isa.isacore.action.IsaCoreBaseAction#isaPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.sap.isa.core.UserSessionData, com.sap.isa.core.util.RequestParser, com.sap.isa.businessobject.BusinessObjectManager, com.sap.isa.core.logging.IsaLocation)
	  */
	 public ActionForward isaPerform(ActionMapping mapping,
									 ActionForm form,
									 HttpServletRequest request,
									 HttpServletResponse response,
									 UserSessionData userSessionData,
									 RequestParser requestParser,
									 BusinessObjectManager bom,
									 IsaLocation log)
			 throws IOException, ServletException, CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
		 
		 
		if (request.getParameter("multishiptobut") != null &&
			request.getParameter("multishiptobut").length() > 0) {

			String multishipto= (String) userSessionData.getAttribute("itemshipto");
		
			if (multishipto != null &&
			    multishipto.length() > 0){				 
			
				//get the item fields from the request
				RequestParser.Parameter multishiptos =
						  requestParser.getParameter("mshiptoval[]");
				 
				ArrayList multiShiptos = new ArrayList(); 
			
				for(int i=0;i<multishiptos.getNumValues();i++){
					String shipto= multishiptos.getValue(i).getString();
					if (shipto != null && shipto.length()>0)
						multiShiptos.add(shipto);
				} 
				userSessionData.setAttribute("multiShipTos", multiShiptos);
		
				createNewItemsForSelectedShiptos(bom,userSessionData, multiShiptos, new TechKey(multishipto));
			
				//remove the session attribute
				userSessionData.removeAttribute("itemshipto");
			}
		}	 
		return mapping.findForward(FORWARD_SUCCESS) ;
	}

	/**
	 * @param userSessionData
	 * @param multiShiptos
	 */
	private void createNewItemsForSelectedShiptos(BusinessObjectManager bom,
												  UserSessionData userSessionData, 
												  ArrayList multiShiptos, 
												  TechKey itemId) throws CommunicationException {
	
		DocumentHandler documentHandler = (DocumentHandler)
		userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
		if (documentHandler == null) {
			log.exiting();
			throw new PanicException("No document handler found");
		}
		
		ManagedDocument onTopDoc = documentHandler.getManagedDocumentOnTop();
		DocumentState document = onTopDoc.getDocument();

		if (document instanceof SalesDocument) {
			
			SalesDocument salesDocument = (SalesDocument)onTopDoc.getDocument();   
			
			//Get the itemList			
			ItemList items = salesDocument.getItems();
			
			//Get the item that has to be copied to different selected shiptos
			ItemSalesDoc mainItem = items.get(itemId);
			
			Object[] selectedShiptos = multiShiptos.toArray();
			
			for (int i=0;i<selectedShiptos.length;i++) {
				if (selectedShiptos[i].toString().equals("")) 
					continue;
					
				ItemSalesDoc newItem = new ItemSalesDoc();
				newItem.setProduct(mainItem.getProduct());
				newItem.setReqDeliveryDate(mainItem.getReqDeliveryDate());
				newItem.setQuantity(mainItem.getQuantity());
				newItem.setUnit(mainItem.getUnit());
				newItem.setNetPrice(mainItem.getNetPrice());
				newItem.setNetValue(mainItem.getNetValue());
				newItem.setGrossValue(mainItem.getGrossValue());
				
				ShipTo selectedShipTo = salesDocument.getShipTo(new TechKey(selectedShiptos[i].toString()));
				newItem.setShipTo((selectedShipTo == null) ? ShipTo.SHIPTO_INITIAL : selectedShipTo);
				
				//newItem.setLatestDlvDate(mainItem.getLatestDlvDate()); //is this required?
				
				//Copy the ipcItem of ISA item to be copied and attach it  
				//new ISA item being copied.
				IPCItem newIpcItem =salesDocument.copyIPCItem(itemId);
				newItem.setExternalItem(newIpcItem);
				items.add(newItem);
			}
			salesDocument.update(bom.getBUPAManager());
		}	
	}	
}	
