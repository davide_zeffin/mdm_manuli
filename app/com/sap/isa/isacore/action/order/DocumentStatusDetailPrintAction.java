/*****************************************************************************
    Class:        DocumentStatusDetailPrintAction
    Copyright (c) 2002, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author
    Created:      May 2002

    $Revision: #1 $
    $Date: 2001/06/26 $
*****************************************************************************/

package com.sap.isa.isacore.action.order;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.businessobject.order.CustomerOrderStatus;
import com.sap.isa.businessobject.order.OrderStatus;
import com.sap.isa.core.ContextConst;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.interaction.InteractionConfig;
import com.sap.isa.core.interaction.InteractionConfigContainer;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.ItemHierarchy;
import com.sap.isa.isacore.uiclass.ItemConfigurationInfoHelper;

/**
 * Handling to print a document in status display mode
 *
 * <br> Note that all Action classes have to provide a non argument constructor
 * because they were instantiated automatically by the action servlet.
 *
 * @author
 * @version 1.0
 *
 */
public class DocumentStatusDetailPrintAction extends DocumentStatusBaseAction {

    public static final String FORWARD_ORDER = "order";
    public static final String FORWARD_ORDERTEMPLATE = "ordertemplate";
    public static final String FORWARD_QUOTATION = "quotation";

    /**
     * Request context constant call of confirmation screen from orderstatus
     */
    public static final String RC_PRINTORDERSTATUS     = "printorderstatus";

	/**
	 * Request context constant call of confirmation screen from HOM orderstatus
	 */
	public static final String RC_CUSTOMERORDERSTATUS   = "customerorderstatus";

    /**
     * Perform order status.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param orderStatus       The order status object
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward performOrderStatus(ActionMapping mapping,
                                            ActionForm form,
                                            HttpServletRequest request,
                                            HttpServletResponse response,
                                            UserSessionData userSessionData,
                                            RequestParser requestParser,
                                            BusinessObjectManager bom,
                                            OrderStatus orderStatus,
                                            IsaLocation log)
            throws CommunicationException {
		final String METHOD_NAME = "performOrderStatus()";
		log.entering(METHOD_NAME);
        String forward = null;

        request.setAttribute( RC_PRINTORDERSTATUS,"true");

        request.setAttribute(MaintainOrderBaseAction.RC_HEADER, orderStatus.getOrderHeader());
        request.setAttribute(MaintainOrderBaseAction.RC_ITEMS, orderStatus.getItemList());
        
        // determine inline config info
       ItemConfigurationInfoHelper itemConfigInfo = new ItemConfigurationInfoHelper();
       ItemHierarchy itemHierarchy = new ItemHierarchy(orderStatus.getItemList());
       char configInfoView = getOrderView(request); 
       char configInfoDetailView = getOrderDetailView(request);
       HashMap configInfoMap = new HashMap();
       HashMap configInfoDetailMap = new HashMap();

       for (int i=0; i < orderStatus.getItemList().size(); i++) {
           ItemSalesDoc item = orderStatus.getItemList().get(i);
           if (item.isConfigurable()) { 
                 itemConfigInfo.setItem(item); 
                 if (itemConfigInfo.isConfigurationAvailable()) {
                     if (configInfoView != ' ' && !(itemHierarchy.isSubItem(item) && item.isItemUsageConfiguration())) {
                         configInfoMap.put(item.getTechKey().getIdAsString(), itemConfigInfo.getItemConfigValues(configInfoView));
                     }
                     if (configInfoDetailView != ' ') {
                         configInfoDetailMap.put(item.getTechKey().getIdAsString(), itemConfigInfo.getItemConfigGroups(configInfoDetailView));
                     }
                 }
           }
       }
       request.setAttribute(MaintainOrderBaseAction.RC_CONFIG_INFO, configInfoMap);
       request.setAttribute(MaintainOrderBaseAction.RC_CONFIG_INFO_DETAIL, configInfoDetailMap);
        
		request.setAttribute(MaintainOrderBaseAction.RC_CAMPDESC, orderStatus.getCampaignDescriptions());
		request.setAttribute(MaintainOrderBaseAction.RC_CAMPTYPEDESC, orderStatus.getCampaignTypeDescriptions());
        boolean isLargeDoc = orderStatus.isLargeDocument(bom.getShop().getLargeDocNoOfItemsThreshold());
        request.setAttribute(MaintainOrderBaseAction.RC_IS_LARGE_DOC, (isLargeDoc) ? "Y":"");

        String docType = orderStatus.getOrderHeader().getDocumentType();

        if (docType.equals(HeaderSalesDocument.DOCUMENT_TYPE_QUOTATION)) {
            forward = FORWARD_QUOTATION;
        } 
        else if (docType.equals(HeaderSalesDocument.DOCUMENT_TYPE_ORDERTEMPLATE)) {
            // depending on the setting of the forceIPCPricing flag in the Jasva-Basket
            // the set of available prices must be determined
            setAvailableValueFlagsForJSP((SalesDocument) orderStatus.getOrder(), request);
            forward = FORWARD_ORDERTEMPLATE;
        }
        else {
            forward = FORWARD_ORDER;
        } 
        
		request.setAttribute(MaintainOrderBaseAction.RC_MESSAGES, orderStatus.getOrder().getMessageList());
        request.setAttribute(ShowOrderConfirmedAction.RC_CONFIRMED_DOCTYPE, docType);

		// check if the order is hosted order document.
        if (orderStatus instanceof CustomerOrderStatus) {
			request.setAttribute(RC_CUSTOMERORDERSTATUS, "true");
        }

		userSessionData.setAttribute(MaintainBasketBaseAction.SC_SHIPTOS, orderStatus.getShipTos());    
		log.exiting();
        return mapping.findForward(forward);
    }
    
    /**
     * Returns configuration valid for the current session. This feature
     * is only available if Extended Configuration Management is turned on.
     * If no configuration data is available <code>null</code> is returned
     * @return configuration data
     */
    public InteractionConfigContainer getInteractionConfig(HttpServletRequest request) {
        UserSessionData userData =
                 UserSessionData.getUserSessionData(request.getSession());
        if (userData == null) {
            return null;
        }
   
        return (InteractionConfigContainer)(userData.getAttribute(SessionConst.INTERACTION_CONFIG));
     }
    
    /**
     * Determines from XCM the application view on the product configuration, 
     * which should be used for display in the item list of the order screen. 
     * 
     * 
     * @return The view represented by a single character
     *
     */ 
    public char getOrderView(HttpServletRequest request) {
        char view = ' ';
        InteractionConfigContainer icc = getInteractionConfig(request);
        InteractionConfig confview = icc.getConfig("ui");
        String iccView = confview.getValue(ContextConst.IC_ITEM_CONFIG_ORDER_VIEW);
        if (iccView != null && iccView.length() > 0) {
            view = iccView.charAt(0);
        }
        return view;
    }
    
    /**
     * Determines from XCM the application view on the product configuration, 
     * which should be used for display in the item list of the order
     *  confirmation screen (detailed view). 
     * 
     * @return The view represented by a single character
     *
     */ 
    public char getOrderDetailView(HttpServletRequest request) {
        char view = ' ';
        InteractionConfigContainer icc = getInteractionConfig(request);
        InteractionConfig confview = icc.getConfig("ui");
        String iccView = confview.getValue(ContextConst.IC_ITEM_CONFIG_ORDER_DETAIL_VIEW);
        if (iccView != null && iccView.length() > 0) {
            view = iccView.charAt(0);
        }       
        return view;
    } 

}