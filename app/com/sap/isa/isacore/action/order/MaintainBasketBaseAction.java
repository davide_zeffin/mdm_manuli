/*****************************************************************************
  Class:        MaintainBasketBaseAction
  Copyright (c) 2001, SAP AG, All rights reserved.
  Author:       SAP AG
  Created:      10.09.2001
  Version:      1.0

  $Revision: #11 $
  $Date: 2003/02/12 $
*****************************************************************************/

package com.sap.isa.isacore.action.order;

import java.util.HashSet;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.businessobject.Address;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.ShipTo;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businessobject.item.ItemList;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.businessobject.order.PartnerListEntry;
import com.sap.isa.businessobject.ordertemplate.OrderTemplate;
import com.sap.isa.businessobject.quotation.Quotation;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.businesspartner.businessobject.PartnerFunctionBase;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.GenericFactory;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.MessageList;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.action.IsaCoreInitAction;
import com.sap.isa.isacore.action.SalesDocumentParser;
import com.sap.isa.isacore.action.campaign.AddCampaignToCatalogueAction;
import com.sap.isa.isacore.action.campaign.GetCampaignAction;

/**
 * Base action for all actions providing functionality to manage the shopping
 * basket and sales documents similar to the basket. <br>
 * <p>
 * This action implements a template pattern, to keep the specialized
 * actions as simple as possible. All necessary data is retrieved by this
 * class and offered to the subclasses using a template design pattern.<br>
 * Subclasses are supposed to overwrite the abstract
 * <code>basketPerform</code> method and they normally do not touch the
 * <code>isaPerform</code> method.<br><br>
 * </p>
 * <p>
 * The following forwards are used by this action:
 * </p>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>FORWARD_UPDATEDOCUMENT</td><td>forward used if something goes wrong to allow application to reach an consitent state again</td></tr>
 * </table>

 * @author Thomas Smits
 * @version 1.0
 */
public abstract class MaintainBasketBaseAction extends MaintainGridBaseAction {

    /**
     * Session context constant for the type of the currently edited document
     */
    public static final String SC_DOCTYPE = "com.sap.isa.isacore.action.order.MaintainBasketAction.doctype";

    /**
     * Session context constant for storage of the list of available ship tos
     */
    public static final String SC_SHIPTOS = "com.sap.isa.isacore.action.order.MaintainBasketAction.shiptos";

    /**
     * Request context constant for storage of the items of the document
     */
    public static final String RK_ITEMS      = "items";

    /**
     * Request context constant for storage of the header of the document
     */
    public static final String RK_HEADER     = "hdr";

    /**
     * Request context constant for storage of messages attached to the document
     */
    public static final String RK_MESSAGES   = "messages";

    /**
     * Request context constant for storage of the whole document
     */
    public static final String RC_BASKET     = "basket";

    /**
     * Request context constant for storage of the available sold tos
     */
    public static final String RC_SOLDTOS    = "soldtos";

    /**
     * Request context constant for storage of the available shipping conditions
     */
    public static final String RC_SHIPCOND   = "shipcond";

    /**
     * Request context constant to indicate an exit from the configuration
     */
    public static final String RC_EXITCONFIG = "exitconfig";
    
    /**
     * Request context constant for storage of the batch elements
     */
    public static final String RC_BATCHS     = "batchs";
    
    /**
     * Request context constant for storage of a single item
     */
    public static final String RC_SINGLE_ITEM     = "item";

    /**
     * Document type constant to indicate a quotation
     */
    public static final String DOCTYPE_QUOTATION        = "quotation";

    /**
     * Document type constant to indicate a order template
     */
    public static final String DOCTYPE_ORDERTEMPLATE    = "ordertemplate";

    /**
     * Document type constant to indicate a basket
     */
    public static final String DOCTYPE_BASKET           = "basket";

    /**
     * Constant for available forwards, value is &quot;showbasketframe&quot;.
     */
    protected static final String FORWARD_SHOWBASKET         = "showbasketframe";

    /**
     * Constant for available forwards, value is &quot;summarybasket&quot;.
     */
    protected static final String FORWARD_SUMMARY_BASKET     = "summarybasket";

    /**
     * Constant for available forwards, value is &quot;ordersimulation&quot;.
     */
    protected static final String FORWARD_ORDERSIMULATION = "ordersimulation";

    /**
     * Constant for available forwards, value is &quot;summaryorder&quot;.
     */
    protected static final String FORWARD_SUMMARY_ORDER = "summaryorder";

    /**
     * Constant for available forwards, value is &quot;summaryquotation&quot;.
     */
    protected static final String FORWARD_SUMMARY_QUOTATION = "summaryquotation";
    
    /**
     * Constant for available forwards, value is &quot;summarytemplate&quot;.
     */
    protected static final String FORWARD_SUMMARY_TEMPLATE = "summarytemplate";

    /**
     * Constant for available forwards, value is &quot;updatedocumentview&quot;.
     */
    protected static final String FORWARD_UPDATEDOCUMENT     = "updatedocumentview";

    /**
     * Constant for available forwards, value is &quot;newshipto&quot;.
     */
    protected static final String FORWARD_NEW_SHIPTO         = "newshipto";

    /**
     * Constant for available forwards, value is &quot;ocisendbasket&quot;.
     */
    protected static final String FORWARD_OCI_SEND_BASKET    = "ocisendbasket";

    /**
     * Constant for available forwards, value is &quot;ocishowbasketframe&quot;.
     */
    protected static final String FORWARD_OCI_SUMMARY_BASKET = "ocishowbasketframe";

    /**
     * Constant for available forwards, value is &quot;closeconfig&quot;.
     */
    protected static final String FORWARD_CLOSE_CONFIG       = "closeconfig";

    /**
     * Constant for available forwards, value is &quot;eauctionconfirm&quot;.
     */
    protected static final String FORWARD_EAUCTION           = "eauctionconfirm";

    /**
     * Constant for available forwards, value is &quot;simulatebasket&quot;.
     */
    protected static final String FORWARD_SIMULATE_BASKET    = "simulatebasket";

    /**
     * Constant for available forwards, value is &quot;configitem&quot;.
     */
    protected static final String FORWARD_CONFIG_ITEM         = "configitem";
    
    /**
     * Constant for available forwards, value is &quot;showbatchframe&quot;.
     */
    protected static final String FORWARD_SHOWBATCH         = "showitembatch";
    
    /**
     * Constant for available forwards, value is &quot;proddet&quot;.
     */
    protected static final String FORWARD_PRODDET         = "proddet";
	
    /**
     * Parser to parse the request
     */
    protected static final SalesDocumentParser STANDARD_PARSER = (SalesDocumentParser) GenericFactory.getInstance("salesDocumentStandardParser");

    /**
     * Helper method to parse the request and construct items
     * for the <code>SalesDocument</code> form the
     * request's data.
     * <p>
     * The following fields are taken from the request and used to
     * construct the item:
     * </p>
     * <table border="1" cellspacing="0" cellpadding="2">
     *   <tr>
     *     <td><b>field</b></td>
     *     <td><b>use</b></td>
     *   </tr>
     *   <tr>
     *     <td>itemTechKey[]</td>
     *     <td>technical key of the item, only relevant for items already added to the document</td>
     *   </tr>
     *   <tr>
     *     <td>product[]</td>
     *     <td>product number</td>
     *   </tr>
     *   <tr>
     *     <td>partnerproduct[]</td>
     *     <td>customer's own product number</td>
     *   </tr>
     *   <tr>
     *     <td>quantity[]</td>
     *     <td>quantity of the product</td>
     *   </tr>
     *   <tr>
     *     <td>oldquantity[]</td>
     *     <td>old quantity of the product</td>
     *   </tr>
     *   <tr>
     *     <td>units[]</td>
     *     <td>the unit of the product</td>
     *   </tr>
     *   <tr>
     *     <td>pcat[]</td>
     *     <td>catalog the item was taken from</td>
     *   </tr>
     *   <tr>
     *     <td>pcatVariant[]</td>
     *     <td>catalog variant the item was taken from</td>
     *   </tr>
     *   <tr>
     *     <td>pcatArea[]</td>
     *     <td>catalog area the item was taken from</td>
     *   </tr>
     *   <tr>
     *     <td>reqdeliverydate[]</td>
     *     <td>requested delivery date for the item</td>
     *   </tr>
     *   <tr>
     *     <td>customer[]</td>
     *     <td>customer to deliver this special item to</td>
     *   </tr>
     *   <tr>
     *     <td>comment[]</td>
     *     <td>addtitional comments for the item entered by the customer</td>
     *   </tr>
     *   <tr>
     *     <td>batchID[]</td>
     *     <td>the ID of a batch element</td>
     *   </tr>
     *   <tr>
     *     <td>batchDedicated[]</td>
     *     <td>true if batches are maintained for the material</td>
     *   </tr>
     *   <tr>
     *     <td>batchAssigned[]</td>
     *     <td>true if characteristics are avaible to set for a batch element</td>
     *   </tr>
     *   <tr>
     *     <td>elementCount[]</td>
     *     <td>the amount of batch selections that exists</td>
     *   </tr>
     *   <tr>
     *     <td>elementName[]</td>
     *     <td>name of the chosen element</td>
     *   </tr>
     *   <tr>
     *     <td>elementTxt[]</td>
     *     <td>language dependend name of the element</td>
     *   </tr>
     *   <tr>
     *     <td>elementCountBs[]</td>
     *     <td>count checkboxes per element</td>
     *   </tr>
     *   <tr>
     *     <td>elementCharValue[]</td>
     *     <td>value of checkboxes from the element</td>
     *   </tr>
     *   <tr>
     *     <td>elementCharValTxt[]</td>
     *     <td>language dependend value of checkboxes from the element</td>
     *   </tr>
     *   <tr>
     *     <td>elementCharValMax[]</td>
     *     <td>language depended unit of the batch element</td>
     *   </tr>
     * </table>
     * <br><br>
     * @param parser The request, wrapped into a parser object
     * @param userSessionData   object wrapping the session
     * @param preOrderSalesDocument the document to store the data in
     * @param headerReqDeliveryDate the date set in the header for the
     *        requested delivery date. This parameter is used to provide
     *        the functionality to provide a preset date for all items
     * @param log reference to the logging context
     * @param isDebugEnabled flag indicating wheter or not logging output
     *        is written
     */
    protected void parseRequestItems(
            RequestParser parser,
            UserSessionData userSessionData,
            SalesDocument preOrderSalesDocument,
            String headerReqDeliveryDate,
            IsaLocation log,
            boolean isDebugEnabled)
                        throws CommunicationException {

        ItemList items = preOrderSalesDocument.getItems();
        
        STANDARD_PARSER.parseItems(parser, userSessionData, items, preOrderSalesDocument,
                                   false, log, isDebugEnabled);
		
        for (int i = 0; i < items.size(); i++) {
            customerExitParseRequestItem(parser, userSessionData, items.get(i));
            customerExitParseRequestItem(parser, userSessionData, items.get(i), i + 1);
        }
    }

    /**
     * Convenience method for developers who want to call
     * <code>parseRequestItems</code> and <code>parseRequestHeader</code> one
     * after the other. This method calls the other two and gains some extra
     * data to keep the call simple.
     *
     * @param parser The request, wrapped into a parser object
     * @param userSessionData   object wrapping the session
     * @param preOrderSalesDocument the document to store the data in
     * @param log reference to the logging context
     */
    protected void parseRequest(
            RequestParser parser,
            UserSessionData userSessionData,
            SalesDocument preOrderSalesDocument,
            IsaLocation log) throws CommunicationException {

        boolean isDebugEnabled = log.isDebugEnabled();

        preOrderSalesDocument.clearMessages();

        parseRequestHeader(parser, userSessionData, preOrderSalesDocument, log,
                           isDebugEnabled);

        String headerReqDeliveryDate = preOrderSalesDocument.getHeader().getReqDeliveryDate();

        parseRequestItems(parser, userSessionData, preOrderSalesDocument,
                          headerReqDeliveryDate, log,
                          isDebugEnabled);
    }

    /**
     * Retrieves the description from the resource files for messages containing
     * only a resource key
     *
     * @param session the actual HTTP session
     * @param msglist list of messages to process
     * @param log reference to the logging context
     * @param isDebugEnabled flag indicating wheter or not logging output
     *        is written
     */
    protected void translateMessageList(
            HttpSession session,
            MessageList msgList,
            IsaLocation log,
            boolean isDebugEnabled)
                        throws CommunicationException {

          Iterator it;

           if (msgList != null) {
               it = msgList.iterator();

               while (it.hasNext()) {

                Message message = (Message) it.next();

                String description = message.getDescription();
                String ressourceKey    = message.getResourceKey();

                    if ((description == null || description.length() == 0)
                         && ressourceKey != null) {
                        // Messagedescription is empty but message contains resource key
                        // retrieve the description
                        message.setDescription(WebUtil.translate(getServlet().getServletContext(),
                                           session,
                                           ressourceKey,
                                           message.getResourceArgs()));
                    }
                }
           }
    }


    /**
     * completes the message description for the sales document
     * only a resource key
     *
     * @param session the actual HTTP session
     * @param preOrderSalesDocument the document to store the data in
     * @param log reference to the logging context
     * @param isDebugEnabled flag indicating wheter or not logging output
     *        is written
     */
    protected void resolveMessageKeys(
            HttpSession session,
            SalesDocument preOrderSalesDocument,
            IsaLocation log,
            boolean isDebugEnabled)
                        throws CommunicationException {

          if (preOrderSalesDocument == null) {
             return;
          }

          translateMessageList(session,
                               preOrderSalesDocument.getMessageList(),
                               log,
                               isDebugEnabled);
          if (preOrderSalesDocument.getHeader() != null) {
              translateMessageList(session,
                               preOrderSalesDocument.getHeader().getMessageList(),
                               log,
                               isDebugEnabled);
          }

          ItemList items = preOrderSalesDocument.getItems();
          if (items != null) {
              for (int i = 0; i < items.size(); i++) {
                        translateMessageList(session,
                                   items.get(i).getMessageList(),
                                   log,
                                   isDebugEnabled);
              }
          }
    }


    /**
     * Helper method to parse the request and construct the header of
     * a <code>SalesDocument</code> form the
     * request's data.
     * <p>
     * The following fields are taken from the request and used to
     * construct the header:
     * </p>
     * <table border="1" cellspacing="0" cellpadding="2">
     *   <tr>
     *     <td><b>field</b></td>
     *     <td><b>use</b></td>
     *   </tr>
     *   <tr>
     *     <td>poNumber</td>
     *     <td>purchase order number the customer entered</td>
     *   </tr>
     *   <tr>
     *     <td>headerDescription</td>
     *     <td>description the customer entered for the order</td>
     *   </tr>
     *   <tr>
     *     <td>headerReqDeliveryDate</td>
     *     <td>requested delivery date for the order; data is used to set the date on the item level</td>
     *   </tr>
     *   <tr>
     *     <td>headerShipTo</td>
     *     <td>ship to selected on the header level</td>
     *   </tr>
     *   <tr>
     *     <td>shipCond</td>
     *     <td>shipping conditions for the document</td>
     *   </tr>
     * </table>
     * <br><br>
     *
     * @param parser The request, wrapped into a parser object
     * @param userSessionData   object wrapping the session
     * @param preOrderSalesDocument the document to store the data in
     * @param log reference to the logging context
     * @param isDebugEnabled flag indicating wheter or not logging output
     *        is written
     */
    protected void parseRequestHeader(
            RequestParser parser,
            UserSessionData userSessionData,
            SalesDocument preOrderSalesDocument,
            IsaLocation log,
            boolean isDebugEnabled)
                        throws CommunicationException {

        STANDARD_PARSER.parseHeader(parser, userSessionData, preOrderSalesDocument.getHeader(), 
                                    preOrderSalesDocument, false, log, isDebugEnabled);
		
        customerExitParseRequestHeader(parser, userSessionData, preOrderSalesDocument);
    }

    /**
     * Convenience method to update the salesdocument in the backend before
     * any further processing is done.
     *
     * @param parser The request, wrapped into a parser object
     * @param userSessionData   object wrapping the session
     * @param preOrderSalesDocument the document to store the data in
     * @param shop              shop business object
     * @param bupama            business partner manager object
     * @param log reference to the logging context
     *
     * @deprecated This method will be replaced by
     * <code>updateSalesDocument(RequestParser parser,
     *                            UserSessionData userSessionData,
     *                            SalesDocument preOrderSalesDocument,
     *                            User user,
     *                            Shop shop,
     *                            BusinessPartnerManager bupama,
     *                            IsaLocation log) </code>
     * @see #updateSalesDocument(RequestParser parser,
     *                            UserSessionData userSessionData,
     *                            SalesDocument preOrderSalesDocument,
     *                            User user,
     *                            Shop shop,
     *                            BusinessPartnerManager bupama,
     *                            IsaLocation log)
     */
    protected void updateSalesDocument(
            RequestParser parser,
            UserSessionData userSessionData,
            SalesDocument preOrderSalesDocument,
            Shop shop,
            IsaLocation log) throws CommunicationException {

        BusinessPartnerManager bupama = null;
        updateSalesDocument(parser, userSessionData, preOrderSalesDocument,
                            shop, bupama, log);
    }

    /**
     * Convenience method to update the salesdocument in the backend before
     * any further processing is done.
     *
     * @param parser The request, wrapped into a parser object
     * @param userSessionData   object wrapping the session
     * @param preOrderSalesDocument the document to store the data in
     * @param shop              shop business object
     * @param bupama            business partner manager object
     * @param log reference to the logging context
     */
    protected void updateSalesDocument(
            RequestParser parser,
            UserSessionData userSessionData,
            SalesDocument preOrderSalesDocument,
            Shop shop,
            BusinessPartnerManager bupama,
            IsaLocation log) throws CommunicationException {

            RequestParser.Parameter delete = parser.getParameter("delete[]");
            int numLines = parser.getParameter("product[]").getNumValues();

            // possible customer exit, to parse request
            customerExitParseRequest(parser, userSessionData, preOrderSalesDocument);

            // dont' save new items, with quantity 0
            ItemList items = preOrderSalesDocument.getItems();

            String quantity = null;
            TechKey techKey = null;
            TechKey parentId = null;

            int newItemsToDelete[] = new int[items.size()];

            HashSet deleteItems = new HashSet();

            // check for main positions where the quantity is 0 and delete them
            // the subpositions should be deleted automatically
            for (int i = 0; i < items.size(); i++) {
                quantity = items.get(i).getQuantity();
                techKey = items.get(i).getTechKey();
                parentId = items.get(i).getParentId();

                if ((techKey == null || techKey.getIdAsString().length() == 0) &&
                    quantity != null && quantity.trim().equals("0")) {
                    // new item with quantity 0, don't save it
                    newItemsToDelete[i] = 1;
                }
                else if (techKey != null && techKey.getIdAsString().length() > 0 &&
                    quantity != null && quantity.trim().equals("0") &&
                    (parentId == null || parentId.getIdAsString().length() == 0)) {
                    deleteItems.add(techKey.getIdAsString());
                    if (log.isDebugEnabled()) {
                       log.debug("Main Item " + items.get(i).getProduct() + " will be deleted, because quantity is 0");
                    }
                }
            }
			
            Iterator itemsIter = items.iterator();
            int k= 0;

            // because there is not method to remove an item directly from
            // the itemlist we have to use the iterator
            while (itemsIter.hasNext()) {
                itemsIter.next();
                if (newItemsToDelete[k] == 1) {
                    if (log.isDebugEnabled()) {
                       log.debug("Deleting new Item " + items.get(k).getProduct() + " before update, because quantity is 0");
                    }
                    itemsIter.remove();
                }
                k++;
            }
            
            for (int i = 1; i <= numLines; i++) {
                if (delete.getValue(i).isSet()) {
                    deleteItems.add(delete.getValue(i).getString());
                }
            }

            TechKey[] deleteKeys = new TechKey[deleteItems.size()];
            Iterator iter = deleteItems.iterator();
            int idx = 0;
            while (iter.hasNext()) {
                deleteKeys[idx++] = new TechKey((String) iter.next());
            }

            if (deleteKeys.length > 0) {
               preOrderSalesDocument.deleteItems(deleteKeys);
            }
            
            //  Note 1040652 Update Basket after deletion... 
            preOrderSalesDocument.update(bupama, shop);   // may throw CommunicationException
            preOrderSalesDocument.read();
    }

    /**
     * This method checks if the soldto field was changed on the jsp and
     * performs everything that is necessary if that case.
     *
     * @param parser The request, wrapped into a parser object
     */
    protected void processCustomerNumberFields(RequestParser parser,
                                               SalesDocument salesDocument,
                                               Shop shop, 
                                               BusinessPartnerManager buPaMa)
                                               throws CommunicationException {

        // are we in a scenario where the soldTo might be changed ?
        //if (shop.getCompanyPartnerFunction().equals(PartnerFunctionBase.RESELLER)) {
        if (shop.isSoldtoSelectable()){

            // has the SoldTo changed ?
            String newCustomerNumber = STANDARD_PARSER.checkSoldToChange(parser);

            if (newCustomerNumber != null && newCustomerNumber.length() > 0) {
  
				BusinessPartner bupa = buPaMa.getBusinessPartner(newCustomerNumber);
                
                boolean bupValid = bupa != null &&  bupa.getTechKey() != null && !"".equals(bupa.getTechKey().getIdAsString());
				
				PartnerListEntry soldToEntry = 
					new PartnerListEntry(bupa!=null ? bupa.getTechKey():null, newCustomerNumber);
                    
				salesDocument.getHeader().getPartnerList().setContact(soldToEntry);
                  
                // as soon as the partner list ist completly supported by the backends,
                // the following actions should be done by the backend
                salesDocument.deleteShipTosInBackend();
                salesDocument.clearShipTos();
                
                if (bupValid) {
                    salesDocument.readShipTos();
                
                    if (shop.getCompanyPartnerFunction().equals(PartnerFunctionBase.RESELLER)){
                        addResellerAddressToShipTos(salesDocument, shop, soldToEntry, buPaMa);
                    }
                }

                if (salesDocument.getNumShipTos() > 0) {
                    salesDocument.getHeader().setShipTo(salesDocument.getShipTo(0));
                }
                else {
                    salesDocument.getHeader().setShipTo(new ShipTo());
                }

                //all shipToLineKeys have to be reset to the default shipTo of the new SoldTo
                ItemList itemList = salesDocument.getItems();
                for (int i = 0; i < itemList.size(); i++) {
                    itemList.get(i).setShipTo(salesDocument.getHeader().getShipTo());
                }

                salesDocument.update(buPaMa);           					            
            }
            else {
                if (salesDocument.getHeader().getPartnerList().getContact() != null) {
                    salesDocument.getHeader().getPartnerList().removePartner(PartnerFunctionData.CONTACT);
                }          
            }

            // finally check if there is an invalid relationship
            // This check has to be done every time, not only when the
            // soldTo was changed
            checkSoldToResellerRelation (salesDocument, shop, buPaMa, false);
        }
    }

    /**
     * This method checks if the there is a valid realtionship between the soldto
     * and the reseller of a sales document. If not, either a warning is shown, or
     * the relationship is created, depending on the value of the flag createMissingRelation.
     *
     * If ACE is active for the user, there is an additional step before the relationship is checked,
     * namely the check wheter the Business Partner is visible according to ACE at all. This is a workaround
     * until the ACE checks can be performed at a more central place.
     *  
     * @param salesDocument         The sales documnet that is checked
     * @param shop                  The shop
     * @param bupama                The businesspartner manager
     * @param createMissingRelation Flag, that indicates, what should happen, if
     *                              there is nor valid relation.
     *                              False: dsiplay a message
     *                              True: create the relation
     */
    public static void checkSoldToResellerRelation (SalesDocument salesDocument,
                                                Shop shop,
                                                BusinessPartnerManager bupama,
                                                boolean createMissingRelation) {

        // only check, if the soldTo is selectable
        if (shop.isSoldtoSelectable()) {

            PartnerListEntry soldToEntry = salesDocument.getHeader().getPartnerList().getSoldTo();
            
//            // Begin ACE check 
//		    // To be removed when ACE check is at a more central place in bupamanger
//                        
//             if (soldToEntry != null && soldToEntry.getPartnerTechKey() != null &&
//			    soldToEntry.getPartnerTechKey().getIdAsString().length() > 0) {
//               
//             BusinessPartner partnerCheck = bupama.getBusinessPartner(soldToEntry.getPartnerTechKey());
//               
//			 if (partnerCheck.checkACE()) {   
//             // End ACE check   
			
            
            	//here we name the partner 'reseller' though it might be a reseller or an agent.
            	//consider the shop setting company partner function
            
                salesDocument.getMessageList().remove(new String[] {"b2b.order.display.norel.reseller", "b2b.order.display.norel.r3"});
            	PartnerListEntry resellerEntry = salesDocument.getHeader().getPartnerList().getPartner(shop.getCompanyPartnerFunction());
                
            	if (soldToEntry != null && resellerEntry != null &&
                    soldToEntry.getPartnerTechKey() != null &&
                    soldToEntry.getPartnerTechKey().getIdAsString().length() > 0 &&
                    resellerEntry.getPartnerTechKey() != null &&
                    resellerEntry.getPartnerTechKey().getIdAsString().length() > 0) {
                        
                	BusinessPartner soldToBuPa = bupama.getBusinessPartner(soldToEntry.getPartnerTechKey());
                	BusinessPartner resellerBuPa = bupama.getBusinessPartner(resellerEntry.getPartnerTechKey());

                	if (!soldToBuPa.checkRelationExists(shop.getCompanyPartnerFunction(), resellerBuPa)) {
                	// no valid relation
                        
                    	if (createMissingRelation) {
                        // create the relation
                        	String ret = soldToBuPa.createRelation(resellerBuPa, shop.getCompanyPartnerFunction());
                        	if (!ret.equalsIgnoreCase("0")) {
                            	Iterator msgIter = resellerBuPa.getMessageList().iterator();
                            	while (msgIter.hasNext()) {
                                	salesDocument.addMessage((Message) msgIter.next());
                            	}
                        	}
                    	}
                    	else {
                        	// show message. Distinguish between CRM and R/3 backend
                        	Message msg = null;
                        
                        	if (shop.getBackend().equals(ShopData.BACKEND_CRM))
                        		msg = new Message(Message.INFO, "b2b.order.display.norel.reseller");
                        	else 	
                        		msg = new Message(Message.ERROR, "b2b.order.display.norel.r3");
                        	
                            salesDocument.addMessage(msg);
                    	}
                	}
            	}
        
//        	// Begin ACE check 					 
//        	} else {   
//            
//             // remove TechKey of partner from sales partner list!			 
//			 salesDocument.getHeader().getPartnerList().getSoldTo().setPartnerTechKey(null);
//			 Message msg = null;
//			 String[] args = { partnerCheck.getId() };			 
//			 msg = new Message(Message.ERROR, "b2b.order.display.nocust", args, "");			 			              
//             salesDocument.addMessage(msg);
//                
//			} 
//        
//		   }        
//		// End ACE check
        
       }
    }


    /**
     * This method adds the address of the reseller to the shiptos of a sales Document
     * if we are in the b2r scenario.
     */
    protected void addResellerAddressToShipTos(SalesDocument salesDocument,
                                               Shop shop,
											   PartnerListEntry soldto,
                                               BusinessPartnerManager bupama) throws CommunicationException {

        PartnerListEntry resellerEntry = salesDocument.getHeader().getPartnerList().getPartner(PartnerFunctionData.RESELLER);

        if (resellerEntry != null &&
            resellerEntry.getPartnerTechKey() != null &&
            resellerEntry.getPartnerTechKey().getIdAsString().length() > 0) {
            BusinessPartner reseller = bupama.getBusinessPartner(resellerEntry.getPartnerTechKey());
            if (reseller.getAddress() != null) {
				Address address = reseller.getAddress();
				ShipTo shipTo = new ShipTo();
        
				shipTo.setAddress(address);
				shipTo.setId(reseller.getId());
				shipTo.setManualAddress();

				salesDocument.addNewShipTo(shipTo);
            }
        }
    }

    /**
     * Template method to be overwritten by the subclasses of this class.
     * Normally you should overwrite this method and you do not touch the
     * <code>isaPreform</code> method.
     *
     * @param request           request context
     * @param response          response context
     * @param session           unwrapped session context
     * @param userSessionData   object wrapping the session
     * @param requestParser     parser to simple retrieve data from the request
     * @param bom               reference to the BusinessObjectManager
     * @param log               reference to the IsaLocation, needed for logging
     * @param startupParameter  object containing the startup parameters
     * @param eventHandler      object to capture events with
     * @param multipleInvocation flag indicating, that a multiple invocation occured
     * @param browserBack       flag indicating a browser back
     * @param shop              shop business object
     * @param preOrderSalesDocument the current sales document edited
     * @param targetDocument    the target document
     * @param documentHan       document handler
     *
     * @return logical key for a forward to another action or jsp
     */
    protected abstract String basketPerform(HttpServletRequest request,
                HttpServletResponse response,
                HttpSession session,
                UserSessionData userSessionData,
                RequestParser parser,
                BusinessObjectManager bom,
                IsaLocation log,
                IsaCoreInitAction.StartupParameter startupParameter,
                BusinessEventHandler eventHandler,
                boolean multipleInvocation,
                boolean browserBack,
                Shop shop,
                SalesDocument preOrderSalesDocument,
                boolean auction,
                DocumentState targetDocument,
                DocumentHandler documentHandler)
                    throws CommunicationException;


    /**
     * Overriden <em>isaPerform</em> method of <em>IsaCoreBaseAction</em>.
     */
    public ActionForward isaPerform(ActionMapping mapping,
                ActionForm form,
                HttpServletRequest request,
                HttpServletResponse response,
                UserSessionData userSessionData,
                RequestParser parser,
                BusinessObjectManager bom,
                IsaLocation log,
                IsaCoreInitAction.StartupParameter startupParameter,
                BusinessEventHandler eventHandler,
                boolean multipleInvocation,
                boolean browserBack)
                        throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        HttpSession session = request.getSession();

        boolean isDebugEnabled = log.isDebugEnabled();


        // get pre-order document on top from document handler
        DocumentHandler documentHandler = (DocumentHandler)
            userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);

        if (documentHandler == null) {
			log.exiting();
            throw new PanicException("No document handler found.");
        }

        DocumentState targetDocument =
            documentHandler.getDocument(ManagedDocument.TARGET_DOCUMENT);

        // get the current pre order sales document - basket is assumed as
        // default
        SalesDocument preOrderSalesDocument = null;

        if (targetDocument instanceof Quotation) {
            preOrderSalesDocument = bom.getQuotation();
            userSessionData.setAttribute(SC_DOCTYPE, DOCTYPE_QUOTATION);
        }
        else if (targetDocument instanceof OrderTemplate) {
            preOrderSalesDocument = bom.getOrderTemplate();
            userSessionData.setAttribute(SC_DOCTYPE, DOCTYPE_ORDERTEMPLATE);
        }
        else {
            preOrderSalesDocument = bom.getBasket();
            userSessionData.setAttribute(SC_DOCTYPE, DOCTYPE_BASKET);
        }

        if (preOrderSalesDocument == null) {
			log.exiting();
            return mapping.findForward(FORWARD_UPDATEDOCUMENT);
        }

        Shop shop = bom.getShop();

        request.setAttribute(RC_BASKET, preOrderSalesDocument);
        preOrderSalesDocument.clearMessages();

        // Check if we are in the action or the normal internet sales
        // environment
        boolean auction =
            ((userSessionData.getAttribute("EAUCTION_PARAMID") != null) ||
            (session.getAttribute("EAUCTION_PARAMID") != null) ||
            (request.getAttribute("EAUCTION_PARAMID") != null));


        String forwardTo = basketPerform(request, response, session, userSessionData,
                             parser, bom, log, startupParameter, eventHandler,
                             multipleInvocation, browserBack, shop,
                             preOrderSalesDocument, auction, targetDocument,
                             documentHandler);

        resolveMessageKeys(session, preOrderSalesDocument, log, isDebugEnabled);
		log.exiting();
        return mapping.findForward(forwardTo);
    }


     /**
     * Template method to be overwritten by the subclasses of this class.
     * This method provides the possibilty, to easily extend the parsing
     * process for a sales document, accordingly to special
     * customer requirements. By default this method is empty.
     * <code>customerExitParseRequestHeader</code> method.
     *
     * @param requestParser     parser to simple retrieve data from the request
     * @param userSessionData   object wrapping the session
     * @param salesDocument     the current sales document edited
     *
     */
     public void customerExitParseRequest(
                RequestParser parser,
                UserSessionData userSessionData,
                SalesDocument salesDocument) {
     }

    /**
     * Template method to be overwritten by the subclasses of this class.
     * This method provides the possibilty, to easily extend the parsing
     * process for the header of a sales document, accordingly to special
     * customer requirements. By default this method is empty.
     * <code>customerExitParseRequestHeader</code> method.
     *
     * @param requestParser     parser to simple retrieve data from the request
     * @param userSessionData   object wrapping the session
     * @param salesDocument     the current sales document edited
     * 
     * @deprecated This method will be replaced by <code>customerExitParseRequestHeader(
     *                                                           RequestParser parser,
     *                                                           UserSessionData userSessionData,
     *                                                           HeaderSalesDocument header,
     *                                                           SalesDocument salesDoc)</code>
     *             of the class <code>SaleDocumentParser</code> or one of its subclasses
     * @see #SaleDocumentParser.customerExitParseRequestHeader(
     *                                  RequestParser parser,
     *                                  UserSessionData userSessionData,
     *                                  HeaderSalesDocument header,
     *                                  SalesDocument salesDoc)
     */
     public void customerExitParseRequestHeader(
                RequestParser parser,
                UserSessionData userSessionData,
                SalesDocument salesDocument) {
     }

     /**
     * Template method to be overwritten by the subclasses of this class.
     * This method provides the possibilty, to easily extend the parsing
     * process for the items of a sales document, accordingly to special
     * customer requirements. By default this method is empty.
     * <code>customerExitParseRequestHeader</code> method.
     *
     * @param requestParser     parser to simple retrieve data from the request
     * @param userSessionData   object wrapping the session
     * @param itemSalesDoc      the current item edited
     * 
     * @deprecated This method will be replaced by 
     *    <code>customerExitParseRequestItem(
     *          RequestParser    parser,
     *          UserSessionData  userSessionData, 
     *          ItemSalesDoc     itemSalesDoc,
     *          int              itemIdx)</code>
     * @see #customerExitParseRequestItem(          
     *       RequestParser       parser,
     *          UserSessionData  userSessionData, 
     *          ItemSalesDoc     itemSalesDoc,
     *          int              itemIdx)
     */
     public void customerExitParseRequestItem(
                RequestParser parser,
                UserSessionData userSessionData,
                ItemSalesDoc itemSalesDoc) {
     }
     
    /**
     * Template method to be overwritten by the subclasses of this class.
     * This method provides the possibilty, to easily extend the parsing
     * process for the items of a sales document, accordingly to special
     * customer requirements. By default this method is empty.
     * <code>customerExitParseRequestHeader</code> method.
     *
     * @param requestParser     parser to simple retrieve data from the request
     * @param userSessionData   object wrapping the session
     * @param itemSalesDoc      the current item edited
     * @param itemIdx		    the index of item related fields in the request 
     *
     * @deprecated This method will be replaced by <code>customerExitParseRequestItem(
     *                                                   RequestParser parser,
     *                                                   UserSessionData userSessionData,
     *                                                   ItemSalesDoc itemSalesDoc,
     *                                                   int itemIdx,
     *                                                   SalesDocument salesDoc)</code>
     *             of the class <code>SaleDocumentParser</code> or one of its subclasses
     * @see #SaleDocumentParser.customerExitParseRequestItem(
     *                                                   RequestParser parser,
     *                                                   UserSessionData userSessionData,
     *                                                   ItemSalesDoc itemSalesDoc,
     *                                                   int itemIdx,
     *                                                   SalesDocument salesDoc)
     */
     public void customerExitParseRequestItem(
                RequestParser parser,
                UserSessionData userSessionData,
                ItemSalesDoc itemSalesDoc,
                int itemIdx) {
     }

     /**
     * Template method to be overwritten by the subclasses of this class.
     * This method provides the possibilty, to easily change the forward
     * of an action, accordingly to customer requirements. if this method
     * returns nothing, the default forward is used
     * By default this method returns null.
     * <code>customerExitDispatcher</code> method.
     *
     * @param requestParser     parser to simple retrieve data from the request
     * @param userSessionData   object wrapping the session
     * @param itemSalesDoc      the current item edited
     */
     public String customerExitDispatcher(RequestParser parser) {
                return null;
     }


	/**
	 * Convenience method for developers who want to call
	 * <code>parseRequest</code>.
	 * This method checks if the campaign Id has changed and
	 * if so the Id is set in the catalog.
     * 
     * @param parser
     * @param userSessionData
     * @param preOrderSalesDocument
     * @param shop
     * @param log
     * @throws CommunicationException
     */
     protected void updateBasket(
					RequestParser parser,
					UserSessionData userSessionData,
					SalesDocument preOrderSalesDocument,
					Shop shop,
					IsaLocation log,
					HttpServletRequest request) throws CommunicationException {

        
        String oldCampaign = "";
		String newCampaign = "";
        
        // don't transfer the campaign to the catalog, if multiple camapigns are allowed
		// read the old campaign Id
		if (!shop.isMultipleCampaignsAllowed() && shop.isManualCampainEntryAllowed() && !shop.isSoldtoSelectable()) {
			if (preOrderSalesDocument.getHeader().getAssignedCampaigns().getCampaign(0) != null) {
				oldCampaign = preOrderSalesDocument.getHeader().getAssignedCampaigns().getCampaign(0).getCampaignId();				
			}
		}        
		
		// parse request
		parseRequest(parser, userSessionData, preOrderSalesDocument, log);
		
        // don't transfer the campaign to the catalog, if multiple camapigns are allowed
		// read the new campaign Id
		if (!shop.isMultipleCampaignsAllowed() && shop.isInternalCatalogAvailable() && shop.isManualCampainEntryAllowed() && !shop.isSoldtoSelectable()) {
			if (preOrderSalesDocument.getHeader().getAssignedCampaigns().getCampaign(0) != null) {
				newCampaign = preOrderSalesDocument.getHeader().getAssignedCampaigns().getCampaign(0).getCampaignId();				
			}
			
			// check if the Id has changed
			if (newCampaign != null && !newCampaign.trim().equals(oldCampaign.trim())) {
			
				// get the catalog
				WebCatInfo webCat =
						getCatalogBusinessObjectManager(userSessionData).getCatalog();
					
				if (shop.isInternalCatalogAvailable() && webCat == null) {
					throw( new IllegalArgumentException("Paramter webCat can not be null!"));
				}					
				// get the bom
				BusinessObjectManager bom = (BusinessObjectManager)
						userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);

				// we have to set the Id in the catalog
                GetCampaignAction.readCampaign(newCampaign, userSessionData, request, log);
				AddCampaignToCatalogueAction.addCampaignToCatalogue(webCat, userSessionData, bom, log);					
			}			
		}
	 }

}