/*****************************************************************************
    Class:        CreateSalesDocumentBase
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      05.03.2004
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.isacore.action.order;

import javax.servlet.http.HttpServletRequest;

import com.sap.isa.businessobject.BORuntimeException;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.order.CampaignListEntry;
import com.sap.isa.businessobject.order.PartnerList;
import com.sap.isa.businessobject.order.PartnerListEntry;
import com.sap.isa.businesspartner.action.GetPartnerHierarchyAction;
import com.sap.isa.businesspartner.backend.boi.BusinessPartnerData;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.ContextConst;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.MessageDisplayer;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 * The class CreateSalesDocumentBase offers some base functionality, which if useful
 * to create sales documents. . <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class CreateSalesDocumentBase extends IsaCoreBaseAction {
    /**
     * Defaul Constructor 
     */
    public CreateSalesDocumentBase() {
        super();
    }
    

	/**
	 * Check if the request parameter <code>soldToId</code> contains a valid id. <br>
	 * The method checks if the id exist and
	 * if the {@link Shop#isBpHierarchyEnable()} returns <code>true</code> also if the
	 * given id is in the business partner hierarchy.
	 * 
	 * @param bupama business partner manager 
	 * @param requestParser request parser
	 * @param userSessionData user session data
	 * @param shop current shop object
	 * @return <code>true</code> if the bp id is valid. 
	 * @throws CommunicationException
	 */
	static public boolean checkSoldToParameter(BusinessPartnerManager bupama, 
											 RequestParser requestParser,
											 UserSessionData userSessionData,
											 Shop shop) 
			throws CommunicationException {
											 	
		// check if the soldToGuid is given and valid
		String soldToId = requestParser.getParameter("soldToId").getValue().getString().trim();
		
		if (soldToId.length() > 0) {
			BusinessPartner buPa = bupama.getBusinessPartner(soldToId);
			if (buPa == null) {
				MessageDisplayer messageDisplayer = new MessageDisplayer();
				messageDisplayer.addToRequest(requestParser.getRequest());
				messageDisplayer.setBackAvailable(true);
				messageDisplayer.addMessage(new Message(Message.ERROR,
                                             "b2b.soldto.idNotValid",
                                              new String[]{soldToId}, 
                                              null));
				return false;												  
				
			}
			else if (shop.isBpHierarchyEnable()) {
				
				ResultData partners = GetPartnerHierarchyAction.getPartner(userSessionData, userSessionData.getMBOM());
				partners.beforeFirst();
				if (!partners.searchRowByColumn(BusinessPartnerData.SOLDTO,soldToId)) {
					MessageDisplayer messageDisplayer = new MessageDisplayer();
					messageDisplayer.addToRequest(requestParser.getRequest());
					messageDisplayer.setBackAvailable(true);
					messageDisplayer.addMessage(new Message(Message.ERROR,
												 "b2b.soldto.notinHierarchy",
												  new String[]{soldToId}, 
												  null));
					return false;												  
				}
			}
		}

		return true;
	}
	
    
	/**
	 * Create the Partnerlist with regards of the partner function of the company. <br>
	 * 
	 * @param bupama business partner manager
	 * @param compPartnerFunction partner function of the company
	 * @param requestParser parser to check if the sold guid is given with the 
	 *         parameter "soldtoguid". 
	 * @return
	 */
	protected PartnerList createPartnerList(BusinessPartnerManager bupama, 
											String  compPartnerFunction,
											RequestParser requestParser) {
        
		final String METHOD_NAME = "createPartnerList()";
		log.entering(METHOD_NAME);
		BusinessPartner buPa;
		PartnerList pList = new PartnerList();

		if (compPartnerFunction == null || compPartnerFunction.length() == 0) {
			compPartnerFunction = PartnerFunctionData.SOLDTO;
		}

		if (compPartnerFunction.equals(PartnerFunctionData.SOLDTO)) {

			// check if the soldToGuid is given and valid
			String soldToId = requestParser.getParameter("soldToId").getValue().getString().trim();
			
			if (soldToId.length() > 0) {
				buPa = bupama.getBusinessPartner(soldToId);
			}
			else {
				buPa = bupama.getDefaultBusinessPartner(compPartnerFunction);
			}
							
			if (buPa != null ) {
				pList.setPartner(compPartnerFunction, new PartnerListEntry(buPa.getTechKey(),buPa.getId()));
			} 
			else {
				log.debug("Given soldTo not valid!");
			}
		} 

		if (compPartnerFunction.equals(PartnerFunctionData.RESELLER)||
			compPartnerFunction.equals(PartnerFunctionData.AGENT)) {
			buPa = bupama.getDefaultBusinessPartner(compPartnerFunction);
			if (buPa != null ) {
				pList.setPartner(compPartnerFunction, new PartnerListEntry(buPa.getTechKey(),buPa.getId()));
			}
		}

		// set contact info, but only, if we are not in the reseller or agent scenario
		buPa = bupama.getDefaultBusinessPartner(PartnerFunctionData.CONTACT);
		if ((!compPartnerFunction.equals(PartnerFunctionData.RESELLER)) && (!compPartnerFunction.equals(PartnerFunctionData.AGENT)) && buPa != null ) {
			pList.setContact(new PartnerListEntry(buPa.getTechKey(),buPa.getId()));
		}

        
		// add responsible at partner in reseller scenario
		if (compPartnerFunction.equals(PartnerFunctionData.RESELLER)) {        			
			BusinessPartner bupa1 = bupama.getDefaultBusinessPartner(PartnerFunctionData.RESP_AT_PARTNER);			
			pList.setPartner(PartnerFunctionData.RESP_AT_PARTNER, new PartnerListEntry(bupa1.getTechKey(),bupa1.getId()));                 
		}
		log.exiting();
		return pList;
	}
	
	
	/**
	 * Set the campaign Id in the campaignEntryList if manually entry 
	 * is allowed, not in B2R szenario and a campaign Id exists in the catalog.
	 * 
     * @param salesDocWebCat
     * @param header
     * @param campaignListEntry
     * @param shop
     * 
     * @return CampaignListEntry campaignListEntry
     */
    protected CampaignListEntry setCampaign(WebCatInfo salesDocWebCat,
											HeaderSalesDocument header,
											CampaignListEntry campaignListEntry,
											Shop shop) {
		
		final String METHOD_NAME = "setCampaign()";
		log.entering(METHOD_NAME);
		// is manually campaign entry allowed
		if (shop.isManualCampainEntryAllowed() && !shop.isSoldtoSelectable()) {	
												
			if (shop.isInternalCatalogAvailable() && salesDocWebCat == null) {
				throw( new IllegalArgumentException("Paramter salesDocWebCat can not be null!"));
			}			
		
			// check the catalog if a campaign Id exists
			if (salesDocWebCat != null && 
			    (salesDocWebCat.getCampaignId() != null && salesDocWebCat.getCampaignId().length() > 0)) { 
			
				// we use the campaign Id of the catalog
				campaignListEntry = null;
						
				// set the campaign Id
				campaignListEntry = 
				header.getAssignedCampaigns().createCampaign(salesDocWebCat.getCampaignId(),
															 TechKey.EMPTY_KEY,
															 "",
															 false,
															 true);									
			}
		}
		log.exiting();
		return campaignListEntry;
	}

    /**
     * Sets the error messages from the sales document into the request. Be aware that the forward to the message page
     * must be set separately!
     * @param request             HTTP request
     * @param documentHandler     the document handler
     * @param nextAction          next Struts action which should be called after message page
     * @param nextActionParameter Parameters for the next Struts action
     * @param salesDocument       the involved sales document
     * @return booelan            Returns true if just an exception is available
     */
    static protected boolean createErrorMessage(
        HttpServletRequest request,
        DocumentHandler documentHandler,
        String nextAction,
        String nextActionParameter,
        SalesDocument salesDocument,
        BORuntimeException BORex) {
  
        boolean retVal = false;
        
        // create a Message Displayer for the error page!
        MessageDisplayer messageDisplayer = new MessageDisplayer();
        
        messageDisplayer.addToRequest(request);
        if (salesDocument.hasMessages() == true) {
            messageDisplayer.copyMessages(salesDocument);
        } else {
            // Only exception available add it, and set retVal to true. This
            // will allow to navigate to an error page instead to the message page
            // where the exception itself is not visible!
            request.setAttribute(ContextConst.EXCEPTION, BORex);
            retVal = true;
        }
        
        // the action which is to call after the message
        messageDisplayer.setAction(nextAction);
        
        messageDisplayer.setActionParameter(nextActionParameter);

        return retVal;
    }


}
