/*****************************************************************************
    Class:        DocumentStatusDetailPrepareAction
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author        Ralf Witt
    Created:      May 2001

    $Revision: #2 $
    $Date: 2001/11/20 $
*****************************************************************************/

package com.sap.isa.isacore.action.order;

import javax.servlet.http.HttpServletRequest;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.order.OrderStatus;

/**
 * Prepares to show a documents detail
 *
 * <br> Note that all Action classes have to provide a non argument constructor
 * because they were instantiated automatically by the action servlet.
 *
 * @author  Ralf Witt
 * @version 0.1
 *
 */
public class CustomerDocumentStatusDetailPrepareAction extends DocumentStatusDetailPrepareBaseAction {



    /**
     * Constructor.
     * Overwrites <code>href_action</code> and <code>forward</code>
     */
    public CustomerDocumentStatusDetailPrepareAction() {

        href_action = "b2b/customerdocumentstatusdetailprepare.do";
        forward = "customer_order_status";

    }


    /**
     * Returns the order status object.
     *
     * @see com.sap.isa.isacore.action.order.DocumentStatusDetailPrepareBaseAction#getSalesDocumentStatus(BusinessObjectManager)
     */
    protected OrderStatus getSalesDocumentStatus(HttpServletRequest request, BusinessObjectManager bom) {
        OrderStatus orderStatusDetail = bom.getCustomerOrderStatus();
        if(orderStatusDetail == null) {
            orderStatusDetail = bom.createCustomerOrderStatus();
        }
        return orderStatusDetail;
    }


    /**
     * @see com.sap.isa.isacore.action.order.DocumentStatusDetailPrepareBaseAction#getSalesDocumentType(HeaderSalesDocument)
     */
    protected String getSalesDocumentType(HeaderSalesDocument header) {

        return "customerOrder";
    }

}
