/*****************************************************************************
    Class:        DocumentListBaseAction
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author
    Created:      March 2001

    $Revision: #2 $
    $Date: 2001/07/18 $
*****************************************************************************/

package com.sap.isa.isacore.action.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectException;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Search;
import com.sap.isa.businessobject.SearchResult;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.businessobject.UserStatusProfileSearchCommand;
import com.sap.isa.businessobject.billing.BillingStatus;
import com.sap.isa.businessobject.order.DocumentListFilter;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.businesspartner.businessobject.PartnerFunctionBase;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.actionform.order.DocumentListSelectorForm;

/**
 * Base class to handle the selection of documents.
 *
 * @author  SAP
 * @version 1.0
 */
public abstract class DocumentListBaseAction extends IsaCoreBaseAction {

    /**
     * Parameter 
     */
	public static final String RP_NO_SEARCH_RESULT = "noSearchResult";

    /**
     * List of sales documents stored in request scope.
     */
    public static final String RK_ORDER_LIST = "orderlist";

    /**
     * List of billing documents stored in request scope.
     */
    public static final String RK_BILLING_LIST = "billinglist";

    /**
     * Number of read document headers
     */
    public static final String RK_DOCUMENT_COUNT = "documentcount";

    /**
     * Shopinstance
     */
    public static final String RK_DLA_SHOP = "shopinstance";
    /**
     *
     */
    public static final String RK_DLA_USERSTATUSPROFILE = "userstatusprofile";

    /**
     * Set this to true to signal a DocumentListSelectorForm Bean is passed not
     * via request but via session
     */
    public static final String SC_CROSSENTRYCALL = "dsls_crossentrycall";

    /**
     * DocumentListSelectorForm Bean in userSessionData Context
     */
    public static final String SC_FORMBEAN = "dsls_formbean";
    
	/**
	 * Get the name to identify the form in the session context
	 * 
	 * @return
	 */
	abstract protected String getNameOfListSelectorForm();
	

    /**
     * Contains the class specific functionality.
     *
     * @param bom                       Reference to the BusinessObjectManager
     * @param documentListSelectorForm  Reference to the action form
     * @param filter                    the filter object contains the selection criteria
     * @param request                   The request object
     * @param requestParser             Parser to simple retrieve data from the request
     * @param log                       Reference to the IsaLocation, needed for logging
     *
     * @return Forward to another action or page
     */
    abstract public ActionForward performSalesOrder(ActionMapping mapping,
             	BusinessObjectManager bom,
                DocumentListSelectorForm documentListSelectorForm,
                DocumentListFilter filter,
                Shop shop,
                boolean performSearch,
                HttpServletRequest request,
                UserSessionData userSessionData,
                RequestParser requestParser,
                IsaLocation log)
            throws CommunicationException;


    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            BusinessObjectManager bom,
            IsaLocation log)
            throws CommunicationException {

		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        ActionForward forward = null;
        	
        // Page that should be displayed next.
        HttpSession session = request.getSession();

        Shop shop = bom.getShop();
        if (shop == null) {
			log.exiting();
             throw new PanicException("shop.notFound");
        }

        // get sold to
        User user = bom.getUser();
        if (user == null) {
			log.exiting();
            return mapping.findForward("login");
        }

        boolean noBean = (request.getParameter("nobean") != null);
        boolean crossEntryCall = (userSessionData.getAttribute(DocumentListBaseAction.SC_CROSSENTRYCALL) != null);

        // get / check the form
        if (form != null && !(form instanceof DocumentListSelectorForm)) {
			log.exiting();
            throw new PanicException("status.message.noListActionForm");
        }

        DocumentListSelectorForm documentListSelectorForm =
                (DocumentListSelectorForm) form;

        // It may happen that this page is called as a forward of another
        // action and the form data is not provided correctly.
        // This bug was reported in CSN message 02529223.
        // To fix this a workaround stores the old form in the session
        // context an uses this as a fallback if no usable form
        // is found.
        if (noBean) {
            Object sessionSelector = session.getAttribute(getNameOfListSelectorForm());

            if (sessionSelector != null) {
                documentListSelectorForm =
                        (DocumentListSelectorForm) sessionSelector;
            }
        }
        else {
            // Store the from in the session context to allow a refreshment of
            // the list with already set search parameter.
            session.setAttribute(getNameOfListSelectorForm(), (DocumentListSelectorForm) documentListSelectorForm.clone());
        }

        // CrossEntry call enabling.
		if (crossEntryCall) {
			Object sessionSelector = userSessionData.getAttribute(SC_FORMBEAN);
			if (sessionSelector != null) {
				documentListSelectorForm =
						(DocumentListSelectorForm) sessionSelector;
				// Store the from in the session context to allow a refreshment of
				// the list with already set search parameter.
				session.setAttribute(getNameOfListSelectorForm(), (DocumentListSelectorForm) documentListSelectorForm);
			}
            // Remove session data
            userSessionData.removeAttribute(SC_CROSSENTRYCALL);
            userSessionData.removeAttribute(SC_FORMBEAN);
        }
		
		
		/*
        // Reset selection start false 
        if (request.getParameter(DocumentListSelectorForm.SETSALESSELECTIONSTART) == null) {
		    documentListSelectorForm.setSalesSelectionStart(false);
        }
		if (request.getParameter(DocumentListSelectorForm.SETBILLINGSELECTIONSTART) == null) {
			documentListSelectorForm.setBillingSelectionStart(false);
		}
		*/


        // When coming from contract JSP, the documenttype has already been seleced there
        String docTypePreset = (String)request.getParameter(DocumentListSelectorForm.DOCUMENT_TYPE);

        if (docTypePreset != null) {
            documentListSelectorForm.setDocumentType(docTypePreset);
        }


        // Get User Status profile for given Procedure and add to request
        Search search = bom.createSearch();
        SearchResult resultStatusProfile = null;
        resultStatusProfile = search.performSearch(new UserStatusProfileSearchCommand(shop.getLanguage()));
        ResultData result =  new ResultData(resultStatusProfile.getTable());
        request.setAttribute(RK_DLA_USERSTATUSPROFILE, result);

        // Make Shop available for JSP (to check for Contract and quotation allowance)
        request.setAttribute(RK_DLA_SHOP, shop);

        // only the search criteria should be displayed
        if (requestParser.getParameter(RP_NO_SEARCH_RESULT).isSet()) {
         
            if (documentListSelectorForm.getDocumentType().equals(DocumentListSelectorForm.ORDER) ||
                	documentListSelectorForm.getDocumentType().equals(DocumentListSelectorForm.ORDERTEMPLATE) ||
                	documentListSelectorForm.getDocumentType().equals(DocumentListSelectorForm.QUOTATION)) {
				log.exiting();
             	return mapping.findForward("orderlist");
            }
            else {
				log.exiting();
                return mapping.findForward("billinglist");
            }
        }

        DocumentListFilter filter = documentListSelectorForm.getDocumentListFilter();

        if (log.isDebugEnabled()) {
            log.debug("Shop ID:        " + shop.getId().trim());
            log.debug("DocType:        " + filter.getType().trim());
            log.debug("DocStatus:      " + filter.getStatus().trim());
            log.debug("DocDate:        " + filter.getChangedDate().trim());
        }

        // SALESDOCUMENTS
        if (filter.getType().equals(DocumentListFilter.SALESDOCUMENT_TYPE_ORDER)         ||
            filter.getType().equals(DocumentListFilter.SALESDOCUMENT_TYPE_ORDERTEMPLATE) ||
            filter.getType().equals(DocumentListFilter.SALESDOCUMENT_TYPE_QUOTATION)) {

                forward = performSalesOrder(mapping,bom,documentListSelectorForm, filter,                
                                     shop, 
                                     noBean || documentListSelectorForm.isSalesSelectionStartTrue(), // noBean || searchButtonPressed,	
				                     request, userSessionData, requestParser,log);
        }

        // BILLINGDOCUMENTS
        if (filter.getType().equals(DocumentListFilter.BILLINGDOCUMENT_TYPE_INVOICE)       ||
            filter.getType().equals(DocumentListFilter.BILLINGDOCUMENT_TYPE_CREDITMEMO)    ||
            filter.getType().equals(DocumentListFilter.BILLINGDOCUMENT_TYPE_DOWNPAYMENT)) {

                 BillingStatus billingList = bom.getBillingStatus();

				 if (noBean || documentListSelectorForm.isBillingSelectionStartTrue()) {	
                     if (billingList == null) {
                         billingList = bom.createBillingStatus();
                     }
                     BusinessPartnerManager buPaMa = bom.createBUPAManager();    
                     TechKey soldToKey = (buPaMa.getDefaultBusinessPartner(PartnerFunctionBase.SOLDTO) != null 
                                          ? buPaMa.getDefaultBusinessPartner(PartnerFunctionBase.SOLDTO).getTechKey()
                                          : new TechKey(""));
                     // Get Billingdocuments headers
                     billingList.readBillingHeaders(soldToKey, shop, filter);
				 // }

                 // if (billingList != null) {
                    // show old search
                 	request.setAttribute(RK_DOCUMENT_COUNT, new Integer(billingList.getBillingCount()).toString());
                    request.setAttribute(RK_BILLING_LIST, billingList);
                 }

                 forward = mapping.findForward("billinglist");
        }


		if (forward == null) {
            // Neither Sales nor Billingdocument !!!
            forward = mapping.findForward("error");
            request.setAttribute("Exception",new BusinessObjectException("no valid document type",new Message(Message.ERROR,"status.error.novaliddocumenttype")));
		}        	

        documentListSelectorForm.setBillingSelectionStart(false);
        documentListSelectorForm.setSalesSelectionStart(false);
		       
		log.exiting();
		return forward;        
        
    }
}