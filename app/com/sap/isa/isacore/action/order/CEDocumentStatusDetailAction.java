/*****************************************************************************
    Class:        CEDocumentStatusDetailAction
    Copyright (c) 2002, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author        Ralf Witt

    $Revision$
    $DateTime$ (Last changed)
    $Change$ (changelist)

*****************************************************************************/
package com.sap.isa.isacore.action.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.actionform.order.DocumentListSelectorForm;

/**
 *
 * Checks where the call comes from, and routes to the specific action
 * provided in parameter <b>nextaction</b>
 */
public class CEDocumentStatusDetailAction extends IsaCoreBaseAction {
	
    // Constants

    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            BusinessObjectManager bom,
            IsaLocation log)
            throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        String xDocKey = (request.getParameter("doc_key") != null ? request.getParameter("doc_key") : "");
        String xDocType = (request.getParameter("doc_type") != null ? request.getParameter("doc_type") : "");
        String xDocId = (request.getParameter("doc_id") != null ? request.getParameter("doc_id") : "");

        //if (xDocKey.equals("") || xDocType.equals("") || xDocId.equals("")) {
		if (xDocKey.equals("") || xDocType.equals("")) {		
			
			xDocKey = (String) (request.getAttribute("doc_key") != null ? request.getAttribute("doc_key") : "");
			xDocType = (String) (request.getAttribute("doc_type") != null ? request.getAttribute("doc_type") : "");
			
			if (xDocKey.equals("") || xDocType.equals("")) {
			log.exiting();
            return mapping.findForward("parameterincomplete");
			}
        }

        request.setAttribute("techkey", xDocKey);
        request.setAttribute("object_id", xDocId);
        request.setAttribute("objecttype", xDocType);
        if (xDocType != null  && (
            xDocType.equals(DocumentListSelectorForm.INVOICE)      ||
            xDocType.equals(DocumentListSelectorForm.DOWNPAYMENT)  ||
            xDocType.equals(DocumentListSelectorForm.CREDITMEMO)))  {
            request.setAttribute("billing", "X");
        }
        request.setAttribute("externalforward","startPdo");
		log.exiting();
        return mapping.findForward("documentstatusdetail");
    }
}