/*****************************************************************************
  Class:        MaintainOrderBatchAction
  Copyright (c) 2001, SAP AG, Germany, All rights reserved.
  Author:       Daniel Seise
  Created:      25.11.2002
  Version:      1.0

  $Revision: #8 $
  $Date: 2002/11/25 $
*****************************************************************************/

package com.sap.isa.isacore.action.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businessobject.order.OrderChange;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.action.IsaCoreInitAction;

/**
 * Action to display the batches for a material.
 * <p>
 * The following forwards are used by this action:
 * </p>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 * 	 <tr><td>FORWARD_SHOWBATCH</td><td>used when the document is valid</td></tr>
 *   <tr><td>FORWARD_CHANGEORDER</td><td>used when the document is not valid</td></tr>
 * </table>
 *
 * @author Daniel Seise
 * @version 1.0
 */
public class MaintainOrderBatchAction extends MaintainOrderBaseAction {

	/**
	 * @see com.sap.isa.isacore.action.order.MaintainOrderBaseAction#orderPerform(HttpServletRequest, HttpServletResponse, HttpSession, UserSessionData, RequestParser, BusinessObjectManager, IsaLocation, StartupParameter, BusinessEventHandler, boolean, boolean, User, Shop, OrderChange, DocumentState, DocumentHandler)
	 */
	protected String orderPerform(
		HttpServletRequest request,
		HttpServletResponse response,
		HttpSession session,
		UserSessionData userSessionData,
		RequestParser parser,
		BusinessObjectManager bom,
		IsaLocation log,
		IsaCoreInitAction.StartupParameter startupParameter,
		BusinessEventHandler eventHandler,
		boolean multipleInvocation,
		boolean browserBack,
		Shop shop,
		OrderChange order,
		DocumentState targetDocument,
		DocumentHandler documentHandler)
		throws CommunicationException {
		
		final String METHOD_NAME = "orderPerform()";
		log.entering(METHOD_NAME);
		updateOrder(parser, userSessionData, order, shop, bom.getBUPAManager(), log, request);

        
        boolean isDebugEnabled = log.isDebugEnabled();
        
        if (!order.isvalid()) {

            	if (isDebugEnabled) {
                	log.debug("Document NOT valid");
                	log.debug("Message provided: " + order.getMessageList());
            	}
				log.exiting();
           		return FORWARD_CHANGEORDER;
        	}
        	else{
       
        		
            	userSessionData.setAttribute("batchselection", request.getParameter("batchselection"));
            	if (order.getItem(new TechKey((String)userSessionData.getAttribute("batchselection"))) != null){
					userSessionData.setAttribute("batchproductid", request.getParameter("batchproductid"));
					userSessionData.setAttribute("batchproduct", request.getParameter("batchproduct"));
					log.exiting();
					return FORWARD_SHOWBATCH;
            	}
            	else{
					log.exiting();
            		return FORWARD_CHANGEORDER;
            	}
        	}
	}

}
