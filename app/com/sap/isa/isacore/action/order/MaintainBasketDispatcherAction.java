/*****************************************************************************
  Class:        MaintainBasketDispatcherAction
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       SAP AG
  Created:      10.09.2001
  Version:      1.0

  $Revision: #8 $
  $Date: 2002/06/18 $
*****************************************************************************/

package com.sap.isa.isacore.action.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.action.IsaCoreInitAction;

/**
 * Action to dispatch all requests coming from the <code>order.jsp</code> to
 * different (specialized) actions. The decision to which action the control
 * is forwarded to depends on the request parameters set.<br><br>
 *
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>Request parameter</b></td>
 *     <td><b>Value of parameter</b></td>
 *     <td><b>Logical forward used</b></td>
 *   <tr>
 *   <tr><td>showonly</td><td>!= ""</td><td>showonly</td><tr>
 *   <tr><td>returnconfig</td><td>!= ""</td><td>returnconfig</td></tr>
 *   <tr><td>exitconfig</td><td>!= ""</td><td>exitconfig</td></tr>
 *   <tr><td>deletekey</td><td>!= ""</td><td>delete</td></tr>
 *   <tr><td>sendpressed</td><td>!= ""</td><td>send</td></tr>
 *   <tr><td>replaceditem</td><td>!= ""</td><td>replaceditem</td></tr>
 *   <tr><td>newshipto</td><td>!= ""</td><td>newshipto</td></tr>
 *   <tr><td>cancelpressed</td><td>!= ""</td><td>cancel</td></tr>
 *   <tr><td>refresh</td><td>!= null</td><td>refresh</td></tr>
 *   <tr><td>newpos</td><td>!= ""</td><td>refresh</td></tr>
 *   <tr><td>simulatepressed</td><td>!= ""</td><td>simulate</td></tr>
 *   <tr><td>configitemid</td><td>!= ""</td><td>prepareitemconfig</td></tr>
 *   <tr><td>processsoldto</td><td>!= ""</td><td>processsoldto</td></tr>
 *   <tr><td>batchselection</td><td>!= ""</td><td>itembatch</td></tr>
 * </table>
 * <p>
 * According to the described parameters the corresponding forwards are used.
 * </p>
 *
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>showonly</td><td>the content of the document is displayed again</td></tr>
 *   <tr><td>returnconfig</td><td>return from the configuration, use the config data</td></tr>
 *   <tr><td>exitconfig</td><td>exit from the configuration, all config data is discarded</td></tr>
 *   <tr><td>delete</td><td>an item in the document is deleted</td></tr>
 *   <tr><td>send</td><td>the user is finished and wants to save the document</td></tr>
 *   <tr><td>newshipto</td><td>new ship tos are added to the document</td></tr>
 *   <tr><td>cancel</td><td>the document is cancelled</td></tr>
 *   <tr><td>refresh</td><td>the data of the document is send to the backend and displayed again</td></tr>
 *   <tr><td>replaceditem</td><td>an item was replaced with another one from the CUA</td></tr>
 *   <tr><td>simulate</td><td>the user wants to simulate the basket as a R/3 order</td></tr>
 *   <tr><td>prepareitemconfig</td><td>save all changes, before an item is configured</td></tr>
 *   <tr><td>processsoldto</td><td>save all changes, before an soldTo is created or detail information is shown</td></tr>
 * 	 <tr><td>itembatch</td><td>save all changes, before entering batch characteristics for the material</td></tr>
 * </table>
 *
 *
 * @author SAP AG
 * @version 1.0
 */
public class MaintainBasketDispatcherAction extends MaintainBasketBaseAction {

    private static final String FORWARD_SHOWONLY     = "showonly";
    private static final String FORWARD_RETURNCONFIG = "returnconfig";
    private static final String FORWARD_EXITCONFIG   = "exitconfig";
    private static final String FORWARD_DELETE       = "delete";
    private static final String FORWARD_SEND         = "send";
    private static final String FORWARD_NEWSHIPTO    = "newshipto";
	private static final String FORWARD_SEARCHSHIPTO = "searchshipto";    
    private static final String FORWARD_CANCEL       = "cancel";
    private static final String FORWARD_REPLACEDITEM = "replaceditem";
    private static final String FORWARD_REFRESH      = "refresh";
    private static final String FORWARD_UPDATEDOC    = "updatedocumentview";
    private static final String FORWARD_SIMULATE     = "simulate";
    protected static final String FORWARD_PREPAREITEMCONFIG = "prepareitemconfig";
    protected static final String FORWARD_PROCESSSOLDTO = "processsoldto";
    protected static final String FORWARD_SHOWCUADETAIL = "showcuadetail";
	protected static final String FORWARD_SHOWPRODUCTDETAIL = "showproductdetail";
    //batch forward
    private static final String FORWARD_BATCH 		= "itembatch";


    /**
     * Overriden <em>basketPerform</em> method of <em>MaintainBasketBaseAction</em>.
     */

    protected String basketPerform(HttpServletRequest request,
                HttpServletResponse response,
                HttpSession session,
                UserSessionData userSessionData,
                RequestParser parser,
                BusinessObjectManager bom,
                IsaLocation log,
                IsaCoreInitAction.StartupParameter startupParameter,
                BusinessEventHandler eventHandler,
                boolean multipleInvocation,
                boolean browserBack,
                Shop shop,
                SalesDocument preOrderSalesDocument,
                boolean auction,
                DocumentState targetDocument,
                DocumentHandler documentHandler)
                    throws CommunicationException {
		final String METHOD_NAME = "basketPerform()";
		log.entering(METHOD_NAME);
        boolean isDebugEnabled = log.isDebugEnabled();

        // Page that should be displayed next.
        String forwardTo = null;
        // if customer likes to override forward, this string is used
        String customerForwardTo = null;

		//Set the grid check  box indicators to session variables,
		//to have it preset for further visits to the screen.
		STANDARD_PARSER.parsePresetGridCheckBoxValues(request, userSessionData);
		
        if (browserBack) {
			log.exiting();
            return FORWARD_UPDATEDOC;
        }

         // check for possible cases
        if ((request.getParameter("showonly") != null) &&
            (request.getParameter("showonly").length() > 0)) {

            forwardTo = FORWARD_SHOWONLY;

            if (isDebugEnabled) {
                log.debug("Requested action 'showonly' dispatching to Action '" + forwardTo + "'");
            }
        }
        else if ((request.getParameter("returnconfig") != null) &&
                 (request.getParameter("returnconfig").length() > 0)) {

            forwardTo = FORWARD_RETURNCONFIG;

            if (isDebugEnabled) {
                log.debug("Requested action 'returnconfig' dispatching to Action '" + forwardTo + "'");
            }
        }
        else if ((request.getParameter("exitconfig") != null) &&
                 (request.getParameter("exitconfig").length() > 0)) {

            forwardTo = FORWARD_EXITCONFIG;

            if (isDebugEnabled) {
                log.debug("Requested action 'exitconfig' dispatching to Action '" + forwardTo + "'");
            }
        }
        else if ((request.getParameter("deletekey") != null) &&
                 (request.getParameter("deletekey").length() > 0)) {

            preOrderSalesDocument.clearMessages();

            // update basket
            updateBasket(parser, userSessionData, preOrderSalesDocument, shop, log, request);

            forwardTo = FORWARD_DELETE;

            if (isDebugEnabled) {
                log.debug("Requested action 'delete' dispatching to Action '" + forwardTo + "'");
            }
        }
        else if ((request.getParameter("sendpressed") != null) &&
                 (request.getParameter("sendpressed").length() > 0)) {

			// update basket
			updateBasket(parser, userSessionData, preOrderSalesDocument, shop, log, request);

            forwardTo = FORWARD_SEND;

            if (isDebugEnabled) {
                log.debug("Requested action 'sendpressed' dispatching to action '" + forwardTo + "'");
            }
        }
        else if ((request.getParameter("simulatepressed") != null) &&
                 (request.getParameter("simulatepressed").length() > 0)) {

			// update basket
			updateBasket(parser, userSessionData, preOrderSalesDocument, shop, log, request);

            forwardTo = FORWARD_SIMULATE;

            if (isDebugEnabled) {
                log.debug("Requested action 'simulatepressed' dispatching to action '" + forwardTo + "'");
            }
        }
        else if ((request.getParameter("newshipto") != null) &&
                 (request.getParameter("newshipto").length() > 0)) {

			// update basket
			updateBasket(parser, userSessionData, preOrderSalesDocument, shop, log, request);

            forwardTo = FORWARD_NEWSHIPTO;

            if (isDebugEnabled) {
                log.debug("Requested action 'newshipto' dispatching to Action '" + forwardTo + "'");
            }
        }
		else if ((request.getParameter("shiptosearch") != null) &&
				 (request.getParameter("shiptosearch").length() > 0)) {

			// update basket
			updateBasket(parser, userSessionData, preOrderSalesDocument, shop, log, request);

			forwardTo = FORWARD_SEARCHSHIPTO;

			if (isDebugEnabled) {
				log.debug("Requested action 'shiptosearch' dispatching to Action '" + forwardTo + "'");
			}
		}        
        else if ((request.getParameter("cancelpressed") != null) &&
                 (request.getParameter("cancelpressed").length() > 0)) {

            forwardTo = FORWARD_CANCEL;

            if (isDebugEnabled) {
                log.debug("Requested action 'cancel' dispatching to Action '" + forwardTo + "'");
            }
        }
        else if ((request.getParameter("replaceditem") != null) &&
                 (request.getParameter("replaceditem").length() > 0)) {

			// update basket
			updateBasket(parser, userSessionData, preOrderSalesDocument, shop, log, request);

            forwardTo = FORWARD_REPLACEDITEM;

            if (isDebugEnabled) {
                log.debug("Requested action 'replaceitem' dispatching to Action '" + forwardTo + "'");
            }
        }
		else if ((request.getParameter("showproduct") != null) &&
				 (request.getParameter("showproduct").length() > 0)) {

			// update basket
			updateBasket(parser, userSessionData, preOrderSalesDocument, shop, log, request);
			updateSalesDocument(parser, userSessionData, preOrderSalesDocument, shop, bom.createBUPAManager(), log);

			request.setAttribute("productId",request.getParameter("showproduct"));			

			forwardTo = FORWARD_SHOWPRODUCTDETAIL;

			if (isDebugEnabled) {
				log.debug("Requested action 'showproductdetail' dispatching to Action '" + forwardTo + "'");
			}
	    }	
        else if ((request.getParameter("configitemid") != null)
                     && (request.getParameter("configitemid").length() > 0)) {

			// update basket
			updateBasket(parser, userSessionData, preOrderSalesDocument, shop, log, request);

			forwardTo = FORWARD_PREPAREITEMCONFIG;

            if (isDebugEnabled) {
                log.debug("Requested action 'prepareitemconfig' dispatching to Action '" + forwardTo + "'");
            }
        }
        
        // batch added
        else if ((request.getParameter("batchselection") != null)
                     && (request.getParameter("batchselection").length() > 0)) {

			// update basket
			updateBasket(parser, userSessionData, preOrderSalesDocument, shop, log, request);

            forwardTo = FORWARD_BATCH;

            if (isDebugEnabled) {
                log.debug("Requested action 'batchselection' dispatching to Action '" + forwardTo + "'");
            }
        }
        
        else if ((request.getParameter("cuaproductkey") != null)
                     && (request.getParameter("cuaproductkey").length() > 0)) {

			// update basket
			updateBasket(parser, userSessionData, preOrderSalesDocument, shop, log, request);
			updateSalesDocument(parser, userSessionData, preOrderSalesDocument, shop, bom.createBUPAManager(), log);

            forwardTo = FORWARD_SHOWCUADETAIL;

            request.setAttribute("forward",forwardTo);

            if (isDebugEnabled) {
                log.debug("Requested action 'showcuadetail' dispatching to Action '" + forwardTo + "'");
            }
        }
        else if ((request.getParameter("processsoldto") != null)
                     && (request.getParameter("processsoldto").length() > 0)) {

			// update basket
			updateBasket(parser, userSessionData, preOrderSalesDocument, shop, log, request);

            forwardTo = FORWARD_PROCESSSOLDTO;

            if (isDebugEnabled) {
                log.debug("Requested action 'processsoldto' dispatching to Action '" + forwardTo + "'");
            }
        }
        else if (request.getParameter("refresh") != null 
                  && request.getParameter("refresh").length() > 0) {

			// update basket
			updateBasket(parser, userSessionData, preOrderSalesDocument, shop, log, request);

            forwardTo = FORWARD_REFRESH;

            log.debug("Requested action 'refresh' dispatching to Action '" + forwardTo + "'");
        }

        // user exit, to modify forward, if necessary
        customerForwardTo = customerExitDispatcher(parser);
        forwardTo = (customerForwardTo == null) ? forwardTo : customerForwardTo;
        if (forwardTo == null) {
        	forwardTo = FORWARD_REFRESH;
        }
		log.exiting();
        return forwardTo;
    }
}