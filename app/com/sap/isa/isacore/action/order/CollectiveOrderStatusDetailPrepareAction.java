/*****************************************************************************
    Class:        CollectiveStatusDetailPrepareAction
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author        SAP
    Created:      November 2002

    $Revision: #2 $
    $Date: 2001/11/20 $
*****************************************************************************/

package com.sap.isa.isacore.action.order;

import javax.servlet.http.HttpServletRequest;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.order.OrderStatus;

/**
 * Prepares to show a collective order
 *
 * @author  SAP
 * @version 0.1
 *
 */
public class CollectiveOrderStatusDetailPrepareAction extends DocumentStatusDetailPrepareBaseAction {

    /**
     * Constructor.
     * Overwrites <code>href_action</code> and <code>forward</code>
     */
    public CollectiveOrderStatusDetailPrepareAction() {

        href_action = null;
        forward = "order_status";
    }


    /**
     * Returns the order status object.
     *
     * @see com.sap.isa.isacore.action.order.DocumentStatusDetailPrepareBaseAction#getSalesDocumentStatus(BusinessObjectManager)
     */
    protected OrderStatus getSalesDocumentStatus(HttpServletRequest request, BusinessObjectManager bom) {
        OrderStatus orderStatusDetail = bom.getCollectiveOrderStatus();
        if(orderStatusDetail == null) {
            orderStatusDetail = bom.createCollectiveOrderStatus();
        }
        return orderStatusDetail;
    }


    /**
     * @see com.sap.isa.isacore.action.order.DocumentStatusDetailPrepareBaseAction#getSalesDocumentType(HeaderSalesDocument)
     */
    protected String getSalesDocumentType(HeaderSalesDocument header) {

        return HeaderSalesDocument.DOCUMENT_TYPE_COLLECTIVE_ORDER;
    }

}
