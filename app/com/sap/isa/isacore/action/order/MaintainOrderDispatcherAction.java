
/*****************************************************************************
  Class:        MaintainOrderDispatcherAction
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:
  Created:      18.09.2001
  Version:      1.0

  $Revision: #6 $
  $Date: 2002/08/21 $
*****************************************************************************/

package com.sap.isa.isacore.action.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businessobject.order.OrderChange;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.action.IsaCoreInitAction;

/**
 * Action to dispatch all requests coming from the <code>order_change.jsp</code> to
 * different (specialized) actions. The decision to which action the control
 * is forwarded to depends on the request parameters set.<br><br>
 *
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>Request parameter</b></td>
 *     <td><b>Value of parameter</b></td>
 *     <td><b>Logical forward used</b></td>
 *   <tr>
 *   <tr><td>returnconfig</td><td>!= ""</td><td>returnconfig</td></tr>
 *   <tr><td>exitconfig</td><td>!= ""</td><td>exitconfig</td></tr>
 *   <tr><td>sendpressed</td><td>!= ""</td><td>send</td></tr>
 *   <tr><td>cancelpressed</td><td>!= ""</td><td>cancel</td></tr>
 *   <tr><td>replaceditem</td><td>!= ""</td><td>replaceditem</td></tr>
 *   <tr><td>refresh</td><td>!= null</td><td>refresh</td></tr>
 *   <tr><td>newpos</td><td>!= ""</td><td>refresh</td></tr>
 *   <tr><td>configitemid</td><td>!= ""</td><td>prepareitemconfig</td></tr>
 *   <tr><td>batchselection</td><td>!= ""</td><td>itembatch</td></tr>
 * </table>
 * <p>
 * According to the described parameters the corresponding forwards are used.
 * </p>
 *
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>returnconfig</td><td>return from the configuration, use the config data</td></tr>
 *   <tr><td>exitconfig</td><td>exit from the configuration, all config data is discarded</td></tr>
 *   <tr><td>send</td><td>the user is finished and wants to save the document</td></tr>
 *   <tr><td>newshipto</td><td>new ship tos are added to the document</td></tr>
 *   <tr><td>cancel</td><td>the document is cancelled</td></tr>
 *   <tr><td>refresh</td><td>the data of the document is send to the backend and displayed again</td></tr>
 *   <tr><td>replaceditem</td><td>an item was replaced with another one from the CUA</td></tr>
 *   <tr><td>prepareitemconfig</td><td>save all changes, before an item is configured</td></tr>
 * 	 <tr><td>itembatch</td><td>save all changes, before entering batch characteristics for the material</td></tr>
 * </table>
 *
 *
 * @author Thomas Smits
 * @version 1.0
 */
public class MaintainOrderDispatcherAction extends MaintainOrderBaseAction {

	
    private static final String FORWARD_RETURNCONFIG   = "returnconfig";
    private static final String FORWARD_EXITCONFIG   = "exitconfig";
    private static final String FORWARD_SEND         = "send";
    private static final String FORWARD_ORDER         = "ordercollectiveorder";
    private static final String FORWARD_CANCEL       = "cancel";
    private static final String FORWARD_REPLACEDITEM = "replaceditem";
    private static final String FORWARD_REFRESH      = "refresh";
    private static final String FORWARD_UPDATEDOC    = "updatedocumentview";
    private static final String FORWARD_NEW_SHIPTO    = "newshipto";
	private static final String FORWARD_SEARCHSHIPTO = "searchshipto";    
    private static final String FORWARD_BATCH		= "itembatch";
    protected static final String FORWARD_SHOWCUADETAIL = "showcuadetail";
    protected static final String FORWARD_PREPAREITEMCONFIG = "prepareitemconfig";
	protected static final String FORWARD_SHOWPRODUCTDETAIL = "showproductdetail";



    /**
     * Overriden <em>isaPerform</em> method of <em>IsaCoreBaseAction</em>.
     */
   protected String orderPerform(HttpServletRequest request,
                HttpServletResponse response,
                HttpSession session,
                UserSessionData userSessionData,
                RequestParser parser,
                BusinessObjectManager bom,
                IsaLocation log,
                IsaCoreInitAction.StartupParameter startupParameter,
                BusinessEventHandler eventHandler,
                boolean multipleInvocation,
                boolean browserBack,
                Shop shop,
                OrderChange order,
                DocumentState targetDocument,
                DocumentHandler documentHandler)
                    throws CommunicationException {
                    	
		final String METHOD_NAME = "orderPerform()";
		log.entering(METHOD_NAME);
        boolean isDebugEnabled = log.isDebugEnabled();


        // Page that should be displayed next.
        String forwardTo = null;

        // if customer likes to override forward, this string is used
        String customerForwardTo = null;
		//Set the grid check  box indicators to session variables,
		//to have it preset for further visits to the screen.
		STANDARD_PARSER.parsePresetGridCheckBoxValues(request, userSessionData);
		
        // check for possible cases
        if ((request.getParameter("showonly") != null) &&
            (request.getParameter("showonly").length() > 0)) {

            forwardTo = FORWARD_UPDATEDOC;

            if (isDebugEnabled) {
                log.debug("Requested action 'showonly' dispatching to Action '" + forwardTo + "'");
            }
        }
        // batch added
        else if ((request.getParameter("batchselection") != null)
                     && (request.getParameter("batchselection").length() > 0)) {

			forwardTo = FORWARD_BATCH;

            if (isDebugEnabled) {
                log.debug("Requested action 'batchselection' dispatching to Action '" + forwardTo + "'");
            }
        }
        else if ((request.getParameter("returnconfig") != null) &&
            (request.getParameter("returnconfig").length() > 0)) {

            forwardTo = FORWARD_RETURNCONFIG;

            if (isDebugEnabled) {
                log.debug("Requested action 'returnconfig' dispatching to Action '" + forwardTo + "'");
            }
        }
        else if ((request.getParameter("exitconfig") != null) &&
                 (request.getParameter("exitconfig").length() > 0)) {

            forwardTo = FORWARD_EXITCONFIG;

            if (isDebugEnabled) {
                log.debug("Requested action 'exitconfig' dispatching to Action '" + forwardTo + "'");
            }
        }
        else if ((request.getParameter("sendpressed") != null) &&
                 (request.getParameter("sendpressed").length() > 0)) {

            forwardTo = FORWARD_SEND;

            if (isDebugEnabled) {
                log.debug("Requested action 'sendpressed' dispatching to Action '" + forwardTo + "'");
            }

        }
        else if ((request.getParameter("orderpressed") != null) &&
                 (request.getParameter("orderpressed").length() > 0)) {

            forwardTo = FORWARD_ORDER;

            if (isDebugEnabled) {
                log.debug("Requested action 'sendpressed' dispatching to Action '" + forwardTo + "'");
            }

        }
        else if ((request.getParameter("cancelpressed") != null) &&
                 (request.getParameter("cancelpressed").length() > 0)){

            forwardTo = FORWARD_CANCEL;

            if (isDebugEnabled) {
                log.debug("Requested action 'cancelpressed' dispatching to Action '" + forwardTo + "'");
            }
        }
        else if ((request.getParameter("replaceditem") != null) &&
                 (request.getParameter("replaceditem").length() > 0)) {

            forwardTo = FORWARD_REPLACEDITEM;

            if (isDebugEnabled) {
                log.debug("Requested action 'replaceditem' dispatching to Action '" + forwardTo + "'");
            }
        }
		else if ((request.getParameter("showproduct") != null) &&
				 (request.getParameter("showproduct").length() > 0)) {

			updateOrder(parser, userSessionData, order, shop , bom.getBUPAManager(), log, request);

			request.setAttribute("productId",request.getParameter("showproduct"));			

			forwardTo = FORWARD_SHOWPRODUCTDETAIL;

			if (isDebugEnabled) {
				log.debug("Requested action 'showproductdetail' dispatching to Action '" + forwardTo + "'");
			}
		}	        
        else if ((request.getParameter("cuaproductkey") != null)
                     && (request.getParameter("cuaproductkey").length() > 0)) {

            // parse request
            updateOrder(parser, userSessionData, order, shop , bom.getBUPAManager(), log, request);

            forwardTo = FORWARD_SHOWCUADETAIL;

            request.setAttribute("forward",forwardTo);

            if (isDebugEnabled) {
                log.debug("Requested action 'showcuadetail' dispatching to Action '" + forwardTo + "'");
            }
        }
        else if ((request.getParameter("newshipto") != null) &&
                 (request.getParameter("newshipto").length() > 0)) {

            forwardTo = FORWARD_NEW_SHIPTO;

            if (isDebugEnabled) {
                log.debug("Requested action 'newshipto' dispatching to Action '" + forwardTo + "'");
            }
        }
		else if ((request.getParameter("shiptosearch") != null) &&
				 (request.getParameter("shiptosearch").length() > 0)) {

			forwardTo = FORWARD_SEARCHSHIPTO;

			if (isDebugEnabled) {
				log.debug("Requested action 'shiptosearch' dispatching to Action '" + forwardTo + "'");
			}
		}        
        else if ((request.getParameter("configitemid") != null)
                      && (request.getParameter("configitemid").length() > 0)) {

             forwardTo = FORWARD_PREPAREITEMCONFIG;

            if (isDebugEnabled) {
                log.debug("Requested action 'configitemid' dispatching to Action '" + forwardTo + "'");
            }
        }
        else if (request.getParameter("refresh") != null
                 && request.getParameter("refresh").length() > 0) {

            forwardTo = FORWARD_REFRESH;

            log.debug("Requested action 'refresh' dispatching to Action '" + forwardTo + "'");
        }
        // user exit, to modify forward, if necessary
        customerForwardTo = customerExitDispatcher(parser);
        forwardTo = (customerForwardTo == null) ? forwardTo : customerForwardTo;
		log.exiting();
        return forwardTo;
    }
}