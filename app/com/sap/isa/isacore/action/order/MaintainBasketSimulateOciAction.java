package com.sap.isa.isacore.action.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sap.isa.businessobject.BusinessObjectException;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businessobject.oci.HookUrlParser;
import com.sap.isa.businessobject.oci.OciLineList;
import com.sap.isa.businessobject.oci.OciServer;
import com.sap.isa.businessobject.order.Order;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.MessageList;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.action.IsaCoreInitAction;

/**
 * Transfers the basket to an external caller via OCI. Therefore makes use
 * of the OciServer business object.
 * 
 * @author SAP
 * 
 */
public class MaintainBasketSimulateOciAction extends MaintainBasketBaseAction {


	/**
	 * Name of the oci line list attribute stored in the request context.
	 */
	public static final String OCI_LINE_LIST = "ociLineList";

	/**
	 * Name of the oci line list attribute stored in the request context.
	 */
	public static final String OCI_HOOK_URL = "ociHookUrl";

	/**
	 * Names for caller, target and okcode parameter constants of an oci client
	 * stored in the request context.
	 */
	public static final String OCI_OKCODE = "~OkCode";
	public static final String OCI_TARGET = "~target";
	public static final String OCI_CALLER = "~caller";

	/**
	 * Name of the oci line list attribute stored in the request context.
	 */
	public static final String OCI_VERSION = "ociVersion";

	private static final String FORWARD_OCI_LINES_SEND = "ocilinessend";
	private static final String FORWARD_ERROR = "error";

	protected String basketPerform(
		HttpServletRequest request,
		HttpServletResponse response,
		HttpSession session,
		UserSessionData userSessionData,
		RequestParser parser,
		BusinessObjectManager bom,
		IsaLocation log,
		IsaCoreInitAction.StartupParameter startupParameter,
		BusinessEventHandler eventHandler,
		boolean multipleInvocation,
		boolean browserBack,
		Shop shop,
		SalesDocument preOrderSalesDocument,
		boolean auction,
		DocumentState targetDocument,
		DocumentHandler documentHandler)
		throws CommunicationException {

		HookUrlParser hup = null;
		HookUrlParser.Parameter hupParameter = null;
		MessageList messageList = new MessageList();
		OciLineList ociLineList = null;
		OciServer ociServer = null;
		String[] ressourceDummy = { "", "" };
		TechKey salesDocumentKey = null;



		Order order = bom.getOrder();

		ociServer = bom.getOciServer();
		if (ociServer == null) {
			ociServer = bom.createOciServer();
		}

		ociServer.setOciVersion(startupParameter.getOciVersion());
		
		if (log.isDebugEnabled()){
			log.debug("got oci server");
		}


		// get hook url & oci version as startup parameters
		if (startupParameter != null) {
			String hookUrl = startupParameter.getHookUrl();
			if (hookUrl.length() > 0) {
				ociServer.setHookUrl(hookUrl);
			} else {
				messageList.add(new Message(Message.ERROR, "oci.noHookUrl", ressourceDummy, ""));
				request.setAttribute("Exception", new BusinessObjectException("oci.noHookUrl", messageList));
				log.warn("oci.noHookUrl");
				return FORWARD_ERROR;
			}
			//				ociServer.setOciVersion(startupParameter.getOciVersion());
		} else {
			return FORWARD_ERROR;
		}

		// try to get sales document key ...

		if (order != null) {

			// read oci lines from the be
			ociLineList = ociServer.readOciLines(order, shop.getLanguage()); // might throw communicationException

			if (ociLineList.getMessageList().size() > 0) {
				messageList.add(new Message(Message.ERROR, "oci.xmlTransferError", ressourceDummy, ""));
				request.setAttribute("Exception", new BusinessObjectException("oci.xmlTransferError", messageList));
				return FORWARD_ERROR;
			}
		} else {
			throw new CommunicationException("no order available");
		}

		// pass values to the JSP via request context
		String hookUrl = ociServer.getHookUrl();
		request.setAttribute(OCI_HOOK_URL, hookUrl);
		request.setAttribute(OCI_VERSION, startupParameter.getOciVersion());
		request.setAttribute(OCI_LINE_LIST, ociLineList);
		hup = new HookUrlParser(hookUrl);
		hupParameter = hup.getParameter(OCI_OKCODE);
		if (hupParameter.isSet()) {
			request.setAttribute(OCI_OKCODE, hupParameter.getValue());
		}
		hupParameter = hup.getParameter(OCI_TARGET);
		if (hupParameter.isSet()) {
			request.setAttribute(OCI_TARGET, hupParameter.getValue());
		} else {
			request.setAttribute(OCI_TARGET, "_top"); // default value
		}
		hupParameter = hup.getParameter(OCI_CALLER);
		if (hupParameter.isSet()) {
			request.setAttribute(OCI_CALLER, hupParameter.getValue());
		}

		return FORWARD_OCI_LINES_SEND;
	}

}
