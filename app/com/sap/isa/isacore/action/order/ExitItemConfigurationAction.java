/*****************************************************************************
  Class:        ExitItemConfigurationAction
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Thomas Smits
  Created:      18.09.2001
  Version:      1.0

  $Revision: #3 $
  $Date: 2001/09/25 $
*****************************************************************************/

package com.sap.isa.isacore.action.order;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 * Action to exit a configuration. The data of that configuration is discarded.
 * <br><br>
 * The following forwards are used by this action:
 * </p>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>FORWARD_UPDATEDOCUMENT</td><td>always used</td></tr>
 * </table>
 *
 * @author SAP
 * @version 1.0
 */
public class ExitItemConfigurationAction extends IsaCoreBaseAction {
    
    protected String FORWARD_SUCCESS  = "success";

    
    /**
     * @see com.sap.isa.isacore.action.IsaCoreBaseAction#isaPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.sap.isa.core.UserSessionData, com.sap.isa.core.util.RequestParser, com.sap.isa.businessobject.BusinessObjectManager, com.sap.isa.core.logging.IsaLocation)
     */
    public ActionForward isaPerform(ActionMapping mapping,
        							ActionForm form,
        							HttpServletRequest request,
        							HttpServletResponse response,
							        UserSessionData userSessionData,
        							RequestParser requestParser,
        							BusinessObjectManager bom,
        							IsaLocation log)
        	throws IOException, ServletException, CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        DocumentHandler documentHandler = (DocumentHandler)
        userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
        if (documentHandler == null) {
			log.exiting();
            throw new PanicException("No document handler found");
        }
        	    
        userSessionData.setAttribute(ItemConfigurationBaseAction.SC_CONFIGURATION_ACTIVE, ItemConfigurationBaseAction.CONFIG_ACTIVE_FALSE);
        userSessionData.removeAttribute(ItemConfigurationBaseAction.SC_ITEM_IN_CONFIG);

        ManagedDocument onTopDoc = documentHandler.getManagedDocumentOnTop();

        // todo: kein docontop da
        ManagedDocument newDoc = onTopDoc.getAssociatedDocument(); 

        documentHandler.add(newDoc);
        documentHandler.setOnTop(newDoc.getDocument());
        
        //set in AddToBasketAction to return to catalog from grid screen
        String fromGridCatalog = (String) request.getSession().getAttribute("gridcatalog");
		if (fromGridCatalog != null && !fromGridCatalog.equals("")){
			request.getSession().removeAttribute("gridcatalog");		
			return mapping.findForward("gridtocatalog");        
		}

		log.exiting();
        return mapping.findForward(FORWARD_SUCCESS);
    }

}