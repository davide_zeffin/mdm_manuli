/*****************************************************************************
    Class         CreateDocumentAction
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Check, which document type should be created
    Created:      13 Maerz 2001
    Version:      0.1

    $Revision: #2 $
    $Date: 2001/07/11 $
*****************************************************************************/

package com.sap.isa.isacore.action.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.ActionConstants;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 * Dispatch depending of order type to create a document to add new items.
 *
 * <h4>Overview over request parameters and attributes</h4>
 * <table border="1" cellspacing="0" cellpadding="4">
 *   <tr>
 *     <th>name</th>
 *     <th>parameter</th>
 *     <th>attribute</th>
 *     <th>description</th>
 *   <tr>
 *   <tr>
 *      <td>orderType</td><td>X</td><td>&nbsp</td>
 *      <td>contains the type of the document, which should be created</td>
 *   </tr>
 *   <tr>
 *      <td>cancel</td><td>X</td><td>&nbsp</td>
 *      <td>user cancels the creation</td>
 *   </tr>
 * </table>
 *
 * <h4>The following forwards are used by this action</h4>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <th>forward</th>
 *     <th>description</th>
 *   <tr>
 *   <tr><td>cancelled</td><td>user has pressed the <i>cancel</i> button</td></tr>
 *   <tr><td>createBasket</td><td>a basket should be created</td></tr>
 *   <tr><td>createQuotation</td><td>a quatation should be created</td></tr>
 *   <tr><td>createOrderTemplate</td><td>an order template should be created</td></tr>
 *   <tr><td>error</td><td>an application error occurs</td></tr>
 * </table>
 * <h4>Overview over attributes which will be set in the request context</h4>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <th>attribute</th>
 *     <th>description</th>
 *   <tr>
 * <tr><td>forward</td><td>logical forward which, will be called after the creation.
 *    The value is <i>addtodocument</i> by default</td></tr>
 * <tr><td><i>AddToBasketAction.RC_TRANSFER_ITEM_LIST</i></td><td>list with basket transfer items,
 *   which should be added to the basket. The value is taken from the session context</td></tr>
 * <tr><td><i>AddToBasketAction.RC_TRANSFER_ITEM</i></td><td>basket transfer item,
 *   which should be added to the basket. The value is taken from the session context</td></tr>
 * <tr><td><i>AddToBasketAction.SC_FORWARD_NAME</i></td><td>logical forward, which will be called after
 *   the <i>add to basket</i> action. The value is taken from the session context</td></tr>
 * </table>
 *
 * @author SAP
 * @version 1.0
 */
public class CreateDocumentAction extends IsaCoreBaseAction {

    public static final String FORWARD_BASKET        = "createBasket";
    public static final String FORWARD_QUOTATION     = "createQuotation";
    public static final String FORWARD_ORDERTEMPLATE = "createOrderTemplate";
    public static final String FORWARD_CANCELLED     = "cancelled";

    /**
     * Overriden <em>isaPerform</em> method of <em>IsaCoreBaseAction</em>.
     */
    public ActionForward isaPerform(ActionMapping mapping,
                ActionForm form,
                HttpServletRequest request,
                HttpServletResponse response,
                UserSessionData userSessionData,
                RequestParser requestParser,
                BusinessObjectManager bom,
                IsaLocation log)
                        throws CommunicationException {

		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
         // Page that should be displayed next.
        String forwardTo = "error";
        
		String cancel = requestParser.getParameter("cancel").getValue().getString();
        if (cancel.equals("true")) {
            return mapping.findForward(FORWARD_CANCELLED);
        }

        // find out the request source for setting the correct forwardTo attribute
        String reqSource = requestParser.getParameter("requestSource").getValue().getString();
        
        // set the correct forward for the creation actions
        // if createAction is called from minibasket-button requestSource is minibasket
        if (reqSource.equals("minibasket")) {
        	request.setAttribute(ActionConstants.RA_FORWARD,"blankdocument");
        }
        else {
        	request.setAttribute(ActionConstants.RA_FORWARD,"addtodocument");
        }

		RequestParser.Parameter orderType = requestParser.getParameter("orderType");
		String orderTypeValue = "";
		String processTypeValue = "";
		if (orderType.isSet()) {
			orderTypeValue = orderType.getValue().getString();
		} else {
			log.exiting();
			return mapping.findForward(forwardTo);
		}
		    
		if (log.isDebugEnabled()) { log.debug("Ordertype: " + orderTypeValue); }
        if ( orderTypeValue.startsWith("o_")) {
        	processTypeValue = orderTypeValue.substring(2);
        	request.setAttribute("processtype",processTypeValue);
			if (log.isDebugEnabled()) { log.debug("Process type: " + processTypeValue); }
            forwardTo = FORWARD_BASKET;
        }
        else if (orderTypeValue.equals("template")) {
            forwardTo = FORWARD_ORDERTEMPLATE;
        }
        else if (orderTypeValue.startsWith("q_")) {
			processTypeValue = orderTypeValue.substring(2);
			request.setAttribute("processtype",processTypeValue);
			if (log.isDebugEnabled()) { log.debug("Process type: " + processTypeValue); }        	
            forwardTo = FORWARD_QUOTATION;
        }

        // reset all needed data for the addtobasket action
        // store transfer items in the request context
        Object obj = request.getSession().getAttribute(AddToBasketAction.SC_TRANSFER_ITEM_LIST);
        if (obj!= null) {
            request.setAttribute(AddToBasketAction.RC_TRANSFER_ITEM_LIST,obj);
        }
        obj = request.getSession().getAttribute(AddToBasketAction.SC_TRANSFER_ITEM);
        if (obj!=null) {
            request.setAttribute(AddToBasketAction.RC_TRANSFER_ITEM, obj);
        }

        // reset the logical forward for the addtobasketaction
        obj = request.getSession().getAttribute(AddToBasketAction.SC_FORWARD_NAME);
        if (obj!=null) {
            request.getSession().removeAttribute(AddToBasketAction.SC_FORWARD_NAME);
            request.setAttribute(AddToBasketAction.SC_FORWARD_NAME, obj);
        }
		log.exiting();
        return mapping.findForward(forwardTo);
    }

}