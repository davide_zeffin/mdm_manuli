/**
 * Class         CreateBasketAction Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
 * Description:  Action for the Profile Maintenance Author: Created:      13 Maerz 2001 Version:      1.0 $Revision:
 * #8 $ $Date: 2002/10/01 $
 */
package com.sap.isa.isacore.action.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.backend.boi.isacore.order.DocumentListFilterData;
import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.businessobject.BORuntimeException;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.ConnectedDocument;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.ShipTo;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businessobject.item.ItemList;
import com.sap.isa.businessobject.order.Basket;
import com.sap.isa.businessobject.order.CampaignList;
import com.sap.isa.businessobject.order.CampaignListEntry;
import com.sap.isa.businessobject.order.OrderStatus;
import com.sap.isa.businessobject.order.PartnerList;
import com.sap.isa.businessobject.order.PartnerListEntry;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.businesspartner.businessobject.EndCustomer;
import com.sap.isa.businesspartner.businessobject.SoldTo;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.action.ActionConstants;
import com.sap.isa.isacore.action.IsaCoreInitAction;


/**
 * Create and display the basket.
 *
 * @author Steffen Mueller
 * @version 0.1
 *
 * @deprecated 1. Oktober 2002
 */
public class CreateBasketAction extends CreateSalesDocumentBase {
    static final private IsaLocation log = IsaLocation.getInstance(CreateBasketAction.class.getName());
    private final static String FORWARD_SUCCESS = "createbasket";
    private static final String FORWARD_SUCCESS_CROSSENTRY = "createbasketcrossentry";
    private static final String MESSAGE_PAGE = "message";
    private static final String ERROR_PAGE = "error";    

    /**
     * DOCUMENT ME!
     */
    public void initialize() {
        if (isB2C()) {
            this.checkUserIsLoggedIn = false;
        }
    }

    /**
     * Overriden  <em> isaPerform </em>  method of  <em> IsaCoreBaseAction </em> .
     *
     * @param mapping Description of Parameter
     * @param form Description of Parameter
     * @param request Description of Parameter
     * @param response Description of Parameter
     * @param userSessionData Description of Parameter
     * @param requestParser Description of Parameter
     * @param bom Description of Parameter
     * @param log Description of Parameter
     * @param startupParameter Description of Parameter
     * @param eventHandler Description of Parameter
     *
     * @return Description of the Returned Value
     *
     * @exception CommunicationException Description of Exception
     * @throws PanicException DOCUMENT ME!
     */
    public ActionForward isaPerform(ActionMapping                      mapping,
                                    ActionForm                         form,
                                    HttpServletRequest                 request,
                                    HttpServletResponse                response,
                                    UserSessionData                    userSessionData,
                                    RequestParser                      requestParser,
                                    BusinessObjectManager              bom,
                                    IsaLocation                        log,
                                    IsaCoreInitAction.StartupParameter startupParameter,
                                    BusinessEventHandler               eventHandler)
                             throws CommunicationException {
                                 
        final String METHOD_NAME = "isaPerform()";
        log.entering(METHOD_NAME);

        // Page that should be displayed next.
        String forwardTo = null;

        Shop shop = bom.getShop();

        //get user
        User user = bom.getUser();

        if (user == null) {
            log.exiting();

            return mapping.findForward("login");
        }

        String doctype = null;

        if (shop.isDocTypeLateDecision()) {
            doctype = DocumentListFilterData.SALESDOCUMENT_TYPE_ORDERTEMPLATE;
        }
        else {
            doctype = DocumentListFilterData.SALESDOCUMENT_TYPE_ORDER;
        }

        //Check if user has permission for Order Creation
        Boolean perm = user.hasPermission(doctype, DocumentListFilterData.ACTION_CREATE);

        if (!perm.booleanValue()) {
            log.exiting();
            throw new CommunicationException("User has no permission to Create " + doctype);
        }

        DocumentHandler documentHandler = (DocumentHandler) userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);

        if (documentHandler == null) {
            log.exiting();
            throw new PanicException("No document handler found");
        }

        // check the given sold to id. <br>
        if (!checkSoldToParameter(bom.getBUPAManager(), requestParser, userSessionData, shop)) {
            return mapping.findForward("message");
        }

        // Ask the business object manager to create a basket.
        bom.releaseBasket();

        Basket basket = bom.createBasket();

        basket.clearMessages();

        CampaignListEntry campaignListEntry = null;

        // Setting of the campaignKey if existing
        if ((startupParameter != null) && (startupParameter.getCampaignKey().length() > 0)) {
            String campaignKey = startupParameter.getCampaignKey();
            basket.setCampaignKey(campaignKey);
            basket.setCampaignObjectType(startupParameter.getCampaignObjectType());
            campaignListEntry = basket.getHeader().getAssignedCampaigns().createCampaign();
            campaignListEntry.setCampaignGUID(new TechKey(campaignKey));
            campaignListEntry.setCampaignTypeId(startupParameter.getCampaignObjectType());
        }

        // read catalog
        WebCatInfo webCat = getCatalogBusinessObjectManager(userSessionData).getCatalog();

        // set the Id in the CampaignListEntry if allowed and existing
        campaignListEntry = setCampaign(webCat, basket.getHeader(), campaignListEntry, shop);

        try {
            executeCreateBasket(basket, shop, webCat, bom, request.getAttribute("lead2Basket") != null,
                                request.getAttribute("opp2Basket") != null,
                                request.getAttribute("activity2Basket") != null,
                                request.getAttribute("soldToIsSelectable") != null,
                                (String) request.getAttribute("BupaKey"), userSessionData, requestParser, campaignListEntry);
        } catch (BORuntimeException BORex) {
            documentHandler.release(basket);
            boolean justExc = createErrorMessage(request, documentHandler, "/b2b/updatedocumentview.do", "", basket, BORex);
            if (justExc) {
                return mapping.findForward(ERROR_PAGE);
            } else {
                return mapping.findForward(MESSAGE_PAGE);
            }

        }

        // may throw CommunicationException
        userSessionData.setAttribute(MaintainBasketBaseAction.SC_DOCTYPE, MaintainBasketBaseAction.DOCTYPE_BASKET);

        // Store the list of ShipTos in session context, to allow all frames access to
        // it
        userSessionData.setAttribute(MaintainBasketBaseAction.SC_SHIPTOS, basket.getShipTos());

        // search for a given forward in request context
        RequestParser.Value forwardValue = requestParser.getAttribute(ActionConstants.RA_FORWARD).getValue();

        if (forwardValue.isSet()) {
            forwardTo = forwardValue.getString();
        }
        else {
            if (request.getAttribute("BasketCrossEntry") != null) {
                forwardTo = FORWARD_SUCCESS_CROSSENTRY;
            }
            else {
                forwardTo = FORWARD_SUCCESS;
            }
        }

        String basketKey = null;

        if (shop.isDocTypeLateDecision()) {
            basketKey = "basket.b2r";
        }
        else {
            basketKey = "basket";
        }

        ManagedDocument mDoc = new ManagedDocument(basket, basketKey, "new", null, null, null, "basket", null, null);
        documentHandler.add(mDoc);
        documentHandler.setOnTop(basket);
        log.exiting();

        return mapping.findForward(forwardTo);
    }

    /**
     * Create the basket and fill Businesspartner list.
     *
     * @param basket
     * @param shop
     * @param webCat
     * @param bom Isacore Business
     * @param lead2Basket
     * @param opp2Basket DOCUMENT ME!
     * @param activity2Basket DOCUMENT ME!
     * @param soldToIsSelectable
     * @param bupaKey
     * @param userSessionData DOCUMENT ME!
     * @param requestParser DOCUMENT ME!
     * @param campaignListEntry DOCUMENT ME!
     *
     * @throws CommunicationException DOCUMENT ME!
     */
    public void executeCreateBasket(Basket                basket,
                                    Shop                  shop,
                                    WebCatInfo            webCat,
                                    BusinessObjectManager bom,
                                    boolean               lead2Basket,
                                    boolean               opp2Basket,
                                    boolean               activity2Basket,
                                    boolean               soldToIsSelectable,
                                    String                bupaKey,
                                    UserSessionData       userSessionData,
                                    RequestParser         requestParser,
                                    CampaignListEntry     campaignListEntry)
                             throws CommunicationException {
        // Ask the business object manager for a user. If this object is not
        // already present, the the next action will to login
        User user = bom.getUser();
        BusinessPartnerManager bupama = bom.createBUPAManager();

        String compPartnerFunction = shop.getCompanyPartnerFunction();

        // process type used for creating the basket
        RequestParser.Parameter processType = null;
        String processTypeValue = "";

        // initialisation of the basket
        PartnerList pList = createPartnerList(bupama, compPartnerFunction, requestParser);

        //when called from lead2BasketAction
        if (lead2Basket || opp2Basket || activity2Basket) {
            transferPreDocData2Basket(basket, bom, shop, user, webCat, bupama, compPartnerFunction, lead2Basket,
                                      opp2Basket, activity2Basket, userSessionData);

            //		   set contact info, but only, if we are not in the reseller or agent scenario
            BusinessPartner buPa = bupama.getDefaultBusinessPartner(PartnerFunctionData.CONTACT);

            if ((!compPartnerFunction.equals(PartnerFunctionData.RESELLER)) &&
                    (!compPartnerFunction.equals(PartnerFunctionData.AGENT)) &&
                    (buPa != null)) {
                basket.getHeader().getPartnerList().setPartner(PartnerFunctionData.CONTACT,
                                                               new PartnerListEntry(buPa.getTechKey(), buPa.getId()));
            }

            //	  add responsible at partner in reseller scenario
            if (compPartnerFunction.equals(PartnerFunctionData.RESELLER)) {
                BusinessPartner bupa1 = bupama.getDefaultBusinessPartner(PartnerFunctionData.RESP_AT_PARTNER);
                basket.getHeader().getPartnerList().setPartner(PartnerFunctionData.RESP_AT_PARTNER,
                                                               new PartnerListEntry(bupa1.getTechKey(), bupa1.getId()));
            }

            basket.update(bupama);
        }
        else { // not called from lead2basket, standard behaviour

            //determine the transaction type
            if (shop.isDocTypeLateDecision()) {
                // in case of late decision take first process type in order to create basket
                ResultData procTypes = shop.getProcessTypes();

                if (procTypes.first()) {
                    processTypeValue = procTypes.getString(1);
                }
            }
            else {
                processType = requestParser.getParameter("processtype");

                if (!processType.isSet()) {
                    processType = requestParser.getAttribute("processtype");
                }

                processTypeValue = processType.isSet() ? processType.getValue().getString() : "";
            }

            if (log.isDebugEnabled()) {
                log.debug("Process type: " + processTypeValue);
            }

            basket.init(shop, bupama, pList, webCat, processTypeValue, campaignListEntry);

            // if soldto is to be filled the soldToSelectable request param has to be set
            // that is when called from bupa search result list, or from account2Basket-Action
            if (soldToIsSelectable) {
                String currentBupa = bupaKey;

                TechKey key = new TechKey(currentBupa);
                BusinessPartner partner = bupama.getBusinessPartner(key);

                // when in on-behalf scenario: set transferred account as soldto
                // when in on-stock scenario: set transferred account as endcustomer
                if (compPartnerFunction.equals(PartnerFunctionData.RESELLER) ||
                        compPartnerFunction.equals(PartnerFunctionData.AGENT)) {
                    //set soldto information in partner list
                    basket.getHeader().getPartnerList().setPartner(PartnerFunctionData.SOLDTO,
                                                                   new PartnerListEntry(partner.getTechKey(),
                                                                                        partner.getId()));

                    //set soldto information in bupa manager
                    SoldTo soldto = new SoldTo();
                    bupama.addPartnerFunction(partner, soldto);

                    //set shipTo information
                    basket.deleteShipTosInBackend();
                    basket.clearShipTos();
                    basket.readShipTos();

                    //add ship to address only if partner is a reseller
                    if (compPartnerFunction.equals(PartnerFunctionData.RESELLER)) {
                        BusinessPartner reseller = bupama.getDefaultBusinessPartner(PartnerFunctionData.RESELLER);
                        reseller.getAddress();
                        basket.addNewShipTo(reseller.getAddress(), partner.getTechKey(), shop.getTechKey(),
                                            reseller.getId());
                    }

                    if (basket.getNumShipTos() > 0) {
                        basket.getHeader().setShipTo(basket.getShipTo(0));
                    }
                    else {
                        basket.getHeader().setShipTo(new ShipTo());
                    }
                }
                else { // on stock scenario
                    //set endcustomer information in partner list
                    basket.getHeader().getPartnerList().setPartner(PartnerFunctionData.END_CUSTOMER,
                                                                   new PartnerListEntry(partner.getTechKey(),
                                                                                        partner.getId()));

                    //set endcustomer information in bupa manager
                    EndCustomer endcustomer = new EndCustomer();
                    bupama.addPartnerFunction(partner, endcustomer);
                }

                basket.update(bupama);
            }
        }
         //called from lead2basket

        // not needed here
        basket.readShipTos();
        //basket.update(bupama);
    }

    /**
     * Fills the basket with the data that is contained in the orderstatus object
     *
     * @param basket DOCUMENT ME!
     * @param bom DOCUMENT ME!
     * @param shop DOCUMENT ME!
     * @param user DOCUMENT ME!
     * @param webCat DOCUMENT ME!
     * @param bupama DOCUMENT ME!
     * @param compPartnerFunction DOCUMENT ME!
     * @param lead2Basket DOCUMENT ME!
     * @param opp2Basket DOCUMENT ME!
     * @param activity2Basket DOCUMENT ME!
     * @param userSessionData DOCUMENT ME!
     *
     * @throws CommunicationException DOCUMENT ME!
     */
    private void transferPreDocData2Basket(Basket                 basket,
                                           BusinessObjectManager  bom,
                                           Shop                   shop,
                                           User                   user,
                                           WebCatInfo             webCat,
                                           BusinessPartnerManager bupama,
                                           String                 compPartnerFunction,
                                           boolean                lead2Basket,
                                           boolean                opp2Basket,
                                           boolean                activity2Basket,
                                           UserSessionData        userSessionData)
                                    throws CommunicationException {
        OrderStatus orderStatus = bom.getOrderStatus();
        SalesDocument order = (SalesDocument) orderStatus.getOrder();

        // explicit copying of items is necessary
        ItemList itemList = orderStatus.getItemList();
        order.setItems(itemList);

        String id = orderStatus.getOrderHeader().getSalesDocNumber();
        order.getHeader().setSalesDocNumber(id);

        // set campaign information
        CampaignList campList = orderStatus.getOrderHeader().getAssignedCampaigns();

        if (campList != null) {
            order.getHeader().setAssignedCampaigns(campList);
        }

        //set predecessor document information
        String orderId1 = order.getHeader().getSalesDocNumber();
        ConnectedDocument conDoc1 = new ConnectedDocument();
        conDoc1.setDocNumber(orderId1);
        conDoc1.setTechKey(order.getHeader().getTechKey());

        if (lead2Basket) {
            conDoc1.setDocType(HeaderData.DOCUMENT_TYPE_LEAD);
        }

        if (opp2Basket) {
            conDoc1.setDocType(HeaderData.DOCUMENT_TYPE_OPPORTUNITY);
        }

        if (activity2Basket) {
            //tbd: new Doctype for Activities...
            conDoc1.setDocType(HeaderData.DOCUMENT_TYPE_ACTIVITY);
        }

        order.getHeader().addPredecessor(conDoc1);

        //initialize basket with data from predecessor Document
        basket.init(order, shop, bupama, webCat);

        // if endcustomer is there, get endcustomer first, if not get prospect		        				
        PartnerList partnerList = orderStatus.getOrderHeader().getPartnerList();

        determinePartnerFunctionsFromPreDoc(partnerList, compPartnerFunction, bupama, basket, user, shop,
                                            userSessionData);

        basket.update(bupama);
    }

    /**
     * DOCUMENT ME!
     *
     * @param partnerList
     * @param compPartnerFunction
     * @param bupama DOCUMENT ME!
     * @param basket DOCUMENT ME!
     * @param user DOCUMENT ME!
     * @param shop DOCUMENT ME!
     * @param userSessionData DOCUMENT ME!
     *
     * @throws CommunicationException DOCUMENT ME!
     */
    private void determinePartnerFunctionsFromPreDoc(PartnerList            partnerList,
                                                     String                 compPartnerFunction,
                                                     BusinessPartnerManager bupama,
                                                     Basket                 basket,
                                                     User                   user,
                                                     Shop                   shop,
                                                     UserSessionData        userSessionData)
                                              throws CommunicationException {
        BusinessPartner prospect = null; // stores the bupa in partner function prospect from the predecessor document
        BusinessPartner endCustomer = null; // stores the bupa in partner function endcustomer from the predecessor document
        BusinessPartner reseller = null;
        boolean isOnStockLead; // is the lead on behalf or on stock? true if the reseller is as prospect in the lead, false otherwise 

        // initialize some constant objects that are needed below
        SoldTo soldToPF = new SoldTo();
        EndCustomer endCustomerPF = new EndCustomer();

        // determine bupa in pf prospect from predecessor document 
        PartnerListEntry entry = partnerList.getPartner(PartnerFunctionData.SALES_PROSPECT);

        if (entry != null) {
            TechKey key = entry.getPartnerTechKey();
            prospect = bupama.getBusinessPartner(key);
            prospect.getAddress();
        }
        else {
            // set marker to raise error message in showbasketaction later					   
            userSessionData.setAttribute("preDocError", "X");

            return;
        }

        // determine bupa in pf endcustomer from predecessor document 		
        PartnerListEntry endCustEntry = partnerList.getPartner(PartnerFunctionData.END_CUSTOMER);

        if (endCustEntry != null) {
            TechKey key = endCustEntry.getPartnerTechKey();
            endCustomer = bupama.getBusinessPartner(key);
            endCustomer.getAddress();
        }

        PartnerListEntry salesPartnerEntry = partnerList.getPartner(PartnerFunctionData.RESELLER);

        if (salesPartnerEntry != null) {
            TechKey key = salesPartnerEntry.getPartnerTechKey();
            reseller = bupama.getBusinessPartner(key);
            reseller.getAddress();
        }

        //		error in prospect?
        if (prospect != null) {
            if ((prospect.getId() != null) && (prospect.getId().length() > 0)) {
                //ok
            }
            else {
                prospect = null;
            }
        }

        //		error in endcustomer?
        if (endCustomer != null) {
            if ((endCustomer.getId() != null) && (endCustomer.getId().length() > 0)) {
                //ok
            }
            else {
                endCustomer = null;
            }
        }

        if (prospect != null) {
            // determine if the lead is on behalf or not
            //BusinessPartner reseller = bupama.getDefaultBusinessPartner(PartnerFunctionData.RESELLER);
            if (reseller != null && reseller.getTechKey().equals(prospect.getTechKey())) { // note 1164472
                isOnStockLead = true;
            }
            else {
                isOnStockLead = false;
            }

            //is scenario on behalf or on stock?
            if (compPartnerFunction.equals(PartnerFunctionData.RESELLER)) {
                // is it a on stock lead? (that is the prospect the sales partner?)
                if (isOnStockLead) {
                    // on stock lead
                    // is the partner function endcustomer available,
                    // if yes, take the endcustomer as soldto, else raise error.
                    if (endCustomer != null) {
                        //set soldto information in partner list
                        basket.getHeader().getPartnerList().setPartner(PartnerFunctionData.SOLDTO,
                                                                       new PartnerListEntry(endCustomer.getTechKey(),
                                                                                            endCustomer.getId()));
                        //set soldto information in bupa manager					
                        bupama.addPartnerFunction(endCustomer, soldToPF);
                    }
                    else {
                        // set marker to raise error message in showbasketaction later					   
                        userSessionData.setAttribute("preDocError", "X");

                        return;
                    }
                }
                else { // on behalf lead, prospect not equal to reseller, use prospect as soldto

                    if (prospect != null) {
                        basket.getHeader().getPartnerList().setPartner(PartnerFunctionData.SOLDTO,
                                                                       new PartnerListEntry(prospect.getTechKey(),
                                                                                            prospect.getId()));
                        bupama.addPartnerFunction(prospect, soldToPF);
                    }
                    else {
                        //set marker to raise error message in showbasketaction later					   
                        userSessionData.setAttribute("preDocError", "X");

                        return;
                    }
                }

                //set sales partner  
                // set sales partner information
                BusinessPartner defreseller = bupama.getDefaultBusinessPartner(PartnerFunctionData.RESELLER);
                basket.getHeader().getPartnerList().setPartner(PartnerFunctionData.RESELLER,
                                                               new PartnerListEntry(defreseller.getTechKey(),
                                                                                    defreseller.getId()));

                // set shipto information
                basket.deleteShipTosInBackend();
                basket.clearShipTos();
                basket.readShipTos();

                basket.addNewShipTo(defreseller.getAddress(), defreseller.getTechKey(), shop.getTechKey(),
                                    defreseller.getId());

                if (basket.getNumShipTos() > 0) {
                    basket.getHeader().setShipTo(basket.getShipTo(0));
                }
                else {
                    basket.getHeader().setShipTo(new ShipTo());
                }
            }
            else { // compPartnerFunction not Reseller -> on stock scenario

                if (isOnStockLead) {
                    // if an endcustomer is there, set it, if not set no endcustomer				
                    if (endCustomer != null) {
                        basket.getHeader().getPartnerList().setPartner(PartnerFunctionData.END_CUSTOMER,
                                                                       new PartnerListEntry(endCustomer.getTechKey(),
                                                                                            endCustomer.getId()));
                        bupama.addPartnerFunction(endCustomer, endCustomerPF);
                    }
                }
                else { //on behalf lead

                    if (prospect != null) {
                        //use the prospect as endcustomer, but display a warning if there is also a different endcustomer				  			  
                        basket.getHeader().getPartnerList().setPartner(PartnerFunctionData.END_CUSTOMER,
                                                                       new PartnerListEntry(prospect.getTechKey(),
                                                                                            prospect.getId()));
                        bupama.addPartnerFunction(prospect, endCustomerPF);

                        // is there a different endcustomer			  
                        if (endCustomer != null) {
                            if (endCustomer.getTechKey().equals(prospect.getTechKey())) {
                                // do nothing
                            }
                            else {
                                // display warning
                            }
                        }
                    }
                    else {
                        log.error("No access to prospect in predecessor document");
                    }
                }
            }
        }
        else {
            userSessionData.setAttribute("preDocError", "X");

            return;
        }
    }
}
