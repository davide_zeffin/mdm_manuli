/*****************************************************************************
    Class:        CustomerDocumentListAction
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author        Ralf Witt
    Created:      March 2001

    $Revision: #2 $
    $Date: 2001/07/18 $
*****************************************************************************/

package com.sap.isa.isacore.action.order;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.order.DocumentListFilter;
import com.sap.isa.businessobject.order.OrderStatus;
import com.sap.isa.businessobject.order.PartnerList;
import com.sap.isa.businessobject.order.PartnerListEntry;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.businesspartner.businessobject.PartnerFunctionBase;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.actionform.order.DocumentListSelectorForm;

/**
 * Manage the document Selection for sales documents.
 *
 * @author  SAP
 * @version 1.0
 */
public class CustomerDocumentListAction extends DocumentListBaseAction {

        /**
         * Handle sales document.
         *
         * @see com.sap.isa.isacore.action.order.DocumentListBaseAction#performSalesOrder(BusinessObjectManager, DocumentListSelectorForm, HttpServletRequest, RequestParser, IsaLocation)
         */
        public ActionForward performSalesOrder(ActionMapping mapping,
                        BusinessObjectManager bom,
                        DocumentListSelectorForm documentListSelectorForm,
                        DocumentListFilter filter,
                        Shop shop,
                        boolean performSearch,
                        HttpServletRequest request,
                        UserSessionData userSessionData,
                        RequestParser requestParser,
                        IsaLocation log)
                        throws CommunicationException {

		final String METHOD_NAME = "performSalesOrder()";
		log.entering(METHOD_NAME);
        OrderStatus orderList = bom.getCustomerOrderStatus();
        
        if (performSearch) {	

			if (orderList == null) {
                orderList = bom.createCustomerOrderStatus();    
			}    
			
        	// handle the partner list in this case
        	PartnerList partnerList = orderList.getPartnerList();

        	// get soldfrom from BuPaMa
        	BusinessPartnerManager buPaMa = bom.createBUPAManager();

        	BusinessPartner buPa = buPaMa.getDefaultBusinessPartner(PartnerFunctionBase.SOLDFROM);

        	// add reseller to the partner list
        	if (buPa != null) {
            	PartnerListEntry partner = new PartnerListEntry();
            	partner.setPartnerId(buPa.getId());
            	partner.setPartnerTechKey(buPa.getTechKey());
            	partnerList.setPartnerData(PartnerFunctionBase.SOLDFROM, partner);
        	}

        	// add soldto to the partner list
        	String soldto = documentListSelectorForm.getSoldto();

        	soldto = soldto.trim();

	        if (soldto != null && soldto.length() > 0) {
	            PartnerListEntry partner = new PartnerListEntry();
	            partner.setPartnerId(soldto);
	            partnerList.setPartnerData(PartnerFunctionBase.SOLDTO, partner);
	        } else {
	            if (partnerList.getSoldTo() != null) {
	                partnerList.removePartner(PartnerFunctionBase.SOLDTO);
	            }
	        }

            orderList.setRequestedPartnerFunctions(new String[]{PartnerFunctionBase.SOLDTO});

	        // Get Salesdocuments headers
    	    orderList.readOrderHeaders(new TechKey(""), shop, filter);  // NO SOLDTO !!
        // }

		// if (orderList != null ) {
        	request.setAttribute(RK_DOCUMENT_COUNT, new Integer(orderList.getOrderCount()).toString());
            request.setAttribute(RK_ORDER_LIST, orderList);
		}
		log.exiting();
        return mapping.findForward("orderlist");

	}


    /**
     * @see com.sap.isa.isacore.action.order.DocumentListBaseAction#getNameOfListSelectorForm()
     */
    protected String getNameOfListSelectorForm() {
        return "CustomerDocumentListSelectorForm";
    }


}