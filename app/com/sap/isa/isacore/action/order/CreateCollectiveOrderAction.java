/**
 *  Class         CreateCollectiveOrderAction
 *  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
 *  Description:  Action for the Profile Maintenance
 *  Author:
 *  Created:      November 2002
 *  Version:      1.0
 *  $Revision: #8 $
 *  $Date: 2002/10/01 $
 */

package com.sap.isa.isacore.action.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.ShipTo;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businessobject.order.CampaignListEntry;
import com.sap.isa.businessobject.order.CollectiveOrder;
import com.sap.isa.businessobject.order.PartnerList;
import com.sap.isa.businessobject.order.PartnerListEntry;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.businesspartner.businessobject.PartnerFunctionBase;
import com.sap.isa.businesspartner.businessobject.SoldTo;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.action.IsaCoreInitAction;

/**
 *  Create the collective order.
 *
 * @author     SAP
 * @created    November 2002
 * @version    1.0
 * @deprecated never used. Will be deleted in the next release.
 */
public class CreateCollectiveOrderAction extends IsaCoreBaseAction {


    /**
     *  Overriden  <em> isaPerform </em>  method of  <em> IsaCoreBaseAction </em> .
     *
     *@param  mapping                     Description of Parameter
     *@param  form                        Description of Parameter
     *@param  request                     Description of Parameter
     *@param  response                    Description of Parameter
     *@param  userSessionData             Description of Parameter
     *@param  requestParser               Description of Parameter
     *@param  bom                         Description of Parameter
     *@param  log                         Description of Parameter
     *@param  startupParameter            Description of Parameter
     *@param  eventHandler                Description of Parameter
     *@return                             Description of the Returned Value
     *@exception  CommunicationException  Description of Exception
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            BusinessObjectManager bom,
            IsaLocation log,
            IsaCoreInitAction.StartupParameter startupParameter,
            BusinessEventHandler eventHandler)
             throws CommunicationException {

		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        Shop shop = bom.getShop();

        BusinessPartnerManager bupaManager = bom.createBUPAManager();

        // Ask the business object manager to create a order.
        CollectiveOrder order = bom.createCollectiveOrder();

        order.clearMessages();
        
		CampaignListEntry campaignListEntry = null;

        // Setting of the campaignKey if existing
        if ((startupParameter != null) && (startupParameter.getCampaignKey().length() > 0)) {
            String campaignKey = startupParameter.getCampaignKey();
            order.setCampaignKey(campaignKey);
            order.setCampaignObjectType(startupParameter.getCampaignObjectType());
            
			campaignListEntry = order.getHeader().getAssignedCampaigns().createCampaign();
			campaignListEntry.setCampaignGUID(new TechKey(campaignKey));
			campaignListEntry.setCampaignTypeId(startupParameter.getCampaignObjectType());
        }

        // read catalog
        WebCatInfo webCat =
                getCatalogBusinessObjectManager(userSessionData).getCatalog();

        // initialisation of the order
        PartnerList pList = new PartnerList();
        BusinessPartner buPa;

        String compPartnerFunction = shop.getCompanyPartnerFunction();

        buPa = bupaManager.getDefaultBusinessPartner(compPartnerFunction);
        if (buPa != null ) {
            pList.setPartner(compPartnerFunction, new PartnerListEntry(buPa.getTechKey(),buPa.getId()));
        }

        // set contact info, but only, if we are not in the reseller scenario
        buPa = bupaManager.getDefaultBusinessPartner(PartnerFunctionData.CONTACT);
        if (buPa != null ) {
            pList.setContact(new PartnerListEntry(buPa.getTechKey(),buPa.getId()));
        }

        // create the object in the backend.
        order.create(shop, pList, webCat, campaignListEntry); // may throw CommunicationException

        // if soldto is to be filled the soldToSelectable request param has to be set
        // that is when called from bupa search result list, or from account2Basket-Action
         if (request.getAttribute("soldToIsSelectable") != null) {

            String currentBupa = (String) request.getAttribute("BupaKey");

           TechKey key = new TechKey(currentBupa);
           BusinessPartner partner = bupaManager.getBusinessPartner(key);

           //set soldto information in partner list
           order.getHeader().getPartnerList().setPartner(PartnerFunctionData.SOLDTO, new PartnerListEntry(partner.getTechKey(),partner.getId()));

           //set soldto information in bupa manager
           SoldTo soldto = new SoldTo();
           bupaManager.addPartnerFunction(partner,soldto);

           //set shipTo information
           order.deleteShipTosInBackend();
           order.clearShipTos();
           order.readShipTos();

		   if (shop.getCompanyPartnerFunction().equals(PartnerFunctionBase.RESELLER)){	
	           BusinessPartner reseller = bupaManager.getDefaultBusinessPartner(PartnerFunctionData.RESELLER);
    	       reseller.getAddress();
        	   order.addNewShipTo(reseller.getAddress(), partner.getTechKey(), shop.getTechKey(), reseller.getId());
		   }

           if (order.getNumShipTos() > 0) {
               order.getHeader().setShipTo(order.getShipTo(0));
           } else {
               order.getHeader().setShipTo(new ShipTo());
           }

           order.update(bupaManager);

           } 

        order.readShipTos();
		log.exiting();
        return mapping.findForward("success");
    }

}
