/*****************************************************************************
  Class:        MaintainOrderBatchDispatcherAction
  Copyright (c) 2001, SAP AG, Germany, All rights reserved.
  Author:       Daniel Seise
  Created:      25.11.2002
  Version:      1.0

  $Revision: #8 $
  $Date: 2002/11/25 $
*****************************************************************************/

package com.sap.isa.isacore.action.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businessobject.order.OrderChange;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.action.IsaCoreInitAction;

/**
 * Action to dispatch all requests coming from the <code>order.jsp</code> to
 * different (specialized) actions. The decision to which action the control
 * is forwarded to depends on the request parameters set.<br><br>
 *
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>Request parameter</b></td>
 *     <td><b>Value of parameter</b></td>
 *     <td><b>Logical forward used</b></td>
 *   <tr>
 *   <tr><td>cancel</td><td>!= ""</td><td>cancel</td><tr>
 *   <tr><td>setbatch</td><td>!= ""</td><td>setbatch</td><tr>
 * </table>
 * <p>
 * According to the described parameters the corresponding forwards are used.
 * </p>
 *
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>cancel</td><td>return to basekt</td></tr>
 *   <tr><td>setbatch</td><td>the data will be saved befor returning to the basket again</td></tr>
 * </table>
 *
 *
 * @author Daniel Seise
 * @version 1.0
 */
public class MaintainOrderBatchDispatcherAction
	extends MaintainOrderBaseAction {


	private static final String FORWARD_CANCEL     	= "cancel";
    private static final String FORWARD_SETBATCH 	= "setbatch";
    private static final String FORWARD_UPDATEDOC    = "updatedocumentview";

	/**
	 * @see com.sap.isa.isacore.action.order.MaintainOrderBaseAction#orderPerform(HttpServletRequest, HttpServletResponse, HttpSession, UserSessionData, RequestParser, BusinessObjectManager, IsaLocation, StartupParameter, BusinessEventHandler, boolean, boolean, User, Shop, OrderChange, DocumentState, DocumentHandler)
	 */
	protected String orderPerform(
		HttpServletRequest request,
		HttpServletResponse response,
		HttpSession session,
		UserSessionData userSessionData,
		RequestParser parser,
		BusinessObjectManager bom,
		IsaLocation log,
		IsaCoreInitAction.StartupParameter startupParameter,
		BusinessEventHandler eventHandler,
		boolean multipleInvocation,
		boolean browserBack,
		Shop shop,
		OrderChange order,
		DocumentState targetDocument,
		DocumentHandler documentHandler)
		throws CommunicationException {
			
		final String METHOD_NAME = "orderPerform()";
		log.entering(METHOD_NAME);
		boolean isDebugEnabled = log.isDebugEnabled();

        // Page that should be displayed next.
        String forwardTo = null;


        if (browserBack) {
			log.exiting();
            return FORWARD_UPDATEDOC;
        }

         // check for possible cases
        if ((request.getParameter("cancel") != null) &&
            (request.getParameter("cancel").length() > 0)) {

            forwardTo = FORWARD_CANCEL;

            if (isDebugEnabled) {
                log.debug("Requested action 'showonly' dispatching to Action '" + forwardTo + "'");
            }
        }
        else if ((request.getParameter("setbatch") != null) &&
                 (request.getParameter("setbatch").length() > 0)) {

            MaintainOrderBatchBaseAction.parseRequestItems(parser, userSessionData, order, log, false);
            forwardTo = FORWARD_SETBATCH;

            if (isDebugEnabled) {
                log.debug("Requested action 'returnconfig' dispatching to Action '" + forwardTo + "'");
            }
        }
        
		log.exiting();
        return forwardTo;
    }
	

}
