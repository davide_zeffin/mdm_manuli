/*****************************************************************************
    Class         GetCollectiveOrderAction
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP
    Created:      November 2002
    Version:      1.0

    $Revision: #2 $
    $Date: 2001/07/03 $
*****************************************************************************/

package com.sap.isa.isacore.action.order;

import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.order.CollectiveOrder;
import com.sap.isa.businessobject.order.CollectiveOrderStatus;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;

/**
 * Get the actual collective order for the combination contact person and soldto.
 * If no collective order exists the action creates a new collective order.
 *
 * @author SAP
 * @version 1.0
 *
 */
public class GetCollectiveOrderStatusAction extends GetCollectiveOrderBaseAction {

    private static final String FORWARD_SUCCESS = "success";


    /**
     * Overriden <em>isaPerform</em> method of <em>IsaCoreBaseAction</em>.
     */
    public ActionForward isaPerform(ActionMapping mapping,
                                    ActionForm form,
                                    HttpServletRequest request,
                                    HttpServletResponse response,
                                    UserSessionData userSessionData,
                                    RequestParser requestParser,
                                    BusinessObjectManager bom,
                                    IsaLocation log)
            throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        // Ask the business object manager for a user. If this object is not
        // already present, the the next action will to login
        User user = bom.getUser();

        if(user == null) {
			log.exiting();
            return mapping.findForward("login");
        }

        Shop shop = bom.getShop();

        // get the order status
        CollectiveOrderStatus orderList = bom.createCollectiveOrderStatus();

        // get the collective order to lock the combination contact soldto
        // for the selection.
        CollectiveOrder collectiveOrder = bom.createCollectiveOrder();

        getCollectiveOrder(collectiveOrder,
                           orderList,
                           userSessionData,
                           request,
                           bom,
                           user,
                           shop,
                           true);
                           
        Iterator iter = orderList.iterator();                           
        HeaderSalesDocument header= (HeaderSalesDocument) iter.next();                           
                           
        request.setAttribute("objectkey",header.getTechKey());     
        request.setAttribute("object_id","");
        request.setAttribute("objecttype","");
                     
		log.exiting();
        return mapping.findForward(FORWARD_SUCCESS);

    }

}