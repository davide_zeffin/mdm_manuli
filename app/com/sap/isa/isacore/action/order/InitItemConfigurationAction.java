/*****************************************************************************
    Class         InitItemConfigurationAction
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP
    Created:      16.08.2001
    Version:      1.0

    $Revision: #15 $
    $Date: 2001/08/01 $
*****************************************************************************/

package com.sap.isa.isacore.action.order;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;

/**
 * Action for initializing the configuration.
 *
 * @author SAP
 * @version 1.0
 */
public class InitItemConfigurationAction extends InitItemConfigurationBaseAction {

    /**
     * The method use the document on top taken from the document handler for the configuration. <br>
     * 
     * @see com.sap.isa.isacore.action.order.InitItemConfigurationBaseAction#getConfiguratorDoc(com.sap.isa.core.UserSessionData, com.sap.isa.businessobject.BusinessObjectManager)
     */
    public ManagedDocument getConfiguratorDoc(DocumentHandler documentHandler,
    										  UserSessionData userSessionData, 
    										  BusinessObjectManager bom) {

		final String METHOD_NAME = "getConfiguratorDoc()";
		log.entering(METHOD_NAME);
        ManagedDocument onTopDoc = documentHandler.getManagedDocumentOnTop();
        
        // todo: kein docontop da
        ManagedDocument configuratorDoc = new ManagedDocument(onTopDoc.getDocument(),
             onTopDoc.getDocType(), onTopDoc.getDocNumber(), onTopDoc.getRefNumber(),
             onTopDoc.getRefName(), onTopDoc.getDate(), "configuration",
             onTopDoc.getHref(), onTopDoc.getHrefParameter());
		log.exiting();
        return configuratorDoc;
    }


}