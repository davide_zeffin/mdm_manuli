/*****************************************************************************
  Class:        MaintainOrderNewShiptoAction
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:
  Created:      11.12.2001
  Version:      1.0

  $Revision: #8 $
  $Date: 2002/08/21 $
*****************************************************************************/

package com.sap.isa.isacore.action.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.ShipTo;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businessobject.order.OrderChange;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.action.ActionConstants;
import com.sap.isa.isacore.action.IsaCoreInitAction;
import com.sap.isa.isacore.action.ShowShipToAction;

/**
 * Action to add a new ship to to a document.
 * <p>
 * The following forwards are used by this action:
 * </p>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>FORWARD_SHOWBASKET</td><td>return to the document screen if something with the data of that document is not ok</td></tr>
 *   <tr><td>FORWARD_NEW_SHIPTO</td><td>display screen to enter data of the new ship to</td></tr>
 * </table>
 *
 * @author Thomas Smits
 * @version 1.0
 */
public class MaintainOrderNewShiptoAction extends MaintainOrderBaseAction {

	/**
	 * Name of the request parameter for the shipto handle.
	 */
	public static final String SHIPTO_HANDLE = "shipToHandle";
    /**
     * Creates a new instance of this class.
     */
    public MaintainOrderNewShiptoAction() {
    }


    protected String orderPerform(HttpServletRequest request,
                HttpServletResponse response,
                HttpSession session,
                UserSessionData userSessionData,
                RequestParser parser,
                BusinessObjectManager bom,
                IsaLocation log,
                IsaCoreInitAction.StartupParameter startupParameter,
                BusinessEventHandler eventHandler,
                boolean multipleInvocation,
                boolean browserBack,
                Shop shop,
                OrderChange order,
                DocumentState targetDocument,
                DocumentHandler documentHandler)
                    throws CommunicationException {
		final String METHOD_NAME = "orderPerform()";
		log.entering(METHOD_NAME);
        String forwardTo = null;

		updateOrder(parser, userSessionData, order, shop , bom.getBUPAManager(), log, request);

        String itemKey = request.getParameter("newshipto");
        if (itemKey.equals("header")) {
            // determine the address of the shipto of the actual item for display
            ShipTo headShipTo = order.getHeader().getShipTo();
            if (headShipTo != null) {
                request.setAttribute(
                    ActionConstants.RC_ADDRESS,
                    headShipTo.getAddress());
                request.setAttribute(
                    ShowShipToAction.PARAM_SHIPTO_SOURCE,
                    ShowShipToAction.VALUE_SHIPTO_FROM_RC);
				request.setAttribute(MaintainOrderNewShiptoAction.SHIPTO_HANDLE, headShipTo.getHandle());    
            }
            itemKey = "";
        }
        else {
            // determine the address of the shipto of the actual item for display
            ShipTo itemShipTo =
                order.getItems().get(new TechKey(itemKey)).getShipTo();
            if (itemShipTo != null) {
                request.setAttribute(
                    ActionConstants.RC_ADDRESS,
                    itemShipTo.getAddress());
                request.setAttribute(
                    ShowShipToAction.PARAM_SHIPTO_SOURCE,
                    ShowShipToAction.VALUE_SHIPTO_FROM_RC);
                request.setAttribute(
                    ShowShipToAction.RC_SHIPTO_KEY,
                    itemShipTo.getTechKey().getIdAsString());
				request.setAttribute(MaintainBasketNewShiptoAction.SHIPTO_HANDLE, itemShipTo.getHandle());
            }
        }

        request.setAttribute(ShowShipToAction.RC_ITEMKEY, itemKey);
        forwardTo = FORWARD_NEW_SHIPTO;

		log.exiting();
        return forwardTo;
    }
}