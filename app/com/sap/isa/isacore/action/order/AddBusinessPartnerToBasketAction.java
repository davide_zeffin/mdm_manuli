/*****************************************************************************
    Class         AddBusinessPartnerToBasketAction
    Copyright (c) 2000, SAP AG, All rights reserved.
    Description:  Adds the business partner information to the basket
    Author:       SAP AG
    Created:      01.06.2001
    Version:      1.0
*****************************************************************************/
package com.sap.isa.isacore.action.order;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.businessobject.order.Basket;
import com.sap.isa.businessobject.order.PartnerListEntry;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;


/**
 * Adds the business partner information to the basket.
 * <p>
 * In the B2C scenario, the logon of the user is usually delayed. Therefore
 * it might be necessary to create a basket without sold-to and ship-to
 * information. This action adds the information to the basket after the
 * user logged on.
 * </p>
 *
 * @author SAP AG
 * @version 1.0
 *
 */
public class AddBusinessPartnerToBasketAction extends IsaCoreBaseAction {

    /**
     * Adds the business partner information to the basket.
     *
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            BusinessObjectManager bom,
            IsaLocation log) throws CommunicationException {

		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
		// page to be displayed next
        String forwardTo = "main_inner";

        // get business objects
        Shop shop = bom.getShop();
        User user = bom.getUser();
        Basket basket = bom.getBasket();

        if (shop != null && user != null &&
                            user.isUserLogged() &&
                            basket != null) {

			BusinessPartnerManager buPaMa = bom.createBUPAManager();
			BusinessPartner soldTo = buPaMa.getDefaultBusinessPartner(PartnerFunctionData.SOLDTO);
			BusinessPartner contact = buPaMa.getDefaultBusinessPartner(PartnerFunctionData.CONTACT);
           
            if (soldTo != null) {	
				PartnerListEntry soldToEntry = new PartnerListEntry(soldTo.getTechKey(), soldTo.getId());
				basket.getHeader().getPartnerList().setSoldTo(soldToEntry);
            }
			if (contact != null) {	
				PartnerListEntry contactEntry = new PartnerListEntry(contact.getTechKey(), contact.getId());
				basket.getHeader().getPartnerList().setContact(contactEntry);
			}
			basket.addBusinessPartner(shop, user);  // may throw CommunicationException
            basket.updateHeader();
        }
		log.exiting();
        return mapping.findForward(forwardTo);
    }

}