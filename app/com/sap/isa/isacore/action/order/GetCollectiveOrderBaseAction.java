/*****************************************************************************
    Class         GetCollectiveOrderAction
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP
    Created:      November 2002
    Version:      1.0

    $Revision: #2 $
    $Date: 2001/07/03 $
*****************************************************************************/

package com.sap.isa.isacore.action.order;

import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import com.sap.isa.backend.boi.isacore.order.DocumentListFilterData;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.GenericSearch;
import com.sap.isa.businessobject.GenericSearchReturn;
import com.sap.isa.businessobject.GenericSearchSearchCommand;
import com.sap.isa.businessobject.GenericSearchSelectOptionLine;
import com.sap.isa.businessobject.GenericSearchSelectOptions;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.order.CampaignListEntry;
import com.sap.isa.businessobject.order.CollectiveOrder;
import com.sap.isa.businessobject.order.CollectiveOrderStatus;
import com.sap.isa.businessobject.order.PartnerList;
import com.sap.isa.businessobject.order.PartnerListEntry;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.businesspartner.businessobject.PartnerFunctionBase;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.util.table.ExtendedResultSet;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.core.util.table.TableRow;
import com.sap.isa.isacore.action.GenericSearchBaseAction;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.action.IsaCoreInitAction;
import com.sap.isa.ui.uiclass.genericsearch.GenericSearchUIData;

/**
 * Get the actual collective order for the combination contact person and soldto.
 * If no collective order exists the action creates a new collective order.
 *
 * @author SAP
 * @version 1.0
 *
 */
abstract public class GetCollectiveOrderBaseAction extends IsaCoreBaseAction {

    /**
     * Returns the collective order.
     * If the object could not be found the order object will be created.
     * 
     * @param userSessionData
     * @param bom
     * @param user
     * @param shop
     *
     * @return The actual collective order
     */
    public CollectiveOrder getCollectiveOrder(UserSessionData userSessionData,
                                              HttpServletRequest request, 
                                              BusinessObjectManager bom, 
                                              User user, 
                                              Shop shop,
                                              boolean setGData)
            throws CommunicationException {
		final String METHOD_NAME = "getCollectiveOrder()";
		log.entering(METHOD_NAME);	    
        // get the collective order to lock the combination contact soldto
        // for the selection.
        CollectiveOrder collectiveOrder = bom.getCollectiveOrder();
        
        if (collectiveOrder == null || collectiveOrder.getTechKey().isInitial()) {
            
            collectiveOrder = bom.createCollectiveOrder();
            
            // get the order status
            CollectiveOrderStatus orderList = bom.createCollectiveOrderStatus();
        
            getCollectiveOrder(collectiveOrder,
                               orderList,
                               userSessionData,
                               request,
                               bom,
                               user,
                               shop,
                               true);
            
            if (setGData) {
				collectiveOrder.setGData(shop, null);                   
            }
        }
        
		log.exiting();
        return collectiveOrder;
    }


    /**
     * Select the collective order.
     * If <code>createOrder</code> is <code>true</code> the order object would be created
     * if the object could be found.
     *
     * @param collectiveOrder could be null if <code>createOrder</code> is <code>false</code>
     * @param orderList
     * @param userSessionData
     * @param bom
     * @param user
     * @param shop
     * @param createOrder
     *
     */
    protected void getCollectiveOrder(CollectiveOrder collectiveOrder,
                                      CollectiveOrderStatus orderList,
                                      UserSessionData userSessionData,
                                      HttpServletRequest request,
                                      BusinessObjectManager bom,
                                      User user,
                                      Shop shop,
                                      boolean createOrder)
            throws CommunicationException {

        // get partner function for order
        BusinessPartnerManager buPaMa = bom.getBUPAManager();

        // get the contact person
        BusinessPartner partner = buPaMa.getDefaultBusinessPartner(PartnerFunctionBase.CONTACT);


        PartnerList partnerList = new PartnerList();

        PartnerListEntry  partnerListEntry = new PartnerListEntry(partner.getTechKey(), partner.getId());
        partnerList.setPartner(PartnerFunctionBase.CONTACT, partnerListEntry);
        orderList.getPartnerList().setPartner(PartnerFunctionBase.CONTACT, partnerListEntry);

        // get the soldTo
        partner = buPaMa.getDefaultBusinessPartner(PartnerFunctionBase.SOLDTO);
        partnerListEntry = new PartnerListEntry(partner.getTechKey(), partner.getId());
        partnerList.setPartner(PartnerFunctionBase.SOLDTO, partnerListEntry);
        orderList.getPartnerList().setPartner(PartnerFunctionBase.SOLDTO, partnerListEntry);

        // Get Salesdocuments headers
        readCollectiveOrders(bom, userSessionData, request, orderList);
        //orderList.readOrderHeaders(user, shop, new DocumentListFilter());

        int orderCount = orderList.getOrderCount();
        if(orderCount == 0) {
            // no order found create the collective order
			//Check if user has permission for creating orders
			Boolean perm = user.hasPermission( DocumentListFilterData.SALESDOCUMENT_TYPE_ORDER,
									DocumentListFilterData.ACTION_CREATE);
			if (!perm.booleanValue()){
				throw new PanicException ("User has no permission to create Orders" );
			}
            // read catalog to create the object
            WebCatInfo catalog = getCatalogBusinessObjectManager(userSessionData).getCatalog();

            if (createOrder) {

                IsaCoreInitAction.StartupParameter startupParameter =
                        (IsaCoreInitAction.StartupParameter)userSessionData.getAttribute(IsaCoreInitAction.SC_STARTUP_PARAMETER);
                
				CampaignListEntry campaignListEntry = null;                     
                // Setting of the campaignKey if existing
                if ((startupParameter != null) && (startupParameter.getCampaignKey().length() > 0)) {
                    String campaignKey = startupParameter.getCampaignKey();
                    collectiveOrder.setCampaignKey(campaignKey);
                    collectiveOrder.setCampaignObjectType(startupParameter.getCampaignObjectType());
                    
					campaignListEntry = collectiveOrder.getHeader().getAssignedCampaigns().createCampaign();
					campaignListEntry.setCampaignGUID(new TechKey(campaignKey));
					campaignListEntry.setCampaignTypeId(startupParameter.getCampaignObjectType());
                }        

                collectiveOrder.create(shop, partnerList, catalog, campaignListEntry);
                collectiveOrder.readHeader();
                orderList.addOrderHeader(collectiveOrder.getHeader());
				orderList.setOrderHeader(collectiveOrder.getHeader());
            }
        }
        else {

            if(collectiveOrder != null) {
			//Check if user has permission for changing B2B Orders, which is sufficient for Coll Orders
				Boolean perm = user.hasPermission(DocumentListFilterData.SALESDOCUMENT_TYPE_ORDER,
						DocumentListFilterData.ACTION_CHANGE);
				if (!perm.booleanValue()){
					throw new PanicException ("User has no permission to change Orders" );
				}
                Iterator iter = orderList.iterator();
                HeaderSalesDocument header = (HeaderSalesDocument)iter.next();
                collectiveOrder.setHeader(header);
                collectiveOrder.setTechKey(header.getTechKey());
				collectiveOrder.readHeader();
				orderList.setOrderHeader(collectiveOrder.getHeader());
            }

            if(orderCount>1) {
                log.debug("More than one collective order");
            }
        }

    }
    /** 
     * Read the collective orders (should only be one!) according to select options:<br>
     * - document type ORDER<br>
     * - status 'COLLECTIVEORDER' (e.g in CRM this is system status 'I1752'<br>
     * - Business Partners CONTACT and SOLDTO<br>
     */
    protected void readCollectiveOrders(BusinessObjectManager bom,
                                        UserSessionData       userSessionData,
                                        HttpServletRequest    request,
                                        CollectiveOrderStatus orderList) {
        // START GENERIC SEARCH FRAMEWORK INTEGRATION ------------------
        // Regarding names of the request attributes check xcm file generic-searchbackend-config.xml
        
        
        // Create a new Select Option object
        int handle = 1;  // only one selection request
        // Set select options (see method description)
        request.setAttribute(GenericSearchUIData.RC_SEARCHCRITERASCREENNAME, "SearchCriteria_B2B_Sales");
        request.setAttribute("rc_documenttypes","ORDER");
        GenericSearchBaseAction gsba = new GenericSearchBaseAction();
        GenericSearchSelectOptions selOpt = gsba.buildSelectOptions(request, userSessionData, handle);
        selOpt.addSelectOptionLine( 
            new GenericSearchSelectOptionLine(handle, "STAT","CL_CRM_REPORT_SET_STATUS",
                                              "", "", "RAN", "I", "EQ", "COLLECTIVEORDER", ""));
        BusinessPartnerManager buPaMa = bom.getBUPAManager();
        // get the contact person
        BusinessPartner buPa = buPaMa.getDefaultBusinessPartner(PartnerFunctionBase.CONTACT);
        selOpt.addSelectOptionLine( 
            new GenericSearchSelectOptionLine(handle, "PARTNER_NO/1","CL_CRM_REPORT_SET_PARTNER",
                                              "", "0007", "RAN", "I", "EQ", buPa.getId(), ""));
        // get the soldfrom party
        buPa = buPaMa.getDefaultBusinessPartner(PartnerFunctionBase.SOLDFROM);
        selOpt.addSelectOptionLine( 
            new GenericSearchSelectOptionLine(handle, "PARTNER_NO/2","CL_CRM_REPORT_SET_PARTNER",
                                              "", "0001", "RAN", "I", "EQ", buPa.getId(), ""));
        GenericSearch searchRequestHandler = bom.createGenericSearch();
        // Use helper method for parsing (Handle = 1 => only one selection request !)
        GenericSearchSearchCommand collOrderSearchCommand = new GenericSearchSearchCommand(selOpt);
        collOrderSearchCommand.setBackendImplementation(gsba.getBackendImplemenation(handle, selOpt));
        GenericSearchReturn returnData = null;
		orderList.clearOrderHeader();
        try {
            returnData = searchRequestHandler.performGenericSearch(collOrderSearchCommand);
            ExtendedResultSet searchResult = new ResultData(returnData.getDocuments());
        } catch (CommunicationException comEx) {
        	log.debug(comEx.getMessage());
        }
        for (int i = 1; i <= returnData.getDocuments().getNumRows(); i++) {
            HeaderSalesDocument header = new HeaderSalesDocument();
            TableRow row = returnData.getDocuments().getRow(i);
            header.setTechKey(new TechKey(row.getField("GUID").getString()));
            orderList.addOrderHeader(header);
        }
        // END GENERIC SEARCH FRAMEWORK INTEGRATION ------------------
    }


}