/*****************************************************************************
    Class         ShowBatchAction
    Copyright (c) 2000, SAP AG, Germany, All rights reserved.
    Description:  Action for the Profile Maintenance
    Author:
    Created:      26.11.2002
    Version:      1.0

    $Revision: #8 $
    $Date: 2002/11/26 $
*****************************************************************************/

package com.sap.isa.isacore.action.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.ProductBatch;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.order.CollectiveOrder;
import com.sap.isa.businessobject.order.CustomerOrder;
import com.sap.isa.businessobject.order.Order;
import com.sap.isa.businessobject.ordertemplate.OrderTemplate;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.eai.BackendRuntimeException;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.MessageDisplayer;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

import com.sap.isa.isacore.uiclass.b2b.order.BatchUIInfoContainer;

/**
 * Reread the data for the batch from the backend to allow
 * a frame tolerant access for the JSP.
 *
 * @author Daniel Seise
 * @version 1.0
 *
 */
public class ShowOrderBatchAction extends IsaCoreBaseAction {
	
	public static final String FORWARD_SUCCESS = "showorderbatch";

    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser parser,
            BusinessObjectManager bom,
            IsaLocation log) throws CommunicationException {

		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        // Page that should be displayed next.
        String forwardTo = "showorderbatch";

        
        DocumentHandler documentHandler = (DocumentHandler)
                userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
        DocumentState docstate = documentHandler.getManagedDocumentOnTop();
         // haben wir eins?
        if (docstate == null) {
           // use global forward to show valid frame
           // this if statement is also reached, if the readForUpdate below failed
           // and the message.jsp was displayed and the user clicks on the back icon
           // of the browser, because in this case the last executed action, which is this action,
           // is recalled. But in that case, the managed document was already deleted
		   log.exiting();
            return mapping.findForward("updatedocumentview");
        }
        ManagedDocument mandoc = (ManagedDocument) docstate;
        docstate = mandoc.getDocument();
        SalesDocument salesDoc = (SalesDocument) docstate;
        salesDoc.clearMessages();

        try {
            if (salesDoc instanceof CustomerOrder) {
                forwardTo = mandoc.getForwardMapping();
            }

            if (salesDoc instanceof Order) {
               ((Order) salesDoc).readForUpdate();
            }
            else if (salesDoc instanceof OrderTemplate) {
               ((OrderTemplate) salesDoc).readForUpdate();
            }
            else {
				log.exiting();
               	throw new PanicException("No valid document returned from document handler");
            }
        }
        catch (BackendRuntimeException ex) {

          log.debug("BackendRuntimeException", ex);
            
          documentHandler.release(salesDoc);

          // create a Message Displayer for the error page!
          MessageDisplayer messageDisplayer = new MessageDisplayer();

          messageDisplayer.addToRequest(request);
          messageDisplayer.copyMessages(salesDoc);


          // the action which is to call after the message
          if (salesDoc instanceof CustomerOrder) {
              messageDisplayer.setAction("customerdocumentstatusdetailprepare.do");
          }
          else if (salesDoc instanceof CollectiveOrder) {
              messageDisplayer.setAction("collectivedocumentstatusdetailprepare.do");
          }
          else {
              messageDisplayer.setAction("documentstatusdetailprepare.do");
          }

          messageDisplayer.setActionParameter("?techkey="+salesDoc.getTechKey().getIdAsString()
                                               +"&objecttype="+mandoc.getDocType()
                                               +"&objects_origin=&object_id=" );
			log.exiting();
          	return mapping.findForward("message");
        }

		ProductBatch productBatch = bom.createProductBatch();
		productBatch.readAllElementsForUpdate(new TechKey((String)userSessionData.getAttribute("batchproductid")));

		// create an InfoContainer that contains all information that are needed to build up the batch.jsp
		BatchUIInfoContainer batchBean = new BatchUIInfoContainer(productBatch, salesDoc.getItem(new TechKey(((String)userSessionData.getAttribute("batchselection")))), true);
		userSessionData.setAttribute("batchbean", batchBean);

		request.setAttribute(MaintainBasketBaseAction.RC_SINGLE_ITEM, salesDoc.getItem(new TechKey((String)userSessionData.getAttribute("batchselection"))));
        
        userSessionData.setAttribute(
                MaintainOrderBaseAction.SC_SHIPTOS, salesDoc.getShipTos());
		log.exiting();
        return mapping.findForward(forwardTo);
    }
}
