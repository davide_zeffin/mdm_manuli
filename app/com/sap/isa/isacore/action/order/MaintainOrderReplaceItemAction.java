/*****************************************************************************
  Class:        MaintainOrderReplaceItemAction
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       SAP
  Created:      18.09.2001
  Version:      1.0

  $Revision: #5 $
  $Date: 2002/08/21 $
*****************************************************************************/

package com.sap.isa.isacore.action.order;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.ShipTo;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.businessobject.order.OrderChange;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.action.IsaCoreInitAction;

/**
 * Action to replace an item of the order with another one during the
 * cross and upselling process.
 * <p>
 * The following forwards are used by this action:
 * </p>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>FORWARD_UPDATEDOCUMENT</td><td>always used</td></tr>
 * </table>
 *
 * @author SAP
 * @version 1.0
 * @deprecated The action will be not used any longer. Because the cua products 
 *               aren't display in item detail action from now. 
 */
public class MaintainOrderReplaceItemAction extends MaintainOrderBaseAction {

    protected String orderPerform(HttpServletRequest request,
                HttpServletResponse response,
                HttpSession session,
                UserSessionData userSessionData,
                RequestParser parser,
                BusinessObjectManager bom,
                IsaLocation log,
                IsaCoreInitAction.StartupParameter startupParameter,
                BusinessEventHandler eventHandler,
                boolean multipleInvocation,
                boolean browserBack,
                Shop shop,
                OrderChange order,
                DocumentState targetDocument,
                DocumentHandler documentHandler)
                    throws CommunicationException {
		final String METHOD_NAME = "orderPerform()";
		log.entering(METHOD_NAME);
        parseRequest(parser, userSessionData, order, log);

        RequestParser.Parameter delete =
                parser.getParameter("delete[]");

        RequestParser.Parameter cancel =
                parser.getParameter("cancel[]");

        int numLines = parser.getParameter("product[]").getNumValues();

        String replaceType = (String)request.getParameter("replacetype");
        boolean itemIsRemoved = false;

        TechKey itemKey = new TechKey((String)request.getParameter("replaceditem"));

        ItemSalesDoc relacedItem = order.getItem(itemKey);

        List cancelItems = new ArrayList(cancel.getNumValues()+1);

        for (int i = 1; i <= numLines; i++) {
            if (cancel.getValue(i).isSet()) {
              TechKey key = new TechKey(cancel.getValue(i).getString());
              cancelItems.add(key);
              if (itemKey.equals(key)) {
                itemIsRemoved = true;
              }
            }
        }

        if (replaceType.equals("cancelable")){
            if (!itemIsRemoved) {
                cancelItems.add(itemKey);
            }
        }

        List deleteItems = new ArrayList(delete.getNumValues()+1);

        for (int i = 1; i <= numLines; i++) {
            if (delete.getValue(i).isSet()) {
              TechKey key = new TechKey(delete.getValue(i).getString());
              deleteItems.add(key);
              if (itemKey.equals(key)) {
                itemIsRemoved = true;
              }
            }
        }

        if (replaceType.equals("deletable")){
            if (!itemIsRemoved) {
                deleteItems.add(itemKey);
            }
        }

        ItemSalesDoc item = null;

        if (replaceType.equals("productchangeable")) {
            relacedItem.setProduct((String)request.getParameter("replacewithproduct"));
        }
        else
            {
            item = new ItemSalesDoc(TechKey.EMPTY_KEY);
            // Set the values into the sales document
            item.setProduct((String)request.getParameter("replacewithproduct"));
            item.setQuantity(relacedItem.getQuantity());
            item.setUnit(relacedItem.getUnit());

            item.setShipTo(ShipTo.SHIPTO_INITIAL);
            item.setText(relacedItem.getText());
            item.setReqDeliveryDate(relacedItem.getReqDeliveryDate());

            item.setProductId(TechKey.EMPTY_KEY);
            item.setPcat(TechKey.EMPTY_KEY);
            item.setContractKey(TechKey.EMPTY_KEY);
            item.setContractItemKey(TechKey.EMPTY_KEY);
        }

        TechKey[] dummy = new TechKey[1];

        // possible customer exit, to parse request
        customerExitParseRequest(parser, userSessionData, order);

        if (deleteItems.size() > 0) {
            order.deleteItems((TechKey[])deleteItems.toArray(dummy));
        }
        if (cancelItems.size() > 0) {
            order.cancelItems((TechKey[])cancelItems.toArray(dummy));
        }

        if (item != null) {
            order.addItem(item);
        }

        order.update(bom.createBUPAManager());            // may throw CommunicationException
        order.clearData();
        order.readForUpdate();

        // set items deletable if document is an ordertemplate
        determineItemsDeletable(order);

        request.setAttribute(RC_HEADER, order.getHeader());
        request.setAttribute(RC_ITEMS, order.getItems());
        request.setAttribute(RC_SHIPCOND, order.readShipCond(shop.getLanguage()));
        request.setAttribute(RC_MESSAGES, order.getMessageList());
		
		log.exiting();
        return FORWARD_UPDATEDOCUMENT;
    }
}