/*****************************************************************************
    Class         ChangeOrderBaseAction
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:
    Created:      November 2002
    Version:      2
*****************************************************************************/
package com.sap.isa.isacore.action.order;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.order.CollectiveOrderStatus;
import com.sap.isa.businessobject.order.CustomerOrderStatus;
import com.sap.isa.businessobject.order.Order;
import com.sap.isa.businessobject.order.OrderStatus;
import com.sap.isa.businessobject.ordertemplate.OrderTemplate;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.backend.boi.isacore.order.DocumentListFilterData;

/**
 * Base class to start the order change process.
 * Use the method <code>setSalesDocument</code> to create a entry for the 
 * <code>DocumentHandler</code>. 
 *
 * @author SAP
 * @version 1.0
 *
 */
abstract public class ChangeOrderBaseAction extends IsaCoreBaseAction {

    // this is used form the updatedocumentview Action 
    // to determine what to display next
    public static String ORDER_CHANGE = "order_change";

    public static String CUSTOMER_ORDER_CHANGE = "customer_order_change";

    /**
     * Set the given sales document in the DocumentHandler.
     * 
     * The method initials the salesdocument and adds it to the document handler. 
     * The follwing sales documents are supported
     * <ul>
     *  <li>{@link com.sap.isa.businessobject.order.Order} </li>
     *  <li>{@link com.sap.isa.businessobject.ordertemplate.OrderTemplate} </li>
     * </ul>
     *  
     * @param docType           type of Document to use in the Document Handler 
     * @param salesDoc          document to add
     * @paran headerSalesDoc    document header, with informations for the document 
     *                          handler 
     * @param bom               Business Object Manager
     * @param userSessionData   Object wrapping the session
     * @param documentHandler   Document Handler to be used
     * @param forward           forward for the document handler
     */
    public void setSalesDocument(String                docType,                                 
                                 SalesDocument         salesDoc,
                                 HeaderSalesDocument   headerSalesDoc,
                                 BusinessObjectManager bom,
                                 UserSessionData       userSessionData,
                                 DocumentHandler       documentHandler,
                                 String                forward)
            throws CommunicationException  {
		final String METHOD_NAME = "setSalesDocument()";
		log.entering(METHOD_NAME);
        Shop shop    = bom.getShop();

        if (shop == null) {
			log.exiting();
           	throw new PanicException("Shop not found in BOM");
        }

        User user  = bom.getUser();

        if (user == null) {
			log.exiting();
          	throw new PanicException("User not found in BOM");
        }
        
        if (salesDoc.isCustomerDocument()) {
			// TODO: Check if user has permission for changing corresponding document
			Boolean perm = user.hasPermission(docType, DocumentListFilterData.ACTION_CHANGE);
						if (!perm.booleanValue()){
								log.exiting();
							  	throw new PanicException ("User has no permission to change " + docType);
						}
        }
		else {
			//Check if user has permission for changing corresponding document
			Boolean perm = user.hasPermission(docType, DocumentListFilterData.ACTION_CHANGE);
			if (!perm.booleanValue()){
					log.exiting();
				  	throw new PanicException ("User has no permission to change " + docType);
			}
		}

        // read catalog
        WebCatInfo webCat =
                getCatalogBusinessObjectManager(userSessionData).getCatalog();


        salesDoc.setState(DocumentState.TARGET_DOCUMENT);

        ManagedDocument mDoc = createManagedDocument(docType, salesDoc, headerSalesDoc, forward);

        documentHandler.add(mDoc);
        documentHandler.setOnTop(salesDoc);
        userSessionData.setAttribute(
                MaintainOrderBaseAction.SC_SHIPTOS, salesDoc.getShipTos());

        if (salesDoc instanceof Order) {

            Order order = (Order)salesDoc;
			// destroy buffers which can contain old information
			order.destroyContent();
            order.setGData(shop, webCat);
			userSessionData.setAttribute(MaintainOrderBaseAction.SC_DOCTYPE, MaintainOrderBaseAction.DOCTYPE_ORDER);
        }
        else if (salesDoc instanceof OrderTemplate) {
            
            OrderTemplate orderTemplate = (OrderTemplate)salesDoc;
			// destroy buffers which can contain old information
			orderTemplate.destroyContent();
            orderTemplate.setGData(shop, webCat);
        }
		log.exiting();
    }


	/**
     * Creates a apropriate manageDocument.
     * 
     * @param docType
     * @param salesDoc
     * @param headerSalesDoc
     * @param forward
     * @return ManagedDocument
     */
    protected ManagedDocument createManagedDocument(
        String docType,
        SalesDocument salesDoc,
        HeaderSalesDocument headerSalesDoc,
        String forward) {
            
        ManagedDocument mDoc = null;
        
        mDoc = new ManagedDocument(salesDoc,
                                   docType,
                                   headerSalesDoc.getSalesDocNumber(),
                                   headerSalesDoc.getPurchaseOrderExt(),
                                   headerSalesDoc.getDescription(),
            //Show CreatedAt Date instead of ChangedAt Date in OrderStatus and OrderChange. According to a request of Cat / Ford and to be consistent within the Application, this was necessary.
            //Involved Developer: Stefan Dendl, Joachim Hartmann, Ralf Witt
                                   headerSalesDoc.getCreatedAt(),
                                   forward,
                                   null,
                                   null);
                                   
        return mDoc;
    }


    /**
     * Get a sales document from the given document.<br>
     * 
     * The method determines the type of the document and returns the corresponding
     * sales document. 
     *
     * @param document          document from the document handler 
     * @param bom               business Object Manager
     * @return the corresponding sales document get from the business object manager
     */
    public SalesDocument getSalesDocument(DocumentState         document,
                                          BusinessObjectManager bom)
            throws CommunicationException  {


        SalesDocument salesDoc = null;

        if (document instanceof CustomerOrderStatus) {
            salesDoc = bom.createCustomerOrder();
        }            
        else if (document instanceof CollectiveOrderStatus) {
            salesDoc = bom.createCollectiveOrder();
        }    
        else if (document instanceof OrderStatus) {

            OrderStatus orderStatus = (OrderStatus)document;        
            String docType = orderStatus.getOrderHeader().getDocumentType();          

            if (docType.equalsIgnoreCase(HeaderSalesDocument.DOCUMENT_TYPE_ORDER) ||
                docType.equalsIgnoreCase(HeaderSalesDocument.DOCUMENT_TYPE_QUOTATION)) {
                   salesDoc = bom.createOrder();
            }
            else if (docType.equalsIgnoreCase(HeaderSalesDocument.DOCUMENT_TYPE_ORDERTEMPLATE)) {
                // Ask the business object manager to get the order
                salesDoc = bom.createOrderTemplate();
            }
        }       

        return salesDoc;
        }


}