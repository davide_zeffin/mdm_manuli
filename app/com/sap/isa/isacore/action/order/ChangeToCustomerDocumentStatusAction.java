/*****************************************************************************
    Class:        ChangeToCustomerDocumentStatusAction
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author        SAP
    Created:      February 2003

    $Revision: #2 $
    $Date: 2001/11/20 $
*****************************************************************************/

package com.sap.isa.isacore.action.order;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.order.OrderStatus;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;

/**
 * Change an extisting orderStatus document to a customer order status and
 * display the new document. <br>
 * 
 * This action take the actual order status from the bom and copies all relevant
 * data to the customer Order Status, which is also taken from bom.
 * Then the customerOrderStatus will be add to the document handler.
 *
 * @author  SAP
 * @version 1.0
 *
 */

public class ChangeToCustomerDocumentStatusAction extends CustomerDocumentStatusDetailPrepareAction {

    /**
     * Constructor.
     * Overwrites <code>href_action</code> and <code>forward</code>
     */
    public ChangeToCustomerDocumentStatusAction() {

		super();

    }


    /**
     * Performs the action.
     * 
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
                                    ActionForm form,
                                    HttpServletRequest request,
                                    HttpServletResponse response,
                                    UserSessionData userSessionData,
                                    RequestParser requestParser,
                                    BusinessObjectManager bom,
                                    IsaLocation log)
    		throws CommunicationException {
    		    
		String externalForward = null;
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);	    
        DocumentHandler documentHandler = 
        		(DocumentHandler)userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);            
     	
		externalForward = (String)request.getAttribute("externalforward");
     	
     	// get the order status object from the bom.
     	OrderStatus orderStatus = bom.getOrderStatus();
     	     
		// get the customerOrderStatus from bom
        OrderStatus customerOrderStatus = getSalesDocumentStatus(request, bom);
        
        // copy the data for the document from the orderStatus to
        // the customer order status
        customerOrderStatus.copyDataFromDocument(orderStatus);     	          	
     	          	
		// add the document to the document handler
		addToDocumentHandler(documentHandler, customerOrderStatus, bom);		     	          	

		
		if (externalForward != null  &&  (! externalForward.equals("")) ) {
			log.exiting();
			return mapping.findForward(externalForward);
		}

		
		log.exiting();
     	return mapping.findForward("success");            
    }

}
