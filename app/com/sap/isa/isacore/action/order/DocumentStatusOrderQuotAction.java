/*****************************************************************************
    Class:        DocumentStatusOrderQuotAction
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP
    Created:      May 2001

    $Revision: #5 $
    $Date: 2001/12/13 $
*****************************************************************************/
package com.sap.isa.isacore.action.order;

import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.order.DocumentListFilter;
import com.sap.isa.businessobject.order.Order;
import com.sap.isa.businessobject.order.OrderStatus;
import com.sap.isa.businessobject.quotation.Quotation;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.History;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.MessageDisplayer;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.action.PaymentActions;

/**
 * Order a existing quotation
 */
public class DocumentStatusOrderQuotAction extends IsaCoreBaseAction {
    /**
     * Signal a different index for Bom order creation
     */
    public static final String SC_QUOT_TO_ORDER = "sc_quottoorder";
    /**
     * Used index for Bom order creation
     */
    public static final int ORDER_INDEX = 7; 

    /**
     * Overriden <em>doPerform</em> method of <em>BaseAction</em>.
     */
    public ActionForward isaPerform(
                ActionMapping mapping,
                ActionForm form,
                HttpServletRequest request,
                HttpServletResponse response,
                UserSessionData userSessionData,
                RequestParser requestParser,
                BusinessObjectManager bom,
                IsaLocation log)
                throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
		// process type used for creating the basket
		RequestParser.Parameter processType = null;
		String processTypeValue = "";  
		processType = requestParser.getParameter("processtype");
		processTypeValue = processType.isSet() ? processType.getValue().getString() : "";  
		     
		if (log.isDebugEnabled()) { log.debug("Process type: " + processTypeValue); }
        OrderStatus orderStatus = bom.getOrderStatus();
        if (orderStatus == null) {
			log.exiting();
            throw new PanicException("status.message.nostatusobject");
        }

        HeaderSalesDocument headerSalesDoc = orderStatus.getOrderHeader();

        if (log.isDebugEnabled()) { 
        	log.debug("--------------------------------------------------------");
			log.debug("techkey: " + headerSalesDoc.getTechKey().getIdAsString().trim());
			log.debug("--------------------------------------------------------");
		}

        Quotation quotation = bom.createQuotation();
        quotation.setTechKey(headerSalesDoc.getTechKey());
        quotation.setHeader(headerSalesDoc);

        // Store docType before switching Quot to Order
        String docType = headerSalesDoc.getDocumentType();

        // create order from quotation
        Shop shop = bom.getShop();
        if (shop == null) {
			log.exiting();
          	throw new PanicException("Shop could not be created in BOM");
        }
        User user = bom.getUser();
        if (user == null) {
			log.exiting();
          	throw new PanicException("User could not be created in BOM");
        }
        Order order = bom.createOrder(ORDER_INDEX);
        if (order == null) {
			log.exiting();
          	throw new PanicException("Order could not be created in BOM");
        }
		order.getHeaderData().setProcessType(processTypeValue); 
		order.getHeaderData().setDocumentType(HeaderData.DOCUMENT_TYPE_ORDER);
        // try to create an order from the quotation
        MessageDisplayer messageDisplayer = null;
        boolean orderSaved = false;
		try {
	           if (shop.getBackend().equals(ShopData.BACKEND_R3_PI)) {                 	
	           	  if ( !shop.isDirectPaymentMaintenanceAllowed()) {
	           	  	// ISA R3: due to technical reasons payment types cannot be maintained 
					  order.createFromQuotation(quotation,shop);
					  orderSaved = true;
	           	  } else {
	           	  	// LORD: payment types maintenance always available 
					  order.createFromQuotation(quotation,shop,false);
	           	  }		     
	           } else  {
	           	  // CRM: in order to retrieve the available payment types, the order must be created first
				  order.createFromQuotation(quotation,shop,false);
				  if (!PaymentActions.isPaymentMaintenanceAvailable(userSessionData, order)) {
				     order.save();
				 	 orderSaved = true;
				  }
	           }
		   }
        
        // care for possible errors (=> error page)
        catch (Exception ex) {
        	log.debug("sending quotation, exception catched: ",ex);
            messageDisplayer = new MessageDisplayer();
        }
        if (!order.isValid()) {
            messageDisplayer = new MessageDisplayer();
        }
        if (messageDisplayer != null) {
            messageDisplayer.addToRequest(request);
            messageDisplayer.addMessage(
                new Message(Message.ERROR,
                            "b2b.quotation.error.order",
                            null,
                            ""));
            Iterator i = order.getMessageList().iterator();
            while (i.hasNext()) {
                log.error(LogUtil.APPS_USER_INTERFACE, (Message)i.next());
            }
            
            // the action which is to call after the message
            messageDisplayer.setAction("/b2b/documentstatusdetailprepare.do");
            messageDisplayer.setActionParameter(
                "?techkey="+quotation.getTechKey().getIdAsString() +
                "&objecttype="+DocumentListFilter.SALESDOCUMENT_TYPE_QUOTATION +
                "&objects_origin=&object_id=" );
            bom.releaseOrder(ORDER_INDEX);
			log.exiting();
            return mapping.findForward("message");
        }
        
        // remove Quotation from History
        DocumentHandler documentHandler = (DocumentHandler)
                userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
        if (documentHandler == null) {
			log.exiting();
            throw new PanicException("No document handler found");
        }

		if (!PaymentActions.isPaymentMaintenanceAvailable(userSessionData, order)) {        
		  if (quotation.getTechKey().equals(order.getTechKey())) {
			History history = documentHandler.getHistory();
			history.remove(docType.trim(),headerSalesDoc.getSalesDocNumber());
		  }
		}

        // prepare confirmation screen
		// First read catalog
		WebCatInfo webCat =
			getCatalogBusinessObjectManager(userSessionData).getCatalog();
		if (webCat != null && orderSaved == true) {
			order.setGData(shop, webCat);
		}
        order.readHeader();               // may throw CommunicationException
        order.readAllItems();         // may throw CommunicationException
        userSessionData.setAttribute(MaintainBasketBaseAction.SC_SHIPTOS, order.getShipTos());
//        request.getSession().setAttribute(MaintainBasketBaseAction.SC_SHIPTOS, order.getShipTos());

        order.setState(DocumentState.VIEW_DOCUMENT);
        ManagedDocument mDoc = new ManagedDocument(order,
										   "basket",
										   headerSalesDoc.getSalesDocNumber(),
										   headerSalesDoc.getPurchaseOrderExt(),
										   headerSalesDoc.getDescription(),
										   //headerSalesDoc.getChangedAt(),  CHN 978390 changed at date is not consistent to other
                                           headerSalesDoc.getCreatedAt(), // application parts (see also class: ChangeOrderBaseAction)
										   "confirmation",
										   null,
										   null);
        documentHandler.add(mDoc);
        documentHandler.setOnTop(order);

		// remove orderstatusdetail from bom & document manager
		if (!PaymentActions.isPaymentMaintenanceAvailable(userSessionData, order)) {
		  bom.releaseOrderStatus();
		  documentHandler.release(orderStatus);
		}  

        userSessionData.setAttribute(SC_QUOT_TO_ORDER, "TRUE"); 
		log.exiting();
                
		if ((!PaymentActions.isPaymentMaintenanceAvailable(userSessionData, order))
			|| (shop.getBackend().equals(ShopData.BACKEND_R3_PI) && !shop.isDirectPaymentMaintenanceAllowed())) {
		   return mapping.findForward("ordersummary");
		} else {
		   // remember where we come from
		   userSessionData.setAttribute(PaymentActions.PAYMENT_PROCESS, PaymentActions.QUOT_ORDERING);    
		   // forward to payment data maintenance
		   return mapping.findForward(PaymentActions.FORWARD_TO_PAYMENT_MAINTAINENCE);
		}
    }
}
