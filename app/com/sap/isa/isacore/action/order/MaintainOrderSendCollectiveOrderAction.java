/*****************************************************************************
  Class:        MaintainOrderSendCollectiveOrderAction
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:
  Created:      December 2002
  Version:      1.0

  $Revision: #8 $
  $Date: 2002/08/21 $
*****************************************************************************/

package com.sap.isa.isacore.action.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businessobject.order.CollectiveOrderChange;
import com.sap.isa.businessobject.order.OrderChange;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.action.IsaCoreInitAction;

/**
 * Action to perform the final step in the processing an collective order.
 * The collective order will be finaly order in the backend.
 * <p>
 * The following forwards are used by this action:
 * </p>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>FORWARD_CHANGEORDER</td><td>something went wrong during the save process</td></tr>
 *   <tr><td>FORWARD_CONFIRMATION_SCREEN</td><td>show confirmation that save was successful</td></tr>
 * </table>
 *
 * @author SAP
 * @version 1.0
 */
public class MaintainOrderSendCollectiveOrderAction extends MaintainOrderBaseAction {

    protected String orderPerform(HttpServletRequest request,
                HttpServletResponse response,
                HttpSession session,
                UserSessionData userSessionData,
                RequestParser parser,
                BusinessObjectManager bom,
                IsaLocation log,
                IsaCoreInitAction.StartupParameter startupParameter,
                BusinessEventHandler eventHandler,
                boolean multipleInvocation,
                boolean browserBack,
                Shop shop,
                OrderChange order,
                DocumentState targetDocument,
                DocumentHandler documentHandler)
                    throws CommunicationException {

		final String METHOD_NAME = "orderPerform()";
		log.entering(METHOD_NAME);
        String forwardTo = null;

        boolean isDebugEnabled = log.isDebugEnabled();

        updateOrder(parser, userSessionData, order, shop, bom.getBUPAManager(), log, request);

        //  order must be read first in order to check validity
        order.readForUpdate();
        
        boolean isInvalid =  !order.isvalid(); 

        if (!(order instanceof CollectiveOrderChange) 
            || (shop.isOnlyValidDocumentAllowed() && isInvalid)) {
            if (isDebugEnabled) {
                log.debug("document NOT valid");
                log.debug("message provided: " + order.getMessageList());
            }
        //    order.readForUpdate(bom.getUser());
            request.setAttribute(RC_HEADER, order.getHeader());
            request.setAttribute(RC_ITEMS, order.getItems());
            request.setAttribute(RC_SHIPCOND, order.readShipCond(shop.getLanguage()));
            request.setAttribute(RC_MESSAGES, order.getMessageList());

            forwardTo = FORWARD_CHANGEORDER;
        }
        else {
           ((CollectiveOrderChange)order).send();
           request.setAttribute(MaintainOrderBaseAction.RC_HEADER, order.getHeader());
           request.setAttribute(MaintainOrderBaseAction.RC_ITEMS, order.getItems());
           request.setAttribute(MaintainOrderBaseAction.RC_SHIPCOND, order.readShipCond(shop.getLanguage()));
           request.setAttribute(MaintainOrderBaseAction.RC_MESSAGES, order.getMessageList());
           userSessionData.setAttribute(SC_SHIPTOS,  order.getShipTos());
           forwardTo = FORWARD_CONFIRMATION_SCREEN;
        }

		log.exiting();
        return forwardTo;
    }
}