/*****************************************************************************
	Class         ShowExtRefObjOrderChangeAction
	Copyright (c) 2004, SAP AG, Germany, All rights reserved.
	Description:  Action for the Profile Maintenance
	Author:       SAP
	Created:      21 Januar 2005
	Version:      0.1

	$Revision: #1 $
	$Date: 2005/01/21 $

*****************************************************************************/
package com.sap.isa.isacore.action.order;

import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.ordertemplate.OrderTemplate;
import com.sap.isa.businessobject.quotation.Quotation;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.action.order.ChangeOrderBaseAction;
import com.sap.isa.isacore.action.order.MaintainBasketBaseAction;

/**
 *  This action forward to the displayextrefobconfirm.jsp
 */
public class ShowExtRefObjOrderChangeAction  extends ChangeOrderBaseAction {


	/**
	 * Implement this method to add functionality to your action.
	 *
	 * @param form              The <code>FormBean</code> specified in the
	 *                          config.xml file for this action
	 * @param request           The request object
	 * @param response          The response object
	 * @param userSessionData   Object wrapping the session
	 * @param requestParser     Parser to simple retrieve data from the request
	 * @param bom               Reference to the BusinessObjectManager
	 * @param log               Reference to the IsaLocation, needed for logging
	 * @return Forward to another action or page
	 */
	public ActionForward isaPerform(ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response,
			UserSessionData userSessionData,
			RequestParser parser,
			BusinessObjectManager bom,
			IsaLocation log) throws CommunicationException {

		// Page that should be displayed next.
		String forwardTo = "success";

		boolean newPos = false;
		int vinHits = 0;

		Shop shp = bom.getShop();
		 if (shp == null) {
		   throw new PanicException("No shop returned from bom");
		 }
		DocumentHandler documentHandler = (DocumentHandler)
				userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);

		ManagedDocument mdoc = documentHandler.getManagedDocumentOnTop();
		DocumentState document = mdoc.getDocument();


		SalesDocument salesDocument = null;
		
		if (document instanceof Quotation){
		   salesDocument = bom.getQuotation();
		} else if (document instanceof OrderTemplate) {
			salesDocument = bom.getOrderTemplate();
		} else {
			salesDocument = bom.getOrder();
		}

		
		if (parser.getParameter("level").getValue().getString().equals("1")) {
			request.setAttribute("level", "1");
		} else {
			request.setAttribute("level", "0");
		}
		
		request.setAttribute(
				MaintainBasketBaseAction.RC_BASKET, salesDocument);

		request.setAttribute(
				MaintainBasketBaseAction.RK_HEADER, salesDocument.getHeader());

		request.setAttribute(
				MaintainBasketBaseAction.RK_ITEMS, salesDocument.getItems());

		int j = 10;
		// auf Itemebene
		if (parser.getParameter("level").getValue().getString().equals("0")) {
//		if (request.getParameter("itemkey") != null && request.getParameter("itemkey").length() > 0) {
			
			String itemKey = parser.getParameter("itemkey").getValue().getString();
			request.setAttribute("itemkey", itemKey);
			
			if (parser.getParameter("linenum").getValue() != null &&
				  parser.getParameter("linenum").getValue().getString().length() > 0) {
					request.setAttribute("linenum", parser.getParameter("linenum").getValue().getString());
			}
			
			if (parser.getParameter("newposvin").getValue() != null && 
				  parser.getParameter("newposvin").getValue().getInt() > 0) {
			      	
					int val[] = getVinCounter(parser, request, j, vinHits);
					j = val[0];
					vinHits = val[1];
					newPos = true;
			}
			
			
			// Order wurde nicht gespeichert, aber Popup wurde wieder geoeffnet
			if (parser.getParameter("vinvalues").getValue() != null &&
				  parser.getParameter("vinvalues").getValue().getString().length() > 0) {
	        	  	
					int val = getVinValues(parser, request, vinHits);
					vinHits = val;
					if (vinHits > j) {
						j = j + vinHits;
					}
					newPos = true;	        	  	
			}
			
			TechKey techKey = new TechKey(itemKey);
			if (salesDocument.getItem(techKey) != null &&
				salesDocument.getItem(techKey).getAssignedExtRefObjects() != null ) {
				vinHits = salesDocument.getItem(techKey).getAssignedExtRefObjects().size();
				if (vinHits > j) {
					j = j + vinHits;
				}						
			}
			
		} else {
			// auf Kopfebene
			if (parser.getParameter("newposvin").getValue() != null && 
				  parser.getParameter("newposvin").getValue().getInt() > 0) {
			      	
					int val[] = getVinCounter(parser, request, j, vinHits);
					j = val[0];
					vinHits = val[1];
					newPos = true;
			}
	        
			// Order wurde nicht gespeichert, aber Popup wurde wieder geoeffnet
			if (parser.getParameter("vinvalues").getValue() != null &&
				  parser.getParameter("vinvalues").getValue().getString().length() > 0) {
	        	  	
					int val = getVinValues(parser, request, vinHits);
					vinHits = val;
					if (vinHits > j) {
						j = j + vinHits;
					}
					newPos = true;	        	  	
			}
	        
			if (salesDocument.getHeader().getAssignedExtRefObjects() != null && 
				salesDocument.getHeader().getAssignedExtRefObjects().size() > 0 &&
				vinHits == 0) {
	        		
					int val = salesDocument.getHeader().getAssignedExtRefObjects().size();
					vinHits = val;
					if (vinHits > j) {
						j = j + vinHits;
					}
			}
                	  	
		}
        
		request.setAttribute("vincounter", String.valueOf(j));
		request.setAttribute("addNewPos", String.valueOf(newPos));
		request.setAttribute("vinhits", String.valueOf(vinHits));

		return mapping.findForward(forwardTo);
	}
    
	protected int[] getVinCounter(RequestParser requestParser, HttpServletRequest request, int j, int vinHits) {
		int value[] = {0,0};
		try {  
			j = Integer.parseInt(requestParser.getParameter("loops").getValue().getString()); 
			for (int i = 0; i < j; i++) {
				String field = "vinNumber_".concat(String.valueOf(i));
				String vin = requestParser.getParameter(field).getValue().getString();
				if (vin != null && vin.length() > 0) {
					request.setAttribute(field, vin);
					vinHits++;
				} else {
					request.setAttribute(field, "");
				}
			}
				
			int k = Integer.parseInt(requestParser.getParameter("newposvin").getValue().getString());
			j = j + k;
			
			value[0] = j;
			value[1] = vinHits;
			
		} catch (NumberFormatException ex) {
			log.debug(ex.getMessage());
		}
		return value;
	}
    
	/**
	 * Read the Vins from the request.
	 * The order is not saved, but the popup is called again.
	 * @param requestParser
	 * @param request
	 * @param vinHits
	 * @return
	 */
	protected int getVinValues(RequestParser requestParser, HttpServletRequest request, int vinHits) {
    	
		StringTokenizer st = new StringTokenizer(
								requestParser.getParameter("vinvalues").getValue().getString(),
								";",
								false);
		int i = 0;
		while (st.hasMoreTokens()) {
			String field = "vinNumber_".concat(String.valueOf(i));
			request.setAttribute(field, st.nextToken());
			vinHits++;
			i++;
		}
		return vinHits;    	
	}
    
}