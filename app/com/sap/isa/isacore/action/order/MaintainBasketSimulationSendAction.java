/*****************************************************************************
  Class:        MaintainBasketSimulationSendAction
  Copyright (c) 2002, SAP AG, All rights reserved.
  Author:       SAP AG
  Created:      21.02.2002
  Version:      1.0

  $Revision: #6 $
  $Date: 2002/11/13 $
*****************************************************************************/

package com.sap.isa.isacore.action.order;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.ConnectedDocument;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businessobject.businessevent.PlaceOrderEvent;
import com.sap.isa.businessobject.lead.Lead;
import com.sap.isa.businessobject.order.Order;
import com.sap.isa.businessobject.ordertemplate.OrderTemplate;
import com.sap.isa.businessobject.quotation.Quotation;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.MessageList;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.action.IsaCoreInitAction;
import com.sap.isa.isacore.action.PaymentActions;

/**
 * Action to save a simulated order in the backend. Only used in case of R3 order and
 * Java Basket Backends.
 * The following forwards are used by this action:
 * </p>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>FORWARD_SUMMARY_ORDER</td><td>show a summary of the ordered products and offer a printout</td></tr>
 *   <tr><td>FORWARD_SUMMARY_QUOTATION</td><td>show a summary of the quotation and offer a printout</td></tr>
 *  <tr><td>FORWARD_SUMMARY_TEMPLATE</td><td>show a summary of the template and offer a printout</td></tr>
 * </table>
 *
 * @author
 * @version 1.0
 */
public class MaintainBasketSimulationSendAction extends MaintainBasketBaseAction {

    public static String FORWARD_SIMULATEBASKET = "simulatebasket";

    /**
     * Creates a new instance of this class.
     */
    public MaintainBasketSimulationSendAction() {
    }


    protected String basketPerform(HttpServletRequest request,
                HttpServletResponse response,
                HttpSession session,
                UserSessionData userSessionData,
                RequestParser parser,
                BusinessObjectManager bom,
                IsaLocation log,
                IsaCoreInitAction.StartupParameter startupParameter,
                BusinessEventHandler eventHandler,
                boolean multipleInvocation,
                boolean browserBack,
                Shop shop,
                SalesDocument preOrderSalesDocument,
                boolean auction,
                DocumentState targetDocument,
                DocumentHandler documentHandler)
                    throws CommunicationException {

		final String METHOD_NAME = "basketPerform()";
		log.entering(METHOD_NAME);
        String forwardTo = null;

        // get the document on top ( the auction document)
        DocumentState doc = documentHandler.getManagedDocumentOnTop();
        if (doc == null) {
			log.exiting();
          	throw new PanicException("No document returned from document handler");
        }

        ManagedDocument mdoc = (ManagedDocument) doc;
        doc = mdoc.getDocument();
        SalesDocument salesDoc = (SalesDocument) doc;

        salesDoc.read();

        if (!salesDoc.isValid()) {
			log.exiting();
            return FORWARD_SIMULATEBASKET;
        }

        // Test for necessary relationship between reseller and soldto and create it if necessary
        checkSoldToResellerRelation(preOrderSalesDocument, shop, bom.createBUPAManager(), true);

        // save the document in the backend
        if (salesDoc instanceof Order) {
        	int retCode = 0;
			if (!PaymentActions.isPaymentMaintenanceAvailable(userSessionData, salesDoc)) {
			  ((Order) salesDoc).saveAndCommit();  // may throw CommunicationException
			} else {
			   retCode = ((Order) salesDoc).saveOrderAndCommit();	
			}
			  
			if (retCode == 0) {
			  
			  salesDoc.readHeader();
			  salesDoc.readAllItems();

			  // fire event
			  eventHandler.firePlaceOrderEvent(
					  new PlaceOrderEvent(bom.getOrder(),bom.getUser()));

			  forwardTo = FORWARD_SUMMARY_ORDER;

			  // to get the confirmation screen working the basket must be added to the document
			  // handler again, replacing the managed document created in the simulation action
			  ManagedDocument mDoc = new ManagedDocument(preOrderSalesDocument,
													 "basket",
													 "new",
													 null,
													 null,
													 null,
													 "basket",
													 null,
													 null);
			  documentHandler.add(mDoc);
			  documentHandler.setOnTop(preOrderSalesDocument);

			  userSessionData.setAttribute(MaintainBasketBaseAction.SC_SHIPTOS, salesDoc.getShipTos());
              setAvailableValueFlagsForJSP(salesDoc, request); 
			} else {
			  // remember where we come from
			 userSessionData.setAttribute(PaymentActions.PAYMENT_PROCESS, PaymentActions.BASKET_SIMULATION);
			 forwardTo = PaymentActions.FORWARD_TO_PAYMENT_MAINTAINENCE;
			 log.exiting();
			 return forwardTo;
			}
        } else {
            
            if (salesDoc instanceof Quotation) {
                ((Quotation) salesDoc).save(shop);
                forwardTo = FORWARD_SUMMARY_QUOTATION;
            }
            else if (salesDoc instanceof OrderTemplate) {
                salesDoc.getHeaderData().setReqDeliveryDate("");
                for (int i = 0; i < salesDoc.getItems().size(); i++) {
                    salesDoc.getItems().get(i).setReqDeliveryDate("");
                }
                ((OrderTemplate) salesDoc).save();
                forwardTo = FORWARD_SUMMARY_TEMPLATE;
            }

            salesDoc.readHeader();
            salesDoc.readAllItems();

            // this is necessary to get the showOrderConfirmedAction working correctly
            mdoc.setState(ManagedDocument.TARGET_DOCUMENT);

            userSessionData.setAttribute(MaintainBasketBaseAction.SC_SHIPTOS, salesDoc.getShipTos());  
            setAvailableValueFlagsForJSP(salesDoc, request);   
        }

		// If the basket is recoverable, delete the basket in the database, it is not needed anymore
		if (bom.getBasket().isDocumentRecoverable()) {
           bom.getBasket().destroyContent();  
		}
        
        if (!(salesDoc instanceof Order)) {
            bom.releaseBasket(); 
        }

		log.exiting();
        return forwardTo;
    }
}