/*****************************************************************************
  Class:        MaintainBasketProcessSoldToAction
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Thomas Smits
  Created:      18.06.2002
  Version:      1.0

  $Revision: #1 $
  $Date: 2002/06/18 $
*****************************************************************************/

package com.sap.isa.isacore.action.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businessobject.order.PartnerListEntry;
import com.sap.isa.businesspartner.action.BupaConstants;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.action.IsaCoreInitAction;

/**
 * Action to display the basket again.
 * <p>
 * The following forwards are used by this action:
 * </p>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>FORWARD_SHOWSSOLDTO</td><td>display soldto detail information</td></tr>
 *   <tr><td>FORWARD_SHOWSSOLDTO</td><td>create a new soldto</td></tr>
 * </table>
 *
 * @author
 * @version 1.0
 */
public class MaintainBasketProcessSoldToAction extends MaintainBasketBaseAction {

    protected static final String FORWARD_SHOWSOLDTO = "showsoldto";
    protected static final String FORWARD_CREATESOLDTO = "createsoldto";

    /**
     * Creates a new instance of this class.
     */
    public MaintainBasketProcessSoldToAction() {
    }


    protected String basketPerform(HttpServletRequest request,
                HttpServletResponse response,
                HttpSession session,
                UserSessionData userSessionData,
                RequestParser parser,
                BusinessObjectManager bom,
                IsaLocation log,
                IsaCoreInitAction.StartupParameter startupParameter,
                BusinessEventHandler eventHandler,
                boolean multipleInvocation,
                boolean browserBack,
                Shop shop,
                SalesDocument preOrderSalesDocument,
                boolean auction,
                DocumentState targetDocument,
                DocumentHandler documentHandler)
                    throws CommunicationException {

		final String METHOD_NAME = "basketPerform()";
		log.entering(METHOD_NAME);
        if (!multipleInvocation) {

            updateSalesDocument(parser, userSessionData, preOrderSalesDocument, shop, bom.createBUPAManager(), log);

            processCustomerNumberFields(parser, preOrderSalesDocument, shop, bom.createBUPAManager());
        }

        String forward = null;

        if (request.getParameter("processsoldto").toLowerCase().equals(FORWARD_SHOWSOLDTO)) {
        	
			PartnerListEntry soldToEntry = preOrderSalesDocument.getHeader().getPartnerList().getSoldTo();

            if (soldToEntry == null || soldToEntry.getPartnerTechKey() == null ||
				soldToEntry.getPartnerTechKey().getIdAsString().length() == 0) {
                forward = FORWARD_SHOWBASKET;
            }
            else {
                forward = FORWARD_SHOWSOLDTO;
                request.setAttribute(BupaConstants.BUPA_TECHKEY, soldToEntry.getPartnerTechKey().getIdAsString());
            }
        }
        else if (request.getParameter("processsoldto").toLowerCase().equals(FORWARD_CREATESOLDTO)) {
            forward = FORWARD_CREATESOLDTO;
        }
		log.exiting();
        return forward;
    }
}