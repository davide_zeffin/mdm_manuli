/*****************************************************************************
  Class:        MaintainOrderExitConfigAction
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Thomas Smits
  Created:      18.09.2001
  Version:      1.0

  $Revision: #3 $
  $Date: 2001/09/25 $
*****************************************************************************/

package com.sap.isa.isacore.action.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businessobject.order.OrderChange;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.action.IsaCoreInitAction;

/**
 * Action to exit a configuration. The data of that configuration is discarded.
 * <br><br>
 * The following forwards are used by this action:
 * </p>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>FORWARD_UPDATEDOCUMENT</td><td>always used</td></tr>
 * </table>
 *
 * @author Thomas Smits
 * @version 1.0
 */
public class MaintainOrderExitConfigAction extends MaintainOrderBaseAction {

    protected String orderPerform(HttpServletRequest request,
                HttpServletResponse response,
                HttpSession session,
                UserSessionData userSessionData,
                RequestParser parser,
                BusinessObjectManager bom,
                IsaLocation log,
                IsaCoreInitAction.StartupParameter startupParameter,
                BusinessEventHandler eventHandler,
                boolean multipleInvocation,
                boolean browserBack,
                Shop shop,
                OrderChange order,
                DocumentState targetDocument,
                DocumentHandler documentHandler)
                    throws CommunicationException {
		final String METHOD_NAME = "orderPerform()";
		log.entering(METHOD_NAME);
        userSessionData.setAttribute(ItemConfigurationBaseAction.SC_CONFIGURATION_ACTIVE, ItemConfigurationBaseAction.CONFIG_ACTIVE_FALSE);
        userSessionData.removeAttribute(ItemConfigurationBaseAction.SC_ITEM_IN_CONFIG);

        ManagedDocument onTopDoc = documentHandler.getManagedDocumentOnTop();
        // todo: kein docontop da
        ManagedDocument orderchangeDoc = new ManagedDocument(onTopDoc.getDocument(),
        onTopDoc.getDocType(), onTopDoc.getDocNumber(), onTopDoc.getRefNumber(),
        onTopDoc.getRefName(), onTopDoc.getDate(), "order_change",
        onTopDoc.getHref(), onTopDoc.getHrefParameter());
        documentHandler.add(orderchangeDoc);
        documentHandler.setOnTop(orderchangeDoc.getDocument());
		log.exiting();
        return FORWARD_UPDATEDOCUMENT;
    }
}