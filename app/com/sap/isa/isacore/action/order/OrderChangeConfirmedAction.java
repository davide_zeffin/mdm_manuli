/*****************************************************************************
    Class         OrderChangeConfirmedAction
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Action for the Profile Maintenance
    Author:
    Created:      23.04.2002
    Version:      0.�
*****************************************************************************/

package com.sap.isa.isacore.action.order;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.businessobject.order.CollectiveOrder;
import com.sap.isa.businessobject.order.CustomerOrder;
import com.sap.isa.businessobject.order.DocumentListFilter;
import com.sap.isa.businessobject.order.Order;
import com.sap.isa.businessobject.order.OrderStatus;
import com.sap.isa.businessobject.ordertemplate.OrderTemplate;
import com.sap.isa.core.ContextConst;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.interaction.InteractionConfig;
import com.sap.isa.core.interaction.InteractionConfigContainer;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ItemHierarchy;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.ManagedDocumentLargeDoc;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.uiclass.ItemConfigurationInfoHelper;

/**
 * Read and display the confirmed order.<br>
 * The order is taken form the document on top from the <code>documentHandler</code>.
 *
 * @version 1.1
 *
 */
public class OrderChangeConfirmedAction extends IsaCoreBaseAction {

    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser parser,
            BusinessObjectManager bom,
            IsaLocation log) throws CommunicationException {
	
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);

        // the confirmation screen is only temporary, this means,
        // if someone press refresh or select another document, the confirmation is lost
        // for this we have to clean up now
        
        String href_action = "b2b/documentstatusdetailprepare.do";
        
        DocumentHandler documentHandler = (DocumentHandler)
            userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);

        DocumentState targetDocument =
            documentHandler.getDocument(ManagedDocument.TARGET_DOCUMENT);

        if (targetDocument == null) {
			log.exiting();
            throw new PanicException("No target document returned from documentHandler");
        }

        // this is needed, to know, what to display on confirmorderchange.jsp
        String doctype = documentHandler.getManagedDocumentOnTop().getDocType();

        request.setAttribute(ShowOrderConfirmedAction.RC_CONFIRMED_DOCTYPE, doctype);

        if (log.isDebugEnabled()) {
            log.debug("Document type: " + doctype);
        }

        Shop shp = bom.getShop();
        SalesDocument salesDoc = (SalesDocument)targetDocument;

        request.setAttribute(MaintainOrderBaseAction.RC_HEADER, salesDoc.getHeader());
        request.setAttribute(MaintainOrderBaseAction.RC_DOC_RECOVERABLE, (salesDoc.isDocumentRecoverable())? "Y":"");
        request.setAttribute(MaintainOrderBaseAction.RC_IS_CHANGE_MODE, "Y");
        request.setAttribute(MaintainOrderBaseAction.RC_ITEMS, salesDoc.getItems());
        // determine inline config info, before document is destroyed
        ItemConfigurationInfoHelper itemConfigInfo = new ItemConfigurationInfoHelper();
        ItemHierarchy itemHierarchy = new ItemHierarchy(salesDoc.getItems());
        char configInfoView = getOrderView(request); 
        char configInfoDetailView = getOrderDetailView(request);
        HashMap configInfoMap = new HashMap();
        HashMap configInfoDetailMap = new HashMap();
        
        for (int i=0; i < salesDoc.getItems().size(); i++) {
            ItemSalesDoc item = salesDoc.getItems().get(i);
            if (item.isConfigurable()) { 
                  itemConfigInfo.setItem(item); 
                  if (itemConfigInfo.isConfigurationAvailable()) {
                      if (configInfoView != ' ' && !(itemHierarchy.isSubItem(item) && item.isItemUsageConfiguration())) {
                          configInfoMap.put(item.getTechKey().getIdAsString(), itemConfigInfo.getItemConfigValues(configInfoView));
                      }
                      if (configInfoDetailView != ' ') {
                          configInfoDetailMap.put(item.getTechKey().getIdAsString(), itemConfigInfo.getItemConfigGroups(configInfoDetailView));
                      }
                  }
            }
        }
        request.setAttribute(MaintainOrderBaseAction.RC_CONFIG_INFO, configInfoMap);
        request.setAttribute(MaintainOrderBaseAction.RC_CONFIG_INFO_DETAIL, configInfoDetailMap);
        
        request.setAttribute(MaintainOrderBaseAction.RC_MESSAGES, salesDoc.getMessageList());
        request.setAttribute(MaintainOrderBaseAction.RC_SHIPCOND, salesDoc.readShipCond(shp.getLanguage()));
        request.setAttribute(MaintainOrderBaseAction.RC_CAMPDESC, salesDoc.getCampaignDescriptions());
        request.setAttribute(MaintainOrderBaseAction.RC_CAMPTYPEDESC, salesDoc.getCampaignTypeDescriptions());
        boolean isLargeDoc = salesDoc.isLargeDocument(bom.getShop().getLargeDocNoOfItemsThreshold());
        request.setAttribute(MaintainOrderBaseAction.RC_IS_LARGE_DOC, (isLargeDoc) ? "Y":"");
       
        if (targetDocument instanceof OrderTemplate) {
            // depending on the setting of the forceIPCPricing flag in the Jasva-Basket
            // the set of available prices must be determined
            setAvailableValueFlagsForJSP(salesDoc, request);
            bom.releaseOrderTemplate();
        }
        else if (targetDocument instanceof CustomerOrder) {
            bom.releaseCustomerOrder();
			log.exiting();
            return mapping.findForward("showcustomerconfirmation");
        }
        else if (targetDocument instanceof CollectiveOrder) {
        	
        	// create history entry for collective order => for other document types this has alreadey been done before 
        	
        	/* -----START Code history entry----- */
            // Build href for ManagedDocument Class
            HeaderSalesDocument preOrderSalesDocHeader = salesDoc.getHeader();
            String href_parameter = ("techkey=".concat(salesDoc.getTechKey().getIdAsString())).concat("&");
            href_parameter = ((href_parameter.concat("object_id=")).concat(preOrderSalesDocHeader.getSalesDocNumber())).concat("&");
            href_parameter = href_parameter.concat("objects_origin=");
            href_parameter = (href_parameter.concat("&objecttype=")).concat(DocumentListFilter.SALESDOCUMENT_TYPE_ORDER);
       
            // add the document to the history
            ManagedDocument mDoc = new ManagedDocument(null,
                                                       "order",
                                                       preOrderSalesDocHeader.getSalesDocNumber(),
                                                       preOrderSalesDocHeader.getPurchaseOrderExt(),
                                                       preOrderSalesDocHeader.getDescription(),
                                                       preOrderSalesDocHeader.getChangedAt(),
                                                       null,
                                                       href_action,
                                                       href_parameter);
            documentHandler.add(mDoc);
            /* -----END Code history entry----- */
        	
            bom.releaseCollectiveOrder();
        }
        else if (targetDocument instanceof Order) {
            bom.releaseOrder();
        }
        
        documentHandler.release(salesDoc);
        
		if (documentHandler.getManagedDocumentOnTop() instanceof ManagedDocumentLargeDoc) {
			OrderStatus orderStatus = (OrderStatus) documentHandler.getManagedDocumentOnTop().getDocument();
			orderStatus.getSelectedItemGuids().clear();
			((ManagedDocumentLargeDoc) documentHandler.getManagedDocumentOnTop()).setNumberofItemsFound(ManagedDocumentLargeDoc.NO_OF_FOUND_ITEMS_UNDEFINED);
			orderStatus.readOrderStatus(orderStatus.getTechKey(), "", "", orderStatus.getOrderHeader().getDocumentType(), null);
		}
		log.exiting();
        return mapping.findForward("showconfirmation");
    }
    
    /**
     * Returns configuration valid for the current session. This feature
     * is only available if Extended Configuration Management is turned on.
     * If no configuration data is available <code>null</code> is returned
     * @return configuration data
     */
    public InteractionConfigContainer getInteractionConfig(HttpServletRequest request) {
        UserSessionData userData =
                 UserSessionData.getUserSessionData(request.getSession());
        if (userData == null) {
            return null;
        }
   
        return (InteractionConfigContainer)(userData.getAttribute(SessionConst.INTERACTION_CONFIG));
     }
    
    /**
     * Determines from XCM the application view on the product configuration, 
     * which should be used for display in the item list of the order screen. 
     * 
     * 
     * @return The view represented by a single character
     *
     */ 
    public char getOrderView(HttpServletRequest request) {
        char view = ' ';
        InteractionConfigContainer icc = getInteractionConfig(request);
        InteractionConfig confview = icc.getConfig("ui");
        String iccView = confview.getValue(ContextConst.IC_ITEM_CONFIG_ORDER_VIEW);
        if (iccView != null && iccView.length() > 0) {
            view = iccView.charAt(0);
        }
        return view;
    }
    
    /**
     * Determines from XCM the application view on the product configuration, 
     * which should be used for display in the item list of the order
     *  confirmation screen (detailed view). 
     * 
     * @return The view represented by a single character
     *
     */ 
    public char getOrderDetailView(HttpServletRequest request) {
        char view = ' ';
        InteractionConfigContainer icc = getInteractionConfig(request);
        InteractionConfig confview = icc.getConfig("ui");
        String iccView = confview.getValue(ContextConst.IC_ITEM_CONFIG_ORDER_DETAIL_VIEW);
        if (iccView != null && iccView.length() > 0) {
            view = iccView.charAt(0);
        }       
        return view;
    } 

}