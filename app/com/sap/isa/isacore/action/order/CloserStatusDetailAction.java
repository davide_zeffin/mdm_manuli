/*****************************************************************************
    Class:        CloserStatusDetailAction
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author        Ralf Witt
    Created:      May 2001

    $Revision: #1 $
    $Date: 2001/06/26 $
*****************************************************************************/
package com.sap.isa.isacore.action.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.billing.BillingStatus;
import com.sap.isa.businessobject.header.HeaderBillingDocument;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.order.OrderStatus;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;

/**
 * Handling of the closer JSP
 *
 *
 * @author  SAP
 * @version 1.1
 *
 */
public class CloserStatusDetailAction extends DocumentStatusBaseAction {

    public static final String RK_CLOSER_DOCTYPE = "closerdocumenttype";
    public static final String RK_CLOSER_DOCSTATUS = "closerdocumentstatus";
    public static final String RK_CLOSER_DOCID = "closerdocumentid";
    public static final String RK_CLOSER_DOCORIGIN = "closerdocumentsorigin";
    public static final String RK_CLOSER_DOCCHANGEABLE = "docchangeable";
    public static final String RK_CLOSER_QUOTEEXTENDED = "quoteextended";
	public static final String RK_CLOSER_DOCKEY = "closerdocumentkey";
	public static final String RK_CLOSER_PROCESSTYPE = "processtype";
	/**
     * Method setAttributes.
     *
     * @param request
     * @param documentId
     * @param documentType
     * @param documentStatus
     * @param documentsOrigin
     * @param docChangeable
     * @param quoteExtended
     */
    protected void setAttributes(HttpServletRequest request,
                                 String documentId,
                                 String documentType,
                                 String documentStatus,
                                 String documentsOrigin,
                                 String docChangeable,
                                 String quoteExtended,
                                 String documentKey,
                                 String processType) {

        if(log.isDebugEnabled()) {
            log.debug("--------------------------------------------------------");
            log.debug("object_id:      " + documentId);
            log.debug("objects_origin: " + documentsOrigin);
            log.debug("object_type:    " + documentType);
            log.debug("object_status:  " + documentStatus);
            log.debug("changeable:     " + docChangeable);
            log.debug("quoteExtended:  " + quoteExtended);
            log.debug("document_key:   " + documentKey);
            log.debug("process_type:   " + processType);
            log.debug("--------------------------------------------------------");
        }

        request.setAttribute(RK_CLOSER_DOCID, documentId);
        request.setAttribute(RK_CLOSER_DOCTYPE, documentType);
        request.setAttribute(RK_CLOSER_DOCSTATUS, documentStatus);
        request.setAttribute(RK_CLOSER_DOCCHANGEABLE, docChangeable);
        request.setAttribute(RK_CLOSER_QUOTEEXTENDED, quoteExtended);
		request.setAttribute(RK_CLOSER_PROCESSTYPE, processType);
        if(documentsOrigin != null)
            request.setAttribute(RK_CLOSER_DOCORIGIN, documentsOrigin);
        request.setAttribute(RK_CLOSER_DOCKEY, documentKey);
    }




    /**
     * @see com.sap.isa.isacore.action.order.DocumentStatusBaseAction#performBillingStatus(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse, UserSessionData, RequestParser, BusinessObjectManager, BillingStatus, IsaLocation)
     */
    public ActionForward performBillingStatus(ActionMapping mapping,
                                              ActionForm form,
                                              HttpServletRequest request,
                                              HttpServletResponse response,
                                              UserSessionData userSessionData,
                                              RequestParser requestParser,
                                              BusinessObjectManager bom,
                                              BillingStatus billingStatus,
                                              IsaLocation log)
            throws CommunicationException {
		final String METHOD_NAME = "performBillingStatus()";
		log.entering(METHOD_NAME);
        String documentId = "";
        String documentType = "";
        String documentStatus = "";
        String documentsOrigin = "";
        String docChangeable = "";
        String quoteExtended = "";
        String documentKey = "";
        String processType = "";

        // BILLING
        HeaderBillingDocument headerBillingDoc = billingStatus.getDocumentHeader();

        documentId = headerBillingDoc.getBillingDocNo();
        documentType = headerBillingDoc.getDocumentType();
        documentStatus = "";                                                // not yet supported!! 3.0
        documentsOrigin = headerBillingDoc.getBillingDocumentsOrigin();
        documentKey = headerBillingDoc.getTechKey().getIdAsString();

        setAttributes(request,
                      documentId,
                      documentType,
                      documentStatus,
                      documentsOrigin,
                      docChangeable,
                      quoteExtended,
                      documentKey,
                      processType);
		log.exiting();
        return mapping.findForward("billingcloser");
    }

    /**
     * @see com.sap.isa.isacore.action.order.DocumentStatusBaseAction#performOrderStatus(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse, UserSessionData, RequestParser, BusinessObjectManager, OrderStatus, IsaLocation)
     */
    public ActionForward performOrderStatus(
                                           ActionMapping mapping,
                                           ActionForm form,
                                           HttpServletRequest request,
                                           HttpServletResponse response,
                                           UserSessionData userSessionData,
                                           RequestParser requestParser,
                                           BusinessObjectManager bom,
                                           OrderStatus orderStatus,
                                           IsaLocation log)
            throws CommunicationException {
		final String METHOD_NAME = "performOrderStatus()";
		log.entering(METHOD_NAME);
        String documentId = "";
        String documentType = "";
        String documentStatus = "";
        String documentsOrigin = "";
        String docChangeable = "";
        String quoteExtended = "";
		String documentKey = "";
        String processType = "";

        // SALES
        HeaderSalesDocument headerSalesDoc = orderStatus.getOrderHeader();

        documentId = headerSalesDoc.getSalesDocNumber();
        documentType = headerSalesDoc.getDocumentType();
        documentStatus = headerSalesDoc.getStatus();
        documentsOrigin = headerSalesDoc.getSalesDocumentsOrigin();
        if(headerSalesDoc.isChangeable() == true)
            docChangeable = "X";
        if(headerSalesDoc.isQuotationExtended()) {
            quoteExtended = "X";
        }
        documentKey = headerSalesDoc.getTechKey().getIdAsString();
        processType = headerSalesDoc.getProcessType();

        setAttributes(request,
                      documentId,
                      documentType,
                      documentStatus,
                      documentsOrigin,
                      docChangeable,
                      quoteExtended,
                      documentKey,
                      processType);
		log.exiting();
        return mapping.findForward("ordercloser");
    }

}
