/*****************************************************************************
  Class:        ShowBasketSimulationAction
  Copyright (c) 2002, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:
  Created:      26.02.2002
  Version:      1.0

  $Revision: #2 $
  $Date: 2002/05/14 $
*****************************************************************************/

package com.sap.isa.isacore.action.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.action.IsaCoreInitAction;
import com.sap.isa.isacore.action.PaymentActions;

/**
 * Action to display a simulated order. Only used in case of R3 order and
 * Java basket Backends.
 * The following forwards are used by this action:
 * </p>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>FORWARD_ORDERSIMULATION</td><td>show the simulated order</td></tr>
 * </table>
 *
 * @author
 * @version 1.0
 */
public class ShowBasketSimulationAction extends MaintainBasketBaseAction {

    /**
     * Creates a new instance of this class.
     */
    public ShowBasketSimulationAction() {
    }


    protected String basketPerform(HttpServletRequest request,
                HttpServletResponse response,
                HttpSession session,
                UserSessionData userSessionData,
                RequestParser parser,
                BusinessObjectManager bom,
                IsaLocation log,
                IsaCoreInitAction.StartupParameter startupParameter,
                BusinessEventHandler eventHandler,
                boolean multipleInvocation,
                boolean browserBack,
                Shop shop,
                SalesDocument preOrderSalesDocument,
                boolean auction,
                DocumentState targetDocument,
                DocumentHandler documentHandler)
                    throws CommunicationException {
		final String METHOD_NAME = "basketPerform()";
		log.entering(METHOD_NAME);
        String forwardTo = null;

        boolean isDebugEnabled = log.isDebugEnabled();

        if (isDebugEnabled) {
            log.debug("ShowBasketSimulationAction");
        }

        // get the document on top ( the auction document)
        DocumentState doc = documentHandler.getManagedDocumentOnTop();
        if (doc == null) {
			log.exiting();
          	throw new PanicException("No document returned from document handler");
        }

        ManagedDocument mdoc = (ManagedDocument) doc;
        doc = mdoc.getDocument();
        SalesDocument salesDoc = (SalesDocument) doc;

        salesDoc.read();

        request.setAttribute(RK_ITEMS, salesDoc.getItems());
        request.setAttribute(RK_HEADER, salesDoc.getHeader());
        request.setAttribute(RK_MESSAGES, salesDoc.getMessageList());
        request.setAttribute(RC_SHIPCOND,  salesDoc.readShipCond(shop.getLanguage()));

        PaymentActions.setPaymentAttributes (userSessionData, request, salesDoc);
         
        userSessionData.setAttribute(SC_SHIPTOS,  salesDoc.getShipTos());

        forwardTo = FORWARD_ORDERSIMULATION;
		log.exiting();
        return forwardTo;
    }
}