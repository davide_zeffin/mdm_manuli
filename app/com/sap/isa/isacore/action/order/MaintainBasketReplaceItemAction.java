/*****************************************************************************
  Class:        MaintainBasketReplaceItemAction
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       SAP
  Created:      10.09.2001
  Version:      1.0

  $Revision: #7 $
  $Date: 2002/02/12 $
*****************************************************************************/

package com.sap.isa.isacore.action.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.action.IsaCoreInitAction;

/**
 * Action to replace an item of the basket with another one during the
 * cross and upselling process.
 * <p>
 * The following forwards are used by this action:
 * </p>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>FORWARD_SHOWBASKET</td><td>always used</td></tr>
 * </table>
 *
 * @author SAP
 * @version 1.0
 * @deprecated The action will be not used any longer. Because the cua products 
 *               aren't display in item detail action from now. 
*/
public class MaintainBasketReplaceItemAction extends MaintainBasketBaseAction {

    /**
     * Creates a new instance of this class.
     */
    public MaintainBasketReplaceItemAction() {
    }


    protected String basketPerform(HttpServletRequest request,
                HttpServletResponse response,
                HttpSession session,
                UserSessionData userSessionData,
                RequestParser parser,
                BusinessObjectManager bom,
                IsaLocation log,
                IsaCoreInitAction.StartupParameter startupParameter,
                BusinessEventHandler eventHandler,
                boolean multipleInvocation,
                boolean browserBack,
                Shop shop,
                SalesDocument preOrderSalesDocument,
                boolean auction,
                DocumentState targetDocument,
                DocumentHandler documentHandler)
                    throws CommunicationException {

		final String METHOD_NAME = "basketPerform()";
		log.entering(METHOD_NAME);
        if (!multipleInvocation) {

            TechKey itemKey = new TechKey((String)request.getParameter("replaceditem"));

            ItemSalesDoc relacedItem = preOrderSalesDocument.getItem(itemKey);

            ItemSalesDoc item = new ItemSalesDoc(TechKey.EMPTY_KEY);

            // Set the values into the sales document
            item.setProduct((String)request.getParameter("replacewithproduct"));

            item.setQuantity(relacedItem.getQuantity());
            item.setUnit(relacedItem.getUnit());

            item.setShipTo(relacedItem.getShipTo());
            item.setText(relacedItem.getText());
            item.setReqDeliveryDate(relacedItem.getReqDeliveryDate());
            item.setNumberInt(relacedItem.getNumberInt());

            item.setProductId(TechKey.EMPTY_KEY);
            item.setPcat(TechKey.EMPTY_KEY);
            item.setContractKey(TechKey.EMPTY_KEY  );
            item.setContractItemKey(TechKey.EMPTY_KEY);

            // possible customer exit, to parse request
            customerExitParseRequest(parser, userSessionData, preOrderSalesDocument);

            preOrderSalesDocument.removeItem(new TechKey((String)request.getParameter("replaceditem")));

            preOrderSalesDocument.addItem(item);

            preOrderSalesDocument.update(bom.createBUPAManager(), shop);   // may throw CommunicationException

            processCustomerNumberFields(parser, preOrderSalesDocument, shop, bom.createBUPAManager());
        }
		log.exiting();
        return FORWARD_SHOWBASKET;
    }
}