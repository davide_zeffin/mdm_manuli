/*****************************************************************************
  Class:        MaintainBasketSendAction
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Thomas Smits
  Created:      10.09.2001
  Version:      1.0

  $Revision: #10 $
  $Date: 2002/03/12 $
*****************************************************************************/

package com.sap.isa.isacore.action.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.ManagedDocumentDetermination;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businessobject.businessevent.PlaceOrderEvent;
import com.sap.isa.businessobject.order.OrderCreate;
import com.sap.isa.businessobject.ordertemplate.OrderTemplate;
import com.sap.isa.businessobject.quotation.Quotation;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.action.DeterminationAction;
import com.sap.isa.isacore.action.IsaCoreInitAction;
import com.sap.isa.isacore.action.PaymentActions;
import com.sap.isa.isacore.action.PrepareDeterminationAction;
import com.sap.isa.isacore.action.oci.OciLinesSendAction;

/**
 * Action to perform the final step in the processing of an document (i.e. to
 * save it in the backend).
 * The following forwards are used by this action:
 * </p>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>FORWARD_OCI_SUMMARY_BASKET</td><td>show a summary of the ordered products and offer an transfer in OCI format to EBP system</td></tr>
 *   <tr><td>FORWARD_OCI_SEND_BASKET</td><td>transfer basket in OCI format to EBP system</td></tr>
 *   <tr><td>FORWARD_EAUCTION</td><td>go to the special eAuction checkout process</td></tr>
 *   <tr><td>FORWARD_SUMMARY_BASKET</td><td>show a summary of the ordered products and offer a printout</td></tr>
 * </table>
 *
 * @author Thomas Smits
 * @version 1.0
 */
public class MaintainBasketSendAction extends MaintainBasketBaseAction {

    /**
     * Session context variable. to store, if atp substitution has taken place
     */
    public static String SC_ATPSUBST = "atpsubst";

    /**
     * Creates a new instance of this class.
     */
    public MaintainBasketSendAction() {
    }

    protected String basketPerform(
        HttpServletRequest request,
        HttpServletResponse response,
        HttpSession session,
        UserSessionData userSessionData,
        RequestParser parser,
        BusinessObjectManager bom,
        IsaLocation log,
        IsaCoreInitAction.StartupParameter startupParameter,
        BusinessEventHandler eventHandler,
        boolean multipleInvocation,
        boolean browserBack,
        Shop shop,
        SalesDocument preOrderSalesDocument,
        boolean auction,
        DocumentState targetDocument,
        DocumentHandler documentHandler)
        throws CommunicationException {
        final String METHOD_NAME = "basketPerform()";
        log.entering(METHOD_NAME);
        String forwardTo = null;

        boolean isDebugEnabled = log.isDebugEnabled();

        RequestParser.Parameter calledFromDetermination = parser.getParameter(DeterminationAction.RC_CALLED_FROM_DET);

        // In case the action is called from the product determination action there is nothing
        // to be parsed
        if (!calledFromDetermination.isSet()) {

            updateSalesDocument(parser, userSessionData, preOrderSalesDocument, shop, bom.createBUPAManager(), log);

            processCustomerNumberFields(parser, preOrderSalesDocument, shop, bom.createBUPAManager());
        }

        if (preOrderSalesDocument.isDeterminationRequired()) {
            request.setAttribute(PrepareDeterminationAction.RC_DET_CALLER, ManagedDocumentDetermination.CALLER_MAINTAIN_BASKET_SEND);
            request.setAttribute(PrepareDeterminationAction.RC_DET_DOC_MODE, ManagedDocumentDetermination.DOC_MODE_NEW);
            log.exiting();
            return FORWARD_PRODDET;
        }
        // there may be error messages, created when the requested was parsed
        // for example, an not existing soldto id was specified. These messages
        // would be destroyed, if we would check after the readHeader(), because
        // the backend doesn't knows about them
        // if there are any error messages for the preOrderSalesDocument or
        // items, you cannot order. The user stays on the same page.
        if (!preOrderSalesDocument.isValid()) {

            if (isDebugEnabled) {
                log.debug("Document NOT valid");
                log.debug("Message provided: " + preOrderSalesDocument.getMessageList());
            }
            log.exiting();
            return FORWARD_SHOWBASKET;
        }

        // check for oci transfer
        boolean ociTransfer = startupParameter != null && startupParameter.getHookUrl().length() > 0;

        // order must be read first in order to check validity
        preOrderSalesDocument.read();

        if (preOrderSalesDocument.getHeader().hasATPSubstOccured()) {
            if (isDebugEnabled) {
                log.debug("ATP substitution took place");
            }
            userSessionData.setAttribute(SC_ATPSUBST, "true");
            log.exiting();
            return FORWARD_SHOWBASKET;
        }

        // if there are any error messages for the preOrderSalesDocument or
        // items, you cannot order. The user stays on the same page.
        if (!preOrderSalesDocument.isValid()) {

            if (isDebugEnabled) {
                log.debug("Document NOT valid");
                log.debug("Message provided: " + preOrderSalesDocument.getMessageList());
            }
            log.exiting();
            return FORWARD_SHOWBASKET;
        }
        
		// if there are no items in the preOrderSalesDocument the user stays on the same page.
		if (preOrderSalesDocument.getItems() == null || preOrderSalesDocument.getItems().isEmpty()) {

			log.debug("Document contains no items");

			log.exiting();
			return FORWARD_SHOWBASKET;
		}

        // quotation
        if (preOrderSalesDocument instanceof Quotation) {
            if (isDebugEnabled) {
                log.debug("Document type: Quotation");
            }

            // save & re-read header to get qotation number
             ((Quotation) preOrderSalesDocument).save(shop);
            preOrderSalesDocument.clearMessages();
            preOrderSalesDocument.read();
            request.setAttribute(RK_ITEMS, preOrderSalesDocument.getItems());
            request.setAttribute(RK_HEADER, preOrderSalesDocument.getHeader());

        }

        // order template
        else if (preOrderSalesDocument instanceof OrderTemplate) {
            if (isDebugEnabled) {
                log.debug("Document type: OrderTemplate");
            }

            // save & re-read header to get document number
             ((OrderTemplate) preOrderSalesDocument).save(); // may throw CommunicationException
            preOrderSalesDocument.read();
            request.setAttribute(RK_ITEMS, preOrderSalesDocument.getItems());
            request.setAttribute(RK_HEADER, preOrderSalesDocument.getHeader());
        }

        // basket (saved only if no oci transfer is requested)
        else if (!ociTransfer) {
            OrderCreate ordr = new OrderCreate(bom.createOrder());

            if (isDebugEnabled) {
                log.debug("Document type: Basket");
            }
            if (preOrderSalesDocument.getHeader().getShop() == null) {
                preOrderSalesDocument.getHeader().setShop(shop);
            }
            ordr.copyFromDocument(preOrderSalesDocument);
            ordr.clearMessages();

            // the order is saved in the backend
            if (!PaymentActions.isPaymentMaintenanceAvailable(userSessionData, ordr.getSalesDocument())) {

                ordr.saveOrderCreate(); // may throw CommunicationException
                if (!ordr.getSalesDocument().isValid()) {

                    // here we get possibly messages if any problem occures;
                    // must be copied to the current basket object!
                    preOrderSalesDocument.copyMessages(ordr.getSalesDocument());
                    log.exiting();
                    return FORWARD_SHOWBASKET;
                }
            }
            else {
                int retCode = ordr.save();
                if (retCode != 0) {
                    // here we get possibly messages if any authentication problem occures;
                    // set the dirty flag of the basket to enable a reread of the message log.
                    preOrderSalesDocument.setDirty(true);
                    preOrderSalesDocument.copyMessages(ordr.getSalesDocument());
                    log.exiting();
                    return FORWARD_SHOWBASKET;
                }
            }

            ordr.read();

            // fire event
            eventHandler.firePlaceOrderEvent(
                    new PlaceOrderEvent(bom.getOrder(),bom.getUser()));

            request.setAttribute(RK_ITEMS, ordr.getItems());
            request.setAttribute(RK_HEADER, ordr.getHeader());
            request.setAttribute(RK_MESSAGES, ordr.getMessageList());
        }
        userSessionData.setAttribute(SC_SHIPTOS, preOrderSalesDocument.getShipTos());

        // forwards depend on oci transfer or not
        if (ociTransfer) {
            userSessionData.setAttribute(OciLinesSendAction.SALES_DOCUMENT_KEY, preOrderSalesDocument.getTechKey().getIdAsString());
            if (preOrderSalesDocument instanceof Quotation || preOrderSalesDocument instanceof OrderTemplate) {
                forwardTo = FORWARD_OCI_SUMMARY_BASKET;
            }
            else {
                forwardTo = FORWARD_OCI_SEND_BASKET;
            }
        }
        else {
            forwardTo = auction ? FORWARD_EAUCTION : FORWARD_SUMMARY_BASKET;
        }

        log.exiting();
        return forwardTo;
    }

    /**
     * @param document
     * @param preOrderSalesDocument
     */
    private void copyMessages(SalesDocument sourceDoc, SalesDocument targetDoc) {
        // First copy header messages
        targetDoc.copyMessages(sourceDoc);

    }
}