/*****************************************************************************
    Class         ShowBatchAction
    Copyright (c) 2000, SAP AG, Germany, All rights reserved.
    Description:  Action for the Profile Maintenance
    Author:
    Created:      26.11.2002
    Version:      1.0

    $Revision: #8 $
    $Date: 2002/11/26 $
*****************************************************************************/

package com.sap.isa.isacore.action.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.ProductBatch;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.ordertemplate.OrderTemplate;
import com.sap.isa.businessobject.quotation.Quotation;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.uiclass.b2b.order.BatchUIInfoContainer;

/**
 * Reread the data for the batch from the backend to allow
 * a frame tolerant access for the JSP.
 *
 * @author Daniel Seise
 * @version 1.0
 *
 */
public class ShowBatchAction extends IsaCoreBaseAction {

	public static final String FORWARD_SUCCESS = "showbatch";

    /**
     * Overriden <em>isaPerform</em> method of <em>IsaCoreBaseAction</em>.
     */
    public ActionForward isaPerform(ActionMapping mapping,
                ActionForm form,
                HttpServletRequest request,
                HttpServletResponse response,
                UserSessionData userSessionData,
                RequestParser requestParser,
                BusinessObjectManager bom,
                IsaLocation log)
                        throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        // Page that should be displayed next.
        String forwardTo = null;

        // Ask the business object manager for a user. If this object is not
        // already present, the the next action will to login
        //User user = bom.getUser();
                
                
        ProductBatch productBatch = bom.createProductBatch();
        
        DocumentHandler documentHandler = (DocumentHandler)
            userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
        if (documentHandler == null) {
        	log.exiting();
            throw new PanicException("No document handler found");
        }
        DocumentState targetDocument =
            documentHandler.getDocument(ManagedDocument.TARGET_DOCUMENT);
        
        SalesDocument preOrderSalesDocument = null;
        if (targetDocument instanceof Quotation) {
            preOrderSalesDocument = bom.getQuotation();
            userSessionData.setAttribute(
                MaintainBasketBaseAction.SC_DOCTYPE,
                MaintainBasketBaseAction.DOCTYPE_QUOTATION);
        }
        else if (targetDocument instanceof OrderTemplate) {
            preOrderSalesDocument = bom.getOrderTemplate();
            userSessionData.setAttribute(
                MaintainBasketBaseAction.SC_DOCTYPE,
                MaintainBasketBaseAction.DOCTYPE_ORDERTEMPLATE);
        }
        else {
            preOrderSalesDocument = bom.getBasket();
            userSessionData.setAttribute(
                MaintainBasketBaseAction.SC_DOCTYPE,
                MaintainBasketBaseAction.DOCTYPE_BASKET);
        }
        if (preOrderSalesDocument == null) {
			log.exiting();
            throw new PanicException("No sales document returned from backend");
        }

        
        productBatch.readAllElementsForUpdate(new TechKey((String)userSessionData.getAttribute("batchproductid")));
		
		// create an InfoContainer that contains all information that are needed to build up the batch.jsp
		BatchUIInfoContainer batchBean = new BatchUIInfoContainer(productBatch, preOrderSalesDocument.getItem(new TechKey(((String)userSessionData.getAttribute("batchselection")))), false);
		userSessionData.setAttribute("batchbean", batchBean);
		
		request.setAttribute(MaintainBasketBaseAction.RC_SINGLE_ITEM, preOrderSalesDocument.getItem(new TechKey((String)userSessionData.getAttribute("batchselection"))));
		
        forwardTo = FORWARD_SUCCESS;
		log.exiting();
        return mapping.findForward(forwardTo);
    }  

}