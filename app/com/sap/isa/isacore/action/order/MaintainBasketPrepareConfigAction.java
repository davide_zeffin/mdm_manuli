/*****************************************************************************
  Class:        MaintainBasketPrepareConfigAction
  Copyright (c) 2002, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Michael Dietrich
  Created:      04.03.2002
  Version:      1.0

  $Revision: #0 $
  $Date: 2002/03/04 $
*****************************************************************************/

package com.sap.isa.isacore.action.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.action.IsaCoreInitAction;

/**
 * Action to update the basket, before an item is configured.
 * <p>
 * The following forwards are used by this action:
 * </p>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>FORWARD_CONFIG_ITEM</td><td>always used</td></tr>
 * </table>
 *
 * @author Michael Dietrich
 * @version 1.0
 */
public class MaintainBasketPrepareConfigAction extends MaintainBasketBaseAction {

    /**
     * Creates a new instance of this class.
     */
    public MaintainBasketPrepareConfigAction() {
    }


    protected String basketPerform(HttpServletRequest request,
                HttpServletResponse response,
                HttpSession session,
                UserSessionData userSessionData,
                RequestParser parser,
                BusinessObjectManager bom,
                IsaLocation log,
                IsaCoreInitAction.StartupParameter startupParameter,
                BusinessEventHandler eventHandler,
                boolean multipleInvocation,
                boolean browserBack,
                Shop shop,
                SalesDocument preOrderSalesDocument,
                boolean auction,
                DocumentState targetDocument,
                DocumentHandler documentHandler)
                    throws CommunicationException {
		final String METHOD_NAME = "basketPerform()";
		log.entering(METHOD_NAME);
		String forwardTo = FORWARD_CONFIG_ITEM;
		
        if (!multipleInvocation) {

            updateSalesDocument(parser, userSessionData, preOrderSalesDocument, shop, bom.createBUPAManager(), log);

            processCustomerNumberFields(parser, preOrderSalesDocument, shop, bom.createBUPAManager());

            userSessionData.setAttribute("newpos", request.getParameter("newpos"));

            // had to be commented out: the message about a missing configuration appears
            // and blocks configuration otherwise!
            // if (!preOrderSalesDocument.isValid()) {
            //    return FORWARD_SHOWBASKET;
            // }
            
           preOrderSalesDocument.readHeader();         // may throw CommunicationException
           preOrderSalesDocument.readAllItemsForUpdate();              

            // forward the techkey of the item that will be configured
            request.setAttribute(ItemConfigurationBaseAction.PARAM_ITEMID,
                                 parser.getParameter("configitemid").getValue().getString());
        }
        
		//Set the correct uiMode to display the grid Screen or Configuration screen.
		setUiModeForGrid(request, userSessionData);
		
		//Get the correct forward depending on the uimode
		forwardTo= forwardDependingOnUiMode(forwardTo, 
											preOrderSalesDocument.getItems(), 
											request, 
											userSessionData);
		log.exiting();
        return forwardTo;
    }
}