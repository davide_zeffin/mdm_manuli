/*****************************************************************************
  Class:        DocumentStatusCloseConfigAction
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Stefan Dendl
  Created:      17.08.2001
  Version:      1.0

  $Revision: #5 $
  $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.isacore.action.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.action.IsaCoreBaseAction;


/**
 *
 *
 * @author Stefan Dendl
 * @version 1.0
 */
public class DocumentStatusCloseConfigAction extends IsaCoreBaseAction {

    private static final String FORWARD_UPDATEDOCUMENT   = "updatedocumentview";

     /**
     * Overriden <em>isaPerform</em> method of <em>IsaCoreBaseAction</em>.
     */
    public ActionForward isaPerform(ActionMapping mapping,
                ActionForm form,
                HttpServletRequest request,
                HttpServletResponse response,
                UserSessionData userSessionData,
                RequestParser parser,
                BusinessObjectManager bom,
                IsaLocation log)
                        throws CommunicationException {

		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
         // Page that should be displayed next.
        String forwardTo = null;

        HttpSession session = request.getSession();

        String sessionId = session.getId();
        boolean isDebugEnabled = log.isDebugEnabled();

        String logPrefix = null;

        if (isDebugEnabled) {
            logPrefix = sessionId + " ~ isaPerform(): ";
        }

        //
        DocumentHandler documentHandler = (DocumentHandler)
                userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);

        if (documentHandler == null) {
			log.exiting();
            throw new PanicException("No document handler found");
        }

            userSessionData.removeAttribute(ItemConfigurationBaseAction.SC_ITEM_IN_CONFIG);

            ManagedDocument onTopDoc = documentHandler.getManagedDocumentOnTop();
            // todo: kein docontop da
            ManagedDocument orderchangeDoc = new ManagedDocument(onTopDoc.getDocument(),
            onTopDoc.getDocType(), onTopDoc.getDocNumber(), onTopDoc.getRefNumber(),
            onTopDoc.getRefName(), onTopDoc.getDate(), "order_status",
            onTopDoc.getHref(), onTopDoc.getHrefParameter());
            documentHandler.add(orderchangeDoc);
            documentHandler.setOnTop(orderchangeDoc.getDocument());
            forwardTo = FORWARD_UPDATEDOCUMENT;
		 log.exiting();
         return mapping.findForward(forwardTo);

    }

}
