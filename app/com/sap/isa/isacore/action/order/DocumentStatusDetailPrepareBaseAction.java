/*****************************************************************************
    Class:        DocumentStatusDetailPrepareBaseAction
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author        Ralf Witt
    Created:      May 2001

    $Revision: #23 $
    $Date: 2004/08/10 $
*****************************************************************************/

package com.sap.isa.isacore.action.order;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.backend.boi.isacore.order.DocumentListFilterData;
import com.sap.isa.businessobject.BORuntimeException;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.ConnectedDocument;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.GenericSearch;
import com.sap.isa.businessobject.SalesDocumentStatus;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.businessobject.billing.BillingStatus;
import com.sap.isa.businessobject.header.HeaderBillingDocument;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.order.CollectiveOrderStatus;
import com.sap.isa.businessobject.order.CustomerOrderStatus;
import com.sap.isa.businessobject.order.OrderStatus;
import com.sap.isa.businessobject.order.PartnerListEntry;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.businesspartner.businessobject.HierarchyKey;
import com.sap.isa.businesspartner.businessobject.PartnerFunctionBase;
import com.sap.isa.core.ContextConst;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.History;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.ManagedDocumentLargeDoc;
import com.sap.isa.isacore.MessageDisplayer;
import com.sap.isa.isacore.action.ActionConstants;
import com.sap.isa.isacore.action.ActionConstantsBase;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.action.IsaCoreInitAction;
import com.sap.isa.isacore.action.PaymentActions;
import com.sap.isa.isacore.action.IsaCoreInitAction.StartupParameter;

/**
 * Prepares to show a documents detail
 *
 * <br> Note that all Action classes have to provide a non argument constructor
 * because they were instantiated automatically by the action servlet.
 *
 * @author  Ralf Witt
 * @version 0.1
 *
 */
abstract public class DocumentStatusDetailPrepareBaseAction extends IsaCoreBaseAction {

    /**
    * sales document stored in request scope.
    */
    public static final String RK_ORDER_STATUS_DETAIL = "orderstatusdetail";
    public static final String RK_ORDER_STATUS_DETAIL_ITEMITERATOR = "itemiterator";
    public static final String RK_SOLDTO_NAME = "soldtoname";
    public static final String RK_SOLDTO_TECHKEY = "soldtotechkey";
    public static final String RK_DOCUMENTID = "documentid";
    private static final String MESSAGE_PAGE = "message";
    private static final String ERROR_PAGE = "error";    

    /** 
     * href for ManagedDocument Class.
     * overwrite with apropriate action
     */
    protected String href_action = "b2b/documentstatusdetailprepare.do";

    /** 
     * forward for ManagedDocument Class.
     * overwrite with apropriate action
     */
    protected  String forward;
    
    
    // constants for order check
    private char CHECK_ERROR = 'E';
    private char CHECK_OK = 'O';
    private char CHECK_CUSTOMER_ORDER = 'C';

    /** 
     * returns the appropriate order status object
     *
     * @param  request           Reference to the HttpServletRequest
     * @param  bom               Reference to the BusinessObjectManager
     * @return order status object
     */
    abstract protected OrderStatus getSalesDocumentStatus(HttpServletRequest request, BusinessObjectManager bom) ;


    /** 
     * returns the logical document type for the document
     *
     * @param  header header of the sales document
     * @return document type used in the managed document
     *
     * @see com.sap.isa.isacore.ManagedDocument
     */
    abstract protected String getSalesDocumentType(HeaderSalesDocument header) ;


    /**
     * creates the Request Parameter which must be given to this action
     *
     * @param orderHeader OrderHeader
     *
     * @return String with the request parameter
     *
     */
    public static String createParameter(HeaderSalesDocument orderheader) {

        StringBuffer parameter = new StringBuffer();

        parameter.append('?').append("techkey=").append(orderheader.getTechKey())
        .append('&').append("object_id=").append(orderheader.getSalesDocNumber())
        .append('&').append("objects_origin=").append(orderheader.getSalesDocumentsOrigin())
        .append('&').append("objecttype=").append(orderheader.getDocumentType())
        .append('&').append("processtype=").append(orderheader.getProcessType());

        return parameter.toString();
    }


    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
                                    ActionForm form,
                                    HttpServletRequest request,
                                    HttpServletResponse response,
                                    UserSessionData userSessionData,
                                    RequestParser requestParser,
                                    BusinessObjectManager bom,
                                    IsaLocation log)
    throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        TechKey documentKey = null;
        String documentId = "";
        String documentsOrigin = "";
        String documentType = "";
        String billing = "";
        String externalForward = null; // Called in crossentry sequence
		String soldToRequest = null;
        
        
        if ( (request.getParameter("techkey")) != null) {
            documentKey = new TechKey(request.getParameter("techkey"));
            documentId = (request.getParameter("object_id") != null ? request.getParameter("object_id") : "");
            documentsOrigin = (request.getParameter("objects_origin") != null ? request.getParameter("objects_origin") : "");
            documentType = (request.getParameter("objecttype") != null ? request.getParameter("objecttype") : "");
            billing = (request.getParameter("billing") != null ? request.getParameter("billing") : "");
			soldToRequest = (request.getParameter("sold_to") != null ? request.getParameter("sold_to") : "");
            externalForward = ((String)request.getParameter("externalforward")) != null ? (String)request.getParameter("externalforward") : "";
        }
        else {
            // check request attributes
            documentKey = new TechKey((String)request.getAttribute("techkey"));
            documentId = (request.getAttribute("object_id")) != null ? (String)request.getAttribute("object_id") : "";
            documentsOrigin = (request.getAttribute("objects_origin") != null ? (String)request.getAttribute("objects_origin") : "");
            documentType = (request.getAttribute("objecttype")) != null ? (String)request.getAttribute("objecttype") : "";
            billing = (request.getAttribute("billing")) != null ? (String)request.getAttribute("billing") : "";
			soldToRequest = (request.getAttribute("sold_to") != null ? (String)request.getAttribute("sold_to") : "");
            externalForward = (request.getAttribute("externalforward")) != null ? (String)request.getAttribute("externalforward") : "";
        }

        Shop shop = bom.getShop();

        DocumentHandler documentHandler = getDocumentHandler(userSessionData);
        StartupParameter startupParameter = (StartupParameter)userSessionData.getAttribute(IsaCoreInitAction.SC_STARTUP_PARAMETER);

        if (log.isDebugEnabled()) {
            log.debug("--------------------------------------------------------");
            log.debug("techkey:        " + documentKey.getIdAsString().trim());
            log.debug("object_id:      " + documentId.trim());
            log.debug("objects_origin: " + documentsOrigin.trim());
            log.debug("objecttype:     " + documentType.trim());
            log.debug("billing:        " + billing);
			log.debug("soldtoRequest   " + soldToRequest);
            log.debug("externalforward:" + (externalForward != null ? externalForward : ""));
            log.debug("--------------------------------------------------------");
        }

        if (documentKey == null) {
            if (log.isDebugEnabled()) {
                log.debug("--------------------------dockeynull------------------------------");
            }
            // No techkey given, than only Id AND Origin identifing a object uniquely
            if ( (documentsOrigin == null || documentsOrigin.equalsIgnoreCase(""))
                 || (documentId == null || documentId.equalsIgnoreCase(""))) {
				log.exiting();
                throw new PanicException("status.message.parameter.missing");

            }

        }
		
		externalForward = getSingleItemPresentationNewForward(request, externalForward);
		
		BusinessPartnerManager buPaMa = bom.createBUPAManager();    
        TechKey soldToKey = (buPaMa.getDefaultBusinessPartner(PartnerFunctionBase.SOLDTO) != null 
                             ? buPaMa.getDefaultBusinessPartner(PartnerFunctionBase.SOLDTO).getTechKey()
                             : new TechKey(""));

		if (!soldToRequest.equals("")){
			soldToKey = new TechKey(soldToRequest);                             
		}
		
		// FOR WHICH DOCUMENTTYPE DO WE RUN   BILLING or SALES
        if ( billing == null || ! billing.equals("X")) {

            // SALES
            if ( ! isSeparateBilling(userSessionData)) {
                bom.releaseBillingStatus();
            }

            OrderStatus orderStatusDetail = getSalesDocumentStatus(request, bom);

            // Is Shop still available
            if ( orderStatusDetail.getShop() == null) {
                orderStatusDetail.setShop(shop);
            }

            // We use OrderStatus and DocumentListFilter as data container for
            // transport purposes between muliple Actions

            // Set attributes given by request
            orderStatusDetail.setTechKey(documentKey);
            orderStatusDetail.setSalesDocumentNumber(documentId);
            orderStatusDetail.setSalesDocumentsOrigin(documentsOrigin);

            // Exception handling
            try {
                orderStatusDetail.readOrderStatus(documentKey, documentId, documentsOrigin, documentType, null);
            } catch (BORuntimeException BORex) {
                documentHandler.release(orderStatusDetail);
                boolean justExc = createErrorMessage(request, documentHandler, "/b2b/updatedocumentview.do", "", orderStatusDetail, BORex);
                if (justExc) { // In case of Exception message only show the error page
                    return mapping.findForward(ERROR_PAGE);
                } else {
                    return mapping.findForward(MESSAGE_PAGE);
                }
            }

            // Backend annonced that the document doesn't exists anymore
            if ( ! orderStatusDetail.isDocumentExistent()) {
                // Handling for a no existing document
                userSessionData.setAttribute(History.NODOCFOUND, "true");
                // remove it from history
                History history = documentHandler.getHistory();
                history.remove(documentType.trim(), documentId);
				log.exiting();
                return mapping.findForward("nodocfound");
            }

			// Get Salesdocument status details
			// get the user first
			User user = bom.getUser();
			
			if (documentType.length() == 0) {
				// read document type if it is not provide via request. 
				documentType = orderStatusDetail.getOrderHeader().getDocumentType();
			}
			
			//Check if user has permission for displaying corresponding document
			Boolean perm = user.hasPermission(documentType,
											  DocumentListFilterData.ACTION_READ);
			if (!perm.booleanValue()){
				log.exiting();
			  	throw new PanicException ("User has no permission to display " + documentType);
			}


            // set the Properties "docType" and "displayable" in the ConnectedDocument.             
            orderStatusDetail.enhanceConnectedDocument();
            //checkConnectedDocumentsVisibility(userSessionData, orderStatusDetail);

			String hierarchyType = null;
			String hierarchyName = null;
			 
			if (shop.isBpHierarchyEnable()) {
				HierarchyKey hierarchy = buPaMa.getPartnerHierachy();
				hierarchyType = hierarchy.getHierarchyType();
				hierarchyName = hierarchy.getHierarchyName(); 
			}

            // check the order:
            // 1) if the order is a customer order
            // 2) if the partner found in the order object match with 
            //    the partner in the buPaMa
            char check = checkOrder(orderStatusDetail, 
                                    bom.getUser(),
                                    bom.getBUPAManager(),
                                    shop.getCompanyPartnerFunction(),
                                    shop.isHomActivated(),
									hierarchyName,
									hierarchyType);

            if (check == CHECK_ERROR) {
                
                // release document
                documentHandler.release(orderStatusDetail);
                // create a Message Displayer for the error page!
                MessageDisplayer messageDisplayer = new MessageDisplayer();

                messageDisplayer.addToRequest(request);
                messageDisplayer.addMessage(new Message(Message.ERROR,"b2b.showDoc.userNotAllowed"));

                messageDisplayer.setAction("b2b/updatedocumentview.do");
				log.exiting();
                return mapping.findForward("message");                
            } 
            else  if (check == CHECK_CUSTOMER_ORDER) {
				log.exiting();
				if (externalForward != null  &&  (! externalForward.equals("")) ) {
					request.setAttribute("externalforward","startPdo");
				}
                return mapping.findForward("showascustomerorder");                
            }
                                    
            HeaderSalesDocument headerSalesDoc = orderStatusDetail.getOrderHeader();
                        
            // check also if the document is a collective order
            if (headerSalesDoc.isDocumentTypeCollectiveOrder() && !(orderStatusDetail instanceof CollectiveOrderStatus)) {
				log.exiting();
                return mapping.findForward("showascollectiveorder");
            }

            // no document handler avilable in B2C
            if (!shop.getApplication().equalsIgnoreCase(Shop.B2C)) {
                addToDocumentHandler(documentHandler, orderStatusDetail, bom);
                // Adjust "external" forward 
				externalForward = getAdjustedSingleItemPresentationNewForward(
												request, orderStatusDetail.isLargeDocument(shop.getLargeDocNoOfItemsThreshold()),
												externalForward, documentHandler);				
            }
            
			// put payment related values into the context
		    PaymentActions.setPaymentAttributes(userSessionData, request, orderStatusDetail);   
        }
        else {

            // BILLING
            if ( ! isSeparateBilling(userSessionData)) {
                bom.releaseOrderStatus();
            }
			//get the user first
			User user = bom.getUser();
			//Check if user has permission for displaying corresponding document
			Boolean perm = user.hasPermission(DocumentListFilterData.BILLINGDOCUMENT_TYPE_INVOICE,
								   DocumentListFilterData.ACTION_READ);
			if (!perm.booleanValue()){
				log.exiting();
				throw new PanicException ("User has no permission to display BillingDocs" );
			}
            BillingStatus billingStatusDetail = bom.createBillingStatus();

            // Get Billingdocument status details
            billingStatusDetail.readBillingStatus(documentKey, documentsOrigin, shop, soldToKey);
            addToDocumentHandler(documentHandler, billingStatusDetail, bom, externalForward);
            // Adjust "external" forward 
			externalForward = getAdjustedSingleItemPresentationNewForward(
											request, billingStatusDetail.isLargeDocument(shop.getLargeDocNoOfItemsThreshold()),
											externalForward, documentHandler);				

        }

        // Another forward provided ??
        if (externalForward != null  &&  (! externalForward.equals("")) ) {
			log.exiting();
            return mapping.findForward(externalForward);
        }
        if (isSeparateBilling(userSessionData)) {
			log.exiting();
            return mapping.findForward("updatebilling");
        }
		log.exiting();
        return mapping.findForward("success");

    }


    /**
     * Adds the given orderStatusDetail object to the document handler
     * 
     * @param documentHandler
     * @param orderStatusDetail
     * @param bom               Reference to the BusinessObjectManager
     */
    protected void addToDocumentHandler(DocumentHandler documentHandler, OrderStatus orderStatusDetail, BusinessObjectManager bom) {
        
        HeaderSalesDocument headerSalesDoc;
        
        headerSalesDoc = orderStatusDetail.getOrderHeader();
        
        if (documentHandler.getManagedDocumentOnTop() instanceof ManagedDocumentLargeDoc &&
            documentHandler.getManagedDocumentOnTop().getDocument() instanceof SalesDocumentStatus) {
            String techKeyStr1 = orderStatusDetail.getTechKey().getIdAsString();
            String hrefStr2 = documentHandler.getManagedDocumentOnTop().getHrefParameter();
            
            if (hrefStr2 != null  &&  hrefStr2.indexOf(techKeyStr1) > 0) {
                return;
            }
         }
        
        String href_parameter = ("techkey=".concat(headerSalesDoc.getTechKey().getIdAsString())).concat("&");
        href_parameter = ((href_parameter.concat("object_id=")).concat(headerSalesDoc.getSalesDocNumber())).concat("&");
        href_parameter = (href_parameter.concat("objects_origin=")).concat(headerSalesDoc.getSalesDocumentsOrigin());
        
        // expand HREF by documenttype
        href_parameter = (href_parameter.concat("&objecttype=")).concat(headerSalesDoc.getDocumentType());
        
        orderStatusDetail.setState(DocumentState.VIEW_DOCUMENT);
        
        ManagedDocument mDoc = null;
        //create managed document for large documents, even if it might not be necessary
        mDoc = new ManagedDocumentLargeDoc(orderStatusDetail,
                                   getSalesDocumentType(headerSalesDoc),
                                   headerSalesDoc.getSalesDocNumber(),
                                   headerSalesDoc.getPurchaseOrderExt(),
                                   headerSalesDoc.getDescription(),
        //Show CreatedAt Date instead of ChangedAt Date in OrderStatus and OrderChange. According to a request of Cat / Ford and to be consistent within the Application, this was necessary.
        //Involved Developer: Stefan Dendl, Joachim Hartmann, Ralf Witt
                                   headerSalesDoc.getCreatedAt(),
                                   forward,
                                   href_action,
                                   href_parameter);
        // Only add associated document if a worklist had been opened before
        if (documentHandler.getManagedDocumentOnTop() != null   && 
            documentHandler.getManagedDocumentOnTop().getDocument() instanceof GenericSearch) {
            mDoc.setAssociatedDocument(documentHandler.getManagedDocumentOnTop());
        }
        if (orderStatusDetail.isLargeDocument(bom.getShop().getLargeDocNoOfItemsThreshold())) {
            // create managed document for large documents
            ((ManagedDocumentLargeDoc) mDoc).setItemSearchStatus(ManagedDocumentLargeDoc.ITEM_STATUS_NOT_SPECIFIED);
            ((ManagedDocumentLargeDoc) mDoc).setItemSearchProperty(ManagedDocumentLargeDoc.ITEM_PROPERTY_NONE);
            ((ManagedDocumentLargeDoc) mDoc).setItemSearchPropertyLowValue("");
            ((ManagedDocumentLargeDoc) mDoc).setItemSearchPropertyHighValue("");
        }
        
        documentHandler.add(mDoc);
        documentHandler.setOnTop(orderStatusDetail);
        // needed if the catalog is displayed and a document gets selected from the history
        documentHandler.setCatalogOnTop(false);
    }
    /**
     * Adds the given billingStatusDetail object to the document handler
     * 
     * @param documentHandler
     * @param billingStatusDetail
     * @param bom               Reference to the BusinessObjectManager
     * @param extFwrd  external Forward
     */
    protected void addToDocumentHandler(DocumentHandler documentHandler, BillingStatus billingStatusDetail,
                                        BusinessObjectManager bom, String extFwrd) {
        StringBuffer href_parameter = new StringBuffer();
        HeaderBillingDocument headerBillingDoc = billingStatusDetail.getDocumentHeader();
        
        if (documentHandler.getManagedDocumentOnTop() instanceof ManagedDocumentLargeDoc &&
            documentHandler.getManagedDocumentOnTop().getDocument() instanceof BillingStatus) {
            String techKeyStr1 = headerBillingDoc.getTechKey().getIdAsString();
            String hrefStr2 = documentHandler.getManagedDocumentOnTop().getHrefParameter();
            
            if (hrefStr2 != null  &&  hrefStr2.indexOf(techKeyStr1) > 0) {
                return;
            }
         }
        href_parameter.append("techkey=" + headerBillingDoc.getTechKey().toString() + "&");
        href_parameter.append("object_id=" + headerBillingDoc.getBillingDocNo() + "&");
        href_parameter.append("objects_origin=" + billingStatusDetail.getBillingDocumentsOrigin() + "&");
        if (! "CRM".equals(billingStatusDetail.getBillingDocumentsOrigin())) {
            // For ERP documents add Sold-to (while payer will be handled as sold-to)
            href_parameter.append("sold_to=" + headerBillingDoc.getPartnerKey(PartnerFunctionData.PAYER).getIdAsString());
        }
        // Expand href by billing indicator
        href_parameter.append("&billing=X");
        if ( extFwrd != null  &&  extFwrd.length() > 0) {
            href_parameter.append("&externalforward=").append(extFwrd);
        }

        billingStatusDetail.setState(DocumentState.VIEW_DOCUMENT);
        ManagedDocument mDoc = new ManagedDocumentLargeDoc(billingStatusDetail,
                                                   headerBillingDoc.getDocumentType(),
                                                   headerBillingDoc.getBillingDocNo(),
                                                   null,                    // NO external ref.No
                                                   null,                    // NO description
                                                   headerBillingDoc.getCreatedAt(),
                                                   "billing_status",
                                                   href_action,
                                                   href_parameter.toString());
        if (billingStatusDetail.isLargeDocument(bom.getShop().getLargeDocNoOfItemsThreshold())) {
            // create managed document for large documents
            ((ManagedDocumentLargeDoc) mDoc).setItemSearchProperty(ManagedDocumentLargeDoc.ITEM_PROPERTY_NONE);
            ((ManagedDocumentLargeDoc) mDoc).setItemSearchPropertyLowValue("");
            ((ManagedDocumentLargeDoc) mDoc).setItemSearchPropertyHighValue("");
        }
        documentHandler.add(mDoc);
        documentHandler.setOnTop(billingStatusDetail);
        // needed if the catalog is displayed and a document gets selected from the history
        documentHandler.setCatalogOnTop(false);
    }
      
    /**
     * Check, if the user is allowed to see the order and if the order should be 
     * be displayed as customer order.
     * 
     * @param orderStatus
     * @param buPaMa
     * @param companyPartnerFunction partner function of the company
     * @param isHomAcitvated within hom there is one additional check.
     */
    protected char checkOrder(OrderStatus orderStatus, 
                              User user,
                              BusinessPartnerManager buPaMa,
                              String companyPartnerFunction,
                              boolean isHomAcitvated,
                              String BpHierarchyName,
                              String BpHierarchyType) {

        ResultData dealerFamilyList = null;
        String loggedOnForID = "";

        HeaderSalesDocument header = orderStatus.getOrderHeader();
        if (BpHierarchyType != null && BpHierarchyName != null) {
            try { 
                dealerFamilyList = buPaMa.getDefaultBusinessPartner(PartnerFunctionData.SOLDTO).getPartnerFromHierachy(BpHierarchyType, BpHierarchyName);
            } catch (CommunicationException comEx){
				if (log.isDebugEnabled()) {
					log.debug("Error ", comEx);
				}
            }
        }


        if (orderStatus instanceof CustomerOrderStatus) {

            // take the soldFrom from header partner list 
            PartnerListEntry soldFromEntry = header.getPartnerList().getSoldFrom();

            TechKey soldFromKey = (soldFromEntry != null) ? soldFromEntry.getPartnerTechKey() : null;

            // get the default soldfrom for this scenario
            BusinessPartner defaultSoldFrom =
                buPaMa.getDefaultBusinessPartner(PartnerFunctionBase.SOLDFROM);                   
           
            // check if the soldfrom of the order is the the same as in the buPaMa
            if (soldFromKey != null && defaultSoldFrom != null
                    && soldFromKey.equals(defaultSoldFrom.getTechKey()) ) {
            
                return CHECK_OK;            
            }
        } 
        else {    
            if (isHomAcitvated) {

                // Within HOM scenario the order is either a re-order ... 
                if (checkPartnerFunction(buPaMa, dealerFamilyList, PartnerFunctionData.SOLDTO, header) == CHECK_OK) {
                    return CHECK_OK;
                }                   

                // or a customer order !
                if (checkPartnerFunction(buPaMa, dealerFamilyList, PartnerFunctionBase.SOLDFROM, header) == CHECK_OK) {
                    return CHECK_CUSTOMER_ORDER;            
                } 
            }

            else {
                char checkPartFlag = checkPartnerFunction(buPaMa, dealerFamilyList, companyPartnerFunction, header);
                if (checkPartFlag == CHECK_ERROR) { 
                    log.error(LogUtil.APPS_COMMON_SECURITY, "Authorization conflict: User '" + user.getUserId() + "' tried to display document '" + 
                              orderStatus.getSalesDocumentNumber() + "'"); 
                }
                return checkPartFlag;                
            }   
        }
        log.error(LogUtil.APPS_COMMON_SECURITY, "Authorization conflict: User '" + user.getUserId() + "' tried to display document '" + 
                  orderStatus.getSalesDocumentNumber() + "'"); 
        return CHECK_ERROR;
    }


    /**
     * Check, if the partner, which is the default in the buisness object manager
     * for the given partner function is the same as found in the header . <br>
     * 
     * @param buPaMa
     * @param partnerFunction
     * @param header
     * @return <code>CHECK_ERROR</code>, if the partner don't match.
     *          <code>CHECK_OK</code>, if the partner do match.
     */
    protected char checkPartnerFunction(BusinessPartnerManager buPaMa,
                                        ResultData dealerFamilyList, 
                                        String partnerFunction, 
                                        HeaderSalesDocument header) {
        
        char ret = CHECK_ERROR;
            
        // take the company Partner Function from header    
        PartnerListEntry partnerEntry = header.getPartnerList().getPartner(partnerFunction);
        
		// get the default partner fromthe bu pa ma
		BusinessPartner defaultPartner =
			 buPaMa.getDefaultBusinessPartner(partnerFunction);
			 
        if (log.isDebugEnabled()){
        	StringBuffer debugOutput = new StringBuffer("\ncheckPartnerFunction"); 
			debugOutput.append("\npartner function & document partner: "+ partnerFunction+", "+partnerEntry);
			if (partnerEntry != null){
				debugOutput.append("\ndocument partner key: "+ partnerEntry.getPartnerTechKey());
			}
			if (defaultPartner != null){
				debugOutput.append("\ndefault partner: "+defaultPartner.getTechKey());
			}
			log.debug(debugOutput.toString());
        }

        TechKey partnerKey =
             (partnerEntry != null) ? partnerEntry.getPartnerTechKey() : null;

             

        // check if the partner of the order is the the same as in the buPaMa
        if (partnerKey != null && defaultPartner != null
             && partnerKey.equals(defaultPartner.getTechKey()) ) {
         
             ret =  CHECK_OK;           
        }
        if (ret != CHECK_OK  &&  PartnerFunctionData.SOLDTO.equals(partnerFunction)  
         &&  defaultPartner != null  &&  dealerFamilyList != null  &&  partnerKey != null) {
            // check partner is belongs to the Business partner hierarchy (only SOLDTO-PARTY)
            dealerFamilyList.first();
            if (dealerFamilyList.searchRowByColumn("SOLDTO_TECHKEY", partnerKey.getIdAsString())) {
                // Part of the business partner hierarchy
                ret = CHECK_OK;
            }
        }
        
        return ret;
    }

    /**
     * Check visiblity of connected documents.
     * @param UserSessionData
     * @param Object (should be OrderStatus or BillingStatus)
     */
    protected void checkConnectedDocumentsVisibility(UserSessionData userSessionData, Object statusObject ) {
        StartupParameter startupParameter = (StartupParameter)userSessionData.getAttribute(IsaCoreInitAction.SC_STARTUP_PARAMETER);
        if (statusObject instanceof OrderStatus) {
            if (! startupParameter.isBillingIncluded()) {
                // Set billing documents to not displayable if not included in the shop
                List successorList = ((OrderStatus)statusObject).getOrderHeader().getSuccessorList();
                ConnectedDocument successorDoc;
                for (int i = 0; i < successorList.size(); i++) {
                    successorDoc = (ConnectedDocument)successorList.get(i);
                    if ("CRMBE".equals(successorDoc.getAppTyp())) {
                        successorDoc.setDisplayable(false);
                    }
                }
            }
        }
    }
    /**
     * Method to retrieve the DocumentHandler. If billing docuemnts run outside ISales frames 
     * (isBillingIncluded = false), then the appropriated document handler has to be returned.
     * @return DocumentHandler docHandler
     */
    protected DocumentHandler getDocumentHandler(UserSessionData userSessionData) {
        String applArea = (String)userSessionData.getAttribute(ActionConstants.SC_APPLAREA);
        DocumentHandler docHandler = null;
        if ( ActionConstants.APPLAREA_SEPBILL.equals(applArea)) {
            docHandler = (DocumentHandler) userSessionData.getAttribute(ActionConstantsBase.DOCUMENT_HANDLER_BILL);
        } else {
            docHandler = (DocumentHandler) userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
        }
        return docHandler;
    }
    /**
     * Return <code>true</code> if the current Application area is the separate billing screen.
     * @return boolean
     */
    protected boolean isSeparateBilling(UserSessionData userSessionData) {
        return (ActionConstants.APPLAREA_SEPBILL.equals(userSessionData.getAttribute(ActionConstants.SC_APPLAREA)));
    }

    /**
     * Sets the error messages from the sales document into the request. Be aware that the forward to the message page
     * must be set separately!
     * @param request             HTTP request
     * @param documentHandler     the document handler
     * @param nextAction          next Struts action which should be called after message page
     * @param nextActionParameter Parameters for the next Struts action
     * @param salesDocument       the involved sales document
     * @return booelan            Returns true if just an exception is available
     */
    static protected boolean createErrorMessage(
        HttpServletRequest request,
        DocumentHandler documentHandler,
        String nextAction,
        String nextActionParameter,
        OrderStatus orderStatus,
        BORuntimeException BORex) {
  
        boolean retVal = false;
        
        // create a Message Displayer for the error page!
        MessageDisplayer messageDisplayer = new MessageDisplayer();
        
        messageDisplayer.addToRequest(request);
        if (orderStatus.hasMessages() == true) {
            messageDisplayer.copyMessages(orderStatus);
        } else {
            // Only exception available add it, and set retVal to true. This
            // will allow to navigate to an error page instead to the message page
            // where the exception itself is not visible!
            request.setAttribute(ContextConst.EXCEPTION, BORex);
            retVal = true;
        }
        
        // the action which is to call after the message
        messageDisplayer.setAction(nextAction);
        
        messageDisplayer.setActionParameter(nextActionParameter);

        return retVal;
    }
    
}