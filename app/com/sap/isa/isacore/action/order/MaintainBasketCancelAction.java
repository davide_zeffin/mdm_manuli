/*****************************************************************************
  Class:        MaintainBasketCancelAction
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Thomas Smits
  Created:      10.09.2001
  Version:      1.0

  $Revision: #3 $
  $Date: 2001/10/29 $
*****************************************************************************/

package com.sap.isa.isacore.action.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businessobject.businessevent.DropDocumentEvent;
import com.sap.isa.businessobject.order.Basket;
import com.sap.isa.businessobject.ordertemplate.OrderTemplate;
import com.sap.isa.businessobject.quotation.Quotation;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.action.IsaCoreInitAction;

/**
 * Action to cancel the ordering process. The actual document is
 * thrown away.
 * <p>
 * The following forwards are used by this action:
 * </p>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>FORWARD_UPDATEDOCUMENT</td><td>always used</td></tr>
 * </table>
 *
 * @author Thomas Smits
 * @version 1.0
 */
public class MaintainBasketCancelAction extends MaintainBasketBaseAction {

    /**
     * Creates a new instance of this class.
     */
    public MaintainBasketCancelAction() {
    }

    /**
     * Overwritten method from <code>MaintainBasketBaseAction</code>.
     */
    protected String basketPerform(HttpServletRequest request,
                HttpServletResponse response,
                HttpSession session,
                UserSessionData userSessionData,
                RequestParser parser,
                BusinessObjectManager bom,
                IsaLocation log,
                IsaCoreInitAction.StartupParameter startupParameter,
                BusinessEventHandler eventHandler,
                boolean multipleInvocation,
                boolean browserBack,
                Shop shop,
                SalesDocument preOrderSalesDocument,
                boolean auction,
                DocumentState targetDocument,
                DocumentHandler documentHandler)
                    throws CommunicationException {

		final String METHOD_NAME = "basketPerform()";
		log.entering(METHOD_NAME);
        DropDocumentEvent dropDocumentEvent = new DropDocumentEvent(preOrderSalesDocument);
        eventHandler.fireDropDocumentEvent(dropDocumentEvent);

        preOrderSalesDocument.destroyContent();       // may throw CommunicationException
        if (preOrderSalesDocument instanceof Quotation) {
            documentHandler.release((Quotation)preOrderSalesDocument);
            bom.releaseQuotation();
        }
        else if (preOrderSalesDocument instanceof OrderTemplate) {
            documentHandler.release((OrderTemplate)preOrderSalesDocument);
            bom.releaseOrderTemplate();
        }
        else {
            documentHandler.release((Basket)preOrderSalesDocument);
            bom.releaseBasket();
        }
		log.exiting();
        return FORWARD_UPDATEDOCUMENT;
    }
}