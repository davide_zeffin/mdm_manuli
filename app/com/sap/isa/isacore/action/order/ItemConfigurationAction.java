/*****************************************************************************
    Class         ItemConfigurationAction
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP AG
    Created:      29.04.2001
    Version:      1.0

    $Revision: #14 $
    $Date: 2001/10/29 $
*****************************************************************************/
package com.sap.isa.isacore.action.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.ipc.IPCBOManager;
import com.sap.isa.businessobject.item.ItemList;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.businessobject.order.Basket;
import com.sap.isa.businessobject.order.Order;
import com.sap.isa.businessobject.order.OrderChange;
import com.sap.isa.businessobject.order.OrderStatus;
import com.sap.isa.businessobject.ordertemplate.OrderTemplate;
import com.sap.isa.businessobject.quotation.Quotation;
import com.sap.isa.core.FrameworkConfigManager;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.ipc.ui.jsp.action.RequestParameterConstants;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.spc.remote.client.object.OriginalRequestParameterConstants;
import com.sap.spc.remote.client.object.imp.IPCClientObjectFactory;
import com.sap.spc.remote.client.object.imp.rfc.RFCIPCItemReference;


/**
 * Action to configure an item from the basket.
 *
 * @author Thomas Smits
 * @version 1.0
 */
public class ItemConfigurationAction extends IsaCoreBaseAction {
    private static final String FORWARD_IPC_CONFIGURATION = "success";
//    private static final String CALLER_B2B_BASKET = "b2b_basket";  centralized in com.sap.spc.remote.client.object.OriginalRequestParameterConstants
//    private static final String CALLER_B2C_BASKET = "b2c_basket";
//    private static final String CALLER_B2C_ORDERSTATUS = "b2c_orderstatus";
//    private static final String CALLER_B2B_ORDER = "b2b_order";
    public static final String  SC_CONFIGURATION_ACTIVE = "com.sap.isa.isacore.action.order.ItemConfigurationAction.configactive";
    public static final String  SC_ITEM_IN_CONFIG = "com.sap.isa.isacore.action.order.ItemConfigurationAction.iteminconfig";
    public static final String  CONFIG_ACTIVE_TRUE = "TRUE";
    public static final String  CONFIG_ACTIVE_FALSE = "FALSE";

    /**
     * Name of the request parameter for the shipto index.
     */
    public static final String PARAM_ITEMID = "itemId";
    public static final String PARAM_B2C_BASKET = "b2cBasket";

    //    public ItemConfigurationAction() {}

    /**
     * Overriden <em>isaPerform</em> method of <em>IsaCoreBaseAction</em>.
     */
    public ActionForward isaPerform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response, UserSessionData userSessionData, RequestParser parser, BusinessObjectManager bom, IsaLocation log) throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        RFCIPCItemReference ipcItemReference = (RFCIPCItemReference)IPCClientObjectFactory.getInstance().newIPCItemReference();

        ItemList         itemList;
        HeaderSalesDocument header;

        TechKey          techKey = null;
        boolean          b2cBasket;

        // in order to avoid that the user changes or deletes an item when it is
        // in a configuration session put the item in the session context
        //  if (userSessionData.getAttribute(SC_CONFIGURATION_ACTIVE) != CONFIG_ACTIVE_TRUE) {
        //      userSessionData.setAttribute(SC_CONFIGURATION_ACTIVE, CONFIG_ACTIVE_TRUE);
        //      userSessionData.setAttribute(SC_ITEM_IN_CONFIG, techKey.getIdAsString());
        //  }
        if (parser.getAttribute("b2cBasket").isSet()) {
            b2cBasket = parser.getAttribute("b2cBasket").getValue().getBoolean();
        }
        else {
            b2cBasket = parser.getParameter("b2cBasket").getValue().getBoolean();
        }

        if (b2cBasket == true) {
            // Konfiguration of an item from the basket
            if (parser.getAttribute("itemId").isSet()) {
                techKey = new TechKey(parser.getAttribute("itemId").getValue().getString());
            }
            else {
                techKey = new TechKey(parser.getParameter("itemId").getValue().getString());
            }

            Basket basket = bom.getBasket();

            // Basket or OrderStatus
            if (basket == null) {
                // OrderStatus
                OrderStatus orderStatus = bom.getOrderStatus();
                Order       order = (Order) orderStatus.getOrder();
                header = (HeaderSalesDocument) orderStatus.getOrder().getHeaderData();
                order.getItemConfig(techKey);
                itemList = orderStatus.getItemList();
            }
            else {
                basket.getItemConfig(techKey);
                itemList     = basket.getItems();
                header       = basket.getHeader();
            }

            if (itemList == null) {
				log.exiting();
                throw new PanicException("****ItemConfigurationAction: Itemlist is empty");
            }

            userSessionData.setAttribute(SC_ITEM_IN_CONFIG, techKey);

            // Basket or OrderStatus
            if (basket != null) {
                request.setAttribute("caller", OriginalRequestParameterConstants.CALLER_B2C_BASKET);
            }
            else {
                request.setAttribute("caller", OriginalRequestParameterConstants.CALLER_B2C_ORDERSTATUS);
            }
        }
        else {
            // get sales document on top from document handler
            techKey = new TechKey(userSessionData.getAttribute(SC_ITEM_IN_CONFIG).toString());

            DocumentHandler documentHandler = (DocumentHandler) userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);

            if (documentHandler == null) {
				log.exiting();
                throw new PanicException("No document handler found");
            }

            DocumentState targetDocument = documentHandler.getManagedDocumentOnTop();

            if (targetDocument == null) {
				log.exiting();
                throw new PanicException("No Document found");
            }

            DocumentState document = ((ManagedDocument) targetDocument).getDocument();
            SalesDocument posd = null;

            if (document instanceof Basket) {
                posd = bom.getBasket();
            }
            else if (document instanceof Quotation) {
                posd = bom.getQuotation();
            }
            else if (document instanceof OrderTemplate) {
                posd = bom.getOrderTemplate();
            }

            if (posd != null) {
                // get the current sales document
                // Konfiguration of an item from the basket
                posd.getItemConfig(techKey);
                itemList     = posd.getItems();
                header       = posd.getHeader();

                if (itemList == null) {
					log.exiting();
                    throw new PanicException("****ItemConfigurationAction: Itemlist is empty");
                }

                request.setAttribute("caller", OriginalRequestParameterConstants.CALLER_B2B_BASKET);
            }
            else if (document instanceof Order) {
                // Konfiguration of an item from the order
                OrderChange order = new OrderChange(bom.getOrder());

                try {
                    order.readForUpdate();
                }
                catch (Exception ex) {
					log.exiting();
                    return mapping.findForward("error");
                }

                order.getItemConfig(techKey);
                itemList = order.getItems();

                if (itemList == null) {
					log.exiting();
                    throw new PanicException("****ItemConfigurationAction: Itemlist is empty");
                }

                header = order.getHeader();
                request.setAttribute(RequestParameterConstants.CALLER, OriginalRequestParameterConstants.CALLER_B2B_ORDER);
            }

            // RW20010712
            else if (document instanceof OrderStatus) {
                OrderStatus orderStatus = bom.getOrderStatus();
                Order       order = (Order) orderStatus.getOrder();
                header = (HeaderSalesDocument) orderStatus.getOrder().getHeaderData();
                order.getItemConfig(techKey);
                itemList = orderStatus.getItemList();
                request.setAttribute(RequestParameterConstants.CALLER, RequestParameterConstants.CALLER_B2B_ORDERSTATUS);
                request.setAttribute(RequestParameterConstants.DISPLAY_MODE, RequestParameterConstants.T);
            }

            // RW20010712
            else {
				log.exiting();
                throw new PanicException("Product-configuration is not implemented for this document type");
            }
        }

        // Find the item for the given technical key
        ItemSalesDoc item = itemList.get(techKey);

        if (item == null) {
			log.exiting();
            throw new PanicException("Invalid item GUID provided");
        }

        if (item.isConfigurableChangeable() == false) {
            request.setAttribute(RequestParameterConstants.DISPLAY_MODE, RequestParameterConstants.T);
        }
        else {
            request.setAttribute(RequestParameterConstants.DISPLAY_MODE, RequestParameterConstants.F);
        }

        request.setAttribute(RequestParameterConstants.IPC_SCENARIO, "ISA");

        // activate automatic variantsearch
        request.setAttribute(RequestParameterConstants.ENABLE_VARIANT_SEARCH, RequestParameterConstants.T);
        request.setAttribute(RequestParameterConstants.ENABLE_AUTOMATIC_SEARCH, RequestParameterConstants.T);

        // set attribute only_var_find
        if (item.getOnlyVarFind() == true) {
            request.setAttribute(RequestParameterConstants.ONLY_VAR_FIND, RequestParameterConstants.T);
        }
        else {
            request.setAttribute(RequestParameterConstants.ONLY_VAR_FIND, RequestParameterConstants.T);
        }

        if (header.getIpcDocumentId() != null) {
            ipcItemReference.setDocumentId(header.getIpcDocumentId().getIdAsString());
        }

        ipcItemReference.setItemId(item.getTechKey().getIdAsString());
        ipcItemReference.setConnectionKey(header.getIpcConnectionKey());

        // get contract filter (if existing)
        String contractFilter = item.getContractConfigFilter();

        if ((contractFilter != null) && (contractFilter.length() > 0)) {
            request.setAttribute("restrictId", contractFilter);
            request.setAttribute("ownerType", "CONTRACT");
            request.setAttribute("repositoryType", "IBASE");
            request.setAttribute("ownerId", item.getContractKey().getIdAsString());
        }

        IPCBOManager ibom = this.getIPCBusinessObjectManager(userSessionData);

        // ServletContext  servletContext = getServlet().getServletContext();
        request.setAttribute(RequestParameterConstants.IPC_ITEM_REFERENCE, ipcItemReference);

        //       String configOnlineEval = servletContext.getInitParameter("configOnlineEvaluate.isa.sap.com");
        //     Parameter configOnlineEvaluate moved from web.xml to xcm-configuration (4.0 SP06)
        String attributeName = "configOnlineEvaluate";
        String configOnlineEval = FrameworkConfigManager.Servlet.getInteractionConfigParameter(request.getSession(), "ui", attributeName, attributeName + ".isa.sap.com");

        if ((configOnlineEval != null) && (configOnlineEval.equalsIgnoreCase("true"))) {
            request.setAttribute(RequestParameterConstants.ONLINE_EVALUATE, RequestParameterConstants.T);
        }
        else {
            request.setAttribute(RequestParameterConstants.ONLINE_EVALUATE, RequestParameterConstants.F);
        }
		log.exiting();
        return mapping.findForward(FORWARD_IPC_CONFIGURATION);
    }
}
