/*****************************************************************************
    Class         ItemConfigurationB2BAction
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Dendl
    Created:      20.09.2001
    Version:      1.0

    $Revision: #2 $
    $Date: 2001/10/29 $
*****************************************************************************/

package com.sap.isa.isacore.action.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.item.ItemList;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.businessobject.order.Basket;
import com.sap.isa.businessobject.order.CustomerOrderChange;
import com.sap.isa.businessobject.order.DocumentListFilter;
import com.sap.isa.businessobject.order.Order;
import com.sap.isa.businessobject.order.OrderChange;
import com.sap.isa.businessobject.order.OrderStatus;
import com.sap.isa.businessobject.ordertemplate.OrderTemplate;
import com.sap.isa.businessobject.quotation.Quotation;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.spc.remote.client.object.OriginalRequestParameterConstants;
import com.sap.spc.remote.client.object.IPCItem;

/**
 * Action to configure an item
 *
 * @author Stefan Dendl
 * @version 1.0
 */
public class ItemConfigurationB2BAction extends ItemConfigurationBaseAction {

    private static final String FORWARD_IPC_CONFIGURATION = "success";

    /**
     * Name of the request parameter
     */
    public static final String PARAM_ITEMID = "itemId";

    /**
     * Overriden <em>isaPerform</em> method of <em>IsaCoreBaseAction</em>.
     */
    public ActionForward isaPerform(ActionMapping mapping,
                ActionForm form,
                HttpServletRequest request,
                HttpServletResponse response,
                UserSessionData userSessionData,
                RequestParser parser,
                BusinessObjectManager bom,
                IsaLocation log)
                        throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        ItemList itemList;
        HeaderSalesDocument header;
        String forward = FORWARD_IPC_CONFIGURATION;
        
        boolean isDebugEnabled = log.isDebugEnabled();
        
        // only used in hom while processing customer order
        boolean setReadOnly = false;

        TechKey techKey = null;

        techKey = new TechKey(userSessionData.getAttribute(SC_ITEM_IN_CONFIG).toString());

        // get sales document on top from document handler
        DocumentHandler documentHandler = (DocumentHandler)
        userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
        if (documentHandler == null) {
			log.exiting();
            throw new PanicException("No document handler found");
        }

        ManagedDocument managedDocument = documentHandler.getManagedDocumentOnTop();
        if (managedDocument == null) {
			log.exiting();
           	throw new PanicException("No Document found");
        }

        DocumentState document = managedDocument.getDocument();
        SalesDocument posd = null;

        // get the current sales document
        if (document instanceof Basket) {
            posd = (SalesDocument) document;
        }
        else if (document instanceof Quotation) {
            posd = (SalesDocument) document;
        }
        else if (document instanceof OrderTemplate) {
            posd = (SalesDocument) document;
        }

        boolean deleteExternalItem = true;
        
        if (posd != null) {
         // Configuration of an item of a preorder sales document
            itemList = posd.getItems();
            header = posd.getHeader();
            // as the item list is stateless, after its deletion in the object layer
            // we have to read it from backend again!
            if (itemList.size() == 0) {
                posd.readAllItemsForUpdate();
                itemList = posd.getItems();
                posd.readHeader();
                header = posd.getHeader();
            }
            //check, if item still exists (could have been deleted)
            if (itemList == null || itemList.get(techKey) == null) {
				log.debug("Item not foud: " + techKey);
				return mapping.findForward("backtoorder");	
            } 
            else {
	            posd.getItemConfig(techKey);
            }

			// set the dirty flag in order to redetermine the configuration of the product
			// after the configuration was finished            
            posd.setDirty(true);
        }
        
        else if (document instanceof Order) {
            
            // Configuration of an item of an orderChange
            if (isDebugEnabled) { 
            	log.debug("Konfig. Order-Item: Guid = " +  techKey);
            }

            OrderChange orderChange = MaintainOrderBaseAction.getOrderChangeDecorator((SalesDocument)document);
            
            try {
                orderChange.readForUpdate();
            }
            catch (Exception ex) {
				log.exiting();
                return mapping.findForward("error");
            }
            
            if (isDebugEnabled) { 
            	log.debug("Order-Guid: " + orderChange.getTechKey());
            }
            
			itemList = orderChange.getItems();
            
			if (itemList == null) {
				log.exiting();
			   throw new PanicException("****ItemConfigurationAction: Itemlist is empty");
			}
			            
			//check, if item still exists (could have been deleted)
			if (itemList.get(techKey) == null) {
				log.debug("Item not foud: " + techKey);
				return mapping.findForward("backtoorder");	
			}    
			       
            orderChange.getItemConfig(techKey);
            
            header = orderChange.getHeader();
			
			if (orderChange instanceof CustomerOrderChange) {            	
		        // check if we currently process the customer order
		        // while processing the customer order configuration is not allowed.
		        ManagedDocument currentDocument = managedDocument.getAssociatedDocument();
		        if (currentDocument != null && currentDocument instanceof ManagedDocumentProcessCustomerOrder) {
		            setReadOnly = true;
            	}
			}
			// set the dirty flag in order to redetermine the configuration of the product
			// after the configuration was finished
			orderChange.setDirty(true);  	    

        }
        else if (document instanceof OrderStatus) {
            deleteExternalItem = false;
            OrderStatus orderStatus = (OrderStatus)document;
			header = (HeaderSalesDocument)orderStatus.getOrderHeader();
            itemList = orderStatus.getItemList();
            orderStatus.getItemConfig(DocumentListFilter.SALESDOCUMENT_TYPE_ORDER, techKey);
            
        }
        else {
			log.exiting();
           	throw new PanicException("Product-configuration is not implemented for this document type");
        }
        
        // Find the item for the given technical key
        ItemSalesDoc item =  itemList.get(techKey);

        if (item == null) {
        	// item could have be deleted
			forward = "backtoorder";
        }
		else if ("0".equals(item.getQuantity())) {	
			log.debug("Item quantity is 0, don't start configuration");
			return mapping.findForward("backtoorder");
		} 
        else {
	        if (setReadOnly) {
	            item.setConfigurableChangeable(false);
	        }
	               
	        if (isItemConfigurableInIPC(item)) {
	            // set request parameter to transfer to IPC
	            setConfigurationParams(OriginalRequestParameterConstants.CALLER_B2B_ORDER, item, header, deleteExternalItem, userSessionData, request);
	        }
	        else {
	            if (item.getExternalItem() != null && item.getExternalItem() instanceof IPCItem) {
	                log.error("IPCItem says it is not configurable - ext_config = " + ((IPCItem) item.getExternalItem()).getConfig());
	            }
	            else {
	                log.error("IPCItem says it is not configurable - ext_config = " + item.getExternalItem());
	            }
	            
	            forward = "backtoorder";
	        }
		}
		log.exiting();
        return mapping.findForward(forward);

    }
}