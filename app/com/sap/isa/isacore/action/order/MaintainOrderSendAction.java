/*****************************************************************************
  Class:        MaintainOrderSendAction
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:
  Created:      18.09.2001
  Version:      1.0

  $Revision: #8 $
  $Date: 2002/08/21 $
*****************************************************************************/

package com.sap.isa.isacore.action.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.ManagedDocumentDetermination;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.order.CollectiveOrderChange;
import com.sap.isa.businessobject.order.CustomerOrderChange;
import com.sap.isa.businessobject.order.OrderChange;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.action.DeterminationAction;
import com.sap.isa.isacore.action.IsaCoreInitAction;
import com.sap.isa.isacore.action.PaymentActions;
import com.sap.isa.isacore.action.PrepareDeterminationAction;

/**
 * Action to perform the final step in the processing of an document (i.e. to
 * save it in the backend).
 * <p>
 * The following forwards are used by this action:
 * </p>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>FORWARD_CHANGEORDER</td><td>something went wrong during the save process</td></tr>
 *   <tr><td>FORWARD_CONFIRMATION_SCREEN</td><td>show confirmation that save was successful</td></tr>
 * </table>
 *
 * @author
 * @version 1.0
 */
public class MaintainOrderSendAction extends MaintainOrderBaseAction {
	
    protected String orderPerform(HttpServletRequest request,
                HttpServletResponse response,
                HttpSession session,
                UserSessionData userSessionData,
                RequestParser parser,
                BusinessObjectManager bom,
                IsaLocation log,
                IsaCoreInitAction.StartupParameter startupParameter,
                BusinessEventHandler eventHandler,
                boolean multipleInvocation,
                boolean browserBack,
                Shop shop,
                OrderChange order,
                DocumentState targetDocument,
                DocumentHandler documentHandler)
                    throws CommunicationException {
		final String METHOD_NAME = "orderPerform()";
		log.entering(METHOD_NAME);
        String forwardTo = null;

        boolean isDebugEnabled = log.isDebugEnabled();

   /*     RequestParser.Parameter delete =
                parser.getParameter("delete[]");

        RequestParser.Parameter cancel =
                parser.getParameter("cancel[]");

        int numLines = parser.getParameter("product[]").getNumValues();

        // submit all data
        parseRequest(parser, userSessionData, order, log);

        TechKey[] cancelItems = new TechKey[cancel.getNumValues()];

        int k = 0;

        for (int i = 1; i <= numLines; i++) {
            if (cancel.getValue(i).isSet()) {
              cancelItems[k++] = new TechKey(cancel.getValue(i).getString());
            }
        }

        TechKey[] deleteItems = new TechKey[delete.getNumValues()];

        k = 0;

        for (int i = 1; i <= numLines; i++) {
            if (delete.getValue(i).isSet()) {
              deleteItems[k++] = new TechKey(delete.getValue(i).getString());
            }
        }

        if (deleteItems.length > 0) {
           order.deleteItems(deleteItems);
        }

        if (cancelItems.length > 0) {
           order.cancelItems(cancelItems);
        }

        // possible customer exit, to parse request
        customerExitParseRequest(parser, userSessionData, order);

        order.update(bom.createBUPAManager());
        
        */
        
        RequestParser.Parameter calledFromDetermination =
                parser.getParameter(DeterminationAction.RC_CALLED_FROM_DET);
        
        // In case the action is called from the product determination action there is nothing
        // to be parsed
        if (!calledFromDetermination.isSet()) {
            updateOrder(parser, 
                        userSessionData, 
                        order, 
                        shop, 
                        bom.getBUPAManager(), 
                        log,
                        request);
        }
        
        if (order.isDeterminationRequired()) {
            request.setAttribute(PrepareDeterminationAction.RC_DET_CALLER, ManagedDocumentDetermination.CALLER_MAINTAIN_ORDER_SEND);
            request.setAttribute(PrepareDeterminationAction.RC_DET_DOC_MODE, ManagedDocumentDetermination.DOC_MODE_CHANGE);
			log.exiting();
            return FORWARD_PRODDET;
        }
        
        //  order must be read first in order to check validity
        order.readForUpdate(bom.getShop());
        
        if (order.getHeader().hasATPSubstOccured()) {
            if (isDebugEnabled) {
                log.debug("ATP substitution took place");
            }
            userSessionData.setAttribute(MaintainBasketSendAction.SC_ATPSUBST, "true");
			log.exiting();
            return FORWARD_CHANGEORDER;
        }

        if (shop.isOnlyValidDocumentAllowed() && !order.isvalid() ) {
            if (isDebugEnabled) {
                log.debug("document NOT valid");
                log.debug("message provided: " + order.getMessageList());
            }
            request.setAttribute(RC_HEADER, order.getHeader());
            request.setAttribute(RC_ITEMS, order.getItems());
            request.setAttribute(RC_SHIPCOND, order.readShipCond(shop.getLanguage()));
            request.setAttribute(RC_MESSAGES, order.getMessageList());

			// roll back: needed for changes in the collective order in the hom sceanrio
			shop.rollBack();
            forwardTo = FORWARD_CHANGEORDER;

        }
        else {
            // Checkout process for Auction order
			String auctionProcessType = shop.getAuctionOrderType();
			if (auctionProcessType != null
				&& auctionProcessType.equalsIgnoreCase(
					order.getHeader().getProcessType())) {
				userSessionData.removeAttribute("EAUCTION_PARAMID");		
				return "eauctionconfirm";
			} 
			if (!PaymentActions.isPaymentMaintenanceAvailable(userSessionData, order.getSalesDocument())) {	
			 	order.saveChanges();
			} else {
			  // does any payment error exist?	
				int retCode = order.saveOrderChanges();
			  	if (retCode != 0) {
					return FORWARD_CHANGEORDER;	   
			  	}
			 }
		   //order must be read first in order to read sub-items also if any
		   order.readAllItems();
           request.setAttribute(MaintainOrderBaseAction.RC_HEADER, order.getHeader());
           request.setAttribute(MaintainOrderBaseAction.RC_ITEMS, order.getItems());
           request.setAttribute(MaintainOrderBaseAction.RC_SHIPCOND, order.readShipCond(shop.getLanguage()));
           request.setAttribute(MaintainOrderBaseAction.RC_MESSAGES, order.getMessageList());
           userSessionData.setAttribute(SC_SHIPTOS,  order.getShipTos());
           forwardTo = FORWARD_CONFIRMATION_SCREEN;
           
           if (order instanceof CollectiveOrderChange) {
                forwardTo = "cancel";
           } 
           if (order instanceof CustomerOrderChange) {
                 
                 SalesDocument salesDoc = (SalesDocument)targetDocument;
                 salesDoc.readHeader();
                 HeaderSalesDocument header = salesDoc.getHeader();
                 
                 String docType = header.getDocumentType();
                 String techkey = salesDoc.getTechKey().getIdAsString();
                             
           	     request.setAttribute("techkey", techkey);
           	     request.setAttribute("object_id", docType);
           	
                 forwardTo = "display";
           } 
        }
        
		log.exiting();
        return forwardTo;
    }
}