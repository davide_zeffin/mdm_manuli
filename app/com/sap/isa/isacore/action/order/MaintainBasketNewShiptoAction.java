/*****************************************************************************
  Class:        MaintainBasketNewShiptoAction
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Thomas Smits
  Created:      10.09.2001
  Version:      1.0

  $Revision: #8 $
  $Date: 2002/02/12 $
*****************************************************************************/

package com.sap.isa.isacore.action.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.ShipTo;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.action.ActionConstants;
import com.sap.isa.isacore.action.IsaCoreInitAction;
import com.sap.isa.isacore.action.ShowShipToAction;

/**
 * Action to add a new ship to to a document.
 * <p>
 * The following forwards are used by this action:
 * </p>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <td><b>forward</b></td>
 *     <td><b>description</b></td>
 *   <tr>
 *   <tr><td>FORWARD_SHOWBASKET</td><td>return to the document screen if something with the data of that document is not ok</td></tr>
 *   <tr><td>FORWARD_NEW_SHIPTO</td><td>display screen to enter data of the new ship to</td></tr>
 * </table>
 *
 * @author Thomas Smits
 * @version 1.0
 */
public class MaintainBasketNewShiptoAction extends MaintainBasketBaseAction {

    /**
     * Creates a new instance of this class.
     */
    public MaintainBasketNewShiptoAction() {
    }

	/**
	 * Name of the request parameter for the shipto handle.
	 */
	public static final String SHIPTO_HANDLE = "shipToHandle";

    protected String basketPerform(HttpServletRequest request,
                HttpServletResponse response,
                HttpSession session,
                UserSessionData userSessionData,
                RequestParser parser,
                BusinessObjectManager bom,
                IsaLocation log,
                IsaCoreInitAction.StartupParameter startupParameter,
                BusinessEventHandler eventHandler,
                boolean multipleInvocation,
                boolean browserBack,
                Shop shop,
                SalesDocument preOrderSalesDocument,
                boolean auction,
                DocumentState targetDocument,
                DocumentHandler documentHandler)
                    throws CommunicationException {
		final String METHOD_NAME = "basketPerform()";
		log.entering(METHOD_NAME);
        String forwardTo = null;

        // possible customer exit, to parse request
        customerExitParseRequest(parser, userSessionData, preOrderSalesDocument);

        preOrderSalesDocument.update(bom.createBUPAManager(), shop);        // may throw CommunicationException

        processCustomerNumberFields(parser, preOrderSalesDocument, shop, bom.createBUPAManager());
        
		preOrderSalesDocument.readHeader();         
		preOrderSalesDocument.readAllItemsForUpdate();
		
        // if there are any error messages for the preOrderSalesDocument/ items, you can not
        // order. The user stays on the same page.
        if (!preOrderSalesDocument.isValid() ) {
            forwardTo = FORWARD_SHOWBASKET;
        }
        else {

          String itemKey = request.getParameter("newshipto");
          if (itemKey.equals("header")) {
              // determine the address of the shipto of the actual item for display
              ShipTo headShipTo = preOrderSalesDocument.getHeader().getShipTo();
              if (headShipTo != null) {
                request.setAttribute(ActionConstants.RC_ADDRESS, headShipTo.getAddress());
                request.setAttribute(ShowShipToAction.PARAM_SHIPTO_SOURCE, ShowShipToAction.VALUE_SHIPTO_FROM_RC);
                request.setAttribute(MaintainBasketNewShiptoAction.SHIPTO_HANDLE, headShipTo.getHandle());
              }
              itemKey = "";
          }
          else {
              // determine the address of the shipto of the actual item for display
              ShipTo itemShipTo = preOrderSalesDocument.getItems().get(new TechKey(itemKey)).getShipTo();
              if (itemShipTo != null) {
                request.setAttribute(ActionConstants.RC_ADDRESS, itemShipTo.getAddress());
                request.setAttribute(ShowShipToAction.PARAM_SHIPTO_SOURCE, ShowShipToAction.VALUE_SHIPTO_FROM_RC);
                request.setAttribute(ShowShipToAction.RC_SHIPTO_KEY, itemShipTo.getTechKey().getIdAsString());
                request.setAttribute(MaintainBasketNewShiptoAction.SHIPTO_HANDLE, itemShipTo.getHandle());
              }
          }

          request.setAttribute(ShowShipToAction.RC_ITEMKEY, itemKey);
          forwardTo = FORWARD_NEW_SHIPTO;

        }
		log.exiting();
        return forwardTo;
    }
}