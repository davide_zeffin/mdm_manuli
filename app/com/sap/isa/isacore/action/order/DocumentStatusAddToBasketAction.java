/*****************************************************************************
    Class:        DocumentStatusAddToBasketAction
    Copyright (c) 2000, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      May 2001

    $Revision: #5 $
    $Date: 2002/12/06 $
*****************************************************************************/
package com.sap.isa.isacore.action.order;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.businessobject.ipc.IPCBOManager;
import com.sap.isa.businessobject.item.BasketTransferItemImpl;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.businessobject.order.CampaignList;
import com.sap.isa.businessobject.order.DocumentListFilter;
import com.sap.isa.businessobject.order.ExtRefObjectList;
import com.sap.isa.businessobject.order.ExternalReferenceList;
import com.sap.isa.businessobject.order.OrderStatus;
import com.sap.isa.businessobject.order.Text;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.RequestParser;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.imp.IPCClientObjectFactory;
import com.sap.spc.remote.client.object.imp.rfc.RFCIPCItemReference;
import com.sap.spc.remote.client.util.ext_config_writable;
import com.sap.spc.remote.client.util.ext_configuration;


/**
 * Retrievs items from HTTP request and transfers those to the basket
 */
public class DocumentStatusAddToBasketAction extends DocumentStatusBaseAction {
	
	public static final String NON_CATALOG_PRODUCTS_ALLOWED = "nonCatalogProductsAllowed";

    /**
     * Perform order status.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param orderStatus       The order status object
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward performOrderStatus(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response, UserSessionData userSessionData, RequestParser requestParser, BusinessObjectManager bom, OrderStatus orderStatus, IsaLocation log) throws CommunicationException {
		final String METHOD_NAME = "performOrderStatus()";
		log.entering(METHOD_NAME);
        List                   transferItems = null;
        BasketTransferItemImpl transferItem = null;
        ItemSalesDoc           itemSalesDoc = null;
        Shop                   shop = null;
        User                   user = null;

        // read the shop data
        shop = bom.getShop();

        if (shop == null) {
			log.exiting();
            throw new PanicException("shop.notFound");
        }

        // get user
        user = bom.getUser();

        if (user == null) {
			log.exiting();
            return mapping.findForward("login");
        }
 
        // create a new list of basket transfer items
        transferItems = new ArrayList();

        RequestParser.Parameter checkbox = requestParser.getParameter((DocumentStatusDetailAction.RK_CHECKBOXNAME + "[]"));
        RequestParser.Parameter checkboxhidden = requestParser.getParameter((DocumentStatusDetailAction.RK_CHECKBOXHIDDEN + "[]"));

        if (checkbox != null) {
            String on;
            String techKeyValue;

            for (int i = 0; i < checkboxhidden.getNumValues(); i++) {
                // Because it is a simulated index which is overtaken from JSP it starts with 1 (ONE)!!
                on               = checkbox.getValue(i + 1).getString();
                techKeyValue     = checkboxhidden.getValue(Integer.toString(i + 1)).getString();

                if (!(on.equals(""))) { // i'th checkbox marked

                    // CREATE TransferItem object
                    transferItem     = new BasketTransferItemImpl();

                    // Get item from the orderstatus
                    itemSalesDoc = orderStatus.getItemByTechKey(new TechKey(techKeyValue.trim()));
                    
                    if(itemSalesDoc == null){
                    	Iterator it = orderStatus.getItemsIterator();
                    	if(it != null){
							ItemSalesDoc item = null;
							while(it.hasNext()){
								item = (ItemSalesDoc)it.next();
								if(item.getTechKey().getIdAsString().equals(techKeyValue)){
									itemSalesDoc = item;
									break;
								}
							}
                    	}
                    }

                    // FILL Transferitem object
                    if (!(itemSalesDoc.getContractKey().getIdAsString().equals(""))) {
                        transferItem.setContractKey(itemSalesDoc.getContractKey().getIdAsString());
                        transferItem.setContractItemKey(itemSalesDoc.getContractItemKey().getIdAsString());
                        transferItem.setContractId(itemSalesDoc.getContractId());
                        transferItem.setContractItemId(itemSalesDoc.getContractItemId());
                        transferItem.setContractConfigFilter(itemSalesDoc.getContractConfigFilter());
                    }

                    transferItem.setSource(orderStatus.getOrderHeader().getDocumentType());
                    transferItem.setProductKey(itemSalesDoc.getProductId().toString());
                    transferItem.setQuantity(itemSalesDoc.getQuantity());
                    transferItem.setUnit(itemSalesDoc.getUnit());
                    transferItem.setText((Text) itemSalesDoc.getText());
                    transferItem.setDataSetExternally(itemSalesDoc.isDataSetExternally());
                    transferItem.setDescription(itemSalesDoc.getDescription());

                    if (itemSalesDoc.isDataSetExternally()) {
                        transferItem.setProductId(itemSalesDoc.getProduct());
                        transferItem.setNetPrice(itemSalesDoc.getNetPrice());
                        transferItem.setCurrency(itemSalesDoc.getCurrency());
                    }

                    // batch
                    transferItem.setBatchCharListJSP(itemSalesDoc.getBatchCharListJSP());

                    // If no ShipTo party is set on item level, item's shipto is same as header's one

                    /* To activate ShipTo transfer remove this comment
                                        if (itemSalesDoc.getShipTo() == null) {
                                            transferItem.setShipTo(orderStatus.getOrderHeader().getShipTo());
                                        } else {
                                            transferItem.setShipTo(itemSalesDoc.getShipTo());
                                        }
                    */
                    if (itemSalesDoc.isConfigurable()) {
                        /*                        Order order = (Order)orderStatus.getOrder();
                                                order.getItemConfig(itemSalesDoc.getTechKey());
                                                IPCItemReference ipcItemReference = new IPCItemReference();
                                                ipcItemReference.setItemId(itemSalesDoc.getTechKey().getIdAsString());
                                                ipcItemReference.setDocumentId(order.getHeader().getIpcDocumentId().toString());
                                                ipcItemReference.setHost(order.getHeader().getIpcHost());
                                                ipcItemReference.setPort(order.getHeader().getIpcPort().trim());
                                                ipcItemReference.setSessionId(order.getHeader().getIpcSession());
                        */
                        IPCItem ipcItem = null;
						ext_configuration extConfig = null;
						ext_config_writable extConfigWritable = null;

                        if (itemSalesDoc.getExternalItem() != null) {
                            ipcItem = (IPCItem) itemSalesDoc.getExternalItem();
                        }
                        else {
						//	Order order = (Order)orderStatus.getOrder();     
							orderStatus.getItemConfig(DocumentListFilter.SALESDOCUMENT_TYPE_ORDER, itemSalesDoc.getTechKey());                   	
							RFCIPCItemReference ipcItemReference = (RFCIPCItemReference)IPCClientObjectFactory.getInstance().newIPCItemReference();
                            ipcItemReference.setItemId(itemSalesDoc.getTechKey().getIdAsString());
                            ipcItemReference.setDocumentId(orderStatus.getOrderHeader().getIpcDocumentId().toString());
                            ipcItemReference.setConnectionKey(orderStatus.getOrderHeader().getIpcConnectionKey());

                            IPCBOManager ibom = this.getIPCBusinessObjectManager(userSessionData);

                            try {
                                IPCClient ipcClient = ibom.createIPCClientUsingConnection(orderStatus.getOrderHeader().getIpcConnectionKey());
                                ipcItem = ipcClient.getIPCItem(ipcItemReference);
                            }
                            catch (IPCException ex) {
                                log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "Create IPCClient failed! - No IPC client available!");
                                log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, ex.getMessage());
                            }
                        }

                        if (ipcItem != null) {
							extConfigWritable = ( ext_config_writable )ipcItem.getConfig();
							extConfigWritable.set_kb_name("");
							extConfigWritable.set_kb_profile_name("");
							extConfigWritable.set_kb_version("");
							ipcItem.setConfig((ext_configuration) extConfigWritable); 
                            ipcItem.setInitialConfiguration((ext_configuration) extConfigWritable);
                            transferItem.setConfigurationItem(ipcItem);
                        }
                    }
                    
                    transferItem.setExtensionMap((HashMap) ((HashMap)itemSalesDoc.getExtensionMap()).clone());
					//Note:1059235 - Set the extensiondata with configType
					if ( (shop != null &&
						 (shop.getBackend() != null && shop.getBackend().equals("CRM")) )&&
						 (itemSalesDoc.getConfigType() != null &&
						  itemSalesDoc.getConfigType().equals(ItemSalesDoc.ITEM_CONFIGTYPE_GRID)) ){
						transferItem.addExtensionData("griditemflag", itemSalesDoc.getConfigType());
					}                    
                    transferItem.setAssignedCampaigns((CampaignList) itemSalesDoc.getAssignedCampaigns().clone());
					transferItem.setAssignedExtRefObjects((ExtRefObjectList) itemSalesDoc.getAssignedExtRefObjects().clone());
					transferItem.setAssignedExternalReferences((ExternalReferenceList)itemSalesDoc.getAssignedExternalReferences().clone());
					transferItem.setDeliveryPriority(itemSalesDoc.getDeliveryPriority());
                    
                    transferItems.add(transferItem);
                    
                    
					//NOTE 1081359 - if the flag is set on false - it should not be possible to order a product which is
					//not in the associated catalog.
	
                    //We have always one ProductCatalog in the WebCatalog
                    //this means we will use this ProductCatalog for the check!
					boolean isOrderingOfNonCatalogProductsAllowed = true;
                    
					if(userSessionData.getAttribute(NON_CATALOG_PRODUCTS_ALLOWED) != null) {
					   isOrderingOfNonCatalogProductsAllowed = ((Boolean) userSessionData.getAttribute(NON_CATALOG_PRODUCTS_ALLOWED)).booleanValue();
					}
					if( log.isDebugEnabled() ) {
					   log.debug("isOrderingOfNonCatalogProductsAllowed :"+isOrderingOfNonCatalogProductsAllowed);
					}
                    if( isOrderingOfNonCatalogProductsAllowed == false ) {
					   transferItem.setCatalog(shop.getCatalog());
					   if( log.isDebugEnabled() ) {
				  	      log.debug("BasketTransferItem.getCatalog() :"+shop.getCatalog());
					   }
                    }
					//END NOTE								  
                }
            }
             // ENDFOR
        }

        // prepare call of AddToBasketAction
        if (!transferItems.isEmpty()) {
            request.setAttribute(AddToBasketAction.RC_TRANSFER_ITEM_LIST, transferItems);
            request.setAttribute(AddToBasketAction.FORWARD_NAME, "orderstatusdetail");

            //  ^- Return Action after adding
            // Forward to addtodocument which will return to the given Action
			log.exiting();
            return mapping.findForward("addtodocument");
        }
		log.exiting();
        // Forward to documentdetails while nothing has been selected
        return mapping.findForward("orderstatusdetail");
    }
}
