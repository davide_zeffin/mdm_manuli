/*****************************************************************************
	Class         MaintainPaymentAction
	Copyright (c) 2004, SAP AG, All rights reserved.
	Description:  Action for the Payment Maintenance
	Author:       SAP AG
	Created:      17.12.2004
	Version:      1.0

*****************************************************************************/

package com.sap.isa.isacore.action.order;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.action.IsaCoreInitAction;
import com.sap.isa.isacore.action.PaymentActions;



public class MaintainPaymentAction extends IsaCoreBaseAction {



	/**
	 * Overriden <em>isaPerform</em> method of <em>IsaCoreBaseAction</em>.
	 */
	public ActionForward isaPerform(ActionMapping mapping,
					ActionForm form,
					HttpServletRequest request,
					HttpServletResponse response,
					UserSessionData userSessionData,
					RequestParser parser,
					BusinessObjectManager bom,
					IsaLocation log,
					IsaCoreInitAction.StartupParameter startupParameter,
					BusinessEventHandler eventHandler,
					boolean multipleInvocation,
					boolean browserBack)
							throws CommunicationException {

		return PaymentActions.maintainPayment2 (mapping,
				request,
				response,
				userSessionData,
				parser,
				bom,
				log,
				startupParameter,
				eventHandler,
				multipleInvocation,
				browserBack);
	 }

}