/*****************************************************************************
    Class         ShowOrderAction
    Copyright (c) 2000, SAP AG, All rights reserved.
    Description:  Action for the Profile Maintenance
    Author:
    Created:      10. April 2001
    Version:      0.2
*****************************************************************************/

package com.sap.isa.isacore.action.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.item.ItemList;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.businessobject.order.CollectiveOrder;
import com.sap.isa.businessobject.order.CustomerOrder;
import com.sap.isa.businessobject.order.Order;
import com.sap.isa.businessobject.ordertemplate.OrderTemplate;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.SoldFrom;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.eai.BackendRuntimeException;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.MessageDisplayer;
import com.sap.isa.isacore.ShipToSelection;
import com.sap.isa.isacore.action.ActionConstants;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.action.PaymentActions;

/**
 * Read and display the order for change purposes.
 * The action takes care of the following order types:
 * <ul>
 *  <li>{@link com.sap.isa.businessobject.order.Order}</li>
 *  <li>{@link com.sap.isa.businessobject.ordertemplate.OrderTemplate}</li>
 *  <li>{@link com.sap.isa.businessobject.order.CollectiveOrder}</li>
 *  <li>{@link com.sap.isa.businessobject.order.CustomerOrder}</li>
 * </ul>
 *
 * @author SAP AG
 * @version 0.1
 */

public class ShowOrderAction extends IsaCoreBaseAction {

    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser parser,
            BusinessObjectManager bom,
            IsaLocation log) throws CommunicationException {

		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        // Page that should be displayed next.
        String forwardTo = "showorder";

        Shop shp = bom.getShop();
        if (shp == null) {
			log.exiting();
          	throw new PanicException("No shop returned from bom");
        }
        DocumentHandler documentHandler = (DocumentHandler)
                userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
        DocumentState docstate = documentHandler.getManagedDocumentOnTop();
         // haben wir eins?
        if (docstate == null) {
           // use global forward to show valid frame
           // this if statement is also reached, if the readForUpdate below failed
           // and the message.jsp was displayed and the user clicks on the back icon
           // of the browser, because in this case the last executed action, which is this action,
           // is recalled. But in that case, the managed document was already deleted
		   log.exiting();
            return mapping.findForward("updatedocumentview");
        }
        ManagedDocument mandoc = (ManagedDocument) docstate;
        docstate = mandoc.getDocument();
        SalesDocument salesDoc = (SalesDocument) docstate;
        
      //sd: clearMessages sets the dirty flag always to true, due to performance reasons 
      //    the call has been encommented  
      //  salesDoc.clearMessages();

        // in case of customer order get forward from managed document
		if (salesDoc instanceof CustomerOrder) {
			 forwardTo = mandoc.getForwardMapping();
		 }
 
        // read document from backend
        try {
            if (salesDoc instanceof Order) {
               ((Order) salesDoc).readForUpdate(shp);
            }
            else if (salesDoc instanceof OrderTemplate) {
               ((OrderTemplate) salesDoc).readForUpdate(shp);
            }
            else {
				log.exiting();
               	throw new PanicException("No valid document returned from document handler");
            }
        }
        catch (BackendRuntimeException ex) {

          log.debug("BackendRuntimeException", ex);
            
          documentHandler.release(salesDoc);

          // create a Message Displayer for the error page!
          MessageDisplayer messageDisplayer = new MessageDisplayer();

          messageDisplayer.addToRequest(request);
          messageDisplayer.copyMessages(salesDoc);


          // the action which is to call after the message
          if (salesDoc instanceof CustomerOrder) {
              messageDisplayer.setAction("/b2b/customerdocumentstatusdetailprepare.do");
          }
          else if (salesDoc instanceof CollectiveOrder) {
              messageDisplayer.setAction("/b2b/collectivedocumentstatusdetailprepare.do");
          }
          else {
              messageDisplayer.setAction("/b2b/documentstatusdetailprepare.do");
          }

          messageDisplayer.setActionParameter("?techkey="+salesDoc.getTechKey().getIdAsString()
                                               +"&objecttype="+mandoc.getDocType()
                                               +"&objects_origin=&object_id=" );
			log.exiting();
          	return mapping.findForward("message");
        }
        
		if (!salesDoc.getItems().isEmpty()) {
			salesDoc.readIpcInfo();
		}
		
        if (userSessionData.getAttribute(MaintainBasketSendAction.SC_ATPSUBST) != null) {
            Message msg = new Message(Message.WARNING, "b2b.order.atp.subst");
            salesDoc.addMessage(msg);
            userSessionData.removeAttribute(MaintainBasketSendAction.SC_ATPSUBST);
            userSessionData.removeAttribute(MaintainBasketSendAction.SC_ATPSUBST);
        }

        // determine document type via document handler in order to
        // activate the possibility to delete positions in order templates (note 521056)
        String docType = salesDoc.getHeader().getDocumentType();

        if (salesDoc.isChangeHeaderOnly()) {
			log.debug("set items unchangeable!!!");
			ItemList items = salesDoc.getItems();
			ItemSalesDoc item;
			for (int i = 0; i < items.size(); i++) {
				item = (ItemSalesDoc)items.get(i);
				log.debug("set item " + item.getDescription()+ " unchangeable!!!");
				item.setChangeable(false);
				item.setCancelable(false);
				item.setDeletable(false);
                if (shp.isEAuctionUsed() && shp.getAuctionOrderType().equals(salesDoc.getHeader().getProcessType())) {
				    item.setFromAuction(true);
                    item.setAssignedCampaignsChangeable(false);
                }                
			}
		}
        
        // set the Properties "docType" and "displayable" in the ConnectedDocument. 
        salesDoc.enhanceConnectedDocument();

        // set deletable flag to true for order templates
        if (docType.equals(HeaderSalesDocument.DOCUMENT_TYPE_ORDERTEMPLATE)) {
            ItemList items = salesDoc.getItems();
            boolean deletable = true;
            ItemSalesDoc item;
            for (int i = 0; i < items.size(); i++) {
                item = (ItemSalesDoc)items.get(i);
                item.setDeletable(deletable);
            }
        }
        resolveMessageKeys(request.getSession(), salesDoc, log, log.isDebugEnabled());
        
        request.setAttribute(MaintainOrderBaseAction.RC_HEADER, salesDoc.getHeader());
        request.setAttribute(MaintainOrderBaseAction.RC_ITEMS, salesDoc.getItems());
        request.setAttribute(MaintainOrderBaseAction.RC_SHIPCOND, salesDoc.readShipCond(shp.getLanguage()));
        request.setAttribute(MaintainOrderBaseAction.RC_MESSAGES, salesDoc.getMessageList());

		//remove grid attributes from userSessionData
		if (userSessionData.getAttribute("prev") != null)
			userSessionData.removeAttribute("prev");
			
		if (userSessionData.getAttribute("next") != null)	
			userSessionData.removeAttribute("next");
			
		if (userSessionData.getAttribute("uimode") != null)	
			userSessionData.removeAttribute("uimode");
			
		if (userSessionData.getAttribute("configtype") != null)	
			userSessionData.removeAttribute("configtype");
			
        if (salesDoc instanceof CustomerOrder) {
			// determine the supported card type            
           	TechKey soldFromKey = salesDoc.getHeader().getPartnerList().getSoldFrom().getPartnerTechKey();
           	BusinessPartner soldFromPartner = bom.createBUPAManager().getBusinessPartner(soldFromKey);
           	SoldFrom soldFrom = new SoldFrom();
           	bom.createBUPAManager().addPartnerFunction(soldFromPartner, soldFrom);
           	soldFrom.readData(soldFromKey, bom.getShop().getLanguage(), null, null, null);
           	request.setAttribute(MaintainOrderBaseAction.RC_CARDTYPE,soldFrom.getCardType());

			// check if the shipTo is the soldfrom	
           	ShipToSelection shipToSelection  = new ShipToSelection(salesDoc.getShipToList(),soldFromPartner);
          	request.setAttribute(ActionConstants.RC_SHIPTOSELECTION,shipToSelection);
        }
        
        userSessionData.setAttribute(
                MaintainOrderBaseAction.SC_SHIPTOS, salesDoc.getShipTos());
		
		// put payment related values into the context
		PaymentActions.setPaymentAttributes(userSessionData, request, salesDoc); 
 
		log.exiting();
        return mapping.findForward(forwardTo);
    }

}