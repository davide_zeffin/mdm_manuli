/*****************************************************************************
    Class:        SalesDocumentRemoveAction
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author        Ralf Witt
    Created:      11 May 2001

    $Revision: #4 $
    $Date: 2001/08/01 $
*****************************************************************************/

package com.sap.isa.isacore.action.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.order.Basket;
import com.sap.isa.businessobject.order.OrderStatus;
import com.sap.isa.businessobject.ordertemplate.OrderTemplate;
import com.sap.isa.businessobject.quotation.Quotation;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.History;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 * Make a Salesdocument (ordertemplate / quotation) unavailable for further processing.
 *                          (delete)      (cancel)
 *
 * @author  Ralf Witt
 * @version 1.0
 *
 */
public class SalesDocumentRemoveAction extends IsaCoreBaseAction {

    public static final String RK_REMOVETYPE_ORDERTEMPLATE = "delete";
    public static final String RK_REMOVETYPE_QUOTATION = "cancel";
    public static final String RK_REMOVEMESSGAGEFLAG = "removemessageflag";
    public static final String RK_DOCLISTRELOADFLAG = "doclistreloadflag";
    public static final String RK_REMOVED_OBJECT = "removeobject";

    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            BusinessObjectManager bom,
            IsaLocation log)
            throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        TechKey documentKey = new TechKey(request.getParameter("techkey"));

        String objectType = request.getParameter("objecttype");
        String removeType = request.getParameter("removetype");
        String object_id = request.getParameter("object_id");

        Basket basket = bom.getBasket();
        //check if displayed basket should be deleted 
        if (basket != null && basket.getHeader().getSalesDocNumber().equals(object_id)) {
			basket.destroyContent();
        	bom.releaseBasket();
        }
        // DELETE A ORDERTEMPALTE
        if ((removeType.trim()).equals(RK_REMOVETYPE_ORDERTEMPLATE)) {
            OrderTemplate orderTemplate = bom.createOrderTemplate();
            orderTemplate.setTechKey(documentKey);
            orderTemplate.delete();
        }

        // CANCEL A QUOTATION
        if ((removeType.trim()).equals(RK_REMOVETYPE_QUOTATION)) {
            Quotation quotation = bom.createQuotation();
            quotation.cancel(documentKey);
        }

        // Make Shop available for JSP (to check for Contract and quotation allowance)
        Shop shop = bom.getShop();

        if (shop == null) {
			log.exiting();
             throw new PanicException("shop.notFound");
        }

        request.setAttribute(DocumentListAction.RK_DLA_SHOP, shop);

        DocumentHandler documentHandler = (DocumentHandler)
                userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);

        OrderStatus os = bom.getOrderStatus();

        // Update only History (remove document from History)
        userSessionData.setAttribute(RK_REMOVEMESSGAGEFLAG,"H");

        if (os != null   &&   os.getTechKey() != null  &&
            os.getTechKey().getIdAsString().trim().equals(documentKey.getIdAsString().trim())) {
            // Is the deleted Document the same in the details screen, than
            // release and with that refresh it
            if (documentHandler != null) {
                // b2b or b2c ?
                documentHandler.release(os);
            }
            userSessionData.setAttribute(RK_REMOVEMESSGAGEFLAG,"X");
            //?? bom.releaseOrderStatus();
        }

        OrderTemplate ot = bom.getOrderTemplate();

        if (ot != null   &&   ot.getTechKey() != null  &&
            ot.getTechKey().getIdAsString().equals(documentKey.getIdAsString())) {
            // Is the deleted Document the same in the details screen, than
            // release and with that refresh it
            if (documentHandler != null) {
                // b2b or b2c ?
                documentHandler.release(ot);
            }
            userSessionData.setAttribute(RK_REMOVEMESSGAGEFLAG,"X");
            bom.releaseOrderTemplate();
        }

        // remove document from history
        if (documentHandler != null) {
            // b2b or b2c ?
            History history = documentHandler.getHistory();
            history.remove(objectType.trim(), object_id);
        }
		log.exiting();
//        request.setAttribute(RK_DOCLISTRELOADFLAG, "X");
        return mapping.findForward("success");

    }
}