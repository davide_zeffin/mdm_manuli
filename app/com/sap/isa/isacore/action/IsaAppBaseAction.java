/*****************************************************************************
    Class         IsaAppBaseAction
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Thomas Smits
    Created:      05.04.2001
    Version:      1.0

    $Revision: #11 $
    $Date: 2001/11/29 $
*****************************************************************************/

package com.sap.isa.isacore.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.GenericBusinessObjectManager;
import com.sap.isa.core.BaseAction;
import com.sap.isa.core.Constants;
import com.sap.isa.core.ContextConst;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.WebUtil;

/**
 * <p>
 * This is an extended core base class with some convenience function
 * like error handling, determing mbom etc.
 * One feature is that default catch statements for
 * all common exceptions are put into this class. The developer of a
 * an action can rely on this class to catch all Exceptions and display
 * corresponding pages.
 * </p>
 * <p>
 * <b>Note - In this version of the class every request coming from
 * one client is handled synchronized. This is done to prevent unpredictable
 * exceptions coming from the underlying middleware (JCo). This approach
 * may not be sufficient for high performance installations and it may
 * be necessary to switch to another model of synchronization later.
 * </p>
 *
 * @author Thomas Smits
 * @version 1.0
 */
public abstract class IsaAppBaseAction extends BaseAction {

    public static final String FORWARD_RUNTIME_EXCEPTION = "runtimeexception";
    public static final String FORWARD_PANIC_EXCEPTION   = "panicexception";
    public static final String FORWARD_PRECONDITION_ERROR = "precondition";

    /**
     * Call this method, if your preconditions are to complex, to check
     * them
     */
    protected final void preconditionErrorDetected(String messageKey) {
    }


    /**
     * Generic user exit do Parse the request for the  business object.
     * overwrite this method to allow generic user exit.
     *
     * @param request           The request object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bob               the business object
     */
    public void parseExit( HttpServletRequest request,
                           UserSessionData userSessionData,
                           RequestParser requestParser,
       					   GenericBusinessObjectManager bom,	                       
                           BusinessObjectBase bob) {

    }


    /**
     * Returns an identifier for the session that can be used in all
     * calls to the logging API to provide session information for
     * the trace-file.
     *
     * @param request The current request (used to get the session)
     * @return an identifier for the session
     */
    protected String getLoggingPrefix(HttpServletRequest request) {
        return "";
    }

    /**
     * Implemented doPerform method used by ActionBase.
     * This method does some useful stuff and then calls isaPerform
     */
    public final ActionForward doPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws IOException, ServletException {
		final String METHOD_NAME = "doPerform()";
		log.entering(METHOD_NAME);
        // Flag for multiple invocations
        boolean multipleInvocation = false;

        // Flag for browser back detection
        boolean browserBack = false;

        // Get the session for the tracing information
        HttpSession session = request.getSession();
        String logId = getLoggingPrefix(request);
        boolean isDebugEnabled = log.isDebugEnabled();

        // get user session data object
        UserSessionData userSessionData =
                UserSessionData.getUserSessionData(request.getSession());

        // check for missing context
        if (userSessionData == null) {
            if (isDebugEnabled) {
				log.debug("No userSessionData found! forward to SESSION_NOT_VALID");
            }
            log.exiting();
            return mapping.findForward(Constants.SESSION_NOT_VALID);
        }

       // get the MBOM
        MetaBusinessObjectManager mbom = userSessionData.getMBOM();

        // wrap an reqeust parser around the request
        RequestParser requestParser = new RequestParser(request);

        ActionForward forward = null;


        // Inner class to catch errors during the lookup of forwards.
        // To provide this extended error reporting functionality, the
        // ActionForward class is subclassed and an overwritten findForward
        // method is provided
        class MyActionMapping extends ActionMapping {

            ActionMapping myMapping;    // renamed to be jikes safe
            HttpSession mySession;

            public MyActionMapping(ActionMapping aMapping, HttpSession aSession) {
                mySession = aSession;
                myMapping = aMapping;
            }

            public ActionForward findForward(String aForward) {
                ActionForward result = myMapping.findForward(aForward);

                // Forward was not found
                if (result == null) {
                    String message = WebUtil.translate(
                            getServlet().getServletContext(),
                            mySession,
                            "system.forward.missing.extended",
                            new String[] { IsaAppBaseAction.this.getClass().getName(), aForward } );
					log.fatal(message);
                    throw new RuntimeException(message);
                }

                return result;
            }
        }

        try {
            // Synchronize the call to the isaPerform method on the
            // BusinessObjectManager object. This should prevent
            // multiple request coming from the same client to
            // collide. The MBOM is a good choice because the
            // object is exclusively created for every session.
            synchronized(mbom) {
                forward = performAction(new MyActionMapping(mapping, session), form, request, response, userSessionData,
                                     requestParser, mbom);
            }
        }
        catch (PanicException e) {
            // Catch the panic exceptions

            // log error
			log.error(LogUtil.APPS_USER_INTERFACE, "exception.panic", e);

            // throw away session
            // request.getSession().invalidate();

            // store exception in context for the error pages
            request.setAttribute(ContextConst.EXCEPTION, e);

            forward = mapping.findForward(FORWARD_PANIC_EXCEPTION);
        }
        catch (RuntimeException e) {
            // Catch all possible Runtime exceptions and log them

            // log error
			log.error(LogUtil.APPS_USER_INTERFACE, "exception.runtime", e);

            // throw away session
            // request.getSession().invalidate();

            // store exception in context for the error pages
            request.setAttribute(ContextConst.EXCEPTION, e);

            forward = mapping.findForward(FORWARD_RUNTIME_EXCEPTION);
        }
        catch (CommunicationException e) {
            // Catch communication exceptions and log them

            // log error
			log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "exception.communication", e);

            // throw away session
            // request.getSession().invalidate();

            // store exception in context for the error pages
            request.setAttribute(ContextConst.EXCEPTION, e);

            forward = mapping.findForward(Constants.FORWARD_BACKEND_ERROR);
        }
        finally {
			log.exiting();
        }
        return forward;
    }



    /**
     * Overwrite this method
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param mbom              Meta business object manager to access your business
     *                          object manager
     * @return Forward to another action or page
     */
    public ActionForward performAction(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            MetaBusinessObjectManager mbom)
            throws IOException, ServletException,
                   CommunicationException {


       return mapping.findForward("error");

    }



    /**
     * Create a new instance of this class.
     */
    public IsaAppBaseAction() {
        if (log.isDebugEnabled()) {
			log.debug("New instance created.");
        }
    }

    public void finalize() throws Throwable{
        if (log.isDebugEnabled()) {
			log.debug("Instance removed by the garbage collector.");
        }
        super.finalize();
    }

}