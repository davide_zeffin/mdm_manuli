/*****************************************************************************
    Class         EComBaseAction
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP
    Created:      05.04.2001
    Version:      1.0

    $Revision: #12 $
    $Date: 2003/09/03 $
*****************************************************************************/

package com.sap.isa.isacore.action;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.BaseAction;
import com.sap.isa.core.Constants;
import com.sap.isa.core.ContextConst;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.MessageListDisplayer;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocumentLargeDoc;
import com.sap.isa.isacore.taglib.RequestSerialTag;


/**
 * <p>
 * Base class that takes care of the save retrievement of the MetaBOM and
 * session context. Another feature is that default catch statements for
 * all common exceptions are put into this class. The developer of a
 * an action can rely on this class to catch all Exceptions and display
 * corresponding pages.<br>
 * If the correct log/tracing level (DEBUG) is set, all contextual data is
 * written do the log file. To provide a security feature, the value of
 * request parameters starting with <code>nolog_</code> are not written to
 * the logfile.<br>
 * You should for example protect credit card information with this
 * method.
 * </p>
 * <p>
 * The action allows also to recognise multiple invocation (the user submits 
 * one form more than once) or browsers back. For this functionality the request
 * parameter {@link com.sap.isa.isacore.taglib.RequestSerialTag#RC_REQUESTSERIAL}
 * must be set. You can use the {@link com.sap.isa.isacore.taglib.RequestSerialTag
 *  to set the parameter in the JSP.
 * </p>
 * <p>
 * <b>Note - In this version of the class every request coming from
 * one client is handled synchronized. This is done to prevent unpredictable
 * exceptions coming from the underlying middleware (JCo). This approach
 * may not be sufficient for high performance installations and it may
 * be necessary to switch to another model of synchronization later.</b>
 * </p>
 *
 * @author SAP
 * @version 1.0
 * 
 * @see com.sap.isa.isacore.taglib.RequestSerialTag
 */
public abstract class EComBaseAction extends BaseAction {

    public static final String FORWARD_RUNTIME_EXCEPTION  = "runtimeexception";
    public static final String FORWARD_PANIC_EXCEPTION    = "panicexception";
    public static final String FORWARD_PRECONDITION_ERROR = "precondition";

    private final String RC_LASTSERIAL_LOCAL = EComBaseAction.class.getName() + ".lastserial";
    private final String SC_LASTSERIAL_GLOBAL = EComBaseAction.class.getName() + "serial";

	private boolean isInitialized = false;
	

	/**
	 * Use this method to configure your action. <br> 
	 * This used e.g. in the IsaCoreBaseAction to set the <code>checkUserIsLoggedIn</code> 
	 * switch in dependence of the application. <br>
	 */
	protected void initialize() {
    	
	}


	/**
	 * Create a new instance of this class.
	 */
	public EComBaseAction() {
		if (log.isDebugEnabled()) {
			log.debug("New instance for the action: " + this.getClass().getName() + " created.");
		}
	}
		

	/**
	 * The object will be destroyed.
	 */
	public void finalize() throws Throwable {
		if (log.isDebugEnabled()) {
			log.debug("Instance removed by the garbage collector.");
		}
		super.finalize();
	}


    /**
     * Retrieves for a given key configuration parameters stored in the
     * web.xml file.
     * <b>Note -</b> Because of the Struts framework, the parameter you
     * want to retrieve here has to be stored as a parameter of Strut's
     * action servlet.
     *
     * @param parameter The name of the parameter to retrieve
     * @return the value of the attribute or <code>null</code> if not
     *         parameter was found for the given name
     */
    public String getInitParameter(String parameter) {
        ServletContext ctx = getServlet().getServletConfig().getServletContext();
        return ctx.getInitParameter(parameter);
    }


	/**
	 * Returns the name of the application (taken from Scenario
	 * parameter in web.xml (see {@link com.sap.isa.core.ContextConst#IC_SCENARIO}). <br>
	 * 
	 * If the parameter doesn't exit in the web.xml and empty string will be 
	 * returned.
	 *
	 * @return name of the application
	 * @deprecated use either {@link #getApplicationName()} to get the name of the application
	 *  or {@link #getScenarioName()} to get the name of the scenario.
	 */
	protected String getApplication() {
    	
		String application = getInitParameter(ContextConst.IC_SCENARIO);
        
		if (application == null) {
			application = "";
		}
		return application;
	}

	/**
	 * Returns the name of the application (taken from Scenario
	 * parameter in web.xml (see {@link com.sap.isa.core.ContextConst#IC_SCENARIO}). <br>
	 * 
	 * If the parameter doesn't exit in the web.xml and empty string will be 
	 * returned.
	 *
	 * @return name of the application
	 */
	protected String getScenarioName() {
    	
		String scenario = getInitParameter(ContextConst.IC_SCENARIO);
        
		if (scenario == null) {
			scenario = "";
		}
		return scenario;
	}


	/**
	 * Returns the name of the application (taken from application name
	 * parameter in web.xml (see {@link com.sap.isa.core.ContextConst#APP_NAME}). <br>
	 * 
	 * If the parameter doesn't exit in the web.xml and empty string will be 
	 * returned.
	 *
	 * @return name of the application
	 */
	protected String getApplicationName() {
    	
		String application = getInitParameter(ContextConst.APP_NAME);
        
		if (application == null) {
			application = "";
		}
		return application;
	}



	/**
	 * Checks, if the given business object is initialized. <br>
	 * If not a <code>PanicException</code> will thrown.
	 * 
	 * @param bob the business object which should be checked
	 * @throws PanicException
	 * 
	 * @see com.sap.isa.core.PanicException
	 */
	protected void checkObjectExist(BusinessObjectBase bob) throws PanicException {

		if (bob == null) {		
			throw new PanicException(bob.getClass().toString() + " not available in context!" );
		}
		
	}
    
    
    /**
     * Checks, if the given business object is initialized. <br>
     * If not a <code>PanicException</code> will thrown.
     * 
     * @param bob the business object which should be checked
     * @throws PanicException
     * 
     * @see com.sap.isa.core.PanicException
     */
    protected void checkObjectExist(BusinessObjectBaseData bob) throws PanicException {

        if (bob == null) {      
            throw new PanicException(bob.getClass().toString() + " not available in context!" );
        }
        
    }


	/**
	 * Generic method to check pre conditions. <br>
	 * Use the method to overwrite. 
	 * By default there is no precheck.
	 * 
     * @param userSessionData user session data
     * @param mbom meta business object manager
     * @throws PanicException
     * 
     * @see com.sap.isa.core.PanicException
     */
    protected String checkPreConditions(UserSessionData userSessionData,
							        MetaBusinessObjectManager mbom) 
			throws PanicException {
				
		return null;				
	}


    /**
     * Implemented doPerform method used by ActionBase.
     * This method does some useful stuff and then calls isaPerform
     */
    public final ActionForward doPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws IOException, ServletException {
		final String METHOD_NAME = "doPerform()";
		log.entering(METHOD_NAME);
        // Flag for multiple invocations
        boolean multipleInvocation = false;

        // Flag for browser back detection
        boolean browserBack = false;

        // Get the session for the tracing information
        HttpSession session = request.getSession();
        //String logId = getLoggingPrefix(request);
        boolean isDebugEnabled = log.isDebugEnabled();

        // get user session data object
        UserSessionData userSessionData =
                UserSessionData.getUserSessionData(request.getSession());

        // check for missing context
        if (userSessionData == null) {
            if (isDebugEnabled) {
                log.debug("No userSessionData found! forward to SESSION_NOT_VALID");
            }
            return mapping.findForward(Constants.SESSION_NOT_VALID);
        }

		if (!isInitialized) {
			isInitialized = true;
			initialize();
		}

        // get the MBOM
        MetaBusinessObjectManager mbom = userSessionData.getMBOM();

        // wrap an reqeust parser around the request
        RequestParser requestParser = new RequestParser(request);

        RequestParser.Parameter serialParam =
                requestParser.getParameter(RequestSerialTag.RC_REQUESTSERIAL);

        // check for information about multiple invocations
        if (serialParam.isSet()) {

            // get the actual serial
            int requestSerial = serialParam.getValue().getInt();

            // get the last serial
            
			int lastSerial = -1;

			// take the request local serial number from the request 	
			Object lastSerialLocalObj = request.getAttribute(RC_LASTSERIAL_LOCAL);

			if ((lastSerialLocalObj != null) && (lastSerialLocalObj instanceof Integer)) {
				lastSerial = ((Integer) lastSerialLocalObj).intValue();
			}

			// if there is still no information in the request,
			// the last global serial number is used. 
			if (lastSerialLocalObj == null) {
            	Object lastSerialGlobalObj = userSessionData.getAttribute(SC_LASTSERIAL_GLOBAL);
				if ((lastSerialGlobalObj != null) && (lastSerialGlobalObj instanceof Integer)) {
					lastSerial = ((Integer) lastSerialGlobalObj).intValue();
				}	
				request.setAttribute(RC_LASTSERIAL_LOCAL,new Integer(lastSerial));
			}	

            // look for multiple invocation
            if (lastSerial == requestSerial) {
                multipleInvocation = true;
                if (isDebugEnabled) {
                    log.debug("Detected a multiple invocation with serial number " + lastSerial + ".");
                }
            }

            if (lastSerial > requestSerial) {
                browserBack = true;
                if (isDebugEnabled) {
                    log.debug("Detected a browser back. Last serial was " + lastSerial + ", browser send " + requestSerial + ".");
                }
            }

            // Store the new serial
            userSessionData.setAttribute(SC_LASTSERIAL_GLOBAL, new Integer(requestSerial));

            if (isDebugEnabled) {
                log.debug("Updated serial. Was " + lastSerial + ", set " + requestSerial + ".");
            }

        }

        ActionForward forward = null;


        try {
            // Synchronize the call to the isaPerform method on the
            // BusinessObjectManager object. This should prevent
            // multiple request coming from the same client to
            // collide. The BOM is a good choice because the
            // object is exclusively created for every session.
            synchronized(mbom) {
				String logForward= checkPreConditions(userSessionData, mbom);
				if (logForward != null) {
					if (logForward.equals(Constants.USER_NO_LOGIN)){
						MessageListDisplayer messageDisplayer = new MessageListDisplayer();
						messageDisplayer.setOnlyLogin();
						messageDisplayer.addMessage(new Message(Message.ERROR,"error.needLogin",null,null));
						messageDisplayer.addToRequest(request);
					}
					return mapping.findForward(logForward);
				}
                forward = ecomPerform(new IsaActionMapping(mapping, session), form, request, response, userSessionData,
                                     requestParser, mbom, multipleInvocation, browserBack);
            }
        }
        catch (PanicException e) {
            // Catch the panic exceptions

            // log error
            log.error(LogUtil.APPS_USER_INTERFACE, "exception.panic", e);

            // throw away session
            // request.getSession().invalidate();

            // store exception in context for the error pages
            request.setAttribute(ContextConst.EXCEPTION, e);

            forward = mapping.findForward(FORWARD_PANIC_EXCEPTION);
        }
        catch (RuntimeException e) {
            // Catch all possible Runtime exceptions and log them

            // log error
            log.error(LogUtil.APPS_USER_INTERFACE, "exception.runtime", e);

            // throw away session
            // request.getSession().invalidate();

            // store exception in context for the error pages
            request.setAttribute(ContextConst.EXCEPTION, e);

            forward = mapping.findForward(FORWARD_RUNTIME_EXCEPTION);
        }
        catch (CommunicationException e) {
            // Catch communication exceptions and log them

            // log error
            log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "exception.communication", e);

            // throw away session
            // request.getSession().invalidate();

            // store exception in context for the error pages
            request.setAttribute(ContextConst.EXCEPTION, e);

            forward = mapping.findForward(Constants.FORWARD_BACKEND_ERROR);
        } finally {
        	log.exiting();
        }
        return forward;
    }

	// Inner class to catch errors during the lookup of forwards.
	// To provide this extended error reporting functionality, the
	// ActionForward class is subclassed and an overwritten findForward
	// method is provided
	protected class IsaActionMapping extends ActionMapping {

		ActionMapping myMapping;    // renamed to be jikes safe
		HttpSession mySession;

		public IsaActionMapping(ActionMapping aMapping, HttpSession aSession) {
			mySession = aSession;
			myMapping = aMapping;
		}

		public ActionForward findForward(String aForward) {
			ActionForward result = myMapping.findForward(aForward);

			// Forward was not found
			if (result == null) {
				String message = WebUtil.translate(
						getServlet().getServletContext(),
						mySession,
						"system.forward.missing.extended",
						new String[] { EComBaseAction.this.getClass().getName(), aForward } );
				log.fatal(message);
				throw new RuntimeException(message);
			}

			return result;
		}
        
        /**
         * Checks if for the current action a forward is defined.
         * @param aForward forward name
         * @return true if forward name is defined
         */
        public boolean isForwardDefined(String aForward) {
            return (myMapping.findForward(aForward) != null) ? true : false;   
        }
	}


    /**
     * You have to have implement here your action coding. <br>
     * As an additional parameter you will get a flag indicating, whether or not
     * a multiple invocation occured.
     *
     * @param mapping            The ActionMapping used to select this instance
     * @param form               The <code>FormBean</code> specified in the
     *                              config.xml file for this action
     * @param request            The request object
     * @param response           The response object
     * @param userSessionData    Object wrapping the session
     * @param requestParser      Parser to simple retrieve data from the request
     * @param mbom               Reference to the MetaBusinessObjectManager
     * @param multipleInvocation Flag indicating, that a multiple invocation occured
     * @param browserBack        Flag indicating a browser back
     * @return Forward to another action or page
     */
    public abstract ActionForward ecomPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
			MetaBusinessObjectManager mbom,
            boolean multipleInvocation,
            boolean browserBack)
            throws IOException, ServletException,
                   CommunicationException;


	/**
	 *  In case of Single Item Presentation (either in a large document or in a non-large document)
	 *  return the new forward <b>itemsearch</b>. To activate Single Item Presentation send request
	 *  parameter <code>fromDocumentSearch</code> with true.<br>
	 *  Method should be used in correspondens to method <code>getAdjustedSingleItemPresentationNewForward</code>.
	 * @param request The request object
	 * @param externalForward 
	 * @return <b>null</b> if Single item presentation is not requested or forward <b>itemsearch</b>.
	 * @see #getAdjustedSingleItemPresentationNewForward
	 */
	public static String getSingleItemPresentationNewForward(HttpServletRequest request, String externalForward) {
		String retVal = externalForward;
		// In the navigator a certain item has already been selected (e.g. search by product id).
		if ("true".equals(request.getParameter("fromDocumentSearch"))) {
			if ( ! "true".equals(request.getAttribute("fromDocumentSearch"))) {
			  // Set request parameter to avoid endless loop between DetailPrepareAction and
			  // ItemsSearchAction (which calls DetailPrepareAction).
			  request.setAttribute("fromDocumentSearch", "true");
			  retVal = "itemsearch";
			}
		}
		return retVal;
	}
    
    
	/**
	 *  In case of Single Item Presentation (either in a large document or in a non-large document)
	 *  return the new forward <b>itemsearch</b>. To activate Single Item Presentation send request
	 *  parameter <code>fromDocumentSearch</code> with true.<br>
	 *  Method should be used in correspondens to method <code>getAdjustedSingleItemPresentationNewForward</code>.
	 * @param request The request object
	 * @param externalForward 
	 * @return <b>null</b> if Single item presentation is not requested or forward <b>itemsearch</b>.
	 * @see #getAdjustedSingleItemPresentationNewForward
	 */
	public static String getSingleItemPresentationNewForward(HttpServletRequest request) {
		String retVal = null;
		// In the navigator a certain item has already been selected (e.g. search by product id).
		if ("true".equals(request.getParameter("fromDocumentSearch"))) {
			if ( ! "true".equals(request.getAttribute("fromDocumentSearch"))) {
			  // Set request parameter to avoid endless loop between DetailPrepareAction and
			  // ItemsSearchAction (which calls DetailPrepareAction).
			  request.setAttribute("fromDocumentSearch", "true");
			  retVal = "itemsearch";
			}
		}
		return retVal;
	}
	
	/**
	 * In case of Single Item Presentation (either in a large document or in a non-large document) check on
	 * the forward. For non-large documents the search for items (<code>itemsearch</code>) is not necessary.
	 * So the forward need to be adjusted.<br>
	 * Method should be used in correspondens to method <code>getSingleItemPresentationNewForward</code>. 
	 * @param request   
	 * @param isLargeDoc
	 * @param externalForward 
	 * @param documentHandler
	 * @return
	 * @see #getSingleItemPresentationNewForward
	 */
	public static String getAdjustedSingleItemPresentationNewForward(HttpServletRequest request, boolean isLargeDoc,
																	 String externalForward, DocumentHandler documentHandler) {
		String retVal = externalForward;
		// Adjust "external" forward since a "fromDocumentSearch" had been send!
		if ( ! isLargeDoc && 
			"itemsearch".equals(externalForward)) {
			// Don't show a item search for non-large document
			retVal = "success";
		}
		if (! isLargeDoc)  {
			String itemNoToShow = null; 
			if (request.getParameter(EComConstantsBase.RK_SEARCH_ITEM_PROPERTY_LOW_VALUE) != null      &&
				request.getParameter(EComConstantsBase.RK_SEARCH_ITEM_PROPERTY_LOW_VALUE).length() > 0 &&
			    ManagedDocumentLargeDoc.ITEM_PROPERTY_NUMBERINT.equals(request.getParameter(EComConstantsBase.RK_SEARCH_ITEM_PROPERTY))) {
			   itemNoToShow =  request.getParameter(EComConstantsBase.RK_SEARCH_ITEM_PROPERTY_LOW_VALUE);
			} else if (request.getAttribute(EComConstantsBase.RK_SEARCH_ITEM_PROPERTY_LOW_VALUE) != null              &&
					 ((String)request.getAttribute(EComConstantsBase.RK_SEARCH_ITEM_PROPERTY_LOW_VALUE)).length() > 0 &&
			          ManagedDocumentLargeDoc.ITEM_PROPERTY_NUMBERINT.equals(request.getAttribute(EComConstantsBase.RK_SEARCH_ITEM_PROPERTY))) {
			   itemNoToShow =  (String)request.getAttribute(EComConstantsBase.RK_SEARCH_ITEM_PROPERTY_LOW_VALUE);
			}
			if (itemNoToShow != null  &&
				documentHandler.getManagedDocumentOnTop() instanceof ManagedDocumentLargeDoc) {
                ((ManagedDocumentLargeDoc)documentHandler.getManagedDocumentOnTop()).setItemSearchProperty(ManagedDocumentLargeDoc.ITEM_PROPERTY_NUMBERINT);					
				((ManagedDocumentLargeDoc)documentHandler.getManagedDocumentOnTop()).setItemSearchPropertyLowValue(itemNoToShow); 
			}
		}
    	
		return retVal;
	}

}