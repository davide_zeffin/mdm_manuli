/*****************************************************************************
    Class         GetTransferItemsAction
    Description:  Action to add products to the basket
    Author:       SAP
    Created:      September 2007
    Version:      1.0

*****************************************************************************/
package com.sap.isa.isacore.action;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.item.BasketTransferItemImpl;
import com.sap.isa.catalog.actions.ActionConstants;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;

/**
 * Create a transfer item list for given product guids and store it in the request context. <br>
 * Example of usage: addproductstodocument.do?productguid[0]=34DC018FD3CCD411858800902761A739&
 *          quantity[0]=3&productguid[1]=1EDA018FD3CCD411858800902761A739&quantity[1]=7
 *
 * @author SAP
 * @version 1.0
 *
 */
public class GetTransferItemsAction extends IsaCoreBaseAction {

	public static final String FORWARD_SUCCESS = "success";

	/**
	 * Name of the request parameter to the product fields
	 */
	public static final String PRODUCT_GUID = "productguid[]";
	public static final String QUANTITY = "quantity[]";
	public static final String UNIT = "unit[]";

	/**
	 * Initialize the action.
	 * In B2C this action could be used by anonymous user.
	 * 
	 * @see com.sap.isa.isacore.action.IsaCoreBaseAction#initialize()
	 */
	public void initialize() {
		if (isB2C()) {
			this.checkUserIsLoggedIn = false;
		}
	}

	/**
	 * Overriden <em>isaPerform</em> method of <em>IsaCoreBaseAction</em>.
	 */
	public ActionForward isaPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		BusinessObjectManager bom,
		IsaLocation log)
		throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
		
        RequestParser.Parameter productKey = requestParser.getParameter(PRODUCT_GUID);
        RequestParser.Parameter quantity = requestParser.getParameter(QUANTITY);
        RequestParser.Parameter unit = requestParser.getParameter(UNIT);

		ArrayList itemList = new ArrayList(productKey.getNumValues());

        for (int i = 0; i < productKey.getNumValues(); i++) {
            if (productKey.getValue(i).getString().length() > 0) {
            	// create the transfer item!
            	BasketTransferItemImpl item = new BasketTransferItemImpl();

            	item.setProductKey(productKey.getValue(i).getString());
            	if (quantity.getValue(i)!= null) {
            		item.setQuantity(quantity.getValue(i).getString());
            	}
            	else {
            		item.setQuantity("1");
            	}	
            	if (unit.getValue(i)!= null) {
        			item.setUnit(unit.getValue().getString());
            	}

            	itemList.add(item);
			}
		} 
        
        request.setAttribute(ActionConstants.RA_ITEMLIST_ISA, itemList);

		log.exiting();
		return mapping.findForward("success");
	}
}