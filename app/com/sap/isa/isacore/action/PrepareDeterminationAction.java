/*****************************************************************************
    Class         PrepareDeterminationAction
    Copyright (c) 2003, SAP AG, All rights reserved.
    Description:  Action to prepare the determination process
    Author:
    Created:      15. Mai 2003
    Version:      1
*****************************************************************************/

package com.sap.isa.isacore.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.ManagedDocumentDetermination;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;

/**
 * Prepare the determination process.
 *
 * @author SAP AG
 * @version 1
 */

public class PrepareDeterminationAction extends IsaCoreBaseAction {
	
    /**
     * Requets attribute to store what action called the productdetermination
     * : actual value <i>proddetcaller</i>
     */
    public static final String RC_DET_CALLER = "detcaller";
    
    /**
     * Requets attribute to store the mode of the document: actual value
     * <i>proddetdocmode</i>
     */
    public static final String RC_DET_DOC_MODE = "detdocmode";
    
    /**
     * Requets attribute to store the document, that shoul be
     * siulated<i>proddetsimdoc</i>
     */
    public static final String RC_DET_SIMULATED_DOC = "detsimdoc";
    
    /**
     * Forward to display determination 
     */
    private static final String FORWARD_FS_DET = "fsdet";
    

    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser parser,
            BusinessObjectManager bom,
            IsaLocation log) throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        // Page that should be displayed next.
        String forwardTo = FORWARD_FS_DET;
        
        String caller = (String) request.getAttribute(RC_DET_CALLER);       
        String docMode = (String) request.getAttribute(RC_DET_DOC_MODE);
        
        String simulatedDoc = "";
        if (caller.equals(ManagedDocumentDetermination.CALLER_MAINTAIN_BASKET_SIMULATE)) {
            simulatedDoc = (String) request.getAttribute(RC_DET_SIMULATED_DOC);
        }
        
        DocumentHandler documentHandler = (DocumentHandler)
                userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
        ManagedDocument currentManagedDoc = documentHandler.getManagedDocumentOnTop();
        
         // haben wir eins?
        if (currentManagedDoc == null) {
           // use global forward to show valid frame
           // this if statement is also reached, if the readForUpdate below failed
           // and the message.jsp was displayed and the user clicks on the back icon
           // of the browser, because in this case the last executed action, which is this action,
           // is recalled. But in that case, the managed document was already deleted
            log.exiting();
            return mapping.findForward("updatedocumentview");
        }
        
        ManagedDocumentDetermination managedDocumentProdDet = new ManagedDocumentDetermination(
                            (SalesDocument) currentManagedDoc.getDocument(),
                            "determination",
                            null,  
                            null,
                            null,
                            null,
                            FORWARD_FS_DET,
                            null,
                            null);
                            
        managedDocumentProdDet.setCaller(caller);
        managedDocumentProdDet.setDocMode(docMode);
        managedDocumentProdDet.setSimulatedDoc(simulatedDoc);

        managedDocumentProdDet.setAssociatedDocument(currentManagedDoc);

        managedDocumentProdDet.setState(DocumentState.TARGET_DOCUMENT);
        documentHandler.add(managedDocumentProdDet);
        documentHandler.setOnTop(managedDocumentProdDet.getDocument());
        log.exiting();
        return mapping.findForward(forwardTo);
    }

}