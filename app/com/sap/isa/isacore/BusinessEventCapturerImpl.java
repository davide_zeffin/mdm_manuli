/*****************************************************************************
    Class         BusinessEventCapturerImpl
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Pavan Bayyapu
    Created:      May 2001
    Version:      1.0

    $Revision$
    $Date$
*****************************************************************************/

package com.sap.isa.isacore;

import java.text.NumberFormat;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.businessevent.AddToDocumentEvent;
import com.sap.isa.businessobject.businessevent.CreateDocumentEvent;
import com.sap.isa.businessobject.businessevent.DropDocumentEvent;
import com.sap.isa.businessobject.businessevent.DropFromDocumentEvent;
import com.sap.isa.businessobject.businessevent.ModifyDocumentItemEvent;
import com.sap.isa.businessobject.businessevent.PlaceOrderEvent;
import com.sap.isa.businessobject.businessevent.ViewCategoryEvent;
import com.sap.isa.businessobject.businessevent.ViewDocumentEvent;
import com.sap.isa.businessobject.businessevent.ViewProductEvent;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.businessobject.lead.Lead;
import com.sap.isa.businessobject.webcatalog.CatalogBusinessObjectManager;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.event.BusinessEvent;
import com.sap.isa.core.businessobject.event.capturer.BusinessEventCapturerFactory;
import com.sap.isa.core.businessobject.event.capturer.CapturerEvent;
import com.sap.isa.core.businessobject.event.capturer.EventCapturer;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.WebUtil;

/**
 * This class capture the business events using the business event capturer
 * framework (see package com.sap.isa.core.businessobject.event.capturer).<p>
 *
 *
 * @see com.sap.isa.core.businessobject.event.BusinessEvent
 * @see com.sap.isa.core.businessobject.event.capturer.EventCapturer
 * @see com.sap.isa.businessobject.businessevent.BusinessEventHandler
 * @see com.sap.isa.isacore.BusinessEventCapturer
 *
 */
 public class BusinessEventCapturerImpl implements BusinessEventCapturer {

    private static final IsaLocation log =
                                IsaLocation.getInstance(BusinessEventCapturerImpl.class.getName());

    private static EventCapturer eventCapturer;

    private static boolean isAvailibiltyCheckAvailable = false;


    /**
     * Set the property availibiltyCheckAvailable
     *
     * @param availibiltyCheckAvailable
     *
     */
    public static void setAvailibiltyCheckAvailable(boolean availibiltyCheckAvailable) {
        isAvailibiltyCheckAvailable = availibiltyCheckAvailable;
    }


    /**
     * Returns the property availibiltyCheckAvailable
     *
     * @return availibiltyCheckAvailable
     *
     */
    public static boolean isAvailibiltyCheckAvailable() {
       return isAvailibiltyCheckAvailable;
    }


    public BusinessEventCapturerImpl() {
       eventCapturer = BusinessEventCapturerFactory.getEventCapturer();
    }


    public static void shutDown() {
        eventCapturer.release();
    }


   /**
     * The capture type is an unique identifier for the capturer, which allows
     * to remove a Capturer from the BusinessEventHandlerBaseImpl
     *
     * @return returns the Capture Type
     *
     */
    public String getCaptureType(){
        return eventCapturer.getImplementation();
    }


    /**
     * Capture a general business event
     *
     */
    public void captureBusinessEvent(BusinessEvent event,
                                     MetaBusinessObjectManager metaBom,
                                     HttpServletRequest request){
    }


    /**
     * Fired a general business event
     *
     */
    public void captureBusinessEvent(BusinessEvent event,
                                     BusinessObjectManager bom,
                                     HttpServletRequest request) {
    }


    /**
     * Create a new Business Event
     *
     */
    protected CapturerEvent createEvent(HttpServletRequest request, String name) {
        return eventCapturer.createEvent(request, name);
    }

    /**
     * Fired a login event
     *
     */
    public void captureLoginEvent(com.sap.isa.businessobject.businessevent.LoginEvent event,
                                  BusinessObjectManager bom,
                                  HttpServletRequest request)  {
        /*System.out.println("login event ");
        try{
          Lead lead = this.getLead(bom);
          lead.setDescription("Lead generated from login event");
          String guidOfLead = getLeadIDFromSession(request);
          if( guidOfLead == null){
            String guid =lead.createLead();
            this.setLeadIDToSession(request, guid);
          }
          //else
          //  lead.modifyLead();
        }
        catch(Exception e){
          System.out.println("exception "+ e);
          e.printStackTrace();
        }*/
    }


    /**
     * Fired a register event
     *
     */
    public void captureRegisterEvent(com.sap.isa.businessobject.businessevent.RegisterEvent event,
                                     BusinessObjectManager bom,
                                     HttpServletRequest request)  {


        //captureLoginEvent(null, bom, request);

    }


    /**
     * Capture a Search event
     *
     */
    public void captureSearchEvent(com.sap.isa.businessobject.businessevent.SearchEvent event,
                                   BusinessObjectManager bom,
                                   HttpServletRequest request) {



    }


    /**
     * Capture a CreateDocument event
     *
     */
    public void captureCreateDocumentEvent(CreateDocumentEvent event,
                                           BusinessObjectManager bom,
                                           HttpServletRequest request) {


    }


    /**
     * Capture a ViewDocument event
     *
     */
    public void captureViewDocumentEvent(ViewDocumentEvent event,
                                         BusinessObjectManager bom,
                                         HttpServletRequest request) {


    }


    /**
     * Fired an AddToDocument event
     *
     */
    public void captureAddToDocumentEvent(AddToDocumentEvent event,
                                          BusinessObjectManager bom,
                                          CatalogBusinessObjectManager cbom,
                                          HttpServletRequest request)  {
        //System.out.println("add todocument event ");
        /*try {
          LeadSavingThread thread = new LeadSavingThread(request.getSession(), bom, event.getItem());
          thread.start();
        }
        catch(Exception ex) {
          ex.printStackTrace();
        }*/
    }



    /**
     * Fired an ModifyDocumentItem event
     *
     */
    public void captureModifyDocumentItemEvent(ModifyDocumentItemEvent event,
                                               BusinessObjectManager bom,
                                               CatalogBusinessObjectManager cbom,
                                               HttpServletRequest request)  {
        //System.out.println("modify document event ");
        //System.out.println("modify document event, old quantity =" + event.getItem().getOldQuantity());
        //System.out.println("modify document event, new quantity =" + event.getItem().getQuantity());
        /*try {
          LeadSavingThread thread = new LeadSavingThread(request.getSession(), bom, event.getItem());
          thread.start();
        }
        catch(Exception ex) {
          ex.printStackTrace();
        }*/
    }


    /**
     * Fire a DropFromDocument event
     *
     */
    public void captureDropFromDocumentEvent(DropFromDocumentEvent event,
                                    BusinessObjectManager bom,
                                    CatalogBusinessObjectManager cbom,
                                    HttpServletRequest request)  {

        //System.out.println("drop from document event ");
        Shop shop = bom.getShop();
        if(!shop.isLeadCreationAllowed())
          return;

        try {
          LeadSavingThread thread = new LeadSavingThread(request.getSession(), bom, event.getItem());
          thread.start();
        }
        catch(Exception ex) {
			log.debug(ex);
        }

    }


    /**
     * Fire a DropDocument event
     *
     */
    public void captureDropDocumentEvent(DropDocumentEvent event,
                                    BusinessObjectManager bom,
                                    HttpServletRequest request)  {
        
		Shop shop = bom.getShop();
		if(!shop.isLeadCreationAllowed())
		  return;
		  
        try{
          SalesDocument doc = (SalesDocument)event.getSource();
          if (doc != null){
			Iterator it = doc.iterator();
			if(it != null) {
              while(it.hasNext()){
				LeadSavingThread thread = new LeadSavingThread(request.getSession(), bom, (ItemSalesDoc)it.next());
				thread.start();
              }
            }
          }
          
          Lead lead = this.getLead(bom);
          log.debug("creating lead for BP" +lead.getBPID());


        }
        catch(Exception e){
          log.debug(e);
        }
    }

    /*public void setLeadIDToSession(HttpSession session, String leadID){
        session.setAttribute("LEAD_ID",leadID);
    }

    public String getLeadIDFromSession(HttpSession session){
        return (String) session.getAttribute("LEAD_ID");
    }*/

    /**
     * Fire a PlaceOrder event
     *
     */
    public void capturePlaceOrderEvent(PlaceOrderEvent  event,
                                       BusinessObjectManager bom,
                                       HttpServletRequest request)  {


    }


    /**
     * Fire a ViewCategory event
     *
     */
    public void captureViewCategoryEvent(ViewCategoryEvent event,
                                         BusinessObjectManager bom,
                                         HttpServletRequest request)  {

    }


    /**
     * Fire a ViewProduct event
     *
     */
    public void captureViewProductEvent(ViewProductEvent event,
                                        BusinessObjectManager bom,
                                        CatalogBusinessObjectManager cbom,
                                        HttpServletRequest request){



    }



    /*
     * private method to read the numberFormat from the shop
     *
     * @param bom
     *
     * @return numberFormat
     *
     */
    public NumberFormat getNumberFormat(BusinessObjectManager bom) {

        Shop shop = bom.getShop();
        if (shop != null) {
            return shop.getNumberFormat();
        }

        return NumberFormat.getInstance();
    }


    private Lead getLead (BusinessObjectManager bom){
        String bp = null;
        try {
            BusinessPartnerManager buPaMa = bom.createBUPAManager();
            BusinessPartner buPa = buPaMa.getDefaultBusinessPartner(PartnerFunctionData.SOLDTO);
            
            if (buPa != null) {
                bp = buPa.getId();
            }
        
        } catch(Exception ex) {
          log.error(LogUtil.APPS_BUSINESS_LOGIC, "can not get bp id in lead creation action");
        }

        if(bp == null)
          return null;

        Lead lead = bom.createLead();
        
        lead.setBPID(bp);
        return lead;
    }

  private class LeadSavingThread extends Thread
  {
    public LeadSavingThread(HttpSession session,
                            BusinessObjectManager bom,
                            ItemSalesDoc item)
    {
      super();
      this.session = session;
      this.bom = bom;
      this.item = item;
    }

    HttpSession session;
    BusinessObjectManager bom;
    ItemSalesDoc item;

    public void run()
    {
      try{
        Lead lead = getLead(bom);
        if(lead == null)
          return;

       synchronized(lead) {
        lead.setBSPAppl("LEAD GENERATION");
        lead.setBSPView("CONSUMER");

        UserSessionData userData = UserSessionData.getUserSessionData(session);
        String language = userData.getLocale().getLanguage();
        if(language == null)
          language = "EN";
        language = language.toUpperCase();

        //String guidOfLead = getLeadIDFromSession(session);
        String guidOfLead = lead.getLeadID();
        lead.addProduct(item.getProductId().toString(),
                        item.getQuantity(),
						item.getUnit(),
                        item.getProduct(),
                        WebUtil.translate(userData.getLocale(),
                              "lead.generated.addbasket", null));
        if( guidOfLead == null){
          lead.setDescription(WebUtil.translate(userData.getLocale(),
                                "lead.generated.title", null));

          lead.createLead(language);
          //setLeadIDToSession(session, guid);
        }
        else
          lead.modifyLead();
        //java.lang.Thread.sleep(2000);
       }
      }
      catch(Exception e){
        System.out.println("exception "+ e);
        e.printStackTrace();
      }
    }
  }
}
