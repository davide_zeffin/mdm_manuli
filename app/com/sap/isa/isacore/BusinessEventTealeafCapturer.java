/*****************************************************************************
    Class         BusinessEventTealeafCapturer
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      May 2001
    Version:      1.0

    $Revision$
    $Date$
*****************************************************************************/

package com.sap.isa.isacore;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;

import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.backend.boi.isacore.order.PartnerListData;
import com.sap.isa.backend.boi.isacore.order.PartnerListEntryData;
import com.sap.isa.businessobject.Address;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.EAnalB0ScenarioMapper;
import com.sap.isa.businessobject.IsaQuickSearch;
import com.sap.isa.businessobject.Product;
import com.sap.isa.businessobject.SalesDocumentBase;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.businessobject.businessevent.AddToDocumentEvent;
import com.sap.isa.businessobject.businessevent.CreateDocumentEvent;
import com.sap.isa.businessobject.businessevent.DropDocumentEvent;
import com.sap.isa.businessobject.businessevent.DropFromDocumentEvent;
import com.sap.isa.businessobject.businessevent.ModifyDocumentItemEvent;
import com.sap.isa.businessobject.businessevent.PlaceOrderEvent;
import com.sap.isa.businessobject.businessevent.ViewCategoryEvent;
import com.sap.isa.businessobject.businessevent.ViewDocumentEvent;
import com.sap.isa.businessobject.businessevent.ViewProductEvent;
import com.sap.isa.businessobject.capture.CapturerConfig;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.businessobject.order.Order;
import com.sap.isa.businessobject.webcatalog.CatalogBusinessObjectManager;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.catalog.webcatalog.WebCatArea;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.event.BusinessEvent;
import com.sap.isa.core.businessobject.event.capturer.BusinessEventCapturerFactory;
import com.sap.isa.core.businessobject.event.capturer.CapturerEvent;
import com.sap.isa.core.businessobject.event.capturer.EventCapturer;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.StartupParameter;


/**
 * This class capture the business events using the business event capturer
 * framework (see package com.sap.isa.core.businessobject.event.capturer).<p>
 *
 * <i>For historical reason the class is called "TealeafCapture", but no tealeaf api
 * is used within this class. The tealeaf calls are encapsulated within the
 * Capturer Framework.</i>
 *
 * Each capturer event calls a protected method, which can be extended to
 * customize the event attributes/properties. You can do the following 
 * <ol>
 * <li> overwriting an already added property --> <code>event.setProperty("Order", "Currency",   "EUR");</code> </li>
 * <li> removing existing property --> <code> event.removeProperty("Order", "CustomerID"); </code> </li>
 * <li> adding new property --> <code> event.setProperty("Order", "shopname", "Electronicsshop"); </code> </li> 
 * </ol>
 * To configure this class use the <code>TealeafInitHandler</code>.
 *
 * @see com.sap.isa.isacore.TealeafInitHandler
 * @see com.sap.isa.core.businessobject.event.BusinessEvent
 * @see com.sap.isa.core.businessobject.event.capturer.EventCapturer
 * @see com.sap.isa.businessobject.businessevent.BusinessEventHandler
 * @see com.sap.isa.isacore.BusinessEventCapturer
 *
 */
public class BusinessEventTealeafCapturer implements BusinessEventCapturer {

    /**
     * Used for logging issues.
     */
    protected static final IsaLocation log =
    IsaLocation.getInstance(BusinessEventTealeafCapturer.class.getName());
    

    /**
     * The used eventCapture.
     * <br>
     *
     * <b>Avoid direct creation of events.
     * Use the methode <code>createEvent</code> instead.</b>
     *
     */
    protected static EventCapturer eventCapturer;


    /**
     * The number format used to create the event output. See <code>setNumberFormat</code> method for details.
     */
    protected static NumberFormat localeNumberFormat = NumberFormat.getInstance();

    private static boolean isAvailibiltyCheckAvailable = false;
    private static boolean removeEmptyValues = false;
    private static boolean useAlwaysSalesDocItem = false;

    private Date theDate = new Date();


    /**
     * Returns if the sales item should be used for item informations. <br>
     * Normally first the product catalog is checked, before the sales doc item
     * is used.
     * The switch is used for the AddToDocumentEvent, ModifyDocumentItemEvent and
     * the DropFromDocumentEvent.
     *
     * @return boolean
     */
    public static boolean isUseAlwaysSalesDocItem() {
        return useAlwaysSalesDocItem;
    }


    /**
     * Sets the flag to use always the sales doc item to get the item
     * information. <br>
     *
     * @param useAlwaysSalesDocItem The value to set
     * @see BusinessEventTealeafCapturer#isUseAlwaysSalesDocItem
     */
    public static void setUseAlwaysSalesDocItem(boolean useAlwaysSalesDocItem) {
        BusinessEventTealeafCapturer.useAlwaysSalesDocItem =
        useAlwaysSalesDocItem;
    }



    /**
     * Set the property availibiltyCheckAvailable. This switce could force the capturer
         * event to filter empty output
     *
     * @param availibiltyCheckAvailable
     *
     */
    public static void setAvailibiltyCheckAvailable(boolean availibiltyCheckAvailable) {
        isAvailibiltyCheckAvailable = availibiltyCheckAvailable;
    }


    /**
     * Return the property removeEmptyValues.
     *
     * @return
     */
    public static boolean isRemoveEmptyValues() {
        return removeEmptyValues;
    }


    /**
     * Set the property removeEmptyValues: This switce could force the capturer
     * event to filter empty output
     *
     * @param removeEmptyValues.
     */
    public static void setRemoveEmptyValues(boolean removeEmptyValues) {
        BusinessEventTealeafCapturer.removeEmptyValues = removeEmptyValues;
    }


    /**
     * Returns the property availibiltyCheckAvailable
     *
     * @return availibiltyCheckAvailable
     *
     */
    public static boolean isAvailibiltyCheckAvailable() {
        return isAvailibiltyCheckAvailable;
    }

    /**
     * Set the number Format used to create numbers to write to the capturer API.
     *
     * @param decimalPointFormat allowed values are the constant defined in
     * the shopData interface and the value <code>#######.##</code>.
     *
     * @see com.sap.isa.backend.boi.isacore.ShopData
     *
     */
    public static void setNumberFormat (String decimalPointFormat) {

        char    decimalSeparator     = ' ';
        char    groupingSeparator    = '.';
        boolean isGroupingUsed = true;

        log.debug("decimalPointFormat: " + decimalPointFormat + " given");


        if(decimalPointFormat.equals(ShopData.DECIMAL_POINT_FORMAT1)) {
            decimalSeparator     = ',';
            groupingSeparator    = '.';
        }
        else if(decimalPointFormat.equals(ShopData.DECIMAL_POINT_FORMAT2)) {
            decimalSeparator     = ',';
            groupingSeparator    = ' ';
        }
        else if(decimalPointFormat.equals(ShopData.DECIMAL_POINT_FORMAT3)) {
            decimalSeparator     = '.';
            groupingSeparator    = ',';
        }
        else if(decimalPointFormat.equals("#######.##")) {
            decimalSeparator     = '.';
            isGroupingUsed       = false;
        }
        else {
            // use format from locale
            log.debug("Unknown decimalPointFormat given");
            return;
        }

        Locale locale = new Locale("","");
        DecimalFormat decimalFormat = (DecimalFormat) NumberFormat.getInstance(locale);

        DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols(locale);
        decimalFormatSymbols.setDecimalSeparator(decimalSeparator);
        if(isGroupingUsed) {
            decimalFormatSymbols.setGroupingSeparator(groupingSeparator);
        }

        decimalFormat.setDecimalFormatSymbols(decimalFormatSymbols);
        decimalFormat.setGroupingUsed(isGroupingUsed);

        localeNumberFormat = decimalFormat;
    }


    public BusinessEventTealeafCapturer() {
        eventCapturer = BusinessEventCapturerFactory.getEventCapturer();
    }


    public static void shutDown() {
        eventCapturer.release();
    }


    /**
      * The capture type is an unique identifier for the capturer, which allows
      * to remove a Capturer from the BusinessEventHandlerBaseImpl
      *
      * @return returns the Capture Type
      *
      */
    public String getCaptureType() {
        return eventCapturer.getImplementation();
    }


    /**
     * Capture a general business event. <br>
     *
     */
    public void captureBusinessEvent(BusinessEvent event,
                                     MetaBusinessObjectManager metaBom,
                                     HttpServletRequest request){
    }


    /**
     * Capture a general business event a general business event.<br>
     *
     */
    public void captureBusinessEvent(BusinessEvent event,
                                     BusinessObjectManager bom,
                                     HttpServletRequest request) {
    }


    /**
     * Create a new capturer event using the current event capturer.
     *
     * @param request The current http request
     * @param name of the event
     *
     * @return a new capturer event
     *
     */
    protected CapturerEvent createEvent(HttpServletRequest request, String name) {

        CapturerEvent capturerEvent = eventCapturer.createEvent(request, name);
        capturerEvent.setIgnoreEmptyValues(removeEmptyValues);

        return capturerEvent;
    }


    /**
     * Captured the login event. <br>
     * A capturer event with the name "LOGIN" will be created.
     *
     * The following groups are used within the event:
     * <ol>
     *  <li>Login</li>
     * </ol>
     * calls setLogin to set the proeprties inside LOGIN event
     * @see BusinessEventTealeafCapturer#setLogin
     *
     */
    public void captureLoginEvent(com.sap.isa.businessobject.businessevent.LoginEvent event,
                                  BusinessObjectManager bom,
                                  HttpServletRequest request)  {
		if(!isEventTobeCaptured("LOGIN", request, bom))
			return;
		
		log.debug("capturing login event");	
        User user = (User)event.getSource();

        try {
            CapturerEvent loginEvent = createEvent(request, "LOGIN");

            setLogin(loginEvent, user, bom);

            loginEvent.publish();
        }
        catch(Exception ex) {
            logException(ex);
        }
        catch(Throwable t){
			logException(t);
        }
		log.debug("captured login event");	
    }


	/**
     * Return true if the event should be captured.
     * If BW is configured to handle the upload of the event.
     * Get the event map for the scenario, which is linked to a BW system
     * and find out whether the event is in the map.
	 * @param eventname name of the event
	 * @return true- if BW is configured properly and event is in the list of events
	 * 				to be captured 
	 * 				or Communication to BW system is failed(safeside)
	 * 			false - otherwise (event is not in the list of events to be captured)
	 */
	private boolean isEventTobeCaptured(String eventname, HttpServletRequest request, BusinessObjectManager bom) {
		boolean isTobeCaptured = true;
		UserSessionData userSessionData = UserSessionData.getUserSessionData(request.getSession());
		StartupParameter startParameter = (StartupParameter) userSessionData.getAttribute(SessionConst.STARTUP_PARAMETER);
		String scenario = startParameter.getParameterValue("scenario.xcm");
			//get the scenario and corresponding event map
		CapturerConfig config = EAnalB0ScenarioMapper.getCaptureConfig(scenario,bom);
		if(config != null){
			isTobeCaptured = config.isEventTobeCaptured(eventname);
		}
		
		if (!isTobeCaptured && log.isDebugEnabled()) {
			log.debug("Event: " + eventname +" is not captured");
		}		
		
		return isTobeCaptured;
	}


	/**
     * Capture a register event. <br>
     *
     * A capturer event with the name "REGISTER" will be created.
     *
     * The following groups are used within the event:
     * <ol>
     *  <li>Register</li>
     *  <li>Customer</li>
     * </ol>
     * calls setCustomer to set the proeprties inside REGISTER event
     * @see BusinessEventTealeafCapturer#setCustomer
     *
     */
    public void captureRegisterEvent(com.sap.isa.businessobject.businessevent.RegisterEvent event,
                                     BusinessObjectManager bom,
                                     HttpServletRequest request)  {
		if(!isEventTobeCaptured("REGISTER",request, bom))
			return;
		log.debug("capturing register event");			
        User user = (User)event.getSource();
        Address address = event.getAddress();


        try {
            CapturerEvent registerEvent = createEvent(request, "REGISTER");

            registerEvent.setProperty("Register", "Register", "True");

            setCustomer(registerEvent, user, address, bom);

            registerEvent.publish();
        }
        catch(Exception ex) {
            logException(ex);
        }
		catch(Throwable t){
			logException(t);
		}
		log.debug("captured register event");

    }


    /**
     * Capture a Search event.
     * calls setSearchEvent to set the proeprties inside SEARCH event
     */
    public void captureSearchEvent(com.sap.isa.businessobject.businessevent.SearchEvent event,
                                   BusinessObjectManager bom,
                                   HttpServletRequest request) {
		if(!isEventTobeCaptured("SEARCH",request, bom))
			return;
		
		log.debug("capturing search event");			
        IsaQuickSearch search = (IsaQuickSearch)event.getSource();

        try {
            CapturerEvent searchEvent = createEvent(request, "SEARCH");


			setSearchEvent( searchEvent, search, bom, request);
            searchEvent.publish();
        }
        catch(Exception ex) {
            logException(ex);
        }
		catch(Throwable t){
			logException(t);
		}

    }

	
	/**
	 * Override this method to do the customization like deleting properties from the event/editing exsting
	 * properties / adding new properties
	 * <pre>
	 *  Properties set in this method include:
	 * searchEvent.setProperty("Search", "URLString", urlString);
	 * searchEvent.setProperty("Search", "SearchString", search.getSearchexpr());
	 * </pre>	 * @param searchEvent SearchEvent fired by the application
	 * @param search Search Object containing context information
	 * @param bom the business object manager
	 * @param request Http Servlet Request
	 */
	private void setSearchEvent(
		CapturerEvent searchEvent,
		IsaQuickSearch search,
		BusinessObjectManager bom,
		HttpServletRequest request) {
		
		String urlString = request.getHeader("Referer");
		searchEvent.setProperty("Search", "URLString", urlString);
		searchEvent.setProperty("Search", "SearchString", search.getSearchexpr());
	
	}


	/**
     * Capture a CreateDocument event. <br>
     *
     * A capturer event with the name "CREATE_BASKET" will be created.
     *
     * The following groups are used within the event:
     * <ol>
     *  <li>Basket</li>
     * </ol>
     * calls setBasket to set the proeprties inside CREATE_BASKET event
     * @see BusinessEventTealeafCapturer#setBasket
     *
     */
    public void captureCreateDocumentEvent(CreateDocumentEvent event,
                                           BusinessObjectManager bom,
                                           HttpServletRequest request) {
		if(!isEventTobeCaptured("CREATE_BASKET",request, bom))
			return;
		log.debug("capturing create document event");			
        SalesDocumentBase document = (SalesDocumentBase) event.getSource();

        try {
            CapturerEvent createBasketEvent =
            createEvent(request, "CREATE_BASKET");

            setBasket(createBasketEvent, document, bom);

            createBasketEvent.publish();
        }
        catch(Exception ex) {
            logException(ex);
        }
		catch(Throwable t){
			logException(t);
		}
		log.debug("captured create document event");	
    }


    /**
     * Capture a ViewDocument event.
     *
     * A capturer event with the name "VIEW_BASKET" will be created.
     *
     * The following groups are used within the event:
     * <ol>
     *  <li>Basket</li>
     * </ol>
     * calls setBasket to set the proeprties inside VIEW_BASKET event
     * @see BusinessEventTealeafCapturer#setBasket
     *
     */
    public void captureViewDocumentEvent(ViewDocumentEvent event,
                                         BusinessObjectManager bom,
                                         HttpServletRequest request) {
		if(!isEventTobeCaptured("VIEW_BASKET",request, bom))
			return;
		log.debug("capturing view document event");
        SalesDocumentBase document = (SalesDocumentBase) event.getSource();
        try {
            CapturerEvent viewBasketEvent =
            createEvent(request, "VIEW_BASKET");

            setBasket(viewBasketEvent, document, bom);

            viewBasketEvent.publish();
        }
        catch(Exception ex) {
            logException(ex);
        }
		catch(Throwable t){
			logException(t);
		}
		log.debug("captured view document event");
    }


    /**
     * Capture an AddToDocument event.
     *
     * A capturer event with the name "ADD_TO_BASKET" will be created.
     *
     * The item informaton will be taken from the catalog product,
     * as long the product in the catalog exists and the
     * property <code>useAlwaysSalesDocItem</code> is set to false.
     *
     *
     * The following groups are used within the event:
     *
     * <ol>
     *  <li>Item</li>
     * </ol>
     * calls setItemInfo to set the proeprties inside ADD_TO_BASKET event
     * @see BusinessEventTealeafCapturer#setSalesDocumentItem
     * @see BusinessEventTealeafCapturer#setItemInfo
     *
     */
    public void captureAddToDocumentEvent(AddToDocumentEvent event,
                                          BusinessObjectManager bom,
                                          CatalogBusinessObjectManager cbom,
                                          HttpServletRequest request)  {
		if(!isEventTobeCaptured("ADD_TO_BASKET",request, bom))
			return;
		log.debug("capturing Add to document event");
        SalesDocumentBase document = (SalesDocumentBase) event.getSource();

        try {
            CapturerEvent addToBasketEvent =
            createEvent(request, "ADD_TO_BASKET");

            if(useAlwaysSalesDocItem) {
                setItemFromSalesItem(addToBasketEvent,
                                     event.getItem(),
                                     document.getTechKey(),
                                     bom);
            }
            else {
                setSalesDocumentItem(addToBasketEvent,
                                     event.getItem(),
                                     document.getTechKey(),
                                     bom,
                                     cbom);
            }

            addToBasketEvent.publish();
        }
        catch(Exception ex) {
            logException(ex);
        }
		catch(Throwable t){
			logException(t);
		}
		log.debug("captured Add to document event");
    }


    /**
     * Captures an ModifyDocumentItem event. <br>
     *
     * A capturer event with the name "MODIFY_BASKET_ITEM" will be created.
     *
     * The item informaton will be taken from the catalog product,
     * as long the product in the catalog exists and the
     * property <code>useAlwaysSalesDocItem</code> is set to false.
     *
     *
     * The following groups are used within the event:
     *
     * <ol>
     *  <li>Item</li>
     * </ol>
     *
     * @see BusinessEventTealeafCapturer#setSalesDocumentItem
     * @see BusinessEventTealeafCapturer#setItemInfo

     * Calls setItemInfo to set the event properties inside MODIFY_BASKET_ITEM event
     */
    public void captureModifyDocumentItemEvent(ModifyDocumentItemEvent event,
                                               BusinessObjectManager bom,
                                               CatalogBusinessObjectManager cbom,
                                               HttpServletRequest request)  {
		if(!isEventTobeCaptured("MODIFY_BASKET_ITEM",request, bom))
			return;
		log.debug("capturing modify document event");
        SalesDocumentBase document = (SalesDocumentBase) event.getSource();

        try {

            CapturerEvent modifyBasketItemEvent =
            createEvent(request, "MODIFY_BASKET_ITEM");

            if(isUseAlwaysSalesDocItem()) {
                setItemFromSalesItem(modifyBasketItemEvent,
                                     event.getItem(),
                                     document.getTechKey(),
                                     bom);
            }
            else {
                setSalesDocumentItem(modifyBasketItemEvent,
                                     event.getItem(),
                                     document.getTechKey(),
                                     bom,
                                     cbom);
            }

            modifyBasketItemEvent.publish();
        }
        catch(Exception ex) {
            logException(ex);
        }
		catch(Throwable t){
			logException(t);
		}
		log.debug("captured modify document event");

    }


    /**
     * Captures a DropFromDocument event. <br>
     *
     * A capturer event with the name "DROP_FROM_BASKET" will be created.
     *
     * The item informaton will be taken from the catalog product,
     * as long the product in the catalog exists and the
     * property <code>useAlwaysSalesDocItem</code> is set to false.
     *
     *
     * The following groups are used within the event:
     *
     * <ol>
     *  <li>Item</li>
     * </ol>
     * calls setItemInfo to set the properties inside DROP_FROM_BASKET event
     * @see BusinessEventTealeafCapturer#setSalesDocumentItem
     * @see BusinessEventTealeafCapturer#setItemInfo

     */
    public void captureDropFromDocumentEvent(DropFromDocumentEvent event,
                                             BusinessObjectManager bom,
                                             CatalogBusinessObjectManager cbom,
                                             HttpServletRequest request)  {

		if(!isEventTobeCaptured("DROP_FROM_BASKET",request, bom))
			return;
		log.debug("capturing drop from document event");
        SalesDocumentBase document = (SalesDocumentBase) event.getSource();
        try {
            CapturerEvent dropFromBasketEvent =
            createEvent(request, "DROP_FROM_BASKET");
            if(isUseAlwaysSalesDocItem()) {
                setItemFromSalesItem(dropFromBasketEvent,
                                     event.getItem(),
                                     document.getTechKey(),
                                     bom);
            }
            else {
                setSalesDocumentItem(dropFromBasketEvent,
                                     event.getItem(),
                                     document.getTechKey(),
                                     bom,
                                     cbom);
            }

            dropFromBasketEvent.publish();
        }
        catch(Exception ex) {
            logException(ex);
        }
		catch(Throwable t){
			logException(t);
		}
		log.debug("captured drop from document event");
    }


    /**
     * Captures a DropDocument event. <br>
     *
     * A capturer event with the name "DROP_BASKET" will be created.
     *
     * The item informaton will be taken from the sales document.
     *
     * The following groups are used within the event:
     *
     * <ol>
     *  <li>Basket</li>
     *  <li>Item for each item</li>
     * </ol>
     * calls setBasket to set the properties inside DROP_BASKET event
     * @see BusinessEventTealeafCapturer#setBasket
     * @see BusinessEventTealeafCapturer#setItemInfo
     *
     */
    public void captureDropDocumentEvent(DropDocumentEvent event,
                                         BusinessObjectManager bom,
                                         HttpServletRequest request)  {

		if(!isEventTobeCaptured("DROP_BASKET",request, bom))
			return;
		log.debug("capturing drop document event");
        SalesDocumentBase document = (SalesDocumentBase) event.getSource();
        try {
            CapturerEvent dropBasketEvent =
            createEvent(request, "DROP_BASKET");

            setBasket(dropBasketEvent, document, bom);

            Iterator iter = document.iterator();
            while(iter.hasNext()) {
                dropBasketEvent.addGroup("Item");
                setItemFromSalesItem(dropBasketEvent,
                                     (ItemSalesDoc) iter.next(),
                                     document.getTechKey(),
                                     bom);
            }

            dropBasketEvent.publish();
        }
        catch(Exception ex) {
            logException(ex);
        }
		catch(Throwable t){
			logException(t);
		}
		log.debug("captured drop document event");
    }


    /**
     * Captures a PlaceOrder event. <br>
     *
     * A capturer event with the name "PLACE_ORDER" will be created.
     *
     * The item informaton will be taken from the sales document.
     *
     * The following groups are used within the event:
     *
     * <ol>
     *  <li>Order</li>
     *  <li>Item for each item</li>
     * </ol>
     *
     * calls setOrder to set the header properties of an order inside PLACE_ORDER event
     * calls setItemInfo to set the item level properties of an order inside PLACE_ORDER event
     * @see BusinessEventTealeafCapturer#setOrder
     * @see BusinessEventTealeafCapturer#setItemInfo
         */
    public void capturePlaceOrderEvent(PlaceOrderEvent  event,
                                       BusinessObjectManager bom,
                                       HttpServletRequest request)  {
		if(!isEventTobeCaptured("PLACE_ORDER",request, bom))
			return;
		log.debug("capturing place order event");
        User user = event.getUser();
        Order document = (Order) event.getSource();

        try {
            CapturerEvent singlePlaceOrder =
            createEvent(request, "PLACE_ORDER");

            setOrder(singlePlaceOrder, document, user, bom);

            Iterator iter = document.iterator();

            while(iter.hasNext()) {
                singlePlaceOrder.addGroup("Item");
                setItemFromSalesItem(singlePlaceOrder,
                                     (ItemSalesDoc) iter.next(),
                                     TechKey.EMPTY_KEY,
                                     bom);
            }

            singlePlaceOrder.publish();
        }
        catch(Exception ex) {
            logException(ex);
        }
		catch(Throwable t){
			logException(t);
		}
		log.debug("captured place order event");

    }


    /**
     * Captures a ViewCategory event. <br>
     *
     * A capturer event with the name "VIEW_CATEGORY" will be created.
     *
     * The following groups are used within the event:
     *
     * <ol>
     *  <li>Category</li>
     * </ol>
     * calls setViewCategory to set properties inside VIEW_CATEGORY event
     */
    public void captureViewCategoryEvent(ViewCategoryEvent event,
                                         BusinessObjectManager bom,
                                         HttpServletRequest request)  {

        try {
			if(!isEventTobeCaptured("VIEW_CATEGORY",request, bom))
				return;
			log.debug("capturing view category event");
            CapturerEvent viewCategoryEvent =
            createEvent(request, "VIEW_CATEGORY");
            setViewCategory(viewCategoryEvent, bom, event.getArea());
            viewCategoryEvent.publish();
        }
        catch(Exception ex) {
            logException(ex);
        }
		catch(Throwable t){
			logException(t);
		}
		log.debug("captured view category event");


    }


    /**
	 * <pre>
	 * Properties set in this method include:
	 * event.setProperty("Category", "CategoryName", area.getAreaName());
	 * </pre>
	 * @param event ViewCateogyEvent
	 * @param bom the Business Object Manager
	 * @param area Web catalog area
	 */
	private void setViewCategory(CapturerEvent event, BusinessObjectManager bom, WebCatArea area) {
		event.setProperty("Category", "CategoryName", area.getAreaName());
	}


	/**
     * Captures a ViewProduct event. <br>
     *
     * A capturer event with the name "VIEW_ITEM" will be created.
     * calls setItemFromProduct to set properties inside VIEW_ITEM event
     *
     * The following groups are used within the event:
     *
     * <ol>
     *  <li>Item</li>
     * </ol>
     *
     * @see BusinessEventTealeafCapturer#setItemInfo
     */
    public void captureViewProductEvent(ViewProductEvent event,
                                        BusinessObjectManager bom,
                                        CatalogBusinessObjectManager cbom,
                                        HttpServletRequest request){

        try {
			if(!isEventTobeCaptured("VIEW_ITEM",request, bom))
				return;
			log.debug("capturing view product event");
            CapturerEvent viewItemEvent = createEvent(request, "VIEW_ITEM");


            Product product = new Product(event.getItem());

            setItemFromProduct(viewItemEvent,
                               product,
                               "",
                               product.getCatalogItem().getQuantity(),
                               "0",
                               null,
                               bom,
                               cbom);

            viewItemEvent.publish();
        }
        catch(Exception ex) {
            logException(ex);
        }
		catch(Throwable t){
			logException(t);
		}
		log.debug("captured view product event");

    }


    /**
     * Set the login information within the <code>loginEvent</code>. <br>
     * <pre>
     * Properties set in this method include:
     * loginEvent.setProperty("Login", "LoginName", getUserId( user))
     * </pre>
     * The method could be overwrite to extends or modify the event output.
     *
     * @param loginEvent loginEvent
     * @param user       current user object
     * @param bom        business object manager
     */
    protected void setLogin(CapturerEvent                 loginEvent,
                            User                          user,
                            BusinessObjectManager         bom) {

        loginEvent.setProperty("Login", "LoginName", getUserId(loginEvent.getName(), user));
    }


    /**
     * Set the customer information within the <code>event</code>. <br>
     *
     * The method could be overwrite to extends or modify the event output. 
     * <pre>
     * Properties set in this method include:
     * event.setProperty("Customer", "CustomerID", getSoldToId(event.getName(), bom));
	 * 	event.setProperty("Customer", "LastName",address.getLastName());
 	 *	event.setProperty("Customer", "FirstName",address.getFirstName());
	 *	event.setProperty("Customer", "Company",companyName);
	 *	event.setProperty("Customer", "City",address.getCity());
	 *	event.setProperty("Customer", "State",address.getRegion());
	 *	event.setProperty("Customer", "ZipCode",address.getPostlCod1());
	 *	event.setProperty("Customer", "Country",address.getCountry());
	 *	event.setProperty("Customer", "Phone",address.getTel1Numbr());
	 *	event.setProperty("Customer", "eMail",address.getEMail());
	 *	event.setProperty("Customer", "Description","");
	 *	event.setProperty("Customer", "LastVisitDate",theDate);
	 *	
	 *	</pre>
     *
     * @param event event
     * @param user       current user object
     * @param address    address of the customer
     * @param bom        business object manager
     */
    protected void setCustomer(CapturerEvent                 event,
                               User                          user,
                               Address                       address,
                               BusinessObjectManager         bom) {

        event.setProperty("Customer", "CustomerID", getSoldToId(event.getName(), bom));
        event.setProperty("Customer", "LastName",address.getLastName());
        event.setProperty("Customer", "FirstName",address.getFirstName());

        String companyName = address.getName1() + " " + address.getName2()+ " " +
                             address.getName3() + " " + address.getName4();

        event.setProperty("Customer", "Company",companyName);
        event.setProperty("Customer", "City",address.getCity());
        event.setProperty("Customer", "State",address.getRegion());
        event.setProperty("Customer", "ZipCode",address.getPostlCod1());
        event.setProperty("Customer", "Country",address.getCountry());
        event.setProperty("Customer", "Phone",address.getTel1Numbr());
        event.setProperty("Customer", "eMail",address.getEMail());
        event.setProperty("Customer", "Description","");
        event.setProperty("Customer", "LastVisitDate",theDate);

    }


    /**
     * Set the basket information within the given <code>event</code>. <br>
     *
     * The method could be overwrite to extends or modify the event output.
     * <pre>
     * Properties set in this method include:
     * event.setProperty("Basket", "BasketID", documentId.getIdAsString());
     * event.setProperty("Basket", "BasketCategory", "");
     * </pre>
     * @param event         event
     * @param salesDocument current SalesDocument
     * @param bom        business object manager
     */
    protected void setBasket(CapturerEvent  event,
                             SalesDocumentBase  salesDocumet,
                             BusinessObjectManager bom) {

        event.setProperty("Basket", "BasketID", salesDocumet.getTechKey().getIdAsString());
        event.setProperty("Basket", "BasketCategory", "");
    }


    /**
     * Set the order information within the given <code>event</code>. <br>
     *<pre>
     * The method could be overwrite to extends or modify the event output.
     * Properties set in this method include:
     * event.setProperty("Order", "Currency",   header.getCurrency());
     * PartnerListData partnerList = header.getPartnerListData();
     * if (partnerList != null) {
     *     PartnerListEntryData soldTo = partnerList.getSoldToData();
     *     if (soldTo != null && soldTo.getPartnerId() != null) {
     *         event.setProperty("Order", "CustomerID", soldTo.getPartnerId());
     *     }
     * }
     * event.setProperty("Order", "TotalPrice", header.getGrossValue());	
     * </pre>
     * The method could be overwrite to extends or modify the event output.
     *
     * @param event  event
     * @param order  current order
     * @param user   involed user
     * @param bom    business object manager
     */
    protected void setOrder(CapturerEvent  event,
                            Order          document,
                            User           user,
                            BusinessObjectManager bom) {


        HeaderData header = document.getHeaderData();

        event.setProperty("Order", "Currency",   header.getCurrency());
        event.setProperty("Order", "OrderID",   header.getSalesDocNumber());
        PartnerListData partnerList = header.getPartnerListData();
        if (partnerList != null) {
            PartnerListEntryData soldTo = partnerList.getSoldToData();
            if (soldTo != null && soldTo.getPartnerId() != null) {
                event.setProperty("Order", "CustomerID", soldTo.getPartnerId());
            }
        }

        // convert the total value to the global number format.
        String grossValue = header.getGrossValue() != null ? header.getGrossValue():"";

        NumberFormat numberFormat = getNumberFormat(bom);

        float priceValue = 0;

        try {
            priceValue = numberFormat.parse(grossValue).floatValue();
        }
        catch(ParseException ex) {
            logException(ex);
        }
		catch(Throwable t){
			logException(t);
		}

        event.setProperty("Order", "TotalPrice", localeNumberFormat.format(priceValue));
    }


    /**
     * You can overwrite this method to implement your own price determination.<br>
     * By default the method <code>getGrossValue</code> for the <code>ItemSalesDoc</code>
     * is used.
     *
     * @param item sales doc item to determine total price
     *
     * @return
     */
    protected String getTotalPriceFromSalesItem(ItemSalesDoc item) {
        return item.getGrossValue() == null  ?  ""  : item.getGrossValue();
    }



    /**
     * Sets the given item info in the item event. <br>
     *
     * You can overwrite this method to filter or enhance the event output.
     * The flag <code>fromProduct</code> give you the source of the current informations.
     *
     * Use the method <code>getName</code> of <code>CapturerEvent</code> to
     * get information over the current event. <br>
     * Futheron the switch <code>removeEmptyValues</code> to filter empty output
     * from the event output. <br>
     *
     * The current implementation is:
     *  <pre>
     *  event.setProperty("Item", "AvailabilityDate", availabilityDate);
     *  event.setProperty("Item", "Availability",     availability);
     *  event.setProperty("Item", "Currency",         currency);
     *  event.setProperty("Item", "Category",         category);
     *  event.setProperty("Item", "BasketID",         basketId);
     *  event.setProperty("Item", "OldQuantity",      oldQuantity);
     *  event.setProperty("Item", "ProductName",      prodName);
     *  event.setProperty("Item", "SKU",              productId);
     *  event.setProperty("Item", "Quantity",         quantity);
     *  event.setProperty("Item", "TotalPrice",       totalPrice);
     *  event.setProperty("Item", "Unit",             unit);
     *  event.setProperty("Item", "UnitPrice",        price);
     *  </pre>
     *
     * @param event event, which is used
     * @param fromProduct flag if the product or the item sales item is used
     * @param item sales document item
     * @param product  product
     * @param bom the business object manager
     * @param availabilityDate availabilityDate
     * @param availability flag if product is available
     * @param currency used currency
     * @param category catalog category
     * @param basketId techKey of the basket
     * @param oldQuantity old quantity
     * @param prodName name of the product
     * @param productId id of the product (not the tech key)
     * @param quantity quantity
     * @param totalPrice total price
     * @param unit unit of measurement
     * @param price price per unit
     *
     * @see com.sap.isa.isacore.TealeafInitHandler
     * @see com.sap.isa.core.businessobject.event.capturer.CapturerEvent
     *
     */
    protected void setItemInfo(CapturerEvent event,
                               boolean fromProduct,
                               ItemSalesDoc item,
                               Product product,
                               BusinessObjectManager bom,
                               Date   availabilityDate,
                               String availability,
                               String currency,
                               String category,
                               String basketId,
                               String oldQuantity,
                               String prodName,
                               String productId,
                               String quantity,
                               String totalPrice,
                               String unit,
                               String price) {

        event.setProperty("Item", "AvailabilityDate", availabilityDate);
        event.setProperty("Item", "Availability",     availability);
        event.setProperty("Item", "Currency",         currency);
        event.setProperty("Item", "Category",         category);
        event.setProperty("Item", "BasketID",         basketId);
        event.setProperty("Item", "OldQuantity",      oldQuantity);
        event.setProperty("Item", "ProductName",      prodName);
        event.setProperty("Item", "SKU",              productId);
        event.setProperty("Item", "Quantity",         quantity);
        event.setProperty("Item", "TotalPrice",       totalPrice);
        event.setProperty("Item", "Unit",             unit);
        event.setProperty("Item", "UnitPrice",        price);
    }


    /**
     * The method decide how to get the item information and forward
     * to the appropriate method.<br>
     *
     * The method try to get the corresponding from the product catalog.
     * If the product could be found the information is taken from
     * sales document item directly.
     *
     * @param event       the used capturer event.
     * @param item        the current sales document.
     * @param documentId  the document id
     * @param bom         the business object manager
     * @param cbom        the catalog business object manager
     */
    protected void setSalesDocumentItem(CapturerEvent                 event,
                                        ItemSalesDoc                  item,
                                        TechKey                       documentId,
                                        BusinessObjectManager         bom,
                                        CatalogBusinessObjectManager  cbom) {

        boolean productFound = false;

        WebCatInfo catalog = cbom.getCatalog();
        TechKey productKey = item.getProductId();

        if(catalog != null && productKey != null) {
            Product product = new Product(productKey);
            String areaType = null;
            // redemption item
            if ( item.isPtsItem() ) {
                areaType = WebCatArea.AREATYPE_LOY_REWARD_CATALOG;
            }
            // buy points item
//            if ( item.isBuyPointsItem() ) {
//                areaType = WebCatArea.AREATYPE_LOY_BUY_POINTS;
//            }

            if( product.enhance(catalog, true, areaType) ) {
                productFound = true;
                setItemFromProduct(event,
                                   product,
                                   documentId.getIdAsString(),
                                   item.getQuantity(),
                                   item.getOldQuantity(),
                                   item,
                                   bom,
                                   cbom);
            }
        }

        if(!productFound) {
            setItemFromSalesItem(event,
                                 item,
                                 documentId,
                                 bom);
        }
    }


    /**
     *
     * Determine the user id used with the events.
     *
     * @param name  name of the event currently captured.
     * @param user  reference to the user, which id will be retrieved
     *
     * @return the userId
     *
     */
    protected String getUserId(String eventName, User user) {


        // if no id found use userId from the login
        return user.getUserId();
    }


    /**
     *
     * Determine the soldto id used with the events.
     *
     * @param name  name of the event currently captured.
     * @param bom  reference to the bom
     *
     * @return the soldToId or empty string
     *
     */
    protected String getSoldToId(String eventName, BusinessObjectManager bom) {


        BusinessPartnerManager buPaMa = bom.createBUPAManager();
        
        BusinessPartner buPa = buPaMa.getDefaultBusinessPartner(PartnerFunctionData.SOLDTO);
        
        if (buPa != null) {
            return buPa.getId();
        }
        return "";
    }




    /*  Internal help fuction */
    private void setItemFromSalesItem(CapturerEvent              event,
                                      ItemSalesDoc               item,
                                      TechKey                    documentId,
                                      BusinessObjectManager      bom) {


        NumberFormat numberFormat = getNumberFormat(bom);

        String productId = item.getProduct() == null       ?  ""  : item.getProduct();
        String currency = item.getCurrency() == null       ?  ""  : item.getCurrency();
        String description = item.getDescription() == null ?  ""  : item.getDescription();
        String oldQuantity = item.getOldQuantity() == null ?  "0" : item.getOldQuantity();
        String price = "";
        String totalPrice = getTotalPriceFromSalesItem(item);
        String availability = "0";
        Date availabilityDate = theDate;

        String date = item.getConfirmedDeliveryDate();

        // if (date != null && date.length()>0) {
        //      availabilityDate = bom.getShop().parseDate(date);
        // }

        if(item.getConfirmedQuantity() != null && !item.getConfirmedQuantity().equals("0")) {
            availability = "1";
        }

        if(oldQuantity.length() == 0) {
            oldQuantity = "0";
        }

        if(productId.length() > 0) {

            // calculate the the price from the totalPrice
            if(totalPrice.length()>0) {
				float quantity = (float)0;
				float priceValue = (float) 0;
                try {
                    Number number = numberFormat.parse(item.getQuantity());

                    quantity = number.floatValue();


                    number = numberFormat.parse(totalPrice);

                    priceValue = number.floatValue();

                    // convert the price to the NumberFomat given with the local
                    // because tealeaf couldn't know over numberformat.
                    price = localeNumberFormat.format((double)priceValue / quantity);
                    totalPrice = localeNumberFormat.format((double)priceValue);
                }
                catch(ParseException ex) {
                    log.error(LogUtil.APPS_BUSINESS_LOGIC, "tealeaf.error.numberFormat", ex);
                    log.error("unable to parse " + item.getQuantity() +" or totalprice " + totalPrice);
					price = totalPrice = "0"; // printed as error in which case upload fails
                }
            }

            setItemInfo(event,
                        false,
                        item,
                        null,
                        bom,
                        availabilityDate,
                        availability,
                        currency,
                        "",
                        documentId.getIdAsString(),
                        oldQuantity,
                        description,
                        productId,
                        item.getQuantity(),
                        totalPrice,
                        item.getUnit(),
                        price);
        }
    }


    /*  Internal help fuction */
    private void setItemFromProduct(CapturerEvent                 event,
                                    Product                       product,
                                    String                        basketId,
                                    String                        quantity,
                                    String                        oldQuantity,
                                    ItemSalesDoc                  item,
                                    BusinessObjectManager         bom,
                                    CatalogBusinessObjectManager  cbom) {

        WebCatInfo catalog = cbom.getCatalog();
        NumberFormat numberFormat = getNumberFormat(bom);

        if(oldQuantity.length() == 0) {
            oldQuantity = "0";
        }

        String totalPrice = "";
        String availability = "";
        Date availabilityDate = theDate;

        String productId = product.getId();
        String description = product.getDescription();
        String currency = product.getCurrency();
        String price = product.getPriceValue();
        String unit = product.getUnit();


        // get name of the area from id, only fro no basket items
        String category = "";

        if(basketId.length() == 0) {
            WebCatItem webCatItem =  product.getCatalogItem();
            if(webCatItem.getAreaID().length()>0) {
                WebCatArea area = catalog.getArea(webCatItem.getAreaID());
                if(area != null) {
                    category = area.getAreaName();
                }
            }
        }


        if(isAvailibiltyCheckAvailable) {
            availability = "0";
            if(cbom.getAtp() == null) {
                cbom.createAtp();
            }
            if(cbom.getAtp() != null) {
                product.setAvailabilityData(cbom.getAtp(),
                                            bom.getShop().getCatalog(),
				                            bom.getShop().getId(),
                                            bom.getShop().getCountry(),
                                            quantity);
                if(product.isAvailable()) {
                    availability = "1";
                }
                if(product.getAvailabilityDate()!= null) {
                    availabilityDate = product.getAvailabilityDate();
                }
            }
        }

        if(productId.length() > 0) {

            // calculate the the total price
            if(price.length()>0) {
				float priceValue = (float) 0;
                try {
                    Number number = numberFormat.parse(quantity);

                    float quantityValue = number.floatValue();

                    number = numberFormat.parse(price);

                    priceValue = number.floatValue();

                    // convert the price to the NumberFomat given with the local
                    // because tealeaf couldn't know over numberformat.
                    price = localeNumberFormat.format((double)priceValue);
                    totalPrice = localeNumberFormat.format((double)priceValue * quantityValue);

                }
                catch(ParseException ex) {
                	log.error(LogUtil.APPS_BUSINESS_LOGIC, "tealeaf.error.numberFormat", ex);
					log.error("unable to parse " + quantity +" or price " + price);
					price = totalPrice = "0"; // printed as error in which case upload fails
                }
            }

            setItemInfo(event,
                        true,
                        item,
                        product,
                        bom,
                        availabilityDate,        // availability date
                        availability,            // availability
                        currency,                // currency
                        category,                // category
                        basketId,                // basket id
                        oldQuantity,             // old quantity
                        description,             // productname
                        productId,               // product id
                        quantity,                // quantity
                        totalPrice,              // total price
                        unit,                    // unit
                        price                    // price per unit
                       );
        }
    }




    private void logError(Exception ex){
        log.error(LogUtil.APPS_BUSINESS_LOGIC, "tealeaf.error.write", ex);
    }

    private void logException(Exception ex){
        log.error(LogUtil.APPS_BUSINESS_LOGIC, "tealeaf.exception", ex);
    }

	private void logException(Throwable t) {
		log.error(LogUtil.APPS_BUSINESS_LOGIC, "tealeaf.exception", t);
	}

    /**
     * Read the numberFormat from the shop
     *
     * @param bom
     *
     * @return numberFormat
     *
     */
    protected NumberFormat getNumberFormat(BusinessObjectManager bom) {

        Shop shop = bom.getShop();
        if(shop != null) {
            return shop.getNumberFormat();
        }

        return NumberFormat.getInstance();
    }

}
