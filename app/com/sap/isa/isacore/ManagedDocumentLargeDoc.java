/**
 *  Class:        ManagedDocumentLargeDoc
 *  Description:  ManagedDocument, to store some additional informations for the handling of large documents
 *  Copyright (c) 2004, SAP AG, All rights reserved.
 *  Author:       SAP AG
 *  Created:      10.01.2004
 *  Version:      1.0
 *  $Revision:    #1 $
 *  $Date:        2004/01/10 $
 */

package com.sap.isa.isacore;

import com.sap.isa.businessobject.DocumentState;


public class ManagedDocumentLargeDoc extends ManagedDocument {
    
    /**
     * Constant to define undefined search property
     */
    public static final String ITEM_PROPERTY_NONE = "none";
    
    /**
     * Constant to define product search property
     */
    public static final String ITEM_PROPERTY_PRODUCT = "product";
    
    /**
     * Constant to define internal ref no search property
     */
    public static final String ITEM_PROPERTY_NUMBERINT = "numberint";
    
    /**
     * Constant to define description no search property
     */
    public static final String ITEM_PROPERTY_DESC = "description";

    /**
     * Constant to define description no search property
     */
    public static final String ITEM_PROPERTY_BACKORDER = "backorder";
    
  
    /**
     * Constant to define description no search property
     */
    public static final String ITEM_PROPERTY_ORDDELDUE = "orddeldue";   
    /**
     * Status OPEN
     */
    public static final String ITEM_STATUS_NOT_SPECIFIED = "all";
    
    /**
     * Status OPEN
     */
    public static final String ITEM_STATUS_OPEN = "open";

    /**
     * Status IN PROCESS
     */
    public static final String ITEM_STATUS_INPROCESS = "inprocess";
    
    /**
     * Status COMPLETED
     */
    public static final String ITEM_STATUS_COMPLETED = "completed";
    
    /**
     * Status DELIVERED
     */
    public static final String ITEM_STATUS_DELIVERED = "delivered";
    
    /**
     * If no search has taken place yet, this marks, that the the
     * number of found items is undefined
     */
    public static final int NO_OF_FOUND_ITEMS_UNDEFINED = -1;
    
    /**
     * For large Documents, status of the items to be searched
     */
    protected String itemSearchStatus = "";

    /**
     * For large Documents, the property of the item to be searched on
     */
    protected String itemSearchProperty = "";
    
    /**
     * For large Documents, the low value of the item property to be searched
     */
    protected String itemSearchPropertyLowValue = "";
    
    /**
     * For large Documents, the high value of the item property to be searched
     */
    protected String itemSearchPropertyHighValue = "";
    
    /**
     * For large Documents, the number of items, that were found by the
     * last search
     */
    protected int numberofItemsFound = NO_OF_FOUND_ITEMS_UNDEFINED;
    
    /**
     *  Constructor
     *
     *@param  doc            Reference off the corresponding business object.
     *@param  docType        Type of the document, e.g. "order", "quotation", "order template".
     *@param  docNumber      Number of the document, given by the backend.
     *@param  refNumber      The reference number; the number the customer can assign to the document.
     *@param  refName        The reference name; the name the customer can assign to the document.
     *@param  docDate        Date of the document.
     *@param  forward        String for the logical forward mapping in the struts framework.
     *@param  href           Href for displaying the document
     *@param  hrefParameter  Additional parameters for the String href.
     */
    public ManagedDocumentLargeDoc(DocumentState doc, String docType, String docNumber, String refNumber,
            String refName, String docDate, String forward, String href, String hrefParameter) {

            super(doc, docType, docNumber, refNumber, refName, docDate, forward, href, hrefParameter);
    }

    /**
     * Returns the product, selected items from a large document should have
     * 
     * @return the property of the item to be searched on
     */
    public String getItemSearchProperty() {
        return itemSearchProperty;
    }

    /**
     * Returns the status, selected items from a large doument should have
     * 
     * @return the status the selected items should have
     */
    public String getItemSearchStatus() {
        return itemSearchStatus;
    }

    /**
     * Sets the product, selected items from a large doument should have
     * 
     * @param itemSearchProperty the property of the item to be searched on
     */
    public void setItemSearchProperty(String itemSearchProperty) {
        this.itemSearchProperty = itemSearchProperty;
    }

    /**
     * Sets the status, selected items from a large doument should have
     * 
     * @param itemSearchStatus the status the selected items should have
     */
    public void setItemSearchStatus(String largeDocItemStatus) {
        this.itemSearchStatus = largeDocItemStatus;
    }

    /**
     * Get the low value of the item property to be searched
     * 
     * @return the low value of the item property to be searched
     */
    public String getItemSearchPropertyLowValue() {
        return itemSearchPropertyLowValue;
    }
    
    /**
     * Get the high value of the item property to be searched
     * 
     * @return the high value of the item property to be searched
     */
    public String getItemSearchPropertyHighValue() {
        return itemSearchPropertyHighValue;
    }

    /**
     * Set the the low value of the item property to be searched
     * 
     * @param itemSearchPropertyValue the low value of the item property to be searched
     */
    public void setItemSearchPropertyLowValue(String itemSearchPropertyLowValue) {
        this.itemSearchPropertyLowValue = itemSearchPropertyLowValue;
    }
    
    /**
     * Set the the high value of the item property to be searched
     * 
     * @param itemSearchPropertyValue the high value of the item property to be searched
     */
    public void setItemSearchPropertyHighValue(String itemSearchPropertyHighValue) {
        this.itemSearchPropertyHighValue = itemSearchPropertyHighValue;
    }

    /**
     * Returns the number of items that were found by the last search
     * or NO_OF_FOUND_ITEMS_UNDEFINED if no search has yet taken place
     * 
     * @return int number of items that were found by the last search
     */
    public int getNumberofItemsFound() {
        return numberofItemsFound;
    }

    /**
     * Sets the number of items that were found by the last search.
     * Should be NO_OF_FOUND_ITEMS_UNDEFINED if no search has yet 
     * taken place
     * 
     * @param int number of found items
     */
    public void setNumberofItemsFound(int numberofItemsFound) {
        this.numberofItemsFound = numberofItemsFound;
    }

}