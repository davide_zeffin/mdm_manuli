/*****************************************************************************
    Interface:    AutoentryErrorConstants
    Copyright (c) 2003, SAP AG, Germany, All rights reserved.
    Author:       SAP AG

    $Revision: #1 $
    $DateTime: 2003/01/06 16:25:20 $ (Last changed)
    $Change: 103557 $ (changelist)

*****************************************************************************/

package com.sap.isa.isacore;

/**
*
* With this interface Internet Sales communications problems which occured during
* auto entry (ie. used by Portal) to external application. During auto entry, no
* selection screen (ie. sold-to selection, shop selection, ...) or any other interaction
* screen can be presented. So a used configuration in combination with auto entry will
* end on an error page.
*/
public interface AutoentryErrorConstants {

/**
* Code to signal the multiple assignment of sold-to parties to a contact
*/
public static final String ERROR_MULTIPLE_SOLDTOS = "9000";
/**
* Code to signal that no shop has been specified via URL but shoplist contains
* more than one shop
*/
public static final String ERROR_FINDING_SHOP = "9001";
/**
* Code to signal that used shop has catalog determination activated.
*/
public static final String ERROR_CAT_DETERMINATION_ACTIVE = "9002";

}
