/**
 *  Class:        HistoryLoader
 *  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
 *  Author:       Franz-Dieter Berger
 *  Created:      20.06.2001
 *  Version:      1.0
 *  $Revision: #4 $
 *  $Date: 2001/07/18 $
 */

package com.sap.isa.isacore;

/**
 *  Loads and saves the elements of the history.
 *  It is an abstract baseclass, loading and storing are empty functions
 *  which must be overriden in derived classes.
 *  @see History
 *
 *@author     Franz-Dieter Berger
 *@created    3. Dezember 2001
 *@version    1.0
 */
public class HistoryLoader {

    /**
     *  Reference to the History.
     */
    protected History history = null;


    /**
     *  Set the reference to the history.
     *
     *@param  h  Reference on History.
     */
    public void setHistory(History h) {
        history = h;
    }


    /**
     *  Loads the elements of the history
     *  Does here nothing and returns true.
     *
     *@param  userid  Unambiguous ID of the user.
     *@param  shopid  Unambiguous ID of the shop.
     *@return         <code>true</code>, if the cookie could be loaded,<br>
     *                <code>false</code>, else.
     */
    public boolean load(String userid, String shopid) {
        return true;
    }


    /**
     *  Saves the elements of the history
     *  Does here anything. Returns true
     *  overrides in derived classes,
     *
     *@param  userid  Unambiguous ID of the user.
     *@param  shopid  Unambiguous ID of the shop.
     *@return         <code>true</code>, if the cookie could be saved,<br>
     *                <code>false</code>, else.
     */

    public boolean save(String userid, String shopid) {
        return true;
    }
}
