/**
 *  Class:        History
 *  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
 *  Author:       Franz-Dieter Berger
 *  Created:      25.05.2001
 *  Version:      1.0
 *  $Revision: #5 $
 *  $Date: 2001/08/01 $
 */

package com.sap.isa.isacore;

import com.sap.isa.core.Iterable;
import com.sap.isa.core.logging.IsaLocation;
import java.util.*;

/**
 *  Maintains a list of HistoryItems.
 *  @see HistoryItem
 *  @see HistoryLoader
 *  @see DocumentHandler
 *
 *@author     Franz-Dieter Berger
 *@created    29. November 2001
 *@version    1.0
 */
public class History
         implements Iterable {

    /**
     *  List of HistoryItems.
     */
    protected List historyItems = new ArrayList();

    /**
     *  Maximal length of the List of HistoryItems.
     */
    protected int maxlength = 10;

    /**
     *  Reference to object, which loads and saves all HistoryItems.
     */
    protected HistoryLoader historyLoader = null;

    /**
     *  Possibly a history item references to a document which does not exist anymore
     *  (perhaps somebody deleted it in the CRM). The responsible action must handle
     *  this case and can use this constant for remembering this status.
     */
    public final static String NODOCFOUND = "NODOCFOUND";

    /**
     * Internal upper bound for the length of the history, just for security.
     * We don't want to use to much memory in the session contect.
     */
    private final static int MAXLENGTH = 100;

    protected static IsaLocation log;
    boolean isDebugEnabled;
	
	public History() {
		log = IsaLocation.getInstance(this.getClass().getName());
		isDebugEnabled = log.isDebugEnabled();
	}
    /**
     *  Set the maximal number of elements in the history. If the history contains
     *  more elements than the new maximal number, then the last elements will be removed.
     *
     *@param  length  The maximal number of elements in the history.
     *@return         <code>true</code>, if the new length could be set<br>
     *                <code>false</code>, if the new length could not be set, because
     *                length >= 0 or length <= MAXLENGTH
     */

    public boolean setLength(int length) {
        boolean ret = false;
        if (length >= 0 && length <= MAXLENGTH) {
            maxlength = length;

            // Matches the history on new length
            ret = true;
        }

        // if the list has more than maxlength elements, delete the last elements
        while (historyItems.size() > maxlength) {
            historyItems.remove(maxlength);
        }

        return ret;
    }

    /**
     *  Sets the history loader for loading and storing history items.
     *
     *@param  loader  The history loader, or <code>null</code>.
     */
    public void setLoader(HistoryLoader loader) {
        historyLoader = loader;
        if (historyLoader != null) {
            historyLoader.setHistory(this);
        }
    }



    /**
     *  Returns the list of history items.
     *
     *@return    The list of history items
     */
    public List getHistoryList() {
        return historyItems;
    }


    /**
     *  Gives the history loader for loading and storing history items.
     *
     *@return    The history loader, or <code>null</code>, if there is no loader.
     */

    public HistoryLoader getLoader() {
        return historyLoader;
    }


    /**
     *  Returns an iterator over the HistoryItems
     *  (needed for the iterator interface)
     *
     *@return    Iterator over history items.
     */
    public Iterator iterator() {
        return historyItems.iterator();
    }


    /**
     *  Adds a ManagedDocument to the history
     *  if doc==null nothing happens; return false.
     *  if doc.getHref==null the doc will not be added to the history; return false.
     *  if doc has added to the history, return true.
     *  If doc has already been in the list, it will be deleted at the relevant place
     *  and pasted at the beginning, return true.
     *
     *@param  doc  The ManagedDocument, whisch should be added.
     *@return      <code>true</code> or <code>false</code>
     */

    public boolean add(ManagedDocument doc) {
		final String METHOD = "add()";
		log.entering(METHOD);
 
        boolean ret = false;

        if (doc == null) {
            if (log.isDebugEnabled()) {
                log.debug("add(doc): doc is null; not added");
            }
            log.exiting();
            return false;
        }
        if (doc.getHref() == null) {
            if (log.isDebugEnabled()) {
                log.debug("add(doc): doc.getHref() is null; not added");
            }
            log.exiting();
            return false;
        }
        // test if doc can be displayed in the history list
        if (doc.getHref() != null) {
            // historyItem erzeugen
            HistoryItem hisitem = new HistoryItem(doc.getDocType(), doc.getDocNumber(),
                    doc.getRefNumber(), doc.getRefName(), doc.getDate(), doc.getHref(), doc.getHrefParameter());

            // if list is empty add doc

            if (historyItems.isEmpty()) {
                historyItems.add(hisitem);
                ret = true;
                if (log.isDebugEnabled()) {
                    log.debug("add(doc): list was empty -> doc successfully added");
                }
            }
            else {
                // remove doc from list; if doc is not in the list then nothing should happen
                for (int i = 0; i < historyItems.size(); ++i) {
                    HistoryItem hi = (HistoryItem) historyItems.get(i);
                    if (hi != null && hi.equals(hisitem)) {
                        historyItems.remove(i);
                    }
                }
                // add doc to the beginning of the list
                if (!(doc.getDocNumber() == null || doc.getDocNumber().equals("new"))) {
                    historyItems.add(0, hisitem);
                    if (log.isDebugEnabled()) {
                        log.debug("add(doc): doc successfully added at the beginning of the list");
                    }
                    ret = true;
                }
            }
            // if the list has more than maxlength elements, delete the last elements

            if (historyItems.size() > maxlength) {
                historyItems.remove(maxlength);
            }
        }
        log.exiting();
        return ret;
    }


    /**
     *  Removes a HistoryItem from the history.
     *  if doctype==null nothing happens; return false.
     *  if docnumber==null nothing happens; return false.
     *  if doc has removed from the history, return true.
     *
     *@param  doctype    see HistoryItem
     *@param  docnumber  see HistoryItem
     *@return            <code>true</code> or <code>false</code>
     */
    public boolean remove(String doctype, String docnumber) {
		final String METHOD = "remove()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("doctype="+doctype+", docnumber="+docnumber);
        boolean ret = false;

        if (doctype == null) {
            if (log.isDebugEnabled()) {
                log.debug("remove(doctype, docnumber): nothing removed, because doctype=null");
            }
            log.exiting();
            return false;
        }
        if (docnumber == null) {
            if (log.isDebugEnabled()) {
                log.debug("remove(doctype, docnumber): nothing removed, because docnumber=null");
            }
            log.exiting();
            return false;
        }

        if ((doctype.length() > 1) && (docnumber.length() > 1)) {
            // create historyItem
            HistoryItem hisitem = new HistoryItem(doctype, docnumber, "", "", "", "", "");

            if (historyItems.isEmpty()) {
                ret = true;
            }
            else {
                // remove doc from list; if doc is not in the list then nothing should happen
                for (int i = 0; i < historyItems.size(); ++i) {
                    HistoryItem hi = (HistoryItem) historyItems.get(i);
                    if (hi != null && hi.equals(hisitem)) {
                        historyItems.remove(i);
                        if (log.isDebugEnabled()) {
                            log.debug("remove(): successfully removed");
                        }
                    }
                }
                ret = true;
            }
        }
        log.exiting();
        return ret;
    }


    /**
     *  Loads the elements of the history.
     *
     *@param  userid      Unambiguously defining the user.
     *@param  conallowed  <code>true</code> if the shop allows contracts; <code>false</code> else
     *@param  quoallowed  <code>true</code> if the shop allows quotations; <code>false</code> else
     *@return             <code>true</code> the items could be loaded, <code>false</code> else
     */
//    public boolean load(String userid, boolean conallowed, boolean quoallowed) {
    public boolean load(String userid, String shopid) {
		final String METHOD = "load()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("userId="+userid+", shopId="+shopid);
        boolean ret = false;
        if (historyLoader != null) {
            ret = historyLoader.load(userid, shopid);
        }
        log.exiting();
        return ret;
    }


    /**
     *  Saves the elements of the history
     *
     *@param  userid  Unambiguously defining the user.
     *@return         <code>true</code> the items could be saved, <code>false</code> else
     */
    public boolean save(String userid, String shopid) {
		final String METHOD = "save()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("userId="+userid+", shopId="+shopid);
        boolean ret = false;
        if (historyLoader != null) {
            ret = historyLoader.save(userid, shopid);
        }
        log.exiting();
        return ret;
    }
}
