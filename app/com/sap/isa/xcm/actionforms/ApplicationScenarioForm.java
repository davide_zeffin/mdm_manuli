/*
 * Created on Mar 9, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.xcm.actionforms;

import java.util.Iterator;

import javax.servlet.jsp.JspFactory;
import javax.servlet.jsp.PageContext;

import com.sap.isa.auction.actionforms.BaseFormConstants;
import com.sap.isa.auction.logging.LogHelper;
import com.sap.isa.auction.util.FormUtility;
import com.sap.isa.core.Constants;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.core.xcm.ConfigContainer;
import com.sap.isa.core.xcm.scenario.ScenarioConfig;
import com.sap.isa.core.xcm.scenario.ScenarioManager;
import com.sap.isa.ui.controllers.ActionForm;
import com.sapportals.htmlb.GridLayout;
import com.sapportals.htmlb.GridLayoutCell;
import com.sapportals.htmlb.ItemList;
import com.sapportals.htmlb.Link;
import com.sapportals.htmlb.TextView;
import com.sapportals.htmlb.enum.CellVAlign;
import com.sapportals.htmlb.enum.TextViewDesign;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ApplicationScenarioForm extends ActionForm {

	public static final String SUCCESSFORWARD = "success";
	public static final String SCENARIOSITEMLIST = "ItemList.Scenarios";
	public static final String CHOOSESCENARIOTEST = "Choose.Scenario";
	public static final String APPLICATIONURL = "/init.do";
	public static final String APPLICATIONHEADER = "Title.Explorer";
	public static final String CHOOSESCENARIO = "Choose.Scenario";
	public static final String NOSCENARIO = "NoScenarios.Text";
	public static final String EXTENDCONFMANAGEMENT = "Scenarios.Admin";
	public static final String SPACES = "                ";
	public static final String SCENARIONAME = "ScenarioName.Admin";
	public static final String DEFSCENARIONAME = "DefaultScenarioName.Admin";
	
	public void showAllScenarios()  {
		
		ItemList itemList = (ItemList)addControl(HTMLB_ITEMLIST , SCENARIOSITEMLIST);
		Iterator iterate = itemList.iterator();
		while(iterate.hasNext())  {
			iterate.next();
			iterate.remove();
		}
		int i = 0;
		try  {
			Iterator iterator = ScenarioManager.getScenarioManager().getScenarioNamesIterator();
			String name = null;
			ConfigContainer container = null;
			Link link = null;
			TextView view = null;
			StringBuffer url = new StringBuffer(WebUtil.getAppsURL(getServlet().getServletContext() , 
				request , response , null , APPLICATIONURL , null , null , false));
			url.append("?");
			url.append(Constants.XCM_SCENARIO_RP);
			url.append("=");
			String scenarioURL = null;
			ScenarioConfig config = null;
			GridLayout grid = null;
			GridLayoutCell cell = null;
//			String scenarioName = translate(
//					BaseFormConstants.BASEBUNDLE ,SCENARIONAME);
//			String defScenarioName = translate(
//					BaseFormConstants.BASEBUNDLE ,DEFSCENARIONAME);
//			BaseUI baseUI = new BaseUI(createPageContext());
			while(iterator.hasNext())  {
				name = (String)iterator.next();
				//container = FrameworkConfigManager.XCM.getXCMScenarioConfig(name);
				if(ScenarioManager.getScenarioManager().isSAPScenario(name))
					continue;
				view = new TextView();
				view.setText(name);
				link = new Link(name);
				config = ScenarioManager.getScenarioManager().getScenarioConfig(name);
				if(config.isDefaultScenario())  {
					view.setDesign(TextViewDesign.EMPHASIZED);
//					if ((null !=baseUI ) && (baseUI.isAccessible)){
//						link.setTooltip(defScenarioName + name);
//					}
				} else {
//					if ((null !=baseUI ) && (baseUI.isAccessible)){
//						link.setTooltip(scenarioName + name);					
//					}
					}
				link.addComponent(view);
				grid = new GridLayout();
				grid.setCellSpacing(5);
				cell = new GridLayoutCell(link);
				cell.setVAlignment(CellVAlign.TOP);
				cell.setWidth("100");
				grid.addCell(1 , 1 , cell);
				
				view = new TextView();
				view.setText(config.getLongtext());
				view.setWrapping(true);
//				if (baseUI.isAccessible){
//					view.setTooltip(config.getLongtext());					
//				}
				if(config.isDefaultScenario())  {
					view.setDesign(TextViewDesign.EMPHASIZED);
				}
				cell = new GridLayoutCell(view);
				grid.addCell(1 , 2, cell);
				scenarioURL = url.toString() + name;
				link.setReference(scenarioURL);
				itemList.addComponent(grid);
				i++;
			}
		}  catch(Exception exc)  {
			LogHelper.logError(cat, location, exc);
		}
		if(i == 0)  {
			TextView view = new TextView();
			view.setText(translate(BaseFormConstants.BASEBUNDLE,NOSCENARIO));
			view.setWrapping(true);
			view.setDesign(TextViewDesign.EMPHASIZED);
			itemList.addComponent(view);
		}
	}
	
	/**
	 * Returns pageContext class used to determine accessibility properties. This is
	 * different from the HTMLB IPageContext, which cannot be used by the BaseUI
	 * class to read the startup parameters for the application.
	 * 
	 * @return pageContext
	 */
	private PageContext createPageContext() {
		JspFactory jspFactory = JspFactory.getDefaultFactory();
		PageContext pContext = null;
		if (null != jspFactory){
			pContext = jspFactory.getPageContext(
					this.getServlet(), request, response, "", false, 8192, true);
		}
		return pContext;
	}

	public String translate(String resourceId, String key) {
		return FormUtility.getResourceString(resourceId , key , session);
	}

}
