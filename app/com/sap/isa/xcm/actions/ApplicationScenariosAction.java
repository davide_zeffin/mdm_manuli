/*
 * Created on Mar 9, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.xcm.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.xcm.actionforms.ApplicationScenarioForm;
import com.sapportals.htmlb.rendering.IPageContext;
import com.sapportals.htmlb.rendering.PageContextFactory;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ApplicationScenariosAction extends Action {

	public ActionForward perform(
		ActionMapping mapping,
		ActionForm actionForm,
		HttpServletRequest request,
		HttpServletResponse response)
		throws IOException, ServletException {
			ApplicationScenarioForm appScenarioForm = (ApplicationScenarioForm)actionForm;
			IPageContext pageContext = PageContextFactory.createPageContext(request , response);
			appScenarioForm.setPageContext(pageContext);
			appScenarioForm.setServlet(getServlet());
			appScenarioForm.setRequest(request);
			appScenarioForm.setResponse(response);
			appScenarioForm.setSession(request.getSession());
			appScenarioForm.showAllScenarios();
			request.setAttribute(com.sap.isa.ui.controllers.ActionForm.ACTIONFORMKEY , actionForm.getClass().getName());
			request.setAttribute(actionForm.getClass().getName() , actionForm);
			
			return mapping.findForward(ApplicationScenarioForm.SUCCESSFORWARD);
	}
}
