/*****************************************************************************
    Class:        HelpValues
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      February 2002
    Version:      1.0

    $Revision: #7 $
    $Date: 2001/07/25 $
*****************************************************************************/

package com.sap.isa.maintenanceobject.util;

import com.sap.isa.core.util.table.ResultData;


/**
 * The HelpValues object allows a generic support to selected arbitrary help values
 * for properties of maintenance objects. The helpValues are stored in an
 * ResultSet. The object contains also the information which column in the ResultSet
 * correspond to which property.
 *
 * @author SAP
 * @version 1.0
 *
 * @see com.sap.isa.maintenanceobject.businessobject.PropertyGroup
 * @see com.sap.isa.maintenanceobject.businessobject.MaintenanceObject
 * @see com.sap.isa.helpvalues.HelpValues
 * 
 * @deprecated use instead {@link com.sap.isa.helpvalues.HelpValues}
 * 
 */

public class HelpValues extends com.sap.isa.helpvalues.HelpValues {

	/**
	 * Standard constructor. <br>
	 * 
	 */
	public HelpValues() {
		super();
	}

	/**
	 * Standard constructor. <br>
	 * @param description
	 * @param values
	 */
	public HelpValues(String description, ResultData values) {
		super(description, values);
	}

 
    public class PropertyLink extends com.sap.isa.helpvalues.HelpValues.PropertyLink{

        /**
         * Standard constructor. <br>
         * @param propertyName
         * @param columnName
         */
        public PropertyLink(String propertyName, String columnName) {
            super(propertyName, columnName);
        }

    }

}
