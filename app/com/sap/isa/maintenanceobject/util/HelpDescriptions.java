/*****************************************************************************
    Class:        HelpValues
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      February 2002
    Version:      1.0

    $Revision: #7 $
    $Date: 2001/07/25 $
*****************************************************************************/

package com.sap.isa.maintenanceobject.util;

import com.sap.isa.core.Iterable;

import java.util.*;

/**
 * The HelpDescription object allows a generic support to display help texts
 * for properties of maintenance objects. The help description are stored in an
 * either in the property or are written from the backend. <br>
 * The object support different formats
 *
 * @author SAP
 * @version 1.0
 *
 * @see com.sap.isa.maintenanceobject.businessobject.PropertyGroup
 * @see com.sap.isa.maintenanceobject.businessobject.MaintenanceObject
 */

public class HelpDescriptions implements Iterable {

	/**
	 * constant for resource_key as description 
	 */
	public final static String RESOURCE_KEY = "resourceKey";   

	/**
	 * constant for resource_key as description 
	 */
	public final static String HTML = "html";   


    protected String format;
    protected List descriptionList;

    /**
     *
     * Standard Constructor
     *
     */
    public HelpDescriptions(){
    }


    /**
     * Constructor with fomat and iterator
     * 
     * @param format format to use
     * @iterator to use
     *
     */
    public HelpDescriptions(String format, List descriptionList){
    	this.format = format;
    	this.descriptionList = descriptionList;
    }

    /**
     * Set the property format
     *
     * @param format format to be used
     *
     */
    public void setFormat(String format) {
        this.format = format;
    }


    /**
     * Returns the property format
     *
     * @return values format to be used
     *
     */
    public String getFormat() {
       return this.format;
    }


	/**
	 * Returns the descriptionList.
	 * @return List
	 */
	public List getDescriptionList() {
		return descriptionList;
	}

	/**
	 * Sets the descriptionList.
	 * @param descriptionList The descriptionList to set
	 */
	public void setDescriptionList(List descriptionList) {
		this.descriptionList = descriptionList;
	}


    /**
     * Returns the links between properties and column names
     *
     * @return iterator over the links between properties and column names within
     *         the inner class <code>PropertyLink</code>
     */
    public Iterator iterator() {
       return descriptionList.iterator();
    }


    /**
     *
     * returns the object as string
     *
     * @return String which contains all fields of the object
     *
     */
    public String toString( ) {

        StringBuffer str = new StringBuffer();

		str.append("format: ").append(format);
				
        if (descriptionList != null) {
        	Iterator iterator = descriptionList.iterator();
        	str.append("descriptions: ");
        	while (iterator.hasNext()) {
				String element = (String) iterator.next();
				str.append(element).append(" ");			
			}	      
        }
        
        return str.toString();
    }



}
