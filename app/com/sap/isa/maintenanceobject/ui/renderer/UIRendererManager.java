package com.sap.isa.maintenanceobject.ui.renderer;

import java.util.HashMap;
import java.util.Map;

import com.sap.isa.maintenanceobject.ui.UITypeRendererData;

public class UIRendererManager {
	
	/**
	 * Constant for standard UIRendererManagerName
	 */
	public static final String STANDARD_UI_RENDERER_MANAGER_NAME = "uiRendererManager";
	
	/**
	 * Constant for standard UI Type input.
	 */
	public static final String UI_TYPE_INPUT = "uitype_input";
	
	/**
	 * Constant for standard UI Type radio button.
	 */	
	public static final String UI_TYPE_RADIO_BUTTON = "uitype_radio";
	
	/**
	 * Constant for standard UI Type drop down list box.
	 */		
	public static final String UI_TYPE_DDLB = "uitype_ddlb";
	
	/**
	 * Constant for standard UI Type checkbox.
	 */		
	public static final String UI_TYPE_CHECKBOX = "uitype_checkbox";
	
	/**
	 * Constant for standard UI Type property group.
	 */		
	public static final String UI_TYPE_PROPERTY_GROUP = "uitype_propgroup";
	
	/**
	 * Constant for standard UI Type assignment block.
	 */		
	public static final String UI_TYPE_ASSIGNMENT_BLOCK = "uitype_assignblock";
	
	/**
	 * Constant for standard UI Type page.
	 */		
	public static final String UI_TYPE_PAGE = "uitype_page";
	
	/**
	 * Constant for standard UI Type list rendered as multiple drop down list box.
	 */
	public static final String UI_TYPE_DDLB_LIST = "uitype_ddlblist";

	/**
	 * Constant for standard UI Type list rendered as checkboxes.
	 */
	public static final String UI_TYPE_CHECKBOX_LIST = "uitype_checkboxlist";

	/**
	 * Constant for standard UI Type date.
	 */
	public static final String UI_TYPE_DATE = "uitype_date";

	/**
	 * Map to hide parts within JSP includes.
	 */
	protected Map invisibleParts = new HashMap();

	
	/**
	 * Map to store the UITypeRenderer
	 */
	protected Map renderer = new HashMap();
	
	
	/**
	 * Constructor
	 */
	public UIRendererManager() {
		invisibleParts.put(UITypeRendererData.THUMBNAIL, null);
		invisibleParts.put(UITypeRendererData.ADD_DOC_LINKS, null);
		invisibleParts.put(UITypeRendererData.RESET_VALUE, null);
		invisibleParts.put(UITypeRendererData.DISPLAY_ALL_OPTION, null);
		invisibleParts.put(UITypeRendererData.DISPLAY_HELP_LINK, null);
	}
	
	/**
	 * Returns the corresponding UITypeRenderer for the given uiType.
	 * 
	 * @param uiType
	 * @return UITypeRenderer
	 */
	public UITypeRendererData getUIRenderer(String uiType) {
		if (uiType.equals(UI_TYPE_INPUT)) {
			if (renderer.get(uiType) == null) {
				UITypeRendererInput rendererInput =
					new UITypeRendererInput();
				renderer.put(uiType, rendererInput);
			}
			return (UITypeRendererData) renderer.get(uiType);
		}
		else if (uiType.equals(UI_TYPE_RADIO_BUTTON)) {
			if (renderer.get(uiType) == null) {
				UITypeRendererRadioButton rendererRadio =
					new UITypeRendererRadioButton();
				renderer.put(uiType, rendererRadio);
			}
			return (UITypeRendererData) renderer.get(uiType);			
		}
		else if (uiType.equals(UI_TYPE_DDLB)) {
			if (renderer.get(uiType) == null) {
				UITypeRendererDDLB rendererDDLB =
					new UITypeRendererDDLB();
				renderer.put(uiType, rendererDDLB);
			}
			return (UITypeRendererData) renderer.get(uiType);			
		}
		else if (uiType.equals(UI_TYPE_CHECKBOX)) {
			if (renderer.get(uiType) == null) {
				UITypeRendererCheckbox rendererCheckbox =
					new UITypeRendererCheckbox();
				renderer.put(uiType, rendererCheckbox);
			}
			return (UITypeRendererData) renderer.get(uiType);			
		}
		else if (uiType.equals(UI_TYPE_PROPERTY_GROUP)) {
			if (renderer.get(uiType) == null) {
				UITypeRendererPropGroup rendererPropGroup =
					new UITypeRendererPropGroup();
				renderer.put(uiType, rendererPropGroup);
			}
			return (UITypeRendererData) renderer.get(uiType);			
		}
		else if (uiType.equals(UI_TYPE_ASSIGNMENT_BLOCK)) {
			if (renderer.get(uiType) == null) {
				UITypeRendererAssignBlock rendererAssignBlock =
					new UITypeRendererAssignBlock();
				renderer.put(uiType, rendererAssignBlock);
			}
			return (UITypeRendererData) renderer.get(uiType);			
		}
		else if (uiType.equals(UI_TYPE_PAGE)) {
			if (renderer.get(uiType) == null) {
				UITypeRendererPage rendererPage =
					new UITypeRendererPage();
				renderer.put(uiType, rendererPage);
			}
			return (UITypeRendererData) renderer.get(uiType);			
		}
		else if (uiType.equals(UI_TYPE_DDLB_LIST)) {
			if (renderer.get(uiType) == null) {
				UITypeRendererDDLBList rendererDDLBList =
					new UITypeRendererDDLBList();
				renderer.put(uiType, rendererDDLBList);
			}
			return (UITypeRendererData) renderer.get(uiType);
		}
		else if (uiType.equals(UI_TYPE_CHECKBOX_LIST)) {
			if (renderer.get(uiType) == null) {
				UITypeRendererCheckboxList rendererCheckboxList =
					new UITypeRendererCheckboxList();
				renderer.put(uiType, rendererCheckboxList);
			}
			return (UITypeRendererData) renderer.get(uiType);
		}
		else if (uiType.equals(UI_TYPE_DATE)) {
			if (renderer.get(uiType) == null) {
				UITypeRendererDate rendererDate =
					new UITypeRendererDate();
				renderer.put(uiType, rendererDate);
			}
			return (UITypeRendererData) renderer.get(uiType);
		}
		
		return null;
	}

	/**
	 * 
	 * Return the property {@link #invisibleParts }. <br>
	 * 
	 * @return invisibleParts
	 */
	public Map getInvisibleParts() {
		return invisibleParts;
	}

	/**
	 * Set the property {@link #invisibleParts }. <br> 
	 * 
	 * @param invisibleParts The invisibleParts to set.
	 */
	public void setInvisibleParts(Map invisibleParts) {
		this.invisibleParts = invisibleParts;
	}


}
