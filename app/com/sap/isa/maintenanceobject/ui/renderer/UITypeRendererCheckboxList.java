/*****************************************************************************
    Class:        UITypeRendererCheckboxList
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Author:       SAP
    Created:      March 2008
    Version:      1.0

    $Revision: #1 $
    $Date: 2008/03/27 $
*****************************************************************************/

package com.sap.isa.maintenanceobject.ui.renderer;

import com.sap.isa.maintenanceobject.ui.UITypeRendererData;
import com.sap.isa.maintenanceobject.ui.handler.UITypeRendererHandler;

/**
 * This class provides the JSP-include name for the uiType checkbox list
 * 
 */
public class UITypeRendererCheckboxList implements UITypeRendererData {

	protected static String JSP_INCLUDE_NAME = "uitype_checkboxlist";
	
	UITypeRendererCheckboxList() {
	}
		
	
	/**
	 * Implements interface method
	 *  
	 * @see com.sap.isa.maintenanceobject.ui.UITypeRendererData#getJSPIncludeName()
	 */
	public String getJSPIncludeName() {
		return UITypeRendererHandler.getJSPInclude(JSP_INCLUDE_NAME);
	}
		
}
