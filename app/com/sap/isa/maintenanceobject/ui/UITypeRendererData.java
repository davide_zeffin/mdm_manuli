/*****************************************************************************
    Class:        UITypeRendererInput
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Author:       SAP
    Created:      February 2008
    Version:      1.0

    $Revision: #1 $
    $Date: 2008/02/15 $
*****************************************************************************/

package com.sap.isa.maintenanceobject.ui;


/**
 * The UITypeRendererData interface provides the corresponding JSP-includes
 *
 */
public interface UITypeRendererData {
 
	
	/**
	 * Constant for UI part thumbnail.
	 */
	public static final String THUMBNAIL = "thumbnail";
	
	/**
	 * Constant for UI part additional document links.
	 */
	public static final String ADD_DOC_LINKS = "add_doc_links";
		
	/**
	 * Constant for UI part reset value link.
	 */
	public static final String RESET_VALUE = "resetValue";
	
	/**
	 * Constant for UI part display all option link.
	 */
	public static final String DISPLAY_ALL_OPTION = "displayAllOption";	
	
	/**
	 * Constant for UI part display help link.
	 */
	public static final String DISPLAY_HELP_LINK = "displayHelpLink";	

	/**
	 * Returns the JSP-include name
	 * 
	 * @return jsp-include name
	 */
	public String getJSPIncludeName();
	
}
