/*****************************************************************************
	Class:        UITypeRendererHandler
	Copyright (c) 2008, SAP AG, Germany, All rights reserved.
	Author:       SAP
	Created:      February 2008
	Version:      1.0

	$Revision: #1 $
	$Date: 2008/02/15 $
*****************************************************************************/

package com.sap.isa.maintenanceobject.ui.handler;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.digester.Digester;

import com.sap.isa.core.FrameworkConfigManager;
import com.sap.isa.core.init.Initializable;
import com.sap.isa.core.init.InitializationEnvironment;
import com.sap.isa.core.init.InitializeException;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.xcm.ConfigContainer;

/**
 * This class provides all JSP-includes which are maintained in 
 * dynamicJSPInclude-config.xml file.<br>
 * The file will be read during initialization process.
 * 
 */
public class UITypeRendererHandler implements Initializable {

	/**
	 * Reference to the IsaLocation. 
	 *
	 * @see com.sap.isa.core.logging.IsaLocation
	 */
	protected static IsaLocation log = IsaLocation.
		  getInstance(UITypeRendererHandler.class.getName());

	/**
	 * Map with the JSP-includes
	 */
	private static Map includes; 

	/**
	 * Returns the JSP-include for the given name
	 * 
	 * @param jspIncludeName
	 * @return JSP-include
	 */
	public static String getJSPInclude(String jspIncludeName) {
		String jspInclude = (String)includes.get(jspIncludeName);
		if (jspInclude == null) {
			log.error("jspinclude name for " + jspIncludeName + "not found in dynamicJSPInclude-config.xml!");
		}
		return jspInclude;
	}
	
	/* (non-Javadoc)
	 * @see com.sap.isa.core.init.Initializable#initialize(com.sap.isa.core.init.InitializationEnvironment, java.util.Properties)
	 */
	public void initialize(InitializationEnvironment env, Properties props)
		throws InitializeException {
			includes = new HashMap(20);   				
			ConfigContainer configContainer = FrameworkConfigManager.XCM.getXCMScenarioConfig(FrameworkConfigManager.NAME_BOOTSTRAP_SCENARIO);

			if (configContainer != null) {
			
				InputStream inputStream = 
					configContainer.getConfigUsingAliasAsStream("dynamicJSPInclude-config");
				if (inputStream != null) {	
					parseConfigFile(inputStream);
					try {
						inputStream.close();
					}
					catch (IOException e) {
						log.error(LogUtil.APPS_BUSINESS_LOGIC,"io.exception",e);
					}
				}
				else {
					log.debug("Entry [dynamicJSPInclude-config] in xcm-config.xml missing"); 
				}
			}
			else {
				log.debug("Scenario [FrameworkConfigManager.NAME_BOOTSTRAP_SCENARIO] missing"); 
			}
	}

	/**
	 * Terminate the Handler . <br>
	 * 
	 * @see com.sap.isa.core.init.Initializable#terminate()
	 */
	public void terminate() {
		includes.clear();
		includes=null;    	
	}

	private static String CRLF = System.getProperty("line.separator");

	/**
	 * Parse the dynamicJSPInclude-config.xml file
	 * 
	 * @param inputStream
	 * @throws InitializeException
	 */
	private void parseConfigFile(InputStream inputStream)
			throws InitializeException {

		// new Struts digester
		Digester digester = new Digester();

		digester.push(this);

		String rootTag = "dynamicJSPIncludes/jspinclude";
		
		digester.push(includes);
		digester.addCallMethod(rootTag, "put", 2, new Class[] {Object.class, Object.class});
		digester.addCallParam("dynamicJSPIncludes/jspinclude", 0, "name");
		digester.addCallParam("dynamicJSPIncludes/jspinclude", 1, "value");

		try {
			digester.parse(inputStream);
		}
		catch (java.lang.Exception ex) {
			String errMsg = "Error reading configuration information" + CRLF + ex.toString();
			throw new InitializeException(errMsg);
		}

	}
}
