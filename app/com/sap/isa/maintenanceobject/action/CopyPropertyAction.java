/*****************************************************************************
    Class         CopyPropertyAction
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Author:       SAP
    Created:      25.02.2008
    Version:      1.0

    $Revision: #1 $
    $Date: 2008/02/25 $
*****************************************************************************/
package com.sap.isa.maintenanceobject.action;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.Constants;
import com.sap.isa.core.ContextConst;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.maintenanceobject.backend.boi.PropertyData;
import com.sap.isa.maintenanceobject.backend.boi.UIElementGroupData;
import com.sap.isa.maintenanceobject.businessobject.DynamicUI;
import com.sap.isa.maintenanceobject.businessobject.DynamicUIMaintenance;
import com.sap.isa.maintenanceobject.businessobject.PropertyMaintenance;

/**
 * @author d033221
 *
 *Action is used to copy a property to another page
 */
public class CopyPropertyAction extends DynamicUIBaseAction {

	
	/* (non-Javadoc)
	 * @see com.sap.isa.isacore.action.EComBaseAction#ecomPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.sap.isa.core.UserSessionData, com.sap.isa.core.util.RequestParser, com.sap.isa.core.businessobject.management.MetaBusinessObjectManager, boolean, boolean)
	 */
	public ActionForward ecomPerform(ActionMapping mapping, 
            HttpServletRequest request, 
            HttpServletResponse response,
            UserSessionData userSessionData, 
            RequestParser requestParser,
            DynamicUIMaintenance dynamicUIMaintenance, 
            DynamicUI dynamicUI,
            MetaBusinessObjectManager mbom, 
            boolean multipleInvocation,			                         
            boolean browserBack) 
		throws IOException, ServletException,CommunicationException { 
		
		if (requestParser.getParameter(ActionConstants.RC_SELECTED_PAGE).isSet()) {

	
			try {
				DynamicUIActionHelper.parseRequest(requestParser, dynamicUI);
			}
			catch (CommunicationException e) {
				// Catch communication exceptions and log them
				// log error
				log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "exception.communication", e);
				// store exception in context for the error pages
				request.setAttribute(ContextConst.EXCEPTION, e);
				return mapping.findForward(Constants.FORWARD_BACKEND_ERROR);
			}
			
			TechKey pageKey	= new TechKey(requestParser.getParameter(ActionConstants.RC_SELECTED_PAGE).getValue().getString());
			UIElementGroupData page = (UIElementGroupData)dynamicUI.getPageGroup().getElement(pageKey);
			
			RequestParser.Parameter checkboxes = requestParser.getParameter(ActionConstants.RC_CHECKBOX);
			RequestParser.Value[] keys = checkboxes.getValues();
			for (int i = 0; i < keys.length; i++) {
				dynamicUI.copyElement(new TechKey(keys[i].getString()),page);
			}
			
		}
		else {
			//TODO add error message!			
			return mapping.findForward("message");
		}
		
		return mapping.findForward("success");
	}
}
