/*****************************************************************************
    Class         EditPropertyAction
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Author:       SAP
    Created:      25.02.2008
    Version:      1.0

    $Revision: #1 $
    $Date: 2008/02/25 $
*****************************************************************************/
package com.sap.isa.maintenanceobject.action;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.maintenanceobject.backend.boi.DynamicUIMaintenanceExitData;
import com.sap.isa.maintenanceobject.businessobject.DynamicUI;
import com.sap.isa.maintenanceobject.businessobject.DynamicUIMaintenance;
import com.sap.isa.maintenanceobject.businessobject.PageMaintenance;

/**
 * @author d033221
 *
 */
public class ShowPageAction extends DynamicUIBaseAction {

	public ActionForward ecomPerform(ActionMapping mapping, 
            HttpServletRequest request, 
            HttpServletResponse response,
            UserSessionData userSessionData, 
            RequestParser requestParser,
            DynamicUIMaintenance dynamicUIMaintenance,
            DynamicUI dynamicUI,
            MetaBusinessObjectManager mbom, 
            boolean multipleInvocation,			                         
            boolean browserBack) 
		throws IOException, ServletException,CommunicationException { 
	
		PageMaintenance pageMaintenance = dynamicUIMaintenance.getPageMaintenance();
		request.setAttribute(ActionConstants.RC_PAGE, pageMaintenance);
		if (dynamicUIMaintenance.getDescriptionList()!= null) {
			request.setAttribute(ActionConstants.RC_DESCRIPTION_LIST, dynamicUIMaintenance.getDescriptionList());

			DynamicUIMaintenanceExitData exitHandler = dynamicUI.getMaintenanceExitData();
			if (exitHandler!= null) {
				Map languageMap = exitHandler.getLanguageMap(userSessionData,mbom);
				request.setAttribute(ActionConstants.RC_LANGUAGE_MAP, languageMap);
			}
		}	
		
		return mapping.findForward("success");
		
	}
}
