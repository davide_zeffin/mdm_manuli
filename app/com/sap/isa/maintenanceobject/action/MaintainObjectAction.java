/*****************************************************************************
    Class         MaintainObjectAction
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Action to maintain a generic object
    Author:       SAP
    Created:      February 2002
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/04 $
*****************************************************************************/
package com.sap.isa.maintenanceobject.action;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.GenericBusinessObjectManager;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.helpvalues.HelpValues;
import com.sap.isa.helpvalues.HelpValuesConstants;
import com.sap.isa.helpvalues.HelpValuesSearchResults;
import com.sap.isa.isacore.action.IsaAppBaseAction;
import com.sap.isa.maintenanceobject.businessobject.MaintenanceObject;
import com.sap.isa.maintenanceobject.businessobject.Property;
import com.sap.isa.maintenanceobject.businessobject.PropertyGroup;
import com.sap.isa.maintenanceobject.businessobject.ScreenGroup;
import com.sap.isa.maintenanceobject.util.HelpDescriptions;

/**
 * Some usefull methods to maintain an generic object.
 *
 * The class contains a set static method to maintain your object.
 *
 * @author SAP
 * @version 1.0
 *
 */
public class MaintainObjectAction {
	static private final IsaLocation log = IsaLocation.
			  getInstance(MaintainObjectAction.class.getName());

    /**
     * Call this method to parse the request in your buisness object. <br>
     * After the object is build, the backend will be synchronized.
     *
     * @param request           The request object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param object            Reference to the BusinessObjectManager
     * @param action            Reference to the action (needed for user exit)
     */
    public static void parseRequest( HttpServletRequest request,
                                     UserSessionData userSessionData,
                                     RequestParser requestParser,
                                     GenericBusinessObjectManager bom,
                                     MaintenanceObject object,
                                     IsaAppBaseAction action)
            throws CommunicationException {
		final String METHOD = "parseRequest()";
		log.entering(METHOD);
        if (object.isNewObject()) {
            object.setId(requestParser.getParameter("objectId").getValue().getString().trim());
        }

        boolean readOnly = false;
        if (requestParser.getParameter("readOnly").isSet()) {
            readOnly = requestParser.getParameter("readOnly").getValue().getString().equalsIgnoreCase("true");
        }

        if (!readOnly) {
        	object.setDescription(requestParser.getParameter("objectDescription").getValue().getString());
        }
        
        object.setReadOnly(readOnly);
        
		// get the active screen group which is currently used on the JSP	
		if (requestParser.getParameter("screenGroup").isSet()) {
			object.setActiveScreenGroup(requestParser.getParameter("screenGroup").getValue().getString());
		}       
	
		DynamicUIActionHelper.parseRequest(requestParser, object);

		if (requestParser.getParameter("activeScreenGroup").isSet()) {
			String activeScreenGroup = requestParser.getParameter("activeScreenGroup").getValue().getString();
			if (activeScreenGroup.length() > 0) {
				object.setActiveScreenGroup(activeScreenGroup);
			}
		}
		
        action.parseExit(request,userSessionData,requestParser, bom , object);

        /* Synchronize Backend bean with changed data */
        object.synchronizeBeanProperties();
        log.exiting();
    }


    private static void parseProperty(RequestParser requestParser, MaintenanceObject object, Property property) {

        if (!property.isHidden() && !property.isDisabled()) {
            RequestParser.Parameter parameter = requestParser.getParameter(property.getName());

            if (property.getType().equals(Property.TYPE_STRING)) {
                property.setValue(parameter.getValue().getString());
            }
            else if (property.getType().equals(Property.TYPE_BOOLEAN)) {
                property.setValue(new Boolean(parameter.isSet()));
            }
            else if (property.getType().equals(Property.TYPE_INTEGER)) {
            	
            	if (!parameter.getValue().isInt()) {
					object.addMessage(new Message(Message.ERROR,
								"mo.error.noInt",
								null,
								property.getName()));
            	}
            	else {				
            		property.setValue(new Integer(parameter.getValue().getInt()));
            	}	
            }
        }
    }


    /**
     * Call this method to check all required fields
     *
     * @param object            Reference to the BusinessObjectManager
     */
    public static void checkRequiredProperties(MaintenanceObject object, String errorKey ) {
		final String METHOD = "checkRequiredProperties()";
		log.entering(METHOD);
        if (object.isNewObject() && object.getId().length() == 0) {
            object.addMessage(new Message(Message.ERROR,"mob.error.noObjectId"));
        }

        Iterator groupIterator = object.iterator();

        while (groupIterator.hasNext()) {

			checkRequiredPropertiesForPropertyGroup(object,
			                                        (PropertyGroup) groupIterator.next(),
			                                        errorKey); 
        }
        log.exiting();
	    
    }


    /**
     * Call this method to check all required fields for one screen group
     *
     * @param object        Reference to the BusinessObjectManager
     * @param screenGroup   Name of the screen group
     * 
     * @return <code>true</code>, if all required properties are filled 
     */
    public static boolean checkRequiredPropertiesForScreenGroup(MaintenanceObject object, 
    														    ScreenGroup screenGroup,	
    														    String errorKey ) {
		final String METHOD = "checkRequiredPropertiesForScreenGroup()";
		log.entering(METHOD);
		boolean ret = true;
		
        Iterator groupIterator = screenGroup.iterator();

        while (groupIterator.hasNext()) {

            PropertyGroup propertyGroup = (PropertyGroup) groupIterator.next();
			
			if (!checkRequiredPropertiesForPropertyGroup(object,
			                                            propertyGroup,
			                                            errorKey)) {
				ret = false;                            	
            }
        } 
        log.exiting();       
        return ret;
    }


    /**
     * Call this method to check all required fields for one screen group
     *
     * @param object        Reference to the BusinessObjectManager
     * @param screenGroup   Name of the screen group
     * 
     * @return <code>true</code>, if all required properties are filled 
     */
    public static boolean checkRequiredPropertiesForPropertyGroup(MaintenanceObject object, 
    														      PropertyGroup propertyGroup,	
    														      String errorKey ) {
		final String METHOD = "checkRequiredPropertiesForPropertyGroup()";
		log.entering(METHOD);
		boolean ret = true;
		
        Iterator properyIterator = propertyGroup.iterator();

        while (properyIterator.hasNext()) {
            Property property = (Property)properyIterator.next();

            if (property.isRequired() && !property.isHidden() && !property.isDisabled()) {
                if ((property.getType().equals(Property.TYPE_STRING)
                	|| property.getType().equals(Property.TYPE_INTEGER)) &&
                        property.getString().length() == 0 ) {
					ret = false;                            	
                    object.addMessage(new Message(Message.ERROR,
                                    errorKey,
                                    null,
                                    property.getName()));
                }
            }
        }
        log.exiting();
        return ret;
    }


    /**
     * Call this method to check, if the help values for a property should
     * be displayed. The parameter "helpButton[]" is expected.
     * The help Values are stored under ACTION_CONSTANTS.RC_HELP_VALUES
     * in the context.
     *
     * @param requestParser     Parser to simple retrieve data from the request
     * @param object            Reference to the BusinessObjectManager
     */
    public static boolean checkHelpValues(RequestParser requestParser,
                                          MaintenanceObject object)
                    throws CommunicationException {

		final String METHOD = "checkHelpValues()";
		log.entering(METHOD);
        boolean found = false;

        // check if a search button is pressed
        RequestParser.Parameter helpButtonParameter = requestParser.getParameter("helpButton[]");

        if (helpButtonParameter.isSet()) {
             // get the property which is pressed with the property name
            Iterator indices = helpButtonParameter.getValueIndices();
            if (indices.hasNext()) {
                String propertyName = (String)indices.next();
                
            	found = showHelpValues(requestParser.getRequest(),object, propertyName);
            }
        }
		log.exiting();
        return found;
    }


    /**
     * Call this method to check, if the help values for a property should
     * be displayed. The parameter "helpButton[]" is expected.
     * The help Values are stored under ACTION_CONSTANTS.RC_HELP_VALUES
     * in the context.
     *
     * @param requestParser     Parser to simple retrieve data from the request
     * @param object            Reference to the BusinessObjectManager
     */
    public static boolean showHelpValues(HttpServletRequest request,
                                           MaintenanceObject object,
                                           String propertyName)
                    throws CommunicationException {

		final String METHOD = "showHelpValues()";
		log.entering(METHOD);
        boolean found = false;

        Property property = (Property)object.getProperty(propertyName);

        HelpValues helpValues = object.getHelpValues(property);
        if (helpValues!=null) {
            request.setAttribute(ActionConstants.RC_HELP_VALUES, helpValues);
            found = true;
        }
		log.exiting();
        return found;

    }


    /**
     * Call this method to display the help for the property given
     * within request parameter "property"
     * The help description are stored under ACTION_CONSTANTS.RC_HELP_DESCRIPTIONS
     * in the context.
     *
     * @param requestParser     Parser to simple retrieve data from the request
     * @param object            Reference to the BusinessObjectManager
     */
    public static void showHelpDescriptions(RequestParser requestParser,
                                            MaintenanceObject object)
                    throws CommunicationException {

		final String METHOD = "showHelpDescriptions()";
		log.entering(METHOD);
		HelpDescriptions helpDescriptions = null;

        RequestParser.Parameter propertyParameter = requestParser.getParameter("property");

        if (propertyParameter.isSet()) {

	        Property property = (Property)object.getProperty(propertyParameter.getValue().getString());
	
			if (property != null) {
	 	       helpDescriptions = object.getHelpDescription(property);
			}   
 
        }
        requestParser.getRequest().setAttribute(ActionConstants.RC_HELP_DESCRIPTIONS, helpDescriptions);
    	log.exiting();
    }



    /**
     * create the Request Parameter for action which selects one help value
     *
     * @param helpValues
     * @param resultData
     *
     * @return the request parameter
     *
     */
    public static String createParameter(HelpValues helpValues, ResultData resultData) {
		final String METHOD = "createParameter()";
		log.entering(METHOD);
        StringBuffer ret = new StringBuffer();

        Iterator iter = helpValues.iterator();
        int  counter = 1;

        while (iter.hasNext()) {

            if (ret.length() > 0) {
                ret.append('&');
            }
            else {
                ret.append('?');
            }

            HelpValues.PropertyLink propertyLink = (HelpValues.PropertyLink)iter.next();
            ret.append("property[").append(counter).append("]=").
            append(propertyLink.getPropertyName());

            String value = resultData.getString(propertyLink.getColumnName());
            ret.append('&').append("value[").append(counter).append("]=").
            append(value);

            counter++;
        }
		log.exiting();
        return ret.toString();
    }


    /**
     * Call this method to select one help value from the help value page.
     *
     * @param requestParser     Parser to simple retrieve data from the request
     * @param object            Reference to the BusinessObjectManager
     */
    public static void selectHelpValue(RequestParser requestParser,
                                       MaintenanceObject object)
                    throws CommunicationException {
		final String METHOD = "selecthelpValue()";
		log.entering(METHOD);
        // check if a search button is pressed
        RequestParser.Parameter propertyParameter = requestParser.getParameter("property[]");
        RequestParser.Parameter valueParameter    = requestParser.getParameter("value[]");

        for (int i = 0; i < propertyParameter.getNumValues(); i++ ) {

            String propertyName = propertyParameter.getValue(i+1).getString();
            String value = valueParameter.getValue(i+1).getString();

            Property property = (Property)object.getProperty(propertyName);
            if (property != null) {
                property.setValue(value);
            }
        }
        log.exiting();
    }


	/**
	 * Call this method to get help values from the HelpValuesSearchResults object. <br>
	 *
	 * @param userSessionData   user session data
	 * @param object            Reference to the BusinessObjectManager
	 */
	public static void getHelpValueResults(UserSessionData userSessionData, 
	                                         MaintenanceObject object) {
		final String METHOD = "getHelpValueResults()";
		log.entering(METHOD);
		
		HelpValuesSearchResults helpValuesSearchResults = (HelpValuesSearchResults)
						userSessionData.getAttribute(HelpValuesConstants.SC_RESULTS);
        
        if (helpValuesSearchResults != null) {
			Iterator iter = helpValuesSearchResults.iterator();
        
			while (iter.hasNext()) {
				HelpValuesSearchResults.HelpValuesSearchResult element = 
				(HelpValuesSearchResults.HelpValuesSearchResult) iter.next();
        	
				Property property = (Property)object.getProperty(element.getParameter());
				if (property != null) {
					property.setValue(element.getValue());
					helpValuesSearchResults.deleteSearchResult(element.getParameter());
				}			
			}
        }
        
		log.exiting();
	}


    /**
     * Call this method to set the activeScreenGroup, if errors occurs.
     *
     * @param object            Reference to the BusinessObjectManager
     */
    public static void adjustActiveScreenGroup(MaintenanceObject object) {
		final String METHOD = "adjustActiveScreenGroup()";
		log.entering(METHOD);
		Map  messageMap = new HashMap();    	

    	String activeScreenGroup = object.getActiveScreenGroup();
		
		if (activeScreenGroup.length()>0) {
			
			// Determine all messages assigned to a property
			Iterator iter = object.getMessageList().iterator();
			
			while (iter.hasNext()) {
				Message message = (Message) iter.next();
				if (message.getProperty().length()>0) {
					messageMap.put(message.getProperty(),message);		
				}	
			}

			if (messageMap.size() == 0) {
				// nothing to do
				return; 
			}	

			List list = object.getScreenGroupList();
	
			boolean errorOnActiveScreen = false;
			String  screenGroupWithErrors = "";
					
			for (int i = 0; i < list.size(); i++) {
				
				ScreenGroup screenGroup = (ScreenGroup)list.get(i);
				iter = screenGroup.iterator();
				
				while (iter.hasNext()) {
					Iterator propertyIter = ((PropertyGroup)iter.next()).iterator();
					
					while (propertyIter.hasNext()) {
						Property property = (Property) propertyIter.next();
						
						if (messageMap.get(property.getName()) != null) {

							screenGroupWithErrors = screenGroup.getName();	
							
							if (screenGroupWithErrors.equals(activeScreenGroup)) {
								errorOnActiveScreen = true;
							}	
							break;													
						}	
						
					} // while property

					if (errorOnActiveScreen || screenGroupWithErrors.length() > 0){
						break;
					}	 	
					
				} // while propertyGroup


				if (errorOnActiveScreen){
					break;
				}	 	
			}  // while screenGroup	
			
						
			if (!errorOnActiveScreen && screenGroupWithErrors.length()>0){
				object.setActiveScreenGroup(screenGroupWithErrors);
			}	
			
		}

        log.exiting();                               	
    }                                       	

}
