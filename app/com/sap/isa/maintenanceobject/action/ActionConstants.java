/*****************************************************************************
    Class:        ActionConstants
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      February 2002
    Version:      1.0

    $Revision: #2 $
    $Date: 2001/07/03 $
*****************************************************************************/

package com.sap.isa.maintenanceobject.action;

/**
 * this class defines some constants for request attributes (RA) which are used
 * in actions to store and retrieve data from request context.
 */
public interface ActionConstants {
    
  /**
   * request parameter to cancel maintenance.
   */
  public final static String RC_CANCEL                = "fw_du_cancel";
  
  /**
   * request parameter to confirm maintenance.
   */
  public final static String RC_CONFIRM               = "fw_du_confirm";
  
  /**
   * request parameter to save maintenance.
   */
  public final static String RC_SAVE                  = "fw_du_save";
  
  /**
   * request parameter to confirm and save maintenance.
   */
  public final static String RC_CONFIRM_SAVE                  = "fw_du_confirm_save"; 

  /**
   * key to store dynamic UI object in request context
   */
  public final static String RC_DYNAMIC_UI            = "fw_du_object";
	
  /**
   * key to store current page in request context
   */
  public final static String RC_CURRENT_PAGE          = "fw_du_page";

  /**
   * key to store current page in request context
   */
  public final static String RC_CHECKBOX	          = "fw_cb";
  
  /**
   * key to store selected page in request context
   */
  public final static String RC_SELECTED_PAGE         = "fw_du_selected_page";
  
  /**
   * key to edit a property
   */
  public final static String RC_EDIT_PROPERTY         = "fw_du_property_key";

  /**
   * data of a property object. <br>
   */
  public final static String RC_PROPERTY              = "fw_du_property";

  /**
   * data of a property object. <br>
   */
  public final static String RC_PAGE                  = "fw_du_page_object";
  
  /**
   * data of a property object. <br>
   */
  public final static String RC_ALLOWED_VALUES        = "fw_du_values";
  
  /**
   * data of a property object. <br>
   */
  public final static String RC_CURRENT_ALLOWED_VALUE = "fw_du_allowed_value";
  
  /**
   * key to store item key in request context
   */
  public final static String RC_HELP_VALUES          = "helpvalues";

  /**
   * key to store help descriptions in request context
   */
  public final static String RC_HELP_DESCRIPTIONS    = "helpDescriptions";

  /**
   * key to store the group which should be collapsed
   */
  public final static String RC_COLLAPSE_GROUP       = "fw_du_collapseElement";

  /**
   * key to store the group which should be expanded
   */
  public final static String RC_EXPAND_GROUP         = "fw_du_expandElement";
  
  /**
   * key to store the parent group within a group should be expanded or collapsed.
   */
  public final static String RC_PARENT_GROUP         = "fw_du_parentElement";
  
  /**
   * key to store current page in request context
   */
  public final static String RC_NAVIGATE_TO_PAGE          = "fw_du_navigate_to_page";
  
  /**
   * key to store the element which is currently in focus. <br>
   */
  public final static String RC_FOCUS_CURRENT_ELEMENT = "fw_du_focusCurrentElement";
  
  /**
   * key to store if wizard mode is enabled. <br>
   */
  public final static String RC_WIZARD_MODE = "fw_du_wizardMode";
  
  /**
   * key to store description list in request. <br>
   */
  public final static String RC_DESCRIPTION_LIST = "fw_du_descriptionList";
  
  /**
   * key to store adding new description line. <br>
   */
  public final static String RC_ADD_DESCRIPTION_LINE = "fw_du_addDescriptionLine";

  /**
   * key to store deleting description. <br>
   */
  public final static String RC_DELETE_DESCRIPTION = "fw_du_deleteDescription";

  /**
   * request parameter for description language. <br>
   */
  public final static String RC_DESCRIPTION_LIST_LANGU = "fw_du_dl_la";

  /**
   * request parameter for description text. <br>
   */
  public final static String RC_DESCRIPTION_LIST_TEXT = "fw_du_dl_txt";

  /**
   * Map with available languages. <br>
   */
  public final static String RC_LANGUAGE_MAP = "fw_du_lang_map";
  
  public final static String RC_MESSAGES = "messagelist";

}