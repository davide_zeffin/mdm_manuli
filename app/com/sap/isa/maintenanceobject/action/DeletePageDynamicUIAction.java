/*****************************************************************************
    Class         AddPageDynamicUIAction
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Author:       SAP
    Created:      25.02.2008
    Version:      1.0

    $Revision: #1 $
    $Date: 2008/02/25 $
*****************************************************************************/
package com.sap.isa.maintenanceobject.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.RequestProcessor;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.MessageListDisplayer;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.maintenanceobject.backend.boi.DynamicUIMaintenanceExitData;
import com.sap.isa.maintenanceobject.backend.boi.UIElementBaseData;
import com.sap.isa.maintenanceobject.backend.boi.UIElementGroupData;
import com.sap.isa.maintenanceobject.businessobject.DynamicUI;
import com.sap.isa.maintenanceobject.businessobject.DynamicUIMaintenance;

/**
 * @author d033221
 *
 * Action is used for adding a new page into UI model
 */
public class DeletePageDynamicUIAction extends DynamicUIBaseAction {

	
	/* (non-Javadoc)
	 * @see com.sap.isa.isacore.action.EComBaseAction#ecomPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.sap.isa.core.UserSessionData, com.sap.isa.core.util.RequestParser, com.sap.isa.core.businessobject.management.MetaBusinessObjectManager, boolean, boolean)
	 */
	public ActionForward ecomPerform(ActionMapping mapping,
			HttpServletRequest request, HttpServletResponse response,
			UserSessionData userSessionData, RequestParser requestParser,
			DynamicUIMaintenance dynamicUIMaintenance, DynamicUI dynamicUI,
			MetaBusinessObjectManager mbom, boolean multipleInvocation,
			boolean browserBack) throws IOException, ServletException,
			CommunicationException {

		if (requestParser.getParameter(ActionConstants.RC_SELECTED_PAGE).isSet()) {

			TechKey key = new TechKey(requestParser.getParameter(ActionConstants.RC_SELECTED_PAGE).getValue().getString());
        	UIElementBaseData page = (UIElementBaseData)dynamicUI.getElementGroup(key);

			boolean deleteObject = true;
			DynamicUIMaintenanceExitData exitHandler = dynamicUI.getMaintenanceExitData();
	        if (exitHandler != null) {
	        	deleteObject = exitHandler.isDeletionAllowed(dynamicUI, page, null);
	        }	
        	if (deleteObject){
        		dynamicUI.deletePage((UIElementGroupData)page);
        	}
        	else {
        		MessageListDisplayer messages = getMessageListDisplayer(request);
        		messages.addMessage(new Message(Message.ERROR,"fw.action.objectNotDeleted"));
        	}

		}
		else {
			// TODO add error message!
			return mapping.findForward("message");
		}

		return mapping.findForward("success");
	}
}
