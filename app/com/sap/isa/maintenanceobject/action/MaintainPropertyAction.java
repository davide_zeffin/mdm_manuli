/*****************************************************************************
    Class         EditPropertyAction
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Author:       SAP
    Created:      25.02.2008
    Version:      1.0

    $Revision: #1 $
    $Date: 2008/02/25 $
*****************************************************************************/
package com.sap.isa.maintenanceobject.action;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.maintenanceobject.backend.boi.DynamicUIMaintenanceExitData;
import com.sap.isa.maintenanceobject.backend.boi.PropertyData;
import com.sap.isa.maintenanceobject.businessobject.DynamicUI;
import com.sap.isa.maintenanceobject.businessobject.DynamicUIMaintenance;
import com.sap.isa.maintenanceobject.businessobject.PropertyMaintenance;

/**
 * @author d033221
 *
 */
public class MaintainPropertyAction extends DynamicUIBaseAction {

	public ActionForward ecomPerform(ActionMapping mapping, 
            HttpServletRequest request, 
            HttpServletResponse response,
            UserSessionData userSessionData, 
            RequestParser requestParser,
            DynamicUIMaintenance dynamicUIMaintenance,
            DynamicUI dynamicUI,
            MetaBusinessObjectManager mbom, 
            boolean multipleInvocation,			                         
            boolean browserBack) 
		throws IOException, ServletException,CommunicationException { 
	
		PropertyMaintenance propertyMaintenance = dynamicUIMaintenance.getPropertyMaintenance();

		PropertyData property = dynamicUI.getProperty(dynamicUIMaintenance.getCurrentPropertyKey());
	    DynamicUIMaintenanceExitData exitHandler = dynamicUI.getMaintenanceExitData();
		
        // first dispatch
        if (requestParser.getParameter(ActionConstants.RC_CANCEL).isSet() 
				&& requestParser.getParameter(ActionConstants.RC_CANCEL).getValue().getString().length() > 0) {

        	dynamicUIMaintenance.setAllowedValues(null);
        	return mapping.findForward("canceled");
        }
        
        if (exitHandler != null) {
        	exitHandler.parseAllowedValues(dynamicUIMaintenance.getAllowedValues(),requestParser);
        }	

        DynamicUIActionHelper.parseRequest(requestParser,propertyMaintenance);

        // first dispatch
        if ((requestParser.getParameter(ActionConstants.RC_CONFIRM).isSet() 
				&& requestParser.getParameter(ActionConstants.RC_CONFIRM).getValue().getString().length() > 0) ||
			    (requestParser.getParameter(ActionConstants.RC_CONFIRM_SAVE).isSet() 
			        && requestParser.getParameter(ActionConstants.RC_CONFIRM_SAVE).getValue().getString().length() > 0)) {

            if (exitHandler != null) {
            	exitHandler.maintainProperty(property, propertyMaintenance);
            }

        	propertyMaintenance.synchronizeBeanProperties(property);
        	if (dynamicUIMaintenance.getAllowedValues()!= null) {
        		property.setAllowedValues(dynamicUIMaintenance.getAllowedValues());
        	}

        	dynamicUIMaintenance.setAllowedValues(null);

			if (requestParser.getParameter(ActionConstants.RC_CONFIRM_SAVE).isSet() 
					&& requestParser.getParameter(ActionConstants.RC_CONFIRM_SAVE).getValue().getString().length() > 0) {
						
			    return mapping.findForward("confirmedandsave");		
			} 
			       	
        	return mapping.findForward("confirmed");
        }	
        		
		return mapping.findForward("success");
		
	}
}
