/*****************************************************************************
    Class         DynamicUIBaseAction
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Author:       SAP
    Created:      25.02.2008
    Version:      1.0

    $Revision: #1 $
    $Date: 2008/02/25 $
*****************************************************************************/
package com.sap.isa.maintenanceobject.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.EComBaseAction;
import com.sap.isa.maintenanceobject.businessobject.DynamicUI;
import com.sap.isa.maintenanceobject.businessobject.DynamicUIMaintenance;
import com.sap.isa.maintenanceobject.businessobject.DynamicUIMaintenanceAware;

/**
 * This class does not do a forward for an action. It is a base class for the dynamicUI actions which retrieves the dynamicUI obejcts and its BOM.
 * @author d033221
 *
 */
public abstract class DynamicUIBaseAction extends EComBaseAction {

	/* (non-Javadoc)
	 * @see com.sap.isa.isacore.action.EComBaseAction#ecomPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.sap.isa.core.UserSessionData, com.sap.isa.core.util.RequestParser, com.sap.isa.core.businessobject.management.MetaBusinessObjectManager, boolean, boolean)
     * Provides reference to DynamicUIMaintenance object implemented in one of the business object managers.<br>
     * Note that the used business object manager must implement the 
     * {@link DynamicUIMaintenanceAware} interface.
	 */
	public ActionForward ecomPerform(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response,
			UserSessionData userSessionData, RequestParser requestParser,
			MetaBusinessObjectManager mbom, boolean multipleInvocation,
			boolean browserBack) throws IOException, ServletException,
			CommunicationException {

    	// get BOM from MBOM
    	DynamicUIMaintenanceAware dynamicUIMaintenanceAware = (DynamicUIMaintenanceAware)mbom.getBOMByType(DynamicUIMaintenanceAware.class);
        DynamicUIMaintenance dynamicUIMaintenance = dynamicUIMaintenanceAware.getDynamicUIMaintenance();
        if(dynamicUIMaintenance == null) {
        	dynamicUIMaintenance = dynamicUIMaintenanceAware.createDynamicUIMaintenance();   
        }
        DynamicUI dynamicUI = dynamicUIMaintenance.getDynamicUI();
        
		return ecomPerform(mapping, 
                           request, 
                           response,
                           userSessionData, 
                           requestParser,
                           dynamicUIMaintenance,
                           dynamicUI,
                           mbom, 
                           multipleInvocation,			                         
                           browserBack); 
	
	}

	
	/* (non-Javadoc)
	 * @see com.sap.isa.isacore.action.EComBaseAction#ecomPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.sap.isa.core.UserSessionData, com.sap.isa.core.util.RequestParser, com.sap.isa.core.businessobject.management.MetaBusinessObjectManager, boolean, boolean)
     * Provides reference to DynamicUIMaintenance object implemented in one of the business object managers.<br>
     * Note that the used business object manager must implement the 
     * {@link DynamicUIMaintenanceAware} interface.
	 */
	abstract public ActionForward ecomPerform(ActionMapping mapping, 
			                         HttpServletRequest request, 
			                         HttpServletResponse response,
			                         UserSessionData userSessionData, 
			                         RequestParser requestParser,
			                         DynamicUIMaintenance dynamicUIMaintenance, 
			                         DynamicUI dynamicUI,
			                         MetaBusinessObjectManager mbom, 
			                         boolean multipleInvocation,			                         
			                         boolean browserBack) 
							throws IOException, ServletException,
			CommunicationException; 
	

}
