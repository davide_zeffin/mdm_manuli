/*****************************************************************************
    Class:        DescriptionMaintainBaseAction
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      18.04.2008
    Version:      1.0
*****************************************************************************/

package com.sap.isa.maintenanceobject.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.EditObjectHandler;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.EComBaseAction;
import com.sap.isa.maintenanceobject.backend.boi.DescriptionListData;

abstract public class EditDescriptionListBaseAction extends EComBaseAction {

	
	//TODO DOCU
	/**
	 * <p> The method getDescriptionList </p>
	 * 
	 * @return
	 */
	abstract protected DescriptionListData getDescriptionList (UserSessionData userSessionData, 
															   RequestParser requestParser,
															   EditObjectHandler editHandler,
															   MetaBusinessObjectManager mbom);
	
	public ActionForward ecomPerform(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response,
			UserSessionData userSessionData, RequestParser requestParser,
			MetaBusinessObjectManager mbom, boolean multipleInvocation,
			boolean browserBack) throws IOException, ServletException,
			CommunicationException {
		
		EditObjectHandler editHandler =  new EditObjectHandler();
		
		DescriptionListData descriptionList = getDescriptionList(userSessionData, requestParser, editHandler, mbom);
		if (descriptionList== null) {
			return mapping.findForward("error");
		}
		
		
		// work with copy too enable the cancel process
		editHandler.setEditObject(descriptionList.clone());
		
		userSessionData.setEditObjectHandler(editHandler);
		
		return mapping.findForward("success");
	}

}
