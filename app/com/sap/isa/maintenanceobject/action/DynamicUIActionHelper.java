
/*****************************************************************************
    Class         DynamicUIActionHelper
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Action to maintain a generic object
    Author:       SAP
    Created:      February 2002
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/04 $
*****************************************************************************/
package com.sap.isa.maintenanceobject.action;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.maintenanceobject.backend.boi.DynamicUIExitData;
import com.sap.isa.maintenanceobject.backend.boi.DynamicUIMaintenanceExitData;
import com.sap.isa.maintenanceobject.backend.boi.UIElementGroupData;
import com.sap.isa.maintenanceobject.businessobject.DynamicUI;
import com.sap.isa.maintenanceobject.businessobject.Property;
import com.sap.isa.maintenanceobject.businessobject.UIPage;

// TODO clarify unique key for property

/**
 * <p>The class contains a set of static methods to handle a {@link com.sap.isa.maintenanceobject.businessobject.DynamicUI} object.</p>
 * 
 * @author SAP
 * @version 1.0
 */
public class DynamicUIActionHelper {
	static private final IsaLocation log = IsaLocation.
			  getInstance(DynamicUIActionHelper.class.getName());

    /**
     * <p>Call this method to parse the request to update the data in the 
     * {@link DynamicUI}object.</p>
     *
     * @param requestParser Parser to simple retrieve data from the request
     * @param dynamicUI     Reference to the dynamic UI object
     */
    public static void parseRequest( RequestParser requestParser,
                                     DynamicUI dynamicUI)
            throws CommunicationException {
		final String METHOD = "parseRequest()";
		log.entering(METHOD);
        Iterator properyIterator = null;

        if (requestParser.getParameter(ActionConstants.RC_CURRENT_PAGE).isSet()) {
			dynamicUI.setActivePageName(requestParser.getParameter(ActionConstants.RC_CURRENT_PAGE).getValue().getString());
		}       
        
        if (!dynamicUI.isReadOnly()) {
			String pageName = dynamicUI.getActivePageName();
			if (pageName.length() > 0) {
				properyIterator = dynamicUI.getActivePage().propertyIterator();		
			} 
			else {	 
				properyIterator = dynamicUI.propertyIterator();        
			}	
	
	        while (properyIterator.hasNext()) {
	        	parseProperty(requestParser, dynamicUI, (Property)properyIterator.next());
	        }
        }     

        // expand AB or group
		if (requestParser.getParameter(ActionConstants.RC_EXPAND_GROUP).isSet() && 
				(requestParser.getParameter(ActionConstants.RC_EXPAND_GROUP).getValue().getString() != null &&
						requestParser.getParameter(ActionConstants.RC_EXPAND_GROUP).getValue().getString().length() > 0)) {
			UIElementGroupData uiElementGroup = 
				dynamicUI.getElementGroup(new TechKey(requestParser.getParameter(ActionConstants.RC_PARENT_GROUP).getValue().getString()));
			if (uiElementGroup != null) {
				uiElementGroup.expandGroup(requestParser.getParameter(ActionConstants.RC_EXPAND_GROUP).getValue().getString());
			}
		}
		
		//collapse AB or group
		if (requestParser.getParameter(ActionConstants.RC_COLLAPSE_GROUP).isSet() && 
				(requestParser.getParameter(ActionConstants.RC_COLLAPSE_GROUP).getValue().getString() != null &&
						requestParser.getParameter(ActionConstants.RC_COLLAPSE_GROUP).getValue().getString().length() > 0)) {
			UIElementGroupData uiElementGroup = 
				dynamicUI.getElementGroup(new TechKey(requestParser.getParameter(ActionConstants.RC_PARENT_GROUP).getValue().getString()));
			if (uiElementGroup != null) {
				uiElementGroup.collapseGroup(requestParser.getParameter(ActionConstants.RC_COLLAPSE_GROUP).getValue().getString());
			}
		}		
		
		// get the element which was onfocus
		if (requestParser.getParameter(ActionConstants.RC_FOCUS_CURRENT_ELEMENT).isSet() && 
				(requestParser.getParameter(ActionConstants.RC_FOCUS_CURRENT_ELEMENT).getValue().getString() != null && 
						requestParser.getParameter(ActionConstants.RC_FOCUS_CURRENT_ELEMENT).getValue().getString().length() > 0)) {
			dynamicUI.setCurrentElementOnFocus(requestParser.getParameter(ActionConstants.RC_FOCUS_CURRENT_ELEMENT).getValue().getString());
		}
		
		// get the wizard mode
		if (dynamicUI.isEditable()) {
			DynamicUIMaintenanceExitData exitHandler = dynamicUI.getMaintenanceExitData();
			if (exitHandler != null) {
				exitHandler.parseDesignerRequest(requestParser, dynamicUI);
			}
			if(requestParser.getParameter(ActionConstants.RC_WIZARD_MODE).isSet()) { 
				if (requestParser.getParameter(ActionConstants.RC_WIZARD_MODE).getValue().getString() != null && 
						requestParser.getParameter(ActionConstants.RC_WIZARD_MODE).getValue().getString().equals("true")) {
			        dynamicUI.setWizardMode(true);	
			    }
			}
			else {
				dynamicUI.setWizardMode(false);
			}
		}
		setNewPage(requestParser,
				   dynamicUI);
		
        log.exiting();
    }

    
	/**
	 * <p>Call this method to parse the request to update the data in the 
	 * {@link DynamicUI}object.</p>
	 *
	 * @param requestParser Parser to simple retrieve data from the request
	 * @param dynamicUI     Reference to the dynamic UI object
	 */
	public static void setNewPage(RequestParser requestParser,
									 DynamicUI dynamicUI) {
	    	
		// get the active page which should be used on the next JSP	
		if (requestParser.getParameter(ActionConstants.RC_NAVIGATE_TO_PAGE).isSet()) {
			String newPageName = requestParser.getParameter(ActionConstants.RC_NAVIGATE_TO_PAGE).getValue().getString();
			if (newPageName.length() > 0) {
				UIElementGroupData newPage = dynamicUI.getPage(newPageName);
				if (!checkNewPage(dynamicUI, newPage)) {
					return;
				}
				
				if (checkWizardMode(dynamicUI,newPage)) {
					if (dynamicUI.checkRequiredPropertiesForElementGroup(dynamicUI.getActivePage(), "appbase.jsp.requiredField")&& dynamicUI.isValid() ) {
						// check if the page is allowed.
						Iterator iter = dynamicUI.pageIterator();
						boolean onlyOneNext = false;
							
						while (iter.hasNext()) {
							UIElementGroupData page = (UIElementGroupData) iter.next();
							if (page.getName().equals(newPageName)) {
								dynamicUI.setActivePageName(newPageName);
								if (onlyOneNext) {
									dynamicUI.setWizardPage(newPageName);
								}
								break;
							}
							if (onlyOneNext && !page.isHidden()) {
								break;
							}
							if (page.getName().equals(dynamicUI.getWizardPage())) {
								onlyOneNext = true;
							}
						}
							
					}
					else {
						dynamicUI.addMessage(new Message(Message.ERROR,
                                "appbase.info.requiredFields",
                                null,
                                ""));
					}
				}
				else {
					dynamicUI.setActivePageName(newPageName);
				}
			}	
		}
	}

	
	/** 
	 * check if new page is valid and visible.
	 */ 
	private static boolean checkNewPage(DynamicUI dynamicUI, UIElementGroupData newPage) {

		boolean ret = false;

		if (newPage != null) {
			if (dynamicUI.isEditable()) {
				return true;
			}
			newPage.determineVisibility();
			if (!newPage.isHidden()) {
				return true;
			}
		}
	
		return ret;
	}	
		
	/** 
	 * check wizard logic only for valid Pages and pages with 
	 * higher group index.
	 */ 
	private static boolean checkWizardMode(DynamicUI dynamicUI, UIElementGroupData newPage) {

		boolean ret = false;

		if (dynamicUI.isWizardMode() && dynamicUI.getActivePage()!= null) {
			int indexNewPage = dynamicUI.getPageGroup().getElementIndex(newPage);
			int indexOldPage = dynamicUI.getPageGroup().getElementIndex(dynamicUI.getActivePage());
			if (indexNewPage > indexOldPage) {
				ret = true;	
			}
		} 
		
		return ret;
	}

    private static void parseProperty(RequestParser requestParser, 
    		                          DynamicUI dynamicUI, 
    		                          Property property) {

    	DynamicUIExitData exitHandler = dynamicUI.getDynamicUIExitData();
    	
        if (!property.isHidden() && !property.isDisabled()) {
            RequestParser.Parameter parameter = requestParser.getParameter(property.getRequestParameterName());

        	Object oldValue = property.getValue();
        	boolean parseValue = parameter.isSet() || property.getType().equals(Property.TYPE_BOOLEAN) || property.getType().equals(Property.TYPE_LIST) ;

            if (parseValue) {
	            if (property.getType().equals(Property.TYPE_STRING)) {
	                property.setValue(parameter.getValue().getString());
	            }
	            else if (property.getType().equals(Property.TYPE_BOOLEAN)) {
	                property.setValue(new Boolean(parameter.isSet()));
	            }
	            else if (property.getType().equals(Property.TYPE_LIST)) {
	            	String[] values = parameter.getValueStrings();
	            	List list = new ArrayList(values.length);
	            	for (int i = 0; i < values.length; i++) {
						list.add(values[i]);
					}
	            	property.setValue(list);
	            }
	            else if (property.getType().equals(Property.TYPE_INTEGER)) {
	            	
	            	if (!parameter.getValue().isInt()) {
	            		dynamicUI.addMessage(new Message(Message.ERROR,
									"mo.error.noInt",
									null,
									property.getName()));
	            	}
	            	else {				
	            		property.setValue(new Integer(parameter.getValue().getInt()));
	            	}	
	            }
            }
            boolean parsedByExitHandler = false;
            
            if (exitHandler!=null) {
            	parsedByExitHandler = exitHandler.parseProperty(requestParser,dynamicUI,parameter,property);
	        }
            
	        if (parseValue || parsedByExitHandler){    
	            if (property.getType().equals(Property.TYPE_LIST)) {
	            	List oldList = new ArrayList();
	            	if (oldValue.toString().length() > 0) {
	            		oldList = (List)oldValue;
	            	}
	            	List newList = property.getList();
	            	boolean changed = false;
	            	if (oldList.size() != newList.size()) {
	            		changed = true;
	            	}
	            	else {
	            		Iterator iter = oldList.iterator();
	            		while (iter.hasNext() && !changed) {
							if (! newList.contains(iter.next())) {
								changed = true;
							}
						} 
	            	}
	            	if (changed) {
	            		property.setValueChanged(true);
	            	}
	            }
	            else if (!property.getValue().equals(oldValue)) {
	            	property.setValueChanged(true);
	            }
	        }        
        }
    }
    

}
