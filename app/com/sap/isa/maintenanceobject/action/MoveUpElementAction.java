/*****************************************************************************
    Class         MoveElementAction
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Author:       SAP
    Created:      25.02.2008
    Version:      1.0

    $Revision: #1 $
    $Date: 2008/02/25 $
*****************************************************************************/
package com.sap.isa.maintenanceobject.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.maintenanceobject.backend.boi.DynamicUIMaintenanceExitData;
import com.sap.isa.maintenanceobject.backend.boi.UIElementGroupData;
import com.sap.isa.maintenanceobject.businessobject.DynamicUI;
import com.sap.isa.maintenanceobject.businessobject.DynamicUIMaintenance;

/**
 * @author d033221
 *
 */
public class MoveUpElementAction extends DynamicUIBaseAction {

	public ActionForward ecomPerform(ActionMapping mapping, 
            HttpServletRequest request, 
            HttpServletResponse response,
            UserSessionData userSessionData, 
            RequestParser requestParser,
            DynamicUIMaintenance dynamicUIMaintenance,
            DynamicUI dynamicUI,
            MetaBusinessObjectManager mbom, 
            boolean multipleInvocation,			                         
            boolean browserBack) 
		throws IOException, ServletException,CommunicationException { 

		if (requestParser.getParameter(ActionConstants.RC_EDIT_PROPERTY).isSet()) {

			String elementName = requestParser.getParameter(ActionConstants.RC_EDIT_PROPERTY).getValue().getString();

			TechKey parentKey = new TechKey(requestParser.getParameter(ActionConstants.RC_PARENT_GROUP).getValue().getString());
			UIElementGroupData parentGroup = dynamicUI.getElementGroup(parentKey);
			if (parentGroup != null) {
				// move the element down in the elementList to move it up on the screen!!
				boolean elementMoved = parentGroup.moveElementDown(elementName);
				if (!elementMoved) {
					DynamicUIMaintenanceExitData exitHandler = dynamicUI.getMaintenanceExitData();
			        if (exitHandler != null) {
			        	exitHandler.extendedElementMoveDown(dynamicUI, parentGroup, parentGroup.getElement(elementName));
			        }	
				}
			}	
		}
		else {
//TODO add error message!			
			return mapping.findForward("message");
		}

		return mapping.findForward("success");
		
	}
}
