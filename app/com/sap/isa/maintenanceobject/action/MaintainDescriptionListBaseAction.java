/*****************************************************************************
    Class:        DescriptionMaintainBaseAction
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      18.04.2008
    Version:      1.0
*****************************************************************************/

package com.sap.isa.maintenanceobject.action;

import java.io.IOException;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.EditObjectHandler;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.EComBaseAction;
import com.sap.isa.maintenanceobject.backend.boi.DescriptionData;
import com.sap.isa.maintenanceobject.backend.boi.DescriptionListData;
import com.sap.isa.maintenanceobject.businessobject.Description;

abstract public class MaintainDescriptionListBaseAction extends EComBaseAction {

	
	abstract protected void setDescriptionList(UserSessionData userSessionData, 
											   MetaBusinessObjectManager mbom,
											   EditObjectHandler editHandler,
											   DescriptionListData descriptionListData);
	
	
	public ActionForward ecomPerform(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response,
			UserSessionData userSessionData, RequestParser requestParser,
			MetaBusinessObjectManager mbom, boolean multipleInvocation,
			boolean browserBack) throws IOException, ServletException,
			CommunicationException {

        // first dispatch
        if (requestParser.getParameter(ActionConstants.RC_CANCEL).isSet() 
				&& requestParser.getParameter(ActionConstants.RC_CANCEL).getValue().getString().length() > 0) {

        	// reset the edit object
        	userSessionData.setEditObjectHandler(null);
            return mapping.findForward("canceled");
        }
		
		EditObjectHandler editHandler = userSessionData.getEditObjectHandler();
		
		if (editHandler == null) {
			return mapping.findForward("error");
		}
			
		DescriptionListData descriptionListData = (DescriptionListData)editHandler.getEditObject();
		if (descriptionListData == null) {
			return mapping.findForward("error");
		}

		parseDescriptionList(requestParser, descriptionListData);
		
		
		// first dispatch
        if (requestParser.getParameter(ActionConstants.RC_CONFIRM).isSet() 
				&& requestParser.getParameter(ActionConstants.RC_CONFIRM).getValue().getString().length() > 0) {

        	setDescriptionList(userSessionData, mbom, editHandler, descriptionListData);
        	// reset the edit object
        	userSessionData.setEditObjectHandler(null);
        	return mapping.findForward("confirmed");
        }	
		
        return mapping.findForward("success");
	}


	protected void parseDescriptionList(RequestParser requestParser, DescriptionListData descriptionListData) {
		RequestParser.Parameter languages = requestParser.getParameter(ActionConstants.RC_DESCRIPTION_LIST_LANGU+"[]");
		RequestParser.Parameter texts = requestParser.getParameter(ActionConstants.RC_DESCRIPTION_LIST_TEXT+"[]");
				
		if (languages.isSet()) {
			for (int index = 0;index <=languages.getNumValues();index++) {
				String language = languages.getValue(""+(index+1)).getString();
				String text = texts.getValue(""+(index+ 1)).getString();
				if (text.length()>0) {
					DescriptionData description = descriptionListData.getDescription(language);
					
					if (description == null) {
						description = new Description();
						description.setLanguage(language);
						descriptionListData.addDescription(description);
					}
					description.setDescription(text);
				}
			}
		}

        if (requestParser.getParameter(ActionConstants.RC_DELETE_DESCRIPTION).isSet()) {
        	String language = requestParser.getParameter(ActionConstants.RC_DELETE_DESCRIPTION).getValue().getString();
		    if (language.length() > 0) {
		    	descriptionListData.deleteDescription(language);
		    }
        }
	}

}
