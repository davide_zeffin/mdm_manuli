/*****************************************************************************
    Class         MoveElementAction
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Author:       SAP
    Created:      25.02.2008
    Version:      1.0

    $Revision: #1 $
    $Date: 2008/02/25 $
*****************************************************************************/
package com.sap.isa.maintenanceobject.action;

import java.util.List;

import com.sap.isa.maintenanceobject.backend.boi.AllowedValueData;

/**
 * @author d033221
 *
 */
public class MoveUpAllowedValueAction extends MoveAllowedValueAction {


	protected void moveValue(List valueList, int index, AllowedValueData value) {
		if (index>0){
			valueList.remove(index);
			valueList.add(index-1, value);
		}
	}

}
