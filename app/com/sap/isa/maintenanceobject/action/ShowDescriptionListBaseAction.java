/*****************************************************************************
    Class:        ShowDescriptionListAction
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      18.04.2008
    Version:      1.0
*****************************************************************************/

package com.sap.isa.maintenanceobject.action;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.EditObjectHandler;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.EComBaseAction;
import com.sap.isa.maintenanceobject.backend.boi.DescriptionListData;


abstract public class ShowDescriptionListBaseAction extends EComBaseAction {

	abstract protected Map getLanguageMap(UserSessionData userSessionData, 
            MetaBusinessObjectManager mbom);

	public ActionForward ecomPerform(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response,
			UserSessionData userSessionData, RequestParser requestParser,
			MetaBusinessObjectManager mbom, boolean multipleInvocation,
			boolean browserBack) throws IOException, ServletException,
			CommunicationException {

		EditObjectHandler editHandler = userSessionData.getEditObjectHandler();
		
		if (editHandler == null) {
			return mapping.findForward("error");
		}
			
		DescriptionListData descriptionListData = (DescriptionListData)editHandler.getEditObject();
		if (descriptionListData == null) {
			return mapping.findForward("error");
		}
		
		request.setAttribute(ActionConstants.RC_DESCRIPTION_LIST, descriptionListData);
		
		Map languageMap = getLanguageMap(userSessionData,mbom);
		
		request.setAttribute(ActionConstants.RC_LANGUAGE_MAP, languageMap);
		
		return mapping.findForward("success");
	}

}
