/*****************************************************************************
    Class         EditPropertyAction
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Author:       SAP
    Created:      25.02.2008
    Version:      1.0

    $Revision: #1 $
    $Date: 2008/02/25 $
*****************************************************************************/
package com.sap.isa.maintenanceobject.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.maintenanceobject.backend.boi.DescriptionData;
import com.sap.isa.maintenanceobject.backend.boi.DescriptionListData;
import com.sap.isa.maintenanceobject.backend.boi.UIPageData;
import com.sap.isa.maintenanceobject.businessobject.Description;
import com.sap.isa.maintenanceobject.businessobject.DynamicUI;
import com.sap.isa.maintenanceobject.businessobject.DynamicUIMaintenance;
import com.sap.isa.maintenanceobject.businessobject.PageMaintenance;

/**
 * @author d033221
 *
 */
public class MaintainPageAction extends DynamicUIBaseAction {

	public ActionForward ecomPerform(ActionMapping mapping, 
            HttpServletRequest request, 
            HttpServletResponse response,
            UserSessionData userSessionData, 
            RequestParser requestParser,
            DynamicUIMaintenance dynamicUIMaintenance,
            DynamicUI dynamicUI,
            MetaBusinessObjectManager mbom, 
            boolean multipleInvocation,			                         
            boolean browserBack) 
		throws IOException, ServletException,CommunicationException { 
	
		
        // first dispatch
        if (requestParser.getParameter(ActionConstants.RC_CANCEL).isSet() 
				&& requestParser.getParameter(ActionConstants.RC_CANCEL).getValue().getString().length() > 0) {

            return mapping.findForward("canceled");
        }

		PageMaintenance pageMaintenance = dynamicUIMaintenance.getPageMaintenance();
		UIPageData page = (UIPageData)dynamicUI.getPage(dynamicUIMaintenance.getCurrentPageName());
		
	    // DynamicUIMaintenanceExitData exitHandler = dynamicUI.getMaintenanceExitData();
        
        DynamicUIActionHelper.parseRequest(requestParser,pageMaintenance);
        
		if (dynamicUIMaintenance.getDescriptionList()!= null) {
			parseDescriptionList(requestParser,dynamicUIMaintenance.getDescriptionList());
		}	

        // first dispatch
		if ((requestParser.getParameter(ActionConstants.RC_CONFIRM).isSet() 
				&& requestParser.getParameter(ActionConstants.RC_CONFIRM).getValue().getString().length() > 0) ||
				(requestParser.getParameter(ActionConstants.RC_CONFIRM_SAVE).isSet() 
					&& requestParser.getParameter(ActionConstants.RC_CONFIRM_SAVE).getValue().getString().length() > 0)) {

        	pageMaintenance.synchronizeBeanProperties(page);
    		if (dynamicUIMaintenance.getDescriptionList()!= null) {
    			page.setDescriptionList(dynamicUIMaintenance.getDescriptionList());
    		}	

			if (requestParser.getParameter(ActionConstants.RC_CONFIRM_SAVE).isSet() 
					&& requestParser.getParameter(ActionConstants.RC_CONFIRM_SAVE).getValue().getString().length() > 0) {
						
				return mapping.findForward("confirmedandsave");		
			} 

        	return mapping.findForward("confirmed");
        }	
        
		return mapping.findForward("success");
		
	}
	
	protected void parseDescriptionList(RequestParser requestParser, DescriptionListData descriptionListData) {
		
		RequestParser.Parameter languages = requestParser.getParameter(ActionConstants.RC_DESCRIPTION_LIST_LANGU+"[]");
		RequestParser.Parameter texts = requestParser.getParameter(ActionConstants.RC_DESCRIPTION_LIST_TEXT+"[]");
				
		if (languages.isSet()) {
			for (int index = 0;index <=languages.getNumValues();index++) {
				String language = languages.getValue(""+(index+1)).getString();
				String text = texts.getValue(""+(index+ 1)).getString();
				if (text.length()>0) {
					DescriptionData description = descriptionListData.getDescription(language);
					
					if (description == null) {
						description = new Description();
						description.setLanguage(language);
						descriptionListData.addDescription(description);
					}
					description.setDescription(text);
				}
			}
		}

        if (requestParser.getParameter(ActionConstants.RC_DELETE_DESCRIPTION).isSet()) {
        	String language = requestParser.getParameter(ActionConstants.RC_DELETE_DESCRIPTION).getValue().getString();
		    if (language.length() > 0) {
		    	descriptionListData.deleteDescription(language);
		    }
        }
	}


}
