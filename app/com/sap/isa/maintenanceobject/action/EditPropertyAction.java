/*****************************************************************************
    Class         EditPropertyAction
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Author:       SAP
    Created:      25.02.2008
    Version:      1.0

    $Revision: #1 $
    $Date: 2008/02/25 $
*****************************************************************************/
package com.sap.isa.maintenanceobject.action;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.maintenanceobject.backend.boi.DynamicUIMaintenanceExitData;
import com.sap.isa.maintenanceobject.backend.boi.PropertyData;
import com.sap.isa.maintenanceobject.businessobject.DynamicUI;
import com.sap.isa.maintenanceobject.businessobject.DynamicUIMaintenance;
import com.sap.isa.maintenanceobject.businessobject.PropertyMaintenance;

/**
 * @author d033221
 *
 */
public class EditPropertyAction extends DynamicUIBaseAction {

	public ActionForward ecomPerform(ActionMapping mapping, 
            HttpServletRequest request, 
            HttpServletResponse response,
            UserSessionData userSessionData, 
            RequestParser requestParser,
            DynamicUIMaintenance dynamicUIMaintenance,
            DynamicUI dynamicUI,
            MetaBusinessObjectManager mbom, 
            boolean multipleInvocation,			                         
            boolean browserBack) 
		throws IOException, ServletException,CommunicationException { 

		if (requestParser.getParameter(ActionConstants.RC_EDIT_PROPERTY).isSet()) {
			
			TechKey key = new TechKey(requestParser.getParameter(ActionConstants.RC_EDIT_PROPERTY).getValue().getString());

			PropertyData property = dynamicUI.getProperty(key);
			
			PropertyMaintenance propertyMaintenance = dynamicUIMaintenance.getPropertyMaintenance();
			propertyMaintenance.synchronizeProperties(property);
			dynamicUIMaintenance.setCurrentPropertyKey(key);
			dynamicUIMaintenance.setAllowedValues(property.getAllowedValues());
			
			DynamicUIMaintenanceExitData exitHandler = dynamicUI.getMaintenanceExitData();
	        if (exitHandler != null) {
	        	exitHandler.adjustPropertyMaintenance(property, propertyMaintenance);
	        }
			
			request.setAttribute(ActionConstants.RC_PROPERTY, propertyMaintenance);
		}
		else {
//TODO add error message!			
			return mapping.findForward("message");
		}

		return mapping.findForward("success");
		
	}
}
