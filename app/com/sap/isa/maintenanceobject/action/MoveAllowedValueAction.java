/*****************************************************************************
    Class         MoveElementAction
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Author:       SAP
    Created:      25.02.2008
    Version:      1.0

    $Revision: #1 $
    $Date: 2008/02/25 $
*****************************************************************************/
package com.sap.isa.maintenanceobject.action;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.maintenanceobject.backend.boi.AllowedValueData;
import com.sap.isa.maintenanceobject.backend.boi.PropertyData;
import com.sap.isa.maintenanceobject.businessobject.DynamicUI;
import com.sap.isa.maintenanceobject.businessobject.DynamicUIMaintenance;

/**
 * @author d033221
 *
 */
abstract public class MoveAllowedValueAction extends DynamicUIBaseAction {

	public ActionForward ecomPerform(ActionMapping mapping, 
            HttpServletRequest request, 
            HttpServletResponse response,
            UserSessionData userSessionData, 
            RequestParser requestParser,
            DynamicUIMaintenance dynamicUIMaintenance,
            DynamicUI dynamicUI,
            MetaBusinessObjectManager mbom, 
            boolean multipleInvocation,			                         
            boolean browserBack) 
		throws IOException, ServletException,CommunicationException { 

		if (requestParser.getParameter(ActionConstants.RC_CURRENT_ALLOWED_VALUE).isSet()) {
	
			String allowedValue = requestParser.getParameter(ActionConstants.RC_CURRENT_ALLOWED_VALUE).getValue().getString();
			
			if (allowedValue.length() > 0) {
				
				List valueList = getAllowedValues(dynamicUIMaintenance, dynamicUI, requestParser);
				
		    	Iterator iter = valueList.iterator();
		    	int i=0;
		    	int index = -1;
		    	AllowedValueData value = null;
		    	while (iter.hasNext()) {
					value = (AllowedValueData) iter.next();					
					if (value.getValue().equals(allowedValue)) {
						index = i;
						break;
					}
					i++;	
				}
				
				moveValue(valueList, index, value);
			}
		}

		return mapping.findForward("success");		
	}

	protected List getAllowedValues(DynamicUIMaintenance dynamicUIMaintenance, DynamicUI dynamicUI, RequestParser requestParser) {
		List valueList = dynamicUIMaintenance.getAllowedValues();
		
		if (valueList == null){
			TechKey key = new TechKey(requestParser.getParameter(ActionConstants.RC_EDIT_PROPERTY).getValue().getString());

			PropertyData property = dynamicUI.getProperty(key);
			
			valueList = property.getAllowedValues();
		}
		return valueList;
	}

	abstract protected void moveValue(List valueList, int index, AllowedValueData value);

}
