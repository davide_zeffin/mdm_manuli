/*****************************************************************************
    Class:        UIElementGroup
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP AG
    Created:      Januray 2002
    Version:      1.0

    $Revision: #7 $
    $Date: 2001/07/25 $
*****************************************************************************/

package com.sap.isa.maintenanceobject.businessobject;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.sap.isa.core.TechKey;
import com.sap.isa.maintenanceobject.backend.boi.UIElementBaseData;
import com.sap.isa.maintenanceobject.backend.boi.UIElementData;
import com.sap.isa.maintenanceobject.backend.boi.UIElementGroupData;

/**
 * The element group manages a group of properties, which should maintain in
 * a generic web ui as one group.
 * The object does not allow to mix elements and groups. 
 *
 * @author SAP AG
 * @version 1.0
 */
public class UIElementGroup extends UIElementBase 
		implements Cloneable, UIElementGroupData {

    // only private access to the elements
    private Map elements = new HashMap();

    // the elements are also stored as a list to keep the order
    private List elementList = new ArrayList();

    /**
     * <p>Flag, if the group has groups as elements. </p> 
     */
    private boolean hasGroups = false;
    
    private boolean hasElements = false;
    
    /**
     * Flag, if the given group is expanded. <br>
     */
    private boolean isExpanded = true;

    /**
     * Flag, if only one group element is expanded. <br>
     */
    private boolean isOnlyOneGroupExpanded = false;
    
    /**
     * Flag, if empty groups should be displayed. <br>
     */
    private boolean displayEmptyGroups = false;
    
    /**
     * Flag, if expanding and collapsing for the given group is allowed.
     */
    private boolean isToggleAllowed = false;

    /**
     * Standard Constructor. <br>
     */
    
    public UIElementGroup(){
    }

    
    /**
     * Constructor for a element group with the given name. <br>
     *
     * @param name name of group
     *
     */
    public UIElementGroup(String name) {
        super(name);
    }

    
    /**
     * This method allows to build a type clean implementation of a group using
     * elements which extends the UIElement objects.
     *  
     * @return <code>true</code> if the instance is allowed. 
     */	
    protected boolean checkElementInstance(UIElementBaseData element) {
        return true;
    }
    
    /**
     * This method allows to build a type clean implementation of a group using
     * elements which extends the UIElement objects.
     *  
     * @return <code>true</code> if the instance is allowed. 
     */	
    protected boolean checkGroupInstance(UIElementBaseData element) {
        return true;
    }
    
    

    //TODO docu
	/**
	 * Overwrites the method . <br>
	 * 
	 * @param element
	 * 
	 * 
	 * @see com.sap.isa.maintenanceobject.backend.boi.UIElementGroupData#addElement(com.sap.isa.maintenanceobject.backend.boi.UIElementData)
	 */
    public void addElement(UIElementData element) {

        if (checkElementInstance(element)&& !hasGroups) {
        	hasElements = true;
            elements.put(element.getName(), element);
            elementList.add(element);
            if (element instanceof UIElementGroupData) {
            	hasGroups = true;
            }
        }
    }

    //TODO DOCU
    /**
     * <p> The method insertElement </p>
     * 
     * @param element
     * @param index
     */
    public void addElement(int index, UIElementData element ) {

        if (checkElementInstance(element)&& !hasGroups) {
        	hasElements = true;
            elements.put(element.getName(), element);
            elementList.add(index, element);
            if (element instanceof UIElementGroupData) {
            	hasGroups = true;
            }
        }
    }

    //TODO docu
	/**
	 * Overwrites the method . <br>
	 * 
	 * @param element
	 * 
	 * 
	 * @see com.sap.isa.maintenanceobject.backend.boi.UIElementGroupData#addElement(com.sap.isa.maintenanceobject.businessobject.UIElement)
	 */
    public void addElement(UIElementGroupData element) {

        if (checkGroupInstance(element) && !hasElements) {
            elements.put(element.getName(), element);
            elementList.add(element);
            hasGroups = true;
        }
    }

    
    /**
     * <p>Delete an element. </p> 
     */
    public void deleteElement(UIElementData element) {
    	elementList.remove(element);
    	elements.remove(element.getName());
    }
    

    /**
     * <p>Delete an element group. </p> 
     */
    public void deleteElement(UIElementGroupData element) {
    	elementList.remove(element);
    	elements.remove(element.getName());
    }
    

    
    /**
     * <p>Clear the complete element group. </p> 
     */
    public void clear() {
    	elementList.clear();
    	elements.clear();
    }

    
    /**
     * <p> Overwrites the method getElement. </p>
     * <p>Returns the UI element within the group on the first level. 
     *    Use the method {@link #getElementList(String)} to find every element with the given name in the
     *    whole element tree </p>   
     * 
     * @param name name of the UI Element.
     * @return the found element.
     * 
     * @see com.sap.isa.maintenanceobject.backend.boi.UIElementGroupData#getElement(java.lang.String)
     */
    public UIElementBaseData getElement(String name) {
        return (UIElementBaseData)elements.get(name);
    }

    /**
     * <p> Overwrites the method getElementList. </p>
     * <p>Returns the list of UI elements within the whole group including sub groups.</p>   
     * 
     * @param name name of the UI Element.
     * @return list with all found elements.
     * 
     * @see com.sap.isa.maintenanceobject.backend.boi.UIElementGroupData#getElement(java.lang.String)
     */
    public List getElementList(String name) {
    	return getElementList(name, null);
    }

    /**
     * <p> Overwrites the method getElementList. </p>
     * <p>Returns the list of UI elements within the whole group including sub groups.</p>   
     * 
     * @param name name of the UI Element.
     * @param isStructured possible filter for structure or unstructure elements. Pass 
     *        <code>null</code> to get all elements. 
     * @return list with all found elements.
     * 
     * @see com.sap.isa.maintenanceobject.backend.boi.UIElementGroupData#getElement(java.lang.String)
     */
    public List getElementList(String name, Boolean isStructured) {
    	List resultList = new ArrayList();
    	
        for(int i = 0; i < elementList.size(); i++) {
        	UIElementBaseData element = (UIElementBaseData)elementList.get(i);
        	 
        	if (element.getName().equals(name)) {
        		if (isStructured == null || isStructured.booleanValue() == element.isStructured()) {
        			resultList.add(element);
        		}	
        	}
            
            if (element instanceof UIElementGroupData) {
            	List children = ((UIElementGroupData)element).getElementList(name, isStructured);
            	if (children.size()>0) {
            		resultList.addAll(children);
            	}	
            }
        }
        
        return resultList;
    }       
    
    /**
     * Overwrites the method <code>getElement</code>. <br>
     * 
     * @param key techKey to identify the element.
     * @return found element or <code>null</code>.
     * 
     * @see com.sap.isa.maintenanceobject.backend.boi.UIElementGroupData#getElement(com.sap.isa.core.TechKey)
     */
    public UIElementBaseData getElement(TechKey key) {
    	
        for(int i = 0; i < elementList.size(); i++) {
        	UIElementBaseData element = (UIElementBaseData)elementList.get(i);
        	 
        	if (element.getTechKey().equals(key)) {
        		return element;
        	}
            
            if (element instanceof UIElementGroupData) {
            	UIElementBaseData foundElement = ((UIElementGroupData)element).getElement(key);
            	if (foundElement != null) {
            		return foundElement;
            	}	
            }
        }
        
        return null;
    }       
    

	/**
     * <p>Returns the parent group for the given key.</p>   
     * 
     * @param key key of the UI Element.
     * @return found element or <code>null</code>.
     */
    public UIElementGroupData getParent(TechKey key) {

    	for(int i = 0; i < elementList.size(); i++) {
	    	UIElementBaseData element = (UIElementBaseData)elementList.get(i);
	    	 
	    	if (element.getTechKey().equals(key)) {
	    		return this;
	    	}
	        
	        if (element instanceof UIElementGroupData) {
	        	UIElementGroupData foundElement = ((UIElementGroupData)element).getParent(key);
	        	if (foundElement != null) {
	        		return foundElement;
	        	}	
	        }
	    }
	    
	    return null;
	}       

    
    // TODO docu
    /**
     * 
     * Return Set the property {@link #}. <br>
     * 
     * @return
     */
    public List getElementList() {
    	return elementList;
    }

    // TODO docu
    /**
     * 
     * Return Set the property {@link #}. <br>
     * 
     * @return
     */
    protected Map getElementMap() {
    	return elements;
    }


    /**
     * Allows to set the elements. <br>
     * 
     * @param elements
     * @param elementListd
     */
    protected void setElements(Map elements, List elementList) {
    	
    	this.elements = elements;
    	this.elementList = elementList;    		
    }
    
    
    //TODO docu
	/**
	 * Overwrites the method . <br>
	 * 
	 * @see com.sap.isa.maintenanceobject.backend.boi.UIElementGroupData#determineVisibility()
	 */
    public void determineVisibility() {

    	boolean isHidden = true;

        if (displayEmptyGroups && !hasGroups) {
        	setHidden(false);
        	return;
        }

        for(int i = 0; i < elementList.size(); i++) {

            UIElementBaseData element = (UIElementBaseData)elementList.get(i);
            
            if (element instanceof UIElementGroupData) {
            	((UIElementGroupData)element).determineVisibility();
            }

            if(displayEmptyGroups || !element.isHidden()) {
            	isHidden = false;
                if (!hasGroups) { 
                    setHidden(isHidden);
                	return;
                }	
            }
        }
        
        setHidden(isHidden);
    }


	//TODO docu
	/**
	 * Overwrites the method . <br>
	 * 
	 * @return
	 * 
	 * 
	 * @see com.sap.isa.maintenanceobject.backend.boi.UIElementGroupData#getNumberOfVisibleElements()
	 */
	public int getNumberOfVisibleElements() {

	    determineVisibility();	
		int number = 0;

		for(int i = 0; i < elementList.size(); i++) {

            UIElementData element = (UIElementData)elementList.get(i);

			if(!element.isHidden()) {
				number++;
			}
		}
		return number;
	}

	
    /**
     * Returns if the object is structured.
     */
    public boolean isStructured() {
    	return true;
    }


    /**
     * <p>Implements the method <code>iterator</code> 
     * of the <code>Iterable</code> interface. </p>
     * 
     * @return iterator over the list of UI elements. 
     * 
     * @see com.sap.isa.core.Iterable#iterator()
     */
    public Iterator iterator() {
        return elementList.iterator();
    }

    
	/**
	 * <p>Implements the method <code>propertyIterator</code> 
	 * of the <code>UIElementGroupData<code> interface. </p>
	 * 
	 * @return an instance of PropertyIterator for elements which have groups or the
	 *         {@link #iterator()} for elements without groups. 
	 * 
	 * @see com.sap.isa.maintenanceobject.backend.boi.UIElementGroupData#propertyIterator()
	 */
    public Iterator propertyIterator() {
    	if (hasGroups) {
    		return new PropertyIterator(elementList);
    	}
    	else {
    		return iterator();
    	}
    }

    
    /**
     * Returns the object as string. <br>
     *
     * @return String which contains all fields of the object
     *
     */
    public String toString( ) {

        StringBuffer str = new StringBuffer("ElementGroup " + super.toString());
        
        str.append("ElementList: ");
        
		for(int i = 0; i < elementList.size(); i++) {
		    str.append(elementList.get(i).toString());
		}

        return str.toString();
    }


	/**
	 * Clones the element group. <br>
	 * All including objects will be also cloned.
	 * 
	 * @return copy of the object and all sub objects
	 * 
	 * @see java.lang.Object#clone()
	 */
	public Object clone()  {
		try {
			UIElementGroup group = (UIElementGroup)super.clone();
			
			// reset the cloned map and list
			group.elements = new HashMap();
			group.elementList = new ArrayList();
				
			Iterator iter = elementList.iterator();
			while (iter.hasNext()) {
			    UIElementBaseData oldElement = (UIElementBaseData)iter.next();
			    if (oldElement instanceof UIElementGroupData) {
			    	UIElementGroupData groupElement = (UIElementGroupData)oldElement; 
			    	group.addElement((UIElementGroupData)groupElement.clone());
			    }
			    else if (oldElement instanceof UIElementData) {
			    	UIElementData element = (UIElementData) oldElement;
			    	group.addElement((UIElementData)element.clone());
			    }
			}    
			return group;
		}
		catch (CloneNotSupportedException e) {
			return null;
		}
	}
	
	// TODO Docu
	protected Object hollowClone() {
		try {
			UIElementGroup group = (UIElementGroup)super.clone();

			// reset the cloned map and list
			group.elements = new HashMap();
			group.elementList = new ArrayList();
				
			Iterator iter = elementList.iterator();
			while (iter.hasNext()) {
			    UIElementBaseData oldElement = (UIElementBaseData)iter.next();
			    if (oldElement instanceof UIElementGroupData) {
			    	group.addElement((UIElementGroupData)oldElement);
			    }
			    else if (oldElement instanceof UIElementData) {
			    	group.addElement((UIElementData)oldElement);
			    }
			}

			return group;
		}
		catch (CloneNotSupportedException e) {
			return null;
		}
	}	

	
	/**
	 * Clones a group without elements . <br>
	 * All including objects won't be cloned.
	 * 
	 * @return copy of the object 
	 * 
	 */
	public Object cloneWithoutElements()  {

		UIElementGroup group;
		try {
			group = (UIElementGroup)super.clone();
			// reset the cloned map and list
			group.elements = new HashMap();
			group.elementList = new ArrayList();
			return group;
			
		} catch (CloneNotSupportedException e) {
			return null;
		}
	}
		
	
	// TODO Docu
	protected Object simpleClone() {
		try {
			UIElementGroup group = (UIElementGroup)super.clone();
			return group;
		}
		catch (CloneNotSupportedException e) {
			return null;
		}
	}		
	
    
	private static class PropertyIterator implements Iterator {
		
        private Iterator propertyIterator;
        private List elementList;
        private int currentIndex = 0;

        public PropertyIterator(List elementList) {
        	this.elementList = elementList;
        	currentIndex = 0;
        }

        /**
         * Overwrites the method hasNext. <br>
         * 
         * @return 
         * 
         * @see java.util.Iterator#hasNext()
         */
        public boolean hasNext() {

			while (propertyIterator == null || !propertyIterator.hasNext()) {
			
				if (currentIndex < elementList.size()) {
					UIElementBaseData nextObject = (UIElementBaseData)elementList.get(currentIndex);
					if (!nextObject.isStructured()) {
						return true;
					}
					propertyIterator = ((UIElementGroupData)nextObject).propertyIterator();
					currentIndex++;					
				}	
				else {
					return false;
				}
			}
	
			if (propertyIterator != null && propertyIterator.hasNext()) {
				return true;
			}

            return false;
        }

        /**
         * Overwrites the method next. <br>
         * 
         * @return 
         * @see java.util.Iterator#next()
         */
        public Object next() {
	
        	Object nextObject = null;
        	
            if (hasNext()) {
            	if (propertyIterator != null) {
            		nextObject = propertyIterator.next();
            	}
            	else {
					nextObject = elementList.get(currentIndex);
					currentIndex++;
            	}	
            }
            return nextObject;
        }


        /**
         * Overwrites the method remove. <br>
         * 
         * @see java.util.Iterator#remove()
         */
        public void remove() {

            if (hasNext()) {
				propertyIterator.remove();
        	}
        }


    }


	/**
	 * 
	 * Return the property {@link #isExpanded}. <br>
	 * 
	 * @return isExpanded
	 */
	public boolean isExpanded() {
		return isExpanded;
	}


	/**
	 *  Set the property {@link #isExpanded}. <br>
	 * 
	 * @param isExpanded The isExpanded to set.
	 */
	public void setExpanded(boolean isExpanded) {
		this.isExpanded = isExpanded;
	}


	/**
	 * 
	 * Return the property {@link #displayEmptyGroups}. <br>
	 * 
	 * @return displayEmptyGroups
	 */
	public boolean isDisplayEmptyGroups() {
		return displayEmptyGroups;
	}


	/**
	 *  Set the property {@link #displayEmptyGroups}. <br>
	 * 
	 * @param displayEmptyGroups The displayEmptyGroups to set.
	 */
	public void setDisplayEmptyGroups(boolean displayEmptyGroups) {
		this.displayEmptyGroups = displayEmptyGroups;
	}

	/**
	 * <p> Returns true if only one group element should be expanded. </p>
	 * 
	 * @return isOnlyOneABExpanded
	 */
    public boolean isOnlyOneGroupExpanded() {
    	return isOnlyOneGroupExpanded;
    }
    
    /**
     * <p> Set the property if only one group element should be expanded. </p>
     * 
     * @param isOnlyOneGroupExpanded
     */
    public void setOnlyOneGroupExpanded(boolean isOnlyOneGroupExpanded) {
    	this.isOnlyOneGroupExpanded = isOnlyOneGroupExpanded;
    }
    
    
    /**
     * Expand the group for the given group name. <br>
     * 
     * @param groupName
     */
    public void expandGroup(String groupName) {

		for (int i = 0; i < elementList.size(); i++) {

			UIElementBaseData element = (UIElementBaseData) elementList.get(i);

			if (element instanceof UIElementGroupData) {
				// expand the element with groupName depending on attribute
				// isOnlyOneGroupExpanded
				if (((UIElementGroupData) element).isOnlyOneGroupExpanded()) {
					if (element.getName().equals(groupName)) {
						((UIElementGroupData) element).setExpanded(true);
						if (((UIElementGroupData) element).isStructured()){
							List subElements = ((UIElementGroupData) element).getElementList();
							if (subElements != null && subElements.size()>0){
								UIElementBaseData subElement = (UIElementBaseData) subElements.get(0);
								if (subElement instanceof UIElementGroupData){
									((UIElementGroupData) subElement).setExpanded(true);
								}
							}
						}
					} 
					else {
						((UIElementGroupData) element).setExpanded(false);
					}
				} 
				else {
					if (element.getName().equals(groupName)) {
						((UIElementGroupData) element).setExpanded(true);
						if (((UIElementGroupData) element).isStructured()){
							List subElements = ((UIElementGroupData) element).getElementList();
							if (subElements != null && subElements.size()>0){
								UIElementBaseData subElement = (UIElementBaseData) subElements.get(0);
								if (subElement instanceof UIElementGroupData){
									((UIElementGroupData) subElement).setExpanded(true);
								}
							}
						}
					}
				}
			}

		}
	}

    
    /**
	 * Collapse the group for the given group name. <br>
	 * 
	 * @param groupName
	 */
    public void collapseGroup(String groupName) {

		UIElementGroupData element = (UIElementGroupData) elements.get(groupName);
		if (element != null) {
		    element.setExpanded(false);
		}
	}


	/**
	 * 
	 * Return the property {@link #isToggleAllowed}. <br>
	 * 
	 * @return isToggleAllowed
	 */
	public boolean isToggleAllowed() {
		return isToggleAllowed;
	}


	/**
	 *  Set the property {@link #isToggleAllowed}. <br>
	 * 
	 * @param isToggleAllowed The isToggleAllowed to set.
	 */
	public void setToggleAllowed(boolean isToggleAllowed) {
		this.isToggleAllowed = isToggleAllowed;
	}
	
	
    /**
     * Move group for the given group name. <br>
     * 
     * @param groupName
     * @param direction
     */
    public boolean moveElementUp(String elementName) {

    	boolean ret = false; 
    	UIElementBaseData element = (UIElementBaseData) elements.get(elementName);    	
		
		if (element != null) {
			int index = elementList.indexOf(element);
			
			if (index>=0 && index<elementList.size()-1 ){

				int foundIndex = index;
				
				// ignore hidden elements while move elements
				while (foundIndex<elementList.size())  {
					foundIndex++;
					UIElementBaseData replaceElement = (UIElementBaseData)elementList.get(foundIndex);
					if (!replaceElement.isHidden()) {
						elementList.remove(index);
						elementList.add(foundIndex, element);
						return true;
					}
				} 	
			}
		}	
		return ret;
    }	

    
    /**
     * Move group for the given group name. <br>
     * 
     * @param groupName
     * @param direction
     */
    public boolean moveElementDown(String elementName) {

    	boolean ret = false; 
		UIElementBaseData element = (UIElementBaseData) elements.get(elementName);
		
		if (element != null) {
			int index = elementList.indexOf(element);
			
			if (index>0) {
				int foundIndex = index;
				
				// ignore hidden elements while move elements
				while (foundIndex>0)  {
					foundIndex--;
					UIElementBaseData replaceElement = (UIElementBaseData)elementList.get(foundIndex);
					if (!replaceElement.isHidden()) {
						elementList.remove(index);
						elementList.add(foundIndex, element);
						return true;
					}
				} 	
			}
		}	
		return ret;
    }	

    /**
     * <p> The method getElementIndex returns the index of the element in the List.</p>
     * <p><strong>Note</strong> That the index starts with <code>1</code></p>
     * 
     * @param element UI element
     * @return index in the list.
     */
    public int getElementIndex(UIElementBaseData element) {
    	
    	int ret = elementList.indexOf(element);
    	if (ret >= 0) {
    		ret++;
    	}
    	return ret;
    }
    
    
}
