/*****************************************************************************
    Class:        GSScreenGroup
    Copyright (c) 2003, SAP AG, Germany, All rights reserved.

    $Revision: #2 $
    $DateTime: 2003/12/12 15:22:03 $ (Last changed)
    $Change: 163880 $ (changelist)
    
*****************************************************************************/

package com.sap.isa.maintenanceobject.businessobject;

/**
 *
 * The GS (generic search) screen group manages the additional attributes which
 * must be defined for the generically build search screen.
 *
 */
public class GSScreenGroup extends ScreenGroup	{
    /**
     * Type of the screen group
     */
    protected String type;
	/**
	 * UI Class name
	 */
	protected String uiClassName;

    /**
     * Standard Constructor
     *
     */
    public GSScreenGroup(){
    }


    /**
     * Constructor for a screen group with the given name
     *
     * @param name name of group
     *
     */
    public GSScreenGroup(String name) {
        super(name);
    }

	/**
	 * Return the type of the Screen Group
	 * @return Type
	 */
	public String getType() {
		return type;
	}

	/**
	 * Set type of the Screen Group
	 * @param string
	 */
	public void setType(String string) {
		type = string;
	}

	/**
	 * Return the object as string
	 *
	 * @return String which contains all fields of the object
	 *
	 */
	public String toString( ) {
		String str = super.toString()
					 .concat(", Type: " + type );

		return str;
	}
	/**
	 * Get UIClass name of the Screen Group
	 * @return String
	 */
	public String getUiClassName() {
		return uiClassName;
	}

	/**
	 * Set UIClass name of the Screen Group
	 * @param string
	 */
	public void setUiClassName(String string) {
		uiClassName = string;
	}

}
