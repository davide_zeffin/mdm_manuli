/*****************************************************************************
    Class:        AllowedValue
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      Januray 2002
    Version:      1.0

    $Revision: #3 $
    $Date: 2004/03/03 $
*****************************************************************************/

package com.sap.isa.maintenanceobject.businessobject;


/**
 * Represent a allow value for a property. With allowed values you can restrict
 * the input of the user to reasonable values. At the moment only String properties
 * are supported.
 *
 * @author SAP
 * @version 1.0
 *
 * @see com.sap.isa.maintenanceobject.businessobject.GSProperty
 * @see com.sap.isa.maintenanceobject.businessobject.GenericSearchBuilder
 */
public class GSAllowedValue extends AllowedValue {

    /**
     * Flag if the value is the default Allowed Value
     */
    protected boolean isDefault;
	/**
	 * Content type (default is space) of the Allowed Value
	 */
	protected String content;
	/**
	 * Content class (default is space) of the Allowed Value
	 */
	protected String contentCreateClass;
	/**
	 * Content create method (default is space) of the Allowed Value
	 */
	protected String contentCreateMethod;
	/**
	 * ResultList name of the Allowed Value
	 */
    protected String resultlistName;
    /**
     * Number of maximum hits to be selected from the database
     */
    protected int maxHitsToSelect = -1;
    /**
     * Standard Constructor
     *
     */
    public GSAllowedValue(){
    }

    /**
     * Return the object as string
     *
     * @return String which contains all fields of the object
     *
     */
    public String toString( ) {

        String str = super.toString()
                     .concat(", isDefault: " + isDefault)
                     .concat(", Content: " + content)
		             .concat(", ContentCreateClass: " + contentCreateClass)
		             .concat(", ContentCreateMethod: " + contentCreateMethod);
        return str;
    }

	/**
	 * Return true if value is the default Value
	 * @return isDefault
	 */
	public boolean isDefault() {
		return isDefault;
	}

	/**
	 * Set Value as the default
	 * @param boolean
	 */
	public void setDefault(boolean bool) {
		isDefault = bool;
	}

	/**
	 * Return the content of the Allowed Value
	 * @return content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * Set the content of the Allowed Value
	 * @param string
	 */
	public void setContent(String string) {
		content = string;
	}

	/**
	 * Return the content create class of the Allowed Value
	 * @return contentClass
	 */
	public String getContentCreateClass() {
		return contentCreateClass;
	}

	/**
	 * Return the content create method of the Allowed Value
	 * @return
	 */
	public String getContentCreateMethod() {
		return contentCreateMethod;
	}

	/**
	 * Set the content create class of the Allowed Value 
	 * @param string
	 */
	public void setContentCreateClass(String string) {
		contentCreateClass = string;
	}

	/**
	 * Set the content create method of the Allowed Value
	 * @param string
	 */
	public void setContentCreateMethod(String string) {
		contentCreateMethod = string;
	}

	/**
	 * Return the resultlist name of the Allowed Value
	 * @return
	 */
	public String getResultlistName() {
		return resultlistName;
	}

	/**
	 * Set the resultlist name of the Allowed Value
	 * @param string
	 */
	public void setResultlistName(String string) {
		resultlistName = string;
	}

    /**
     * Get the number of maximum hits to be selected from the database
     * @return int max hits parameter
     */
    public int getMaxHitsToSelect() {
        return maxHitsToSelect;
    }

    /**
     * Set the number of maximum hits to be selected from the database
     * @param int max hits parameter
     */
    public void setMaxHitsToSelect(int i) {
        maxHitsToSelect = i;
    }
}
