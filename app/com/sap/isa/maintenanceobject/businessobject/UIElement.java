/*****************************************************************************
    Class:        UIElement
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      13.10.2004
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.maintenanceobject.businessobject;

import com.sap.isa.maintenanceobject.backend.boi.UIElementData;

/**
 * Base class for UI elements which contains the properties name,
 * descrition and enable flag which are used from all editable UI elements.
 *
 * @author SAP
 * @version 1.0
 */
public class UIElement extends UIElementBase	implements UIElementData {
    
    /**
     * flag if property is disabled. <br>
     */
    protected boolean isDisabled = false;

	/**
	 * Container, which allows to store backend depending properties s<br>
	 */
	protected Object backendProperties;


	/**
	 * Standard Constructor
	 *
	 */
	public UIElement() {
	}
	
	
	/**
	 * Constructor for a property group with the given name
	 *
	 * @param name name of group
	 *
	 */
	public UIElement(String name) {
		super(name);
	}

	
    /**
     * Set the property disabled to the given value. <br>
     *
     * @param disabled
     *
     */
    public void setDisabled(boolean disabled) {
        this.isDisabled = disabled;
    }


    /**
     * Disables the element. <br>
     *
     * @param disabled
     *
     */
    public void setDisabled() {
        this.isDisabled = true;
    }

    
    /**
     * Enables the elements. <br>
     *
     * @param disabled
     *
     */
    public void setEnabled() {
        this.isDisabled = false;
    }


    /**
     * Returns if the element is disabled. <br>
     *
     * @return <code>true</code> if the element isn't editable and <code>false</code>
     *    if the element is editable
     *
     */
    public boolean isDisabled() {
       return isDisabled;
    }


    /**
     * Returns if the element is enabled. <br>
     *
     * @return <code>true</code> if the element is editable and <code>false</code>
     *    if the element isn't editable
     *
     */
    public boolean isEnabled() {
       return ! isDisabled;
    }


	/**
	 * Return the property {@link #backendProperties}. <br>
	 * 
	 * @return
	 */
	public Object getBackendProperties() {
		return backendProperties;
	}

	/**
	 * Set the property {@link #backendProperties}. <br>
	 * 
	 * @param backendProperties {@link #backendProperties}
	 */
	public void setBackendProperties(Object backendProperties) {
		this.backendProperties = backendProperties;
	}


}
