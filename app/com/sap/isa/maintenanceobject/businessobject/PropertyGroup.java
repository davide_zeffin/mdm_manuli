/*****************************************************************************
    Class:        PropertyGroup
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP AG
    Created:      Januray 2002
    Version:      1.0

    $Revision: #7 $
    $Date: 2001/07/25 $
*****************************************************************************/

package com.sap.isa.maintenanceobject.businessobject;

import com.sap.isa.maintenanceobject.backend.boi.PropertyData;
import com.sap.isa.maintenanceobject.backend.boi.PropertyGroupData;
import com.sap.isa.maintenanceobject.backend.boi.UIElementBaseData;
import com.sap.isa.maintenanceobject.ui.renderer.UIRendererManager;

/**
 * <p>The property group manages a group of elements, which should maintain in
 * a generic web UI as one group .</p> 
 * <p> For example all catalog relevant fields
 * in the shop maintenance.<p>
 * <p>You can only add {@link com.sap.isa.maintenanceobject.businessobject.Property} objects to 
 * this group</p> 
 *
 * @author SAP AG
 * @version 1.0
 */
public class PropertyGroup extends UIElementGroup 
		implements Cloneable, PropertyGroupData {


    /**
     * Standard Constructor
     */
    public PropertyGroup(){
    	uiType = UIRendererManager.UI_TYPE_PROPERTY_GROUP;
    }

    /**
     * Constructor for a property group with the given name
     *
     * @param name name of group
     */
    public PropertyGroup(String name) {
        super(name);
        uiType = UIRendererManager.UI_TYPE_PROPERTY_GROUP;
    }

    /**
     * This method allows to build a type clean implementation of a group using
     * elements which extends the UIElement objects.
     *  
     * @return <code>true</code> if the instance is a 
     * {@link com.sap.isa.maintenanceobject.businessobject.Property} object. 
     */	
    protected boolean checkElementInstance(UIElementBaseData element) {
        if (element instanceof Property) {
        	return true;
        }
        return false;
    }

	/**
	 * <p>Overwrites the method checkGroupInstance. </p>
	 * <p>The method avoid that no other UI element groups can be added. </p>
	 * 
	 * @param element
	 * @return always <code>false</code> because it is not allowed to add groups to the property group. <br>
	 * 
	 * 
	 * @see com.sap.isa.maintenanceobject.businessobject.UIElementGroup#checkGroupInstance(com.sap.isa.maintenanceobject.backend.boi.UIElementBaseData)
	 */
	final protected boolean checkGroupInstance(UIElementBaseData element) {
		return false;
	}

	/**
      * add a given property to the property group
      *
      * @param property property to be added
      *
      */
    public void addProperty(PropertyData property) {
    	addElement(property);
    }


    /**
      * add a given property to the property group
      *
      * @param property property to be added
      *
      */
    public void addProperty(Property property) {
    	addElement(property);
    }


    /**
      * creates a new property
      *
      * @param name name of the property
      *
      */
    public PropertyData createProperty(String propertyName) {
        return new Property(propertyName);
    }


    /**
     * Returns the property with the given name
     *
     * @return name
     */
    public PropertyData getProperty(String name) {
        return (Property)getElement(name);
    }


    /**
     * returns the object as string
     *
     * @return String which contains all fields of the object
     *
     */
    public String toString( ) {

        String str =  "PropertyGroup " + super.toString();

        return str;
    }

	/**
	 * Returns the number of visible properties in the group.
	 * 
	 * @return number of visible properties
	 */
	public int getNumberOfVisibleProperty() {
		return getNumberOfVisibleElements();
	}	

	
	
// Already by the UIELementGroup 
//	/**
//	 * Clones the property group. <br>
//	 * All including objects will be also cloned.
//	 * 
//	 * @return copy of the object and all sub objects
//	 * 
//	 * @see java.lang.Object#clone()
//	 */
//	public Object clone()  {
//		PropertyGroup group = (PropertyGroup)super.clone();
//
//		// reset the cloned map and list
//		group.elements = new HashMap();
//		group.elementList = new ArrayList();
//			
//		Iterator iter = elementList.iterator();
//		while (iter.hasNext()) {
//			Property oldProperty = (Property)iter.next();
//			group.addProperty((Property)oldProperty.clone());
//		}
//		return group;
//	}

}
