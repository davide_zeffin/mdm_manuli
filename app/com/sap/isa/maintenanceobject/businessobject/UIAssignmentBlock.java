/*****************************************************************************
    Class:        UIAssignmentBlock
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      06.03.2008
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.maintenanceobject.businessobject;

/**
 * <p>The class UIAssignmentBlock represent an UI Group which is comparable with the 
 * assignment block within the CRM WebClient UI. </p>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class UIAssignmentBlock extends UIElementGroup {

}
