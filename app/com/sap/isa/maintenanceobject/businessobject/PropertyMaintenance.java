/*****************************************************************************
    Class:        PropertyMaintenance
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Author:       SAP
    Created:      19.02.20088
    Version:      1.0

    $Revision: #1 $
    $Date: 2008/02/19 $
*****************************************************************************/
package com.sap.isa.maintenanceobject.businessobject;

import com.sap.isa.core.eai.BackendException;
import com.sap.isa.maintenanceobject.backend.boi.MaintenanceObjectBackend;

/**
 * <p>The class PropertyMaintenance handles the maintenance of a {@link Property} object. </p>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class PropertyMaintenance extends MaintenanceObject {

	private MaintenanceObjectBackend backendService;
	
	
	/**
     * Standard constructor
     */
	public PropertyMaintenance() {
	}

	/**
     * get the BackendService, if necessary
     */
	protected MaintenanceObjectBackend getBackendService()
			throws BackendException {
		 if (backendService == null) {
			throw new BackendException("no valid backend is set in DynamicUI object");
		}
		return backendService;
	}
	
}
