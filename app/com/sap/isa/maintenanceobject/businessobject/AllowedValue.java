/*****************************************************************************
    Class:        AllowedValue
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP
    Created:      Januray 2002
    Version:      1.0
*****************************************************************************/

package com.sap.isa.maintenanceobject.businessobject;

import com.sap.isa.maintenanceobject.backend.boi.AllowedValueData;
import com.sap.isa.maintenanceobject.backend.boi.UIElementStatus;

/**
 * <p>Represent a allow value for a property. With allowed values you can restrict
 * the input of the user to reasonable values. At the moment only String properties
 * are supported.</p>
 * <p>The attribute {@link #additionalData} can be used to add arbitrary data to all UI elements.</p> 
 *
 * @author SAP
 * @version 1.0
 *
 * @see com.sap.isa.maintenanceobject.businessobject.Property
 * @see com.sap.isa.maintenanceobject.businessobject.MaintenanceObject
 */
public class AllowedValue implements AllowedValueData, Cloneable {

	/**
	 * <p>Extension hook to add additional data to all UI elements. </p>
	 */
	protected Object additionalData = null;
	
	
    protected String value;
    
    protected String description;
  
    /**
	 * flag if property description text should be retrieved from the resourse key file. <br>
	 */
	protected boolean isRessourceKeyUsed = true;

	/**
	 * <p>Hold the status of a value.</p>
	 */
	protected UIElementStatus status = null; 
   
    /**
     * flag if the value is visible
     */
    protected boolean isHidden = false;


	/**
	 * flag if the value is readOnly
	 */
	protected boolean isReadOnly = false;

    /**
     * Standard Constructor
     *
     */
    public AllowedValue(){
    }


    /**
     * Creates an allowed Value
     *
     *
     * @param value the allowed value
     * @param description teh description for the allowed value
     *
     */
    public AllowedValue(String value, String description) {

        this.value = value;
        this.description = description;
    }

	/**
	 * Return the property {@link #additionalData}. <br> 
	 *
	 * @return Returns the {@link #additionalData}.
	 */
	public Object getAdditionalData() {
		return additionalData;
	}

	/**
	 * Set the property {@link #additionalData}. <br>
	 * 
	 * @param additionalData The {@link #additionalData} to set.
	 */
	public void setAdditionalData(Object additionalData) {
		this.additionalData = additionalData;
	}
 
    

    /**
     * Set the description for an allowed value.
     *
     * @param description
     *
     */
    public void setDescription(String description) {
        this.description = description;
    }


    /**
     * Returns the description for an allowed value
     *
     * @return description
     *
     */
    public String getDescription() {
       return description;
    }

    /**
     * Returns if the value is visible
     * 
     * @return <code>true</code>  if the value is hidden     
     *     and  <code>false</code> if the value is visible
     */
    public boolean isHidden() {
        return isHidden;
    }


    /**
     * Sets the hidden flag. 
     * 
     * @param hidden The hidden flag to be set
     */
    public void setHidden(boolean hidden) {
        this.isHidden = hidden;
    }

    /**
     * Returns true if labels for the property are retrieved from the resource key file.<br> 
     * @return flag if the resource key is used to display the description.
     */
    public boolean isRessourceKeyUsed() {
        return isRessourceKeyUsed;
    }

    /**
     * Set the parameter for isRessourceKeyUsed <br>
     * @param ressourceKeyUsed flag if the resource key is used.
     */
    public void setRessourceKeyUsed(boolean ressourceKeyUsed) {
        isRessourceKeyUsed = ressourceKeyUsed;
    }

    /**
	 * Return the property {@link #status}. <br> 
	 *
	 * @return Returns the {@link #status}.
	 */
	public UIElementStatus getStatus() {
		return status;
	}

	/**
	 * Set the property {@link #status}. <br>
	 * 
	 * @param status The {@link #status} to set.
	 */
	public void setStatus(UIElementStatus status) {
		this.status = status;
	}

	/**
	  * Returns if the value is read only.<br>
	  *
	  * @return <code>true</code> if the value is read only and
	  *         <code>false</code> if the value is read write
	  */
	public boolean isReadOnly(){
		return this.isReadOnly;
	}


	/**
	  * Sets the readOnly flag. <br>
	  *
	  * @param readOnly The readOnly flag to be set
	  */
	public void setReadOnly(boolean readOnly){
		this.isReadOnly = readOnly;
	}


    /**
     * Set the allowed value
     *
     * @param value
     *
     */
    public void setValue(String value) {
        this.value = value;
    }


    /**
     * Returns the allowed value
     *
     * @return value
     *
     */
    public String getValue() {
       return this.value;
    }


    /**
     * returns the object as string
     *
     * @return String which contains all fields of the object
     *
     */
    public String toString( ) {

        String str =  "Value: " + value + ", " +
                      "description: " + description;

        return str;
    }


	/**
	 * Clones the allowed value. <br>
	 * 
	 * @return copy of the object
	 * 
	 * @see java.lang.Object#clone()
	 */
	public Object clone()  {
		try {
			return super.clone();

		}
		catch (CloneNotSupportedException e) {
			return null;
		}
	}

}
