/*****************************************************************************
    Class:        GSProperty
    Copyright (c) 2003, SAP AG, Germany, All rights reserved.

    $Revision: #10 $
    $DateTime: 2004/07/08 14:52:11 $ (Last changed)
    $Change: 195223 $ (changelist)
    
*****************************************************************************/

package com.sap.isa.maintenanceobject.businessobject;


/**
 * The property represent an arbitrary property of a maintenance object (Generic
 * SearchBuilder). It holds all relevant information to display and maintain 
 * a property in an web frontend.
 * Serval properties could be group in property groups.
 *
 * @author SAP
 * @version 1.0
 *
 * @see com.sap.isa.maintenanceobject.businessobject.GSPropertyGroup
 * @see com.sap.isa.maintenanceobject.businessobject.GenericSearchBuilder
 */
public class GSProperty extends Property {
    /**
     * Implemenation Filter of the property
     */
    protected boolean implemenationFilter;
    /**
     * Resultlist Name of the property
     */
    protected String resultlistName;
    /**
     * Columntitle of the property
     */
    protected String columnTitle;
    /**
     * Hyperlink of the property
     */
    protected String hyperlink;
    /**
     * Hyperlink parameter class of the property
     */
    protected String linkParamClass;
    /**
     * Hyperlink parameter class method of the property
     */
    protected String linkParamMethod;
    /**
     * Entity Type of the property
     */
    protected String entityType;
    /**
     * Token Type of the property
     */
    protected String tokenType;
    /**
     * Parmeter Type of the property
     */
    protected String parameterType;
    /**
     * Request Parmeter Name of the property
     */
    protected String requestParameterName;
    /**
     * Name of a Cascading Stylesheet class of the property 
     */
    protected String cssClassName;
    /**
     * Name of a link target frame of the property
     */
    protected String linkTargetFrameName;
    /**
     * Default sorting sequence 
     */
    protected String defaultSortSequence;
    /**
     * Prefix for a translation key
     */
    protected String translationPrefix;
    /**
     * Name of the field output handler class of the property
     */
    protected String fieldOutputHandlerClass;
    /**
     * Name of the field output handler method of the property
     */
    protected String fieldOutputHandlerMethod;
    /**
     * Java Script functions which should be called
     */
    protected String UIJScriptToCall;
    /**
     * Java Script functions which should be called on 'onchange' Event
     */
    protected String UIJScriptOnChange;
    /**
     * Name of the screen area where this property should be placed.<br>
     * Default is <b>1</b>
     */
    protected String screenArea = "1";
    /**
     * Label of the property
     */
    protected String label;
    /**
     * Alignement control for the <code>label</code>
     */
    protected String labelAlignment = "left";
    /**
     * "write under Property" information of the property
     */
    protected String writeUnderProperty = null;
    /**
     * Number of maximum hits to be selected from the database
     */
    protected int maxHitsToSelect = -1;

    /**
     * Standard Constructor
     */
    public GSProperty(){
    	super();
		// Set default values for Generic Search Framework
		setSize(5);
		setMaxLength(5);
    }


    /**
     * Constructor for a property with the given name
     *
     * @param name
     */
    public GSProperty(String name) {
        super(name);
    }


    /**
     * Returns the Implemenation Filter. 
     * 
     * @return ImplemantationFilter
     */
    public boolean isImplemenationFilter() {
        return implemenationFilter;
    }

    /**
     * Set the Implemantion Filter
     * 
     * @param string
     */
    public void setImplemenationFilter(boolean implemantionFilter) {
        this.implemenationFilter = implemantionFilter;
    }

    /**
     * Return the resultlist name of the Property
     * @return ResultlistName
     */
    public String getResultlistName() {
        return resultlistName;
    }

    /**
     * Set the resultlist name of the Property
     * @param string
     */
    public void setResultlistName(String string) {
        resultlistName = string;
    }

    /**
     * Return the column title of the Property
     * @return ColumnTitle
     */
    public String getColumnTitle() {
        return columnTitle;
    }

    /**
     * Set the column title of the Property
     * @param string
     */
    public void setColumnTitle(String string) {
        columnTitle = string;
    }
    /**
     * Return the hyperlink of the Property
     * @return Hyperlink
     */
    public String getHyperlink() {
        return hyperlink;
    }

    /**
     * Set the hyperlink of the Property
     * @param string
     */
    public void setHyperlink(String string) {
        hyperlink = string;
    }

    /**
     * Return the hyperlink parameter class of the Property
     * @return LinkParamClass
     */
    public String getLinkParamClass() {
        return linkParamClass;
    }

    /**
     * Set the hyperlink parameter class of the Property
     * @param string
     */
    public void setLinkParamClass(String string) {
        linkParamClass = string;
    }

    /**
     * Return the Hyperlink parameter method of the Property
     * @return LinkParamMethod
     */
    public String getLinkParamMethod() {
        return linkParamMethod;
    }

    /**
     * Set the Hyperlink parameter method of the Property
     * @param string
     */
    public void setLinkParamMethod(String string) {
        linkParamMethod = string;
    }

    /**
     * Return the Entity Type of the Property
     * @return Entity type
     */
    public String getEntityType() {
        return entityType;
    }

    /**
     * Return the Token Type of the Property
     * @return Token type
     */
    public String getTokenType() {
        return tokenType;
    }

    /**
     * Set the Entity type of the Property
     * @param string
     */
    public void setEntityType(String string) {
        entityType = string;
    }

    /**
     * Set the Token Type of the Property
     * @param string
     */
    public void setTokenType(String string) {
        tokenType = string;
    }

    /**
     * Return the Parameter Type of the Property
     * @return string
     */
    public String getParameterType() {
        return parameterType;
    }

    /**
     * Set the Parameter Type of the Property
     * @param string
     */
    public void setParameterType(String string) {
        parameterType = string;
    }
    /**
     * Return the Request Parameter Name of the Property
     * @return Request parameter Name 
     */
    public String getRequestParameterName() {
        return requestParameterName;
    }

    /**
     * Set the Request Parameter Name of the Property
     * @param string
     */
    public void setRequestParameterName(String string) {
        requestParameterName = string;
    }

    /**
     * Return the object as string
     *
     * @return String which contains all fields of the object
     *
     */
    public String toString( ) {
        StringBuffer strB = new StringBuffer(super.toString());
        
        strB.append(", ColumnTitle: " + columnTitle );
        strB.append(", Hyperlink: " + hyperlink );
        strB.append(", ImplementationFilter: " + implemenationFilter );
        strB.append(", ResultlistName: " + resultlistName );
        strB.append(", LinkParamClass: " + linkParamClass );
        strB.append(", LinkParamMethod: " + linkParamMethod );
        strB.append(", ParameterType: " + parameterType );
        strB.append(", EntityType: " + entityType );
        strB.append(", TokenType: " + tokenType );
        strB.append(", RequestParameterName: " + requestParameterName);
        strB.append(", ResultListName: " + resultlistName);
        strB.append(", DefaultSortSequence: " + defaultSortSequence);
        strB.append(", CssClassName: " + cssClassName);
        strB.append(", LinkTargetFrameName: " + linkTargetFrameName);

        return strB.toString();
    }

    /**
     * Return the Cascading Stylesheet Name of the Property
     * @return String Name of the CSS Name
     */
    public String getCssClassName() {
        return cssClassName;
    }

    /**
     * Return the Target Frame name of the Property
     * @return String Target Frame name
     */
    public String getLinkTargetFrameName() {
        return linkTargetFrameName;
    }

    /**
     * Set the Cascading Stylesheet Name of the Property
     * @param string
     */
    public void setCssClassName(String string) {
        cssClassName = string;
    }

    /**
     * Set the Target Frame name of the Property
     * @param string
     */
    public void setLinkTargetFrameName(String string) {
        linkTargetFrameName = string;
    }

    /**
     * Return the sorting sequence which should be used by default
     * @return String sorting sequence
     */
    public String getDefaultSortSequence() {
        return defaultSortSequence;
    }

    /**
     * Set sorting sequence <b>ASCENDING</b> or <b>DESCENDING</b>
     * @param string
     */
    public void setDefaultSortSequence(String string) {
        defaultSortSequence = string;
    }

    /**
     * Return the prefix for a translation key
     * @return string
     */
    public String getTranslationPrefix() {
        return translationPrefix;
    }

    /**
     * Set prefix for a translation key
     * @param string
     */
    public void setTranslationPrefix(String string) {
        translationPrefix = string;
    }

    /**
     * Return the class for the field output handler
     * @return String
     */
    public String getFieldOutputHandlerClass() {
        return fieldOutputHandlerClass;
    }

    /**
     * Return the method for the field output handler
     * @return String
     */
    public String getFieldOutputHandlerMethod() {
        return fieldOutputHandlerMethod;
    }

    /**
     * Set class for the field output handler
     * @param string
     */
    public void setFieldOutputHandlerClass(String string) {
        fieldOutputHandlerClass = string;
    }

    /**
     * Set method for the field output handler
     * @param string
     */
    public void setFieldOutputHandlerMethod(String string) {
        fieldOutputHandlerMethod = string;
    }

    /**
     * Return the JScript functions to call 
     * @return String JScript functions to call
     */
    public String getUIJScriptToCall() {
        return UIJScriptToCall;
    }

    /**
     * Set the JScript functions to call
     * @param String JScript function to call
     */
    public void setUIJScriptToCall(String string) {
        UIJScriptToCall = string;
    }
    /**
     * Return the JScript functions to call be callde on 'onchange' Event
     * @return String JScript functions to call
     */
    public String getUIJScriptOnChange() {
        return UIJScriptOnChange;
    }

    /**
     * Set the JScript functions to call be called on 'onchange' Event
     * @param String JScript function to call
     */
    public void setUIJScriptOnChange(String string) {
        UIJScriptOnChange = string;
    }

    /**
     * Return the screen area where this property should be placed
     * @return String screen area
     */
    public String getScreenArea() {
        return screenArea;
    }

    /**
     * Set the screen area where this property should be placed
     * @param string screen area
     */
    public void setScreenArea(String string) {
        screenArea = string;
    }

    /**
     * Return the label of this property
     * @return String label
     */
    public String getLabel() {
        return label;
    }

    /**
     * Set the label of this property
     * @param string label
     */
    public void setLabel(String string) {
        label = string;
    }

    /**
     * Get the "write under" formating information
     * @return String "write under" formating information 
     */
    public String getWriteUnderProperty() {
        return writeUnderProperty;
    }

    /**
     * Set the "write under" formating information
     * @param String "write under" formating information
     */
    public void setWriteUnderProperty(String string) {
        writeUnderProperty = string;
    }

    /**
     * Get the number of maximum hits to be selected from the database
     * @return int max hits parameter
     */
    public int getMaxHitsToSelect() {
        return maxHitsToSelect;
    }

    /**
     * Set the number of maximum hits to be selected from the database
     * @param int max hits parameter
     */
    public void setMaxHitsToSelect(int i) {
        maxHitsToSelect = i;
    }
    /**
     * Get the label alignment 
     * @return String label alignement
     */
    public String getLabelAlignment() {
        return labelAlignment;
    }

    /**
     * Set the label alignment
     * @param string label alignment
     */
    public void setLabelAlignment(String string) {
        labelAlignment = string;
    }
}