/*****************************************************************************
    Class:        GSPropertyGroup
    Copyright (c) 2003, SAP AG, Germany, All rights reserved.

    $Revision: #8 $
    $DateTime: 2004/07/21 15:29:47 $ (Last changed)
    $Change: 196763 $ (changelist)
    
*****************************************************************************/

package com.sap.isa.maintenanceobject.businessobject;


/**
 *
 * The property group manages a group of properties, which should maintain in
 * a generic web ui as one group group. For example all properties on a
 * document search screen.
 *
 * @author SAP AG
 * @version 1.0
 */
public class GSPropertyGroup extends PropertyGroup {

    /**
     * Name of the Property carrying the file type 
     * information. 
     */
    protected static String DOCUMENT_TYPES = "document_types";
    /**
     * Search request memory flag of the the Property Group.<br>
     * Default is <b>false</b>.
     */
    protected boolean useSearchRequestMemory = false;
    /**
     * Number of screen areas for the search criteria.<br>
     * Default number is <b>1</b>
     */
    protected int maxScreenAreas = 1;
    /**
     * CSS class which will be added to the &lt;body&gt; tag.<br>
     * Default is <b>document-search</b> 
     */
    protected String cssBodyClassName = "document-search";
    /**
     * Path/Name of the &lt;jsp:include ... which will be placed on the top
     * of the HTML &lt;body&gt;. 
     */
    protected String bodyInclTop = null;
    /**
     * Path/Name of the &lt;jsp:include ... which will be placed on the bottom
     * of the HTML page before the &lt;/body&gt; tag. 
     */
    protected String bodyInclBottom = null;
	/**
	 * Path/Name of the &lt;jsp:include ... which will be placed before the 
	 * result list &lt;table&gt;. 
	 */
	protected String bodyInclBeforeResult = null;
	/**
	 * Path/Name of the &lt;jsp:include ... which will be placed after the 
	 * result list &lt;/table&gt;. 
	 */
	protected String bodyInclAfterResult = null;
    /**
     * Name of the JScript function which will be called at the 'onclick' event.
     * Default is <b>sendRequest();</b>
     */
    protected String UIJScriptOnStartButton = "sendRequest();";
	/**
	 * Declare a result list as a expanded one.
	 * Default is <b>false</b>
	 */
	protected boolean isExpandedResultlist = false;
    /**
     * Name of the target frame.
     */
    protected String linkTargetFrameName = null;
    /**
     * Resource key prefix for message of how many objects have been found 
     * The key will be extended either by<br>
     * - <code>0</code> for no document found<br>
     * - <code>1</code> for exactly 1 document found<br>
     * - <code>n</code> for more than one documents found<br>
     * - <code>evt.s</code> for simple "max hits" event occured<br>
     * - <code>evt.e</code> for enhanced "max hits" event occured<br>
     * Default is <b>gs.result.msg.</b>
     */
    protected String msgKeyObjectFound = "gs.result.msg.";
    /**
     * Standard Constructor
     *
     */
    public GSPropertyGroup(){
    }

    /**
     * Constructor for a property group with the given name
     *
     * @param name name of group
     *
     */
    public GSPropertyGroup(String name) {
        super(name);
    }


    /**
     * returns the object as string
     *
     * @return String which contains all fields of the object
     *
     */
    public String toString( ) {

        String str =  "GSPropertyGroup: " +
                      "useSearchRequestMemory: " + this.useSearchRequestMemory + ", " + 
                      "bodyInclTop" + this.bodyInclTop + ", " +
                      "cssBodyClassName" + this.cssBodyClassName + ", " +
                      "maxScreenAreas" + this.maxScreenAreas + ", " +
                      super.toString();

        return str;
    }
    
    /**
     * Return the search request memory flag
     * @return boolean
     */
    public boolean isUseSearchRequestMemory() {
        return useSearchRequestMemory;
    }

    /**
     * Set the search request memory flag
     * @param string
     */
    public void setUseSearchRequestMemory(boolean bool) {
        useSearchRequestMemory = bool;
    }

    /**
     * Get the maximal number of screen areas for the search criteria
     * @return int max screen areas
     */
    public int getMaxScreenAreas() {
        return maxScreenAreas;
    }

    /**
     * Set the maximal number of screen areas for the search criteria
     * @param int max screen areas
     */
    public void setMaxScreenAreas(int i) {
        maxScreenAreas = i;
    }

    /**
     * Return the CSS class name for the &lt;body&gt; tag
     * @return String css body class name
     */
    public String getCssBodyClassName() {
        return cssBodyClassName;
    }

    /**
     * Set the CSS class name for the &lt;body&gt; tag
     * @param string
     */
    public void setCssBodyClassName(String string) {
        cssBodyClassName = string;
    }
    /**
     * Get the path/name of the &lt;jsp:include
     * @return String path/name
     */
    public String getBodyInclTop() {
        return bodyInclTop;
    }

    /**
     * Set the path/name of the &lt;jsp:include
     * @param string path/name
     */
    public void setBodyInclTop(String string) {
        bodyInclTop = string;
    }

    /**
     * Get the path/name of the &lt;jsp:include
     * @return String path/name
     */
    public String getBodyInclBottom() {
        return bodyInclBottom;
    }

    /**
     * Set the path/name of the &lt;jsp:include
     * @param string
     */
    public void setBodyInclBottom(String string) {
        bodyInclBottom = string;
    }

    /**
     * Return the name of the JScript function which will be called
     * at the 'onclick' event.
     * @return String name
     */
    public String getUIJScriptOnStartButton() {
        return UIJScriptOnStartButton;
    }

    /**
     * Set the name of the JScript function which will be called at
     * the 'onclick' event.
     * @param string name
     */
    public void setUIJScriptOnStartButton(String string) {
        UIJScriptOnStartButton = string;
    }

    /**
     * Return the Target Frame name.
     * @return String Target Frame name
     */
    public String getLinkTargetFrameName() {
        return linkTargetFrameName;
    }

    /**
     * Set the Target Frame name of the Property
     * @param string
     */
    public void setLinkTargetFrameName(String string) {
        linkTargetFrameName = string;
    }
    /**
     * Return the resource key prefix for message of how many objects have been found. 
     * @return String resource key prefix
     */
    public String getMsgKeyObjectFound() {
        return msgKeyObjectFound;
    }

    /**
     * Set the resource key prefix for message of how many objects have been found.
     * @param String resource key prefix
     */
    public void setMsgKeyObjectFound(String string) {
        msgKeyObjectFound = string;
    }
	/**
	 * Get the path/name of the &lt;jsp:include
	 * @return
	 */
	public String getBodyInclAfterResult() {
		return bodyInclAfterResult;
	}

	/**
	 * Get the path/name of the &lt;jsp:include
	 * @return
	 */
	public String getBodyInclBeforeResult() {
		return bodyInclBeforeResult;
	}

	/**
	 * Set the path/name of the &lt;jsp:include
	 * @param string
	 */
	public void setBodyInclAfterResult(String string) {
		bodyInclAfterResult = string;
	}

	/**
	 * Set the path/name of the &lt;jsp:include
	 * @param string
	 */
	public void setBodyInclBeforeResult(String string) {
		bodyInclBeforeResult = string;
	}

	/**
	 * Returns whether the result list is a expaned one or not
	 * @return true or false
	 */
	public boolean isExpandedResultlist() {
		return isExpandedResultlist;
	}

	/**
	 * Set the flag whether the result list should be considered as expanded one or not.
	 * @param Flag
	 */
	public void setExpandedResultlist(boolean b) {
		isExpandedResultlist = b;
	}

}