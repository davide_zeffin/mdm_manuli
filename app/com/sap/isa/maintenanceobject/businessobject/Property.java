/*****************************************************************************
    Class:        Property
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      Januray 2002
    Version:      1.0

    $Revision: #7 $
    $Date: 2001/07/25 $
*****************************************************************************/

package com.sap.isa.maintenanceobject.businessobject;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.maintenanceobject.backend.boi.AllowedValueData;
import com.sap.isa.maintenanceobject.backend.boi.PropertyData;
import com.sap.isa.maintenanceobject.backend.boi.VisibilityConditionData;
import com.sap.isa.maintenanceobject.ui.renderer.UIRendererManager;

/**
 * The property represent an arbitrary property of a maintenance object.
 * It holds all relevant information to display and maintain a property in
 * an web frontend.
 * Serval properties could be group in property groups.
 *
 * @author SAP
 * @version 1.0
 *
 * @see com.sap.isa.maintenanceobject.businessobject.PropertyGroup
 * @see com.sap.isa.maintenanceobject.businessobject.MaintenanceObject
 */
public class Property extends UIElement implements Cloneable, PropertyData {

    /**
     * type of the property
     */
    private String type = TYPE_STRING;  

    /**
     * description for the valaue of the property
     */
    protected String valueDescription = "";

    /**
     * Name of a method to read helpvalues for the property. <br>
     */
    protected String helpValuesMethod;

    /**
     * Name of a help value search. <br> 
     */
    protected String helpValuesSearch = "";
       
    /**
     * <p>Name of the field in the help value search. </p> 
     */
    protected String helpValuesFieldName = "";

    /**
     * <p>Request Parmeter Name of the property. </p>
     */
    protected String requestParameterName;

    /**
     * <p>flag if the value has changed. </p>
     */
    protected boolean isValueChanged = false;
    
    /**
     * This switch controls if the property could be syncronized with a 
     * corresponding bean property. 
     */
    protected boolean beanAvailable = true;

    /**
     * This switch controls if the property is an input field.
     */
    protected boolean isReadOnly = false;
    

    /**
     * flag if property is exitField
     */
    protected boolean isExitField;

    /**
     * flag if property is exitField
     */
    protected boolean isHelpAvailable;

    /**
     * flag if property is required
     */
    protected boolean isRequired;


    /**
     * the value of the property.<br> 
     * At the moment the following types are supported
     * (UI) idents type which are directly supported from the user interface
     *
     * <ul>
     * <li> String (UI)</li>
     * <li> Boolean (UI)</li>
     * <li> Integer (UI)</li>
     * <li> Enumneration </li>
     * <li> Table </li>
     * </ul>
     *
     */
    protected Object value = new String("");

    /**
     * Default Value for a property
     */
    protected Object defaultValue = new String("");

    /**
     * list with allowed values
     */
    protected List allowedValues = new ArrayList();

    /**
     * list with help descriptions
     */
    protected List helpDescription = new ArrayList();

    // Size used to display the property
    private int size = 30;

    // max length of the property;
    private int maxLength = 0;

    private boolean isSimpleType = true;

    /**
     * List with condition whic must be true, that the property is visible
     */
    protected List visibilityConditions = new ArrayList();
    
    /**
     * Input pattern for a property.
     */
    protected String inputPattern = "";
    
    /**
     * Height for a input field. <br>
     * <p>It can be used in different ways for differnt parts 
     * heigh in pixel or visible rows.</p>
     */
    protected String height = "";

    /**
     * Default JavaScript function for exit field.
     */
    protected String exitFieldFunction = "submitForm()";
	/** 
	  *  The IsaLocation of the current Action.
	  *  This will be instantiated during perform and is always present.
	  */
	 protected IsaLocation log = IsaLocation.getInstance(Property.class.getName());
	 
	/** 
	  *  Default Date Format.
	  */	 
	 private String defaultDateFormat;
	 
    /**
     * Input interval for a property.
     */
    protected String inputInterval = "";	 

    /**
     * Language dependent formatted String for a property.
     */
    protected String formattedString = "";
    
	/**
     *
     * Standard Constructor
     *
     */
    public Property() {
    	super();
    }

    /**
     * Constructor for a property with the given name
     *
     * @param name
     *
     */
    public Property(String name) {
        super(name);
    }

    /**
	 * <p>Return the property {@link #allowedValues}. </p> 
	 *
	 * @return Returns the {@link #allowedValues}.
	 */
	public List getAllowedValues() {
		return allowedValues;
	}

	/**
	 * <p>Set the property {@link #allowedValues}. </p>
	 * 
	 * @param allowedValues The {@link #allowedValues} to set.
	 */
	public void setAllowedValues(List allowedValues) {
		this.allowedValues = allowedValues;
	}

	/**
     * Set the description for the value
     *
     * @param valueDescription
     *
     */
    public void setValueDescription(String valueDescription) {
        this.valueDescription = valueDescription;
    }

    /**
     * Returns the description of the value
     *
     * @return valueDescription
     *
     */
    public String getValueDescription() {
        return valueDescription;
    }

    
    /**
     * Return the property {@link #beanAvailable}. <br>
     * 
     * @return {@link #beanAvailable}
     */
    public boolean isBeanAvailable() {
        return beanAvailable;
    }

    /**
     * Return Set the property {@link #beanAvailable}. <br>
     * 
     * @param beanAvailable {@link #beanAvailable}
     */
    public void setBeanAvailable(boolean beanAvailable) {
        this.beanAvailable = beanAvailable;
    }

    /**
     * Set the property {@link #exitField}. <br>
     *
     * @param {@link #exitField}
     *
     */
    public void setExitField(boolean exitField) {
        this.isExitField = exitField;
    }

    /**
     * Returns the property {@link #exitField}. <br>
     *
     * @return {@link #exitField}
     *
     */
    public boolean isExitField() {
        return isExitField;
    }

     /**
     * Returns if a help description is available for this property.
     * @return boolean
     */
    public boolean isHelpAvailable() {
        return isHelpAvailable;
    }

    /**
     * Sets if a help description is available for this property.
     * @param helpAvailable The helpAvailable to set
     */
    public void setHelpAvailable(boolean helpAvailable) {
        isHelpAvailable = helpAvailable;
    }

    /**
     * Returns a list with the help descriptions
     * 
     * @return list to iterate over help lines
     */
    public List getHelpDescription() {

        return helpDescription;
    }

    /**
     * Returns a list with the visibiliy conditions
     * 
     * @return list to iterate over visibility conditions
     */
    public List getvisibilityConditions() {

        return visibilityConditions;
    }

    /**
     * Set the method, which provide the help values from the backend.
     * The method must exit within the backend object and must have the
     * following signature
     * <code>public HelpValues methodName(MaintenanceObjectData maintenanceObject,
     *       PropertyData property);</code>.
     *
     * @param helpValuesMethod name of the method
     *
     */
    public void setHelpValuesMethod(String helpValuesMethod) {
        this.helpValuesMethod = helpValuesMethod;
    }

    /**
     * Returns the name of the method, which provide the help values from the backend.
     * The method must exit within the backend object and must have the
     * following signature
     * <code>public HelpValues methodName(MaintenanceObjectData maintenanceObject,
     *       PropertyData property);</code>.
     *
     * @return helpValuesMethod name of the method
     *
     */
    public String getHelpValuesMethod() {
        return this.helpValuesMethod;
    }

    /**
     * Return the name of a help values search for the property. <br>
     * 
     * @return name of the help value search.
     */
    public String getHelpValuesSearch() {
        return helpValuesSearch;
    }

    /**
     * Set the name of a help value search to use for the property . <br>
     * 
     * @param helpValuesSearch
     */
    public void setHelpValuesSearch(String helpValuesSearch) {
        this.helpValuesSearch = helpValuesSearch;
    }

	/**
	 * Return the property {@link #isReadOnly}. <br> 
	 *
	 * @return Returns the {@link #isReadOnly}.
	 */
	public boolean isReadOnly() {
		return isReadOnly;
	}
	/**
	 * Set the property {@link #isReadOnly}. <br>
	 * 
	 * @param isReadOnly The {@link #isReadOnly} to set.
	 */
	public void setReadOnly(boolean isReadOnly) {
		this.isReadOnly = isReadOnly;
	}

	/**
	 * <p>Return the property {@link #requestParameterName}. </p>
	 * <p><strong>Note:</strong> If the property isn't maintained the name of the property is used 
	 * automatically</p> 
	 *
	 * @return Returns the {@link #requestParameterName}.
	 */
	public String getRequestParameterName() {
		if (requestParameterName != null && requestParameterName.length()>0) {
			return requestParameterName;
		}
		return name;
	}

	/**
	 * <p>Set the property {@link #requestParameterName}. </p>
	 * 
	 * @param requestParameterName The {@link #requestParameterName} to set.
	 */
	public void setRequestParameterName(String requestParameterName) {
		this.requestParameterName = requestParameterName;
	}

	/**
     * Returns the maxLength.
     * @return int
     */
    public int getMaxLength() {
        return maxLength;
    }

    /**
     * Sets the maxLength.
     * @param maxLength The maxLength to set
     */
    public void setMaxLength(int maxLength) {
        this.maxLength = maxLength;
        if (size > maxLength && maxLength > 0) {
            size = maxLength;
        }
    }

    /**
     * Set the property type
     *
     * @param type
     *
     */
    public void setType(String type) {
        this.type = type;
        if (type.equals(TYPE_ENUMERATION) || type.equals(TYPE_TABLE)) {
            isSimpleType = false;
        }
    }

    /**
     * Returns the property type
     *
     * @return type
     *
     */
    public String getType() {
        return this.type;
    }

    /**
     * Set the property required
     *
     * @param required
     *
     */
    public void setRequired(boolean required) {
        this.isRequired = required;
    }

    /**
     * Returns the property required
     *
     * @return required
     *
     */
    public boolean isRequired() {
        return this.isRequired;
    }

    /**
     * Returns the size.
     * @return int
     */
    public int getSize() {
        return size;
    }

    /**
     * Sets the size.
     * @param size The size to set
     */
    public void setSize(int size) {
        this.size = size;
    }

    /**
     * Set the property value
     *
     * @param value
     *
     */
    public void setValue(Object value) {
        this.value = value;
    }

    /**
     * Returns the property value
     *
     * @return value
     *
     */
    public Object getValue() {
        return this.value;
    }

    
    /**
	 * <p>Return the property {@link #isValueChanged}. </p> 
	 *
	 * @return Returns the {@link #isValueChanged}.
	 */
	public boolean isValueChanged() {
		return isValueChanged;
	}

	
	/**
	 * <p>Set the property {@link #isValueChanged}. </p>
	 * 
	 * @param isValueChanged The {@link #isValueChanged} to set.
	 */
	public void setValueChanged(boolean isValueChanged) {
		this.isValueChanged = isValueChanged;
	}


    /**
     * <p>Set the property value. </p>
     *
     * @param value
     */
    public void setValue(String value) {
    	if (type != null && 
    		type.equals(TYPE_LIST)) {
    		List list = getList();
    		list.add(value);
    	}
    	else {
    		this.value = value;
    	}	
    }

    /**
     * Clears the value of the property.
     * If default value exists, it will be used.
     *
     */
    public void clearValue() {

        if (value instanceof String) {
            value = defaultValue;
        }
        else if (value instanceof Boolean) {
            if (defaultValue instanceof String) {
                value = new Boolean((String) defaultValue);
            }
            else if (defaultValue instanceof Boolean) {
                value = defaultValue;
            }
            else {
                value = new Boolean("false");
            }
        }
        else if (value instanceof Integer) {
            if (defaultValue instanceof String) {
                if (defaultValue.toString().trim().length() == 0) {
                    defaultValue = "0";
                }
                value = new Integer((String) defaultValue);
            }
            else if (defaultValue instanceof Integer) {
                value = defaultValue;
            }
            else {
                value = new Integer("0");
            }
        }
        else {
            value = defaultValue;
        }
        valueDescription = "";
    }

    /**
     * Returns the value as String
     *
     * @return value
     *
     */
    public String getString() {
    	if (getFormattedString() != null && getFormattedString().length() > 0) {
    		return formattedString;
    	}
        if (value == null) {
            return "";
        }
        if (value instanceof String) {
            return (String) value;
        }
        return value.toString();
    }

    /**
     * Set the property value
     *
     * @param value
     *
     */
    public void setValue(boolean value) {
        this.value = new Boolean(value);
    }

    /**
     * Set the value to the value of corresponding bean property in the 
     * given bean. <br>
     *
     * @param bean reference to the besan object.
     *
     */
    public void setValueFromBean(Object bean) {

        if (beanAvailable) {
            try {
                PropertyDescriptor propertyDescriptor = new PropertyDescriptor(getName(), bean.getClass());

                Method method = propertyDescriptor.getReadMethod();

                Object valueObject = method.invoke(bean, null);

                setValue(valueObject);

            }
            catch (Exception ex) {
            	log.debug(ex.getMessage());
            }
        }
    }

    /**
     * Set the value of the corresponding bean property in the given bean object
     * with the current value of the property. <br>
     *
     * @param bean reference to the besan object.
     *
     */
    public void setBeanPropertyValue(Object bean) {

        if (beanAvailable) {
            try {
                PropertyDescriptor propertyDescriptor = new PropertyDescriptor(getName(), bean.getClass());

                Method method = propertyDescriptor.getWriteMethod();

                method.invoke(bean, new Object[] { getValue()});

            }
            catch (Exception ex) {
				log.debug(ex.getMessage());
            }
        }
    }

    /**
     * Returns the value as boolean
     *
     * @return value
     *
     */
    public boolean getBoolean() {

        if (value != null && value instanceof Boolean) {
            return ((Boolean) value).booleanValue();
        }
        return false;
    }

    /**
     * Returns the value as integer
     * 
     * @return value
     */
    public int getInteger() {

        if (value != null && value instanceof Integer) {
            return ((Integer) value).intValue();
        }
        return 0;
    }

    /**
     * Set the property value
     * 
     * @param value
     */
    public void setValue(int value) {
        this.value = new Integer(value);
    }

    /**
     * Set the property value
     *
     * @param value
     *
     */
    public void setValue(Enumeration value) {
        this.value = value;
    }

	/**
	 *  Set the property {@link #values}. <br>
	 * 
	 * @param list The values to set.
	 */
	public void setValue(List list) {
		this.value = list;
	}

    
    /**
     * Returns the value as Enumeration
     *
     * @return value
     */
    public Enumeration getEnumeration() {
        return (Enumeration) value;
    }

    /**
     * Set the property value
     *
     * @param value
     *
     */
    public void setValue(Table value) {
        this.value = value;
    }

    /**
     * Returns the property value
     *
     * @return value
     *
     */
    public Table getTable() {
        return (Table) this.value;
    }

    /**
     * Returns the property value as a list. <br>
     * 
     * @return value as a list object
     */
    public List getList() {
    	List list = null;
    	if (type.equals(PropertyData.TYPE_LIST)) {
    		if (value == null || !(value instanceof List)) {
    			value = new ArrayList();
    		}	
    		list = (List)value;
    	}
    	return list;
    }

    
    /**
     * Set the default value of the property
     *
     * @param value
     */
    public void setDefaultValue(Object defaultValue) {
        this.defaultValue = defaultValue;
    }

    /**
     * Returns the default value of the property
     *
     * @return value
     */
    public Object getDefaultValue() {
        return defaultValue;
    }

    /**
     * Add an allowed Value to the property
     *
     * @param the allowedValue
     *
     */
    public void addAllowedValue(AllowedValueData allowedValue) {

        if (allowedValue instanceof AllowedValue) {
            allowedValues.add(allowedValue);
        }

    }

    /**
     * Add an allowed Value to the property
     *
     *
     * @param value allowed value
     * @param description for the allowed value
     *
     */
    public void addAllowedValue(String value, String description) {

        AllowedValue allowedValue = new AllowedValue(value, description);
        allowedValues.add(allowedValue);

    }

    /**
     * Add a help description to the property
     *
     * @param the help description
     *
     */
    public void addHelpDescription(String helpDescription) {

        this.helpDescription.add(helpDescription);
    }

    /**
     * Add a visibility condition Value to the property
     *
     * @param the allowedValue
     *
     */
    public void addVisibilityCondition(VisibilityConditionData visibilityCondition) {

        if (visibilityCondition instanceof VisibilityCondition) {
            visibilityConditions.add(visibilityCondition);
        }

    }

    /**
     * Return the number of the allowed values for this property
     *
     * @return number
     *
     */
    public int countAllowedValues() {

        return allowedValues.size();
    }

    /**
     * Returns an iterator over the allowed values for the property
     *
     * @return iterator to iterate over the allowed Values
     */
    public Iterator iterator() {
        return allowedValues.iterator();
    }

    /**
     *
     * returns the object as string
     *
     * @return String which contains all fields of the object
     *
     */
    public String toString() {

        String str =
            "property: "
                + name
                + ", "
                + "type: "
                + type
                + ", "
                + "description: "
                + description
                + ", "
                + "helpValuesMethod: "
                + helpValuesMethod
                + ", "
                + "value: "
                + getString()
                + ", "
                + "inputPattern: "
                + inputPattern
                + ", "
                + "height: "
                + height;

        return str;
    }

    /**
     * Clones the property. <br>
     * All including objects will be also cloned.
     * 
     * @return copy of the object and all sub objects
     * 
     * @see java.lang.Object#clone()
     */
    public Object clone() {
        try {
            Property newProperty = (Property) super.clone();
 
            newProperty.allowedValues = new ArrayList(allowedValues.size());

            for (Iterator iter = allowedValues.iterator(); iter.hasNext();) {
                newProperty.allowedValues.add(((AllowedValue) iter.next()).clone());
            }

            newProperty.visibilityConditions = new ArrayList(visibilityConditions.size());

            for (Iterator iter = visibilityConditions.iterator(); iter.hasNext();) {
                newProperty.visibilityConditions.add(((VisibilityCondition) iter.next()).clone());
            }

            newProperty.helpDescription = new ArrayList(helpDescription.size());

            for (Iterator iter = helpDescription.iterator(); iter.hasNext();) {
                newProperty.helpDescription.add(new String(iter.next().toString()));
            }

            return newProperty;
        }
        catch (CloneNotSupportedException e) {
            return null;
        }
    }

    /**
     * Returns true for simple datatype like String, boolean and int
     */
    public boolean isSimpleType() {

        return isSimpleType;
    }


    public String getHelpValuesFieldName() {
        return helpValuesFieldName;
    }

    public void setHelpValuesFieldName(String helpValuesFieldName) {
        this.helpValuesFieldName = helpValuesFieldName;
    }

	
	public String getUiType() {
		if (uiType == null || uiType.length() <= 0) {
			determineUiType();
		}

		return uiType;
	}
	
	protected void determineUiType() {
		if (this.getType().equals(TYPE_STRING) || this.getType().equals(TYPE_INTEGER)) {
			if (this.countAllowedValues() == 0) {
				setUiType(UIRendererManager.UI_TYPE_INPUT);
			}
			else if (this.countAllowedValues() < 4) {
				setUiType(UIRendererManager.UI_TYPE_RADIO_BUTTON);
			}
			else {
				setUiType(UIRendererManager.UI_TYPE_DDLB);
			}
		}
		else if (this.getType().equals(TYPE_BOOLEAN)) {
			setUiType(UIRendererManager.UI_TYPE_CHECKBOX);
		}
		else if (this.getType().equals(TYPE_LIST)) {
			if (this.countAllowedValues() < 4) {
				setUiType(UIRendererManager.UI_TYPE_CHECKBOX_LIST);
			}
			else {
			    setUiType(UIRendererManager.UI_TYPE_DDLB_LIST);
			}
		}
		
	}

	/**
	 * 
	 * Return the property {@link #inputPattern}. <br>
	 * 
	 * @return inputPattern
	 */
	public String getInputPattern() {
		return inputPattern;
	}

	/**
	 *  Set the property {@link #inputPattern}. <br> 
	 *  
	 * @param inputPattern The inputPattern to set.
	 */
	public void setInputPattern(String inputPattern) {
		this.inputPattern = inputPattern;
	}

	/**
	 * 
	 * Return the property {@link #height}. <br>
	 * 
	 * @return height
	 */
	public String getHeight() {
		return height;
	}

	/**
	 *  Set the property {@link #height}. <br>
	 * 
	 * @param height The height to set.
	 */
	public void setHeight(String height) {
		this.height = height;
	}

	/**
	 * 
	 * Return the property {@link #exitFieldFunction}. <br>
	 * 
	 * @return exitFieldFunction
	 */
	public String getExitFieldFunction() {
		return exitFieldFunction;
	}

	/**
	 *  Set the property {@link #exitFieldFunction}. <br>
	 * 
	 * @param exitFieldFunction The exitFieldFunction to set.
	 */
	public void setExitFieldFunction(String exitFieldFunction) {
		this.exitFieldFunction = exitFieldFunction;
	}

	/**
	 * Returns the default date format. <br>
	 * 
	 * @return defaultDateFormat
	 */
	public String getDefaultDateFormat() {
		return this.defaultDateFormat;
	}
    
    
	/**
	 * Set the default date format. <br>
	 * 
	 * @param dateFormat
	 */
	public void setDefaultDateFormat(String dateFormat) {
		this.defaultDateFormat = dateFormat;
	}

	/**
	 * 
	 * Return the property {@link #inputInterval}. <br>
	 * 
	 * @return inputInterval
	 */
	public String getInputInterval() {
		return inputInterval;
	}

	/**
	 *  Set the property {@link #inputInterval}. <br>
	 * 
	 * @param inputInterval The inputInterval to set.
	 */
	public void setInputInterval(String inputInterval) {
		this.inputInterval = inputInterval;
	}

	/**
	 * 
	 * Return the property {@link #formattedString}. <br>
	 * 
	 * @return formattedString
	 */
	public String getFormattedString() {
		return formattedString;
	}

	/**
	 *  Set the property {@link #formattedString}. <br>
	 * 
	 * @param formattedString The formattedString to set.
	 */
	public void setFormattedString(String formattedString) {
		this.formattedString = formattedString;
	}

}
