/*****************************************************************************
    Class:        DynamicUI
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Author:       SAP
    Created:      February 2008
    Version:      1.0

 *****************************************************************************/

package com.sap.isa.maintenanceobject.businessobject;


import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.businessobject.BusinessObjectHelper;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.util.GenericFactory;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.MessageList;
import com.sap.isa.maintenanceobject.backend.boi.DynamicUIBackend;
import com.sap.isa.maintenanceobject.backend.boi.DynamicUIData;
import com.sap.isa.maintenanceobject.backend.boi.DynamicUIExitData;
import com.sap.isa.maintenanceobject.backend.boi.DynamicUIMaintenanceExitData;
import com.sap.isa.maintenanceobject.backend.boi.DynamicUIProviderData;
import com.sap.isa.maintenanceobject.backend.boi.PropertyData;
import com.sap.isa.maintenanceobject.backend.boi.UIElementBaseData;
import com.sap.isa.maintenanceobject.backend.boi.UIElementGroupData;
import com.sap.isa.maintenanceobject.ui.renderer.UIRendererManager;
import com.sap.isa.maintenanceobject.util.HelpDescriptions;

/**
 * <p>The class DynamicUI includes all data to build a generic web ui. </p>
 * <p>Beyond the DynamicUI allows to edit the UI model.</p>
 *
 * @author  SAP AG
 * @version 1.0
 */
abstract public class DynamicUI extends BusinessObjectBase
        implements Cloneable, DynamicUIData {


	protected DynamicUIProviderData uiProvider;
	
	/**
	 * <p>page which is currently active</p>
	 */
    protected String activePage = "";
    
    protected boolean isPropertyKeyForMessagesUsed = true;
    
    /**
     * <p>flag that the complete UI is read only </p>
     */
    protected boolean isReadOnly = false;
    
    /**
     * 
     */
    protected UIRendererManager uiRendererManager;
    
    /**
     * <p>element which is currenty on focus</p>
     */
    protected String currentElementOnFocus;
    
    
    private int requestParameterNameCounter = 1;

	/**
	 * <p>highest page which was rouched by the user</p>
	 */
     private String wizardPage = "";
	
	/**
	 * <p>indicates if the wizardMode is set</p>
	 */	
	private boolean wizardMode = false;

    /**
     * <p>The edit Modus allows an inline editing of the UI object.</p>
     */
    protected boolean isEditable = false; 
 
    /**
     * Returns the screen group with the given name
     *
     * @return screen group
     *
     */
    public UIElementGroup getPage(String pageName) {

        return (UIElementGroup)uiProvider.getPageGroup().getElement(pageName);
    }

    
    /**
     * @return
     */
    public DynamicUIExitData getDynamicUIExitData(){
    	return uiProvider.getDynamicUIExitData();
    }

    
    /**
     * <p> The method getElementGroup return the list of the element groups with the given name </p>
     * 
     * @param groupName name of the ui element group.
     * @return List with element groups
     */
    public List getElementGroupList(String groupName) {

        return uiProvider.getElementGroupList(groupName);
    }


    /**
     * <p> The method getElementGroup returns the element group with the given key </p>
     * 
     * @param key key of the ui element group.
     * @return the found uielement group.
     */
    public UIElementGroupData getElementGroup(TechKey key) {

        return uiProvider.getElementGroup(key);
    }

    
	/**
	 * Return the list of property with the given name. <br>
	 * 
	 * @param elementName name of the element
	 * @return the list of UI elements {@link PropertyData} with the given element name. 
	 */
    public List getPropertyList(String elementName) {

    	return uiProvider.getPropertyList(elementName);
    }

	/**
     * <p>Returns the parent group for the given key.</p>   
     * 
     * @param key key of the UI Element.
     * @return found element or <code>null</code>.
     */
    public UIElementGroupData getParent(TechKey key) {
    	return uiProvider.getParent(key);
    }

    /**
     * Return a UI element group which contains all pages of the dynamic UI object. <br>
     * 
     * @return UI element group with all pages.
	 * @see com.sap.isa.maintenanceobject.backend.boi.DynamicUIProviderData#getPageGroup()
	 */
	public UIElementGroupData getPageGroup() {
		return uiProvider.getPageGroup();
	}


	/**
	 * Return the property with the given key. <br>
	 * 
	 * @param key key of the property.
	 * @return the UI element with the given element name. 
	 */
    public PropertyData getProperty(TechKey key) {

    	return uiProvider.getProperty(key);
    	
    }
    
    
	/**
	 * Returns the active page.
	 * 
	 * @return String
	 */
	public UIElementGroup getActivePage() {
		return getPage(activePage);
	}


	/**
	 * Returns the name of active screen group.
	 * 
	 * @return String
	 */
	public String getActivePageName() {
		return activePage;
	}

	/**
	 * Sets the active page.
	 * @param activeScreenGroup The name of the active screen group to set
	 */
	public void setActivePageName(String activePage) {
		this.activePage = activePage;
	}


	/**
	 * <p>Return the property {@link #isEditable}. </p> 
	 *
	 * @return Returns the {@link #isEditable}.
	 */
	public boolean isEditable() {
		return isEditable;
	}


	/**
	 * <p>Set the property {@link #isEditable}. </p>
	 * 
	 * @param isEditable The {@link #isEditable} to set.
	 */
	public void setEditable(boolean isEditable) {
		this.isEditable = isEditable;
	}


	/**
	 * Return the property {@link #isReadOnly}. <br> 
	 *
	 * @return Returns the {@link #isReadOnly}.
	 */
	public boolean isReadOnly() {
		return isReadOnly;
	}


	/**
	 * Set the property {@link #isReadOnly}. <br>
	 * 
	 * @param isReadOnly The {@link #isReadOnly} to set.
	 */
	public void setReadOnly(boolean isReadOnly) {
		this.isReadOnly = isReadOnly;
	}


	/**
	 * Return the property {@link #isPropertyKeyForMessagesUsed}. <br> 
	 *
	 * @return Returns the {@link #isPropertyKeyForMessagesUsed}.
	 */
	public boolean isPropertyKeyForMessagesUsed() {
		return isPropertyKeyForMessagesUsed;
	}

	/**
	 * Check if the properties in the messages are visible. <br> 
	 * If not, the property field in the message will be deleted.  
	 *
	 **/
	public void checkObject() {

		MessageList messageList = getMessageList();
		Map messageMap = buildMessageMap(messageList);
		
		Iterator propertyIterator = propertyIterator();
		while (propertyIterator.hasNext()) {
			PropertyData property = (PropertyData) propertyIterator.next();
			checkPropertyMessage(messageMap, property);	
			if (property instanceof Property) {
				checkVisibilityDependencies((Property)property);
			}
			if (property.getName().equals(property.getRequestParameterName())) {
				property.setRequestParameterName("fw-rp-el-"+requestParameterNameCounter);
				requestParameterNameCounter++;
			}
		}

		removePropertyFromMessages(messageMap);
		
	}

	
	/**
	 * Check if the properties in the messages are visible. <br> 
	 * If not, the property field in the message will be deleted.  
	 *
	 **/
	public void generateRequestParameterNames() {

		Iterator propertyIterator = propertyIterator();
		while (propertyIterator.hasNext()) {
			PropertyData property = (PropertyData) propertyIterator.next();
			if (property.getName().equals(property.getRequestParameterName())) {
				property.setRequestParameterName("fw-rp-el-"+requestParameterNameCounter);
				requestParameterNameCounter++;
			}
		}
	
	}



	/**
	 * Check the visibility conditions of all properties. <br>
	 * <strong>Note:</strong> Properties, which have dependencies will be 
	 * unhidden before the check.
	 */
	public void checkVisibilityDependencies() {

		Iterator iter = propertyIterator();
		
		while (iter.hasNext()) {
			Property property = (Property) iter.next();
            
			checkVisibilityDependencies(property);
		}	
	}


	/**
	 * Check the visibility conditions for a properties. <br>
	 * <strong>Note:</strong> Properties, which have dependencies will be 
	 * unhidden before the check.<br>
	 * @param property Propertey to be checked.
	 */
	protected void checkVisibilityDependencies(Property property) {
	
		List conditions = property.getvisibilityConditions();
		
		int size = conditions.size();
		
		if (size > 0) {
			// unhide the property, only when visibility conditions exists
			property.setHidden(false);
		
			for (int i = 0; i < size; i++) {
		    	
				if (!checkCondition((VisibilityCondition)conditions.get(i))) {
					property.setHidden(true);
		    		break;
				}    
			}
		}
	}


	/**
	 * Set isHidden flag of all property groups, depending the
	 * corresponding properties.
	 * 
	 */ 
	public void determineVisibility() {
		
		checkVisibilityDependencies();
		
		Iterator iter = uiProvider.getPageGroup().iterator();
		
		while (iter.hasNext()) {
			UIElementGroupData page = (UIElementGroupData) iter.next();
			page.determineVisibility();
		}	
	}	

	
    /**
     * Add a element related message to messagelist. </br> 
     *
     * @param element element to which the message belongs
     * @param message message to add
     */
	public void addMessage(UIElementBase element, Message message) {
		super.addMessage(message);
		if (message != null && element != null) {
			if (isPropertyKeyForMessagesUsed) {
				message.setProperty(element.getTechKey().getIdAsString());
			}
			else {
				message.setProperty(element.getName());
			}
		}
	}


	/**
	 * Check if the properties in the messages are visible. <br> 
	 * If not, the property field in the message will be deleted.  
	 *
	 **/
	public void checkMessagesProperties() {

		MessageList messageList = getMessageList();
		Map messageMap = buildMessageMap(messageList);
		
		Iterator propertyIterator = propertyIterator();
		while (propertyIterator.hasNext()) {
			PropertyData property = (PropertyData) propertyIterator.next();
			checkPropertyMessage(messageMap, property);	
		}

		removePropertyFromMessages(messageMap);
		
	}


	/**
	 * Remove the properties from all given messages<br>
	 * 
	 * @param messageMap map with all messages
	 */
	protected void removePropertyFromMessages(Map messageMap) {
		// remove all other message for which no property is found at all.
		Iterator iter = messageMap.values().iterator();
		while (iter.hasNext()) {
			Message message = (Message) iter.next();
			message.setProperty("");
		}
	}


	/**
	 * Build a map with messages. The map key is the property name <br>
	 * 
	 * @param messageList message list with message
	 * @return map with message arranged by message property
	 */
	protected Map buildMessageMap(MessageList messageList) {
		Map messageMap = new HashMap();
		
		// fill a message map for faster access.
		Iterator iter = messageList.iterator();
		while (iter.hasNext()) {
			Message message = (Message) iter.next();
			String property = message.getProperty();
			if (property!= null && property.length() > 0) {
				messageMap.put(property,message);
			}
		}
		return messageMap;
	}

	/**
	 * Remove the message property for all messsage where the related property is hidden. <br>
	 * 
	 * @param messageMap map with message arranged by message property
	 * @param property property object
	 */
	protected void checkPropertyMessage(Map messageMap, PropertyData property) {
		Message message = null;
		if (isPropertyKeyForMessagesUsed) {
			message = (Message)messageMap.get(property.getTechKey().getIdAsString());
		} 
		else {
			message = (Message)messageMap.get(property.getName());
		}
		if (message != null) {
			if (property.isHidden()) {
				message.setProperty("");
			}
			messageMap.remove(message.getProperty());
		}
	}

    /**
      * Returns a list of help description for the given property.
      * Therefore the getHelpDescription method of the property object could called,
      * if the properties
      *
      * @param  property property for which helpvalues are searched
      * @return a <code>HelpDescription</code> with help description or <code>null</code>,
      *         if the property has no help text.
      *
      * @see com.sap.isa.maintenanceobject.util.HelpDescriptions
      */
    public HelpDescriptions getHelpDescription(PropertyData property)
            throws CommunicationException {
		// final String METHOD = "getHelpDescription()";
        return null;
    }


    /**
      * Synchronize bean properties with properties.
      *
      * Looking for corresponding properties in the given bean and
      * set the values of this bean properties.
      * See also the method {@link Property#setBeanPropertyValue(Object)} from the {@link Property} object. 
      *
      * @param bean backend object as bean
      *
      */
    public void synchronizeBeanProperties(Object bean) {

    	if (bean != null) {
    		Iterator fieldIterator = propertyIterator();
	
	        while (fieldIterator.hasNext()) {
				((PropertyData)fieldIterator.next()).setBeanPropertyValue(bean);
	        }
    	}        
    }

    
    /**
     * Synchronize all properties with bean properties.
     *
     * Looking for corresponding properties in the given bean and
     * get the values from this bean properties.
     * See also the method {@link Property#setValueFromBean(Object)} from the {@link Property} object. 
     */
    public void synchronizeProperties(Object bean) {

    	if (bean != null) {
    		Iterator fieldIterator = propertyIterator();
	
	        while (fieldIterator.hasNext()) {
				((PropertyData)fieldIterator.next()).setValueFromBean(bean);
	        }
    	}        
    }

    /**
      * Clear id , description and all properties
      *
      */
    public void clearData() throws CommunicationException {

        activePage = "";
        Iterator properyIterator = propertyIterator();

        while (properyIterator.hasNext()) {
            ((PropertyData)properyIterator.next()).clearValue();
        }
    }


 	/**
	 * Returns an iterator over all the properties. <br>
	 *
	 * @return iterator to iterate over the property groups
	 */
	public Iterator propertyIterator() {
		return uiProvider.getPropertyIterator();
	}


    /**
     * Returns an iterator for the screen groups.
     *
     * @return iterator to iterate over the screen groups
     */
    public Iterator pageIterator() {
        return uiProvider.getPageGroup().iterator();
    }

    
    /**
     * Returns a list for the pages.<p>
     *
     * @return list of the pages.<p>
     */
    public List getPageList() {
        return uiProvider.getPageGroup().getElementList();
    }

    
    /**
     * <p> The method addElement allows to add a new UI element under full control of 
     * dynamic UI object. </p>
     * 
     * @param newElement new Element to 
     * @param parentGroup parentGroup for this element.
     */
    public void addElement(UIElementBaseData newElement, UIElementGroupData parentGroup) {
        uiProvider.addElement(newElement, parentGroup);
    }

    
    /**
     * <p> The method deletePage delete the given page from the
     * dynamic UI object. </p>
     * 
     * @param element page to delete 
     */
    public void deletePage(UIElementGroupData element) {
        uiProvider.deletePage((UIElementGroupData)element);
    }

    /**
     * Delete the given element from the given element group. <br>
     * 
     * @param element element to be delete
     * @param group group which holds the element to be deleted.
     */
    public void deleteElement(UIElementBaseData element, UIElementGroupData group) {
        uiProvider.deleteElement(element, group);
    }

    
    /**
     * <p>Delete all pages of the dynamic UI. </p> 
     */
    public void deleteAllPages() {
    	uiProvider.deleteAllPages();
    }
    
    
	/**
	 * Check the given visibility condition. <br>
	 * 
	 * @param condition
	 * @return
	 */
	protected boolean checkCondition(VisibilityCondition condition) {
		
		PropertyData property= null;
		
		List list = getPropertyList(condition.getPropertyName());
		if (list != null && list.size() > 0) {
			property = (PropertyData)list.get(0);
		}
		
		if (property != null) {
			return property.getString().equals(condition.getValue());
		}
		
		return false;
	}


	/**
	 * <p>Returns the  UIRendererManager instance from the {@link GenericFactory} 
	 * via getInstance() method.</p>
	 * 
	 * @return UIRendererManager 
	 */		
	public UIRendererManager getUIRendererManager() {
		final String METHOD = "getUIRendererManager()";
	    log.entering(METHOD);	
	    
	    if (uiRendererManager== null) {
	    	uiRendererManager =
				 (UIRendererManager) GenericFactory.getInstance(uiProvider.getUIRendererManagerName());
	    	if (uiRendererManager == null) {
	    		log.error("entry [uipeRendererManager] missing in factory-config.xml");
	    		return null;
	    	}
	    }	

	    if (uiRendererManager!= null && uiProvider.getInvisibleParts() != null) {
	    	uiRendererManager.setInvisibleParts(uiProvider.getInvisibleParts());
	    }
	    
		log.exiting();				
		return uiRendererManager;
	}


    /**
     * <p>Call this method to check, if all required fields are provided. </p>
     *
     * @param dynamicUI  Reference to the dynmic UI object.
     * @param errorKey   Resource key to generate the message if a field is not entered.
     */
    public void checkRequiredProperties(String errorKey ) {
		
    	final String METHOD = "checkRequiredProperties()";
		log.entering(METHOD);

		checkRequiredPropertiesForPropertyIterator(propertyIterator(),
		                                           errorKey); 

		log.exiting();
	    
    }
	
	
    /**
     * Call this method to check all required fields for one UI Element Group
     *
     * @param elementGroup Name of the screen group
     * @param errorKey     Resource key to generate the message if a field is not entered.
     * 
     * @return <code>true</code>, if all required properties are filled 
     */
    public boolean checkRequiredPropertiesForElementGroup(UIElementGroup elementGroup,	
    												      String errorKey ) {
		final String METHOD = "checkRequiredPropertiesForElementGroup()";
		log.entering(METHOD);
		boolean ret = true;
		
		if (!checkRequiredPropertiesForPropertyIterator(elementGroup.propertyIterator(),
		                                                errorKey)) {
			ret = false;                            	
        }
		
        log.exiting();       
        return ret;
    }

	
    /**
     * <p>Call this method to check all required fields for a given property iterator. </p>
     *
     * @param object        Reference to the BusinessObjectManager
     * @param screenGroup   Name of the screen group
     * 
     * @return <code>true</code>, if all required properties are filled 
     */
    protected boolean checkRequiredPropertiesForPropertyIterator(Iterator properyIterator,	
    														     String errorKey ) {
		final String METHOD = "checkRequiredPropertiesForPropertyIterator()";
		log.entering(METHOD);
		boolean ret = true;
		
        while (properyIterator.hasNext()) {
            Property property = (Property)properyIterator.next();

            if (property.isRequired() && !property.isHidden() && !property.isDisabled()) {
                if ((property.getType().equals(Property.TYPE_STRING)
                	|| property.getType().equals(Property.TYPE_INTEGER)) &&
                        property.getString().length() == 0 ) {
					ret = false;
					if (errorKey != null) {
						addMessage(property, new Message(Message.ERROR,
	                                         errorKey,
	                                         null,
	                                         ""));
					}	
                }
            }
        }
        log.exiting();
        return ret;
    }

	/**
	 * Creates a new page and add it to the object.
	 */
    public void addPage() {
    	
		UIElementGroupData page = uiProvider.createPage();
		page.setName(page.getTechKey().getIdAsString());
		uiProvider.addElement(page, uiProvider.getPageGroup());
    }
    

    /**
     * Provide the exit handler need for the maintenance of the current dynamic UI object. <br>
     * 
     * @return instance of {@link DynamicUIMaintenanceExitData}
     */
    public DynamicUIMaintenanceExitData getMaintenanceExitData() {
		return uiProvider.getMaintenanceExitData();
	}

    
    /**
     * <p>Reads the UI model from the database. </p>
     * @throws CommunicationException 
     */
    public void read () throws CommunicationException {
    	DynamicUIBackend backendService;
		try {
			backendService = getDynamicUIBackendService();
			if (backendService != null) {
    			backendService.readData(this);
			}
		} catch (BackendException exception) {
			BusinessObjectHelper.splitException(exception);
		}
    }	
    
    /**
     * Overwrite this method to allow reading the object from database.
     */
    protected DynamicUIBackend getDynamicUIBackendService() throws BackendException {
    	return null;
    }


	/**
	 * 
	 * Return the property {@link #currentElementOnFocus}. <br>
	 * 
	 * @return currentElementOnFocus
	 */
	public String getCurrentElementOnFocus() {
		return currentElementOnFocus;
	}


	/**
	 *  Set the property {@link #currentElementOnFocus}. <br>
	 * 
	 * @param currentElementOnFocus The currentElementOnFocus to set.
	 */
	public void setCurrentElementOnFocus(String currentElementOnFocus) {
		this.currentElementOnFocus = currentElementOnFocus;
	}
    
    public void copyElement(TechKey elementKey, UIElementGroupData page){
    	uiProvider.copyElement(elementKey, page);    	
    }
    
    public void moveElement(TechKey elementKey, UIElementGroupData page){
    	uiProvider.moveElement(elementKey, page);    	
    }


	/**
	 * <p>Set the highest wizard page which was touched by the user</p> 
	 */    
	public void setWizardPage(String newPageName){
		this.wizardPage = newPageName;
	}

	/**
	 * <p>Get the highest wizard page which was touched by the user</p> 
	 */	
	public String getWizardPage(){
		if (this.wizardPage.equals("")) {
			this.wizardPage = this.activePage;
		}
		
		return this.wizardPage;
	}

	/**
	 * <p>Get the highest wizard page which was touched by the user</p> 
	 */		
	public boolean isWizardMode(){
		return this.wizardMode;
	}
	
	/**
	 * <p>Set the WizardMode </p> 
	 */	
	public void setWizardMode(boolean wizardMode){
		this.wizardMode = wizardMode;
	}
    
}
