/*****************************************************************************
    Class:        PropertyMaintenance
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Author:       Wei-Ming Tchay
    Created:      19.02.20088
    Version:      1.0

    $Revision: #1 $
    $Date: 2008/02/19 $
*****************************************************************************/
package com.sap.isa.maintenanceobject.businessobject;

import java.util.List;

import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.businessobject.BusinessObjectHelper;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.maintenanceobject.backend.boi.DescriptionListData;
import com.sap.isa.maintenanceobject.backend.boi.DynamicUIMaintenanceBackend;
import com.sap.isa.maintenanceobject.backend.boi.DynamicUIMaintenanceData;
import com.sap.isa.maintenanceobject.backend.boi.DynamicUIMaintenanceExitData;



public class DynamicUIMaintenance extends BusinessObjectBase implements BackendAware, DynamicUIMaintenanceData{
	
	private DynamicUI dynamicUI;
	
	private DynamicUIMaintenanceBackend dynamicUIMaintenanceBackend;
	
	private PropertyMaintenance propertyMaintenance;

	private PageMaintenance pageMaintenance;
	
	protected BackendObjectManager bem;
	
	protected List allowedValues;
	
	/**
	 * <p>The <code>descriptionList</code> holds language dependent descriptions. </p>
	 */
	protected DescriptionListData descriptionList;
	
	
	/**
	 * Key of the property which is currently maintained. <br>
	 */
	protected TechKey currentPropertyKey;
	
	/**
	 * Name of the page which is currently maintained. <br>
	 */
	protected String currentPageName;

	
	//TODO must be removed!!!!
	/**
	 * <p>Return the property {@link #bem}. </p> 
	 *
	 * @return Returns the {@link #bem}.
	 */
	public BackendObjectManager getBem() {
		return bem;
	}

	/*
	 * Standard Constructor
	 */
	public DynamicUIMaintenance() {

	}

	public DynamicUI getDynamicUI() {
		return dynamicUI;
	}

	public PropertyMaintenance getPropertyMaintenance() {
		return propertyMaintenance;
	}

	/**
	 * <p> The method getPageMaintenance returns a maintenacne object for the UI page. </p>
	 * 
	 * @return instance of {@link PageMaintenance}
	 */
	public PageMaintenance getPageMaintenance() {
		return pageMaintenance;
	}


	public List getAllowedValues() {
		return allowedValues;
	}

	public void setAllowedValues(List allowedValues) {
		this.allowedValues = allowedValues;
	}

	/**
	 * <p>Return the property {@link #descriptionList}. </p> 
	 *
	 * @return Returns the {@link #descriptionList}.
	 */
	public DescriptionListData getDescriptionList() {
		return descriptionList;
	}

	/**
	 * <p>Set the property {@link #descriptionList}. </p>
	 * 
	 * @param descriptionList The {@link #descriptionList} to set.
	 */
	public void setDescriptionList(DescriptionListData descriptionList) {
		this.descriptionList = descriptionList;
	}

	public void startEditDynamicUI(DynamicUI dynamicUI) throws CommunicationException {

		setDynamicUI(dynamicUI);
		// create propertyMaintenance for maintain properties
		if (propertyMaintenance == null) {
			propertyMaintenance = new PropertyMaintenance();
			pageMaintenance = new PageMaintenance();
			
			try {
				getBackendService().initMaintenanceObjects(this);
			} catch (BackendException ex) {
				BusinessObjectHelper.splitException(ex);
			}
		}
	}
	
	public void setDynamicUI(DynamicUI dynamicUI) {
		this.dynamicUI = dynamicUI;
	}

	public void saveDynamicUI() throws CommunicationException {

		if (propertyMaintenance == null) {
			throw new PanicException("propertyMaintenance is missing");
		}	
			
		try {
			getBackendService().saveData(dynamicUI, propertyMaintenance);
		} catch (BackendException ex) {
			BusinessObjectHelper.splitException(ex);
		}
	}
	
	
	
	public void setDynamicUIMaintenanceBackend(
			DynamicUIMaintenanceBackend dynamicUIMaintenanceBackend) {
		this.dynamicUIMaintenanceBackend = dynamicUIMaintenanceBackend;
	}

	public void setPropertyMaintenance(PropertyMaintenance propertyMaintenance) {
		this.propertyMaintenance = propertyMaintenance;
	}

	/**
	 * <p>Return the property {@link #currentPageName}. </p> 
	 *
	 * @return Returns the {@link #currentPageName}.
	 */
	public String getCurrentPageName() {
		return currentPageName;
	}

	/**
	 * <p>Set the property {@link #currentPageName}. </p>
	 * 
	 * @param currentPageName The {@link #currentPageName} to set.
	 */
	public void setCurrentPageName(String currentPageName) {
		this.currentPageName = currentPageName;
	}

	public TechKey getCurrentPropertyKey() {
		return currentPropertyKey;
	}

	public void setCurrentPropertyKey(TechKey currentPropertyKey) {
		this.currentPropertyKey = currentPropertyKey;
	}

	/**
     * Sets the BackendObjectManager for the basket object. This method is used
     * by the object manager to allow the user object interaction with
     * the backend logic. This method is normally not called
     * by classes other than BusinessObjectManager.
     *
     * @param bem BackendObjectManager to be used
     */
    public void setBackendObjectManager(BackendObjectManager bem) {
        this.bem = bem;
    }
    
	private DynamicUIMaintenanceBackend getBackendService()
			throws BackendException{
		
		if (dynamicUIMaintenanceBackend == null) {
            // get the Backend from the Backend Manager
			String boType = "DynamicUIMaintenance";
			if (dynamicUI != null) {
				DynamicUIMaintenanceExitData exitHandler = dynamicUI.getMaintenanceExitData();
		        if (exitHandler != null) {
		        	String type = exitHandler.getBOType();
		        	if (type != null && type.length() > 0) {
		        		boType = type;
		        	}
		        }
			}    
			dynamicUIMaintenanceBackend = 
                    (DynamicUIMaintenanceBackend) bem.createBackendBusinessObject(boType, null);
        }
		return dynamicUIMaintenanceBackend;		
		
	}
	
}
