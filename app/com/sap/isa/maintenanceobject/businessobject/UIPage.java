/*****************************************************************************
    Class:        Page
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Author:       SAP
    Created:      February 2008
    Version:      1.0

    $Revision: #1 $
    $Date: 2008/02/15 $
*****************************************************************************/

package com.sap.isa.maintenanceobject.businessobject;

import com.sap.isa.maintenanceobject.backend.boi.DescriptionData;
import com.sap.isa.maintenanceobject.backend.boi.DescriptionListData;
import com.sap.isa.maintenanceobject.backend.boi.UIPageData;
import com.sap.isa.maintenanceobject.ui.renderer.UIRendererManager;

/**
 *
 *
 */
public class UIPage extends UIElementGroup implements UIPageData, Cloneable {

	/**
	 * <p> list with language dependend descriptions.</p>
	 */
	protected DescriptionListData descriptionList = new DescriptionList();
	
	/**
	 * <p>Page Image. </p>
	 */
	protected String imageURL = "";

	/**
	 * <p>Height of the page image.</p>
	 */
	protected String imageHeight = "";
	
	/**
	 * <p>Width of the page image.</p>
	 */
	protected String imageWidth = "";


	/**
	 * Standard Constructor
	 *
	 */
	public UIPage(){
		setUiType(UIRendererManager.UI_TYPE_PAGE);
	}
	
	/**
	 * Constructor for a page with the given name
	 *
	 * @param name name of group
	 *
	 */
	public UIPage(String name) {
		super(name);
		setUiType(UIRendererManager.UI_TYPE_PAGE);
	}


	/**
	 * <p>Return the property {@link #descriptionList}. </p> 
	 *
	 * @return Returns the {@link #descriptionList}.
	 */
	public DescriptionListData getDescriptionList() {
		return descriptionList;
	}


	/**
	 * <p>Set the property {@link #descriptionList}. </p>
	 * 
	 * @param descriptionList The {@link #descriptionList} to set.
	 */
	public void setDescriptionList(DescriptionListData descriptionList) {
		this.descriptionList = descriptionList;
	}

	

	/**
	 * <p>Return the property {@link #imageURL}. </p> 
	 *
	 * @return Returns the {@link #imageURL}.
	 */
	public String getImageURL() {
		return imageURL;
	}

	/**
	 * <p>Set the property {@link #imageURL}. </p>
	 * 
	 * @param imageURL The {@link #imageURL} to set.
	 */
	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}
		
	/**
	 * 
	 * Return the property {@link #imageHeight}. <br>
	 * 
	 * @return imageHeight
	 */
	public String getImageHeight() {
		return imageHeight;
	}


	/**
	 *  Set the property {@link #imagelHeight}. <br>
	 * 
	 * @param imageHeight The imageHeight to set.
	 */
	public void setImageHeight(String imageHeight) {
		this.imageHeight = imageHeight;
}
	/**
	 * 
	 * Return the property {@link #imageWidth}. <br>
	 * 
	 * @return imageWidth
	 */
	public String getImageWidth() {
		return imageWidth;
	}

	/**
	 *  Set the property {@link #imageWidth}. <br>
	 * 
	 * @param imageWidth The imageWidth to set.
	 */
	public void setImageWidth(String imageWidth) {
		this.imageWidth = imageWidth;
	}
		
	
	/**
	 * <p> The method getDescription returns a language dependent description.</p>
	 * <p> If no lanaguage dependent description is maintained the default language is used. </p>
	 * 
	 * @param language language
	 * @return language dependent description if available. Otherwise the standard description.
	 */
	public String getDescription (String language) {
		DescriptionData description = null;
		if (language!= null && language.length() > 0) {
			description = descriptionList.getDescription(language);
			if (description != null) {
				return description.getDescription();
			}
		}
		return this.description;
	}
	
}
