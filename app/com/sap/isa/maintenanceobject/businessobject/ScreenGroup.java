/*****************************************************************************
    Class:        ScreenGroup
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP
    Created:      Januray 2002
    Version:      1.0

    $Revision: #7 $
    $Date: 2001/07/25 $
*****************************************************************************/

package com.sap.isa.maintenanceobject.businessobject;

import com.sap.isa.maintenanceobject.backend.boi.ScreenGroupData;
import com.sap.isa.maintenanceobject.backend.boi.PropertyGroupData;
import com.sap.isa.maintenanceobject.backend.boi.UIElementBaseData;

import java.util.*;

/**
 *
 * The screen group manages a group of property groups, which should be display
 * on differnt screens. This could used to implement tabs or wizards
 * in the shop maintenance
 *
 * @author SAP
 * @version 1.0
 */
public class ScreenGroup extends UIPage 
		implements Cloneable, ScreenGroupData {

    // only private access to properties
	/**
	 * @deprecated will be changed to privated access
	 */
    protected Map propertyGroups = getElementMap();

    // the properties are also stored as a list to keep the order
	/**
	 * @deprecated will be changed to privated access
	 */
    protected List propertyGroupList = getElementList();

	// temp list with all propertyGroup
	private List propertyGroupNames = new ArrayList();

    /**
     * Standard Constructor
     *
     */
    public ScreenGroup(){
    }


    /**
     * Constructor for a screen group with the given name
     *
     * @param name name of group
     *
     */
    public ScreenGroup(String name) {
        super(name);
    }

    /**
     * This method allows to build a type clean implementation of a group using
     * elements which extends the UIElement objects.
     *  
     * @return <code>true</code> if the instance is allowed. 
     */	
    protected boolean checkGroupInstance(UIElementBaseData element) {
        if (element instanceof PropertyGroup) {
        	return true;
        }
        return false;
    }
    
    /**
      * add a given propertyGorup to the screen group
      *
      * @param propertyGroup propertyGroup to be added
      *
      */
    public void addPropertyGroup(PropertyGroupData propertyGroup) {

    	addElement(propertyGroup);
    }


    /**
      * add a given property group to the screen group
      *
      * @param propertyGroup propertyGroup to be added
      *
      */
    public void addPropertyGroup(PropertyGroup propertyGroup) {
    	addElement(propertyGroup);
    }


    /**
     * add the name of a property group to the temporary name list.
     * The assigned property group can be added later
     *
     * @param propertyGroupName name of propertyGroup to be added
     *
     */
  	public void addTempPropertyGroupName(String propertyGroupName) {
        propertyGroupNames.add(propertyGroupName);
    }


    /**
     * Get the names of a property group which should be added. <br>
     * <strong>This function should only be used by the parsing process.</strong>
     *
     * @return list of property groupnames
     *
     */
  	public Iterator getTempPropertyGroupNames() {
        return propertyGroupNames.iterator();
    }


	/**
	 * Delete the names of a property group, which should be added. <br>
     * <strong>This function should only be used by the parsing process.</strong>
	 *
	 */
	public void deleteTempPropertyGroupNames() {
		if (propertyGroupNames != null) {
			Iterator iter = propertyGroupNames.iterator();
			while (iter.hasNext()) {
                String element = (String) iter.next();
                if (getElement(element)!=null) {
                	iter.remove();
                }
            }
		}
	}	


    /**
      * creates a new property group
      *
      * @param name name of the propertyGroup
      *
      */
    public PropertyGroupData createPropertyGroup(String propertyGroupName) {
        return new PropertyGroup(propertyGroupName);
    }


    /**
     * Returns the proeprty group with the given name
     *
     * @return name  name of the property group
     *
     */
    public PropertyGroupData getPropertyGroup(String name) {
        return (PropertyGroup)getElement(name);
    }


    /**
     * returns the object as string
     *
     * @return String which contains all fields of the object
     *
     */
    public String toString( ) {

        String str =  "Screen Group " + super.toString();

        return str;
    }



	/**
	 * Clones the screen group. <br>
	 * <strong>Including objects will not be cloned.<strong>
	 * 
	 * @return copy of the object
	 * 
	 * @see java.lang.Object#clone()
	 */
	public Object clone()  {
		return simpleClone();
	}



}
