/*****************************************************************************
    Class:        DescriptionList
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      18.04.2008
    Version:      1.0
*****************************************************************************/

package com.sap.isa.maintenanceobject.businessobject;

import java.util.Iterator;
import java.util.SortedMap;
import java.util.TreeMap;

import com.sap.isa.maintenanceobject.backend.boi.DescriptionData;
import com.sap.isa.maintenanceobject.backend.boi.DescriptionListData;

/**
 * <p>The class DescriptionList represent a list of language dependent descriptions. </p>
 *
 * @author  SAP AG
 * @version 1.0
 * @see com.sap.isa.maintenanceobject.backend.boi.DescriptionData
 */
public class DescriptionList implements DescriptionListData  {
	
	/**
	 * <p>sorted map with the languages.</p>  
	 */
	protected SortedMap descriptionMap = new TreeMap();
	
	
	//TODO DOCU
	//TODO docu
	/**
	 * <p>Overwrites/Implements the method <code>addDescription</code> 
	 * of the <code>DescriptionList</code>. </p>
	 * 
	 * @param description
	 * 
	 * 
	 * @see com.sap.isa.maintenanceobject.backend.boi.DescriptionListData#addDescription(com.sap.isa.maintenanceobject.backend.boi.DescriptionData)
	 */
	public void addDescription(DescriptionData description) {
		descriptionMap.put(description.getLanguage(), description);
	}
	
	//TODO docu
	/**
	 * <p>Overwrites/Implements the method <code>getDescription</code> 
	 * of the <code>DescriptionList</code>. </p>
	 * 
	 * @param language
	 * @return
	 * 
	 * 
	 * @see com.sap.isa.maintenanceobject.backend.boi.DescriptionListData#getDescription(java.lang.String)
	 */
	public DescriptionData getDescription(String language) {
		return (DescriptionData)descriptionMap.get(language);
	}

	//TODO docu
	/**
	 * <p>Overwrites/Implements the method <code>deleteDescription</code> 
	 * of the <code>DescriptionList</code>. </p>
	 * 
	 * @param language
	 * 
	 * 
	 * @see com.sap.isa.maintenanceobject.backend.boi.DescriptionListData#deleteDescription(java.lang.String)
	 */
	public void deleteDescription(String language) {
		descriptionMap.remove(language);
	}
	
	//TODO DOCU
	//TODO docu
	/**
	 * <p>Overwrites/Implements the method <code>iterator</code> 
	 * of the <code>DescriptionListData</code> interface. </p>
	 * 
	 * @return
	 * 
	 * 
	 * @see com.sap.isa.maintenanceobject.backend.boi.DescriptionListData#iterator()
	 */
	public Iterator iterator() {
		return descriptionMap.values().iterator();
	}
	
	//TODO docu
	/**
	 * <p>Overwrites/Implements the method <code>clone</code> 
	 * of the <code>DescriptionListData</code> interface. </p>
	 * 
	 * @return
	 * 
	 * 
	 * @see com.sap.isa.maintenanceobject.backend.boi.DescriptionListData#clone()
	 */
	public Object clone() {
		DescriptionList newList = null;
		try {
			newList = (DescriptionList)super.clone();
			newList.descriptionMap = new TreeMap();
			Iterator iter = iterator();
			while (iter.hasNext()) {
				newList.addDescription((DescriptionData )iter.next());
			}
		} catch (CloneNotSupportedException e) {
            return null;
		}
		return newList;  
	}

	
}
