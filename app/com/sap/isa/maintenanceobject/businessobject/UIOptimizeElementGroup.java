/*****************************************************************************
    Class:        UIOptimizeElementGroup
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      06.03.2008
    Version:      1.0
*****************************************************************************/

package com.sap.isa.maintenanceobject.businessobject;

import java.util.HashMap;
import java.util.Map;

//TODO docu
/**
 * <p>The class UIOptimizeElementGroup allows an optimize access to elements with hash maps. </p>
 * <p>The class can be used by default for fixed sets of elements (elements will not be deleted). <br>
 * Elements which will be handled by the UI editor will be taken into account. </p>
 * <p>The group can only hold element groups and no elements itself.
 * <p><strong>It is only allowed to use this type of group per 
 * {@link com.sap.isa.maintenanceobject.businessobject.DynamicUI}</strong></p>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class UIOptimizeElementGroup extends UIElementGroup {
	
	private Map optimizeElementMap = new HashMap();
	
	
	private class MapElement {
		
		/**
		 * <p>Holds the UI element itself. </p>.
		 */
		protected UIElementBase element; 

		/**
		 * <p>Holds the parent of UI element itself. </p>.
		 */
		protected UIElementBase parentElement;

		/**
		 * <p>Return the property {@link #element}. </p> 
		 *
		 * @return Returns the {@link #element}.
		 */
		public UIElementBase getElement() {
			return element;
		}

		/**
		 * <p>Set the property {@link #element}. </p>
		 * 
		 * @param element The {@link #element} to set.
		 */
		public void setElement(UIElementBase element) {
			this.element = element;
		}

		/**
		 * <p>Return the property {@link #parentElement}. </p> 
		 *
		 * @return Returns the {@link #parentElement}.
		 */
		public UIElementBase getParentElement() {
			return parentElement;
		}

		/**
		 * <p>Set the property {@link #parentElement}. </p>
		 * 
		 * @param parentElement The {@link #parentElement} to set.
		 */
		public void setParentElement(UIElementBase parentElement) {
			this.parentElement = parentElement;
		} 

	}

}
