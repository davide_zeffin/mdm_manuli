/*****************************************************************************
    Class:        Description
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      18.04.2008
    Version:      1.0
*****************************************************************************/

package com.sap.isa.maintenanceobject.businessobject;

import com.sap.isa.maintenanceobject.backend.boi.DescriptionData;

/**
 * <p>The class Description represents a language depending description for any kind UI Element. </p>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class Description implements DescriptionData {
	
	/**
	 * <p> Language of the description. </p>
	 */
	protected String language = "";
	
	/**
	 * <p>The description itself.</p>
	 */
	protected String description = "";

	//TODO docu
	/**
	 * <p>Overwrites/Implements the method <code>getDescription</code> 
	 * of the <code>Description</code>. </p>
	 * 
	 * @return Description text.
	 * 
	 * 
	 * @see com.sap.isa.maintenanceobject.backend.boi.DescriptionData#getDescription()
	 */
	public String getDescription() {
		return description;
	}

	//TODO docu
	/**
	 * <p>Overwrites/Implements the method <code>setDescription</code> 
	 * of the <code>Description</code>. </p>
	 * 
	 * @param description
	 * 
	 * 
	 * @see com.sap.isa.maintenanceobject.backend.boi.DescriptionData#setDescription(java.lang.String)
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	//TODO docu
	/**
	 * <p>Overwrites/Implements the method <code>getLanguage</code> 
	 * of the <code>Description</code>. </p>
	 * 
	 * @return language of the description.
     *
	 * @see com.sap.isa.maintenanceobject.backend.boi.DescriptionData#getLanguage()
	 */
	public String getLanguage() {
		return language;
	}

	//TODO docu
	/**
	 * <p>Overwrites/Implements the method <code>setLanguage</code> 
	 * of the <code>Description</code>. </p>
	 * 
	 * @param language
	 * 
	 * @see com.sap.isa.maintenanceobject.backend.boi.DescriptionData#setLanguage(java.lang.String)
	 */
	public void setLanguage(String language) {
		this.language = language;
	}

}
