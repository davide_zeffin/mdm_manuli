/*****************************************************************************
    Class:        VisibilityCondition
    Copyright (c) 2003, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      09.12.2003
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.maintenanceobject.businessobject;

import com.sap.isa.maintenanceobject.backend.boi.VisibilityConditionData;

/**
 * The class VisibilityCondition defines a condition, which must be true if a property
 * should be visible. <br> 
 * The condition consist of the name and the value of a property. Optional it is 
 * possible to add the name of the property group to the condition for a faster access
 * within the maintenacnce object<br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class VisibilityCondition implements Cloneable, VisibilityConditionData {

	/**
	 * The name of the property, whic should be checked .
	 */
	protected String propertyName = "";


	/**
	 * The name of the property group for a faster access to the property.
	 */
	protected String propertyGroupName = "";


	/**
	 * The value, which the property must have that the condition is true.
	 *
	 */
	protected String value = ""; 

    /**
     * standard constructor 
     */
    public VisibilityCondition() {
        super();
    }


    /**
     * Return the property {@link #propertyGroupName}. <br>
     * 
     * @return {@link #propertyGroupName}
     */
    public String getPropertyGroupName() {
        return propertyGroupName;
    }

    /**
     * Set the property {@link #propertyGroupName}. <br>
     * 
     * @param propertyGroupName see {@link #propertyGroupName}
     */
    public void setPropertyGroupName(String propertyGroupName) {
        this.propertyGroupName = propertyGroupName;
    }

    /**
     * Return the property {@link #propertyName}. <br>
     * 
     * @return {@link #propertyName}
     */
    public String getPropertyName() {
        return propertyName;
    }

    /**
     * Return Set the property {@link #propertyName}. <br>
     * 
     * @param propertyName see {@link #propertyName}
     */
    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    /**
     * Return the property {@link #value}. <br>
     * 
     * @return {@link #value}
     */
    public String getValue() {
        return value;
    }

    /**
     * Set the property {@link #value}. <br>
     * 
     * @param value see {@link #value}
     */
    public void setValue(String value) {
        this.value = value;
    }


	/**
	 * Clones the visibility condition. <br>
	 * 
	 * @return copy of the object
	 * 
	 * @see java.lang.Object#clone()
	 */
	public Object clone()  {
		try {
			return super.clone();

		}
		catch (CloneNotSupportedException e) {
			return null;
		}
	}
    


}
