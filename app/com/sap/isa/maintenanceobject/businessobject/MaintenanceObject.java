/*****************************************************************************
    Class:        MaintenanceObject
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP
    Created:      Januray 2002
    Version:      1.0

    $Revision: #7 $
    $Date: 2001/07/25 $
*****************************************************************************/

package com.sap.isa.maintenanceobject.businessobject;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.sap.isa.businessobject.BusinessObjectHelper;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.Iterable;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.helpvalues.HelpValues;
import com.sap.isa.maintenanceobject.backend.boi.DynamicUIExitData;
import com.sap.isa.maintenanceobject.backend.boi.DynamicUIMaintenanceExitData;
import com.sap.isa.maintenanceobject.backend.boi.DynamicUIProviderData;
import com.sap.isa.maintenanceobject.backend.boi.MaintenanceObjectBackend;
import com.sap.isa.maintenanceobject.backend.boi.MaintenanceObjectData;
import com.sap.isa.maintenanceobject.backend.boi.PropertyData;
import com.sap.isa.maintenanceobject.backend.boi.PropertyGroupData;
import com.sap.isa.maintenanceobject.backend.boi.ScreenGroupData;
import com.sap.isa.maintenanceobject.backend.boi.UIElementBaseData;
import com.sap.isa.maintenanceobject.backend.boi.UIElementData;
import com.sap.isa.maintenanceobject.backend.boi.UIElementGroupData;
import com.sap.isa.maintenanceobject.ui.renderer.UIRendererManager;
import com.sap.isa.maintenanceobject.util.HelpDescriptions;

/**
 * The maintenance class is generic object which can be used to maintain
 * properties, which are arranged inside property groups on a generic web ui.
 *
 * @author SAP
 * @version 1.0
 *
 */

abstract public class MaintenanceObject extends DynamicUI
        implements MaintenanceObjectData, Cloneable {

    // only private access to the property groups
    private Map  propertyGroups    = new HashMap();

    // the groups are also stored as a list to keep the order
    private List propertyGroupList = new ArrayList();

    // only private access to the property groups
    private Map  screenGroups    = new HashMap();

    // the screen groups are also stored as a list to keep the order
    private List screenGroupList = new ArrayList();

    
    private PageGroup pageGroup = new PageGroup();
    
	/**
	 * Flag to sign a new object
	 */
    protected boolean isNewObject = true;

    /**
     * property id: id of the object
     */
    protected String id;


    /**
     * @deprecated
     */
	protected String activeScreenGroup = "";

    /**
     * property description: description of the object
     */
    protected String description;
    
    
    // max length of the id;
    private int maxLengthId = 0;


    // max length of the description;
    private int maxLengthDescription = 0;


	private boolean isLocked = false;

	
	/**
	 * The ui provider will be set in the standard constructor. 
	 */
	public MaintenanceObject() {
		uiProvider = new UIProvider();
		isPropertyKeyForMessagesUsed = false;
	}
	
    /**
     * Set the property id
     *
     * @param id
     */
    public void setId(String id) {
        this.id = id.trim();
    }


    /**
     * Returns the property id
     *
     * @return id
     */
    public String getId() {
       return this.id.trim();
    }


    /**
     * Set the description for the object
     *
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }


    /**
     * Returns the description of the object
     *
     * @return description
     */
    public String getDescription() {
       return this.description;
    }


    /**
     * Set the flag, if the object is created
     *
     * @param newObject
     *
     */
    public void setNewObject(boolean newObject) {
        this.isNewObject = newObject;
    }


    /**
     * Returns if the object is created
     *
     * @return newObject
     *
     */
    public boolean isNewObject() {
       return isNewObject;
    }

		
	/**
	 * Set a lock the object 
	 */ 
	public void lock() throws CommunicationException {
		final String METHOD = "lock()";
		log.entering(METHOD);
		if (!isLocked && getTechKey().getIdAsString().length()>0) {
			try {
	            // Initialize object in the backend
	            if (getBackendService().lockObject(this)) {
	            	isLocked = true;
	            }	
	        }
	        catch (BackendException ex) {
	        	log.exiting();
	            BusinessObjectHelper.splitException(ex);
	        }		
	        
	    }
	    log.exiting();		
	}	

	
	/**
	 * Delete a lock from the object 
	 */ 
	public void unlock() throws CommunicationException {
		final String METHOD = "unlock()";
		log.entering(METHOD);
		if (isLocked) {
			try {
	            // Initialize object in the backend
	            getBackendService().unlockObject(this);
	        }
	        catch (BackendException ex) {
	        	log.exiting();
	            BusinessObjectHelper.splitException(ex);
	        }		
	       
			isLocked = false;
	    }
	    log.exiting();			
	}	

	/**
	 * Returns the if the object is locked.
	 * @return boolean
	 */
	public boolean isLocked() {
		return isLocked;
	}


	/**
	 * Sets locked flag of the object.
	 * 
	 * @param locked The isLocked to set
	 */
	public void setLocked(boolean locked) {
		this.isLocked = locked;
	}

    /**
     * Returns the screen group with the given name
     *
     * @return screen group
     *
     */
    public ScreenGroupData getScreenGroup(String screenGroupName) {

        return (ScreenGroup)screenGroups.get(screenGroupName);
    }


    /**
     * Returns the property group with the given name
     *
     * @return Property group
     *
     */
    public PropertyGroupData getPropertyGroup(String propertyGroupName) {

        return (PropertyGroup)propertyGroups.get(propertyGroupName);
    }


    /**
     * Returns the property with the given name in the given property group
     *
     * @return property
     *
     */
    public PropertyData getProperty(String propertyGroupName, String propertyName) {

        PropertyGroupData propertyGroup = this.getPropertyGroup(propertyGroupName);
        if (propertyGroup != null) {
            return propertyGroup.getProperty(propertyName);
        }
        return null;
    }


    /**
     * Returns the property with the given name in the given property group
     *
     * @return propertyName
     *
     */
    public PropertyData getProperty(String propertyName) {

        Iterator groupIterator = iterator();

        while (groupIterator.hasNext()) {

           PropertyData property = ((PropertyGroup) groupIterator.next()).getProperty(propertyName);
           if (property != null) {
               return property;
            }
        }
        return null;
    }


    /**
      * add a given property group to the maintened object
      *
      * @param propertyGroup group to add
      *
      */
    public void addPropertyGroup(PropertyGroupData propertyGroup) {

        if (propertyGroup instanceof PropertyGroup) {
            addPropertyGroup((PropertyGroup)propertyGroup);
        }
    }

    /**
      * add a given property group to the maintened object
      *
      * @param propertyGroup group to add
      *
      */
    public void addPropertyGroup(PropertyGroup propertyGroup) {

        propertyGroups.put(propertyGroup.getName(), propertyGroup);
        propertyGroupList.add(propertyGroup);
    }


    /**
      * add a given screen group to the maintened object
      *
      * @param screenGroup group to add
      *
      */
    public void addScreenGroup(ScreenGroupData screenGroup) {

        if (screenGroup instanceof ScreenGroup) {
            addScreenGroup((ScreenGroup)screenGroup);
        }
    }


    /**
      * add a given screen group to the maintened object
      *
      * @param screenGroup group to add
      *
      */
    public void addScreenGroup(ScreenGroup screenGroup) {

        screenGroups.put(screenGroup.getName(), screenGroup);
        screenGroupList.add(screenGroup);
        
    }


	/**
	 * Returns the name of active screen group.
	 * 
	 * @return String
	 */
	public String getActiveScreenGroup() {
		// return activeScreenGroup;
		return getActivePageName();
	}

	/**
	 * Sets the active screen group.
	 * @param activeScreenGroup The name of the active screen group to set
	 */
	public void setActiveScreenGroup(String activeScreenGroup) {
		this.activeScreenGroup = activeScreenGroup;
		setActivePageName(activeScreenGroup);
	}

	
	/**
	 * Sets the active screen group.
	 * @param activeScreenGroup The name of the active screen group to set
	 */
	public void setActivePageName(String activeScreenGroup) {
		this.activeScreenGroup = activeScreenGroup;
		super.setActivePageName(activeScreenGroup);
	}


	
    /**
     * Returns the max length for the id.
     * @return max length (<code>int</code>)
     */
    public int getMaxLengthId() {
        return maxLengthId;
    }

    /**
     * Sets the max length for the id.
     * @param maxLengthId The max length for the id
     */
    public void setMaxLengthId(int maxLengthId) {
        this.maxLengthId = maxLengthId;
    }

    
    /**
     * Returns the max length for the description.
     * @return max length (<code>int</code>)
     */
    public int getMaxLengthDescription() {
        return maxLengthDescription;
    }

    /**
     * Sets the max length for the description.
     * @param maxLengthDescription The max length for the description
     */
    public void setMaxLengthDescription(int maxLengthDescription) {
        this.maxLengthDescription = maxLengthDescription;
    }


    /**
      * creates a new property group with the given name
      *
      * @param propertyGroupName name of the propertyGroup
      *
      */
    public PropertyGroupData createPropertyGroup(String propertyGroupName) {
        return new PropertyGroup(propertyGroupName);
    }


    /**
      * creates a new screen group with the given name
      *
      * @param screenGroupName name of the screenGroup
      *
      */
    public ScreenGroupData createScreenGroup(String screenGroupName) {
        return new ScreenGroup(screenGroupName);
    }


    /**
      * Returns a <code>HelpValues</code> object within helpvalues for the given
      * property.
      * Therefore the getHelpValue method of the property object is called.
      *
      * @param  property property for which helpvalues are searched
      * @return a <code>HelpValues</code> with help values or <code>null</code>,
      *         if the property has no method or the method doesn't exist for
      *         the backend object.
      *
      * @see com.sap.isa.maintenanceobject.util.HelpValues
      */
    public HelpValues getHelpValues(PropertyData property)
            throws CommunicationException {
		final String METHOD = "getHelpValues()";
		log.entering(METHOD);

        try {
            MaintenanceObjectBackend backend = getBackendService();

            // First get the class of backend service
            Class backendClass = backend.getClass();

            // read the method name of the object
            String methodName = property.getHelpValuesMethod();

            if (methodName != null && methodName.length() > 0) {
                try {

                    // get the help value method from the class
                    Method method = backendClass.getMethod(methodName,
                            new Class[]{MaintenanceObjectData.class, PropertyData.class});

                    // invoke the method to get the result Data
                    return (HelpValues) method.invoke(backend, new Object[]{this, property});
                }
                catch (Exception exception) {
                    log.debug(exception);
                }
            }
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally {
        	log.exiting();
        }
        return null;
    }


    /**
      * Returns a list of help description for the given property.
      * Therefore the getHelpDescription method of the property object could called,
      * if the properties
      *
      * @param  property property for which helpvalues are searched
      * @return a <code>HelpDescription</code> with help description or <code>null</code>,
      *         if the property has no help text.
      *
      * @see com.sap.isa.maintenanceobject.util.HelpDescriptions
      */
    public HelpDescriptions getHelpDescription(PropertyData property)
            throws CommunicationException {
		final String METHOD = "getHelpDescription()";
		log.entering(METHOD);
		try {
			return getBackendService().getHelpDescriptions(this, property);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally {
        	log.exiting();
        }
        return null;
    }


     /**
      * Synchronize backend properties with properties.
      *
      * Looking for corresponding properties in the backend bean and
      * set the values of this properties.
      *
      */
    public void synchronizeBeanProperties() throws CommunicationException {

        try {
            MaintenanceObjectBackend bean = getBackendService();

            synchronizeBeanProperties(bean);

        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
    }


    /**
      * Synchronize backend properties with properties.
      *
      * Looking for corresponding properties in the backend bean and
      * set the values of this properties.
      *
      * @param bean backend object as bean
      *
      */
    public void synchronizeBeanProperties(MaintenanceObjectBackend bean) {

        Iterator groupIterator = iterator();

        while (groupIterator.hasNext()) {

            Iterator properyIterator = ((PropertyGroup) groupIterator.next()).iterator();

            while (properyIterator.hasNext()) {
				((Property)properyIterator.next()).setBeanPropertyValue(bean);
            }

        }
    }


    /**
      * Synchronize properties with backend properties.
      *
      * Looking for corresponding properties in the backend bean and
      * get the values from this properties.
      *
      */
    public void synchronizeProperties() throws BackendException{

        MaintenanceObjectBackend bean = getBackendService();
        
        if (bean != null) {
        	synchronizeProperties(bean);
        }
    }


    /**
      * Clear id , description and all properties
      *
      */
    public void clearData() throws CommunicationException {

        isNewObject = true;
        id = "";
        description = "";
        activeScreenGroup = "";
        Iterator properyIterator = propertyIterator();

        while (properyIterator.hasNext()) {
            ((Property)properyIterator.next()).clearValue();
        }
        
        synchronizeBeanProperties();
    }

    
    /**
     * Returns an iterator for the property groups. <br> 
     * Method necessary because of
     * the <code>Iterable</code> interface.
     *
     * @return iterator to iterate over the property groups
     */
    public Iterator iterator() {
        return propertyGroupList.iterator();
    }


    /**
     * Returns an iterator for the screen groups.
     *
     * @return iterator to iterate over the screen groups
     */
    public Iterator screenGroupIterator() {
        return screenGroupList.iterator();
    }


    /**
     * Returns a list for the screen groups.
     *
     * @return list of the screen groups
     */
    public List getScreenGroupList() {
        return screenGroupList;
    }


	/**
	 * Clones the Maintenance object. <br>
	 * All including objects will be also cloned.
	 * 
	 * @return copy of the object and all sub objects
	 * 
	 * @see java.lang.Object#clone()
	 */
	public Object clone()  {
		try {
			MaintenanceObject maintenanceObject = (MaintenanceObject)super.clone();

			maintenanceObject.copy((MaintenanceObject)this);

			return maintenanceObject;
		}
		catch (CloneNotSupportedException e) {
			return null;
		}
	}

	/**
	 * Clones the Maintenance object. <br>
	 * All including objects will be also cloned.
	 *
	 * @param objectData source Object 
	 * @return copy of the object and all sub objects
	 * 
	 */
	public void copy(MaintenanceObjectData objectData) {
		
		// reset the cloned map and list
		// This is needed because we have to create new object while clone a object
		// It is not relevant for the pure copy case!
		propertyGroups = new HashMap();
		propertyGroupList = new ArrayList();

		screenGroups = new HashMap();
		screenGroupList = new ArrayList();
		
		pageGroup = new PageGroup();
				
		Iterator iter = objectData.iterator();
		while (iter.hasNext()) {
			PropertyGroup oldGroup = (PropertyGroup)iter.next();
			addPropertyGroup((PropertyGroup)oldGroup.clone());
		}

		iter = objectData.screenGroupIterator();
		
		while (iter.hasNext()) {
			ScreenGroup oldGroup = (ScreenGroup)iter.next();
			ScreenGroup newGroup = (ScreenGroup)oldGroup.clone();

			// reset the cloned map and list
			newGroup.propertyGroups = new HashMap();
			newGroup.propertyGroupList = new ArrayList();
			
			// adjust goups
			newGroup.setElements(newGroup.propertyGroups, newGroup.propertyGroupList);
			
			// add the correct references
			Iterator iter2 = oldGroup.iterator();
			while (iter2.hasNext()) {
				String name = ((PropertyGroup)iter2.next()).getName();
				newGroup.addPropertyGroup((PropertyGroup)propertyGroups.get(name));
			}
			
			addScreenGroup(newGroup);
		}
	}


    /**
     * Initialize the object. All need property groups and properties will
     * be created.
     *
     */
    public void initObject() throws CommunicationException {
		final String METHOD = "initObject()";
		log.entering(METHOD);

        try {
            // Initialize object in the backend
            getBackendService().initObject(this);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally {
        	log.exiting();
        }

    }


    /**
     * returns the backend object
     */
    abstract protected MaintenanceObjectBackend getBackendService()
            throws BackendException;


    /**
     * //TODO docu
    /**
     * The class ScreenGroupGroup . <br>
     *
     * @author  SAP AG
     * @version 1.0
     */
    private class PageGroup extends UIElementGroup  implements UIElementGroupData {
    	
    	public PageGroup() {
    		setElements(screenGroups, screenGroupList);
    	}
    }
    
    
	/**
	 * <p>The inner class UIProvider fulfill the requirements of the {@link DynamicUIProviderData} interface. </p>
	 *
	 * @author  SAP AG
	 * @version 1.0
	 */
	private class UIProvider implements DynamicUIProviderData {

		/**
		 * <p>Overwrites the method getElementGroupList.</p>
		 * <p>Call the the method {@link UIElementGroup#getElementList(String, Boolean)} 
		 * to get all element groups for the given name.</p> 
		 * 
		 * @param elementName
		 * @return list of element groups.
		 * 
		 * @see com.sap.isa.maintenanceobject.backend.boi.DynamicUIProviderData#getElementGroupList(java.lang.String)
		 */
		public List getElementGroupList(String elementName) {
			return pageGroup.getElementList(elementName, new Boolean(true));
		}


		//TODO docu
		/**
		 * <p>Overwrites the method getPageGroup. </p>
		 * 
		 * @return
		 * 
		 * 
		 * @see com.sap.isa.maintenanceobject.backend.boi.DynamicUIProviderData#getPageGroup()
		 */
		public UIElementGroupData getPageGroup() {
			return pageGroup;
		}

		
		/**
		 * <p>Overwrites the method getPropertyList.</p>
		 * <p>Call the the method {@link UIElementGroup#getElementList(String, Boolean)} 
		 * to get all element groups for the given name.</p> 
		 * 
		 * @param elementName
		 * @return list of properties with the given name. 
		 * 
		 * @see com.sap.isa.maintenanceobject.backend.boi.DynamicUIProviderData#getPropertyList(java.lang.String)
		 */
		public List getPropertyList(String elementName) {
			return pageGroup.getElementList(elementName, new Boolean(false));
		}

		
		//TODO docu
		/**
		 * Overwrites the method . <br>
		 * 
		 * @param key
		 * @return
		 * 
		 * 
		 * @see com.sap.isa.maintenanceobject.backend.boi.DynamicUIProviderData#getElementGroup(com.sap.isa.core.TechKey)
		 */
		public UIElementGroupData getElementGroup(TechKey key) {
			UIElementBaseData element = pageGroup.getElement(key);
			if (element instanceof UIElementGroupData) {
				return (UIElementGroupData) element;
			}
			return null;
		}


		//TODO docu
		/**
		 * Overwrites the method . <br>
		 * 
		 * @param key
		 * @return
		 * 
		 * 
		 * @see com.sap.isa.maintenanceobject.backend.boi.DynamicUIProviderData#getProperty(com.sap.isa.core.TechKey)
		 */
		public PropertyData getProperty(TechKey key) {
			UIElementBaseData element = pageGroup.getElement(key);
			if (element instanceof PropertyData) {
				return (PropertyData) element;
			}
			return null;
		}


		//TODO docu
		/**
		 * <p>Overwrites/Implements the method <code>getParent</code> 
		 * of the <code>UIProvider</code>. </p>
		 * 
		 * @param key
		 * @return
		 * 
		 * 
		 * @see com.sap.isa.maintenanceobject.backend.boi.DynamicUIProviderData#getParent(com.sap.isa.core.TechKey)
		 */
		public UIElementGroupData getParent(TechKey key) {
			return pageGroup.getParent(key);
		}


		//TODO docu
		/**
		 * Overwrites the method . <br>
		 * 
		 * @return
		 * 
		 * 
		 * @see com.sap.isa.maintenanceobject.backend.boi.DynamicUIProviderData#getPropertyIterator()
		 */
		public Iterator getPropertyIterator() {
			return new PropertyIterator(propertyGroupList.iterator());
		}


		//TODO docu
		/**
		 * <p>Overwrites the method getInvisibleParts. </p>
		 * 
		 * @return <code>null</code>
		 * 
		 * @see com.sap.isa.maintenanceobject.backend.boi.DynamicUIProviderData#getInvisibleParts()
		 */
		public Map getInvisibleParts() {
			return null;
		}


		//TODO docu
		/**
		 * <p>Overwrites the method getUIRendererManagerName. </p>
		 * 
		 * @return the standard UI Renderer Manager
		 * 
		 * @see com.sap.isa.maintenanceobject.backend.boi.DynamicUIProviderData#getUIRendererManagerName()
		 */
		public String getUIRendererManagerName() {
			return UIRendererManager.STANDARD_UI_RENDERER_MANAGER_NAME;
		}


		//TODO docu
		/**
		 * <p>Overwrites/Implements the method <code>addElement</code> 
		 * of the <code>UIProvider</code>. </p>
		 * 
		 * @param newElement
		 * @param parentGroup
		 * 
		 * 
		 * @see com.sap.isa.maintenanceobject.backend.boi.DynamicUIProviderData#addElement(com.sap.isa.maintenanceobject.backend.boi.UIElementBaseData, com.sap.isa.maintenanceobject.backend.boi.UIElementGroupData)
		 */
		public void addElement(UIElementBaseData newElement, UIElementGroupData parentGroup) {
			
			if (newElement instanceof PropertyGroupData) {
				addPropertyGroup((PropertyGroupData) newElement);
				if (parentGroup != null) {
					parentGroup.addElement((PropertyGroupData)newElement);
				}
			}

			if (newElement instanceof ScreenGroupData) {
				pageGroup.addElement((ScreenGroupData)newElement);
			}

			if (newElement instanceof PropertyData) {
				parentGroup.addElement((ScreenGroupData)newElement);
			}
		}


		//TODO docu
		/**
		 * <p>Overwrites/Implements the method <code>deleteAllPages</code> 
		 * of the <code>UIProvider</code>. </p>
		 * 
		 * 
		 * 
		 * @see com.sap.isa.maintenanceobject.backend.boi.DynamicUIProviderData#deleteAllPages()
		 */
		public void deleteAllPages() {
            pageGroup.clear();			
			propertyGroupList.clear();
			propertyGroupList.clear();
			screenGroupList.clear();
			screenGroups.clear();
		}

		//TODO docu
		/**
	     * <p>Delete pages of dynamic UI. </p> 
	     */
	    public void deletePage(UIElementGroupData page){
	    	pageGroup.deleteElement((UIElementData)page);
	    }
	    
//	  TODO docu    
	    /**
	     */
	    public void deleteElement(UIElementBaseData element, UIElementGroupData group){
	    	if (element instanceof UIElementData) {
	    		group.deleteElement((UIElementData)element);
	    	}
	    	else if (element instanceof UIElementGroupData) {
	    		group.deleteElement((UIElementGroupData)element);
	    		
	    	}
	    }
	    
		
//TODO docu
		public UIElementGroupData createPage() {
			return new ScreenGroup();
		}

		
//	  TODO docu    
	    /**
	     */
	    public void moveElement(TechKey elementKey, UIElementGroupData page){
	    
	    }

	    
//	  TODO docu    
	    /**
	     */
	    public void copyElement(TechKey elementKey, UIElementGroupData page){
	    	
	    }

		
//TODO docu
		public DynamicUIMaintenanceExitData getMaintenanceExitData() {
			// TODO Auto-generated method stub
			return null;
		}
		
		/**
		 * <p> The method getDynamicUIExitsHandler provides exits which can be used for Dynamic UI
		 * implementation </p>
		 * 
		 * @return instance of {@link DynamicUIExitData}
		 */
		public DynamicUIExitData getDynamicUIExitData() {
			return null;
		}
		
		
	}
	
	private static class PropertyIterator implements Iterator {
		
        private Iterator groupIterator;
        private Iterator propertyIterator;

        public PropertyIterator(Iterator groupIterator) {
            this.groupIterator = groupIterator;
        }

        /**
         * Overwrites the method hasNext. <br>
         * 
         * @return 
         * 
         * @see java.util.Iterator#hasNext()
         */
        public boolean hasNext() {

			if (propertyIterator == null || !propertyIterator.hasNext()) {
			
				if (groupIterator.hasNext()) {
					propertyIterator = ((Iterable)groupIterator.next()).iterator();
				}	
			}
	
			if (propertyIterator != null && propertyIterator.hasNext()) {
				return true;
			}

            return false;
        }

        /**
         * Overwrites the method next. <br>
         * 
         * @return
         * 
         * 
         * @see java.util.Iterator#next()
         */
        public Object next() {
	
            return hasNext()? propertyIterator.next():null;
        }


        /**
         * Overwrites the method remove. <br>
         * 
         * @see java.util.Iterator#remove()
         */
        public void remove() {

            if (hasNext()) {
				propertyIterator.remove();
        	}
        }


    }
    
}
