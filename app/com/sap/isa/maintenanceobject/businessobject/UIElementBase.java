/*****************************************************************************
    Class:        UIElementBase
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP
    Created:      November 2002
    Version:      1.0

    $Revision: #7 $
    $Date: 2001/07/25 $
*****************************************************************************/

package com.sap.isa.maintenanceobject.businessobject;

import com.sap.isa.core.TechKey;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.maintenanceobject.backend.boi.UIElementBaseData;
import com.sap.isa.maintenanceobject.backend.boi.UIElementStatus;

/**
 * <p>Base class for UI elements which contains properties like name and
 * descrition which are used from all UI elements.</p> 
 * <p>The attribute {@link #additionalData} can be used to add arbitrary data to all UI elements.</p> 
 *
 * @see com.sap.isa.maintenanceobject.businessobject.DynamicUI
 * 
 * @author SAP
 * @version 1.0
 */
public class UIElementBase implements UIElementBaseData {

	/**
	 * <p>Reference to the IsaLocation.</p> 
	 *
	 * @see com.sap.isa.core.logging.IsaLocation
	 */
	protected static IsaLocation log = IsaLocation.
		  getInstance(UIElementBase.class.getName());
	
	/**
	 * <p>Technical key of UI element.</p>
	 * <p>The key will automatically generate if the element is created. </p> 
	 * <p>But it can be overrite afterwards with the {@link #setTechKey} method.</p>
	 */
	protected TechKey techKey = TechKey.generateKey();

	/**
	 * <p>Extension hook to add additional data to all UI elements. </p>
	 */
	protected Object additionalData = null;
	
	/**
	 * Name of the element. <br>
	 */
	protected String name = "";

	/**
	 * Description of the element. <br>
	 */
	protected String description = "";

	/**
	 * flag if element is visible. <br>
	 */
	private boolean isHidden = false;

	/**
	 * flag if the element is allowed. Only allowed fields could be unhide. <br>  
	 **/
	private boolean isAllowed = true;

	/**
	 * holds the level on which allowed flag was set. <br>   
	 **/
	private int setAllowedLevel = UIElementBaseData.DEFAULT;
	
	/**
	 * <p>Hold the status of an UI element.</p>
	 */
	protected UIElementStatus status = null; 

    /**
	 * flag if property description text should be retrieved from the resourse key file. <br>
	 */
	protected boolean isRessourceKeyUsed = true;
	
	/**
	 * UI Type name of the element. <br>
	 */
	protected String uiType = "";	
	
	/**
	 * Access key of the element. <br>
	 */
	protected String accessKey = "";
	
	/**
	 * Tab index of the element. <br>
	 */
	protected String tabIndex = "";
	
	/**
	 * Style classes of the element. <br>
	 * <p>The Array styleClasses will automatically initialized with size 3
	 * if the element is created. </p> 
	 * <p>But it can be overwritten afterwards with the {@link #setStyleClasses} method.</p>
	 */
	protected String[] styleClasses = new String[3];
	
	
	/**
	 * Standard Constructor
	 *
	 */
	public UIElementBase() {
	}

	/**
	 * Constructor for a property group with the given name
	 *
	 * @param name name of group
	 *
	 */
	public UIElementBase(String name) {
		this.name = name;
	}

	
	/**
	 * 
	 * Return the property {@link #accessKey}. <br>
	 * 
	 * @return accessKey
	 */
	
	public String getAccessKey() {
		return accessKey;
	}

	/**
	 *  Set the property {@link #accessKey}. <br> 
	 *  
	 * @param accessKey The accessKey to set.
	 */
	
	public void setAccessKey(String accessKey) {
		this.accessKey = accessKey;
	}

	
	/**
	 * Return the property {@link #additionalData}. <br> 
	 *
	 * @return Returns the {@link #additionalData}.
	 */
	public Object getAdditionalData() {
		return additionalData;
	}

	/**
	 * Set the property {@link #additionalData}. <br>
	 * 
	 * @param additionalData The {@link #additionalData} to set.
	 */
	public void setAdditionalData(Object additionalData) {
		this.additionalData = additionalData;
	}

	/**
	 * Tells whether the element is allowed to display.
	 * <br> Only allowed element can be unhide, if they are hidden.
	 * 
	 * @return boolean {@link #isAllowed}
	 */
	public boolean isAllowed() {
		return isAllowed;
	}

	/**
	 * Sets isAllowed Flag with default reason level
	 * <br> Only allowed elements can be set to visible
	 *
	 * @param allowed allowed flag to set
	 */
	public void setAllowed(boolean allowed) {
		setAllowed(allowed, UIElementBase.DEFAULT);
	}

	/**
	 * Sets if the element is allowed to display.
	 * <br> Only allowed elements can be set to visible.
	 * <p> The level concept allows to handle different levels on which
     *     an UI element can be controlled. E.g. If an element is hidden by configuration
     *     the authorithy can not show this element again. Therefor only if the level is higher as
     *     the current level an element can be set to <em>allowed</em>". </p>
     *     
	 * @param allowed allowed flag to set
	 * @param level strength the reason and importance why the flag was set
	 * 
	 */
	public void setAllowed(boolean allowed, int level) {
		
		if (level >= setAllowedLevel) {
			this.setAllowedLevel = level;
			this.isAllowed = allowed;
			if (!isAllowed()) {
				isHidden = true;
			}
		}
	}

	/**
	 * Set the description of the element.<br>
	 *
	 * @param description description of the UI element.
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Returns the description of the element. <br>
	 *
	 * @return description
	 *
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 * Returns if the element is visible.<br>
	 * 
	 * @return <code>true</code> if the element is hidden and 
	 *         <code>false</code> if the element is visible
	 */
	public boolean isHidden() {
		return isHidden;
	}

	/**
	 * Sets the hidden flag. <br>
	 * <b>Note</b>: The element can only be unhidden, if the element is allowed  
	 * 
	 * @param hidden The hidden flag to be set
	 */
	public void setHidden(boolean hidden) {
		if (isAllowed || hidden == true) {
			this.isHidden = hidden;
		}
	}

	/**
	 * Returns the name of the element. <br>
	 *
	 * @return name of the element.
	 *
	 */
	public String getName() {
		return name;
	}


	/**
	 * Set the name of the element.<br>
	 *
	 * @param name name of the element
	 *
	 */
	public void setName(String name) {
		this.name = name;
	}

    /**
	 * Return the property {@link #status}. <br> 
	 *
	 * @return Returns the {@link #status}.
	 */
	public UIElementStatus getStatus() {
		return status;
	}

	/**
	 * Set the property {@link #status}. <br>
	 * 
	 * @param status The {@link #status} to set.
	 */
	public void setStatus(UIElementStatus status) {
		this.status = status;
	}

	/**
	 * 
	 * Return the property {@link #styleClasses}. <br>
	 * 
	 * @return styleClasses
	 */
	
	public String[] getStyleClasses() {
		return styleClasses;
	}

	/**
	 *  Set the property {@link #styleClasses}. <br> 
	 *  
	 * @param styleClasses The styleClasses to set.
	 */
	
	public void setStyleClasses(String[] styleClasses) {
		this.styleClasses = styleClasses;
	}

	
	/**
     * Returns true if labels for the property are retrieved from the resource key file.<br> 
     */
    public boolean isRessourceKeyUsed() {
        return isRessourceKeyUsed;
    }

    /**
     * Set the parameter for isRessourceKeyUsed <br>
     * @param ressourceKeyUsed flag if the resource key is used.
     */
    public void setRessourceKeyUsed(boolean ressourceKeyUsed) {
        isRessourceKeyUsed = ressourceKeyUsed;
    }

	/**
	 * 
	 * Return the property {@link #tabIndex}. <br>
	 * 
	 * @return tabIndex
	 */
	
	public String getTabIndex() {
		return tabIndex;
	}

	/**
	 *  Set the property {@link #tabIndex}. <br> 
	 *  
	 * @param tabIndex The tabIndex to set.
	 */
	
	public void setTabIndex(String tabIndex) {
		this.tabIndex = tabIndex;
	}

   
    /**
     * Returns if the object is structured.<br>
     */
    public boolean isStructured() {
    	return false;
    }

	/**
	 * Return the {@link #techKey} of the element. <br> 
	 *
	 * @return Returns the {@link #techKey}.
	 */
	public TechKey getTechKey() {
		return techKey;
	}

	/**
	 * Set the {@link #techKey} of the element. <br>
	 * 
	 * @param techKey The {@link #techKey} to set.
	 */
	public void setTechKey(TechKey techKey) {
		this.techKey = techKey;
	}

	/**
	 * Returns the {@link #uiType} of the element. 
	 * 
	 * @return uiType
	 */
	public String getUiType() {
		return this.uiType;
	}

	/**
	 * Sets the {@link #uiType} of the element
	 * 
	 * @param uiType
	 */
	public void setUiType(String uiType) {
		this.uiType = uiType;
	}
	
	
	
	/**
	 * Returns the object as string. <br> 
	 *
	 * @return String which contains all fields of the object. <br>
	 *
	 */
	public String toString() {

		String str =
			"name: "
				+ name
				+ ", "
				+ "description: "
				+ description
				+ ", "
				+ "hidden: "
				+ isHidden
				+ ", "
				+ "allowed: "
				+ isAllowed
				+ ", "
				+ "level: "
				+ setAllowedLevel
				+ ", "
				+ "uiType: "
				+ uiType
				+ ", "
				+ "accessKey: "
				+ accessKey
				+ ", "
				+ "tabIndex: "
				+ tabIndex
				+ ", "
				+ "styleClasses: "
				+ styleClasses.toString();

		return str;
	}

	/**
	 * Clones the ui element. <br>
	 * <strong>Including objects will not be cloned.<strong>
	 * 
	 * @return copy of the object
	 * 
	 * @see java.lang.Object#clone()
	 */
	public Object clone() throws CloneNotSupportedException {
		UIElementBase newElement = (UIElementBase)super.clone();
		newElement.setTechKey(TechKey.generateKey());
		return newElement;
		
	}

	/**
	 * <p>The inner class <code>Status</code> represent the status of an arbritrary
	 * UI element. </p>
	 *
	 * @author  SAP AG
	 * @version 1.0
	 */
	public class Status implements UIElementStatus {
		
		/**
		 * <p>Description of the status. Not as resource key.</p>
		 */
		protected String description;
		
		/**
		 * <p>Style to display status information.</p> 
		 */
		protected String style;

	    /**
		 * flag if property description text should be retrieved from the resourse key file. <br>
		 */
		protected boolean isRessourceKeyUsed = true;
		
		/**
		 * <p>URL for the image to display status information.</p>
		 */
		protected String imageURL;
		
		/**
		 * Return the property {@link #description}. <br> 
		 *
		 * @return Returns the {@link #description}.
		 */
		public String getDescription() {
			return description;
		}

		/**
		 * Set the property {@link #description}. <br>
		 * 
		 * @param description The {@link #description} to set.
		 */
		public void setDescription(String description) {
			this.description = description;
		}

		/**
		 * Return the property {@link #style}. <br> 
		 *
		 * @return Returns the {@link #style}.
		 */
		public String getStyle() {
			return style;
		}

		/**
		 * Set the property {@link #style}. <br>
		 * 
		 * @param style The {@link #style} to set.
		 */
		public void setStyle(String style) {
			this.style = style;
		} 

		/**
	     * Returns true if labels for the property are retrieved from the resource key file.<br> 
	     * @return flag if the resource key is used to display the description.
	     */
	    public boolean isRessourceKeyUsed() {
	        return isRessourceKeyUsed;
	    }

	    /**
	     * Set the parameter for isRessourceKeyUsed <br>
	     * @param ressourceKeyUsed flag if the resource key is used.
	     */
	    public void setRessourceKeyUsed(boolean ressourceKeyUsed) {
	        isRessourceKeyUsed = ressourceKeyUsed;
	    }

		/**
		 * 
		 * Return the property {@link #imageURL}. <br>
		 * 
		 * @return imageURL
		 */
		public String getImageURL() {
			return imageURL;
		}

		/**
		 *  Set the property {@link #imageURL}. <br>
		 * 
		 * @param imageURL The imageURL to set.
		 */
		public void setImageURL(String imageURL) {
			this.imageURL = imageURL;
		}
	}
	
		
}
