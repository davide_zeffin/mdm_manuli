/*****************************************************************************
    Class:        BackendProperties
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      10.11.2004
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.maintenanceobject.businessobject;

import com.sap.isa.core.util.GenericCacheKey;
import com.sap.isa.maintenanceobject.backend.boi.BackendPropertiesData;


/**
 * The class BackendProperties include ABAP specific attribute like structure, 
 * fieldname and field description name. <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class BackendProperties implements BackendPropertiesData {
    
    /**
     * Name of the field. <br>
     */
    protected String fieldName = "";

    /**
     * Name of the field with a description. <br>
     */
    protected String descriptionFieldName = "";
    
    /**
     * name of a corresponding structure or function module parameter. <br>
     */
    protected String structureName = "";
    
    
    /**
     * Return the property {@link #descriptionFieldName}. <br> 
     *
     * @return Returns the {@link #descriptionFieldName}.
     */
    public String getDescriptionFieldName() {
        return descriptionFieldName;
    }

    
    /**
     * Set the property {@link #descriptionFieldName}. <br>
     * 
     * @param descriptionFieldName The {@link #descriptionFieldName} to set.
     */
    public void setDescriptionFieldName(String descriptionFieldName) {
        this.descriptionFieldName = descriptionFieldName;
    }
    
    
    /**
     * Return the property {@link #fieldName}. <br> 
     *
     * @return Returns the {@link #fieldName}.
     */
    public String getFieldName() {
        return fieldName;
    }
    
    
    /**
     * Set the property {@link #fieldName}. <br>
     * 
     * @param fieldName The {@link #fieldName} to set.
     */
    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }
    
    
    /**
     * Return the property {@link #structureName}. <br> 
     *
     * @return Returns the {@link #structureName}.
     */
    public String getStructureName() {
        return structureName;
    }
    
    
    /**
     * Set the property {@link #structureName}. <br>
     * 
     * @param structureName The {@link #structureName} to set.
     */
    public void setStructureName(String structureName) {
        this.structureName = structureName;
    }
    

	/**
	 * Provides generic key for a backend field element. <br>
	 * 
	 * @return generic key
	 */
	public Object getKey(){
		return new GenericCacheKey(new String[]{structureName,fieldName});
	}
   
   
}
