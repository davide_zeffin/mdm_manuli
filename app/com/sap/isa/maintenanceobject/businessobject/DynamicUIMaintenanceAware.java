/*****************************************************************************
    Interface:    DynamicUIMaintenanceAware
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Author:       SAP
    Created:      21.02.2008
    Version:      1.0

    $Revision: #1 $
    $Date: 2008/01/21 $
*****************************************************************************/
package com.sap.isa.maintenanceobject.businessobject;

/**
 * Allows implementing object interaction with the 
 * {@link com.sap.isa.user.businessobject.DynamicUIMaintenance} object.
 * <br>
 * This interface will be used to enshure that the existing 
 * BusinessObjectManager works with {@link com.sap.isa.user.businessobject.DynamicUIMaintenance}
 * and therefore the user structure bases on the UserBase class.
 *
 * @see com.sap.isa.businessobject.GenericBusinessObjectManager
 *
 * @author D033221
 * @version 1.0
 *
 */

public interface DynamicUIMaintenanceAware {
	
	/**
     * Creates {@link com.sap.isa.user.businessobject.DynamicUIMaintenance} object. 
     * 
     * @return DynamicUIMaintenance created {@link com.sap.isa.maintenanceobject.businessobject.DynamicUIMaintenance} object
     */
	public DynamicUIMaintenance createDynamicUIMaintenance();
	
	/**
     * Retrieves {@link com.sap.isa.maintenanceobject.businessobject.DynamicUIMaintenance} object. 
     * 
     * @return DynamicUIMaintenance 
     */
	public DynamicUIMaintenance getDynamicUIMaintenance();
}
