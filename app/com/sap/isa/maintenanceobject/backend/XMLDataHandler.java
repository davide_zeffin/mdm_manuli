/*****************************************************************************
    Class:        XMLDataHandler
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      February 2002
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.maintenanceobject.backend;

import com.sap.isa.maintenanceobject.backend.boi.*;

import com.sap.isa.core.eai.*;
import com.sap.isa.core.util.*;

import java.util.*;
import java.io.*;

import com.sap.isa.core.TechKey;
import com.sap.isa.core.init.InitializationHandler;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.XMLHelper;

import com.sap.isa.core.util.table.*;

// struts imports
import org.apache.commons.digester.Digester;

/**
 * Read and write the data for a maintenance object in a XML-File.
 * For each object an own file will be used.
 * The name of the file path must be defined in the eai-config file
 * as a parameter with the name <code>OBJECT_FILE_PATH</code>.<br>
 * <b>Example</b>
 * <pre>
 *
 * &lt;businessObject type="shopAdmin" name="shopAdmin" className="com.sap.isa.shopadmin.backend.crm.ShopAdminCRM" connectionFactoryName="JCO" defaultConnectionName="ISAStateful"&gt;
 *   &lt;params&gt;
 *     &lt;param name="Object-Path" value="C:\ISales\shops"/&gt;
 *   &lt;/params&gt;
 * &lt;/businessObject&gt;
 * </pre>
 *
 *
 * @author Wolfgang Sattler
 * @version 1.0
 *
 */
public class XMLDataHandler implements MaintenanceObjectDataHandler {
	/**
	 * reference to the IsaLocation
	 *
	 * @see com.sap.isa.core.logging.IsaLocation
	 */
	private static final IsaLocation log =
		IsaLocation.getInstance(XMLDataHandler.class.getName());

	/** 
	 * Encoding used in the xml-file 
	 * 
	 */
	protected String ENCODING = "UTF-8";

	/**
	 *
	 * List with addional properties which should be stored with the object-list
	 * Only this properties could be used by search as additional properties
	 *
	 */
	protected List additionalProperties = new ArrayList();

	private static String PATH_SEPARATOR = System.getProperty("file.separator");
	private static String LIST_FILE_NAME = "_objectList";
	private static char ADD_OBJECT = 'A';
	private static char CHANGE_OBJECT = 'C';

	private String filePath = "";
	private String objectListFileName;
	private int debugLevel = 0;
	private XMLHelper xmlHelper = new XMLHelper();
	private Properties properties;
	private SortedMap objects;
	private FileInputStream inputStream;
	
	protected static String FILE_EXTENSION = ".XML";

	/**
	 * global constant for the Paramter defined in the directory for the objects.
	 *
	 */
	public static String OBJECT_FILE_PATH = "Object-Path";

	/**
	 * Set the debugLevel for the xml parse process
	 *
	 * @param debugLevel
	 *
	 */
	public void setDebugLevel(int debugLevel) {
		this.debugLevel = debugLevel;
	}

	/**
	 * This method initialize the maintenance object handler in the backend.
	 *
	 * @param connectionFactory to get connections
	 * @param props a set of properties which may be useful to initialize the object
	 */
	public void initHandler(
		ConnectionFactory connectionFactory,
		Properties props)
		throws MaintenanceObjectHelperException {
		final String METHOD = "initHandler()";
		log.entering(METHOD);
		String pathName = props.getProperty(OBJECT_FILE_PATH);

		if (pathName != null && pathName.length() > 0) {

			filePath = pathName + PATH_SEPARATOR;
			log.debug("Used filePath is: " + filePath);

		} else {
			log.error(
				LogUtil.APPS_COMMON_CONFIGURATION,
				"shopadmin.error.noPathName");
		}

		objectListFileName = filePath + LIST_FILE_NAME + FILE_EXTENSION;
		log.exiting();
	}

	/**
	 * Reads a list of objects and returns this list
	 * using a tabluar data structure.
	 *
	 * @param objectId object id as search pattern
	 * @param props, additional properties to restrict the search result
	 * @return List of all available objects
	 */
	public ResultData readObjectList(String objectId, Properties props)
		throws MaintenanceObjectHelperException {
		final String METHOD = "readObjectList()";
		log.entering(METHOD);
		boolean readAll = (objectId.length() == 0 || objectId.equals("*"));

		List searchTokens = null;

		if (props != null) {
			log.debug(
				"Search for objectId "
					+ objectId
					+ "and properties: "
					+ props.toString());
		} else {
			log.debug("Search for objectId " + objectId);
		}

		Table objectTable = new Table("Objects");

		objectTable.addColumn(Table.TYPE_STRING, MaintenanceObjectData.ID);
		objectTable.addColumn(
			Table.TYPE_STRING,
			MaintenanceObjectData.DESCRIPTION);

		readObjectList();

		Iterator iter = objects.values().iterator();

		if (!readAll) {
			// get the serach tokens for the regualar search
			searchTokens = getSearchTokens(objectId);
		}

		while (iter.hasNext()) {

			XMLObject object = (XMLObject) iter.next();

			if (checkProperties(object, props)
				&& (readAll
					|| simpleRegularSearch(searchTokens, object.getId()))) {

				TableRow objectRow = objectTable.insertRow();

				objectRow.setRowKey(new TechKey(object.getId()));
				objectRow.getField(MaintenanceObjectData.ID).setValue(
					object.getId());
				objectRow.getField(MaintenanceObjectData.DESCRIPTION).setValue(
					object.getDescription());
			}
		}
		log.exiting();
		return new ResultData(objectTable);
	}

	private boolean checkProperties(XMLObject object, Properties props) {

		if (props != null) {
			Iterator iter = props.keySet().iterator();

			while (iter.hasNext()) {
				String key = (String) iter.next();

				String value1 = object.getProperty(key);
				String value2 = props.getProperty(key);

				if (value1 != null
					&& value2 != null
					&& !value1.equals(value2)) {
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * This method deletes a maintenance object with the key <code>objectKey</code>
	 * from the database
	 *
	 * @param objectKey <code>TechKey</code> of the object
	 */
	public void deleteObject(TechKey objectKey)
		throws MaintenanceObjectHelperException {
		final String METHOD = "deleteObject()";
		log.entering(METHOD);
		String objectId = objectKey.getIdAsString();
		if (log.isDebugEnabled())
			log.debug("Object id=" + objectId);
		// First delete object from object list
		readObjectList();

		XMLObject object = (XMLObject) objects.get(objectId);

		if (object != null) {
			objects.remove(objectId);
		}

		saveObjectListSynchronized(this);

		// then delete the file with object data
		String fileName = filePath + objectId + FILE_EXTENSION;

		File file = new File(fileName);

		log.debug("Delete object-file: " + fileName);

		// first check if file exists
		if (file.exists()) {
			// then delete it.
			file.delete();
		}
		log.exiting();
	}

	/**
	 * This method read the values of the maintenance object from the backend
	 *
	 * @param maintenanceObject
	 */
	public void readData(MaintenanceObjectData maintenanceObject)
		throws MaintenanceObjectHelperException {

		final String METHOD = "readData()";
		log.entering(METHOD);
		String fileName = filePath + maintenanceObject.getId() + FILE_EXTENSION;

		properties = new Properties();

		parseXMLFile(fileName);

		readObjectList();

		XMLObject object = (XMLObject) objects.get(maintenanceObject.getId());

		if (object != null) {
			maintenanceObject.setDescription(object.getDescription());
		}

		Iterator groupIterator = maintenanceObject.iterator();

		while (groupIterator.hasNext()) {

			Iterator properyIterator =
				((PropertyGroupData) groupIterator.next()).iterator();

			while (properyIterator.hasNext()) {
				setPropertyValue((PropertyData) properyIterator.next());
			}
		}
		log.exiting();
	}

	/**
	 * This method save the maintenance object in an xML-File with the name of the
	 * id of the object. The file will be stored in the path, which must be given
	 * within the initialization.
	 *
	 * @param maintenanceObject the object, which should be saved
	 */
	public void saveData(MaintenanceObjectData maintenanceObject)
		throws MaintenanceObjectHelperException {

		final String METHOD = "saveData()";
		log.entering(METHOD);
		if (maintenanceObject.isNewObject()) {
			maintenanceObject.setId(maintenanceObject.getId().toUpperCase());
			writeObjectToList(maintenanceObject, ADD_OBJECT);
		} else {
			writeObjectToList(maintenanceObject, CHANGE_OBJECT);
		}

		if (!maintenanceObject.isValid()) {
			if (log.isDebugEnabled())
				log.debug("to be saved object is not valid, return");
			log.exiting();
			return;
		}

		String fileName = filePath + maintenanceObject.getId() + FILE_EXTENSION;

		log.debug("Save Object-file: " + fileName);

		StringBuffer sb = new StringBuffer();

		try {
			FileOutputStream fileOutputStrem = new FileOutputStream(fileName);

			// create file writer to get the encoding
			OutputStreamWriter fileWriter =
				new OutputStreamWriter(fileOutputStrem, ENCODING);

			sb.append("<?xml version=\"1.0\" encoding=\"").append(
				ENCODING).append(
				"\"?>");

			sb.append(xmlHelper.createTag("object", false));

			Iterator groupIterator = maintenanceObject.iterator();

			while (groupIterator.hasNext()) {

				Iterator properyIterator =
					((PropertyGroupData) groupIterator.next()).iterator();

				while (properyIterator.hasNext()) {
					appendProperty(sb, (PropertyData) properyIterator.next());
				}
			}
			sb.append(xmlHelper.createEndTag("object"));

			// write the XML File
			fileWriter.write(sb.toString(), 0, sb.length());
			fileWriter.close();
		} catch (Exception ex) {
			String errMsg = ex.toString();
			log.error(
				LogUtil.APPS_COMMON_CONFIGURATION,
				"mob.dh.writeFile",
				new Object[] { fileName, errMsg },
				null);
			MaintenanceObjectHelperException wrapperEx =
				new MaintenanceObjectHelperException(
					new Message(Message.ERROR, "mob.error.saveData"),
					errMsg);
			log.throwing(wrapperEx);
			throw wrapperEx;
		} finally {
			log.exiting();
		}
	}

	/* first simplest implementation for the pattern search */
	private boolean simpleRegularSearch(List tokens, String testString) {

		int searchIndex = 0;
		boolean lastWasStar = false;

		for (int i = 0; i < tokens.size(); i++) {

			String token = (String) tokens.get(i);

			if (token.equals("*")) {
				lastWasStar = true;
				continue;
			}

			searchIndex = testString.indexOf(token, searchIndex);

			if (searchIndex == -1 || (!lastWasStar && searchIndex > 0)) {
				// the token could found
				return false;
			}
			searchIndex += token.length();
			lastWasStar = false;
		}

		if (!lastWasStar && searchIndex != testString.length()) {
			return false;
		}

		return true;
	}

	/* first simplest implementation for the pattern search */
	private List getSearchTokens(String pattern) {

		String pat = pattern.toUpperCase();
		List tokens = new ArrayList();

		int wildCardIndex, startIndex = 0, endIndex;

		// search all tokens given with the search pattern
		do {
			wildCardIndex = pat.indexOf('*', startIndex);

			if (wildCardIndex != -1) {
				endIndex = wildCardIndex;
			} else {
				endIndex = pat.length();
			}

			if (wildCardIndex == 0) {
				tokens.add("*");
			}

			if (endIndex > startIndex) {
				tokens.add(pat.substring(startIndex, endIndex));
			}

			if (wildCardIndex > 0) {
				tokens.add("*");
			}

			startIndex = wildCardIndex + 1;

		} while (wildCardIndex != -1);

		return tokens;
	}

	/* private method to parse the config-file */
	private void parseXMLFile(String fileName)
		throws MaintenanceObjectHelperException {

		// new Struts digester
		Digester digester = new Digester();

		digester.setDebug(debugLevel);

		digester.push(this);

		// set all properties for the object
		//digester.addSetProperties("object");

		// create a new property
		digester.addObjectCreate(
			"object/property",
			XMLProperty.class.getName());

		// set all properties for the object
		digester.addSetProperties("object/property");

		// add property to the maintenance object
		digester.addSetNext(
			"object/property",
			"addProperty",
			XMLProperty.class.getName());

		try {
			log.debug("Parse Object-File: " + fileName);

			if (fileName == null || fileName.length() > 0) {

				File file = new File(fileName);

				if (!file.exists()) {
					if (log.isDebugEnabled()) {
						log.debug(
							"not found as absolute path, try relative path: "
								+ fileName);
					}
					fileName = InitializationHandler.getRealPath(fileName);
				}
				FileInputStream inputStream = new FileInputStream(fileName);
				digester.parse(inputStream);
			} else {
				String errMsg =
					"Error reading maintenance object: No file name set";
				log.error(LogUtil.APPS_COMMON_CONFIGURATION, "mob.dh.noFile");
				MaintenanceObjectHelperException wrapperEx =
					new MaintenanceObjectHelperException(
						new Message(Message.ERROR, "mob.error.readData"),
						errMsg);
				log.throwing(wrapperEx);
				throw wrapperEx;
			}
		} catch (Exception ex) {
			String errMsg = ex.toString();
			log.error(
				LogUtil.APPS_COMMON_CONFIGURATION,
				"mob.dh.readFile",
				new Object[] { fileName, errMsg },
				null);
			MaintenanceObjectHelperException wrapperEx =
				new MaintenanceObjectHelperException(
					new Message(Message.ERROR, "mob.error.readData"),
					errMsg);
			log.throwing(wrapperEx);
			throw wrapperEx;
		}
	}

	/*
	 * A private method, which set the property in the object, with property value
	 * which is found in the XML- File
	 *
	 */
	private void setPropertyValue(PropertyData property) {

		String value = properties.getProperty(property.getName());

		if (value != null) {
			if (property.getType().equals(PropertyData.TYPE_STRING)) {
				property.setValue(value);
			} else if (property.getType().equals(PropertyData.TYPE_BOOLEAN)) {
				property.setValue(Boolean.valueOf(value));
			} else if (property.getType().equals(PropertyData.TYPE_INTEGER)) {
				property.setValue(Integer.valueOf(value));
			} else {
				property.clearValue();
			}
		} else {
			property.clearValue();
		}
	}

	/*
	 * A private method, which writes a property into the XML-file
	 */
	private void appendProperty(StringBuffer sb, PropertyData property) {

		Properties attributes = new Properties();

		attributes.setProperty("name", property.getName());

		if (property.isSimpleType()) {
			attributes.setProperty("value", property.getValue().toString());
			sb.append("  ").append(
				xmlHelper.createElement("property", "", attributes));
		}
	}

	/*
	 * Private method to save the object in the list, for adding the object and
	 * changing the description
	 *
	 * Note: Access to the object list will be done over a synchronized methods
	 *
	 */
	private void writeObjectToList(
		MaintenanceObjectData maintenanceObject,
		char mode)
		throws MaintenanceObjectHelperException {

		XMLObject object =
			new XMLObject(
				maintenanceObject.getId(),
				maintenanceObject.getDescription());

		// extend with additional properties
		extendXMLObject(object, maintenanceObject);

		readObjectList();
		if (mode == ADD_OBJECT) {
			if (objects.get(object.getId()) != null) {
				String args[] = { object.getId()};
				maintenanceObject.addMessage(
					new Message(
						Message.ERROR,
						"mob.error.objectexist",
						args,
						null));
			} else {
				objects.put(object.getId(), object);
				saveObjectListSynchronized(this);
			}
		}
		if (mode == CHANGE_OBJECT) {
			objects.put(object.getId(), object);
			saveObjectListSynchronized(this);
		}
	}

	/* private method to read the object list */
	private void readObjectList() throws MaintenanceObjectHelperException {

		objects = new TreeMap();

		// new Struts digester
		Digester digester = new Digester();

		digester.setDebug(debugLevel);

		digester.push(this);

		// set all properties for the object
		digester.addSetProperties("object-list");

		// create a new property
		digester.addObjectCreate(
			"object-list/object",
			XMLObject.class.getName());

		// set all properties for the object
		digester.addSetProperties("object-list/object");

		// add property to the maintenance object
		digester.addSetNext(
			"object-list/object",
			"addObject",
			XMLObject.class.getName());

		// create a new property
		digester.addObjectCreate(
			"object-list/object/property",
			XMLProperty.class.getName());

		// set all properties for the object
		digester.addSetProperties("object-list/object/property");

		// add property to the maintenance object
		digester.addSetNext(
			"object-list/object/property",
			"addProperty",
			XMLProperty.class.getName());

		// check if the file exist.
		File file = new File(objectListFileName);
		if (!file.exists()) {
				if (log.isDebugEnabled()) {
					log.debug(
						"not found as absolute path, try relative path: "
							+ objectListFileName);
				}
				objectListFileName = InitializationHandler.getRealPath(objectListFileName);
				file = new File(objectListFileName);
				if (!file.exists()){
					String errMsg = "Objectlist-file: "	+ objectListFileName + " does not exist";
					log.debug(errMsg);

					MaintenanceObjectHelperException wrapperEx =
						new MaintenanceObjectHelperException(
							new Message(Message.ERROR, "mob.error.readData"),
						errMsg);
					log.throwing(wrapperEx);
					throw wrapperEx;
				}
		}
		
		try {
			log.debug("Parse objectlist-file: " + objectListFileName);

			InputStream inputStream = new FileInputStream(file);
			digester.parse(inputStream);
		} catch (Exception ex) {
			String errMsg = ex.toString();
			log.error(
				LogUtil.APPS_COMMON_CONFIGURATION,
				"mob.dh.readFile",
				new Object[] { objectListFileName, errMsg },
				null);
			MaintenanceObjectHelperException wrapperEx =
				new MaintenanceObjectHelperException(
					new Message(Message.ERROR, "mob.error.readData"),
					errMsg);
			log.throwing(wrapperEx);
			throw wrapperEx;
		}
	}

	/*
	 * Private method to save the object in the list, for adding the object and
	 * changing the description
	 *
	 * Note: Access to the object list will be done over a synchronized methods
	 *
	 */
	private synchronized static void saveObjectListSynchronized(XMLDataHandler xmlDataHandler)
		throws MaintenanceObjectHelperException {
		final String METHOD = "saveObjectListSynchronized()";
		log.entering(METHOD);
		xmlDataHandler.saveObjectList();
		log.exiting();
	}

	private void saveObjectList() throws MaintenanceObjectHelperException {

		StringBuffer sb = new StringBuffer();

		// create file path if necessary
		File file = new File(objectListFileName);

		if (!file.exists()) {
			log.debug(
				"Objectlist-file: " + objectListFileName + " must be created");

			File path = file.getParentFile();
			path.mkdirs();
		}

		try {

			FileOutputStream fileOutputStrem = new FileOutputStream(file);

			// create file writer to get the encoding
			OutputStreamWriter fileWriter =
				new OutputStreamWriter(fileOutputStrem, ENCODING);

			sb.append("<?xml version=\"1.0\" encoding=\"").append(
				ENCODING).append(
				"\"?>");

			sb.append(xmlHelper.createTag("object-list", false));

			Iterator iter = objects.values().iterator();

			while (iter.hasNext()) {

				XMLObject object = (XMLObject) iter.next();

				Properties attributes = new Properties();

				attributes.setProperty("id", object.getId());

				attributes.setProperty("description", object.getDescription());

				if (additionalProperties.size() > 0) {
					sb.append("  ");
					sb.append(xmlHelper.createTag("object", attributes, false));

					for (int i = 0; i < additionalProperties.size(); i++) {
						appendXMLProperty(
							sb,
							object,
							(String) additionalProperties.get(i));
					}
					sb.append(xmlHelper.createEndTag("object"));
				} else {
					sb.append(
						xmlHelper.createElement("object", "", attributes));
				}
			}

			sb.append(xmlHelper.createEndTag("object-list"));

			log.debug("Save objectlist-file: " + objectListFileName);
			// write the XML file
			fileWriter.write(sb.toString(), 0, sb.length());
			fileWriter.close();
		} catch (Exception ex) {
			String errMsg = ex.toString();
			log.error(
				LogUtil.APPS_COMMON_CONFIGURATION,
				"mob.dh.writeFile",
				new Object[] { objectListFileName, errMsg },
				null);
			MaintenanceObjectHelperException wrapperEx =
				new MaintenanceObjectHelperException(
					new Message(Message.ERROR, "mob.error.saveData"),
					errMsg);
			log.throwing(wrapperEx);
			throw wrapperEx;
		}
	}

	/*
	 * A private method, which writes a property into the XML-file
	 */
	private void appendXMLProperty(
		StringBuffer sb,
		XMLObject object,
		String key) {

		Properties attributes = new Properties();

		attributes.setProperty("name", key);
		String value = object.getProperty(key);

		if (value != null) {
			attributes.setProperty("value", value);
			sb.append("    ");
			sb.append(xmlHelper.createElement("property", "", attributes));
		}
	}

	/*
	 * A private method, which writes a property into the XML-file
	 */
	private void extendXMLObject(
		XMLObject object,
		MaintenanceObjectData maintenanceObject) {

		for (int i = 0; i < additionalProperties.size(); i++) {

			PropertyData property =
				maintenanceObject.getProperty(
					(String) additionalProperties.get(i));

			if (property != null && property.isSimpleType()) {
				object.addProperty(
					property.getName(),
					property.getValue().toString());
			}
		}
	}

	/**
	 * add property method for digister
	 *
	 * @param property property to add the the local property list
	 */
	public void addProperty(XMLProperty property) {

		properties.setProperty(property.getName(), property.getValue());
	}

	/**
	 * add object method for digister
	 *
	 * @param object object to add the the local property list
	 */
	public void addObject(XMLObject object) {

		objects.put(object.getId(), object);
	}

	/**
	 * This Class is needed for parsing the XML-Files
	 * It stores temporary a property found in the XML-File
	 *
	 */
	public static class XMLProperty {

		private String name;
		private String value;

		/**
		 *
		 * standard construtor
		 *
		 */
		public XMLProperty() {

		}

		/**
		 * Set the property name
		 *
		 * @param name
		 *
		 */
		public void setName(String name) {
			this.name = name;
		}

		/**
		 * Returns the property name
		 *
		 * @return name
		 *
		 */
		public String getName() {
			return this.name;
		}

		/**
		 * Set the property value
		 *
		 * @param value
		 *
		 */
		public void setValue(String value) {
			this.value = value;
		}

		/**
		 * Returns the property value
		 *
		 * @return value
		 *
		 */
		public String getValue() {
			return this.value;
		}
	}

	/**
	 * this Class is needed for parsing the XML-Files
	 * It store temporary a object found in the XML-File
	 *
	 */
	public static class XMLObject {

		private String id;
		private String description;

		/**
		 * standard construtor
		 *
		 */
		public XMLObject() {

		}

		private Properties properties = new Properties();

		/**
		 * standard construtor
		 *
		 */
		private XMLObject(String id, String description) {
			this.id = id;
			this.description = description;
		}

		/**
		 * addProperty method for digister
		 *
		 * @param property property to add the the local property list
		 */
		public void addProperty(XMLProperty property) {
			properties.setProperty(property.getName(), property.getValue());
		}

		/**
		 * addProperty method for digister
		 *
		 * @param propertyName name of the property to add to the local property list
		 * @param value value of the property to add to the local property list
		 */
		public void addProperty(String propertyName, String value) {
			properties.setProperty(propertyName, value);
		}

		public String getProperty(String key) {
			return properties.getProperty(key);
		}

		/**
		 * Set the property id
		 *
		 * @param id
		 *
		 */
		public void setId(String id) {
			this.id = id;
		}

		/**
		 * Returns the property id
		 *
		 * @return id
		 *
		 */
		public String getId() {
			return this.id;
		}

		/**
		 * Set the property description
		 *
		 * @param description
		 *
		 */
		public void setDescription(String description) {
			this.description = description;
		}

		/**
		 * Returns the property description
		 *
		 * @return description
		 *
		 */
		public String getDescription() {
			return this.description;
		}

	}
}
