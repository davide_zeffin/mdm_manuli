/*
 * Created on Dec 2, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.maintenanceobject.backend;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Properties;
import java.util.TreeMap;

import com.sap.isa.core.TechKey;
import com.sap.isa.core.db.DBHelper;
import com.sap.isa.core.db.DBQuery;
import com.sap.isa.core.db.DBQueryResult;
import com.sap.isa.core.db.jdbc.JdbcDAO;
import com.sap.isa.core.db.jdbc.JdbcPersistenceManager;
import com.sap.isa.core.db.jdbc.JdbcPersistenceManagerFactory;
import com.sap.isa.core.eai.ConnectionFactory;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.core.util.table.TableRow;
import com.sap.isa.maintenanceobject.backend.boi.MaintenanceObjectData;
import com.sap.isa.maintenanceobject.backend.boi.PropertyData;
import com.sap.isa.maintenanceobject.backend.boi.PropertyGroupData;
import com.sap.isa.maintenanceobject.businessobject.MaintenanceObject;

/**
 * Read and write the data for a maintenance object in a J2EE data base.
 * The name of the data source must be defined in the eai-config file
 * as a parameter with the name <code>DATA_SRC</code>.<br>
 * <b>Example</b>
 * <pre>
 *
 * &lt;businessObject type="shopAdmin" name="shopAdmin" className="com.sap.isa.shopadmin.backend.crm.ShopAdminR3" 
 * 	connectionFactoryName="JCO" defaultConnectionName="ISAStateful"&gt;
 *   &lt;params&gt;
 *     &lt;param name="DATA_SRC" value="jdbc/crmjdbcpool"/&gt;
 *   &lt;/params&gt;
 * &lt;/businessObject&gt;
 * </pre>
 *
 * @author SAP
 * @version 1.0
 *
 */
public abstract class J2EEPersistencyDataHandler implements MaintenanceObjectDataHandler {



	private static final IsaLocation log = IsaLocation.
			  getInstance(J2EEPersistencyDataHandler.class.getName());

	/**
	 * Contstant for the init property for the data source. 
	 */
	public static final String DATA_SRC = "J2EE_DATA_SRC";

	/**
	 * Contstant for the init property for the data source. 
	 */
	public static final String DATA_SOURCE = "java:comp/env/SAP/CRM_ISA_SHP";

	/**
	 * Contstant for the init property for the used client. 
	 */
	public static final String CLIENT = "client";

	/**
	 * Contstant for the init property for used system id. 
	 */
	public static final String SYSTEM = "system";

	public static final String DESCRIPTION = "description";
	
	
	protected TreeMap keyValues;
	protected JdbcPersistenceManagerFactory pmfactory;
	
	protected ArrayList additionalProperties = new ArrayList();

	/** 
	 * used client. <br>
	 */
	protected String client = null;
	
	/** 
	 * used systemId. <br>
	 */
	protected String systemId = null;

	protected String HEADER = "ObjectData-HEADER";
	
	protected String CHILDREN = "ObjectData-CHILDREN";
	
	protected String ID = "ObjectID";
	
	
	/**
	 * This method initialize the maintenance object handler in the backend.
	 *
	 * @param connectionFactory to get connections
	 * @param props a set of properties which may be useful to initialize the object
	 */
	public void initHandler(ConnectionFactory connectionFactory,
							 Properties props)
			throws MaintenanceObjectHelperException {
			
		final String METHOD = "initHandler()";
		log.entering(METHOD);
		
		adjustConstants();
		
		client = props.getProperty(J2EEPersistencyDataHandler.CLIENT);
		systemId = props.getProperty(J2EEPersistencyDataHandler.SYSTEM);     
			
		try  {
			Properties properties = new Properties();
			properties.put(DBHelper.DB_DATASOURCE , DATA_SOURCE);
			properties.put(DBHelper.DB_PMF,JdbcPersistenceManagerFactory.class.getName());
			pmfactory = (JdbcPersistenceManagerFactory)
				DBHelper.getPersistenceManagerFactory(properties);
			if(null == pmfactory)  {
				String errorMessage = "Unable to initialized database persistency";
				log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, errorMessage);
				throw exception(errorMessage , "shopData.initialize");
			}  
		}
		catch(Exception exc)  {
			log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "Unable to initialize db" , exc);
			log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "Sample test");
			throw exception("Unable to initialize DB" , "shopdata.dbinitialize"); 
		}
		finally {
			log.exiting();
		}
	}

	
	/**
	 * 
	 * Use this methods to overwrite the constants {@link #HEADER} 
	 * {@link #CHILDREN} {@link #ID} . <br>
	 *
	 */
	protected abstract void adjustConstants();

	/**
	 * @return
	 */
	protected MaintenanceObjectHelperException exception(String msg , String resourceId) {
		
		MaintenanceObjectHelperException exc = new MaintenanceObjectHelperException(new Message(Message.ERROR,
															   resourceId),
												   msg);
	    return exc;
	}


	/* (non-Javadoc)
	 * @see com.sap.isa.maintenanceobject.backend.MaintenanceObjectDataHandler#readObjectList(java.lang.String, java.util.Properties)
	 */
	public TreeMap readObjects(Properties props , Properties systemProps)
		throws MaintenanceObjectHelperException {
		
		final String METHOD = "readObjects()";
		log.entering(METHOD);
		Table objects = new Table("objects");
		
		objects.addColumn(Table.TYPE_STRING , MaintenanceObjectData.ID);
		objects.addColumn(Table.TYPE_STRING , MaintenanceObjectData.DESCRIPTION);
		TreeMap groups = new TreeMap();
		JdbcPersistenceManager pm = null;		
		try  {
			pm = (JdbcPersistenceManager)
									pmfactory.getDBPersistenceManager();
			
			DBQuery query = KeyValDAO.getQuery(props , systemProps , pm);
			DBQueryResult result = query.execute(getParameterValues(systemProps, props));
			if(result.size() > 0)  {
				KeyValDAO obj = null;
				result.position(1);
				TreeMap group = null;
				while(result.hasNext())  {
					obj = (KeyValDAO)result.next();
					group = (TreeMap)groups.get(obj.getGroupName());
					if(null == group)  {
						group = new TreeMap();
						groups.put(obj.getGroupName() , group);
					}
					group.put(obj.getDataSource() + "-" + obj.getPropName() , obj);
					obj.getPersistenceManager().detach(obj);
				}
			}
		}  catch(Exception exc)  {
			String msg = "Retrieval of shop data failed";
			log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, msg , exc);
			throw exception(msg , "");
		} finally  {
			if(pm != null)  {
				pm.close();
			}
			log.exiting();
		}
		return groups;
	}

	/**
	 * @param systemProps
	 * @param props
	 * @return
	 */
	private Object[] getParameterValues(Properties verticalProps, Properties horiProps) {
		ArrayList list = new ArrayList();
		Enumeration enumeration;
		if(verticalProps != null)  {
			enumeration = verticalProps.elements();
			while(enumeration.hasMoreElements())  {
				list.add(enumeration.nextElement());
			}
		}
		if(horiProps != null)  {
			enumeration = horiProps.elements();
			while(enumeration.hasMoreElements())  {
				list.add(enumeration.nextElement());
			}
		}
		return list.toArray();
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.maintenanceobject.backend.MaintenanceObjectDataHandler#deleteObject(com.sap.isa.core.TechKey)
	 */
	public void deleteObject(TechKey objectKey)
		throws MaintenanceObjectHelperException {
		
		final String METHOD = "deleteObject()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("objectKey="+objectKey);
		JdbcPersistenceManager pm = null;
		try  {
			pm = startTransaction();
			KeyValDAO keyVal = new KeyValDAO();
			keyVal.setGroupName(objectKey.toString());
			keyVal.initializeKey();
			pm.attach(keyVal);
			pm.delete(keyVal);
			pm.getTransaction().commit();
			
		}  catch(Exception exc)  {
			final String msg = "Unable to delete the Keyvalues - Shop ";
			log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, msg + objectKey, exc);
			rollbackTransaction(pm);
			throw exception(msg + objectKey, "");
		} finally {
			log.exiting();
		}

	}


	/* (non-Javadoc)
	 * @see com.sap.isa.maintenanceobject.backend.MaintenanceObjectDataHandler#saveData(com.sap.isa.maintenanceobject.backend.boi.MaintenanceObjectData)
	 */
	public void saveData(Collection oldDaos , Collection newDaos)
		throws MaintenanceObjectHelperException {
		final String METHOD = "saveData()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("isNew="+ (oldDaos != null && oldDaos.size() > 0));
		if((oldDaos != null && oldDaos.size() > 0)
			|| (newDaos != null && newDaos.size() > 0))  {
			JdbcPersistenceManager pm = null;
			try  {
				pm = startTransaction();
				if(oldDaos != null)  {
					attach(oldDaos , pm);
					pm.saveAll(oldDaos);
				}
				if(newDaos != null)
					pm.saveAll(newDaos);
				commitTransaction(pm);
				if(oldDaos != null)
					detach(oldDaos);
				if(newDaos != null)
					detach(newDaos);
				closeTransaction(pm);
			}  catch(Exception exc)  {
				final String msg = "Unable to save the Keyvalues";
				log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, msg , exc);
				rollbackTransaction(pm);
				throw exception(msg , "");
			} finally  {
				closeTransaction(pm);
				log.exiting();
			}
		}

	}
	
	

	/* (non-Javadoc)
	 * @see com.sap.isa.maintenanceobject.backend.MaintenanceObjectDataHandler#readObjectList(java.lang.String, java.util.Properties)
	 */
	public ResultData readObjectList(String objectId, 
					Properties props)
					throws MaintenanceObjectHelperException {
		final String METHOD = "readObjectList()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("objectId="+objectId);
		if(null == props)  {
			props = new Properties();
		}
		Properties systemProps = new Properties();
		systemProps.put(KeyValDAO.CLIENT , client);
		systemProps.put(KeyValDAO.SYSTEMID , systemId);
		if(objectId != null && objectId.trim().length() > 0)
		// D041773 Database query does not get any results if the name of the XCM configuration
		// contains letters in lower case. Message 724679 2006
			systemProps.put(KeyValDAO.GRP_NAME, objectId.toUpperCase());
		systemProps.put(KeyValDAO.PROP_NAME , DESCRIPTION);
		//insertion CHHI 20050119: the type of the header is also important
		systemProps.put(KeyValDAO.DATA_TYPE, HEADER);
		try  {
			TreeMap groups = readObjects(props , systemProps);
			Table objects = new Table("objectList");
			if(groups != null && groups.size() > 0)  {
				objects.addColumn(Table.TYPE_STRING , MaintenanceObject.ID);
				objects.addColumn(Table.TYPE_STRING , MaintenanceObject.DESCRIPTION);
			
				Iterator iterate = groups.keySet().iterator();
				String shopName = null;
				KeyValDAO keyValue = null;
				TableRow row = null;
				TreeMap properties = null;
				while(iterate.hasNext())  {
					shopName = (String)iterate.next(); 
					properties = (TreeMap)groups.get(shopName);
					row = objects.insertRow();
					
					keyValue = (KeyValDAO)properties.get(HEADER + "-" + DESCRIPTION);
					row.setRowKey(new TechKey(keyValue.getGroupName()));
					row.getField(MaintenanceObject.ID).setValue(keyValue.getGroupName());
					row.getField(MaintenanceObject.DESCRIPTION).setValue(keyValue.getPropValue());
				}
			}
			return new ResultData(objects);
		}  catch(Exception exc)  {
			log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "Unable to retrieve shop data" , exc);
			throw exception("Unable to retrieve shop data" , "shopdata.searchfailed");
		}  catch(Throwable th)  {
			log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "Unable to retrieve shop data" , th);
			throw exception("Unable to retrieve shop data" , "shopdata.searchfailed");
		} finally {
			log.exiting();
		}
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.maintenanceobject.backend.MaintenanceObjectDataHandler#readData(com.sap.isa.maintenanceobject.backend.boi.MaintenanceObjectData)
	 */
	public void readData(MaintenanceObjectData maintenanceObject)
		throws MaintenanceObjectHelperException {
			
		final String METHOD = "readData()";
		log.entering(METHOD);
		Properties systemProps = new Properties();
		systemProps.put(KeyValDAO.CLIENT , client);
		systemProps.put(KeyValDAO.SYSTEMID , systemId);
		
		// D041773 Database query does not get any results if the name of the XCM configuration
		// contains letters in lower case. Message 724679 2006
		String objectId = maintenanceObject.getId().trim().toUpperCase();
		
		if(objectId != null && objectId.trim().length() > 0)
			systemProps.put(KeyValDAO.GRP_NAME, objectId);
		try  {
			TreeMap groups = readObjects(null , systemProps);
			// D041773 Here should be the objectID used.
//			TreeMap properties = (TreeMap)groups.get(maintenanceObject.getId().trim());
			TreeMap properties = (TreeMap)groups.get(objectId);
			if(properties == null)  {
				return;
			}
			keyValues = properties;
			Iterator groupIterator = maintenanceObject.iterator();

			maintenanceObject.setDescription(((KeyValDAO)properties.get(HEADER + "-" + DESCRIPTION)).getPropValue());
			while (groupIterator.hasNext()) {

				Iterator properyIterator = ((PropertyGroupData) groupIterator.next()).iterator();

				while (properyIterator.hasNext()) {
					setPropertyValue(properties , (PropertyData)properyIterator.next());
				}
			}
		}  catch(Exception exc)  {
			log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "Unable read the shop data" , exc);
			throw exception("Unable to read the shop data" , "shopdata.readfailed");
		} finally {
			log.exiting();
		}
	}

	/**
	 * @param data
	 */
	private void setPropertyValue(TreeMap props , PropertyData data) {

		if(props != null && data != null)  {
			log.info(LogUtil.APPS_COMMON_INFRASTRUCTURE, "Setting shop attribute " + data.getName());
			KeyValDAO keyVal = (KeyValDAO)props.get(CHILDREN + "-" + data.getName());
			if(null == keyVal)  {
				log.info(LogUtil.APPS_COMMON_INFRASTRUCTURE, "Unknown attrbiute " + data.getName());
				data.clearValue();
				return;
			}
			String value = keyVal.getPropValue();
			if (data.getType().equals(PropertyData.TYPE_STRING)) {
				data.setValue(value);
			}
			else if (data.getType().equals(PropertyData.TYPE_BOOLEAN)) {
				data.setValue(Boolean.valueOf(value));
			}
			else if (data.getType().equals(PropertyData.TYPE_INTEGER)) {
				data.setValue(Integer.valueOf(value));
			}  else  {
				data.clearValue();		
			}
			
		}  else  {
			data.clearValue();
		}
		
	}
	
	/**
	 * Creates the given object on the database {@link #}. <br>
	 * 
	 * @param maintenanceObject
	 * @throws MaintenanceObjectHelperException
	 */
	protected void createObject(MaintenanceObjectData maintenanceObject)
			throws MaintenanceObjectHelperException  {
			
		final String METHOD = "createObject()";		
		log.entering(METHOD);
		
		if (log.isDebugEnabled())
			log.debug("to be created object="+maintenanceObject);
		KeyValDAO keyVal = null;
		ArrayList daos = new ArrayList();
		try  {
			
			checkDuplicateData(maintenanceObject);
			
			if(!maintenanceObject.isValid())  {
				log.exiting();
				return;
			}
			
			//Add the Identifier and Description rows
			keyVal = newKeyValDAO(maintenanceObject , true , ID , maintenanceObject.getId().trim());
			daos.add(keyVal);
			keyVal = newKeyValDAO(maintenanceObject , true , DESCRIPTION , maintenanceObject.getDescription());
			daos.add(keyVal);
			
			for (int i = 0; additionalProperties != null && 
								i< additionalProperties.size(); i++) {
				PropertyData property = maintenanceObject.getProperty((String)additionalProperties.get(i));
		
				if (property != null && property.isSimpleType()) {
					keyVal = newKeyValDAO(maintenanceObject , true , property.getName() , property.getValue());
					daos.add(keyVal);
				}
			}
			
			Iterator groupIterator = maintenanceObject.iterator();
			while (groupIterator.hasNext()) {
				Iterator propertyIterator = ((PropertyGroupData) groupIterator.next()).iterator();
				PropertyData property = null;
				while (propertyIterator.hasNext()) {
					property = (PropertyData)propertyIterator.next();
					keyVal = newKeyValDAO(maintenanceObject , false , property.getName() , property.getValue());
					daos.add(keyVal);
				}
			}
			saveData(null , daos);
		}  catch(Exception exc)  {
			log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "Unable to save the shop data" , exc);
			throw exception("Unable to save the shop data" , "shopdata.savefailed");
		} finally  {
			log.exiting();
		}
	}
	
	
	/**
	 * @param maintenanceObject
	 */
	private void checkDuplicateData(MaintenanceObjectData maintenanceObject) 
			throws MaintenanceObjectHelperException  {
		try  {
			if(maintenanceObject.getId() != null)  {
				maintenanceObject.setId(maintenanceObject.getId().trim().toUpperCase());
			}
			Properties systemProps = new Properties();
			systemProps.put(KeyValDAO.CLIENT , client);
			systemProps.put(KeyValDAO.SYSTEMID , systemId);
			systemProps.put(KeyValDAO.GRP_NAME, maintenanceObject.getId().trim());
			systemProps.put(KeyValDAO.DATA_TYPE, HEADER);
			TreeMap map = readObjects(null , systemProps);
			if(map.get(maintenanceObject.getId().trim()) != null)  {
				String args[]= {maintenanceObject.getId().trim()};
				maintenanceObject.addMessage(new Message(Message.ERROR,"mob.error.objectexist",args,null));
			}
		}  catch(Exception exc)  {
			log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "Unable to check duplicate shop id" , exc);
			throw exception("Unable to check duplicate shop id" , "");
		} 
	}

	/**
	 * Update the data of the given object on the database. <br>
	 * 
	 * @param maintenanceObject
	 * @throws MaintenanceObjectHelperException
	 */
	protected void modifyObject(MaintenanceObjectData maintenanceObject)
			throws MaintenanceObjectHelperException  {
		
		final String METHOD = "modifyObject()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("to be modified object="+maintenanceObject);
		if(!maintenanceObject.isValid())  {
			log.exiting();
			return;
		}
		ArrayList oldDaos = new ArrayList();
		ArrayList newDaos = new ArrayList();
		KeyValDAO keyVal = null;
		try  {

			
			if(keyValues == null)  {
				return;
			}
			
			//Add the Identifier and Description rows
			keyVal = (KeyValDAO)keyValues.get(HEADER + "-" + DESCRIPTION);
			if(isChanged(keyVal.getPropValue() , maintenanceObject.getDescription()))  {
				keyVal.setPropValue(maintenanceObject.getDescription());
				oldDaos.add(keyVal);		
			}
			
			String value = null;	
			for (int i = 0; additionalProperties != null && 
								i< additionalProperties.size(); i++) {
				PropertyData property = maintenanceObject.getProperty((String)additionalProperties.get(i));

				if (property != null && property.isSimpleType()) {
					keyVal = (KeyValDAO)keyValues.get(HEADER + "-" + property.getName());
					value = property.getValue() != null ? property.getValue().toString() : ""; 
					if(keyVal == null)  {
						keyVal = newKeyValDAO(maintenanceObject , true , property.getName() , value);
						newDaos.add(keyVal);
						continue;
					}
						
					if(isChanged(keyVal.getPropValue() , value))  {
						keyVal.setPropValue(value);
						oldDaos.add(keyVal);
					}
				}
			}
				
			Iterator groupIterator = maintenanceObject.iterator();
			
			while (groupIterator.hasNext()) {
				Iterator properyIterator = ((PropertyGroupData) groupIterator.next()).iterator();
				PropertyData property = null;
				while (properyIterator.hasNext()) {
					property = (PropertyData)properyIterator.next();
					keyVal = (KeyValDAO)keyValues.get(CHILDREN + "-" + property.getName());
					value = property.getValue() != null ? property.getValue().toString() : "";
					if(keyVal == null)  {
						keyVal = newKeyValDAO(maintenanceObject , false , property.getName() , value);
						newDaos.add(keyVal);
						continue;
					}
					if(isChanged(keyVal.getPropValue() , value))  {
						keyVal.setPropValue(value);
						oldDaos.add(keyVal);
					}
				}
			}
			saveData(oldDaos , newDaos);
		}    catch(Exception exc)  {
			log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "Unable to save the shop data" , exc);
			throw exception("Unable to save the shop data" , "shopdata.savefailed");
		} finally {
			log.exiting();
		}
	}
	
	private static boolean isChanged(String oldValue , String newValue)  {
		return (oldValue == null && newValue != null)  ||
			   (oldValue != null && newValue == null) ||
			   !oldValue.equals(newValue);		 
	}
	
	/* (non-Javadoc)
	 * @see com.sap.isa.maintenanceobject.backend.MaintenanceObjectDataHandler#saveData(com.sap.isa.maintenanceobject.backend.boi.MaintenanceObjectData)
	 */
	public void saveData(MaintenanceObjectData maintenanceObject)
		throws MaintenanceObjectHelperException {
			final String METHOD = "saveData()";
			log.entering(METHOD);
			if(maintenanceObject.isNewObject())  {
				createObject(maintenanceObject);
			}  else  {
				modifyObject(maintenanceObject);
			}
			if(maintenanceObject.isValid())
				readData(maintenanceObject);
			log.exiting();
	}
	
	private KeyValDAO createKeyVal(MaintenanceObjectData maintenanceObjectData , boolean header)  {
		KeyValDAO keyVal = new KeyValDAO();
		keyVal.setClient(client);
		keyVal.setSystemID(systemId);
		keyVal.setDataSource(header ? HEADER : CHILDREN);
		keyVal.setGroupName(maintenanceObjectData.getId());
		return keyVal;
	}
	
	private KeyValDAO newKeyValDAO(MaintenanceObjectData maintenanceObjectData , boolean header ,
									String propName , Object propValue)  {
		KeyValDAO keyVal = createKeyVal(maintenanceObjectData , header);
		keyVal.setPropName(propName);
		if(propValue != null)
			keyVal.setPropValue(propValue.toString());
		else
			keyVal.setPropValue(null);
		keyVal.initializeKey();
		return keyVal;										
	}


	
	/**
	 * @param pm
	 */
	private void closeTransaction(JdbcPersistenceManager pm) {
		if(pm != null)  {
			pm.close();
		}
	}

	private JdbcPersistenceManager startTransaction()  {
		JdbcPersistenceManager pm = (JdbcPersistenceManager)
								pmfactory.getDBPersistenceManager();
		pm.getTransaction().begin();
		return pm;		
	}
	
	private void commitTransaction(JdbcPersistenceManager pm)  {
		pm.getTransaction().commit();
	}
	
	private void rollbackTransaction(JdbcPersistenceManager pm)  {
		pm.getTransaction().rollback();
		pm.close();
	}
	
	protected void clearKeyValues()  {
		if(keyValues != null)  {
			keyValues.clear();
		}
	}

	protected void detach(Collection daos)  {
		if(daos != null)  {
			Iterator iterator = daos.iterator();
			JdbcDAO dao = null;
			while(iterator.hasNext())  {
				dao = (JdbcDAO)iterator.next();
				dao.getPersistenceManager().detach(dao);
			}
		}
	}

	protected void attach(Collection daos , JdbcPersistenceManager pm)  {
		if(daos != null)  {
			Iterator iterator = daos.iterator();
			JdbcDAO dao = null;
			while(iterator.hasNext())  {
				dao = (JdbcDAO)iterator.next();
				pm.attach(dao);
			}
		}
	}
}
