/*****************************************************************************
 Class:        IPCPropertyData
 Copyright (c) 2008, SAP AG, Germany, All rights reserved.
 Author:       SAP AG
 Created:      11.04.2008
 Version:      1.0
 *****************************************************************************/

package com.sap.isa.maintenanceobject.backend.boi;

//TO avoid compile errors on JSP we need the interface in the appbase packages!!! 

//TODO docu
/**
 * <p>The class IPCPropertyData . </p>
 *
 * @author  SAP AG
 * @version 1.0
 */
public interface IPCPropertyData extends IPCImageLinkData {

	
	/**
	 * <p>Return the property {@link #additionalDocumentURL}. </p> 
	 *
	 * @return Returns the {@link #additionalDocumentURL}.
	 */
	public String getAdditionalDocumentURL();

	/**
	 * <p>Set the property {@link #additionalDocumentURL}. </p>
	 * 
	 * @param additionalDocumentURL The {@link #additionalDocumentURL} to set.
	 */
	public void setAdditionalDocumentURL(String additionalDocumentURL);

	/**
	 * <p>Return the property {@link #displayDocumentLink}. </p> 
	 *
	 * @return Returns the {@link #displayDocumentLink}.
	 */
	public boolean isDisplayDocumentLink();

	/**
	 * <p>Set the property {@link #displayDocumentLink}. </p>
	 * 
	 * @param displayDocumentLink The {@link #displayDocumentLink} to set.
	 */
	public void setDisplayDocumentLink(boolean displayDocumentLink);

	/**
	 * <p>Return the property {@link #displayHelpLink}. </p> 
	 *
	 * @return Returns the {@link #displayHelpLink}.
	 */
	public boolean isDisplayHelpLink();

	/**
	 * <p>Set the property {@link #displayHelpLink}. </p>
	 * 
	 * @param displayHelpLink The {@link #displayHelpLink} to set.
	 */
	public void setDisplayHelpLink(boolean displayHelpLink);

	/**
	 * <p>Return the property {@link #helpLinkLabel}. </p> 
	 *
	 * @return Returns the {@link #helpLinkLabel}.
	 */
	public String getHelpLinkLabel();

	/**
	 * <p>Set the property {@link #helpLinkLabel}. </p>
	 * 
	 * @param helpLinkLabel The {@link #helpLinkLabel} to set.
	 */
	public void setHelpLinkLabel(String helpLinkLabel);

	/**
	 * <p>Return the property {@link #helpLinkURL}. </p> 
	 *
	 * @return Returns the {@link #helpLinkURL}.
	 */
	public String getHelpLinkURL();

	/**
	 * <p>Set the property {@link #helpLinkURL}. </p>
	 * 
	 * @param helpLinkURL The {@link #helpLinkURL} to set.
	 */
	public void setHelpLinkURL(String helpLinkURL);

	/**
	 * <p>Return the property {@link #resetURL}. </p> 
	 *
	 * @return Returns the {@link #resetURL}.
	 */
	public String getResetURL();

	/**
	 * <p>Set the property {@link #resetURL}. </p>
	 * 
	 * @param resetURL The {@link #resetURL} to set.
	 */
	public void setResetURL(String resetURL);

	/**
	 * 
	 * Return the property {@link #longText}. <br>
	 * 
	 * @return longText
	 */
	public String getLongText();
	
	/**
	 *  Set the property {@link #longText}. <br>
	 * 
	 * @param longText The longText to set.
	 */
	public void setLongText(String longText);
	
	/**
	 * 
	 * Return the property {@link #allOptionsLink}. <br>
	 * 
	 * @return allOptionsLink
	 */
	public String getAllOptionsLink();
	
	/**
	 *  Set the property {@link #allOptionsLink}. <br>
	 * 
	 * @param allOptionsLink The allOptionsLink to set.
	 */
	public void setAllOptionsLink(String allOptionsLink);
			
}