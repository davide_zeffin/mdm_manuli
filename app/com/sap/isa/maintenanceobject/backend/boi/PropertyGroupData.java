/*****************************************************************************
    Class:        PropertyGroupData
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      Januray 2002
    Version:      1.0

    $Revision: #7 $
    $Date: 2001/07/25 $
*****************************************************************************/

package com.sap.isa.maintenanceobject.backend.boi;

import com.sap.isa.core.Iterable;


/**
 * @author SAP
 * @version 1.0
 */
public interface PropertyGroupData extends UIElementGroupData, Iterable {

    /**
      * add a given property to the proeprty group
      *
      */
    public void addProperty(PropertyData property);


    /**
      * creates a new property
      *
      */
    public PropertyData createProperty(String propertyName);


    /**
     * Returns the name of the proeprty group
     *
     * @return name
     *
     */
    public PropertyData getProperty(String name);

}
