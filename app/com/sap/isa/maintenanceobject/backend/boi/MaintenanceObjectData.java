/*****************************************************************************
    Inteface:     MaintenanceObjectData
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      February 2002
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.maintenanceobject.backend.boi;

//import com.sap.isa.maintenanceobject.util.HelpValues;

import java.util.Iterator;

import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.Iterable;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.helpvalues.HelpValues;

/**
 * The MaintenanceObjectData interface handles all kind of settings for the application.<p>
 *
 * @author Wolfgang Sattler
 * @version 1.0
 */
public interface MaintenanceObjectData extends Iterable, BusinessObjectBaseData, DynamicUIData {

    /**
      * Constant to indentify the ID field in the Result Set
      */
    public final static String ID = "ID";

    /**
     * Constant to indentify the Description field in the Result Set
     */
    public final static String DESCRIPTION = "DESCRIPTION";
    
  

    /**
     * Set the property id
     *
     * @param id
     *
     */
    public void setId(String id);


    /**
     * Returns the property id
     *
     * @return id
     *
     */
    public String getId();


    /**
     * Set the property description
     *
     * @param description
     *
     */
    public void setDescription(String description);


    /**
     * Returns the property description
     *
     * @return description
     *
     */
    public String getDescription();


    /**
     * Returns the max length for the id.
     * @return max length (<code>int</code>)
     */
    public int getMaxLengthId();


    /**
     * Sets the max length for the id.
     * @param maxLengthId The max length for the id
     */
    public void setMaxLengthId(int maxLengthId);


    /**
     * Returns the max length for the description.
     * @return max length (<code>int</code>)
     */
    public int getMaxLengthDescription();

    /**
     * Sets the max length for the description.
     * @param maxLengthDescription The max length for the description
     */
    public void setMaxLengthDescription(int maxLengthDescription);


    /**
     * Set the property newObject
     *
     * @param newObject
     *
     */
    public void setNewObject(boolean newObject);


    /**
     * Returns the property newObject
     *
     * @return newObject
     *
     */
    public boolean isNewObject();

	
	/**
	 * Returns the iterator over all screen groups. <br>
	 *
	 * @return screen groups
	 *
	 */
	public Iterator screenGroupIterator();


    /**
     * Returns the screen group with the given name
     *
     * @return screen group
     *
     */
    public ScreenGroupData getScreenGroup(String screenGroupName);


    /**
     * Returns the property group with the given name
     *
     * @return propertyGroupName group
     *
     */
    public PropertyGroupData getPropertyGroup(String propertyGroupName);


    /**
     * Returns the property with the given name in the given property group
     *
     * @return propertyName
     *
     */
    public PropertyData getProperty(String propertyGroupName, String propertyName);


    /**
     * Returns the property with the given name in the given property group
     *
     * @return propertyName
     *
     */
    public PropertyData getProperty(String propertyName);


    /**
      * add a given property group to the maintened object
      *
      * @param propertyGroup  group
      */
    public void addPropertyGroup(PropertyGroupData propertyGroup);


    /**
      * add a given screen group to the maintened object
      *
      * @param screenGroup group to add
      *
      */
    public void addScreenGroup(ScreenGroupData screenGroup) ;

    /**
      * creates a new property group
      *
      * @param propertyGroupName name of the group
      *
      */
    public PropertyGroupData createPropertyGroup(String propertyGroupName);


    /**
     * creates a new screen group with the given name
     *
     * @param screenGroupName name of the screenGroup
     *
     */
    public ScreenGroupData createScreenGroup(String screenGroupName);


    /**
     * Set isHidden flag of all property groups, depending the
     * corresponding properties.
     *
     */
    public void determineVisibility();


    /**
     * Synchronize properties with backend properties.
     *
     * Looking for corresponding properties in the backend bean and
     * get the values from this properties.
     *
     */
    public void synchronizeProperties() throws BackendException;


    /**
      * Synchronize backend properties with properties.
      *
      * Looking for corresponding properties in the backend bean and
      * set the values of this properties.
     *
     * @param bean the backend object
     *
     */
    public void synchronizeBeanProperties(MaintenanceObjectBackend bean);


    /**
     * Set a lock the object
     */
    public void lock() throws CommunicationException;


    /**
     * Delete a lock from the object
     */
    public void unlock() throws CommunicationException;


    /**
     * Returns the HelpValues for the given Property
     *
     * @param property
     * @return help values
     *
     */
    public HelpValues getHelpValues(PropertyData property)
    		throws CommunicationException;
    
    
	/**
	 * Clones the Maintenance object. <br>
	 * All including objects will be also cloned.
	 * 
	 * @return copy of the object and all sub objects
	 * 
	 * @see java.lang.Object#clone()
	 */
	public Object clone();


	/**
	 * Clones the Maintenance object. <br>
	 * All including objects will be also cloned.
	 *
	 * @param objectData source Object 
	 * 
	 */
	public void copy(MaintenanceObjectData objectData);

}
