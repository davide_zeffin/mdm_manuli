/*****************************************************************************
    Class:        DynamicUIProviderData
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      08.02.2008
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.maintenanceobject.backend.boi;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.sap.isa.core.TechKey;

/**
 * The interface DynamicUIProviderData describes objects which provide the structure 
 * and functionality which used by the {@link #com.sap.isa.maintenanceobject.businessobject.DynamicUI} object. <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public interface DynamicUIProviderData {
	
    /**
     * Return a UI element group which contains all pages of the dynamic UI object. <br>
     * 
     * @return UI element group with all pages.
     */
	public UIElementGroupData getPageGroup();


    /**
     * Return a iterator over all properties in the dynamic UI object. <br>
     * 
     * @return iterator for the properties.
     */
    public Iterator getPropertyIterator();
	
	
	/**
	 * <p>Return the list of element with the given name. </p>
	 * 
	 * @param elementName name of the element.
	 * @return the list of UI element groups ({@link UIElementGroupData})
	 *         with the given element group name. 
	 */
	public List getElementGroupList(String elementName);
	
	/**
	 * <p>Return the element with the given techKey. </p>
	 * 
	 * @param key key of the UI element group.
	 * @return the UI element group with the given element name. 
	 */
	public UIElementGroupData getElementGroup(TechKey key);

	/**
	 * Return the list of property with the given name. <br>
	 * 
	 * @param elementName name of the element
	 * @return the list of UI elements {@link PropertyData} with the given element name. 
	 */
	public List getPropertyList(String elementName);

	/**
	 * Return the property with the given key. <br>
	 * 
	 * @param key key of the property.
	 * @return the UI element with the given element name. 
	 */
	public PropertyData getProperty(TechKey key);

	
	/**
     * <p>Returns the parent group for the given key.</p>   
     * 
     * @param key key of the UI Element.
     * @return found element or <code>null</code>.
     */
    public UIElementGroupData getParent(TechKey key);
	
	/**
	 * <p> The method getUIRendererManagerName provides the alias under which
	 *     the {@link com.sap.isa.maintenanceobject.ui.renderer.UIRendererManager} can be found. </p>
	 * 
	 * @return name to find {@link com.sap.isa.maintenanceobject.ui.renderer.UIRendererManager} within
	 *         factory-config.xml. 
	 * @see com.sap.isa.maintenanceobject.ui.renderer.UIRendererManager
	 * @see com.sap.isa.core.util.GenericFactory        
	 */
	public String getUIRendererManagerName();

	
	/**
	 * <p> The method getMaintenanceExitData provides an <code>DynamicUIMaintenanceExitData</code> object, which provides exits 
	 * which can be used for Dynamic UI maintenance implementation </p>
	 * 
	 * @return instance of {@link DynamicUIMaintenanceExitData}
	 */
	public DynamicUIMaintenanceExitData getMaintenanceExitData();

	
	/**
	 * <p> The method getDynamicUIExitsHandler provides an <code>DynamicUIExitData</code> object, which provides exits 
	 * which can be used for Dynamic UI implementation </p>
	 * 
	 * @return instance of {@link DynamicUIExitData}
	 */
	public DynamicUIExitData getDynamicUIExitData();

	/**
	 * <p> The method getInvisibleParts provides the map with invisible parts which will 
	 * be used in the {@link com.sap.isa.maintenanceobject.ui.renderer.UIRendererManager}</p>
	 * <p> Please see {@link com.sap.isa.maintenanceobject.ui.renderer.UIRendererManager#invisibleParts}
	 * for details.
	 * 
	 * @return map with invisible parts.
	 */
	public Map getInvisibleParts();
	
	
    /**
     * <p> The method addElement allows to add a new UI element under full control of 
     * dynamic UI object. </p>
     * 
     * @param newElement new Element to 
     * @param parentGroup parentGroup for this element.
     */
    public void addElement(UIElementBaseData newElement, UIElementGroupData parentGroup);

    	
    /**
     * <p>Delete all pages of the dynamic UI. </p> 
     */
    public void deleteAllPages();

    
    /**
     * <p>Delete pages of dynamic UI. </p> 
     */
    public void deletePage(UIElementGroupData page);
    
    
    /**
     * <p>Create a new Page. </p> 
     * @return created page!
     */
    public UIElementGroupData createPage();
    
    /**
     * <p> The method deleteElement deletes the give <code>element</code> from the given ui group </p>
     * 
     * @param element UI element which should be deleted.
     * @param group group from which the element is deleted.
     */
    public void deleteElement(UIElementBaseData element, UIElementGroupData group);

    
    /**
     * <p> The method moveElement moves the element give with the <code>elementKey</code> to the 
     * given target page. </p>
     * 
     * @param elementKey key of the element which should be moved.
     * @param page target page to which the element will be moved.
     */
    public void moveElement(TechKey elementKey, UIElementGroupData page);

    
    /**
     * <p> The method copyElement copies the element give with the <code>elementKey</code> to the 
     * given target page. </p>
     * 
     * @param elementKey key of the element which should be copied.
     * @param page target page to which the element will be copied.
     */
    public void copyElement(TechKey elementKey, UIElementGroupData page);

}
