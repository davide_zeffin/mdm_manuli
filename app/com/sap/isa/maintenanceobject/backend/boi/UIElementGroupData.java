/*****************************************************************************
 Class:        UIElementGroupData
 Copyright (c) 2008, SAP AG, Germany, All rights reserved.
 Author:       SAP AG
 Created:      08.02.2008
 Version:      1.0

 $Revision: #13 $
 $Date: 2003/05/06 $ 
 *****************************************************************************/

package com.sap.isa.maintenanceobject.backend.boi;

import java.util.Iterator;
import java.util.List;

import com.sap.isa.core.Iterable;
import com.sap.isa.core.TechKey;

public interface UIElementGroupData extends UIElementBaseData, Iterable, Cloneable {

	/**
	 * Add a ui element to the group. <br>
	 *
	 * @param element element to be added.
	 *
	 */
	public abstract void addElement(UIElementGroupData element);

	/**
	 * Add a given element to the group. <br>
	 *
	 * @param element element to be added.
	 *
	 */
	public abstract void addElement(UIElementData element);

	
	/**
	 * Delete a given element from the group. <br>
	 *
	 * @param element element to be deleted.
	 *
	 */
	public abstract void deleteElement(UIElementData element);

	
	/**
	 * Delete a given element from the group. <br>
	 *
	 * @param element element to be deleted.
	 *
	 */
	public abstract void deleteElement(UIElementGroupData element);

	
	/**
	 * Returns the element with the given name. <br>
	 *
	 * @return ui element for the given name or <code>null</code> if no element
	 *          could be found.
	 */
	public abstract UIElementBaseData getElement(String name);

	
	/**
     * <p>Returns the list of UI elements within the whole group including sub groups.</p>   
     * 
     * @param name name of the UI Element.
     * @return list with all found elements.
     */
    public List getElementList(String name);


	/**
     * <p>Returns the element for the given key.</p>   
     * 
     * @param key key of the UI Element.
     * @return found element or <code>null</code>.
     */
    public UIElementBaseData getElement(TechKey key);

   
	/**
     * <p>Returns the parent group for the given key.</p>   
     * 
     * @param key key of the UI Element.
     * @return found element or <code>null</code>.
     */
    public UIElementGroupData getParent(TechKey key);
    
    
    /**
     * <p>Returns the list of UI elements within the whole group including sub groups.</p>   
     * 
     * @param name name of the UI Element.
     * @param isStructured possible filter for structure or unstructure elements. Pass 
     *        <code>null</code> to get all elements. 
     * @return list with all found elements.
     */
    public List getElementList(String name, Boolean isStructured);

    
    /**
     * 
     * Return the list with the elements. <br>
     * 
     * @return list with elements.
     */
    public List getElementList();
    
    
    /**
     * <p>Clear the element group </p> 
     */
    public void clear();

	
	/**
	 * Set the <code>isHidden</code> flag depending of the visibility of the 
	 * corresponding elements. <br>
	 */
	public abstract void determineVisibility();

	/**
	 * Returns the number of visible elements in the group.
	 * The method {@link #determineVisibility()} will be call before execution.
	 * 
	 * @return number of visible elements
	 */
	public abstract int getNumberOfVisibleElements();

	
	/**
	 * <p>Returns an iterator for all property objects in the entire structure.  <br>
	 * 
	 * @return iterator: over all property objects within the group and all sub groups.
	 * 
	 * @see com.sap.isa.maintenanceobject.backend.boi.UIElementGroupData#iterator()
	 */
    public abstract Iterator propertyIterator();
	
	
	/**
	 * Returns an iterator for the element list. <br>
	 *
	 * @return iterator to iterate over the elements in the list.
	 */
	public abstract Iterator iterator();

	/**
	 * Clones the element. <br>
	 * All including objects should be also cloned.
	 * 
	 * @return copy of the object and all its sub objects
	 * 
	 * @see java.lang.Object#clone()
	 */
	public Object clone() throws CloneNotSupportedException; 

	
	/**
	 * Clones a group without elements . <br>
	 * All including objects won't be cloned.
	 * 
	 * @return copy of the object 
	 * 
	 */
	public Object cloneWithoutElements() throws CloneNotSupportedException;
	
	
	/**
	 * Returns true if the given element is expanded. <br>
	 * 
	 * @return boolean
	 */
	public boolean isExpanded();
	
	
	/**
	 * Set the <code>isExpanded</code> flag at the given element. <br>
	 * 
	 * @param isExpanded
	 */
	public void setExpanded(boolean isExpanded);
	

	/**
	 * <p> Returns true if only one group element should be expanded. </p>
	 * 
	 * @return isOnlyOneABExpanded
	 */
    public boolean isOnlyOneGroupExpanded();

    
    /**
     * <p> Set the property if only one group element should be expanded. </p>
     * 
     * @param isOnlyOneGroupExpanded
     */
    public void setOnlyOneGroupExpanded(boolean isOnlyOneGroupExpanded);

    
    /**
     * Expand or Collapse the group for the given group name. <br>
     * 
     * @param groupName
     */
    public void expandGroup(String groupName);    
    
    
    /**
     * Collapse the group for the given group name. <br>
     * 
     * @param groupName
     */
    public void collapseGroup(String groupName);
    
    
	/**
	 * 
	 * Return the property {@link #isToggleAllowed}. <br>
	 * 
	 * @return isToggleAllowed
	 */
	public boolean isToggleAllowed();
	
	
	/**
	 *  Set the property {@link #isToggleAllowed}. <br>
	 * 
	 * @param isToggleAllowed The isToggleAllowed to set.
	 */
	public void setToggleAllowed(boolean isToggleAllowed);	
	
    /**
     * Move group for the given group name. <br>
     * 
     * @param groupName
     * @param direction
     */
    public boolean moveElementUp(String elementName);

    
    /**
     * Move group for the given group name. <br>
     * 
     * @param groupName
     * @param direction
     */
    public boolean moveElementDown(String elementName);
    
    
    /**
     * <p> The method getElementIndex returns the index of the element in the List.</p>
     * <p><strong>Note</strong> That the index starts with <code>1</code></p>
     * 
     * @param element UI element
     * @return index in the list.
     */
    public int getElementIndex(UIElementBaseData element);
}
