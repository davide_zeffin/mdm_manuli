/*****************************************************************************
 Class:        DynamicUIMaintenanceData
 Copyright (c) 2008, SAP AG, Germany, All rights reserved.
 Author:       SAP AG
 Created:      25.04.2008
 Version:      1.0
 *****************************************************************************/

package com.sap.isa.maintenanceobject.backend.boi;

import com.sap.isa.maintenanceobject.businessobject.PageMaintenance;
import com.sap.isa.maintenanceobject.businessobject.PropertyMaintenance;

/**
 * The class DynamicUIMaintenanceData allows access to the 
 * {@link com.sap.isa.maintenanceobject.businessobject.PropertyMaintenance} and 
 * {@link com.sap.isa.maintenanceobject.businessobject.PageMaintenance} objects. <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public interface DynamicUIMaintenanceData {

    /**
     * <p> The method getPropertyMaintenance returns a maintenance object for the UI Property. </p>
     * 
     * @return instance of {@link PageMaintenance}
     */
	public PropertyMaintenance getPropertyMaintenance();

	/**
	 * <p> The method getPageMaintenance returns a maintenance object for the UI page. </p>
	 * 
	 * @return instance of {@link PageMaintenance}
	 */
	public PageMaintenance getPageMaintenance();

}