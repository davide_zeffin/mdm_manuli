/*****************************************************************************
    Class:        UIFieldData
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      24.03.2004
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.maintenanceobject.backend.boi;



// TODO docu
/**
 * The interface UIField . <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public interface UIFieldData extends PropertyData {



}
