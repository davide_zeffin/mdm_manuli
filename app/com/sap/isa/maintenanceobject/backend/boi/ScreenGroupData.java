/*****************************************************************************
    Class:        ScreenGroupData
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      Januray 2002
    Version:      1.0

    $Revision: #7 $
    $Date: 2001/07/25 $
*****************************************************************************/

package com.sap.isa.maintenanceobject.backend.boi;

import java.util.Iterator;


/**
 * @author SAP
 * @version 1.0
 */
public interface ScreenGroupData extends UIElementGroupData {

    /**
      * add a given property to the proeprty group
      *
      */
    public void addPropertyGroup(PropertyGroupData propertyGroup);


    /**
      * creates a new property
      *
      */
    public PropertyGroupData createPropertyGroup(String propertyGroupName);


	/**
	 * Get the names of a property group which should be added. <br>
	 * <strong>This function should only be used by the parsing process.</strong>
	 *
	 * @return list of property groupnames
	 *
	 */
	public Iterator getTempPropertyGroupNames();


	/**
	 * Delete the names of a property group, which should be added. <br>
	 * <strong>This function should only be used by the parsing process.</strong>
	 *
	 */
	public void deleteTempPropertyGroupNames();



}
