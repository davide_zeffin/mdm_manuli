/*****************************************************************************
    Interface:    DynamicUIMaintenanceBackend
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Author:       SAP
    Created:      19.02.2008
    Version:      1.0

*****************************************************************************/
package com.sap.isa.maintenanceobject.backend.boi;

import com.sap.isa.core.eai.BackendException;
import com.sap.isa.maintenanceobject.businessobject.DynamicUI;

/**
 * The class DynamicUIMaintenanceCRM allows to store dynamic UI object in a backend system. <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public interface DynamicUIMaintenanceBackend {
	 
    /**
     * This method save the dynamic object in backend. <br>
     *
     * @param dynamicUI dynamic ui object
     * @param propertyMaintenance maintenance object which describe the properties of the Property object.
     * 
     */
    public void saveData(DynamicUI dynamicUI, MaintenanceObjectData propertyMaintenance)
            throws BackendException;
    
    
    /**
     * This method initialize the property maintenance object in the backend. <br>
     *
     * @param maintenanceObject property maintenance object
     */
    public void initMaintenanceObjects(DynamicUIMaintenanceData maintenaceData)
                    throws BackendException;
    

    
}
