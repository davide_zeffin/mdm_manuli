/*****************************************************************************
    Class:        UIElementStatus
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      02.04.2008
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.maintenanceobject.backend.boi;

/**
 * The interface <code>UIElementStatus</code> represent the status of an arbritrary
 * UI element. It includes a description and a CSS style to render icon etc. <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public interface UIElementStatus {
	
	/**
	 * Return a description of the status. <br> 
	 *
	 * @return Returns the status description.
	 */
	public String getDescription();

	/**
	 * Set the description of the status. <br>
	 * 
	 * @param description The description of the status to set.
	 */
	public void setDescription(String description);

	/**
	 * Return a style class to render the status. <br> 
	 *
	 * @return Returns CSS style class.
	 */
	public String getStyle();

	/**
	 * Set the style class for the status. <br>
	 * 
	 * @param style The style class to set.
	 */
	public void setStyle(String style);
	
    /**
     * Returns true if labels for the property are retrieved from the resource key file.<br> 
     * 
     * @return boolean
     */
    public boolean isRessourceKeyUsed();
    
    
    /**
     * Set the parameter for isRessourceKeyUsed <br>
     * 
     * @param ressourceKeyUsed flag if the resource key is used.
     */
    public void setRessourceKeyUsed(boolean ressourceKeyUsed);    

    
    /**
     * Returns the image URL to display the status image. <br>
     *  
     * @return Returns the URL for the status image.
     */
    public String getImageURL();
    
    
    /**
     * Set the URL for the status image. <br>
     * 
     * @param imageURL The image URL to set
     */
    public void setImageURL(String imageURL);
    
}
