/*****************************************************************************
 Class:        UIPageData
 Copyright (c) 2008, SAP AG, Germany, All rights reserved.
 Author:       SAP AG
 Created:      25.04.2008
 Version:      1.0
 *****************************************************************************/

package com.sap.isa.maintenanceobject.backend.boi;


public interface UIPageData {

	/**
	 * <p>Return the property {@link #descriptionList}. </p> 
	 *
	 * @return Returns the {@link #descriptionList}.
	 */
	public DescriptionListData getDescriptionList();

	/**
	 * <p>Set the property {@link #descriptionList}. </p>
	 * 
	 * @param descriptionList The {@link #descriptionList} to set.
	 */
	public void setDescriptionList(DescriptionListData descriptionList);

	/**
	 * <p>Return the property {@link #imageURL}. </p> 
	 *
	 * @return Returns the {@link #imageURL}.
	 */
	public String getImageURL();

	/**
	 * <p>Set the property {@link #imageURL}. </p>
	 * 
	 * @param imageURL The {@link #imageURL} to set.
	 */
	public void setImageURL(String imageURL);

	/**
	 * 
	 * Return the property {@link #imageHeight}. <br>
	 * 
	 * @return imageHeight
	 */
	public String getImageHeight();

	/**
	 *  Set the property {@link #imagelHeight}. <br>
	 * 
	 * @param imageHeight The imageHeight to set.
	 */
	public void setImageHeight(String imageHeight);

	/**
	 * 
	 * Return the property {@link #imageWidth}. <br>
	 * 
	 * @return imageWidth
	 */
	public String getImageWidth();

	/**
	 *  Set the property {@link #imageWidth}. <br>
	 * 
	 * @param imageWidth The imageWidth to set.
	 */
	public void setImageWidth(String imageWidth);

	/**
	 * <p> The method getDescription returns a language dependent description.</p>
	 * <p> If no lanaguage dependent description is maintained the default language is used. </p>
	 * 
	 * @param language language
	 * @return language dependent description if available. Otherwise the standard description.
	 */
	public String getDescription(String language);

}