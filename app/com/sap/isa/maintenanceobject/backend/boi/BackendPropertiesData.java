/*****************************************************************************
    Class:        BackendPropertiesData
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      10.11.2004
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.maintenanceobject.backend.boi;

//TODO docu
/**
 * The class BackendPropertiesData . <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public interface BackendPropertiesData extends BackendElementKeyHolder{


	/**
	 * Return the property {@link #descriptionFieldName}. <br> 
	 *
	 * @return Returns the {@link #descriptionFieldName}.
	 */
	public String getDescriptionFieldName() ;

    
	/**
	 * Set the property {@link #descriptionFieldName}. <br>
	 * 
	 * @param descriptionFieldName The {@link #descriptionFieldName} to set.
	 */
	public void setDescriptionFieldName(String descriptionFieldName);
    
    
	/**
	 * Return the property {@link #fieldName}. <br> 
	 *
	 * @return Returns the {@link #fieldName}.
	 */
	public String getFieldName();
    
    
	/**
	 * Set the property {@link #fieldName}. <br>
	 * 
	 * @param fieldName The {@link #fieldName} to set.
	 */
	public void setFieldName(String fieldName);
    
    
	/**
	 * Return the property {@link #structureName}. <br> 
	 *
	 * @return Returns the {@link #structureName}.
	 */
	public String getStructureName();
    
    
	/**
	 * Set the property {@link #structureName}. <br>
	 * 
	 * @param structureName The {@link #structureName} to set.
	 */
	public void setStructureName(String structureName);
	

}
