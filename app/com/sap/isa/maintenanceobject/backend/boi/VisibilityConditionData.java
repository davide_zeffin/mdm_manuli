/*****************************************************************************
    Class:        VisibilityConditionData
    Copyright (c) 2003, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      10.12.2003
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.maintenanceobject.backend.boi;

/**
 * The interface VisibilityConditionData . <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public interface VisibilityConditionData {
	
    /**
     * Return the property {@link #propertyGroupName}. <br>
     * 
     * @return {@link #propertyGroupName}
     */
    public String getPropertyGroupName();
    
    /**
     * Set the property {@link #propertyGroupName}. <br>
     * 
     * @param propertyGroupName see {@link #propertyGroupName}
     */
    
    public void setPropertyGroupName(String propertyGroupName);
    
    /**
     * Return the property {@link #propertyName}. <br>
     * 
     * @return {@link #propertyName}
     */
    
    public String getPropertyName();
    
    /**
     * Return Set the property {@link #propertyName}. <br>
     * 
     * @param propertyName see {@link #propertyName}
     */
    public void setPropertyName(String propertyName);
    
    /**
     * Return the property {@link #value}. <br>
     * 
     * @return {@link #value}
     */
    public String getValue();
    
    /**
     * Set the property {@link #value}. <br>
     * 
     * @param value see {@link #value}
     */
    public void setValue(String value);

    
}