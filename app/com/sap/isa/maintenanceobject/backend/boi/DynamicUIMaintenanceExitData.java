/*****************************************************************************
    Interface:    DynamicUIMaintenanceExitData
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Author:       SAP
    Created:      19.02.2008
    Version:      1.0

*****************************************************************************/

package com.sap.isa.maintenanceobject.backend.boi;

import java.util.List;
import java.util.Map;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.maintenanceobject.businessobject.DynamicUI;
import com.sap.isa.maintenanceobject.businessobject.PropertyMaintenance;


/**
 * The interface DynamicUIMaintenanceExitData describes exits to allow object specific
 * maintenance of a dynamic UI object . <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public interface DynamicUIMaintenanceExitData {
	
	/**
	 * Exit to maintain a property. <br>
	 * 
	 * @param property property object
	 * @param propertyMaintenance property maintenance object
	 */
	public void maintainProperty(PropertyData property, PropertyMaintenance propertyMaintenance);
	
	/**
	 * Exit to parse the allowed values. <br>
	 * 
	 * @param allowedValues allowed values
	 * @param requestParser request parser to access the request 
	 */
	public void parseAllowedValues(List allowedValues, RequestParser requestParser);
	
	/**
	 * Not used at the moment. <br>
	 * 
     * @param property property object
     * @param propertyMaintenance property maintenance object
     * @deprecated
	 */
	public void adjustAllowedProperties(PropertyData property, PropertyMaintenance propertyMaintenance);	


    /**
	 * Exit to do any additional parsing of a request within the designer round trip. <br>
	 * 
	 * @param requestParser request parser to access the request
	 * @param dynamicUI reference to dynamic UI object.
	 */
	public void parseDesignerRequest(RequestParser requestParser, DynamicUI dynamicUI);
	
	/**
	 * Exit to adjust the <code>propertyMaintenance</code> to the given <code>property</code> before
     * the maintenance of the property object starts. <br>
	 * 
     * @param property property object
     * @param propertyMaintenance property maintenance object
	 */
	public void adjustPropertyMaintenance(PropertyData property, PropertyMaintenance propertyMaintenance);
    
	/**
	 * Return if the given UI element is allowed to delete. <br>
	 * 
	 * @param dynamicUI dynamic UI object 
	 * @param element UI element to delete
	 * @param parent parent element of UI element
	 * @return <code>true</code> if the element is allowed to be deleted
	 */
	public boolean isDeletionAllowed(DynamicUI dynamicUI, UIElementBaseData element, UIElementGroupData parent);
	
	/**
	 * Exit to extend the MOVE UP logic. <br>
	 * 
     * @param dynamicUI dynamic UI object 
     * @param element UI element to move
     * @param parent parent element of UI element
	 */
	public void extendedElementMoveUp(DynamicUI dynamicUI, UIElementGroupData parent, UIElementBaseData element);

    /**
     * Exit to extend the MOVE DOWN logic. <br>
     * 
     * @param dynamicUI dynamic UI object 
     * @param element UI element to move
     * @param parent parent element of UI element
     */
	public void extendedElementMoveDown(DynamicUI dynamicUI, UIElementGroupData parent, UIElementBaseData element);

	/**
	 * <p> The method getBOType returns the type to get the backend object with the backend business object manager</p>
	 * 
	 * @return boType for the dynamicUI object.
	 */
	public String getBOType();

    
	/**
	 * <p> The method getLanguageMap provides the language map for description maintenance</p>
	 * 
	 * @param userSessionData user session data.
	 * @param mbom meta business object manager to have access to any business object which can maybe provide the
     *     language
	 * @return Map with languages.
	 */
	public Map getLanguageMap(UserSessionData userSessionData, 
            MetaBusinessObjectManager mbom);


}
