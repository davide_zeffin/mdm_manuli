/*****************************************************************************
    Interface:    DynamicUIData
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Author:       SAP
    Created:      19.02.2008
    Version:      1.0

*****************************************************************************/

package com.sap.isa.maintenanceobject.backend.boi;

import com.sap.isa.maintenanceobject.businessobject.Property;

/**
 * The interface describes the dynamic UI functionality. <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public interface DynamicUIData {
	
	/**
	 * Return a UI element group which contains all pages of the dynamic UI object. <br>
	 * 
	 * @return UI element group with all pages.
	 */
	public UIElementGroupData getPageGroup();
	
    /**
     * Synchronize all properties with bean properties.
     *
     * Looking for corresponding properties in the given bean and
     * get the values from this bean properties.
     * See also the method {@link Property#setValueFromBean(Object)} from the {@link Property} object. 
     */
    public void synchronizeProperties(Object bean);
    
    /**
     * Synchronize all properties with bean properties.
     *
     * Looking for corresponding properties in the given bean and
     * get the values from this bean properties.
     * See also the method {@link Property#setValueFromBean(Object)} from the {@link Property} object. 
     */
    public void synchronizeBeanProperties(Object bean);
    

    /**
     * <p> The method addElement allows to add a new UI element under full control of 
     * dynamic UI object. </p>
     * 
     * @param newElement new Element to 
     * @param parentGroup parentGroup for this element.
     */
    public void addElement(UIElementBaseData newElement, UIElementGroupData parentGroup);

    	
    /**
     * <p>Delete page of the dynamic UI. </p> 
     */
    public void deletePage(UIElementGroupData element);
    
    
    /**
     * <p>Delete all pages of the dynamic UI. </p> 
     */
    public void deleteAllPages();
    
    
    /**
     * Delete the given element from the given element group. <br>
     * 
     * @param element element to be delete
     * @param group group which holds the element to be deleted.
     */
    public void deleteElement(UIElementBaseData element, UIElementGroupData group);


}
