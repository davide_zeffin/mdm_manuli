/*****************************************************************************
    Class:        DynamicUIExitData
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      11.04.2008
    Version:      1.0
*****************************************************************************/

package com.sap.isa.maintenanceobject.backend.boi;

import com.sap.isa.core.util.RequestParser;
import com.sap.isa.maintenanceobject.businessobject.DynamicUI;
import com.sap.isa.maintenanceobject.businessobject.Property;

/**
 * The interface DynamicUIExitData provides exits to enhance the behaviour of a dynamic UI object. <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public interface DynamicUIExitData {
	
	/**
	 * <p> The method parseProperty allows you to enhance the parse proces for an property.</p>
	 * <p> E.g. you can read additional data stored in the request which belongs to the property. </p>
	 * 
	 * @param requestParser request parser allows the access to the request
	 * @param dynamicUI the dynamic ui object
	 * @param parameter parameter for the parsed property
	 * @param property the parsed property
	 */
	public boolean parseProperty(RequestParser requestParser, 
                              DynamicUI dynamicUI, 
                              RequestParser.Parameter parameter,
                              Property property);

}
