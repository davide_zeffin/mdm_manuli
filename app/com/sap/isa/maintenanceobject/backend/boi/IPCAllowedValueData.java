/*****************************************************************************
 Class:        IPCAllowedValueData
 Copyright (c) 2008, SAP AG, Germany, All rights reserved.
 Author:       SAP AG
 Created:      11.04.2008
 Version:      1.0
 *****************************************************************************/

package com.sap.isa.maintenanceobject.backend.boi;


//TODO docu
// TO avoid compile errors on JSP we need the interface in the appbase packages!!! 
/**
 * <p>The class IPCAllowedValueData . </p>
 * 
 * @author  SAP AG
 * @version 1.0
 */
public interface IPCAllowedValueData extends IPCImageLinkData {

	/**
	 * <p>Return the property {@link #additionalDocumentURL}. </p> 
	 *
	 * @return Returns the {@link #additionalDocumentURL}.
	 */
	public String getAdditionalDocumentURL();

	/**
	 * <p>Set the property {@link #additionalDocumentURL}. </p>
	 * 
	 * @param additionalDocumentURL The {@link #additionalDocumentURL} to set.
	 */
	public void setAdditionalDocumentURL(String additionalDocumentURL);

	/**
	 * <p>Return the property {@link #displayDocumentLink}. </p> 
	 *
	 * @return Returns the {@link #displayDocumentLink}.
	 */
	public boolean isDisplayDocumentLink();

	/**
	 * <p>Set the property {@link #displayDocumentLink}. </p>
	 * 
	 * @param displayDocumentLink The {@link #displayDocumentLink} to set.
	 */
	public void setDisplayDocumentLink(boolean displayDocumentLink);

	/**
	 * 
	 * Return the property {@link #longText}. <br>
	 * 
	 * @return longText
	 */
	public String getLongText();
	
	/**
	 *  Set the property {@link #longText}. <br>
	 * 
	 * @param longText The longText to set.
	 */
	public void setLongText(String longText);
		
	/**
	 * 
	 * Return the property {@link #soundURL}. <br>
	 * 
	 * @return soundURL
	 */
	public String getSoundURL();
	
	/**
	 *  Set the property {@link #soundURL}. <br>
	 * 
	 * @param soundURL The soundURL to set.
	 */
	public void setSoundURL(String soundURL);
	
}