/*****************************************************************************
    Class:        UIElementBaseData
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP
    Created:      November 2002
    Version:      1.0

*****************************************************************************/

package com.sap.isa.maintenanceobject.backend.boi;

import com.sap.isa.core.TechKey;

/**
 * Base interface for UI elements which contains properties like name and
 * descrition which are used from all UI elements. <br>
 * 
 * @see com.sap.isa.maintenanceobject.businessobject.DynamicUI
 *
 * @author SAP
 * @version 1.0
 */
public interface UIElementBaseData {

	/**
	 * Lowest level for setting of element to allowed. <br>
	 * See also {@link #setAllowed(boolean, int)} <br>
	 */
	public final static int DEFAULT = 0;
	
	/**
	 * Indicates that the allowed state was changed due to business logic. <br>
	 * See also {@link #setAllowed(boolean, int)} <br>
	 */
	public final static int BUSINESS_LOGIC = 100;

	/**
	 * Indicates that the allowed state was changed after authority checks.<br>
	 * See also {@link #setAllowed(boolean, int)} <br>
	 */
	public final static int AUTHORITY_CHECK = 500;
 
	/**
	 * Indicates that the allowed state set due to configurtation. <br>
	 * See also {@link #setAllowed(boolean, int)} <br>
	 */
	public final static int CONFIGURATION = Integer.MAX_VALUE;

	/**
	 * Return additional data object of the UI element. <br> 
	 *
	 * @return Returns the object with additional data.
	 */
	public Object getAdditionalData();
 
	/**
	 * Set a object with additonal data.. <br>
	 * 
	 * @param additionalData The object with additional data to set.
	 */
	public void setAdditionalData(Object additionalData);

	
    /**
     * Returns if the element is allowed to display.
     * <br> Only allowed element can be unhide, if they are hidden
     * @return boolean
     */
    public boolean isAllowed();

	/**
	 * Sets if the element is allowed to display.
	 * <br> Only allowed element can be unhide, if they are hidden
	 * @param allowed flag to set
	 */
	public void setAllowed(boolean allowed);

	
    /**
     * <p>Sets if the element is allowed to display.</p>
     * <p>Only allowed element can be unhide, if they are hidden. </p>
	 * <p> The level concept allows to handle different levels on which
     *     an UI element can be controlled. E.g. If an element is hidden by configuration
     *     the authorithy can not show this element again. Therefor only if the level is higher as
     *     the current level an element can be set to <em>allowed</em>". </p>
     *     
	 * @param allowed allowed flag to set
	 * @param level strength the reason and importance why the flag was set
     */
    public void setAllowed(boolean allowed, int changeLevel);


    /**
     * Set the description of the element. <br>
     *
     * @param description new description of the element.
     */
    public void setDescription(String description);


    /**
     * Returns the description of the element.<br>
     *
     * @return description of the element.
     */
    public String getDescription();


    /**
     * Returns if the element is visible.<br>
     *
     * @return <code>true</code> if the element is hidden and
     *         <code>false</code> if the element is visible
     */
    public boolean isHidden();


    /**
     * Sets the hidden flag. <br>
     *
     * @param hidden The hidden flag to be set
     */
    public void setHidden(boolean hidden);


    /**
     * Set the name of the element. <br>
     *
     * @param name
     *
     */
    public void setName(String name);


    /**
     * Returns the name of the element. <br>
     *
     * @return name
     *
     */
    public String getName();


    /**
     * Returns if the object is structured.
     */
    public boolean isStructured();
  
    
	/**
	 * Return the technical key of the element. <br> 
	 *
	 * @return Returns the technical key.
	 */
	public TechKey getTechKey();

	
	/**
	 * Set the technical key of the element. <br>
	 * 
	 * @param techKey The technical key  to set.
	 */
	public void setTechKey(TechKey techKey);


    
	/**
	 * Set the UI type of the element. <br>
	 *
	 * @param uiType
	 *
	 */
	public void setUiType(String uiType);


	/**
	 * Returns the UI type of the given element. <br>
	 *
	 * @return uiType
	 *
	 */
	public String getUiType();
	
	
	/**
	 * Returns the access key of the given element. <br>
	 * 
	 * @return accessKey
	 */
	public String getAccessKey();
	
	
	/**
	 * Set the access key of the element. <br>
	 * 
	 * @param accessKey
	 */
	public void setAccessKey(String accessKey);


    /**
     * Returns the tab index of the given element. <br>
     * 
     * @return tabIndex
     */
    public String getTabIndex();
    
    
    /**
     * Set the tab index of the element. <br>
     * 
     * @param tabIndex
     */
    public void setTabIndex(String tabIndex);

    /**
	 * Return the status of the UI element. <br> 
	 *
	 * @return Returns the the status of the UI element.
	 */
	public UIElementStatus getStatus(); 

	/**
	 * Set the status of the UI element. <br>
	 * 
	 * @param status The status to set.
	 */
	public void setStatus(UIElementStatus status);
    

    /**
     * Returns the style classes of the given element. <br>
     * 
     * @return String[] styleClasses
     */
    public String[] getStyleClasses();
    
    
    /**
     * Set the style classes of the element. <br>
     * 
     * @param styleClasses
     */
    public void setStyleClasses(String[] styleClasses);
    

    /**
     * Returns true if labels for the property are retrieved from the resource key file.<br> 
     * 
     * @return boolean
     */
    public boolean isRessourceKeyUsed();
    
    
    /**
     * Set the parameter for isRessourceKeyUsed <br>
     * 
     * @param ressourceKeyUsed flag if the resource key is used.
     */
    public void setRessourceKeyUsed(boolean ressourceKeyUsed);    
}
