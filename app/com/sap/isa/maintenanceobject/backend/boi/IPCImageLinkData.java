/*****************************************************************************
 Class:        IPCPropertyData
 Copyright (c) 2008, SAP AG, Germany, All rights reserved.
 Author:       SAP AG
 Created:      11.04.2008
 Version:      1.0
 *****************************************************************************/

package com.sap.isa.maintenanceobject.backend.boi;

//TO avoid compile errors on JSP we need the interface in the appbase packages!!! 

//TODO docu
/**
 * <p>The class IPCPropertyData . </p>
 *
 * @author  SAP AG
 * @version 1.0
 */
public interface IPCImageLinkData {

	
	public final static String IMAGE_LINK_DISPLAY_TYPE_THUMBNAIL = "";

	public final static String IMAGE_LINK_DISPLAY_TYPE_HYPERLINK = "1"; 

	public final static String IMAGE_LINK_DISPLAY_TYPE_NOTHING = "2"; 
	
	/**
	 * <p>Return the property {@link #displayImageType}. </p> 
	 *
	 * @return Returns the {@link #displayImageType}.
	 */
	public String getDisplayImageType();

	/**
	 * <p>Set the property {@link #displayImageType}. </p>
	 * 
	 * @param displayImageType The {@link #displayImageType} to set.
	 */
	public void setDisplayImageType(String displayImageType);

	/**
	 * <p>Return the property {@link #imageURL}. </p> 
	 *
	 * @return Returns the {@link #imageURL}.
	 */
	public String getImageURL();

	/**
	 * <p>Set the property {@link #imageURL}. </p>
	 * 
	 * @param imageURL The {@link #imageURL} to set.
	 */
	public void setImageURL(String imageURL);

	/**
	 * <p>Return the property {@link #thumbnailURL}. </p> 
	 *
	 * @return Returns the {@link #thumbnailURL}.
	 */
	public String getThumbnailURL();

	/**
	 * <p>Set the property {@link #thumbnailURL}. </p>
	 * 
	 * @param thumbnailURL The {@link #thumbnailURL} to set.
	 */
	public void setThumbnailURL(String thumbnailURL);

	/**
	 * 
	 * Return the property {@link #o2cImageURL}. <br>
	 * 
	 * @return o2cImageURL
	 */
	public String getO2cImageURL();
	
	/**
	 *  Set the property {@link #o2cImageURL}. <br>
	 * 
	 * @param image The o2cImage to set.
	 */
	public void setO2cImageURL(String o2cImageURL);

	/**
	 * 
	 * Return the property {@link #soundURL}. <br>
	 * 
	 * @return soundURL
	 */
	public String getSoundURL();
	
	/**
	 *  Set the property {@link #soundURL}. <br>
	 * 
	 * @param soundURL The soundURL to set.
	 */
	public void setSoundURL(String soundURL);
	
	/**
	 *  Set the property {@link #thumbnailWidth}. <br>
	 * 
	 * @param thumbnailWidth The thumbnailWidth to set.
	 */	
	public void setThumbnailWidth(String thumbnailWidth);
	
	/**
	 * 
	 * Return the property {@link #thumbnailWidth}. <br>
	 * 
	 * @return thumbnailWidth
	 */	
	public String getThumbnailWidth();
	
	/**
	 *  Set the property {@link #thumbnailHeight}. <br>
	 * 
	 * @param thumbnailHeight The thumbnailHeight to set.
	 */	
	public void setThumbnailHeight(String thumbnailHeight);
	
	/**
	 * 
	 * Return the property {@link #thumbnailHeight}. <br>
	 * 
	 * @return thumbnailHeight
	 */	
	public String getThumbnailHeight();
	
}