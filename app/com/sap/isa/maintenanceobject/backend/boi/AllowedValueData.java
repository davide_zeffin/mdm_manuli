
/*****************************************************************************
    Class:        AllowedValueData
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP
    Created:      Januray 2002
    Version:      1.0
*****************************************************************************/

package com.sap.isa.maintenanceobject.backend.boi;

/**
 * Represent an allow value for a property. With allowed values you can restrict
 * the input of the user to reasonable values. At the moment only String
 * properties are support.
 *
 * @author SAP
 * @version 1.0
 *
 * @see com.sap.isa.maintenanceobject.businessobject.Property
 * @see com.sap.isa.maintenanceobject.businessobject.MaintenanceObject
 */
public interface AllowedValueData {

	/**
	 * Return additional data object of the value. <br> 
	 *
	 * @return Returns the object with additional data.
	 */
	public Object getAdditionalData();
 
	/**
	 * Set a object with additonal data. <br>
	 * 
	 * @param additionalData The object with additional data to set.
	 */
	public void setAdditionalData(Object additionalData);

    /**
     * Set the property description
     *
     * @param description
     *
     */
    public void setDescription(String description);


    /**
     * Returns the property description
     *
     * @return description
     *
     */
    public String getDescription();


     /**
      * Returns if the value is visible
      * 
      * @return <code>true</code>  if the value is hidden     
      *     and  <code>false</code> if the value is visible
      */
     public boolean isHidden() ;     


     /**
      * Sets the hidden flag. 
      * 
      * @param hidden The hidden flag to be set
      */
     public void setHidden(boolean hidden);    


     /**
      * Returns true if labels for the property are retrieved from the resource key file.<br> 
      * 
      * @return boolean
      */
     public boolean isRessourceKeyUsed();
     
     
     /**
      * Set the parameter for isRessourceKeyUsed <br>
      * 
      * @param ressourceKeyUsed flag if the resource key is used.
      */
     public void setRessourceKeyUsed(boolean ressourceKeyUsed);    
    
     /**
 	 * Return the status of the value. <br> 
 	 *
 	 * @return Returns the the status of the value.
 	 */
 	public UIElementStatus getStatus(); 

 	/**
 	 * Set the status of the value. <br>
 	 * 
 	 * @param status The status to set.
 	 */
 	public void setStatus(UIElementStatus status);
     

    /**
     * Set the property value
     *
     * @param value
     *
     */
    public void setValue(String value);


    /**
     * Returns the property value
     *
     * @return value
     *
     */
    public String getValue();
    
    
	/**
	  * Returns if the AllowedValue is read only.<br>
	  *
	  * @return <code>true</code> if the AllowedValue is read only and
	  *         <code>false</code> if the AllowedValue is read write
	  */
	public boolean isReadOnly();


	/**
	  * Sets the readOnly flag. <br>
	  *
	  * @param readOnly The readOnly flag to be set
	  */
	public void setReadOnly(boolean readOnly);
    
}
