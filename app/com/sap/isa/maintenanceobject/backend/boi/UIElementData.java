/*****************************************************************************
 Class:        UIElementData
 Copyright (c) 2004, SAP AG, Germany, All rights reserved.
 Author:       SAP AG
 Created:      13.10.2004
 Version:      1.0

 $Revision: #13 $
 $Date: 2003/05/06 $ 
 *****************************************************************************/

package com.sap.isa.maintenanceobject.backend.boi;


/**
 * Base interface for UI elements which contains properties like name and
 * descrition which are used from all editable UI elements.
 *
 * @author  SAP AG
 * @version 1.0
 */
public interface UIElementData extends UIElementBaseData, Cloneable {
    
 
	/**
	 * Return the container for Backend depending properties. <br>
	 * 
	 * @return
	 */
	public Object getBackendProperties() ;


	/**
	 * Set the container for Backend depending properties. <br>
	 * 
	 * @param backendProperties backend properties.
	 */
	public void setBackendProperties(Object backendProperties);

    /**
     * Set the property disabled to the given value. <br>
     *
     * @param disabled
     *
     */
    public void setDisabled(boolean disabled);

    /**
     * Disables the element. <br>
     *
     * @param disabled
     *
     */
    public void setDisabled();

    /**
     * Enables the elements. <br>
     *
     * @param disabled
     *
     */
    public void setEnabled();

    /**
     * Returns if the element is disabled. <br>
     *
     * @return <code>true</code> if the element isn't editable and <code>false</code>
     *    if the element is editable
     *
     */
    public boolean isDisabled();

    /**
     * Returns if the element is enabled. <br>
     *
     * @return <code>true</code> if the element is editable and <code>false</code>
     *    if the element isn't editable
     *
     */
    public boolean isEnabled();
    
    
	/**
	 * Clones the element. <br>
	 * All including objects should be also cloned.
	 * 
	 * @return copy of the object and all its sub objects
	 * 
	 * @see java.lang.Object#clone()
	 */
	public Object clone() throws CloneNotSupportedException; 

}