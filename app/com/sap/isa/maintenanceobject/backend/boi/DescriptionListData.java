/*****************************************************************************
 Class:        DescriptionListData
 Copyright (c) 2008, SAP AG, Germany, All rights reserved.
 Author:       SAP AG
 Created:      18.04.2008
 Version:      1.0
 *****************************************************************************/

package com.sap.isa.maintenanceobject.backend.boi;

import com.sap.isa.core.Iterable;

public interface DescriptionListData extends Iterable, Cloneable {

	/**
	 * <p> The method addDescription add a description to the list of description. </p>
	 * 
	 * @param description description to add.
	 */
	public void addDescription(DescriptionData description);

	/**
	 * <p> The method getDescription returns the description for the given language. </p>
	 * 
	 * @param language
	 * @return description for the given language.
	 */
	public DescriptionData getDescription(String language);

	/**
	 * <p> The method deleteDescription delete the description for the given language. </p>
	 * 
	 * @param language
	 */
	public void deleteDescription(String language);

	
	/**
	 * <p>Overwrites/Implements the method <code>clone</code> 
	 * of the <code>DescriptionList</code>. </p>
	 * 
	 * @return a deep copy of the Description List.
	 * 
	 * @see java.lang.Object#clone()
	 */
	public Object clone();

}