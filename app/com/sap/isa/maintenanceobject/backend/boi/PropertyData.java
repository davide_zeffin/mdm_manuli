/*****************************************************************************
    Class:        PropertyData
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      Januray 2002
    Version:      1.0

    $Revision: #7 $
    $Date: 2001/07/25 $
*****************************************************************************/

package com.sap.isa.maintenanceobject.backend.boi;

import java.util.Enumeration;
import java.util.List;

import com.sap.isa.core.Iterable;
import com.sap.isa.core.util.table.Table;

/**
 * The interface describes a property object.
 *
 * @author Wolfgang Sattler
 * @version 1.0
 */
public interface PropertyData extends UIElementData, Iterable {

    /* Allowed Datatypes for properties */

    /**
     * Constant for datatype String. <br>
     */
    public static String TYPE_STRING = "String";

    /**
     * Constant for datatype Boolean. <br>
     */
    public static String TYPE_BOOLEAN = "Boolean";

    /**
     * Constant for datatype Integer. <br>
     */
    public static String TYPE_INTEGER = "Integer";

    /**
     *
     * Constant for datatype Enumeration (Strings)
     *
     */
    public static String TYPE_ENUMERATION = "Enumeration";

    /**
     *
     * Constant for datatype Table
     *
     */
    public static String TYPE_TABLE = "Table";
    
    /**
     * 
     * Constant for datatype List (multiple String values). <br>
     * 
     */
    public static String TYPE_LIST = "List";


    /**
     * Add an allowed Value to the property
     *
     * @param the allowedValue
     *
     */
    public void addAllowedValue(AllowedValueData allowedValue);
  
    
    /**
	 * <p>Return the property {@link #allowedValues}. </p> 
	 *
	 * @return Returns the {@link #allowedValues}.
	 */
	public List getAllowedValues();

	/**
	 * <p>Set the property {@link #allowedValues}. </p>
	 * 
	 * @param allowedValues The {@link #allowedValues} to set.
	 */
	public void setAllowedValues(List allowedValues);

    
    
    /**
     * Return the property beanAvailable . <br>
     * This switch controls if the property could be syncronized with a 
     * corresponding bean property. 
     * 
     * @return beanAvailable
     */
    public boolean isBeanAvailable();

    /**
     * Return Set the property beanAvailable. <br>
     * This switch controls if the property could be syncronized with a 
     * corresponding bean property. 
     * 
     * @param beanAvailable property beanAvailable
     */
    public void setBeanAvailable(boolean beanAvailable);

    /**
     * Return the container for Backend depending properties. <br>
     * 
     * @return
     */
    public Object getBackendProperties();

    /**
     * Set the container for Backend depending properties. <br>
     * 
     * @param backendProperties backend properties.
     */
    public void setBackendProperties(Object backendProperties);

    /**
     * Set the description for the value
     *
     * @param valueDescription
     *
     */
    public void setValueDescription(String valueDescription);

    /**
     * Returns the description of the value
     *
     * @return valueDescription
     *
     */
    public String getValueDescription();

    /**
     * Set the property disabled
     *
     * @param disabled
     *
     */
    public void setDisabled(boolean disabled);

    /**
     * Returns the property disabled
     *
     * @return disabled
     *
     */
    public boolean isDisabled();

    /**
     * Set the property exitField
     *
     * @param exitField
     *
     */
    /*  public void setExitField(String exitField); */

    /**
     * Set the property exitField
     *
     * @param exitField
     *
     */
    public void setExitField(boolean exitField);

    /**
     * Returns the property exitField
     *
     * @return exitField
     *
     */
    public boolean isExitField();

    /**
     * Returns if a help description is available for this property.
     * @return boolean
     */
    public boolean isHelpAvailable();

    /**
     * Sets if a help description is available for this property.
     * @param helpAvailable The helpAvailable to set
     */
    public void setHelpAvailable(boolean helpAvailable);

    /**
     * Returns an list with the help descriptions
     * 
     * @return list to iterate over help lines
     */
    public List getHelpDescription();

    /**
     * Set the property helpValuesMethod
     *
     * @param helpValuesMethod
     *
     */
    public void setHelpValuesMethod(String helpValuesMethod);

    /**
     * Returns the property helpValuesMethod
     *
     * @return helpValuesMethod
     *
     */
    public String getHelpValuesMethod();

	/**
	 * Return if the property is read only. <br> 
	 *
	 * @return Returns the if the property is read only.
	 */
	public boolean isReadOnly();

	/**
	 * Set the property read only. <br>
	 * 
	 * @param isReadOnly The isInputAllowed to set.
	 */
	public void setReadOnly(boolean isReadOnly);
    
    
    /**
     * Returns the maxLength.
     * @return int
     */
    public int getMaxLength();

    /**
     * Sets the maxLength.
     * @param maxLength The maxLength to set
     */
    public void setMaxLength(int maxLength);

    /**
     * Set the property required
     *
     * @param required
     *
     */
    public void setRequired(boolean required);

    /**
     * Returns the property required
     *
     * @return required
     *
     */
    public boolean isRequired();

    /**
     * Returns the size.
     * @return int
     */
    public int getSize();

    /**
     * Sets the size.
     * @param size The size to set
     */
    public void setSize(int size);

    /**
     * Set the property type
     *
     * @param type
     *
     */
    public void setType(String type);

    /**
     * Returns the property type
     *
     * @return type
     *
     */
    public String getType();

    /**
     * Set the property value
     *
     * @param value
     *
     */
    public void setValue(Object value);

    /**
     * Returns the property value
     *
     * @return value
     *
     */
    public Object getValue();


    /**
	 * <p>Return if the value of the property has changed. </p> 
	 *
	 * @return Returns if the value of the property has changed.
	 */
	public boolean isValueChanged();

	
	/**
	 * <p>Set the property flag that describes if the value has changed. </p>
	 * 
	 * @param isValueChanged Flag if the value of the property has changed.
	 */
	public void setValueChanged(boolean isValueChanged);

    
    
    /**
     * Clears the value of the property
     *
     */
    public void clearValue();

    /**
     * Set the property value
     *
     * @param value
     *
     */
    public void setValue(String value);

    /**
     * Returns the value as String
     *
     * @return value
     *
     */
    public String getString();

    /**
     * Set the property value
     *
     * @param value
     *
     */
    public void setValue(boolean value);

    /**
     * Returns the value as boolean
     *
     * @return value
     *
     */
    public boolean getBoolean();

    /**
     * Set the property value
     *
     * @param value
     *
     */
    public void setValue(int value);

    /**
     * Returns the value as int
     *
     * @return value
     *
     */
    public int getInteger();

    /**
     * Set the property value
     *
     * @param value
     *
     */
    public void setValue(Enumeration value);

    /**
     * Returns the value as Enumeration
     *
     * @return value
     *
     */
    public Enumeration getEnumeration();

    /**
     * Set the property value
     *
     * @param value
     *
     */
    public void setValue(Table value);

    /**
     * Returns the property value
     *
     * @return value
     *
     */
    public Table getTable();

    
    /**
     * Set the property value of type {@link #TYPE_LIST}. <br>
     *
     * @param value
     *
     */
    public void setValue(List value);

    
    /**
     * Returns the property value for type {@link #TYPE_LIST}. <br>
     *
     * @return value
     *
     */
    public List getList();


    /**
     * Set the default value of the property
     *
     * @param value
     */
    public void setDefaultValue(Object defaultValue);

    /**
     * Returns the default value of the property
     *
     * @return value
     */
    public Object getDefaultValue();

    /**
     * Add a help description to the property
     *
     * @param the help description
     *
     */
    public void addHelpDescription(String helpDescription);

    /**
     * Returns true for simple types like String, boolean and int
     * @return
     */
    public boolean isSimpleType();

    /**
     * Returns true if labels for the property are retrieved from the resource key file. 
     */
    public boolean isRessourceKeyUsed();

    /**
     * Set the parameter for isRessourceKeyUsed
     * @param ressourceKeyUsed
     */
    public void setRessourceKeyUsed(boolean ressourceKeyUsed);
    
	/**
	 * <p>Return the property {@link #requestParameterName}. </p>
	 * <p><strong>Note:</strong> If the property isn't maintained the name of the property is used 
	 * automatically</p> 
	 *
	 * @return Returns the {@link #requestParameterName}.
	 */
	public String getRequestParameterName();

	/**
	 * <p>Set the property {@link #requestParameterName}. </p>
	 * 
	 * @param requestParameterName The {@link #requestParameterName} to set.
	 */
	public void setRequestParameterName(String requestParameterName);
    
    /**
     * Sets the help values field name.
     */
    public String getHelpValuesFieldName();

    /**
     * Returns the help values field name.
     */
    public void setHelpValuesFieldName(String helpValuesFieldName);
    
    /**
     * Return the name of a help values search for the property. <br>
     * 
     * @return name of the help value search.
     */
    public String getHelpValuesSearch();

    /**
     * Set the name of a help value search to use for the property . <br>
     * 
     * @param helpValuesSearch
     */
    public void setHelpValuesSearch(String helpValuesSearch);

    
    /**
     * Set the value of the corresponding bean property in the given bean object
     * with the current value of the property. <br>
     *
     * @param bean reference to the besan object.
     *
     */
    public void setBeanPropertyValue(Object bean);
    
    
    /**
     * Set the value to the value of corresponding bean property in the 
     * given bean. <br>
     *
     * @param bean reference to the besan object.
     *
     */
    public void setValueFromBean(Object bean);
    
    
    /**
     * Returns the input pattern for the property. <br>
     *  
     * @return inputPattern
     */
    public String getInputPattern();
    
    
    /**
     * Set the input pattern for the property. <br>
     * 
     * @param inputPattern
     */
    public void setInputPattern(String inputPattern);
    
    
    /**
     * Returns the height of the input field in pixel or rows. <br>
     * 
     * @return height (pixel or rows)
     */
    public String getHeight();
    
    
    /**
     * Set the height of the input field in pixel or rows. <br>
     * 
     * @param height (pixel or rows)
     */
    public void setHeight(String height);
    
    
    /**
     * Returns the name of the JavaScript function for the given exit field. <br>
     * 
     * @return exitFieldFunction
     */
    public String getExitFieldFunction();
    
    
    /**
     * Set the name of the JavaScript function for the given exit field. <br>
     * 
     * @param exitFieldFunction
     */
    public void setExitFieldFunction(String exitFieldFunction);
   
	/**
	 * Returns the default date format. <br>
	 * 
	 * @return defaultDateFormat
	 */
	public String getDefaultDateFormat();
    
    
	/**
	 * Set the default date format. <br>
	 * 
	 * @param dateFormat
	 */
	public void setDefaultDateFormat(String dateFormat);
 
    /**
     * Returns the input interval for the property. <br>
     *  
     * @return inputInterval
     */
    public String getInputInterval();
    
    
    /**
     * Set the input interval for the property. <br>
     * 
     * @param inputInterval
     */
    public void setInputInterval(String inputInterval);
	
}

