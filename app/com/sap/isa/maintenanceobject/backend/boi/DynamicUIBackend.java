/*****************************************************************************
    Interface:    DynamicUIBackend
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Author:       SAP
    Created:      19.02.2008
    Version:      1.0

*****************************************************************************/
package com.sap.isa.maintenanceobject.backend.boi;

import com.sap.isa.core.eai.BackendException;
import com.sap.isa.maintenanceobject.businessobject.DynamicUI;

/**
 * <p>The DynamicUIBackend interfaces described a backend object, which provide the data for a dynamic UI object 
 * from the backend. </p>
 *   
 * @author SAP
 */
public interface DynamicUIBackend {
	 
	/**
     * This method read the values of the dynamic UI object from the backend. <br>
     *
     * @param dynamicUI
     */
    public void readData(DynamicUI dynamicUI) throws BackendException;

    

    
}
