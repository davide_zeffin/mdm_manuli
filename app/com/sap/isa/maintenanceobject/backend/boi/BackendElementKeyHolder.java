/*
 * Created on 23.11.2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.maintenanceobject.backend.boi;

/**
 * @author d020955
 * 
 *  Interface for generic access to a key for a backend field element name  
 *  fieldname and field description name. <br>
 */

public interface BackendElementKeyHolder {

	/**
	 * Provides generic key for a backend field element. <br>
	 * 
	 * @return generic key
	 */
	public Object getKey();

}
