/*****************************************************************************
 Class:        DescriptionData
 Copyright (c) 2008, SAP AG, Germany, All rights reserved.
 Author:       SAP AG
 Created:      18.04.2008
 Version:      1.0
 *****************************************************************************/

package com.sap.isa.maintenanceobject.backend.boi;

public interface DescriptionData {

	/**
	 * <p>Return the property {@link #description}. </p> 
	 *
	 * @return Returns the {@link #description}.
	 */
	public String getDescription();

	/**
	 * <p>Set the the description. </p>
	 * 
	 * @param description The {@link #description} to set.
	 */
	public void setDescription(String description);

	/**
	 * <p>Return the language of the decription. </p> 
	 *
	 * @return Returns the language.
	 */
	public String getLanguage();

	/**
	 * <p>Set the language of the description. </p>
	 * 
	 * @param language The language to use for this description.
	 */
	public void setLanguage(String language);

}