/*****************************************************************************
    Class:        MaintenanceObjectBackend
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      February 2002
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.maintenanceobject.backend.boi;

import com.sap.isa.core.eai.BackendException;
import com.sap.isa.maintenanceobject.util.HelpDescriptions;


/**
 * Describes the interface from a maintenance object with the backend.
 *
 * @author Wolfgang Sattler
 * @version 1.0
 */
public interface MaintenanceObjectBackend
{

    /**
     * This method initialize the maintenance object in the backend.
     *
     * @param maintenanceObject
     */
    public void initObject(MaintenanceObjectData maintenanceObject)
            throws BackendException;
            
	/**
	 * Set a lock to the maintenance object
	 * 
     * @param maintenanceObject
     * @return if the object is locked
     */   
    public boolean lockObject(MaintenanceObjectData maintenanceObject)
            throws BackendException;        

          
	/**
	 * Delete a lock from the maintenance object
	 * 
     * @param maintenanceObject
     */   
    public void unlockObject(MaintenanceObjectData maintenanceObject)
            throws BackendException;        


    /**
     * This method returns help Values form the backend.
     *
     * @param maintenanceObject
     * @param property
     */
    public HelpDescriptions getHelpDescriptions(MaintenanceObjectData maintenanceObject,
                                                PropertyData          property)
            throws BackendException;
            
}
