/*****************************************************************************
    Class:        MaintenanceObjectDataHandler
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      February 2002
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.maintenanceobject.backend;

import com.sap.isa.maintenanceobject.backend.boi.*;

import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.*;
import com.sap.isa.core.util.table.ResultData;
import java.util.*;


/**
 * The MaintenanceObjectDataHandler describes an interface for all classes which
 * will handle the data of a maintenance object.
 *
 * @author Wolfgang Sattler
 * @version 1.0
 *
 */
public interface MaintenanceObjectDataHandler {

    /**
     * This method initialize the maintenance object handler in the backend.
     * The method will read the given properties to connect to a data base
     * or get the filename for a appropriate file.
     *
     * @param connectionFactory to get connections
     * @param props a set of properties which may be useful to initialize the object
     */
    public void initHandler(ConnectionFactory connectionFactory, Properties props)
        throws MaintenanceObjectHelperException;


    /**
     * Reads a list of shops and returns this list
     * using a tabluar data structure.
     *
     * @param objectId object id as search pattern
     * @param props, additional properties to restrict the search result
     * @return List of all available objects
     */
    public ResultData readObjectList(String objectId, Properties props)
        throws MaintenanceObjectHelperException;


    /**
     * This method deletes a maintenance object with the key <code>objectKey</code>
     *
     * @param objectKey <code>TechKey</code> of the object
     */
    public void deleteObject(TechKey objectKey)
            throws MaintenanceObjectHelperException;


    /**
     * This method read the values of the maintenance object from the backend
     *
     * @param maintenanceObject
     */
    public void readData(MaintenanceObjectData maintenanceObject)
        throws MaintenanceObjectHelperException;


    /**
     * This method save the maintenance object in backend
     *
     * @param maintenanceObject
     */
    public void saveData(MaintenanceObjectData maintenanceObject)
        throws MaintenanceObjectHelperException;

}

