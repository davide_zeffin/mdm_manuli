/*
 * Created on Dec 2, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.maintenanceobject.backend;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.BitSet;
import java.util.Enumeration;
import java.util.Properties;

import com.sap.isa.core.db.jdbc.JdbcDAO;
import com.sap.isa.core.db.jdbc.JdbcPersistenceManager;
import com.sap.isa.core.db.jdbc.ListResultSetReader;
import com.sap.isa.core.db.jdbc.ResultSetReader;
import com.sap.isa.core.db.jdbc.RowReader;
import com.sap.isa.core.db.jdbc.SQLParameter;
import com.sap.isa.core.db.jdbc.SQLQuery;
import com.sap.isa.core.db.jdbc.SQLQueryProvider;
import com.sap.isa.core.util.OIDGenerator;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class KeyValDAO extends JdbcDAO implements Serializable {

	public static final String TABLENAME = "CRM_ISA_KEYVALUES";
	public static final String CLIENT = "CLIENT";
	public static final String SYSTEMID = "SYSTEMID";
	public static final String GRP_NAME = "GRP_NAME";
	public static final String PROP_NAME = "PROP_NAME";
	public static final String DATA_TYPE = "DATA_TYPE";
	public static final String[] COLUMNS = { "KVID" , CLIENT , SYSTEMID , DATA_TYPE , "GRP_NAME" , PROP_NAME , "PROP_VALUE"
	};
	public static final String[] KEYCOLUMNS = { COLUMNS[0] };
	
	private String gUID;
	private String groupName;
	private String dataSource;
	private String propName;
	private String propValue;
	private String client;
	private String systemID;
	private static String INSERTSQL;
	private static String UPDATESQL;
	private static String DELETESQL;
	private static String SELECTSQL;
	
	static  {
		INSERTSQL = 
			SQLQueryProvider.getQueryProvider().getInsertSQLQuery(TABLENAME,COLUMNS);
		UPDATESQL = 
			SQLQueryProvider.getQueryProvider().getUpdateSQLQuery(TABLENAME,new String[]{COLUMNS[6]} , KEYCOLUMNS);		
		DELETESQL = 
			SQLQueryProvider.getQueryProvider().getDeleteSQLQuery(TABLENAME, new String[] { COLUMNS[4] } );
		SELECTSQL = 
			SQLQueryProvider.getQueryProvider().getSelectSQLQuery(TABLENAME,COLUMNS,KEYCOLUMNS);			
	}
	
	public KeyValDAO()  {
		
	}
	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.jdbc.JdbcDAO#doLoad(java.sql.Connection)
	 */
	protected void doLoad(Connection arg0) throws SQLException {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.jdbc.JdbcDAO#doInsert(java.sql.Connection)
	 */
	protected void doInsert(Connection conn) throws SQLException {
		PreparedStatement stmt = conn.prepareStatement(INSERTSQL);
		stmt.setBytes(1,OIDGenerator.hexStringToBytes(OIDGenerator.newOID()));
		stmt.setString(2,client);
		stmt.setString(3,systemID);	
		stmt.setString(4,dataSource);
		stmt.setString(5, groupName);
		stmt.setString(6,propName);
		stmt.setString(7,getPropValue());		
		stmt.execute();
		stmt.close();
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.jdbc.JdbcDAO#doUpdate(java.util.BitSet, java.sql.Connection)
	 */
	protected void doUpdate(BitSet arg0, Connection conn) throws SQLException {
		
		StringBuffer updateBuffer = new StringBuffer(UPDATESQL);
		PreparedStatement stmt = conn.prepareStatement(updateBuffer.toString());
		stmt.setString(1,getPropValue());
		stmt.setBytes(2, OIDGenerator.hexStringToBytes(getKey().toString()));
		stmt.execute();
		stmt.close();
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.jdbc.JdbcDAO#doDelete(java.sql.Connection)
	 */
	protected void doDelete(Connection conn) throws SQLException {
		StringBuffer updateBuffer = new StringBuffer(DELETESQL);
		PreparedStatement stmt = conn.prepareStatement(updateBuffer.toString());
		stmt.setString(1, getGroupName());
		stmt.execute();
		stmt.close();
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.jdbc.JdbcDAO#getKeyColumns()
	 */
	protected String[] getKeyColumns() {
		return COLUMNS;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.jdbc.JdbcDAO#getResultSetReader()
	 */
	public ResultSetReader getResultSetReader() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public static SQLQuery getQuery(Properties props , Properties verticalProps , JdbcPersistenceManager pm)  {
		SQLQuery query = (SQLQuery)pm.newSearchQuery(null);
		query.setResultSetReader(new ListResultSetReader(new DAOReader()));
		String sql = SQLQueryProvider.getQueryProvider().getSelectSQLQuery(TABLENAME , COLUMNS , null);
		StringBuffer queryBuffer = new StringBuffer();
		queryBuffer.append(sql);
		queryBuffer.append(" WHERE ( ");
		Enumeration enum = verticalProps.keys();
		String prop = null;
		SQLParameter param = null;
		while(enum.hasMoreElements())  {
			prop = (String)enum.nextElement();
			queryBuffer.append(addFilter(prop , verticalProps.getProperty(prop).indexOf("*") >= 0));
			if(verticalProps.getProperty(prop).indexOf("*") >= 0)  {
				verticalProps.setProperty(prop , verticalProps.getProperty(prop).replace('*' , '%'));
			}
			if(enum.hasMoreElements())  {
				queryBuffer.append(" AND ");
			}
			param = new SQLParameter();
			param.setName(prop);
			param.setJdbcType(Types.VARCHAR);
			query.addParameter(param);
		}
		queryBuffer.append(" ) ");
		
		if(props != null)  {
			enum = props.keys();
			if(enum.hasMoreElements())  {
				queryBuffer.append(" AND GRP_NAME IN ( ");
				queryBuffer.append(getNestedSQL(props));
				queryBuffer.append(" ) ");
			}
		}
		
		query.setSQL(queryBuffer.toString());
		query.setSQLComplete(true);
		return query;
	}
	
	private static String getNestedSQL(Properties props)  {
		StringBuffer queryBuffer = new StringBuffer();
		final String sql = "SELECT A.GRP_NAME FROM CRM_ISA_KEYVALUES A , CRM_ISA_KEYVALUES B WHERE A.GRP_NAME = B.GRP_NAME AND A.PROP_NAME='";
		final String sql2 = "SELECT A.GRP_NAME FROM CRM_ISA_KEYVALUES A WHERE A.PROP_NAME='";
		 
		Enumeration enum;
		String prop = null;
		String key1 = null, key2 = null;
		if(props != null)  {
			if(2 == props.size())  {
				queryBuffer.append(sql);
				enum = props.keys();
				if(enum.hasMoreElements())  {
					prop = (String)enum.nextElement();
					queryBuffer.append(prop);
					queryBuffer.append("' AND A.PROP_VALUE='");
					queryBuffer.append(props.getProperty(prop));
					queryBuffer.append("'");
					key1 = prop;
				}
				if(enum.hasMoreElements())  {
					prop = (String)enum.nextElement();
					queryBuffer.append(" AND B.PROP_NAME='");
					queryBuffer.append(prop);
					queryBuffer.append("' AND B.PROP_VALUE='");
					queryBuffer.append(props.getProperty(prop));
					queryBuffer.append("'");
					props.remove(prop);
					key2 = prop;
				}
				if(key1 != null)
					props.remove(key1);
				if(key2 != null)
					props.remove(key2);
			}  else  if(props.size() == 1){
				queryBuffer.append(sql2);
				enum = props.keys();
				if(enum.hasMoreElements())  {
					prop = (String)enum.nextElement();
					queryBuffer.append(prop);
					queryBuffer.append("' AND A.PROP_VALUE='");
					queryBuffer.append(props.getProperty(prop));
					queryBuffer.append("'");
					props.remove(prop);
				}
			}
		}
		return queryBuffer.toString();
	}
	
	private static String addFilter(String field , boolean like)  {
		if(like)
			return field + " like ?";
		return field + "= ? ";
	}
	
	private static String addFilter(String field , String value)  {
		return "(PROP_NAME = '" + field + "' AND " + "PROP_VALUE = '" + value + "')"; 
	}
	
	/**
	 * @return
	 */
	public static String getCLIENT() {
		return CLIENT;
	}

	/**
	 * @return
	 */
	public static String[] getCOLUMNS() {
		return COLUMNS;
	}

	/**
	 * @return
	 */
	public static String getSYSTEMID() {
		return SYSTEMID;
	}

	/**
	 * @return
	 */
	public static String getTABLENAME() {
		return TABLENAME;
	}

	/**
	 * @return
	 */
	public String getClient() {
		return client;
	}

	/**
	 * @return
	 */
	public String getDataSource() {
		return dataSource;
	}

	/**
	 * @return
	 */
	public String getGroupName() {
		return groupName;
	}

	/**
	 * @return
	 */
	public String getGUID() {
		return getKey().toString();
	}

	/**
	 * @return
	 */
	public String getPropName() {
		return propName;
	}

	/**
	 * @return
	 */
	public String getPropValue() {
		if(propValue != null)  {
			propValue = propValue.trim();
			if(propValue.length() == 0)  {
				return null;
			}
			return propValue;
		}
		return null;
	}

	/**
	 * @return
	 */
	public String getSystemID() {
		return systemID;
	}

	/**
	 * @param string
	 */
	public void setClient(String string) {
		client = string;
	}

	/**
	 * @param string
	 */
	public void setDataSource(String string) {
		dataSource = string;
	}

	/**
	 * @param string
	 */
	public void setGroupName(String string) {
		groupName = string;
	}

	/**
	 * @param string
	 */
	public void setGUID(String string) {
		gUID = string;
	}

	/**
	 * @param string
	 */
	public void setPropName(String string) {
		propName = string;
	}

	/**
	 * @param string
	 */
	public void setPropValue(String string) {
		propValue = string;
	}

	/**
	 * @param string
	 */
	public void setSystemID(String string) {
		systemID = string;
	}

	public static class DAOReader implements RowReader {
		/* (non-Javadoc)
		 * @see com.sap.isa.core.db.jdbc.RowReader#read(java.sql.ResultSet)
		 */
		public Object read(ResultSet resultset) throws SQLException {
			KeyValDAO dao = new KeyValDAO();
			read(resultset,dao);
			return dao;
		}
		
		/**
		 * @param rs
		 * @param dao
		 */
		public void read(ResultSet resultset, KeyValDAO dao) throws SQLException {
			dao.setDataSource(resultset.getString(COLUMNS[3]));
			dao.setGroupName(resultset.getString(COLUMNS[4]));
			dao.setPropName(resultset.getString(COLUMNS[5]));
			dao.setPropValue(resultset.getString(COLUMNS[6]));
			dao.setClient(resultset.getString(COLUMNS[1]));
			dao.setSystemID(resultset.getString(COLUMNS[2]));
			dao.setKey(OIDGenerator.bytesToHexString(resultset.getBytes(COLUMNS[0])));
		}
	}

	public String toString()  {
		return "DS = " + dataSource + " GRP = " + groupName + " Pty = " + propName + " Value = " + propValue;
	}
	
	public static class Key implements Serializable {
		private String dataSource;
		private String groupName;
		private String propName;
		
		public Key() {
			
		}
		public Key(String dataSource, String groupName , String propName) {
			this.dataSource = dataSource;
			this.groupName = groupName;
			this.propName = propName;
		}
		
		/* (non-Javadoc)
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		public boolean equals(Object obj) {
			if(this == obj) return true;
			if(!(obj instanceof Key)) return false;
			if(dataSource != null && groupName != null && propName != null) {
				Key other = (Key)obj;
				if(dataSource.equals(other.dataSource) && groupName.equals(other.groupName)
								&& propName.equals(propName)) {
					return true;
				}
				else {
					return false;
				}
			}
			return super.equals(obj);
		}

		/* (non-Javadoc)
		 * @see java.lang.Object#hashCode()
		 */
		public int hashCode() {
			if(dataSource != null && groupName != null && propName != null) {
				return 37*groupName.hashCode() + dataSource.hashCode() + propName.hashCode();
			}		
			return super.hashCode();
		}

		/* (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		public String toString() {
			return dataSource + '#' + groupName + '#' + propName;
		}

	}
	/**
	 * 
	 */
	public void initializeKey() {
		this.setKey(new Key(dataSource , groupName , propName));
	}	
}
