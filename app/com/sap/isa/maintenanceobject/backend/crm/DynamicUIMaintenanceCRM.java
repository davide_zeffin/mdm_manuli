/*****************************************************************************
 Class:        PropertyMaintenance
 Copyright (c) 2008, SAP AG, Germany, All rights reserved.
 Author:       Wei-Ming Tchay
 Created:      19.02.20088
 Version:      1.0

 $Revision: #1 $
 $Date: 2008/02/19 $
 *****************************************************************************/
package com.sap.isa.maintenanceobject.backend.crm;

import java.util.Iterator;
import java.util.List;

import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.crm.MessageCRM;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.maintenanceobject.backend.MaintenanceObjectHelper;
import com.sap.isa.maintenanceobject.backend.MaintenanceObjectHelperException;
import com.sap.isa.maintenanceobject.backend.boi.AllowedValueData;
import com.sap.isa.maintenanceobject.backend.boi.DescriptionData;
import com.sap.isa.maintenanceobject.backend.boi.DescriptionListData;
import com.sap.isa.maintenanceobject.backend.boi.DynamicUIMaintenanceBackend;
import com.sap.isa.maintenanceobject.backend.boi.DynamicUIMaintenanceData;
import com.sap.isa.maintenanceobject.backend.boi.MaintenanceObjectData;
import com.sap.isa.maintenanceobject.backend.boi.PropertyData;
import com.sap.isa.maintenanceobject.backend.boi.UIElementBaseData;
import com.sap.isa.maintenanceobject.backend.boi.UIElementGroupData;
import com.sap.isa.maintenanceobject.businessobject.DynamicUI;
import com.sap.isa.maintenanceobject.businessobject.UIPage;
import com.sap.mw.jco.JCO;

/**
 * The class DynamicUIMaintenanceCRM allows to store dynamic UI object in the CRM backend.<br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class DynamicUIMaintenanceCRM extends DynamicUICRM
		implements DynamicUIMaintenanceBackend {

	
    /**
     * This method save the dynamic UI object in backend. <br>
     *
     * @param dynamicUI dynamic ui object
     * @param propertyMaintenance maintenance object which describe the properties of the Property object.
     */
    public void saveData(DynamicUI dynamicUI, MaintenanceObjectData propertyMaintenance) throws BackendException {
    	
    	JCoConnection connection = getDefaultJCoConnection();
    	
    	try {
    		// now get repository infos about the function
    		JCO.Function function = connection.getJCoFunction("COM_DYN_UI_EDITOR_SAVE");

    		// getting import parameter
    		JCO.ParameterList importParams = function.getImportParameterList();

    		importParams.setValue(dynamicUI.getTechKey().isInitial() ? "X" : "", "IV_NEW");
    		
    		if (dynamicUI.getTechKey().isInitial()) {
    			dynamicUI.setTechKey(TechKey.generateKey());
    		}

    		JCO.Record uiDetail = importParams.getStructure("IS_DYNAMIC_UI");

    		JCOTables jcoTables = new JCOTables(); 
    		
    		jcoTables.setStructureDetail(importParams.getTable("IT_GROUP_RELATION"));
    		
    		jcoTables.setPropertyDetail(importParams.getTable("IT_PROPERTIES"));
    		
    		jcoTables.setElementGroup(importParams.getTable("IT_PROP_GROUP"));
    		
    		jcoTables.setPropertyDescription(importParams.getTable("IT_PROPERTIES_TXT"));
    		
    		jcoTables.setValueDetail(importParams.getTable("IT_ALLOWED_VALUES"));
    		
    		jcoTables.setPageGroup(importParams.getTable("IT_PAGE"));
    		
    		jcoTables.setPageGroupDescription(importParams.getTable("IT_PAGE_TXT"));
    		
    		
    		fillDynamicUIStructure(uiDetail, dynamicUI);
    		
    		UIElementGroupData pages = dynamicUI.getPageGroup();
    		
			fillStructureTable(propertyMaintenance, pages, dynamicUI.getTechKey().getIdAsString(), "0", jcoTables );

            // call the function
            connection.execute(function);

            // get the output parameter
            JCO.ParameterList exportParams = function.getExportParameterList();

            // check the message table
            JCO.Table messages = exportParams.getTable("ET_MESSAGES");


            // write some useful logging information
            if (log.isDebugEnabled()) {
                 JCoHelper.logCall("COM_DYN_UI_EDITOR_SAVE", importParams, exportParams,log);
            }
            
            MessageCRM.addMessagesToBusinessObject(dynamicUI, 
        		  								 messages);

        }
        catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        } 
    	
        connection.close();
        
    }
    
    
    /**
     * Fill JCO structure for UI details from dynamic UI object. <br>
     * 
     * @param uiDetail JCO structure
     * @param dynamicUI dynamic UI object
     */
    protected void fillDynamicUIStructure(JCO.Record uiDetail, DynamicUI dynamicUI) {
    	uiDetail.setValue(dynamicUI.getTechKey().getIdAsString(), "GUID");
    	JCoHelper.setValue(uiDetail,dynamicUI.isWizardMode(), "WIZARD_MODE");
    }
        

    /**
     * <p> The method fillStructureTable fill the JCO structure table from dynamic UI object </p>
     * 
     * @param propertyMaintenance
     * @param uiElement
     * @param parentGuid
     * @param index
     * @param jcoTables
     */
    protected void fillStructureTable(MaintenanceObjectData propertyMaintenance,
    								  UIElementBaseData uiElement,
    								  String parentGuid,
    								  String index,
    								  JCOTables jcoTables) {
    	
    	JCO.Table structureDetail = jcoTables.getStructureDetail();
    	
    	structureDetail.appendRow();
    	structureDetail.setValue(uiElement.getTechKey().getIdAsString(), "GUID");
    	structureDetail.setValue(parentGuid, "GROUP_GUID");
    	structureDetail.setValue(uiElement.getTechKey().getIdAsString(), "ELEMENT_GUID");
    	String elementType = getElementType(uiElement);
    	
    	structureDetail.setValue(elementType, "ELEMENT_TYPE");
    	structureDetail.setValue(index, "ELEMENT_ORDER");
    	
    	if (uiElement instanceof UIElementGroupData) {
    		List list = ((UIElementGroupData)uiElement).getElementList();
    		if (uiElement instanceof UIPage) {
    			fillPageTable((UIPage)uiElement,jcoTables.getPageGroup(),jcoTables.getPageGroupDescription());
    		}
    		else {
    			fillGroupTable((UIElementGroupData)uiElement,jcoTables.getElementGroup());
    		}
    		for (int i = 0; i < list.size(); i++) {
    			fillStructureTable(propertyMaintenance,(UIElementBaseData)list.get(i), uiElement.getTechKey().getIdAsString(), "" + i, jcoTables );
			}
    		
    	}
    	if (uiElement instanceof PropertyData) {
			fillPropertyTable(propertyMaintenance,(PropertyData)uiElement, jcoTables);
    		
    	}	
    }


    /**
	 * Determines the ABAP identifier for different UI Element Types. <br>
	 * 
	 * @param uiElement ui element
	 * @return id definig element type.
	 */
	protected String getElementType(UIElementBaseData uiElement) {
		String elementType = "";
    	if (uiElement instanceof UIPage) {
    		elementType = "1";
    	}
    	else if (uiElement instanceof UIElementGroupData) {
    		elementType = "3";
    	}
    	else if (uiElement instanceof PropertyData) {
    		elementType = "4";
    	}
		return elementType;
	}

    /**
     * <p> The method fillPropertyTable fill the JCO table with property information.</p>
     * 
     * @param propertyMaintenance
     * @param uiElement
     * @param jcoTables
     */
    protected void fillPropertyTable(MaintenanceObjectData propertyMaintenance,
    								 PropertyData uiElement,
    								 JCOTables jcoTables) {
    	
    	JCO.Table propertyDetail = jcoTables.getPropertyDetail(); 
		
    	propertyDetail.appendRow();
    	propertyDetail.setValue(uiElement.getTechKey().getIdAsString(), "GUID");
    	propertyDetail.setValue(uiElement.getClass().getName(), "CLASS_NAME");
    	propertyDetail.setValue(uiElement.getName(), "NAME");
    	propertyDetail.setValue(uiElement.getUiType(), "UI_TYPE");
    	propertyDetail.setValue(uiElement.getType(), "TYPE");
    	
    	propertyDetail.setValue(uiElement.getDescription(), "DESCRIPTION");
    	JCoHelper.setValue(propertyDetail,uiElement.isRessourceKeyUsed(), "IS_RES_KEY_USED");
    	
    	fillPropertyTableExit(uiElement,propertyDetail);

    	propertyMaintenance.synchronizeProperties(uiElement);
    	MaintenanceObjectHelper.fillJCORecordFromObject(propertyMaintenance, null,propertyDetail);
    	
    	JCO.Table structureDetail = jcoTables.getStructureDetail();
    	
    	Iterator iter = ((PropertyData)uiElement).iterator();
    	int index = 0;
    	while (iter.hasNext()) {
			AllowedValueData value = (AllowedValueData) iter.next();
			
			// generate key for structure table
			TechKey key = TechKey.generateKey();
	    	
	    	structureDetail.appendRow();
	    	structureDetail.setValue(key.getIdAsString(), "GUID");
	    	structureDetail.setValue(uiElement.getTechKey().getIdAsString(), "GROUP_GUID");
	    	structureDetail.setValue(key.getIdAsString(), "ELEMENT_GUID");
	    	
	    	structureDetail.setValue("5", "ELEMENT_TYPE");
	    	structureDetail.setValue(""+index, "ELEMENT_ORDER");
	    	index++;
	    	fillAllowedValueTable(value,key,uiElement.getTechKey(),jcoTables);
		}

    }

    /**
	 * User exit to enhance JCO Property Table.<br>
	 * 
	 * @param property propterty object
	 * @param propertyDetail JCO structure with property data
	 */
	protected void fillPropertyTableExit(PropertyData property, JCO.Table propertyDetail) {
	}

    
    /**
     * <p> The method fillPropertyTable fill the JCO table with AllowedValueData information </p>
     * 
     * @param value allowed value
     * @param key Key of the value
     * @param propertyKey key of the property
     * @param jcoTables JCO Tables
     */
    protected void fillAllowedValueTable(AllowedValueData value,
    		                             TechKey key,
    		                             TechKey propertyKey,
    								     JCOTables jcoTables) {
    	
    	JCO.Table valueDetail = jcoTables.getValueDetail(); 
		
    	valueDetail.appendRow();
    	valueDetail.setValue(key.getIdAsString(), "GUID");
    	valueDetail.setValue(propertyKey.getIdAsString(), "PROPERTY_GUID");
    	valueDetail.setValue(value.getClass().getName(), "CLASS_NAME");
		valueDetail.setValue(value.getValue(), "VALUE");
    	valueDetail.setValue(value.getDescription(), "DESCRIPTION");
    	JCoHelper.setValue(valueDetail,value.isRessourceKeyUsed(), "IS_RES_KEY_USED");

    	fillAllowedValueTableExit(value, valueDetail);
    }


	/**
     * User exit to enhance JCO Allowed Value structure.<br>
	 * 
	 * @param value AllowedValue object 
	 * @param valueDetail JCO struturce with AllowedValue data
	 */
	protected void fillAllowedValueTableExit(AllowedValueData value, JCO.Table valueDetail) {
	}

    
    /**
     * <p> The method fillPageTable fill the JCO table with page information. </p>
     * 
     * @param page ui page
     * @param pageTable JCO page table
     * @param pageDescriptionTable JCO page description table
     */
    protected void fillPageTable(UIPage page,
			                     JCO.Table pageTable,
			                     JCO.Table pageDescriptionTable) {

    	pageTable.appendRow();
    	pageTable.setValue(page.getTechKey().getIdAsString(), "GUID");
    	pageTable.setValue(page.getName(), "NAME");
    	JCoHelper.setValue(pageTable,page.isHidden(),"IS_HIDDEN");
    	pageTable.setValue(page.getClass().getName(), "CLASS_NAME");    	
    	pageTable.setValue(page.getDescription(), "DESCRIPTION");
    	pageTable.setValue(page.getImageURL(), "IMAGE_URL");
    	pageTable.setValue(page.getImageHeight(), "IMAGE_HEIGHT");
    	pageTable.setValue(page.getImageWidth(), "IMAGE_WIDTH");
		JCoHelper.setValue(pageTable,page.isRessourceKeyUsed(), "IS_RES_KEY_USED");
    	
		DescriptionListData descriptionList = page.getDescriptionList();
		Iterator iter = descriptionList.iterator();
		while (iter.hasNext()) {
			DescriptionData description = (DescriptionData) iter.next();
			pageDescriptionTable.appendRow();
			pageDescriptionTable.setValue(page.getTechKey().getIdAsString(), "PAGE_GUID");
			pageDescriptionTable.setValue(description.getDescription(), "DESCRIPTION");
			pageDescriptionTable.setValue(description.getLanguage(), "LANGU");
		}

    	
     }	


    /**
     * <p> The method fillGroupTable fill the JCO table with ui element group information.  </p>
     * 
     * @param group group object
     * @param groupTable JCO UI element table
     */
    protected void fillGroupTable(UIElementGroupData group,
                                  JCO.Table groupTable) {

		groupTable.appendRow();
		groupTable.setValue(group.getTechKey().getIdAsString(), "GUID");
		groupTable.setValue(group.getName(), "NAME");
		JCoHelper.setValue(groupTable,group.isHidden(),"IS_HIDDEN");
		groupTable.setValue(group.getClass().getName(), "CLASS_NAME");
		groupTable.setValue(group.getDescription(), "DESCRIPTION");
		JCoHelper.setValue(groupTable,group.isRessourceKeyUsed(), "IS_RES_KEY_USED");
		
    }	


    /**
     * Initialized the needed {@link com.sap.isa.maintenanceobject.businessobject.PropertyMaintenance} and 
     * {@link com.sap.isa.maintenanceobject.businessobject.PageMaintenance}. <br>
     * 
     * @param maintenanceObject property maintenance instance.
     * @throws BackendException
     */

	public void initMaintenanceObjects(DynamicUIMaintenanceData maintenaceData) 
			throws BackendException {

        try {
            String fileName = initProperties.getProperty(PROPERTY_CONFIG_FILE);

            if (fileName != null && fileName.length()>0 ) {
            	objectHelper.initObjectFromFile(maintenaceData.getPropertyMaintenance(), fileName, true);
            }

            fileName = initProperties.getProperty(PAGE_CONFIG_FILE);

            if (fileName != null && fileName.length()>0 ) {
            	objectHelper.initObjectFromFile(maintenaceData.getPageMaintenance(), fileName, true);
            }
		} catch (MaintenanceObjectHelperException e) {
			throw new BackendException("Could not initialize the property maintenance object");
		}
	}


}
