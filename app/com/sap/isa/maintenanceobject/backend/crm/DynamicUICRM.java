/*****************************************************************************
 Class:        DynamicUICRM
 Copyright (c) 2008, SAP AG, Germany, All rights reserved.
 Author:       SAP
 Created:      19.02.2008
 Version:      1.0

 *****************************************************************************/
package com.sap.isa.maintenanceobject.backend.crm;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.SortedMap;
import java.util.TreeMap;

import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.crm.MessageCRM;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.BackendBusinessObjectBaseSAP;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.maintenanceobject.backend.MaintenanceObjectHelper;
import com.sap.isa.maintenanceobject.backend.MaintenanceObjectHelperException;
import com.sap.isa.maintenanceobject.backend.boi.AllowedValueData;
import com.sap.isa.maintenanceobject.backend.boi.DynamicUIBackend;
import com.sap.isa.maintenanceobject.backend.boi.MaintenanceObjectData;
import com.sap.isa.maintenanceobject.backend.boi.PropertyData;
import com.sap.isa.maintenanceobject.backend.boi.UIElementBaseData;
import com.sap.isa.maintenanceobject.backend.boi.UIElementGroupData;
import com.sap.isa.maintenanceobject.businessobject.Description;
import com.sap.isa.maintenanceobject.businessobject.DynamicUI;
import com.sap.isa.maintenanceobject.businessobject.MaintenanceObject;
import com.sap.isa.maintenanceobject.businessobject.PropertyMaintenance;
import com.sap.isa.maintenanceobject.businessobject.UIPage;
import com.sap.mw.jco.JCO;

/**
 * <p>The class DynamicUICRM read the data of a dynamic ui object from the backend. </p>
 * <p>The function module <code>COM_DYN_UI_EDITOR_GET</code> provide this functionality within the CRM backend. </p>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class DynamicUICRM extends BackendBusinessObjectBaseSAP
		implements DynamicUIBackend {

    /**
     * global constant for the Paramter defined in the eai-config file, which
     * describes the filename of the config file, which contains the properties.
     *
     */
    public static String PROPERTY_CONFIG_FILE="Property-File";

    /**
     * global constant for the Paramter defined in the eai-config file, which
     * describes the filename of the config file, which contains the properties.
     *
     */
    public static String PAGE_CONFIG_FILE="Page-File";
    
	/**
	 * Reference to a <code>MaintenanceObjectHelper</code> object, which will
	 * handle all backend independent stuff, like reading the configuration,
	 * generic reading and writing of properties.
	 * 
	 * @see com.sap.isa.shopadmin.backend.MaintenanceObjectHelper
	 */
	protected MaintenanceObjectHelper objectHelper;
	
	protected MaintenanceObject propertyMaintenance = null;

	protected Properties initProperties;

	protected static IsaLocation log = IsaLocation.getInstance(
			DynamicUICRM.class.getName());
	
	
	/**
	 * <p>Initializes the backend object.</p>
     * <p>The {@link #objectHelper} instance is created. 
     * The {@link #objectHelper} is used to create a maintenance object, which decribes the properties of the  
     * Dynamic UI Property objects.</p>
	 * 
	 * @param props a set of properties which may be useful to initialize the
	 *            object
	 * @param params a object which wraps parameters
	 */
	public void initBackendObject(Properties props,
			BackendBusinessObjectParams params) throws BackendException {

		initProperties = (Properties) props.clone();

		try {
			/* create the object helper */
			objectHelper = new MaintenanceObjectHelper(getConnectionFactory(),
					initProperties);

		} catch (Exception ex) {
			throw new BackendException(
					"Could not initialize the backend object");
		}
	}
	
	/**
     * <p>This method read the values of the maintenance object from the backend. </p>
     *
     * @param dynamicUI dynamic UI instance
     */
    public void readData(DynamicUI dynamicUI) throws BackendException {
    	
    	
    	if (propertyMaintenance == null) {
    		propertyMaintenance = new PropertyMaintenance();
    		initPropertyMaintenance(propertyMaintenance);
    	}
    	    	
    	JCoConnection connection = getDefaultJCoConnection();
    	
    	try {
    		// now get repository infos about the function
    		JCO.Function function = connection.getJCoFunction("COM_DYN_UI_EDITOR_GET");

    		// getting import parameter
    		JCO.ParameterList importParams = function.getImportParameterList();
    		
    		importParams.setValue(dynamicUI.getTechKey().getIdAsString(),"IV_DYNAMIC_UI_GUID");

            // call the function
            connection.execute(function);

    		JCO.ParameterList exportParams = function.getExportParameterList();

    		JCOTables jcoTables = getJCOTablesForRead(function);

            String returnCode = exportParams.getString("EV_RETURNCODE");
            if (returnCode.length() == 0) {
            	JCO.Record uiDetail = exportParams.getStructure("ES_DYNAMIC_UI");            	
            	readUiDetails(dynamicUI, uiDetail);
            	
            	Map elementMap = new HashMap();
            	buildObject(dynamicUI, propertyMaintenance, elementMap, jcoTables);
            }	
    		
            // check the message table
            JCO.Table messages = exportParams.getTable("ET_MESSAGES");


            // write some useful logging information
            if (log.isDebugEnabled()) {
                 JCoHelper.logCall("COM_DYN_UI_EDITOR_GET", importParams, exportParams,log);
            }
            
            MessageCRM.addMessagesToBusinessObject(dynamicUI, 
        		  								 messages);

        }
        catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        } 
    	
        connection.close();
        
    }

	/**
	 * Update the dynamic UI object with data from <code>uiDetail</code> structure. <br>
	 * 
	 * @param dynamicUI dynamic UI instance
	 * @param uiDetail ABAP structure with detailed data
	 */
	protected void readUiDetails(DynamicUI dynamicUI, JCO.Record uiDetail) {
		dynamicUI.setWizardMode(JCoHelper.getBoolean(uiDetail, "WIZARD_MODE"));
	}

    
	/**
	 * <p> The method getJCOTablesForRead fill the JCOTable from the read function module. </p>
	 * <p> You can overwrite this method, if you have addiational tables beside the standard one.</p>
	 * 
	 * @param function JCO Function
	 * @return filled JCOTable object.
	 */
	protected JCOTables getJCOTablesForRead(JCO.Function function) {
		
		JCOTables jcoTables = new JCOTables(); 
		
		JCO.ParameterList exportParams = function.getExportParameterList();
		
		jcoTables.setStructureDetail(exportParams.getTable("ET_GROUP_RELATION"));
		
		jcoTables.setPropertyDetail(exportParams.getTable("ET_PROPERTIES"));
		
		jcoTables.setElementGroup(exportParams.getTable("ET_PROP_GROUP"));
		
		jcoTables.setPropertyDescription(exportParams.getTable("ET_PROPERTIES_TXT"));
		
		jcoTables.setValueDetail(exportParams.getTable("ET_ALLOWED_VALUES"));
		
		jcoTables.setPageGroup(exportParams.getTable("ET_PAGE"));
		
		jcoTables.setPageGroupDescription(exportParams.getTable("ET_PAGE_TXT"));
		
		return jcoTables;
	}

   
    

    /**
     * <p> The method buildStructure builds the dynamicUI object with the given 
     * JCO tables. </p>
     * 
     * @param propertyMaintenance maintenance object with information about the property structure
     * @param dynamicUI dynamic UI instance
     * @param elementMap map to find UI elements
     * @param jcoTables JCO tables with content from the backend.
     * @throws BackendException 
     */
    protected void buildObject(DynamicUI dynamicUI,
    						   MaintenanceObjectData propertyMaintenance,
    						   Map elementMap,
    						   JCOTables jcoTables) throws BackendException {

    	// Read Page Table
    	readPageTable(elementMap, jcoTables);
    	
    	readPageDescriptionTable(elementMap, jcoTables); 

        // handle all other groups    	
        readElementGroupTable(elementMap, jcoTables);

        // handle all other properties    	
        readPropertyTable(propertyMaintenance, elementMap, jcoTables);
        
        // Handle allowed values
        readValueTable(elementMap,jcoTables);
        
    	Map groupMap = readStructureTable(dynamicUI, jcoTables);
        
        dynamicUI.deleteAllPages();
        UIElementGroupData pageGroup = dynamicUI.getPageGroup();
        buildStructure(dynamicUI, pageGroup, groupMap, elementMap);
        
        buildObjectExit(dynamicUI, groupMap, elementMap, jcoTables);
    }

	/**
	 * Read the JCO structure tables to build up the hierarchie of the object. See {@link #buildStructure(DynamicUI, UIElementBaseData, Map, Map)}
     * for details <br>
	 * 
	 * @param dynamicUI dynamic UI instance 
	 * @param jcoTables JCO structure tables
	 * @return map with all UI elements
	 */
	protected Map readStructureTable(DynamicUI dynamicUI, JCOTables jcoTables) {
		int numValues;
		JCO.Table structureDetail = jcoTables.getStructureDetail();
    	
    	numValues = structureDetail.getNumRows();
    	
    	// the group will consist of sorted map with the order within the group.
    	Map groupMap = new HashMap();
    	String uiKey = dynamicUI.getTechKey().getIdAsString();    	

        for (int i = 0; i < numValues; i++) {
        	
            String groupKey = structureDetail.getString("GROUP_GUID");
            String elementKey = structureDetail.getString("ELEMENT_GUID");
            int elementOrder =  structureDetail.getInt("ELEMENT_ORDER");
            
            // find the key of the page group!
            if (groupKey.equals(uiKey)) {
            	dynamicUI.getPageGroup().setTechKey(new TechKey(elementKey));
            }
            
            // search for the sorted map of the group
            SortedMap elementOrderMap = (SortedMap)groupMap.get(groupKey);
            if (elementOrderMap == null) {
            	elementOrderMap = new TreeMap();
            	groupMap.put(groupKey, elementOrderMap);
            }
            
            // add the found element in the sorted map.
            elementOrderMap.put(new Integer(elementOrder),elementKey);

            structureDetail.nextRow();
        }
		return groupMap;
	}

	/**
	 * Read the property data from the backend. <br>
	 * 
	 * @param propertyMaintenance
	 * @param elementMap
	 * @param jcoTables
	 * @throws BackendException
	 */
	protected void readPropertyTable(MaintenanceObjectData propertyMaintenance, Map elementMap, JCOTables jcoTables) throws BackendException {

		int numValues;
		JCO.Table propertyDetail = jcoTables.getPropertyDetail();
        numValues = propertyDetail.getNumRows();

        for (int i = 0; i < numValues; i++) {
        	
            String className = propertyDetail.getString("CLASS_NAME");
            UIElementBaseData element = createUIElement(className);
            
            if (element != null) {
	            if (!(element instanceof PropertyData)) {
	            	throw new BackendException("inconsist data on database: wrong class name for Property Object");
	            }

	            PropertyData property = (PropertyData)element;
            	String guid = propertyDetail.getString("GUID");
            	property.setTechKey(new TechKey(guid));
            	property.setName(propertyDetail.getString("NAME"));
            	property.setUiType(propertyDetail.getString("UI_TYPE"));
            	property.setDescription(propertyDetail.getString("DESCRIPTION"));
            	property.setRessourceKeyUsed(JCoHelper.getBoolean(propertyDetail, "IS_RES_KEY_USED"));
            	
            	if (propertyDetail.getString("TYPE").length() > 0) {
            		property.setType(propertyDetail.getString("TYPE"));
            	}	
            	
            	MaintenanceObjectHelper.fillObjectFromJCORecord(propertyMaintenance, propertyDetail);
            	
            	propertyMaintenance.synchronizeBeanProperties(property);
            	
            	buildPropertyExit(propertyDetail,property);
            	elementMap.put(guid,property);
            }	

            propertyDetail.nextRow();
        }
	}

	/**
	 * User Exit to influence the creation of the property to handle additional obejct specific fields. <br>
	 * 
	 * @param propertyDetail JCO Table
	 * @param property property object
	 */
	protected void buildPropertyExit(JCO.Table propertyDetail, PropertyData property) {
	}
	
	
	/**
	 * Analyse the value table provided from the backend. <br>
	 * 
	 * @param propertyMaintenance
	 * @param elementMap
	 * @param jcoTables
	 * @throws BackendException
	 */
	protected void readValueTable(Map elementMap, JCOTables jcoTables) throws BackendException {

		int numValues;
		JCO.Table valueDetail = jcoTables.getValueDetail();
        numValues = valueDetail.getNumRows();

        for (int i = 0; i < numValues; i++) {
        	
            String className = valueDetail.getString("CLASS_NAME");
            AllowedValueData value = createValue(className);
            
            if (value != null) {
            	buildValueExit(valueDetail, value);
            	String guid = valueDetail.getString("GUID");
        		value.setValue(valueDetail.getString("VALUE"));
        		value.setDescription(valueDetail.getString("DESCRIPTION"));
        		value.setRessourceKeyUsed(JCoHelper.getBoolean(valueDetail, "IS_RES_KEY_USED"));
        		
            	elementMap.put(guid,value);
            }	

            valueDetail.nextRow();
        }
	}

	/**
     * User Exit to influence the creation of the value to handle additional obejct specific fields. <br>
	 * 
	 * @param valueDetail JCO Table with value data
	 * @param value value object
	 */
	protected void buildValueExit(JCO.Table valueDetail, AllowedValueData value) {
	}
	
	
	/**
	 * Analyse the UI element group table. <br>
	 * 
	 * @param element Map map with UI elements
	 * @param jcoTables JCO Tables with CRM Data
	 */
	protected void readElementGroupTable(Map elementMap, JCOTables jcoTables) {
		int numValues;
		JCO.Table elementGroupTable = jcoTables.getElementGroup();
        numValues = elementGroupTable.getNumRows();

        for (int i = 0; i < numValues; i++) {
        	
            String className = elementGroupTable.getString("CLASS_NAME");
            
            UIElementBaseData element = createUIElement(className);

            if (element != null) {
            	String guid = elementGroupTable.getString("GUID");
            	element.setTechKey(new TechKey(guid));
            	element.setName(elementGroupTable.getString("NAME"));
            	element.setDescription(elementGroupTable.getString("DESCRIPTION"));
            	element.setHidden(JCoHelper.getBoolean("IS_HIDDEN"));
            	buildGroupExit(elementGroupTable, element);
            	elementMap.put(guid,element);
            }	

            elementGroupTable.nextRow();
        }
	}

	/**
     * User Exit to influence the creation of the element groups to handle additional obejct specific fields. <br>
	 * 
	 * @param elementGroupTable JCO Table with group data
	 * @param group ui element group object 
	 */
	protected void buildGroupExit(JCO.Table elementGroupTable, UIElementBaseData group) {
	}
	

	/**
	 * Analyse the JCO page table . <br>
	 * 
	 * @param elementMap map with ui elements
	 * @param jcoTables JCO tables with backend data
	 */
	protected void readPageTable(Map elementMap, JCOTables jcoTables) {
		JCO.Table pages = jcoTables.getPageGroup();
    	
    	int numValues = pages.getNumRows();

        for (int i = 0; i < numValues; i++) {
        	
            String className = pages.getString("CLASS_NAME");
            UIPage element = (UIPage)createUIElement(className);

            if (element != null) {
            	String guid = pages.getString("GUID");
            	element.setTechKey(new TechKey(guid));
            	element.setDescription(pages.getString("DESCRIPTION"));
            	element.setHidden(JCoHelper.getBoolean(pages,"IS_HIDDEN"));
            	element.setRessourceKeyUsed(JCoHelper.getBoolean(pages,"IS_RES_KEY_USED"));
            	element.setImageURL(pages.getString("IMAGE_URL"));
            	element.setImageHeight(pages.getString("IMAGE_HEIGHT"));
            	element.setImageWidth(pages.getString("IMAGE_WIDTH"));
            	element.setName(pages.getString("NAME"));
            	buildPageExit(pages,element);
            	elementMap.put(guid,element);
            }	
            pages.nextRow();
        }
	}
	

	
    /**
     * Analyse the JCO page description table . <br>
     * 
     * @param elementMap map with ui elements
     * @param jcoTables JCO tables with backend data
     */
    protected void readPageDescriptionTable(Map elementMap,JCOTables jcoTables) {

    	JCO.Table pageDescription = jcoTables.getPageGroupDescription();
    	
    	int numValues = pageDescription.getNumRows();

    	for (int i = 0; i < numValues; i++) {

    		String guid = pageDescription.getString("PAGE_GUID");

    		UIPage page = (UIPage)elementMap.get(guid);

    		if (page != null) {
    			Description description = new Description();
    			description.setDescription(pageDescription.getString("DESCRIPTION"));
    			description.setLanguage(pageDescription.getString("LANGU"));
    			page.getDescriptionList().addDescription(description);
    		}	
    		pageDescription.nextRow();
    	}
    }
	
	

    /**
     * User Exit to influence the creation of the page to handle additional obejct specific fields. <br>
     * 
     * @param pageTable JCO Table with page data
     * @param page page object
     */
	protected void buildPageExit(JCO.Table pageTable, UIElementBaseData page) {
	}
	

    /**
     * <p> The method buildStructure created the structure of the dynamicUI object with the given 
     * JCO tables. </p>
     * 
     * @param dynamicUI dynamic UI instance
     * @param parentElement parent elemet of the current node.
     * @param groupMap map with group relations
     * @param elementMap map to find UI elements
     */
    protected void buildStructure(DynamicUI dynamicUI, UIElementBaseData parentElement, Map groupMap, Map elementMap) {
    	
    	String groupKey = parentElement.getTechKey().getIdAsString();
    	SortedMap elementOrderMap = (SortedMap)groupMap.get(groupKey);
    	if (elementOrderMap != null) {
    		Iterator iter = elementOrderMap.values().iterator();
    		while (iter.hasNext()) {
				String elementKey = (String) iter.next();
				Object object = elementMap.get(elementKey);
				if (object != null) {
					if (object instanceof UIElementBaseData) {
						UIElementBaseData element = (UIElementBaseData)object;
						if (parentElement instanceof UIElementGroupData) {
							dynamicUI.addElement(element,(UIElementGroupData)parentElement);	
						}	
						buildStructure(dynamicUI,element,groupMap,elementMap);
					}
					else if (object instanceof AllowedValueData) {
						if (parentElement instanceof PropertyData) {
							((PropertyData)parentElement).addAllowedValue((AllowedValueData)object);	
						}
					}
				}	
			}
    	}
    }
        

    /**
     * User exist to control the creation of the object <br>
     * 
     * @param dynamicUI dynamic UI instance
     * @param groupMap map with group relations
     * @param elementMap map to find UI elements
     * @param jcoTables JCO table with backend data
     */
    protected void buildObjectExit(DynamicUI dynamicUI,Map groupMap, Map elementMap, JCOTables jcoTables) {
    	
    }
  
	/**
	 * Creates a UI Element for the given classname <br>
	 * 
	 * @param className name of the UI element
	 * @return instance of UI element 
	 */
	protected UIElementBaseData createUIElement(String className) {
		
		UIElementBaseData instance = null;
		try {
			Class foundClass = Class.forName(className);
			instance = (UIElementBaseData)foundClass.newInstance();
			
		} catch (ClassNotFoundException e) {
			log.error(e);
		} catch (InstantiationException e) {
			log.error(e);
		} catch (IllegalAccessException e) {
			log.error(e);
		}
		return instance;
	}
    
    /**
     * Creates a "allowed value" for the given classname <br>
     * 
     * @param className name of the "allowed value"
     * @return instance of "allowed value" 
     */
	protected AllowedValueData createValue(String className) {
		
		AllowedValueData instance = null;
		try {
			Class foundClass = Class.forName(className);
			instance = (AllowedValueData)foundClass.newInstance();
			
		} catch (ClassNotFoundException e) {
			log.error(e);
		} catch (InstantiationException e) {
			log.error(e);
		} catch (IllegalAccessException e) {
			log.error(e);
		}
		return instance;
	}
   
    
	/**
	 * Initialized the needed {@link com.sap.isa.maintenanceobject.businessobject.PropertyMaintenance}. <br>
	 * 
	 * @param maintenanceObject property maintenance instance.
	 * @throws BackendException
	 */
	public void initPropertyMaintenance(MaintenanceObjectData maintenanceObject) throws BackendException {

        try {
            String fileName = initProperties.getProperty(PROPERTY_CONFIG_FILE);

            if (fileName != null && fileName.length()>0 ) {
            	objectHelper.initObjectFromFile(maintenanceObject, fileName, true);
            }
		} catch (MaintenanceObjectHelperException e) {
			throw new BackendException("Could not initialize the property maintenance object");
		}
	}


	/**
	 * The class JCOTables structured the access to all used JCO tables . <br>
	 *
	 * @author  SAP AG
	 * @version 1.0
	 */
	public class JCOTables {
		
   		protected JCO.Table structureDetail;
		
   		protected JCO.Table propertyDetail;

   		protected JCO.Table pageGroup;

   		protected JCO.Table pageGroupDescription;

   		protected JCO.Table elementGroup;
		
   		protected JCO.Table propertyDescription;
		
   		protected JCO.Table valueDetail;

		/**
		 * <p>Return the property {@link #elementGroup}. </p> 
		 *
		 * @return Returns the {@link #elementGroup}.
		 */
		public JCO.Table getElementGroup() {
			return elementGroup;
		}

		/**
		 * <p>Set the property {@link #elementGroup}. </p>
		 * 
		 * @param elementGroup The {@link #elementGroup} to set.
		 */
		public void setElementGroup(JCO.Table elementGroup) {
			this.elementGroup = elementGroup;
		}

		/**
		 * <p>Return the property {@link #pageGroup}. </p> 
		 *
		 * @return Returns the {@link #pageGroup}.
		 */
		public JCO.Table getPageGroup() {
			return pageGroup;
		}

		/**
		 * <p>Set the property {@link #pageGroup}. </p>
		 * 
		 * @param pageGroup The {@link #pageGroup} to set.
		 */
		public void setPageGroup(JCO.Table pageGroup) {
			this.pageGroup = pageGroup;
		}

		/**
		 * <p>Return the property {@link #pageGroupDescription}. </p> 
		 *
		 * @return Returns the {@link #pageGroupDescription}.
		 */
		public JCO.Table getPageGroupDescription() {
			return pageGroupDescription;
		}

		/**
		 * <p>Set the property {@link #pageGroupDescription}. </p>
		 * 
		 * @param pageGroupDescription The {@link #pageGroupDescription} to set.
		 */
		public void setPageGroupDescription(JCO.Table pageGroupDescription) {
			this.pageGroupDescription = pageGroupDescription;
		}

		/**
		 * <p>Return the property {@link #propertyDescription}. </p> 
		 *
		 * @return Returns the {@link #propertyDescription}.
		 */
		public JCO.Table getPropertyDescription() {
			return propertyDescription;
		}

		/**
		 * <p>Set the property {@link #propertyDescription}. </p>
		 * 
		 * @param propertyDescription The {@link #propertyDescription} to set.
		 */
		public void setPropertyDescription(JCO.Table propertyDescription) {
			this.propertyDescription = propertyDescription;
		}

		/**
		 * <p>Return the property {@link #propertyDetail}. </p> 
		 *
		 * @return Returns the {@link #propertyDetail}.
		 */
		public JCO.Table getPropertyDetail() {
			return propertyDetail;
		}

		/**
		 * <p>Set the property {@link #propertyDetail}. </p>
		 * 
		 * @param propertyDetail The {@link #propertyDetail} to set.
		 */
		public void setPropertyDetail(JCO.Table propertyDetail) {
			this.propertyDetail = propertyDetail;
		}

		/**
		 * <p>Return the property {@link #structureDetail}. </p> 
		 *
		 * @return Returns the {@link #structureDetail}.
		 */
		public JCO.Table getStructureDetail() {
			return structureDetail;
		}

		/**
		 * <p>Set the property {@link #structureDetail}. </p>
		 * 
		 * @param structureDetail The {@link #structureDetail} to set.
		 */
		public void setStructureDetail(JCO.Table structureDetail) {
			this.structureDetail = structureDetail;
		}

		/**
		 * <p>Return the property {@link #valueDetail}. </p> 
		 *
		 * @return Returns the {@link #valueDetail}.
		 */
		public JCO.Table getValueDetail() {
			return valueDetail;
		}

		/**
		 * <p>Set the property {@link #valueDetail}. </p>
		 * 
		 * @param valueDetail The {@link #valueDetail} to set.
		 */
		public void setValueDetail(JCO.Table valueDetail) {
			this.valueDetail = valueDetail;
		}

		
	}
	
	
}
