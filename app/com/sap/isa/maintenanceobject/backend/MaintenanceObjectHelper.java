/*****************************************************************************
    Class:        MaintenanceObjectHelper
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP AG
    Created:      February 2002
    Version:      1.0

    $Revision: #7 $
    $Date: 2001/07/25 $
*****************************************************************************/
package com.sap.isa.maintenanceobject.backend;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.digester.Digester;

import com.sap.isa.backend.JCoHelper;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.cache.Cache;
import com.sap.isa.core.eai.ConnectionFactory;
import com.sap.isa.core.init.InitializationHandler;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.GenericCacheKey;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.core.xcm.ConfigContainer;
import com.sap.isa.maintenanceobject.backend.boi.AllowedValueData;
import com.sap.isa.maintenanceobject.backend.boi.BackendElementKeyHolder;
import com.sap.isa.maintenanceobject.backend.boi.BackendPropertiesData;
import com.sap.isa.maintenanceobject.backend.boi.MaintenanceObjectData;
import com.sap.isa.maintenanceobject.backend.boi.PropertyData;
import com.sap.isa.maintenanceobject.backend.boi.PropertyGroupData;
import com.sap.isa.maintenanceobject.backend.boi.ScreenGroupData;
import com.sap.isa.maintenanceobject.backend.boi.VisibilityConditionData;
import com.sap.isa.maintenanceobject.businessobject.BackendProperties;
import com.sap.mw.jco.JCO;


/**
 * The MaintenanceObjectHelper offers backend independ functionality to support
 * a maintenance object. <br>
 * 
 * The method {@link #initObjectFromFile} builds a maintenance object
 * from a config file or an input stream. 
 * The name of the config file must be defined in the eai-config file as a parameter
 * with the name <code>PROPERTY_CONFIG_FILE</code>.<br>
 * <b>Example</b>
 * <pre>
 *
 * &lt;businessObject type="shopAdmin" name="shopAdmin" className="com.sap.isa.shopadmin.backend.crm.ShopAdminCRM" connectionFactoryName="JCO" defaultConnectionName="ISAStateful"&gt;
 *     &lt;params&gt;
 *                 &lt;!-- the internet user is assigned to a business partner with the following role --&gt;
 *                 &lt;param name="Property-File" value="WEB-INF\cfg\ShopCRM.xml"/&gt;
 *     &lt;/params&gt;
 * &lt;/businessObject&gt;
 *
 * </pre>
 * 
 * Secondly the class is a factory and a decorator for the
 * <code>MaintenanceObjectDataHandler</code>.
 * The Method {@link #createDataHandler} creates a suitable instance of the 
 * {@link MaintenanceObjectDataHandler}.
 * You can also use directly the corresponding constructor of the class.
 * Therefore the backend classes, which use the helper class doesn't need to hold
 * a reference of the data handler itself. <br>
 * 
 * <b>Example</b>
 * <pre>
 * 
 *   public void initBackendObject(Properties props, 
 *                                 BackendBusinessObjectParams params)
 *                          throws BackendException {
 *
 *       try {
 *           // create the object helper
 *           objectHelper = new MaintenanceObjectHelper(getConnectionFactory(), 
 *                                                      props);
 *       }
 *       catch (Exception ex) {
 *           throw new BackendException(
 *                   "Could not initialize the backend object");
 *       }
 *   }
 *
 * </pre>
 * 
 * Last but not least the class could be used as factory for maintenance object.
 * This is possible, if the information for the maintenance object is stored in
 * a XCM file, which is handle with a XCM file alias.
 *
 * @author SAP AG
 * @version 1.0
 *
 * @see com.sap.isa.maintenanceobject.backend.MaintenanceObjectDataHandler
 *
 */
public class MaintenanceObjectHelper {


    /**
     * reference to the IsaLocation
     *
     * @see com.sap.isa.core.logging.IsaLocation
     */
    private static final IsaLocation log = IsaLocation.
          getInstance(MaintenanceObjectHelper.class.getName());


    /**
     * global constant for the Paramter defined in the eai-config file, which
     * describes the filename of the config file, which contains the properties.
     *
     */
    public static String PROPERTY_CONFIG_FILE="Property-File";

    /**
     * global constant for the Paramter defined in the eai-config file, which
     * describes the class which is to use as data handles
     *
     * @see com.sap.isa.maintenanceobject.backend.MaintenanceObjectDataHandler
     *
     */
    public static String PROPERTY_DATA_HANDLER="Data-Handler";
    
    /**
     * Persistency database handler
     */
    public static String DATABASE_DATA_HANDLER = "DB-Data-Handler";


	/** 
	 * Name of the class to use for property-groups
	 * 
	 */
	protected String PROPERTY_GROUP_CLASS_NAME = "com.sap.isa.maintenanceobject.businessobject.PropertyGroup";


	/** 
	 * Name of the class to use for screen-groups
	 * 
	 */
	protected String SCREEN_GROUP_CLASS_NAME = "com.sap.isa.maintenanceobject.businessobject.ScreenGroup";


	/** 
	 * Name of the class to use for properties
	 * 
	 */
	protected String PROPERTY_CLASS_NAME = "com.sap.isa.maintenanceobject.businessobject.Property";

	
	/** 
	 * Name of the class to use for allowed values
	 * 
	 */
	protected String ALLOWED_VALUE_CLASS_NAME = "com.sap.isa.maintenanceobject.businessobject.AllowedValue";

	
	/** 
	 * Name of the class to use for allowed values
	 * 
	 */
	protected String BACKEND_PROPERTIES_CLASS_NAME = "com.sap.isa.maintenanceobject.businessobject.BackendProperties";


	/** 
	 * Name of the class to use for visibility conditions
	 * 
	 */
	protected String VISIBILITY_CONDITION_CLASS_NAME = "com.sap.isa.maintenanceobject.businessobject.VisibilityCondition";

	
    private final static String CACHE_NAME="MaintenanceObject-Cache";

	static {
	
	    // define cache behaviour
	    Cache.Attributes attr = new Cache.Attributes();
	    	
	    // your region has to be added to the cache (your own cache)
	    // Note: This region can also be preconfigured in the web-inf/cfg/cache-config.xml file
	    try {
	    	if (!Cache.isReady()) {
				Cache.init(); // The cache is accessed using an Access object
	    	}
	        Cache.addRegion(CACHE_NAME, attr); // init cache
	    }
	    catch (Exception ex) {
	        log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, ex.getMessage(), ex);
	    }
	
	}

    /**
     * reference of the used data handler
     *
     * @see com.sap.isa.maintenanceobject.backend.MaintenanceObjectDataHandler
     */
    protected MaintenanceObjectDataHandler dataHandler;

    // only temporary needed
    private MaintenanceObjectData maintenanceObject = null;
    private int debugLevel = 0;
    private static String CRLF = System.getProperty("line.separator");


	/**
	 * Constructor for MaintenanceObjectHelper.
	 */
	public MaintenanceObjectHelper ()	{

	}

    /**
     * Constructor for MaintenanceObjectHelper.
     * The constructor also creates an instance for the data handler.
     * The classname for the data handler is given with the property
     * <code>PROPERTY_DATA_HANDLER</code>
     *
     * @param connectionFactory to get connections
     * @param props a set of properties which may be useful to initialize the object
     */
    public MaintenanceObjectHelper (ConnectionFactory connectionFactory, Properties props)
            throws MaintenanceObjectHelperException {

        createDataHandler(connectionFactory, props);
    }


    /**
     * Constructor for MaintenanceObjectHelper.
     * This constructor get an instance for the data handler.
     *
     * @param dataHandler data handle which should be used
     */
    public MaintenanceObjectHelper (MaintenanceObjectDataHandler dataHandler)
            throws MaintenanceObjectHelperException {

        this.dataHandler = dataHandler;
    }


    /**
     * <p>The method fills given JCO records (<code>recordMap</code> with the information of the
     *  property backend properties.</p>
     * 
     * @param maintenanceObject maintenance object to be use
     * @param recordMap JCo records to fill
     */
    public static void fillJCORecordFromObject (MaintenanceObjectData maintenanceObject,
                                                Map recordMap) {
        
        Iterator iter = maintenanceObject.iterator();
        while (iter.hasNext()) {
            Iterator propertyIterator = ((PropertyGroupData)iter.next()).iterator();
            
            while (propertyIterator.hasNext()) {
				PropertyData property = (PropertyData) propertyIterator.next();
                BackendPropertiesData backendProperties = 
                		(BackendProperties) property.getBackendProperties();

				if (backendProperties != null) {
                
					String fieldName = backendProperties.getFieldName();
					String structureName = backendProperties.getStructureName();
					JCO.Record record = (JCO.Record)recordMap.get(structureName);
						
					if (record != null && fieldName.length() > 0) {
		                fillRecordFieldFromProperty(property, fieldName, record);
					}
				}	
			}
		}
    }

    /**
     * <p>The method fills a given JCO record with the information of the
     *  property backend properties. </p>
     * 
     * @param maintenanceObject maintenance object to be use
     * @param structureName name of structure which correspond to the JCoRecord 
     * @param record JCo record to fill
     */
    public static void fillJCORecordFromObject (MaintenanceObjectData maintenanceObject,
    		                                    String structureName,
                                                JCO.Record record) {
        
        Iterator iter = maintenanceObject.iterator();
        while (iter.hasNext()) {
            Iterator propertyIterator = ((PropertyGroupData)iter.next()).iterator();
            
            while (propertyIterator.hasNext()) {
				PropertyData property = (PropertyData) propertyIterator.next();
                BackendPropertiesData backendProperties = 
                		(BackendProperties) property.getBackendProperties();

				if (backendProperties != null) {
					String fieldName = backendProperties.getFieldName();
						
					if (checkStructureName(structureName, backendProperties) && fieldName.length() > 0) {
		                fillRecordFieldFromProperty(property, fieldName, record);
					}
				}	
			}
		}
    }

    /**
     * Fill shop object from Jco using property mapping table
     * 
     * @param shop shop object to fill
     * @param record JCo record to use
     * 
     */
    public static void fillObjectFromJCORecord(MaintenanceObjectData shop, 
                                               JCO.Record record) {
                                                      	
		Iterator iter = shop.iterator();
		while (iter.hasNext()) {
			Iterator propertyIterator = ((PropertyGroupData)iter.next()).iterator();

			while (propertyIterator.hasNext()) {
				PropertyData property = (PropertyData) propertyIterator.next();
				BackendPropertiesData backendProperties = (BackendProperties) property.getBackendProperties();
                
				if (backendProperties != null) {
					String fieldName = backendProperties.getFieldName();
	
					if (fieldName.length() > 0) {
	
						if (property.getType().equals(PropertyData.TYPE_BOOLEAN)) {
							property.setValue(JCoHelper.getBoolean(record, fieldName));
						}
						else if (property.getType().equals(PropertyData.TYPE_INTEGER)) {
							property.setValue(record.getInt(fieldName));
						}
						else {
							property.setValue(record.getString(fieldName));
						}
					}
				}		
			}
        }
    }
    
    

    // checks if structure name correspond to structure in backendProperty
    private static boolean checkStructureName(String structureName, BackendPropertiesData backendProperties) {
    	if (structureName != null) {
    		return structureName.equals(backendProperties.getStructureName());
    	}
    	return true;
    }

    // fill JCO Record field from Property.
    private static void fillRecordFieldFromProperty(PropertyData property, String fieldName, JCO.Record record) {
		if (property.getType().equals(PropertyData.TYPE_BOOLEAN)) {
		    JCoHelper.setValue(record, 
		                       property.getBoolean(), 
		                       fieldName);
		}
		else if (property.getType().equals(PropertyData.TYPE_INTEGER)) {
			JCoHelper.setValue(record,
							   property.getInteger(),
							   fieldName);
		}
		else {
		    record.setValue(property.getString(), fieldName);
		}
	}


    
	/**
	 * Fills a map with a mapping between backend property and property. 
     *
	 * @param maintenanceObject maintenanceObject object to use
	 */
	public static Map generateBackendMapping(MaintenanceObjectData maintenanceObject) {
        
        Map mapping = new HashMap();
        
		Iterator iter = maintenanceObject.iterator();
		while (iter.hasNext()) {
			Iterator propertyIterator = ((PropertyGroupData)iter.next()).iterator();
            
			while (propertyIterator.hasNext()) {
				PropertyData property = (PropertyData) propertyIterator.next();
				BackendPropertiesData backendProperties = (BackendProperties) property.getBackendProperties();
                
                if (backendProperties != null) {
					if (backendProperties != null && backendProperties instanceof BackendElementKeyHolder) {
						mapping.put(((BackendElementKeyHolder)backendProperties).getKey(),property.getName());
					}
				}	
			}
		}
		
		return mapping;
	}

    

	/**
	 * Initialize the maintenance object. <br>
	 *
	 * @param maintenanceObject the object which should be initialized.
	 * @param inputStream stream with object properties.
	 *
	 */
	public MaintenanceObjectData createObjectFromXCM(String className,
					            	ConfigContainer configContainer,	
								    String fileAlias)
				throws MaintenanceObjectHelperException {
		final String METHOD = "createObjectFromXCM()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("className="+className+", filealias="+fileAlias);
		MaintenanceObjectData newObject = null;

		CacheKey cacheKey = new CacheKey(className, "", fileAlias, configContainer.getConfigKey());

		try {
			Cache.Access access = Cache.getAccess(CACHE_NAME);

			// store an object in the Cache
			newObject = (MaintenanceObjectData) access.get(cacheKey);
			
			if (newObject == null) {
				
				newObject = (MaintenanceObjectData)(Class.forName(className)).newInstance();
				
				// store object for the add property-group method
				this.maintenanceObject = newObject;
				InputStream inputStream = configContainer.getConfigUsingAliasAsStream(fileAlias);
				parseConfigFile(inputStream);
				this.maintenanceObject = null;							   	
				
				synchronized(newObject) {
					// Since method createObjectFromXCM can be used from un-synchronized
					// UI classes it could come to an insert problem with an unneccessary  
					// error message in log file
					if (access.get(cacheKey) == null) {
						access.put(cacheKey, newObject);
					}
				}

			} 
					
		}
		catch (Cache.Exception exception) {
			log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "Failed to create object", exception);
		}
		catch (Exception exception) {
			log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "Failed to create object", exception);
		}
		finally {
			log.exiting();
		}
		return (MaintenanceObjectData) newObject.clone(); 
		
	} 


	
	/**
	 * Initialize the maintenance object. <br>
	 *
	 * @param maintenanceObject the object which should be initialized.
	 * @param inputStream stream with object properties.
	 *
	 */
	public void initObjectFromXCM(MaintenanceObjectData maintenanceObject,
								  ConfigContainer configContainer,	
								  String fileAlias)
				throws MaintenanceObjectHelperException {

		final String METHOD = "initObjectFromXCM()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("fileAlias="+fileAlias);
		// store object for the add property-group method
		this.maintenanceObject = maintenanceObject;
		InputStream inputStream = configContainer.getConfigUsingAliasAsStream(fileAlias);
		parseConfigFile(inputStream);
		this.maintenanceObject = null;							   	
		try {
			inputStream.close();
		}
		catch (IOException e) {
			log.error(LogUtil.APPS_COMMON_CONFIGURATION, "Failed to close the file", e);
		}
		log.exiting();
	} 


	/**
	 * Initialize the maintenance object. <br>
	 *
	 * @param maintenanceObject the object which should be initialized.
	 * @param inputStream stream with object properties.
	 *
	 */
	public void initObjectFromFile(MaintenanceObjectData maintenanceObject,
								   FileInputStream inputStream)
				throws MaintenanceObjectHelperException {
		final String METHOD = "initObjectFromFile()";
		log.entering(METHOD);
		// store object for the add property-group method
		this.maintenanceObject = maintenanceObject;
		parseConfigFile(inputStream);
		this.maintenanceObject = null;		
		log.exiting();						   	
								   	
    }
	

	/**
	 * Initialize the maintenance object. <br>
	 * The method checks if the object is already in the cache and returns then
	 * a copy.
	 *
	 * @param maintenanceObject the object which should be initialized.
	 * @param fileName name of the file which should be parsed
	 * @param isRelativePath <code>true</code> if the file name is given relative
	 *         to the web application
	 *
	 */
	public void initObjectFromFile(MaintenanceObjectData maintenanceObject,
	                               String fileName,
	                               boolean isRelativePath)
			throws MaintenanceObjectHelperException {


		final String METHOD = "initObjectFromFile()";
		log.entering(METHOD);
		if (log.isDebugEnabled()) {
			log.debug("fileName="+fileName+", isRelativePath="+isRelativePath);
		}	
			
		if (fileName != null && fileName.length()>0 ) {

			String fullFileName = fileName;
			
			if (isRelativePath) {
				fullFileName = InitializationHandler.getRealPath(fileName);
			}	

			try {
				Cache.Access access = Cache.getAccess(CACHE_NAME);

				MaintenanceObjectData cacheObject = (MaintenanceObjectData) access.get(fullFileName);
			
				if (cacheObject == null) {
		
            		initObjectfromFile(maintenanceObject, fullFileName);
					access.put(fullFileName, maintenanceObject.clone());
				} 
				else {
					maintenanceObject.copy(cacheObject);
				}
					
			}
			catch (Cache.Exception exception) {
				log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "Failed to create object", exception);
			}
			catch (Exception exception) {
				log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "Failed to create object", exception);
			}
			finally {
				log.exiting();
			}

		}
		else {
			log.exiting();
			throw new MaintenanceObjectHelperException ("Error reading configuration information" + CRLF +
														"No file name set");
		}
	}

	/**
	 * Initialize the maintenance object. <br>
	 *
	 * @param maintenanceObject the object which should be initialized.
	 * @param fileName name of the file which should be parsed
	 * @param isRelativePath <code>true</code> if the file name is given relative
	 *         to the web application
	 *
	 */
    protected void initObjectfromFile(MaintenanceObjectData maintenanceObject,
        							   String fullFileName)
        		throws MaintenanceObjectHelperException {
			final String METHOD = "initObjectFromFile()";
			
		log.entering(METHOD);

        try {
            FileInputStream inputStream = new FileInputStream(fullFileName);
        
        	// store object for the add property-group method
        	this.maintenanceObject = maintenanceObject;
        	parseConfigFile(inputStream);
        	this.maintenanceObject = null;
        	inputStream.close();
        
        }
        catch (FileNotFoundException e) {
        	throw new MaintenanceObjectHelperException ("Error reading configuration information" + CRLF +
        												"No file name set");
        } catch (IOException e) {
            log.error(LogUtil.APPS_COMMON_CONFIGURATION, "Failed to close file", e);
        }
        finally {
        	log.exiting();
        }
        log.info(LogUtil.APPS_COMMON_CONFIGURATION, "system.eai.init.readConfigFile", new Object[] { fullFileName});
        
    }



    /**
     * Initialize the maintenance object.<br>
     * Therefore the method read the property <code>PROPERTY_CONFIG_FILE</code>
     * and parse the file given with this filename.
     *
     * @param maintenanceObject
     * @param props a set of properties which may be useful to initialize the object
     *
     */
    public void initObjectFromFile(MaintenanceObjectData maintenanceObject,
                                   Properties props)
            throws MaintenanceObjectHelperException {

        String fileName = props.getProperty(PROPERTY_CONFIG_FILE);

        if (fileName != null && fileName.length()>0 ) {
			this.initObjectFromFile(maintenanceObject, fileName, true);
        }
    }


    /**
      * add a given property group to the maintened object
      *
      * only needed from the digester by parsing the config file
      *
      */
    public void addPropertyGroup(PropertyGroupData propertyGroup) {

        if (maintenanceObject != null) {
            maintenanceObject.addPropertyGroup(propertyGroup);
        }
    }


    /**
      * add a given screen group to the maintened object
      *
      * only needed from the digester by parsing the config file
      *
      */
    public void addScreenGroup(ScreenGroupData screenGroup) {

        if (maintenanceObject != null) {
            maintenanceObject.addScreenGroup(screenGroup);
        }
    }


    /**
     * This method create the instance of the MaintenanceObjectDataHandler.
     * The classname is given with the Property code>PROPERTY_DATA_HANDLER</code>
     *
     * @param connectionFactory to get connections
     * @param props a set of properties which may be useful to initialize the object
     */
    public void createDataHandler(ConnectionFactory connectionFactory, Properties props)
        throws MaintenanceObjectHelperException  {
		final String METHOD = "createDataHandler()";
		log.entering(METHOD);
        String className = props.getProperty(PROPERTY_DATA_HANDLER);
		dataHandler = createDataHandler(className , connectionFactory , props);

    	log.exiting();
    }
    
    private MaintenanceObjectDataHandler createDataHandler(String className , ConnectionFactory connectionFactory , Properties props)  
    		throws MaintenanceObjectHelperException  {
		if (className != null && className.length()>0 ) {

			try {
				// take the handler class from init file
				Class handlerClass = Class.forName(className);

				// now get the instance from the handler
				MaintenanceObjectDataHandler dataHandler = (MaintenanceObjectDataHandler) handlerClass.newInstance();

				if (dataHandler != null) {
					dataHandler.initHandler(connectionFactory, props);
				}
				return dataHandler;
				
			}
			catch (Exception ex) {
				String errMsg = "Error creating data handler class" + CRLF + ex.toString();
				log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "system.eai.exception", new Object[] { errMsg}, null);
				MaintenanceObjectHelperException wrapperEx = new MaintenanceObjectHelperException(errMsg);
				log.throwing(wrapperEx);
				throw wrapperEx;
			}
		}
		return null;
		
    }


    /**
     * Reads a list of objects and returns this list
     * using a tabluar data structure.
     *
     * @param objectId object id as search pattern
     * @param props, additional properties to restrict the search result
     * @return List of all available objects
     */
    public ResultData readObjectList(String objectId, Properties props)
            throws MaintenanceObjectHelperException {
		if (dataHandler != null) {
			return dataHandler.readObjectList(objectId,props);
		}
		return null;
	}


    /**
     * This method deletes a maintenance object with the key <code>objectKey</code>
     *
     * @param objectKey <code>TechKey</code> of the object
     */
 	public void deleteObject(TechKey objectKey)
			throws MaintenanceObjectHelperException {
		final String METHOD = "deleteObject()";
		log.entering(METHOD);
		if (dataHandler != null) {
			dataHandler.deleteObject(objectKey);
		}
		log.exiting();
	}


    /**
     * This method read the values of the maintenance object from the backend
     *
     * @param maintenanceObject
     *
     * @see com.sap.isa.maintenanceobject.backend.MaintenanceObjectDataHandler
     *
     */
    public void readData(MaintenanceObjectData maintenanceObject)
            throws MaintenanceObjectHelperException{
		final String METHOD = "readData()";
		log.entering(METHOD);
        if (dataHandler != null) {
            dataHandler.readData(maintenanceObject);
        }
        log.exiting();
    }

    /**
     * This method save the maintenance object in backend
     *
     * @param maintenanceObject
     *
     * @see com.sap.isa.maintenanceobject.backend.MaintenanceObjectDataHandler
     *
     */
    public void saveData(MaintenanceObjectData maintenanceObject)
            throws MaintenanceObjectHelperException {
		final String METHOD = "saveXMLData()";
		log.entering(METHOD);
        if (dataHandler != null) {
            dataHandler.saveData(maintenanceObject);
        }
        log.exiting();
    }

    /**
     * Set the debugLevel for the xml parse process
     *
     * @param debugLevel
     *
     */
    public void setDebugLevel(int debugLevel) {
        this.debugLevel = debugLevel;
    }


    /* private method to parse the config-file */
    private void parseConfigFile(InputStream inputStream)
    		throws MaintenanceObjectHelperException {

        // new Struts digester
        Digester digester = new Digester();

        digester.setDebug(debugLevel);

        digester.push(this);

        // create a new property group
        digester.addObjectCreate("maintenance-object/property-group",
                                 PROPERTY_GROUP_CLASS_NAME);

        // set all properties for the property group
        digester.addSetProperties("maintenance-object/property-group");

        // add property-group to the maintenance object
        digester.addSetNext("maintenance-object/property-group",
                            // "addPropertyGroup", "com.sap.isa.maintenanceobject.businessobject.PropertyGroup");
                            "addPropertyGroup", PropertyGroupData.class.getName());

        // create a new property
        digester.addObjectCreate("maintenance-object/property-group/property",
                                 PROPERTY_CLASS_NAME);

        // set all properties for the property
        digester.addSetProperties("maintenance-object/property-group/property");

		// get the help descriptions	
		digester.addCallMethod("maintenance-object/property-group/property/helpDescription","addHelpDescription",0);

        // add property to property group
        digester.addSetNext("maintenance-object/property-group/property",
                            "addProperty", PropertyData.class.getName());

        // create a new allowed value
        digester.addObjectCreate("maintenance-object/property-group/property/allowedValue",
                                 ALLOWED_VALUE_CLASS_NAME);

        // set all properties for the allowed value
        digester.addSetProperties("maintenance-object/property-group/property/allowedValue");

        // add allowed value to the property
        digester.addSetNext("maintenance-object/property-group/property/allowedValue",
                            "addAllowedValue", AllowedValueData.class.getName());


		// create a new backend properties object
		digester.addObjectCreate("maintenance-object/property-group/property/backend",
								 BACKEND_PROPERTIES_CLASS_NAME);

		// set all properties for the backend properties
		digester.addSetProperties("maintenance-object/property-group/property/backend");

		// add backend properties to the property
		digester.addSetNext("maintenance-object/property-group/property/backend",
							"setBackendProperties", Object.class.getName());


		// create a new visibilty condition
		digester.addObjectCreate("maintenance-object/property-group/property/depending",
								 VISIBILITY_CONDITION_CLASS_NAME);

		// set all properties for the allowed value
		digester.addSetProperties("maintenance-object/property-group/property/depending");

		// add allowed value to the property
		digester.addSetNext("maintenance-object/property-group/property/depending",
							"addVisibilityCondition", VisibilityConditionData.class.getName());

        // create a new screen group
        digester.addObjectCreate("maintenance-object/screen-group",
                                 SCREEN_GROUP_CLASS_NAME);

        // set all properties for the screen group
        digester.addSetProperties("maintenance-object/screen-group");

		// add property-group to the screen groups	
		digester.addCallMethod("maintenance-object/screen-group/property-group","addTempPropertyGroupName",0);

        // add property-group to the maintenance object
        digester.addSetNext("maintenance-object/screen-group",
                            "addScreenGroup", ScreenGroupData.class.getName());

		try {
        	digester.parse(inputStream);
            adjustScreenGroups();        	
        }
        catch (Exception ex) {
            String errMsg = "Error reading configuration information" + CRLF + ex.toString();
            log.error(LogUtil.APPS_COMMON_CONFIGURATION, "mob.error.eaiException", new Object[] { errMsg}, null);
			MaintenanceObjectHelperException wrapperEx = new MaintenanceObjectHelperException(errMsg);
			log.throwing(wrapperEx);
			throw wrapperEx;
        }
    }


	/**
	 * Add the temporary stored property groups to the screen group. <br>
	 */
    protected void adjustScreenGroups() {
        Iterator screenIter = maintenanceObject.screenGroupIterator();
        
        while (screenIter.hasNext()) {
            ScreenGroupData screenGroup = (ScreenGroupData) screenIter.next();
            
        	Iterator iter = screenGroup.getTempPropertyGroupNames();
        	while (iter.hasNext()) {
        	
        		PropertyGroupData propertyGroup = maintenanceObject.getPropertyGroup((String) iter.next());
        		if (propertyGroup != null) {
        			screenGroup.addPropertyGroup(propertyGroup);
        		}	
        	}
        screenGroup.deleteTempPropertyGroupNames();
        }
    }


	/* Private class to handle the cache key */
	private class CacheKey extends GenericCacheKey{

		public CacheKey(String className, String fileName, String fileAlias, String configuration) {
			super(new Object[] {className, fileName, fileAlias, configuration});
		}

	}
	


}
