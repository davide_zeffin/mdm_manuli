/*****************************************************************************
    Class:        GenericSearchBuilderHelper
    Copyright (c) 2003, SAP AG, Germany, All rights reserved.

    $Revision: #1 $
    $DateTime: 2003/12/10 12:07:46 $ (Last changed)
    $Change: 163422 $ (changelist)
    
*****************************************************************************/
package com.sap.isa.maintenanceobject.backend;

/**
 * Build on the MaintenanceObjectHelper class takes care to use the extended
 * objects of <code>screen-group</code>, <code>property-group</code>,
 * <code>property</code> and <code>allowedValue</>.
 *
 */

public class GenericSearchBuilderHelper extends MaintenanceObjectHelper {


	/**
	 * Constructor for GenericSearchBuilderHelper.
	 */
	public GenericSearchBuilderHelper ()	{

        // Set the new class names.

		PROPERTY_GROUP_CLASS_NAME = "com.sap.isa.maintenanceobject.businessobject.GSPropertyGroup";

		SCREEN_GROUP_CLASS_NAME = "com.sap.isa.maintenanceobject.businessobject.GSScreenGroup";

		PROPERTY_CLASS_NAME = "com.sap.isa.maintenanceobject.businessobject.GSProperty";
		
		ALLOWED_VALUE_CLASS_NAME = "com.sap.isa.maintenanceobject.businessobject.GSAllowedValue";

	}

}
