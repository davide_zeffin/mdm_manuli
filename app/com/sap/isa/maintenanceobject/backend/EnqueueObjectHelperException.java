/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Wolfgang Sattler
  Created:      February 2002
  Version:      1.0

  $Revision: #7 $
  $Date: 2001/07/25 $
*****************************************************************************/
package com.sap.isa.maintenanceobject.backend;

import com.sap.isa.core.util.Message;

/**
 * This exception is thrown by the EnqueueObjectHelper if something goes wrong
 * while lock/unlock maintenance objects
 */
public class EnqueueObjectHelperException extends Exception {

    private Message isaMessage;

    /**
     * Constructor
     * @param msg Message describing the exception
     */
    public EnqueueObjectHelperException(String msg) {
        super(msg);
    }


    /**
     * Constructor
     * @param message Message describing the exception userfriendly
     * @param msg Message describing the exception
     */
    public EnqueueObjectHelperException(Message isaMessage, String msg) {
        super(msg);
        this.isaMessage = isaMessage;
    }


    /**
     * Set the property message
     *
     * @param message
     *
     */
    public void setIsaMessage(Message isaMessage) {
        this.isaMessage = isaMessage;
    }


    /**
     * Returns the property message
     *
     * @return message
     *
     */
    public Message getIsaMessage() {
        return this.isaMessage;
    }

}