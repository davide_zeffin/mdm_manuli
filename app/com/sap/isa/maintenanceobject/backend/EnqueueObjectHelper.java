/*****************************************************************************
    Class:        EnqueueObjectHelper
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP
    Created:      February 2002
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.maintenanceobject.backend;


import java.util.*;

import com.sap.isa.core.logging.IsaLocation;



/**
 *
 * @author SAP
 * @version 1.0
 *
 */
public class EnqueueObjectHelper 
{
    /**
     * reference to the IsaLocation
     *
     * @see com.sap.isa.core.logging.IsaLocation
     */
    private static final IsaLocation log = IsaLocation.
          getInstance(EnqueueObjectHelper.class.getName());


	private static EnqueueObjectHelper singelton = new EnqueueObjectHelper();  


	/**
	 * get the EnqueueObjectHelper 
	 * 
	 * @return the singelton
	 */
	public static EnqueueObjectHelper getEnqueueObjectHelper() {
		
		return singelton;  
	}	

	/**
	 * Map to store the locked objects	
	 * 
	 */ 
    protected Map lockObjectMap;

	
	/* Singelton */
    private EnqueueObjectHelper() {
        lockObjectMap = Collections.synchronizedMap(new HashMap());
    }


	/**
	 * Locks an object 
	 * 
	 */
    public void lockObject(String objectType, String objectId)
        throws EnqueueObjectHelperException {
        	this.lockObject(objectType,objectId,"");
        }


	/**
	 * Locks an object 
	 */
    public void lockObject(String objectType, String objectId, String sessionID)
        throws EnqueueObjectHelperException {
        synchronized(lockObjectMap) {
			final String METHOD = "lockObject()";
			log.entering(METHOD);
			if (log.isDebugEnabled())
				log.debug("objectType="+objectType+", objectId="+objectId+", sessionID="+sessionID);
            ObjectKey objectKey = new ObjectKey(objectType, objectId);
            if(lockObjectMap.containsKey(objectKey)) {
            	log.exiting();
                throw new EnqueueObjectHelperException("Object named '" + objectId + "' type '" + objectType + "' already locked");
            }    
            ObjectInfo objectInfo = new ObjectInfo();
            objectInfo.setObjectType(objectType);
            objectInfo.setSessionID(sessionID);
            lockObjectMap.put(objectKey, objectInfo);
            log.exiting();
        }
        
    }

    public void unLockObject(String objectType, String objectID)
    {
        synchronized(lockObjectMap)
        {	
        	final String METHOD = "unLockObject()";
			log.entering(METHOD);
			if (log.isDebugEnabled())
				log.debug("objectType="+objectType+", objectID="+objectID);
            ObjectKey objectKey = new ObjectKey(objectType, objectID);
            ObjectInfo objectInfo = (ObjectInfo)lockObjectMap.get(objectKey);
            if(objectInfo == null)
            {
                if(log.isDebugEnabled())
                    log.debug("Object doesn't exist: type " + objectType + " objectID " + objectID);
                log.exiting();
                return;
            }
            lockObjectMap.remove(objectKey);
            objectInfo = null;
            log.exiting();
        }
    }

    public void unLockObjects(String sessionID) {
        synchronized(lockObjectMap) {
			final String METHOD = "unLockObject()";
			log.entering(METHOD);
			if (log.isDebugEnabled())
				log.debug("sessionID="+sessionID);
            
            Iterator iterator = lockObjectMap.values().iterator();
            ObjectInfo objectInfo = null;
            while(iterator.hasNext())  {
                objectInfo = (ObjectInfo)iterator.next();
                if(objectInfo.getSessionID().equals(sessionID)) {
                    iterator.remove();
                }    
            }
            log.exiting();
        }
    }


	/**
	 * inner class to handle the object key
	 * for the key of the map
	 */
	protected class ObjectKey {

	    private String objectType;
	    private String objectID;
	
	    public ObjectKey (String objectType, String objectID) {
	        this.objectType = objectType;
	        this.objectID = objectID;
	    }

	    public int hashCode() {
	        return objectType.hashCode() ^ objectID.hashCode();
	    }
	
	    public boolean equals(Object o) {
	        if(o == null)
	            return false;
	        if(o == this)
	            return true;
	        if(!(o instanceof ObjectKey)) {
	            return false;
	        }
	        else {
	            ObjectKey command = (ObjectKey)o;
	            return objectType.equals(command.objectType) & objectID.equals(command.objectID);
	        }
	    }
	
	    public String toString() {
	        return "objectType: "+ objectType +
	               " objectId: " + objectID;
	    }


	}	


	/**
	 * inner class to handle the object info data
	 * for the entry of the map
	 */
	protected class ObjectInfo {

	    private String objectType;
	    private String sessionID;
	    
	    public void setObjectType(String objectType){
	        this.objectType = objectType;
	    }
	
	    public String getObjectType(){
	        return objectType;
	    }
	
	    public void setSessionID(String sessionID) {
	        this.sessionID = sessionID;
	    }
	
	    public String getSessionID() {
	        return sessionID;
	    }

	}





}
