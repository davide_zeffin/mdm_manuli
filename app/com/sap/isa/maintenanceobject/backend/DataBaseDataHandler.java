/*****************************************************************************
    Class:        DataBaseDataHandler
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      February 2002
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.maintenanceobject.backend;

import com.sap.isa.maintenanceobject.backend.MaintenanceObjectDataHandler;
import com.sap.isa.maintenanceobject.backend.MaintenanceObjectHelperException;

import com.sap.isa.maintenanceobject.backend.boi.*;

import com.sap.isa.core.TechKey;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.eai.ConnectionFactory;
import com.sap.isa.core.eai.BackendException;

import com.sap.isa.core.util.Message;
import com.sap.isa.core.eai.sp.jdbc.*;
import com.sap.isa.core.util.table.*;

import java.util.*;

import java.sql.*;

/**
 * Read and write the data for a maintenance object in a data base.
 *
 * @author Wolfgang Sattler
 * @version 1.0
 *
 */
public class DataBaseDataHandler implements MaintenanceObjectDataHandler {
	/**
	 * reference to the IsaLocation
	 *
	 * @see com.sap.isa.core.logging.IsaLocation
	 */
	private static final IsaLocation log =
		IsaLocation.getInstance(DataBaseDataHandler.class.getName());

	/**
	 * global constant for the Parameter which defines the name of the JDBC connectionFactory
	 *
	 */
	public static String JDBC_FACTORY_NAME = "JDBCFactoryName";

	/**
	 * global constant for the Parameter which defines the name of the JDBC connection
	 *
	 */
	public static String JDBC_CONNECTION_NAME = "JDBCConnectionName";

	/**
	 * global constant for the Parameter which defines the name of the data base
	 *
	 */
	public static String DATA_BASE_NAME = "DataBaseName";

	/**
	 * global constant for the Parameter which defines the name of the object table
	 *
	 */
	public static String OBJECT_TABLE_NAME = "ObjectTableName";

	/**
	 * global constant for the Parameter which defines the name of the object table
	 *
	 */
	public static String PROPERTY_TABLE_NAME = "PropertyTableName";

	private ConnectionFactory connectionFactory;
	private String factoryName;
	private String ConnectionName;

	private String dataBaseName;
	private String objectTableName;
	private String propertyTableName;

	/*
	 * A private method which returns a valid connection
	 */
	private Connection getConnection()
		throws MaintenanceObjectHelperException {
		final String METHOD = "getConnection()";
		log.entering(METHOD);
		try {
			JDBCConnection fwConnection =
				(JDBCConnection) connectionFactory.getConnection(
					factoryName,
					ConnectionName);

			Connection connection = fwConnection.getConnection();
			connection.setCatalog(dataBaseName);
			return connection;
		} catch (BackendException ex) {
			String errMsg = ex.toString();
			log.error(
				LogUtil.APPS_COMMON_INFRASTRUCTURE,
				"mob.dh.createConnection",
				new Object[] { factoryName, errMsg },
				null);
			MaintenanceObjectHelperException wrapperEx =
				new MaintenanceObjectHelperException(
					new Message(Message.ERROR, "mob.error.createConnection"),
					errMsg);
			log.throwing(wrapperEx);
			throw wrapperEx;

		} catch (SQLException ex) {
			String errMsg = ex.toString();
			log.error(
				LogUtil.APPS_BUSINESS_LOGIC,
				"mob.dh.setDataBase",
				new Object[] { dataBaseName, errMsg },
				null);
			MaintenanceObjectHelperException wrapperEx =
				new MaintenanceObjectHelperException(
					new Message(Message.ERROR, "mob.error.createConnection"),
					errMsg);
			log.throwing(wrapperEx);
			throw wrapperEx;

		} finally {
			log.exiting();
		}
	}

	/**
	 * This method initialize the maintenance object handler in the backend.
	 *
	 * @param connectionFactory to get connections
	 * @param props a set of properties which may be useful to initialize the object
	 */
	public void initHandler(
		ConnectionFactory connectionFactory,
		Properties props) {
		final String METHOD = "initHandler()";
		log.entering(METHOD);
		this.connectionFactory = connectionFactory;

		factoryName = props.getProperty(JDBC_FACTORY_NAME);
		ConnectionName = props.getProperty(JDBC_CONNECTION_NAME);

		dataBaseName = props.getProperty(DATA_BASE_NAME);
		objectTableName = props.getProperty(OBJECT_TABLE_NAME);
		propertyTableName = props.getProperty(PROPERTY_TABLE_NAME);

		if (factoryName == null
			|| ConnectionName == null
			|| dataBaseName == null
			|| objectTableName == null
			|| propertyTableName == null) {
			log.error(
				LogUtil.APPS_COMMON_INFRASTRUCTURE,
				"mob.error.noTableNames");
		}
		log.exiting();
	}

	/**
	 * Reads a list of shops and returns this list
	 * using a tabluar data structure.
	 *
	 * @param objectId object id as search pattern
	 * @param props, additional properties to restrict the search result
	 * @return List of all available objects
	 */
	public ResultData readObjectList(String objectId, Properties props)
		throws MaintenanceObjectHelperException {
		final String METHOD = "readObjectList()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("objectId=" + objectId);
		boolean readAll = (objectId.length() == 0 || objectId.charAt(0) == '*');
		Connection connection = getConnection();

		// create the table for the result set
		Table objectTable = new Table("Objects");

		objectTable.addColumn(Table.TYPE_STRING, MaintenanceObjectData.ID);
		objectTable.addColumn(
			Table.TYPE_STRING,
			MaintenanceObjectData.DESCRIPTION);

		// build the select statement
		StringBuffer query = new StringBuffer();

		if (props != null) {
			// add optional properties to the query using the join statement

			Iterator iter = props.keySet().iterator();

			if (iter.hasNext()) {
				String key = (String) iter.next();

				query
					.append("SELECT o.id, o.description FROM ")
					.append(objectTableName)
					.append(" o JOIN ")
					.append(propertyTableName)
					.append(" p ON o.id = p.id ")
					.append(" WHERE (")
					.append("p.name = '")
					.append(key)
					.append("') AND (p.pvalue = '")
					.append(props.getProperty(key))
					.append("')");

				while (iter.hasNext()) {
					key = (String) iter.next();

					query
						.append(" AND (")
						.append("p.name = '")
						.append(key)
						.append("') AND (p.pvalue = '")
						.append(props.getProperty(key))
						.append("')");
				}

				if (!readAll) {
					query.append(" AND (o.id = '").append(objectId).append(
						"')");
				}
			}
		}

		if (query.length() == 0) {
			// no addional properties found
			query.append("SELECT id, description FROM ").append(
				objectTableName);

			if (!readAll) {
				query.append(" WHERE id = '").append(objectId).append('\'');
			}
		}

		try {
			// create the statement and start the query
			Statement statement = connection.createStatement();

			log.debug("read object list query: " + query.toString());

			ResultSet resultSet = statement.executeQuery(query.toString());

			while (resultSet.next()) {
				TableRow objectRow = objectTable.insertRow();

				objectRow.setRowKey(new TechKey(resultSet.getString(1).trim()));
				objectRow.getField(MaintenanceObjectData.ID).setValue(
					resultSet.getString(1).trim());
				objectRow.getField(MaintenanceObjectData.DESCRIPTION).setValue(
					resultSet.getString(2).trim());
			}

			resultSet.close();
			statement.close();
		} catch (SQLException ex) {
			String errMsg = ex.toString();
			log.error(
				LogUtil.APPS_BUSINESS_LOGIC,
				"mob.dh.readDataBase",
				new Object[] { objectTableName, errMsg },
				null);

			MaintenanceObjectHelperException wrapperEx =
				new MaintenanceObjectHelperException(
					new Message(Message.ERROR, "mob.error.readData"),
					errMsg);
			log.throwing(wrapperEx);
			throw wrapperEx;
		} finally {
			log.exiting();
		}

		return new ResultData(objectTable);
	}

	/**
	 * This method deletes a maintenance object with the key <code>objectKey</code>
	 * from the database
	 *
	 * @param objectKey <code>TechKey</code> of the object
	 */
	public void deleteObject(TechKey objectKey)
		throws MaintenanceObjectHelperException {

		final String METHOD = "deleteObject()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("objectKey=" + objectKey);
		String objectId = objectKey.getIdAsString();

		Connection connection = getConnection();

		// build the select statement
		StringBuffer query1 = new StringBuffer();

		query1
			.append("DELETE FROM ")
			.append(objectTableName)
			.append(" WHERE id = '")
			.append(objectId)
			.append('\'');

		// build the select statement
		StringBuffer query2 = new StringBuffer();

		query2
			.append("DELETE FROM ")
			.append(propertyTableName)
			.append(" WHERE id = '")
			.append(objectId)
			.append('\'');

		try {
			// create the statement and start the query
			Statement statement = connection.createStatement();

			log.debug("Delete object query: " + query1.toString());

			statement.executeUpdate(query1.toString());

			log.debug("Delete property query: " + query2.toString());

			statement.executeUpdate(query2.toString());

			statement.close();

			connection.commit();

		} catch (SQLException ex) {
			String errMsg = ex.toString();
			log.error(
				LogUtil.APPS_BUSINESS_LOGIC,
				"mob.dh.writeDataBase",
				new Object[] { propertyTableName, errMsg },
				null);
			try {
				connection.rollback();
			} catch (SQLException sex) {
				log.debug(sex.getMessage());
			}
			MaintenanceObjectHelperException wrapperEx =
				new MaintenanceObjectHelperException(
					new Message(Message.ERROR, "mob.error.saveData"),
					errMsg);
			log.throwing(wrapperEx);
			throw wrapperEx;

		} finally {
			log.exiting();
		}
	}

	/**
	 * This method read the values of the maintenance object from the backend
	 *
	 * @param maintenanceObject
	 */
	public void readData(MaintenanceObjectData maintenanceObject)
		throws MaintenanceObjectHelperException {

		final String METHOD = "readData()";
		log.entering(METHOD);
		Connection connection = getConnection();
		Properties properties = new Properties();
		String objectId = maintenanceObject.getId();

		// build the select statement
		StringBuffer query = new StringBuffer();

		query
			.append("SELECT name, pvalue FROM ")
			.append(propertyTableName)
			.append(" WHERE id = '")
			.append(objectId)
			.append('\'');

		try {
			// create the statement and start the query
			Statement statement = connection.createStatement();

			log.debug("read properties query: " + query.toString());

			ResultSet resultSet = statement.executeQuery(query.toString());

			while (resultSet.next()) {
				properties.setProperty(
					resultSet.getString(1).trim(),
					resultSet.getString(2).trim());
			}

			resultSet.close();
			statement.close();
		} catch (SQLException ex) {
			String errMsg = ex.toString();
			log.error(
				LogUtil.APPS_BUSINESS_LOGIC,
				"mob.dh.readDataBase",
				new Object[] { propertyTableName, errMsg },
				null);
			log.exiting();
			MaintenanceObjectHelperException wrapperEx =
				new MaintenanceObjectHelperException(
					new Message(Message.ERROR, "mob.error.readData"),
					errMsg);
			log.throwing(wrapperEx);
			throw wrapperEx;

		}

		maintenanceObject.setDescription(readDescription(connection, objectId));

		Iterator groupIterator = maintenanceObject.iterator();

		while (groupIterator.hasNext()) {

			Iterator properyIterator =
				((PropertyGroupData) groupIterator.next()).iterator();

			while (properyIterator.hasNext()) {
				setPropertyValue(
					(PropertyData) properyIterator.next(),
					properties);
			}
		}
		log.exiting();
	}

	/**
	 * This method save the maintenance object in backend
	 *
	 * @param maintenanceObject
	 */
	public void saveData(MaintenanceObjectData maintenanceObject)
		throws MaintenanceObjectHelperException {
		final String METHOD = "saveData()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("to be saved object=" + maintenanceObject);
		try {
			Connection connection = getConnection();

			// first save change at the object header
			saveObject(connection, maintenanceObject);

			// nov save all properties
			Iterator groupIterator = maintenanceObject.iterator();

			while (groupIterator.hasNext()) {

				Iterator properyIterator =
					((PropertyGroupData) groupIterator.next()).iterator();

				while (properyIterator.hasNext()) {
					appendProperty(
						connection,
						maintenanceObject,
						(PropertyData) properyIterator.next());
				}
			}
			connection.commit();
		} catch (SQLException ex) {
			String errMsg = ex.toString();
			log.error(
				LogUtil.APPS_BUSINESS_LOGIC,
				"mob.dh.writeDataBase",
				new Object[] { objectTableName, errMsg },
				null);
			MaintenanceObjectHelperException wrapperEx =
				new MaintenanceObjectHelperException(
					new Message(Message.ERROR, "mob.error.saveData"),
					errMsg);
			log.throwing(wrapperEx);
			throw wrapperEx;

		} finally {
			log.exiting();
		}
	}

	/*
	 * A private method, which writes a property into the XML-file
	 */
	private void appendProperty(
		Connection connection,
		MaintenanceObjectData maintenanceObject,
		PropertyData property)
		throws MaintenanceObjectHelperException, SQLException {

		if (property.isSimpleType()) {

			// build the select statement
			StringBuffer query = new StringBuffer();

			if (maintenanceObject.isNewObject()) {
				query
					.append("INSERT INTO ")
					.append(propertyTableName)
					.append(" VALUES('")
					.append(maintenanceObject.getId())
					.append("', '")
					.append(property.getName())
					.append("', '")
					.append(property.getValue().toString())
					.append("')");
			} else {
				query
					.append("UPDATE ")
					.append(propertyTableName)
					.append(" SET pvalue = '")
					.append(property.getValue().toString())
					.append("' WHERE (id = '")
					.append(maintenanceObject.getId())
					.append("') AND (name = '")
					.append(property.getName())
					.append("')");
			}

			try {
				// create the statement and start the query
				Statement statement = connection.createStatement();

				log.debug("save property query: " + query.toString());

				statement.executeUpdate(query.toString());

				statement.close();
			} catch (SQLException ex) {
				String errMsg = ex.toString();
				log.error(
					LogUtil.APPS_BUSINESS_LOGIC,
					"mob.dh.writeDataBase",
					new Object[] { propertyTableName, errMsg },
					null);
				connection.rollback();
				MaintenanceObjectHelperException wrapperEx =
					new MaintenanceObjectHelperException(
						new Message(Message.ERROR, "mob.error.saveData"),
						errMsg);
				log.throwing(wrapperEx);
				throw wrapperEx;

			}

		}

	}

	/* a private method which reads the Description for one object */
	private String readDescription(Connection connection, String objectId)
		throws MaintenanceObjectHelperException {

		String description = "";

		// build the select statement
		StringBuffer query = new StringBuffer();

		query
			.append("SELECT id, description FROM ")
			.append(objectTableName)
			.append(" WHERE id = '")
			.append(objectId)
			.append('\'');

		try {
			// create the statement and start the query
			Statement statement = connection.createStatement();

			log.debug("read description query: " + query.toString());

			ResultSet resultSet = statement.executeQuery(query.toString());

			if (resultSet.next()) {
				description = resultSet.getString(2);
			}

			resultSet.close();
			statement.close();
		} catch (SQLException ex) {
			String errMsg = ex.toString();
			log.error(
				LogUtil.APPS_BUSINESS_LOGIC,
				"mob.dh.readDataBase",
				new Object[] { objectTableName, errMsg },
				null);
			MaintenanceObjectHelperException wrapperEx =
				new MaintenanceObjectHelperException(
					new Message(Message.ERROR, "mob.error.readData"),
					errMsg);
			log.throwing(wrapperEx);
			throw wrapperEx;

		}
		return description;
	}

	/* a private method which reads the Description for one object */
	private void saveObject(
		Connection connection,
		MaintenanceObjectData maintenanceObject)
		throws MaintenanceObjectHelperException, SQLException {

		// build the select statement
		StringBuffer query = new StringBuffer();

		if (maintenanceObject.isNewObject()) {
			query
				.append("INSERT INTO ")
				.append(objectTableName)
				.append(" VALUES('")
				.append(maintenanceObject.getId())
				.append("', '")
				.append(maintenanceObject.getDescription())
				.append("')");
		} else {
			query
				.append("UPDATE ")
				.append(objectTableName)
				.append(" SET description = '")
				.append(maintenanceObject.getDescription())
				.append("' WHERE id = '")
				.append(maintenanceObject.getId())
				.append('\'');
		}

		try {
			// create the statement and start the query
			Statement statement = connection.createStatement();

			log.debug("save object query: " + query.toString());

			statement.executeUpdate(query.toString());

			statement.close();
		} catch (SQLException ex) {
			String errMsg = ex.toString();
			log.error(
				LogUtil.APPS_BUSINESS_LOGIC,
				"mob.dh.writeDataBase",
				new Object[] { objectTableName, errMsg },
				null);
			connection.rollback();
			MaintenanceObjectHelperException wrapperEx =
				new MaintenanceObjectHelperException(
					new Message(Message.ERROR, "mob.error.saveData"),
					errMsg);
			log.throwing(wrapperEx);
			throw wrapperEx;
		}

	}

	/*
	 * A private method, which set the property in the object, with property value
	 * which is found in the property table
	 *
	 */
	private void setPropertyValue(
		PropertyData property,
		Properties properties) {

		String value = properties.getProperty(property.getName());

		if (value != null) {
			if (property.getType().equals(PropertyData.TYPE_STRING)) {
				property.setValue(value);
			} else if (property.getType().equals(PropertyData.TYPE_BOOLEAN)) {
				property.setValue(Boolean.valueOf(value));
			} else if (property.getType().equals(PropertyData.TYPE_INTEGER)) {
				property.setValue(Integer.valueOf(value));
			}
		} else {
			property.clearValue();
		}
	}

}
