/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Franz Mueller
  Created:      08 Februar 2001
*****************************************************************************/

package com.sap.isa.core;

import com.sap.isa.core.eai.sp.jco.JCoManagedConnectionFactory;

/**
 * This is the description of all the attribute-names
 * which may either be placed in the application context through
 * the <code>setAttribute()</code> method or for
 * all the initial parameteres from the <code>web.xml</code>
*/


public interface ContextConst  extends SharedConst {


    /**
     *  Defaultencoding
     */
    public  static final String DEFAULT_ENCODING = "ISO-8859-1";

	/**
	 *  Defaultencoding
	 */
	public  static final String DEFAULT_ENCODING_UTF8 = "UTF-8";


    /**
     *  ISA scenario
     */
    public  static final String ISA_SCENARIO = "scenario.core.isa.sap.com";

    /**
     * ISA scenario: B2B
     */
    public  static final String ISA_SCENARIO_B2B = "B2B";

    /**
     * ISA scenario: B2C
     */
    public  static final String ISA_SCENARIO_B2C = "B2C";


    /**
     *  Language of ISA application set in <code>web.xml</code> file
     */
    public  static final String LANGUAGE = "language.isa.sap.com";
	/**
	 *  Language of ISA application set in <code>interaction-config.xml</code> file
	 */
	public  static final String LANGUAGE_IC = "language";


    /**
     *  MIMES  is set in the <code>web.xml</code> file
     */
    public static final String MIMES = "mimes.core.isa.sap.com";
	public static final String MIMES_IC = "mimes.core";

    /**
     *  HTTP_PORT  is set in the <code>web.xml</code> file
     *  This one is just required when a web application
     *  has to support SSL
     */
    public static final String HTTP_PORT = "http.port.core.isa.sap.com";
	public static final String HTTP_PORT_IC = "http.port.core";
    /**
     *  HTTPS_PORT  is set in the <code>web.xml</code> file
     *  This one is just required when a web application
     *  has to support SSL
     */
    public static final String HTTPS_PORT = "https.port.core.isa.sap.com";
	public static final String HTTPS_PORT_IC = "https.port.core";

    /**
     * Constant to store an excpetion in the context and pass it to another
     * page.
     */
    public static final String EXCEPTION = "error.exception";

    /**
     *  InitialisationEnvironment
     */
    public  static final String INIT_ENV = "initialization.environment.core.isa.sap.com";

    /**
     * EAI constant extension (JCO)
     */
    public static final String LOGON_JCO_CONST_EXTENSION = ".jco.sp.eai.core.isa.sap.com";

    /**
     * EAI constant extension (IPC)
     */
    public static final String LOGON_IPC_CONST_EXTENSION = ".ipc.sp.eai.core.isa.sap.com";

    /**
     *  CCMS_CONFIG_PATH  is set in the <code>web.xml</code> file
     */
    public static final String CCMS_CONFIG_PATH = "ccms.path.core.isa.sap.com";
    
    /**
     * UME_FLAG is set in <code>web.xml</code> file (UME)
     */
    public static final String UME_FLAG = "useUME.B2B.sap.com";     

    /**
     * Class name of class which performs mapping of context-params with
     * eai connection params.
     */
    public static final String LOGON_MAPPING_CLASS = "classname.init.eai.core.isa.sap.com";

    /**
     * This parameter is specifies in the <code>web.xml</code> whether session tracking should be turned on
     * Possible values are <code>true|false</code>
     */
    public static final String SESSION_TRACKING = "sessiontracking.core.isa.sap.com";
    
     
	/**
	 * This parameter is specifies in the <code>web.xml</code> a url for a relogin if the session is lost.
	 * Possible values are <code>true|false</code>
	 */ 
    public static final String RELOGIN_URL      = "reloginurl.core";
    
	
	/**
	 * This parameter in the <code>web.xml</code> specifies the name of the init action
	 * of the application. <br>
	 * <strong>Example</strong><br>b2b/init.do   
	 * 
	 */ 
	public static final String APP_INIT_ACTION      = "initAction.core.isa.sap.com";
    
    /**
     * This parameter is specifies in the <code>web.xml</code> whether relogin cookie should be turned on.
     * Possible values are <code>true|false</code>
     */
	public static final String RELOGIN_COOKIE   = "enablerelogincookie.core";
    
	
    // constants used to connect to SAP system
    public  static final String LOGON_SAP_CLIENT =
        JCoManagedConnectionFactory.JCO_CLIENT  + LOGON_JCO_CONST_EXTENSION;
    public  static final String LOGON_SAP_USER =
        JCoManagedConnectionFactory.JCO_USER    + LOGON_JCO_CONST_EXTENSION;
    public  static final String LOGON_SAP_PASSWD =
        JCoManagedConnectionFactory.JCO_PASSWD  + LOGON_JCO_CONST_EXTENSION;
    public  static final String LOGON_SAP_LANG =
        JCoManagedConnectionFactory.JCO_LANG    + LOGON_JCO_CONST_EXTENSION;
    public  static final String LOGON_SAP_SYSNR =
        JCoManagedConnectionFactory.JCO_SYSNR   + LOGON_JCO_CONST_EXTENSION;
    public  static final String LOGON_SAP_ASHOST =
        JCoManagedConnectionFactory.JCO_ASHOST + LOGON_JCO_CONST_EXTENSION;
    public  static final String LOGON_SAP_MSHOST =
        JCoManagedConnectionFactory.JCO_MSHOST + LOGON_JCO_CONST_EXTENSION;
    public  static final String LOGON_SAP_GWHOST =
        JCoManagedConnectionFactory.JCO_GWHOST + LOGON_JCO_CONST_EXTENSION;
    public  static final String LOGON_SAP_GWSERV =
        JCoManagedConnectionFactory.JCO_GWSERV + LOGON_JCO_CONST_EXTENSION;
    public  static final String LOGON_SAP_R3NAME =
        JCoManagedConnectionFactory.JCO_R3NAME + LOGON_JCO_CONST_EXTENSION;
    public  static final String LOGON_SAP_GROUP =
        JCoManagedConnectionFactory.JCO_GROUP + LOGON_JCO_CONST_EXTENSION;
    public  static final String LOGON_SAP_TPHOST =
        JCoManagedConnectionFactory.JCO_TPHOST + LOGON_JCO_CONST_EXTENSION;
    public  static final String LOGON_SAP_TPNAME =
        JCoManagedConnectionFactory.JCO_TPNAME + LOGON_JCO_CONST_EXTENSION;
    public  static final String LOGON_SAP_TYPE =
        JCoManagedConnectionFactory.JCO_TYPE + LOGON_JCO_CONST_EXTENSION;
    public  static final String LOGON_SAP_TYPE_EXTERNAL =
        JCoManagedConnectionFactory.JCO_TYPE_EXTERNAL + LOGON_JCO_CONST_EXTENSION;
    public  static final String LOGON_SAP_TYPE_R3 =
        JCoManagedConnectionFactory.JCO_TYPE_R3 + LOGON_JCO_CONST_EXTENSION;
    public  static final String LOGON_SAP_MAXCON =
        JCoManagedConnectionFactory.JCO_MAXCON + LOGON_JCO_CONST_EXTENSION;


    /**
     * EAI constant extension (IMS)
     */
    public static final String CATALOG_IMS_CONST_EXTENSION = ".ims.catalog.isa.sap.com";

    public static final String CATALOG_USE_DYN_CONN_PARAM = "useDynConnParam.ims.catalog.isa.sap.com";

    // constants used to connect to IMS server
    public  static final String CATALOG_CONN_IMS_GWHOST = "gwhost.ims.catalog.isa.sap.com";
    public  static final String CATALOG_CONN_IMS_GWSERV = "gwserv.ims.catalog.isa.sap.com";
    public  static final String CATALOG_CONN_IMS_TPHOST = "tphost.ims.catalog.isa.sap.com";
    public  static final String CATALOG_CONN_IMS_TPNAME = "tpname.ims.catalog.isa.sap.com";


    //constants used by IPC client backen objects
    public static final String BO_IPCLIENT_EXTENSION = ".ipc.boi.eai.core.isa.sap.com";

    public  static final String IPCLIENT_CLIENT     = "client.ipc.boi.eai.core.isa.sap.com";
    public  static final String IPCLIENT_HOST       = "host.ipc.boi.eai.core.isa.sap.com";
    public  static final String IPCLIENT_PORT       = "port.ipc.boi.eai.core.isa.sap.com";
    public  static final String IPCLIENT_TYPE       = "type.ipc.boi.eai.core.isa.sap.com";
    public  static final String IPCLIENT_ENCODING   = "encoding.ipc.boi.eai.core.isa.sap.com";
    public  static final String IPCLIENT_DISPATCHER = "dispatcher.ipc.boi.eai.core.isa.sap.com";

    //constants used for eauction integration
    public static final String BO_EAUCTION_ENABLED     = "enable.auction.isa.sap.com";
    public static final String BO_EAUCTION_PROCESSTYPE = "processtype.auction.isa.sap.com";
    
    //constants used for order download
    public static final String BO_ORDERDOWNLOAD_ENABLED     = "enable.orderdownload.isa.sap.com";
    


    // constants used by IsaCore (IC)
    public static final String IC_ENABLE_TEALEAF_CAPTURING = "capturing.core.isa.sap.com";
    public static final String IC_SCENARIO                 = "scenario.core.isa.sap.com";
    public static final String IC_SHOP                     = "shop.core";
    public static final String IC_SHOW_STACK_TRACE         = "showstacktrace.isacore";
	public static final String IC_ENTRY_URL                = "entryurl.isacore";
    public static final String IC_MERGE_IDENTICAL_PRODUCTS = "mergeidenticalproducts";
    public static final String IC_ENABLE_PRICE_ANALYSIS    = "enable.priceAnalysis";
	public static final String IC_PRICE_ANALYSIS_URL       = "url.priceAnalysis";
    public static final String IC_ENABLE_NON_CATALOG_PRODUCTS = "enable.nonCatalogProducts";
	public static final String IC_ITEM_CONFIG_ORDER_VIEW   = "configinfo.order.view";
	public static final String IC_ITEM_CONFIG_ORDER_DETAIL_VIEW  = "configinfo.orderdetail.view";
    public static final String IC_ITEM_CONFIG_CATALOG_VIEW  = "configinfo.catalog.view";
    public static final String IC_INITIAL_NEWPOS           = "initial.nwpos";

    /**
     * This constant is used to define a default backend configuration
     */
    public static final String EAI_CONF_ID = "confid.eai.core.isa.sap.com";

    /**
     * This constant is used to define a default backend data source
     */
    public static final String EAI_CONF_DATA = "confdata.eai.core.isa.sap.com";

    /**
     * Specifies whether it is allowed to download log files remotely. This
     * feature is used in the logging administration area
     */
    public static final String ADMIN_AREA_CONFIG = "adminconfig.core.isa.sap.com";

    /**
     * Specifies the role the user must have to access the XCM area in writeable mode.
     */
    public static final String ADMIN_WRITE_ACCESS_ROLE = "admin.user.writeable";
    
    /**
     * Specifies the role the user must have to access the XCM area in read only mode.
     */
    public static final String ADMIN_READ_ONLY_ACCESS_ROLE = "admin.user.readonly";

    /**
     * Constant for maximal hits in customer search
     */
    public static final String BUPA_SEARCH_MAXHITS = "maxhits.search.action.businesspartner";
    /**
     * constant for personalization enablement context paramter
     */
     public static final String PERSONALIZATION_ENABLED =  "enable.pers.isa.sap.com";

    /**
     * constant for personalization action interceptor context parameter
     */
    public static final String ACTION_INTERCEPTOR = "implclass.ActionInterceptor.isa.sap.com";
    
    /**
     * constant indicating the path to the configuration file
     * of the Extended Configuration Management
     */
	public static final String XCM_PATH = "path.xcm.config.isa.sap.com";        

    /**
     * constant indicating the the default scenario of 
     * the Extended Configuration Management
     */
	public static final String XCM_SCENARIO= "scenario.xcm.config.isa.sap.com";        

	/**
	 * Constant indicating the path to the customer configuration data
	 * when using Extended Configuration Management
	*/
	public static final String XCM_PATH_CUST_CONFIG = "customer.config.path.xcm.config.isa.sap.com";

	/**
	 * Constant indicating the name of the application
	 */
	public static final String APP_NAME = "application.name";
	
	/**
	 * Turns unicode encoding on off
	*/
	public static final String UNICODE_ENCODING = "unicode.encoding.i18n";
	
	/**
	 * Possible value of XCM_PATH_CUST_CONFIG context parameter
	 */
	public static final String XCM_PATH_CUST_CONFIG_VALUE = "[PLEASE MAINTAIN]";	

	/**
	 * Possible value of XCM_PATH_CUST_CONFIG context parameter
	 */
	public static final String XCM_PATH_CUST_CONFIG_VALUE_630 = "%sys.global.dir%/xcm/${com.sap.dc_name}";
	
	/**
	 * Constant to store the current ui include context in the request context. <br>
	 */
	final static public String UI_INCLUDE_CONTEXT =  "context.uiclass.ui.isa.sapmarkets.com";

    /**
     * Constant to store the layout in the request. <br>
     */
    final static public String UI_CURRENT_AREA =  "area.uiclass.ui.isa.sapmarkets.com";

	
	/**
	 *  Default country of ISA application set in <code>ui-config.xml</code> file
	 */
	public  static final String COUNTRY = "country";
	
}


