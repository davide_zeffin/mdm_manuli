/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      14 June 2001

  $Revision: #3 $
  $Date: 2001/06/29 $
*****************************************************************************/
package com.sap.isa.core.cache;

// java imports
import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.digester.Digester;

import com.sap.isa.core.init.Initializable;
import com.sap.isa.core.init.InitializationEnvironment;
import com.sap.isa.core.init.InitializeException;
import com.sap.isa.core.init.StandaloneHandler;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;


/**
 * This class is used to initialize the EAI layer.
 */

public class CacheInitHandler implements Initializable {

    protected static IsaLocation log = IsaLocation.
        getInstance(CacheInitHandler.class.getName());

    private static final String webAppHome = "C:/dev/perforce/sapm/esales/dev/web/shared";
    private StandaloneHandler standaloneHandler;
    private InputStream is;
    private String configFileName;

    private String maxNumObjects;
    private String cleanInterval;

    public static void main(String[] args) {

      CacheInitHandler handler = new CacheInitHandler();

      handler.standaloneHandler = new StandaloneHandler();
      handler.standaloneHandler.setClassNameMessageResources("ISAResources");
      handler.standaloneHandler.setBaseDir(webAppHome);
      handler.standaloneHandler.setInitConfigPath("/WEB-INF/cfg/init-config.xml");
      try {
        handler.standaloneHandler.initialize();
      } catch (Throwable ex) {
        log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.exception", new Object[] { ex.toString() }, ex);
      }
    }

    /**
    * Initialize the implementing component.
    *
    * @param env the calling environment
    * @param props   component specific key/value pairs
    * @exception InitializeException at initialization error
    */
    public void initialize(InitializationEnvironment env, Properties props)
             throws InitializeException {
		final String METHOD = "initialize()";
		log.entering(METHOD);
		
        log.info("system.cache.init.begin");

        is = env.getResourceAsStream(props.getProperty("config-file"));
        try {
          initDigester();
          initCache();
          log.info("system.cache.init.end");
        } catch (Exception ex) {
          log.warn("system.cache.init.failed");
          new InitializeException(ex.toString());
        } finally {
        	log.exiting();
        }
    }

    /**
    * Terminate the component.
    */
    public void terminate() {
      // clean up the cache
      Cache.destroy();
    }

    private void initDigester() throws InitializeException {
		final String METHOD = "initDigester()";
		log.entering(METHOD);
      // new Struts digester
      Digester digester = new Digester();
      digester.setDebug(0);

      digester.push(this);

      // set property of connection definition object
      digester.addSetProperties("cache");

      digester.addCallMethod(
          "cache/regions/region",
          "addRegion", 5, new String[]
              {"java.lang.String", "java.lang.String", "java.lang.String", "java.lang.String", "java.lang.String"});
      // add regions
      digester.addCallParam("cache/regions/region"
          , 0, "name");
      digester.addCallParam("cache/regions/region"
          , 1, "loaderClassName");
      digester.addCallParam("cache/regions/region"
          , 2, "idleTime");
      digester.addCallParam("cache/regions/region"
          , 3, "timeToLive");
      digester.addCallParam("cache/regions/region"
          , 4, "descr");

      try {
        if (is != null)
          digester.parse(is);

        else if (configFileName != null && configFileName.length() > 0) {

          log.info("system.cache.init.readConfigFile", new Object[] { configFileName }, null);

          digester.parse(configFileName);
        } else
          throw new InitializeException("Error reading configuration information\n" +
              "No input stream or file name set");
       } catch (Exception ex) {
        throw new InitializeException("Error reading configuration information\n" + ex.toString() );
      } finally {
      	log.exiting();
      }
    }

  /**
   * Sets file name (and path) of configuration file
   * @param configFileName name (and path) of configuration file
   */
  public void setConfigFileName(String configFileName) {
    this.configFileName = configFileName;
  }

  /**
   * Initializes cache
   * @param cleanInterval
   * @param maxNumObjects
   */
  private void initCache() throws Cache.Exception {
	final String METHOD = "initCache()";
	log.entering(METHOD);
	
    Cache.Config config = new Cache.Config();
    try {
      config.setCleanInterval(Integer.parseInt(cleanInterval));
       // not used yet
      //config.setMaxNumObjects(Integer.parseInt(maxNumObjects));
    } catch (NumberFormatException ex) {
      log.warn("system.cache.init.use.defaultSettings");
      Cache.init();
      log.exiting();
      return; 
    }
    // initialize cache if necessary
	if (!Cache.isReady())
    	Cache.init(config);
    log.exiting();
  }

  public void setCleanInterval (String cleanInterval) {
    this.cleanInterval = cleanInterval;
  }
  public void setMaxNumObjects (String maxNumObjects) {
    this.maxNumObjects = maxNumObjects;
  }

  /**
   *
   * @param name name of region
   * @param loaderClassName name of loader class
   * @param idleTime idleTime
   * @param timeToLive time to live
   * @param description description of region
   * @throws Exception if adding region fails (e.g. region already exists)
   */
  public void addRegion(String name, String loaderClassName, String idleTime, String timeToLive, String description)
      throws Cache.Exception {
    Cache.Attributes attr = new Cache.Attributes();
    try {
      int attr1 = Integer.parseInt(idleTime);
      int attr2 = Integer.parseInt(timeToLive);
      attr.setIdleTime(attr1);
      attr.setTimeToLive(attr2);
      attr.setDescription(description);

    } catch (NumberFormatException ex) {
      log.warn("system.cache.init.fail.addRegion.wrongParams", new Object[] { name }, null);
      return;
    }
    Cache.Loader loader;
    if (loaderClassName != null) {
      try {
        Class loaderClass = Class.forName(loaderClassName);
        loader = (Cache.Loader)loaderClass.newInstance();
        attr.setLoader(loader);
      } catch (Throwable ex) {
        log.warn("system.init.cache.fail.addRegion.wrongParams", new Object[] { name }, ex);
        return;
      }
    }
    try {
      Cache.addRegion(name, attr);
    } catch (Throwable ex) {
        log.warn("system.init.cache.fail.addRegion.wrongParams", new Object[] { name }, ex);
        return;
    }
  }
}