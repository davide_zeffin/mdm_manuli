/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      18 April 2001

  $Revision: #2 $
  $Date: 2001/06/28 $
*****************************************************************************/
package com.sap.isa.core.cache;


/**
 * This class is used as a custom object loader
 */
public class TestLoader extends Cache.Loader{

  TestDataSource testDataSource = new TestDataSource();

  public Object load(Object key, Object attributes) throws Cache.Exception {

    TestDataSource.DataContainer dc;
    String p1 = (String)key;
    int p2 = ((Integer)attributes).intValue();
    dc = testDataSource.getData(p1, p2);

    return dc;
  }

  public TestDataSource getTestDataSource() {
    return testDataSource;
  }
}