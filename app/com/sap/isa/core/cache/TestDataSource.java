/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      18 April 2001

  $Revision: #2 $
  $Date: 2001/06/28 $
*****************************************************************************/
package com.sap.isa.core.cache;

import com.sap.isa.core.logging.IsaLocation;


/**
 * This class is used to simulate a slow datasource
 */
public class TestDataSource {

  private long dataAccessTime = 1000;
  
  /** 
	*  The IsaLocation of the current Action.
	*  This will be instantiated during perform and is always present.
	*/
   protected IsaLocation log = IsaLocation.getInstance(TestDataSource.class.getName());
  

  public TestDataSource() {
  }

  /**
   * Sets time it takes to retrieve data
   * @param accessTime in milliseconds
   */
  public void setDataAccessTime(long milliseconds) {
    dataAccessTime = milliseconds;
  }

  /**
   * Returns a String containing n times the passed string
   * @param string
   * @param n how many times to repeat string
   */
  public DataContainer getData(String string, int n) {
    long start = System.currentTimeMillis();
    StringBuffer sb = new StringBuffer(string);
    int i = 0;
    for (; i < n; i++) {
      sb.append(string);
    }
    long timeRemaining = dataAccessTime - (System.currentTimeMillis() - start);
    if (timeRemaining > 0) {
      try {
        Thread.sleep(timeRemaining);
      } catch (InterruptedException ex) {
      	log.debug(ex.getMessage());
      }
    }
    DataContainer dc = new DataContainer(sb.toString());
    return dc;

  }


  public static class DataContainer {
    String data;

    public DataContainer(String data) {
      this.data = data;
    }

    /**
     * Gets data
     */
    public String getData() {
      return data;
    }
  }

}