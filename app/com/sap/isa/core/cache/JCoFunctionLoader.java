/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      12 June 2001

  $Revision: #3 $
  $Date: 2001/06/28 $
*****************************************************************************/
package com.sap.isa.core.cache;

// JCo imports
import com.sap.mw.jco.JCO;
import com.sap.isa.core.logging.IsaLocation;


/**
 * This class acts as a loader for JCo functions
 */
public class JCoFunctionLoader extends Cache.Loader {

  protected static IsaLocation log = IsaLocation.
        getInstance(JCoFunctionLoader.class.getName());


  /**
   *  This method is used to return a <code>JCO.Function</code> to the
   *  cache. This function is already prepared to be accessed by
   *  several threads
   */
  public Object load(Object key, Object attributes) throws Cache.Exception {
	final String METHOD = "load()";
	log.entering(METHOD);
	if (log.isDebugEnabled())
		log.debug("key="+key+", attributes="+attributes);
    JCoFunctionAttr attr = (JCoFunctionAttr)attributes;
    JCO.Function oldFunc = attr.getJCoFunction();

    JCO.Client client = attr.getJCoClient();

    JCO.Throughput jcoTp = null;

    if (log.isDebugEnabled())
      log.debug("Calling function module '" + oldFunc.getName() +
            "' delegated to JCoFunctionLoader");

    try {
      client.execute(oldFunc);


    } catch (JCO.Exception ex) {
    	log.exiting();
      throw new Cache.Exception (ex.toString());
    }

    JCO.ParameterList impParam = oldFunc.getImportParameterList();
    JCO.ParameterList expParam = oldFunc.getExportParameterList();
    JCO.ParameterList tabParam = oldFunc.getTableParameterList();

    JCO.Function newFunc = JCO.createFunction(
          oldFunc.getName(),
          (impParam == null ? null : (JCO.ParameterList)impParam.clone()),
          (expParam == null ? null : (JCO.ParameterList)expParam.clone()),
          (tabParam == null ? null : (JCO.ParameterList)tabParam.clone()));
	log.exiting();
    return newFunc;
  }

  /**
   *
   */
  public static class JCoFunctionAttr {

    private JCO.Client client;
    private JCO.Function func;

    /**
     * Constructor for a data container used by <code>JCoFunctionLoader</code>
     * @param client JCo client
     * @param func JCo function
     */
    public JCoFunctionAttr(JCO.Client client, JCO.Function func) {
      this.client = client;
      this.func = func;
    }

    /**
     * Gets the JCo client
     * @return JCo client
     */
    public JCO.Client getJCoClient() {
      return client;
    }

    /**
     * Gets the JCo function
     * @return JCo function
     */
    public JCO.Function getJCoFunction() {
      return func;
    }
  }


}