/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      12 April 2001

  $Revision: #2 $
  $Date: 2001/06/28 $
*****************************************************************************/
package com.sap.isa.core.cache;

import com.sap.mw.jco.IRepository;
import com.sap.mw.jco.JCO;

/**
 * This class is used to test the cache capabilites
 */
public class TestCache {

  private long inc = 0;

  public static void main(String[] args) {


//    TestCache testCache = new TestCache();
    //testCache.testDataSource();
    //testCache.testCacheLoader();
 //   testCache.testJCoLoader();
      testSimpleCache();
/*

    try {
      String string1 = new String("Marek");
      String string2 = new String("Barwicki");

      // initializing cache
      Cache.init();

      // test1: default region
      Cache.Access cache1 = Cache.getAccess();
      cache1.put(string1.toString(), string1);
      System.out.println(cache1.get(string1.toString()));

      // test2: custom region
      Cache.addRegion("myRegion");
      Cache.Access cache2 = Cache.getAccess("myRegion");
      cache2.put(string2.toString(), string2);
      System.out.println(cache2.get(string2.toString()));

      // invalidate object
      cache2.invalidate(string2.toString());
      // access to invalidate object
      try {
          System.out.println(cache2.get(string2.toString()));
      } catch (Cache.ObjectNotFoundException onfex) {
        System.out.println("ok: " + onfex.toString());
      }

      // fill cache with mass data
      int i = 0;
      System.out.println("Filling cache with 20000 Integers");
      for (i = 0; i < 20000; i++) {
        cache2.put(String.valueOf(i), new Integer(i));
      }
      System.out.println("Retrieving 20000 Integers");
      long checksum = 0;
      Integer value = null;
      for (i = 0; i < 20000; i++) {
        value = (Integer)cache2.get(String.valueOf(i));
        checksum += value.intValue();
      }
      System.out.println("checksum : " + checksum);

    } catch (Exception ex) {
      System.out.println(ex.toString());
    }*/
  }

  public void testDataSource() {
    TestDataSource ds = new TestDataSource();
    ds.setDataAccessTime(1000);

    for (int i = 0; i < 10; i++) {
      TestDataSource.DataContainer dc = ds.getData(String.valueOf(i), 10);
      System.out.println("data: " + dc.getData());
    }
  }

  public void testCacheLoader() {
    try {

    // initializing cache
    Cache.init();

    TestLoader testLoader = new TestLoader();
    Cache.Attributes attr = new Cache.Attributes();

    attr.setLoader(testLoader);
    attr.setIdleTime(3000);
    attr.setTimeToLive(30);

    Cache.addRegion("myRegion", attr);
    Cache.Access access = Cache.getAccess("myRegion");

    TestDataSource.DataContainer data;
    int i = 0;
    // getting objects using loader
    for (; i < 10; i++) {
      data = (TestDataSource.DataContainer)access.get(String.valueOf(i), new Integer(2));
    }
    i = 0;
    // getting objects from cache
/*    for (; i < 10; i++) {
      data = (TestDataSource.DataContainer)access.get(String.valueOf(i));
      System.out.println(data.getData());
    }
*/


    } catch (Exception ex) {
      System.out.println(ex.toString());
    }
  }

  public void testJCoLoader() {
/*
    try {
      CacheInitHandler.main(null);

      Cache.Access access = Cache.getAccess("JCO");

      // get JCO Function key
      JCO.Function func = getCompCodeFunc();

      JCoCacheHelper jcoHelper = new JCoCacheHelper();

      String key = jcoHelper.getJCoFunctionKey(func);
      System.out.println("JCo function key: " + key);

      // get JCo client which eventually will be used to get an updated
      // version of the function
      JCO.Client client = JCO.createClient("800", "eai1", "iae1", "de", "iwdf9019", "00");

      JCoFunctionLoader.JCoFunctionAttr aJCoCacheAttr =
            new  JCoFunctionLoader.JCoFunctionAttr(client, func);

      // using cache
      long startTime = System.currentTimeMillis();
//     JCO.setTraceLevel(99);
      for (int i = 1; i < 100; i++) {
        JCO.Function newFunc = (JCO.Function)access.get(key, aJCoCacheAttr);
        func.setTableParameterList((JCO.ParameterList)newFunc.getTableParameterList().clone());
      }
      System.out.println("time using cache: " + (System.currentTimeMillis() - startTime));

      // without using cache
      startTime = System.currentTimeMillis();
      for (int i = 1; i < 100; i++) {
        client.execute(func);
      }
//      System.out.println("time without using cache: " + (System.currentTimeMillis() - startTime));

    } catch (Exception ex) {
      System.out.println(ex.toString());
    }*/
  }

  private JCO.Function getCompCodeFunc() {


    JCO.Client client = JCO.createClient("800", "eai1", "iae1", "de", "iwdf9019", "00");
    client.connect();

    IRepository rep = JCO.createRepository("teset", client);

    JCO.Function func = rep.getFunctionTemplate("RFC_FUNCTION_SEARCH").getFunction();
//      JCO.Function func = rep.getFunctionTemplate("BAPI_COMPANYCODE_GETLIST").getFunction();
    func.getImportParameterList().setValue("a*", "FUNCNAME");
    func.getImportParameterList().setValue("*", "GROUPNAME");

    return func;
  }



  private JCO.Function getJCoFunction() {


  JCO.ParameterList importList = new JCO.ParameterList();

    importList.appendValue("param1", JCO.TYPE_CHAR, 255, "This is my first Jayco example.");

    JCO.Structure struct1 = JCO.createStructure("STRUCT1");
    struct1.addInfo("VAL1", JCO.TYPE_CHAR, 20);
    struct1.addInfo("VAL2", JCO.TYPE_CHAR, 20);
    struct1.addInfo("nested", JCO.TYPE_STRUCTURE, 40);

    struct1.setValue("Hallo", "VAL1");
    struct1.setValue("Welt", "VAL2");

    JCO.Structure struct2 = JCO.createStructure("nested");
    struct2.addInfo("V1", JCO.TYPE_CHAR, 20);
    struct2.addInfo("V2", JCO.TYPE_CHAR, 20);

    struct2.setValue("aaaa", "V1");
    struct2.setValue("bbbb", "V2");

    struct1.setValue(struct2, "nested");


    importList.appendValue("param2", struct1);

    return JCO.createFunction("TestFunc", importList, null, null);

  }

  private JCO.Function getCachedFunction (JCO.Function func) {
/*
    JCO.ParameterList oldTableParams = func.getTableParameterList();

    JCO.ParameterList newTableParams = JCO.createParameterList();

    int numTables = oldTableParams.getFieldCount();

    JCoTableWrapper tabWrapper = new JCoTableWrapper("test");

    newTableParams.appendValue(tabWrapper.getName(), tabWrapper);

    for (int i = 0; i < numTables; i++) {

    }*/
   return null;
  }

  public void test() {
    inc++;
  }

  private static void testSimpleCache() {

  // define cache behaviour
  Cache.Attributes attr = new Cache.Attributes();
  // Sets the maximum time the object will remain in the cache without beeing
  // referenced (in minutes)
  attr.setIdleTime(60);
  // Sets the maximum time the object will stay in cache before it gets
  // invalidated (in minutes)
  attr.setTimeToLive(360);

 try {
  // your region has to be added to the cache (your own cache)
  // This region can also be preconfigured in the web-inf/cfg/cache-config.xml file
  Cache.addRegion("myCache", attr);

  // init cache
  Cache.init();

  // The cache is accessed using a Access object
  Cache.Access access = Cache.getAccess("myCache");

  // store an object in the Cache
  access.put("msg", "This is a string");

  // getting object from Cache
  System.out.println(access.get("msg"));

  } catch (Exception ex) {
    System.out.println(ex);
  }

  }


}