
/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      15 April 2001

  $Revision: #5 $
  $Date: 2001/07/26 $
*****************************************************************************/

package com.sap.isa.core.cache;

// java imports
import java.util.Collection;
import java.util.Collections;
import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;

/**
 * This class represents a Cache. It can only be one cache of this type
 * per process
 * The client interface of this cache is based on a specification which is
 * used as a basis for the upcomming jcache specification from SUN.
 * For further details please see JSR 107 of SUN.
 * You can extend this cache by writing your own cache loader. There
 * will be soon a cache loader for JCo Functions.
 */
public class Cache {

    /** object type: Memory */
    public static final byte OBJ_TYPE_MEMORY   = 0x1;
    /** object type: Region */

    public static final byte OBJ_TYPE_REGION   = 0x2;
    /** object type: Group (not used yet) */
    public static final byte OBJ_TYPE_GROUP    = 0x4;


    // default configuration for the cache

    /** default value: clean up the cache every ? min */
    protected static final int DEFAULT_CLEAN_INTERVAL  = 10;
    /** default value: cache can store up to 10000 objects */
    protected static final int DEFAULT_MAX_NUM_OBJECTS = 1000;
    /** default value: maximum size of cache in mega bytes: 20MB */
    protected static final int DEFAULT_CACHE_SIZE      = 20;
    /** default value: idle time in min */
    protected static final int DEFAULT_IDLE_TIME       = 10;
    /** default value: time to live in min */
    protected static final int DEFAULT_TIME_TO_LIVE    = 10;

    /** Name of default region */
    protected static final String DEFAULT_REGION_NAME = "DEFAULT";

    protected static IsaLocation log = IsaLocation.
        getInstance(CacheInitHandler.class.getName());

    // stores configuration informaion of the cache
    private static Config cacheConfig = null;

    private static Map cache;

    private static boolean isReady   = false;

    private static ServiceThread mServiceThread;

    /**
     * Thrown if someone tries to overwrite an existing object in the cache but
     * this is not allowed
     */
    public static class ObjectExistsException extends java.lang.Exception {

        /**
         * Creates a new exception without any message
         */
        public ObjectExistsException() {
            super();
        }

        /**
         * Creates a new exception with a given message
         *
         * @param msg Message to give further information about the exception
         */
        public ObjectExistsException(String msg) {
            super(msg);
        }
    }

    /**
     * Thrown when an argument of a method is wrong
     */
    public static class InvalidArgumentException extends java.lang.Exception {

        /**
         * Creates a new exception without any message
         */
        public InvalidArgumentException() {
            super();
        }

        /**
         * Creates a new exception with a given message
         *
         * @param msg Message to give further information about the exception
         */
        public InvalidArgumentException(String msg) {
            super(msg);
        }
    }


  /**
     * Thrown when something goes wrong within the cache
     */
    public static class Exception extends java.lang.Exception {

        /**
         * Creates a new exception without any message
         */
        public Exception() {
            super();
        }

        /**
         * Creates a new exception with a given message
         *
         * @param msg Message to give further information about the exception
         */
        public Exception(String msg) {
            super(msg);
        }

    }

    /**
     * Each object in the cache has an associated <code>CacheObjectInfo</code>
     * object. This object stores additonal information which is used to
     * managed the cached object in the cache
     */
    private static class ObjectInfo {

      private Object name;
      private int accessesNum;
      private String regionName;
      private byte type;
      // in ms
      private long creationTime = System.currentTimeMillis();
      // in ms
      private long lastAccessTime = System.currentTimeMillis();
      // in ms
      private long expirationTime;

      /**
       * Sets the name of the object
       * @param objectName name of cached object
       */
      public void setObjectName (Object name) {
        this.name = name;
      }

      /**
       * Gets the name of the object
       * @return name of cached object
       */
      public Object getObjectName () {
        return name;
      }

      /**
       * Sets the type of the object
       * @param type type of cached object
       */
      public void setObjectType (byte type) {
        this.type = type;
      }

      /**
       * Gets the type of the object
       * @return type of cached object
       */
      public byte getObjectType () {
        return type;
      }


      /**
       * Increments the number of accesses to the cached object
       */
      public void incNumAccesses() {
        accessesNum++;
        lastAccessTime = System.currentTimeMillis();
      }

      /**
       * Returns the number of accesses to the cached object
       * @return the number of accesses to the cached object
       */
      public int getAccessesNum() {
        return accessesNum;
      }

      /**
       * Returns the time when the object has been accessed
       * @return the time when the object has been accessed
       */
      public long getLastAccessTime() {
        return lastAccessTime;
      }

      /**
       * Sets the time the object will expire
       * @expirationTime the time the obejct will expire
       */
      public void setExpirationTime(long expirationTime) {
        this.expirationTime = expirationTime;
      }

      /**
       * Returns the time the object will expire
       * @return the time the obejct will expire
       */
      public long getExpirationTime() {
        return expirationTime;
      }
      /**
       * Sets name of the region
       * @param regionName name of the region
       */
      public void setRegion(String regionName) {
        this.regionName = regionName;
      }

      /**
       * Gets name of the region
       * @return name of the region
       */
      public String getRegion() {
        return regionName;
      }

      /**
       * @return The time the object was loaded into cache. The time is the number
       * of milliseconds from midnight, January 1, 1970(UTC)
       */
      public long getCreationTime() {
        return creationTime;
      }
    }

    /**
     * This class is used by the cache to create cached object. It should be
     * extended by custom loaders
     */
    public static abstract class Loader {

      /**
       * Loads an Object from a external source into the cache
       * @param key A key identifying the object which has to be loaded
       * @param attributes A set of attributes used to load this Object
       */
      public abstract Object load(Object key, Object attributes) throws Cache.Exception;
    }

    /**
     * Defines Attributes which apply to a cached object.
     */
    public static class Attributes {

      /**
       * Indicates that the object should be moved to disk when it has to be removed
       * from memory because of space limitations. <br>
       * Note: not used yet
       */
      public static final int PASSIVATE = 0x01;

      /**
       * A object can be local or distributed. This flag declares an object
       * as local. By default each object is local<br>
       * Note: the current implementation only supports local objects
       */
      public static final int LOCAL = 0x02;

      /**
       * If this flag is set then the associated objects get deleted when
       * the VM memory runs low.
       */
      public static final int MEMORY_AWARE = 0x04;


      private long timeToLive   = 0;
      private long idleTime     = 0;
      private long creationTime = 0;
      private long flags        = LOCAL;
      private Loader cacheLoader = null;
      private String mDescr = "";

      /**
       * Sets the maximum time the object will stay in cache before it gets
       * invalidated
       * @param timeToLive maximum time the object will stay in cache before it gets
       * invalidated. Time to live is set in minutes.
       */
      public void setTimeToLive(long timeToLive) {
        // convert to milliseconds
        this.timeToLive = timeToLive * 1000 * 60;
      }

      /**
       * Sets a description to
       * @param text
       */
      public void setDescription(String text) {
        mDescr = text;
      }

      /**
       * Gets the description of this attributes
       * @return description
       */
      public String getDescription() {
        return mDescr;
      }

      /**
       * Gets the maximum time the object will stay in cache before it gets
       * invalidated. Time to live is set in minutes.
       * @return maximum time the object will stay in cache before it gets
       * invalidated (in minutes)
       */
      public long getTimeToLive() {
        if (timeToLive == 0)
          return 0;

        return timeToLive / (1000 * 60);
      }
      /**
       * Gets the maximum time the object will stay in cache before it gets
       * invalidated
       * @return maximum time the object will stay in cache before it gets
       * invalidated (in miliseconds)
       */
      public long getTimeToLiveMs() {
        if (timeToLive == 0)
          return 0;

        return timeToLive;
      }


      /**
       * Sets the maximum time the object will remain in the cache without beeing
       * referenced (in minutes)
       * @param idleTime
       */
      public void setIdleTime(long idleTime) {
        // convert to milliseconds
        this.idleTime = idleTime * 1000 * 60;
      }

      /**
       * Gets the maximum time the object will remain in the cache without beeing
       * referenced
       * @return idle time in minutes
       */
      public long getIdleTime() {
        if (idleTime == 0)
          return 0;

        return idleTime / (1000 * 60);
      }

      /**
       * Gets the maximum time the object will remain in the cache without beeing
       * referenced in ms
       * @return idle time in minutes
       */
      public long getIdleTimeMs() {
        if (idleTime == 0)
          return 0;

        return idleTime;
      }


      /**
       * Associates a cache loader for this attribute object
       * @param loader a loader associated with this attribute object
       */
       public void setLoader(Loader loader) {
        this.cacheLoader = loader;
      }

      /**
       * Returns the cache loader for this object
       * @return the cache loader for this object
       */
       public Loader getLoader() {
        return cacheLoader;
      }

      /**
       * Sets flags for this attribute object. The flags can be combined
       * using the 'or' operator
       * @param flags
       */
      public void setFlags(long flags) {
        this.flags = flags;
      }

      /**
       * Gets flags associated with this attribute object.
       * @return flags associated with this attribute object.
       */
      public long getFlags() {
        return flags;
      }
    }

	/**
	 * Interface for cache region listeners 
	 * Listener is notfied when an object is removed from cache
	 * This occours if object is removed manually or deleted by 
	 * the cleanup process
	 *
	 */
	public static interface RemoveObjectListener {	
		
		/**
		 * Method is called when object is removed from cache
		 * @param key key of object
		 * @param object object removed from cache
	 	 *
		 */		
		public void objectRemoveEvent(Object key);
	}

    /**
     * This class represents the client interface to access the cache
     */
    public static class Access {

      private RegionObject regionObject;

	  /**
	   * Adds an remove object listener to this region
	   * @param listener
	   */
	  public void addRemoveObjectListener(RemoveObjectListener listener) {
		this.regionObject.addRemoveObjectListener(listener);
	  }

	  /**
	   * Removes an remove object listener from this region
	   * @param listener
	   */
	  public void removeRemoveObjectListener(RemoveObjectListener listener) {
		this.regionObject.removeRemoveObjectListener(listener);
	  }

      /**
       * This class cannot be instatiated directly.
       * Use <code>Cache.getAccess()</code>
       * @param regionObject the region the <code>Access</code> is used to access
       */
      protected Access (RegionObject regionObject) {
        this.regionObject = regionObject;
      }

      /**
       * Gets an object from the cache
       * @param key name of object which should be retrieved from the cache
       * @return  the object or <code>null</code> if the object could not be found
       *          in the cache
       */
      public Object get(Object key) {
        return regionObject.get(key);
      }

      /**
       * Gets an object from the cache. If the object is not available in the cache
       * and a loader is associated with the object then it will be loader using
       * the loader
       * @param key key of object which should be retrieved from the cache
       * @param parameters parameters used by the cache loader to create the object
       * @return  the object or <code>null</code> if the object could not be found
       *          in the cache
       */
      public Object get(Object key, Object parameters) {
        return regionObject.get(key, parameters);
      }

      /**
       * Returns keys
       */
      public Set getKeys() {
        return regionObject.getKeys();
      }

      /**
       * Puts an memory object into the cache (region object)
       * @param name name of object
       * @param object object which is put into the cache
       * @param ObjectExistsException thrown if object already exists in cache
       * @param Exception throws if something goes wrong while adding object to
       *                  cache
       */
      public synchronized void put(Object name, Object object)
                                throws Exception, ObjectExistsException {
        regionObject.put(name, object);
      }

      /**
       * Marks an object in the cache as invalid. After calling <code>invalidate</code>
       * the object remains in the cache but cannot be longer accessed. An attempt to
       * access this object leads to creation of a new object if there is a loader
       * associated with this object. The object is romoved from the cache if there
       * are no more strong referces referencing this object.
       * @param key the name of the object which has to be invalidated
       * @throws Cache.Exception if invalidation fails
       */
      public synchronized void invalidate(Object key) throws Cache.Exception{
        regionObject.invalidate(key);
      }

      /**
       * Marks all object in the cache for the given region as invalid. After calling <code>invalidate</code>
       * the object remains in the cache but cannot be longer accessed. An attempt to
       * access these object leads to creation of a new objects if there is a loader
       * associated with these objects. The objects are romoved from the cache if there
       * are no more strong referces referencing these object.
       * @param regionName
       * @throws Cache.Exception if invalidation fails
       */
      public synchronized void invalidateAllObjects() throws Cache.Exception{
        regionObject.invalidateAllObjects();
      }

      /**
       * Returns the attributes of this region
       * @return the attributes of this region
       */
      public Cache.Attributes getRegionAttributes() {
        return regionObject.getAttributes();
      }



      /**
       * removes an object in the cache. The cached object cannot be longer used.
       * @param key the name of the object which has to be removed from the cache
       * @throws Cache.Exception if removing object fails
       */
      public synchronized void remove(Object key) throws Cache.Exception{
        regionObject.remove(key);
      }

    /**
     * Returns the default value of the idle time for all objects which are
     * added to the region
     * @param regionName
     * @return the default value of the idle time for all objects which are
     *         added to the region
     * @throws Cache.Exception when region does not exist
     */
    public static long getDefaultIdleTime(String regionName) throws Cache.Exception {

      Attributes attr = getAttributes(regionName);
      if (attr == null)
        new Cache.Exception ("Region '" + regionName + "' does not exist");

      return attr.getIdleTime();
    }

    /**
     * Returns the default value of time-to-live for all objects which are
     * added to the region
     * @param regionName
     * @return the default value of the idle time for all objects which are
     *         added to the region
     * @throws Cache.Exception when region does not exist
     */
    public static long getDefaultTimeToLive(String regionName) throws Cache.Exception {

      Attributes attr = getAttributes(regionName);
      if (attr == null) {
        throw new Cache.Exception ("Region '" + regionName + "' does not exist");
      }
      return attr.getTimeToLive();
    }

    /**
     * Indicates how often an object has been hit
     * @param regionName
     * @param key
     * @return number of hits for the given object
     * @throws Cache.Exception when region or the object does not exist
     */
    public static long getNumAccesses(String regionName, Object key)
            throws Cache.Exception {
      return getObjectInfo(regionName, key).getAccessesNum();
    }

    /**
     * Indicates how often all object in this region have been hit
     * @param regionName
     * @return number of hits for the given object
     * @throws Cache.Exception when region does not exist
     */
    public long getNumAccesses(String regionName) throws Cache.Exception {
        Set keys = regionObject.getKeys();
        long numHits = 0;
        for(Iterator iter = keys.iterator(); iter.hasNext();) {
				Object key = iter.next();
				numHits = numHits + Cache.Access.getNumAccesses(regionName, key);
        }
		return numHits;
    }

    /**
     * Indicates when the object has been accessed for the last time
     * @param regionName
     * @param key
     * @return number of hits for the given region object
     * @throws Cache.Exception when region or the object does not exist
     */
    public static long getLastAccessTime(String regionName, String key)
            throws Cache.Exception {
      return getObjectInfo(regionName, key).getLastAccessTime();
    }
    /**
     * Indicates when the object expires
     * @param regionName
     * @param key
     * @return number of hits for the given region object
     * @throws Cache.Exception when region or the object does not exist
     */
    public static long getExpirationTime(String regionName, String key) throws Cache.Exception {
      return getObjectInfo(regionName, key).getExpirationTime();
    }

    /**
     * Returns number of objects currently stored in the cache
     * @return number of objects currently stored in the cache
     */
    public int getNumObjects() {
      Set keys = regionObject.getKeys();
      if (keys == null)
        return 0;
      else
        return keys.size();
    }


    /**
     * Returns a clone of the Attributes for a given Region
     * @return a clone of the Attributes for a given Region
     */
    private static Cache.Attributes getAttributes(String regionName) {
      Cache.RegionObject ro = (Cache.RegionObject)cache.get(regionName);

      if (ro != null)
        return ro.getAttributes();
      else
        return null;
    }

    /**
     * Returns object info for a given region and key
     * @throws Cache.Exception when there is no region, key or ObjectInfo
     */
    private static ObjectInfo getObjectInfo(String regionName, Object key)
            throws Cache.Exception {

      Cache.RegionObject ro = (Cache.RegionObject)cache.get(regionName);
      if (ro == null)
        new Cache.Exception ("Region '" + regionName + "' does not exist");

      return ro.getObjectInfo(key);
    }
  }


    private static class ServiceThread extends Thread {

      Config cacheConfig;
      private volatile boolean mStopRequested = false;
      private Thread mRunThread;

      /**
       * Sets information on cache configuration
       */
      public ServiceThread(Config cacheConfig) {
        super();
        this.cacheConfig = cacheConfig;
      }

      public void stopRequest() {
        mStopRequested = true;
        if (mRunThread != null)
        {
			mRunThread.interrupt();
			mRunThread=null;
        }
      }

      public void run() {
        mRunThread = Thread.currentThread();
        while(!mStopRequested) {
          try {
            synchronized (cache) {
              RegionObject regionObject = null;
              Iterator iterator = cache.values().iterator();
              while (iterator.hasNext()) {
                regionObject = (Cache.RegionObject)iterator.next();
                regionObject.cleanup();
              }
            }
            // wait for an externally defined period of time
            sleep(cacheConfig.getCleanIntervalMs());
          } catch (InterruptedException iex) {
            if (mStopRequested)
              Thread.currentThread().interrupt(); // reassert
          }
        }
		mRunThread = null;
      }
    }

      /**
       * Static class used to configure the behaviour of the cache
       */
      public static class Config {
        // in ms
        private static long cleanInterval;
        private static int maxNumObjects;
        private static int cacheSize;

      /**
       * Sets the time which indicates how often the cache should be checked
       * for invalidated objects.
       * @param min clean interval in minutes
       */
      public void setCleanInterval(int min) {
        cleanInterval = min * 60 * 1000;
      }

      /**
       * Specifies the maximum number of objects allowed in the memory cache.
       * @param size
       */
      public void setMaxNumObjects(int size) {
        maxNumObjects = size;
      }

      /**
       * Sets the maximum size of the memory cache. <code>Size</code> is in megabytes.<br>
       * Note: not used yet
       * @param size The maximum size of the memory cache.
       *             <code>Size</code> is in megabytes.
       */
      public void setMemoryCacheSize(int size) {
        cacheSize = size;
      }
      /**
       * Gets the time which indicates how often the cache should be checked
       * for objects invalidated by "time to live" or "idle time" attributes
       * @return minutes
       */
      public long getCleanInterval() {
        return cleanInterval / (1000 * 60);
      }

      /**
       * Gets the time which indicates how often the cache should be checked
       * for objects invalidated by "time to live" or "idle time" attributes
       * @return clean interval in milliseconds
       */
      public long getCleanIntervalMs() {
        return cleanInterval;
      }


      /**
       * The maximum number of objects allowed in the memory cache.
       * @return size
       */
      public int getMaxNumObjects() {
        return maxNumObjects;
      }

      /**
       * The maximum size of the memory cache. <code>Size</code> is in megabytes.<br>
       * Note: not used yet
       * @return The maximum size of the memory cache.
       *         <code>Size</code> is in megabytes.
       */
      public int getMemoryCacheSize() {
        return cacheSize;
      }

    /**
     * Indicates how often a region object has been hit.
     * @param regionName
     * @return number of hits for the given region object
     * @throws Cache.Exception when region does not exist
     */
    public static long getNumAccesses(String regionName) throws Cache.Exception {
      Cache.RegionObject ro = (Cache.RegionObject)cache.get(regionName);

      if (ro == null)
        new Cache.Exception ("Region '" + regionName + "' does not exist");

      return ro.getObjectInfo().getAccessesNum();
    }

    }

    /**
     * Returns a <code>CacheAccess</code> object for the default region
     * @return a <code>CacheAccess</code> object for the default region. If
     *         cache is not ready then this method returns <code>null</code>
     * @throws Cache.Exception if cache is not initialized
     *
     */
    public static synchronized Cache.Access getAccess() throws Cache.Exception{

      return getAccess(DEFAULT_REGION_NAME);
    }

    /**
     * Returns a <code>Cache.Access</code> object for the specified region
     * @param regionName name of the region which is associated with the returned
     *        object
     * @return a <code>Cache.Access</code> object for the specified region
     */
    public static synchronized Cache.Access getAccess(String regionName)
            throws Cache.Exception {
      if (!isReady())
        throw new Cache.Exception("Cannot return access for region '" + regionName +
            "'. Cache is not ready");

      if (!cache.containsKey(regionName))
        throw new Cache.Exception("Cannot return access for region '" + regionName +
            "'. Region does not exist. Please create it first");

      return new Access((RegionObject)cache.get(regionName));
    }

    /**
     * Defines a named region in this cache. A region is a namespace in the cache
     * which is used to group objects which should share the same cache. Default
     * attributes are used for this region
     * @param regionName name of region. Should be unique
     */
    public synchronized static void addRegion(String regionName) throws Cache.Exception {

        addRegion(regionName, getDefaultAttributes());
    }

    /**
     * Returns a collection containing all reqion names in the cache.
     * @return collection containing all reqion names in the cache.
     */
    public static Collection getAllRegionNames() {
      return Collections.unmodifiableCollection(cache.keySet());
    }

    /**
     * Defines a named region in this cache. A region is a namespace in the cache
     * which is used to group objects which should share the same cache.
     * @param regionName name of region. Should be unique
     * @param attributes contain default settings which apply to object in this
     *        cache region
     */
    public synchronized static void addRegion(String regionName,
            Cache.Attributes attributes) throws Cache.Exception {
      if (cache != null && cache.containsKey(regionName)) {
        String msg = "region '" + regionName + "' already exists in cache";
        log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.cache.exception", new Object[] { msg }, null);
         throw new Cache.Exception(msg);
      }

      log.info(LogUtil.APPS_BUSINESS_LOGIC,"system.cache.addRegion", new Object[] { regionName });

      // add default region object
      RegionObject regionObject = new RegionObject();
      regionObject.setAttributes(attributes);
      regionObject.setObjectInfo(createObjectInfo(attributes,
                                                  regionName,
                                                  OBJ_TYPE_REGION,
                                                  regionName));
	  if(cache==null)
		 cache = Collections.synchronizedMap(new HashMap());
	  	                                                  
      cache.put(regionName, regionObject);
    }

    /**
     * Initializes the cache. Since there is only one cache per process
     * it makes sense to call this method only once. Uses default settings
     * to initialize the cache
     */
    public static void init() throws Cache.Exception {
      // use default settings for cache configuration
      Config cacheConfig = getDefaultCacheConfig();

      init(cacheConfig);
    }

    /**
     * Initializes the cache. Since there is only one cache per process
     * it makes sense to call this method only once.
     * @param cacheConfig Object encapsulating cache configuration information
     */
    public static synchronized void init(Cache.Config cacheConfig) throws Cache.Exception {

      if (isReady)
        throw new Cache.Exception("Cache already initialized");
      if(cache==null)
	  	 cache = Collections.synchronizedMap(new HashMap());
      Cache.cacheConfig = cacheConfig;

      // add default region object
      RegionObject regionObject = new RegionObject();
      regionObject.setAttributes(getDefaultAttributes());
      regionObject.setObjectInfo(createObjectInfo(getDefaultAttributes(),
                                                  DEFAULT_REGION_NAME,
                                                  OBJ_TYPE_REGION,
                                                  DEFAULT_REGION_NAME));
      cache.put(DEFAULT_REGION_NAME, regionObject);

      isReady = true;

      // initialize and start the service thread
      if (mServiceThread == null) {
        mServiceThread = new ServiceThread(cacheConfig);
        mServiceThread.start();
      }
    }

    /**
     * Cleans up the cache. The service thread is stopped
     */
    public static void destroy() {
    	if (mServiceThread != null)
	      mServiceThread.stopRequest();
    	cacheConfig = null;
    	isReady   = false;
		cache = null;	      
		if(mServiceThread!=null)
		{
			mServiceThread.interrupt();
			mServiceThread=null;
		}
    }

    /**
     * Returns <code>true</code> if the cache has been initialized and not closed
     * @return <code>true</code> if the cache has been initialized and not closed
     */
    public static boolean isReady() {
      return isReady;
    }
    /**
     * Returns default attributes
     * @return a set of default attributes for a cache region
     */
    private static Cache.Attributes getDefaultAttributes() {
      Cache.Attributes attr = new Cache.Attributes();

      attr.setFlags(Attributes.LOCAL | Attributes.MEMORY_AWARE);
      attr.setIdleTime(DEFAULT_IDLE_TIME);
      attr.setTimeToLive(DEFAULT_TIME_TO_LIVE);

      return attr;
    }

    /**
     * Returns default cache config object
     * @return default cache config object
     */
    private static Cache.Config getDefaultCacheConfig() {
      Config cacheConfig = new Config();
      cacheConfig.setCleanInterval(DEFAULT_CLEAN_INTERVAL);
      cacheConfig.setMaxNumObjects(DEFAULT_MAX_NUM_OBJECTS);
      cacheConfig.setMemoryCacheSize(DEFAULT_CACHE_SIZE);
      return cacheConfig;
    }


    /**
     * Returns a meta data object which contains some meta data based on the
     * Attributes
     */
    private static ObjectInfo createObjectInfo(Attributes attr,
                                        Object objectName,
                                        byte type,
                                        String regionName) {
      ObjectInfo objectInfo = new ObjectInfo();

      objectInfo.setObjectName(objectName);
      objectInfo.setObjectType(type);
      objectInfo.setRegion(regionName);

      // convert timeToLive from seconds to milliseconds
      long timeToLive = attr.getTimeToLiveMs();
      if (timeToLive != 0)
        // time to live: current time + timeToLive
        objectInfo.setExpirationTime(System.currentTimeMillis() + timeToLive);

      return objectInfo;
    }


    /**
     * This class represents a namspace in the cache. A region object is a collection
     * which can store other types of objects. Region Objects are manipulated
     * using an <code>Access</code> object
     */
    private static class RegionObject extends CachedObject {

      private Map objects = Collections.synchronizedMap(new HashMap());
      private Set mRemoveObjectListeners = new HashSet();
      
      /**
       * Adds an remove object listener to this region
       * @param listener
       */
      public void addRemoveObjectListener(RemoveObjectListener listener) {
		mRemoveObjectListeners.add(listener);
      }

	  /**
	   * Removes an remove object listener from this region
	   * @param listener
	   */
	  public void removeRemoveObjectListener(RemoveObjectListener listener) {
		mRemoveObjectListeners.remove(listener);
	  }

	  private void notifiyRemoveObjectListeners(Object key) {
	  	for (Iterator iter = mRemoveObjectListeners.iterator(); iter.hasNext(); ) {
	  		RemoveObjectListener listener = (RemoveObjectListener)iter.next();
	  		listener.objectRemoveEvent(key);
	  	}
	  	
	  }

      /**
       * This method is called by the <code>ServiceThread</code>. It invalidates
       * objects which are expired or are idle for a specific time
       */
      public void cleanup() {
        // synchronized necessary because of iterator
        synchronized (objects) {
        log.info("system.cache.invalidate.region",
              new Object[] { getObjectInfo().getObjectName() }, null );
        Iterator iterator = objects.keySet().iterator();
        CachedObject cachedObject = null;
        ObjectInfo objectInfo = null;
        long currentTime = System.currentTimeMillis();
        while (iterator.hasNext()) {
          try {
          	Object key = iterator.next();
            cachedObject = (CachedObject)objects.get(key);
            objectInfo = cachedObject.getObjectInfo();
            // check for expiration time
            if (objectInfo.getExpirationTime() <= currentTime) {
              if (log.isDebugEnabled())
                log.debug("Object has expired: " + objectInfo.getObjectName());
              iterator.remove();
              notifiyRemoveObjectListeners(key);
              continue;
            }
            // check for idle objects
            if ((objectInfo.getLastAccessTime() +
                    cachedObject.getAttributes().getIdleTimeMs()) <= currentTime) {
              if (log.isDebugEnabled())
                log.debug("Object was too long idle: " + objectInfo.getObjectName());
              iterator.remove();
              continue;
            }
          } catch (ConcurrentModificationException ex) {
            log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.cache.exception", new Object[] { ex.toString() }, null);
            break;
          }
        }
      }
    }

      /**
       * Puts an object into the cache (current region object)
       * @param key name of object
       * @param objectName object which is put into the cache
       * @param ObjectExistsException thrown if object already exists in cache
       * @param Exception throws if something goes wrong while adding object to
       *                  cache
       */
      public void put(Object key, Object object)
                                throws Exception, ObjectExistsException {
        //check if object is already in cache
        synchronized (objects) {
          if (objects.containsKey(key))
            throw new ObjectExistsException ("Object named '" + key
                    + "' already exists in cache");
          objects.put(key, createMemoryObject(key, object));
        }
      }

      /**
       * Gets an object from the cache
       * @name key key whose associated value is to be returned from the cache
       */
      public Object get(Object key) {

        CachedObject co = (CachedObject)objects.get(key);

        if (co == null)
          return co;
        // add reference counter of cached object
        co.getObjectInfo().incNumAccesses();

        return co.getObject();
      }

      /**
       * Returns object info for a given key
       */
      public ObjectInfo getObjectInfo(Object key) {
        CachedObject co = (CachedObject)objects.get(key);

        if (co != null)
          return co.getObjectInfo();
        else
          return null;
      }

      /**
       * Gets an object from the cache. If the object is not available in the cache
       * and a loader is associated with the object then it will be loader using
       * the loader
       * @param key key whose associated value is to be returned from the cache
       * @param parameters parameters used by the cache loader to create the object
       * @return
       */
      public Object get(Object key, Object parameters) {

        CachedObject co = (CachedObject)objects.get(key);
        if (co == null) {
          // try to load object using cache loader
          Attributes attr = getAttributes();
          Cache.Loader loader = attr.getLoader();
          Object object;
          try {
            object = loader.load(key, parameters);
          } catch (Cache.Exception ex) {
            return null;
          }
          // add object to cache
          co = createMemoryObject(key, object);
          objects.put(key, co);
        }

        // add reference counter of cached object
        co.getObjectInfo().incNumAccesses();

        return co.getObject();
      }


      /**
       * Invalidates all cached objects
       */
      public void invalidateAllObjects() throws Exception {

       synchronized (objects) {

         Set keys = objects.keySet();
          for(Iterator iter = keys.iterator(); iter.hasNext();) {
            String key = (String)iter.next();
            CachedObject co = (CachedObject)objects.get(key);
            if (co == null)
              // nothing to invalidate
              continue;
            co.setInvalid(true);
            // remove invalid object from cache
            // the object can still be used by objects which hold
            // an reference on it
            iter.remove();
          }
        }
      }

      /**
       * Invalidates a cached object
       * @param objectName name of object which has to be invalidated
       */
      public void invalidate(Object objectName) throws Exception {

        synchronized (objects) {
          CachedObject co = (CachedObject)objects.get(objectName);

          if (co == null)
            // nothing to invalidate
            return;

          co.setInvalid(true);
          // remove invalid object from cache
          // the object can still be used by objects which hold
          // an reference on it
          objects.remove(objectName);
        }
      }

      /**
       * Destroys a cached object
       * @param key name of object which has to be invalidated
       */
      public void remove(Object key) throws Cache.Exception {

        synchronized (objects) {
          CachedObject co = (CachedObject)objects.get(key);

          if (co == null)
            // nothing to destroy
            return;

          objects.remove(key);
          notifiyRemoveObjectListeners(key);
          co = null;
        }
      }

      /**
       * Returns keys
       * @return a collection of key for this region
       */
      public Set getKeys() {
        return objects.keySet();
      }


      /**
       * Create memory object
       */
      public MemoryObject createMemoryObject(Object objectName, Object object) {

        // add memory object
        MemoryObject memoryObject = new MemoryObject();
        // use attributes of this region object as default attributes for memory object
        memoryObject.setAttributes(getAttributes());
        memoryObject.setObjectInfo(
            createObjectInfo(getAttributes(),
                             objectName,
                             OBJ_TYPE_MEMORY,
                             getObjectInfo().getRegion()));
        memoryObject.setObject(object);

        return memoryObject;
      }

   }

   /**
    * This class is used to encapsulate memory objects
    */
   private static class MemoryObject extends CachedObject {
   }

   /**
    * Base class for all objects which are stored in the cache
    */
    private static class CachedObject {
      // object specific meta data needed to manage the object in the cache
      private ObjectInfo objectInfo = new ObjectInfo();
      // the object which is cached
      private Object object;
      // attributes associated with the cached object
      private Attributes attributes;

      private boolean valid = true;

      /**
       * Specifies meta data associated with the object which has to be cached
       * @param objectInfo meta data
       */
      public void setObjectInfo(ObjectInfo objectInfo) {
        this.objectInfo = objectInfo;
      }
      /**
       * Gets meta data associated with the object which has to be cached
       * @param objectInfo meta data
       */
      public ObjectInfo getObjectInfo() {
        return objectInfo;
      }

      /**
       * Sets the object which has to be cached
       * @param object object which has to be cached
       */
      public void setObject(Object object) {
        this.object = object;
      }

      /**
       * Gets reference the object which is cached
       * @return reference the cached object
       */
      public Object getObject() {
        return object;
      }
      /**
       * Sets attributes associated with the cached object
       * @param attributes attributes associated with the cached object
       */
      public void setAttributes(Attributes attributes) {
        this.attributes = attributes;
      }
      /**
       * Gets attributes associated with the cached object
       * @return attributes associated with the cached object
       */
      public Attributes getAttributes() {
        return attributes;
      }

      /**
       * Sets the invalidate status of this cached object
       * @param valid  <code>true</code> or <code>false</code>
       */
      public void setInvalid(boolean valid) {
        this.valid = valid;
      }

     /**
      * Returns information wheater the object has been invalidated
      * @return <code>true</code> if the object is valid, <code>false<code>
      *         if the object has been invalidated
      */
     public boolean isValid() {
      return valid;
    }
  }
}