package com.sap.isa.core.taglib;

import java.io.File;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyTagSupport;

import com.sap.isa.core.SharedConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.ui.BrowserType;
import com.sap.isa.core.ui.ExtendedStyleManager;
import com.sap.isa.core.ui.StyleManager;
import com.sap.isa.core.util.WebUtil;

/**
 * Title:        ESales_BASE
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public class IncludeTag extends  BodyTagSupport  {

  private static final IsaLocation log =
               IsaLocation.getInstance("com.sap.isa.core.taglib");

  private String name = null;

  public IncludeTag() {
  }

  public int doStartTag() throws javax.servlet.jsp.JspException {
      return super.SKIP_BODY;
  }

  public int doEndTag() throws javax.servlet.jsp.JspException {

	  boolean styleDirectionRTL = false;
	  UserSessionData userSessionData = UserSessionData.getUserSessionData(pageContext.getSession());
	  if (userSessionData != null) {
	  	  String parameter = (String)userSessionData.getAttribute(SharedConst.STYLE_DIRECTION);
		  if (parameter != null && parameter.equalsIgnoreCase("true")) {
			  styleDirectionRTL = true;
		  }
	  }
      Iterator iterateList = StyleManager.getStyleResources().iterator();
      String theme = WebUtil.getTheme(pageContext);

      try  {

          JspWriter out = pageContext.getOut();
          File parentFP = null, themeFP = null;
          File fp = null;
          StringBuffer absoluteFilePointer = new StringBuffer(pageContext.getServletContext().getRealPath("/"));
          StringBuffer themeAbsoluteFilePointer = new StringBuffer(pageContext.getServletContext().getRealPath("/"));
          StringBuffer parentPath = new StringBuffer(((HttpServletRequest)pageContext.getRequest()).getContextPath());

          if (!parentPath.toString().endsWith("/"))
              parentPath.append('/');

          StringBuffer themePath = new StringBuffer(parentPath.toString());

          //Check if the themes are required
          if(null != theme && !theme.equals(""))  {
              //absoluteFilePointer.append(StyleManager.DEFAULTTHEMEPATH);
              themeAbsoluteFilePointer.append(theme);
              themeAbsoluteFilePointer.append("/");


              //parentPath.append(StyleManager.DEFAULTTHEMEPATH);
              themePath.append(theme);
              themePath.append("/");
          }
          absoluteFilePointer.append(StyleManager.DEFAULTRESOURCEPATH);
          parentPath.append(StyleManager.DEFAULTRESOURCEPATH);

          parentPath.append(StyleManager.getMimesPath());
          themePath.append(StyleManager.getMimesPath());
          absoluteFilePointer.append(StyleManager.getMimesPath());
          themeAbsoluteFilePointer.append(StyleManager.getMimesPath());
          parentFP = new File(absoluteFilePointer.toString());
          themeFP = new File(themeAbsoluteFilePointer.toString());
          String browser = detectUserAgent((HttpServletRequest)pageContext.getRequest());
          StringBuffer styleSheet;
          parentPath.append("/");
          themePath.append("/");

          log.debug(parentPath);
          if(theme != null && !theme.equals(""))  {
              log.debug("Theme - " + themePath);
          }

          boolean useDefault = false;
          while(iterateList.hasNext())  {
              useDefault = true;
              styleSheet = new StringBuffer();
              styleSheet.append("<link type=\"text/css\" rel=\"STYLESHEET\" href=\"");
              name = (String)iterateList.next();

              if(theme != null)  {
                  useDefault = false;
                  if(checkIfStyleSheetExists(themeFP , browser))  {
                      useDefault = false;
                  }  else  {
                      useDefault = true;
                  }
              }
              if(useDefault && !checkIfStyleSheetExists(parentFP , browser))  {
                  continue;
              }
              if(!useDefault)
                  styleSheet.append(themePath.toString());
              else
                  styleSheet.append(parentPath.toString());
              styleSheet.append(name);
              styleSheet.append(".css");
              styleSheet.append("\">");
              out.write(styleSheet.toString());
          }

		if (styleDirectionRTL) {
			ExtendedStyleManager.writeRTLStyles(pageContext, theme);
		}
				
      }  catch(Exception exc)  {
          log.debug("Unable to include the stylesheet references");
      }
      return super.doEndTag();
  }

  private boolean checkIfStyleSheetExists(File parent , String browser)  {
      boolean exists = false;
      File fp = new File(parent , name + browser + ".css");
      if(null != fp && fp.exists())  {
          name = name + browser;
          exists = true;
      }  else  {
          log.debug("The browser dependent stylesheet file is not found " + parent.getAbsolutePath() + "/" + name+browser + ".css");
          fp = new File(parent , name + ".css");
          exists = true;
          if((null == fp) || (false == fp.exists()))  {
              log.debug("The default stylesheet file is not found " + parent.getAbsolutePath() + "/" + name + ".css");
              exists = false;
          }
      }
      return exists;
  }

  private String detectUserAgent (HttpServletRequest httpServletRequest)  {
      // Try to determine User-Agent
      String userAgent = httpServletRequest.getHeader("User-Agent");
      if (userAgent != null) {
          userAgent = userAgent.toUpperCase();
          String userAgentUP = userAgent.toUpperCase();
          int msie_start_pos = userAgentUP.indexOf("MSIE");
          if (msie_start_pos >= 0) {
              // extract MSIE part (avoids confusion with 'Windows 5.0' part):
              int msie_end_pos = userAgentUP.indexOf(";", msie_start_pos);
              String msie_part = (msie_end_pos >= 0) ?
                  userAgentUP.substring(msie_start_pos, msie_end_pos) :
                  userAgentUP.substring(msie_start_pos);
              if (msie_part.indexOf("5.01") >= 0) {
                  return BrowserType.MSIE5; // 5.01? Differences??
              } else if (msie_part.indexOf("5.0") >= 0) {
                  return BrowserType.MSIE5;
              } else if (msie_part.indexOf("5.5") >= 0) {
                  return BrowserType.MSIE55;
              } else if (msie_part.indexOf("6.0") >= 0) {
                  return BrowserType.MSIE6;
              } else if (msie_part.indexOf("7.0") >= 0) {
			      return BrowserType.MSIE7;
		      }			
              
          }

          //MOZILLA/5.0 (WINDOWS; U; WINDOWS NT 5.0; EN-US; RV:1.0.1) GECKO/20020823 NETSCAPE/7.0
          else if(userAgent.indexOf("NETSCAPE/7.0") >= 0)  {
             return BrowserType.NETSCAPE_7;
          }  else if (userAgent.indexOf("MOZILLA/5.0") >= 0) {
              return BrowserType.NETSCAPE_6;
          } else if (userAgent.indexOf("MOZILLA/4.7") >= 0) {
              // Different for 4.74 vs. 475??
              return BrowserType.NETSCAPE_4;
          }  else if(userAgent.indexOf("Opera/7") >= 0)  {
          	  return BrowserType.OPERA7;
          } else {
              return BrowserType.DEFAULT;
          }
      }
      return BrowserType.DEFAULT;
  }

}