/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Franz Mueller
  Created:      08 Februar 2001
*****************************************************************************/


package com.sap.isa.core.taglib;





/**
 * The Tag which would like to offer a functionallity of
 * Innertags <param name="" value=""> has to implement this interface
*/    


public interface ParameterSupport {

    public void append(String name, String value);
}