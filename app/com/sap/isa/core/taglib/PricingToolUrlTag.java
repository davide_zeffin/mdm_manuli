/*****************************************************************************
	Class:        PricingToolUrlTag
	Copyright (c) 2003, SAP AG, Germany, All rights reserved.
	Author:       SAP
	Created:      July 2003
	Version:      1.0

    $Revision: #3 $
    $DateTime: 2004/02/12 16:06:23 $ (Last changed)
    $Change: 170004 $ (changelist)
******************************************************************************/

package com.sap.isa.core.taglib;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyTagSupport;

import com.sap.isa.core.interaction.InteractionConfig;
import com.sap.isa.core.interaction.InteractionConfigContainer;

/**
 * <p>
 * The tag picks-up the parameter <code>url.priceAnalysis.isa.sap.
 * com</code> of the web.xml file and includes the value to the jsp.
 * <br> <b>Example</b>
 * <pre>
 *      &lt;a href="&lt;isacore:PricingToolUrl/&gt;?server=...
 *</pre>
 *
 * <p>
 *
 * @author SAP
 * @version 1.0
 *
 */
 public class PricingToolUrlTag extends BodyTagSupport {

	private String url = null;


	/**
	 * Create a new instance of the tag handler.
	 */
	public PricingToolUrlTag() {
	}


	/**
	 *
	 * @exception JspException if a JSP exception has occurred
	 */
	public int doStartTag() throws JspException {

		try {
			InteractionConfigContainer icc= com.sap.isa.core.FrameworkConfigManager.Servlet.getInteractionConfig(
				pageContext.getSession());
			InteractionConfig ic= icc.getConfig("ui");
			if( ic != null )
			{
				url= ic.getValue("url.priceAnalysis");
				JspWriter out = pageContext.getOut();
				out.print(url);
			}
		}
		catch (Exception ex) {
			return SKIP_BODY;
		}

		return SKIP_BODY;
	}

}