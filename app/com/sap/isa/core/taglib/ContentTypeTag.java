
/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Franz Mueller
  Created:      08 Februar 2001

  $Revision: #1 $
  $Date: 2001/09/07 $
*****************************************************************************/

package com.sap.isa.core.taglib;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.tagext.TagSupport;

import com.sap.isa.core.RequestProcessor;
import com.sap.isa.core.util.HttpServletRequestFacade;
import com.sap.isa.core.util.WebUtil;


/**
 * Custom tag that sets the contenttype 
 *
 */

public class ContentTypeTag extends TagSupport {

/*
    private static final IsaLocation log =
                 IsaLocation.getInstance("com.sap.isa.core.taglib");
*/

    public static final String VAR_NAME = "encodedRequest";

    /**
     * Construct a new instance of this tag.
     */
    public ContentTypeTag() {
	super();
    }


    /**
     * The mime of the content type
     */
    private String type;



    /**
     * Return the mime-type.
     */
    public String getType() {

	return type;
    }


    /**
     * Set the mime-type.
     *
     * @param type the type-part of the content type
     */
    public void setType(String type) {

	this.type = type.trim();
    }




    /**
     * Release any acquired resources.
     */
    public void release() {

	super.release();

	type    = null;
    }


    /**
     * Process the start tag.
     *
     */
    public int doStartTag() {

       // set the content type
       WebUtil.setContentType(pageContext, type);

       HttpServletRequest request = (HttpServletRequest)pageContext.getRequest();
       String check = (String)request.getAttribute(RequestProcessor.ENCODED_REQ);
       if (check != null) {
          if (check.equals("ENCODE")) {
             request = HttpServletRequestFacade.determinRequest(request);
          }
       }

       
       /*
          Object obj = pageContext.findAttribute(name);
       
          if (obj instanceof HttpServletRequestFacade) {
            ((HttpServletRequestFacade)obj.setHttpServletRequest(request);
       }
       */

       pageContext.setAttribute(VAR_NAME, request);


       // Continue processing this page
       return (SKIP_BODY);
    }
}
