/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Franz Mueller
  Created:      08 Februar 2001
*****************************************************************************/


package com.sap.isa.core.taglib;


import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyTagSupport;

import org.apache.struts.util.MessageResources;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.ui.context.ContextManager;
import com.sap.isa.core.util.WebUtil;


/**
 * This Tag generates a URL identifying the current web application
 *
 * <table>
 * <tr><td cols="1">Attributes</td></tr>
 * <tr><td>name</td><td>required</td><td>default</td><td>values</td></tr>
 * <tr><td>name</td><td>yes</td><td>N/A</td><td>N/A</td></tr>
 * <tr><td>secure</td><td>no</td><td>N/A</td><td>ON|OFF</td></tr>
 * <tr><td>completeUrl</td><td>no</td><td>N/A</td><td>ON|OFF</td></tr>
 * </table>
 *
 * Additaional parameters can be specified through the usage of
 * the nested tag &lt;parameter /&gt;
 * In this case the <strong>value</strong> attribute will
 * be http complient encoded.
 *
 * If SSL support should be supported, the following additional
 * requirements should be met:
 *
 * The following entries are required in the web.xml file:
 *     <code>http.port.isa.core.sap.com</code>
 *           specifies the http - port
 *     <code>https.port.isa.core.sap.com</code>
 *           specifies the https - port
 *
 * After the URL is generated it will ensure that the session
 * id is transfered to the client through a call to
 * <code>HttpServletResponse.encodeURL<code>
*/


public class WebappsURLTag extends BodyTagSupport 
		implements ExtendedParameterSupport {

    private static final IsaLocation log =
                 IsaLocation.getInstance("com.sap.isa.core.taglib");


    public static final String ON  = "ON";
    public static final String OFF = "OFF";

    protected String name;
    protected String anchor;
    protected boolean completeUrl = false;
    protected Boolean secure = null;

    protected StringBuffer parameters;
    protected Map parameterMap;
	protected Map contextValueMap = new HashMap();


    public WebappsURLTag() {
        super();
    }


    public void setSecure(String s) {
        if (s.equalsIgnoreCase(ON)) {
           secure = new Boolean(true);
        } else if (s.equalsIgnoreCase(OFF)){
           secure = new Boolean(false);
        }
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAnchor(String anchor) {
        this.anchor = anchor;
    }

    public void setCompleteUrl(String s) {
        if (s.equalsIgnoreCase(ON)) {
           this.completeUrl = true;
        }
        else {
           this.completeUrl = false;
        }
    }

    public void release() {
        super.release();

        name        = null;
        secure      = null;
        anchor      = null;
        completeUrl = false;
        if (parameterMap != null) {
			parameterMap.clear();
        }
		
		contextValueMap.clear();

        if (parameters != null)
            parameters.setLength(0);
    }


	/**
	 * specified through the interface <code>ParameterSupport</code>
	 *
	 * This method will be called for each nested &lt;parameter&gt; tag.
	 * This method appends the names and values to an
	 * <code>StringBuffer</code> instance.
	 * The single name-value pairs will be separated through the
	 * symbol <strong>&amp;</strong>.
	 *
	 * Each single <strong>value</strong> will be http complient encoded.
	 *
	 * @param name the name of the parameter
	 * @param value the value of the parameter
	 *
	 */
	public void append(String name, String value, boolean isContextValue) {
		
		if (isContextValue == false) {
			append(name, value);
		}
		
		contextValueMap.put(name,value);
	}		


    /**
     * specified through the interface <code>ParameterSupport</code>
     *
     * This method will be called for each nested &lt;parameter&gt; tag.
     * This method appends the names and values to an
     * <code>StringBuffer</code> instance.
     * The single name-value pairs will be separated through the
     * symbol <strong>&amp;</strong>.
     *
     * Each single <strong>value</strong> will be http complient encoded.
     *
     * @param name the name of the parameter
     * @param value the value of the parameter
     *
     */
    public void append(String name, String value) {
        if (parameters == null) {
			parameters = new StringBuffer();
        }
		
		if (parameterMap == null) {
			parameterMap = new HashMap();
		}
		
		parameterMap.put(name,value);

        if (parameters.length() > 0)
           parameters.append(new String("&"));

        parameters.append(name);
        parameters.append('=');
        parameters.append( WebUtil.encodeURL(value) );
    }


    public int doStartTag() {
        return EVAL_BODY_TAG;
    }


    public int doEndTag() throws JspException {

		
      	try {

			List changedContextValues = null;
			List newContextValues = null;

			// Context handling 
			// overrule existing values with given values
			// or add new values to the context
			if (contextValueMap.size() > 0) {
				
				changedContextValues = new ArrayList();
				newContextValues = new ArrayList();
				
				ContextManager contextManager = ContextManager.getManagerFromRequest((HttpServletRequest) pageContext.getRequest());
				
				Iterator iter = contextValueMap.entrySet().iterator();
				while (iter.hasNext()) {
                    Map.Entry element = (Map.Entry)iter.next();
                    
                    String name  = (String)element.getKey();
                    String value = (String)element.getValue();
                    
                    String oldValue = contextManager.getContextValue(name);
					if (oldValue != null) {
						changedContextValues.add(new String[] {name, oldValue});
					}
					else {
						newContextValues.add(name);						
					}
					contextManager.setContextValue(name, value);
                }
				
			}

            String appsUrl =
                    WebUtil.getAppsURL(pageContext, secure, name,
                              (parameters==null?null:parameters.toString()),
                              anchor, completeUrl);

             JspWriter out =  pageContext.getOut();
             out.print(appsUrl);
             
			
			// Context handling
			// reset context to original state 
			
			if (contextValueMap.size() > 0) {

				ContextManager contextManager = ContextManager.getManagerFromRequest((HttpServletRequest) pageContext.getRequest());
				
				Iterator iter = changedContextValues.iterator();
				while (iter.hasNext()) {
                    String[] element = (String[]) iter.next();
                    
					contextManager.setContextValue(element[0], element[1]);
                }
                
				iter = newContextValues.iterator();
				while (iter.hasNext()) {
					contextManager.removeContextValue((String)iter.next());
				}				
			} 
             

         } catch (IOException ex) {
             ex.printStackTrace();

             log.error(LogUtil.APPS_USER_INTERFACE,"system.io", ex);

             MessageResources resources =
                        WebUtil.getResources(pageContext.getServletContext());

         throw new JspException
                (resources.getMessage("system.io", ex.toString()));
         } catch (Exception ex) {
             ex.printStackTrace();
             throw new JspException(ex.getMessage());
         }

         return EVAL_PAGE;
    }




}