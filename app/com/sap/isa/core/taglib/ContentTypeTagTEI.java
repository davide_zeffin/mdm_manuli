/*****************************************************************************
    Class:        IterateTagTEI
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Thomas Smits
    Created:      20.2.2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/06/26 $
*****************************************************************************/

package com.sap.isa.core.taglib;

import javax.servlet.jsp.tagext.TagData;
import javax.servlet.jsp.tagext.TagExtraInfo;
import javax.servlet.jsp.tagext.VariableInfo;


/**
 * Utility class used only by the JSP-container to use <code>IterateTag</code>.
 * This class is not used by the application developer and does not contain
 * any valuable documentation. Its only purpose is to provide some extra info
 * for the JSP-container.
 *
 * @author Thomas Smits
 * @version 1.0
 *
 * @see IterateTag Iterate tag for mor information.
 */
public final class ContentTypeTagTEI extends TagExtraInfo {

    /**
     * Look at the documentation of the superclass for more details.
     */
    public VariableInfo[] getVariableInfo(TagData data) {

       return new VariableInfo[] {
          new VariableInfo(ContentTypeTag.VAR_NAME,
                           //"com.sap.isa.core.util.HttpServletRequestFacade",
                           "javax.servlet.http.HttpServletRequest",
                       true,
                       VariableInfo.AT_END)
       };
    }
}