/*****************************************************************************
    Class:        ReEntryURLTag
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP
    Created:      October 2001

    $Revision: #1 $
    $Date: 2001/08/12 $
*****************************************************************************/

package com.sap.isa.core.taglib;

import java.util.Iterator;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;

import com.sap.isa.core.ContextConst;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.util.StartupParameter;
import com.sap.isa.core.util.WebUtil;

/**
 * <p>
 * The tag insert the entry name of the application.
 * You can maintain an URL in web.xml with the flag <code>ContextConst.IC_ENTRY_URL</code>.
 * If no url is maintained the standard url is used with the parameter given
 * to the application.
 * <p>
 *
 * @author SAP
 * @version 1.0
 */
public class ReEntryURLTag extends WebappsURLTag {


    /**
     * Create a new instance of the tag handler.
     */
    public ReEntryURLTag() {
    }


    /**
     *
     * @exception JspException if a JSP exception has occurred
     */
    public int doEndTag() throws JspException {

        try {
            String parameter = pageContext.getServletContext().getInitParameter(ContextConst.IC_ENTRY_URL);

            JspWriter out = pageContext.getOut();
            if(parameter != null && parameter.length()>0 ) {
                out.print(parameter);
                if(parameters!=null) {
                    out.print("?" + parameters.toString());
                }                
            }
            else {
				UserSessionData userSessionData = UserSessionData.getUserSessionData(pageContext.getSession());

				if(userSessionData != null) {
					StartupParameter startupParameter = (StartupParameter)userSessionData
							.getAttribute(SessionConst.STARTUP_PARAMETER);
					Iterator iter = startupParameter.reentryParameter();
					while (iter.hasNext()) {
						StartupParameter.Parameter element = (StartupParameter.Parameter) iter.next();
						append(element.getName(),element.getValue());
                    }

				}

                String appsUrl = WebUtil.getAppsURL(pageContext, secure, name,
                                                    (parameters==null?null:parameters.toString()),
                                                    anchor, completeUrl);
                                                                    
                out.print(appsUrl);
            }

        }
        catch(Exception ex) {
            return EVAL_PAGE;
        }

        return EVAL_PAGE;

    }

}
