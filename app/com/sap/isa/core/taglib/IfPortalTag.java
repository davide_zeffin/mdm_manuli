/*****************************************************************************
    Class:        IfPortalTag
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP
    Created:      February 2002
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/08/01 $
*****************************************************************************/

package com.sap.isa.core.taglib;

import java.io.IOException;

import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyTagSupport;

import com.sap.isa.core.Constants;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.util.StartupParameter;

/**
 * <p>
 * The tag checks, depending of the applciation is running into a portal, if
 * the body should be displayed.
 * Therefore you can give a value <true> or <false> to check if you run or run not
 * in the portal.
 * <br>
 * <b>Example</b>
 * <pre>
 *   &lt;isacore:ifPortal value = "true" &gt; <br>
 *     &lt;a href="&lt;isa:webappsURL name="b2b/bestseller.do"/&gt /%&gt; <br>
 *   &lt;/isa:ifPortal&gt;
 * </pre>
 *
 * <p>
 *
 * @author SAP
 * @version 1.0
 *
 */
 public class IfPortalTag extends BodyTagSupport {

    private String value = "true";
    private boolean ignoreNull; // Ignore null values in page context

    /**
     * Create a new instance of the tag handler.
     */
    public IfPortalTag() {
    }


	/**
	  * Set the property value
	  *
	  * @param value
	  *
	  */
	 public void setValue(String value) {
		 if (value != null && value.length()>0) {
		 	this.value = value;
		 }
	 }


    /**
     *
     * @exception JspException if a JSP exception has occurred
     */
    public int doStartTag() throws JspException {


		HttpSession session = pageContext.getSession(); 
		
		// check for missing context
		if (session == null) {
		  return SKIP_BODY;
		}	

        // get user session data object
        UserSessionData userSessionData =
                UserSessionData.getUserSessionData(session);

        // check for missing context
        if (userSessionData == null) {
          return SKIP_BODY;
        }

        boolean isPortal = (new Boolean(value)).booleanValue();

        if (isPortal(userSessionData) == isPortal) {
            return EVAL_BODY_TAG;
        }

        return SKIP_BODY;
    }

    /**
     *
     * Nothing to do here
     *
     * @exception JspException if a JSP exception has occurred
     */
    public int doAfterBody() throws JspException {

          return SKIP_BODY;
    }

    /**
     * Clean up after processing this enumeration.
     *
     * @exception JspException if a JSP exception has occurred
     */
    public int doEndTag() throws JspException {

        // Render any previously accumulated body content
        if (bodyContent != null) {
            try {
                JspWriter out = getPreviousOut();
                out.print(bodyContent.getString());
            } catch (IOException e) {
                throw new JspException();
            }
        }
        // Continue processing this page
        return EVAL_PAGE;
    }

    /**
     * Drop the state of this tag handler. This method is called by the
     * container
     * if reuse of tag handlers is implemented.
     */
    public void release() {
        super.release();
        value = "true";
        ignoreNull = false;
    }
    
    /**
     * Returns if the application runs in the portal
     *
     * @para userSessionData
     * @return portal
     *
     */
    protected boolean isPortal(UserSessionData userSessionData) {
		
		StartupParameter startupParameter = (StartupParameter) userSessionData.getAttribute(SessionConst.STARTUP_PARAMETER); 
		    
        String portal = startupParameter.getParameterValue(Constants.PORTAL);
		    
        return portal.equalsIgnoreCase("YES");  	
    }



}