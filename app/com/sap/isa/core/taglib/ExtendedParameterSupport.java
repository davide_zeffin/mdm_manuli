/*****************************************************************************
    Class:        ExtendedParameterSupport
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      29.11.2004
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.core.taglib;

/**
 * A tag which would like to use a functionallity of
 * Inner tags like <param name="" value="" isContextValues> has to implement the 
 * ExtendedParameterSupport interface. <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public interface ExtendedParameterSupport extends ParameterSupport {
	
	/**
	 * Append a parameter or a context value to tag context. <br>
	 * 
	 * @param name name of the parameter
	 * @param value value of the parameter
	 * @param isContextValue <code>true</code> for context values
	 */
	public void append(String name, String value, boolean isContextValue);

}
