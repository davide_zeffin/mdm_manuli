/*****************************************************************************
    Class:        MessageTag
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Thomas Smits
    Created:      20.3.2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/06/26 $
*****************************************************************************/

package com.sap.isa.core.taglib;

import java.io.IOException;
import java.util.Iterator;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.BodyTagSupport;

import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.MessageList;
import com.sap.isa.core.util.MessageListDisplayer;
import com.sap.isa.core.util.MessageListHolder;
import com.sap.isa.core.util.WebUtil;

/**
 * <p>
 * Iterates over the messages belonging to a specified business object.
 * The object containing the messages has to be a subclass of
 * <code>BusinessObjectBase</code>. You are able to provide an instance
 * of <code>MessageList</code> directly to the tag instead of using
 * a <code>BusinessObjectBase</code>.
 * </p>
 * <p>
 * The tag searches for a instance of the object containing the messages
 * in page, request, session and application scope (exactly in this order).<br>
 * The name of the object searched for is given using the <code>name</code>
 * parameter. If the object is found a scripting variable with the name set by
 * the <code>id</code> parameter is brought into the scripting context.<br>
 * <b>Note - </b> This variable is only available between the
 * <code>message</code> tags.
 * <p>
 * The type of the scripting variable is <code>java.lang.String</code>. Using
 * the (optional) <code>type</code> parameter you can specify what kind of
 * you want to see (refer to <code>Message</code>). And the second
 * optional parameter <code>property</code> allows you to specify the
 * property of the business object assciated with the message.
 * </p>
 * <p>
 * <b>Example</b>
 * <pre>
 *   &lt;isa:message  id="description"
 *                 name="&lt;%=BusinessObjectBase.CONTEXT_NAME %&gt;"
 *                 type="&lt;%=Message.Error%&gt;"&gt;
 *     &lt;%=description %&gt;
 *   &lt;/isa:message&gt;
 * </pre>
 *
 * <pre>
 *   &lt;isa:message  id="description"
 *                 name="&lt;%=BusinessObjectBase.CONTEXT_NAME %&gt;"
 *                 type="&lt;%=Message.Error%&gt;"
 *                 property="street"&gt;
 *     &lt;%=description %&gt;
 *   &lt;/isa:message&gt;
 * </pre>
 * <p>
 * If you want the tag to die silently if the object specified by the
 * <code>name</code> parameter is not found, set the optional
 * <code>ignorNull</code> parameter to <code>"on"</code> or <code>"true"</code>.
 * </p>
 * @author Thomas Smits
 * @version 1.0
 *
 * @see com.sap.isa.businessobject.BusinessObjectBase BusinessObjectBase
 */
 public class MessageTag extends BodyTagSupport {
    private String id;
    private String name = null;
    private String property;
    private int type = -1;
    private MessageList msgList;
    private Iterator it;
    private boolean ignoreNull; // Ignore null values in page context


	/**
	 * Get the message list from the page context. <br>
	 * 
	 * @param pageContext
	 * @param name
	 * @return
	 * 
	 * @throws JspException
	 */	
	static public MessageList getMessageListFromContext(PageContext pageContext,
														 String name)
			throws JspException {
    	
		MessageList  msgList = null;
		
		// Try to find the object to get Messages from
		Object obj = pageContext.findAttribute(name);
        
		if (obj != null) { 
			if (obj instanceof MessageListHolder) {
				msgList = ((MessageListHolder) obj).getMessageList();
			}
			else if (obj instanceof MessageList) {
				msgList = (MessageList) obj;
			}
			else {
				throw new JspException("MessageListHolder instance called \"" + name + "\" not found");
			}		
		}
        
		return msgList;
	}




    /**
     * Create a new instance of the tag handler.
     */
    public MessageTag() {
    }

    /**
     * Sets the name of the scripting variable created by this tag to
     * access the messages it iterates over.
     *
     * @param id Name of the scripting variable
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Sets the name of the request, session or application scope bean used
     * to extract messages from over.
     *
     * @param name The name of the bean
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Sets the type of message elements that should be displayed.
     * If you want all messages to be displayed (independend of their
     * type) don't set this property.
     *
     * @param type Type of messages to be displayed by tag
     */
    public void setType(int type) {
        this.type = type;
    }

    /**
     * Sets the property of the business object the message is associated
     * with.
     *
     * @param property Name of property
     */
    public void setProperty(String property) {
        this.property = property;
    }

    /**
     * Indicates whether the tag should ignore objects specified by
     * the <code>name</code> parameter not found in any scope.
     *
     * @param value "true" or "on" tells the tag to ignore <code>null</code>
     *              values, all other values tell the tag to throw an
     *              exception if this happens.
     */
    public void setIgnoreNull(String value) {
        if (value.equalsIgnoreCase("true") ||
                value.equalsIgnoreCase("on")) {
            ignoreNull = true;
        }
        else {
            ignoreNull = false;
        }
    }

    /**
     * Construct an iterator for the specified instance of
     * <em>BusinessObjectBase</em>,
     * and begin looping through the body once per element.
     *
     * @exception JspException if a JSP exception has occurred
     */
    public int doStartTag() throws JspException {

    	if (name!= null) {    		
    		msgList = getMessageListFromContext(pageContext,name);
    	}
    	else {
    		// use message List displayer as a default, when no name is given.
    		msgList = getMessageListFromContext(pageContext,MessageListDisplayer.CONTEXT_NAME);
    		ignoreNull = true;
    	}    	
		
		if (msgList == null) { // obj is null 
		
	        // User wants to ignore unfound objects, die silently
	        if ((ignoreNull)) {
	            return SKIP_BODY;
	        }
	        // Only use instances of BusinessObjectBase for iteration
	        else {
	            throw new JspException("No object called \"" + name + "\" found in context");
	        }
		}


        if (type != -1) {
            if (property != null) {
                msgList = msgList.subList(type, property);
            }
            else {
                msgList = msgList.subList(type);
            }
        }

        if (msgList != null) {
            it = msgList.iterator();

            if (it.hasNext()) {

                Message message = (Message) it.next();

                String description = message.getDescription();

                if ((description != null) && (description.length() != 0)) {
                    // Message contains localized data
                    pageContext.setAttribute(id, description);
                }
                else {
                    // Message contains ressource key
                    String ressourceKey    = message.getResourceKey();
                    String[] ressourceArgs = message.getResourceArgs();
                    pageContext.setAttribute(id,
                            WebUtil.translate(pageContext,
                                              ressourceKey,
                                              ressourceArgs));
                }
            }

            return EVAL_BODY_TAG;
        }
        else {
          return SKIP_BODY;
        }
    }


    /**
     * Make the next message element available and loop, or
     * finish the iterations if there are no more elements.
     *
     * @exception JspException if a JSP exception has occurred
     */
    public int doAfterBody() throws JspException {

        if (it.hasNext()) {
            Message message = (Message) it.next();

            String description = message.getDescription();

            if ((description != null) && (description.length() != 0)) {
                // Message contains localized data
                pageContext.setAttribute(id, description);
            }
            else {
                // Message contains ressource key
                String ressourceKey    = message.getResourceKey();
                String[] ressourceArgs = message.getResourceArgs();
                pageContext.setAttribute(id,
                        WebUtil.translate(pageContext,
                                          ressourceKey,
                                          ressourceArgs));
            }
            return EVAL_BODY_TAG;
        }
        else {
          return SKIP_BODY;
        }
    }

    /**
     * Clean up after processing this enumeration.
     *
     * @exception JspException if a JSP exception has occurred
     */
    public int doEndTag() throws JspException {

        // Render any previously accumulated body content
        if (bodyContent != null) {
            try {
                JspWriter out = getPreviousOut();
                out.print(bodyContent.getString());
            } catch (IOException e) {
                throw new JspException();
            }
        }
        // Continue processing this page
        return EVAL_PAGE;
    }

    /**
     * Drop the state of this tag handler. This method is called by the
     * container
     * if reuse of tag handlers is implemented.
     */
    public void release() {
        super.release();
        id = null;
        name = null;
        property = null;
        type = -1;
        msgList = null;
        it = null;
        ignoreNull = false;
    }
}