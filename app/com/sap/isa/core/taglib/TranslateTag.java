
/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Franz Mueller
  Created:      08 Februar 2001

  $Revision: #1 $
  $Date: 2001/06/26 $
*****************************************************************************/

package com.sap.isa.core.taglib;


import java.io.IOException;
import java.util.MissingResourceException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.struts.util.MessageResources;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.WebUtil;


/**
 * Custom tag that retrieves an internationalized messages string
 * (with optional parametric replacement) from the
 * <code>ActionResources</code> object stored as a context attribute
 * by our associated <code>ActionServlet</code> implementation.
 * There are two kinds of replacements:<br/>
 *  1. Parameter replacement: e.g.  {0}will be replaced by the arg0 parameter<br/>
 *  2. html code replacement: E.g. <0> will be replaced by the html parameter<br/>
 */

public class TranslateTag extends TagSupport {


    private static final IsaLocation log =
                 IsaLocation.getInstance("com.sap.isa.core.taglib");


    /**
     * Construct a new instance of this tag.
     */
    public TranslateTag() {
	super();
    }


    /**
     * The optional argument.
     */
    private String[] args;

    /**
     * The optional html tag argument.
     */
    private String[] htmlArgs;
    
    /**
     * The message key of the message to be retrieved.
     */
    private String key = null;

    /**
     * The language (ISO - 2 letter code) to be used
     */
    private String lang = null;

    private void setHtmlArg(int i, String arg) {
        if (htmlArgs == null)
            htmlArgs = new String[6];

        htmlArgs[i] = arg;
    }


    private void setArgument(int i, String arg) {
        if (args == null)
            args = new String[5];

		args[i] = arg;
    }



    /**
     * Return the args.
     */
    public String[] getArgs() {
    	  return args;
    }

    /**
     * Return the args.
     */
    public String[] getHtmlArgs() {
    	  return htmlArgs;
    }

    /**
     * Set the first optional html argument.
     *
     * @param arg0 The new optional argument
     */
    public void setHtml0(String arg0) {
        setHtmlArg(0, arg0);
    }



    /**
     * Set the second optional html argument.
     *
     * @param arg1 The new optional argument
     */
    public void setHtml1(String arg1) {
        setHtmlArg(1, arg1);
    }


    /**
     * Set the third optional html argument.
     *
     * @param arg2 The new optional argument
     */
    public void setHtml2(String arg2) {
        setHtmlArg(2, arg2);
    }

    /**
     * Set the fourth optional html argument.
     *
     * @param arg3 The new optional argument
     */
    public void setHtml3(String arg3) {
        setHtmlArg(3, arg3);
    }


    /**
     * Set the fifth optional html argument.
     *
     * @param arg4 The new optional argument
     */
    public void setHtml4(String arg4) {
        setHtmlArg(4, arg4);
    }
    /**
     * Set the fifth optional html argument.
     *
     * @param arg4 The new optional argument
     */
    public void setHtml5(String arg5) {
        setHtmlArg(5, arg5);
    }

    /**
     * Set the first optional argument.
     *
     * @param arg0 The new optional argument
     */
    public void setArg0(String arg0) {
       setArgument(0, arg0);
    }



    /**
     * Set the second optional argument.
     *
     * @param arg1 The new optional argument
     */
    public void setArg1(String arg1) {
       setArgument(1, arg1);
    }


    /**
     * Set the third optional argument.
     *
     * @param arg2 The new optional argument
     */
    public void setArg2(String arg2) {
       setArgument(2, arg2);
    }

    /**
     * Set the fourth optional argument.
     *
     * @param arg3 The new optional argument
     */
    public void setArg3(String arg3) {
       setArgument(3, arg3);
    }


    /**
     * Set the fifth optional argument.
     *
     * @param arg4 The new optional argument
     */
    public void setArg4(String arg4) {
       setArgument(4, arg4);
    }

    /**
     * Return the message key.
     */
    public String getKey() {

	return key;
    }


    /**
     * Set the message key.
     *
     * @param key The new message key
     */
    public void setKey(String key) {

	this.key = key;
    }

    /**
     * Return the language key.
     */
    public String getLang() {

	return lang;
    }

    /**
     * Set the language key.
     *
     * @param key The new message key
     */
    public void setLang(String lang) {

	this.lang = lang.toLowerCase();
    }


    /**
     * Release any acquired resources.
     */
    public void release() {

	super.release();

	args = null;
	key  = null;
	lang = null;
    }




    // ------------------------------------------------------ Public Methods


    /**
     * Process the start tag.
     *
     * @exception JspException if a JSP exception has occurred
     */
    public int doStartTag() throws JspException {

	try {
            // do the translation
	    String text = WebUtil.translate(pageContext, key, args, htmlArgs, lang);

	    // Print the retrieved message to our output writer
	    JspWriter writer = pageContext.getOut();
	    
	    if (lang == null)
	       writer.print(text);
	    else 	            
	       writer.print(WebUtil.encodeEntities(text));


	} catch (MissingResourceException ex) {
		log.error(LogUtil.APPS_USER_INTERFACE,"system.io", ex);
		if( log.isDebugEnabled())
			log.debug("Message resource for key " + key + " not found"); 
	    throw new JspException("system.noResource" + ex.getMessage());

	} catch (IOException ex) {

	    log.error(LogUtil.APPS_USER_INTERFACE,"system.io", ex);

        MessageResources resources =
                        WebUtil.getResources(pageContext.getServletContext());
	    throw new JspException
		(resources.getMessage("system.io", ex.toString()));
	}

	// Continue processing this page
	return (SKIP_BODY);

    }


}
