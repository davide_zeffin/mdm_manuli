/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Franz Mueller
  Created:      08 Februar 2001
*****************************************************************************/


package com.sap.isa.core.taglib;

import javax.servlet.jsp.tagext.Tag;
import javax.servlet.jsp.tagext.TagSupport;



/**
 * This Tag can be used as a nested Tag inside of all
 * <strong>isa</strong> Tags, which implements the interface
 * <code>ParameterSupport</code>
 *
 * <table>
 * <tr><td cols="1">Attributes</td></tr>
 * <tr><td>name</td><td>required</td><td>default</td><td>values</td></tr>
 * <tr><td>name</td><td>yes</td><td>N/A</td><td>N/A</td></tr>
 * <tr><td>value</td><td>yes</td><td>N/A</td><td>N/A</td></tr>
 * </table>
 * 
*/    


public class ParameterTag extends TagSupport  {

    private String name;
    private String value;
	private boolean isContextValue = false;


    public void setName(String name) {
        this.name = name;
    }
    public void setValue(String value) {
        this.value = value;
    }

	/**
	 * Set the property isContextValue. <br>
	 * 
	 * @param isContextValue <code>true</code> if the value describes a context
	 *  value.
	 */
	public void setContextValue(String isContextValue) {
		this.isContextValue = isContextValue.equalsIgnoreCase("true");
	}


    public void release() {
    	super.release();
    	
        name  = null;
        value = null;
    }



    public int doStartTag() {

         Tag parentTag = getParent();
             
		if (parentTag instanceof ExtendedParameterSupport) {          	
					((ExtendedParameterSupport)parentTag).append(name, value, isContextValue);
		}
		else if (parentTag instanceof ParameterSupport) {          	
            ((ParameterSupport)parentTag).append(name, value);
         } 
         
         return (SKIP_BODY);
    }
    
}