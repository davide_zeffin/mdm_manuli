/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Thorsten Kampp
  Created:      08 Februar 2001

  $Revision: #1 $
  $Date: 2001/06/26 $
*****************************************************************************/
package com.sap.isa.core.taglib;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import com.sap.isa.core.logging.IsaLocation;

/**
 * Synopsis
 * <isa:log priority="" key=""/>
 * 
 * @deprecated This implementation will be replaced by {@link com.sap.isa.core.taglib.DebugMsgTag}. 
 *             This class will be removed in one of the succeeding releases to CRM 5.2. 
 *
 */
public class LogTag extends TagSupport
 {

    /**
     * The priority
     */
    private String priority = null;

    /**
     * The optional argument.
     */
    private String[] args = null;

    /**
     * The message key of the message to be retrieved.
     */
    private String key = null;

    /**
     * Creates new LogTag
     */
    public LogTag() {
    }

    private void setArgument(int i, String arg) {
        if (args == null) {
            args = new String[5];
        }

	args[i] = arg;
    }

    /**
     * Set the first optional argument.
     *
     * @param arg0 The new optional argument
     */
    public void setArg0(String arg0) {
       setArgument(0, arg0);
    }

    /**
     * Set the second optional argument.
     *
     * @param arg1 The new optional argument
     */
    public void setArg1(String arg1) {
       setArgument(1, arg1);
    }


    /**
     * Set the third optional argument.
     *
     * @param arg2 The new optional argument
     */
    public void setArg2(String arg2) {
       setArgument(2, arg2);
    }

    /**
     * Set the fourth optional argument.
     *
     * @param arg3 The new optional argument
     */
    public void setArg3(String arg3) {
       setArgument(3, arg3);
    }


    /**
     * Set the fifth optional argument.
     *
     * @param arg4 The new optional argument
     */
    public void setArg4(String arg4) {
       setArgument(4, arg4);
    }

    /**
     * Return the message key.
     */
    public String getKey() {
	return key;
    }

    /**
     * Set the message key.
     *
     * @param key The new message key
     */
    public void setKey(String key) {
	this.key = key;
    }

    /**
     * Set the priority.
     *
     * @param pri The new priority
     */
    public void setPriority(String pri) {
        this.priority = pri;
    }

    /**
     * Process the start of this tag.
     *
     * @exception JspException if a JSP exception has occurred
     */
    public int doStartTag() throws JspException {

        IsaLocation log = IsaLocation.getInstance("com.sap.isa.jsp." + pageContext.getServletConfig().getServletName());
        
        if(key!=null && priority != null) {
            if(args != null) {
                log.log(priority, key, args, null);
            } else {
                log.log(priority, key, null, null);
            }
        }
        return EVAL_BODY_INCLUDE;
    }

}
