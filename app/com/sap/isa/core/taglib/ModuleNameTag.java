/*****************************************************************************
    Class:        ModuleNameTag
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      September 2001

    $Revision: #1 $
    $Date: 2001/08/12 $
*****************************************************************************/

package com.sap.isa.core.taglib;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import com.sap.isa.core.SharedConst;
import com.sap.isa.core.UserSessionData;

/**
 * <p>
 * The tag displays the name of the module, if the flag <code>ContextConst.IC_SHOW_MODULE_NAME</code>
 * is set in the web.xml.
 *
 * <p>
 *
 * @author Wolfgang Sattler
 * @version 1.0
 */
 public class ModuleNameTag extends TagSupport {

    private String name ;

    /**
     * Create a new instance of the tag handler.
     */
    public ModuleNameTag() {
    }



    /**
     * Set the property name
     *
     * @param name
     *
     */
    public void setName(String name) {
        this.name = name;
    }


    /**
     * Returns the property name
     *
     * @return name
     *
     */
    public String getName() {
       return this.name;
    }



    /**
     *
     * @exception JspException if a JSP exception has occurred
     */
    public int doStartTag() throws JspException {

        try {
            // get user session data object
            UserSessionData userSessionData =
                    UserSessionData.getUserSessionData(pageContext.getSession());

            // check for missing context
            if (userSessionData == null) {
              return SKIP_BODY;
            }

            String parameter = (String)userSessionData.getAttribute(SharedConst.SHOW_MODULE_NAME);

            if (parameter != null && parameter.equalsIgnoreCase("true")) {
                JspWriter out = pageContext.getOut();
                out.print(name);
            }

        }
        catch (Exception ex) {
            return SKIP_BODY;
        }

        return SKIP_BODY;

    }

}
