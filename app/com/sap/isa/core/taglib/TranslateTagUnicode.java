
/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Franz Mueller
  Created:      08 Februar 2001

  $Revision: #1 $
  $Date: 2001/06/26 $
*****************************************************************************/

package com.sap.isa.core.taglib;


import java.io.IOException;
import java.util.MissingResourceException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.struts.util.MessageResources;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.WebUtil;


/**
 * Custom tag that retrieves an internationalized messages string
 * (with optional parametric replacement) from the
 * <code>ActionResources</code> object stored as a context attribute
 * by our associated <code>ActionServlet</code> implementation. The
 * difference between {@link TranslateTag} is that the returned 
 * String has instead international characters unicode.
 * Example:
 *    Instead of ä would be the result \u00e4 
 *
 */

public class TranslateTagUnicode extends TagSupport {


    private static final IsaLocation log =
                 IsaLocation.getInstance("com.sap.isa.core.taglib");


    /**
     * Construct a new instance of this tag.
     */
    public TranslateTagUnicode() {
	super();
    }


    /**
     * The optional argument.
     */
    private String[] args;


    /**
     * The message key of the message to be retrieved.
     */
    private String key = null;

    private void setArgument(int i, String arg) {
        if (args == null)
            args = new String[5];

		args[i] = arg;
    }



    /**
     * Return the args.
     */
    public String[] getArgs() {
    	  return args;
    }


    /**
     * Set the first optional argument.
     *
     * @param arg0 The new optional argument
     */
    public void setArg0(String arg0) {
       setArgument(0, arg0);
    }



    /**
     * Set the second optional argument.
     *
     * @param arg1 The new optional argument
     */
    public void setArg1(String arg1) {
       setArgument(1, arg1);
    }


    /**
     * Set the third optional argument.
     *
     * @param arg2 The new optional argument
     */
    public void setArg2(String arg2) {
       setArgument(2, arg2);
    }

    /**
     * Set the fourth optional argument.
     *
     * @param arg3 The new optional argument
     */
    public void setArg3(String arg3) {
       setArgument(3, arg3);
    }


    /**
     * Set the fifth optional argument.
     *
     * @param arg4 The new optional argument
     */
    public void setArg4(String arg4) {
       setArgument(4, arg4);
    }


    /**
     * Return the message key.
     */
    public String getKey() {

	return key;
    }


    /**
     * Set the message key.
     *
     * @param key The new message key
     */
    public void setKey(String key) {

	this.key = key;
    }

    /**
     * Release any acquired resources.
     */
    public void release() {

	super.release();

	args = null;
	key  = null;
    }




    // ------------------------------------------------------ Public Methods


    /**
     * Process the start tag.
     *
     * @exception JspException if a JSP exception has occurred
     */
    public int doStartTag() throws JspException {

	try {
            // do the translation
	    String text = WebUtil.translate(pageContext, key, args);

	    // convert the text in unicode
	    text = WebUtil.toUnicodeEscapeString(text);
	    
	    // we need to replace the ' in the text.
	    text = text.replaceAll("\"", "\\\\u0022");
	    // we need to replace the " in the text.
	    text = text.replaceAll("'", "\\\\u0027");
	    
	    // Print the retrieved message to our output writer
	    JspWriter writer = pageContext.getOut();
	    
       writer.print(text);


	} catch (MissingResourceException ex) {
		log.error(LogUtil.APPS_USER_INTERFACE,"system.io", ex);
		if( log.isDebugEnabled())
			log.debug("Message resource for key " + key + " not found"); 
	    throw new JspException("system.noResource" + ex.getMessage());

	} catch (IOException ex) {

	    log.error(LogUtil.APPS_USER_INTERFACE,"system.io", ex);

        MessageResources resources =
                        WebUtil.getResources(pageContext.getServletContext());
	    throw new JspException
		(resources.getMessage("system.io", ex.toString()));
	}

	// Continue processing this page
	return (SKIP_BODY);

    }


}
