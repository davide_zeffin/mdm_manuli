/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Franz Mueller
  Created:      08 Februar 2001
  
  $Revision: #2 $
  $Date: 2001/06/29 $
*****************************************************************************/


package com.sap.isa.core.taglib;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.struts.util.MessageResources;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.WebUtil;


/**
 * This Tag builds a full qualified HREF to a resource.
 *
 * <table>
 * <tr><td cols="1">Attributes</td></tr>
 * <tr><td>name</td><td>required</td><td>default</td><td>values</td></tr>
 * <tr><td>name</td><td>yes</td><td>N/A</td><td>N/A</td></tr>
 * <tr><td>theme</td><td>no</td><td>from web.xml</td><td>N/A</td></tr>
 * <tr><td>language</td><td>no</td><td>N/A</td><td>N/A</td></tr>
 * </table>
 *
 * Requirements:
 *     The following entries are required in the web.xml file:
 *      -  <code>theme.isa.core.sap.com</code>
 *           default theme. 
 *      -  <code>mimes.isa.core.sap.com</code>
 *           referenz to the mimes server
 *
 *  The pattern is the following:
 *    %theme.isa.core.sap.com%/%theme%/%language%/%name%
 * 
 *  The <code>theme.isa.core.sap.com</code> is expected as an
 *        initial parameter in the application
 *
 *  <code>theme</code> is an optional attribut of this Tag.
 *  If it is not explicitely set then the implementation tries
 *  to determin the <code>theme</code> 
 *   -from the session-scope under the attribute name
 *   <code>theme.isa.core.sap.com</code> 
 *  and if not found there from
 *   -the initial parameter under the key
 *   <code>theme.isa.core.sap.com</code>.
 * 
 *  If the attribute <strong>language</strong> is used,
 *  its' value will be concatenated just befor the value of 
 *  <strong>name</strong>.
 *
*/    


public class MimeURLTag extends TagSupport  {

    private static final IsaLocation log =
                 IsaLocation.getInstance("com.sap.isa.core.taglib");

    private String _name;
    private String _theme;
    private String _language;


    public void setName(String name) {
        _name = name;
    }
    public void setTheme(String theme) {
        _theme = theme;
    }
    public void setLanguage(String language) {
        _language = language;
    }

    public void release() {
    	super.release();
    	
        _name     = null;
        _theme    = null;
        _language = null;
    }



    public int doStartTag() throws JspException {

         try {
             String theme = (_theme != null)?_theme:WebUtil.getTheme(pageContext);
                                       
             String s = WebUtil.getMimeURL(pageContext, theme, _language, _name);
               
             JspWriter out = pageContext.getOut();         
	     out.print(s);
         } catch (IOException ex) {

	    log.error(LogUtil.APPS_USER_INTERFACE,"system.io", ex);

            MessageResources resources =
                        WebUtil.getResources(pageContext.getServletContext());
	    throw new JspException
		(resources.getMessage("system.io", ex.toString()));
         }

         return (SKIP_BODY);
    }
}