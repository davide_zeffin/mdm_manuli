/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Franz Mueller
  Created:      08 Februar 2001
*****************************************************************************/


package com.sap.isa.core.taglib;


import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.struts.util.MessageResources;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.WebUtil;


/**
 * This Tag encodes, http compliant, the value of the attribute
 * <strong>text</strong>.
 *
 * <table>
 * <tr><td cols="1">Attributes</td></tr>
 * <tr><td>name</td><td>required</td><td>default</td><td>values</td></tr>
 * <tr><td>text</td><td>yes</td><td>N/A</td><td>N/A</td></tr>
 * </table>
 *
*/


public class EncodeURLTag extends TagSupport {

    private static final IsaLocation log =
                  IsaLocation.getInstance("com.sap.isa.core.taglib");
    private String text;

    public EncodeURLTag() {
	super();
    }

    public void setText(String text) {
    	this.text = text;
    }

    public void release() {
    	super.release();

    	text = null;
    }

    public int doStartTag() {
    	return SKIP_BODY;
    }


    public int doEndTag() throws JspException {

         try {
             String encText = WebUtil.encodeURL(text, (HttpServletRequest)pageContext.getRequest());

             JspWriter out = pageContext.getOut();
             out.print(encText);
         } catch (IOException ex) {
	    	 log.error(LogUtil.APPS_USER_INTERFACE,"system.io", ex);

        	 MessageResources resources =
                        WebUtil.getResources(pageContext.getServletContext());

	    	 throw new JspException
				(resources.getMessage("system.io", ex.toString()));
         }

         return (EVAL_PAGE);
    }

}