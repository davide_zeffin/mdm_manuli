/*
 * @(#)LogCategoryTag.java
 * Created on 4. Februar 2001, 13:06
 *
 * Copyright (c) 2000 SAP AG & SAPMarkets, Inc.
 */

/*
|<---            this code is formatted to fit into 80 columns             --->|
|<---            this code is formatted to fit into 80 columns             --->|
|<---            this code is formatted to fit into 80 columns             --->|
*/

package com.sap.isa.core.taglib;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import com.sap.isa.core.SessionConst;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.JspPathMapping;

/**
 *
 * @author  Thorsten Kampp, d019905
 * @version 0.1
 * @deprecated This implementation will be replaced by {@link com.sap.isa.core.taglib.DebugMsgTag}. 
 *             This class will be removed in one of the succeeding releases to CRM 5.2. 
 */
public class LogCategoryTag extends TagSupport {

    protected String  priority = null;
    protected String  name     = null;

    /** Creates new LoggerTag */
    public LogCategoryTag() {
    }

    public void setPriority(String pri) {
        this.priority = pri;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * Process the start of this tag.
     *
     * @exception JspException if a JSP exception has occurred
     */
    public int doStartTag() throws JspException {
        IsaLocation log = null;

        if(name==null) {
            HttpServletRequest req = (HttpServletRequest)pageContext.getRequest();
            name = JspPathMapping.getCategoryNameForURI(req.getServletPath());

            if(name==null) {
              // no mapping defined, thus we won't log at all!
              return EVAL_BODY_INCLUDE;
            }

            log = IsaLocation.getInstance(name);
        } else {
            // get a defined category
            log = IsaLocation.getInstance(name);
        }

        if(priority!=null) {
            log.setPriority(priority);
        }

        pageContext.setAttribute(SessionConst.LOG_CATEGORY, (Object)log);
        return EVAL_BODY_INCLUDE;
    }

}
