/*****************************************************************************
	Class:        StyleSheetsTag
	Copyright (c) 2004, SAPMarkets Europe GmbH, Germany, All rights reserved.
	Author:       SAP
	Created:      March 2004
	Version:      1.0

	$Revision: #1 $
	$Date: 2001/08/01 $
*****************************************************************************/

package com.sap.isa.core.taglib;

import javax.servlet.jsp.tagext.BodyTagSupport;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.ui.ExtendedStyleManager;

// TODO integration of the favicon!!
/**
 * The tag generate the link tags for the stylesheet files, which should be used 
 * in the page. <br> 
 * See {@link com.sap.isa.core.ui.ExtendedStyleManager} for details. 
 * 
 * @author SAP
 * @version 1.0
 */
public class StyleSheetsTag extends  BodyTagSupport  {

	protected static final IsaLocation log = IsaLocation.getInstance("com.sap.isa.core.taglib");
	
	private String theme;
	
	private String group; 
	
	public StyleSheetsTag() {
	}
	
	/**
	 * <p>Set the property {@link #group}. </p>
	 * 
	 * @param group The {@link #group} to set.
	 */
	public void setGroup(String group) {
		this.group = group;
	}

	
	public void setTheme(String theme) {
	    this.theme = theme;
	}
	
	public void release() {
	    super.release();
	
	    theme = null;
	}
	
	public int doStartTag() throws javax.servlet.jsp.JspException {
	    return super.SKIP_BODY;
	}
	
	public int doEndTag() throws javax.servlet.jsp.JspException {

		ExtendedStyleManager.includeStyles(pageContext,theme,group);
		
	    return super.doEndTag();
	}

}