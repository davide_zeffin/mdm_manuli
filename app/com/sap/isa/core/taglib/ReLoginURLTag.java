/*****************************************************************************
    Class:        ReLoginURLTag
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP
    Created:      June 2003

    $Revision: #1 $
    $Date: 2001/08/12 $
*****************************************************************************/

package com.sap.isa.core.taglib;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyTagSupport;

import com.sap.isa.core.ContextConst;
import com.sap.isa.core.init.InitializationHandler;
import com.sap.isa.core.util.WebUtil;

/**
 * <p>
 * The tag insert the url to re login after you have lost your session. 
 * You can maintain an URL in web. xml with the flag <code>ContextConst.
 * RELOGIN_URL</code>. 
 * <p>
 *
 * @author SAP
 * @version 1.0
 */
public class ReLoginURLTag extends BodyTagSupport {


    /**
     * Create a new instance of the tag handler.
     */
    public ReLoginURLTag() {
    }


    /**
     *
     * @exception JspException if a JSP exception has occurred
     */
    public int doEndTag() throws JspException {

        try {
			String parameter = WebUtil.getApplicationConfigParameter(pageContext.getServletContext(), 
				InitializationHandler.getApplicationName()+"config", 
				ContextConst.RELOGIN_URL, 
				ContextConst.RELOGIN_URL+ ".isa.sap.com");

            JspWriter out = pageContext.getOut();
            if(parameter != null && parameter.length()>0 ) {
                out.print(parameter);
            }

        }
        catch(Exception ex) {
            return EVAL_PAGE;
        }

        return EVAL_PAGE;

    }

}
