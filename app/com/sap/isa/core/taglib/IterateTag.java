/*****************************************************************************
    Class:        IterateTag
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Thomas Smits
    Created:      20.2.2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/06/26 $
*****************************************************************************/

package com.sap.isa.core.taglib;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Iterator;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyTagSupport;

import com.sap.isa.core.Iterable;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;

/**
 * <p>
 * Iterates over a given collection of elements. The object that contains
 * the elements to iterate has to implement <code>Iterable</code> or has to
 * be one of the standard Java collection classes (<code>List</code>,
 * <code>Set</code>), a standard Java collection iterator
 * (<code>Iterator</code>), a JDBC <code>ResultSet</code> or any one dimensional
 * Java language array.
 * </p>
 * <p>
 * Because of the internal Structure of an <code>ResultSet</code> the
 * cursor maintained by this class may not be on the first element
 * when the iterate tag is used. To force the tag to move the cursor back
 * to the first element, set the <code>resetCursor</code> parameter of the
 * tag to <code>"true"</code>. This will cause a call to the <code>first()</code>
 * method of the ResultSet. Please note that not all JDBC based result sets
 * support this method. The <code>ResultData</code> implementation of
 * the <code>ResultSet</code> interface is guaranteed to support the
 * <code>first</code> method.<br>
 * <b>Note - </b>A <code>SQLException</code> during a call to the
 * <code>first()</code> method is catched and ignored.
 * </p>
 * <p>
 * The use of this tag is quite simple, it searches for a instance of the
 * object to iterate over in page, request, session and application scope
 * (exactly in this order).<br>
 * The name of the object searched for is given using the <code>name</code>
 * parameter. If the object is found a scripting variable with the name set by
 * the <code>id</code> parameter is brought into the scripting context.<br>
 * <b>Note - </b> This variable is only available between the
 * <code>iterate</code> tags.
 * <p>
 * To set the type of the scripting variable correct (this prevents a lot
 * of casts) the <code>type</code> parameter is used. The given type and the
 * objects returned by the iterator must be compatible, otherwise the tag
 * will throw a JspException.
 * </p>
 * <p>
 * It is recommended to use the <code>ResultData</code> class for modelling
 * in memory result sets and to specifiy exactly this type in the
 * <code>type</code> parameter of the tag. The advantage of this approach is
 * that you don't have to write try/catch statements - the in memory
 * representation can hardly throw any <code>SQLExceptions.</code>
 * </p>
 * <p>
 * <b>Example: iterating over an implementor of Iterable</b>
 * <pre>
 *   &lt;table border="1"&gt;
 *     <b>&lt;isa:iterate id="soldto"
 *                  name="&lt;%= Application.RK_SOLD_TO_LIST %&gt;"
 *                  type="com.sap.isa.businessobject.SoldTo"&gt;</b>
 *       &lt;tr&gt;
 *         &lt;td&gt;<b>&lt;%= soldto.getName() %&gt;</b>&lt;/td&gt;
 *       &lt;/tr&gt;
 *     <b>&lt;/isa:iterate&gt;</b>
 *   &lt;/table&gt;
 * </pre>
 * </p>
 * <p>
 * <p>
 * This example shows how to use the iterate tag to create a table.
 * A request scope bean with
 * the name found in the constant <code>Application.RK_SOLD_TO_LIST</code>
 * is used and a scripting variable named <code>soldto</code> is created.
 * The type of the scriping variable is set to
 * <code>com.sap.isa.businessobject.SoldTo</code>. This variable can
 * be used in the code embraced by the iterate tag.
 * </p>
 * <b>Example: iterating over an array of String</b>
 * <pre>
 * &lt;%
 *    request.setAttribute("animalarray",new String[] { "Dog", "Mouse", "Horse", "Bigfoot" });
 * %&gt;
 *
 * &lt;isa:iterate id="animal"
 *     name="animalarray"
 *     type="java.lang.String"&gt;
 *     &lt;%= animal %&gt;&lt;br&gt;
 *  &lt;/isa:iterate&gt;
 * </pre>
 * </p>
 * <p>
 * If you want the tag to die silently if the object specified by the
 * <code>name</code> parameter is not found, set the optional
 * <code>ignorNull</code> parameter to <code>"on"</code> or <code>"true"</code>.
 * </p>
 * <p>
 * To reset a <code>ResultSet</code> to the first element, set the
 * <code>resetCursor</code> parameter to <code>"true"</code> or
 * <code>"on"</code>. The default value is <code>"false"</code>.
 * For all other types of iterable objects, this setting has no meaning.
 * </p>
 *
 * @author Thomas Smits
 * @version 1.0
 *
 * @see com.sap.isa.core.Iterable Iterable
 */
public class IterateTag extends BodyTagSupport {

    private Iterable list;  // Object to iterate over
    private String name;    // Name of object that contains data
    private String id;      // Scripting variable to create
    private String type;    // Type of scripting variable
    private Iterator it;    // Iterator
    private boolean ignoreNull; // Ignore null values in page context
    private boolean resetCursor = true; // reset the cursor of a ResultSet

	protected static IsaLocation log = IsaLocation.
		   getInstance(IterateTag.class.getName());

    // Utility class to iterate over ResultSets
    private class ResultSetIterator implements Iterator {

        private ResultSet rs;

        public ResultSetIterator(ResultSet rs) {
            this.rs = rs;
        }

        public boolean hasNext() {
            try {
                return rs.next();
            }
            catch (Exception e) {
				log.debug(e.getMessage());
                return false;
            }
        }

        public Object next() {
            return (Object) rs;
        }

        public void remove() {
        }
    }

    // Utility class to iterate over normal arrays
    private class ArrayIterator implements Iterator {
        private Object[] array;
        private int position;
        private int numElements;

        public ArrayIterator(Object[] array) {
            this.array = array;
            numElements = array.length;
        }

        public boolean hasNext() {
            return position < numElements;
        }

        public Object next() {
            return array[position++];
        }

        public void remove() {
        }
    }
    /**
     * Creates a new instance of the tag handler.
     */
    public IterateTag() {
    }

    /**
     * Sets the name of the scripting variable created by this tag to
     * access the iterating data.
     *
     * @param id Name of the scripting variable
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Gets the name of the scripting variable created by this tag to
     * access the iterating data.
     *
     * @return Name of the scripting variable
     */
    public String getId() {
        return id;
    }

    /**
     * Indicates whether the tag should ignore objects specified by
     * the <code>name</code> parameter not found in any scope.
     *
     * @param value "true" or "on" tells the tag to ignore <code>null</code>
     *              values, all other values tell the tag to throw an
     *              exception if this happens.
     */
    public void setIgnoreNull(String value) {
        if (value.equalsIgnoreCase("true") ||
                value.equalsIgnoreCase("on")) {
            ignoreNull = true;
        }
        else {
            ignoreNull = false;
        }
    }

    /**
     * Indicates whether the tag should reset the cursor of a
     * <code>ResultSet</code> or not.
     *
     * @param value "true" or "on" tells the tag to reset the
     *              cursor, all other values tell the tag to use the
     *              <code>ResultSet</code> as is.
     */
    public void setResetCursor(String value) {
        if (value.equalsIgnoreCase("true") ||
                value.equalsIgnoreCase("on")) {
            resetCursor = true;
        }
        else {
            resetCursor = false;
        }
    }

    /**
     * Sets the type of the variable created for the scripting context.
     *
     * @param type Full qualified class name for the type of the scripting variable
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Reads the type of the scripting variable
     *
     * @return Full qualified class name for the type of the scripting variable
     */
    public String getType() {
        return type;
    }


    /**
     * Sets the name of the request, session or application scope bean used
     * to iterate over.
     *
     * @param name The name of the bean
     */
    public void setName(String name) {
        this.name = name;
    }


    /**
     * Construct an iterator for the specified instance of <em>Iterable</em>,
     * and begin looping through the body once per element.
     *
     * @exception JspException if a JSP exception has occurred
     */
    public int doStartTag() throws JspException {

        Object obj = null;   // Reference to iterable object

        // Try to find the object to iterate over
        // in the page context
        obj = pageContext.findAttribute(name);

        // If search wasn't successful, try to find it in the
        // UserSessionData object. This object stores the data in session
        // context but adds the session Id to the attribute name.
        if (obj == null) {
            UserSessionData userSessionData =
                    UserSessionData.getUserSessionData(pageContext.getSession());

            obj = userSessionData.getAttribute(name);
        }

        // Only use instances of Iterable, Collection, [] and
        // ResultSet for iteration tag

        // User wants to ignore unfound objects, die silently
        if ((obj == null) && (ignoreNull)) {
            return SKIP_BODY;
        }
        else if ((obj == null) && (!ignoreNull)) {
            throw new JspException("No object called \"" + name + "\" found in context");
        }
        else if (obj instanceof Iterable) {
            it = ((Iterable) obj).iterator();
        }
        else if (obj instanceof Collection) {
            it = ((Collection) obj).iterator();
        }
        else if (obj instanceof ResultSet) {
            if (resetCursor) {
                try {
                    ((ResultSet) obj).beforeFirst();
                }
                catch (SQLException ex) {
                	log.debug(ex.getMessage());
                }
            }
            it = (Iterator) new ResultSetIterator((ResultSet) obj);
        }
        else if (obj instanceof Iterator) {
            it = (Iterator) obj;
        }
        else if (obj instanceof Object[]) {
            it = new ArrayIterator((Object[]) obj);
        }
        else {
            throw new JspException("Iterable instance called \"" + name + "\" not found");
        }

        if (it.hasNext()) {
           Object element = it.next();
           // remove check against given classname because problems with arrays
           // instead the "Incompatible Type" a "ClassCastException" will occur 
           //try {
           //  Class cl = Class.forName(type);
           //  if (cl.isInstance(element)) {
           pageContext.setAttribute(id, element);
           //  }
           //  else {
           //    throw new JspException("Incompatible Type");
           //  }
           //}
           //catch (Exception e) {
           //  throw new JspException("Incompatible Type");
           //}

           return EVAL_BODY_TAG;
        }
        else {
          return SKIP_BODY;
        }
    }


    /**
     * Make the next collection element available and loop, or
     * finish the iterations if there are no more elements.
     *
     * @exception JspException if a JSP exception has occurred
     */
    public int doAfterBody() throws JspException {

        if (it.hasNext()) {
           Object element = it.next();
           // remove check against given classname because problems with arrays
           // instead the "Incompatible Type" a "ClassCastException" will occur 
           try {
           //  Class cl = Class.forName(type);
           //  if (cl.isInstance(element)) {
               pageContext.setAttribute(id, element);
           //  }
           //  else {
           //    throw new JspException("Incompatible Type");
           //  }

             /*
                 without the following two lines the the tag breaks,
                 diplaying a large amount of data.
             */
             getPreviousOut().print(bodyContent.getString());
             bodyContent.clear();
           }
           catch (Exception e) {
			log.debug(e.getMessage());
             e.printStackTrace();
             throw new JspException("Incompatible Type");
           }

           return EVAL_BODY_TAG;
        }
        else {
          return SKIP_BODY;
        }
    }

    /**
     * Clean up after processing this enumeration.
     *
     * @exception JspException if a JSP exception has occurred
     */
    public int doEndTag() throws JspException {

        // Render any previously accumulated body content
        if (bodyContent != null) {

            try {
                JspWriter out = getPreviousOut();
                out.print(bodyContent.getString());
            } catch (IOException e) {
				log.debug(e.getMessage());
                e.printStackTrace();
                throw new JspException();
            }

        }

        // Continue processing this page
        return EVAL_PAGE;
    }

    /**
     * Drop the state of this tag handler. This method is called by the container
     * if reuse of tag handlers is implemented.
     */
    public void release() {
        super.release();
        list = null;
        name = null;
        id = null;
        type = null;
        it = null;
        ignoreNull = false;
        resetCursor = true;
    }

}