/*****************************************************************************
  Copyright (c) 2007, SAP AG, Germany, All rights reserved.
  Author:       D038380
  Created:      02 Februar 2007

  $Revision: #1 $
  $Date: 2007/02/02 $
*****************************************************************************/
package com.sap.isa.core.taglib;

import java.io.IOException;
import java.util.Random;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.BodyContent;
import javax.servlet.jsp.tagext.BodyTagSupport;

import com.sap.isa.core.SharedConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;

/**
 * The tag handles debugging messages on the JSP.<br />
 * 
 * Here are two ways to handle debugging information:
 * <ul>
 * <li>
 *   <b>Write debugging logs into the standard log file</b><br />
 *   With such usage:<br /> 
 *   <code>&lt;isa:debugmsg logtxt=&quot;my log file message&quot; /&gt;</code><br />
 *   the message &quot;my log file message&quot; will be written into the standard 
 *   log with DEBUG priority.<br />
 *   <br />
 * </li>
 * <li>
 *   <b>Display debugging information directly on the JSP</b><br />
 *   If the tag will be used in such way:<br />
 *   <code>
 *   &lt;isa:debugmsg&gt;<br />
 *   my direct out message<br />
 *   &lt;/isa:debugmsg&gt;<br />
 *   </code>
 *   the text &quot;my direct out message&quot; will be directly rendered together with the HTML code cames from the
 *   JSP.<br />
 *   <br />
 *   As default the messages will be rendered within an HTML-based box with the ability to collapse the message. 
 *   If it's not desired to have such additional HTML-based box, the optional parameter <code>displaymode</code> needs 
 *   to be set to <code>plain</code>.<br />
 *   <br />
 *   <b>Hint:</b><br />
 *   This functionality will only be activated if in the maintained <i>XCM Application Configuration</i>, 
 *   in <i>uidata</i> area the parameter <i>showjspdebugmsg.core</i> is set to <code>true</code>.
 *   Additionally the application must be started with the request parameter <code>showjspdebugmsg</code>
 *   and value <code>true</code>
 *   (i.e. http://&lt;host&gt;:&lt;port&gt;/&lt;app_name&gt;/init.do?showjspdebugmsg=true)  
 *    
 * </li>
 * </ul>     
 * @author D038380
 * @version 1.0
 *
 */
public class DebugMsgTag extends BodyTagSupport {

    private static final IsaLocation log = IsaLocation.getInstance(DebugMsgTag.class.getName());
    private static Random random = new Random();
	
    private String logtxt = null;
    private String displayMode = null;
    private String headtxt = null;

    /**
     * Creates new IsaLogTag
     */
    public DebugMsgTag() {
    }

    /**
     * Set the log text.
     *
     * @param logtxt The log text which should be written into log file
     */
    public void setLogtxt(String logtxt) {
        this.logtxt = logtxt;
    }

    /**
     * Set the display mode for debugging messages directly on the JSP.<br />
     * Currently only the value &quot;plain&quot; will be supported which with 
     * no HTML-based box will be generated around the debugging message. 
     *
     * @param displayMode The display mode
     */
    public void setDisplaymode(String displayMode) {
        this.displayMode = displayMode;
    }

    /**
     * Set the optional header text in case of displaying debug 
     * messages directly on the JSP.
     *
     * @param headtxt The header text
     */
    public void setHeadtxt(String headtxt) {
        this.headtxt = headtxt;
    }
    
    /**
     * Writes debug message into J2EE log file if a message was given by 
     * <code>logtxt</code> parameter.
     *
     * @exception JspException if a JSP exception has occurred
     */
    public int doStartTag() throws JspException {

        if(this.logtxt != null) {
        	
        	// Add name of compiled JSP to the begin of the message string:
        	String logtxtExt = pageContext.getServletConfig().getServletName() + ": " + this.logtxt;
            log.debug(logtxtExt);
        }
        return EVAL_BODY_BUFFERED;
    }

    /**
     * Displays debugging messages directly on the JSP if it was enabled by 
     * request parameter and additionally in XCM.
     *
     * @exception JspException if a JSP exception has occurred
     */
    public int doAfterBody() throws JspTagException {
    
        boolean jspDebugMsgAllowed = false; 
        
        // check at first, if request parameter for JSP debug out is set to TRUE
        UserSessionData userSessionData = UserSessionData.getUserSessionData(pageContext.getSession());
        if (userSessionData == null) {
            return SKIP_BODY;
        }
        String parameter = (String)userSessionData.getAttribute(SharedConst.SHOW_JSP_DEBUGMSG);
        if (parameter != null && parameter.equalsIgnoreCase("true")) {
        	jspDebugMsgAllowed = true;
        }
    	
        // if body content is available, write it out
        try {
            BodyContent bc = getBodyContent();
            if( jspDebugMsgAllowed && bc != null && !bc.getString().equals("") ) {
            	
            	String out = "";
            	
            	if(this.displayMode != null && this.displayMode.equalsIgnoreCase("plain")) {
            		out = bc.getString();
            	}
            	else {
                    String uniqueVal = Integer.toString(random.nextInt());
                    String headTextVal = (this.headtxt != null) ? this.headtxt : "JSP Debug Info";
                    out = "<div class=\"jspdebugmsg\">" +
                          "<div class=\"jspdebugmsg-head\"><table><tr><td>" + headTextVal + "</td><td style=\"text-align: right;\">[" +
                          "<span id=\"" + uniqueVal + "-minus\"><a href=\"#\" onclick=\"document.getElementById('" + uniqueVal + "').style.display='none'; document.getElementById('" + uniqueVal + "-minus').style.display='none'; document.getElementById('" + uniqueVal + "-plus').style.display='inline'; return false;\">-</a></span>" +
                          "<span id=\"" + uniqueVal + "-plus\" style=\"display: none;\"><a href=\"#\" onclick=\"document.getElementById('" + uniqueVal + "').style.display='block'; document.getElementById('" + uniqueVal + "-minus').style.display='inline'; document.getElementById('" + uniqueVal + "-plus').style.display='none'; return false;\">+</a></span>" +
                          "]</td></tr></table></div>" +
                          "<div class=\"jspdebugmsg-body\" id=\"" + uniqueVal + "\">" + bc.getString() + "</div>" +
                          "</div>";
            	}
                bc.clearBody();
                bc.getEnclosingWriter().write( out );	
            }
            return SKIP_BODY;
        } catch( IOException ex ) {
            return SKIP_BODY;
        }
    }

    /**
     * Finish tag processing.
     *
     * @exception JspException if a JSP exception has occurred
     */
    public int doEndTag() throws JspTagException {
      return EVAL_PAGE;
    }
    
}
