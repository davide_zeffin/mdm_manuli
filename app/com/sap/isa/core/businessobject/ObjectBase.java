/*****************************************************************************
    Class:        ObjectBase
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Thomas Smits
    Created:      19.3.2001
    Version:      1.0

    $Revision: #2 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.core.businessobject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.sap.isa.core.Iterable;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.businessobject.boi.ObjectBaseData;

/**
 * Base class for all objects in the internet sales scenario. The
 * common feature of all these objects is that they contain a unique key
 * (e.g. a primary key of an underlying database, an object id or an
 * GUID). This key is abstract and wrapped by the <code>TechKey</code>
 * class.
 *
 * @author Thomas Smits
 * @version 1.0
 */
public abstract class ObjectBase extends BOBase implements ObjectBaseData {

    private static long handleInt = 0;

    protected TechKey techKey;
    protected String  handle = "";

    /**
     * Default constructor for this object
     */
    public ObjectBase() {
        techKey = TechKey.EMPTY_KEY;
    }

    /**
     * Constructor for the object directly taking the technical
     * key for the object
     *
     * @param techKey Technical key for the object
     */
    protected ObjectBase(TechKey techKey) {
        this.techKey = techKey;
    }

    /**
     * Retrieves the key for the object.
     *
     * @return The object's key
     */
    public TechKey getTechKey() {
        return techKey;
    }

    /**
     * Sets the key for the document.
     *
     * @param key Key to be set
     */
    public void setTechKey(TechKey techKey){
        this.techKey = techKey;
    }

    /**
     * Callback method for the <code>BusinessObjectManager</code> to tell
     * the business object that life is over and that it has to release
     * all ressources. You have to overwrite this method to do something
     * useful, this implementation does nothing.
     */
    public void destroy() {
    }

    /**
     * Returns the string representation of the given object.
     *
     * @retun String representation
     */
    public String toString() {
        String retVal;

        retVal = super.toString() + ", TechKey=[";
        retVal += (techKey == null) ? "NULL" : techKey.toString() + "]";
        retVal +=  ", hashCode=[" + hashCode() + "]";;

        return retVal;
    }

    /**
     * Compares this object to the specified object.
     * The result is <code>true</code> if and only if the argument is not
     * <code>null</code> and is a <code>ObjectBase</code> object that
     * has the same technical key (<code>TechKey</code>)
     * as this object.
     *
     * @param o Object to compare with
     * @return <code>true</code> if the objects are identical; otherwiese
     *         <code>false</code>.
     */
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        else if (o == this) {
            return true;
        }
        else if (o instanceof ObjectBase) {
            return techKey.equals(((ObjectBase) o).techKey);
        }
        else {
            return false;
        }
    }

    /**
     * Returns the hash code for this object.
     *
     * @return Hash code
     */
    public int hashCode() {
        return techKey.hashCode();
    }


    /**
     *
     * The method returns an iterator over all sub objects of the BusinessObject
     *
     * @return Iterator to loop over sub objects
     *
     */
    public Iterator getSubObjectIterator( ) {
        List objList = new ArrayList();

        if (this instanceof HasHeader) {
             ObjectBase bo = ((HasHeader)this).getHeader();
             if (bo != null) {
                 // add the found object
                 objList.add(bo);
             }
        }

        if (this instanceof Iterable) {

            Iterator i = ((Iterable)this).iterator();

            while (i != null &&i.hasNext()) {
                Object obj = i.next();
                if (obj instanceof ObjectBase ) {
                    ObjectBase bo = (ObjectBase) obj;
                    // add the found object
                    objList.add(bo);

                    // add all sub object of the object
                    Iterator objIterator = bo.getSubObjectIterator();
                    while (objIterator.hasNext()) {
                        objList.add(objIterator.next());
                    }

                }
            }

        }

        return objList.iterator();
    }


    /**
     *
     * The method returns an reference to a sub objects for a given tech key
     *
     * @return sub object
     *
     */
    public ObjectBaseData getSubObject(TechKey techKey) {

        if (techKey.toString().length() > 0) {

            Iterator iter = getSubObjectIterator();

            while (iter.hasNext()) {
                ObjectBaseData iBob = (ObjectBaseData)iter.next();
                if (iBob.getTechKey().equals(techKey)) {
                    return iBob;
                }
            }
        }
        return null;
    }

   /**
    *
    * This method creates a unique handle, as an alternative key for the business object,
    * because at the creation point no techkey for the object exists. Therefore
    * maybay the handle is needed to identify the object in backend
    *
    */
   public void createUniqueHandle() {

       handleInt++;
       if (handleInt > 9999999999L) {
           handleInt = 1;
       }
       handle = "" + handleInt;

   }


    /**
     * This method sets the handle, as an alternative key for the business object,
     * because at the creation point no techkey for the object exists. Therefore
     * maybay the handle is needed to identify the object in backend
     *
     * @param handle the handle of business object which identifies the object
     * in the backend, if the techkey still not exists
     *
     */
    public void setHandle(String handle) {
        this.handle = handle;
    }


  /**
   *
   * This method returns the handle, as an alternative key for the business object,
   * because at the creation point no techkey for the object exists. Therefore
   * maybay the handle is needed to identify the object in backend
   *
   * return the handle of business object which is needed to identify the object
   * in the backend, if the techkey still not exists
   *
   */
   public String getHandle() {
       return handle;
   }

   /**
    * Returns the information, if a handle for the object exists.
    *
    * @return true, if there is a handle
    *         false, if the handle is <code>null</code> or 
    *                if the handle is an empty string.
    */
    public boolean hasHandle() {
        
        if (handle == null) {
            return false;
        }
        
        if ("".equals(handle.trim())) {
            return false;
        }
        
        return true;
    }

    /**
     * Makes a copy of the object.<br> 
     * 
     * @returns a copy of object. 
     * 
     */
    protected Object clone() throws CloneNotSupportedException {
    
        ObjectBase objectBase = (ObjectBase)super.clone();

        objectBase.handle = handle;
        objectBase.techKey = new TechKey(techKey.getIdAsString());

        return objectBase;
    }

}
