/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      28 March 2001

  $Header: //java/sapm/esales/30/src/com/sap/isa/core/businessobject/management/BOManager.java#1 $
  $Revision: #1 $
  $Date: 2001/07/20 $
*****************************************************************************/
package com.sap.isa.core.businessobject.management;


/**
 * This interface has to be implemented by all Business Object Managers
 * which are managed by the MetaBusinessObjectManager.
 */
public interface BOManager {
    public void release();
}