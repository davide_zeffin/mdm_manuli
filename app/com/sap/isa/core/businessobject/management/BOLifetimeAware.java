/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Franz Mueller
  Created:      24 July 2001

  $Header: //java/sapm/esales/30/src/com/sap/isa/core/businessobject/management/BOLifetimeAware.java#1 $
  $Revi$
*****************************************************************************/
package com.sap.isa.core.businessobject.management;

import com.sap.isa.core.businessobject.BOLifetimeObserver;

/**
 * This interface has to be implemented by all Business Object Managers
 * which are managed by the MetaBusinessObjectManager and which would like to
 * participate in the notification of the lifetime of their BOs
 */
public interface BOLifetimeAware {
    public void setBOLifetimeObserver(BOLifetimeObserver lifetimeObserver);
}