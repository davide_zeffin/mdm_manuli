/*****************************************************************************
    Class:        MetaBOMConfigReader
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Franz Mueller
    Created:      11.July.2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/07/20 $
*****************************************************************************/

package com.sap.isa.core.businessobject.management;


/**
 * Instances of this class are used to obtain the informations
 * from the configuration file and to temporary store them.
 * After the initialization the instances will be released
 */
public class  BOMsDescription {
   private String name;
   private String className;

   public BOMsDescription() {
   }

   public void setName(String name) {
       this.name = name;
   }
   public String getName() {
       return name;
   }

   public void setClassName(String className) {
       this.className = className;
   }
   public String getClassName() {
       return className;
   }

}


