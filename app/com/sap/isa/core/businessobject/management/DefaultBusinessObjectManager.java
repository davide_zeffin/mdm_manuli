/*****************************************************************************
    Interface:    BusinessObjectManagerBase
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Marek Barwicki
    Created:      15.October.2001
    Version:      1.0

    $Revision:  $
    $Date: $
*****************************************************************************/
package com.sap.isa.core.businessobject.management;

import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.eai.BackendObjectManager;

/**
 * This Business Object Manager can be used as a base class
 * for all Bausiness Object Managers which manage backend aware
 * Business Objects
 */
public abstract class DefaultBusinessObjectManager
            extends BusinessObjectManagerBase
            implements BackendAware {

  protected BackendObjectManager bem;

  /**
   * Callback for the manager of this bom to set a reference to the
   * right BackendObjectManager.
   *
   * @param bem the reference to be set
   */
  public void setBackendObjectManager(BackendObjectManager bem) {
      this.bem = bem;
  }

  /**
   * Utility method which can be used to assign the BackendObjectManager
   * to Business Objects which implement the BackendAware
   * @see BackendAware BackendAware
   */
  protected void assignBackendObjectManager(Object obj) {
      if ((obj instanceof BackendAware) && (obj != null)) {
          ((BackendAware) obj).setBackendObjectManager(bem);
      }
  }
}