/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Franz Mueller
  Created:      09 July 2001

  $Header: //java/sapm/esales/30/src/com/sap/isa/core/businessobject/management/BOMInstantiationException.java#1 $
  $Revision: #1 $
  $Date: 2001/07/20 $
*****************************************************************************/
package com.sap.isa.core.businessobject.management;


/**
 * BOMInstantiationException occures whenever the MBOM is
 * unable to instantiate a BOM for a given type.
 */
public class BOMInstantiationException extends RuntimeException {
    public BOMInstantiationException(String message) {
         super(message);
    }
}