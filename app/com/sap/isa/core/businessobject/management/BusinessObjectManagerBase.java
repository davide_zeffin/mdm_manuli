/*****************************************************************************
    Interface:    BusinessObjectManagerBase
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Franz Mueller 
    Created:      23.July.2001
    Version:      1.0

    $Revision: #4 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.core.businessobject.management;

import com.sap.isa.core.businessobject.BOLifetimeObserver;
import com.sap.isa.core.logging.IsaLocation;



/**
 * Convinient BusinessObjectManager base.
 */
public abstract class BusinessObjectManagerBase 
           implements BOManager, BOLifetimeAware, BOLifetimeObserver {

    /**
     * The IsaLocation of the current BOM.
     * This will be instantiated during construction and is always present.
     */
    protected static IsaLocation  log = null;


    private BOLifetimeObserver lifetimeObserver;
 
    

    public BusinessObjectManagerBase() {

        // Retrieve the actual IsaLocation
        log = IsaLocation.getInstance(this.getClass().getName());
    }
    
    
    /*
    * interface BOLifetimeAware
    */
    public void setBOLifetimeObserver(BOLifetimeObserver lifetimeObserver) {
        this.lifetimeObserver = lifetimeObserver;
    }
    
    
    /*
    * interface BOLifetimeObserver
    */
    public void removalNotification(Object obj) {        
        lifetimeObserver.removalNotification(obj);
    }

    public void creationNotification(Object obj) {
        lifetimeObserver.creationNotification(obj);
    }


    /*
    * This method has to be implemented by the extendign class
    */    
    public abstract void release();
    
        
}