/*****************************************************************************
    Class:        MetaBOMConfigReader
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Franz Mueller
    Created:      09.July.2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/07/20 $
*****************************************************************************/

package com.sap.isa.core.businessobject.management;



import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;

import org.apache.commons.digester.Digester;
import org.apache.struts.util.MessageResources;

import com.sap.isa.core.init.Initializable;
import com.sap.isa.core.init.InitializationEnvironment;
import com.sap.isa.core.init.InitializeException;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;

/**
 * This class is responsible for reading and managing the configuration file,
 * which describes the available BOMs.
 */
public class MetaBOMConfigReader implements Initializable {

    protected static IsaLocation log;

    private static final String DESCR_CLASS =
        "com.sap.isa.core.businessobject.management.BOMsDescription";

    private static HashMap bomDescriptions;
    
	private static HashMap boMappings;

    public MetaBOMConfigReader() {
		log = IsaLocation.getInstance(this.getClass().getName());
    }


    private Digester initCfgDigester(int debugLevel) {

     	Digester digester = new Digester();
       	digester.push(this);
       	digester.setDebug(debugLevel); 
       	digester.setValidating(false);

       	digester.addObjectCreate("BusinessObjectManagers/BusinessObjectManager", DESCR_CLASS);
       	digester.addSetProperties("BusinessObjectManagers/BusinessObjectManager");
       	digester.addSetNext("BusinessObjectManagers/BusinessObjectManager", "addBOM",
                                                                  DESCR_CLASS);

		digester.addObjectCreate("BusinessObjectManagers/BusinessObject", BOMapping.class.getName());
		digester.addSetProperties("BusinessObjectManagers/BusinessObject");
		digester.addSetNext("BusinessObjectManagers/BusinessObject", "addBO", BOMapping.class.getName());

       	return digester;
    }

    public void initialize(InitializationEnvironment env, Properties props)
        throws InitializeException {
		final String METHOD = "initialize()";
		log.entering(METHOD);
		
        bomDescriptions = new HashMap();
		boMappings = new HashMap();
        
		
        MessageResources msgs = env.getMessageResources();
		try {
		
	        String cfgFileName = props.getProperty("config-file");
	        if (cfgFileName == null) {
	           log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.noProperty", new Object[] {"config-file"}, null);
	           throw new InitializeException(msgs.getMessage("system.noProperty",
	                                                               "config-file"));
	        }
	
	        InputStream input  = env.getResourceAsStream(cfgFileName);
	        if (input == null) {
	           log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.noResource", new Object[] {cfgFileName}, null);
	           throw new InitializeException(msgs.getMessage("system.noResource",
	                                                                 cfgFileName));
	        }
	
	        Digester digester = initCfgDigester(0);
	
	        try {
	           digester.parse(input);
	        } catch (Throwable ex) {
	           log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.exception", new Object[] {ex.getMessage()}, ex);
	           throw new InitializeException(msgs.getMessage("system.exception",
	                                                              ex.getMessage()));
	        } finally {
	           try {
	              input.close();
	           } catch (IOException ex) {
	           	log.debug(ex.getMessage());
	           }
	        }
		} finally {
			log.exiting();
		}
   }

   public void terminate() {
      // Make it easier for Garbage collection
		bomDescriptions=null;
		boMappings=null;
   }


   public static void addBOM(BOMsDescription descr) {
       bomDescriptions.put(descr.getName(), descr.getClassName());
   }

   	public static void addBO(BOMapping boMapping) {
		boMappings.put(boMapping.getName(), boMapping.getBom());
	}

   	public static String getClassName(String bomName) {
       return (String)bomDescriptions.get(bomName);
   	}


	/**
	 * Return the business object, which could provide the given business object. <br>
	 * 
	 * @param boName
	 * @return the assigned business object Manager.
	 */
	public static String getClassNameForObject(String boName) {
		final String METHOD = "getClassNameForObject()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("boName="+boName);
		if (boMappings == null) {
			boMappings = new HashMap();
		}
		try {
		
			String bom =(String)boMappings.get(boName);
			if (bom == null) {
				log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.error.noBOM", new Object[] {boName}, null);
				return null;	
			}
				
			return getClassName(bom);
		} finally {
			log.exiting();	
		}
	}

	/**
	 * Returns an iterator over all BOM's. <br>
	 * 
	 * @return iterator over all bom class names.
	 */
	 public static Iterator getBOMIterator() {
	 	if (bomDescriptions == null) {
			bomDescriptions = new HashMap();
	 	}
		return bomDescriptions.values().iterator();
	 }

	/**
	 * Inner class to handle the mapping between business object and
	 * and the bom. This allows to get an appropriate BOM for a given 
	 * business object. <br>
	 */
	static public class BOMapping {
		
		protected String name="";
		
		protected String bom ="";
		
        /**
         * Return the property {@link #bom}. <br>
         * 
         * @return {@link #bom}
         */
        public String getBom() {
            return bom;
        }

		/**
		 * Set the property {@link #bom}. <br>
		 * 
		 * @param string {@link #bom}
		 */
		public void setBom(String string) {
			bom = string;
		}


        /**
         * Return the property {@link #name}. <br>
         * 
         * @return {@link #name}
         */
        public String getName() {
            return name;
        }

        /**
         * Set the property {@link #name}. <br>
         * 
         * @param string {@link #name}
         */
        public void setName(String string) {
            name = string;
        }

	}

}

