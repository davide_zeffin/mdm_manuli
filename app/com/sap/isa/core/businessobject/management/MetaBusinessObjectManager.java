/*****************************************************************************
    Class:        MetaBusinessObjectManager
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Franz Mueller
    Created:      09.July.2001
    Version:      1.0

    $Revision: #6 $
    $Date: 2001/07/25 $
*****************************************************************************/

package com.sap.isa.core.businessobject.management;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import org.apache.struts.util.MessageResources;

import com.sap.isa.core.businessobject.BOLifetimeObserver;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.businessobject.event.BusinessObjectCreationEvent;
import com.sap.isa.core.businessobject.event.BusinessObjectCreationListener;
import com.sap.isa.core.businessobject.event.BusinessObjectRemovalEvent;
import com.sap.isa.core.businessobject.event.BusinessObjectRemovalListener;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.core.eai.BackendObjectManagerImpl;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;



/**
 * This class is responsible for creation, management and destruction of other
 * BusinessObjectManagers (BOMs).
 * The framework generates for each session an instance of this type.
 * It is also responsible for the creation of the BackendBusinessObjectManager.
 * Whenever a specific BOM implements the BackendAware interface, an instance
 * (allways the same in the context of one instance of MetaBOM) of BackendManager
 * will be assigned to it.
 * If the BOM implements the BOLifetimeAware interface an instance (allways the
 * same in the context of one instance of MetaBOM) of the inner class BOMsEventHandling
 * will be assigned to it. This interface (in conjunction with the interface
 * BOLifetimeObserver ) will be used to fire BO lifetime notifications.
 */
public class MetaBusinessObjectManager implements HttpSessionBindingListener {

    private static final String INSTANTIATION_ERROR_KEY =
                                               "system.bom.instantiation.error";

    private MessageResources resources;
    private BackendObjectManagerImpl bem;

    private HashMap knownBOMs;

    private List creationListener;
    private List removalListener;

    private BOMsEventHandling eventHandling;

    protected static IsaLocation log;

    private Properties props;

    /**
     * An instance of the MetaBOM is assigned to each single isa-session
     * It is constructed through the core.InitAction and will be destroyed
     * whenever the session will be invalidated.
     * @param resource resources used for language depenent texts
     * @param props properties needed for configuration of the initialization process
     */
    public MetaBusinessObjectManager(MessageResources resources, Properties props) {
        this.resources = resources;
        this.props     = props;
        log = IsaLocation.getInstance(this.getClass().getName());
		initialize();
    }

    /**
     * An instance of the MetaBOM is assigned to each single isa-session
     * It is constructed through the core.InitAction and will be destroyed
     * whenever the session will be invalidated.
     */
    public MetaBusinessObjectManager(MessageResources resources) {
        this.resources = resources;
		log = IsaLocation.getInstance(this.getClass().getName());
		initialize();
	}


	/**
	 * Set the language, which could used in the backend. <br>
	 * The value will be stored in the backend context.
	 * 
	 * @param language
	 */
	public void setBackendLanguage (String language) {
		getBackendObjectManager().setAttribute(BackendObjectManager.LANGUAGE,language);
	}

	
	/**
	 * Set the language, which could used in the backend. <br>
	 * The value will be stored in the backend context.
	 * 
	 * @param uiController
	 */
	public void setBackendUIController (Object uiController) {
		getBackendObjectManager().setAttribute(BackendObjectManager.UI_CONTROLLER,uiController);
	}


	/**
	 * Initialize the instance. <br>
	 */
	protected void initialize() {
		final String METHOD = "initialize()";
		log.entering(METHOD);
		
		knownBOMs = new HashMap();

		creationListener = new ArrayList();
		removalListener  = new ArrayList();

		eventHandling = new BOMsEventHandling();


		 // create all available boms, ignore errors.
		 // errors will logged in the error log.
		Iterator iter = MetaBOMConfigReader.getBOMIterator();
		while (iter.hasNext()) {
            String className = (String) iter.next();
            try {
				getBOM(className);
            }
            catch (BOMInstantiationException exception) {
            	log.debug(exception.getMessage());
            }
        }
        log.exiting();
		
	}


    private String getMessage(String msgKey, Object[] args) {
        if (resources != null)
           return resources.getMessage(msgKey, args);

        return msgKey;
    }
    
    private String getMessage(String msgKey) {
        if (resources != null)
           return resources.getMessage(msgKey);

        return msgKey;
    }

    private void checkNull(Object obj) throws BOMInstantiationException {

       if (obj == null) {
           Object[] args = new Object[1]; args[0] = "null";
           log.error(LogUtil.APPS_BUSINESS_LOGIC,INSTANTIATION_ERROR_KEY, args, null);
           throw new BOMInstantiationException(
                                    getMessage(INSTANTIATION_ERROR_KEY, args));
       }
    }


    /*
     * creates and manages an instance of the BackendObjectManager
    */
    protected BackendObjectManagerImpl getBackendObjectManager() {
       if (bem == null)
          bem = new BackendObjectManagerImpl(props);
       return bem;

    }

    /*
     * This method is called whenever an new BOM-instance was created
    */
    private void newBOMIntroduction(BOManager bom) {
       if (bom instanceof BackendAware)
         ((BackendAware)bom).setBackendObjectManager(getBackendObjectManager());

       if (bom instanceof BOLifetimeAware)
         ((BOLifetimeAware)bom).setBOLifetimeObserver(eventHandling);
    }


    /*
     *  Factory for the BOM
     *
     * @type listener The listener to be removed
    */
    public synchronized BOManager getBOM(Class type)
                                      throws BOMInstantiationException {
		final String METHOD = "getBOM()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("type="+type);
       checkNull(type);

       BOManager bom = (BOManager)knownBOMs.get(type);
       if (bom == null) {
          try {
             bom = (BOManager)type.newInstance();

             newBOMIntroduction(bom);

             knownBOMs.put(type, bom);
             log.info("system.bom.created",
                            new Object[] {type.getName()}, null);
          } catch (Throwable ex ) {
              log.error(LogUtil.APPS_BUSINESS_LOGIC,INSTANTIATION_ERROR_KEY, new Object[] {type.getName()}, ex);
              // throw new BOMInstantiationException(ex.getMessage());
          }
       }
		log.exiting();
       return bom;
    }

    public BOManager getBOM(String bomClassName) throws BOMInstantiationException {
		final String METHOD = "getBOM()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("bomClassName="+bomClassName);
			
       checkNull(bomClassName);

       try {
        log.debug("DebugInfo: "+bomClassName);
          Class type = Class.forName(bomClassName);
          return getBOM(type);
       } catch (ClassNotFoundException ex) {
          log.error(LogUtil.APPS_BUSINESS_LOGIC,INSTANTIATION_ERROR_KEY, new Object[] {bomClassName}, ex);
          throw new BOMInstantiationException(ex.getMessage());
       } finally {
       		log.exiting();
       }
       
    }

    
    /**
     * Return if the BOM for the given name is known in the meta bom. <br>
     * 
     * @param bomName
     * @return
     */
    public boolean checkBOMByName(String bomName) {
    	 String className = lookupName(bomName);
    	 return className != null;  	
    }
    
    
    /**
     * Return if the BOM for the given name is known in the meta bom. <br>
     * In opposite of the method {@link #checkBOMByName(String)} the check is done with the
     * <code>instanceof</code> java feature. Therefore also Interface could be checked.
     * The calling application have to ensure to allow all types only once in the application. 
     * 
     * @param type
     * @return found bom.
     */
    public boolean checkBOMByType(Class type) {
        
        final String METHOD = "checkBOMByType";
        log.entering(METHOD);
        if (log.isDebugEnabled())
            log.debug("type=" + type);
            
        checkNull(type); 
        try {
            
            Iterator iter = knownBOMs.values().iterator();
            while (iter.hasNext()) {
                BOManager boManager = (BOManager) iter.next();
                
                if (type.isAssignableFrom(boManager.getClass())) {
                    return true;
                }
            }
            
            
            log.error(LogUtil.APPS_BUSINESS_LOGIC,INSTANTIATION_ERROR_KEY, new Object[] {type.getName()}, null);
            //no class for type -> return false
//			throw new BOMInstantiationException("No class for type: " + type.getName());
            return false;
        }
		finally {
            log.exiting();  
        }
    }
    
    
    public BOManager getBOMbyName(String bomName) throws BOMInstantiationException {
		final String METHOD = "getBOMbyName()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("bomName="+bomName);
       checkNull(bomName);

       String className = null;
       try {
          className = lookupName(bomName);
       } catch (Throwable ex) {
          log.error(LogUtil.APPS_BUSINESS_LOGIC,INSTANTIATION_ERROR_KEY, new Object[] {bomName}, ex);
          throw new BOMInstantiationException(ex.getMessage());
       } finally {
       		log.exiting();
       }

       return getBOM(className);
    }


	/**
	 * Return a bom which is of the given type. <br>
	 * In opposite of the method {@link #getBOM(String)} the check is done with the
	 * <code>instanceof</code> java feature. Therefore also Interface could be checked.
	 * Because the returned BOM is not uniqued. The calling application have to ensure 
	 * to allow all types only once in the application. 
	 * 
	 * @param type
	 * @return found bom.
	 * @throws BOMInstantiationException
	 */
	public BOManager getBOMByType(Class type) throws BOMInstantiationException {
		final String METHOD = "getBOMByType()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("type="+type);
		checkNull(type); 
		try {
			
			Iterator iter = knownBOMs.values().iterator();
			while (iter.hasNext()) {
				BOManager boManager = (BOManager) iter.next();
	            
	            if (type.isAssignableFrom(boManager.getClass())) {
	            	return boManager;
	            }
	        }
			
			log.error(LogUtil.APPS_BUSINESS_LOGIC,INSTANTIATION_ERROR_KEY, new Object[] {type.getName()}, null);
			throw new BOMInstantiationException("No class for type: " + type.getName());
		} finally {
			log.exiting();
		}
	}


	/**
	  * Return the business object for the given name <code>businessObjectName</code>. <br>
	  * 
	  * The appropriate business object manger is taken
	  * from the meta business object config reader. <br>                                                                                     
	  *                                                                                                                                                                                               
	  * The business object with the name {@link #businessObjectName} must be
	  * a bean property of the business object manager. See 
	  * {@link MetaBusinessObjectManager#getBusinessObject(String, BOManager)} for deatils.
	  * 
	  * @param businessObjectName name of the business object.
	  * 
	  * @return reference to the business object or <code>null</code> if no 
	  * object could be found.
	  */
	 public Object getBusinessObject(String businessObjectName) {
		final String METHOD = "getBusinessObject()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("businessObjectName="+businessObjectName);
		checkNull(businessObjectName); 
		
		String boManagerClassName = MetaBOMConfigReader.getClassNameForObject(businessObjectName);
		try {
		
			if (boManagerClassName == null) {
				log.debug("business object manager for business object: "+ businessObjectName + " does not exists!");
				return null;
			}	
				
			 // first get the business object manager from the meta bom		
			BOManager bom = getBOM(boManagerClassName);
	 		if (bom==null) {
				 log.debug("business object manager: "+ boManagerClassName + " does not exists!");
				 return null;
			 }
			
	        return getBusinessObject(businessObjectName, bom);
		} finally {
			log.exiting();
		}
	 }

	/**
	  * Try to get the business object with the name <code>businessObjectName</code> from the given
	  * business object manager. <br>
	  * 
	  * The business object with the name {@link #businessObjectName} must be
	  * a bean property of the business object manager. 
	  * That means that the method 
	  * get&lt;{@link #businessObjectName}&gt;() must exist for the found bom. <br>         
	  * <strong>Example</strong><br/>                                                                                                                                                                      
	  * If the business object name is "basket". The method 
	  * <code>getBasket()</code> is called on the business object manager 
	  * to get a reference to the business object. <br/>                   
	  * If the object could not found with the "get" method it will be tried 
	  * to call the "create" method to get the object. (Within the example the 
	  * <code>createBasket</code> will called if the getBasket method returns 
	  * <code>null</code>.                         
	  * 
	  * @param businessObjectName name of the business object.
	  * @param bom the business object mananger to get the business 
	  *     object.
	  * 
	  * @return reference to the business object or <code>null</code> if no 
	  * object could be found.
	  */
    public Object getBusinessObject(String businessObjectName, BOManager bom) {
         
		final String METHOD = "getBusinessObject()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("businessObjectName="+businessObjectName+ ", bom="+bom);
         Object businessObject = null;
        
         // Get the business object now.
         // the name of the business object is assumed as a property name of the 
         // business object manager.		
         try {
        
        	 String methodName = "create" + businessObjectName.substring(0,1).toUpperCase() + businessObjectName.substring(1);
        			
        	 // get the help value method from the class
        	 java.lang.reflect.Method  method = bom.getClass().getMethod(methodName,
        			 new Class[]{});
        
        	 // invoke the method to get the result Data
        	 businessObject = method.invoke(bom, new Object[]{});
        
        	 if (businessObject == null) {
        
        		 methodName = "get" + businessObjectName.substring(0,1).toUpperCase() + businessObjectName.substring(1);
        			
        		 // get the help value method from the class
        		 method = bom.getClass().getMethod(methodName, new Class[]{});
        
        		 // 	invoke the method to get the result Data
        		 businessObject = method.invoke(bom, new Object[]{});			
        	 }		
        
         } 
         catch (Exception ex) {
        	 log.debug("business object: "+ businessObjectName + " could not be found");
        	 log.debug(ex.getMessage());
         }
         finally {
         	log.exiting();
         }
         return businessObject;
    }



    private String lookupName(String name) {
       return MetaBOMConfigReader.getClassName(name);
    }

    /**
     * Callback for the servlet environment to notify this object that
     * it is bound to the session context. This method is not implemented and
     * does nothing.
     *
     * @param e Event
     */
    public void valueBound(HttpSessionBindingEvent e) {
    }

    /**
     * Callback for the servlet environment to notify this object that
     * it is going to be unbound from the session context.
     *
     * @param e Event
     */
    public void valueUnbound(HttpSessionBindingEvent e)
    {
    	destroy();
    }
     
	public void destroy()
    {
        Object[] args = new Object[1];
        Iterator iterator = knownBOMs.keySet().iterator();

        while (iterator.hasNext()) {
            Class type = (Class)iterator.next();
            BOManager bom = (BOManager)knownBOMs.get(type);
            bom.release();

            args[0] = type.getName();
            log.info("system.bom.released", args, null);
        }

        if (bem != null) {
            bem.destroy();
            log.info("system.bom.destroy.bem");

            bem = null;
        }

        knownBOMs.clear();

        removalListener.clear();
        creationListener.clear();
    }




    /**
     * Adds a new listener for the events fired by the BOM after the creation
     * of new business objects.
     *
     * @param listener The listener to be registered
     */
    public synchronized void addBusinessObjectCreationListener(BusinessObjectCreationListener listener) {
        creationListener.add(listener);
    }

    /**
     * Adds a new listener for the events fired by the BOM after the removal
     * of new business objects.
     *
     * @param listener The listener to be registered
     */
    public synchronized void addBusinessObjectRemovalListener(BusinessObjectRemovalListener listener) {
        removalListener.add(listener);
    }

    /**
     * Removes a listener for the events fired by the BOM after the creation
     * of new business objects.
     *
     * @param listener The listener to be removed
     */
    public synchronized void removeBusinessObjectCreationListener(BusinessObjectCreationListener listener) {
        creationListener.remove(listener);
    }

    /**
     * Removes a listener for the events fired by the BOM after the removal
     * of new business objects.
     *
     * @param listener The listener to be removed
     */
    public synchronized void removeBusinessObjectRemovalListener(BusinessObjectRemovalListener listener) {
        removalListener.remove(listener);
    }



    private class BOMsEventHandling implements BOLifetimeObserver {


       public synchronized void removalNotification(Object obj) {

          BusinessObjectRemovalEvent e = new BusinessObjectRemovalEvent(obj);

          int size = removalListener.size();

          for (int i = size-1; i >= 0; i--) {
            ((BusinessObjectRemovalListener) removalListener.get(i)).objectRemoved(e);
          }
       }


       public synchronized void creationNotification(Object obj) {

         BusinessObjectCreationEvent e = new BusinessObjectCreationEvent(obj);

         int size = creationListener.size();

         for (int i = size-1; i >= 0; i--) {
            ((BusinessObjectCreationListener) creationListener.get(i)).objectCreated(e);
         }
       }

    }  // class BOMsEventHandling
}