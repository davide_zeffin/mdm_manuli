/*****************************************************************************
    Interface:    BusinessObjectRemovalListener
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Thomas Smits
    Created:      8.5.2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/07/20 $
*****************************************************************************/

package com.sap.isa.core.businessobject.event;

/**
 * Interface to allow observation of the removal process performed
 * by the <code>BusinessObjectManager</code>. Implement this interface
 * if you want to be notices of business object removals.
 *
 * @author Thomas Smits
 * @version 1.0
 */
public interface BusinessObjectRemovalListener {

    /**
     * Method called after a business object was removed
     *
     * @param e Event containing information about the removal
     */
    public void objectRemoved(BusinessObjectRemovalEvent e);
}