/*****************************************************************************
    Class         BusinessEventInitHandler
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      May 2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/07/06 $
*****************************************************************************/
package com.sap.isa.core.businessobject.event;

import java.util.Properties;

import com.sap.isa.core.init.Initializable;
import com.sap.isa.core.init.InitializationEnvironment;
import com.sap.isa.core.init.InitializeException;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;

/**
 * This class set the classname for the BusinessEventHandler which should be used.
 * The class is also the factory for the instance for the BusinessEventHandler.
 *
 * @see com.sap.isa.core.businessobject.event.BusinessEventHandlerBase
 *
 */
public class BusinessEventInitHandler implements Initializable {

    private static final String STANDARD_CLASS_NAME = "com.sap.isa.core.businessobject.event.BusinessEventHandlerBaseImpl";
    
    // classname for BusinessEventHandlerImpl class
    private static String className = STANDARD_CLASS_NAME;
    private static boolean readMissingInfoFromBackend = false;

    private static IsaLocation log = IsaLocation.getInstance("com.sap.isa.core.businessobject.event.BusinessEventInitHandler");

    /**
    * Initialize the implementing component.
    *
    * @param env the calling environment
    * @param props   component specific key/value pairs
    * @exception InitializeException at initialization error
    */
    public void initialize(InitializationEnvironment env, Properties props)
             throws InitializeException {
        try {
        	
			log.debug("Initialization of BusinessEventInitHandler is in progress");
			        	
            className=props.getProperty("event-handler");

            if (className == null || className.length() == 0) {
                className = STANDARD_CLASS_NAME;
            }

            log.debug( "Use event handler: " + className);

            String readMissingInfoFromBackend = props.getProperty("readMissingInfoFromBackend");
 
            if (readMissingInfoFromBackend!= null) {
 
                if (readMissingInfoFromBackend.equals("true")) {
                    BusinessEventInitHandler.readMissingInfoFromBackend = true;
                }
                else {
                    BusinessEventInitHandler.readMissingInfoFromBackend = false;
                }
                 
                log.debug( "event handler reads missing info from backend: "+ readMissingInfoFromBackend);
            }

        } catch (Exception ex) {
          new InitializeException(ex.toString());
        }
        log.debug("Finished initialization of BusinessEventInitHandler");
    }


    /**
     * Returns an instance of the BusinessEventHandler
     *
     * @return instance
     *
     */
    public static BusinessEventHandlerBase createEventHandler() {
        // take the handler class from init file

        try {
            Class handlerClass = Class.forName(className);
     
            BusinessEventHandlerBase businessEventHandler = (BusinessEventHandlerBase)handlerClass.newInstance();
 
            businessEventHandler.setReadMissingInfoFromBackend(readMissingInfoFromBackend);
 
            return businessEventHandler;
        }
        catch (Exception ex){
        	String msg = "error creating event handler";
            log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.exception", new Object[]{msg},ex);
        }
        return null;
    }



    /**
    * Terminate the component.
    */
    public void terminate() {
    }


}
