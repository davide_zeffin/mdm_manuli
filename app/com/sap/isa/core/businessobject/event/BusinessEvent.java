/*****************************************************************************
    Class         BusinessEventManager
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Action add a product to the basket
    Author:       Wolfgang Sattler
    Created:      May 2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/06/26 $
*****************************************************************************/
package com.sap.isa.core.businessobject.event;

import java.util.EventObject;

/**
 * The super class for all business events. <br>
 * To use the event framework your event should extend this class.
 *
 * <p><b>Example</b>
 * <pre>
 * public class LoginEvent extends BusinessEvent {
 *
 *
 *      public LoginEvent(User user) {
 *          this.source = user.
 *      }
 * }
 *
 * </pre>
 *
 * @author  Wolfgang Sattler
 * @version 1.0
 *
 */
public class BusinessEvent extends EventObject {

    /**
     * the constructor of an Business Event.<br>
     * If your event is assigned to a Business object you should give this object
     * as <code>source<code>.
     *
     * param object object which affect from the event
     *
     */
    public BusinessEvent(Object source) {
        super(source);
    }

}
