/*****************************************************************************
    Class:        UnsecureConnectionEvent
    Copyright (c) 2005, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      11.08.2005
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.core.businessobject.event;

import javax.servlet.http.HttpServletRequest;

import com.sap.isa.core.UserSessionData;

/**
 * The class UnsecureConnectionEvent is fired if unsecure connection is detect althougt
 * the secure switch is set to on. <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public interface UnsecureConnectionEventHandler {
	
	
	/**
	 * This method is called if the connection is unsecure. <br>
	 *
	 */
	public void onUnSecureConnection (HttpServletRequest request, UserSessionData userData);
	

}
