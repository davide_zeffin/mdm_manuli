/*****************************************************************************
    Class:        BusinessEventSourceSetup
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Thomas Smits
    Created:      31.5.2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/06/26 $
*****************************************************************************/

package com.sap.isa.core.businessobject.event;

/**
 * Class that observes a BOM for the creation and destruction of
 * BusinessObjects. For objects that implement the
 * <code>BusinessEventSource</code> interface, a reference to the
 * <code>BusinessEventHandler</code> is set into the object.<br>
 * The purpose of this class is to keep all this extra creation stuff
 * outside the BOM, so that the clear design ;-) of the BOM is not diluted.
 *
 * @author Thomas Smits
 * @version 1.0
 */
public class BusinessEventSourceSetup
        implements BusinessObjectCreationListener {

    private BusinessEventHandlerBase businessEventHandler;

    /**
     * Creates a new instance and sets the handler that should be used for all
     * newly created BOs.
     *
     * @param businessEventHandler The handler to be used
     */
    public BusinessEventSourceSetup(BusinessEventHandlerBase businessEventHandler) {
        this.businessEventHandler = businessEventHandler;
    }

    /**
     * Method called after a business object was created
     *
     * @param e Event containing information about the creation
     */
    public void objectCreated(BusinessObjectCreationEvent e) {
        Object createdObject = e.getCreatedObject();

        if (createdObject instanceof BusinessEventSource) {
            ((BusinessEventSource)createdObject).setBusinessEventHandler(businessEventHandler);
        }
    }
}