/*****************************************************************************
    Class         BusinessEventHandlerBase
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      May 2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/06/26 $
*****************************************************************************/
package com.sap.isa.core.businessobject.event;

import javax.servlet.http.HttpServletRequest;

import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;


/**
 * This interface must be implement by a class which will handle events.
 * It will used by all objects which implement the <code>BusinessEventSource</code>
 * interface to fire <code>BusinessEvent</code>s
 *
 * @see BusinessEvent BusinessEventSource
 *
 */

public interface BusinessEventHandlerBase {

    /**
     * Fired a general business event
     *
     */
    public void fireBusinessEvent(BusinessEvent event);


    /**
     * set the request
     * 
     * @param request 
     */   
    public void setRequest(HttpServletRequest request);

    /**
     * Set the the metaBom
     *
     * @param metaBom
     *
     */
    public void setMetaBom(MetaBusinessObjectManager metaBom);


    /**
     * Returns <code>true</code> if missing infos should be read from backend.
     * @return boolean
     */
    public boolean isReadMissingInfoFromBackend();
 
 
    /**
     * Returns if the business event handle is active. <br>
     * 
     * @return <code>true</code> if teh handler is active
     */
    public boolean isActive();
 
 
    /**
     * Sets a flag, if missing infos should be read from backend..
     * @param readMissingInfoFromBackend flag to set
     */
    public void setReadMissingInfoFromBackend(boolean readMissingInfoFromBackend);
 
 


}
