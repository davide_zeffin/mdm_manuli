/*****************************************************************************
    Class         BusinessEventSource
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      May 2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/06/26 $
*****************************************************************************/
package com.sap.isa.core.businessobject.event;


/**
 * Allows implementing object which could fire events.
 * Therefore the object get a reference to the BusinessEventHandlerBase interface
 * which could fire business events
 * <br>
 * <i>Only for ISA-Core:<b>
 * Implementing this interface tells the BusinessObjectManager to give the
 * object a reference to the BusinessEventHandler.
 * </i>
 *
 * @see com.sap.isa.core.businessobject.BusinessEventHandlerBase
 *
 * @author  Wolfgang Sattler
 * @version 1.0
 *
 */

public interface BusinessEventSource {

    /**
     *
     * Set the <code>BusinessEventHandlerBase</code> which can used to fire BusinessEvents
     *
     * @param eventHandler reference to a <code>BusinessEventHandler</code>
     */
    public void setBusinessEventHandler(BusinessEventHandlerBase eventHandler);

}
