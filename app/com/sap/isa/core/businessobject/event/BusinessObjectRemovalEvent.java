/*****************************************************************************
    Class:        BusinessObjectRemovalEvent
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Thomas Smits
    Created:      20.2.2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/07/20 $
*****************************************************************************/

package com.sap.isa.core.businessobject.event;

/**
 * Event fired during the removal of business objects
 *
 * @author Thomas Smits
 * @version 1.0
 */
public class BusinessObjectRemovalEvent {

    private Object removedObject;

    /**
     * Returns a reference to the object removed by the BOM. If you
     * want to allow the garbage collector the totally removal of an
     * object, drop all references to this object.
     *
     * @return the removed object
     */
    public Object getRemovedObject() {
        return removedObject;
    }

    /**
     * Creates a new instance of the Event
     *
     * @param removedObject a reference to the object removed by the BOM
     */
    public BusinessObjectRemovalEvent(Object removedObject) {
        this.removedObject = removedObject;
    }
}