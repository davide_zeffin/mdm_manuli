/*****************************************************************************
    Class         BusinessEventTealeafCapturer
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      May 2001
    Version:      1.0

    $Revision: #19 $
    $Date: 2001/11/20 $
*****************************************************************************/

package com.sap.isa.core.businessobject.event.capturer.tealeaf;

import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import com.sap.isa.core.businessobject.event.capturer.CapturerEvent;
import com.sap.isa.core.businessobject.event.capturer.CapturerException;
import com.sap.isa.core.businessobject.event.capturer.EventCapturer;
import com.sap.isa.core.businessobject.event.capturer.EventCapturerFactory;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.tealeaf.Manager;
import com.tealeaf.pipeline.CapturePipeline;


/**
 * This class capture the Business events with Tealeaf API to TealealCapturer
 *
 * @see BusinessEventManager
 * @see BusinessEventLogger
 * @see BusinessEvent
 *
 */
public class BusinessEventTealeafCapturer  implements EventCapturer,
                                                      EventCapturerFactory {

    protected static final IsaLocation log =
                             IsaLocation.getInstance(BusinessEventTealeafCapturer.class.getName());

    private static final String OK        = "OK";
    private static final String IMPL_TYPE = "tealeaf";

    private CapturePipeline eventCapturer;


    public BusinessEventTealeafCapturer() {
    }

    public EventCapturer getEventCapturer(Properties properties) {
	
		if(log.isDebugEnabled())
			log.debug("Getting event capturer ");
        String propertyFile = properties.getProperty("property-file");
        String type         = properties.getProperty("type","SAPJ2EE");
        String app          = properties.getProperty("app", "SAP Internet Sales");

        try {
            eventCapturer = Manager.getPipeline();
            if (eventCapturer == null){
            	if(log.isDebugEnabled())
					log.debug("Initializing the tealeaf pipeline with properties " + properties);
				eventCapturer = Manager.init(propertyFile, type, app);
				if(log.isDebugEnabled())
					log.debug("Initialized the tealeaf pipeline " + eventCapturer);
            }
            
        } catch (Throwable ex) {

            if (log.isDebugEnabled())
               log.debug("Exception occured initializing TeaLeaf:", ex);
            else {
				log.error(LogUtil.APPS_BUSINESS_LOGIC,"wec.tealeaf.initfailed", ex);
            }

            throw new CapturerException(ex.getMessage());
        }

        return this;
    }

    public void release() {
        if (eventCapturer == null)
            return;

        Manager.shutDown();
        eventCapturer = null;
    }

    public String getImplementation() {
        return IMPL_TYPE;
    }

    public void publishEvent(CapturerEvent event) {
        if (event == null)
            return;

        event.publish();
    }


    public CapturerEvent createEvent(HttpServletRequest request,
                                     String name, String status) {
        return new CapturerEventTealeafImpl(request, name, status);
    }
    public CapturerEvent createEvent(HttpServletRequest request, String name) {
        return createEvent(request, name, OK);
    }

}
