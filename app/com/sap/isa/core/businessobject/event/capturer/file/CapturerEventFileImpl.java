/*****************************************************************************
    Class         CapturerEventFileImpl
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Franz Mueller
    Created:      13 December 2001
    Version:      1.0

    $Revision:$
    $Date:$
*****************************************************************************/

package com.sap.isa.core.businessobject.event.capturer.file;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import com.sap.isa.core.businessobject.event.capturer.CapturerEvent;



/**
 *  Implementation of file publishing events
 */
public class CapturerEventFileImpl implements  CapturerEvent {

/*
    private static final IsaLocation log = 
                             IsaLocation.getInstance(CapturerEventFileImpl.class.getName());
*/
    
    private static final String CR = System.getProperty("line.separator");

    private StringBuffer sb;
    private BusinessEventFileCapturer fileCapturer;
    private String name;
	private boolean ignoreEmptyValues = false;


    public CapturerEventFileImpl(HttpServletRequest request, String name, 
                                    BusinessEventFileCapturer fileCapturer ) {
                                        
           this.fileCapturer = fileCapturer;
           sb = new StringBuffer(CR);
           sb.append("<EVENT name=\"").append(name).append("\">").append(CR);
    }


	/**
	 * Returns if emtpy values should be ignore for the event output.
	 * 
	 * @return <true>, if empty values will be ignored in the <code>setProperty</code> method.
	 */
	public boolean isIgnoreEmptyValues() {
		return ignoreEmptyValues;
	}


	/**
	 * Set the switch to ignored empty values within the event output
	 * 
	 * @param ignoreEmptyValues
	 */
	public void setIgnoreEmptyValues(boolean ignoreEmptyValues) {
		this.ignoreEmptyValues = ignoreEmptyValues;
	}

    public CapturerEventFileImpl(HttpServletRequest request, 
                                    String name, String status ) {
		this.name = name;                                    	
    }
    
    
	/**
	 * This method give you the name of the event.
	 * @return the name of the event
	 */
	public String getName() {
		return name;
	}

    
    public void setProperty(String group, String name, String value) {

    	if ((!ignoreEmptyValues || value.length() > 0) && name.length() > 0) {
			sb.append("\t").append('<').append(group).append(':');
		    sb.append(name).append('=').append("\"").append(value).append("\"");
			sb.append('>').append(CR);
		}
    }
    
    public void setProperty(String group, String name, Date value) {
        setProperty(group, name, String.valueOf(value));
    }
    
    /**
     * @see com.sap.isa.core.businessobject.event.capturer.CapturerEvent#addGroup(java.lang.String)
     */
    public void addGroup(String group) {
    }
    
    public void publish() {
        sb.append("</EVENT>").append(CR);
        
        fileCapturer.write(sb.toString());
    }        
    
	/* 
	 * Always returns false, as removing a property of an
	 * event is not possible for file based capturer
	 * @see com.sap.isa.core.businessobject.event.capturer.CapturerEvent#removeProperty(java.lang.String, java.lang.String)
	 */
	public boolean removeProperty(String group, String property) {
		//always return false. As removing a property is not possible for 
		//file based capturer
		return false;
	}

}
