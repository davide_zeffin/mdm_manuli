/*****************************************************************************
    Class         CapturerEventTealeafImpl
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Franz Mueller
    Created:      13 December 2001
    Version:      1.0

    $Revision:$
    $Date:$
*****************************************************************************/
 
package com.sap.isa.core.businessobject.event.capturer.tealeaf;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import com.sap.isa.core.businessobject.event.capturer.CapturerEvent;
import com.sap.isa.core.logging.IsaLocation;
import com.tealeaf.sdk.BusinessEventGroup;
import com.tealeaf.sdk.StandardBizEvent;
import com.tealeaf.util.TLTIllegalArgumentException;


/**
 *
 */
public class CapturerEventTealeafImpl extends   StandardBizEvent
                                      implements   CapturerEvent {

    protected static final IsaLocation log = 
                             IsaLocation.getInstance(CapturerEventTealeafImpl.class.getName());

    protected BusinessEventGroup businessEventGroup = null;

	private String name;
	private boolean ignoreEmptyValues = false;


    public CapturerEventTealeafImpl(HttpServletRequest request, 
                                    String name, String status ) {
        super(request, name, status);
        this.name = name;
        businessEventGroup = null;                                        
    }
    
    
    public BusinessEventGroup getGroup(String group) {
        
        if(businessEventGroup == null) {
            try {
                businessEventGroup = mThisEvent.getGroup(group);
 
                if(businessEventGroup == null)
                    businessEventGroup = mThisEvent.createGroup(group);
 
            }
            catch(TLTIllegalArgumentException ex) {
 
                if(log.isDebugEnabled())
                    log.debug("Exception occured :", ex);
                else
                    ex.printStackTrace();
            }
        }
 
        return businessEventGroup;
    }
 
    public void addGroup(String group) {
 
        try {
            businessEventGroup = mThisEvent.createGroup(group);
        }
        catch(TLTIllegalArgumentException ex) {
 
            if(log.isDebugEnabled())
                log.debug("Exception occured :", ex);
            else
                ex.printStackTrace();
        }
 
  	}


	/**
	 * Returns if emtpy values should be ignore for the event output.
	 * 
	 * @return <true>, if empty values will be ignored in the <code>setProperty</code> method.
	 */
	public boolean isIgnoreEmptyValues() {
		return ignoreEmptyValues;
	}


	/**
	 * Set the switch to ignored empty values within the event output
	 * 
	 * @param ignoreEmptyValues
	 */
	public void setIgnoreEmptyValues(boolean ignoreEmptyValues) {
		this.ignoreEmptyValues = ignoreEmptyValues;
	}
  	
	/**
	 * This method give you the name of the event.
	 * @return the name of the event
	 */
	public String getName() {
		return name;
	}

    
    public void setProperty(String group, String name, String value) {

		if (!ignoreEmptyValues || value.length() > 0) {        
	        BusinessEventGroup bGroup = getGroup(group);
	        if (bGroup != null)
	            bGroup.setVariable(name, value);
		}
    }

    public void setProperty(String group, String name, Date value) {
        setProperty(group, name, super.formatDate(value));
    }
    
    public void publish() {
        mThisEvent.sendAndClose();
        businessEventGroup = null;
    }        


	/* 
	 * @see com.sap.isa.core.businessobject.event.capturer.CapturerEvent#removeProperty(java.lang.String, java.lang.String)
	 */
	public boolean removeProperty(String group, String property) {     
		BusinessEventGroup bGroup = getGroup(group);
		if (bGroup != null){
			if( bGroup.getVariable(property) != null){ 
				//check to verify the property is added previously to the group
				bGroup.removeVariable(property);
				return true;
			}
		}
		return false;
	}

} 
