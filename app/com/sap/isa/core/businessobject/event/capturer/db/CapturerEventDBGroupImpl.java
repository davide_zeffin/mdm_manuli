/*****************************************************************************
    Class         CapturerEventDBGroupImpl
    Copyright (c) 
    Author:       pb
    Created:      02 December 2004
*****************************************************************************/
package com.sap.isa.core.businessobject.event.capturer.db;

import java.util.Properties;

/**
 * Event Group contains the properties
 */
public class CapturerEventDBGroupImpl implements CapturerEventDBGroup {

	private Properties properties = new Properties();
	private String name;


	/**
	 * @param group
	 */
	public CapturerEventDBGroupImpl(String groupName) {
		name = groupName;
	}	
	
	/* 
	 */
	public void setProperty(String propName, String propValue) {
		properties.put(propName, propValue);
	}

	/* 
	 */
	public boolean removeProperty(String propName) {
		return 
			(properties.remove(propName) == null) ? false : true; 
	}


	public String getName(){
		return name;
	}
	
	public void setName(String aName){
		name = aName;
	}

	/* 
	 * @see com.sap.isa.core.businessobject.event.capturer.db.CapturerEventDBGroup#addProperty(java.lang.String, java.lang.String)
	 */
	public void addProperty(String property, String value) {
		properties.put(property, value) ; 
	}

	/*
	 * @see com.sap.isa.core.businessobject.event.capturer.db.CapturerEventDBGroup#getProperty(java.lang.String)
	 */
	public String getProperty(String property) {
		return properties.getProperty(property);
	}
}
