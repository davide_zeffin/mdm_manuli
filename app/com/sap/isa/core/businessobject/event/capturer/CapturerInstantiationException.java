/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Franz Mueller
  Created:      12 December 2001

  $Revision: #1 $
  $Date:$
*****************************************************************************/
package com.sap.isa.core.businessobject.event.capturer;


/**
 * CapturerInstantiationException occures whenever the CapturerFactory is
 * unable to instantiate a Capturer for a given type.
 */
public class CapturerInstantiationException extends RuntimeException {
    public CapturerInstantiationException(String message) {
         super(message);
    }
}