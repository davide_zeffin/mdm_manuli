/*****************************************************************************
	Class         CapturerEventDBGroupImpl
	Copyright (c) SAP AG
	Author:       pb
	Created:      04 January 2005
	Version:      1.0

*****************************************************************************/

package com.sap.isa.core.businessobject.event.capturer.db;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Vector;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.sap.isa.core.logging.IsaLocation;

/**
 * Commits the contents of the business events to the database
 */
public class BusinessEventDBCapturerThread implements Runnable {

	private Connection connection;
	private Vector pipeline;
	private BusinessEventDBCapturer dbCapturer;
	
	protected static final IsaLocation loc = IsaLocation.getInstance(BusinessEventDBCapturerThread.class.getName());
	
	public BusinessEventDBCapturerThread(Vector pipeline, BusinessEventDBCapturer dbCapturer){
		
		this.pipeline = pipeline;
		this.dbCapturer = dbCapturer;
		
	}
	
	/* 
	 * Create the datasource and commit the content to the DB
	 * @see java.lang.Runnable#run()
	 */
	public void run() {
		try {
			InitialContext context = new InitialContext();
			DataSource ds = dbCapturer.getDatasource();
			connection = ds.getConnection();

			if(connection == null ){
				loc.info("Web event capturing is disabled as the connection to database system failed " );
				return;
			}
			
			connection.setAutoCommit(false);
			insertIntoDB(connection, pipeline);
			connection.commit();
			connection.close();
		} catch (SQLException e) {
			loc.debug("Error occured while writing events to DB :"+ e); 
		} catch (NamingException e) {
			loc.info("Web event capturing failed due to naming server problems "+ e);
		}finally{
			if (connection != null){
				try {
					if (! connection.isClosed())
						connection.close();
				} catch (SQLException exceptionalException) {
					loc.debug(exceptionalException.getMessage());
					//ignore
				}
			}
		}
	}
		
	
	
	/**
	 * Does the actual work of commiting content to the DB
	 * @param connection - DB connection
	 * @param eventPipeline - vector consisting of event(s) 
	 */
	private synchronized  void insertIntoDB(Connection connection, Vector events) {
		String sql = "INSERT INTO CRM_ISA_WEC_EVENT ( " +
					"WEB_SESSN , HIT_ID , CUSTOMER , WEB_LOGIN, WEB_APP , WEB_EVENT , WEB_EVTYP , WEB_GROUP , WEB_COMSTA, " +
					"BBP_PO_ID, MATERIAL ,  WEB_BASCAT , WEB_BASKET, WEB_CATTYP, WEB_ERROR,  WEB_NUM_IT, WEB_TVALUE , " +
					"WEB_UPRICE , UNIT, CURRENCY  , CALDAY , EVENT_TIME ) " +
			"VALUES ( ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? )" ; 

			
			for (int i=0; i< events.size(); i++ ){
				CapturerEventDBImpl event = (CapturerEventDBImpl) events.elementAt(i);
				Enumeration groupList = event.getGroupList();
				if (groupList != null){
					while (groupList.hasMoreElements()){
						try {
							CapturerEventDBGroupImpl group= (CapturerEventDBGroupImpl) groupList.nextElement();

							PreparedStatement stmt = connection.prepareStatement(sql);										
							stmt.setString(1, event.getSessionId()); //WEB_SESSN 
							stmt.setInt(2, dbCapturer.nextHitID()); //HIT_ID 		20, 
							stmt.setString(3, trim(group.getProperty("CustomerID"),10)); //CUSTOMER 
							stmt.setString(4, trim(group.getProperty("LoginName"),20)); //WEB_LOGIN
							stmt.setString(5, trim(dbCapturer.getAppName(),32)); //WEB_APP 
							stmt.setString(6, trim(event.getName(),20)); //WEB_EVENT 
							stmt.setString(7, trim(dbCapturer.getEventType(),5)); //WEB_EVTYP 
							stmt.setString(8, trim(group.getName(),15)); //WEB_GROUP 
							stmt.setString(9, trim("",0)); //WEB_COMSTA
							stmt.setString(10, trim(group.getProperty("OrderID"),10)); //BBP_PO_ID
							stmt.setString(11, trim(group.getProperty("SKU"),18)); //MATERIAL 
							stmt.setString(12, trim(group.getProperty("BasketCategory"),20)); //WEB_BASCAT
							stmt.setString(13, trim(group.getProperty("BasketID"),32)); //WEB_BASKET
							stmt.setString(14, trim(group.getProperty("Category"),20)); //WEB_CATTYP
							stmt.setString(15, trim("",0)); //WEB_ERROR
							stmt.setBigDecimal(16, convertToBigDecimal(group.getProperty("Quantity"))); //WEB_NUM_IT 17,3
							stmt.setBigDecimal(17, convertToBigDecimal(group.getProperty("TotalPrice"))); //WEB_TVALUE 20,2, 
							stmt.setBigDecimal(18, convertToBigDecimal( group.getProperty("UnitPrice"))); //WEB_UPRICE 20,2 
							stmt.setString(19, trim(group.getProperty("Unit"),3)); //UNIT 
							stmt.setString(20, trim(group.getProperty("Currency"),5)); //CURRENCY 
							stmt.setString(21, event.getEventDay()); //CALDAY  YYYYMMDD
							stmt.setString(22, event.getEventTime()); //EVENT_TIME HHMMSS
							
							
							/**
							 * missing properties 
							 * PO ID/Order id
							 * completion status
							 * error code
							 * 
							 */
							stmt.execute();
						} catch (RuntimeException ex) {
							loc.debug("Record aborted... Exception occured while inserting WEC record " + ex);
						} catch (SQLException e) {
							loc.debug("Record aborted... Exception occured while inserting WEC record " + e);
						}
					}
				}

			}

		if(connection!= null){
			try {
				connection.commit();
			} catch (SQLException e) {
				loc.debug("Error occured while commiting the record " + e);
			}
		}

	}

	/**
	 * Trims the string to the specified length
	 * @param str
	 * @param length
	 * @return
	 */
	private String trim(String str, int length){
		if ((str == null) || (length <= 0) || ("".equals(str)))
			return null;
		if( str.length() > length ){
			str =  str.substring(0, length);
		}
		return str;
    	
	}
    
    /**
     * Helper utility to convert string to decmal
     * @param str number in string format
     * @return decimal
     */
	private BigDecimal convertToBigDecimal(String str){
		 BigDecimal decimalVal = null;
    	 
		 if( (str != null)&& (str.length() > 0)){
			try {
				decimalVal = new BigDecimal(str);
			} catch (RuntimeException e) {
				loc.debug("Unable to convert string to decimal :"+ str); //ignore the error
				decimalVal = null;
			}
		 }
		 return decimalVal;
	}
}
