/*****************************************************************************
    Interface     EventCapturer
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Franz Mueller
    Created:      12 December  2001
    Version:      1.0

    $Revision:$
    $Date:$
*****************************************************************************/

package com.sap.isa.core.businessobject.event.capturer;


import javax.servlet.http.HttpServletRequest;

/**
 *   Capturerhandling interface.
 */

public interface EventCapturer {

    public void release();

    public String getImplementation();

    public void publishEvent(CapturerEvent event);    
    public CapturerEvent createEvent(HttpServletRequest request, String name);
    public CapturerEvent createEvent(HttpServletRequest request, String name, 
                                                                 String status);

}
