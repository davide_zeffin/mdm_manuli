/*
 *
 */
package com.sap.isa.core.businessobject.event.capturer.db;

/**
 *Event group consists of properties
 */
public interface CapturerEventDBGroup{
	public void setProperty(String propName, String propValue);
	public boolean removeProperty(String propName);
	public String getName();
	public void setName(String aName);
	public void addProperty(String property, String value);
	public String getProperty(String property);
						
}
