/*****************************************************************************
    Class         CapturerEventDBImpl
    Author:       pb
    Created:      1 December 2004
    Version:      1.0

    $Revision:$
    $Date:$
*****************************************************************************/

package com.sap.isa.core.businessobject.event.capturer.db;


import java.sql.Time;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import com.sap.isa.core.businessobject.event.capturer.CapturerEvent;
import com.sap.isa.core.logging.IsaLocation;



/**
 *  Implementation of database publishing of events
 *  Event consists of event groups
 */
public class CapturerEventDBImpl implements  CapturerEvent {

    private static final IsaLocation log = 
                             IsaLocation.getInstance(CapturerEventDBImpl.class.getName());

    
    private BusinessEventDBCapturer dbCapturer;
    private String name;
	private boolean ignoreEmptyValues = false;
	private Vector groupList = null;
	private String sessionId ;
	private Calendar time;
	/**
	 * 
	 * Initializes the event sessionid and time at which the event occured.
	 * @param request
	 * @param name Name of the event
	 * @param dbCapturer Event capturer
	 */
    public CapturerEventDBImpl(HttpServletRequest request, String name, 
                                    BusinessEventDBCapturer dbCapturer ) {
           this.dbCapturer = dbCapturer;
           sessionId = request.getSession().getId();
           if (sessionId != null ){
           		if(sessionId.length() >  32 ){
					sessionId = sessionId.substring(sessionId.length()-32, sessionId.length());
           		}
				//BW upload fails for non alpha chars and lower case
				sessionId = sessionId.replace(')', ' '); 
				//few J2EE engines have cookie with the char )
				sessionId = sessionId.toUpperCase();
           }
           time = Calendar.getInstance();
           groupList = new Vector(1,2);
           this.name = name;
    }

	/**
	 * Returns the day (YYYY-MM-DD) when the event occured.
	 * @return Date in jdbc escape format yyyy-MM-dd
	 */
	public java.sql.Date getEventDayInJDBCFormat(){
		String jdbcDate = time.get(Calendar.YEAR)
								+ "-"
								+ ( time.get(Calendar.MONTH)+ 1 )
								+ "-"
								+ time.get(Calendar.DATE);
		return java.sql.Date.valueOf(jdbcDate);
	}
	
	
	/**
	 * Returns the event time (local time of the server) in HHmmss
	 * @return time in jdbc escaped hh:mm:ss
	 */
	public Time getEventTimeInJDBCFormat(){
		String jdbcTime = time.get(Calendar.HOUR_OF_DAY)
							+ ":"
							+ time.get(Calendar.MINUTE)
							+ ":"
							+ time.get(Calendar.SECOND);
		return java.sql.Time.valueOf(jdbcTime);
	}
	
	/**
	 * Returns the session id to which the event belongs
	 * @return sessionid
	 */
	public String getSessionId(){
		return sessionId;
	}
	
	/**
	 * Returns if emtpy values should be ignore for the event output.
	 * 
	 * @return <true>, if empty values will be ignored in the <code>setProperty</code> method.
	 */
	public boolean isIgnoreEmptyValues() {
		return ignoreEmptyValues;
	}


	/**
	 * Set the switch to ignored empty values within the event output
	 * 
	 * @param ignoreEmptyValues
	 */
	public void setIgnoreEmptyValues(boolean ignoreEmptyValues) {
		this.ignoreEmptyValues = ignoreEmptyValues;
	}

    public CapturerEventDBImpl(HttpServletRequest request, 
                                    String name, String status ) {
		this.name = name;                                    	
    }
    
    
	/**
	 * returns the name of the event.
	 * @return the name of the event
	 */
	public String getName() {
		return name;
	}

    /**
     * Inserts the property into the event
     */
    public void setProperty(String groupname, String property, String value) {

    	if ((!ignoreEmptyValues || value.length() > 0) && (property != null) && (property.length() > 0) && (groupname != null) ) {
				CapturerEventDBGroup group =  getGroup(groupname);
				if( group != null){
					group.addProperty(property, (value == null)? "" : value.toUpperCase());
				}
				else{
					log.debug("trying to a property to a non existing group "+ property + ":"+ value);
				}
		}
		else{
			log.debug("group or property is null ");
		}
    }
    
    public void setProperty(String group, String name, Date value) {
    	setProperty(group, name, String.valueOf(value));
    }
    
    /**
     * @see com.sap.isa.core.businessobject.event.capturer.CapturerEvent#addGroup(java.lang.String)
     */
    public void addGroup(String group) {
    	if((group == null) || "".equals(group))
    		return;
		group = group.toUpperCase();
		
    	groupList.add(createGroup(group));
    }
    
    
    private CapturerEventDBGroup createGroup(String group){
    	return new CapturerEventDBGroupImpl(group);
    }
    
    private void addGroup(CapturerEventDBGroup group){
    	groupList.add(group);
    }
	
	/**
	 * Write the content of the event into the DB
	 */    
    public void publish() {
		dbCapturer.publishEvent(this);
    }        
    
	/* 
	 *Removes the property from the event
	 * @see com.sap.isa.core.businessobject.event.capturer.CapturerEvent#removeProperty(java.lang.String, java.lang.String)
	 */
	public boolean removeProperty(String groupname, String property) {
		if ((groupname != null) && (property != null)){
			CapturerEventDBGroup group =  getGroup(groupname);
			if( group != null)
				return group.removeProperty(property);
			else {
				log.debug("trying to remove the property on an empty group " + property);
			}
		} else{ 
			log.debug("trying to remove an empty property  " + property);
		}
		
		return false;
	}

	/**
	 * REturns the group corresponding to the name <code> groupName </code>
	 * @param groupName name of the group
	 * @return Group object
	 */
	public CapturerEventDBGroup getGroup(String groupName){
		CapturerEventDBGroup group = null;
		if( groupName != null){
			//keep looking for a group from the end of the group list
			//this assures the last group be picked
			groupName = groupName.toUpperCase();

			for (int i= groupList.size(); i > 0; i-- ){
				group = (CapturerEventDBGroup) groupList.elementAt(i-1);
				if ( groupName.equalsIgnoreCase( group.getName())){
					return group;
				}
			}
			//if the group doesnot exist in the list
			if (group == null){
				group = createGroup(groupName);
				addGroup(group);
			}
		} 
		
		return group;
	}
	
	public String getProperty(String groupname, String property) {
		String value ="";
		if ( (property != null) && (property.length() > 0) && (groupname != null) ) {
			CapturerEventDBGroup group =  getGroup(groupname);
			if( group != null)
				value = group.getProperty(property);
		}
		return value;
	}
	
	public Enumeration getGroupList(){
		return groupList.elements();
	}

	/**
	 * Returns the day (YYYYMMDD) when the event occured.
	 * @return Date in BW format yyyyMMdd
	 */
	public String getEventDay() {
		return time.get(Calendar.YEAR)
		+ ""
		+ appendZeroIfNeeded(( time.get(Calendar.MONTH)+ 1 ))
		+ ""
		+ appendZeroIfNeeded(time.get(Calendar.DATE));
	}

	/**
	 * Returns the event time (local time of the server) in HHmmss
	 * @return time in BW internal format
	 */
	public String getEventTime() {
		return appendZeroIfNeeded(time.get(Calendar.HOUR_OF_DAY))
		+ ""
		+ appendZeroIfNeeded(time.get(Calendar.MINUTE))
		+ ""
		+ appendZeroIfNeeded(time.get(Calendar.SECOND));
		
	}
	
	/**
	 * Append 0 to the beginning, if the number has one digit
	 * @param num 
	 * @return
	 */
	private String appendZeroIfNeeded(int num){
		return (num < 10) ? ( "0"+num) : (""+ num);	
	}
	
}
