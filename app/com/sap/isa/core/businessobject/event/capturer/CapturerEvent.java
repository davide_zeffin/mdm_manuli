/*****************************************************************************
    Class         CapturerEvent
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP
    Created:      May 2001
    Version:      1.0

    $Revision: #19 $
    $Date: 2001/11/20 $
*****************************************************************************/

package com.sap.isa.core.businessobject.event.capturer;

import java.util.Date;


/**
 *   Events participating in the capturer notification 
 *   should implement this interface.
 */

public interface CapturerEvent {
   
   	/**
 	 * This method publish the event. 
 	 */
	public void publish();
   
   
	/**
	 * Returns if emtpy values should be ignore for the event output.
	 * 
	 * @return <true>, if empty values will be ignored in the <code>setProperty</code> method.
	 */
	public boolean isIgnoreEmptyValues();


	/**
	 * Set the switch to ignored empty values within the event output
	 * 
	 * @param ignoreEmptyValues
	 */
	public void setIgnoreEmptyValues(boolean ignoreEmptyValues);

   
    /**
     * This method give you the name of the event.
     * @return the name of the event
     */
    public String getName();
    
   
    /**
     * Add a new group to the event.  
     * 
     * @param group
     */
    public void addGroup(String group);
   
   
    /**
     * Set a property of the event.<b>
     * If the group does not exist, it will be add implicity added.
     * 
     * @param group name of the group, the property belongs to
     * @param property  name of the property
     * @param value value to be set
     */
    public void setProperty(String group, String property, String value);      
   
   
	/**
	 * Set a property of the event.<b>
	 * If the group does not exist, it will be add implicity added.
	 * 
	 * @param group name of the group, the property belongs to
	 * @param property  name of the property
	 * @param value date value to be set
	 */
    public void setProperty(String group, String property, Date   value);
    
    /**
     * Removes the property of theevent
     * @param group name of the group, the property belongs to
     * @param property name of the property
     * @return true if removing of property is successful
     * false otherwise
     */
    public boolean removeProperty(String group, String property);      
}
