/*****************************************************************************
    Class         BusinessEventTealeafCapturer
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Franz Mueller
    Created:      25 January 2002
    Version:      1.0

    $Revision:$
    $Date:$
*****************************************************************************/

package com.sap.isa.core.businessobject.event.capturer;

import java.util.Date;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;


/**
 * This class is a default -do-nothing- implementation of the 
 * EventCapturer and CapturerEvent
 *
 */
public class BusinessEventDummyCapturer  implements CapturerEvent,
                                                    EventCapturer,
                                                    EventCapturerFactory {

	private String name;

    public BusinessEventDummyCapturer() {
    }

	public BusinessEventDummyCapturer(String name) {
		this.name = name;
	}

    public EventCapturer getEventCapturer(Properties properties) {
        return this;
    }

    public void release() {
    }

    public String getImplementation() {
        return "DUMMY";
    }

    public void publishEvent(CapturerEvent event) {
    }


    public CapturerEvent createEvent(HttpServletRequest request,
                                     String name, String status) {
               
										                                     		
        return new BusinessEventDummyCapturer(name);
    }
    
    public CapturerEvent createEvent(HttpServletRequest request, String name) {
		return new BusinessEventDummyCapturer(name);
    }

    public void addGroup(String group) {
    }
 
    public void setProperty(String group, String para, String value) {
    }
    
    public void setProperty(String group, String para, Date   value) {
    }
    
    public void publish() {
    }

	/**
	 * Returns if emtpy values should be ignore for the event output.
	 * 
	 * @return <true>, if empty values will be ignored in the <code>setProperty</code> method.
	 */
	public boolean isIgnoreEmptyValues() {
		return false;
	}


	/**
	 * Set the switch to ignored empty values within the event output
	 * 
	 * @param ignoreEmptyValues
	 */
	public void setIgnoreEmptyValues(boolean ignoreEmptyValues) {
	}	


    /** 
     * @see com.sap.isa.core.businessobject.event.capturer.CapturerEvent#getName()
     */
    public String getName() {
        return this.name;
    }

	/* 
	 * Dummy capturer always returns false
	 * @see com.sap.isa.core.businessobject.event.capturer.CapturerEvent#removeProperty(java.lang.String, java.lang.String)
	 */
	public boolean removeProperty(String group, String property) {
		return false;
	}

}

