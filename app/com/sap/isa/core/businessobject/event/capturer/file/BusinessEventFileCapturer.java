/*****************************************************************************
    Class         BusinessEventFileCapturer
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Franz Mueller
    Created:      18-Dez 2001
    Version:      1.0

    $Revision:$
    $Date:$
*****************************************************************************/

package com.sap.isa.core.businessobject.event.capturer.file;

import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import com.sap.isa.core.businessobject.event.capturer.CapturerEvent;
import com.sap.isa.core.businessobject.event.capturer.EventCapturer;
import com.sap.isa.core.businessobject.event.capturer.EventCapturerFactory;
import com.sap.isa.core.logging.IsaLocation;


/**
 *  A Capturer protocolling the CapturerEvents to a (log-)file
 */
public class BusinessEventFileCapturer  implements EventCapturer,
                                        EventCapturerFactory {

    protected static final IsaLocation file = IsaLocation.getInstance("FileCapturer");
    
    protected static final String IMPL_TYPE = "file";


    public BusinessEventFileCapturer() {
    }

    public EventCapturer getEventCapturer(Properties properties) {

        return this;
    }

    public void release() {
    }

    public String getImplementation() {
        return IMPL_TYPE;
    }

    public void publishEvent(CapturerEvent event) {
        if (event == null)
            return;

        event.publish();
    }


    public CapturerEvent createEvent(HttpServletRequest request, String name) {
        return new CapturerEventFileImpl(request, name, this);
    }

    public CapturerEvent createEvent(HttpServletRequest request,
                                     String name, String status) {
        return createEvent(request, name);
    }


    protected void write(String s) {
        file.log("INFO", "system.capturer.output", new String[] {s}, null);
    }

}
