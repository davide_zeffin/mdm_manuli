/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Franz Mueller
  Created:      12 December 2001

  $Revision: #1 $
  $Date:$
*****************************************************************************/
package com.sap.isa.core.businessobject.event.capturer;


/**
 * CapturerException is a wrapper above all the exceptions from
 * a capturer
 */
public class CapturerException extends RuntimeException {
    public CapturerException(String message) {
         super(message);
    }
}