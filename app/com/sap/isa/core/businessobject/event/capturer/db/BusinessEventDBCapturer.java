/*****************************************************************************
    Class         BusinessEventDBCapturer
    Copyright (c) SAP AG
    Author:       pb
    Created:      02 December 2004
*****************************************************************************/

package com.sap.isa.core.businessobject.event.capturer.db;

import java.util.Properties;
import java.util.Vector;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import com.sap.isa.core.businessobject.event.capturer.CapturerEvent;
import com.sap.isa.core.businessobject.event.capturer.EventCapturer;
import com.sap.isa.core.businessobject.event.capturer.EventCapturerFactory;
import com.sap.isa.core.db.DBHelper;
import com.sap.isa.core.logging.IsaLocation;
 

/**
 * Capturer to capture the business events into RDBMS
 * The captured content can be uploaded into BW for analysis.
 * Various reports provided in BW to analyze the health of the webshop.
 * This functionality does not require any extra software. Events are captured into Database
 * and uploaded into Business warehouse using DBconnect or UDConnect.
 * 
 * Extend <code>publishEvent</code> method to customize. Eg: adding new events with 
 * properties not existing in the existing events. If the newly created event
 * uses the properties used by SAP delivered events, no custom development is required
 * 
 * 
 */
public class BusinessEventDBCapturer  implements EventCapturer,
                                        EventCapturerFactory {

    protected static final IsaLocation loc = IsaLocation.getInstance(BusinessEventDBCapturer.class.getName());
    
    protected static final String IMPL_TYPE = "DATABASE";
    protected static final String PLACE_ORDER = "PLACE_ORDER";
	protected static final String LOGIN = "LOGIN";
	protected static final String DEFAULT_APP_NAME = "SAP ESELLING";
	protected static final String DEFAULT_EVENT_TYPE = "SAPJ2EE";
	/**
	 * fallback datasource name
	 * Default datasource name is : java:comp/env/SAP/CRM.<applicationname>
	 */
	protected static String FALLBACK_DATASOURCE_NAME = "java:comp/env/SAP/CRM_ISA_WEC_DB"; 
	
		
	protected static short DEFAULT_EVENTPOOL_SIZE = 100;
	
	private static int hitID = 0;
	private Vector eventPipeline = new Vector(10,10);
	protected Properties capturerProps;
	

	protected static short EVENTPOOL_SIZE = DEFAULT_EVENTPOOL_SIZE;
	
	private String appName = DEFAULT_APP_NAME;
	private String eventType = DEFAULT_EVENT_TYPE; 	
	
    public BusinessEventDBCapturer() {
    }

	/**
	 * Returns the initialized DB event capturer
	 */
    public EventCapturer getEventCapturer(Properties properties) {
		capturerProps = properties;
		eventType = properties.getProperty("type",DEFAULT_EVENT_TYPE).toUpperCase();
		appName = properties.getProperty("app", DEFAULT_APP_NAME).toUpperCase();
		String eventPoolSize = properties.getProperty("eventPoolSize"); 
		if( (eventPoolSize != null ) && !("".equals(eventPoolSize)) ){
			try {
				EVENTPOOL_SIZE = Short.parseShort(eventPoolSize);
			} catch (NumberFormatException e) {
				// ignore the exception
				EVENTPOOL_SIZE = DEFAULT_EVENTPOOL_SIZE;
			}
		}
        return this;
    }

	public String getAppName(){
		return appName;
	}
	
	public String getEventType(){
		return eventType;
	}
	
	
    public void release() {
    	publishPipeline();
    }

	protected void finalize() throws Throwable {
		try {
			release();
		} finally {
			super.finalize();
		}
	}

	
    public String getImplementation() {
        return IMPL_TYPE;
    }

	/**
	 * Get the connection from the datasource and
	 * publish the event data into the database
	 * @param event
	 */

    public void publishEvent(CapturerEvent event) {
        if (event == null){
			loc.debug("tried to publish null event");
			return;
        }
        else {
        	eventPipeline.add(event);
        	if (
        		(eventPipeline.size() > EVENTPOOL_SIZE) ||
        		(LOGIN.equalsIgnoreCase(event.getName())) || 
        		(PLACE_ORDER.equalsIgnoreCase(event.getName())) ){
					publishPipeline();
        		}
        }
        	
    }

    /**
     * Publishes the event pipeline in a new thread
	 * @param eventPipeline
	 */
	private synchronized void publishPipeline() {
		//clone the event list and release it to start publishing in a thread
		// So that the parent thread doesn't have to wait longer for the child thread to 
		// finish up
		

		synchronized (eventPipeline){
			Vector clonedPipeline = (Vector) eventPipeline.clone();
			BusinessEventDBCapturerThread capturerThread = new BusinessEventDBCapturerThread( clonedPipeline, this);
			Thread worker = new Thread(capturerThread);
			worker.start();
			eventPipeline =  new Vector (10, 10);
			
		}
		
	}



	public CapturerEvent createEvent(HttpServletRequest request, String name) {
        return new CapturerEventDBImpl(request, name, this);
    }

    public CapturerEvent createEvent(HttpServletRequest request,
                                     String name, String status) {
        return createEvent(request, name);
    }
    

    /**
     * Common counter for all worker threads that commit events to the DB
     * @return integer counter
     */
    public synchronized int nextHitID(){
    	return hitID = ((++hitID) % Integer.MAX_VALUE ) ; 
    }
    
    /**
     * Returns the datasource 
     * First tries the application's common datasource java:comp/env/SAP/CRM.<applicationname>
     * Incase of failure to lookup, the application looks up fallback datasource
     * @return datasource 
     */
    public DataSource getDatasource() throws NamingException {
    	return DBHelper.getApplicationSpecificDataSource(FALLBACK_DATASOURCE_NAME);
    }
    
    protected void setFallbackDatasourceName(String name){
		FALLBACK_DATASOURCE_NAME = name;
    }
}
