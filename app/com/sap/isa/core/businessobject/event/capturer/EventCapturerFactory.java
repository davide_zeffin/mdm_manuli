/*****************************************************************************
    Interface     EventCapturerFactory
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Franz Mueller
    Created:      12 December 2001
    Version:      1.0

    $Revision:$
    $Date:$
*****************************************************************************/

package com.sap.isa.core.businessobject.event.capturer;

import java.util.Properties;

public interface EventCapturerFactory {

    public EventCapturer getEventCapturer(Properties properties);

}
