/*****************************************************************************
    Class         BusinessEventCapturerFactory
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Created:      May 2001
    Version:      1.0

    $Revision:$
    $Date:$
*****************************************************************************/

package com.sap.isa.core.businessobject.event.capturer;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.Enumeration;
import java.util.Properties;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import com.sap.isa.core.FrameworkConfigManager;
import com.sap.isa.core.init.Initializable;
import com.sap.isa.core.init.InitializationEnvironment;
import com.sap.isa.core.init.InitializeException;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.security.SecureStore;
import com.sap.isa.core.util.MiscUtil;
import com.sap.isa.core.xcm.config.ComponentConfig;
import com.sap.isa.core.xcm.config.ComponentConfigContainer;
import com.sap.isa.core.xcm.init.ExtendedConfigInitHandler;
import com.sap.isa.core.xcm.xml.XMLUtil;


/**
 *
 *
 */
public class BusinessEventCapturerFactory implements Initializable {


    private static IsaLocation log = IsaLocation.getInstance(BusinessEventCapturerFactory.class.getName());

    private static String capturerFactoryName =
         "com.sap.isa.core.businessobject.event.capturer.BusinessEventDummyCapturer";

    private static BusinessEventDummyCapturer dummyCapturer = null;

    private static boolean _initialized = false;

    private static Properties _props;
    private static EventCapturerFactory capturerFactory = null;



    /**
     *   Called once from the ActionServlet.init.
     *   Transfers all the properties to the Capturer-Handler class (with the
     *   following exceptions:
     *     - capturer-handler will not be transfered;
     *       it is only for the BECapturerFactory
     *     - if there is a property-file attribute, its real-path name (the full
     *       qualified file-name) will be passed to the Handler
     *     - all the other properties wil be passed 'without touching' them
     *
     *   This method stores all the informations but will not instanciate
     *   the handler !
    */
    public void initialize(InitializationEnvironment env,
                           Properties props) throws InitializeException {

        try {
        	log.debug("Initializing BusinessEventFactory");
			ComponentConfigContainer ccc = FrameworkConfigManager.XCM.getApplicationScopeConfig();
			ComponentConfig compContainer = ccc.getComponentConfig("wec", "wecconfig");
							
			String isWecEnabled = compContainer.getParamConfig().getProperty("enable.BEventCapturing", "false");
            if("false".equalsIgnoreCase(isWecEnabled)){
            	log.info("wec.disabled");
				return;
            }else{
				log.info("wec.enabled.inprogress");
            }
            	
            String propertyFile = props.getProperty("property-file");
            if (propertyFile != null && propertyFile.length() > 0)
               propertyFile = env.getRealPath(propertyFile);


            String className=props.getProperty("capturer-handler");
            if (className != null && className.length() > 0)
                capturerFactoryName = className;

            log.debug( "Use capture handler: " + capturerFactoryName);
            
            _props = new Properties();
            for (Enumeration enumeration=props.propertyNames(); enumeration.hasMoreElements();) {
                String property = (String)enumeration.nextElement();

                String value;

                if (property.equals("capturer-handler"))
                    continue;
                else if (property.equals("property-file"))
                    value = propertyFile;
                else
                    value = props.getProperty(property);

                _props.setProperty(property, value);
            }

            Class type = Class.forName(capturerFactoryName);
            capturerFactory = (EventCapturerFactory)type.newInstance();
            if("com.sap.isa.core.businessobject.event.capturer.tealeaf.BusinessEventTealeafCapturer".equals(capturerFactoryName)){
            	//retrieve context param configfile.tealeaf.isa.sap.com
            	String tealeafConfigFile;
				
				tealeafConfigFile = compContainer.getParamConfig().getProperty("configfile.tealeaf");

  		        if ( (tealeafConfigFile != null ) && (tealeafConfigFile.trim().length() > 1)){
  		        		//use the configuration file mentioned via the parameter configfile.tealeaf 
					log.info("Using the tealeaf configuration file mentioned via XCM param configfile.tealeaf" + tealeafConfigFile); 	
  		        	tealeafConfigFile = MiscUtil.replaceSystemProperty(tealeafConfigFile);
  		        }else{
  		        	// Preapare the configuration file from the template
					log.info("Using the generated tealeaf configuration file");
					tealeafConfigFile = getTealeafConfigurationFile(env, compContainer.getParamConfig());
            	}
            	if( tealeafConfigFile != null){
            		log.debug("Using the tealeaf configuration file " + tealeafConfigFile);
            		
            		_props.setProperty("property-file", tealeafConfigFile);
            	}
            }

            //force an instantiation
            capturerFactory.getEventCapturer(_props);

            //log.info("system.capturer.name", new Object[] { capturerFactoryName }, null);
            _initialized = true;
            log.info("wec.enabled.initialized");

        } catch (Exception ex) {
            capturerFactory = null;
            log.error("wec.error.initialization" , new Object[]{ BusinessEventDummyCapturer.class.getName() }, ex );
            new InitializeException(ex.toString());
        }
    }

    /**
     * Generates Tealeaf configuration file from configuration template, if necessary.
     * Returns the generated configuration file
	 * @param compContainer XCM component 
	 * @return name of the configuration file generated
	 */
	private String getTealeafConfigurationFile(InitializationEnvironment env, Properties wec) throws Exception {
		
		String tealeafCfgTemplate = env.getRealPath("WEB-INF/cfg/TeaLeafJ2EE.xsl");
		
		File tealeafCfgTemplFile = new File(tealeafCfgTemplate);
		
		if ((tealeafCfgTemplFile == null) || !(tealeafCfgTemplFile.canRead()) ){
			log.error("wec.tealeaf.cfgfilemissing" , new Object[]{ tealeafCfgTemplate }, null);
			throw new Exception("Tealeaf configuration template file missing " + tealeafCfgTemplate);
		}
		
		String tealeafCfg = ExtendedConfigInitHandler.getXCMCacheDir() + "/TeaLeafJ2EE.xml";
		
		File tealeafCfgFile = new File(tealeafCfg); 
				
		generateTealeafConfigurationFile(tealeafCfgTemplFile, tealeafCfgFile, wec);
		
		return tealeafCfg;
	}

	/**
	 * Generates tealeafConfiguration file from template
	 * @param tealeafCfgTemplFile tealeaf configuration template
	 * @param tealeafCfgFile tealeaf configuration file to be generated
	 * @param compContainer XCm component configuration
	 */
	private void generateTealeafConfigurationFile(File tealeafCfgTemplFile, File tealeafCfgFile, Properties wec) throws TransformerException {
		ByteArrayInputStream is	= new  ByteArrayInputStream("<xml/>".getBytes());
		StreamResult res= new StreamResult(tealeafCfgFile);
		if( log.isDebugEnabled())
			log.debug("Transforming the tealeaf configuration Template :" + tealeafCfgTemplFile.getAbsolutePath() );
		try {
			  
			Transformer tf=XMLUtil.getTransformerFactory(this).newTransformer(new StreamSource(tealeafCfgTemplFile));
			tf.setParameter("tealeaf.logdir", wec.getProperty("tealeaf.logdir","C:\\temp"));
			tf.setParameter("tealeaf.server", wec.getProperty("tealeaf.server" , "localhost") );
			tf.setParameter("tealeaf.port", wec.getProperty("tealeaf.port" , "1966") );
			tf.setParameter("tealeaf.user", wec.getProperty("tealeaf.user", "user") );
			String pwd = wec.getProperty("tealeaf.password" );
			if ( (pwd != null) && (pwd.trim().length() > 1) ){
				pwd = SecureStore.retrieve(pwd);
			}
			tf.setParameter("tealeaf.password", pwd );
			
			tf.transform(new StreamSource(is), res);
			log.info("tealeaf configuration file successfully generated" + tealeafCfgFile.getAbsolutePath() );
			
		} catch (TransformerConfigurationException e) {
			log.error("wec.error.cfg.transformation", new Object[]{
													tealeafCfgTemplFile.getAbsolutePath(),
													tealeafCfgFile.getAbsolutePath() },
						e);
			throw e;
		} catch (TransformerFactoryConfigurationError e) {
			log.error("wec.error.cfg.transformation", new Object[]{
										tealeafCfgTemplFile.getAbsolutePath(),
										tealeafCfgFile.getAbsolutePath() },
				e);
			throw e;
		} catch (TransformerException e) {
			log.error("wec.error.cfg.transformation", new Object[]{
										tealeafCfgTemplFile.getAbsolutePath(),
										tealeafCfgFile.getAbsolutePath() },
				e);
			throw e;
		}

		
	}

	public void terminate() {
      dummyCapturer = null;
      _props = null;
      capturerFactory = null;
    }

    /**
     *   There may be attemps to obtain a EventCapturer befor the initialization
     *   is done. This flag indicates whether the init is executed.
     *   Bad design :(
     */
    public static boolean isInitialized() {
        return _initialized;
    }

    /**
     *   returns an instance of the capturer handler
     */
    public static EventCapturer getEventCapturer() {
        return (capturerFactory != null)?
                    capturerFactory.getEventCapturer(_props):
                    getDummyCapturer();
    }


    public static synchronized EventCapturer getDummyCapturer() {
        if (dummyCapturer == null)
            dummyCapturer = new BusinessEventDummyCapturer();

        return dummyCapturer;
    }

}
