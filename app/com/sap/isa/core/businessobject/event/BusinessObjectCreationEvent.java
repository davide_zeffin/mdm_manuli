/*****************************************************************************
    Class:        BusinessObjectCreationEvent
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Thomas Smits
    Created:      20.2.2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/07/20 $
*****************************************************************************/

package com.sap.isa.core.businessobject.event;

/**
 * Event fired during the creation of business objects
 *
 * @author Thomas Smits
 * @version 1.0
 */
public class BusinessObjectCreationEvent {

    private Object createdObject;

    /**
     * Returns a reference to the object created by the BOM.
     *
     * @return the created object
     */
    public Object getCreatedObject() {
        return createdObject;
    }

    /**
     * Creates a new instance of the Event
     *
     * @param createdObject a reference to the object created by the BOM
     */
    public BusinessObjectCreationEvent(Object createdObject) {
        this.createdObject = createdObject;
    }
}