/*****************************************************************************
    Class         BusinessEventCapturerBase
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      May 2001
    Version:      1.0

    $Revision: #2 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.core.businessobject.event;

import javax.servlet.http.HttpServletRequest;

import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;


/**
 * Classes which would be listen to Business Events must implement this interface
 * and then register on <code>BusinessEventHandleBaseImpl</code>
 *
 * @see com.sap.isa.core.businessobject.event.BusinessEvent
 * @see com.sap.isa.core.businessobject.event.BusinessEventHandlerBase
 * @see com.sap.isa.core.businessobject.event.BusinessEventHandlerBaseImpl
 *
 */
public interface BusinessEventCapturerBase {


    /**
     * The capture type is an unique identifier for the capturer, which allows
     * to remove a Capturer from the BusinessEventHandlerBaseImpl
     *
     * @return returns the Capture Type
     *
     */
    public String getCaptureType();


    /**
     * Capture a general business event
     *
     */
    public void captureBusinessEvent(BusinessEvent event,
                                     MetaBusinessObjectManager metaBom,
                                     HttpServletRequest request);


}
