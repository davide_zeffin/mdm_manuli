/*****************************************************************************
    Class         BusinessEventHandlerBaseImpl
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      May 2001
    Version:      1.0

    $Revision: #2 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.core.businessobject.event;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.logging.IsaLocation;


/**
 * This class is a base for classes which handles business events
 * and capture the events with the <code>BusinessCapturerBase</code> interface.
 *
 * @see com.sap.isa.core.businessobject.event.BusinessEvent
 * @see com.sap.isa.core.businessobject.event.BusinessEventHandlerBase
 * @see com.sap.isa.core.businessobject.event.BusinessEventCapturerBase
 *
 */
public class BusinessEventHandlerBaseImpl implements BusinessEventHandlerBase {
	protected static IsaLocation log =
		IsaLocation.getInstance("com.sap.isa.core.businessobject.event.BusinessEventHandlerBaseImpl");

    protected MetaBusinessObjectManager metaBom;
    protected HttpServletRequest request;
    protected List listener = new ArrayList();

    protected boolean readMissingInfoFromBackend = false;
    
    /**
     * constructor
     *
     */
    public BusinessEventHandlerBaseImpl() {
    }

    /**
     * Constructs a new object already providing a reference to the
     * bom and the request.
     *
     * @param mbom A reference to the meta business object manager
     * @param request A reference to the request
     */
    public BusinessEventHandlerBaseImpl(MetaBusinessObjectManager metaBom,
                                        HttpServletRequest request) {
        this.metaBom = metaBom;
        this.request = request;
    }

    /**
     * Returns if the business event handle is active. <br>
     * The event handler is active if at least one listern is registered 
     * 
     * @return <code>true</code> if at least one listern is registered
     */
    public boolean isActive() {
        return !listener.isEmpty();
    }
 

    /**
     * Set the property metaBom
     *
     * @param metaBom
     *
     */
    public void setMetaBom(MetaBusinessObjectManager metaBom) {
        this.metaBom = metaBom;
    }


    /**
     * Returns the property metaBom
     *
     * @return metaBom
     *
     */
    public MetaBusinessObjectManager getMetaBom() {
       return metaBom;
    }

    /**
     * Returns <code>true</code> if missing infos should be read from backend.
     * @return boolean
     */
    public boolean isReadMissingInfoFromBackend() {
        return readMissingInfoFromBackend;
    }
 
 
   /**
     * Sets a flag, if missing infos should be read from backend..
     * @param readMissingInfoFromBackend flag to set
     */
    public void setReadMissingInfoFromBackend(boolean readMissingInfoFromBackend) {
        this.readMissingInfoFromBackend = readMissingInfoFromBackend;
    }
 

    /**
     * Set the property request
     *
     * @param request
     *
     */
    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }


    /**
     * Returns the property request
     *
     * @return request
     *
     */
    public HttpServletRequest getRequest() {
       return request;
    }


    /**
     * register an BusinessEventCapturerBase to obtain events
     *
     */
    public void register(BusinessEventCapturerBase capturer) {
        listener.add(capturer);
    }


    /**
     * remove an BusinessEventCapturerBase with the given capturer type
     *
     */
    public void remove(String captureType) {

        int size = listener.size();
        for (int i= 0; i < size; i++) {
            String type = ((BusinessEventCapturerBase)listener.get(i)).getCaptureType();
            if (type.equals(captureType)) {
                listener.remove(i);
                break;
            }
        }

    }


    /**
     * Fired a general business event
     *
     */
    public void fireBusinessEvent(BusinessEvent event) {
        if (log.isDebugEnabled()) 
        	log.debug("Firing business event " + event.getSource());
        Iterator i = listener.iterator();
        while (i.hasNext()) {
            BusinessEventCapturerBase eventCapturer = (BusinessEventCapturerBase)i.next();
            eventCapturer.captureBusinessEvent(event, metaBom, request);
        }
		if (log.isDebugEnabled()) 
			log.debug("Fired business event " + event.getSource());
    }

}
