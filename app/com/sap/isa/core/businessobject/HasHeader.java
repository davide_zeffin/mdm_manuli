/*****************************************************************************
    Interface:    HasHeader
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Thomas Smits
    Created:      20.2.2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/07/20 $
*****************************************************************************/

package com.sap.isa.core.businessobject;


/**
 * Allows generic access to objects, which has an header object.
 * <br>
 *
 * @author Wolfgang Sattler
 * @version 1.0
 *
 */
public interface HasHeader {

  public ObjectBase getHeader();

}