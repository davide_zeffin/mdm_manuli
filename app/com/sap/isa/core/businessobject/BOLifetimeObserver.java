/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Franz Mueller
  Created:      24 July 2001

  $Header: //java/sapm/esales/30/src/com/sap/isa/core/businessobject/BOLifetimeObserver.java#2 $
  $Revi$
*****************************************************************************/
package com.sap.isa.core.businessobject;



/**
 * This interface has to be implemented by all Business Object Managers
 * which are managed by the MetaBusinessObjectManager and should be aware 
 * of the notified about the creation and removal of the BOs
 */
public interface BOLifetimeObserver {
    public void removalNotification(Object obj);       
    public void creationNotification(Object obj);
}