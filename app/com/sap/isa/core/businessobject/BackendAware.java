/*****************************************************************************
    Interface:    BackendAware
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Thomas Smits
    Created:      20.2.2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/07/20 $
*****************************************************************************/

package com.sap.isa.core.businessobject;

import com.sap.isa.core.eai.BackendObjectManager;

/**
 * Allows implementing object interaction with the backend using the
 * backend manager.
 * <br>
 * Implementing this interface tells the BusinessObjectManager to give the
 * object a reference to the BackendObjectManager.
 * You need this reference to call methods of the back end logic
 * (BE).
 *
 * @see BackendObjectManager BackendObjectManager
 *
 * @author Thomas Smits
 * @version 1.0
 *
 */
public interface BackendAware {

  /**
   * Method the object manager calls after a new object is created and
   * after the session has timed out, to remove references to the
   * BackendObjectManager. The standard implementation of this method
   * stores a reference to the bem for later use.
   * <b>Note - </b>At the end of the sessions lifetime, this method is
   * called with the value <code>null</code> to avoid cyclic references.
   *
   * @param bom Reference to the <code>BackendObjectManager</code>
   *            object at creation time or <code>null</code> at the end
   *            of the objects lifetime.
   */
  public void setBackendObjectManager(BackendObjectManager bom);

}