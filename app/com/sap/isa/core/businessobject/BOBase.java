package com.sap.isa.core.businessobject;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.sap.isa.core.RequestContext;
import com.sap.isa.core.businessobject.boi.BOBaseData;
import com.sap.isa.core.logging.IsaSession;
import com.sap.isa.core.system.RequestContextContainer;

/**
 * This class is the base class for all Business Object managed
 * by the Business Object Manager
 */
public class BOBase implements BOBaseData {

    protected Map extensionData = new HashMap(0);

    /**
     * Constructor
     */
    public BOBase() {
    }

    /**
     * This method stores arbitrary data within this Business Object
     * @param name key with which the specified value is to be associated.
     * @param value value to be associated with the specified key.
     */
    public void addExtensionData(Object name, Object value) {
        extensionData.put(name, value);
    }

    /**
    * This method retrieves extension data associated with the
    * Business Object
    * @param name key with which the specified value is to be associated.
    * @return value which is associated with the specified key
    */
    public Object getExtensionData(Object name) {
        return extensionData.get(name);
    }


    /**
     * This method retrieves all extension data associated with the
     * Business Object
     */
    public Set getExtensionDataValues() {
        return extensionData.entrySet();
    }


    /**
     * This method removes extension data from the Business Object
     */
    public void removeExtensionData(Object key) {
        extensionData.remove(key);
    }


    /**
     * This method removes all extensions data from the Business Object
     */
    public void removeExtensionDataValues() {
        extensionData.clear();
    }


    /**
     * Makes a copy of the object.<br>
     *
     * The HashMap is copied but not the references within!
     *
     * @returns a copy of object.
     *
     */
    protected Object clone() throws CloneNotSupportedException {

        BOBase bob = (BOBase)super.clone();

        bob.extensionData = (HashMap) ((HashMap)extensionData).clone();

        return bob;
    }

    /**
     * sets the extension map to the given map
     *
     * @param extensionData the new extension HashMap for the object
     *
     */
    public void setExtensionMap(HashMap extensionData) {
        this.extensionData = extensionData;
    }

     /**
     * returns the extension map
     *
     * @return extensionData, the extension Map of the object
     *
     */
    public Map getExtensionMap() {
        return extensionData;
    }

	/**
	 * Returns the request context. This context can be used to pass data between layers
	 * of the ISA framework.
	 * The context has the scope of an request (HTTP request).
	 * <p/><b>NOTE:</b><br/> 
	 * This context MUST NOT be used in standard development. It is a tool for quickly extending the 
	 * functionality of the standard in CUSTOMER projects. 
	 * <br/>
	 * @return the request context
	 */
	public RequestContext getRequestContext() {
		return RequestContextContainer.getInstance().getRequestContext();
	}
}