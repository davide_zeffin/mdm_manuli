/*****************************************************************************
    Interface:    ObjectBaseData
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Thomas Smits
    Created:      29.3.2001
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.core.businessobject.boi;

import java.util.Iterator;

import com.sap.isa.core.TechKey;

/**
 * Represents the common base of all backend data interfaces.
 *
 * @author Thomas Smits
 * @version 1.0
 */
public interface ObjectBaseData extends BOBaseData {

    /**
     * Retrieves the key for the object.
     *
     * @return The object's key
     */
    public TechKey getTechKey();

    /**
     * Sets the key for the document.
     *
     * @param key Key to be set
     */
    public void setTechKey(TechKey techKey);


    /**
     *
     * The method returns an iterator over all sub objects of the BusinessObject
     *
     * @return Iterator to loop over sub objects
     *
     */
    public Iterator getSubObjectIterator();


    /**
     *
     * The method returns an reference to a sub objects for a given tech key
     *
     * @return sub object
     *
     */
    public ObjectBaseData getSubObject(TechKey techKey);


    /**
     *
     * This method creates a unique handle, as an alternative key for the business object,
     * because at the creation point no techkey for the object exists. Therefore
     * maybay the handle is needed to identify the object in backend
     *
     */
    public void createUniqueHandle();


    /**
     * This method sets the handle, as an alternative key for the business object,
     * because at the creation point no techkey for the object exists. Therefore
     * maybay the handle is needed to identify the object in backend
     *
     * @param handle the handle of business object which identifies the object
     * in the backend, if the techkey still not exists
     *
     */
    public void setHandle(String handle);


   /**
    *
    * This method returns the handle, as an alternative key for the business object,
    * because at the creation point no techkey for the object exists. Therefore
    * maybay the handle is needed to identify the object in backend
    *
    * return the handle of business object which is needed to identify the object
    * in the backend, if the techkey still not exists
    *
    */
    public String getHandle();


}