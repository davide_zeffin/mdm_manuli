/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      24 October 2001

  $Header:  $
  $Revision:  $
  $Date: $
*****************************************************************************/
package com.sap.isa.core.businessobject.boi;

import java.util.Set;

/**
 * This interface provived access to extension data of a Business Object
 */
public interface BOBaseData {

    /**
     * This method stores arbitrary data within this Business Object
     * @param name key with which the specified value is to be associated.
     * @param value value to be associated with the specified key.
     */
    public void addExtensionData(Object name, Object value);

    /**
    * This method retrieves extension data associated with the
    * Business Object
    * @param name key with which the specified value is to be associated.
    * @return value which is associated with the specified key
    */
    public Object getExtensionData(Object name);

    /**
     * This method removes extension data from the Business Object
     */
    public void removeExtensionData(Object key);


    /**
     * This method retrieves all extension data associated with the
     * Business Object
     */
    public Set getExtensionDataValues();


    /**
     * This method removes all extensions data from the Business Object
     */
    public void removeExtensionDataValues();


}