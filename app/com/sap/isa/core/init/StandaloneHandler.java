/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      19 March 2001


  $Revision: #2 $
  $Date: 2001/07/03 $
*****************************************************************************/
package com.sap.isa.core.init;

// java imports
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Hashtable;

import org.apache.struts.util.MessageResources;
import org.apache.struts.util.MessageResourcesFactory;

import com.sap.isa.core.FrameworkConfigManager;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.xcm.ConfigContainer;
import com.sap.isa.core.xcm.ExtendedConfigManager;
import com.sap.isa.core.xcm.init.ExtendedConfigInitHandler;

/**
 * This handler can be used to initialize a environment in a non-web
 * scenario e.g. when you are testing your application
 * To set up the <code>StandaloneHanlder</code> perform the following steps:
 * <ul>
 * <li>Create an object of type <code>StandaloneHanlder</code></li>
 * <li>Set the class name of the message resources property file using
 * <code>setClassNameMessageResources</code> method. This property file must
 * be located in the classpath</li>
 * <li>Set the base dir using the <code>setBaseDir</code> method. All configuration
 * files which are used to� initialize the environment will be searched
 * reliative to this path</li>
 * <li>Set the path (relative to the base dir) of the initialization xml-file</li>
 * <li>Call the <code>initialize</code> method of the <code>StandaloneHandler</code>
 * </ul>
 * <br>
 * <pre>
 *  ...
 *		StandaloneHandler standaloneHandler = new StandaloneHandler();
 *		standaloneHandler.setBootStrapInitConfigPath("/WEB-INF/xcm/sap/system/bootstrap-config.xml");
 *		standaloneHandler.setInitConfigPath("/WEB-INF/xcm/sap/system/init-config.xml");
 *		standaloneHandler.setClassNameMessageResources("com.sap.isa.ISAResources");
 *		standaloneHandler.setBaseDir("C:/SAP_J2EEngine6.20_00/cluster/server/services/servlet_jsp/work/jspTemp/b2b_40_SP_COR/root");
 * 	    Hashtable parameters = new Hashtable();
 * 		standaloneHandler.setParameter(parameters);
 *		parameters.put("customer.config.path.xcm.config.isa.sap.com",  "/WEB-INF/xcm/customer/configuration");
 *		standaloneHandler.initialize();
 *    ...
 *
 * </pre>
 */
public class StandaloneHandler implements InitializationEnvironment {
  protected static IsaLocation log = IsaLocation.
        getInstance(StandaloneHandler.class.getName());

  private String initConfigPath;
  private String bootStrapInitConfigPath;
  private String classNameMessageResources;
  private String baseDir = "";
  private Hashtable params;

  /**
   * The resources object for our application resources (if any).
   */
  protected MessageResources messageResources = null;
  // the initialization manager
  protected InitializationHandler initHandler;



  /**
   * Initializes this handler
   */
  public void initialize() throws InitializeException {

	try {
	  	initializeBootstrap();
	    initMessageResource();
	    initHandler = new InitializationHandler(initConfigPath);
	    initHandler.initialize(this, null);
    }
    catch (Throwable ex){
		log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.exception", ex);
    }
  }
  
  private void initializeBootstrap() throws InitializeException
  {
	   // perform only if XCM activated
	   // vni: At this time, XCM is not active it will be activated
	   // inside the bootstrapInitHandler, that's why 
	   // isXCMActive would be false and wouldn't be initialized now
	   // if (FrameworkConfigManager.XCM.isXCMActive()) 
	   {
			InitializationHandler bootstrapInitHandler = new InitializationHandler(bootStrapInitConfigPath);
			bootstrapInitHandler.initialize(this, null);
	   }
  }


  /**
   * Sets the path to the ini-file which initalized this init-handler
   * @param path path to the ini-file which initalized this init-handler
   */
  public void setInitConfigPath(String path) {
    initConfigPath = path;
  }

  /**
   * Return the path to the ini-file which initalized this init-handler
   * @return path to the ini-file which initalized this init-handler
   */
  public String getInitConfigPath() {
    return initConfigPath;
  }

  /**
   * Sets the base directory used to access files using the
   * <code>getResourceAsStream</code> method
   */
  public void setBaseDir(String baseDir) {
    this.baseDir = baseDir;
  }

  /**
   * Gets the base directory used to access files using the
   * <code>getResourceAsStream</code> method
   */
  public String getBaseDir() {
    return baseDir;
  }

  /**
   * Determines the <code>RealPath</code> for a given local path.
   *
   * @param path the name of the resource
   * @return the real absolute path
   */
  public String getRealPath(String path) {
      return baseDir + path;
  }

  /**
   * Set the class name of the file with the message resources
   * @param classNameMessageResources the class name of the file with the message resources
   */
  public void setClassNameMessageResources(String classNameMessageResources) {
    this.classNameMessageResources = classNameMessageResources;
  }

  /**
   * Gets the class name of the file with the message resources
   * @return the class name of the file with the message resources
   */
  public String getClassNameMessageResources() {
    return classNameMessageResources;
  }


   /**
    * determins the environment specific <code>MessageResources<code>
    *
    * @return the MessageResource
    */
   public MessageResources getMessageResources() {
    return messageResources;
  }
  public void setMessageResources(MessageResources resources) {
   messageResources= resources;
 }

   /**
    * Determines the <code>InputStream<code> for a given resource.
    * First this method tries to open a file for the given path. If this
    * fails then the resource is searched in the classpath.
    * The calling instance is responsible to close the InputStream.
    * If an InputStream couldn't be determined, the method has to
    * return <code>null</code>
    *
    *
    * @param path the name of the resource
    * @return the InputStream or <strong>null<strong>
    */
   public InputStream getResourceAsStream(String path) {
    try {
  	 if (ExtendedConfigInitHandler.isActive()) {
		// logging is already initialized
		ExtendedConfigManager xcManager = ExtendedConfigManager.getExtConfigManager();
		// check if file is part of XCM
		if (xcManager.isManagedFile(path)) {
			// get default configuration
			ConfigContainer cc = xcManager.getModifiedConfig(null);
			InputStream is = cc.getConfigAsStream(path);
			if (is != null)
				return is;
			}
		}
      String fullPath = null;
      if (baseDir == null || baseDir.length() == 0)
        fullPath = path;
      else
        fullPath  = new File(baseDir + path).getCanonicalPath();
      File file = new File(fullPath);
      FileInputStream fis = new FileInputStream(file);
      return fis;
    } catch (Exception ex) {
      // try to find the resource in classpath
      return this.getClass().getClassLoader().getResourceAsStream(path);
    }
   }

  /**
    * Inits Struts message resources
    */
  protected void initMessageResource() {

    if (classNameMessageResources == null)
      return;
     // Already initialized
	if (messageResources != null)
	  return;
      MessageResourcesFactory factoryObject =
      		com.sap.isa.core.MessageResourcesFactory.createFactory();
      messageResources = factoryObject.createResources(classNameMessageResources);
      messageResources.setReturnNull(false);
    }

   /**
    * Returns a set of parameter names which belong to the given environment.
    * Parameter values can be retrieved using the <code>getParameter()</code>
    * method
    *
    * @return an Enumeration of String objects containing the names of the
    *         initialization parameters
    * /
    */
    public Enumeration getParameterNames() {
      if (params != null)
        return params.keys();
      else
        return null;
    }

   /**
    * Sets parameter names which belong to the given environment.
    * Parameter values can be retrieved using the <code>getParameter()</code>
    * method
    * @param params a hashtable containing the parameters
    */
    public void setParameter(Hashtable params) {
      this.params = params;
    }

  /**
   * Returns a String containing the value of the named initialization parameter
   * or <code>null</code> if the parameter does not exist.
   * @param name a String containing the name of the parameter whose value is requested
   * Returns a String containing the value of the named initialization parameter
   *         or <code>null</code> if the parameter does not exist.
   */
  public String getParameter(String name) {
    if (params != null)
      return (String)params.get(name);
    else
      return null;
  }
/**
 * Returns the bootStrapInitConfigPath.
 * @return String
 */
public String getBootStrapInitConfigPath() {
	return bootStrapInitConfigPath;
}

/**
 * Sets the bootStrapInitConfigPath.
 * @param bootStrapInitConfigPath The bootStrapInitConfigPath to set
 */
public void setBootStrapInitConfigPath(String bootStrapInitConfigPath) {
	this.bootStrapInitConfigPath = bootStrapInitConfigPath;
}
/**
 * Terminate the component.
 */
public void terminate() {
	initHandler.terminate();
}

}
