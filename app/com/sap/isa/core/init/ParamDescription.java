/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Franz Mueller
  Created:      06 March 2001
  
  $Header: //java/sapm/esales/30/src/com/sap/isa/core/init/ParamDescription.java#1 $
  $Revision: #1 $
  $Date: 2001/06/26 $ 
*****************************************************************************/

package com.sap.isa.core.init;




/**
 * <code>ParamDescription</code> wrapps the informations 
 * from the <strong>param<strong> tag in the configuration file
 * Instances of this class are created through the <code>Digester</code>
 */

public class ParamDescription
{
    private String name;
    private String value;
  

    public void setName(String name) {
    	this.name = name;
    }  
    public void setValue(String value) {
    	this.value = value;
    }  
    
    public String getName() {
    	return name;
    }
    public String getValue() {
    	return value;
    }
}
