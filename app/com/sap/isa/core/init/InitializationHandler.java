/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Franz M�ller
  Created:      06 March 2001


  $Revision: #1 $
  $Date: 2001/06/26 $
*****************************************************************************/

package com.sap.isa.core.init;


import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Properties;
import java.util.Vector;

import org.apache.commons.digester.Digester;
import org.apache.struts.util.MessageResources;
import org.xml.sax.SAXException;

import com.sap.isa.core.ContextConst;
import com.sap.isa.core.EnvironmentManager;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;



/**
 * <strong>InitializationHandler</strong> handles all the
 * required initializations.
 */

public class InitializationHandler implements Initializable
{

    static private InitializationEnvironment environment;

    // name of the class which wrapps the content of each
    // <initialization> tag in the configuration file
    protected String initClass =
                      "com.sap.isa.core.init.InitDescription";

    // name of the class which wrapps the informations from
    // the <param> tag in the configuration file
    protected String paramClass =
                      "com.sap.isa.core.init.ParamDescription";

	/**
	 * Add the following parameter to your init handler if 
	 * you want your init handler to run only on specific J2EE engines
	 * <param name="j2eeengineversion" value="sap620 or sap630"/>
	 */
	public static final String J2EE_ENGINE_VERSION = "run2eeengine";
	
	public static final String J2EE_ENGINE_VERSION_SAP620 = "sap620";
	
	public static final String J2EE_ENGINE_VERSION_SAP630 = "sap630";

	public static final String J2EE_ENGINE_VERSION_NON_SAP = "nonsap";
	
	static private IsaLocation log = IsaLocation.getInstance(InitializationHandler.class.getName());

    protected Vector inits;

    // name of the main initialization configuration file
    private   String initCfg;


    public InitializationHandler(String initCfg) {
	this.initCfg  = initCfg;

	inits = new Vector();
    }

    /**
     * Construct and return a digester that uses the new configuration
     * file format.
     */
    protected Digester initCfgDigester(int detail) {

	// Initialize a new Digester instance
	Digester digester = new Digester();
	digester.push(this);
	digester.setDebug(detail);
	digester.setValidating(false);

	// Configure the processing rules
        digester.addObjectCreate("initializations/initialization",
                                 initClass);
        digester.addSetProperties("initializations/initialization");
        digester.addSetNext("initializations/initialization",
                            "addInitialization",
                            initClass);

        digester.addObjectCreate("initializations/initialization/param",
                                 paramClass);
        digester.addSetProperties("initializations/initialization/param");
        digester.addSetNext("initializations/initialization/param",
                            "addParamDescription",
                            paramClass);

	return (digester);
    }


    protected void performTermination(InitDescription initDescr) {

		final String METHOD = "performTermination()";
		log.entering(METHOD);
		
		if(!checkInitHandler(initDescr, log)){
			log.exiting();
			return;
		}
		
		try {
	        Initializable init = initDescr.getInitializable();
	        init.terminate();
	        init = null;
        }  catch (Throwable ex) { 
        	log.debug(ex.getMessage());
		}
		log.exiting();
    }

    public void performTermination(String className) {
		final String METHOD = "performTermination()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("className="+className);
		try {
		
	        if (className == null)
	           return;
	
	        for (Enumeration enum=inits.elements(); enum.hasMoreElements();) {
	            InitDescription initDescr =
	                                    (InitDescription)enum.nextElement();
	            if (initDescr.getClassName().equals(className)) {
	                performTermination(initDescr);
	                return;
	            }
			}
		} finally {
			log.exiting();
		}
    }

    /**
     * Terminate the component.
     */
     public void terminate() {

		final String METHOD = "terminate()";
		log.entering(METHOD);
		
        for (Enumeration enum=inits.elements(); enum.hasMoreElements();) {

            InitDescription initDescr =
                                    (InitDescription)enum.nextElement();

			long startInit = System.currentTimeMillis();
			log.debug("Begin termination of [class]='" + initDescr.getClassName() + "'");

            performTermination(initDescr);
            
			log.debug("End termination of [class]='" + initDescr.getClassName() + "' in [ms]='" 
					+ (System.currentTimeMillis() - startInit) + "'");
					
		}

		inits = new Vector();
		InitializationHandler.environment = null;
		log.debug("Termination finished");
		log.exiting();
     }



     public void initialize(InitializationEnvironment environment,
			    Properties  properties)
                 throws InitializeException         {
		final String METHOD = "initialize()";
		log.entering(METHOD);
		
	 	InitializationHandler.environment = environment;
	
        MessageResources resource = environment.getMessageResources();
        InputStream input = environment.getResourceAsStream(initCfg);
        try{      
	        if (input == null) {
	            throw new InitializeException(
	                            resource.getMessage("system.configMissing",
			    	                         initCfg));
	        }
	
	    	Digester cfgDigester = initCfgDigester(0);
	
			// Parse the input stream to configure our mappings
			try {
			    cfgDigester.parse(input);
			} catch (IOException ex) {
				log.debug(ex.getMessage());
			    ex.printStackTrace();
			    throw new InitializeException(
			            resource.getMessage("system.configIO", initCfg ));
			} catch (SAXException ex) {
				log.debug(ex.getMessage());
			    ex.printStackTrace();
			    throw new InitializeException(
			            resource.getMessage("system.configParse", initCfg ));
			}
		
			try {
			    input.close();
			} catch (IOException ex) {
				log.debug(ex.getMessage());
			}
	        
			performInitialization();
        } finally{
        	log.exiting();
        }

     }


    protected void performInitialization(InitDescription initDescr)
                       throws InitializeException {
                       	
  	long startInit = System.currentTimeMillis();
	Initializable init = null;
	try {

		if(!checkInitHandler(initDescr, log))
			return;

        init = initDescr.getInitializable();

		log.debug("Begin initalization of [class]='" + init.getClass().getName() + "'");
		
		
		log.debug("Checking if init handler runs only on some J2EE Engine releases");
		
		
		
		Properties initProps = initDescr.getProperties();
		
		init.initialize(environment, initDescr.getProperties());

        }  catch (Throwable ex) {
            MessageResources msgs = environment.getMessageResources();
            log.error(LogUtil.APPS_BUSINESS_LOGIC, "system.initFailed", new String[] {initDescr.getClassName()}, ex);

			String msg = "Error initializing '" + initDescr.getClassName() + "'\n" + ex.toString();
	     	throw new InitializeException(msg,ex);
		}
	
		log.debug("End initalization of [class]='" + init.getClass().getName() + "' in [ms]='" 
					+ (System.currentTimeMillis() - startInit) + "'");
    }

    public void performInitialization(String className)
                       throws InitializeException {
		final String METHOD = "performInitialization()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("className="+className);
		try {
		
	        if (className == null) {
	            MessageResources msgs = environment.getMessageResources();
	
	            // It is safe to use logging now!
	//            IsaLocation log = IsaLocation.getInstance(
	//               "com.sap.isa.core.init.InitializationHandler.performInitialization");
	
	            log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.init.className.missing");
	            return;
	        }
	
	
	        for (Enumeration enum=inits.elements();enum.hasMoreElements();) {
	
	             InitDescription initDescr =
	             			(InitDescription)enum.nextElement();
	             if (initDescr.getClassName().equals(className)) {
	                performInitialization(initDescr);
	                return;
	             }
			}
		} finally{
			log.exiting();
		}
     }

    /**
     * Calls for each <strong>initialization</strong> in the configuration
     * file the <code>initialize</code> method
     *
     * @exception InitializeException if there are any problem related
     *                             to the initialization
     */
    protected void performInitialization()
                       throws InitializeException {

        for (Enumeration enum=inits.elements();enum.hasMoreElements();) {
             InitDescription initDescr =
             			(InitDescription)enum.nextElement();
             performInitialization(initDescr);
		}
    }


    /**
     *  <code>addInitialization</code> is called from the <code>Digester</code>
     *  whenever the pattern <strong>initializations/initialization</strong>
     *  is matched.
     *
     *  @param initDescr this object encapsulates configuration data
     */
    public void addInitialization(InitDescription initDescr) {
        inits.addElement(initDescr);
    }


	public static String getRealPath(String path) {
		return environment.getRealPath(path); 
	}

	public static InputStream getAsInputStream(String path) {
		return environment.getResourceAsStream(path); 
	}	
	
	/**
	 * Returns the content of the 'application.name' context parameter
	 * @return String
	 */
	public static String getApplicationName() {
		if( environment != null )
			return environment.getParameter(ContextConst.APP_NAME);
		return null;
	}
	
	/**
	 * Returns an instance of the language dependent message resources
	 * @return instance of the language dependent message resources
	 */
	public static MessageResources getMessageResources() {
		return environment.getMessageResources();
	}
	
	public static String getMessageResource(String key) {
		return environment.getMessageResources().getMessage(key);
	}
	
	/**
	 * Check if init handler runs on the right j2ee version
	 * @param log reference to ISALocation
	 * @param initProps init properties of init handler
	 * @param clasInitHanlder class of init handler
	 * @return true if the engine has the right version
	 */
	public static boolean isRightJ2EEEngineVersion(IsaLocation log, Properties initProps, String clasInitHanlder) {
		final String METHOD = "isRightJ2EEEngineVersion()";
		log.entering(METHOD);
		try {
		
			String j2eeengineversion = initProps.getProperty(J2EE_ENGINE_VERSION);
			if (j2eeengineversion == null)
				return true;
				
			if (j2eeengineversion.equalsIgnoreCase(J2EE_ENGINE_VERSION_SAP620) && EnvironmentManager.isSAPJ2EE620()) {
					String msg = "Assuming to running on SAP J2EE Engine 6.20. Version dependent init handler enabled.";
					log.info("system.ccms.info", new Object[] { msg }, null);
					return true;
			}
			if (j2eeengineversion.equalsIgnoreCase(J2EE_ENGINE_VERSION_SAP630) && EnvironmentManager.isSAPJ2EE630()) {
				String msg = "Assuming to running on SAP J2EE Engine 6.30. Version dependent init handler enabled.";
				log.info("system.ccms.info", new Object[] { msg }, null);
				return true;
			}
	
			if (j2eeengineversion.equalsIgnoreCase(J2EE_ENGINE_VERSION_NON_SAP) 
					&& !EnvironmentManager.isSAPJ2EE630() 
					&& !EnvironmentManager.isSAPJ2EE620()) {
				String msg = "Assuming to running on non SAP J2EE Engine. Version dependent init handler enabled.";
				log.info("system.ccms.info", new Object[] { msg }, null);
				return true;
			}
			
			
			return false;
		} finally {
			log.exiting();
		}
	}
	
	
	private boolean checkInitHandler(InitDescription initDescr, IsaLocation log) {
		
		if (!isRightJ2EEEngineVersion(log, initDescr.getProperties(), initDescr.getClassName())) {
			log.debug("initalization of [class]='" + initDescr.getClassName() + "' skipped. Wrong J2EE Engine version"); 
			return false;
		}
		try {
			Initializable init = initDescr.getInitializable();

			if (init == null)
				return false;

			if (!initDescr.isClassAvailable()) {
				String msg = "initialization of [class]='" + initDescr.getClassName() + "' skipped. Class not found";
				log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.exception", new String[] {msg}, null);
				return false;
			}
		} catch (Throwable t){
			String msg = "termination of [class]='" + initDescr.getClassName() + "' failed";
			log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.exception", new String[] {msg}, t);
			return false;
		}
		return true;
	}
	
	
}

