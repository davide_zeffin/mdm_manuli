
package com.sap.isa.core.init;

/**
 * <strong>InitializeException</strong> may occure
 * at the initialization state of a specific component.
 */

public class InitializeException extends Exception 
{
    public InitializeException(String msg) {
	  super(msg);	
    }
	public InitializeException(String msg, Throwable cause) {
	  super(msg, cause);	
	}
}
