
package com.sap.isa.core.init;

// java imports
import java.io.InputStream;
import java.util.Enumeration;

import org.apache.struts.util.MessageResources;



/**
 * <strong>InitializationEnvironment</strong> has to be implemented by
 * all initialization hosting components which are aware of the
 * environment and are able to return environment specific
 * informations
 */

public interface InitializationEnvironment
{
     /**
      * determins the environment specific <code>MessageResources<code>
      *
      * @return the MessageResource
      */
     public MessageResources getMessageResources();

     /**
      * dependent on the implementing environment this method
      * determins the <code>InputStream<code> for a given resource
      * The calling instance is responsible to close the InputStream.
      * If an InputStream couldn't be determin, the method has to
      * return <code>null</code>
      *
      * @param path the name of the resource
      * @return the InputStream or <strong>null<strong>
      */
     public InputStream getResourceAsStream(String path);

     /**
      * Determines the <code>RealPath</code> for a given local path.
      * You ought to use getResourceAsStream for most reading purposes,
      * if you need to write to a specific directory relatively to
      * your web-application, you may use this method.
      *
      * @param path the name of the resource
      * @return the real absolute path
      */
     public String getRealPath(String path);

     /**
      * Returns a set of parameter names which belong to the given environment.
      * Parameter values can be retrieved using the <code>getParameter()</code>
      * method
      * @return an Enumeration of String objects containing the names of the
      *         initialization parameters
      * /
      */
    public Enumeration getParameterNames();

    /**
     * Returns a String containing the value of the named initialization parameter
     * or <code>null</code> if the parameter does not exist.
     * @param name a String containing the name of the parameter whose value is requested
     * Returns a String containing the value of the named initialization parameter
     *         or <code>null</code> if the parameter does not exist.
     */
    public String getParameter(String name);
}
