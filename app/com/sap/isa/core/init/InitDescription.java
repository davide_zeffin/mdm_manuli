/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Franz Mueller
  Created:      06 March 2001
  
  $Header: //java/sapm/esales/30/src/com/sap/isa/core/init/InitDescription.java#1 $
  $Revision: #1 $
  $Date: 2001/06/26 $ 
*****************************************************************************/

package com.sap.isa.core.init;


import java.util.Properties;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;


/**
 * <code>InitDescription</code> wrappes the informations from 
 * the initializations' configuration file
 */

public class InitDescription {
    private String name;
    private String className;
    private Initializable initializable;
  
    private Properties props;
    
    public InitDescription() {
    	props = new Properties();
    }

    public void setName(String name) {
    	this.name = name;
    }  
    public void setClassName(String clazz) {
    	this.className = clazz;
    }  
    
    public String getName() {
    	return name;
    }
    public String getClassName() {
    	return className;
    }

    /**
     * <code>addParamDescription</code> is called from the <code>Digester</code> 
     * to fill up the parameters.
     */    
    public void addParamDescription(ParamDescription para) {
    	props.setProperty(para.getName(), para.getValue());
    }
    
    public Properties getProperties() {
    	return props;
    }
    
    /**
     * <code>getInitializable</code> creates an instance corresponding
     * to the <strong>type</strong> attribute.
     * 
     * @exception any of the following Exceptions may occure:
	 	                  <code>InstantiationException</code>
	 	                  <code>IllegalAcessException</code>
	 	                  <code>ClassCastException</code>             all of
	 	                  them are mapped to the generic <code>Exception</code>
     */
    public synchronized Initializable getInitializable() 
    		throws InstantiationException, IllegalAccessException {
    	
    	if (initializable == null) {
    	   
		   // Instantiate the new object and cast it to the appropriate type
		   try {
			Class clazz = Class.forName(className);
			Object instance = clazz.newInstance();
	    	
				initializable = (Initializable)instance;       
			 } catch (Throwable cnfex) {
				IsaLocation log = IsaLocation.getInstance(
				   	"com.sap.isa.core.init.InitDescription.getInitializable");
				log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.initFailed", new String[] {className}, cnfex);
		   } 
	    }
		return initializable;
    }
    
    /**
     * Returns true if the class for initialization is available. 
     * This method should be called after calling getInitializable()
     * @return true if the class for initialization is available.
     */
    public boolean isClassAvailable() {
    	if (initializable == null)
    		return false;
    	else
    		return true;
    }
}
