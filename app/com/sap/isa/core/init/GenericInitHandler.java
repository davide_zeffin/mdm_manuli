/*****************************************************************************
    Class         GenericInitHandler
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      October 2004
    Version:      1.0

*****************************************************************************/
package com.sap.isa.core.init;

import java.util.Properties;

import com.sap.isa.core.init.Initializable;
import com.sap.isa.core.init.InitializationEnvironment;
import com.sap.isa.core.init.InitializeException;
import com.sap.isa.core.logging.IsaLocation;

/**
 * This class provides a generic solution to store properties in the init-config.xml. <br>
 */
public class GenericInitHandler implements Initializable {

    /**
     * Log handler.
     */
    protected static IsaLocation log = IsaLocation.getInstance("com.sap.isa.core.init.GenericInitHandler");
    
    /**
     * Hold the properties provide while Initialization. <br>
     */
    static protected Properties properties;


    /**
    * Initialize the implementing component.
    *
    * @param env the calling environment
    * @param props   component specific key/value pairs
    * @exception InitializeException at initialization error
    */
    public void initialize(InitializationEnvironment env, Properties props)
             throws InitializeException {
		properties = props;             	
    }


    /**
     * Returns an the value for the property with the given name. <br> 
     *
     * @return property value or <code>null</code> if the values doesn't exist.
     *
     */
    public static String getPropertyValue(String name) {
    	if (properties != null) {
			return properties.getProperty(name);
    	}
		return null;
    }


    /**
    * Terminate the component.
    */
    public void terminate() {
    	properties = null;
    }


}
