package com.sap.isa.core.init;


import java.util.Properties;



/**
 * <strong>Initializable</strong> has to be implemented by all 
 * components which requires an initialization 
 * All managed Beans interfaces should extends this interface.
 */

public interface Initializable 
{

    /**
     * Initialize the implementing component.  
     *
     * @param env the calling environment     
     * @param props   component specific key/value pairs
     * @exception InitializeException at initialization error
     */               	     	
     public void initialize(InitializationEnvironment env, Properties props) 
                 throws InitializeException;

    /**
     * Terminate the component.  
     */               	     	
     public void terminate();     
}
