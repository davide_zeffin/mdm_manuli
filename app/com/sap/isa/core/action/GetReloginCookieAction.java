/*****************************************************************************
    Class         GetReloginCookie
    Copyright (c) 2001, SAP AG, All rights reserved.
    Description:  Store the startup parameter in a cookie for an relogin
    Author:       SAP AG
    Created:      August 2003
    Version:      1.0

    $Revision: #25 $
    $Date: 2002/11/05 $
*****************************************************************************/
package com.sap.isa.core.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.Constants;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.CookieUtil;
import com.sap.isa.core.util.JspUtil;
import com.sap.isa.core.util.StartupParameter;

/**
 * This action read all relevant startup parameter for an relogin from a cookie. <br>
 * 
 * * <h4>Overview over request parameters and attributes</h4>
 * <table border="1" cellspacing="0" cellpadding="4">
 *   <tr>
 *     <th>name</th>
 *     <th>parameter</th>
 *     <th>attribute</th>
 *     <th>description</th>
 *   <tr>
 *   <tr>
 *      <td>forward</td><td>X</td><td>&nbsp</td>
 *      <td>logical forward, to use after the action</td>
 *   </tr>
 * </table>
 *
 * <h4>Overview over attributes which will be set in the request context</h4>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <th>attribute</th>
 *     <th>description</th>
 *   <tr>
 * <tr><td>parameter</td><td>list of start parameter.
 *  see {@link com.sap.isa.core.util.StartupParameter.Parameter}</td></tr>
 *  <tr><td>cookieFound</td><td>flag indicated that a cookie was found</td></tr>
 *  <tr><td>isPortal</td><td>flag indicated that the session runs within the portal</td></tr>

 * </table>
 *
 * @see com.sap.isa.core.action.SetReloginCookieAction
 */
public class GetReloginCookieAction extends Action {
	static final private IsaLocation log = IsaLocation.getInstance(GetReloginCookieAction.class.getName());

    /**
     * 
     */
    public GetReloginCookieAction() {
        super();
    }

    /**
     * Read the relogin cookie from the request.
     * 
     * @see com.sap.isa.core.BaseAction#doPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    public ActionForward execute(ActionMapping mapping,
                                  ActionForm form,
                                  HttpServletRequest request,
                                  HttpServletResponse response)
        		throws IOException, ServletException {
		final String METHOD_NAME = "execute()";
		log.entering(METHOD_NAME);
 		boolean cookieFound = false;
		boolean isPortal    = false;
		List parameterList      = new ArrayList(10); 		
 		
 		String cookieContent = CookieUtil.getCookieContent(SetReloginCookieAction.COOKIE_NAME, request); 		

 		if (cookieContent != null) {
 			
			cookieFound = true;
 			
			// now we have a cookie content, which is worth for being parsed.
			// parse the cookie content and seperate all parameter
			JspUtil.SimpleEscapeSafeStringTokenizer readtoken = new JspUtil.SimpleEscapeSafeStringTokenizer(cookieContent, '|');

			while (readtoken.hasMoreTokens()) {
				String token = readtoken.nextToken();
				int index = token.indexOf('=');
				if (index > 0) {
					String name = token.substring(0,index); 
					String value = token.substring(index+1);
					parameterList.add(new StartupParameter.Parameter(name,value,true));
					if (name.equals(Constants.PORTAL) && value.equals("YES")) {
						isPortal = true; 
					}
				}
			}
 		}

		request.setAttribute("parameter", parameterList);
		request.setAttribute("cookieFound", new Boolean(cookieFound));
		request.setAttribute("isPortal", new Boolean(isPortal));		
 		log.exiting();
        return mapping.findForward("success");
    }

}
