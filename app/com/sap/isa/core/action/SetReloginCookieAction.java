/*****************************************************************************
    Class         SetReloginCookie
    Copyright (c) 2001, SAP AG, All rights reserved.
    Description:  Store the startup parameter in a cookie for an relogin
    Author:       SAP AG
    Created:      August 2003
    Version:      1.0

    $Revision: #25 $
    $Date: 2002/11/05 $
*****************************************************************************/
package com.sap.isa.core.action;

import java.io.IOException;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.BaseAction;
import com.sap.isa.core.ContextConst;
import com.sap.isa.core.FrameworkConfigManager;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.util.CookieUtil;
import com.sap.isa.core.util.StartupParameter;

/**
 * This action stores all relevant startup parameter for an relogin in a cookie. <br>
 * The parameters will be separated with the "|" sign. <br>
 * The functionality must be enabled in the <code>web.xml</code> 
 * (see {@link com.sap.isa.core.ContextConst#RELOGIN_COOKIE}).
 * 
 * <h4>Overview over request parameters and attributes</h4>
 * <table border="1" cellspacing="0" cellpadding="4">
 *   <tr>
 *     <th>name</th>
 *     <th>parameter</th>
 *     <th>attribute</th>
 *     <th>description</th>
 *   <tr>
 *   <tr>
 *      <td>forward</td><td>X</td><td>&nbsp</td>
 *      <td>logical forward, to use after the action</td>
 *   </tr>
 * </table>
 *
 * <h4>Overview over attributes which will be set in the request context</h4>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <th>attribute</th>
 *     <th>description</th>
 *   <tr>
 * <tr><td>none</td><td></td></tr>
 * </table>

 *   
 */
public class SetReloginCookieAction extends SetReloginCookieBaseAction {


	/**
	 * The name of the cookie which stores the user's details
	 */
	public static final String COOKIE_NAME = SetReloginCookieBaseAction.COOKIE_NAME;
	
    /**
     * 
     */
    public SetReloginCookieAction() {
        super();
    }

    /**
     * Set the Relogin Cookie.
     * @see com.sap.isa.core.BaseAction#doPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    public ActionForward doPerform(
        ActionMapping mapping,
        ActionForm form,
        HttpServletRequest request,
        HttpServletResponse response)
        	throws IOException, ServletException {
        		
		final String METHOD_NAME = "doPerform()";
		log.entering(METHOD_NAME);
		
		ActionForward forward = super.doPerform( mapping, form, request, response);
		
		String forwardName = request.getParameter("forward");
		
		if (forwardName != null && forwardName.length() > 0) {
			log.exiting();
			return mapping.findForward(forwardName);
		}
		log.exiting();
        return forward;
    }
    
    

}
