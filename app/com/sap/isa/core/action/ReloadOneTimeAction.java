/*****************************************************************************
Class:        ReloadOneTimeAction
Copyright (c) 2006, SAP AG, Germany, All rights reserved.
Author:       SAP AG
Created:      25.01.2006
Version:      1.0

$Revision: #1 $
$Date: 2006/01/25 $ 
*****************************************************************************/

package com.sap.isa.core.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.BaseAction;

/**
 * This Action implements a switch which returns the Action forward <code>success</code>
 * only if the parameter <code>rf=y</code> is available in the request context.<br/>
 * This functionality can be used to call Action chains twice (refresh). Typically this 
 * action would be placed at the end of a such chain. The definition of this action 
 * could look like:<br />
 * <code>
 * <p>
 * &nbsp;&lt;action path=&quot;/base/reloadOneTime&quot; type=&quot;com.sap.isa.core.action.ReloadOneTimeAction&quot;&gt;<br>
 * &lt;forward name=&quot;success&quot; path=&quot;showResult.jsp&quot;/&gt;<br>
 * &lt;forward name=&quot;reload&quot; path=&quot;/appl/startCalculateResult.do.do?fl=y&quot; redirect=&quot;true&quot;/&gt;<br>
 * &lt;/action&gt;<br>
 * </p>
 * </code>
 *
 * @author  d038380
 * @version 1.0
 */
public class ReloadOneTimeAction extends BaseAction {

	/**
	 * The name of the request parameter which shows if this action was executed already.
	 * (rf = Redirect Flush)
	 */
	public static final String REQ_REDIRECT_FLUSH = "rf";
	
    /**
     * Implements standard perform method which base on the <code>execute</code> method 
     * of the Struts framework. 
     * 
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return Action forward.
     * @throws IOException
     * @throws ServletException
     * 
     * @see com.sap.isa.core.BaseAction#doPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
	public ActionForward doPerform(ActionMapping mapping, 
			                       ActionForm form, 
								   HttpServletRequest request, 
								   HttpServletResponse response) 
	        throws IOException, ServletException {

		final String METHOD_NAME = "doPerform()";
		log.entering(METHOD_NAME);

	    String forward = "success";
		
	    String flushAfterRedirect = (String) request.getParameter(REQ_REDIRECT_FLUSH);
	    if (flushAfterRedirect == null) {
	    	flushAfterRedirect = (String) request.getAttribute(REQ_REDIRECT_FLUSH);
	    }    
	    if (flushAfterRedirect == null || !flushAfterRedirect.equalsIgnoreCase("y")) {
	    	forward = "reload";
	    }
		
		log.exiting();	        		        
        return mapping.findForward(forward);
    }
}