/*****************************************************************************
    Class:        SwitchAccessibilityAction
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      26.10.2004
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.core.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.BaseAction;
import com.sap.isa.core.Constants;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.MessageListDisplayer;
import com.sap.isa.core.util.StartupParameter;

/**
 * The action SwitchAccessibilityAction allows to switch the accessibility mode
 * dynamically. <br> 
 * Use the parameter {@link Constants#ACCESSIBILITY} to set the value. The value
 * must be <code>X</code> to activate the accessibilty mode and empty to deactivate
 * it. <br>
 * 
 * Per default the action provide the logical forward "success".<br>
 * The action provides a MessagesListDisplayer with a success message to the context. 
 * This could be used to display directly a message<br> 
 * 
 *
 * @author  SAP AG
 * @version 1.0
 */
public class SwitchAccessibilityAction extends BaseAction {

	/**
	 * Set the accessibility switch in the StartupParameter. <br> 
	 *  
	 * @param request http request 
	 * @param startupParameter startup parameter from the session context.
	 */
	static public void setAccessibilitySwitch(HttpServletRequest request,
		                                        StartupParameter startupParameter) {
		                                        	
		String accessibility = request.getParameter(Constants.ACCESSIBILITY);
        
		// Make uniform portal parameter values.
		if (accessibility != null){
			if (accessibility.toUpperCase().equals("X") ) {
				accessibility = "X";
			}
			else {
				accessibility = "";
			}
		}
		else {
			accessibility = "";
		}
        
		startupParameter.addParameter(Constants.ACCESSIBILITY, accessibility, true);
	}


	/**
	 * Get the value of the accessibility switch. <br> 
	 *  
	 * @param userSessionData user session data. 
	 */
	static public boolean getAccessibilitySwitch(UserSessionData userSessionData) {
		
		boolean isAccessible = false;
			
		if (userSessionData != null) {

			StartupParameter startupParameter = (StartupParameter)userSessionData.getAttribute(SessionConst.STARTUP_PARAMETER);

			if (startupParameter.getParameterValue(Constants.ACCESSIBILITY).equalsIgnoreCase("X")) {
				isAccessible = true;
			}
		}
		
		return isAccessible;
		
	}


    /**
     * Overwrites the method doPerform. <br>
     * 
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return
     * @throws IOException
     * @throws ServletException
     * 
     * 
     * @see com.sap.isa.core.BaseAction#doPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    public ActionForward doPerform(ActionMapping mapping,
                                    ActionForm form,
                                    HttpServletRequest request,
                                    HttpServletResponse response)
        	throws IOException, ServletException {
		final String METHOD_NAME = "doPerform()";
		log.entering(METHOD_NAME);
		UserSessionData userSessionData = UserSessionData.getUserSessionData(request.getSession());
	
		if (userSessionData != null) {
			StartupParameter startupParameter = (StartupParameter)userSessionData
					.getAttribute(SessionConst.STARTUP_PARAMETER);

            setAccessibilitySwitch(request, startupParameter);
						        		    
			String args[]= {"" + startupParameter.getParameterValue(Constants.ACCESSIBILITY).equals("X")};
			MessageListDisplayer messageDisplayer = new MessageListDisplayer();
			messageDisplayer.addToRequest(request);
			messageDisplayer.setOnlyDisplayMessages();
			messageDisplayer.addMessage(new Message(Message.INFO,"system.msg.accessibility",args,null));
		}

		log.exiting();	        		        
        return mapping.findForward("success");
        
    }
    
    

}
