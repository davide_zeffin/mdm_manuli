/*****************************************************************************
    Class:        StartSecureAction
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      28.09.2004
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.core.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.BaseAction;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.util.SecureHandler;
import com.sap.isa.core.util.WebUtil;

/**
 * The class StartSecureAction start a secure connection if necessary. <br>
 * The Action checks if already the secure switch is set. If not the action forces
 * a redirect with a secure connection. <br>
 * <strong>All request parameter passed to the action will be lost. Ensure that this 
 * action will call only without any context. </strong><br>
 * The global switch from XCM will be used. <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class StartSecureAction extends BaseAction {
	
    /**
     * Redirect path. Will be set in the request if html redirect is enabled.
     */
    public static final String REDIRECT_PATH = "redirect_path";
    /**
     * Flad to enable redirect with HTML.
     */
    public static final String HTML_REDIRECT = "htmlredirect";

    /**
     * Start a secure communication, if neccessary. <br>
     * 
     * @param mapping action mapping
     * @param form action foram
     * @param request http request 
     * @param response http response
     * @return
     * @throws IOException
     * @throws ServletException
     * 
     * 
     * @see com.sap.isa.core.BaseAction#doPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    public ActionForward doPerform(ActionMapping mapping,
        							ActionForm form,
        							HttpServletRequest request,
        							HttpServletResponse response)
        	throws IOException, ServletException {
		final String METHOD_NAME = "doPerform()";
		log.entering(METHOD_NAME);
        
        String htmlredirect = request.getParameter(HTML_REDIRECT);

		SecureHandler secureHandler = SecureHandler.getHandlerFromRequest(request);
		        
		boolean isEnabled = SecureHandler.isSSLEnabled(request);
       	
		if (isEnabled) {		
			
			// set secure mode for an already existing secure connection.
			if (secureHandler.isSecureConnection()) {
				SecureHandler.setSecureMode(request,SecureHandler.SECURE_ON);
			} 
        	
			// check if communication already secure 
			if (!SecureHandler.isSecureMode(request) || !secureHandler.isSecureConnection()) {

				SecureHandler.setSecureMode(request,SecureHandler.SECURE_ON);
				
				ActionForward retForward;
				String path = WebUtil.getAppsURL(getServlet().getServletConfig().getServletContext(),
				                                 request,
				                                 response,
				                                 new Boolean(true),
				                                 WebUtil.decodeContextValueFromURL(request.getServletPath(),request),
				                                 null,
				                                 null,
				                                 true);

				path = WebUtil.decodeContextValueFromURL(path,request);
								                                 
				if(htmlredirect != null && htmlredirect.equalsIgnoreCase("true")) {
                    
                    request.setAttribute(REDIRECT_PATH, path);
                    return mapping.findForward("htmlredirect");
				}
                else {
                    retForward = new ActionForward(path);
                    retForward.setRedirect(true);
                    retForward.setName("unSecure");
                    log.exiting();
                    return retForward;                          
                }
			}
		}	 
        	
        log.exiting();
        return mapping.findForward("success");
    }

}
