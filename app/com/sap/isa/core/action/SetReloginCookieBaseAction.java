/*****************************************************************************
    Class         SetReloginCookie
    Copyright (c) 2001, SAP AG, All rights reserved.
    Description:  Store the startup parameter in a cookie for an relogin
    Author:       SAP AG
    Created:      August 2003
    Version:      1.0

    $Revision: #25 $
    $Date: 2002/11/05 $
*****************************************************************************/
package com.sap.isa.core.action;

import java.io.IOException;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.BaseAction;
import com.sap.isa.core.ContextConst;
import com.sap.isa.core.FrameworkConfigManager;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.util.CookieUtil;
import com.sap.isa.core.util.StartupParameter;

/**
 * This action stores all relevant startup parameter for an relogin in a cookie. <br>
 * The parameters will be separated with the "|" sign. <br>
 * The functionality must be enabled in the <code>web.xml</code> 
 * (see {@link com.sap.isa.core.ContextConst#RELOGIN_COOKIE}).
 * 
 * <h4>Overview over request parameters and attributes</h4>
 * <table border="1" cellspacing="0" cellpadding="4">
 *   <tr>
 *     <th>name</th>
 *     <th>parameter</th>
 *     <th>attribute</th>
 *     <th>description</th>
 *   <tr>
 *   <tr>
 *   </tr>
 * </table>
 *
 * <h4>Overview over attributes which will be set in the request context</h4>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <th>attribute</th>
 *     <th>description</th>
 *   <tr>
 * <tr><td>none</td><td></td></tr>
 * </table>

 *   
 */
public class SetReloginCookieBaseAction extends BaseAction {


	/**
	 * The name of the cookie which stores the user's details
	 */
	public static final String COOKIE_NAME = "isa_relogin";
	
    /**
     * 
     */
    public SetReloginCookieBaseAction() {
        super();
    }

    /**
     * Set the Relogin Cookie.
     * @see com.sap.isa.core.BaseAction#doPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    public ActionForward doPerform(
        ActionMapping mapping,
        ActionForm form,
        HttpServletRequest request,
        HttpServletResponse response)
        	throws IOException, ServletException {
        		
		final String METHOD_NAME = "doPerform()";
		log.entering(METHOD_NAME);
		String flag= FrameworkConfigManager.Servlet.getInteractionConfigParameter(request.getSession(),"ui", ContextConst.RELOGIN_COOKIE, ContextConst.RELOGIN_COOKIE + ".isa.sap.com");
		boolean isEnabled = (flag != null && flag.equals("true"));

		UserSessionData userSessionData = UserSessionData.getUserSessionData(request.getSession());

		if (isEnabled && userSessionData != null) {
			StartupParameter startupParameter = (StartupParameter)userSessionData
					.getAttribute(SessionConst.STARTUP_PARAMETER);

			
			Iterator iter = startupParameter.reentryParameter();
			
			StringBuffer contentBuffer = new StringBuffer();
			boolean isFirst = true;
			
			while (iter.hasNext()) {
				if (isFirst) {
					isFirst = false;
				} else {
					contentBuffer.append('|');
				}
				StartupParameter.Parameter parameter = (StartupParameter.Parameter) iter.next();
				contentBuffer.append(parameter.getName()).append('=')
				             .append(parameter.getValue());
            }

			CookieUtil.createOrUpdateTempCookie(contentBuffer.toString(), COOKIE_NAME, request, response);       
		}
 
		log.exiting();
        return mapping.findForward("success");
    }
    
    

}
