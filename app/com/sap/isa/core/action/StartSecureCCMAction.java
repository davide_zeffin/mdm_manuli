package com.sap.isa.core.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.FrameworkConfigManager;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.init.InitializationHandler;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.core.xcm.config.ComponentConfig;

public class StartSecureCCMAction extends StartSecureAction {

	/**
	 * Start a secure communication, if neccessary. <br>
	 * 
	 * @param mapping action mapping
	 * @param form action foram
	 * @param request http request 
	 * @param response http response
	 * @return
	 * @throws IOException
	 * @throws ServletException
	 * 
	 * 
	 * @see com.sap.isa.core.BaseAction#doPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward doPerform(ActionMapping mapping,
									ActionForm form,
									HttpServletRequest request,
									HttpServletResponse response)
			throws IOException, ServletException {
		final String METHOD_NAME = "doPerform()";
		log.entering(METHOD_NAME);
        
        UserSessionData userData = UserSessionData.getUserSessionData(request.getSession());
        	
        boolean callCenterMode = false; 
        {
            String cm = FrameworkConfigManager.Servlet.getInteractionConfigParameter(
                request.getSession(), "user", "CallCenterMode", "");
                
            if (cm != null) { 
                callCenterMode = cm.equalsIgnoreCase("true");
            }
        }
        if (callCenterMode == true) {
            return CallCenterModeSSOHandler(mapping, request, userData);
        }
		log.exiting();
		return mapping.findForward("success");
	}

	private ActionForward CallCenterModeSSOHandler(ActionMapping mapping, HttpServletRequest request, UserSessionData userData) {
		/* copied from UserActions */
		String PN_USERID 	= "UserId";
		String PN_PASSWORD  = "nolog_password";
		/* copied from CcBpSearchAction */
		String PN_BUPAID 	= "bupaid";
		boolean noSSO = false;
		String strTmp = null;
		ActionForward retAction;
		
		if (userData.getAttribute(PN_PASSWORD) != null) { 
			request.setAttribute(PN_PASSWORD, userData.getAttribute(PN_PASSWORD).toString());
			userData.removeAttribute(PN_PASSWORD);
		}
		else
			noSSO = true;

		if (userData.getAttribute(PN_USERID) != null) {
			request.setAttribute(PN_USERID, userData.getAttribute(PN_USERID).toString());
			userData.removeAttribute(PN_USERID);
		}
		else
			noSSO = true;

		if (userData.getAttribute(PN_BUPAID) != null) {
			request.setAttribute(PN_BUPAID, userData.getAttribute(PN_BUPAID).toString());
			userData.removeAttribute(PN_BUPAID);
		}
		
		if (noSSO == true)
			retAction = mapping.findForward("success");
		else
			retAction = mapping.findForward("ssosuccess");
			
		return retAction;
	}
}
