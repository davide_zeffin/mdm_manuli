package com.sap.isa.core;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Enumeration;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.businessobject.event.BusinessEventHandlerBase;
import com.sap.isa.core.interaction.InteractionConfig;
import com.sap.isa.core.interaction.InteractionConfigContainer;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.system.RequestContextContainer;
import com.sap.isa.core.ui.context.ContextManager;
import com.sap.isa.core.ui.context.GlobalContextManager;
import com.sap.isa.core.ui.layout.AreaContext;
import com.sap.isa.core.util.HttpServletRequestWrapper;
import com.sap.isa.core.util.MessageListDisplayer;
import com.sap.isa.core.util.SecureHandler;
import com.sap.isa.core.util.VersionGet;
import com.sap.isa.core.xcm.ConfigContainer;
import com.sap.util.monitor.jarm.IMonitor;

/**
 *  The <code>BaseAction</code> is the Base for all ISA-Actions.
 *  All ISA-Actions have to specialize the <code>BaseAction</code> and
 *  implement the <code>doPerform</code> method.
 *  When the <code>ActionServlet</code> calls a specific ISA-Action, the
 *  <code>perform</code> method on the <code>BaseAction</code> is executed.
 *  It checks whether the current session is valid.
 *  If the session is not valid it forwards the request to <strong>session_not_valid</strong>.
 *  If the session is valid it calls the method <code>doPerform</code> on its
 *  specialization.
 *
 */
 
public abstract class BaseAction extends Action {

    /** 
     *  The IsaLocation of the current Action.
     *  This will be instantiated during perform and is always present.
     */
	protected IsaLocation log = null;


    /**
     *  The Name of the Class (String access is somehow faster than
     *  a dynamic access to the name of the class)
     */
    private String className = null;

    /**
     *  Description of the Field
     */
    protected static IsaLocation isaRuntimeTracing =
            IsaLocation.getInstance(Constants.TRACING_RUNTIME_CATEGORY);

	

    /**
     *  Constructor for the BaseAction object
     */
    public BaseAction() {
        super();
        initSupervision();
    }


    /**
     * This method checks whether the session is valid or not.
     * If it is not valid it forwards the control to <strong>session_not_valid</strong>
     * If it is valid it calls the <code>doPerform</code> on the specialized instance.
     * To prevent the overwritting of this method it is declared as <strong>final</strong>
     *
     * @param  mapping               The ActionMapping used to select this instance
     * @param  request               The HTTP request we are processing
     * @param  response              The HTTP response we are creating
     * @param  form                  Description of Parameter
     * @return                        <code> ActionForward </code>  describing where the control should be forwarded
     * @exception  IOException       if an input/output error occurs
     * @exception  ServletException  if a servlet exception occurs
     */
    public final ActionForward execute(ActionMapping mapping,
                                        ActionForm form,
                                        HttpServletRequest request,
                                        HttpServletResponse response)
             throws IOException, ServletException {
		long start_time = 0;
        long delta = 0;
        
        // To avoid null-pointer exceptions.
        // It is REQUIRED that log is available.
        if (log == null) {
			throw new ServletException("Could not retrieve IsaCategory.");
        }
		final String METHOD_NAME = "execute()";
		log.entering(METHOD_NAME);
        
        // Check session and add request to the business event handler
        // ActionForward forward = isSessionValid(mapping, request);
        // if (forward != null) {
        // get user session data object

        UserSessionData userData =
            UserSessionData.getUserSessionData(request.getSession());

		ActionForward forward = null;
		try {
	        // check session
	        if (userData == null) {
	            if (log.isDebugEnabled()) {
	                log.debug("BaseAction: " + Constants.SESSION_NOT_VALID);
	            }            
				forward = mapping.findForward(Constants.SESSION_NOT_VALID);
				return forward;
	
	        }
	        else {
	            // Retrieve the BusinessEventHandler
	            BusinessEventHandlerBase eventHandler = userData.getBusinessEventHandler();
	            if (eventHandler != null) {
	                eventHandler.setRequest(request);
	            }
	        }
	        
			//check if communication is stil secure 
	        checkSecureConnection(request, userData);
	        
	        // Hint: Always check if debug is enabled, to avoid noticeing it
	        //       deeper in the logging API.
	        if (log.isDebugEnabled()) {
	            log.debug("doPerform executing: path=" + mapping.getPath());
	        }
	        
	        /**
	         * Begin p13n modification
	         */
	        String newForwardStr = null;
	        ActionForward newForward = null;
	        /**
	         * end p13n modification
	         */
	        try {
	            
	            start_time = System.currentTimeMillis();
	            
	            if (isaRuntimeTracing.isDebugEnabled()) {
	                StringBuffer sb = new StringBuffer("[actionxecution]='begin' [actionclass]='");
	                sb.append(this.getClass().getName());
	                sb.append("' [path]='");
	                sb.append(mapping.getPath());
	                sb.append("'");
	                isaRuntimeTracing.debug(sb.toString());
	            }
	            
	            if (log.isDebugEnabled()) {
	                log.debug("** START BEFORE [" + this.getClass().getName() + "].isaPerform(...)");
	                log.debug("struts.form=\"" + (form == null ? "NULL" : form.toString()) + "\"");
	                log.debug("struts.mapping=\"" + (mapping == null ? "NULL" : mapping.toString()) + "\"");
	                trace(request);
	                log.debug("** END BEFORE [" + this.getClass().getName() + "].isaPerform(...)");
	            }
	            
	            /**
	             * Begine p13n modification: insert the hook to p13n before calling doPerform
	             */
	            ActionInterceptor interceptor = null;
	            try
	            {
	                if(isPersonalizationEnabled(request))
	                {
	                    if (log.isDebugEnabled())
	                        log.debug("Calling of the personalization interceptor");
	                    String interceptorClass = "com.sap.isa.pers.parse.PersConfigReader";
	                    interceptor = (ActionInterceptor)ActionInterceptorWrapper.instance(interceptorClass);
	                    if(interceptor != null)
	                        interceptor.interceptBefore(className,request.getSession());
	                }
	            }
	            catch(Exception e)
	            {//ignore
	                if (log.isDebugEnabled())
	                    log.debug(e);
	            }
	            /**
	             * End p13n modification
	             */
	            
	            String compName = null;
	            IMonitor monitor = (IMonitor)request.getAttribute(SharedConst.SAT_MONITOR);
	            if (monitor != null) { 
	                compName = VersionGet.getSATRequestPrefix() + this.getClass().getName();
	                monitor.startComponent(compName);
	            }
	            
	            if (ActionServlet.isSessionTrackingEnabled()) {
	                forward = doPerform(mapping, form, new HttpServletRequestWrapper(request, mapping.getPath()), response);
	            }
	            else {
	                forward = doPerform(mapping, form, request, response);
	            }
	            
	            
	            if (monitor != null)
	                monitor.endComponent(compName);
	            
	            
	            // Begin p13n modification: insert the hook to p13n after calling doPerform
	            try
	            {
	                if(isPersonalizationEnabled(request)) {
	                    if (log.isDebugEnabled())
	                        log.debug("Calling of the personalization interceptor");
	                    if(interceptor != null)
	                        newForwardStr = interceptor.interceptAfter(className,request.getSession());
	                }
	            }
	            catch(Exception e)
	            {//ignore
	                if (log.isDebugEnabled())
	                    log.debug(e);
	            }
	            // End p13n modification
	            
	            // After doPerform
	            delta = System.currentTimeMillis() - start_time;
	            
	            if (log.isDebugEnabled()) {
	                log.debug("** START AFTER [" + this.getClass().getName() + "].isaPerform(...)");
	                trace(request);
	                if (forward == null) {
	                    log.debug("action.[forward]=\"NULL\"");
	                }
	                else {
	                    log.debug("action.[forward]=\"" + forward.getPath() + "\"");
	                }
	                
	                if (log.isDebugEnabled()) {
	                    log.debug("doPerform finished (time: " + delta + "ms)");
	                }
	                
	                log.debug("** END AFTER [" + this.getClass().getName() + "].isaPerform(...)");
	            }
	            
	            if (isaRuntimeTracing.isDebugEnabled()) {
	                StringBuffer sb = new StringBuffer("[actionxecution]='end' [actionclass]='");
	                sb.append(this.getClass().getName());
	                sb.append("' [path]='");
	                sb.append(mapping.getPath());
	                sb.append("' [forward]='");
	                sb.append((forward == null) ? "NULL" : forward.getPath());
	                sb.append("' [exectime]='");
	                sb.append(delta);
	                sb.append("'");
	                isaRuntimeTracing.debug(sb.toString());
	            }
	            
	        }
	        catch (IOException e) {
	            
	            writeError(request, response, e);
	            log.error(LogUtil.APPS_USER_INTERFACE,"system.io", e);
				// rethrow exception
	            throw e;
	        }
	        catch (ServletException e) {
	            if( e.getRootCause() != null )
	                writeError(request, response, e.getRootCause());
	            else
	                writeError(request, response, e);
	            
	            log.error(LogUtil.APPS_USER_INTERFACE,"system.servletDifficulties", e);
				// rethrow exception
	            throw e;
	        }
	        catch (RuntimeException e) {
	            writeError(request, response, e);
	            log.error(LogUtil.APPS_USER_INTERFACE,"system.servletDifficulties", e);
	            // rethrow exception
				throw e;
	        }
	        finally {
	        }
	         // p13n code , if the new forward is returned by the p13nEngine use it overriding the
	         // one dtermined by the struts, else use the struts forward
	
	        //if(ActionServlet.isPersonalizationEnabled())
	        if(true)
	        {
	            if(newForwardStr != null)
	            {
	                log.debug("flow changed from "+ forward.getPath() + "  to "+ newForwardStr);
	                newForward = new ActionForward(newForwardStr);
	                newForward.setName(newForwardStr);
	                newForward.setPath(newForwardStr);
	            }
	            if (newForward != null) {
	                forward = newForward;
	            }    
	        }
        // End p13n modification
        } finally {
        
			log.exiting();
        }
            
        return forward;
    }


    /**
     *  Process the specified HTTP request, and create the corresponding HTTP
     *  response (or forward to another web component that will create it).
     *  Return an <code>ActionForward</code> instance describing where and how
     *  control should be forwarded, or <code>null</code> if the response has
     *  already been completed.
     *
     *@param  mapping               The ActionMapping used to select this instance
     *@param  request               The HTTP request we are processing
     *@param  response              The HTTP response we are creating
     *@param  form                  Description of Parameter
     *@return                       Description of the Returned Value
     *@exception  IOException       if an input/output error occurs
     *@exception  ServletException  if a servlet exception occurs
     */
    public abstract ActionForward doPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
             throws IOException, ServletException;


	/**
	 * Check if for a active secure switch an secure connection exist. <br>
	 * 
     * @param request current http request 
	 * @param userData the user session data object
	 */
	private void checkSecureConnection(HttpServletRequest request, UserSessionData userData) {

		SecureHandler secureHandler = SecureHandler.getHandlerFromRequest(request);
		secureHandler.checkSecureConnection(request,userData);
		 
	}


    /**
     *  If an instance of <code>UserSessionData</code> does not exist in the
     *  session scope, the session will be flagged as invalid. In this case
     *  this method forwards to <code>Constants.SESSION_NOT_VALID</code>
     *
     *@param  mapping  current  <code> ActionMapping </code>
     *@param  request  current  <code> HttpServletRequest </code>
     *@return          <code>null</code> if the session is valid and <code>ActionForward</code>
     *  to <code>Constants.SESSION_NOT_VALID</code> if not.
     */
    protected ActionForward isSessionValid(ActionMapping mapping,
            HttpServletRequest request) {

        HttpSession session = request.getSession();

        UserSessionData userData = UserSessionData.getUserSessionData(session);

        if (userData == null) {
            return mapping.findForward(Constants.SESSION_NOT_VALID);
        }

        return null;
    }


    /**
     *  This method overwrites the one from the <code>Action</code> class.
     *  The reason is, the request parametername in struts is
     *  <strong>org.apache.struts.taglib.html.TOKEN</strong> and way to long.
     *
     *@param  request  Description of Parameter
     *@return          The TokenValid value
     */
    protected boolean isTokenValid(HttpServletRequest request) {

        // Retrieve the saved transaction token from our session
        HttpSession session = request.getSession(false);
        if (session == null) {
            return (false);
        }

        // at this point we already know, the session is valid
        UserSessionData userData = UserSessionData.getUserSessionData(session);

        String saved = userData.getToken();
        if (saved == null) {
            return false;
        }

        String token = (String) request.getParameter(Constants.TOKEN);
        if (token == null) {
            return false;
        }

        // Do the values match?
        return saved.equals(token);
    }



    /**
     *  The values of the expected session attribute <strong>org.apache.struts.action.TOKEN</strong>
     *  and of the request attribute <strong>org.apache.struts.taglib.html.TOKEN</strong>
     *  will be compared and if they are equal this method returns <code>null</code>
     *  In any other case <code>Constants.STATE_NOT_VALID</code>
     *
     *@param  mapping  current  <code> ActionMapping </code>
     *@param  request  current  <code> HttpServletRequest </code>
     *@return          <code>null</code> if the session is valid and <code>ActionForward</code>
     *  to <code>Constants.SESSION_NOT_VALID</code> if not.
     */
    protected ActionForward isStateValid(ActionMapping mapping,
            HttpServletRequest request) {

        if (!isTokenValid(request)) {
            return mapping.findForward(Constants.STATE_NOT_VALID);
        }

        return null;
    }


    /* Trace the session and request data and write it to the logfile with
       tracelevel */
    /**
     *  Description of the Method
     *
     *@param  request  Description of Parameter
     */
    protected void trace(HttpServletRequest request) {

        HttpSession session = request.getSession();

        Enumeration paramNames = request.getParameterNames();
        String output;
        String local;

        local = request.getQueryString();
        log.debug("request.environment.[querystring]=\"" + (local == null ? "NULL" : local) + "\"");

        local = request.getRemoteAddr();
        log.debug("request.environment.[remoteaddr]=\"" + (local == null ? "NULL" : local) + "\"");

        /* disabled because of any problems in the InQMy-environment
        local = request.getRemoteHost();
        log.debug(id + "request.environment.[remotehost]=\"" + (local == null ? "NULL" : local) + "\"");
        */
        local = request.getRemoteUser();
        log.debug("request.environment.[remoteuser]=\"" + (local == null ? "NULL" : local) + "\"");

        local = request.getRequestURI();
        log.debug("request.environment.[requesturi]=\"" + (local == null ? "NULL" : local) + "\"");

        while (paramNames.hasMoreElements()) {
            String name = (String) paramNames.nextElement();

            output = "request.parameter.[" + name + "]=";

            if (name.toLowerCase().startsWith("nolog_") || (name.indexOf(Constants.HTMLB) != -1)) {
                output += "--hidden--";
            }
            else {
                String[] values = request.getParameterValues(name);

                if (values != null) {
                    if (values.length > 1) {
                        output += "{ ";
                    }

                    for (int i = 0; i < values.length; i++) {
                        output += "\"" + values[i] + "\" ";
                    }

                    if (values.length > 1) {
                        output += "} ";
                    }
                }
            }
            log.debug(output);
        }

        Enumeration attributeNames = request.getAttributeNames();

        while (attributeNames.hasMoreElements()) {
            String name = (String) attributeNames.nextElement();

            if (name.toLowerCase().startsWith("nolog_") || (name.indexOf(Constants.HTMLB) != -1)) {
                log.debug("--hidden--");
            }
            else {
                String value;

                if (request.getAttribute(name) == null) {
                    value = "NULL";
                }
                else {
                    value = request.getAttribute(name).toString();
                }

                if (value != null) {
                    StringTokenizer tokenizer = new StringTokenizer(value, "\n");

                    if (tokenizer.countTokens() > 1) {
                        int i = 0;
                        while (tokenizer.hasMoreElements()) {
                            output = "request.attribute.[" + name + "][" + i++ + "]=\"" + tokenizer.nextElement() + "\"";
                            log.debug(output);
                        }
                    }
                    else {
                        output = "request.attribute.[" + name + "]=\"" + value + "\"";
                        log.debug(output);
                    }
                }
            }
        }

        log.debug("session.environment.[creationtime]=\"" + session.getCreationTime() + "\"");
        local = session.getId();
        log.debug("session.environment.[id]=\"" + (local == null ? "NULL" : local) + "\"");
        log.debug("session.environment.[lastaccessedtime]=\"" + session.getLastAccessedTime() + "\"");
        log.debug("session.environment.[maxinactiveinterval]=\"" + session.getMaxInactiveInterval() + "\"");
        log.debug("session.environment.[isnew]=\"" + session.isNew() + "\"");

		synchronized (session) {
			attributeNames = session.getAttributeNames();
			while (attributeNames.hasMoreElements()) {
				String name = (String) attributeNames.nextElement();
				Object attribute = session.getAttribute(name);

				String value = (attribute == null) ? "NULL" : attribute.toString();

				output = "session.attribute.[" + name + "]=\"" + value + "\"";
				log.debug(output);
			
		}

        }
    }


    /**
     *  Initalize the supervision of the class, e.g. logging and
     *  monitoring. This method need to be called once.
     */
    private void initSupervision() {

        // set the classname
        className = this.getClass().getName();

        // Retrieve the actual IsaLocation
        log = IsaLocation.getInstance(className);
    }


    /**
     *  Writes an exception stacktrace into the response
     *
     *@param  response
     *@param  ex
     *@exception  IOException  Description of Exception
     */
    private void writeError(HttpServletRequest request,HttpServletResponse response, Throwable ex) throws IOException {
    	
    	RequestProcessor.writeException(request, response, ex);
    }
    
    /**
     * Returns configuration valid for the current session. This feature
     * is only available if Extended Configuration Management is turned on.
     * If no configuration data is available <code>null</code> is returned
     * @param request the HTTP request
     * @return configuration data
     */
    public InteractionConfigContainer getInteractionConfig(HttpServletRequest request) {
		UserSessionData userData =
                UserSessionData.getUserSessionData(request.getSession());
        if (userData == null)
        	return null;

		return (InteractionConfigContainer)(userData.getAttribute(SessionConst.INTERACTION_CONFIG));
    }
    
    /**
     * Returns a container containing all configuration files valid. This
     * feature is only available if Extended Configuration Management is 
     * turned on.
     * for this session. 
     * @param request the HTTP request
     * @return configuration container
     */
    public ConfigContainer getXCMConfigContainer(HttpServletRequest request) {
		UserSessionData userData =
                UserSessionData.getUserSessionData(request.getSession());
        if (userData == null)
        	return null;

		return (ConfigContainer)(userData.getAttribute(SessionConst.XCM_CONFIG));
    }
    
	/**
	 * Returns the request context. This context can be used to pass data between layers
	 * of the ISA framework.
	 * The context has the scope of an request (HTTP request).
	 * <p/><b>NOTE:</b><br/> 
	 * This context MUST NOT be used in standard development. It is a tool for quickly extending the 
	 * functionality of the standard in CUSTOMER projects. 
	 * <br/>
	 * @return the request context
	 */
	public RequestContext getRequestContext() {
		return RequestContextContainer.getInstance().getRequestContext(); 
	}

	protected boolean isPersonalizationEnabled(HttpServletRequest request)
	{
		boolean retval = false;
		try
		{
			InteractionConfigContainer icc = FrameworkConfigManager.Servlet.getInteractionConfig(request);
			InteractionConfig uiInteractionConfig = null;
		
			if( null != icc ) 
			   uiInteractionConfig=icc.getConfig("ui");
			String enablePers = null;
			if (uiInteractionConfig != null)
				enablePers = uiInteractionConfig.getValue("enable.pers");
			if (enablePers != null && enablePers.equalsIgnoreCase(Boolean.TRUE.toString()))
				retval = true;
		}
		catch(Throwable th)
		{
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw, true);
			th.printStackTrace(pw);
			log.error(LogUtil.APPS_USER_INTERFACE,sw.getBuffer().toString());
		}
		if (log.isDebugEnabled())
		{
			log.debug("Personalization, for this scenario request is: " + (retval ? Boolean.TRUE.toString() : Boolean.FALSE.toString()));
		}
		return retval;
	}
	
	/****** Browser Back Framework *****************/

	/**
	 * Store a value as an extended request attribute. <br>
	 * An extended request attribute will be available after an http redirect.
	 * 
	 * @param request current request
	 * @param name name of the attribute
	 * @param value value of the attribute
	 */
	protected void setExtendedRequestAttribute(HttpServletRequest request, String name, Object value) {
		
		RequestProcessor.setExtendedRequestAttribute(request,name, value);
	}

	/**
	 * <p> The method getMessageListDisplayer returns a message list displayer from the request. </p>
	 * <p> The method creates one if it's not available in the request.</p>
	 * <p> <strong>Note:</strong> The message list displayer will also be avaible after an redirect.</p>
	 * 
	 * @param request http request
	 * @return instance of {@link #MessageListDisplayer}
	 */
	protected MessageListDisplayer getMessageListDisplayer(HttpServletRequest request) {
	
		MessageListDisplayer messageListDisplayer = (MessageListDisplayer)request.getAttribute(MessageListDisplayer.CONTEXT_NAME);
		if (messageListDisplayer == null) {
			messageListDisplayer = new MessageListDisplayer(); 
			request.setAttribute(MessageListDisplayer.CONTEXT_NAME, messageListDisplayer);
			addExtendedRequestAttributeName(request, MessageListDisplayer.CONTEXT_NAME);
		}
		return messageListDisplayer;
	}
	

	/**
	 * Store the name of a extended request attribute. <br>
	 * An extended request attribute will be available after an http redirect.
	 * 
	 * @param request current request
	 * @param name name of the attribute
	 * @param value value of the attribute
	 */
	protected void addExtendedRequestAttributeName(HttpServletRequest request, String name) {
		
		RequestProcessor.addExtendedRequestAttributeName(request,name);	
	}	

	/**
	 * Change the context value in already existing context. <br>
	 * The value will not automatically transfer to the new context. 
	 * The method allows to change exiting values in one action (data processing)
	 * and add them to the context while the data displaying.
	 * <p>
	 * <strong>Example</strong>
	 * <br>
	 * Action 1: getting new product id to display:
	 * <pre>
	 * String productKey = (String)request.getParameter("PRODUCT_KEY");
     *	
	 * changeContextValue("CV_PRODUCT_KEY", productKey);
	 * </pre> 
	 * 
	 * Action 2: displaying the current product:
	 * <pre>
	 * String productKey = getContextValue ("CV_PRODUCT_KEY");
     *	
	 * setContextValue("CV_PRODUCT_KEY", productKey);
	 * </pre> 
	 * </p>
	 * <strong>Note</strong>: Do not use this method for multi values. 
	 * 
	 * @param request http request
	 * @param name name of the context value
	 * @param newValue new value for the context value
	 */
	protected void changeContextValue(HttpServletRequest request, String name, String newValue) {
		
		ContextManager contextManager =  ContextManager.getManagerFromRequest(request);
	
		contextManager.changeContextValue(name,newValue);	 
	}

	
	/**
	 * Add a value to the context values. <br>
	 * <strong>Note</strong>: Use this method only to add additional values to multi values. 
	 * 
	 * @param request http request
	 * @param name name of the context value
	 * @param newValue new value for the context value
	 */
	protected void addContextValue(HttpServletRequest request, String name, String newValue) {
		
		ContextManager contextManager =  ContextManager.getManagerFromRequest(request);
	
		contextManager.addContextValue(name,newValue);	 
	}

	/**
	 * Delete all existing multiple values. <br>
	 * <strong>Note</strong>: Use this method only for multi values. 
	 * 
	 * @param request http request
	 * @param name name of the context value
	 */
	protected void resetContextValue(HttpServletRequest request, String name) {
		
		ContextManager contextManager =  ContextManager.getManagerFromRequest(request);
	
		contextManager.resetContextValue(name);	 
	}

	
	/**
	 * Set the given value for the context value. <br>
	 * <strong>Note</strong>: For multi values this value replaces all existing values. 
	 * 
	 * @param request http request
	 * @param name name of the context value
	 * @param newValue new value for the context value
	 */
	protected void setContextValue(HttpServletRequest request, String name, String newValue) {
		
		ContextManager contextManager =  ContextManager.getManagerFromRequest(request);
	
		contextManager.setContextValue(name,newValue);	 
	}

	
	/**
	 * Return the context value from the already existing context. <br>
	 * <strong>Note</strong>: Do not use method this for multi values. 
	 * 
	 * @param request http request
	 * @param name name of the context value
	 * @param newValue new value for the context value
	 */
	protected String getContextValue(HttpServletRequest request, String name) {
		
		ContextManager contextManager =  ContextManager.getManagerFromRequest(request);
	
		return contextManager.getContextValue(name);	 
	}

	
	/**
	 * Return the list of context values from the already existing context. <br>
	 * <strong>Note</strong>: Do not use this method for single value. 
	 * 
	 * @param request http request
	 * @param name name of the context value
	 * @param newValue new value for the context value
	 */
	protected List getContextValues(HttpServletRequest request, String name) {
		
		ContextManager contextManager =  ContextManager.getManagerFromRequest(request);
	
		return contextManager.getContextValues(name);	 
	}


	/**
	 * Register the given context value. <br>
	 * The value will be created as obligatory and single value.
	 * 
	 * @param name name of the context value
	 */
	static protected void registerContextValue(String name) {
		
		GlobalContextManager.registerValue(name, true, false);	
	}


	/**
	 * Overwrite this static method to initialize your context values. <br>
	 * Add also an entry to the ContextInitandler. 
	 */
	static public void initContextValues() {
	}

	
	/**
	 * Register the given context value. <br>
	 * 
	 * @param name name of the context value
	 * @param isObligatory flag if value contains obligatory information. 
	 * @param isMultiValue flag if the value is a multi value. 
	 */
	protected void registerContextValue(String name, boolean isObligatory, boolean isMultiValue) {
		
		GlobalContextManager.registerValue(name, isObligatory, isMultiValue);
	}

	
	/**
	 * Return the cuurent area context. <br>
	 * <strong>Note:</strong> The function work also without using the layout framework. 
	 * 
	 * @param request current request
	 * @return current available area context  
	 */
	protected AreaContext getAreaContext(HttpServletRequest request) {
		
		return AreaContext.getContextFromRequest(request);
	}
}
