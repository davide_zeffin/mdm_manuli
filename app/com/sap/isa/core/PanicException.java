/*****************************************************************************
    Class:        PanicException
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Thomas Smits
    Created:      05.04.2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/06/26 $
*****************************************************************************/

package com.sap.isa.core;

/**
 * This exceptions represents an illegal system state caused by
 * misconfiguration. For example a query that definitely has to return at least
 * 1 value returns zero, but at all runs without any problems. This is
 * a case, where programm execution should be cancelled and a exception
 * of this type can be thrown.
 *
 * @author Thomas Smits
 * @version 1.0
 */
public class PanicException extends RuntimeException {

    /**
     * Constructs a <code>PanicException</code> with no detail  message.
     */
    public PanicException() {
        super();
    }

   /**
     * Constructs a <code>PanicException</code> with the specified
     * detail message.
     *
     * @param   s   the detail message.
     */
    public PanicException(String msg) {
        super(msg);
    }
}