package com.sap.isa.core;

/**
 * Provides Information about the currently used environment 
 */
public class EnvironmentManager {
	
	private static boolean mIsJ2EE620 = false;
	private static boolean mIsJ2EE630 = false;

	/**
	 * Returns <code>true</code> if a com.sap.jmx.monitoring.api.MBeanManager.MBeanManager is available
	 */	
	public static boolean isMBeanManager() {
		// check if class sqlj.runtime.ConnectionContext available
		ClassLoader cl = EnvironmentManager.class.getClassLoader();
		try {
			Class connectionContextClass = cl.loadClass("com.sap.jmx.monitoring.api.MBeanManager");
			return true;
		} catch (ClassNotFoundException cnfEx) {
			return false;
		}
	}


	
	/**
	 * Returns <code>true</code> if sqlj connection context is available
	 *
	 */	
	public static boolean isSQLJAvailable() {
		// check if class sqlj.runtime.ConnectionContext available
		ClassLoader cl = EnvironmentManager.class.getClassLoader();
		try {
			Class connectionContextClass = cl.loadClass("sqlj.runtime.ConnectionContext");
			return true;
		} catch (ClassNotFoundException cnfEx) {
			return false;
		}
	}
	
	/**
	 * Returns <code>true</code> if the application is running in SAP J2EE Engine 6.20 environment
	 */
	public static boolean isSAPJ2EE620() {
		return mIsJ2EE620;
	}

	/**
	 * Returns <code>true</code> if the application is running in SAP J2EE Engine 6.30 environment
	 */
	public static boolean isSAPJ2EE630() {
		return mIsJ2EE630;
	}

	
	static void setJ2EEServerInfo(String info) {
		if (info != null && info.indexOf("6.20") != -1  && info.indexOf("SAP") != -1) {
			mIsJ2EE620 = true;
			return;
		}

		if (info != null && info.indexOf("6.30") != -1  && info.indexOf("SAP") != -1) {
			mIsJ2EE630 = true;
			return;
		}
		if (info != null && info.indexOf("6.40") != -1  && info.indexOf("SAP") != -1) {
			mIsJ2EE630 = true;
			return;
		}
	}
}
