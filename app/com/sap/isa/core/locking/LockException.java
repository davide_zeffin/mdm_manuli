/*
 * Created on Sep 14, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.core.locking;

import com.sap.exception.BaseException;
import com.sap.isa.core.db.DALResourceBundle;
import com.sap.localization.LocalizableText;
import com.sap.tc.logging.Location;

/**
 * @author I802891
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class LockException extends BaseException {
	
	private static DALResourceBundle bundle = DALResourceBundle.getInstance();
	private static final String ERROR_PREFIX = LockException.class.getName() + '_';
	
	public static final String USER_ERROR = ERROR_PREFIX + 0;
	public static final String INTERNAL_ERROR = ERROR_PREFIX + 0;
	/**
	 * @param arg0
	 */
	public LockException(Location arg0) {
		super(arg0);
	}


	/**
	 * @param arg0
	 * @param arg1
	 */
	public LockException(Location arg0, Throwable arg1) {
		super(arg0, arg1);
	}


	/**
	 * @param arg0
	 * @param arg1
	 */
	public LockException(Location arg0, LocalizableText arg1) {
		super(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 */
	public LockException(
		Location arg0,
		LocalizableText arg1,
		Throwable arg2) {
		super(arg0, arg1, arg2);
	}



	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @param arg3
	 */
	public LockException(
		Location arg0,
		String arg2,
		Object[] arg3) {
		super(arg0, bundle, arg2, arg3);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @param arg3
	 * @param arg4
	 */
	public LockException(
		Location arg0,
		String arg2,
		Object[] arg3,
		Throwable arg4) {
		super(arg0, bundle, arg2, arg3, arg4);
	}
}
