/*
 * Created on Jul 23, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.core.locking;

/**
 * This Class can be xtended to provide additional information about the Lock to LockManager
 * @author I802891
 */
public class LockInfo {
	
	private byte lockType = LockManager.READ_LOCK;
	private byte lockMode = LockManager.MODE_ESCALATIVE;
	private Object resource = null;

	
	public byte getLockType() {
		return lockType;
	}
	
	public void setLockType(byte type) {
		this.lockType = type;
	}
	
	public byte getLockMode() {
		return lockMode;
	}
	
	public void setLockMode(byte mode) {
		this.lockMode = mode;
	}
	
	public Object getResource() {
		return this.resource;
	}
	
	public void setResource(Object resource) {
		this.resource = resource;
	}
}
