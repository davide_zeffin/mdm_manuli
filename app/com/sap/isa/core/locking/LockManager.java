/*
 * Created on Jul 23, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.core.locking;

import java.util.List;

/**
 * @author I802891
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public interface LockManager {

	public static final byte READ_LOCK = 1;
	public static final byte WRITE_LOCK = 2;
	
	/**
	 * Only One WRITE_LOCK can be obtained by Same Session
	 */
	public static final byte MODE_EXCLUSIVE = 0;
	/**
	 * Multiple WRITE_LOCK can be Obtained by Same Session
	 */
	public static final byte MODE_COMMULATIVE = 2;
	/**
	 * READ_LOCK can be escalated to WRITE_LOCK if held by the same session
	 */
	public static final byte MODE_ESCALATIVE = 4;
	
	/**
	 * 
	 * @param info
	 * @param timeout
	 * @throws LockException
	 */
	void lock(LockInfo info, int timeout) throws LockException;
	
	/**
	 * 
	 * @param info lock information
	 * @param async true => locks are released asychronously
	 * @throws LockException
	 */
	void unlock(LockInfo info, boolean async) throws LockException;
	
	/**
	 * For debugging purposes only
	 * @return LockInfo[] locks obtained by this session
	 */ 
	LockInfo[] getLocksObtained() throws LockException;
	
	/**
	 * 
	 * @param async true => locks are released asynchronously
	 * @throws LockException
	 */
	void unlockAll(boolean async) throws LockException;
	
	/**
	 * Release all user session level locks
	 * @return List list of errors
	 * @throws LockException
	 */
	List close();
	
	boolean isClosed();
}
