/*
 * Created on Sep 3, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.core.locking;

import com.sap.localization.LocalizableText;
import com.sap.tc.logging.Location;

/**
 * @author I802891
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class LockNotGrantedException extends LockException {
	
	private static final String ERROR_PREFIX = LockNotGrantedException.class.getName() + '_';
	
	public static final String ALREADY_LOCKED = ERROR_PREFIX + 0;
	

	/**
	 * @param arg0
	 */
	public LockNotGrantedException(Location arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public LockNotGrantedException(Location arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public LockNotGrantedException(Location arg0, LocalizableText arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 */
	public LockNotGrantedException(
		Location arg0,
		LocalizableText arg1,
		Throwable arg2) {
		super(arg0, arg1, arg2);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 * @param arg2
	 * @param arg3
	 */
	public LockNotGrantedException(Location arg0, String arg2, Object[] arg3) {
		super(arg0, arg2, arg3);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 * @param arg2
	 * @param arg3
	 * @param arg4
	 */
	public LockNotGrantedException(
		Location arg0,
		String arg2,
		Object[] arg3,
		Throwable arg4) {
		super(arg0, arg2, arg3, arg4);
		// TODO Auto-generated constructor stub
	}

}
