/*
 * Created on Aug 6, 2004
 *
 */
package com.sap.isa.core.locking.enqueue;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.naming.InitialContext;

import com.sap.engine.frame.core.locking.TechnicalLockException;
import com.sap.engine.services.applocking.TableLocking;
import com.sap.isa.core.locking.LockException;
import com.sap.isa.core.locking.LockInfo;
import com.sap.isa.core.locking.LockManager;
import com.sap.isa.core.locking.LockNotGrantedException;
import com.sap.isa.core.locking.NotLockedException;
import com.sap.tc.logging.Location;

/**
 * @author I802891
 *
 */
public class EnqueueLockManager implements LockManager {
	private static final Location tracer = 
		Location.getLocation(EnqueueLockManager.class.getPackage().getName());
	
	public static final byte LIFETIME_SESSION = TableLocking.LIFETIME_USERSESSION;
	public static final byte LIFETIME_TRANSACTION = TableLocking.LIFETIME_TRANSACTION;

	/**
	 * Locks obtained
	 */
	private List mLocks = null;	
	
	/**
	 * Distributed Enqueue Table Locking service
	 */
	private TableLocking mLockService = null;
	
	/**
	 * Database connection
	 */
	private Connection mConn;
	
	private boolean mInitialized = false;
	
	
	/**
	 * Life cycle is tied to database Connection
	 * @param conn
	 */
	public EnqueueLockManager() {
		mLocks = new LinkedList();

	}
	
	private void checkInitialized() throws LockException {
		if(!mInitialized)
			throw new LockException(tracer,LockException.USER_ERROR, new String[]{"EnqueueLockManager is not initialized"});
			
		if(mInitialized && this.mConn == null)
			throw new LockException(tracer,LockException.USER_ERROR, new String[]{"EnqueueLockManager is closed"});			
	}
	
	/**
	 * <p>
	 * Enqueue Lock Managet must be attached to a Connection before making any calls
	 * to lock/unlock. Connection object must be set exactly once.
	 * </p>
	 * @param conn
	 */
	public synchronized void setConnection(Connection conn) throws LockException {
		// session already initialized
		if(mInitialized && this.mConn != null) {
			throw new LockException(tracer,LockException.USER_ERROR, new String[]{"EnqueueLockManager is already initialized"});
		}
		
		// session closed
		if(mInitialized && this.mConn == null) {
			throw new LockException(tracer,LockException.USER_ERROR, new String[]{"EnqueueLockManager is closed"});
		}
		
		// connection is already closed
		try {
			if(conn.isClosed()) {
				throw new LockException(tracer,LockException.USER_ERROR, new String[]{"connection is closed"});
			}
		}
		catch(SQLException e) {
			throw new LockException(tracer,LockException.USER_ERROR, new String[]{"Connection is not usable"},e);
		}
		
		
		try {
			InitialContext ctxt = new InitialContext();
			mLockService = (TableLocking)ctxt.lookup(TableLocking.JNDI_NAME);
		}
		catch (Exception e) {
			throw new LockException(tracer,LockException.INTERNAL_ERROR,new String[]{TableLocking.JNDI_NAME + "lookup failed"},e);
		}
				
		this.mConn = conn;
		
		mInitialized = true;
	}
	
	public Connection getConnection() {
		return mConn;
	}
	
	/* 
	 * <p> All session level locks are release if the Connection is still not closed </p>
	 * @see com.sap.isa.core.locking.LockManager#close()
	 */
	public List close() {
		if(!mInitialized) return null;
		
		// it is assumed that Transaction specific locks have been unlocked
		// explicitly or implicitly with committ/rollback of tx
		// release all the Session level locks held by lock manager
		// if the connection is not closed
		ArrayList errors = null;
		try {
			if(!mConn.isClosed()) {
				LockInfo[] locks = null;
				try {
					locks = getLocksObtained();
				}
				catch(LockException e) {
					LockException lex = new LockException(tracer,LockException.INTERNAL_ERROR,new String[]{"close()"},e);
					// will not occur
				}
				for(int i = 0; i < locks.length; i++) {
					EnqueueLockInfo enLock = (EnqueueLockInfo)locks[i];
					if(enLock.getLifetime() == LIFETIME_SESSION) {
						// try to release as many locks as possible
						try {
							unlock(locks[i],true);
						}
						catch(LockException e) {
							if(errors == null) errors = new ArrayList();
							errors.add(e);
						}
					}
				}
			}

		} 
		catch(SQLException e) {
			LockException lex = new LockException(tracer,LockException.INTERNAL_ERROR,new String[]{"connection is not usable"},e);
			if(errors != null && errors.size() > 0) {
				errors.add(lex);
			}			
		}
		finally {
			this.mConn = null;
			mLocks.clear(); 
		}
		
		return errors;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.locking.LockManager#lock(java.lang.Object, com.sap.isa.core.locking.LockInfo)
	 */
	public void lock(LockInfo info, int timeout) throws LockException {
		checkInitialized();
		
		EnqueueLockInfo eqLockInfo = (EnqueueLockInfo)info;
		char mode = TableLocking.MODE_EXCLUSIVE_CUMULATIVE;
				
		switch(info.getLockType()) {
			case LockManager.READ_LOCK : mode = TableLocking.MODE_SHARED ;break;
			case LockManager.WRITE_LOCK:
				switch(info.getLockMode()) {
					 case LockManager.MODE_EXCLUSIVE :  mode = TableLocking.MODE_EXCLUSIVE_NONCUMULATIVE; break;
					 case LockManager.MODE_COMMULATIVE :  mode = TableLocking.MODE_EXCLUSIVE_CUMULATIVE; break;
					 case LockManager.MODE_ESCALATIVE : mode = TableLocking.MODE_EXCLUSIVE_CUMULATIVE; break;						 
				} 
				break;
		}
						
		try {
			if(timeout > 0) {
				mLockService.lock(eqLockInfo.getLifetime(),mConn,eqLockInfo.getTableName(),eqLockInfo.getKeys(),mode,timeout);
			}
			else {
				mLockService.lock(eqLockInfo.getLifetime(),mConn,eqLockInfo.getTableName(),eqLockInfo.getKeys(),mode);				
			}
			
		}
		catch(com.sap.engine.frame.core.locking.LockException e) {
			LockNotGrantedException ex = new LockNotGrantedException(tracer,LockNotGrantedException.ALREADY_LOCKED,new Object[]{info},e);
			throw ex;
		}
		catch (TechnicalLockException e) {
			LockException ex = new LockException(tracer,LockNotGrantedException.INTERNAL_ERROR,new Object[]{info},e);
			throw ex;
		}
		catch(IllegalArgumentException e) {
			LockException ex = new LockException(tracer,LockNotGrantedException.INTERNAL_ERROR,new Object[]{info},e);
			throw ex;
		}
		
		mLocks.add(info);
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.locking.LockManager#unlock(java.lang.Object, com.sap.isa.core.locking.LockInfo)
	 */
	public void unlock(LockInfo info, boolean async) throws LockException {
		checkInitialized();
		
		if(!mLocks.contains(info)) {
			NotLockedException e = new NotLockedException(tracer,NotLockedException.USER_ERROR,new Object[]{info});
		}
		
		EnqueueLockInfo eqLockInfo = (EnqueueLockInfo)info;
		char mode = TableLocking.MODE_EXCLUSIVE_CUMULATIVE;
				
		switch(info.getLockType()) {
			case LockManager.READ_LOCK : mode = TableLocking.MODE_SHARED ;break;
			case LockManager.WRITE_LOCK:
				switch(info.getLockMode()) {
					 case LockManager.MODE_EXCLUSIVE :  mode = TableLocking.MODE_EXCLUSIVE_NONCUMULATIVE; break;
					 case LockManager.MODE_COMMULATIVE :  mode = TableLocking.MODE_EXCLUSIVE_CUMULATIVE; break;
					 case LockManager.MODE_ESCALATIVE : mode = TableLocking.MODE_EXCLUSIVE_CUMULATIVE; break;						 
				} 
				break;
		}
						
		try {
			mLockService.unlock(eqLockInfo.getLifetime(),mConn,eqLockInfo.getTableName(),eqLockInfo.getKeys(),mode,async);
		}
		catch (TechnicalLockException e) {
			LockException ex = new LockException(tracer,LockNotGrantedException.USER_ERROR,new Object[]{"No such lock exists",info},e);
			throw ex;
		}
		catch(IllegalArgumentException e) {
			LockException ex = new LockException(tracer,LockException.INTERNAL_ERROR,new Object[]{info},e);
			throw ex;
		}		
		
		mLocks.remove(info);		
	}
	
	/* (non-Javadoc)
	 * @see com.sap.isa.core.locking.LockManager#getLocksObtained()
	 */
	public LockInfo[] getLocksObtained() throws LockException {
		checkInitialized();
		LockInfo[] arr = new LockInfo[mLocks.size()];
		mLocks.toArray(arr);
		return arr;
	}


	/* (non-Javadoc)
	 * @see com.sap.isa.core.locking.LockManager#unlockAll()
	 */
	public void unlockAll(boolean async) throws LockException {
		LockInfo[] lockInfos = getLocksObtained();
		for(int i = 0; i < lockInfos.length; i++) {
			unlock(lockInfos[i],async);
		}
	}
	/* (non-Javadoc)
	 * @see com.sap.isa.core.locking.LockManager#isClosed()
	 */
	public boolean isClosed() {
		if(!mInitialized) return true;
		return mConn == null && mInitialized == true;
	}

}
