/*
 * Created on Aug 10, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.core.locking.enqueue;

import java.util.Map;

import com.sap.isa.core.locking.LockInfo;

/**
 * @author I802891
 *
 */
public final class EnqueueLockInfo extends LockInfo {
	private String tableName;
	private Map keys;
	private byte lifetime = EnqueueLockManager.LIFETIME_SESSION;
	
	public byte getLifetime() {
		return lifetime;
	}
	
	public void setLifetime(byte val){
		this.lifetime = val;
	}	
	
	public void setTableName(String name) {
		this.tableName = name;
	}
	
	public String getTableName() {
		return tableName;
	}
	
	public void setKeys(Map keys) {
		this.keys = keys;
	}
	
	public Map getKeys() {
		return keys;
	}
}
