/*
 * Created on Sep 3, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.core.locking.enqueue;

import com.sap.isa.core.locking.LockManager;
import com.sap.isa.core.locking.LockManagerFactory;

/**
 * @author I802891
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class EnqueueLockManagerFactory implements LockManagerFactory {

	/* (non-Javadoc)
	 * @see com.sap.isa.core.locking.LockManagerFactory#getLockManager()
	 */
	public LockManager getLockManager() {
		return new EnqueueLockManager();
	}
}
