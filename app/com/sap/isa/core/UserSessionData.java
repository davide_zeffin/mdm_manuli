/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Franz Mueller
  Created:      01 March 2001


  $Revision: #3 $
  $Date: 2001/07/23 $
*****************************************************************************/

package com.sap.isa.core;


import java.util.Enumeration;
import java.util.Locale;
import java.util.Vector;

import javax.servlet.http.HttpSession;

import org.apache.struts.Globals;
import org.apache.struts.action.Action;

import com.sap.isa.core.businessobject.event.BusinessEventHandlerBase;
import com.sap.isa.core.businessobject.management.BOManager;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.CodePageUtils;
import com.sap.isa.core.util.EditObjectHandler;
import com.sap.isa.core.xcm.ConfigContainer;

/**
 * This class represents a container of all session relevant
 * informations. The main purpose of it is to collect and store the
 * session attributes which are introduced through our application.
*/
public class UserSessionData {

    protected static IsaLocation log = IsaLocation.
          getInstance(UserSessionData.class.getName());

   private HttpSession session;
   private String sessionId;
   private static boolean mSessionTracking;
   
   private EditObjectHandler editObjectHandler = null; 
   
   // application is intialized completely
   // will be used from Reinvoke mechanism. See also Reinvoke Container and SessionConst.REINVOKE_CONTAINER
   private boolean isInitialized = false;
   
   private UserSessionData(HttpSession session) {
   	this.session   = session;
 
   	this.sessionId = "." + session.getId();
   }

   protected String getSessionId() {
        return sessionId;
   }


   /**
     * Creates an instance of <code>UserSessionData</code> and
     * stores it in the session context under the key <strong>SessionConst.USER_DATA</strong>
     * All under the 'old' key stored attributes will be removed.
     * If there is a previously stored instance of this type it will
     * be removed befor a new one is stored.
     * If you use this method then session tracking is turned off.
     *
     * @param     session   current <code>HttpSession</code>
     * @return    the created <code>UserSessionData</code> instance
    */

   public static UserSessionData createUserSessionData(HttpSession session) {
      return createUserSessionData(session, false);
   }


   /**
     * Creates an instance of <code>UserSessionData</code> and
     * stores it in the session context under the key <strong>SessionConst.USER_DATA</strong>
     * All under the 'old' key stored attributes will be removed.
     * If there is a previously stored instance of this type it will
     * be removed befor a new one is stored.
     *
     * @param     session   current <code>HttpSession</code>
     * @param     aSessionTracking if the value is <code>true</code> then session tracking is turned on
     *                             That means that the whole communication with the <code>UserSessionData</code> is logged.
     * @return    the created <code>UserSessionData</code> instance
    */

   public static UserSessionData createUserSessionData(HttpSession session, boolean aSessionTracking) {

        UserSessionData userData = getUserSessionData(session);
        mSessionTracking = aSessionTracking;

   	if (userData != null) {
           for (Enumeration enum=userData.getAttributeNames(); enum.hasMoreElements();) {
                String attrName = (String)enum.nextElement();
                userData.removeAttribute(attrName);
           }

   	   session.removeAttribute(SessionConst.USER_DATA);
   	}

   	userData = new UserSessionData(session);
   	session.setAttribute(SessionConst.USER_DATA, userData);

   	return userData;
   }


   /**
     * Get the <code>UserSessionData</code> from the session
     *
     * @param     session   current <code>HttpSession</code>
     * @return    the previously stored <code>UserSessionData</code>
    */
   public static UserSessionData
                      getUserSessionData(HttpSession session) {
        if (session != null) {              	
			return (UserSessionData)
				   session.getAttribute(SessionConst.USER_DATA);
        }
		return null;
   }


   private String getKey(String name) {
   	return name + sessionId;
   }


   public void setAttribute(String name, Object object) {
        if (mSessionTracking) {
          if (log.isDebugEnabled()) {
            StringBuffer sb =
              new StringBuffer("SessionTracking: session.setAttribute: [name]=\"" + name + "\" [value]=\"");
            if (object == null)
              sb.append("NULL\"");
            else
              sb.append(object.toString() + "\" [type]=\"" + object.getClass().toString() + "\"");
            log.debug(sb.toString());
          }
        }
   	session.setAttribute(getKey(name), object);
   }

   public void removeAttribute(String name) {
        if (mSessionTracking) {
          if (log.isDebugEnabled())
            log.debug("SessionTracking: session.removeAttribute: [name]=\"" + name + "\"");
        }
	session.removeAttribute(getKey(name));
   }

   public Object getAttribute(String name) {
      Object attr = session.getAttribute(getKey(name));
        if (mSessionTracking) {
          if (log.isDebugEnabled()) {
            if (attr != null)
              log.debug("SessionTracking: session.readAttribute: [name]=\"" + name
              + "\" [value]=\"" + attr.toString() + "\"");
            else
              log.debug("SessionTracking: session.readAttribute: [name]=\"" + name + "\" [value]=\"NULL\"");
          }
        }
      return attr;
   }

   public Enumeration getAttributeNames() {
	Vector v = new Vector();

	for (Enumeration enum=session.getAttributeNames();
	enum.hasMoreElements();) {
	     String name = (String)enum.nextElement();
	     if (name.endsWith(sessionId))
	        v.addElement(name.substring(0,
	                         (name.length()-sessionId.length())));
	}

	return v.elements();
   }

   // convenient methods

   /**
    *  Obtain the <strong>MetaBusinessObjectManager</strong> from the
    *  session context
    *
    * @return MetaBusinessObjectManager used in this session
    */
    public BOManager getBOM(String name) {
        MetaBusinessObjectManager metaBOM = getMBOM();
        if (metaBOM == null)
           throw new PanicException("MetaBOM is null");

        return metaBOM.getBOMbyName(name);
    }


   /**
    *  Obtain the <strong>MetaBusinessObjectManager</strong> from the
    *  session context
    *
    * @return MetaBusinessObjectManager used in this session
    */
    public MetaBusinessObjectManager getMBOM() {
        return (MetaBusinessObjectManager)getAttribute(SessionConst.MBOM_MANAGER);
    }

   /**
    *  Set the <strong>MetaBusinessObjectManager</strong> in the
    *  session context
    *
    *  @param metaBOM the MetaBusinessObjectManager
    */
    public void setMBOM(MetaBusinessObjectManager metaBOM) {
          setAttribute(SessionConst.MBOM_MANAGER, metaBOM);
    }


   /**
    *  Obtain the <strong>BusinessEventHandlerBase</strong> from the
    *  session context
    *
    * @return BusinessEventHandlerBase used in this session
    */
    public BusinessEventHandlerBase getBusinessEventHandler() {
        return (BusinessEventHandlerBase)getAttribute(SessionConst.BUSINESS_EVENT_HANDLER);
    }


   /**
    *  Set the <strong>BusinessEventHandlerBase</strong> in the
    *  session context
    *
    *  @param beh the BusinessEventHandler
    */
    public void setBusinessEventHandler(BusinessEventHandlerBase beh) {
          setAttribute(SessionConst.BUSINESS_EVENT_HANDLER, beh);
    }


   /**
    * Convenient methods to obtain the SAP Language
    *
    * @return SAP Language for this session
    */
   public String getSAPLanguage() {
   	Locale locale = getLocale();
	   return (locale == null) ? 
		   null : CodePageUtils.getSapLangForJavaLanguageAndCountry(locale.getLanguage(), locale.getCountry());

   }


   /**
    * Convenient methods to access the Locale Object
    *
    * @return Locale used in this session
    */
   public Locale getLocale() {
   	return (Locale)session.getAttribute(Globals.LOCALE_KEY);
   }

   /**
    * Convenient methods to set the Locale for the current session
    *
    * @param Locale to be used in this session
    */

   public void setLocale(Locale locale) {
   	session.setAttribute(Globals.LOCALE_KEY, locale);
   	setStyleDirection();
   }


   /**
    *  Since the <strong>Token</strong> is initially set through the
    *  <strong>Struts</strong> and not through an instance of
    *  this type we offer some convenient methods to access this
    *  attributes
    *
    * @return the token placed in the session or <code>null</code>
    *         in any
    */
   public String getToken() {
   	return (String)
   	          session.getAttribute(Globals.TRANSACTION_TOKEN_KEY);
   }
   
   /**
    * Returns a XCM configuration container for the currently active scenario or
    * <code>null</code> if the application is not using XCM
    * @return XCM configuration container for the currently active scenario or
    * <code>null</code> if the application is not using XCM
 	*	
    */
   public ConfigContainer getCurrentConfigContainer() {
   		return (ConfigContainer)getAttribute(SessionConst.XCM_CONFIG);
   }

	/**
	 * Return if the appplication is intialized completely. <br>
	 *     
     * This will be used from Reinvoke mechanism. 
     * See also Reinvoke Container and SessionConst.REINVOKE_CONTAINER
	 *
	 * @return Returns if the application is initialized completely.
	 */
	public boolean isInitialized() {
		return isInitialized;
	}

	/**
	 * Set the flag if the appplication is intialized completely. <br>
	 *     
     * This will be used from Reinvoke mechanism. 
     * See also Reinvoke Container and SessionConst.REINVOKE_CONTAINER
	 * 
	 * @param isInitialized flag if the application is intialized completely
	 */
	public void setInitialized(boolean isInitialized) {
		this.isInitialized = isInitialized;
	}
	
	/**
	 * Set the style direction flag. <br>
	 *     
	 * This will be used to generate an additional stylesheet link in the JSP 
	 * In case of hebrew or arabic the style diretion is RTL
	 * 
	 */	
	protected void setStyleDirection() {
		String styleDirectionXCM = 
					  FrameworkConfigManager.Servlet.getInteractionConfigParameter(session, "ui",
																				   SharedConst.STYLE_DIRECTION_IC,
																				   SharedConst.STYLE_DIRECTION);
		String styleDirection = "false";
		if (styleDirectionXCM != null && 
			  styleDirectionXCM.toUpperCase().indexOf(getSAPLanguage().toUpperCase()) >= 0) {
				  styleDirection = "true";
		}
		setAttribute(SharedConst.STYLE_DIRECTION, styleDirection);		
	}

	/**
	 * <p>Return the property {@link #editObjectHandler}. </p> 
	 *
	 * @return Returns the {@link #editObjectHandler}.
	 */
	public EditObjectHandler getEditObjectHandler() {
		return editObjectHandler;
	}

	/**
	 * <p>Set the property {@link #editObjectHandler}. </p>
	 * 
	 * @param editObjectHandler The {@link #editObjectHandler} to set.
	 */
	public void setEditObjectHandler(EditObjectHandler editObjectHandler) {
		this.editObjectHandler = editObjectHandler;
	}
	
	
	
	
}
