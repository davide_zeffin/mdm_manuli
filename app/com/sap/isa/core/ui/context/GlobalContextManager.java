/*****************************************************************************
    Class:        GlobalContextManager
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      23.07.2004
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.core.ui.context;

import java.util.HashMap;
import java.util.Map;

import com.sap.isa.core.PanicException;
import com.sap.isa.core.logging.IsaLocation;

//TODO docu
/**
 * The class GlobalContextManager manages all available context values. <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class GlobalContextManager {
	
	/**
	 * Reference to the IsaLocation. <br>
	 *
	 * @see com.sap.isa.core.logging.IsaLocation
	 */
	protected static IsaLocation log = IsaLocation.
		  getInstance(GlobalContextManager.class.getName());
	
	
	/**
	 * Map with all available context values
	 */
	protected static Map contextValues =  new HashMap();
	
	
	/**
	 * Register the given context value. <br>
	 * <strong>Note:</strong> <code>isObligatory</code> must be set to <code>true</code>
	 * in this implementation!
	 * 
	 * @param name name of the context value
	 * @param isObligatory flag if value contains obligatory information.
	 * @param isMultiValue flag if the value is a multi value. 
	 */
	public static void registerValue (String name,
										boolean isObligatory,
										boolean isMultiValue) {
		
		ContextValue value = new ContextValue(name, isObligatory, isMultiValue);
		
		if (contextValues.get(value.getName()) == null) {
			contextValues.put(value.getName(), value);
		}
		else {
			log.debug("context value: "+ value.getName() + " is already registered");
		}
		 
	}
	

	/**
	 * Return a copy for the context value with the given name. <br>
	 * 
	 * @param name name of the context value.
	 * 
	 * @return copy of the context value or <code>null</code> if the value 
	 *          can't be found.
	 */
	public static ContextValue getValue(String name) {
		
		ContextValue contextValue = getOriginalValue(name);
		if (contextValue != null) {
			return (ContextValue)contextValue.clone();
		}
					
		return null;					
	}
	

	/**
	 * Return the context value with the given name. <br>
	 * 
	 * @param name name of the context value.
	 * 
	 * @return copy of the context value or <code>null</code> if the value 
	 *          can't be found.
	 */
	public static ContextValue getOriginalValue(String name) {
		
		ContextValue contextValue = (ContextValue)contextValues.get(name);
		
		if (contextValue == null) {
			log.debug("context value: "+ name + " can't be found");
			return null;
		}

		return (ContextValue)contextValue;			
	}
	
	/**
	 * Clear all values . <br>
	 */
	public static void clear() {
		contextValues.clear();
	}

}
