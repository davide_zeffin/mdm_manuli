/*****************************************************************************
        Class:        ContextInitHandler
        Copyright (c) 2004, SAPMarkets Europe GmbH, Germany, All rights reserved.
        Author:       SAP
        Created:      March 2004
        Version:      1.0

        $Revision: #1 $
        $Date: 2001/08/01 $
*****************************************************************************/
package com.sap.isa.core.ui.context;

import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.Properties;

import com.sap.isa.core.init.Initializable;
import com.sap.isa.core.init.InitializationEnvironment;
import com.sap.isa.core.init.InitializeException;


/**
 * The class allows to register all needed context values for a application.
 *
 * @author  SAP AG
 * @version 1.0
 *
 */
public class ContextInitHandler implements Initializable {


    public ContextInitHandler() {
    }

    /**
     * This method is called from the InitializationHandler to intialize the
     * context values for an application. <br>
     *
     * @param env
     * @param props
     * @throws InitializeException
     *
     */
    public void initialize(InitializationEnvironment env, Properties props)
                throws InitializeException {

        try {

            Iterator iter = props.values().iterator();


            while (iter.hasNext()) {
                String className = (String) iter.next();

                // take the handler class from init file
                Class handlerClass = Class.forName(className);

                // get the init method from the class
                Method method = handlerClass.getMethod("initContextValues", null);

                // invoke the method to initialize the handler
                method.invoke(null, null);
            }

        }
        catch (Exception exc) {
            throw new InitializeException("Unable to initialize the style file list");
        }
    }


        /**
         * Implements the method terminate. <br>
         *
         * @see com.sap.isa.core.init.Initializable#terminate()
         */
        public void terminate() {
			GlobalContextManager.clear();
        }

}
