/*****************************************************************************
    Class:        ContextValue
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      23.07.2004
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.core.ui.context;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.sap.isa.core.PanicException;

/**
 * The class ContextValue defines a value, which decribes the context . <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class ContextValue implements Cloneable {

	/**
	 * Name of the context value. <br>
	 */
	protected String name;
	
	/**
	 * A context value could consist of more than one value. <br>
	 * Then the flag  must be set to <code>true</code>. In this case the method 
	 * {@link #addValue} must be used to add values. <br>   
	 */
	protected boolean multiValue = false;

	
	/**
	 * Flag to store a context value permanent for the whole applictaion. <br>
	 */
	protected boolean permanent = false;


	/**
	 * Flag to store a context value permanent for the whole applictaion. <br>
	 */
	protected boolean layoutRelated = false;
	
	/**
	 * Value as string. <br>
	 */
	protected String value = ""; 

	
	/**
	 * Values as a list. <br>
	 */
	protected List values; 

	
	/**
	 * Flag is the value is obligatory. <br> 
	 */
	boolean obligatory = false;
	
	
	/**
	 * Standard Constructor for the Value. <br>
	 * 
	 * @param name name of the context value
	 * @param isObligatory flag if value contains obligatory information. 
	 * @param isMultiValue flag if the value is a multi value. 
	 */
	public ContextValue(String name,
						 boolean isObligatory, 
						 boolean isMultiValue) {
		
		this.name       = name;
		this.multiValue = isMultiValue;
		this.obligatory = isObligatory;
		if (multiValue) {
		    values = new ArrayList();
		}
		 
	}


	/**
	 * Returns the property {@link #multiValue}. <br>
	 * 
	 * @return  {@link #multiValue}
	 */
	public boolean isMultiValue() {
		return multiValue;
	}

	/**
	 * Return Set the property {@link #layoutRelated}. <br>
	 * 
	 * @return
	 */
	public boolean isLayoutRelated() {
		return layoutRelated;
	}

	/**
	 * Return Set the property {@link #layoutRelated}. <br>
	 * 
	 * @param b
	 */
	public void setLayoutRelated(boolean b) {
		layoutRelated = b;
	}


    /**
     * Return the property {@link #permanent}. <br> 
     *
     * @return Returns the {@link #permanent}.
     */
    public boolean isPermanent() {
        return permanent;
    }
    
    
    /**
     * Set the property {@link #permanent}. <br>
     * 
     * @param permanent The {@link #permanent} to set.
     */
    public void setPermanent(boolean permanent) {
        this.permanent = permanent;
    }

    /**
	 * Return the {@link #name} of the value. <br>
	 * 
	 * @return {@link #name}
	 */
	public String getName() {
		return name;
	}

	/**
	 * Set the {@link #name} of the value. <br>
	 * @param name see {@link #name}
	 */
	public void setName(String name) {
		this.name = name;
	}


	/**
	 * Return if the list of a multi values is empty. <br>
	 * 
	 * @return <code>true</code> only if the list of values is empty.
	 */
	public boolean isEmpty() {
		
		if (multiValue && values.size() == 0) {
			return true; 
		}
		
		return false;
	}



	/**
	 * Returns the property {@link #}. <br>
	 * 
	 * @return
	 */
	public boolean isObligatory() {
		return obligatory;
	}

	
	/**
	 * Returns the current {@link #value}. <br>
	 * 
	 * @return
	 */
	public String getValue() {
		
		if (multiValue) {
			return getValuesAsString();			
		}
		else {
			return value;
		}
	}

	
	/**
	 * Returns the {@link #values} of a multivalued context value. <br>
	 * 
	 * @return list with all values. 
	 */
	public List getValues() {
		
		if (multiValue) {
			return values;			
		}
		else {
		    List list = new ArrayList(1);
		    list.add(value);
			return list;
		}
	}
	
	
	/**
	 * Return all values of a multi value value concatenate as one string 
	 * separated by <code>,</code>. <br>
	 * 
	 * @return String contain all values.
	 */
	protected String getValuesAsString() {
		if (multiValue) {
		 
			 StringBuffer stringBuffer = new StringBuffer();
			 
			 Iterator iter = values.iterator();
			 
			 while (iter.hasNext()) {
	            stringBuffer.append((String)iter.next());
	            if (iter.hasNext()) {
					stringBuffer.append(',');	
	            }            
	        }
			 
			 return stringBuffer.toString();
		}
		return value;
	}
	

	/**
	 * Set the property {@link #value} of a single value value. <br>
	 * For multi values all other values will be deleted and then the new value
	 * will be added.
	 * 
	 * @param string new value.
	 */
	public void setValue(String value) {
		if (multiValue) {
			values.clear();
			values.add(value);
		}	
		else {
			this.value = value;
		}
	}

	
	/**
	 * Reset the value. <br>
	 * For multi values all values will be deleted.
	 */
	public void resetValue() {
		if (multiValue) {
			values.clear();
		}	
		else {
			this.value = "";
		}
		
	}

	
	/**
	 * Add the given value to the {@link #values} list. <br>
	 * Or set sets the single value with the given value.
	 * 
	 * @param string new value.
	 */
	public void addValue(String value) {
		if (multiValue) {
			values.add(value);
		}
		else {
			this.value = value;
		}
	}


	/**
	 * Clones the Context Value. <br>
	 * The method reset the values before the returning the new object. 
	 * 
	 */
	public Object clone() {
		
		try {
            ContextValue newValue = (ContextValue)super.clone();
            newValue.values = new ArrayList();
            newValue.value = "";
            return newValue;
        }
        catch (CloneNotSupportedException e) {
            throw new PanicException("Could not clone " + this.getClass().getName());
        }       
		 
	}


}
