/*****************************************************************************
    Class:        ContextManager
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      26.07.2004
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.core.ui.context;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;

import com.sap.isa.core.Constants;
import com.sap.isa.core.ContextConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.EComTokenizer;
import com.sap.isa.core.util.MiscUtil;
import com.sap.isa.core.util.StringUtil;
import com.sap.isa.core.util.WebUtil;

/**
 * The class ContextManager handles the context for the current request. <br>
 * The context manager distinguish between the old context provide from the 
 * request and the new request which is used to store in all subsequent requests.
 * 
 *
 * @author  SAP AG
 * @version 1.0
 */
public class ContextManager {
		
	/**
	 * Name of the current manager in the request. <br>
	 */
	public static final String MANAGER = "com.sap.isa.core.ui.context.ContextManager";

	/**
	 * Name of the current context map stored in the user session. <br>
	 */
	public static final String OPTIONAL_CONTEXT_MAP = "com.sap.isa.core.ui.context.ContextManager";

	
	/**
	  * Reference to the IsaLocation. <br>
	  *
	  * @see com.sap.isa.core.logging.IsaLocation
	  */
	 protected static IsaLocation log = IsaLocation.
			   getInstance(ContextManager.class.getName());


    /**
     * The key for optional context values is also a key.
     */
    static {
        GlobalContextManager.registerValue(Constants.CV_OPTIONAL_KEY, true, false);
    }

	/**
	 * context taken from Request.
	 */
	protected Map context = new HashMap();
	
	/**
	 * optional context taken from user session.
	 */
	private Map optional_context_map;

	/**
	 * Key for optional values.
	 */
	private String optionalValuesKey;
	
	/**
	 * context taken from Request.
	 */
	protected Map newContext = new HashMap();


	/**
	 * Return the manager instance currently store in the request. <br>
	 *
	 * If no instance exists, the instance will be created and stored in
	 * request.
	 *
	 * @param request current request.
	 * @return current manager instance.
	 */
	public static ContextManager getManagerFromRequest(HttpServletRequest request) {

        ContextManager manager = (ContextManager) request.getAttribute(MANAGER);

        if (manager == null) {
            manager = new ContextManager();
            manager.setOptionalContextMap(request);
            request.setAttribute(MANAGER, manager);
        }
        
        return manager;
	}

	/**
	 * Return the manager instance currently store in the request. <br>
	 * <strong>The context will be rebuild from the given context!</strong> 
	 *
	 * If no instance exists, the instance will be created and stored in
	 * request.
	 *
	 * @param request current request.
	 * @return current manager instance.
	 */
	public static ContextManager getManagerFromRequest(HttpServletRequest request, 
														String context) {

		ContextManager manager = getManagerFromRequest(request);

		manager.buildContextFromRequest(context);
		manager.getOptionalContextValues();
        
		return manager;
	}


	/**
	 * Change the context value in already existing context. <br>
	 * The value will also automatically transfer to the new context to allow a
	 * redirect between processing and display. 
	 * The method allows to change exiting values in one action (data processing).
	 * <p>
	 * <strong>Example</strong>
	 * <br>
	 * Action 1: getting new product id to display:
	 * <pre>
	 * String productKey = (String)request.getParameter("PRODUCT_KEY");
     *	
	 * changeContextValue("CV_PRODUCT_KEY", productKey);
	 * </pre> 
	 * 
	 * Action 2: displaying the current product:
	 * <pre>
	 * String productKey = getContextValue ("CV_PRODUCT_KEY");
     *	
	 * setContextValue("CV_PRODUCT_KEY", productKey);
	 * </pre> 
	 * </p>
	 * <strong>Note</strong>: If the <code>newValues</code> is <code>null</code> the 
	 * value will be deleted from the old and the new context.<br> 
	 * <strong>Note</strong>: Do not use this method for multi values. 
	 * 
	 * @param request http request
	 * @param name name of the context value
	 * @param newValue new value for the context value
	 */
	public void changeContextValue(String name, String newValue) {

		// remove the context values if the new values is null.
		if (newValue == null) {
			context.remove(name);
			removeContextValue(name);
			return;
		}

		ContextValue contextValue = (ContextValue)context.get(name);
		
		if (contextValue == null) {
			log.debug("value not in context "+ name);
			contextValue = GlobalContextManager.getValue(name);
			if (contextValue != null) {
				context.put(name,contextValue);
			}
			else {
				log.debug("Unknown context value: "+ name);
				return;
			}
		}

		contextValue.setValue(newValue);
		// also adjust the new context
		setContextValue(name, newValue);						
	}

	/**
	 * Set the given value for the context value. <br>
	 * <strong>Note</strong>: For multi values this value replaces all existing values. 
	 * 
	 * @param request http request
	 * @param name name of the context value
	 * @param newValue new value for the context value
	 */
	public void setContextValue(String name, String newValue) {

	    ContextValue contextValue = getContextValueFromGlobalManager(name);

		if (contextValue != null && newValue!=null) {
		    contextValue.setValue(newValue);
		}    
	}

	/**
	 * Add a value to the context values. <br>
	 * <strong>Note</strong>: Use this method only to add additional values to multi values. 
	 * 
	 * @param request http request
	 * @param name name of the context value
	 * @param newValue new value for the context value
	 */
	public void addContextValue(String name, String newValue) {

	    ContextValue contextValue = getContextValueFromGlobalManager(name);

		if (contextValue != null && newValue!=null) {
		    contextValue.addValue(newValue);
		}    
	}

	
	/**
	 * Delete all existing multiple values. <br>
	 * <strong>Note</strong>: Use this method only for multi values. 
	 * 
	 * @param request http request
	 * @param name name of the context value
	 */
	public void resetContextValue(String name) {

	    ContextValue contextValue = getContextValueFromGlobalManager(name);

		if (contextValue != null) {
		    contextValue.resetValue();
		}    
	}


	/**
	 * Remove a context value from the new context. <br>
	 * 
	 * @param request http request
	 * @param name name of the context value
	 */
	public void removeContextValue(String name) {
		
		newContext.remove(name);

	}
	
	
    /**
     * Set the property {@link #context}. <br>
     * 
     * @param context The {@link #context} to set.
     */
    public void setContext(Map context) {
        this.context = MiscUtil.copyMap(context);
    }
    
    
    /**
     * Return the property {@link #context}. <br> 
     *
     * @return Returns the {@link #context}.
     */
    public Map getContext() {
        return context;
    }
    

	/**
	 * Set the property {@link #newContext}. <br>
	 * 
	 * @param context The {@link #context} to set.
	 */
	public void setNewContext(Map context) {
		this.newContext = MiscUtil.copyMap(context);
	}
    
	/**
     * Return the context value for the given name. <br>
     * The value is taken from the new context. If it is not found then it is
     * taken from the {@link GlobalContextManager}. <br>
     * 
     * @param name name of the context value.
     * @return context value or <code>null</code> if the value could not be
     *          found.
     */
    protected ContextValue getContextValueFromGlobalManager(String name) {
        
        ContextValue contextValue = (ContextValue)newContext.get(name);
		
		if (contextValue == null) {
			log.debug("value not in context "+ name);
			contextValue = GlobalContextManager.getValue(name);
			if (contextValue != null) {
				newContext.put(name,contextValue);
			}
			else {
				log.debug("Unknown context value: "+ name);
			}
		}
        return contextValue;
    }

	/**
	 * Return the context value from the already existing context. <br>
	 * <strong>Note</strong>: Do not use method this for multi values. 
	 * 
	 * @param request http request
	 * @param name name of the context value
	 * @param newValue new value for the context value
	 */
	public String getContextValue(String name) {

		ContextValue contextValue = (ContextValue)context.get(name);
		
		if (contextValue == null) {
			log.debug("value not in context "+ name);
			return null;
		}

		return contextValue.getValue();						
	}

	
	/**
	 * Return the list of context values from the already existing context. <br>
	 * <strong>Note</strong>: Do not use this method for single value. 
	 * 
	 * @param request http request
	 * @param name name of the context value
	 * @param newValue new value for the context value
	 */
	public List getContextValues(String name) {

		ContextValue contextValue = (ContextValue)context.get(name);
		
		if (contextValue == null) {
			log.debug("value not in context "+ name);
			return null;
		}

		return contextValue.getValues();						
	}


	/**
	 * Save optional context values. <br>
	 *
	 * @return String with all context values. 
	 */
	private void getOptionalContextValues() {

        String  oldOptionalKey = getContextValue(Constants.CV_OPTIONAL_KEY);

        if (oldOptionalKey != null && oldOptionalKey.length() > 0 && optional_context_map != null) {
			Map values = (Map)optional_context_map.get(oldOptionalKey);
	
			if (values != null) {
				Iterator iter = values.values().iterator();
				
				while (iter.hasNext()) {
		            ContextValue value = (ContextValue) iter.next();
		            
		            if (!value.isEmpty()) {
		    			context.put(value.getName(),value);
		    			if (value.isPermanent()) {
		    			    newContext.put(value.getName(),value);
		    			}
		            }
				}
			}	
        }	
	}	

	// private method to build the context from the request.
	// all existing values will be cleared before. 
    private void buildContextFromRequest(String contextString) {
    
    	context.clear();
    	newContext.clear();
    	
    	StringBuffer contextStringBuffer = new StringBuffer(contextString);
    	StringUtil.replaceAll(contextStringBuffer,WebUtil.CONTEXT_QUESTIONMARK,"?");
    	StringUtil.replaceAll(contextStringBuffer,WebUtil.CONTEXT_SEMICOLON,";");
    	StringUtil.replaceAll(contextStringBuffer,WebUtil.CONTEXT_PERCENT,"%");
    	    	
    	StringTokenizer values = new EComTokenizer(contextStringBuffer.toString(), WebUtil.CONTEXT_SEPARATOR); 
    
        while (values.hasMoreElements()) {
            String token = (String) values.nextElement();

			StringTokenizer nameValue = new EComTokenizer(token, "=");
			String name = null;
			if (nameValue.hasMoreElements()) {
				name = (String) nameValue.nextElement();
			}
			String value = null;
			if (nameValue.hasMoreElements()) {
				value = (String) nameValue.nextElement();
			}

            if (name!=null && value!=null) {
            	
            	ContextValue contextValue = (ContextValue)context.get(name);
            	if (contextValue == null) {  
					contextValue = GlobalContextManager.getValue(name);
            	}
            	if (contextValue != null) {
            		contextValue.addValue(value);            		
        			context.put(name,contextValue);
        			if (contextValue.isPermanent()) {
        			    newContext.put(name,contextValue);
        			}
            	}
            	else {
            		log.debug("Unknown context parameter: "+ name);
            	}
            }
        }
    }
 
    
	/**
	 * Save optional context values. <br>
	 *
	 * @return String with all context values. 
	 */
	protected void storeOptionalContextValues() {
	
		boolean optionalExist = false;
		
		Map optional_context = null;
		
		Iterator iter = newContext.values().iterator();
		
		while (iter.hasNext()) {
            ContextValue value = (ContextValue) iter.next();
            
            if (!value.isObligatory() && !value.isEmpty()) {
            	
            	if (optional_context == null) {
            		optional_context = new HashMap();
            	}
            	
            	optional_context.put(value.getName(),value);
            	optionalExist = true;
            }
            
		}
		if (optionalExist && optional_context_map!= null) {
			optional_context_map.put(optionalValuesKey,optional_context);
			setContextValue(Constants.CV_OPTIONAL_KEY,optionalValuesKey);
		}
	}	

	/**
	 * Return all obilgatory values as one String separated with the
	 * {@link WebUtil#CONTEXT_SEPARATOR}. <br>
	 *
	 * @return String with all context values. 
	 */
	public String getContextValuesAsString() {
	
		storeOptionalContextValues();	
		StringBuffer parameter = new StringBuffer();
		boolean isFirst = true;
		
		Iterator iter = newContext.values().iterator();
		
		while (iter.hasNext()) {
            ContextValue value = (ContextValue) iter.next();
            
            if (!value.isObligatory() || value.isEmpty()) {
            	continue;
            }
            
            if (isFirst) {
				isFirst = false;
            }
            else {
            	parameter.append(WebUtil.CONTEXT_SEPARATOR);
            }
            
            if (value.isMultiValue()) {
                Iterator multiValueIter = value.getValues().iterator();
                while (multiValueIter.hasNext()) {
                    String element = (String) multiValueIter.next();
    				parameter.append(value.getName())
					 .append('=').append(WebUtil.encodeURL(element,ContextConst.DEFAULT_ENCODING_UTF8));
					if (multiValueIter.hasNext()) {
						parameter.append(WebUtil.CONTEXT_SEPARATOR);
					}
                }
            }
            else {            	
            	String valueString = WebUtil.encodeURL(value.getValue(),ContextConst.DEFAULT_ENCODING_UTF8); 
				parameter.append(value.getName())
						 .append('=').append(valueString);
            }
        } 
		return parameter.toString();
	}

	/**
	 * Add all old values to the new context. <br>
	 * <strong>If a old value already exit in the new context,
	 * it will be ignored.</strong>
     *
	 */
	public void addOldValues() {
		
		Iterator iter = context.entrySet().iterator();
			
		while (iter.hasNext()) {
			Map.Entry element = (Map.Entry) iter.next();
	            
			if (newContext.get(element.getKey())== null) {
				newContext.put(element.getKey(),element.getValue());
			}
	            
		}
	}

	
	private void setOptionalContextMap(HttpServletRequest request) {
		
		if (request.getSession() != null) {
			UserSessionData data = UserSessionData.getUserSessionData(request.getSession());
			if (data != null) {
				optional_context_map = (Map)data.getAttribute(OPTIONAL_CONTEXT_MAP);
				if (optional_context_map == null) {
					optional_context_map = new HashMap();
					data.setAttribute(OPTIONAL_CONTEXT_MAP,optional_context_map);
				}
				optionalValuesKey = "" + (optional_context_map.size()+1);
			}
		}
	}



}
