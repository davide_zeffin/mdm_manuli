/*****************************************************************************
    Class:        ReInvokeContainer
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      17.09.2004
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.core.ui.context;

import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.sap.isa.core.ui.layout.UILayoutManager;
import com.sap.isa.core.util.MiscUtil;

/**
 * The class ReInvokeContainer holds all information for the reInvoke of the 
 * application and for the UIAction command. <br>
 * The container includes
 * <ol>
 * <li> all context values </li>
 * <li> all request parameter from the header </li>
 * <li> action to be called after Reinvoke </li>
 * </ol>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class ReInvokeContainer {
    
        
	/**
	 * Name of the current manager in the request data. <br>
	 */
	public static final String CONTAINER = "com.sap.isa.core.ui.context.ReInvokeContainer";

	/**
	 * Prefix to indicate Startup parameter. <br>
	 */
	public static final String STARTUP_PREFIX = "STARTUP_";
	
    /**
     * Map with available context information for the ReInvoke. <br>
     */
    protected Map context;
    
    
    /**
     * Name of the action which should be called by the ReInvoke action. 
     */
    protected String action;

    /**
     * Map with all request parameter. <br>
     */
    protected Map requestParameter;	
    
    
    /**
     * Field to define the usage of the reInvoke container.
     */
    protected String usage = "";
    
    /**
     * Constructor to build up the reInvoke Containter. <br>
     * 
     * @param action
     * @param context
     */
    public ReInvokeContainer(String action,Map context) {
        super();
        this.action = action;
        this.context = MiscUtil.copyMap(context);
    }
    
    
    /**
     * Set the property {@link #action}. <br>
     * 
     * @param action The {@link #action} to set.
     */
    public void setAction(String action) {
        this.action = action;
    }
  
    
    /**
     * Return the property {@link #action}. <br> 
     *
     * @return Returns the {@link #action}.
     */
    public String getAction() {
        return action;
    }
    
    
    /**
     * Set the property {@link #context}. <br>
     * 
     * @param context The {@link #context} to set.
     */
    public void setContext(Map context) {
        this.context = context;
    }

    
    /**
     * Return the property {@link #context}. <br> 
     *
     * @return Returns the {@link #context}.
     */
    public Map getContext() {
        return context;
    }
    
    
    /**
     * Return the property {@link #requestParameter}. <br>
     * 
     * @return {@link #requestParameter}
     */
    public Map getRequestParameter() {
        return requestParameter;
    }


    /**
     * Set the property {@link #requestParameter}. <br>
     * 
     * @param requestParameter {@link #requestParameter}
     */
    public void setRequestParameter(Map requestParameter) {
        this.requestParameter = requestParameter;
    }


	/**
	 * Return the property {@link #usage}. <br>
	 * 
	 * @return
	 */
	public String getUsage() {
		return usage;
	}


	/**
	 * Set the property {@link #usage}. <br>
	 * 
	 * @param usage
	 */
	public void setUsage(String usage) {
		this.usage = usage;
	}


  	/**
     * Return an action with all parameter and also adjust the current context 
     * with the values stored in the container. <br>
     * The current UILayout Manager will also removed from the request.
     * <strong>You have to use always a redirect after the reinvoke to ensure that the 
     * context values will be encoded in the action.</strong>
     *  
     * @param request current http request.
     * @return action with all parameter
     * 
     */
	public String reInvoke(HttpServletRequest request) {
		
		boolean parameterFound = false;
		StringBuffer path = new StringBuffer(getAction());
		
		// Add request parameter.
		if (getRequestParameter()!= null) {
			Iterator iter = getRequestParameter().entrySet().iterator();
			while (iter.hasNext()) {
				Map.Entry element = (Map.Entry) iter.next();
	
				String parameterKey = (String)element.getKey();
				if (parameterKey.indexOf(STARTUP_PREFIX) == -1) {
					String[] values = (String[])element.getValue();                
		                
					for (int i = 0; i < values.length; i++) {
						if (parameterFound) {
							path.append("&");
						}
						else {
							path.append("?");
							parameterFound = true;
						}
						path.append(parameterKey).append('=').append(values[i]);
					}
				}	
			} 
		}
        	
        // Adjust the context values
		ContextManager contextManager = ContextManager.getManagerFromRequest(request);
		contextManager.setContext(getContext());
		contextManager.setNewContext(getContext());
		
		// Remove the UI layout Manager from the request.	
		UILayoutManager layoutManager = (UILayoutManager) request.getAttribute(UILayoutManager.MANAGER);
		if (layoutManager != null) {
			request.removeAttribute(UILayoutManager.MANAGER); 
		}
		
		return path.toString();
	}


    /**
     * Clear the request parameter. <br>
     * 
     */
    public void clearRequestParameter() {
    	requestParameter.clear();
    }

}
