/*****************************************************************************
    Class:        StyleSheet
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      03.11.2004
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.core.ui;

import java.util.HashMap;
import java.util.Map;

/**
 * <The class StyleSheet represent a style sheet file which is used in the application. <br>
 * The class mainly manages the mapping between browser and file extension. 
 *
 * @author  SAP AG
 * @version 1.0
 */
public class StyleSheet {
	
	/**
	 * Logical key for the style sheet file. <br>
	 */
	protected String key = "";

	/**
	 * Used path for the style sheet. <br>
	 */
	protected String path = "";
	
	static protected String DEFAULT_MAPPING = "_default";
	
	static protected String DEFAULT_GROUP = "default";

	
	/**
	 * maps a browser type to a file extension with an existing style sheet file.
	 */
	protected Map browserTypeExtensions = new HashMap();


	/**
	 * group to which the Stylesheet belongs.
	 */
	protected Map groups = new HashMap();

	
	/**
	 * Standard constructor for the style sheet.
	 */
	public StyleSheet(String key, String path) {
		this.key = key;
		this.path = path;
	}	
	

	/**
	 * Return Set the property {@link #}. <br>
	 * 
	 * @return
	 */
	public Map getBrowserTypeExtensions() {
		return browserTypeExtensions;
	}

	/**
	 * Return Set the property {@link #}. <br>
	 * 
	 * @param map
	 */
	public void setBrowserTypeExtensions(Map map) {
		browserTypeExtensions = map;
	}


	/**
	 * Set the extension, which should be used to build the filename in the 
	 * StyleSheetTag for the given browser type. <br>
	 * 
	 * @param browserType type of the browser
	 * @param usedExtension extension used in the stylesheet filename for the browser
	 */
	public void setBrowserTypeExtension(String browserType, String usedExtension) {
		browserTypeExtensions.put(browserType, usedExtension);
	}
	
	
	/**
	 * Return the extension which should be used for the given browser type. <br>
	 * 
	 * @param browserType
	 * @return
	 */
	public String getBrowserTypeExtension(String browserType) {

		String extension = (String) browserTypeExtensions.get(browserType);
		if (extension != null) {
			return extension;
		}

		extension = (String) browserTypeExtensions.get(DEFAULT_MAPPING);
		if (extension != null) {
			return extension;
		}

		return "";
	}


    /**
     * Return the key of the stylesheet file. <br>
     * 
     * @return
     */
    public String getKey() {
        return key;
    }


    /**
     * Return the file path to the stylesheet file. <br>
     * 
     * @return
     */
    public String getPath() {
        return path;
    }

    
    /**
     * <p> The method addGroup add the given group to the style sheet.</p>
     * 
     * @param group name of the group.
     */
    public void addGroup(String group) {
    	groups.put(group, group);
    }

    
	/**
	 * <p>Return if the style sheet belongs to a given group. </p>
	 * <p>If there are no groups maintained at least <code>default</code> group is supported. </p>
	 * <p><strong>Note: </strong>Also if the default group maintain the stylesheet is always used.</p> 
	 * 
	 * @param group group 
	 * @return <code>true</code> if the style sheet belongs to the given group
	 */
	public boolean checkGroup(String group) {
		if (groups.isEmpty()) {
		 return group.equals(DEFAULT_GROUP);
		}
		if (groups.get(DEFAULT_GROUP) != null) {
			return true;
		}
		
		return groups.get(group) != null;
	}


}
