package com.sap.isa.core.ui;

import javax.servlet.http.HttpServletRequest;

/**
 * Title:        ESales_BASE
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public class BrowserType {

    public final static String DEFAULT = "";
	public final static String NETSCAPE = "_nn";
    public final static String NETSCAPE_4 = "_nn4";
    public final static String NETSCAPE_6 = "_nn6";
    public final static String NETSCAPE_7 = "_nn7";
	public final static String OPERA = "_op";
    public final static String OPERA7 = "_op7";
	public final static String GECKO = "_ge";
	public final static String WEBKIT = "_wk";
    public final static String MSIE4 = "_ie4";
    public final static String MSIE5 = "_ie5";
    public final static String MSIE55 = "_ie55";
    public final static String MSIE6 = "_ie6";
	public final static String MSIE7 = "_ie7";
	public final static String MSIE8 = "_ie8";
	public final static String MSIE = "_ie";
    
    
    /**
     * Return the browser type determined from the http header. <br>
     * 
     * @param httpServletRequest
     * @return browser type
     */
	static public String getBrowserType(HttpServletRequest httpServletRequest)  {
		// Try to determine User-Agent
		String userAgent = httpServletRequest.getHeader("User-Agent");
		if (userAgent != null) {
			userAgent = userAgent.toUpperCase();			
			String userAgentUP = userAgent.toUpperCase();
			
			int msie_start_pos = userAgentUP.indexOf("MSIE");
			if (msie_start_pos >= 0) {
				// extract MSIE part (avoids confusion with 'Windows 5.0' part):
				int msie_end_pos = userAgentUP.indexOf(";", msie_start_pos);
				String msie_part = (msie_end_pos >= 0) ?
					userAgentUP.substring(msie_start_pos, msie_end_pos) :
					userAgentUP.substring(msie_start_pos);
				if (msie_part.indexOf("5.01") >= 0) {
					return BrowserType.MSIE5; // 5.01? Differences??
				} else if (msie_part.indexOf("5.0") >= 0) {
					return BrowserType.MSIE5;
				} else if (msie_part.indexOf("5.5") >= 0) {
					return BrowserType.MSIE55;
				} else if (msie_part.indexOf("8.0") >= 0) {
					return BrowserType.MSIE8;
				} else if (msie_part.indexOf("6.0") >= 0) {
					return BrowserType.MSIE6;
				} else if (msie_part.indexOf("7.0") >= 0) {
					return BrowserType.MSIE7;		
				} else {
					return BrowserType.MSIE;
				}
			}  else if(userAgent.indexOf("OPERA/7") >= 0)  {
				return BrowserType.OPERA7;
			}  else if(userAgent.indexOf("OPERA") >= 0)  {
				return BrowserType.OPERA;
			}  else if(userAgent.indexOf("APPLEWEBKIT") >= 0)  {
				return BrowserType.WEBKIT;
			}

			//MOZILLA/5.0 (WINDOWS; U; WINDOWS NT 5.0; EN-US; RV:1.0.1) GECKO/20020823 NETSCAPE/7.0
			else if(userAgent.indexOf("NETSCAPE/7.0") >= 0)  {
			   return BrowserType.NETSCAPE_7;
			}  else if (userAgent.indexOf("MOZILLA/5.0") >= 0) {
				if(userAgentUP.indexOf("FIREFOX") >= 0) {
				return BrowserType.GECKO;
				} else {
				return BrowserType.NETSCAPE_6;
				}
			} else if (userAgent.indexOf("MOZILLA/4.7") >= 0) {
				// Different for 4.74 vs. 475??
				return BrowserType.NETSCAPE_4;
			}  else if(userAgent.indexOf("NETSCAPE") >= 0)  {
				return BrowserType.NETSCAPE;
			}  else if(userAgent.indexOf("GECKO") >= 0)  {
				return BrowserType.GECKO;
			} else {
				return BrowserType.DEFAULT;
			}
		}
		return BrowserType.DEFAULT;
	}
    
    
    
    
}
