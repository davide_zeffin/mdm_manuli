package com.sap.isa.core.ui;

import java.util.ArrayList;
import java.util.Collection;


/**
 * Title:        ESales_BASE
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */


public class StyleManager {

  private static ArrayList styleResources = new ArrayList();
  public static String mimesRoot = "mimes/shared/style";
  public static final String DEFAULTRESOURCEPATH = "";
  public static final String DEFAULTTHEMEPATH = "themes";


  public static Collection getStyleResources()  {
      return styleResources;
  }

  public static void addResource(String resource)  {
      styleResources.add(resource);
  }

  public static String getMimesPath()  {
      return mimesRoot;
  }

  public static void setMimesPath(String path)  {
      mimesRoot = path;
  }
}