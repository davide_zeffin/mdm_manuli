/*****************************************************************************
	Class:        ExtendedInitStyleHandler
	Copyright (c) 2004, SAPMarkets Europe GmbH, Germany, All rights reserved.
	Author:       SAP
	Created:      March 2004
	Version:      1.0

	$Revision: #1 $
	$Date: 2001/08/01 $
*****************************************************************************/
package com.sap.isa.core.ui;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.SortedMap;
import java.util.TreeMap;

import com.sap.isa.core.init.Initializable;
import com.sap.isa.core.init.InitializationEnvironment;
import com.sap.isa.core.init.InitializeException;
import com.sap.isa.core.util.EComTokenizer;


/**
 * The class ExtendedInitStyleHandler fills the ExtendedStyleManager with data
 * provided from the init-config.xml. <br>
 * Only properties with a key which include the string "CSS" will be interpret
 * as style sheet resources. All other parameter will be used to map browser 
 * type to the file extension.   
 *
 * @author  SAP AG
 * @version 1.0
 * 
 * @see com.sap.isa.core.ui.ExtendedStyleManager 
 */
public class ExtendedInitStyleHandler implements Initializable {

	/**
	 * <p>Suffix for group entries </p> 
	 */
	public static String GROUP_SUFFIX = "_group"; 

	/**
	 * <p>Group for undefined group parameter </p> 
	 */
	public static String UNDEFINED_GROUP_DEFAULT = "group_for_undefined_group_parameter"; 

    public ExtendedInitStyleHandler() {
    }

	/**
	 * This method is called from the InitializationHandler to intialize the
	 * ExtendedStyleManager. <br>
	 * 
	 * @param env
	 * @param props
	 * @throws InitializeException
	 * 
	 */
    public void initialize(InitializationEnvironment env, Properties props) 
    		throws InitializeException {

        try {
        	
            Enumeration lists = props.keys();
            
            SortedMap styles = new TreeMap();
            
            String keyId = null;
            
            Map mappingsMap = new HashMap();
            Map mappings;
			Map globalMappings = new HashMap();
			
			Map groupMap = new HashMap();
            

            while (lists.hasMoreElements()) {
                keyId = (String) lists.nextElement();
                
                if (keyId.equals(UNDEFINED_GROUP_DEFAULT)) {
                	ExtendedStyleManager.setUndefinedGroupDefault(props.getProperty(keyId));
                }

				int index = keyId.indexOf("_");	
                if (index >=0) {
                	// check Browser mappings and style groups 

					String styleKey = keyId.substring(0,index);
                	
                	String suffix = keyId.substring(index); 
                	if (suffix.equals(GROUP_SUFFIX)) {
                		// style group is found 
                		groupMap.put(styleKey, props.getProperty(keyId));
                		continue;
                	}
                	
					mappings = globalMappings;
					if (index >0) {
						// no global mapping

						// check if there exists already an mapping map for the stylesheet definition.
						mappings = (Map)mappingsMap.get(styleKey);
						if (mappings == null) {
							mappings = new HashMap();
							mappingsMap.put(styleKey, mappings);
						}
					}
					
					mappings.put(suffix, props.getProperty(keyId));
					
                    continue;
                }
                
                // add styles to a sortmap to sort the entries.
				styles.put(keyId, new StyleSheet(keyId, props.getProperty(keyId)));
            }


            Iterator iter = styles.values().iterator();
            while (iter.hasNext()) {
				StyleSheet styleSheet = (StyleSheet) iter.next();
				ExtendedStyleManager.addStyleSheet(styleSheet);
				Map browserMappings = (Map) mappingsMap.get(styleSheet.getKey());
				if (browserMappings == null) {
					browserMappings = globalMappings;
				}
				styleSheet.setBrowserTypeExtensions(browserMappings);
				
				// add style groups
				String groups = (String)groupMap.get(styleSheet.getKey());
				if (groups==null) {
					groups = (String)groupMap.get("");
				}
				if (groups!= null && groups.length()>0) {
					EComTokenizer tokenizer = new EComTokenizer(groups,',');
					while (tokenizer.hasMoreTokens()) {
						styleSheet.addGroup(tokenizer.nextToken().trim());
					}	
				}
            }
        }
        catch (Exception exc) {
            throw new InitializeException("Unable to initialize the style file list");
        }
    }


  	/**
	 * Implements the method terminate. <br>
	 * 
	 * @see com.sap.isa.core.init.Initializable#terminate()
	 */
	public void terminate() {
		ExtendedStyleManager.clear();
	}

}
