/*****************************************************************************
    Class:        UIAreaContainer
    Copyright (c) 2005, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      16.09.2005
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.core.ui.layout;

/**
 * The class UIAreaContainer allows you to group some subsequent UIAreas in one container. <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class UIAreaContainer {
	
	/**
	 * Name of the container. </br>
	 */
	protected String name = "";
	
	/**
	 * Name of the CSS id to be used for this area container. <br>
	 */
	protected String cssIdName = "";
	
	/**
	 * Name of the area the container starts with. <br>
	 */
	protected String startArea = "";
	

	/**
	 * Name of the area the container starts with. <br>
	 */
	protected String endArea = "";
	


    /**
     * Return the property {@link #cssIdName}. <br> 
     *
     * @return Returns the {@link #cssIdName}.
     */
    public String getCssIdName() {
		if (cssIdName.length()>0) {
			return cssIdName;
		}
		else {
			return name;
		}
    }
    
    /**
     * Set the property {@link #cssIdName}. <br>
     * 
     * @param cssIdName The {@link #cssIdName} to set.
     */
    public void setCssIdName(String cssIdName) {
        this.cssIdName = cssIdName;
    }
    
    /**
     * Return the property {@link #endArea}. <br> 
     *
     * @return Returns the {@link #endArea}.
     */
    public String getEndArea() {
        return endArea;
    }
    
    /**
     * Set the property {@link #endArea}. <br>
     * 
     * @param endArea The {@link #endArea} to set.
     */
    public void setEndArea(String endArea) {
        this.endArea = endArea;
    }
    
    /**
     * Return the property {@link #name}. <br> 
     *
     * @return Returns the {@link #name}.
     */
    public String getName() {
        return name;
    }
    
    /**
     * Set the property {@link #name}. <br>
     * 
     * @param name The {@link #name} to set.
     */
    public void setName(String name) {
        this.name = name;
    }
    
    /**
     * Return the property {@link #startArea}. <br> 
     *
     * @return Returns the {@link #startArea}.
     */
    public String getStartArea() {
        return startArea;
    }
    
    /**
     * Set the property {@link #startArea}. <br>
     * 
     * @param startArea The {@link #startArea} to set.
     */
    public void setStartArea(String startArea) {
        this.startArea = startArea;
    }
}
