/*****************************************************************************
    Class:        UICommand
    Copyright (c) 2005, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      15.02.2005
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.core.ui.layout;

import javax.servlet.http.HttpServletRequest;

import com.sap.isa.core.PanicException;
import com.sap.isa.core.RequestProcessor;

/**
 * The class UICommand describes a possible ui command which could be used in the 
 * config.xml file. <br>
 * <strong>Example</strong>
 * <pre>
 * &lt;action path="/base/start.do"         
 *            type="com.sapmarkets.isacore.core.b2c.action.StartAction"&gt;
 *    &lt;forward name="success"
 *                path="UILayout:catalogLayout[header=header,workarea=catalogentry]"/&gt;
 *    &lt;forward name="error" 
 *                path="UILayout:catalogLayout[header=header,workarea=error]"/&gt;
 *        ...
 * &lt;/action&gt; 
 * </pre>
 *
 * @author  SAP AG
 * @version 1.0
 */
public interface UICommand {
	
	/**
	 * This method process the ui command. <br>
	 * 
	 * @param processor current struts request processor
	 * @param request current HTTP request
	 * @param forward logical which should be processed
	 * @param layoutHandler reference to the layout handler
	 * @param layoutManager reference to the layout manager
	 * @param token currently token
	 * @param lastCommand flag if the command is the last command in a sequence
	 * @param redirect flag if a redirect should be processed
	 * @return UICommandReturnValue
	 * @throws PanicException
	 */
	public UICommandReturnValue processCommand(RequestProcessor processor,
			 					                HttpServletRequest request,
								                String forward,
							                    GlobalUILayoutHandler layoutHandler,
								                UILayoutManager layoutManager,
								                String token,
								                boolean lastCommand,
								                boolean redirect)
			throws PanicException ;


	/**
	 * Return the name of the command. <br>
	 * 
	 * @return name of the command.
	 */
	public String getName();

}
