/*****************************************************************************
    Class:        UICommandReturnValue
    Copyright (c) 2005, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      15.02.2005
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.core.ui.layout;

/**
 * The class UICommandReturnValue provide information from the execution of
 * a {@link com.sap.isa.core.ui.layout.UICommand}. <br>
 *
 * @author  SAP AG
 * @version 1.0
 * @see com.sap.isa.core.ui.layout.UICommand
 */
public class UICommandReturnValue {
	
	
	private String forward;
	
	private boolean redirect;
	
		
	public UICommandReturnValue(String forward, boolean redirect) {
		this.forward = forward;
		this.redirect = redirect;
	}


    /**
     * Return the logical forward which should be used. <br>
     * 
     * @return String with logical forward
     */
    public String getForward() {
        return forward;
    }


    /**
     * Return if the response should be a redirect. <br>
     * 
     * @return <code>true</code> if it is redirect.  
     */
    public boolean isRedirect() {
        return redirect;
    }

}
