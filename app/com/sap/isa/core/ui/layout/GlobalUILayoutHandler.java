/*****************************************************************************
    Class:        GlobalUILayoutHandler
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      March 2004
    Version:      1.0

    $Revision: #7 $
    $Date: 2001/07/25 $
*****************************************************************************/
package com.sap.isa.core.ui.layout;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.commons.digester.Digester;

import com.sap.isa.core.PanicException;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.EComTokenizer;
import com.sap.isa.core.xcm.ConfigContainer;

/**
 * The GlobalUILayoutHandler manages the layouts for a frameless web application. <br>
 *
 * The class parses also the layout-config.xml to get the current layout configuration.
 * <br>
 *
 *
 * The class follows the singelton pattern. To get the instance use the
 * {@link com.sap.isa.ui.layoutUILayoutInitHandler#getHandlerInstance}.
 * This allows the customer to extend the GlobalUILayoutHandler class. <br>
 *
 *
 * <h3>How to modify this class</h3>
 * To extend this class you have to overwrite {@link #createInstance} method.
 * You must also overwrite the
 * {@link com.sap.isa.ui.layoutUILayoutInitHandler#getHandlerInstance} method
 * and returns your instance in this method. The modified InitHandler could be register
 * in the init.config.xml.
 *
 *
 * @author SAP AG
 * @version 1.0
 *
 * @see com.sap.isa.maintenanceobject.backend.MaintenanceObjectDataHandler
 *
 */
public class GlobalUILayoutHandler {

    /**
     * Reference to the IsaLocation. <br>
     *
     * @see com.sap.isa.core.logging.IsaLocation
     */
    protected static IsaLocation log = IsaLocation.
          getInstance(GlobalUILayoutHandler.class.getName());

    
    static public final char KEY_SEPARATOR = '_';

	static public final char VERSION_SEPARATOR = '-';


    /**
     * Used instance (Singeleton Pattern). <br>
     */
    protected static GlobalUILayoutHandler instance;


	/**
	 * Name of the class to use for UILayout. <br>
	 *
	 */
	protected String CONFIGURATION_CLASS_NAME = "";

    /**
     * Name of the class to use for UILayout. <br>
     *
     */
    protected String LAYOUT_CLASS_NAME = "";
    
    protected String HTML_ATTRIBUTE_CLASS_NAME = "";

	protected String AREA_CONTAINER_CLASS_NAME = "";

    /**
     * Name of the class to use for UIArea. <br>
     *
     */
    protected String AREA_CLASS_NAME = "";


    /**
     * Name of the class to use for Form. <br>
     *
     */
    protected String FORM_CLASS_NAME = "";


    /**
     * Name of the class to use for UIComponent. <br>
     *
     */
    protected String COMPONENT_CLASS_NAME = "";

	/**
	 * Configuration object with global settings.
	 */
	protected UILayoutConfiguration configuration = new UILayoutConfiguration ();

    /**
     * Map, which includes all layouts. <br>
     */
    protected Map uiLayouts = new HashMap();

    /**
     * Map, which includes all layouts with alternative key. <br>
     */
    protected Map uiLayoutsAlt = new HashMap();


    /**
     * Map, which includes all components. <br>
     */
    protected Map uiComponents = new HashMap();

    /**
     * Map, which includes all components  with alternative key. <br>
     */
    protected Map uiComponentsAlt = new HashMap();


    /**
     * The file alias used in the XCM configuration. <br>
     */
    protected String FILE_ALIAS= "layout-config";

    // ensures singeleton pattern
    private static boolean isInitialized = false;
    // carriage line feed
    private final static String CRLF = System.getProperty("line.separator");


    /**
     * Constructor for GlobalUILayoutHandler. <br>
     */
    protected GlobalUILayoutHandler () {
        if (isInitialized) {
            throw new PanicException("Only one instance is allowed in the singelton pattern");
        }
        initInstance();
        isInitialized = true;
    }


    /**
     * Create the soley instance of the singelton. <br>
     * This method could be overwritten for modification.
     * @return the instance
     */
    protected static GlobalUILayoutHandler createInstance() {
            instance = new GlobalUILayoutHandler();
            return instance;
    }


    /**
     * Return the only instance of the class (Singelton). <br>
     * <strong>Don't use this method directly. Please use
     * {@link com.sap.isa.ui.layout.UILayoutInitHandler#getHandlerInstance}
     * instead to allow customer modification.</strong>
     *
     * @return
     */
    public static synchronized GlobalUILayoutHandler getInstance() {
    	if( instance == null)
			createInstance();
        return instance;
    }


    /**
     * Initialize the instance. <br>
     * This method set the class names for the used classes.
     */
    protected void initInstance() {
    	CONFIGURATION_CLASS_NAME = UILayoutConfiguration.class.getName();
        LAYOUT_CLASS_NAME = UILayout.class.getName();
        AREA_CLASS_NAME = UIArea.class.getName();
        COMPONENT_CLASS_NAME = UIComponent.class.getName();
        FORM_CLASS_NAME = UILayout.Form.class.getName();
		HTML_ATTRIBUTE_CLASS_NAME = HtmlAttribute.class.getName();
		AREA_CONTAINER_CLASS_NAME = UIAreaContainer.class.getName();
    }


    /**
     * Return a copy of the component with the given name. <br>
     *
     * @param name name of the component
     */
    public UIComponent getComponent(String name) {

		UIComponent uiComponent = (UIComponent)uiComponents.get(name);
		
		if (uiComponent != null) {
			try {
                return (UIComponent) uiComponent.clone();
            }
            catch (CloneNotSupportedException e) {
                log.error(LogUtil.APPS_USER_INTERFACE,"system.exception",e);
            }
		}
		
        return null;
    }


    /**
     * Return a copy of the layout with the given name. <br>
     *
     * @param name name of the layout
     */
    public UILayout getLayout(String name) {
		
		UILayout uiLayout = (UILayout)uiLayouts.get(name);
		
		if (uiLayout != null) {
			try {
                return (UILayout)uiLayout.clone();
            }
            catch (CloneNotSupportedException e) {
                log.error(LogUtil.APPS_USER_INTERFACE,"system.exception",e);
            }
		}

        return null;
    }


	/**
	 * Check if the version of the given key fits to version defined in the 
	 * configuration. <br>
	 * 
	 * @param key
	 * @return <code>true</code> if the version is ok.
	 */
	public boolean checkVersion(String key) {

		String version = "";
		
		int index = key.indexOf(VERSION_SEPARATOR);
		if (index  >= 0 ) {
			version = key.substring(0,index);
		}
		
		return configuration.getVersion().equals(version);
	}


    /**
     * Return the layout with the given layout key. <br>
     * The consist of the name of the layout and the name of all components in
     * the areas. The names are separated with a underline. <br>
     * <strong>Example</strong><br>
     * <pre>catalogLayout_header_basket_minibasket.</pre>
     *
     * @param name name of the layout
     */
    public UILayout getLayoutFromKey(String key) {

		String myKey = key;
		
		int index = myKey.indexOf(VERSION_SEPARATOR);
		if (index  >= 0 ) {
			myKey = key.substring(index+1);
		}
        
        StringTokenizer tokenizer = new EComTokenizer(myKey, KEY_SEPARATOR);
        UILayout layout = null;

        if (tokenizer.hasMoreElements()) {
            String name = (String) tokenizer.nextElement();
            layout = (UILayout) uiLayoutsAlt.get(name);
        }
        else {
            return layout;
        }

        if (layout == null) {
            return layout;
        }

		try {
            layout = (UILayout)layout.clone();
        }
        catch (CloneNotSupportedException e) {
            log.error(LogUtil.APPS_USER_INTERFACE,"system.exception",e);
        }

        Iterator iter = layout.iterator();
        while (iter.hasNext()) {
            UIArea area = (UIArea) iter.next();

            if (tokenizer.hasMoreElements()) {
                UIComponent component =
                    (UIComponent) uiComponentsAlt.get(
                        tokenizer.nextElement());
                if (component != null) {
                    try {
                        area.setComponent((UIComponent)component.clone());
                    }
                    catch (CloneNotSupportedException e1) {
                        log.error(LogUtil.APPS_USER_INTERFACE,"system.exception",e1);
                    }
                }
            }
        }

        if (tokenizer.hasMoreElements()) {
            index = Integer.parseInt((String) tokenizer.nextElement());
            layout.setActiveArea((UIArea) layout.areaList.get(index));
        }

        return layout;
    }


    
	/**
	 * Return the logical key for the given layout. <br>
	 * The logical key is build up from the {@link UILayout#altKey} and
	 * the {@link UIComponent#altKey} separated with the 
	 * {@link GlobalUILayoutHandler#KEY_SEPARATOR}. 
	 * 
	 * @param uiLayout ui layout
	 * @return logical key
	 */
	public String getLayoutKey(UILayout uiLayout) {

		String version = configuration.getVersion();
		
		version = version!=null && version.length()>0?version+VERSION_SEPARATOR :"";

		StringBuffer sb = new StringBuffer(version);

		sb.append(uiLayout.getAltKey());

		Iterator iter = uiLayout.iterator();

		while (iter.hasNext()) {
			UIArea area = (UIArea) iter.next();
			sb.append(KEY_SEPARATOR).append(area.getComponent().getAltKey());
		}

		if (uiLayout.getActiveArea() != null) {
			sb.append(GlobalUILayoutHandler.KEY_SEPARATOR).append(uiLayout.getActiveArea().getAltKey());
		}

		return sb.toString();
	}
    


    /**
     * Read the layout data for the given XCM config container. <br>
     * <strong>Note:</strong> This method should be called only from the
     * init handler.
     *
     * @param configContainer
     * @throws GlobalUILayoutHandlerException
     */
    public void readLayoutData(ConfigContainer configContainer)
    		throws GlobalUILayoutHandlerException {

        uiComponents.clear();
        uiComponentsAlt.clear();
        uiLayouts.clear();
        uiLayoutsAlt.clear();

        InputStream inputStream =
            configContainer.getConfigUsingAliasAsStream(FILE_ALIAS);

        if (inputStream != null) {
            parseConfigFile(inputStream);

            // update the areas with the assigned default components
            Iterator iter = uiLayouts.values().iterator();

            while (iter.hasNext()) {
                UILayout layout = (UILayout) iter.next();
                Iterator areaIterator = layout.iterator();
                while (areaIterator.hasNext()) {
                    UIArea area = (UIArea) areaIterator.next();
                    UIComponent uiCompoment =
                        (UIComponent) uiComponents.get(
                            area.getDefaultComponent());
                    if (uiCompoment != null) {
                        area.setComponent(uiCompoment);
                    }
                    else {
                        // Default component {1} is missing for area {0}
                        String msg = "lay.error.defaultComponent";
                        String args[] = {area.getName(), area.getDefaultComponent()};
                        log.error(LogUtil.APPS_USER_INTERFACE,msg, args, null);
                    }
                }
            }
        }
        else {
            //Entry [" + {0} + "] in xcm-config.xml missing
            String msg = "lay.error.fileAlias";
            String args[] = { FILE_ALIAS };
            // log.error(msg,args,null);
            log.error(LogUtil.APPS_USER_INTERFACE,msg, args, null);
        }

    }


    /**
     * Clear all included data. <br>
     * <strong>Note:</strong> This method should be called only from the
     * init handler.
     */
    public void clearLayoutData() {
        uiComponents.clear();
        uiComponentsAlt.clear();
        uiLayouts.clear();
        uiLayoutsAlt.clear();
    }


	/**
	 * Return the property {@link #configuration}. <br>
	 * 
	 * @return
	 */
	public UILayoutConfiguration getConfiguration() {
		return configuration;
	}

	/**
	 * Set the property {@link #configuration}. <br>
	 * 
	 * @param configuration
	 */
	public void setConfiguration(UILayoutConfiguration configuration) {
		this.configuration = configuration;
	}

    /**
     * Add the given layout. <br>
     *
     * @param layout ui layout
     *
     */
  	public void addUILayout(UILayout layout) {

        uiLayouts.put(layout.getName(), layout);

        String index  = configuration.getAltKeyPrefix() + uiLayoutsAlt.size();
        uiLayoutsAlt.put(index, layout);
        if (layout.getAltKey().length() == 0) {
			layout.setAltKey(index);
        }
    }


    /**
     * Add the given component. <br>
     *
     * @param Component ui Component
     *
     */
    public void addUIComponent(UIComponent component) {

        uiComponents.put(component.getName(), component);

        String index  = configuration.getAltKeyPrefix() + uiComponentsAlt.size();
        uiComponentsAlt.put(index, component);
		if (component.getAltKey().length() == 0) {
        	component.setAltKey(index);
		}	
    }


    /* private method to parse the config-file */
    private void parseConfigFile(InputStream inputStream)
                throws GlobalUILayoutHandlerException {


        // new Struts digester
        Digester digester = new Digester();

        digester.push(this);


		// parsing configuration
		String path = "UILayouts/UIConfiguration";

		// create a new Layout
		digester.addObjectCreate(path, CONFIGURATION_CLASS_NAME);

		// set all properties for the layout
		digester.addSetProperties(path);

		// add layout to map
		digester.addSetNext(path, "setConfiguration", UILayoutConfiguration.class.getName());

        // parsing layouts
        path = "UILayouts/UILayout";

        // create a new Layout
        digester.addObjectCreate(path, LAYOUT_CLASS_NAME);

        // set all properties for the layout
        digester.addSetProperties(path);

        //create body tag attributes and add them to the layout
        digester.addObjectCreate(path + "/BodyAttribute", HTML_ATTRIBUTE_CLASS_NAME);
        digester.addSetProperties(path  + "/BodyAttribute");
        digester.addSetNext(path + "/BodyAttribute", "addBodyAttribute", HtmlAttribute.class.getName());
        
        // add layout to map
        digester.addSetNext(path, "addUILayout", UILayout.class.getName());

        // create a new area
        digester.addObjectCreate(path + "/UIArea", AREA_CLASS_NAME);

        // set all properties for the area
        digester.addSetProperties(path + "/UIArea");

        // add area to the layout
        digester.addSetNext(path + "/UIArea", "addUIArea", UIArea.class.getName());

        // create a new form
        digester.addObjectCreate(path + "/Form", FORM_CLASS_NAME);

        // set all properties for the area
        digester.addSetProperties(path + "/Form");

        // add area to the layout
        digester.addSetNext(path + "/Form", "setForm", UILayout.Form.class.getName());

		//create tag attributes and add them to the form
		digester.addObjectCreate(path + "/Form/Attribute", HTML_ATTRIBUTE_CLASS_NAME);
		digester.addSetProperties(path  + "/Form/Attribute");
		digester.addSetNext(path + "/Form/Attribute", "addAttribute", HtmlAttribute.class.getName());
        

		//create area conatiner and add them to the layout
		digester.addObjectCreate(path + "/UIAreaContainer", AREA_CONTAINER_CLASS_NAME);
		digester.addSetProperties(path  + "/UIAreaContainer");
		digester.addSetNext(path + "/UIAreaContainer", "addUIAreaContainer", UIAreaContainer.class.getName());

        
        // parsing components
        path = "UILayouts/UIComponent";

        // create a new component
        digester.addObjectCreate(path, COMPONENT_CLASS_NAME);

        // set all properties for the Component
        digester.addSetProperties(path);

        // add component to the map
        digester.addSetNext(path, "addUIComponent", UIComponent.class.getName());

        try {
            digester.parse(inputStream);
        }
        catch (Exception ex) {
            String errMsg = "Error reading configuration information" + CRLF + ex.toString();
            log.error(LogUtil.APPS_USER_INTERFACE,"system.eai.exception", new Object[] { errMsg }, null);
            throw new GlobalUILayoutHandlerException(errMsg);
        }
    }


    /**
     * The class GlobalUILayoutHandlerException handles excpetion within the
     * global handler. <br>
     *
     * @author  SAP AG
     * @version 1.0
     */
    public class GlobalUILayoutHandlerException extends Exception {

        /**
         * Standard Constructor. <br>
         */
        public GlobalUILayoutHandlerException() {
            super();
        }


        /**
         * Standard Constructor. <br>
         * @param msg
         */
        public GlobalUILayoutHandlerException(String msg) {
            super(msg);
        }
    }
    public void destroy()
    {
		instance=null;
		isInitialized = false;		
    }


}
