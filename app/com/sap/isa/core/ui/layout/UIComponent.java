/*****************************************************************************
    Class:        UIComponent
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      22.04.2004
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $
*****************************************************************************/

package com.sap.isa.core.ui.layout;

import java.util.ArrayList;
import java.util.List;

import com.sap.isa.core.util.HttpUtil;

/**
 * The UIComoment provides the content, which should be displayed in a {@link UIArea}. <br> .
 * A component includes either directly a JSP include or an action which provide all the 
 * data necessary on a JSP include.
 * The jsp which should be used page must set with UIInlude command in the struts 
 * configuration file.  <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class UIComponent extends UIBaseElement implements Cloneable {

    /**
     * Struts Action to be called. The page will be set with the UIInlude command within
     * the config.xml. <br>
     */
    protected String action = "";
    
    /**
     * Page to display. <br>
     */
    protected String page = "";

    /**
     * flag to indicate that the action is an external HTML action.
     */
    protected boolean isExternal = false;
    
    /**
     * flag to indicate that the action is an external HTML action.
     */
    protected boolean isExternalHeaderUsed = true;

    // response from an external page
    private HttpUtil.HttpResponse externalResponse;
    
    
    /**
     * Standard constructor. <br>
     *
     */
    public UIComponent() {
        super();
    }
    
    
    /**
     * Calls an external page. <br>
     * You can acces the content with the {@link #getExternalHeader} and 
     * {@link #getExternalBody} method.   
     */
    public void callExternalPage() {
    	if (isExternal) {
    		externalResponse = HttpUtil.callURL(page);
    	}
    }
    
    
    /**
     * Return the header of an external page. <br>
     * You have to call the external with {@link #callExternalHead} before. 
     */
    public List getExternalHeader() {
    	if (isExternal && isExternalHeaderUsed) {
    		return externalResponse.getHeader();
    	}
    	return new ArrayList();
    }
    

    /**
     * Return the body of an external page. <br>
     * You have to call the external with {@link #callExternalHead} before. 
     */
    public List getExternalBody() {
    	if (isExternal) {
    		return externalResponse.getBody();
    	}
    	return new ArrayList();
    }

    
    /**
     * Return the property {@link #action}. <br>
     *
     * @return {@link #action}
     */
    public String getAction() {
        return action;
    }
    
    /**
     * Set the property {@link #action}. <br>
     *
     * @param action {@link #action}
     */
    public void setAction(String action) {
        this.action = action;
    }

 
	/**
	 * Return the property {@link #isExternal}. <br> 
	 *
	 * @return Returns the {@link #isExternal}.
	 */
	public boolean isExternal() {
		return isExternal;
	}


	/**
	 * Set the property {@link #isExternal}. <br>
	 * 
	 * @param isExternal The {@link #isExternal} to set.
	 */
	public void setExternal(boolean isExternal) {
		this.isExternal = isExternal;
	}


   /**
	 * Return the property {@link #isExternalHeaderUsed}. <br> 
	 *
	 * @return Returns the {@link #isExternalHeaderUsed}.
	 */
	public boolean isExternalHeaderUsed() {
		return isExternalHeaderUsed;
	}


	/**
	 * Set the property {@link #isExternalHeaderUsed}. <br>
	 * 
	 * @param isExternalHeaderUsed The {@link #isExternalHeaderUsed} to set.
	 */
	public void setExternalHeaderUsed(boolean isExternalHeaderUsed) {
		this.isExternalHeaderUsed = isExternalHeaderUsed;
	}


/**
     * Return the property {@link #page}. <br>
     *
     * @return {@link #page}
     */
    public String getPage() {
        return page;
    }
    

	/**
     * Set the property {@link #page}. <br>
     *
     * @param page {@link #page}
     */
    public void setPage(String page) {
        this.page = page;
    }
    
    
	/**
	 * clones the component . <br>
	 *
	 * @return Copy of the component.
	 *
	 */
	public Object clone() throws CloneNotSupportedException {
        
		UIComponent component = (UIComponent)super.clone();
        
		return component;
	}
    
}
