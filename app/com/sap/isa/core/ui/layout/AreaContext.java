/*****************************************************************************
    Class:        AreaContext
    Copyright (c) 2006, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      08.03.2006
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.core.ui.layout;

import java.util.*;
import javax.servlet.http.*;

/**
 * The class AreaContext allows to store attribute which are only valid for
 * the area context. This allow to use one component in more then one area  <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class AreaContext {

	protected Map attributes =  new HashMap();
	
	/**
	 * Return the attribute with the given <code>key</code>. <br>
	 * 
	 * @param key key to find the attribute
	 * @return found attribute or <code>null</code>
	 */
	public Object getAttribute(String key) {
		return attributes.get(key);
	}

	/**
	 * Set the attribute with the given <code>key </code>  and <code>value</code> 
	 * in the context. <br>
	 * 
	 * @param key key for the attribute
	 * @param value value of the attribute
	 */
	public void setAttribute(String key, Object value) {
		attributes.put(key,value);
	}

	
    /**
     * Return the manager instance currently store in the request. <br>
     *
     * If no instance exists, the instance will be created and stored in
     * request.
     *
     * @param request current request.
     * @return current manager instance.
     */
    public static AreaContext getContextFromRequest(HttpServletRequest request) {

    	Manager manager = (Manager)request.getAttribute(Manager.MANAGER);

        if (manager == null) {
            manager = new Manager();
            request.setAttribute(Manager.MANAGER, manager);
        }
        
        return manager.getContext(request);
    }

	
	protected static class Manager {

	    /**
	     * Name of the current manager in the userSessionData.
	     */
	    public static final String MANAGER = "com.sap.isa.core.ComponentContextManager";

		protected Map contextes = new HashMap();
		
		/**
		 * Return the context for the current area. <br>
		 * 
		 * @param request request object
		 * @return context of the current area
		 */
		protected AreaContext getContext(HttpServletRequest request){
			
			String key = getCurrentAreaName(request);
			
			AreaContext context = (AreaContext)contextes.get(key);
			
			if (context== null) {
				context = new AreaContext();
				contextes.put(key,context);
			}
			
			return context;
		}

		/**
		 * Determine the name of the current active area.. <br>
		 * 
		 * @param request request object
		 * @return name of the current area or <code>""</code> if the are is not available
		 */
		protected String getCurrentAreaName(HttpServletRequest request){
			
			UIArea area = null;
	        
			UILayoutManager layoutManager = (UILayoutManager)request.getAttribute(UILayoutManager.MANAGER);
	        
			if (layoutManager != null) {
				area = layoutManager.getCurrentUIArea();
			}
			
			return area!=null?area.getName():""; 	
		}
		
	}
	
}
