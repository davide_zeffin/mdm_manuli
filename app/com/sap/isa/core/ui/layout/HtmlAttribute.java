/*
 * Created on 15.04.2005
 * Represents an html attribute associated to the body tag of the UILayout.
 *
 */
package com.sap.isa.core.ui.layout;


/**
 * Represents an html attribute associated to the body tag of the UILayout. <br>
 * @author SAP
 *
 */
public class HtmlAttribute {
	
	private String name;
	private String value;
	
	public HtmlAttribute() {
		this.name = new String();
		this.value = new String();
	}
	
	public HtmlAttribute(String name, String value) {
		this.name = name;
		this.value = value;
	}
	
	/**
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param string
	 */
	public void setName(String string) {
		name = string;
	}

	/**
	 * @param string
	 */
	public void setValue(String string) {
		value = string;
	}

}
