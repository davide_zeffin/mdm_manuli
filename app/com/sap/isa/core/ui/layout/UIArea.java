/*****************************************************************************
    Class:        UIArea
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      22.04.2004
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $
*****************************************************************************/

package com.sap.isa.core.ui.layout;

import com.sap.isa.core.logging.IsaLocation;

/**
 * The class UIArea represents a screen area on a JSP. <br>
 * The area always display a {@link UIComponent}. In the configuration a 
 * default component could be definied, which could overwritten later. 
 *
 * @author  SAP AG
 * @version 1.0
 */
public class UIArea implements Cloneable {

    final static private IsaLocation log = IsaLocation.
              getInstance(UIArea.class.getName());
              
    /**
     * Alternative key for the area. <br>
     */
    protected String altKey = "";
    
    
    /**
     * Name of the area. <br>
     */
    protected String name = "";
    
    
    /**
     * Name of the controlled ui area. <br>
     */
    protected String controlledArea = "";
    
    
    /**
     * Name of the CSS id to be used for this area. <br>
     */
    protected String cssIdName = "";
    
    /**
     * Name of the default component. <br>
     */
    protected String defaultComponent = "";
    
    
    /**
     * Reference to the currently used component. <br>
     */
    protected UIComponent component = null;
    

	/**
	 * flag if element is visible;
	 */
	protected boolean hidden = false;
    
    /**
     * Contains a resource key for a character, which  is used as a hot spot for 
     * this area. <br>
     */
    protected String hotSpot = ""; 


    /**
     * Contains a resource key to give a short explanation for the accessibility 
     * mode. <br>
     */
    protected String navigationText = ""; 

    
    
    /**
     * Standard constructor. <br>
     *
     */
    public UIArea() {
        super();

    }


    /**
     * Return the property {@link #altKey}. <br>
     *
     * @return {@link #altKey}
     */
    public String getAltKey() {
        return altKey;
    }
    
    /**
     * Set the property {@link #altKey}. <br>
     *
     * @param altKey {@link #altKey}
     */
    public void setAltKey(String altKey) {
        this.altKey = altKey;
    }
    
    /**
     * Return the property {@link #component}. <br>
     *
     * @return {@link #component}
     */
    public UIComponent getComponent() {
        return component;
    }
    
    /**
     * Set the property {@link #component}. <br>
     *
     * @param component {@link #component}
     */
    public void setComponent(UIComponent component) {
        this.component = component;
    }
    
    
    /**
     * Return the property {@link #controlledArea}. <br>
     *
     * @return {@link #controlledArea}
     */
    public String getControlledArea() {
        return controlledArea;
    }
    
    
    /**
     * Set the property {@link #controlledArea}. <br>
     *
     * @param controlledArea {@link #controlledArea}
     */
    public void setControlledArea(String controlledArea) {
        this.controlledArea = controlledArea;
    }
    
    
    /**
     * Return the property {@link #cssIdName}. <br>
     * If the cssIdName is empty the {@link #name} will be returned.
     *
     * @return {@link #cssIdName}
     */
    public String getCssIdName() {
        if (cssIdName.length()>0) {
            return cssIdName;
        }
        else {
            return name;
        }
    }
    
    /**
     * Set the property {@link #cssIdName}. <br>
     *
     * @param cssIdName {@link #cssIdName}
     */
    public void setCssIdName(String cssIdName) {
        this.cssIdName = cssIdName;
    }
    
    /**
     * Return the property {@link #defaultComponent}. <br>
     *
     * @return {@link #defaultComponent}
     */
    public String getDefaultComponent() {
        return defaultComponent;
    }
    
    /**
     * Set the property {@link #defaultComponent}. <br>
     *
     * @param defaultComponent {@link #defaultComponent}
     */
    public void setDefaultComponent(String defaultComponent) {
        if (defaultComponent!= null) {
            this.defaultComponent = defaultComponent;
        }
    }
    
	/**
	 * Returns if the area is visible. <br>
	 * 
	 * @return <code>true</code> if the element is hidden and 
	 *         <code>false</code> if the element is visible
	 */
	public boolean isHidden() {
		return hidden;
	}


	/**
	 * Sets the hidden flag. <br>
	 * 
	 * @param hidden The hidden flag to be set
	 */
	public void setHidden(boolean hidden) {
		this.hidden = hidden;
	}


    /**
     * Return the property {@link #hotSpot}. <br> 
     *
     * @return Returns the {@link #hotSpot}.
     */
    public String getHotSpot() {
        return hotSpot;
    }
    
    
    /**
     * Set the property {@link #hotSpot}. <br>
     * 
     * @param hotSpot The {@link #hotSpot} to set.
     */
    public void setHotSpot(String hotSpot) {
        this.hotSpot = hotSpot;
    }
    
    
    /**
     * Return the property {@link #name}. <br>
     *
     * @return  {@link #name}
     */
    public String getName() {
        if (name == null) {
            log.debug("UIArea name == null");
        }
        return name;
    }
    
    /**
     * Set the property {@link #name}. <br>
     *
     * @param name  {@link #name}
     */
    public void setName(String name) {
        this.name = name;
    }
    

    /**
     * Return the property {@link #navigationText}. <br> 
     *
     * @return Returns the {@link #navigationText}.
     */
    public String getNavigationText() {
        return navigationText;
    }
    
    
    /**
     * Set the property {@link #navigationText}. <br>
     * 
     * @param navigationText The {@link #navigationText} to set.
     */
    public void setNavigationText(String navigationText) {
        this.navigationText = navigationText;
    }
    

    /**
     * Clones the ui area. <br>
     * <strong>Including objects will not be cloned.<strong>
     *
     * @return copy of the object
     *
     * @see java.lang.Object#clone()
     */
    public Object clone() throws CloneNotSupportedException  {
        return super.clone();
    }
    
}
