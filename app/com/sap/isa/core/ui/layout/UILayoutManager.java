/*****************************************************************************
    Class:        UILayoutManager
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      11.02.2004
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $
*****************************************************************************/

package com.sap.isa.core.ui.layout;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.sap.isa.core.Constants;
import com.sap.isa.core.ContextConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.ui.context.ContextManager;
import com.sap.isa.core.ui.context.ReInvokeContainer;

/**
 * The class UILayoutManager hold all UI layouts and forwards currently
 * used in the request. <br>
 *
 * The UILayout Manager is stored in the request. <br>
 * Use the static method {@link #getManager} to get the current instance for
 * the request. If it is missed it will be created
 *
 * @author  SAP AG
 * @version 1.0
 */
public class UILayoutManager {
    

	private static final String FORWARD_REINVOKE_PREFIX = "frwd";
	private static final String LAYOUT_REINVOKE_PREFIX = "lay";

    
    /**
     * Reference to the IsaLocation. <br>
     *
     * @see com.sap.isa.core.logging.IsaLocation
     */
    protected static IsaLocation log = IsaLocation.
              getInstance(UILayoutManager.class.getName());

    /**
     * Name of the current manager in the userSessionData.
     */
    public static final String MANAGER = "com.sap.isa.core.UILayoutManager";
    
    private UILayout uiLayout = null;

	private UILayout storedLayout;

	private String storedForward = null;
	
	protected ContainerHandler containerHandler;
	
	
    /**
     * Return the manager instance currently store in the request. <br>
     *
     * If no instance exists, the instance will be created and stored in
     * request.
     *
     * @param request current request.
     * @return current manager instance.
     */
    public static UILayoutManager getManagerFromRequest(HttpServletRequest request) {

        GlobalUILayoutHandler layoutHandler = UILayoutInitHandler.getHandlerInstance();

        UILayoutManager manager = (UILayoutManager)request.getAttribute(MANAGER);

        if (manager == null) {
            manager = new UILayoutManager();
            request.setAttribute(MANAGER, manager);

            manager.getContextValues(request,layoutHandler);
            
            String uiAreaParameter = request.getParameter(Constants.RP_UITARGET);
            if (uiAreaParameter != null
                && uiAreaParameter.length() > 0) {
                UILayout layout = manager.getCurrentUILayout();
                UIArea area = layout.getUiArea(uiAreaParameter);
                layout.setActiveArea(area);
            }
            
			if (request.getSession() != null) {
				UserSessionData userSessionData = UserSessionData.getUserSessionData(request.getSession());
				if (userSessionData!= null) {
					manager.containerHandler = (ContainerHandler) userSessionData.getAttribute("genericForwardHandler");
					if (manager.containerHandler == null) {
						manager.createGenericForwardHandler();
						userSessionData.setAttribute("genericForwardHandler",manager.containerHandler);
					}
				}
			}

        }

        return manager;
    }

    

	protected void createGenericForwardHandler() {
		containerHandler = new ContainerHandler();
	}

    /**
     * Return the current ui layout. <br>
     *
     * @param request current request
     * @return ui layout object or null if the object couldn't be found.
     * @throws CommunicationException
     */
    public static UILayout getCurrentLayout(HttpServletRequest request) {

        UILayoutManager manager = getManagerFromRequest(request);

        return manager.getCurrentUILayout();
    }


	/**
	 * Returns the value for the RequestParameter {@link Constants#RP_UITARGET}. <br>
	 * 
	 * @param request current HTTP request.
	 * @return It returns either the name of the controlled area or the name of the
	 *          the area itself. If no area is defined an emtpy string will be returned. 
	 */
	public static String getUiTarget(HttpServletRequest request) {
		
		String ret = "";
		UIArea area = (UIArea)request.getAttribute(ContextConst.UI_CURRENT_AREA);
		if (area != null) {
			
			if (area.getControlledArea().length() >0) {
				ret = area.getControlledArea();  
			}
			else {
				ret = area.getName();
			}

			return getCurrentLayout(request).getUIAreaPosition(ret);
		}
		

		return null;		
	}


    /**
     * Return the current ui layout. <br>
     *
     * @return ui layout object or null if the object couldn't be found.
     */
    public UILayout getCurrentUILayout() {

        return uiLayout;
    }

    
    /**
     * Return the currently processed ui area. <br>
     *
     * @return ui area object or null if the object couldn't be found.
     */
    public UIArea getCurrentUIArea() {

        return uiLayout!= null?uiLayout.getCurrentArea():null;
    }
   
    
    /**
     * Store the current layout for a later use. <br>
     * The layout will also store in the ui request context.
     */
    public void storeLayout() {
        storedLayout = uiLayout;
    }

	/**
	  * Store the given layout for a later use. <br>
	  * The layout will also store in the ui request context.
	  * 
	  * @param layout layout which should be stored.
	  */
    public void storeLayout(UILayout layout) {
		storedLayout = layout;
    }


	/**
	 * Return the stored layout. <br>
	 * 
	 * @return layout which was stored in the request context for later use.
	 */
    public UILayout getStoredLayout() {
        
        UILayout layout= storedLayout;
        
        storedLayout = null;
        
        return layout;
    }


    /**
 	 * Store the given forward for a later use. <br>
	 * The forward will also store in the ui request context.
	 * 
     * @param forward forward which should be stored.
     */
    public void setForward(String forward) {
        storedForward = forward;
    }


	/**
	 * Return the stored forward. <br>
	 * 
	 * @return forward which was stored in the request context for later use.
	 */
    public String getForward() {

        String forward = storedForward;
		
		storedForward = null;

        return forward;
    }


    /**
     * Set the property {@link #uiLayout}. <br>
     *
     * @param uiLayout see {@link #uiLayout}.
     */
    public void setUiLayout(UILayout uiLayout) {
        this.uiLayout = uiLayout;
    }


    /**
     * Return the key of the current layout. <br>
     * 
     * @return key of the current layout.
     */
    public String getCurrentLayoutKey() {

        return GlobalUILayoutHandler.getInstance().getLayoutKey(uiLayout);
    }

    /**
     * This method adjust the layout manager from the current context values . <br>
     * 
     * @param request current request.
     */
    public void getContextValues(HttpServletRequest request, 
                                  GlobalUILayoutHandler layoutHandler) {
        
        ContextManager contextManager = ContextManager.getManagerFromRequest(request);
		String layoutKey = contextManager.getContextValue(Constants.CV_LAYOUT_NAME);

		if (layoutKey != null) {
	        UILayout layout = layoutHandler.getLayoutFromKey(layoutKey);
	        setUiLayout(layout);

			String uiAreaParameter = contextManager.getContextValue(Constants.CV_UITARGET);
			if (uiAreaParameter != null
				&& uiAreaParameter.length() > 0) {
				UIArea area = layout.getUIAreaFromPosition(uiAreaParameter);
				layout.setActiveArea(area);
			}
		}    

		List list = contextManager.getContextValues(Constants.CV_LAYOUT_MAP);
		if (list != null) {
		    Iterator layoutStack = list.iterator();
	
	        while (layoutStack.hasNext()) {
	            layoutKey = (String)layoutStack.next();
	            UILayout layout =
	                layoutHandler.getLayoutFromKey(layoutKey);
	            storeLayout(layout);
	        }
    	}

		list = contextManager.getContextValues(Constants.CV_FORWARD_MAP);
		if (list != null) {
		    Iterator forwardStack = list.iterator();

	        while (forwardStack.hasNext()) {
	            String forward = (String)forwardStack.next();
	            setForward(forward);
	        }
		}
    }


    /**
     * This method set all context values which are necessary in the given
     * request. <br>
     * 
     * @param request current request.
     */
    public void setContextValues(HttpServletRequest request) {
        
        ContextManager contextManager = ContextManager.getManagerFromRequest(request);
        
		if (getCurrentUILayout() != null) {
		    // setting layout key	
			String layoutKey = getCurrentLayoutKey();
			contextManager.addContextValue(Constants.CV_LAYOUT_NAME,layoutKey);
		}
			
		// adding layout map
		contextManager.resetContextValue(Constants.CV_LAYOUT_MAP);
		if (storedLayout != null) {
			String layoutKey = GlobalUILayoutHandler.getInstance().getLayoutKey(storedLayout);
			contextManager.addContextValue(Constants.CV_LAYOUT_MAP, layoutKey);
		}

        // adding forward map
		contextManager.resetContextValue(Constants.CV_FORWARD_MAP);
		if (storedForward != null) {
			contextManager.addContextValue(Constants.CV_FORWARD_MAP,storedForward);
		}
    }


	/**
	 * This method adds a reinvoke container to the global forward stack and 
	 * generate a key for this container. The key will store as a forward  
	 * name.
	 * @param reInvokeContainer reinvoke container
	 * @return generate logical key for the container
	 */
	public String addForwardContainer(ReInvokeContainer reInvokeContainer) {
		
		String forwardName = containerHandler.addForwardContainer(FORWARD_REINVOKE_PREFIX, reInvokeContainer);
		setForward(forwardName);
		
		return forwardName;
	}


	/**
	 * This method adds a reinvoke container with the given key to the container handler. <br>  
	 * @param reInvokeContainer reinvoke container
	 * @param layoutKey key of the layout
	 */
	public void addLayoutContextContainer(ReInvokeContainer reInvokeContainer, String layoutKey) {
		
		containerHandler.addContainer(reInvokeContainer, getLayoutContextContainerKey(layoutKey));
	}


	/**
     * Return the key which will be used for the context conatiner for the
     * given layout key. <br>
     * 
     * @param layoutKey
     * @return key used to store the container.
     */
    public String getLayoutContextContainerKey(String layoutKey) {
		return  LAYOUT_REINVOKE_PREFIX + layoutKey;
	}
	
	/**
	 * Return a {@link ReInvokeContainer} from the container handler with the 
	 * key generate by the method {@link #addForwardContainer(ReInvokeContainer)} . <br>
	 * The container will be delete then form the stack.
	 * 
	 * @param forwardName
	 * @return ReInvokeContainer
	 */
	public ReInvokeContainer getReInvokeContainer(String forwardName) {
		return containerHandler.getReInvokeContainer(forwardName);
	}	

	
	/**
	 * Return if there exist a reinvoke container for the given key. <br>
	 * 
	 * @param forwardName
	 * @return <code>true</code> if the container exist.
	 */
	public boolean hasReInVokeContainer (String forwardName) {
		return forwardName.indexOf(FORWARD_REINVOKE_PREFIX) == 0;
	}



	// handles a map with reinvoke container.
	protected class ContainerHandler {


		private Map reinvokeContainer = new HashMap();
	
		private int fowardKeyIndex = 0;
		
		private boolean holdContainerContent = true; 	


		public String addForwardContainer(String prefix, ReInvokeContainer container) {
		
			String forwardName = null;
		
			synchronized (this) {
				forwardName = prefix + fowardKeyIndex;
				fowardKeyIndex++;
			}
		
			reinvokeContainer.put(forwardName, container);
		
			return forwardName;
		}


		public void addContainer(ReInvokeContainer container, String key) {
			reinvokeContainer.put(key, container);
		}


		/**
		 * Return the container and delete it from the map. <br>
		 * 
		 * @param key
		 * @return
		 */
		public ReInvokeContainer getReInvokeContainer(String key) {
		
			ReInvokeContainer container =(ReInvokeContainer)reinvokeContainer.get(key);

			if (container != null && !holdContainerContent) {
				reinvokeContainer.remove(key);
			}
			
			return container;
		}	


		/**
		 * Return if the the container for the given key exists. <br>
		 * 
		 * @param key
		 * @return
		 */
		public boolean checkReInvokeContainer(String key) {
			return reinvokeContainer.get(key) != null;
		}

        /**
         * Return Set the property holdContainerContent. <br>
         * 
         * @return
         */
        public boolean isHoldContainerContent() {
            return holdContainerContent;
        }

        /**
         * Return Set the property holdContainerContent. <br>
         * 
         * @param holdContainerContent
         */
        public void setHoldContainerContent(boolean holdContainerContent) {
            this.holdContainerContent = holdContainerContent;
        }

	}

}

