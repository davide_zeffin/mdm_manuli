/*****************************************************************************
    Class:        UIBaseElement
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      22.04.2004
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $
*****************************************************************************/

package com.sap.isa.core.ui.layout;


/**
 * The class UIBaseElement is hold the common parts of UILayout and UIComponent. <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class UIBaseElement {


	/**
	  * Name of the layout. <br>
	  */
	 protected String name = "";
    
 
	/**
	 * Alternative key for the element. <br>
	 */
	protected String altKey = "";

	/**
	 * Name of a JSP include, which includes only java script coding. <br>
	 */
	protected String jsInclude = "";
    
    
	/**
	 * Name of a java script function which should  be called for the Onload event
	 * of the complete HTML page. <br>
	 */
	protected String jsOnload = "";
 

	/**
	 * Title to use. <br>
	 */
	protected String title = "";
    

	/**
	 * Argument for the title. <br>
	 * The argument describes the name of a value of a request parameter.
	 */
	protected String titleArg0 = "";
    

	/**
	 * Argument for the title. <br>
	 * The argument describes the name of a value of a request parameter.
	 */
	protected String titleArg1 = "";

	/**
	 * Argument for the title. <br>
	 * The argument describes the name of a value of a request parameter.
	 */
	protected String titleArg2 = "";

	/**
	 * Argument for the title. <br>
	 * The argument describes the name of a value of a request parameter.
	 */
	protected String titleArg3 = "";

    
    /**
     * Standard constructor. <br>
     *
     */
    public UIBaseElement() {
        super();
    }


    
    /**
     * Return the property {@link #altKey}. <br>
     *
     * @return {@link #altKey}
     */
    public String getAltKey() {
        return altKey;
    }
    
    
    /**
     * Set the property {@link #altKey}. <br>
     *
     * @param altKey {@link #altKey}
     */
    public void setAltKey(String altKey) {
        this.altKey = altKey;
    }
    
   
	/**
	 * Return the property {@link #jsInclude}. <br> 
	 *
	 * @return Returns the {@link #jsInclude}.
	 */
	public String getJsInclude() {
		return jsInclude;
	}
    
    
	/**
	 * Set the property {@link #jsInclude}. <br>
	 * 
	 * @param jsInclude The {@link #jsInclude} to set.
	 */
	public void setJsInclude(String jsInclude) {
		this.jsInclude = jsInclude;
	}
    
    
	/**
	 * Return the property {@link #jsOnload}. <br> 
	 *
	 * @return Returns the {@link #jsOnload}.
	 */
	public String getJsOnload() {
		return jsOnload;
	}
    
    
	/**
	 * Set the property {@link #jsOnload}. <br>
	 * 
	 * @param jsOnload The {@link #jsOnload} to set.
	 */
	public void setJsOnload(String jsOnload) {
		this.jsOnload = jsOnload;
	}
    
        
    /**
     * Return the property {@link #name}. <br>
     *
     * @return {@link #name}
     */
    public String getName() {
        return name;
    }
    
    
    /**
     * Set the property {@link #name}. <br>
     *
     * @param name {@link #name}
     */
    public void setName(String name) {
        this.name = name;
    }
    

    /**
     * Return the property {@link #title}. <br> 
     *
     * @return Returns the {@link #title}.
     */
    public String getTitle() {
        return title;
    }
    
    
    /**
     * Set the property {@link #title}. <br>
     * 
     * @param title The {@link #title} to set.
     */
    public void setTitle(String title) {
        this.title = title;
    }
        

    /**
     * Return the property {@link #titleArg0}. <br>
     * 
     * @return {@link #titleArg0}
     */
    public String getTitleArg0() {
        return titleArg0;
    }

    /**
     * Return Set the property {@link #titleArg0}. <br>
     * 
     * @param titleArg0 {@link #titleArg0}
     */
    public void setTitleArg0(String titleArg0) {
        this.titleArg0 = titleArg0;
    }

	/**
	 * Return the property {@link #titleArg1}. <br>
	 * 
	 * @return {@link #titleArg1}
	 */
	public String getTitleArg1() {
		return titleArg1;
	}

	/**
	 * Return Set the property {@link #titleArg1}. <br>
	 * 
	 * @param titleArg1 {@link #titleArg1}
	 */
	public void setTitleArg1(String titleArg1) {
		this.titleArg1 = titleArg1;
	}

	/**
	 * Return the property {@link #titleArg2}. <br>
	 * 
	 * @return {@link #titleArg2}
	 */
	public String getTitleArg2() {
		return titleArg2;
	}

	/**
	 * Return Set the property {@link #titleArg2}. <br>
	 * 
	 * @param titleArg2 {@link #titleArg2}
	 */
	public void setTitleArg2(String titleArg2) {
		this.titleArg2 = titleArg2;
	}

	/**
	 * Return the property {@link #titleArg3}. <br>
	 * 
	 * @return {@link #titleArg3}
	 */
	public String getTitleArg3() {
		return titleArg3;
	}

	/**
	 * Return Set the property {@link #titleArg3}. <br>
	 * 
	 * @param titleArg3 {@link #titleArg3}
	 */
	public void setTitleArg3(String titleArg3) {
		this.titleArg3 = titleArg3;
	}

}
