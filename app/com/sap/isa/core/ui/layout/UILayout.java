/*****************************************************************************
    Class:        UILayout
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      22.04.2004
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $
*****************************************************************************/

package com.sap.isa.core.ui.layout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.sap.isa.core.Iterable;
import com.sap.isa.core.logging.IsaLocation;

/**
 * The class UILayout hold the information over a page layout. <br>
 * A UI layout consist of bunch of {@link com.sap.isa.core.ui.layout.UIArea} 
 * elements.
 * Additionally the object includes arbitrary HTML attributes for the body and 
 * informations for accessiblilty.</br> 
 * The object can include also a HTML from. 
 * See {@link com.sap.isa.core.ui.layout.UILayout.Form} for deatils.
 *
 * @author  SAP AG
 * @version 1.0
 */
public class UILayout extends UIBaseElement implements Iterable, Cloneable {

	protected static IsaLocation log = IsaLocation.
		   getInstance(UILayout.class.getName());

	/**
	 * Action to control the layout or to read data which is global required. <br>
	 * <strong>Note: The action which is finally called must extend the ControlLayoutAction 
	 * class.</strong>
	 * 
	 */
	protected String action = "";

    
    /**
     * The active area describes the area which have initate the current request. <br>
     */
    protected UIArea activeArea = null;
    
    
    /**
     * Path t the corresponding jsp. <br>
     */
    protected String path = "";
    
	/**
	 * Name of the css class to use in the body. <br>
	 */
	protected String cssClassName = "";


	/**
	 * <p>Name of stylesheet group to use in the stylesheet tag . </p>
	 */
	protected String styleGroupName = "";

    /**
     * Allows to define a workarea, which provide the resource key for title for 
     * the HTML page. The title is taken from the current component of this area.
     * see {@link UIComponent}.
     */
    protected String titleArea = "";
    
    /**
     * List with the included areas in this layout.
     */
    protected List areaList = new ArrayList(5);
    
    /**
     * List with the html attributes attached to the body of the layout.
     */
    protected List bodyAttributeList = new ArrayList(4);
    

	/**
	 * List with the included areas in this layout.
	 */
	protected List containerList = new ArrayList(5);


	/**
	 * map with the container a list fo containers.
	 */
	protected Map containerStartsWithAreas = new HashMap();


	/**
	 * map with the container .
	 */
	protected Map containerEndsWithAreas = new HashMap();
	
    
    /**
     * HTML form infomration for a global form. <br>
     */
    protected Form form;

    
    /**
     * Contains a resource key for a character, which  is used as a hot spot for 
     * this layout. <br>
     */
    protected String hotSpot = ""; 

    
    /**
     * Contains a resource key for a character, which  is used as a hot spot for 
     * the bottom of this layout. <br>
     */
    protected String hotSpotBottom = ""; 

    
    /**
     * Contains a resource key to give a short explanation for the accessibility 
     * mode. <br>
     */
    protected String navigationText = ""; 


    /**
     * Contains a resource key to give a short explanation for the accessibility 
     * mode. <br>
     */
    protected String navigationTextBottom = ""; 
    
    
    // The iterator is used by start iteration and getAreaIterator
    private Iterator areaActionIterator = null;
    
    /**
     * The current area store the area is currently processed. <br>
     */
    protected UIArea currentArea = null;
    
    /**
     * <p>The attribute <code>isXHTMLSupported</code> controls is the layout can be renderer as XHTML.</p>
     */
    protected boolean isXHTMLSupported = false;
    
    /**
     * Standard constructor. <br>
     *
     */
    public UILayout() {
        super();
    }
    

	/**
	 * Return Set the property {@link #action}. <br>
	 * 
	 * @return {@link #action}
	 */
	public String getAction() {
		return action;
	}


	/**
	 * Return Set the property {@link #action}. <br>
	 * 
	 * @param action {@link #action}
	 */
	public void setAction(String action) {
		this.action = action;
	}

    
    /**
     * Return the property {@link #activeArea}. <br>
     *
     * @return {@link #activeArea}
     */
    public UIArea getActiveArea() {
        return activeArea;
    }
    
    /**
     * Set the property {@link #activeArea}. <br>
     *
     * @param activeArea {@link #activeArea}
     */
    public void setActiveArea(UIArea activeArea) {
        this.activeArea = activeArea;
    }

 
	/**
	 * Return Set the property {@link #cssClassName}. <br>
	 * 
	 * @return {@link #cssClassName}
	 */
	public String getCssClassName() {
		return cssClassName;
	}

	/**
	 * Return Set the property {@link #cssClassName}. <br>
	 * 
	 * @param cssClassName {@link #cssClassName}
	 */
	public void setCssClassName(String cssClassName) {
		this.cssClassName = cssClassName;
	}
   
    /**
     * Return the property {@link #form}. <br> 
     *
     * @return Returns the {@link #form}.
     */
    public Form getForm() {
        return form;
    }
    
    
    /**
     * Set the property {@link #form}. <br>
     * 
     * @param form The {@link #form} to set.
     */
    public void setForm(Form form) {
        this.form = form;
    }
    

    /**
     * Return the property {@link #hotSpot}. <br> 
     *
     * @return Returns the {@link #hotSpot}.
     */
    public String getHotSpot() {
        return hotSpot;
    }
    
    
    /**
     * Set the property {@link #hotSpot}. <br>
     * 
     * @param hotSpot The {@link #hotSpot} to set.
     */
    public void setHotSpot(String hotSpot) {
        this.hotSpot = hotSpot;
    }
        
    
    /**
     * Return the property {@link #hotSpotBottom}. <br> 
     *
     * @return Returns the {@link #hotSpotBottom}.
     */
    public String getHotSpotBottom() {
        return hotSpotBottom;
    }
    
    
    /**
     * Set the property {@link #hotSpotBottom}. <br>
     * 
     * @param hotSpotBottom The {@link #hotSpotBottom} to set.
     */
    public void setHotSpotBottom(String hotSpotBottom) {
        this.hotSpotBottom = hotSpotBottom;
    }
    
    
    /**
     * Return the property {@link #navigationText}. <br> 
     *
     * @return Returns the {@link #navigationText}.
     */
    public String getNavigationText() {
        return navigationText;
    }
    
    
    /**
     * Set the property {@link #navigationText}. <br>
     * 
     * @param navigationText The {@link #navigationText} to set.
     */
    public void setNavigationText(String navigationText) {
        this.navigationText = navigationText;
    }
    
  
    
    /**
     * Return the property {@link #navigationTextBottom}. <br> 
     *
     * @return Returns the {@link #navigationTextBottom}.
     */
    public String getNavigationTextBottom() {
        return navigationTextBottom;
    }
    
    
    /**
     * Set the property {@link #navigationTextBottom}. <br>
     * 
     * @param navigationTextBottom The {@link #navigationTextBottom} to set.
     */
    public void setNavigationTextBottom(String navigationTextBottom) {
        this.navigationTextBottom = navigationTextBottom;
    }
    
    
    /**
	 * <p>Return the property {@link #styleGroupName}. </p> 
	 *
	 * @return Returns the {@link #styleGroupName}.
	 */
	public String getStyleGroupName() {
		return styleGroupName;
	}


	/**
	 * <p>Set the property {@link #styleGroupName}. </p>
	 * 
	 * @param styleGroupName The {@link #styleGroupName} to set.
	 */
	public void setStyleGroupName(String styleGroupName) {
		this.styleGroupName = styleGroupName;
	}


	/**
     * Add a UIArea object to the {@link #areaList}.  <br>
     *
     * @param uiArea
     */
    public void addUIArea(UIArea uiArea) {
        String index = "" + areaList.size();
        areaList.add(uiArea);
        uiArea.setAltKey(index);
    }
    

	/**
	 * Add a UIAreaContainer object to the {@link #containerList}.  <br>
	 *
	 * @param container conatiner to be added.
	 */
	public void addUIAreaContainer(UIAreaContainer container) {
		containerList.add(container);
		List cssNames = (List)containerStartsWithAreas.get(container.getStartArea());
		if (cssNames == null) {
		    cssNames = new ArrayList(3);
		    containerStartsWithAreas.put(container.getStartArea(),cssNames);
		}
		
		cssNames.add(container.getCssIdName());

		cssNames = (List)containerEndsWithAreas.get(container.getEndArea());
		if (cssNames == null) {
		    cssNames = new ArrayList(3);
		    containerEndsWithAreas.put(container.getEndArea(),cssNames);
		}
		
		cssNames.add(container.getCssIdName());

	}
    

	/**
     * Return a iterator over all css container which starts with the given area. <br>
     * 
     * @param areaName name of the area
     * @return iterator over the css id names of the found container
     */
	public Iterator getContainerStartsWithArea(String areaName) {
       
       List cssNames = (List)containerStartsWithAreas.get(areaName);
       if (cssNames == null) {
           cssNames = new ArrayList(1);
       }
       
       return cssNames.iterator();
   	}


	/**
     * Return a iterator over all css container which starts with the given area. <br>
     * 
     * @param areaName name of the area
     * @return iterator over the css id names of the found container
     */
	public Iterator getContainerEndsWithArea(String areaName) {
	       
	    List cssNames = (List)containerEndsWithAreas.get(areaName);
	    if (cssNames == null) {
	        cssNames = new ArrayList(1);
	    }
	       
	    return cssNames.iterator();
	}

	
    /**
     * Return the UIArea with the given name. <br>
     *
     * @param name name of the UI area
     * @return UIArea or <code>null</code> if the area couldn't be found.
     */
    public UIArea getUiArea(String name) {
        
        for (int i = 0; i < areaList.size(); i++) {
            UIArea area = (UIArea)areaList.get(i);
            if (area.getName().equals(name)) {
                return area;
            }
        }
        
        return null;
    }
   
   
   	/**
   	 * Add an HTML attribute, which will be used in the HTML body tag. <br>
   	 * 
   	 * @param bodyAttribute name value pair with the HTML attribute
   	 */ 
    public void addBodyAttribute(HtmlAttribute bodyAttribute) {
    	bodyAttributeList.add(bodyAttribute);
    }
    

    /**
     * Return a list with HTML attribute, which should be used in the body text. <br>
     * 
     * @return list with {@link HtmlAttribute}
     */
    public List getBodyAttributes() {
    	return bodyAttributeList;
    }
    

	/**
	 * Return the position number of the area in the layout. <br>
	 * 
	 * @param areaName name of the area.
	 * @return area position
	 * 
	 */
	public String getUIAreaPosition(String areaName) {

		for (int i = 0; i < areaList.size(); i++) {
			if (((UIArea) areaList.get(i)).getName().equals(areaName)) {
				return "" +i;
			}
            
		}    	
		return "";
	}


	/**
	 * Return the position number of the area in the layout. <br>
	 * 
	 * @param areaPosition name of the area.
	 * @return area
	 *  
	 */
	public UIArea getUIAreaFromPosition(String areaPosition) {

		try {
			int pos = Integer.parseInt(areaPosition);
		
			return (UIArea) areaList.get(pos);
		}
		catch (Exception e) {
			log.debug(e.getMessage());
		}

		return null;		 
	}

    
    /**
     * Return the property {@link #path}. <br>
     *
     * @return Returns {@link #path}.
     */
    public String getPath() {
        return path;
    }
    
    /**
     * Set the property {@link #path}. <br>
     *
     * @param path see {@link #path}.
     */
    public void setPath(String path) {
        this.path = path;
    }
    
    /**
     * Return the currtly used component in the area with the given name. <br>
     *
     * @param name name of the UI area
     * @return UIComponent or <code>null</code> if the area couldn't be found.
     */
    public UIComponent getCurrentComponentForArea(String name) {
        
        UIArea area = getUiArea(name);
        if (area != null) {
            return area.getComponent();
        }
        
        return null;
    }

    
    /**
     * Return the property {@link #titleArea}. <br> 
     *
     * @return Returns the {@link #titleArea}.
     */
    public String getTitleArea() {
        return titleArea;
    }
    
    
    /**
     * Set the property {@link #titleArea}. <br>
     * 
     * @param titleArea The {@link #titleArea} to set.
     */
    public void setTitleArea(String titleArea) {
        this.titleArea = titleArea;
    }    

	/**
	 * <p>Return the property {@link #isXHTMLSupported}. </p> 
	 *
	 * @return Returns the {@link #isXHTMLSupported}.
	 */
	public boolean isXHTMLSupported() {
		return isXHTMLSupported;
	}


	/**
	 * <p>Set the property {@link #isXHTMLSupported}. </p>
	 * 
	 * @param isXHTMLSupported The {@link #isXHTMLSupported} to set.
	 */
	public void setXHTMLSupported(boolean isXHTMLSupported) {
		this.isXHTMLSupported = isXHTMLSupported;
	}


	/**
	 * Iterator over all UIAreas.
	 * 
	 * @return iterator over UIAreas. 
	 * 
	 */    
    public Iterator iterator() {
        return areaList.iterator();
    }
    
    /**
     * Start the iteration over all component actions which have to performed for
     * the current UI layout. <br>
     * Use {@link #getcomponentActionIterator} to get the current iterator afterwards. <br>
     * <strong>Note:</strong> Also the current area is set accordingly.
     */
    public void startComponentActionIteration() {
        ArrayList areaActionList = new  ArrayList(areaList.size());
        
        Iterator iter = iterator();
        while (iter.hasNext()) {
            UIArea area = (UIArea)iter.next();
            
            UIComponent component = area.getComponent();
            
            if (component.getAction().length()>0) {
                areaActionList.add(area);
            }
            
        }
        areaActionIterator = areaActionList.iterator();
    }

    
    /**
     * Call all external components for the current UI layout. <br>
     */
    public void processExternalComponents() {

        Iterator iter = iterator();
        while (iter.hasNext()) {
            UIArea area = (UIArea)iter.next();
            UIComponent component = area.getComponent();
            
            if (component.isExternal()) {
                component.callExternalPage();
            }
        }
    }

    
    /**
     * Set the property {@link #currentArea}. <br>
     * 
     * @param currentArea currently processed area. 
     */
    public void setCurrentArea (UIArea currentArea) {
    	this.currentArea = currentArea;
    }

    
    /**
     * Return the currently processed area see {@link #currentArea}. <br>
     *
     * @return current area
     */
    public UIArea getCurrentArea() {
        return currentArea;
    }
    
    
    /**
     * Return the current component. <br>
     *
     * @return current component
     */
    public UIComponent getCurrentComponent() {
        return currentArea.getComponent();
    }

    
    /**
     * Return if a further component available. <br>
     *
     * @return <code>true</code> if a further component exist. 
     */    
    public boolean hasNextComponent() {
        return areaActionIterator!=null?areaActionIterator.hasNext():false;
    }
    
    
    /**
     * Return the next component of the component iterator. <br>
     *
     * @return next component 
     */    
    public UIComponent nextComponent() {
        currentArea = (UIArea) areaActionIterator.next();
        
        return getCurrentComponent();
    }
 

    /**
     * clones the layout and the included area list. <br>
     *
     * @return Copy of the layout.
     *
     */
    public Object clone() throws CloneNotSupportedException {
        
       	//D040230 : Correction from Wolfgang Sattler
       	//for message 2506995 2006 (note 977487) 
		UILayout newLayout = (UILayout)super.clone();
	    newLayout.areaList = new ArrayList(areaList.size());
        
		for (int i = 0; i < areaList.size(); i++) {
			UIArea area = (UIArea)areaList.get(i);
			newLayout.addUIArea((UIArea)area.clone());
		}
        
		if (form != null) {
			newLayout.form = (Form) form.clone();
		}
        
		return newLayout;
    }
    
    
    /**
     * The class Form represent a gloabl HTML form which belongs to the 
     * layout. <br>
     *
     * @author  SAP AG
     * @version 1.0
     */
	static public class Form implements Cloneable {
		
		/**
		 * Id of the HTML form. <br>
		 */
		protected String id = "";
		

		/**
		 * name of the HTML form. <br>
		 */
		protected String name = "";
		
		
		/**
		 * action of the HTML form. <br>
		 */
		protected String action = "";
		

		/**
		 * method of the HTML form. <br>
		 */
		protected String method = "";


		/**
		 * Switch if the form uses the global dispatcher action. <br>
		 */
		protected boolean dispatcherAvailable;
		    		

		/**
		 * List with the html attributes attached to the body of the layout.
		 */
		protected List attributeList = new ArrayList(4);
	    
        /**
         * Return the property {@link #action}. <br> 
         *
         * @return Returns the {@link #action}.
         */
        public String getAction() {
            return action;
        }
  
        
        /**
         * Set the property {@link #action}. <br>
         * 
         * @param action The {@link #action} to set.
         */
        public void setAction(String action) {
            this.action = action;
        }
        

		/**
		 * Add an HTML attribute. <br>
		 * 
		 * @param attribute name value pair with the HTML attribute
		 */ 
		public void addAttribute(HtmlAttribute attribute) {
			attributeList.add(attribute);
		}
    

		/**
		 * Return a list with HTML attribute. <br>
		 * 
		 * @return list with {@link HtmlAttribute}
		 */
		public List getAttributes() {
			return attributeList;
		}

		/**
		 * Return the property {@link #dispatcherAvailable}. <br>
		 * 
		 * @return {@link #dispatcherAvailable}
		 */
		public boolean isDispatcherAvailable() {
			return dispatcherAvailable;
		}


		/**
		 * Set the property {@link #dispatcherAvailable}. <br>
		 * 
		 * @param dispatcherAvailable {@link #dispatcherAvailable}
		 */
		public void setDispatcherAvailable(boolean dispatcherAvailable) {
			this.dispatcherAvailable = dispatcherAvailable;
		}

        
        /**
         * Return the property {@link #id}. <br> 
         *
         * @return Returns the {@link #id}.
         */
        public String getId() {
            return id;
        }
        
        
        /**
         * Set the property {@link #id}. <br>
         * 
         * @param id The {@link #id} to set.
         */
        public void setId(String id) {
            this.id = id;
        }
        
        
        /**
         * Return the property {@link #method}. <br> 
         *
         * @return Returns the {@link #method}.
         */
        public String getMethod() {
            return method;
        }
        
        
        /**
         * Set the property {@link #method}. <br>
         * 
         * @param method The {@link #method} to set.
         */
        public void setMethod(String method) {
            this.method = method;
        }
        
        
        /**
         * Return the property {@link #name}. <br> 
         *
         * @return Returns the {@link #name}.
         */
        public String getName() {
            return name;
        }
        
        
        /**
         * Set the property {@link #name}. <br>
         * 
         * @param name The {@link #name} to set.
         */
        public void setName(String name) {
            this.name = name;
        }


        /**
         * clones the HTML form. <br>
         *
         * @return Copy of the form object.
         *
         */
        public Object clone() throws CloneNotSupportedException {
            return super.clone();
        }
        
	}
	

}
