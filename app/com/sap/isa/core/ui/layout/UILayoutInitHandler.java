/*****************************************************************************
    Class:        UILayoutInitHandler
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      22.04.2004
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $
*****************************************************************************/

package com.sap.isa.core.ui.layout;

import java.util.Properties;

import com.sap.isa.core.FrameworkConfigManager;
import com.sap.isa.core.init.Initializable;
import com.sap.isa.core.init.InitializationEnvironment;
import com.sap.isa.core.init.InitializeException;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.ui.layout.GlobalUILayoutHandler.GlobalUILayoutHandlerException;
import com.sap.isa.core.xcm.ConfigContainer;

/**
 *
 * The UILayoutInitHandler initialize the ui layout framework. <br>
 * The handler build up the {@link com.sap.isa.core.ui.layout.GlobalUILayoutHandler}.  
 *
 * @author  SAP AG
 * @version 1.0
 */
public class UILayoutInitHandler implements Initializable {

    /**
     * Reference to the IsaLocation.
     *
     * @see com.sap.isa.core.logging.IsaLocation
     */
    protected static IsaLocation log = IsaLocation.
              getInstance(UILayoutInitHandler.class.getName());


    /**
     * Return the GlobalUILayoutHandler instance, which is to be used. <br>
     *
     * @return
     */
    public static GlobalUILayoutHandler getHandlerInstance() {

        return GlobalUILayoutHandler.getInstance();
    }


    /**
     * Standard constructor
     */
    public UILayoutInitHandler() {
        super();
    }


    /**
     * Initialize the GlobalUILayoutHandler . <br>
     *
     * @param env
     * @param props
     * @throws InitializeException
     *
     * @see com.sap.isa.core.init.Initializable#initialize(com.sap.isa.core.init.InitializationEnvironment, java.util.Properties)
     */
    public void initialize(InitializationEnvironment env, Properties props)
                throws InitializeException {

        ConfigContainer configContainer =
            FrameworkConfigManager.XCM.getXCMScenarioConfig(
                FrameworkConfigManager.NAME_BOOTSTRAP_SCENARIO);

        if (configContainer != null) {

            GlobalUILayoutHandler layoutHandler = getHandlerInstance();

            try {
                layoutHandler.readLayoutData(configContainer);
            }
            catch (GlobalUILayoutHandlerException exception) {
                log.error(LogUtil.APPS_USER_INTERFACE,"system.exception",exception);
                throw new InitializeException(exception.getMessage());
            }
        }
        else {
            //TODO create message key
            String msg =
                "Scenario [FrameworkConfigManager.NAME_BOOTSTRAP_SCENARIO] missing";
            log.error(LogUtil.APPS_USER_INTERFACE,
                "Scenario [FrameworkConfigManager.NAME_BOOTSTRAP_SCENARIO] missing");
            throw new InitializeException(msg);
        }
    }


    /**
     * Terminate the Handler . <br>
     *
     * @see com.sap.isa.core.init.Initializable#terminate()
     */
    public void terminate() {
        GlobalUILayoutHandler layoutHandler = getHandlerInstance();
        layoutHandler.clearLayoutData();
        layoutHandler.destroy();
    }

}
