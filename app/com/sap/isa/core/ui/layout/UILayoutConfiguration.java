/*****************************************************************************
    Class:        UILayoutConfiguration
    Copyright (c) 2005, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      18.02.2005
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.core.ui.layout;

/**
 * The class UILayoutConfiguration is the global configuration for the layout 
 * framework . <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class UILayoutConfiguration {
    
    /**
     * Global title for the generic layout. <br>
     * Will show always additional to other titles.
     */
    protected String title = "";
    
    /**
     * Prefix for the alt key generation. <br>
     */
    protected String altKeyPrefix = "";
    
    /**
     * The version will be add to the generate layout key to detect 
     * layout changes and prevent errors in the bookmark handling
     */
    protected String version = "";
    

    /**
     * Return the property {@link #altKeyPrefix}. <br> 
     *
     * @return Returns the {@link #altKeyPrefix}.
     */
    public String getAltKeyPrefix() {
        return altKeyPrefix;
    }
    
    /**
     * Set the property {@link #altKeyPrefix}. <br>
     * 
     * @param altKeyPrefix The {@link #altKeyPrefix} to set.
     */
    public void setAltKeyPrefix(String altKeyPrefix) {
        this.altKeyPrefix = altKeyPrefix;
    }

    
    /**
     * Return the property {@link #title}. <br> 
     *
     * @return Returns the {@link #title}.
     */
    public String getTitle() {
        return title;
    }
    
    
    /**
     * Set the property {@link #title}. <br>
     * 
     * @param title The {@link #title} to set.
     */
    public void setTitle(String title) {
        this.title = title;
    }


    /**
     * Return the property {@link #version}. <br> 
     *
     * @return Returns the {@link #version}.
     */
    public String getVersion() {
        return version;
    }

    
    /**
     * Set the property {@link #version}. <br>
     * 
     * @param version The {@link #version} to set.
     */
    public void setVersion(String version) {
        this.version = version;
    }

}
