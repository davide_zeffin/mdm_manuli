package com.sap.isa.core.ui;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Properties;

import com.sap.isa.core.init.Initializable;
import com.sap.isa.core.init.InitializationEnvironment;
import com.sap.isa.core.init.InitializeException;

/**
 * Title:        ESales_BASE
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public class InitStyleHandler implements Initializable {

  private static final String STYLESHEETPATHKEY = "StylesheetPath";

  private static Collection stylesList = new ArrayList();

  public InitStyleHandler() {
  }

  public void initialize(InitializationEnvironment env, Properties props) throws InitializeException {

      try  {
          if(props.getProperty(STYLESHEETPATHKEY) != null)
              StyleManager.setMimesPath(props.getProperty(STYLESHEETPATHKEY));

          Enumeration lists = props.keys();
          String keyId = null;
          while(lists.hasMoreElements())  {
              keyId = (String)lists.nextElement();
              if(keyId.equals(STYLESHEETPATHKEY))  {
                  continue;
              }
              StyleManager.addResource(props.getProperty(keyId));
          }
      }  catch(Exception exc)  {
          throw new InitializeException("Unable to initialize the style file list");
      }
  }

  public void terminate() {
      stylesList.clear();
      stylesList = null;
  }

  public static Collection getStyleList()  {
      return stylesList;
  }
}
