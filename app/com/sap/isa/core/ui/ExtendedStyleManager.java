/*****************************************************************************
	Class:        ExtendedStyleManager
	Copyright (c) 2004, SAPMarkets Europe GmbH, Germany, All rights reserved.
	Author:       SAP
	Created:      March 2004
	Version:      1.0

	$Revision: #1 $
	$Date: 2001/08/01 $
*****************************************************************************/
package com.sap.isa.core.ui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

import com.sap.isa.core.Constants;
import com.sap.isa.core.SharedConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.WebUtil;


/**
 * The class ExtendedStyleManager manages all stylesheets, which should be used 
 * in the application. <br>
 * Use the <code>StyleSheetsTag</code> to include the style on your JSP.
 * The manager will be initialize in the <code>ExtendedInitStyleHandler</code>.
 *
 * @author  SAP AG
 * @version 1.0
 * 
 * @see com.sap.isa.core.ui.ExtendedInitStyleHandler
 * @see com.sap.isa.core.taglib.StyleSheetsTag
 */
public class ExtendedStyleManager {

	/**
	 * List with all available style sheets. <br>
	 */	
	protected static ArrayList styleResources = new ArrayList();
	
	/**
	 * <p>Group for undefined group parameter </p> 
	 */
	protected static String undefinedGroupDefault = null;
	
	protected static final IsaLocation log = IsaLocation.getInstance("com.sap.isa.core.ui");
		
    /**
     * Return a list with logical stylesheet keys. <br>
     * 
     * @return list with logical stylesheet names. 
     */
    public static Iterator getStyles() {
        return styleResources.iterator();
    }


    /**
     * Add a stylesheet to the manager. <br>
     * <strong>Note:<strong> the stylesheet name includes also the path 
     * relative to the web application.
     * 
     * @param resource stylesheet name. 
     */
    public static void addStyleSheet(StyleSheet style) {
        styleResources.add(style);
    }
	
	
	/**
	 *	Delete all static information in the class. 
	 *
	 */
	public static void clear(){
		styleResources.clear();
	}
  
	/**
	 * <p>Set the property {@link #undefinedGroupDefault}. </p>
	 * 
	 * @param undefinedGroupDefault The {@link #undefinedGroupDefault} to set.
	 */
	public static void setUndefinedGroupDefault(String undefinedGroupDefault) {
		ExtendedStyleManager.undefinedGroupDefault = undefinedGroupDefault;
	}


	/**
	 *	Write the link tag for the stylesheet file, which should be used in case of 
	 *  hebrew or arabic. <br>
	 *  The style direction requires RTL. 
	 *   
	 */  
	public static void writeRTLStyles(PageContext pageContext, String theme) {
		
		StringBuffer styleSheet;

		Iterator iterateList = ExtendedStyleManager.getStyles();

		try {
			JspWriter out = pageContext.getOut();
			while (iterateList.hasNext()) {
	
				StyleSheet style = (StyleSheet) iterateList.next();
	
				String extension = style.getBrowserTypeExtension(Constants.CV_STYLE_RTL);
	
				if (extension.length() > 0) {
	
					styleSheet = new StringBuffer();
					styleSheet
							.append("<link type=\"text/css\" rel=\"stylesheet\" href=\"");
	
					String name = style.getPath()
							+ extension + ".css";
	
					styleSheet.append(WebUtil.getMimeURL(pageContext, theme, null,
							name));
	
					styleSheet.append("\" />");
	
					out.println(styleSheet.toString());
				}
			}
		}
		catch (IOException exc) {
			log.debug("Unable to include the stylesheet references");
		}

	}
 
	/**
	 * <p> The method includeStyles generate the include statements 
	 * for all styles into the given <code>pageContext</code>.</p>
	 * 
	 * @param pageContext current page context
	 * @param theme theme
	 * @param group sytlesheet group which shoul be used.
	 */
	public static void includeStyles(PageContext pageContext, String theme, String group) {
		boolean styleDirectionRTL = false; 
        
		UserSessionData userSessionData = UserSessionData.getUserSessionData(pageContext.getSession());
		if (userSessionData != null) {
			String parameter = (String)userSessionData.getAttribute(SharedConst.STYLE_DIRECTION);
			if (parameter != null && parameter.equalsIgnoreCase("true")) {
				styleDirectionRTL = true;
			}
		}
  	
	    Iterator iterateList = ExtendedStyleManager.getStyles();
	    String used_theme = (theme != null) ? theme : WebUtil.getTheme(pageContext);
	    String usedGroup = group;
	    
	    if (group == null && undefinedGroupDefault != null) {
	    	usedGroup = undefinedGroupDefault;
	    }
	    
	    boolean isGroupParameterUsed = usedGroup!= null && usedGroup.length()>0;
	
	    try {
	        JspWriter out = pageContext.getOut();
	
	        StringBuffer styleSheet;
	        String browser = BrowserType.getBrowserType((HttpServletRequest) pageContext.getRequest());
	
	        while (iterateList.hasNext()) {
				StyleSheet style = (StyleSheet) iterateList.next();
	        	
				// check if the style support the given group.
				if (isGroupParameterUsed && !style.checkGroup(usedGroup)) {
					continue;
				}
				
	            styleSheet = new StringBuffer();
	            styleSheet.append("<link type=\"text/css\" rel=\"stylesheet\" href=\"");
	
	            String name = style.getPath() + style.getBrowserTypeExtension(browser)  + ".css";
	
	            styleSheet.append(WebUtil.getMimeURL(pageContext, used_theme, null, name));
	
	            styleSheet.append("\" />");
	            out.println(styleSheet.toString());            	            
	        }
	        
			if (styleDirectionRTL) {
				ExtendedStyleManager.writeRTLStyles(pageContext, theme);
			}	        
	
	        // out.println("  <link rel=\"SHORTCUT ICON\"  href=\""
	        //        + ((HttpServletRequest) pageContext.getRequest()).getContextPath()
	        //        + "/favicon_b2b.ico\"/>");
	        // out.println("  <link rel=\"SHORTCUT ICON\"  href=\"/favicon.ico\"/>" );
	
	    }
	    catch (Exception exc) {
	        log.debug("Unable to include the stylesheet references");
	    }
	}
	
}