/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Thorsten Kampp
  Created:      4. Februar 2001, 13:06

  $Revision: #6 $
  $Date: 2001/07/23 $
*****************************************************************************/

package com.sap.isa.core;

import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.util.MessageResources;

import com.sap.isa.core.config.AdminConfig;
import com.sap.isa.core.init.InitializationEnvironment;
import com.sap.isa.core.init.InitializationHandler;
import com.sap.isa.core.init.InitializeException;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.CodePageUtils;
import com.sap.isa.core.util.VersionGet;
import com.sap.isa.core.xcm.ConfigContainer;
import com.sap.isa.core.xcm.ExtendedConfigException;
import com.sap.isa.core.xcm.ExtendedConfigManager;
import com.sap.isa.core.xcm.config.ComponentConfig;
import com.sap.isa.core.xcm.init.ExtendedConfigInitHandler;



public class ActionServlet extends org.apache.struts.action.ActionServlet
               implements InitializationEnvironment {

    // default name of the initailizations' Configuration file
    protected String initConfig = "/WEB-INF/init-config.xml";

    // the initialization manager
    protected InitializationHandler initHandler;
    // the initialization manager
    protected InitializationHandler bootstrapInitHandler;

 	private String classNameMessageResources; 
	boolean mIsJarm = false; 
	boolean mIsSat = false;	
	
    /**
     * The IsaLocation of the ActionServlet.
     * This will be instantiated during init.
     */
	static private IsaLocation log = IsaLocation.getInstance(ActionServlet.class.getName());


    protected static boolean sessionTracking = false;

     //protected static boolean personalizationEnabled = false;



    protected static boolean decodeInterceptor = true;

	public static final String ENCODING_SKIP = "skip.encoding.request.core.isa.sap.com";

    /**
     * Provides a references to the one and only ActionServlet Instance
     * This value will be set in the init method.
     */
    private static ActionServlet theOnlyInstance;

    /**
     * Creates the for the initialization neccessory objects through the
     * <code>Digester</code>.
     *
     * This method overwrittes the one from its base class. The
     * codesequence of the <code>init</code> of the base class is executed
     * befor the logic of this function is executed.
     *
     * @exception ServletException if there is any problem with the
     *                             configuration file
     */

    public void init() throws ServletException {
		final String METHOD_NAME = "init()";
		log.entering(METHOD_NAME);
        super.init();

		// provide class name of message ressources in a static way
		String serverInfo = getServletContext().getServerInfo();
		EnvironmentManager.setJ2EEServerInfo(serverInfo);


        theOnlyInstance = this;
        // check if session tracking is turned on
        String sessionTrackingOn = getServletContext().getInitParameter(ContextConst.SESSION_TRACKING);

        if ((sessionTrackingOn != null) && sessionTrackingOn.equalsIgnoreCase("true"))
          sessionTracking = true;
        else
          sessionTracking = false;

		// bootstrap initialization (only if XCM activated)
		String configFile = getServletConfig().getServletContext().getInitParameter(ContextConst.XCM_PATH);
		if (configFile != null && configFile.length() > 0) {

	     	// perform only if XCM activated
	        bootstrapInitHandler = new InitializationHandler(configFile);
	        try {
	            bootstrapInitHandler.initialize(this, null);
				// Retrieve the actual IsaLocation
				log.debug("XCM Bootsprap initialized succesfully");
	        } catch (InitializeException ex) {
	            ex.printStackTrace();
	            log.exiting();
	            String msg = "Initialization of Extended Configuration Management failed."; 
	            if (ExtendedConfigInitHandler.isTargetDatastoreDB() 
	            	&& !ExtendedConfigInitHandler.isDBInitSuccessful()) {
	            	msg += "\nCould not initialize XCM configuration in Database." +	            		"\nCheck if you have deployed the Software Component 'SAP JAVA DATA DICTIONARY 5.0'";		
	          	}
	            throw new ServletException(msg);
	        }
		}
 
        // Initialize the context-relative path to our configuration resources
        String value = getServletConfig().getInitParameter("initconfig");
        if (value != null)
            initConfig = value;

		initHandler = new InitializationHandler(initConfig);
        try {
        	log.debug("ActionScerlvet.init(): BEGIN initialization proocess");
            initHandler.initialize(this, null);
			log.debug("ActionScerlvet.init(): END initialization proocess");            
        } catch (InitializeException ex) {
            ex.printStackTrace();
            log.exiting();
            throw new ServletException();
        }

		
		// enable non unicode encoding
		value = getServletContext().getInitParameter(ContextConst.UNICODE_ENCODING);
		if (value != null && value.equalsIgnoreCase("false"))
			CodePageUtils.setUnicodeEncoding(false);
			



        getServletContext().setAttribute(ContextConst.INIT_ENV, (InitializationEnvironment)this);

        // determine the configuration of the administration area
        String adminConfig = getServletContext().getInitParameter(ContextConst.ADMIN_AREA_CONFIG);
        if (adminConfig != null)
          AdminConfig.setConfig(adminConfig);
		log.exiting();
        //personalization
        /*
        String personalizationOn = getServletContext().getInitParameter(ContextConst.PERSONALIZATION_ENABLED);

        if ((personalizationOn != null) && personalizationOn.equalsIgnoreCase("true"))
          personalizationEnabled = true;
        else
          personalizationEnabled = false;
        */
        
		ComponentConfig cc = FrameworkConfigManager.XCM.getApplicationScopeConfig(InitializationHandler.getApplicationName() + "config");
		if (cc != null) {
			Properties props = cc.getParamConfig();
			if (props != null) {
				String jarmString = props.getProperty(Constants.JARM);
				if (jarmString != null) {
					if (jarmString.equalsIgnoreCase("true")) {
						mIsJarm = true;			
					}
				}
				String satString = props.getProperty(Constants.SAT);
				if (satString != null) {
					if (satString.equalsIgnoreCase("true")) {
						mIsSat = true;			
					}
				}
				
			}
		}
		
		// initialize Version Object
		new VersionGet();
        

    }
	
    /**
     *  Re-initialization
     *
     * @param className The name of the class which should be re-initialized
     *                  should correspond to this in the init-config.xml
     * @exception InitializeException if a problem during the initialization occurs
    */
    public void performInitialization(String className) throws InitializeException {
        if (bootstrapInitHandler != null)
			bootstrapInitHandler.performInitialization(className);
		initHandler.performInitialization(className);
    }

    /**
     *  Termination
     *
     * @param className The name of the class which should be terminated
     *                  should correspond to this in the init-config.xml
    */
    public void performTermination(String className) {
		if (bootstrapInitHandler != null)
			bootstrapInitHandler.performTermination(className);

        initHandler.performTermination(className);
        
    }

    /**
	 * Befor handling the execution to the struts.ActionServlet
	 * this method sets the <strong>sessionId<strong> in the ThreadLocal
	 * of the current thread. This enables to log the sessionId through all
	 * the application layers.
	 *
	 * @param request The servlet request we are processing
	 * @param response The servlet response we are creating
	 *
	 * @exception IOException if an input/output error occurs
	 * @exception ServletException if a servlet exception occurs
	 */
	protected void process(HttpServletRequest  request,
						   HttpServletResponse response)
		throws IOException, ServletException {


			super.process(request, response);
            
	}


    static public ActionServlet getTheOnlyInstance()
    {
      return theOnlyInstance;
    }

    //Implementation of the <code>InitializationEnvironment<code>
    //interface

    /**
     * determins the environment specific <code>MessageResources<code>
     *
     * @return the MessageResource
     **/
    public MessageResources getMessageResources() {
        return getResources();
    }

    /**
     * Determins the <code>InputStream<code> for a given resource
     * If an InputStream couldn't be determin, the method
     * returns <code>null</code>
     *
     * @param path the name of the resource
     * @return the InputStream or <strong>null<strong>
     */
    public InputStream getResourceAsStream(String path) {
		// check if Extended Configuration Management is turned on
		if (ExtendedConfigInitHandler.isActive()) {
			// logging is already initialized
	        if (log.isDebugEnabled())
	    		log.debug("Extended Configuration Management (XCM) is ACTIVE");
			ExtendedConfigManager xcManager = ExtendedConfigManager.getExtConfigManager();
			// check if file is part of XCM
			if (xcManager.isManagedFile(path)) {
		    	if (log.isDebugEnabled())
		    		log.debug("File '" + path + "' is part of XCM");

				// get default configuration
				try {
					ConfigContainer cc = xcManager.getModifiedConfig(null);
					InputStream is = cc.getConfigAsStream(path);
					if (is != null)
						return is;

				} catch (ExtendedConfigException xcmex) {
					log.error(LogUtil.APPS_USER_INTERFACE_CONTROLLER,"system.xcm.exception", new Object[] {"Error retrieving file '" + path + "from XkCM"}, null);
				}
			}
		}
        return getServletContext().getResourceAsStream(path);
    }

    /**
     * Determines the <code>RealPath</code> for a given local path.
     * You ought to use getResourceAsStream for most reading purposes,
     * if you need to write to a specific directory relatively to
     * your web-application, you may use this method.
     *
     * @param path the name of the resource
     * @return the real absolute path
     */
    public String getRealPath(String path) {
        return getServletContext().getRealPath(path);
    }


  /**
   * Returns a String containing the value of the named initialization parameter
   * or <code>null</code> if the parameter does not exist.
   * @param name a String containing the name of the parameter whose value is requested
   * Returns a String containing the value of the named initialization parameter
   *         or <code>null</code> if the parameter does not exist.
   */
  public String getParameter(String name) {
        return getServletContext().getInitParameter(name);
  }

   /**
    * Returns a set of parameter names which belong to the given environment.
    * Parameter values can be retrieved using the <code>getParameter()</code>
    * method
    *
    * @return an Enumeration of String objects containing the names of the
    *         initialization parameters
    * /
    */
    public Enumeration getParameterNames() {
        return getServletContext().getInitParameterNames();
    }

   /**
    * Returns <code>true</code> if session tracking is turned on. Session tracking
    * is a feature which logs all communication between the session/request and
    * a Struts-Action
    */
    public static boolean isSessionTrackingEnabled() {
       return sessionTracking;
    }

   /**
    * Returns <code>true</code> if personalization is turned on. With Personalization
    * Web shop can be personalized. Eg: Showing promotions etc
    */
   	/*
    public static boolean isPersonalizationEnabled() {
       return personalizationEnabled;
    }
    */

    public void destroy() {
      super.destroy();
	  terminateInitHandlers();
	  bootstrapInitHandler=null;
      theOnlyInstance=null;
      initHandler = null;
      getServletContext().setAttribute(ContextConst.INIT_ENV, null);
      IsaLocation.terminate();
    }
	
	private void terminateInitHandlers() {
		//terminate handlers in bootstrapconfig
		if (bootstrapInitHandler != null)
		  bootstrapInitHandler.terminate();

		initHandler.terminate();
	}
	
	public boolean isJarmEnabled() {
		return mIsJarm;
	}
  }