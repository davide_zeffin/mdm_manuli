/*****************************************************************************
    Interface:    Iterable
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Thomas Smits
    Created:      20.2.2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/06/26 $
*****************************************************************************/

package com.sap.isa.core;

import java.util.Iterator;

/**
 * Interface used by the iterate custom tag to iterate over a list of
 * elements. Implement this interface if you want to use objects of
 * your class together with the iterate tag.
 *
 * @author Thomas Smits
 * @version 1.0
 *
 * @see com.sap.isa.core.taglib.IterateTag IterateTag
 */
public interface Iterable {

  /**
   * Returns an iterator over the elements contained in this object.
   *
   * @return Iterator for this object
   */
  public Iterator iterator();
}