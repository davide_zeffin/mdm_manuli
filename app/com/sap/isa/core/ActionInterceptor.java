/**
 * Interface to define action interceptor
 * Aimed at intercepting the action servlet's execution
 */
package com.sap.isa.core;

import javax.servlet.http.HttpSession;

public interface ActionInterceptor {
  /**
   * Implement it to perform the tasks before doPerform of BaseAction
   */
  public String interceptBefore(String actionName, HttpSession session) throws Exception;
  /**
   * Implement it to perform the tasks after doPerform of BaseAction
   * New forward name. This would override the flow of the application
   */
  public String interceptAfter(String actionName, HttpSession session) throws Exception;

  /**
   * Returns the instance
   */
  public ActionInterceptor makeInstance() ;
}