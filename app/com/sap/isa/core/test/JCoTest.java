package com.sap.isa.core.test;

// JCO imports
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import com.sap.isa.core.init.InitializationHandler;
import com.sap.isa.core.security.SecureStore;
import com.sap.mw.jco.JCO;



/**
 *
 */
public class JCoTest extends BaseComponentTest {

	private static final String JCO_PING = "jcoping";
		

	public JCoTest() {
		BaseComponentTest.TestDescriptor jcoPing = new BaseComponentTest.TestDescriptor(JCO_PING);
		jcoPing.setTestDescription("Test connection to the SAP system using SAP Java Connector (JCo)");
		addTestDescriptor(jcoPing);
	}
		
	/**
	 * @see com.sap.isa.core.xcm.admin.test.ComponentTest#performTest(Properties)
	 */
	public boolean performTest(String testName) {
		if (testName == null)		
			return false;

		Map params = getTestParameters(testName);
		Properties conProps = new Properties();
		if (params != null && params.size() > 0)  
			for (Iterator iter = params.keySet().iterator(); iter.hasNext(); ) {
				String paramName = (String)iter.next();
				String paramValue = (String)params.get(paramName);
				if (paramValue == null)
					continue;
				if (SecureStore.ERROR_RETRIEVING_SECURE_STORE.equals(paramValue)) {
					StringBuffer sb = new StringBuffer(InitializationHandler.getMessageResource("system.eai.error.secure.storage1"));
					sb.append(InitializationHandler.getMessageResource("system.eai.error.secure.storage2"));
					sb.append(InitializationHandler.getMessageResource("system.eai.error.secure.storage3"));
					setTestResultDescription(testName, InitializationHandler.getMessageResource("system.eai.error.secure.storage4") + sb.toString());
					return false;
				}
				conProps.setProperty(paramName, paramValue);
			}
				
		JCO.Client client = JCO.createClient(conProps);
		
		try {
			client.connect();
			setTestResultDescription(testName, client.getAttributes().toString());
			return true;
			
		} catch (JCO.Exception ex) {
			setTestResultDescription(testName, "Connection failed. " + ex.toString());
			return false;	
		}
	}
}
