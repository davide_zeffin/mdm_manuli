package com.sap.isa.core.test;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;


/**
 * Base class for all tests
 */
public abstract class BaseComponentTest implements ComponentTest {
 	protected static IsaLocation log = IsaLocation.
        getInstance(BaseComponentTest.class.getName());

	private Map mTests = new HashMap();
	private boolean timeout=false;

	private int maxTimeout;
	static private String mTimeoutDescr = "Test timed out";
	protected static class TestDescriptor {
		
		private String mTestNotRunYetDescr = "Test not run yet";
		private Map mTestParams;
		private String mName;
		private String mDescr = mTestNotRunYetDescr;
		private String mTestResultDescr;
		private boolean mIsTestPerformed = false;
		private boolean mResult=false;
		public void setTestParams(Map testParams) {
			mTestParams = testParams;			
		}

		public Map getTestParams() {
			return mTestParams;			
		}

		public TestDescriptor(String name) {
			mName = name;			
		}		

		String getName() {
			return mName;
		}

		public void setTestDescription(String descr) {
			mDescr = descr;
		}

		public void setTestResult(boolean result) {
			mResult = result;
		}
		/**
		 * Method getTestResult.
		 * @return boolean
		 */
		boolean getTestResult() {
			return mResult;
		}


		/**
		 * Default: 'Not run yet'
		 */
		String getTestDescription() {
			return mDescr;
		}
		
		void setTestResultDescription(String descr) {
			mTestResultDescr = descr;
		}
		String getTestResultDescription() {
			return mTestResultDescr;
		}

		void setIsTestPerformed(boolean isTestPerformed) {
			mIsTestPerformed = isTestPerformed;
		}

		boolean getIsTestPerformed() {
			return mIsTestPerformed;
		}
		void reset() {
			mDescr = mTestNotRunYetDescr;
			mIsTestPerformed = false;
		}
	}
	
	protected void addTestDescriptor(TestDescriptor descriptor) {
		if (descriptor != null)
			mTests.put(descriptor.getName(), descriptor);
	}
	

	/**
	 * @see com.sap.isa.core.xcm.admin.test.ComponentTest#getTestDescription(String)
	 */
	public String getTestDescription(String testName) {
		if (testName == null || mTests.get(testName) == null) 
			return "";
		else
			return ((TestDescriptor)mTests.get(testName)).getTestDescription();
	}

	/**
	 * @see com.sap.isa.core.xcm.admin.test.ComponentTest#getTestDescription(String)
	 */
	public boolean getTestResult(String testName) {
		if (testName == null || mTests.get(testName) == null) 
			return false;
		else
			return ((TestDescriptor)mTests.get(testName)).getTestResult();
	}




	public void setTestDescription(String testName, String descr) {
		if (testName == null || mTests.get(testName) == null) 
			return;
		else
			((TestDescriptor)mTests.get(testName)).setTestDescription(descr);
	}


	/**
	 * @see com.sap.isa.core.xcm.admin.test.ComponentTest#getTestResultDescription(String)
	 */
	public String getTestResultDescription(String testName) {
		if (testName == null || mTests.get(testName) == null) 
			return "";
		else
			return ((TestDescriptor)mTests.get(testName)).getTestResultDescription();
	}
	/**
	 * sets timeout flag. This will terminate the performTests method
	 * The test results of the actual running test will not be stored
	 */
	public void setTimeout()
	{
		 timeout=true;
	}
	/**
	 * sets timeout flag. This will terminate the performTests method
	 * The test results of the actual running test will not be stored
	 */
	public boolean getTimeout()
	{
		 return timeout;
	}

	/**
	 * returns maximal runtime for all tests.
	 */
	public int getMaxTimeout()
	{
		return 10000;
	}

	/**
	 * Performs all tests of a component
	 * @return <code>true<code> all the tests were successful. 
	 *          Otherwise <code>false</code>
	 */
	public boolean performTests()
	{
		Iterator iter=getTestNames().iterator();
		boolean rc = false;
		while( iter.hasNext()&& !timeout )
		{
			String name = (String)iter.next();
			boolean thisResult = performTest(name);
			rc |= thisResult;
			if (thisResult == true ) {
				if( log.isDebugEnabled())
				{
					log.debug( name + 
						"-test successfully executed: test passed");
				}
			} else {
				log.error(LogUtil.APPS_BUSINESS_LOGIC,
					"ccms.heartbeat.test.failed",
					new Object[] { name, "unknown" },
					new Exception(this.getTestResultDescription(name)));
			}
			setIsTestPerformed(name, true);
			setTestResult(name,thisResult);
		}
		return rc;
	}

	/**
	 * Sets the result of a certain test
	 * @param testName
	 * @param result 
	 */
	public void setTestResult(String testName, boolean result)
	{
		if(!timeout)
			((TestDescriptor)mTests.get(testName)).setTestResult(result);
	}

	public void setTestResultDescription(String testName, String descr) {
		if (timeout || testName == null || mTests.get(testName) == null) 
			return;
		else
		{
			if( timeout )
				((TestDescriptor)mTests.get(testName)).setTestResultDescription(mTimeoutDescr);
			else
			((TestDescriptor)mTests.get(testName)).setTestResultDescription(descr);
		}
	}


	/**
	 * @see com.sap.isa.core.xcm.admin.test.ComponentTest#isTestPerformed(String)
	 */
	public boolean isTestPerformed(String testName) {
		if (testName == null || mTests.get(testName) == null) 
			return false;
		else
			return ((TestDescriptor)mTests.get(testName)).getIsTestPerformed();

	}

	/**
	 * @see com.sap.isa.core.xcm.admin.test.ComponentTest#isTestPerformed(String)
	 */
	protected void setIsTestPerformed(String testName, boolean isPerformed) {
		if (testName == null || mTests.get(testName) == null) 
			return;
		else
			((TestDescriptor)mTests.get(testName)).setIsTestPerformed(isPerformed);

	}


	/**
	 * @see com.sap.isa.core.xcm.admin.test.ComponentTest#resetTest(String)
	 */
	public void resetTest(String testName) {
		if (testName == null || mTests.get(testName) == null) 
			return;
		((TestDescriptor)mTests.get(testName)).reset();
	}

	/**
	 * @see com.sap.isa.core.xcm.admin.test.ComponentTest#getTestNames()
	 */
	public Set getTestNames() {
		return Collections.unmodifiableSet(mTests.keySet());
	}

	/**
	 * @see com.sap.isa.core.xcm.admin.test.ComponentTest#getTestParameters(java.lang.String)
	 */
	public Map getTestParameters(String testName) {
		if (testName == null || mTests.get(testName) == null) 
			return Collections.EMPTY_MAP;
		return ((TestDescriptor)mTests.get(testName)).getTestParams();

	}
	/**
	 * @see com.sap.isa.core.xcm.admin.test.ComponentTest#setTestParameters(java.lang.String, java.util.Map)
	 */
	public void setTestParameters(String testName, Map testParams) {
		if (testName == null || mTests.get(testName) == null)
			return;
		((TestDescriptor)mTests.get(testName)).setTestParams(testParams);
	}
	
}
