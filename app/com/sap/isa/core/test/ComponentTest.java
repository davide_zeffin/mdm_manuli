package com.sap.isa.core.test;

import java.util.Map;
import java.util.Set;

/**
 * A class implementing this interface can be used
 * to perform a test on a component
 */
public interface ComponentTest {

	/**
	 * Returns a description of the test
	 * @param testName
	 * @return a description of the test
	 */
	public String getTestDescription(String testName);
	
	/**
	 * Performs the test
	 * @return <code>true<code> if the test was successful. 
	 *          Otherwise <code>false</code>
	 */
	public boolean performTest(String testName);

	/**
	 * Performs all tests of a component
	 * @return <code>true<code> all the tests were successful. 
	 *          Otherwise <code>false</code>
	 */
	public boolean performTests();
	
	/**
	 * Returns an unmodifiable map of the tes properties
	 * @param testName
	 * @return an unmodifiable map of the tes properties
	 */
	public Map getTestParameters(String testName);
	
	
	/**
	 * Returns a textual description of the test result
	 * @param testName
	 * @return a textual description of the test result
	 */
	public String getTestResultDescription(String testName);

	/**
	 * Returns the result of a certain test
	 * @param testName
	 * @return a textual description of the test result
	 */
	public boolean getTestResult(String testName);

	/**
	 * Sets the result of a certain test
	 * @param testName
	 * @param result 
	 */
	public void setTestResult(String testName, boolean result);
	
	/**
	 * Returns <code>true</code> if the test has been performed
	 * @param testName
	 * @return <code>true</code> if the test has been performed
	 */
	public boolean isTestPerformed(String testName);
	
	/**
	 * Resets the test. After this a new test can be performed
	 * @param testName
	 */
	public void resetTest(String testName);
	
	/**
	 * Returns a Set containing the names of the test
	 * which can be performed
	 * @return a Map containing the names of the test
	 * 			which can be performed
	 */
	public Set getTestNames(); 

	/**
	 * Used to be called if the calling thread signals a timeout
	 * to the performTests method
	 */
	public void setTimeout();

	/**
	 * should returns true if setTimeout was called
	 */
	public boolean getTimeout();

	/**
	 * Set test parameter for a given test
	 * @param testName name of test
	 * @param testParams test parameters
	 */
	public void setTestParameters(String testName, Map testParams);

	/**
	 * returns maximal runtime for all tests.
	 */
	public int getMaxTimeout();


}
