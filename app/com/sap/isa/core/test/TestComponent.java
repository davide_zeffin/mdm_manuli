package com.sap.isa.core.test;

/**
 *
 */
public class TestComponent extends BaseComponentTest {

	private static final String TEST1 = "test1";
	private static final String TEST2 = "test2";	

	public TestComponent() {
		BaseComponentTest.TestDescriptor test1 = new BaseComponentTest.TestDescriptor(TEST1);
		test1.setTestDescription("Descr of test1");
		addTestDescriptor(test1);
		BaseComponentTest.TestDescriptor test2 = new BaseComponentTest.TestDescriptor(TEST2);		
		test2.setTestDescription("Descr of test2");	
		addTestDescriptor(test2);		
	}
		
	/**
	 * @see com.sap.isa.core.xcm.admin.test.ComponentTest#performTest(Properties)
	 */
	public boolean performTest(String testName) {
		if (testName == null)		
			return false;
		if (testName.equals(TEST1))
			return test1();
				
		if (testName.equals(TEST2))
			return test2();

		return false;
	}

	private boolean test1() {
		setTestResultDescription(TEST1, "Test failed");
		setIsTestPerformed(TEST1, true);
		return false;
	}

	private boolean test2() {
		setTestResultDescription(TEST2, "Test successful");
		setIsTestPerformed(TEST2, true);
		return true;
	}

}
