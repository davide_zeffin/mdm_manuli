package com.sap.isa.core.test;

// ISA imports
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jdbc.JDBCConstants;
import com.sap.isa.core.eai.sp.jdbc.JDBCManagedConnectionFactory;



/**
 * @deprecated Use com.sap.isa.basketdb.test.BasketDBTest.JDBCTest instead.
 */
public class JDBCTest extends BaseComponentTest {

	private static final String JDBC_PING = "jdbcping";
	private static final String JDBC_DATA_STRUCT = "jdbc_javabasket";
		

	public JDBCTest() {
		BaseComponentTest.TestDescriptor jdbcPing = new BaseComponentTest.TestDescriptor(JDBC_PING);
		jdbcPing.setTestDescription("Test connection to the Database");
		addTestDescriptor(jdbcPing);

		BaseComponentTest.TestDescriptor jdbcDataStruct = new BaseComponentTest.TestDescriptor(JDBC_DATA_STRUCT);
		jdbcDataStruct.setTestDescription("Checks if database has correct data structures needed by Java Basket");
		addTestDescriptor(jdbcDataStruct);
	}
		
	/**
	 * @see com.sap.isa.core.xcm.admin.test.ComponentTest#performTest(Properties)
	 */
	public boolean performTest(String testName) {
		if (testName == null)		
			return false;

		Map params = getTestParameters(testName);
		Properties conProps = new Properties();
		if (params != null) 
			for (Iterator iter = params.keySet().iterator(); iter.hasNext(); ) {
				String paramName = (String)iter.next();
				String paramValue = (String)params.get(paramName);
				conProps.setProperty(paramName, paramValue);
			}
		if (testName.equals(JDBC_PING))
			return testConnection(testName, conProps);

		if (testName.equals(JDBC_DATA_STRUCT))
			return testJavaBasketDatastructure(testName, conProps);

		
		return false;
	}
	
	private boolean testConnection(String testName, Properties conProps) {
		String resultDescr = "";
		try {
			String usePool = conProps.getProperty(JDBCConstants.JDBC_USE_POOL);
			
			if (usePool != null && usePool.equalsIgnoreCase("true"))
				resultDescr = "JDBC connection management configured to use connection pooling. ";
			else
				resultDescr = "JDBC connection configured to access the database DIRECTLY. Note: Due performance reasons, it is recommended to use a JDBC connection Pool. Use connection pool instead. ";
			
			resultDescr = resultDescr + JDBCManagedConnectionFactory.testConnection(conProps);
			setTestResultDescription(testName, resultDescr);
			return true;
			
		} catch (Throwable bex) {
			bex.printStackTrace();
			resultDescr = resultDescr + bex.toString();
			setTestResultDescription(testName, resultDescr);
			return false;
		}
	}

	protected boolean testJavaBasketDatastructure(String testName, Properties conProps) {

		String smokerClassName = "com.sap.isa.backend.jdbc.order.Smoker";
		// get SQL Statements using reflection
		Class c = null;
		Map sqlSelects = new HashMap();
		try {
		  c = Class.forName(smokerClassName);
		} catch (ClassNotFoundException ex) {
			
		  String errorMsg = "class '" + smokerClassName + "' not found";
		  setTestResultDescription(testName, errorMsg);
		  return false;
		}
		Method getSmokeStrings;
		String sqlStatement = null;
		try {
		  getSmokeStrings = c.getMethod("getSmokeStrings", null);
		  sqlSelects = (Map) getSmokeStrings.invoke(c, null);
		} catch (NoSuchMethodException e) {
			setTestResultDescription(testName, e.toString());
			return false;
		} catch (IllegalAccessException e) {
			setTestResultDescription(testName, e.toString());
			return false;
		} catch (InvocationTargetException e) {
			setTestResultDescription(testName, e.toString());
			return false;
		}

		  java.sql.Connection sqlCon = null;
		  try {
			sqlCon = (java.sql.Connection)JDBCManagedConnectionFactory.getJDBCConnection(conProps);
		  } catch (BackendException bex) {
			String errorMsg = "Cannot open connection to database\n" + bex.toString();
			setTestResultDescription(testName, errorMsg);
			return false;
		  }
		  try {
			for (Iterator iter = sqlSelects.values().iterator(); iter.hasNext();) {
			  sqlStatement = (String)iter.next();
			  Statement stmt = sqlCon.createStatement();
			  stmt.execute(sqlStatement);
			}
			sqlCon.close();
		  } catch (SQLException e) {
		  	  try {
				  sqlCon.close();
				  String errorMsg = "Error executing '" + sqlStatement + "'" + "\n" + e.toString();
				  setTestResultDescription(testName, errorMsg);
		  	  }
		  	  catch(SQLException e1)
		  	  {
				  String errorMsg = "Error closing JDBC Connection\n" + e1.toString();
				  setTestResultDescription(testName, errorMsg);
		  	  }
			  return false;
		  }
		  setTestResultDescription(testName, "Data structures for Java Basket correct");
		  return true;
	}
}
