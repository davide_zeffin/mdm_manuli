package com.sap.isa.core.test;

import java.util.Iterator;

/**
 *	Determine the result of all tests of a ComponentTest
 */
public class ComponentTestUtils {
	public static final int OK=0;
	public static final int TESTS_FAILED=1;
	public static final int TESTS_PARTLYEXECUTED=2;
	public static final int TESTS_TIMEOUT=3;

	/**
	 * Constructor for ComponentTestUtils.
	 */
	public ComponentTestUtils() {
		super();
	}
	public static int getOverAllStatus(ComponentTest test)
	{
		boolean allTestsExecuted=true;
		boolean allTestsOK=true;
		boolean timeout=false;
		Iterator iter= test.getTestNames().iterator();
		while( iter.hasNext())
		{
			String name=(String)iter.next();
			allTestsExecuted &= test.isTestPerformed(name);
			allTestsOK &= test.getTestResult(name);

		}
		if( allTestsExecuted )
			if( allTestsOK )
				return OK;
			else
				return TESTS_FAILED;
		if( test.getTimeout())
			return TESTS_TIMEOUT;
		return TESTS_PARTLYEXECUTED;
	}
	

}
