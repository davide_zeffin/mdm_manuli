/*@lineinfo:filename=CrmJdbcStandalone*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.sap.isa.core.sqlj;
/*
 * SQLJ Context for Standalone Database connections
 */

/*@lineinfo:generated-code*//*@lineinfo:6^2*/

//  ************************************************************
//  SQLJ context declaration:
//  ************************************************************

public class CrmJdbcStandalone 
extends com.sap.sql.sqlj.runtime.ref.UrlContextImpl
implements sqlj.runtime.ConnectionContext
{
  public CrmJdbcStandalone(java.sql.Connection conn) 
    throws java.sql.SQLException 
  {
    super(null, conn);
  }
  public CrmJdbcStandalone(sqlj.runtime.ConnectionContext other) 
    throws java.sql.SQLException 
  {
    super(null, other);
  }
  public CrmJdbcStandalone(java.lang.String url, java.lang.String user, java.lang.String password, boolean autoCommit) 
    throws java.sql.SQLException 
  {
    super(null, url, user, password, autoCommit);
  }
  public CrmJdbcStandalone(java.lang.String url, java.util.Properties info, boolean autoCommit) 
    throws java.sql.SQLException 
  {
    super(null, url, info, autoCommit);
  }
  public CrmJdbcStandalone(java.lang.String url, boolean autoCommit) 
    throws java.sql.SQLException 
  {
    super(null, url, autoCommit);
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:6^36*//*@lineinfo:generated-code*/