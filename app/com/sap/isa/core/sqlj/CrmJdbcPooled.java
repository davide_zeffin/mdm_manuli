/*@lineinfo:filename=CrmJdbcPooled*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.sap.isa.core.sqlj;
/*
 * SQLJ Context for Standalone Database connections
 */
 
/*@lineinfo:generated-code*//*@lineinfo:6^2*/

//  ************************************************************
//  SQLJ context declaration:
//  ************************************************************

public class CrmJdbcPooled 
extends com.sap.sql.sqlj.runtime.ref.DataSourceContextImpl
implements sqlj.runtime.ConnectionContext
{
  public CrmJdbcPooled(java.sql.Connection conn) 
    throws java.sql.SQLException 
  {
    super(null, conn);
  }
  public CrmJdbcPooled(sqlj.runtime.ConnectionContext other) 
    throws java.sql.SQLException 
  {
    super(null, other);
  }
  public CrmJdbcPooled() 
    throws java.sql.SQLException 
  {
    super(null, dataSource);
  }
  public CrmJdbcPooled(java.lang.String user, java.lang.String password) 
    throws java.sql.SQLException 
  {
    super(null, dataSource, user, password);
  }
  public static final java.lang.String dataSource =  com.sap.isa.core.Constants.DATASOURCE_LOOKUP_NAME + "/" + com.sap.isa.core.init.InitializationHandler.getApplicationName();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:6^175*//*@lineinfo:generated-code*/