/*****************************************************************************
    Class:        FrameworkException
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP AG
    Created:      05.04.2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/06/26 $
*****************************************************************************/

package com.sap.isa.core;

/**
 * This exceptions represents an illegal system state caused by
 * misconfiguration. 
 *
 * @author SAP AG
 * @version 1.0
 */
public class FrameworkException extends RuntimeException {

    /**
     * Constructs a <code>PanicException</code> with no detail  message.
     */
    public FrameworkException() {
        super();
    }

   /**
     * Constructs a <code>PanicException</code> with the specified
     * detail message.
     *
     * @param   s   the detail message.
     */
    public FrameworkException(String msg) {
        super(msg);
    }
}