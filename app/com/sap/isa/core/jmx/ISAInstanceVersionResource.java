package com.sap.isa.core.jmx;


import com.sap.isa.core.init.InitializationHandler;
import com.sap.jmx.monitoring.api.ActiveDatasupplierSupport;
import com.sap.jmx.monitoring.api.ConfigurationList;
import com.sap.jmx.monitoring.api.VersionInfo;
import com.sap.jmx.monitoring.api.VersionReportNotification;
import com.sap.jmx.monitoring.api.VersionResourceMBean;
 

public class ISAInstanceVersionResource 
					extends ActiveDatasupplierSupport
					implements VersionResourceMBean 
 {
  	  private VersionInfo mVersionInfo;
	  private VersionReportNotification mNotification;
	  private String mApplicationName = InitializationHandler.getApplicationName();

	public ISAInstanceVersionResource(String instanceName)
	{
		
	  mNotification = new VersionReportNotification(this);
	  		
	  ConfigurationList list = new ConfigurationList();
	  
	  mVersionInfo = new VersionInfo(
	  	mApplicationName,
		"",
		"",
		"",
		"",
		"");
	  
	}
	
	public VersionInfo getVersion() {
		return mVersionInfo;
	}

	public void report() {
		sendNotification(mNotification);
	}
	
}
