package com.sap.isa.core.jmx;


import com.sap.isa.core.init.InitializationHandler;
import com.sap.isa.core.util.VersionProvider;
import com.sap.jmx.monitoring.api.ConfigurationList;
import com.sap.jmx.monitoring.api.VersionInfo;
import com.sap.jmx.monitoring.api.VersionReportNotification;
 

public class ISAVersionResource 
 {
  	  private VersionInfo mVersionInfo;
	  private VersionReportNotification mNotification;
	  private VersionProvider mVersionProvider = null;
	  private String mApplicationName = InitializationHandler.getApplicationName();
	  private static String mSolution; 
	  private static String mApp; 

	public ISAVersionResource(String componentName, VersionProvider versionProvider)
	{
		
	  mVersionProvider = versionProvider;
	  mNotification = new VersionReportNotification(this);
	  		
	  ConfigurationList list = new ConfigurationList();
	  
		mSolution = mVersionProvider.getSolution();
		mApp = mVersionProvider.getApplication();
    }
	public static String getSolution() {
		return mSolution;				
	}
	public static String getApplication() {
		return mApp; 
	}
}
