package com.sap.isa.core.jmx;

import com.sap.jmx.monitoring.api.ActiveDatasupplierSupport;
import com.sap.jmx.monitoring.api.SimpleValueReportNotification;
import com.sap.jmx.monitoring.api.SimpleValueResourceMBean;
import com.sap.isa.core.eai.sp.jco.JCoManagedConnectionFactory;

public class FMActivityResource 
		extends ActiveDatasupplierSupport 
		implements SimpleValueResourceMBean {

	private String mFMName;
	private SimpleValueReportNotification mNotification;
	
	private FMActivityResource() {
		super();
	}

	public FMActivityResource(String fmName) {
		mFMName = fmName;
		mNotification = new SimpleValueReportNotification(this);
	}

	public int getValue() {
		return JCoManagedConnectionFactory.getActivityData(mFMName);
	}
	
	public void report() {
		sendNotification(mNotification);
	}
	
}
