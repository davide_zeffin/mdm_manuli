package com.sap.isa.core.jmx;

/**
 * This interface should be implemented by a class which provides activity 
 * information. 
 */
public interface ActivitySupplier {
	
	/**
	 * Returns activity information for the given name. The return value is
	 * implementation specific.
	 */
	public int getActivityData(String activityObjectName);
 

}
