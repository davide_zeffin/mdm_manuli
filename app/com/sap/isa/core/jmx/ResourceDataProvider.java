package com.sap.isa.core.jmx;


/**
 * This data collects confiugration data provided to CCMS 
 */
public class ResourceDataProvider {

	static ResourceDataProvider mResData = new ResourceDataProvider(); 
	
	private ResourceDataProvider() { 
	}

	public static synchronized ResourceDataProvider getResourceDataProvider() {
		return mResData;
	}
	
	

}
