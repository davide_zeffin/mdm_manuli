package com.sap.isa.core.jmx;

import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.w3c.dom.Document;

import com.sap.isa.core.cache.Cache;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.conv.ParamConverter;
import com.sap.isa.core.util.conv.ParamConverterUtil;
import com.sap.isa.core.util.conv.PasswordConverter;
import com.sap.isa.core.xcm.ConfigContainer;
import com.sap.isa.core.xcm.XCMConstants;
import com.sap.isa.core.xcm.XCMUtils;
import com.sap.isa.core.xcm.init.ExtendedConfigInitHandler;
import com.sap.isa.core.xcm.init.XCMAdminComponentMetadata;
import com.sap.isa.core.xcm.init.XCMAdminInitHandler;
import com.sap.isa.core.xcm.init.XCMAdminParamConstrainMetadata;
import com.sap.isa.core.xcm.scenario.ScenarioManager;
import com.sap.jmx.monitoring.api.ActiveDatasupplierSupport;
import com.sap.jmx.monitoring.api.ConfigurationList;
import com.sap.jmx.monitoring.api.ConfigurationReportNotification;
import com.sap.jmx.monitoring.api.ConfigurationResourceMBean;


/**
 * This class extracts the configuration parameters for each 
 * component of currently active scenarios
 */
public class XCMConfigResource extends ActiveDatasupplierSupport 
		implements ConfigurationResourceMBean, ActiveResource {

	private ConfigurationReportNotification mNotification; 
	protected static IsaLocation log =
						   IsaLocation.getInstance(XCMConfigResource.class.getName());

	private String mScenarioName;
	private ConfigContainer mCC;

	public XCMConfigResource(String scenarioName, ConfigContainer cc) {
		mScenarioName = scenarioName;
		mCC = cc;
		mNotification = new ConfigurationReportNotification(this);
		
	}


	private static Map getScenarioParams(String scenName) {
		ScenarioManager scenMgr = ScenarioManager.getScenarioManager();
		return scenMgr.getConfigParams(scenName);

	}

	private int getMaxParamLength(Map configParams) {
		int maxLength = 0;
		for (Iterator iter = configParams.values().iterator(); iter.hasNext(); ) {
			String value = (String)iter.next();
			if (value.length() > maxLength)
			maxLength = value.length();
		}
		return maxLength;
	}


	public void report() {
		sendNotification(mNotification);
	}


	public static Map getCurrentScenarioNames() {
		
		Map scenarios = new HashMap();
		Cache.Access access = null;
		try {
			access = Cache.getAccess(XCMConstants.CACHE_REGION_NAME);
		} catch (Cache.Exception cex) {
			String msg = "Erorr accessing cache [region]='" + XCMConstants.CACHE_REGION_NAME + "'";
			log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.exception", new Object[] { msg }, cex);
			return Collections.EMPTY_MAP;
		}
		Set keys = access.getKeys();
			
		// iterate through all loaded scenarios
		// ommit the bootstrap scenario 
		for (Iterator iter = keys.iterator(); iter.hasNext();) {
			String key = (String) iter.next();
			ConfigContainer cc = (ConfigContainer) access.get(key);
			String scenName = cc.getScenarioName();
			if(log.isDebugEnabled()) 
				log.debug("processing [scenario]='" + scenName + "'");
			if (scenName.equals(ScenarioManager.NAME_DEFAULT_SCENARIO))
				continue;
			scenarios.put(scenName, cc);
		}
		return scenarios;	
	}

	public ConfigurationList getConfigurationParameters() {
		final String METHOD = "getConfigurationParameters()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("getting scenario params for [scenario]='" + mScenarioName + "'");

		ConfigurationList list = new ConfigurationList();
		Map configParams = new HashMap();
		Map configParamsDescr = new HashMap();


		Map scenParams = getScenarioParams(mScenarioName);

			String custPath = mCC.getConfigParams().getParamValue(ExtendedConfigInitHandler.PARAMNAME_PATH_CUSTOMER_CONFIG);

			Document compConfig =
				mCC.getDocument(custPath + "/" + XCMConstants.DEFAULT_FILE_NAME_CONFIG_DATA);

		
		for (Iterator iter2 = scenParams.keySet().iterator(); iter2.hasNext(); ) {
			configParams.put("xcm.scenario", mScenarioName);
			configParamsDescr.put("xcm.scenario", "Name of XCM scenario");
			String scenParamName = (String) iter2.next();
			String scenParamValue =
				(String) scenParams.get(scenParamName);
			XCMAdminParamConstrainMetadata paramMetaData =
				XCMAdminInitHandler.getXCMAdminScenarioMetadata(
					scenParamName);
			String componentName = null;					
			if (paramMetaData == null) {
				String msg = "Metadata missing in xcmadmin-config.xml for [component]='" + scenParamName + "'";
				log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.exception", new Object[] { msg }, null);
				configParams.put(scenParamName, "ERROR");
				configParamsDescr.put(scenParamName, "!!!" + msg);
				continue;
			} else
				componentName = paramMetaData.getComponent();
			if (componentName == null) {
				// fix value
				XCMAdminParamConstrainMetadata scenParamMetaData =
					XCMAdminInitHandler.getXCMAdminScenarioMetadata(scenParamName);
				String scenParamValueDesc = scenParamMetaData.getAllowedValueShorttext(scenParamValue);
				String paramName = scenParamName; 
				if ((scenParamValueDesc != null) && (scenParamValueDesc.length() > 0))
					configParamsDescr.put(paramName, scenParamValueDesc);
				configParams.put(paramName, scenParamValue);

			} else {
				if (compConfig != null) {
					Properties compProps =
						XCMUtils.getCompConfProps(
							compConfig,
							componentName,
							scenParamValue);
					XCMAdminComponentMetadata compMetaData = 
						XCMAdminInitHandler.getComponentMetadata(componentName);
					for (Enumeration enum = compProps.keys();
						enum.hasMoreElements();
						) {

						String compParamName = (String) enum.nextElement();
						String compParamValue = (String) compProps.getProperty(compParamName);
						String paramName = componentName + "." + compParamName;
						String convClassString = compMetaData.getConvClassName(compParamName);
						if (convClassString != null && convClassString.length() > 0) {
							compParamValue = "CONVERSION ERROR using [class]='" + convClassString + "'" ;
							Object convClassObject = ParamConverterUtil.initConvObject(convClassString);
							if (convClassObject != null) {
								if (convClassObject instanceof PasswordConverter)
									compParamValue = "****";
								else {
									if (ParamConverterUtil.isParamConverter(convClassObject))
										compParamValue = ((ParamConverter)convClassObject).decode(compParamValue);
									else {
										// 	custom decode method
										String decodeMethodName = compMetaData.getDecodeMethodName(compParamName);
										compParamValue = ParamConverterUtil.customDecode(convClassObject, decodeMethodName, componentName, "", compParamName, compParamValue); 
									}								
								}
						}			
					}  
					configParams.put(paramName, compParamValue);

					configParamsDescr.put(paramName, compMetaData.getParamShorttext(compParamName));
				}
			}
			}
		}

		int maxParamValueLength = getMaxParamLength(configParams);

		for (Iterator iter = configParams.keySet().iterator(); iter.hasNext(); ) {
			String key = (String)iter.next();
			String value = (String)configParams.get(key);
			String descr = (String)configParamsDescr.get(key);
			if (descr == null) {
				descr = "MISSING!";
				String msg = "Description for configuration [parameter]='" + key + "' missing";
				log.warn("system.ccms.warn", new Object[] { msg }, null);
			}
				
			StringBuffer sb = new StringBuffer(value);
			
/*			for (int i = sb.length(); i <= maxParamValueLength; i++) {
				sb.append(" ");
			}
*/
			sb.append(" | ");
			String cuttedValue = sb.toString() + descr; 
			if (cuttedValue.length() > 256)
				cuttedValue = (sb.toString() + descr).substring(0, 254);
				
			if (key.length() > 40)
				key = key.substring(0, 39);
			list.setConfigurationParameter(key, cuttedValue);
		}
		log.exiting();
		return list;
	}


}
