package com.sap.isa.core.jmx.ext;

import com.sap.jmx.monitoring.api.VersionInfo;

/** 
 * For testing only
 *
 */
public class XCMVersionResourceInstResource implements XCMVersionResourceInstMBean {
	
	
	public VersionInfo getVersion(String instance) {
		VersionInfo info =
			new VersionInfo(
				instance, 
				"3", 
				"6", 
				"4", 
				"4711", 
				"test");  	
		return info;
	}
	
}
