package com.sap.isa.core.jmx.ext;

import com.sap.jmx.monitoring.api.VersionInfo;

/**
 * For testing only *
 */

public interface XCMVersionResourceInstMBean 
{
	public VersionInfo getVersion(String instance);
}
