
package com.sap.isa.core.jmx.ext;

/**
 * Interface used to retrieve names of currently loaded XCM configurations
 *
 */
public interface XCMInstProviderMBean
{
  public String[] getInstances();
}
