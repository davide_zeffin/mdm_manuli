package com.sap.isa.core.jmx.ext;

import java.util.Iterator;
import java.util.Map;

import com.sap.isa.core.FrameworkConfigManager;
import com.sap.isa.core.xcm.XCMUtils;



/**
 *  Returns names of loaded XCM configurations
 */
public class XCMInstProvider implements XCMInstProviderMBean
{
	/**
	 * Returns an array of currently loaded XCM scenarios
	 * @return an array of currently loaded XCM scenarios
	 */
  public String[] getInstances()
  {
  	Map scenarios = XCMUtils.getCurrentLoadedScenarioConfigs();
  	int numScen = scenarios.size();
  	String[] names = new String[numScen];
  	int i = 0;
  	for (Iterator iter = scenarios.keySet().iterator(); iter.hasNext();) {
  		String name = (String)iter.next(); 
  		if (name.equals(FrameworkConfigManager.NAME_BOOTSTRAP_SCENARIO))
  			continue;
  		names[i] = name;
  		i++;
  	}
	return names;
  }

}
