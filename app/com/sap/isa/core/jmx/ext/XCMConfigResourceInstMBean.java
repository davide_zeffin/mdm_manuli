package com.sap.isa.core.jmx.ext;

import com.sap.jmx.monitoring.api.ConfigurationList;
import com.sap.jmx.monitoring.api.ConfigurationResourceMBean;


/**
 * Interface used for reporting of XCM parameters
 *
 */
public interface XCMConfigResourceInstMBean
  extends ConfigurationResourceMBean
{
	public ConfigurationList getConfigurationParameters(String instance);
}
