package com.sap.isa.core.jmx.ext;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Enumeration;
import java.util.Properties;

import javax.management.NotCompliantMBeanException;

import com.sap.isa.core.init.Initializable;
import com.sap.isa.core.init.InitializationEnvironment;
import com.sap.isa.core.init.InitializationHandler;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.jmx.monitoring.api.MBeanManager;
import com.sap.jmx.monitoring.api.MBeanManagerException;
import com.sap.jmx.monitoring.api.PartialRegistrationException;
import com.sap.jmx.monitoring.api.ResourceMBeanWrapper;

/**
 * Class provides basic functionality of a bootstrapper needed
 * by JMX based CCMS integration
 */
public class ExtBootstrapper implements Initializable {

	/**
	 * Class name of class providing version information
	 * This class has to implement the interface 
	 * com.sap.isa.core.util.VersionProvider
	 */
	public static final String VERSION_PROVIDER_CLASS = "versionProviderClass";

	/**
	 * Defalt collection intervall: 600s
	 */
	public static final int DEFAULT_COLLECTION_INTERVAL = 600;

	/**
	 * Prefix for init handler parameter when registering mbeans 'resourcename:'
	 * example:</br>
	 * <param name="resourcename:com.sap.isa.core.jmx.ext.MyXCMInstVersionProvider" 
	 * value="class:com.sap.isa.core.jmx.ext.XCMVersionResourceInstResource, interface:com.sap.isa.core.jmx.ext.XCMVersionResourceInstMBean"/>

	 */
	public static final String PREFIX_MBEAN_NAME = "resourcename:";

	/**
	 * Prefix for init handler parameter when registering mbeans 'class:'
	 * example:</br>
	 * <param name="resourcename:com.sap.isa.core.jmx.ext.MyXCMInstVersionProvider" 
	 * value="class:com.sap.isa.core.jmx.ext.XCMVersionResourceInstResource, interface:com.sap.isa.core.jmx.ext.XCMVersionResourceInstMBean"/>

	 */
	public static final String PREFIX_MBEAN_CLASS = "class:";


	/**
	 * Prefix for init handler parameter when registering mbeans 'interface:'
	 * example:</br>
	 * <param name="resourcename:com.sap.isa.core.jmx.ext.MyXCMInstVersionProvider" 
	 * value="class:com.sap.isa.core.jmx.ext.XCMVersionResourceInstResource, interface:com.sap.isa.core.jmx.ext.XCMVersionResourceInstMBean"/>
	 */
	public static final String PREFIX_MBEAN_INTERFACE = "interface:";
	

	/**
	 * Parameter in init handler: 
	 * Interval for collection data to CCMS system in
	 * seconds: 'data_collection_interval'
	 */
	public static final String COLLECTION_INTERVAL_PARAM =
		"ccmsDataCollectionInterval";

	private String mMonitoringGroupName =
		InitializationHandler.getApplicationName();

	protected static IsaLocation log =
		IsaLocation.getInstance(ExtBootstrapper.class.getName());

	private static final String PACKAGE =
		ExtBootstrapper.class.getPackage().getName();

	public static final String ctxMainVersionProvider =
		PACKAGE + ".MyVersionResource";
	public static final String ctxXCMInstProvider =
		PACKAGE + ".MyXCMInstProvider";
	public static final String ctxXCMApplConfigProvider =
		PACKAGE + ".MyXCMConfigProvider";

	private static final String[] ctxObjectNames =
		new String[] { ctxXCMInstProvider, ctxXCMApplConfigProvider };

	private boolean mStop = false;
	private int mColInterval = DEFAULT_COLLECTION_INTERVAL;
	protected InitializationEnvironment mEnv;


	private MBeanManager mbeanManager = new MBeanManager();

	private void init(Properties props) throws Exception {
		final String METHOD = "init()";
		log.entering(METHOD);
		log.info(
			LogUtil.APPS_COMMON_INFRASTRUCTURE,
			"system.ccms.info",
			new Object[] { "begin initializing JMX based CCMS monitoring (6.30 Engine)" });

		// XCM instance data provider
		XCMInstProvider configProvider = new XCMInstProvider();

		// XCM config provider
		XCMConfigResource xcmConfigResource = new XCMConfigResource();

		ResourceMBeanWrapper[] mbeans = null;

		try {
			mbeans =
				new ResourceMBeanWrapper[] {
					new ResourceMBeanWrapper(configProvider, XCMInstProviderMBean.class),
					new ResourceMBeanWrapper(xcmConfigResource, XCMConfigResourceInstMBean.class)};
		} catch (NotCompliantMBeanException e) {
			log.error(LogUtil.APPS_BUSINESS_LOGIC,
				"system.ccms.error",
				new Object[] { "unable to create MBeans" },
				e);
		}

		try {
			//mbeanManager.registerMBeans(ctxObjectNames, mbeans);
			mbeanManager.registerMBeans(ctxObjectNames, mbeans);
			// register further mbeans
			registerMbeans(props);
			log.info(
				LogUtil.APPS_COMMON_INFRASTRUCTURE,
				"system.eai.init.printEaiConfig",
				new Object[] { "report" });


		} catch (MBeanManagerException e) {
			log.error(LogUtil.APPS_BUSINESS_LOGIC,
				"system.ccms.error",
				new Object[] { "unable to register MBeans" },
				e);
		}

		log.info(
			LogUtil.APPS_COMMON_INFRASTRUCTURE,
			"system.ccms.info",
			new Object[] { "End initializing JMX based CCMS monitoring" });
		log.exiting();
	}

		/**
		* Called by the ISA framework. Initialized the Bootstrapper
		*
		* @param env the calling environment
		* @param props   component specific key/value pairs
		* @exception InitializeException at initialization error
		*/
		public void initialize(
			InitializationEnvironment env,
			Properties props) {

			
			final String METHOD = "initialize()";
			log.entering(METHOD);
			
			if (mMonitoringGroupName == null) {
				String msg =
					"Error initializing CCMS reporting. Context parameter 'application.name' missing in WEB-INF/web.xml. This mandatory parameter should contain the name of the web application.";
				log.info(
					LogUtil.APPS_COMMON_INFRASTRUCTURE,
					"system.ccms.error", new Object[] { msg });
				log.exiting();
				return;
			}

			String msg = null;

			if (log.isDebugEnabled())
				log.debug("Init begin");
			try {

				init(props);
			} catch (Throwable mex) {
				msg = "Error initializing JMX based CCMS monitoring";
				log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.exception", new Object[] { msg }, mex);
				return;
			} finally {
				if (log.isDebugEnabled())
					log.debug("Init end");
				log.exiting();
			}

		}

		/**
		* Called by the ISA framework. Terminates the bootrtrapper
		*/
		public void terminate() {

			final String METHOD = "terminate()";
			log.entering(METHOD);
			if (log.isDebugEnabled())
				log.debug("Terminating agent");

			// Unregister mbeans
			try {
				mbeanManager.unregisterMBeans();
			} catch (PartialRegistrationException ignored) {
				log.debug(ignored.getMessage());
				/*
				 * Ignore exceptions since MBeanManager logs with severity ERROR to the /System/Server category
				 */
			}

			if (log.isDebugEnabled())
				log.debug("Terminating agent");
			log.exiting();
		}

		/**
		 * Is called when the component name is retrieved
		 * @return String
		 */
		protected String getComponentName() {
			return "version component";
		}
		
		/**
		 * Registers additional mbeans if available
		 */
		private void registerMbeans(Properties props) {
			if (props == null)
				return;
			for (Enumeration enum = props.keys(); enum.hasMoreElements(); ) {
				String key = (String)enum.nextElement();
				String value = props.getProperty(key);
				
				if (key.indexOf(PREFIX_MBEAN_NAME) == -1)
					continue;
				
				if (value.indexOf(PREFIX_MBEAN_CLASS) == -1)
					continue;

				if (value.indexOf(PREFIX_MBEAN_INTERFACE) == -1)
					continue;
				
				int posDelim = value.indexOf(',');
				if (posDelim == -1)
					posDelim = value.indexOf(';');
				if (posDelim == -1)
					continue;
					
				String resourceName = key.substring(PREFIX_MBEAN_NAME.length(), key.length());
				
				String resourceClassName = value.substring(PREFIX_MBEAN_CLASS.length(), posDelim);
				
				String resourceInterfaceName = value.substring(posDelim + PREFIX_MBEAN_INTERFACE.length() + 2, value.length());


				if (log.isDebugEnabled()) {
					log.debug("registering custom mbean [id]='" + resourceName + "' [class]='" + resourceClassName + "'");
				}

				String msg =
					"Error instantiating mbean. Please maintain init handler property as follows: "
					+ "<param name=\"resourcename:<id of mbean used in monitoring-configuration.xml>\" value=\"class:<class name of mbean>, interface:<interface of mbean>\"/>";
				try
				{
					Class resourceClass = Class.forName(resourceClassName);
					Class resourceInterface = Class.forName(resourceInterfaceName);
					Object resource = resourceClass.newInstance();
    
					Constructor resMBeanWrapperConstructor = ResourceMBeanWrapper.class.getConstructor(new Class[]{resourceInterface});
					ResourceMBeanWrapper resourceMBeanWrapperInstance = (ResourceMBeanWrapper)resMBeanWrapperConstructor.newInstance(new Object[]{resource});

					mbeanManager.registerMBean(resourceName, resourceMBeanWrapperInstance);
				}
				catch(InvocationTargetException itex)
				{
					log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.exception", new Object[] {
						msg
					}, itex);
				}
				catch(NoSuchMethodException nsmex)
				{
					log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.exception", new Object[] {
						msg
					}, nsmex);
				}
				catch(MBeanManagerException mgrex)
				{
					log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.exception", new Object[] {
						msg
					}, mgrex);
				}
				catch(ClassNotFoundException cnfoex)
				{
					log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.exception", new Object[] {
						msg
					}, cnfoex);
				}
				catch(InstantiationException iex)
				{
					log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.exception", new Object[] {
						msg
					}, iex);
				}
				catch(IllegalAccessException iaex)
				{
					log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.exception", new Object[] {
						msg
					}, iaex);
				}
		}
	}
}