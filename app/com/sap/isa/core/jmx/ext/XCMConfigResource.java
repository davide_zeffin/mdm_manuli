package com.sap.isa.core.jmx.ext;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.w3c.dom.Document;

import com.sap.isa.core.FrameworkConfigManager;
import com.sap.isa.core.init.InitializationHandler;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.conv.ParamConverter;
import com.sap.isa.core.util.conv.ParamConverterUtil;
import com.sap.isa.core.util.conv.PasswordConverter;
import com.sap.isa.core.xcm.ConfigContainer;
import com.sap.isa.core.xcm.XCMConstants;
import com.sap.isa.core.xcm.XCMUtils;
import com.sap.isa.core.xcm.config.ComponentConfig;
import com.sap.isa.core.xcm.config.ComponentConfigContainer;
import com.sap.isa.core.xcm.init.XCMAdminComponentMetadata;
import com.sap.isa.core.xcm.init.XCMAdminParamConstrainMetadata;
import com.sap.isa.core.xcm.scenario.ScenarioManager;
import com.sap.jmx.monitoring.api.ConfigurationList;

/**
 * Reports application scope XCM configuration
 *
 */
public class XCMConfigResource
  implements XCMConfigResourceInstMBean
{
	private ConfigurationList list;
	private Map mConfigurationLists = new HashMap(); 

	private static IsaLocation log =
 						   IsaLocation.getInstance(XCMConfigResource.class.getName());


  public XCMConfigResource()
  {
		list = new ConfigurationList();
		list.setConfigurationParameter("parameter1", "valueA");
		list.setConfigurationParameter("parameter2", "valueB");
		list.setConfigurationParameter("parameter3", "valueC");
		list.setConfigurationParameter("parameter4", "valueD");
  }
  
  /**
   * Get configuration for application scope parameters
   * @returns configuration for application scope parameters
   */
  public ConfigurationList getConfigurationParameters(){
	final String METHOD = "getConfigurationParameters()";
	log.entering(METHOD);
	
	ConfigurationList appConfig = new ConfigurationList();



	ComponentConfigContainer ccc = FrameworkConfigManager.XCM.getApplicationScopeConfig();
	Set compNames = ccc.getComponentNames();
	for (Iterator iter = compNames.iterator(); iter.hasNext();) {
		String compId = (String)iter.next();
		Set configs = ccc.getComponentConfigs(compId);
		for(Iterator iter2 = configs.iterator(); iter2.hasNext();) {
			ComponentConfig compConfig = (ComponentConfig)iter2.next();
			String id = compConfig.getId();
			if (id.indexOf("default") != -1)
				continue;
			appendComponentConfig(compId, compConfig, appConfig);
		}
	}
	log.exiting();
    return appConfig;
  }

	/**
	 * Returns configuration for the passed XCM configuration name
	 * @param instance corresponds to XCM configuration name
	 */
  public ConfigurationList getConfigurationParameters(String scenName)
  {	
	final String METHOD = "getConfigurationParameters()";
	log.entering(METHOD);
	if (log.isDebugEnabled())
		log.debug("scenName="+scenName);
	Map scenarios = XCMUtils.getCurrentLoadedScenarioConfigs();
	String scenKey = null;
	for (Iterator iter = scenarios.keySet().iterator(); iter.hasNext();) {
		String name = (String)iter.next();
		if (name.equals(scenName)) {
			ConfigContainer cc = (ConfigContainer)scenarios.get(name);
			scenKey = cc.getConfigKey();
		}
	}
	
	if (scenKey == null) {
		log.exiting();
		return new ConfigurationList();	
	}
	
	if (mConfigurationLists.containsKey(scenKey)) {
		log.exiting();
		return (ConfigurationList)mConfigurationLists.get(scenKey);
 	}
	
	ConfigContainer  cc = FrameworkConfigManager.XCM.getXCMScenarioConfig(scenKey);
	String scenario = cc.getScenarioName(); 	
	ConfigurationList cl = new ConfigurationList();
	cl.setConfigurationParameter("xcm.configuration", scenario);	
	// get XCM configuration parameters
	Map scenConfigParams = ScenarioManager.getScenarioManager().getConfigParams(scenario);
	// iterate through all configuration parameter
	for (Iterator iter = scenConfigParams.keySet().iterator(); iter.hasNext();) {
		String scenParamName = (String)iter.next();
		String scenParamValue = (String)scenConfigParams.get(scenParamName);
		cl.setConfigurationParameter(scenParamName, scenParamValue);

		cl.setConfigurationParameter("xcm.configuration", scenario);		
		// get metadata for the given configuration parameter
		XCMAdminParamConstrainMetadata scenParamMetaData =
			XCMUtils.getScenarioConfigParamMetadata(scenParamName);

		// meta data missing			
		if (scenParamMetaData == null) {
			String msg = "Metadata missing in XCM for [configuration parameter]='" + scenParamName + "'";
			log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.exception", new Object[] { msg }, null);
			cl.setConfigurationParameter(scenParamName, "ERROR" + msg);
			continue;
		}
		// check if configuration dependend on component
		String componentName = scenParamMetaData.getComponent();
		if (componentName == null) {
			// report parameter value
			cl.setConfigurationParameter(scenParamName, scenParamValue);
		} else {
			// report component configuration 
			Properties compProps =
				getCompConfig(componentName, scenParamValue, cc);
			for (Enumeration enum = compProps.keys(); enum.hasMoreElements();) {
				String paramName = (String)enum.nextElement();
				String paramValue = compProps.getProperty(paramName);
				String decodedValue = decodeParam(componentName, paramName, paramValue);
				String realParamName = componentName + "." + paramName;
				if (realParamName.length() > 40)
					realParamName = realParamName.substring(0, 39);
				cl.setConfigurationParameter(realParamName, decodedValue);
			}
		}
	}
	log.exiting();
	return cl;
  }
  
  /**
   * Appends XCM configuration parameters to the JMX list
   * @param compId
   * @param compConfig
   * @param jmxList
   */
  private void appendComponentConfig(String compId, ComponentConfig compConfig, ConfigurationList jmxList) {
	// check if configuration should be shown
	if (compConfig.isForInternalUse(compConfig.getId()))
		return;
	Properties props = compConfig.getAllParams();
	if (props == null)
		return;
	for (Enumeration enum = props.propertyNames(); enum.hasMoreElements();) {
		String name = (String)enum.nextElement();
		String value = props.getProperty(name);
		String decodedValue = decodeParam(compId, name, value);
		String paramName = compId + "." + name;
		if (paramName.length() > 40)
		paramName = paramName.substring(0, 39);
		jmxList.setConfigurationParameter(paramName, decodedValue);
	}
  }
  
  /**
   * Returns the component config for the given component id
   *
   */
  private Properties getCompConfig(String componentName, String scenParamValue, ConfigContainer cc) {
  	if (cc == null)
  		return new Properties(); 			
  	Document compConfig =
		cc.getConfigUsingAliasAsDocument(XCMConstants.CONFIG_DATA_FILE_ALIAS);
		Properties compProps =
			XCMUtils.getCompConfProps(
				compConfig,
				componentName,
				scenParamValue);
  	return compProps;
  }
  
  /**
   * Checks if parameter should be decoded. 
   * @return If yes then the decoded value is 
   * returned. In case parameter is a password **** is returned 
   */
  private String decodeParam(String compId, String paramName, String paramValue) {
	XCMAdminComponentMetadata compMetaData = XCMUtils.getComponentMetadata(compId);
	String convClassString = compMetaData.getConvClassName(paramName);
	if (convClassString == null || convClassString.length() == 0)
		return paramValue;
	
	Object convClassObject = ParamConverterUtil.initConvObject(convClassString);
	if (convClassObject == null)
		return "ERROR: could not initialize [conversion class]='" + convClassString + "'";
	
	if (convClassObject instanceof PasswordConverter)
		return "*****";
	
	if (ParamConverterUtil.isParamConverter(convClassObject))
		return ((ParamConverter)convClassObject).decode(paramValue);
	
	// 	custom decode method
	String decodeMethodName = compMetaData.getDecodeMethodName(paramName);
	String decodedParamValue = ParamConverterUtil.customDecode(convClassObject, decodeMethodName, paramName, "", paramName, paramValue); 
	
  	return decodedParamValue; 
  }
}
