package com.sap.isa.core.config;

import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import com.sap.isa.core.ContextConst;
import com.sap.isa.core.FrameworkConfigManager;
import com.sap.isa.core.init.InitializationHandler;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.xcm.config.ComponentConfig;



/**
 * This class privides access to the configuration of the administration area.
 * Since this area is used by many application it is possible to turn features
 * on/off. The actual configuration can be accessed using this class
 *
 */
public class AdminConfig {

    protected static IsaLocation log =
        IsaLocation.getInstance(AdminConfig.class.getName());

	/**
	 * Key for the xcm Admin part of the admin area value: 'xcmadmin'
	 */
	public static final String KEY_XCM_ADMIN = "xcmadmin";

	/**
	 * Key for the for turning on Single Activity Trace: 'sat'
	 */
	public static final String KEY_SAT = "sat";


  /**
   * Key for the application-cache part of the admin area
   * value: 'isacorecache'
   */
  public static final String KEY_ISACORE_CACHE = "isacorecache";

  /**
   * Key for the catalog-cache part of the admin area
   * value: 'catalogcache'
   */
  public static final String KEY_CATALOG_CACHE = "catalogcache";
  /**
   * Key for the system-cache part of the application
   * value: 'corecache'
   */
  public static final String KEY_CORE_CACHE = "corecache";
  /**
   * Key for the JCo-statistics part of the application
   * value: 'jcoinfo'
   */
  public static final String KEY_JCO_INFO = "jcoinfo";
  /**
   * Key for the logging part of the application
   * value: 'logging'
   */
  public static final String KEY_LOGGING = "logging";

  /**
   * Specifies whether download of log files is supported
   * value: 'logingfiledownload'
   */
  public static final String KEY_LOGGING_FILE_DOWNLOAD = "loggingfiledownload";

  /**
   * Specifies whether download of log files is supported. Controlled by XCM
   * value: 'logfiledownload'
   */
  public static final String KEY_XCM_LOGGING_FILE_DOWNLOAD = "logfiledownload";


  /**
   * Key for the version part of the application
   * value: 'version'
   */
  public static final String KEY_VERSION = "version";

  /**
   * Key defines if application info page can be called using appinfo=true
   * request parameter
   * value: 'appinfo'
   */
  public static final String KEY_APP_INFO = "appinfo";

  /**
   * Key defines if ccms heartbeat customizing page can be called
   * request parameter
   * value: 'appinfo'
   */
  public static final String KEY_CCMS_HEARTBEAT = "ccmsheartbeat";



  /**
   * Key for the post-installtion test part of the application
   * This key specifies, that the version of JCo has to be shown
   * value: 'jcoversion'
   */
  public static final String KEY_TEST_JCO_VERSION = "jcoversion";
  /**
   * Key for the post-installtion test part of the application
   * This key specifies, that the a connection test to the SAP system
   * has to be performed
   * value: 'jcocon'
   */
  public static final String KEY_TEST_JCO_CON = "jcocon";
  /**
   * Key for the post-installtion test part of the application
   * This key specifies, that the a connection test to the IPC server
   * has to be performed
   * value: 'ipccon'
   */
  public static final String KEY_TEST_IPC_CON = "ipccon";
  /**
   * Key for the post-installtion test part of the application
   * This key specifies, that the a connection test to the database
   * has to be performed
   * value: 'jdbccon'
   */
  public static final String KEY_TEST_JDBC_CON = "jdbccon";
  
  /**
   * Key that specifies if databas migration for ISA for J2EE 
   * 6.20 to ISA for WEB AS 6.30 is supported
   * value: 'dbmig'
   */
  public static final String KEY_DB_MIG = "dbmig";

  /**
   * Key for the version part of the application
   * value: 'scheduler'
   */
  public static final String KEY_SCHEDULER = "scheduler";

  /**
   * Key for the knowledgebase List part of the admin area
   * value: 'kblist'
   */
  public static final String KEY_KBLIST = "kblist";


  /*
   * Key for Shop admin migration
   */
  public static final String KEY_SHOPADMIN = "shopadmin";
  
//  private static String mConfigString = "";
  private static AdminConfig mAdminConfig;

  private static boolean mIsIsacoreCache    = false;
  private static boolean mIsCatalogCache    = false;
  private static boolean mIsCoreCache       = false;
  private static boolean mIsJcoInfo         = false;
  private static boolean mIsLogging         = false;
  private static boolean mIsVersion         = false;
  private static boolean mIsScheduler         = false;
  private static boolean mIsPostInstallTest = false;
  private static boolean mIsLogfileDownload = false;
  private static boolean mIsAppInfo         = false;
  private static boolean mIsXCMAdmin        = false;
  private static boolean mIsCcmsHeartbeatCustomize = false;
  private static boolean mIsSAT             = false;
  private static boolean mIsDBMigration     = false;
  private static boolean mIsShopAdmin       = false;
  private static boolean mIsKBList          = false;

  /**
   * used to test class
   */
  public static void main(String[] args) {
    AdminConfig config = AdminConfig.getInstance();
    String configString = "logging, isacorecache, corecache, version, postinstalltest(jcoversion, jcocon, appinfo)";
    System.out.println("Config String: " + configString);
    AdminConfig.setConfig(configString);
    System.out.println(config.toString());
  }

  /**
   * Private construction
   */
  private AdminConfig() {
    super();
  }

  /**
   * Returns a single instace of this class
   * @return a single instance of this class
   */
  public static synchronized AdminConfig getInstance() {
    if (mAdminConfig == null)
      mAdminConfig = new AdminConfig();

    return mAdminConfig;
  }

  /**
   * Sets the string which configures the administration area.
   * The string has the following format
   * <key of admin area>, <key of admin area>, ,
   * <kea of test>(<key of test>, <key of test>, ..)
   * After the string has been set the configuration is updated
   * @param configString a string defining the configuration of the administration area
   */
  public static void setConfig(String configString) {

    if (configString.indexOf(KEY_ISACORE_CACHE) != -1)
      mIsIsacoreCache = true;
    if (configString.indexOf(KEY_CATALOG_CACHE) != -1)
      mIsCatalogCache = true;
    if (configString.indexOf(KEY_CORE_CACHE) != -1)
      mIsCoreCache = true;
    if (configString.indexOf(KEY_JCO_INFO) != -1)
      mIsJcoInfo = true;
	if (configString.indexOf(KEY_LOGGING) != -1)
	  mIsLogging = true;
    if (configString.indexOf(KEY_XCM_ADMIN) != -1)
      mIsXCMAdmin = true;
    if (configString.indexOf(KEY_VERSION) != -1)
      mIsVersion = true;
	if (configString.indexOf(KEY_SCHEDULER) != -1)
	  mIsScheduler = true;      
    if (configString.indexOf(KEY_LOGGING_FILE_DOWNLOAD) != -1)
      mIsLogfileDownload = true;
    if (configString.indexOf(KEY_APP_INFO) != -1)
      mIsAppInfo = true;
    if (configString.indexOf(KEY_CCMS_HEARTBEAT) != -1)
      mIsCcmsHeartbeatCustomize = true; 
	if (configString.indexOf(KEY_SAT) != -1)
	  mIsSAT = true; 
	if (configString.indexOf(KEY_DB_MIG) != -1)
	  mIsDBMigration = true;
	if(configString.indexOf(KEY_SHOPADMIN) != -1)
	  mIsShopAdmin = true;
    if(configString.indexOf(KEY_KBLIST) != -1)
      mIsKBList = true;
      
  }

  /**
   * Sets the authorizations for the logged in user. This user may have read-only or write rights. 
   * @param request The request from the client.
   * @return <code>true</code> if everything is allright or <code>false</code> if something went wrong. 
   * Something went wrong if the usermapping in web-j2ee-engine.xml or the web.xml has been 
   * maintained incorrectly. 
   */
  public static boolean setUserAuthorization(HttpServletRequest request) {
	  // get the different roles from the initialization process.
	  String userRoleReadOnly = request.getSession().getServletContext().getInitParameter(ContextConst.ADMIN_READ_ONLY_ACCESS_ROLE);
	  String userRoleWriteable = request.getSession().getServletContext().getInitParameter(ContextConst.ADMIN_WRITE_ACCESS_ROLE);

	  if (userRoleReadOnly == null || userRoleWriteable == null) {
		  log.debug("Roles are not maintained in web.xml");
		  return false;
	  }
	  
	  // Set the configuration in read only mode. 
	  if(request.isUserInRole(userRoleWriteable)) {
		  request.getSession().setAttribute(ContextConst.ADMIN_WRITE_ACCESS_ROLE, "true");
		  request.getSession().setAttribute(ContextConst.ADMIN_READ_ONLY_ACCESS_ROLE, "true");
		  return true;
	  } else 
	  // Set the configuration in writeable mode. 
	  if(request.isUserInRole(userRoleReadOnly)) {
		  request.getSession().setAttribute(ContextConst.ADMIN_WRITE_ACCESS_ROLE, "false");
		  request.getSession().setAttribute(ContextConst.ADMIN_READ_ONLY_ACCESS_ROLE, "true");
		  return true;
	  } else {
		  // This code will only be executed if something is wrong in the web.xml or web-j2ee-engine.xml.
		  // restrict access!!!
		  request.getSession().setAttribute(ContextConst.ADMIN_WRITE_ACCESS_ROLE, "false");
		  request.getSession().setAttribute(ContextConst.ADMIN_READ_ONLY_ACCESS_ROLE, "false");
		  return false;
	  }
  }
  
  /**
   * Returns true if the application cache area is accessible
   * @return true if the application cache area is accessible
   */
  public static boolean isIsacoreCache() {
    return mIsIsacoreCache;
  }

  /**
   * Returns true if the catalog cache area is accessible
   * @return true if the catalog cache area is accessible
   */
  public static boolean isCatalogCache() {
    return mIsCatalogCache;
  } 

  /**
   * Returns true if the system cache area is accessible
   * @return true if the system cache area is accessible
   */
  public static boolean isCoreCache() {
    return mIsCoreCache;
  }

  /**
   * Returns true if the Jco information area is accessible
   * @return true if the Jco information  area is accessible
   */
  public static boolean isJCoInfo() {
    return mIsJcoInfo;
  }

  /**
   * Returns true if the logging area is accessible
   * @return true if the logging  area is accessible
   */
  public static boolean isLogging() {
    return mIsLogging;
  }

  /**
   * Returns true if the version page is accessible
   * @return true if the version page is accessible
   */
  public static boolean isVersion() {
    return mIsVersion;
  }

  /**
   * Returns true if the scheduler page is accessible
   * @return true if the scheduler page is accessible
   */
  public static boolean isScheduler() {
	return mIsScheduler;
  }
  
  /**
   * Returns true if the post installation page is accessible
   * @return true if the post installation is accessible
   */
  public static boolean isPostInstallTest() {
    return mIsPostInstallTest;
  }

  public static boolean isShopAdmin()  {
  	return mIsShopAdmin;
  }
  /**
   * Returns true if it is allowed to download log files
   * @return true if it is allowed to download log files
   */
  public static boolean isLogfileDownload() {
	// this value can be overwritten by an XCM setting
	if (FrameworkConfigManager.XCM.isXCMActive())
		return getXCMParameter(KEY_XCM_LOGGING_FILE_DOWNLOAD, mIsLogfileDownload);  	
  	
  	
    return mIsLogfileDownload;
  }

  /**
   * Returns true if it is allowed to call the application info page
   * @return true if it is allowed to call the application info page
   */
  public static boolean isIsAppInfo() {
  
  	// this value can be overwritten by an XCM setting
	if (FrameworkConfigManager.XCM.isXCMActive())
		return getXCMParameter(KEY_APP_INFO, mIsAppInfo);  	
  	
    return mIsAppInfo;
  }

	/**
	 * Returns true if it is allowed to run the Single Activity Trace
	 * @return boolean true if it is allowed to run the Single Activity Trace
	 */
	public static boolean isSAT() {
		return mIsSAT;
	}

  /**
   * Returns true if it is allowed to call the xcm admin page
   * @return true if it is allowed to call the application info page
   */
  public static boolean isXCMAdmin() {
	return mIsXCMAdmin;
  }

  /**
   * Returns true if it is allowed to call the xcm admin page
   * @return true if it is allowed to call the application info page
   */
  public static boolean isXCMAdmin(HttpServletRequest request) {
	  if(request.getSession().getAttribute(ContextConst.ADMIN_WRITE_ACCESS_ROLE) == null) {
		  setUserAuthorization(request);
	  }
	  return request.getSession().getAttribute(ContextConst.ADMIN_WRITE_ACCESS_ROLE).equals("true");
  }

  /**
   * Returns true if it is allowed to call the xcm admin page
   * @return true if it is allowed to call the application info page
   */
  public static boolean isXCMAdmin_RO(HttpServletRequest request) {
	  if(request.getSession().getAttribute(ContextConst.ADMIN_READ_ONLY_ACCESS_ROLE) == null) {
		  setUserAuthorization(request);
	  }
	  return request.getSession().getAttribute(ContextConst.ADMIN_READ_ONLY_ACCESS_ROLE).equals("true");
  }
  
  /**
   * Check if an ccms heartbeat test should be performed
   * @return true if the ccms heartbeat customizing is accessable has to be performed
   */
  public static boolean isCcmsHeartbeatCustomize() {
    return mIsCcmsHeartbeatCustomize;
  }
  
  /**
   * Returns true, if database migration is accessible
   * @return true, if database migration is accessible
   */
  public static boolean isDBMigration() {
	return mIsDBMigration;
  }

  /**
   * Returns true if it is allowed to display the IPC Knowledgebase List
   * @return boolean true if it is allowed to display the IPC Knowledgebase List
   */
  public static boolean isKBList()  {
    return mIsKBList;
  }

  /**
   * Returns the configuration as string
   * @return the configuration as string
   */
  public String toString() {
    StringBuffer sb = new StringBuffer();

	sb.append("isXCMAdmin() " + isXCMAdmin());
    sb.append("isIsacoreCache() " + isIsacoreCache());
	sb.append("isXCMAdmin() " + isXCMAdmin());
    sb.append("isCatalogCache() " + isCatalogCache());
    sb.append("isCoreCache() " + isCoreCache());
    sb.append("isJCoInfo() " + isJCoInfo());
    sb.append("isLogging() " + isLogging());
    sb.append("isVersion() " + isVersion());
    sb.append("isAppInfo() " + isIsAppInfo());
    sb.append("isCcmsHeartbeatCustomize() " + isCcmsHeartbeatCustomize());
    sb.append("isLogfileDownload() " + isLogfileDownload());
    sb.append("isPostInstallTest() " + isPostInstallTest());
	sb.append("isDBMigration() " + isDBMigration());
	sb.append("isScheduler() " + isScheduler());
    sb.append("isKBList() " + isKBList());
    return sb.toString();
  }
	
	private static boolean getXCMParameter(String name, boolean fallback) {
		ComponentConfig cc = FrameworkConfigManager.XCM.getApplicationScopeConfig(InitializationHandler.getApplicationName() + "config");

		if (cc != null) {
			Properties props = cc.getParamConfig();
			if (props != null) {
				String jarmString = props.getProperty(name);
				if (jarmString != null) {
					if (jarmString.equalsIgnoreCase("true")) {
						return true;			
					} else
						return false;
						
				}
			}
		}
		return fallback;
	}

}