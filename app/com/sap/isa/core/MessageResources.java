/*
 * Created on Jun 14, 2004
 *
 */
package com.sap.isa.core;

import java.lang.reflect.Array;
import java.util.Locale;
import java.util.StringTokenizer;

import org.apache.struts.util.MessageResourcesFactory;
import org.apache.struts.util.PropertyMessageResourcesFactory;


/**
 * This class behaves just as it's parent class. The mayor difference is that
 * it's able to load the language resources from more than one file.
 * The configuration can contain a comma separated list with a set of resource 
 * file names.
 * The files will be searched in the order they are listed in the config string.
 * So, the resource file containing the resources which are used most often should
 * be one of the first entries in the configuration. 
 * 
 * @see org.apache.struts.util.MessageResources
 * @see org.apache.struts.util.PropertyMessageResources
 */
public class MessageResources extends org.apache.struts.util.MessageResources {
	
	org.apache.struts.util.MessageResources allResources[];
    PropertyMessageResourcesFactory propFactory;
	/**
	 * @param factory
	 * @param config
	 */
	public MessageResources(MessageResourcesFactory factory, String config) {
		super ( factory, config);
		init(factory, config, true);
	}
	
 	/**
	 * @param factory
	 * @param config
	 * @param returnNull
	 */
	public MessageResources(
		MessageResourcesFactory factory,
		String config,
		boolean returnNull) {
		super(factory, config, returnNull);
		init(factory, config, returnNull);
	}
	private void init(MessageResourcesFactory factory,
	 String config,
	 boolean returnNull)
	 {
		 String lconfig=config;
		 propFactory = new PropertyMessageResourcesFactory();
		 StringTokenizer s = new StringTokenizer(config," \t\n\r\f;,");
		 allResources = new org.apache.struts.util.PropertyMessageResources[s.countTokens()];
		 int k=0;
		 while( s.hasMoreElements())
		 {
			 lconfig = s.nextToken();
			 // The last PropertyResource Instance will
			 // create the error message, if return null is false
			 // All others will return null, if a specified resource is not
			 // available
			 boolean retNull = s.hasMoreElements();
			 if ( !retNull)
				 retNull= returnNull;
			 if(lconfig.length() != 0 )
			 	allResources[k++]= new org.apache.struts.util.PropertyMessageResources(factory, lconfig, retNull);
		 }
	 }

	/* (non-Javadoc)
	 * @see org.apache.struts.util.MessageResources#getMessage(java.util.Locale, java.lang.String)
	 */
	public String getMessage(Locale locale, String id) {
        int len=Array.getLength(allResources);
        for( int k=0; k < len;k++)
        {
        	String msg=null;
        	if( allResources[k] != null && 
        	    null !=(msg = allResources[k].getMessage(locale, id )))
        	   {
        	   	  return msg;
        	   }
        } 		
		return null;
	}
    public void setReturnNull(boolean retNull)
    {
		int len=Array.getLength(allResources);
		if( len > 0)
		{
			allResources[len-1].setReturnNull(retNull);
		}
    	
    }
}
