/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Franz M�ller
  Created:      14. November 2001

  $Revision$
  $Date:$
*****************************************************************************/

package com.sap.isa.core;


import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.StateHandlingUtil;


/**
      1. if !UserSessionData          --> initial
      2. if req-para(nextAction)      --> nextAction
      
      3. collect all the req-attributes and req-paras
      
      4. if !jsp check if default     yes --> default
                                      no  --> jsp_error
                                            
      5. --> jsp                                                      
**/


public class RefreshHandlingAction extends Action {

    /*
    private static IsaLocation log =
                           IsaLocation.getInstance(RefreshHandlingAction.class.getName());
    */
	private static final IsaLocation log =
							  IsaLocation.getInstance(RefreshHandlingAction.class.getName());


    public final ActionForward perform(ActionMapping mapping,
                 ActionForm form,
                 HttpServletRequest request,
                 HttpServletResponse response)
        throws IOException, ServletException {
                
                
        //System.out.println("RefreshHandlingAction...");
                        
		final String METHOD_NAME = "perform()";
		log.entering(METHOD_NAME);
        HttpSession session = request.getSession();


        // create an indicator object in the session scope
        UserSessionData userData = UserSessionData.getUserSessionData(session);
        if (userData == null) {
        	log.exiting();
			return mapping.findForward("initial");
        }
            
        
        String nextAction = request.getParameter("nextAction");
        if (nextAction != null) {
        	log.exiting();
			return mapping.findForward(nextAction);
        
        }
            


        int len0 = StateHandlingUtil.REQ_ATTR_PREFIX.length();
        int len1 = StateHandlingUtil.REQ_PARA_PREFIX.length();
        
        StringBuffer sb = null;
        String url = null;
        
        for (Enumeration enum=userData.getAttributeNames(); enum.hasMoreElements();) {
            String attrName = (String)enum.nextElement();
            
            if (attrName.startsWith(StateHandlingUtil.REQ_ATTR_PREFIX)) {
                String reqAttrName = attrName.substring(len0);
                
                request.setAttribute(reqAttrName, userData.getAttribute(attrName));
                
      //System.out.println("Req. Attribute["+reqAttrName+"]='" +userData.getAttribute(attrName)+"'");
                
            } else if (attrName.startsWith(StateHandlingUtil.REQ_PARA_PREFIX)) {
                String reqParaName  = attrName.substring(len1);
                String[] reqParaValue = (String[])userData.getAttribute(attrName); 
                if (sb == null) 
                    sb = new StringBuffer();
                else 
                    sb.append('&');    
                               
                for (int i=0; i < reqParaValue.length; i++) {
                    sb.append(reqParaName).append('=').append(reqParaValue[i]);
                    if (i > 0)
                        sb.append('&');
                }               
                                                       
            } else if (attrName.startsWith(StateHandlingUtil.REQ_EXEC_NAME)) {  
                url = (String)userData.getAttribute(attrName); 
            }
        }            

        
        if (url == null) {
                
           ActionForward forward = mapping.findForward("default");
           if (forward == null)           
               forward = mapping.findForward(StateHandlingUtil.NO_NEXT_EXECUTE);
           log.exiting();
           return forward;    
        }
           
        //StateHandlingUtil.removeState(userData);
      
        if (sb != null) {
           if (url.indexOf("?") > 0)
               url = url + "&" + sb.toString();
           else
               url = url + "?" + sb.toString();    
        }
		log.exiting();
        return new ActionForward(url);
    }

}