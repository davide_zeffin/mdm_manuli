/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Franz Mueller
  Created:      08 Februar 2001
*****************************************************************************/

package com.sap.isa.core;


/**
 * This is the description of all the attribute-names
 * which may appear either in the session or/and in the
 * application context.
*/


public interface SharedConst {

   /**
     *  THEME  is initially set in the <code>web.xml</code> file
     *         and may be overwritten for the useres
    */
    public static final String THEME = "theme.core.isa.sap.com";
	public static final String THEME_IC = "theme.core";

   /**
     *  THEME  is initially set in the <code>web.xml</code> file
     *         and may be overwritten for the useres
     *         This is a short version of theme. 'theme'
    */
    public static final String THEME_SHORT = "theme";



   /**
     *  SHOW_MODULE_NAME  is initially set in the <code>web.xml</code> file
     *         and may be overwritten for the useres
    */
    public static final String SHOW_MODULE_NAME = "showmodulename.core.isa.sap.com";
	public static final String SHOW_MODULE_NAME_IC = "showmodulename.core";

    /**
     *  SHOW_JSP_DEBUG_IC is a configuration value in the XCM Interaction component "UI".
     *  With this parameter it's possible to enable displaying of debug information directly on the JSP's.
     *  (Additionally to this XCM configuration value the request parameter "showmodulename" need to be set to "true".)
    */
	public static final String SHOW_JSP_DEBUGMSG = "showjspdebugmsg.core.isa.sap.com";
	public static final String SHOW_JSP_DEBUGMSG_IC = "showjspdebugmsg.core";

	/**
	 *  STYLE_DIRECTION_IC is a configuration value in the XCM Interaction component "UI".
	 *  With this parameter it's possible to define ISO languages which need the screen
	 *  alignment to the right.
	*/
	public static final String STYLE_DIRECTION = "styledirection.core.isa.sap.com";
	public static final String STYLE_DIRECTION_IC = "styledirection.core";
	
    /**
     *  SECURE_HANDLING is set in the <code>web.xml</code> file. The value has to be 'ON'.
     *  Is only required when a web application has to support SSL and
     *  a third-party Webserver is active and handles the SSL.
     *
     * In the session context it represents the key for storing the isSecure - information.
     * Is only applicable if also set in the web.xml
     */

    public static final String SECURE_HANDLING = "secure.handling.core.isa.sap.com";

    /**
     *  SECURE_HANDLER 
     */

    public static final String SECURE_HANDLER = "secure.handler.core.isa.sap.com";


    /**
     * constant indicating the path to the configuration file
     * of the Extended Configuration Management
     */
	public static final String XCM_PATH = "path.xcm.config.isa.sap.com";        
	
    /**
     * Constant used to retrieve the key of the currently used configuration
     * It can for example be used when storing configuration dependend data in caches.
     */
    public static final String XCM_CONF_KEY = "confkey.xcm.core.isa.sap.com";
	

	/**
	 * Constant used to store a reference of the SAT Monitor
	 * This parameter is used a session and request attribute
	 */
	public final static String SAT_MONITOR = "sat.monitor.core.isa.sap.com";
	
	/**
	 * Constant used to store the SAT Checker in the session context
	 */
	public final static String SAT_CHECKER = "sat.checker.core.isa.sap.com"; 	

	/**
	 * This request/session parameter is used to store the session timeout callback URL
	 */
	public final static String CALLBACK_URL= "callbackurl";

	/**
	 * This parameter can be used as request attribute to temporariliy disable the 
	 * HttpServletRequestFacade to wrap the Http request. The parameter value must be 
	 * != null
	 */
	public final static String DISABLE_I18N_ENCODING = "disablei18nencoding";
}