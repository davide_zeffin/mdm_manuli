package com.sap.isa.core.interaction;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


/**
 * This class acts as an container for configuration data which 
 * can be used in the interaction layer
 */
public class InteractionConfig {

	private Map mParams = new HashMap();
	private String mId;
	private static String CRLF = System.getProperty("line.separator");
	
	/**
	 * Sets the id of this configuration
	 * @param id id of this configuration
	 */
	public void setId(String id) {
		mId = id;
	}
	
	/**
	 * Adds parameter to configuration
	 * @param name parameter name
	 * @param value parameter value
	 */
	public void addParam(String name, String value) {
		mParams.put(name, value);	
	}
	
	/**
	 * Returns the id of this configuration data
	 * @return the name of this configuration data
	 */
	public String getId() {
		return mId;	
	}
	
	/**
	 * Returns the value of a parameter for the given name
	 * If there is no parameter with the given name <code>null</code>
	 * is returned
	 * @param name parameter name
	 * @return the value of the parameter 
	 */
	public String getValue(String name) {
		return (String)mParams.get(name);
	}
	
	/**
	 * Retunrs an unmodifiable MAP of all parameters stored by this
	 * configuration
	 * @return an unmodifiable MAP of all parameters stored by this
	 * configuration
	 */
	public Map getParams() {
		return Collections.unmodifiableMap(mParams);
	}
	
	/** 
	 * Returns a string representation of this configuration
	 * @return a string representation of this configuration
	 */
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("Interaction configuration [id]='").append(mId).append("'").append(CRLF);
		for (Iterator iter = mParams.keySet().iterator(); iter.hasNext();) {
			String name = (String)iter.next();
			String value = (String)mParams.get(name);
			sb.append("[name]='").append(name).append("'").append(CRLF);
			sb.append("[value]='").append(value).append("'").append(CRLF).append(CRLF);			
		}						
		return sb.toString();	
	}
}
