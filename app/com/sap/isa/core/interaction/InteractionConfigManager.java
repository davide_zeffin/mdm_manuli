package com.sap.isa.core.interaction;

// java imports
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.digester.Digester;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.xcm.ConfigContainer;
import com.sap.isa.core.xcm.init.ExtendedConfigInitHandler;

/**
 * This class manages configuration data for the interaction layer
 */
public class InteractionConfigManager {

    protected static IsaLocation log = IsaLocation.
        getInstance(InteractionConfigManager.class.getName());


	private static final String mFileName = 
		ExtendedConfigInitHandler.getFilesConfig().getPath("interaction-config");


	private static Map mConfigs = new HashMap();
	
	/**
	 * Retrieves the configuration from the config container
	 * and parses it, if necessary
	 */
	public static InteractionConfigContainer getInteractionConfig(ConfigContainer cc) {
		final String METHOD = "getInteractionConfig()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("configKey="+cc.getConfigKey());
	 	// get current configuration
		String key = cc.getConfigKey();

		InteractionConfigContainer config = null;
		
		synchronized (mConfigs) {
		
			if (mConfigs.containsKey(key)) {
				log.exiting();			
				return (InteractionConfigContainer)mConfigs.get(key);
			}
			InputStream is = cc.getConfigAsStream(mFileName);
				
			config = readFileConfig(is);
	
			if (config != null)
				mConfigs.put(key, config);
		}
		log.exiting();
		return config;	
	}
	
	/**
	 * reads the interaction config and 
	 */
	/**
	 * Returns a container configuration of files which are processed
	 * by the Extended Configuration Management
	 */
	private static InteractionConfigContainer readFileConfig(InputStream is)  {

		if (log.isDebugEnabled())
			log.debug("reading Interaction Configuration file");

      // new Struts digester
      Digester digester = new Digester();

	  InteractionConfigContainer container = new InteractionConfigContainer();
	
      digester.push(container);
      
      digester.setDebug(0);

    // create a new interaction configuration
    digester.addObjectCreate("configs/config",
        "com.sap.isa.core.interaction.InteractionConfig");

     // set property (id) of interaction configuration
     digester.addSetProperties("configs/config");

	 // add parameters to container 
      digester.addCallMethod(
          "configs/config/params/param",
          "addParam", 2, new String[]
              {"java.lang.String", "java.lang.String"});

		// add property name
      digester.addCallParam("configs/config/params/param"
          , 0, "name");
      // add path
      digester.addCallParam("configs/config/params/param"
          , 1, "value");

	// add configuration parameter to container
    digester.addSetNext("configs/config",
        "addConfig", "com.sap.isa.core.interaction.InteractionConfig");


      try {
        if (is != null)
          digester.parse(is);

        } catch (Exception ex) {
        	log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.config.error.readfile\n" + ex.toString() );
      } finally {
      	try {
      		is.close();
      	} catch (IOException ioex) {
      		log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.config.error.readfile", new Object[] { }, ioex);
    	}
      }

      return container;
	}
}
