package com.sap.isa.core.interaction;

// java imports
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Stores multiple interaction configs
 */ 
public class InteractionConfigContainer {

	private Map mConfigs = new HashMap();
	private static String CRLF = System.getProperty("line.separator");
	
	/**
	 * Adds a configuration to the container
	 * @param config 
	 */
	public void addConfig(InteractionConfig config) {
		if (config == null)
			return;
		mConfigs.put(config.getId(), config);
	}	

	/**
	 * Returns a configuration for the given id
	 * If no configuration for the given id is available <code>
	 * null</code> is returned
	 * @param id the id of the configuration
	 * @return interaction configuration
	 */
	public InteractionConfig getConfig(String id) {
		if (id == null)
			return null;
		return (InteractionConfig)mConfigs.get(id);
	}	
	
	/** 
	 * Returns a string representation of this configuration container
	 * @return a string representation of this configuration
	 */
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("Interaction configuration:").append(CRLF);
		for (Iterator iter = mConfigs.values().iterator(); iter.hasNext();) {
			InteractionConfig config = (InteractionConfig)iter.next();
			sb.append(config.toString());			
		}						
		return sb.toString();	
	}	
	
	
}
