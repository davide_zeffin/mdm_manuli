/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Franz Mueller
  Created:      08 Februar 2001
*****************************************************************************/

package com.sap.isa.core;

/**
 * This is the description of all the attribute-names
 * which may appear in the session context.
 */
public interface SessionConst extends SharedConst {


    /**
     * String constant to store the business event manager in the session
     * context. Use this and <b>only</b> this constant to retrieve the
     * object. Never rely on the actual string used in this
     * constant, it may change without notice.
     */
    public static final String BUSINESS_EVENT_HANDLER =
                                      "BusinessEventHandler.isa.sap.com";

    /**
     * String constant to store the business object manager in the session
     * context. Use this and <b>only</b> this constant to retrieve the
     * MetaBusinessObjectManager object. Never rely on the actual string used
     * in this constant, it may change without notice.
     */
    public static final String MBOM_MANAGER =
             "MetaBusinessObjectManager.businessobject.core.isa.sap.com";

    /**
     * String constant to store the document handler in the session context.
     * Use this and <b>only</b> this constant to retrieve the
     * DocumentHandler object. Never rely on the actual string used in this
     * constant, it may change without notice.
     */
    public static final String DOCUMENT_HANDLER =
                                   "DocumentHandler.isacore.isa.sap.com";

    /**
     *  USER_DATA identifies the <code>UserSessionData</code> object in the
     *            session scope.
     */
    public static final String USER_DATA =
                                      "UserSessionData.core.isa.sap.com";

    /**
     *  LOG_CATEGORY used in the logging taglibs to determine and store the
     *               current logging Category that is going to be used.
     *               If there is none available, the JSP->Category Mapping will
     *               be used to evaluate the current Category
     */
    public static final String LOG_CATEGORY =
                                             "Category.core.isa.sap.com";


    /**
     *  KEY_CONCAT_MSG used through the translation tag. When an object
     *                with this key exists in the session scope,
     *                the key of the message will be concatinated to
     *                the message. Format: key:message
     */
    public static final String KEY_CONCAT_MSG =
                                "concat.key.show.translate.core.isa.sap.com";

    /**
     *  KEY_ONLY_MSG used through the translation tag. Instead of the
     *                message the key itself will be display, when
     *                an object in the session scope is stored under this
     *                key. If both, this and the KEY_CONCAT_MSG exists
     *                in the session scope, KEY_ONLY_MSG will be prefered.
     */

    public static final String KEY_ONLY_MSG =
                                "only.key.show.translate.core.isa.sap.com";


    /**
     * Constant used to retrieve the id of the currently used backend configuration
     * from the session context
     * Note: This is only valid if Extended Configuration Management is turned off
     */
    public static final String EAI_CONF_ID = ContextConst.EAI_CONF_ID;

    /**
     * Constant used to retrieve the id of the data used by the currently active
     * backend configuration
     * Note: This is only valid if Extended Configuration Management is turned off
     */
    public static final String EAI_CONF_DATA = ContextConst.EAI_CONF_DATA;

    /**
     * Constant used to retrieve the key of the curretnly used backend configuration
     * This key is an identifier for a backned configuration. It can for example
     * be used when storing configuration dependend data in caches.
     * Note: This is only valid if Extended Configuration Management is turned off
     */
    public static final String EAI_CONF_KEY = "confkey.eai.core.isa.sap.com";

 
    /**
     * Constant used to retrieve the values of startup parameter.
     */
    public final static String STARTUP_PARAMETER = "startupParameter";
  
 	/**
 	 * Constant used to retrieve the currently used interaction configuration
 	 * The object passed from the user session data is of type
 	 * <code>InteractionConfigContainer</code>
 	 * Note: This is only valid if Extended Configuration Management is turned on
 	 */
 	public final static String INTERACTION_CONFIG = "interaction.core.isa.sap.com";
 
	/**
 	 * Constant used to retrieve the currently used configuration container
 	 * The object passed from the user session data is of type
 	 * <code>ConfigContainer</code>
 	 * Note: This is only valid if Extended Configuration Management is turned on
 	 */
 	public final static String XCM_CONFIG = "config.core.isa.sap.com";
 	

	/**
	 * Constant used to retrieve the values of startup parameter.
	 */
	public final static String REINVOKE_CONTAINER = "reinvoke.context.ui.core.sap.com";
 	
}