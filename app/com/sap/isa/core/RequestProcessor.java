package com.sap.isa.core;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.config.ForwardConfig;
import org.apache.struts.config.ModuleConfig;
import org.apache.struts.util.MessageResources;

import com.sap.isa.core.businessobject.event.UnsecureConnectionEventHandler;
import com.sap.isa.core.init.InitializationHandler;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.IsaSession;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.logging.sla.IsaLocationSla;
import com.sap.isa.core.sat.jarm.ISASATCheck;
import com.sap.isa.core.system.RequestContextContainer;
import com.sap.isa.core.ui.context.ContextManager;
import com.sap.isa.core.ui.context.ReInvokeContainer;
import com.sap.isa.core.ui.layout.GlobalUILayoutHandler;
import com.sap.isa.core.ui.layout.UIArea;
import com.sap.isa.core.ui.layout.UICommand;
import com.sap.isa.core.ui.layout.UICommandReturnValue;
import com.sap.isa.core.ui.layout.UIComponent;
import com.sap.isa.core.ui.layout.UILayout;
import com.sap.isa.core.ui.layout.UILayoutInitHandler;
import com.sap.isa.core.ui.layout.UILayoutManager;
import com.sap.isa.core.util.GenericFactory;
import com.sap.isa.core.util.HttpServletRequestFacade;
import com.sap.isa.core.util.SecureHandler;
import com.sap.isa.core.util.VersionGet;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.core.xcm.config.ComponentConfig;
import com.sap.tc.logging.Category;
import com.sap.util.monitor.jarm.IMonitor;
import com.sap.util.monitor.jarm.TaskMonitor;
import com.sap.util.monitor.jarm.sat.SatCheckHandler;


/**
 * The class RequestProcessor extends the {@link org.apache.struts.action.RequestProcessor}
 * class to provide ECommerce specific feature like Framesless and Browser Back Support
 * and other extensions. <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class RequestProcessor	extends org.apache.struts.action.RequestProcessor {
		
	protected static final String REINVOKE_PORTAL = "ReInvokePortal";

	protected static final String REINVOKE = "ReInvoke";

	protected static final String SEND_RESPONSE = "SendResponse";

	MessageResources resources=IsaLocationSla.getMessageResources();
	
	protected static IsaLocation log =
			IsaLocation.getInstance(org.apache.struts.action.RequestProcessor.class.getName());

	/**
	  * If set to true in web.xml, action will be intercepted before and after the
	  * execution of perform method in BaseAction
	  */
	protected static boolean decodeInterceptor = true;
	
	protected final static String CRLF = System.getProperty("line.separator");

	protected final static String FORWARD_ACTION = "/core/forwardToAction.do";

	protected Map uiCommands = new HashMap(10);
	
	protected static SecureConnectionChecker secureConnectionChecker = createSecureConnectionChecker();
	
	/**
	 * If this property was set, the Forward name calculated in an action 
	 * will be stored in the request attribute  com.sap.isa.core.RequestProcessor.forwardName
	 */
	private static boolean storeResultForwardName=false;

	/**
	 *  this attribut determins whether the request parameter are already encode or not
	 *  If this attribute is available in the request context, that the parameter are
	 *  encoded.
	 */
	public static final String ENCODED_REQ = "encoded.request.core.isa.sap.com";

	
	/**
	 * Name to store the orginal servlet path in the request. 
	 */
	protected static final String ORIGNAL_SERVLETPATH = "original.action.core.isa.sap.com";


	/**
	 * Name to store the names of the extended request attributes in the request. 
	 */
	protected static final String EXTENDED_REQUEST_ATTRIBUTES = "extended.attributes.core.isa.sap.com";
	/**
	 * Name to store the forward name 
	 */
	public static final String FORWARD_NAME_REQUEST_ATTRIBUTE = "com.sap.isa.core.RequestProcessor.forwardName";

	
    /**
     * Standard constructor. <br>
     * 
     */
    public RequestProcessor() {
        super();
		initUICommands();
    }



    /** 
	 *  Writes an exception stacktrace into the response. <br>
	 *
	 * @param  response
	 * @param  ex
	 * @exception  IOException  Description of Exception
	 */
	public static void writeException(HttpServletRequest request,HttpServletResponse response, Throwable ex) throws IOException {

		// If the response is already committed no error will be written!
		if (response.isCommitted()) {
			return;
		}
		
		if (false) {
			PrintWriter out = response.getWriter();
			out.println("<html><body><h3>ISA Framework: Internal Error: "+ ex.toString() + "</h3><p><pre>");
	    
			String showStacktraceOn = FrameworkConfigManager.Servlet.getInteractionConfigParameter(request.getSession(),"ui", ContextConst.IC_SHOW_STACK_TRACE, ContextConst.IC_SHOW_STACK_TRACE + ".isa.sap.com");
			if ((showStacktraceOn != null) && showStacktraceOn.equalsIgnoreCase("true")) {
				ex.printStackTrace(out);
			}
			out.println("</pre></body></html>");
		}

		StringBuffer msg = new StringBuffer();
		if (ex instanceof ServletException && ((ServletException)ex).getRootCause() instanceof FrameworkException) {
			msg.append("ISA Framework: "+ ex.getMessage());
		}
		else {
			msg.append("ISA Framework: "+ ex.toString());
			String showStacktraceOn = FrameworkConfigManager.Servlet.getInteractionConfigParameter(request.getSession(),"ui", ContextConst.IC_SHOW_STACK_TRACE, ContextConst.IC_SHOW_STACK_TRACE + ".isa.sap.com");
			if ((showStacktraceOn != null) && showStacktraceOn.equalsIgnoreCase("true")) {
				msg.append(CRLF).append(CRLF);
				StackTraceElement[] elements = ex.getStackTrace();
				for (int i = 0; i < elements.length; i++) {
	                msg.append(elements[i].toString()).append(CRLF);
	            }	
			}
		}
		response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
				 msg.toString());
	}


	/**
	 * Store a value as an extended request attribute. <br>
	 * An extended request attribute will be available after an http redirect.
	 * 
	 * @param request current request
	 * @param name name of the attribute
	 * @param value value of the attribute
	 */
	static public void setExtendedRequestAttribute(HttpServletRequest request, String name, Object value) {
		
		getExtendedRequestAttributeNames(request).add(name);
		request.setAttribute(name, value);
	}

	
	/**
	 * Store the name of a extended request attribute. <br>
	 * An extended request attribute will be available after an http redirect.
	 * 
	 * @param request current request
	 * @param name name of the attribute
	 * @param value value of the attribute
	 */
	static public void addExtendedRequestAttributeName(HttpServletRequest request, String name) {

		getExtendedRequestAttributeNames(request).add(name);
	}		

	
	/**
	 * Return the list with the name of the extended request attributes. <br>
	 * 
	 * @param request current request
	 * @return list with the names
	 */	
	static protected List getExtendedRequestAttributeNames(HttpServletRequest request) {
		
		List names = (List)request.getAttribute(EXTENDED_REQUEST_ATTRIBUTES);
		if (names == null) {
			names = new ArrayList();
			request.setAttribute(EXTENDED_REQUEST_ATTRIBUTES,names);
		}
		return names;	
	}

	
	void init( ActionServlet servlet, ModuleConfig moduleConfig )
	{
	// ignore I18N
		String value = servlet.getInitParameter("decodeInterceptor");
		if (value != null) {
			if ("false".equalsIgnoreCase(value.trim()))
			   decodeInterceptor = false;
		}
		resources=com.sap.isa.core.logging.sla.IsaLocationSla.getMessageResources();
		
	}



	/**
	  * Render the HTTP headers to defeat browser caching if requested.
	  *
	  * Expires with the value "0" means "already expires"
	  *  rfc 2616/http1.1  Chapter 14.21
	  *
	  * @param response The servlet response we are creating
	  *
	  * @exception IOException if an input/output error occurs
	  * @exception ServletException if a servlet exception occurs
	  */
	 protected void processNoCache(HttpServletResponse response)
		 throws IOException, ServletException {
		 	
		 response.setHeader("Pragma", "no-cache");
		 response.setHeader("Cache-Control", "no-cache");
		 response.setHeader("Expires", "0");
	 }

	/**
	 * Forward to the specified destination, by the specified mechanism,
	 * if an <code>ActionForward</code> instance was returned by the
	 * <code>Action</code>.
	 * If an instance of <code>ActionForward</code> could not be associated,
	 * this method reports this back (HTTP 500) to the requester.
	 *
	 * @param forward The ActionForward returned by our action
	 * @param mapping The ActionMapping we are processing
	 * @param formInstance The ActionForm we are processing
	 * @param request The servlet request we are processing
	 * @param response The servlet response we are creating
	 *
	 * @exception IOException if an input/output error occurs
	 * @exception ServletException if a servlet exception occurs
	 */
	protected void processForwardConfig( HttpServletRequest request,
										 HttpServletResponse response,
										 ForwardConfig forward)
		throws IOException, ServletException {
		if (forward != null) {
			
		   log.info(Category.SYS_LOGGING,"system.forward.info",
					   new String[] {
						  "forward-path[<"  + forward.getPath() + ">]"
					   });

		   if (!forward.getPath().equals(SEND_RESPONSE)) {
			   super.processForwardConfig(getOriginalRequest(request),
										  response, forward);
		   }
		} else {

			log.error(LogUtil.APPS_USER_INTERFACE_CONTROLLER,"system.forward.missing");

			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
					 resources.getMessage("system.forward.missing"));
		}
	}


	private HttpServletRequest determinRequest(HttpServletRequest  request,HttpServletResponse response) 
			throws IOException {

		Object skip = request.getSession().getAttribute(ActionServlet.ENCODING_SKIP);

		if (skip != null)
			return request;


		if (request.getParameter(SharedConst.DISABLE_I18N_ENCODING) != null)
			return request;

		Object obj = request.getAttribute(ENCODED_REQ);

		if (decodeInterceptor) {
			if (request.getParameterNames().hasMoreElements()) {
			   request = HttpServletRequestFacade.determinRequest(request);
			}
		}

		if (obj == null) {
		   printRequest(request, response);

		   request.setAttribute(ENCODED_REQ,
							(request instanceof HttpServletRequestFacade)?"ENCODE":"FM");
		}

		return request;
	}


	/**
	 * Befor handling the execution to the struts.ActionServlet
	 * this method sets the <strong>sessionId</strong> in the ThreadLocal
	 * of the current thread. This enables to log the sessionId through all
	 * the application layers.
	 *
	 * @param request The servlet request we are processing
	 * @param response The servlet response we are creating
	 *
	 * @exception IOException if an input/output error occurs
	 * @exception ServletException if a servlet exception occurs
	 */
	public void process(HttpServletRequest  request,
						   HttpServletResponse response)
		throws IOException, ServletException {

		HttpSession session = request.getSession();
    
		if (request.getAttribute(RequestContext.REQUEST_CONTEXT) == null) {
			RequestContextContainer.getInstance().init();
		
			if (log.isDebugEnabled())
				log.debug("Creating new request context [objid]='" + RequestContextContainer.getInstance().getRequestContext() + "'");
			request.setAttribute(RequestContext.REQUEST_CONTEXT, RequestContextContainer.getInstance().getRequestContext());
		} 
	 
		if (session != null) {
			 IsaSession.setThreadSessionId(session.getId());
		} else {
			 IsaSession.setThreadSessionId(null);
		}

		HttpServletRequest  req = determinRequest(request,response);
		IsaSession.setAttribute("http_request", req);

		restoreExtendedRequestAttributes(req);
		
		try {

			IMonitor monitor = (IMonitor)request.getAttribute(SharedConst.SAT_MONITOR);
			
			boolean firstReq = false;
			
			String reqName = null;
			String sessionId = null;
			// turn Single Activity Trace on
			if (ActionServlet.getTheOnlyInstance() != null && (ActionServlet.getTheOnlyInstance().mIsJarm || (ActionServlet.getTheOnlyInstance().mIsSat) && request.getParameter(Constants.SAT) != null) && monitor == null) {
				// turn SAT on
				reqName = VersionGet.getSATRequestPrefix() + request.getRequestURI();
				ISASATCheck isaSATCheck = new ISASATCheck();
				sessionId = request.getSession().getId();
				ISASATCheck.setJarmOn(request.getSession().getId());
				if (request.getParameter(Constants.SAT) != null) {
					ISASATCheck.setTraceOn(request.getSession().getId());
					SatCheckHandler.registerSatCheck(isaSATCheck);
				}
				monitor = TaskMonitor.getRequestMonitor(sessionId, reqName);
				request.setAttribute(SharedConst.SAT_MONITOR, monitor);
				//request.setAttribute(SharedConst.SAT_CHECKER, isaSATCheck);
				// store checker in sesion context. This object will not be deleted by init action
				request.getSession().setAttribute(SessionConst.SAT_CHECKER, isaSATCheck);				
				firstReq = true;
				if (log.isDebugEnabled()) {
					log.debug("Single Activity Trace enabled for [request]='" + reqName + "' and [sessionid]='" + sessionId + "'"); 
				}
			}
			
			 
				// first request in a session with SAT or JARM turned on
			if (!firstReq && (monitor == null) && (request.getSession().getAttribute(SessionConst.SAT_CHECKER) != null)) {
				firstReq = true;
				reqName = VersionGet.getSATRequestPrefix() + request.getRequestURI();
				sessionId = request.getSession().getId();
				monitor = TaskMonitor.getRequestMonitor(sessionId, reqName);
				request.setAttribute(SharedConst.SAT_MONITOR, monitor);				
			}


			// TEST trace JARM
			IsaSession.setAttribute(SharedConst.SAT_MONITOR, monitor);

			super.process(req, response);
            
			// if this is the first request and SAT is enabled end the request
			if (firstReq && monitor != null) {
				monitor.endRequest(reqName);
			}
					                        
        
		} catch (Throwable exception) {
			if(log.isDebugEnabled()) {
			   log.error(LogUtil.APPS_USER_INTERFACE_CONTROLLER,"system.exception", exception);
			} else {
				exception.printStackTrace();
			}
			writeException(request, response, exception);
		} finally {
			// release the sessionid to avoid later logging problems
			IsaSession.releaseThreadSessionId();

			IsaSession.setAttribute("http_request", null);
		}
	}


	/**
	 * Identify and return the path component (from the request URI) that
	 * we will use to select an ActionMapping to dispatch with.  If no such
	 * path can be identified, create an error response and return
	 * <code>null</code>.
	 *
	 * @param request The servlet request we are processing
	 * @param response The servlet response we are creating
	 *
	 * @exception IOException if an input/output error occurs
	 */
	protected String processPath(HttpServletRequest request,
								 HttpServletResponse response)
		throws IOException {

		String path= super.processPath(request, response);
		
		adjustOriginalServletPath(request);
		
		String ret = WebUtil.decodeContextValueFromURL(path,true,request);
		
		return ret;
	}


	private HttpServletRequest getOriginalRequest(HttpServletRequest request) {

		if (request instanceof HttpServletRequestFacade) {
			return ((HttpServletRequestFacade)request).getHttpServletRequest();
		}

		return request;
	}

	/**
	 * Return servlet path which starts the request. <br>
	 * 
	 * @param request
	 * @return
	 */
	private String getOriginalServletPath(HttpServletRequest request) {

		String path = (String)request.getAttribute(ORIGNAL_SERVLETPATH);

		return path;
	}

	/**
	 * Adjust servlet path which starts the request. <br>
	 * 
	 * @param request
	 * @return
	 */
	private void adjustOriginalServletPath(HttpServletRequest request) {

		String originalPath = (String)request.getAttribute(ORIGNAL_SERVLETPATH);
		if (originalPath == null) {
			request.setAttribute(ORIGNAL_SERVLETPATH,request.getServletPath());
		}

	}


	private void saveExtendedRequestAttributes(HttpServletRequest request) {
		
		UserSessionData userData =
			UserSessionData.getUserSessionData(request.getSession());

		if (userData != null) {
			List names = getExtendedRequestAttributeNames(request);
			Map map = new HashMap(names.size());
			Iterator iter = names.iterator();
			while (iter.hasNext()) {
                String name = (String) iter.next();
                map.put(name, request.getAttribute(name));
            } 
			if (map.size()>0) {
				userData.setAttribute(EXTENDED_REQUEST_ATTRIBUTES,map);		
			}
		}
	}

	
	private void restoreExtendedRequestAttributes(HttpServletRequest request) {
		UserSessionData userData =
			UserSessionData.getUserSessionData(request.getSession());

		if (userData != null) {
			Map map = (Map)userData.getAttribute(EXTENDED_REQUEST_ATTRIBUTES);		
			if (map != null) {
				userData.removeAttribute(EXTENDED_REQUEST_ATTRIBUTES);
				Iterator iter = map.entrySet().iterator();
				while (iter.hasNext()) {
                    Map.Entry element = (Map.Entry) iter.next();
                    if (request.getAttribute((String)element.getKey()) == null) {
                    	request.setAttribute((String)element.getKey(),element.getValue());
                    }	
                }
			}
			
		}
	}


	protected boolean processInclude(HttpServletRequest request,
									 HttpServletResponse response,
									 ActionMapping mapping)
		throws IOException, ServletException {
	
		return super.processInclude(getOriginalRequest(request), response, mapping);
	}
	
	/*
	*   This method logs the request. The request
	*/
	private void printRequest(HttpServletRequest request, HttpServletResponse response) throws IOException {
	
		 if(!log.isDebugEnabled())
			return;
	
		 StringBuffer sb = new StringBuffer();
		 sb.append(processPath(request,response)).append(".do").append(CRLF);
		 Enumeration names = request.getParameterNames();
		 while (names.hasMoreElements()) {
			  String name     = (String)names.nextElement();
	
			if (name.toLowerCase().startsWith("nolog_") || (name.indexOf(Constants.HTMLB) != -1)) {
				 sb.append(name).append('=').append("--hidden--");
				 sb.append(CRLF);
				 continue;
			  }
	
			  String[] values = request.getParameterValues(name);
	
			  for (int i=0; i < values.length; i++) {
				   sb.append(name).append('=');
				   sb.append(WebUtil.toUnicodeEscapeString(values[i]));
				   if (i+1 < values.length)
					  sb.append(CRLF);
			  }
	
			  sb.append(CRLF);
		 }
	
		 log.debug(sb.toString());
	}
	

	/**
	*
	*   This method completes the BaseAction's analysis for session validaty.
	*   If there is not an UserSessionData object in the session scope it
	*   returns <code>true</code> and transfers the handling of this inconsistency
	*   to the BaseAction.
	**/
	protected boolean processValidate(HttpServletRequest request,
									  HttpServletResponse response,
									  ActionForm formInstance,
									  ActionMapping mapping)
		throws IOException, ServletException {

		if (null == formInstance)
		   return true;

		if (null == UserSessionData.getUserSessionData(request.getSession()))
		   return true;

		return super.processValidate( getOriginalRequest(request), response, formInstance, mapping);
	}


    /**
     * Overwrites the method processForward method. <br>
     * The method have two main tasks.<br>
     * It will replace the used request facade with the original request.
     * It handles UI commands in the <code>forward</code> attribute.
     * 
     * @param request current http request
     * @param response http repsonse
     * @param mapping action mapping
     * @return
     * @throws IOException
     * @throws ServletException
     * 
     * 
     * @see org.apache.struts.action.RequestProcessor#processForward(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, org.apache.struts.action.ActionMapping)
     */
    protected boolean processForward(HttpServletRequest request,
									   HttpServletResponse response,
									   ActionMapping mapping)
			throws IOException, ServletException {
	
	
		String forward = mapping.getForward();
	
		if (forward != null) {		
			 				
			// check invalid session 					
			if (isSessionInvalid(request) && isUICommand(forward)) {
				forward = mapping.findForward(Constants.SESSION_NOT_VALID).getPath();
			}
			 		
            boolean redirect = false;		
			String newForward = forward;
            	 				
			UICommandReturnValue retValue = handleConfigCommands(forward, redirect, request);
			
			if (retValue != null) {
				newForward = retValue.getForward();
				redirect = retValue.isRedirect();							
			}
			else {
				// check re invoke command
				String 	newPath = handleReInvokeCommands(forward, request, mapping);
				if (newPath!= null) {
					newForward = newPath;
					redirect = getRedirectForReinvoke(); 
				}
			}
		
			if (redirect) {
				handleRedirect(request);
				newForward = WebUtil.getAppsURL(servlet.getServletConfig().getServletContext(),
						request,response,null,newForward,null,null,false);
				response.sendRedirect(newForward);
				return (false);	
			}
		
			internalModuleRelativeForward(newForward, getOriginalRequest(request), response);
			return (false);
		}		
		return true;
	}



    /**
     * Overwrites the method processActionPerform. <br>
     * The method handles the UI Commands it the <code>path</code> attribute of the 
     * <code>forward</code> tag in the config.xml files. 
     * 
     * @param request current request
     * @param reponse repsonse
     * @param action current action
     * @param form current action form
     * @param mapping action mapping
     * @return the logical forward given by the action or from the UI Command.
     * @throws IOException
     * @throws ServletException
     * 
     * @see org.apache.struts.action.RequestProcessor#processActionPerform(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, org.apache.struts.action.Action, org.apache.struts.action.ActionForm, org.apache.struts.action.ActionMapping)
     */
    protected ActionForward processActionPerform(HttpServletRequest request,
        										  HttpServletResponse reponse,
                                                  Action action,
        										  ActionForm form,
        										  ActionMapping mapping)
        	throws IOException, ServletException {
     
		ActionForward forward = super.processActionPerform(request, 
														    reponse, 
														    action, 
														    form, 
														    mapping);
		
		if( storeResultForwardName )
		{
			request.setAttribute( RequestProcessor.FORWARD_NAME_REQUEST_ATTRIBUTE, forward);
		}
         if (forward != null) {
			UICommandReturnValue retValue = handleConfigCommands(forward.getPath(), 
			                                   forward.getRedirect(), request);
    		if (retValue!=null) {
    			forward = getNewForward(forward,retValue.getForward());
    			forward.setRedirect(retValue.isRedirect());
    		}
    		else {
				String newPath = handleReInvokeCommands(forward.getPath(), request, mapping);
				if (newPath!=null) {
					forward = getNewForward(forward,newPath);
					forward.setRedirect(getRedirectForReinvoke());
					return forward;		
				}
    		}
    
    		// Check for redirect to encode forward path with context values
    		if (forward.getRedirect()) {                
				handleRedirect(request);
    			forward = getRedirectForward(forward, request);
    		}
        }
		
	    return forward;	
    }


	/**
	 * Add all existing old values, which aren't changed to the new context. <br>
	 * And also set context values from the Layout Manager.
	 * 
	 * @param request current http request.
	 */
	protected void handleRedirect(HttpServletRequest request) {

		saveExtendedRequestAttributes(request);
		
		UILayoutManager layoutManager = (UILayoutManager)request.getAttribute(UILayoutManager.MANAGER);
		if (layoutManager!= null) {
			layoutManager.setContextValues(request);
		}
    	
		ContextManager contextManager = ContextManager.getManagerFromRequest(request);
		contextManager.addOldValues();
	}


	/****** Frameless Framework *****************/

	/**
	 * Investigate the logical forward, if it contains some
	 * key words of the frameless framework. <br>
	 * Allowed keywords are at the moment:
	 * <ol>
	 * <li><strong>UILayout</strong> Set the the given layout.</li>
	 * <li><strong>UIArea</strong> Set the the given layout area.</li>
	 * <li><strong>UIInclude</strong> Set the the given jsp as page for the 
	 *  current component.</li>
	 * <li><strong>UIStoreLayout</strong> Push the current layout to the stack.</li>
	 * <li><strong>UIGetLayout</strong> Pop the layout from the stack.</li>
	 * <li><strong>UIStoreForward</strong> Push the given forward to the stack.</li>
	 * <li><strong>UIGetForward</strong> Pop the forward from the stack.</li>
	 * </ol>
	 * <em>
	 * You can add an arbitrary command, if you add a class which implements the 
	 * {@link UICommand} interface to the {@link #uiCommands} map. <br> 
	 * </em>
	 * 
	 * @param forward original forward.
	 * 
	 * @return the forward command. If the forward is changed a new forward object
	 * will be created. 
	 */
	protected UICommandReturnValue handleConfigCommands(String path,
										   boolean redirect,	
										   HttpServletRequest request) {

		
		UICommandReturnValue retValue = null;

		String retForward = path;

		if (isUICommand(path)) {
			
			int countUICommands = 0;

			if (log.isDebugEnabled())
				log.debug("Parse forward: " + path);

			StringTokenizer tokenizer = new StringTokenizer(path, ";");

			GlobalUILayoutHandler layoutHandler = UILayoutInitHandler.getHandlerInstance();
			UILayoutManager layoutManager =
				UILayoutManager.getManagerFromRequest(request);

			// separate different commands
			while (tokenizer.hasMoreElements()) {
				countUICommands++;
				
				String token = (String) tokenizer.nextElement();
				boolean lastCommand = !tokenizer.hasMoreElements();

				if (log.isDebugEnabled())
					log.debug("Parse token: " + token);


				// determine the command name form the token	
				int index = token.indexOf(":");

			
				String uiCommandName = token;
				if (index >= 0) {
					// command with parameter:
					uiCommandName = token.substring(0,index+1);
				}

				UICommand uiCommand = (UICommand) uiCommands.get(uiCommandName);
				if (uiCommand != null) { 
                    retValue = uiCommand.processCommand(this,
                                                        request,
                                                        retForward,
                                                        layoutHandler,
                                                        layoutManager,
                                                        token,
                                                        lastCommand,
                            							redirect);

					continue;
				}

				if (countUICommands > 1 || token.indexOf("UI") == 0) {
					throw new PanicException("Unknown command "+ token + " in the path: " + path);
				}	
			}
		}

		return retValue;
	}




	/**
	 * Return if the given path includes an UI command . <br>
	 * 
	 * @param path path for the forward
	 * 
	 * @return <code>true</code> if the path includes an UI command
	 */
    protected boolean isUICommand(String path) {
        return path != null && path.indexOf("UI") == 0;
    }


	/**
     * Investigate the logical forward, if it contains the <code>ReInvoke</code>
	 * command. <br>
     * 
     * @param path    path of the current forward.
     * @param request current http request
     * @param mapping action mapping
     * @return new path or <code>null</code> if the path doesn't contains the reinvoke
     * command.
     */
	protected String handleReInvokeCommands(String path,
											 HttpServletRequest request,
											 ActionMapping mapping) {

		String retForward = null;

		// handle the reinvoke command        
		if (path != null && path.indexOf(REINVOKE) >= 0) {


			boolean portalReinvoke = path.indexOf(REINVOKE_PORTAL) >= 0;	
			 
			ComponentConfig compContainer = FrameworkConfigManager.XCM.getApplicationScopeConfig(InitializationHandler.getApplicationName() + "config");
			String enabled = compContainer.getParamConfig().getProperty("ReInvokeEnabled");
        	
			if (portalReinvoke || (enabled != null && enabled.equals("true"))) {
		
				String actionName = "";
		            
				int pos = path.indexOf(':');
		            
				if (!portalReinvoke && pos >= 0) {
					actionName = path.substring(pos+1);
				}
				else {
					actionName = WebUtil.decodeContextValueFromURL(request.getServletPath(),request);
				}
		                
				ContextManager contextManager = ContextManager.getManagerFromRequest(request);
				String layoutKey = contextManager.getContextValue(Constants.CV_LAYOUT_NAME);
				if (layoutKey!= null && !GlobalUILayoutHandler.getInstance().checkVersion(layoutKey)) {
					actionName = "/core/wrongBookmark.do";	
					contextManager.getContext().remove(Constants.CV_LAYOUT_NAME);
					contextManager.getContext().remove(Constants.CV_UITARGET);
				}
				        
                ReInvokeContainer reInvokeContainer =
                    createReinvokeContainer(request, mapping, actionName);
				HttpSession session = request.getSession();
					
				// store the reInvoke container in the request.
				session.setAttribute(ReInvokeContainer.CONTAINER,reInvokeContainer);
				        	                      
				// Call The init action of the application
				if (portalReinvoke) { 
					retForward = getInitAction(request); 
				}
				else {
					retForward = getInitAction();
				}
			}
			else {
				retForward = mapping.findForward(Constants.REINVOKE_NOT_ENABLED).getPath();
			}
			        	                      
		}   
		return retForward;
	}

	
	
	/**
	 * <p> The method getRedirectForReinvoke return if a redirect should be used while reinvoking the application </p>
	 * <p> The behavior is control with the property <code>ReInvokeSuppressRedirect</code> in the application configuration. </p>
	 * 
	 * @return <code>true</code> for redirects
	 */
	static public boolean getRedirectForReinvoke() {

		boolean redirect = true;
		
		ComponentConfig compContainer = FrameworkConfigManager.XCM.getApplicationScopeConfig(InitializationHandler.getApplicationName() + "config");
		String suppressRedirect = compContainer.getParamConfig().getProperty("ReInvokeSuppressRedirect");
		if (suppressRedirect != null && suppressRedirect.equals("true")) {
			redirect = false;
		}

		return redirect;
	}
	

	/** 
	 * Create a reinvoke container. <br>
	 * The method fill the context values and also the request parameter (but only 
	 * for action of type <code>GET</code>.
	 * 
	 * @param request current http request 
	 * @param mapping action mapping
	 * @param actionName name of the action to be performed after the ReInvoke.
	 * 
	 * @return ReInvokeContainer.
	 */
    protected ReInvokeContainer createReinvokeContainer(HttpServletRequest request,
                                                         ActionMapping mapping,
                                                         String actionName) {
                                                         	
        ContextManager contextManager = ContextManager.getManagerFromRequest(request);
            
        ReInvokeContainer reInvokeContainer = 
        		new ReInvokeContainer(actionName, 
        							  contextManager.getContext());
        
        if (request.getMethod().equalsIgnoreCase("POST")) {	
        	if (mapping != null) {
				ActionForward forward = mapping.findForward(Constants.REINVOKE_POST_METHOD);
				if (forward != null && forward.getPath().length() > 0 ) {
					reInvokeContainer.setAction(forward.getPath());	
				}
        	}
        }	
        else {	
        	reInvokeContainer.setRequestParameter(request.getParameterMap());
        }
        
        return reInvokeContainer;
    }


	/**
     * Return if the session is invalid. <br>
     * 
     * @param request current request.
     * @return <code>true</code> if the session is invlaid.
     */
    protected boolean isSessionInvalid(HttpServletRequest request) {

		UserSessionData userData =
			UserSessionData.getUserSessionData(request.getSession());

		if (userData == null) {
			return true;	
		}    
    	
        return false;
    }


    /**
	 * Returns the name of the action to start the application. <br>
	 * 
	 * @return The value stored under the web.xml parameter {@link ContextConst#APP_INIT_ACTION}
	 *  or <code>init.do</code>
	 */
	protected String getInitAction() {
		String action =  servlet.getServletConfig().getServletContext().getInitParameter(ContextConst.APP_INIT_ACTION);
		if (action == null) {
			action = "/init.do";
		}
		return action;
	}

	
    /**
	 * Returns the name of the action to start the application with start up parameter. <br>
	 * 
	 * @return The value stored under the web.xml parameter {@link ContextConst#APP_INIT_ACTION}
	 *  or <code>init.do</code>
	 */
	protected String getInitAction(HttpServletRequest request) {
		
		StringBuffer path = new StringBuffer(getInitAction());
		boolean parameterFound = (path.indexOf("?")>=0) ;
		
		// Add request parameter.
		if (request.getParameterMap()!= null) {
			Iterator iter = request.getParameterMap().entrySet().iterator();
			int prefixLength = ReInvokeContainer.STARTUP_PREFIX.length();
			while (iter.hasNext()) {
				Map.Entry element = (Map.Entry) iter.next();
				boolean use_parameter = false;
	
				String parameterKey = (String)element.getKey();
				String key = parameterKey;
				
				// Check for Startup prefix
				int pos = parameterKey.indexOf(ReInvokeContainer.STARTUP_PREFIX);
				if (pos >= 0) {
					 key = parameterKey.substring(prefixLength);
					 use_parameter = true;
				} 
				else {
					// xcm scenario will passed in any case to simplify CRM WEB Client Transaction Luncher functionility 
					if (key.equals(Constants.XCM_SCENARIO_RP)) {
						use_parameter = true;
					}
				}				

				if (use_parameter) {										
					String[] values = (String[])element.getValue();                
		                
					for (int i = 0; i < values.length; i++) {
						if (parameterFound) {
							path.append("&");
						}
						else {
							path.append("?");
							parameterFound = true;
						}
						path.append(key).append('=').append(values[i]);
					}
				}	
			} 
		}
		return path.toString();
	}
	

	/**
	 * Handles the area list which could be defined within brackets. <br>
	 * <strong>Example</strong><br>
	 * [header=header,workarea=basket]
	 * 
	 * @param uiLayout ui layout to be used.
	 * @param tokenizer tokenizer include the parameter in the form header=header etc. 
	 */
	protected void setLayoutAreas(
		UILayout uiLayout,
		StringTokenizer tokenizer) {

		while (tokenizer.hasMoreElements()) {
			String token = (String) tokenizer.nextElement();
			StringTokenizer nameValue = new StringTokenizer(token, "=");
			String name = null;
			if (nameValue.hasMoreElements()) {
				name = (String) nameValue.nextElement();
			}
			String value = null;
			if (nameValue.hasMoreElements()) {
				value = (String) nameValue.nextElement();
			}
			if (name == null || value == null) {
				throw new PanicException(
					"error in config file: wrong area definition ["
						+ token
						+ "]! ");
			}

			UIArea uiArea = uiLayout.getUiArea(name.trim());

			if (uiArea == null) {
				throw new PanicException(
					"error in config file: Area ["
						+ name
						+ "] not found in the layout ["
						+ uiLayout.getName()
						+ "]!");
			}

			GlobalUILayoutHandler layoutHandler = UILayoutInitHandler.getHandlerInstance();
			UIComponent component = layoutHandler.getComponent(value.trim());

			if (component == null) {
				throw new PanicException(
					"error in config: component [" + value + "] not found!");
			}

			uiArea.setComponent(component);
		}
	}


	/**
	 * Start the layout processing. <br>
	 * The iteration over the components will be initialized and all necessary
	 * context values will be set in the request. 
	 * 
	 * @param layoutManager layout manager
	 * @param request       current http request
	 */
	protected String startLayoutProcessing(UILayoutManager layoutManager, 
										  HttpServletRequest request) {
        
        UILayout uiLayout = layoutManager.getCurrentUILayout();
        uiLayout.processExternalComponents();
		uiLayout.startComponentActionIteration();
		layoutManager.setContextValues(request);

		String ret = uiLayout.getAction();
		if (ret.length() == 0) {
			ret = getNewPath(uiLayout);
		}
		return ret;
        
	}
    
    
	/**
	 * Call a component action or return the layout page. <br>
	 * The method lool for the next component of the current layout and calls the 
	 * corresponding action (if definied) and return the action as forward. 
	 * If all compoments are called the page defined in the ui layout will be 
	 * returned. <br>
	 * This method is triggered by the UILayout, UIArea and UIInclude commands.
	 * But only the commands UILayout and UIArea reset the component iterator.        
	 * 
	 * @param uiLayout ui layout object
	 * @return path to a component action or to the layout jsp if all components
	 *  are performed.
	 */
	protected String getNewPath(UILayout uiLayout) {

		String newPath = "";
		if (uiLayout.hasNextComponent()) {
			newPath = uiLayout.nextComponent().getAction();
		} else {
			newPath = uiLayout.getPath();
		}

		return newPath;
	}


	protected ActionForward getNewForward(ActionForward forward, 
										   String newPath) {
		ActionForward retForward;
		retForward = new ActionForward(newPath);
		retForward.setRedirect(forward.getRedirect());
		retForward.setName(forward.getName());
        
		return retForward;
	}


	/**
	 * Parse the parameter list of a UI Command. <br>
	 * The parameter list could consist of a layoutname and an area list which 
	 * is defined within brackets. <br>
	 * <strong>Example</strong>
	 * <pre>
	 * basketLayout[header=header,workarea=basket]
	 * </pre>
	 * 
	 * @param token parameter list.
	 * 
	 * @return an object array which consist of two elements. <code>object[0]</code>
	 * contains the name of the layout. <code>object[1]</code> a StringTokenizer 
	 * with the layout areas or <code>null</code> if the list is empty . 
	 * See {@link #setLayoutAreas} for details. 
	 * 
	 */
	protected Object[] parseCommandParameter(String token) {

		Object[] ret = new Object[2];
		
		if (token != null) {
			String foundName = null;
			int index = token.indexOf("[");
			if (index >= 0) {
				String name = null;
				if (index > 0) {
					name = token.substring(0, index);
				}
				int lastIndex = token.length() - 1;
				String tokens = token.substring(index + 1, lastIndex);
				StringTokenizer tokenizer = new StringTokenizer(tokens, ",");
	
				if (name != null) {
					foundName = name.trim();
				}
				
				ret[1] = tokenizer;
			} else {
				foundName = token.trim();
			}
			// check if the found name isn't empty
			if (foundName!=null && foundName.length() > 0) {
				ret[0] = foundName;
			}
		}

		return ret;
	}


	/**
	 * Return a new forward with encoded Context values for a proper
	 * redirect behaviour. <br>
	 * 
	 * @param forward current forward
	 * @param request current http request
	 * 
	 * @return modified forward
	 */
	protected ActionForward getRedirectForward(ActionForward forward,
												HttpServletRequest request) {

		String newPath = WebUtil.encodeURLWithContextValues(forward.getPath(),request);

		return getNewForward(forward, newPath);
	}


	/**
     * This method initialize the ui commands. <br>
     * In this method should add all used ui commands. 
     */
    protected void initUICommands() {

		UICommand uiCommand = new UILayoutWithParameter();
		uiCommands.put(uiCommand.getName(), uiCommand);	

		uiCommand = new UILayoutCommand();
		uiCommands.put(uiCommand.getName(), uiCommand);	

		uiCommand = new UIAction();
		uiCommands.put(uiCommand.getName(), uiCommand);

		uiCommand = new UIRedirect();
		uiCommands.put(uiCommand.getName(), uiCommand);

		uiCommand = new UIExtendedRedirect();
		uiCommands.put(uiCommand.getName(), uiCommand);
				
		uiCommand = new UIAreaCommand();
		uiCommands.put(uiCommand.getName(), uiCommand);
		
		uiCommand = new UIInclude();
		uiCommands.put(uiCommand.getName(), uiCommand);
			
		uiCommand = new UIJSInclude();
		uiCommands.put(uiCommand.getName(), uiCommand);

		uiCommand = new UIStoreLayoutWithParameter();
		uiCommands.put(uiCommand.getName(), uiCommand);

		uiCommand = new UIStoreLayout();
		uiCommands.put(uiCommand.getName(), uiCommand);

		uiCommand = new UIGetLayout();
		uiCommands.put(uiCommand.getName(), uiCommand);

		uiCommand = new UIStoreAction();
		uiCommands.put(uiCommand.getName(), uiCommand);

		uiCommand = new UIStoreActionWithParameter();
		uiCommands.put(uiCommand.getName(), uiCommand);

		uiCommand = new UIStoreForward();
		uiCommands.put(uiCommand.getName(), uiCommand);

		uiCommand = new UIGetForward();
		uiCommands.put(uiCommand.getName(), uiCommand);

		uiCommand = new UIContinue();
		uiCommands.put(uiCommand.getName(), uiCommand);
		
	}

		
	/**
 	 * The class UILayoutWithParameter handles the UI Command <strong>UILayout</strong>
 	 * with additional parameters. <br>
     *
     * @author  SAP AG
     * @version 1.0
 	 */
	protected class UILayoutWithParameter implements UICommand {
		
		/**
		 * Return the name of the command. <br>
		 * 
		 * @return
		 */
		public String getName() {
			return "UILayout:"; 
		}
		
		
		/**
         * Processing of the command. <br>
         * 
         * @param processor      request processor
         * @param request        http request
         * @param forward        original forward string
         * @param layoutHandler  global layout handler
         * @param layoutManager  layout manager
         * @param token          current toke 
         * @param lastCommand    flag if the command is the last one.
         * @param redirect       indicates if a redirect is necessary.
         * @return
         * @throws PanicException
         */
        public UICommandReturnValue processCommand(RequestProcessor processor,
									  HttpServletRequest request,
									  String forward,
									  GlobalUILayoutHandler layoutHandler,
									  UILayoutManager layoutManager,
									  String token,
									  boolean lastCommand,
									  boolean redirect)
									  
				throws PanicException {
			
			String retForward = forward;
					
			Object[] parameter =
				parseCommandParameter(token.substring(getName().length()));
        
			String layoutName = (String) parameter[0];
        
			UILayout uiLayout;
			if (layoutName != null) {
				uiLayout = layoutHandler.getLayout(layoutName);
				if (uiLayout == null) {
					throw new PanicException(
						"error in config file: Layout ["
							+ layoutName
							+ "] not found!");
        
				}
				layoutManager.setUiLayout(uiLayout);
				
			} else {
				uiLayout = layoutManager.getCurrentUILayout();
        
				if (uiLayout == null) {
					throw new PanicException("No active layout for the UILayout command");
				}
			}
        
			if (parameter[1] != null) {
				processor.setLayoutAreas(uiLayout,(StringTokenizer) parameter[1]);
			}
        
			if (lastCommand) {
				if (redirect) {
					throw new PanicException("Redirect is not allowed for the UILayout command: "+  forward +  ". Add UIAction to get implement this!");	
				}
				
				// Start processing, only if last ui command
				retForward = processor.startLayoutProcessing(layoutManager,request);
			}
			
			return new UICommandReturnValue(retForward,redirect);
		}

		
	}


	/**
	 * The class UILayout handles the UI Command <strong>UILayout</strong>
	 * without additional parameters. <br>
	 *
	 * @author  SAP AG
	 * @version 1.0
	 */
	protected class UILayoutCommand implements UICommand {
		
		/**
		 * Return the name of the command. <br>
		 * 
		 * @return
		 */
		public String getName() {
			return "UILayout"; 
		}
		
		
		/**
		 * Processing of the command. <br>
		 * 
		 * @param processor      request processor
		 * @param request        http request
		 * @param forward        original forward string
		 * @param layoutHandler  global layout handler
		 * @param layoutManager  layout manager
		 * @param token          current toke 
		 * @param lastCommand    flag if the command is the last one.
		 * @param redirect       indicates if a redirect is necessary.
		 * @return
		 * @throws PanicException
		 */
		public UICommandReturnValue processCommand(RequestProcessor processor,
									  HttpServletRequest request,
									  String forward,
									  GlobalUILayoutHandler layoutHandler,
									  UILayoutManager layoutManager,
									  String token,
									  boolean lastCommand,
									  boolean redirect)
									  
				throws PanicException {

			String retForward = forward;
		
			UILayout uiLayout = layoutManager.getCurrentUILayout();

			if (uiLayout == null) {
				throw new PanicException("No active layout for the UILayout command");
			}

			if (lastCommand) {
				if (redirect) {
					throw new PanicException("Redirect is not allowed for the UILayout command: "+  forward +  ". Add UIAction to get implement this!");	
				}
				retForward = processor.startLayoutProcessing(layoutManager,request);
			}	
			
			return new UICommandReturnValue(retForward,redirect);
		}

		
	}


	/**
	 * The class UIAction handles the UI Command <strong>UIAction</strong>. <br>
	 *
	 * @author  SAP AG
	 * @version 1.0
	 */
	protected class UIAction implements UICommand {
		
		
		public boolean setRedirect(boolean redirect) {
			return redirect;	
		}
		
		/**
		 * Return the name of the command. <br>
		 * 
		 * @return
		 */
		public String getName() {
			return "UIAction:"; 
		}
		
		
		/**
		 * Processing of the command. <br>
		 * 
		 * @param processor      request processor
		 * @param request        http request
		 * @param forward        original forward string
		 * @param layoutHandler  global layout handler
		 * @param layoutManager  layout manager
		 * @param token          current token 
		 * @param lastCommand    flag if the command is the last one.
		 * @param redirect       indicates if a redirect is necessary.
		 * @return
		 * @throws PanicException
		 */
		public UICommandReturnValue processCommand(RequestProcessor processor,
									  HttpServletRequest request,
									  String forward,
									  GlobalUILayoutHandler layoutHandler,
									  UILayoutManager layoutManager,
									  String token,
									  boolean lastCommand,
									  boolean redirect)
									  
				throws PanicException {

			String forwardName = token.substring(getName().length());

			if (forwardName == null) {
				throw new PanicException("No foward defined in the UIAction command");
			}

			layoutManager.setContextValues(request);


			return new UICommandReturnValue(forwardName,setRedirect(redirect));
		}

		
	}


	/**
	 * The class UIRedirect handles the UI Command <strong>UIRedirect</strong>. <br>
	 *
	 * @author  SAP AG
	 * @version 1.0
	 */
	protected class UIRedirect extends UIAction implements UICommand {
		
		
		public boolean setRedirect(boolean redirect) {
			return true;	
		}
		
		/**
		 * Return the name of the command. <br>
		 * 
		 * @return
		 */
		public String getName() {
			return "UIRedirect:"; 
		}

	}		


	/**
	 * The class UIExtendedRedirect handles the UI Command <strong>UIExtendedRedirect</strong>. <br>
	 *
	 * @author  SAP AG
	 * @version 1.0
	 */
	protected class UIExtendedRedirect extends UIRedirect implements UICommand {
		
		
		/**
		 * Return the name of the command. <br>
		 * 
		 * @return
		 */
		public String getName() {
			return "UIExtendedRedirect:"; 
		}

		/**
		 * Processing of the command. <br>
		 * 
		 * @param processor      request processor
		 * @param request        http request
		 * @param forward        original forward string
		 * @param layoutHandler  global layout handler
		 * @param layoutManager  layout manager
		 * @param token          current toke 
		 * @param lastCommand    flag if the command is the last one.
		 * @param redirect       indicates if a redirect is necessary.
		 * @return
		 * @throws PanicException
		 */
		public UICommandReturnValue processCommand(RequestProcessor processor,
									  HttpServletRequest request,
									  String forward,
									  GlobalUILayoutHandler layoutHandler,
									  UILayoutManager layoutManager,
									  String token,
									  boolean lastCommand,
									  boolean redirect) {
									  	
			Enumeration enumrator = request.getAttributeNames();
			List names = getExtendedRequestAttributeNames(request);
			while (enumrator.hasMoreElements()) {
				names.add((String) enumrator.nextElement());
			}
												  
			return super.processCommand(processor,request,forward,layoutHandler,layoutManager,token,
				lastCommand,redirect);									  
		}
	}		

	/**
	 * The class UIAreaCommand handles the UI Command <strong>UIArea</strong>. <br>
	 *
	 * @author  SAP AG
	 * @version 1.0
	 */
	protected class UIAreaCommand implements UICommand {
		
		/**
		 * Return the name of the command. <br>
		 * 
		 * @return
		 */
		public String getName() {
			return "UIArea:"; 
		}
		
		
		/**
		 * Processing of the command. <br>
		 * 
		 * @param processor      request processor
		 * @param request        http request
		 * @param forward        original forward string
		 * @param layoutHandler  global layout handler
		 * @param layoutManager  layout manager
		 * @param token          current toke 
		 * @param lastCommand    flag if the command is the last one.
		 * @param redirect       indicates if a redirect is necessary.
		 * @return
		 * @throws PanicException
		 */
		public UICommandReturnValue processCommand(RequestProcessor processor,
									  HttpServletRequest request,
									  String forward,
									  GlobalUILayoutHandler layoutHandler,
									  UILayoutManager layoutManager,
									  String token,
									  boolean lastCommand,
									  boolean redirect)
									  
				throws PanicException {

			String retForward = forward;

			String componentName =
				token.substring(getName().length());

			UILayout uiLayout = layoutManager.getCurrentUILayout();

			if (uiLayout == null) {
				throw new PanicException(
					"Error in layout management: "
						+ "No layout for forward ["
						+ token
						+ "]! ");
			}

			UIArea uiArea = uiLayout.getActiveArea();

			if (uiArea == null) {
				throw new PanicException(
					"error in config file: "
						+ "No active area for forward ["
						+ token
						+ "]! ");
			}

			UIComponent component =
				layoutHandler.getComponent(componentName);

			if (component == null) {
				throw new PanicException(
					"error on jsp: component ["
						+ componentName
						+ "] not found!");
			}

			uiArea.setComponent(component);

			if (lastCommand) {
				// Start processing, only if last ui command
				retForward = startLayoutProcessing(layoutManager,request);
			}	


			return new UICommandReturnValue(retForward,redirect);
		}

		
	}

	/**
	 * The class UIInclude handles the UI Command <strong>UIInclude</strong>. <br>
	 *
	 * @author  SAP AG
	 * @version 1.0
	 */
	protected class UIInclude implements UICommand {
		
		/**
		 * Return the name of the command. <br>
		 * 
		 * @return
		 */
		public String getName() {
			return "UIInclude:"; 
		}
		
		
		/**
		 * Processing of the command. <br>
		 * 
		 * @param processor      request processor
		 * @param request        http request
		 * @param forward        original forward string
		 * @param layoutHandler  global layout handler
		 * @param layoutManager  layout manager
		 * @param token          current toke 
		 * @param lastCommand    flag if the command is the last one.
		 * @param redirect       indicates if a redirect is necessary.
		 * @return
		 * @throws PanicException
		 */
		public UICommandReturnValue processCommand(RequestProcessor processor,
									  HttpServletRequest request,
									  String forward,
									  GlobalUILayoutHandler layoutHandler,
									  UILayoutManager layoutManager,
									  String token,
									  boolean lastCommand,
									  boolean redirect)
									  
				throws PanicException {

			String retForward = forward;

			UILayout uiLayout = layoutManager.getCurrentUILayout();

			if (uiLayout == null) {
				throw new PanicException(
					"Error in layout management: "
						+ "No layout for forward ["
						+ token
						+ "]! ");
			}

			String includeName =
				token.substring(getName().length());

			UIComponent component = uiLayout.getCurrentComponent();

			if (component != null) {
				component.setPage(includeName);
			} else {
				throw new PanicException(
					"error in config file: "
						+ "No active component for forward ["
						+ token
						+ "]! ");
			}

			retForward = processor.getNewPath(uiLayout);

			return new UICommandReturnValue(retForward,redirect);
		}

		
	}


	/**
	 * The class UIJSInclude handles the UI Command <strong>UIJSInclude</strong>. <br>
	 *
	 * @author  SAP AG
	 * @version 1.0
	 */
	protected class UIJSInclude implements UICommand {
		
		/**
		 * Return the name of the command. <br>
		 * 
		 * @return
		 */
		public String getName() {
			return "UIJSInclude:"; 
		}
		
		
		/**
		 * Processing of the command. <br>
		 * 
		 * @param processor      request processor
		 * @param request        http request
		 * @param forward        original forward string
		 * @param layoutHandler  global layout handler
		 * @param layoutManager  layout manager
		 * @param token          current toke 
		 * @param lastCommand    flag if the command is the last one.
		 * @param redirect       indicates if a redirect is necessary.
		 * @return
		 * @throws PanicException
		 */
		public UICommandReturnValue processCommand(RequestProcessor processor,
									  HttpServletRequest request,
									  String forward,
									  GlobalUILayoutHandler layoutHandler,
									  UILayoutManager layoutManager,
									  String token,
									  boolean lastCommand,
									  boolean redirect)
									  
				throws PanicException {

			UILayout uiLayout = layoutManager.getCurrentUILayout();

			String includeName =
				token.substring(getName().length());

			UIComponent component = uiLayout.getCurrentComponent();

			if (component != null) {
				component.setJsInclude(includeName);
			} else {
				throw new PanicException(
					"error in config file: "
						+ "No active component for forward ["
						+ token
						+ "]! ");
			}
			
			if (lastCommand) {
				throw new PanicException("Error in config file: "
										+ "["
										+ getName()
										+ "] is not allowed as last command ");
			}

			return new UICommandReturnValue(forward,redirect);
		}

		
	}


	/**
	 * The class UIStoreLayoutWithParameter handles the UI Command <strong>UIStoreLayout</strong>
	 * with additional parameters. <br>
	 *
	 * @author  SAP AG
	 * @version 1.0
	 */
	protected class UIStoreLayoutWithParameter implements UICommand {
		
		/**
		 * Return the name of the command. <br>
		 * 
		 * @return
		 */
		public String getName() {
			return "UIStoreLayout:"; 
		}
		
		
		/**
		 * Processing of the command. <br>
		 * 
		 * @param processor      request processor
		 * @param request        http request
		 * @param forward        original forward string
		 * @param layoutHandler  global layout handler
		 * @param layoutManager  layout manager
		 * @param token          current toke 
		 * @param lastCommand    flag if the command is the last one.
		 * @param redirect       indicates if a redirect is necessary.
		 * @return
		 * @throws PanicException
		 */
		public UICommandReturnValue processCommand(RequestProcessor processor,
									  HttpServletRequest request,
									  String forward,
									  GlobalUILayoutHandler layoutHandler,
									  UILayoutManager layoutManager,
									  String token,
									  boolean lastCommand,
									  boolean redirect)
									  
				throws PanicException {
			
					
			Object[] parameter =
				parseCommandParameter(token.substring(getName().length()));

			String layoutName = (String) parameter[0];

			UILayout uiLayout = null;
			if (layoutName != null) {
				uiLayout = layoutHandler.getLayout(layoutName);
				if (uiLayout == null) {
					throw new PanicException(
						"error in config file: Layout ["
							+ layoutName
							+ "] not found!");

				}
			} else {
				try {
					uiLayout = (UILayout)layoutManager.getCurrentUILayout().clone();
				}
				catch (CloneNotSupportedException e) {
					log.error(LogUtil.APPS_USER_INTERFACE_CONTROLLER,"system.exception",e);
				}

				if (uiLayout == null) {
					throw new PanicException("No active layout for the UILayout command");
				}
			}

			if (parameter[1] != null) {
				setLayoutAreas(
					uiLayout,
					(StringTokenizer) parameter[1]);
			}

			layoutManager.storeLayout(uiLayout);
			
			return new UICommandReturnValue(forward,redirect);
		}

		
	}


	/**
	 * The class UIStoreLayoutr handles the UI Command <strong>UIStoreLayout</strong>
	 * without additional parameters. <br>
	 *
	 * @author  SAP AG
	 * @version 1.0
	 */
	protected class UIStoreLayout implements UICommand {
		
		/**
		 * Return the name of the command. <br>
		 * 
		 * @return
		 */
		public String getName() {
			return "UIStoreLayout"; 
		}
		
		
		/**
		 * Processing of the command. <br>
		 * 
		 * @param processor      request processor
		 * @param request        http request
		 * @param forward        original forward string
		 * @param layoutHandler  global layout handler
		 * @param layoutManager  layout manager
		 * @param token          current toke 
		 * @param lastCommand    flag if the command is the last one.
		 * @param redirect       indicates if a redirect is necessary.
		 * @return
		 * @throws PanicException
		 */
		public UICommandReturnValue processCommand(RequestProcessor processor,
									  HttpServletRequest request,
									  String forward,
									  GlobalUILayoutHandler layoutHandler,
									  UILayoutManager layoutManager,
									  String token,
									  boolean lastCommand,
									  boolean redirect)
									  
				throws PanicException {
			
					
			UILayout uiLayout = layoutManager.getCurrentUILayout();

			if (uiLayout == null) {
				throw new PanicException("No active layout for the UILayout command");
			}

			layoutManager.storeLayout();

			
			return new UICommandReturnValue(forward,redirect);
		}

		
	}


	/**
	 * The class UIGetLayout handles the UI Command <strong>UIGetLayout</strong>
	 * without additional parameters. <br>
	 *
	 * @author  SAP AG
	 * @version 1.0
	 */
	protected class UIGetLayout implements UICommand {
		
		/**
		 * Return the name of the command. <br>
		 * 
		 * @return
		 */
		public String getName() {
			return "UIGetLayout"; 
		}
		
		
		/**
		 * Processing of the command. <br>
		 * 
		 * @param processor      request processor
		 * @param request        http request
		 * @param forward        original forward string
		 * @param layoutHandler  global layout handler
		 * @param layoutManager  layout manager
		 * @param token          current toke 
		 * @param lastCommand    flag if the command is the last one.
		 * @param redirect       indicates if a redirect is necessary.
		 * @return
		 * @throws PanicException
		 */
		public UICommandReturnValue processCommand(RequestProcessor processor,
									  HttpServletRequest request,
									  String forward,
									  GlobalUILayoutHandler layoutHandler,
									  UILayoutManager layoutManager,
									  String token,
									  boolean lastCommand,
									  boolean redirect)
									  
				throws PanicException {
			
					
			UILayout uiLayout = layoutManager.getStoredLayout();

			layoutManager.setUiLayout(uiLayout);
			
			return new UICommandReturnValue(forward,redirect);
		}
	}


	/**
	 * The class UIStoreForward handles the UI Command <strong>UIStoreForward</strong>
	 * with additional parameters. <br>
	 *
	 * @author  SAP AG
	 * @version 1.0
	 */
	protected class UIStoreForward implements UICommand {
		
		/**
		 * Return the name of the command. <br>
		 * 
		 * @return
		 */
		public String getName() {
			return "UIStoreForward:"; 
		}
		
		
		/**
		 * Processing of the command. <br>
		 * 
		 * @param processor      request processor
		 * @param request        http request
		 * @param forward        original forward string
		 * @param layoutHandler  global layout handler
		 * @param layoutManager  layout manager
		 * @param token          current toke 
		 * @param lastCommand    flag if the command is the last one.
		 * @param redirect       indicates if a redirect is necessary.
		 * @return
		 * @throws PanicException
		 */
		public UICommandReturnValue processCommand(RequestProcessor processor,
									  HttpServletRequest request,
									  String forward,
									  GlobalUILayoutHandler layoutHandler,
									  UILayoutManager layoutManager,
									  String token,
									  boolean lastCommand,
									  boolean redirect)
									  
				throws PanicException {
			
					
			String forwardName =
				token.substring(getName().length());

			if (forwardName == null) {
				throw new PanicException("No foward defined in the UIStoreForward command");
			}

			layoutManager.setForward(forwardName);

			
			return new UICommandReturnValue(forward,redirect);
		}
	}


	/**
	 * The class UIStoreActionWithParameter handles the UI Command <strong>UIStoreAction</strong>
	 * with additional parameters. <br>
	 * In contrast to UIAction without parameter no request parameter will be taken into account.
	 *
	 * @author  SAP AG
	 * @version 1.0
	 */
	protected class UIStoreActionWithParameter implements UICommand {
		
		/**
		 * Return the name of the command. <br>
		 * 
		 * @return
		 */
		public String getName() {
			return "UIStoreAction:"; 
		}
		
		
		/**
		 * Processing of the command. <br>
		 * 
		 * @param processor      request processor
		 * @param request        http request
		 * @param forward        original forward string
		 * @param layoutHandler  global layout handler
		 * @param layoutManager  layout manager
		 * @param token          current toke 
		 * @param lastCommand    flag if the command is the last one.
		 * @param redirect       indicates if a redirect is necessary.
		 * @return
		 * @throws PanicException
		 */
		public UICommandReturnValue processCommand(RequestProcessor processor,
									  HttpServletRequest request,
									  String forward,
									  GlobalUILayoutHandler layoutHandler,
									  UILayoutManager layoutManager,
									  String token,
									  boolean lastCommand,
									  boolean redirect)
				throws PanicException {
					
			String actionName = token.substring(getName().length()); 

			ReInvokeContainer container = 
					processor.createReinvokeContainer(request, null, actionName);

			container.clearRequestParameter();
					
			layoutManager.addForwardContainer(container);	
			
			return new UICommandReturnValue(forward,redirect);
		}
	}


	/**
	 * The class UIStoreAction handles the UI Command <strong>UIStoreAction</strong>
	 * without additional parameters. <br>
	 *
	 * @author  SAP AG
	 * @version 1.0
	 */
	protected class UIStoreAction implements UICommand {
		
		/**
		 * Return the name of the command. <br>
		 * 
		 * @return
		 */
		public String getName() {
			return "UIStoreAction"; 
		}
		
		
		/**
		 * Processing of the command. <br>
		 * 
		 * @param processor      request processor
		 * @param request        http request
		 * @param forward        original forward string
		 * @param layoutHandler  global layout handler
		 * @param layoutManager  layout manager
		 * @param token          current toke 
		 * @param lastCommand    flag if the command is the last one.
		 * @param redirect       indicates if a redirect is necessary.
		 * @return
		 * @throws PanicException
		 */
		public UICommandReturnValue processCommand(RequestProcessor processor,
									  HttpServletRequest request,
									  String forward,
									  GlobalUILayoutHandler layoutHandler,
									  UILayoutManager layoutManager,
									  String token,
									  boolean lastCommand,
									  boolean redirect)
				throws PanicException {
					
			// Store the orginal called action and rebuild the context values.		
			String actionName=WebUtil.decodeContextValueFromURL(getOriginalServletPath(request),true,request); 
			ReInvokeContainer container = 
					processor.createReinvokeContainer(request, null, actionName);
					
			layoutManager.addForwardContainer(container);	
			
			return new UICommandReturnValue(forward,redirect);
		}
	}

		
	/**
	 * The class UIGetForward handles the UI Command <strong>UIGetForward</strong>. <br>
	 *
	 * @author  SAP AG
	 * @version 1.0
	 */
	protected class UIGetForward implements UICommand {
			
		/**
		 * Return the name of the command. <br>
		 * 
		 * @return
		 */
		public String getName() {
			return "UIGetForward"; 
		}
			
			
		/**
		 * Processing of the command. <br>
		 * 
		 * @param processor      request processor
		 * @param request        http request
		 * @param forward        original forward string
		 * @param layoutHandler  global layout handler
		 * @param layoutManager  layout manager
		 * @param token          current toke 
		 * @param lastCommand    flag if the command is the last one.
		 * @param redirect       indicates if a redirect is necessary.
		 * @return
		 * @throws PanicException
		 */
		public UICommandReturnValue processCommand(RequestProcessor processor,
									  HttpServletRequest request,
									  String forward,
									  GlobalUILayoutHandler layoutHandler,
									  UILayoutManager layoutManager,
									  String token,
									  boolean lastCommand,
									  boolean redirect)
										  
				throws PanicException {
				
			String retForward = forward;
			boolean retRedirect = redirect;
						
			String forwardName = layoutManager.getForward();
	
			if (forwardName == null) {
	//	throw new PanicException("No forward defined for the UIGetForward command");
				retForward =FORWARD_ACTION+"?forward=default";
				return new UICommandReturnValue(retForward,retRedirect);
			}
	
			if (layoutManager.hasReInVokeContainer(forwardName)) {
				ReInvokeContainer reInvokeContainer = layoutManager.getReInvokeContainer(forwardName);
				if (reInvokeContainer != null) {
					retForward =  reInvokeContainer.reInvoke(request);
					retRedirect = true; 
				}
				else {
					retForward =FORWARD_ACTION+"?forward=default";
				}
			}
			else {         
				retForward = FORWARD_ACTION + "?forward=" + forwardName;
			}
	
	
			return new UICommandReturnValue(retForward,retRedirect);
		}
	}
	

	/**
	 * The class UIContinue handles the UI Command <strong>UIContinue</strong>. <br>
	 *
	 * @author  SAP AG
	 * @version 1.0
	 */
	protected class UIContinue implements UICommand {
			
		/**
		 * Return the name of the command. <br>
		 * 
		 * @return
		 */
		public String getName() {
			return "UIContinue"; 
		}
			
			
		/**
		 * Processing of the command. <br>
		 * 
		 * @param processor      request processor
		 * @param request        http request
		 * @param forward        original forward string
		 * @param layoutHandler  global layout handler
		 * @param layoutManager  layout manager
		 * @param token          current toke 
		 * @param lastCommand    flag if the command is the last one.
		 * @param redirect       indicates if a redirect is necessary.
		 * @return
		 * @throws PanicException
		 */
		public UICommandReturnValue processCommand(RequestProcessor processor,
									  HttpServletRequest request,
									  String forward,
									  GlobalUILayoutHandler layoutHandler,
									  UILayoutManager layoutManager,
									  String token,
									  boolean lastCommand,
									  boolean redirect)
										  
				throws PanicException {
				
			UILayout uiLayout = layoutManager.getCurrentUILayout();

			if (uiLayout == null) {
				throw new PanicException(
					"Error in layout management: "
						+ "No layout for forward ["
						+ token
						+ "]! ");
			}

			String retForward = processor.getNewPath(uiLayout);
	
			return new UICommandReturnValue(retForward,redirect);
		}
	}
	

	/**
	 * Return the instance of the secureConnectionChecker. <br>
	 * 
	 * @return SecureConnectionChecker
	 */
	public static SecureConnectionChecker getSecureConnectionChecker() {
		return secureConnectionChecker;
	}	


	/**
	 * Return the instance of the secureConnectionChecker. <br>
	 * 
	 * @return SecureConnectionChecker
	 */
	private static SecureConnectionChecker createSecureConnectionChecker() {
		
		SecureConnectionChecker secureConnectionChecker
				= (SecureConnectionChecker)GenericFactory.getInstance("SecureConnectionChecker",
				 							  SecureConnectionChecker.class.getName());
		
		return secureConnectionChecker;
	}



	/**
	 * The class SecureConnectionChecker check if the current request is a secure 
	 * connection. <br>
	 *
	 * @author  SAP AG
	 * @version 1.0
	 */
	public static class SecureConnectionChecker {
		
		protected List listener = new ArrayList();

		
		/**
		 * This method is called if the connection is unsecure. <br>
		 *
		 * @param request current http request 
		 * @param userData the user session data object
		 */
		public void onUnSecureConnection (HttpServletRequest request, UserSessionData userData) {

			SecureHandler.handleUnsecureConnection(request, userData);
					
			if (log.isDebugEnabled()) 
				log.debug("Firing UnsecureConnectionEvent ");
			Iterator i = listener.iterator();
			while (i.hasNext()) {
				UnsecureConnectionEventHandler eventCapturer = (UnsecureConnectionEventHandler)i.next();
				eventCapturer.onUnSecureConnection(request, userData);
			}
		}
		
		
		/**
		 * Register an {@link UnsecureConnectionEventHandler} to obtain events.
		 * 
		 * @param UnsecureConnectionEventHandler handler for the event.
		 *
		 */
		public void registerEventHandler(UnsecureConnectionEventHandler capturer) {
			listener.add(capturer);
		}


		/**
		 * Overwrite this method to perform your own check. <br>
		 * 
		 * @param request current http request 
		 * @param userData the user session data object
		 * @return <code>false<code> if the connection is unsecure.
		 */
		protected boolean checkHttpsProtocol(HttpServletRequest request, UserSessionData userData) {
			
			String protocol = request.getHeader("protocol-header");

			if (log.isDebugEnabled()) 
				log.debug("Value for protocol-header: " + protocol);
			
			if (protocol != null) {
				return protocol.equalsIgnoreCase("https");	
			}			
			
			return request.isSecure();
		}


		/**
		 * Check the if the connection is still secure. If not the <code>onUnSecureConnection</code> event
		 * is fired. <br>
		 * 
		 * @param request current http request 
		 * @param userData the user session data object
		 */
		public void performCheck(HttpServletRequest request, UserSessionData userData) {

			if (log.isDebugEnabled()) 
				log.debug("Check if the connection is secure ");
			
			if (!checkHttpsProtocol(request, userData)) {
				onUnSecureConnection(request, userData);
			}
		}

		
		/**
		 * Check the if the connection is secure or unsecure.
		 *  
		 * @param request current http request 
		 * @return <code>false<code> if the connection is unsecure.
		 */
		public boolean isRequestSecure(HttpServletRequest request, UserSessionData userData) {
			
			 return checkHttpsProtocol(request, userData);
		}

	}

	/**
	 * If the parameter resultForwardName was set to true,
	 * the resulting forwardName of an action which was executed will be stored
	 * in the request attributes.
	 */
	public static void setStoreResultForwardName(boolean storeResultForwardName) {
		RequestProcessor.storeResultForwardName = storeResultForwardName;
	}


}
