package com.sap.isa.core;

import java.util.Enumeration;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts.util.MessageResources;
import org.apache.struts.util.MessageResourcesFactory;
import org.w3c.dom.Document;

import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.init.InitializationHandler;
import com.sap.isa.core.interaction.InteractionConfig;
import com.sap.isa.core.interaction.InteractionConfigContainer;
import com.sap.isa.core.interaction.InteractionConfigManager;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.ui.context.ContextValue;
import com.sap.isa.core.ui.context.ReInvokeContainer;
import com.sap.isa.core.xcm.ConfigContainer;
import com.sap.isa.core.xcm.ExtendedConfigException;
import com.sap.isa.core.xcm.ExtendedConfigManager;
import com.sap.isa.core.xcm.XCMConstants;
import com.sap.isa.core.xcm.XCMUtils;
import com.sap.isa.core.xcm.config.ComponentConfig;
import com.sap.isa.core.xcm.config.ComponentConfigContainer;
import com.sap.isa.core.xcm.init.ExtendedConfigInitHandler;
import com.sap.isa.core.xcm.scenario.ScenarioManager;

/**
 * This class is a central instance providing methods for initialization of
 * XCM the environment. Most of the initialization tasks done by InitAction have
 * been moved to this class. This enables applications not using InitAction to
 * perform initialization Tasks themselves. Besides that, this class acts as a
 * facade for some often used XCM functions. It can be considered as a central
 * point for getting XCM based configuration data
 */
public class FrameworkConfigManager {
	
	protected static IsaLocation log =
						   IsaLocation.getInstance(FrameworkConfigManager.class.getName());
	
	/**
	 * scenario name used when no scenario has been specified 
	 */
	public static final String NAME_BOOTSTRAP_SCENARIO = ScenarioManager.NAME_DEFAULT_SCENARIO;
	
	
	public static class XCM {

		/**
		 * Returns component configuration container for 
		 * application scope configuration. This method assumes that the 
		 * configuration is stored in a file with the alias 'config-data'
		 * @param component name
		 * @return component configuration container or <code>null</code> if 
		 * configuration could not be found
		 */
		public static ComponentConfigContainer getApplicationScopeConfig() {
			//String defaultScenarioName = getDefaultXCMScenarioName();
			ConfigContainer cc = getXCMScenario(FrameworkConfigManager.NAME_BOOTSTRAP_SCENARIO);
			if (cc == null)
				return null;
			Document doc = cc.getConfigUsingAliasAsDocument(XCMConstants.CONFIG_DATA_FILE_ALIAS);
			if (doc == null)
				return null;
			
			return new ComponentConfigContainer(doc, true);
		}

		/**
		 * Returns a config container containing 
		 * @param configName
		 * @return
		 */

		/**
		 * Returns an application scope configuration. The configuration is taken
		 * from an XCM managed application scope component with same name as the application.
		 * E.g. in B2B, data is taken from application scope XCM component named 'b2b'.
		 * The application name is maintained in the context parameter 'application.name'
		 * This method assumes that the configuration is stored  
		 * in a file with the alias 'config-data'
		 * @param configName configuration name. 
		 * @return Component configurition
		 * @throws RuntimeException if context parameter application.name is not maintained
		 */
		static public ComponentConfig getApplicationScopeConfig(String configName) {
			if (InitializationHandler.getApplicationName() == null)
				throw new RuntimeException("Error getting XCM application scope configuration. Context parameter 'application.name' not maintained");
			
			ComponentConfigContainer ccc = getApplicationScopeConfig();
			
			if (ccc == null)
				return null;
				
			return ccc.getComponentConfig(InitializationHandler.getApplicationName(), configName);
		}


		/**
		 * Returns the XCM configuration key for the given scenario name
		 * @param scenarioName
		 * @return XCM configuration key for the given scenario name or <code>null</code>
		 *         if sceanrio is not available
		 */
		public static String getXCMScenarioKey(String scenarioName) {
			ConfigContainer cc = getXCMScenarioConfig(scenarioName);
			if (cc != null)
				return cc.getConfigKey();
			else
				return null;
		}

		/**
		 * Returns true if XCM is enabled, otherwise false
		 * @return if XCM is enabled, otherwise false
		 */
		public static boolean isXCMActive() {
			return ExtendedConfigInitHandler.isActive();
		}

		/**
		 * Checks if there is a XCM scenaro for the given name 
		 * @param scenName
		 * @return <code>true</code> if the scenario exists, otherwise
		 * <code>false</code>
		 * @throws 
		 */
		public static boolean isXCMScenario(String scenName) {
			return ScenarioManager.getScenarioManager().isScenario(scenName);
		}

		/**
		 * Returns the name of the default XCM scenario
		 * @return The name of the default XCM scenario name
		 */
		public static String getDefaultXCMScenarioName() {
			return ScenarioManager.getScenarioManager().getDefaultScenario();
		}

		/**
		 * Returns a configuration container for the given scenario name
		 * @deprecated Please use getXCMScenarioConfig() instead
		 * @param scenName scenarioName
		 * @return Scenario key. If <code>null</code> scenario does not exist
		 */ 
		public static ConfigContainer getXCMScenario(String scenName) {
			return getXCMScenarioConfig(scenName);
		}
		
		/**
		 * Returns a configuration container for the given scenario name or scenario key
		 * @param scenName scenarioName
		 * @return Scenario key. If <code>null</code> scenario does not exist
		 */ 
		public static ConfigContainer getXCMScenarioConfig(String name) {
			ExtendedConfigManager xcmManager = ExtendedConfigManager.getExtConfigManager();
			if (isXCMScenario(name)) {
				// use scenario name
				return xcmManager.getConfig(name);
			} else {
				// assuming that the passed value is a configuration key
				return xcmManager.getExistingConfig(name);
			}
		}		


		/**
		 * Returns the InteractionConfigContainer for the given scenario name
		 * This container is used to store context parameter parameters
		 * maintained by XCM.
		 * @param scenName scenario name
		 * @return the InteractionConfigContainer for the given scenario
		*/
		public static InteractionConfigContainer getInteractionConfigContainer(String scenName) {
			ConfigContainer cc = getXCMScenario(scenName);
			return InteractionConfigManager.getInteractionConfig(cc);
		}

	}
	
	public static class Servlet {
		
		/**
		 * Initializes a meta business object manager
		 * @param request HTTP Requeset for this session
		 * @param msgRes message resources
		 * @param props addtional properties for lower layers
		 * @return meta bom
		 * @throws Exception if initialization fails
		 */
		public static MetaBusinessObjectManager initXCMBasedEnvironment(HttpServletRequest request, MessageResources msgRes, Properties props) throws Exception {
			final String METHOD = "initXCMBasedEnvironment()";
			log.entering(METHOD);		
			String configKey = initXCMBasedEnvironment(request); 
			
			MetaBusinessObjectManager mbom = BusinessObject.initFramework(configKey, msgRes, props);

			HttpSession session = request.getSession();
			
			UserSessionData userData =
									 UserSessionData.getUserSessionData(session);
			
			userData.setMBOM(mbom);
			log.exiting();
			return mbom;			
		}

		/**
		 * Initializes a meta business object manager
		 * @param request HTTP Requeset for this session
		 * @param msgRes message resources
		 * @return meta bom
		 * @throws Exception if initialization fails
		 */
		public static MetaBusinessObjectManager initXCMBasedEnvironment(HttpServletRequest request, MessageResources msgRes) throws Exception {
			final String METHOD = "initXCMBasedEnvironment()";
			log.entering(METHOD);
			String configKey = initXCMBasedEnvironment(request); 
			
			MetaBusinessObjectManager mbom = BusinessObject.initFramework(configKey, msgRes);

			HttpSession session = request.getSession();

			UserSessionData userData =
									 UserSessionData.getUserSessionData(session);
			
			userData.setMBOM(mbom); 
			log.exiting();
			return mbom;			
				
		}

		/**
		 * Initializes a meta business object manager
		 * @param request HTTP Requeset for this session
		 * @param classNameMessageResources class name of message resources
		 * @return meta bom
		 * @throws Exception if initialization fails
		 */
		public static MetaBusinessObjectManager initXCMBasedEnvironment(HttpServletRequest request, String classNameMessageResources) throws Exception {
			final String METHOD = "initXCMBasedEnvironment()";
			log.entering(METHOD);
			if (log.isDebugEnabled())
				log.debug("classNameMessageResources="+classNameMessageResources);	
			String configKey = initXCMBasedEnvironment(request); 
			
			MetaBusinessObjectManager mbom = BusinessObject.initFramework(configKey, classNameMessageResources);

			HttpSession session = request.getSession();

			UserSessionData userData =
									 UserSessionData.getUserSessionData(session);
			
			userData.setMBOM(mbom);
			log.exiting();
			return mbom;			
		}
		/**
		 * Returns a configuration value for an XCM Interaction component
		 * If the requested xcm component <strong>componentName</strong> or the 
		 * <strong>propertyName</strong> doesn't exist in XCM the method returns 
		 * the web.xml context parameter <strong>ctxParamName</strong>
		 * 
		 * @param session The httpSession for retrieving the context parameter
		 * @param componentName The XCM InteractionConfig (e.g. "ui" )
		 * @param propertyName The requested property 
		 * @param ctxParamName The name of the context parameter to be used as fallback
		 * @return The value from XCM or web.xml or null
		 */
		public static String getInteractionConfigParameter(HttpSession session, String componentName, String propertyName, String ctxParamName)
		{
			InteractionConfigContainer icc = FrameworkConfigManager.Servlet.getInteractionConfig(session);
			InteractionConfig ic =null;
			String rc=null;
			if( icc != null && null != ( ic= icc.getConfig(componentName)) )
				rc = ic.getValue(propertyName);
			if(rc == null )
			{
				if( log.isDebugEnabled())
					log.debug("Property not found in XCM component " + componentName 
						+ ", propertyname " + propertyName + " using web.xml context parameter " + ctxParamName);
				if (session != null) { 						
					rc = session.getServletContext().getInitParameter(ctxParamName);
				}
			}
			return rc;
		}

		/**
		 * This method does the following
		 * It gets information about the XCM scenario from the request 
		 * If initializes the XCM scenario
		 * It put the config container for the given scenario onto the user
		 * session data using SessionConst.XCM_CONFIG
		 * It put the InteractionConfigContainer into the user session data
		 * using SessionConst.INTERACTION_CONFIG
		 * Sets the config key into the user session data using SharedConst.
		 * XCM_CONF_KEY
		 * The method gets the MetaBom for the given configuration initializes
		 * it and puts it into the user session data 
		 * 
		 * @param request
		 * @throws Exception
		 */
		private static String initXCMBasedEnvironment(HttpServletRequest request) throws Exception {
			final String METHOD = "initXCMBasedEnvironment()";
			log.entering(METHOD);
			
			Properties props = getXCMScenarioParams(request);
		

			ExtendedConfigManager xcmManager = ExtendedConfigManager.getExtConfigManager();
			// get current configuration
			ConfigContainer configContainer = null;
			try {
				Map xcmProps = XCMUtils.getExtConfProps(props);
				// get context root
				String context = request.getContextPath();
				if (context != null) {
					if (context.substring(0,1).equals("/"))
						context = context.substring(1);
					xcmProps.put(XCMConstants.CONTEXT_PATH, context);
				}
			
				if (log.isDebugEnabled())
					log.debug("Initializing backend with [parameters]='" + xcmProps + "'");
				configContainer = xcmManager.getModifiedConfig(xcmProps);
			} catch (ExtendedConfigException xcmex) {
			  String errMsg = "Error reading configuration information" + xcmex.toString();
			  log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.xcm.exception", new Object[] { errMsg }, xcmex);
			}

			HttpSession session = request.getSession();
			
			UserSessionData userData =
									 UserSessionData.getUserSessionData(session);


			// put config container into session
			userData.setAttribute(SessionConst.XCM_CONFIG, configContainer);		
	
			// get key of configuration
			String configKey = configContainer.getConfigKey();

			// get the interaction configuration
			InteractionConfigContainer interactionContainer = InteractionConfigManager.getInteractionConfig(configContainer);
		
			// put the interaction config into the session
			userData.setAttribute(SessionConst.INTERACTION_CONFIG, interactionContainer);

			userData.setAttribute(SharedConst.XCM_CONF_KEY, configKey);
			log.exiting();
			return configKey;	
		}

		/**
		 * Returns scenario parameters passed by a request. This method also
		 * calls the getScenarioName() and puts the current scenario name into
		 * the properties
		 */
		 static Properties getXCMScenarioParams(HttpServletRequest request) throws Exception {

			Properties props = new Properties();
			if (request.getParameterNames() != null) {
				for (Enumeration enum = request.getParameterNames(); enum.hasMoreElements();) {
					String paramName = (String)enum.nextElement();
					String paramValue = request.getParameter(paramName);
					// check if request parameter is an Extended Configuration Management parameter
					if ((paramName.length() > 4) && (paramName.lastIndexOf(XCMConstants.XCM_CONF_PARAM_EXTENSION) == (paramName.length() - 4))) {
						props.put(paramName, paramValue);
					}
				}
			} 
			
			String scenarioName = getXCMScenarioName(request);
			
			props.put(Constants.XCM_SCENARIO_RP, scenarioName);
			
			return props;
		}

		/**
		 * Returns the interaction configuration for the current session.
		 * @param request the HTTP request
		 * @return interaction configuration data
		 */
		public static InteractionConfigContainer getInteractionConfig(HttpServletRequest request) {
			if (request.getSession() == null)
				return null;
			UserSessionData userData =
					UserSessionData.getUserSessionData(request.getSession());
			if (userData == null)
				return null;

			return (InteractionConfigContainer)(userData.getAttribute(SessionConst.INTERACTION_CONFIG));
		}

		/**
		 * Returns the interaction configuration for the current session.
		 * @param session the session
		 * @return interaction configuration data
		 */
		public static InteractionConfigContainer getInteractionConfig(HttpSession session) {
			if (session == null) {
				return null;
			}
			UserSessionData userData =
					UserSessionData.getUserSessionData(session);
			if (userData == null)
				return null;

			return (InteractionConfigContainer)(userData.getAttribute(SessionConst.INTERACTION_CONFIG));
		}

		/**
		 * Returns a set of properties used to create a new XCM configuration
		 * This properties contain the scenario name either passed by the
		 * request or the defualt scenario. 
		 * @throws Exception If there is not scenario name in the request and no
		 * default scenario
		 */
		protected static String getXCMScenarioName(HttpServletRequest request) throws Exception {

			// get default scenario
			String defaultScenario = FrameworkConfigManager.XCM.getDefaultXCMScenarioName();


			// get scenario passed as request parameter
			String scenarioName = request.getParameter(Constants.XCM_SCENARIO_RP);
			if (scenarioName == null)
				scenarioName = request.getParameter(Constants.XCM_CONFIGURATION_RP);
            if (scenarioName == null) // overcome weekness of engine to decode POST parameters
                scenarioName = request.getParameter(Constants.XCM_SCENARIO_RP_ENCODED);

			if (scenarioName == null) {
				// For a reinvoke use the scenario store in the context	
				UserSessionData userData = UserSessionData.getUserSessionData(request.getSession());
				ReInvokeContainer reInvokeContainer = (ReInvokeContainer) userData.getAttribute(SessionConst.REINVOKE_CONTAINER);
				if (reInvokeContainer != null) {
					ContextValue contextValue = (ContextValue)reInvokeContainer.getContext().get(Constants.CV_XCM_SCENARIO);
					if (contextValue!= null) {
						scenarioName = (String)contextValue.getValue();
					}
				}
			}
				
			if (scenarioName == null)
				scenarioName = defaultScenario;			

		
					// no scenario has been specified so far => error message
			if (scenarioName == null) {
				String errMsg = "No XCM application configuration has been passed and there is no default XCM application configuration defined. Check XCM configuration";
				throw new Exception(errMsg); 
			}
			else {
				// scenario has been passed
				// check if scenario exists
				if(!FrameworkConfigManager.XCM.isXCMScenario(scenarioName)) {
					String errMsg = "The [XCM application configuration]='" + scenarioName + "' does not exist. Check XCM configuration.";
					throw new Exception(errMsg); 
				}
			}
			return scenarioName;
		}
	}
	
	
	
	
		
	public static class BusinessObject {
		
		
		/**
		 * This method initializes the Business Object Layer and the Backend
		 * Object Layer for a given XCM configuration key
		 *	
		 */
		public static MetaBusinessObjectManager initFramework(String configKey, String classNameMessageResources) {
			final String METHOD = "initFramework()";
			log.entering(METHOD);
			if (log.isDebugEnabled())
				log.debug("configKey="+configKey+", classNameMessageResources="+classNameMessageResources);
			Properties props = new Properties();
			
			props.setProperty(SharedConst.XCM_CONF_KEY, configKey);
			
			MessageResourcesFactory factoryObject =
				  MessageResourcesFactory.createFactory();
			MessageResources messageResources = factoryObject.createResources(classNameMessageResources);
			messageResources.setReturnNull(false);

			MetaBusinessObjectManager metaBOM =
						 new MetaBusinessObjectManager(messageResources, props);
					
			log.exiting();
			return metaBOM;
		}
		
		/**
		 * This method initializes the Business Object Layer and the Backend
		 * Object Layer for a given XCM configuration key
		 * @param configKey XCM configuration key
		 * @param msgRes resource keys
		 *	
		 */
		public static MetaBusinessObjectManager initFramework(String configKey, MessageResources msgRes) {
			final String METHOD = "initFramework()";
			log.entering(METHOD);
			if (log.isDebugEnabled())
				log.debug("configKey="+configKey+", msgRes="+msgRes);
			
			Properties props = new Properties();
			props.setProperty(SharedConst.XCM_CONF_KEY, configKey);
			log.exiting();
			return new MetaBusinessObjectManager(msgRes, props);
		}

		/**
		 * This method initializes the Business Object Layer and the Backend
		 * Object Layer for a given XCM configuration key
		 * @param configKey XCM configuration key
		 * @param msgRes resource keys
		 * @param props addtional properties which should be passed to the lower layers   
		 */
		public static MetaBusinessObjectManager initFramework(String configKey, MessageResources msgRes, Properties props) {
			final String METHOD = "initFramework()";
			log.entering(METHOD);
			if (log.isDebugEnabled())
				log.debug("configKey="+configKey+", msgRes="+msgRes);
			
			if (props == null)
				props = new Properties();
				 
			props.setProperty(SharedConst.XCM_CONF_KEY, configKey);
			log.exiting();
			return new MetaBusinessObjectManager(msgRes, props);
		}


		
	}
}
