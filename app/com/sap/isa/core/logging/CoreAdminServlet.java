package com.sap.isa.core.logging;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sap.isa.core.config.AdminConfig;
import com.sap.isa.core.logging.sla.SessionLoggingSupport;


public class CoreAdminServlet extends HttpServlet {

//	  public void doGet(HttpServletRequest request, HttpServletResponse response)
//		  throws ServletException, IOException
//	  {
//		  throw new ServletException("This Operation is Forbidden");
//	 }
   public void doGet(HttpServletRequest request, HttpServletResponse response)
			  throws ServletException, IOException
		  {
			// check if file download is allowed
			if (!AdminConfig.isLogfileDownload())
			  throw new ServletException("This Operation is Forbidden");
   
			 // check if there is a request parameter specifying the forward
			//String dynamicForward = request.getParameter("nextaction");
			String logFileName = request.getParameter("logfilename");
			String sessionlogOperation = request.getParameter("sessionlog");
			String showlogOperation = request.getParameter("fromshowlog");
			
			// workaround for IE5.5 bug
			String sBrowser = "" + request.getHeader("User-Agent") ;
			boolean bIE55 = (sBrowser.toUpperCase().indexOf("MSIE 5.5")!=-1) ;
   			
			if (bIE55){
			  response.setContentType("application/download; name=\""+ logFileName +".zip\"") ;
			  response.setHeader("Content-Disposition", "anything; filename="+ logFileName + ".zip\";") ;
			}
			else{
				// download type = 'application/octet-stream' or 'application/x-zip-compressed'
				response.setContentType("application/zip ; name=\"" + logFileName + ".zip\"") ;
				response.setHeader("Content-Disposition","attachment; filename=\"" + logFileName + ".zip\";");
			}
   
			ServletOutputStream out = response.getOutputStream();
			byte[] zipFile = null;
			if (logFileName.indexOf("..") != -1)
				throw new ServletException("This Operation is Forbidden");     	
		
			if (logFileName != null) {
				String filePath = null;
				if(sessionlogOperation != null) {
					SessionLoggingSupport sessionlogConf = (SessionLoggingSupport) request.getSession().getAttribute("SessionLoggingSupport.sla.logging.core.isa.sap.com");
					if (sessionlogConf != null) {
						filePath = sessionlogConf.getLogFilePath();
					} else {
						filePath = request.getParameter("logfilepath");
					}
				} else if(showlogOperation != null) {
					Admin admin = (Admin) request.getSession().getAttribute("admin");
					filePath = admin.getCurrentLogFilePath();
				} else {
					Admin admin = (Admin) request.getSession().getAttribute("admin");
					filePath = admin.getLogFilePathForLogFileName(logFileName);
				}
				zipFile = Admin.createZipFile(new File(filePath));
   
			}
			 out.write(zipFile);
			 out.flush();
			 out.close();
		  }

}