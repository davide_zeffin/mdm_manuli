package com.sap.isa.core.logging;

import java.util.Locale;

import org.apache.struts.util.MessageResources;
import com.sap.isa.core.init.InitializationHandler;
import com.sap.tc.logging.Category;

public class LogUtil {

	public static Category APPS_COMMON_BACKUP = Category.getCategory("/Applications/Common/Backup");	
	public static Category APPS_COMMON_ARCHIVING  = Category.getCategory("/Applications/Common/Archiving");
	public static Category APPS_COMMON_RESOURCES = Category.getCategory("/Applications/Common/Resources");	
	public static Category APPS_COMMON_SECURITY  = Category.getCategory("/Applications/Common/Security");
	public static Category APPS_COMMON_FAILOVER = Category.getCategory("/Applications/Common/Failover");
	public static Category APPS_COMMON_CONFIGURATION  = Category.getCategory("/Applications/Common/Configuration");
	public static Category APPS_COMMON_INFRASTRUCTURE   = Category.getCategory("/Applications/Common/Infrastructure");	
	public static Category APPS_USER_INTERFACE = Category.getCategory(Category.APPLICATIONS, "/UserInterface");	
	public static Category APPS_BUSINESS_LOGIC = Category.getCategory(Category.APPLICATIONS, "/BusinessLogic");
	public static Category APPS_USER_INTERFACE_CONTROLLER = Category.getCategory(Category.APPLICATIONS, "/UserInterface/Controller");

	/**
	 * This helper method returns a string representation of a log message.
	 * It should be used whenever a log message is written. Texts have to be 
	 * defined in resource bundle.
	 * @param description Description: what has happened. If null then key is ignored
	 * @param descrParam parameters used in the description 
	 * @param impact which impact has this event. If null then key is ignored
	 * @param reason what causes this event. If null then key is ignored
	 * @param furtherInformation what should i do with this event. If null then key is ignored
	 */
	public static LogMessage getLogMessage(String descriptionKey, Object[] descrParams, String impactKey, String reasonKey, String furtherInformationKey) {
		StringBuffer sb = new StringBuffer();
		
		if (descriptionKey != null)
			sb.append("description: " + getResourceBundleString(descriptionKey, descrParams));
		if (impactKey != null)
			sb.append("impact: " + getResourceBundleString(impactKey, null));
		if (reasonKey != null)
			sb.append("reason: " + getResourceBundleString(reasonKey, null));
			
		sb.append("further info: " + getResourceBundleString(furtherInformationKey, null));
		return new LogMessage(sb.toString());		
	}


	/**
	 * This helper method returns a string representation of a log message.
	 * It should be used whenever a log message is written. Texts have to be 
	 * defined in resource bundle.
	 * @param description Description: what has happened. If null then key is ignored
	 * @param impact which impact has this event. If null then key is ignored
	 * @param reason what causes this event. If null then key is ignored
	 * @param furtherInformation what should i do with this event. If null then key is ignored
	 */
	public static LogMessage getLogMessage(String descriptionKey, String impactKey, String reasonKey, String furtherInformationKey) {
		
		StringBuffer sb = new StringBuffer();
		sb.append("description: " + getResourceBundleString(descriptionKey, null) + "\n");
		sb.append("impact: " + getResourceBundleString(impactKey, null) + "\n");
		sb.append("reason: " + getResourceBundleString(reasonKey, null) + "\n");
		sb.append("further info: " + getResourceBundleString(furtherInformationKey, null) + "\n");
		return new LogMessage(sb.toString());		
	}
	
	private static String getResourceBundleString(String key, Object[] params) {
		if (key == null || key.length() == 0)
			return "";
		
		MessageResources mr = InitializationHandler.getMessageResources();
		if (mr == null)
			return "logging not initialized yet; [resource key]='" + key + "'";

		if (mr.isPresent(Locale.ENGLISH, key.toString()))
			return mr.getMessage(Locale.ENGLISH, key.toString(), params); 
		else
			return "[resource key missing]='" + key + "'";
	}
}
