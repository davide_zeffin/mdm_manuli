package com.sap.isa.core.logging.test;

import java.util.Properties;

import com.sap.tc.logging.Location;
import com.sap.tc.logging.PropertiesConfigurator;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Severity;

public class DemoLogging {
	
	
	/*
	* Define the Location which is used for this class as a static   *final.
		 * As a rule use the package name to initialize the Location.
		 */
		private static final Location loc = 
		Location.getLocation(DemoLogging.class.getName());
	
	public static void main(String[] args) {
		
		Properties props = new Properties();
		props.setProperty("com.sap.isa.core.logging.test.severity", "DEBUG");
		props.setProperty("com.sap.isa.core.logging.test.logs", "log[demo]");
		props.setProperty("/Applications", "log[demo]");
		props.setProperty("log[demo]", "ConsoleLog");
		
		PropertiesConfigurator pcfg = new PropertiesConfigurator(props);
		pcfg.setClassLoader(DemoLogging.class.getClassLoader());
		pcfg.configure();
		
		
		DemoLogging demo = new DemoLogging();
		
		// entering exiting method		
		demo.enterExitMethod();	
		
		// writing traces
		demo.writingTraces();	

		// writing Log
		demo.writingLogs();	

		demo.ISALocationMigration();
		
	}
	
	public void enterExitMethod() {
		String METHOD_NAME = "enterExitMethod()";

		loc.entering(METHOD_NAME);

		try {
			// do something useful 
		} finally {
			// 
			loc.exiting();	
		}
	}

	public void writingTraces() {
		String METHOD_NAME = "writingTraces()";

		
		// trace level DEBUG		
		loc.debugT(METHOD_NAME, "User {0} started transaction", 
		   new Object[] { "Marek" });

		// trace level ERROR		
		loc.errorT(METHOD_NAME, "User {0} started transaction. Error", 
		   new Object[] { "Marek" });

		// trace exception
		Exception ex = new Exception("An error occured");
		loc.traceThrowableT(Severity.ERROR, "error  {0}", new Object[] { "in test programm" }, ex); 
	}
	
	public void writingLogs() {
		
		String METHOD_NAME = "writingLogs()";

		// using default application category
		Category.APPLICATIONS.infoT(loc, "log");
		
		//category.logThrowableT(Severity.WARNING, location, getResourceBundleString(key, args), t);
	}
	
	public void ISALocationMigration() {
		
		// migration of log.isDebugEnabled()
		if (true) {
			// migration of debug
			loc.debugT("This is a debug message");
		}
		
		// migration of logging
		// log.warn("system.eai.exception", new Object[] { msg }, null);
		
		String msg = "message";
				
		loc.warningT(getResourceBundleString("system.eai.exception", new Object[] { msg }, null));
	}
	
	public String getResourceBundleString(String key, Object[] params, Throwable t) {
		return null; 
	}
}

