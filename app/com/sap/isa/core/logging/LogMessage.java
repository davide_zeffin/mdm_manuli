package com.sap.isa.core.logging;

/**
 * Class encapsulates a log message
 */
public class LogMessage {
		
	private String mLogMessage;
		
	/**
	 * Creates an message object containing the message
	 * @param logMessage message text
	 */
	public LogMessage(String logMessage) {
		mLogMessage = logMessage;
	}
		
	/**
	 * Returns the message text
	 * @return the message text
	 */
	public String getMessageText() {
		return mLogMessage;
	}
	
	/**
	 * Returns the message text
	 * @return message text
	 */
	public String toString() {
		return mLogMessage;
	}
}
