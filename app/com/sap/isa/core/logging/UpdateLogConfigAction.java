/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      12 December, 2001

  $Revision: #2 $
  $Date: 2001/07/04 $
*****************************************************************************/

package com.sap.isa.core.logging;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * This action gets the <code>logconfig</code> request atrribute
 * and stores the content of this parameter in the currently used
 * logging configuration file
 */

public class UpdateLogConfigAction extends Action {

    private static final String LOGCONFIG_CLASS =
                           "com.sap.isa.core.logging.SaveConfigFileAction";

    /**
     *
     * @param mapping The ActionMapping used to select this instance
     * @param action The optional ActionForm bean for this request (if any)
     * @param request The HTTP request we are processing
     * @param response The HTTP response we are creating
     *
     * @return <code>ActionForward</code> describing where the control should
     *         be forwarded
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     */
    public final ActionForward perform(ActionMapping mapping,
                 ActionForm form,
                 HttpServletRequest request,
                 HttpServletResponse response)
        throws IOException, ServletException {


      String logConfigContent = request.getParameter("logconfig");

      Admin.saveConfigFile(logConfigContent);

      return mapping.findForward("success");
    }
  }