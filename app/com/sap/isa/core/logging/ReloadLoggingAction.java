/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Thorsten Kampp
  Created:      21. Juni 2001

  $Revision: #2 $
  $Date: 2001/07/04 $
*****************************************************************************/

package com.sap.isa.core.logging;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.Constants;
import com.sap.isa.core.init.InitializeException;

/**
 * The <code>ReloadLoggingAction</code> basic purpose is to reload the
 * log configuration files.
 */

public class ReloadLoggingAction extends Action {

    private static final String LOGCONFIG_CLASS =
                           "com.sap.isa.core.logging.LogConfigurator";

    /**
     * The <code>ReloadLoggingAction</code> basic purpose is to reload the
     * log configuration files.
     * By passing the reqest parameter <code>nextAction</next> you
     * can define the next action dynamically
     *
     * @param mapping The ActionMapping used to select this instance
     * @param action The optional ActionForm bean for this request (if any)
     * @param request The HTTP request we are processing
     * @param response The HTTP response we are creating
     *
     * @return <code>ActionForward</code> describing where the control should
     *         be forwarded
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     */
    public final ActionForward perform(ActionMapping mapping,
                 ActionForm form,
                 HttpServletRequest request,
                 HttpServletResponse response)
        throws IOException, ServletException {

        IsaLocation log =
            IsaLocation.getInstance(ReloadLoggingAction.class.getName());

        // check if there is a request parameter specifying the forward
        String dynamicForward = request.getParameter("nextaction");

        String nextAction = Constants.SUCCESS;
        com.sap.isa.core.ActionServlet servlet = null;
        Object check = getServlet();

        // Check if "check" is really our ...core.ActionServlet
        if(check instanceof com.sap.isa.core.ActionServlet) {

           try {

             servlet = (com.sap.isa.core.ActionServlet)check;

             if(log.isDebugEnabled()) {
              log.debug("Refreshing the log system");
             }

             servlet.performTermination( LOGCONFIG_CLASS );
             servlet.performInitialization( LOGCONFIG_CLASS );

           } catch(InitializeException e) {

             log.warn("system.initLogging.failed", e);
             nextAction = Constants.FAILURE;

           } catch(Exception e) {

             log.warn("system.initFailed", e);
             nextAction = Constants.FAILURE;

           }

        } else {

           if(log.isDebugEnabled()) {
              log.debug("Could not retrieve our ActionServlet");
           }
           nextAction = Constants.FAILURE;

        }
        if (dynamicForward != null) {
          ActionForward forward = new ActionForward();
          forward.setPath(dynamicForward);
          return forward;
        } else
          return mapping.findForward(nextAction);
    }

}