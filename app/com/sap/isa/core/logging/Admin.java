package com.sap.isa.core.logging;
/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      13.12.2001

  $Revision: $
  $Date: $
*****************************************************************************/

// java imports
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import com.sap.isa.core.logging.sla.LogConfiguratorSla;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.core.util.table.TableRow;
import com.sap.tc.logging.FileLog;
import com.sap.tc.logging.Formatter;
import com.sap.tc.logging.FormatterType;
import com.sap.tc.logging.ListFormatter;
import com.sap.tc.logging.Location;
import com.sap.tc.logging.Log;
import com.sap.tc.logging.Severity;
import com.sap.tc.logging.TraceFormatter;
import com.sap.tc.logging.XMLFormatter;

/**
 * This class provides methods which can be used to administer the logging
 * capabilities
 */
public class Admin {

	private static String mConfigFileName;
	private static final String CRLF = System.getProperty("line.separator");
	;
	private Table currentLogFileTable;
	private ResultData currentLogFileResultData;
	private int currentRow = 0;
	private int currentRowDelta;
	private String currentLogFilePath = "";
	private String currentLogFileName = "";
	private long currentFileSize;
	private String currentLocation = "com.sap.isa";
	private boolean locationChanged = true;
	private ResultData currentLogConfig = null;
	private HashMap filesInLocation = new HashMap();
	private boolean isSessionLog = false;

	protected static IsaLocation log =
		IsaLocation.getInstance(Admin.class.getName());

	/**
	 * This class returns the content of the currently used logging configuration file
	 * @return the currently used logging configuration file
	 */
	public String getConfigFile() {
		if (mConfigFileName == null)
			return "";
		StringBuffer sb = new StringBuffer();
		try {
			LineNumberReader lnr =
				new LineNumberReader(new FileReader(mConfigFileName));

			for (String line = lnr.readLine(); line != null;) {
				sb.append(line);
				sb.append(CRLF);
				line = lnr.readLine();
			}
		} catch (Exception ex) {
			return "An Exception has occured\n" + ex.toString();
		}
		return sb.toString();
	}

	/**
	 * Overwrites the content of the currently used logging configuration file
	 * @param @configFileContent the new content of the logging config file
	 */
	public static void saveConfigFile(String configFileContent) {
		if (configFileContent == null)
			return;
		try {
			FileWriter fw = new FileWriter(mConfigFileName);
			fw.write(configFileContent, 0, configFileContent.length());
			fw.flush();
			fw.close();
		} catch (Exception ex) {
			return;
		}
	}

	/**
	 * Sets the name and path of the logging configuration file.
	 * @param fileName the file name of the logging configuration file
	 */
	public void setConfigFileName(String fileName) {
		mConfigFileName = fileName;
	}

	/**
	  * Returns a table containing a list of all log files
	  * @return a table containing a list of log files
	  */
	public Table getLogFiles() {
		File file = new File(LogConfigurator.getLogPath());
		StringBuffer sb = new StringBuffer();

		Table logFilesTable = new Table("logfiles");
		logFilesTable.addColumn(Table.TYPE_STRING, "filename");
		logFilesTable.addColumn(Table.TYPE_STRING, "filesize");
		logFilesTable.addColumn(Table.TYPE_STRING, "lastmodified");
		try {
			File[] dirFiles = file.listFiles();

			for (int i = 0; i < dirFiles.length; i++) {
				TableRow row = logFilesTable.insertRow();
				row.getField(1).setValue(dirFiles[i].getName());
				row.getField(2).setValue(String.valueOf(dirFiles[i].length()));
				row.getField(3).setValue(
					new Date((dirFiles[i].lastModified())));
			}
		} catch (Exception ex) {
			log.error(
				LogUtil.APPS_BUSINESS_LOGIC,
				"system.exception",
				new Object[] {
					"Could not find directory: "
						+ LogConfigurator.getLogPath()},
				null);
			return null;
		}
		return logFilesTable;
	}

	/**
	 * Loads a log file. One Admin object can hold one log file at one point in time
	 * @param logFileName name of log file
	 */
	public void loadLogFile() throws Exception {
		// get the path for the chosen logfile
		if (currentLogFilePath.equalsIgnoreCase("null")) {
			log.error("The chosen log couldn't be loaded.");
			throw new Exception("The chosen log couldn't be loaded.");
		}

		// initialize table
		currentLogFileTable = new Table("logfile");
		currentLogFileTable.addColumn(Table.TYPE_STRING, "content");

		try {
			// read the complete file into internal table
			//String logFilePath = "";
			// consturct the path of the log file
			//String loggingPath = LogConfigurator.getLogPath();
			//File logFile = new File(getLogFilePath(logFileName));
			File logFile = new File(currentLogFilePath);
			currentLogFileName = logFile.getName();

			currentFileSize = logFile.length();
			LineNumberReader lnr =
				new LineNumberReader(new FileReader(logFile));

			for (String line = lnr.readLine(); line != null;) {
				TableRow row = currentLogFileTable.insertRow();
				row.getField(1).setValue(line);
				line = lnr.readLine();
			}
		} catch (FileNotFoundException fnfex) {
			throw new Exception(fnfex.toString());
		}
		currentLogFileResultData = new ResultData(currentLogFileTable);
	}

	/**
		 * Returns the absolte path of the log file loaded by the viewer.
		 * @return The absolte path of the log file loaded by the viewer.
		 */
	public String getCurrentLogFilePath() {
		return currentLogFilePath;
	}

	/**
	 * Sets the number of lines used when navigating the logging table
	 * @param rowDelta number of lines
	 */
	public void setRowDelta(int rowDelta) {
		currentRowDelta = rowDelta;
	}

	/**
	 * Gets the number of lines used when navigating the logging table
	 * @param rowDelta number of lines
	 */
	public int getRowDelta() {
		return currentRowDelta;
	}

	/**
	* Moves the cursor to the first row of the log table and returns a set of log entries.
	* @return a set of log entries
	*/
	public ResultData first() {
		currentRow = 1;
		return getLogFileData();
	}

	/**
	 * If possible moves the cursor to the last row minus the navigation delta.
	 * @return data
	 */
	public ResultData last() {
		int numRows = currentLogFileResultData.getNumRows();
		// only one page can be displayed
		if (currentRowDelta > numRows)
			return first();
		else
			currentRow = numRows - currentRowDelta + 1;

		return getLogFileData();

	}

	/**
	 * Moves the cursor to the next delta
	 * @return data
	 */
	public ResultData next() {
		if ((currentRow + currentRowDelta)
			>= currentLogFileResultData.getNumRows())
			return last();

		currentRow = currentRow + currentRowDelta;
		return getLogFileData();
	}

	/**
	 * Moves the cursor to the given line
	 * @param lineNumber
	 * @return data
	 */
	public ResultData line(int lineNumber) {
		if (lineNumber <= 1)
			return first();

		if (lineNumber >= currentLogFileResultData.getNumRows())
			return last();

		currentRow = lineNumber;
		return getLogFileData();
	}

	/**
	 * Returns data at the current position
	 * @return data
	 */
	public ResultData current() {
		return getLogFileData();
	}

	/**
	 * Moves the cursor to the previous delta
	 * @return data
	 */
	public ResultData previous() {
		if ((currentRow - currentRowDelta) < 1)
			return first();

		currentRow = currentRow - currentRowDelta;
		return getLogFileData();
	}

	private ResultData getLogFileData() {

		int numRows = currentLogFileTable.getNumRows();
		if (numRows == 0)
			return getEmptyResultData();

		int startRow = 0;
		int endRow = 0;
		if (currentRow > numRows) {
			startRow = numRows;
			endRow = startRow;
		} else
			startRow = currentRow;

		currentRow = startRow;

		if ((startRow + currentRowDelta) > numRows)
			endRow = numRows;
		else
			endRow = startRow + currentRowDelta - 1;

		// copy number of rows into a temporary table
		Table tmpTable = new Table("tmp");
		tmpTable.addColumn(Table.TYPE_STRING, "content");
		currentLogFileResultData.absolute(startRow);
		for (long i = startRow; i <= endRow; i++) {
			TableRow row = tmpTable.insertRow();
			String line = currentLogFileResultData.getString(1);
			row.getField(1).setValue(stringNormalizer(line));
			currentLogFileResultData.next();
		}
		return new ResultData(tmpTable);
	}

	/**
	 * Returns the starting row used when accessing log file data
	 * @return the starting row used when accessing log file data
	 */
	public int getCurrentRow() {
		return currentRow;
	}

	/**
	 * The number of rows in the current log file
	 * @return the number of rows in the current log file
	 */
	public int getNumRows() {
		if (currentLogFileResultData != null)
			return currentLogFileResultData.getNumRows();
		else
			return 0;
	}

	/**
	 * Returns the current log file name
	 * @return the current log file name
	 */
	public String getLogFileName() {
		return currentLogFileName;
	}

	/**
	 * Returns the size of the current log file in bytes
	 * @return the size of the current log file in bytes
	 */
	public long getFileSize() {
		return currentFileSize;
	}

	private ResultData getEmptyResultData() {
		Table tmpTable = new Table("empty");
		tmpTable.addColumn(Table.TYPE_STRING, "content");
		return new ResultData(tmpTable);
	}

	/**
	 * This method checks the element value if it contains illegal characters and converts them
	 * to legal characters
	 * @param value String checked for illegal characters
	 * @return string containing only legal characters
	 */
	private static String stringNormalizer(String s) {
		StringBuffer str = new StringBuffer();

		int len = (s != null) ? s.length() : 0;
		for (int i = 0; i < len; i++) {
			char ch = s.charAt(i);
			switch (ch) {
				case '<' :
					{
						str.append("&lt;");
						break;
					}
				case '&' :
					{
						str.append("&amp;");
						break;
					}
				case '"' :
					{
						str.append("&quot;");
						break;
					}
				case '>' :
					{
						str.append("&gt;");
						break;
					}
				default :
					{
						str.append(ch);
					}
			}
		}
		return str.toString();
	}

	/**
	 * Initializes the Admin object by loading a log file into memory
	 * @param logFileName the name of the log file
	 * @param rowDelta Number of rows used when navigating the table
	 */
	public void init(String logFileName, int rowDelta) throws Exception {
		if (logFileName == null || rowDelta == 0 || filesInLocation == null)
			throw new Exception(
				"Error initializing logging Admin object. [logFileName]='"
					+ logFileName
					+ "' [navDelta]='"
					+ rowDelta
					+ "'");

		// even if the result is null, it will be parsed to string null.
		currentLogFilePath = (String) filesInLocation.get(logFileName);
		// get the path for the chosen logfile
		if (currentLogFilePath.equalsIgnoreCase("null")) {
			throw new Exception(
				"The log file name " + logFileName + " was not in the list");
		}

		currentRowDelta = rowDelta;
		this.setSessionLog(false);
		// load table into memory
		loadLogFile();

	}

	/**
	 * Initializes the Admin object by loading a log file into memory
	 * @param logFileName the name of the log file
	 * @param logPath The path where the file is located
	 * @param rowDelta Number of rows used when navigating the table
	 */
	public void initSessionLog(
		String logFileName,
		String logPath,
		int rowDelta)
		throws Exception {
		if (logFileName == null || rowDelta == 0 || logPath == null)
			throw new Exception(
				"Error initializing logging Admin object. [logFileName]='"
					+ logFileName
					+ "' [navDelta]='"
					+ rowDelta
					+ "'");

		// even if the result is null, it will be parsed to string null.
		currentLogFilePath = logPath;

		currentRowDelta = rowDelta;
		this.setSessionLog(true);
		// load table into memory
		loadLogFile();

	}

	/**
	 * Initializes the Admin object by loading a log file into memory
	 * @param currentLocation The location that should be used with the log file.
	 * @param logFileName the name of the log file
	 * @param rowDelta Number of rows used when navigating the table
	 */
	public void init(String currentLocation, String logFileName, int rowDelta)
		throws Exception {
		this.init(logFileName, rowDelta);
		if (currentLocation != null && !currentLocation.equals("")) {
			this.currentLocation = currentLocation;
		}
	}

	/**
	 * Creates a String containing a zipped representation of the file
	 * @param logFile an instance of the file that should be zipped
	 * @return the content of the zip file
	 */
	public static byte[] createZipFile(File logFile) {
		log.entering("createZipFile(File logFile)");
		log.debug("Start Zipping file " + logFile);

		byte b[] = new byte[1100];
		//    String zip = "";
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			//String logFilePath = getLogFilePath(logFileName);
			//	  We have already the whole file path
			FileInputStream fis = new FileInputStream(logFile);
			ZipOutputStream zout = new ZipOutputStream(baos);

			int numRead = 0;
			//      int size=0;

			zout.putNextEntry(new ZipEntry(logFile.getName()));

			while (numRead != -1) {
				numRead = fis.read(b);
				if (numRead != -1)
					zout.write(b, 0, numRead);
			}
			zout.closeEntry();
			zout.finish();

		} catch (Exception ex) {
			log.error(
				LogUtil.APPS_BUSINESS_LOGIC,
				"system.exception",
				new Object[] { "Error creating zip file : " + ex.toString()},
				null);
		}
		log.debug("zip file created.");
		log.exiting();

		return baos.toByteArray();
	}

	/**
	 * Returns the fully qualified directory path for the give log location
	 * @param locationName The name of the log location.
	 * @param destination The current destination
	 * @return the fully qualified directory path for the give log file
	 */
	private String getLogFilePathForLocation(
		String locationName,
		String destination)
		throws IOException {

		Location loc = getApplicationLocation(locationName);
		FileLog isaLog = null;

		if (loc != null) {
			if (!loc.getLogs().isEmpty()) {
				java.util.Iterator logs = loc.getLogs().iterator();
				Log currentLog = null;
				while (logs.hasNext()) {
					currentLog = (Log) logs.next();
					if (currentLog != null
						&& (currentLog instanceof FileLog)) {
						isaLog = (FileLog) currentLog;
						//check if the path is the same and exit
						if (isaLog.getLogName().equalsIgnoreCase(destination))
							break;
					}
				}
			}
		}
		return isaLog.getFilePathPattern();
	}

	/**
	 * Returns the registered {@link FileLog} object registered in the log configurator.
	 * @param loc - Location
	 * @param path - the destination/file log path
	 * @return the a FileLog if found otherwise null
	 */
	private FileLog getFileLog(Location loc, String path) {

		FileLog isaLog = null;
		boolean logFound = false;
		if (loc != null) {
			if (!loc.getLogs().isEmpty()) {
				java.util.Iterator logs = loc.getLogs().iterator();
				Log currentLog = null;
				while (logs.hasNext()) {
					currentLog = (Log) logs.next();
					if (currentLog != null
						&& (currentLog instanceof FileLog)) {
						isaLog = (FileLog) currentLog;
						//check if the path is the same and exit
						if (isaLog.getLogName().equalsIgnoreCase(path)) {
							logFound = true;
							break;
						}
					}
				}
			}
		}

		return logFound ? isaLog : null;
	}

	/***
	 * Deletes a Log Destination defined by the provided path and for the given Location
	 * @param locationName - the name of the Location
	 * @param path - destination path
	 * @return
	 */
	public boolean removeFileLog(String locationName, String path) {

		Location loc = getApplicationLocation(locationName);
		FileLog isaLog = this.getFileLog(loc, path);

		if (null != isaLog) {
			loc.removeLog(isaLog);
			locationChanged = true;
			return true;
		} else {
			return false;
		}

	}

	/**
	 * A helper to locate the Logging Location
	 * @param locationName - name of the Location
	 * @return the Location Object
	 */
	private Location getApplicationLocation(String locationName) {
		if (locationName == null || locationName == "") {
			if (LogConfiguratorSla.getLoggingPrefixName() != null) {
				//enable note 1032305
				locationName =
					LogConfiguratorSla.getLoggingPrefixName()
						+ "."
						+ "com.sap.isa";
			} else {
				locationName = "com.sap.isa";
			}
		}
		return Location.getLocation(locationName);
	}

	/**
		 * Returns the existing configuration settings. Check logging.jsp too
		 * @param locationName the location for that the configuration is needed. If the value is <code>null</code>, then the default location will be used.
		 * @return as a ResultData the existing Location configuration
		 */
	public ResultData getCurrentLogConfiguration(String locationName) {
		
		if (!locationChanged) {
			return currentLogConfig;
		}

		if (locationName == null) {
			locationName = currentLocation;
		}

		Table logConfig = new Table("logConfiguration");

		//generate the output table structure
		logConfig.addColumn(Table.TYPE_STRING, "Location");
		logConfig.addColumn(Table.TYPE_STRING, "EffectiveSeverity");
		logConfig.addColumn(Table.TYPE_STRING, "Destination");
		logConfig.addColumn(Table.TYPE_STRING, "Path");
		logConfig.addColumn(Table.TYPE_STRING, "Limit");
		logConfig.addColumn(Table.TYPE_STRING, "Count");
		logConfig.addColumn(Table.TYPE_STRING, "FormatterName");
		logConfig.addColumn(Table.TYPE_STRING, "FormatterPattern");
		logConfig.addColumn(Table.TYPE_STRING, "FormatterType");

		// get the Location	   
		Location loc = getApplicationLocation(locationName);

		if (loc != null) {

			if (!loc.getLogs().isEmpty()) {
				java.util.Iterator logs = loc.getLogs().iterator();
				Log currentLog = null;
				Formatter formatter = null;
				while (logs.hasNext()) {

					//Populate a new row and save the Location Name
					TableRow row = logConfig.insertRow();

					//Get the Log Destinations
					currentLog = (Log) logs.next();

					// I try to avoid calling synchronized methods to often.
					String currentLocationName = loc.getName();
					if (currentLocationName != null) {
						row.getField(1).setValue(currentLocationName);
					} else {
						row.getField(1).setValue("none");
					}

					//Work with the FileLogs
					if (currentLog != null && (currentLog instanceof FileLog)) {

						//for us the Log Severity is more important then the Location severity
						//might happen that because of a session trace the Location severity is
						//set to DEBUG, The Log/Destination Severity of the other Logs should
						//not be changed to DEBUG
						row.getField(2).setValue(Severity.toString(currentLog.getEffectiveSeverity()));
						if (((FileLog) currentLog).getName() != null)
							row.getField(3).setValue(((FileLog) currentLog).getName());

						if (((FileLog) currentLog).getPath() != null)
							row.getField(4).setValue(((FileLog) currentLog).getPath());

						//the size of every single log file
						row.getField(5).setValue(
							Integer.toString(((FileLog) currentLog).getLimit()));
						//number of log files to be generated
						row.getField(6).setValue(Integer.toString(((FileLog) currentLog).getCnt()));

						//Get the FORMATTER
						formatter = ((FileLog) currentLog).getFormatter();
						if (formatter != null) {
							row.getField(7).setValue(formatter.getFormatterName());
							row.getField(9).setValue(formatter.getFormatterType().toString());
							//IF TRACE FORMATTER get the pattern
							if (formatter.getFormatterType() == FormatterType.TRACEFORMAT) {
								row.getField(8).setValue(((TraceFormatter) formatter).getPattern());
							} else {
								row.getField(8).setValue("none");
							}
						}
					} else {
						//it is not a FILE-LOG - to be implemented
					}
				}
			} else {
				//add only the location data
				TableRow row = logConfig.insertRow();
				if (loc.getName() != null) {
					row.getField(1).setValue(loc.getName());
				} else {
					row.getField(1).setValue("none");
				}

				// get the severity of the Location (DEBUG LEVEL)
				row.getField(3).setValue(Severity.toString(loc.getEffectiveSeverity()));
				
				// put empty entries into the other fields
				for (int i = 4; i < 10; i++) {
					row.getField(i).setValue("");
				}
			}

		}
		currentLogConfig = new ResultData(logConfig);
		locationChanged = false;
		
		return currentLogConfig;
	}

	/**
	 * Returns the number of logs available for the current location. 
	 * @return The number of logs available for the current location.
	 */
	public int getNumOfLogLacations() {
		return getCurrentLogConfiguration(this.getCurrentLocation())
			.getNumRows();
	}

	/**
	 * Creates a new destination in the log configurator.
	 * @param rLocationIndex The index whitch should be modified.
	 * @param rSeverity The new severity of the location.
	 * @param rPath The path to the file where the logs should be written.
	 * @param rLimit The file size limit.
	 * @param rCounter The number of files for this location.
	 * @param rFormatterType The formatter to be used.
	 * @param rPattern The pattern for the formatter.
	 */
	public boolean saveLocationChanges(
		String rLocationIndex,
		String rSeverity,
		String rDestination,
		String rLimit,
		String rCounter,
		String rFormatterType,
		String rPattern) {

		currentLogConfig.absolute(Integer.parseInt(rLocationIndex));

		Location loc = getApplicationLocation(getCurrentLocation());
		try {
			if (loc != null) {
				log.debug(
					"Removing old log configuration to create a new one with the new configuration.");
				// remove the log that the user has modified.
				FileLog logToRemove =
					getFileLog(loc, currentLogConfig.getString("Destination"));
				if (logToRemove == null) {
					log.error(
						"Error occured finding file log for the requested destination. "
							+ "location destinastion = "
							+ rDestination);
					return false;
				}
				loc.removeLog(logToRemove);
				log.debug("Starting creating new log configuration.");
				return createNewDestination(
					getCurrentLocation(),
					rDestination,
					rSeverity,
					rLimit,
					rCounter,
					rFormatterType,
					rPattern);
			}
		} catch (Exception e) {
			log.error("Error occured while removing log. " + e.getMessage());
			return false;
		}
		log.error("Log location could not be found for the current location.");
		return false;
	}

	/**
	 * Creates a new destination in the log configurator.
	 * @param locationName The location name.
	 * @param mLogFilePath The log file path.
	 * @param severity The default severity.
	 * @param limit The file size limit.
	 * @param count The number of files.
	 * @param formaterType The formatter to be used.
	 * @param mFormatterPattern The pattern for the formatter.
	 */
	public boolean createNewDestination(
		String locationName,
		String mLogFilePath,
		String severity,
		String limit,
		String count,
		String formatterType,
		String mFormatterPattern) {

		Location loc = getApplicationLocation(locationName);
		FileLog newLog = null;

		if (loc != null) {
			Formatter formatter = null;
			if (formatterType != null) {

				if (formatterType.equalsIgnoreCase("TraceFormat")) {
					formatter = new TraceFormatter(mFormatterPattern);
				} else if (formatterType.equalsIgnoreCase("ListFormat")) {
					formatter = new ListFormatter();
				} else if (formatterType.equalsIgnoreCase("XMLFormat")) {
					formatter = new XMLFormatter();
				}
			}
			File pathHelper = new File(mLogFilePath);
			String mFileLogPattern = null;
			try {
				mFileLogPattern = pathHelper.getCanonicalPath();
			} catch (IOException e) {
				log.error(
					"An error occured while retrieving canonical path of location: "
						+ mLogFilePath);
				return false;
			}

			int mLimitInt = 0;
			int mCnt = 0;
			int mSevertiy = 0;
			try {
				mLimitInt = Integer.parseInt(limit);
				mCnt = Integer.parseInt(count);
				mSevertiy = Integer.parseInt(severity);
			} catch (NumberFormatException nfex) {
				log.debug(
					"One of the entered values could not be parsed to an integer: "
						+ "file size limit: "
						+ limit
						+ " file count: "
						+ count
						+ " severity: "
						+ severity
						+ "\t Default entries will be used.");
				mLimitInt = 10000000;
				mCnt = 5;
				mSevertiy = 3;
			}
			if (mLimitInt != 0 && mCnt != 0)
				newLog =
					new FileLog(mFileLogPattern, mLimitInt, mCnt, formatter);
			else
				newLog = new FileLog(mFileLogPattern, formatter);

			newLog.setEffectiveSeverity(mSevertiy);

			log.debug("Adding new destination to the log configuration");
			try {
				loc.addLog(newLog);
				loc.setEffectiveSeverity(mSevertiy);
			} catch (Exception e) {
				log.debug(
					"Error open the new file. Happens when the file has not been generated yet. The logging will work anyway");
			}

			locationChanged = true;
			return true;
		}
		log.error("Log location could not be found for the current location.");
		return false;
	}

	/**
	 * Returns a list of all Log files created in the log directory
	 * @return a Table of all generated log files
	 */
	public Table getLogFilesForLocation(String location, String destination)
		throws Exception {

		File file = null;
		String path = null;
		try {
			//get the directory where all Log files are saved
			path = getLogFilePathForLocation(location, destination);
			file = new File(getLogFilePathForLocation(location, destination));

		} catch (Exception fnfex) {
			log.error(
				"file couldn't be found, please check if the file/folder exsists: "
					+ path
					+ "\n"
					+ fnfex.toString());
			throw new Exception(fnfex.toString());
		}

		//generate a table structure for the output
		Table logFilesTable = new Table("logfiles");
		logFilesTable.addColumn(Table.TYPE_STRING, "filename");
		logFilesTable.addColumn(Table.TYPE_STRING, "filesize");
		logFilesTable.addColumn(Table.TYPE_STRING, "lastmodified");
		logFilesTable.addColumn(Table.TYPE_STRING, "path");

		try {
			//get all files from the directory
			File[] dirFiles = file.listFiles();
			// empty the old file list
			filesInLocation.clear();
			for (int i = 0; i < dirFiles.length; i++) {
				if (dirFiles[i].isFile()) {
					// add the file into the table
					TableRow row = logFilesTable.insertRow();
					row.getField(1).setValue(dirFiles[i].getName());
					row.getField(2).setValue(
						String.valueOf(dirFiles[i].length()));
					row.getField(3).setValue(
						new Date((dirFiles[i].lastModified())));
					row.getField(4).setValue(dirFiles[i].getCanonicalPath());
					// add the file also to the hashmap for displaying
					filesInLocation.put(
						dirFiles[i].getName(),
						dirFiles[i].getCanonicalPath());
				}
			}
		} catch (Exception ex) {
			log.error(
				LogUtil.APPS_BUSINESS_LOGIC,
				"system.exception",
				new Object[] {
					"Could not find directory: "
						+ getLogFilePathForLocation(location, destination)},
				null);
			return null;
		}
		return logFilesTable;
	}

	/**
	 * Returns the path to the requested logfile name. It returns <code>null</code> or
	 * the string null if the path has not been loaded. Therefore the method getLogFilesForLocation
	 * must be called.
	 * @param logfileName The name of the logfile to which path we need.
	 * @return Returns the path to the requested logfile name.
	 */
	public String getLogFilePathForLogFileName(String logfileName) {
		if (filesInLocation.size() > 0) {
			return (String) filesInLocation.get(logfileName);
		}
		return null;
	}
	/**
	 * Returns the current location.
	 * @return The current location.
	 */
	public String getCurrentLocation() {
		return currentLocation;
	}

	/**
	 * Sets the location the user wants to work now.
	 * @param string The location the user wants to work now.
	 */
	public void setCurrentLocation(String string) {
		currentLocation = string;
		locationChanged = true;
	}

	/*###################################################################################
	 * START JSP helper methods.
	 */
	/**
	 * Returns a html string for a selection box. E.g. <option value="100">Debug</option>  
	 * @param severity The severity that should be selected as default.
	 * @return a html string for a selection box. E.g. <option value="100">Debug</option>
	 */
	public String getSeverityOptions(String severity) {
		StringBuffer result = new StringBuffer();
		List severityList = Severity.VALUES;
		String currValue = null;

		for (int i = 0; i < severityList.size(); i++) {
			currValue = severityList.get(i).toString();
			result.append(
				"<option value=\"" + Severity.parse(currValue) + "\" ");
			if (severity.equalsIgnoreCase(currValue)) {
				result.append("selected ");
			}
			result.append(" >" + currValue + "</option>\n");
		}

		return result.toString();
	}

	/**
	 * Adds a space in a string after a specified length. 
	 * @param inString The string that has to be modified.
	 * @param space_after The number at what place a space should be added.
	 * @return A string with spaces after a specified length.
	 */
	public static String wrap(String inString, int space_after) {
		if (inString == null) {
			return null;
		}
		StringBuffer result = new StringBuffer();
		int loops = inString.length() / space_after;

		if (inString.length() > space_after) {
			result.append(inString.substring(0, space_after) + " ");

			for (int i = 1; i < loops; i++) {
				result.append(
					inString.substring(space_after * i, space_after * (i + 1))
						+ " ");
			}
			result.append(inString.substring(space_after * loops));
		} else {
			return inString;
		}

		return result.toString();
	}

	/*###################################################################################
	 * END JSP helper methods.
	 */

	public boolean isSessionLog() {
		return isSessionLog;
	}

	public void setSessionLog(boolean isSessionLog) {
		this.isSessionLog = isSessionLog;
	}
}