/*
 * Created on 21.11.2003
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.core.logging.sessiontrace;

import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import com.sap.tc.logging.FileLog;
import com.sap.tc.logging.Filter;
import com.sap.tc.logging.Formatter;
import com.sap.tc.logging.ListFormatter;
import com.sap.tc.logging.Location;
import com.sap.tc.logging.LogRecord;
import com.sap.tc.logging.Severity;

/**
 * Session trace filter. Only records of the current web session are traced 
 *
 */
public class SessionTraceLoggingFilter implements Filter, HttpSessionBindingListener {

	private static final Location LOCATION = Location.getLocation(SessionTraceLoggingFilter.class); 


	private String mSessionId;
	private Set mRootLocations = new HashSet();
	private Set mRootLoggers = new HashSet();	
	private FileLog mSessionFileLog;
	private String mLocation;
	private String mFileDest;
	private static Map mOriginalSeverities = new HashMap();
	
	public SessionTraceLoggingFilter(String sessionId, String location, String fileDest) {
		mSessionId = sessionId;
		mLocation = location;
		mFileDest = fileDest;
		mRootLocations.add(location);
	}
	

	/**
	  * Callback for the servlet environment to notify this object that
	  * it is bound to the session context. This method is not implemented and
	  * does nothing.
	  *
	  * @param e Event
	  */
	 public void valueBound(HttpSessionBindingEvent e) {
		registerLogger();
		String METHOD = "valueBound(HttpSessionBindingEvent e)";
		LOCATION.debugT(METHOD, "bound", null); 	
	 }

	 /**
	  * Callback for the servlet environment to notify this object that
	  * it is going to be unbound from the session context.
	  *
	  * @param e Event
	  */
	 public void valueUnbound(HttpSessionBindingEvent e) {
		restoreOriginalLocationSeverities();
		String METHOD = "valueUnbound(HttpSessionBindingEvent e)";
		LOCATION.debugT(METHOD, "bound", null); 	
	 }

	/**
	 * Called by the Logging  API. Decides whether the message will pass the filter.
	 * @param record
	 */
	public boolean beLogged(LogRecord record) {
	  synchronized (record) {
		String id = ThreadContainer.getThreadSessionId();
	  	if (id.equals(mSessionId))
	  		return true;
	  }
	  return false;
	}


	 /**
	  * Registers an additional logger. This file logger logs all log entries
	  * for the session accociated with this object
	  */
	 private void registerLogger() {

		String METHOD = "registerLogger()";
		LOCATION.debugT(METHOD, "Session tracing activated", null); 	
	
	   /*
		 Programatically set the following settings
		 log[session]           = FileLog
		 log[session].severity  = DEBUG
		 log[session].pattern   = path/session.log
		 log[session].limit     = 10000000
		 log[session].cnt       = 10
		 log[session].filters   = com.sap.isa.core.logging.sla.SessionFilter
		 log[session].formatter = formatter[session]
	   */
	
	   // get root location of ISA application
	
	   Formatter listFormatter = new ListFormatter();
	   mSessionFileLog = new FileLog(mFileDest + "/" + getSessionLogFileName(), listFormatter);
	   // instantiate a Session Filter valid only for this session

	   mSessionFileLog.addFilter(this);
	   mSessionFileLog.setEffectiveSeverity(Severity.DEBUG);
	
	   changeOriginalLocationSeverities();
		//	   add file logger to locations
	   
	   
	   
   }
	   
	/**
	 * Returns the file name where the session is loggen in
	 * @return file name
	 */
	private String getSessionLogFileName() {
	  Calendar calendar = Calendar.getInstance(Locale.ENGLISH);
	  StringBuffer timeStamp = new StringBuffer("session-");
	  timeStamp.append(calendar.get(Calendar.YEAR));
	  timeStamp.append("-");
	  timeStamp.append((calendar.get(Calendar.MONTH) + 1));
	  timeStamp.append("-");
	  timeStamp.append((calendar.get(Calendar.DAY_OF_MONTH)));
	  timeStamp.append("_");
	  timeStamp.append(calendar.get(Calendar.HOUR_OF_DAY));
	  timeStamp.append("-");
	  timeStamp.append(calendar.get(Calendar.MINUTE));
	  timeStamp.append("_");

	  return timeStamp.toString() + mSessionId + ".log";
   }
	
	private void changeOriginalLocationSeverities() {

	String METHOD = "changeOriginalLocationSeverities()";
	
			
	  for (Iterator iter = mRootLocations.iterator(); iter.hasNext();) {
		String loc = (String)iter.next();
		Location location = Location.getLocation(loc);
		if (!location.beDebug()) {
		  // log level is not sufficient, change to DEBUG
		  String originalSeverity = String.valueOf(location.getEffectiveSeverity());

		  LOCATION.debugT(METHOD, "Storing original Severity [location]='{0}' [severity]='{1}'", new Object[] {loc, originalSeverity });

		  mOriginalSeverities.put(loc, originalSeverity);
		  // change severity to DEBUG
		  location.setEffectiveSeverity(Severity.DEBUG);
		}
		location.addLog(mSessionFileLog);
	  }
	}

	private void restoreOriginalLocationSeverities() {

	String METHOD = "restoreOriginalLocationSeverities()";		
		
	  for (Iterator iter = mOriginalSeverities.keySet().iterator(); iter.hasNext();) {
		String loc = (String)iter.next();
		Location location = Location.getLocation(loc);
		String severityString = (String)mOriginalSeverities.get(loc);
		int severity = 0;
		try {
		  severity = Integer.parseInt(severityString);
		} catch (NumberFormatException nfex) {
		  // should not happen
		  LOCATION.debugT(METHOD, "Error while parsing original [severity]='{0}' of [location]='{1}': [error]='{2}'", new Object[] {severityString, location, nfex.toString() });
		  LOCATION.throwing(METHOD, nfex);
		}
		LOCATION.debugT(METHOD, "Restoring original [location]='{0}' [severity])='{1}'", new Object[] { location, severityString}); 
	
		location.setEffectiveSeverity(severity);
		location.removeLog(mSessionFileLog);
	  }
	}
}
	 
	 
