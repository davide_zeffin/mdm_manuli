package com.sap.isa.core.logging.sessiontrace;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import com.sap.tc.logging.Location;

/**
 * @author d025909
 *
 */
public class SessionTraceServletFilter implements Filter {
	
	private static final Location LOCATION = Location.getLocation(SessionTraceServletFilter.class);

	private static Map mSessionTraces = new HashMap();
	/**
	 * ID used to store logging filter in the session
	 */
	public static final String LOGGING_FILTER = "SessionTraceLoggingFilter.logging.isa.crm.sap.com";

	/**
	 * Id of request parameter used to turn on session tracing
	 */
	public static final String SESSION_TRACE_ON = "on.sessiontrace";

	/**
	 * Name of location for which session trace should be enabled
	 */
	public static final String SESSION_TRACE_LOCATION="location.sessiontrace";
	
	/**
	 * Log File destination of session trace
	 */
	public static final String SESSION_TRACE_PATTERN = "pattern.sessiontrace";


	private String mLocation = "com.sap.isa";
	private String mPattern = "c:/";
 
	
	/* (non-Javadoc)
	 * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
	 */
	public synchronized void init(FilterConfig config) throws ServletException {
		String METHOD = "init(FilterConfig config)";
		LOCATION.debugT(METHOD, "init Filter", null); 	
		try {
			this.wait(100000);
		} catch (Throwable t) {
			t.printStackTrace();
		}
		
		// TODO Auto-generated method stub

	}

	public static void unregisterSessionTrace(String sessionId) {
		mSessionTraces.remove(sessionId);
	}

	/* (non-Javadoc)
	 * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
	 */
	public void doFilter(
		ServletRequest request,
		ServletResponse response,
		FilterChain chain)
		throws IOException, ServletException {

		String METHOD = "doFilter()";
		 	
		
		HttpServletRequest req = (HttpServletRequest)request;
		
		String sessionTrace = req.getParameter(SESSION_TRACE_ON);
		if (req.getParameter(SESSION_TRACE_LOCATION) != null)
			mLocation = req.getParameter(SESSION_TRACE_LOCATION);
		
		LOCATION.debugT(METHOD, "[location]='{0}'", new Object[] { mLocation });

		
		if (req.getParameter(SESSION_TRACE_PATTERN) != null)
			mPattern = req.getParameter(SESSION_TRACE_PATTERN);
		
		LOCATION.debugT(METHOD, "[pattern]='{0}'", new Object[] { mPattern });		
			
		if (sessionTrace != null) {
			LOCATION.debugT(METHOD, "[sessionrequest] found. Setting filter into session", null);
			String id = req.getSession().getId();
			SessionTraceLoggingFilter filter = new SessionTraceLoggingFilter(id, mLocation, mPattern);
			mSessionTraces.put(id, filter);
			SessionTraceContextListener.setLoggerSessionId(id);
			req.getSession().setAttribute(LOGGING_FILTER, filter);
			ThreadContainer.setThreadSessionId(id);
		} else {
			String id = req.getSession().getId();
			if (mSessionTraces.containsKey(id) && req.getSession().getAttribute(LOGGING_FILTER) == null) {
				LOCATION.debugT(METHOD, "Session active for tracing but no filter in session. Adding filter to session", null);
				req.getSession().setAttribute(LOGGING_FILTER, mSessionTraces.get(id));
				ThreadContainer.setThreadSessionId(id);
			}
				
		}
		chain.doFilter(request, response);
	}

	/* (non-Javadoc)
	 * @see javax.servlet.Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub

	}

}
