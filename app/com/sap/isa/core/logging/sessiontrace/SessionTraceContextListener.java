/*
 * Created on 21.11.2003
 *
 */
package com.sap.isa.core.logging.sessiontrace;

import java.util.HashSet;
import java.util.Set;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import com.sap.tc.logging.Location;

/**
 * @author d025909
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class SessionTraceContextListener
	implements ServletContextListener, HttpSessionListener {

	/**
	 * ID used to store logging filter in the session
	 */
	public static final String LOGGING_FILTER = "SessionTraceLoggingFilter.logging.isa.crm.sap.com";

	private static final Location LOCATION = Location.getLocation(SessionTraceContextListener.class);

	private static Set mSessionLogger = new HashSet();

	public static void setLoggerSessionId(String id) {
		mSessionLogger.add(id);
	}

	/* (non-Javadoc)
	 * @see javax.servlet.ServletContextListener#contextInitialized(javax.servlet.ServletContextEvent)
	 */
	public void contextInitialized(ServletContextEvent arg0) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see javax.servlet.ServletContextListener#contextDestroyed(javax.servlet.ServletContextEvent)
	 */
	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpSessionListener#sessionCreated(javax.servlet.http.HttpSessionEvent)
	 */
	public void sessionCreated(HttpSessionEvent sessionEvent) {
		String METHOD = "sessionCreated(HttpSessionEvent sessionEvent)";
		LOCATION.debugT(METHOD, "sesion created event: [sessionid]='{0}'", new Object[] { sessionEvent.getSession().getId()}); 	
	}

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpSessionListener#sessionDestroyed(javax.servlet.http.HttpSessionEvent)
	 */
	public void sessionDestroyed(HttpSessionEvent sessionEvent) {
		String id = sessionEvent.getSession().getId();
		if (mSessionLogger.contains(id)) {
			String METHOD = "sessionDestroyed(HttpSessionEvent sessionEvent)";
			LOCATION.debugT(METHOD, "unregister session trace: [sessionid]='{0}'", new Object[] { sessionEvent.getSession().getId()}); 	
			mSessionLogger.remove(id);
			SessionTraceServletFilter.unregisterSessionTrace(id);
		}
		
	}

}
