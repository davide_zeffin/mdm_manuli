/*
 *
 */
package com.sap.isa.core.logging;

import org.apache.commons.logging.Log;

/**
 * Implementation of the struts log interface. Redirects all logging
 * informations to SAP logging API
 *
 */
public class StrutsLog implements Log {
	
	protected static final String ORG_APACHE_STRUTS_LOG = "org.apache.commons.logging.Log";
	
	IsaLocation isaLoc;
	
	/**
	 * @param string
	 */
	public StrutsLog(String name) {
		// isaLoc = IsaLocation.getInstance(name);
	    // This is easier to cleanup in IsaLocation.terminate
		isaLoc = IsaLocation.getInstance(ORG_APACHE_STRUTS_LOG);
	}

	/**
	 * 
	 */
	public StrutsLog() {
		super();
		isaLoc = IsaLocation.getInstance(ORG_APACHE_STRUTS_LOG);
	}

	/* (non-Javadoc)
	 * @see org.apache.commons.logging.Log#isDebugEnabled()
	 */
	public boolean isDebugEnabled() {
		
		return isaLoc.isDebugEnabled();
	}

	/* (non-Javadoc)
	 * @see org.apache.commons.logging.Log#isErrorEnabled()
	 */
	public boolean isErrorEnabled() {
		return true;
	}

	/* (non-Javadoc)
	 * @see org.apache.commons.logging.Log#isFatalEnabled()
	 */
	public boolean isFatalEnabled() {
		return true;
	}

	/* (non-Javadoc)
	 * @see org.apache.commons.logging.Log#isInfoEnabled()
	 */
	public boolean isInfoEnabled() {
		return isaLoc.isInfoEnabled();
	}

	/* (non-Javadoc)
	 * @see org.apache.commons.logging.Log#isTraceEnabled()
	 */
	public boolean isTraceEnabled() {
		return isaLoc.isDebugEnabled();
	}

	/* (non-Javadoc)
	 * @see org.apache.commons.logging.Log#isWarnEnabled()
	 */
	public boolean isWarnEnabled() {
		
		return isaLoc.isInfoEnabled();
	}

	/* (non-Javadoc)
	 * @see org.apache.commons.logging.Log#trace(java.lang.Object)
	 */
	public void trace(Object message) {
		isaLoc.debug(message);

	}

	/* (non-Javadoc)
	 * @see org.apache.commons.logging.Log#trace(java.lang.Object, java.lang.Throwable)
	 */
	public void trace(Object message, Throwable throwable) {
		isaLoc.debug(message, throwable );

	}

	/* (non-Javadoc)
	 * @see org.apache.commons.logging.Log#debug(java.lang.Object)
	 */
	public void debug(Object message) {
	isaLoc.debug(message);

	}

	/* (non-Javadoc)
	 * @see org.apache.commons.logging.Log#debug(java.lang.Object, java.lang.Throwable)
	 */
	public void debug(Object message, Throwable throwable) {
		isaLoc.debug(message, throwable );
	}

	/* (non-Javadoc)
	 * @see org.apache.commons.logging.Log#info(java.lang.Object)
	 */
	public void info(Object message) {
		isaLoc.info(LogUtil.APPS_COMMON_INFRASTRUCTURE ,"system.forward.info", new Object[]{message});
		}

	/* (non-Javadoc)
	 * @see org.apache.commons.logging.Log#info(java.lang.Object, java.lang.Throwable)
	 */
	public void info(Object message, Throwable throwable) {
		isaLoc.info(LogUtil.APPS_COMMON_INFRASTRUCTURE ,"system.forward.info", new Object[]{message});

	}

	/* (non-Javadoc)
	 * @see org.apache.commons.logging.Log#warn(java.lang.Object)
	 */
	public void warn(Object message) {
		isaLoc.info(LogUtil.APPS_COMMON_INFRASTRUCTURE ,"system.forward.info", new Object[]{message});

	}

	/* (non-Javadoc)
	 * @see org.apache.commons.logging.Log#warn(java.lang.Object, java.lang.Throwable)
	 */
	public void warn(Object message, Throwable throwable) {
		isaLoc.warn(LogUtil.APPS_COMMON_INFRASTRUCTURE ,"system.forward.info", new Object[]{message});
	}

	/* (non-Javadoc)
	 * @see org.apache.commons.logging.Log#error(java.lang.Object)
	 */
	public void error(Object message) {
		isaLoc.error(LogUtil.APPS_COMMON_INFRASTRUCTURE ,"system.forward.info", new Object[]{message});

	}

	/* (non-Javadoc)
	 * @see org.apache.commons.logging.Log#error(java.lang.Object, java.lang.Throwable)
	 */
	public void error(Object arg0, Throwable message) {
		isaLoc.error(LogUtil.APPS_COMMON_INFRASTRUCTURE ,"system.forward.info", new Object[]{message});
	}

	/* (non-Javadoc)
	 * @see org.apache.commons.logging.Log#fatal(java.lang.Object)
	 */
	public void fatal(Object message) {
		isaLoc.fatal(LogUtil.APPS_COMMON_INFRASTRUCTURE ,"system.forward.info", new Object[]{message});

	}

	/* (non-Javadoc)
	 * @see org.apache.commons.logging.Log#fatal(java.lang.Object, java.lang.Throwable)
	 */
	public void fatal(Object arg0, Throwable message) {
		isaLoc.fatal(LogUtil.APPS_COMMON_INFRASTRUCTURE ,"system.forward.info", new Object[]{message});

	}

}
