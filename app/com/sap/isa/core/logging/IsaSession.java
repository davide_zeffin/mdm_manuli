/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Thorsten Kampp
  Created:      15. Februar 2001, 13:06

  $Revision: #2 $
  $Date: 2001/07/05 $
*****************************************************************************/

package com.sap.isa.core.logging;


import java.util.HashMap;


/**
 * The IsaSession class provides access to the current SessionId throughout
 * the Application.
 *
 * fm: A generic store for objects (in the scope of a thread) implemented
 *
 */
public final class IsaSession {


    /**
      * Container to store the SessionId's of the currently running
      * threads.
    */
    private static ThreadProperties container = new ThreadProperties();

    public static final String SESSION_ID_KEY = "_isa_sessionID";



    /**
      * It is not allowed to instatiate objects of type IsaSession
    */
    private IsaSession() {
    }


    /**
     * Sets the SessionId of the current thread.
     *
     * @param sessionId current Id, e.g. from HttpSession
    */
    public static void setThreadSessionId(String sessionId) {   
         container.setProperty(SESSION_ID_KEY, sessionId);     
    }

    /**
     * Releases the Session Logging
     */
    public static void releaseThreadSessionId() {
         setThreadSessionId("released");
    }

    /**
      * Returns the SessionId of the current thread.
      *
      * @return the Id from HttpSession
     */
    public static String getThreadSessionId() {
        
         String value = (String)container.getProperty(SESSION_ID_KEY);     
         return value==null?"undefined":value;
    }


    public static void setAttribute(String name, Object value) {
         container.setProperty(name, value);     
    }
    public static Object getAttribute(String name) {
         return container.getProperty(name);     
    }

 
    private static class ThreadProperties extends ThreadLocal {
     
         HashMap props = new HashMap();
      
         public void setProperty(String name, Object value) {
              if (value != null)
                  props.put(name, value);
              else
                  props.remove(name);                  
         }
      
         public Object getProperty(String name) {
              return props.get(name);
         }
    }

}

