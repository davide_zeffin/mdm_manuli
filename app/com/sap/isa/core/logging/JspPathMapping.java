/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Thorsten Kampp
  Created:      4. Februar 2001, 13:06

  $Revision: #2 $
  $Date: 2001/07/04 $
*****************************************************************************/

package com.sap.isa.core.logging;

import java.util.Hashtable;
import java.util.StringTokenizer;

/**
 * Class that stores the JSP -> CategoryMapping, e.g. there will be mapping
 * that allows logging from within JSP pages.
 *
 * According to the path of the JSP pages, the logging calls will be made within
 * a specific (mapped) location/category.
 */
public class JspPathMapping {

    private static IsaLocation 	log        = null;
    private static Hashtable    jspMapping = null;

    static {
    	log = IsaLocation.getInstance("com.sap.isa.core.logging.JspPathMapping");
    	jspMapping = new Hashtable();
    }

    /**
     * Adds a mapping between path (url) and a named category (log4j-term)
     *
     * @param url      the path (e.g. / for root, /catalog/ for catalog)
     * @param catName  name of the category that should be used
     */
    public static void addMapping(String url, String catName) {
    	jspMapping.put((Object)url, (Object)catName);
    	log.config("Added mapping from [" + url + "] to [" + catName + "]");
    }

    /**
     * This method return for a given url, a mapped logging
     * category.
     *
     * @param 	url 	a full qualified url
     * @return 	String 	the name for the category
     */
    public static String getCategoryNameForURI(String url) {
        int i = 0;
        String cat = null;

        StringTokenizer sto = new StringTokenizer(url, "/", true);
        String all[] = new String[sto.countTokens()];
        while(sto.hasMoreTokens()) {
            if(i!=0) {
             all[i] = all[i-1] + sto.nextToken();
            } else {
             all[i] = sto.nextToken();
            }
            i++;
        }

        for(i=all.length-1;i>=0;i--) {
            cat = (String)jspMapping.get((Object)all[i]);
            if(cat != null) {
                return cat;
            }
        }
        return null;
    }

}