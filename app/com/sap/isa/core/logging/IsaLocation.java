package com.sap.isa.core.logging;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.sap.tc.logging.Category;


/**
 * The IsaLocation is a facade for the SAP logging API
 *
 * You should always call the getInstance method of IsaLocation to obtain
 * an concrete implementation of an IsaLocation Object. Currently there are
 * three available implementation.<br>
 *
 */
public class IsaLocation {
//  private static List allocatedClasses= new ArrayList();

  /**
   * Default Logging API. This value is subject to change in later
   * releases.
   */
  private static int loggingAPI = LogConfigurator.ISALOCATION_SAPMARKETS;
  private static IsaLocation log= new com.sap.isa.core.logging.sla.IsaLocationSla(IsaLocation.class.getName()); 

  /**
   * Please <b>do not</b> construct IsaLocation object's manually.
   * Please use instead <b>IsaLoaction.getInstance()</b> to obtain a specialized
   * object of IsaLocation.
   */
  public IsaLocation() {
  }
 
  /**
   * <p>The method to retrieve the logging object. The name should
   * contain the packagename and classname. 
   * @param name - the name of the current location.
   */
  public static IsaLocation getInstance(String name) {
//  	    try {
//			Class clazz = IsaLocation.class.getClassLoader().loadClass(name);
////			log.error("loading class: " + name);
//			allocatedClasses.add(clazz);
//		} catch (ClassNotFoundException e) {
//			if(log.isDebugEnabled())
//			    log.debug("Add class " + name + " failed ", e);
//		}
        return new com.sap.isa.core.logging.sla.IsaLocationSla(name);
  } 

  // --------------------------------------------------------------------------
  // GENERIC
  // --------------------------------------------------------------------------

  /**
   * Generic logging routine. This implementation does nothing.
   *
   * @param priority  the priority to be logged.
   * @param key       the key from the struts Messagebundle
   * @param obj       an array of parameters for internationalized
   * @param t         an exception
   */
  public void log(Object priority, Object key, Object obj[], Throwable t) {
  }


  // --------------------------------------------------------------------------
  // LOG FATAL
  // --------------------------------------------------------------------------
  /**
   * Logs a message object with the FATAL priority.
   *
   * @param category category of this log
   * @param key The message key. There must be a corresponding key in the message resources
   */
  public void fatal(Category category, Object key) {
  }

  /**
   * Logs a message object with the FATAL priority together with a stack trace
   *
   * @param category category of this log
   * @param key The message key. There must be a corresponding key in the message resources
   * @param t Throwable, the superclass of all errors and exceptions
   */
  public void fatal(Category category, Object key, Throwable t) {
  }

  /**
   * Logs a message object with the FATAL priority 
   *
   * @param category category of this log
   * @param key The message key. There must be a corresponding key in the message resources
   * @param args Arguments as object references
   */
  public void fatal(Category category, Object key, Object[] args) {
  }



  /**
   * Logs a message object with the FATAL priority together with a stack trace
   *
   * @param category category of this log
   * @param key The message key. There must be a corresponding key in the message resources
   * @param args Arguments as object references
   * @param t Throwable, the superclass of all errors and exceptions
   */
  public void fatal(Category category, Object key, Object[] args, Throwable t) {
  }


  // --------------------------------------------------------------------------
  // FATAL
  // --------------------------------------------------------------------------

  /**
   * Logging routine for FATAL entries.
   *
   * @param message   the key to be logged
   */
  public void fatal(Object message) {
  }

  /**
   * Logging routine for FATAL entries.
   *
   * @param message   the key to be logged
   * @param t         an occuring exception
   */
  public void fatal(Object message, Throwable t) {
  }
  public void fatal(Object message, Object params[], Throwable t) {
  }


  // --------------------------------------------------------------------------
  // PATH TRACES
  // --------------------------------------------------------------------------

  /**
   * Should be used when an exception is throwed.
   * The exception is written using its method toString.
   * @param msg   the key to be logged
   * @param t         an occuring exception
   */
  public void throwing(String msg, Throwable t) {
  }
  
  /**
   * Should be used when an exception is throwed.
   * The exception is written using its method toString. 
   * @param t         an occuring exception
   */
  public void throwing(Throwable t) {
  }

  /**
   * Traces message of severity Severity.PATH and appends a string denoting a sublocation to the name of this location. 
   * This method must be balanced with a call to exiting when leaving the traced method, for example exiting().
   * @param method name of sublocation
   */
  public void entering(String method) {
  }
  /**
   * Traces message of group severity which indicates that execution is about to leave 
   * this method location. This method is to be balanced with a call to the method entering().
   */
  public void exiting() {
  }


  // --------------------------------------------------------------------------
  // LOG ERROR 
  // --------------------------------------------------------------------------
  /**
   * Logs a message object with the ERROR priority.
   *
   * @param category category of this log
   * @param key The message key. There must be a corresponding key in the message resources
   */
  public void error(Category category, Object key) {
  }

  /**
   * Logs a message object with the ERROR priority.
   *
   * @param category category of this log
   * @param key The message key. There must be a corresponding key in the message resources
   * @param args Arguments as object references
   * @param t Throwable, the superclass of all errors and exceptions* 
   */
  public void error(Category category, Object key, Object[] args, Throwable t) {
  }
  
  /**
   * Logs a message object with the ERROR priority together with a stack trace
   *
   * @param category category of this log
   * @param key The message key. There must be a corresponding key in the message resources
   * @param t Throwable, the superclass of all errors and exceptions
   */
  public void error(Category category, Object key, Throwable t) {
  }

  /**
   * Logs a message object with the ERROR priority.
   *
   * @param category category of this log
   * @param key The message key. There must be a corresponding key in the message resources
   * @param args Arguments as object references
   */
  public void error(Category category, Object key, Object[] args) {
  }


  // --------------------------------------------------------------------------
  // ERROR
  // --------------------------------------------------------------------------

  /**
   * Logging routine for ERROR messages.
   *
   * @param message   the key to be logged
   */
  public void error(Object message) {
  }

  /**
   * Logging routine for ERROR messages.
   *
   * @param message   the key to be logged
   * @param t         an occuring exception
   */
  public void error(Object message, Throwable t) {
  }

  public void error(Object message, Object params[], Throwable t) {
  }

  // --------------------------------------------------------------------------
  // LOG WARNING
  // --------------------------------------------------------------------------
  /**
   * Logs a message object with the WARNING priority.
   *
   * @param category category of this log
   * @param key The message key. There must be a corresponding key in the message resources
   */
  public void warn(Category category, Object key) {
  }

  /**
   * Logs a message object with the WARNING priority.
   *
   * @param category category of this log
   * @param key The message key. There must be a corresponding key in the message resources
   * @param args Arguments as object references
   */
  public void warn(Category category, Object key, Object[] args) {
  }

  /**
   * Logs a message object with the WARNING priority together with a stack trace
   *
   * @param category category of this log
   * @param key The message key. There must be a corresponding key in the message resources
   * @param t Throwable, the superclass of all errors and exceptions
   */
  public void warn(Category category, Object key, Throwable t) {
  }

  /**
   * Logs a message object with the WARN priority together with a stack trace
   *
   * @param category category of this log
   * @param key The message key. There must be a corresponding key in the message resources
   * @param args Arguments as object references
   * @param t Throwable, the superclass of all errors and exceptions
   */
  public void warn(Category category, Object key, Object[] args, Throwable t) {
  }

  // --------------------------------------------------------------------------
  // WARNING
  // --------------------------------------------------------------------------

  /**
   * Logging routine for WARNINGS
   *
   * @param message   the key to be logged
   */
  public void warn(Object message) {
  }

  /**
   * Logging routine for WARNINGS
   *
   * @param message   the key to be logged
   * @param t         an occuring exception
   */
  public void warn(Object message, Throwable t) {
  }
  public void warn(Object message, Object params[], Throwable t) {
  }

  // --------------------------------------------------------------------------
  // LOG INFO
  // --------------------------------------------------------------------------
  /**
   * Logs a message object with the INFO priority.
   *
   * @param category category of this log
   * @param key The message key. There must be a corresponding key in the message resources
   */
  public void info(Category category, Object key) {
  }

  /**
   * Logs a message object with the ERROR priority 
   *
   * @param category category of this log
   * @param key The message key. There must be a corresponding key in the message resources
   * @param args Arguments as object references
   */
  public void info(Category category, Object key, Object[] args) {
  }


  // --------------------------------------------------------------------------
  // INFO
  // --------------------------------------------------------------------------

  /**
   * Logging routine for INFOS
   *
   * @param message   the key to be logged
   */
  public void info(Object message) {
  }

  /**
   * Logging routine for INFOS
   *
   * @param message   the key to be logged
   * @param t         an occuring exception
   * @deprecated do not trace exceptions using this severity. Use FATAL, ERROR, WARN or PATH instead.
   */
  public void info(Object message, Throwable t) {
  }

  /**
   * Logging routine for INFOS
   *
   * @param message   the key to be logged
   * @param args Arguments as object references
   * @param t         an occuring exception
   * @deprecated do not trace exceptions using this severity. Use FATAL, ERROR, WARN or PATH instead.  
   */
  public void info(Object message, Object args[], Throwable t) {
  }

  // --------------------------------------------------------------------------
  // DEBUG
  // --------------------------------------------------------------------------

  /**
   * Logging routine for debug statements. These messages do not require
   * internationalization, thus they are allowed to be plain (english) text.
   *
   * @param message   the message to be logged
   */
  public void debug(Object message) {
  }



  /**
   * Logging routine for debug statements.
   *
   * @param message   the key to be logged
   * @param t         an occuring exception
   */
  public void debug(Object message, Throwable t) {
  }

  // -------------------------------------------------------------------------
  // ACCESSOR
  // -------------------------------------------------------------------------

  /**
   * Checks whether logging is enabled for debug statements. It is recommended
   * to check this, whenever debug calls are being made.
   *
   * The default implementation always returns false.
   *
   * @return boolean true if debugging is allowed.
   */
  public boolean isDebugEnabled() {
    return false;
  }

  /**
   * Checks whether logging is enabled for info statements. It may helpful
   * if there are many info messages that should be logged to check this
   * beforehand.
   *
   * The default implementation always returns false.
   *
   * @return boolean true if info-logging is allowed.
   */
  public boolean isInfoEnabled() {
    return false;
  }

  /**
   * Sets the priority of the current location to pri. Allowed values are
   * <ul>
   * <li>FATAL
   * <li>ERROR
   * <li>WARN
   * <li>INFO
   * <li>DEBUG
   * </ul>
   */
  public void setPriority(String pri) {
  }

  // --------------------------------------------------------------------------
  // Log methods that do not use the l7dlog method
  // That is: CONFIG
  // --------------------------------------------------------------------------

  /**
   * Log a message object with the CONFIG priority.
   *
   * @param message the message object to log. 
   * @deprecated should not be used because there is no corresponding log level in SAP Logging API. 
   *             Use {@link #debug(Object)} instead   
   */
  public void config(Object message) {
  }

  /**
   * Log a message object with the CONFIG priority.
   * This implementation simply does DEBUG.
   * @param message the message object to log. 
   * @param t a throwable object
   * @deprecated should not be used because there is no corresponding log level in SAP Logging API. 
   *             Use {@link #debug(Object)} instead   
   */
  public void config(Object message, Throwable t) {
  }
  /**
   * Cleans up the static instances of the IsaLocation using Reflection
   * 
   */
  static public void terminate()
  {
//  	Iterator iter = allocatedClasses.iterator();
//  	while(iter.hasNext())
//  	{
//  		Class clazz = (Class)iter.next();
//  		String fieldName="";
//  		if(log.isDebugEnabled() )
//  		    fieldName= clazz.getName() + ".";
//  		Field flds[]=clazz.getDeclaredFields();
//  		for(int k =0; k< flds.length;k++)
//  		{
//  		  	    Field fld = flds[k];
//  		  	    if( log.isDebugEnabled())
//  		  	        fieldName=clazz.getName() + "." + fld.getName();
//  		  	    if( fld.getType() == IsaLocation.class && Modifier.isStatic(fld.getModifiers()))
//  		  	    {
//			    	fld.setAccessible(true);
//			    	try {
//                        fld.set(null,null);
//    			    	if( log.isDebugEnabled())
//    			    	    log.debug("terminate(): Set " + fieldName + " = null");
//                    }  catch (Exception e) {
//            			if( log.isDebugEnabled())
//            			    log.debug("terminate(): Unable to set " +  fieldName + ": "+e.getMessage(),e);
//            		}
//			    }
//		}
//  	}
//  	allocatedClasses = null;
  	log=null;
  }
}