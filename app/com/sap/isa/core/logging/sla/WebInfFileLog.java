package com.sap.isa.core.logging.sla;

// java imports
import java.io.IOException;

import com.sap.tc.logging.FileLog;


public class WebInfFileLog extends FileLog {

    private static String mPath;
    private static String mFileName;

  public WebInfFileLog() {
    super(mPath + "isa.log");
  }

  /**
   * Sets the path to where log files will be stored
   */
  public synchronized static void setPath(String path) {
    mPath = path;
  }

  /**
   * Sets the filename (path addon)
   */
  public synchronized void setFile(String fileName) throws IOException {
    if(mPath != null) {
      mFileName = mPath + fileName;
    }
  }

}