package com.sap.isa.core.logging.sla;

import com.sap.tc.logging.LogRecord;
import com.sap.tc.logging.TraceFormatter;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @author
 * @version 1.0
 */

public class IsaTraceFormatter extends TraceFormatter {

    /**
    * For the MessageKey (Resourcebundle) please use '%[isa:key]'
    */
    static final String MESSAGE_KEY  = "%[isa:key]";

    /**
    * For the SessionId please use '%[isa:id]'
    */
    static final String SESSION_ID   = "%[isa:id]";

  public IsaTraceFormatter() {
    super("%d,%-3p %t %-5s %l [%[isa:key]|%[isa:id]] %m");
  }

  public IsaTraceFormatter(String pattern) {
    super(pattern);
  }


  public String format(LogRecord rec) {
//    String[] args = rec.getArgs();
    String[] args = (String[]) rec.getArgs().toArray(new String[0]);
    String key = "?";
    String id = "undefined";
    if (args != null && args.length == 3) {
      key = args[args.length - 2];
      id  = args[args.length - 1];
    }

    return assignCustomMessages(super.format(rec), key, id);
  }

  public void setPattern(java.lang.String pattern) {
    super.setPattern(pattern);
  }

  /**
   * Process for custom message keys
   */
  private String assignCustomMessages(String msg, String key, String id) {

    // process message key
    int keyIndex = msg.indexOf(MESSAGE_KEY);
    StringBuffer newMsg = new StringBuffer();
    if (keyIndex != -1) {
      String stringLeft = msg.substring(0, keyIndex);
      newMsg = new StringBuffer(stringLeft);
      newMsg.append(key);
      newMsg.append(msg.substring(keyIndex + MESSAGE_KEY.length(), msg.length()));
    }
    msg = newMsg.toString();
    // process session id
    keyIndex = msg.indexOf(SESSION_ID);
    newMsg = new StringBuffer();
    if (keyIndex != -1) {
      String stringLeft = msg.substring(0, keyIndex);
      newMsg = new StringBuffer(stringLeft);
      newMsg.append(id);
      newMsg.append(msg.substring(keyIndex + SESSION_ID.length(), msg.length()));
    }

    return newMsg.toString();
  }
}