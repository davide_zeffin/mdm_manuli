/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      10 Januar, 2001

  $Revision:  $
  $Date:  $
*****************************************************************************/

package com.sap.isa.core.logging.sla;

// java imports
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogConfigurator;

/**
 * When this acction is called a new session logging is started.
 * A new log file is created containing only log entries valid for the
 * session used when calling this action.
 */
public class StartSessionLoggingAction extends Action {

	private static IsaLocation log =
		IsaLocation.getInstance(StartSessionLoggingAction.class.getName());

	private String defaultSessionLogForward = "admin";

	/**
	 *
	 * @param mapping The ActionMapping used to select this instance
	 * @param action The optional ActionForm bean for this request (if any)
	 * @param request The HTTP request we are processing
	 * @param response The HTTP response we are creating
	 *
	 * @return <code>ActionForward</code> describing where the control should
	 *         be forwarded
	 *
	 * @exception IOException if an input/output error occurs
	 * @exception ServletException if a servlet exception occurs
	 */
	public final ActionForward perform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws IOException, ServletException {

		//String mappingName = request.getParameter("mapping");
		String forward = request.getParameter("forward");
		// action - start or stop session logging
		String action = request.getParameter("action");
		if (action == null)
			action = "startsessiontrace";

		if (action.equalsIgnoreCase("stopsessiontrace")) {

			SessionLoggingSupport sessionLogging = null;
			if (request.getSession() != null
				&& request.getSession().getAttribute(
					"SessionLoggingSupport.sla.logging.core.isa.sap.com")
					!= null) {
				sessionLogging =
					(SessionLoggingSupport) request.getSession().getAttribute(
						"SessionLoggingSupport.sla.logging.core.isa.sap.com");
			}
			if (sessionLogging != null && request.getSession() != null) {
				sessionLogging.stopSessionLogging(request.getSession().getId());
			}

		} else if (action.equalsIgnoreCase("startsessiontrace")) {

			if (LogConfigurator
				.getLoggingApiType()
				.equals(LogConfigurator.ISALOCATION_SAPMARKETS_NAME)) {
				SessionLoggingSupport sessionLogging =
					new SessionLoggingSupport(request.getSession().getId());
				request.getSession().setAttribute(
					"logfilename.sla.logging.core.isa.sap.com",
					sessionLogging.getLogFileName());
				//if (mappingName == null)
				//  mappingName = "init";

				request.getSession().setAttribute(
					"SessionLoggingSupport.sla.logging.core.isa.sap.com",
					sessionLogging);
					
			    request.getSession().setAttribute(
                    "LogFilePath.SessionLoggingSupport.sla.logging.core.isa.sap.com",
                     sessionLogging.getLogFilePath());
			}
		} else {
			// no session logging possible
			log.info("system.sessionLogging.notpossible");
		}
		if (forward != null)
			return new ActionForward(forward);
		else
			return mapping.findForward(defaultSessionLogForward);
	}

}