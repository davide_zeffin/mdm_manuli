/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      08. November 2001

  $Revision: $
  $Date: $
*****************************************************************************/

package com.sap.isa.core.logging.sla;

import com.sap.isa.core.logging.IsaSession;
import com.sap.tc.logging.Filter;
import com.sap.tc.logging.LogRecord;

public class SessionFilter implements Filter {

	private final String mSessionCategory;

	/**
	 * This filter is used to filter Log Messages which belong to a specific
	 * Session
	 * @param sessionCategory The category which identifies a specific session
	 */
	public SessionFilter(String sessionCategory) {
		mSessionCategory = sessionCategory;
	}

	/**
	 * Called by the Logging  API. Decides whether the message will pass the filter.
	 * @param record
	 */
	public boolean beLogged(LogRecord record) {
		//    boolean pass = false;
		//    synchronized (record) {
		////      LogController[] relatives = record.getRelatives();
		//      LogController[] relatives = (LogController[]) record.getRelatives().toArray(new LogController[0]);
		//
		//      if (relatives.length > 0) {
		//      String cat = relatives[0].getName();
		//      if (relatives[0].getName().equals(mSessionCategory))
		//        pass = true;
		//      }
		//    }
		//    return pass;
		synchronized (record) {
			String id = IsaSession.getThreadSessionId();
			if (id.equals(mSessionCategory))
				return true;
		}
		return false;
	}
}