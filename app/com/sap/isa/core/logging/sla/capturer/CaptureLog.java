package com.sap.isa.core.logging.sla.capturer;


import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import com.sap.tc.logging.Log;
import com.sap.tc.logging.LogRecord;
import com.sap.tc.logging.Severity;
import com.sap.isa.core.businessobject.event.capturer.BusinessEventCapturerFactory;
import com.sap.isa.core.businessobject.event.capturer.CapturerEvent;
import com.sap.isa.core.businessobject.event.capturer.EventCapturer;
import com.sap.isa.core.logging.IsaSession;



public class CaptureLog extends Log {

     private static final String LOG_RECORD_KEY = "logRecord";

     private String enc;
     private EventCapturer eventCapturer;


     public CaptureLog() {
     }

     public String getEncoding() {
        return enc;
     }
     public void setEncoding(String enc) {
        this.enc = enc;
     }


    public LogRecord write(LogRecord rec) {
        IsaSession.setAttribute(LOG_RECORD_KEY, rec);
        return super.write(rec);
     }



     protected void writeInt(String s) throws IOException {

        if (s == null ||
            s.length()== 0)
            return;

        if (!BusinessEventCapturerFactory.isInitialized()) {
            return;
        }


        if (eventCapturer == null) {

            eventCapturer = BusinessEventCapturerFactory.getEventCapturer();

            if (eventCapturer == null)
               return;
        }


        HttpServletRequest request =
             (HttpServletRequest)IsaSession.getAttribute("http_request");

        if (request == null)
            return;

        String sessId = request.getSession()==null?
                                    "":request.getSession().getId();

        CapturerEvent logEvent =
                           eventCapturer.createEvent(request, "LOGGING_EVENT");

        LogRecord logRecord = (LogRecord)IsaSession.getAttribute(LOG_RECORD_KEY);
        if (logRecord == null)
            return;

        logEvent.setProperty("Details", "Date",        logRecord.getTime());
        logEvent.setProperty("Details", "Time",        "");
        logEvent.setProperty("Details", "Thread",      logRecord.getThread().toString());
        logEvent.setProperty("Details",
                             "Level", Severity.toString(logRecord.getMsgType()));
        logEvent.setProperty("Details", "Location",    logRecord.getLocation());
        logEvent.setProperty("Details", "SessionId",   sessId);
        logEvent.setProperty("Details",
                             "ResourceKey", logRecord.getMsgCode()==null?
                                                       "":logRecord.getMsgCode());
        logEvent.setProperty("Details", "Message",     logRecord.getMsgClear());


        logEvent.publish();
     }

     protected void flushInt() throws IOException {
     }

     protected void closeInt() throws IOException {
     }

}
