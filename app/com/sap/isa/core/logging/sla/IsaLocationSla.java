package com.sap.isa.core.logging.sla;

// sap logging imports
import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.apache.struts.util.MessageResources;

import com.sap.isa.core.init.InitializationHandler;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogMessage;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.tc.logging.Severity;

/**
 * The IsaLocation is a facade for the SAP logging API
 * It enables you to tracing as well as logging.
 */
public class IsaLocationSla extends IsaLocation {

	private Location location= null;
	private static Map mSessionIds= new HashMap();

	/**
	 * The MessageResources set for all IsaCategories
	 */
	private static MessageResources mMessageResources= null;
	private static Map mLocations= new HashMap(1000);

	/**
	 * When no key (e.g. at debug/config messages) is set, this will be set
	 */
	private static final String NO_KEY= "?";
	private static final String NULL= "null";


	public IsaLocationSla(String name) {
		try {
			if (mMessageResources == null)
				mMessageResources = InitializationHandler.getMessageResources();	
		} catch (Throwable t) {
			debug(t.getMessage());
		}
		
		
		location= (Location) mLocations.get(name);
		if (location == null) {
			synchronized (mLocations) {
				location= Location.getLocation(name);
				mLocations.put(name, location);
			}
		}
	}

	/**
	 * <p>The method to retieve the logging object. The name must
	 * contain at least packagename and classname.</p>
	 *
	 * @param name - the name of the current location.
	 */
	public static IsaLocation getInstance(String name) {
		return new IsaLocationSla(name);
	}

	// --------------------------------------------------------------------------
	// GENERIC TRACES
	// --------------------------------------------------------------------------

	/**
	 * Generic tracing routine.  
	 *
	 * @param priority  the priority to be logged. Please use a one of the following strings:
	 *                  "DEBUG", "ERROR"
	 * @param key       the key from the struts Messagebundle
	 * @param obj       an array of parameters for internationalized
	 * @param t         an exception
	 */
	public void log(Object priority, Object key, Object obj[], Throwable t) {

		if (priority == null)
			return;
		String prio= priority.toString();
		String msg= null;
		if (t == null)
			msg=
				mMessageResources.getMessage(
					Locale.ENGLISH,
					key != null ? key.toString() : NULL,
					obj);
		else
			msg=
				mMessageResources.getMessage(
					Locale.ENGLISH,
					key != null ? key.toString() : NULL,
					obj)
					+ System.getProperty("line.separator")
					+ getStacktrace(t);


		if (prio.equalsIgnoreCase("DEBUG")) {
			if (t != null)
				location.logT(
					Severity.DEBUG,					
					key != null ? key.toString() : NULL + getStacktrace(t));
			else
				location.logT(
					Severity.DEBUG, key != null ? key.toString() : NULL);
			return;
		}

		if (prio.equalsIgnoreCase("ERROR")) {
			location.logT(
				Severity.ERROR, getResourceBundleString(
					key != null ? key.toString() : NULL, obj, t));
			return;
		}

		if (prio.equalsIgnoreCase("FATAL")) {
			location.logT(
				Severity.FATAL, 
				getResourceBundleString(key, obj, t));
			return;
		}

	}

	// --------------------------------------------------------------------------
	// FATAL TRACES
	// --------------------------------------------------------------------------

	/**
	 * Traces a message object with the FATAL priority.
	 * @param message the message object to log. 
	 * @deprecated fatal should not be used for tracing only for logging (together with a category)
	 */
	public void fatal(Object message) {
		if (isKeyAvailable(message))
			location.fatalT(getResourceBundleString(message));
		else
			location.fatalT(message.toString());
	}
	/**
	 * Traqce a message object with the <code>FATAL</code> priority including
	 * the stack trace of the {@link Throwable} <code>t</code> passed as
	 * parameter.
	 * 
	 * @param message the message object to log. 
	 * @param t the exception to log, including its stack trace.
	 * @deprecated fatal should not be used for tracing only for logging (together with a category)
	 */
	public void fatal(Object message, Throwable t) {
		if (isKeyAvailable(message))
			location.fatalT(getResourceBundleString(message, t));
		else
			location.fatalT(getString(message, t));
		
		
	}
	/**
	 * Traces a message object with the <code>FATAL</code> priority including
	 * the stack trace of the {@link Throwable} <code>t</code> passed as
	 * parameter.
	 *
	 * @param message the message object to log. 
	 * @param params the parameters
	 * @param t the exception to log, including its stack trace.
	 * @deprecated fatal should not be used for tracing only for logging (together with a category) 
	 */
	public void fatal(Object message, Object params[], Throwable t) {
		if (isKeyAvailable(message))
			location.fatalT(getResourceBundleString(message, params, t));
		else
			location.fatalT(getString(message, t), params);
	}

	// --------------------------------------------------------------------------
	// LOG ERROR 
	// --------------------------------------------------------------------------
	/**
	 * Logs a message object with the ERROR priority.
	 *
	 * @param category category of this log
	 * @param key The message key. There must be a corresponding key in the message resources. 
	 *            You can also pass an object of type LogUtil.LogMessage
	 * 
	 */
	public void error(Category category, Object key) {
		category.errorT(location, getResourceBundleString(key));
	}

	/**
	 * Logs a message object with the ERROR priority.
	 *
	 * @param category category of this log
	 * @param key The message key. There must be a corresponding key in the message resources
	 * @param args Arguments as object references
	 * @param t Throwable, the superclass of all errors and exceptions* 
	 */
	public void error(Category category, Object key, Object[] args, Throwable t) {
		if (t == null)
			error(category, key, args);	
		else
			category.logThrowableT(Severity.ERROR, location, getResourceBundleString(key), args, t);
	}

	/**
	 * Logs a message object with the ERROR priority together with a stack trace
	 *
	 * @param category category of this log
	 * @param key The message key. There must be a corresponding key in the message resources
	 * @param t Throwable, the superclass of all errors and exceptions
	 */
	public void error(Category category, Object key, Throwable t) {
		if (t == null)
			error(category, key); 
		else
			category.logThrowableT(Severity.ERROR, location, getResourceBundleString(key), t);
	}

	/**
	 * Logs a message object with the ERROR priority.
	 *
	 * @param category category of this log
	 * @param key The message key. There must be a corresponding key in the message resources
	 * @param args Arguments as object references
	 */
	public void error(Category category, Object key, Object[] args) {
		category.errorT(location, getResourceBundleString(key), args);
	}

	// --------------------------------------------------------------------------
	// LOG FATAL
	// --------------------------------------------------------------------------
	/**
	 * Logs a message object with the FATAL priority.
	 *
	 * @param category category of this log
	 * @param key The message key. There must be a corresponding key in the message resources
	 */
	public void fatal(Category category, Object key) {
		category.fatalT(location, getResourceBundleString(key));
	}

	/**
	 * Logs a message object with the FATAL priority together with a stack trace
	 *
	 * @param category category of this log
	 * @param key The message key. There must be a corresponding key in the message resources
	 * @param t Throwable, the superclass of all errors and exceptions
	 */
	public void fatal(Category category, Object key, Throwable t) {
		if (t == null) {
			fatal(category, key);
		} else
			category.logThrowableT(Severity.FATAL, location, getResourceBundleString(key), t);
	}

	/**
	 * Logs a message object with the FATAL priority 
	 *
	 * @param category category of this log
	 * @param key The message key. There must be a corresponding key in the message resources
	 * @param args Arguments as object references
	 */
	public void fatal(Category category, Object key, Object[] args) {
		category.fatalT(location, getResourceBundleString(key), args);
	}



	/**
	 * Logs a message object with the FATAL priority together with a stack trace
	 *
	 * @param category category of this log
	 * @param key The message key. There must be a corresponding key in the message resources
	 * @param args Arguments as object references
	 * @param t Throwable, the superclass of all errors and exceptions
	 */
	public void fatal(Category category, Object key, Object[] args, Throwable t) {
		if (t == null)
			fatal(category, key, args);
		else
			category.logThrowableT(Severity.FATAL, location, getResourceBundleString(key, args), t);
	}


	// --------------------------------------------------------------------------
	// LOG WARNING
	// --------------------------------------------------------------------------
	/**
	 * Logs a message object with the WARNING priority.
	 *
	 * @param category category of this log
	 * @param key The message key. There must be a corresponding key in the message resources
	 */
	public void warn(Category category, Object key) {
		category.warningT(location, getResourceBundleString(key));
	}

	/**
	 * Logs a message object with the WARNING priority.
	 *
	 * @param category category of this log
	 * @param key The message key. There must be a corresponding key in the message resources
	 * @param args Arguments as object references
	 */
	public void warn(Category category, Object key, Object[] args) {
		category.warningT(location, getResourceBundleString(key), args);
	}

	/**
	 * Logs a message object with the WARNING priority together with a stack trace
	 *
	 * @param category category of this log
	 * @param key The message key. There must be a corresponding key in the message resources
	 * @param t Throwable, the superclass of all errors and exceptions
	 */
	public void warn(Category category, Object key, Throwable t) {
		if (t == null)
			warn(category, key);
		else
			category.logThrowableT(Severity.WARNING, location, getResourceBundleString(key), t);
	}

	/**
	 * Logs a message object with the WARN priority together with a stack trace
	 *
	 * @param category category of this log
	 * @param key The message key. There must be a corresponding key in the message resources
	 * @param args Arguments as object references
	 * @param t Throwable, the superclass of all errors and exceptions
	 */
	public void warn(Category category, Object key, Object[] args, Throwable t) {
		if (t == null)
			warn(category, key, args);
		else
			category.logThrowableT(Severity.WARNING, location, getResourceBundleString(key, args), t);
	}

	// --------------------------------------------------------------------------
	// LOG INFO
	// --------------------------------------------------------------------------
	/**
	 * Logs a message object with the INFO priority.
	 *
	 * @param category category of this log
	 * @param key The message key. There must be a corresponding key in the message resources
	 */
	public void info(Category category, Object key) {
		if (isKeyAvailable(key))
		category.infoT(location, getResourceBundleString(key));
		else
		location.infoT(key.toString());
	}

	/**
	 * Logs a message object with the ERROR priority 
	 *
	 * @param category category of this log
	 * @param key The message key. There must be a corresponding key in the message resources
	 * @param args Arguments as object references
	 */
	public void info(Category category, Object key, Object[] args) {
		category.infoT(location, getResourceBundleString(key), args);
	}

	// --------------------------------------------------------------------------
	// ERROR TRACES
	// --------------------------------------------------------------------------

	/**
	 * Traces  a message object with the ERROR priority.
	 *
	 * @param key The message key. There must be a corresponding key in the message resources 
	 */
	public void error(Object key) {
		if (isKeyAvailable(key))
			location.errorT(getResourceBundleString(key));
		else
			location.errorT(key.toString());
	}

	/**
	 * Log a message object with the <code>ERROR</code> priority including
	 * the stack trace of the {@link Throwable} <code>t</code> passed as
	 * parameter.
	 *
	 * @param message the message object to log. 
	 * @param t the exception to log, including its stack trace.
	 */
	public void error(Object message, Throwable t) {
		if ( isKeyAvailable(message))
			location.errorT(getResourceBundleString(message, t));
		else
			location.errorT(getString(message, t));
	}
	/**
	 * Log a message object with the <code>ERROR</code> priority including
	 * the stack trace of the {@link Throwable} <code>t</code> passed as
	 * parameter.
	 *
	 * @param message the message object to log. 
	 * @param params the parameters
	 * @param t the exception to log, including its stack trace.
	 */
	public void error(Object message, Object[] params, Throwable t) {
		if (isKeyAvailable(message))
			location.errorT(getResourceBundleString(message, params, t));
		else
			location.errorT(getString(message, t), params);
	}



	// --------------------------------------------------------------------------
	// PATH TRACES
	// --------------------------------------------------------------------------

	/**
	 * Should be used when an exception is throwed. 
	 * The exception is written using its method toString.
	 * @param msg   the key to be logged
	 * @param t         an occuring exception
	 */
	public void throwing(String msg, Throwable t) {
		location.throwing(msg, t);
	}
  
	/**
	 * Should be used when an exception is throwed.
	 * The exception is written using its method toString.
	 * @param t         an occuring exception
	 */
	public void throwing(Throwable t) {
		location.throwing(t);
	}

	/**
	 * Traces message of severity Severity.PATH and appends a string denoting a sublocation to the name of this location. 
	 * This method must be balanced with a call to exiting when leaving the traced method, for example exiting().
	 * @param method name of sublocation
	 */
	public void entering(String method) {
		if (method == null)
			location.entering();
		else
			location.entering(method);
	}
	/**
	 * Traces message of group severity which indicates that execution is about to leave 
	 * this method location. This method is to be balanced with a call to the method entering().
	 */
	public void exiting() {
		location.exiting();
	}


	// --------------------------------------------------------------------------
	// WARNING TRACES
	// --------------------------------------------------------------------------

	/**
	 * Trace a message object with the WARN priority.
	 * @param message the message object to log. 
	 * @deprecated fatal should not be used for tracing 
	 */
	public void warn(Object key) {
		if (isKeyAvailable(key))
		location.warningT(getResourceBundleString(key));
		else
			location.warningT(key.toString());
		
	}
	/**
	 * Log a message object with the <code>WARN</code> priority including
	 * the stack trace of the {@link Throwable} <code>t</code> passed as
	 * parameter.
	 *
	 * @param message the message object to log. 
	 * @param t the exception to log, including its stack trace.
	 * @deprecated fatal should not be used for tracing
	 */
	public void warn(Object message, Throwable t) {
		if (isKeyAvailable(message))
			location.warningT(getResourceBundleString(message, t));
		else
			location.warningT(getString(message, t));
	}

	/**
	 * Log a message object with the <code>WARN</code> priority including
	 * the stack trace of the {@link Throwable} <code>t</code> passed as
	 * parameter.
	 *
	 * @param message the message object to log. 
	 * @param params the parameters
	 * @param t the exception to log, including its stack trace.
	 * @deprecated warn should not be used for tracing
	 */
	public void warn(Object message, Object params[], Throwable t) {
		if (isKeyAvailable(message))
			location.warningT(getResourceBundleString(message, params, t));
		else
			location.warningT(getString(message, t), params);
	}

	// --------------------------------------------------------------------------
	// INFO TRACES
	// --------------------------------------------------------------------------

	/**
	 * Log a message object with the {@link Priority#INFO INFO} priority.
	 *
	 * @param message the message object to log. 
	 * @deprecated info should not be used for tracing for logging (together with a category)
	 */
	public void info(Object message) {
			location.infoT(getResourceBundleString(message));
	}

	/**
	 * Log a message object with the <code>INFO</code> priority including
	 * the stack trace of the {@link Throwable} <code>t</code> passed as
	 * parameter.
	 *
	 * @param message the message object to log. 
	 * @param t the exception to log, including its stack trace.
	 * @deprecated info should not be used for tracing only for logging (together with a category)
	 */
	public void info(Object message, Throwable t) {
		if (isKeyAvailable(message))
			location.infoT(getResourceBundleString(message, t));
		else
			location.infoT(getString(message, t));
		
	}


	// --------------------------------------------------------------------------
	// CONFIG tracing
	// --------------------------------------------------------------------------

	/**
	 * Log a message object with the CONFIG priority.
	 * This implementation simply does DEBUG.
	 *
	 * @param message the message object to log. 
	 * @deprecated should not be used because there is no corresponding log level in SAP Logging API. 
	 *             Use {@link #debug(Object)} instead   
	 */
	public void config(Object message) {
		location.debugT(getResourceBundleString(message));
	}

	/**
	 * Log a message object with the CONFIG priority.
	 * This implementation simply does DEBUG.
	 * @param message the message object to log. 
	 * @param t a throwable object
	 * @deprecated should not be used because there is no corresponding log level in SAP Logging API. 
	 *             Use {@link #debug(Object)} instead   
	 */
	public void config(Object message, Throwable t) {
		location.debugT(getResourceBundleString(message, t));
	}


	// --------------------------------------------------------------------------
	// DEBUG Tracing
	// --------------------------------------------------------------------------

	/**
	 * Log a message object with the {@link Priority#DEBUG DEBUG} priority.
	 *
	 * @param message the message object to log using plain english. 
	 */
	public void debug(Object message) {
		if (message != null)
			location.debugT(message.toString());
	}

	/**
	 * Log a message object with the <code>DEBUG</code> priority including
	 * the stack trace of the {@link Throwable} <code>t</code> passed as
	 * parameter.
	 *
	 * <p>See {@link #debug(Object)} form for more detailed information.
	 *
	 * @param message the message object to log. 
	 * @param t the exception to log, including its stack trace.
	 */
	public void debug(Object message, Throwable t) {
		if (message == null)
			location.debugT(getStacktrace(t));
		else {
			StringBuffer sb = new StringBuffer(message.toString());
			sb.append(getStacktrace(t));
			location.debugT(sb.toString());
		}
		
	}

	// -------------------------------------------------------------------------
	// ACCESSOR
	// -------------------------------------------------------------------------

	/**
	* Check whether this location is enabled for the <code>DEBUG</code>
	* priority.
	*
	* <p> This function is intended to lessen the computational cost of
	* disabled log debug statements.
	*
	* <p> For some <code>cat</code> location object, when you write,
	* <pre>
	*   cat.debug("This is entry number: " + i );
	* </pre>
	*
	* <p>You incur the cost constructing the message, concatenatiion in
	* this case, regardless of whether the message is logged or not.
	*
	* <p>If you are worried about speed, then you should write
	* <pre>
	*   if(cat.isDebugEnabled()) {
	*     cat.debug("This is entry number: " + i );
	*   }
	* </pre>
	*
	* <p>This way you will not incur the cost of parameter construction
	* if debugging is disabled for <code>cat</code>. On the other hand,
	* if the <code>cat</code> is debug enabled, you will incur the cost
	* of evaluating whether the location is debug enabled twice. Once
	* in <code>isDebugEnabled</code> and once in the
	* <code>debug</code>.  This is an insignificant overhead since
	* evaluating a location takes about 1% of the time it takes to
	* actually log.
	*
	* @return boolean - <code>true</code> if this location is debug
	* enabled, <code>false</code> otherwise.
	*
	*/
	public boolean isDebugEnabled() {
		if (location.beDebug())
			return true;
		else {
			if (location.getEffectiveSeverity() <= Severity.DEBUG)
				return true;
			else {
				return false;
			}
		}
	} 

	/**
	 * Check whether this location is enabled for the info priority.
	 * See also {@link #isDebugEnabled}.
	 *
	 * @return boolean - <code>true</code> if this location is enabled
	 * for priority info, <code>false</code> otherwise.
	 */
	public boolean isInfoEnabled() {
		if (location.getEffectiveSeverity() <= Severity.INFO)
			return true;
		else
			return false;
	}


	/**
	 * Sets the MessageResources for ALL IsaCategories
	 *
	 * @param m - the MessageResource to be used
	 */
	public static void setMessageResources(MessageResources m) {
		mMessageResources= m;
	}

	/**
	 * Gets the MessageResources from ALL IsaCategories
	 */
	public static MessageResources getMessageResources() {
		return mMessageResources;
	}

	/**
	 * Gets the ENGLISH representation of the given key. If key 
	 * is missing the following string is returned: [resource key missing]='<key name>'
	 *
	 * @param key - the key to be used
	 */
	private String getResourceBundleString(Object key) {
		if (key instanceof LogMessage)
			return ((LogMessage)key).getMessageText();
		
		if (key == null)
			return NO_KEY;
		else {
			if (mMessageResources == null)
				return (String)key;
			else {
				if (mMessageResources.isPresent(Locale.ENGLISH, key.toString()))
					return mMessageResources.getMessage(Locale.ENGLISH, key.toString()); 
				else
					return key.toString();
			}
		}
	}


	/**
	 * Returns true if resource key is available, otherwise false
	 * @param key
	 * @return true if resource key is available, otherwise false
	 */
	private boolean isKeyAvailable(Object key) {
		if (key instanceof LogMessage)
			return false;
		if(mMessageResources == null )
		{
			return false;
		}
		return mMessageResources.isPresent(Locale.ENGLISH, key.toString());
	}



	/**
	 * Gets the ENGLISH representation of the given key
	 *
	 * @param key - the key to be used
	 * @return the ENGLISH representation of the given key
	 */
	private String getResourceBundleString(Object key, Throwable t) {
		if (t == null)
			return getResourceBundleString(key);
		else {
			return getResourceBundleString(key)
				+ System.getProperty("line.separator")
				+ t.toString()
				+ System.getProperty("line.separator")
				+ getStacktrace(t);
		}
	}

	/**
	 * Gets the ENGLISH representation of the given key
	 *
	 * @param key the key to be used
	 * @param args array of arguments
	 * @return the ENGLISH representation of the given key
	 */
	private String getResourceBundleString(
		Object key,
		Object args[],
		Throwable t) {
		if (t == null)
			return getResourceBundleString(key, args);
		else {
			return getResourceBundleString(key, args)
				+ System.getProperty("line.separator")
				+ t.toString()
				+ System.getProperty("line.separator")
				+ getStacktrace(t);
		}
	}



	/**
	 * Gets the ENGLISH representation of the given key
	 *
	 * @param key - the key to be used
	 * @param args array of arguments
	 */
	private String getResourceBundleString(Object key, Object args[]) {
		if (key == null)
			return null;
		else
			return mMessageResources.getMessage(Locale.ENGLISH, key.toString(), args);
	}

	/**
	 * Returns the passed string together with a string representation of the
	 * stacktrace
	 * @param string passed string
	 * @param t throwable
	 * @return the passed string together with a string representation of the
	 *         stacktrace
	 */
	private String getString(Object message, Throwable t) {
		return message.toString() + " " + getStacktrace(t);
	}

	/**
	 * Returns the stack-trace as string
	 * @param t Throwable
	 * @return stack-trace as string
	 */
	private String getStacktrace(Throwable t) {
		String st= null;
		try {
			ByteArrayOutputStream ostr= new ByteArrayOutputStream();
			t.printStackTrace(new PrintWriter(ostr, true));
			st= ostr.toString();
		} catch (Exception f) {
			debug(f.getMessage());
			// Do nothing.
		}
		return st;
	}

	/**
	 * Returns a category object containing the session id if available
	 * @return category object containing the session id if available
	 */
//	public Category getCategory() {
//		return Category.APPLICATION;
//	}

	/**
	 * This methiod returns the real session id for the given hash-value
	 * @param hash the hash value
	 * @return the real session id
	 */
	private static String getSessionId(String hash) {
		return (String) mSessionIds.get(hash);
	}

	/**
	 * This methiod removes the real session id for the given hash-value
	 * @param hash the hash value
	 */
	private static void removeSessionId(String hash) {
	}

}