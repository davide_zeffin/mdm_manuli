/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      08. November 2001

  $Revision: $
  $Date: $
*****************************************************************************/
package com.sap.isa.core.logging.sla;

// struts imports
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Properties;
import java.util.StringTokenizer;

import org.apache.struts.util.MessageResources;

import com.sap.isa.core.init.Initializable;
import com.sap.isa.core.init.InitializationEnvironment;
import com.sap.isa.core.init.InitializeException;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.tc.logging.PropertiesConfigurator;

/**
 * Class that configures the logging for the SAP Markets Logging API
 */
public class LogConfiguratorSla implements Initializable {

    /**
     * Special pattern used to set the root logging-path determined by the ISA logging wrapper
     * Value: %[isa:path]
     */
    public final static String ISA_LOG_ROOT_PATH = "%[isa:path]";

    public final static double DEFAULT_LOG4J_VERSION = 1.04;

    private final static String FILE_LOGGER_NAME = "FileLog";

    private PropertiesConfigurator pcfg;
    
    private static String mPrefixName;
    
	/** 
	  *  The IsaLocation of the current Action.
	  *  This will be instantiated during perform and is always present.
	  */
	 protected IsaLocation log = IsaLocation.getInstance(LogConfiguratorSla.class.getName());
    

    /**
     * Creates new LogConfigurator
     */
    public LogConfiguratorSla() {}

    /**
     * This method will be called from the <code>ActionServlet</code>
     * It initializes the Logging facilities.
     *
     * @param servlet The ActionServlet we are called from
     * @param props The Properties required by the initialization
     *
     * @exception InitializationException at any problem during the intialization
     */
    public void initialize(InitializationEnvironment environment,
                           Properties props)
                 throws InitializeException {

        // --------------------------------------------------------------------
        // Happens only for webapp MSA which runs on Tomcat
        // --------------------------------------------------------------------
        String config = props.getProperty("config-file");
		if (config != null && config.length() > 0) {
			MessageResources  msgResource   = null;
			InputStream       input         = null;

			msgResource = environment.getMessageResources();
			input = environment.getResourceAsStream(config);
			if (input == null) {
				throw new InitializeException(
					msgResource.getMessage("system.noProperty", "config-file"));
			}
			try {
				input.close();
			} catch (IOException ex) {
				log.debug(ex.getMessage());
			}

			String path = props.getProperty("path");

			// --------------------------------------------------------------------
			// Set WebInfLogAppender
			// --------------------------------------------------------------------

			if (path != null) {
				WebInfFileLog.setPath(path);
			}

			// --------------------------------------------------------------------
			// Configure via SAP Markets configuration file
			// --------------------------------------------------------------------

			Properties configProps = new Properties();
			InputStream is = null;
			try {
				is = environment.getResourceAsStream(config);
				configProps.load(is);
			} catch (Exception ex) {
				throw new InitializeException(
					"Exception within ExtendedLogConfigurator" + ex);
			} finally {
				try {
					is.close();
				} catch (IOException ex) {
					log.debug(ex.getMessage());
				}
			}

			// process extended configuration
			processExtFilePatterns(configProps, path);
			// adjust locations to web app
			Properties newProps = processLocations(configProps);

			pcfg = new PropertiesConfigurator(newProps);
			pcfg.setClassLoader(getClass().getClassLoader());
			pcfg.configure();
			// --------------------------------------------------------------------
			// Set Message Resources
			// --------------------------------------------------------------------

			IsaLocationSla.setMessageResources(msgResource);

			// --------------------------------------------------------------------
			// End of webapp MSA
			// --------------------------------------------------------------------
		} else {
//			Note 1032305
			mPrefixName = props.getProperty("logging-location-prefix");
		}
    }

    /**
     * This method parses a set of Properties. If there are properties
     * containing '%[isa:path]' then this string is replaced by the content
     * of the webInfPath parameter
     * @param props      Properties containing logging configuration
     * @param webInfPath the path
     */
    public static Properties processLocations(Properties props) {
      // search the properties for the custom WebInfFileLog logger
      String key = null;
      String element = null;
      String pattern = null;
      Properties newProps = new Properties();
      for (Enumeration enum = props.keys(); enum.hasMoreElements(); ) {
        key = (String)enum.nextElement();
        element = (String)props.getProperty(key);
		StringTokenizer st = new StringTokenizer(key, ".");
		if (st.countTokens() >=3) {
			// must be location
			// add modified element
			newProps.setProperty(key, element);
		} else
			 newProps.setProperty(key, element);
      }
      return newProps;
    }

	/**
	 * This method parses a set of Properties. If there are properties
	 * containing '%[isa:path]' then this string is replaced by the content
	 * of the webInfPath parameter
	 * @param props      Properties containing logging configuration
	 * @param webInfPath the path
	 */
	public static void processExtFilePatterns (Properties props, String webInfPath) {
	  // search the properties for the custom WebInfFileLog logger
	  String key = null;
	  String element = null;
	  String pattern = null;
	  for (Enumeration enumer = props.keys(); enumer.hasMoreElements(); ) {
		    key = (String)enumer.nextElement();
	    element = (String)props.getProperty(key);
	    if (element.equals(FILE_LOGGER_NAME)) {
	      // get pattern for this logger
	      String patternKey = key + ".pattern";
	      pattern = (String)props.getProperty(patternKey);
	      // search for "%w"
	      if (pattern != null && pattern.indexOf(ISA_LOG_ROOT_PATH) == 0) {
	        // replace path with web-inf path
	        props.setProperty(patternKey, webInfPath + pattern.substring(ISA_LOG_ROOT_PATH.length()));
	      }
	    }
	  }
	}

    /**
     *  This method does nothing here.
     */
    public void terminate() {
    	if(pcfg != null) {
    		pcfg.setPeriodicity(0);
    	}
    }
    
	/**
	 * Returns the logging prefix name
	 * 
	 * @return String mPrefixName
	 */    
	public static String getLoggingPrefixName() {
		return mPrefixName;
	}    
}