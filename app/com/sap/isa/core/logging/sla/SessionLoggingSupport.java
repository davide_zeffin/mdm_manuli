/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      13.01.2002

  $Revision: $
  $Date: $
*****************************************************************************/
package com.sap.isa.core.logging.sla;

// isa imports
import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogConfigurator;
import com.sap.tc.logging.FileLog;
import com.sap.tc.logging.Formatter;
import com.sap.tc.logging.Location;
import com.sap.tc.logging.Log;
import com.sap.tc.logging.Severity;
import com.sap.tc.logging.TraceFormatter;

/**
 * This class provides functionlity needed to support the session logging
 * capabilities.
 */
public class SessionLoggingSupport implements HttpSessionBindingListener {

	/**
	 * property name of pattern used for the file logger in the init.config.xml
	 * 'session-logging-log-pattern'
	 */
	public static final String FILE_LOG_PATTERN_PROP_NAME =
		"session-logging-log-pattern";

	/**
	 * The default file logger pattern: "%[isa:path]/"
	 */
	public static final String DEFAULT_FILE_LOG_PATTERN = "%[isa:path]/";

	/**
	 * property name of file size limit in the init.config.xml
	 * 'session-logging-limit'
	 */
	public static final String LIMIT_PROP_NAME = "session-logging-limit";

	/**
	 * The default file size limit in bytes: 10000000
	 */
	public static final int DEFAULT_LIMIT = 10000000;

	/**
	 * property name for the default number of files in the init.config.xml
	 * 'session-logging-cnt'
	 */
	public static final String CNT_PROP_NAME = "session-logging-cnt";

	/**
	 * The default number of files: 10
	 */
	public static final int DEFAULT_CNT = 10;

	/**
	 * property name for the default formatter pattern in the init.config.xml
	 * 'session-logging-formatter-pattern'
	 */
	public static final String FORMATTER_PATTERN_PROP_NAME =
		"session-logging-formatter-pattern";

	/**
	 * The default Trace Formatter pattern: "%d,%-3p %t %-7s %l %m"
	 */
	public static final String DEFAULT_FORMATTER_PATTERN =
		"%d,%-3p %t %-7s %l %m";

	/**
	 * property name for the root location in the init.config.xml
	 * 'session-logging-formatter-pattern'
	 */
	public static final String ROOT_LOCATION_PROP_NAME =
		"session-logging-root-location";

	/**
	 * The root location for the ISA application: "com.sap.isa"
	 */
	public static String DEFAULT_ROOT_LOCATION = "com.sap.isa";

	/**
	 * Contains the root path of the category used in session logging
	 * The result category has the form:
	 * SESSION_LOGGING_CATEGORY + SessionId
	 */
	public static final String SESSION_LOGGING_CATEGORY = "/sessionid";

	private static IsaLocation log =
		IsaLocation.getInstance(SessionLoggingSupport.class.getName());

	private static Set mSessionFileLogs = new HashSet();
	private static Map mOriginalSeverities = new HashMap();
	private static Map mOriginalLogSeverities = new HashMap();

	private FileLog mSessionFileLog;
	private HashSet mRootLocations = new HashSet();
	private final String mSessionId;
	private String mFileLogPattern;
	private int mLimitInt;
	private int mCnt;
	private final String mFormatterPattern;
	private String mLogFileName;

	/**
	 * When a session has to be logged a new instance of this class is created
	 * @param sessionId
	 */
	public SessionLoggingSupport(String sessionId) {

		if (LogConfiguratorSla.getLoggingPrefixName() != null)
			DEFAULT_ROOT_LOCATION =
				LogConfiguratorSla.getLoggingPrefixName()
					+ "."
					+ DEFAULT_ROOT_LOCATION;

		mSessionId = sessionId;
		// get root location(s) of application
		Enumeration initPropsEnum =
			LogConfigurator.getInitPropertyValues().keys();
		//there can be more then one location - for example the customer can maintain
		//their own locations as com.customer.isa.*
		while (initPropsEnum.hasMoreElements()) {
			String propValue = (String) initPropsEnum.nextElement();
			if (propValue.indexOf(ROOT_LOCATION_PROP_NAME) != -1) {
				String value = LogConfigurator.getInitPropertyValue(propValue);
				if (LogConfiguratorSla.getLoggingPrefixName() != null)
					value =
						LogConfiguratorSla.getLoggingPrefixName() + "." + value;
				mRootLocations.add(value);
			}
		}
		//	if no location available use the default one com.sap.isa
		if (mRootLocations.size() == 0)
			mRootLocations.add(DEFAULT_ROOT_LOCATION);

		// eventually store original severities
		if (mSessionFileLogs.size() == 0)
			changeOriginalSeverities();

		// file log pattern
		if (LogConfigurator.getInitPropertyValue(FILE_LOG_PATTERN_PROP_NAME)
			== null)
			mFileLogPattern = DEFAULT_FILE_LOG_PATTERN;
		else
			mFileLogPattern =
				LogConfigurator.getInitPropertyValue(
					FILE_LOG_PATTERN_PROP_NAME);

		// file size limit
		// not used for now - but can be configured in the bootstrap-config.xml
		if (LogConfigurator.getInitPropertyValue(LIMIT_PROP_NAME) != null) {
			String limit =
				LogConfigurator.getInitPropertyValue(LIMIT_PROP_NAME);
			try {
				mLimitInt = Integer.parseInt(limit);
			} catch (NumberFormatException nfex) {
				mLimitInt = DEFAULT_LIMIT;
			}
		}

		// file count
		// not used for now - but can be configured in the bootstrap-config.xml
		if (LogConfigurator.getInitPropertyValue(CNT_PROP_NAME) != null) {
			String cnt = LogConfigurator.getInitPropertyValue(CNT_PROP_NAME);
			try {
				mCnt = Integer.parseInt(cnt);
			} catch (NumberFormatException nfex) {
				mCnt = DEFAULT_CNT;
			}
		}

		// formatter property
		if (LogConfigurator.getInitPropertyValue(FORMATTER_PATTERN_PROP_NAME)
			== null)
			mFormatterPattern = DEFAULT_FORMATTER_PATTERN;
		else
			mFormatterPattern =
				LogConfigurator.getInitPropertyValue(
					FORMATTER_PATTERN_PROP_NAME);

		// register new logger to logging API
		registerLogger();

		// log the used settings
		if (log.isDebugEnabled()) {
			StringBuffer msg =
				new StringBuffer("Session logging properties: [sessionid]='");
			msg.append(mSessionId);
			msg.append("' [session-logging-log-pattern]='");
			msg.append(mFileLogPattern);
			msg.append("' [session-logging-limit]='");
			msg.append(mLimitInt);
			msg.append("' [session-logging-cnt]='");
			msg.append(mCnt);
			msg.append("' [session-logging-formatter-pattern]='");
			msg.append(mFormatterPattern);
			msg.append("' [session-logging-root-location(s)]=");
			for (Iterator i = mRootLocations.iterator(); i.hasNext();)
				msg.append(" '" + (String) i.next() + "'");
			log.debug(msg.toString());
		}
	}

	/**
	 * Notifies the object that it is being bound to a session and identifies the session.
	 * @param event the event that identifies the session
	 */
	public void valueBound(HttpSessionBindingEvent event) {
		// do nothing
	}

	/**
	 * Notifies the object that it is being unbound from a session and identifies the session.
	 * @param event the event that identifies the session
	 */
	public void valueUnbound(HttpSessionBindingEvent event) {
		log.warn(
			"system.sessionLogging.deactivate",
			new Object[] { mSessionId },
			null);
		// unregister session filter
		//mSessionFileLogs.remove(mSessionFileLog);
		boolean success = mSessionFileLogs.remove(mSessionFileLog);
		// remove Session Logger from Logging
		if (success) {
			for (Iterator i = mRootLocations.iterator(); i.hasNext();) {
				Location isaLoc = Location.getLocation((String) i.next());
				isaLoc.removeLog(mSessionFileLog);
			}

			if (mSessionFileLogs.size() == 0) {
				synchronized (mOriginalSeverities) {
					restoreOriginalSeverities();
				}
			}
		}
	}

	/**
	 * Registers an additional logger. This file logger logs all log entries
	 * for the session accociated with this object
	 */
	protected void registerLogger() {

		log.warn(
			"system.sessionLogging.activate",
			new Object[] { mSessionId },
			null);
		/*
		  Programatically set the following settings
		  log[session]           = FileLog
		  log[session].severity  = DEBUG
		  log[session].pattern   = %[isa:path]/session.log
		  log[session].limit     = 10000000
		  log[session].cnt       = 10
		  log[session].filters   = com.sap.isa.core.logging.sla.SessionFilter
		  log[session].formatter = formatter[session]
		*/

		// get root location of ISA application

		Formatter formatter = new TraceFormatter(mFormatterPattern);
		// determine the logging path
		Properties props = new Properties();
		// simulate a logging configuration
		//(only needed to use the processExtFilePatterns() method)
		props.setProperty("log[session]", "FileLog");
		props.setProperty("log[session].pattern", mFileLogPattern);
		// parse custom file log pattern
		LogConfiguratorSla.processExtFilePatterns(
			props,
			LogConfigurator.getLogPath());
		mLogFileName = getSessionLogFileName();
		File pathHelper =
			new File(props.get("log[session].pattern") + "/" + mLogFileName);
		mFileLogPattern = pathHelper.getAbsolutePath();

		if (mLimitInt != 0 && mCnt != 0)
			mSessionFileLog =
				new FileLog(mFileLogPattern, mLimitInt, mCnt, formatter);
		else
			mSessionFileLog = new FileLog(mFileLogPattern, formatter);
		// instantiate a Session Filter valid only for this session
		// changed as we do not use the Category objects for session logging anymore
		SessionFilter sessionFilter = new SessionFilter(mSessionId);
		// register the session filter
		mSessionFileLogs.add(mSessionFileLog);

		mSessionFileLog.addFilter(sessionFilter);
		mSessionFileLog.setEffectiveSeverity(Severity.DEBUG);

		// add file logger to locations
		for (Iterator i = mRootLocations.iterator(); i.hasNext();) {
			String locString = (String) i.next();
			Location loc = Location.getLocation(locString);
			if (loc != null) {
				loc.addLog(mSessionFileLog);
			}
		}
	}

	/**
	 * Helper function to get a valid Category for session logging.
	 * @param sessionId The session id sometimes contains characters which are not
	 *                  allowed for the Categroy.
	 * @return a String which can be used for a Category. The string has the following
	 *         format: SESSION_LOGGING_CATEGORY_ROOT + hascode of session id
	 */
	public static String getSessionLoggingCategory(String sessionId) {
		if (sessionId == null)
			return SESSION_LOGGING_CATEGORY + "undefined";
		else
			return SESSION_LOGGING_CATEGORY
				+ String.valueOf(Math.abs(sessionId.hashCode()));
	}

	/**
	 * Returns true if there is at least one session logging currently turned on
	 * @return true if there is at least on session logging currently activated
	 */
	public static boolean isSessionLoggingRunning() {
		if (mSessionFileLogs.size() > 0)
			return true;
		else
			return false;
	}

	/**
	 * Returns the file name where the session is loggen in
	 * @return file name
	 */
	private String getSessionLogFileName() {
		Calendar calendar = Calendar.getInstance(Locale.ENGLISH);
		StringBuffer timeStamp = new StringBuffer("session-");
		timeStamp.append(calendar.get(Calendar.YEAR));
		timeStamp.append("-");
		timeStamp.append((calendar.get(Calendar.MONTH) + 1));
		timeStamp.append("-");
		timeStamp.append((calendar.get(Calendar.DAY_OF_MONTH)));
		timeStamp.append("_");
		timeStamp.append(calendar.get(Calendar.HOUR_OF_DAY));
		timeStamp.append("-");
		timeStamp.append(calendar.get(Calendar.MINUTE));
		timeStamp.append("_");

		return timeStamp.toString() + mSessionId + ".log";
	}

	/**
	 * Returns the file name of the log file
	 */
	public String getLogFileName() {
		return mLogFileName;
	}

	/**
	 * Foreach Location save the current Severity
	 *
	 */
	private void changeOriginalSeverities() {

		for (Iterator iter = mRootLocations.iterator(); iter.hasNext();) {
			String location = (String) iter.next();
			Location loc = Location.getLocation(location);
			if (!loc.beDebug()) {
				// log level is not sufficient, change to DEBUG
				String originalSeverity =
					String.valueOf(loc.getEffectiveSeverity());
				if (log.isDebugEnabled()) {
					log.debug(
						"Storing original Severity [location]='"
							+ location
							+ "' [severity]='"
							+ originalSeverity
							+ "'");
				}
				mOriginalSeverities.put(location, originalSeverity);
				//be sure that you save all severities on Destination/FileLog level
				// so that they can be later restored
				saveLogsOriginalSeveritiesPerLocation(loc);

				// change severity to DEBUG
				loc.setEffectiveSeverity(Severity.DEBUG);
				//This changes the severity for all Destinations to DEBUG
				//so now restore them back to the original severities.
				//In this way only the session log(s) stay on DEBUG
				//But save the HashMap entries for further processing
				restoreLogsOriginalSeveritiesPerLocation(loc, false);

			}
		}
	}
	/***
	 * Foreach Destination of the current Location save the current severities.
	 * @param loc - the current Location
	 */

	private void saveLogsOriginalSeveritiesPerLocation(Location loc) {
		if (loc != null && !loc.getLogs().isEmpty()) {
			java.util.Iterator logs = loc.getLogs().iterator();
			Log currentLog = null;
			while (logs.hasNext()) {
				//Get the Log Destinations
				currentLog = (Log) logs.next();
				if (currentLog != null) {
					String key = loc.getName() + currentLog.getName();
					String originalSeverity =
						String.valueOf(loc.getEffectiveSeverity());
					mOriginalLogSeverities.put(key, originalSeverity);
				}

			}
		}
	}

	/***
	 * Will restore the original severity of the current Location(s)
	 * Check changeOriginalSeverities where the Severities were 
	 * saved for later usage. From this method we call the 
	 * restoreLogsOriginalSeveritiesPerLocation that will restore
	 * the severities on Destination Level
	 *
	 */
	private void restoreOriginalSeverities() {
		for (Iterator iter = mOriginalSeverities.keySet().iterator();
			iter.hasNext();
			) {
			String location = (String) iter.next();
			String severityString = (String) mOriginalSeverities.get(location);
			int severity = 0;
			try {
				severity = Integer.parseInt(severityString);
			} catch (NumberFormatException nfex) {
				// should not happen
				String msg =
					"Error while parsing original [severity]=' "
						+ severityString
						+ "' of [location]='"
						+ location
						+ "'";
				log.error("system.exception", new Object[] { msg }, nfex);
			}
			if (log.isDebugEnabled()) {
				log.debug(
					"Restoring original Severity [location]='"
						+ location
						+ "' [severity])='"
						+ severityString
						+ "'");
			}

			Location loc = Location.getLocation(location);
			loc.setEffectiveSeverity(severity);
			restoreLogsOriginalSeveritiesPerLocation(loc, true);
			mOriginalSeverities.remove(location);
		}
	}

	/***
	 * Will restore the Severities for every Destination of the current Location.
	 * 
	 * @param loc - the current Location
	 * @param blnClearMap - TRUE should be used only when the Session Tracing finishes 
	 * (Example: by session timeout, by STOP Session Tracing or by Log Off of the user)
	 * so that the entries in the HashMap mOriginalLogSeverities will be cleared
	 * FALSE - should be used for all other calls of the method so that the HashMap stays
	 * untouched and the severities can be restored later
	 */
	private void restoreLogsOriginalSeveritiesPerLocation(
		Location loc,
		boolean blnClearMap) {
		if (loc != null && !loc.getLogs().isEmpty()) {
			java.util.Iterator logs = loc.getLogs().iterator();
			Log currentLog = null;
			while (logs.hasNext()) {
				//Get the Log Destinations
				currentLog = (Log) logs.next();
				if (currentLog != null) {
					String key = loc.getName() + currentLog.getName();
					String severityString =
						(String) mOriginalLogSeverities.get(key);
					int severity = 0;
					try {
						severity = Integer.parseInt(severityString);
					} catch (NumberFormatException nfex) {
						String msg =
							"Error while parsing original [severity]=' "
								+ severityString
								+ "' of [location LOG]='"
								+ key
								+ "'";
						log.error(
							"system.exception",
							new Object[] { msg },
							nfex);
					}
					//restore the debug level
					currentLog.setEffectiveSeverity(severity);
					if (blnClearMap)
						mOriginalLogSeverities.remove(key);
				}

			}
		}
	}

	/***
	 * @param sessionId - the ID of the current session
	 * Will release the session trace and will remove the generated 
	 * File Log from the current application Location
	 * At the end all severities will be restored
	 */
	public void stopSessionLogging(String sessionId) {
		log.warn(
			"system.sessionLogging.stopped",
			new Object[] { mSessionId },
			null);
		// unregister session filter
		boolean success = mSessionFileLogs.remove(mSessionFileLog);
		// remove Session Logger from Logging
		if (success) {

			for (Iterator i = mRootLocations.iterator(); i.hasNext();) {
				Location isaLoc = Location.getLocation((String) i.next());
				isaLoc.removeLog(mSessionFileLog);
			}

			if (mSessionFileLogs.size() == 0) {
				synchronized (mOriginalSeverities) {
					restoreOriginalSeverities();
				}
			}
		}
	}

	/**
	 * Returns the filelog path or <code>null</code> if something goes wrong.
	 * @return The filelog path.
	 */
	public String getLogFilePath() {
		try {
			if (mFileLogPattern != null) {
				return new File(mFileLogPattern).getCanonicalPath();
			}
		} catch (IOException e) {
			log.error(
				"The session log path could not be determined. "
					+ e.toString());
		}
		return null;
	}

}