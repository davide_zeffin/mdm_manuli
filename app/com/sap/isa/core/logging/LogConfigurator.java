/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Thorsten Kampp
  Created:      15. Februar 2001, 13:06

  $Revision: #3 $
  $Date: 2001/07/04 $
*****************************************************************************/

package com.sap.isa.core.logging;

import java.io.File;
import java.util.Properties;

import org.apache.struts.util.MessageResources;

import com.sap.isa.core.init.Initializable;
import com.sap.isa.core.init.InitializationEnvironment;
import com.sap.isa.core.init.InitializeException;
import com.sap.isa.core.logging.sla.LogConfiguratorSla;

/**
 * Class that configures the logging. This class should be called from
 * the init-config.xml. It provides the following parameters:
 *
 * <ul>
 *  <li><b><code>type</code></b>:<br>
 *     <p>Selects the logging API to be used. Default is <code>log4j</code>.<br>
 *     Other possibilities include <code>sap</code>, which requires
 *     the SAPMarkets Logging API and the additional package
 *     <code>com.sap.isa.core.logging.sla</code>, that contains an
 *     specific IsaLocationSla Implementation as well as an simple
 *     configuration facility.<br>
 *     Finally you can use <code>off</code> to turn of logging completely.
 *     </p><br>
 *
 *  <li><b><code>version</code></b>:<br>
 *     <p>The version of the logging API. Currently the following versions
 *     are supported:<br>
 *     <ul>
 *      <li>log4j Version 1.0.4 (please use "1.04")
 *     </ul>
 *     Furthermore log4j Version 1.1.2. works but is not supported. While using
 *     the SAPMarkets Logging API, the version has no effect.</p>
 *
 *  <li><b><code>config-file</code></b>:<br>
 *     <p>The name of the config-file, to be used. This resembles either a
 *     log4j-configuration file using the DOMConfigurator or an similar format
 *     for the SAPMarkets Loggging API (which is subject to change).</p>
 *
 *  <li><b><code>path</code></b>:<br>
 *     <p>Path where to store the logfiles. If the <code>path</code> attribute
 *     is not set, the default {application}/WEB-INF/logs/ will be used as
 *     logging directory. If you use / the path will be constructed relative
 *     to /WEB-INF/, e.g. "/tmp/logs/" will log to
 *     {application}/WEB-INF/tmp/logs/. Finally you may also use absolute
 *     paths to a directory, e.g. c:/temp/ as log path.
 *
 *  A typical configuration could look like this:
 *
 * <code>
 * <pre>
 *  &lt;initialization className="com.sap.isa.core.logging.LogConfigurator"&gt;
 *    &lt;param name="type"        value="log4j" /&gt;
 *    &lt;param name="version"     value="1.04" /&gt;
 *    &lt;param name="config-file" value="/WEB-INF/cfg/log-config.xml" /&gt;
 *  &lt;/initialization>
 * </pre>
 * </code>
 *
 * Other configurations may allow to do the following:
 *
 * If you would like to use log4j 1.1.2, you will also need to recompile the
 * application with the new log4j.jar. Please also notice, that there are some
 * changes in the configuration file.
 *
 * <code>
 * <pre>
 *   &lt;param name="type"        value="log4j" /&gt;
 *   &lt;param name="version"     value="1.12" /&gt;
 *   &lt;param name="config-file" value="/WEB-INF/cfg/log-config-log4j_112.xml" /&gt;
 * </pre>
 * </code>
 *
 * Usage of the SAPMarkets Logging API is possible but the possibility to
 * configure the logging is not yet standarized. Notice that you need an
 * additional jar file with the logging API.
 *
 * <code>
 * <pre>
 *   &lt;param name="type"        value="sap" /&gt;
 *   &lt;param name="config-file" value="/WEB-INF/cfg/log-config-sap.xml" /&gt;
 * </pre>
 * </code>
 *
 */
public class LogConfigurator implements LogConfiguratorMBean {

	/**
	 * This will be our logger, as soon as everything is configured
	 */
	protected static IsaLocation localCat = null;

	private static boolean mIsLogFileDownloadAllowed = false;
	/**
	 * The name of the current logger
	 */
	protected final static String localCatName =
		// TODO: check                                   LogConfigurator.class.toString();
	LogConfigurator.class.getName();

	/**
	 * Path to store logs to
	 */
	private static String logPath = null;

	/**
	 * Internal constant for a no-logging system
	 */
	public static int ISALOCATION_NONE = 0;

	/**
	 * Internal constant for log4j logging
	 */
	public static int ISALOCATION_LOG4J = 1;

	/**
	 * Internal constant to use the SAPMarkets Logging API
	 */
	public static int ISALOCATION_SAPMARKETS = 2;

	/**
	 * String used in config file, to use no logging system at all.
	 */
	public static String ISALOCATION_NONE_NAME = "off";

	/**
	 * String used in config file, to use the log4j logging system.
	 */
	public static String ISALOCATION_LOG4J_NAME = "log4j";

	/**
	 * String used in config file, to use the SAPMarkets logging system.
	 */
	public static String ISALOCATION_SAPMARKETS_NAME = "sap";

	/**
	 * Logging initializer
	 */
	Initializable initLogSystem = null;

	/**
	 * Classfiles
	 */
	private static String lType;

	/**
	 * Errorcodes (Please use 1,2,4,8,16,..)
	 */
	private static int LOG4J_NOT_AVAILABLE = 0x1;

	/**
	 * The properites that has been read.
	 */
	protected static Properties props = null;

	private static String rootPathWebApp;

	/**
	 * Creates new LogConfigurator
	 */
	public LogConfigurator() {
	}

	/**
	 * Returns the current config-file to be used.
	 *
	 * @return the name of the config file including path
	 */
	public String getConfigFile() {
		return props.getProperty("config-file");
	}

	/**
	 * Changes the current config-file to be used.
	 * You have to initalize the logging after changing the config file.
	 *
	 * @param configFile the name of the config file including path
	 */
	public void setConfigFile(String configFile) {
		props.setProperty("config-file", configFile);
	}

	/**
	 * Returns the path to the log files (absolute). You may use this
	 * after the LogConfigurator has been initialized properly. Otherwise
	 * the result is null. If the directory does not exists or is invalid
	 * the result will be an empty string.
	 *
	 * @param path absolute directory to the log files
	 */
	public static String getLogPath() {
		return logPath;
	}

	/**
	 * Returns the root path of the web application
	 */
	public static String getWebAppRootPath() {
		return rootPathWebApp;
	}

	/**
	 * This method will be called from the <code>ActionServlet</code>
	 * It initializes the Logging facilities.
	 *
	 * @param servlet The ActionServlet we are called from
	 * @param props The Properties required by the initialization
	 *
	 * @exception InitializationException at any problem during the intialization
	 */
	public void initialize(
		InitializationEnvironment environment,
		Properties props)
		throws InitializeException {

		LogConfigurator.props = props;

		rootPathWebApp = environment.getRealPath("/");
		MessageResources msgResource = null;
		//InputStream input = null;
		String path = null;
		int errorCode = 0;
		// --------------------------------------------------------------------
		// Retrieve data out of the environment
		// --------------------------------------------------------------------

		msgResource = environment.getMessageResources();
		/* not required any more - the whole configuration is in bootstrap-config.xml
		input = environment.getResourceAsStream(getConfigFile());
		if (input == null) {
			throw new InitializeException(
				msgResource.getMessage("system.noProperty", "config-file"));
		}
        */
		// --------------------------------------------------------------------
		// Finding type of logging API
		// --------------------------------------------------------------------
		// we support inly the J2EE Logging API in this version
		lType = props.getProperty("type");
		if(lType == null && !lType.equalsIgnoreCase("sap")) lType = "sap";

		// --------------------------------------------------------------------
		// Finding Path to /WEB-INF/logs/
		// --------------------------------------------------------------------

		// first check if there is a path at all
		path = props.getProperty("path");

		if (path == null || path.equals("")) {
			path = environment.getRealPath("/WEB-INF/logs/");

		} else if (path.startsWith("/WEB-INF/")) {
			path = environment.getRealPath(path);

			// if the path starts with a slash assume relative to WEB-INF
			// (due to security, there is no way easy way to log directly to
			// the directory of the web application)

		} else if (path.startsWith("/")) {
			path = environment.getRealPath("/WEB-INF" + path);
		}
		// finally use the absolute path
		File check = new File(path);
		// now check if the path exists and is a directory - otherwise
		// set check/path to null (we do not use it in that case..)
		if (!(check != null && check.exists() && check.isDirectory())) {
			check = null;
			path = "";
		}

		// update property
		props.setProperty("path", path);

		// set logpath statically to allow access to other components
		LogConfigurator.logPath = path;

		// provide the Admin class with the name of the configuratio file
		//not required
		//Admin.setConfigFileName(environment.getRealPath(getConfigFile()));

		// --------------------------------------------------------------------
		// Configuring using specialized configurator
		// --------------------------------------------------------------------

		// dynamically try to instantiate sla configurator
		initLogSystem = new LogConfiguratorSla();
		
		if (initLogSystem != null)
			initLogSystem.initialize(environment, props);

		// ----------------------------------------------------------------- //
		// Logging is configured now                                         //
		// ----------------------------------------------------------------- //

		localCat = IsaLocation.getInstance(localCatName);

		// Report any errors that might have occurred
		localCat.warn("system.initLogging.configured", null);
	}

	/**
	 *  This method will be called from the <code>ActionServlet</code>
	 *  It terminates the logging facilities.
	 */
	public void terminate() {

		// Allow to clean up...
		LogConfigurator.props = null;
		LogConfigurator.localCat = null;
		initLogSystem.terminate();
	}

	/**
	 * Returns the value of an initialization property for the given key
	 * @return key key of the property
	 * @return the value of the property
	 */
	public static String getInitPropertyValue(String key) {
		return props.getProperty(key);
	}

	/**
	 * Returns all initialization property values
	 * @return the initialization property values
	 */
	public static Properties getInitPropertyValues() {
		return props;
	}

	/**
	 * Returns the type of the currently active logging API
	 * @return Type of the currently active logging API
	 */
	public static String getLoggingApiType() {
		return lType;
	}
}
