package com.sap.isa.core.logging;

/**
 * A class for displying messages on the logging JSP.
 *
 */
public class AdminMessage {
	
	public final static String ERROR = "error";
	public final static String INFO  = "info";
	
	/**
	 * Contains ths severity of the message.
	 */
	private String severity = INFO;
	
	/**
	 * The string with the message.
	 */
	private String messageString = "";

	/**
	 * Creates an message instance with the severity and the message itself.
	 * @param severity The severity of this message, please use the constants in this class.
	 * @param message The message that should be displayed.
	 */
	public AdminMessage(String severity, String message) {
		if( severity != null ) {
			this.severity = severity;
		}
		this.messageString = message;
	}
	
	/** 
	 * Creates an empty message instance. Default severity is info.
	 */
	public AdminMessage() {
	}
	
	/**
	 * Returns the message string. 
	 * @return The message string.
	 */
	public String getMessageString() {
		return messageString;
	}

	/**
	 * Sets the message string.
	 * @param messageString The message string.
	 */
	public void setMessageString(String messageString) {
		this.messageString = messageString;
	}

	/**
	 * Returns the message severity.
	 * @return The message severity.
	 */
	public String getSeverity() {
		return severity;
	}

	/**
	 * Sets the message severity.
	 * @param severity The message severity.
	 */
	public void setSeverity(String severity) {
		this.severity = severity;
	}
}
