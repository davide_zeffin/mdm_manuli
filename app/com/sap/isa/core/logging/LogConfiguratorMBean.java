/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:      Franz Mueller
  Created:     06. March 2001

  $Revision: #2 $
  $Date: 2001/07/04 $
*****************************************************************************/

package com.sap.isa.core.logging;

import com.sap.isa.core.init.Initializable;

/**
 *   Using this interface it is possible to control the implementing
 *   instance through JMX.
 */
public interface LogConfiguratorMBean extends Initializable {

    /**
     * Get the name of the config file
     *
     * @return config filename
     */
    public String getConfigFile();

    /**
     * Set the name of the config file
     *
     * @param configFile a filename to a config file
     */
    public void setConfigFile(String configFile);

}