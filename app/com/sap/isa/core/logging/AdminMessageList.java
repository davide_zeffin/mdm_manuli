package com.sap.isa.core.logging;

import java.util.HashSet;
import java.util.Iterator;

/**
 * A container class for messages.
 */
public class AdminMessageList {
	
	private HashSet infoMessages = null;
	private HashSet errorMessages = null;
	private boolean hasInfoMessages = false;
	private boolean hasErrorMessages = false;
	
	/**
	 * Constructs an empty message list.
	 *
	 */
	public AdminMessageList() {
		infoMessages = new HashSet();
		errorMessages = new HashSet();
	}
	
	/**
	 * Adds an info message into the list. The message must be severity of info, it won't be added. 
	 * @param message The message to be displayed on the page.
	 */
	public void addInfoMessage(AdminMessage message) {
		if(message.getSeverity().equals(AdminMessage.INFO)) {
			infoMessages.add(message);
			hasInfoMessages = true;
		}
	}
	
	/**
	 * Adds an error message into the list. The message must be severity of error, it won't be added. 
	 * @param message The message to be displayed on the page.
	 */
	public void addErrorMessage(AdminMessage message) {
		if(message.getSeverity().equals(AdminMessage.ERROR)) {
			errorMessages.add(message);
			hasErrorMessages = true;
		}
	}

	/**
	 * Removes a message from the list.
	 * @param message The message that should be removed from the list.
	 * @return <code>true</code> if the message could be removed, otherwise <code>false</code>.
	 */
	public boolean removeMessage(AdminMessage message) {
		if(message.getSeverity().equals(AdminMessage.INFO)) {
			return infoMessages.remove(message);
		} else if(message.getSeverity().equals(AdminMessage.ERROR)) {
			return errorMessages.remove(message);
		} else {
			return false;
		}
	}
	
	/**
	 * Returns all info messages as a html string.
	 * <strong>Example</strong>
	 * <span>message</span><br /><span.... 
	 * @return Returns all info messages as a html string.
	 */
	public String getInfoMessages() {
		return getMessages(infoMessages);
	}

	/**
	 * Returns all error messages as a html string.
	 * <strong>Example</strong>
	 * <span>message</span><br /><span.... 
	 * @return Returns all error messages as a html string.
	 */		
	public String getErrorMessages() {
		return getMessages(errorMessages);
	}
	
	/**
	 * A private method that is generating the html messages from a hashset.
	 * @param messages The hashset with the messages.
	 * @return A html string for display on the JSP.
	 */
	private String getMessages(HashSet messages) {
		String tagStart = "<span>";
		String tagEnd = "</span><br />\n";
		StringBuffer messStr = new StringBuffer();
		
		Iterator messItr = messages.iterator();
		AdminMessage messObj = null; 
		while (messItr.hasNext()) {
			messObj = (AdminMessage) messItr.next();
			messStr.append(tagStart + messObj.getMessageString() + tagEnd);
		}
		return messStr.toString();
	}

	/**
	 * Returns <code>true</code> if the list contains info messages, otherfalls <code>false</code>. 
	 * @return <code>true</code> if the list contains info messages, otherfalls <code>false</code>.
	 */
	public boolean hasInfoMessages() {
		return hasInfoMessages;
	}

	/**
	 * Returns <code>true</code> if the list contains error messages, otherfalls <code>false</code>. 
	 * @return <code>true</code> if the list contains error messages, otherfalls <code>false</code>.
	 */
	public boolean hasErrorMessages() {
		return hasErrorMessages;
	}
}