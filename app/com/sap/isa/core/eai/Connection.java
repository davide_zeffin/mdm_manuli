/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      06 March 2001
*****************************************************************************/
package com.sap.isa.core.eai;

/**
 * This interface should be implemented by a class wich encapsulates a
 * physical backend connection
 */
public interface Connection {

  /**
   * Closes this backend connection. It is not defined whether the underlying
   * physical connection is closed or not.
   */
  public void close();

  /**
   * This method destroys the backend connection. Call this method only if
   * you are sure that no one else depends on this connection.
   */
  public void destroy();

  /**
   * Gets a key which unbiguously identifies this connection. This
   * key can be used to get the same connection from the connection factory
   * @return key which unbiguously identifies this connection
   */
  public String getConnectionKey();

  /**
   * This method checks if the connection is vaid and can be used to connect
   * the backend system.
   * @return <code>true</code> if the connection can be used to communicate with
   *    the backend system. <code>false</code> if the connection is not valid
   *    and therefoce cannot be used to communicate with the backend system.
   *    You do not need to check if a connection is valid before you use it. If
   *    you try to execute a function module using an invalid connection then
   *    a <code>BackendException</code> is thrown
   */
  public boolean isValid();

  /**
   * Adds a listener to this managed connection.
   * @param aListener A listener which will be notofied if connection events occur
   */
  public void addConnectionEventListener(ConnectionEventListener aListener);


}