package com.sap.isa.core.eai.sp.jco;

// java imports
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;

import com.sap.isa.core.cache.Cache;
import com.sap.isa.core.cache.JCoFunctionLoader;
import com.sap.isa.core.eai.BackendContext;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.ConnectionConfig;
import com.sap.isa.core.eai.ConnectionDefinition;
import com.sap.isa.core.eai.ManagedConnection;
import com.sap.isa.core.eai.ManagedConnectionFactory;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.security.PasswordEncrypt;
import com.sap.isa.core.security.SecureStore;
import com.sap.isa.core.util.CodePageUtils;
import com.sap.isa.core.util.MiscUtil;
import com.sap.mw.jco.IFunctionTemplate;
import com.sap.mw.jco.IRepository;
import com.sap.mw.jco.JCO;

/**
 * This is a specific Connection Manager for JCo connections
 */
public class JCoManagedConnectionFactory
	implements ManagedConnectionFactory {
 
	public static final String JCO_CLIENT = "client";
	public static final String JCO_USER = "user";
	public static final String JCO_PASSWD = "passwd";
	public static final String JCO_LANG = "lang";
	public static final String JCO_SYSNR = "sysnr";
	public static final String JCO_ASHOST = "ashost";
	public static final String JCO_MSHOST = "mshost";
	public static final String JCO_GWHOST = "gwhost";
	public static final String JCO_GWSERV = "gwserv";
	public static final String JCO_R3NAME = "r3name";
	public static final String JCO_GROUP = "group";
	public static final String JCO_TPHOST = "tphost";
	public static final String JCO_TPNAME = "tpname";
	public static final String JCO_TYPE = "type";
	public static final String JCO_CODEPAGE = "codepage";

	public static final String JCO_MYSAPSSO2 = "jco.client.mysapsso2";
	public static final String JCO_GETSSO2 = "jco.client.getsso2";

	public static final String JCO_TYPE_EXTERNAL = "E";
	public static final String JCO_TYPE_R3 = "3";

	public static final String JCO_MAXCON = "maxcon";
	public static final String JCO_RESET_ON_RELEASE = "resetOnRelease";

	static final String INVALID_PASSWORD = SecureStore.ERROR_RETRIEVING_SECURE_STORE;

	/**
	 * Used for read only cache of a function module
	 * The key to distinguish function modules is constructed as follows:
	 * <sap logon value of client> + <sap logon value of language> +
	 * <string containing values of all import parameters>
	 */
	public static final String CACHE_TYPE_A = "cacheType_A";

	/**
	 * Used for read only cache of a function module
	 * The key to distinguish function modules is constructed as follows:
	 * <sap logon value of client> + <sap logon value of language> + <sap logon user name>
	 * <string containing values of all import parameters>
	 */
	public static final String CACHE_TYPE_B = "cacheType_B";

	/**
	 * Used as prefix for specifying a function module which has
	 * to be replaced by an other function module
	 */
	public static final String FUNCTION_MODULE_PREFIX = "FM:";

	/**
	 * Used as name of properties of JCoManagedConnectionFactory
	 * This property name specifies that a event listener has
	 * to recieve events when a specific function module is executed
	 */
	public static final String EXEC_EVENT_PREFIX = "FMEVENT:";

	/**
	 * Used as name of properties of JCoManagedConnectionFactory
	 * This property name specifies that a function module is to be traced
	 * before it is called
	 */
	public static final String JCO_FUNCTION_TRACE_BEFORE =
		"jco.function.trace.before";

	/**
	 * Used as name of properties of JCoManagedConnectionFactory
	 * This property name specifies that a function module is to be traced
	 * after it is called
	 */
	public static final String JCO_FUNCTION_TRACE_AFTER =
		"jco.function.trace.after";

	/**
	 * Used as parameter to configure which import parameter should not be trace
	 * in function module trace
	 */
	public static final String JCO_FUNCTION_TRACE_EXCLUDE_IMPORT =
		"excludeimport";

	/**
	 * Used as parameter to configure which import parameter should not be trace
	 * in function module trace
	 */
	public static final String JCO_FUNCTION_TRACE_EXCLUDE_EXPORT =
		"excludeexport";

	/**
	 * Used as parameter to configure which import parameter should not be trace
	 * in function module trace
	 */
	public static final String JCO_FUNCTION_TRACE_EXCLUDE_TABLE =
		"excludetable";


	/**
	 * The name of the cache-region used to cache JCo functions
	 */
	public static final String CACHE_REGION_NAME = "JCO";

	/**
	 * name of the JCo managed connection factory
	 */
	public static final String JCO_MANAGED_CON_FACTORY = "JCO";

	/**
	 * Connection property which indicates that ABAP debug should be enabled
	 * value: jco.client.abap_debug
	 */
	public static final String JCO_ABAP_DEBUG = "jco.client.abap_debug";

	/**
	 * Connection property which indicates which JCo Function modules should
	 * be ABAP-debugged.
	 * Note: This makes only sense when working with Statelss Connections
	 */
	public static final String JCO_ABAP_DEBUG_FUNC =
		"jco.client.abap_debug.func";

	public static final String CON_STATUS_CONNECTED = "CONNECTED";
   
	public static final String CON_STATUS_CLOSED = "CLOSED";


	private static List paramNames = new LinkedList();
	public static final String PASSWD_DELIMITER = "?";
	private static boolean mIsPooledSupported = JCoUtil.isPooledMethodSupported();


	static {

		// JCO.setTraceLevel(1);

		paramNames.add(JCO_CLIENT);
		paramNames.add(JCO_USER);
		paramNames.add(JCO_PASSWD);
		paramNames.add(JCO_LANG);
		paramNames.add(JCO_SYSNR);
		paramNames.add(JCO_ASHOST);
		paramNames.add(JCO_MSHOST);
		paramNames.add(JCO_GWHOST);
		paramNames.add(JCO_GWSERV);
		paramNames.add(JCO_GROUP);
		paramNames.add(JCO_TPHOST);
		paramNames.add(JCO_TPNAME);
		paramNames.add(JCO_MAXCON);
		paramNames.add(JCO_R3NAME);
		paramNames.add(JCO_MYSAPSSO2);
		paramNames.add(JCO_GETSSO2);
		paramNames.add(JCO_CODEPAGE);

	}

	protected static IsaLocation log =
		IsaLocation.getInstance(JCoManagedConnectionFactory.class.getName());

	private Properties conFacProps;
	private Map conConfigs = new HashMap();
	private Map conDefs;
	private Map conDefsToJCoPoolName =
		Collections.synchronizedMap(new HashMap());
	private Map mJCoPoolNames = Collections.synchronizedMap(new HashMap());
	private String factoryName;
	private Map mJCoRepositories = Collections.synchronizedMap(new HashMap());
	private Map cacheFuncNames = new HashMap();
	private Map mReplaceFM;

	private Set mFuncTraceBeforeExec = new HashSet();
	private Set mFuncTraceAfterExec = new HashSet();

	private Map mFuncTraceBeforeExecExcludeImport = new HashMap();
	private Map mFuncTraceBeforeExecExcludeExport = new HashMap();
	private Map mFuncTraceBeforeExecExcludeTable = new HashMap();

	private Map mFuncTraceAfterExecExcludeImport = new HashMap();
	private Map mFuncTraceAfterExecExcludeExport = new HashMap();
	private Map mFuncTraceAfterExecExcludeTable = new HashMap();
	private Map mPasswordHash = new HashMap();


	private Map mExecEvents = new HashMap();
	private Map mExecEventListener = new HashMap();
	private static boolean mIsActivityFMEnabled = false;
	private static Map mActivityFMs;

	private static String[] propertyNamesUser = new String[4];
	private static String[] propertyNamesHost = new String[2];
	private static String[] propertyNamesPool = new String[1];

	/**
	 * Constructor
	 */
	public JCoManagedConnectionFactory() {
	}

	/**
	 * Specifies the name of the factory
	 * @param factoryName type of factory
	 */
	public void setFactoryName(String factoryName) {
		this.factoryName = factoryName;
	}

	/**
	 * Gets the type of the factory
	 * @return type of factory
	 */
	public String getFactoryName() {
		return factoryName;
	}

	/**
	 * Gets a managed connection
	 * @param conConfigName name of connection configuration which should be
	 *    used to create the managed connection
	 * @param conDefProps connection definition properties
	 * @return a managed connection
	 * @throws BackendException if creating managed connection fails
	 */
	public synchronized ManagedConnection getManagedConnection(
		String conConfigName,
		Properties conDefProps)
		throws BackendException {
		if (log.isDebugEnabled()) {
			log.debug(
				"getManagedConnection(String, Properties) entered; conConfigName=\""
					+ conConfigName
					+ "\" conDef props=\""
					+ getPropsAsString(conDefProps)
					+ "\"");
		}

		ManagedConnection managedConnection =
			createManagedConnection(conConfigName);

		String conDefName = managedConnection.getConnectionDefinitionName();

		// check if connection properties contain a codepage, if not then assign one
		assignCodepageToConProps(conDefProps);

		// create mixed connection definition object
		ConnectionDefinition mixedConDef =
			createMixedConnectionDefiniton(conDefName, conDefProps);

		if (isPooled(mixedConDef.getProperties()))
			 ((JCoConnection) managedConnection).isPooled(true);

		// get key for that new connection definition
		String conDefKey = mixedConDef.getName();

		if (mJCoPoolNames.get(conDefKey) == null) {
			// no suitable pool available. try to create a new connection pool
			try {
				addJCoPool(mixedConDef);
				// the pool could not be created. The missing pool will be reported to
				// the user while trying to execute JCO.Function
			} catch (BackendException ex) {
				log.debug(ex.getMessage());
			}
		}

		if (!conDefs.containsValue(conDefKey)) {
			if (log.isDebugEnabled())
				log.debug(
					"Adding a new connection definition: "
						+ getConDefNameString(conDefKey));

			// specify that this definition name is from now on available
			conDefs.put(conDefKey, mixedConDef);
			// key of connection definition equals name of corresponging JCo pool
			conDefsToJCoPoolName.put(conDefKey, conDefKey);
		}

		managedConnection.setConnectionDefinitionName(conDefKey);

		// getting system id of the corresponding JCo pool
		String sid = (String) mJCoPoolNames.get(conDefKey);

		// associating sid with connection
		 ((JCoConnection) managedConnection).setJCoRepositoryName(sid);
		((JCoConnection) managedConnection).setCachedFunctionNames(
			cacheFuncNames);

		managedConnection.init(null);

		return managedConnection;
	}

	/**
	 * Gets a managed connection
	 * @param conConfigName name of connection configuration which should be
	 *    used to create the managed connection
	 * @return a managed connection
	 * @throws BackendException if creating managed connection fails
	 */
	public synchronized ManagedConnection getManagedConnection(String conConfigName)
		throws BackendException {
		if (log.isDebugEnabled())
			log.debug("getManagedConnection(String) entered");

		ManagedConnection managedConnection =
			createManagedConnection(conConfigName);

		// set JCo repository name
		String conDefName = managedConnection.getConnectionDefinitionName();
		String poolName = (String) conDefsToJCoPoolName.get(conDefName);
		String sid = (String) mJCoPoolNames.get(poolName);
		((JCoConnection) managedConnection).setJCoRepositoryName(sid);

		// init connection
		managedConnection.init(null);

		return managedConnection;
	}

	/**
	 * Inititalizes a managed connection factory
	 * @param conDefs A Map containing backend connection definitions
	 * @throws BackendException if initializing connection fails
	 */
	public void initConnections(Map conDefs) throws BackendException {
		this.conDefs = new HashMap(conDefs);

		log.info("system.eai.jco.init.con");

		if (conDefs == null || conDefs.size() == 0) {
			String errMsg =
				"There is no configuration for JCo connection Manager";
			log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.eai.exception", new Object[] { errMsg }, null);
			throw new BackendException(errMsg);
		}

		Iterator conDefIterator = conDefs.values().iterator();
		ConnectionDefinition conDef = null;
		while (conDefIterator.hasNext()) {
			conDef = (ConnectionDefinition) conDefIterator.next();
			// get connection parameters
			Properties conProps = conDef.getProperties();

			// check if codepage is set, if not assign codepage
			assignCodepageToConProps(conProps);

			//decrypt password
			decryptPassword(conProps);

			// create corresonding key to the connection definition
			conDefsToJCoPoolName.put(conDef.getName(), getConDefKey(conDef));

			log.info(
				"system.eai.jco.init.ProcessingConDef",
				new Object[] { getConDefNameString(conDef.getName())},
				null);

			// check if connection parameters ok
			if (isPooled(conProps)) {
				try {
					addJCoPool(conDef);
				} catch (BackendException ex) {
					log.debug("Adding pool failed");
				}
			}
		}

		initProperties();
	}

	/**
	 * This method initializes a new JCo repository, if is not already initialized
	 * @param sid system id of SAP system
	 * @param poolName name of the JCo pool which should be used by the repository.
	 *      If the poolName is <code>null<code> then the JCO.Client is used
	 */
	private void addJCoRepository(String sid, String poolName) {
		if (sid == null || sid.length() == 0)
			return;

		// check if repository for the given connection already exists
		if (mJCoRepositories.containsKey(sid))
			return;
		try {
			IRepository repository = null;
			if (!(poolName == null || poolName.length() == 0))
				repository = JCO.createRepository(sid, poolName);

			log.info(
				"system.eai.jco.addRep.Success",
				new Object[] { sid },
				null);

			if (repository != null)
				mJCoRepositories.put(sid, repository);

		} catch (JCO.Exception ex) {
			if (log.isDebugEnabled())
				log.debug("Adding JCo repository for sid=" + sid + " failed");
		}
	}

	/**
	 * Returns a JCO.Function for the given name of a remote
	 * callable function module
	 * @param repName name of repository which should serve the JCO.Function
	 * @param funcName name of remote callable function module
	 *    is no repository for the given system
	 * @param raiseException if true then no exception is thrown in case function module is not 
	 * available but <code>null</code> is returned
	 * @return JCO.Function or <code>null</code> if there is no repository for the
	 *    given name
	 * @throws BackendException if getting JCo function fails and raiseException=true
	 */
	public JCO.Function getJCoFunction(String repName, String funcName, boolean raiseException)
		throws BackendException {

		// check if function has to be replaced by
		if (mReplaceFM != null) {
			String newFM = (String) mReplaceFM.get(funcName);
			if (newFM != null)
				funcName = newFM;
		}

		IRepository rep = (IRepository) mJCoRepositories.get(repName);
		if (rep == null) {
			String exMsg =
				"requesting repository for system \""
					+ repName
					+ "\" No JCo Function repository found. Please create a connection pool for this backend system";
			if (log.isDebugEnabled())
				log.warn(exMsg);
			throw new BackendException(exMsg);
		}

		try {
			if (log.isDebugEnabled())
				log.debug(
					"Serving function module \""
						+ funcName
						+ "\" from existing repository \""
						+ repName
						+ "\"");
			IFunctionTemplate template = rep.getFunctionTemplate(funcName);
			if (template == null) {
				String msg =
					"Error getting function module \""
						+ funcName
						+ "\" from system"
						+ "\""
						+ repName
						+ "\". Function module does not exist.";
				if (raiseException) {
					log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.eai.exception", new Object[] { msg }, null);
					throw new BackendException(msg);	
				} else {
					return null;
				}
			}
			return template.getFunction();
		} catch (Throwable ex) {
			String msg =
				"Error getting function module \""
					+ funcName
					+ "\" from system"
					+ "\""
					+ repName
					+ "\"";
			msg = msg + ex.toString();
			if (raiseException) {
				log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.eai.exception", new Object[] { msg }, ex);
				throw new BackendException(ex.toString());	
			} else {
				return null;
			}
			
			
		}
	}

	/**
	 * Returns a JCo Function using a single client connection
	 * @param client JCo Client
	 * @param funcName name of JCo function
	 * @param isPooled true if JCo client is pooled
	 * @return a JCo function
	 * @throws BackendException if getting JCo function fails
	 *
	 */
	public JCO.Function getJCoFunction(
		JCO.Client client,
		String funcName,
		boolean isPooled)
		throws BackendException {
		// check if function has to be replaced by
		if (mReplaceFM != null) {
			String newFM = (String) mReplaceFM.get(funcName);
			if (newFM != null)
				funcName = newFM;
		}

		JCO.Function func;
		try {
			client.connect();
			IRepository mJCoRep = JCO.createRepository("tmp", client);
			IFunctionTemplate template = mJCoRep.getFunctionTemplate(funcName);
			if (template == null) {
				String msg =
					"Error getting function module \""
						+ funcName
						+ "\" from system"
						+ "\""
						+ mJCoRep
						+ "\". Function module does not exist.";
				log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.eai.exception", new Object[] { msg }, null);
				throw new BackendException(msg);
			}
			return template.getFunction();
		} catch (Throwable ex) {
			String msg = "Error getting function module '" + funcName + "'";
			msg = msg + " using single connection";
			msg = msg + JCoUtil.getClientInfo(client);
			msg = msg + ex.toString();
			log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.eai.exception", new Object[] { msg }, ex);
			throw new BackendException(msg.toString());
		} finally {
			releaseClient(client, isPooled);
		}
	}

	/**
	 * Checks the validity of parameters container by the connection definition
	 * @param conProps connection properties
	 * @return true if connection properties provide enough information to create
	 *      a JCo connection
	 */
	protected boolean checkConParams(Properties conProps) {
		Iterator conPropsIterator = conProps.keySet().iterator();
		String propValue = null;
		while (conPropsIterator.hasNext()) {
			propValue = (String) conPropsIterator.next();
			if (!paramNames.contains(propValue)) {
				log.debug(
					"JCo connection parameter '"
						+ propValue
						+ "' is not valid.");
				return false;
			}
		}
		return true;
	}
	/**
	 * Adds a JCo pool to this JCo connection manager
	 * @param conDef a connection definition object containing information on a
	 *               JCo connection
	 * @throws BackendException if adding pool fails
	 */
	protected synchronized void addJCoPool(ConnectionDefinition conDef)
		throws BackendException {
		Properties conProps = conDef.getProperties();

		String sid = null;
		String poolSize = conProps.getProperty(JCO_MAXCON);
		if (poolSize == null)
			return;

		// check if poolsize = 0
		try {
			int poolSizeInt = Integer.parseInt(poolSize);
			if (poolSizeInt == 0) {
				// poolsize = 0
				return;
			}
		} catch (NumberFormatException ex) {
			log.debug(ex.getMessage());
			// poolsize not a number
			return;
		}

		// all properties seem to be there
		// create and add new JCo pool
		String poolName = getConDefKey(conDef);

		JCO.Client client = null;
		try {
			log.info(
				"system.eai.jco.addPool",
				new Object[] { getConDefNameString(poolName)},
				null);

			//open connection to check if parameters are sufficient
			client = JCO.createClient(conProps);
			log.debug("Checking if logon parameter valid...");

			// check if password and user is available (only when logging on to R/3)
			if (!JCoUtil.isClientConnectable(client)) {
				String msg = JCoUtil.isClientConnectableMessage(client) + ". " +
						"JCO cannot create pool:" + getConDefNameString(poolName) + "'";  
				log.debug(msg);
				//throw new BackendException("Adding JCo pool \"" + getConDefNameString(poolName) + "\" failed");
				return;
			}

			long start = System.currentTimeMillis();
			client.connect();
			long end = System.currentTimeMillis();
			long millis = end - start;
			if (log.isDebugEnabled()) {
				log.debug("system.eai.jco.addPool '" + poolName  + "' needs " + millis + " to connect.");
			}
			
			// connection is valid, check if pool already exists
			if (JCO.getClientPoolManager().getPool(poolName) != null) {
				log.warn(
					"system.eai.jco.poolAlreadyExists",
					new Object[] { poolName },
					null);
			} else {
				// add pool only if it not already exists (pools have VM scope)
				int maxConnections =
					Integer.parseInt(conProps.getProperty(JCO_MAXCON));
				JCO.addClientPool(poolName, // pool name
				maxConnections, // max connections
				conProps);
			}
			log.info(
				"system.eai.jco.addPool.success",
				new Object[] { getConDefNameString(poolName)},
				null);

			// add JCo repository
			sid =
				client.getAttributes().getSystemID()
					+ client.getAttributes().getClient();
			sid.toUpperCase();
			if (sid != null && sid.length() != 0)
				addJCoRepository(sid, poolName);

			// check if connection returned to the pool have to be resetted
			String resetOnRelease = conProps.getProperty(JCO_RESET_ON_RELEASE);
			if (resetOnRelease != null
				&& resetOnRelease.equalsIgnoreCase("false")) {
				JCO.getClientPoolManager().getPool(poolName).setResetOnRelease(
					false);
			}
			// disconnect test client
			client.disconnect();
		} catch (Exception ex) {
			if (client != null && client.isAlive())
				client.disconnect();
			log.warn(
				"system.eai.jco.addPool.failure",
				new Object[] { getConDefNameString(poolName)},
				null);
			throw new BackendException(
				"Adding JCo pool \""
					+ getConDefNameString(poolName)
					+ "\" failed");
		}
		mJCoPoolNames.put(poolName, sid);

	}

	/**
	 * Adds connection configurations to the connection factory
	 * @param conConfigs a set of connection configurations
	 */
	public void addConnectionConfigs(Map conConfigs) {
		this.conConfigs.putAll(conConfigs);
		//    this.conConfigs = conConfigs;
	}

	/**
	 * This method returns a physical connection of the specified connection definition
	 * The definition has to be complete.
	 * @param conDefName Name of the connection definition
	 * @return a JCO.Client reference
	 * @throws BackendException if getting connection fails
	 */
	public synchronized Object getConnection(String conDefName)
		throws BackendException {
		// check if there is a suitable pool which can handle this request
		ConnectionDefinition conDef =
			(ConnectionDefinition) conDefs.get(conDefName);
		//String conDefKey = getConDefKey(conDef);
		String conDefKey = (String) conDefsToJCoPoolName.get(conDefName);
		if (mJCoPoolNames.containsKey(conDefKey)) {
			//String poolName = (String)mJCoPoolNames.get(conDefName);
			JCO.Client client = JCO.getClient(conDefKey);
			if (log.isDebugEnabled())
				log.debug(
					" Serving existing JCO.Client '"
						+ client
						+ "' from JCo pool '"
						+ getConDefNameString(conDefKey)
						+ "' for '"
						+ getConDefNameString(conDefName)
						+ "' connection definition");
			return client;
		} else {
			Properties props = conDef.getProperties();
			JCO.Client client = JCO.createClient(props);
			if (log.isDebugEnabled())
				log.debug(
					"Serving connection handle for a new JCO.Client '"
						+ client
						+ "' connection for '"
						+ getConDefNameString(conDefName)
						+ "' connection definition, con params"
						+ getPropsAsString(props).toString());
			return client;
		}
	}

	/**
	 * This method returns a physical connection of the specified connection definition
	 * The passed connection parameters can extend/replace existing parameters.
	 * NOTE: not implemented for JCo
	 * @param conDefName Name of the connection definition
	 * @param conDefProps
	 * @return null
	 * @throws BackendException
	 *
	 */
	public synchronized Object getConnection(
		String conDefName,
		Properties conDefProps)
		throws BackendException {
		return null;
	}

	/**
	 * This method calculates a key for a given connection definition object.
	 * This key can be used for a name of a JCo pool
	 * @param conDef connection definition
	 * @return key created using data contained in the connection definition
	 */
	private String getConDefKey(ConnectionDefinition conDef) {

		StringBuffer sb = new StringBuffer();
		Properties conProps = (Properties) conDef.getProperties().clone();
		String password = conProps.getProperty(JCO_PASSWD);
		if (password == null)
			password = "";
		conProps.remove(JCO_PASSWD);
		Iterator conPropsIterator = conProps.values().iterator();
		while (conPropsIterator.hasNext())
			sb.append((String) conPropsIterator.next());

		String passwordHash = null;
		if (mPasswordHash.containsKey(password))
			passwordHash = (String)mPasswordHash.get(password);
		else  {
			passwordHash = MiscUtil.getDigest(password);
			mPasswordHash.put(password, passwordHash);
		}




		if (password != null && password.length() > 0)
			sb.append(PASSWD_DELIMITER + passwordHash + PASSWD_DELIMITER);

		return sb.toString().toLowerCase();
	}

	/**
	 * This method can be used to check if the connection factory can provide
	 * a connection for the provided connection parameters.
	 * This method is not implemented
	 * @param conDef Connection Definition
	 * @return true if the connection factory can provide a connection for the
	 *         provided connection definition
	 * @throws UnsupportedOperationException
	 */
	public boolean isConnectionAvailable(ConnectionDefinition conDef) {
		if (true)
			throw new UnsupportedOperationException();
		return false;
	}

	/**
	 * This method creates a new connection definiton object containing properties
	 * of an existing connection definition object and the passed parametes
	 * @param existingConDefName name of existing connection definition
	 * @param newConDefProps new properties which should be mixed up with the
	 *    properties of the existing connection definition
	 * @return a new connection definition with mixed properties
	 *
	 */
	private ConnectionDefinition createMixedConnectionDefiniton(
		String existingConDefName,
		Properties newConDefProps) {

		if (log.isDebugEnabled())
			log.debug(
				"Creating mixed connection definition. Mixing existing conDef \""
					+ getConDefNameString(existingConDefName)
					+ "\" with properties \""
					+ getPropsAsString(newConDefProps)
					+ "\"");

		ConnectionDefinition newConDef = new ConnectionDefinition();

		ConnectionDefinition existingConDef =
			(ConnectionDefinition) conDefs.get(existingConDefName);

		if (existingConDef == null) {
			newConDef.setProperties(newConDefProps);
		} else {
			// transfer all properties form the new connection definition into a clone
			// of the existing connection definition using new connection definition
			Properties newMixedProps =
				(Properties) existingConDef.getProperties().clone();
			newConDef.setProperties(newMixedProps);

			Enumeration propNamesEnum = newConDefProps.propertyNames();
			String propName = null;
			while (propNamesEnum.hasMoreElements()) {
				propName = (String) propNamesEnum.nextElement();
				newMixedProps.put(
					propName,
					newConDefProps.getProperty(propName));
			}
		}

		newConDef.setName(getConDefKey(newConDef));

		return newConDef;
	}

	/**
	 * Creates an instance of a managed connection
	 * @param conConfigName
	 * @return instance of a managed connection
	 * @throws BackendException if creating managed connection fails
	 */
	private ManagedConnection createManagedConnection(String conConfigName)
		throws BackendException {

		ManagedConnection managedConnection;
		ConnectionConfig conConfig =
			(ConnectionConfig) conConfigs.get(conConfigName);
		if (conConfig == null) {
			String errMsg = "Connection " + conConfigName + " not found";
			log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.eai.exception", new Object[] { errMsg }, null);
			throw new BackendException(errMsg);
		}

		try {
			// create a managed connection
			Class jcoManagedConnectionClass =
				Class.forName(conConfig.getClassName());
			managedConnection =
				(ManagedConnection) jcoManagedConnectionClass.newInstance();

			// set connection config name
			String conDefName = conConfig.getConnectionDefinition();
			managedConnection.setConnectionDefinitionName(conDefName);
			managedConnection.setConnectionConfigName(conConfigName);
			managedConnection.setConnectionFactoryName(factoryName);
			managedConnection.setManagedConnectionFactory(this);

			((JCoConnection) managedConnection).setCachedFunctionNames(
				cacheFuncNames);

			ConnectionDefinition conDef =
				(ConnectionDefinition) conDefs.get(conDefName);
			if (conDef == null) {
				String errMsg =
					"Connection Definition" + conDefName + " not found";
				log.error(LogUtil.APPS_BUSINESS_LOGIC,
					"system.eai.exception",
					new Object[] { errMsg },
					null);
				throw new BackendException(errMsg);
			}
			Properties conDefProps = conDef.getProperties();
			if (isPooled(conDefProps))
				 ((JCoConnection) managedConnection).isPooled(true);

			// assign properties
			if (conConfig.getProperties().size() > 0)
				assignParams(
					conConfig.getProperties(),
					(JCoConnection) managedConnection);

		} catch (Exception ex) {
			String errMsg =
				"Could not create managed connection with the class name '"
					+ conConfig.getName()
					+ "'\n"
					+ ex.toString();
			log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.eai.exception", new Object[] { errMsg }, ex);
			throw new BackendException(errMsg);
		}
		return managedConnection;
	}

	static String getPropsAsString(Properties p) {
		Properties props = (Properties) p.clone();
		if (props.containsKey(JCoManagedConnectionFactory.JCO_PASSWD));
		props.setProperty(
			JCoManagedConnectionFactory.JCO_PASSWD,
			PASSWD_DELIMITER);
		return props.toString();
	}

	private String getConDefNameString(String conDefName) {
		if (conDefName == null)
			return null;

		int i = 0;
		if ((i = conDefName.indexOf(PASSWD_DELIMITER)) != -1)
			conDefName = conDefName.substring(0, i) + PASSWD_DELIMITER;

		return conDefName;
	}

	/**
	 * Returns a list of valid JCo property keys.
	 * @return a List of valid property keys
	 */
	public static List getValidPropertyKeys() {
		return Collections.unmodifiableList(paramNames);
	}

	/**
	 * Sets a set of properties
	 * @param props Properties which can be used for additional configuratio
	 *        of a factory
	 */
	public void setProperties(Properties props) {
		conFacProps = (Properties) props.clone();
	}

	/**
	 * Initializes factory if some additional properties has been passed
	 * to the factory
	 */
	private void initProperties() {
		if (!(conFacProps != null && conFacProps.size() > 0))
			return;
		Enumeration enum = conFacProps.keys();
		String key = null;
		String value = null;
		String token = null;
		while (enum.hasMoreElements()) {
			key = (String) enum.nextElement();
			value = conFacProps.getProperty(key);
			if (key.equalsIgnoreCase(CACHE_TYPE_A)
				|| key.equalsIgnoreCase(CACHE_TYPE_B)) {
				StringTokenizer tokenizer = new StringTokenizer(value, ",;:");
				while (tokenizer.hasMoreElements()) {
					token = tokenizer.nextToken().trim();
					if (log.isDebugEnabled())
						log.debug(
							"Adding SAP function module \""
								+ token
								+ "\"to cache");
					cacheFuncNames.put(token, key);
				}
			}
			key = key.toUpperCase();
			if (key.indexOf(FUNCTION_MODULE_PREFIX) != -1) {
				String oldFmName =
					key.substring(
						FUNCTION_MODULE_PREFIX.length(),
						key.length());
				if (mReplaceFM == null)
					mReplaceFM = new HashMap();

				if (log.isDebugEnabled()) {
					log.debug("Adding function module for replacement [old module]='" + oldFmName +
						" [new module]='" + value + "'");
				}
				mReplaceFM.put(oldFmName, value);
			}
			if (key.indexOf(EXEC_EVENT_PREFIX) != -1) {
				String fmName =
					key.substring(EXEC_EVENT_PREFIX.length(), key.length());
				if (mExecEvents == null)
					mExecEvents = new HashMap();
				if (log.isDebugEnabled()) {
					log.debug("Adding JCo execution event listener [functin module]='" + fmName +
						" [event listener]='" + value + "'");
				}

				mExecEvents.put(fmName, value);
			}

			if (key.equalsIgnoreCase(JCO_FUNCTION_TRACE_BEFORE)) {
				parseFunctionModuleTraceConfig(value, true);
			}

			if (key.equalsIgnoreCase(JCO_FUNCTION_TRACE_AFTER)) {
				parseFunctionModuleTraceConfig(value, false);
			}
		}
	}

	/**
	 * Executes a function. If the function has to be cached it is retrived
	 * from the cache
	 * @param client JCo client used to execute the function
	 * @param func JCo function which has to be exectued
	 * @param cacheType
	 * @throws BackendException if exectuing function fails
	 */
	public final void executeJCoFunction(
		JCO.Client client,
		JCO.Function func,
		String cacheType,
		BackendContext context)
		throws BackendException {

		String fmName = func.getName();

		if (log.isDebugEnabled())
			log.debug(
				"BEGIN executeJCoFunction; [client]='"
					+ client
					+ "' + [func]='"
					+ fmName
					+ "'");

		long startTime = System.currentTimeMillis();
		long execTime = 0;

		// check if there are function execution event listeners for
		// this function module
		String listenerClassName = (String) mExecEvents.get(fmName);
		JCoConnectionEventListener listener = null;

		//*************
		// BEGIN: JCO function exection listerner
		if (listenerClassName != null) {
			// check if listener has already be instantiated
			listener =
				(JCoConnectionEventListener) mExecEventListener.get(
					listenerClassName);
			if (listener == null) {
				// instantiate a new listener
				Class listenerClass = null;
				String errMsg =
					"Error in processing JCo function-module execution events.";
				try {
					listenerClass = Class.forName(listenerClassName);
				} catch (ClassNotFoundException ex) {
					log.error(LogUtil.APPS_BUSINESS_LOGIC,
						"system.eai.exception",
						new Object[] { errMsg },
						ex);
				}
				try {
					listener =
						(JCoConnectionEventListener) listenerClass
							.newInstance();
					// add listener to list of available listeners
					mExecEventListener.put(listenerClassName, listener);
				} catch (IllegalAccessException ex) {
					log.error(LogUtil.APPS_BUSINESS_LOGIC,
						"system.eai.exception",
						new Object[] { errMsg },
						ex);
				} catch (InstantiationException ex) {
					log.error(LogUtil.APPS_BUSINESS_LOGIC,
						"system.eai.exception",
						new Object[] { errMsg },
						ex);
				}
			}
		}
		//*************
		// END: JCO function exection listerner

		//************
		// BEGIN: JCO connection listerener
		if (listener != null)
			listener.connectionEvent(
				new JCoConnectionEvent(
					this,
					func,
					JCoConnectionEvent.BEFORE_JCO_FUNCTION_CALL,
					context));
		//************
		// END: JCO connection listerener

		//************
		// BEGIN: Function module trace
		// check if function has to be traced before execution
		if (mFuncTraceBeforeExec.contains(fmName)) {
			log.debug("Function module trace BEFORE exection");
			JCoUtil.logCall(
				fmName,
				func.getImportParameterList(),
				func.getExportParameterList(),
				func.getTableParameterList(),
				log,
				JCoUtil.LOG_IN,
				(Set)mFuncTraceBeforeExecExcludeImport.get(fmName),
				(Set)mFuncTraceBeforeExecExcludeExport.get(fmName),
				(Set)mFuncTraceBeforeExecExcludeTable.get(fmName));
		}
		//************
		// END: Function module trace

		if (cacheType == null || cacheType.length() == 0) {
			// NO CACHE
			client.execute(func);

		} else {
			// CACHED FUNCTION EXECUTION
			// execute cached function
			StringBuffer key =
				new StringBuffer(client.getAttributes().getClient());
			key.append(client.getAttributes().getLanguage());
			// when the cache type is B than use als the client name in the key
			if (cacheType.equals(CACHE_TYPE_B))
				key.append(client.getAttributes().getUser());

			key.append(getJCoFunctionKey(func));

			Cache.Access cacheAccess = null;
			try {
				cacheAccess = Cache.getAccess(CACHE_REGION_NAME);
			} catch (Cache.Exception ex) {
				// region not available -> standard execute
				client.execute(func);
				return;
			}
			if (log.isDebugEnabled())
				log.debug(
					"Serving Function Module \""
						+ func.getName()
						+ "\" from cache");
			cacheAccess.get(
				key.toString(),
				new JCoFunctionLoader.JCoFunctionAttr(client, func));

			JCO.Function cachedFunc =
				(JCO.Function) cacheAccess.get(
					key.toString(),
					new JCoFunctionLoader.JCoFunctionAttr(client, func));

			JCO.ParameterList expParam = cachedFunc.getExportParameterList();
			JCO.ParameterList tabParam = cachedFunc.getTableParameterList();
			if (expParam != null)
				func.setExportParameterList(
					(JCO.ParameterList) expParam.clone());

			if (tabParam != null)
				func.setTableParameterList(
					(JCO.ParameterList) tabParam.clone());
		}

		//************
		// BEGIN: Function module trace
		// check if function has to be traced after execution
		if (mFuncTraceAfterExec.contains(fmName)) {
			log.debug("Function module trace AFTER exection");
			JCoUtil.logCall(
				fmName,
				func.getImportParameterList(),
				func.getExportParameterList(),
				func.getTableParameterList(),
				log,
				JCoUtil.LOG_OUT,
				(Set)mFuncTraceAfterExecExcludeImport.get(fmName),
				(Set)mFuncTraceAfterExecExcludeExport.get(fmName),
				(Set)mFuncTraceAfterExecExcludeTable.get(fmName));
		}
		//************
		// END: Function module trace

		//************
		// BEGIN: JCO function exection listerner
		if (listener != null)
			listener.connectionEvent(
				new JCoConnectionEvent(
					this,
					func,
					JCoConnectionEvent.AFTER_JCO_FUNCTION_CALL,
					context));
		//*************
		// END: JCO function exection listerner

		execTime = System.currentTimeMillis() - startTime;

		if (mIsActivityFMEnabled) {
			FMActivity activity = (FMActivity) mActivityFMs.get(fmName);
			if (activity != null)
				activity.setExecTime(execTime);
		}

		if (log.isDebugEnabled())
			log.debug("END executeJCoFunction");

	}

	/**
	 * This method returns a key for a given JCo.Function.
	 * The key is constructed using the import parameters
	 * @param func JCo function for which a key has to be created
	 * @return the JCo key. If the key is <code>null</code> then
	 *          no key could be created for the given function
	 */
	private String getJCoFunctionKey(JCO.Function func) {

		StringBuffer key = new StringBuffer(func.getName());

		JCO.ParameterList impParamList = func.getImportParameterList();

		if (impParamList == null)
			return key.toString();

		int numParams = impParamList.getFieldCount();
		StringBuffer sb = new StringBuffer(func.getName());

		for (int i = 0; i < numParams; i++) {
			if (impParamList.isStructure(i))
				sb.append(getKey(impParamList.getStructure(i)));
			else
				sb.append(impParamList.getString(i));
		}

		return sb.toString();
	}

	/**
	 * Recursive function which creates a String representation of a (nested)
	 * JCo structure
	 * @param struct JCO.Structure object
	 * @return a String representation of the structure
	 */
	private String getKey(JCO.Structure struct) {

		StringBuffer sb = new StringBuffer();

		int numParams = struct.getFieldCount();

		for (int i = 0; i < numParams; i++) {

			// in case of nested structure
			if (struct.isStructure(i))
				sb.append(getKey(struct.getStructure(i)));
			else
				sb.append(struct.getString(i));
		}
		return sb.toString();
	}

	/**
	 * Checks if connection parameters of connection definition contain
	 * password, and if yes decripts it
	 * @param conProps connection properties
	 */
	public static void decryptPassword(Properties conProps) {
		Enumeration enum = conProps.keys();

		while (enum.hasMoreElements()) {
			String key = (String) enum.nextElement();
			if (key.equals(JCO_PASSWD)) {
				String encryptedPasseword = conProps.getProperty(key);
				if (log.isDebugEnabled())
					log.debug("decrypting JCo password");
				String decryptedPassword = "";
				try {
					decryptedPassword = decryptPassword(encryptedPasseword);
						
					conProps.setProperty(key, decryptedPassword);
					if (log.isDebugEnabled())
						log.debug(
							"decryption of password successful");

				} catch (Exception ex) {
					StringBuffer sb =
						new StringBuffer(
							"decryption of password failed!!!");
					sb.append(
						"Please make sure that the password used during deployment was created using XCM Administrator");
					sb.append(
						"Continuing using passad value as password");
					log.warn(
						"system.eai.exception",
						new Object[] { sb.toString()},
					ex);
				}
			}
		}
	}

	/**
	 * This method checks if a set of connection properties already contains
	 * the 'jco.client.codepage' property. If this is not the case that,
	 * this property is set depending on the logon-language.
	 * @param conProps connection properties
	 */
	private void assignCodepageToConProps(Properties conProps) {
		if (conProps != null) {
			String codepage = conProps.getProperty(JCO_CODEPAGE);

			if (codepage != null)
				// codepage already available, leave it as it is
				return;

			String language = conProps.getProperty(JCO_LANG);
			if (language != null) {
				codepage =
					CodePageUtils.getSapCpForSapLanguage(
						language.toUpperCase());
				if (codepage == null) {
					log.debug(
						"Error assigning codepage to language. There is no suitable SAP codepage for language '"
							+ language
							+ "'");
					return;
				}
				conProps.setProperty(JCO_CODEPAGE, codepage);
			}
		}
	}

	/**
	 * This method checks if the connection has some Jco specific parameters.
	 * @param props The properties.
	 * @param jcoCon An instance of a JCoConnection.
	 *
	 */
	private void assignParams(Properties props, JCoConnection jcoCon) {
		String isAbapDebug = props.getProperty(JCO_ABAP_DEBUG);
		// set debug flag
		if (isAbapDebug != null && isAbapDebug.equalsIgnoreCase("true"))
			jcoCon.setAbapDebug(true);
		// set function to debug
		String debugFunc = props.getProperty(JCO_ABAP_DEBUG_FUNC);
		Set debugFuncNames = new HashSet();
		if (debugFunc != null && debugFunc.length() > 0) {
			String token = null;
			StringTokenizer tokenizer = new StringTokenizer(debugFunc, ",;:");
			while (tokenizer.hasMoreElements()) {
				token = tokenizer.nextToken().trim();
				if (log.isDebugEnabled())
					log.debug(
						"Adding SAP function module \""
							+ token
							+ "\"to ABAP debug");
				debugFuncNames.add(token);
			}
			jcoCon.setAbapDebugFunctionNames(debugFuncNames);
		}
	}

	/**
	 * Returns true if there is a JCo repository which can provide metadata
	 * @param id repository id
	 * @return true if repository exists
	 */
	public boolean isRepository(String id) {
		IRepository rep = (IRepository) mJCoRepositories.get(id);
		if (rep == null)
			return false;
		else
			return true;
	}

	/**
	 * Disconnect or releases Client if it is part of a connection pool
	 * @param client a JCo client
	 */
	private void releaseClient(JCO.Client client, boolean isPooled) {
		// ask jco client if pooled
		if (log.isDebugEnabled())
			log.debug(
				"Releaseing client '"
					+ client
					+ " with isPooled '"
					+ isPooled
					+ "'");

		if (client != null && client.isAlive()) {
			if (mIsPooledSupported)
				isPooled = JCoUtil.isPooled(client);
				  	
			synchronized (client) {
				if (isPooled) {
					if (log.isDebugEnabled())
						log.debug(
							"releasing JCo client; [client]='" + client + "'");
					JCO.releaseClient(client);
				} else {
					if (log.isDebugEnabled())
						log.debug(
							"disconnecting JCo client; [client]='"
								+ client
								+ "'");
					client.disconnect();
				}
			}
			client = null;
		}
	}

	private boolean isPooled(Properties conProps) {

		String maxConProp = conProps.getProperty(JCO_MAXCON);
		if (maxConProp != null && maxConProp.length() == 0)
			return false;
		try {
			if (maxConProp != null && Integer.parseInt(maxConProp) > 0)
				return true;
			else
				return false;
		} catch (NumberFormatException nfex) {
			String errMsg =
				"value of [maxcon]='"
					+ maxConProp
					+ "' specifying SAP Java Connecotor connection pool size invalid.";
			log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.eai.exception", new Object[] { errMsg }, nfex);
			return false;
		}
	}

	public static class FMActivity {

		String mName = null;
		long mExectuionTime = 0;

		public FMActivity(String name) {
			mName = name;
		}

		    void setExecTime(long execTime) {
			mExectuionTime = execTime;
		}

		public long getExecTime() {
			return mExectuionTime;
		}

		public String getFMName() {
			return mName;
		}


	}

	/**
	 * Sets names of function modules for which activity monitoring should
	 * be performed
	 * @param fms names of function modules
	 */
	public static void setFMActivtiyMonitoring(Set fms) {
		mIsActivityFMEnabled = false;
		if (fms == null || fms.size() == 0)
			return;

		mActivityFMs = new HashMap();
		for (Iterator iter = fms.iterator(); iter.hasNext();) {
			String fmName = (String) iter.next();
			mActivityFMs.put(fmName.trim(), new FMActivity(fmName.trim()));
			log.debug(
				"Adding function module for CCMS activity monitoring: [name]='"
					+ fmName
					+ "'");
		}
		mIsActivityFMEnabled = true;
	}

	/**
	 * Gets activity data for function modules
	 * @return activity data for function modules
		*/
	public static Map getFMActivityMonitoring() {
		return mActivityFMs;
	}

	/**
	 * Returns the lanst execution time for the given function module name
	 * @param activityObjectName name of function module
	 * @return exection time in milliseconds
     *	@see	 com.sap.isa.core.jmx.
     *ActivitySupplier#getActivityData(String)
	 */
	public static int getActivityData(String activityObjectName) {
		FMActivity activity = (FMActivity) mActivityFMs.get(activityObjectName);
		if (activity == null)
			return 0;
		else
			return (int)activity.getExecTime();
	}

	/**
	 * Parses the configuration data defining which parts of a function module
	 * should be traced
	 * @param config configuration string
	 * @param isBefore defines if this is a 'before exectution' trace or 'after
	 * execution' trace
 	 *
	 */
	private void parseFunctionModuleTraceConfig(String configString, boolean isBefore) {

		StringTokenizer tokenizerFM = new StringTokenizer(configString, ",;:");
		String tokenFM = null;
		while (tokenizerFM.hasMoreElements()) {
			tokenFM = tokenizerFM.nextToken().trim();

			log.debug("Found function module for tracing [name]='" + tokenFM + "'");
			StringTokenizer tokenizer = new StringTokenizer(tokenFM, "([]) ");
			Map currentMap = null;
			String currentFM = null;
			Set params = new HashSet();
			while(tokenizer.hasMoreElements()) {
				String token = tokenizer.nextToken().trim();
				log.debug("Parsing [token]='" + token + "'");
				if (token.equalsIgnoreCase(JCO_FUNCTION_TRACE_EXCLUDE_EXPORT)) {
					if (isBefore)
						currentMap = mFuncTraceBeforeExecExcludeExport;
					else
						currentMap = mFuncTraceAfterExecExcludeExport;
					continue;
				}
				if (token.equalsIgnoreCase(JCO_FUNCTION_TRACE_EXCLUDE_IMPORT)) {
					if (isBefore)
						currentMap = mFuncTraceBeforeExecExcludeImport;
					else
						currentMap = mFuncTraceAfterExecExcludeImport;
					continue;
				}
				if (token.equalsIgnoreCase(JCO_FUNCTION_TRACE_EXCLUDE_TABLE)) {
					if (isBefore)
						currentMap = mFuncTraceBeforeExecExcludeTable;
					else
						currentMap = mFuncTraceAfterExecExcludeTable;
					continue;
				}
				if (currentMap == null && isBefore) {
					mFuncTraceBeforeExec.add(token);
					currentFM = token;
					continue;
				}

				if (currentMap == null && !isBefore) {
					mFuncTraceAfterExec.add(token);
					currentFM = token;
					continue;
				}

				currentMap.put(currentFM, params);
				params.add(token);
			}
		}
	}

	/**
	 * Helper method for testing. Calls method initProperties()
	 *
	 */
	public void testInitConfig(Properties props) {
		conFacProps = props;
		initProperties();
	}

	private static String decryptPassword(String password) throws BackendException  {
		if (PasswordEncrypt.isSecureStorage(password)) {
			String pw = SecureStore.retrieve(password); 
			if (password.equals(SecureStore.ERROR_RETRIEVING_SECURE_STORE))
				throw new BackendException("Error retrieving password from secure storage");
			return pw;
		}
			
		else
			return PasswordEncrypt.decryptStringAsHex(password);		
	}
	
	/**
	 * Returns the connection properties for the given connection definition name
	 * @param conDefName
	 * @return connection properties
	 */
	public Properties getConProps(String conDefName) {
		ConnectionDefinition conDef =
			(ConnectionDefinition) conDefs.get(conDefName);
		return conDef.getProperties();
	}
}
