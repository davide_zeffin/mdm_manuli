/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      01 October 2001

  $Revision: $
  $Date: $
*****************************************************************************/
package com.sap.isa.core.eai.sp.jco;

// java imports
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import com.sap.mw.jco.JCO;

/**
 * This class provied some functionality used by various part
 * of the JCo service provider
 */
public class JCoSupport {

  public JCoSupport() {
  }

  private HashSet mGlobalJCoConnectionListener = new HashSet();
  private HashMap mSpecificJCoConnectionListener = new HashMap();

  /**
   * This method is used to add JCo-Connection listener to this backend object.
   * A JCo listener will be notified before a JCo call is being executed
   * and after the JCo call has been exected.
   * @param listener a listener for JCo calls
   */
  public void addJCoConnectionEventListener(JCoConnectionEventListener listener) {
    mGlobalJCoConnectionListener.add(listener);
  }

  /**
   * This method is used to add JCo-Connection listener to this backend object.
   * A JCo listener will be notified before a JCo call is being executed
   * and after the JCo call has been exected.
   * @param listener         a listener for JCo calls
   * @param aJCoFunctionName this parameter specifies the name of a JCo function
   *                         module. The listener will be notified whenever
   *                         this module is called by this business object
   */
  public void addJCoConnectionEventListener(JCoConnectionEventListener listener, String aJCoFunctionName) {
    Set listenerSet = (Set)mSpecificJCoConnectionListener.get(aJCoFunctionName);

    if (listenerSet == null) {
      listenerSet = new HashSet();
      listenerSet.add(listener);
      mSpecificJCoConnectionListener.put(aJCoFunctionName, listenerSet);
    } else {
      listenerSet.add(listener);
    }
  }

  /**
   * This method notifies the registered listeners before a JCo Function is
   * executed
   * @param func JCo function before it has been called
   */
  public void fireBeforeJCoCallEvent(JCO.Function func) {
    if (func == null)
      return;
    int numListener = mGlobalJCoConnectionListener.size();
    Iterator iterator = null;
    Set listenerSet = null;
    if (numListener > 0) {
      iterator = mGlobalJCoConnectionListener.iterator();
      // notify listener
      while (iterator.hasNext());
       // ((JCoConnectionEventListener)iterator.next()).beforeJCoCall(func);
    }

    if ((listenerSet = (Set)mSpecificJCoConnectionListener.get(func.getName())) != null) {
      iterator = listenerSet.iterator();
        // notify listener
        while (iterator.hasNext());
        //  ((JCoConnectionEventListener)iterator.next()).beforeJCoCall(func);
    }
  }  /**
   * This method notifies the registered listeners after a JCo Function has been
   * executed
   * @param func JCo function before it has been called
   */
  public void fireAfterJCoCallEvent(JCO.Function func) {
    if (func == null)
      return;
    int numListener = mGlobalJCoConnectionListener.size();
    Iterator iterator = null;
    Set listenerSet = null;
    if (numListener > 0) {
      iterator = mGlobalJCoConnectionListener.iterator();
      // notify listener
      while (iterator.hasNext());
       // ((JCoConnectionEventListener)iterator.next()).afterJCoCall(func);
    }

    if ((listenerSet = (Set)mSpecificJCoConnectionListener.get(func.getName())) != null) {
      iterator = listenerSet.iterator();
        // notify listener
        while (iterator.hasNext());
        //  ((JCoConnectionEventListener)iterator.next()).afterJCoCall(func);
    }
  }
}