/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Thorsten Kampp
  Created:      15 February 2001
*****************************************************************************/

package com.sap.isa.core.eai.sp.jco;

import java.util.Properties;

import com.sap.mw.jco.JCO;
import com.sap.isa.core.init.Initializable;
import com.sap.isa.core.init.InitializationEnvironment;
import com.sap.isa.core.init.InitializeException;
import com.sap.isa.core.logging.IsaLocation;

/**
 * Class that allows the logging of the JCo Trace. You may add this class
 * to the init-config.xml configuration file in order to become active.<br>
 *
 * The required parameter is "tracelevel" that is being propagated to
 * JCO.setTraceLevel, which requires a numeric value from 0 (no tracing) to
 * 9 (all tracing).<br>
 *
 * Be carefull with the setting, because even on level 3 you can get huge
 * log files real quick.<br>
 *
 * The trace events will be propagated to the attached logging facility via
 * the IsaLocation class under the location name "com.sap.mw.jco". Thus you
 * might need to configure the logging facility appropriate.
 */
public class JCoTraceConfig implements Initializable {

    private static IsaLocation log =
                   IsaLocation.getInstance(JCoTraceConfig.class.getName());

    private JCO.TraceListener tlc                = null;
    private int               previousTraceLevel = 0;

    /**
     * Initialize the implementing component.
     *
     * @param env the calling environment
     * @param props   component specific key/value pairs
     * @exception InitializeException at initialization error
     */
    public void initialize(InitializationEnvironment env, Properties props)
                 throws InitializeException {

         previousTraceLevel = JCO.getTraceLevel();

         int traceLevel = 0;
         {
           String tmp = props.getProperty("tracelevel");
           try {
             traceLevel = Integer.parseInt(tmp);
           } catch(Exception e) {
             log.debug("Could not parse property 'tracelevel': " + tmp, e);
           }
         }

         tlc = new IsaTraceListener();

         JCO.addTraceListener(tlc);
         JCO.setTraceLevel(traceLevel);

         if(log.isDebugEnabled())
            log.debug("JCo Trace set to tracelevel " + traceLevel);
    }

    /**
     * Terminate the component.
     */
    public void terminate() {

         JCO.setTraceLevel(previousTraceLevel);
         if(tlc != null) {
           JCO.removeTraceListener(tlc);
         }
         tlc = null;

         if(log.isDebugEnabled())
            log.debug("JCo Trace terminated");

    }

    /**
     * Nested class that implements a bridge from JCo to our logging facility.
     */
    class IsaTraceListener implements JCO.TraceListener {

        private IsaLocation log =
                IsaLocation.getInstance("com.sap.mw.jco");

        /**
         * Called when a trace message is
         * @param trace_level the trace level
         * @param message the trace message
         */
        public void trace(int trace_level,java.lang.String message) {
            if(log.isDebugEnabled()) {
              StringBuffer tmp = new StringBuffer();
              tmp.append("[");
              tmp.append(trace_level);
              tmp.append("] ");
              tmp.append(message);
              log.debug(tmp.toString());
            }
        }
   }
}