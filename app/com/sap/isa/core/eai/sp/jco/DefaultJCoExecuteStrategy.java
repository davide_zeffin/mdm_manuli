package com.sap.isa.core.eai.sp.jco;

/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      01 August 2001

  $Revision: #1 $
  $Date: 2001/08/03 $
*****************************************************************************/

// isa imports
import com.sap.mw.jco.JCO;
import com.sap.isa.core.logging.IsaLocation;

public class DefaultJCoExecuteStrategy implements JCoExecuteStrategy {

  protected static IsaLocation log = IsaLocation.
        getInstance("monitoring.jco.function");

  private JCoFunctionWriter jcoWriter = new JCoFunctionWriter();

  public DefaultJCoExecuteStrategy() {
  }

  /**
   * This method is called before JCo Function is executed.
   * @param function JCo function
   * @param session a string identifying a session
   */
  public void preExecute(JCO.Function function, String session) {
    try {
      log.debug(jcoWriter.
          emitFunction(function, session, JCoFunctionWriter.FUNC_STATE_PRE_EXEC));
    } catch (Exception ex) {
      System.out.println(ex.toString());
      ex.printStackTrace();
    }
  }

  /**
   * This method is called after a JCo Function has been executed.
   * @param function JCo function
   * @param session a string identifying a session
   */
  public void postExecute(JCO.Function function, String session) {
    try {
      log.debug(jcoWriter.
          emitFunction(function, session, JCoFunctionWriter.FUNC_STATE_POST_EXEC));
    } catch (Exception ex) {
      System.out.println(ex.toString());
      ex.printStackTrace();
    }
  }

}