/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      14 May 2001
  $Id: //java/sapm/esales/30/src/com/sap/isa/core/eai/sp/jco/JCoFunctionWriter.java#1 $
  $Revision: #1 $
  $Change: 26908 $
  $DateTime: 2001/08/03 14:31:36 $
  $Author: d025909 $
*****************************************************************************/
package com.sap.isa.core.eai.sp.jco;

// java import
import java.util.Map;
import java.util.Properties;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.XMLHelper;
import com.sap.mw.jco.JCO;

/**
 * This class is used to modify the eai config file.
 */
public class JCoFunctionWriter {

    public static final String FUNC_STATE_PRE_EXEC  = "pre-execute";
    public static final String FUNC_STATE_POST_EXEC = "post-execute";


    protected static IsaLocation log = IsaLocation.
        getInstance(JCoFunctionWriter.class.getName());


    private XMLHelper xmlHelper = new XMLHelper();

    public JCoFunctionWriter() {
      JCoFunctionMetaDataProvider eaiMdProv
          = new JCoFunctionMetaDataProvider();

      xmlHelper.setMetaDataProvider(eaiMdProv);
    }


  /**
   * This class contains meta data needed to generate XML files which represent
   * the EAI configuration file
   */
  private class JCoFunctionMetaDataProvider implements XMLHelper.MetaDataProvider
  {
    protected Properties values = new Properties();
    protected final String nameSpace = "";

    /** Creates new BOGeneratorMetaData */
    public JCoFunctionMetaDataProvider() {

      // common tag names
      values.put("tFunction",                   "function");
      values.put("tFunction.aSession",          "session");
      values.put("tFunction.aState",            "state");

      values.put("tImportParams",               "import-params");
      values.put("tExportParams",               "export-params");
      values.put("tTableParams",                "table-params");
      values.put("tRow",                        "row");


      values.put("tSimpleParam",                "simple-param");
      values.put("tComplexParam",               "complex-param");
      values.put("tTableParam",                 "table-param");

      values.put("tSimpleField",                "simple-Field");
      values.put("tComplexField",               "complex-Field");

      values.put("aIndex",                      "index");
      values.put("aName",                       "name");
      values.put("aValue",                      "value");
    }

    /**
     * This method returns the name of a tag which corresponds to the given key
     * @param key key of a specific tag
     * @return name of tag or ""
     */
    public String getValue(String key)
    {
      if (key != null) {
        String tagName = (String)values.get(key);
        return tagName == null ? "" : tagName;
      }
      else
        return "";
    }
    /**
     * Returns namespace of this meta data provider
     * @return namespace
     */
    public String getNamespace()
    {
      return nameSpace;
    }
  }
  /**
   * Create a string representation of EAI configuration
   * @param factories
   * @param backendObjects
   * @return string representation of EAI configuration
   */
  public String toXML(Map factories, Map backendObjects) {
    StringBuffer sb = new StringBuffer();
    try {
      sb.append(xmlHelper.createTagWithKey("tBackend", false));
      // append factories description
//      sb.append(emitFactories(factories.values()));
    } catch (XMLHelper.XMLHelperException ex) {
      log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.exception", new Object[] { ex.toString() }, ex);
    }
    return sb.toString();
  }


  /**
   * Create a XML string representation of a complex field
   * @param aParam complex parameter
   * @return string representation of a complex parameter
   */
  private String emitComplexParam(JCO.Structure struct)
      throws XMLHelper.XMLHelperException {

    if (struct == null)
      return "";

    StringBuffer sb = new StringBuffer();
    sb.append(xmlHelper.createTagWithKey("tComplexParam", "aName", struct.getName(), false));

    int numFields = struct.getFieldCount();
    for (int i = 0; i < numFields; i++) {
      if (struct.isStructure(i))
        sb.append(emitComplexField(struct.getStructure(i)));
      else
        sb.append(emitSimpleField(struct.getName(i), struct.getString(i)));
    }
    sb.append(xmlHelper.createEndTagWithKey("tComplexParam"));
    return sb.toString();
  }

  /**
   * Create a XML string representation of a complex field
   * @param aStruct complex field
   * @return string representation of a complex field
   */
  private String emitComplexField(JCO.Structure aStruct)
      throws XMLHelper.XMLHelperException {

    if (aStruct == null)
      return "";

    StringBuffer sb = new StringBuffer();

    sb.append(xmlHelper.createTagWithKey("tComplexField", "aName", aStruct.getName(), false));

    int numFields = aStruct.getFieldCount();
    for (int i = 0; i < numFields; i++) {
      if (aStruct.isStructure(i))
        sb.append(emitComplexField(aStruct.getStructure(i)));
      else
        sb.append(emitSimpleField(aStruct.getName(i), aStruct.getString(i)));
    }
    sb.append(xmlHelper.createEndTagWithKey("tComplexField"));

    return sb.toString();
  }



  /**
   * Create a XML string representation of a simple field
   * @param aParamName simple field name
   * @param aParamValue simple field value
   * @return string representation of a simple parameter
   */
  private String emitSimpleField(String aParamName, String aParamValue)
      throws XMLHelper.XMLHelperException {
    StringBuffer sb = new StringBuffer();
    Properties props = new Properties();
    props.setProperty("aName", aParamName);
    props.setProperty("aValue", aParamValue);

    sb.append(xmlHelper.createTagWithKey("tSimpleField", props, true));

    return sb.toString();
  }


  /**
   * Create a XML string representation of a simple parameter
   * @param aParamName simple parameter name
   * @param aParamValue simple parameter value
   * @return string representation of a simple parameter
   */
  private String emitSimpleParam(String aParamName, String aParamValue)
      throws XMLHelper.XMLHelperException {
    StringBuffer sb = new StringBuffer();
    Properties props = new Properties();
    props.setProperty("aName", aParamName);
    props.setProperty("aValue", aParamValue);

    sb.append(xmlHelper.createTagWithKey("tSimpleParam", props, true));

    return sb.toString();
  }

 /**
   * Create a XML string representation of JCO table
   * @param aTable JCO table
   * @return string representation of table
   */
  private String emitTableParam(JCO.Table aTable)
      throws XMLHelper.XMLHelperException {

    if (aTable == null)
      return "";

    if (aTable.getNumRows() == 0)
      return xmlHelper.createTagWithKey("tTableParam", "aName", aTable.getName(), true);

    StringBuffer sb = new StringBuffer();
    sb.append(xmlHelper.createTagWithKey("tTableParam", "aName", aTable.getName(), false));

    int numRows = aTable.getNumRows();
    int numColumns = aTable.getNumColumns();
    for (int i = 0; i < numRows; i++) {
      sb.append(xmlHelper.createTagWithKey("tRow", "aIndex", String.valueOf(i), false));

      for (int j = 0; j < numColumns; j++) {
        if (aTable.isStructure(j))
          emitComplexField(aTable.getStructure(j));
        else
          sb.append(emitSimpleField(aTable.getName(j), aTable.getString(j)));
        aTable.nextRow();
      }
      sb.append(xmlHelper.createEndTagWithKey("tRow"));
    }
    sb.append(xmlHelper.createEndTagWithKey("tTableParam"));

    return sb.toString();
  }



  /**
   * Create a XML string representation of JCO import parameters
   * @param aImpParams JCO import parameters
   * @return string representation of import parameters
   */
  private String emitImportParams(JCO.ParameterList importParams)
      throws XMLHelper.XMLHelperException {

    if ((importParams == null) || (importParams.getFieldCount() == 0)) {
      return xmlHelper.createTagWithKey("tImportParams", true);
    }

    StringBuffer sb = new StringBuffer();

    sb.append(xmlHelper.createTagWithKey("tImportParams", false));

      int numParams = importParams.getFieldCount();
      for (int i = 0; i < numParams; i++) {
        if (importParams.isStructure(i))
          sb.append(emitComplexParam(importParams.getStructure(i)));
        else
          sb.append(emitSimpleParam(
              importParams.getName(i), importParams.getString(i)));
      }
    sb.append(xmlHelper.createEndTagWithKey("tImportParams"));

    return sb.toString();
  }

  /**
   * Create a XML string representation of JCO export parameters
   * @param aExportParams JCO import parameters
   * @return string representation of import parameters
   */
  private String emitExportParams(JCO.ParameterList aExportParams)
      throws XMLHelper.XMLHelperException {

    if ((aExportParams == null) || (aExportParams.getFieldCount() == 0)) {
      return xmlHelper.createTagWithKey("tImportParams", true);
    }

    StringBuffer sb = new StringBuffer();

    sb.append(xmlHelper.createTagWithKey("tExportParams", false));

      int numParams = aExportParams.getFieldCount();
      for (int i = 0; i < numParams; i++) {
        if (aExportParams.isStructure(i))
          sb.append(emitComplexParam(aExportParams.getStructure(i)));
        else
          sb.append(emitSimpleParam(
              aExportParams.getName(i), aExportParams.getString(i)));
      }
    sb.append(xmlHelper.createEndTagWithKey("tExportParams"));

    return sb.toString();
  }


  /**
   * Create a XML string representation of JCO table parameters
   * @param aTableParams JCO table parameters
   * @return string representation of import parameters
   */
  private String emitTableParams(JCO.ParameterList aTableParams)
      throws XMLHelper.XMLHelperException {

    if ((aTableParams == null) || (aTableParams.getFieldCount() == 0)) {
      return xmlHelper.createTagWithKey("tTableParams", true);
    }

    StringBuffer sb = new StringBuffer();

    sb.append(xmlHelper.createTagWithKey("tTableParams", false));

      int numParams = aTableParams.getFieldCount();
      for (int i = 0; i < numParams; i++) {
          sb.append(emitTableParam(aTableParams.getTable(i)));
      }
    sb.append(xmlHelper.createEndTagWithKey("tTableParams"));

    return sb.toString();
  }



  /**
   * Create a XML string representation of JCO Function
   * @param func JCO Function
   * @param session id identifying function (usually session context)
   * @return string representation of JCo Function as XML
   */
  public String emitFunction(JCO.Function func, String session, String state)
      throws XMLHelper.XMLHelperException {

    if (func == null)
      return "";

    StringBuffer sb = new StringBuffer();
    Properties props = new Properties();
    props.setProperty("aName", func.getName());
    props.setProperty("tFunction.aSession", session);
    props.setProperty("tFunction.aState",   state);

    JCO.ParameterList importParams = func.getImportParameterList();
    JCO.ParameterList exportParams = func.getExportParameterList();
    JCO.ParameterList tableParams = func.getTableParameterList();

    sb.append(xmlHelper.createTagWithKey("tFunction", props, false));

    // emit import parameters
    if (importParams == null || importParams.getFieldCount() == 0)
      sb.append(xmlHelper.createTagWithKey("tImportParams", true));
    else
      sb.append(emitImportParams(func.getImportParameterList()));

    // emit export parameters
    if (exportParams == null || exportParams.getFieldCount() == 0)
      sb.append(xmlHelper.createTagWithKey("tExportParams", true));
    else
      sb.append(emitExportParams(func.getExportParameterList()));

    // emit table parameters
    if (tableParams == null || tableParams.getFieldCount() == 0)
      sb.append(xmlHelper.createTagWithKey("tTableParams", true));
    else
      sb.append(emitTableParams(func.getTableParameterList()));

    sb.append(xmlHelper.createEndTagWithKey("tFunction"));

    return sb.toString();
  }
}