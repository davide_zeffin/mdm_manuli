package com.sap.isa.core.eai.sp.jco;

// java imports
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
 
import com.sap.mw.jco.JCO;
import com.sap.util.monitor.jarm.IMonitor;
import com.sap.isa.core.ActionServlet;
import com.sap.isa.core.Constants;
import com.sap.isa.core.SharedConst;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.ConnectionEventListener;
import com.sap.isa.core.eai.ManagedConnectionBase;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.IsaSession;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.sat.jarm.ISASATCheck;
import com.sap.isa.core.util.MiscUtil;
import com.sap.isa.core.util.VersionGet;

/**
 * This is a JCo specific implementation of a managed connection.
 * All backend business object which communicate with an R/3 system use
 * this type of connection to obtain a JCO.Client.
 * A release does not release the connection to the JCo pool
 */
public class JCoConnectionStateful
        extends ManagedConnectionBase
        implements JCoConnection {

  protected static IsaLocation log = IsaLocation.
        getInstance(JCoConnectionStateful.class.getName());

  protected static IsaLocation jcoMonPools = IsaLocation.
        getInstance("monitoring.jco.pools");
  protected static IsaLocation jcoMonPerf = IsaLocation.
        getInstance("monitoring.jco.performance");

  protected static IsaLocation isaRuntimeTracing = IsaLocation.
        getInstance(Constants.TRACING_RUNTIME_CATEGORY);

  private static int LISTSIZE = 1000;
  private static Map mConnections = new HashMap(LISTSIZE);

  private String mSessionID = IsaSession.getThreadSessionId();
  private boolean mIsSATOn = ISASATCheck.isJarmOn(mSessionID);
  private String mClassName = this.getClass().getName(); 
	 
  private Map mData = new HashMap();
  private JCO.Client client;
  private String clientHandle;
  private String repName;
  private boolean isAbapDebug = false;
  private boolean isTrace = false;
  private boolean isInitialized = false;
  

  private Map cachedFunctionNames;
  private Set mListeners;
  private boolean mIsPooled = false;
  private String mJcsId = null;
  private Object mJcsObject = null;
  private String mJCSAppID = null;
  private boolean mJcsInit = false;
  
  public static boolean isJcoMonitoring = false;
  
 	/**
	 * Returns the internal JCO.Client
	 * @return the internal JCO.Client
	 * @deprecated Use execute() instead of retreving the internal JCO.Client
	 */
  public JCO.Client getJCoClient() throws BackendException {
     if (log.isDebugEnabled())
          log.debug("ENTER [objid]='" + this + "' + getJCoClient()");

    if (client == null) {

      if (log.isDebugEnabled())
        log.debug("Requesting a new JCo client for \"" + getConDefNameString(conDefName) + "\"");

      try {

		if (isJcsEnabled()) {
			client = getJCSClient();
		} else
			client = (JCO.Client)managedConnectionFactory.getConnection(conDefName);        	

		if (client !=  null && client.getAbapDebug() != isAbapDebug)
	        client.setAbapDebug(isAbapDebug);

		// TODO revert this change
//    	if (client != null && client.getTrace() != isTrace)
//        	client.setTrace(isTrace);

        // now check if the client has all the mandatory parameters (e.g.
        // user, password and so on to avoid shortdumps on the SAP system side

        if(JCoUtil.isClientConnectable(client)) {
		    long start = System.currentTimeMillis();
			client.connect();
			long end = System.currentTimeMillis();
			long millis = end - start;
			if (log.isDebugEnabled()) {
			    log.debug("getJCoClient(): for '" + getConDefNameString(conDefName)  + "' needs " + millis + " to connect.");
			}
            isInitialized = true;
		    monitoringConnectConnection(client, conKey);
        } else {

          // If it is not possible to connect, we throw a BackendException
		  String msg = "Cannot connect to backend system. " + JCoUtil.isClientConnectableMessage(client) 
		   + JCoUtil.getClientInfo(client);
          releaseClient(client);
          client = null;
          //log.warn("system.eai.exception", new Object[] { msg }, null);
          throw new BackendException(msg);
        }
      } catch (JCO.Exception ex) {
        log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.eai.exception", new Object[] { ex.toString() + " [client info]=" + JCoUtil.getClientInfo(client) }, null);
        releaseClient(client);
        client = null;
        throw new BackendException(ex.toString() + "\n" + JCoUtil.getClientInfo(client));
      }
    }
    if (log.isDebugEnabled())
      log.debug("getJCoClient(): JCO.Client '" + client  + "' [state]='" + JCoUtil.getJCoState(client) + "'");

    if (log.isDebugEnabled())
        log.debug("EXIT getJCoClient()");

    return client;
  }

  /**
   * This method does not release the client to the pool. To release the
   * connection to the JCo pool call the <code>destroy()</code> method
   */
  public void close() {
    if (log.isDebugEnabled())
      log.debug("close(); [jcoclient]='" + client  + "' [state]='" + JCoUtil.getJCoState(client) + "'");
  }

  /**
   * Releases the underlying JCo connection to the JCo pool. This method is
   * called by the framework in order to clean up the connection management
   */
  public void cleanup() {
    if (log.isDebugEnabled())
      log.debug("cleanup(); [jcoclient]='" + client  + "' [state]='" + JCoUtil.getJCoState(client) + "'");
    releaseClient(client);
    client = null;
  }


  /**
   * Destroys the underlying JCo connection
   */
  public void destroy() {
    if (log.isDebugEnabled())
      log.debug("destroy(); [jcoclient]='" + client  + "' [state]='" + JCoUtil.getJCoState(client) + "'");
    cleanup();
  }


  /**
   * Allocates a physical connection to this managed connection
   * @connection physical connection
   */
  public void associateConnection(Object connection) {
     client = (JCO.Client)connection;
  }

  /**
   * Gets a key which unbiguously identifies this connection. This
   * key can be used to get the same connection from the connection factory
   * @return key which unbiguously identifies this connection
   */
  public String getConnectionKey() {
    return conKey;
  }

  /**
   * Gets the name of connection factory used to create the physical
   * connection
   * @return Gets the name of connection factory
   */
  public String getConnectionFactoryName() {
    return conFactoryName;
  }

  /**
   * Gets the name of connection configuration used to create the
   * physical connection
   * @return name of connection configuration used to create the physical connection
   */
  public String getConnectionConfigName() {
    return conConfigName;
  }

  /**
   * Sets the name of the JCO.Repository which serves metadata for the backend
   * system which belongs to this connection. This function is called by the
   * backend managed connection factory.
   * @param repName name of repository
   */
  public void setJCoRepositoryName(String repName) {
    this.repName = repName;
  }

  /**
   * Gets the name of the JCO.Repository which serves metadata for the backend
   * system which belongs to this connection
   * @param repName name of repository
   */
  public String getJCoRepositoryName() {
    return repName;
  }


  /**
   * Returns a JCO.Function object for the given function name
   * @param funcName name of remote callable fuction module on the SAP system
   * @return JCO.Function
   * @throws BackendException when something goes wrong while retrieving meta data   *
   */
  public synchronized JCO.Function getJCoFunction(String funcName) throws BackendException {
    if (log.isDebugEnabled())
      log.debug("ENTER [objid]='" + this + "' + getJCoFunction(String funcName)");

    JCO.Function func = null;
    JCO.Client clientRep = null;
	if (client != null) {
		repName = client .getAttributes().getSystemID() +
				   client .getAttributes().getClient();
	} else {
	    if (repName == null) {
	      clientRep = getInternalJCoClient();
	      try {
	        clientRep.connect();
			monitoringConnectConnection(client, conKey);
	        // create a new repository id (maybe there is a suitable repository)
	        repName = clientRep.getAttributes().getSystemID() +
	                  clientRep.getAttributes().getClient();
	      } catch (JCO.Exception ex) {
	        log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.eai.exception", new Object[] { ex.toString(), JCoUtil.getClientInfo(client) }, ex);
	        throw new BackendException(JCoUtil.getClientInfo(client) + "\n" + ex.toString());
	      } finally {
	        releaseClient(clientRep);
	        clientRep = null;
	      }
	    }
	}

    JCoManagedConnectionFactory aJCo = (JCoManagedConnectionFactory)managedConnectionFactory;

    if (aJCo.isRepository(repName)) {
        if (log.isDebugEnabled())
          log.debug("EXIT getJCoFunction(String funcName): Returning function using existing repository");
        try {
        	return aJCo.getJCoFunction(repName, funcName, true);
        } catch (BackendException bex) {
        	log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.eai.exception", new Object[] { bex.toString() }, bex);
        	throw bex;
        }
    } else {
        // create function dynamically
        JCO.Client tmpClient = getInternalJCoClient();
        func = aJCo.getJCoFunction(tmpClient, funcName, mIsPooled);
	      log.warn("system.eai.jco.noRepository1", new Object[] { funcName, repName }, null);
    	  log.warn("system.eai.jco.noRepository2", new Object[] { funcName, repName }, null);
        if (log.isDebugEnabled())
          log.debug("EXIT getJCoFunction(String funcName): Returning function using dynamic client");
        return func;
     }
  }

  /**
   * This method is used to execute a JCO.Function on behalf of this connection.
   * @param func a JCO.Function which should be executed
   * @throws BackendException thrown if something goes wrong in the EAI layer
   *         or if an JCO.Exception is thrown by the SAP Java Connector.
   * @throws JCO.AbapException runtime exception thrown if the called Function
   *         Module throws an Abap exception
   */
  public synchronized void execute(JCO.Function func) throws BackendException {

      boolean isLog = log.isDebugEnabled();

     if (isLog)
      log.debug("ENTER [objid]='" + this + "' + execute(JCO.Function func)");



      boolean isJcoMonPerf = jcoMonPerf.isDebugEnabled();
      boolean isIsaRuntimeTracing = isaRuntimeTracing.isDebugEnabled();

     if (isLog)
      log.debug("begin executing JCO.Function \"" + func.getName() + "\"");

    if (isJcoMonPerf)
      jcoMonPerf.debug("begin executing JCO.Function \"" + func.getName() + "\"");

    JCO.Client client = getJCoClient();
    if (isLog) {
      log.debug("execute with JCo client \"" + client + "\"");
    }

	String satCompName = null;
    if (isIsaRuntimeTracing || mIsSATOn) {
      JCO.Attributes attr = client.getAttributes();
      StringBuffer sb = new StringBuffer("[jcofuncionexecution]='begin' [funcname]='");
      sb.append(func.getName());
      sb.append("' [ashost]='");
      sb.append(attr.getPartnerHost());
      sb.append("' [sysid]='");
      sb.append(attr.getSystemID());
      sb.append("'");
      isaRuntimeTracing.debug(sb.toString());
	  IMonitor monitor = (IMonitor)IsaSession.getAttribute(SharedConst.SAT_MONITOR);
	  if (monitor != null) {
		  satCompName = VersionGet.getSATRequestPrefix() + func.getName();
		  monitor.startComponent(satCompName);
		  monitor.compAction(satCompName, sb.toString());
	  }
    }

    if (isJcoMonPerf) {
      String[] poolNames = JCO.getClientPoolManager().getPoolNames();
      int numPools = poolNames.length;
      jcoMonPools.debug("pool Name | max pool size | max cons used | num current used cons");
      JCO.Pool pool;
      for (int i = 0; i < numPools; i++) {
        pool = JCO.getClientPoolManager().getPool(poolNames[i]);
        StringBuffer sb = new StringBuffer(poolNames[i]);
        sb.append("|");
        sb.append(pool.getMaxPoolSize());
        sb.append("|");
        sb.append(pool.getMaxUsed());
        sb.append("|");
        sb.append(pool.getNumUsed());
        jcoMonPools.debug(sb.toString());
      }
    }

    long startTime;
    long execTime;

    JCO.Throughput jcoTp = null;
    if (isJcoMonPerf) {
      jcoTp = new JCO.Throughput();
    }

    // fire event
    if (mListeners != null)
      fireConnectionEvents(new JCoConnectionEvent(this, func, JCoConnectionEvent.BEFORE_JCO_FUNCTION_CALL, mBackendContext));

    try {
      String cacheType = (String)cachedFunctionNames.get(func.getName());
      startTime = System.currentTimeMillis();
      // do not cache function
      if (jcoMonPerf.isDebugEnabled()) {
          client.setThroughput(jcoTp);
      }

      synchronized (client) {
          ((JCoManagedConnectionFactory)managedConnectionFactory).
              executeJCoFunction(client, func, cacheType, mBackendContext);
        }

        execTime = System.currentTimeMillis() - startTime;

        if (isJcoMonPerf) {
          jcoMonPerf.debug("end executing JCo Function \"" + func.getName()
              + "\" in \"" + execTime + "\" milliseconds");
          jcoMonPerf.debug(jcoTp.toString());
        }

        execTime = System.currentTimeMillis() - startTime;

        if (isJcoMonPerf) {
            jcoMonPerf.debug("Function \"" + func.getName() +
                  "\" served from cache in \"" + execTime + "\" milliseconds");
        }

      if (isIsaRuntimeTracing || mIsSATOn) {
        JCO.Attributes attr = client.getAttributes();
        StringBuffer sb = new StringBuffer("[jcofunctionexecution]='end' [funcname]='");
        sb.append(func.getName());
        sb.append("' [ashost]='");
        sb.append(attr.getPartnerHost());
        sb.append("' [sysid]='");
        sb.append(attr.getSystemID());
        sb.append("' [exectime]='");
        sb.append(execTime);
        sb.append("'");
        isaRuntimeTracing.debug(sb.toString());
		IMonitor monitor = (IMonitor)IsaSession.getAttribute(SharedConst.SAT_MONITOR);
		if (monitor != null) {
			monitor.endComponent(satCompName);
		}

      }
    } catch (JCO.Exception ex) {
	      // if abap exception then throw it, otherwise throw a BackendException
	      if (ex instanceof JCO.AbapException)
	        throw ex;
	      else {
			log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.eai.exception", new Object[] { ex.toString(), JCoUtil.getClientInfo(client) }, ex);
			throw new BackendException(JCoUtil.getClientInfo(client) + "\n" + ex.toString());
	      }
    }

    // fire event
    if (mListeners != null)
      fireConnectionEvents(new JCoConnectionEvent(this, func, JCoConnectionEvent.AFTER_JCO_FUNCTION_CALL, mBackendContext));


    if (isLog) {
      log.debug("end executing JCo Function \"" + func.getName()
              + "\" in \"" + execTime + "\" milliseconds");
    }
     if (isLog)
      log.debug("EXIT execute(JCO.Function func)");


  }

  /**
   * This method checks if the connection is vaid and can be used to connect
   * the backend system.
   * @return <code>true</code> if the connection can be used to communicate with
   *    the backend system. <code>false</code> if the connection is not valid
   *    and therefoce cannot be used to communicate with the backend system.
   *    You do not need to check if a connection is valid before you use it. If
   *    you try to execute a function module using an invalid connection then
   *    a <code>BackendException</code> is thrown
   */
  public boolean isValid() {

     if (log.isDebugEnabled())
      log.debug("ENTER [objid]='" + this + "' + isValid()");


     if (log.isDebugEnabled())
      log.debug("isValid(); [jcoclient]='" + client  + "' [state]='" + JCoUtil.getJCoState(client) + "'");

    boolean valid = false;
    JCO.Client client = null;
    try {
      client = getJCoClient();
      if (client == null)
      	return false;
      valid = client.isAlive();
    } catch (BackendException ex) {
    	log.debug(ex.getMessage());
        // no exception => connection not valid!
        //log.error("system.eai.exception", new Object[] { ex.toString(), JCoUtil.getClientInfo(client) }, ex);
    }
      finally {
//        close();
    }
     if (log.isDebugEnabled())
      log.debug("EXIT isValid()");

    return valid;
  }

/**
 * Returns if already a real connection to the backend has taken place. <br> 
 *
 * @return Returns the <true> if there was already a connection to the backend system.
 */
public boolean isInitialized() {
	return isInitialized;
}


/**
   * Enables/disables trace. You should use this method before you
   * execute a JCo function. If you use this method on a open connection
   * the connection will be closed and reopened.
   * @param trace if <code>true</code> enables the trace and disables it otherwise.
   */
  public void setTrace(boolean trace) {
     if (log.isDebugEnabled())
      log.debug("setTrace(boolean trace)");

    isTrace = trace;
    if (client != null && client.getTrace() != trace)
      client.setTrace(trace);
  }

  /**
   * Enables/disables Abap debug. You should use this method before you
   * execute a JCo function. If you use this method on a open connection
   * the connection will be closed and reopened.
   * @param debug if <code>true</code> enables the Abap debug and disables it otherwise.
   */
  public void setAbapDebug(boolean debug) {
     if (log.isDebugEnabled())
      log.debug("setAbapDebug(boolean debug)");

    isAbapDebug = debug;
    if (client != null && client.getAbapDebug() != debug)
      client.setAbapDebug(debug);
  }

    private String getConDefNameString(String conDefName) {
      if (conDefName == null)
        return null;

      int i = 0;
      if ((i = conDefName.indexOf(JCoManagedConnectionFactory.PASSWD_DELIMITER)) != -1)
        conDefName = conDefName.substring(0, i) + "?";

      return conDefName;
    }

  /**
   * Sets the name of JCo functions which should be cached instead called in the
   * SAP system
   * @param cachedFunctionNames a map containing function names which should be cached
   */
  public void setCachedFunctionNames(Map cachedFunctionNames) {
    this.cachedFunctionNames = cachedFunctionNames;
  }

  /**
   * Adds a listener to this managed connection.
   * @param aListener A listener which will be notofied if connection events occur
   */
  public void addConnectionEventListener(ConnectionEventListener aListener) {
    if (mListeners == null)
      mListeners = new HashSet();

    mListeners.add(aListener);
  }

  /**
   * fires connection events
   * @param aEvent the event which has to be fired to all listeners
   */
  private void fireConnectionEvents(JCoConnectionEvent aEvent) {
    if (mListeners == null)
      return;

    Iterator iterator = mListeners.iterator();
    Object listener = null;
    while (iterator.hasNext()) {
      listener = iterator.next();
      if (listener instanceof JCoConnectionEventListener)
        ((JCoConnectionEventListener)listener).connectionEvent(aEvent);
    }
  }

  /**
   * Returns the Attributes representation of the RFC_ATTRIBUTES
   * @return Attributes representation of the RFC_ATTRIBUTES or null if underlying 
   *         connection is not existing or not valid
   */
  public JCO.Attributes getAttributes() {
    if (client != null)
      return client.getAttributes();
    else
      return null;
  }

  /**
   * Sets the name of JCo functions which should debugged using Abap Gui.
   * In a stateful connection it is not possible to DEBUG a specific function
   * because debugging can not be turned on/off on a open connection. Therefore
   * specifying function names for debugging on a stateful connection has
   * no effect.
   * @param debugFuncNames a map containing function names which should be debugged
   */
  public void setAbapDebugFunctionNames(Set debugFuncNames) {
    // no implementation
  }

  /**
   * Specifies that a connection is pooled
   * @param ispooled true if is pooled otherwise false
   */
  public void isPooled(boolean isPooled) {
    mIsPooled = isPooled;
  }

  /**
   * Returns true if a connection is pooled
   * @return true if a connection is pooled
   */
  public boolean isPooled() {
    return mIsPooled;
  }

  /**
   * Disconnect or releases Client if it is part of a connection pool
   * @param client a JCo client
   */
  private void releaseClient(JCO.Client client) {

	if (log.isDebugEnabled())
	 log.debug("ENTER [objid]='" + this + "' + releaseClient(JCO.Client client); client '" + client
	   + "' with isPooled '" + mIsPooled 	 
	   + "' [state]='" + JCoUtil.getJCoState(client)
	   + "' [isAlive]= " + (client != null ? String.valueOf(client.isAlive()) : "false") +
	   "' [isValid]= " + (client != null ? String.valueOf(client.isValid()) : "false") + "'");
  	
    if (client != null ) {
		boolean pooled = mIsPooled;
		// ask jco client if pooled
		pooled = client.isPooled();  	

		if (log.isDebugEnabled())
			log.debug("[objid]='" + this + " [isPooled]='" + pooled + "'");
			
		
		if (isJcsEnabled()) {
			destroyJCSClient();
			monitoringCloseConnection(client);
		}
			
		else {
			try {
				synchronized (client) {
				  monitoringCloseConnection(client);
				  if (pooled) {
					 if (log.isDebugEnabled())
					   log.debug("releasing JCo client; [client]='" + client + "'");
					 JCO.releaseClient(client);
				  } else {
					 if (log.isDebugEnabled())
					   log.debug("disconnecting JCo client; [client]='" + client + "'");
					 client.disconnect();
				  }
				}
			} catch (Throwable t) {
				log.error("system.eai.exception", new Object[] { "Error disconnecting JCo client "  + JCoUtil.getClientInfo(client) }, null);	
			}

		}
      client = null;
    }
   if (log.isDebugEnabled())
    log.debug("EXIT releaseClient(JCO.Client client); client '" + client);
  }

  /**
   * Returns a JCo client connection. The JCO.client connection is connected to
   * the SAP system. For each call to this method a new connection is created
   * @return a open JCO.Client connection to the SAP system
   */
  private JCO.Client getInternalJCoClient() throws BackendException {
    JCO.Client client = null;
    if (log.isDebugEnabled())
      log.debug("ENTER [objid]='" + this + "' + getInternalJCoClient():");

    try {
      synchronized (managedConnectionFactory) {
        client = (JCO.Client)managedConnectionFactory.getConnection(conDefName);
      }
        if(JCoUtil.isClientConnectable(client)) {
          client.connect();
        } else {

          // If it is not possible to connect, we throw a BackendException
           String msg = "Cannot connect to backend system. " + JCoUtil.isClientConnectableMessage(client) + ". " +  
            JCoUtil.getClientInfo(client);
          log.warn("system.eai.exception", new Object[] { msg }, null);
          throw new BackendException(msg);
        }
      } catch (JCO.Exception ex) {
		log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.eai.exception", new Object[] { JCoUtil.getClientInfo(client) }, ex);
        if (client != null)
          releaseClient(client);
        throw new BackendException(ex.toString() + "\n" + JCoUtil.getClientInfo(client));
      }
      if (log.isDebugEnabled())
        log.debug("EXIT getInternalJCoClient(): JCO.Client '" + client + "'");

    return client;
  }
   /**
   * Allows to associate arbitrary data with this connection
   * @param name name of data
   * @param value value
   */
  public void setData(Object name, Object value) {
	mData.put(name, value);
  }

  /**
   * Returns user data from this connection
   * @param name name of data
   * @return data
   */
  public Object getData(Object name) {
  	return mData.get(name);
  }

   /**
    * Returns <code>true</code> if the JCo Function is available
    * on the SAP system the connection is connected to.
    * @param name function name
    * @return <code>true</code> if the JCo Function is available,
    *          otherwise <code>false</code>
    */
   public boolean isJCoFunctionAvailable(String name) {
   		try {
   			if (repName == null)
				getJCoFunction(name);
			else {
				JCoManagedConnectionFactory aJCo = (JCoManagedConnectionFactory)managedConnectionFactory;
				if (aJCo.getJCoFunction(repName, name, false) != null)
					return true;
				else return false;
			}
			return true;
   		} catch (BackendException bex) {
			log.debug(bex.getMessage());
   			return false;
   		}
	}
	
	/**
	 * Resets the client, i.e. frees all resources allocated by this 
	 * connection on the client and server side.
	 * Call this method on stateful conenctions only
	 */
	public void reset() {
		try {
			JCO.Client client = getJCoClient();
			if (client != null)
				client.reset();		

		} catch (BackendException bex) {
			log.debug(bex.getMessage());
		}
	}

	/**
	 * Returns a JCO.Client managed by the JCo Connection Sharing component
	 * @return JCO.Client
	 * @throws BackendException if creating client was not possible
	 *
	 */	
	private JCO.Client getJCSClient() throws BackendException {
		String compId = mJCSAppID;
		Properties conProps = 
			((JCoManagedConnectionFactory)managedConnectionFactory).getConProps(conDefName);

		if(JCoUtil.isClientConnectable(conProps)) {			
			try { 
				Class[] paramTypes = new Class[] { Properties.class, String.class, String.class };
				Object[] paramValues = new Object[] {conProps, mJcsId, compId };
				clientHandle = (String)MiscUtil.callMethod(mJcsObject, "getJCoClientHandle", paramTypes, paramValues);
				paramTypes = new Class[] { String.class, String.class };
				paramValues = new Object[] {clientHandle, compId};
				client = (JCO.Client)MiscUtil.callMethod(mJcsObject, "leaseJCoClient", paramTypes, paramValues); 
			} catch(Exception ex) {
				log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.eai.exception", new Object[] { JCoUtil.getClientInfo(client) }, ex);
				if (client != null)
				  releaseClient(client);
				throw new BackendException(ex.toString() + "\n" + JCoUtil.getClientInfo(client));
			}
		}
		return client;
	}
	
	private void destroyJCSClient() {
		String compId = mJCSAppID;
		Class[] paramTypes = new Class[] { String.class, String.class };
		Object[] paramValues = new Object[] {clientHandle, compId };
		clientHandle = (String)MiscUtil.callMethod(mJcsObject, "releaseJCoClient", paramTypes, paramValues);
		client = null;
		clientHandle = null;
	}
	
	/**
	 * Sets the common id used for JCo connection sharing. For internal use only  
	 * @param id
	 */
	public void setJCS(String id) {
		mJcsId = id;
	}

	private boolean isJcsEnabled() {
		if (mJcsId != null && mJcsObject != null)
			return true;
		if (mJcsId != null) {
			initJCS();
			if (mJcsObject != null)
				return true;
		}
		return false;
	}

	private Object initJCS() {
		if (mJcsObject != null)
			return mJcsObject;

		try {
			mJCSAppID = ActionServlet.getTheOnlyInstance().toString();
			Class jcsClass = Class.forName("com.sap.isa.jcs.JCS");
			mJcsObject = jcsClass.newInstance();
			mJcsInit = true;			
		} catch (IllegalAccessException iaex) {
			log.debug(iaex.getMessage());
			System.out.println("Illegal Access exception " + iaex);
		} catch (InstantiationException iex) {
			log.debug(iex.getMessage());
			System.out.println("Instantiation exception " + iex);			 
		} catch (ClassNotFoundException cnfex) {
			log.debug(cnfex.getMessage());
			System.out.println("Class not found " + cnfex);			 
		}
		return mJcsObject;	
	}

	public static class ConnectionMonitoringContainer {
		private String mState;
		private String mSessionId;
		private String mKey;
		
		ConnectionMonitoringContainer(String state, String sessionId, String key) {
			mState = state;
			mSessionId = sessionId;
			mKey = key;
		}
		
		public void setState(String state) {
			mState = state;
		}
		

		public String getState() {
			return mState;
		}
		
		public String getSessionId() {
			return mSessionId;
		}
		public String getKey() {
			return mKey;
		}
		
	}
	
	/**
		* Adds monitoring information about created connections
		*/
	private static void monitoringConnectConnection(JCO.Client client, String conKey) {
		if (client == null)
			return;
		synchronized (mConnections) {
			// Caution!!!
			// This is a usefull monitoring tool but could only be used in Debug mode
			// and has to be switched on/off on jco_mon.jsp.
			// In a production system this will be run into a memory leak.
			if (log.isDebugEnabled() && isJcoMonitoring) {
				mConnections.put(
					client.toString(),
					new ConnectionMonitoringContainer(
						JCoManagedConnectionFactory.CON_STATUS_CONNECTED,
						IsaSession.getThreadSessionId(),
						conKey));
	
			} else {
				mConnections.clear();
			}
		}
	}
	
   /**
	* Adds monitoring information about disconnected connections
	*/
   private static void monitoringCloseConnection(JCO.Client client) {
	if (client == null)
		return;
	synchronized(mConnections) {
		ConnectionMonitoringContainer cc = (ConnectionMonitoringContainer)mConnections.get(client.toString());
		if (cc == null)
			return;
		cc.setState(JCoManagedConnectionFactory.CON_STATUS_CLOSED);
	}
   }
	
   /**
	* Returns monitoring information on connected/closed connections
	*/   
   public static Map getConnectionHistory() {
	return Collections.unmodifiableMap(mConnections);
   }
   
   public static void clearConnectionMonitoring() {
		synchronized(mConnections) {
			mConnections.clear();	
		}
		
   }


/**
 * @return
 */
public static boolean isJcoMonitoring() {
	return isJcoMonitoring;
}

/**
 * @param b
 */
public static void setJcoMonitoring(boolean isJcoMonitorin) {
	isJcoMonitoring = isJcoMonitorin;
}

}
