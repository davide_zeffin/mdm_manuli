package com.sap.isa.core.eai.sp.jco;
 
// java imports
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.sap.isa.core.Constants;
import com.sap.isa.core.SharedConst;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.ConnectionEventListener;
import com.sap.isa.core.eai.ManagedConnectionBase;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.IsaSession;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.sat.jarm.ISASATCheck;
import com.sap.isa.core.util.VersionGet;
import com.sap.mw.jco.JCO;
import com.sap.util.monitor.jarm.IMonitor;

/**
 * This is a JCo specific implementation of a managed connection.
 * The underlying connection is stateless. That means that it exists only
 * during the execution time of a function and is then released to the connection
 * pool (if one exists)
 */
public class JCoConnectionStateless extends ManagedConnectionBase
                                          implements JCoConnection {

  private static boolean mIsPooledSupported = JCoUtil.isPooledMethodSupported();

  protected static IsaLocation log = IsaLocation.
        getInstance(JCoConnectionStateless.class.getName());

  protected static IsaLocation jcoMonPools = IsaLocation.
        getInstance("monitoring.jco.pools");
  protected static IsaLocation jcoMonPerf = IsaLocation.
        getInstance("monitoring.jco.performance");

  protected static IsaLocation isaRuntimeTracing = IsaLocation.
        getInstance(Constants.TRACING_RUNTIME_CATEGORY);

  private JCoSupport mJCoSupport = null;

  private JCO.Client mClient = null;

  private Map mData = new HashMap();
  private boolean isAbapDebug = false;
  private boolean isTrace = false;
  private  String repName;
  private Map cachedFunctionNames;
  private Set mDebugFunctionNames = new HashSet();
  private boolean mIsPooled = false;

  private String mSessionID = IsaSession.getThreadSessionId();
  private boolean mIsSATOn = ISASATCheck.isJarmOn(mSessionID);
  private String mClassName = this.getClass().getName(); 
  private String mJcsId = null;


	/**
	 * Returns the internal JCO.Client
	 * @return the internal JCO.Client
	 * @deprecated Use execute() instead of retreving the internal JCO.Client
	 */
  public synchronized JCO.Client getJCoClient() throws BackendException {
     if (log.isDebugEnabled())
          log.debug("ENTER [objid]='" + this + "' + getJCoClient()");


      if ((mClient != null && mClient.isAlive())) {

         if (log.isDebugEnabled())
            log.debug("EXIT getJCoClient(): serving existing client");
        return mClient;
      }

        mClient = getInternalJCoClient();

        if (log.isDebugEnabled())
          log.debug("getJCoClient(): JCO.Client '" + mClient + "' [state]='" + JCoUtil.getJCoState(mClient) + "'");

         if (log.isDebugEnabled())
            log.debug("EXIT getJCoClient()");

        return mClient;

  }

  /**
   * Returns a JCo client connection. The JCO.client connection is connected to
   * the SAP system. For each call to this method a new connection is created
   * @return a open JCO.Client connection to the SAP system
   */
  private JCO.Client getInternalJCoClient() throws BackendException {
    JCO.Client client = null;
    if (log.isDebugEnabled())
      log.debug("ENTER [objid]='" + this + "' + getInternalJCoClient():");
    try {
      synchronized (managedConnectionFactory) {
        client = (JCO.Client)managedConnectionFactory.getConnection(conDefName);
      }

      // turn on/off debugging for specific functions
      if (mDebugFunctionNames != null && mDebugFunctionNames.size() == 0 && isAbapDebug != client.getAbapDebug() )
        client.setAbapDebug(isAbapDebug);

//		TODO revert this change
//	  if (client != null && client.getTrace() != isTrace)
//        client.setTrace(isTrace);

        // now check if the client has all the mandatory parameters (e.g.
        // user, password and so on to avoid shortdumps on the SAP system side

        if(JCoUtil.isClientConnectable(client)) {
        	long start = System.currentTimeMillis();
            client.connect();
            long end = System.currentTimeMillis();
            long millis = end - start;
			if (log.isDebugEnabled()) {
			    log.debug("getInternalJCoClient(): JCO.Client '" + client  + "' needs " + millis + " to connect.");
			}
        } else {

          // If it is not possible to connect, we throw a BackendException
		  String msg = "Cannot connect to backend system. " + JCoUtil.isClientConnectableMessage(client) + ". " 
		   + JCoUtil.getClientInfo(client);
          log.warn("system.eai.exception", new Object[] { msg }, null);
          throw new BackendException(msg);

        }

      } catch (JCO.Exception ex) {
		log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.eai.exception", new Object[] { JCoUtil.getClientInfo(client) }, ex);
        if (client != null)
          releaseClient(client);
          client = null;
        throw new BackendException(ex.toString() + "\n" + JCoUtil.getClientInfo(client));

      }
      if (log.isDebugEnabled())
        log.debug("EXIT getInternalJCoClient(): JCO.Client '" + client  + "' [state]='" + JCoUtil.getJCoState(client) + "'");
    return client;
  }

  /**
   * Has no implementation (The underlying connection is
   * stateless and has a lifetime of one function execution)
   */
  public synchronized void close() {
     if (log.isDebugEnabled()) 
      log.debug("close(); [jcoclient]='" + mClient  + "' [state]='" + JCoUtil.getJCoState(mClient) + "'");
     
     releaseClient(mClient);
     mClient = null;
    
  }
  /**
   * Cleans up underlying physical connection. This method is
   * called by the framework in order to clean up the connection management
   */
  public void cleanup() {
    if (log.isDebugEnabled())
      log.debug("cleanup(); [jcoclient]='" + mClient  + "' [state]='" + JCoUtil.getJCoState(mClient) + "'");
    close();
  }


  /**
   * Has no implementation (The underlying connection is
   * stateless and has a lifetime of one function execution)
   */
  public void destroy() {
    if (log.isDebugEnabled())
      log.debug("destroy(); [jcoclient]='" + mClient  + "' [state]='" + JCoUtil.getJCoState(mClient) + "'");

    close();
  }

  /**
   * Has no implementation (The underlying connection is
   * stateless and has a lifetime of one function execution)
   * @connection physical connection
   */
  public void associateConnection(Object connection) {
    // client = (JCO.Client)connection;
  }

  /**
   * Gets a key which unbiguously identifies this connection. This
   * key can be used to get the same connection from the connection factory
   * @return key which unbiguously identifies this connection
   */
  public String getConnectionKey() {
    return conKey;
  }

  /**
   * Gets the name of connection factory used to create the physical
   * connection
   * @return Gets the name of connection factory
   */
  public String getConnectionFactoryName() {
    return conFactoryName;
  }

  /**
   * Gets the name of connection configuration used to create the
   * physical connection
   * @return name of connection configuration used to create the physical connection
   */
  public String getConnectionConfigName() {
    return conConfigName;
  }

  /**
   * Sets the name of the JCO.Repository which serves metadata for the backend
   * system which belongs to this connection. This function is called by the
   * backend managed connection factory.
   * @param repName name of repository
   */
  public void setJCoRepositoryName(String repName) {
    this.repName = repName;
  }

  /**
   * Gets the name of the JCO.Repository which serves metadata for the backend
   * system which belongs to this connection
   * @param repName name of repository
   */
  public String getJCoRepositoryName() {
    return repName;
  }


  /**
   * Returns a JCO.Function object for the given function name
   * @param funcName name of remote callable fuction module on the SAP system
   * @return JCO.Function
   * @throws BackendException when something goes wrong while retrieving meta data   *
   */
  public synchronized JCO.Function getJCoFunction(String funcName) throws BackendException {
    if (log.isDebugEnabled())
      log.debug("ENTER [objid]='" + this + "' + getJCoFunction(String funcName)");

    JCO.Function func = null;
    JCO.Client client = null;
    
    try {
      if (repName == null) {
        client = getInternalJCoClient();
        
		long start = System.currentTimeMillis();
		client.connect();
		long end = System.currentTimeMillis();
		long millis = end - start;
		if (log.isDebugEnabled()) {
			log.debug("getInternalJCoClient(): JCO.Client '" + client  + "' needs " + millis + " to connect.");
		}
			
        // create a new repository id (maybe there is a suitable repository)
        repName = client.getAttributes().getSystemID() +
                  client.getAttributes().getClient();
        log.debug("Creating a repository id  \"" + repName + "\" dynamically. Maybe there is a suitable repository available");
      }
    } catch (JCO.Exception ex) {
        log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.eai.exception", new Object[] { ex.toString(), JCoUtil.getClientInfo(client) }, ex);
        throw new BackendException(JCoUtil.getClientInfo(client) + "\n" + ex.toString());
      } finally {
      if (client != null) {
        releaseClient(client);
        client = null;
      }
    }
    JCoManagedConnectionFactory aJCo = (JCoManagedConnectionFactory)managedConnectionFactory;
    if (aJCo.isRepository(repName)) {
        if (log.isDebugEnabled())
          log.debug("EXIT getJCoFunction(String funcName): Returning function using existing repository");
        try {
        	return aJCo.getJCoFunction(repName, funcName, true);
        } catch (BackendException bex) {
        	log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.eai.exception", new Object[] { bex.toString() }, bex);
        	throw bex;
        }

      } else {
          try {
          // create function dynamically
          client = (JCO.Client)getInternalJCoClient();
          if (log.isDebugEnabled())
            log.debug("getJCoFunction(String funcName): Returning function using dynamic client");
          func = aJCo.getJCoFunction(client, funcName, mIsPooled);
	      log.warn("system.eai.jco.noRepository1", new Object[] { funcName, repName }, null);
    	  log.warn("system.eai.jco.noRepository2", new Object[] { funcName, repName }, null);        

          } catch (JCO.Exception ex) {
      log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.eai.exception", new Object[] { ex.toString(), JCoUtil.getClientInfo(client) }, ex);
      throw new BackendException(JCoUtil.getClientInfo(client) + "\n" + ex.toString());
      } finally {
      if (client != null) {
        releaseClient(client);
        client = null;
        }
      }
    }
    if (log.isDebugEnabled())
      log.debug("EXIT getJCoFunction(String funcName)");
    return func;
 }

  /**
   * This method is used to execute a JCO.Function on behalf of this connection.
   * @param func a JCO.Function which should be executed
   * @throws BackendException thrown if something goes wrong in the EAI layer
   *         or if an JCO.Exception is thrown by the SAP Java Connector.
   * @throws JCO.AbapException runtime exception thrown if the called Function
   *         Module throws an Abap exception
   */
  public void execute(JCO.Function func) throws BackendException {

     boolean isLog = log.isDebugEnabled();

     if (isLog)
      log.debug("ENTER [objid]='" + this + "' + execute(JCO.Function func)");

      boolean isJcoMonPerf = jcoMonPerf.isDebugEnabled();
      boolean isIsaRuntimeTracing = isaRuntimeTracing.isDebugEnabled();

     if (log.isDebugEnabled())
      log.debug("begin executing JCO.Function \"" + func.getName() + "\"");

    if (isJcoMonPerf)
      jcoMonPerf.debug("begin executing JCO.Function \"" + func.getName() + "\"");

    JCO.Client client = getInternalJCoClient();
     if (isLog) {
      log.debug("Execute with JCo client \"" + client  + "' [state]='" + JCoUtil.getJCoState(client) + "'");
    }

    // turn on/off debugging for specific functions
    if (mDebugFunctionNames.size() > 0) {
      if (mDebugFunctionNames.contains(func.getName())) {
        client.setAbapDebug(true);
        log.debug("debugging function " + func.getName());
      } else {
        client.setAbapDebug(false);
      }
    }

	String satCompName = null;
	if (isIsaRuntimeTracing || mIsSATOn) {
	
	  JCO.Attributes attr = client.getAttributes();
	  StringBuffer sb = new StringBuffer("[jcofuncionexecution]='begin' [funcname]='");
	  sb.append(func.getName());
	  sb.append("' [ashost]='");
	  sb.append(attr.getPartnerHost());
	  sb.append("' [sysid]='");
	  sb.append(attr.getSystemID());
	  sb.append("'");
	  isaRuntimeTracing.debug(sb.toString());
	  IMonitor monitor = (IMonitor)IsaSession.getAttribute(SharedConst.SAT_MONITOR);
	  if (monitor != null) {
		  satCompName = VersionGet.getSATRequestPrefix() + func.getName();
		  monitor.startComponent(satCompName);
		  monitor.compAction(satCompName, sb.toString());
	  }
	}


    if (isJcoMonPerf) {
      String[] poolNames = JCO.getClientPoolManager().getPoolNames();
      int numPools = poolNames.length;
      jcoMonPools.debug("pool Name | max pool size | max cons used | num current used cons");
      JCO.Pool pool;
      for (int i = 0; i < numPools; i++) {
        pool = JCO.getClientPoolManager().getPool(poolNames[i]);
        StringBuffer sb = new StringBuffer(poolNames[i]);
        sb.append("|");
        sb.append(pool.getMaxPoolSize());
        sb.append("|");
        sb.append(pool.getMaxUsed());
        sb.append("|");
        sb.append(pool.getNumUsed());
        jcoMonPools.debug(sb.toString());
      }
    }

    long startTime;
    long execTime;

    JCO.Throughput jcoTp = null;
    if (isJcoMonPerf) {
      jcoTp = new JCO.Throughput();
    }

    // fire event
    if (mListeners != null)
      fireConnectionEvents(new JCoConnectionEvent(this, func, JCoConnectionEvent.BEFORE_JCO_FUNCTION_CALL, mBackendContext));

    try {
      String cacheType = (String)cachedFunctionNames.get(func.getName());
      startTime = System.currentTimeMillis();
      // do not cache function
      if (jcoMonPerf.isDebugEnabled()) {
          client.setThroughput(jcoTp);
      }

       synchronized(client) {
         ((JCoManagedConnectionFactory)managedConnectionFactory).
             executeJCoFunction(client, func, cacheType, mBackendContext);

        execTime = System.currentTimeMillis() - startTime;

        if (isJcoMonPerf) {
          jcoMonPerf.debug("end executing JCo Function \"" + func.getName() +
                "\" in \"" + execTime + "\" milliseconds");
          jcoMonPerf.debug(jcoTp.toString());
        }

        execTime = System.currentTimeMillis() - startTime;

        if (isJcoMonPerf) {
            jcoMonPerf.debug("Function \"" + func.getName() +
                  "\" served from cache in \"" + execTime + "\" milliseconds");
        }
      }
	  if (isIsaRuntimeTracing || mIsSATOn) {
		JCO.Attributes attr = client.getAttributes();
		StringBuffer sb = new StringBuffer("[jcofunctionexecution]='end' [funcname]='");
		sb.append(func.getName());
		sb.append("' [ashost]='");
		sb.append(attr.getPartnerHost());
		sb.append("' [sysid]='");
		sb.append(attr.getSystemID());
		sb.append("' [exectime]='");
		sb.append(execTime);
		sb.append("'");
		isaRuntimeTracing.debug(sb.toString());
		IMonitor monitor = (IMonitor)IsaSession.getAttribute(SharedConst.SAT_MONITOR);
		if (monitor != null) {
			monitor.endComponent(satCompName);
		}
	  }

    } catch (JCO.Exception ex) {
      // if abap exception then throw it, otherwise throw a BackendException
      if (ex instanceof JCO.AbapException)
        throw ex;
      else {
		log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.eai.exception", new Object[] { ex.toString(), JCoUtil.getClientInfo(client) }, ex);
		throw new BackendException(ex.toString());
      }
    } finally {
      if (client != null) {
        releaseClient(client);
        client = null;
      }

    }

    // fire event
    if (mListeners != null)
      fireConnectionEvents(new JCoConnectionEvent(this, func, JCoConnectionEvent.AFTER_JCO_FUNCTION_CALL, mBackendContext));

    if (isLog) {
      log.debug("end executing JCo Function \"" + func.getName() +
                "\" in \"" + execTime + "\" milliseconds");
    }
     if (isLog)
      log.debug("EXIT execute(JCO.Function func)");
  }

  /**
   * This method checks if the connection is vaid and can be used to connect
   * the backend system.
   * @return <code>true</code> if the connection can be used to communicate with
   *    the backend system. <code>false</code> if the connection is not valid
   *    and therefoce cannot be used to communicate with the backend system.
   *    You do not need to check if a connection is valid before you use it. If
   *    you try to execute a function module using an invalid connection then
   *    a <code>BackendException</code> is thrown
   */
  public boolean isValid() {
     if (log.isDebugEnabled())
      log.debug("ENTER [objid]='" + this + "' + isValid()");

    boolean valid = false;
    JCO.Client client = null;
    try {
      client = getInternalJCoClient();
      valid = client.isAlive();
    } catch (BackendException ex) {
    	log.debug(ex.getMessage());
    }
      finally {
        if (client != null) {
          releaseClient(client);
          client = null;
        }
    }
    if (log.isDebugEnabled())
       log.debug("EXIT isValid()");

    return valid;
  }

  /**
   * Enables/disables trace
   * @param trace if <code>true</code> enables the trace and disables it otherwise.
   */
  public void setTrace(boolean trace) {
    isTrace = trace;
/*    if (client != null)
      client.setTrace(trace); */
  }

  /**
   * Enables/disables Abap debug
   * @param debug if <code>true</code> enables the Abap debug and disables it otherwise.
   */
  public void setAbapDebug(boolean debug) {
    isAbapDebug = debug;
/*    if (client != null)
      client.setAbapDebug(debug); */
  }

    private String getConDefNameString(String conDefName) {
      if (conDefName == null)
        return null;

      int i = 0;
      if ((i = conDefName.indexOf(JCoManagedConnectionFactory.PASSWD_DELIMITER)) != -1)
        conDefName = conDefName.substring(0, i) + "?";

      return conDefName;
    }

  /**
   * Sets the name of JCo functions which should be cached instead called in the
   * SAP system
   * @param cachedFunctionNames a map containing function names which should be cached
   */
  public void setCachedFunctionNames(Map cachedFunctionNames) {
     this.cachedFunctionNames = cachedFunctionNames;
  }

  /**
   * This object contains some data/functionality which is shared
   * by various JCoConnection objects
   * @param JCoSupport aJCoSupport object contains some data/functionality which
   *                   is shared by multiple JCoConnection objects
   */
  public void setJCoSupport(JCoSupport aJCoSupport) {
    mJCoSupport = aJCoSupport;
  }

  /**
   * Adds a listener to this managed connection.
   * @param aListener A listener which will be notofied if connection events occur
   */
  public void addConnectionEventListener(ConnectionEventListener aListener) {
    if (mListeners == null)
      mListeners = new HashSet();

    mListeners.add(aListener);
  }

  /**
   * fires connection events
   * @param aEvent the event which has to be fired to all listeners
   */
  private void fireConnectionEvents(JCoConnectionEvent aEvent) {
    if (mListeners == null)
      return;

    Iterator iterator = mListeners.iterator();
    Object listener = null;
    while (iterator.hasNext()) {
      listener = iterator.next();
      if (listener instanceof JCoConnectionEventListener)
        ((JCoConnectionEventListener)listener).connectionEvent(aEvent);
    }
  }

  /**
   * Returns the Attributes representation of the RFC_ATTRIBUTES
   * @return Attributes representation of the RFC_ATTRIBUTES or null if underlying 
   *         connection is not existing or not valid
   */
  public JCO.Attributes getAttributes() {
    synchronized(this) {
      JCO.Attributes attr = null;
      JCO.Client client;
      try {
        client = getJCoClient();
        attr = client.getAttributes();
      } catch (BackendException ex) {
      	log.debug(ex.getMessage());
      }
        finally {
      }
      return attr;
    }
  }

  /**
   * Sets the name of JCo functions which should debugged using Abap Gui
   * For those functions listed in the map the ABAP debug is turned on
   * when a function is executed
   * Please note: if abap debugging is turned on only those functions will
   * be debugged which are registered using this method. If you want to debug
   * all functions no function should be registered for debugging.
   * @param debugFuncNames a set containing function names which should be debugged
   */
  public void setAbapDebugFunctionNames(Set debugFuncNames) {
    mDebugFunctionNames = debugFuncNames;
  }

  /**
   * Specifies that a connection is pooled
   * @param ispooled true if is pooled otherwise false
   */
  public void isPooled(boolean isPooled) {
    mIsPooled = isPooled;
  }

  /**
   * Returns true if a connection is pooled
   * @return true if a connection is pooled
   */
  public boolean isPooled() {
    return mIsPooled;
  }

  /**
   * Disconnect or releases Client if it is part of a connection pool
   * @param client a JCo client
   */
  private void releaseClient(JCO.Client client) {

	if (log.isDebugEnabled())
	log.debug("ENTER [objid]='" + this + "' + releaseClient(JCO.Client client); client '" + client
	  + "' with isPooled '" + mIsPooled 
	  + "' [state]='" + JCoUtil.getJCoState(client)
	  + "' [isAlive]= " + (client != null ? String.valueOf(client.isAlive()) : "false") +
	  "' [isValid]= " + (client != null ? String.valueOf(client.isValid()) : "false") + "'");

  	
    if (client != null) {
		boolean pooled = mIsPooled;
		// ask jco client if pooled
		if (mIsPooledSupported)
			pooled = JCoUtil.isPooled(client);  	

		if (log.isDebugEnabled())
			log.debug("[objid]='" + this + " [isPooled]='" + pooled + "'");

      synchronized (client) {
      if (pooled) {
         if (log.isDebugEnabled())
           log.debug("releasing JCo client; [client]='" + client + "'");
         JCO.releaseClient(client);
      } else {
         if (log.isDebugEnabled())
           log.debug("disconnecting JCo client; [client]='" + client + "'");
         client.disconnect();
      }
    }
      client = null;
    }
    log.debug("EXIT releaseClient(JCO.Client client); client '" + client);

  }
  
  
   /** 
   * Allows to associate arbitrary data with this connection
   * @param name name of data
   * @param value value
   */
  public void setData(Object name, Object value) {
	mData.put(name, value);  	
  }
  
  /** 
   * Returns user data from this connection
   * @param name name of data
   * @return data
   */
  public Object getData(Object name) {
  	return mData.get(name);  	
  }

   /**
    * Returns <code>true</code> if the JCo Function is available
    * on the SAP system the connection is connected to.
    * @param name function name
    * @return <code>true</code> if the JCo Function is available,
    *          otherwise <code>false</code>
    */
   public boolean isJCoFunctionAvailable(String name) {
   		try {
   			if (repName == null)
				getJCoFunction(name);
			else {
				JCoManagedConnectionFactory aJCo = (JCoManagedConnectionFactory)managedConnectionFactory;
				if (aJCo.getJCoFunction(repName, name, false) != null)
					return true;
				else return false;
			}
   			return true;
   		} catch (BackendException bex) {
   			log.debug(bex.getMessage());
   			return false;
   		}
   	
   }
 
   
   /**
    * does nothing
	*/
   public void reset() {}  

   /**
	* Sets the common id used for JCo connection sharing. For internal use only  
	* @param id
	*/
   public void setJCS(String id) {
	   mJcsId = id;
   }
  
  
}
