/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      01 October 2001
  $Id:  $
  $Revision:  $
  $Change:  $
  $DateTime:  $
  $Author:  $
*****************************************************************************/
package com.sap.isa.core.eai.sp.jco;

// ISA imports
import com.sap.mw.jco.JCO;
import com.sap.isa.core.eai.BackendContext;
import com.sap.isa.core.eai.ConnectionEvent;

/**
 * This object contains information about a JCo Connection event
 */
public class JCoConnectionEvent extends ConnectionEvent {

  /**
   * Id which indicates that the event has been fired just before a
   * JCo Function module has been called
   */
  public static int BEFORE_JCO_FUNCTION_CALL = 1;

  /**
   * Id which indicates that the event has been fired after a
   * JCo Function module has been called
   */
  public static int AFTER_JCO_FUNCTION_CALL = 2;

  private int mId = 0;
  private JCO.Function mJCoFunction;

  /**
   * Contructs a event object
   * @param source the source of the event
   * @param aJCoFunction a JCo Function
   * @param id id indicating what kind of event it is
   * @param context the backend context
   *
   */
  public JCoConnectionEvent(Object event, JCO.Function aJCoFunction,
                            int id, BackendContext context) {
    super(event, context);
    mJCoFunction = aJCoFunction;
    mId = id;
  }

  /**
   * Returns a JCo function if this event is related with a call of a JCo function
   * @return a JCo function if this event is related with a call of a JCo function
   */
  public JCO.Function getJCoFunction() {
    return mJCoFunction;
  }

  /**
   * Returns an id which indicates what kind of event it is
   */
  public int getId() {
    return mId;
  }

}