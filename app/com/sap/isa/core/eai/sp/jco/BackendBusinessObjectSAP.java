package com.sap.isa.core.eai.sp.jco;

// ISA core imports
import java.util.Properties;

import com.sap.isa.core.eai.BackendBusinessObject;
import com.sap.isa.core.eai.BackendException;

/**
 * This interface should be implemented by Backend Business Object
 * wich communicate with an SAP System using SAP´s Java Connector
 */

public interface BackendBusinessObjectSAP
              extends BackendBusinessObject {

  /**
   * Returns a JCo connection to an SAP system. This is a default connection
   * configured externally. This connection can be used to get a JCo.Client
   * connection. After finishing using the JCo.Client connection the connection
   * should be released by calling JCoConnection.close()
   * @return JCo client connection to an SAP system
   */
  public JCoConnection getDefaultJCoConnection();

  /**
   * Returns a connection which is a modified default connection. This is
   * a convenience function which allows to getting modified default connections
   * quickly
   * @param conDefParams connection parameters which overwrite/extend the connection
   *    parameters of the default connection
   * @return a connection
   */
  public JCoConnection getModDefaultJCoConnection(Properties conDefProps)
          throws BackendException;


  /**
   * returns an existing connection for the given key
   * @param conKey key which unambiguously specifies a conneciton
   * @return a connection if existing, otherwise <code>null</code>
   */
  public JCoConnection getJCoConnection(String conKey)
        throws BackendException;

  /**
   * If the connection definition for the default connection is incomplete
   * then it is possible to complete the connection data using this method.
   * After a default connection has been established it is no longer possible
   * to use this function again. (i.e. the connection definition for the
   * default connection cannot be changed again)
   * @param conDefProps additional connection properties to complete an incomplete
   *        connection definition for the default connection
   * @return a connection if existing, otherwise <code>null</code>
   */
  public JCoConnection getDefaultJCoConnection(Properties conDefProps)
        throws BackendException;

  /**
   * This method is used to add JCo-call listener to this backend object.
   * A JCo listener will be notified before a JCo call is being executed
   * and after the JCo call has been exected.
   * @param listener a listener for JCo calls
   */
  public void addJCoConnectionEventListener(JCoConnectionEventListener listener);
}