/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      01 August 2001

  $Revision: #1 $
  $Date: 2001/08/03 $
*****************************************************************************/
package com.sap.isa.core.eai.sp.jco;

// JCo import
import com.sap.mw.jco.JCO;

/**
 * This interface has to be implemented by a class which wants to interfer
 * a JCo Function call. An implementation of such class could be for example
 * used to trace data passed by the function to the SAP System
 */
public interface JCoExecuteStrategy {

  /**
   * This method is called before JCo Function is executed.
   * @param function JCo function
   * @param session a string identifying a session
   */
  public void preExecute(JCO.Function function, String session);

  /**
   * This method is called after a JCo Function has been executed.
   * @param function JCo function
   * @param session a string identifying a session
   */
  public void postExecute(JCO.Function function, String session);

}