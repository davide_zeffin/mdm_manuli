package com.sap.isa.core.eai.sp.jco;

// core imports
import java.util.Map;
import java.util.Set;

import com.sap.mw.jco.JCO;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.Connection;

public interface JCoConnection extends Connection {

  /**
   * Returns a JCo client connection
   * @return a JCo client connection
   */
  public JCO.Client getJCoClient() throws BackendException;

  /**
   * Gets the name of connection factory used to create the physical
   * connection
   * @return Gets the name of connection factory
   */
  public String getConnectionFactoryName();

  /**
   * Gets the name of connection configuration used to create the
   * physical connection
   * @return name of connection configuration used to create the physical connection
   */
  public String getConnectionConfigName();

  /**
   * Returns a JCO.Function object for the given function name
   * @param funcName name of remote callable fuction module on the SAP system
   * @return JCO.Function
   * @throws BackendException when something goes wrong while retrieving meta data
   */
  public JCO.Function getJCoFunction(String funcName) throws BackendException;

  /**
   * Returns a JCo connection which is a modified default connection. This is
   * a convenience function which allows to getting modified default connections
   * quickly
   * @param conParams connection parameters which overwrite/extend the connection
   *    parameters of the default connection
   * @return a JCoConnection
   */

  /**
   * This method is used to execute a JCO.Function on behalf of this connection.
   * @param func a JCO.Function which should be executed
   * @throws BackendException thrown if something goes wrong in the EAI layer
   *         or if an JCO.Exception is thrown by the SAP Java Connector.
   * @throws JCO.AbapException runtime exception thrown if the called Function
   *         Module throws an Abap exception
   */
  public void execute(JCO.Function func) throws BackendException;

  /**
   * Sets the name of the JCO.Repository which serves metadata for the backend
   * system which belongs to this connection
   * @param repName name of repository
   */
  public void setJCoRepositoryName(String repName);



	/**
	 * Sets the common id used for JCo connection sharing. For internal use only  
	 * @param id
	 */
	public void setJCS(String id);

  /**
   * Gets the name of the JCO.Repository which serves metadata for the backend
   * system which belongs to this connection
   * @param repName name of repository
   */
  public String getJCoRepositoryName();

  /**
   * Gets a key which unbiguously identifies this connection. This
   * key can be used to get the same connection from the connection factory
   * @return key which unbiguously identifies this connection
   */
  public String getConnectionKey();

  /**
   * This method checks if the connection is valid and can be used to connect
   * the backend system.
   * @return <code>true</code> if the connection can be used to communicate with
   *    the backend system. <code>false</code> if the connection is not valid
   *    and therefoce cannot be used to communicate with the backend system.
   *    You do not need to check if a connection is valid before you use it. If
   *    you try to execute a function module using an invalid connection then
   *    a <code>BackendException</code> is thrown
   */
  public boolean isValid();

  /**
   * Sets the name of JCo functions which should be cached instead called in the
   * SAP system
   * @param cachedFunctionNames a map containing function names which should be cached
   */
  public void setCachedFunctionNames(Map cachedFunctionNames);

  /**
   * Sets the name of JCo functions which should debugged using Abap Gui
   * Please note that the result of this function is highly depended
   * on the concrete implementation
   * @param debugFuncNames a map containing function names which should be debugged
   */
  public void setAbapDebugFunctionNames(Set debugFuncNames);

  /**
   * Returns the Attributes representation of the RFC_ATTRIBUTES
   * @return Attributes representation of the RFC_ATTRIBUTES or null if underlying 
   *         connection is not existing or not valid
   */
  public JCO.Attributes getAttributes();

  /**
   * Enables/disables Abap debug. You should use this method before you
   * execute a JCo function. If you use this method on a open connection
   * the connection will be closed and reopened.
   * @param debug if <code>true</code> enables the Abap debug and disables it otherwise.
   */
  public void setAbapDebug(boolean debug);

  /**
   * Enables/disables trace
   * @param trace if <code>true</code> enables the trace and disables it otherwise.
   */
  public void setTrace(boolean trace);
 
  /**
   * Specifies that a connection is pooled
   * @param ispooled true if is pooled otherwise false
   */
  public void isPooled(boolean ispoolead);

  /**
   * Returns true if a connection is pooled
   * @return true if a connection is pooled
   */
  public boolean isPooled();
  
  /** 
   * Allows to associate arbitrary data with this connection
   * @param name name of data
   * @param value value
   */
  public void setData(Object name, Object value);
  
  /** 
   * Returns user data from this connection
   * @param name name of data
   * @return data
   */
  public Object getData(Object name);
 
   /**
    * Returns <code>true</code> if the JCo Function is available
    * on the SAP system the connection is connected to.
    * @param name function name
    * @return <code>true</code> if the JCo Function is available,
    *          otherwise <code>false</code>
    */
   public boolean isJCoFunctionAvailable(String name);
   
  /**
   * Resets the client, i.e. frees all resources allocated by this 
   * connection on the client and server side.
   * Call this method on stateful conenctions only
   */
  public void reset();
   
}
