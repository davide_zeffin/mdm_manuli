/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      28 September 2001

  $Revision: #1 $
  $Date: 2001/08/03 $
*****************************************************************************/
package com.sap.isa.core.eai.sp.jco;

// java imports
import com.sap.isa.core.eai.ConnectionEventListener;

/**
 * This interface should be impemented by a class which wants to
 * be notified about JCo Calls to the SAP system
 */
public interface JCoConnectionEventListener extends ConnectionEventListener
{
  /**
   * Method is called when a connection event occurs
   */
  public void connectionEvent(JCoConnectionEvent event);
}