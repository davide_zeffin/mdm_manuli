/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      12 March 2001
*****************************************************************************/
package com.sap.isa.core.eai.sp.jco;

// ISA core imports
import java.util.Properties;

import com.sap.isa.core.eai.BackendBusinessObjectBase;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.core.eai.ConnectionDefinition;
import com.sap.isa.core.eai.ManagedConnectionFactoryConfig;
import com.sap.isa.core.logging.IsaLocation;

/**
 * This interface should be implemented by Backend Business Object
 * wich communicate with an SAP System using SAP´s Java Connector
 */

public class BackendBusinessObjectBaseSAP
          extends BackendBusinessObjectBase
          implements BackendBusinessObjectSAP {

    protected IsaLocation log = IsaLocation.getInstance(BackendBusinessObjectBaseSAP.class.toString());

   /**
    * Returns a JCo connection to an SAP system. This is a default connection
    * configured externally. After finishing using this connection the connection
    * should be released by calling JCoConnection.close()<br />
    * 
    * The methods checks if the connection is valid. 
    * If it is invalid, it will be tried to rebuild the connection if
    * <ul><li>connection is of type JCoConnectionStateless</li>
    * <li>user ID from the old connection is the same as the user from EAI-Config.</li></ul>
    * The rebuild reads language from context and it will be used to complete the connection 
    * (the parameter user and password are taken from an connection called 
    * ISA-COMPLETE which must be definied is EAI-Config.
    * 
    * @return JCo client connection to an SAP system
    */
    public JCoConnection getDefaultJCoConnection() {

        JCoConnection connection = (JCoConnection)conFactory.getDefaultConnection();

        if (!connection.isValid()) {

        	if(connection instanceof JCoConnectionStateful) {
        		String msg = "It was tried to rebuild the connection because of invalidity. ";
        		msg += "It is not allowed because the connection type is not JCoConnectionStateless.";
        		msg += " You may also check the user authorizations.";
        		log.error(msg);
        		throw new NullPointerException(msg);
        	}

            Properties conProps = new Properties();

            String language = (String) context.getAttribute(BackendObjectManager.LANGUAGE);

            if (language != null && language.length() > 0) {
                conProps.setProperty(JCoManagedConnectionFactory.JCO_LANG, language);
            }

            try {

                //---> get JCO Connection Factory Configuration
                ManagedConnectionFactoryConfig conFacConfig =
                    (ManagedConnectionFactoryConfig) getConnectionFactory().getManagedConnectionFactoryConfigs().get(
                        com.sap.isa.core.eai.init.InitEaiISA.FACTORY_NAME_JCO);
                //---> get the connection definition
                ConnectionDefinition cd = null;
                if (connection.getConnectionConfigName().indexOf("Alt") >= 0) {
                	cd = conFacConfig.getConnectionDefinition(com.sap.isa.core.eai.init.InitEaiISA.CONDEF_NAME_ISA_COMPLETE_ALT);
                }
                else {
//                ConnectionDefinition cd =
                    cd = conFacConfig.getConnectionDefinition(com.sap.isa.core.eai.init.InitEaiISA.CONDEF_NAME_ISA_COMPLETE);
                }
                //---> get the user and the password parameters of the connection definition
                String defaultUser = cd.getProperties().getProperty(JCoManagedConnectionFactory.JCO_USER);
                String defaultPassword = cd.getProperties().getProperty(JCoManagedConnectionFactory.JCO_PASSWD);

                // set the user parameter in the connection definition only if it is specified
                if (defaultUser != null && defaultUser.length() > 0) {
                    conProps.setProperty(JCoManagedConnectionFactory.JCO_USER, defaultUser);
                }

                // set the password parameter in the connection definition only if it is specified
                if (defaultPassword != null && defaultPassword.length() > 0) {
                    conProps.setProperty(JCoManagedConnectionFactory.JCO_PASSWD, defaultPassword);
                }
                
                // ---> if old user ID and new user ID isn't equal, don't create new connection
                if(connection.getAttributes() != null 
                		&& connection.getAttributes().getUser() != null
                		&& connection.getAttributes().getUser() != defaultUser) {
                			
                    String msg  = "It was tried to rebuild the connection because of invalidity. ";
                           msg += "It is not allowed because the user ID of the old and new connection is not equal.";
                    log.error(msg);
                    return null;
                }

                connection = getDefaultJCoConnection(conProps);
            }
            catch (BackendException ex) {
                return connection;
            }
        }

        return connection;
    }

   /**
    * Returns a connection which is a modified default connection. This is
    * a convenience function which allows to getting modified default connections
    * quickly
    * @param conDefParams connection parameters which overwrite/extend the connection
    *    parameters of the default connection
    * @return a connection
    */
    public JCoConnection getModDefaultJCoConnection(Properties conDefProps)
            throws BackendException {
              
        JCoConnection defaultConnection = (JCoConnection)conFactory.getDefaultConnection();      
        
        return (JCoConnection) conFactory.getConnection(
                defaultConnection.getConnectionFactoryName(),
                defaultConnection.getConnectionConfigName(),
                conDefProps);
    }

   /**
    * returns an existing connection for the given key
    * @param conKey key which unambiguously specifies a conneciton
    * @return a connection if existing, otherwise <code>null</code>
    */
    public JCoConnection getJCoConnection(String conKey)
            throws BackendException {
    
        return (JCoConnection) conFactory.getConnection(conKey);
    }

   /**
    * If the connection definition for the default connection is incomplete
    * then it is possible to complete the connection data using this method.
    * After a default connection has been established it is no longer possible
    * to use this function again. (i.e. the connection definition for the
    * default connection cannot be changed again)
    * @param conDefProps additional connection properties to complete an incomplete
    *        connection definition for the default connection
    * @return an connection. Check using isValid() whether the physical connection is valid
    */
    public JCoConnection getDefaultJCoConnection(Properties conDefProps)
            throws BackendException {
            
        JCoConnection defaultConnection = (JCoConnection)conFactory.getDefaultConnection();    
            
        return (JCoConnection) conFactory.getDefaultConnection(
                defaultConnection.getConnectionFactoryName(),
                defaultConnection.getConnectionConfigName(),
                conDefProps);
    }

   /**
    * This method is used to add JCo-call listener to this backend object.
    * A JCo listener will be notified before a JCo call is being executed
    * and after the JCo call has been exected.
    * @param listener         a listener for JCo calls
    */
    public void addJCoConnectionEventListener(JCoConnectionEventListener listener) {
        conFactory.addConnectionEventListener(listener);
    }
   /**
    * Removes a JCo-call listener to this backend object.
    * @param listener a listener for JCo calls
    */
    public void removeJCoConnectionEventListener(JCoConnectionEventListener listener) {
        conFactory.removeConnectionEventListener(listener);
    }
}