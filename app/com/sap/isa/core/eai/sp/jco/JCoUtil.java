package com.sap.isa.core.eai.sp.jco;

// java imports
import java.util.Collections;
import java.util.Properties;
import java.util.Set;

import com.sap.mw.jco.JCO;
import com.sap.isa.core.MessageResources;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.init.InitializationHandler;
import com.sap.isa.core.logging.IsaLocation;

/**
 * This class provides some convenience methods for JCo
 */
public class JCoUtil {
	
  private static boolean mIsPooledMethod = false;
  private static IsaLocation log =
                 IsaLocation.getInstance(JCoUtil.class.getName());

  public static final int LOG_IN  = 1;
  public static final int LOG_OUT = 2;
  private static String HIDDEN = "---hidden---";

  // check if JCO.Cleint.isPooled() method is supported 
  static {
	  try {
		  Class client = JCO.Client.class;
		  client.getMethod("isPooled", null);
			mIsPooledMethod = true;
			
	  } catch (Throwable t) {
	  	log.debug(t.getMessage());
		  // do nothing
	  }
  }


  private JCoUtil() {
  }

   /**
   * Creates a string representation of the logon info
   */
  public static String getClientInfo(JCO.Client client) {
    if (client == null)
      return "";

    StringBuffer sb = new StringBuffer("parameters: ");
    sb.append("[client]='" + client.getClient() + "' ");
    sb.append("[user]='" + client.getUser() + "' ");
    sb.append("[language]='" + client.getLanguage() + "' ");
    sb.append("[ashost]='" + client.getASHost() + "' ");
    sb.append("[systemnumber]='" + client.getSystemNumber() + "' ");
    sb.append("[mshost]='" + client.getMSHost() + "' ");
    sb.append("[gwhost]='" + client.getGWHost() + "' ");
    sb.append("[gwserv]='" + client.getGWServ() + "' ");
    sb.append("[group]='" + client.getGroup() + "' ");
    sb.append("[systemid]='" + client.getSystemID() + "' ");
    Properties props = (Properties)client.getProperties().clone();
    if (props.getProperty(JCoManagedConnectionFactory.JCO_PASSWD) != null)
        props.setProperty(JCoManagedConnectionFactory.JCO_PASSWD, "?");

    sb.append("\nProperties: " + props);

    if (client.isAlive()) {
      sb.append("Client connected");
      sb.append(client.getAttributes().toString());
    } else
      sb.append("\nClient not connected");

    return sb.toString();
  }

	/**
     * Method checks whether the client can be used to connect to the SAP
     * system. It checks the required properties that are mandatory
     * and returns either true (you may connect) or false (something is missing).
 	 * @param client JCo client 
	 * @return true if connectiong would be possible otherwise false
	 * @throws BackendException
	 */
	private static boolean isClientConnectableInternal(JCO.Client client) throws BackendException { 
		if (client == null)
			return false;
		Properties props = (Properties)client.getProperties().clone();
		return isClientConnectableInternal(props);
	}

	/**
	 * Method checks whether the properties passed could be used to connect to the SAP
	 * system using JC0
	 *
	 * External Programms (TYPE E) are required to have tphost only. User and
	 * Password are not mandatory.
	 *
	 * @param props properties used to connect to SAP system 
	 * @returns true if connectiong would be possible otherwise false
	 */
	public static boolean isClientConnectable(Properties props)  { 
		try {
			return isClientConnectableInternal(props);
		} catch (BackendException bex) {
			log.debug(bex.getMessage());
			return false;
		}
	}
	

  /**
   * Method checks whether the properties passed could be used to connect to the SAP
   * system using JC0
   *
   * External Programms (TYPE E) are required to have tphost only. User and
   * Password are not mandatory.
   *
   * @param props properties used to connect to SAP system 
   * @returns true if connectiong would be possible otherwise false
   */
  private static boolean isClientConnectableInternal(Properties props) throws BackendException {
	
	String msg = "OK";
    boolean ret = true;
    boolean isR3 = true;
    //StringBuffer debugLog = new StringBuffer();

	if (props.size() == 0)
		return false;

    // check if it is an external RFC server, i.e. IMS
    String type   = props.getProperty(JCoManagedConnectionFactory.JCO_TYPE);
    String tphost = props.getProperty(JCoManagedConnectionFactory.JCO_TPHOST);
    if(type != null &&
       type.equals(JCoManagedConnectionFactory.JCO_TYPE_EXTERNAL) &&
       tphost != null) {
       isR3 = false;
       // debugLog.append("No R3 System ");
    }

	String password = props.getProperty(JCoManagedConnectionFactory.JCO_PASSWD);

    if(isR3 == true) {
      if(password == null) {
      	msg = "password missing";
      	ret = false;	
      } else if (password.equals(JCoManagedConnectionFactory.INVALID_PASSWORD)) {
      	ret = false;
      	StringBuffer sb = new StringBuffer(InitializationHandler.getMessageResource("system.eai.error.secure.storage1"));
      	sb.append(InitializationHandler.getMessageResource("system.eai.error.secure.storage2"));
		sb.append(InitializationHandler.getMessageResource("system.eai.error.secure.storage3"));
		msg = sb.toString();      	 
      }
      if(props.getProperty(JCoManagedConnectionFactory.JCO_USER) == null) {
        msg = "user missing";
        ret = false;
      }
    }
    if (ret == false) 
    	throw new BackendException(msg);
    else
    	return true;
 }

	public static boolean isClientConnectable(JCO.Client client) {
		try {
			return isClientConnectableInternal(client);
		} catch (BackendException bex) {
			log.debug(bex.getMessage());
			return false;
		}
		
	} 

	public static String isClientConnectableMessage(JCO.Client client) {
		String msg = "OK";
		try {
			isClientConnectableInternal(client);
			return msg;	
		} catch (BackendException bex) {
			log.debug(bex.getMessage());
			return bex.toString();
		}
	} 



 public static String getJCoState(JCO.Client client) {
    StringBuffer stateString = new StringBuffer();
    if (client == null)
      return "null";

    byte state = client.getState();

    if ((state & JCO.STATE_CONNECTED) == JCO.STATE_CONNECTED)
        stateString.append("CONNECTED ");
    if ((state & JCO.STATE_DISCONNECTED) == JCO.STATE_DISCONNECTED)
        stateString.append("DISCONNECTED ");
    if ((state & JCO.STATE_BUSY) == JCO.STATE_BUSY)
        stateString.append("BUSY ");
    if ((state & JCO.STATE_USED) == JCO.STATE_USED)
        stateString.append("USED ");

    return stateString.toString();
 }

    public static void logCall(String functionName,
            JCO.ParameterList importParams,
            JCO.ParameterList exportParams,
			JCO.ParameterList tableParams,
            IsaLocation logExternal, int inOrOut,
            Set excludeImport, Set excludeExport, Set excludeTable) {

			if (excludeImport == null)
				excludeImport = Collections.EMPTY_SET;
				 
			if (excludeExport == null)
				excludeExport = Collections.EMPTY_SET;

			if (excludeTable == null)
				excludeTable = Collections.EMPTY_SET;


			if (inOrOut == LOG_IN) {            	
	            // log import/export parameters
//				int numImportParams = importParams.getNumFields();
				logCall(functionName, importParams, null, log, excludeImport);
				logCall(functionName, exportParams, null, log, excludeExport);
	            	
				// log tables
				if (tableParams != null) {
				int numTables = tableParams.getNumFields();
					for (int i = 0; i < numTables; i++) {
						JCO.Table table = tableParams.getTable(i);
						logCall(functionName, table, log, true);
					}
				}
	            	
				// log tables
//				if (tableParams != null) {
//				int numTables = tableParams.getNumFields();
//					for (int i = 0; i < numTables; i++) {
//						JCO.Table table = tableParams.getTable(i);
//						if (table.isEmpty()) {
//						  //log table name or something else
//						} else {
//						  logCall(functionName, table, null, log, excludeTable);
//					    }
//					}
//				}
			} else {
	            // log import/export parameters
	            logCall(functionName, null, importParams, log, excludeImport);
	            logCall(functionName, null, exportParams, log, excludeImport);
			}
	            	
	            // log tables
				if (tableParams != null) {
		            int numTables = tableParams.getNumFields();
		            for (int i = 0; i < numTables; i++) {
		            	String paramName = tableParams.getName(i);
		            	if (excludeTable.contains(paramName)) {
		            		StringBuffer sb = new StringBuffer();
							sb.append("::")
							  .append(functionName)
							  .append(":: ").append(paramName).append("='").append(HIDDEN).append("'");
		            		log.debug(sb);
		            		continue;
		            	}
		            		
						JCO.Table table = tableParams.getTable(i);
						
						if (inOrOut == LOG_IN) {
							if (!table.isEmpty())
							logCall(functionName, table, logExternal, true);
						}
							
							
						if (inOrOut == LOG_OUT) {
							if (!table.isEmpty()) {
								logCall(functionName, table, logExternal, false);	
							}
						}
		            }
				}
			}			
    /**
     * Logs a RFC-call into the log file of the application.
     *
     * @param functionName the name of the JCo function that was executed
     * @param input input data for the function module. You may set this
     *        parameter to <code>null</code> if you don't have any data
     *        to be logged.
     * @param output output data for the function module. You may set this
     *        parameter to <code>null</code> if you don't have any data
     *        to be logged.
     * @param log the logging context to be used
     */
    public static void logCall(String functionName,
            JCO.Table table,
            IsaLocation logExternal, boolean isIn) {

	if (table == null) 
		return;

		int rows = table.getNumRows();
		int cols = table.getNumColumns();
		String inputName = table.getName();
		

		for (int i = 0; i < rows; i++) {
	
			StringBuffer sb = new StringBuffer();
			sb.append("::")
			  .append(functionName)
			  .append("::")
			  .append((isIn ? " - IN: " : " - OUT: "))
			  .append(inputName)
			  .append("[")
			  .append(i)
			  .append("] ");

			// add an additional space, just to be fancy
			if (i < 10) {
				sb.append("  ");
			}
			else if (i < 100) {
				sb.append(' ');
			}

			sb.append("* ");

			table.setRow(i);

			for (int k = 0; k < cols; k++) {
				sb.append(table.getMetaData().getName(k))
				  .append("='")
				  .append(table.getString(k))
				  .append("' ");
				
			}
			logExternal.debug(sb.toString());
		}
		table.firstRow();
		

    }


    /**
     * Logs a RFC-call into the log file of the application.
     *
     * @param functionName the name of the JCo function that was executed
     * @param input input data for the function module. You may set this
     *        parameter to <code>null</code> if you don't have any data
     *        to be logged.
     * @param output output data for the function module. You may set this
     *        parameter to <code>null</code> if you don't have any data
     *        to be logged.
     * @param log the logging context to be used
     */
    public static void logCall(String functionName,
            JCO.Record input,
            JCO.Record output,
            IsaLocation logExternal, 
            Set excludeParam) {


        if (input != null) {

            StringBuffer in = new StringBuffer();

            in.append("::")
              .append(functionName)
              .append("::")
              .append(" - IN: ")
              .append(input.getName())
              .append(" * ");

            JCO.FieldIterator iterator = input.fields();
			in.append(paramIterator(iterator, excludeParam));
			logExternal.debug(in.toString());
        }

        if (output != null) {

            StringBuffer out = new StringBuffer();

            out.append("::")
               .append(functionName)
               .append("::")
               .append(" - OUT: ")
               .append(output.getName())
               .append(" * ");

            JCO.FieldIterator iterator = output.fields();
			out.append(paramIterator(iterator, excludeParam));
			logExternal.debug(out.toString());
        }
    }

	private static String paramIterator(JCO.FieldIterator iterator, Set excludeParam) {
		StringBuffer sb = new StringBuffer();
		while (iterator.hasMoreFields()) {
			JCO.Field field = iterator.nextField();
			String name = field.getName();
			if (excludeParam.contains(name)) {
				sb.append(field.getName())
				  .append("='").append(HIDDEN).append("'");
			} else {
				sb.append(field.getName())
				   .append("='")
				   .append(field.getString())
				   .append("' ");
			}
		}
		return sb.toString();
	}
	
	public static boolean isPooledMethodSupported() {
		return mIsPooledMethod;
	}
	
	public static boolean isPooled(JCO.Client client) {
		return client.isPooled();			
	}
	
	public static boolean isJCSAvailable() {
		Class jcsClass = null;
		try {
			jcsClass = Class.forName("com.sap.isa.jcs.JCS");
		} catch (Throwable t) {
			log.debug(t.getMessage());
			return false;
		}
		if (jcsClass == null)
			return false;
		else 
			return true;
	}
	
	/**
	 * Retrieve the message text for a JCO exception, using BAPI_MESSAGE_GETDETAIL
	 * @param ex the exception that contains the necessesary info (but not the text)
	 * @param connection connection to the backend
	 * @return the language dependent message text
	 * @throws BackendException exception from call of BAPI_MESSAGE_GETDETAIL
	 */
	public static String getMessageText(JCO.Exception ex, JCoConnection connection) throws BackendException {

		String id = ex.getMessageClass();
		String number = ex.getMessageNumber();
		JCO.Function bapiMessageGetDetail = connection.getJCoFunction("BAPI_MESSAGE_GETDETAIL");
		JCO.ParameterList importParams = bapiMessageGetDetail.getImportParameterList();

		//now fill import parameters
		importParams.setValue(id, "ID");
		importParams.setValue(number, "NUMBER");
		importParams.setValue("ASC", "TEXTFORMAT");

		//check on additional parameters
		for (int i = 0; i < 4; i++) {
			String messageValue = ex.getMessageParameter(i);
			if (messageValue != null && !messageValue.equals("")) {
				importParams.setValue(messageValue, "MESSAGE_V" + (i + 1));
			}
		}

		//execute function module
		connection.execute(bapiMessageGetDetail);

		return bapiMessageGetDetail.getExportParameterList().getString("MESSAGE");
		
	}
	
	
	
}