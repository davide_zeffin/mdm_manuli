/*****************************************************************************
    Class:        ExtensionSAP
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      October 2001
    Version:      0.1

    $Revision: #2 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.core.eai.sp.jco;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.sap.mw.jco.JCO;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.businessobject.boi.ObjectBaseData;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.core.util.table.TableRow;


/**
 * The ExtensionSAP class corresponds to a CRM extension with structure
 * CRMT_ISALES_EXTENENTION and
 * will offer a set of static methods, which handle the converting
 * between the Java objects and the JCo structures.
 *
 * <h4>Example for generic filling of JCo table</h4>
 * <pre>
 *    JCO.Table extensionTable = function.getTableParameterList()
 *                                      .getTable("EXTENSION_IN");
 *
 *   // add all relvant extension from the basket object to the given JCo table.
 *   ExtensionSAP.fillExtensionTable(extensionTable, basket);
 * </pre>
 *
 * <H4>Example for adding extensions to a business object</H4>
 * <pre>
 *   JCO.Table extensionTable = function.getTableParameterList()
 *                                      .getTable("EXTENSION_OUT");
 *
 *   // add all extensions to the items of the sales document
 *   ExtensionSAP.addToBusinessObject(salesDoc.iterator(),extensionTable);
 * </pre>
 *
 * <H4>Example for using extensions in ResultData</H4>
 * <pre>
 *    JCO.Table extensionTable = function.getTableParameterList()
 *                               .getTable("EXTENSION_OUT");
 *
 *    // pre process extensions for use in result data
 *    ExtensionSAP.TablePreprocessedExtensions tableExtensions
 *            = ExtensionSAP.createTablePreprocessedExtensions(extensionTable);
 *
 *    // get the shoplist
 *    JCO.Table shops = function.getTableParameterList().getTable("SHOPS");
 *
 *    shopTable.addColumn(Table.TYPE_STRING,ShopData.DESCRIPTION);
 *    shopTable.addColumn(Table.TYPE_STRING,ShopData.ID);
 *
 *    // add columns found in the extensions
 *    ExtensionSAP.addTableColumnFromExtension(shopTable, tableExtensions);
 *
 *    int numShops = shops.getNumRows();
 *
 *    for (int i = 0; i < numShops; i++) {
 *
 *        TableRow shopRow = shopTable.insertRow();
 *
 *        shopRow.setRowKey(new TechKey(shops.getString("ID")));
 *        shopRow.getField(ShopData.ID).setValue(shops.getString("ID"));
 *        shopRow.getField(ShopData.DESCRIPTION).
 *                setValue(shops.getString("DESCRIPTION"));
 *
 *        // fill extension columns with the data from the extensions
 *        ExtensionSAP.fillTableRowFromExtension(shopRow, tableExtensions);
 *
 *        shops.nextRow();
 *    }
 * </pre>
 *
 * @author Wolfgang Sattler
 * @version 1.0
 */
public class ExtensionSAP {


    /**
     * The createMap method builds a map with all extensions given within a
     * JCo Table
     *
     * @param  extensionTable a JCo table with the extensions
     * @return a map with all extension given with the JCo Table. <br>
     *         The map contains <code>List</code>s with the extension for every
     *         object.<br>
     *         The key is given with the refKey if exists or the handle else.
     *
     */
    protected static Map createMap(JCO.Table extensionTable) {

        Map map = new HashMap();

        if (extensionTable == null) {
            return map;
        }

        int numExtension = extensionTable.getNumRows();

        extensionTable.firstRow();

        ExtensionSAP extensionSAP= null;

        for (int i = 0; i < numExtension; i++) {

            if (extensionTable.getInt("COUNTER")>1) {
                extensionSAP.setValue(extensionSAP.getValue()
                                     +extensionTable.getString("VALUE"));
            }
            else {
                // the extension object was created
                extensionSAP = new ExtensionSAP(extensionTable);

                TechKey refKey = extensionSAP.getRefTechKey();
                Object key = null;

                if (!refKey.isInitial() && refKey.getIdAsString().length()>0) {
                    key = refKey;
                }
                else {
                    key = extensionSAP.getHandle();
                }

                // check, if some extension for the combination type + handle could be found
                List extensions = (List)map.get(key);

                    if (extensions == null) {
                        // create one List for every business object
                        extensions = new ArrayList(5);
                        map.put(key,extensions);
                    }

                extensions.add(extensionSAP);
            }
            extensionTable.nextRow();
        } // for

        return map;
    }


    /* a private method which add all extensions to a given Business Object */
    private static void addExtensionsToBob(ObjectBaseData bob, Map map) {

        TechKey refKey = bob.getTechKey();
        Object key = null;

        if (refKey != null && !refKey.isInitial() && refKey.getIdAsString().length()>0) {
            key = refKey;
        }
        else {
            key = bob.getHandle();
        }

        if (key != null) {
            List extensions = (List)map.get(key);
            if (extensions != null) {
                Iterator iterator = extensions.iterator();
                while (iterator.hasNext() ) {
                    ExtensionSAP extensionSAP = (ExtensionSAP)iterator.next();
                    bob.addExtensionData(extensionSAP.getName(),extensionSAP.getValue());
                    map.remove(key);
                }
            }
        }
    }


    /**
     * This method add all given extensions to the given business object.
     * <b> No checks will be done against the refKey, in this method <b>
     *
     * @see import com.sap.isa.core.businessobject.ObjectBase;
     *
     */
    public static void addToBusinessObject(ObjectBaseData bob,
                                           JCO.Table extensionTable) {

        if (extensionTable == null) {
            return;
        }

        int numExtension = extensionTable.getNumRows();

        extensionTable.firstRow();

        for (int i = 0; i < numExtension; i++) {
            String name  = extensionTable.getString("NAME");
            String value = extensionTable.getString("VALUE");

            if (extensionTable.getInt("COUNTER")>1) {
                bob.addExtensionData(name,(String)bob.getExtensionData(name)
                                          +extensionTable.getString("VALUE"));
            }
            else {
                bob.addExtensionData(name,value);
            }
        extensionTable.nextRow();
        }

    }


    /**
     * This method add all extensions to the given business object and is aware
     * of sub objects too, if <code>withSubObjects</code> is set to <code>true</code>.
     * See ObjectBase for more details about sub objects
     *
     * @param bob the business object which obtains the extensions
     * @param extensionTable JCo table with extension structures
     * @param withSubObjects flag, if subobjects are aware
     *
     * @see import com.sap.isa.core.businessobject.ObjectBase;
     *
     */
    public static void addToBusinessObject(ObjectBaseData bob,
                                           JCO.Table extensionTable,
                                           boolean withSubObjects) {

        Map map = createMap(extensionTable);

        if (map.isEmpty()) {
            return;
        }

        addExtensionsToBob(bob, map);

        if (!withSubObjects) {
            return;
        }

        // now check if the sub objects also have extensions
        Iterator iter = bob.getSubObjectIterator();
        while (iter.hasNext()) {
            Object next = iter.next();
            if (next instanceof ObjectBaseData) {
                ObjectBaseData iBob = (ObjectBaseData)next;
                addExtensionsToBob(iBob, map);
                if (map.isEmpty()) {
                    return;
                }
            }
        }
    }


    /**
     * This method add all extensions to the given business objects, which are given
     * within an iterator.
     *
     * @param iterator list of business objects which should obtain extension
     * @param extensionTable JCo table with extension structures
     *
     */
    public static void addToBusinessObject(Iterator iterator,
                                           JCO.Table extensionTable) {

        Map map = createMap(extensionTable);

        if (map.isEmpty()) {
            return;
        }

        // now check if the sub objects also have extensions
        while (iterator.hasNext()) {
            Object next = iterator.next();
            if (next instanceof ObjectBaseData) {
                ObjectBaseData iBob = (ObjectBaseData)next;
                addExtensionsToBob(iBob, map);
                if (map.isEmpty()) {
                    return;
                }
            }
        }
    }


    /**
     *
     * This method add all extensions which belongs to the given buiness object
     * to the given JCo table.
     *
     * @param bob the business object which contains the extensions
     * @param extensionTable JCo table with extension structures
     *
     */
    public static void fillExtensionTable(JCO.Table extensionTable, ObjectBaseData bob) {

        int fieldLength = extensionTable.getField("VALUE").getLength();

        Iterator iterator = bob.getExtensionDataValues().iterator();


        while (iterator.hasNext() ) {

            Map.Entry extention = (Map.Entry)iterator.next();
            Object name = extention.getKey();

            /* only string-like objects can be used as extensions */
            if (name instanceof String && extention.getValue() instanceof String) {

                String refGuid = "";

                if (bob.getTechKey() != null) {
                    refGuid = bob.getTechKey().getIdAsString();
                }

                String value = (String)extention.getValue();

                if (value.length() > fieldLength) {
                    int pos = 0;
                    int counter = 1;

                    while (pos<value.length()) {
                        extensionTable.appendRow();
                        extensionTable.setValue(refGuid,"REF_GUID");
                        extensionTable.setValue(bob.getHandle(),"ALT_HANDLE");
                        extensionTable.setValue((String)name,  "NAME");
                        String valueSubString = "";
                        if (pos+fieldLength-1<value.length()) {
							valueSubString = value.substring(pos,pos+fieldLength);
                        }
                        else {
							valueSubString = value.substring(pos);
                        }
                        
						extensionTable.setValue(valueSubString, "VALUE");
                        
                        extensionTable.setValue(counter, "COUNTER");
						counter++;
                        pos += fieldLength;
                    }
                }
                else {
                    extensionTable.appendRow();
                    extensionTable.setValue(refGuid,"REF_GUID");
                    extensionTable.setValue(bob.getHandle(),"ALT_HANDLE");
                    extensionTable.setValue((String)name,  "NAME");
                    extensionTable.setValue((String)value, "VALUE");
                }
            }
        }
    }


    /**
     * This method add all extensions which belongs to the given buiness object
     * to the given JCo table.<br>
     * The method is aware of sub objects too, if <code>withSubObjects</code>
     * is set to <code>true</code>. <br>
     * See ObjectBase for more details about sub objects
     *
     * @param bob the business object which obtains the extensions
     * @param extensionTable JCo table with extension structures
     * @param withSubObjects flag, if subobjects are aware
     *
     * @see import com.sap.isa.core.businessobject.ObjectBase;
     *
     */
    public static void fillExtensionTable(JCO.Table extensionTable,
                                          ObjectBaseData bob,
                                          boolean withSubObjects) {

        // First check, if the business itself has extensions
        fillExtensionTable(extensionTable, bob);

        if (withSubObjects) {
            // now check if the sub objects also have extensions
            Iterator iter = bob.getSubObjectIterator();
            while (iter.hasNext()) {
                Object next = iter.next();
                if (next instanceof ObjectBaseData) {
                    ObjectBaseData iBob = (ObjectBaseData)next;
                    fillExtensionTable(extensionTable,iBob);
                }
            }
        }

    }


    /**
     * This method add all extensions which belongs to the given buiness object
     * within the Iterator to the given JCo table.
     *
     * @param extensionTable JCo table with extension structures
     * @param iterator list of business objects which should obtain extension
     *
     *
     */
    public static void fillExtensionTable(JCO.Table extensionTable, Iterator iterator) {

        while (iterator.hasNext()) {
            Object next = iterator.next();
            if (next instanceof ObjectBaseData) {
                ObjectBaseData iBob = (ObjectBaseData)next;
                fillExtensionTable(extensionTable,iBob);
            }
        }
    }



    /**
     * This method creates an instance of the inner class
     * <code>TablePreprocessedExtensions</code>, which is needed to use the
     * extensions to extend a ResultData
     *
     * @param extensionTable JCo table with extension structures
     *
     * @return a proprocced extension table
     *
     * @see TablePreprocessedExtensions
     *
     */
    public static TablePreprocessedExtensions createTablePreprocessedExtensions(JCO.Table extensionTable) {

        return new TablePreprocessedExtensions(extensionTable);
    }


    /**
     * This method creates an instance of the inner class
     * <code>TablePreprocessedExtensions</code>, which is needed to use the
     * extensions to extend a ResultData
     *
     * @param extensionTable JCo table with extension structures
     *
     * @return a proprocced extension table
     *
     * @see TablePreprocessedExtensions
     *
     */
    public static void addTableColumnFromExtension(Table table,
                                              TablePreprocessedExtensions extensionTable) {

        Iterator iter = extensionTable.getAdditionalColumnNames();

        while (iter.hasNext()) {

            Map.Entry column = (Map.Entry)iter.next();

            table.addColumn(Table.TYPE_STRING,(String)column.getKey());
        }

    }


    /**
     * This method creates an instance of the inner class
     * <code>TablePreprocessedExtensions</code>, which is needed to use the
     * extensions to extend a ResultData
     *
     * @param extensionTable JCo table with extension structures
     *
     * @return a proprocced extension table
     *
     * @see TablePreprocessedExtensions
     *
     */
    public static void fillTableRowFromExtension(TableRow tableRow,
                                                 TablePreprocessedExtensions extensionTable) {

        Iterator iter = extensionTable.getExtensions((TechKey)tableRow.getRowKey(),"");

        if (iter != null) {

            while (iter.hasNext()) {

                ExtensionSAP extension = (ExtensionSAP)iter.next();

                tableRow.getField(extension.getName()).setValue(extension.getValue());
            }
        }

    }


    private TechKey refTechKey;
    private String  handle;
    private String  type;
    private String  name;
    private String  value;

    /**
     *  The constructor creates an ExtensionSAP object from a JCO.Record
     *
     *  @param extensionRecord
     */
    public ExtensionSAP(JCO.Record extensionRecord) {

        refTechKey = new TechKey(extensionRecord.getString("REF_GUID"));
//        type = extensionRecord.getString("REF_TYPE");
        handle = extensionRecord.getString("ALT_HANDLE");

        name = extensionRecord.getString("NAME");
        value = extensionRecord.getString("VALUE");
    }

    /**
     * Set the property refTechKey.
     *
     * @param refTechKey techKey of the object, to which the extension refer
     *
     */
    public void setRefTechKey(TechKey refTechKey) {
        this.refTechKey = refTechKey;
    }


    /**
     * Returns the property refTechKey.
     *
     * @return refTechKey techKey of the object, to which the extension refer
     *
     */
    public TechKey getRefTechKey() {
        return refTechKey;
    }



    /**
     * Set name of the extension
     *
     * @param name
     *
     */
    public void setName(String name) {
        this.name = name;
    }


    /**
     * Returns the name of the extension
     *
     * @return name
     *
     */
    public String getName() {
        return name;
    }



    /**
     * Set the value of the extension
     *
     * @param value
     *
     */
    public void setValue(String value) {
        this.value = value;
    }


    /**
     * Returns the value of the extension
     *
     * @return value
     *
     */
    public String getValue() {
        return this.value;
    }



    /**
     * Set the property type
     *
     * @param type
     *
     */
    public void setType(String type) {
        this.type = type;
    }


    /**
     * Returns the property type
     *
     * @return type
     *
     */
    public String getType() {
        return type;
    }



    /**
     * Set the property handle
     *
     * @param handle
     *
     */
    public void setHandle(String handle) {
        this.handle = handle;
    }


    /**
     * Returns the property handle
     *
     * @return handle
     *
     */
    public String getHandle() {
       return handle;
    }



    /**
     * Overwrite the method of the object class.
     *
     * @param obj the object which will be compare with the object
     * @return true if the objects are equal
     *
     */
    public boolean equals(Object obj) {

        if (obj == null) {
            return false;
        }

        if (obj == this) {
            return true;
        }

        if (obj instanceof ExtensionSAP) {
            ExtensionSAP extensionSAP = (ExtensionSAP)obj;

            return  extensionSAP.refTechKey.equals(refTechKey)
            && extensionSAP.value.equals(handle)
            && extensionSAP.name.equals(name)
            && extensionSAP.value.equals(value)
            && extensionSAP.type.equals(type);

        }
        return false;
    }


    /**
     * returns hashcode for the extension.
     *
     * @return hashcode of the objects
     *
     */
    public int hashCode( ) {

        return refTechKey.hashCode() ^
        handle.hashCode() ^
        type.hashCode() ^
        name.hashCode() ^
        value.hashCode();
    }

    /**
     * Returns the object as string.
     *
     * @return String which contains all fields of the object
     */
    public String toString( ) {

        return "refTechKey: " + refTechKey.toString() + "\n" +
        "type: " + type + "\n" +
        "handle: " + handle + "\n" +
        "name: " + name + "\n" +
        "value: " + value;
    }


    public static class TablePreprocessedExtensions {

        JCO.Table extensionTable;
        Map map = new HashMap();
        Map additionalFieldNames = new HashMap(5);

        private TablePreprocessedExtensions(JCO.Table extensionTable){
            this.extensionTable = extensionTable;

            int numExtension = extensionTable.getNumRows();

            extensionTable.firstRow();

            ExtensionSAP extensionSAP = null;

            for (int i = 0; i < numExtension; i++) {

                if (extensionTable.getInt("COUNTER")>1) {
                    extensionSAP.setValue(extensionSAP.getValue()
                                         +extensionTable.getString("VALUE"));
                }
                else {
                    // the extension object was created
                    extensionSAP = new ExtensionSAP(extensionTable);

                    TechKey refKey = extensionSAP.getRefTechKey();
                    Object key = null;

                    if (!refKey.isInitial() && refKey.getIdAsString().length()>0) {
                        key = refKey;
                    }
                    else {
                        key = extensionSAP.getHandle();
                    }

                    // check, if some extension for the combination type + handle could be found
                    Map extensions = (Map)map.get(key);

                        if (extensions == null) {
                            // create one List for every business object
                            extensions = new HashMap(5);
                            map.put(key,extensions);
                        }

                    extensions.put(extensionSAP.getName(),extensionSAP);
                    additionalFieldNames.put(extensionSAP.getName(),null);
                }

                extensionTable.nextRow();

            } // for
        }


        /* returns an iterator over MapEntries */
        private Iterator getAdditionalColumnNames() {
            return additionalFieldNames.entrySet().iterator();
        }


        /* returns the extensions for a given key */
        private Iterator getExtensions(TechKey refKey, String handle){

            Object key = null;

            if (!refKey.isInitial() && refKey.getIdAsString().length()>0) {
                key = refKey;
            }
            else {
                key = handle;
            }

            Map extensions = (Map)map.get(key);

            if (extensions != null) {
                return extensions.values().iterator();
            }

            return null;
        }

    }




}


