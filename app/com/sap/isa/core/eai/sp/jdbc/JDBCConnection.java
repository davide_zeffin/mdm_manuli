/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      14 May 2001
  $Id: //java/sapm/esales/30/src/com/sap/isa/core/eai/sp/ipc/IPCConnection.java#1 $
  $Revision: #1 $
  $Change: 22942 $
  $DateTime: 2001/06/26 10:29:54 $
  $Author: d028486 $
*****************************************************************************/
package com.sap.isa.core.eai.sp.jdbc;

import java.sql.Connection;

import sqlj.runtime.ConnectionContext;

/**
 * Wrapper for a JDBC connection
 */
public interface JDBCConnection extends com.sap.isa.core.eai.Connection {

  /**
   * Returns the real JDBC connection
   * @return the real JDBC connection
   */
  public Connection getConnection();

  /**
   *  Returns a SQLJ Connection Context
   * @return a SQLJ Connection Context
   */
  public ConnectionContext getConnectionContext();

}