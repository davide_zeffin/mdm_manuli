/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      14 February 2002
  $Id: //java/sapm/esales/30/src/com/sap/isa/core/eai/sp/ipc/IPCConnection.java#1 $
  $Revision: #1 $
  $Change: 22942 $
  $DateTime: 2001/06/26 10:29:54 $
  $Author: d028486 $
*****************************************************************************/
package com.sap.isa.core.eai.sp.jdbc;


// java imports
import java.sql.Connection;
import java.sql.SQLException;

import sqlj.runtime.ConnectionContext;

import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.ConnectionEventListener;
import com.sap.isa.core.eai.ManagedConnectionBase;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;



public class JDBCConnectionImpl extends ManagedConnectionBase
                                implements JDBCConnection {

  protected static IsaLocation log = IsaLocation.
        getInstance(JDBCConnectionImpl.class.getName());

  private Connection mJdbcCon;
  //private ConnectionContext mConContext;

  /**
   * This methis is not implemented yet
   * @param aListener A listener which will be notofied if connection events occur
   */
  public void addConnectionEventListener(ConnectionEventListener aListener) {
  }


  /**
   * Allocates a physical connection to this managed connection
   * @param connection physical connection
   */
  public void associateConnection(Object connection) {
  	ConnectionContainer conCon = (ConnectionContainer)connection; 
    mJdbcCon = conCon.getSQLConnection();
    //mConContext = conCon.getConnectionContext();
  }
  public boolean isValid() {
    if (mJdbcCon != null)
      return true;
    else
      return false;
  }

  /**
   * Closes the internal JDBC connection. The current implementation does nothing.
   * The connection is closed/returned to pool when it is cleaned up
   */
  public void close() {
  }

  /**
   * Returns the real JDBC connection. The connection is either
   * created or served from a pool
   * @return the real JDBC connection
   */
  public Connection getConnection() {
    return mJdbcCon;
  }

  /**
   * Cleans up underlying physical connection. This method is
   * called by the framework in order to clean up the connection management
   */
  public void cleanup() {
    try {
      mJdbcCon.close();
    } catch (SQLException ex) {
       log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.eai.exception", new Object[] { "Could not close JDBC connection" }, ex);
    }
/*	try {
	  mConContext.close();
	} catch (SQLException ex) {
	   log.error("system.eai.exception", new Object[] { "Could not close connection context" }, ex);
	}
*/
  }
  
  /**
   * Returns a connection context used for SQLJ 
   * A new context is returned every time this method is callse
   * @return a new connection context every time this method is called
   */
  public ConnectionContext getConnectionContext()  {
	try {
		ConnectionContext cc = (ConnectionContext)managedConnectionFactory.getConnection(conDefName, null);
		return cc;
	} catch (BackendException bex) {
		log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.eai.exception", new Object[] { "Could not close connection context" }, bex);
		return null;
	}
  	}
  	

}