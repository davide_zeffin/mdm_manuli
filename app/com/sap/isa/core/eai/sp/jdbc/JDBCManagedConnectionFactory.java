/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      15 February 2002
*****************************************************************************/
package com.sap.isa.core.eai.sp.jdbc;
  
// java imports
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import javax.sql.DataSource;

import sqlj.runtime.ConnectionContext;

import com.sap.isa.core.db.DBException;
import com.sap.isa.core.db.DBHelper;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.ConnectionConfig;
import com.sap.isa.core.eai.ConnectionDefinition;
import com.sap.isa.core.eai.ManagedConnection;
import com.sap.isa.core.eai.ManagedConnectionFactory;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;

/**
 * This is a specific Connection Manager for JCo connections
 */
public class JDBCManagedConnectionFactory implements ManagedConnectionFactory {

  protected static IsaLocation log = IsaLocation.
        getInstance(JDBCManagedConnectionFactory.class.getName());
  
  private Properties conFacProps;
  private Map conConfigs = new HashMap();
  private Map conDefs;
  private String factoryName;
  private Map mDataSources = new HashMap();

  /**
   * Constructor
   */
  public JDBCManagedConnectionFactory() {}


  /**
   * Specifies the name of the factory
   * @param factoryName type of factory
   */
  public void setFactoryName(String factoryName) {
    this.factoryName = factoryName;
  }

  /**
   * Gets the type of the factory
   * @return type of factory
   */
  public String getFactoryName() {
    return factoryName;
  }



  /**
   * Gets a managed connection
   * @param conConfigName name of connection configuration which should be
   *    used to create the managed connection
   * @param conDefProps connection definition properties
   * @return a managed connection
   */
  public ManagedConnection getManagedConnection(String conConfigName, Properties conDefProps)
              throws BackendException {
    if (log.isDebugEnabled()) {
      log.debug("getManagedConnection(String, Properties) entered; conConfigName = \"" + conConfigName
            + "\" conDef props = \"" + conDefProps.toString() + "\"");
    }

    ConnectionConfig conConfig = (ConnectionConfig)conConfigs.get(conConfigName);

    if (conConfig == null) {
      String msg = "Connection named '" + conConfigName + "' not found";
      log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.eai.exception", new Object[] { msg }, null);
      throw new BackendException(msg);
    }

    String conDef = conConfig.getConnectionDefinition();
    ConnectionDefinition def = (ConnectionDefinition)conDefs.get(conDef);

    if (def == null) {
      String msg = "Connection Definition named '" + conDef + "' not found";
      log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.eai.exception", new Object[] { msg }, null);
      throw new BackendException(msg);
    }

    Properties existingProps = def.getProperties();

    Properties newMixedProps = (Properties)existingProps.clone();

    Enumeration passedPropNamesEnum = conDefProps.propertyNames();

    String propName = null;
    while (passedPropNamesEnum.hasMoreElements()) {
      propName = (String)passedPropNamesEnum.nextElement();
      newMixedProps.put(propName, conDefProps.getProperty(propName));
    }

    if (log.isDebugEnabled()) {
      log.debug("mixed connection properties: " + newMixedProps.toString());
    }

    return createManagedConnection(conConfigName, newMixedProps);
  }

  /**
   * Gets a managed connection
   * @param conConfigName name of connection configuration which should be
   *    used to create the managed connection
   * @return a managed connection
   */
  public ManagedConnection getManagedConnection(String conConfigName)
              throws BackendException {
    if (log.isDebugEnabled())
      log.debug("getManagedConnection(String) entered");

      ConnectionConfig conConfig = (ConnectionConfig)conConfigs.get(conConfigName);
      if (conConfig == null) {
        String msg = "Connection named '" + conConfigName + "' not found";
        log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.eai.exception", new Object[] { msg }, null);
        throw new BackendException(msg);
      }


      String conDef = conConfig.getConnectionDefinition();
      ConnectionDefinition def = (ConnectionDefinition)conDefs.get(conDef);
      if (def == null) {
        String msg = "Connection Definition named '" + conDef + "' not found";
        log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.eai.exception", new Object[] { msg }, null);
        throw new BackendException(msg);
      }


      Properties props = def.getProperties();

      return createManagedConnection(conConfigName, props);
    }

  /**
   * Inititalizes a managed connection factory
   * @param conDefs A Map containing backend connection definitions
   */
  public void initConnections(Map conDefs)
          throws BackendException {
    this.conDefs = new HashMap(conDefs);

    log.info("system.eai.jdbc.init.con");

    if (conDefs == null || conDefs.size() == 0)
      return;

    Iterator conDefIterator = conDefs.values().iterator();
    ConnectionDefinition conDef = null;
    while (conDefIterator.hasNext()) {
        conDef = (ConnectionDefinition)conDefIterator.next();

        log.info("system.eai.jdbc.init.ProcessingConDef", new Object[] { conDef.getName() }, null);

        this.conDefs.put(conDef.getName(), conDef);
      }
  }


  /**
   * Adds a connection configurations to the connection factory
   * @param a set of connection configurations
   */
  public void addConnectionConfigs(Map conConfigs) {
    this.conConfigs.putAll(conConfigs);
  }

  public Object getConnection(String conDefName) throws BackendException {
    ConnectionDefinition def = (ConnectionDefinition)conDefs.get(conDefName);
    if (def == null) {
      String msg = "Connection Definition named '" + conDefName + "' not found";
      log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.eai.exception", new Object[] { msg }, null);
      throw new BackendException(msg);
    }
    Properties props = def.getProperties();

	
	ConnectionContainer conCon = getConnectionContainer(props);
    

    return conCon;
  }


  /**
   * Returns a connection container with an new connection context. No new JDBC conenction
   * is created
   * @return null
   */
  public Object getConnection(String conDefName, Properties conDefProps) throws BackendException {
	ConnectionDefinition def = (ConnectionDefinition)conDefs.get(conDefName);
	if (def == null) {
	  String msg = "Connection Definition named '" + conDefName + "' not found";
	  log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.eai.exception", new Object[] { msg }, null);
	  throw new BackendException(msg);
	}
	Properties props = def.getProperties();
  	// this mehtod is used to just to create a connection context
	ConnectionContext cc = DBHelper.getSQLJContext();
    
    return cc;
  }

  /**
   * Not implemented for JDBC
   * @return false
   */
  public boolean isConnectionAvailable(ConnectionDefinition conDef) {
      return false;
  }

  /**
   * Gets a JDBC connection.
   * @return A JDBC connection
   */
  public static Connection getJDBCConnection(Properties props) throws BackendException {
    if (log.isDebugEnabled())
      log.debug("JDBC connection requested");
      
    // return always connection returned from pool
    return getPooledConnection();
  }

/**
   * This method returns a managed connection which encapsulates a JDBC connection
   * @param conConfigName Name of connection configuration
   * @param props Connection properties
   * @return A new managed connection
   * @throws BackendException If something goes wrong while creating a managed connection
   */
  private ManagedConnection createManagedConnection(String conConfigName, Properties props)
    throws BackendException {

    ConnectionConfig conConfig = (ConnectionConfig)conConfigs.get(conConfigName);
    ConnectionDefinition conDef = (ConnectionDefinition)conDefs.get(conConfig.getConnectionDefinition());
    String conDefName = conConfig.getConnectionDefinition();

    ManagedConnection managedConnection;

    try {
      Class jdbcManagedConnectionClass = Class.forName(conConfig.getClassName());
      managedConnection =
          (ManagedConnection)jdbcManagedConnectionClass.newInstance();

      } catch (Exception ex) {
        throw new BackendException("Could not create managed connection with the class name '"
              + conConfig.getName() + "\n" + ex.toString());
    }

    // set connection config name
    managedConnection.setConnectionDefinitionName(conDefName);
    managedConnection.setConnectionConfigName(conConfigName);
    managedConnection.setConnectionFactoryName(factoryName);
    managedConnection.setManagedConnectionFactory(this);

	ConnectionContainer conCon = getConnectionContainer(props);

    managedConnection.associateConnection(conCon);

    return managedConnection;
  }

  /**
   * Sets a set of properties
   * @param props Properties which can be used for additional configuratio
   *        of a factory
   */
  public void setProperties(Properties props) {
    conFacProps = (Properties)props.clone();
  }

  /**
   * Gets connection from JDBC pool
   */
  private static Connection getPooledConnection() throws BackendException {
    if (log.isDebugEnabled())
      log.debug("Getting pooled JDBC connection");

    // check if data source already available
    DataSource ds = null;
    Connection con = null;

    // perform lookup
    String lookupString = "java:comp/env/jdbc/crmjdbcpool";
	
    if (ds == null) {
      try {
      	Properties props= new Properties();
      	props.put(DBHelper.DB_DATASOURCE, lookupString);
        ds = DBHelper.getDataSource(props);;
      }
      catch (RuntimeException ex) {
        //log.error("system.eai.exception", null, ex);
        throw new BackendException(ex);
      }
    }
    try {
      con = ds.getConnection();
    } catch (SQLException ex) {
        String msg = "Could not find Data Source for [lookup_name]='" + lookupString + "'";
        //log.error("system.eai.exception", new Object[] { msg }, ex);
        throw new BackendException(ex);
    }
    return con;
  }

  /**
   * Gets a jdbc connection directly from the driver
   */
  private static Connection getDirectCon(Properties props) throws BackendException {
    if (log.isDebugEnabled())
      log.debug("Getting direct JDBC connection");
    Connection con = null;
    String dbclass = props.getProperty(JDBCConstants.JDBC_DB_CLASS);
    String dburl = props.getProperty(JDBCConstants.JDBC_URL);
    String user = props.getProperty(JDBCConstants.JDBC_USER);
    String password = props.getProperty(JDBCConstants.JDBC_PASSWORD);
    if (dbclass == null || dburl == null) {
      String msg = "Getting JDBC connection failed. [dbclass] or [dburl] missing";
      //log.error("system.eai.exception", new Object[] { msg }, null);
      return con;
    }

    try {
      Class.forName(dbclass);
    } catch (ClassNotFoundException ex) {
	  String msg = ("could not load JDBC driver '" + dbclass + "'");
	  //log.error("system.eai.exception", new Object[] { msg }, ex);
      throw new BackendException(msg);
    }

    // create connection
    try {
      if (user == null)
        con = DriverManager.getConnection(dburl);
      else
        con = DriverManager.getConnection(dburl, user, password);
    } catch (SQLException ex) {
      String msg = "could not establish database connection [dburl]='" + dburl +
                   "' [user]='" + user + "' [password]=' " +
                   (password == null ? "missing" : "?");
      //log.error("system.eai.exception", new Object[] { msg }, ex);
      throw new BackendException(msg);
    }

    return con;
  }

  /**
   * This method can be used to check whether to passed set of properties is
   * sufficient in order to create a connection to a SAP system
   * @param props connection properties
   * @return true if connection could be established, otherwise false
   */
  public static String testConnection(Properties props) throws BackendException {
    String retVal = "";
	Connection con = null;
    try {
      con = getJDBCConnection(props);
      retVal = con.getMetaData().getDatabaseProductName();
      
    } catch (SQLException ex) {
      throw new BackendException(ex);
    } finally {
    	if (con != null) {
    		try {
				con.close();
    		} catch (SQLException sqlex) {
    			log.debug(sqlex.getMessage());
    		}
		
    	}
    }
    return retVal;
  }
  
  private ConnectionContainer getConnectionContainer(Properties props) throws BackendException {
	ConnectionContext conContext = null;
	Connection jdbcCon = null;
	String errorMsg1 = null;
	String errorMsg2 = null;
	BackendException bex1 = null;
	BackendException bex2 = null;
	String conType = props.getProperty(JDBCConstants.JDBC_CONNECTIONTYPE);
	try {
			conContext = DBHelper.getSQLJContext();
	} catch (DBException dbex) {
		bex1 = new BackendException(dbex);
	}
	try {
			jdbcCon = getJDBCConnection(props);
		} catch (BackendException bex) {
			log.debug(bex.getMessage());
			bex1 = bex;
		}
	conContext = null;

	// if both connections null throw exception
	if ((jdbcCon == null) && (conContext == null)) {
		if (bex1 != null) {
			log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.eai.exception", new Object[] { "Error creating JDBC Connection" }, bex1); 
			throw bex1;
		}
			
			
		if (bex2 != null) {
			log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.eai.exception", new Object[] { "Error creating SQLJ Connection Context Connection" }, bex1);
			throw bex2;	 
		}
	}
	// close connection context again. Was created only for testing purposes
	if (conContext != null) {
		try {
			conContext.close(); 
		} catch (SQLException sqlException) {
			log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.eai.exception", new Object[] { "Error closing SQLJ Connection Context Connection" }, sqlException);
		}
	}			
	return new ConnectionContainer(jdbcCon, conContext);	
  }
  
}

