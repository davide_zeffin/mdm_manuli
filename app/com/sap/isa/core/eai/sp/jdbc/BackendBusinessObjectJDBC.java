/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      14 May 2001
  $Id: //java/sapm/esales/30/src/com/sap/isa/core/eai/sp/ipc/BackendBusinessObjectIPC.java#1 $
  $Revision: #1 $
  $Change: 22942 $
  $DateTime: 2001/06/26 10:29:54 $
  $Author: d028486 $
*****************************************************************************/
package com.sap.isa.core.eai.sp.jdbc;

import java.util.Properties;

import com.sap.isa.core.eai.BackendBusinessObject;
import com.sap.isa.core.eai.BackendException;

/**
 * This interface should be implemented by Backend Business Object
 * wich communicate with the with a Database via JDBC
 */

public interface BackendBusinessObjectJDBC extends BackendBusinessObject {

    /**
     * Returns a JDBC connection which uses default connection settings as specified in the
     * external configuration file.
     */
    public JDBCConnection getDefaultJDBCConnection();

    /**
     * Returns a JDBC Connection which uses settings based on the settings for the
     * default JDBC connection. The passed set of properties extends/overwrites the
     * settings specified for the default JDBC connection.
     * @param props A set of properties which overwrites/extends the properties
     *        specified for the default JDBC connection for this backend object
     * @return JDBC connection
     */
    public JDBCConnection getModDefaultJDBCConnection(Properties props) throws BackendException ;

}
