package com.sap.isa.core.eai.sp.jdbc;

/**
 * Global constants needed to define JDBC connection parameters
 */
public interface JDBCConstants {

  /**
   * name of the JDBC managed connection factory
   */
  public static final String JDBC_MANAGED_CON_FACTORY = "JDBC";


  /**
   * User
   * value: USER
   */
  public static final String JDBC_USER   = "user";

  /**
   * Password
   * value: PASSWORD
   */
  public static final String JDBC_PASSWORD     = "password";

  /**
   * JDBC URL
   * value: url
   */
  public static final String JDBC_URL     = "url";

  /**
   * Database class
   * value: dbclass
   */
  public static final String JDBC_DB_CLASS     = "dbclass";

  /**
   * The name of pool to look up
   * value: lookup_name
   */
  public static final String JDBC_USE_POOL  = "usepool";

	/**
	 * Parameter defines type of  connection (e.g. "sqlj" or "jdbc" )
	 * value: sqlj
	 */
  public static final String JDBC_CONNECTIONTYPE = "connectiontype";

}
