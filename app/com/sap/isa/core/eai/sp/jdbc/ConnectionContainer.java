package com.sap.isa.core.eai.sp.jdbc;

import java.sql.Connection;

import sqlj.runtime.ConnectionContext;


/**
 * Stores connection of type sql.Connection and sqlj.runtime.ConnectionContext
 */
public class ConnectionContainer {
	
	private Connection mSQLCon;
	private ConnectionContext mConContext;
	
	public ConnectionContainer(Connection sqlCon, ConnectionContext conContext) {
		mSQLCon = sqlCon;
		mConContext = conContext;
	}

	public Connection getSQLConnection() {
		return mSQLCon;
	}
	
	public ConnectionContext getConnectionContext() {
		return mConContext;
	}

}
