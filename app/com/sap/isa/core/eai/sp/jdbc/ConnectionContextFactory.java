package com.sap.isa.core.eai.sp.jdbc;

import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.sqlj.CrmJdbcPooled;
import com.sap.isa.core.sqlj.CrmJdbcStandalone;
import com.sap.isa.core.util.MiscUtil;
//import com.sap.sql.connect.OpenSQLDataSource;


import sqlj.runtime.ConnectionContext;

/**
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 * @depreceated This class is not used since CRM 5.0. Only Pooled connectiosn are supported
 */
public class ConnectionContextFactory { 
	protected static IsaLocation log = IsaLocation.
		  getInstance(ConnectionContextFactory.class.getName());


  
	static ConnectionContext getPooledContext() throws BackendException {
	  ConnectionContext con = null;
	  try {
		  if (log.isDebugEnabled())
		   log.debug("requesting pooled connection context");
	   	
		 con = new CrmJdbcPooled();
	   } catch (SQLException ex) {
		   String msg = "Could not create pooled SQLJ connection context for [pool]='jdbc/crmjdbcpool'";
		   log.error("system.eai.exception", new Object[] { msg }, ex);
		   throw new BackendException(ex);
	   }
	   return con;  	
	}
    /**
     * 
     * @param props: Jdbc connection parameter
     * @return SQLJ ConnectionContext
     * @throws BackendException
     */
	static ConnectionContext getDirectConnectionContext(Properties props) throws BackendException {
	  if (log.isDebugEnabled())
		log.debug("Getting direct SQLJ connection context");
	  try {
		  return new CrmJdbcStandalone(getDirectOpenSqlCon(props));
	   } catch (SQLException ex) {
	  	  String user = props.getProperty(JDBCConstants.JDBC_USER);
		  String password = props.getProperty(JDBCConstants.JDBC_PASSWORD);
		  String dburl = props.getProperty(JDBCConstants.JDBC_URL);

		  String msg = "could not establish database connection [dburl]='" + dburl +
					   "' [user]='" + user + "' [password]=' " +
					   (password == null ? "missing" : "? [errormsg]='" + ex.toString() + "'");
		   log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.eai.exception", new Object[] { msg }, ex);
		   throw new BackendException(ex);
	   }
	} 
	
	/**
	 * Returns the Connection Context
     * @throws BackendException
	 */
	public static ConnectionContext getConnectionContext(Properties props) throws BackendException {
	  if (log.isDebugEnabled())
		log.debug("Connection context requested");
	  // check if connnection has to be retrieved from a pool

	  String usePool = props.getProperty(JDBCConstants.JDBC_USE_POOL);
	  ConnectionContext con = null;

	  if (usePool != null && usePool.equalsIgnoreCase("true")) {
		  con = getPooledContext();	
	  } else if (usePool == null){
			con = getPooledContext();
	  } else  {
		con = getDirectConnectionContext(props);
	  }
	  return con;
	}
	
	/**
	 * Opens a JDBCConnection
     * @param props: jdbc connection parameter (url, user, password ...)
     * @return Open/SQL jdbc connection to maxdb database
     */
	  static Connection getDirectOpenSqlCon(Properties props) throws BackendException {
	  String dbclass = props.getProperty(JDBCConstants.JDBC_DB_CLASS);
	  String dburl = props.getProperty(JDBCConstants.JDBC_URL);
	  String user = props.getProperty(JDBCConstants.JDBC_USER);
	  String password = props.getProperty(JDBCConstants.JDBC_PASSWORD);
	  try {
	  	Class osdsClass = Class.forName("com.sap.sql.connect.OpenSQLDataSource");
		  Method newInstance= osdsClass.getDeclaredMethod("newInstance",null);
		  Object osds = newInstance.invoke(null,null);
		  MiscUtil.callMethod(osds,"setDriverProperties", 
		  	new Class[] {String.class,String.class,String.class,String.class},
		  	new Object[]{dbclass,          
				dburl, 
				"SAPMAXDB",                                 
				"SAP"}
			);
		  //	osds.setConnectionType(OpenSQLDataSource.CONN_TYPE_OPEN_SQL);
		  MiscUtil.callMethod(osds,"setConnectionType", new Class[]{int.class}, new Object[]{new Integer(1)});
		  return (Connection)MiscUtil.callMethod(osds, "getConnection", new Class[]{String.class,String.class}, new Object[]{user,password});
		  /*	 Same code without reflection:
		      OpenSQLDataSource osds= OpenSQLDataSource.newInstance();
		  osds.setDriverProperties(dbclass,          
				dburl, 
				"SAPMAXDB",                                 
				"SAP");
		  osds.setConnectionType(OpenSQLDataSource.CONN_TYPE_OPEN_SQL);
		  Connection con=osds.getConnection(user,password);
		  return con; 
		  */
	  }
	  	  catch (Exception ex) {
		   String msg = "could not establish database connection [dburl]='" + dburl +
					   "' [user]='" + user + "' [password]=' " +
					   (password == null ? "missing" : "? [errormsg]='" + ex.toString() + "'");
		   log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.eai.exception", new Object[] { msg }, ex);
		   throw new BackendException(ex);
	  }
  }
}
