package com.sap.isa.core.eai.sp.ipc;

import com.sap.spc.remote.client.ClientLogEvent;
import com.sap.spc.remote.client.ClientLogListener;
import com.sap.util.monitor.jarm.IMonitor;
import com.sap.isa.core.Constants;
import com.sap.isa.core.SharedConst;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.IsaSession;
import com.sap.isa.core.sat.jarm.ISASATCheck;
import com.sap.isa.core.util.VersionGet;

/**
 * Listens to IPC client logs and logs them using the ISA log API
 */
public class IsaClientLogListener implements ClientLogListener
{
	private long tActivate;

	private IsaLocation log;
	private IsaLocation isaRuntimeTracing;
	private String mSessionID = IsaSession.getThreadSessionId();
	private boolean mIsSATOn = ISASATCheck.isJarmOn(mSessionID);

	private static final String UNKNOWN = "unknown";

	public IsaClientLogListener() {
		log = IsaLocation.getInstance("com.sap.spc.remote.client.object.IPCClient");
	    isaRuntimeTracing = IsaLocation.getInstance(Constants.TRACING_RUNTIME_CATEGORY);
		tActivate = -1;
	}

	public void activated(ClientLogEvent event) {
		tActivate = System.currentTimeMillis();
		if (isaRuntimeTracing.isDebugEnabled()) {
			String logString = getLogString(event.getSource(), false, event.getMessage(), -1);
			isaRuntimeTracing.debug(logString);
			IMonitor monitor = (IMonitor)IsaSession.getAttribute(SharedConst.SAT_MONITOR);
			if (monitor != null) {
				String compName = getIPCComponentName(event.getMessage());
				if (compName != null) {
					monitor.startComponent(compName);
					monitor.compAction(compName, logString);
					monitor.endComponent(compName);
				}
			}

		}
		if (log.isDebugEnabled()) {
			log.debug("request: "+event.getMessage());
		}
	}

	public void requestSent(ClientLogEvent event) {
		// no need to log
	}

	public void responseReceived(ClientLogEvent event) {
		long tResponse;
		if (tActivate != -1) {
			tResponse = System.currentTimeMillis()-tActivate;
		}
		else {
			// should never happen
			tResponse = -1;
		}
		if (isaRuntimeTracing.isDebugEnabled()) {
			String logString = getLogString(event.getSource(), true, event.getMessage(), tResponse);
			isaRuntimeTracing.debug(logString);
		}
		if (log.isDebugEnabled()) {
			log.debug("response: "+event.getMessage());
		}
	}

	private String getLogString(Object source, boolean isEnd, String message, long responseTime) {
		StringBuffer b = new StringBuffer();
		b.append("[ipccommandexecution]=");
		b.append(isEnd ? "'end'" : "'begin'");
		b.append(" ");
		if (message != null) {
			b.append("[cmdname]='");
			// the client event's message doesn't separate between command and parameters,
			// so we have to separate now
			int commandEnd = message.indexOf(':');
			if (commandEnd != -1) {
			    b.append(message.substring(0, commandEnd));
			}
			else {
				b.append("unknown");
			}
			b.append("'");
		}
		String id;
		String host;
		String port;
		if (source instanceof com.sap.spc.remote.client.AbsClientSupport) {
			com.sap.spc.remote.client.AbsClientSupport clientSupport =
			    (com.sap.spc.remote.client.AbsClientSupport)source;
			id = clientSupport.getSessionId();

			// the basic communication interface does not define implementation details like
			// host and port, so we have to ask implementations
			if (source instanceof com.sap.spc.remote.client.tcp.Client) {
				com.sap.spc.remote.client.tcp.Client client = (com.sap.spc.remote.client.tcp.Client)source;
			    host = client.getHost();
				port = Integer.toString(client.getPort());
			}
			else if (source instanceof com.sap.spc.remote.client.tcp.TcpClientSupport) {
				com.sap.spc.remote.client.tcp.TcpClientSupport client = (com.sap.spc.remote.client.tcp.TcpClientSupport)source;
				host = client.getHost();
				port = Integer.toString(client.getPort());
			}
			else {
				host = null;
				port = null;
			}
			if (host != null) {
				b.append(" [host]='");
				b.append(host);
				b.append("' [port]='");
				b.append(port);
				b.append("'");
			}
			if (id != null) {
				b.append(" [session]='");
				b.append(id);
				b.append("'");
			}
		}

		if (responseTime != -1) {
			b.append(" [exectime]='");
			b.append(responseTime);
			b.append("'");
		}

		return b.toString();
	}


	public void closed(ClientLogEvent event) {
		String id = UNKNOWN;
		Object source = event.getSource();
		if (source instanceof com.sap.spc.remote.client.AbsClientSupport) {
			com.sap.spc.remote.client.AbsClientSupport clientSupport =
			    (com.sap.spc.remote.client.AbsClientSupport)source;
			id = clientSupport.getSessionId();
		}

		if (isaRuntimeTracing.isDebugEnabled()) {
			isaRuntimeTracing.debug("[ipclogoff] [session]='"+id+"'");
		}
		if (log.isDebugEnabled()) {
			log.debug("session closed: "+id);
		}
	}

	public void exceptionThrown(ClientLogEvent event) {
		Throwable e = event.getException();
		log.error("ipc.exception", new String[] { e.getMessage() }, e);
	}

	private String getIPCComponentName(String message) {
		String compName = VersionGet.getSATRequestPrefix();
		if (message != null) {
			int commandEnd = message.indexOf(':');
			if (commandEnd != -1)
				return compName + message.substring(0, commandEnd);
			else
				return compName + UNKNOWN;
		}
		return compName + UNKNOWN;
	}
}
