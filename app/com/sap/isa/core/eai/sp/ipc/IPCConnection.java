/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      14 May 2001
  $Id: //java/sapm/esales/30/src/com/sap/isa/core/eai/sp/ipc/IPCConnection.java#1 $
  $Revision: #1 $
  $Change: 22942 $
  $DateTime: 2001/06/26 10:29:54 $
  $Author: d028486 $
*****************************************************************************/
package com.sap.isa.core.eai.sp.ipc;

import com.sap.spc.remote.client.object.IClient;
//import com.sap.spc.remote.client.object.imp.CachingClient;
import com.sap.isa.core.eai.Connection;

public interface IPCConnection extends Connection {

  /**
   * Returns an instance of a IPC Client
   * @return IPCClient instance
   */
  public IClient getClient();

  /**
   * Gets the name of connection factory used to create the IPC connection
   * @return Gets the name of connection factory
   */
  public String getConnectionFactoryName();

  /**
   * Gets the name of connection configuration used to create the
   * IPC connection
   * @return name of connection configuration
   */
  public String getConnectionConfigName();

}