package com.sap.isa.core.eai.sp.ipc;

/**
 * Global constants needed by IPC to define connection parameters and such.
 */
public interface IPCConstants {
  /**
   * SAP Client (MANDT)
   */
  public static final String CLIENT   = "client";

  /**
   * Type (e.g. "ISA", "CRM")
   */
  public static final String TYPE     = "type";

  /**
   * TCP/IP host
   */
  public static final String HOST     = "host";

  /**
   * TCP/IP port
   */
  public static final String PORT     = "port";

  /**
   * Unicode character encoding
   */
  public static final String ENCODING = "encoding";

  /**
   * Should we use a dispatcher
   */
  public static final String USE_DISPATCHER = "dispatcher";

  /**
   * The default port of IPC servers
   */
  public static final int DEFAULT_PORT = 4444;

  /**
   * The IPC "type" for internet sales
   */
  public static final String ISA_TYPE = "ISA";

  /**
   * tells if datasource is CRM or R3
   */
  public static final String DATA_SOURCE= "datasource";
  
  /**
   * security.level
   */
  public static final String SECURITY_LEVEL = "security.level";
  
  /**
   * security.ssl.keystoreType
   */
  public static final String SECURITY_SSL_KEYSTORETYPE = "security.ssl.keystoreType";
  
  /**
   * security.ssl.keystoreLocation
   */
  public static final String SECURITY_SSL_KEYSTORELOCATION = "security.ssl.keystoreLocation";
  
  /**
   * security.ssl.keystorePassword
   */
  public static final String SECURITY_SSL_KEYSTOREPASSWORD = "security.ssl.keystorePassword";
  
  /**
   * security.ssl.keyAlias
   */
  public static final String SECURITY_SSL_KEYALIAS = "security.ssl.keyAlias";
  
  /**
   * security.ssl.keyPassword
   */
  public static final String SECURITY_SSL_KEYPASSWORD = "security.ssl.keyPassword";
  
}
