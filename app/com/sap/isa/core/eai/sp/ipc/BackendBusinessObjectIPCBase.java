/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      14 May 2001
  $Id: //java/sapm/esales/30/src/com/sap/isa/core/eai/sp/ipc/BackendBusinessObjectIPCBase.java#1 $
  $Revision: #1 $
  $Change: 22942 $
  $DateTime: 2001/06/26 10:29:54 $
  $Author: d028486 $
*****************************************************************************/

package com.sap.isa.core.eai.sp.ipc;

import java.util.Properties;

import com.sap.isa.core.PanicException;
import com.sap.isa.core.eai.BackendBusinessObjectBase;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.spc.remote.client.object.IClient;
import com.sap.spc.remote.client.object.IPCBOManagerBase;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItemReference;
import com.sap.spc.remote.client.object.IPCSession;
import com.sap.spc.remote.client.object.imp.DefaultIPCClient;
import com.sap.spc.remote.client.object.imp.IPCClientObjectFactory;

/**
 * This is the base class for all backend objects which use the Internet
 * Pricing Configurator (IPC)
 */

public class BackendBusinessObjectIPCBase extends BackendBusinessObjectBase
                                          implements BackendBusinessObjectIPC {

  protected static IsaLocation log = IsaLocation.
        getInstance(BackendBusinessObjectIPCBase.class.getName());

	public static final String IPC_BO_MANAGER = "ipcBoManager";

	protected IPCClient defaultClient = null;
	protected String    mandt;
  protected String source = SOURCE_CRM;

  public BackendBusinessObjectIPCBase() {
  }

  public String getSource(){
    return source;
  }

  public IPCItemReference getDefaultItemReference() {
    if (defaultClient == null) {
       defaultClient = getDefaultIPCClient();
    }
    return defaultClient.getItemReference();
  }


  /**
	 * Returns a IPC Client which uses the passed connection key as specified in the
	 * external configuration file to connect to the IPC Server. In order to get
	 * the IPCClient you have to provide the SAP Client (mandant). You can do this by
	 * either calling <code>setDefaultSAPClient(String sapClient)</code>
	 * or by providing the backend business object with the SAP client using
	 * a parameter in the configuration file. Please use the following
	 * syntax:
	 * <pre>
	 * <param name="client" value="???"/>
	 * e.g.
	 * <param name="client" value="805"/>
	 * <pre>
	 * @return IPCClient either a valid IPCClient or <code>null</code>
	 */
	public IPCClient getDefaultIPCClient(String connectionKey) {

		IPCClient ipcClient = null; 
		IPCBOManagerBase ipcBOManager = (IPCBOManagerBase)context.getAttribute(IPC_BO_MANAGER);
		
		if (ipcBOManager == null) { 
			throw new PanicException("IPC BO Manager is not in the backend context");
		}
		if (connectionKey == null || "".equals(connectionKey)) {
			ipcClient = ipcBOManager.createIPCClient();
		}
		else  {	
			ipcClient = ipcBOManager.createIPCClientUsingConnection(connectionKey);
		}
		
		return ipcClient;
	  }
  		
  
  /**
   * Returns a IPC Client which uses the passed connection key as specified in the
   * external configuration file to connect to the IPC Server. In order to get
   * the IPCClient you have to provide the SAP Client (mandant). You can do this by
   * either calling <code>setDefaultSAPClient(String sapClient)</code>
   * or by providing the backend business object with the SAP client using
   * a parameter in the configuration file. Please use the following
   * syntax:
   * <pre>
   * <param name="client" value="???"/>
   * e.g.
   * <param name="client" value="805"/>
   * <pre>
   * @return IPCClient either a valid IPCClient or <code>null</code>
   */
  public IPCClient getDefaultIPCClient() {
    return getDefaultIPCClient("");
  }
  
  /**
   * This method is called after the IPC backend object has been instantiated.
   * The defualt implementation of this method checks if a parameter named
   * <code>IPCConstants.CLIENT</code> is specified in the configuration file.
   * If this is the case then the value of this parameter is stored. It is used
   * to create the default IPCClient.
   * Important: If you overwrite this method do not forget to call the method
   *            of the base class.
   * @return the IPCReference for the default IPCClient
   */
  public void initBackendObject(Properties props, BackendBusinessObjectParams params) throws BackendException {
    mandt = props.getProperty(IPCConstants.CLIENT);

    //CHHI 2001/10/31
    //set IPC data source
    String dataSource = props.getProperty(IPCConstants.DATA_SOURCE);
    if (dataSource != null && dataSource.toLowerCase().equals(SOURCE_R3))
      source = SOURCE_R3;
    else source = SOURCE_CRM;
  }

  /**
   * Sets the default SAP Client (manadant) which has to be used to create
   * the default IPC Client
   * @param sapClient
   */
  public void setDefaultSAPClient(String sapClient) {
    mandt = sapClient;
  }

  /**
   * @deprecated
   * Returns a IPC connection which is a modified default connection. This is
   * a convenience function which allows to getting modified default connections
   * quickly
   * @param conDefParams connection parameters which overwrite/extend the connection
   *    parameters of the default connection
   * @return a IPC connection
   * @throws BackendException if creating IPCClient fails
   */
   public IPCClient getModDefaultIPCClient(Properties props) throws BackendException {

   IPCConnection ipcConnection = (IPCConnection)conFactory.getConnection(
          ((IPCConnection)conFactory.getDefaultConnection()).getConnectionFactoryName(),
          ((IPCConnection)conFactory.getDefaultConnection()).getConnectionConfigName(),
          props);
    IPCClient ipcClient;
    try {
      ipcClient = initClient(ipcConnection);
    } catch (IPCException ex) {
      throw new BackendException (ex.toString());
    }
    return ipcClient;
  }

  protected IPCClient initClient(IPCConnection connection) throws IPCException {
    if (mandt == null || connection == null)
      return null;

    IClient cachingClient = connection.getClient();
    //DefaultIPCClient client = new DefaultIPCClient(mandt, IPCConstants.ISA_TYPE, cachingClient);
	DefaultIPCClient client = (DefaultIPCClient)IPCClientObjectFactory.getInstance().newIPCClient(mandt, IPCConstants.ISA_TYPE, cachingClient);
	
    client.addLogListener(new IsaClientLogListener());
    return client;
  }

  public void destroyBackendObject() {
		if (defaultClient != null) {
			try {
				IPCSession ipcsession = defaultClient.getIPCSession();
				if (ipcsession != null)  {        // we still have an ipc session
					ipcsession.close();            // close this session to free tcp/ip socket & memory
				}
			} catch (Exception ex) {
                            String msg = "Error closing IPC session";
                            log.error("system.eai.exception", new Object[] { msg }, ex);

			} finally {
				defaultClient = null;
			}
		}


  }

}
