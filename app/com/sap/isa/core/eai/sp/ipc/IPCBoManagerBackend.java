/*
 * Created on 03.08.2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.core.eai.sp.ipc;

import java.util.Properties;

import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.spc.remote.client.object.imp.rfc.IPCClientBackendBOParams;


/**
 * @author 
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class IPCBoManagerBackend extends BackendBusinessObjectIPCBase {
	
	
	

	/* (non-Javadoc)
	 * @see com.sap.isa.core.eai.BackendBusinessObject#initBackendObject(java.util.Properties, com.sap.isa.core.eai.BackendBusinessObjectParams)
	 */
	public void initBackendObject(Properties props,
								  BackendBusinessObjectParams params)
			throws BackendException {
				
		IPCClientBackendBOParams IPCparams = (IPCClientBackendBOParams)params;
		getContext().setAttribute(IPC_BO_MANAGER, IPCparams.getIPCBOManager());				
				
	}



}
