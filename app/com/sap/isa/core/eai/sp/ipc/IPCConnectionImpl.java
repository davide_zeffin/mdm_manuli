package com.sap.isa.core.eai.sp.ipc;

/**
 * Title:        Enterprse Application Integration
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      SAPMarkets
 * @author Marek Barwicki
 * @version 1.0
 */

import java.util.Properties;

import com.sap.spc.remote.client.object.IClient;
//import com.sap.spc.remote.client.object.imp.CachingClient;
import com.sap.spc.remote.client.object.imp.tcp.TCPDefaultClient;
import com.sap.spc.remote.client.ClientException;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.ConnectionEventListener;
import com.sap.isa.core.eai.ManagedConnectionBase;
import com.sap.isa.core.logging.IsaLocation;

public class IPCConnectionImpl  extends ManagedConnectionBase
                                implements IPCConnection, IPCConstants {

protected static IsaLocation log = IsaLocation.
	  getInstance(IPCConnectionImpl.class.getName());


  protected IClient mClient;
  protected Properties mClientProps;  

  public IPCConnectionImpl() {
  }

  public IClient getClient() {
    return getCachingClient();
  }

  public void close() {
    if  (getCachingClient() != null) {
		((TCPDefaultClient)getCachingClient()).close();
		mClient = null;
    }
  }
  public void destroy() {
    if  (getCachingClient() != null) {
		((TCPDefaultClient)getCachingClient()).close();
		mClient = null;
    }
  }

  public void cleanup() {
    if  (getCachingClient() != null) {
		((TCPDefaultClient)getCachingClient()).close();
		mClient = null;
    }
  }

  public String getConnectionKey() {
    return null;
  }

  public boolean isValid() {
    /** @todo: server ping or something like this */
    return true;
  }

  /**
   * This method is called after creating the managed connection. It can
   * be used to initialize the managed connection
   * param initParams
   */

  public void init(Object initParams) {
    //this.client = (CachingClient)initParams;
	mClientProps = (Properties)initParams;
  }

  /**
   * Allocates a physical connection to this managed connection
   * @connection physical connection
   */
  public void associateConnection(Object connection) {

  }

  /**
   * Sets the name of connection factory which is used to create the
   * physical connection
   * @param conFactoryName name of connection factory
   */
  public void setConnectionFactoryName(String conFactoryName) {
    this.conFactoryName = conFactoryName;
  }

  /**
   * Gets the name of connection factory used to create the physical
   * connection
   * @return Gets the name of connection factory
   */
  public String getConnectionFactoryName() {
    return conFactoryName;
  }

  /**
   * This methis is not implemented yet
   * @param aListener A listener which will be notofied if connection events occur
   */
  public void addConnectionEventListener(ConnectionEventListener aListener) {
  }

 
  protected IClient getCachingClient()  {
  	
  	if (mClient == null) {
  		try {
  			mClient = createIPCCachingClient(mClientProps);
  		} catch (BackendException ex) {
  			String msg = "Could not create IPC managed connection with the class name '" +
			"\n[props]='" + mClientProps + "'\n" + ex.toString();
			log.error("system.eai.exception", new Object[] { msg }, null);
			return null;
  		}
  	}
  	return mClient;
  }

  protected IClient createIPCCachingClient(Properties props) throws BackendException {

	  String host = props.getProperty(HOST);
	  String portString = props.getProperty(PORT);
	  int port;
	  try {
		port = Integer.parseInt(portString);
	  }
	  catch(NumberFormatException e) {
		port = DEFAULT_PORT;
		if (log.isDebugEnabled())
		  log.debug("Port \"" + portString + "\" is no integer. Using default port for IPC connection");
	  }
	  String encoding = props.getProperty(ENCODING);

	  String dispatcherString = props.getProperty(USE_DISPATCHER);
	  // default: use dispatcher
	  boolean viaDispatcher = (dispatcherString == null || dispatcherString.equalsIgnoreCase("true"));

	  IClient client = null;
	  try {
		client = new TCPDefaultClient(host, port, encoding, new Boolean(viaDispatcher));
	  } catch (ClientException ex) {
		  throw new BackendException (ex.toString());
	  }
	return client;
  }


}