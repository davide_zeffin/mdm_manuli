/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      14 May 2001
  $Id: //java/sapm/esales/30/src/com/sap/isa/core/eai/sp/ipc/BackendBusinessObjectIPC.java#1 $
  $Revision: #1 $
  $Change: 22942 $
  $DateTime: 2001/06/26 10:29:54 $
  $Author: d028486 $
*****************************************************************************/
package com.sap.isa.core.eai.sp.ipc;

import java.util.Properties;

import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCItemReference;
import com.sap.isa.core.eai.BackendBusinessObject;
import com.sap.isa.core.eai.BackendException;

/**
 * This interface should be implemented by Backend Business Object
 * wich communicate with the IPC
 */

public interface BackendBusinessObjectIPC extends BackendBusinessObject {

    public final static String SOURCE_CRM = "crm";
    public final static String SOURCE_R3 = "r3";

    /**
     * Returns a IPC Client which uses default connection settings as specified in the
     * external configuration file to connect to the IPC Server. In order to get
     * the IPCClient you have to provide the SAP Client (mandant). You can do this by
     * either calling <code>setDefaultSAPClient(String sapClient)</code>
     * or by providing the backend business object with the SAP-client using
     * a parameter in the configuration file. Please use the following
     * syntax:
     * <pre>
     * <param name="client" value="???"/>
     * e.g.
     * <param name="client" value="805"/>
     * <pre>
     * @return IPCClient
     */
    public IPCClient getDefaultIPCClient();
    /**
     * Returns the IPCReference for the default IPCClient
     * @return the IPCReference for the default IPCClient
     */
    public IPCItemReference getDefaultItemReference();
    /**
     * Sets the default SAP Client (manadant) which has to be used to create
     * the default IPC Client
     * @param sapClient
     */
    public void setDefaultSAPClient(String sapClient);
    /**
     * @deprecated
     * Returns an IPCClient which uses settings based on the settings for the
     * default IPCClient. The passed set of properties extends/overwrites the
     * settings specified for the default IPCClient. The passed properties
     * should also contain the SAP-client parameter (mandant)
     * @param props A set of properties which overwrites/extends the properties
     *        specified for the defualt IPCClient for this backend object
     * @return IPCClient
     */
    public IPCClient getModDefaultIPCClient(Properties props) throws BackendException ;


	/**
	 *  Sets the Source attribute of the BackendBusinessObjectIPC object. <p>
         *  CHHI 2001/10/31 tells if IPC's data source is CRM or R/3. That is
         *  needed since header attributes have to be passed to IPC differently.
	 *
	 *@return  the Source value
	 */
	public String getSource();
}
