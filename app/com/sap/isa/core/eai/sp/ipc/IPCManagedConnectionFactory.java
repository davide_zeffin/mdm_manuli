/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      15 February 2001
*****************************************************************************/
package com.sap.isa.core.eai.sp.ipc;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

//import com.sap.spc.remote.client.object.imp.CachingClient;
//import com.sap.sxe.socket.client.Client;
//import com.sap.sxe.socket.client.ClientException;
import com.sap.spc.remote.client.tcp.Client;
import com.sap.spc.remote.client.ClientException;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.ConnectionConfig;
import com.sap.isa.core.eai.ConnectionDefinition;
import com.sap.isa.core.eai.ManagedConnection;
import com.sap.isa.core.eai.ManagedConnectionFactory;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.security.PasswordEncrypt;
import com.sap.isa.core.security.SecureStore;

/**
 * This is a specific Connection Manager for JCo connections
 */
public class IPCManagedConnectionFactory implements ManagedConnectionFactory, IPCConstants {

  protected static IsaLocation log = IsaLocation.
        getInstance(IPCManagedConnectionFactory.class.getName());

  protected Properties conFacProps;
  protected Map conConfigs = new HashMap();
  protected Map conDefs;
  protected String factoryName;

  /**
   * Constructor
   */
  public IPCManagedConnectionFactory() {}


  /**
   * Specifies the name of the factory
   * @param factoryName type of factory
   */
  public void setFactoryName(String factoryName) {
    this.factoryName = factoryName;
  }

  /**
   * Gets the type of the factory
   * @return type of factory
   */
  public String getFactoryName() {
    return factoryName;
  }



  /**
   * Gets a managed connection
   * @param conConfigName name of connection configuration which should be
   *    used to create the managed connection
   * @param conDefProps connection definition properties
   * @return a managed connection
   */
  public ManagedConnection getManagedConnection(String conConfigName, Properties conDefProps)
              throws BackendException {
    if (log.isDebugEnabled()) {
      log.debug("getManagedConnection(String, Properties) entered; conConfigName = \"" + conConfigName
            + "\" conDef props = \"" + conDefProps.toString() + "\"");
    }

    ConnectionConfig conConfig = (ConnectionConfig)conConfigs.get(conConfigName);

    if (conConfig == null) {
      String msg = "Connection named '" + conConfigName + "' not found";
      log.error("system.eai.exception", new Object[] { msg }, null);
      throw new BackendException(msg);
    }

    String conDef = conConfig.getConnectionDefinition();
    ConnectionDefinition def = (ConnectionDefinition)conDefs.get(conDef);

    if (def == null) {
      String msg = "Connection Definition named '" + conDef + "' not found";
      log.error("system.eai.exception", new Object[] { msg }, null);
      throw new BackendException(msg);
    }



    Properties existingProps = def.getProperties();

    Properties newMixedProps = (Properties)existingProps.clone();

    Enumeration passedPropNamesEnum = conDefProps.propertyNames();

    String propName = null;
    while (passedPropNamesEnum.hasMoreElements()) {
      propName = (String)passedPropNamesEnum.nextElement();
      newMixedProps.put(propName, conDefProps.getProperty(propName));
    }

    if (log.isDebugEnabled()) {
      log.debug("mixed connection properties: " + newMixedProps.toString());
    }

    return createManagedConnection(conConfigName, newMixedProps);
  }

  /**
   * Gets a managed connection
   * @param conConfigName name of connection configuration which should be
   *    used to create the managed connection
   * @return a managed connection
   */
  public ManagedConnection getManagedConnection(String conConfigName)
              throws BackendException {
    if (log.isDebugEnabled())
      log.debug("getManagedConnection(String) entered");

      ConnectionConfig conConfig = (ConnectionConfig)conConfigs.get(conConfigName);
      if (conConfig == null) {
        String msg = "Connection named '" + conConfigName + "' not found";
        log.error("system.eai.exception", new Object[] { msg }, null);
        throw new BackendException(msg);
      }


      String conDef = conConfig.getConnectionDefinition();
      ConnectionDefinition def = (ConnectionDefinition)conDefs.get(conDef);
      if (def == null) {
        String msg = "Connection Definition named '" + conDef + "' not found";
        log.error("system.eai.exception", new Object[] { msg }, null);
        throw new BackendException(msg);
      }


      Properties props = def.getProperties();

      return createManagedConnection(conConfigName, props);
    }

  /**
   * Inititalizes a managed connection factory
   * @param conDefs A Map containing backend connection definitions
   */
  public void initConnections(Map conDefs)
          throws BackendException {
    this.conDefs = new HashMap(conDefs);

    log.info("system.eai.ipc.init.con");

    if (conDefs == null || conDefs.size() == 0)
      return;

    Iterator conDefIterator = conDefs.values().iterator();
    ConnectionDefinition conDef = null;
    while (conDefIterator.hasNext()) {
        conDef = (ConnectionDefinition)conDefIterator.next();

        log.info("system.eai.ipc.init.ProcessingConDef", new Object[] { conDef.getName() }, null);

        this.conDefs.put(conDef.getName(), conDef);
      }
  }

  /**
   * Adds a connection configurations to the connection factory
   * @param a set of connection configurations
   */
  public void addConnectionConfigs(Map conConfigs) {
    //this.conConfigs = conConfigs;
    this.conConfigs.putAll(conConfigs);
  }

  /**
   * Not implemented for IPC
   * @return null
   */
  public Object getConnection(String conDefName) throws BackendException {
    return null;
  }

  /**
   * Not implemented for IPC
   * @return null
   */
  public Object getConnection(String conDefName, Properties conDefProps)
        throws BackendException {
      return null;
  }

  /**
   * Not implemented for IPC
   * @return false
   */
  public boolean isConnectionAvailable(ConnectionDefinition conDef) {
      return false;
  }

  protected ManagedConnection createManagedConnection(String conConfigName, Properties props)
    throws BackendException {

    ConnectionConfig conConfig = (ConnectionConfig)conConfigs.get(conConfigName);
    ConnectionDefinition conDef = (ConnectionDefinition)conDefs.get(conConfig.getConnectionDefinition());
    String conDefName = conConfig.getConnectionDefinition();

    ManagedConnection managedConnection;


    try {
      Class ipcManagedConnectionClass = Class.forName(conConfig.getClassName());
      managedConnection =
          (ManagedConnection)ipcManagedConnectionClass.newInstance();

      } catch (Exception ex) {
        throw new BackendException("Could not create managed connection with the class name '"
              + conConfig.getName() + "\n" + ex.toString());
      }

    // set connection config name
    managedConnection.setConnectionDefinitionName(conDefName);
    managedConnection.setConnectionConfigName(conConfigName);
    managedConnection.setConnectionFactoryName(factoryName);
    managedConnection.setManagedConnectionFactory(this);

    //managedConnection.init(createIPCCachingClient(props));
    // switched creation of ipc client to lazy creation
	managedConnection.init(props);
	getSecurityProps(props);
      return managedConnection;
  }

  /**
   * Sets a set of properties
   * @param props Properties which can be used for additional configuratio
   *        of a factory
   */
  public void setProperties(Properties props) {
    conFacProps = (Properties)props.clone();
  }

    /**
   * This method can be used to check whether to passed set of properties is
   * sufficient in order to create a connection to a SAP system
   * @param props connection properties
   * @return true if connection could be established, otherwise false
   */
  public static String testConnection(Properties props) throws BackendException {
    StringBuffer retVal = new StringBuffer();
	Client client = null;
    try {
      String host = props.getProperty(IPCConstants.HOST);
      String portString = props.getProperty(IPCConstants.PORT);
      int port = Integer.parseInt(portString);
      Boolean isDispatcher = new Boolean(props.getProperty(IPCConstants.USE_DISPATCHER));
      String encoding = props.getProperty(IPCConstants.ENCODING);
      retVal.append("\nhost:        ").append(host).append("\n");
      retVal.append("port:        ").append(portString).append("\n");
      retVal.append("encoding:    ").append(encoding).append("\n");

      boolean ipcIsAlive = false;
      client = new Client(host, port, encoding, isDispatcher, null);
      // broadcast
      client.cmd("Ping", new String[0]);

      return retVal.toString();
    }
    catch(RuntimeException e) {
      retVal.append("\n" + e.toString());
      throw new BackendException(retVal.toString());
    }
    catch(ClientException e) {
      retVal.append("\n" + e.toString());
      throw new BackendException(retVal.toString());
    } finally {
    	if (client != null)
			client.close();
    }
  }
  
  /**
   * This is a convenience method. Pass IPC connection data obtained from application configuration (XCM)
   * to this method. The method analyses if secure connection is turned on and returns the 
   * appropriate properties which can be directly passed to the ipc caching client constructor.   
   * If needed some values are decrypted or retrieved from secure storage
   * @param props IPC connection parameter obtained from XCM
   * @return  appropriate properties which can be directly passed to the ipc caching client constructor
   */
  public static Properties getSecurityProps(Map props) {
  		Properties secProps = new Properties();
		secProps.setProperty(IPCConstants.SECURITY_LEVEL, "0");
		
  		if (props == null) {
			return secProps;
  		}
  		
		String level = (String)props.get(IPCConstants.SECURITY_LEVEL);
		
		if (level == null || level.equals("0"))
			return secProps;
		
		String keystoreType = (String)props.get(IPCConstants.SECURITY_SSL_KEYSTORETYPE);
		String keystoreLocation = (String)props.get(IPCConstants.SECURITY_SSL_KEYSTORELOCATION);
		String keystorePassword = (String)props.get(IPCConstants.SECURITY_SSL_KEYSTOREPASSWORD);
		String keyAlias = (String)props.get(IPCConstants.SECURITY_SSL_KEYALIAS);
		String keyPassword = (String)props.get(IPCConstants.SECURITY_SSL_KEYPASSWORD);
		keystorePassword = decryptPassword(keystorePassword);
		keyPassword = decryptPassword(keyPassword);
	
		secProps.setProperty(IPCConstants.SECURITY_LEVEL, level);
		secProps.setProperty(IPCConstants.SECURITY_SSL_KEYSTORETYPE, keystoreType);
		secProps.setProperty(IPCConstants.SECURITY_SSL_KEYSTORELOCATION, keystoreLocation);
		secProps.setProperty(IPCConstants.SECURITY_SSL_KEYSTOREPASSWORD, keystorePassword);
		secProps.setProperty(IPCConstants.SECURITY_SSL_KEYALIAS, keyAlias);
		secProps.setProperty(IPCConstants.SECURITY_SSL_KEYPASSWORD, keyPassword); 
		
		return secProps;  	
  }
  
  private static String decryptPassword(String password) {
	  if (PasswordEncrypt.isSecureStorage(password))
		  return SecureStore.retrieve(password);
	  else
		  return PasswordEncrypt.decryptStringAsHex(password);		
  }

}

