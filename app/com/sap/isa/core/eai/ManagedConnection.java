/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      15 February 2001
*****************************************************************************/
package com.sap.isa.core.eai;

/**
 * Interface which has to be implemented by all connections which are
 * managed by the connection manager
 */
public interface ManagedConnection {

  /**
   * Sets a reference to a backend specific connection factory
   * @param connectionFactory reference to backend connection manager
   */
  public void setManagedConnectionFactory(ManagedConnectionFactory connectionFactory);

  /**
   * Sets name of connection Definition used to create connection
   * @param conDefName Name of connection definition used
   *    to create this connection
   */
  public void setConnectionDefinitionName(String conDefName);

  /**
   * Returns name of connection cofiguration used to create this connection
   * @return name of connection cofiguration used to create this connection
   */
  public String getConnectionDefinitionName();

  /**
   * This method is called after creating the managed connection. It can
   * be used to initialize the managed connection
   * param initParams
   */

  public void init(Object initParams);


  /**
   * Cleans up underlying physical connection. This method is
   * called by the framework in order to clean up the connection management
   */
  public void cleanup();


  /**
   * Destroys the underlying physical connection.
   */
  public void destroy();

  /**
   * Allocates a physical connection to this managed connection
   * @connection physical connection
   */
  public void associateConnection(Object connection);

  /**
   * Sets a key which unbiguously identifies this connection
   * @param conKey key which unbiguously identifies this connection
   */
  public void setConnectionKey(String conKey);

  /**
   * Gets a key which unbiguously identifies this connection
   * @return key which unbiguously identifies this connection
   */
  public String getConnectionKey();

  /**
   * Sets the name of connection factory which is used to create the
   * physical connection
   * @param conFactoryName name of connection factory
   */
  public void setConnectionFactoryName(String conFactoryName);

  /**
   * Gets the name of connection factory used to create the physical
   * connection
   * @return Gets the name of connection factory
   */
  public String getConnectionFactoryName();

  /**
   * Sets the name of connection configuration used to create the
   * physical connection
   * @param conConfigName name of connection configuration used to
   *    create the physical connection
   */
  public void setConnectionConfigName(String conConfigName);

  /**
   * Gets the name of connection configuration used to create the
   * physical connection
   * @return name of connection configuration used to create the physical connection
   */
  public String getConnectionConfigName();

  /**
   * Sets the Backend Context
   * @param context the Backend Context
   */
  public void setBackendContext(BackendContext context);

}