package com.sap.isa.core.eai;

// Java imports
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;


/**
 * This interface should be implemented by a backend connection factory
 */
public class ConnectionFactory {

  private BackendObjectManager backendObjectManager;
  private Connection defaultCon;
  private Set mListener = null;
  private BackendContext mBackendContext;
  private String mJcs = null;

  /** 
	*  The IsaLocation of the current Action.
	*  This will be instantiated during perform and is always present.
	*/
   protected IsaLocation log = IsaLocation.getInstance(ConnectionFactory.class.getName());

  /**
   * Default constructor
   */
  private ConnectionFactory() {}

  /**
   * Constructor
   * @param backendObjectManager Backend Object Manager the business object
   *      which usess this connection factory belongs to
   */
  public ConnectionFactory(BackendObjectManager backendObjectManager) {
    this.backendObjectManager = backendObjectManager;
  }

  /**
   * returns a backend specific connection with external specified
   * connection parameters
   * @param facName the name of the backend connection factory.
   * @param conConfigName name of connection configuration used to create
   *    connection
   * @return a backend specific connection
   */
  public Connection getConnection(String facName, String conConfigName) throws BackendException {
    Connection con = ConnectionManager.getConnectionManger().
        allocateConnection(backendObjectManager, facName, conConfigName);
    if (con == null)
      return null;

    ((ManagedConnection)con).setBackendContext(mBackendContext);
    addAllListeners(con);
	setJCoSettings(con, null, mJcs);
    return con;
  }
  /**
   * returns a backend specific connection. A set of properties is passed
   * which overwrites existing or missing properties of the connection
   * configuration
   * @param facName the name of the backend connection factory.
   * @param conConfigName name of connection configuration used to create
   *    connection
   * @param conDefProps connection properties which overwrite/complete properties
   *    set in the connection configuration file of the connection manager
   * @return a backend specific connection
   */
  public Connection getConnection(String facName, String conConfigName, Properties conDefProps)
        throws BackendException {
    Connection con = ConnectionManager.getConnectionManger().
        allocateConnection(backendObjectManager, facName, conConfigName, conDefProps);
    if (con == null)
      return null;

    ((ManagedConnection)con).setBackendContext(mBackendContext);
    addAllListeners(con);
	setJCoSettings(con, null, mJcs);
    return con;
  }

  /**
   * returns an existing connection for the given key
   * @param conKey key which unambiguously specifies a conneciton
   * @return a backend specific connection if existing, otherwise <code>null</code>
   */
  public Connection getConnection(String conKey)
        throws BackendException {
    Connection con = ConnectionManager.getConnectionManger().
          allocateConnection(backendObjectManager, conKey);
    if (con == null)
      return null;

    ((ManagedConnection)con).setBackendContext(mBackendContext);
    addAllListeners(con);
    setJCoSettings(con, null, mJcs);
    return con;
  }

  /**
   * Sets default connection to the backend system
   * @param defaultCon default connection
   */
  public void setDefaultConnection(Connection defaultCon) {
    this.defaultCon = (Connection)defaultCon;
  }

  /**
   * Sets the Backend Context
   * @param context the Backend Context
   */
  void setBackendContext(BackendContext context) {
    mBackendContext = context;
  }

	/**
	 * Sets the id of jco connection sharing. For internal use only.
	 * @param id  
	 */
  void setJCS(String id) {
  	mJcs = id;
  }

  /**
   * Gets default connection to the backend system
   * @return default connection
   */
  public Connection getDefaultConnection() {
    if (defaultCon == null)
      return null;

    ((ManagedConnection)defaultCon).setBackendContext(mBackendContext);
    addAllListeners(defaultCon);
	setJCoSettings(defaultCon, null, mJcs);
    return defaultCon;
  }

  /**
   * Returns a connection which is a modified default connection. This is
   * a convenience function which allows to getting modified default connections
   * quickly
   * @param conDefParams connection parameters which overwrite/extend the connection
   *    parameters of the default connection
   * @return a connection
   */
/*  public Connection getModDefaultConnection(Properties conDefProps) {
    return defaultCon;
  }*/

  /**
   * If the connection definition for the default connection is incomplete
   * then it is possible to complete the connection data using this method.
   * After a default connection has been established it is no longer possible
   * to use this function.
   * @param conDefProps additional connection properties to complete an incomplete
   *        connection definition for the default connection
   * @return a connection. Check using isValid() whether the physical connection is valid 
   */
  public Connection getDefaultConnection(String facName,
                                         String conConfigName,
                                         Properties conDefProps)
                                         throws BackendException {
    // first check if the current default connection is valid
    if (defaultCon == null)
      return null;
 //   if (defaultCon.isValid())
 //     throw new BackendException ("You cannot change a valid default connection");
      //return defaultCon;

    // first retrieve a new connection with the provided parameters
    ManagedConnection mgNewDefaultCon = (ManagedConnection)ConnectionManager.getConnectionManger().
        allocateConnection(backendObjectManager, facName, conConfigName, conDefProps);

    //copy all parameters from the new connection into the default connection
    ManagedConnection mgDefaultCon = (ManagedConnection)defaultCon;
    mgDefaultCon.setConnectionDefinitionName(mgNewDefaultCon.getConnectionDefinitionName());
//    mgDefaultCon.setConnectionKey(mgNewDefaultCon.getConnectionKey());
    // destroy the current invalid connection handle
    mgDefaultCon.destroy();

    // This part of code works only for the JCo connection factory
    setJCoSettings((JCoConnection)mgDefaultCon, ((JCoConnection)mgNewDefaultCon).getJCoRepositoryName(), mJcs);
    
    addAllListeners((Connection)mgDefaultCon);
    ((ManagedConnection)mgDefaultCon).setBackendContext(mBackendContext);
    
    return (Connection)mgDefaultCon;
  }

  /**
   * Returns the id of the Backend Object Manager the factory is associated with
   */
  public String getId() {
    return backendObjectManager.getId();
  }

  /**
   * Adds a listener to all connections created by this connection factory
   * @param aListener A listener which will be notofied if connection events occur
   */
  public void addConnectionEventListener(ConnectionEventListener aListener) {
    if (mListener == null)
      mListener = new HashSet();

    mListener.add(aListener);
  }

  /**
   * Removes a conennection event listener
   * @param aListener A listener which will be notofied if connection events occur
   */
  public void removeConnectionEventListener(ConnectionEventListener aListener) {
    if (mListener != null)
      mListener.remove(aListener);
  }

  private void addAllListeners(Connection con) {
    if (mListener == null)
      return;
    Iterator iterator = mListener.iterator();

    while (iterator.hasNext()) {
      ConnectionEventListener listener = (ConnectionEventListener)iterator.next();
      con.addConnectionEventListener(listener);
    }
  }

  /**
   * Returns a Map containing configurations for managed connection factories
   * @return a Map containing configurations for managed connection factories
   */
  public Map getManagedConnectionFactoryConfigs() throws BackendException {
    return backendObjectManager.getBackendConfig().getManagedConnectionFactoryConfigs();
  }

  /**
   * @SuppressWarnings("finally").
   */  
  private Connection setJCoSettings(Connection con, String repid, String jcs) {
	// This part of code works only for the JCo connection factory
	try {
		if (repid != null)
	  		((JCoConnection)con).setJCoRepositoryName(repid);
	  	if (jcs != null)
	  		((JCoConnection)con).setJCS(jcs);
	} catch (Throwable ex) {
		log.debug(ex.getMessage());
	  // do nothing, in case that this connection is not served by
	  // a JCo connection factory
	} 
    
    return con;
  }
	/**
	 * Cleans up connection factory. For internal use only
	 */
	void cleanup() {
		backendObjectManager = null;
		defaultCon = null;
		mListener = null;
		mBackendContext = null;
	}
	
  }

