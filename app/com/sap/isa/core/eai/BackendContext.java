/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      10 April 2001

  $Revision: #3 $
  $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.core.eai;

import com.sap.isa.core.logging.IsaLocation;

/**
 * This class acts as an context which is shared between all backend business
 * objects which are created by the same Backend Object Manager
 */
public class BackendContext {

    private BackendObjectManager bem;
    protected static IsaLocation log = IsaLocation.getInstance(BackendContext.class.getName());

    /**
     * Constructor
     * @param bem Backend Object Manager which is responsible for maintaining the context
     */

    public BackendContext(BackendObjectManager bem) {
        this.bem = bem;
    }

    /**
     * Binds the object to the backend context using the name specified. The context
     * is common to all backend business object created by the same Backend Object Manager
     * instance
     * @param name The name to which the object is bound; cannot be <code>null</code>
     * @param value The object to be bound; cannot be <code>null</code>
     */
    public void setAttribute(String name, Object value) {

        if (log.isDebugEnabled())
            log.debug("Adding attribute to the backend context. name = \"" + name + "\", value = \"" + value + "\"");

        bem.setAttribute(name, value);
    }

    /**
     * Returns the object bound with the specified name in this session,
     * or <code>null</code> if no object is bound under the name
     * @param name a string specifying the name of the object
     * @returns the object with the specified name
     */
    public Object getAttribute(String name) {

        Object attr = bem.getAttribute(name);

        if (log.isDebugEnabled())
            log.debug(
                "Getting attribute from the backend context. name = \""
                    + name
                    + "\", value = \""
                    + (attr == null ? "null" : attr.toString())
                    + "\"");

        return attr;
    }

    /**
     * Clean up. For internal use only
     */
    void cleanup() {
        bem = null;
    }

    public String getBackendConfigKey() {
        return bem.getBackendConfigKey();
    }
}
