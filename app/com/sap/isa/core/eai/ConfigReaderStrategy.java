package com.sap.isa.core.eai;

// java imports
import java.util.Map;

/**
 * @deprecated Configuration is handled by the Extended Configuration Management
 */

public interface ConfigReaderStrategy {

  /**
   * This method is called by the <code>ConfigReader</code> after the eai-config
   * file has been read. The implementation of this method can be used to
   * post-initialize the read configuration information
   * @param conFactoryConfigs a map containing configuration information on
   *        all managed connection factories
   * @param businessObjectConfigs a map containing configuration information on
   *        all backend objects
   */
  public void postInit (Map conFactoryConfigs, Map businessObjectConfigs);

}