/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      15 February 2001
*****************************************************************************/

package com.sap.isa.core.eai;

import com.sap.isa.core.logging.IsaLocation;

/**
 * Thrown if something goes wrong in the backend layer
 *
 * NOTE: Currently there is an additional logging done here in order to find
 *       a serious bug. The logging should be removed afterwards.
 */
public class BackendException extends Exception {

    public static IsaLocation log =
                  IsaLocation.getInstance(BackendException.class.getName());

    private Exception rootEx;

    /**
     * Constructor
     *
     * @param msg Message for the Excepetion
     */
    public BackendException(String msg) {
        super(msg);
        if (log.isDebugEnabled()) {
          log.debug("BackendException raised: " + msg);
        }
    }

    /**
     * Constructor
     *
     * @param msg Message for the Excepetion
     */
    public BackendException(Exception ex) {
        super(ex.toString());
        if (log.isDebugEnabled()) {
          log.debug("BackendException raised: " + ex);
        }
    }


    /**
     * Constructor
     */
    public BackendException() {
        super();
        if (log.isDebugEnabled()) {
          log.debug("BackendException has been raised");
        }
    }

}