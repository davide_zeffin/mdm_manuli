/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      28 May 2001
  $Id: //java/sapm/esales/30/src/com/sapmarkets/isa/core/eai/init/InitEaiISA.java#6 $
  $Revision: #6 $
  $Change: 26587 $
  $DateTime: 2001/07/31 17:26:36 $
  $Author: d025909 $
*****************************************************************************/
package com.sap.isa.core.eai.init;



/**
 * This class is responsibe for mapping logon-paramteters set in the web.xml
 * deployment desciptor with parameters specified by the eai-config file.
 * The implementation of this class is specific for the ISA application. It
 * requires a specific content of the eai-config.xml file. This content is mixed
 * up with parameters specified in the web.xml file.
 */
public class InitEaiISA  {

    public static final String CONDEF_NAME_ISA_COMPLETE      = "ISA_COMPLETE";
    public static final String CONDEF_NAME_ISA_INCOMPLETE    = "ISA_INCOMPLETE";
    public static final String CONDEF_NAME_ISA_INCOMPLETE_POOLED = "ISA_INCOMPLETE_POOLED";
    public static final String CONDEF_NAME_ISA_COMPLETE_ALT  = "ISA_COMPLETE_ALT";
    public static final String CONDEF_NAME_ISA_INCOMPLETE_ALT = "ISA_INCOMPLETE_ALT";    
	public static final String CONDEF_NAME_ISA_IPCJCO    	 = "IPCJCO";
    public static final String CONDEF_NAME_IPC_CON1          = "CON1";
    public static final String CON_NAME_ISA_STATEFUL         = "ISAStateful";
    public static final String CON_NAME_ISA_STATELESS        = "ISAStateless";
    public static final String CON_NAME_ISA_STATEFUL_ALT     = "ISAStatefulAlt";
    public static final String CON_NAME_ISA_STATELESS_ALT    = "ISAStatelessAlt";    
    public static final String CON_NAME_IPC_STATEFUL		 = "IPCStateful";
    public static final String CON_NAME_IPC_STATELESS        = "IPCStateless";
    public static final String CONDEF_NAME_ISA_IMS           = "IMS";
    public static final String FACTORY_NAME_JCO              = "JCO";
    public static final String FACTORY_NAME_BWJCO		   = "BWJCO";
    public static final String FACTORY_NAME_IPC              = "IPC";
    public static final String BO_TYPE_IPCCLIENT             = "IPCClient";
    public static final String BO_TYPE_CATALOG               = "CATALOG";
    public static final String BO_TYPE_BASKET                = "Basket";

}