/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      15 February 2001
*****************************************************************************/
package com.sap.isa.core.eai;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import com.sap.isa.core.eai.config.ConfigManager;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;


/**
 * This is the central instance which manages connections which belong to
 * a specific web application
 */
public class ConnectionManager {

  private static ConnectionManager connectionManager;
  private static Map mgConFactories = Collections.synchronizedMap(new HashMap());
  protected static IsaLocation log = IsaLocation.getInstance(ConnectionManager.class.getName());

  /**
   * This class acts as a container for managed connections which belong
   * to a specifig web application
   */
  protected static class ManagedConnectionContainer {


    protected Map managedConnections = Collections.synchronizedMap(new HashMap());

    /**
     * Retrieves an existing managed connection
     * @param connectionId id which specifies an managed conneciton
     * @return managed connection if available, otherwise null
     */
    public ManagedConnection getManagedConnection(String connectionId) {
      return (ManagedConnection)managedConnections.get(connectionId);
    }

    /**
     * Sets a managed connection in the managed connection container
     * @param managedConnection managed connection
     */
     public void addManagedConnection(String connectionId, ManagedConnection managedConnection) {
	  if (log.isDebugEnabled()) {
	  	log.debug("Adding managed connection to connection container [id]='" + connectionId + "' [ibjid]='" + managedConnection + "'");
	  }
      managedConnections.put(connectionId, managedConnection);
     }
  }

  /**
   * Stores a connection container for each web application
   */
  protected Map mgConContainers = Collections.synchronizedMap(new HashMap());


  /**
   * This class is a singelton.
   * To get an instance call getConnectionManager()
   */
  private ConnectionManager() {};

  /**
   * Returns a single instance of Connection Manager
   * @return single instance of connection manager
   */
  public static synchronized ConnectionManager getConnectionManger() throws BackendException{
    if (connectionManager == null) {
          connectionManager = new ConnectionManager();
          // init all connection managers
 //         initManagedConnectionFactories();
    }
    return connectionManager;
  }

  /**
   * This method inits all specialized connection managers which will serve
   * backend specific connections
   * @throws BackendException if there is no configuration or something goes wrong
   *                          during configuration
   */
  protected static void initManagedConnectionFactories() throws BackendException {
    initManagedConnectionFactories(ConfigManager.getConfigManager().getDefaultConfig());
  }
  /**
   * Returns a backend specific connection and associates this connection
   * with a specific Backend Object Manager
   * @param bem a reference to a BackendObjectManager object
   * @param conFactoryName name of connection factory which should create the
   *    connection
   * @param conName name of connection configuration which should be used to
   *    create the connection
   */
  public Connection allocateConnection(BackendObjectManager bem,
        String conFactoryName, String conName) throws BackendException {
    if (log.isDebugEnabled())
      log.debug("Enter allocateConnection(BackendObjectManager bem, String conFactoryName, String conName)");
    String facKey = bem.getBackendConfigKey() + conFactoryName;
    ManagedConnection mgCon = getCachedManagedConnection(bem, conFactoryName, conName, null);
    if (mgCon != null)
      return (Connection)mgCon;
    else
      return (Connection)createManagedConnection(bem, conFactoryName, conName, null);
  }

  /**
   * Returns an existing connection for the given key
   * @param bem a reference to a BackendObjectManager object
   * @param conFactoryName name of connection factory which should create the
   *    connection
   * @param conKey key which unambiguously specifies a conneciton
   * @return a backend specific connection if existing, otherwise <code>null</code>
   */
  public Connection allocateConnection(BackendObjectManager bem, String conKey)
              throws BackendException {

  if (log.isDebugEnabled())
      log.debug("Serving an existing connection using key \"" + conKey + "\"");

    ManagedConnectionContainer mgConContainer =
        (ManagedConnectionContainer)mgConContainers.get(bem);
    return (Connection)mgConContainer.getManagedConnection(conKey);
  }



  /**
   * Returns a backend specific connection and associates this connection
   * with a specific Backend Object Manager
   * @param bem a reference to a BackendObjectManager object
   * @param conFactoryName name of connection factory which should create the
   *    connection
   * @param conName name of connection configuration which should be used to
   *    create the connection
   * @param conDevProps a set of properties which extends/overwrites properties
   *    of the connection definition which is associated with the connection
   */
  public synchronized Connection allocateConnection(BackendObjectManager bem,
        String conFactoryName, String conName, Properties conDefProps) throws BackendException {
    if (log.isDebugEnabled())
      log.debug("Enter allocateConnection(BackendObjectManager bem, String conFactoryName, String conName, Properties conDefProps)");


    if(log.isDebugEnabled())
     log.debug("Serving connection based on \"" + conName + "\" connection with additional properties");

    String facKey = bem.getBackendConfigKey() + conFactoryName;
    ManagedConnection mgCon = getCachedManagedConnection(bem, conFactoryName, conName, conDefProps);
    if (mgCon != null)
      return (Connection)mgCon;
    else
      return (Connection)createManagedConnection(bem, conFactoryName, conName, conDefProps);
  }

  /**
   * This method checks if there is a suitable managed connection for this
   * request and returns one if available
   * @returns a managed connection if there is a suitable connection available,
   *      otherwise null
   */
  private synchronized ManagedConnection getCachedManagedConnection(BackendObjectManager bem,
        String conFactoryName, String conName, Properties conDefProps) {

	if (log.isDebugEnabled()) {
 		log.debug("ENTER getCachedManagedConnection(BackendObjectManager bem, String conFactoryName, String conName, Properties conDefProps)");
	}

    String mgConName = conName + (conDefProps == null ? "" : String.valueOf(conDefProps.hashCode()));
    ManagedConnection mgCon = null;

    // check if there is already a connection container with this application id
    ManagedConnectionContainer mgConContainer =
        (ManagedConnectionContainer)mgConContainers.get(bem);
    if (mgConContainer != null) {
      // get managed connection from managed connection container
      mgCon = mgConContainer.getManagedConnection(mgConName);
      if (mgCon != null) {
        if (log.isDebugEnabled())
          log.debug("Serving an existing managed connection \"" + mgConName + "\" from managed connection container");
        return mgCon;
      }
   } else {
      if (log.isDebugEnabled()) {
		log.debug("Creating managed connection container for \"" + bem + "\"");
		log.debug("EXIT getCachedManagedConnection(BackendObjectManager bem, String conFactoryName, String conName, Properties conDefProps)");
      }
      mgConContainer = new ManagedConnectionContainer();
      mgConContainers.put(bem, mgConContainer);
    }
	if (log.isDebugEnabled()) {
		log.debug("EXIT getCachedManagedConnection(BackendObjectManager bem, String conFactoryName, String conName, Properties conDefProps)");
	}

    return null;
  }

  /**
   * This method creates a new managed connection
   * @return a new managed connection
   */
  public synchronized ManagedConnection createManagedConnection(BackendObjectManager bem,
        String conFactoryName, String conName, Properties conDefProps) throws BackendException {

    if (log.isDebugEnabled())
      log.debug("Creating a new managed connection (factoryName=\"" + conFactoryName +
      "\" conName=\"" + conName + "\"");

    String facKey = bem.getBackendConfigKey() + conFactoryName;
     // get appropriate managed connection factory
    ManagedConnectionFactory mgConFactory = (ManagedConnectionFactory)mgConFactories.get(facKey);

    if (mgConFactory == null)
      throw new BackendException("No connection Factory with the given name \""
          + conFactoryName + "\" found");

    ManagedConnection mgCon = null;


    if (conDefProps == null)
      mgCon = mgConFactory.getManagedConnection(conName);
    else
	  mgCon = mgConFactory.getManagedConnection(conName, (Properties)conDefProps.clone());


    String mgConName = conName + (conDefProps == null ? "" : String.valueOf(conDefProps.hashCode()));

    if (log.isDebugEnabled())
      log.debug("Managed connection \"" + mgConName + "\" created");


    // add this connection to connection conatiner
    ManagedConnectionContainer mgConContainer =
        (ManagedConnectionContainer)mgConContainers.get(bem);
    mgConContainer.addManagedConnection(mgConName, mgCon);

    mgCon.setConnectionKey(mgConName);
    return mgCon;
  }

  /**
   * Cleans up a single managed connection for the given Backend Object Manager
   */
  public void cleanupConnections(BackendObjectManager bem, String conKey) {
    if (log.isDebugEnabled())
      log.debug("Cleaning up connection + \"" + conKey + "\" for \"" + bem + "\" Backend Object Manager");

    if (bem == null)
      return;

    // get corresponding backend object manager
    ManagedConnectionContainer mgConContainer =
        (ManagedConnectionContainer)mgConContainers.get(bem);

    if (mgConContainer == null)
      return;

    ManagedConnection mgCon = (ManagedConnection)mgConContainer.managedConnections.get(conKey);
    if (mgCon != null) {
      mgCon.cleanup();
      // remove the connection from the hashmap, it has been cleaned up!
      // otherwise the old connection will be reused again.
      mgConContainer.managedConnections.remove(conKey);
    }
  }


  /**
   * Cleans up connections for the given Backend Object Manager
   */
  public void cleanupConnections(BackendObjectManager bem) {
    if (log.isDebugEnabled())
      log.debug("Cleaning up connections for \"" + bem + "\" Backend Object Manager");

    if (bem == null)
      return;

    // get corresponding backend object manager
    ManagedConnectionContainer mgConContainer =
        (ManagedConnectionContainer)mgConContainers.get(bem);

    if (mgConContainer == null)
      return;

    Iterator mgConKeysIterator = mgConContainer.managedConnections.keySet().iterator();
    String conKey = null;
    ManagedConnection mgCon = null;
    while(mgConKeysIterator.hasNext()) {
      conKey = (String)mgConKeysIterator.next();
      if (log.isDebugEnabled())
        log.debug("Removing managed connection \"" + conKey + "\"");
      mgCon = (ManagedConnection)mgConContainer.managedConnections.get(conKey);
      try {
		mgCon.cleanup();
      } catch (Throwable t) {
      	continue;
      }
      
    }
    // removing backend object manager
    mgConContainers.remove(bem);
  }

  /**
   * Initializes connection factoreis for a specific configuration
   */
  public static void initManagedConnectionFactories(BackendConfig config)
                          throws BackendException {

    String backendConfigKey = config.getId() + config.getDefaultdata();

    
    Map mgConFactoryConfigs =
      config.getManagedConnectionFactoryConfigs();

    log.info("system.eai.init.ConFac");

    // initialize backend connection manager
    if (mgConFactoryConfigs == null || mgConFactoryConfigs.size() == 0) {
      log.info("system.eai.init.processingFactory", new Object[] { "no factories" }, null);
      return;
    }
      //throw new BackendException("There is no configuration for managed connection factories");

    ManagedConnectionFactoryConfig mgConFactoryConfig;
    Iterator factoryConfigsIterator = mgConFactoryConfigs.values().iterator();
    String factoryName = null;
    while (factoryConfigsIterator.hasNext()) {
      ManagedConnectionFactory mgConFactory;
      mgConFactoryConfig = (ManagedConnectionFactoryConfig)factoryConfigsIterator.next();
      factoryName = mgConFactoryConfig.getName();
      try {
        Class mgConFactoryClass = Class.forName(mgConFactoryConfig.getClassName());
        log.info("system.eai.init.processingFactory", new Object[] {factoryName}, null);

        mgConFactory = (ManagedConnectionFactory)mgConFactoryClass.newInstance();
        mgConFactory.setFactoryName(factoryName);
        // register managed connection factory to connection manager
        String factoryKey = backendConfigKey + mgConFactoryConfig.getName();
        mgConFactories.put(factoryKey, mgConFactory);
        mgConFactory.setProperties(mgConFactoryConfig.getProperties());
        mgConFactory.initConnections(mgConFactoryConfig.getConnectionDefinitions());
        mgConFactory.addConnectionConfigs(mgConFactoryConfig.getConnectionConfigs());
        log.debug("Factory with [key]='" + factoryKey + "' added to connection manager");
      } catch (Throwable ex) {
        String errMsg = "Error creating backend connection manager "
              + "config name: \"" + mgConFactoryConfig.getName()
                  + "\"; class name: \"" + mgConFactoryConfig.getClassName() + ex.toString();
        log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.eai.exception", new Object[] { errMsg }, ex);
//        ex.printStackTrace();
        //throw new BackendException(errMsg);
      }
    }
  }
}