package com.sap.isa.core.eai;
  /*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      19 February 2002
*****************************************************************************/

// isa imports
import java.util.Map;
import java.util.Properties;

import com.sap.isa.core.eai.config.Config;
import com.sap.isa.core.eai.config.ConfigBase;
import com.sap.isa.core.util.XMLHelper;
import com.sap.isa.core.xcm.init.ExtendedConfigInitHandler;


/**
 * This container represents a backend configuration
 */
public class BackendConfig extends ConfigBase {

  private XMLHelper xmlHelper = new XMLHelper();
  private static final String PROPNAME_MGR_FACTORIES    = "managedConnectionFactories";
  private static final String PROPNAME_BACKEND_BOS = "backendBusinessObjects";
  private static final String PROPNAME_DEFAULT_DATA = "defaultdata";

  public BackendConfig (Map data) {
    mData = data;
  }

  public BackendConfig () {
    mData.put(PROPNAME_BACKEND_BOS, new BackendBusinessObjectConfigs());
    mData.put(PROPNAME_MGR_FACTORIES, new ManagedConnectionFactoryConfigs());
  }

  /**
   * Returns a clone of the configuration
   */
  public Config getConfigClone() {
    return new BackendConfig(getClonedData());
  }

  /**
   * Sets the default data source
   * @param defaultData the default data source
   */
  public void setDefaultdata(String defaultData) {
    mData.put(PROPNAME_DEFAULT_DATA, defaultData);
  }

  /**
   * Gets the default data source
   * @return name of the default data source
   */
  public String getDefaultdata() {
  	if (!ExtendedConfigInitHandler.isActive())
    	return (String)mData.get(PROPNAME_DEFAULT_DATA);
    else
    	return "";
  }


  /**
   * Adds a managed connection to this configuration
   * @param config a managed connection factory
   */
  public void addManagedConnectionFactoryConfig(ManagedConnectionFactoryConfig config) {
    getInternalMgrConFacConfigs().addMgrConFactoryConfig(config);
  }

  /**
   * Adds a managed connection to this configuration
   * @param fac a managed connection factory
   */
  public void addBackendBOConfig(BackendBusinessObjectConfig config) {
    getInternalBOConfigs().addBackendBOConfig(config);
  }

  /**
   * Returns the internal container for backend business objects
   * @return the internal container for backend business objects
   */
   private BackendBusinessObjectConfigs getInternalBOConfigs() {
    BackendBusinessObjectConfigs configs =
        (BackendBusinessObjectConfigs)mData.get(PROPNAME_BACKEND_BOS);
    if (configs == null) {
      configs =  new BackendBusinessObjectConfigs();
      mData.put(PROPNAME_BACKEND_BOS, configs);
    }
    return (BackendBusinessObjectConfigs)mData.get(PROPNAME_BACKEND_BOS);
   }
  /**
   * Returns the internal container for connection factories
   * @return the internal container for connection factories
   */
   private ManagedConnectionFactoryConfigs getInternalMgrConFacConfigs() {
    ManagedConnectionFactoryConfigs configs =
        (ManagedConnectionFactoryConfigs)mData.get(PROPNAME_MGR_FACTORIES);
    if (configs == null) {
      configs =  new ManagedConnectionFactoryConfigs();
      mData.put(PROPNAME_MGR_FACTORIES, configs);
    }
    return (ManagedConnectionFactoryConfigs)mData.get(PROPNAME_MGR_FACTORIES);
   }

  /**
   * Returns a string representation of this configuration
   * @return a string representation of this configuration
   */
  public String toString() {
    StringBuffer sb = new StringBuffer();
    Properties params = new Properties();
    if (getId() != null)
      params.setProperty("id", getId());
    if (getBase() != null)
      params.setProperty("base", getBase());
    if (getDefaultdata() != null)
      params.setProperty("defaultdata", getDefaultdata());

    sb.append(xmlHelper.createTag("config", params, false));
    sb.append(getInternalMgrConFacConfigs().toString());
    sb.append(getInternalBOConfigs().toString());
    sb.append(xmlHelper.createEndTag("config"));
    return sb.toString();
  }

  /**
   * Returns a Map containing configurations for managed connection factories
   * @return a Map containing configurations for managed connection factories
   */
  public Map getManagedConnectionFactoryConfigs() {
    Map map = getInternalMgrConFacConfigs().getManagedConnectionFactoryConfigs();
    return map;
  }

  /**
   * Returns information on a specific backend Business Object
   * @return  information on a specific backend Business Object
   **/
  public BackendBusinessObjectConfig getBackendBusinessObjectConfig(String boType) {
    return (BackendBusinessObjectConfig)getInternalBOConfigs().getBackendBOConfig(boType);
  }

  /**
   * Returns a Map containing configurations for backend business objects
   * @return a Map containing configurations for backend business objects
   */
    public Map getBackendBusinessObjectConfigs() {
    Map map = getInternalBOConfigs().getBackendBusinessObjectConfigs();
    return map;
  }
}