/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      19 March 2001

  $Revision: #2 $
  $Date: 2001/06/28 $
*****************************************************************************/
package com.sap.isa.core.eai;


/**
 * This empty interface has to be implemented by parameter container which are passed
 * to a backend business object while it is created
 */
public interface BackendBusinessObjectParams {
}