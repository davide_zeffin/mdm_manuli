/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      12 March 2001
*****************************************************************************/
package com.sap.isa.core.eai;


import java.util.Properties;


/**
 * This is the base interface which has to be implemented by all backend
 * business object which are managed by the Backend Object Layer
 */
public interface BackendBusinessObject {

  /**
   * Sets a reference to a connection factory which can be used to
   * retrieve connections to the backend system which are not preconfigured
   * @param conFactory a reference to a connection factory
   */
  public void setConnectionFactory(ConnectionFactory conFactory);

  /**
   * Returns a reference to a connection factory which can be used to create
   * arbirtrary backend connections
   * @return a referece to a connection factory
   */
  public ConnectionFactory getConnectionFactory();


  /**
   * This method is called by the BackendObjectManager. It can be used to
   * initialize the backend business object. The set of propeties can by
   * is set in a configuration file
   * @param props a set of properites which can be used to initialize the
   *    business object
   * @throws BackendException throws if something goes wrong which prevents
   *    the business object from working properly
   */
  public void initBackendObject(Properties props, BackendBusinessObjectParams params) throws BackendException;

 /**
   * This method is called by the BackendObjectManager before the object
   * gets invalidated. It can be used to lean up resources allocated by
   * the backend object
   */
  public void destroyBackendObject();


  /**
   * Sets context which is common to all Backend Business Objects created
   * by the same Backend Object Manager
   * @param context the backend context
   */
  public void setContext(BackendContext context);

  /**
   * Gets context which is common to all Backend Business Objects created
   * by the same Backend Object Manager
   * @return the backend context
   */
  public BackendContext getContext();

  /**
   * Returns an object which provides some additional functionality
   * related with a backend business object
   * @return reference to s backend object support object
   */
  public BackendObjectSupport getBackendObjectSupport();

  /**
   * Sets an object which provides some additional functionality
   * related with a backend business object
   * @param support reference to s backend object support object
   */
  public void setBackendObjectSupport(BackendObjectSupport support);

}