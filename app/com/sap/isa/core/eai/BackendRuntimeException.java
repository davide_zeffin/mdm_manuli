/*****************************************************************************
    Class:        BackendRuntimeException
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Thomas Smits
    Created:      28.3.2001
    Version:      1.0

    $Revision: #2 $
    $Date: 2001/06/28 $
*****************************************************************************/

package com.sap.isa.core.eai;

/**
 * Exception for Errors caused by the developer of the isales application.
 * <br>
 * Don't catch this Exception except in the action layer because it
 * represents an error in the implementation and not the underlying
 * backend system.
 */
public class BackendRuntimeException extends RuntimeException {

    /**
     * Creates a new exception without any message
     */
    public BackendRuntimeException() {
        super();
    }

    /**
     * Creates a new exception with a given message
     *
     * @param msg Message to give further information about the exception
     */
    public BackendRuntimeException(String msg) {
        super(msg);
    }
}