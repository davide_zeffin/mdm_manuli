package com.sap.isa.core.eai;

// internet sales imports
import java.io.InputStream;
import java.util.Collection;
import java.util.Collections;
import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import com.sap.isa.core.Constants;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.SharedConst;
import com.sap.isa.core.eai.config.ConfigManager;
import com.sap.isa.core.eai.xcm.BackendConfigManager;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.xcm.ConfigContainer;
import com.sap.isa.core.xcm.ExtendedConfigManager;
import com.sap.isa.core.xcm.init.ExtendedConfigInitHandler;

/**
 * This is the base class for all <code>BackendBusinessManager</code> which
 * use the EAI layer
 */
public class BackendObjectManagerImpl implements BackendObjectManager {

  protected static IsaLocation log = IsaLocation.
        getInstance(BackendObjectManagerImpl.class.getName());


  private Hashtable context = new Hashtable();
  private Map backendObjects = Collections.synchronizedMap(new HashMap());
  private String mId = null;
  private BackendConfig mCurrentConfig;
  static int test = 1;
  private String mConfigId = ConfigManager.DEFAULT_CONFIG_ID;
  private String mConfigData = ConfigManager.DEFAULT_DATASOURCE;
  private String mJcs = null;

	// XCM variables
	public static final String XCM_FILE_PROPERTY_NAME = "eai-config";

  /**
   * Defaulot Constructor
   */
  public BackendObjectManagerImpl() {
    super();
    
    if (log.isDebugEnabled())
    	log.debug("START BackendObjectManagerImpl()");
    
  	if (!ExtendedConfigInitHandler.isActive()) {    
    	initBackendConfig31();
  	}
    createStandaloneBackendBusinessObjects();

  }


  /**
   * Constructor
   * @param id id which uniquely identifies the backend object manager
   */
  public BackendObjectManagerImpl(String aId) {

    if (log.isDebugEnabled())
    	log.debug("BackendObjectManagerImpl(String aId)");
 
  	if (!ExtendedConfigInitHandler.isActive()) { 
	    initBackendConfig31();  		
  	}
 
    mId = aId;
    createStandaloneBackendBusinessObjects();
  }

  /**
   * Constructor
   * @param props properties used to initialize backend object manager
   */
  public BackendObjectManagerImpl(Properties props) {

    if (log.isDebugEnabled())
    	log.debug("START BackendObjectManagerImpl(Properties props)");

  	if (!ExtendedConfigInitHandler.isActive()) {
  	 
	    if (props.getProperty(SessionConst.EAI_CONF_ID) != null)
	      mConfigId = props.getProperty(SessionConst.EAI_CONF_ID);
	
	    if (props.getProperty(SessionConst.EAI_CONF_DATA) != null)
	      mConfigData = props.getProperty(SessionConst.EAI_CONF_DATA);
	
		initBackendConfig31();
	 } else {
	 	initBackendConfig(props);
	 	mJcs = props.getProperty(Constants.JCS);
  	}
    
  	String server = props.getProperty(Constants.RP_JCO_INIT_APPLICATION_SERVER);
  	String sysNr = props.getProperty(Constants.RP_JCO_INIT_SYSTEM_NUMBER);
  	String client = props.getProperty(Constants.RP_JCO_INIT_CLIENT);
  	if (server != null && sysNr != null && client != null) {
  	    context.put(Constants.RP_JCO_INIT_APPLICATION_SERVER, server);
  	    context.put(Constants.RP_JCO_INIT_SYSTEM_NUMBER, sysNr);
  	    context.put(Constants.RP_JCO_INIT_CLIENT, client);
  	}
  	
    createStandaloneBackendBusinessObjects();
  }

  /**
   * Creates a backend object, gets a default connection from the appropriate
   * connection foctory and associates the connection with the business object
   * @param beBOType specifies the type of the business object. The value of the
   *        <code>beBOType</code> is defined externally in the configuration
   *        file
   * @param params a set of runtime parameters which are passed to the
   *        <code>initBackendObject</code> method of the <code>BackendBusinessObject</code>
   *
   */
  public synchronized final BackendBusinessObject createBackendBusinessObject(String beBOType, BackendBusinessObjectParams params)
        throws BackendException {

    if (beBOType == null)
      throw new BackendException("Backend Business Object Type is 'null'");

    // getting Information on specific Business Object from configuration
    BackendBusinessObjectConfig boc =
        mCurrentConfig.getBackendBusinessObjectConfig(beBOType);
    if (boc == null)
      throw new BackendException("No entry for " + beBOType + " Business Object found");

    // create business object
    BackendBusinessObject beBO;
    try {
      String className = boc.getClassName();
      if (log.isDebugEnabled())
      	log.debug("Creating backend object for [class]='" + className + "'");

      Class boClass = Class.forName(className);

     log.debug("Creating new Backend Business Object '" + boClass.toString() + "'");

      beBO = (BackendBusinessObject)boClass.newInstance();

    } catch (Throwable ex) {
      String msg = "Current class [loader]=" + this.getClass().getClassLoader();
      msg = msg + "\n" + ex.toString();
      throw new BackendException(msg);
    }

    // now get a managed connection from Connection Manager
    ConnectionManager conManager = ConnectionManager.getConnectionManger();

    // get a default managed connection as specified in the configuration
    // for this business object
    String conFactoryName = boc.getConnectionFactoryName();
    String conDefName = boc.getDefaultConnectionName();
    Connection defaultCon = null;

    if ((conFactoryName != null && conFactoryName.length() > 0)&&
            (conDefName != null && conDefName.length() > 0)) {
      try {
        defaultCon = conManager.allocateConnection(this, conFactoryName, conDefName);
      } catch (BackendException bex) {
        String msg = "Could not create default connection for Backend Object '" + beBO + "'";
        log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.eai.exception", new Object[] { msg }, bex);
      }
    }

    // Adds a connection factory to the backend business object
    ConnectionFactory mgConFactory = new ConnectionFactory(this);

    beBO.setConnectionFactory(mgConFactory);

     // Add support object
     BackendObjectSupport support =
        new BackendObjectSupport(this, getBackendBusinessObjectMetaData(beBOType));

    beBO.setBackendObjectSupport(support);

    mgConFactory.setDefaultConnection(defaultCon);
	mgConFactory.setJCS(mJcs);

    // set the backend context
    BackendContext bc = new BackendContext(this);
    mgConFactory.setBackendContext(bc);

    beBO.setContext(bc);


    // initialize business object
    beBO.initBackendObject(boc.getProperties(), params);
	
	synchronized (backendObjects) {
		backendObjects.put(beBO, beBOType);	
	}
    

    return beBO;
  }
  /**
   * Creates a backend object and associates a default connection (if defined) to it.
   * This is a convenience method for backend objects which do not need any
   * parameters. The value of the parameter is implicitly <code>null</code>
   * @param beBOType specifies the type of the business object. The value of the
   *        <code>beBOType</code> is defined externally in the configuration
   *        file
   */
  public BackendBusinessObject createBackendBusinessObject(String beBOType)
        throws BackendException {
      return createBackendBusinessObject(beBOType, null);
  }

  /**
   * This method returns a container with metadata on backend business objects
   * Please not that this information is cloned from the original data.
   * Changing this data will not have any effect on the behaviour of the
   * EAI layer
   * @param type type of backend business object
   * @return container containing meta data on backend business object. <code>
   *    null</code> if no backend business object for the given type is found.
   */
  public BackendBusinessObjectMetaData
        getBackendBusinessObjectMetaData(String type) {

    BackendBusinessObjectConfig beBoConfig = null;
    ConnectionConfig defaultConConfig = null;
    ConnectionDefinition defaultConDef = null;

    beBoConfig = mCurrentConfig.getBackendBusinessObjectConfig(type);

    if (beBoConfig == null)
      return null;

    beBoConfig = (BackendBusinessObjectConfig)beBoConfig.getConfigClone();

    String defaultConFacName = beBoConfig.getConnectionFactoryName();

    if (defaultConFacName == null)
      return new BackendBusinessObjectMetaData(getBackendConfigKey(), beBoConfig, null, null);

//    ManagedConnectionFactoryConfig conFacConfig =
//        (ManagedConnectionFactoryConfig)cr.
//              getManagedConnectionFactoryConfigs().get(defaultConFacName);

    ManagedConnectionFactoryConfig conFacConfig =
        (ManagedConnectionFactoryConfig)mCurrentConfig.
            getManagedConnectionFactoryConfigs().get(defaultConFacName);



    if (conFacConfig == null)
      return new BackendBusinessObjectMetaData(getBackendConfigKey(),beBoConfig, null, null);

    defaultConConfig = (ConnectionConfig)conFacConfig.
          getConnectionConfig(beBoConfig.getDefaultConnectionName()).getConfigClone();

    if (defaultConConfig == null)
      return new BackendBusinessObjectMetaData(getBackendConfigKey(), beBoConfig, null, null);

    defaultConDef = (ConnectionDefinition)conFacConfig.
        getConnectionDefinition(defaultConConfig.getConnectionDefinition()).getConfigClone();

    return new BackendBusinessObjectMetaData(getBackendConfigKey(), beBoConfig, defaultConConfig, defaultConDef);
  }

  /**
   * Binds the object to the backend context using the name specified. The context
   * is common to all backend business object created by the same Backend Object Manager
   * instance
   * @param name The name to which the object is bound; cannot be <code>null</code>
   * @param value The object to be bound; cannot be <code>null</code>
   */
  public void setAttribute(String name, Object value) {
    if (name == null || value == null)
      return;

    context.put(name, value);
  }

  /**
   * Returns the object bound with the specified name in this session,
   * or <code>null</code> if no object is bound under the name
   * @param name a string specifying the name of the object
   * @returns the object with the specified name
   */
  public Object getAttribute(String name) {
    if (name == null)
      return null;
    return context.get(name);
  }

  /**
   * This method should be called when the backend objects which have been
   * created by this BackendObjectManager are taken out of service.
   * This method releases all connection to the corresponding connection factories
   */
  public synchronized void destroy() {
	if (log.isDebugEnabled()) 
		log.debug("Cleaning up environment");
  	
    try {
      synchronized (backendObjects) {
      // give backend objects opportunity for clean up
      Set keySet = backendObjects.keySet();
      Iterator iterator = keySet.iterator();
      String boClass = "";
      while (iterator.hasNext()) {
      try {
      		BackendBusinessObject bbo = (BackendBusinessObject)iterator.next();
      		try {
				boClass = bbo.getBackendObjectSupport().getMetaData().getBackendBusinessObjectConfig().getClassName();
      		} catch (NullPointerException npe) {
      			log.debug(npe.getMessage());
      			// should never happen
      		}
      		
			bbo.destroyBackendObject();
			// delete references hold by backend object
			BackendObjectSupport support = bbo.getBackendObjectSupport();
			if (support != null) {
				support.cleanup();
		}
			bbo.setBackendObjectSupport(null);

			BackendContext bc = bbo.getContext();
			if (bc != null)
				bc.cleanup();

			bbo.setContext(null);

 
      } catch (ConcurrentModificationException ccme) {
      	String msg = "Concurrent Modification Exceltion for [business object]='" + boClass +  "'";
		log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.eai.exception", new Object[] { msg }, ccme);
		break;
      } catch (Throwable t) {
		String msg = "Error cleaning up environment for [business object]='" + boClass +  "'";
		log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.eai.exception", new Object[] { msg }, t);
      }

      }
    }
      ConnectionManager.getConnectionManger().cleanupConnections(this);
      synchronized (backendObjects) {
		backendObjects.clear();	
      }
    } catch (BackendException ex) {
		String msg = "Error cleaning up connections";
		log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.eai.exception", new Object[] { msg }, ex);
    }
    // delete references to backend objects
	
	// delete backend context
	// context.clear();
	// mCurrentConfig = null;


  }

  /**
   * This method returns an unique id which is associated with the Backend Object
   * Manager. The id can be passed to the constructor of the Backend Object
   * Manager.
   * @return the id of the Backend Object Manager
   */
  public String getId() {
    if (mId == null)
      return this.toString();
    else
      return mId;
  }

  /**
   * This method can be used to add a connection listener to all object of
   * one type created by the BackendObjectManager
   * @param boType type of object the listener is added to
   * @param listener connection listener
   */
  public void addConnectionListener(String boType, ConnectionEventListener listener) {
      // give backend objects opportunity for clean up
      synchronized (backendObjects) {
		Set keySet = backendObjects.keySet();
		Iterator iterator = keySet.iterator();
		BackendBusinessObject beBO = null;
		String type = null;
		while (iterator.hasNext()) {
		  beBO = ((BackendBusinessObject)iterator.next());
		  type = (String)backendObjects.get(beBO);
		  if (type.equals(boType)) {
			beBO.getConnectionFactory().addConnectionEventListener(listener);
		  }
		}
      }
  }

  /**
   * Instantiates standalone backend objects and put their references
   * into the backend context. The key used in the backend context corresponds
   * to the type of the backend object.
   */
  private void createStandaloneBackendBusinessObjects()  {
    try {
      //Collection beConfs = ConfigReader.getConfigReader().getBackendBusinessObjectConfigs().values();
      Collection beConfs = mCurrentConfig.getBackendBusinessObjectConfigs().values();
      for (Iterator beIter = beConfs.iterator(); beIter.hasNext();) {
        BackendBusinessObjectConfig beConf = (BackendBusinessObjectConfig)beIter.next();
        if (beConf.isStandalone()) {
          String beType = beConf.getType();
          if(log.isDebugEnabled())
            log.debug("Creating stand alone backend object. [type]='" + beType + "'");
          BackendBusinessObject beObj = createBackendBusinessObject(beType);
          context.put(beType, beObj);
         }
      }
    } catch (BackendException bex) {
      log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.exception", new Object[] { bex }, bex);
    }
  }

  /**
   * Initializes the currently used backend configuration
   * This initialization routine uses the configuration management
   * introduced with release 3.1
   */
  private void initBackendConfig31() {

    ConfigManager mgr = ConfigManager.getConfigManager();

    if (mgr.isBackendConfigNew(mConfigId, mConfigData)) {

      if(log.isDebugEnabled())
        log.debug("Initializing configuration for [id]='" + mConfigId + "' [defaultdata]='" + mConfigData + "'");
 
        mCurrentConfig = mgr.getBackendConfig(mConfigId, mConfigData);
        try {
          ConnectionManager.initManagedConnectionFactories(mCurrentConfig);
        } catch (BackendException ex) {
          String msg = "Init backend configuration '" + getBackendConfigKey() + "' failed";
          log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.exception", new Object[] { msg }, ex);
      }
      } else {
        if(log.isDebugEnabled())
        log.debug("Using available configuration for [id]='" + mConfigId + "' [defaultdata]='" + mConfigData + "'");
        mCurrentConfig = mgr.getBackendConfig(mConfigId, mConfigData);
      }
  }

  /**
   * Initializes the currently used backend configuration
   * This initialization routine uses the Extended Configuration Management
   */
  private void initBackendConfig(Properties props) {

	ExtendedConfigManager xcmManager = ExtendedConfigManager.getExtConfigManager();
	String configKey = props.getProperty(SharedConst.XCM_CONF_KEY);

	if (log.isDebugEnabled())
		log.debug("Backend configuration [key]=' "+ configKey + "'");


	if (configKey == null) {
      String errMsg = "Error reading configuration information for [key]='null'";
      log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.xcm.exception", new Object[] { errMsg }, null);
      return;
	}
	
	// get current configuration
	ConfigContainer configContainer = xcmManager.getExistingConfig(configKey);

	if (configContainer == null) {
      String errMsg = "Configuration for [key]='" + configKey + "' does not exist";
      log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.xcm.exception", new Object[] { errMsg }, null);
	  return;	
	}

	// check if configuration for the given key already available
	BackendConfigManager backendConfMgr = BackendConfigManager.getConfigManager();
	
	if (backendConfMgr.isNewBackendConfig(configKey)) {
		if (log.isDebugEnabled())
			log.debug("Retrieving new backend configuration for [key]='" + configKey + "'");
		// get path to root eai-config file
		String eaiConfigPath = (String)ExtendedConfigInitHandler.getFilesConfig().getPaths().get(XCM_FILE_PROPERTY_NAME);
		// get configuration file as Input Stream
		InputStream is = configContainer.getConfigAsStream(eaiConfigPath);
		// read new configuration 
		if (is == null) {
	      String errMsg = "Input stream for [path]='" + eaiConfigPath + "' is null";
	      log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.xcm.exception", new Object[] { errMsg }, null);
		}
		mCurrentConfig = backendConfMgr.getNewBackendConfig(configKey, is);
		mCurrentConfig.setId(configKey);
	     try {
	          ConnectionManager.initManagedConnectionFactories(mCurrentConfig);
	        } catch (BackendException ex) {
	          String msg = "Init backend configuration '" + getBackendConfigKey() + "' failed";
	          log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.exception", new Object[] { msg }, ex);
	      }

	} else {
		if (log.isDebugEnabled())
			log.debug("Using existing backend configuration for [key]='" + configKey + "'");
		// read existing backend config
		mCurrentConfig = backendConfMgr.getExistingBackendConfig(configKey);
	}


  }

  /**
   * Returns the backend configuration used by this backend object manager
   * @return the backend configuration used by this backend object manager
   */
  public BackendConfig getBackendConfig() {
    return mCurrentConfig;
  }

  /**
   * Returns a key identifying a backend configuration
   * @return a key identifying a backend configuration
   */
  public String getBackendConfigKey() {
		return mCurrentConfig.getId() + mCurrentConfig.getDefaultdata();
  }
}  
