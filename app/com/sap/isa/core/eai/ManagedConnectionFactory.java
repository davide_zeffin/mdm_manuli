/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      06 March 2001
*****************************************************************************/
package com.sap.isa.core.eai;

import java.util.Map;
import java.util.Properties;

/**
 * An ConnectionFactory which creates backend specific managed connections
 */
public interface ManagedConnectionFactory {

  /**
   * Gets a managed connection
   * @param conConfigName name of connection configuration which should be
   *    used to create the managed connection
   * @return a managed connection
   */
  public ManagedConnection getManagedConnection(String conConfigName)
      throws BackendException;

  /**
   * Gets a managed connection
   * @param conConfigName name of connection configuration which should be
   *    used to create the managed connection
   * @param conDefProps connection properties which overwrite/complete properties
   *    set in the connection configuration file of the connection manager
   * @return a managed connection
   */
  public ManagedConnection getManagedConnection(String conConfigName, Properties conDefProps)
      throws BackendException;

  /**
   * Returns a managed connection. An existing connection configuration is used.
   * The passed set of properties extends/overwrites data set of the connection
   * definition which belongs to the connection configuration
   */

  /**
   * Adds a connection configurations to the connection factory
   * @param a set of connection configurations
   */
  public void addConnectionConfigs(Map conConfigs);

  /**
   * Inititalizes a managed connection factory. This function can be called
   * multiple times. If it is not called for the first time, then the
   * configuration of the connection factory should be updated
   * @param connectionDefinitions An array of backend connection configurations
   *
   */
  public void initConnections(Map connectionDefinitions)
        throws BackendException;

  /**
   * Returns a physical connection specified by the connection definition name
   * @param conDefName Name of the connection definition.
   */
  public Object getConnection(String conDefName) throws BackendException;

  /**
   * Returns a physical connection specified by the connection definition name.
   * An additional set of properties extends/overwrites existing connection properties
   * @param conDefName Name of the connection definition.
   * @param conDefProps connection properties which overwrite/complete properties
   *    set in the connection configuration file of the connection manager
   */
  public Object getConnection(String conDefName, Properties conDefProps) throws BackendException;

  /**
   * This method can be used to check if the connection factory can provide
   * a connection for the provided connection parameters. If this is not
   * the case than the connection factory has to be initialized first
   * by calling the @see #initConnection(ConnectionConfig, Map)
   * @param conDef connection definition
   */
  public boolean isConnectionAvailable(ConnectionDefinition conDef);

  /**
   * Specifies the name of the factory
   * @param factoryName type of factory
   */
  public void setFactoryName(String factoryName);

  /**
   * Gets the name of the factory
   * @return name of factory
   */
  public String getFactoryName();

  /**
   * Sets a set of properties
   * @param props Properties which can be used for additional configuratio
   *        of a factory
   */
  public void setProperties(Properties props);
}