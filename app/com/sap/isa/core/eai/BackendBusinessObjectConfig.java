/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      15 February 2001
*****************************************************************************/
package com.sap.isa.core.eai;

// java imports
import java.util.Map;
import java.util.Properties;

import com.sap.isa.core.eai.config.Config;
import com.sap.isa.core.eai.config.ConfigBase;

/**
 * This class provides information about an backend business object which
 * is necessary during object creation
 */

public class BackendBusinessObjectConfig extends ConfigBase {

  /**
   * The value of this constant can be used to specify the 'STANDALONE'
   * attribute when calling setAttributes() method
   */
  public static final String ATTR_STANDALONE_STRING = "standalone";

  private static final String PROPNAME_IS_STANDALONE = "isStandalone";
  private static final String PROPNAME_NAME = "name";
  private static final String PROPNAME_CLASS_NAME = "classname";
  private static final String PROPNAME_CONNECTION_FACTORY_NAME = "connectionFactoryName";
  private static final String PROPNAME_DEFAULT_CONNECTION_NAME = "defaultConnectionName";
  private static final String PROPNAME_TYPE = "type";
  private static final String PROPNAME_PROPERTIES = "properties";
//  private static final String PROPNAME_ATTR = "attr";

/*  private boolean isStandalone = false;
  private String name;
  private String className;
  private String connectionFactoryName;
  private String defaultConnectionName;
  private String type;
  private Properties params = new Properties();
  private String mAttr;
*/
  /**
   * Constructor
   */
  public BackendBusinessObjectConfig() {
    getInternalProps();
  }

  public BackendBusinessObjectConfig(Map data) {
    mData = data;
    getInternalProps();
  }

  /**
   * Specifies the name of the backend business object
   * @param name name of backend business object
   */
  public void setName(String name) {
    mData.put(PROPNAME_NAME, name);
  }

  /**
   * Returns name of backend business object
   * @return name of backend business object
   */
  public String getName() {
    return (String)mData.get(PROPNAME_NAME);
  }

  /**
   * Specifies the implementation class of this backend business object
   * @param className name of impelementation class
   */
  public void setClassName(String className) {
    mData.put(PROPNAME_CLASS_NAME, className);
  }

  /**
   * Returns name of implementation class of this backend business object
   */
  public String getClassName() {
    return (String)mData.get(PROPNAME_CLASS_NAME);
  }

  /**
   * Specifies the name of connection which should be uses as a default
   * for this backend business object
   * @param defaultConnectionName name of connection used as default connection
   *      for this backend business object
   */
  public void setDefaultConnectionName(String defaultConnectionName) {
    mData.put(PROPNAME_DEFAULT_CONNECTION_NAME, defaultConnectionName);
  }

  /**
   * @return name of connection used as default connection for this backend business object
   */
  public String getDefaultConnectionName() {
    return (String)mData.get(PROPNAME_DEFAULT_CONNECTION_NAME);
  }

  /**
   * Sets the name of the connection factory which should be used to create the
   * default connection
   * @param connectionFactoryName name of connection factory
   */
  public void setConnectionFactoryName(String connectionFactoryName) {
    mData.put(PROPNAME_CONNECTION_FACTORY_NAME, connectionFactoryName);
  }

  /**
   * Returns the name of the connection factory used to create the default
   * connection
   * @return the name of the connection factory used to create the default connection
   */
  public String getConnectionFactoryName() {
    return (String)mData.get(PROPNAME_CONNECTION_FACTORY_NAME);
  }

  /**
   * Sets the type of the business object
   * @param type type of business object
   */
  public void setType(String type) {
    mData.put(PROPNAME_TYPE, type);
  }

  /**
   * Gets the type of the business object
   * @return type of business object
   */
  public String getType() {
    return (String)mData.get(PROPNAME_TYPE);
  }


  /**
   * Adds a property
   * @param name name of property
   * @param value value of property
   */
  public void addProperty(String name, String value) {
    getInternalProps().setProperty(name, value);
  }

  /**
   * Returns a set of properties which are associated with the backend
   * business object
   * @return a set of properties
   */
  public Properties getProperties() {
    return getInternalProps();
  }

  /**
   * Sets properties which are associated with the backend
   * business object
   * @param a set of properties
   */
  public void setProperties(Properties params) {
    mData.put(PROPNAME_PROPERTIES, params);
  }

  /**
   * Sets attributes associated with the Backend Object.
   * The attributes are represented as an string and habe the following
   * format: Att1, Attr2, Attr3, ...,
   * @param attr Attributes reprented as String
   */
  public void setAttributes(String attr) {
    if (attr == null || attr.length() == 0)
      return;
    attr = attr.toLowerCase();
    if (attr.indexOf(ATTR_STANDALONE_STRING) != -1) {
     mData.put(PROPNAME_IS_STANDALONE, "true");
    }

  }

  /**
   * Sets attributes associated with the Backend Object.
   * The attributes are represented as an string and habe the following
   * format: Att1, Attr2, Attr3, ...,
   * @param attr Attributes reprented as String
   */
  public String getAttributes() {
      return (String)mData.get(PROPNAME_IS_STANDALONE);
  }


  /**
   * Returns true if the Object has the 'STANDALONE' attribute
   * set
   * @return Returns true if the Object has the 'STANDALONE' attribute set
   */
  public boolean isStandalone() {
    String attr = (String)mData.get(PROPNAME_IS_STANDALONE);
    if (attr == null)
      return false;
    return Boolean.valueOf(attr).booleanValue();
  }

  /**
   * Returns a clone of the configuration
   */
  public Config getConfigClone() {
    return new BackendBusinessObjectConfig(getClonedData());
  }

  /**
   * Returns the internal properties object
   * @param the internal properties object
   */
  private Properties getInternalProps() {
    if (mData.get(PROPNAME_PROPERTIES) == null)
      mData.put(PROPNAME_PROPERTIES, new Properties());

    return (Properties)mData.get(PROPNAME_PROPERTIES);
  }

  /**
   * Returns a XML String representation of this backend business object
   * @return XML String representation of this backend business object
   */
   public String toString() {
    StringBuffer sb = new StringBuffer();
    Properties params = new Properties();
    if (getName() != null)
      params.setProperty("name", getName());

    if (getType() != null)
      params.setProperty("type", getType());

    if (getClassName() != null)
      params.setProperty("className", getClassName());
    if (getAttributes() != null)
      params.setProperty("attributes", getAttributes());

    if (getConnectionFactoryName() != null)
      params.setProperty("connectionFactoryName", getConnectionFactoryName());

    if (getDefaultConnectionName() != null)
      params.setProperty("defaultConnectionName ", getDefaultConnectionName());


    sb.append(xmlHelper.createTag("businessObject", params, false));
    sb.append(getAsXMLString(getInternalProps()));
    sb.append(xmlHelper.createEndTag("businessObject"));
    return sb.toString();
  }

}