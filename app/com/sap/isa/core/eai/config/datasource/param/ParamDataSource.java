/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      19 February 2001
*****************************************************************************/
package com.sap.isa.core.eai.config.datasource.param;

// java imports
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.digester.Digester;

import com.sap.isa.core.eai.config.datasource.DataSource;
import com.sap.isa.core.eai.config.datasource.DataSourceConfig;
import com.sap.isa.core.eai.config.datasource.DataSourceProvider;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.XMLHelper;

/**
 * This class represents a data source which provides information based
 * on a property configuration file. An example of such an configuration
 * file is the eai-data.xml file
 */

public class ParamDataSource implements DataSource {

  protected static IsaLocation log = IsaLocation.
        getInstance(ParamDataSource.class.getName());
  private XMLHelper xmlHelper = new XMLHelper();
  /**
   * Constant containing 'null'
   */
  public static final String NULL = "null";

  /**
   * The type of this data source
   * value: paramdata
   */
  public static final String DATA_SOURCE_TYPE = "paramdata";

  private String mPath;
  private int debugLevel = 0;
  private ParamDataSourceConfig mConfig;
  private final DataSourceProvider mDataSourceProvider;
  private Set mProcessedInheritance = new HashSet();
  private InputStream mIs;

  /**
   * Constructor
   * @param is the file input stream for the configuration file
   * @param dataSourceProvider a data provied which can proved other data sources
   */
  public ParamDataSource(InputStream is, DataSourceProvider dataSourceProvider) {
    if (log.isDebugEnabled())
      log.debug("Creating new 'paramdata' data source; [datasourceprovider]='"
          + dataSourceProvider + "[fileinputstream]='" + is + "'");
    mDataSourceProvider = dataSourceProvider;
    mIs = is;
    init();
  }
  /**
   * Constructor
   * @param path the path to the data file
   * @param dataSourceProvider reference to data source provider
   */
  public ParamDataSource(String path, DataSourceProvider dataSourceProvider) {
    if (log.isDebugEnabled())
      log.debug("Creating new 'paramdata' data source; [datasourceprovider]='"
                + dataSourceProvider + "[path]='" + path + "'");
    mDataSourceProvider = dataSourceProvider;
    mPath = path;
    init();
  }

  private void init() {
    try {
      // read configuration file
      readDataFile();
      // register data sourcese to data source provider
      registerDataSources();
      // retrieve dynamic values
      processDynValues();
      // process inherited configurations
      processInheritedConfigs();
    } catch (Exception ex) {
      log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.exception", new Object[] { "Error in ParamDataSource" }, ex);
      ex.printStackTrace();
    }
  }

  /**
   * Sets a parameter configuration
   * @param config parameter configuration
   */
  public void setParamDataSourceConfig(ParamDataSourceConfig config) {
    mConfig = config;
  }
  /**
   * This method is used to retrieve a property value from the data source
   * The passed value can have the following formats:
   *  [id of configuration]/[id of param set]/[name of parameter]
   *  example:
   *  'default/logon/password'
   * @param propName path to propery
   * @return a property value
   */
  public String getProperty(String propName) {
      String retVal = "param not found = [" + propName + "]";
      int index = propName.lastIndexOf("/");
      if (index == -1)
        return retVal;

      String paramName = propName.substring(index + 1, propName.length());
      Properties props = getProperties(propName);
      if (props == null)
        return retVal;
      String value = props.getProperty(paramName);
      if (value == null)
        return retVal;

      return value;

  }



    /**
     * Reads the configuration file
     * @throws Exception if reading configuration file fails
     */
    private void readDataFile() throws Exception {
    if(log.isDebugEnabled())
      log.debug("Reading parameter file");
    // new Struts digester
    Digester digester = new Digester();
    digester.setDebug(debugLevel);

    digester.push(this);

    ParamDataSourceConfig dataConfig = new ParamDataSourceConfig();

    // create a new data source config object
    digester.addObjectCreate("data",
        "com.sap.isa.core.eai.config.datasource.param.ParamDataSourceConfig");

    // create a new data source config object
    digester.addObjectCreate("data/datasources/datasource",
        "com.sap.isa.core.eai.config.datasource.DataSourceConfig");
    // set all properties which belong to data source config object
    digester.addSetProperties("data/datasources/datasource");

    // add static name/value pairs to source config object
    digester.addCallMethod(
        "data/datasources/datasource/param",
        "addProperty", 2, new String[] {"java.lang.String", "java.lang.String"});

    digester.addCallParam("data/datasources/datasource/param"
        , 0, "name");
    digester.addCallParam("data/datasources/datasource/param"
        , 1, "value");

    // add data sources to configuration
   digester.addSetNext("data/datasources/datasource",
      "addDataSourceConfig", "com.sap.isa.core.eai.config.datasource.DataSourceConfig");

    // add parameter config
    digester.addObjectCreate("data/configs/config",
          "com.sap.isa.core.eai.config.datasource.param.ParamConfig");
    // set properties for property configuration
    digester.addSetProperties("data/configs/config");

    // add property config
    digester.addObjectCreate("data/configs/config/params",
          "com.sap.isa.core.eai.config.datasource.param.ParamSet");
    // set all properties which belong to data parameter config object
    digester.addSetProperties("data/configs/config/params");

    // add properties to parameter set
    digester.addCallMethod(
        "data/configs/config/params/param",
        "addProperty", 2, new String[] {"java.lang.String", "java.lang.String" });

    digester.addCallParam("data/configs/config/params/param"
        , 0, "name");
    digester.addCallParam("data/configs/config/params/param"
        , 1, "value");

    // add properties to property configuration
   digester.addSetNext("data/configs/config/params",
      "addParamSet", "com.sap.isa.core.eai.config.datasource.param.ParamSet");

    // add property configuration to data configuration
   digester.addSetNext("data/configs/config",
      "addParamConfig", "com.sap.isa.core.eai.config.datasource.param.ParamConfig");

    // add whole param data to this data source
   digester.addSetNext("data",
     "setParamDataSourceConfig", "com.sap.isa.core.eai.config.datasource.param.ParamDataSourceConfig");

    try {
      if (mPath != null) {
        FileInputStream fis = new FileInputStream(mPath);
        digester.parse(mPath);
      }
      if (mIs != null)
        digester.parse(mIs);
    } catch (Throwable ex) {
      log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.exception", new Object[] { ex }, ex);
    }
    if (log.isDebugEnabled())
      log.debug("Reading parameter file successful");
  }

  /**
   * This methods replaces place holders for dynamic data by retrieving
   * the actual data from appropriate data sources
   */
  private void processDynValues() {
  if (log.isDebugEnabled())
    log.debug("Begin Executing processDynValues()");

  if (mConfig != null) {
    Map paramConfigs = mConfig.getParamConfigs();

    if (paramConfigs == null || paramConfigs.size() == 0)
      return;
    // loop over all parameter configurations
    for(Iterator configIter = paramConfigs.keySet().iterator(); configIter.hasNext();) {
      String configKey = (String)configIter.next();
      ParamConfig paramConfig = (ParamConfig)paramConfigs.get(configKey);
      paramConfig.setDataSourceProvider(mDataSourceProvider);
      paramConfig.processDynamicData();
      }
    } else
      log.debug("No configs to process");
    if (log.isDebugEnabled())
      log.debug("End Executing processDynValues()");

  }

  /**
   * This method checks if the value of a parameter is dynamic. This is only
   * the case if value has the following format:
   * 'datasource:path' and the data source exists. If the value is dynamic
   * then the value is retrieved from a appropriate data source
   * @param value value of a parameter
   */
/*  private String getDynamicValue(String paramValue) {
    // check if the value is dynamic
    if (paramValue == null)
      return NULL;

    int colonPos = paramValue.indexOf(":");
    if (colonPos == -1)
      return paramValue;

    // it seems that there is a data source
    String dataSourceType = paramValue.substring(0, colonPos);

    // check if there is an appropriate data source which can provide the data
    Map dataSourceConfigs = mConfig.getDataSourceConfigs();
    if (dataSourceConfigs == null)
      return paramValue;

    DataSourceConfig config = (DataSourceConfig)dataSourceConfigs.get(dataSourceType);

    // there is not suitable data source
    if (config == null)
      return paramValue;

    // get param value without data provider
    String realParamValue = paramValue.substring(colonPos + 1, paramValue.length());

    // Get data source from data provider
    DataSource dataSource = mDataSourceProvider.getDataSource(config);

    return dataSource.getProperty(realParamValue);
 }
*/
 /**
  * Processes configurations which inherit from other configurations
  */
  private void processInheritedConfigs() {
    if (log.isDebugEnabled())
      log.debug("Begin executing processInheritedConfigs()");

    // check if there are configurations which have a base configuration
    if (mConfig != null) {
      Map paramConfigs = mConfig.getParamConfigs();
      for(Iterator configIter = paramConfigs.keySet().iterator();configIter.hasNext();) {
        String key = (String)configIter.next();
        ParamConfig currentConfig = (ParamConfig)paramConfigs.get(key);
        processInheritedConfig(currentConfig);
/*        String base = currentConfig.getBase();
        if (base != null && base.length() > 0) {
          ParamConfig baseConfig = (ParamConfig)mConfig.getParamConfigs().get(base);
          currentConfig.mixConfiguration(baseConfig);
        }
*/
      }
    } else
      log.debug("No configurations to process");
    if (log.isDebugEnabled())
      log.debug("End executing processInheritedConfigs()");
  }

  /**
   * Used to mix configurations
   * @param currentConfig processes inheritance recursively
   */
  private void processInheritedConfig(ParamConfig currentConfig) {
    String base = currentConfig.getBase();
    log.debug("Processing inherited configuration: " + currentConfig.getId());
    if (base != null && base.length() > 0) {
        ParamConfig baseConfig = (ParamConfig)mConfig.getParamConfigs().get(base);
        // check if config has predecessors
        processInheritedConfig(baseConfig);
        currentConfig.mixConfiguration(baseConfig);
        // mark config as already processed
        mProcessedInheritance.add(currentConfig.getId());
    }

  }


  /**
   * Returns a string representation of this data source
   * @return a string representation of this data source
   */
  public String toString() {
    StringBuffer sb = new StringBuffer();
    sb.append(xmlHelper.createTag("data", false));
    if (mConfig != null) {
    Map dataSources = mConfig.getDataSourceConfigs();
    if (dataSources != null && dataSources.size() > 0) {
      sb.append(xmlHelper.createTag("datasources", false));
      for (Iterator dataSourceIter = dataSources.keySet().iterator(); dataSourceIter.hasNext();) {
        String id = (String)dataSourceIter.next();
        sb.append(((DataSourceConfig)dataSources.get(id)).toString());
      }
      sb.append(xmlHelper.createEndTag("datasources"));
    } else
      sb.append(xmlHelper.createEndTag("datasources"));


    sb.append(xmlHelper.createTag("configs", false));
    Map paramConfigs = mConfig.getParamConfigs();
    for (Iterator configIter = paramConfigs.keySet().iterator(); configIter.hasNext();) {
      String configKey = (String)configIter.next();
      ParamConfig paramConfig = (ParamConfig)paramConfigs.get(configKey);
      sb.append(paramConfig.toString());
    }
    sb.append(xmlHelper.createEndTag("configs"));
    sb.append(xmlHelper.createEndTag("data"));
    }
    return sb.toString();
  }

  /**
   * This method is used to retrieve a set of properties from a data source
   * The format of the propName is:
   * [id of configuration]/[id of param set]
   * @param propName name specifying a set of properties
   * @return a set of properties; an empty set if something goes wrong
   */
  public Properties getProperties(String propName) {
    // tokenize name
    Properties retProps = new Properties();

    int index = propName.indexOf("/");
    if (index == -1)
      return retProps;
    String configId = propName.substring(0, index);
    propName = propName.substring(index + 1, propName.length());
    index = propName.indexOf("/");
    if (index == -1)
      return retProps;
    String paramSetName = propName.substring(0, index);

    if (configId != null && paramSetName != null) {
      // get configuration
      ParamConfig paramConfig = (ParamConfig)mConfig.getParamConfigs().get(configId);
      if (paramConfig == null)
        return retProps;
      ParamSet paramSet = paramConfig.getParamSet(paramSetName);
      if (paramSet != null)
        retProps = paramSet.getProperties();
    }
      return retProps;
  }


  /**
   * Registers configured data sources to data source provider
   */
  private void registerDataSources() {
    if (log.isDebugEnabled())
      log.debug("Begin executing registerDataSources()");
    if (mConfig != null) {
      Map dataSourceConfigs = mConfig.getDataSourceConfigs();
      for(Iterator dataSourceIter = dataSourceConfigs.keySet().iterator(); dataSourceIter.hasNext();) {
        String key = (String)dataSourceIter.next();
        if (log.isDebugEnabled())
          log.debug("Registering data source '" + key + "'");
        DataSourceConfig config = (DataSourceConfig)dataSourceConfigs.get(key);
        mDataSourceProvider.registerDataSource(config);
      }
    } else
      log.debug("No data sources to register");
    if (log.isDebugEnabled())
      log.debug("End executing registerDataSources() finished");
  }

  /**
   * This method returns true if the property name can be used to retrieve
   * a set of properties
   * @param propName name of property. If the name contains wildcards then
   *                 this property name can be used to retrieve a set of properties
   * @return true if the property name can be used to retrieve a set of properties
   */
  public boolean isMultiSelect(String propName) {
    if (propName != null && propName.indexOf("*") != -1)
      return true;
    else
      return false;
  }
}
