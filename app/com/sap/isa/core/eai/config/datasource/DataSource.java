package com.sap.isa.core.eai.config.datasource;
/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      19 February 2001
  $Id:  $
  $Revision:  $
  $Change:  $
  $DateTime:  $
  $Author:  $
*****************************************************************************/

import java.util.Properties;

/**
 * This interface is used to access data configuration data
 */
public interface DataSource {

  /**
   * This method is used to retrieve a property value from a data source
   * @param propName name of property
   * @return a property value
   */
  public String getProperty(String propName);

  /**
   * This method is used to retrieve a set of properties from a data source
   * @param propName name specifying a set of properties
   * @return a set of properties
   */
  public Properties getProperties(String propName);

  /**
   * This method returns true if the property name can be used to retrieve
   * a set of properties
   * @param propName name of property. If the name contains wildcards then
   *                 this property name can be used to retrieve a set of properties
   * @return true if the property name can be used to retrieve a set of properties
   */
  public boolean isMultiSelect(String propName);
}