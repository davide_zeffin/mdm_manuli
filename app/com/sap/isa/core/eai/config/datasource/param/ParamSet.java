/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      19 February 2002
  $Id:  $
  $Revision: $
  $Change: $
  $DateTime: $
  $Author: $
*****************************************************************************/
package com.sap.isa.core.eai.config.datasource.param;

// isa imports
import java.util.Enumeration;
import java.util.Map;
import java.util.Properties;

import com.sap.isa.core.eai.config.Config;
import com.sap.isa.core.eai.config.ConfigBase;
import com.sap.isa.core.util.XMLHelper;

/**
 * This class encapsulates a named set of properties
 */
public class ParamSet extends ConfigBase{

  public static final String PARAMNAME_PARAMSET = "ParamSet";

  private XMLHelper xmlHelper = new XMLHelper();

  /**
   * Constructor
   */
  public ParamSet() { }

  /**
   * Constructor
   * @param data internal data
   */
  public ParamSet(Map data) {
    mData = data;
  }


  /**
   * Adds a property
   * @param name name of property
   * @param value property value
   */
  public void addProperty(String name, String value) {
      getInternalProps().setProperty(name, value);
  }

  /**
   * Returns a property
   * @param name name of property
   * @return the value which corresponds to the given name
   */
  public String getProperty(String name) {
    return getInternalProps().getProperty(name);
  }

  /**
   * Returns the properties contained in this property set
   * @return the properties contained in this property set
   */
  public Properties getProperties() {
    return getInternalProps();
  }

  /**
   * Creates a clone of this object
   * @return a clone of this object
   */
  public Config getConfigClone() {
    Map data = getClonedData();
    ParamSet clone = new ParamSet(data);
    return clone;
  }

  /**
   * Resturns a string representation of this parameter set
   * @return a string representation of this parameter set
   */
  public String toString() {
    StringBuffer sb = new StringBuffer();
    if (getId() != null)
    	sb.append(xmlHelper.createTag("params", "id", getId(), false));
    else
    	sb.append(xmlHelper.createTag("params", false));
    for(Enumeration propEnum = getInternalProps().keys(); propEnum.hasMoreElements();) {
      String propKey = (String)propEnum.nextElement();
      String propValue =  getInternalProps().getProperty(propKey);
      Properties props = new Properties();
      props.setProperty("name", propKey);
      props.setProperty("value", propValue);
      sb.append(xmlHelper.createTag("param", props, true));
    }
    sb.append(xmlHelper.createEndTag("params"));
    return sb.toString();
  }

  /**
   * Returns the internal properties object
   * @return the internal properties object
   */
  private Properties getInternalProps() {
    if (mData.get(PARAMNAME_PARAMSET) == null)
      mData.put(PARAMNAME_PARAMSET, new Properties());

    return (Properties)mData.get(PARAMNAME_PARAMSET);
  }

}
