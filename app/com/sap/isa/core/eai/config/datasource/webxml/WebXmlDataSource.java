package com.sap.isa.core.eai.config.datasource.webxml;

/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      19 February 2001
  $Id: //java/sapm/esales/30/src/com/sap/isa/core/eai/ConfigReader.java#3 $
  $Revision: #3 $
  $Change: 23641 $
  $DateTime: 2001/07/03 14:30:25 $
  $Author: d025909 $
*****************************************************************************/
import java.util.Properties;

import com.sap.isa.core.eai.config.datasource.DataSource;
import com.sap.isa.core.eai.config.datasource.DataSourceConfig;

/**
 * This data source is used to retrieve data from the web.xml deployment
 * descriptor
 */
public class WebXmlDataSource implements DataSource {

  /**
   * The type of this data source
   * value: webxml
   */
  public static final String DATA_SOURCE_TYPE = "webxml";

  private static final String WILDCARD = "*";

  private DataSourceConfig mConfig;
  private final Properties mContextProps;

  /**
   * Constructor
   * @param contextProps web xml context properties
   */
  public WebXmlDataSource(Properties contextProps) {
    mContextProps = contextProps;
  }


  /**
   * This method is used to retrieve a property value from a data source
   * @param contextParamName name of context parameter
   * @return a property value. If no value is found then null is returned.
   */
  public String getProperty(String contextParamName) {
    //return "testwebxml: " + contextParamName;#
    String paramValue = "context param not found = [" + contextParamName + "]";
    if (mContextProps == null)
      return paramValue;

    if (mContextProps.getProperty(contextParamName) == null)
      return paramValue;
    else
      return mContextProps.getProperty(contextParamName);
  }

  /**
   * This method is used to retrieve a set of properties from a data source
   * This method can be used when working with wildcards
   * You can use the following format:
   * [*][partial name of parameter]
   * Example:
   * *.jco.sp.eai.core.isa.sap.com
   * Returns all parameters ending with '.jco.sp.eai.core.isa.sap.com'
   * @param contextParamName name specifying a set of properties
   * @return a set of properties
   */
  public Properties getProperties(String contextParamName) {
    // TODO
    Properties retVal = new Properties();
/*
    if (contextParamName == null)
      return retVal;
    // check if the propery name contains a wildcard
    int index = contextParamName.indexOf(WILDCARD);
    if (index == -1) {
      // no wildcard; return single parameter
      String contextParam = getProperty(contextParamName);
      if (contextParam != null) {
        retVal.setProperty(contextParamName, contextParam);
        return retVal;
      } else
        return retVal;
    }
    // retrieve a set of properties
    String subName = contextParamName.substring(index, contextParamName.length());
*/
    return retVal;

  }

  /**
   * This method returns true if the property name can be used to retrieve
   * a set of properties
   * NOTE: The return value in this implementation is always FALSE
   * @param propName name of property. If the name contains wildcards then
   *                 this property name can be used to retrieve a set of properties
   * @return true if the property name can be used to retrieve a set of properties
   */
  public boolean isMultiSelect(String propName) {
    return false;
  }

}
