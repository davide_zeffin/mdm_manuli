/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      19 February 2002
*****************************************************************************/
package com.sap.isa.core.eai.config.datasource.param;

// java imports
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import com.sap.isa.core.eai.config.Config;
import com.sap.isa.core.eai.config.ConfigBase;
import com.sap.isa.core.util.XMLHelper;


/**
 * This class represents a named configuration consisting of multiple
 * sets of named parameters
 */

public class ParamConfig extends ConfigBase {

  private XMLHelper xmlHelper = new XMLHelper();
  private Map mParamSets = new HashMap();

  /**
   * Constructor
   */
  public ParamConfig() {
    super();
  }


  /**
   * Constructor
   * @param data internal data
   */
  public ParamConfig(Map data) {
    mData = data;
  }

  /**
   * Adds a set of parameters to this configuration
   * @param params a set of parameters
   */
  public void addParamSet(ParamSet params) {
    mData.put(params.getId(), params);
    mParamSets.put(params.getId(), params);
  }

  /**
   * Returns the parameters belonging to this parameter configuration
   * @return the parameters belonging to this parameter configuration
   */
  public Map getParamSets() {
    return mParamSets;
  }

  /**
   * Get a specific parameter configuration
   * @param id id of configuration
   * @return parameter configuration for a given key
   */
  public ParamSet getParamSet(String id) {
    return (ParamSet)mData.get(id);
  }

  /**
   * Creates a clone of this object.
   * @return a clone of this object
   */
  public Config getConfigClone() {
    mData = getClonedData();
    ParamConfig clone = new ParamConfig(mData);
    return clone;
  }

  /**
   * Returns a string representation of this parameter configuration
   * @return a string representation of this parameter configuration
   */
  public String toString() {
    StringBuffer sb = new StringBuffer();
    Properties props = new Properties();
    if (getBase() != null)
      props.setProperty("base", getBase());
    if (getId() != null)
      props.setProperty("id", getId());
    sb.append(xmlHelper.createTag("config", props, false));

    for(Iterator configIter = mData.keySet().iterator(); configIter.hasNext();) {
      String configKey = (String)configIter.next();
      Object value = mData.get(configKey);
      if (value instanceof ParamSet) {
        ParamSet paramSet = (ParamSet)value;
        sb.append(paramSet.toString());
      }
    }
    sb.append(xmlHelper.createEndTag("config"));
    return sb.toString();
  }
}