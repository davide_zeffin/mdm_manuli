/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      20 February 2002
*****************************************************************************/
package com.sap.isa.core.eai.config.datasource;

/**
 * This interface provides a means how an data provider can be accessed
 */

public interface DataSourceProvider {

    /**
     * Gets a data source
     * @param type type of data source
     * @return data source of the requested type
     */
    public DataSource getDataSource(String type);

    /**
     * Gets a data source
     * @param config data source config
     * @return data source of the requested type
     */
    public DataSource getDataSource(DataSourceConfig config);


    /**
     * Registers a data source to this data source provider
     * @param config configuration of data source to register
     */
    public void registerDataSource(DataSourceConfig config);

    /**
     * Returns true if the passed data source type is known to the data source
     * provider
     * @param type type of data source
     * @return true if the passed data source type is known to the data source
     * provider
     */
    public boolean isDataSource(String type);
}