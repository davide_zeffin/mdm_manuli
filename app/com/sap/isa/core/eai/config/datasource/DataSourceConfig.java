/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      19 February 2001
*****************************************************************************/
package com.sap.isa.core.eai.config.datasource;

// java imports
import java.util.Enumeration;
import java.util.Properties;

import com.sap.isa.core.util.XMLHelper;

/**
 *  This class containes configuration information for a data source
 */

public class DataSourceConfig {

  private String mType;
  private String mName;
  private String mSource;
  private String mClassName;
  private Properties mProps = new Properties();
  private XMLHelper xmlHelper = new XMLHelper();

  /**
   * Adds properties to this configuration
   * @param name name of property
   * @param value value of property
   */
  public void addProperty(String name, String value) {
    mProps.setProperty(name, value);
  }

  /**
   * Returns the type of the datasource
   * @return the type of the datasource
   */
  public String getType() {
    return mType;
  }

  /**
   * Returns the name of the datasoruce
   * @return the name of the datasoruce
   */
  public String getName() {
    return mName;
  }

  /**
   * Returns the a string indicating the source the data is retrieved from
   * @return a string indicating the source the data is retrieved from
   */
  public String getSource() {
    return mSource;
  }

  /**
   * Returns the class name of the data source
   * @return The class name of the data source
   */
  public String getClassName() {
    return mClassName;
  }

  /**
   * Sets the type of the datasource
   * @param type the type of the datasource
   */
  public void setType(String type) {
    mType = type;
  }

  /**
   * Sets the name of the datasoruce
   * @param name the name of the datasoruce
   */
  public void setName(String name) {
    mName = name;
  }

  /**
   * Sets a string indicating the source the data is retrieved from
   * @param source a string indicating the source the data is retrieved from
   */
  public void setSource(String source) {
    mSource = source;
  }

  /**
   * Sets the class name of the data source
   * @param className The class name of the data source
   */
  public void setClassName(String className) {
    mClassName = className;
  }

  /**
   * Returns a string representation of this data source configuration
   * @return a string representation of this data source configuration
   */
  public String toString() {
    StringBuffer sb = new StringBuffer();

    Properties props = new Properties();
    props.setProperty("source", mSource);
    props.setProperty("name",   mName);
    props.setProperty("type",   mType);

    if (mProps.size() > 0) {
      sb.append(xmlHelper.createTag("datasource", props, false));
      for(Enumeration propEnum = mProps.keys(); propEnum.hasMoreElements();) {
        String key = (String)propEnum.nextElement();
        String value = mProps.getProperty(key);
        props = new Properties();
        props.setProperty("name", key);
        props.setProperty("value", value);
        sb.append(xmlHelper.createTag("parma", props, true));
      }
      sb.append(xmlHelper.createEndTag("datasource"));
    } else
      sb.append(xmlHelper.createTag("datasource", props, true));

    return sb.toString();

  }

}