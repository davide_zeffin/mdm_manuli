package com.sap.isa.core.eai.config.datasource.param;
  /*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      19 February 2002
*****************************************************************************/

// isa imports
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.sap.isa.core.eai.config.datasource.DataSourceConfig;
import com.sap.isa.core.util.XMLHelper;


/**
 * This container represents parameter configuration data
 */
public class ParamDataSourceConfig {

  private Map mParamConfigs      = new HashMap();
  private Map mDataSourceConfigs = new HashMap();
  private XMLHelper xmlHelper = new XMLHelper();

  /**
   * Adds a parameter configuration
   * @param config a parameter configuration
   */
  public void addParamConfig(ParamConfig config) {
    if (config == null)
      return;
    mParamConfigs.put(config.getId(), config);
  }

  /**
   * Returns a set of all ids of the parameter configuration
   * @return all ids of the parameter configuration
   */
  public Set getParamConfigIds() {
    return Collections.unmodifiableSet(mParamConfigs.keySet());
  }

  /**
   * Adds a datasource configuration
   * @param config a datasource configuration
   */
  public void addDataSourceConfig(DataSourceConfig config) {
    if (config == null)
      return;
    mDataSourceConfigs.put(config.getName(), config);
  }

  /**
   * Returns a map containing the parameter configurations
   * @return a map containing the parameter configurations
   */
  public Map getParamConfigs() {
    return mParamConfigs;
  }

  /**
   * Returns a map containing the known data source configurations
   * @return a map containing the known data sources configurations
   */
  public Map getDataSourceConfigs() {
    return mDataSourceConfigs;
  }

  /**
   * Returns a string representation of this object
   * @return a string representation of this object
   */
  public String toString() {
    StringBuffer sb = new StringBuffer();
    if (mDataSourceConfigs.size() > 0) {
      for(Iterator dataSourceIter = mDataSourceConfigs.keySet().iterator(); dataSourceIter.hasNext();) {
        DataSourceConfig config = (DataSourceConfig)mDataSourceConfigs.get((String)dataSourceIter.next());
        sb.append(config.toString());
      }
    }



    return sb.toString();
  }

}