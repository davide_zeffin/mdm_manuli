package com.sap.isa.core.eai.config;
/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      15 February 2001
*****************************************************************************/

// java imports
import java.util.Map;

import com.sap.isa.core.eai.config.datasource.DataSourceProvider;

/**
 * This interface has to be implemented by each container
 * storing configuration data
 */

public interface Config {

  public static final String PARAMNAME_ID = "id";
  public static final String PARAMNAME_BASE = "base";
  public static final String PARAMNAME_DEFAULT_DATASOURCE = "defaultdata";
  public static final String PARAMNAME_NAMESPACE = "namespace";

  /**
   * Specifies the id of this configuration
   * @param id the id of this configuration
   */
  public void setId(String id);

  /**
   * Returns the id of this configuration
   * @return the id of this configuration
   */
  public String getId();

  /**
   * Sets the id of the base configuration
   * @param id id of the base configuration
   */
  public void setBase(String id);

  /**
   * Gets the id of the base configuration
   * @return id of the base configuration
   */
  public String getBase();

  /**
   * Returns a clone of the configuration
   * @return a clone of the configuration
   */
  public Config getConfigClone();

  /**
   * This method is used to mix the existing configuration with
   * a base configuration. The passed configuration is used as base
   * configuration.
   * @param baseConfig base configuration
   */
  public void mixConfiguration(Config baseConfig);

  /**
   * Gets a clone of the internal data
   * @return a clone of the internal data
   */
  public Map getClonedData();

  /**
   * Sets a data source provider which can be used to get references to
   * data sources. Data sources are used to retrieve dynamic values
   * @param dataSourceProvider a data source provider which can be used to get references to data sources
   */
  public void setDataSourceProvider(DataSourceProvider dataSourceProvider);

  /**
   * When this method is called, dynamic data is retrieved from the appropriate
   * data sources
   */
  public void processDynamicData();

  /**
   * Sets name of default data source for dynamic values
   * @param name of default data source for dynamic values
   */
  public void setDefaultDataSource(String name);

  /**
   * Gets name of default data source for dynamic values
   * @return name of default data source for dynamic values
   */
  public String getDefaultDataSource();


  /**
   * Sets a parameter value.
   * @param id id of parameter.
   * @param value value of parameter
   */
  public void setParameter(String id, Object value);

  /**
   * Gets a parameter value.
   * @param id id of parameter.
   * @return a parameter value.
   */
  public Object getParameter(String id);

  /**
   * Set namespace
   * @param name the namespace
   */
  public void setNamespace(String name);
}