/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      20 February 2002

  $Revision: #2 $
  $Date: 2001/07/16 $
*****************************************************************************/
package com.sap.isa.core.eai.config;

// isa imports
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import com.sap.isa.core.eai.config.datasource.DataSource;
import com.sap.isa.core.eai.config.datasource.DataSourceConfig;
import com.sap.isa.core.eai.config.datasource.DataSourceProvider;
import com.sap.isa.core.eai.config.datasource.param.ParamDataSource;
import com.sap.isa.core.eai.config.datasource.webxml.WebXmlDataSource;
import com.sap.isa.core.init.Initializable;
import com.sap.isa.core.init.InitializationEnvironment;
import com.sap.isa.core.init.InitializeException;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;


/**
 * This class is used as a factory of data sources. It has the knowledge
 * of the environment and can provide the data sources with the necessary
 * information.
 */

public class ConfigHandler implements Initializable {

  protected static IsaLocation log = IsaLocation.
        getInstance(ConfigHandler.class.getName());

  /**
   * Type of default data source
   */
  public static final String DEFAULT_DATA_SOURCE_TYPE = Config.PARAMNAME_DEFAULT_DATASOURCE;

  private static InitializationEnvironment mEnv;
  private static Map mDataSources = new HashMap();
  private static InternalDataSourceProvider mDataSourceProvider = new InternalDataSourceProvider(mDataSources);

  private static class InternalDataSourceProvider implements DataSourceProvider  {

  Map mDataSources;

  public InternalDataSourceProvider(Map dataSourcer) {
    mDataSources = dataSourcer;

    }
    /**
     * Gets a data source
     * @param type type of data source
     * @return data source of the requested type
     */
    public DataSource getDataSource(String type) {
      return (DataSource)mDataSources.get(type);
    }

    /**
     * Gets a data source for a given configuration
     * @param config configuration
     * @return data source of the requested type
     */
    public DataSource getDataSource(DataSourceConfig config) {
      return ConfigHandler.getDataSource(config);
    }


    public void registerDataSource(DataSourceConfig config) {
      ConfigHandler.registerDataSource(config);
    }
   /**
     * Returns true if the passed data source type is known to the data source
     * provider
     * @param type data source type
     * @return true if the passed data source type is known to the data source provider
     */
    public boolean isDataSource(String type) {
      if (type.equalsIgnoreCase(WebXmlDataSource.DATA_SOURCE_TYPE) ||
          type.equalsIgnoreCase(ParamDataSource.DATA_SOURCE_TYPE) ||
          type.equalsIgnoreCase(DEFAULT_DATA_SOURCE_TYPE))
        return true;

      return false;
    }

  }

    /**
    * Initialize the implementing component.
    *
    * @param env the calling environment
    * @param props   component specific key/value pairs
    * @exception InitializeException at initialization error
    */
    public void initialize(InitializationEnvironment env, Properties props)
             throws InitializeException {
      log.debug("ConfigHandler initialized");
      mEnv = env;

      InputStream is = env.getResourceAsStream(props.getProperty("eai-config-file"));
      ConfigManager confMgr = ConfigManager.getConfigManager();
      ConfigManager.setInputStream(is);
      ConfigManager.setDataSourceProvider(getDataSourceProvider());
    }
    /**
    * Terminate the component.
    */
    public void terminate() {
      mEnv = null;
      mDataSources = null;
      mDataSourceProvider = null;
    }
    /**
     * This method returns a data source according to the passed
     * data source configuration
     * @param config a data source configuration
     * @return a data source according to the passed data source configuration
     */
    public static DataSource getDataSource(DataSourceConfig config) {
      DataSource dataSource = null;
      if (config == null)
        return dataSource;
      if (config.getType().equalsIgnoreCase(WebXmlDataSource.DATA_SOURCE_TYPE))
        dataSource = getWebXmlDataSource(config);

      if (config.getType().equalsIgnoreCase(ParamDataSource.DATA_SOURCE_TYPE))
        dataSource = getParamDataSource(config);

      return dataSource;
    }

    /**
     * This method returns a data source according to the passed
     * data source configuration
     * @param config a data source configuration
     */
    public static void registerDataSource(DataSourceConfig config) {
      DataSource dataSource = null;
      if (config == null) {
        log.debug("Could not regitster data source = 'null'");
        return;
      }
      if (log.isDebugEnabled())
        log.debug("Registering data source '" + config.getName() + "'");
      getDataSource(config);
    }

    /**
     * Gets the data source provider
     * @return the data source provider
     */
    public static DataSourceProvider getDataSourceProvider() {
      return mDataSourceProvider;
    }

    /**
     * Returns a reference to a web xml data source. If there is already
     * such a data source then the exiting one is returned
     * @param config a data source configuration
     * @return a reference to a web xml data source
     */
    private static DataSource getWebXmlDataSource(DataSourceConfig config) {
      if (log.isDebugEnabled())
        log.debug("Retrieving 'webxml' datasource");
      // check if there is already an exiting web xml data source
      DataSource dataSource = (DataSource)mDataSources.get(config.getType());
      if (dataSource != null) {
        if (log.isDebugEnabled())
          log.debug("Returning existing 'webxml' datasource");
        return dataSource;
      }
      // create a new web xml data source

      // copy all context parameters
      Enumeration enum = mEnv.getParameterNames();
      String paramValue, paramName;
      Properties props = new Properties();
      if (enum != null) {
        while(enum.hasMoreElements()) {
          paramName = (String)enum.nextElement();
          paramValue = mEnv.getParameter(paramName);
          props.setProperty(paramName, paramValue);
        }
      }
      dataSource = new WebXmlDataSource(props);
      mDataSources.put(config.getType(), dataSource);
      if (log.isDebugEnabled())
        log.debug("New 'webxml' datasource created: " + dataSource);

      return dataSource;
    }

    /**
     * Returns a reference to a parameter data source. If there is already
     * such a data source then the exiting one is returned
     * @param config a data source configuration
     * @return a reference to a parameter data source
     */
    private static DataSource getParamDataSource(DataSourceConfig config) {
      if (log.isDebugEnabled())
        log.debug("Retrieving 'paramdata' datasource");
      // check if there is already an exiting web xml data source
      DataSource dataSource = (DataSource)mDataSources.get(config.getType());
      if (dataSource != null) {
        if (log.isDebugEnabled())
          log.debug("Returning existing 'paramdata' datasource");
        return dataSource;
      }

      // create a parameter data source
      String fileName = config.getSource();
      ParamDataSource paramDataSource = null;
      try {
        InputStream is = mEnv.getResourceAsStream(fileName);
        if (is == null) {
          String msg = "Error initializing " + config.getType() + " data source. File not found '" + fileName + "'";
          log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.exception", new Object[] { msg }, null);
        }
        paramDataSource = new ParamDataSource(is, getDataSourceProvider());
      } catch (Exception ex) {
        String msg = "Error initializing " + config.getType() + " data source ";
        log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.exception", new Object[] { msg }, ex);
      }
      mDataSources.put(config.getType(), paramDataSource);
      if (log.isDebugEnabled())
        log.debug("New 'paramdata' datasource created: " + paramDataSource);
      return dataSource;
    }

}