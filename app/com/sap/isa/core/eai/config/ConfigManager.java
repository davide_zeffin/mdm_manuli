/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      19 February 2002
  $Id:  $
  $Revision:  $
  $Change:  $
  $DateTime:  $
  $Author:  $
*****************************************************************************/
package com.sap.isa.core.eai.config;

// java imports
import java.io.InputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.commons.digester.Digester;

import com.sap.isa.core.eai.BackendConfig;
import com.sap.isa.core.eai.config.datasource.DataSourceConfig;
import com.sap.isa.core.eai.config.datasource.DataSourceProvider;
import com.sap.isa.core.eai.config.datasource.param.ParamDataSource;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.XMLHelper;

/**
 * This singelton is a provider for information used by the eai layer
 */
public class ConfigManager {

  public static final String DEFAULT_CONFIG_ID = "default";
  public static final String DEFAULT_DATASOURCE = "default";
  public static final String DEFAULT_DATASOURCE_PARAMDATA = "paramdata:default/";


  protected static IsaLocation log = IsaLocation.
        getInstance(ConfigManager.class.getName());

  protected static  IsaLocation logEaiXML =
        IsaLocation.getInstance(ConfigManager.class.getName() + ".xml");

  private Set mProcessedInheritance = new HashSet();
  private Set mBackendConfigIds     = new HashSet();
  private Map mBackendConfigs      = new HashMap();
  private Map mDataSourceConfigs   = new HashMap();
  private static DataSourceProvider mDataSourceProvider;

  private XMLHelper xmlHelper      = new XMLHelper();

  private static String CRLF = System.getProperty("line.spearator");
  private static String mDataFileName;
  private static String mConfigFileName;
  private static int debugLevel = 0;
  private static InputStream mIs;
  private static ConfigManager mConfigManager = new ConfigManager();
  private boolean mIsInit = false;

  /**
   * Class is a singelton
   */
  private ConfigManager() {}
  /**
   * Returns an instance of the Config Manager
   * @return an instance of the Config Manager
   */
  public static ConfigManager getConfigManager() {
    return mConfigManager;
  }

  /**
   * Specifies a reference to a data source provider
   * @param provider data source provider
   */
  public static void setDataSourceProvider(DataSourceProvider provider) {
    mDataSourceProvider = provider;
  }

  /**
   * Adds a backend configuration
   * @param config a backend configuration
   */
  public void addBackendConfig(BackendConfig config) {
    if (config == null)
      return;
    mBackendConfigIds.add(config.getId());
    mBackendConfigs.put(config.getId(), config);
  }

  /**
   * Adds a datasource configuration
   * @param config a datasource configuration
   */
  public void addDataSourceConfig(DataSourceConfig config) {
    if (config == null)
      return;
    mDataSourceConfigs.put(config.getName(), config);
  }


  /**
   * Sets file name (and path) of eai data configuration file
   * @param dataFileName name (and path) of configuration file
   */
  public static void setDataFileName(String dataFileName) {
    mDataFileName = dataFileName;
  }

  /**
   * Sets file name (and path) of eai config configuration file
   * @param configFileName file name (and path) of eai config configuration file
   */
  public static void setConfigFileName(String configFileName) {
    mConfigFileName = configFileName;
  }

  /**
   * Sets debug level of the Config reader
   * @param debugLevel
   */
  public static void setDebugLevel(int debugLevel) {
    ConfigManager.debugLevel = debugLevel;
  }

  private void readConfigFile() throws ConfigManagerException {
    // new Struts digester
    Digester digester = new Digester();
    digester.setDebug(debugLevel);

    // rules for managed connections

    digester.push(this);

     // create a new data source config object
    digester.addObjectCreate("backend/datasources/datasource",
        "com.sap.isa.core.eai.config.datasource.DataSourceConfig");
    // set all properties which belong to data source config object
    digester.addSetProperties("backend/datasources/datasource");

    // add static name/value pairs to source config object
    digester.addCallMethod(
        "backend/datasources/datasource/param",
        "addProperty", 2, new String[] {"java.lang.String", "java.lang.String"});

    digester.addCallParam("backend/datasources/datasource/param"
        , 0, "name");
    digester.addCallParam("backend/datasources/datasource/param"
        , 1, "value");

    // add data sources to configuration
   digester.addSetNext("backend/datasources/datasource",
      "addDataSourceConfig", "com.sap.isa.core.eai.config.datasource.DataSourceConfig");

    // create new backend config object
    digester.addObjectCreate("backend/configs/config",
        "com.sap.isa.core.eai.BackendConfig");

    // set all properties which belong to data source config object
    digester.addSetProperties("backend/configs/config");

    digester.addObjectCreate("backend/configs/config/managedConnectionFactories/managedConnectionFactory",
        "com.sap.isa.core.eai.ManagedConnectionFactoryConfig");
    // set all properties which belong to a connection factory configuration
    digester.addSetProperties("backend/configs/config/managedConnectionFactories/managedConnectionFactory");

    // Create connection definition object
    digester.addObjectCreate("backend/configs/config/managedConnectionFactories/managedConnectionFactory/connectionDefinitions/connectionDefinition",
      "com.sap.isa.core.eai.ConnectionDefinition");

    // set property of connection definition object
    digester.addSetProperties("backend/configs/config/managedConnectionFactories/managedConnectionFactory/connectionDefinitions/connectionDefinition");

    // add name/value pairs to connection definition object
    digester.addCallMethod(
        "backend/configs/config/managedConnectionFactories/managedConnectionFactory/connectionDefinitions/connectionDefinition/params/param",
        "addProperty", 2, new String[] {"java.lang.String", "java.lang.String"});

    digester.addCallParam("backend/configs/config/managedConnectionFactories/managedConnectionFactory/connectionDefinitions/connectionDefinition/params/param"
        , 0, "name");
    digester.addCallParam("backend/configs/config/managedConnectionFactories/managedConnectionFactory/connectionDefinitions/connectionDefinition/params/param"
        , 1, "value");

   // add connection definition to connection factory
    digester.addSetNext("backend/configs/config/managedConnectionFactories/managedConnectionFactory/connectionDefinitions/connectionDefinition",
        "addConnectionDefinition", "com.sap.isa.core.eai.ConnectionDefinition");

    // Create connection configuration object
    digester.addObjectCreate("backend/configs/config/managedConnectionFactories/managedConnectionFactory/connections/connection",
      "com.sap.isa.core.eai.ConnectionConfig");

    // set property of connection definition object
    digester.addSetProperties("backend/configs/config/managedConnectionFactories/managedConnectionFactory/connections/connection");

    // add name/value pairs to connection
    digester.addCallMethod(
        "backend/configs/config/managedConnectionFactories/managedConnectionFactory/connections/connection/params/param",
        "addProperty", 2, new String[] {"java.lang.String", "java.lang.String"});

    // add connection parameters
    digester.addCallParam("backend/configs/config/managedConnectionFactories/managedConnectionFactory/connections/connection/params/param"
        , 0, "name");
    digester.addCallParam("backend/configs/config/managedConnectionFactories/managedConnectionFactory/connections/connection/params/param"
        , 1, "value");

    // add connection configuration to connection factory
    digester.addSetNext("backend/configs/config/managedConnectionFactories/managedConnectionFactory/connections/connection",
        "addConnectionConfig", "com.sap.isa.core.eai.ConnectionConfig");

    // add name/value pairs to connection factory
    digester.addCallMethod(
        "backend/configs/config/managedConnectionFactories/managedConnectionFactory/params/param",
        "addProperty", 2, new String[] {"java.lang.String", "java.lang.String"});

    digester.addCallParam("backend/configs/config/managedConnectionFactories/managedConnectionFactory/params/param"
        , 0, "name");
    digester.addCallParam("backend/configs/config/managedConnectionFactories/managedConnectionFactory/params/param"
        , 1, "value");


    // add connection factory configuration to backend config
    digester.addSetNext("backend/configs/config/managedConnectionFactories/managedConnectionFactory",
        "addManagedConnectionFactoryConfig", "com.sap.isa.core.eai.ManagedConnectionFactoryConfig");


    // process backend business object configuration
    digester.addObjectCreate("backend/configs/config/businessObjects/businessObject",
        "com.sap.isa.core.eai.BackendBusinessObjectConfig");

    // set properties of business object configuration
    digester.addSetProperties("backend/configs/config/businessObjects/businessObject");

    // add properties which belong to business object
    digester.addCallMethod(
        "backend/configs/config/businessObjects/businessObject/params/param",
        "addProperty", 2 , new String[] {"java.lang.String", "java.lang.String"});

    digester.addCallParam("backend/configs/config/businessObjects/businessObject/params/param"
        , 0, "name");
    digester.addCallParam("backend/configs/config/businessObjects/businessObject/params/param"
        , 1, "value");

    // add business object configuration to the config manager
    // add connection factory configuration to config reader
    digester.addSetNext("backend/configs/config/businessObjects/businessObject",
        "addBackendBOConfig", "com.sap.isa.core.eai.BackendBusinessObjectConfig");

    // add backend configuration
    digester.addSetNext("backend/configs/config",
        "addBackendConfig", "com.sap.isa.core.eai.BackendConfig");


    try {
      if (mIs != null)
        digester.parse(mIs);
      else if (mConfigFileName == null || mConfigFileName.length() > 0) {
        log.info("system.eai.init.readConfigFile", new Object[] { mConfigFileName }, null);
        digester.parse(mConfigFileName);
      } else
        throw new ConfigManagerException("Error reading configuration information" + CRLF +
            "No input stream or file name set");

    } catch (Exception ex) {
      String errMsg = "Error reading configuration information" + CRLF + ex.toString();
      log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.eai.exception", new Object[] { errMsg }, ex);
      throw new ConfigManagerException(errMsg);
    } finally {
      try {
        mIs.close();
      } catch (Exception ex) {
        String errMsg = "Error closing input stream" + CRLF + ex.toString();
        log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.eai.exception", new Object[] { errMsg }, ex);
        throw new ConfigManagerException(errMsg);

      }
    }
  }

  /**
   * Sets an input stream which will be used to access the configuration file
   * @param is input stream
   */
  public static void setInputStream(InputStream is) {
    mIs = is;
  }

  /**
   * Returns the current configuration as XML
   * @return the current configuration as XML
   */
  public String toString() {
    StringBuffer sb = new StringBuffer();
    sb.append(xmlHelper.createTag("backend", false));

    // data sources
    sb.append(xmlHelper.createTag("datasources", false));
    for (Iterator iter = mDataSourceConfigs.keySet().iterator(); iter.hasNext();) {
      String key = (String)iter.next();
      DataSourceConfig config =  (DataSourceConfig)mDataSourceConfigs.get(key);
      sb.append(config.toString());
    }
    sb.append(xmlHelper.createEndTag("datasources"));

    // backend configs
    sb.append(xmlHelper.createTag("configs", false));
    for (Iterator iter = mBackendConfigs.keySet().iterator(); iter.hasNext();) {
      String key = (String)iter.next();
      BackendConfig config =  (BackendConfig)mBackendConfigs.get(key);
      sb.append(config.toString());
    }
    sb.append(xmlHelper.createEndTag("configs"));

    sb.append(xmlHelper.createEndTag("backend"));
    return sb.toString();
  }

  /**
   * Returns a backend configuration. Dynamic values are retrieved from the
   * default data source
   * @param id id of configuration
   * @return a backend configuration
   */
  public BackendConfig getBackendConfig(String id) {
    init();
    // get configuration for a given key
    BackendConfig config = (BackendConfig)mBackendConfigs.get(id);
    if (config == null) {
      String msg = "Backend configuration '" + id + "' does not exist";
      log.warn("system.exception", new Object[] { msg }, null);
      return null;
    }
    String defaultData = config.getDefaultdata();
    return getBackendConfig(id, defaultData);
  }

  /**
   * Returns a backend configuration. Dynamic values are retrieved from the
   * data source named by the
   * @param id id of configuration
   * @param defaultDataSource name of default data source
   * @return a backend configuration
   */
  public BackendConfig getBackendConfig(String id, String defaultDataSource) {
    init();

    // check if there is a configuration for the given id
    BackendConfig config = (BackendConfig)mBackendConfigs.get(id);
    if (config == null) {
      String errMsg = "No configuration for the [id]='" + id + "' found";
      log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.eai.exception", new Object[] { errMsg }, null);
      return null;
    }
    String realDataSourceName = defaultDataSource;

    // use the default data source for the given configuration?
    if (defaultDataSource != null && defaultDataSource.equals(DEFAULT_DATASOURCE))
      realDataSourceName = config.getDefaultdata();
    // gets the real name of the default data source
    realDataSourceName = getDefaultDataSource(config.getDefaultdata(), realDataSourceName);

    String backendConfigKey = id + realDataSourceName;

    // return existing configuration
    if (!isBackendConfigNew(id, realDataSourceName)) {
      return (BackendConfig)mBackendConfigs.get(backendConfigKey);
    }
    BackendConfig newConfig = null;

    // create a clone of the base configuration
    newConfig = (BackendConfig)config.getConfigClone();

    // gets the real name of the default data source
//    String dataSource =
//        getDefaultDataSource(newConfig.getDefaultDataSource(), defaultDataSource);

    // set namespace for the new configuration
    newConfig.setNamespace(backendConfigKey);
    // associate the default data source to the whole configuration
    newConfig.setDefaultDataSource(realDataSourceName);
    newConfig.processDynamicData();
    // register new configuration
    mBackendConfigs.put(backendConfigKey, newConfig);

    if(log.isDebugEnabled()) {
       log.debug("Returning configuration for [id]='" + id + "' [defaultdata]='" + realDataSourceName + "'");
       log.debug(newConfig);
       logEaiXML.debug("Returning configuration for [id]='" + id + "' [defaultdata]='" + realDataSourceName + "'");
       logEaiXML.debug(newConfig);
    }

    return newConfig;
  }

  /**
   * This method performs the inital initialization of the config manager
   */
  private synchronized void init() {
    if (mIsInit)
      return;
    try {
        // read config file
        readConfigFile();
        // init data sources
        registerDataSources();
        // process inherited configurations
        processInheritedConfigs();
        mIsInit = true;
      } catch (ConfigManagerException ex) {
        log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.exception", new Object[] { "Error initializing Config Manager" }, ex);
      }
  }
  /**
   * Registers configured data sources to data source provider
   */
  private void registerDataSources() {
    for(Iterator dataSourceIter = mDataSourceConfigs.keySet().iterator(); dataSourceIter.hasNext();) {
      String key = (String)dataSourceIter.next();
      if (log.isDebugEnabled())
        log.debug("Registering data source: '" + key + "'");
      DataSourceConfig config = (DataSourceConfig)mDataSourceConfigs.get(key);
      mDataSourceProvider.registerDataSource(config);
    }
  }

  /**
   * Returns the name of the data source for the given parameter value
   * @param value
   * @return the name of the data source for the given parameter value
   */
  private String getDataSourceName(String value) {
    if (value == null)
      return "";

    int colonPos = value.indexOf(":");
    if (colonPos == -1)
      return "";

    return value.substring(0, colonPos);
  }

 /**
  * Processes configurations which inherit from other configurations
  */
  private void processInheritedConfigs() {
    // check if there are configurations which have a base configuration
    for(Iterator configIter = mBackendConfigs.keySet().iterator();configIter.hasNext();) {
      String key = (String)configIter.next();
      BackendConfig currentConfig = (BackendConfig)mBackendConfigs.get(key);
      processInheritedConfig(currentConfig);
    }
  }

  /**
   * Used to mix configurations
   * @param currentConfig configuration inheriting from other configurations
   */
  private void processInheritedConfig(BackendConfig currentConfig) {
    String base = currentConfig.getBase();
    log.debug("Processing inherited configuration: " + currentConfig.getId());
    if (base != null && base.length() > 0) {
        BackendConfig baseConfig = (BackendConfig)mBackendConfigs.get(base);
        // check if config has predecessors
        processInheritedConfig(baseConfig);
        currentConfig.mixConfiguration(baseConfig);
        // mark config as already processed
        mProcessedInheritance.add(currentConfig.getId());
    }

  }

  /**
   * Returns the default backend configuration with the default data source provider
   * @return the defualt backend configuration
   */
  public BackendConfig getDefaultConfig() {
      BackendConfig conf = getBackendConfig("default", "paramdata:default/");
      return conf;
  }

  /**
   * Returns true if this backend configuration already exits (is not
   * retrieved for the first time)
   * @param id id of configuration
   * @param defaultDataSource name of default data source
   * @return true if this backend configuration already exits
   */
  public boolean isBackendConfigNew(String id, String defaultDataSource) {
    // get configuration for a given key
    BackendConfig config = (BackendConfig)mBackendConfigs.get(id);
    if (config == null) {
      return true;
    }
    String realDataSourceName = getDefaultDataSource(config.getDefaultdata(), defaultDataSource);

    String backendConfigKey = id + realDataSourceName;

//    String backendConfigKey = id + defaultDataSource;
    if (log.isDebugEnabled())
      log.debug("Retrieving configuration for [key]='" + backendConfigKey + "']");

    // check if there is a suitable configuration
    BackendConfig newConfig = null;
    newConfig = (BackendConfig)mBackendConfigs.get(backendConfigKey);
    if (newConfig != null)
      return false;
    else
      return true;
  }

  /**
   * Returns the default data source used for this configuration.
   * @param configDS the name of the configured data source
   * @param passedDS the name of the passed data source
   * @return the default data source used for this configuration
   */
  private String getDefaultDataSource(String configDS, String passedDS) {

    if(log.isDebugEnabled()) {
      log.debug("Processing real name of passed default data source '" + passedDS + "'");
    }
    String retVal = passedDS;
    if (getDataSourceName(passedDS).length() == 0) {
      // passed data has no data source defined. Use the default one
      String ds = getDataSourceName(configDS);
      retVal = ds + ":" + passedDS;
    }
    if (getDataSourceName(retVal).equalsIgnoreCase(ParamDataSource.DATA_SOURCE_TYPE)) {
    // param data data source
    // check if last character is an '/' (has to be!!)
    String lastChar = passedDS.substring(passedDS.length() - 1, passedDS.length());
    if (!lastChar.equals("/"))
      retVal = retVal + "/";
    }
    if(log.isDebugEnabled()) {
      log.debug("Real name of passed default data source '" + retVal + "'");
    }
    return retVal;
  }

  /**
   * Returns the ids of available backend configs
   * @return the ids of available backend configs
   */
  public Set getBackendConfigIds() {
    return Collections.unmodifiableSet(mBackendConfigIds);
  }

  /**
   * Returns the ids of data sources known to the backend configuration
   * @return the ids of data sources known to the backend configuration
   */
  public Set getDataSourceIds() {
    return Collections.unmodifiableSet(mDataSourceConfigs.keySet());
  }

  /**
   * Returns a data source configuration for the given id
   * @param id data source id
   * @return a data source configuration for the given id
   */
  public DataSourceConfig getDataSourceConfig(String id) {
    return (DataSourceConfig)mDataSourceConfigs.get(id);
  }
}
