package com.sap.isa.core.eai.config;
/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      19 February 2001
  $Id:  $
  $Revision:  $
  $Change:  $
  $DateTime:  $
  $Author:  $
*****************************************************************************/

// java import
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import com.sap.isa.core.eai.config.datasource.DataSource;
import com.sap.isa.core.eai.config.datasource.DataSourceProvider;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.XMLHelper;

/**
 * This class is the base class for each configuration data container
 */

public abstract class ConfigBase implements Config {


  /**
   * Name of default data source
   * Value: DEFAULT
   */
  public static final String DEFAULT = "DEFAULT";

  /**
   * Constant containing 'null'
   */
  public static final String NULL = "null";

  protected IsaLocation log;
  public String mParamPrefix;

  //this map contains all data of this data container
  protected Map mData = new HashMap();
  protected Config mBaseConfig;
  protected static XMLHelper xmlHelper = new XMLHelper();
  protected static DataSourceProvider mDataSourceProvider;

  public ConfigBase() {
    // set the classname
    String className = this.getClass().getName();
    mParamPrefix = className;
    // Retrieve the actual IsaLocation
    log = IsaLocation.getInstance(className);
  }

  /**
   * Sets a parameter value.
   * @param id id of parameter. Before the value is set, the content of PARAM_PREFIX
   *           is added to the id
   * @param value value of parameter
   */
  public void setParameter(String id, Object value) {
    mData.put(id, value);
  }

  /**
   * Gets a parameter value.
   * @param id id of parameter. Before the value is retrieved, the content of PARAM_PREFIX
   *           is added to the id
   * @return value value of parameter
   */
  public Object getParameter(String id) {
    return (String)mData.get(id);
  }


  /**
   * Specifies the id of this configuration
   * @param id the id of this configuration
   */
  public void setId(String id) {
    setParameter(PARAMNAME_ID, id);
  }

  /**
   * Returns the id of this configuration
   * @return the id of this configuration
   */
  public String getId() {
    return (String)getParameter(PARAMNAME_ID);
  }

  /**
   * Sets the id of the base configuration
   * @param idBase id of the base configuration
   */
  public void setBase(String idBase) {
    setParameter(PARAMNAME_BASE, idBase);
  }

  /**
   * Gets the id of the base configuration
   * @return id of the base configuration
   */
  public String getBase() {
    return (String)getParameter(PARAMNAME_BASE);
  }



  /**
   * This method is used to mix the existing configuration with
   * a base configuration. The passed configuration is used as base
   * configuration and mixed up with the current configuration
   * @param baseConfig base configuration
   */
  public void mixConfiguration(Config baseConfig) {
    // get clone of base configuration
    Map cloneBase = baseConfig.getClonedData();
    //overwrite data of base configuration with existing data
    for(Iterator dataIter = mData.keySet().iterator(); dataIter.hasNext();) {
      String currentKey = (String)dataIter.next();
      Object currentValue = mData.get(currentKey);
      // process different types of configuration
      if (currentValue instanceof String) {
        cloneBase.put(currentKey, currentValue);
        continue;
      }
      if (currentValue instanceof Properties) {
        Properties currentProps = (Properties)currentValue;
        Properties baseProps = (Properties)cloneBase.get(currentKey);
        if (currentProps == null)
          continue;
        else {
          mixProperties(baseProps, currentProps);
        }

        continue;
      }
      if (currentValue instanceof Config) {
        Config cConfig = (Config)currentValue;
        Config bConfig = (Config)cloneBase.get(currentKey);
        if (bConfig != null)
          cConfig.mixConfiguration(bConfig);

         cloneBase.put(currentKey, cConfig);
      }
    }
    mData = cloneBase;
  }

  /**
   * Gets a clone of the internal data
   * @return a clone of the internal data
   */
  public Map getClonedData() {
    Map clone = new HashMap();
    // loop over all data of this configuration
    for(Iterator dataIter = mData.keySet().iterator(); dataIter.hasNext();) {
      String key = (String)dataIter.next();
      Object value = mData.get(key);
      // process different types of configuration
      if (value instanceof String) {
        clone.put(key, value);
        continue;
      }
      if (value instanceof Properties) {
        clone.put(key, cloneProperties((Properties)value));
        continue;
      }
      if (value instanceof Config) {
        clone.put(key, ((Config)value).getConfigClone());
      }
    }
    return clone;
  }

  /**
   * Creates a clone of properties
   * @param baseProps the properties to clone
   * @return a clone of the properties
   */
  protected Properties cloneProperties(Properties baseProps) {
    Properties clone = new Properties();
    for(Enumeration enum = baseProps.keys(); enum.hasMoreElements();) {
      String key = (String)enum.nextElement();
      clone.setProperty(key, baseProps.getProperty(key));
    }
    return clone;
  }

  /**
   * Mix properties
   * @param baseProps the base properties of these properties
   * @param currentProps the current properties
   * @return the mixed properties
   */
  private Properties mixProperties(Properties baseProps, Properties currentProps) {

    if (baseProps == null) {
      baseProps = new Properties();
    }
    if (currentProps == null)
      return baseProps;

    // mix properties
    for(Enumeration propsKeyEnum = currentProps.keys();propsKeyEnum.hasMoreElements();) {
      String currentPropKey = (String)propsKeyEnum.nextElement();
      baseProps.setProperty(currentPropKey, currentProps.getProperty(currentPropKey));
    }
    return baseProps;
  }

  /**
   * Returns the properties as XML string
   * @param params
   * @return XML representation
   */
  protected String getAsXMLString(Properties params) {
    StringBuffer sb = new StringBuffer();
    if (params == null || params.size() == 0) {
      sb.append(xmlHelper.createElement("params", null));
      return sb.toString();
    }

    sb.append(xmlHelper.createTag("params", false));

    for(Enumeration propEnum = params.keys(); propEnum.hasMoreElements();) {
      String propKey = (String)propEnum.nextElement();
      String propValue =  params.getProperty(propKey);
      Properties props = new Properties();
      props.setProperty("name", propKey);
      props.setProperty("value", propValue);
      sb.append(xmlHelper.createTag("param", props, true));
    }
    sb.append(xmlHelper.createEndTag("params"));

    return sb.toString();
  }

  /**
   * Sets a data source provider which can be used to get references to
   * data sources. Data sources are used to retrieve dynamic values
   * @param dataSourceProvider a data source provider which can be used to get references to data sources
   */
  public void setDataSourceProvider(DataSourceProvider dataSourceProvider) {
    mDataSourceProvider = dataSourceProvider;
  }

  /**
   * When this method is called, dynamic data is retrieved from the appropriate
   * data sources
   */
  public void processDynamicData() {
      processDynamicData(mData);
  }

 /**
   * When this method is called, dynamic data is retrieved from the appropriate
   * data sources
   * @param data the data
   */
  public void processDynamicData(Map data) {
    if (mDataSourceProvider == null) {
      log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.exception", new Object[] { "No data source provider specified" }, null);
      return;
    }
    // loop over all data
    for(Iterator dataIter = data.keySet().iterator(); dataIter.hasNext();) {
      String dataKey = (String)dataIter.next();
      Object value = data.get(dataKey);
      // check type of data
      if (value instanceof Properties) {
        processDynamicProps((Properties)value);
      }
      // call same function recursively
      if (value instanceof Config)
        ((Config)value).processDynamicData();
    }
  }

  /**
   * Processes dynamic values of properties
   * @param props properties
   * @return the properties after processing
   */
  private Properties processDynamicProps(Properties props) {
    // loop over all properties and check whether they are dynamic
    Properties newProps = new Properties();
    for (Enumeration propEnum = props.keys(); propEnum.hasMoreElements();) {
      String propKey = (String)propEnum.nextElement();
      String propValue = (String)props.getProperty(propKey);
      processDynamic(props, propKey, propValue);
//      props.setProperty(propKey, processDynamic(propValue));
    }
    return newProps;
  }
  /**
   * This method checks if the value of a parameter is dynamic. This is only
   * the case if value has the following format:
   * 'datasource:path' and the data source exists. If the value is dynamic
   * then the value is retrieved from a appropriate data source
   * @param props properties
   * @param propKey key of a property
   * @param propValue value of a property
   */
  private void processDynamic(Properties props, String propKey, String propValue) {
    // check if the value is dynamic
    if (propValue == null)
      return;

    int colonPos = propValue.indexOf(":");
    if (colonPos == -1) {
      props.setProperty(propKey, propValue);
      return;
    }

    // it seems that there is a data source
    String dataSourceType = propValue.substring(0, colonPos);

    // check if that data source is available by the data source provider
    if(!mDataSourceProvider.isDataSource(dataSourceType)) {
      // there is no data source which can handle data
      props.setProperty(propKey, propValue);
      return;
    }

    // get param value without data provider
    String realParamValue = propValue.substring(colonPos + 1, propValue.length());

    // handling for parameter values feed from a default data source
    if (dataSourceType.equalsIgnoreCase(PARAMNAME_DEFAULT_DATASOURCE)) {
      propValue = getDefaultDataSource() + realParamValue;
      colonPos = propValue.indexOf(":");
      if (colonPos == -1)
        props.setProperty(propKey, propValue);
      dataSourceType = propValue.substring(0, colonPos);
      realParamValue = propValue.substring(colonPos + 1, propValue.length());
    }
    DataSource dataSource = mDataSourceProvider.getDataSource(dataSourceType);

    if (dataSource == null) {
      props.setProperty(propKey, "Data Source=" + dataSourceType + " not found; " + propValue);
      return;
    }
    // check if the selection is multi valued
    if (dataSource.isMultiSelect(realParamValue)) {
      Properties multiProps = dataSource.getProperties(realParamValue);
      for(Enumeration enum = multiProps.keys(); enum.hasMoreElements();) {
        String key = (String)enum.nextElement();
        String value = multiProps.getProperty(key);
        props.put(key, value);
        // remove entry with wildcard
        props.remove(propKey);
      }
    } else
      props.setProperty(propKey, dataSource.getProperty(realParamValue));
 }

  /**
   * Returns true if the property value has a data source defined
   * @param propValue the value of a property which has to be examined
   * @return true if the property value has a data source defined
   */
  protected boolean hasDatasource(String propValue) {
    int colonPos = propValue.indexOf(":");
    if (colonPos == -1)
      return false;

    return true;
  }


  /**
   * Gets name of default data source for dynamic values
   * @return name of default data source for dynamic values
   */
  public String getDefaultDataSource() {
    return (String)getParameter(PARAMNAME_DEFAULT_DATASOURCE);
  }

  /**
   * Sets name of default data source for dynamic values to this
   * configuration and all depending configurations
   * @param name of default data source for dynamic values
   */
  public void setDefaultDataSource(String name) {
    if(log.isDebugEnabled())
      log.debug("Setting default data source [name]='" + name + "'");

   setParameter(PARAMNAME_DEFAULT_DATASOURCE, name);

    // loop over all data of this configuration
    for(Iterator dataIter = mData.keySet().iterator(); dataIter.hasNext();) {
      String key = (String)dataIter.next();
      Object value = mData.get(key);
      if (value instanceof Config) {
        if(log.isDebugEnabled())
          log.debug("Processing configuration [name]='" + key + "' [type]='" + value.getClass() + "'");
         ((Config)value).setDefaultDataSource(name);
      }
    }
  }

  /**
   * Sets the namespace attribute to this and all depended
   * configurations
   * @param namespace the namespace attribute
   */
  public void setNamespace(String namespace) {
    if(log.isDebugEnabled())
      log.debug("Setting namespace [namespace]='" + namespace + "'");
   setParameter(PARAMNAME_NAMESPACE, namespace);
    // loop over all data of this configuration
    for(Iterator dataIter = mData.keySet().iterator(); dataIter.hasNext();) {
      String key = (String)dataIter.next();
      Object value = mData.get(key);
      if (value instanceof Config) {
        if(log.isDebugEnabled())
          log.debug("Processing configuration [name]='" + key + "' [type]='" + value.getClass() + "'");
         ((Config)value).setNamespace(namespace);
      }
    }
  }
}
