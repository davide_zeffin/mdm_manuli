package com.sap.isa.core.eai.config;

/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      15 February 2001
*****************************************************************************/

/**
 * This exception is thrown by the ConfigReader if something goes wrong
 * while reading/processing configuration
 */
public class ConfigManagerException extends Exception {

  /**
   * Constructor
   * @param msg Message describing the exception
   */
  public ConfigManagerException(String msg) {
    super(msg);
  }
}