/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      20 February 2001
*****************************************************************************/
package com.sap.isa.core.eai;

// java imports
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import com.sap.isa.core.eai.config.Config;
import com.sap.isa.core.eai.config.ConfigBase;

/**
 * This class holds configuration data needed by a connection factory
 */
public class ManagedConnectionFactoryConfig extends ConfigBase {

  private static final String PROPNAME_PROPERTIES           = "properties";
  private static final String PROPNAME_CONFAC_NAME          = "connectionFactoryName";
  private static final String PROPNAME_CONFAC_CLASS_NAME    = "connectionFactoryClassName";
  private static final String PROPNAME_CONFAC_VERSION       = "connectoinFactoryVersion";
  private static final String PROPNAME_CON_DEFS             = "connectionDefinitions";
  private static final String PROPNAME_CON_CONFIGS          = "connectionConfigs";

  protected Map connectionDefinitions = Collections.synchronizedMap(new HashMap());
  protected Map connectionConfigs = Collections.synchronizedMap(new HashMap());

  /**
   * Default constructor
   */
  public ManagedConnectionFactoryConfig() {
    getInternalConConfigs();
    getInternalConDefs();
    getInternalProps();
  }

  public ManagedConnectionFactoryConfig(Map data) {
    mData = data;
    getInternalConConfigs();
    getInternalConDefs();
    getInternalProps();
  }

  /**
   * Constructor of configuration
   * @param connectionFactoryName name which describes type of connection factory
   * @param connectionFactoryClassName name of class which implements connection factory
   * @param connectionDefinitions a vector of connection definitions used by this connection factory
   * @param connectionConfigs a vector of connection configurations used by this connection factory
   */
  public ManagedConnectionFactoryConfig(
      String connectionFactoryName,
      String connectionFactoryClassName,
      Map connectionDefinitions,
      Map connectionConfigs) {
    mData.put(PROPNAME_CONFAC_NAME, connectionFactoryName);
    mData.put(PROPNAME_CONFAC_CLASS_NAME, connectionFactoryClassName);
    mData.put(PROPNAME_CON_DEFS,  new ConnectionDefinitions(connectionDefinitions));
    mData.put(PROPNAME_CON_CONFIGS, new ConnectionConfigs(connectionConfigs));
    getInternalProps();
  }

  /**
   * Gets name of connection factory
   * @return name of connection factory
   */
  public String getName() {
    return (String)mData.get(PROPNAME_CONFAC_NAME);
  }

  /**
   * Sets a string definig the type of this backend connection factory
   * @param type a string definig the type of this backend connection factory
   */
  public void setName(String name) {
    mData.put(PROPNAME_CONFAC_NAME, name);
  }

  /**
   * Gets class name of this connection factory
   * @return class name of this connection factory
   */
  public String getClassName() {
    return (String)mData.get(PROPNAME_CONFAC_CLASS_NAME);
  }

  /**
   * Sets name of class which implements this backend connection factory
   * @param className name of class which implements this backend connection factory
   */
  public void setClassName(String className) {
    mData.put(PROPNAME_CONFAC_CLASS_NAME, className);
  }

  /**
   * Sets version of the used connection factory
   * @param version version of the connection factory
   */
  public void setVersion(String version) {
    mData.put(PROPNAME_CONFAC_VERSION, version);
  }

  /**
   * Returns version of the connection factory
   * @return version of the connection factory
   */
  public String getVersion() {
    return (String)mData.get(PROPNAME_CONFAC_VERSION);
  }

  /**
   * Returns a specific connection definition
   * @param connectionDefinitionName name of connection definition
   * @return A connection definition
   */
  public ConnectionDefinition getConnectionDefinition(String connectionDefinitionName) {
    return getInternalConDefs().getConnectionDefinition(connectionDefinitionName);
  }

  /**
   * Returns a specific connection configuration
   * @param connectionDefinitionName name of connection configuration
   * @return A connection configuration
   */
  public ConnectionConfig getConnectionConfig(String connectionConfigName) {
    return getInternalConConfigs().getConnectionConfig(connectionConfigName);
  }

  /**
   * Adds a new connection definition to this backend connection factory
   * @param connectionDefinition a new connection definition
   */
  public void addConnectionDefinition(ConnectionDefinition connectionDefinition) {
    getInternalConDefs().addConnectionDefinition(connectionDefinition);
  }

  /**
   * Adds a new connection configuration to this backend connection factory
   * @param connectionConfig a new connection configuration
   */
  public void addConnectionConfig(ConnectionConfig connectionConfig) {
    getInternalConConfigs().addConnectionConfig(connectionConfig);
  }

  /**
   * Returns a hashtable containing connection configurations used by
   * this connection factory
   * @return a hashtable containing connection configurations used by
   *      this connection factory
   */
  public Map getConnectionConfigs() {
    return getInternalConConfigs().getConnectionConfigs();
  }

  /**
   * Returns a hashtable containing connection definitions used by
   * this connection factory
   * @return a hashtable containing connection definitions used by
   *      this connection factory
   */
  public Map getConnectionDefinitions() {
    return getInternalConDefs().getConnectionDefinitions();
  }

  /**
   * Adds a set of properties to the factory
   * @param propName name of property
   * @param propValue value of property
   */
  public void addProperty(String propName, String propValue) {
     getInternalProps().setProperty(propName, propValue);
  }

  /**
   * Returns a set of properties which are associated with the factory
   * @return a set of properties
   */
  public Properties getProperties() {
    return getInternalProps();
  }

  /**
   * Clones this connection configuration
   */
  public Config getConfigClone() {
    return new ManagedConnectionFactoryConfig(getClonedData());
  }

  /**
   * Returns the internal properties object
   * @param the internal properties object
   */
  private Properties getInternalProps() {
    if (mData.get(PROPNAME_PROPERTIES) == null)
      mData.put(PROPNAME_PROPERTIES, new Properties());

    return (Properties)mData.get(PROPNAME_PROPERTIES);
  }

  /**
   * Returns the internal container for connection configurations
   * @return the internal container for connection configurations
   */
   private ConnectionConfigs getInternalConConfigs() {
    ConnectionConfigs conConfigs = (ConnectionConfigs)mData.get(PROPNAME_CON_CONFIGS);
    if (conConfigs == null) {
      conConfigs =  new ConnectionConfigs();
      mData.put(PROPNAME_CON_CONFIGS, conConfigs);
    }
    return (ConnectionConfigs)mData.get(PROPNAME_CON_CONFIGS);
   }

  /**
   * Returns the internal container for connection definitions
   * @return the internal container for connection configurations
   */
   private ConnectionDefinitions getInternalConDefs() {
    ConnectionDefinitions conDefs = (ConnectionDefinitions)mData.get(PROPNAME_CON_DEFS);
    if (conDefs == null) {
      conDefs =  new ConnectionDefinitions();
      mData.put(PROPNAME_CON_DEFS, conDefs);
    }
    return (ConnectionDefinitions)mData.get(PROPNAME_CON_DEFS);
   }

  /**
   * Returns a XML String representation of this connection factory
   * @return XML String representation of this connection factory
   */
   public String toString() {
    StringBuffer sb = new StringBuffer();
    Properties params = new Properties();
    if (getName() != null)
    	params.setProperty("name", getName());
    if (getVersion() != null)
    	params.setProperty("version", getVersion());
    if (getClassName() != null)
    	params.setProperty("className", getClassName());
    sb.append(xmlHelper.createTag("managedConnectionFactory", params, false));
    sb.append(getAsXMLString(getInternalProps()));
    sb.append(getInternalConDefs().toString());
    sb.append(getInternalConConfigs().toString());
    sb.append(xmlHelper.createEndTag("managedConnectionFactory"));
    return sb.toString();
  }
}