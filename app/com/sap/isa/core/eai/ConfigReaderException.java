/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      15 February 2001
*****************************************************************************/
package com.sap.isa.core.eai;

 
/**
 * This exception is thrown by the ConfigReader if something goes wrong
 * while reading/processing configuration
 * @deprecated Configuration is handled by the Extended Configuration Management 
 */
class ConfigReaderException extends Exception {

  /**
   * Constructor
   * @param msg Message describing the exception
   */
  public ConfigReaderException(String msg) {
    super(msg);
  }

}