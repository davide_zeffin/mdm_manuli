/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      4. Februar 2001, 13:06

  $Header: //java/sapm/esales/30/src/com/sap/isa/core/eai/BackendBusinessObjectBase.java#6 $
  $Revision: #6 $
  $Date: 2001/08/03 $
*****************************************************************************/
package com.sap.isa.core.eai;

import java.util.Properties;

/**
 * This is a convenience class which can be used as base class for backend object
 * It provides a default implementation for the BackendBusinessObject interface
 */
public class BackendBusinessObjectBase implements BackendBusinessObject {

  protected ConnectionFactory conFactory;
  protected BackendContext context;
  protected BackendObjectSupport mSupport;

  /**
   * Sets a reference to a connection factory which can be used to
   * retrieve connections to the backend system which are not preconfigured
   * @param conFactory a reference to a connection factory
   */
  public void setConnectionFactory(ConnectionFactory conFactory) {
    this.conFactory = conFactory;
  }

  /**
   * Returns a reference to a connection factory which can be used to create
   * arbitrary backend connections
   * @return a referece to a connection factory
   */
  public ConnectionFactory getConnectionFactory() {
    return conFactory;
  }

  /**
   * Initializes Business Object. Default implementation does nothing
   * @param props a set of properties which may be useful to initialize the object
   * @param params a object which wraps parameters
   */
  public void initBackendObject(Properties props, BackendBusinessObjectParams params) throws BackendException {
  }

  /**
   * Sets context which is common to all Backend Business Objects created
   * by the same Backend Object Manager
   * @param context the backend context
   */
  public void setContext(BackendContext context) {
    this.context = context;
  }

  /**
   * Gets context which is common to all Backend Business Objects created
   * by the same Backend Object Manager
   * @return the backend context
   */
  public BackendContext getContext() {
    return context;
  }

 /**
   * This method is called by the BackendObjectManager before the object
   * gets invalidated. It can be used to lean up resources allocated by
   * the backend object
   */
  public void destroyBackendObject() {
  	this.conFactory.cleanup();
  }

  /**
   * Returns an object which provides some additional functionality
   * related with a backend business object
   * @return reference to s backend object support object
   */
  public BackendObjectSupport getBackendObjectSupport() {
    return mSupport;
  }

  /**
   * Sets an object which provides some additional functionality
   * related with a backend business object
   * @param support reference to s backend object support object
   */
  public void setBackendObjectSupport(BackendObjectSupport support) {
    mSupport = support;
  }





}