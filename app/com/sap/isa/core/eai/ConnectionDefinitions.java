/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      19 February 2002
*****************************************************************************/
package com.sap.isa.core.eai;

// java imports
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.sap.isa.core.eai.config.Config;
import com.sap.isa.core.eai.config.ConfigBase;
import com.sap.isa.core.util.XMLHelper;

/**
 * This class represents a collection of connection definitions
 */

public class ConnectionDefinitions extends ConfigBase {

  private static XMLHelper xmlHelper = new XMLHelper();

  public ConnectionDefinitions () { }


  public ConnectionDefinitions (Map connectionDefinitions) {
    mData = connectionDefinitions;
  }

  /**
   * Adds a new connection definition
   * @param connectionDefinition a new connection definition
   */
  public void addConnectionDefinition(ConnectionDefinition connectionDefinition) {
    mData.put(connectionDefinition.getName(), connectionDefinition);
  }

  /**
   * Returns a specific connection definition
   * @param connectionDefinitionName name of connection definition
   * @return A connection definition
   */
  public ConnectionDefinition getConnectionDefinition(String connectionDefinitionName) {
    return (ConnectionDefinition)mData.get(connectionDefinitionName);
  }


  /**
   * Returns a clone of the configuration
   */
  public Config getConfigClone() {
    return new ConnectionDefinitions(getClonedData());
  }

 /**
  * Returns a XML string representation of the connection definitions
  * @return a XML string representation of the connection definitions
  */
  public String toString() {
    StringBuffer sb = new StringBuffer();

    sb.append(xmlHelper.createTag("connectionDefinitions", false));
    for (Iterator iter = mData.keySet().iterator(); iter.hasNext();) {
      String name = (String)iter.next();
      Object value = mData.get(name);
      if (value instanceof ConnectionDefinition)
        sb.append(((ConnectionDefinition)value).toString());
    }
    sb.append(xmlHelper.createEndTag("connectionDefinitions"));
    return sb.toString();
  }

  /**
   * Returns a hashtable containing connection definitions
   * @return a hashtable containing connection definitions
   */
  public Map getConnectionDefinitions() {
    Map conDefs = new HashMap();
    for(Iterator iter = mData.keySet().iterator(); iter.hasNext();) {
      String key = (String)iter.next();
      Object value = mData.get(key);
      if (value instanceof ConnectionDefinition) {
        conDefs.put(key, value);
      }
    }
    return conDefs;
  }



}