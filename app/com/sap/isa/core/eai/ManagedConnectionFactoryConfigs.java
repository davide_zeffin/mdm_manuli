/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      19 February 2002
*****************************************************************************/
package com.sap.isa.core.eai;

// java imports
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.sap.isa.core.eai.config.Config;
import com.sap.isa.core.eai.config.ConfigBase;

/**
 * This class represents a collection of connection configurations
 */

public class ManagedConnectionFactoryConfigs extends ConfigBase {

  public ManagedConnectionFactoryConfigs () {}


  public ManagedConnectionFactoryConfigs (Map mgrFacConfigs) {
    mData = mgrFacConfigs;
  }

  /**
   * Adds a new connection factory configuration
   * @param config new connection factory configuration
   */
  public void addMgrConFactoryConfig(ManagedConnectionFactoryConfig config) {
    mData.put(config.getName(), config);
  }

  /**
   * Returns meta data of a specific connection factory
   * @param conFacName name of connection factory
   * @return conFacType meta data of a specific connection factory
   */
  public ManagedConnectionFactoryConfig getMgrConFactoryConfig(String conFacName) {
    return (ManagedConnectionFactoryConfig)mData.get(conFacName);
  }

  /**
   * Returns a clone of the configuration
   */
  public Config getConfigClone() {
    return new ManagedConnectionFactoryConfigs(getClonedData());
  }

 /**
  * Returns a XML string representation of the connection factory
  * @return a XML string representation of the connection factory
  */
  public String toString() {
    StringBuffer sb = new StringBuffer();

    sb.append(xmlHelper.createTag("managedConnectionFactories", false));
    for (Iterator iter = mData.keySet().iterator(); iter.hasNext();) {
      String name = (String)iter.next();
      Object value = mData.get(name);
      if (value instanceof ManagedConnectionFactoryConfig)
        sb.append(((ManagedConnectionFactoryConfig)value).toString());
    }
    sb.append(xmlHelper.createEndTag("managedConnectionFactories"));
    return sb.toString();
  }

  /**
   * Returns a Map containing configurations for managed connection factories
   * @return a Map containing configurations for managed connection factories
   */
  public Map getManagedConnectionFactoryConfigs() {
    Map facConfigs = new HashMap();
    for(Iterator iter = mData.keySet().iterator(); iter.hasNext();) {
      String key = (String)iter.next();
      Object value = mData.get(key);
      if (value instanceof ManagedConnectionFactoryConfig) {
        facConfigs.put(key, value);
      }
    }
    return facConfigs;
  }


}
