/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      19 February 2002
*****************************************************************************/
package com.sap.isa.core.eai;

// java imports
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.sap.isa.core.eai.config.Config;
import com.sap.isa.core.eai.config.ConfigBase;

/**
 * This class represents a collection of connection configurations
 */

public class BackendBusinessObjectConfigs extends ConfigBase {

  public BackendBusinessObjectConfigs () {}


  public BackendBusinessObjectConfigs (Map backendBOConfi) {
    mData = backendBOConfi;
  }

  /**
   * Adds a new backend business object configuration
   * @param connectionDefinition a new connection definition
   */
  public void addBackendBOConfig(BackendBusinessObjectConfig backdendBO) {
    mData.put(backdendBO.getType(), backdendBO);
  }

  /**
   * Returns meta data of a specific backend business object
   * @param backendBOType the type of the backend business object
   * @return meta data of a specific backend business object
   */
  public BackendBusinessObjectConfig getBackendBOConfig(String backendBOType) {
    return (BackendBusinessObjectConfig)mData.get(backendBOType);
  }

  /**
   * Returns a clone of the configuration
   */
  public Config getConfigClone() {
    return new BackendBusinessObjectConfigs(getClonedData());
  }

 /**
  * Returns a XML string representation of the connection factories
  * @return a XML string representation of the connection factories
  */
  public String toString() {
    StringBuffer sb = new StringBuffer();

    sb.append(xmlHelper.createTag("businessObjects", false));
    for (Iterator iter = mData.keySet().iterator(); iter.hasNext();) {
      String type = (String)iter.next();
      Object value = mData.get(type);
      if (value instanceof BackendBusinessObjectConfig)
        sb.append(((BackendBusinessObjectConfig)value).toString());
    }
    sb.append(xmlHelper.createEndTag("businessObjects"));
    return sb.toString();
  }

  /**
   * Returns a Map containing configurations for backend business objects
   * @return a Map containing configurations for backend business objects
   */
  public Map getBackendBusinessObjectConfigs() {
    Map boConfigs = new HashMap();
    for(Iterator iter = mData.keySet().iterator(); iter.hasNext();) {
      String key = (String)iter.next();
      Object value = mData.get(key);
      if (value instanceof BackendBusinessObjectConfig) {
        boConfigs.put(key, value);
      }
    }
    return boConfigs;
  }


}