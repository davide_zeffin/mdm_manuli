/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      19 February 2002
*****************************************************************************/
package com.sap.isa.core.eai;

// java imports
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.sap.isa.core.eai.config.Config;
import com.sap.isa.core.eai.config.ConfigBase;
import com.sap.isa.core.util.XMLHelper;

/**
 * This class represents a collection of connection configurations
 */

public class ConnectionConfigs extends ConfigBase {

  private static XMLHelper xmlHelper = new XMLHelper();

  public ConnectionConfigs () {}


  public ConnectionConfigs (Map connectionConfigs) {
    mData = connectionConfigs;
  }

  /**
   * Adds a new connection definition to this backend connection factory
   * @param connectionDefinition a new connection definition
   */
  public void addConnectionConfig(ConnectionConfig connectionConfig) {
    mData.put(connectionConfig.getName(), connectionConfig);
  }

  /**
   * Returns a specific connection configuration
   * @param connectionDefinitionName name of connection configuration
   * @return A connection configuration
   */
  public ConnectionConfig getConnectionConfig(String connectionConfigName) {
    return (ConnectionConfig)mData.get(connectionConfigName);
  }

  /**
   * Returns a clone of the configuration
   */
  public Config getConfigClone() {
    return new ConnectionConfigs(getClonedData());
  }

 /**
  * Returns a XML string representation of the connection
  * @return a XML string representation of the connection
  */
  public String toString() {
    StringBuffer sb = new StringBuffer();
    sb.append(xmlHelper.createTag("connections", false));
    for (Iterator iter = mData.keySet().iterator(); iter.hasNext();) {
      String name = (String)iter.next();
      Object value = mData.get(name);
      if (value instanceof ConnectionConfig)
         sb.append(((ConnectionConfig)value).toString());
    }
    sb.append(xmlHelper.createEndTag("connections"));
    return sb.toString();
  }

  /**
   * Returns a hashtable containing all connection configurations
   * @return a hashtable containing all connection configurations
   */
  public Map getConnectionConfigs() {
    Map conConfs = new HashMap();
    for(Iterator iter = mData.keySet().iterator(); iter.hasNext();) {
      String key = (String)iter.next();
      Object value = mData.get(key);
      if (value instanceof ConnectionConfig) {
        conConfs.put(key, value);
      }
    }
    return conConfs;
  }

}