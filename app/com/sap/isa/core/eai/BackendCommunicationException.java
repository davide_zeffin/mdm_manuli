/*****************************************************************************
    Class:        BackendCommunicationException
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Thomas Smits
    Created:      28.3.2001
    Version:      1.0

    $Revision: #2 $
    $Date: 2001/06/28 $
*****************************************************************************/

package com.sap.isa.core.eai;

/**
 * Represents a general superclass for all exceptions of the backend layer
 * caused by communication problems (network failures etc.).
 *
 * @author Thomas Smits
 * @version 1.0
 */
public class BackendCommunicationException extends BackendException {

    /**
     * Creates a new exception without any message
     */
    public BackendCommunicationException() {
        super();
    }

    /**
     * Creates a new exception with a given message
     *
     * @param msg Message to give further information about the exception
     */
    public BackendCommunicationException(String msg) {
        super(msg);
    }
}