/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Creator:      Marek Barwicki
  Created:      08 May 2001

  $Id: //java/sapm/esales/30/src/com/sap/isa/core/eai/BackendObjectManager.java#5 $
  $Revision: #5 $
  $Change: 26908 $
  $DateTime: 2001/08/03 14:31:36 $
  $Author: d025909 $
*****************************************************************************/
package com.sap.isa.core.eai;



/**
 * This interface provides methods for accessing the Backen Object Manager.
 * The Backend Object Manager is responsible for creating Backend Object. Each
 * backend object provides is accessed via an well-defined interface.
 */
public interface BackendObjectManager {
	
	
	/**
	 * Constant to store language information in the backend container. <br> 
	 */
	public final static String LANGUAGE = "isalanguage";	
	
	/**
	 * Constant to store ui controller in the backend container. <br> 
	 */
	public final static String UI_CONTROLLER = "uiController";	
	

  /**
   * Creates a backend object and associates a default connection (if defined) to it.
   * @param beBOType specifies the type of the business object. The value of the
   *        <code>beBOType</code> is defined externally in the configuration
   *        file
   * @param params a set of runtime parameters which are passed to the
   *        <code>initBackendObject</code> method of the <code>BackendBusinessObject</code>
   *
   */
  public BackendBusinessObject createBackendBusinessObject(String beBOType, BackendBusinessObjectParams params)
        throws BackendException;

  /**
   * Creates a backend object and associates a default connection (if defined) to it.
   * This is a convenience method for backend objects which do not need any
   * parameters. The value of the parameter is implicitly <code>null</code>
   * @param beBOType specifies the type of the business object. The value of the
   *        <code>beBOType</code> is defined externally in the configuration
   *        file
   */
  public BackendBusinessObject createBackendBusinessObject(String beBOType)
        throws BackendException;


  /**
   * This method returns a container with metadata on backend business objects
   * Please not that this information is cloned from the original data.
   * Changing this data will not have any effect on the behaviour of the
   * EAI layer
   * @param type type of backend business object
   * @return container containing meta data on backend business object. <code>
   *    null</code> if no backend business object for the given type is found.
   */
  public BackendBusinessObjectMetaData
        getBackendBusinessObjectMetaData(String type);

  /**
   * Binds the object to the backend context using the name specified. The context
   * is common to all backend business object created by the same Backend Object Manager
   * instance
   * @param name The name to which the object is bound; cannot be <code>null</code>
   * @param value The object to be bound; cannot be <code>null</code>
   */
  public void setAttribute(String name, Object value);

  /**
   * Returns the object bound with the specified name in this session,
   * or <code>null</code> if no object is bound under the name
   * @param name a string specifying the name of the object
   * @returns the object with the specified name
   */
  public Object getAttribute(String name);

  /**
   * This method returns an unique id which is associated with the Backend Object
   * Manager. The id can be passed to the constructor of the Backend Object
   * Manager.
   * @return the id of the Backend Object Manager
   */
  public String getId();

  /**
   * This method can be used to add a connection listener to all object of
   * one type created by the BackendObjectManager
   * @param boType
   */
  public void addConnectionListener(String boType, ConnectionEventListener listener);

  /**
   * Returns the backend configuration used by this backend object manager
   * @return the backend configuration used by this backend object manager
   */
  public BackendConfig getBackendConfig();

  /**
   * Returns a key identifying a backend configuration
   * @return a key identifying a backend configuration
   */
  public String getBackendConfigKey();

}