/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      17 February 2001
*****************************************************************************/
package com.sap.isa.core.eai;

// java imports
import java.util.Map;
import java.util.Properties;

import com.sap.isa.core.eai.config.Config;
import com.sap.isa.core.eai.config.ConfigBase;
import com.sap.isa.core.util.XMLHelper;

/**
 * This class holds data needed to set up a specific managed connection
 */

public class ConnectionConfig extends ConfigBase {


  private static final String PROPNAME_NAME = "name";
  private static final String PROPNAME_CLASS_NAME = "classname";
  private static final String PROPNAME_CONNECTION_DEFINITION_NAME = "connectionDefinitionName";
  private static final String PROPNAME_PROPERTIES = "properties";

  private static XMLHelper xmlHelper = new XMLHelper();
  /**
   * Default constructor
   */
  public ConnectionConfig() {
    getInternalProps();
  };

  /**
   * Constructor
   * @param name name of connection
   * @param className implementation class name of connection
   * @param connectionDefinition Connection definition which is used to create this
   *        connection
   */
  public ConnectionConfig(String name, String className, String connectionDefinition) {
    mData.put(PROPNAME_NAME, name);
    mData.put(PROPNAME_CLASS_NAME, className);
    mData.put(PROPNAME_CONNECTION_DEFINITION_NAME, connectionDefinition);
    getInternalProps();
  }

  public ConnectionConfig(Map data) {
    mData = data;
    getInternalProps();
  }


  /**
   * Sets name of connection
   * @param name of connection
   */
  public void setName(String name) {
    mData.put(PROPNAME_NAME, name);
  }

  /**
   * Gets name of connection
   * @return Name of connection
   */
  public String getName() {
    return (String)mData.get(PROPNAME_NAME);
  }

  /**
   * Sets class name of connection
   * @param class name of connection
   */
  public void setClassName(String className) {
    mData.put(PROPNAME_CLASS_NAME, className);
  }

  /**
   * Gets name of implementation class
   * @return name of implementation class
   */
  public String getClassName() {
    return (String)mData.get(PROPNAME_CLASS_NAME);
  }

  /**
   * Sets connection definition name of the connection definition which should
   * be uses to create this connection
   */
  public void setConnectionDefinition(String connectionDefinition) {
    mData.put(PROPNAME_CONNECTION_DEFINITION_NAME, connectionDefinition);
  }

  /**
   * Returns the name of the conneciton definition used by this connection
   * @return the name of the conneciton definition used by this connection
   */
  public String getConnectionDefinition() {
    return (String)mData.get(PROPNAME_CONNECTION_DEFINITION_NAME);
  }

  /**
   * Clones this connection configuration
   */
  public Config getConfigClone() {
    return new ConnectionConfig(getClonedData());
  }

  /**
   * Adds properties to the connection configuration
   * @param propName name of property
   * @param propValue value of property
   */
  public void addProperty(String name, String value) {
    getInternalProps().setProperty(name, value);
  }

  /**
   * Returns a set of properties which are associated with the connection
   * @return a set of properties
   */
  public Properties getProperties() {
    return getInternalProps();
  }

  /**
   * Returns the internal properties object
   * @param the internal properties object
   */
  private Properties getInternalProps() {
    if (mData.get(PROPNAME_PROPERTIES) == null)
      mData.put(PROPNAME_PROPERTIES, new Properties());

    return (Properties)mData.get(PROPNAME_PROPERTIES);
  }

  /**
   * Returns a XML String representation of this connection configuration
   * @return XML String representation of this connection configuration
   */
  public String toString() {
    StringBuffer sb = new StringBuffer();
    Properties params = new Properties();
    if (getName() != null)
    	params.setProperty("name", getName());
    if (getConnectionDefinition() != null)
    	params.setProperty("connectionDefinition", getConnectionDefinition());
    if (getClassName() != null)
    	params.setProperty("className", getClassName());
    sb.append(xmlHelper.createTag("connection", params, false));
    sb.append(getAsXMLString(getInternalProps()));
    sb.append(xmlHelper.createEndTag("connection"));
    return sb.toString();
  }
}