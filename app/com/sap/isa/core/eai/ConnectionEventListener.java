/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Creator:      Marek Barwicki
  Created:      01 October 2001

  $Id:  $
  $Revision:  $
  $Change:  $
  $DateTime:  $
  $Author: $
*****************************************************************************/
package com.sap.isa.core.eai;

/**
 * This interface should be implemented by an object which would like
 * to receive connection events
 * It has not methods because it will be usually extended by a more
 * specialized listener interface.
 */
public interface ConnectionEventListener {
}