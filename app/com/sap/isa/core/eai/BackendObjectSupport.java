package com.sap.isa.core.eai;

import com.sap.isa.core.RequestContext;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.system.RequestContextContainer;

/**
 * This class provides some additional methods relevant when working
 * with backend business objects
 */
public class BackendObjectSupport {

	private BackendBusinessObjectMetaData mMetaData;
	private BackendObjectManager mBem;


	protected static IsaLocation log =
		IsaLocation.getInstance(BackendObjectSupport.class.getName());

	/**
	 * Constructor
	 * @param bem a reference to the Backend Object Manager
	 * @param metaData the meta data of this backend object
	 */
	public BackendObjectSupport(
		BackendObjectManager bem,
		BackendBusinessObjectMetaData metaData) {
		mMetaData = metaData;
		mBem = bem;
	}

	/**
	 * This method returns a container with metadata on the current backend business object
	 * Please not that this information is cloned from the original data.
	 * Changing this data will not have any effect on the behaviour of the
	 * EAI layer
	 * @return container containing meta data on backend business object.
	 */
	public BackendBusinessObjectMetaData getMetaData() {
		if (log.isDebugEnabled())
			log.debug(
				"Requesting Backend Object meta data [metadata]='"
					+ mMetaData
					+ "'");
		return mMetaData;
	}

	/**
	 * This method returns a container with metadata on any backend business object
	 * Please not that this information is cloned from the original data.
	 * Changing this data will not have any effect on the behaviour of the
	 * EAI layer
	 * @param type type of Backend Object
	 * @return container containing meta data on backend business object.
	 */
	public BackendBusinessObjectMetaData getMetaData(String boType) {
		BackendBusinessObjectMetaData metadata =
			mBem.getBackendBusinessObjectMetaData(boType);
		if (log.isDebugEnabled())
			log.debug(
				"Requesting Backend Object meta data [type]='"
					+ boType
					+ "' [metadata]='"
					+ metadata
					+ "'");

		return metadata;
	}

	/**
	 * Creates a backend object and associates a default connection (if defined) to it.
	 * @param beBOType specifies the type of the business object. The value of the
	 *        <code>beBOType</code> is defined externally in the configuration
	 *        file
	 * @param params a set of runtime parameters which are passed to the
	 *        <code>initBackendObject</code> method of the <code>BackendBusinessObject</code>
	 *
	 */
	public BackendBusinessObject createBackendBusinessObject(
		String beBOType,
		BackendBusinessObjectParams params)
		throws BackendException {
		return mBem.createBackendBusinessObject(beBOType, params);
	}

	/**
	 * Creates a backend object and associates a default connection (if defined) to it.
	 * This is a convenience method for backend objects which do not need any
	 * parameters. The value of the parameter is implicitly <code>null</code>
	 * @param beBOType specifies the type of the business object. The value of the
	 *        <code>beBOType</code> is defined externally in the configuration
	 *        file
	 */
	public BackendBusinessObject createBackendBusinessObject(String beBOType)
		throws BackendException {
		return mBem.createBackendBusinessObject(beBOType);
	}

	/**
	 * Returs the key of the currently used backend configuration. The key consist
	 * of the used backend configuration id and the id if the default data used
	 * by the backend configuration
	 * @return key of the currently used backend configuration
	 */
	public String getBackendConfigKey() {
		return mBem.getBackendConfigKey();
	}

  /**
   * Cleans up support object. For internal use only
   */
  void cleanup() {
	mMetaData = null;
	mBem = null;
  }

	/**
	 * Returns the request context. This context can be used to pass data between layers
	 * of the ISA framework.
	 * The context has the scope of an request (HTTP request).
	 * <p/><b>NOTE:</b><br/>
	 * This context MUST NOT be used in standard development. It is a tool for quickly extending the
	 * functionality of the standard in CUSTOMER projects.
	 * <br/>
	 * @return the request context
	 */
	public RequestContext getRequestContext() {
		return RequestContextContainer.getInstance().getRequestContext();
	}

}
