/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      15 February 2001
*****************************************************************************/
package com.sap.isa.core.eai;

// java imports
import java.util.Set;


/**
 * Base implementation of a Managed Connection
 */
public abstract class ManagedConnectionBase implements ManagedConnection {

  protected String conDefName;
  protected String conConfigName;
  protected String conFactoryName;
  protected String conKey;
  protected ManagedConnectionFactory managedConnectionFactory;
  protected Set mListeners = null;
  protected BackendContext mBackendContext;

  /**
   * Constructor
   */
  public ManagedConnectionBase() {
  }

  /**
   * Sets a connection factory which has created/creates this connection
   * @param backendConnectionFactory Reference to connection factory which has
   *      created/creates this connection
   */
  public void setManagedConnectionFactory(ManagedConnectionFactory managedConnectionFactory) {
    this.managedConnectionFactory = managedConnectionFactory;
  }

  /**
   * Sets name of a connection definition.
   * @param conDefName name of connection definition used by this
   *    managed connection
   */

  public void setConnectionDefinitionName(String conDefName) {
    this.conDefName = conDefName;
  }

  /**
   * Returns name of connection configuration of this connection
   * @return name of connection configuration of this connection
   */
  public String getConnectionDefinitionName() {
    return conDefName;
  }

  /**
   * Cleans up underlying physical connection. In case of JCo this
   * method releases the connection to the JCo client pool
   */
  public void cleanup() {
  }


  /**
   * Destroys the encapsulates JCo connection
   */
  public void destroy() {
  }

  /**
   * Sets a key which unbiguously identifies this connection
   * @param conKey key which unbiguously identifies this connection
   */
  public void setConnectionKey(String conKey) {
    this.conKey = conKey;
  }

  /**
   * Gets a key which unbiguously identifies this connection
   * @return key which unbiguously identifies this connection
   */
  public String getConnectionKey() {
    return conKey;
  }

  /**
   * Sets the name of connection configuration used to create the
   * physical connection
   * @param conConfigName name of connection configuration used to
   *    create the physical connection
   */
  public void setConnectionConfigName(String conConfigName) {
    this.conConfigName = conConfigName;
  }

  /**
   * Gets the name of connection configuration used to create the
   * physical connection
   * @return name of connection configuration used to create the physical connection
   */
  public String getConnectionConfigName() {
    return conConfigName;
  }

  /**
   * Sets the name of connection factory which is used to create the
   * physical connection
   * @param conFactoryName name of connection factory
   */
  public void setConnectionFactoryName(String conFactoryName) {
    this.conFactoryName = conFactoryName;
  }

  /**
   * Gets the name of connection factory used to create the physical
   * connection
   * @return Gets the name of connection factory
   */
  public String getConnectionFactoryName() {
    return conFactoryName;
  }

  /**
   * This method is called after creating the managed connection. Since the
   * connection is stateless this method does nothing
   * @param initParams
   */
  public void init(Object initParams) {
  }

  /**
   * Sets the Backend Context
   * @param context the Backend Context
   */
  public void setBackendContext (BackendContext context) {
    mBackendContext = context;
  }

}