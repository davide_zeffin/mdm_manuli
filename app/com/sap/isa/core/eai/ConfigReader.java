/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      06 March 2001
  $Id: //java/sapm/esales/30/src/com/sap/isa/core/eai/ConfigReader.java#3 $
  $Revision: #3 $
  $Change: 23641 $
  $DateTime: 2001/07/03 14:30:25 $
  $Author: d025909 $
*****************************************************************************/
package com.sap.isa.core.eai;

// java imports
import java.io.InputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.digester.Digester;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;

/**
 * This singelton is a provider for information which can be
 * confired externally
 * @deprecated Configuration is handled by the Extended Configuration Management
 */
public class ConfigReader {

  protected static IsaLocation log = IsaLocation.
        getInstance(ConfigReader.class.getName());

  private static ConfigReaderStrategy configReaderStrategy;
  private static String CRLF = System.getProperty("line.spearator");
  private static ConfigReader configReader;
  private static String configFileName;
  private static Map mgConFactoryConfigs = Collections.synchronizedMap(new HashMap());
  private static Map beBusinessObjectConfigs = Collections.synchronizedMap(new HashMap());
  private static int debugLevel = 0;
  private static InputStream is = null;

  private ConfigReader() {
  }

  /**
   * Returns single instace of the ConfigReader
   * @return Instance of ConfigReader
   */
  public static synchronized ConfigReader getConfigReader() throws BackendException {
    if(configReader == null) {
      configReader = new ConfigReader();

      try {
        configReader.initDigester();
      } catch(ConfigReaderException crex) {
        String errMsg = "Error initializing ConfigReader" + CRLF + crex.toString();
        log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.eai.exception", new Object[] { errMsg }, crex);
        throw new BackendException(errMsg);
      }
    }
    return configReader;
  }

  /**
   * Returns information on a specific backend Business Object
   * @return  information on a specific backend Business Object
   **/
  BackendBusinessObjectConfig getBackendBusinessObjectConfig(String boType) {
    return (BackendBusinessObjectConfig)beBusinessObjectConfigs.get(boType);
  }

  /**
   * Returns a map containing all Backend Business Object configurations
   * @return  a map containing all Backend Business Object configurations
   **/
  Map getBackendBusinessObjectConfigs() {
    return Collections.unmodifiableMap(beBusinessObjectConfigs);
  }

  private void initDigester() throws ConfigReaderException {
    // new Struts digester
    Digester digester = new Digester();
    digester.setDebug(debugLevel);

    // rules for managed connections

    digester.push(this);

    // process connection factory configuration
    // create a new backend factory object
    digester.addObjectCreate("backend/managedConnectionFactories/managedConnectionFactory",
        "com.sap.isa.core.eai.ManagedConnectionFactoryConfig");
    // set all properties which belong to a connection factory configuration
    digester.addSetProperties("backend/managedConnectionFactories/managedConnectionFactory");

    // Create connection definition object
    digester.addObjectCreate("backend/managedConnectionFactories/managedConnectionFactory/connectionDefinitions/connectionDefinition",
      "com.sap.isa.core.eai.ConnectionDefinition");

    // set property of connection definition object
    digester.addSetProperties("backend/managedConnectionFactories/managedConnectionFactory/connectionDefinitions/connectionDefinition");

    // add name/value pairs to connection definition object
    digester.addCallMethod(
        "backend/managedConnectionFactories/managedConnectionFactory/connectionDefinitions/connectionDefinition/param",
        "addProperty", 2, new String[] {"java.lang.String", "java.lang.String"});

    digester.addCallParam("backend/managedConnectionFactories/managedConnectionFactory/connectionDefinitions/connectionDefinition/param"
        , 0, "name");
    digester.addCallParam("backend/managedConnectionFactories/managedConnectionFactory/connectionDefinitions/connectionDefinition/param"
        , 1, "value");

   // add connection definition to connection factory
    digester.addSetNext("backend/managedConnectionFactories/managedConnectionFactory/connectionDefinitions/connectionDefinition",
        "addConnectionDefinition", "com.sap.isa.core.eai.ConnectionDefinition");

    // Create connection configuration object
    digester.addObjectCreate("backend/managedConnectionFactories/managedConnectionFactory/connections/connection",
      "com.sap.isa.core.eai.ConnectionConfig");

    // set property of connection definition object
    digester.addSetProperties("backend/managedConnectionFactories/managedConnectionFactory/connections/connection");

    // add name/value pairs to connection
    digester.addCallMethod(
        "backend/managedConnectionFactories/managedConnectionFactory/connections/connection/params/param",
        "addProperty", 2, new String[] {"java.lang.String", "java.lang.String"});

    digester.addCallParam("backend/managedConnectionFactories/managedConnectionFactory/connections/connection/params/param"
        , 0, "name");
    digester.addCallParam("backend/managedConnectionFactories/managedConnectionFactory/connections/connection/params/param"
        , 1, "value");



    // add connection configuration to connection factory
    digester.addSetNext("backend/managedConnectionFactories/managedConnectionFactory/connections/connection",
        "addConnectionConfig", "com.sap.isa.core.eai.ConnectionConfig");

    // add name/value pairs to connection factory
    digester.addCallMethod(
        "backend/managedConnectionFactories/managedConnectionFactory/params/param",
        "addProperty", 2, new String[] {"java.lang.String", "java.lang.String"});

    digester.addCallParam("backend/managedConnectionFactories/managedConnectionFactory/params/param"
        , 0, "name");
    digester.addCallParam("backend/managedConnectionFactories/managedConnectionFactory/params/param"
        , 1, "value");


    // add connection factory configuration to config reader
    digester.addSetNext("backend/managedConnectionFactories/managedConnectionFactory",
        "addManagedConnectionFactoryConfig", "com.sap.isa.core.eai.ManagedConnectionFactoryConfig");

    // process backend business object configuration
    digester.addObjectCreate("backend/businessObjects/businessObject",
        "com.sap.isa.core.eai.BackendBusinessObjectConfig");

    // set properties of business object configuration
    digester.addSetProperties("backend/businessObjects/businessObject");

    // add properties which belong to business object
    digester.addCallMethod(
        "backend/businessObjects/businessObject/params/param",
        "addProperty", 2, new String[] {"java.lang.String", "java.lang.String"});

    digester.addCallParam("backend/businessObjects/businessObject/params/param"
        , 0, "name");
    digester.addCallParam("backend/businessObjects/businessObject/params/param"
        , 1, "value");

    // add business object configuration to the config reader
    // add connection factory configuration to config reader
    digester.addSetNext("backend/businessObjects/businessObject",
        "addBackendBusinessObjectConfig", "com.sap.isa.core.eai.BackendBusinessObjectConfig");

    try {
      if (is != null)
        digester.parse(is);
      else if (configFileName == null || configFileName.length() > 0) {

        log.info("system.eai.init.readConfigFile", new Object[] { configFileName }, null);

        digester.parse(configFileName);
      } else
        throw new ConfigReaderException("Error reading configuration information" + CRLF +
            "No input stream or file name set");

      if (configReaderStrategy != null) {
        if (log.isDebugEnabled())
          log.debug("Post initialization phase...");

        configReaderStrategy.postInit(mgConFactoryConfigs, beBusinessObjectConfigs);
      }
    } catch (Exception ex) {
      String errMsg = "Error reading configuration information" + CRLF + ex.toString();
      log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.eai.exception", new Object[] { errMsg }, ex);
      throw new ConfigReaderException(errMsg);
    }
  }

  /**
   * Sets file name (and path) of configuration file
   * @param configFileName name (and path) of configuration file
   */
  public static void setConfigFileName(String configFileName) {
    ConfigReader.configFileName = configFileName;
  }
  /**
   * Sets debug level of the Config reader
   */
  public static void setDebugLevel(int debugLevel) {
    ConfigReader.debugLevel = debugLevel;
  }

  /**
   * Sets an input stream which will be used to access the configuration file
   * @param is input stream
   */
  public static void setInputStream(InputStream is) {
    ConfigReader.is = is;
  }

  /**
   * Adds a new managed connection factory configuration
   * @param mgConFactoryConfig a new managed connection factory configuration
   */
  public void addManagedConnectionFactoryConfig(ManagedConnectionFactoryConfig mgConFactoryConfig) {
    mgConFactoryConfigs.put(mgConFactoryConfig.getName(), mgConFactoryConfig);
  }

  /**
   * Adds a new BackendBusinessObjectConfig
   * @param beBusinessObjectConfig a new BackendBusinessObjectConfig
   */
  public void addBackendBusinessObjectConfig(BackendBusinessObjectConfig beBusinessObjectConfig) {
    beBusinessObjectConfigs.put(beBusinessObjectConfig.getType(), beBusinessObjectConfig);
  }

  /**
   * Returns a Map containing configurations for managed connection factories
   * @return a Map containing configurations for managed connection factories
   */
  public Map getManagedConnectionFactoryConfigs() {
    return Collections.unmodifiableMap(mgConFactoryConfigs);
  }

  /**
   * Sets the ConfigReader strategy object. This passed object has to implement
   * the <code>ConfigReaderStrategy</code> interface
   */
  public static void setConfigReaderStrategy(ConfigReaderStrategy configReaderStrategy) {
    ConfigReader.configReaderStrategy = configReaderStrategy;
  }
}