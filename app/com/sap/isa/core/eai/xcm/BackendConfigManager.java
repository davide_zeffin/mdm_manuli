package com.sap.isa.core.eai.xcm;

// java imports
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.digester.Digester;

import com.sap.isa.core.eai.BackendConfig;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;

/**
 * This singelton is a provider for information used by the eai layer
 * This class is used when the Exteded Configuration Management is
 * activated
 */
public class BackendConfigManager {


  protected static IsaLocation log = IsaLocation.
        getInstance(BackendConfigManager.class.getName());

  private Map mBackendConfigs      = new HashMap();

  private static String CRLF = System.getProperty("line.spearator");
  private static String mDataFileName;
  private static String mConfigFileName;
  private static int debugLevel = 0;
  private static BackendConfigManager mConfigManager = new BackendConfigManager();
  private boolean mIsInit = false;

  /**
   * Class is a singelton
   */
  private BackendConfigManager() {}
  /**
   * Returns an instance of the Config Manager
   * @return an instance of the Config Manager
   */
  public static BackendConfigManager getConfigManager() {
    return mConfigManager;
  }

  /**
   * Adds a backend configuration
   * @param config a backend configuration
   */
  public void addBackendConfig(String id, BackendConfigContainer config) {
    if (config == null || id == null)
      return;
    if (log.isDebugEnabled()) {
    	log.debug("Adding new backend configuration [id]='" + id + "'");
    }
    mBackendConfigs.put(id, config);
  }

  /**
   * Sets file name (and path) of eai config configuration file
   * @param configFileName file name (and path) of eai config configuration file
   */
  public static void setConfigFileName(String configFileName) {
    mConfigFileName = configFileName;
  }

  /**
   * Sets debug level of the Config reader
   * @param debugLevel
   */
  public static void setDebugLevel(int debugLevel) {
    BackendConfigManager.debugLevel = debugLevel;
  }

  private void readConfigFile(String id, InputStream is) throws BackendConfigManagerException {
    // new Struts digester
    Digester digester = new Digester();
    digester.setDebug(debugLevel);

	BackendConfigContainer confContainer = new BackendConfigContainer();

    digester.push(confContainer);

    // create new backend config object
    digester.addObjectCreate("backend/configs/config",
        "com.sap.isa.core.eai.BackendConfig");

    // set all properties which belong to data source config object
    digester.addSetProperties("backend/configs/config");

    digester.addObjectCreate("backend/configs/config/managedConnectionFactories/managedConnectionFactory",
        "com.sap.isa.core.eai.ManagedConnectionFactoryConfig");
    // set all properties which belong to a connection factory configuration
    digester.addSetProperties("backend/configs/config/managedConnectionFactories/managedConnectionFactory");

    // Create connection definition object
    digester.addObjectCreate("backend/configs/config/managedConnectionFactories/managedConnectionFactory/connectionDefinitions/connectionDefinition",
      "com.sap.isa.core.eai.ConnectionDefinition");

    // set property of connection definition object
    digester.addSetProperties("backend/configs/config/managedConnectionFactories/managedConnectionFactory/connectionDefinitions/connectionDefinition");

    // add name/value pairs to connection definition object
    digester.addCallMethod(
        "backend/configs/config/managedConnectionFactories/managedConnectionFactory/connectionDefinitions/connectionDefinition/params/param",
        "addProperty", 2, new String[] {"java.lang.String", "java.lang.String"});

    digester.addCallParam("backend/configs/config/managedConnectionFactories/managedConnectionFactory/connectionDefinitions/connectionDefinition/params/param"
        , 0, "name");
    digester.addCallParam("backend/configs/config/managedConnectionFactories/managedConnectionFactory/connectionDefinitions/connectionDefinition/params/param"
        , 1, "value");

   // add connection definition to connection factory
    digester.addSetNext("backend/configs/config/managedConnectionFactories/managedConnectionFactory/connectionDefinitions/connectionDefinition",
        "addConnectionDefinition", "com.sap.isa.core.eai.ConnectionDefinition");

    // Create connection configuration object
    digester.addObjectCreate("backend/configs/config/managedConnectionFactories/managedConnectionFactory/connections/connection",
      "com.sap.isa.core.eai.ConnectionConfig");

    // set property of connection definition object
    digester.addSetProperties("backend/configs/config/managedConnectionFactories/managedConnectionFactory/connections/connection");

    // add name/value pairs to connection
    digester.addCallMethod(
        "backend/configs/config/managedConnectionFactories/managedConnectionFactory/connections/connection/params/param",
        "addProperty", 2, new String[] {"java.lang.String", "java.lang.String"});

    // add connection parameters
    digester.addCallParam("backend/configs/config/managedConnectionFactories/managedConnectionFactory/connections/connection/params/param"
        , 0, "name");
    digester.addCallParam("backend/configs/config/managedConnectionFactories/managedConnectionFactory/connections/connection/params/param"
        , 1, "value");

    // add connection configuration to connection factory
    digester.addSetNext("backend/configs/config/managedConnectionFactories/managedConnectionFactory/connections/connection",
        "addConnectionConfig", "com.sap.isa.core.eai.ConnectionConfig");

    // add name/value pairs to connection factory
    digester.addCallMethod(
        "backend/configs/config/managedConnectionFactories/managedConnectionFactory/params/param",
        "addProperty", 2, new String[] {"java.lang.String", "java.lang.String"});

    digester.addCallParam("backend/configs/config/managedConnectionFactories/managedConnectionFactory/params/param"
        , 0, "name");
    digester.addCallParam("backend/configs/config/managedConnectionFactories/managedConnectionFactory/params/param"
        , 1, "value");


    // add connection factory configuration to backend config
    digester.addSetNext("backend/configs/config/managedConnectionFactories/managedConnectionFactory",
        "addManagedConnectionFactoryConfig", "com.sap.isa.core.eai.ManagedConnectionFactoryConfig");


    // process backend business object configuration
    digester.addObjectCreate("backend/configs/config/businessObjects/businessObject",
        "com.sap.isa.core.eai.BackendBusinessObjectConfig");

    // set properties of business object configuration
    digester.addSetProperties("backend/configs/config/businessObjects/businessObject");

    // add properties which belong to business object
    digester.addCallMethod(
        "backend/configs/config/businessObjects/businessObject/params/param",
        "addProperty", 2 , new String[] {"java.lang.String", "java.lang.String"});

    digester.addCallParam("backend/configs/config/businessObjects/businessObject/params/param"
        , 0, "name");
    digester.addCallParam("backend/configs/config/businessObjects/businessObject/params/param"
        , 1, "value");

    // add business object configuration to the config manager
    // add connection factory configuration to config reader
    digester.addSetNext("backend/configs/config/businessObjects/businessObject",
        "addBackendBOConfig", "com.sap.isa.core.eai.BackendBusinessObjectConfig");

    // add backend configuration
    digester.addSetNext("backend/configs/config",
        "setBackendConfig", "com.sap.isa.core.eai.BackendConfig");


    try {
      if (is != null) {
        digester.parse(is);
       	// store configuration
       	
       	addBackendConfig(id, confContainer); 
        
      }
        
        
        
      else 
        throw new BackendConfigManagerException("Error reading configuration information" + CRLF +
            "No input stream for [id]='" + id + "'");

    } catch (Exception ex) {
      String errMsg = "Error reading configuration information" + CRLF + ex.toString();
      log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.xcm.exception", new Object[] { errMsg }, ex);
      BackendConfigManagerException xcmEx = new BackendConfigManagerException(errMsg);
      xcmEx.setBaseException(ex);
      throw xcmEx;
    } finally {
      try {
        is.close();
      } catch (Exception ex) {
        String errMsg = "Error closing input stream" + CRLF + ex.toString();
        log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.eai.exception", new Object[] { errMsg }, ex);
        throw new BackendConfigManagerException(errMsg);

      }
    }
  }

  /**
   * Returns an existing backend configuration. 
   * @param id id of configuration
   * @return a backend configuration
   */
  public BackendConfig getExistingBackendConfig(String id) {
    // get configuration for a given key
    BackendConfigContainer configContainer 
    		= (BackendConfigContainer)mBackendConfigs.get(id);
    if (configContainer == null || configContainer.getBackendConfigs() == null) {
      String msg = "Backend configuration '" + id + "' does not exist";
      log.warn("system.exception", new Object[] { msg }, null);
      return null;
    } 
    
    Map map = configContainer.getBackendConfigs();
    
	BackendConfig config = configContainer.getBackendConfig();

    return config;
  }

  /**
   * Returns an new backend configuration. 
   * @param id id of configuration
   * @param is input stream from where the configuration is retrieved from 
   * @return a backend configuration
   */
  public BackendConfig getNewBackendConfig(String id, InputStream is) {
    init(id, is);
    // get configuration for a given key
    return getExistingBackendConfig(id);
  }



  /**
   * performs the initialization of the config manager
   */
  private synchronized void init(String id, InputStream is) {
	if (log.isDebugEnabled()) 
		log.debug("Reading new backend configuration for [id]='" + id + "'");

    try {
        // read config file
        readConfigFile(id, is);
      } catch (BackendConfigManagerException ex) {
        log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.exception", new Object[] { "Error initializing Config Manager" }, ex);
      }
  }

	/**
	 * Returns true if the configuration for the given key is not
	 * loaded yet
	 * @param configid id of configuration
	 * @return true if configuration is not loaded yet
	 */
	public boolean isNewBackendConfig(String configid) {
		if (mBackendConfigs.get(configid) == null)
			return true;
		else 
			return false;	
	}

  
  /**
   * Stores backend configurations
   */
  public class BackendConfigContainer{
  		
 		Map mBackendConfigIds = new HashMap();
  		BackendConfig mConfig = null;
  	
	  /**
	   * Adds a backend configuration
	   * @param config a backend configuration
	   */
	  public void setBackendConfig(BackendConfig config) {
	  	mConfig = config;
	  }
	
		public BackendConfig getBackendConfig () {
			return mConfig;
		}  
	
		public Map getBackendConfigs() {
			return mBackendConfigIds;
		}  
  }
}
