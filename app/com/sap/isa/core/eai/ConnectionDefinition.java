/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      06 February 2001
*****************************************************************************/
package com.sap.isa.core.eai;
import java.util.Map;
import java.util.Properties;

import com.sap.isa.core.eai.config.Config;
import com.sap.isa.core.eai.config.ConfigBase;
import com.sap.isa.core.util.XMLHelper;


/**
 * This class is a generic container for data needed by a specific connection
 * factory to create backend connections
 */
public class ConnectionDefinition extends ConfigBase {

  private static final String PROPNAME_NAME = "name";
  private static final String PROPNAME_PROPERTIES = "properties";
  private static XMLHelper xmlHelper = new XMLHelper();

  /**
   * Default constructor
   */
  public ConnectionDefinition() {
    getInternalProps();
  }

  /**
   * Default constructor
   */
  public ConnectionDefinition(Map data) {
    mData = data;
    getInternalProps();
  }


  /**
   * Constructs a connection definition object
   * @param name of connection definition
   * @param connectionProps set properties which specify the connection definition
   */
  public ConnectionDefinition(String name, Properties connectionProps) {
    mData.put(PROPNAME_NAME, name);
    mData.put(PROPNAME_PROPERTIES, connectionProps);
  }

  /**
   * Gets connection definitionj name
   * @return connection definition name
   */
  public String getName() {
    return (String)mData.get(PROPNAME_NAME);
  }

  /**
   * Sets the name of this connection definition
   * @param name name of connection definition
   */
  public void setName(String name) {
    mData.put(PROPNAME_NAME, name);
  }

  /**
   * Gets connection configuration properties
   * @return connection configuration properties
   */
   public Properties getProperties() {
    return getInternalProps();
  }

  /**
   * Sets connection properties
   * @param conProps connection properties
   */
  public void setProperties(Properties conProps) {
    mData.put(PROPNAME_PROPERTIES, conProps);
  }

  public void addProperty(String propName, String propValue) {
    getInternalProps().setProperty(propName, propValue);
  }

  /**
   * Clones this connection definition
   */
  public Config getConfigClone()  {
    return new ConnectionDefinition(getClonedData());
  }

  /**
   * Returns the internal properties object
   * @param the internal properties object
   */
  private Properties getInternalProps() {
    if (mData.get(PROPNAME_PROPERTIES) == null)
      mData.put(PROPNAME_PROPERTIES, new Properties());
	
	Properties props = (Properties)mData.get(PROPNAME_PROPERTIES);
		
    return props; 
  }

  /**
   * Returns a XML String representation of this connection definition
   * @return XML String representation of this connection definition
   */
  public String toString() {
    StringBuffer sb = new StringBuffer();
    if (getName() != null)
    	sb.append(xmlHelper.createTag("connectionDefinition", "name", getName(), false));
    else
    	sb.append(xmlHelper.createTag("connectionDefinition", false));
    Properties props = (Properties)getInternalProps().clone();
	if (props.getProperty("passwd") != null);
		props.setProperty("passwd", "***");
    
    sb.append(getAsXMLString(props));
	

    sb.append(xmlHelper.createEndTag("connectionDefinition"));

    return sb.toString();
  }
}