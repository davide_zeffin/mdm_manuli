/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      19 March 2001

  $Revision: #2 $
  $Date: 2001/06/28 $
*****************************************************************************/
package com.sap.isa.core.eai;

/**
 * This class acts a container for static metadata describing a backend business
 * object
 */
public class BackendBusinessObjectMetaData
{

  private final BackendBusinessObjectConfig beBOConfig;
  private final ConnectionConfig defaultConConfig;
  private final ConnectionDefinition defaultConDef;
  private final String mConfigKey; 

  /**
   * Constructs a <code>BackendBusinessObjectMetaData</code> object
   */
  public BackendBusinessObjectMetaData(String key,
            BackendBusinessObjectConfig beBOConfig,
            ConnectionConfig defaultConConfig,
            ConnectionDefinition defaultConDef) {
      this.mConfigKey = key;
      this.beBOConfig = beBOConfig;
      this.defaultConConfig = defaultConConfig;
      this.defaultConDef = defaultConDef;
  }

  /**
   * Gets meta data of backend business object
   * @return meta data of backend business object
   */
  public BackendBusinessObjectConfig getBackendBusinessObjectConfig() {
    return beBOConfig;
  }

  /**
   * Gets meta data of the default connection
   * @return meta data of the default connection
   */
  public ConnectionConfig getDefaultConnectionConfig() {
    return defaultConConfig;
  }

  /**
   * Gets meta data of connectection definition which belongs to the
   * default connection
   * @return meta data of connectection definition which belongs to the
   * default connection
   */
  public ConnectionDefinition getDefaultConnectionDefinition() {
    return defaultConDef;
  }

  /**
   * Returs the key of the backend configuration this meta data belongs to. 
   * @return key of the currently used backend configuration
   */
  public String getBackendConfigKey() {
	return mConfigKey; 
  }


}