/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Creator:      Marek Barwicki
  Created:      01 October 2001

  $Id:  $
  $Revision:  $
  $Change:  $
  $DateTime:  $
  $Author:  $
*****************************************************************************/
package com.sap.isa.core.eai;

// java imports
import java.util.EventObject;

import com.sap.isa.core.RequestContext;
import com.sap.isa.core.logging.IsaSession;
import com.sap.isa.core.system.RequestContextContainer;

/**
 * This class provides information on a connection event
 * The BackendContext and Request Context can be used to exchange data
 * with other layers 
 * @see com.sap.isa.core.RequestContext RequestContext
 */
public class ConnectionEvent extends EventObject {

	private BackendContext mContext;
	/**
	 * Constructs an event
	 * @param source the source of the event
	 */
	public ConnectionEvent(Object source, BackendContext context) {
		super(source);
		mContext = context;
	}

	/**
	 * Returns a reference to the backend context
	 * @return a reference to the backend context.
	 */
	public BackendContext getBackendContext() {
		return mContext;
	}

	/**
	 * Returns the request context. This context can be used to pass data between layers
	 * of the ISA framework.
	 * The context has the scope of an request (HTTP request).
	 * <p/><b>NOTE:</b><br/> 
	 * This context MUST NOT be used in standard development. It is a tool for quickly extending the 
	 * functionality of the standard in CUSTOMER projects. 
	 * <br/>
	 * @return the request context
	 */
	public RequestContext getRequestContext() {
		return RequestContextContainer.getInstance().getRequestContext();
	}

}