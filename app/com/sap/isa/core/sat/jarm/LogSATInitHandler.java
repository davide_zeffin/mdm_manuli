package com.sap.isa.core.sat.jarm;

// struts imports
import java.util.Properties;

import com.sap.isa.core.init.Initializable;
import com.sap.isa.core.init.InitializationEnvironment;
import com.sap.isa.core.init.InitializeException;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.util.monitor.jarm.sat.SatCheckHandler;

/**
 * Class that configures the Single Activity Trace
 */
public class LogSATInitHandler implements Initializable {

	protected static IsaLocation log =
			IsaLocation.getInstance(LogSATInitHandler.class.getName());


    /**
     * Creates new LogConfigurator
     */
    public LogSATInitHandler() {}
 
    /**
     * This method will be called from the <code>ActionServlet</code>
     * It initializes the Single Activity Trace facilities.
     *
     * @param servlet The ActionServlet we are called from
     * @param props The Properties required by the initialization
     *
     * @exception InitializationException at any problem during the intialization
     */
    public void initialize(InitializationEnvironment environment,
                           Properties props)
                 throws InitializeException {
             
        try {    	
        	log.debug("Initializing Single Activity Trace");
			ISASATCheck isaSatCheck = new ISASATCheck();
			SatCheckHandler.registerSatCheck(isaSatCheck);                	
        } catch (Throwable t) {
        	String msg = "Error during initialization of Single Activity Trace";
        	log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.exception", new Object[] { msg }, t);
        	}
        }

    /**
     *  This method does nothing here.
     */
    public void terminate() {
    }

   
}