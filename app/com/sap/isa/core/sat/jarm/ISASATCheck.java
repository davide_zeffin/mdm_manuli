package com.sap.isa.core.sat.jarm;

import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import com.sap.util.monitor.jarm.sat.ISatCheck;
import com.sap.isa.core.logging.IsaLocation;

/**
 * Determines whether Single Activity Trace is turned in 
 */
public class ISASATCheck implements ISatCheck, HttpSessionBindingListener {

	private static Set mSessions = new HashSet();
	private static Set mJarmSessions = new HashSet();

	protected static IsaLocation log =
			IsaLocation.getInstance(ISASATCheck.class.getName());


	/**
	 * @see com.sap.util.monitor.jarm.sat.ISatCheck#isTraceOn(java.lang.String, java.lang.String)
	 */
	public boolean isTraceOn(String userName, String reqName) {
		if (mSessions.contains(userName))
			return true;
		else 
			return false;
	}

	/**
	 * @see com.sap.util.monitor.jarm.sat.ISatCheck#isTraceOn(java.lang.String, java.lang.String, long, int)
	 */
	public boolean isTraceOn(String userName, String reqName, long duration, int outData) {
		if (mSessions.contains(userName))
			return true;
		else 
			return false;

	}

	public static void setTraceOn(String sessionId) {
		if (log.isDebugEnabled()) 
			log.debug("Single Activity Trace activated for [session]='" + sessionId + "'");

		mSessions.add(sessionId);
	}
	
	public static void setJarmOn(String sessionId) {
		if (log.isDebugEnabled()) 
			log.debug("Jarm activated for [session]='" + sessionId + "'");

		mJarmSessions.add(sessionId);
	}
	
	/**
	 * @see javax.servlet.http.HttpSessionBindingListener#valueBound(javax.servlet.http.HttpSessionBindingEvent)
	 */
	public void valueBound(HttpSessionBindingEvent arg0) {
	}
	/**
	 * Removes session from the list of session which are to be traced
	 */
	public void valueUnbound(HttpSessionBindingEvent event) {
		String sessionid = event.getSession().getId();
		if (log.isDebugEnabled())
			log.debug("Unregister Single Activity Trace for [sessionid]='" + sessionid + "'"); 
		
		mSessions.remove(sessionid);
		mJarmSessions.remove(sessionid);
	}
	
	public static boolean isTraceOn(String sessionId) {
		return mSessions.contains(sessionId);  
	}
	
	public static boolean isJarmOn(String sessionId) {
		return mJarmSessions.contains(sessionId);  
	}
}
