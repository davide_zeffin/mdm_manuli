package com.sap.isa.core.security;

import java.rmi.RemoteException;

import javax.naming.NamingException;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.conv.PasswordConverter;


/**
 * Provides secure store helper utility for xcm
 */
public class SecureStore implements PasswordConverter {

	public static final String ERROR_RETRIEVING_SECURE_STORE = "ERROR RETRIEVING VALUE FROM SECURE STORE";
 
 	private static final String MODE_ENCRYPT = "#encrypt";
	private static final String MODE_STORE = "#store"; 	
																		//is stored in securestore
	private static final IsaLocation log = IsaLocation.getInstance(SecureStore.class.getName());
 
	/**
	 * Save <code>paramValue</code> in secure store with key comprising of
	 * <code>#securestorage#compName#configName#paramName#</code>.
	 * If unsuccessful in saving into securestore encrypt the value and return
	 * encrypted string (provide minimal security)
	 * @param compName Component name
	 * @param configName configuration name
	 * @param paramName Parameter of a component
	 * @param paramValue Value to be saved to secure store
	 * @return Key used to store the value in secure store, if successful saving in securestore
	 * 	If failed to store in secure store, return encrypted  <code>paramValue</code>
	 *
	 */
	public String save(String compName, String configName, String paramName, String paramValue) {
		final String METHOD = "save()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("compName="+compName+", configName="+configName+", paramName="+paramName);
		String key = compName + "#" + configName + "#" + paramName+"#";
		String retValue = null;
		retValue = save(key, paramValue);
		log.exiting();
		return retValue; //Stored securely in secure storage
	}

	/**
	 * Retrieve value stored in secure store with key comprising of
	 * <code>#securestorage#compName#configName#paramName#</code> if the paramValue
	 * starts with  #securestorage# which mean while saving we used securedstore
	 * Otherwise, while saving primitive encoding is used
	 * @param compName Component name
	 * @param configName configuration name
	 * @param paramName Parameter of a component
	 * @param paramValue ParamValue can be in two forms
	 * #securestorage#compName#configName#paramName# for values stored in secure store. This forms a key to look up
	 * encryptedstring If encoding is used, as the secure store functionality is failed
	 *
	 * @return INVALID_PASSWORD if unsuccessful in retrieving from securestore
	 * 		string, if successful in retrieving from securestore Or successful in decrypting the string
	 *
	 */
	public String retrieve(String compName, String configName, String paramName, String paramValue) {
		return retrieve(paramValue);
	}


	/**
	 * Remove key,value stored in secure store with key comprising of
	 * <code>#securestorage#compName#configName#paramName#</code> if the paramValue
	 * starts with  #securestorage# which mean while saving we used securedstore
	 * Otherwise, while saving primitive encoding is used. so no removing is required in this case
	 * @param compName Component name
	 * @param configName configuration name
	 * @param paramName Parameter of a component
	 * @param paramValue ParamValue can be in two forms
	 * #securestorage#compName#configName#paramName# for values stored in secure store. This forms a key to look up
	 * encryptedstring If encoding is used, as the secure store functionality is failed
	 *
	 * @return false if unsuccessful in removing from securestore
	 * 		true, if successful in removing from securestore, or if the entry to be removed is not securestore one
	 *
	 */
	public boolean remove(String compName, String configName, String paramName, String paramValue) {
		return remove(paramValue);
	}
	
	/**
	 * Retrieve value stored in secure store with key comprising of
	 * <code>#securestorage#compName#configName#paramName#</code> if the paramValue
	 * starts with  #securestorage# which mean while saving we used securedstore
	 * Otherwise, while saving primitive encoding is used
	 *
	 * @param paramValue ParamValue can be in two forms
	 * #securestorage#compName#configName#paramName# for values stored in secure store. This forms a key to look up
	 * encryptedstring If encoding is used, as the secure store functionality is failed
	 *
	 * @return string, if successful in retrieving from securestore Or successful in decrypting the string
	 * 			ERROR_RETRIEVING_SECURE_STORE, otherwise
	 *
	 */
	public static String retrieve(String paramValue) {
		final String METHOD = "retrieve()";
		log.entering(METHOD);
		String retValue = ERROR_RETRIEVING_SECURE_STORE;
		if(paramValue == null)
			return null;

		if(isSecureStorage(paramValue)){ // value is stored in secure storage
			if(paramValue.endsWith(MODE_ENCRYPT)) {
				try { //paramValue itself forms the key, no need to construct new key
					int start = Constants.SECURE_STORE_KEY_PREFIX.length();
					int end = paramValue.length() - MODE_ENCRYPT.length();
					retValue = (String) SecurityUtil.decryptFromBase64(paramValue.substring(start,end));
				} catch (Exception e) { //serious error - stored in secure store but not retrievable
					log.error(LogUtil.APPS_COMMON_SECURITY,"error.retrieve.securestore", new Object[] { e.toString()  },e);
				}
			}
			else {
				try { //paramValue itself forms the key, no need to construct new key
					retValue = (String) SecurityUtil.getFromSecureStorage(paramValue);
				} catch (Exception e) { //serious error - stored in secure store but not retrievable
					log.error(LogUtil.APPS_COMMON_SECURITY,"error.retrieve.securestore", new Object[] { e.toString()  },e);
				}
			}
		}
		else{ //use regular decryption
			retValue = PasswordEncrypt.decryptStringAsHex(paramValue,Constants.SEC);
		}
		log.exiting();
		return retValue;
	}

	

	/**
	 * Remove value stored in secure store with key comprising of
	 * <code>#securestorage#compName#configName#paramName#</code> if the paramValue
	 * starts with  #securestorage# which mean while saving we used securestore
	 * Otherwise, while saving primitive encoding is used. no remove required
	 * 
	 * @param paramValue ParamValue can be in two forms
	 * #securestorage#compName#configName#paramName# for values stored in secure store. This forms a key to look up
	 * encryptedstring If encoding is used, as the secure store functionality is failed
	 *
	 * @return true, if successful in deleting from securestore Or paramValue is not a securestore entry
	 * 			false, otherwise
	 *
	 */
	public static boolean remove(String paramValue) {
		final String METHOD = "remove()";
		log.entering(METHOD);
		boolean isSuccessful = false;
		if(paramValue == null) {
			log.exiting();
			return true;
		}

		if(isSecureStorage(paramValue)){ // value is stored in secure storage
			try { //paramValue itself forms the key, no need to construct new key
				SecurityUtil.removeFromSecureStorage(paramValue);
				isSuccessful = true;
			} catch (RemoteException e) { //serious error - stored in secure store but not retrievable
				log.error(LogUtil.APPS_COMMON_SECURITY,"error.delete.securestore",e);
			} catch (NamingException e) {//serious error - stored in secure store but not retrievable
				log.error(LogUtil.APPS_COMMON_SECURITY,"error.delete.securestore",e);
			} catch (ISASecurityException e) {
				log.error(LogUtil.APPS_COMMON_SECURITY,"error.delete.securestore",e.getBaseException());
			} catch (RuntimeException e){ //safeside
				log.error(LogUtil.APPS_COMMON_SECURITY,"error.delete.securestore",e);
			}
		}
		else{ //not a secure store entry do nothing
			isSuccessful = true;
		}
		log.exiting();
		return isSuccessful;
	}
	
	/**
	 * Save <code>paramValue</code> in secure store with key comprising of
	 * <code>key</code>. This method is useful for non XCM based parameters, which
	 * requires extra level of security. Like passwords stored elsewhere.	
	 * If unsuccessful in saving into securestore encrypt the value and return
	 * encrypted string (provide minimal security)
	 * @param key Key name. Same key should be used while retrieving later
	 * @return paramValue used to store the value in secure store, if successful saving in securestore
	 * 	If failed to store in secure store, return encrypted  <code>paramValue</code>
	 *
	 */	
	public static String save(String key, String paramValue){
		final String METHOD = "save()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("key="+key);
		String retValue = null;
		
		try {
			retValue = SecurityUtil.encryptToBase64(paramValue);
			retValue = Constants.SECURE_STORE_KEY_PREFIX + retValue + MODE_ENCRYPT;
			log.debug("Secure storage encrypted [key]='"+ key + "'");			
		} catch (Exception e) {
			if(log.isDebugEnabled())
				log.debug("Secure store encryption failed for [key]='"+ key + "'");
			log.warn("securestore.error.failure",new String[]{"encrypt",key},e);
		}		
				
		if(retValue == null) {
			try {
				SecurityUtil.storeInSecureStorage(key,paramValue);
				if(log.isDebugEnabled())
					log.debug("Secure storage store failed for [key]='"+ key + "'");
				retValue = Constants.SECURE_STORE_KEY_PREFIX + key;				
			} catch (Exception e) {
				log.warn("securestore.error.failure",new String[]{"store",key},e);				
			}
		}

		if(retValue == null){ //if successfully stored in secure storage
			if(log.isDebugEnabled())
				log.debug("Using default hex encryption for [key]='"+ key);
			retValue = PasswordEncrypt.encryptStringAsHex(paramValue, Constants.SEC);
		}

				
		log.exiting();		
		return retValue;
	}
	
	/**
	 * Checks if the value starts with a identifier used to prefix 
	 * data stored in secure storage
	 * @param value
	 * @return true if stored in secure storage, otherwise false
	 */
	public static boolean isSecureStorage(String value) {
		if (value.startsWith(Constants.SECURE_STORE_KEY_PREFIX)) {
			return true;		
		} else
			return false;
	}
}
