/************************************************************************

	Copyright (c) 1999 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.isa.core.security;

// java imports
import java.io.ByteArrayOutputStream;

import com.sap.isa.core.util.HexUtil;

/**
 * This class provides a simple en-/decryption mechanism for strings
 */

public class PasswordEncrypt
{
	/**
	 * Encrypts a string (with the help of a key).
	 * @param in	The string to encrypt
	 * @param key	A key used to encrypt
	 */
	public static String encryptString(String in, String key)
	{
        	if (key == null || key.length() < 1)
			throw new RuntimeException("Invalid key. The key must have at least one character.");

		if (in == null || in.length() < 1)
			//throw new SXERuntimeException("Invalid string. The string must have at least one character.");
			return in; //don't encrypt

		byte[] baKey = key.getBytes();
		byte[] baIn = in.getBytes();
		byte[] baEncrypted = new byte[in.length() * 2];

		for (int i=0; i<baIn.length; i++)
		{
			byte bIn = (byte)(baIn[i] ^ baKey[i % baKey.length]);

			do
			{
				byte rndm = (byte)( 1 + Math.random() * 6);

				baEncrypted[2*i] = (byte)( (bIn & 0xF0) | rndm );
				baEncrypted[2*i+1] = (byte)( ( (bIn & 0x0F) | (rndm << 4) ) );
			}
			while (baEncrypted[2*i] == 0x00 | baEncrypted[2*i+1] == 0x00);
		}
                return new String(baEncrypted);
	}

	/**
	 * Encrypts a string (with the help of a key).
	 * @param in	The string to encrypt
	 * @param key	A key used to encrypt
	 */
	public static String encryptStringAsHex(String in, String key) {
          return HexUtil.convert(encryptString(in, key).getBytes());
        }



	/**
	 * Decrypts a string.
	 * @param in	The string to encrypt
	 * @param key	A key used to encrypt. Must be the same used to encrypt.
	 */
	public static String decryptString(String in, String key)
	{
		if (key == null || key.length() < 1)
			throw new RuntimeException("Invalid key. The key must have at least one character.");

		if (in == null || in.length() < 1)
			//throw new SXERuntimeException("Invalid string. The string must have at least one character.");
			return in; //don't decrypt

		byte[] baKey = key.getBytes();
		byte[] baIn = in.getBytes();
                //byte[] baIn = HexUtil.convert(in);
		byte[] baDecrypted = new byte[in.length() / 2];

		for (int i=0; i<baDecrypted.length; i++)
		{
			byte blOut = (byte)(baIn[2*i] & 0xF0);
			byte brOut = (byte)(baIn[2*i+1] & 0x0F);

			baDecrypted[i] = (byte)(blOut | brOut);
			baDecrypted[i] = (byte)(baDecrypted[i] ^ baKey[i % baKey.length]);
		}

                // change into something which can by copy/pasted
		return new String(baDecrypted);
	}

	/**
	 * Decrypts a string. Only A-F and 0-9 characters are used.
	 * @param in	The string to encrypt
	 * @param key	A key used to encrypt. Must be the same used to encrypt.
	 */
	public static String decryptStringAsHex(String in, String key) {
            String asEncrypted = new String(HexUtil.convert(in));
            return decryptString(asEncrypted, key);
        }

	/**
	 * Decrypts a string. Only A-F and 0-9 characters are used.
	 * @param in The string to encrypt
	 * @return the decrypted string
	 */
	public static String decryptStringAsHex(String in) {
			String asEncrypted = new String(HexUtil.convert(in));
			return decryptString(asEncrypted, Constants.SEC);
		}

	

	/**
	 * For testing purpose.
	 */
	public static void main(String[] args)
	{
          if (args.length == 0) {
            System.out.println("Internet Sales 3.0 utility for creating encrypted passwords\n");
            System.out.println("Usage: PasswordEncrypt <password>\n");
          } else {
            System.out.println("Encrypted password:\n");
            System.out.println(encryptStringAsHex(args[0], Constants.SEC) + "\n");
          }
          System.out.println("copy the encrypted password and paste it in the");
          System.out.println("passwd.jco.sp.eai.core.isa.sap.com context-parameter");
          System.out.println("when deploying Internet Sales 3.0\n");


          /*  String internalKey = "people";
              String password = "secret";

              String encrypted = encryptStringAsHex(password, internalKey);
              System.out.println("encrypted: " + encrypted);
              String decrypted = decryptStringAsHex(encrypted, internalKey);
              System.out.println("decrypted: " + decrypted);
          */
	}

    /**
     * [Private] Convert the specified value (0 .. 15) to the corresponding
     * hexadecimal digit.
     *
     * @param value Value to be converted
     */
    private static char convertDigit(int value) {

	value &= 0x0f;
	if (value >= 10)
	    return ((char) (value - 10 + 'a'));
	else
	    return ((char) (value + '0'));
    }

    /**
     * Convert a String of hexadecimal digits into the corresponding
     * byte array by encoding each two hexadecimal digits as a byte.
     *
     * @param digits Hexadecimal digits representation
     *
     * @exception IllegalArgumentException if an invalid hexadecimal digit
     *  is found, or the input string contains an odd number of hexadecimal
     *  digits
     */
    public static byte[] convert(String digits) {

	ByteArrayOutputStream baos = new ByteArrayOutputStream();
	for (int i = 0; i < digits.length(); i += 2) {
	    char c1 = digits.charAt(i);
	    if ((i+1) >= digits.length())
		throw new IllegalArgumentException("hexUtil.odd");
	    char c2 = digits.charAt(i + 1);
	    byte b = 0;
	    if ((c1 >= '0') && (c1 <= '9'))
		b += ((c1 - '0') * 16);
	    else if ((c1 >= 'a') && (c1 <= 'f'))
		b += ((c1 - 'a' + 10) * 16);
	    else if ((c1 >= 'A') && (c1 <= 'F'))
		b += ((c1 - 'A' + 10) * 16);
	    else
		throw new IllegalArgumentException("hexUtil.bad");
	    if ((c2 >= '0') && (c2 <= '9'))
		b += (c2 - '0');
	    else if ((c2 >= 'a') && (c2 <= 'f'))
		b += (c2 - 'a' + 10);
	    else if ((c2 >= 'A') && (c2 <= 'F'))
		b += (c2 - 'A' + 10);
	    else
		throw new IllegalArgumentException("hexUtil.bad");
	    baos.write(b);
	}
	return (baos.toByteArray());
    }

	/**
	 * Checks if the value starts with a identifier used to prefix 
	 * data stored in secure storage
	 * @param value
	 * @return true if stored in secure storage, otherwise false
	 */
	public static boolean isSecureStorage(String value) {
		if (value.startsWith(Constants.SECURE_STORE_KEY_PREFIX)) {
			return true;		
		} else
			return false;
	}

}
