/*****************************************************************************
  Copyright (c) 2003, SAP Labs, All rights reserved.
  Author:       Pavan Bayyapu
  Created:      Sept 16, 2003

  Revision: #
  Date:
*****************************************************************************/
package com.sap.isa.core.security;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.HashMap;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.security.core.server.destinations.api.Destination;
import com.sap.security.core.server.destinations.api.DestinationException;
import com.sap.security.core.server.destinations.api.DestinationService;
import com.sap.security.core.server.destinations.api.HTTPDestination;
import com.sap.security.core.server.securestorage.SecureStorageObject;
import com.sap.security.core.server.securestorage.SecureStorageRuntimeInterface;
import com.sap.security.core.server.securestorage.exception.ObjectDeletionException;
import com.sap.security.core.server.securestorage.exception.ObjectRetrievalException;
import com.sap.security.core.server.securestorage.exception.ObjectStorageException;
import com.sap.security.core.server.securestorage.exception.StorageLocationOpenException;
import com.sap.security.core.server.securestorage.remote.RemoteSecureStorageClientContextInterface;


/**
 * Contains security utilities like secure storage and destination service
 */
public class SecurityUtil {
	/**
	 * Reference to destination service
	 */
	private static DestinationService destService = null;
	
	/**
	 * Reference to secure storage service context
	 */
	private static RemoteSecureStorageClientContextInterface secureStorageContext = null;
	private static final IsaLocation log = IsaLocation.getInstance(SecurityUtil.class.getName());

	/**
	 * Return destination with the name specified
	 * @param name Name of the destination. This destination should already be created on the J2EE
	 * @return destination, if exists 
	 * @throws NamingException 
	 * @throws DestinationException
	 * @throws RemoteException
	 * @see com.sap.security.core.server.destinations.api.DestinationService
	 */
	public static Destination getDestination(String name)
		throws NamingException, DestinationException, RemoteException {
		final String METHOD = "getDestination()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("name="+name);
		HTTPDestination destination = null;
		DestinationService dstService = getDestinationServiceContext();

		destination = (HTTPDestination) dstService.getDestination("HTTP", name);
		log.exiting();
		return destination;
	}

	/**
	 * Internal utility method to get the destination service context
	 * for reusing it in future
	 * 
	 * @return Destination service context
	 * @throws NamingException
	 */
	private static DestinationService getDestinationServiceContext()
		throws NamingException {
		if (destService != null)
			return destService;
		Context ctx = new InitialContext();
		DestinationService destService =
			(DestinationService) ctx.lookup(DestinationService.JNDI_KEY);
		if (destService == null)
			throw new NamingException("Destination Service not available");
		else {
			setDestinationService(destService); //saveit for future use
			return destService;
		}

	}

	/**
	 * If the destination with name <b>destName</b> already exist:
	 * Overwrites url of destination with name <strong> destName </strong> 
	 * with URL Name <b>url</b> <br>
	 * If the destination name doesn't exist: create new destination with 
	 * name <b>destName</b> and the URL <b>url</b>
	 * @param name - Name of the destination
	 * @param url - URL of the destination
	 * @throws DestinationException
	 * @throws RemoteException
	 * @throws NamingException
	 */
	public static void synchDestination(String destName, String url)
		throws DestinationException, RemoteException, NamingException {
		final String METHOD = "synchDestination()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("destName="+destName+", url="+url);
		HTTPDestination destination = null;
		DestinationService destService = getDestinationServiceContext();
		destination =
			(HTTPDestination) destService.getDestination("HTTP", destName);
		if (destination == null)
			destination =
				(HTTPDestination) destService.createDestination("HTTP");
		destination.setUrl(url);
		destination.setName(destName);
		destService.storeDestination("HTTP", destination);
		log.exiting();
		return;
	}

	/**
	 * Store the <b>(param,value)</b> in the secure storage
	 * 
	 * @param param - key under which the value should be stored
	 * @param value serializable object
	 * @throws ObjectStorageException
	 * @throws RemoteException
	 * @throws NamingException
	 */
	public synchronized static void storeInSecureStorage(String param, Object value)
		throws ISASecurityException, RemoteException, NamingException {
		final String METHOD = "storeInSecureStorage()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("param="+param);
		RemoteSecureStorageClientContextInterface secureStorageContext =
			getSecureStorageContext();
		Object oldValue= null;
		
		try {
			oldValue = secureStorageContext.retrieveObject(param);
		} catch (ObjectRetrievalException e) {
			log.debug(e.getMessage());
			//ignore
		} catch (RemoteException e) {
			log.debug(e.getMessage());
			//ignore
		}
		if(oldValue != null){ //same key already exists
			try {
				secureStorageContext.deleteObject(param);
			} catch (ObjectDeletionException e1) {
				log.debug(e1.getMessage());
				// ignore
			} catch (RemoteException e1) {
				log.debug(e1.getMessage());
				//ignore
			}
		}
		try {
			secureStorageContext.storeObject((Serializable) value, param);	
		} catch (ObjectStorageException osex) {
			throw new ISASecurityException(osex);
		} finally {
			log.exiting();
		}
		
	}
	
	/**
	 * Encrypts (param,value) pair using Secure Storage encryption
	 * @param param
	 * @param value
	 * @return
	 */
	public static String encryptToBase64(Serializable obj) 
		throws ISASecurityException, RemoteException, NamingException {
			final String METHOD = "encrypteToBase64()";
			log.entering(METHOD);
			if(obj == null) {
				log.exiting();
				throw new ISASecurityException("obj is null");
			}
			
			RemoteSecureStorageClientContextInterface secureStorageContext =
				getSecureStorageContext();
			try {
				// encrypt
				SecureStorageObject secObj = secureStorageContext.encrypt(obj);
				byte[] serializedObj = secureStorageContext.base64Encoding(secObj);
				String objAsString = new String(serializedObj,"us-ascii");
				return objAsString;	
			} catch(Exception orex) {
				throw new ISASecurityException(orex);
			} finally {
				log.exiting();				
			}
	}

	private static void test1(RemoteSecureStorageClientContextInterface ctxt) {
		try {
			SecureStorageObject secObj = secureStorageContext.encrypt("test-string");
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(bos);
			oos.writeObject(secObj);
			byte[] bytes = bos.toByteArray();
			

			ByteArrayInputStream bin = new ByteArrayInputStream(bytes);
			ObjectInputStream ois = new ObjectInputStream(bin);
			SecureStorageObject obj = (SecureStorageObject)ois.readObject();			
			
			byte[] serializedObj = bos.toByteArray();
			
			byte[] base64Bytes = secureStorageContext.base64Encoding(serializedObj);
			String objAsString = new String(base64Bytes,"us-ASCII");
			byte[] asciiBytes = objAsString.getBytes("us-ascii");
			byte[] decBytes = secureStorageContext.base64Decoding(base64Bytes);
			SecureStorageObject secObj1 = new SecureStorageObject(serializedObj);
			Object test = secureStorageContext.decrypt(obj);			
		}
		catch(Exception ex) {
			log.debug(ex.getMessage());
		}
	}
	
	
	public static Serializable decryptFromBase64(String encryptedStr) 
		throws ISASecurityException, RemoteException, NamingException {
			final String METHOD = "decryptFromBase64()";
			log.entering(METHOD);
			if(encryptedStr == null || encryptedStr.length() == 0) {
				log.exiting();
				throw new ISASecurityException(encryptedStr + " is invalid");
			}
			
			RemoteSecureStorageClientContextInterface secureStorageContext =
				getSecureStorageContext();

			try {
				byte[] serializedObj = secureStorageContext.base64Decoding(encryptedStr.getBytes("us-ascii"));
				ByteArrayInputStream bin = new ByteArrayInputStream(serializedObj);
				ObjectInputStream ois = new ObjectInputStream(bin);
				SecureStorageObject secObj = (SecureStorageObject)ois.readObject();
				Serializable obj = (Serializable)secureStorageContext.decrypt(secObj);
				return obj;
			} catch (Exception e) {
				throw new ISASecurityException(e);	
			} finally {
				log.exiting();			
			}
	}
	
	/**
	 * Removes the <b>(param)</b> from the secure storage
	 * 
	 * @param param - key to be removed from secure storage
	 * @throws ObjectDeletionException
	 * @throws RemoteException
	 * @throws NamingException
	 */
	public synchronized static void removeFromSecureStorage(String param) 
			throws RemoteException, NamingException, ISASecurityException
		{
		final String METHOD = "removeFromSecureStorage()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("param="+param);
		RemoteSecureStorageClientContextInterface secureStorageContext =
			getSecureStorageContext();
		try {
			secureStorageContext.deleteObject(param);	
		} catch (ObjectDeletionException odelex) {
			throw new ISASecurityException(odelex);
		} finally {
			log.exiting();
		}
		
	}
	
	
	/**
	 * Retreive <b>value</b> associated with the key <b>param</b>
	 * @param param Key
	 * @return value associated with the key
	 * @throws ObjectRetrievalException
	 * @throws RemoteException
	 * @throws NamingException
	 */
	public synchronized static Object getFromSecureStorage(String param)
		throws ISASecurityException, RemoteException, NamingException {
		final String METHOD = "getFromSecureStorage()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("param="+param);
		RemoteSecureStorageClientContextInterface secureStorageContext =
			getSecureStorageContext();
		//boolean encryptFlag = secureStorageContext.isSecure("pass");
		//System.out.println(encryptFlag? "isSecured":"isNotSecured");
	
		Object retVal = null;
		try {
			retVal = secureStorageContext.retrieveObject(param); 
		} catch (ObjectRetrievalException orex) {
			throw new ISASecurityException(orex);
		} finally {
			log.exiting();
		}
		return retVal;
	} 
	

	/**
	 * Utility method to get the secure storage context for using it later
	 * @return
	 * @throws NamingException
	 * @throws RemoteException
	 */
	private static RemoteSecureStorageClientContextInterface getSecureStorageContext()
		throws NamingException, RemoteException {
		if (secureStorageContext != null)
			return secureStorageContext;
		Context ctx = new InitialContext();

		Object obj = (Object) ctx.lookup("tc~sec~securestorage~service");
		if (obj == null) {
			System.out.println("secure storage service not started<br>");
		} else {
			//Cast
			SecureStorageRuntimeInterface secStore =
				(SecureStorageRuntimeInterface) obj;
			//Continue with implementation
			RemoteSecureStorageClientContextInterface secStorageContext =
				secStore.getSecureStorageClientContext();
			setSecureStorageContext(secStorageContext);
			return secStorageContext;
		}
		return null;

	}

	/**
	 * Uitility function to save the secure storage context for future calls
	 * @param secStorageContext
	 */
	private static void setSecureStorageContext(RemoteSecureStorageClientContextInterface secStorageContext) {
		secureStorageContext = secStorageContext;
	}

	/**
	 * Sets the destination service 
	 * @param service
	 */
	private static void setDestinationService(DestinationService service) {
		destService = service;
	}
	
	/**
	 * Retruns keys stored in secure storage under web application context in String array form
	 * @return String array containing the key list
	 * @throws NamingException 
	 * @throws StorageLocationOpenException
	 * @throws ObjectRetrievalException
	 * @throws RemoteException
	 */
	public synchronized static String [] getSecureStoreEntries() throws NamingException, StorageLocationOpenException, ObjectRetrievalException, RemoteException{
		RemoteSecureStorageClientContextInterface secureStorageContext =
					getSecureStorageContext();
		String [] keys = secureStorageContext.getObjectIDs();
		
		return keys;
	}
	
	/**
	 * Returns key value pairs stored in secure storage under web application context in HashMap form
	 * @return Hashmap containing keys and values stored in secure store
	 * @throws StorageLocationOpenException
	 * @throws ObjectRetrievalException
	 * @throws RemoteException
	 * @throws NamingException
	 */
	public synchronized static HashMap getSecureStoreParamValues() 
		throws ISASecurityException, RemoteException, NamingException{
		final String METHOD = "getSecureStoreParamValues()";
		log.entering(METHOD);
			
		HashMap map = null;
		try {
			String []  list = getSecureStoreEntries();
			if( (list!= null) && (list.length > 0) ) {
				map = new HashMap(list.length);
				for(int i=0; i< list.length ; i++){
					map.put(list[i], getFromSecureStorage(list[i]) );	
				}
			}
		} catch (StorageLocationOpenException ex) {
			throw new ISASecurityException(ex);
		} catch (ObjectRetrievalException ex) {
			throw new ISASecurityException(ex);
		} finally {
			log.exiting();
		}

		return map;
	}
}
