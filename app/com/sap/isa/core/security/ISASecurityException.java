package com.sap.isa.core.security;

/**
 * J2EE Engine independent Security Exception
 */
public class ISASecurityException extends Exception {

	private Exception mBaseEx;
	private String mMsg = "";

	/** 
	 * Constructor
	 * @param msg Message describing the exception
	 */
	public ISASecurityException(String msg) {
		super(msg);
		mMsg = msg;
	}

	/** 
	 * Constructor
	 * @param msg Message describing the exception
	 */
	public ISASecurityException(Exception baseException) {
		super(baseException.toString());
		mMsg = baseException.toString();
		mBaseEx = baseException;
	}


	/**
	 * Returns the base exception or <code>null</code> if there
	 * is no root exception
	 * @return the root exception
	 */
	public Exception getBaseException() {
		return mBaseEx;
	}

	/**
	 * Returns a string representation of this exception
	 * @return a string representation of this exception
	 */
	public String toString() {
		if (mBaseEx == null)
			return mMsg;
		else
			return mMsg + "\n" + mBaseEx.toString();
	}

}