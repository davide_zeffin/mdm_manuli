/**
 * Wrapper for ActionInterceptor
 * For Implemeting the
 * Aimed at intercepting the action servlet's execution
 */
package com.sap.isa.core;


public class ActionInterceptorWrapper {
   /**
    * A reference to a possibly alternate factory.
    */
   static private ActionInterceptor _factory = null;

   /**
    * Instance of the ActionInterceptor
    */
   static private ActionInterceptor _instance = null;

   /**
    * This is the default factory method.
    * It is called to create a new ActionInterceptor when
    * a new instance is needed and _factory is null.
    */
   static private ActionInterceptor makeInstance(String factoryClassName) throws Exception{
      if(null == factoryClassName)
        return null;
      _instance = (ActionInterceptor) Class.forName(factoryClassName).newInstance();
      return _instance;
   }

   /**
    * This is the accessor for the ActionInterceptor.
    */
   static public synchronized ActionInterceptor instance(String factoryClassName) throws Exception{
      if(null == _instance) {
         _instance = (null == _factory) ? makeInstance(factoryClassName) : _factory.makeInstance();
      }
      return _instance;
   }

   /**
    * This is the accessor for the ActionInterceptor.
    */
   static public synchronized ActionInterceptor instance() throws Exception {
      return instance(null);
   }

   /**
    * Sets the factory method used to create new instances.
    * You can set the factory method to null to use the default method.
    * @param factory The ActionInterceptor factory
    */
   static public synchronized void setFactory(ActionInterceptor factory) {
      _factory = factory;
   }

   /**
    * Sets the current ActionInterceptor instance.
    * @param instance The ActionInterceptor instance to use.
    */
   static public synchronized void setInstance(ActionInterceptor instance) {
      _instance = instance;
   }
}
