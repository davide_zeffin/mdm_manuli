/**
 * Class:        ExtendedTechKey Copyright (c) 2006, SAP AG, Germany, All rights reserved. Created:      22.07.2009
 * Class:        Helper to get Timestamp for cache validation after delta replication
 */
package com.sap.isa.core;

import com.sap.isa.core.util.table.RowKey;

public class ExtendedTechKey implements RowKey {

      private TechKey techKey = null;
      private String timeStamp = null;
      
      public ExtendedTechKey(TechKey techKey, String timeStamp) {
            this.techKey = techKey;
            this.timeStamp = timeStamp;
      }
      
      public ExtendedTechKey(TechKey techKey) {
            this(techKey, null);
      }
      
      public ExtendedTechKey(String timeStamp) {
            this(null, timeStamp);
      }

      public TechKey getTechKey() {
            return techKey;
      }

      public void setTechKey(TechKey techKey) {
            this.techKey = techKey;
      }

      public String getTimeStamp() {
            return timeStamp;
      }

      public void setTimeStamp(String timeStamp) {
            this.timeStamp = timeStamp;
      }
      
}
