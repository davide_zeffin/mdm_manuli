/*
 * Created on Jul 22, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.core.db;

import java.sql.Connection;

/**
 * @author I802891
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public interface DBTransaction {
	boolean isActive();
	void begin();
	void commit();
	void rollback();
	/**
	 * Supported only for JDO
	 * @param val
	 */	
	void setOptimistic(boolean val);
	boolean isOptimistic();
	void setSychronization(javax.transaction.Synchronization sync);
	/**
	 * <p>
	 * This is the JDBC Connection to allow external users to do JDBC operations within the
	 * scope pf same transaction. Transction must be active to retreive this connection  
	 * It allows only limited usage. 
	 * All set Operations are Illegal
	 * </p>
	 * @return
	 */
	Connection getClientConnection();
}
