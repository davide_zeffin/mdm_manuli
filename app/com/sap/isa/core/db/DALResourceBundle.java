/*
 * Created on Aug 30, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.core.db;

import com.sap.localization.*;

/**
 * @author I802891
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class DALResourceBundle extends ResourceAccessor {
	
	public static final String ERROR_RESOURCE_BUNDLE_NAME = DALResourceBundle.class.getName();
	
	private static DALResourceBundle singleton = null;
	    
	private DALResourceBundle(String resourcebundle) {
		super(resourcebundle);
		// reserve 
	}

	public static synchronized DALResourceBundle getInstance() {
		if(singleton == null) {
			singleton = new DALResourceBundle(ERROR_RESOURCE_BUNDLE_NAME);
		}
		return singleton;
	}
	
	/**
	 * Factory for creating Internationalized Error Message
	 * @return
	 */
	public LocalizableText newLocalizableText() {
		LocalizableTextFormatter msg = new LocalizableTextFormatter();
		msg.setResourceAccessor(this);
		return msg;
	}
	
	/**
	 * Factory for creating Internationalized Error Message
	 * @return
	 */
	public LocalizableText newLocalizableText(String key) {
		return new LocalizableTextFormatter(this,key);
	}
		
	/**
	 * Factory for creating Internationalized Error Message
	 * @return
	 */
	public LocalizableText newLocalizableText(String key, Object[] args) {
		return new LocalizableTextFormatter(this,key,args);
	}
	
	public LocalizableText newI18nMessage(String key, Object[] args, String arg) {
		return new LocalizableTextFormatter(this,key,args,arg);
	}
}
