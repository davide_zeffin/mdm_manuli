/*
 * Created on Aug 12, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.core.db;

import com.sap.localization.LocalizableText;
import com.sap.tc.logging.Location;

/**
 * @author I802891
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ResourceAccessFailureException extends DBException {



	private static final String ERROR_PREFIX = ResourceAccessFailureException.class.getName() + "_";
	
	public static final String FAILED_TO_OBTAIN_DB_CONNECTION = ERROR_PREFIX + 1;
	public static final String FAILED_START_DB_TRANSACTION = ERROR_PREFIX + 2;
	public static final String FAILED_START_JTA_TRANSACTION = ERROR_PREFIX + 3;	
	public static final String ROLLBACK_FAILED = ERROR_PREFIX + 4;	
	public static final String COMMIT_FAILED = ERROR_PREFIX + 5;
	public static final String TX_ROLLBACK_ONLY = ERROR_PREFIX + 6;
	public static final String READ_FAILED = ERROR_PREFIX + 7;	
	public static final String RW_FAILED = ERROR_PREFIX + 8;	
	
	/**
	 * @param arg0
	 */
	public ResourceAccessFailureException(Location arg0) {
		super(arg0);
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public ResourceAccessFailureException(Location arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public ResourceAccessFailureException(
		Location arg0,
		LocalizableText arg1) {
		super(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 */
	public ResourceAccessFailureException(
		Location arg0,
		LocalizableText arg1,
		Throwable arg2) {
		super(arg0, arg1, arg2);
	}

	/**
	 * @param arg0
	 * @param arg2
	 * @param arg3
	 */
	public ResourceAccessFailureException(
		Location arg0,
		String arg2,
		Object[] arg3) {
		super(arg0, arg2, arg3);
	}

	/**
	 * @param arg0
	 * @param arg2
	 * @param arg3
	 * @param arg4
	 */
	public ResourceAccessFailureException(
		Location arg0,
		String arg2,
		Object[] arg3,
		Throwable arg4) {
		super(arg0, arg2, arg3, arg4);
	}
}
