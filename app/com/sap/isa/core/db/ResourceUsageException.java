/*
 * Created on Aug 12, 2004
 *
 */
package com.sap.isa.core.db;

import com.sap.localization.LocalizableText;
import com.sap.tc.logging.Location;

/**
 * Root for exceptions thrown when we use a data access resource incorrectly.
 * Thrown for example on specifying bad SQL when using a RDBMS.
 * @author I802891
 */
public class ResourceUsageException extends DBException {
	
	private static final String ERROR_PREFIX = ResourceUsageException.class.getName() + "_";
	
	public static final String PREPARE_STMT_FAILED = ERROR_PREFIX + 1;
	public static final String RESOURCE_CREATE_FAILED = ERROR_PREFIX + 2;
		
	/**
	 * @param arg0
	 */
	public ResourceUsageException(Location arg0) {
		super(arg0);
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public ResourceUsageException(Location arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public ResourceUsageException(Location arg0, LocalizableText arg1) {
		super(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 */
	public ResourceUsageException(
		Location arg0,
		LocalizableText arg1,
		Throwable arg2) {
		super(arg0, arg1, arg2);
	}

	/**
	 * @param arg0
	 * @param arg2
	 * @param arg3
	 */
	public ResourceUsageException(Location arg0, String arg2, Object[] arg3) {
		super(arg0, arg2, arg3);
	}

	/**
	 * @param arg0
	 * @param arg2
	 * @param arg3
	 * @param arg4
	 */
	public ResourceUsageException(
		Location arg0,
		String arg2,
		Object[] arg3,
		Throwable arg4) {
		super(arg0, arg2, arg3, arg4);
	}
}
