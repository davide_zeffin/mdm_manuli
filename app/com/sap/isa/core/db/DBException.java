/*
 * Created on Jul 22, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.core.db;

import com.sap.exception.BaseRuntimeException;
import com.sap.localization.LocalizableText;
import com.sap.tc.logging.Location;

/**
 * Base Exception of all exceptions thrown by Database Access layer
 * @author I802891
 *
 */
public class DBException extends BaseRuntimeException {
	
	private static final String ERROR_PREFIX = DBException.class.getName() + "_";
	private static DALResourceBundle bundle = DALResourceBundle.getInstance();
	
	public static final String UNKNOWN_ERROR = ERROR_PREFIX + 1;
	public static final String INTERNAL_ERROR = ERROR_PREFIX + 2;	
	public static final String UNSUPPORTED_OPERATION = ERROR_PREFIX + 3;
	
	/**
	 * @param arg0
	 */
	public DBException(Location arg0) {
		super(arg0);
	}


	/**
	 * @param arg0
	 * @param arg1
	 */
	public DBException(Location arg0, Throwable arg1) {
		super(arg0, arg1);
	}


	/**
	 * @param arg0
	 * @param arg1
	 */
	public DBException(Location arg0, LocalizableText arg1) {
		super(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 */
	public DBException(
		Location arg0,
		LocalizableText arg1,
		Throwable arg2) {
		super(arg0, arg1, arg2);
	}



	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @param arg3
	 */
	public DBException(
		Location arg0,
		String arg2,
		Object[] arg3) {
		super(arg0, bundle, arg2, arg3);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @param arg3
	 * @param arg4
	 */
	public DBException(
		Location arg0,
		String arg2,
		Object[] arg3,
		Throwable arg4) {
		super(arg0, bundle, arg2, arg3, arg4);
	}
}
