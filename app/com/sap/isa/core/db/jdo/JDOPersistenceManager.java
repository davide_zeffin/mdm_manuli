/*
 * Created on Aug 19, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.core.db.jdo;

import javax.jdo.PersistenceManager;

import com.sap.isa.core.db.DBPersistenceManager;

/**
 * 
 * Provides 2 flavours of usage
 * @see javax.jdo.PersistenceManager
 * @see DBPersistenceManager
 * @author I802891
 *
 */
public interface JDOPersistenceManager extends PersistenceManager, DBPersistenceManager  {

}
