/*
 * Created on Jul 22, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.core.db.jdo;

import java.util.Collection;
import java.util.Properties;

import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;

import com.sap.isa.core.db.DBPersistenceManager;
import com.sap.isa.core.db.DBPersistenceManagerFactory;


/**
 * @author I802891
 *
 */
public final class JDOPersistenceManagerFactory implements DBPersistenceManagerFactory {
	private PersistenceManagerFactory jdoPMF = null;
	
	public JDOPersistenceManagerFactory(PersistenceManagerFactory jdoPMF) {
		this.jdoPMF = jdoPMF;
	}
	
	public DBPersistenceManager getDBPersistenceManager() {
		return null;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		return jdoPMF.equals(obj);
	}

	/**
	 * @return
	 */
	public String getConnectionDriverName() {
		return jdoPMF.getConnectionDriverName();
	}

	/**
	 * @return
	 */
	public Object getConnectionFactory() {
		return jdoPMF.getConnectionFactory();
	}

	/**
	 * @return
	 */
	public Object getConnectionFactory2() {
		return jdoPMF.getConnectionFactory2();
	}

	/**
	 * @return
	 */
	public String getConnectionFactory2Name() {
		return jdoPMF.getConnectionFactory2Name();
	}

	/**
	 * @return
	 */
	public String getConnectionFactoryName() {
		return jdoPMF.getConnectionFactoryName();
	}

	/**
	 * @return
	 */
	public String getConnectionURL() {
		return jdoPMF.getConnectionURL();
	}

	/**
	 * @return
	 */
	public String getConnectionUserName() {
		return jdoPMF.getConnectionUserName();
	}

	/**
	 * @return
	 */
	public boolean getIgnoreCache() {
		return jdoPMF.getIgnoreCache();
	}

	/**
	 * @return
	 */
	public boolean getMultithreaded() {
		return jdoPMF.getMultithreaded();
	}

	/**
	 * @return
	 */
	public boolean getNontransactionalRead() {
		return jdoPMF.getNontransactionalRead();
	}

	/**
	 * @return
	 */
	public boolean getNontransactionalWrite() {
		return jdoPMF.getNontransactionalWrite();
	}

	/**
	 * @return
	 */
	public boolean getOptimistic() {
		return jdoPMF.getOptimistic();
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @return
	 */
	public PersistenceManager getPersistenceManager(String arg0, String arg1) {
		return jdoPMF.getPersistenceManager(arg0, arg1);
	}

	/**
	 * @return
	 */
	public Properties getProperties() {
		return jdoPMF.getProperties();
	}

	/**
	 * @return
	 */
	public boolean getRestoreValues() {
		return jdoPMF.getRestoreValues();
	}

	/**
	 * @return
	 */
	public boolean getRetainValues() {
		return jdoPMF.getRetainValues();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode() {
		return jdoPMF.hashCode();
	}

	/**
	 * @param arg0
	 */
	public void setConnectionDriverName(String arg0) {
		jdoPMF.setConnectionDriverName(arg0);
	}

	/**
	 * @param arg0
	 */
	public void setConnectionFactory(Object arg0) {
		jdoPMF.setConnectionFactory(arg0);
	}

	/**
	 * @param arg0
	 */
	public void setConnectionFactory2(Object arg0) {
		jdoPMF.setConnectionFactory2(arg0);
	}

	/**
	 * @param arg0
	 */
	public void setConnectionFactory2Name(String arg0) {
		jdoPMF.setConnectionFactory2Name(arg0);
	}

	/**
	 * @param arg0
	 */
	public void setConnectionFactoryName(String arg0) {
		jdoPMF.setConnectionFactoryName(arg0);
	}

	/**
	 * @param arg0
	 */
	public void setConnectionPassword(String arg0) {
		jdoPMF.setConnectionPassword(arg0);
	}

	/**
	 * @param arg0
	 */
	public void setConnectionURL(String arg0) {
		jdoPMF.setConnectionURL(arg0);
	}

	/**
	 * @param arg0
	 */
	public void setConnectionUserName(String arg0) {
		jdoPMF.setConnectionUserName(arg0);
	}

	/**
	 * @param arg0
	 */
	public void setIgnoreCache(boolean arg0) {
		jdoPMF.setIgnoreCache(arg0);
	}

	/**
	 * @param arg0
	 */
	public void setMultithreaded(boolean arg0) {
		jdoPMF.setMultithreaded(arg0);
	}

	/**
	 * @param arg0
	 */
	public void setNontransactionalRead(boolean arg0) {
		jdoPMF.setNontransactionalRead(arg0);
	}

	/**
	 * @param arg0
	 */
	public void setNontransactionalWrite(boolean arg0) {
		jdoPMF.setNontransactionalWrite(arg0);
	}

	/**
	 * @param arg0
	 */
	public void setOptimistic(boolean arg0) {
		jdoPMF.setOptimistic(arg0);
	}

	/**
	 * @param arg0
	 */
	public void setRestoreValues(boolean arg0) {
		jdoPMF.setRestoreValues(arg0);
	}

	/**
	 * @param arg0
	 */
	public void setRetainValues(boolean arg0) {
		jdoPMF.setRetainValues(arg0);
	}

	/**
	 * @return
	 */
	public Collection supportedOptions() {
		return jdoPMF.supportedOptions();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return jdoPMF.toString();
	}
}
