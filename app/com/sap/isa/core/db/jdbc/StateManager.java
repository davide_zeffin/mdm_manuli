/*
 * Created on Aug 10, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.core.db.jdbc;

import java.sql.SQLException;
import java.util.BitSet;

import com.sap.isa.core.db.APIUserException;
import com.sap.tc.logging.Location;

/**
 * StateManaged manages the state of DAO and transitions to different states
 * @author I802891
 */
public class StateManager {
	private static final Location tracer = Location.getLocation(StateManager.class.getName());
	
	private JdbcDAO dao = null; 
	private boolean isNew = true;	
	private boolean dirty = false;
	private boolean deleted = false;
	private boolean persistent = false;
	private boolean transactional = false;
	private JdbcPersistenceManagerImpl pm = null;
	
	private BitSet dirtyIndexes = null;;
	
	StateManager(JdbcDAO dao, JdbcPersistenceManagerImpl pm) {
		this.pm = pm;
		this.dao = dao;
		int fieldCount = dao.getFieldCount();
		if(fieldCount > 0)
			dirtyIndexes = new BitSet(fieldCount);
	}
	
	JdbcPersistenceManager getPersistenceManager() {
		return pm;
	}
	
	boolean isDirty() {
		return dirty;
	}
	
	boolean isDeleted() {
		return deleted;
	}
	
	boolean isNew() {
		return isNew;
	}
	
	boolean isPersistent() {
		return persistent;
	}
	
	boolean isTransactional() {
		return transactional;
	}
	
	BitSet getDirtyIndexes() {
		return dirtyIndexes;
	}

	void markPersistent(JdbcDAO dao) {
		persistent = true;
	}
	
	void markTransactional(JdbcDAO dao) {
		transactional = true;
	}
	
	void markNonTransactional(JdbcDAO dao) {
		transactional = false;
	}
	
	void markDeleted(JdbcDAO dao) {
		deleted = true;
	}
	
	void markLoaded(JdbcDAO dao) {
		isNew = false;
		persistent = true;
		dirty = false;
		deleted = false;
	}
	
	void markRefreshed(JdbcDAO dao) {
		isNew = false;
		dirty = false;
		persistent = true;
		deleted = false;
	}
	
	void markDirty(JdbcDAO dao) {
		this.dirty = true;
	}
	
	void markFlushed(JdbcDAO dao) {
		dirty = false;
		isNew = false;
		transactional = false;
		persistent = true;
	}
	
	void refresh(JdbcDAO dao) {
		if(deleted) throw new APIUserException(tracer,APIUserException.DAO_IS_DELETED,null);
		if(!isNew && persistent) refresh();
		markRefreshed(dao);
	}
	
	void load(JdbcDAO dao) {
		if(!isNew && persistent) load();
		markLoaded(dao);
	}
	
	void flush(JdbcDAO dao, boolean commit) {
		if(commit) {
			save();
			markFlushed(dao);
		}
		else {
			// let the dao retain its old state, this is useful for UI to retain changes
			markNonTransactional(dao);
		}
		// else object retains its dirty state 
		if(dirtyIndexes != null) dirtyIndexes.clear();
	
	}
	
	
	/**
	 * Use this to creat pre image of the DAO object
	 * @param index
	 * @param newValue
	 * @param oldValue
	 */
	void setAttribute(int index, Object newValue, Object oldValue) {
		if(newValue != oldValue && !newValue.equals(oldValue)) {
			if(dirtyIndexes != null) dirtyIndexes.set(index);
			markDirty(dao);
		}
	}
	
	final void load() {
		if(!isNew()) {
			JdbcTransaction tx = (JdbcTransaction)getPersistenceManager().getTransaction();
			try {
				dao.doLoad(tx.getClientConnection());
			}
			catch(SQLException ex) {
				throw getPersistenceManager().getExceptionTranslator().translateException(ex,tracer);
			}
		}
		
		try {
			dao.afterLoad();
		}
		catch(Exception e) {
			APIUserException ex = new APIUserException(tracer,APIUserException.AFTER_LOAD_FAILED,null,e);
			throw ex;								
		} 		
	}
	
	/**
	 * Logic:
	 * If Tx in Optimistic mode 
	 * 	Places a write Lock with LifeCycle of Tx
	 * If Object is New 
	 * Then Insert
	 * else Update
	 * 
	 */
	final void save() {
		try {
			JdbcTransaction tx = (JdbcTransaction)getPersistenceManager().getTransaction();
			if(isNew()) {
				try{
					dao.beforeSave();
				}
				catch(Exception e) {
					APIUserException ex = new APIUserException(tracer,APIUserException.BEFORE_SAVE_FAILED,null,e);
					throw ex;
				}
				
				if(!isDeleted())
					dao.doInsert(tx.getClientConnection());
			}
			else if(isDeleted()) {
				try {
					dao.beforeDelete();
				}
				catch(Exception e) {
					APIUserException ex = new APIUserException(tracer,APIUserException.BEFORE_DELETE_FAILED,null,e);
					throw ex;						
				}
				dao.doDelete(tx.getClientConnection());
			}
			else {
				dao.beforeSave();
				dao.doUpdate(getDirtyIndexes(),tx.getClientConnection());
			}
		} catch (SQLException e) {
			throw getPersistenceManager().getExceptionTranslator().translateException(e,tracer);
		}
	}
	
	/**
	 * If Object is in Transaction
	 *
	 */
	final void refresh() {
		if(!isNew()) {
			JdbcTransaction tx = (JdbcTransaction)getPersistenceManager().getTransaction();	
			try {
				dao.doRefresh(tx.getClientConnection());
			} catch (SQLException e) {
				throw getPersistenceManager().getExceptionTranslator().translateException(e,tracer);
			}
		} 		
	}
		
	/**
	 * @param index
	 * @return
	 */
	Object getAttribute(int index) {
		return null;
	}

	/**
	 * @param sm
	 */
	void replaceStateManager(StateManager sm) {
		if(this == sm) {
			// do nothing
		}
		// detach scenario
		else if (sm == null) {
			dao = null;			
		}
		// replace scenario
		else {
		}
	}
}
