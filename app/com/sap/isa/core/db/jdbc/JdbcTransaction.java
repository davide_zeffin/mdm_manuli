/*
 * Created on Aug 10, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.core.db.jdbc;

import java.sql.Connection;
import java.sql.SQLException;

import javax.transaction.Status;
import javax.transaction.Synchronization;
import javax.transaction.Transaction;

import com.sap.isa.core.db.*;
import com.sap.tc.logging.Location;

/**
 * This class is thread-safe<br>
 * @author I802891
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class JdbcTransaction implements DBTransaction {
	
	private static final Location tracer = Location.getLocation(JdbcTransaction.class.getName());
	
	/**
	 * Transaction Status from javax.transaction.Status
	 */
	private int status = Status.STATUS_NO_TRANSACTION;
	
	/**
	 * The commit process has already begun (even though the status is still
	 * STATUS_ACTIVE).  This is the first thing set during commit or rollback.
	 */
	private boolean startedCommit = false;
		
	/**
	 * Optimistic transactions are not supported 
	 */
	private boolean optimistic = false;
	
	/**
	 * 
	 */
	private JdbcPersistenceManagerImpl pm;
	
	/**
	 * Physical database connection.
	 * Pessismistic: Transaction is started immediately when DBTransaction is started
	 * Optimistic: Transaction is started when DBTransaction is Committed (Not Supported) 
	 */
	private Connection conn = null;
	
	/**
	 * This is connection provided for use by Clients
	 * it restricts certain operations on Connection object
	 */
	private ClientConnection clientConn = null;
	/**
	 * JTA Transaction
	 */
	private Transaction jtaTx = null;
	
	private int txType = -1;
	
	private static final int UMT_JTA = 1; // User Transaction started By DBTransaction 
	private static final int CMT_JTA	= 2; // Container started transaction
	private static final int NON_MGD = 3; // Local trasaction started, Non Managed
	
	JdbcTransaction(JdbcPersistenceManagerImpl pm) {
		this.pm = pm;
		try {
			conn = pm.getDataSource().getConnection();			
		} 
		catch(SQLException e) {
			throw pm.getExceptionTranslator().translateException(e,tracer);
		}
		
		clientConn =  new ClientConnection(conn);		
	}
	
	private boolean isTerminated() {
		synchronized (this) {
			return status == Status.STATUS_COMMITTED ||
				   status == Status.STATUS_ROLLEDBACK ||
				   status == Status.STATUS_NO_TRANSACTION;
		}
	}
	
	/* 
	 * if Pessimistic Locking 
	 * 	start a new DB Transaction
	 * @see com.sap.isa.core.db.DBTransaction#begin()
	 */
	public void begin() {
		pm.assertOpen();
		
		// set Transaction type
		if(pm.isManagedRuntime()) {
			txType = UMT_JTA;
		}
		else {
			// A tx must be already active & register a synchronizer
			txType = NON_MGD;
		}
				
		beginInternal();			
	}
	
	private synchronized void beginInternal() {
		/*
		 * New Transaction cannot be started
		 */
		if(!isTerminated()) {
			// throw User Exception
		}

		if(pm.isManagedRuntime()) {
			JTAHelper jta = pm.getJTAHelper();
			// Start Transaction explicitly
			try {
				if(txType == UMT_JTA) {
					jta.getUserTransaction().begin();
				}
			
				jtaTx = jta.getTransaction();
				TxSynchronization txSync = new TxSynchronization();
				jtaTx.registerSynchronization(txSync);
				//pmFactory.registerPersistenceManager(pm,jtaTx);
			} catch (Exception e) {
				// Sync registration failed, rollback
				if(jtaTx != null && txType == UMT_JTA) {
					try {
						jta.getUserTransaction().rollback();
					} catch (Exception e1)  {
						throw new ResourceUsageException(tracer,ResourceAccessFailureException.FAILED_START_JTA_TRANSACTION,null);
					}
				}
				jtaTx = null;
				throw new ResourceUsageException(tracer,ResourceAccessFailureException.FAILED_START_JTA_TRANSACTION,null);
			}
		}
		
		try {
			if(txType == NON_MGD) {
				conn.setAutoCommit(false);
			}
			status = Status.STATUS_ACTIVE;
		} 
		catch(SQLException e) {
			throw pm.getExceptionTranslator().translateException(e,tracer);
		}
		
		//clientConn =  new ClientConnection(conn);
	}
	
	/**
	 * This called when PersistenceManager finds that CMT transaction is already in progress 
	 * @param tx
	 */
	void begin(Transaction tx) {
		// set Transaction type
		txType = CMT_JTA;
		
		beginInternal();
	}
	
	public void commit() {
		pm.assertOpen();
				
		if(txType == CMT_JTA) {
			// Not Allowed
			throw new APIUserException(null);
		}
		else if (txType == UMT_JTA){
			try {
				pm.getJTAHelper().getUserTransaction().commit();
			}
			catch(Exception ex) {
				throw new ResourceAccessFailureException(tracer,ResourceAccessFailureException.ROLLBACK_FAILED,null);
			}
		}
		else {
			synchronized(this) {
				if(startedCommit) {
					// throw new UserException
					throw new APIUserException(tracer,APIUserException.TX_COMMIT_ROLLBACK_IN_PROGRESS,null);
				}
				startedCommit = true;
			}
			
			try {
				this.prepareFlush(true); // do actual beforeCompletion.
				this.commitPrepare(); // check internal status.
				this.commitComplete(); // commitConnection and set status to success.

			} catch (Throwable e) {
				this.internalRollback();
				if (e instanceof DBException) {
					throw (DBException)e;
				}
				throw new DBException(tracer,DBException.INTERNAL_ERROR,null,e);
			} finally {
				this.internalAfterCompletion(-1); // do afterCompletion and cleanup.
				startedCommit = false;
			}
		}
	}
	
	/**
	 * Start Phase 1 
	 * @param commit
	 */
	private void prepareFlush(boolean commit) {
		boolean  rollbackOnly = false; //marked for rollback

		//
		// Validate transaction state before we commit
		//
		
		/**
		 * if SetRollbackOnly was called while Preparation
		 */
		if ((this.status == Status.STATUS_ROLLING_BACK)
			||    (this.status == Status.STATUS_ROLLEDBACK)) {
			throw new APIUserException(tracer,ResourceAccessFailureException.TX_ROLLBACK_ONLY,null); // NOI18N
		}

		if (this.status != Status.STATUS_ACTIVE) {
			throw new APIUserException(tracer,APIUserException.NO_ACTIVE_TX,null); // NOI18N
		}

		//
		// User notifications done outside of lock - check for concurrent
		// rollback or setRollbackOnly during notification.
		//
		if (!rollbackOnly) {
			pm.flushInstances(commit);

			if (this.status == Status.STATUS_ACTIVE) {        // All ok
				if (this.startedCommit) { // inside commit - change status.
					this.setStatus(Status.STATUS_PREPARING);
				}

			} else if (this.status == Status.STATUS_MARKED_ROLLBACK) {
				// This could happen only if this.setRollbackOnly() was called 
				// during flushInstances() without throwing an
				// exception.
				rollbackOnly = true;

			} else {    
				// concurrently rolled back - should not happen.
				throw new DBException(tracer,DBException.INTERNAL_ERROR,null); 
			}
		}
		
		if (rollbackOnly) {
			// Do not rollback here, but throw the exception and the rollback
			// will happen in the 'catch' block. Usually happens if the
			// connector was set rollback-only before the commit.
			// this.setRollbackOnly();

			throw new ResourceAccessFailureException(tracer,ResourceAccessFailureException.TX_ROLLBACK_ONLY,null); // NOI18N

		}
		
		// actually flush the changes to database
		this.setStatus(Status.STATUS_PREPARING);
	}
	
	/**
	 * End of Phase 1 - low level
	 *
	 */
	private void commitPrepare() {
		//
		// Validate initial state
		//
		if (this.status != Status.STATUS_PREPARING) {
			throw new DBException(tracer,DBException.INTERNAL_ERROR,null); // NOI18N
		}

		
		// Nothing needs to be done
		this.setStatus(Status.STATUS_PREPARED);		
	}
	
	/** 
	 * Phase 2
	 * @see com.sap.isa.core.db.DBTransaction#commit()
	 */
	private void commitComplete() {
		this.setStatus(Status.STATUS_COMMITTING);		
		if(txType == NON_MGD) {
			try {
				// Do the actual commit
				conn.commit();
			} catch (SQLException e) {
				throw  new ResourceAccessFailureException(tracer,ResourceAccessFailureException.COMMIT_FAILED,null,e);
			}
		}
		this.setStatus(Status.STATUS_COMMITTED);
	}
	
	/**
	 * Handler for JTA Transaction synchronization
	 *
	 */
	private void internalBeforeCompletion() {
		try {
			this.prepareFlush(true); // do actual beforeCompletion.
			this.commitPrepare(); 	// check internal status.
			this.setStatus(Status.STATUS_COMMITTED); // No actual commit is done here
		} catch (Throwable e) {
			this.internalRollback();
			if (e instanceof DBException) {
				throw (DBException)e;
			}
			throw new DBException(tracer,DBException.INTERNAL_ERROR,null,e); // NOI18N
		} 
	}
	
	/**
	 * Handler for JTA Transaction synchronization
	 * @param status
	 */
	private void internalAfterCompletion(int status) {
		if(status == Status.STATUS_COMMITTED) {
			// do nothing
		}
		else if (status == Status.STATUS_ROLLEDBACK){
			// check if PM needs to be notified of this to do some post clean up
			this.setStatus(Status.STATUS_ROLLEDBACK);
		}
	}
	
	/**
	 * Set status under lock (may be a nested lock which is ok)
	 */
	private void setStatus(int status) {
		/*
		if (debugging()) {
			logger.debug(
				"Tran[" + this.toString() + "].setStatus: " + // NOI18N
				this.statusString(this.status) + " => " + // NOI18N
				this.statusString(status));
		}
		*/
		synchronized(this) {
			this.status = status;
		}
	}
		
	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.DBTransaction#isActive()
	 */
	public boolean isActive() {
		pm.assertOpen();

		synchronized(this) {
			return status == Status.STATUS_ACTIVE;
		}
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.DBTransaction#isOptimistic()
	 */
	public boolean isOptimistic() {
		return false;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.DBTransaction#rollback()
	 */
	public void rollback() {
		pm.assertOpen();
		
		synchronized (this) {
			//
			// Disallow parallel transaction completion calls:
			//
			if (startedCommit) {
				throw new APIUserException(tracer,APIUserException.TX_COMMIT_ROLLBACK_IN_PROGRESS,null); 

			}
			if(isTerminated()) {
				throw new APIUserException(tracer,APIUserException.NO_ACTIVE_TX,null);				
			}
			
			if ((this.status != Status.STATUS_ACTIVE) &&    
				(this.status != Status.STATUS_MARKED_ROLLBACK)) {
				throw new APIUserException(tracer,APIUserException.TX_COMMIT_ROLLBACK_IN_PROGRESS,null);
			}
        
			// This flag prevents user from making any changes to the transaction object.
			this.startedCommit = true;
		}
		
		try {
			if(txType == NON_MGD) {
				internalRollback();
			}
			else if(txType == UMT_JTA) {
				try {
					pm.getJTAHelper().getUserTransaction().rollback();
				} catch (Exception e) {
					throw new DBException(tracer,ResourceAccessFailureException.ROLLBACK_FAILED,null,e);
				}
			}
			else {
				// not allowed to Rollback
				throw new APIUserException(tracer,APIUserException.JTA_MANAGED_USER_TX,null);
			}
		}
		finally {
			this.startedCommit = false;
		}
	}

	private void internalRollback() {
		this.setStatus(Status.STATUS_ROLLING_BACK);
		
		try {
			if(txType == CMT_JTA || txType == UMT_JTA) {
				try {
					jtaTx.setRollbackOnly();
				} catch (Exception e) {
					throw new ResourceAccessFailureException(tracer,ResourceAccessFailureException.ROLLBACK_FAILED,null,e);
				}
			}
			else {
				try {
					conn.rollback();
				} catch (SQLException e) {
					throw new ResourceAccessFailureException(tracer,ResourceAccessFailureException.ROLLBACK_FAILED,null,e);
				}
			}
		}
		finally {
			this.setStatus(Status.STATUS_ROLLEDBACK);
		}
	}
	
	/**
	 * Modify the transaction object such that the only possible outcome of
	 * the transaction is to roll back. This is only way in case of Container
	 * Managed Transactions
	 */
	public void setRollbackOnly() {
		
		synchronized(this) {		
			if ((this.status == Status.STATUS_ROLLING_BACK)
					||    (this.status == Status.STATUS_ROLLEDBACK)
					||     (this.status == Status.STATUS_MARKED_ROLLBACK)) {
				//
				// Already rolled back, rollback in progress or already marked.
				//
				return;
			}
		}

		if (txType != NON_MGD) {
			try {
				jtaTx.setRollbackOnly();
			} catch (Exception e) { 
				throw new ResourceAccessFailureException(tracer,ResourceAccessFailureException.ROLLBACK_FAILED,null); // NOI18N
			}
		} else {
			// no effect on Database Connection
		}
		
		this.setStatus(Status.STATUS_MARKED_ROLLBACK);
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.DBTransaction#setOptimistic(boolean)
	 */
	public void setOptimistic(boolean val) {
		pm.assertOpen();
		
		if(val)
			throw new DBException(tracer,DBException.UNSUPPORTED_OPERATION,new String[]{"setOptimistic"});
	}


	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.DBTransaction#setSychronization(javax.transaction.Synchronization)
	 */
	public void setSychronization(Synchronization sync) {
		pm.assertOpen();		
		// Ignored
	}
	
	/**
	 * <p>
	 * This connection is shared for external usage
	 * by clients for JDBC operations to be done as part
	 * of the same transaction
	 * This is only supported for Non JTA Transactions
	 * </p>
	 * @return
	 */	
	public Connection getClientConnection() {
		pm.assertOpen();
		
		/*if(!isActive()) {
			throw new APIUserException(tracer,APIUserException.NO_ACTIVE_TX,null);
		}*/
		
		return clientConn;
	}
	
	void close() {
		try {
			conn.close();
		}
		catch(SQLException ex) {
			throw new DBException(tracer,DBException.INTERNAL_ERROR, new String[]{"close"});
		}
	}
	
	private class TxSynchronization implements Synchronization {
			/* (non-Javadoc)
		 * @see javax.transaction.Synchronization#afterCompletion(int)
		 */
		public void afterCompletion(int status) {
			internalAfterCompletion(status);
		}

		/* (non-Javadoc)
		 * @see javax.transaction.Synchronization#beforeCompletion()
		 */
		public void beforeCompletion() {
			internalBeforeCompletion();
		}
	}
}
