/*
 * Created on Aug 24, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.core.db.jdbc;

import java.text.MessageFormat;

/**
 * <p>
 * ANSI SQL 92 Compliant SQL Provider
 * </p>
 * @author I802891
 */
public class SQLQueryProvider {
	private static final SQLQueryProvider singleton = new SQLQueryProvider();
	public static SQLQueryProvider getQueryProvider() {
		return singleton;
	}
	
	private static MessageFormat INSERT_TMPL = 
		new MessageFormat("INSERT INTO {0} ({1}) VALUES({2})");
	private static MessageFormat UPDATE_TMPL = 
		new MessageFormat("UPDATE {0} SET {1} WHERE {2}");	
	private static MessageFormat DELETE_TMPL = 
		new MessageFormat("DELETE FROM {0} WHERE {1}");
	private static MessageFormat SELECT_TMPL_1 = 
		new MessageFormat("SELECT {0} FROM {1}");
	private static MessageFormat SELECT_TMPL_2 = 
		new MessageFormat("SELECT {0} FROM {1} WHERE {2}");		

		
	public String getInsertSQLQuery(String tableName, String[] colNames) {
		StringBuffer catCols = new StringBuffer();
		StringBuffer catVals = new StringBuffer();
		for(int i = 0 ; i < colNames.length; i++) {
			if(i > 0) {
				catCols.append(", ");
				catVals.append(", "); 
			} 
			catCols.append(colNames[i]);
			catVals.append("?");
		}
		String[] vals = new String[3];
		vals[0] = tableName;
		vals[1] = catCols.toString();
		vals[2] = catVals.toString();
		return INSERT_TMPL.format(vals);
	}
	
	public String getUpdateSQLQuery(String tableName, String[] colNames, String[] whereCols) {
		StringBuffer catCols = new StringBuffer();
		StringBuffer catWhereCols = new StringBuffer();
		for(int i = 0 ; i < colNames.length; i++) {
			if(i > 0) {
				catCols.append(",");
			} 
			catCols.append(colNames[i]); 
			catCols.append(" = ");
			catCols.append("?");
		}
		if(whereCols != null) {
			for(int i = 0 ; i < whereCols.length; i++) {
				if(i > 0) {
					catWhereCols.append(" AND ");
				}
				catWhereCols.append(tableName);
				catWhereCols.append('.');				
				catWhereCols.append(whereCols[i]);
				catWhereCols.append(" = ");
				catWhereCols.append("?");
			}
		}		
		String[] vals = new String[3];
		vals[0] = tableName;
		vals[1] = catCols.toString();
		vals[2] = catWhereCols.toString();
		return UPDATE_TMPL.format(vals);		
	}
	
	public String getDeleteSQLQuery(String tableName, String[] whereCols) {
		StringBuffer catWhereCols = new StringBuffer();
		if(whereCols != null) {
			for(int i = 0 ; i < whereCols.length; i++) {
				if(i > 0) {
					catWhereCols.append(" AND ");
				}
				catWhereCols.append(tableName);
				catWhereCols.append('.');				
				catWhereCols.append(whereCols[i]);
				catWhereCols.append(" = ");
				catWhereCols.append("?");				
			}
		}		
		String[] vals = new String[2];
		vals[0] = tableName;
		vals[1] = catWhereCols.toString();
		return DELETE_TMPL.format(vals);			
	}

	/**
	 * @param TABLE_NAME
	 * @param COL_NAMES
	 * @param KEY_COL_NAMES
	 * @return
	 */
	public String getSelectSQLQuery(String tableName, String[] colNames, String[] whereCols) {
		StringBuffer catCols = new StringBuffer();
		StringBuffer catWhereCols = new StringBuffer();
		for(int i = 0 ; i < colNames.length; i++) {
			if(i > 0) {
				catCols.append(",");
			} 
			catCols.append(tableName);
			catCols.append('.');
			catCols.append(colNames[i]); 
		}
		if(whereCols != null) {
			for(int i = 0 ; i < whereCols.length; i++) {
				if(i > 0) {
					catWhereCols.append(" AND ");
				} 
				catWhereCols.append(tableName);
				catWhereCols.append('.');				
				catWhereCols.append(whereCols[i]);
				catWhereCols.append(" = ");
				catWhereCols.append("?");
			}
		}		
		String[] vals = new String[3];
		vals[0] = catCols.toString();		
		vals[1] = tableName;
		vals[2] = catWhereCols.toString();
		if(catWhereCols.length() > 0)
			return SELECT_TMPL_2.format(vals);
		else
			return SELECT_TMPL_1.format(vals);			
	}
}
