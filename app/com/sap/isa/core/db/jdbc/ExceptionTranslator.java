/*
 * Created on Aug 16, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.core.db.jdbc;

import java.sql.SQLException;

import com.sap.isa.core.db.DBException;
import com.sap.tc.logging.Location;

/**
 * @author I802891
 */
public class ExceptionTranslator {
	public DBException translateException(SQLException ex, Location tracer) {
		return new DBException(tracer,DBException.UNKNOWN_ERROR,null,ex);
	}
}	
