/*
 * Created on Aug 6, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.core.db.jdbc;

import java.util.LinkedList;
import java.util.List;

import com.sap.isa.core.db.DBPersistenceManager;

/**
 * @author I802891
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public abstract class RdbmsOperation {
	
	protected JdbcPersistenceManagerImpl pm = null;
	protected String sql = null;
	protected boolean sqlComplete = false;
	protected List parameters = new LinkedList();
	
	protected RdbmsOperation(JdbcPersistenceManagerImpl pm) {
		this.pm = pm;
	}
	
	public void setSQL(String sql) {
		this.sql = sql;
	}
	
	public boolean isSQLComplete() {
		return sqlComplete;
	}
	
	public void setSQLComplete(boolean val) {
		this.sqlComplete = val;
	}
	
	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.DBQuery#getPersistenceManager()
	 */
	public DBPersistenceManager getPersistenceManager() {
		return pm;
	}
	
	public String getSQL() {
		return sql;
	}
	
	public void addParameter(SQLParameter param) {
		if(!parameters.contains(param)) {
			parameters.add(param);
		}
	}
	
	public List getParameters() {
		return parameters;
	}
}
