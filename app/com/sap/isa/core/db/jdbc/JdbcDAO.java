/*
 * Created on Jul 22, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.core.db.jdbc;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.BitSet;

import com.sap.engine.frame.core.locking.LockException;
import com.sap.isa.core.db.APIUserException;
import com.sap.isa.core.locking.LockInfo;
import com.sap.isa.core.locking.enqueue.EnqueueLockManager;
import com.sap.tc.logging.Location;

/**
 * <p>
 * It is upto Derived class to implement java.io.Serializable interface and contract<br/>
 * This class supports java.io.Serializable contract<br/>
 * Entire inheritence heirarchy supports only one key<br/>
 * Derived classes implements do*() methods<br/>
 * </p>
 * @author I802891
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public abstract class JdbcDAO {
	
	private static final Location tracer = Location.getLocation(JdbcDAO.class.getName());
	
	private transient StateManager sm = null;
	protected Serializable key = null;

	/**
	 * Only Default ctor is provided
	 */
	protected JdbcDAO() {
	}
	
	final protected StateManager getStateManager() {
		return sm;
	}
	
	final void setStateManager(StateManager sm) {
		if(this.sm != null) {
			this.sm.replaceStateManager(sm);
		}
		
		this.sm = sm;
	}	

	public final JdbcPersistenceManager getPersistenceManager() {
		if(sm != null) return sm.getPersistenceManager();
		return null;
	}
		
	/**
	 * This method must be invoked by each Set Method on the Bean
	 * if(StateMgr is set)
	 * 	Make Object Transactional 
	 * 	If Pessimistic Locking then place WRITE lock
	 * 	Set Dirty Bit to true at index
	 */ 
	protected void makeDirty(int index, Object oldValue, Object newValue) {
		if(sm != null) {
			getPersistenceManager().makeTransactional(this);
			sm.setAttribute(index,newValue,oldValue);
		}
	}
		
	final public boolean isDirty() {
		if(sm != null)
			return sm.isDirty();
		else 
			return false;
	}
	
	final public boolean isNew() {
		if(sm != null)
			return sm.isNew();
		else 
			return false;
	}
	
	final public boolean isDeleted() {
		if(sm != null)
			return sm.isDeleted();
		else 
			return false;
	}
	
	final public boolean isPersistent() {
		if(sm != null) {
			return sm.isPersistent();
		}
		else 
			return false;
	}
	
	final public Serializable getKey() {
		return key;
	}
	
	final public void setKey(Serializable key) {
		this.key = key;
	}
	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		if(this == obj) return true;
		
		if(obj instanceof JdbcDAO) {
			JdbcDAO dao = (JdbcDAO)obj;
			if(dao.getKey() == null) return false;
			if(this.getKey() == null) return false; // undefined behavior
			return this.getKey().equals(dao.getKey());
		}
		else 
			return false;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode() {
		if(this.getKey() != null) {
			return this.getKey().hashCode();
		}
		else {
			return super.hashCode();
		}
	}
	
	//////////////////////////////////////////////////////////////////
	// Derived class contract
	 
	protected abstract void doLoad(Connection conn) throws SQLException;
	protected abstract void doInsert(Connection conn) throws SQLException;
	protected abstract void doUpdate(BitSet dirtyIndexes, Connection conn) throws SQLException;
	protected abstract void doDelete(Connection conn) throws SQLException;
	protected void doRefresh(Connection conn) throws SQLException {
		doLoad(conn);
	}

	/**
	 * The concrete class implementing this method must declare it final
	 * @return
	 */
	protected abstract String[] getKeyColumns();
	
	/**
	 * 
	 * @return ResultSetReader
	 */
	public abstract ResultSetReader getResultSetReader();
	
	/////////////////////////////////////////////////////////////////
	// Optional contract
	
	protected int getFieldCount() {
		return 0;
	}
	
	protected String getColumnName(int fieldCount) {
		return null;
	}
	
	
	protected int[][] getFilterColumns() {
		return null;
	}
	
	protected int[][] getOrderByColumns() {
		return null;
	}
	
	public SQLQuery getNamedQuery(String name) {
		return null;
	}
	
	public SQLQuery[] getNamedQueries() {
		return new SQLQuery[0];
	}
	
	///////////////////////////////////////
	// callbacks
	protected void beforeSave() {
	}
	
	protected void beforeDelete() {
	}
	
	protected void afterLoad() {
	}
}
