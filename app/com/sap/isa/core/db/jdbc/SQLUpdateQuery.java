/*
 * Created on Jul 22, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.core.db.jdbc;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Iterator;
import java.util.Map;

import com.sap.isa.core.db.APIUserException;
import com.sap.isa.core.db.DBException;
import com.sap.isa.core.db.ResourceUsageException;
import com.sap.tc.logging.Location;

/**
 * @author I802891
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class SQLUpdateQuery extends RdbmsOperation {
	
	private static final Location tracer = Location.getLocation(SQLUpdateQuery.class.getName());
	
	private PreparedStatement ps = null;
	private int rowsEffected;
	private int expectedEffectedRows;

	
	/**
	 * @param pm
	 */
	protected SQLUpdateQuery(JdbcPersistenceManagerImpl pm) {
		super(pm);
	}
	
	/**
	 * @return
	 */
	public int getExpectedEffectedRows() {
		return expectedEffectedRows;
	}

	/**
	 * @return
	 */
	public int getRowsEffected() {
		return rowsEffected;
	}

	/**
	 * @param i
	 */
	public void setExpectedEffectedRows(int i) {
		expectedEffectedRows = i;
	}

	/**
	 * @param i
	 */
	public void setRowsEffected(int i) {
		rowsEffected = i;
	}
	
	public int update(Map paramValues) {
		pm.assertOpen();
		
		Iterator it = parameters.iterator();
		Object[] values = new Object[parameters.size()];
		int i = 0;
		while(it.hasNext()) {
			String paramName = ((SQLParameter)it.next()).getName();
			if(!paramValues.containsKey(paramName))
				throw new APIUserException(tracer,APIUserException.QUERY_PARAM_MISSING, new String[]{paramName});
			values[i++] = paramValues.get(paramName);
		}
		return update((Object[])values);			
	}
	
	/**
	 * Generic method to execute the update given arguments.
	 * All other update methods invoke this method.
	 * @param args array of object arguments
	 * @return the number of rows affected by the update
	 */
	public int update(Object[] paramValues) throws DBException {
		pm.assertOpen();		
		
		if(sql == null || sql.trim().length() == 0) throw new APIUserException(tracer,APIUserException.QUERY_PARAM_COUNT_MISMATCH,null);

		validateParameters(paramValues);
		
		try {
			ps = pm.getTransaction().getClientConnection().prepareStatement(getSQL());
			setValues(paramValues);
			int rowsAffected = ps.executeUpdate();
			checkRowsAffected(rowsAffected);
			return rowsAffected;
		}
		catch(SQLException ex) {
			// TODO
			ex.printStackTrace();
			throw new ResourceUsageException(tracer,ResourceUsageException.PREPARE_STMT_FAILED,new String[]{getSQL()});
		}
	}

	/**
	 * @param paramValues
	 */
	private void validateParameters(Object[] paramValues) {
		// TODO Auto-generated method stub
		
	}

	private void setValues(Object[] values) throws SQLException {
		// Set arguments: Does nothing if there are no parameters.
		for (int i = 0; i < this.parameters.size(); i++) {
			SQLParameter declaredParameter = (SQLParameter) parameters.get(i);
			// we need SQL type to be able to set null
			if (this.parameters.get(i) == null) {
				ps.setNull(i + 1, declaredParameter.getJdbcType());
			}
			else {
				switch (declaredParameter.getJdbcType()) {
					case Types.VARCHAR:
						ps.setString(i + 1, this.parameters.get(i).toString());
						break;
					default :
						ps.setObject(i + 1, this.parameters.get(i), declaredParameter.getJdbcType());
						break;
				}
			}
		}
	}

	/**
	 * @param rowsAffected
	 */
	private void checkRowsAffected(int rowsAffected) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * Convenience method to execute an update with no parameters.
	 */
	public int update() throws DBException {
		return update((Object[]) null);
	}
		
}
