/*
 * Created on Aug 12, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.core.db.jdbc;

import javax.transaction.Transaction;
import javax.transaction.UserTransaction;

/**
 * <p>
 * This interface serves as the Client for JTA in a managed enviorment
 * An Implementation of this is plugged to PersistenceManager to indicate 
 * usage in managed enviorment
 * </p>
 * @author I802891
 */
public interface JTAHelper {
	/**
	 * 
	 * @return
	 */
	UserTransaction getUserTransaction();
	
	/**
	 * 
	 * @return
	 */
	Transaction getTransaction();
	
	/**
	 * Optional
	 * @param obj
	 * @return
	 */
	Object enlistResource(Object obj, Transaction tx);
	
	/**
	 * Optional
	 * @param obj
	 */
	void delistResource(Object obj);
}
