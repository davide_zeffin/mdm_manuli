/*
 * Created on Aug 24, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.core.db.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 * @author I802891
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ListResultSetReader implements ResultSetReader {
	private RowReader rowReader = null;
	private LinkedList list = new LinkedList();
	
	public ListResultSetReader(RowReader rowReader) {
		this.rowReader = rowReader;
	}
	
	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.jdbc.ResultSetReader#read(java.sql.ResultSet)
	 */
	public Object read(ResultSet resultset) throws SQLException {
		while(resultset.next()) {
			list.add(rowReader.read(resultset));
		}
		return list;
	}
	
	public List getList() {
		return list;
	}
	
	public int getRowCount() {
		return list.size();
	}
}
