/*
 * Created on Jul 22, 2004
 *
 */
package com.sap.isa.core.db.jdbc;

import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.Types;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.sap.isa.core.db.APIUserException;
import com.sap.isa.core.db.DBQuery;
import com.sap.isa.core.db.DBQueryResult;
import com.sap.isa.core.db.QueryFilter;
import com.sap.isa.core.db.ResourceUsageException;
import com.sap.tc.logging.Location;

/**
 * @author I802891
 *
 */
public final class SQLQuery extends RdbmsOperation implements DBQuery {
	
	private static final Location tracer = Location.getLocation(SQLQuery.class.getName());
	
	private LinkedList results;
	private String strFilter;
	private String strOrderBy;
	private boolean asc = false;
	private int resultSetType = ResultSet.TYPE_FORWARD_ONLY;
	private PreparedStatement ps = null;
	private ResultSetReader rsr = null;
	private boolean compiled = false;

	/**
	 * @param pm
	 */
	protected SQLQuery(JdbcPersistenceManagerImpl pm) {
		super(pm);
		results = new LinkedList();
	}

	public void setResultSetType(int type) {
		pm.assertOpen();
		checkEditable();
		this.resultSetType = type;
	}
	
	public int getResultSetType() {
		pm.assertOpen();		
		return resultSetType;
	}
	
	
	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.DBQuery#close(com.sap.isa.core.db.DBQueryResult)
	 */
	public void closeAll() {
		pm.assertOpen();
		
		JdbcQueryResult[] arr = new JdbcQueryResult[results.size()]; 
		for(int i = 0; i < arr.length; i++) {
			JdbcQueryResult result = arr[i];
			result.close();		
		}
	}

	/**
	 * @param result
	 */
	void close(DBQueryResult result) {
		results.remove(result);
	}
	
	/* 
	 * (non-Javadoc)
	 * @see com.sap.isa.core.db.DBQuery#getClassType()
	 */
	public Class getClassType() {
		pm.assertOpen();		
		return null;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.DBQuery#getFilter()
	 */
	public String getFilter() {
		pm.assertOpen();		
		return strFilter;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.DBQuery#setFilter(com.sap.isa.core.db.QueryFilter)
	 */
	public void setFilter(QueryFilter filter) {
		pm.assertOpen();		
		checkEditable();		
		if(filter == null) {
			throw new IllegalArgumentException();
		}
		this.strFilter = toWhereClause(filter);
	}

	private String toWhereClause(QueryFilter filter) {
		StringBuffer where = new StringBuffer();
		List crits = filter.getCriterias();
		Iterator it = crits.iterator();
		while(it.hasNext()) {
			Object element = it.next();

			if(element instanceof QueryFilter) {
				QueryFilter nestedFilter = (QueryFilter)element;

				switch(nestedFilter.getOperator()) {
					case QueryFilter.AND:where.append(" AND "); break;
					case QueryFilter.NOT:where.append(" NOT "); break;
					case QueryFilter.OR:where.append(" OR "); break;								
				}
				
				where.append("(");
				where.append(toWhereClause(nestedFilter));
				where.append(")");
			}
			else {
				where.append(" AND ");
				QueryFilter.Criteria crit = (QueryFilter.Criteria)element;
				SQLParameter param = new SQLParameter();
				param.setName(param.getName());
				parameters.add(param);				
				where.append(crit.getParameterName());
				switch(crit.getOperator()) {
					case QueryFilter.Criteria.NOT_ET: where.append(" <> "); break;
					case QueryFilter.Criteria.ET: where.append(" = "); break;
					case QueryFilter.Criteria.GT: where.append(" > "); break;
					case QueryFilter.Criteria.GT_ET: where.append(" >= "); break;
					case QueryFilter.Criteria.LT: where.append(" < "); break;
					case QueryFilter.Criteria.LT_ET: where.append(" <= "); break;
				}
				if(((String)crit.getValue()).trim().startsWith("?")) {
					where.append(" ? ");
				}
				else {
					where.append(crit.getValue().toString());
				}
			}
		}
		return where.toString();
	}
	
	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.DBQuery#setFilter(java.lang.String)
	 */
	public void setFilter(String str) {
		pm.assertOpen();		
		checkEditable();		
		if(str == null || str.trim().length() == 0) {
			throw new IllegalArgumentException();
		}
		this.strFilter = str;
	}

	public ResultSetReader getResultSetReader() {
		pm.assertOpen();		
		return rsr;
	}
	
	public void setResultSetReader(ResultSetReader reader) {
		pm.assertOpen();		
		checkEditable();		
		if(reader == null) throw new IllegalArgumentException();
		this.rsr = reader;
	}
	
	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.DBQuery#compile()
	 */
	public void compile() {
		pm.assertOpen();		
		if(compiled) return;
		
		if(sql == null || sql.trim().length() == 0) throw new ResourceUsageException(tracer,ResourceUsageException.PREPARE_STMT_FAILED,null);
		
		if(rsr == null) {
			APIUserException ex = new APIUserException(tracer,APIUserException.QUERY_PARAM_NOT_DECL,null);
		}
		
		String sqlquery = null;
		if(isSQLComplete()) {
			sqlquery = sql;
		}
		else {
			StringBuffer bufSql = new StringBuffer(sql);
			if(strFilter != null && strFilter.trim().length() > 0) {
				bufSql.append(" WHERE ");
				bufSql.append(strFilter);
			}
			if(strOrderBy != null && strOrderBy.trim().length() > 0) {
				bufSql.append(" ORDER BY ");
				bufSql.append(strOrderBy);
				if(asc) {
					bufSql.append(" ASC ");
				}
				else {
					bufSql.append(" DESC ");
				}
			}
			sqlquery = bufSql.toString();
		}
		
		try {
			ps = pm.getTransaction().getClientConnection().prepareStatement(sql);
		} catch (SQLException e) {
			throw new ResourceUsageException(tracer,ResourceUsageException.PREPARE_STMT_FAILED,new String[]{sql},e);
		}
		compiled = true;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.DBQuery#execute()
	 */
	public DBQueryResult execute() {
		pm.assertOpen();
		return execute((Object[])null);
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.DBQuery#execute(java.util.HashMap)
	 */
	public DBQueryResult execute(HashMap paramValues) {
		pm.assertOpen();
		
		Iterator it = parameters.iterator();
		Object[] values = new Object[parameters.size()];
		int i = 0;
		while(it.hasNext()) {
			String paramName = ((SQLParameter)it.next()).getName();
			if(!paramValues.containsKey(paramName))
				throw new APIUserException(tracer,APIUserException.QUERY_PARAM_NOT_DECL, new String[]{paramName});
			values[i++] = paramValues.get(paramName);
		}
		return execute((Object[])values);
	}
	
	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.DBQuery#execute(java.lang.Object[])
	 */
	public DBQueryResult execute(Object[] values) {
		pm.assertOpen();
		
		if(values == null && parameters.size() > 0) {
			throw new APIUserException(tracer,APIUserException.QUERY_PARAM_MISSING,null);
		}
		
		if(values != null && (values.length != parameters.size())) {
			throw new APIUserException(tracer,APIUserException.QUERY_PARAM_COUNT_MISMATCH,null);
		}
		
		compile();
		
		List list = null;
		try {
			setValues(values == null ? new Object[]{} : values);
			ResultSet rs = ps.executeQuery();
			SQLWarning warning = ps.getWarnings();
			ps.clearWarnings();
			// TODO: log warnings
			ps.clearParameters();
			
			ResultSetReader rr = getResultSetReader();
			list = (List)rr.read(rs);
			// register loaded objects, if they are JdbcDAOs
			for(int i = 0; i < list.size();i++) {
				Object o = list.get(i);
				if(o instanceof JdbcDAO) {
					JdbcDAO dao = (JdbcDAO)list.get(i);
					StateManager sm = new StateManager(dao,pm);
					dao.setStateManager(sm);
					pm.registerNonTransactional((JdbcDAO)list.get(i));
					dao.getStateManager().markLoaded(dao);
				}
			}
		} catch (SQLException e) {
			throw pm.getExceptionTranslator().translateException(e,tracer);
		}
		
		JdbcQueryResult result = new JdbcQueryResult(this,list);
		results.add(result);
		return result;
	}
	
	private void setValues(Object[] values) throws SQLException {
		// Set arguments: Does nothing if there are no parameters.
		for (int i = 0; i < this.parameters.size(); i++) {
			SQLParameter declaredParameter = (SQLParameter) parameters.get(i);
			// we need SQL type to be able to set null
			if (this.parameters.get(i) == null) {
				ps.setNull(i + 1, declaredParameter.getJdbcType());
			}
			else {
				switch (declaredParameter.getJdbcType()) {
					case Types.VARCHAR:
						ps.setString(i + 1, values[i].toString());
						break;
					case Types.SMALLINT:
						ps.setShort(i + 1, ((Short)values[i]).shortValue());
						break;						
					case Types.INTEGER:
						ps.setInt(i + 1, ((Integer)values[i]).intValue());
						break;
					case Types.BINARY:
					case Types.VARBINARY: 
						ps.setBytes(i + 1, (byte[])values[i]);
						break;
					case Types.DECIMAL:
					case Types.NUMERIC:
						ps.setBigDecimal(i +1,(java.math.BigDecimal)values[i]);
						break;
					case Types.FLOAT:
						ps.setFloat(i+1,((Float)values[i]).floatValue());
						break;
					case Types.BIGINT:
						ps.setLong(i+1,((Long)values[i]).longValue());
						break;
					case Types.DOUBLE:
						ps.setDouble(i+1,((Double)values[i]).doubleValue());
						break;					
					case Types.BLOB:
						ps.setBlob(i + 1, (java.sql.Blob)values[i]);
						break;						
					case Types.CLOB:
						ps.setClob(i +1, (java.sql.Clob)values[i]);
						break;						
					default :
						ps.setObject(i + 1, values[i], declaredParameter.getJdbcType());
						break;
				}
			}
		}
	}
	
	private void checkEditable() {
		if(compiled) 
			throw new APIUserException(tracer,APIUserException.QUERY_IS_COMPILED,null);
	}
	
	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.DBQuery#setOrderBy(java.lang.String, boolean)
	 */
	public void setOrderBy(String str, boolean asc) {
		pm.assertOpen();
		checkEditable();
		this.strOrderBy = str;
		this.asc = asc;
	}
	
	public String getOrderBy() {
		pm.assertOpen();
		return strOrderBy;
	}
	
	public boolean isAscending() {
		pm.assertOpen();
		return asc;
	}
}
