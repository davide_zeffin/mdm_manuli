/*
 * Created on Aug 31, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.core.db.jdbc;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.sql.DataSource;

/**
 * @author I802891
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class DriverManagerDataSource implements DataSource {

		private String driverClassName = "";

		private String url = "";

		private String username = "";

		private String password = "";

		/**
		 * Constructor for bean-style configuration.
		 */
		public DriverManagerDataSource() {
		}

		/**
		 * Create a new SingleConnectionDataSource with the given standard
		 * DriverManager parameters.
		 */
		public DriverManagerDataSource(String driverClassName, String url, String username, String password)
				 {
			setDriverClassName(driverClassName);
			setUrl(url);
			setUsername(username);
			setPassword(password);
		}

		public void setDriverClassName(String driverClassName) {
			this.driverClassName = driverClassName;
			try {
				Class.forName(this.driverClassName, true, Thread.currentThread().getContextClassLoader());
			}
			catch (ClassNotFoundException ex) {
				throw new RuntimeException("Could not load JDBC driver class [" +
																									 this.driverClassName + "]", ex);
			}
			//logger.info("Loaded JDBC driver: " + this.driverClassName);
		}

		public String getDriverClassName() {
			return driverClassName;
		}

		public void setUrl(String url) {
			this.url = url;
		}

		public String getUrl() {
			return url;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public String getUsername() {
			return username;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public String getPassword() {
			return password;
		}

		/**
		 * This DataSource returns a new connection every time:
		 * Close it when returning one to the "pool".
		 */
		public boolean shouldClose(Connection conn) {
			return true;
		}

		public Connection getConnection() throws SQLException {
			return getConnectionFromDriverManager();
		}

		public Connection getConnection(String username, String password) throws SQLException {
			return getConnectionFromDriverManager(this.url, username, password);
		}

		/**
		 * Get a new Connection from DriverManager, with the connection
		 * properties of this DataSource.
		 */
		protected Connection getConnectionFromDriverManager() throws SQLException {
			return getConnectionFromDriverManager(this.url, this.username, this.password);
		}

		/**
		 * Getting a connection using the nasty static from DriverManager is extracted
		 * into a protected method to allow for easy unit testing.
		 */
		protected Connection getConnectionFromDriverManager(String url, String username, String password)
			throws SQLException {
			/*if (logger.isInfoEnabled()) {
				logger.info("Creating new JDBC connection to [" + url + "]");
			}*/
			return DriverManager.getConnection(url, username, password);
		}

	/* (non-Javadoc)
	 * @see javax.sql.DataSource#getLoginTimeout()
	 */
	public int getLoginTimeout() throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see javax.sql.DataSource#getLogWriter()
	 */
	public PrintWriter getLogWriter() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see javax.sql.DataSource#setLoginTimeout(int)
	 */
	public void setLoginTimeout(int seconds) throws SQLException {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see javax.sql.DataSource#setLogWriter(java.io.PrintWriter)
	 */
	public void setLogWriter(PrintWriter out) throws SQLException {
		// TODO Auto-generated method stub

	}

}
