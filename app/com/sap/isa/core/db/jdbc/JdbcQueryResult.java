/*
 * Created on Aug 19, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.core.db.jdbc;

import java.util.List;

import com.sap.isa.core.db.DBQueryResult;

/**
 * @author I802891
 */
public final class JdbcQueryResult extends DBQueryResult {
	
	private List list;
	
	/**
	 * @param query
	 */
	JdbcQueryResult(SQLQuery query, List list) {
		super(query);
		this.list = list;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.DBQueryResult#doPosition(int)
	 */
	protected void doPosition(int pos) {
		// do nothing
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.DBQueryResult#next()
	 */
	public Object next() {
		Object val = list.get(pos - 1);
		pos++;
		return val;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.DBQueryResult#size()
	 */
	public int size() {
		return list.size();
	}
	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.DBQueryResult#isDetached()
	 */
	public boolean isDetached() {
		// TODO Auto-generated method stub
		return super.isDetached();
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.DBQueryResult#close()
	 */
	public void close() {
		super.close();
		((SQLQuery)query).close(this);
	}
}
