/*
 * Created on Aug 16, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.core.db.jdbc;

import java.lang.ref.SoftReference;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

import javax.sql.DataSource;

import com.sap.isa.core.db.APIUserException;
import com.sap.isa.core.db.DBException;
import com.sap.isa.core.db.DBQuery;
import com.sap.isa.core.db.DBQueryResult;
import com.sap.isa.core.db.DBTransaction;
import com.sap.isa.core.locking.enqueue.EnqueueLockManager;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;

/**
 * @author I802891
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public final class JdbcPersistenceManagerImpl implements JdbcPersistenceManager {
	
	private static final Location tracer = Location.getLocation(JdbcPersistenceManagerImpl.class.getName());
	private static Category logger = Category.SYS_DATABASE; // default logger
		
	private ExceptionTranslator translator = new ExceptionTranslator();;

	private JdbcPersistenceManagerFactory pmf;
	
	/**
	 * 
	 */
	private JTAHelper jta;

	/**
	 * 
	 */
	private DataSource ds;
	
	/**
	 * 
	 */
	private EnqueueLockManager lm;
	
	/**
	 * 
	 */
	private boolean closed = false;
	
	/**
	 * 
	 */
	private boolean threadSafe = false;
	
	/**
	 * If threadSafe then lock is used for synchronization
	 */
	private Object lock = null;
	
	/**
	 * 
	 */
	private JdbcTransaction tx;
	
	/**
	 * Memory sensitive cache of Objects
	 */
	private HashMap cache;
	
	/**
	 * 
	 */
	private HashSet dirtyObjects;

	/**
	 * 
	 * @param pmf
	 */
	JdbcPersistenceManagerImpl(JdbcPersistenceManagerFactory pmf) {
		this.pmf = pmf;
		this.ds = pmf.getDataSource();
		
		cache = new HashMap();
		dirtyObjects = new HashSet();
		
		this.tx = new JdbcTransaction(this);
		
		closed = false;
	}
	
	void assertOpen() {
		if(closed) {
			throw new APIUserException(tracer,APIUserException.PM_IS_CLOSED,null);
		}
	}
	
	private void checkActiveTransaction() {
		if(!tx.isActive()) {
			throw new APIUserException(tracer,APIUserException.NO_ACTIVE_TX,null);
		}
	}
	
	private JdbcDAO checkDAOContract(Object o) {
		if(o == null || !(o instanceof JdbcDAO)) {
			throw  new APIUserException(tracer,APIUserException.DAO_CONTRACT_NOT_IMPLEMENTED,null);
		}
		
		JdbcDAO dao = (JdbcDAO)o;

		// Only Application defined key supported
		if(dao.getKey() == null) {
			throw new APIUserException(tracer,APIUserException.DAO_KEY_IS_NULL,null);
		}
		
		if(dao.getPersistenceManager() != null && dao.getPersistenceManager() != this) {
			throw new APIUserException(tracer,APIUserException.DAO_IS_ATTACHED_OTHER_PM,null);
		}
		
		
		return dao;
	}
	
	JdbcDAO[] checkDAOContract(Object[] objs) {
		if(objs == null) return null;
		
		JdbcDAO[] daos = new JdbcDAO[objs.length];
		
		for(int i = 0; i < objs.length; i++) {
			daos[i] = checkDAOContract(objs[i]);
		}
		
		return daos;
	}
	
	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.jdbc.JdbcPersistenceManager#getDataSource()
	 */
	public DataSource getDataSource() {
		assertOpen();
		
		return ds;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.jdbc.JdbcPersistenceManager#getJTAHelper()
	 */
	public JTAHelper getJTAHelper() {
		assertOpen();
		
		return jta;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.jdbc.JdbcPersistenceManager#getLockManager()
	 */
	public EnqueueLockManager getLockManager() {
		assertOpen();
		
		return lm;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.jdbc.JdbcPersistenceManager#newSQLFunction()
	 */
	public SQLFunction newSQLFunction() {
		assertOpen();
		
		SQLFunction func = new SQLFunction(this);
		
		return func;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.jdbc.JdbcPersistenceManager#newStoredProcedure()
	 */
	public StoredProcedure newStoredProcedure() {
		assertOpen();
		
		StoredProcedure proc = new StoredProcedure(this);
		
		return proc;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.jdbc.JdbcPersistenceManager#newUpdateQuery()
	 */
	public SQLUpdateQuery newUpdateQuery() {
		assertOpen();
		
		SQLUpdateQuery query = new SQLUpdateQuery(this);
		
		return query;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.jdbc.JdbcPersistenceManager#setLockManager(com.sap.isa.core.locking.enqueue.EnqueueLockManager)
	 */
	public void setLockManager(EnqueueLockManager lockMgr) {
		assertOpen();
		if(this.lm == null) {
			this.lm = lockMgr;
		}
		else {
			// already set, throw 
			throw new APIUserException(tracer,APIUserException.PROPERTY_CHANGE_ALLOWED_ONLY_ONCE,null);
		}
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.DBPersistenceManager#attach(java.lang.Object)
	 */
	public void attach(Object obj) {
		assertOpen();
		
		JdbcDAO dao  = checkDAOContract(obj);
		
		if(dao.getStateManager() == null) {
			dao.setStateManager(new StateManager(dao,this));
			if(dao.getKey() != null) {				
				dao.getStateManager().markLoaded(dao);
				try {
					// Check that OID exists in the database
					dao.getStateManager().refresh();
				}
				catch(Exception ex) {
					dao.setStateManager(null);
					if(ex instanceof DBException) {
						throw (DBException)ex;
					}
					DBException dbex = new DBException(tracer,DBException.INTERNAL_ERROR,
														new String[]{"Refresh failed of DAO;Key = " + dao.getKey()},ex);
				}
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.DBPersistenceManager#attachQueryResult(com.sap.isa.core.db.DBQueryResult)
	 */
	public void attachQueryResult(DBQueryResult result) {
		assertOpen();
		
		if(!result.isDetached()) {
			throw new APIUserException(tracer,APIUserException.QUERY_RESULT_IS_ATTACHED,null);
		}
		
		//result.attach(this);
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.DBPersistenceManager#close()
	 */
	public void close() {
		if(closed) return;// ignore;
		
		if(tx.isActive()) {
			throw new APIUserException(tracer,APIUserException.TX_IS_ACTIVE,null);		
		}
		tx.close();
		this.dirtyObjects.clear();
		this.cache.clear();
		this.closed = true;
		
		this.pmf.returnToPool(this);
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.DBPersistenceManager#delete(java.lang.Object)
	 */
	public void delete(Object obj) {
		assertOpen();
		
		checkActiveTransaction();
		
		JdbcDAO dao = checkDAOContract(obj);
		
		registerTransactional(dao);
		
		deleteInternal(dao);		
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.DBPersistenceManager#deleteAll(java.util.Collection)
	 */
	public void deleteAll(Collection objects) {
		assertOpen();
		
		checkActiveTransaction();
		
		JdbcDAO[] daos = checkDAOContract(objects != null ? objects.toArray() : null);
		
		if(daos != null) {
			registerTransactional(daos);
			
			deleteInternal(daos);	
		}	
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.DBPersistenceManager#detach(java.lang.Object)
	 */
	public void detach(Object obj) {
		assertOpen();
		
		JdbcDAO dao = checkDAOContract(obj);
		
		if(dao.getPersistenceManager() == this) {
			// if Object is dirty in tx
			if(dirtyObjects.contains(obj)) {
				throw new APIUserException(tracer,APIUserException.DAO_IS_DIRTY,null);
			}
			dao.setStateManager(null);
		}
		else if(dao.getPersistenceManager() != null) {
			throw  new APIUserException(tracer,APIUserException.DAO_IS_ATTACHED_OTHER_PM,null);
		}
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.DBPersistenceManager#getObjectById(java.lang.Object)
	 */
	public Object getObjectById(Object id) {
		assertOpen();

		if(id == null) throw new APIUserException(tracer,APIUserException.DAO_KEY_IS_NULL,null);
				
		throw new DBException(tracer, DBException.UNSUPPORTED_OPERATION,new String[]{"getObjectById"});
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.DBPersistenceManager#getTransaction()
	 */
	public DBTransaction getTransaction() {
		assertOpen();
		return tx;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.DBPersistenceManager#newSearchQuery(java.lang.Class)
	 */
	public DBQuery newSearchQuery(Class type) {
		assertOpen();
		
		return new SQLQuery(this);
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.DBPersistenceManager#refresh(java.lang.Object)
	 */
	public void refresh(Object obj) {
		assertOpen();
		
		// checkActiveTransaction();
		
		JdbcDAO dao = checkDAOContract(obj);
		
		refreshInternal(dao);
		
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.DBPersistenceManager#refreshAll(java.util.Collection)
	 */
	public void refreshAll(Collection objects) {
		assertOpen();

		JdbcDAO[] daos = checkDAOContract(objects != null ? objects.toArray() : null);
		
		refreshInternal(daos);
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.DBPersistenceManager#save(java.lang.Object)
	 */
	public void save(Object object) {
		assertOpen();
		
		checkActiveTransaction();
		
		JdbcDAO dao = checkDAOContract(object);
		
		registerTransactional(dao);

		saveInternal(dao);
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.DBPersistenceManager#saveAll(java.util.Collection)
	 */
	public void saveAll(Collection objects) {
		assertOpen();
		
		checkActiveTransaction();
		
		JdbcDAO[] daos  = checkDAOContract(objects != null ? objects.toArray() : null);
		
		if(daos != null) {
			registerTransactional(daos);
			
			saveInternal(daos);
		}
	}
	
	private void saveInternal(JdbcDAO dao) {
		dao.getStateManager().markPersistent(dao);
	}
	
	private void saveInternal(JdbcDAO[] daos) {
		for(int i = 0; i < daos.length; i++) {
			saveInternal(daos[i]);
		}
	}

	private void deleteInternal(JdbcDAO dao) {
		dao.getStateManager().markDeleted(dao);
	}
	
	private void deleteInternal(JdbcDAO[] daos) {
		for(int i = 0; i < daos.length; i++) {
			deleteInternal(daos[i]);
		}
	}
	
	private void loadInternal(JdbcDAO dao) {
		dao.getStateManager().load(dao);
	}
	
	private void loadInternal(JdbcDAO[] daos) {
		for(int i = 0; i < daos.length; i++) {
			loadInternal(daos[i]);
		}
	}

	private void refreshInternal(JdbcDAO dao) {
		dao.getStateManager().refresh(dao);
	}
	
	private void refreshInternal(JdbcDAO[] daos) {
		for(int i = 0; i < daos.length; i++) {
			deleteInternal(daos[i]);
		}
	}
	
	boolean isManagedRuntime() {
		return getJTAHelper() != null;
	}
	
	void flushInstances(boolean commit) {
		try {
			Iterator it = dirtyObjects.iterator();
			while(it.hasNext()) {
				JdbcDAO dao = (JdbcDAO)it.next();
				dao.getStateManager().flush(dao,commit);
			}
		}		
		finally {
			//cache.clear();
			dirtyObjects.clear();
		}
	}
	
	void registerTransactional(JdbcDAO dao) {
		if(!dirtyObjects.contains(dao)) {
			if(dao.getStateManager() == null) {
				StateManager sm = new StateManager(dao,this);
				dao.setStateManager(sm);
			}
			dao.getStateManager().markTransactional(dao);
			registerNonTransactional(dao);			
			// Place write lock if Pessimistic tx
			if(!tx.isOptimistic() && lm != null) {
				/*
				EnqueueLockInfo lockInfo = new EnqueueLockInfo();
				lockInfo.setLockType(EnqueueLockManager.WRITE_LOCK);
				try {
					lm.lock(lockInfo,0);
				} catch (LockException e) {

				}
				*/	
			}
			
			dirtyObjects.add(dao);
		}
	}
	
	void registerTransactional(JdbcDAO[] daos) {
		for(int i = 0; i < daos.length; i++) {
			registerTransactional(daos[i]);
		}
	}
	
	/**
	 * This makes object loaded by query to register with PM
	 * @param dao
	 */
	void registerNonTransactional(JdbcDAO dao) {
		if(!cache.containsKey(dao)) {
			this.attach(dao);
			cache.put(dao.getKey(), new SoftReference(dao));
		}
	}
	
	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.jdbc.JdbcPersistenceManager#load(java.lang.Object)
	 */
	public void load(Object o) {
		
		assertOpen();
		
		JdbcDAO dao = checkDAOContract(o);
		
		loadInternal(dao);
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.jdbc.JdbcPersistenceManager#getExceptionTranslator()
	 */
	public ExceptionTranslator getExceptionTranslator() {
		assertOpen();
		return translator;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.jdbc.JdbcPersistenceManager#setExceptionTranslator(com.sap.isa.core.db.jdbc.ExceptionTranslator)
	 */
	public void setExceptionTranslator(ExceptionTranslator translator) {
		assertOpen();
		this.translator = translator;
	}
	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.DBPersistenceManager#makeTransactional(java.lang.Object)
	 */
	public void makeTransactional(Object obj) {
		assertOpen();
		
		checkActiveTransaction();
		
		JdbcDAO dao = checkDAOContract(obj);
		
		registerTransactional(dao);
	}

	/**
	 * @return
	 */
	public Category getLogger() {
		return logger;
	}

	/**
	 * @param category
	 */
	public void setLogger(Category category) {
		logger = category;
	}
	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.DBPersistenceManager#detachQueryResult(com.sap.isa.core.db.DBQueryResult)
	 */
	public void detachQueryResult(DBQueryResult result) {
		// TODO Auto-generated method stub

	}

}
