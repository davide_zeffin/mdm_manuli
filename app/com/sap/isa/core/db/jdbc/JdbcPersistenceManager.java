/*
 * Created on Jul 22, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.core.db.jdbc;

import com.sap.isa.core.db.DBPersistenceManager;
import javax.sql.DataSource;

import com.sap.isa.core.locking.enqueue.EnqueueLockManager;

/**
 * @author I802891
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public interface JdbcPersistenceManager extends DBPersistenceManager {
	
	void load(Object o);
	
	JTAHelper getJTAHelper();
	
	DataSource getDataSource();
	
	SQLUpdateQuery newUpdateQuery();
		
	SQLFunction newSQLFunction();
	
	StoredProcedure newStoredProcedure();
	
	void setLockManager(EnqueueLockManager lockMgr);
	
	EnqueueLockManager getLockManager();
	
	ExceptionTranslator getExceptionTranslator();
	
	void setExceptionTranslator(ExceptionTranslator translator);
}
