/*
 * Created on Aug 6, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.core.db.jdbc;

import java.sql.ResultSet;

/**
 * @author I802891
 *
 */
public interface IndexedRowReader {
	Object read(ResultSet rs, int index);
}
