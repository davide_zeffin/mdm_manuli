/*
 * Created on Aug 6, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.core.db.jdbc;

import java.sql.Types;

/**
 * @author I802891
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class SQLParameter {
	private int jdbcType = Types.JAVA_OBJECT;
	private String name;
	private String typeName;
	/**
	 * @return
	 */
	public int getJdbcType() {
		return jdbcType;
	}

	/**
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return
	 */
	public String getTypeName() {
		return typeName;
	}

	/**
	 * @param i
	 */
	public void setJdbcType(int i) {
		jdbcType = i;
	}

	/**
	 * @param string
	 */
	public void setName(String string) {
		name = string;
	}

	/**
	 * @param string
	 */
	public void setTypeName(String string) {
		typeName = string;
	}

}
