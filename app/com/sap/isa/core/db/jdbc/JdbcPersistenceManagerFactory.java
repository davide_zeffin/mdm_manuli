/*
 * Created on Aug 10, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.core.db.jdbc;

import java.util.HashMap;

import javax.sql.DataSource;

import com.sap.isa.core.db.APIUserException;
import com.sap.isa.core.db.DBPersistenceManager;
import com.sap.isa.core.db.DBPersistenceManagerFactory;
import com.sap.isa.core.db.ResourceUsageException;
import com.sap.tc.logging.Location;

/**
 * @author I802891
 *
 */
public final class JdbcPersistenceManagerFactory implements DBPersistenceManagerFactory {
	
	private static final Location tracer = Location.getLocation(JdbcPersistenceManagerFactory.class.getName());
	
	private DataSource ds = null;
	
	private JTAHelper jta = null;
	
	private boolean useCache = false;
	
	private HashMap txToPM;
	
	private int maxPoolSize = 10;
	
	private int size = 0;
	
	private boolean propertiesSet = false;
	
	public void setDataSource(DataSource ds) {
		this.ds = ds;
	}
	
	public DataSource getDataSource() {
		return ds;
	}
	
	public void setJTAHelper(JTAHelper jta) {
		this.jta = jta;
	}
	
	public JTAHelper getJTAHelper() {
		return jta;
	}
	
	public void setUseCache(boolean useCache) {
		this.useCache = useCache;
	}
	
	public boolean getUseCache() {
		return this.useCache;
	}
	
	public void setMaxPoolSize(int size) {
		this.maxPoolSize = size;
	}
	
	public int getMaxPoolSize() {
		return maxPoolSize;
	}

	private void checkPropertySetAllowed() {
		if(propertiesSet) {
			throw new APIUserException(tracer,APIUserException.PROPERTY_CHANGE_NOT_ALLOWED,null);	
		}
	}
	
	private void afterPropertiesSet() {
		if(!propertiesSet) {
			// do validation of properties
			propertiesSet = true;
		}
		propertiesSet = true;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.DBPersistenceManagerFactory#getPersistenceManager()
	 */
	public synchronized DBPersistenceManager getDBPersistenceManager() {
		if(size < maxPoolSize) {
			JdbcPersistenceManager pm = new JdbcPersistenceManagerImpl(this);
			size++;
			return pm;
		}
		else throw new ResourceUsageException(tracer,ResourceUsageException.RESOURCE_CREATE_FAILED,null);
		
	}
	
	synchronized void returnToPool(JdbcPersistenceManager pm) {
		size--;
	}
}
