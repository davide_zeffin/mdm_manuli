/*
 * Created on Jul 22, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.core.db;

import java.util.HashMap;

/**
 * @author I802891
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public interface DBQuery {
	Class getClassType();
	void setFilter(QueryFilter filter);
	String getFilter();
	void setFilter(String str);
	void setOrderBy(String str, boolean asc);
	void compile();
	DBQueryResult execute(Object[] params);
	DBQueryResult execute(HashMap params);
	void closeAll();
	DBPersistenceManager getPersistenceManager();

}
