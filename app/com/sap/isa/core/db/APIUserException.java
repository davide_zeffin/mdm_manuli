/*
 * Created on Aug 12, 2004
 *
 */
package com.sap.isa.core.db;

import com.sap.localization.LocalizableText;
import com.sap.tc.logging.Location;

/**
 * @author I802891
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class APIUserException extends DBException {



	private static final String ERROR_PREFIX = APIUserException.class.getName() + "_";
	
	public static final String PM_IS_CLOSED = ERROR_PREFIX + 1;
	public static final String NO_ACTIVE_TX = ERROR_PREFIX + 2;	
	public static final String DAO_CONTRACT_NOT_IMPLEMENTED = ERROR_PREFIX + 3;	
	public static final String DAO_KEY_IS_NULL = ERROR_PREFIX + 4;
	public static final String DAO_IS_DIRTY = ERROR_PREFIX + 5;
	public static final String DAO_IS_ALREADY_ATTACHED = ERROR_PREFIX + 6;
	public static final String DAO_IS_ATTACHED_OTHER_PM = ERROR_PREFIX + 7;	
	public static final String QUERY_IS_CLOSED = ERROR_PREFIX + 8;
	public static final String QUERY_IS_COMPILED = ERROR_PREFIX + 9;
	public static final String PROPERTY_CHANGE_NOT_ALLOWED = ERROR_PREFIX + 10;	
	public static final String PROPERTY_CHANGE_ALLOWED_ONLY_ONCE = ERROR_PREFIX + 11;
	public static final String ILLEGAL_OPERATION = ERROR_PREFIX + 12;	
	
	public static final String INVALID_ARGUMENT_VALUE = ERROR_PREFIX + 14;	
	public static final String TX_IS_ACTIVE = ERROR_PREFIX + 15;
	public static final String TX_COMMIT_ROLLBACK_IN_PROGRESS = ERROR_PREFIX + 16;
	public static final String JTA_MANAGED_USER_TX = ERROR_PREFIX + 16;	

	public static final String QUERY_RESULT_IS_ATTACHED = ERROR_PREFIX + 17;	
	public static final String QUERY_PARAM_NOT_DECL = ERROR_PREFIX + 17;	
	public static final String QUERY_PARAM_MISSING = ERROR_PREFIX + 18;
	public static final String QUERY_PARAM_COUNT_MISMATCH = ERROR_PREFIX + 19;
	public static final String DAO_IS_DELETED = ERROR_PREFIX + 20;
	
	public static final String BEFORE_SAVE_FAILED = ERROR_PREFIX + 21;
	public static final String BEFORE_DELETE_FAILED = ERROR_PREFIX + 22;	
	public static final String AFTER_LOAD_FAILED = ERROR_PREFIX + 23;	
	
	public static final String PROPERTY_NOT_SET = ERROR_PREFIX + 24;	
	
	/**
	 * @param arg0
	 */
	public APIUserException(Location arg0) {
		super(arg0);
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public APIUserException(Location arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public APIUserException(Location arg0, LocalizableText arg1) {
		super(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 */
	public APIUserException(
		Location arg0,
		LocalizableText arg1,
		Throwable arg2) {
		super(arg0, arg1, arg2);
	}

	/**
	 * @param arg0
	 * @param arg2
	 * @param arg3
	 */
	public APIUserException(Location arg0, String arg2, Object[] arg3) {
		super(arg0, arg2, arg3);
	}

	/**
	 * @param arg0
	 * @param arg2
	 * @param arg3
	 * @param arg4
	 */
	public APIUserException(
		Location arg0,
		String arg2,
		Object[] arg3,
		Throwable arg4) {
		super(arg0, arg2, arg3, arg4);
	}
}
