/*
 * Created on Jul 22, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.core.db;

import java.util.Collection;

/**
 * @author I802891
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public interface DBPersistenceManager {
	void save(Object object);
	void saveAll(Collection objects);
	void delete(Object obj);
	void deleteAll(Collection obj);
	Object getObjectById(Object id);
	DBQuery newSearchQuery(Class type);
	void attachQueryResult(DBQueryResult result);
	void detachQueryResult(DBQueryResult result);
	void refresh(Object obj);
	void refreshAll(Collection objects);
	void attach(Object obj);
	void detach(Object obj);
	void close();
	void makeTransactional(Object obj);
	DBTransaction getTransaction();
}
