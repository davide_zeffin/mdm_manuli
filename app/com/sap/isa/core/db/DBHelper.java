/*
 * Created on Sep 15, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.core.db;

import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import sqlj.runtime.ConnectionContext;

import com.sap.isa.core.Constants;
import com.sap.isa.core.db.jdbc.DriverManagerDataSource;
import com.sap.isa.core.db.jdbc.JdbcPersistenceManagerFactory;
import com.sap.isa.core.db.jdo.JDOPersistenceManagerFactory;
import com.sap.isa.core.init.InitializationHandler;
import com.sap.isa.core.locking.LockManagerFactory;
import com.sap.isa.core.locking.enqueue.EnqueueLockManagerFactory;
import com.sap.isa.core.sqlj.CrmJdbcPooled;
import com.sap.isa.core.sqlj.CrmJdbcStandalone;
import com.sap.isa.core.util.MiscUtil;
import com.sap.tc.logging.Location;

/**
 * @author I802891
 */
public class DBHelper {
	public static final String DB_PMF = "isa.core.db.persistenceManagerFactory";
	public static final String DB_DATASOURCE = "isa.core.db.dataSource";
	public static final String DB_USER_NAME = "isa.core.db.userName";
	public static final String DB_PWD = "isa.core.db.password";
	public static final String DB_DRIVER = "isa.core.db.driver";
	public static final String DB_URL = "isa.core.db.url";
	public static final String DB_JNDI_JDOPMF = "isa.core.db.jdoJNDI";
	public static final String DB_TYPE = "isa.core.db.type";
	public static final String LMF = "isa.core.locking.lockManagerFactory";
	
	private static final Location tracer = Location.getLocation(DBHelper.class.getName());
	
	private DBHelper() {
	}
	
	static Properties alternatePersistencyProperties = null;
	/**
	 * This method sets alternative connection parameters for the
	 * getPersistenceManagerFactory method. If the lookup of the JDBC connection via JNDI fails
	 * and the properties are set via this method, the getPersistenceManagerFactory tries to 
	 * connect to the database using the alternate connection parameters 
	 * @param props
	 */
	public static void setPersistencyProperties(Properties props)
	{
		alternatePersistencyProperties=props;
	}
	static private DataSource getDsByProperties(Properties props)
	{
		String userName = getMandatoryProperty(props,DB_USER_NAME);
			String pwd = getMandatoryProperty(props,DB_PWD);
			String driver = getMandatoryProperty(props,DB_DRIVER);
			String url = getMandatoryProperty(props,DB_URL);
								
			userName = userName.trim();
			pwd = pwd.trim();
			driver = driver.trim();
			url = url.trim();
			return new DriverManagerDataSource(driver,url,userName,pwd);
	}
	public static DBPersistenceManagerFactory getPersistenceManagerFactory(Properties props) {
		String pmfClassName = getMandatoryProperty(props,DB_PMF);
		
		DBPersistenceManagerFactory pmf = null;
		
		if(pmfClassName.equals(JdbcPersistenceManagerFactory.class.getName())) {
			JdbcPersistenceManagerFactory jdbcPMF = new JdbcPersistenceManagerFactory();
			String dsName = props.getProperty(DB_DATASOURCE);
			
			DataSource ds = null;
			if(dsName != null && dsName.trim().length() > 0) {
				InitialContext ctxt;
				try {
					ds = getApplicationSpecificDataSource(dsName);
				} catch (NamingException e) {
					if(alternatePersistencyProperties!= null)
					{
						ds = getDsByProperties(alternatePersistencyProperties);
					}
					else
						throw new DBException(tracer,DBException.INTERNAL_ERROR,new String[]{e.getMessage()},e);
				}
			}
			else{
				ds = getDsByProperties(props);	
			}
			
			jdbcPMF.setDataSource(ds);
			pmf = jdbcPMF;		
		}
		else if(pmfClassName.equals(JDOPersistenceManagerFactory.class.getName())) {
			pmf = new JDOPersistenceManagerFactory(null);
		}
		else {
			throw new APIUserException(tracer,APIUserException.INVALID_ARGUMENT_VALUE,new String[]{DB_PMF});
		}
		
		return pmf;
	}
	/**
	 * 
	 * @param dsName Name of the Data Source
	 * Returns the one And Only dataSource for the application
	 * 
	 * The datasource must be unique to the application, because a deployment of an application which uses
	 * using the same datasource name as another application would cause a restart of both applications
	 * The name will be built from the com.sap.isa.core.Constants.DATASOURCE_LOOKUP_NAME + "." + application name from web.xml
	 * 
	 * As a fallback the datasource given by the name passed to this method will be retrieved. 
	 * @return the retrieved Datasource
	 * @throws NamingException
	 */
	public static DataSource getApplicationSpecificDataSource(String dsName) throws NamingException
	{
			InitialContext ctxt = new InitialContext();
			try{
				return (DataSource)ctxt.lookup(Constants.DATASOURCE_LOOKUP_NAME + "/" + InitializationHandler.getApplicationName());
			}
			catch( NamingException ex)
			{
				if(dsName != null )
				{
					try {
						return (DataSource)ctxt.lookup(dsName);
					}
					catch( NamingException ex1)
					{
						if(alternatePersistencyProperties!= null)
						{
							return getDsByProperties(alternatePersistencyProperties);
						}
					}
				}
				throw ex;
			}
	}
	public static LockManagerFactory getLockManagerFactory(Properties props) {
		LockManagerFactory lmf = null;
		
		String lmfClassName = getMandatoryProperty(props,LMF);
		
		if(lmfClassName.equals(EnqueueLockManagerFactory.class.getName())) {
			lmf = new EnqueueLockManagerFactory();
		}
		else {
			throw new APIUserException(tracer,APIUserException.INVALID_ARGUMENT_VALUE,new String[]{DB_PMF});
		}		
		return lmf;
	}
	
	public static DataSource getDataSource(Properties props) {
		String dsName = null;
		if( props != null )
		   dsName = props.getProperty(DB_DATASOURCE);
			
		DataSource ds = null;
		if(props == null ||( dsName != null && dsName.trim().length() > 0)) {
			InitialContext ctxt;
			try {
				ds = getApplicationSpecificDataSource(dsName);
			} catch (NamingException e) {
				if(alternatePersistencyProperties!= null)
				{
					ds = getDsByProperties(alternatePersistencyProperties);
				}
				else
					throw new DBException(tracer,DBException.INTERNAL_ERROR,new String[]{e.getMessage()},e);
			}
		}
		else {
			String userName = getMandatoryProperty(props,DB_USER_NAME);
			String pwd = getMandatoryProperty(props,DB_PWD);
			String driver = getMandatoryProperty(props,DB_DRIVER);
			String url = getMandatoryProperty(props,DB_URL);
								
			userName = userName.trim();
			pwd = pwd.trim();
			driver = driver.trim();
			url = url.trim();
			ds = new DriverManagerDataSource(driver,url,userName,pwd);
		}
		return ds;
	}
	
	private static String getMandatoryProperty(Properties props, String prop) {
		String val = props.getProperty(prop);
		
		if(val == null) {
			APIUserException ex = new APIUserException(tracer,APIUserException.PROPERTY_NOT_SET,new String[]{prop});
			throw ex;
		}
		
		return val.trim();	
	}
	public static ConnectionContext getSQLJContext() throws DBException {
	  ConnectionContext con = null;
	  try {
		 con = new CrmJdbcPooled();
	   } catch (SQLException ex) {
		   String poolname = com.sap.isa.core.Constants.DATASOURCE_LOOKUP_NAME + "/" + com.sap.isa.core.init.InitializationHandler.getApplicationName();
		   String msg = "Could not create pooled SQLJ connection context for [pool]='"+ poolname + "'";
		   try {
				Connection conStandalone = DBHelper.getDirectOpenSqlCon(alternatePersistencyProperties);
				return new CrmJdbcStandalone(conStandalone);
			}  catch (SQLException ex2) {
//				if( log.isDebugEnabled())
//					log.debug("Unable to connect to standalone JDBC connection " + ex2.getMessage());
				throw new DBException(tracer,DBException.INTERNAL_ERROR,new String[]{ex.getMessage()},ex);
			}
	   }
	   return con;  	
	}
	
	/**
	 * Opens a JDBCConnection
	 * Limitation: Currently 
	 * @param props: jdbc connection parameter (url, user, password ...)
	 * @return Open/SQL jdbc connection to maxdb database
	 */
	  public static Connection getDirectOpenSqlCon(Properties props) throws DBException {
	  String dbclass = props.getProperty(DB_DRIVER);
	  String dburl = props.getProperty(DB_URL);
	  String user = props.getProperty(DB_USER_NAME);
	  String password = props.getProperty(DB_PWD);
	  String dbtype = props.getProperty(DB_TYPE);
	  if( dbtype == null)
	  	dbtype="SAPMAXDB";
	  try {
		Class osdsClass = Class.forName("com.sap.sql.connect.OpenSQLDataSource");
		  Method newInstance= osdsClass.getDeclaredMethod("newInstance",null);
		  Object osds = newInstance.invoke(null,null);
		  MiscUtil.callMethod(osds,"setDriverProperties", 
			new Class[] {String.class,String.class,String.class,String.class},
			new Object[]{dbclass,          
				dburl, 
				dbtype,                                 
				"SAP"}
			);
		  //	osds.setConnectionType(OpenSQLDataSource.CONN_TYPE_OPEN_SQL);
		  MiscUtil.callMethod(osds,"setConnectionType", new Class[]{int.class}, new Object[]{new Integer(1)});
		  return (Connection)MiscUtil.callMethod(osds, "getConnection", new Class[]{String.class,String.class}, new Object[]{user,password});
		  /*	 Same code without reflection:
			  OpenSQLDataSource osds= OpenSQLDataSource.newInstance();
		  osds.setDriverProperties(dbclass,          
				dburl, 
				"SAPMAXDB",                                 
				"SAP");
		  osds.setConnectionType(OpenSQLDataSource.CONN_TYPE_OPEN_SQL);
		  Connection con=osds.getConnection(user,password);
		  return con; 
		  */
	  }
		  catch (Exception ex) {
		   throw new DBException(tracer,DBException.INTERNAL_ERROR,new String[]{ex.getMessage()},ex);
	  }
  }
  static public Properties getAlternateJDOProperties()
  {
  	if(alternatePersistencyProperties == null )
  		return null;
	Properties jdoProps = new Properties();
	jdoProps.setProperty("javax.jdo.option.ConnectionDriverName",alternatePersistencyProperties.getProperty(DB_DRIVER));
	jdoProps.setProperty("javax.jdo.option.ConnectionURL",alternatePersistencyProperties.getProperty(DB_URL));
	jdoProps.setProperty("javax.jdo.option.ConnectionUserName",alternatePersistencyProperties.getProperty(DB_USER_NAME));
	jdoProps.setProperty("javax.jdo.option.ConnectionPassword",alternatePersistencyProperties.getProperty(DB_PWD));
    return jdoProps;
  }
}
