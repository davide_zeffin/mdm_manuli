/*
 * Created on Aug 12, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.core.db;

import com.sap.localization.LocalizableText;
import com.sap.tc.logging.Location;

/**
 * @author I802891
 *
 */
public class DuplicateKeyException extends DBException {

	/**
	 * @param arg0
	 */
	public DuplicateKeyException(Location arg0) {
		super(arg0);
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public DuplicateKeyException(Location arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public DuplicateKeyException(Location arg0, LocalizableText arg1) {
		super(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 */
	public DuplicateKeyException(
		Location arg0,
		LocalizableText arg1,
		Throwable arg2) {
		super(arg0, arg1, arg2);
	}

	/**
	 * @param arg0
	 * @param arg2
	 * @param arg3
	 */
	public DuplicateKeyException(Location arg0, String arg2, Object[] arg3) {
		super(arg0, arg2, arg3);
	}

	/**
	 * @param arg0
	 * @param arg2
	 * @param arg3
	 * @param arg4
	 */
	public DuplicateKeyException(
		Location arg0,
		String arg2,
		Object[] arg3,
		Throwable arg4) {
		super(arg0, arg2, arg3, arg4);
	}
}
