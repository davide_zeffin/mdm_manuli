/*
 * Created on Jul 22, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.core.db;

/**
 * @author I802891
 * <p>
 * Results from a search method returns QueryResult
 * This object is a logical map to SQL Resultset.
 * QueryResult mostly return Java Objects matching the Query Criteria but may also
 * returns rows of ResultSet if used in native mode
 * <b>QueryResult are read Only</b><br/>
 * Behaves in 2 Modes<br/>
 * - Forward Only Cursor<br/>
 * - Scrollable Cursor<br/>

 * For Value Queries corresponding Object Represenatations of Column Values
 * are returned. For Multiple Column Value Queries, Object[] is returned
 * </p>
 * <p>
 * QueryResult behaves like an Iterator with support for random positioning in the
 * result set. This class is not thread safe. 
 * Execute query and retreive a new QueryResult for each thread
 * </p>
 */
public abstract class DBQueryResult {
	public static final int DEFAULT_FETCH_SIZE = 10;	

	protected int fetchSize = DEFAULT_FETCH_SIZE;
	protected int pos = 1; // position start at 1
	protected boolean closed = false;
	protected DBQuery query = null;
	
	protected DBQueryResult(DBQuery query) {
		this.query = query;
	}


	public DBQuery getDBQuery() {
		return query;
	}
	
	protected void usageCheck() {
		if(closed) throw new IllegalStateException("QueryResult is closed");
	}

	/**
	 * Sets the Fetch Size for retreival by Fetch {@link #fetch}
	 */
	public void setFetchSize(int size) {
		usageCheck();
		if(size <= 0) throw new IllegalArgumentException("Invalid fetch size: " + size);
		if(size > size()) {
			size = size();
		} 
		this.fetchSize = size;
	}

	/**
	 *
	 */
	public int getFetchSize() {
		usageCheck();
		return fetchSize;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isDetached() {
		return false;
	}

	/////////////////////////////////////////////////////
	// Minimum Implementation Contract	
	/**
	 * @return the Total no of Result Records obtained by Query execution
	 */
	public abstract int size();

	/**
	 * @return Object
	 */
	public abstract Object next();
	
	/**
	 * @param pos
	 */
	protected abstract void doPosition(int pos);
	//
	//////////////////////////////////////////////////////
	
	/**
	 * If more matching Objects are available
	 */
	public boolean hasNext() {
		return pos <= size();
	}
	

	/**
	 * QueryResult is placed at position 0 initially i.e. before the first search element
	 * Positions by absolute pos. For random positioning in resultset
	 */
	public void position(int pos) {
		if(pos > size()) throw  new IllegalArgumentException("Invalid position = " + pos + " : Result size = " + fetchSize);
		
		doPosition(pos);
		
		this.pos = pos;
	}


	
	/**
	 * @return int the current Absolute Pos
	 */
	public int getPosition() {
		return pos;
	}
	
	
	/**
	 * Retreives the objects based on set fetch size
	 */
	public Object[] fetch() {
		Object[] result = new Object[fetchSize];
		int i = 0;
		while(hasNext() && i < fetchSize) {
			result[i++] = next();
		}
		return result;
	}

	/**
	 * <p>
	 * Retreives the next <code>count</code> searched objects from result
	 * Converts the Search into Value Beans for Usage by UI  Layer
	 * </p>
	 *
	 * @param count number of next objects to retreive begining current position.
	 * @return Object[] fetched objects , Object[].length <= count
	 */
	public Object[] fetch(int count) {
		if(count < 1) throw new IllegalArgumentException("arg count in invalid, value : " + count);
		int oldFetch = getFetchSize();
		setFetchSize(count);
		Object[] result = fetch();
		setFetchSize(oldFetch);
		return result;
	}

	/**
	 * Closes an existing QueryResult and releases the underlying resources
	 * Close on an already closed result does nothing. QueryResult must be
	 * closed after usage, otherwise it will lead to a resource leakage
	 */
	public void close() {
		if(closed) return;
		closed = true;
	}
    
	public boolean isClosed()  {
		return closed;
	}	
}
