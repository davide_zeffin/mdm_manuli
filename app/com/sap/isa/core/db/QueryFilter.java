/*
 * Created on Jul 22, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.core.db;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author I802891
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class QueryFilter implements Serializable {
	/**
	 * Logical Operator
	 */
	public static final int NONE = 0;	
	public static final int AND = 1;
	public static final int OR = 2;
	public static final int NOT = 3;
	
	private int operator = NONE; // => not nested
	
	private ArrayList elements = new ArrayList();	
	
	public void and(QueryFilter other) {

	}
	
	public void or(QueryFilter other) {

	}

	public int getOperator() {
		return operator;
	}
	
	public void not() {
	}
	
	public void addCriteria(Criteria crit) {
		if(!elements.contains(crit)) {
			elements.add(crit);
		}
	}
	
	public List getCriterias() {
		return elements;
	}
	
	public static class Criteria {
		private String paramName;
		private Object value;
		private int operator = ET;
		
		public static final int ET = 0;
		public static final int NOT_ET = 1;
		public static final int GT = 2;
		public static final int GT_ET = 3;
		public static final int LT = 4;
		public static final int LT_ET = 5;
		
		public void setParameterName(String name) {
			this.paramName = name;		
		}
		
		public String getParameterName() {
			return paramName;
		}
		
		public void setValue(Object val) {
			this.value = val;
		}
		
		/**
		 * Can be a literal or variable, if param it must follow
		 * convention ?[<variable_name>] 
		 * @return
		 */
		public Object getValue() {
			return value;
		}
		
		public int getOperator() {
			return operator;
		}
		
		void setOperator(int operator) {
			this.operator = operator;
		}
		
		boolean evaluate(Object lhval, Object rhval) {
			return false;
		}
	}
}
