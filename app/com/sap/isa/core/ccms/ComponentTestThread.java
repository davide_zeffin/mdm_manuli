package com.sap.isa.core.ccms;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.test.ComponentTest;

/**
 *  The run method of this Thread executes a test defined by
 *  componentKey, testClass and testName and updates the testResults.
 *
 *  The class used to implement a timeout for ComponentTest in CCMS Heartbeat scenarios.
 */
public class ComponentTestThread extends Thread {

  	protected static IsaLocation log = IsaLocation.
        getInstance(ComponentTestThread.class.getName());

	ComponentTest testClass;
	String componentKey;
	String testName;
	/**
	 * Constructor for ComponentTestThread.
	 * @param name Name of the component to test (used for log messages)
	 */
	public ComponentTestThread( String componentKey) {
		super( "ComponentTest-" + componentKey);
		this.componentKey=componentKey;
	}
	public void setTestClass(ComponentTest testClass)
	{
		this.testClass = testClass;
	}
	public void setTestName(String testName)
	{
		this.testName = testName;
	}

	/**
	 * Executes all component tests for a single component
	 * and fills the result of the test into testProperties
	 * 
	 * @see java.lang.Runnable#run()
	 */
	public void run() {
		
		if( testClass == null )
		{
			log.error(LogUtil.APPS_BUSINESS_LOGIC,"ccms.no.testclass.found", new Object[]{ componentKey, "" }, null );
			return;
		}
		try{
		if( testName != null )
			testClass.performTest(testName);
		else
			testClass.performTests();			
		}catch( Exception ex )
		{
			log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.exception",new Object[]{ex.getMessage()}, ex);
		}
	}
	/**
	 * Sets the timeout flag in the testClass
	 */
	public void setTimeout()
	{
		testClass.setTimeout();
	}
}//class

