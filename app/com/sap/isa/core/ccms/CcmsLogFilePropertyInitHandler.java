package com.sap.isa.core.ccms;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.Properties;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import com.sap.isa.core.EnvironmentManager;
import com.sap.isa.core.init.Initializable;
import com.sap.isa.core.init.InitializationEnvironment;
import com.sap.isa.core.init.InitializeException;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.xcm.init.ExtendedConfigInitHandler;
import com.sap.isa.core.xcm.init.ParamsConfig;
import com.sap.isa.core.xcm.xml.ParamsStrategy;
import com.sap.isa.core.xcm.xml.XMLUtil;

/**
 *
 */
public class CcmsLogFilePropertyInitHandler implements Initializable {
  protected static IsaLocation log = IsaLocation.
        getInstance(CcmsLogFilePropertyInitHandler.class.getName());
 
	/**
	 * Constructor for CcmsLogFilePropertyInitHandler.
	 */
	public CcmsLogFilePropertyInitHandler() {
		super();
	}

	/**
	 * @see com.sap.isa.core.init.Initializable#initialize(InitializationEnvironment, Properties)
	 */
	public void initialize(InitializationEnvironment env, Properties props)
		throws InitializeException {
			
		final String METHOD = "initialize()";
		log.entering(METHOD);
		try {
		
			if( EnvironmentManager.isSAPJ2EE620()) {
				try{
				       String ccmsDir =props.getProperty("ccmsLogmonDirectory");
					   if( ccmsDir==null || ccmsDir.length() == 0 )
					   {
						   if(log.isDebugEnabled() == true)
						   {
							  log.debug("CCMS Log Property File will not be created, because there is no ccmsLogmonDirectory defined in XCM" );
						   }
						 return;
					   }
					   String logPropName = props.getProperty("ccmsLogPropertyFilename");
					   String templateName = props.getProperty("logPropertyTemplate");
					   if(log.isDebugEnabled() == true)
					   {
						  log.debug("Generating CCMS Log Properties File: " + ccmsDir + "/" + logPropName + " template:" + templateName );
					   }
					   File ccmsLogFile = new File(ccmsDir, logPropName);
					   File template = new File(env.getRealPath(templateName));
					   ParamsConfig params=ExtendedConfigInitHandler.getParamsConfig();
					   String applicationName="";
					   if( params != null)
					   {
							ParamsStrategy.Params p= params.getParamsContainer();
							applicationName=p.getParamValue("application.name");
					   }
					   createLogPropertiesFile( template, ccmsLogFile, env.getRealPath("/"), applicationName);
					}
					catch(Exception ex)
					{
						log.error(LogUtil.APPS_BUSINESS_LOGIC,"ccms.logproperties.error", ex);
					}
			   }
			   else
			   {
			   	 if( log.isDebugEnabled())
			   	  	log.debug("Writing no log file monitoring config file, because the Servlet Container is no SAP J2EE Engine 6.20");
			   }
		} finally {
			log.exiting();
		}
	}

	/**
	 * Method createLogPropertiesFile. Creates a ccmsLogFile properties file.
	 * This file will be used by the CCMS Agent.
	 * @param logFileTemplate
	 * @param ccmsLogFile
	 * @param applicationContextRoot
	 */
	public void createLogPropertiesFile(
		File logFileTemplFile,
		File ccmsLogFile,
		String applicationContextRoot,
		String applicationName) throws TransformerConfigurationException, TransformerException {
		ByteArrayInputStream is	= new  ByteArrayInputStream("<xml/>".getBytes());
		StreamResult res= new StreamResult(ccmsLogFile);
		if(logFileTemplFile.exists())
		{
				if( log.isDebugEnabled())
					log.debug("ccms log property File Template exists:" + logFileTemplFile.getAbsolutePath() );
				if( !ccmsLogFile.exists() || logFileTemplFile.lastModified() > ccmsLogFile.lastModified())
				{   
				    
					Transformer tf=XMLUtil.getTransformerFactory(this).newTransformer(new StreamSource(logFileTemplFile));
					tf.setParameter("application-context-root", applicationContextRoot);
					tf.setParameter("application-name", applicationName);
					tf.transform(new StreamSource(is), res);
					log.info("ccms.customize.logWrite.success", new Object[]{ccmsLogFile.getAbsolutePath()}, null);
				}
				else
				{
					if( log.isDebugEnabled())
						log.debug(ccmsLogFile.getAbsolutePath() +" is up to date");
				}
		}
		else
		{
			if( log.isDebugEnabled())
				log.debug("ccms log property File Template does not exist " + logFileTemplFile.getAbsolutePath());
		}
	}


	/**
	 * @see com.sap.isa.core.init.Initializable#terminate()
	 */
	public void terminate() {
	}

}
