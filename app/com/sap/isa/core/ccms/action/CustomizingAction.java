package com.sap.isa.core.ccms.action;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
/*
 * @todo jstartupFramework
 */
 //import com.sap.bc.proj.jstartup.JStartupFramework;

import com.sap.isa.core.BaseAction;
import com.sap.isa.core.ccms.HeartbeatCustomizingForm;
import com.sap.isa.core.config.AdminConfig;
import com.sap.isa.core.init.InitializationEnvironment;
import com.sap.isa.core.interaction.InteractionConfig;
import com.sap.isa.core.interaction.InteractionConfigContainer;
import com.sap.isa.core.util.WebUtil;

/**
 * CustomizingAction generates a CCMS Heartbeat customizing xml File
 * it can be uploaded to CCMS via GRMG transaction.
 * 
 * The action uses the following request parameter:
 * 1. ccmsDir
 * 1. generate: If the parameter generate is set, the XML file will be created
 *    and the action forwards to "result"
 *    Otherwise, the HeartbeatCustomizingForm will be initialized and the action
 *    forwards to "showcustomizing"
 * 2. scenarios[]: Array containing the selected scenarios
 * 
 * @see com.sap.isa.core.ccms.HeartbeatCustomizingForm
 */
public class CustomizingAction extends BaseAction {
	
  public CustomizingAction()  {
  }
  /**
   * @see com.sap.isa.core.BaseAction#doPerform(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)
   */
  public ActionForward doPerform(ActionMapping actionMapping, ActionForm actionForm, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
      // check if file download is allowed
	final String METHOD_NAME = "doPerform()";
	log.entering(METHOD_NAME);  
	
    if (!AdminConfig.isCcmsHeartbeatCustomize()) {
    	log.exiting();
		throw new ServletException("This Operation is Forbidden");
    }
        
    HeartbeatCustomizingForm form=(HeartbeatCustomizingForm) actionForm;
  	
	InteractionConfigContainer interactionContainter = getInteractionConfig(request);;
	try{ 
	    if( request.getParameter("generate") != null &&  AdminConfig.isXCMAdmin(request))
	    {
	       String ccmsDirectory= form.getCcmsdir();
	       File ccmsDir= new File(ccmsDirectory);
	       if( !ccmsDir.exists())
	       	   ccmsDir.mkdirs();
	       URL f= new URL(form.getUrl());
	       String port="";
	       if( f.getPort() != -1)
	       	  port = "_" + Integer.toString(f.getPort());
		   File ccmsHeartbeatFile = new File(ccmsDirectory, 
			   	"GRMG_" + f.getHost()+ port + ".xml" );
		   if(log.isDebugEnabled() == true)
		   		log.debug("Generating Customizing XML File: " + ccmsHeartbeatFile.getAbsolutePath());
		   setSelectedScenarios(form, request);
		   form.createCustomizingXmlFile( ccmsHeartbeatFile );

	       return actionMapping.findForward("result");
	    }
	    if( !form.isInitialized())
	    {
	         form.init();
			 String ccmsDirectory = null;
			 try{
					Class jFramework = Class.forName("com.sap.bc.proj.jstartup.JStartupFramework");
					  Class paramC[]=new Class[] {String.class};
					  Object param[]=new Object[] {"DIR_PERF"};
					  Method getParamMethod = jFramework.getDeclaredMethod("getParam",paramC);
					  String dir= (String) getParamMethod.invoke(null, param);
						
				 	ccmsDirectory = new File(dir, "grmg").getAbsolutePath();
				}
			catch(Throwable ex)
				{
					if(log.isDebugEnabled())
						log.debug("Couldn't retrieve grmg directory from J2EE 6.30 engine");
				}
		     
			InteractionConfig ccmsXsl= interactionContainter.getConfig("ccmsHeartbeat");
			if( null != ccmsXsl && null !=  ccmsXsl.getValue("ccmsCustomizingXsl"))
			{
				String xslFile= WebUtil.getAbsolutePath(ccmsXsl.getValue("ccmsCustomizingXsl"), (InitializationEnvironment) getServlet());
			 	form.setCustomizingXslFile( xslFile );
			}
		    if( ccmsDirectory != null)
				 form.setCcmsdir(ccmsDirectory);
			 else
				 form.setCcmsdir("");
	         form.setUrl( "http://" + request.getServerName() 
                          + ":" + request.getServerPort() 
                          + request.getContextPath());                          
	    }
	    return actionMapping.findForward("showcustomizing");
	}
	catch( Exception ex )
	{ 
		if( log.isDebugEnabled())
		{
			ex.fillInStackTrace();
			log.debug( "Exception thrown in CustomizingAction", ex );
		}
		form.setMessage(ex.getMessage());
	    return actionMapping.findForward("result");
	} finally {
		log.exiting();
	}
  }
  /**
   * Bypass for a struts problem:
   * Sometimes the form bean mechanism doesn't work for indexed properties
   * That's why they need to be set manually here:
   * @param form
   * @param request
   */
  private void setSelectedScenarios(HeartbeatCustomizingForm form, HttpServletRequest request) {
	  Enumeration e= request.getParameterNames();
	  while( e.hasMoreElements())
	  {
		 String name=(String)e.nextElement();
		 if( name.startsWith("scenariosSelected["))
		 {
			int idx=Integer.parseInt(name.substring("scenariosSelected[".length(), name.indexOf(']')));
			form.setScenariosSelected(idx,"on");
			if( log.isDebugEnabled())
			   log.debug("scenariosSelected[" + idx + "] was selected");
		 }
		if( name.startsWith("componentSelected["))
		{
		   int idx=Integer.parseInt(name.substring("componentSelected[".length(), name.indexOf(']')));
		   if(request.getParameter(name)!= null && request.getParameter(name).length() != 0)	
			   	form.setComponentSelected(idx,"on");
		   else
		   		form.setComponentSelected(idx,"");
		   
		   if( log.isDebugEnabled())
			  log.debug("componentSelected[" + idx + "] was selected");
		}
	  }	
  }

}