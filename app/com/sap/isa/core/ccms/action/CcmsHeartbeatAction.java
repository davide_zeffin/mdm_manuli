package com.sap.isa.core.ccms.action;

import java.io.IOException;
import java.net.InetAddress;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;

import com.sap.isa.core.BaseAction;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.test.ComponentTest;
import com.sap.isa.core.xcm.ConfigContainer;
import com.sap.isa.core.xcm.XCMTestProperties;
import com.sap.isa.core.xcm.XCMUtils;
import com.sap.isa.core.xcm.init.XCMAdminInitHandler;
import com.sap.util.monitor.grmg.GrmgComponent;
import com.sap.util.monitor.grmg.GrmgProperty;
import com.sap.util.monitor.grmg.GrmgRequest;
import com.sap.util.monitor.grmg.GrmgRequestException;
import com.sap.util.monitor.grmg.GrmgScenario;




/**
 *
 * This class receives a request from a CCMS - System, reads the datas enclosed,
 * launchs XCM tests and sends the results of the tests back to the CCMS - System.
 */

public class CcmsHeartbeatAction extends BaseAction{

	private static final String GRMG_REQUEST = "grmg_request";
	private static final String R3_MSG_CLASS = "RT";
	public static final String SCENARIO_TESTS = "scenario-tests";
	
	public ActionForward doPerform(
						        ActionMapping mapping,
        						ActionForm form,
        						HttpServletRequest request,
        						HttpServletResponse response)
    {  	
		final String METHOD_NAME = "doPerform()";
		log.entering(METHOD_NAME);
		String forwardTo="";
		String errorMSG="";
		UserSessionData userSessionData = 
	   		UserSessionData.getUserSessionData(request.getSession());
    		
    	try{
	    	GrmgRequest grmgReq = new GrmgRequest(request.getInputStream());
	    	ConfigContainer configContainer = (ConfigContainer) userSessionData.getAttribute(SessionConst.XCM_CONFIG);

			
			String xcmTestScenario = configContainer.getScenarioName();
			if(log.isDebugEnabled() == true)
				log.debug("START HeartbeatAction for scenario "+ xcmTestScenario);
								
			if(log.isDebugEnabled() == true)
				log.debug("Scenario for XCM - Tests is: " + xcmTestScenario);
			Set additionalTests = (Set)userSessionData.getAttribute(CcmsHeartbeatAction.SCENARIO_TESTS );
			if( executeTests(grmgReq, xcmTestScenario, configContainer, additionalTests))
				forwardTo="success";
			else
				forwardTo="failure";
			userSessionData.setAttribute(GRMG_REQUEST, grmgReq);
			
			if(log.isDebugEnabled())
				log.debug("end of HeartbeatAction");
			
	        return mapping.findForward(forwardTo);

    	}//try
    	catch(GrmgRequestException e){
    		log.error(LogUtil.APPS_USER_INTERFACE,"system.exception" ,e);
    	  	userSessionData.setAttribute("failure", "CCMS GRMG request error:" + e);
    	  	return mapping.findForward("failure");
    	}    	
    	catch(IOException e){
    	  	log.error(LogUtil.APPS_USER_INTERFACE,"system.exception" , e);
    	  	userSessionData.setAttribute("failure", "Exception occured in HeartbeatAction: " + e);
    	  	return mapping.findForward("failure");
    	} finally {
    		log.exiting();
    	}
    }//doPerform


	/**
	 * Method executeTests executes all tests requested by the
	 * grmgReq and fills the result messages into the grmgrequest
	 * 
	 * @param grmgReq GrmgRequest created from InputStream
	 * @param xcmTestScenario ScenarioName
	 * @param xcmAdm Path and name of the xcmadmin-config.xml file
	 * @param scenarioConfig ScenarioConfig processed by XCM
	 * @param configData ConfigData processed by XCM
	 * @param url URL to this local host
	 *
	 * @return boolean
	 * If all tests are OK, the method returns true. 
	 * Otherwise it returns false
	 */
	public boolean executeTests(GrmgRequest grmgReq,
		String xcmTestScenario, 
		ConfigContainer configContainer,
		Set additionalTests
	)
	{
   		GrmgScenario scenario = grmgReq.getScenario();			  	   	
    	GrmgComponent component=null;
    	GrmgComponent webappGrmgComponent=null;
    	String xcmConfigData=XCMAdminInitHandler.getConfigData(1);
		Document configData = configContainer.getConfigUsingAliasAsDocument("config-data"); 

		boolean returncode=true;
    	XCMUtils xcmUtils = new XCMUtils();				    	
    	Map testList=null;
    	Set components = new TreeSet();
    	for( int k=0; k < scenario.getNumberOfComponents(); k++)
    	{
    		component = scenario.getComponent(k);
    		if (component == null) {
    			log.error(LogUtil.APPS_USER_INTERFACE,"system.ccms.error", new Object[] { "component [number='" + k + "'] of [scenario]='" + scenario.getName() + "' null" }, null);
    			continue;
    		}
    		
    		if( component.getName().equalsIgnoreCase("webapp"))
    			webappGrmgComponent = component;
    		else
    		{
				GrmgProperty id = component.getPropertyByName("componentId");
				String compid = "";
				if (id == null ) {
					XCMTestProperties addTest = findAdditionalTest(component.getName(), additionalTests);
					if(addTest != null )
						compid = addTest.getConfigID();
					else
					{
						log.error(LogUtil.APPS_USER_INTERFACE,"system.ccms.error", new Object[] { 
							"componentId property null in Scenario " + scenario.getName() + " grmg component: " + component.getName() + ". The component was not configured in XCM. Either remove it in the GRMG scenario or configure it in XCM" 
							}, null);
						createError("The component was not configured in XCM.", component);	
						continue;
					}
				}
				else
    				compid=id.getValue();    			
				GrmgProperty compName=component.getPropertyByName("componentName");
				if( compName != null)
	    			components.add(compName.getValue()+"##"+ compid);
	    		else
					components.add(component.getName()+"##"+ compid);
    		}
    	}
    	try {
    		
			testList = xcmUtils.executeComponentTestsForScenario(xcmTestScenario,  configData, components, additionalTests);
    		if( webappGrmgComponent != null )
    		{
				setCompDatas(webappGrmgComponent, xcmTestScenario);
				createAdditionalInfo(webappGrmgComponent,"Host: " + InetAddress.getLocalHost().getHostName());
    		}
			else
				if( log.isDebugEnabled())
					log.debug("Component webapp is not in GRMG request");
    	}
    	catch( Throwable ex )
    	{
    		if( webappGrmgComponent != null )
    			createError(ex.getMessage(), webappGrmgComponent);
    		returncode=false;
    		log.error(LogUtil.APPS_USER_INTERFACE,"system.exception", ex);
    	}
		Iterator iter = components.iterator();
		while( testList!= null && iter.hasNext())
		{
			String componentName = (String) iter.next();
			String grmgcompName=componentName.substring(0,componentName.indexOf('#'));
			XCMTestProperties currentTest=(XCMTestProperties) testList.get(componentName);
			if( currentTest == null )
			{
				Iterator i = scenario.getComponents().iterator();
				component=null;
				while(i.hasNext() && component == null)
				{
				  GrmgComponent c=(GrmgComponent)i.next();
				  GrmgProperty p= c.getPropertyByName("componentName");
				  if( p!= null && p.getValue().equals(grmgcompName))
				  	component=c;
				}
				if( component == null )
					component = scenario.getComponentByName(grmgcompName);
				if( component != null )
					createError("Test for component " + componentName + " not found in XCM configuration", component);
				else
				{ 
					if(webappGrmgComponent != null )
						  createError("Test for component " + componentName + " not found in XCM configuration", webappGrmgComponent);
					else
					  log.error(LogUtil.APPS_USER_INTERFACE,"system.exception", new Exception("Component " + componentName +" not found"));
				}
			}else 
			{
				Iterator iterc = scenario.getComponents().iterator();
				component=null;
				while(iterc.hasNext() && component==null)
				{
					GrmgComponent c=(GrmgComponent)iterc.next();
					GrmgProperty compId=c.getPropertyByName("componentId");

					if( c.getName().equals(currentTest.getCcmsName()) &&
						(compId == null || compId.getValue().equals(currentTest.getConfigID())))
							component = c;
				}

				if( component == null )
					log.error(LogUtil.APPS_USER_INTERFACE,"system.exception", new Exception("Component " + currentTest.getCcmsName() +" not found"));
				else
				{
					String host=currentTest.getCcmsName();
					if(currentTest.getConfigID()!= null )
						host=currentTest.getConfigID();
					setCompDatas(component, host);
					
					{
						ComponentTest testClass= currentTest.getTestClass();
						Iterator iterCTest= testClass.getTestNames().iterator();
						boolean overallResult=true;
						String resultMsg= "";
						while(iterCTest.hasNext())
						{
							String testName=(String)iterCTest.next();
							overallResult &= currentTest.getTestClass().getTestResult(testName);
							if( testClass.getTestResultDescription(testName) != null)
									resultMsg = resultMsg + "," + testClass.getTestResultDescription(testName);
						}
						if(overallResult == true){
							createOK(component);
							if(log.isDebugEnabled())
								log.debug("CCMS heartbeat for ISA: test " + component.getName() + " passed");
						} else {
							if(resultMsg.length()==0)
								resultMsg = "Unknown error";
							createError( resultMsg, component);
							returncode=false;
						}
					}
				}
			}
    	}//for
	    return returncode;
	}
    
    private XCMTestProperties findAdditionalTest(String compName, Set additionalTests) {
		Iterator iter = additionalTests.iterator();
		while(iter.hasNext())
		{
			XCMTestProperties test = (XCMTestProperties)iter.next();
			if( test.getCcmsName().equals(compName))
				return test;
		}
		return null;
	}


	private void setCompDatas(GrmgComponent component, String host){
    	component.setHost(host);
    	String version=component.getVersion();
    	if(version != null && version.length() > 0)
			component.setInst(version);	
    	else
	    	component.setInst("001");	
    }//setCompDatas
   
    private void createError(String text, GrmgComponent component)
    {
			createMessage("ERROR", "255", null, null, null, null, null, text, component);
    }//createError
    
    private void createOK(GrmgComponent component)
	{
      createMessage("OKAY", "0", null, null, null, null, null, "Test executed successful", component);
  	}//create OK
    private void createAdditionalInfo(GrmgComponent component, String msg)
	{
      createMessage("OKAY", "0", null, null, null, null, null, msg, component);
  	}//create OK
  	
  	private void createMessage(String alert, String severity,
                             String r3MsgNum, String msgParm1, String msgParm2, String msgParm3, String msgParm4,
                             String text, GrmgComponent component)
  	{
      // MH: Messages are sent to the GRMG R/3 System,
      	component.addMessage().setMessageParameters
        (alert, severity, R3_MSG_CLASS, r3MsgNum, msgParm1, msgParm2, msgParm3, msgParm4, text);
  	}//createMessage 
    
}//CcmsHeartbeatAction
