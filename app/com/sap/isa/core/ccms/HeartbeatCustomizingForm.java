package com.sap.isa.core.ccms;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.struts.action.ActionForm;
import org.w3c.dom.Attr;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.xcm.ExtendedConfigException;
import com.sap.isa.core.xcm.XCMTestProperties;
import com.sap.isa.core.xcm.XCMUtils;

/**
 * The HeartbeatCustomizingForm manages a list of Scenarios and components.
 *
 * It's used together with admin/ccms/customizing.jsp and
 * com.sap.isa.core.ccms.action.CustomizingAction
 * 
 * The init method initializes the form by reading the XCM scenarios.
 * It fills the scenarios array containing all scenarios having components
 * with XCM component tests.
 * 
 * The jsp calls the setComponentSelected and setScenarioSelected methods.
 * 
 * The createCustomizingXmlFile writes the configured CCMS Heartbeat file
 */
public class HeartbeatCustomizingForm extends ActionForm
{
  protected static IsaLocation log = IsaLocation.
        getInstance(HeartbeatCustomizingForm.class.getName());
     	
  protected Vector scenarios;
  private boolean initialized=false;
  private String urln;
  private boolean generate=false;
  private String ccmsdir;
  private String message;
  protected Node customizingFile;
  private int scenarioCount;
  private String filename;
  private String customizingXslFile=null;
  public class Scenario {
    String name;
    boolean selected=false;
    protected Scenario( String newName)
    {
      name=newName;
    }
    public void setSelected(boolean newValue)
    {
       selected= newValue;
    }
    public boolean getSelected()
    {
       return selected;
    }
    public void setName(String newValue)
    {
       name= newValue;
    }
    public String getName()
    {
       return name;
    }
	/**
	 * Method addScenarioTests. returns always null
	 * used to give derived classes a chance to add 
	 * scenarioTests to the List of tests to execute
	 * @return XCMTestProperties[]
	 */
	protected XCMTestProperties[] addScenarioTests() {
		return null;
	}


  }
  public HeartbeatCustomizingForm() {
    scenarios= new Vector();
  }
  public String getUrl()
  {
  	return urln;
  }
  public void setUrl( String newurl)
  {
  	urln=newurl;
  }

  public boolean getGenerate()
  {
  	return generate;
  }
  public void setGenerate( String newGenerate)
  {
		if(0 < newGenerate.length())
			  	generate = true;
  }
  public String getScenariosName(int idx)
  {
    return ((Scenario)scenarios.get(idx)).getName();
  }
  public void setScenariosName(int idx, String newValue)
  {
    scenarios.add(idx,new Scenario(newValue));
  }
  public String getScenariosSelected(int idx)
  {
	if(((Scenario)scenarios.get(idx)).getSelected())
	{
		return "on";
	}
	return null;
  }
  /**
   * @return returns the scenarios node of the XML file
   * created in the init() method.
   */
    public Node getScenariosNode()
  {
	boolean found=false;
	NodeList scenarios=customizingFile.getChildNodes();
	for( int e=0; e < scenarios.getLength() && !found;e++)
	{
		Node scenario=scenarios.item(e);
		if( scenario.getNodeName().equals("scenarios"))
		{
			return scenario;
		}
	}
	return null;
}
  /**
   * All components have a unique index. This method adds
   * a selected Attribut with value"yes" to the component XML element which has
   * an attribute index with value idx.
   *
   * @param idx 
   * @param newValue ignored 
   */
  public void setComponentSelected(int idx, String newValue)
  {
	boolean value = ( newValue.length() != 0);
	boolean found=false;
	Node scenariosNode=getScenariosNode();
	if(scenariosNode == null)
	{
		log.debug("Scenarios Node not found");
		return;
	}
	NodeList scenarios =  scenariosNode.getChildNodes();
	for( int k=0; k < scenarios.getLength() && !found;k++)
	{	
		Node scenario=scenarios.item(k);
		if( scenario.getNodeName().equals("tests"))
		{
			NodeList tests=scenario.getChildNodes();
			for( int i=0; i < tests.getLength() && !found;i++)
			{
				Node comp= tests.item(i);
				NamedNodeMap attr=comp.getAttributes();
				Node index = attr.getNamedItem("index");
				if( index != null && 
					index.getNodeValue().equals(Integer.toString(idx)))
				{
					found=true;
					Document doc=comp.getOwnerDocument();
					Attr a =doc.createAttribute("selected");
					a.setNodeValue("yes");
					comp.getAttributes().setNamedItem(a);
					NamedNodeMap m=comp.getAttributes();
					log.debug("Enabled Component for index" + idx );
				}
			}
		}
		if( !found )
			log.debug("Component for index" + idx + " not found ");
	}
  }
  /** 
   * Sets the selected flag in the inner Scenario class
   * The createCustomizingXmlFile will create a GRMG scenario entry for each
   * scenario which having this attribute set.
   * @param idx index in scenarios array
   * @param newValue ignored 
   */
  public void setScenariosSelected(int idx, String newValue)
  {
 	boolean value=( newValue.length() != 0);
    ((Scenario)scenarios.get(idx)).setSelected(true);
  }
  public int getScenariosSize()
  {
     return scenarios.size();
  }
	/**
	 * Returns the ccmsdir.
	 * @return String
	 */
	public String getCcmsdir() {
		return ccmsdir;
	}

	/**
	 * Sets the ccmsdir.
	 * @param ccmsdir The ccmsdir to set
	 */
	public void setCcmsdir(String ccmsdir) {
		this.ccmsdir = ccmsdir;
	}

/**
 * Method createCustomizingXmlFile.
 * Creates the CCMS Heartbeat Customizing file
 * For all scenarios having the selected flag set a grmg scenario will
 * be created. The scenario will contain all components of the corresponding
 * which have a "selected" attribute in the customizingFile
 * 
 * @param ccmsFile: File to create
 * @throws ParserConfigurationException
 * @throws IOException
 * @throws SAXException
 * @throws TransformerException
 * @throws ExtendedConfigException
 */
  public void createCustomizingXmlFile( File ccmsFile)
    throws ParserConfigurationException, IOException,SAXException, TransformerException,ExtendedConfigException
  {
    Iterator iter = scenarios.iterator();
    Map scenariosMap = new HashMap();
    scenarioCount=0;
    setFilename(ccmsFile.getAbsolutePath());
    while( iter.hasNext())
    {
        Scenario current= (Scenario)iter.next();
        if( current.getSelected())
        {
            scenariosMap.put(current.getName(), current.addScenarioTests());
            if( log.isDebugEnabled())
            log.debug("add Scenario " + current.getName());
            scenarioCount++;
        }
    }
    XCMUtils utils=new XCMUtils();
    File tmpXslFile=null;
    if(customizingXslFile != null)
    {
		tmpXslFile = new File(customizingXslFile);
    }
    utils.createCcmsCustomizingForScenarios(getUrl(),scenariosMap, ccmsFile, tmpXslFile, getScenariosNode());
  }

  public boolean isInitialized()
  {
     return initialized;
  }
  /**
   *  Reads the Scenarios list into the scenarios array and sets the customizingFile
   *  property to a file retrieved with  
   *  com.sap.isa.core.xcm.XCMUtils#getComponentConfigurationXML
   */
  public void init()
    throws ParserConfigurationException, IOException,SAXException, TransformerException,ExtendedConfigException
  {
    XCMUtils utils=new XCMUtils();
    customizingFile = utils.getComponentConfigurationXML(null, getCustomerScenariosOnly());
	Node scenario= customizingFile.getFirstChild().getLastChild();
	
	int idx=0;
	setMessage("");
	while( scenario != null)
	{
		Node ccmsscenname = scenario.getAttributes().getNamedItem("ccmsscenname");
		Node scenname = scenario.getAttributes().getNamedItem("scenario-name");
		if( ccmsscenname != null )
		{
			setScenariosName(idx, scenname.getNodeValue());
		}
		scenario = scenario.getPreviousSibling();
	}
		
    initialized=true;
  }
/**
 * This method returns always true. It's used for initialization of
 * the form (init() method). If it returns false in an overwritten class, 
 * the initialization will retrieve all XCM application configurations
 * instead of the customer configurations only. 
 * @return true 
 */
protected boolean getCustomerScenariosOnly() {
	return true;
}
/**
 * Returns the scenarioCount.
 * @return int
 */
public int getScenarioCount() {
	return scenarioCount;
}



/**
 * Returns the message.
 * @return String
 */
public String getMessage() {
	return message;
}

/**
 * Sets the message.
 * @param message The message to set
 */
public void setMessage(String message) {
	this.message = message;
}

/**
 * Returns the filename.
 * @return String
 */
public String getFilename() {
	return filename;
}

/**
 * Sets the filename.
 * @param filename The filename to set
 */
public void setFilename(String filename) {
	this.filename = filename;
}

/**
 * Returns the customizingXslFile.
 * @return String
 */
public String getCustomizingXslFile() {
	return customizingXslFile;
}

/**
 * Sets the customizingXslFile.
 * @param customizingXslFile The customizingXslFile to set
 */
public void setCustomizingXslFile(String customizingXslFile) {
	this.customizingXslFile = customizingXslFile;
}

}