/*
 * Created on Jun 14, 2004
 *
 */
package com.sap.isa.core;

import org.apache.struts.util.MessageResources;

/**
 * Creates com.sap.isa.core.MessageResources
 */
public class MessageResourcesFactory
	extends org.apache.struts.util.MessageResourcesFactory {

	/**
	 * 
	 */
	public MessageResourcesFactory() {
		super();
	}

	/* (non-Javadoc)
	 * @see org.apache.struts.util.MessageResourcesFactory#createResources(java.lang.String)
	 */
	public MessageResources createResources(String config) {
		MessageResources msgR= new com.sap.isa.core.MessageResources(this,config);
		return msgR;
	}
	static public org.apache.struts.util.MessageResourcesFactory createFactory()
	{
		return new MessageResourcesFactory();
	}
}
