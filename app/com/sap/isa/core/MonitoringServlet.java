package com.sap.isa.core;

import java.io.IOException;
import java.util.Properties;

import javax.management.MBeanServer;
import javax.management.ObjectName;
import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sap.isa.core.config.AdminConfig;
import com.sap.isa.core.jmx.ext.ExtBootstrapper;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.WebUtil;



public class MonitoringServlet extends HttpServlet {

	private static IsaLocation log =
						   IsaLocation.getInstance(MonitoringServlet.class.getName());

	
	private ExtBootstrapper ccmsBootstrapper;

	/**
	 * Turns J2EE Engine based session trace on
	 */
	public void init() throws ServletException {
		final String METHOD_NAME = "init()";
		log.entering(METHOD_NAME);
		super.init();
		try {
			// Register jmx classes
			log.debug("Begin initializing ccms reporting");
			ccmsBootstrapper = new ExtBootstrapper();
			Properties props = new Properties();
			props.put(ExtBootstrapper.VERSION_PROVIDER_CLASS, "com.sap.isa.core.util.VersionGet");
			ccmsBootstrapper.initialize(null, props);
			log.debug("End initializing ccms reporting");
		} catch (Throwable t) {
			log.error(LogUtil.APPS_USER_INTERFACE_CONTROLLER,"system.exception", new Object[] { "Error init ccms reporting" }, t);
		} finally {
			log.exiting();
		}
	}

	/**
	 * Provides information about session trace ids
	 * @param req HTTP request
	 * @param res HTTP response
	 */
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException 
	{
		final String METHOD_NAME = "doGet()";
		log.entering(METHOD_NAME);
		if(AdminConfig.isIsAppInfo())
		{
			String trackActivityCount = this.getServletConfig().getInitParameter("trackActivityCount");
			if(trackActivityCount == null)
				trackActivityCount="100";
			turnOnSingleSessionTrace(Integer.parseInt(trackActivityCount));
			req.getRequestDispatcher("/admin/appinfo.jsp?securityId=" + WebUtil.getSecurityId(req.getSession().getId())).forward(req, res);
		}
		log.exiting();
	}
	
	private void turnOnSingleSessionTrace(int trackActivityCount) {
		try {
			InitialContext ctx = new InitialContext();	
			ObjectName mbeanName= WebUtil.getApptracingMBean(ctx); 
			//String methodName = "getActivities";
			String methodName = "setSessionTraceSettings";
			// and here the well known properties from the GUI
			Properties parameter = new Properties();
			parameter.put("enableSessionTrace", "true");
			parameter.put("trackActivityCount", "100");
			parameter.put("sessionTraceURLParam", Constants.SAP_SAT);
			Object[] parameters = new Object[] {parameter};
			String[] signature = new String[] {"java.util.Properties"};
			// invoke the method
			MBeanServer mbs = (MBeanServer) ctx.lookup("jmx");
			Object obj = mbs.invoke(mbeanName, methodName, parameters, signature);
		} catch (Throwable ex) {
			log.debug("Calling getActivities() failed", ex);	
		}  
	}
	
    public void destroy() {
      super.destroy();
	  ccmsBootstrapper.terminate();
	  ccmsBootstrapper=null;
    }
  }