package com.sap.isa.core.xcm;

import com.sap.isa.core.test.ComponentTest;

/**
 * This class contains all neccessary datas of a XCM - Test, that are needed
 * in the CCMS heartbeat for ISA
 */

public class XCMTestProperties {
	private String componentID="";
	private String configID="";
	private String ccmsName=null;
	private ComponentTest testClass;
	private boolean testRun=false;
	
	
	public String getComponentID(){
		return(this.componentID);	
	}//getComponentID

	public String getConfigID(){
		return(this.configID);	
	}//getConfigID
	
	public ComponentTest getTestClass(){
		return(this.testClass);	
	}//getTestClass
		
	public void setComponentID(String compID){
		this.componentID = compID;	
	}//setComponentID

	public void setConfigID(String confID){
		this.configID = confID;	
	}//setConfigID
	
	public void setTestClass(String className) 
		throws IllegalAccessException,InstantiationException, ClassNotFoundException
	{
		testClass = null;
		testClass =(ComponentTest) getClass()
					.getClassLoader()
					.loadClass(className)
					.newInstance();
	}//setTestClass
	public void setTestClass(ComponentTest classInstance) 
	{
		testClass =classInstance;
	}//setTestClass

	/**
	 * @return If ccmsName was set the method return it, otherwise it
	 * returns the componentId
	 */
	public String getCcmsName() {
		if( ccmsName != null &&ccmsName.length() > 0 )
			return ccmsName;
		return getComponentID();
	}

	/**
	 * @param 8 character CCMS name
	 */
	public void setCcmsName(String string) {
		ccmsName = string;
	}

}//XCMTestProperties
