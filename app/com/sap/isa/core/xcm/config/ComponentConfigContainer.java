package com.sap.isa.core.xcm.config;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.sap.isa.core.xcm.XCMUtils;

/**
 * Acts as container for component configurations of the following structure
 * If the xcmcontrol attribute is specified the configuration is for internal
 * use only 
 * <pre>
 * 	<data>
 * 		<component id="component name">
 * 			<configs>
 *				<config id="config name">
 *					<params id="parameter set name">
 *						<param name="param name" value="param value"/>
 *					</params>
 *					<params id="parameter set name">
 *						<param name="param name" value="param value"/>
 *					</params>
 *				<config/>		 
 *				<config id="config name" xcmcontrol="invisible">
 *					<params id="parameter set name">
 *						<param name="param name" value="param value"/>
 *					</params>
 *					<params id="parameter set name">
 *						<param name="param name" value="param value"/>
 *					</params>
 *				<config/>		 
 *  		</configs>
 * 		</component>
 * 	</data>
 * </pre>
  * 
 */
public class ComponentConfigContainer {

	private Map mComponents = new HashMap();

	/**
	 * In order to initialize the container a DOM document has to be passed
	 * @param doc DOM document
	 */
	public ComponentConfigContainer(Document doc, boolean applicationScopeOnly) {

		NodeList paramNodes = doc.getElementsByTagName("component");
		for (int i = 0; i < paramNodes.getLength(); i++) {
			// current param config
			Node currentNode = paramNodes.item(i);
			if (currentNode.getNodeType() == Node.ELEMENT_NODE) {
				Component comp = new Component((Element)currentNode);
				String id = comp.getId();
				if (applicationScopeOnly) {
					if (XCMUtils.isApplicationScopeCompConfig(id))
						mComponents.put(comp.getId(), comp);
					continue;
				}
				mComponents.put(comp.getId(), comp);
			}
		}
	}
	
	/**
	 * Returns a set containing names of available component configurations
	 * @return a set containing names of available component configurations
	 */
	public Set getComponentNames() {
		return Collections.unmodifiableSet(mComponents.keySet());
	}

	/**
	 * Returns names of available component configurations for a given component 
	 * @param compName component name
	 * @return a set containing names of the available configurations for the given component
	 */
	public Set getComponentConfigs(String compName) {
		Component comp =
			(Component) mComponents.get(compName);
		if (comp == null)
			return Collections.EMPTY_SET;
		else {
			return comp.getComponentConfigs();
		}
	}

	/**
	 * Returns a component configuration
	 * @param compName 
	 * @param compConfigName
	 * @return a component configuration 
	 */
	public ComponentConfig getComponentConfig(String compName, String compConfigName) {
		Component comp = (Component)mComponents.get(compName);
		if (comp == null)
			return null;
		else
			return comp.getComponentConfig(compConfigName);
	}

	/**
	 * Container for component configurations
	 */
	private class Component {
		
		private String mId;
		private Map mCompConfigs = new HashMap();
		private Set mCompConfigValues = new HashSet();
		private boolean mIsApplicationScope = false;

		private Component(Element rootElement) {
			mId = ((Element)rootElement).getAttribute("id");
			NodeList paramNodes = rootElement.getElementsByTagName("config");
			for (int i = 0; i < paramNodes.getLength(); i++) {
				// current param config
				Node currentNode = paramNodes.item(i);
				if (currentNode.getNodeType() == Node.ELEMENT_NODE) {
					ComponentConfig config = new ComponentConfig((Element)currentNode);
					
					mCompConfigs.put(config.getId(), config);
					mCompConfigValues.add(config);
				}
			}
		}

		/**
		 * Returns <code>true</code> if this is an application scope component otherwise 
		 * <code>false</code>
		 * @return <code>true</code> if this is an application scope component otherwise 
		 * <code>false</code>
		 */
		public boolean isApplicationScope() {
			return mIsApplicationScope;
		}


		private String getId() {
			return mId;
		}

		private ComponentConfig getComponentConfig(String compConfigId) {
			if (compConfigId == null)
				return null;
			ComponentConfig config =
				(ComponentConfig) mCompConfigs.get(compConfigId);
			return config;
		}

		private Set getComponentConfigs() {
			return Collections.unmodifiableSet(mCompConfigValues);
		}

	}
}
