package com.sap.isa.core.xcm.config;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Represents a single component configuration. A component configuration
 * contains one or more parameter sets.
 *
 * <pre> 
 *  	<config id="config name">
*			<params id="parameter set name">
*				<param name="param name" value="param value"/>
*			</params>
*			<params id="parameter set name">
*				<param name="param name" value="param value"/>
*			</params>
 *		<config/>
 * </pre>	 
 *
 */
public class ComponentConfig {

	public static final String DEFAULT_ID          = "default";
	public static final String XML_ATTR_PARAMS     = "params";
	public static final String XML_ATTR_PARAM      = "param";
	public static final String XML_ATTR_NAME       = "name";
	public static final String XML_ATTR_VALUE      = "value";
	public static final String XML_ATTR_ID         = "id";
	public static final String XML_ATTR_XCMCONTROL = "xcmcontrol";
	public static final String XML_ATTR_VALUE_INVISIBLE = "invisible";

	private Map mParamConfigs = new HashMap();
	private Set mIsForInternalUseOnly = new HashSet();

	private String mId;

	
	/**
	 * Initializes a component configuration
	 * @param rootElement
	 */	
	ComponentConfig(Element rootElement) {
		mId = ((Element)rootElement).getAttribute(XML_ATTR_ID);
		NodeList paramNodes = rootElement.getElementsByTagName(XML_ATTR_PARAMS);
		for (int i = 0; i < paramNodes.getLength(); i++) {
			// current param config
			Node currentNode = paramNodes.item(i);
			if (currentNode.getNodeType() == Node.ELEMENT_NODE) {
				ParamConfig config = new ParamConfig((Element)currentNode);
				mParamConfigs.put(config.getId(), config);
				String xcmcontrol = ((Element)currentNode).getAttribute(XML_ATTR_XCMCONTROL);
				if (xcmcontrol != null && xcmcontrol.equalsIgnoreCase(XML_ATTR_VALUE_INVISIBLE))		
					mIsForInternalUseOnly.add(config.getId());
			}
		}

	}

	/**
	 * Returns true if the component configuration is for internal use only
	 * This is 
	 * @return  <code>true</code> if the component configuration is for internal use only
	 */
	public boolean isForInternalUse(String configId) {
		return mIsForInternalUseOnly.contains(configId);
	}


	/**
	 * Returns the id of the component
	 * @return get id
	 */
	public String getId() {
		return mId;
	}

	/**
	 * Returns a set of properties for the given id
	 * @param paramConfigId
	 * @return set of properties for the given id
	 */
	public Properties getParamConfig(String paramConfigId) {
		ParamConfig paramConfig = (ParamConfig)mParamConfigs.get(paramConfigId);
		if (paramConfig == null)
			return null;
		else
			return paramConfig.getProperties();
	}
	
	/**
	 * Returns a set of properties of all parameters part of this configuration, no matter
	 * if they params XML element has an id attribute or not 
	 * @return set of properties of all parameters part of this configuration
	 */
	public Properties getAllParams() {
		Properties allProps = getParamConfig();
		if (allProps == null)
			allProps = new Properties();
		for (Iterator iter = mParamConfigs.values().iterator(); iter.hasNext();) {
			ParamConfig config = (ParamConfig)iter.next(); 
			Properties tmpProps = config.getProperties();
			for (Enumeration enum = tmpProps.keys(); enum.hasMoreElements();) {
				String name = (String)enum.nextElement();
				String value = tmpProps.getProperty(name);
				allProps.setProperty(name, value);
			}
		}
		return allProps;
	}
	
	/**
	 * Returs a set of properties. Those properties are returned 
	 * whose <params> XML element has no 'id' attribute
	 * @return set of properties or null if no properties found
	 */
	public Properties getParamConfig() {
		ParamConfig paramConfig = (ParamConfig)mParamConfigs.get(DEFAULT_ID);
		if (paramConfig == null)
			return null;
		else
			return paramConfig.getProperties();		
	}
	
	private class ParamConfig {

		private Properties mParams = new Properties();
		private String mId;

		private ParamConfig(Element rootElement) {
			mId = ((Element)rootElement).getAttribute(XML_ATTR_ID);
			
			if (mId == null || mId.length() == 0)
				mId = DEFAULT_ID;
			
			
			NodeList paramNodes = rootElement.getElementsByTagName(XML_ATTR_PARAM);
			for (int i = 0; i < paramNodes.getLength(); i++) {
				// current param config
				Node currentNode = paramNodes.item(i);
				if (currentNode.getNodeType() == Node.ELEMENT_NODE) {
					Element currentElement = (Element)currentNode; 
					String name = currentElement.getAttribute(XML_ATTR_NAME);
					String value = currentElement.getAttribute(XML_ATTR_VALUE);
					mParams.setProperty(name, value);				
				}
			}		
		}

		/**
		 * Returns the id of the parameter configuration
		 * @return the id of the parameter configuration
		 */
		private String getId() {
			return mId;
		}
	
		private Properties getProperties() {
			return mParams;
		}
	}
}
