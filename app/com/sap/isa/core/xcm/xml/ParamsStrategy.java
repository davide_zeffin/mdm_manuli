package com.sap.isa.core.xcm.xml;

// isa imports
import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.w3c.dom.Attr;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import com.sap.isa.core.logging.IsaLocation;


/**
 * This class acts as an event listener when an XML document
 * is processed by the <code>XIncludeProcessor</code> When
 * nodes are processed it is checked wheter these nodes
 * contain parameters. If this is the case then the parameters
 * are replaced by their actual values
 */
public class ParamsStrategy implements XIncludeProcessor.NodeManipulatorStrategy {
	
	//private Map mParams = new HashMap();

	private Params mParams = new ParamsContainer(); 

	private static int mTraceLevel = XMLConstants.TRACE_ALL;

	protected static IsaLocation log =
		IsaLocation.getInstance(ParamsStrategy.class.getName());


	/**
	 * Sets a container with configuration parameters
	 *@param params configuration parameters
	 */
	public void setParams(Params params) {
		mParams	= params;
	}

	/**
	 * This method checks if the Node contains parameters.
	 * If this is the case and the parameters are known to this
	 * object, then they are replaced by their actual 
	 * values
	 */
	public void processNode(Node node) {
		if (node != null && node.getNodeType() == Node.ELEMENT_NODE) {
			NamedNodeMap nodeMap = node.getAttributes();
			if (nodeMap.getLength() == 0)
				return;
			// iteratre through all attributes			
			for (int i = 0; i < nodeMap.getLength(); i++) {
				Attr attr = (Attr)nodeMap.item(i);
				replaceParams(attr);
			}
		}

	}
	
	/**
	 * Adds a new parameter 
	 * @param name parameter name
	 * @param value value of parameter
	 */
/*	public void addParam(String name, String value) {
		mParams.put(name, value)	;
	}	
*/	
	/**
	 * Removes a parameter
	 * @param name name of parameter
	 */
/*	public void removeParam(String name) {
		mParams.remove(name);
	}
*/	
	/**
	 * Check if node is an Element
	 * @param node DOM node
	 * @return <code>true</code> if node is an DOM element, 
	 *         otherwise <code>false</code>
	 */
	public boolean isElement(Node node) {
		if (node != null && node.getNodeType() == Node.ELEMENT_NODE)
			return true;
		else
			return false;		
	}

	/**
	 * Checks if Value contains a parameter know by this objet
	 * @param attr DOM attribute
	 */
	protected void replaceParams(Attr attr) { 

		if (attr.getValue() == null || attr.getValue().length() == 0)
			return;

		for (Iterator iter = mParams.getParamMap().keySet().iterator(); iter.hasNext(); ) {
			String attrValue = attr.getValue();
			String rawName = (String)iter.next();	
			String paramName = getConfigParam(rawName);
			int index;
			while ( -1 != (index = attrValue.indexOf(paramName))) {

				// replace param with value
				String paramValue = mParams.getParamValue(rawName);
				if(paramName.equals(paramValue))
				{
					if ( log.isDebugEnabled() ) 
						log.debug("Parameter recursion detected for  attribute '" + attr.getName() +
							"' value: '" + attr.getValue() + "' parameter: '" + paramName + "'" );
					break;
				}
				if (mTraceLevel == XMLConstants.TRACE_ALL && log.isDebugEnabled() ) {	 
					log.debug("Replacing parameter: attribute name = '" + attr.getName() 
						+ "' attribute value = '" + attr.getValue() 
						+ "' param name = '" + paramName 
						+ "' param value = '" + paramValue + "'");
				}					
				StringBuffer sb = new StringBuffer(attrValue.substring(0, index));
				sb.append(paramValue);
				sb.append(attrValue.substring(index + paramName.length(), attrValue.length()));
				// assign new value to attribute
				attrValue = sb.toString();
				attr.setValue(attrValue);

				if (mTraceLevel == XMLConstants.TRACE_ALL && log.isDebugEnabled() ) 
					log.debug("Replcement result: '" + sb.toString());
			}			
		}
	}


	public static interface Params extends Serializable {
		/**
		 * Adds a a parameter 
		 * @param name name of parameter
		 * @param value value of parameter
		 */
		public void addParam(String name, String value);
		
		/**
		 * returns the value of a given parameter
		 * @param name parameter name
		 * @return the value of a given parameter
		 */
		public String getParamValue(String name);
		
		/**
		 * Returns a unmodifiable map containing the parameter name/values
		 * @return a unmodifiable map containing the parameter name/values
		 */
		public Map getParamMap();
	}	
	
	
	public static class ParamsContainer implements Params {
		
		private Map mExtKeys = new HashMap();

		/**
		 * Adds a a parameter 
		 * @param name name of parameter
		 * @param value value of parameter
		 */
		public void addParam(String name, String value) {
			mExtKeys.put(name, value);
		}
		
		/**
		 * returns the value of a given parameter
		 * @param name parameter name
		 * @return the value of a given parameter
		 */
		public String getParamValue(String name) {
			return (String)mExtKeys.get(name);
		}
		
		/**
		 * Returns a unmodifiable map containing the parameter name/values
		 * @return a unmodifiable map containing the parameter name/values
		 */
		public Map getParamMap() {
			return mExtKeys;
		}
		
		public String toString() {
			StringBuffer sb = new StringBuffer();
			for(Iterator iter = mExtKeys.keySet().iterator(); iter.hasNext();) {
				String tagName = (String)iter.next();	
				String keyName = (String)mExtKeys.get(tagName);
				sb.append("param name = '" + tagName + "'; param value = '" + keyName + "'\n");
			}	
			
			return sb.toString();
		}
	}
	
	/**
	 * Returns the configuration parameter 
	 */
	public static String getConfigParam(String configParam) {
		// check if parameter has already the right format
		StringBuffer param = new StringBuffer();
		if (configParam == null)
			return "";

		// check if parameter has the format ${name}			
		if ((configParam.indexOf("${") == 0) 
			&& (configParam.lastIndexOf("}") == configParam.length() - 1)) {
			return configParam;
			} else {
				param.append("${").append(configParam).append("}");
			}
		return param.toString();
	}

	/**
	 * Sets the trace level 
	 * @param traceLevel either XMLConstants.TRACE_ALL or XMLConstants.TRACE_PATH
	 */
	public static void setTraceLevel(int traceLevel) {
		mTraceLevel = traceLevel;	
	}


}


