package com.sap.isa.core.xcm.xml;

import org.w3c.dom.Document;

import com.sap.isa.core.logging.IsaLocation;

/**
 * This class combines the extension mechaninsm with XInclude
 * Before a document is included into an other document using XInclude,
 * it is checked there are some nodes extending other nodes. If this
 * is the case then this is processed and the result is passed 
 * back to the XInclude Processor
 */
public class ExtensionStrategy implements XIncludeProcessor.DocManipulatorStrategy {

	protected static IsaLocation log =
		IsaLocation.getInstance(ExtensionStrategy.class.getName());


	private ExtensionProcessor mExtProc;

	private static int mTraceLevel = XMLConstants.TRACE_ALL;

	/**
	 * Processes the extension mechanism before an docuemt is 
	 * included into an other document
	 * @param doc 
	 * @param href 
	 */
	public Document postInclude(Document doc, String href) {
		if (mTraceLevel >= XMLConstants.TRACE_PATH && log.isDebugEnabled() )
			log.debug("ENTER postInclude(Document doc)");

		Document result = doc;
		if (mExtProc != null)
			result = mExtProc.parse(doc);

		if (mTraceLevel >= XMLConstants.TRACE_PATH && log.isDebugEnabled() )
			log.debug("EXIT postInclude(Document doc)");
		return result;
	}

	/**
	 * Sets the extension processor which will manipulate the 
	 * included documents
	 * @param extProc the extension processor
	 */
	public void setExtensionProcessor(ExtensionProcessor extProc) {
		mExtProc = extProc;
	}

	/**
	 * Sets the trace level 
	 * @param traceLevel either XMLConstants.TRACE_ALL or XMLConstants.TRACE_PATH
	 */
	public static void setTraceLevel(int traceLevel) {
		mTraceLevel = traceLevel;	
	}
	

}
