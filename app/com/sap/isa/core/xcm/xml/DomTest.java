package com.sap.isa.core.xcm.xml;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
//import com.sap.engine.lib.xml.dom.xpath.*;
//import com.sap.engine.lib.xml.dom.xpath.*;
 
 
public class DomTest {
 

  public static void main (String[] args ) throws Throwable  {
	String xml1= "C:/dev/perforce3100/sap/ESALES_base/dev/src/_core/test/local/config/dom_test1.xml";
	String xml2= "C:/dev/perforce3100/sap/ESALES_base/dev/src/_core/test/local/config/dom_test2.xml";
	//System.setProperty("javax.xml.parsers.DocumentBuilderFactory", "com.sap.engine.lib.jaxp.DocumentBuilderFactoryImpl");
    String query = "/configs/config[@id='default']/params/param[@id='set1']";

    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    factory.setValidating(true);
	factory.setNamespaceAware(true);
    DocumentBuilder builder = factory.newDocumentBuilder();
    Document document1  = builder.parse(xml1);
    Document document2  = builder.parse(xml2);
	System.out.println("old: " + document1);

	Node test = document1.importNode(document2.getFirstChild(), true);
	
    NodeList nlTest2 = document1.getElementsByTagName("test2");
    Node test1 = nlTest2.item(0);
	
	document1.getFirstChild().replaceChild(test, test1);

    System.out.println(test.getParentNode());
	
	System.out.println("new: " + document1);    
	
	
    
  }


}
