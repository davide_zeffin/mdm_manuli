package com.sap.isa.core.xcm.xml;

/**
 * Constants shared by classed in this package
 */
public interface XMLConstants {

	/**
	 * If this trace level is enabled everything is traces if
	 * global log level is set to DEBUG
	 */
	public static int TRACE_ALL = 10;

 
	/**
	 * If this trace level is enabled path information is traced
	 * if global log level is set to DEBUG
	 */
	public static int TRACE_PATH = 5;

	/**
	 * HREF attribute
	 * value: 'href'
	 */
	public static final String HREF 		 = "href";
	
	/**
	 * Base attribute with namespace
	 * value 'xml:base'
	 */
	public static final String BASE_NS 	 = "xml:base";
	/**
	 * Base attribute without namespace
	 * value 'base'
	 */
	public static final String BASE 		 = "base";

	/**
	 * Fallback attribute without namespace
	 * value 'fallback'
	 */
	public static final String FALLBACK    = "fallback";

	/**
	 * attribute for Node id with namespace
	 */
	public static final String NODE_ID_NS    = "isa:elementid";

	/**
	 * attribute for Node id without namespace
	 */
	public static final String NODE_ID 	   = "elementid";

	/**
	 * 'extends' attribute 
	 */
	public final static String EXTENDS       = "extends";

	/**
	 * 'isa:extends' attribute 
	 */
	public final static String EXTENDS_NS       = "isa:extends";

	/**
	 * 'isa:extended' attribute 
	 */
	public final static String EXTENDED_NS   = "isa:extended";

	/**
	 * 'base' attribute 
	 */
	public final static String ISA_BASE       = "base";

	/**
	 * 'isa:base' attribute 
	 */
	public final static String ISA_BASE_NS       = "isa:base";


	/**
	 * 'extended' attribute 
	 */
	public final static String EXTENDED   = "extended";


	/**
	 * replaced attribute with namespace
	 */
	public final static String REPLACED_NS   = "isa:replaced";

	/**
	 * replaced attribute 
	 */
	public final static String REPLACED   = "replaced";


	/**
	 * hash attribute with namespace
	 */
	public final static String HASH_NS       = "isa:hash";

	/**
	 * hash attribute without namespace
	 */
	public final static String HASH          = "hash";

	/**
	 * 'include' element
	 */
	public final static String XINCLUDE      = "include";

	/**
	 * 'xi:include' element
	 */
	public final static String XINCLUDE_NS      = "xi:include";

	/**
	 * Element used to save parameter values (in order to be able to restore them)
	 */
	public  final static String SAVE_PARAM_VALUE = "savevalue";

	/**
	 * 'isa:savexinclude' element
	 */
	public final static String SAVE_XINCLUDE_NS  = "isa:savexiinclude";

	public final static String XINCLUDE_NAMESPACE =
		"http://www.w3.org/2001/XInclude";
	public final static String XML_NAMESPACE =
		"http://www.w3.org/XML/1998/namespace";

	public final static String ISA_NAMESPACE 
		= "com.sap.isa.core.config";

	/**
	 * 'name' XML attribute
	 * 
	 */
	public static final String NAME = "name";
}
