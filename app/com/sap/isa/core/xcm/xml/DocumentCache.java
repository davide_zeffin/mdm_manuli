package com.sap.isa.core.xcm.xml;

import org.w3c.dom.Document;

/**
 * This interface is used to retrieve cached DOM documents
 */
public interface DocumentCache {
	
	/**
	 * returns a DOM for the given url or <code>null</code>
	 * if no DOM is in cache
	 * @param href the href to the document
	 * @return the DOM of the document of <code>null</code>
	 *          if no document in cache
	 */
	public Document getDocument(String href);

}
