package com.sap.isa.core.xcm.xml;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
//import com.sap.engine.lib.xml.dom.xpath.*;
 

public class DomXpathTest1 {


  public static void main (String[] args ) throws Throwable  {
	String xml= "C:/dev/perforce3100/sap/ESALES_base/dev/src/_core/test/local/config/extension_test2.xml";
	//System.setProperty("javax.xml.parsers.DocumentBuilderFactory", "com.sap.engine.lib.jaxp.DocumentBuilderFactoryImpl");
	String query = "/configs/config[@id='default']/params/param[@id='set1']";

    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    factory.setValidating(true);
	factory.setNamespaceAware(true);
    DocumentBuilder builder = factory.newDocumentBuilder();
    Document document  = builder.parse(xml);
    NodeList nl = document.getElementsByTagName("config");
    Node contextNode = nl.item(0);

    Element newElement = document.createElement("test");
    contextNode.appendChild(newElement);
    System.out.println("context Node: " + contextNode.getOwnerDocument());    

/*
  
    XPathEvaluatorImpl e  = new XPathEvaluatorImpl();  
    XPathExpressionImpl expr = e.createExpression(query, null);
    XPathResultImpl res = new XPathResultImpl();
         
    
    res = expr.evaluate(contextNode, (short)0, res);
    System.out.println("The type of the result is: " + res.getResultType());
  
  	XPathSetIteratorImpl iter = (XPathSetIteratorImpl)res.getSetIterator(false);
	
  	while(iter.hasNext()) {
  		Node node = iter.nextNode();
  		System.out.println("result" + node);
  		((Element)node).setAttribute("test", "test");
  	}
  	System.out.println("doc: \n" + document);
/*
  	
	
    XPathSetSnapshotImpl shot = res.getSetSnapshot(true);
        
    for (int i = 0; i < shot.getLength(); i ++) {
    	Node node = shot.item(i);
    	System.out.println("NODE: " + node);
    	((Element)node).setAttribute("new", "attr");
    }
      System.out.println(document);    
    
  }
*/
}
}
