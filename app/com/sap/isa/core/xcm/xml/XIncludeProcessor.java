package com.sap.isa.core.xcm.xml;

// isa imports
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.Stack;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.xcm.ExtendedConfigException;
import com.sap.isa.core.xcm.XCMUtils;

/**
 * This class implements a subset of XInclude. 
 * For information about XInclude refer to http://www.w3.org/TR/xinclude
 * This include machanism bases on the DOM model.
 */
public class XIncludeProcessor {

	protected static IsaLocation log =
		IsaLocation.getInstance(XIncludeProcessor.class.getName());

	public static final String XPOINTER 	 = "#xpointer";


	private static int mTraceLevel = XMLConstants.TRACE_ALL;
	

	// list of node-event listeners
	private Set mDocumentEventListeners = new HashSet();	

	// reference to doc manipulator 
	private DocManipulatorStrategy mDocManipulator;

	// reference to node manipulator
	private NodeManipulatorStrategy mNodeManipulatorStrategy;

	//get a DocumentBuilderFactory from the underlying implementation
	private DocumentBuilderFactory mDocFac;

	//get a DocumentBuilder from the factory
	private DocumentBuilder mDocBuilder;

	// stack used to find inclusion loops
	private Stack mStack = new Stack();

	// document cache
	private DocumentCache mDocCache = null;

	/**
	 * Max number of inclusions (1000)
	 * This value can be changed using the 
	 * <code>setMaxNumInclusions()</code> method
	 */
	public static final int MAX_NUM_INCLUSION = 1000;

	private int mMaxNumInclusions = MAX_NUM_INCLUSION;

	/**
	 * Constructor of this class
	 */
	public XIncludeProcessor() throws ParserConfigurationException {

		if (mTraceLevel == XMLConstants.TRACE_ALL && log.isDebugEnabled() )
			log.debug("XInclude instantiated");

		mDocFac = XMLUtil.getDocBuilderFactory(this);
		mDocFac.setValidating(false);
		mDocFac.setNamespaceAware(true);
		mDocBuilder = mDocFac.newDocumentBuilder();
	}

	/**
	 * This method returns a new document with all XIncludes resolved
	 * @param sourceDoc document
	 * @param base base of this document
	 * @return a new document with all XIncludes resolved
	 */
	public Document parse(String base, Document sourceDoc)
		throws ResourceAccessException, CircularInclusionException {

			
		if (mTraceLevel >= XMLConstants.TRACE_PATH && log.isDebugEnabled() ) 
			log.debug("ENTER parse(String, Document)");

		if (sourceDoc == null)
			throw new NullPointerException("document = 'null'");

		// create a clone of the passed document
		// the returned document is a new document
		Document resultDoc = (Document) sourceDoc.cloneNode(true);

		Node rootNode = resultDoc.getDocumentElement();

		// call node manipulator
		if (mNodeManipulatorStrategy != null)
			mNodeManipulatorStrategy.processNode(rootNode);
		
		
		// check if document has own base
		String docBase = getBase(resultDoc);
		if (docBase != null) {
			if (mTraceLevel == XMLConstants.TRACE_ALL && log.isDebugEnabled() )
				log.debug(
					"Document "
						+ sourceDoc
						+ " hase new base attribute '"
						+ docBase
						+ "'");
		base = docBase; 
		}
		

		// process all nodes of this document
		DocumentFragment docFrag = resultDoc.createDocumentFragment();
		docFrag.appendChild(resultDoc.importNode(rootNode, true));
		
		// document fragment in order to process the root node
		// in case this node is an <xi:include> element
		processNodes(base, docFrag);
		
		Node newRootNode = resultDoc.importNode(docFrag, true);
		resultDoc.removeChild(rootNode);
		resultDoc.appendChild(newRootNode);

		if (mTraceLevel >= XMLConstants.TRACE_PATH && log.isDebugEnabled() ) 
			log.debug("EXIT parse(String, Document)");
		return resultDoc;
		
	}

	/**
	 * Returns the value of base if the document has a base attribute
	 * @param doc a DOM Document
	 * @return the value of the base attribute or <code>null</code>
	 */
	private String getBase(Document doc) {
		Element root = doc.getDocumentElement();
		return getBase(root);
	}

	/**
	 * Returns the value of base if the Node has a base attribute
	 * @param doc a DOM Document
	 * @return the value of the base attribute or <code>null</code>
	 */
	private String getBase(Node node) {
		String baseAttribute = null;
		if (node.getNodeType() == Node.ELEMENT_NODE) {
			// Check for xml:base 
			String attr = ((Element) node).getAttributeNS(XMLConstants.XML_NAMESPACE, XMLConstants.BASE);
			if (attr != null && !attr.equals(""))
				baseAttribute = attr;
		}
		return baseAttribute;
	}

	/**
	 * This method returs a DOM Document for the given URL
	 * @param base base of document. If base = <code>null</code> then the document base
	 * @param url url of resource 
	 * attribute is checked.
	 * @return resource as dom
	 */
	public Document getDoc(String base, String url)
		throws ResourceAccessException {

		Document xmlDoc = null;
		if (url == null)
			throw new ResourceAccessException(ResourceAccessException.HREF_MISSING, "uri is null");

		String absURL = getAbsoluteURL(base, url);
		
		try {
			if (mDocCache != null)
				xmlDoc = mDocCache.getDocument(absURL);
			if( log.isDebugEnabled())
				log.debug("absolute URL" + absURL);

			// remove xpointer
			absURL = getURL(absURL);
			
			if (xmlDoc == null)
			{
				xmlDoc = mDocBuilder.parse(absURL);
				if( null== xmlDoc.getFirstChild() )
				{
					log.debug("Corrupt XML document " + xmlDoc.toString());
					String msg =
						"Error parsing resource for URL = '" + "'" + absURL + "'";
					ResourceAccessException raEx = 
						new ResourceAccessException(ResourceAccessException.RES_ERROR_PARSING, msg);
					throw raEx;
				}
			}
				
		} catch (IOException ioEx) {
			String msg =
				"Resource for given URL = '" + "'" + absURL + "' not found";
			ResourceAccessException raEx = 
				new ResourceAccessException(ResourceAccessException.RES_NOT_FOUND, msg);
			raEx.setBaseException(ioEx);
			throw raEx;
		} catch (SAXException saxEx) {
			String msg =
				"Error parsing resource for URL = '" + "'" + absURL + "'";
			ResourceAccessException raEx = 
				new ResourceAccessException(ResourceAccessException.RES_ERROR_PARSING, msg);
			raEx.setBaseException(saxEx);
			throw raEx;

		}
		return xmlDoc;
	}

	/**
	 * Returns the ablolute URL 
	 * @param base base URI
	 * @param href href of the resource
	 * @return the ablolute URL 
	 */
	public String getAbsoluteURL(String base, String href)
		throws ResourceAccessException {
		try {
			return XMLUtil.getInstance().getAbsoluteURL(base, href);
		} catch (NullPointerException npex) {
			throw new ResourceAccessException(ResourceAccessException.HREF_INVALID ,"HREF = 'null'");
		} catch (MalformedURLException ex) {
			String msg =
				"Resource for the given HREF = '"
					+ href
					+ "' and base = '"
					+ base
					+ "' not found";
			ResourceAccessException raEx = 
				new ResourceAccessException(ResourceAccessException.RES_NOT_FOUND, msg);
			raEx.setBaseException(ex);
			throw raEx;
		}
	}

	/**
	 * Checks whether an Element is an XInclude element
	 * @param node 
	 */
	private static boolean isXIncludeElement(Node node) {
		if (XMLConstants.XINCLUDE.equals(node.getLocalName())
			&& XMLConstants.XINCLUDE_NAMESPACE.equals(node.getNamespaceURI())) {
			return true;
		}
		return false;

	}
	
	/**
	 * Checks if an node has a fallback element. 
	 * @param node 
	 * @param name xml element name
	 * @param ns name space
	 * @return the root of the fallback element otherwise <code>null</code>
	 */
	private static Element getChildElement(Node node, String name, String ns) {
		if (node == null)
			return null;
		NodeList childNodes = node.getChildNodes();
		for (int i = 0; i < childNodes.getLength(); i++) {
			Node currentNode = childNodes.item(i);
			if (name.equals(currentNode.getLocalName()) 
				&& ns.equals(currentNode.getNamespaceURI())) {
				return (Element)currentNode;
			} 
		}
		return null;		
	}
	

	

	/**
	 * Processes an Element which is a XInclude element. The result is 
	 * a document fragment with all XIncludes resolved
	 * @param element Element representing an XInclude
	 * @param base base for this XInclude
	 */
	private DocumentFragment getResultInfoset(Element element, String base)
		throws ResourceAccessException, CircularInclusionException {

		// get href
		if (mTraceLevel >= XMLConstants.TRACE_PATH && log.isDebugEnabled() ) {
			log.debug("ENTER getResultInfoset(Element, String)");
			log.debug("Current element." + element);	
		}
		if (!element.hasAttribute(XMLConstants.HREF)) {
			throw new ResourceAccessException(ResourceAccessException.HREF_MISSING, "href attribute missing");
		}
		String href = element.getAttribute(XMLConstants.HREF);

		String url = getURL(href);

		String absHref = getAbsoluteURL(base, url);

		if (mTraceLevel == XMLConstants.TRACE_ALL && log.isDebugEnabled() )
			log.debug(
				"processing xinclude href = '" + href 
					+ "' base = '" + base + "' url = '"
					+ absHref + "'");


		// load included document		
		Document resultDoc = (Document)getDoc(base, href).cloneNode(true);

		DocumentFragment resultInfoset = resultDoc.createDocumentFragment();

		// if href has a xpointer select node pointed by xpointer
		String xpointer = getXPointer(href);

		Node rootNodeResult = resultDoc.getFirstChild();

		rootNodeResult = XMLUtil.getFirstElementSibling(rootNodeResult);

		if (rootNodeResult.getNodeType() == Node.ELEMENT_NODE)
			resultInfoset.appendChild(rootNodeResult);
		else {
			// search siblings for first Element (first one could be a comment)
			while((rootNodeResult = rootNodeResult.getNextSibling())!= null) {
				if (rootNodeResult.getNodeType() == Node.ELEMENT_NODE) {
					resultInfoset.appendChild(rootNodeResult);
					break;
				}
			}
		} 

		// get root node of result infoset
		Element rootNode = (Element)resultInfoset.getFirstChild();
		rootNode = XMLUtil.getFirstElementSibling(rootNode);
		
		// check if there was a result
		if (rootNode == null) {
			// include not possible, throw exception
			throw new ResourceAccessException(ResourceAccessException.RES_NOT_FOUND, "Resource for given href = '" + href + "' not found");
		}
			
		// check if result infoset has own base 
		String docBase = getBase(rootNode);
		if (docBase != null) {
			if (mTraceLevel == XMLConstants.TRACE_ALL && log.isDebugEnabled() )
				log.debug(
					"New base = '" + docBase + "' of document '"
						+ absHref + "'");
			base = docBase;
		}

		// push root Node on stack
		mStack.push(resultInfoset);
		
		// check if max number of inclusions reached
		if (mStack.size() == mMaxNumInclusions)
			throw new CircularInclusionException(
				"Max number (" + mMaxNumInclusions + ") of inclusions reached");

		// process all children recursively and resolve XIncludes, if available
		processNodes(base, resultInfoset);


		// call the doc manipulator
		if (mDocManipulator != null) {
			// create a document for document manipulator and event listeners
			Document newDoc = (Document)resultInfoset.getOwnerDocument().cloneNode(true);
			newDoc.appendChild(newDoc.importNode(XMLUtil.getFirstElementSibling(resultInfoset.getFirstChild()), true));
			Document postIncludeDoc = mDocManipulator.postInclude(newDoc, absHref);
			if (mTraceLevel == XMLConstants.TRACE_ALL && log.isDebugEnabled() )
				log.debug("Document after post include manipulator: \n" + postIncludeDoc);
			resultInfoset = resultDoc.createDocumentFragment();
			
			NodeList childList = postIncludeDoc.getChildNodes();
			Node currentNode = postIncludeDoc.getFirstChild();
			currentNode = XMLUtil.getFirstElementSibling(currentNode);
			if (currentNode.getNodeType() != Node.ELEMENT_NODE) {
				for (int currentChild = 0; currentChild < childList.getLength(); currentChild++) {
					currentNode = childList.item(currentChild);
					if (currentNode.getNodeType() == Node.ELEMENT_NODE)
						break;
				}
			}
			resultInfoset.appendChild(resultDoc.importNode(currentNode, true));
		}

		// fire document-before-inclusion events
		Document eventDoc = (Document)resultInfoset.getOwnerDocument().cloneNode(true);
		eventDoc.appendChild(eventDoc.importNode(XMLUtil.getFirstElementSibling(resultInfoset.getFirstChild()), true));
		fireDocumentEvents(absHref, 
				eventDoc, DocumentListener.EVENTTYPE_BEFORE_INCLUSION);

		// pop root Node from stack
		mStack.pop();

		if (xpointer != null) {
			if (mTraceLevel == XMLConstants.TRACE_ALL && log.isDebugEnabled() ) {
				log.debug("processing xpointer = '" + xpointer + "'");
			}
			try {
				resultInfoset = XMLUtil.getInstance().getXPathResult(resultInfoset, xpointer);	
			} catch (ExtendedConfigException ecex) {
				throw new RuntimeException(ecex.toString());
			}
			
			if (resultInfoset == null || resultInfoset.getFirstChild() == null) {
				// include has produced no elements
				throw new ResourceAccessException(ResourceAccessException.RES_NOT_FOUND, "Resource for given href = '" + href + "' not found");
			}
		}		

		// add base attributes to included nodes
		NodeList resultNodes = resultInfoset.getChildNodes();
		for (int i = 0; i < resultNodes.getLength(); i++) {
			Node currentNode = resultNodes.item(i);
			if (currentNode.getNodeType() == Node.ELEMENT_NODE) {
				((Element)currentNode).setAttributeNS(XMLConstants.XML_NAMESPACE, XMLConstants.BASE_NS, absHref + (xpointer == null ? "" : xpointer));
			}
		}

		if (mTraceLevel >= XMLConstants.TRACE_PATH && log.isDebugEnabled() ) 
			log.debug("EXIT getResultInfoset(Element, String)");

		return resultInfoset;
	}

	/**
	 * Get Fallback
	 * @param fallbackRoot fallback xml element
	 * @param base base for this fallback 
	 */
	private DocumentFragment getFallback(Element fallbackRoot, String base) 
		throws ResourceAccessException, CircularInclusionException {

		Document parentDoc = fallbackRoot.getOwnerDocument();
		DocumentFragment resultInfoset = parentDoc.createDocumentFragment();
		// search for next XML element
		
		Node firstChildFallbackElement  = XMLUtil.getFirstChildElement(fallbackRoot, Node.ELEMENT_NODE);
		// it seems that there is no Element as child. 
		if (firstChildFallbackElement == null) {
			// maybe some other content which has not to be processed
			if (fallbackRoot.getChildNodes().getLength() > 0) {
				resultInfoset.appendChild(fallbackRoot);
				return resultInfoset;
			}
			return null;
		}
			
		resultInfoset.appendChild(firstChildFallbackElement);

		// process all children recursively and resolve XIncludes, if available
		processNodes(base, resultInfoset);

		//resultInfoset.appendChild(resultInfoset);

		return resultInfoset;
	}


	/**
	 * This method processes all nodes in the XML tree recursively
	 */
	private void processNodes(String base, Node node)
		throws ResourceAccessException, CircularInclusionException {
		//log.debug("ENTER processChildren(String, Node)");						

		Document originalDoc = node.getOwnerDocument();
		//Get the children of this Node
		NodeList children = node.getChildNodes();

		//go through all the children of the node
		for (int i = 0; i < children.getLength(); i++) {
			//get the next child
			Node currentNode = children.item(i);
			// call node manipulator
			if (mNodeManipulatorStrategy != null)
				mNodeManipulatorStrategy.processNode(currentNode);
			
			// check if node is an XInclude

			if ((currentNode.getNodeType() == Node.ELEMENT_NODE)
				&& isXIncludeElement(currentNode)) {
				if (mTraceLevel == XMLConstants.TRACE_ALL && log.isDebugEnabled() )
					log.debug("XINCLUDE found '" 
						+ ((Element)currentNode).getAttribute(XMLConstants.HREF) + "'");
				// try to get the included document 
				DocumentFragment resultInfoset = null;
				try {
					resultInfoset = getResultInfoset((Element) currentNode, base);
				} catch (ResourceAccessException ex) {
					if (mTraceLevel == XMLConstants.TRACE_ALL && log.isDebugEnabled() )
						log.debug("XInclude failed. " + ex.toString());
					// check if there is a fallback 
					Element fallBackRoot = getChildElement(currentNode, XMLConstants.FALLBACK, XMLConstants.XINCLUDE_NAMESPACE);
					if (fallBackRoot == null)
						throw ex;
					else {
						try {
							resultInfoset = getFallback(fallBackRoot, base);
						} catch (XIncludeException fallbackEx) {
							// something went wrong while processing fallback
							// => fatal exception
							ResourceAccessException fatalEx =
								new ResourceAccessException(
									ResourceAccessException.FATAL_ERROR, "Processing fallback failed");
							fatalEx.setBaseException(fallbackEx);
							throw fatalEx;
						}
					}
				}

				if (resultInfoset == null) {
					// fallback has an empty body
					node.removeChild(currentNode);
					continue;
				} else {
					// import include document
					// add all children to 
					Node resultChild = resultInfoset.getFirstChild();
					resultChild = XMLUtil.getFirstElementSibling(resultChild);
					Node resultNode = originalDoc.importNode(resultChild, true);
					// replace XInclude node with included document
					Node currentNodeNextSibling = currentNode.getNextSibling();
				 
					Node nextSibling = null;
					while ((resultChild = resultChild.getNextSibling()) != null) {
						nextSibling = originalDoc.importNode(resultChild, true);
						if (currentNodeNextSibling != null)
							node.insertBefore(nextSibling, currentNodeNextSibling);
						else
							node.appendChild(nextSibling);
					}
					node.replaceChild(resultNode, currentNode);					
					continue;
				}			
			}

			//get the type of the child
			short childType = currentNode.getNodeType();
			if (childType == Node.ELEMENT_NODE) {
				processNodes(base, currentNode);
			}
				//log.debug("EXIT processChildren(String, Node)");						      
		}
	}

	/**
	 * This method parses the content of the given URI and returns
	 * a DOM document. The XInclude elements are processed
	 */
	public Document parse(String base, String uri)
		throws ResourceAccessException, CircularInclusionException {
		// get Document for the given uri

		if (mTraceLevel >= XMLConstants.TRACE_PATH && log.isDebugEnabled() ) {
			log.debug("ENTER parse(String, String)");
			log.debug("base = '" + base + "' uri = '" + uri + "'");
		}


		Document original = getDoc(base, uri);

		// processes or XIncludes
		Document result = parse(base, original);

		if (mTraceLevel >= XMLConstants.TRACE_PATH && log.isDebugEnabled() )
			log.debug("EXIT parse(String, String)");
		return result;
	}


	/**
	 * Returns the XPointer part of the href. If href contains no 
	 * xpointer <code>null</code> is returned
	 * @param href the href
	 * @return the XPointer part of the href 
	 */
	private String getXPointer(String href) {
		// check if XPointer is part of href
		int index = href.indexOf(XPOINTER);
		String xpointer = null;

		if (index != -1)
			xpointer =
				href.substring(
					index + XPOINTER.length() + 1,
					href.length() - 1);

		return xpointer;
	}

	/**
	 * Returns the URL part of the href
	 * @param href the href
	 * @return the URL part of the href
	 */
	private String getURL(String href) {
		// check if XPointer is part of href
		int index = href.indexOf(XPOINTER);

		if (index == -1)
			return href;

		return href.substring(0, index);

	}

	/**
	 * Sets the maximum number of inclusions
	 * This value is also importat in case of inclusion loops.
	 * After reaching this value a <code>CircularInclusionException</code>
	 * is thrown
	 * @param num max number of inclusions
	 */
	public void setMaxNumInclusions(int num) {
		mMaxNumInclusions = num;
	}		


	/**
	 * Adds a new document event listerner
	 * @param listener a node-event listener
	 */
	public void addDocEventListener(DocumentListener listener) {
		mDocumentEventListeners.add(listener);
	}
	
	/**
	 * Removes a document event listener
	 * @param listener 
	 */
	public void removeDocEventListener(DocumentListener listener) {
		mDocumentEventListeners.remove(listener);
	}
	
	/**
	 * Firest events to registered node-event listeners
	 * @param href href of document
	 * @param doc the currently processed document
	 * @param type the type of event 
	 */
	private void fireDocumentEvents(String href, Document doc, int type) {
		if (mDocumentEventListeners.size() == 0) 
			return;
		for (Iterator iter = mDocumentEventListeners.iterator(); iter.hasNext();) {
			DocumentListener listener = (DocumentListener)iter.next();
			listener.documentEvent(href, doc, type);
		}
	}



	/**
	 * Adds a post inclusion manipulator
	 * @param strategy
	 */
	public void setDocManipulatorStrategy(DocManipulatorStrategy strategy) {
		mDocManipulator = strategy;
	}

	/**
	 * Adds a node manipulator
	 * @param strategy
	 */
	public void setNodeManipulatorStrategy(NodeManipulatorStrategy strategy) {
		mNodeManipulatorStrategy = strategy;
	}


	/**
	 * Base Exception for XInclude exceptions
	 */
	public static class XIncludeException extends Exception {
		private Exception mBaseEx;
		private String mMsg = "";

		/**
		 * Constructor
		 * @param msg a message describing the exception
		 * by this exception
		 */
		public XIncludeException(String msg) {
			mMsg = msg;
		}

		/**
		 * Returns the base exception or <code>null</code> if there
		 * is no root exception
		 * @return the root exception
		 */
		public Exception getBaseException() {
			return mBaseEx;
		}

		/**
		 * Sets the base exception
		 * @param ex the base exception 
		 */
		public void setBaseException(Exception baseEx) {
			mBaseEx = baseEx;
		}

		/**
		 * Returns a string representation of this exception
		 * @return a string representation of this exception
		 */
		public String toString() {
			if (mBaseEx == null)
				return mMsg;
			else
				return mMsg + "\n" + mBaseEx.toString();
		}

	}

	/**
	 * Thrown if a resource for a given URI is not accessible
	 */
	public static class ResourceAccessException extends XIncludeException {
		private int mErrCode;

		/**
		 * Fatal error. Resource could not be accessed an there is no fallback
		 */
		public static final int FATAL_ERROR = 1;

		
		/**
		 * If HREF attribute is missing
		 */
		public static final int HREF_MISSING = 2;
		
		/**
		 * If HREF invalid
		 */
		public static final int HREF_INVALID = 3;


		/**
		 * Resource for a given HREF not found
		 */
		public static final int RES_NOT_FOUND = 4;

		/**
		 * Error parsing resource which has to be included
		 */
		public static final int RES_ERROR_PARSING = 5;



		/**
		 * Constructor
		 * @param code error code
		 * @param msg a message describing the exception
		 * by this exception
		 */
		public ResourceAccessException(int code, String msg) {
			super(msg);
			mErrCode = code;
		}
		
		/**
		 * Returns the error code of this exception
		 */
		public int getErrorCode() {
			return mErrCode;
		}
	}

	/**
	 * Thrown if max number of inclusions reached. The reasen could also be
	 * an recursive inclusion
	 */
	public static class CircularInclusionException extends XIncludeException {

		/**
		 * Constructor
		 * @param msg a message describing the exception
		 * by this exception
		 */
		public CircularInclusionException(String msg) {
			super(msg);
		}
	}
	
	/**
	 * A class implementing this interface can be used to interfere
	 * the includes processing. 
	 * This method is called after a document has been processed 
	 * i.e. all includes have been processed. 
	 * The strategy  has the opportunity to manipulate
	 * the document and return a new docuemnt before it is included or XPointer is processed on it
	 */
	public static interface DocManipulatorStrategy {
		/**
		 * This method is called when a document has been processed
		 * The returned document will be used for further processing
		 * @param doc the document
		 * @param href href of the document
		 * @return the document which will be used for further processing
		 */
		public Document postInclude(Document doc, String href);
	}
	
	/**
	 * A class implementing this interface can be used to interfere
	 * the parsing process.
	 */
	public static interface NodeManipulatorStrategy {
	 /**
	 * This method is called when the dom tree is processed but before
	 * any xincludes are performed. It has the opportunity to manipulate
	 * a node before further processing
	 * @param node node currently processed. it can be directly manipulated
	 */
	public void processNode(Node node);

		
	}
	
	/**
	 * A class implementing this interface is notified 
	 * when a document is processed
	 */
	public interface DocumentListener {
		
		/**
		 * Indicates that the document has not yet been included
		 */
		public static final int EVENTTYPE_BEFORE_INCLUSION = 0;
		
		/**
		 * This Method is called after a docuemnt has been processed
		 * (and after processing all strategies) but before it is included
		 * @param href the href of the document
		 * @param doc the document 
		 * @param type the type of event
		 */
		public void documentEvent(String href, Document doc, int type);
	}
	
	/**
	 * Sets a document cache
	 * @param cache the document cache
	 */
	public void setDocCache(DocumentCache cache) {
		mDocCache = cache;
	}
	
	/**
	 * Sets the trace level 
	 * @param traceLevel either XMLConstants.TRACE_ALL or XMLConstants.TRACE_PATH
	 */
	public static void setTraceLevel(int traceLevel) {
		mTraceLevel = traceLevel;	
	}
		
}
