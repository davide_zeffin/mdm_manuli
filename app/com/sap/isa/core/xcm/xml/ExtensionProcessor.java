package com.sap.isa.core.xcm.xml;

// isa imports
import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.xcm.XCMUtils;

/**
 * This provides an extension mechanism for XML documents
 */
public class ExtensionProcessor {

	public static final String PROCESSED = "processed";

	protected static IsaLocation log =
		IsaLocation.getInstance(ExtensionProcessor.class.getName());

	//	private Map mExtTags = new HashMap();
	private ExtensionKeys mExtKeys;

	//get a DocumentBuilderFactory from the underlying implementation
	private DocumentBuilderFactory mDocFac;

	//get a DocumentBuilder from the factory
	private DocumentBuilder mDocBuilder;

	private static int mTraceLevel = XMLConstants.TRACE_ALL;

	private XMLUtil mXMLUtil = XMLUtil.getInstance();

	private Map mReplacedNodes = new HashMap();

	/**
	 * Constructor of this class
	 */
	public ExtensionProcessor() throws ParserConfigurationException {
		mDocFac = XMLUtil.getDocBuilderFactory(this);
		mDocFac.setValidating(false);
		mDocFac.setNamespaceAware(true);
		mDocBuilder = mDocFac.newDocumentBuilder();
	}

	public static void main(String[] args) {

	}

	/**
	 * Processes all extensions in the passed document and returns a 
	 * document containing the result. 
	 * @param sourceDoc document
	 */
	public Document parse(Document sourceDoc) {

		if (mTraceLevel >= XMLConstants.TRACE_PATH && log.isDebugEnabled())
			log.debug("ENTER parse(Document)");

		if (sourceDoc == null)
			throw new NullPointerException("document = 'null'");

		// create a clone of the passed document
		// the returned document is a new document
		Document resultDoc = (Document) sourceDoc.cloneNode(true);

		//Node rootNode = resultDoc.getDocumentElement();
		// get first ELEMENT child
		
		Node rootNode = resultDoc.getFirstChild();
		rootNode = XMLUtil.getFirstElementSibling(rootNode);

		// process all nodes of this document
		processNodes(rootNode);
		if (mTraceLevel >= XMLConstants.TRACE_PATH && log.isDebugEnabled())
			log.debug("EXIT parse(Document)");

		// check if some nodes have to be removed
		if (mReplacedNodes.size() > 0) {
			Set removeNodes = new HashSet();
			long start = System.currentTimeMillis();
			Node firstElement = resultDoc.getFirstChild();
			firstElement = XMLUtil.getFirstElementSibling(firstElement);
			
			processReplacedNodes(removeNodes, firstElement);
			for (Iterator iter = removeNodes.iterator(); iter.hasNext();) {
				Node node = (Node) iter.next();
				node.getParentNode().removeChild(node);
			}
		}

		mReplacedNodes = new HashMap();

		// remove all isa:extends attributes
		resultDoc =
			(Document) XMLUtil.getInstance().setAttr(
				resultDoc,
				XMLConstants.ISA_NAMESPACE,
				XMLConstants.EXTENDS,
				PROCESSED);

		return resultDoc;

	}

	/**
	 * This method processes all nodes in the XML tree recursively
	 * @param rootNode
	 */
	public void processNodes(Node rootNode) {
		//log.debug("ENTER processChildren(String, Node)");

		Document originalDoc = rootNode.getOwnerDocument();
		//Get the children of this Node
		NodeList children = rootNode.getChildNodes();

		//go through all the children of the node
		for (int i = 0; i < children.getLength(); i++) {
			//get the next child
			Node currentNode = children.item(i);
			if (currentNode.getNodeType() == Node.ELEMENT_NODE) {

				String noProcess =
					((Element) currentNode).getAttribute("NoProcess");
				if (noProcess.length() != 0)
					continue;

				Node baseNode = getBaseNodeWithExtension(currentNode);
				if (baseNode == null) {
					// call nested notes recursively
					
					processNodes(currentNode);
					continue;
				}

				if (mTraceLevel >= XMLConstants.TRACE_PATH
					&& log.isDebugEnabled()) {
					log.debug("Base node:" + baseNode);
					//baseNode =  baseNode.cloneNode(true);
				}
				Document currentDoc = currentNode.getOwnerDocument();

				// process extension mechanism on base and ext node
				Node extendedNode = extendNode(baseNode, currentNode);
				Node newExtendedNode =
					currentDoc.importNode(extendedNode, true);
				copyAttrbutes(newExtendedNode, currentNode);
				// replace the processed node with the current node
				rootNode.replaceChild(newExtendedNode, currentNode);
				processNodes(currentNode);

			}

			//log.debug("EXIT processChildren(String, Node)");                  
		}

	}

	/**
	 * This method iterates through all nodes recursively. Nodes which
	 * have been replaced will be removed
	 */
	private void processReplacedNodes(Set removeNodes, Node rootNode) {

		//Get the children of this Node
		NodeList children = rootNode.getChildNodes();

		//go through all the children of the node
		for (int i = 0; i < children.getLength(); i++) {
			//get the next child
			Node currentNode = children.item(i);

			if (currentNode.getNodeType() == Node.ELEMENT_NODE) {
				// check if there is a node named as current node which has to be removed
				if (mReplacedNodes.get(currentNode.getNodeName()) != null) {
					Element currentElement = (Element) currentNode;
					// check if node has replaced another node
					if (currentElement
						.getAttribute(XMLConstants.REPLACED_NS)
						.length()
						== 0) {
						KeyValueContainer cont =
							(KeyValueContainer) mReplacedNodes.get(
								currentNode.getNodeName());
						String keyValue =
							currentElement.getAttribute(cont.getKeyName());
						if (cont.hasKeyValue(keyValue)) {
							//currentNode.getParentNode().removeChild(currentNode);
							removeNodes.add(currentNode);
						}

					}
				}
				processReplacedNodes(removeNodes, currentNode);
			}

		}
	}

	/**
	 * This method processes the extension mechanism on a node and all their children
	 * @param baseNode the node 
	 * @param extNode the node which extends the base node
	 */
	public Node extendNode(Node baseNode, Node extNode) {
		if (mTraceLevel >= XMLConstants.TRACE_PATH && log.isDebugEnabled()) {
			log.debug("ENTER extendNode(Node baseNode, Node extNode)");
			log.debug("base node " + baseNode);
			log.debug("extending node " + extNode);
		}
		Node newBaseNode = null;
		// check if base doc extends another doc
		Node baseNodeExtension = getBaseNodeWithExtension(baseNode);

		if (baseNodeExtension != null) {
			if (mTraceLevel == XMLConstants.TRACE_ALL && log.isDebugEnabled())
				log.debug("Process multiple inheritance " + baseNodeExtension);

			// process the base node first
			newBaseNode =
				extendNode(baseNodeExtension, baseNode).cloneNode(true);
			//baseNode.replaceChild(newBaseNode, baseNode);

		} else {
			// create a copy of the base node 
			// this node and all their children will be extendend with the extending node
			newBaseNode = baseNode.cloneNode(true);
			// clone the baseNode, so that the original baseNode is not modified 
		}

		//Get the children of these nodes
		NodeList extChildren = extNode.getChildNodes();
		Document baseDoc = newBaseNode.getOwnerDocument();
		Document extDoc = extNode.getOwnerDocument();

		// iterate through all children of the extending node
		for (int i = 0; i < extChildren.getLength(); i++) {
			Node currentExtNode = extChildren.item(i);
			// check if there is a node which can be extended
			if (currentExtNode.getNodeType() != Node.ELEMENT_NODE)
				continue;

			NodeList baseChildren = newBaseNode.getChildNodes();
			//NodeList updatedBaseChildren =  
			Node baseNodeWithSameKey =
				getBaseNodeWithKey(baseChildren, currentExtNode);
			if (baseNodeWithSameKey == null) {
				// no node to extend
				if (mTraceLevel == XMLConstants.TRACE_ALL
					&& log.isDebugEnabled())
					log.debug("Node has to be added: " + currentExtNode);
				newBaseNode.appendChild(
					baseDoc.importNode(currentExtNode, true));
				continue;
			}

			if (mTraceLevel == XMLConstants.TRACE_ALL
				&& log.isDebugEnabled()) {
				log.debug("Current extending node: " + currentExtNode);
				log.debug(("Base node with same key: " + baseNodeWithSameKey));
			}

			// check if extending node has base attribute
			String baseAttr =
				((Element) currentExtNode).getAttributeNS(
					XMLConstants.ISA_NAMESPACE,
					XMLConstants.ISA_BASE);

			Node additionalBaseNode = null;

			if (baseAttr.length() > 0) {
				// add base node to extending node
				additionalBaseNode =
					newBaseNode.getOwnerDocument().importNode(
						baseNodeWithSameKey.cloneNode(true),
						true);
				newBaseNode.appendChild(additionalBaseNode);
				copyAttrbutes(additionalBaseNode, currentExtNode);
				//extendNode(additionalBaseNode, currentExtNode);   
				//replace additionalBaseNode with extAdditionalBaseNode, because changes were not made to the additionalBaseNode, but to a cloning of it: newBaseNode = baseNode.cloneNode(true); above
				Node extAdditionalBaseNode =
					extendNode(additionalBaseNode, currentExtNode);
				newBaseNode.replaceChild(
					extAdditionalBaseNode,
					additionalBaseNode);
				continue;
			}  

			// mix attrbutes of current element
			copyAttrbutes(baseNodeWithSameKey, currentExtNode);
			//copyAttrbutes(currentExtNode, baseNodeWithSameKey);

			if (baseAttr.length() > 0) {
				newBaseNode.appendChild(additionalBaseNode);
			}

			// process nested element recursively

			//replace baseNodeWithSameKey with extBaseNodeWithSameKey, because changes were not made to the baseNodeWithSameKey, but to a cloning of it: newBaseNode = baseNode.cloneNode(true); above
			Node extBaseNodeWithSameKey =
				extendNode(baseNodeWithSameKey, currentExtNode);
			newBaseNode.replaceChild(extBaseNodeWithSameKey, baseNodeWithSameKey);
		}
		return newBaseNode;
	}

	/**
	 * Checks if node has an isa:extends attribute. If this is the case
	 * then a query is performed and the result is returned
	 * @param node node to check
	 * @return base node or <code>null</code>
	 */
	public Node getBaseNodeWithExtension(Node node) {

		if (mTraceLevel >= XMLConstants.TRACE_PATH && log.isDebugEnabled())
			log.debug("ENTER getBaseNodeWithExtension(Node node)");

		if (node.getNodeType() != Node.ELEMENT_NODE)
			return null;

		Attr extendsAttr =
			((Element) node).getAttributeNodeNS(
				XMLConstants.ISA_NAMESPACE,
				XMLConstants.EXTENDS);
		if (extendsAttr == null || extendsAttr.getValue().equals(PROCESSED))
			return null;

		String extPath = extendsAttr.getNodeValue();
		if (mTraceLevel == XMLConstants.TRACE_ALL && log.isDebugEnabled())
			log.debug("Extension found base = '" + extPath + "'");

		Element element = (Element) node;

		Document doc = element.getOwnerDocument();

		// remove extends attribute, preventing further procession
		//element.removeAttribute("isa:extends");

		// add hash value to node
		String currentNodeHash = String.valueOf(element.hashCode());
		element.setAttributeNS(
			XMLConstants.ISA_NAMESPACE,
			XMLConstants.HASH_NS,
			currentNodeHash);

		Node baseNode =
			mXMLUtil.getXPathResultRel(node, extPath, currentNodeHash);

		if (baseNode == null) {
			String msg =
				"Base node not found when extending node; [query]='"
					+ extPath
					+ "' [contextNode]=\n"
					+ node;
			log.warn("system.xcm.warn", new Object[] { msg }, null);
			return null;
		}

		String extKeyName = getKeyName(node);
		String extKeyValue = null;
		if (extKeyName != null)
			extKeyValue = element.getAttribute(extKeyName);

		String baseKeyName = getKeyName(baseNode);
		String baseKeyValue = null;
		if (baseKeyName != null)
			baseKeyValue = ((Element) baseNode).getAttribute(baseKeyName);

		String nodeName = node.getNodeName();

		// check if extending node will replace the base node
		if (nodeName.equals(baseNode.getNodeName())
			&& extKeyValue.length() == 0) {

			if (mTraceLevel == XMLConstants.TRACE_ALL
				&& log.isDebugEnabled()) {
				log.debug("Replacing node. ");
				log.debug("node replacing base node [node]" + element);
				log.debug("base node [node]" + baseNode);
			}
			element.setAttributeNS(
				XMLConstants.ISA_NAMESPACE,
				XMLConstants.REPLACED_NS,
				extPath);
			KeyValueContainer keyCont =
				(KeyValueContainer) mReplacedNodes.get(nodeName);
			if (keyCont == null) {
				keyCont = new KeyValueContainer();
				keyCont.setKeyName(extKeyName);
			}
			keyCont.addKeyValue(baseKeyValue);
			mReplacedNodes.put(nodeName, keyCont);
		} else {
			// add extended node
			// this node is only for information purposes and not processed anymore
			element.setAttributeNS(
				XMLConstants.ISA_NAMESPACE,
				XMLConstants.EXTENDED_NS,
				extPath);
		}
		return baseNode;
	}

	/**
	 * Gets the key of the current node and searches for a node in the 
	 * node list with the same key
	 */
	private Node getBaseNodeWithKey(NodeList baseNodes, Node currentExtNode) {
		if (mTraceLevel >= XMLConstants.TRACE_PATH && log.isDebugEnabled()) {
			log.debug(
				"ENTER getBaseNode(NodeList baseNodes, Node currentExtNode)");
			log.debug("[number of base nodes]='" + baseNodes.getLength() + "'");
		}

		boolean hasElements = true;

		if (currentExtNode.getNodeType() != Node.ELEMENT_NODE)
			return null;

		Element currentExtElement = (Element) currentExtNode;

		boolean hasElementId = false;
		if (currentExtElement
			.getAttributeNodeNS(XMLConstants.ISA_NAMESPACE, "elementid")
			!= null)
			hasElementId = true;

		// if current node has no attributes than search for the first element
		// in the base nodes with the same name
		if ((currentExtElement.getAttributes().getLength()) == 0
			|| ((currentExtElement.getAttributes().getLength() == 1)
				&& hasElementId)) {
			hasElements = false;
		}

		String key = getKeyName(currentExtNode);
		String extNodeName = currentExtNode.getNodeName();

		Attr keyAttr = currentExtElement.getAttributeNode(key);
		String keyValue = null;
		// if there are attributes but no key => no base can be found
		if ((keyAttr == null && hasElements == true)) {
			if (mTraceLevel == XMLConstants.TRACE_ALL && log.isDebugEnabled())
				log.debug(
					"There are attributes but no key => no base can be found");
			return null;
		}

		// there is a key => get key value    
		if (keyAttr != null)
			keyValue = keyAttr.getValue();

		if (mTraceLevel == XMLConstants.TRACE_ALL && log.isDebugEnabled())
			log.debug(
				"[element]='"
					+ extNodeName
					+ "' ; [key value] = '"
					+ keyValue
					+ "'; [hasAttributes]='"
					+ hasElements
					+ "'");

		// get an eventually available isa:base attribute
		// if this is available then search for a node with
		// the same key but the value of this attribute
		String baseAttr =
			currentExtElement.getAttributeNS(
				XMLConstants.ISA_NAMESPACE,
				XMLConstants.ISA_BASE);

		if (mTraceLevel == XMLConstants.TRACE_ALL && log.isDebugEnabled()) {
			log.debug("[isa:base='" + baseAttr + "'");
		}

		// serach for the base node with the same key
		for (int i = 0; i < baseNodes.getLength(); i++) {
			Node currentBaseNode = baseNodes.item(i);

			if (mTraceLevel == XMLConstants.TRACE_ALL
				&& log.isDebugEnabled()) {
				log.debug("Current [base node]: " + currentBaseNode);
			}

			if (currentBaseNode.getNodeType() != Node.ELEMENT_NODE)
				continue;

			if (currentBaseNode.getNodeName().equals(extNodeName)) {
				if (mTraceLevel == XMLConstants.TRACE_ALL
					&& log.isDebugEnabled()) {
					log.debug(
						"Found base node: [name]='"
							+ currentBaseNode.getNodeName()
							+ "'");
					log.debug("[hasElements]='" + hasElements + "'");
				}

				// if there is no key then return element with same name
				if (hasElements == false)
					return currentBaseNode;

				// check key attribute
				Attr baseKeyAttr =
					((Element) currentBaseNode).getAttributeNode(key);

				if (mTraceLevel == XMLConstants.TRACE_ALL
					&& log.isDebugEnabled()) {
					log.debug("[keyAttr]='" + baseKeyAttr + "'");
				}

				if (baseAttr.length() > 0
					&& baseAttr.equals(baseKeyAttr.getValue())) {
					return currentBaseNode;
				}

				if (baseKeyAttr != null
					&& baseKeyAttr.getValue().equals(keyValue)) {
					// found base node with the same key => has to be mixed
					return currentBaseNode;
				}
			}
		}
		// no base node with same key found
		return null;
	}

	/**
	 * This method copies attributes from one element into an other element
	 * @param baseNode this element gets the attributes from the extending node
	 * @param extendNode
	 */
	public void copyAttrbutes(Node baseNode, Node extNode) {
		if (mTraceLevel >= XMLConstants.TRACE_PATH && log.isDebugEnabled())
			log.debug("ENTER copyAttrbutes(Node baseNode, Node extNode)");

		Document baseDoc = baseNode.getOwnerDocument();

		if (baseNode.getNodeType() != Node.ELEMENT_NODE) {
			if (mTraceLevel >= XMLConstants.TRACE_PATH && log.isDebugEnabled())
				log.debug("EXIT copyAttrbutes(Node baseNode, Node extNode)");
			return;
		}

		Element baseElement = (Element) baseNode;
		// iterate over all attributes of the extending element and add them
		NamedNodeMap attributes = ((Element) extNode).getAttributes();
		for (int i = 0; i < attributes.getLength(); i++) {
			Node attr = attributes.item(i);
			if (attr.getNodeType() == Node.ATTRIBUTE_NODE) {
				String attrName = attr.getNodeName();
				String oldAttrValue = baseElement.getAttribute(attrName);
				String newAttrValue = ((Attr) attr).getValue();

				baseElement.setAttributeNode(
					(Attr) baseDoc.importNode(attr, false));
				// create an attribute saving the original value
				if (attrName.equals("value")
					&& !oldAttrValue.equals(newAttrValue)) {
					((Element) extNode).setAttribute(
						XMLConstants.SAVE_PARAM_VALUE,
						oldAttrValue);
					//baseElement.setAttribute(newAttrName, oldAttrValue)}
				}

			}
		}
		if (mTraceLevel >= XMLConstants.TRACE_PATH && log.isDebugEnabled()) {
			log.debug("copy attributes result: ");
			log.debug(extNode);
		}

		if (mTraceLevel >= XMLConstants.TRACE_PATH && log.isDebugEnabled())
			log.debug("EXIT copyAttrbutes(Node baseNode, Node extNode)");

	}

	/**
	 * Creates an extension key conatiner
	 */
	public static ExtensionKeys createExtensionKeyContainer() {
		return new ExtensionKeyContainer();
	}

	/**
	 * This method returs a DOM Document for the given URL
	 * @param base base of document. If base = <code>null</code> then the document base
	 * @param url url of resource 
	 * attribute is checked.
	 * @return resource as dom
	 */
	public Document getDoc(String base, String url) throws ExtensionException {

		Document xmlDoc = null;

		String absURL = null;
		try {
			absURL = XMLUtil.getInstance().getAbsoluteURL(base, url);
		} catch (Exception ex) {
			ExtensionException urlEx =
				new ExtensionException(
					"Invalid URL = '" + url + "'",
					ExtensionException.HREF_INVALID);
			urlEx.setBaseException(ex);
			throw urlEx;
		}
		try {
			xmlDoc = mDocBuilder.parse(absURL);
		} catch (IOException ioEx) {
			String msg =
				"Resource for given URL = '" + "'" + absURL + "' not found";
			ExtensionException raEx =
				new ExtensionException(
					msg,
					ExtensionException.RES_ACCESS_EXCEPTION);
			raEx.setBaseException(ioEx);
			throw raEx;
		} catch (SAXException saxEx) {
			String msg =
				"Error parsing resource for URL = '" + "'" + absURL + "'";
			ExtensionException raEx =
				new ExtensionException(msg, ExtensionException.PARSE_EXCEPTION);
			raEx.setBaseException(saxEx);
			throw raEx;

		}
		return xmlDoc;
	}

	/**
	 * Sets extension parameters
	 * @param keys a set of extension keys
	 */
	public void setExtensionKeys(ExtensionKeys keys) {
		mExtKeys = keys;
	}

	/**
	 * Base Exception for XInclude exceptions
	 */
	public static class ExtensionException extends Exception {
		private Exception mBaseEx;
		private String mMsg = "";
		private int errorCode;

		/**
		 * Error while parsing document
		 */
		public static final int PARSE_EXCEPTION = 1;

		/**
		 * Error while accessing resource
		 */
		public static final int RES_ACCESS_EXCEPTION = 2;

		/**
		 * If HREF invalid
		 */
		public static final int HREF_INVALID = 3;

		/**
		 * Constructor
		 * @param msg a message describing the exception
		 * by this exception
		 */
		public ExtensionException(String msg, int errorCode) {
			mMsg = msg;
		}

		/**
		 * Returns the base exception or <code>null</code> if there
		 * is no root exception
		 * @return the root exception
		 */
		public Exception getBaseException() {
			return mBaseEx;
		}

		/**
		 * Sets the base exception
		 * @param ex the base exception 
		 */
		public void setBaseException(Exception baseEx) {
			mBaseEx = baseEx;
		}

		/**
		 * Returns a string representation of this exception
		 * @return a string representation of this exception
		 */
		public String toString() {
			if (mBaseEx == null)
				return mMsg;
			else
				return mMsg + "\n" + mBaseEx.toString();
		}

	}

	/**
	 * Sets the trace level 
	 * @param traceLevel either XMLConstants.TRACE_ALL or XMLConstants.TRACE_PATH
	 */
	public static void setTraceLevel(int traceLevel) {
		mTraceLevel = traceLevel;
	}

	public static interface ExtensionKeys extends Serializable {
		/**
		 * Adds a name of extension element 
		 * @param name name of XML tag
		 * @param key attribute name of XML element which acts as key
		 */
		public void addExtensionTag(String name, String key);

		/**
		 * Adds a name of extension element 
		 * @param tagName name of XML tag
		 * @return attribute name of XML element which acts as key
		 */
		public String getExtensionKey(String tagName);
	}

	public static class ExtensionKeyContainer implements ExtensionKeys {

		private Map extKeys = new HashMap();

		public void addExtensionTag(String name, String key) {
			extKeys.put(name, key);
		}

		public String getExtensionKey(String tagName) {
			return (String) extKeys.get(tagName);
		}

		public String toString() {
			StringBuffer sb = new StringBuffer();
			for (Iterator iter = extKeys.keySet().iterator();
				iter.hasNext();
				) {
				String tagName = (String) iter.next();
				String keyName = (String) extKeys.get(tagName);
				sb.append(
					"tag name = '"
						+ tagName
						+ "'; key name = '"
						+ keyName
						+ "'\n");
			}

			return sb.toString();
		}

		/**
		 * Returns a clone of this container
		 * @return a clone of this container
		 */
		public Object clone() {
			ExtensionKeyContainer exContainer = new ExtensionKeyContainer();
			for (Iterator iter = extKeys.keySet().iterator();
				iter.hasNext();
				) {
				String name = (String) iter.next();
				String value = (String) extKeys.get(name);
				exContainer.addExtensionTag(name, value);
			}
			return exContainer;
		}
	}

	public static class KeyValueContainer {

		private String mKeyName;
		private Set mKeyValues = new HashSet();

		public void setKeyName(String keyName) {
			mKeyName = keyName;
		}

		public String getKeyName() {
			return mKeyName;
		}

		public void addKeyValue(String keyValue) {
			mKeyValues.add(keyValue);
		}

		public boolean hasKeyValue(String keyValue) {
			return mKeyValues.contains(keyValue);
		}

	}

	/**
	 * Returns the extension-key name of the passed node
	 * @param node 
	 * @return key name of node or <code>null</code> if node has no key
	 */
	private String getKeyName(Node node) {
		// get the name of the key for this XML element 
		String extNodeName = node.getNodeName();

		String key = null;
		if (mExtKeys == null) {
			if (mTraceLevel == XMLConstants.TRACE_ALL && log.isDebugEnabled())
				log.debug("No extension keys have been associated !!!");
		} else {
			key = mExtKeys.getExtensionKey(extNodeName);
			if (mTraceLevel == XMLConstants.TRACE_ALL && log.isDebugEnabled())
				log.debug(
					"Key name for XML element '"
						+ extNodeName
						+ "' is '"
						+ key
						+ "'");
		}
		return key;
	}
}
