package com.sap.isa.core.xcm.xml;

// java imports
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.sap.engine.lib.xml.dom.xpath.XPathEvaluatorImpl;
import com.sap.engine.lib.xml.dom.xpath.XPathExpressionImpl;
import com.sap.engine.lib.xml.dom.xpath.XPathResultImpl;
import com.sap.engine.lib.xml.dom.xpath.XPathSetIteratorImpl;
import com.sap.engine.lib.xml.dom.xpath.XPathSetSnapshotImpl;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.xcm.ExtendedConfigException;

/**
 * This class prives some useful methods when working with XML
 */
public class XMLUtil {
	private static XMLUtil mXMLUtil;

	protected static IsaLocation log =
		IsaLocation.getInstance(XMLUtil.class.getName());

	private DocumentBuilderFactory mDocFac;

	private DocumentBuilder mDocBuilder;

	private static int mTraceLevel = XMLConstants.TRACE_ALL;

	private int mId = 0;

	/**
	 * Private constructor
	 */
	private XMLUtil() {
		mDocFac = XMLUtil.getDocBuilderFactory(this);
		mDocFac.setValidating(true);
		mDocFac.setNamespaceAware(true);
		try {
			mDocBuilder = mDocFac.newDocumentBuilder();
		} catch (ParserConfigurationException pcex) {
			log.error(LogUtil.APPS_BUSINESS_LOGIC,
				"system.xcm.exception",
				new Object[] { pcex.toString()},
				pcex);
		}

	}

	/**
	 * Returns a instance of this class
	 */
	public synchronized static XMLUtil getInstance() {
		if (mXMLUtil == null)
			mXMLUtil = new XMLUtil();

		return mXMLUtil;
	}

	/**
	 * Returns a String representation of a XML Document
	 * @return String representation of a XML Document
	 */
	public String toString(Node doc) {
		StringWriter stringWriter = new StringWriter();
		try {
			
			TransformerFactory tfactory = getTransformerFactory(this);
			Transformer serializer = tfactory.newTransformer();
			Properties props = new Properties();
			props.setProperty(OutputKeys.METHOD, "xml");
			props.setProperty(OutputKeys.INDENT, "yes");
			serializer.setOutputProperties(props);

			serializer.transform(
				new DOMSource(doc),
				new StreamResult(stringWriter));
		} catch (Exception ex) {
			String msg = "Error creating String";
			log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.xcm.exception", new Object[] { msg }, ex);
		}

		return stringWriter.toString();
	}

	/**
	 * This method adds an attribute NODE_ID_NS containing an index to 
	 * each element of the passed document
	 * @param startIndex of id
	 * @param doc the DOM
	 * @return the new document
	 */
	public Document addIdsToDoc(int startIndex, Document doc) {
		Node rootNode = doc.getFirstChild();
		mId = startIndex;
		addObjId(rootNode);
		return doc;
	}

	/**
	 * This method removes all nodes containing or not containing an attribute.
	 * @param node the root node
	 * @param namespace
	 * @param attrName the name of the attribute
	 * @param hasAttribute if set to <code>true</code> than all nodes are remove
	 * which contain the <code>nodeName</code> attribute. if set to <code>false</code> than all nodes are remove
	 * which do not contain the <code>nodeName</code> attribute. 
	 * @return the processed document
	 */
	public Node removeNodes(Node node, String namespace, String attrName, boolean hasAttribute) {
		Set removeNodes = new HashSet();
		removeNodesRec(removeNodes, node, namespace, attrName, hasAttribute);
		for (Iterator iter = removeNodes.iterator(); iter.hasNext(); ) {
			Node removeNode = (Node)iter.next();
			removeNode.getParentNode().removeChild(removeNode);
			
		}
		
		return node;

	}

	/**
	 * This method removes all attributes with the given name
	 * @param node the root node
	 * @param namespace
	 * @param attrName the name of the attribute
	 * @return the processed document
	 */
	public Node removeAttr(Node node, String namespace, String attrName) {
		attrRec(node, namespace, attrName, null, true);
		return node;

	}

	/**
	 * This method assignes the passed value to all attributes in the 
	 * XML tree
	 * @param node the root node
	 * @param namespace
	 * @param attrName the name of the attribute
	 * @param value the value of the attribute
	 * @return the processed document
	 */
	public Node setAttr(Node node, String namespace, String attrName, String value) {
		attrRec(node, namespace, attrName, value, false);
		return node;

	}

	/**
	 * Returns the ablolute URL 
	 * @param base base URI
	 * @param href href of the resource
	 * @return the ablolute URL 
	 */
	public String getAbsoluteURL(String base, String href)
		throws MalformedURLException {

		if (log.isDebugEnabled()) {
			log.debug("START getAbsoluteURL()");
			log.debug("[base]='" + base + "' [href]='" + href + "'");
		}

		// references a share 
		// the href has the following format:
		// file:///\\hostname\sharename
		// do not change (this is a hack!)
		if(href.indexOf("///\\\\") != -1) 
			return href;

		if (href == null)
			throw new NullPointerException("HREF = 'null'");

		URL url = null;
		if (href.substring(0, 1).equals("/")
			|| href.substring(0, 1).equals("\\"))
			href = href.substring(1);

		if (base != null) {
			URL context = new URL(base);
			url = new URL(context, href);
		} else {
			url = new URL(href);
		}
		String remote = url.toExternalForm();
		
		if (log.isDebugEnabled())
			log.debug("[absolute path]='" + remote + "'");
		
		return remote;
	}

	/**
	 * Returns a document fragment as result of a xpath query
	 */
	DocumentFragment getXPathResult(Node rootNode, String query) throws ExtendedConfigException  {
		if (mTraceLevel == XMLConstants.TRACE_ALL && log.isDebugEnabled()) {
			log.debug(
				"getXPathResult(DocumentFragment docFrag, String xpath): query = '"
					+ query
					+ "'");
			log.debug(
				"getXPathResult(DocumentFragment docFrag, String xpath): on node: \n"
					+ rootNode);
		}

		// This object is used to create expressions. 
		XPathEvaluatorImpl e = new XPathEvaluatorImpl();

		// create expression
		XPathExpressionImpl expr = e.createExpression(query, null);

		// create result object
		XPathResultImpl res = new XPathResultImpl();

		// create a new document with passed root element 		
		Document newDoc =
			(Document) rootNode.getOwnerDocument().cloneNode(true);
		newDoc.appendChild(newDoc.importNode(rootNode, true));
		DocumentFragment result = newDoc.createDocumentFragment();

		// evaluates the expression
		try {
			res = expr.evaluate(newDoc, (short) 0, res);
		} catch (Exception ex) {
			String msg = "Error evaluatin XPath for [query]='" + query + "' " + ex.toString();
			throw new ExtendedConfigException(msg);
		} 
		
		XPathSetSnapshotImpl shot = res.getSetSnapshot(true);

		for (int i = 0; i < shot.getLength(); i++) {
			Node node = newDoc.importNode(shot.item(i), true);

			result.appendChild(node);
		}

		if (log.isDebugEnabled() && mTraceLevel == XMLConstants.TRACE_ALL)
			log.debug(
				"getXPathResult(DocumentFragment docFrag, String xpath): result: \n"
					+ result);

		return result;
	}

	/**
	 * Returns a document fragment as result of a xpath query
	 */
	void removeNodeOfXPathResult(Node rootNode, String query) {
		if (mTraceLevel == XMLConstants.TRACE_ALL && log.isDebugEnabled()) {
			log.debug(
				"removeNodeOfXPathResult(DocumentFragment docFrag, String xpath): query = '"
					+ query
					+ "'");
			log.debug(
				"removeNodeOfXPathResult(DocumentFragment docFrag, String xpath): on node: \n"
					+ rootNode);
		}

		// This object is used to create expressions. 
		XPathEvaluatorImpl e = new XPathEvaluatorImpl();

		// create expression
		XPathExpressionImpl expr = e.createExpression(query, null);

		// create result object
		XPathResultImpl res = new XPathResultImpl();

		// create a new document with passed root element 		
		Document doc = (Document) rootNode.getOwnerDocument();

		// evaluates the expression
		res = expr.evaluate(rootNode, (short) 0, res);

		XPathSetIteratorImpl iter =
			(XPathSetIteratorImpl) res.getSetIterator(false);

		while (iter.hasNext()) {
			Node node = iter.nextNode();
			// first node found is the base node (second one should be the extending node)
			if (node != null) {
				((Element) node).setAttribute("remove", "true");
				break;
			}
		}

	}

	/**
	 * Returns a document fragment containing the result of the XPath query.
	 * It the query was not successful <code>null</code> is returned
	 * @param contextNode current context node
	 * @param String query
	 * @param hash hash value of context node. Is stored in attribute <code>isa:hash</code>
	 * @return the node xpath is pointing to
	 */
	Node getXPathResultRel(Node contextNode, String query, String hash) {
		if (mTraceLevel == XMLConstants.TRACE_ALL && log.isDebugEnabled()) {
			log.debug(
				"ENTER getXPathResultRel(Node contextNode, String query, String hash)");

			log.debug("getXPathResultRel(): query = '" + query + "'");
			log.debug("getXPathResultRel(): on node: \n" + contextNode);
			log.debug("Document: " + contextNode.getOwnerDocument());
		}

		// This object is used to create expressions. 
		XPathEvaluatorImpl e = new XPathEvaluatorImpl();

		// create expression
		XPathExpressionImpl expr = e.createExpression(query, null);

		// create result object
		XPathResultImpl res = new XPathResultImpl();

		// evaluates the expression
		res = expr.evaluate(contextNode, (short) 0, res);

		XPathSetIteratorImpl iter = res.getSetIterator(false);
		Node resultRoot = null;

		Node node = null;
		while (iter.hasNext()) {
			node = iter.nextNode();
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				String hashQueryNode =
					((Element) node).getAttributeNS(
						XMLConstants.ISA_NAMESPACE,
						XMLConstants.HASH);
			}
		}
		if (mTraceLevel == XMLConstants.TRACE_ALL && log.isDebugEnabled()) {
			log.debug(
				"EXIT getXPathResultRel(Node contextNode, String query, String hash)");
		}

		return node;

	}

	/**
	 * Returns a document fragment containing the result of the XPath query.
	 * It the query was not successful <code>null</code> is returned
	 * @param String xpath 
	 * @return the node xpath is pointing to
	<	 */
	DocumentFragment getXPathResult(Document doc, String xpath) {

		if (mTraceLevel >= XMLConstants.TRACE_PATH && log.isDebugEnabled()) {
			log.debug("ENTER getXPathResult(Document, String)");
			log.debug("Processing XPath query '" + xpath + "'");
		}

		// This object is used to create expressions. 
		XPathEvaluatorImpl e = new XPathEvaluatorImpl();

		// create expression
		XPathExpressionImpl expr = e.createExpression(xpath, null);

		// create result object
		XPathResultImpl res = new XPathResultImpl();

		// evaluates the expression
		res = expr.evaluate(doc, (short) 0, res);

		DocumentFragment resDoc = null;

		// a single node selected
		if (res.getResultType() == XPathResultImpl.SINGLE_NODE_TYPE) {
			if (mTraceLevel == XMLConstants.TRACE_ALL && log.isDebugEnabled())
				log.debug("Found single node");
			Node resultRoot = res.getSingleNodeValue();
			resDoc = doc.createDocumentFragment();
			resDoc.appendChild(doc.importNode(resultRoot, true));
		}

		// set of nodes selected
		if (res.getResultType() == XPathResultImpl.NODE_SET_TYPE) {
			if (mTraceLevel == XMLConstants.TRACE_ALL && log.isDebugEnabled())
				log.debug("Found node set");
			resDoc = doc.createDocumentFragment();
			XPathSetSnapshotImpl shot = res.getSetSnapshot(true);
			for (int i = 0; i < shot.getLength(); i++) {
				resDoc.appendChild(doc.importNode(shot.item(i), true));
			}

		}
		if (mTraceLevel >= XMLConstants.TRACE_PATH && log.isDebugEnabled())
			log.debug("EXIT getXPathResult(Document, String)");

		return resDoc;
	}

	private void addObjId(Node node) {
		//Get the children of this Node
		NodeList children = node.getChildNodes();

		//go through all the children of the node
		for (int i = 0; i < children.getLength(); i++) {
			//get the next child
			Node child = children.item(i);
			if (child.getNodeType() == Node.ELEMENT_NODE) {
				((Element) child).setAttributeNS(
					XMLConstants.ISA_NAMESPACE,
					XMLConstants.NODE_ID_NS,
					String.valueOf(mId));
				mId++;
				addObjId(child);
			}
		}
	}


	private void removeNodesRec(Set removingNodes, Node node, String namespace, String attrName, boolean hasAttribute) {
		//Get the children of this Node
		NodeList children = node.getChildNodes();

		//go through all the children of the node
		for (int i = 0; i < children.getLength(); i++) {
			//get the next child
			Node child = children.item(i);
			//get the type of the child
			if (child.getNodeType() == Node.ELEMENT_NODE) {
				Element currentElement = (Element) child;
				// remove node it the attributes is available
				Attr attr =
					currentElement.getAttributeNodeNS(namespace, attrName);
				if (hasAttribute && attr != null) {
					removingNodes.add(currentElement);
					continue;
				}
 
				if (!hasAttribute && attr == null) {
					removingNodes.add(currentElement);
					continue;
				}
				removeNodesRec(removingNodes, currentElement, namespace, attrName, hasAttribute);
			}
			removeNodesRec(removingNodes, child, namespace, attrName, hasAttribute);
		}
	}

	private void attrRec(Node node, String namespace, String attrName, String value, boolean delete) {
		//Get the children of this Node
		NodeList children = node.getChildNodes();

		//go through all the children of the node
		for (int i = 0; i < children.getLength(); i++) {
			//get the next child
			Node child = children.item(i);
			//get the type of the child
			if (child.getNodeType() == Node.ELEMENT_NODE) {
				Element currentElement = (Element) child;
				
				Attr attr = null;
				if (namespace == null)
					attr = currentElement.getAttributeNode(attrName);
				else
					attr =
						currentElement.getAttributeNodeNS(namespace, attrName);
				if (attr != null) {
					if (delete) {
						currentElement.removeAttributeNode(attr);
					} else {
						attr.setNodeValue(value);	
					}
				}
				attrRec(currentElement, namespace, attrName, value, delete);
			}

		}
	}

	/**
	 * Returns a DOM for the given String representing an 
	 * valid XML document, otherwise <code>null</code>
	 * @param xmlString string representing an XML document
	 * @return a DOM
	 */
	public Document getDoc(String xmlString) {

		try {
			InputStream sis = new ByteArrayInputStream(xmlString.getBytes("UTF-8"));
			Document doc = mDocBuilder.parse(sis);
			sis.close();
			return doc;
		} catch (SAXException saxex) {
			log.error(LogUtil.APPS_BUSINESS_LOGIC,
				"system.xcm.exception",
				new Object[] { saxex.toString()},
				saxex);
			return null;
		} catch (IOException ioex) {
			log.error(LogUtil.APPS_BUSINESS_LOGIC,
				"system.xcm.exception",
				new Object[] { ioex.toString()},
				ioex);
			return null;
		}
	}

	/**
	 * Sets the trace level 
	 * @param traceLevel either XMLConstants.TRACE_ALL or XMLConstants.TRACE_PATH
	 */
	public static void setTraceLevel(int traceLevel) {
		mTraceLevel = traceLevel;
	}

	/**
	 * This method returs a DOM Document for the given URL
	 * @param base base of document. If base = <code>null</code> then the document base
	 * @param url url of resource 
	 * attribute is checked.
	 * @return resource as dom
	 */
	public Document getDoc(String base, String url) throws Exception {

		Document xmlDoc = null;
		if (url == null)
			throw new Exception("uri is null");

		String absURL = getAbsoluteURL(base, url);
		try {

			xmlDoc = mDocBuilder.parse(absURL);

		} catch (IOException ioEx) {
			String msg =
				"Resource for given URL = '" + "'" + absURL + "' not found";
			Exception ex = new Exception(ioEx.toString());
			throw ex;
		} catch (SAXException saxEx) {
			String msg =
				"Error parsing resource for URL = '" + "'" + absURL + "'";
			Exception ex = new Exception(saxEx.toString());
			throw ex;

		}
		return xmlDoc;
	}

	public static DocumentBuilderFactory getDocBuilderFactory(Object thisObject) {
		DocumentBuilderFactory fact;                                            
		ClassLoader oldLoader = Thread.currentThread().getContextClassLoader(); 
		try {                                                                   
		  Thread.currentThread().setContextClassLoader(                         
		  thisObject.getClass().getClassLoader());
		  // As soon as XCM runs with every javax parser this needs to be changed:
		  // there is no com.sap.engine 
		  // fact= DocumentBuilderFactory.newInstance();
		  fact = new com.sap.engine.lib.jaxp.DocumentBuilderFactoryImpl();                          
		} finally {                                                             
		  Thread.currentThread().setContextClassLoader(oldLoader);              
		}
		return fact;
	}

	public static TransformerFactory getTransformerFactory(Object thisObject) {
		TransformerFactory fact;                                            
		ClassLoader oldLoader = Thread.currentThread().getContextClassLoader(); 
		try {                                                                   
		  Thread.currentThread().setContextClassLoader(                         
		  thisObject.getClass().getClassLoader());
		  // As soon as XCM runs with every javax parser this needs to be changed:
		  // there is no com.sap.engine 
		  // fact= DocumentBuilderFactory.newInstance();
		  fact = new com.sap.engine.lib.jaxp.TransformerFactoryImpl();                          
		} finally {                                                             
		  Thread.currentThread().setContextClassLoader(oldLoader);              
		}
		return fact;
	}

	
	/**
	 * Returns the first child of type Node.ELEMENT_NODE 
	 * @param node 
	 * @param type type of node
	 * @return first xml child element otherwise <code>null</code>
	 */
	public static Element getFirstChildElement(Node node, short type) {
		if (node == null)
			return null;
		NodeList childNodes = node.getChildNodes();
		for (int i = 0; i < childNodes.getLength(); i++) {
			Node currentNode = childNodes.item(i);
			if (currentNode.getNodeType() == type)
				return (Element)currentNode;
		}
		return null;		
	}

	/**
	 * Returns the first ELEMENT Sibiling
	 * @param node
	 */
	public static Element getFirstElementSibling(Node node) {
		
		if (node == null)
			return null;
			
		if (node.getNodeType() == Node.ELEMENT_NODE)
			return (Element)node;

		while((node = node.getNextSibling())!= null) {
			if (node.getNodeType() == Node.ELEMENT_NODE)
				break;
		}
		return (Element)node;		
	}

	
	
}
