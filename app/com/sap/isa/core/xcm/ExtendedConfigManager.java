package com.sap.isa.core.xcm;

// isa imports
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import com.sap.isa.core.Constants;
import com.sap.isa.core.cache.Cache;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.xcm.init.ExtendedConfigInitHandler;
import com.sap.isa.core.xcm.init.ExtensionKeyConfig;
import com.sap.isa.core.xcm.init.FilesConfig;
import com.sap.isa.core.xcm.init.ParamsConfig;
import com.sap.isa.core.xcm.xml.ParamsStrategy;

/**
 * This is the central instance managing the Extended Configuration Management
 */
public class ExtendedConfigManager {

	private static ExtendedConfigManager mConfigMgr;
	private static Map mExternalConfigParams = new HashMap();
	
	protected static IsaLocation log = IsaLocation.
        getInstance(ExtendedConfigManager.class.getName());
	
	
	private String mBaseDir = null; 

	/**
	 * Private constructor
	 */
	private ExtendedConfigManager () {}


	/**
	 * Returns an instance of this Extended Configuration Manager
	 */
	public static synchronized ExtendedConfigManager getExtConfigManager() {
		if (mConfigMgr == null)
			mConfigMgr = new ExtendedConfigManager();
		return mConfigMgr;
	}
	
	/**
	 * Returns a configuration container with default settings 
	 * @return a configuration container with default settings 
	 */
	private ConfigContainer getDefaultConfigContainer() {
		ConfigContainer cc = new ConfigContainer();

		// provide files which have to be processed
		FilesConfig filesConfig = ExtendedConfigInitHandler.getFilesConfig();
		Map files = filesConfig.getPaths();
		for (Iterator iter = files.keySet().iterator(); iter.hasNext();) {
			String name = (String)iter.next();
			String path = (String)files.get(name);
			String scope = filesConfig.getScope(name);
			String realURL = ExtendedConfigInitHandler.getAbsoluteURL(path);
			cc.addIncludeDocHref(mBaseDir, realURL, scope, name);
		}
		
		// provide extension parameters
		ExtensionKeyConfig extKeyConfig = 
			ExtendedConfigInitHandler.getExtensionKeyConfig();
		cc.setExtensionKeys(extKeyConfig.getExtensionKeyContainer());

		// provide default configuratin parameters
		ParamsConfig paramConfig = ExtendedConfigInitHandler.getParamsConfig();
		ParamsStrategy.Params params = paramConfig.getParamsContainer();
		cc.setConfigParams(params);
		
		return cc;
	}
	
	/**
	 * Returns a fully processed Configuration Container with default settings plus 
	 * passed configuration parameters
	 * @param confgParams configuration parameters
	 * @param scenario name of scenario
	 * @return a configuration container
	 */
	public synchronized ConfigContainer getModifiedConfig(Map configParams) 
				throws ExtendedConfigException {
		if (configParams == null)
			configParams = new HashMap();
		// get default configuration 
		ConfigContainer configContainer = getDefaultConfigContainer();
		ParamsStrategy.Params params = configContainer.getConfigParams();
		// add additional properties to configuration container
		for (Iterator iter = configParams.keySet().iterator(); iter.hasNext();) {
			String paramName = (String)iter.next();
			String paramValue = (String)configParams.get(paramName);

			if (log.isDebugEnabled())
				log.debug("Adding additional parameters [name]='" + paramName + 
				"' [value]='" + paramValue + "'");

			params.addParam(paramName, paramValue);
		}				
		// get scenario name
		String scenario = params.getParamValue(Constants.XCM_SCENARIO);
		configContainer.setScenarioName(scenario);
		
		// create key for this configuration 
		String key = getConfigKey(params);
		if (log.isDebugEnabled())
			log.debug("Configuration [key]='" + key + "'");
		configContainer.setConfigKey(key);
		
		// add external parameters to this parameter set
		params = mixExternalParams(params);		
		configContainer.setConfigParams(params);
		
		// get Cache Access 
		try {
			Cache.Access access = Cache.getAccess(XCMConstants.CACHE_REGION_NAME);
			configContainer = (ConfigContainer)access.get(configContainer.getConfigKey(), configContainer);
		} catch (Cache.Exception cex) {
			String msg = "Error retrieveing configuration [key]='" + configContainer.getConfigKey() + "'";
			log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.exm.exception", new Object[] { msg }, cex);			
			ExtendedConfigException ecex = new ExtendedConfigException(msg);
			ecex.setBaseException(cex);
			throw ecex;
		}		
		return configContainer;
	}
	
	/**
	 * Returns a config container for the given key. If no container
	 * is available <code>null</code> is returned
	 * @param key configuration key
	 * @return config container for the given key or <code>null</code>
	 */
	public ConfigContainer getExistingConfig(String key) {

		ConfigContainer configContainer = new ConfigContainer();
		
		// get Cache Access 
		try {
			Cache.Access access = Cache.getAccess(XCMConstants.CACHE_REGION_NAME);
			configContainer = (ConfigContainer)access.get(key);
		} catch (Cache.Exception cex) {
			configContainer = null;
		}		
		return configContainer;
	}
	
	
	/**
	 * Adds static external configuration parameters which are always
	 * mixed with the externally configured configuration parameters
	 * @param paramName name of parameter
	 * @param paramValue value of parameter
	 */
	public static void addExternalConfigParam(String paramName, String paramValue) {
		if (paramName == null)			
			return;
		mExternalConfigParams.put(paramName, paramValue);
	}

	/**
	 * Returns an unmodifiable map of external configuration paramters
	 * @return an unmodifiable map of external configuration paramters
	 */
	public static Map getExternalConfigParams() {
		return Collections.unmodifiableMap(mExternalConfigParams);
	}
	

	/**
	 * Mixes external configuration parametrs with the parameters
	 * passed in the parameter container
	 * @param params parameter container
	 */
	public static ParamsStrategy.Params mixExternalParams(ParamsStrategy.Params params) {
		
		for(Iterator iter = mExternalConfigParams.keySet().iterator(); iter.hasNext();) {
			String paramName = (String)iter.next();	
			String paramValue = (String)mExternalConfigParams.get(paramName);
			params.addParam(paramName, paramValue);
		}
		return params;	
	}

	/**
	 * Sets the base directory for configuration files
	 * @param baseDir base directory used when retrieving configuration files
	 */		
	public void setBaseDir(String baseDir) {
		mBaseDir = baseDir;
	}


	/**
	 * Returns a configuration key for the given parameters
	 * @param params configuration parameters
	 * @return a configuration key for the given parameters
	 */ 
	private String getConfigKey(ParamsStrategy.Params params) {
		
		StringBuffer sb = new StringBuffer();
		for(Iterator iter = params.getParamMap().keySet().iterator(); iter.hasNext();) {
			String paramName = (String)iter.next();
			String paramValue = params.getParamValue(paramName);
			sb.append(paramName).append(":").append(paramValue).append("#");
		}
		return sb.toString().substring(0, sb.toString().length() - 1);		
	}
	
	
	/**
	 * Returns true if the passed file name is part of the 
	 * Extended Configuration Management
	 */
	public boolean isManagedFile(String fileName) {

		String tmpFileName = fileName;
		if (fileName.substring(0,1).equals("/") || 
			fileName.substring(0,1).equals("\\"))
			tmpFileName = fileName.substring(1);
			
		FilesConfig filesConfig = ExtendedConfigInitHandler.getFilesConfig();		
		Map files = filesConfig.getPaths();

		for (Iterator iter = files.values().iterator(); iter.hasNext();) {
			String path = (String)iter.next();
			String tmpPath = path;
			if (path.substring(0,1).equals("/") || 
					path.substring(0,1).equals("\\")) {
				tmpPath = path.substring(1);
			}
			if (tmpFileName.equals(tmpPath)) 
					return true;
		}
		return false;
	}
	 
	/**
	 * Returns a configuration container for the given scenario name
	 * @param scenarioName
	 * @return ConfigContainer
	 */
	public ConfigContainer getConfig(String scenarioName) {
		Properties p = new Properties();
		p.setProperty(Constants.XCM_SCENARIO_RP, scenarioName);
		
		Map xcmProps = XCMUtils.getExtConfProps(p);

		try {
			ConfigContainer cc = getModifiedConfig(xcmProps);
			return cc;
		} catch (Exception ex) {
			String msg = "Error retrieveing configuration for [scenario]='" + scenarioName + "'";
			log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.exm.exception", new Object[] { msg }, ex);			
		}
		return null;
	}
	public void destroy()
	{
		mConfigMgr=null;
		mExternalConfigParams=null;
	}
}
