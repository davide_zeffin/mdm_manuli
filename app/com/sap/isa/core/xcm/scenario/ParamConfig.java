package com.sap.isa.core.xcm.scenario;

// java imports


/**
 * Container for a scenario parameter
 */
public class ParamConfig {

	private String mName;
	private String mValue;
	private static String CRLF = System.getProperty("line.separator");
 
	/**
	 * Sets name and value
	 * @param name parameter name
	 * @param value parameter value
	 */
	public void setParam(String name, String value) {
		mName = name;
		mValue = value;
	}

	/**
	 * returns the value of this parameter
	 */
	public String getValue() {
		return mValue;
	}

	/**
	 * set the value of this parameter
	 * @param value 
	 */
	public void setValue(String value) {
		mValue = value;
	}


	/**
	 * returns the name of this parameter
	 */
	public String getName() {
		return mName;
	}

	/**
	 * set the name of this parameter
	 * @param name
	 */
	public void setName(String name) {
		mName = name;
	}

	/**
	 * String representation of this parameter configuration
	 * @return representation of this parameter configuration
	 */
	public String toString() {
		StringBuffer sb = new StringBuffer();

		sb.append("Scenario Parameter: [name]='");
		sb.append(mName != null ? mName : "");
		sb.append("' [value]='");
		sb.append(mName != null ? mValue : "");
		sb.append("'").append(CRLF);
		
		return sb.toString();
	}
}
