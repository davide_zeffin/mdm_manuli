package com.sap.isa.core.xcm.scenario;

// java imports
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

import com.sap.isa.core.xcm.init.XCMAdminInitHandler;

/**
 * Stored information about constrains of a scenarion parameter
 */
public class ParamConstrainConfig {
	
	private String mLongtext;
	private String mShorttext;
	private String mName;
	private String mScope;
	private Set mAllowedValues = new LinkedHashSet();
	private static String CRLF = System.getProperty("line.separator");
	
	
	/**
	 * Sets the name of the parameter this constrain belongs to
	 * @param name name of parameter
	 */
	public void setName(String name) {
		mName = name;
	}
	
	/**
	 * Returns the name of the parameter this constrain belongs to
	 * @return name of parameter
	 */
	public String getName() {
		return mName;
	}

	/**
	 * Sets the scope of the parameter 
	 * @param scope scope of parameter
	 */
	public void setScope(String scope) {
		mScope = scope;
	}
	
	/**
	 * Returns the scope of the parameter
	 * @return scope of parameter
	 */
	public String getScope() {
		return mScope;
	}

	
	/**
	 * Adds an allowed value of this parameter configuration
	 * @param allowedValue an allowed value for this parameter
	 */
	public void addAllowedValue(AllowedParamValueConfig allowedValue) {
		mAllowedValues.add(allowedValue);	
	}
	
	/**
	 * Returns a set of allowed values for this parameter
	 * @return a set of allowed values for this parameter
	 */
	public Set getAllowedValues() {
		return Collections.unmodifiableSet(mAllowedValues);
	}
	
	/**
	 * A String represenation of this allowed-value configuration
	 * @return a String represenation of this allowed-value configuration
	 */
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("parameter-constrain [param-name]='");
		sb.append(mName).append("'").append(CRLF);
		
		for (Iterator iter = mAllowedValues.iterator(); iter.hasNext();) {
			AllowedParamValueConfig allowedValue = (AllowedParamValueConfig)iter.next();	
			sb.append("allowed value [base-value]='");
			sb.append(allowedValue.getBasevalue()).append("'").append(CRLF);
		}
		return sb.toString();
	}
	
	public void setLongtext(String longtext) {
		mLongtext = XCMAdminInitHandler.getText(longtext);
	}
	
	public void setShorttext(String shorttext) {
		mShorttext = XCMAdminInitHandler.getText(shorttext);
	}

	public String getLongtext() {
		return mLongtext;
	}
	
	public String getShorttext() {
		return mShorttext;
	}
}