package com.sap.isa.core.xcm.scenario;

// java imports
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import com.sap.isa.core.xcm.init.XCMAdminInitHandler;

/**
 * Encapsulates configuration data for an scenario
 */
public class ScenarioConfig {
	
	private static final String FALSE = "false";
	private static final String TRUE  = "true";
	private static final String OBSOLETE = "obsolete";

	private String mName;
	private String mShorttext;
	private String mLongtext;
	private Map mParams = new LinkedHashMap();
	private Map mConstrains = new LinkedHashMap();
	private static String CRLF = System.getProperty("line.separator");
	private String mIsDefaultScenario = FALSE;
	private String mCcmsScenName;
	private String mIsActiveScenario = TRUE;
	private boolean mObsolete = false;

	/**
	 * Sets the name of this scenario
	 * @param name name of the scenario
	 */
	public void setName(String name) {
		mName = name;
	}

	/**
	 * Returns the name of this scenario configuration
	 * @return the name of this scenario configuration
	 */
	public String getName() {
		return mName;
	}

	/**
	 * Returns an unmodifiable MAP of scenario params
	 * @return an unmodifiable MAP of scenario params
	 */
	public Map getParams() {
		return Collections.unmodifiableMap(mParams);
	}

	/**
	 * Adds scenario configuration parameter
	 * @param configParam configuration parameter
	 */
	public void addParamConfig(ParamConfig param) {
		if ((param != null) && (param.getValue() != null)) 
			mParams.put(param, param.getValue());
	}
	
	/**
	 * Adds an parameter constraing for this scenario
	 * @param config configuration of parameter constrain
	 */
	public void addParamConstrainConfig(ParamConstrainConfig config) {
		if (config != null)
			mConstrains.put(config.getName(), config);
	}
	
	/**
	 * Returns an unmodifiable map of parameter constrains
	 * @return an unmodifiable map of parameter constrains
	 */
	public Map getParamConstrains() {
		return Collections.unmodifiableMap(mConstrains);
	}
	
	/**
	 * String representation of this scenario configuration
	 * @return String representation of this scenario configuration
	 */
	public String toString() {
		StringBuffer sb = new StringBuffer();

		sb.append("Scenario Configuation: [name]='");
		sb.append((mName != null ? mName : ""));
		sb.append("' [ccmsscenname]='");
		sb.append((mCcmsScenName != null ? mCcmsScenName : ""));
		sb.append("'").append(CRLF);
		
		for (Iterator iter = mParams.values().iterator(); iter.hasNext();) {
			sb.append(iter.next().toString());	
			sb.append(CRLF);
		}		
		return sb.toString();
	}
	
	public void setActivescenario(String activeScenario) {
		mIsActiveScenario = activeScenario;		
	}

	public void setDefaultscenario(String defaultScenario) {
		mIsDefaultScenario = defaultScenario;		
	}


	public void setDescription(String descr) {
		mShorttext = XCMAdminInitHandler.getText(descr);
	}
	
	public void setStatus(String status) {
		if (status != null && status.equalsIgnoreCase(OBSOLETE))
			mObsolete = true;
	}
	
	public boolean isObsolete() {
		return mObsolete;
	}

	public String getDescription() {
		return mShorttext;
	}

	public void setShorttext(String descr) {
		mShorttext = XCMAdminInitHandler.getText(descr);
	}

	public String getShorttext() {
		return mShorttext;
	}

	public void setLongtext(String longtext) {
		mLongtext = XCMAdminInitHandler.getText(longtext);
	}

	public String getLongtext() {
		return mLongtext;
	}


	public boolean isDefaultScenario() {
		if (mIsDefaultScenario.equalsIgnoreCase(TRUE))
			return true;
		else
			return false;	
	}


	public boolean isActiveScenario() {
		if (mIsActiveScenario.equalsIgnoreCase(TRUE))
			return true;
		else
			return false;	
	}
	
		
	
	/**
	 * Returns the mCcmsScenarioName.
	 * @return String
	 */
	public String getCcmsscenname() {
		return mCcmsScenName;
	}

	/**
	 * Sets the mCcmsScenarioName.
	 * @param mCcmsScenarioName The mCcmsScenarioName to set
	 */
	public void setCcmsscenname(String ccmsScenarioName) {
		this.mCcmsScenName = ccmsScenarioName;
	}

}