package com.sap.isa.core.xcm.scenario;

// java imports
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Container for allowed values 
 */
public class AllowedParamValueConfig {
	private String mBaseValue;
	private String mValue;
	private String mCompId;
	private Set mDependentParams = new LinkedHashSet();
	private static String CRLF = System.getProperty("line.separator");

	/**
	 * Sets the allowed base-value of this parameter
	 * @param value 
	 */
	public void setBasevalue(String value) {
		mBaseValue = value;
	}

	/**
	 * Returns the base-value of this allowed-values set
	 * @return base-value of this allowed-values set
	 */
	public String getBasevalue() {
		return mBaseValue;
	}

	/**
	 * Sets the allowed value of this parameter
	 * @param value 
	 */
	public void setValue(String value) {
		mValue = value;
	}

	/**
	 * Returns the value of component used to configure this parameter
	 * @return component id
	 */
	public String getComponent() {
		return mCompId;
	}

	/**
	 * Sets the id of the component used to confugre this parameter
	 * @param component id
	 */
	public void setComponenet(String compId) {
		mCompId = compId;
	}

	/**
	 * Returns the value of this parameter
	 * @return value 
	 */
	public String getValue() {
		return mValue;
	}


	/**
	 * Adds the name of an dependent parameter
	 * @param paramName name of dependent parameter
	 */
	public void addDependentParam(String paramName) {
		mDependentParams.add(paramName);
	}

	/**
	 * Returns a set containing name/value pairs of 
	 * dependend parameters for this allowed value
	 */
	public Set getDependentParam(String parmName) {
		return Collections.unmodifiableSet(mDependentParams);
	}

	/**
	 * String representation of this allowed values configuration
	 * @return String representation of this allowed values configuration
	 */
	public String toString() {
		StringBuffer sb = new StringBuffer();

		sb.append("Allowed Value: [base-value]='");
		sb.append(mBaseValue != null ? mBaseValue : "");
		sb.append("'").append(CRLF);
		
		return sb.toString();
	}
}