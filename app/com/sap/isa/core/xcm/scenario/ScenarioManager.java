package com.sap.isa.core.xcm.scenario;

// java imports
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.digester.Digester;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.xcm.ConfigContainer;
import com.sap.isa.core.xcm.ExtendedConfigException;
import com.sap.isa.core.xcm.ExtendedConfigManager;
import com.sap.isa.core.xcm.xml.XMLConstants;



/**
 * This class is used to manage scenarios
 */
public class ScenarioManager  {

	private static ScenarioManager mScenarioManager = null;
	private String mConfigFileName;
	private Map mScenarios = new LinkedHashMap();
	private static String CRLF = System.getProperty("line.separator");
	private static String mDefaultScenario;
	private Set mScenarioSAP = new LinkedHashSet();

	/**
	 * scenario name used when no scenario has been specified 
	 */
	public static final String NAME_DEFAULT_SCENARIO = "bootstrap-scenario";
			
	protected static IsaLocation log = IsaLocation.
        getInstance(ScenarioManager.class.getName());
	
	
	/**
	 * Private constructor
	 */
	private ScenarioManager() {}
	
	/**
	 * Returns a reference to the ScenarioManager
	 * @return a reference to the ScenarioManager
	 */
	public static synchronized ScenarioManager getScenarioManager() {
			
		if (mScenarioManager == null)
			mScenarioManager = new ScenarioManager();
		
		return mScenarioManager;
	}

	/**
	 * Sets the name of the configuration file for the scenario management
	 * @param name of the configuration file for the scenario management
	 */
	public void setConfigFileName(String fileName) {
		mConfigFileName = fileName;
	}

	/**
	 * Initializes the Scenarion Manager. When additional parameters are passed 
	 * then a new configuration is retrieved and the Scenario Mangager is
	 * initialized using the eventually changed configuration
	 * @param params additional configuration parameter ia availbale, otherwise
	 *                <code>null</code>
	 */
	public void init(Map params, String defaultScenario) throws ScenarioConfigException {
		try {
			mScenarios = new LinkedHashMap();
			mDefaultScenario = defaultScenario;
			ExtendedConfigManager exsm = ExtendedConfigManager.getExtConfigManager();
	        ConfigContainer configContainer = exsm.getModifiedConfig(params);	
			InputStream is = configContainer.getConfigAsStream(mConfigFileName);
			readScenarioConfig(is);		
			
			Document scenarioDocs = configContainer.getDocument(mConfigFileName);
			NodeList scenarioList = scenarioDocs.getElementsByTagName("scenario");
			for (int item = 0; item < scenarioList.getLength(); item++) {
				Node scenario = scenarioList.item(item);
				if (scenario.getNodeType() == Node.ELEMENT_NODE) {
					Element scenarioElement = (Element)scenario; 
					String extended = scenarioElement.getAttribute(XMLConstants.EXTENDED_NS);
					if (extended == null || extended.length() == 0) {
						String name = scenarioElement.getAttribute(XMLConstants.NAME);
						mScenarioSAP.add(name);
					}
				}
			}
		} catch (ExtendedConfigException extconfEx) {
			String msg = "Error initializing Scenario Manager ";
			log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.xcm.exception.sm", new Object[] { msg }, extconfEx);
			ScenarioConfigException scex = new ScenarioConfigException(msg);
			scex.setBaseException(extconfEx);
			throw scex;
		}
	}


	/**
	 * Adds a new scenario configuration to this scenario manager
	 * Only active configurations are added
	 * @param config scenario configuration
	 */
	public void addScenarioConfig(ScenarioConfig config) {
		if (config != null) {
			if (!config.isActiveScenario() || config.isObsolete()) {
				if (log.isDebugEnabled())
					log.debug("scenario config for [scenario]='" + config.getName() + "' inactive. skipping scenario");
				return;
			}
			mScenarios.put(config.getName(), config);
		}
	}

	/**
	 * Returns a container with configuration parameters
	 * like configured in the external configuration file
	 * 
	 */
	private void readScenarioConfig(InputStream is) 
				throws ScenarioConfigException {
		
      // new Struts digester
      Digester digester = new Digester();
      digester.setDebug(0);
 
	  ScenarioConfig scenarioConfig = new ScenarioConfig();

      digester.push(this);

    // create a new scenario configuration 
    digester.addObjectCreate("scenarios/scenario",
        "com.sap.isa.core.xcm.scenario.ScenarioConfig");

	digester.addCallMethod("scenarios/scenario/longtext", "setLongtext", 0);

      // set property of scenario configuration
      digester.addSetProperties("scenarios/scenario");

    // create a new scenario configuration parameter 
    digester.addObjectCreate("scenarios/scenario/configparams/configparam",
        "com.sap.isa.core.xcm.scenario.ParamConfig");

    // set properties of scenario configuration parameters
    digester.addSetProperties("scenarios/scenario/configparams/configparam");

	// add configuration parameter to scenario
    digester.addSetNext("scenarios/scenario/configparams/configparam",
        "addParamConfig", "com.sap.isa.core.xcm.scenario.ParamConfig");

    // create a new param constrain configuration
    digester.addObjectCreate("scenarios/scenario/constrains/paramconstraint",
        "com.sap.isa.core.xcm.scenario.ParamConstrainConfig");

	digester.addCallMethod("scenarios/scenario/constrains/paramconstraint/longtext", "setLongtext", 0);

      // set properties of allowed values container
      digester.addSetProperties("scenarios/scenario/constrains/paramconstraint");

    // create a new allowed values container
    digester.addObjectCreate("scenarios/scenario/constrains/paramconstraint/allowedvalue",
        "com.sap.isa.core.xcm.scenario.AllowedParamValueConfig");

      // set properties of allowed values container
      digester.addSetProperties("scenarios/scenario/constrains/paramconstraint/allowedvalue");

/*	// add dependent parameter
    digester.addSetNext("scenarios/scenario/constrains/paramconstrain/allowedvalue/dependentparam/name",
        "addDependentParam", "java.lang.String");
*/
	// add allowed params configuration to parameter constrain	
    digester.addSetNext("scenarios/scenario/constrains/paramconstraint",
        "addParamConstrainConfig", "com.sap.isa.core.xcm.scenario.ParamConstrainConfig");


	// add scenario configuration to configuration manager
    digester.addSetNext("scenarios/scenario",
        "addScenarioConfig", "com.sap.isa.core.xcm.scenario.ScenarioConfig");
	

      try {
        
        if (is != null) {
          digester.parse(is);
		         
			if (log.isDebugEnabled())
				log.debug("Scenario Configuration; " + CRLF + toString());

        } else if (mConfigFileName != null && mConfigFileName.length() > 0) {

          log.info("system.xcm.init.read.smconf", new Object[] { mConfigFileName }, null);

          digester.parse(mConfigFileName);
        } else
          throw new ScenarioConfigException("Error reading configuration information\n" +
              "No input stream or file name set");
       } catch (Exception ex) {
        throw new ScenarioConfigException("Error reading configuration information\n" + ex.toString() );
      } finally {
      	try {
			if (is != null)
	      		is.close();
      	} catch (IOException ioex) {
      		log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.xcm.init.read.smconf", new Object[] { mConfigFileName }, ioex);
    	}
      }
	}


	/**
	 * String representation of all scenario configurations
	 * @return String representation of all scenario configurations
	 */
	public String toString() {

		StringBuffer sb = new StringBuffer();

		for (Iterator iter = mScenarios.values().iterator(); iter.hasNext();) {
			ScenarioConfig config = (ScenarioConfig)iter.next();
			sb.append(config.toString());	
			sb.append(CRLF);
		}		
		return sb.toString();
	}

	
	/**
	 * Returns a configuration for the given scenario name
	 */
	public ScenarioConfig getScenarioConfig(String scenarioName) {
		return (ScenarioConfig)mScenarios.get(scenarioName); 
	}
	
	/**
	 * Returns a map containing configuration parameter for a given scenario
	 * @param scenarioName 
	 * @return a map containing configuration parameter for a given scenario
	 */
	public Map getConfigParams(String scenarioName) {
		
		if (scenarioName == null)
			return new LinkedHashMap();

		ScenarioConfig scenarioConfig = (ScenarioConfig)mScenarios.get(scenarioName);
		if (scenarioConfig == null)
			return new LinkedHashMap();
			
		// get Scenarion params
		Map currentScenaroParams = new LinkedHashMap();
		for (Iterator iter = scenarioConfig.getParams().keySet().iterator(); iter.hasNext();) {
			ParamConfig paramConfig = (ParamConfig)iter.next();
			// add value of current parameter
			currentScenaroParams.put(paramConfig.getName(), paramConfig.getValue());
		}
			
		return currentScenaroParams;
	}
	
	/**
	 * Returns <code>true</code> if the passed scenario name exists
	 * @param name name of scenario
	 */
	public boolean isScenario(String name) {
		if (name.equals(NAME_DEFAULT_SCENARIO))
			return true;
			
		if (mScenarios.get(name) == null)
			return false;
		else
			return true;
	}
	/**
	 * returns an Iterator to retrieve all scenario names
	 * from <code>mScenarios<code>
	 */
	public Iterator getScenarioNamesIterator()
	{
		return mScenarios.keySet().iterator();
	}
	/**
	 * returns an keySet to retrieve all scenario names
	 * from <code>mScenarios<code>
	 */
	public Set getScenarioNames()
	{
		return mScenarios.keySet();
	}

	/**
	 * Specifies the name of the default scenario
	 */
	public String getDefaultScenario(){
		for (Iterator iter = mScenarios.values().iterator(); iter.hasNext(); ) {
			ScenarioConfig config = (ScenarioConfig)iter.next();
			if (config.isDefaultScenario()) {
				String defaultScenario = config.getName(); 
				if (log.isDebugEnabled())
					log.debug ("Default scenario from [scenario-config.xml]='" + defaultScenario + "'");
				return defaultScenario;
			}
		}
		if (log.isDebugEnabled())
			log.debug("Default scenario from [web.xml]='" + mDefaultScenario + "'");
		return mDefaultScenario;

	}

	/**
	 * Returs true if the passed scenario name is a SAP scenario
	 * @param name scenario name
	 * @return true if the passed scenario name is a SAP scenario
	 */
	public boolean isSAPScenario(String name) {
		if (mScenarioSAP.contains(name))
			return true;
		else
			return false;
	}
	/**
	 * Cleanup for garbage collection
	 */
	public void destroy()
	{
		mScenarioSAP=null;
		mDefaultScenario=null;
		mScenarioManager=null;
	}
}

