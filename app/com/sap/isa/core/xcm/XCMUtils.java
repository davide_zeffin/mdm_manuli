package com.sap.isa.core.xcm;

// java imports
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.sap.engine.lib.xml.dom.xpath.XPathEvaluatorImpl;
import com.sap.engine.lib.xml.dom.xpath.XPathExpressionImpl;
import com.sap.engine.lib.xml.dom.xpath.XPathResultImpl;
import com.sap.engine.lib.xml.dom.xpath.XPathSetSnapshotImpl;
import com.sap.isa.core.Constants;
import com.sap.isa.core.cache.Cache;
import com.sap.isa.core.ccms.ComponentTestThread;
import com.sap.isa.core.eai.sp.jco.JCoManagedConnectionFactory;
import com.sap.isa.core.init.InitializationHandler;
import com.sap.isa.core.jmx.ISAVersionResource;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.test.ComponentTest;
import com.sap.isa.core.util.VersionGet;
import com.sap.isa.core.xcm.init.XCMAdminComponentMetadata;
import com.sap.isa.core.xcm.init.XCMAdminInitHandler;
import com.sap.isa.core.xcm.init.XCMAdminParamConstrainMetadata;
import com.sap.isa.core.xcm.scenario.ScenarioConfig;
import com.sap.isa.core.xcm.scenario.ScenarioManager;
import com.sap.isa.core.xcm.xml.XMLUtil;

public class XCMUtils {
	
		
	protected static IsaLocation log = IsaLocation.
		getInstance(XCMUtils.class.getName());

	
	private static String getText(String text) {
			if (text == null)
				return "";
			else
				return text;
	
	}
	
	/**
	 * Returns XCM Configuration parameter meta data 
	 * @param paramName name of XCM configuration name
	 * @return XCM Configuration parameter meta data
	 */
	public static XCMAdminParamConstrainMetadata getScenarioConfigParamMetadata(String paramName) {
		return XCMAdminInitHandler.getXCMAdminScenarioMetadata(paramName);
	}
	
	
	/** 
	 * Get config param shorttext
	 */
	public static String getScenarioConfigParamLongtext(String configParam) {
		
		XCMAdminParamConstrainMetadata scenMetaData = XCMAdminInitHandler.getXCMAdminScenarioMetadata(configParam);
		if (scenMetaData == null) {
			String msg = "Configuration parameter [name]='" + configParam + "' not found"; 
			log.warn("system.xcm.warn", new Object[] { msg }, null);
			return "";
		}
		// check if this parameter is configured using component
		String compId = scenMetaData.getComponent();
	
		if (compId == null)
			return getText(scenMetaData.getLongtext());
		
		XCMAdminComponentMetadata compMetaData = XCMAdminInitHandler.getComponentMetadata(compId);
		
		if (compMetaData == null)
			return "";
		else
			return getText(compMetaData.getLongtext()); 		
	}
	/**
	 * Get config param shorttext
	 */
	public static String getScenarioConfigParamShorttext(String configParam) {
		
		XCMAdminParamConstrainMetadata scenMetaData = XCMAdminInitHandler.getXCMAdminScenarioMetadata(configParam);
		if (scenMetaData == null) {
			String msg = "Configuration parameter [name]='" + configParam + "' not found"; 
			log.warn("system.xcm.warn", new Object[] { msg }, null);
			return "";
		}
		// check if this parameter is configured using component
		String compId = scenMetaData.getComponent();
	
		if (compId == null)
			return getText(scenMetaData.getShorttext());
		
		XCMAdminComponentMetadata compMetaData = XCMAdminInitHandler.getComponentMetadata(compId);
		
		if (compMetaData == null)
			return "";
		else
			return getText(compMetaData.getShorttext()); 		
	}
	/**
	 * Returns shorttext for value of config param
	 */
	public static String getScenarioConfigParamValueLongtext(String configParam, String configValue) {
		XCMAdminParamConstrainMetadata scenMetaData = XCMAdminInitHandler.getXCMAdminScenarioMetadata(configParam);
		if (scenMetaData == null) {
			String msg = "Configuration parameter [name]='" + configParam + "' not found"; 
			log.warn("system.xcm.warn", new Object[] { msg }, null);
			return "";
		} 
	
		// check if this parameter is configured using component
		String compId = scenMetaData.getComponent();
		
		return getText(scenMetaData.getAllowedValueLongtext(configValue));
	}
	public static String getCompConfigLongtext(String compId, String configName) {
	
		XCMAdminParamConstrainMetadata scenMetaData = 
		XCMAdminInitHandler.getXCMAdminScenarioMetadataComp(compId);
		
		if (scenMetaData == null) {
			String msg = "No meta data found for component [id]='" + compId + "'";
			log.warn("system.xcm.warn", new Object[] { msg }, null);
			return "";
			}
		return getText(scenMetaData.getAllowedValueLongtext(configName));
	}
	/**
	 * Returns longtext for component config param
	 */
	public static String getCompConfigParamLongtext(String compId, String configParamName) {
		XCMAdminComponentMetadata md = XCMAdminInitHandler.getComponentMetadata(compId);
		if (md == null) {
			String msg = "No meta data found for component [id]='" + compId + "'";
			log.warn("system.xcm.warn", new Object[] { msg }, null);
			return "";
		}		
 		return getText(md.getParamLongtext(configParamName));
	}
	/**
	 * Returns shorttext for component config param
	 */
	public static String getCompConfigParamShorttext(String compId, String configParamName) {
		XCMAdminComponentMetadata md = XCMAdminInitHandler.getComponentMetadata(compId);
		return getText(md.getParamShorttext(configParamName));
	}
	
	/**
		 * Returns status for component config param
		 */
		public static String getCompConfigParamStatus(String compId, String configParamName) {
			XCMAdminComponentMetadata md = XCMAdminInitHandler.getComponentMetadata(compId);
			if (md == null) {
				String msg = "No meta data found for component [id]='" + compId + "'";
				log.warn("system.xcm.warn", new Object[] { msg }, null);
				return "";
			}		
			return md.getParamStatus(configParamName);
		}
	
	
	/**
	 * Returns longtext for component config param value
	 */
	public static String getCompConfigParamValueLongtext(String compId, String configParamName, String configParamValue) {
		XCMAdminComponentMetadata md = XCMAdminInitHandler.getComponentMetadata(compId);
		return getText(md.getParamAllowedValueLongtext(configParamName, configParamValue));
	}
	/**
	 * Returns shorttext for component config param value
	 */
	public static String getCompConfigParamValueShorttext(String compId, String configParamName, String configParamValue) {
		XCMAdminComponentMetadata md = XCMAdminInitHandler.getComponentMetadata(compId);
		return getText(md.getParamAllowedValueShorttext(configParamName, configParamValue));
	}

	/**
	 * Returns the type of a component value
	 */
	public static String getCompConfigParamType(String compId, String configParamName) {
		XCMAdminComponentMetadata md = XCMAdminInitHandler.getComponentMetadata(compId);
		return md.getParamType(configParamName);
	}	
	
	public static String getCompConfigShorttext(String compId, String configName) {
	
		XCMAdminParamConstrainMetadata scenMetaData = 
		XCMAdminInitHandler.getXCMAdminScenarioMetadataComp(compId);
		
		if (scenMetaData == null) {
			String msg = "No meta data found for component [id]='" + "'";
			log.warn("system.xcm.warn", new Object[] { msg }, null);
			return "";
			}
		 
		return getText(scenMetaData.getAllowedValueShorttext(configName));
	}

	public static boolean isCompConfigObsolete(String compId, String configName) {
	
		XCMAdminParamConstrainMetadata scenMetaData = 
		XCMAdminInitHandler.getXCMAdminScenarioMetadataComp(compId);
		
		if (scenMetaData == null) {
			String msg = "No meta data found for component [id]='" + "'";
			log.warn("system.xcm.warn", new Object[] { msg }, null);
			return false;
			}
		 
		return scenMetaData.isAllowedValueObsolete(configName);
	}
	
	
	public static String getCompLongtext(String compId) {
		XCMAdminComponentMetadata cmpMetaData = XCMAdminInitHandler.getComponentMetadata(compId);
		if (cmpMetaData == null)
			return "";
	
		return getText(cmpMetaData.getLongtext());
	}
	public static String getCompShorttext(String compId) {
		XCMAdminComponentMetadata cmpMetaData = XCMAdminInitHandler.getComponentMetadata(compId);
		if (cmpMetaData == null)
			return "";
	
		return getText(cmpMetaData.getShorttext());
	}
	
	/**
	 * Returns component metadata 
	 * @param compId component id 
	 * @return the meta data or <code>null</code> if component not available
	 */
	public static XCMAdminComponentMetadata getComponentMetadata(String compId) {
		return XCMAdminInitHandler.getComponentMetadata(compId);
	}
 	

    /**
     * Returns a map containing configuration parameters used to configure
     * the Extended Configuration Management
     * This properties are recognized by the following format:
     * <request parameter name>.xcm
     * @param request the http request
     * @param props the found properties are added to this set of properties
     * @return mixed properties
     */  
	public static Map getExtConfProps(Properties props) {

		if (log.isDebugEnabled())
			log.debug("START getExtConfProps(Properties props), passed [props]='" + props + "'");
		
		if (props == null)
			return new LinkedHashMap();

		Map xcmProps = new LinkedHashMap();

		String scenarioName = props.getProperty(Constants.XCM_SCENARIO_RP);
		if (scenarioName == null)
			scenarioName = props.getProperty(Constants.XCM_CONFIGURATION_RP);
		
		if (scenarioName == null)
			scenarioName = 	ScenarioManager.NAME_DEFAULT_SCENARIO;
		
		if (log.isDebugEnabled())
			log.debug("Scenario [name]='" + scenarioName + "'");

		ScenarioManager sm = ScenarioManager.getScenarioManager();

		xcmProps = sm.getConfigParams(scenarioName);

		if (log.isDebugEnabled()) {
			StringBuffer sb = new StringBuffer();
			for (Iterator iter = xcmProps.keySet().iterator(); iter.hasNext();) {
	 			String paramName = (String)iter.next();
				String paramValue = (String)xcmProps.get(paramName);
				sb.append("[name]='").append(paramName);
				sb.append("' [value]='").append(paramValue).append("'; ");
			}
			log.debug("Scenario [params]='" + sb.toString() + "'");
		}
	
		// overwrite scenario params with passed params
		for (Enumeration enum = props.keys(); enum.hasMoreElements();) {
			String paramName = (String)enum.nextElement();
			String paramValue = props.getProperty(paramName);
			// check if request parameter is an Extended Configuration Management parameter
			if ((paramName.length() > 4) && (paramName.lastIndexOf(XCMConstants.XCM_CONF_PARAM_EXTENSION) == (paramName.length() - 4))) {
				String xcmName = paramName.substring(0, paramName.length() - 4);
				if (log.isDebugEnabled())
					log.debug("XCM configuration parameter [name]='" + xcmName 
							+ "' [value]='" + paramValue + "'");
				xcmProps.put(xcmName, paramValue);
			}
		}

		for (Iterator iter = xcmProps.keySet().iterator(); iter.hasNext();) {
			String paramName = (String)iter.next(); 
			if(log.isDebugEnabled())
				log.debug("XCM params: " + paramName + " " + xcmProps.get(paramName));
		}


		
		return xcmProps;	
	}  

	/**
     * Returns Properties containing configuration parameters used to configure
     * the Extended Configuration Management
     * @param confDataXML is a document that contains the config-data.xml
     * <pre> 
     * <data>
     *   <component id= @param compID>
     *     <configs>
     *       <config id= @param confID>
     *		   <params id="">
     * 			 <param name="" value="" savevalue=""/>
     * 		   </params>
     * ...
     * </pre>
     * @param compID contains the component ID (in the confDataXML)
     * @param confID contains the configuration ID (in the confDataXML)
     * 
     */ 
	public static Properties getCompConfProps(Document confDataXML, String compID, String confID){
		Properties props = new Properties();
		
		if (log.isDebugEnabled())
			log.debug("START getCompConfProps(String compID, String confID), passed [compID]='" + compID + "', [confID]='" + confID + "'");
		
		if (compID == null || confID == null)
			return( new Properties());
		
		String knot = "/data/component[@id='" + compID +"']/configs/config[@id='" + confID +"']/params/*";
			
		XPathEvaluatorImpl e  = new XPathEvaluatorImpl();    
	    XPathExpressionImpl expr = e.createExpression(knot,null);  
	    XPathResultImpl res = new XPathResultImpl();
		 
	    try{  		    
			res = expr.evaluate(confDataXML, (short)0, res);
			if (res.getResultType() < 3){ // if the result not consist of one or more nodes
				if (log.isDebugEnabled())
					log.debug("didn't found any properties in the configuration " + confID + " for the component " + compID);
				return new Properties();
			}//if (res.getResultType() < 3)
			
			XPathSetSnapshotImpl shot = res.getSetSnapshot(true); // obtain a snapshot of the result
			
			if (log.isDebugEnabled())
				log.debug("found"  + shot.getLength() + " parameters:");
			
			String tmpName="";
			String tmpValue="";
					
			for(int i=0; i<shot.getLength(); i++){
				NamedNodeMap attributes = shot.item(i).getAttributes();
				tmpName = attributes.getNamedItem("name").getNodeValue();
				tmpValue = attributes.getNamedItem("value").getNodeValue();
				
				if(log.isDebugEnabled()==true)
					log.debug("name = " + tmpName + "; value = " + tmpValue);
				
				props.put( tmpName, tmpValue);			
			}//for(int i=0; i<shot.getLength(); i++)
			JCoManagedConnectionFactory.decryptPassword(props);

			if(log.isDebugEnabled())
				log.debug("properties read succesfully");			
			return(props);
		} catch(NullPointerException err){
			log.debug(err.getMessage());
			return( new Properties());
	    } catch(Throwable err){
			log.debug(err.getMessage());
	    	return( new Properties());
	    }	
	}//getCompConfProps(String compID, String confID)
	
	/**
	 * Launches XCMTests and returns a map with the test results.
	 * belongs to CCMS - Heartbeat for ISA
	 * @param scenarioName
     * @param xcmadminConfigPath
     * @param components is an enumeration of names of components to test.
     * Each name must be a concatenation of  component id, "##" and configuration
     * E.g. "jco##Q4C_705"
     * 
	 * @throws TransformerException
     * @throws TransformerConfigurationException
     * @throws ExtendedConfigException
     * @return returns a <code>Map</code> containing all Components for the 
     *  given scenarioName. But it executes the tests for the <code>components</code>
     *  passed in the components <code>Set</code> only.
	 */
	public Map executeComponentTestsForScenario(String scenarioName, 
		 Document confDataXML, Set components, Set additionalTests) 
		 throws ExtendedConfigException
	{	
		Map testListMap=new LinkedHashMap();
		
		if (log.isDebugEnabled())
			log.debug("START executeTestsForScenario for scenario"  + scenarioName);

		Node document = getComponentConfigurationXML(scenarioName, false);
		getComponentTests(document, testListMap);
		if( additionalTests != null )
		{
			Iterator iter = additionalTests.iterator();	
			while( iter.hasNext())
			{
				XCMTestProperties addTest= (XCMTestProperties)iter.next();
				testListMap.put( addTest.getComponentID() + "##" + addTest.getConfigID(), addTest );
				if( log.isDebugEnabled() )
				   log.debug("Additional Test added " + addTest.getComponentID() + "##" + addTest.getConfigID());
			}
		}
		if(log.isDebugEnabled()) {
			log.debug("Found " + testListMap.size() +" Component tests for the XCM-test scenario " + scenarioName);
			if( components != null )
				log.debug("Number of requested tests: " + components.size());
		}//if
		
		Iterator keyIter;
		// if there is no component list, execute all available tests.
		if( components==null )
		   keyIter = testListMap.keySet().iterator();
		else  
			keyIter=components.iterator();
		Vector threads= new Vector(4,4);	
		int timeOutInMillis=1000;
		while(keyIter.hasNext())
		{
			String componentKey=(String)keyIter.next();
			XCMTestProperties currentTest = (XCMTestProperties)testListMap.get(componentKey);
			if( currentTest == null )
			{
				
				Iterator iter=testListMap.keySet().iterator();
				StringBuffer sb= new StringBuffer("");
				while( iter.hasNext())
				{
					sb.append("\'" );
					sb.append(iter.next().toString());
					sb.append("\', " );
				}
				
				log.warn("ccms.no.testclass.found", new Object[]{ componentKey, sb.toString() }, null );
				if( log.isDebugEnabled())
					log.debug("testclass not found component " + componentKey + "Map: " +  sb.toString());
			}
			else
			{

				ComponentTestThread testThread= new ComponentTestThread( componentKey);
				ComponentTest testClass;
				String testClassName, tmpTestDisc, tmpTestName;
				String testName = null;
				boolean tmpTestResult = false;
				Properties testProps =
					XCMUtils.getCompConfProps(
					confDataXML,
					currentTest.getComponentID(),
					currentTest.getConfigID());
				testClass = currentTest.getTestClass();
				testThread.setTestClass(testClass);
				if( testClass.getMaxTimeout()>timeOutInMillis )
					timeOutInMillis=testClass.getMaxTimeout();

				Iterator iter = testClass.getTestNames().iterator();
				while(iter.hasNext())
				{
					testClass.setTestParameters((String)iter.next(),testProps);
				}				
				threads.add(testThread);
				testThread.start();				
			}
		}
		long startTime=System.currentTimeMillis();
		Iterator iter = threads.iterator();
		while(iter.hasNext())
		{
			try {
				ComponentTestThread currentThread=(ComponentTestThread)iter.next();
				long now=System.currentTimeMillis();
				long timeSinceStart = now - startTime;
				if( timeSinceStart < timeOutInMillis)
				{
					currentThread.join((timeOutInMillis -timeSinceStart),0);
				}
				if( currentThread.isAlive())
				{
					currentThread.setTimeout();
					log.error(LogUtil.APPS_BUSINESS_LOGIC,"highav.test.timed.out", new Object[]{currentThread.getName()},null);
				}
			}
			catch( InterruptedException ex)
			{
				log.debug(ex.getMessage());
			}
		};		
		return testListMap;
	}//executeTestsForScenario

	/**
	 * Method getCcmsCustomizingForScenario.
	 * Creates a customizing heartbeat node for a single scenario
	 * It sets the selected attribute in a component nodes to "yes"
	 *   
	 * @param url to call by CCMS ( e.g. http://yourhost:50000)
	 * @param scenarioNames XCM scenario Name of the scenarios to heartbeat.
	 * @param ccmsFile Name of the output File
	 * @param xslFilename Name for the xsl file which
	 * @deprecated Use the new method with passes the scenariosNode it
	 * With the new signatur a component selection is possible
	 * @throws ExtendedConfigException, TransformerConfigurationException, TransformerConfigurationException, TransformerException
	 */
	public void createCcmsCustomizingForScenarios(String url, Map scenariosToTest
		 , File ccmsFile, File xslFilename) 
		 throws ExtendedConfigException, TransformerConfigurationException, TransformerConfigurationException, TransformerException
	{	
		Node document = getComponentConfigurationXML(null, false );	
		Node scenariosNode = document.getFirstChild();
		Node scenario= scenariosNode.getFirstChild();
		while( scenario != null)
			{
				Node tests=scenario.getFirstChild();
				while( tests != null)
				{
					Node attr=tests.getOwnerDocument().createAttribute("selected");
					attr.setNodeValue("yes");
					NamedNodeMap nm=tests.getAttributes();
					if( nm != null)
						nm.setNamedItem(attr);
					tests=tests.getNextSibling();
				}
				scenario=scenario.getNextSibling();
			}
		// for compatibility reasons, all scenarios are
		createCcmsCustomizingForScenarios(url, scenariosToTest, ccmsFile, xslFilename, scenariosNode);
	}
	/**
	 * Method getCcmsCustomizingForScenario.
	 * Creates a customizing heartbeat node for a single scenario 
	 * @param url to call by CCMS ( e.g. http://yourhost:50000)
	 * @param scenarioNames XCM scenario Name of the scenarios to heartbeat.
	 * @param ccmsFile Name of the output File
	 * @param xslFilename Name for the xsl file which
	 * @param scenariosNode Nod containing the scenarios to create the heartbeats for.
	 * @throws ExtendedConfigException, TransformerConfigurationException, TransformerConfigurationException, TransformerException
	 */	
	public void createCcmsCustomizingForScenarios(String url, Map scenariosToTest
			 , File ccmsFile, File xslFilename, Node scenariosNode) 
			 throws ExtendedConfigException, TransformerConfigurationException, TransformerConfigurationException, TransformerException
		{	
		if (log.isDebugEnabled())
			log.debug("START getCcmsCustomizingForScenario");

		// Delete Nodes which are not in scenariosToTest
		Node scenarioNode = scenariosNode.getFirstChild();
		String scenName = null;
		Vector deleteCandiates = new Vector();
		while( scenarioNode != null)
			{
				Node currentname=null;
				if(
					scenarioNode.hasAttributes() &&
					null !=( currentname = scenarioNode.getAttributes().getNamedItem("scenario-name")) && 
					scenariosToTest.containsKey(currentname.getNodeValue()))
				{
					scenName = scenarioNode.getAttributes().getNamedItem("scenario-name").getNodeValue();
					XCMTestProperties additionalTests[] =(XCMTestProperties[]) scenariosToTest.get(currentname.getNodeValue());
					if(additionalTests!= null )
						for( int k = 0; k < additionalTests.length; k++ )
							if( additionalTests[k].getComponentID() != null &&
								additionalTests[k].getConfigID() !=null )
							{
								Element component=scenarioNode.getOwnerDocument().createElement("component");
								scenarioNode.appendChild(component);
								component.setAttribute("id",additionalTests[k].getComponentID() );
								component.setAttribute("configuration", additionalTests[k].getConfigID() );
								String ccmsname=additionalTests[k].getCcmsName();
								if( ccmsname!= null && ccmsname.length()> 0)
									component.setAttribute("ccmsname",ccmsname);
								else
									component.setAttribute("ccmsname",additionalTests[k].getConfigID());
							}
				}
				else
				{
					if(log.isDebugEnabled() && currentname != null)
						log.debug("Delete Node " + currentname.getNodeValue());
				 	deleteCandiates.add( scenarioNode);
				}
				scenarioNode = scenarioNode.getNextSibling();
			}

    	Iterator iter = deleteCandiates.iterator();
    	while( iter.hasNext())
    	{
    	 	Node n = (Node)iter.next();
    	 	scenariosNode.removeChild(n);
    	}
	  // generate ccms customizing 
	  
		  
   	StreamSource xslSource;
   	if( xslFilename == null)
   	{
   		xslSource = new StreamSource(getClass().getResourceAsStream("/ccmsCustomizing.xsl"));
	   	xslSource.setSystemId("ccmsCustomizing.xsl");
   	}
   	else
   		xslSource = new StreamSource(xslFilename);
		
		TransformerFactory tf = XMLUtil.getTransformerFactory(this);
		Templates stylesheets= tf.newTemplates(xslSource);
		Transformer xslt=stylesheets.newTransformer();
		String sol = ISAVersionResource.getSolution();
		String app = ISAVersionResource.getApplication();
		String applicationName = InitializationHandler.getApplicationName();
		String scenDesUrl = url;
		if (sol == null || app == null) {
			VersionGet vg = new VersionGet();
			sol = vg.getSolution();
			app = vg.getApplication();
		}
		
		try {
			URL realURL = new URL(url);
			scenDesUrl = realURL.getHost() + ":" + realURL.getPort();
		} catch (MalformedURLException ex) {
			log.debug(ex.getMessage());
			// use url string as it is
		}
		
		String scenDesc =  sol + "." + app + "." + applicationName + "." ;
		xslt.setParameter("scendesc", scenDesc);
		
		if(url != null)
			xslt.setParameter("URL", url);
		StreamResult res=new StreamResult(ccmsFile);
		xslt.transform(new DOMSource(scenariosNode), res);
	}
	/**
	 * Returns an xml formated String containing the testclass, component id and configuration id
     * of all XCM - Test, that belong to the scenario @param scenarioName
	 * @param scenarioName:		Name of the XCM-test scenario wich should be run
	 * @param customerScenariosOnly: returns Nodes only for Customer XCM scenarios  
	 * @throws ExtendedConfigException If there is no scenario node in the result
     */ 
	public Node getComponentConfigurationXML(String scenarioName, boolean customerScenariosOnly)
		throws ExtendedConfigException
	{
		ScenarioManager sm=ScenarioManager.getScenarioManager();
		Iterator scenIter= sm.getScenarioNamesIterator();
		
		try{
		DocumentBuilderFactory factory = XMLUtil.getDocBuilderFactory(this);
		Document doc= factory.newDocumentBuilder().newDocument();
		Element e=doc.createElement("scenarios");
		doc.appendChild(e);
		int componentId=0;
		if( scenarioName == null )
		{
			while( scenIter.hasNext())
			{
				String s=(String)scenIter.next();
				if( !customerScenariosOnly || !sm.isSAPScenario(s))
				{
					componentId = createTestsNode( doc,s, sm, componentId);
				}
			}
		}
		else
		{
			if( !customerScenariosOnly || !sm.isSAPScenario(scenarioName))
			{
				int newComponentId = createTestsNode( doc,scenarioName, sm, componentId);
				if( newComponentId == componentId )
				{
					ExtendedConfigException ex=new ExtendedConfigException( "The scenarioconfig.xml has no <scenarios> node");
					ex.fillInStackTrace();
					log.debug("ccms.noscenario.found: Scenario: " + scenarioName);
					throw ex;
				}
			}
		}
		return(doc);

		}catch( ParserConfigurationException ex)
		{
				ExtendedConfigException ex1=new ExtendedConfigException( "Error creating Document element");
				ex1.setBaseException(ex);
				throw ex1;
		}
	}//getComponentConfigurationXML
	/**
	 * returns a document of the following structure
	 * @pre
	 *  <tests scenario-name="??" ccmsscenname="??">
	 * 		<test configuration="??" id="??" testclass="??">
	 *      <test ...>
	 * If the there is no test defined for the scenario, a null Pointer will be returned
	 */
    private int createTestsNode( Document doc, String scenarioName, ScenarioManager sm, int componentId)
	{		
		boolean testFound=false;
		ScenarioConfig sc= sm.getScenarioConfig(scenarioName);
		Element tests=doc.createElement("tests");
		tests.setAttribute("scenario-name", scenarioName);
		tests.setAttribute("ccmsscenname", sc.getCcmsscenname());
		Map configParams = sm.getConfigParams(scenarioName);
		Iterator cfgnames= configParams.keySet().iterator();
		StringBuffer sb= new StringBuffer("TestScenario ");
		if( log.isDebugEnabled())
		{
			sb.append( scenarioName);
			sb.append( " Tests found for Configurations:");
		}
		while( cfgnames.hasNext())
		{
			String cfgname=(String) cfgnames.next();
			boolean testForCfg = createComponentNodes(tests, cfgname, (String)configParams.get(cfgname), componentId);
			testFound |= testForCfg;
			if( testForCfg )
			{
				componentId++;
				if( log.isDebugEnabled())
				   sb.append( " " ).append(cfgname);
			}
		}
		if( log.isDebugEnabled())
			log.debug(sb.toString());
		if( ! testFound)
			return componentId;
		NodeList n = doc.getElementsByTagName("scenarios");
		n.item(0).appendChild(tests);
		return componentId;
	}

	/**
	 * Appends a test Element to the given <code>node<code> to the node 
	 * parameter if there is a test class in the XCMAdmin-config defined for it.
	 */
	private boolean createComponentNodes( Element node, String configparam, String configvalue, int componentId )
	{
		if( configparam==null)
			return false;
		XCMAdminParamConstrainMetadata sc= XCMAdminInitHandler.getXCMAdminScenarioMetadata(configparam);
		if( sc ==null)
			return false;
		String componentname = sc.getComponent();
		XCMAdminComponentMetadata d;
		if( componentname != null && 
			null !=(d= XCMAdminInitHandler.getComponentMetadata(componentname)) &&
			null !=(d.getTestclass())
		)
		{
			Document doc=node.getOwnerDocument();
			Element e=doc.createElement("component");
			e.setAttribute("configuration",configvalue);
			e.setAttribute("id",d.getId());
			e.setAttribute("testclass",d.getTestclass());
			String ccmsname=d.getCcmsname();
			if( ccmsname!= null && ccmsname.length()> 0)
				e.setAttribute("ccmsname",ccmsname);
			else
				e.setAttribute("ccmsname",d.getId());
			e.setAttribute("index", Integer.toString(componentId));
			node.appendChild(e);
			return true;
		}
		return false;
	}


	/**
     * Reads the testclass, component id and configuration id
     * of all XCM - Test in the xmlFile and returns them as XCMTestProperties.
     * @param xmlFile
     * <pre> 
     * <tests>
     *   <...>
     * ...
     * </pre>
     */ 
	private void getComponentTests(Node xmlFile, Map tests){	
		String knot = "/scenarios/tests/*";
			
		XPathEvaluatorImpl e  = new XPathEvaluatorImpl();    
	    XPathExpressionImpl expr = e.createExpression(knot,null);  
	    XPathResultImpl res = new XPathResultImpl();
		 
			res = expr.evaluate(xmlFile, (short)0, res);
			if (res.getResultType() < 3) // if the result not consist of one or more nodes
				return;			
			XPathSetSnapshotImpl shot = res.getSetSnapshot(true); // obtain a snapshot of the result
			
			XCMTestProperties test;
			for(int i=0; i<shot.getLength(); i++){
					NamedNodeMap attributes = shot.item(i).getAttributes();
					test=new XCMTestProperties();
					Node n = attributes.getNamedItem("ccmsname");
					String id = attributes.getNamedItem("id").getNodeValue();
					if( n != null)
						test.setCcmsName( n.getNodeValue());
					String configuration=attributes.getNamedItem("configuration").getNodeValue();
					test.setComponentID( id);
					test.setConfigID(configuration );
				try {
					test.setTestClass( attributes.getNamedItem("testclass").getNodeValue());
					tests.put(id+"##"+ configuration, test );
				}
				catch( Exception ex)
				{
					log.error(LogUtil.APPS_BUSINESS_LOGIC,"ccms.no.testclass.found", new Object[]{id + "##" + configuration},ex);
				}
			}//for(int i=0; i<shot.getLength(); i++)
	}//getComponentID
	
	/**
	 * Returns the configuration of the currently loaded scenarios 
	 * @return a map containing the currently loaded scenario configurations
	 */
	public static Map getCurrentLoadedScenarioConfigs() {
		
		Map scenarios = new LinkedHashMap();
		Cache.Access access = null;
		try {
			access = Cache.getAccess(XCMConstants.CACHE_REGION_NAME);
		} catch (Cache.Exception cex) {
			String msg = "Erorr accessing cache [region]='" + XCMConstants.CACHE_REGION_NAME + "'";
			log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.exception", new Object[] { msg }, cex);
			return Collections.EMPTY_MAP;
		}
		Set keys = access.getKeys();
			
		// iterate through all loaded scenarios
		// ommit the bootstrap scenario 
		for (Iterator iter = keys.iterator(); iter.hasNext();) {
			String key = (String) iter.next();
			ConfigContainer cc = (ConfigContainer) access.get(key);
			String scenName = cc.getScenarioName();
			if(log.isDebugEnabled()) 
				log.debug("processing [scenario]='" + scenName + "'");
			if (scenName.equals(ScenarioManager.NAME_DEFAULT_SCENARIO))
				continue;
			scenarios.put(scenName, cc);
		}
		return scenarios;	
	}
	
	
	public static boolean isApplicationScopeCompConfig(String id) {
		
		XCMAdminComponentMetadata md = XCMAdminInitHandler.getComponentMetadata(id);
		if (md == null) {
			log.warn("system.xcm.warn", new Object[] { "Metadata for [component]='" + id + "' missing!" }, null);
			return false;
		}
			
		String scope = md.getScope();
		if (scope.equals(XCMAdminComponentMetadata.SCOPE_APPLICATION))
			return true;
		else
			return false;
	}

}
