package com.sap.isa.core.xcm;

public interface XCMConstants {

	/**
	 * Extension used to distinguish ECM parameters
	 */
	public static final String XCM_CONF_PARAM_EXTENSION = ".xcm";

	/**
	 * Specifies that a configuration parameter or a file
	 * has application scope 
	 * Value: 'application'
	 */
	public static final String SCOPE_APPLICATIN = "application";
	
	/**
	 * Specifies that a configuration parameter or a file has 
	 * session scope
	 * Value: 'session'
	 */
	public static final String SCOPE_SESSION = "session";

	/**
	 * If this trace level is enabled everything is traces if
	 * global log level is set to DEBUG
	 */
	public static int TRACE_ALL = 10;

 
	/**
	 * If this trace level is enabled path information is traced
	 * if global log level is set to DEBUG
	 */
	public static int TRACE_PATH = 5;

		
	/**
	 * Scenario parameter used to store the context path
	 */ 
	public static String CONTEXT_PATH = "context_path";



	public static final String CACHE_REGION_NAME = "XCM_SESSION_SCOPE";


	/**
	 * Default name of config-data.xml file 
	 */
	public static final String DEFAULT_FILE_NAME_CONFIG_DATA     = "config-data.xml";
	/**
	 * Default name of scenario-config.xml file
	 */
	public static final String DEFAULT_FILE_NAME_SCENARIO_CONFIG = "scenario-config.xml";
	/**
	 * Default configuration description (Internet Sales)
	 */
	public static final String DEFAULT_CONFIG_DESCRIPTION        = "Internet Sales";
	/**
	 * Alias of config-data.xml file
	 */
 	public static final String CONFIG_DATA_FILE_ALIAS = "config-data";

	/**
	 * Alias of config-data.xml file
	 */
	public static final String SCENARIO_CONFIG_ALIAS = "scenario-config";
 	
 	public static final String DATASOURCE_LOOKUP_NAME = "java:comp/env/SAP/CRM_XCM_DB";

}
