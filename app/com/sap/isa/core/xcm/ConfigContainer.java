package com.sap.isa.core.xcm;

// java imports
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;

import com.sap.isa.core.cache.Cache;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.xcm.init.ExtendedConfigInitHandler;
import com.sap.isa.core.xcm.xml.DocumentCache;
import com.sap.isa.core.xcm.xml.ExtensionProcessor;
import com.sap.isa.core.xcm.xml.ParamsStrategy;
import com.sap.isa.core.xcm.xml.XIncludeProcessor;
import com.sap.isa.core.xcm.xml.XMLUtil;

/**
 * This class is used to access a configuration.
 * 
 */
public class ConfigContainer
	implements XIncludeProcessor.DocumentListener, DocumentCache, Serializable {

	/**
	 * Name of Cache region used to store application scope configuration files
	 * Value: XCM_APP_SCOPE
	 */
	public static final String CACHE_REGION_APPL_SCOPE_CONFIG = "XCM_APP_SCOPE";

	protected static IsaLocation log =
		IsaLocation.getInstance(ConfigContainer.class.getName());

	private String mScenarioName;
	private Map mDOMConfigs = new HashMap();

	private ExtensionProcessor.ExtensionKeys mExtKeys;
	private ParamsStrategy.Params mParams;

	private Map mIncludeDocHrefs  = new HashMap();
	private Map mIncludeDocScopes = new HashMap();
	private Map mFileAliases      = new HashMap();


	private Set mKeys = new TreeSet();
	private String mConfigKey = "";

	private static String CRLF = System.getProperty("line.spearator");

	private static int mTraceLevel = XCMConstants.TRACE_ALL;

	private transient Cache.Access mCacheAccess = null;

	public ConfigContainer() {

		initCache();
	}

	public static class DocPath implements Serializable {
		private final String mBase;
		private final String mHref;

		public DocPath(String base, String href) {
			mBase = base;
			mHref = href;
		}

		/**
		 * returns the base of this document
		 * @retrun 	the base of this document
		 */
		public String getBase() {
			return mBase;
		}

		/**
		 * Returns the href of this document
		 * @return the href of this document
		 */
		public String getHref() {
			return mHref;
		}

	}

	/**
	 * adds configuration parameters 
	 * @param name parameter name
	 * @param value parameter value
	 */
	/*	public void addConfigParam(String name, String value) {
			mConfigParams.put(name, value);
		}
	*/
	/**
	 * Returns a configuration key for this configuration container
	 * @return a configuration key for this configuration container
	 */
	public String getConfigKey() {
		return mConfigKey;
	}

	/**
	 * Specifies the key of this configuration
	 * @param key key of this configuration
	 */
	void setConfigKey(String key) {
		mConfigKey = key;
	}

	/**
	 * Sets a container containing configuration data
	 */
	public void setConfigParams(ParamsStrategy.Params params) {
		mParams = params;
	}

	/**
	 * Gets a container containing configuration data
	 */
	public ParamsStrategy.Params getConfigParams() {
		return mParams;
	}

	
	/**
	 * Sets extension keys used when loading this configuration
	 * @param keys a container containing extension keys
	 */
	public void setExtensionKeys(ExtensionProcessor.ExtensionKeys keys) {
		mExtKeys = keys;
	}

	/**
	 * Returns a container containing extension keys
	 * @return a container containing extension keys
	 */
	public ExtensionProcessor.ExtensionKeys getExtensionKeys() {
		return mExtKeys;
	}

	/**
	 * Returns  a string representation of a configuration file 
	 * DO NOT USE THIS METHOD. USE getConfigUsingAliasAsString() INSTEAD.
	 * @param path path of configuration
	 * @return a string representation of a configuration file 
	 */
	public String getConfigAsString(String path) {

		Document doc = getDocument(path);
		if (doc == null)
			return null;

		return XMLUtil.getInstance().toString(doc);
	}

	/**
	 * Returns content of configuration file using the alias-name as defined in xcm-config.xml
	 * @param alias name of configuration file
	 * @return a configuration as input stream
	 */
	public InputStream getConfigUsingAliasAsStream(String alias) {
		String path = ExtendedConfigInitHandler.getFilesConfig().getPath(alias);
		if (path == null)
			return null;
		else
			return getConfigAsStream(path);
	}

	/**
	 * Returns content of configuration file using the alias-name as defined in xcm-config.xml
	 * @param alias name of configuration file
	 * @return a configuration as String
	 */
	public String getConfigUsingAliasAsString(String alias) {
		String path = ExtendedConfigInitHandler.getFilesConfig().getPath(alias);
		if (path == null)
			return null;
		else
			return getConfigAsString(path);
	}

	/**
	 * Returns content of configuration file using the alias-name as defined in xcm-config.xml
	 * @param alias name of configuration file
	 * @return the DOM of the document of <code>null</code>
	 *          if no document in cache
	 */
	public Document getConfigUsingAliasAsDocument(String alias) {
		String path = ExtendedConfigInitHandler.getFilesConfig().getPath(alias);
		if (path == null)
			return null;
		else
			return getDocument(path);
	}

	/**
	 * Returns a configuration as input stream or <code>null</code>
	 * if no configuration for the given href could be found
	 * DO NOT USE THIS METHOD. USE getConfigUsingAliasAsStream() INSTEAD.
	 * @param path path of configuration
	 * @return a configuration as input stream 
	 */
	public InputStream getConfigAsStream(String path) {
/*  
		if (log.isDebugEnabled())
			log.debug(
				"Retrieving configuration file [filename]='" + path + "'");
		String config = getConfigAsString(path);
		if (config == null) {
			if (log.isDebugEnabled()) {
				String msg =
					"Configuration file for [path]='"
						+ path
						+ "' not found in config container";
				log.warn("system.xcm.warn", new Object[] { msg }, null);
			}
			return null;
		}

		StringInputStream sis = new StringInputStream(config);

		return sis;
 */
	Document doc = getDocument(path);
	if (doc == null) {
		if (log.isDebugEnabled()) {
			String msg =
				"Configuration file for [path]='"
					+ path
					+ "' not found in config container";
			log.warn("system.xcm.warn", new Object[] { msg }, null);
		}
		return null;
	}
 
	TransformerFactory transformerFactory = XMLUtil.getTransformerFactory(this);
	ByteArrayInputStream docByteArrayInput = null;
	try {
		Transformer transformer = transformerFactory.newTransformer();	

		ByteArrayOutputStream docByteArrayOutput = new ByteArrayOutputStream();

	  transformer.transform(new DOMSource(doc), new StreamResult(docByteArrayOutput));

	  docByteArrayInput = new ByteArrayInputStream(docByteArrayOutput.toByteArray());

	} catch (TransformerConfigurationException tcex) {
		log.error(LogUtil.APPS_BUSINESS_LOGIC,
			"system.xcm.exception",
			new Object[] { tcex.toString()},
		tcex);
	} catch (TransformerException tex) {
		log.error(LogUtil.APPS_BUSINESS_LOGIC,
			"system.xcm.exception",
			new Object[] { tex.toString()},
		tex);
		
	}
	return docByteArrayInput;
	}

	/**
	 * Adds a String representation of a configuration
	 * @param configName name of configuration
	 * @param 
	 */
	//	public void _addConfig(String href, String body) {
	//		mConfigs.put(href, body);
	//	}

	/**
	 * Adds a DOM representation of a configuration
	 * @param configName name of configuration
	 * @param doc
	 */
	public void addConfig(String href, Document doc) {
		// check scope of document

		
		// add document to local container
		mDOMConfigs.put(href, doc);

		if (mCacheAccess != null) {

			URL urlPassed = null;
			try {
				urlPassed = new URL(href);
			} catch (MalformedURLException murlmex) {
				log.debug(murlmex.getMessage());
				return;
			}

			String pathPassed = urlPassed.getPath();
			for (Iterator iter = mIncludeDocScopes.keySet().iterator();
				iter.hasNext();
				) {
				String truePath = (String) iter.next();
				try {
					URL urlStored = new URL(truePath);
					String pathStored = urlStored.getPath();
					if (pathPassed.equals(pathStored)) {
						String scope = (String) mIncludeDocScopes.get(truePath);
						if (scope != null
							&& scope.equals(XCMConstants.SCOPE_APPLICATIN)) {
							try {
								mCacheAccess.put(href, doc);
								if (log.isDebugEnabled())
									log.debug(
										"Adding [file]='"
											+ href
											+ "' with application scope to cache");
							} catch (Cache.ObjectExistsException ex) {
								log.debug(ex.getMessage());
							} catch (Cache.Exception ex) {
								log.error(LogUtil.APPS_BUSINESS_LOGIC,
									"system.xcm.exception",
									new Object[] { ex.toString()},
									ex);
							}
						}
					}
				} catch (MalformedURLException murlmex) {
					log.error(LogUtil.APPS_BUSINESS_LOGIC,
						"system.xcm.exception",
						new Object[] { murlmex },
						murlmex);
					continue;
				}
			}
		}
	}

	/**
	 * Returns a unmodifiable map of the configurations (path, DOM Documents)
	 * @return a unmodifiable map if the configurations
	 */
	public Map getConfigs() {
		return Collections.unmodifiableMap(mDOMConfigs);
	}

	/**
	 * Adds href of docuemnt which has to be processed
	 * @param base base URI of document
	 * @param href href of document
	 * @param scope scope of document 
	 *         (use com.sap.isa.core.config.init.XCMConstants for specifing scope)
	 */
	public void addIncludeDocHref(String base, String href, String scope, String alias) {

		DocPath docPath = new DocPath(base, href);
		try {
			String absURL = XMLUtil.getInstance().getAbsoluteURL(base, href); 

			if (log.isDebugEnabled())
				log.debug("Adding registered file [href]='" + absURL + "'");	


			mIncludeDocHrefs.put(absURL, docPath);
			mIncludeDocScopes.put(absURL, scope);
			mFileAliases.put(alias, absURL);
		} catch (MalformedURLException murlex) {
			log.error(LogUtil.APPS_BUSINESS_LOGIC,
				"system.xcm.exception",
				new Object[] { murlex.toString()},
				murlex);
		}
	}

	/**
	 * Returns a unmodifiable set of hrefs of document which have to be processed
	 * in order to create this configuration
	 * @return a unmodifiable set of href of document which have to be processed
	 */
	public Map getIncludeDocHrefs() {
		return Collections.unmodifiableMap(mIncludeDocHrefs);
	}

	/**
	 * This Method is called after a document has been processed
	 * (and after processing all strategies) but before it is included
	 * @param href the href of the document
	 * @param doc the document 
	 * @param type the type of event
	 */
	public void documentEvent(String href, Document doc, int type) {
		String scope = null;
		// check scope of document
		mDOMConfigs.put(href, doc);
	}

	/**
	 * returns a DOM for the given url or <code>null</code>
	 * if no DOM is in cache
	 *  DO NOT USE THIS METHOD. USE getConfigUsingAliasAsDocument() INSTEAD.
	 * @param href the href to the document
	 * @return the DOM of the document of <code>null</code>
	 *          if no document in cache
	 */
	public Document getDocument(String href) {

		// check if href has xpointer
		int index = href.indexOf("xpointer");
		String trueHref = href;

		if (index != -1)
			trueHref = href.substring(0, index - 1);

		if (log.isDebugEnabled())
			log.debug(
				"Retrieving document from cache for the given [href]='"
					+ trueHref
					+ "'");

		String normalizedPassedHref = getNormalizedPath(trueHref);

		log.debug(
			"normalized path for the given href. [path]='"
				+ normalizedPassedHref
				+ "'");

		// search for document in global cache
		Object obj = mCacheAccess.get(href);
		if (obj != null) {
			if (log.isDebugEnabled())
				log.debug(
					"Application scope Cache Hit!. Retrieveing [file]='"
						+ href
						+ "'");
			return (Document) obj;
		}

		Object doc = mDOMConfigs.get(normalizedPassedHref);

		if (doc == null) {

			for (Iterator iter = mDOMConfigs.keySet().iterator();
				iter.hasNext();
				) {
				// try access without normalizing path
				doc = mDOMConfigs.get(href);
				if (doc != null)
					break;

				String storedHref = (String) iter.next();
				if (log.isInfoEnabled()) {
					log.debug(
						"Comparing path with stored document [path]='"
							+ storedHref
							+ "'");
				}
				String normalizedStoredHref = getNormalizedPath(storedHref);

				if (log.isDebugEnabled())
					log.debug(
						"Normalized path for stored document [path]='"
							+ normalizedStoredHref
							+ "'");

				// TODO: this code has to be optimized

				// check with normalized paths 
				index = normalizedStoredHref.indexOf(normalizedPassedHref);
				if (index != -1) {
					if (index == 0) {
						doc = (Document) mDOMConfigs.get(storedHref);
						break;
					}
					// check if the passed path is the last part of the href
					String firstPart = normalizedStoredHref.substring(0, index);
					log.debug("index: " + index + "; firstPart: " + firstPart);
					if ((firstPart + normalizedPassedHref)
						.equals(normalizedStoredHref)) {
						doc = mDOMConfigs.get(storedHref);
						break;
					}
				}

				// check passed path with stored paths  
				index = storedHref.indexOf(normalizedPassedHref);
				if (index != -1) {
					if (index == 0) {
						doc = (Document) mDOMConfigs.get(storedHref);
						break;
					}
					// check if the passed path is the last part of the href
					String firstPart = storedHref.substring(0, index);
					log.debug("index: " + index + "; firstPart: " + firstPart);
					if ((firstPart + normalizedPassedHref)
						.equals(storedHref)) {
						doc = mDOMConfigs.get(storedHref);
						break;
					}
				}

			}
		}
		if (log.isDebugEnabled()) {
			if (doc != null) {
				log.debug(
					"Session scope Cache Hit!. Retrieveing [file]='"
						+ href
						+ "'");
			}
		}
		return (Document) doc;
	}

	/**
	 * Sets the trace level 
	 * @param traceLevel either XMLConstants.TRACE_ALL or XMLConstants.TRACE_PATH
	 */
	public static void setTraceLevel(int traceLevel) {
		mTraceLevel = traceLevel;
	}

	public static void dummy(int traceLevel) {
		mTraceLevel = traceLevel;
	}

	/**
	 * initializes cache
	 */
	public void initCache() {

		try {
			mCacheAccess = Cache.getAccess(CACHE_REGION_APPL_SCOPE_CONFIG);

		} catch (Cache.Exception cex) {
			String msg =
				"No cache region '"
					+ CACHE_REGION_APPL_SCOPE_CONFIG
					+ "'configured. Adding region with Cache default settings";
			log.warn("system.xcm.warn", new Object[] { msg }, cex);
			try {
				Cache.addRegion(CACHE_REGION_APPL_SCOPE_CONFIG);
				mCacheAccess = Cache.getAccess(CACHE_REGION_APPL_SCOPE_CONFIG);
			} catch (Cache.Exception cex2) {
				String msg2 =
					"Adding cache region '"
						+ CACHE_REGION_APPL_SCOPE_CONFIG
						+ "' failed. No caching for application scope confguration files!";
				log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.xcm.exception", new Object[] { msg2 }, cex2);

			}

		}
	}

	void setScenarioName(String scenarioName) {
		mScenarioName = scenarioName;
	}

	public String getScenarioName() {
		return mScenarioName;
	}

	private String getNormalizedPath(String path) {
		try {
			URL url = new URL(path);
			String urlPath = url.getPath();
			// no URL path
			return new File(urlPath).getPath();

		} catch (MalformedURLException ex) {
			log.debug(ex.getMessage());
			// no URL path
			return new File(path).getPath();
		}

	}

	/**
	 * This method deletes non-registered files from configiuration container
	 * These are intermediate files included by files registered in XCM
	 */
	public void deleteNonRegisteredFiles() {
		Set delFiles = new HashSet();
		for(Iterator iter = mDOMConfigs.keySet().iterator(); iter.hasNext();) {
			String key = (String)iter.next();
			if (!mIncludeDocHrefs.containsKey(key)) {
				if (log.isDebugEnabled())
					log.debug("remving no registered file from config container [key]='" + key + "'");
				delFiles.add(key);	
			}
		}
		// delete not registered files from container
		for (Iterator iter = delFiles.iterator(); iter.hasNext();) {
			mDOMConfigs.remove(iter.next());
		}
	}

	/**
	 * String representation of container
	 * @return String representation of container
	 */
	/*	public String toString() {
			Document doc = null;
			for (Iterator iter = mDOMConfigs.values().iterator(); iter.hasNext();) {
				doc = (Document)iter.next();	
				break;
			}
			
			return doc.toString();
		}
	*/

}
