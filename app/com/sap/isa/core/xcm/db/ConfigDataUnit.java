/*
 * Created on Aug 24, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.core.xcm.db;

import java.io.Reader;
import java.util.Date;

/**
 * @author I802891
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public interface ConfigDataUnit {
	
	static final int TYPE_CONFIG_DATA = 1;	
	static final int TYPE_SCENARIO_DATA = 2;

	String getUnitName();
	void setUnitName(String name);
	int getUnitType();
	void setUnitType(int type);
	Date getLastModifiedDate();
	void setLastModifiedDate(Date time);
	Reader getData();
	String getDataAsString();
	void setData(Reader reader);
	void setDataAsString(String data);
}
