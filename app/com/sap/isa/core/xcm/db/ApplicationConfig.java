/*
 * Created on Aug 24, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.core.xcm.db;

import java.util.Collection;

/**
 * @author I802891
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public interface ApplicationConfig {
	String getApplicationName();
	void setApplicationName(String name);
	ConfigSet getConfigSetByName(String name);	
	Collection getConfigSets();
	void addConfigSet(ConfigSet dataset);
	void removeConfigSet(ConfigSet dataset);
}
