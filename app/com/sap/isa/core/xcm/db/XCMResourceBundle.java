/*
 * Created on Sep 2, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.core.xcm.db;

import com.sap.localization.ResourceAccessor;

/**
 * @author I802891
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class XCMResourceBundle extends ResourceAccessor {

	private static XCMResourceBundle singleton = new XCMResourceBundle();

	private static final String BUNDLE_NAME = XCMResourceBundle.class.getName();
	
	/**
	 * @param arg0
	 */
	private XCMResourceBundle() {
		super(BUNDLE_NAME);
	}
	
	public static XCMResourceBundle	getInstance() {
		return singleton;
	}
}
