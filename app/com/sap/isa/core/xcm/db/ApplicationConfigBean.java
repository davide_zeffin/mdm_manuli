/*
 * Created on Sep 10, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.core.xcm.db;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

/**
 * @author I802891
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ApplicationConfigBean implements ApplicationConfig {
	
	private String appName;
	private Collection configSets;
	

	/* (non-Javadoc)
	 * @see com.sap.isa.core.xcm.db.ApplicationConfig#getApplicationName()
	 */
	public String getApplicationName() {
		return appName;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.xcm.db.ApplicationConfig#getConfigDataSets()
	 */
	public Collection getConfigSets() {
		return configSets;
	}
	
	public void setConfigDataSets(Collection coll) {
		this.configSets = coll;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.xcm.db.ApplicationConfig#removeConfigDataSet(com.sap.isa.core.xcm.db.ConfigSet)
	 */
	public void removeConfigSet(ConfigSet dataset) {
		if(dataset != null && configSets != null)
			configSets.remove(dataset);
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.xcm.db.ApplicationConfig#addConfigDataSet(com.sap.isa.core.xcm.db.ConfigSet)
	 */
	public void addConfigSet(ConfigSet dataset) {
		if(configSets == null) configSets = new HashSet();
			configSets.add(dataset);
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.xcm.db.ApplicationConfig#getConfigSetByName(java.lang.String)
	 */
	public ConfigSet getConfigSetByName(String name) {
		if(configSets != null) {
			Iterator it = configSets.iterator();
			while(it.hasNext()) {
				ConfigSet set = (ConfigSet)it.next();
				if(set.getName().equals(name)) {
					return set;
				}
			}
		} 
		return null;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.xcm.db.ApplicationConfig#setApplicationName(java.lang.String)
	 */
	public void setApplicationName(String name) {
		this.appName = name;
	}
}
