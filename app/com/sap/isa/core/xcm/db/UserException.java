/*
 * Created on Sep 2, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.core.xcm.db;

import com.sap.exception.BaseRuntimeException;
import com.sap.localization.LocalizableText;
import com.sap.tc.logging.Location;

/**
 * @author I802891
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class UserException extends BaseRuntimeException {
	
	private static final String ERROR_PREFIX = UserException.class.getName() + "_";
	
	
	public static final String DUPLICATE_NAME_ERROR = ERROR_PREFIX + 1;
	public static final String VALIDATION_ERROR = ERROR_PREFIX + 2;	
	public static final String INVALID_ATTR_VALUE = ERROR_PREFIX + 2;	
	
	/**
	 * @param arg0
	 */
	public UserException(Location arg0) {
		super(arg0);
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public UserException(Location arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public UserException(Location arg0, LocalizableText arg1) {
		super(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 */
	public UserException(
		Location arg0,
		LocalizableText arg1,
		Throwable arg2) {
		super(arg0, arg1, arg2);
	}

	/**
	 * @param arg0
	 * @param arg2
	 * @param arg3
	 */
	public UserException(Location arg0, String arg2, Object[] arg3) {
		super(arg0,XCMResourceBundle.getInstance(), arg2, arg3);
	}

	/**
	 * @param arg0
	 * @param arg2
	 * @param arg3
	 * @param arg4
	 */
	public UserException(
		Location arg0,
		String arg2,
		Object[] arg3,
		Throwable arg4) {
		super(arg0, XCMResourceBundle.getInstance(),arg2, arg3, arg4);
	}
}
