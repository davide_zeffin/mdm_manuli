/*
 * Created on Aug 24, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.core.xcm.db.jdbc;

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.BitSet;
import java.util.Date;

import com.sap.isa.core.db.ResourceAccessFailureException;
import com.sap.isa.core.db.jdbc.JdbcPersistenceManager;
import com.sap.isa.core.db.jdbc.JdbcSQLJDAO;
import com.sap.isa.core.db.jdbc.ListResultSetReader;
import com.sap.isa.core.db.jdbc.ResultSetReader;
import com.sap.isa.core.db.jdbc.RowReader;
import com.sap.isa.core.db.jdbc.SQLParameter;
import com.sap.isa.core.db.jdbc.SQLQuery;
import com.sap.isa.core.db.jdbc.SQLQueryProvider;
import com.sap.isa.core.util.OIDGenerator;
import com.sap.isa.core.util.StringBufferReader;
import com.sap.isa.core.xcm.db.ConfigDataUnit;
import com.sap.isa.core.xcm.db.UserException;
import com.sap.tc.logging.Location;

/**
 * @author I802891
 *
 */
public final class ConfigDataUnitDAO extends JdbcSQLJDAO implements ConfigDataUnit, Serializable {
	
	/**
	 * Composite Key
	 * @author I802891
	 */
	public static class Key implements Serializable {
		public String configSetId;
		public String unitName;
		
		public Key() {
			
		}
		public Key(String configSetId, String unitName) {
			this.configSetId = configSetId;
			this.unitName = unitName;
		}
		
		/* (non-Javadoc)
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		public boolean equals(Object obj) {
			if(this == obj) return true;
			if(!(obj instanceof Key)) return false;
			if(configSetId != null && unitName != null) {
				Key other = (Key)obj;
				if(configSetId.equals(other.configSetId) && unitName.equals(other.unitName)) {
					return true;
				}
				else {
					return false;
				}
			}
			return super.equals(obj);
		}

		/* (non-Javadoc)
		 * @see java.lang.Object#hashCode()
		 */
		public int hashCode() {
			if(configSetId != null && unitName != null) {
				return 37*configSetId.hashCode() + unitName.hashCode();
			}		
			return super.hashCode();
		}

		/* (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		public String toString() {
			return configSetId + '#' + unitName;
		}

	}
	
	////////////////////////////////////////////////////
	// DAO Meta Data
	public static final String TABLE_NAME = "CRM_XCM_CONFDATUNT";
	/**
	 * Indexed by Col Position #
	 */
	public static final String[] COL_NAMES = {"CONFIG_SETID","UNIT_NAME","DATA_TYPE","CDATA","MOD_DATE"};
	public static final String[] KEY_COL_NAMES = {COL_NAMES[0],COL_NAMES[1]};
	public static final String[] ATTRIBUTE_NAMES = {"configSetId","unitName","unitType","data","modMillis"};
	//
	//////////////////////////////////////////////////////
	
	private static Location tracer = Location.getLocation(ConfigDataUnitDAO.class.getName());
	
	private static String INSERT_SQL;
	private static String LOAD_SQL;	
	private static String UPDATE_SQL;
	private static String DELETE_SQL;	
	static {
		INSERT_SQL = 
			SQLQueryProvider.getQueryProvider().getInsertSQLQuery(TABLE_NAME,COL_NAMES);
		UPDATE_SQL = 
			SQLQueryProvider.getQueryProvider().getUpdateSQLQuery(TABLE_NAME,new String[]{COL_NAMES[2],COL_NAMES[3],COL_NAMES[4]},KEY_COL_NAMES);		
		DELETE_SQL = 
			SQLQueryProvider.getQueryProvider().getDeleteSQLQuery(TABLE_NAME,KEY_COL_NAMES);
		LOAD_SQL = 
			SQLQueryProvider.getQueryProvider().getSelectSQLQuery(TABLE_NAME,COL_NAMES,KEY_COL_NAMES);			
	}
	
	public ConfigDataUnitDAO() {
		setKey(new Key());
	}
	
	

	// Attributes
	private String configSetId;
	private String unitName;
	private int unitType = TYPE_CONFIG_DATA;
	private long modMillis = System.currentTimeMillis();
	/** this field is persistent transient*/
	private StringBuffer data;
	/** This attribute is persistent transient*/
	private Date modDate;
	
	/**
	 * @return
	 */
	public String getConfigSetId() {
		return configSetId;
	}

	/**
	 * @param string
	 */
	public void setConfigSetId(String string) {
		configSetId = string;
		((Key)super.key).configSetId = configSetId;		
	}
		
	/* (non-Javadoc)
	 * @see com.sap.isa.core.xcm.db.ConfigDataUnit#getData()
	 */
	public Reader getData() {
		if(data != null)
			return new StringBufferReader(data);
		else 
			return null;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.xcm.db.ConfigDataUnit#getDataAsString()
	 */
	public String getDataAsString() {
		if(data != null) return data.toString();
		else return null;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.xcm.db.ConfigDataUnit#getLastModifiedDate()
	 */
	public Date getLastModifiedDate() {
		if(modDate == null) modDate = new Date(modMillis);
		return modDate;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.xcm.db.ConfigDataUnit#getUnitName()
	 */
	public String getUnitName() {
		return unitName;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.xcm.db.ConfigDataUnit#getUnitType()
	 */
	public int getUnitType() {
		return unitType;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.xcm.db.ConfigDataUnit#setData(java.io.Reader)
	 */
	public void setData(Reader reader) {
		super.makeDirty(3,null,null);
		internalSetData(reader);
	}
	
	private void internalSetData(Reader reader) {
		if(reader != null) {
			if(this.data == null) 
				this.data = new StringBuffer();
			else
				this.data.delete(0,data.length());
			// read into StringBuffer, 512 bytes at a time
			char[] buff = new char[256];
			int charsRead = 0;
			try {
				while((charsRead = reader.read(buff)) != -1) {
					this.data.append(buff,0,charsRead);	
				}
			} catch (IOException e) {
				// truncate the read data
				if(this.data != null) this.data.delete(0,data.length() - 1);
				ResourceAccessFailureException ex = 
					new ResourceAccessFailureException(tracer,ResourceAccessFailureException.READ_FAILED,null,e);
				throw ex;
			}
		}
		else {
			data = null;
		}		
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.xcm.db.ConfigDataUnit#setDataAsString(java.lang.String)
	 */
	public void setDataAsString(String strData) {
		super.makeDirty(3,null,null);		
		internalSetDataAsString(strData);
	}
	
	private void internalSetDataAsString(String strData) {
		super.makeDirty(3,null,null);		
		if(strData != null) {
			if(this.data == null) 
				this.data = new StringBuffer();
			else
				this.data.delete(0,data.length());
			this.data.insert(0,strData);
		}			
		else 
			 this.data = null;		
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.xcm.db.ConfigDataUnit#setUnitName(java.lang.String)
	 */
	public void setUnitName(String name) {
		super.makeDirty(1,null,null);		
		this.unitName = name;
		((Key)super.key).unitName = unitName;				
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.xcm.db.ConfigDataUnit#setUnitType(int)
	 */
	public void setUnitType(int type) {		
		if(!(type == TYPE_CONFIG_DATA || type == TYPE_SCENARIO_DATA)) {
			throw new IllegalArgumentException();
		}
		super.makeDirty(2,null,null);		
		this.unitType = type;
	}
	
	public void setLastModifiedDate(Date time) {
		super.makeDirty(4,null,null);		
		if(time != null) {
			modDate = time;
			modMillis = time.getTime();
		}
	}
	
	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.jdbc.JdbcSQLJDAO#getContext()
	 */
	public Object getContext() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.jdbc.JdbcDAO#doDelete(java.sql.Connection)
	 */
	protected void doDelete(Connection conn) throws SQLException {
		PreparedStatement stmt = conn.prepareStatement(DELETE_SQL);
		stmt.setString(1,configSetId);		
		stmt.setString(2,unitName);	
		stmt.execute();
		stmt.close();
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.jdbc.JdbcDAO#doInsert(java.sql.Connection)
	 */
	protected void doInsert(Connection conn) throws SQLException {
		PreparedStatement stmt = conn.prepareStatement(INSERT_SQL);
		stmt.setBytes(1,OIDGenerator.hexStringToBytes(configSetId));		
		stmt.setString(2,unitName);		
		stmt.setShort(3,(short)unitType);
		stmt.setCharacterStream(4,new StringBufferReader(data),data.length());
		stmt.setLong(5,modMillis);
		stmt.execute();
		stmt.close();
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.jdbc.JdbcDAO#doLoad(java.sql.Connection)
	 */
	protected void doLoad(Connection conn) throws SQLException {
		PreparedStatement stmt = conn.prepareStatement(LOAD_SQL);
		stmt.setBytes(1,OIDGenerator.hexStringToBytes(configSetId));		
		stmt.setString(2,unitName);
		ResultSet rs = null;		
		try {	
			stmt.execute();
			rs = stmt.getResultSet();
			
			// Atleast one record
			if(!rs.next()) {
				throw new SQLException("No matching record found");
			}
			new DAOReader().read(rs,this);
			// More than one record
			if(rs.next()) {
				throw new SQLException("More than one matching record found");
			}
		}
		finally {
			stmt.close();
		}
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.jdbc.JdbcDAO#doUpdate(java.util.BitSet, java.sql.Connection)
	 */
	protected void doUpdate(BitSet dirtyIndexes, Connection conn)
		throws SQLException {
			PreparedStatement stmt = conn.prepareStatement(UPDATE_SQL);
			stmt.setShort(1,(short)unitType);		
			stmt.setCharacterStream(2,new StringBufferReader(data),data.length());
			stmt.setLong(3,modMillis);
			// set the OID
			stmt.setBytes(4,OIDGenerator.hexStringToBytes(configSetId));		
			stmt.setString(5,unitName);				
			stmt.execute();
			stmt.close();
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.jdbc.JdbcDAO#getKeyColumns()
	 */
	protected String[] getKeyColumns() {
		return KEY_COL_NAMES;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.jdbc.JdbcDAO#getResultSetReader()
	 */
	public ResultSetReader getResultSetReader() {
		return new ListResultSetReader(new DAOReader());
	}

	protected void validate() {
		// validate Object state
		if(unitName == null) {
			throw new UserException(tracer,UserException.INVALID_ATTR_VALUE,new String[]{"unitName"});
		}
		
		if(configSetId == null) {
			throw new UserException(tracer,UserException.INVALID_ATTR_VALUE,new String[]{"configSetId"});			
		}
		
		if(data == null) {
			throw new UserException(tracer,UserException.INVALID_ATTR_VALUE,new String[]{"data"});			
		}
	}
		
	/////////////////////////////////////////////////////////
	// Callbacks	
	protected void beforeSave() {
		validate();
	}
	
	public static SQLQuery getConfigDataUnitsByConfigSetIdQuery(JdbcPersistenceManager pm) {
		SQLQuery query = (SQLQuery)pm.newSearchQuery(null);
		query.setResultSetReader(new ListResultSetReader(new DAOReader()));
		final String sql =	SQLQueryProvider.getQueryProvider().getSelectSQLQuery(TABLE_NAME,COL_NAMES,new String[]{COL_NAMES[0]});
		query.setSQLComplete(true); 
		query.setSQL(sql);
		SQLParameter param = new SQLParameter();
		param.setName("CONFIG_SETID");
		param.setJdbcType(Types.BINARY);
		query.addParameter(param);		
		
		return query;
	}

	
	/////////////////////////////////////////////////////////
	// Reader Helper classes
	
	public static class DAOReader implements RowReader {
		/* (non-Javadoc)
		 * @see com.sap.isa.core.db.jdbc.RowReader#read(java.sql.ResultSet)
		 */
		public Object read(ResultSet resultset) throws SQLException {
			ConfigDataUnitDAO dao = new ConfigDataUnitDAO();
			read(resultset,dao);
			return dao;
		}
		
		/**
		 * @param rs
		 * @param dao
		 */
		public void read(ResultSet resultset, ConfigDataUnitDAO dao) throws SQLException {
			// KEYID
			dao.setConfigSetId(OIDGenerator.bytesToHexString(resultset.getBytes(COL_NAMES[0])));			
			// UNIT_NAME
			dao.unitName = resultset.getString(COL_NAMES[1]);
			// UNIT_TYPE
			dao.unitType = resultset.getShort(COL_NAMES[2]);
			// CLOB
			dao.internalSetData(resultset.getClob(COL_NAMES[3]).getCharacterStream());
			// Last Modfied Millis
			dao.modMillis = resultset.getLong(COL_NAMES[4]);				
		}
	}
	//
	//////////////////////////////////////////////////////////////
	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.jdbc.JdbcDAO#getFieldCount()
	 */
	protected int getFieldCount() {
		return ATTRIBUTE_NAMES.length + super.getFieldCount();
	}
}
