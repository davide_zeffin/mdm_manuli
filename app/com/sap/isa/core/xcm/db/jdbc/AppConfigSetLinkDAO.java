/*
 * Created on Aug 24, 2004
 *
 */
package com.sap.isa.core.xcm.db.jdbc;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.BitSet;

import com.sap.isa.core.db.jdbc.JdbcPersistenceManager;
import com.sap.isa.core.db.jdbc.JdbcSQLJDAO;
import com.sap.isa.core.db.jdbc.ListResultSetReader;
import com.sap.isa.core.db.jdbc.ResultSetReader;
import com.sap.isa.core.db.jdbc.RowReader;
import com.sap.isa.core.db.jdbc.SQLParameter;
import com.sap.isa.core.db.jdbc.SQLQuery;
import com.sap.isa.core.db.jdbc.SQLQueryProvider;
import com.sap.isa.core.db.jdbc.SQLUpdateQuery;
import com.sap.isa.core.util.OIDGenerator;
import com.sap.isa.core.xcm.db.ConfigSet;
import com.sap.isa.core.xcm.db.UserException;
import com.sap.tc.logging.Location;

/**
 * <p>
 * This object represents the Link Table for M.. N relation
 * between Application and ConfigSets
 * </p>
 * @author I802891
 */
public final class AppConfigSetLinkDAO extends JdbcSQLJDAO implements Serializable {
	////////////////////////////////////////////////////
	// DAO Meta Data
	public static final String TABLE_NAME = "CRM_XCM_APPCONF";
	/**
	 * Indexed by Col Position #
	 */
	public static final String[] COL_NAMES = {"KEYID","APP_NAME","CONFIG_SETID"};
	public static final String[] KEY_COL_NAMES = {COL_NAMES[0]};
	//
	//////////////////////////////////////////////////////
	
	private static String INSERT_SQL;
	private static String LOAD_SQL;	
	private static String UPDATE_SQL;
	private static String DELETE_SQL;	
	static {
		INSERT_SQL = 
			SQLQueryProvider.getQueryProvider().getInsertSQLQuery(TABLE_NAME,COL_NAMES);
		UPDATE_SQL = 
			SQLQueryProvider.getQueryProvider().getUpdateSQLQuery(TABLE_NAME,new String[]{COL_NAMES[1],COL_NAMES[2]},KEY_COL_NAMES);		
		DELETE_SQL = 
			SQLQueryProvider.getQueryProvider().getDeleteSQLQuery(TABLE_NAME,KEY_COL_NAMES);
		LOAD_SQL = 
			SQLQueryProvider.getQueryProvider().getSelectSQLQuery(TABLE_NAME,COL_NAMES,KEY_COL_NAMES);			
	}
	
	private static Location tracer = Location.getLocation(AppConfigSetLinkDAO.class.getName());
	
	/**
	 * DAO Attributes
	 */
	private String applicationName;
	private String configSetId;
	/** Persistent Transient attribute*/
	private ConfigSetDAO configSet;
	

	/* (non-Javadoc)
	 * @see com.sap.isa.core.xcm.db.ApplicationConfig#addConfigDataSet(com.sap.isa.core.xcm.db.ConfigSet)
	 */
	public void setConfigDataSet(ConfigSetDAO dataset) {
		configSet = dataset;
		if(dataset != null) configSetId = dataset.getKey().toString();
		else configSetId = null;
	}
	
	/**
	 * @return
	 */
	public String getConfigSetId() {
		return configSetId;
	}

	/**
	 * @param string
	 */
	public void setConfigSetId(String string) {
		configSetId = string;
	}
	
	/* (non-Javadoc)
	 * @see com.sap.isa.core.xcm.db.ApplicationConfig#getApplicationName()
	 */
	public String getApplicationName() {
		return this.applicationName;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.xcm.db.ApplicationConfig#getConfigDataSets()
	 */
	public ConfigSet getConfigDataSet() {
		if(configSet == null && configSetId != null && this.getPersistenceManager() != null) {
			ConfigSetDAO dao = new ConfigSetDAO();
			dao.setKey(this.getConfigSetId());
			this.getPersistenceManager().load(dao);
			configSet = dao;
		}
		return configSet;
	}
	
	/* 
	 * Check Duplicate Name
	 * @see com.sap.isa.core.xcm.db.ApplicationConfig#setApplicationName()
	 */
	public void setApplicationName(String name) {
		this.applicationName = name;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.jdbc.JdbcSQLJDAO#getContext()
	 */
	public Object getContext() {
		return null;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.jdbc.JdbcDAO#doDelete(java.sql.Connection)
	 */
	protected void doDelete(Connection conn) throws SQLException {
		PreparedStatement stmt = conn.prepareStatement(DELETE_SQL);
		stmt.setBytes(1,OIDGenerator.hexStringToBytes(key.toString()));		
		stmt.execute();
		stmt.close();
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.jdbc.JdbcDAO#doInsert(java.sql.Connection)
	 */
	protected void doInsert(Connection conn) throws SQLException {
		PreparedStatement stmt = conn.prepareStatement(INSERT_SQL);
		stmt.setBytes(1,OIDGenerator.hexStringToBytes(key.toString()));
		stmt.setString(2,applicationName);		
		stmt.setBytes(3,OIDGenerator.hexStringToBytes(configSetId));	
		stmt.execute();
		stmt.close();
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.jdbc.JdbcDAO#doLoad(java.sql.Connection)
	 */
	protected void doLoad(Connection conn) throws SQLException {
		PreparedStatement stmt = conn.prepareStatement(LOAD_SQL);
		stmt.setBytes(1,OIDGenerator.hexStringToBytes(key.toString()));
		try {
			stmt.execute();
			ResultSet rs = stmt.getResultSet();
			if(!rs.next()) {
				throw new SQLException();
			}
			new DAOReader().read(rs,this);
			if(rs.next()) {
				throw new SQLException();
			}
			stmt.close();
		}
		finally {
			stmt.close();
		}		
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.jdbc.JdbcDAO#doUpdate(java.util.BitSet, java.sql.Connection)
	 */
	protected void doUpdate(BitSet dirtyIndexes, Connection conn)
		throws SQLException {
			PreparedStatement stmt = conn.prepareStatement(UPDATE_SQL);
			stmt.setString(1,applicationName);		
			stmt.setBytes(2,OIDGenerator.hexStringToBytes(configSetId));
			// WHERE clause
			stmt.setBytes(3,OIDGenerator.hexStringToBytes(key.toString()));
			stmt.execute();
			stmt.close();
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.jdbc.JdbcDAO#getKeyColumns()
	 */
	protected String[] getKeyColumns() {
		return KEY_COL_NAMES;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.jdbc.JdbcDAO#getResultSetReader()
	 */
	public ResultSetReader getResultSetReader() {
		return new ListResultSetReader(new DAOReader());
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.jdbc.JdbcDAO#getColumnName(int)
	 */
	protected String getColumnName(int fieldCount) {
		if(fieldCount >= COL_NAMES.length) {
			throw new IllegalArgumentException();
		}
		return COL_NAMES[fieldCount];
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.jdbc.JdbcDAO#getFieldCount()
	 */
	protected int getFieldCount() {
		return COL_NAMES.length;
	}
	
	/**
	 * Used to look up links of an application
	 * @param pm
	 * @return
	 */
	public static SQLQuery getLinksByAppNameQuery(JdbcPersistenceManager pm) {
		SQLQuery query = (SQLQuery)pm.newSearchQuery(null);
		final String sql = SQLQueryProvider.getQueryProvider().getSelectSQLQuery(TABLE_NAME,COL_NAMES,new String[]{COL_NAMES[1]});
		query.setSQL(sql);
		SQLParameter param = new SQLParameter();
		param.setName("APP_NAME");
		param.setJdbcType(Types.VARCHAR);
		query.addParameter(param);								
		query.setSQLComplete(true);
		query.setResultSetReader(new ListResultSetReader(new DAOReader()));
		return query;
	}
	/**
	 * Used to Delete links of an application
	*/
	public static SQLUpdateQuery deleteLinksByAppNameQuery(JdbcPersistenceManager pm) {
		SQLUpdateQuery query = pm.newUpdateQuery();
		final String sql = SQLQueryProvider.getQueryProvider().getDeleteSQLQuery(TABLE_NAME,new String[]{COL_NAMES[1]});
		query.setSQL(sql);
		SQLParameter param = new SQLParameter();
		param.setName("APP_NAME");
		param.setJdbcType(Types.VARCHAR);
		query.addParameter(param);								
		query.setSQLComplete(true);
		return query;
	}
	
	public static SQLUpdateQuery deleteLinksByConfigSetIdQuery(JdbcPersistenceManager pm) {
		SQLUpdateQuery query = pm.newUpdateQuery();
		final String sql = SQLQueryProvider.getQueryProvider().getDeleteSQLQuery(TABLE_NAME,new String[]{COL_NAMES[2]});
		query.setSQL(sql);
		SQLParameter param = new SQLParameter();
		param.setName("CONFIG_SETID");
		param.setJdbcType(Types.BINARY);
		query.addParameter(param);
		query.setSQLComplete(true);
		return query;
	}
	
	/////////////////////////////////////////////////////////
	// Callbacks	
	protected void beforeSave() {
		// validate Object state
		if(applicationName == null) throw new UserException(tracer,UserException.INVALID_ATTR_VALUE, new String[]{"applicationName"});
	}

	/////////////////////////////////////////////////////////
	// Reader Helper classes
	
	public static class DAOReader implements RowReader {
			/* (non-Javadoc)
		 * @see com.sap.isa.core.db.jdbc.RowReader#read(java.sql.ResultSet)
		 */
		public void read(ResultSet resultset, AppConfigSetLinkDAO dao) throws SQLException {
			
			// KEYID
			dao.setKey(OIDGenerator.bytesToHexString(resultset.getBytes(COL_NAMES[0])));			
			// APP_NAME
			dao.applicationName = resultset.getString(COL_NAMES[1]);
			// CONFIG_SETID
			dao.configSetId = OIDGenerator.bytesToHexString(resultset.getBytes(COL_NAMES[2]));
		}
		
		public Object read(ResultSet resultset) throws SQLException {
			AppConfigSetLinkDAO dao = new AppConfigSetLinkDAO();
			read(resultset,dao);
			return dao;
		}
		
	}
	//
	//////////////////////////////////////////////////////////////

}
