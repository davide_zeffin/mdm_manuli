/*
 * Created on Aug 24, 2004
 *
 */
package com.sap.isa.core.xcm.db.jdbc;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.BitSet;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

import com.sap.isa.core.db.APIUserException;
import com.sap.isa.core.db.DBQueryResult;
import com.sap.isa.core.db.jdbc.JdbcPersistenceManager;
import com.sap.isa.core.db.jdbc.JdbcSQLJDAO;
import com.sap.isa.core.db.jdbc.ListResultSetReader;
import com.sap.isa.core.db.jdbc.ResultSetReader;
import com.sap.isa.core.db.jdbc.RowReader;
import com.sap.isa.core.db.jdbc.SQLParameter;
import com.sap.isa.core.db.jdbc.SQLQuery;
import com.sap.isa.core.db.jdbc.SQLQueryProvider;
import com.sap.isa.core.locking.LockException;
import com.sap.isa.core.locking.LockManager;
import com.sap.isa.core.locking.enqueue.EnqueueLockInfo;
import com.sap.isa.core.locking.enqueue.EnqueueLockManager;
import com.sap.isa.core.util.OIDGenerator;
import com.sap.isa.core.xcm.db.ConfigDataUnit;
import com.sap.isa.core.xcm.db.ConfigSet;
import com.sap.isa.core.xcm.db.UserException;
import com.sap.tc.logging.Location;

/**
 * @author I802891
 *
 */
public final class ConfigSetDAO extends JdbcSQLJDAO implements ConfigSet, Serializable {
	
	////////////////////////////////////////////////////
	// DAO Meta Data
	public static final String TABLE_NAME = "CRM_XCM_CONFSET";
	/**
	 * Indexed by Col Position #
	 */
	public static final String[] COL_NAMES = {"CONFIG_SETID","CONFIG_NAME","SHARED","DESCR"};
	public static final String[] KEY_COL_NAMES = {COL_NAMES[0]};
	// 
	// Position in the array indicates the Attributes index
	//
	private static final String[] ATTRIBUTE_NAMES = {"key","setName","shared","description"};	
	//
	//////////////////////////////////////////////////////
	
	private static String INSERT_SQL;
	private static String LOAD_SQL;	
	private static String UPDATE_SQL;
	private static String DELETE_SQL;	
	static {
		INSERT_SQL = 
			SQLQueryProvider.getQueryProvider().getInsertSQLQuery(TABLE_NAME,COL_NAMES);
		UPDATE_SQL = 
			SQLQueryProvider.getQueryProvider().getUpdateSQLQuery(TABLE_NAME,new String[]{COL_NAMES[1],COL_NAMES[2],COL_NAMES[3]},KEY_COL_NAMES);		
		DELETE_SQL = 
			SQLQueryProvider.getQueryProvider().getDeleteSQLQuery(TABLE_NAME,KEY_COL_NAMES);
		LOAD_SQL = 
			SQLQueryProvider.getQueryProvider().getSelectSQLQuery(TABLE_NAME,COL_NAMES,KEY_COL_NAMES);			
	}
	
	private static final Location tracer = Location.getLocation(ConfigSetDAO.class.getName());
	
	private static final short TRUE = 1;
	private static final short FALSE = 0;
	// attributes
	private String setName;
	private boolean shared;
	private String description;	
	/** This attribute is persistent transient*/
	private Collection configDataUnits;
	
	/**
	 * @return
	 */
	public String getConfigSetId() {
		return (String)getKey();
	}

				
	/* (non-Javadoc)
	 * @see com.sap.isa.core.xcm.db.ConfigSet#addConfigDataUnit(com.sap.isa.core.xcm.db.ConfigDataUnit)
	 */
	public void addConfigDataUnit(ConfigDataUnit dataUnit) {
		if(configDataUnits == null) {
			// Force Load
			getConfigDataUnits();
		}
		
		if(configDataUnits == null) configDataUnits = new HashSet();
		
		if(!configDataUnits.contains(dataUnit)) {
			((ConfigDataUnitDAO)dataUnit).setConfigSetId((String)key);			
			ConfigDataUnit unit = getConfigDataUnitByName(dataUnit.getUnitName());
			if(unit != null) throw new UserException(tracer,UserException.DUPLICATE_NAME_ERROR,new String[]{dataUnit.getUnitName()});
			configDataUnits.add(dataUnit);
		} 
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.xcm.db.ConfigSet#getConfigDataUnitByName(java.lang.String)
	 */
	public ConfigDataUnit getConfigDataUnitByName(String name) {
		Collection coll = getConfigDataUnits();
		if(coll == null) return null;
		Iterator it = coll.iterator();
		// it is not expected to have more 5-6 Data Units, so iteration is not a big overhead
		while(it.hasNext()) {
			ConfigDataUnitDAO unitDAO = (ConfigDataUnitDAO)it.next();
			if(unitDAO.getUnitName().equalsIgnoreCase(name)) {
				return unitDAO;
			}
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.xcm.db.ConfigSet#getConfigDataUnits()
	 */
	public Collection getConfigDataUnits() {
		// Load on demand
		if(this.getPersistenceManager() != null && configDataUnits == null) {
			if(key != null && !this.isNew()) {
				SQLQuery query = ConfigDataUnitDAO.getConfigDataUnitsByConfigSetIdQuery(this.getPersistenceManager());
				byte[] oid = OIDGenerator.hexStringToBytes((String)key);
				DBQueryResult result = query.execute(new Object[]{oid});
				configDataUnits = new HashSet();
				while(result.hasNext()) {
					configDataUnits.add(result.next());
				}
				result.close();
			}
		}
		return configDataUnits;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.xcm.db.ConfigSet#getDescription()
	 */
	public String getDescription() {
		return description;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.xcm.db.ConfigSet#getName()
	 */
	public String getName() {
		return setName;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.xcm.db.ConfigSet#isShared()
	 */
	public boolean isShared() {
		return shared;
	}


	/**
	 * @param string
	 */
	public void setConfigSetId(String string) {
		super.makeDirty(0,null,null);
		setKey(string);
	}
	
	/* (non-Javadoc)
	 * @see com.sap.isa.core.xcm.db.ConfigSet#setDescription(java.lang.String)
	 */
	public void setDescription(String desc) {
		super.makeDirty(3,null,null);
		this.description = desc;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.xcm.db.ConfigSet#setName(java.lang.String)
	 */
	public void setName(String name) {
		super.makeDirty(1,null,null);
		this.setName = name;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.xcm.db.ConfigSet#setShared(boolean)
	 */
	public void setShared(boolean val) {
		super.makeDirty(2,null,null);
		this.shared = val;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.jdbc.JdbcSQLJDAO#getContext()
	 */
	public Object getContext() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.jdbc.JdbcDAO#doDelete(java.sql.Connection)
	 */
	protected void doDelete(Connection conn) throws SQLException {
		PreparedStatement stmt = conn.prepareStatement(DELETE_SQL);
		stmt.setBytes(1,OIDGenerator.hexStringToBytes(getKey().toString()));		
		stmt.execute();
		stmt.close();
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.jdbc.JdbcDAO#doInsert(java.sql.Connection)
	 */
	protected void doInsert(Connection conn) throws SQLException {
		PreparedStatement stmt = conn.prepareStatement(INSERT_SQL);
				
		stmt.setBytes(1,OIDGenerator.hexStringToBytes(key.toString()));
		stmt.setString(2,setName);
		stmt.setShort(3,shared ? TRUE : FALSE);	
		stmt.setString(4,description);		
		stmt.execute();
		stmt.close();
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.jdbc.JdbcDAO#doLoad(java.sql.Connection)
	 */
	protected void doLoad(Connection conn) throws SQLException {
		PreparedStatement stmt = conn.prepareStatement(LOAD_SQL);
		stmt.setBytes(1,OIDGenerator.hexStringToBytes(key.toString()));
		
		try {
			stmt.execute();
			ResultSet rs = stmt.getResultSet();
			if(!rs.next()) {
				throw new SQLException();
			}
			new DAOReader().read(rs,this);
			if(rs.next()) {
				throw new SQLException();
			}
			stmt.close();
		}
		finally {
			stmt.close();
		}	
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.jdbc.JdbcDAO#doUpdate(java.util.BitSet, java.sql.Connection)
	 */
	protected void doUpdate(BitSet dirtyIndexes, Connection conn)
		throws SQLException {
			PreparedStatement stmt = conn.prepareStatement(UPDATE_SQL);
			stmt.setString(1,setName);	
			stmt.setShort(2,shared ? TRUE : FALSE);
			stmt.setString(3,description);
			stmt.setBytes(4,OIDGenerator.hexStringToBytes(key.toString()));
			stmt.execute();
			stmt.close();
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.jdbc.JdbcDAO#getKeyColumns()
	 */
	protected String[] getKeyColumns() {
		return KEY_COL_NAMES;
	}
	
	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.jdbc.JdbcDAO#getFieldCount()
	 */
	protected int getFieldCount() {
		return super.getFieldCount() + COL_NAMES.length;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.jdbc.JdbcDAO#getColumnName(int)
	 */
	protected String getColumnName(int fieldCount) {
		return COL_NAMES[fieldCount - super.getFieldCount()];
	}
	
	protected String getFieldName(int fieldCount) {
		return ATTRIBUTE_NAMES[fieldCount - super.getFieldCount()];
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.jdbc.JdbcDAO#getResultSetReader()
	 */
	public ResultSetReader getResultSetReader() {
		return new ListResultSetReader(new DAOReader());
	}
	
	public static SQLQuery getConfigSetsByApplicationNameQuery(JdbcPersistenceManager pm) {
		SQLQuery query = (SQLQuery)pm.newSearchQuery(null);
		query.setResultSetReader(new ListResultSetReader(new DAOReader()));
		// final helps in compiler optimization		
		final String sql = SQLQueryProvider.getQueryProvider().getSelectSQLQuery(TABLE_NAME,COL_NAMES,null) +
			" ," +  AppConfigSetLinkDAO.TABLE_NAME + " WHERE " +
			AppConfigSetLinkDAO.TABLE_NAME + ".CONFIG_SETID = " + ConfigSetDAO.TABLE_NAME + ".CONFIG_SETID" + " AND " +
			AppConfigSetLinkDAO.TABLE_NAME + ".APP_NAME = ?";
		query.setSQL(sql);
		query.setSQLComplete(true);
		SQLParameter param = new SQLParameter();
		param.setName("APP_NAME");
		param.setJdbcType(Types.VARCHAR);
		query.addParameter(param);
		return query;
	}
	
	protected void validate() {
		// validate Object state
		if(setName == null) {
			throw new UserException(tracer,UserException.INVALID_ATTR_VALUE,new String[]{"setName"});
		}
	}
	
	/////////////////////////////////////////////////////////
	// callbacks
	protected void beforeSave() {
		validate();
	}
	
	/////////////////////////////////////////////////////////
	// Reader Helper classes
	public static class DAOReader implements RowReader {
		public Object read(ResultSet resultset) throws SQLException {
			ConfigSetDAO dao = new ConfigSetDAO();
			read(resultset,dao);
			return dao;
		}
				
		/* (non-Javadoc)
		 * @see com.sap.isa.core.db.jdbc.RowReader#read(java.sql.ResultSet)
		 */
		public Object read(ResultSet resultset, ConfigSetDAO dao) throws SQLException  {
			// KEYID
			dao.setKey(OIDGenerator.bytesToHexString(resultset.getBytes(COL_NAMES[0])));			
			// CONFIG_NAME
			dao.setName = resultset.getString(COL_NAMES[1]);
			// SHARED
			dao.shared = (resultset.getShort(COL_NAMES[2]) == TRUE) ? true : false;
			// DESC
			dao.description = resultset.getString(COL_NAMES[3]);
			
			return dao;
		}
	}
	//
	//////////////////////////////////////////////////////////////
	
	public void lock(EnqueueLockManager lm) throws LockException {
		EnqueueLockInfo lock = new EnqueueLockInfo();
		lock.setLifetime(EnqueueLockManager.LIFETIME_SESSION);
		lock.setLockType(LockManager.WRITE_LOCK);		
		fillLockInfo(lock);
		lm.lock(lock,0);
	}
	
	public void unlock(EnqueueLockManager lm) throws LockException {
		EnqueueLockInfo lock = new EnqueueLockInfo();
		lock.setLifetime(EnqueueLockManager.LIFETIME_SESSION);
		lock.setLockType(LockManager.WRITE_LOCK);
		fillLockInfo(lock);
		lm.unlock(lock,false);
	}
		
	/* (non-Javadoc)
	 * @see com.sap.isa.core.db.jdbc.JdbcDAO#lock(com.sap.isa.core.locking.enqueue.EnqueueLockManager, com.sap.isa.core.locking.LockInfo)
	 */
	public void fillLockInfo(EnqueueLockInfo info) {
		if(getConfigSetId() == null) {
			APIUserException ex = new APIUserException(tracer,APIUserException.DAO_KEY_IS_NULL,null);
		}
		
		info.setTableName(TABLE_NAME);
		HashMap keys = new HashMap();
		keys.put(ConfigSetDAO.KEY_COL_NAMES[0],getConfigSetId());
		info.setKeys(keys);
	}
}
