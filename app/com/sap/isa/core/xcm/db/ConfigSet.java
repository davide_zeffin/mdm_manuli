/*
 * Created on Aug 24, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.core.xcm.db;

import java.util.Collection;

/**
 * @author I802891
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public interface ConfigSet {
	String getName();
	void setName(String name);
	String getDescription();
	void setDescription(String desc);
	boolean isShared();
	void setShared(boolean val);
	Collection getConfigDataUnits();
	void addConfigDataUnit(ConfigDataUnit dataUnit);
	ConfigDataUnit getConfigDataUnitByName(String name);
}
