package com.sap.isa.core.xcm.admin.ui;

import java.util.Iterator;
import java.util.Set;

import com.sap.isa.core.xcm.admin.Constants;
import com.sap.isa.core.xcm.admin.actionform.CompConfigForm;
import com.sap.isa.core.xcm.admin.controller.ComponentConfigController;
import com.sap.isa.core.xcm.admin.model.table.ComponentDetailTableModel;
import com.sap.isa.core.xcm.init.XCMAdminAllowedValueMetadata;
import com.sap.isa.core.xcm.init.XCMAdminComponentMetadata;
import com.sap.isa.core.xcm.init.XCMAdminConstants;
import com.sap.isa.core.xcm.init.XCMAdminInitHandler;
import com.sapportals.htmlb.Button;
import com.sapportals.htmlb.DefaultListModel;
import com.sapportals.htmlb.DropdownListBox;
import com.sapportals.htmlb.Label;
import com.sapportals.htmlb.InputField;
import com.sapportals.htmlb.enum.DataType;
import com.sapportals.htmlb.rendering.IPageContext;
import com.sapportals.htmlb.table.DefaultCellRenderer;
import com.sapportals.htmlb.table.ICellRenderer;
import com.sapportals.htmlb.table.TableView;
import com.sap.isa.core.init.InitializationHandler;



/**
 *
 */
public class ComponentDetailCellRenderer implements ICellRenderer {

	private ComponentDetailTableModel mCompConfigTableModel;
	private ComponentConfigController mCompController;
	private String mAllwedValueWidth = "20ex";
	private static org.apache.struts.util.MessageResources mMessageResources =
		InitializationHandler.getMessageResources();
	public static final String LABEL_TOOLTIP_DERIVED = mMessageResources.getMessage("xcm.compconf.tab.label.d");
	public static final String LABEL_TOOLTIP = mMessageResources.getMessage("xcm.compconf.tab.label");
	public static final String LABEL_TOOLTIP_DEPRECATED = mMessageResources.getMessage("xcm.compconf.tab.label.dep");

	
	private static String mLogoHref;

	public ComponentDetailCellRenderer(ComponentConfigController compCon, ComponentDetailTableModel model) {
		
		mCompConfigTableModel = model;
		mCompController = compCon;
		String compId = compCon.getCurrentCompId();
		XCMAdminComponentMetadata compMD = XCMAdminInitHandler.getComponentMetadata(compId);
		if (compMD != null ) {
			int width = compMD.getMaxParamAllowedValueLength();
			if (width > 20)
				mAllwedValueWidth = String.valueOf(width + 1) + "ex";
		}
	}

	/**
	 * @see com.sapportals.htmlb.table.ICellRenderer#renderCell(int, int, TableView, IPageContext)
	 */
	public void renderCell(
		int row,
		int column,
		TableView tableView,
		IPageContext rendererContext) {
		InputField paramValue = new InputField(ComponentDetailTableModel.NAME_PARAM_VALUE_INPUT_FIELD);
		String derived = mCompConfigTableModel.getValueAt(row, 6).toString();
		String name = mCompConfigTableModel.getValueAt(row, 1).getValueAsString();
		String status = mCompConfigTableModel.getStatus(row); 
			
		if (column == 1) {
			Label listboxLabel = new Label("listboxLabel" + row);
			listboxLabel.setText(name);
			if (status.equalsIgnoreCase(XCMAdminConstants.OBSOLETE)){
				listboxLabel.setTooltip(LABEL_TOOLTIP + name +	LABEL_TOOLTIP_DEPRECATED);
			}
			else if (derived.equals("false") && mCompController.isAccessible())
				listboxLabel.setTooltip(LABEL_TOOLTIP + name +	LABEL_TOOLTIP_DERIVED);
			else
				listboxLabel.setTooltip(LABEL_TOOLTIP + name);
			listboxLabel.setLabelFor(paramValue);
			listboxLabel.render(rendererContext);
			return;
		}

		if (column == 2) {
			if (mCompConfigTableModel.getParamType(row).equals(XCMAdminAllowedValueMetadata.VALUE_TYPE_TEXT)) {
				// input field
				paramValue.setEnabled(!mCompConfigTableModel.isReadOnly());
				paramValue.setWidth(mAllwedValueWidth);
				
				if (mCompConfigTableModel.isPassword(row)) {
					paramValue.setPassword(mCompConfigTableModel.isPassword(row));
					// instead of displaying the real value disply dummy
					paramValue.setString(Constants.DISPLAY_PASSWORD);
				} else
					paramValue.setString(mCompConfigTableModel.getValueAt(row, column));
				
				paramValue.setType(DataType.STRING);
				// set the right value from the model
				if (status.equalsIgnoreCase(XCMAdminConstants.OBSOLETE)){
					paramValue.setTooltip(LABEL_TOOLTIP + name + LABEL_TOOLTIP_DEPRECATED);
				}
				else if (derived.equals("false") && mCompController.isAccessible())
					paramValue.setTooltip(LABEL_TOOLTIP + name + LABEL_TOOLTIP_DERIVED);
				else
					paramValue.setTooltip(LABEL_TOOLTIP + name);
				paramValue.render(rendererContext);			
				
			} else {
				// dropdown listbox
				DropdownListBox listBox = new DropdownListBox(ComponentDetailTableModel.NAME_PARAM_VALUE_DROPDOWN_LISTBOX);
				listBox.setEnabled(!mCompConfigTableModel.isReadOnly());
				Set allowedValues = mCompConfigTableModel.getParamValues(row);
				DefaultListModel listModel = new DefaultListModel();
				listBox.setWidth(mAllwedValueWidth);

				listModel.setSingleSelection(true);
				for (Iterator iter = allowedValues.iterator(); iter.hasNext();) {
					String value = (String)iter.next();
					listModel.addItem(value, value);
				}
				listBox.setModel(listModel);
				listBox.setSelection(mCompConfigTableModel.getValueAt(row, column).getValueAsString());
				if (derived.equals("false"))
					listBox.setTooltip(LABEL_TOOLTIP + name + LABEL_TOOLTIP_DERIVED);
				else
					listBox.setTooltip(LABEL_TOOLTIP + name);
				listBox.render(rendererContext);
			}
				
			return;
		}

	    if (column == 3) {
			if (mCompConfigTableModel.getParamType(row).equals(XCMAdminAllowedValueMetadata.VALUE_TYPE_SINGLESELECTION)) {	    		
				Button helpButton = new Button(String.valueOf("00" + row));
				helpButton.setOnClick(CompConfigForm.ON_COMP_PARAM_VALUE_DESCR_HELP_EVENT);
				helpButton.setTooltip(Constants.TOOLTIP_SCEN_PARAM_HELP);
				helpButton.setText("?");
				helpButton.render(rendererContext);
			}
			return;
	    }
	    
		if (column == 4) {
//			return;			
			String baseValue = mCompConfigTableModel.getValueAt(row, 4).getValueAsString();
			if (baseValue == null || baseValue.length() == 0)
				return;
			Button baseButton = new Button(String.valueOf("000" + row));
			baseButton.setOnClick(CompConfigForm.ON_COMP_PARAM_RESTORE_VALUE_EVENT);
			baseButton.setTooltip(Constants.TOOLTIP_COMP_RESTORE_BASE_BUTTON + baseValue);
			baseButton.setEnabled(!mCompConfigTableModel.isReadOnly());
			baseButton.setText("R");
			baseButton.render(rendererContext);
			return;

		}

		String compParamName = mCompConfigTableModel.getValueAt(row, 1).getValueAsString();
		String longtext = mCompController.getConfigParamLongtext(compParamName);
		if (column == 6) {
			if (longtext != null && longtext.length() > 0) {	    		
				Button helpButton = new Button(String.valueOf("000" + row));
				helpButton.setOnClick(CompConfigForm.ON_COMP_PARAM_DESCR_HELP_EVENT);
				helpButton.setTooltip(Constants.TOOLTIP_SCEN_PARAM_DESCR_HELP);
				helpButton.setText("?");
				helpButton.render(rendererContext);
			}
			return;


	    }
	    else {
	      DefaultCellRenderer dcr = new DefaultCellRenderer();
	      dcr.renderCell(row, column, tableView, rendererContext);
	    }
	}

	public static void setLogoHref(String logoHref) {
		mLogoHref = logoHref;
	}
	
}
