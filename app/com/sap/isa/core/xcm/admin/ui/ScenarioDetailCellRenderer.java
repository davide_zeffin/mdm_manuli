package com.sap.isa.core.xcm.admin.ui;

import com.sap.isa.core.xcm.admin.Constants;
import com.sap.isa.core.xcm.admin.actionform.ScenarioConfigForm;
import com.sap.isa.core.xcm.admin.controller.ScenarioConfigController;
import com.sap.isa.core.xcm.admin.model.table.ScenarioDetailTableModel;
import com.sap.isa.core.xcm.init.XCMAdminAllowedValueMetadata;
import com.sapportals.htmlb.Button;
import com.sapportals.htmlb.DropdownListBox;
import com.sapportals.htmlb.IListModel;
import com.sapportals.htmlb.TextView;
import com.sapportals.htmlb.Label;
import com.sapportals.htmlb.rendering.IPageContext;
import com.sapportals.htmlb.table.DefaultCellRenderer;
import com.sapportals.htmlb.table.ICellRenderer;
import com.sapportals.htmlb.table.TableView;
import com.sapportals.htmlb.type.AbstractDataType;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.init.InitializationHandler;
			
			 


/**
 *
 */
public class ScenarioDetailCellRenderer implements ICellRenderer {

	private static org.apache.struts.util.MessageResources mMessageResources =
						InitializationHandler.getMessageResources();
	public static final String SCEN_PARAM_HELP_PARAM_NAME = "paramName";
	public static final String SCEN_PARAM_HELP_ROW        = "row";
	public static final String LABEL_LISTBOX_TEXT = mMessageResources.getMessage("xcm.scenDetail.table.label.text");
	public static final String LABEL_TOOLTIP_DERIVED = mMessageResources.getMessage("xcm.scenconf.tab.label.d");
	public static final String LABEL_TOOLTIP = mMessageResources.getMessage("xcm.scenconf.tab.label");


	private ScenarioDetailTableModel mScenConfigTableModel;
	private ScenarioConfigController mScenarioController; 
	protected static IsaLocation log = null;
	
				
	public ScenarioDetailCellRenderer(ScenarioConfigController scenarioController, ScenarioDetailTableModel model) {
		mScenConfigTableModel = model;	
		mScenarioController = scenarioController;
		
		log = IsaLocation.getInstance(this.getClass().getName());	
	}

	/**
	 * @see com.sapportals.htmlb.table.ICellRenderer#renderCell(int, int, TableView, IPageContext)
	 */
	public void renderCell(
		int row,
		int column,
		TableView tableView,
		IPageContext rendererContext) {
	
		
		try {
			DropdownListBox allowedValuesListBox = new DropdownListBox(ScenarioConfigForm.PARAM_VALUE_LIST_BOX_ID);
			
			//commented out, otherwise jumping to component configuration does not work
			//allowedValuesListBox.setEnabled(!mScenConfigTableModel.isReadOnly());
			
			if (column == 1) {
				AbstractDataType paramName = mScenConfigTableModel.getValueAt(row, 1);
				AbstractDataType derived = mScenConfigTableModel.getValueAt(row, 6);
				//TextView text = new TextView(paramName.toString());
				//text.setText(paramName.toString());
				//text.render(rendererContext);
				Label listboxLabel = new Label("listboxLabel" + row);
				listboxLabel.setText(paramName.toString());
				
				if (derived.toString().equals("true") && mScenarioController.isAccessible())
					listboxLabel.setTooltip(LABEL_TOOLTIP + paramName.toString()
						+ LABEL_TOOLTIP_DERIVED);
				else
					listboxLabel.setTooltip(LABEL_TOOLTIP + paramName.toString());
				
				listboxLabel.setLabelFor(allowedValuesListBox);
				listboxLabel.render(rendererContext);
			return;
			}
		
			if (column == 2) {
				AbstractDataType paramName = mScenConfigTableModel.getValueAt(row, 1);
				AbstractDataType derived = mScenConfigTableModel.getValueAt(row, 6);
				IListModel listBoxModel = mScenConfigTableModel.getAllowedValuesListModel(row);
				if (derived.toString().equals("true") && mScenarioController.isAccessible())
					allowedValuesListBox.setTooltip(LABEL_TOOLTIP + paramName.toString() + LABEL_TOOLTIP_DERIVED);
				else
					allowedValuesListBox.setTooltip(LABEL_TOOLTIP + paramName.toString());
				allowedValuesListBox.setModel(listBoxModel);
				int length = XCMAdminAllowedValueMetadata.getMaxValueLength();
				String scenNameLength = "20ex";
				if (length > 20)
					scenNameLength = String.valueOf(length + 3) + "ex";
				allowedValuesListBox.setWidth(scenNameLength);
				
				allowedValuesListBox.render(rendererContext);
	 
				return;
			}
			if (column == 3) {
				AbstractDataType paramName = mScenConfigTableModel.getValueAt(row, 1);
				if (paramName.toString() != null) {
					Button helpButton = new Button(String.valueOf("0" + row));
					helpButton.setOnClick(ScenarioConfigForm.ON_SCENARIO_PARAM_HELP_EVENT);
					helpButton.setTooltip(Constants.TOOLTIP_SCEN_PARAM_HELP);
					helpButton.setText("?");
					helpButton.render(rendererContext);
				}
				return;
			}
			if (column == 4) {
				AbstractDataType compName = mScenConfigTableModel.getValueAt(row, column);
				if (compName == null || compName.toString() == null) {
					TextView tw = new TextView("empty");
					tw.setText("");
					tw.render(rendererContext);
					return;
				}
				Button configureButton = new Button("00" + row);
				configureButton.setWidth("100%");				
				configureButton.setText(compName.toString());
				configureButton.setTooltip(Constants.TOOLTIP_SCEN_PARAM_COMP_BUTTON);
				configureButton.setOnClick(ScenarioConfigForm.ON_SCENARIO_COMP_CONFIG_EVENT);
				configureButton.render(rendererContext);
				return;
			}
			if (column == 6) {
				AbstractDataType paramName = mScenConfigTableModel.getValueAt(row, 1);
				String longText = mScenarioController.getConfigParamLongtext(paramName.getValueAsString());
				if (longText == null || longText.length() == 0)
					return;
				if (paramName.toString() != null) {
					Button helpButton = new Button(String.valueOf("00" + row));
					helpButton.setOnClick(ScenarioConfigForm.ON_SCENARIO_PARAM_DESCR_HELP_EVENT);
					helpButton.setTooltip(Constants.TOOLTIP_SCEN_PARAM_DESCR_HELP);
					helpButton.setText("?");
					helpButton.render(rendererContext);
				}
				return;
			}

		}
			catch (Throwable t) {
				t.printStackTrace();
			}
	
	      DefaultCellRenderer dcr = new DefaultCellRenderer();
	      dcr.renderCell(row, column, tableView, rendererContext);
	      return;
	}
}

