package com.sap.isa.core.xcm.admin.ui;

import com.sap.isa.core.xcm.admin.controller.MonitoringController;
import com.sap.isa.core.xcm.admin.model.table.MonitoringFileTableModel;
import com.sapportals.htmlb.Link;
import com.sapportals.htmlb.rendering.IPageContext;
import com.sapportals.htmlb.table.DefaultCellRenderer;
import com.sapportals.htmlb.table.ICellRenderer;
import com.sapportals.htmlb.table.TableView;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.init.InitializationHandler;
import com.sap.isa.core.xcm.init.ExtendedConfigInitHandler;

/**
 *
 */
public class MonitoringFileTableCellRenderer implements ICellRenderer {

	private static org.apache.struts.util.MessageResources mMessageResources =
						InitializationHandler.getMessageResources();
	
	private MonitoringFileTableModel mMonFileTableModel;
	private MonitoringController mMonController; 
	protected static IsaLocation log = null;
	
				
	public MonitoringFileTableCellRenderer(MonitoringController monController, MonitoringFileTableModel model) {
		mMonFileTableModel = model;	
		mMonController = monController;
		
		log = IsaLocation.getInstance(this.getClass().getName());	
	}

	/**
	 * @see com.sapportals.htmlb.table.ICellRenderer#renderCell(int, int, TableView, IPageContext)
	 */
	public void renderCell(
		int row,
		int column,
		TableView tableView,
		IPageContext rendererContext) {
	
		
		try {
			
			if(column == 1){
				String href = (String) mMonFileTableModel.getValueAt(row, 1).toString();
				String path = (String) mMonFileTableModel.getValueAt(row, 2).toString();
				String file = (String) mMonFileTableModel.getValueAt(row, 3).toString();
				Link link = new Link ("file1"+row);
				link.setTooltip(path);
				link.addText(file);
				
				link.setReference(href);
				link.setTarget("_new");
				link.render(rendererContext);
				
				return;
			}
			
			if (column == 2) {
				String href = (String) mMonFileTableModel.getValueAt(row, 1).toString();
				String path = (String) mMonFileTableModel.getValueAt(row, 2).toString();
				String shortPath[];
				Link link = new Link ("file2"+row);
				link.setTooltip(path);
				
				shortPath = path.split(ExtendedConfigInitHandler.DEFAULT_SAP_PATH);
				
				if (shortPath.length > 1){
					link.addText("/.../WEB-INF/xcm/sap" + shortPath[shortPath.length-1]);
				}
				else{
					shortPath = path.split(ExtendedConfigInitHandler.DEFAULT_CUST_CONFIG_PATH);
					if (shortPath.length > 1){
						link.addText("/.../WEB-INF/xcm/customer/configuration" + shortPath[shortPath.length-1]);
					}
					else{
						shortPath = path.split(ExtendedConfigInitHandler.DEFAULT_CUST_MAD_PATH);
						if (shortPath.length > 1){
							link.addText("/.../WEB-INF/xcm/customer/modification" + shortPath[shortPath.length-1]);
						}
						else{
							link.addText(path);
						}
					}
				}

					
				link.setReference(href);
				link.setTarget("_new");
				link.render(rendererContext);
				
			return;
			}

		}
			catch (Throwable t) {
				t.printStackTrace();
			}
	
	      DefaultCellRenderer dcr = new DefaultCellRenderer();
	      dcr.renderCell(row, column, tableView, rendererContext);
	      return;
	}
}

