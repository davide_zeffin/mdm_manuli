package com.sap.isa.core.xcm.admin.ui;

import com.sap.isa.core.xcm.admin.model.table.ComponentDetailTableModel;
import com.sapportals.htmlb.Image;
import com.sapportals.htmlb.InputField;
import com.sapportals.htmlb.enum.DataType;
import com.sapportals.htmlb.rendering.IPageContext;
import com.sapportals.htmlb.table.DefaultCellRenderer;
import com.sapportals.htmlb.table.ICellRenderer;
import com.sapportals.htmlb.table.TableView;


/**
 *
 */
public class CompTreeNodeRenderer implements ICellRenderer {

	private ComponentDetailTableModel mCompConfigTableModel;

	public CompTreeNodeRenderer(ComponentDetailTableModel model) {
		mCompConfigTableModel = model;		
	}

	/**
	 * @see com.sapportals.htmlb.table.ICellRenderer#renderCell(int, int, TableView, IPageContext)
	 */
	public void renderCell(
		int row,
		int column,
		TableView tableView,
		IPageContext rendererContext) {

		if (column == 2) {
			InputField paramValue = new InputField(ComponentDetailTableModel.NAME_PARAM_VALUE_INPUT_FIELD);
			paramValue.setWidth("100%");
			paramValue.setType(DataType.STRING);
			// set the right value from the model
			paramValue.setString(mCompConfigTableModel.getValueAt(row, column));
			paramValue.render(rendererContext);			
			return;
		}

	    if (column == 4) {
	      Image icon = new Image("C:/java/SAP_J2EEngine6.20_SP4/cluster/server/services/servlet_jsp/work/jspTemp/b2b_dev/root/xcm/mimes/s_b_wiza.gif", "");
	      icon.setTooltip("Restore base value");
	      icon.render(rendererContext);
	    }
	    else {
	      DefaultCellRenderer dcr = new DefaultCellRenderer();
	      dcr.renderCell(row, column, tableView, rendererContext);
	    }
	}
}
