package com.sap.isa.core.xcm.admin.ui;

import com.sap.isa.core.xcm.admin.controller.HelpController;
import com.sap.isa.core.xcm.admin.model.table.HelpItemSummaryTableModel;
import com.sapportals.htmlb.HTMLFragment;
import com.sapportals.htmlb.Link;
import com.sapportals.htmlb.rendering.IPageContext;
import com.sapportals.htmlb.table.DefaultCellRenderer;
import com.sapportals.htmlb.table.ICellRenderer;
import com.sapportals.htmlb.table.TableView;
import com.sapportals.htmlb.type.AbstractDataType;
import com.sapportals.htmlb.TextView;
 
/**
 * TableView for Component Parameter Configuration
 */
public class HelpSummaryCellRenderer implements ICellRenderer {
//comment
	public static final String REQ_PARAM_SCEN_NAME = "scenName";
	public static final String REQ_PARAM_COMP_NAME = "compName";
	public static final String REQ_SCEN_PAGE_NAME = "HelpScenConfig.jsp";
	public static final String REQ_COMP_PAGE_NAME = "HelpCompConfig.jsp";

	private HelpItemSummaryTableModel mHelpItemSummaryTableModel;
	private boolean mGenerateLink;
	private boolean mGenerateLinkDest;
	private boolean mGenerateLinkScenName;
	private boolean mGenerateLinkCompName;
	private boolean mShowLongtext; 
	private boolean mShowShorttext;
	private String mReqParamName;
	private String mReqPageName;
	private String mLinkDestprefix;
	private String mLinkprefix;
	
	public HelpSummaryCellRenderer(HelpItemSummaryTableModel model, int mode, String linkDestPrefix, String linkPrefix) {
		mLinkprefix = linkPrefix;
		mLinkDestprefix = linkDestPrefix;
		
		mHelpItemSummaryTableModel = model;
		mGenerateLink = (mode & HelpController.MODE_SUMMARY_TABLE_NAME_AS_LINK) 
							== HelpController.MODE_SUMMARY_TABLE_NAME_AS_LINK; 

		mGenerateLinkDest = (mode & HelpController.MODE_SUMMARY_TABLE_NAME_AS_LINK_DEST) 
							== HelpController.MODE_SUMMARY_TABLE_NAME_AS_LINK_DEST; 

		mShowShorttext = (mode & HelpController.MODE_SUMMARY_TABLE_SHORTTEXT) 
							== HelpController.MODE_SUMMARY_TABLE_SHORTTEXT; 

		mGenerateLinkScenName = (mode & HelpController.MODE_SUMMARY_TABLE_NAME_AS_SCEN_LINK) 
									== HelpController.MODE_SUMMARY_TABLE_NAME_AS_SCEN_LINK; 
		
		if(mGenerateLinkScenName) {
			mReqParamName = REQ_PARAM_SCEN_NAME;
			mReqPageName = REQ_SCEN_PAGE_NAME;
		}

		mGenerateLinkCompName = (mode & HelpController.MODE_SUMMARY_TABLE_NAME_AS_COMP_LINK) 
									== HelpController.MODE_SUMMARY_TABLE_NAME_AS_COMP_LINK; 

		if(mGenerateLinkCompName) {
			mReqParamName = REQ_PARAM_COMP_NAME;
			mReqPageName = REQ_COMP_PAGE_NAME;
		}
		
		mShowLongtext = (mode & HelpController.MODE_SUMMARY_TABLE_LONGTEXT) 
							== HelpController.MODE_SUMMARY_TABLE_LONGTEXT; 

		
	}
	
	/**
	 * @see com.sapportals.htmlb.table.ICellRenderer#renderCell(int, int, TableView, IPageContext)
	 */
	public void renderCell(
		int row,
		int column,
		TableView tableView,
		IPageContext rendererContext) {

		if (column == 1 && mGenerateLink) {
			AbstractDataType adt = mHelpItemSummaryTableModel.getValueAt(row, 1);
			if (adt.getValueAsString() != null) {
				String name = adt.getValueAsString();
				Link compLink = new Link(name);
				compLink.addText(name);
				compLink.setReference("#" + mLinkprefix + name);
				compLink.render(rendererContext);
			}
			return;
		}

		if (column == 1 && mGenerateLinkDest) {
			AbstractDataType adt = mHelpItemSummaryTableModel.getValueAt(row, 1);
			if (adt.getValueAsString() != null) {
				String name = adt.getValueAsString();
				HTMLFragment html1 = new HTMLFragment("<a name='#" + mLinkDestprefix + name + "'>");
				html1.render(rendererContext);
				TextView text1 = new TextView(name);
				text1.setText(name);
				text1.render(rendererContext);
				HTMLFragment html2 = new HTMLFragment("</a>");
				html2.render(rendererContext);
			}
			return;
		}

		if (column == 1 && (mReqParamName != null)) {
			AbstractDataType adt = mHelpItemSummaryTableModel.getValueAt(row, 1);
			if (adt.getValueAsString() != null) {
				String name = adt.getValueAsString();
				Link link2 = new Link(name);
				link2.addText(name);
				link2.setReference(mReqPageName + "?" + mReqParamName + "=" + name);
				link2.render(rendererContext);
				
				
			}
			return;
		}



		if (column == 2) {
			AbstractDataType shortTextADT = mHelpItemSummaryTableModel.getValueAt(row, 2);
			AbstractDataType longTextADT = mHelpItemSummaryTableModel.getValueAt(row, 3);
			String shortText = "";
			String longText = "";
			if (shortTextADT.getValueAsString() != null)
				shortText = shortTextADT.getValueAsString();

			if (longTextADT.getValueAsString() != null)
				longText = longTextADT.getValueAsString();
			
			TextView description = new TextView(shortText);
			description.setEncode(false);
			description.setWrapping(true);
			//HTMLFragment html = new HTMLFragment(shortText);  
			if (mShowLongtext) {
				description.setText(shortText + "<br/>" + longText);
				//html.setHtml(shortText + "<br/>" + longText);
			
			//html.render(rendererContext);
			description.render(rendererContext);
			return;			
			}
		}


		DefaultCellRenderer dcr = new DefaultCellRenderer();
		dcr.renderCell(row, column, tableView, rendererContext);
	}
}
	
	
	