package com.sap.isa.core.xcm.admin.ui;

import java.util.Collections;
import java.util.Enumeration;

import com.sapportals.htmlb.TreeNode;

public class XCMTreeNode extends TreeNode {

	public XCMTreeNode(String id) {
		super(id);
	}
	
	public Enumeration getChildNodes() {
		if (isOpen())
			return super.getChildNodes();
		else
			return Collections.enumeration(Collections.EMPTY_LIST);
	}
}
