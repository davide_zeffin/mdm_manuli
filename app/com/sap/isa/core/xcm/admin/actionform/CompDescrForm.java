package com.sap.isa.core.xcm.admin.actionform;

// servlet imports
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.xcm.admin.eventhandler.EventHandlerMap;
import com.sap.isa.core.xcm.admin.eventhandler.button.CompConfigCreateEventHandler;

/** 
 *
 */
public class CompDescrForm extends XCMAdminBaseForm {

	public static final String BUTTON_BACK_TEXT = "xcm.compdescript.button.back.text";
	public static final String BUTTON_BACK_ACCESS = "xcm.compdescript.button.back.access.text";
	public static final String BUTTON_BACK_TOOLTIP = "xcm.compdescript.button.back.tooltip";
	public static final String BUTTON_DOCU1_TEXT="xcm.compdescript.button.docu1.text";
	public static final String BUTTON_DOCU1_TOOLTIP = "xcm.compdescript.button.docu1.tooltip";
	public static final String GROUP_DESC_TITLE = "xcm.compdescript.group.desc.title";
	public static final String GROUP_DESC_TOOLTIP1 = "xcm.compdescript.group.desc.tooltip1";
	public static final String GROUP_DESC_TOOLTIP2 = "xcm.compdescript.group.desc.tooltip2";
	public static final String GROUP_CREATE_TITLE = "xcm.compdescript.group.create.title";
	public static final String CONFIGNAME_TEXT = "xcm.compdescript.configname.text";
	public static final String BUTTON_CREATE_TEXT = "xcm.compdescript.button.create.text";
	public static final String BUTTON_CREATE_TOOLTIP = "xcm.compdescript.button.create.tooltip";
	
	public static final String GROUP_TITLE = "xcm.compdescr.group.title";
	public static final String GROUP_TOOLTIP = "xcm.compdescr.group.tooltip";
	public static final String TEXT = "xcm.compdescr.text";
	
	
	public static final String ON_CONFIG_CREATE_EVENT =	"OnConfigCreate.actionform.admin.xcm.core.isa.sap.com";


	public static final String CONFIG_NAME_INPUT_FIELD_ID = "ConfNameInputField";
 
	protected static IsaLocation log = IsaLocation.getInstance(CompDescrForm.class.getName());	
 
	protected void registerEventHandlers() {
		if (log.isDebugEnabled()) {
			log.debug("Registering event handlers");	
		}
		EventHandlerMap evMap = EventHandlerMap.getEventHandlerMap();
		evMap.addEventHandler(this.getClass(), ON_CONFIG_CREATE_EVENT, new CompConfigCreateEventHandler());		
	}
}
