package com.sap.isa.core.xcm.admin.actionform;

// servlet imports


/** 
 *
 */
public class HelpCompConfigForm extends XCMAdminBaseForm {
	
	
	public static final String PAGE_TITLE ="xcm.helpsum.page.title";
	public static final String GROUP_TITLE = "xcm.helpsum.group.title";
	public static final String CONFIG_TEXT = "xcm.helpsum.config.text";
	public static final String INTRO_TEXT = "xcm.helpsum.intro.text";
	public static final String SAP_CONF_OVERV_TEXT = "xcm.helpsum.sapconf.overview.text";
	public static final String COMP_TEXT = "xcm.helpsum.comps.text";
	public static final String LIST_INTRO_TEXT = "xcm.helpsum.listintro.text";
	public static final String LIST_ITEM1_TEXT = "xcm.helpsum.listitem1.text";
	public static final String LIST_ITEM2_TEXT = "xcm.helpsum.listitem2.text";
	public static final String CONF_TEST_TEXT = "xcm.helpsum.conftest.text";
	public static final String COMP_OVERV_TEXT = "xcm.helpsum.comp.overview.text";
	public static final String COMP_OVERV2_TEXT = "xcm.helpsum.comp.overview.mult.text";
	
	public static final String COMP_DETAIL_TEXT = "xcm.helpcompconf.comp.de.te";
	public static final String CONF_DETAIL_TEXT = "xcm.helpcompconf.conf.de.te";
	public static final String PARAM_DETAIL_TEXT = "xcm.helpcompconf.param.de.te";
 	public static final String CONFIG_SUM_TEXT = "xcm.helpcompconf.config.sum.te";
 	public static final String CONF_PARAM_SUM = "xcm.helpcomp.conf.conf.param.sum.te";
}
