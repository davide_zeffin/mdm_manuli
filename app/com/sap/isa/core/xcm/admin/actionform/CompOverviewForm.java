package com.sap.isa.core.xcm.admin.actionform;

// servlet imports
/** 
 *
 */
public class CompOverviewForm extends XCMAdminBaseForm {

	public static final String PAGE_TITLE = "xcm.compoverview.page.title";
	public static final String GROUP_TITLE = "xcm.compoverview.group.title";
	public static final String GROUP_TOOLTIP = "xcm.compoverview.group.tooltip";
	public static final String TEXT_TEXT = "xcm.compoverview.text.text";

	public static final String TRAY_TITLE = "xcm.compoverview.tray.title";
	public static final String TRAY_TOOLTIP = "xcm.compoverview.tray.tooltip";
	public static final String TRAY_TEXT = "xcm.compoverview.tray.text";
	public static final String LIST_ITEM1 = "xcm.list.item1.text";
	public static final String LIST_ITEM2 = "xcm.list.item2.text";
	public static final String LIST_ITEM3 = "xcm.list.item3.text";
	public static final String LIST_ITEM4_ACCESS = "xcm.list.item4.text.access";
	public static final String LIST_ITEM4 = "xcm.list.item4.text";
	public static final String LIST_ITEM5 = "xcm.list.item5.text";
	public static final String LIST_ITEM6 = "xcm.list.item6.text";
	public static final String LIST_ITEM7 = "xcm.list.item7.text";
	public static final String LIST_ITEM8 = "xcm.list.item8.text";
	public static final String LIST_ITEM9 = "xcm.list.item9.text";
}
