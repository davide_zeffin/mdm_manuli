package com.sap.isa.core.xcm.admin.actionform;

/** 
 *
 */
public class HelpTextForm extends XCMAdminBaseForm {
	public static final String COMPONENT_TITLE = "xcm.helptext.comp.title";
	public static final String SCENARIO_TITLE_1 = "xcm.helptext.scen.title1"; 
	public static final String SCENARIO_TITLE_2 = "xcm.helptext.scen.title2";
	public static final String SCENARIO_TOOLTIP = "xcm.helptext.scen.to";
	public static final String ACTIVATE_SCEN_TITLE = "xcm.helptext.act.ti";
	public static final String DEFAULT_SCEN_TITLE = "xcm.helptext.def.ti";
	public static final String BASE_COMP_TITLE="xcm.helptext.base.ti";
	public static final String ALT_TEXT_MISSING="xcm.helptext.missing";
	public static final String ALT_TEXT_MISSING_DESCRIPTION1="xcm.helptext.missing_desc1";
	public static final String ALT_TEXT_MISSING_DESCRIPTION2="xcm.helptext.missing_desc2";
	
	 	
	public static final String ADVANCED_SETTINGS_ATTR = "AdvancedSettings";
	public static final String CONFIG_TYPE_ATTR = "SwitchXCMConfig";

}


