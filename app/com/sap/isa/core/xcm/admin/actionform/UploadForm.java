/*
 * Created on Dec 15, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.core.xcm.admin.actionform;

import org.apache.struts.upload.FormFile;

import com.sap.isa.core.xcm.admin.action.ConfigDataUploadAction;
import com.sap.isa.core.xcm.admin.eventhandler.EventHandlerMap;

/**
 * @author I802891
 */
public class UploadForm extends XCMAdminBaseForm {
	private FormFile configDataFile ;
	private FormFile scenarioConfigFile ;
	 
	public static final String ON_UPLOAD_SUBMIT_EVENT = "button.submit.UploadForm.actionform.admin.xcm.core.isa.sap.com";
	
	public void setConfigDataFile(FormFile file) {
		this.configDataFile = file;
	}
	
	public FormFile getConfigDataFile() {
		return configDataFile;
	}

	public void setScenarioConfigFile(FormFile file) {
		this.scenarioConfigFile = file;
	}
	
	public FormFile getScenarioConfigFile() {
		return scenarioConfigFile;
	}

}
