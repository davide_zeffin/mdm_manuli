package com.sap.isa.core.xcm.admin.actionform;

// struts imports
import org.apache.struts.action.ActionForm;

import com.sap.isa.core.logging.IsaLocation;

/**
 * Base form for all action forms used in the 
 * XCM administration
 */
public class XCMAdminBaseForm extends ActionForm {

	protected static IsaLocation log = null;

	public XCMAdminBaseForm() {
		if (log == null)
			log = IsaLocation.getInstance(this.getClass().getName());
			
		registerEventHandlers();
		
	}

	protected void registerEventHandlers() {
	}

}
