/*
 * Created on Dec 16, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.core.xcm.admin.actionform;

/**
 * @author D041775
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ScenarioOverviewForm extends XCMAdminBaseForm {
	public static final String PAGE_TITLE = "xcm.scenoverview.page.title";
	public static final String GROUPBOX_TITLE = "xcm.scenoverview.groupbox.title";
	public static final String GROUPBOX_TOOLTIP = "xcm.scenoverview.groupbox.tooltip";
	public static final String TEXTFIELD_TEXT = "xcm.scenoverview.textfield.text";

}
