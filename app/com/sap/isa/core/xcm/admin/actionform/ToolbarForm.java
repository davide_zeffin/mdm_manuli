package com.sap.isa.core.xcm.admin.actionform;

// servlet imports
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.xcm.admin.eventhandler.EventHandlerMap;
import com.sap.isa.core.xcm.admin.eventhandler.toolbar.ConfigEventHandler;
import com.sap.isa.core.xcm.admin.eventhandler.toolbar.MonitoringEventHandler;

/**
 *
 */
public class ToolbarForm extends XCMAdminBaseForm {
	
	public static final String TOOLBAR_TITLE = "xcm.toolbar.title";
	public static final String CONFIGURATION_TEXT = "xcm.toolbar.conf.text";
	public static final String CONFIGURATION_TOOLTIP = "xcm.toolbar.conf.tooltip";
	public static final String MONITORING_TEXT = "xcm.toolbar.mon.text";
	public static final String MONITORING_TOOLTIP = "xcm.toolbar.mon.tooltip";
	public static final String DOCUMENTATION_TEXT = "xcm.toolbar.doc.text";
	public static final String DOCUMENTATION_TOOLTIP = "xcm.toolbar.doc.tooltip";

	public static final String CONFIG_ON_CLICK_EVENT =
		"ConfigOnClick.ToolbarForm.toolbar.actionform.admin.xcm.core.isa.sap.com";

	public static final String MONITORING_ON_CLICK_EVENT =
		"MonitoringOnClick.ToolbarForm.toolbar.actionform.admin.xcm.core.isa.sap.com";

	protected static IsaLocation log = IsaLocation.getInstance(ToolbarForm.class.getName());	


	protected void registerEventHandlers() {
		if (log.isDebugEnabled()) {
			log.debug("Registering event handlers");	
		}
		EventHandlerMap evMap = EventHandlerMap.getEventHandlerMap();
		evMap.addEventHandler(this.getClass(),	CONFIG_ON_CLICK_EVENT.intern(), new ConfigEventHandler());
		evMap.addEventHandler(this.getClass(),	MONITORING_ON_CLICK_EVENT.intern(), new MonitoringEventHandler());
	}
}
