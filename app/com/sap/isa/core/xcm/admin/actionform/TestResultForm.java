package com.sap.isa.core.xcm.admin.actionform;




/** 
 *
 */
public class TestResultForm extends XCMAdminBaseForm {
	
	
	public static final String PAGE_TITLE1 = "xcm.testres.page.title1";
	public static final String PAGE_TITLE2 = "xcm.testres.page.title2";
	public static final String SUCCESS = "xcm.testres.success";
	public static final String FAILED = "xcm.testres.page.failed";
	public static final String BUTTON_CLOSE = "xcm.testres.button.close";
	public static final String LIGHT_ALT1 = "xcm.testres.light.alt1"; 
	public static final String LIGHT_ALT2 = "xcm.testres.light.alt2";
	public static final String TRAY1_TEXT = "xcm.testres.tray1.text";
	public static final String TRAY1_TOOL = "xcm.testres.tray1.tool";
	public static final String PARAM_ERROR = "xcm.testres.param.error";
	public static final String HELP_TOOL = "xcm.testres.help.tool";
	public static final String TESTMESSAGE_TEXT = "xcm.testres.testmess.text";
	public static final String TESTMESSAGE_TOOLTIP = "xcm.testres.testmess.tool";
	public static final String PARAMETER = "xcm.testres.parameter";
	public static final String VALUE = "xcm.testres.value";
	public static final String TABLESUM1 = "xcm.testrest.table.sum1";
	public static final String TABLESUM2 = "xcm.testrest.table.sum2";
	}
