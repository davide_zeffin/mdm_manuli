package com.sap.isa.core.xcm.admin.actionform;

// servlet imports
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.xcm.admin.eventhandler.EventHandlerMap;
import com.sap.isa.core.xcm.admin.eventhandler.button.EditEventHandler;
import com.sap.isa.core.xcm.admin.eventhandler.button.RefreshEventHandler;
import com.sap.isa.core.xcm.admin.eventhandler.button.SaveEventHandler;
import com.sap.isa.core.xcm.admin.eventhandler.link.HomeInfoLinkEventHandler;
import com.sap.isa.core.xcm.admin.eventhandler.listbox.GlobalConfigSelectEventHandler;

/** 
 *
 */
public class HomeForm extends XCMAdminBaseForm {
	
	
	public static final String PAGE_TITLE = "xcm.home.page.title";
	public static final String GROUP_TITLE = "xcm.home.group.title";
	public static final String GROUP_TOOLTIP = "xcm.home.group.tooltip";
	public static final String TEXTFIELD_TEXT = "xcm.home.text1";
	public static final String TRAY_TITLE = "xcm.home.tray.title";
	public static final String TRAY_TOOLTIP = "xcm.home.tray.tooltip";
	public static final String TRAY_TEXT1 = "xcm.home.tray.text1";
	public static final String TRAY_LINK1 = "xcm.home.tray.link1";
	public static final String TRAY_TEXT2 = "xcm.home.tray.text2";
	public static final String TRAY_LINK2 = "xcm.home.tray.link2";
	public static final String TRAY_TEXT3 = "xcm.home.tray.text3";
	public static final String TRAY_TEXT4 = "xcm.home.tray.text4";
	public static final String TRAY_TEXT4_ACCESS = "xcm.home.tray.text4.access";
	public static final String TRAY_TEXT5 = "xcm.home.tray.text5";
	
	public static final String SPACE = " ";

	public static final String ON_COMPONENT_HOME_HELP_EVENT =
		"component.homehelp.HomeForm.actionform.admin.xcm.core.isa.sap.com";

	public static final String ON_SCENARIO_HOME_HELP_EVENT =
		"scenario.homehelp.HomeForm.actionform.admin.xcm.core.isa.sap.com";

	public static final String ON_GLOBAL_CONFIGURATION_SELECT =
		"global.config.select.HomeForm.actionform.admin.xcm.core.isa.sap.com";

	public static final String ON_GLOBAL_CONFIGURATION_LIST_BOX_ID =
		"id.listbox.config.select.HomeForm.actionform.admin.xcm.core.isa.sap.com";

	public static final String ON_EDIT_CLICK_EVENT =
		"edit.button.HomeForm.actionform.admin.xcm.core.isa.sap.com";

	public static final String ON_REFRESH_CLICK_EVENT =
		"refresh.button.HomeForm.actionform.admin.xcm.core.isa.sap.com";
		
	public static final String ON_SAVE_CLICK_EVENT =
		"save.button.HomeForm.actionform.admin.xcm.core.isa.sap.com";

	public static final String ON_CANCEL_CLICK_EVENT = "cancel.button.HomeForm.actionform.admin.xcm.core.isa.sap.com";
	
	public static final String ON_DISPLAY_CLICK_EVENT = "display.button.HomeForm.actionform.admin.xcm.core.isa.sap.com";
	
	public static final String ON_DOWNLOAD_COMP_CONFIG_DATA_EVENT = "download.button.compconfig.HomeForm.actionform.admin.xcm.core.isa.sap.com";

	public static final String ON_DOWNLOAD_SCENARIO_CONFIG_DATA_EVENT = "download.button.scenarioconfig.HomeForm.actionform.admin.xcm.core.isa.sap.com";

	protected static IsaLocation log = IsaLocation.getInstance(HomeForm.class.getName());	
 
 	private String eventType;
 	
 	public void setDownloadFormEventType(String val) {
 		this.eventType = val;
 	}
 	
 	public String getDownLoadFormEventType() {
 		return eventType;
 	}
 	
	protected void registerEventHandlers() {
		if (log.isDebugEnabled()) {
			log.debug("Registering event handlers");	
		}
		EventHandlerMap evMap = EventHandlerMap.getEventHandlerMap();
		evMap.addEventHandler(this.getClass(), ON_COMPONENT_HOME_HELP_EVENT, new HomeInfoLinkEventHandler());		
		evMap.addEventHandler(this.getClass(), ON_SCENARIO_HOME_HELP_EVENT, new HomeInfoLinkEventHandler());		
		evMap.addEventHandler(this.getClass(), ON_GLOBAL_CONFIGURATION_SELECT, new GlobalConfigSelectEventHandler());
		EditEventHandler editDisplayEH = new EditEventHandler();
		evMap.addEventHandler(this.getClass(), ON_EDIT_CLICK_EVENT, editDisplayEH );
		evMap.addEventHandler(this.getClass(), ON_DISPLAY_CLICK_EVENT, editDisplayEH);
		evMap.addEventHandler(this.getClass(), ON_REFRESH_CLICK_EVENT, new RefreshEventHandler());
		SaveEventHandler saveCancelEH = new SaveEventHandler();
		evMap.addEventHandler(this.getClass(), ON_SAVE_CLICK_EVENT, saveCancelEH);
		evMap.addEventHandler(this.getClass(), ON_CANCEL_CLICK_EVENT, saveCancelEH);
	}

}
