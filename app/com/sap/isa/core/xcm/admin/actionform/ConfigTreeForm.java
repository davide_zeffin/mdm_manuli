package com.sap.isa.core.xcm.admin.actionform;

// servlet imports
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.xcm.admin.eventhandler.EventHandlerMap;
import com.sap.isa.core.xcm.admin.eventhandler.configTree.CloseEventHandler;
import com.sap.isa.core.xcm.admin.eventhandler.configTree.CompSelectEventHandler;
import com.sap.isa.core.xcm.admin.eventhandler.configTree.ConfigParamSelectEventHandler;
import com.sap.isa.core.xcm.admin.eventhandler.configTree.ExpandEventHandler;
import com.sap.isa.core.xcm.admin.eventhandler.configTree.HomeSelectEventHandler;
import com.sap.isa.core.xcm.admin.eventhandler.configTree.ModeSelectEventHandler;
import com.sap.isa.core.xcm.admin.eventhandler.configTree.ScenarioSelectEventHandler;
import com.sap.isa.core.xcm.admin.model.tree.ConfigTreeModel;

/** 
 *
 */
public class ConfigTreeForm extends XCMAdminBaseForm {


	protected static IsaLocation log = IsaLocation.getInstance(ConfigTreeForm.class.getName());	

	protected void registerEventHandlers() {
		if (log.isDebugEnabled()) {
			log.debug("Registering event handlers");	
		}
		EventHandlerMap evMap = EventHandlerMap.getEventHandlerMap();
		evMap.addEventHandler(this.getClass(), ConfigTreeModel.COMPONENT_ON_CLICK_EVENT, new CompSelectEventHandler());
		evMap.addEventHandler(this.getClass(), ConfigTreeModel.CONFIG_ON_CLICK_EVENT, new ConfigParamSelectEventHandler());
		evMap.addEventHandler(this.getClass(), ConfigTreeModel.SCENARIO_ON_CLICK_EVENT, new ScenarioSelectEventHandler());
		evMap.addEventHandler(this.getClass(), ConfigTreeModel.MODE_ON_CLICK_EVENT, new ModeSelectEventHandler());
		evMap.addEventHandler(this.getClass(), ConfigTreeModel.HOME_ON_CLICK_EVENT, new HomeSelectEventHandler());
		evMap.addEventHandler(this.getClass(), ConfigTreeModel.TREE_NODE_EXPAND_EVENT, new ExpandEventHandler());
		evMap.addEventHandler(this.getClass(), ConfigTreeModel.TREE_NODE_CLOSE_EVENT, new CloseEventHandler());		
	}
}
