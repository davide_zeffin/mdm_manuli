package com.sap.isa.core.xcm.admin.actionform;

// servlet imports
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.xcm.admin.eventhandler.EventHandlerMap;
import com.sap.isa.core.xcm.admin.eventhandler.button.ScenarioCompConfigEventHandler;
import com.sap.isa.core.xcm.admin.eventhandler.button.ScenarioConfigCreateEventHandler;
import com.sap.isa.core.xcm.admin.eventhandler.button.ScenarioConfigDeleteEventHandler;
import com.sap.isa.core.xcm.admin.eventhandler.button.ScenarioConfigSaveEventHandler;
import com.sap.isa.core.xcm.admin.eventhandler.button.ScenarioParamDescrHelpEventHandler;
import com.sap.isa.core.xcm.admin.eventhandler.button.ScenarioParamValueHelpEventHandler;
import com.sap.isa.core.xcm.admin.eventhandler.button.SelectAdvancedScenarioEventHandler;
import com.sap.isa.core.xcm.admin.eventhandler.checkbox.SelectDefaultScenarioEventHandler;
import com.sap.isa.core.xcm.admin.eventhandler.table.ScenarioTableNavEventHandler;
import com.sap.isa.core.xcm.admin.eventhandler.tray.ScenarioConfigTrayEventHandler;
import com.sap.isa.core.xcm.admin.model.table.ScenarioDetailTableModel;

/** 
 *
 */
public class ScenarioConfigForm extends XCMAdminBaseForm {
  
	public static final String SCEN_HELP_TOOLTIP="xcm.scenconfig.comp.help.tooltip";
	public static final String SAVE_BUTTON_TEXT = "xcm.scenconfig.button.save.text";
	public static final String SAVE_BUTTON_TOOLTIP = "xcm.scenconfig.button.save.tooltip";
	public static final String DELETE_BUTTON_TEXT = "xcm.scenconfig.button.delete.text";
	public static final String DELETE_BUTTON_TOOLTIP = "xcm.scenconfig.button.delete.tooltip";
	public static final String CONFIG_BUTTON_TEXT = "xcm.scenconfig.button.config.text";
	public static final String CONFIG_BUTTON_TOOLTIP = "xcm.scenconfig.button.config.tooltip";
	//public static final String CHECKBOX_TEXT = "xcm.scenconfig.checkbox.text";
	//public static final String CHECKBOX_TOOLTIP = "xcm.scenconfig.checkbox.tooltip";
	public static final String CHECKBOX_DEACTIV_TEXT = "xcm.scenconfig.chkbox.deact.te";
	public static final String CHECKBOX_DEACTIV_TOOLTIP = "xcm.scenconfig.chkbox.deact.to";
	public static final String DESCHEADER = "xcm.scenconfig.tray.text";
	public static final String TRAY1_TOOLTIP = "xcm.scenconfig.tray.tooltip";
	public static final String DESCCREATE1 ="xcm.scenconfig.tray2.text1";
	public static final String DESCCREATE2 ="xcm.scenconfig.tray2.text2";
	public static final String LABEL_SCEN_NAME_TEXT = "xcm.scenconfig.label.scen_name_input";	
	public static final String BUTTON_CREATE_TEXT ="xcm.scenconfig.button.create.text";
	public static final String BUTTON_CREATE_TOOLTIP ="xcm.scenconfig.button.create.tooltip";
	public static final String TRAY3_TEXT = "xcm.scenconfig.tray3.text";
	public static final String TRAY3_TOOLTIP= "xcm.scenconfig.tray3.tooltip";
	public static final String LABEL_SCEN_ID_TEXT = "xcm.scenconfig.label.scen_id_input";
	public static final String LABEL_BASE_SCEN_TEXT = "xcm.scenconfig.label.base_scen_input";
	public static final String CHECKBOX_ADVANCED_OFF_TOOLTIP = "xcm.scenconfig.checkbox.advanced.off.tooltip";
	public static final String CHECKBOX_ADVANCED_ON_TOOLTIP = "xcm.scenconfig.checkbox.advanced.on.tooltip";
	public static final String CHECKBOX_ADVANCED_ON_TEXT = "xcm.scenconfig.checkbox.advanced.on.text";
	public static final String CHECKBOX_ADVANCED_OFF_TEXT = "xcm.scenconfig.checkbox.advanced.off.text";
	public static final String CHECKBOX_DEF_CONF_TOOLTIP = "xcm.scenconfig.checkbox.def_conf.tooltip";
	public static final String CHECKBOX_DEF_CONF_TEXT = "xcm.scenconfig.checkbox.def_conf.text";
	public static final String BUTTON_DEF_SCEN_HELP_TOOLTIP="xcm.scenconfig.button.default_scen_help.tooltip";
	public static final String BUTTON_ACT_SCEN_HELP_TOOLTIP="xcm.scenconfig.bton.activate.hlp.to";
	public static final String LEGEND_TEXT = "xcm.scenconfig.legend.text";
	public static final String LEGEND_TEXT2 = "xcm.scenconfig.legend.text2";
	public static final String LEGEND_TEXT2_ACCESS = "xcm.scenconfig.legend.text2.access";
	public static final String HOW_TO_EXPAND_TEXT = "xcm.scenconfig.how_to_expand";
	public static final String DEFAULT_SCENARIO_HELP1 = "xcm.scenconfig.default_scen_help1";
	public static final String DEFAULT_SCENARIO_HELP2 = "xcm.scenconfig.default_scen_help2";
	public static final String ACTIVE_SCENARIO_HELP="xcm.scenconfig.active_scen_help";
	public static final String SCENSHORT_LABEL_TEXT = "xcm.scenconfig.scenshort.label.te";
	public static final String SCENSHORT_LABEL_TOOLTIP = "xcm.scenconfig.scenshort.label.to";

	public static final String ADVANCED_SETTINGS_TITLE = "xcm.helptext.advanced.settings.ti";
	public static final String ADVANCED_SETTINGS_DESCR = "xcm.helptext.advanced.settings.desc";
	public static final String ADVANCED_SETTINGS_TOOLTIP = "xcm.helptext.advanced.settings.tooltip";
		
	
	public static final String SCEN_SHORT_TEXT_INPUT_FIELD_ID = "ScenarioShortTextInputField";

	public static final String SCEN_NAME_INPUT_FIELD_ID = "ScenarioNameInputField";	

	public static final String ON_SCENARIO_CREATE_EVENT =
		"OnScenarioCreate.actionform.admin.xcm.core.isa.sap.com";

	public static final String ON_SCENARIO_CONFIG_TRAY_OPEN_EVENT =
		"OnScenarioConfigTrayOpen.actionform.admin.xcm.core.isa.sap.com";

	public static final String ON_SCENARIO_CONFIG_TRAY_CLOSE_EVENT =
		"OnScenarioConfigTrayClose.actionform.admin.xcm.core.isa.sap.com";

	public static final String ON_SCENARIO_DESCR_TRAY_OPEN_EVENT =
		"OnScenarioDescrTrayOpen.actionform.admin.xcm.core.isa.sap.com";

	public static final String ON_SCENARIO_DESCR_TRAY_CLOSE_EVENT =
		"OnScenarioDescrTrayClose.actionform.admin.xcm.core.isa.sap.com";


	public static final String ON_SCENARIO_SAVE_EVENT =
		"OnScenarioSave.actionform.admin.xcm.core.isa.sap.com";
	
	public static final String ON_SCENARIO_DELETE_EVENT =
		"OnScenarioDelete.actionform.admin.xcm.core.isa.sap.com";

	public static final String ON_SCENARIO_PARAM_HELP_EVENT =
		"OnScenarioParamHelp.actionform.admin.xcm.core.isa.sap.com";

	public static final String ON_SCENARIO_PARAM_DESCR_HELP_EVENT =
		"OnScenarioParamDescrHelp.actionform.admin.xcm.core.isa.sap.com";


	public static final String ON_SCENARIO_COMP_CONFIG_EVENT =
		"OnScenarioCompConfig.actionform.admin.xcm.core.isa.sap.com";

	public static final String ON_DEFAULT_SCENARIO_EVENT =
		"OnDefaultScenario.actionform.admin.xcm.core.isa.sap.com";

	public static final String ON_ADVANCED_SCENARIO_OPTIONS_EVENT =
		"OnAdvancedOptionsScenario.actionform.admin.xcm.core.isa.sap.com";


	public static final String SCENARIO_TABLE_VIEW_ID = "scenarioConfigDataTableView";

	public static final String PARAM_VALUE_LIST_BOX_ID = "scenarioParamValueDropdownListBox";
	
	protected static IsaLocation log = IsaLocation.getInstance(ScenarioConfigForm.class.getName());	
 
	protected void registerEventHandlers() {
		if (log.isDebugEnabled()) {
			log.debug("Registering event handlers");	
		}
		EventHandlerMap evMap = EventHandlerMap.getEventHandlerMap();
		evMap.addEventHandler(this.getClass(), ScenarioDetailTableModel.ON_NAVIGATE_EVENT, new ScenarioTableNavEventHandler());
		evMap.addEventHandler(this.getClass(), ON_SCENARIO_CREATE_EVENT, new ScenarioConfigCreateEventHandler());		
		evMap.addEventHandler(this.getClass(), ON_SCENARIO_SAVE_EVENT, new ScenarioConfigSaveEventHandler());
		evMap.addEventHandler(this.getClass(), ON_SCENARIO_DELETE_EVENT, new ScenarioConfigDeleteEventHandler());
		evMap.addEventHandler(this.getClass(), ON_SCENARIO_CONFIG_TRAY_CLOSE_EVENT, new ScenarioConfigTrayEventHandler());
		evMap.addEventHandler(this.getClass(), ON_SCENARIO_CONFIG_TRAY_OPEN_EVENT, new ScenarioConfigTrayEventHandler());
		evMap.addEventHandler(this.getClass(), ON_SCENARIO_DESCR_TRAY_CLOSE_EVENT, new ScenarioConfigTrayEventHandler());
		evMap.addEventHandler(this.getClass(), ON_SCENARIO_DESCR_TRAY_OPEN_EVENT, new ScenarioConfigTrayEventHandler());
		evMap.addEventHandler(this.getClass(), ON_SCENARIO_PARAM_HELP_EVENT, new ScenarioParamValueHelpEventHandler());
		evMap.addEventHandler(this.getClass(), ON_SCENARIO_COMP_CONFIG_EVENT, new ScenarioCompConfigEventHandler());
		evMap.addEventHandler(this.getClass(), ON_DEFAULT_SCENARIO_EVENT, new SelectDefaultScenarioEventHandler());
		evMap.addEventHandler(this.getClass(), ON_ADVANCED_SCENARIO_OPTIONS_EVENT, new SelectAdvancedScenarioEventHandler());		
		evMap.addEventHandler(this.getClass(), ON_SCENARIO_PARAM_DESCR_HELP_EVENT, new ScenarioParamDescrHelpEventHandler());		
	}
}
