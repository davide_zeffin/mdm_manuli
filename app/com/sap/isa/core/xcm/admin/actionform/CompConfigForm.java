package com.sap.isa.core.xcm.admin.actionform;

// servlet imports
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.xcm.admin.eventhandler.EventHandlerMap;
import com.sap.isa.core.xcm.admin.eventhandler.button.CompConfigDeleteEventHandler;
import com.sap.isa.core.xcm.admin.eventhandler.button.CompConfigResotoreValueEventHandler;
import com.sap.isa.core.xcm.admin.eventhandler.button.CompConfigSaveEventHandler;
import com.sap.isa.core.xcm.admin.eventhandler.button.CompParamHelpEventHandler;
import com.sap.isa.core.xcm.admin.eventhandler.button.CompParamValueHelpEventHandler;
import com.sap.isa.core.xcm.admin.eventhandler.button.ScenarioBackCompConfigEventHandler;
import com.sap.isa.core.xcm.admin.eventhandler.listbox.BaseConfigSelectEventHandler;
import com.sap.isa.core.xcm.admin.eventhandler.listbox.TestSelectEventHandler;
import com.sap.isa.core.xcm.admin.eventhandler.table.CompConfigTableNavEventHandler;
import com.sap.isa.core.xcm.admin.model.table.ComponentDetailTableModel;
import com.sap.isa.core.init.InitializationHandler;

/** 
 *
 */
public class CompConfigForm extends XCMAdminBaseForm {

	
	/**
	 *
	 */
	private static org.apache.struts.util.MessageResources mMessageResources =
			InitializationHandler.getMessageResources();
			
	public static final String COMP_DESCR="xcm.compconfig.comp_descr"; 
	public static final String BACK_BUTTON_TEXT = "xcm.compconfig.back.button.text";
	public static final String BACK_BUTTON_TEXT_ACCESS = "xcm.compconfig.back.button.text.access";
	public static final String BACK_BUTTON_TOOLTIP="xcm.compconfig.back.button.tooltip";
	public static final String SAVE_BUTTON_TEXT ="xcm.compconfig.save.button.text";
	public static final String SAVE_BUTTON_TOOLTIP = "xcm.compconfig.save.button.tooltip";
	public static final String DELETE_BUTTON_TEXT = "xcm.compconfig.delete.button.text";
	public static final String DELETE_BUTTON_TOOLTIP = "xcm.compconfig.delete.button.tooltip";		
	public static final String DOCU1_BUTTON_TEXT = "xcm.compconfig.docu1.button.text";
	public static final String DOCU1_BUTTON_TOOLTIP = "xcm.compconfig.docu1.button.tooltip";
	public static final String WARNING1_TEXT1="xcm.compconfig.warning1.text1";
	public static final String WARNING1_TEXT2="xcm.compconfig.warning1.text2";
	public static final String WARNING2_TEXT="xcm.compconfig.warning2.text";
	public static final String GROUP_TEXT="xcm.compconfig.group.title";
	public static final String GROUP_TOOLTIP="xcm.compconfig.group.tooltip";
	public static final String LABEL_COMPID_TEXT="xcm.compconfig.label.compid.text";
	public static final String LABEL_COMPID_TOOLTIP="xcm.compconfig.label.compid.tooltip";
	public static final String COMP_HELP_TOOLTIP="xcm.compconfig.comp.help.tooltip";
	public static final String LABEL_CONFIGID_TEXT = "xcm.compconfig.label.configid.text";
	public static final String LABEL_CONFIGID_TOOLTIP = "xcm.compconfig.label.configid.tooltip";
	public static final String LABEL_BASECONF_TEXT = "xcm.compconfig.label.baseconf.text";
	public static final String LABEL_BASECONF_TOOLTIP = "xcm.compconfig.label.baseconf.tooltip";
	public static final String BASECOMP_HELP_TOOLTIP = "xcm.compconfig.basecomp.help.tooltip";
	public static final String GROUP2_TEXT = "xcm.compconfig.group2.title";
	public static final String GROUP2_TOOLTIP = "xcm.compconfig.group2.tooltip";
	public static final String LABEL_CONFIGTEST_TEXT = "xcm.compconfig.label.configtest.text";
	public static final String LABEL_CONFIGTEST_TOOLTIP = "xcm.compconfig.label.configtest.tooltip";
	public static final String DROPDOWN_CONFIGTEST_TOOLTIP = "xcm.compconfig.dropdown.configtest.tooltip";
	public static final String HELP_CONFIGTEST_TOOLTIP = "xcm.compconfig.help.configtest.tooltip";
	public static final String BUTTON_RUNTEST_TEXT = "xcm.compconfig.button.runtest.text";
	public static final String BUTTON_RUNTEST_TOOLTIP = "xcm.compconfig.button.runtest.tooltip";
	public static final String LABEL_SAVECONF_TEXT = "xcm.compconfig.label.saveconf.text";
	public static final String LEGEND_TEXT = "xcm.compconfig.legend.text";
	public static final String LEGEND_TEXT_ACCESS = "xcm.compconfig.legend.text.access";
	public static final String LONGTEXT_DEPRECATED = mMessageResources.getMessage("xcm.compconfig.longtext.deprecated");
	
	public static final String COMP_SHORT_TEXT_INPUT_FIELD_ID = "xcm.compconfig.scenshort.label.te";
	public static final String COMPSHORT_LABEL_TOOLTIP = "xcm.compconfig.scenshort.label.to";
	public static final String COMPSHORT_LABEL_TEXT = "xcm.compconfig.scenshort.label.te";
	
	
	public static final String TRAY2_TEXT = "xcm.compconfig.tray2.text";
	public static final String TRAY2_TOOLTIP = "xcm.compconfig.tray2.tooltip";
	
	
	public static final String ON_CONFIG_SAVE_EVENT =
		"OnConfigSave.actionform.admin.xcm.core.isa.sap.com";

	public static final String ON_CONFIG_DELETE_EVENT =
		"OnConfigDelete.actionform.admin.xcm.core.isa.sap.com";

	public static final String ON_TEST_SELECT_EVENT =
		"OnTestSelect.actionform.admin.xcm.core.isa.sap.com";

	public static final String ON_BASE_SELECT_EVENT =
		"OnBaseSelect.actionform.admin.xcm.core.isa.sap.com";

	public static final String ON_SCENARIO_BACK_COMP_CONFIG_EVENT =
		"OnScenarioBackCompConfig.actionform.admin.xcm.core.isa.sap.com";

	public static final String ON_COMP_PARAM_VALUE_DESCR_HELP_EVENT =
		"OnCompParamValueDescrHelp.actionform.admin.xcm.core.isa.sap.com";

	public static final String ON_COMP_PARAM_DESCR_HELP_EVENT =
		"OnCompParamDescrHelp.actionform.admin.xcm.core.isa.sap.com";

	public static final String ON_COMP_PARAM_RESTORE_VALUE_EVENT =
		"OnCompValueRestoreDescrHelp.actionform.admin.xcm.core.isa.sap.com";


	public static final String COMP_NAME_INPUT_FIELD_ID = "ComponentNameInputField";

	public static final String PARAM_TABLE_VIEW_ID = "paramConfigDataTableView";	
	
	public static final String SCENARIO_TABLE_VIEW_ID = "scenarioConfigDataTableView";	

	public static final String TEST_LIST_BOX_ID = "testCasesDropdownListBox";	
	
	public static final String BASE_LIST_BOX_ID = "baseCompDropdownListBox";


	protected static IsaLocation log = IsaLocation.getInstance(CompConfigForm.class.getName());	
 
	protected void registerEventHandlers() {
		if (log.isDebugEnabled()) {
			log.debug("Registering event handlers");	
		}
		EventHandlerMap evMap = EventHandlerMap.getEventHandlerMap();
		evMap.addEventHandler(this.getClass(), ComponentDetailTableModel.ON_NAVIGATE_EVENT, new CompConfigTableNavEventHandler());		
		evMap.addEventHandler(this.getClass(), ON_CONFIG_SAVE_EVENT, new CompConfigSaveEventHandler());		
		evMap.addEventHandler(this.getClass(), ON_CONFIG_DELETE_EVENT, new CompConfigDeleteEventHandler());
		evMap.addEventHandler(this.getClass(), ON_TEST_SELECT_EVENT, new TestSelectEventHandler());		
		evMap.addEventHandler(this.getClass(), ON_BASE_SELECT_EVENT, new BaseConfigSelectEventHandler());
		evMap.addEventHandler(this.getClass(), ON_SCENARIO_BACK_COMP_CONFIG_EVENT, new ScenarioBackCompConfigEventHandler());
		evMap.addEventHandler(this.getClass(), ON_COMP_PARAM_VALUE_DESCR_HELP_EVENT, new CompParamValueHelpEventHandler());
		evMap.addEventHandler(this.getClass(), ON_COMP_PARAM_DESCR_HELP_EVENT, new CompParamHelpEventHandler());		
		evMap.addEventHandler(this.getClass(), ON_COMP_PARAM_RESTORE_VALUE_EVENT, new CompConfigResotoreValueEventHandler());
	}
}
