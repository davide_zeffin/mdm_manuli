/*
 * Created on Dec 16, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.core.xcm.admin.actionform;

/**
 * @author D041775
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */  
public class OptionsConfigForm extends XCMAdminBaseForm { 
	public static final String PAGE_TITLE= "xcm.optionsconf.page.title";
	public static final String GROUP_TITLE = "xcm.optionsconf.group.title";
	public static final String GROUP_TOOLTIP = "xcm.optionsconf.group.tooltip";
	public static final String STORETYPE_LABEL_TEXT = "xcm.optionsconf.storetype.text";
	public static final String DROPLIST_LABEL_TEXT = "xcm.optionsconf.droplist.label.text";
	public static final String DROPLIST_LABEL_TOOLTIP = "xcm.optionsconf.droplist.label.tooltip";
	public static final String DROP_LIST_TOOLTIP = "xcm.optionsconf.droplist.tooltip";
	public static final String COMP_CONF_DATA_LABEL_TEXT = "xcm.optionsconf.compconf.data.label.text";
	public static final String COMP_CONF_DATA_LABEL_TOOLTIP = "xcm.optionsconf.compconf.data.label.tooltip";
	public static final String CONF_DATA_LABEL_TEXT = "xcm.optionsconf.conf.data.label.text";
	public static final String CONF_DATA_LABEL_TOOLTIP="xcm.optionsconf.conf.data.label.tooltip";
	public static final String BUTTON_TOOLTIP = "xcm.optionsconf.button.tooltip";
	public static final String HELP_CURRENT_CONFIG="xcm.help.current.config";
	public static final String BUTTON_UPLOAD_TEXT = "xcm.optionsconf.button.upload.text";
	public static final String NOT_IN_EDIT_MODE_TEXT = "xcm.optionsconf.notInEdit.text";
	public static final String TRAY_DOWN_TO_FILE_TEXT ="xcm.optionsconf.tray.downtofile.text";
	public static final String BUTTON_DOWNL_TEXT = "xcm.optionsconf.button.download.text";
	public static final String BUTTON_DOWNL_TOOLTIP = "xcm.optionsconf.button.download.tooltip";
	public static final String COMP_CONF_DATA_TEXT = "xcm.optionsconf.comp.conf.data.text";
	public static final String APPL_CONF_DATA_TEXT = "xcm.optionsconf.appl.conf.data.text";
}