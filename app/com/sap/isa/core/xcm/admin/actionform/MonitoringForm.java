package com.sap.isa.core.xcm.admin.actionform;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.xcm.admin.eventhandler.EventHandlerMap;
import com.sap.isa.core.xcm.admin.eventhandler.listbox.UsedScenSelectEventHandler;
import com.sap.isa.core.xcm.admin.eventhandler.button.RefreshMonitoringEventHandler;

/** 
 *
 */
public class MonitoringForm extends XCMAdminBaseForm {
	
 
	public static final String GROUP_TITLE = "xcm.monitoring.group.te";
	public static final String GROUP_TOOLTIP = "xcm.monitoring.group.to";
	public static final String PAGE_TITLE="xcm.stats.page.title";
	public static final String BUTTON_TEXT="xcm.stats.button.te";
	public static final String LABEL_TEXT = "xcm.stats.label.te";
	public static final String LABEL_TOOLTIP = "xcm.stats.label.to";
	public static final String ERROR_TEXT = "xcm.stats.error.te";
	public static final String CACHE_ERROR_TEXT = "xcm.stats.cacheerror.te";

	public static final String ON_USEDSCEN_SELECT = "usedscen.listbox.MonitoringForm.actionform.admin.xcm.core.isa.sap.com";
	public static final String ON_REFRESH_CLICK = "refreshMonitoring.button.MonitoringForm.actionform.admin.xcm.core.isa.sap.com";
	
	
	protected static IsaLocation log =
			IsaLocation.getInstance(MonitoringForm.class.getName());
			
	protected void registerEventHandlers() {
			EventHandlerMap evMap = EventHandlerMap.getEventHandlerMap();
			evMap.addEventHandler(this.getClass(), ON_USEDSCEN_SELECT, new UsedScenSelectEventHandler());
			evMap.addEventHandler(this.getClass(), ON_REFRESH_CLICK, new RefreshMonitoringEventHandler());		
	}
		
	 	
}


