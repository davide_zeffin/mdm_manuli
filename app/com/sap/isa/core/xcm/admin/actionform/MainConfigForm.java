package com.sap.isa.core.xcm.admin.actionform;

/** 
 *
 */
public class MainConfigForm extends XCMAdminBaseForm {
	public static final String PAGE_TITLE = "xcm.mainconfig.page.title";
	public static final String GROUPBOX_TITLE = "xcm.mainconfig.groupbox.title";
	public static final String GROUPBOX_TOOLTIP = "xcm.mainconfig.groupbox.tooltip";
	public static final String MESSAGES_TEXT = "xcm.mainconfig.messages.text";
	public static final String EDIT_BUTTON_TEXT = "xcm.mainconfig.button.edit.text";
	public static final String EDIT_BUTTON_TOOLTIP = "xcm.mainconfig.button.edit.tooltip";
	public static final String SAVE_BUTTON_TEXT = "xcm.mainconfig.button.save.text";
	public static final String SAVE_BUTTON_TOOLTIP = "xcm.mainconfig.button.save.tooltip";
	public static final String CANCEL_BUTTON_TEXT = "xcm.mainconfig.button.cancel.text";
	public static final String CANCEL_BUTTON_TOOLTIP = "xcm.mainconfig.button.cancel.tooltip";
	public static final String REFRESH_BUTTON_TEXT = "xcm.mainconfig.button.refresh.text";
	public static final String REFRESH_BUTTON_TOOLTIP = "xcm.mainconfig.button.refresh.tooltip";
	public static final String DISPLAY_BUTTON_TEXT = "xcm.mainconfig.button.display.text";
	public static final String DISPLAY_BUTTON_TOOLTIP = "xcm.mainconfig.button.display.tooltip";

	public static final String XCM_CONFIG_TITLE = "xcm.helptext.configtype.ti";
	public static final String XCM_CONFIG_TOOLTIP = "xcm.helptext.configtype.tooltip";
	public static final String XCM_CONFIG_TEXT = "xcm.helptext.configtype.text";
 	
}


