package com.sap.isa.core.xcm.admin.actionform;

// servlet imports


/** 
 *
 */
public class HelpScenConfigForm extends XCMAdminBaseForm {
	
	
	public static final String CONFIG_DETAIL_TEXT ="xcm.helpscenconf.conf.det.te";
	public static final String MAND_CONFIG_PARAMS = "xcm.helpscenconf.mand.conf.te";
	public static final String OPT_CONFIG_PARAMS = "xcm.helpscenconf.opt.conf.te";
	public static final String MAND_PARAM_DETAIL = "xcm.helpscenconf.man.detail.te";
	public static final String OPT_PARAM_DETAIL = "xcm.helpscenconf.opt.detail.te";

 
}
