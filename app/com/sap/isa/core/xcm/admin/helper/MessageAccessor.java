/*
 * Created on Dec 17, 2004
 *
 */
package com.sap.isa.core.xcm.admin.helper;

import java.util.Locale;
import com.sap.localization.*;


/**
 * @author I802891
 *
 */
public class MessageAccessor {
	
	private static final MessageAccessor singleton = new MessageAccessor();
	
	private ResourceAccessor msgProvider;
	protected MessageAccessor() {
	}
	
	public static MessageAccessor getMessageAccessor() {
		return singleton;
	}

	public void setResourceAccessor(ResourceAccessor resourceProvider) {
		this.msgProvider = resourceProvider;
	}
	
	public Message getMessage(String msgId) {
		return getMessage(msgId,(Object[])null);
	}

	public Message getMessage(String msgId, Object[] args) {
		return getMessage(msgId,args,null);
	}
	
	public Message getMessage(String msgId, Locale locale) {
		return getMessage(msgId,null,locale);
	}
	
	public Message getMessage(String msgId, Object[] args, Locale locale) {
		String messageText = msgId;
		if(msgProvider != null) {
			String val = msgProvider.getMessageText(locale,msgId);
			if(val != null)
				messageText = val;
		}
		Message msg = new Message(messageText,args);
		return msg;		
	}
}
