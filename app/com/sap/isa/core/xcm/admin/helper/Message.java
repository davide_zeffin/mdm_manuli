/*
 * Created on Dec 17, 2004
 */
package com.sap.isa.core.xcm.admin.helper;

import java.text.MessageFormat;

/**
 * @author I802891
 */
public class Message {
	public static final short ERROR = 1;
	public static final short WARNING = 2;
	public static final short INFO = 3;
	
	private short severity = INFO;
	private String messageText;
	private Object[] args;
	
	public Message(String msg, Object[] args) {
		String formattedMsg = msg;
		if(msg != null && args != null) {
			MessageFormat formatter = new MessageFormat(msg);
			formattedMsg = formatter.format(args);
		}
		messageText = formattedMsg;
	}
	
	public String getMessageText() {
		return messageText;
	}
	
	public short getSeverity() {
		return severity;
	}
	
	public void setSeverity(short severity) {
		if(severity >=1 && severity <=3)
			this.severity = severity;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return getMessageText();
	}

}
