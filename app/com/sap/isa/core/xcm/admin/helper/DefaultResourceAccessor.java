/*
 * Created on Dec 17, 2004
 */
package com.sap.isa.core.xcm.admin.helper;


import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

import com.sap.localization.ResourceAccessor;

/**
 * @author I802891
 */
public class DefaultResourceAccessor extends ResourceAccessor {
	
	private static final DefaultResourceAccessor singleton = new DefaultResourceAccessor();
	
	private Map msgMap = new LinkedHashMap();
	
	private DefaultResourceAccessor() {
		super("DefaultResourceAccessor");
	}
	
	public final static DefaultResourceAccessor getInstance() {
		return singleton; 
	}
	
	/* (non-Javadoc)
	 * @see com.sap.localization.ResourceAccessor#getMessageText(java.util.Locale, java.lang.String)
	 */
	public String getMessageText(Locale arg0, String msgId) {
		return (String)msgMap.get(msgId); 
	}
	
	public void setMessageText(String msgId, String msgText) {
		if(msgId != null && msgId.length() > 0 && msgText != null)
			msgMap.put(msgId,msgText);
	}
}
