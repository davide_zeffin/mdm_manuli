package com.sap.isa.core.xcm.admin;

/**
 * Constants used in actions when forwarding the request flow
 */
public interface ActionForwardConstants {

	/**
	 * SUCCESS
	 */
	public static final String SUCCESS         = "success";
	public static final String FAILURE         = "runtimeexception";	
	public static final String MESSAGE         = "message";
	public static final String MAIN_CONFIG     = "config";	
	public static final String MAIN_MONITORING = "monitoring";	
	public static final String COMP_DESCR      = "compdescr";
	public static final String EMPTY      = "empty";
}
