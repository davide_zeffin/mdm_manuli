/*
 * Created on Dec 16, 2004
 */
package com.sap.isa.core.xcm.admin.action;

import java.io.File;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.xcm.admin.SessionConstants;
import com.sap.isa.core.xcm.admin.actionform.HomeForm;
import com.sap.isa.core.xcm.admin.controller.AppConfigController;
import com.sap.isa.core.xcm.admin.controller.ComponentConfigController;

/**
 * @author I802891
 *
 */
public class ConfigDataDownloadAction extends XCMBaseAction {
	public ConfigDataDownloadAction() {
		log = IsaLocation.getInstance(ConfigDataDownloadAction.class.getName());
	}
	
	/**
	 * Has to be implemented by the extending class
	 */
	public ActionForward perform(ActionMapping mapping,
									ActionForm form,		
									HttpServletRequest request,
									HttpServletResponse response) {

		HomeForm homeForm = (HomeForm)form;
		String eventId = homeForm.getDownLoadFormEventType();
		
		HttpSession session = request.getSession();
		
		UserSessionData userData = UserSessionData.getUserSessionData(session);
		
		AppConfigController appConfig = (AppConfigController)userData.getAttribute(SessionConstants.APP_CONFIG_CONTROLLER);
		ComponentConfigController compConfig = appConfig.getCompConfigController();
		
		response.setContentType("application/Octet-stream");
		/*
		 *These header settings are required for IE file download bug 
		 */	
		response.setHeader("Cache-Control", "");
		response.setHeader("Pragma", "");				
	
		if (eventId.equals(HomeForm.ON_DOWNLOAD_COMP_CONFIG_DATA_EVENT)) {
			try {
				String fileName = "config-data.xml";
				String docHref = appConfig.getCompConfigController().getDataManipulator().getDocHREF();
				if(docHref != null)
					fileName = docHref.substring(docHref.lastIndexOf(File.separator) + 1);
				response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
				String docAsString = appConfig.getCompConfigController().getDataManipulator().getExtendingDocAsString();
				response.getOutputStream().write(docAsString.getBytes("UTF-8"));
				response.getOutputStream().flush();
				response.getOutputStream().close();
			}
			catch(Exception e) {
				log.warn(LogUtil.APPS_USER_INTERFACE,"XCM Admin: component configuration data download failed",e);
			}
		}
					
		if (eventId.equals(HomeForm.ON_DOWNLOAD_SCENARIO_CONFIG_DATA_EVENT)) {
			try {
				String fileName = "scenario-data.xml";
				String docHref = appConfig.getScenarioConfigController().getDataManipulator().getDocHREF();
				if(docHref != null)
					fileName = docHref.substring(docHref.lastIndexOf(File.separator) + 1);
				response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
				String docAsString = appConfig.getScenarioConfigController().getDataManipulator().getExtendingDocAsString();
				response.getOutputStream().write(docAsString.getBytes("UTF-8"));
				response.getOutputStream().flush();
				response.getOutputStream().close();				
			}
			catch(Exception e) {
				log.warn(LogUtil.APPS_USER_INTERFACE,"XCM Admin: component application configuration data download failed",e);
			}			
		}
	
		return null;
	}
}
