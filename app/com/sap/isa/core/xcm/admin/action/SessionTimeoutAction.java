package com.sap.isa.core.xcm.admin.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.MessageListDisplayer;
import com.sap.isa.core.xcm.admin.ActionForwardConstants;


/**
 * Base Action is called after a session timeout happend in the XCM admin
 */
public class SessionTimeoutAction extends Action {


  protected IsaLocation log = null;

	public SessionTimeoutAction() {
		log = IsaLocation.getInstance(this.getClass().getName());	
	}

  public ActionForward perform( ActionMapping mapping,
								ActionForm form,
								HttpServletRequest request,
								HttpServletResponse response)
  throws IOException, ServletException {
  	
	MessageListDisplayer messageDisplayer = new MessageListDisplayer();
	messageDisplayer.addToRequest(request);
	messageDisplayer.setLoginAvailable(false); 
	messageDisplayer.setAction("admin/xcm/init.do");
	messageDisplayer.addMessage(new Message(Message.ERROR,"system.xcm.session.timeout",null,null));
	return mapping.findForward(ActionForwardConstants.SUCCESS);	
  }
	
	
}
