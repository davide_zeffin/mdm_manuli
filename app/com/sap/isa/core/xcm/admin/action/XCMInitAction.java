package com.sap.isa.core.xcm.admin.action;

// struts imports
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.ActionServlet;
import com.sap.isa.core.ContextConst;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.action.SwitchAccessibilityAction;
import com.sap.isa.core.config.AdminConfig;
import com.sap.isa.core.db.DBHelper;
import com.sap.isa.core.db.jdbc.JdbcPersistenceManagerFactory;
import com.sap.isa.core.locking.LockException;
import com.sap.isa.core.locking.LockManager;
import com.sap.isa.core.locking.enqueue.EnqueueLockInfo;
import com.sap.isa.core.locking.enqueue.EnqueueLockManager;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.MessageListDisplayer;
import com.sap.isa.core.util.MiscUtil;
import com.sap.isa.core.util.StartupParameter;
import com.sap.isa.core.xcm.XCMConstants;
import com.sap.isa.core.xcm.admin.ActionForwardConstants;
import com.sap.isa.core.xcm.admin.Constants;
import com.sap.isa.core.xcm.admin.SessionConstants;
import com.sap.isa.core.xcm.admin.controller.AppConfigController;
import com.sap.isa.core.xcm.admin.controller.LockingController;
import com.sap.isa.core.xcm.admin.controller.MonitoringController;
import com.sap.isa.core.xcm.admin.helper.DefaultResourceAccessor;
import com.sap.isa.core.xcm.admin.helper.MessageAccessor;
import com.sap.isa.core.xcm.admin.model.CompDataManipulator;
import com.sap.isa.core.xcm.admin.model.ScenarioDataManipulator;
import com.sap.isa.core.xcm.admin.model.XMLAdminUtil;
import com.sap.isa.core.xcm.admin.model.XMLAdminUtilDB;
import com.sap.isa.core.xcm.admin.model.XMLAdminUtilFS;
import com.sap.isa.core.xcm.db.ConfigDataUnit;
import com.sap.isa.core.xcm.db.jdbc.AppConfigSetLinkDAO;
import com.sap.isa.core.xcm.init.ExtendedConfigInitHandler;
import com.sap.isa.core.xcm.init.XCMAdminInitHandler;


/**
 *
 */
public class XCMInitAction extends Action {

		 
	 
    protected static IsaLocation log =
                           IsaLocation.getInstance(XCMInitAction.class.getName());

	public ActionForward perform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws IOException, ServletException {

		// Set XCM user authorization
		AdminConfig.setUserAuthorization(request);
		// check the user rights...
		if (!AdminConfig.isXCMAdmin_RO(request))
		  throw new ServletException("This Operation is Forbidden");
 
		// As of now use the Default Accessor which provide msgs in US_EN
		MessageAccessor msgAccessor = MessageAccessor.getMessageAccessor();
		msgAccessor.setResourceAccessor(DefaultResourceAccessor.getInstance());
		
		if (!XCMAdminInitHandler.isActive()) {
			String msg = "Could not initialize XCM Admin: XCMAdmin init handler not registered in bootstrap-config.xml";
			
			log.error(LogUtil.APPS_USER_INTERFACE,"system.xcm.admin.ex", new Object[] { msg }, null);
			// hard coded action forward
			return mapping.findForward(ActionForwardConstants.FAILURE);
		}

		// check content of customer.config.path.xcm.config.isa.sap.com context parameter
		// this is required if the target datastore is FS
		String customerPath = getServlet().getServletContext().getInitParameter(ContextConst.XCM_PATH_CUST_CONFIG);
		if (!ExtendedConfigInitHandler.isTargetDatastoreDB() && 
			(customerPath == null || customerPath.length() == 0 
				|| customerPath.equalsIgnoreCase(ContextConst.XCM_PATH_CUST_CONFIG_VALUE)
				|| customerPath.equalsIgnoreCase(ContextConst.XCM_PATH_CUST_CONFIG_VALUE_630))) {
			MessageListDisplayer messageDisplayer = new MessageListDisplayer();
			messageDisplayer.addToRequest(request);
			messageDisplayer.setLoginAvailable(false); 
			messageDisplayer.setAction("admin/xcm/init.do");
			messageDisplayer.addMessage(new Message(Message.ERROR,"system.xcm.init.error.cust.path1",null,null));
			messageDisplayer.addMessage(new Message(Message.ERROR,"system.xcm.init.error.cust.path2",null,null));
			messageDisplayer.addMessage(new Message(Message.ERROR,"system.xcm.init.error.cust.path3",null,null));
			return mapping.findForward(ActionForwardConstants.MESSAGE);
		}


	
        // create an indicator object in the session scope
        HttpSession session = request.getSession();
        
		UserSessionData userData = null;
		 
		
		if (request.getParameter(Constants.KEEP_SESSION_REQUEST_PARAM) != null) {
			userData = UserSessionData.getUserSessionData(session);
		}
        
        if (userData == null)
        	userData = UserSessionData.createUserSessionData(session, false);

		// Store data in StartupParameter
		StartupParameter startupParameter = new StartupParameter();
		userData.setAttribute(SessionConst.STARTUP_PARAMETER, startupParameter);


		String custURL = ExtendedConfigInitHandler.getCustRealConfigURL();

		// get default configuration files
		String globalConfigIndex = (String)request.getParameter(Constants.SELECT_GLOBAL_CONFIG_REQUEST_PARAM);

		String configDataPath = XCMAdminInitHandler.getConfigData(1);
		String scenarioConfig = XCMAdminInitHandler.getScenarioConfig(1);
		String configDescription = XCMAdminInitHandler.getConfigName(1);

		if (globalConfigIndex != null) {
			int index = Integer.parseInt(globalConfigIndex);
			
			configDataPath = XCMAdminInitHandler.getConfigData(index);
			scenarioConfig = XCMAdminInitHandler.getScenarioConfig(index);
			configDescription = XCMAdminInitHandler.getConfigName(index);
		}
		 
		if (log.isDebugEnabled()) {
			log.debug("Default [config-data file name]='" + configDataPath + "'");
			log.debug("Default [scenario-config file name]='" + scenarioConfig + "'");
		}
		
		// disable isa framework encoder
		session.setAttribute(ActionServlet.ENCODING_SKIP, "true");
		
		
		// nps: XCM DB enhancements
		CompDataManipulator confDataCustomer = new CompDataManipulator();
		//confDataCustomer.setBaseHREF("file:///" + baseDir);
		confDataCustomer.setBaseHREF(null);
		confDataCustomer.setDocHREF(custURL + File.separator +  configDataPath);
		confDataCustomer.setTargetDataStoreDB(ExtendedConfigInitHandler.isTargetDatastoreDB());
		
		ScenarioDataManipulator scenDataCustomer = new ScenarioDataManipulator();
		//scenDataCustomer.setBaseHREF("file:///" + baseDir);
		scenDataCustomer.setBaseHREF(null);
		scenDataCustomer.setDocHREF(custURL + File.separator + scenarioConfig);
		scenDataCustomer.setTargetDataStoreDB(ExtendedConfigInitHandler.isTargetDatastoreDB());
				
		try {
			XMLAdminUtil adminUtil1 = null;
			XMLAdminUtil adminUtil2 = null;
			if(ExtendedConfigInitHandler.isTargetDatastoreDB()) {
				
				LockingController lockController = new LockingController();
				
				// Check to create Empty Config Set first in database
				checkAndCreateConfigSet(lockController,ExtendedConfigInitHandler.getApplicationName(),configDescription);
				
				Properties props = new Properties();
				String dsName = MiscUtil.replaceSystemProperty(XCMConstants.DATASOURCE_LOOKUP_NAME);
				props.setProperty(DBHelper.DB_DATASOURCE,dsName);
				props.setProperty(DBHelper.DB_PMF,JdbcPersistenceManagerFactory.class.getName());
				JdbcPersistenceManagerFactory pmf = 
					(JdbcPersistenceManagerFactory)DBHelper.getPersistenceManagerFactory(props);
					
				XMLAdminUtilDB adminUtilDB = new XMLAdminUtilDB(pmf);
				adminUtilDB.setAppName(ExtendedConfigInitHandler.getApplicationName());
				adminUtilDB.setConfigSetName(configDescription);
				adminUtilDB.setConfigUnitName(configDataPath);
				adminUtilDB.setConfigUnitType(ConfigDataUnit.TYPE_CONFIG_DATA);
				adminUtil1 = adminUtilDB;
				
				adminUtilDB = new XMLAdminUtilDB(pmf);
				adminUtilDB.setAppName(ExtendedConfigInitHandler.getApplicationName());
				adminUtilDB.setConfigSetName(configDescription);
				adminUtilDB.setConfigUnitName(scenarioConfig);
				adminUtilDB.setConfigUnitType(ConfigDataUnit.TYPE_SCENARIO_DATA);
				adminUtil2 = adminUtilDB;
				
				// Do lazy initialization as it holds a database connection
				userData.setAttribute(SessionConstants.LOCKING_CONTROLLER, lockController);
			}
			else {
				XMLAdminUtilFS adminUtilFS = new XMLAdminUtilFS();
				adminUtilFS.setBaseHREF(null);
				adminUtilFS.setDocHREF(custURL + File.separator + configDataPath);
				adminUtil1= adminUtilFS;
								
				adminUtilFS = new XMLAdminUtilFS();
				adminUtilFS.setBaseHREF(null);
				adminUtilFS.setDocHREF(custURL + File.separator + scenarioConfig);
				adminUtil2 = adminUtilFS;
			}
			
			confDataCustomer.init(adminUtil1);
			scenDataCustomer.init(adminUtil2);
			
		} catch (Exception ex) { 
			String msg = ex.getMessage(); 
			if(ex instanceof LockException) {
				msg = "Failed to obtain internal lock. Please refresh.";
			}
			
			log.error(LogUtil.APPS_USER_INTERFACE,"system.xcm.admin.ex", new Object[] { msg }, ex);
			request.setAttribute(ContextConst.EXCEPTION, ex);
			return mapping.findForward(ActionForwardConstants.FAILURE);
		}
		

//		set accessibility switch	
		SwitchAccessibilityAction.setAccessibilitySwitch(request, startupParameter);
			 
		// initialize the XCM admin UI controller
		AppConfigController appConfigController = new AppConfigController(configDescription, confDataCustomer, scenDataCustomer, SwitchAccessibilityAction.getAccessibilitySwitch(userData));
		userData.setAttribute(SessionConstants.APP_CONFIG_CONTROLLER, appConfigController);
		MonitoringController monController = new MonitoringController();
		userData.setAttribute(SessionConstants.MONITORING_CONTROLLER, monController);
		

		// initialize configuration tree
		appConfigController.initConfigTreeModel();

		return mapping.findForward(ActionForwardConstants.SUCCESS);
	}
		
//	private void readXCMAdminConfig(String configFilePath) {
//			
//		
//	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////
	//

	private static final String SELECT_APPCONFIG_SQL = "SELECT CRM_XCM_APPCONF.KEYID FROM CRM_XCM_APPCONF, CRM_XCM_CONFSET " +
		" WHERE CRM_XCM_APPCONF.CONFIG_SETID = CRM_XCM_CONFSET.CONFIG_SETID AND CRM_XCM_APPCONF.APP_NAME = ? AND CRM_XCM_CONFSET.CONFIG_NAME = ?";
	private static final String INSERT_APPCONFIG_SQL = "INSERT INTO CRM_XCM_APPCONF (KEYID,APP_NAME,CONFIG_SETID) VALUES(?,?,?)";	
	private static final String INSERT_CONFIGSET_SQL = "INSERT INTO CRM_XCM_CONFSET (CONFIG_SETID,CONFIG_NAME,SHARED,DESCR) VALUES(?,?,?,?)";

	
	private void checkAndCreateConfigSet(LockingController lc, String appName, String configSetName) throws Exception {
		Connection conn = null;
		boolean active = false;
		
		try {
			DataSource ds = DBHelper.getApplicationSpecificDataSource(appName);
			conn = ds.getConnection();

			if(checkAppConfig(conn,appName, configSetName)) return;
			
			lc.initLockManager();
			EnqueueLockManager lm = (EnqueueLockManager)lc.getLockManager();
			EnqueueLockInfo lock = new EnqueueLockInfo();
			
			// Logical Lock for creating the empty config set
			// Lock set to prevent race across cluster
			Map keys = new HashMap();
			// Logical key id
			keys.put(AppConfigSetLinkDAO.KEY_COL_NAMES[0],Integer.toHexString((appName + "." + configSetName).hashCode()));
			
			lock.setKeys(keys);
			lock.setLifetime(EnqueueLockManager.LIFETIME_SESSION);
			lock.setTableName(AppConfigSetLinkDAO.TABLE_NAME);
			lock.setLockType(LockManager.WRITE_LOCK);
			
			// wait 200 millis
			lm.lock(lock,200);
			
			// check again
			if(checkAppConfig(conn,appName, configSetName)) return;
			
			
			// Now create
			conn.setAutoCommit(false);
			active = true;
			byte[] configSetId = createAppConfig(conn,appName);
			createConfigSet(conn, configSetId,configSetName);
			conn.commit();
			log.info(LogUtil.APPS_USER_INTERFACE,"Created Empty Config Set {0} for Application {1}", new String[]{configSetName, appName});
		}
		catch(Exception e) {
			if(active && conn != null) conn.rollback();
			throw e;
		}
		finally {
			lc.closeLockManager();
			if(conn != null) 
				conn.close();
		}
	}
		
	private boolean checkAppConfig(Connection conn, String appName, String configSetName) throws SQLException {
		boolean retVal = false;
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			stmt = conn.prepareStatement(SELECT_APPCONFIG_SQL);
			stmt.setString(1,appName);
			stmt.setString(2,configSetName);
			rs = stmt.executeQuery();
			if(rs.next()) {
				retVal = true;
			}
			rs.close(); rs = null;
			stmt.close(); stmt = null;
		}
		finally {
			if(rs != null) rs.close();
			if(stmt != null) stmt.close();
		}
		return retVal;		
	}
	
	private byte[] createAppConfig(Connection conn, String appName) throws SQLException {
		
		PreparedStatement stmt = null;
		byte[] configSetId;
		try {
			configSetId = com.sap.isa.persistence.helpers.OIDGenerator.hexStringToBytes(com.sap.isa.persistence.helpers.OIDGenerator.newOID());
			stmt = conn.prepareStatement(INSERT_APPCONFIG_SQL);
			stmt.setBytes(1,com.sap.isa.persistence.helpers.OIDGenerator.hexStringToBytes(com.sap.isa.persistence.helpers.OIDGenerator.newOID()));
			stmt.setString(2,appName);
			stmt.setBytes(3,configSetId);
			
			stmt.executeUpdate();

			stmt.close(); stmt = null;
		}
		finally {
			if(stmt != null) stmt.close();
		}
		return configSetId;		
	}
	
	private void createConfigSet(Connection conn, byte[] configSetId, String configSetName) throws SQLException {
		PreparedStatement stmt = null;

		try {
			stmt = conn.prepareStatement(INSERT_CONFIGSET_SQL);
			stmt.setBytes(1,configSetId);
			stmt.setString(2,configSetName);
			stmt.setShort(3,(short)0);
			stmt.setString(4,configSetName);
			
			stmt.executeUpdate();
			stmt.close(); stmt = null;
		}
		finally {
			if(stmt != null) stmt.close();
		}		
	}	

}

