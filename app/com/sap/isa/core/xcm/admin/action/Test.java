package com.sap.isa.core.xcm.admin.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sapportals.htmlb.TabStrip;
import com.sapportals.htmlb.event.Event;
import com.sapportals.htmlb.event.TabSelectEvent;
import com.sapportals.htmlb.rendering.IPageContext;
import com.sapportals.htmlb.rendering.PageContextFactory;

/**
 * 
 */
public class Test extends Action {

    public ActionForward perform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
             throws IOException, ServletException {
             	
        IPageContext myContext = PageContextFactory.createPageContext(request, response);
        Event myEvent = myContext.getCurrentEvent();
 		TabStrip tabStrip = (TabStrip)myContext.getComponentForId("myTabStrip1");
 
 		TabSelectEvent event = (TabSelectEvent)myEvent;
 		int index = event.getSelectedIndex();
 		tabStrip.setSelection(1);
             	
     	return mapping.findForward("success");
             	
     }



}
