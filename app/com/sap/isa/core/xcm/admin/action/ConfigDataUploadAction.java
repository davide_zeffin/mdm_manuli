/*
 * Created on Dec 14, 2004
 *
 */
 
package com.sap.isa.core.xcm.admin.action;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;
import org.apache.struts.upload.MultipartRequestWrapper;
import org.w3c.dom.Document;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.xcm.admin.Constants;
import com.sap.isa.core.xcm.admin.SessionConstants;
import com.sap.isa.core.xcm.admin.actionform.UploadForm;
import com.sap.isa.core.xcm.admin.controller.AppConfigController;
import com.sap.isa.core.xcm.admin.controller.ComponentConfigController;
import com.sap.isa.core.xcm.admin.controller.ScenarioConfigController;
import com.sap.isa.core.xcm.admin.helper.DefaultResourceAccessor;
import com.sap.isa.core.xcm.admin.helper.Message;
import com.sap.isa.core.xcm.admin.helper.MessageAccessor;
import com.sap.isa.core.xcm.admin.model.XCMAdminException;
import com.sap.isa.core.xcm.xml.XMLUtil;

/**
 * @author I802891
 *
 */
public class ConfigDataUploadAction extends XCMBaseAction {
	static {
		DefaultResourceAccessor accessor = DefaultResourceAccessor.getInstance();
		accessor.setMessageText(MyMessages.DATA_UPLOAD_FAILED,"Configuration data upload failed");
		accessor.setMessageText(MyMessages.DATA_UPLOAD_SUCCESS,"Configuration data uploaded successfully");
		accessor.setMessageText(MyMessages.DATA_INVALID,"No valid configuration data provided for upload");
		accessor.setMessageText(MyMessages.NOT_WELL_FORMED_XML_DOC,"XML document in file {0} is not well formed");
		accessor.setMessageText(MyMessages.DATA_SAVE_ALERT,"To finish upload, you must perform Save operation");
		accessor.setMessageText(MyMessages.NO_VALID_FILE_FOR_UPLOAD,"No valid file provided for upload");		
	}
	
	public ConfigDataUploadAction() {
		log = IsaLocation.getInstance(ConfigDataUploadAction.class.getName());
	}
	
	/**
	 * @see com.sap.isa.core.xcm.admin.eventhandler.ButtonOnClickEventHandler#perform(ActionMapping, ButtonOnClickEventHandler, IPageContext, HttpSession, HttpServletRequest, HttpServletResponse)
	 */
	public ActionForward perform(
		ActionMapping mapping,
		ActionForm form,		
		HttpServletRequest request,
		HttpServletResponse response) {

		HttpSession session = request.getSession();

		UserSessionData userData = UserSessionData.getUserSessionData(session);
		AppConfigController appConfig = (AppConfigController)userData.getAttribute(SessionConstants.APP_CONFIG_CONTROLLER);
		ComponentConfigController compConfig = appConfig.getCompConfigController();
		ScenarioConfigController scenarioConfig = appConfig.getScenarioConfigController();
		
		Collection msgs = new ArrayList();
		boolean abort = false;
		
		if(request instanceof MultipartRequestWrapper) {
			UploadForm uploadForm = (UploadForm)form;
			FormFile configDataFile = uploadForm.getConfigDataFile();
			FormFile scenarioConfigFile = uploadForm.getScenarioConfigFile();
			Document configDataDoc = null;
			Document scenarioDataDoc = null;
			try {
				if(configDataFile != null && 
					configDataFile.getFileName() != null && 
					configDataFile.getFileName().length() > 0 &&
					configDataFile.getFileSize() > 0) {
					StringBuffer configData = getDataAsString(configDataFile);
					convertISANamespace(configData);
					configDataDoc = XMLUtil.getInstance().getDoc(configData.toString());
					
					if(configDataDoc == null) {
						Message msg = MessageAccessor.getMessageAccessor().getMessage(MyMessages.NOT_WELL_FORMED_XML_DOC, new Object[]{configDataFile.getFileName()});
						msg.setSeverity(Message.ERROR);
						msgs.add(msg);
						abort = true;
					}
				}
	
				if(scenarioConfigFile != null &&
					scenarioConfigFile.getFileName() != null &&
					scenarioConfigFile.getFileName().length() > 0 &&
					scenarioConfigFile.getFileSize() > 0) {
					StringBuffer scenarioData = getDataAsString(scenarioConfigFile);
					convertISANamespace(scenarioData);
					scenarioDataDoc = XMLUtil.getInstance().getDoc(scenarioData.toString());
					
				
					if(scenarioDataDoc == null) {
						Message msg = MessageAccessor.getMessageAccessor().getMessage(MyMessages.NOT_WELL_FORMED_XML_DOC, new Object[]{scenarioConfigFile.getFileName()});
						msg.setSeverity(Message.ERROR);
						msgs.add(msg);
						abort = true;
					}					
				}
				
			}
			catch(IOException ex) {
				log.error(LogUtil.APPS_USER_INTERFACE,"XCM Admin: Configuration data upload failed",ex);
				abort = true;
			}
			
			if(configDataDoc == null && scenarioDataDoc == null) {
				Message msg = MessageAccessor.getMessageAccessor().getMessage(MyMessages.DATA_INVALID);
				msg.setSeverity(Message.ERROR);
				msgs.add(msg);
				abort = true;
			}
			
			Document oldCompConfidDoc = null;
			try {
				if(!abort && configDataDoc != null) {
					oldCompConfidDoc = compConfig.getDataManipulator().getDocBeforeInclude();
					XMLUtil.getInstance().addIdsToDoc(0,configDataDoc);
					compConfig.getDataManipulator().overwriteConfiguration(configDataDoc);
				}
			}
			catch(XCMAdminException ex) {
				log.error(LogUtil.APPS_USER_INTERFACE,"XCM Admin: Configuration data upload failed",ex);				
				abort = true;
			}
			
			try {
				if(!abort && scenarioDataDoc != null) {
					XMLUtil.getInstance().addIdsToDoc(0,scenarioDataDoc);
					scenarioConfig.getDataManipulator().overwriteConfiguration(scenarioDataDoc);
				}
			}
			catch(XCMAdminException ex) {
				log.error(LogUtil.APPS_USER_INTERFACE,"XCM Admin: Configuration data upload failed",ex);				
				try {
					compConfig.getDataManipulator().overwriteConfiguration(oldCompConfidDoc);
				} catch (XCMAdminException ignore) {
					log.debug(ignore.getMessage());
				}
				abort = true;
			}
		}
		
		// Save changes to datastore
		String retval = appConfig.getCompConfigController().saveConfiguration();
		if(retval.equalsIgnoreCase(Constants.OK))
			retval = appConfig.getScenarioConfigController().saveConfiguration();
		else
			abort = true;
								
		if(abort) {
			Message msg = MessageAccessor.getMessageAccessor().getMessage(MyMessages.DATA_UPLOAD_FAILED);
			msg.setSeverity(Message.ERROR);
			msgs.add(msg);
		}
		else {
			// Invalidate the controller
			appConfig.invalidate();
			appConfig.setConfigType(AppConfigController.OPTIONS_MODE);
			
			Message msg = MessageAccessor.getMessageAccessor().getMessage(MyMessages.DATA_UPLOAD_SUCCESS);
			msgs.add(msg);
		}
		
		request.setAttribute(Constants.MESSAGES,msgs);
		
		return mapping.findForward("success");
	}
	
	private StringBuffer getDataAsString(FormFile file) throws IOException {
		StringWriter writer = new StringWriter();
		// It is assumed that the files are UTF-8 encoded on the client
		InputStreamReader reader = new InputStreamReader(file.getInputStream(),"UTF-8");
		
		char[] buffer = new char[256];
		int read = 0;
		while((read = reader.read(buffer)) > 0) {
			writer.write(buffer,0,read);
		}
		return writer.getBuffer();
	}
	
	private static final String NS_ISA_SAPMARKETS = "xmlns:isa=\"com.sapmarkets.isa.core.config\"";
	private static final String NS_ISA_SAP = "xmlns:isa=\"com.sap.isa.core.config\"";
	private static void convertISANamespace(StringBuffer configData) {
		String strData = configData.toString();
		int index = -1;
		while((index = strData.indexOf(NS_ISA_SAPMARKETS)) >=0) {
			configData.replace(index,index + NS_ISA_SAPMARKETS.length(),NS_ISA_SAP);
			strData = configData.toString(); 
		} 
	}
	
	private static interface MyMessages {
		public static final String ID = MyMessages.ID + "_";
		public static final String NOT_WELL_FORMED_XML_DOC = ID + 1;
		public static final String DATA_UPLOAD_FAILED = ID + 2;
		public static final String DATA_UPLOAD_SUCCESS = ID + 3;
		public static final String DATA_INVALID = ID + 4;
		public static final String DATA_SAVE_ALERT = ID + 5;
		public static final String NO_VALID_FILE_FOR_UPLOAD = ID + 6;		
	}
}
