package com.sap.isa.core.xcm.admin.action;

// struts imports
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.xcm.admin.ActionForwardConstants;


/**
 * 
 */
public class ConfigTreeAction extends XCMAdminBaseAction {

	public ActionForward xcmPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws IOException, ServletException {

		log.debug("ConfigTreeAction");

		return mapping.findForward(ActionForwardConstants.SUCCESS);
	}
}
