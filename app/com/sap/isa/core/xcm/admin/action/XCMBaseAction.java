package com.sap.isa.core.xcm.admin.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.logging.IsaLocation;
import com.sapportals.htmlb.event.Event;
import com.sapportals.htmlb.rendering.IPageContext;
import com.sapportals.htmlb.rendering.PageContextFactory;


/**
 * Base Action providing services needed by all actions
 * used in XCM administration
 */
public class XCMBaseAction extends Action {


  protected IsaLocation log = null;

	public XCMBaseAction() {
		log = IsaLocation.getInstance(this.getClass().getName());	
	}

  public ActionForward perform( ActionMapping mapping,
                                ActionForm form,
                                HttpServletRequest request,
                                HttpServletResponse response)
  throws IOException, ServletException {
  	
	IPageContext pageContext = PageContextFactory.createPageContext(request, response);  	
	Event currentEvent = pageContext.getCurrentEvent();
	
  	return null;	
  }
	
	
}
