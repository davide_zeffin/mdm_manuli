package com.sap.isa.core.xcm.admin.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.BaseAction;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.xcm.admin.ActionForwardConstants;
import com.sap.isa.core.xcm.admin.eventhandler.EventHandler;
import com.sap.isa.core.xcm.admin.eventhandler.EventHandlerMap;
import com.sapportals.htmlb.event.Event;
import com.sapportals.htmlb.rendering.IPageContext;
import com.sapportals.htmlb.rendering.PageContextFactory;

/**
 * Base Action providing services needed by all actions
 * used in XCM administration
 * The main service is event dispatching. If there is an event
 * handler for a given event, it is called and the action flow
 * is forwarded to the returned action mapping. If there
 * is no event handler <code>doPerform</code> is called
 */
public class XCMAdminBaseAction extends BaseAction {

	public XCMAdminBaseAction() {
		if (log == null)
			log = IsaLocation.getInstance(this.getClass().getName());
	}

	/**
	 * Handles event dispatching 
	 */
	public ActionForward doPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws IOException, ServletException {
		
		if (log.isDebugEnabled()) {
			log.debug("ENTER perform()");
		}

		HttpSession session = request.getSession();
		IPageContext pageContext =
				PageContextFactory.createPageContext(request, response);
		Event currentEvent = pageContext.getCurrentEvent();
		if (currentEvent != null) {
			if (log.isDebugEnabled())
				log.debug("Event found [event]='" + currentEvent  + "'");

			// get event handler which can handle this event
			EventHandlerMap evMap = EventHandlerMap.getEventHandlerMap();
 
			EventHandler evtHandler = EventHandlerMap.getEventHandlerMap().getEventHandler(form.getClass(), currentEvent.getAction());
			
			if (evtHandler != null) {

				if (log.isDebugEnabled())
					log.debug("Event [handler]='" + evtHandler + "'");

				ActionForward forward = evtHandler.perform(mapping, currentEvent, pageContext, session, request, response);
				if (log.isDebugEnabled()) {
					log.debug("EXIT perform()");
				}

				return forward;
			} else {
				if (log.isDebugEnabled())
					log.debug("No event handler found to handle request. Calling doPerform()");
			}
			
		}

		ActionForward forward = xcmPerform(mapping, form, request, response);
		if (log.isDebugEnabled()) {
			log.debug("EXIT perform()");
		}

		return forward;
	}

	/**
	 * Default implementation
	 * @return always ActionForwardConstants.SUCCESS
	 */
	public ActionForward xcmPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws IOException, ServletException {
			
		return mapping.findForward(ActionForwardConstants.SUCCESS);
	}
	}
