package com.sap.isa.core.xcm.admin.eventhandler.button;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.xcm.admin.ActionForwardConstants;
import com.sap.isa.core.xcm.admin.SessionConstants;
import com.sap.isa.core.xcm.admin.controller.AppConfigController;
import com.sap.isa.core.xcm.admin.controller.ComponentConfigController;
import com.sap.isa.core.xcm.admin.controller.ScenarioConfigController;
import com.sap.isa.core.xcm.admin.eventhandler.ButtonOnClickEventHandler;
import com.sap.isa.core.xcm.admin.model.table.ComponentDetailTableModel;
import com.sapportals.htmlb.event.ButtonClickEvent;
import com.sapportals.htmlb.rendering.IPageContext;
import com.sapportals.htmlb.type.AbstractDataType;

/**
 *
 */
public class CompParamValueHelpEventHandler
	extends ButtonOnClickEventHandler {

	/**
	 * @see com.sap.isa.core.xcm.admin.eventhandler.ButtonOnClickEventHandler#perform(ActionMapping, ButtonOnClickEventHandler, IPageContext, HttpSession, HttpServletRequest, HttpServletResponse)
	 */
	public ActionForward perform(
		ActionMapping mapping,
		ButtonClickEvent event,
		IPageContext pageContext,
		HttpSession session,
		HttpServletRequest request,
		HttpServletResponse response) {
  

        UserSessionData userData = UserSessionData.getUserSessionData(session);
		AppConfigController appConfig = (AppConfigController)userData.getAttribute(SessionConstants.APP_CONFIG_CONTROLLER);
		ComponentConfigController compConfig = appConfig.getCompConfigController();
		/*
		// string comp name equals row number
		String compName = event.getComponentName(); 
		
		TableView tableView = (TableView)pageContext.getComponentForId(CompConfigForm.PARAM_TABLE_VIEW_ID);
		int visibleFirstRow = tableView.getVisibleFirstRow();
		int visibleRowCount = tableView.getVisibleRowCount();
		
		
		int row = Integer.parseInt(compName);
		
		int visibleRow = row % visibleRowCount;		
	
		if (visibleRow == 0)
			visibleRow = visibleRowCount; 
						
		AbstractDataType value = pageContext.getDataForComponentId(CompConfigForm.PARAM_TABLE_VIEW_ID, ComponentDetailTableModel.NAME_PARAM_VALUE_DROPDOWN_LISTBOX, visibleRow);
		if (value.toString() != null) {
			ComponentDetailTableModel model = compConfig.getCompDetailTableModel();
			AbstractDataType adt = model.getValueAt(row, 1);
			if (adt.toString() != null) {
				String paramName = adt.toString();	
				String  paramValue = value.toString();	

				String shorttext = compConfig.getConfigParamValueShorttext(paramName, paramValue);
				String longtext = compConfig.getConfigParamValueLongtext(paramName, paramValue);
				if (shorttext == null)
					shorttext = Constants.MSG_SHORTTEXT_MISSING;


				compConfig.isCompParamHelp(true);
				model.setValueAt(value, row, 1);
				compConfig.saveTableModel();
				//compConfig.updateConfigController(compConfig.getCurrentCompId());
				userData.setAttribute(SessionConstants.SCEN_CONFIG_PARAM_VALUE_HELP_SHORTTEXT, shorttext);
				userData.setAttribute(SessionConstants.SCEN_CONFIG_PARAM_VALUE_HELP_LONGTEXT, longtext);
				userData.setAttribute(SessionConstants.SCEN_CONFIG_PARAM_VALUE_HELP, paramValue);
			}*/
			
			compConfig.isCompParamValueHelp(true);
			String compName = event.getComponentName(); 
			int row = Integer.parseInt(compName);
			ComponentDetailTableModel model = compConfig.getCompDetailTableModel();
			AbstractDataType adt = model.getValueAt(row, 1);
			compName = compConfig.getCurrentCompId();
			if (adt.toString() != null) {
				String paramName = adt.toString();
				userData.setAttribute(SessionConstants.PARAM_NAME, paramName);
				userData.setAttribute(SessionConstants.COMP_NAME, compName);
			}
			
			
		
		return mapping.findForward(ActionForwardConstants.SUCCESS);
	}

}
