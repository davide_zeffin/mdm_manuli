package com.sap.isa.core.xcm.admin.eventhandler.button;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.xcm.admin.ActionForwardConstants;
import com.sap.isa.core.xcm.admin.SessionConstants;
import com.sap.isa.core.xcm.admin.actionform.CompConfigForm;
import com.sap.isa.core.xcm.admin.controller.AppConfigController;
import com.sap.isa.core.xcm.admin.controller.ComponentConfigController;
import com.sap.isa.core.xcm.admin.controller.HelpController;
import com.sap.isa.core.xcm.admin.eventhandler.ButtonOnClickEventHandler;
import com.sap.isa.core.xcm.admin.model.table.ComponentDetailTableModel;
import com.sapportals.htmlb.event.ButtonClickEvent;
import com.sapportals.htmlb.rendering.IPageContext;
import com.sapportals.htmlb.table.TableView;
import com.sapportals.htmlb.type.AbstractDataType;


/**
 *
 */
public class CompConfigResotoreValueEventHandler
	extends ButtonOnClickEventHandler {

	/**
	 * @see com.sap.isa.core.xcm.admin.eventhandler.ButtonOnClickEventHandler#perform(ActionMapping, ButtonOnClickEventHandler, IPageContext, HttpSession, HttpServletRequest, HttpServletResponse)
	 */
	public ActionForward perform(
		ActionMapping mapping,
		ButtonClickEvent event,
		IPageContext pageContext,
		HttpSession session,
		HttpServletRequest request,
		HttpServletResponse response) {
  

        UserSessionData userData = UserSessionData.getUserSessionData(session);
		AppConfigController appConfig = (AppConfigController)userData.getAttribute(SessionConstants.APP_CONFIG_CONTROLLER);
		ComponentConfigController compConfig = appConfig.getCompConfigController();
		HelpController helpController = appConfig.getHelpController(); 

		// string comp name equals row number
		String compName = event.getComponentName(); 
		
		TableView tableView = (TableView)pageContext.getComponentForId(CompConfigForm.PARAM_TABLE_VIEW_ID);
		int visibleFirstRow = tableView.getVisibleFirstRow();
		int visibleRowCount = tableView.getVisibleRowCount();
		
		 
		int row = Integer.parseInt(compName);

		// get name of parameter
		ComponentDetailTableModel model = compConfig.getCompDetailTableModel();
		AbstractDataType adt = model.getValueAt(row, 1);
		if (adt.toString() != null) {
			String paramName = adt.toString();
			
			String baseValue = compConfig.restoreParameterBaseValue(paramName);
			compConfig.updateConfigController(compConfig.getCurrentParamConfigId());
			
		}
		return mapping.findForward(ActionForwardConstants.SUCCESS);
	}

}
