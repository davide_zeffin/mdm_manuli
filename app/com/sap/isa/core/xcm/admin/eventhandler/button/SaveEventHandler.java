/*
 * Created on Nov 18, 2004
 *
 */
package com.sap.isa.core.xcm.admin.eventhandler.button;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.config.AdminConfig;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.xcm.admin.ActionForwardConstants;
import com.sap.isa.core.xcm.admin.Constants;
import com.sap.isa.core.xcm.admin.SessionConstants;
import com.sap.isa.core.xcm.admin.actionform.HomeForm;
import com.sap.isa.core.xcm.admin.controller.AppConfigController;
import com.sap.isa.core.xcm.admin.controller.ComponentConfigController;
import com.sap.isa.core.xcm.admin.controller.LockingController;
import com.sap.isa.core.xcm.admin.controller.ScenarioConfigController;
import com.sap.isa.core.xcm.admin.eventhandler.ButtonOnClickEventHandler;
import com.sap.isa.core.xcm.admin.helper.DefaultResourceAccessor;
import com.sap.isa.core.xcm.admin.helper.Message;
import com.sap.isa.core.xcm.admin.helper.MessageAccessor;
import com.sap.isa.core.xcm.admin.model.XCMAdminException;
import com.sap.tc.logging.Category;
import com.sapportals.htmlb.event.ButtonClickEvent;
import com.sapportals.htmlb.rendering.IPageContext;

/**
  * <p> 
 * This event handler is responsible for save the
 * configurations to datastore. This operation insures
 * latest updated data is saved to datastore
 * This button is enabled when the User switched to Edit Mode
 * </p>
 *  
 * @author I802891
 */
public class SaveEventHandler extends ButtonOnClickEventHandler {

	protected static IsaLocation log = IsaLocation.
		getInstance(AppConfigController.class.getName());


	static {
		DefaultResourceAccessor accessor = DefaultResourceAccessor.getInstance();
		accessor.setMessageText(MyMessages.SAVE_FAILURE,"Failed to save configuration data. Configuration changes will not be available after restarting the application");
		accessor.setMessageText(MyMessages.SAVE_FAILURE_RECOMMENDATION,"It is recommeded to cancel the Edit operation");		
		accessor.setMessageText(MyMessages.SAVE_SUCCESS,"Configuration data saved successfully. Configuration changes will be available after restarting the application");
		accessor.setMessageText(MyMessages.REFRESH_FAILURE,"Configuration data refresh failed");
	}
	/**
	 * @see com.sap.isa.core.xcm.admin.eventhandler.ButtonOnClickEventHandler#perform(ActionMapping, ButtonOnClickEventHandler, IPageContext, HttpSession, HttpServletRequest, HttpServletResponse)
	 * 
	 * This event handler handles both Save and Cancel button click events
	 */
	public ActionForward perform(
		ActionMapping mapping,
		ButtonClickEvent event,
		IPageContext pageContext,
		HttpSession session,
		HttpServletRequest request,
		HttpServletResponse response) {
  
		// check again if the user has XCM Admin rights???
		if(!AdminConfig.isXCMAdmin(request)) {
			return mapping.findForward(ActionForwardConstants.FAILURE);
		}
		
		UserSessionData userData = UserSessionData.getUserSessionData(session);
		AppConfigController appConfig = (AppConfigController)userData.getAttribute(SessionConstants.APP_CONFIG_CONTROLLER);
		ComponentConfigController compConfig = appConfig.getCompConfigController();
		ScenarioConfigController scenarioConfig = appConfig.getScenarioConfigController();
		LockingController lockController = 
			(LockingController)userData.getAttribute(SessionConstants.LOCKING_CONTROLLER);
		
		String eventId = event.getAction();
		
		ArrayList msgs = new ArrayList();
		
		String retval = Constants.OK;
		
		if(eventId.equals(HomeForm.ON_SAVE_CLICK_EVENT)) {
			// Save changes to datastore
			retval = appConfig.getCompConfigController().saveConfiguration();
			if(retval.equalsIgnoreCase(Constants.OK))
				retval = appConfig.getScenarioConfigController().saveConfiguration();

			if(retval.equals(Constants.OK)) {	
				msgs.add(MessageAccessor.getMessageAccessor().getMessage(MyMessages.SAVE_SUCCESS));			
			}
			else {
				msgs.add(MessageAccessor.getMessageAccessor().getMessage(MyMessages.SAVE_FAILURE));			
				msgs.add(MessageAccessor.getMessageAccessor().getMessage(MyMessages.SAVE_FAILURE_RECOMMENDATION));			
			}				
		}
		
		if(eventId.equals(HomeForm.ON_CANCEL_CLICK_EVENT)) {
			// invalidate changes made to configuration data models
			try {
				compConfig.getDataManipulator().invalidateConfiguration();
				scenarioConfig.getDataManipulator().invalidateConfiguration();
			}
			catch(XCMAdminException ex) {
				Message msg = MessageAccessor.getMessageAccessor().getMessage(MyMessages.REFRESH_FAILURE);
				msg.setSeverity(Message.ERROR);
				msgs.add(msg);
				log.error(LogUtil.APPS_USER_INTERFACE,msg.getMessageText(),ex);
			}
			// invalidate the controller
			appConfig.invalidate();
		}
		
		// Common handling code for Save and Cancel buttons
		if(retval.equals(Constants.OK)) {	
			if(lockController != null) {
				
				// This implicitly release locks 
				try {
					lockController.closeLockManager();
				}
				catch(Exception e) {
					log.debug(e.getMessage());
				}
			}
			appConfig.setEditMode(false);
		}
		
		
		request.setAttribute(Constants.SAVE_STATUS, retval);
		request.setAttribute(Constants.MESSAGES,msgs);

		log.info(Category.APPS_COMMON_CONFIGURATION, "xcm.config.save");
		
		return mapping.findForward(ActionForwardConstants.SUCCESS);		
	}
	
	private static interface MyMessages {
		public static final String ID = MyMessages.class.getName() + "_";
		
		public static final String SAVE_SUCCESS = ID + 1;
		public static final String SAVE_FAILURE = ID + 2;
		public static final String SAVE_FAILURE_RECOMMENDATION = ID + 3;		
		public static final String REFRESH_FAILURE = ID + 4;
	}
}
