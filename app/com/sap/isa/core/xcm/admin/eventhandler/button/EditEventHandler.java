/*
 * Created on Nov 17, 2004
 */
package com.sap.isa.core.xcm.admin.eventhandler.button;

import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.config.AdminConfig;
import com.sap.isa.core.locking.LockException;
import com.sap.isa.core.locking.LockManager;
import com.sap.isa.core.locking.LockNotGrantedException;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.xcm.admin.ActionForwardConstants;
import com.sap.isa.core.xcm.admin.Constants;
import com.sap.isa.core.xcm.admin.SessionConstants;
import com.sap.isa.core.xcm.admin.actionform.HomeForm;
import com.sap.isa.core.xcm.admin.controller.AppConfigController;
import com.sap.isa.core.xcm.admin.controller.LockingController;
import com.sap.isa.core.xcm.admin.eventhandler.ButtonOnClickEventHandler;
import com.sap.isa.core.xcm.admin.helper.DefaultResourceAccessor;
import com.sap.isa.core.xcm.admin.helper.Message;
import com.sap.isa.core.xcm.admin.helper.MessageAccessor;
import com.sapportals.htmlb.event.ButtonClickEvent;
import com.sapportals.htmlb.rendering.IPageContext;

/**
 * <p> 
 * This event handler is responsible for starting the edit of 
 * configurations by XCM Admin
 * By default the data is provided in read only mode
 * </p>
 * 
 * @author I802891
 */
public class EditEventHandler extends ButtonOnClickEventHandler {
	
	static {
		DefaultResourceAccessor accessor = DefaultResourceAccessor.getInstance();
		accessor.setMessageText(MyMessages.CONFIGURATION_ALREADY_LOCKED,"Configuration is already locked for Edit by another User. Please wait for some time for other user to finish Edit operation");
		accessor.setMessageText(MyMessages.CONFIGURATION_LOCK_SUCCESS,"Configuration is exclusively locked for Edit. Don't forget to switch to Display mode after saving changes");
		accessor.setMessageText(MyMessages.LOCK_SERVICE_INIT_FAILED,"Lock manager initialized failed.");
		accessor.setMessageText(MyMessages.NON_LOCK_MODE_USAGE,"Reverted to non-locked mode. Concurrent users can modify Configuration data simultaneously.");
		accessor.setMessageText(MyMessages.CONFIGURATION_UPDATE_HINT,"'Update Configuration' must be done for changes made in Edit mode(required before 'Save')");		
	}
	
	public EditEventHandler() {
		log = IsaLocation.getInstance(EditEventHandler.class.getName());
	}
	
	/**
	 * @see com.sap.isa.core.xcm.admin.eventhandler.ButtonOnClickEventHandler#perform(ActionMapping, ButtonOnClickEventHandler, IPageContext, HttpSession, HttpServletRequest, HttpServletResponse)
	 */
	public ActionForward perform(
		ActionMapping mapping,
		ButtonClickEvent event,
		IPageContext pageContext,
		HttpSession session,
		HttpServletRequest request,
		HttpServletResponse response) {
		
		// do this operation only if the user ia a XCM Admin because only them is allowed to switch to edit mode, all others not!
		if(!AdminConfig.isXCMAdmin(request)) {
			Collection msgs = new ArrayList();
			Message msg = MessageAccessor.getMessageAccessor().getMessage(MyMessages.LOCK_SERVICE_INIT_FAILED);
			msg.setSeverity(Message.ERROR);
			msgs.add(msg);
			request.setAttribute(Constants.MESSAGES,msgs);
			
			return mapping.findForward(ActionForwardConstants.SUCCESS);
		}
		UserSessionData userData = UserSessionData.getUserSessionData(session);
		AppConfigController appConfig = (AppConfigController)userData.getAttribute(SessionConstants.APP_CONFIG_CONTROLLER);
		LockingController lockController = (LockingController)userData.getAttribute(SessionConstants.LOCKING_CONTROLLER);

		// string comp name equals row number
		String eventId = event.getAction();
		
		boolean lockSuccess = true;
		
		Collection msgs = new ArrayList();
		
		if(eventId.equalsIgnoreCase(HomeForm.ON_EDIT_CLICK_EVENT)) {
			if(!appConfig.isEditMode()) {
				
				if(lockController != null) {
					// init a Lock Manager session, lazy initialization
					try {
						lockController.initLockManager();
					}
					catch(Exception ex) {
						// Lock service Initialization failed
						// Revert to Non - Locked Mode - show warning
						// Allow data editing at this level
						Message msg = MessageAccessor.getMessageAccessor().getMessage(MyMessages.LOCK_SERVICE_INIT_FAILED);
						msg.setSeverity(Message.ERROR);
						msgs.add(msg);
						
						log.warn(LogUtil.APPS_USER_INTERFACE,msg.getMessageText(),ex);
						
						msg = MessageAccessor.getMessageAccessor().getMessage(MyMessages.NON_LOCK_MODE_USAGE);
						msg.setSeverity(Message.WARNING);
						msgs.add(msg);
						
						log.warn(LogUtil.APPS_USER_INTERFACE,msg.getMessageText());
					}
					
					LockManager lm = lockController.getLockManager();
					
					if(lm != null) {
						// Lock the configurations
						try {
							appConfig.lock(lm);
							msgs.add(MessageAccessor.getMessageAccessor().getMessage(MyMessages.CONFIGURATION_LOCK_SUCCESS));
						}
						catch(LockException e) {
							if(e instanceof LockNotGrantedException) {
								lockSuccess = false;
								msgs.add(MessageAccessor.getMessageAccessor().getMessage(MyMessages.CONFIGURATION_ALREADY_LOCKED));
							}
							// Failed to obtain a lock, cancel edit operation
							// Do not allow editing
							try {
								lockController.closeLockManager();
							} catch (Exception e1) {
								log.warn(LogUtil.APPS_USER_INTERFACE,"Lock manager close failed",e1);
							}
						}
					}
				}				
				// set edit mode to true 
				if(lockSuccess) appConfig.setEditMode(true);
			}
		}
	
		if(eventId.equalsIgnoreCase(HomeForm.ON_DISPLAY_CLICK_EVENT)) {
			if(appConfig.isEditMode()) {
	
				if(lockController != null) {
					// This implicitly release locks 
					try {
						lockController.closeLockManager();
					}
					catch(Exception e) {
						log.debug(e.getMessage());
					}
				}
				appConfig.setEditMode(false);	
			}		
		}
			
		request.setAttribute(Constants.MESSAGES,msgs);
		
		return mapping.findForward(ActionForwardConstants.SUCCESS);		
	}
	
	private static interface MyMessages {
		public static final String ID = MyMessages.class.getName() + "_";
		public static final String LOCK_SERVICE_INIT_FAILED = ID + 1;
		public static final String NON_LOCK_MODE_USAGE = ID + 2;
		public static final String CONFIGURATION_ALREADY_LOCKED = ID + 3;
		public static final String CONFIGURATION_LOCK_SUCCESS = ID + 4;
		public static final String CONFIGURATION_UPDATE_HINT = ID + 5;		
	}
}
