package com.sap.isa.core.xcm.admin.eventhandler.button;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.config.AdminConfig;
import com.sap.isa.core.xcm.admin.ActionForwardConstants;
import com.sap.isa.core.xcm.admin.SessionConstants;
import com.sap.isa.core.xcm.admin.actionform.CompDescrForm;
import com.sap.isa.core.xcm.admin.controller.AppConfigController;
import com.sap.isa.core.xcm.admin.controller.ComponentConfigController;
import com.sap.isa.core.xcm.admin.controller.HelpController;
import com.sap.isa.core.xcm.admin.eventhandler.ButtonOnClickEventHandler;
import com.sapportals.htmlb.TreeNode;
import com.sapportals.htmlb.event.ButtonClickEvent;
import com.sapportals.htmlb.rendering.IPageContext;
import com.sapportals.htmlb.type.AbstractDataType;

/**
 *
 */
public class CompConfigCreateEventHandler
	extends ButtonOnClickEventHandler {

	/**
	 * @see com.sap.isa.core.xcm.admin.eventhandler.ButtonOnClickEventHandler#perform(ActionMapping, ButtonOnClickEventHandler, IPageContext, HttpSession, HttpServletRequest, HttpServletResponse)
	 */
	public ActionForward perform(
		ActionMapping mapping,
		ButtonClickEvent event,
		IPageContext pageContext,
		HttpSession session,
		HttpServletRequest request,
		HttpServletResponse response) {
		
			// check again if the user has XCM Admin rights???
			if(!AdminConfig.isXCMAdmin(request)) {
				return mapping.findForward(ActionForwardConstants.FAILURE);
			}
			
			UserSessionData userData = UserSessionData.getUserSessionData(session);

			AppConfigController appConfig = (AppConfigController)userData.getAttribute(SessionConstants.APP_CONFIG_CONTROLLER);
			ComponentConfigController compConfig = appConfig.getCompConfigController();
			HelpController helpController = appConfig.getHelpController();
		
			// get new configuration name
			AbstractDataType adt = pageContext.getDataForComponentId(CompDescrForm.CONFIG_NAME_INPUT_FIELD_ID);
			if (adt == null) {
				// do not switch to config configuration
				return mapping.findForward(ActionForwardConstants.COMP_DESCR);
			}
			
			String paramConfigName = adt.toString();
			if (paramConfigName.length() == 0) {
				// do not switch to config configuration
				return mapping.findForward(ActionForwardConstants.COMP_DESCR);
			}
			
			
			appConfig.setConfigType(AppConfigController.COMPONENTS_CONFIG_MODE);
			// switch in the customer branch
			compConfig.isSAP(false);
			


			// update controller
			compConfig.updateConfigController(paramConfigName);


			// check if parameter configuration already available
			if (compConfig.isParamConfigIdExisting(paramConfigName)) {
				compConfig.updateConfigController(paramConfigName);
				appConfig.getConfigTreeModel().
					setComponentActiveNode(compConfig.getCurrentCompId(), paramConfigName, compConfig.isSAP());
				compConfig.isCurrentParamConfigNew(false);
				if (log.isDebugEnabled())
					log.debug("Creating a new parameter configuration with existing [name]='" + paramConfigName + "'");
			} else {
				// create
				compConfig.createCompParamConfig(paramConfigName);
				compConfig.updateConfigController(paramConfigName);
				compConfig.isCurrentParamConfigNew(true);				
				if (log.isDebugEnabled())
					log.debug("Creating a new parameter configuration [name]='" + paramConfigName + "'");

			}

			TreeNode currentNode = appConfig.getConfigTreeModel().getActiveNode();
			currentNode.setOpen(true);
			
			helpController.updateController();

			return mapping.findForward(ActionForwardConstants.SUCCESS);
	}

}
