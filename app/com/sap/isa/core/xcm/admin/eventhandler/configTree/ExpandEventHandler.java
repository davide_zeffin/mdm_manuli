package com.sap.isa.core.xcm.admin.eventhandler.configTree;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.xcm.admin.ActionForwardConstants;
import com.sap.isa.core.xcm.admin.SessionConstants;
import com.sap.isa.core.xcm.admin.controller.AppConfigController;
import com.sap.isa.core.xcm.admin.eventhandler.TreeNodeExpandEventHandler;
import com.sap.isa.core.xcm.admin.model.tree.ConfigTreeModel;
import com.sapportals.htmlb.TreeNode;
import com.sapportals.htmlb.event.TreeNodeExpandEvent;
import com.sapportals.htmlb.rendering.IPageContext;

/**
 *
 */
public class ExpandEventHandler extends TreeNodeExpandEventHandler {

	/**
	 * @see com.sap.isa.core.xcm.admin.eventhandler.EventHandler#perform(ActionMapping, Event, IPageContext, HttpSession, HttpServletRequest, HttpServletResponse)
	 */
	public ActionForward perform(
		ActionMapping mapping,
		TreeNodeExpandEvent event,
		IPageContext pageContext,
		HttpSession session,
		HttpServletRequest request,
		HttpServletResponse response) {

        UserSessionData userData = UserSessionData.getUserSessionData(session);
		
		AppConfigController appConfig = (AppConfigController)userData.getAttribute(SessionConstants.APP_CONFIG_CONTROLLER);

		if (appConfig == null)
			return mapping.findForward(ActionForwardConstants.SUCCESS);
					
		String nodeId = event.getNodeKey();
		// set the current node in session

		if (log.isDebugEnabled())
			log.debug("Node Id selected [id]='" + nodeId + "'");
		
		
		ConfigTreeModel treeModel = appConfig.getConfigTreeModel();		
		TreeNode node = treeModel.getNodeByID(nodeId);
		//TreeNode node = treeModel.getNodeByID("Scenarios.SAP"); 
		
		if (node != null)
			node.setOpen(true);
		
		return mapping.findForward(ActionForwardConstants.SUCCESS);
	}

}
