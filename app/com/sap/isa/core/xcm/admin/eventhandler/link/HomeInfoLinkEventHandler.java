package com.sap.isa.core.xcm.admin.eventhandler.link;

// servlet imports
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.xcm.admin.ActionForwardConstants;
import com.sap.isa.core.xcm.admin.SessionConstants;
import com.sap.isa.core.xcm.admin.actionform.HomeForm;
import com.sap.isa.core.xcm.admin.controller.AppConfigController;
import com.sap.isa.core.xcm.admin.controller.ComponentConfigController;
import com.sap.isa.core.xcm.admin.eventhandler.LinkBaseEventHandler;
import com.sap.isa.core.xcm.admin.model.tree.ConfigTreeModel;
import com.sapportals.htmlb.event.LinkClickEvent;
import com.sapportals.htmlb.rendering.IPageContext;


public class HomeInfoLinkEventHandler extends LinkBaseEventHandler {

	/**
	 * Has to be implemented by the extending class
	 */
	public ActionForward perform( ActionMapping mapping,
										   LinkClickEvent event,
										   IPageContext pageContext,
										   HttpSession session,
										   HttpServletRequest request,
										   HttpServletResponse response) {
											
		String eventId = event.getAction();
		
		UserSessionData userData = UserSessionData.getUserSessionData(session);

		AppConfigController appConfig = (AppConfigController)userData.getAttribute(SessionConstants.APP_CONFIG_CONTROLLER);
		ComponentConfigController compConfig = appConfig.getCompConfigController();

		if (eventId.equals(HomeForm.ON_COMPONENT_HOME_HELP_EVENT)) {
			appConfig.getConfigTreeModel().setComponentsActiveTreeNode();
			appConfig.setConfigType(AppConfigController.COMPONENTS_OVERVIEW_MODE);
			appConfig.getConfigTreeModel().expandNode(ConfigTreeModel.COMP_CUST_NODE_ID, true);
			appConfig.getConfigTreeModel().expandNode(ConfigTreeModel.COMP_SAP_NODE_ID, true);
		}
				
		if (eventId.equals(HomeForm.ON_SCENARIO_HOME_HELP_EVENT)) {
			appConfig.getConfigTreeModel().setScenariosActiveTreeNode();
			appConfig.getConfigTreeModel().expandNode(ConfigTreeModel.SCENARIOS_SAP_NODE_ID, true);
			appConfig.getConfigTreeModel().expandNode(ConfigTreeModel.SCENARIOS_CUST_NODE_ID, true);
			appConfig.setConfigType(AppConfigController.SCENARIO_OVERVIEW_MODE);
		}

		return mapping.findForward(ActionForwardConstants.SUCCESS);						   	
	}
									


}
