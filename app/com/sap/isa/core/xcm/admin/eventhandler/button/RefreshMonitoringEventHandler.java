/*
 * Created on Nov 17, 2004
 */
package com.sap.isa.core.xcm.admin.eventhandler.button;



import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.xcm.admin.ActionForwardConstants;
import com.sap.isa.core.xcm.admin.eventhandler.ButtonOnClickEventHandler;
import com.sapportals.htmlb.event.ButtonClickEvent;
import com.sapportals.htmlb.rendering.IPageContext;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.xcm.admin.ActionForwardConstants;
import com.sap.isa.core.xcm.admin.SessionConstants;
import com.sap.isa.core.xcm.admin.controller.MonitoringController;
/**
  * <p> 
 * This event handler is responsible for refreshing the
 * configurations from datastore. This operation insures
 * latest data is used for editing
 * This button is enabled when the User switched to Edit Mode
 * </p>
 *  
 * @author I802891
 */
public class RefreshMonitoringEventHandler extends ButtonOnClickEventHandler {
	
		
	public RefreshMonitoringEventHandler() {
		log = IsaLocation.getInstance(RefreshMonitoringEventHandler.class.getName());
	}
	
	/**
	 * @see com.sap.isa.core.xcm.admin.eventhandler.ButtonOnClickEventHandler#perform(ActionMapping, ButtonOnClickEventHandler, IPageContext, HttpSession, HttpServletRequest, HttpServletResponse)
	 */
	public ActionForward perform(
		ActionMapping mapping,
		ButtonClickEvent event,
		IPageContext pageContext,
		HttpSession session,
		HttpServletRequest request,
		HttpServletResponse response) {
  
			UserSessionData userData = UserSessionData.getUserSessionData(session);
			MonitoringController monController = (MonitoringController)userData.getAttribute(SessionConstants.MONITORING_CONTROLLER);
			monController.invalidate();
			return mapping.findForward(ActionForwardConstants.SUCCESS);
		
				
	}
	

}
