package com.sap.isa.core.xcm.admin.eventhandler.table;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.xcm.admin.ActionForwardConstants;
import com.sap.isa.core.xcm.admin.Constants;
import com.sap.isa.core.xcm.admin.SessionConstants;
import com.sap.isa.core.xcm.admin.actionform.CompConfigForm;
import com.sap.isa.core.xcm.admin.controller.AppConfigController;
import com.sap.isa.core.xcm.admin.controller.ComponentConfigController;
import com.sap.isa.core.xcm.admin.eventhandler.TableOnNavigateEvent;
import com.sap.isa.core.xcm.admin.model.table.ComponentDetailTableModel;
import com.sapportals.htmlb.event.TableNavigationEvent;
import com.sapportals.htmlb.rendering.IPageContext;
import com.sapportals.htmlb.type.AbstractDataType;

/**
 *
 */
public class CompConfigTableNavEventHandler extends TableOnNavigateEvent {

	/**
	 * @see com.sap.isa.core.xcm.admin.eventhandler.TableOnNavigateEvent#perform(ActionMapping, TableNavigationEvent, IPageContext, HttpSession, HttpServletRequest, HttpServletResponse)
	 */
	public ActionForward perform(
		ActionMapping mapping,
		TableNavigationEvent event,
		IPageContext pageContext,
		HttpSession session,
		HttpServletRequest request,
		HttpServletResponse response) {
			
        UserSessionData userData = UserSessionData.getUserSessionData(session);

		AppConfigController appConfig = (AppConfigController)userData.getAttribute(SessionConstants.APP_CONFIG_CONTROLLER);
		ComponentConfigController compConfig = appConfig.getCompConfigController();
		
		int firstVisRow = event.getFirstVisibleRow();
		int visibleRowCount = compConfig.getCompDetailTableView().getVisibleRowCount();

		int lastRowModel = visibleRowCount + firstVisRow - 1;
		
		ComponentDetailTableModel model = compConfig.getCompDetailTableModel();

		if (log.isDebugEnabled())
			log.debug("[last row in model]='" + model.getRowCount());

		try {

			if (lastRowModel > model.getRowCount())
				lastRowModel = model.getRowCount();
	
			int row = firstVisRow;
			int loopcount = 0;
			for (int i = 1; i <= visibleRowCount; i++) {
				loopcount++;
				AbstractDataType value = pageContext.getDataForComponentId(CompConfigForm.PARAM_TABLE_VIEW_ID, ComponentDetailTableModel.NAME_PARAM_VALUE_INPUT_FIELD, loopcount);
				if (value == null || value.getValueAsString() == null)
					value = pageContext.getDataForComponentId(CompConfigForm.PARAM_TABLE_VIEW_ID, ComponentDetailTableModel.NAME_PARAM_VALUE_DROPDOWN_LISTBOX, loopcount);

				if (!value.getValueAsString().equals(Constants.DISPLAY_PASSWORD))
					model.setValueAt(value, row, 2);	

				row++;

			}
		} catch (Throwable t) {
			log.error(LogUtil.APPS_USER_INTERFACE,"system.xcm.admin.exception", new Object[] {}, t);
		}
	
		if (event.getNavigationType().equals(TableNavigationEvent.EVENT_PAGEDOWN))
			firstVisRow += visibleRowCount;

		if (event.getNavigationType().equals(TableNavigationEvent.EVENT_PAGEUP))
			firstVisRow -= visibleRowCount;

		if (event.getNavigationType().equals(TableNavigationEvent.EVENT_BOTTOM)) {
			int numRows = compConfig.getCompDetailTableModel().getRowCount();
			int deltas = numRows / visibleRowCount;
			if (visibleRowCount * deltas == numRows)
				firstVisRow = visibleRowCount * (deltas - 1) + 1;
			else
				firstVisRow = visibleRowCount * deltas + 1;
		}
 
		if (event.getNavigationType().equals(TableNavigationEvent.EVENT_TOP))
			firstVisRow = 1;

		compConfig.getCompDetailTableView().setVisibleFirstRow(firstVisRow);

	
		return mapping.findForward(ActionForwardConstants.SUCCESS);
	}

}
