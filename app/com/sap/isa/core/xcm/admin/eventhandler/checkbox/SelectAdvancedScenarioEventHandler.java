package com.sap.isa.core.xcm.admin.eventhandler.checkbox;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.xcm.admin.ActionForwardConstants;
import com.sap.isa.core.xcm.admin.SessionConstants;
import com.sap.isa.core.xcm.admin.controller.AppConfigController;
import com.sap.isa.core.xcm.admin.controller.ScenarioConfigController;
import com.sap.isa.core.xcm.admin.eventhandler.CheckBoxClickEventHandler;
import com.sapportals.htmlb.event.CheckboxClickEvent;
import com.sapportals.htmlb.rendering.IPageContext;

public class SelectAdvancedScenarioEventHandler
	extends CheckBoxClickEventHandler {

	public ActionForward perform(
		ActionMapping mapping,
		CheckboxClickEvent event,
		IPageContext pageContext,
		HttpSession session,
		HttpServletRequest request,
		HttpServletResponse response) {
			
		UserSessionData userData = UserSessionData.getUserSessionData(session);
		AppConfigController appConfig = (AppConfigController)userData.getAttribute(SessionConstants.APP_CONFIG_CONTROLLER);

		ScenarioConfigController scenConfig = appConfig.getScenarioConfigController();

		scenConfig.isAdvancedOptionsOn(!scenConfig.isAdvancedOptionsOn());
 			
		scenConfig.updateConfigController(scenConfig.getCurrentScenarioName());
		return mapping.findForward(ActionForwardConstants.SUCCESS);
	}

}
