package com.sap.isa.core.xcm.admin.eventhandler.configTree;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.xcm.admin.ActionForwardConstants;
import com.sap.isa.core.xcm.admin.SessionConstants;
import com.sap.isa.core.xcm.admin.controller.AppConfigController;
import com.sap.isa.core.xcm.admin.eventhandler.TreeNodeSelectEventHandler;
import com.sap.isa.core.xcm.admin.model.tree.ConfigTreeModel;
import com.sapportals.htmlb.TreeNode;
import com.sapportals.htmlb.event.TreeNodeSelectEvent;
import com.sapportals.htmlb.rendering.IPageContext;

/**
 *
 */
public class HomeSelectEventHandler extends TreeNodeSelectEventHandler {

	/**
	 * @see com.sap.isa.core.xcm.admin.eventhandler.EventHandler#perform(ActionMapping, Event, IPageContext, HttpSession, HttpServletRequest, HttpServletResponse)
	 */
	public ActionForward perform(
		ActionMapping mapping,
		TreeNodeSelectEvent event,
		IPageContext pageContext,
		HttpSession session,
		HttpServletRequest request,
		HttpServletResponse response) {


        UserSessionData userData = UserSessionData.getUserSessionData(session);
		
		AppConfigController appConfig = (AppConfigController)userData.getAttribute(SessionConstants.APP_CONFIG_CONTROLLER);

		if (appConfig == null)
			return mapping.findForward(ActionForwardConstants.FAILURE);
					
		String mode = event.getNodeKey();
		// set the current node in session
		
		TreeNode node = appConfig.getConfigTreeModel().getNodeByID(mode);
		
		if (log.isDebugEnabled())
			log.debug("Mode selected [id]='" + mode + "'");

		appConfig.getConfigTreeModel().setActiveNode(node);

		if (mode == null)
			return mapping.findForward(ActionForwardConstants.SUCCESS);

		if (getMode(mode) != null)
			appConfig.setConfigType(getMode(mode));

		appConfig.isConfigCompFromScenario(false);
				
		return mapping.findForward(ActionForwardConstants.SUCCESS);
	}
	
	public static String getMode(String nodeId) {
		if (nodeId.equals(ConfigTreeModel.OPTIONS_NODE_ID))
			return AppConfigController.OPTIONS_MODE;
 		if(nodeId.equals(ConfigTreeModel.COMP_NODE_ID))
 			return AppConfigController.COMPONENTS_OVERVIEW_MODE;
		if (nodeId.equals(ConfigTreeModel.SCENARIOS_NODE_ID))
 			return AppConfigController.SCENARIO_OVERVIEW_MODE;
		if (nodeId.equals(ConfigTreeModel.HOME_NODE_ID))
			return AppConfigController.HOME_MODE;



		return null;
	}

}
