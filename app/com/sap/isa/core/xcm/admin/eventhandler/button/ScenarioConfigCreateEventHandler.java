package com.sap.isa.core.xcm.admin.eventhandler.button;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.config.AdminConfig;
import com.sap.isa.core.xcm.admin.ActionForwardConstants;
import com.sap.isa.core.xcm.admin.SessionConstants;
import com.sap.isa.core.xcm.admin.actionform.ScenarioConfigForm;
import com.sap.isa.core.xcm.admin.controller.AppConfigController;
import com.sap.isa.core.xcm.admin.controller.ScenarioConfigController;
import com.sap.isa.core.xcm.admin.eventhandler.ButtonOnClickEventHandler;
import com.sapportals.htmlb.event.ButtonClickEvent;
import com.sapportals.htmlb.rendering.IPageContext;
import com.sapportals.htmlb.type.AbstractDataType;

/**
 *
 */
public class ScenarioConfigCreateEventHandler
	extends ButtonOnClickEventHandler {

	/**
	 * @see com.sap.isa.core.xcm.admin.eventhandler.ButtonOnClickEventHandler#perform(ActionMapping, ButtonOnClickEventHandler, IPageContext, HttpSession, HttpServletRequest, HttpServletResponse)
	 */
	public ActionForward perform(
		ActionMapping mapping,
		ButtonClickEvent event,
		IPageContext pageContext,
		HttpSession session,
		HttpServletRequest request,
		HttpServletResponse response) {

			// check again if the user has XCM Admin rights???
			if(!AdminConfig.isXCMAdmin(request)) {
				return mapping.findForward(ActionForwardConstants.FAILURE);
			}
			
			UserSessionData userData = UserSessionData.getUserSessionData(session);

			AppConfigController appConfig = (AppConfigController)userData.getAttribute(SessionConstants.APP_CONFIG_CONTROLLER);
			ScenarioConfigController scenConfig = appConfig.getScenarioConfigController();

			// get new configuration name
			AbstractDataType adt = pageContext.getDataForComponentId(ScenarioConfigForm.SCEN_NAME_INPUT_FIELD_ID);
			if (adt == null) {
				// do not switch to config configuration
				return mapping.findForward(ActionForwardConstants.SUCCESS);
			}
			
			String scenConfigName = adt.toString();
			if (scenConfigName.length() == 0) {
				// do not switch to config configuration
				return mapping.findForward(ActionForwardConstants.SUCCESS);
				
			}
			
			if (log.isDebugEnabled())
				log.debug("Creating new scenario [name]='" + scenConfigName + "'");
			
			
			if (appConfig.isScenarioNameAvilable(scenConfigName)) {
				appConfig.isScenarioNameAvilable(true);
				return mapping.findForward(ActionForwardConstants.SUCCESS);
			}
			
			// check if scenario already available
			if (scenConfig.isScenarioAvailbale(scenConfigName)) {
				scenConfig.resetController();
				scenConfig.updateConfigController(scenConfigName);
				// indicated which node in the tree is active
				appConfig.getConfigTreeModel().setScenarioActiveNode(scenConfigName, scenConfig.isSAP());
				scenConfig.isSAP(scenConfig.isSAP());
			} else {
				// create new scenario
				//scenConfig.resetController();
				scenConfig.createScenarioConfiguration(scenConfigName);
				appConfig.addGlobalScenarioName(scenConfigName);
				appConfig.getConfigTreeModel().setScenariosCustomerActiveTreeNode();
				scenConfig.isSAP(false);

			}
			
			if (scenConfig.isSAP()) {
				scenConfig.isScenarioConfigTrayCollapsed(true);
				scenConfig.isScenarioDescrTrayCollapsed(false);
			} else {
				scenConfig.isScenarioConfigTrayCollapsed(false);
				scenConfig.isScenarioDescrTrayCollapsed(true);
			}	
			

			return mapping.findForward(ActionForwardConstants.SUCCESS);
	}

}
