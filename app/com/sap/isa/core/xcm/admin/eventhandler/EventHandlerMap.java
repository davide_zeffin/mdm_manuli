package com.sap.isa.core.xcm.admin.eventhandler;

// isa imports
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import com.sap.isa.core.logging.IsaLocation;

/**
 * Used to register event handlers for the XCM administration
 */
public class EventHandlerMap {

	protected static IsaLocation log = IsaLocation.getInstance(EventHandlerMap.class.getName());	

	protected Map eventMap = new LinkedHashMap();

	private static final EventHandlerMap evMap = new EventHandlerMap();

	/**
	 * Private constructor
	 */
	private EventHandlerMap() {
	}

	/**
	 * Returns a single instance of this event hanlder map
	 * @return a single instance of this event hanlder map
	 */
	public static EventHandlerMap getEventHandlerMap() {
		return evMap;
	}

	/**
	 * Returns an event handler or <code>null<code> if no event
	 * handler for the given parameter can be found
	 * @param formClass class of struts form 
	 * @param event id identifying event
	 * @return the event handler or <code>null</code>
	 */
	public EventHandler getEventHandler(Class formClass, String event) {
		EventHandler retval = null;
		Map classHandlers = (Map) eventMap.get(formClass);
		if (classHandlers != null) {
			retval = (EventHandler) classHandlers.get(event);
		}
		return retval;
	}

	/**
	 * Adds an event handler to this map
	 * @param formClass class of form associated with the event
	 * @param event id of event
	 * @param handler handler instance
	 */
	public void addEventHandler(
		Class formClass,
		String event,
		EventHandler handler) {
		Map classHandlers = (Map) eventMap.get(formClass);
		if (classHandlers == null) {
			classHandlers = new LinkedHashMap();
			eventMap.put(formClass, classHandlers);
		}
		classHandlers.put(event, handler);
	}

	/**
	 * Removes event from map
	 * @param formClass class of form associated with the event
	 * @param event id of event
	 */
	public void removeEventHandler(Class formClass, String event) {
		Map classHandlers = (Map) eventMap.get(formClass);
		if (classHandlers != null) {
			classHandlers.remove(event);
		}
	}

	/**
	 * Checks if event is part of the map
	 * @param formClass class of form associated with the event
	 * @param possibleMapMember class of the event handler
	 * @return <code>true</code> if event handler is associated with the 
	 *          given form, otherwise <code>false</code>
	 */
	public boolean isMemberOfMap(Class formClass, Class possibleMapMember) {
		boolean retval = false;
		Map classHandlers = (Map) eventMap.get(formClass);
		if (classHandlers != null) {
			for (Iterator iter = classHandlers.entrySet().iterator();iter.hasNext();) {
					EventHandler evH = (EventHandler) iter.next();
					if (evH.getClass() == possibleMapMember) {
						retval = true;
						break;
					}
			}
		}
		return retval;
	}

}