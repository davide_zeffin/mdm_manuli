package com.sap.isa.core.xcm.admin.eventhandler.tray;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.xcm.admin.ActionForwardConstants;
import com.sap.isa.core.xcm.admin.SessionConstants;
import com.sap.isa.core.xcm.admin.actionform.ScenarioConfigForm;
import com.sap.isa.core.xcm.admin.controller.AppConfigController;
import com.sap.isa.core.xcm.admin.controller.ScenarioConfigController;
import com.sap.isa.core.xcm.admin.eventhandler.EventHandler;
import com.sapportals.htmlb.event.Event;
import com.sapportals.htmlb.event.TrayHeaderClickEvent;
import com.sapportals.htmlb.rendering.IPageContext;

public class ScenarioConfigTrayEventHandler implements EventHandler {

	public ActionForward perform(
		ActionMapping mapping,
		Event event,
		IPageContext pageContext,
		HttpSession session,
		HttpServletRequest request,
		HttpServletResponse response) {

			UserSessionData userData = UserSessionData.getUserSessionData(session);

			AppConfigController appConfig = (AppConfigController)userData.getAttribute(SessionConstants.APP_CONFIG_CONTROLLER);
			ScenarioConfigController scenConfig = appConfig.getScenarioConfigController();
			String action = null;		
		if (event instanceof TrayHeaderClickEvent) {

			TrayHeaderClickEvent trayEvent = (TrayHeaderClickEvent)event; 
			
			action = trayEvent.getAction();
 				
			if (action.equals(ScenarioConfigForm.ON_SCENARIO_CONFIG_TRAY_CLOSE_EVENT)) {
				scenConfig.isScenarioConfigTrayCollapsed(true);
			}
			if (action.equals(ScenarioConfigForm.ON_SCENARIO_CONFIG_TRAY_OPEN_EVENT)) {
				scenConfig.isScenarioConfigTrayCollapsed(false);
			}

			if (action.equals(ScenarioConfigForm.ON_SCENARIO_DESCR_TRAY_CLOSE_EVENT)) {
				scenConfig.isScenarioDescrTrayCollapsed(true);
			}

			if (action.equals(ScenarioConfigForm.ON_SCENARIO_DESCR_TRAY_OPEN_EVENT)) {
				scenConfig.isScenarioDescrTrayCollapsed(false);
			}
		}
		
	

		
		return mapping.findForward(ActionForwardConstants.SUCCESS);
	}

}
