/*
 * Created on Nov 17, 2004
 */
package com.sap.isa.core.xcm.admin.eventhandler.button;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.xcm.admin.ActionForwardConstants;
import com.sap.isa.core.xcm.admin.Constants;
import com.sap.isa.core.xcm.admin.SessionConstants;
import com.sap.isa.core.xcm.admin.controller.AppConfigController;
import com.sap.isa.core.xcm.admin.controller.ComponentConfigController;
import com.sap.isa.core.xcm.admin.controller.ScenarioConfigController;
import com.sap.isa.core.xcm.admin.eventhandler.ButtonOnClickEventHandler;
import com.sap.isa.core.xcm.admin.helper.DefaultResourceAccessor;
import com.sap.isa.core.xcm.admin.helper.Message;
import com.sap.isa.core.xcm.admin.helper.MessageAccessor;
import com.sap.isa.core.xcm.admin.model.XCMAdminException;
import com.sapportals.htmlb.event.ButtonClickEvent;
import com.sapportals.htmlb.rendering.IPageContext;

/**
  * <p> 
 * This event handler is responsible for refreshing the
 * configurations from datastore. This operation insures
 * latest data is used for editing
 * This button is enabled when the User switched to Edit Mode
 * </p>
 *  
 * @author I802891
 */
public class RefreshEventHandler extends ButtonOnClickEventHandler {
	
	static {
		DefaultResourceAccessor accessor = DefaultResourceAccessor.getInstance();
		accessor.setMessageText(MyMessages.REFRESH_SUCCESS,"Configuration data refreshed");
		accessor.setMessageText(MyMessages.REFRESH_FAILURE,"Configuration data refresh failed");
	}
	
	public RefreshEventHandler() {
		log = IsaLocation.getInstance(RefreshEventHandler.class.getName());
	}
	
	/**
	 * @see com.sap.isa.core.xcm.admin.eventhandler.ButtonOnClickEventHandler#perform(ActionMapping, ButtonOnClickEventHandler, IPageContext, HttpSession, HttpServletRequest, HttpServletResponse)
	 */
	public ActionForward perform(
		ActionMapping mapping,
		ButtonClickEvent event,
		IPageContext pageContext,
		HttpSession session,
		HttpServletRequest request,
		HttpServletResponse response) {
  

		UserSessionData userData = UserSessionData.getUserSessionData(session);
		AppConfigController appConfig = (AppConfigController)userData.getAttribute(SessionConstants.APP_CONFIG_CONTROLLER);
		ComponentConfigController compConfig = appConfig.getCompConfigController();
		ScenarioConfigController scenarioConfig = appConfig.getScenarioConfigController();

		String compName = event.getComponentName();
		
		ArrayList msgs = new ArrayList();
		
		boolean success = true;
		// refresh configuration data models
		try {
			compConfig.getDataManipulator().refreshConfiguration();
			scenarioConfig.getDataManipulator().refreshConfiguration();
		}
		catch(XCMAdminException ex) {
			Message msg = MessageAccessor.getMessageAccessor().getMessage(MyMessages.REFRESH_FAILURE);
			msg.setSeverity(Message.ERROR);
			msgs.add(msg);
			log.error(LogUtil.APPS_USER_INTERFACE,msg.getMessageText(),ex);
			success = false;
		}
		
		if(success) {
			Message msg = MessageAccessor.getMessageAccessor().getMessage(MyMessages.REFRESH_SUCCESS);
			msgs.add(msg);
		}
		
		request.setAttribute(Constants.MESSAGES,msgs);
					
		// invalidate the controller
		appConfig.invalidate();
		
		return mapping.findForward(ActionForwardConstants.SUCCESS);		
	}
	
	private interface MyMessages {
		public static final String ID = MyMessages.class.getName() + "_";
		public static final String REFRESH_SUCCESS = ID + 1;
		public static final String REFRESH_FAILURE = ID + 2;
	}
}
