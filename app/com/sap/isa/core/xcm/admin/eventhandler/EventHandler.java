package com.sap.isa.core.xcm.admin.eventhandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sapportals.htmlb.event.Event;
import com.sapportals.htmlb.rendering.IPageContext;



/**
 * Interface for all classes acting as an htmlb event handler
 */
public interface EventHandler {
	
  public ActionForward perform(ActionMapping mapping,
                               Event event,
                               IPageContext pageContext,
                               HttpSession session,
                               HttpServletRequest request,
                               HttpServletResponse response);	
}
