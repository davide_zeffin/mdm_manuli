package com.sap.isa.core.xcm.admin.eventhandler.button;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.xcm.admin.ActionForwardConstants;
//import com.sap.isa.core.xcm.admin.Constants;
import com.sap.isa.core.xcm.admin.SessionConstants;
//import com.sap.isa.core.xcm.admin.actionform.ScenarioConfigForm;
import com.sap.isa.core.xcm.admin.controller.AppConfigController;
//import com.sap.isa.core.xcm.admin.controller.ComponentConfigController;
//import com.sap.isa.core.xcm.admin.controller.ControllerUtils;
import com.sap.isa.core.xcm.admin.controller.ScenarioConfigController;
import com.sap.isa.core.xcm.admin.eventhandler.ButtonOnClickEventHandler;
//import com.sap.isa.core.xcm.admin.model.table.ScenarioDetailTableModel;
import com.sapportals.htmlb.event.ButtonClickEvent;
import com.sapportals.htmlb.rendering.IPageContext;
//import com.sapportals.htmlb.table.TableView;
//import com.sapportals.htmlb.type.AbstractDataType;

/**
 *
 */
public class SelectAdvancedScenarioEventHandler
	extends ButtonOnClickEventHandler {

	/**
	 * @see com.sap.isa.core.xcm.admin.eventhandler.ButtonOnClickEventHandler#perform(ActionMapping, ButtonOnClickEventHandler, IPageContext, HttpSession, HttpServletRequest, HttpServletResponse)
	 */
	public ActionForward perform(
		ActionMapping mapping,
		ButtonClickEvent event,
		IPageContext pageContext,
		HttpSession session,
		HttpServletRequest request,
		HttpServletResponse response) {
  

			UserSessionData userData = UserSessionData.getUserSessionData(session);
			AppConfigController appConfig = (AppConfigController)userData.getAttribute(SessionConstants.APP_CONFIG_CONTROLLER);

			ScenarioConfigController scenConfig = appConfig.getScenarioConfigController();

			scenConfig.isAdvancedOptionsOn(!scenConfig.isAdvancedOptionsOn());
 			
			scenConfig.updateConfigController(scenConfig.getCurrentScenarioName());
			return mapping.findForward(ActionForwardConstants.SUCCESS);
	}

}
