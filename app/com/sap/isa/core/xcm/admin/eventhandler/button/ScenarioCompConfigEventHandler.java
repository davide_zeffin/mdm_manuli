package com.sap.isa.core.xcm.admin.eventhandler.button;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.xcm.admin.ActionForwardConstants;
import com.sap.isa.core.xcm.admin.Constants;
import com.sap.isa.core.xcm.admin.SessionConstants;
import com.sap.isa.core.xcm.admin.actionform.ScenarioConfigForm;
import com.sap.isa.core.xcm.admin.controller.AppConfigController;
import com.sap.isa.core.xcm.admin.controller.ComponentConfigController;
import com.sap.isa.core.xcm.admin.controller.ControllerUtils;
import com.sap.isa.core.xcm.admin.controller.ScenarioConfigController;
import com.sap.isa.core.xcm.admin.eventhandler.ButtonOnClickEventHandler;
import com.sap.isa.core.xcm.admin.model.table.ScenarioDetailTableModel;
import com.sapportals.htmlb.event.ButtonClickEvent;
import com.sapportals.htmlb.rendering.IPageContext;
import com.sapportals.htmlb.table.TableView;
import com.sapportals.htmlb.type.AbstractDataType;

/**
 *
 */
public class ScenarioCompConfigEventHandler
	extends ButtonOnClickEventHandler {

	/**
	 * @see com.sap.isa.core.xcm.admin.eventhandler.ButtonOnClickEventHandler#perform(ActionMapping, ButtonOnClickEventHandler, IPageContext, HttpSession, HttpServletRequest, HttpServletResponse)
	 */
	public ActionForward perform(
		ActionMapping mapping,
		ButtonClickEvent event,
		IPageContext pageContext,
		HttpSession session,
		HttpServletRequest request,
		HttpServletResponse response) {
  

        UserSessionData userData = UserSessionData.getUserSessionData(session);
		AppConfigController appConfig = (AppConfigController)userData.getAttribute(SessionConstants.APP_CONFIG_CONTROLLER);
		ComponentConfigController compConfig = appConfig.getCompConfigController();
		ScenarioConfigController scenConfig = appConfig.getScenarioConfigController();

		ControllerUtils.saveCurrentScenarioParams(pageContext, scenConfig);

		// string comp name equals row number
		String rowString = event.getComponentName(); 

		TableView tableView = (TableView)pageContext.getComponentForId(ScenarioConfigForm.SCENARIO_TABLE_VIEW_ID);
		int visibleFirstRow = tableView.getVisibleFirstRow();
		int visibleRowCount = tableView.getVisibleRowCount();
		int row = Integer.parseInt(rowString);
		int visibleRow = row % visibleRowCount;
		if (visibleRow == 0)
			visibleRow = visibleRowCount; 
	
		AbstractDataType value = pageContext.getDataForComponentId(ScenarioConfigForm.SCENARIO_TABLE_VIEW_ID, ScenarioConfigForm.PARAM_VALUE_LIST_BOX_ID, visibleRow);
		if (value.toString() != null) {
			ScenarioDetailTableModel model = scenConfig.getCompDetailTableModel();
			AbstractDataType adt = model.getValueAt(row, 1);
			if (adt.toString() == null) 
				return mapping.findForward(ActionForwardConstants.SUCCESS);
				
			String paramName = adt.toString();	
			String  paramValue = value.toString();	
			 	
			adt = model.getValueAt(row, 4);
	
			if (adt.toString() == null) 
				return mapping.findForward(ActionForwardConstants.SUCCESS);
		
			String compName = adt.toString();
			
			// a new configuration is created
			if (paramValue.equals(Constants.CREATE_NEW_CONFIG_ID))
				paramValue = "";
			appConfig.configCompFromScenario(compName, paramValue);
		}



			

		return mapping.findForward(ActionForwardConstants.SUCCESS);
	}

}
