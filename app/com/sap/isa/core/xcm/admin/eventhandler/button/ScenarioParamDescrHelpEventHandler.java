package com.sap.isa.core.xcm.admin.eventhandler.button;
 
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.xcm.admin.ActionForwardConstants;
import com.sap.isa.core.xcm.admin.Constants;
import com.sap.isa.core.xcm.admin.SessionConstants;
import com.sap.isa.core.xcm.admin.actionform.ScenarioConfigForm;
import com.sap.isa.core.xcm.admin.controller.AppConfigController;
import com.sap.isa.core.xcm.admin.controller.ComponentConfigController;
import com.sap.isa.core.xcm.admin.controller.ControllerUtils;
import com.sap.isa.core.xcm.admin.controller.ScenarioConfigController;
import com.sap.isa.core.xcm.admin.eventhandler.ButtonOnClickEventHandler;
import com.sap.isa.core.xcm.admin.model.table.ScenarioDetailTableModel;
import com.sapportals.htmlb.event.ButtonClickEvent;
import com.sapportals.htmlb.rendering.IPageContext;
import com.sapportals.htmlb.table.TableView;
import com.sapportals.htmlb.type.AbstractDataType;

/**
 *
 */
public class ScenarioParamDescrHelpEventHandler
	extends ButtonOnClickEventHandler {

	/**
	 * @see com.sap.isa.core.xcm.admin.eventhandler.ButtonOnClickEventHandler#perform(ActionMapping, ButtonOnClickEventHandler, IPageContext, HttpSession, HttpServletRequest, HttpServletResponse)
	 */
	public ActionForward perform(
		ActionMapping mapping,
		ButtonClickEvent event,
		IPageContext pageContext,
		HttpSession session,
		HttpServletRequest request,
		HttpServletResponse response) {
  

        UserSessionData userData = UserSessionData.getUserSessionData(session);
		AppConfigController appConfig = (AppConfigController)userData.getAttribute(SessionConstants.APP_CONFIG_CONTROLLER);
		ComponentConfigController compConfig = appConfig.getCompConfigController();
		ScenarioConfigController scenConfig = appConfig.getScenarioConfigController();

		// string comp name equals row number
		String compName = event.getComponentName(); 
		
		TableView tableView = (TableView)pageContext.getComponentForId(ScenarioConfigForm.SCENARIO_TABLE_VIEW_ID);


		int firstVisRow = tableView.getVisibleFirstRow();
		int visibleRowCount = tableView.getVisibleRowCount();

		int lastRowModel = visibleRowCount + firstVisRow - 1;

		ScenarioDetailTableModel model = scenConfig.getCompDetailTableModel();		

		ControllerUtils.saveCurrentScenarioParams(pageContext, scenConfig);

		int row = Integer.parseInt(compName);
		
		int visibleRow = row % visibleRowCount;		
	
		if (visibleRow == 0)
			visibleRow = row; 
						
		AbstractDataType value = pageContext.getDataForComponentId(ScenarioConfigForm.SCENARIO_TABLE_VIEW_ID, ScenarioConfigForm.PARAM_VALUE_LIST_BOX_ID, visibleRow);
		if (value.toString() != null) {
			AbstractDataType adt = model.getValueAt(row, 1);
			if (adt.toString() != null) {
				String paramName = adt.toString();	

				String descr = Constants.MSG_SHORTTEXT_MISSING;
 
				String shortText = scenConfig.getConfigParamShorttext(paramName);
				String longText = scenConfig.getConfigParamLongtext(paramName);

				if (shortText == null)
					shortText = Constants.MSG_SHORTTEXT_MISSING;
					 
				if (longText == null)
					longText = "";

				scenConfig.isScenarioParamDescrHelp(true);
//				model.setValueAt(value, row, 1);
//				scenConfig.saveTableModel();
//				scenConfig.updateConfigController(scenConfig.getCurrentScenarioName());
				userData.setAttribute(SessionConstants.SCEN_CONFIG_PARAM_VALUE_HELP_SHORTTEXT, shortText);
				userData.setAttribute(SessionConstants.SCEN_CONFIG_PARAM_VALUE_HELP_LONGTEXT, longText);
				userData.setAttribute(SessionConstants.SCEN_CONFIG_PARAM_VALUE_HELP, "application configuration parameter description");
				
			}
		}
			

		return mapping.findForward(ActionForwardConstants.SUCCESS);
	}

}

