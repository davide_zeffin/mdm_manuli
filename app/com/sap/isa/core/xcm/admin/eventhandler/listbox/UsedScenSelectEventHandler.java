package com.sap.isa.core.xcm.admin.eventhandler.listbox;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.xcm.admin.ActionForwardConstants;
import com.sap.isa.core.xcm.admin.SessionConstants;
import com.sap.isa.core.xcm.admin.controller.MonitoringController;
import com.sap.isa.core.xcm.admin.eventhandler.ListSelectEventEventHandler;
import com.sapportals.htmlb.DropdownListBox;
import com.sapportals.htmlb.event.ListSelectEvent;
import com.sapportals.htmlb.rendering.IPageContext;

/**
 *
 */
public class UsedScenSelectEventHandler extends ListSelectEventEventHandler {

	/**
	 * @see com.sap.isa.core.xcm.admin.eventhandler.ListSelectEventEventHandler#perform(ActionMapping, ListSelectEvent, IPageContext, HttpSession, HttpServletRequest, HttpServletResponse)
	 */
	public ActionForward perform(
		ActionMapping mapping,
		ListSelectEvent event,
		IPageContext pageContext,
		HttpSession session,
		HttpServletRequest request, 
		HttpServletResponse response) {

        UserSessionData userData = UserSessionData.getUserSessionData(session);
		MonitoringController monController = (MonitoringController)userData.getAttribute(SessionConstants.MONITORING_CONTROLLER);
		DropdownListBox listBox = (DropdownListBox)pageContext.getComponentForId("UsedScenario");
		String selectedEntry = listBox.getSelection();
		
		monController.setTableViews(selectedEntry);
		monController.setListBoxPreSelection(selectedEntry);
		
		return mapping.findForward(ActionForwardConstants.SUCCESS);
	}

}
