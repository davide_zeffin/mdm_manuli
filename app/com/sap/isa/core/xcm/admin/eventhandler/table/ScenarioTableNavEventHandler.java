package com.sap.isa.core.xcm.admin.eventhandler.table;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.xcm.admin.ActionForwardConstants;
import com.sap.isa.core.xcm.admin.SessionConstants;
import com.sap.isa.core.xcm.admin.actionform.ScenarioConfigForm;
import com.sap.isa.core.xcm.admin.controller.AppConfigController;
import com.sap.isa.core.xcm.admin.controller.ScenarioConfigController;
import com.sap.isa.core.xcm.admin.eventhandler.TableOnNavigateEvent;
import com.sap.isa.core.xcm.admin.model.table.ScenarioDetailTableModel;
import com.sapportals.htmlb.event.TableNavigationEvent;
import com.sapportals.htmlb.rendering.IPageContext;
import com.sapportals.htmlb.type.AbstractDataType;

/**
 *
 */
public class ScenarioTableNavEventHandler extends TableOnNavigateEvent {

	/**
	 * @see com.sap.isa.core.xcm.admin.eventhandler.TableOnNavigateEvent#perform(ActionMapping, TableNavigationEvent, IPageContext, HttpSession, HttpServletRequest, HttpServletResponse)
	 */
	public ActionForward perform(
		ActionMapping mapping,
		TableNavigationEvent event,
		IPageContext pageContext,
		HttpSession session,
		HttpServletRequest request,
		HttpServletResponse response) {
			
        UserSessionData userData = UserSessionData.getUserSessionData(session);

		AppConfigController appConfig = (AppConfigController)userData.getAttribute(SessionConstants.APP_CONFIG_CONTROLLER);
		ScenarioConfigController scenConfig = appConfig.getScenarioConfigController();
		
		int firstVisRow = event.getFirstVisibleRow();
		int visibleRowCount = scenConfig.getScenarioDetailTableView().getVisibleRowCount();

		int lastRowModel = visibleRowCount + firstVisRow - 1;
		
		ScenarioDetailTableModel model = scenConfig.getScenarioDetailTableModel();

		if (log.isDebugEnabled())
			log.debug("[last row in model]='" + model.getRowCount());

		try {

			if (lastRowModel > model.getRowCount())
				lastRowModel = model.getRowCount();
	
			int loopcount = 0;
			for (int i = firstVisRow; i <= lastRowModel; i++) {
				loopcount++;
				AbstractDataType value = pageContext.getDataForComponentId(ScenarioConfigForm.SCENARIO_TABLE_VIEW_ID, ScenarioConfigForm.PARAM_VALUE_LIST_BOX_ID, loopcount);
				model.setValueAt(value, i, 2);

			}
		} catch (Throwable t) {
			log.error(LogUtil.APPS_USER_INTERFACE,"system.xcm.admin.exception", new Object[] {}, t);
		}
	
		if (event.getNavigationType().equals(TableNavigationEvent.EVENT_PAGEDOWN))
			firstVisRow += visibleRowCount;

		if (event.getNavigationType().equals(TableNavigationEvent.EVENT_PAGEUP))
			firstVisRow -= visibleRowCount;

		if (event.getNavigationType().equals(TableNavigationEvent.EVENT_BOTTOM)) {
			int numRows = scenConfig.getCompDetailTableModel().getRowCount();
			int deltas = numRows / visibleRowCount;
			if (visibleRowCount * deltas == numRows)
				firstVisRow = visibleRowCount * (deltas - 1) + 1;
			else
				firstVisRow = visibleRowCount * deltas + 1;
			
		}
 
		if (event.getNavigationType().equals(TableNavigationEvent.EVENT_TOP))
			firstVisRow = 1;

		scenConfig.getScenarioDetailTableView().setVisibleFirstRow(firstVisRow);

	
		return mapping.findForward(ActionForwardConstants.SUCCESS);
	}

}
