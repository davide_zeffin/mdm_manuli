package com.sap.isa.core.xcm.admin.eventhandler.button;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.config.AdminConfig;
import com.sap.isa.core.xcm.admin.ActionForwardConstants;
import com.sap.isa.core.xcm.admin.Constants;
import com.sap.isa.core.xcm.admin.SessionConstants;
import com.sap.isa.core.xcm.admin.controller.AppConfigController;
import com.sap.isa.core.xcm.admin.controller.HelpController;
import com.sap.isa.core.xcm.admin.controller.ScenarioConfigController;
import com.sap.isa.core.xcm.admin.eventhandler.ButtonOnClickEventHandler;
import com.sap.isa.core.xcm.admin.helper.DefaultResourceAccessor;
import com.sap.isa.core.xcm.admin.helper.MessageAccessor;
import com.sapportals.htmlb.event.ButtonClickEvent;
import com.sapportals.htmlb.rendering.IPageContext;

/**
 *
 */
public class ScenarioConfigDeleteEventHandler
	extends ButtonOnClickEventHandler {

	static {
		DefaultResourceAccessor accessor = DefaultResourceAccessor.getInstance();
		accessor.setMessageText(MyMessages.SAVE_FAILURE,"Failed to delete application configuration data. Configuration changes will not be available after restarting the application");
		accessor.setMessageText(MyMessages.SAVE_FAILURE_RECOMMENDATION,"It is recommeded to cancel the Edit operation");		
		accessor.setMessageText(MyMessages.SAVE_SUCCESS,"Application configuration data deleted successfully. Configuration changes will be available after restarting the application");
	}
		
	/**
	 * @see com.sap.isa.core.xcm.admin.eventhandler.ButtonOnClickEventHandler#perform(ActionMapping, ButtonOnClickEventHandler, IPageContext, HttpSession, HttpServletRequest, HttpServletResponse)
	 */
	public ActionForward perform(
		ActionMapping mapping,
		ButtonClickEvent event,
		IPageContext pageContext,
		HttpSession session,
		HttpServletRequest request,
		HttpServletResponse response) {

		// check again if the user has XCM Admin rights???
		if(!AdminConfig.isXCMAdmin(request)) {
			return mapping.findForward(ActionForwardConstants.FAILURE);
		}
		
        UserSessionData userData = UserSessionData.getUserSessionData(session);
		AppConfigController appConfig = (AppConfigController)userData.getAttribute(SessionConstants.APP_CONFIG_CONTROLLER);
		
		ScenarioConfigController scenConfig = appConfig.getScenarioConfigController();
		HelpController helpController = appConfig.getHelpController(); 
		
		ArrayList msgs = new ArrayList();
		
		appConfig.removeGlobalScenarioName(scenConfig.getCurrentScenarioName());
		scenConfig.deleteCurrentScenarioConfig();
		
		appConfig.getConfigTreeModel().updateScenNodes();
		
		helpController.updateController();
		
		scenConfig.updateConfigController(null);

		appConfig.setConfigType(AppConfigController.SCENARIO_OVERVIEW_MODE);
		
		String retval = scenConfig.saveConfiguration();
		
		if(retval.equals(Constants.OK)) {	
			msgs.add(MessageAccessor.getMessageAccessor().getMessage(MyMessages.SAVE_SUCCESS));			
		}
		else {
			msgs.add(MessageAccessor.getMessageAccessor().getMessage(MyMessages.SAVE_FAILURE));			
			msgs.add(MessageAccessor.getMessageAccessor().getMessage(MyMessages.SAVE_FAILURE_RECOMMENDATION));			
		}	
		
		request.setAttribute(Constants.SAVE_STATUS, retval);
		request.setAttribute(Constants.MESSAGES,msgs);		

		return mapping.findForward(ActionForwardConstants.SUCCESS);
	}
	
	private static interface MyMessages {
		public static final String ID = MyMessages.class.getName() + "_";
		
		public static final String SAVE_SUCCESS = ID + 1;
		public static final String SAVE_FAILURE = ID + 2;
		public static final String SAVE_FAILURE_RECOMMENDATION = ID + 3;		
	}
}
