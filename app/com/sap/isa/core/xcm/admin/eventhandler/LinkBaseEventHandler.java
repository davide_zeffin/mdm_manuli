package com.sap.isa.core.xcm.admin.eventhandler;

// servlet imports
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.logging.IsaLocation;
import com.sapportals.htmlb.event.Event;
import com.sapportals.htmlb.event.LinkClickEvent;
import com.sapportals.htmlb.rendering.IPageContext;


/**
 * Abstract base class for event handlers handling tab-select events 
 */
public abstract class LinkBaseEventHandler implements EventHandler {

	protected static IsaLocation log = null;

	public LinkBaseEventHandler() {
		if (log == null)
			log = IsaLocation.getInstance(this.getClass().getName());
	}


  public ActionForward perform(ActionMapping mapping,
                               Event event,
                               IPageContext pageContext,
                               HttpSession session,
                               HttpServletRequest request,
                               HttpServletResponse response)
  {
    return perform(mapping, (LinkClickEvent)event, pageContext, session, request, response);
  }

  /**
   * Has to be implemented by the extending class
   */
  public abstract ActionForward perform( ActionMapping mapping,
                                         LinkClickEvent event,
                                         IPageContext pageContext,
                                         HttpSession session,
                                         HttpServletRequest request,
                                         HttpServletResponse response);
}



