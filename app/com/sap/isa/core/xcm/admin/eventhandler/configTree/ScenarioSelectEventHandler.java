package com.sap.isa.core.xcm.admin.eventhandler.configTree;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.xcm.admin.ActionForwardConstants;
import com.sap.isa.core.xcm.admin.SessionConstants;
import com.sap.isa.core.xcm.admin.controller.AppConfigController;
import com.sap.isa.core.xcm.admin.controller.ComponentConfigController;
import com.sap.isa.core.xcm.admin.controller.ScenarioConfigController;
import com.sap.isa.core.xcm.admin.eventhandler.TreeNodeSelectEventHandler;
import com.sap.isa.core.xcm.admin.model.tree.ConfigTreeModel;
import com.sapportals.htmlb.TreeNode;
import com.sapportals.htmlb.event.TreeNodeSelectEvent;
import com.sapportals.htmlb.rendering.IPageContext;

/**
 *
 */
public class ScenarioSelectEventHandler extends TreeNodeSelectEventHandler {

	/**
	 * @see com.sap.isa.core.xcm.admin.eventhandler.EventHandler#perform(ActionMapping, Event, IPageContext, HttpSession, HttpServletRequest, HttpServletResponse)
	 */
	public ActionForward perform(
		ActionMapping mapping,
		TreeNodeSelectEvent event,
		IPageContext pageContext,
		HttpSession session,
		HttpServletRequest request,
		HttpServletResponse response) {

        UserSessionData userData = UserSessionData.getUserSessionData(session);

		AppConfigController appConfig = (AppConfigController)userData.getAttribute(SessionConstants.APP_CONFIG_CONTROLLER);
		ComponentConfigController compConfig = appConfig.getCompConfigController();
		ScenarioConfigController scenConfig = appConfig.getScenarioConfigController();
		
		if (scenConfig.isCurrentParamConfigNew()) {
			appConfig.removeGlobalScenarioName(scenConfig.getCurrentScenarioName());
			scenConfig.deleteCurrentScenarioConfig();
		}
	
		
		String nodeId = event.getNodeKey();
		TreeNode scenNode = appConfig.getConfigTreeModel().getNodeByID(nodeId);
		if (scenNode == null)
			return mapping.findForward(ActionForwardConstants.FAILURE);		
			
		// indicated which node in the tree is active
		appConfig.getConfigTreeModel().setActiveNode(scenNode);

		String scenId = scenNode.getID();
					
		appConfig.setConfigType(AppConfigController.SCENARIO_CONFIG_MODE);

		if (log.isDebugEnabled())
			log.debug("Scenario selected [id]='" + scenId + "'");
		
		String scenName = ConfigTreeModel.getNameTextView(scenNode.getComponent()).getText();
		
		// update scenarion controller with selected scenario
		scenConfig.resetController();
		scenConfig.updateConfigController(scenName);
		
		TreeNode scenTypeNode = scenNode.getParentNode();
		String scenTypeId = scenTypeNode.getID();
		
		if (scenTypeId.equals(ConfigTreeModel.SCENARIOS_SAP_NODE_ID))
			scenConfig.isSAP(true);

		if (scenTypeId.equals(ConfigTreeModel.SCENARIOS_CUST_NODE_ID))
			scenConfig.isSAP(false);

		if (scenConfig.isSAP()) {
			scenConfig.isScenarioConfigTrayCollapsed(true);
			scenConfig.isScenarioDescrTrayCollapsed(false);
		} else {
			scenConfig.isScenarioConfigTrayCollapsed(false);
			scenConfig.isScenarioDescrTrayCollapsed(true);
		}	

		appConfig.isConfigCompFromScenario(false);

		return mapping.findForward(ActionForwardConstants.SUCCESS);
	}

}
