package com.sap.isa.core.xcm.admin.eventhandler.configTree;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.xcm.admin.ActionForwardConstants;
import com.sap.isa.core.xcm.admin.SessionConstants;
import com.sap.isa.core.xcm.admin.controller.AppConfigController;
import com.sap.isa.core.xcm.admin.controller.ComponentConfigController;
import com.sap.isa.core.xcm.admin.eventhandler.TreeNodeSelectEventHandler;
import com.sap.isa.core.xcm.admin.model.tree.ConfigTreeModel;
import com.sapportals.htmlb.TreeNode;
import com.sapportals.htmlb.event.TreeNodeSelectEvent;
import com.sapportals.htmlb.rendering.IPageContext;

/**
 *
 */
public class CompSelectEventHandler extends TreeNodeSelectEventHandler {

	/**
	 * @see com.sap.isa.core.xcm.admin.eventhandler.EventHandler#perform(ActionMapping, Event, IPageContext, HttpSession, HttpServletRequest, HttpServletResponse)
	 */
	public ActionForward perform(
		ActionMapping mapping,
		TreeNodeSelectEvent event,
		IPageContext pageContext,
		HttpSession session,
		HttpServletRequest request,
		HttpServletResponse response) {

        UserSessionData userData = UserSessionData.getUserSessionData(session);

		AppConfigController appConfig = (AppConfigController)userData.getAttribute(SessionConstants.APP_CONFIG_CONTROLLER);
		ComponentConfigController compConfig = appConfig.getCompConfigController();

		// if current configuration new delete it => user moves away from new configuration
		if (compConfig.isCurrentParamConfigNew())
			compConfig.deleteCurrentCompParamConfig();

		String nodeId = event.getNodeKey();
		TreeNode compNode = appConfig.getConfigTreeModel().getNodeByID(nodeId);
		if (compNode == null)
			return mapping.findForward(ActionForwardConstants.FAILURE);		

		appConfig.getConfigTreeModel().setActiveNode(compNode);

		String compId = compNode.getID();
					
		appConfig.setConfigType(AppConfigController.COMPONENT_DESCR_MODE);

		if (log.isDebugEnabled())
			log.debug("Component selected [id]='" + compId + "'");
		
		//String compName = ((TextView)compNode.getComponent()).getText();
		
		String compName = compNode.getText();
		compConfig.resetController();
		// set the name of the component
		compConfig.setCurrentCompId(compName);
		// clrear config table
		compConfig.updateConfigController(compName);
		compConfig.setCurrentBaseParamConfigId(null);	
		
		TreeNode compTypeNode = compNode.getParentNode();
		String compTypeId = compTypeNode.getID();
		
		if (compTypeId.equals(ConfigTreeModel.COMP_SAP_NODE_ID))
			compConfig.isSAP(true);

		if (compTypeId.equals(ConfigTreeModel.COMP_CUST_NODE_ID))
			compConfig.isSAP(false);
			
		appConfig.isConfigCompFromScenario(false);

		return mapping.findForward(ActionForwardConstants.SUCCESS);
	}

}
