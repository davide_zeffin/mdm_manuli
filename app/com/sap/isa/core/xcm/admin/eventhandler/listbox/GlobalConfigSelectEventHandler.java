package com.sap.isa.core.xcm.admin.eventhandler.listbox;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.xcm.admin.Constants;
import com.sap.isa.core.xcm.admin.SessionConstants;
import com.sap.isa.core.xcm.admin.actionform.HomeForm;
import com.sap.isa.core.xcm.admin.controller.AppConfigController;
import com.sap.isa.core.xcm.admin.controller.ComponentConfigController;
import com.sap.isa.core.xcm.admin.eventhandler.ListSelectEventEventHandler;
import com.sapportals.htmlb.DropdownListBox;
import com.sapportals.htmlb.event.ListSelectEvent;
import com.sapportals.htmlb.rendering.IPageContext;

/**
 *
 */
public class GlobalConfigSelectEventHandler extends ListSelectEventEventHandler {

	/**
	 * @see com.sap.isa.core.xcm.admin.eventhandler.ListSelectEventEventHandler#perform(ActionMapping, ListSelectEvent, IPageContext, HttpSession, HttpServletRequest, HttpServletResponse)
	 */
	public ActionForward perform(
		ActionMapping mapping,
		ListSelectEvent event,
		IPageContext pageContext,
		HttpSession session,
		HttpServletRequest request, 
		HttpServletResponse response) {

        UserSessionData userData = UserSessionData.getUserSessionData(session);

		AppConfigController appConfig = (AppConfigController)userData.getAttribute(SessionConstants.APP_CONFIG_CONTROLLER);
		ComponentConfigController compConfig = appConfig.getCompConfigController();

		
		DropdownListBox listBox = (DropdownListBox)pageContext.getComponentForId(HomeForm.ON_GLOBAL_CONFIGURATION_LIST_BOX_ID);
		
		String selectedEntry = listBox.getSelection();
		
		ActionForward aw = new ActionForward("success");
		String path = "/admin/xcm/init.do?" + Constants.SELECT_GLOBAL_CONFIG_REQUEST_PARAM + "=" + selectedEntry 
			+ "&" + Constants.KEEP_SESSION_REQUEST_PARAM + "=true";
		aw.setPath(path);

		return aw;
	}

}
