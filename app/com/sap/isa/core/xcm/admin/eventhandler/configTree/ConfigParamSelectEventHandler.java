package com.sap.isa.core.xcm.admin.eventhandler.configTree;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.xcm.admin.ActionForwardConstants;
import com.sap.isa.core.xcm.admin.SessionConstants;
import com.sap.isa.core.xcm.admin.controller.AppConfigController;
import com.sap.isa.core.xcm.admin.controller.ComponentConfigController;
import com.sap.isa.core.xcm.admin.eventhandler.TreeNodeSelectEventHandler;
import com.sap.isa.core.xcm.admin.model.tree.ConfigTreeModel;
import com.sapportals.htmlb.TreeNode;
import com.sapportals.htmlb.event.TreeNodeSelectEvent;
import com.sapportals.htmlb.rendering.IPageContext;

/**
 *
 */
public class ConfigParamSelectEventHandler extends TreeNodeSelectEventHandler {

	/**
	 * @see com.sap.isa.core.xcm.admin.eventhandler.EventHandler#perform(ActionMapping, Event, IPageContext, HttpSession, HttpServletRequest, HttpServletResponse)
	 */
	public ActionForward perform(
		ActionMapping mapping,
		TreeNodeSelectEvent event,
		IPageContext pageContext,
		HttpSession session,
		HttpServletRequest request,
		HttpServletResponse response) {

        UserSessionData userData = UserSessionData.getUserSessionData(session);

		AppConfigController appConfig = (AppConfigController)userData.getAttribute(SessionConstants.APP_CONFIG_CONTROLLER);
		ComponentConfigController compConfig = appConfig.getCompConfigController();

		// if current configuration new delete it => user moves away from new configuration
		if (compConfig.isCurrentParamConfigNew())
			compConfig.deleteCurrentCompParamConfig();

		
		appConfig.setConfigType(AppConfigController.COMPONENTS_CONFIG_MODE);
		
	
		String currentNodeId = event.getNodeKey();
		// get name of component
		TreeNode currentNode = appConfig.getConfigTreeModel().getNodeByID(currentNodeId);
		if (currentNode == null) 
			mapping.findForward(ActionForwardConstants.FAILURE);			

		appConfig.getConfigTreeModel().setActiveNode(currentNode);

		// get component name
		
		//String compName = currentNode.getParentNode().getText();
		String compName = ConfigTreeModel.getNameTextView(currentNode.getParentNode().getComponent()).getText();
		
		compConfig.setCurrentCompId(compName);
		// get param config id
		String paramConfigId = currentNode.getText();
		
		String compNodeId = currentNode.getParentNode().getParentNode().getID();
		
		if (compNodeId.equals(ConfigTreeModel.COMP_SAP_NODE_ID))
			compConfig.isSAP(true);

		if (compNodeId.equals(ConfigTreeModel.COMP_CUST_NODE_ID) ||
			compNodeId.equals(ConfigTreeModel.GENERAL_CUST_NODE_ID))
			compConfig.isSAP(false);

		if (log.isDebugEnabled())
			log.debug("Configuration selected: [id]='" + paramConfigId + "'");

		compConfig.updateConfigController(paramConfigId)	;
		
		appConfig.isConfigCompFromScenario(false);

		return mapping.findForward(ActionForwardConstants.SUCCESS);
	}

}

