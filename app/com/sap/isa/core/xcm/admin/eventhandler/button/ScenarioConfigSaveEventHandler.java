package com.sap.isa.core.xcm.admin.eventhandler.button;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.xcm.admin.ActionForwardConstants;
import com.sap.isa.core.xcm.admin.Constants;
import com.sap.isa.core.xcm.admin.SessionConstants;
import com.sap.isa.core.xcm.admin.actionform.ScenarioConfigForm;
import com.sap.isa.core.xcm.admin.controller.AppConfigController;
import com.sap.isa.core.xcm.admin.controller.HelpController;
import com.sap.isa.core.xcm.admin.controller.ScenarioConfigController;
import com.sap.isa.core.xcm.admin.eventhandler.ButtonOnClickEventHandler;
import com.sap.isa.core.xcm.admin.model.table.ScenarioDetailTableModel;
import com.sapportals.htmlb.Checkbox;
import com.sapportals.htmlb.event.ButtonClickEvent;
import com.sapportals.htmlb.rendering.IPageContext;
import com.sapportals.htmlb.table.TableView;
import com.sapportals.htmlb.type.AbstractDataType;
import java.util.Collection;
import java.util.ArrayList;
import com.sap.isa.core.xcm.admin.helper.MessageAccessor;
import com.sap.isa.core.xcm.admin.helper.DefaultResourceAccessor;
import com.sap.isa.core.config.AdminConfig;
import com.sap.isa.core.init.InitializationHandler;

/**
 *
 */
public class ScenarioConfigSaveEventHandler
	extends ButtonOnClickEventHandler {

	/**
	 * @see com.sap.isa.core.xcm.admin.eventhandler.ButtonOnClickEventHandler#perform(ActionMapping, ButtonOnClickEventHandler, IPageContext, HttpSession, HttpServletRequest, HttpServletResponse)
	 */
	private static org.apache.struts.util.MessageResources mMessageResources =
		InitializationHandler.getMessageResources();
		
	static {
		DefaultResourceAccessor accessor = DefaultResourceAccessor.getInstance();
		accessor.setMessageText(MyMessages.DEFAULT_NOT_DEACTIVATE,mMessageResources.getMessage("xcm.scenconfig.messages.deactiv"));
		accessor.setMessageText(MyMessages.DEFAULT_ACTIVATED,mMessageResources.getMessage("xcm.scenconfig.messages.def.activ"));
		accessor.setMessageText(MyMessages.SAVE_FAILURE,"Failed to save application configuration data. Configuration changes will not be available after restarting the application");
		accessor.setMessageText(MyMessages.SAVE_FAILURE_RECOMMENDATION,"It is recommeded to cancel the Edit operation");		
		accessor.setMessageText(MyMessages.SAVE_SUCCESS,"Application configuration data saved successfully. Configuration changes will be available after restarting the application");				
	}
	
	public ActionForward perform(
		ActionMapping mapping,
		ButtonClickEvent event,
		IPageContext pageContext,
		HttpSession session,
		HttpServletRequest request,
		HttpServletResponse response) {
			
		// check again if the user has XCM Admin rights???
		if(!AdminConfig.isXCMAdmin(request)) {
			return mapping.findForward(ActionForwardConstants.FAILURE);
		}
		
		UserSessionData userData = UserSessionData.getUserSessionData(session);
		AppConfigController appConfig = (AppConfigController)userData.getAttribute(SessionConstants.APP_CONFIG_CONTROLLER);
		
		  
		// get data from the visible rows and write them into the model 
		TableView tableView = (TableView)pageContext.getComponentForId(ScenarioConfigForm.SCENARIO_TABLE_VIEW_ID);
		int visibleFirstRow = tableView.getVisibleFirstRow();
		int visibleRowCount = tableView.getVisibleRowCount();

		ScenarioConfigController scenConfig = appConfig.getScenarioConfigController();
		ScenarioDetailTableModel model = scenConfig.getCompDetailTableModel();
		HelpController helpController = appConfig.getHelpController();
		Collection msgs = (Collection)request.getAttribute(Constants.MESSAGES);	
		if (msgs == null){
			msgs = new ArrayList();
		}

	
		int lastRowModel = visibleRowCount + visibleFirstRow - 1;

		try {

			if (lastRowModel > model.getRowCount())
				lastRowModel = model.getRowCount();
	
			int loopcount = 0;
			for (int i = visibleFirstRow; i <= lastRowModel; i++) {
				loopcount++;
				AbstractDataType value = pageContext.getDataForComponentId(ScenarioConfigForm.SCENARIO_TABLE_VIEW_ID, ScenarioConfigForm.PARAM_VALUE_LIST_BOX_ID, loopcount);
				model.setValueAt(value, i, 2);
			}
		} catch (Throwable t) {
			log.error(LogUtil.APPS_USER_INTERFACE,"system.xcm.admin.exception", new Object[] {}, t);
		}
	
		Checkbox cb = (Checkbox)pageContext.getComponentForId("defaultScenarioCheckbox"); 
		String isDefault = cb.getValueAsDataType().getValueAsString();
		Checkbox chk_deactivate = (Checkbox)pageContext.getComponentForId("deactivateScenarioCheckbox");
		String isActive = chk_deactivate.getValueAsDataType().getValueAsString();
		
		// if it is the only customer scenario set it a default scenario
		scenConfig.setSingleCustomerScenarioAsDefault();
		boolean def_not_active = false;
		if (isDefault.equalsIgnoreCase(Constants.TRUE)) {
			if (isActive.equalsIgnoreCase(Constants.TRUE)) {
				scenConfig.setCurrentScenarioAsDefault();
			}
			else{
				//scenConfig.setCurrentScenarioAsDefault();
				def_not_active = true;
			}
			
		}
		
		if (isActive.equalsIgnoreCase(Constants.TRUE)) {
			scenConfig.setCurrentScenarioAsActive(true);
		}else{
			if (isDefault.equalsIgnoreCase(Constants.TRUE)) {
				def_not_active = true;				
			}
			else{
			scenConfig.setCurrentScenarioAsActive(false);
			}
		}

		if (def_not_active)
			msgs.add(MessageAccessor.getMessageAccessor().getMessage(MyMessages.DEFAULT_NOT_DEACTIVATE));
		
		scenConfig.saveTableModel();

		scenConfig.updateConfigController(scenConfig.getCurrentScenarioName());
		
		String retVal = scenConfig.saveConfiguration();
		// nps: xcm db enhancements, hack to trigger save of component configuration 
		// data as well to force reencryption of secure data		
		if(retVal.equalsIgnoreCase(Constants.OK))
			retVal = appConfig.getScenarioConfigController().saveConfiguration();

		if(retVal.equals(Constants.OK)) {	
			msgs.add(MessageAccessor.getMessageAccessor().getMessage(MyMessages.SAVE_SUCCESS));			
		}
		else {
			msgs.add(MessageAccessor.getMessageAccessor().getMessage(MyMessages.SAVE_FAILURE));			
			msgs.add(MessageAccessor.getMessageAccessor().getMessage(MyMessages.SAVE_FAILURE_RECOMMENDATION));			
		}			

		
				
		// get update short text
		AbstractDataType adt = pageContext.getDataForComponentId(ScenarioConfigForm.SCEN_SHORT_TEXT_INPUT_FIELD_ID);
		if (adt != null) {
			String shortText = adt.toString();
			scenConfig.setCurrentScenarioShorttext(shortText);			
		}
		 
				
		request.setAttribute(Constants.SAVE_STATUS, Constants.OK);			

		appConfig.getConfigTreeModel().updateScenNodes();

		appConfig.getConfigTreeModel().setScenarioActiveNode(scenConfig.getCurrentScenarioName(), false);

		helpController.updateController();
		
		request.setAttribute(Constants.MESSAGES,msgs);

		return mapping.findForward(ActionForwardConstants.SUCCESS);
	}


private static interface MyMessages {
	public static final String ID = MyMessages.class.getName() + "_";		
	public static final String DEFAULT_NOT_DEACTIVATE = ID + 1;
	public static final String DEFAULT_ACTIVATED = ID + 2;
		
	public static final String SAVE_SUCCESS = ID + 3;
	public static final String SAVE_FAILURE = ID + 4;
	public static final String SAVE_FAILURE_RECOMMENDATION = ID + 5;		
	public static final String REFRESH_FAILURE = ID + 6;
}
}