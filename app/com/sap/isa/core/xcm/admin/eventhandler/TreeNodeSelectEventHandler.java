package com.sap.isa.core.xcm.admin.eventhandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.logging.IsaLocation;
import com.sapportals.htmlb.event.Event;
import com.sapportals.htmlb.event.TreeNodeSelectEvent;
import com.sapportals.htmlb.rendering.IPageContext;

/**
 *
 */
public abstract class TreeNodeSelectEventHandler implements EventHandler {

	protected static IsaLocation log = null;

	public TreeNodeSelectEventHandler() {
		if (log == null)
			log = IsaLocation.getInstance(this.getClass().getName());
	}


	/**
	 * @see com.sap.isa.core.xcm.admin.eventhandler.EventHandler#perform(ActionMapping, Event, IPageContext, HttpSession, HttpServletRequest, HttpServletResponse)
	 */
	public ActionForward perform(
		ActionMapping mapping,
		Event event,
		IPageContext pageContext,
		HttpSession session,
		HttpServletRequest request,
		HttpServletResponse response) {

    return perform(mapping, (TreeNodeSelectEvent)event, pageContext, session, request, response);
	}

  /**
   * Has to be implemented by the extending class
   */
  public abstract ActionForward perform( ActionMapping mapping,
                                         TreeNodeSelectEvent event,
                                         IPageContext pageContext,
                                         HttpSession session,
                                         HttpServletRequest request,
                                         HttpServletResponse response);

	
}
