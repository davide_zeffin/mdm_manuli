package com.sap.isa.core.xcm.admin.eventhandler.listbox;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.xcm.admin.ActionForwardConstants;
import com.sap.isa.core.xcm.admin.SessionConstants;
import com.sap.isa.core.xcm.admin.actionform.CompConfigForm;
import com.sap.isa.core.xcm.admin.controller.AppConfigController;
import com.sap.isa.core.xcm.admin.controller.ComponentConfigController;
import com.sap.isa.core.xcm.admin.eventhandler.ListSelectEventEventHandler;
import com.sapportals.htmlb.DropdownListBox;
import com.sapportals.htmlb.event.ListSelectEvent;
import com.sapportals.htmlb.rendering.IPageContext;

/**
 *
 */
public class TestSelectEventHandler extends ListSelectEventEventHandler {

	/**
	 * @see com.sap.isa.core.xcm.admin.eventhandler.ListSelectEventEventHandler#perform(ActionMapping, ListSelectEvent, IPageContext, HttpSession, HttpServletRequest, HttpServletResponse)
	 */
	public ActionForward perform(
		ActionMapping mapping,
		ListSelectEvent event,
		IPageContext pageContext,
		HttpSession session,
		HttpServletRequest request,
		HttpServletResponse response) {

        UserSessionData userData = UserSessionData.getUserSessionData(session);

		AppConfigController appConfig = (AppConfigController)userData.getAttribute(SessionConstants.APP_CONFIG_CONTROLLER);
		ComponentConfigController compConfig = appConfig.getCompConfigController();

		DropdownListBox listBox = (DropdownListBox)pageContext.getComponentForId(CompConfigForm.TEST_LIST_BOX_ID);
		
		String selectedTest = listBox.getSelection();
		
		compConfig.setCurrentTestName(selectedTest);


		return mapping.findForward(ActionForwardConstants.SUCCESS);
	}

}
