package com.sap.isa.core.xcm.admin.eventhandler.button;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.xcm.admin.ActionForwardConstants;
import com.sap.isa.core.xcm.admin.Constants;
import com.sap.isa.core.xcm.admin.SessionConstants;
import com.sap.isa.core.xcm.admin.actionform.CompConfigForm;
import com.sap.isa.core.xcm.admin.controller.AppConfigController;
import com.sap.isa.core.xcm.admin.controller.ComponentConfigController;
import com.sap.isa.core.xcm.admin.eventhandler.ButtonOnClickEventHandler;
import com.sap.isa.core.xcm.admin.model.table.ComponentDetailTableModel;
import com.sapportals.htmlb.event.ButtonClickEvent;
import com.sapportals.htmlb.rendering.IPageContext;
import com.sapportals.htmlb.table.TableView;
import com.sapportals.htmlb.type.AbstractDataType;

/**
 *
 */
public class CompParamHelpEventHandler
	extends ButtonOnClickEventHandler {

	/**
	 * @see com.sap.isa.core.xcm.admin.eventhandler.ButtonOnClickEventHandler#perform(ActionMapping, ButtonOnClickEventHandler, IPageContext, HttpSession, HttpServletRequest, HttpServletResponse)
	 */
	public ActionForward perform(
		ActionMapping mapping,
		ButtonClickEvent event,
		IPageContext pageContext,
		HttpSession session,
		HttpServletRequest request,
		HttpServletResponse response) {
  

        UserSessionData userData = UserSessionData.getUserSessionData(session);
		AppConfigController appConfig = (AppConfigController)userData.getAttribute(SessionConstants.APP_CONFIG_CONTROLLER);
		ComponentConfigController compConfig = appConfig.getCompConfigController();

		// string comp name equals row number
		String compName = event.getComponentName(); 
		
		TableView tableView = (TableView)pageContext.getComponentForId(CompConfigForm.PARAM_TABLE_VIEW_ID);

		int firstVisRow = tableView.getVisibleFirstRow();
		int visibleRowCount = tableView.getVisibleRowCount();

		int lastRowModel = visibleRowCount + firstVisRow - 1;
		
		ComponentDetailTableModel model = compConfig.getCompDetailTableModel();

		if (log.isDebugEnabled())
			log.config("[last row in model]='" + model.getRowCount());

		try {

			if (lastRowModel > model.getRowCount())
				lastRowModel = model.getRowCount();
	
			int row = firstVisRow;
			int loopcount = 0;
			for (int i = 1; i <= visibleRowCount; i++) {
				loopcount++;
				AbstractDataType value = pageContext.getDataForComponentId(CompConfigForm.PARAM_TABLE_VIEW_ID, ComponentDetailTableModel.NAME_PARAM_VALUE_INPUT_FIELD, loopcount);
				if (value == null || value.getValueAsString() == null)
					value = pageContext.getDataForComponentId(CompConfigForm.PARAM_TABLE_VIEW_ID, ComponentDetailTableModel.NAME_PARAM_VALUE_DROPDOWN_LISTBOX, loopcount);
				model.setValueAt(value, row, 2);
				row++;

			}
		} catch (Throwable t) {
			log.error(LogUtil.APPS_USER_INTERFACE,"system.xcm.admin.exception", new Object[] {}, t);
		}



		 
		
		int row = Integer.parseInt(compName);
		
		int visibleRow = row % visibleRowCount;		
	
		if (visibleRow == 0)
			visibleRow = row; 
						
			AbstractDataType adt = model.getValueAt(row, 1);
			if (adt.toString() != null) {
				String paramName = adt.toString();	

				String shorttext = compConfig.getConfigParamShorttext(paramName);
				String longtext = compConfig.getConfigParamLongtext(paramName);
				if (shorttext == null)
					shorttext = Constants.MSG_SHORTTEXT_MISSING;

				userData.setAttribute(SessionConstants.SCEN_CONFIG_PARAM_VALUE_HELP_SHORTTEXT, shorttext);
				userData.setAttribute(SessionConstants.SCEN_CONFIG_PARAM_VALUE_HELP_LONGTEXT, longtext);
				userData.setAttribute(SessionConstants.SCEN_CONFIG_PARAM_VALUE_HELP, paramName);


				compConfig.isCompParamHelp(true);
			}

		return mapping.findForward(ActionForwardConstants.SUCCESS);
	}

}
