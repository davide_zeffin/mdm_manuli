package com.sap.isa.core.xcm.admin.eventhandler.button;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.config.AdminConfig;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.xcm.admin.ActionForwardConstants;
import com.sap.isa.core.xcm.admin.Constants;
import com.sap.isa.core.xcm.admin.SessionConstants;
import com.sap.isa.core.xcm.admin.actionform.CompConfigForm;
import com.sap.isa.core.xcm.admin.controller.AppConfigController;
import com.sap.isa.core.xcm.admin.controller.ComponentConfigController;
import com.sap.isa.core.xcm.admin.controller.HelpController;
import com.sap.isa.core.xcm.admin.eventhandler.ButtonOnClickEventHandler;
import com.sap.isa.core.xcm.admin.helper.DefaultResourceAccessor;
import com.sap.isa.core.xcm.admin.helper.MessageAccessor;
import com.sap.isa.core.xcm.admin.model.table.ComponentDetailTableModel;
import com.sapportals.htmlb.event.ButtonClickEvent;
import com.sapportals.htmlb.rendering.IPageContext;
import com.sapportals.htmlb.table.TableView;
import com.sapportals.htmlb.type.AbstractDataType;

/**
 *
 */
public class CompConfigSaveEventHandler
	extends ButtonOnClickEventHandler {

		static {
			DefaultResourceAccessor accessor = DefaultResourceAccessor.getInstance();
			accessor.setMessageText(MyMessages.SAVE_FAILURE,"Failed to save component configuration data. Configuration changes will not be available after restarting the application");
			accessor.setMessageText(MyMessages.SAVE_FAILURE_RECOMMENDATION,"It is recommeded to cancel the Edit operation");			
			accessor.setMessageText(MyMessages.SAVE_SUCCESS,"Component configuration data saved successfully. Configuration changes will be available after restarting the application");				
		}

	/**
	 * @see com.sap.isa.core.xcm.admin.eventhandler.ButtonOnClickEventHandler#perform(ActionMapping, ButtonOnClickEventHandler, IPageContext, HttpSession, HttpServletRequest, HttpServletResponse)
	 */
	public ActionForward perform(
		ActionMapping mapping,
		ButtonClickEvent event,
		IPageContext pageContext,
		HttpSession session,
		HttpServletRequest request,
		HttpServletResponse response) {
  
		// check again if the user has XCM Admin rights???
		if(!AdminConfig.isXCMAdmin(request)) {
			return mapping.findForward(ActionForwardConstants.FAILURE);
		}
		
        UserSessionData userData = UserSessionData.getUserSessionData(session);
		AppConfigController appConfig = (AppConfigController)userData.getAttribute(SessionConstants.APP_CONFIG_CONTROLLER);
		ComponentConfigController compConfig = appConfig.getCompConfigController();
		HelpController helpController = appConfig.getHelpController(); 
		ComponentDetailTableModel model = compConfig.getCompDetailTableModel();
		
		ArrayList msgs = new ArrayList();
		
		// get data from the visible rows and write them into the model 
		TableView tableView = (TableView)pageContext.getComponentForId(CompConfigForm.PARAM_TABLE_VIEW_ID);
		int visibleFirstRow = tableView.getVisibleFirstRow();
		int visibleRowCount = tableView.getVisibleRowCount();

		
		int lastRowModel = visibleRowCount + visibleFirstRow - 1;
		

		try {

			if (lastRowModel > model.getRowCount())
				lastRowModel = model.getRowCount();
	
			int row = visibleFirstRow;
			int loopcount = 0;
			for (int i = 1; i <= visibleRowCount; i++) {
				loopcount++;
				AbstractDataType value = pageContext.getDataForComponentId(CompConfigForm.PARAM_TABLE_VIEW_ID, ComponentDetailTableModel.NAME_PARAM_VALUE_INPUT_FIELD, loopcount);
				if (value == null || value.getValueAsString() == null)
					value = pageContext.getDataForComponentId(CompConfigForm.PARAM_TABLE_VIEW_ID, ComponentDetailTableModel.NAME_PARAM_VALUE_DROPDOWN_LISTBOX, loopcount);
				
				// check if value is not password which has not been changed
				if (value == null) {
					row++;
					continue;
				}
				if (!value.getValueAsString().equals(Constants.DISPLAY_PASSWORD))
					model.setValueAt(value, row, 2);	

				
				row++;

			}
		} catch (Throwable t) {
			log.error(LogUtil.APPS_USER_INTERFACE,"system.xcm.admin.exception", new Object[] {}, t);
		}
		// check if scenario default scenario
		
		
		
		// save the content of the table model into the config file
		compConfig.saveTableModel();

		// update model 
		String paramConfigId = compConfig.getCurrentParamConfigId();
				
		compConfig.updateConfigController(paramConfigId);
		
    	String retVal = compConfig.saveConfiguration();

		request.setAttribute(Constants.SAVE_STATUS, Constants.OK);
		
		if(retVal.equals(Constants.OK)) {	
			msgs.add(MessageAccessor.getMessageAccessor().getMessage(MyMessages.SAVE_SUCCESS));			
		}
		else {
			msgs.add(MessageAccessor.getMessageAccessor().getMessage(MyMessages.SAVE_FAILURE));			
			msgs.add(MessageAccessor.getMessageAccessor().getMessage(MyMessages.SAVE_FAILURE_RECOMMENDATION));			
		}			
		
		// update tree model
		appConfig.updateConfigTreeModel(false);

		// set active node
		appConfig.getConfigTreeModel().setComponentActiveNode(compConfig.getCurrentCompId(), paramConfigId, compConfig.isSAP());

		helpController.updateController();

		request.setAttribute(Constants.MESSAGES,msgs);
		
		return mapping.findForward(ActionForwardConstants.SUCCESS);
	}

	private static interface MyMessages {
		public static final String ID = MyMessages.class.getName() + "_";
		
		public static final String SAVE_SUCCESS = ID + 1;
		public static final String SAVE_FAILURE = ID + 2;
		public static final String SAVE_FAILURE_RECOMMENDATION = ID + 3;		
	}
}
