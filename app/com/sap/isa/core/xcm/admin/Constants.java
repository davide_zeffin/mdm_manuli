package com.sap.isa.core.xcm.admin;

/**
 * Constants used by the XCM Administration
 */
public interface Constants {
	
	public static final String DISPLAY_MAIN_TABSTRIP_WIDTH  = "800";
	public static final String DISPLAY_MAIN_TABSTRIP_HEIGHT = "400";
	public static final String MSG_SHORTTEXT_MISSING = "Shorttext missing";
	public static final String MSG_DESCRIPTION_NEW_CONFIG = "Enables you to create a new component configuration. Press the button next to the dropdown-listbox to create a new configuration.";
	public static final String TOOLTIP_SCEN_PARAM_HELP = "Additional information about the currently selected parameter value";
	public static final String TOOLTIP_COMP_RESTORE_BASE_BUTTON = "Restore the parameter value of base configuration [value]=";
	public static final String TOOLTIP_SCEN_PARAM_DESCR_HELP = "Additional description about the parameter";
	public static final String CREATE_NEW_CONFIG_ID = "*** Select to create component configuration ***";
	public static final String TOOLTIP_SCEN_PARAM_COMP_BUTTON = "If '" + CREATE_NEW_CONFIG_ID + "' is selected a new component configuration is created. If a parameter value is selected you can view the exisiting component configuration";
	
	
	public static final String SELECT_GLOBAL_CONFIG_REQUEST_PARAM = "globalConfig.xcm";
	public static final String TRUE = "true";
	public static final String FALSE = "false";
	
	public static final String OK = "ok";
	public static final String SAVE_STATUS = "save.status";
		
	/**
	 * if passed as request parameter when running the
	 * application, that a user session data is kept if it is available
  	*/
 	public static final String KEEP_SESSION_REQUEST_PARAM = "keepsession";

	public static final String ERROR_MSG = "error_msg";
	
	public static final String MESSAGES = "xcm_messages";

	/**
	 * Placeholder for displaying passwords
	 */
	public static final String DISPLAY_PASSWORD = "*#*~#*~";

}
