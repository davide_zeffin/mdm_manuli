package com.sap.isa.core.xcm.admin.controller;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.xcm.XCMUtils;
import com.sap.isa.core.xcm.admin.Constants;
import com.sap.isa.core.xcm.admin.actionform.ScenarioConfigForm;
import com.sap.isa.core.xcm.admin.model.CompDataManipulator;
import com.sap.isa.core.xcm.admin.model.ScenarioDataManipulator;
import com.sap.isa.core.xcm.admin.model.table.ScenarioDetailTableModel;
import com.sap.isa.core.xcm.admin.ui.ScenarioDetailCellRenderer;
import com.sap.isa.core.xcm.init.ExtendedConfigInitHandler;
import com.sap.isa.core.xcm.init.XCMAdminComponentMetadata;
import com.sap.isa.core.xcm.init.XCMAdminInitHandler;
import com.sap.isa.core.xcm.init.XCMAdminParamConstrainMetadata;
import com.sapportals.htmlb.enum.TableCellStyle;
import com.sapportals.htmlb.table.TableView;
 

/**
 * Used for scenario management
 */
public class ScenarioConfigController {

	private ScenarioDataManipulator mScenDataManipulator;
	private CompDataManipulator mCompDataManipulator;
	private ScenarioDetailTableModel mTableModel;
	private TableView mTableView;
	private String mCurrentScenName;
	private boolean mIsSAP = false; 
	private boolean mIsCurrentParamConfigNew = false;
	private boolean mIsScenConfigTrayCollapsed = true;
	private boolean mIsScenDescrTrayCollapsed = false;
	private boolean mIsScenarioParamHelp = false;
	private boolean mIsScenarioParamDescrHelp = false;	
	private boolean mIsAdvancedOptionsOn = false;
	private boolean editable;
	private boolean accessible = false;

	protected static IsaLocation log =
						   IsaLocation.getInstance(ScenarioConfigController.class.getName());

	public ScenarioConfigController (CompDataManipulator compDataManipulator, ScenarioDataManipulator man, boolean accessible) {
		setAccessible(accessible);
		mTableModel = new ScenarioDetailTableModel(null, null);
		mTableModel.setReadOnly(!editable);
		mCompDataManipulator = compDataManipulator;
		mScenDataManipulator = man;
	} 
	
	public ScenarioDataManipulator getDataManipulator() {
		return mScenDataManipulator;
	}
	
	public ScenarioDetailTableModel getCompDetailTableModel() {
		return mTableModel;	
	}
	

	public void isSAP(boolean isSAP) {
		mIsSAP = isSAP;
	}

	public boolean isSAP() {
		return mIsSAP;
	}

	public String getCurrentScenarioName() {
		return mCurrentScenName;
	}

	public String getBaseConfigName() {
		if (mCurrentScenName == null || mCurrentScenName.length() == 0)
			return "";
		return getCurrentScenarioConfig().getBaseConfigName();
	}

	/**
	 * Creates new configuration, based on the currently selected
	 */
	public void createScenarioConfiguration(String newScenName) {
		if (mScenDataManipulator.isScenarioAvailable(newScenName))
			return;
		
		mIsCurrentParamConfigNew = true;
		mScenDataManipulator.createScenarioConfig(newScenName, mCurrentScenName);
		updateConfigController(newScenName);
	}

	public String getCurrentScenarioShorttext() {
		ScenarioDataManipulator.ScenarioConfig scenConfig = 
			mScenDataManipulator.getScenarioConfig(mCurrentScenName);
		if (scenConfig == null)
			return "";
		return scenConfig.getShorttext();
	}

	public void setCurrentScenarioShorttext(String shortText) {
		ScenarioDataManipulator.ScenarioConfig scenConfig = 
			mScenDataManipulator.getScenarioConfig(mCurrentScenName);
		if (scenConfig == null)
			return;
		scenConfig.setShortText(shortText);
		
	}


	public String getCurrentScenarioLongtext() {
		ScenarioDataManipulator.ScenarioConfig scenConfig = 
			mScenDataManipulator.getScenarioConfig(mCurrentScenName);
		if (scenConfig == null)
			return "";
		return scenConfig.getLongtext();
	}

	public String getCurrentScenarioValue(String paramName) {
		ScenarioDataManipulator.ScenarioConfig scenConfig = 
			mScenDataManipulator.getScenarioConfig(mCurrentScenName);
		if (scenConfig == null)
			return "";
		Map params = scenConfig.getParamValues();
		 
		return scenConfig.getShorttext();
	}

	public List getCurrentScenarioAllowedValues(String paramName) {
		return ControllerUtils.getScenarioParamAllowedValues(mCurrentScenName, paramName, mScenDataManipulator, mCompDataManipulator);
		}

	public TableView getScenarioDetailTableView() {
		if (mTableView == null)
			initScenarioDetailTableView();

		mTableView.setModel(mTableModel);
		return mTableView;
	}


	public void updateConfigController(String scenName) {
		mCurrentScenName = scenName;
		
		// update table model
		updateScenDetailTable(scenName);
		// update table view
		initScenarioDetailTableView();
		mIsSAP = !isScenarioDerived(scenName);
	}

	private void initScenarioDetailTableView() {
		mTableView = new TableView(ScenarioConfigForm.SCENARIO_TABLE_VIEW_ID);
		mTableView.setModel(mTableModel);
		mTableView.setHeaderText("Data");
		mTableView.setHeaderVisible(true);
		mTableView.setWidth("100%");
		mTableView.setFillUpEmptyRows(true);
		mTableView.setHeaderVisible(false);
		mTableView.getColumn(2).setWidth("20%");
		
		if (isAccessible()) {
			mTableView.setVisibleRowCount(mTableView.getRowCount());
		}
		else {
			mTableView.setVisibleRowCount(20);
		}
			
		mTableView.setOnNavigate(ScenarioDetailTableModel.ON_NAVIGATE_EVENT);
		mTableView.setCellRenderer(new ScenarioDetailCellRenderer(this, mTableModel));

		if (!isAccessible()) {
		for (int i = 0; i <= mTableModel.getRowCount(); i++) {
			if (mTableModel.isDerived(i)) {
				mTableView.setStyleForCell(i, 1, TableCellStyle.CRITICALVALUE_LIGHT);
				mTableView.setStyleForCell(i, 2, TableCellStyle.CRITICALVALUE_LIGHT);
				mTableView.setStyleForCell(i, 3, TableCellStyle.CRITICALVALUE_LIGHT);				
				mTableView.setStyleForCell(i, 4, TableCellStyle.CRITICALVALUE_LIGHT);				
				mTableView.setStyleForCell(i, 5, TableCellStyle.CRITICALVALUE_LIGHT);				
				mTableView.setStyleForCell(i, 6, TableCellStyle.CRITICALVALUE_LIGHT);				
			}
		}
		for (int i = 0; i <= mTableModel.getRowCount(); i++) {
			if (mTableModel.isDeprecated(i)) {
				mTableView.setStyleForCell(i, 1, TableCellStyle.TOTAL);
				mTableView.setStyleForCell(i, 2, TableCellStyle.TOTAL);
				mTableView.setStyleForCell(i, 3, TableCellStyle.TOTAL);				
				mTableView.setStyleForCell(i, 4, TableCellStyle.TOTAL);				
				mTableView.setStyleForCell(i, 5, TableCellStyle.TOTAL);				
				mTableView.setStyleForCell(i, 6, TableCellStyle.TOTAL);
								
			}
		}
	}

	}

	/**
	 * Returns the type of the parameter
	 */
	public String getConfigParamType(String configParam) {
		XCMAdminParamConstrainMetadata scenMetaData = XCMAdminInitHandler.getXCMAdminScenarioMetadata(configParam);
		String type = null;
		if (scenMetaData == null) {
			String msg = "Configuration parameter [name]='" + configParam + "' not found"; 
			log.warn("system.xcm.warn", new Object[] { msg }, null);
			return "";
		}
		// check if type has been overwritten directly in scneario
		// configuration
		ScenarioDataManipulator.ParamConstrain currentConstrain = 
			getCurrentParamConstrain(configParam);
		if(currentConstrain != null && currentConstrain.getType() != null && currentConstrain.getType().length() > 0)
			type = currentConstrain.getType();
		else
			type =  scenMetaData.getType();
		
		return type;
	}
 


	/**
	 * Get config param description
	 */
	public String getConfigParamShorttext(String configParam) {
		// if this a new scenario, take the description of the base scenario
		if (isCurrentParamConfigNew()) {
			return ControllerUtils.getScenConfigParamShorttext(getBaseConfigName(), configParam);
		}
		return ControllerUtils.getScenConfigParamShorttext(mCurrentScenName, configParam);
	}

	/**
	 * Get config param description
	 */
	public String getConfigParamLongtext(String configParam) {
		return ControllerUtils.getScenConfigParamLongtext(mCurrentScenName, configParam);
	}		

	public void deleteCurrentScenarioConfig() {
		// specify the base configuration 
		mScenDataManipulator.deleteScenarioConfig(mCurrentScenName);
	}	



	/**
	 * Returns description for value of config param
	 */
	public String getConfigParamValueLongtext(String configParam, String configValue) {
		return XCMUtils.getScenarioConfigParamValueLongtext(configParam, configValue);
	}

	/**
	 * Returns description for value of config param
	 */
	public String getConfigParamValueShorttext(String configParam, String configValue) {
		return ControllerUtils.getScenarioConfigParamValueShorttext(configParam, configValue, mCompDataManipulator);
	}
 
	public boolean isParamConfigDerived(String paramName) {
		if (mCurrentScenName == null)
			return false;
		
		ScenarioDataManipulator.ParamConfig paramConfig = 
			getCurrentParamConfig(paramName);
		if (paramConfig == null) {
			String msg = "Configuration parameter [name]='" + paramName + "' for [scenario]='" + mCurrentScenName + "' not found"; 
			log.warn("system.xcm.warn", new Object[] { msg }, null);
			
			return false;
		}
		return paramConfig.isDerived();
	}

	public boolean isScenarioDerived(String scenName) {
		if (mCurrentScenName == null)
			return false;
		
		ScenarioDataManipulator.ScenarioConfig scenConfig = 
			mScenDataManipulator.getScenarioConfig(mCurrentScenName); 
			
		if (scenConfig == null) {
			String msg = "Application configuration [name]='" + scenConfig + "' not found"; 
			log.warn("system.xcm.warn", new Object[] { msg }, null);
			return false;
		}
		return scenConfig.isScenarioDerived();
	}

	
	public boolean isCurrentScenarioObsolete() {
		if (mCurrentScenName == null)
			return false;
		return mScenDataManipulator.getScenarioConfig(mCurrentScenName).isObsolete();
	}
	
	public boolean isScenarioParamObsolete (String paramName) {
		if (mCurrentScenName == null)
			return false;

		XCMAdminParamConstrainMetadata scenParam = XCMAdminInitHandler.getXCMAdminScenarioMetadata(paramName);
		
		
		if (scenParam == null) {
			String msg = "Configuration parameter [name]='" + paramName + "' for [scenario]='" + mCurrentScenName + "' not found"; 
			log.warn("system.xcm.warn", new Object[] { msg }, null);
			return false;
		}
		if (scenParam.getStatus() == null || !scenParam.getStatus().equals(XCMAdminComponentMetadata.OBSOLETE))
			return false;
		return true;	
	
	}

	private ScenarioDataManipulator.ScenarioConfig getCurrentScenarioConfig() {
		return mScenDataManipulator.getScenarioConfig(mCurrentScenName);
	}

	private ScenarioDataManipulator.ParamConfig getCurrentParamConfig(String paramName) {
		ScenarioDataManipulator.ScenarioConfig currentScen = 
		getCurrentScenarioConfig();
		if (currentScen == null)
			return null;
		else
			return currentScen.getParamConfig(paramName);
	}

	private ScenarioDataManipulator.ParamConstrain getCurrentParamConstrain(String paramName) {
		ScenarioDataManipulator.ScenarioConfig currentScen = 
		getCurrentScenarioConfig();
		if (currentScen == null)
			return null;
		else
			return currentScen.getParamConstrain(paramName);
	}


	/**
	 * Updates the table model. The data for the table is
	 * retrieved form the component data manipulator
	 * If paramConfigId is null or empty string then 
	 * an empty table is initialized
	 */
	private void updateScenDetailTable(String scenarioName) {
		if (log.isDebugEnabled())
			log.debug("Updating scenario detail table, [name]='" + scenarioName + "'");

		ScenarioDataManipulator.ScenarioConfig scenConfig = mScenDataManipulator.getScenarioConfig(scenarioName);
		if (scenConfig == null) {
			if (log.isDebugEnabled())
				log.debug("Scenario [name]='" + scenarioName + "' not found");
			return;
		}
		mTableModel = new ScenarioDetailTableModel(this, scenConfig);
		mTableModel.setReadOnly(!editable);				
	}

	public ScenarioDetailTableModel getScenarioDetailTableModel() {
		return mTableModel;	
	}


	/**
	 * Saves the current configuration to the file system
	 */
	public String saveConfiguration() {
		try {
			mScenDataManipulator.saveConfiguration();
		} catch (Exception ex) {
			log.error(LogUtil.APPS_USER_INTERFACE_CONTROLLER,"system.xcm.admin.exception", new Object[] {"Error saving configuration"}, ex);			
			return ex.toString();
		}
		
		isCurrentParamConfigNew(false);
		
		// update ISA Framework
		ExtendedConfigInitHandler.updateXCMConfig();
		return Constants.OK; 
	}

	/**
	 * invalidates the Controller Models
	 *
	 */
	public void invalidate() {
		/*
		try {
			mScenDataManipulator.invalidateConfiguration();
		} catch (XCMAdminException ex) {
			log.error(LogUtil.APPS_USER_INTERFACE_CONTROLLER,"system.xcm.error",new Object[]{ex},ex);
		}*/
		
		if(!(mCurrentScenName == null || mCurrentScenName.equals(""))) {
			if (mIsCurrentParamConfigNew)
				resetController();
			else
				updateConfigController(mCurrentScenName);
		}				
	}

	public void overwrite() {
	}
	
	public boolean isCurrentParamConfigNew() {
		return mIsCurrentParamConfigNew;
	}
	
	/**
	 * Returns true if the current scenario is the
	 * current scenario
	 */
	public boolean isCurrentScenarioDefault() {
		if (mCurrentScenName == null || mCurrentScenName.length() == 0)
			return false;
		ScenarioDataManipulator.ScenarioConfig scenConfig = 
			mScenDataManipulator.getScenarioConfig(mCurrentScenName);
		
		if (scenConfig == null)
			return false;
			
		return scenConfig.isDefaultScenario();
	}
	
	public boolean isCurrentScenarioActive() {
		if (mCurrentScenName == null || mCurrentScenName.length() == 0)
			return false;
		ScenarioDataManipulator.ScenarioConfig scenConfig = 
			mScenDataManipulator.getScenarioConfig(mCurrentScenName);
		
		if (scenConfig == null)
			return false;
			
		return scenConfig.isActiveScenario();
	}

	public void isCurrentParamConfigNew(boolean isNew) {
		mIsCurrentParamConfigNew = isNew;
	}

	
	public void isAdvancedOptionsOn(boolean isAdvancedOn) {
		mIsAdvancedOptionsOn = isAdvancedOn;
	}

	public boolean isAdvancedOptionsOn() {
		return mIsAdvancedOptionsOn;
	}


	/**
	 * Sets the current scenario as default
	 */
	public void setCurrentScenarioAsDefault() {
		mScenDataManipulator.setAsDefaultConfig(mCurrentScenName);
	}

	/**
	 * Sets the current scenario as default
	 */
	public boolean setSingleCustomerScenarioAsDefault() {
		return mScenDataManipulator.setSingleCustomerScenarioAsDefault();
	}


	/**
	 * Writes the data entered by the user into the table back to the XML
	 * docuemnts The data is retrieved from the currently active table model
	 */
	public void saveTableModel() {
		int rowCount = mTableModel.getRowCount();
		ScenarioDataManipulator.ScenarioConfig currentConfig = getCurrentScenarioConfig();	
		for (int i = 0; i <= rowCount; i++) {
			String  value = mTableModel.getValueAt(i, ScenarioDetailTableModel.COL_INDEX_SCENARIO_VALUE).getValueAsString();
			String  name = mTableModel.getValueAt(i, ScenarioDetailTableModel.COL_INDEX_SCENARIO_NAME).getValueAsString();
			ScenarioDataManipulator.ParamConfig currentParam = getCurrentParamConfig(name);
			if (currentParam != null)
				currentParam.setValue(value);
		}
	}

	public void isScenarioConfigTrayCollapsed(boolean isCollapsed) {
		mIsScenConfigTrayCollapsed = isCollapsed;
	}

	public boolean isScenarioConfigTrayCollapsed() {
		return mIsScenConfigTrayCollapsed;
	}

	public void isScenarioDescrTrayCollapsed(boolean isCollapsed) {
		mIsScenDescrTrayCollapsed = isCollapsed;
	}

	public boolean isScenarioDescrTrayCollapsed() {
		return mIsScenDescrTrayCollapsed;
	}

	public boolean isScenarioParamHelp() {
		return mIsScenarioParamHelp;
	}

	public void isScenarioParamHelp(boolean isHelp) {
		mIsScenarioParamHelp = isHelp;
	}

	public void isScenarioParamDescrHelp(boolean isHelp) {
		mIsScenarioParamDescrHelp = isHelp;
	}

	public boolean isScenarioParamDescrHelp() {
		return mIsScenarioParamDescrHelp;
	}

	/**
	 * Resets controller
	 */
	public void resetController() {
		if (mIsCurrentParamConfigNew)
			deleteCurrentScenarioConfig();
		mIsSAP = false;
		mIsCurrentParamConfigNew = false;
		mTableModel = null;
		mCurrentScenName = "";
		mIsSAP = false; 
		mIsCurrentParamConfigNew = false;
		mIsScenarioParamHelp = false;
		mIsScenarioParamDescrHelp = false;	
		mIsAdvancedOptionsOn = false;
		
	}
	
	public boolean isScenarioAvailbale(String scenName) {
		return mScenDataManipulator.isScenarioAvailable(scenName);	
	}
	/**
	 * @return
	 */
	public boolean isEditable() {
		return editable;
	}

	/**
	 * @param b
	 */
	public void setEditable(boolean b) {
		editable = b;
		if(mTableModel != null) {
			mTableModel.setReadOnly(!editable);
		}
	}

	/**
	 * @return
	 */
	public boolean isAccessible() {
		return accessible;
	}

	/**
	 * @param b
	 */
	public void setAccessible(boolean b) {
		accessible = b;
	}
	
	public void setCurrentScenarioAsActive(boolean active){
		mScenDataManipulator.setAsActive(mCurrentScenName, active);		
	}
	

}
