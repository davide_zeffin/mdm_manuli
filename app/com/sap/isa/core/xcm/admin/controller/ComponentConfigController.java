package com.sap.isa.core.xcm.admin.controller;

// java imports
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import com.sap.isa.core.init.InitializationHandler;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.test.ComponentTest;
import com.sap.isa.core.xcm.XCMUtils;
import com.sap.isa.core.xcm.admin.Constants;
import com.sap.isa.core.xcm.admin.actionform.CompConfigForm;
import com.sap.isa.core.xcm.admin.model.CompDataManipulator;
import com.sap.isa.core.xcm.admin.model.table.ComponentDetailTableModel;
import com.sap.isa.core.xcm.admin.ui.ComponentDetailCellRenderer;
import com.sap.isa.core.xcm.init.XCMAdminComponentMetadata;
import com.sap.isa.core.xcm.init.XCMAdminConstants;
import com.sap.isa.core.xcm.init.XCMAdminInitHandler;
import com.sapportals.htmlb.DefaultListModel;
import com.sapportals.htmlb.IListModel;
import com.sapportals.htmlb.enum.TableCellStyle;
import com.sapportals.htmlb.table.TableView;


/**
 * This class controls the behaviour of the UI used
 * to configure components
 */
public class ComponentConfigController {

	public static final String TRUE =  "true";
	public static final String FALSE = "false";
	
	private static org.apache.struts.util.MessageResources mMessageResources =
			InitializationHandler.getMessageResources();


    protected static IsaLocation log =
                           IsaLocation.getInstance(ComponentConfigController.class.getName());

	
	private boolean mIsSAP = false;
	private boolean mIsTestAvailable = false;
	private String mCurrentCompId = "";
	private String mCurrentParamConfigId = "";	
	private ComponentDetailTableModel mTableModel;
	private CompDataManipulator mConfigManipulator;
	private TableView mConfDataTableView;
	private ComponentTest mCompTests;
	private String mCurrentTestName;	
	private String mCurrentBaseConfigName;	
	private boolean mIsCurrentParamConfigNew = false;
	private boolean mIsCompParamHelp = false;
	private boolean mIsCompParamValueHelp = false;
	private boolean editable;
	private boolean accessible;
	
	public ComponentConfigController(CompDataManipulator manipulator, boolean accessible) {
		setAccessible(accessible);
		mTableModel = new ComponentDetailTableModel(null);
		mTableModel.setReadOnly(!editable);
		mConfigManipulator = manipulator;
	}	
	
	public CompDataManipulator getDataManipulator() {
		return mConfigManipulator;
	}
	 	
	/**
	 * Returns configuration parameter meta data
	 */
	private CompDataManipulator.ParamConfig getConfigParam(String configParamName, String configValue) {
		CompDataManipulator.CompConfig compConfig = mConfigManipulator.getCompConfig(mCurrentCompId);
		if (compConfig == null)
			return null;
		
		CompDataManipulator.ParamConfig paramConfig = compConfig.getParamConfig(configParamName);
		return  paramConfig;
	}

	/**
	 * Returns longtext for value of config param
	 */
	public String getConfigParamValueLongtext(String configParamName, String configValue) {
			return XCMUtils.getCompConfigParamValueLongtext(mCurrentCompId, configParamName, configValue);
	}

	/**
	 * Returns longtext for value of config param
	 */
	public String getConfigParamShorttext(String configParamName) {
			return XCMUtils.getCompConfigParamShorttext(mCurrentCompId, configParamName);
	}

	/**
	 * Returns longtext for value of config param
	 */
	public String getConfigParamLongtext(String configParamName) {
			String longtext = XCMUtils.getCompConfigParamLongtext(mCurrentCompId, configParamName);
			if ((longtext == null || longtext.length()<=0) && getConfigParamStatus(configParamName).
				equalsIgnoreCase(XCMAdminConstants.OBSOLETE))
				longtext = mMessageResources.getMessage("xcm.tree.deprecated.tooltip");
			return longtext;
	}

	/**
	 * Returns longtext for value of config param
	 */
	public String getConfigParamValueShorttext(String configParamName, String configValue) {
			return XCMUtils.getCompConfigParamValueShorttext(mCurrentCompId, configParamName, configValue);
	}
	
	public String getConfigParamStatus(String configParamName){
		return XCMUtils.getCompConfigParamStatus(mCurrentCompId, configParamName);
	}



	/**
	 * Returns true, if there current component
	 * has a parameter configuration with the provided name
	 */
	public boolean isParamConfigIdExisting(String configId) {
		
		CompDataManipulator.CompConfig compConfig = mConfigManipulator.getCompConfig(mCurrentCompId);
		return compConfig.isParamConfigAvailable(configId);
	}
	
	public boolean isParamConfigDerived(String paramConfigName) {
		CompDataManipulator.ParamConfig paramConfig = 
			mConfigManipulator.getCompConfig(mCurrentCompId).getParamConfig(paramConfigName);
		
		if (paramConfig != null && paramConfig.isDerivedCompConfig())
			return true;
		else
			return false;
	}

	public String isTestDisabled() {
		if (!(mIsTestAvailable && mCurrentParamConfigId != null))
			return TRUE;
		else 
			return FALSE;
	}	

	public void isSAP(boolean isSAP) {
		mIsSAP = isSAP;
	}

	public boolean isSAP() {
		return mIsSAP;
	}

	public boolean isCompGeneral() {
		return XCMUtils.isApplicationScopeCompConfig(mCurrentCompId);
	}

	public String isNameDisabled() {
		if (isSAP())
			return TRUE;
		else 
			return FALSE;
	}	

	public boolean isCurrentParamConfigNew() {
		return mIsCurrentParamConfigNew;
	}

	public void isCurrentParamConfigNew(boolean isNew) {
		mIsCurrentParamConfigNew = isNew;
	}


	public String isBaseDisabled() {
		if (isSAP())
			return TRUE;
		else 
			return FALSE;
	}	

	public void isCompParamHelp(boolean isCompParamHelp) {
		mIsCompParamHelp = isCompParamHelp;
	}

	public boolean isCompParamHelp() {
		return mIsCompParamHelp;
	}
	
	public void isCompParamValueHelp(boolean isCompParamValueHelp){
		mIsCompParamValueHelp = isCompParamValueHelp;
	}
	
	public boolean isCompParamValueHelp() {
			return mIsCompParamValueHelp;
	}


	public String getCurrentCompId() {
		return mCurrentCompId;		
	}

	public String getCurrentCompShorttext() {
		return XCMUtils.getCompShorttext(mCurrentCompId);
	}

	public String getCurrentCompLongtext() {
		return XCMUtils.getCompLongtext(mCurrentCompId);
	}


	public String getCurrentBaseConfigShorttext() {
		return XCMUtils.getCompConfigShorttext(mCurrentCompId, mCurrentBaseConfigName);
	}

	public String getCurrentBaseConfigLongtext() {
		return XCMUtils.getCompConfigLongtext(mCurrentCompId, mCurrentBaseConfigName);
	}


	public void setCurrentCompId(String id) {
		mCurrentCompId= id;

	}

	public String getCurrentParamConfigId() {
		return mCurrentParamConfigId;		
	}
	

	/**
	 * Creates a new parameter configuration on the first entry
	 * of the base config list 
	 */
	public void createCompParamConfig(String paramConfigId) {
		// check if there is already a configuration with the same name
		if (mCurrentParamConfigId.equals(paramConfigId)) {
 			deleteCurrentCompParamConfig();
		}
		
	
		// specify the base configuration 
		if (mCurrentBaseConfigName == null) {
			// initialize the base list
			IListModel model = getBaseConfigListModel();
			
		}
		mConfigManipulator.getCompConfig(mCurrentCompId).createParamConfig(paramConfigId, mCurrentBaseConfigName);
	}
	
	/**
	 * Deletes a parameter configuration list
	 */
	public void deleteCurrentCompParamConfig() {
		// specify the base configuration 
		mConfigManipulator.getCompConfig(mCurrentCompId).deleteParamConfig(mCurrentParamConfigId);
	}	

	public ComponentDetailTableModel getCompDetailTableModel() {
		return mTableModel;	
	}
	
	public void updateConfigController(String paramConfigId) {
		
		mIsCurrentParamConfigNew = false;
		mCurrentParamConfigId= paramConfigId;
	
		updateCompDetailTable(paramConfigId);
		initCompDetailTableView();		
		// check is test class available
		updateTestConfig();
		// init base config list 
		CompDataManipulator.CompConfig compConfig = mConfigManipulator.getCompConfig(mCurrentCompId);		
		CompDataManipulator.ParamConfig paramConfig = 	compConfig.getParamConfig(paramConfigId);
		//	set the current base config
		if (paramConfig != null) {
			setCurrentBaseParamConfigId(paramConfig.getBaseConfigId());
		}
	}
	
	/**
	 * Updates the component test functionality
	 */
	private void updateTestConfig() {

		mCurrentTestName = null;
		mIsTestAvailable = false;
		mCompTests = null;
		XCMAdminComponentMetadata cmpMetaData = XCMAdminInitHandler.getComponentMetadata(mCurrentCompId);
		if (cmpMetaData == null)
			return;
			

		String compTestClass = cmpMetaData.getTestclass();
		
		if (compTestClass == null) {
			mIsTestAvailable = false;
			mCompTests = null;
			return;
		} else 
			mIsTestAvailable = true;

		// instantiate class
		try {
			Class testClass = Class.forName(compTestClass);
			mCompTests = (ComponentTest)testClass.newInstance();
		} catch (ClassNotFoundException cnfex) {
			log.error(LogUtil.APPS_USER_INTERFACE_CONTROLLER,"system.xcm.admin.exception", new Object[] {}, cnfex);
		} catch (IllegalAccessException iaex) {
			log.error(LogUtil.APPS_USER_INTERFACE_CONTROLLER,"system.xcm.admin.exception", new Object[] {}, iaex);
		} catch (InstantiationException iex) {
			log.error(LogUtil.APPS_USER_INTERFACE_CONTROLLER,"system.xcm.admin.exception", new Object[] {}, iex);
		}
		
	}
	
	/**
	 * Updates the table model. The data for the table is
	 * retrieved form the component data manipulator
	 * If paramConfigId is null or empty string then 
	 * an empty table is initialized
	 */
	private void updateCompDetailTable(String paramConfigId) {
		if (log.isDebugEnabled())
			log.debug("Updating configuration table, [component id]='" + mCurrentCompId +
			"' [param config id]='" + paramConfigId + "'");

		CompDataManipulator.CompConfig compConfig = mConfigManipulator.getCompConfig(mCurrentCompId);
		if (compConfig == null) {
			if (log.isDebugEnabled())
				log.debug("Component configuration [id]='" + mCurrentCompId + "' not found");
			return;
		}


		CompDataManipulator.ParamConfig paramConfig = 	compConfig.getParamConfig(paramConfigId);

		mTableModel = new ComponentDetailTableModel(paramConfig);
		mTableModel.setReadOnly(!editable);
	}

	public TableView getCompDetailTableView() {
		if (mConfDataTableView == null)
			initCompDetailTableView();

		mConfDataTableView.setModel(mTableModel);
		
		return mConfDataTableView;
	}
	
	/**
	 * Writes the data entered by the user back to the XML docuemnts
	 * The data is retrieved from the currently active table model
	 */
	public void saveTableModel() {

		CompDataManipulator.CompConfig compConfig = mConfigManipulator.getCompConfig(mCurrentCompId);
		if (compConfig == null) {
			log.debug("Component configuration for [id]='" + mCurrentCompId + "' not found");
			return;
		}
		CompDataManipulator.ParamConfig paramConfig = compConfig.getParamConfig(mCurrentParamConfigId);

		if (paramConfig == null) {
			log.debug("Parameter configuration for [id]='" + mCurrentParamConfigId + "' not found");
			return;
		}


		int rowCount = mTableModel.getRowCount();	
		for (int i = 0; i <= rowCount; i++) {
			String  value = mTableModel.getValueAt(i, ComponentDetailTableModel.COL_INDEX_PARAM_VALUE).getValueAsString();
			String  name = mTableModel.getValueAt(i, ComponentDetailTableModel.COL_INDEX_PARAM_NAME).getValueAsString();
			paramConfig.setValue(name, value);
		}
	}
	
	/**
	 * Inits the table view
	 */
	private void initCompDetailTableView() {
		mConfDataTableView = new TableView(CompConfigForm.PARAM_TABLE_VIEW_ID);
		mConfDataTableView.setModel(mTableModel);
		mConfDataTableView.setHeaderText("Data");
		mConfDataTableView.setHeaderVisible(true);
		mConfDataTableView.setWidth("100%");
		mConfDataTableView.setFillUpEmptyRows(true);
		mConfDataTableView.getColumn(2).setWidth("20%");
		
		if (isAccessible()) {
			mConfDataTableView.setVisibleRowCount(mConfDataTableView.getRowCount());
		} else {
		
			mConfDataTableView.setVisibleRowCount(20);
		}

		mConfDataTableView.setOnNavigate(ComponentDetailTableModel.ON_NAVIGATE_EVENT);
		mConfDataTableView.setCellRenderer(new ComponentDetailCellRenderer(this, mTableModel));
		
		if (!isAccessible()){ 
			for (int i = 0; i <= mTableModel.getRowCount(); i++) {
				if (!mTableModel.isDerived(i)) {
					mConfDataTableView.setStyleForCell(i, 1, TableCellStyle.CRITICALVALUE_LIGHT);
					mConfDataTableView.setStyleForCell(i, 2, TableCellStyle.CRITICALVALUE_LIGHT);
					mConfDataTableView.setStyleForCell(i, 3, TableCellStyle.CRITICALVALUE_LIGHT);				
					mConfDataTableView.setStyleForCell(i, 4, TableCellStyle.CRITICALVALUE_LIGHT);				
					mConfDataTableView.setStyleForCell(i, 5, TableCellStyle.CRITICALVALUE_LIGHT);				
					mConfDataTableView.setStyleForCell(i, 6, TableCellStyle.CRITICALVALUE_LIGHT);
				}
				if(mTableModel.getStatus(i).equalsIgnoreCase(XCMAdminComponentMetadata.OBSOLETE)){
					mConfDataTableView.setStyleForCell(i, 1, TableCellStyle.TOTAL);
					mConfDataTableView.setStyleForCell(i, 2, TableCellStyle.TOTAL);
					mConfDataTableView.setStyleForCell(i, 3, TableCellStyle.TOTAL);				
					mConfDataTableView.setStyleForCell(i, 4, TableCellStyle.TOTAL);				
					mConfDataTableView.setStyleForCell(i, 5, TableCellStyle.TOTAL);				
					mConfDataTableView.setStyleForCell(i, 6, TableCellStyle.TOTAL);
				}
			}
		}
	}
	

	/**
	 * Returns a model used to show the base configuration
	 */
	public IListModel getBaseConfigListModel() {
		DefaultListModel listBoxModel = new DefaultListModel();
		String id = null;
		try {
			listBoxModel.setSingleSelection(true);
	
			CompDataManipulator.CompConfig currentComp = mConfigManipulator.getCompConfig(mCurrentCompId);
			Map currentParamConfigs = currentComp.getParamConfigs();
			String derivedId = null;
			for (Iterator iter = currentParamConfigs.keySet().iterator(); iter.hasNext(); ) {
				id = (String)iter.next();
				CompDataManipulator.ParamConfig paramConfig = 
					(CompDataManipulator.ParamConfig)currentParamConfigs.get(id);
				if (!paramConfig.isDerivedCompConfig()) {
					// do not add obsolete param configs
					if (XCMUtils.isCompConfigObsolete(currentComp.getCompId(), paramConfig.getId()) && isCurrentParamConfigNew())
						continue;
					listBoxModel.addItem(id, id);
					if (derivedId == null)
						derivedId = id;
				}
			}

			// if no base selected, use first derived entry			
			if (mCurrentBaseConfigName == null && derivedId != null)
				mCurrentBaseConfigName = derivedId;
			  
			if (mCurrentBaseConfigName == null || mCurrentBaseConfigName.length() == 0) {
				listBoxModel.addItem("empty", "");
				listBoxModel.setSelection("empty");
			} else 
				listBoxModel.setSelection(mCurrentBaseConfigName);
			
			
			
		} catch (Throwable t) {
			log.error(LogUtil.APPS_USER_INTERFACE_CONTROLLER,"system.xcm.admin.exception", new Object[] {}, t);
		}		
		return listBoxModel;		
	}
	
	/**
	 * Returns a model used to show the base configuration
	 */
	public IListModel getTestListModel() {
		DefaultListModel listBoxModel = new DefaultListModel();
		try {
			listBoxModel.setSingleSelection(true);
	
			if (mCompTests == null)
				return listBoxModel;
			String testName = "";
			for (Iterator iter = mCompTests.getTestNames().iterator(); iter.hasNext(); ) {
				testName = (String)iter.next();
				listBoxModel.addItem(testName, testName);
			}
			
			if (mCurrentTestName == null) {
				mCurrentTestName = testName;
			}
			listBoxModel.setSelection(mCurrentTestName);
		} catch (Throwable t) {
			log.error(LogUtil.APPS_USER_INTERFACE_CONTROLLER,"system.xcm.admin.exception", new Object[] {}, t);
		}		
		return listBoxModel;		
	}
	
	/**
	 * Performs the currently active test 
	 */
	public boolean performCurrentCompTest() {
		if (mCompTests == null)
			return false;
			
		// get current properties
		CompDataManipulator.CompConfig compConfig = mConfigManipulator.getCompConfig(mCurrentCompId);
		CompDataManipulator.ParamConfig paramConfig = 	compConfig.getParamConfig(mCurrentParamConfigId);
		if (paramConfig == null)
			return false;
		Set params = paramConfig.getParamNames();
		
		if (log.isDebugEnabled()) {	
			log.debug("Performing [test]='" + mCurrentTestName + "' for [component]='" + 
					mCurrentCompId + " [paramConfigId]='" + mCurrentParamConfigId + "'");
		}
		
		Map testParams = new LinkedHashMap();
		for (Iterator iter = params.iterator(); iter.hasNext();) {
			String name = (String)iter.next();
			String value = paramConfig.getValue(name);
			testParams.put(name, value);
		}
		
		mCompTests.setTestParameters(mCurrentTestName, testParams);
		boolean testResult = mCompTests.performTest(mCurrentTestName);
		
		return testResult;
	}
	
	/**
	 * Returns a result desrciption of the current test
	 */
	public String getCurrentTestResultDescription() {
		if (mCompTests == null)
			return "";
			
		return mCompTests.getTestResultDescription(mCurrentTestName);		
	}


	/**
	 * Returns a description for the current test
	 */
	public String getCurrentTestDescription() {
		if (mCurrentTestName == null)
			return "";
		
		return mCompTests.getTestDescription(mCurrentTestName);
	}	



	/**
	 * Returns the test parameters for the current test
	 */
	public Map getCurrentTestParams() {
		if (mCurrentTestName == null)
			return Collections.EMPTY_MAP;
		
		return mCompTests.getTestParameters(mCurrentTestName);
	}	

	
	public String getCurrentTestName() {
		if (mCurrentTestName == null)
			return "";
		
		return mCurrentTestName;
	}	
	
	public void setCurrentTestName(String testName) {
		if (testName == null)
			return;

		if (log.isDebugEnabled()) {		
			log.debug("selected [test]='" + testName + "'");
		}
		
		mCurrentTestName = testName;
	} 

	public void setCurrentBaseParamConfigId(String baseConfig) {
		if (baseConfig == null)
			return;

		if (log.isDebugEnabled()) {		
			log.debug("selected [base config]='" + baseConfig + "'");
		}
		
		mCurrentBaseConfigName = baseConfig;
		 
	}

	public String getCurrentBaseParamConfigId() {
		return mCurrentBaseConfigName;
	}

	
	/**
	 * Saves the current configuration to the file system
	 */
	public String saveConfiguration() {
		try {
			mConfigManipulator.saveConfiguration();
		} catch (Exception ex) {		
			log.error(LogUtil.APPS_USER_INTERFACE_CONTROLLER,"system.xcm.admin.exception", new Object[] {"Error saving configuration"}, ex);
			return ex.toString();			
		}
		
		isCurrentParamConfigNew(false);
		return Constants.OK;
	}

	/**
	 * This invalidates the current controller context
	 */
	public void invalidate() {
		/*
		try {
			mConfigManipulator.invalidateConfiguration();
		} catch (Exception ex) {
			log.error(LogUtil.APPS_USER_INTERFACE_CONTROLLER,"system.xcm.error",new Object[]{ex},ex);			
		}*/
		
		if(!(mCurrentParamConfigId == null || mCurrentParamConfigId.equals(""))) {
			if (mIsCurrentParamConfigNew)
				resetController();
			else
				updateConfigController(mCurrentParamConfigId);
		}	
	}
	
	
	/**
	 * Resets controller
	 */
	public void resetController() {
		if (mIsCurrentParamConfigNew)
			deleteCurrentCompParamConfig();
		mIsSAP = false;
		mIsTestAvailable = false;
		mCurrentCompId = "";
		mCurrentParamConfigId = "";	
		mTableModel = null;
		mCompTests = null;
		mCurrentTestName = null;	
		mCurrentBaseConfigName = null;	
		mIsCurrentParamConfigNew = false;
		
	}
	
	/**
	 * Restores value of component configuration paramter
	 * @param paramName parameter name
 *
	 */
	public String restoreParameterBaseValue(String paramName) {
		
		CompDataManipulator.ParamConfig paramConfig = mConfigManipulator.getCompConfig(mCurrentCompId).getParamConfig(mCurrentParamConfigId);
		
		if (paramConfig != null) 
			return paramConfig.restoreValue(paramName);
		else 
			return "";
	}
	/**
	 * @return
	 */
	public boolean isEditable() {
		return editable;
	}

	/**
	 * @param b
	 */
	public void setEditable(boolean b) {
		editable = b;
		if(mTableModel != null)
			mTableModel.setReadOnly(!editable);
	}

	/**
	 * @return
	 */
	public boolean isAccessible() {
		return accessible;
	}

	/**
	 * @param b
	 */
	public void setAccessible(boolean b) {
		accessible = b;
	}

}
