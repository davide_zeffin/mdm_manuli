package com.sap.isa.core.xcm.admin.controller;

import java.util.Map;
import java.util.Set;

import com.sap.isa.core.xcm.admin.ui.MonitoringFileTableCellRenderer;
import com.sapportals.htmlb.table.TableView;
import com.sapportals.htmlb.enum.TableViewDesign;
import com.sapportals.htmlb.DefaultListModel;
import com.sap.isa.core.cache.Cache;
import java.util.Iterator;
import com.sap.isa.core.xcm.XCMConstants;
import java.util.StringTokenizer;
import com.sap.isa.core.xcm.admin.model.table.MonitoringFileTableModel;
import com.sap.isa.core.xcm.admin.model.table.MonitoringAttributesTableModel;
import com.sap.isa.core.xcm.ConfigContainer;
import java.util.*;
import org.w3c.dom.Document;


import com.sap.isa.core.xcm.xml.ParamsStrategy;

/**
 * Used for scenario management
 */
public class MonitoringController {
	//MonitoringListModel mListModel = new MonitoringListModel();
	private DefaultListModel listBoxModel;
	
	private TableView fileTableView;
	private TableView attributesTableView;
	private Cache.Access access;
	private Map paramsMap;
	private Map configsMap;
	private Map keysMap;
	private Map noConfigContainerMap;
	private String listBoxPreSelection;
	private String noConfigContainer_key;
	
	//private Map filesMap;

	public MonitoringController() {
		listBoxModel = new DefaultListModel();

	}

	public void setListModel() {
		try {
			access = Cache.getAccess(XCMConstants.CACHE_REGION_NAME);
			Set keys = access.getKeys();
			Iterator keysIterator = keys.iterator();
			String key = null;
			String scenario = null;
			String tmp_key = null;
			paramsMap = new LinkedHashMap();
			configsMap = new LinkedHashMap();
			keysMap = new LinkedHashMap();
			noConfigContainerMap = new LinkedHashMap();
			ConfigContainer cc = null;
			Document domDoc = null;
			List tmp_listBoxModel = new LinkedList();

			while (keysIterator.hasNext()) {
				key = (String) keysIterator.next();
				Object storedConfig = access.get(key);
				if (storedConfig instanceof ConfigContainer) {
					Map l_paramsMap;
					Map l_configsMap;
					cc = (ConfigContainer) storedConfig;
					ParamsStrategy.Params params = cc.getConfigParams();
					l_paramsMap = params.getParamMap();
					l_configsMap = cc.getConfigs();
					scenario = l_paramsMap.get("scenario").toString();
					tmp_listBoxModel.add(scenario);
					paramsMap.put(scenario, l_paramsMap);
					configsMap.put(scenario, l_configsMap);
					keysMap.put(scenario,key);
				
					
				} else {
					StringTokenizer st = new StringTokenizer(key, "#");
					while (st.hasMoreTokens()) {
						tmp_key = (String) st.nextElement();
						if (tmp_key.startsWith("scenario")) {
							scenario = tmp_key.replaceFirst("scenario:", "");
						}
					}
					tmp_listBoxModel.add(scenario);
					noConfigContainerMap.put(scenario, key);
				}
			}
			//sort
			Comparator comparator = Collections.reverseOrder();
			Collections.sort(tmp_listBoxModel);
			
			Iterator i = tmp_listBoxModel.iterator();
			
			while(i.hasNext()){
				
				String arg0 = i.next().toString();
				listBoxModel.addItem(arg0, arg0);
			}
			listBoxModel.setSingleSelection(true);

			
		} catch (Cache.Exception e) {
			listBoxModel = null;
		}
		
		
	}

	public void setTableViews(String scenario) {

		if (noConfigContainerMap.containsKey(scenario)){
			fileTableView = null;
			attributesTableView = null;
			noConfigContainer_key=noConfigContainerMap.get(scenario).toString();
			return;
		}

		Map l_configsMap = (Map) configsMap.get(scenario);
		String l_key = (String) keysMap.get(scenario);
		MonitoringFileTableModel mFileTableModel =
			new MonitoringFileTableModel(l_configsMap, l_key);
		fileTableView = new TableView("fileTableView");
		fileTableView.setModel(mFileTableModel);
		fileTableView.setCellRenderer(
			new MonitoringFileTableCellRenderer(this, mFileTableModel));
		fileTableView.setWidth("100%");
		fileTableView.setDesign(TableViewDesign.ALTERNATING);
		fileTableView.setFooterVisible(false);
		fileTableView.setHeaderText("XCM Managed Configuration Files");

		attributesTableView = new TableView("attributesTableView");
		Map params = (Map) paramsMap.get(scenario);
		MonitoringAttributesTableModel mAttributesTableModel =
			new MonitoringAttributesTableModel(params);
		attributesTableView.setModel(mAttributesTableModel);
		attributesTableView.setWidth("100%");
		attributesTableView.setDesign(TableViewDesign.ALTERNATING);
		attributesTableView.setFooterVisible(false);
		attributesTableView.setHeaderText("XCM Parameters");

	}

	public TableView getFilesTableView() {
		return fileTableView;
	}

	public TableView getAttributesTableView() {
		return attributesTableView;
	}

	public String getListBoxPreSelection() {
		String retVal = listBoxPreSelection;
		listBoxPreSelection = null;
		return retVal;
	}

	public void setListBoxPreSelection(String selection) {
		listBoxPreSelection = selection;
	}
	
	public DefaultListModel getListModel(){
		return listBoxModel;
	}
	
	public void invalidate(){
		listBoxModel = new DefaultListModel();
		fileTableView=null;
		attributesTableView=null;
		access=null;
		configsMap=null;
		keysMap=null;
		noConfigContainerMap=null;
		listBoxPreSelection=null;
	}

	public String getNoConfigContainerKey(){
		return noConfigContainer_key;
	}
}
