/*
 * Created on Nov 22, 2004
 */
package com.sap.isa.core.xcm.admin.controller;

import java.sql.Connection;
import java.util.Properties;

import javax.sql.DataSource;

import com.sap.isa.core.db.DBHelper;
import com.sap.isa.core.locking.LockManager;
import com.sap.isa.core.locking.LockManagerFactory;
import com.sap.isa.core.locking.enqueue.EnqueueLockManager;
import com.sap.isa.core.locking.enqueue.EnqueueLockManagerFactory;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.xcm.XCMConstants;

/**
 * @author I802891
 */
public class LockingController {
	protected static IsaLocation log = IsaLocation.
		getInstance(AppConfigController.class.getName());
	
	private static final Properties sProps = new Properties();
	private static LockManagerFactory mLMF;
	
	private LockManager mLM;
	private Connection mConn;
		
	static {
		sProps.setProperty(DBHelper.DB_DATASOURCE,XCMConstants.DATASOURCE_LOOKUP_NAME);
		Properties props = new Properties();
		props.setProperty(DBHelper.LMF,EnqueueLockManagerFactory.class.getName());
		mLMF = DBHelper.getLockManagerFactory(props);
	}
	private static DataSource getDataSource() {
		DataSource ds = DBHelper.getDataSource(sProps);
		return ds;
	}
	
	public LockManager getLockManager() {
		return mLM;
	}
	
	public void initLockManager() throws Exception {
		if(this.mLM != null && !this.mLM.isClosed()) return;
		LockManager lm = mLMF.getLockManager();
		// obtain a separate Database connection for underlying locks obtainer by this Lock Manager
		DataSource ds = getDataSource();
		mConn = ds.getConnection();
		((EnqueueLockManager)lm).setConnection(mConn);
		this.mLM = lm;
	}

	public void closeLockManager() throws Exception {
		if(this.mLM == null || this.mLM.isClosed()) return;
		
		Exception error = null;		
		try {
			this.mLM.close();
			this.mConn.close();
		}
		catch(Exception ex) {
			log.error(LogUtil.APPS_USER_INTERFACE_CONTROLLER,ex.getMessage(),ex);
			error = ex;
		}
		
		this.mLM = null;
		this.mConn = null;
		if(error != null) {
			log.throwing(error);
			throw error;
		} 
	}	
}
