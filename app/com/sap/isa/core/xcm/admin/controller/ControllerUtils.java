package com.sap.isa.core.xcm.admin.controller;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.xcm.XCMUtils;
import com.sap.isa.core.xcm.admin.Constants;
import com.sap.isa.core.xcm.admin.actionform.ScenarioConfigForm;
import com.sap.isa.core.xcm.admin.model.CompDataManipulator;
import com.sap.isa.core.xcm.admin.model.ScenarioDataManipulator;
import com.sap.isa.core.xcm.admin.model.table.ScenarioDetailTableModel;
import com.sap.isa.core.xcm.init.XCMAdminAllowedValueMetadata;
import com.sap.isa.core.xcm.init.XCMAdminInitHandler;
import com.sap.isa.core.xcm.init.XCMAdminParamConstrainMetadata;
import com.sap.isa.core.xcm.scenario.ParamConstrainConfig;
import com.sap.isa.core.xcm.scenario.ScenarioConfig;
import com.sap.isa.core.xcm.scenario.ScenarioManager;
import com.sapportals.htmlb.rendering.IPageContext;
import com.sapportals.htmlb.table.TableView;
import com.sapportals.htmlb.type.AbstractDataType;

public class ControllerUtils {

	protected static IsaLocation log =
						   IsaLocation.getInstance(ControllerUtils.class.getName());


	/**
	 * Returns description for value of config param
	 */
	public static String getScenarioConfigParamValueShorttext(String configParam, String configValue, CompDataManipulator compMan) {
		XCMAdminParamConstrainMetadata scenMetaData = XCMAdminInitHandler.getXCMAdminScenarioMetadata(configParam);
		if (scenMetaData == null) {
			String msg = "Configuration parameter [name]='" + configParam + "' not found"; 
			log.warn("system.xcm.warn", new Object[] { msg }, null);
			return "";
		} 
	
		// check if this parameter is configured using component
		String compId = scenMetaData.getComponent();

		// check if default configuration or derived one
		if (compId != null && compId.length() > 0) {
			CompDataManipulator.CompConfig compConfig = compMan.getCompConfig(compId);
			if (compConfig == null) {
				String msg = "Meta data for [component]='" + compId + "' not found"; 
				log.warn("system.xcm.warn", new Object[] { msg }, null);
				return msg;
			}
				
			CompDataManipulator.ParamConfig paramConfig = compConfig.getParamConfig(configValue);
			if (paramConfig != null && paramConfig.isDerivedCompConfig()) {
				// get base config
				configValue = paramConfig.getBaseConfigId();
				String baseDescr = "User defined configuration based on '" + configValue + "' configuration.";  
				return baseDescr;
				
			}
		}
		
		return getText(scenMetaData.getAllowedValueShorttext(configValue));
	}

	
	
	
	public static List getScenarioParamAllowedValues(String scenName, String paramName, ScenarioDataManipulator scenMan, CompDataManipulator compMan) {

		Set fixAllowedValues = Collections.EMPTY_SET;
		
		List metaDataValues = Collections.EMPTY_LIST; 
		
		try {
	
		// check if there are static param constrains defined
		// directly in scenario-config.xml
		ScenarioDataManipulator.ParamConstrain currentConstrain = 
			scenMan.getScenarioConfig(scenName).getParamConstrain(paramName);
		if (currentConstrain != null && currentConstrain.getAllowedValues().size() > 0 ) {
			fixAllowedValues =  currentConstrain.getAllowedValues();
			// check if all overwritten values have meta data
			for (Iterator iter = fixAllowedValues.iterator(); iter.hasNext();) {
				String allowedValue = (String)iter.next();
			}
		}

		metaDataValues = getScenarioAllowedValuesMetaData(scenName, paramName, fixAllowedValues, scenMan, compMan);
		} catch (Throwable t) {
			log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.xcm.admin.exception", new Object[] { }, t);
		}

		return metaDataValues;

		}
	

	/** 
	 * Returns a map containing allowed values for the 
	 * given parameter name
	 */
	private static List getScenarioAllowedValuesMetaData(String scenName, String paramName, Set overwrittenValues, ScenarioDataManipulator scenMan, CompDataManipulator compMan) {
		
		Set checkValues = new LinkedHashSet(overwrittenValues); 
		 
		if (log.isDebugEnabled())
			log.debug("Getting allowed values: [param name]='" + paramName + "'");

		List allowedValues = new LinkedList(overwrittenValues);

			ScenarioDataManipulator.ScenarioConfig scenConfig = 
		scenMan.getScenarioConfig(scenName);
							
			XCMAdminParamConstrainMetadata metaData = XCMAdminInitHandler.getXCMAdminScenarioMetadata(paramName);
			if (metaData == null)
				return Collections.EMPTY_LIST;
			Map allowedValuesMetaData = metaData.getAllowedValueMetaData();
			for (Iterator iter = allowedValuesMetaData.values().iterator(); iter.hasNext(); ) {
				XCMAdminAllowedValueMetadata md = (XCMAdminAllowedValueMetadata)iter.next();
				String paramValue = md.getValue();
				String paramType = md.getType();
				
				if (paramType.equals(XCMAdminAllowedValueMetadata.VALUE_TYPE_FIX)) {
					if (overwrittenValues.size() == 0) {
						// add this value if there are not overwritten values
						allowedValues.add(paramValue);
					} else {
						checkValues.remove(paramValue);
					}
				}
				if (paramType.equals(XCMAdminAllowedValueMetadata.VALUE_TYPE_BASE)||
				paramType.equals(XCMAdminAllowedValueMetadata.VALUE_TYPE_DERIVED)) {
					// check if there are contrains for this value
					if (overwrittenValues.size() > 0 && !overwrittenValues.contains(paramValue))
						continue;
					
					checkValues.remove(paramValue);
					
					String compId = metaData.getComponent();
					CompDataManipulator.CompConfig compConfig = compMan.getCompConfig(compId);
					if (compConfig == null) {
						String msg = "No component found for configuratin parameter [name]='" + paramName + "' for parameter [value]='" + paramValue + "'" ; 
						log.error(LogUtil.APPS_USER_INTERFACE,"system.xcm.admin.exception", new Object[] { msg }, null);
						
					}
					allowedValues.add(Constants.CREATE_NEW_CONFIG_ID);
					Map descParams = compConfig.getDescendendParamConfigs(paramValue);
					for (Iterator iter2 = descParams.keySet().iterator(); iter2.hasNext(); ) {
						String descValue = (String)iter2.next();
						allowedValues.add(descValue);		
					}
					if (paramType.equals(XCMAdminAllowedValueMetadata.VALUE_TYPE_BASE))
						allowedValues.add(paramValue);
					
				}

			}
	
		// check if there are overwritten contrains which do not match the central contrains
		for (Iterator iter = checkValues.iterator(); iter.hasNext(); ) {
			String invalidValue = (String)iter.next();
			String msg = "meta data missing for the scenario configuration parameter [name]='" + paramName 
			+ "' [paramValue]='" + invalidValue + "' Check xcmadmin-config.xml";
			log.error(LogUtil.APPS_USER_INTERFACE,"system.xcm.admin.exception", new Object[] { msg }, null);
		}
	
		return allowedValues;
	}

	
	private static String getText(String text) {
		if (text == null)
			return "";
		else
			return text;
	}
	
	public static String getScenarioParamDependentComponentName(String paramName) {
		XCMAdminParamConstrainMetadata metaData = XCMAdminInitHandler.getXCMAdminScenarioMetadata(paramName);
		if (metaData == null)
			return null;
		String compId = metaData.getComponent();
		return compId;	
	}
	
	public static void saveCurrentScenarioParams(IPageContext pageContext, ScenarioConfigController scenConfig) {
		TableView tableView = (TableView)pageContext.getComponentForId(ScenarioConfigForm.SCENARIO_TABLE_VIEW_ID);

		int firstVisRow = tableView.getVisibleFirstRow();
		int visibleRowCount = tableView.getVisibleRowCount();

		int lastRowModel = visibleRowCount + firstVisRow - 1;

		ScenarioDetailTableModel model = scenConfig.getCompDetailTableModel();		
		try {

			if (lastRowModel > model.getRowCount())
				lastRowModel = model.getRowCount();
	
			int row = firstVisRow;
			int loopcount = 0;
			for (int i = firstVisRow; i <= lastRowModel; i++) {
				loopcount++;
				AbstractDataType value = pageContext.getDataForComponentId(ScenarioConfigForm.SCENARIO_TABLE_VIEW_ID, ScenarioConfigForm.PARAM_VALUE_LIST_BOX_ID, loopcount);
				model.setValueAt(value, i, 2);
			}
		} catch (Throwable t) {
			log.error(LogUtil.APPS_USER_INTERFACE,"system.xcm.admin.exception", new Object[] {}, t);
		}
	}

	public static boolean idObsolete(String scenName) {

		ScenarioConfig scenConfig = ScenarioManager.getScenarioManager().getScenarioConfig(scenName);
		if (scenConfig != null) {
			return scenConfig.isObsolete();
		}
		return false;
	}

	public static String getScenConfigParamShorttext(String scenName, String configParam) {

		// check if shorttext was overwritten on scenario level
		ScenarioConfig scenConfig = ScenarioManager.getScenarioManager().getScenarioConfig(scenName);
		if (scenConfig != null) {
			Map constrains = scenConfig.getParamConstrains();
			if (constrains != null) {
				ParamConstrainConfig pcc = (ParamConstrainConfig)constrains.get(configParam);	
				if (pcc != null) {
					if (pcc.getShorttext() != null)
						return pcc.getShorttext();
				}
			}
		}
		return XCMUtils.getScenarioConfigParamShorttext(configParam);
	}


	public static String getScenConfigParamLongtext(String scenName, String configParam) {

		// check if shorttext was overwritten on scenario level
		ScenarioConfig scenConfig = ScenarioManager.getScenarioManager().getScenarioConfig(scenName);
		
		if (scenConfig != null) {
			Map constrains = scenConfig.getParamConstrains();
			if (constrains != null) {
				ParamConstrainConfig pcc = (ParamConstrainConfig)constrains.get(configParam);	
				if (pcc != null) {
					if (pcc.getLongtext() != null)
						return pcc.getLongtext();
				}
			}
		}
		return XCMUtils.getScenarioConfigParamLongtext(configParam);
	}



}
