package com.sap.isa.core.xcm.admin.controller;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.xcm.XCMUtils;
import com.sap.isa.core.xcm.admin.model.CompDataManipulator;
import com.sap.isa.core.xcm.admin.model.HelpConstants;
import com.sap.isa.core.xcm.admin.model.ScenarioDataManipulator;
import com.sap.isa.core.xcm.admin.model.help.HelpItem;
import com.sap.isa.core.xcm.admin.model.help.HelpItemBase;
import com.sap.isa.core.xcm.admin.model.help.HelpItemContainer;
import com.sap.isa.core.xcm.admin.model.help.HelpItemContainerBase;
import com.sap.isa.core.xcm.admin.model.help.comp.CompConfigHelpContainer;
import com.sap.isa.core.xcm.admin.model.help.comp.CompConfigParamHelpContainer;
import com.sap.isa.core.xcm.admin.model.help.comp.CompDetailHelpItem;
import com.sap.isa.core.xcm.admin.model.help.comp.CompParamAllowedValuesContainer;
import com.sap.isa.core.xcm.admin.model.help.comp.CompParamHelpContainer;
import com.sap.isa.core.xcm.admin.model.help.scenario.ScenDetailHelpItem;
import com.sap.isa.core.xcm.admin.model.help.scenario.ScenParamAllowedValuesContainer;
import com.sap.isa.core.xcm.admin.model.help.scenario.ScenParamHelpContainer;
import com.sap.isa.core.xcm.admin.model.table.HelpItemSummaryTableModel;
import com.sap.isa.core.xcm.admin.ui.HelpSummaryCellRenderer;
import com.sap.isa.core.xcm.init.XCMAdminParamConstrainMetadata;
import com.sapportals.htmlb.enum.CellVAlign;
import com.sapportals.htmlb.enum.TableViewDesign;
import com.sapportals.htmlb.table.TableView;
 


/**
 * Class controlls how help is displayed
 *
 */
public class HelpController {
	


	public static final int MODE_DETAIL_NONE                                = 0;
	public static final int MODE_DETAIL_SHOW_VALUE                          = 1;
	public static final int MODE_DETAIL_BASE_CONFIG_NAME                    = 2;
	public static final int MODE_DETAIL_NAME_AS_LINK_DEST       			= 4;
	public static final int MODE_DETAIL_VALUE_AS_LINK                       = 8;
	
	public static final int MODE_SUMMARY_TABLE_SHORTTEXT          = 1;
	public static final int MODE_SUMMARY_TABLE_LONGTEXT           = 2;
	public static final int MODE_SUMMARY_TABLE_NAME_AS_LINK       = 4;
	public static final int MODE_SUMMARY_TABLE_NAME_AS_LINK_DEST  = 8;
	public static final int MODE_SUMMARY_TABLE_NAME_AS_SCEN_LINK  = 16;
	public static final int MODE_SUMMARY_TABLE_NAME_AS_COMP_LINK  = 32;


	protected static IsaLocation log = IsaLocation.
		getInstance(AppConfigController.class.getName());



	private static final String SAP_SCEN_SUMMARY_TABLE_MODEL_ID = "SAP_SCEN_SUMMARY_TABLE_MODEL_ID";
	private static final String CUST_SCEN_SUMMARY_TABLE_MODEL_ID = "CUST_SCEN_SUMMARY_TABLE_MODEL_ID";

	// scenario summary container
	private HelpItemContainerBase mSAPScenContainer = new HelpItemContainerBase("SAP Scenario");
	private HelpItemContainerBase mCustScenContainer = new HelpItemContainerBase("Customer Scenario");

	private HelpItemContainerBase mCompContainerApplScope = new HelpItemContainerBase("Components");
	private HelpItemContainerBase mCompContainerScenScope = new HelpItemContainerBase("Components");

	private String mCurrentLinkPrefix;
	private String mCurrentLinkDestPrefix;

	// scenario detail
	private Map mHelpItemsScenSAP = new LinkedHashMap();	
	private Map mHelpItemsScenCust = new LinkedHashMap();

	// component detail
	private Map mHelpItemsComponents = new LinkedHashMap();
	
	// scenario parameter 
	private Map mScenParamManContainer = new LinkedHashMap();
	private Map mScenParamOptContainer = new LinkedHashMap();

	// component parameter
	private Map mComponentParameter = new LinkedHashMap();
	
	// scenario parameter allowed values container
	
	// scenario summary table views 
	private TableView mSAPScenSumTableView;
	private TableView mCustScenSumTableView;
	
	// component summary table views
	private TableView mSAPCompSumTableView;
	private TableView mCustCompSumTableView;



	private int mCurrentDetailMode;
	private HelpItem mCurrentHelpItem;
	private HelpItemContainer mCurrentHelpItemContainer;
	private int mCurrentSummaryMode;

	private String mCurrentScenarioName;
	private String mCurrentComponentName;
	
	private CompDataManipulator mCompDataMan;
	private ScenarioDataManipulator mScenDataMan;
	
	public void setCurrentScenarioName(String scenName) {
		mCurrentScenarioName = scenName;
	}

	public void setCurrentComponentName(String compName) {
		mCurrentComponentName = compName;
	}

	public String getCurrentComponentName() {
		return mCurrentComponentName;
	}

	
	public String getCurrentScenarioName() {
		return mCurrentScenarioName;
	}
	
	public void setCurrentDetailMode(int mode, String linkPrefix, String linkDestPrefix) {
		mCurrentLinkPrefix = linkPrefix;
		mCurrentLinkDestPrefix = linkDestPrefix;
		mCurrentDetailMode = mode;
	}

	public boolean checkCurrentDetailMode(int checkMode) {
		return (mCurrentDetailMode & checkMode) == checkMode; 
	}
	
	public int getCurrentDetailMode() {
		return mCurrentDetailMode;
	}
	
	public void setCurrentDetailItem(HelpItem item) {
		mCurrentHelpItem = item;
	}

	public HelpItem getCurrentDetailItem() {
		return mCurrentHelpItem;
	}
	
	public void setCurrentSummaryMode(int mode, String linkPrefix, String linkDestPrefix) {
		mCurrentLinkDestPrefix = linkDestPrefix;
		mCurrentLinkPrefix = linkPrefix;
		mCurrentSummaryMode = mode;
	}

	public boolean checkCurrentSummaryMode(int checkMode) {
		return (mCurrentSummaryMode & checkMode) == checkMode; 
	}
	
	public int getCurrentSummaryMode() {
		return mCurrentSummaryMode;
	}
	
	public void setCurrentSummaryContainer(HelpItemContainer container) {
		mCurrentHelpItemContainer = container;
	}
	
	public HelpItemContainer getCurrentSummaryContainer() {
		return mCurrentHelpItemContainer;
	}

//constructor
	public HelpController(ScenarioDataManipulator scenDataMan, CompDataManipulator comDataMan) {
		mScenDataMan = scenDataMan;
		mCompDataMan = comDataMan;
		try {
			initScenDetail();

			initScenParams();
			
			initCompDetail();

		} catch (Throwable t) {
			log.error(LogUtil.APPS_USER_INTERFACE_CONTROLLER,"system.xcm.admin.exception", new Object[] {}, t);
		}
		
		//initSummaryTables();
		
	}
	
	public HelpItemContainer getSAPScenSummaryContainer() {
		return mSAPScenContainer;
	}
	
	
	private void initCompDetail() {
		Set compConfigs = mCompDataMan.getCompConfigs();
		for (Iterator iter = compConfigs.iterator(); iter.hasNext();) {
			String compId = (String)iter.next();
			CompDataManipulator.CompConfig compConfig = 
				mCompDataMan.getCompConfig(compId);
			CompDetailHelpItem item = new CompDetailHelpItem(compConfig);
			if (XCMUtils.isApplicationScopeCompConfig(compId))
				mCompContainerApplScope.addItem(item);
			else	
				mCompContainerScenScope.addItem(item);
			
			mHelpItemsComponents.put(item.getName(), item);
		}
	}
	
	

	private void initScenDetail() {
		// get list of scenarios
		Map mScenConfigs = mScenDataMan.getScenarioConfigs();
		
		for(Iterator iter = mScenConfigs.keySet().iterator(); iter.hasNext();) {
			String scenName = (String)iter.next();
			addScenDetail(scenName);
		}
	}

	private HelpItem addScenDetail(String scenName) {

			Map mScenConfigs = mScenDataMan.getScenarioConfigs();
			ScenarioDataManipulator.ScenarioConfig scenConfig = (ScenarioDataManipulator.ScenarioConfig)mScenConfigs.get(scenName);
			
			HelpItem scenDetail = new ScenDetailHelpItem("scenDetail", scenName, scenConfig.getBaseConfigName(), scenConfig);						
			if (!scenConfig.isScenarioDerived()) {
				mSAPScenContainer.addItem(scenDetail);
				mHelpItemsScenSAP.put(scenDetail.getName(),scenDetail);
			} else {
				mCustScenContainer.addItem(scenDetail);
				mHelpItemsScenCust.put(scenDetail.getName(),scenDetail);
			}
			return scenDetail;
		}
	 

	private void initScenParams() {
		// customer scenarios
		for (Iterator iter = mHelpItemsScenCust.keySet().iterator(); iter.hasNext();) {
			String scenName = (String)iter.next();

			ScenarioDataManipulator.ScenarioConfig scenConfig = mScenDataMan.getScenarioConfig(scenName);
			if(scenConfig == null)
				continue;
			ScenParamHelpContainer scenParamOptHelpContainer = 
				new ScenParamHelpContainer("Optional Scenario Parameter", XCMAdminParamConstrainMetadata.TYPE_OPTIONAL, scenConfig, mCompDataMan);
			ScenParamHelpContainer scenParamManHelpContainer = 
				new ScenParamHelpContainer("Mandatory Scenario Parameter", XCMAdminParamConstrainMetadata.TYPE_MANDATORY, scenConfig, mCompDataMan);
			mScenParamManContainer.put(scenName, scenParamManHelpContainer);
			mScenParamOptContainer.put(scenName, scenParamOptHelpContainer);
		}
		// SAP scenarios
		for (Iterator iter = mHelpItemsScenSAP.keySet().iterator(); iter.hasNext();) {
			String scenName = (String)iter.next();
			ScenarioDataManipulator.ScenarioConfig scenConfig = mScenDataMan.getScenarioConfig(scenName);
			ScenParamHelpContainer scenParamOptHelpContainer = 
				new ScenParamHelpContainer("Optional Scenario Parameter", XCMAdminParamConstrainMetadata.TYPE_OPTIONAL, scenConfig, mCompDataMan);
			ScenParamHelpContainer scenParamManHelpContainer = 
				new ScenParamHelpContainer("Mandatory Scenario Parameter", XCMAdminParamConstrainMetadata.TYPE_MANDATORY, scenConfig, mCompDataMan);
			mScenParamManContainer.put(scenName, scenParamManHelpContainer);
			mScenParamOptContainer.put(scenName, scenParamOptHelpContainer);
		}		
	}
	 
	/**
	 * 
	 * @param helpItemContainer
	 * @param mode see HelpItemSummaryTableModel
	 * @param generateLink if true links are generated in the first colum:
	 * reference value: 'detail'+value
	 * @return TableView
	 */
	public TableView getCurrentSummaryTableView() {
		HelpItemSummaryTableModel model = new HelpItemSummaryTableModel(mCurrentHelpItemContainer); 
		// id has to be unique
		TableView tableView = new TableView(mCurrentHelpItemContainer.getContainerTitle() + String.valueOf(Math.random()));
		tableView.setModel(model);
		tableView.setCellRenderer(new HelpSummaryCellRenderer(model, mCurrentSummaryMode, mCurrentLinkDestPrefix, mCurrentLinkPrefix));
		tableView.setFooterVisible(false);
		tableView.setWidth("100%");
		tableView.setColumnVAlignment(1, CellVAlign.TOP);
		tableView.setDesign(TableViewDesign.ALTERNATING);
		
		return tableView;
	}

		
	/**
	 * Help container containing optional scenario params
	 * @param scenName
	 * @return HelpItemContainer
	 */
	public HelpItemContainer getScenParamOptionalContainer(String scenName) {
		return (HelpItemContainer)mScenParamOptContainer.get(scenName);
	}

	/**
	 * Help container containing mandatory scenario params
	 * @param scenName
	 * @return HelpItemContainer
	 */
	public HelpItemContainer getScenParamMandatoryContainer(String scenName) {
		return (HelpItemContainer)mScenParamManContainer.get(scenName);
	}

	/**
	 * Returns a container with allowed values for a scenario parameter
	 */
	public HelpItemContainer getScenParamAllowedValuesContainer(String scenName, String paramName) {
		return new ScenParamAllowedValuesContainer("allowedValues", scenName, paramName, mScenDataMan, mCompDataMan);
	}
	
	public HelpItem getScenDetailHelpItem(String scenName) {
		HelpItem scenDetail = (HelpItem)mHelpItemsScenSAP.get(scenName);
		if (scenDetail == null)
			scenDetail = (HelpItem)mHelpItemsScenCust.get(scenName);

		if (scenDetail == null) {
			// new scenario created
			scenDetail = addScenDetail(scenName);
			initScenParams();
		}
		// create empty detail container
		if (scenDetail == null) 
			scenDetail = new HelpItemBase(HelpConstants.TYPE_SCENARIO, "empty", "", "", "", "", "", "");

		return scenDetail;
	}
	
	public HelpItemContainer getScenParamConfigParams(String scenName) {
		return null;
	}

	public HelpItem getCompDetailHelpItem(String compId) {
		return (HelpItem)mHelpItemsComponents.get(compId);
	}

	public HelpItemContainer getGeneralScopeComponentSummaryContainer() {
		return mCompContainerApplScope;
	}

	public HelpItemContainer getConfigScopeComponentSummaryContainer() {
		return mCompContainerScenScope;
	}

	public HelpItemContainer getComponentConfigContainer(String compId) {
		CompDataManipulator.CompConfig compConfig = mCompDataMan.getCompConfig(compId);
		if (compConfig == null)
			return new HelpItemContainerBase("empty");
		
		return new CompConfigHelpContainer("Component Configuration", compConfig, compId);		
	}

	public HelpItemContainer getComponentConfigParamSummary(String compId, String paramId) {
		CompDataManipulator.CompConfig compConfig = mCompDataMan.getCompConfig(compId);
		CompDataManipulator.ParamConfig paramConfig = compConfig.getParamConfig(paramId); 
		return new CompConfigParamHelpContainer(
				"comp config summary", 
				getComponentParamContainer(compId), paramConfig);
	};
	
	public HelpItemContainer getComponentParamAllowdValueSummary(String compId, String paramId) {
		CompDataManipulator.CompConfig compConfig = mCompDataMan.getCompConfig(compId);
		CompDataManipulator.ParamConfig paramConfig = compConfig.getParamConfig(paramId); 
		return new CompParamAllowedValuesContainer(
				"comp param allowed values", 
				compId, paramId, paramConfig);
	}

	public HelpItemContainer getComponentParamContainer(String compId) {

		if (mComponentParameter.get(compId) != null)
			return (HelpItemContainer)mComponentParameter.get(compId);
		
		CompDataManipulator.CompConfig compConfig = mCompDataMan.getCompConfig(compId);
		if (compConfig == null)
			return new HelpItemContainerBase("empty");
		HelpItemContainer compParamContainer = 
			new CompParamHelpContainer("Component Parameter", compId, compConfig);
		mComponentParameter.put(compId, compParamContainer);
		return compParamContainer;
	}

	public String getCurrentLinkPrefix() {
		return mCurrentLinkPrefix;	
	}

	public String getCurrentLinkDestPrefix() {
		return mCurrentLinkDestPrefix;	
	}
	
	public void updateController() {
		try {
			initScenDetail();

			initScenParams();
			
			initCompDetail();

		} catch (Throwable t) {
			log.error(LogUtil.APPS_USER_INTERFACE_CONTROLLER,"system.xcm.admin.exception", new Object[] {}, t);
		}
		
	}
		
}
