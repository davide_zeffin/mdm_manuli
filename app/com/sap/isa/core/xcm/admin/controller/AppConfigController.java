package com.sap.isa.core.xcm.admin.controller;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.sap.isa.core.locking.LockException;
import com.sap.isa.core.locking.LockManager;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.xcm.admin.Constants;
import com.sap.isa.core.xcm.admin.model.CompDataManipulator;
import com.sap.isa.core.xcm.admin.model.ScenarioDataManipulator;
import com.sap.isa.core.xcm.admin.model.tree.ConfigTreeModel;
import com.sap.isa.core.xcm.init.XCMAdminInitHandler;
import com.sapportals.htmlb.DefaultListModel;
import com.sapportals.htmlb.IListModel;

/**
 * 
 */
public class AppConfigController {
	
	public static final String HOME_MODE                 = "home";
	public static final String OPTIONS_MODE              = "options";
	public static final String SCENARIO_MODE             = "scenario";
	public static final String SCENARIO_OVERVIEW_MODE    = "scenario_overview";	
	public static final String SCENARIO_CONFIG_MODE      = "scenario_config";
	public static final String COMPONENTS_OVERVIEW_MODE  = "components_overview";
	public static final String COMPONENT_DESCR_MODE      = "component_descr";
	public static final String COMPONENTS_CONFIG_MODE    = "components_config";	
		
	protected static IsaLocation log = IsaLocation.
		getInstance(AppConfigController.class.getName());
	
	
	private ComponentConfigController mCompConfigController;
	private ScenarioConfigController mScenConfigController;
	private String mConfigType = HOME_MODE;
	private ConfigTreeModel mTreeModel;
	private CompDataManipulator mConfigManipulator;
	private ScenarioDataManipulator mScenDataManipulator;
	private HelpController mHelpController;
	private boolean mIsScenarioCompConfig = false;
	private String mCurrentGlobalConfigName;
	private boolean accessible = false;
	private boolean mEditMode;
	private static Set mAllScenNames = null;
	private boolean mIsScenarioNameAvilable = false;
		
	public AppConfigController(String configName, 
							   CompDataManipulator confDataManipulator, 
							   ScenarioDataManipulator scenDataManipulator, boolean accessible) {
		setAccessible(accessible);
		mCurrentGlobalConfigName = configName;
		mConfigManipulator = confDataManipulator;	
		mScenDataManipulator = scenDataManipulator;
		mCompConfigController = new ComponentConfigController(confDataManipulator, isAccessible());
		mScenConfigController = new ScenarioConfigController(mConfigManipulator, scenDataManipulator, isAccessible());
		mHelpController = new HelpController(mScenDataManipulator, mConfigManipulator);
		setEditMode(false);
		// get globally used scenario names
		if (mAllScenNames == null)
			mAllScenNames = new HashSet();
		Map scenConfigs = scenDataManipulator.getScenarioConfigs();
		for (Iterator iter = scenConfigs.values().iterator(); iter.hasNext();) {
			ScenarioDataManipulator.ScenarioConfig scenConfig = (ScenarioDataManipulator.ScenarioConfig)iter.next();
			mAllScenNames.add(scenConfig.getName());
			
		}
	}
	
	
	
	public HelpController getHelpController() {
		return mHelpController;
	}
		
	public void initConfigTreeModel() {
		mTreeModel = new ConfigTreeModel("root", mConfigManipulator, mScenDataManipulator, isAccessible());
	}

	public void updateConfigTreeModel(boolean isDeleted) {
		mTreeModel.updateCompNodes(mCompConfigController.getCurrentCompId(), isDeleted);
	}


	public ConfigTreeModel getConfigTreeModel() {
		return mTreeModel;
	}
		
	public void setConfigType(String configType) {
		mConfigType = configType;	
	}
	
	public String getConfigType() {
		return mConfigType;	
		
	}

	public ComponentConfigController getCompConfigController() {
		return mCompConfigController;
	}
	
	public ScenarioConfigController getScenarioConfigController() {
		return mScenConfigController;
	}
	
	public void configCompFromScenario(String compName, String paramConfig) {
		// expand components node
		mTreeModel.expandNode(ConfigTreeModel.COMP_NODE_ID, true);
		mTreeModel.expandNode(ConfigTreeModel.SCENARIOS_NODE_ID, false);

		mCompConfigController.resetController();
		mCompConfigController.setCurrentCompId(compName);
		mCompConfigController.updateConfigController(paramConfig);


		if (paramConfig != null && paramConfig.length() > 0) {

			if (mCompConfigController.isParamConfigDerived(paramConfig)) {			
				mCompConfigController.isSAP(false);
				mTreeModel.expandNode(ConfigTreeModel.COMP_CUST_NODE_ID, true);				
			} else {
				mCompConfigController.isSAP(true);
				mTreeModel.expandNode(ConfigTreeModel.COMP_SAP_NODE_ID, true);				
			}

			// set active node
			mTreeModel.setComponentActiveNode(
					mCompConfigController.getCurrentCompId(), paramConfig, mCompConfigController.isSAP());

			mConfigType = COMPONENTS_CONFIG_MODE;
			// existing configuration
			// check if customer or SAP 
		} else {
			// create new parameter configuration
			mConfigType = COMPONENT_DESCR_MODE;
			// set active node
			mTreeModel.setComponentActiveNode(
					mCompConfigController.getCurrentCompId(), null, false);

		}
		 
		mIsScenarioCompConfig = true;
	}
	
	public boolean isConfigCompFromScenario() {
		return mIsScenarioCompConfig;
	}

	public void isConfigCompFromScenario(boolean isBack) {
		mIsScenarioCompConfig = isBack;
	}

	
	public void backToScenarioConfig() {
		// expand components node
		mTreeModel.expandNode(ConfigTreeModel.COMP_CUST_NODE_ID, false);
		mTreeModel.expandNode(ConfigTreeModel.COMP_NODE_ID, false);
		mTreeModel.expandNode(ConfigTreeModel.SCENARIOS_NODE_ID, true);

		mTreeModel.setScenarioActiveNode(
				mScenConfigController.getCurrentScenarioName(), mScenConfigController.isSAP());
		
		mConfigType = SCENARIO_CONFIG_MODE;
		
		mIsScenarioCompConfig = false;
		
	}

	/**
	 * Returns a model used to show the base configuration
	 */
	public IListModel getConfigsListModel() {
		DefaultListModel listBoxModel = new DefaultListModel();
		try {
			listBoxModel.setSingleSelection(true);
			int numConfigs = XCMAdminInitHandler.getNumConfiguarions();
			for (int i = 1; i <= numConfigs; i++) {
				String configName = XCMAdminInitHandler.getConfigName(i);
				listBoxModel.addItem(String.valueOf(i), configName);
				if (mCurrentGlobalConfigName.equals(configName))
					listBoxModel.setSelection(String.valueOf(i));
			}
		} catch (Throwable t) {
			log.error(LogUtil.APPS_USER_INTERFACE_CONTROLLER,"system.xcm.admin.exception", new Object[] {}, t);
		}		
		return listBoxModel;		
	}
	
	public String getConfigDataPath() {
		return mConfigManipulator.getDocHREF();
	}

	public String getScenarioConfigPath() {
		return mScenDataManipulator.getDocHREF();
	}	

	public String getConfigName() {
		return mCurrentGlobalConfigName;
	}
	
	public void setEditMode(boolean val) {
		if(this.mEditMode && val) return;
		
		if(!this.mEditMode && !val) return;
		
		mCompConfigController.setEditable(val);
		mScenConfigController.setEditable(val);
		
		this.mEditMode = val;
	}
	
	public boolean isEditMode() {
		return mEditMode;
	}
	
	/**
	 * Invalidate the Controller
	 * @return
	 */
	public String invalidate() {
		try {
			mConfigType = HOME_MODE;
			
			getCompConfigController().invalidate();
			getScenarioConfigController().invalidate();
			/*
			// check and restore active node
			String activeNodeId = null;
			if(mTreeModel.getActiveNode() != null)
				activeNodeId = mTreeModel.getActiveNode().getID();
			*/
			initConfigTreeModel();
			/*
			if(activeNodeId != null) {
				TreeNode activeNode = mTreeModel.getNodeByID(activeNodeId);
				// node may have been deleted
				if(activeNode != null) {
					mTreeModel.setActiveNode(activeNode);
				}
			}*/			
		}
		catch(Exception ex) {
			return ex.toString();
		}
		return Constants.OK;
	}

	public void lock(LockManager lm) throws LockException {
		mConfigManipulator.lockConfiguration(lm);
		try {
			mScenDataManipulator.lockConfiguration(lm);
		}
		catch(LockException le) {
			mConfigManipulator.unlockConfiguration(lm);
			throw le;
		}
	}
	
	public void unLock(LockManager lm) throws LockException {
		LockException le = null;
		try {
			mConfigManipulator.unlockConfiguration(lm);
		}
		catch(LockException e) {
			le = e;
		}
		
		try {
			mScenDataManipulator.unlockConfiguration(lm);
		}
		catch(LockException e) {
			le = e;		
		}
		
		if(le != null) throw le;
	}
	

	/**
	 * @return
	 */
	public boolean isAccessible() {
		return accessible;
	}

	/**
	 * @param b
	 */
	public void setAccessible(boolean b) {
		accessible = b;
	}
	
	public boolean isScenarioNameAvilable(String name) {
		return mAllScenNames.contains(name);
 
	}

	public void isScenarioNameAvilable(boolean available ) {
		mIsScenarioNameAvilable = available;			
	}

	public boolean isScenarioNameAvilable() {
		return mIsScenarioNameAvilable;			
	}

	public void addGlobalScenarioName(String scenName) {
		if (mAllScenNames != null) {
			mAllScenNames.add(scenName);
		}
	}

	public void removeGlobalScenarioName(String scenName) {
		if (mAllScenNames != null) {
			mAllScenNames.remove(scenName);
		}
	}

}
