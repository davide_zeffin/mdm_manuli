
package com.sap.isa.core.xcm.admin;

import java.io.File;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;

import org.w3c.dom.Document;

import com.sap.isa.core.FrameworkConfigManager;
import com.sap.isa.core.db.DBHelper;
import com.sap.isa.core.db.jdbc.JdbcPersistenceManagerFactory;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.MiscUtil;
import com.sap.isa.core.xcm.ConfigContainer;
import com.sap.isa.core.xcm.XCMConstants;
import com.sap.isa.core.xcm.XCMUtils;
import com.sap.isa.core.xcm.admin.controller.ControllerUtils;
import com.sap.isa.core.xcm.admin.model.CompDataManipulator;
import com.sap.isa.core.xcm.admin.model.ScenarioDataManipulator;
import com.sap.isa.core.xcm.admin.model.XCMAdminException;
import com.sap.isa.core.xcm.admin.model.XMLAdminUtil;
import com.sap.isa.core.xcm.admin.model.XMLAdminUtilDB;
import com.sap.isa.core.xcm.admin.model.XMLAdminUtilFS;
import com.sap.isa.core.xcm.config.ComponentConfig;
import com.sap.isa.core.xcm.config.ComponentConfigContainer;
import com.sap.isa.core.xcm.db.ConfigDataUnit;
import com.sap.isa.core.xcm.init.ExtendedConfigInitHandler;
import com.sap.isa.core.xcm.init.XCMAdminAllowedValueMetadata;
import com.sap.isa.core.xcm.init.XCMAdminComponentMetadata;
import com.sap.isa.core.xcm.init.XCMAdminInitHandler;
import com.sap.isa.core.xcm.init.XCMAdminParamConstrainMetadata;

/**
 * Class is used to privide information about the XCM metadata consistency 
 */
public class CheckXCMConsitency {


	protected static IsaLocation log =
						   IsaLocation.getInstance(CheckXCMConsitency.class.getName());
	
	/**
	 * Checks if there is metadata for each used component
	 */
	public static Map getAppCompMetaData() {
		Map result = new LinkedHashMap();
		ConfigContainer cc = FrameworkConfigManager.XCM.getXCMScenarioConfig(FrameworkConfigManager.NAME_BOOTSTRAP_SCENARIO);
		if (cc == null) {
			result.put("could not retrieve metadata", "");
			return result;
		}
		Document doc = cc.getConfigUsingAliasAsDocument("config-data");
		if (doc == null) {
			result.put("could not retrieve config-data.xml", "");
			return result;
		}

		// get all components defined by application
		ComponentConfigContainer ccc = new ComponentConfigContainer(doc, false);

		Set compNames = ccc.getComponentNames();
		for (Iterator iter = compNames.iterator(); iter.hasNext();) {
			String compId = (String)iter.next();
			XCMAdminComponentMetadata compMetaData = XCMUtils.getComponentMetadata(compId);
			result.put(compId, compMetaData);
		}				 
		 return result;
	}

	/**
	 * Returns parameters defined by application for a given component
	 * @param compid
	 * @return parameters defined by application for a given component
	 */
	public static Set getApplDefCompParams(String compId) {
		Set allPropertyNames = new LinkedHashSet();
		ConfigContainer cc = FrameworkConfigManager.XCM.getXCMScenarioConfig(FrameworkConfigManager.NAME_BOOTSTRAP_SCENARIO);
		Document doc = cc.getConfigUsingAliasAsDocument(XCMConstants.CONFIG_DATA_FILE_ALIAS);
		// get all components defined by application
		ComponentConfigContainer ccc = new ComponentConfigContainer(doc, false);
		Set compConfigs = ccc.getComponentConfigs(compId);
		
		for (Iterator iter = compConfigs.iterator(); iter.hasNext();) {
			ComponentConfig compConfig = (ComponentConfig)iter.next();
			Properties currentProps = compConfig.getAllParams();
			
			if (currentProps == null)
				continue;
			for(Enumeration enum = currentProps.keys(); enum.hasMoreElements();) {
				String propName = (String)enum.nextElement();
				allPropertyNames.add(propName); 
			}
		}
		return allPropertyNames;
	}
	
	public static Map getSAPScenarioConfigs(
		ScenarioDataManipulator scenDataMan, 
		CompDataManipulator compDataMan) {
		Map scenConfigs = scenDataMan.getScenarioConfigs();
		Map sapScenConfigs = new LinkedHashMap();
		for (Iterator iter = scenConfigs.keySet().iterator(); iter.hasNext();) {
			String scenName = (String)iter.next();
			ScenarioDataManipulator.ScenarioConfig sc =
				(ScenarioDataManipulator.ScenarioConfig)scenConfigs.get(scenName);
			if (!sc.isScenarioDerived())
				sapScenConfigs.put(scenName, sc);
		}		
		return sapScenConfigs;
	}
	
	public static ScenarioDataManipulator getScenDataManipulator() {
		String custURL = ExtendedConfigInitHandler.getCustRealConfigURL();
		String scenarioConfig = XCMAdminInitHandler.getScenarioConfig(1);
		String configDescription = XCMAdminInitHandler.getConfigName(1);
		ScenarioDataManipulator scenDataCustomer = new ScenarioDataManipulator();
		//scenDataCustomer.setBaseHREF("file:///" + baseDir);
		scenDataCustomer.setBaseHREF(null);
		scenDataCustomer.setDocHREF(custURL + File.separator + scenarioConfig);
		try {
			XMLAdminUtil adminUtil = null;
			if(ExtendedConfigInitHandler.isTargetDatastoreDB()) {
				JdbcPersistenceManagerFactory pmf = getPersistenceManagerFactory();
					
				XMLAdminUtilDB adminUtilDB = new XMLAdminUtilDB(pmf);
				adminUtilDB.setAppName(ExtendedConfigInitHandler.getApplicationName());
				adminUtilDB.setConfigSetName(configDescription);
				adminUtilDB.setConfigUnitName(scenarioConfig);
				adminUtilDB.setConfigUnitType(ConfigDataUnit.TYPE_SCENARIO_DATA);
				adminUtil = adminUtilDB;				
			}
			else {
				XMLAdminUtilFS adminUtilFS = new XMLAdminUtilFS();
				adminUtilFS.setBaseHREF(null);
				adminUtilFS.setDocHREF(custURL + File.separator + scenarioConfig);
				adminUtil= adminUtilFS;								
			}
			
			scenDataCustomer.init(adminUtil);
		} catch (XCMAdminException ex) { 
			return null;
		}
		finally {
		}
		return scenDataCustomer;
	}

	public static CompDataManipulator getCompDataManipulator() {
		String custURL = ExtendedConfigInitHandler.getCustRealConfigURL();
		String configDataPath = XCMAdminInitHandler.getConfigData(1);
		String configDescription = XCMAdminInitHandler.getConfigName(1);
		CompDataManipulator confDataCustomer = new CompDataManipulator();
		//confDataCustomer.setBaseHREF("file:///" + baseDir);
		confDataCustomer.setBaseHREF(null);
		confDataCustomer.setDocHREF(custURL + File.separator +  configDataPath);
		
		try {
			XMLAdminUtil adminUtil = null;
			if(ExtendedConfigInitHandler.isTargetDatastoreDB()) {
				JdbcPersistenceManagerFactory pmf = getPersistenceManagerFactory();

				XMLAdminUtilDB adminUtilDB = new XMLAdminUtilDB(pmf);
				adminUtilDB.setAppName(ExtendedConfigInitHandler.getApplicationName());
				adminUtilDB.setConfigSetName(configDescription);
				adminUtilDB.setConfigUnitName(configDataPath);
				adminUtilDB.setConfigUnitType(ConfigDataUnit.TYPE_CONFIG_DATA);
				adminUtil = adminUtilDB;				
			}
			else {
				XMLAdminUtilFS adminUtilFS = new XMLAdminUtilFS();
				adminUtilFS.setBaseHREF(null);
				adminUtilFS.setDocHREF(custURL + File.separator + configDataPath);
				adminUtil= adminUtilFS;								
			}			
			confDataCustomer.init(adminUtil);
		} catch (XCMAdminException ex) { 
			return null;
		}
		finally {
		}
		return confDataCustomer;
	}

	private static JdbcPersistenceManagerFactory getPersistenceManagerFactory() {
		Properties props = new Properties();
		String dsName = MiscUtil.replaceSystemProperty(XCMConstants.DATASOURCE_LOOKUP_NAME);
		props.setProperty(DBHelper.DB_DATASOURCE,dsName);
		props.setProperty(DBHelper.DB_PMF,JdbcPersistenceManagerFactory.class.getName());
		JdbcPersistenceManagerFactory pmf = 
			(JdbcPersistenceManagerFactory)DBHelper.getPersistenceManagerFactory(props);
		return pmf;		
	}


	public static Map getScenarioComponentParamAllowedValues(String scenName, String paramName, ScenarioDataManipulator scenMan, CompDataManipulator compMan, boolean all) {

		Map metaDataValues = new LinkedHashMap(); 
		Set fixAllowedValues = Collections.EMPTY_SET;

		// check if there are static param constrains defined
		// directly in scenario-config.xml
		ScenarioDataManipulator.ParamConstrain currentConstrain = 
			scenMan.getScenarioConfig(scenName).getParamConstrain(paramName);
		if (currentConstrain != null && currentConstrain.getAllowedValues().size() > 0 ) {
			fixAllowedValues =  currentConstrain.getAllowedValues();
		}	
		
			metaDataValues = getScenarioComponentAllowedValuesConsistency(scenName, paramName, fixAllowedValues, scenMan, compMan, all);
		return metaDataValues;
	}

	public static boolean isScenarioParamComponentDependent(String paramName) {
		XCMAdminParamConstrainMetadata metaData = XCMAdminInitHandler.getXCMAdminScenarioMetadata(paramName);
		if (metaData == null)
			return false;
			
		if (metaData.getComponent() != null)
			return true;
		else
			return false;
	}


	public static Set getScenarioFixedParamAllowedValues(String scenName, String paramName, ScenarioDataManipulator scenMan, CompDataManipulator compMan, boolean all) {

		Set fixAllowedValues = new LinkedHashSet();
		
		Set metaDataValues = Collections.EMPTY_SET; 
		
		// check if there are static param constrains defined
		// directly in scenario-config.xml
		ScenarioDataManipulator.ParamConstrain currentConstrain = 
			scenMan.getScenarioConfig(scenName).getParamConstrain(paramName);
		if (currentConstrain != null && currentConstrain.getAllowedValues().size() > 0 ) {
			fixAllowedValues =  currentConstrain.getAllowedValues();
		}	


		metaDataValues = getScenarioAllowedValuesConsistency(scenName, paramName, fixAllowedValues, scenMan, compMan, all);

		return metaDataValues;
	}

	/** 
	 * Returns a map containing allowed values for the 
	 * given parameter name
	 */
	private static Set getScenarioAllowedValuesConsistency(String scenName, String paramName, Set overwrittenValues, ScenarioDataManipulator scenMan, CompDataManipulator compMan, boolean centralDefined) {
		
		Set checkValues = new LinkedHashSet(overwrittenValues); 
		
		// if there is no overwritten meta data use the central one
		if (overwrittenValues != null && overwrittenValues.size() == 0)
			centralDefined = true;


		if (!centralDefined) {
			for (Iterator iter = overwrittenValues.iterator(); iter.hasNext();) {
				String value = (String)iter.next();
				checkValues.add(value);
			}	
			return checkValues;
		}

		 
		if (log.isDebugEnabled())
			log.debug("Getting allowed values: [param name]='" + paramName + "'");

		Set allowedValues = new TreeSet(overwrittenValues);

			ScenarioDataManipulator.ScenarioConfig scenConfig = 
		scenMan.getScenarioConfig(scenName);
							
			XCMAdminParamConstrainMetadata metaData = XCMAdminInitHandler.getXCMAdminScenarioMetadata(paramName);
			if (metaData == null)
				return Collections.EMPTY_SET;
			Map allowedValuesMetaData = metaData.getAllowedValueMetaData();
			for (Iterator iter = allowedValuesMetaData.values().iterator(); iter.hasNext(); ) {
				XCMAdminAllowedValueMetadata md = (XCMAdminAllowedValueMetadata)iter.next();
				String paramValue = md.getValue();
				String paramType = md.getType();
				
				allowedValues.add(paramValue);
/*				if (paramType.equals(XCMAdminAllowedValueMetadata.VALUE_TYPE_FIX)) {
					if (overwrittenValues.size() == 0) {
						// add this value if there are not overwritten values
						allowedValues.add(paramValue);
					} else {
						//checkValues.remove(paramValue);
					}
				}
				*/
			}
			
		if (!centralDefined) {
			for (Iterator iter = checkValues.iterator(); iter.hasNext(); ) {
				String invalidValue = (String)iter.next();
				allowedValues.add(invalidValue);
			}
		}
			
			
		return allowedValues;
	}

	

	/** 
	 * Returns a map containing allowed values for the 
	 * given parameter name
	 */
	private static Map getScenarioComponentAllowedValuesConsistency(String scenName, String paramName, Set overwrittenValues, ScenarioDataManipulator scenMan, CompDataManipulator compMan, boolean onlyCentral) {
		
		Set checkValues = new LinkedHashSet(overwrittenValues); 
		 
		if (checkValues.size() == 0) {
			// show all constraints since not overwritten
			onlyCentral = true;		
		}
		 
		if (log.isDebugEnabled())
			log.debug("Getting allowed values: [param name]='" + paramName + "'");

		Map allowedValues = new LinkedHashMap();

			ScenarioDataManipulator.ScenarioConfig scenConfig = 
		scenMan.getScenarioConfig(scenName);
							
			XCMAdminParamConstrainMetadata metaData = XCMAdminInitHandler.getXCMAdminScenarioMetadata(paramName);
			if (metaData == null)
				return Collections.EMPTY_MAP;
			Map allowedValuesMetaData = metaData.getAllowedValueMetaData();
			for (Iterator iter = allowedValuesMetaData.values().iterator(); iter.hasNext(); ) {
				XCMAdminAllowedValueMetadata md = (XCMAdminAllowedValueMetadata)iter.next();
				String paramValue = md.getValue();
				String paramType = md.getType();
				if (paramType.equals(XCMAdminAllowedValueMetadata.VALUE_TYPE_FIX)) {
						checkValues.remove(paramValue);
						continue;
				}

				if (onlyCentral) {
					allowedValues.put(paramValue, paramType);	
				} else {
					if (checkValues.contains(paramValue)) {
						allowedValues.put(paramValue, paramType);
						checkValues.remove(paramValue);
					}
				}

				if (paramType.equals(XCMAdminAllowedValueMetadata.VALUE_TYPE_BASE)) {
					paramType = "This config and descendants";					
				}
				
/*		
				if (paramType.equals(XCMAdminAllowedValueMetadata.VALUE_TYPE_BASE)||
				paramType.equals(XCMAdminAllowedValueMetadata.VALUE_TYPE_DERIVED)) {
					checkValues.remove(paramValue);
				}
*/		
			}
/*					
					String compId = metaData.getComponent();
					CompDataManipulator.CompConfig compConfig = compMan.getCompConfig(compId);
					if (compConfig == null) {
						String msg = "No component found for configuratin parameter [name]='" + paramName + "' for parameter [value]='" + paramValue + "'" ; 
						log.error("system.xcm.admin.exception", new Object[] { msg }, null);
						
					}
					Map descParams = compConfig.getDescendendParamConfigs(paramValue);
					for (Iterator iter2 = descParams.keySet().iterator(); iter2.hasNext(); ) {
						String descValue = (String)iter2.next();
						allowedValues.add(descValue);		
					}

					if (paramType.equals(XCMAdminAllowedValueMetadata.VALUE_TYPE_BASE))
						allowedValues.add(paramValue);
					allowedValues.add(Constants.CREATE_NEW_CONFIG_ID);
				}
			*/
			
	
		// check if there are overwritten contrains which do not match the central contrains
		if (!onlyCentral) {
			for (Iterator iter = checkValues.iterator(); iter.hasNext(); ) {
				String invalidValue = (String)iter.next();
				allowedValues.put(invalidValue, "MISSING METADATA");
			}
		}
	
		return allowedValues;
	}

	
}
