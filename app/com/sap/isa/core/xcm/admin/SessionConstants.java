package com.sap.isa.core.xcm.admin;

/**
 *
 */
public interface SessionConstants {


	public static final String CONFIG_DATA_MANIPULATOR = "ConfigDataManipulator.action.admin.xcm.core.isa.sap.com";
	public static final String CONFIG_TREE_MODEL       = "ConfigTreeModel.model.admin.xcm.core.isa.sap.com";
	public static final String CURRENT_COMP_ID         = "CurrentCompId.model.admin.xcm.core.isa.sap.com";
	public static final String APP_CONFIG_CONTROLLER   = "AppConfigController.controller.admin.xcm.core.isa.sap.com";
	public static final String MONITORING_CONTROLLER   = "MonitoringController.controller.admin.xcm.core.isa.sap.com";
	public static final String LOCKING_CONTROLLER = "LockingController.controller.admin.xcm.core.isa.sap.com";
	
	public static final String SCEN_CONFIG_PARAM_VALUE_HELP_SHORTTEXT = "paramvaluehelpshorttext.scenario.controller.admin.xcm.core.isa.sap.com";
	public static final String SCEN_CONFIG_PARAM_VALUE_HELP_LONGTEXT = "paramvaluehelplongtext.scenario.controller.admin.xcm.core.isa.sap.com";
	public static final String SCEN_CONFIG_PARAM_VALUE_HELP = "paramvaluehelp.scenario.controller.admin.xcm.core.isa.sap.com";
	public static final String SCEN_CONFIG_PARAM_DESCR_HELP = "paramdescrhelp.scenario.controller.admin.xcm.core.isa.sap.com";
	public static final String SCEN_NAME = "paramscenhelp.scenario.controller.admin.xcm.core.isa.sap.com";
	public static final String PARAM_NAME = "paramnamevaluehelp.scenario.controller.admin.xcm.core.isa.sap.com";
	public static final String COMP_NAME = "paramcomphelp.scenario.controller.admin.xcm.core.isa.sap.com";
}
