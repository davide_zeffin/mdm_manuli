package com.sap.isa.core.xcm.admin.model.help;

public class HelpItemBase implements HelpItem {

	private int mType;
	private String mName;
	private String mShorttext;
	private String mLongtext;
	private String mBaseConfig;
	private String mValue;	
	private String mDependendCompName;
	private String mScope;

	public HelpItemBase(int type, String name, String shorttext, String longtext, String baseConfig, String value, String dependendCompName, String scope) {
		mType = type;
		mName = name;
		mShorttext = shorttext;
		mLongtext = longtext;
		mBaseConfig = baseConfig;
		mValue = value;
		mDependendCompName = dependendCompName;
		mScope = scope;
	}

	public int getType() {
		return mType;
	}

	public String getTypeName() {
		return "TODO: Type";
	}

	public String getName() {
		return mName;
	}

	public String getShorttext() {
		if (mShorttext == null)
			return "";
		else
			return mShorttext;
	}

	public String getLongtext() {
		if (mLongtext == null)
			return null;
		else
			return mLongtext;
	}

	public String getBaseConfigName() {
		if (mBaseConfig == null)
			return "";
		else
			return mBaseConfig;
	}

	public String getConfigValue() {
		return mValue;
	}

	public String getDependendCompName() {
		return mDependendCompName;
	}
	
	/**
	 * Returns null
	 * @see com.sap.isa.core.xcm.admin.model.help.HelpItem#getChildren()
	 */
	public HelpItemContainer getChildren() {
		return null;
	}
	
	public String getScope() {
		return mScope;		
	}
}