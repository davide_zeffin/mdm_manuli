package com.sap.isa.core.xcm.admin.model;

// java imports
import java.io.File;
import java.io.FileWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.sap.isa.core.locking.LockException;
import com.sap.isa.core.locking.LockManager;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.xcm.ExtendedConfigManager;
import com.sap.isa.core.xcm.StringInputStream;
import com.sap.isa.core.xcm.init.ExtendedConfigInitHandler;
import com.sap.isa.core.xcm.init.ParamsConfig;
import com.sap.isa.core.xcm.xml.ExtensionProcessor;
import com.sap.isa.core.xcm.xml.ParamsStrategy;
import com.sap.isa.core.xcm.xml.XIncludeProcessor;
import com.sap.isa.core.xcm.xml.XMLConstants;
import com.sap.isa.core.xcm.xml.XMLUtil;


/**
 * This class provides some XML related function needed by the XCM
 * administration
 */
public abstract class XMLAdminUtil {

	public static final String XML_ELEMENT_ID_VALUE_OVERWRITTEN = "overwritten";
 
	protected static IsaLocation log =
		IsaLocation.getInstance(XMLAdminUtil.class.getName());

	// loaded document with added elementid
	protected Document mLoadDoc;
	// docuemnt with all inclusions processed
	protected Document mIncludeDoc;

	private XIncludeProcessor mXIncludeProc = null;
	
	private ExtensionProcessor mExtProc = null;
	private static ExtensionProcessor.ExtensionKeys mExtKeys = null;	

	public XMLAdminUtil() throws XCMAdminException {
		try {
			// initialize XInclude processor 
			mXIncludeProc = new XIncludeProcessor();
			// initialize Extension Processor			
			mExtProc = new ExtensionProcessor();		
			
			// add config params 
			ParamsConfig paramConfig = ExtendedConfigInitHandler.getParamsConfig();
			
			// add context parameter to param config
			Map contextParams = ExtendedConfigManager.getExternalConfigParams();
			for (Iterator iter = contextParams.keySet().iterator(); iter.hasNext();) {
				String name = (String)iter.next();
				String value = (String)contextParams.get(name);
				paramConfig.addParamConfig(name, value, "context parameter");
			}
			
			mExtKeys = 	ExtendedConfigInitHandler.getExtensionKeyConfig().getExtensionKeyContainer();

			ParamsStrategy paramsStrategy = new ParamsStrategy();
			paramsStrategy.setParams(paramConfig.getParamsContainer());
			mXIncludeProc.setNodeManipulatorStrategy(paramsStrategy);

			// add extension config 
			mExtProc.setExtensionKeys
				(ExtendedConfigInitHandler.getExtensionKeyConfig().getExtensionKeyContainer());


		} catch (ParserConfigurationException pcex) {
			String msg = "Error initializing XInclude processor";
			log.error(LogUtil.APPS_USER_INTERFACE,"system.xcm.admin.exception", new Object[] { msg }, pcex);
			XCMAdminException ex = new XCMAdminException(msg);
			ex.setBaseException(pcex);
			throw ex;
		}
	}
	
	/**
	 * Initialized this XMLAdminUtil
	 * The document is loaded, xinclude and extensions are processed
	 */
	

	/**
	 * Performs the xinclude
	 */
	private Document performXInlclude(String baseHref, Document doc) throws XCMAdminException {
		try {
			return mXIncludeProc.parse(baseHref, doc);
		} catch (XIncludeProcessor.XIncludeException xiex) {
			XCMAdminException ex = new XCMAdminException(xiex.getMessage());
			ex.setBaseException(xiex);
			throw ex;
		}
	}


	/**
	 * Returns a DOM representation of the extending document
	 * 
	 * @return a DOM representation of the document
	 */
	public Document loadDocument(String baseHref, String docHref) throws XCMAdminException {
		// nps: XCM db enhancements
		return loadConfigurationDocument();
	}
	
	// nps : Derived class implementation contract
	public abstract Document loadConfigurationDocument() throws XCMAdminException;
	public abstract void refreshConfiguration() throws XCMAdminException;	
	public abstract void saveConfiguration(String data) throws XCMAdminException;
	public void lockConfiguration(LockManager lm) throws LockException {
		// do nothing
	}
	
	public void unlockConfiguration(LockManager lm) throws LockException {
		// do nothing
	}
	
	/**
	 * Returns a DOM representation of the document after all inclusions 
	 * @return a DOM representation of the extending document
	 */
	public Document processXInclude(String baseHref, Document doc) throws XCMAdminException {
		return performXInlclude(baseHref, doc);
	}

	/**
	 * If the passed node extends another node then the extension 
	 * machanism is per<formed and the result of the extension is returned
	 * The passed context node must be part of the result document
	 * @param contextNode contextNode
	 * @return copy of context node with calculated extension
	 */
	public Node getExtension(Node contextNode) {
		Node baseNode = mExtProc.getBaseNodeWithExtension(contextNode);
		// clone base node
		Node clonedBaseNode = baseNode.cloneNode(true);

		mExtProc.copyAttrbutes(clonedBaseNode, contextNode);
		return mExtProc.extendNode(clonedBaseNode, contextNode);
	}
	
	/**
	 * Returns a document with all extensions processed
	 * @param doc
	 * @param copy of document with all extension processed
	 */
	public Document processExtension(Document doc) {
		return mExtProc.parse(doc);
		
	}

	/**
	 * Removes all nodes without 'isa:elementid'
	 * @param node the root node
	 * @return a document without elements with 'isa:elementid' attribute
	 */
	public static Node removeAllNodesWithoutId(Node doc) {
		return XMLUtil.getInstance().removeNodes(doc, XMLConstants.ISA_NAMESPACE, XMLConstants.NODE_ID, false);
	}

	/**
	 * Removes attributes from document.
	 * @param rootNode the root node
	 * @param namespace
	 * @param attrName name of attribute
	 * @return a document without elements with 'isa:elementid' attribute
	 */
	public static Node removeAttrbutes(Node rootNode, String namespace, String attrName) {
		return XMLUtil.getInstance().removeAttr(rootNode, namespace, attrName);
	}

	/**
	 * This method searches for the 'isa:elementid' of the first node.
	 * It then searches for a node with the same id in the second node.
	 * The found node is replaced with the first node. Additionally the following
	 * tasks are performed:
	 * - xml:base attribute is removed
	 * - isa:extended is renamed to isa:extends
	 * - isa:hash is removed
	 * @param sourceNode 
	 * @param destDoc
	 */
	public static void replaceNodeWithId(Node sourceNode, Document destDoc) {
		if (sourceNode == null || destDoc == null 
				|| sourceNode.getNodeType() != Node.ELEMENT_NODE)
			return;
		Element sourceElement = (Element)sourceNode;
		Attr idAttr = sourceElement.
			getAttributeNodeNS(XMLConstants.ISA_NAMESPACE, XMLConstants.NODE_ID);
		if (idAttr == null)
			return;
		String id = idAttr.getValue(); 
		String elementName = sourceElement.getNodeName();
		String elementNS = sourceElement.getNamespaceURI();
		// search for node with same id in the dest document
		NodeList nodeList = destDoc.getElementsByTagNameNS(elementNS, elementName);
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node currentNode = nodeList.item(i);
			if (currentNode.getNodeType() == Node.ELEMENT_NODE) {
				Element currentElement = (Element)currentNode;
				Attr currentAttr = currentElement.getAttributeNodeNS(XMLConstants.ISA_NAMESPACE, XMLConstants.NODE_ID);
				if (currentAttr == null)
					continue;
				if (currentAttr.getValue().equals(id)) {
					String extendsString = 
						currentElement.getAttributeNS(XMLConstants.ISA_NAMESPACE, XMLConstants.EXTENDS);
					
					if (extendsString != null) {
						sourceElement.removeAttributeNS(XMLConstants.XML_NAMESPACE, XMLConstants.BASE);
						sourceElement.removeAttributeNS(XMLConstants.ISA_NAMESPACE, XMLConstants.HASH);						
						sourceElement.removeAttributeNS(XMLConstants.ISA_NAMESPACE, XMLConstants.EXTENDED);
						sourceElement.setAttributeNS(XMLConstants.ISA_NAMESPACE, XMLConstants.EXTENDS_NS, extendsString);
					}
					Node newSourceNode = destDoc.importNode(sourceNode, true);
					currentElement.getParentNode().replaceChild(newSourceNode, currentElement);
				}
			}
		}
	}
	
	/**
	 * Restore replaced nodes
	 * This method does the following:
	 * 1) It searches all nodes with an isa:replaced attribute
	 * 2) It creates a copy of this node
	 * 3) The key of the copy is removed
	 * 4) if the copy has an xml:base attribute than it is converted into an xinclude
	 */
	public static void restoreReplacedNodes(Node rootNode) {
		
		NodeList childNodes = rootNode.getChildNodes();
		
		for (int i = 0; i < childNodes.getLength(); i++) {
			Node childNode = childNodes.item(i);
			if (childNode.getNodeType() != Node.ELEMENT_NODE)
				continue;
			
			Element childElement = (Element)childNode;
			// check if node has replaced attribue
			Attr replaced = childElement.
				getAttributeNodeNS(XMLConstants.ISA_NAMESPACE, XMLConstants.REPLACED);
			if (replaced != null) {
				String extPath = replaced.getValue();
				// remove replaced node from child node
				childElement.removeAttributeNode(replaced);
				// add isa:extends node
				childElement.setAttributeNS(XMLConstants.ISA_NAMESPACE, XMLConstants.EXTENDS_NS, extPath);
				// remove id attribute
				String key = mExtKeys.getExtensionKey(childElement.getNodeName());
				if (key != null && childElement.hasAttribute(key)) {
					childElement.removeAttribute(key);
				}
				// check if xml:base available
				Attr xmlAttr = childElement.getAttributeNodeNS(XMLConstants.XML_NAMESPACE, XMLConstants.BASE);
			}
			restoreReplacedNodes(childNode)	;
		}
	}

	/**
	 * Restore replaced nodes
	 * This method does the following:
	 * 1) It searches all nodes with an isa:replaced attribute
	 * 2) It creates a copy of this node
	 * 3) The key of the copy is removed
	 * 4) if the copy has an xml:base attribute than it is converted into an xinclude
	 */
	public static void restoreExtendedNodes(Node rootNode) {
		
		NodeList childNodes = rootNode.getChildNodes();
		
		for (int i = 0; i < childNodes.getLength(); i++) {
			Node childNode = childNodes.item(i);
			if (childNode.getNodeType() != Node.ELEMENT_NODE)
				continue;
			
			Element childElement = (Element)childNode;
			// check if node has replaced attribue
			Attr extended = childElement.
				getAttributeNodeNS(XMLConstants.ISA_NAMESPACE, XMLConstants.EXTENDED);
			if (extended != null) {
				String extPath = extended.getValue();
				// remove extended node from child node
				childElement.removeAttributeNode(extended);
				// add isa:extends node
				childElement.setAttributeNS(XMLConstants.ISA_NAMESPACE, XMLConstants.EXTENDS_NS, extPath);
			}
			restoreExtendedNodes(childNode)	;
		}
	}
		
		
	/**
	 * Saves the xinclude element in order to be able to restore it after the
	 * xinclude processing
	 */
	public void saveXInclude(Node rootNode) {
		NodeList childNodes = rootNode.getChildNodes();
		
		for (int i = 0; i < childNodes.getLength(); i++) {
			Node childNode = childNodes.item(i);
			if (childNode.getNodeType() != Node.ELEMENT_NODE)
				continue;
			
			Element childElement = (Element)childNode;
			// check if xinclude
			if (childElement.getNodeName().equals(XMLConstants.XINCLUDE_NS)) {
				String href = childElement.getAttribute(XMLConstants.HREF);
				// save xinclude
				Element saveXIncludeElement = childElement.getOwnerDocument().
					createElementNS(XMLConstants.ISA_NAMESPACE, XMLConstants.SAVE_XINCLUDE_NS);

				// in order to prevent parameters to be recognized change $ => #
				href = href.replace('$', '<');
				saveXIncludeElement.setAttribute(XMLConstants.HREF, href);
				saveXIncludeElement.setAttributeNS(XMLConstants.ISA_NAMESPACE, XMLConstants.NODE_ID_NS, XMLConstants.SAVE_XINCLUDE_NS);
				childElement.getParentNode().appendChild(saveXIncludeElement);
			}
			saveXInclude(childElement);
		}
	}
	
	public static void restoreXInclude(Node rootNode) {
		
		NodeList childNodes = rootNode.getChildNodes();
		
		for (int i = 0; i < childNodes.getLength(); i++) {
			Node childNode = childNodes.item(i);
			if (childNode.getNodeType() != Node.ELEMENT_NODE)
				continue;
			
			Element childElement = (Element)childNode;
			// check if node has replaced attribue
			if (childElement.getNodeName().equals(XMLConstants.SAVE_XINCLUDE_NS)) {
				String href = childElement.getAttribute(XMLConstants.HREF);
				// restore xinclude
				Element xincludeElement = childElement.getOwnerDocument().
					createElementNS(XMLConstants.XINCLUDE_NAMESPACE, XMLConstants.XINCLUDE_NS);
				href = href.replace('<', '$');
				xincludeElement.setAttribute(XMLConstants.HREF, href);
				childElement.getParentNode().appendChild(xincludeElement);
				childElement.getParentNode().removeChild(childElement);
				
				continue;
			}
			restoreXInclude(childNode)	;
			}
			
		}

	/**
	 * Adds element id to parent node recursively
	 */
	public static void addElementId(Node node) {
		if (node.getNodeType() != Node.ELEMENT_NODE || node.getParentNode() == null)
			return;
		((Element)node).setAttributeNS(XMLConstants.ISA_NAMESPACE, 
				XMLConstants.NODE_ID_NS, XML_ELEMENT_ID_VALUE_OVERWRITTEN);
		addElementId(node.getParentNode());
	}	

	/**
	 * Removes element id to parent node recursively
	 */
	public static void removeElementId(Node node) {
		if (node.getNodeType() != Node.ELEMENT_NODE || node.getParentNode() == null)
			return;
			
		// remove element if
		((Element)node).removeAttribute(XMLConstants.NODE_ID_NS);
			
			
		if (node.getChildNodes().getLength() > 1)
			return;
			
		// if current node is the only child node then remove node
		if (node.getParentNode().getChildNodes().getLength() == 1) {
			node.getParentNode().removeChild(node);
			removeElementId(node.getParentNode());
		}
	}	

	/**
	 * Returns a string representation of the XML file after removing 
	 * all nodes without <code>isa:elementid<code>
	 * @return 
	 */
	public static String getExtendingDocAsString(Document doc) {
		Document extDoc = (Document)doc.cloneNode(true);

		Node rootNode = extDoc.getFirstChild();

		while(true) {
			if (rootNode == null || rootNode.getNodeType() == Node.ELEMENT_NODE) 
				break;
			rootNode = rootNode.getNextSibling();
		}


		// remove all nodes without isa:elementid attribute
		// these are extended nodes
 
		XMLAdminUtil.removeAllNodesWithoutId(rootNode);
		// process isa:replaced attributes
		restoreReplacedNodes(rootNode);

		// process isa:extended attributes
		restoreExtendedNodes(rootNode);

		// restore xinclude
		restoreXInclude(rootNode);
		// remove the following attributes
		// isa:elementid
		XMLAdminUtil.removeAttrbutes(rootNode, XMLConstants.ISA_NAMESPACE, XMLConstants.NODE_ID);
		// xml:base
		XMLAdminUtil.removeAttrbutes(rootNode, XMLConstants.XML_NAMESPACE, XMLConstants.BASE);
		// isa:extended
		XMLAdminUtil.removeAttrbutes(rootNode, XMLConstants.ISA_NAMESPACE, XMLConstants.EXTENDED);
		// isa:hash
		XMLAdminUtil.removeAttrbutes(rootNode, XMLConstants.ISA_NAMESPACE, XMLConstants.HASH);
		return XMLUtil.getInstance().toString(extDoc);
	}
	

	/**
	 * Saves the current configuration to the file system
	 */
	public static void saveConfiguration(String config, String baseHref, String docHref) throws Exception {
		String href = null;
		String path = null;
		try {
			if (log.isDebugEnabled()) {
				log.debug("Save path [baseHref]='" + baseHref + "' [docHre]='" + docHref + "'");
			}
			href = XMLUtil.getInstance().getAbsoluteURL(baseHref, docHref);
			if (log.isDebugEnabled()) {
				log.debug("Save path [href]='" + href);
				log.debug("path of docHre [href]='" + new URL(docHref).getPath());
				log.debug("host of docHre [href]='" + new URL(docHref).getHost());
			}
			path = new URL(docHref).getPath();
			if (log.isDebugEnabled()) {
				log.debug("Saving component configuration [path]='" + path + "'");
			}
		} catch (MalformedURLException murlex) {
			log.error(LogUtil.APPS_USER_INTERFACE,"system.xcm.admin.exception", new Object[] {}, murlex);
		}
		File configFile = new File(path);
		FileWriter fw = null;
		StringInputStream sis = null;
		try {
			fw = new FileWriter(configFile);
			sis = new StringInputStream(config);


			int c;

			while ((c = sis.read()) != -1)
			   fw.write(c);

			fw.close();
			sis.close();
    						
		} catch (Throwable ex) {
			try {
				if (fw != null)
					fw.close();
				if (sis != null)
					sis.close();		
			} catch (Throwable t) {
				log.debug(t.getMessage());
			}
				
			log.error(LogUtil.APPS_USER_INTERFACE,"system.xcm.admin.exception", new Object[] {}, ex);
			String msg = ex.toString();
			StringBuffer str = new StringBuffer();

			int len = (msg != null) ? msg.length() : 0;
			for (int i = 0; i < len; i++ ) {
			  char ch = msg.charAt(i);
			  switch ( ch ) {
				case '\\': {
				  str.append("/");
				  break; 
				}
				default: {
				  str.append(ch);
				}
			  }
			}
			throw new Exception(str.toString());
		}
	}
}
