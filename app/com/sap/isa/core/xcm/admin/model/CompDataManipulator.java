package com.sap.isa.core.xcm.admin.model;

// dom imports
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.sap.isa.core.init.InitializationHandler;
import com.sap.isa.core.locking.LockException;
import com.sap.isa.core.locking.LockManager;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.conv.ParamConverter;
import com.sap.isa.core.util.conv.ParamConverterUtil;
import com.sap.isa.core.util.conv.PasswordConverter;
import com.sap.isa.core.xcm.XCMUtils;
import com.sap.isa.core.xcm.init.XCMAdminAllowedValueMetadata;
import com.sap.isa.core.xcm.init.XCMAdminComponentMetadata;
import com.sap.isa.core.xcm.init.XCMAdminInitHandler;
import com.sap.isa.core.xcm.xml.XMLConstants;

/**
 * This class provides methods useful when manipulating configuration 
 * files of the format:
 * <pre>
 *   <data version="4.0">
 *	   <component id="component id" >
 *		   <configs isa:elementid="1">
 *			   <config id="config name">
 *				   <params id="param set name" >
 *					   <param name="param name " value="value"/>
 *     				</params>
 *	    		</config>
 *			
 *		   </configs>
 *	    </component>
 *    </data>
 * 
 * </pre>
 */
public class CompDataManipulator {
	
	public static final String XML_ELEMENT_COMPONENT = "component";
	public static final String XML_ELEMENT_PARAM     = "param";
	public static final String XML_ELEMENT_CONFIG    = "config";
	public static final String XML_ATTR_ID           = "id";
	public static final String XML_ATTR_NAME         = "name";
	public static final String XML_ATTR_VALUE        = "value";	
	public static final String XML_ATTR_XCM_CONTROL  = "xcmcontrol";
	public static final String XML_ATTR_DESCR        = "description";	
	public static final String XML_ATTR_TEST_CLASS   = "testclass";

	public static final String INVISIBLE_XCM_CONTROL = "invisible";

	public static final String XML_ELEMENT_ID_VALUE_OVERWRITTEN = "overwritten";

	protected static IsaLocation log = IsaLocation.getInstance(CompDataManipulator.class.getName());

	private String mDocHref = null;
	private String mBaseHref = null;	
	private Document mDocBeforeInclude;
	private Document mDocAfterInclude;
	private Document mDocAfterExtension;
	private Map mCompConfigs = new TreeMap();
	private Map mGeneralConfigs = new TreeMap();
	private boolean mInit = false;
	private XMLAdminUtil mXmlAdmin;
	
	private boolean targetDatastoreDB = false;
	
	public void init(XMLAdminUtil adminUtil) throws XCMAdminException {
	
		mXmlAdmin = adminUtil;

		// get document as stored on filesystem
		// additionally an 'isa:elementid' is added to every node
		setDocBeforeInclude(mXmlAdmin.loadDocument(mBaseHref, mDocHref));
		
		mInit = true;		
	}
	
	/**
	 * Returns a DOM representation of the extending document
	 * @return a DOM representation of the extending document
	 */
	private void setDocBeforeInclude(Document docBeforeInclude) throws XCMAdminException {
		mDocBeforeInclude = (Document)docBeforeInclude.cloneNode(true);
		// save xinclude information
		mXmlAdmin.saveXInclude(mDocBeforeInclude);
		// perform include 
		mDocAfterInclude = mXmlAdmin.processXInclude(mBaseHref,mDocBeforeInclude);
		// perform extension
		mDocAfterExtension = mXmlAdmin.processExtension(mDocAfterInclude);	
		// iterater throug all components and store their configuration
		NodeList compNodes = getFirstElementSibling(mDocAfterExtension.getFirstChild()).getChildNodes();
		for (int i = 0; i < compNodes.getLength(); i++) {
			Node currentNode = compNodes.item(i);
			String id;
			if (currentNode.getNodeType() == Node.ELEMENT_NODE && 
					currentNode.getNodeName().equals(XML_ELEMENT_COMPONENT)) {
				id = ((Element)currentNode).getAttribute(XML_ATTR_ID);
				if (log.isDebugEnabled())
					log.debug("processing component [id]='" + id + "'");
				
				CompConfig compConfig = new CompConfig(mDocAfterExtension, currentNode, id, XCMUtils.isApplicationScopeCompConfig(id));
				mCompConfigs.put(id, compConfig);
			}
		}
	}	


	/**
	 * Returns a DOM representation of the extending document
	 * @return a DOM representation of the extending document
	 */
	public Document getDocBeforeInclude() throws XCMAdminException {
		return mDocBeforeInclude;
	}
	
	/**
	 * Returns a DOM representation of the result document
	 * @return a DOM representation of the extending document
	 */
	public Document getDocAfterInclude() throws XCMAdminException {
		return mDocAfterInclude;
	}

	/**
	 * Returns a document with processed extensions
	 */
	public Document getDocAfterExtension() {
		return mDocAfterExtension;	
	}


	/**
	 * Sets the HREF to the extending document
	 * @param href HREF to the extending document
	 */
	public void setDocHREF(String href) {
		mDocHref = href;	
	}

	/**
	 * Gets the HREF to the extending document
	 * @return href HREF to the extending document
	 */
	public String getDocHREF() {
		return mDocHref;	
	}


	/**
	 * Sets the base HREF
	 * @param href base HREF 
	 */
	public void setBaseHREF(String href) {
		mBaseHref = href;	
	}
	
	
	/**
	 * @param flag true => target datastore is database else filesystem
	 */
	public void setTargetDataStoreDB(boolean flag) {
		this.targetDatastoreDB = flag;
	}
	
	/**
	 * @return true of the target datastore is database
	 */
	public boolean isTargetDatastoreDB() {
		return targetDatastoreDB;
	}

	/**
	 * Container for component configuration
	 */
	public static class CompConfig {
		
		private Map mParamConfigs = new LinkedHashMap();
		private Node mCurrentNode;
		private String mCompId; 
		private Document mDoc;
		private boolean mIsGeneralScope = false;
		
		/**
		 * Constructor
		 * @param the root node of the component configuration
		 */
		public CompConfig(Document doc, Node node, String compId, boolean compScope) {
			mDoc = doc;
			mCompId = compId;
			mCurrentNode = node;
			mIsGeneralScope = compScope;
			
			// get name of configuration
			String mName =  ((Element)mCurrentNode).getAttribute(XML_ATTR_ID);

			NodeList paramNodes = ((Element)mCurrentNode).getElementsByTagName(XML_ELEMENT_CONFIG);
			for (int i = 0; i < paramNodes.getLength(); i++) {
				// current param config
				Node currentNode = paramNodes.item(i);
				if (!currentNode.getNodeName().equals(XML_ELEMENT_CONFIG))
					continue;
					
				String id = ((Element)currentNode).getAttribute(XML_ATTR_ID);
				if (log.isDebugEnabled())
					log.debug(" processing parameter configuration [id]='" + id + "'");
				mParamConfigs.put(id, new ParamConfig(mCompId, currentNode));
			}
		}

		/**
		 * Returns the id of this component
		 */
		public String getCompId() {
			return ((Element)mCurrentNode).getAttribute(XML_ATTR_ID);
		}


		
		/**
		 * Returns the class name of a class which can be used
		 * to test this component. This class has to impelement
		 * the com.sap.isa.core.test.ComponentTest
		 * interface
		 * @return class Name if available otherwise <code>null</code>
		 */
		public String getTestClassName() {
			String testClass = ((Element)mCurrentNode).getAttribute(XML_ATTR_TEST_CLASS);
			if (testClass.length() == 0)
				return null;	
			else
				return testClass;
		}


		/**
		 * Returns a unmodifiable map of parameter configs
		 */
		public Map getParamConfigs() {
			return mParamConfigs;
		}

		/**
		 * Returns a parameter config for a given id or <code>null</code>
		 * @param id id of configuration
		 * @return parameter config
		 */
		public ParamConfig getParamConfig(String id) {
			return (ParamConfig)mParamConfigs.get(id);	
		}

		/**
		 * Returns true if there is at least one derived cutomer parameter configuration
		 * @param baseConfigId
		 * @return
		 */
		public boolean isDescendendParamConfigAvailable() {
			Map paramConfigs = new LinkedHashMap();
			for (Iterator iter = mParamConfigs.keySet().iterator(); iter.hasNext();) {
				String id = (String)iter.next();
				ParamConfig param = (ParamConfig)mParamConfigs.get(id);
				// check for application scope components. Return false if no user settings
/*				if (XCMUtils.isApplicationScopeCompConfig(mCompId)) {
					if (param.isDerivedCompConfig()) {
						Set paramNames = param.getParamNames();
						for (Iterator iter2 = paramNames.iterator(); iter2.hasNext();) {
							String paramName = (String)iter2.next();
							if (param.isDerived(paramName))
								return true;
						}
					}
					continue;
				}
*/
								if (param.isDerivedCompConfig())
					return true;
			}
			return false; 
		}

		/**
		 * Returns parameter configuration for all descendents of the  
		 * this configuration
		 */
		public Map getDescendendParamConfigs(String baseConfigId) {
			Map paramConfigs = new LinkedHashMap();
			for (Iterator iter = mParamConfigs.keySet().iterator(); iter.hasNext();) {
				String id = (String)iter.next();
				ParamConfig param = (ParamConfig)mParamConfigs.get(id);
				if (param.getBaseConfigId().equals(baseConfigId)) {
					paramConfigs.put(id, param);
				}
			}
			return paramConfigs;
		}

		/**
		 * Returns a string representation of this configuration
		 * @return a string representation of this configuration
		 */
		public String toString() {
			return mCurrentNode.toString();
		}

		/**
		 * Adds a new configuration
		 */
		public void createParamConfig(String paramConfigId, String baseConfigId) {
			if (log.isDebugEnabled()) {
				log.debug("Adding new parameter configuration [id]='" + paramConfigId + 
				"' based on parameter configuration [id base]='" + baseConfigId + "'");
			}
			
			// get base configuration
			ParamConfig baseConfigParams = (ParamConfig)mParamConfigs.get(baseConfigId);
			Element baseConfigNode = baseConfigParams.getRootNode();
			Element newConfigNode = (Element)baseConfigNode.cloneNode(true);
			// set new id
			//newConfigNode = (Element)mDoc.importNode(newConfigNode, true);
			newConfigNode.setAttribute(XML_ATTR_ID, paramConfigId);
			// add new node
			baseConfigNode.getParentNode().insertBefore(newConfigNode, baseConfigNode);
			// add new parameter configuration
			mParamConfigs.put(paramConfigId, new ParamConfig(mCompId, newConfigNode));
			// add element id in order to prevent this node to be deleted
			newConfigNode.setAttributeNS(XMLConstants.ISA_NAMESPACE, 
								XMLConstants.NODE_ID_NS, XML_ELEMENT_ID_VALUE_OVERWRITTEN);
			// add base attribute only if not application scope configuration
			if (!mIsGeneralScope)
				newConfigNode.setAttributeNS(XMLConstants.ISA_NAMESPACE, 
					XMLConstants.ISA_BASE_NS, baseConfigId);
				
			// add element id node to all parent nodes
			XMLAdminUtil.addElementId(newConfigNode);
			
			// eventually add the isa:extends attribute
			String isaExtends = ((Element)mCurrentNode).getAttribute("isa:extends");
			if (isaExtends.length() == 0) {
				// add isa:extends attribute
				String extendsValue = "../component[@id='" + mCompId + "']";
				((Element)mCurrentNode).setAttributeNS(
					XMLConstants.ISA_NAMESPACE, XMLConstants.EXTENDS_NS, extendsValue);
				// remove id attribute
				((Element)mCurrentNode).removeAttribute(XML_ATTR_ID);
			}
		}		

		/**
		 * Deletes an existing parameter configuration
		 */
		public void deleteParamConfig(String paramConfigId) {
			ParamConfig configParams = (ParamConfig)mParamConfigs.get(paramConfigId);
			if (configParams != null) {
				Node rootNode = configParams.getRootNode(); 
				rootNode.getParentNode().removeChild(rootNode);	
				mParamConfigs.remove(paramConfigId);
				// check existing parmeter configuration whether they are in the SAP folder
				for (Iterator iter = mParamConfigs.values().iterator(); iter.hasNext(); ) {
					ParamConfig configParam = (ParamConfig)iter.next();
					if (configParam.isDerivedCompConfig()) {
						// no need to delete the component configuration in the 
						// customer file
						return;
					}
				}
				// component config in the customer file can be deleted
				XMLAdminUtil.removeElementId(mCurrentNode); 
			}
		}

		/**
		 * Returns true if there is a parameter configuration for 
		 * the given id
		 */
		public boolean isParamConfigAvailable(String configId) {
			return mParamConfigs.containsKey(configId);
		}
		
		public String getStatus() {		
			XCMAdminComponentMetadata md = XCMAdminInitHandler.getComponentMetadata(mCompId);
			if (md == null)
				return "";
			return XCMAdminInitHandler.getComponentMetadata(mCompId).getStatus();
		}

	}

	/**
	 * Container for configuration data
	 */	
	public static class ParamConfig {
		
		private Node mRootNode;
		private Map mParamNodes = new LinkedHashMap();
		private Map mParamNames; 
		private String mName;
		private String mCompId;
		private Object mCurrentConv;
		private String mCurrentEncodeMethodName = null;
		private String mCurrentDecodeMethodName = null;
		
		ParamConfig(String compId, Node node) {
			
			
			mCompId = compId;
			mRootNode = node;
			// get name of configuration
			mName =  ((Element)mRootNode).getAttribute(XML_ATTR_ID);
			
			NodeList paramNodes = ((Element)mRootNode).getElementsByTagName(XML_ELEMENT_PARAM);
			for (int i = 0; i < paramNodes.getLength(); i++) {
				Node currentNode = paramNodes.item(i);
				String nodeName = ((Element)currentNode).getAttribute(XML_ATTR_NAME);
				mParamNodes.put(nodeName, currentNode);
					
			}
		}	
	
		/**
		 * Returns id of this parameter configuration
		 * @return Returns id of this parameter configuration
		 */
		public String getId() {
			return mName;
		}

		/**
		 * 
		 * Returns the name of the base configuration
		 */
		public String getBaseConfigId() {
			return ((Element)mRootNode).getAttributeNS(XMLConstants.ISA_NAMESPACE, XMLConstants.ISA_BASE);
		}


		/**
		 * Returns the root node of this parameter configuration
		 */
		public Element getRootNode() {
			return (Element)mRootNode;
		}
		
		/**
		 * Returns the value of the xcmcontroll attribute
		 * @return String the value of the xcmcontroll attribute
		 */
		public String getXCMControl() {
			return ((Element)mRootNode).getAttribute(XML_ATTR_XCM_CONTROL);
		}
		
		/**
		 * Returns the decoded version of the parameter
		 * if there is a converter for it
		 */
		private String getDisplayValue(String name, String value) {
			initConvObject(name);
			if (mCurrentConv != null) {
				if (ParamConverterUtil.isParamConverter(mCurrentConv))
					return ((ParamConverter)mCurrentConv).decode(value);	
				else
					return ParamConverterUtil.customDecode(mCurrentConv, mCurrentDecodeMethodName, mCompId, mName, name, value);				
			} else 
				return value;
		}
			
		/**
		 * Returns an instance of the conversion class
		 * @return an instance of the conversion class
		 */
		private void initConvObject(String paramName) {
			mCurrentConv = null;
			XCMAdminComponentMetadata md = XCMAdminInitHandler.getComponentMetadata(mCompId);
			if (md == null)
				return;
			
			String convClassString = md.getConvClassName(paramName);
			if (convClassString == null || convClassString.length() == 0)
				return;
			
			mCurrentDecodeMethodName = md.getDecodeMethodName(paramName);
			mCurrentEncodeMethodName = md.getEncodeMethodName(paramName);
			
			if (mCurrentConv != null && mCurrentConv.getClass().toString().equals(convClassString))
				return ;

			mCurrentConv = ParamConverterUtil.initConvObject(convClassString);
			}
		
		/**
		 * Returns true if the parameter is a JCO password
		 * @return true if the parameter is a JCO password
		 */
		public boolean isPassword(String name) {
			initConvObject(name);
			if (mCurrentConv != null && mCurrentConv instanceof PasswordConverter)
				return true;
			else
				return false;
		}

		/**
		 * Returns the decoded version of the parameter
		 * if there is a converter for it
		 */
		private String getStorageValue(String name, String value) {
			initConvObject(name);
			if (mCurrentConv != null) {
				if (ParamConverterUtil.isParamConverter(mCurrentConv))
					return ((ParamConverter)mCurrentConv).encode(value);	
				else
					return ParamConverterUtil.customEncode(mCurrentConv, mCurrentEncodeMethodName, mCompId, mName, name, value);				
			}
			else 
				return value;
			}

		/**
		 * Restores a derived value
		 * @param name name of parameter which value is to be restored
		 * @return the base value
		 */
		public String restoreValue(String name) {
			Element param = (Element)mParamNodes.get(name);
			String currentValue = param.getAttribute(XML_ATTR_VALUE);
			String derivedValue = param.getAttribute(XMLConstants.SAVE_PARAM_VALUE);			
			
//			if (currentValue.equals(derivedValue))
//				return derivedValue;
			
			param.setAttribute(XML_ATTR_VALUE, derivedValue);
			param.removeAttribute(XMLConstants.SAVE_PARAM_VALUE);
			XMLAdminUtil.removeElementId(param);
			
			return derivedValue;
		
		}	


		/**
		 * Returns <code>true</code>if this component configuration is 
		 * derived from an other configuration
		 * @return <code>true</code>if this component configuration is 
		 * derived from an other configuration
		 */
		public boolean isDerivedCompConfig() {
			return isDerivedCompConfig(mRootNode);
		}

		public boolean isDerivedNode() {
			return isDerivedNode(mRootNode);
		}

	
		/**
		 * Returns <code>true</code>if this component configuration is 
		 * derived from an other configuration
		 * @return <code>true</code>if this component configuration is 
		 * derived from an other configuration
		 */
		private static boolean isDerivedCompConfig(Node node) {

			if (node.getNodeType() != Node.ELEMENT_NODE)
				return false;
				
			Element element = (Element)node;
			String extended = element.getAttribute(XMLConstants.EXTENDED_NS);
			String base = element.getAttribute(XMLConstants.ISA_BASE_NS);
			if (extended.length() > 0 || base.length() > 0) {
				return true;
			}
			return false;
		}

		/**
		 * Returns true if node overwrites a base node
		 * @return
		 */
		public static boolean isDerivedNode(Node node) {
			boolean derived = isDerivedCompConfig(node);
			if (derived)
				return true;

			if (node.getNodeType() != Node.ELEMENT_NODE)
				return false;
			
			Element element = (Element)node;
			String elementID = element.getAttribute(XMLConstants.NODE_ID_NS);
			if (elementID != null && elementID.length() > 0)
				return true;
			else
				return false;

			
			
		}
		
		/**
		 * Returns an unmodifiable map of parameters. The Map contains
		 * name/value pairs. If there are parameters
		 * with an converter, then the display value of the 
		 * parameter is returned
		 * @return an unmodifiable map of parameters
		 */
		public Set getParamNames() {
			if (mParamNames == null) {
				mParamNames = new LinkedHashMap();
				for (Iterator iter = mParamNodes.keySet().iterator(); iter.hasNext();) {
					String paramName = (String)iter.next();
					mParamNames.put(paramName, paramName);
				}
			}
			return mParamNames.keySet();
		}
		
		/**
		 * Returns a parameter value. If there are parameters with an converter,
		 * then the display value of the parameter is returned
		 * @param paramName
		 * @return String value or null
		 */
		public String getValue(String paramName) {
			Element paramElement = (Element)mParamNodes.get(paramName); 
			String paramValue = paramElement.getAttribute(XML_ATTR_VALUE);
			return getDisplayValue(paramName, paramValue);

		}

		/**
		 * Returns the base value if the parameter has overwritten it
		 * Otherwise the value is an empty string
		 */
		public String getBaseValue(String paramName) {
			Element paramElement = (Element)mParamNodes.get(paramName);
			if (paramElement == null)
				return "";
			return paramElement.getAttribute(XMLConstants.SAVE_PARAM_VALUE);
		}
		
		/**
		 * Returns a shorttext of a parameter
		 */
		public String getShorttext(String paramName) {
			
			Element paramElement = (Element)mParamNodes.get(paramName);
			if (paramElement == null)
				return "";
				
			XCMAdminComponentMetadata md = XCMAdminInitHandler.getComponentMetadata(mCompId);
			if (md == null)
				return "";
			return XCMAdminInitHandler.getComponentMetadata(mCompId).getParamShorttext(paramName);
		}

		/**
		 * Returns a shorttext of a parameter
		 */
		public String getLongtext(String paramName) {
			
			Element paramElement = (Element)mParamNodes.get(paramName);
			if (paramElement == null)
				return "";
				
			XCMAdminComponentMetadata md = XCMAdminInitHandler.getComponentMetadata(mCompId);
			if (md == null)
				return "";
			return XCMAdminInitHandler.getComponentMetadata(mCompId).getParamLongtext(paramName);
		}

		
		/**
		 * Return the type of the parameter
		 */
		public String getType(String paramName) {
			
			XCMAdminComponentMetadata md = XCMAdminInitHandler.getComponentMetadata(mCompId);
			if (md == null)
				return XCMAdminAllowedValueMetadata.VALUE_TYPE_TEXT;
			return XCMAdminInitHandler.getComponentMetadata(mCompId).getParamType(paramName);
		}
		
		public Set getAllowedValues(String paramName) {
				
			XCMAdminComponentMetadata md = XCMAdminInitHandler.getComponentMetadata(mCompId);
			if (md == null)
				return Collections.EMPTY_SET;
			return md.getParamAllowedValues(paramName);
		}
		
		
		/**
		 * Returns <code>true</code> if the parameter is a derived parameter, 
		 * otherwise <code>false</code>
		 * @param paramName
		 * @return Returns <code>true</code> if the parameter is a derived parameter, 
		 *          otherwise <code>false</code>
		 */
		public boolean isDerived(String paramName) {
			Element param = (Element)mParamNodes.get(paramName);
			if (param == null)
				return false;
			String nodeId = param.getAttributeNS(XMLConstants.ISA_NAMESPACE, XMLConstants.NODE_ID);
			if (nodeId == null || nodeId.length() == 0)
				return true;
			else
				return false;
		}
		
		/**
		 * Sets a new value. If the value is a derived value and has a different
		 * value than the derived value, then it overwrites the derived value
		 * @param name name of parameter
		 */
		public void setValue(String name, String value) {
			Element param = (Element)mParamNodes.get(name);
			if (param == null)
				return;
			
			if(value == null) 
				value = "";
			
			String storageValue = getStorageValue(name, value);

			// if new value equals old value then do nothing 
			String oldValue = param.getAttribute(XML_ATTR_VALUE);
			
			if(oldValue == null)
				 oldValue = "";
				 
			if (oldValue.equals(storageValue)) 
				return;

			// the attribute should not have a savevalue attribute. If it has one
			// this should be kept
			String saveValue = param.getAttribute(XMLConstants.SAVE_PARAM_VALUE);
			if (isDerived(name) && saveValue != null) {
				// save the old  value
				param.setAttribute(XMLConstants.SAVE_PARAM_VALUE, oldValue);
				param.setAttribute(XML_ATTR_VALUE, storageValue);
				XMLAdminUtil.addElementId(param);
			} else
				param.setAttribute(XML_ATTR_VALUE, storageValue);
		}
		

		/**
		 * Returns a string representation of this parameter configuration
		 * @return a string representation of this parameter configuration
		 */
		public String toString () {
							
/*	StringBuffer sb = new StringBuffer();
			for (Iterator iter = mParamNodes.values().iterator(); iter.hasNext();) {
				sb.append(iter.next().toString());	
			}
*/			return mRootNode.toString();
		}
		
		/**
		 * Returns status of a parameter, e.g. deprecated
		 */
		public String getStatus(String paramName) {
			
			Element paramElement = (Element)mParamNodes.get(paramName);
			if (paramElement == null)
				return "";
				
			XCMAdminComponentMetadata md = XCMAdminInitHandler.getComponentMetadata(mCompId);
			if (md == null)
				return "";
			return XCMAdminInitHandler.getComponentMetadata(mCompId).getParamStatus(paramName);
		}

	}

	/**
	 * Returns a Params Container representing the configuration of a component
	 * with the given id
	 * The params container provides a simpler representation of the
	 * configuration which can be easily manipulated
	 * @param compId 
	 * @return a params container representing the configuration
	 */
	public CompConfig getCompConfig(String id) {
		return (CompConfig)mCompConfigs.get(id);	
	}
	
	/**
	 * Returns an unmodifiable Set containing all component ids
	 * @return an unmodifiable Set containing all component ids
	 */
	public Set getCompConfigs() {
		return mCompConfigs.keySet();
	}
	
	/**
	 * Returns a string representation of the XML file after removing 
	 * all nodes without <code>isa:elementid<code>
	 * @return 
	 */
	public String getExtendingDocAsString() {
		return XMLAdminUtil.getExtendingDocAsString(mDocAfterExtension);
	}
	
	/**
	 * Saves the current configuration to the file system
	 */
	public void saveConfiguration() throws XCMAdminException {
		forceEncryption();
		mXmlAdmin.saveConfiguration(getExtendingDocAsString());
	}
	
	public void invalidateConfiguration() throws XCMAdminException {
		// reset the Doc
		setDocBeforeInclude(mXmlAdmin.loadConfigurationDocument());
	}
	
	public void refreshConfiguration() throws XCMAdminException {
		mXmlAdmin.refreshConfiguration();
		setDocBeforeInclude(mXmlAdmin.loadConfigurationDocument());
	}
	
	public void overwriteConfiguration(Document docBeforeInclude) throws XCMAdminException {
		setDocBeforeInclude(docBeforeInclude);
	}
	
	public void lockConfiguration(LockManager lm) throws LockException {
		mXmlAdmin.lockConfiguration(lm);
	}
	
	public void unlockConfiguration(LockManager lm) throws LockException {
		mXmlAdmin.unlockConfiguration(lm);
	}
	
	/**
	 * <p>
	 * This method looks through the XCM Componenet meta data and triggers fresh encryption of secure data.
	 * This is required to insure that Secure data is not stale, if the 
	 * Secure Application Key has been changed by Administrator  
	 * </p>
	 */
	private void forceEncryption() {
		Iterator it = mCompConfigs.keySet().iterator();
		while(it.hasNext()) {
			CompConfig compConfig = (CompConfig)mCompConfigs.get(it.next());
			Iterator paramIt = compConfig.getParamConfigs().keySet().iterator();
			while(paramIt.hasNext()) {
				ParamConfig paramConfig = (ParamConfig)compConfig.getParamConfigs().get(paramIt.next());
				Set paramNames = paramConfig.getParamNames();
				Iterator paramNameIt = paramNames.iterator();
				while(paramNameIt.hasNext()) {
					String paramName = (String)paramNameIt.next();
					if(paramConfig.isPassword(paramName) && paramConfig.isDerivedNode()) {
						// reset to force reencryption
						paramConfig.setValue(paramName,paramConfig.getValue(paramName));
					}
				}
			}
		}
	}
	
	
	/**
	 * Returns the first ELEMENT Sibiling
	 * @param node
	 */
	public static Element getFirstElementSibling(Node node) {
		
		if (node == null)
			return null;
			
		if (node.getNodeType() == Node.ELEMENT_NODE)
			return (Element)node;

		while((node = node.getNextSibling())!= null) {
			if (node.getNodeType() == Node.ELEMENT_NODE)
				break;
		}
		return (Element)node;		
	}


}

