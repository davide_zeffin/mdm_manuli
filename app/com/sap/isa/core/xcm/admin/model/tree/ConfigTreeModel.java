package com.sap.isa.core.xcm.admin.model.tree;

// java imports
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.sap.isa.core.xcm.XCMUtils; 

import com.sap.isa.core.xcm.admin.model.CompDataManipulator;
import com.sap.isa.core.xcm.admin.model.ScenarioDataManipulator;
import com.sap.isa.core.xcm.admin.ui.XCMTreeNode;
import com.sap.isa.core.xcm.init.XCMAdminComponentMetadata;
import com.sap.isa.core.xcm.init.XCMAdminConstants;
import com.sapportals.htmlb.Component;
import com.sapportals.htmlb.GridLayout;
import com.sapportals.htmlb.GridLayoutCell;
import com.sapportals.htmlb.Image;
import com.sapportals.htmlb.TextView;
import com.sapportals.htmlb.TreeNode;
import com.sapportals.htmlb.enum.TextViewDesign;
import com.sap.isa.core.init.InitializationHandler;

/**
 *
 */
public class ConfigTreeModel extends TreeNode {

	private static org.apache.struts.util.MessageResources mMessageResources =
		InitializationHandler.getMessageResources();

	public static final String NO_NODE_ID = "NoNode";

	public static final String HOME_NODE_ID = "Start";
	public static final String HOME_NODE_TEXT =
		mMessageResources.getMessage("xcm.tree.home.text");
	public static final String HOME_NODE_TOOLTIP =
		mMessageResources.getMessage("xcm.tree.home.tooltip");

	public static final String OPTIONS_NODE_ID = "Options";
	public static final String OPTIONS_NODE_TEXT =
		mMessageResources.getMessage("xcm.tree.options.text");
	public static final String OPTIONS_NODE_TOOLTIP =
		mMessageResources.getMessage("xcm.tree.options.tooltip");

	public static final String SCENARIOS_NODE_ID = "Application Configurations";
	public static final String SCENARIOS_NODE_TEXT =
		mMessageResources.getMessage("xcm.tree.scenarios.text");
	public static final String SCENARIOS_NODE_TOOLTIP =
		mMessageResources.getMessage("xcm.tree.scenarios.tooltip");
	public static final String SCENARIOS_SAP_NODE_ID = "Scenarios.SAP";
	public static final String SCENARIOS_CUST_NODE_ID = "Scenarios.Customer";
	public static final String GENERAL_NODE_ID = "General";
	public static final String GENERAL_NODE_TEXT =
		mMessageResources.getMessage("xcm.tree.general.text");
	public static final String GENERAL_NODE_TOOLTIP =
		mMessageResources.getMessage("xcm.tree.general.tooltip");
	public static final String GENERAL_NODE_TOOLTIP_ACCESS =
		mMessageResources.getMessage("xcm.tree.general.tooltip.access");

	public static final String GENERAL_CUST_NODE_ID = "General.Customer";
	public static final String GENERAL_CUST_NODE_TEXT =
		mMessageResources.getMessage("xcm.tree.general.cust.text");
	public static final String GENERAL_CUST_NODE_TOOLTIP =
		mMessageResources.getMessage("xcm.tree.general.cust.tooltip");
	public static final String GENERAL_CUST_NODE_TOOLTIP_ACCESS =
		mMessageResources.getMessage("xcm.tree.general.cust.tooltip.access");

	public static final String COMP_NODE_ID = "Components";
	public static final String COMP_NODE_TEXT =
		mMessageResources.getMessage("xcm.tree.comp.text");
	public static final String COMP_NODE_TOOLTIP =
		mMessageResources.getMessage("xcm.tree.comp.tooltip");
	public static final String COMP_SAP_NODE_ID = "Components.SAP";
	public static final String COMP_CUST_NODE_ID = "Components.Customer";

	public static final String SAP_NODE_TEXT =
		mMessageResources.getMessage("xcm.tree.sap.text");
	public static final String SAP_NODE_TOOLTIP =
		mMessageResources.getMessage("xcm.tree.sap.tooltip");
	public static final String SAP_NODE_TOOLTIP_ACCESS =
		mMessageResources.getMessage("xcm.tree.sap.tooltip.access");
	public static final String CUST_NODE_TEXT =
		mMessageResources.getMessage("xcm.tree.general.cust.text");
	public static final String CUST_NODE_TOOLTIP =
		mMessageResources.getMessage("xcm.tree.cust.tooltip");
	public static final String CUST_NODE_TOOLTIP_ACCESS =
		mMessageResources.getMessage("xcm.tree.sap.cust.tooltip.access");

	public static final String SAP_NODE_ID_EXTENSION = "SAP";
	public static final String CUST_NODE_ID_EXTENSION = "CUS";

	public static final String CUST_GENERAL_NODE_ID_EXTENSION = "GEN_CUS";
	public static final String PARAMETER_SET1 =
		mMessageResources.getMessage("xcm.tree.param.set1.tooltip");
	public static final String PARAMETER_SET2 =
		mMessageResources.getMessage("xcm.tree.param.set2.tooltip");
	public static final String CONF_FOR_COMP_TOOLTIP =
		mMessageResources.getMessage("xcm.tree.conf.for.comp.tooltip");
	public static final String SCENARIO_TOOLTIP =
		mMessageResources.getMessage("xcm.tree.scenario.conf.tooltip");
	public static final String GREEN_LIGHT_TOOLTIP =
		mMessageResources.getMessage("xcm.tree.green.tooltip");
	public static final String RED_LIGHT_TOOLTIP =
		mMessageResources.getMessage("xcm.tree.red.tooltip");
	public static final String EMPTY_LIGHT1_TOOLTIP =
		mMessageResources.getMessage("xcm.tree.empty1.tooltip");
	public static final String EMPTY_LIGHT2_TOOLTIP =
		mMessageResources.getMessage("xcm.tree.empty2.tooltip");
	public static final String DEPRECATED_COMPONENT_TOOLTIP =
		mMessageResources.getMessage("xcm.tree.deprecated.tooltip");

	public static final String DEPRECATED_COMPONENT_CONFIG_TOOLTIP =
		mMessageResources.getMessage("xcm.tree.deprecated.compconfig.tooltip");

	

	public static final String DEPRECATED_SCENARIO_TOOLTIP =
		mMessageResources.getMessage("xcm.tree.deprecated.scen.tooltip");

	public static final String COMP_SAP_NODE_TOOLTIP_ACCESS =
		mMessageResources.getMessage("xcm.tree.conf.for.comp.sap.access");
	public static final String COMP_CUST_NODE_TOOLTIP_ACCESS =
		mMessageResources.getMessage("xcm.tree.conf.for.comp.cust.access");

	public static final String COMP_SAP_NODE_TOOLTIP =
		mMessageResources.getMessage("xcm.tree.conf.for.comp.sap");
	public static final String COMP_CUST_NODE_TOOLTIP =
		mMessageResources.getMessage("xcm.tree.conf.for.comp.cust");


	public static final String HOME_ON_CLICK_EVENT =
		"HomeNodeSelect.ConfigTreeForm.toolbar.actionform.admin.xcm.core.isa.sap.com";

	public static final String COMPONENT_ON_CLICK_EVENT =
		"CompNodeSelect.ConfigTreeForm.toolbar.actionform.admin.xcm.core.isa.sap.com";

	public static final String SCENARIO_ON_CLICK_EVENT =
		"ScenNodeSelect.ConfigTreeForm.toolbar.actionform.admin.xcm.core.isa.sap.com";

	public static final String CONFIG_ON_CLICK_EVENT =
		"ConfigNodeSelect.ConfigTreeForm.toolbar.actionform.admin.xcm.core.isa.sap.com";

	public static final String GENERAL_CONFIG_ON_CLICK_EVENT =
		"GeneralConfigNodeSelect.ConfigTreeForm.toolbar.actionform.admin.xcm.core.isa.sap.com";

	public static final String MODE_ON_CLICK_EVENT =
		"Mode.ConfigTreeForm.toolbar.actionform.admin.xcm.core.isa.sap.com";

	public static final String TREE_NODE_EXPAND_EVENT =
		"NodeExpandEvent.toolbar.actionform.admin.xcm.core.isa.sap.com";

	public static final String TREE_NODE_CLOSE_EVENT =
		"NodeCloseEvent.toolbar.actionform.admin.xcm.core.isa.sap.com";

	public static final String APPL_GENERAL_ON_CLICK_EVENT =
		"general.application.ConfigTreeForm.toolbar.actionform.admin.xcm.core.isa.sap.com";

	public boolean accessibility;

	private TreeNode mHomeNode;
	private TreeNode mOptionsNode;
	private TreeNode mScenariosNode;
	private TreeNode mComponentsNode;
	private TreeNode mScenariosSAPNode;
	private TreeNode mComponentsSAPNode;
	private TreeNode mScenariosCustNode;
	private TreeNode mComponentsCustNode;
	private TreeNode mCompNodeSAP;
	private TreeNode mCompNodeCust;
	private TreeNode mGeneralCompNodeCust;
	private TreeNode mGeneralNode;
	private TreeNode mGeneralCustNode;

	private TreeNode mCurrentActiveNode;

	private CompDataManipulator mConfigData;
	private ScenarioDataManipulator mScenarioData;

	/**
	 * Initializes the configuration tree 
	 * @param test id of root node
	 * @param configData object representing the configuration data
	 */
	public ConfigTreeModel(
		String text,
		CompDataManipulator configData,
		ScenarioDataManipulator scenData,
		boolean accessible) {
		super(text);
		if (mMessageResources == null)
			mMessageResources = InitializationHandler.getMessageResources();
		mConfigData = configData;
		mScenarioData = scenData;
		accessibility = accessible;
		init();
	}

	private void init() {

		mHomeNode = new TreeNode(HOME_NODE_ID);
		mHomeNode.setText(HOME_NODE_ID);
		TextView tw = new TextView(HOME_NODE_TEXT);
		tw.setDesign(TextViewDesign.EMPHASIZED);
		mHomeNode.setComponent(tw);
		mCurrentActiveNode = mHomeNode;
		mHomeNode.setTooltip(HOME_NODE_TOOLTIP);
		if (accessibility == false) {
			mHomeNode.setOnNodeExpand(TREE_NODE_EXPAND_EVENT);
			mHomeNode.setOnNodeClose(TREE_NODE_CLOSE_EVENT);
		}
		mHomeNode.setOnNodeClick(HOME_ON_CLICK_EVENT);
		mHomeNode.setOpen(true);
		this.addChildNode(mHomeNode);

		mOptionsNode = new XCMTreeNode(OPTIONS_NODE_ID);
		//mConfigurationNode.setText(OPTIONS_NODE_ID);
		mOptionsNode.setComponent(new TextView(OPTIONS_NODE_TEXT));
		mOptionsNode.setOnNodeClick(MODE_ON_CLICK_EVENT);
		if (accessibility == false) {
			mOptionsNode.setOnNodeExpand(TREE_NODE_EXPAND_EVENT);
			mOptionsNode.setOnNodeClose(TREE_NODE_CLOSE_EVENT);
		}
		mOptionsNode.setTooltip(OPTIONS_NODE_TOOLTIP);

		mHomeNode.addChildNode(mOptionsNode);

		// general application settings 
		mGeneralNode = new XCMTreeNode(GENERAL_NODE_ID);
		mGeneralNode.setComponent(new TextView(GENERAL_NODE_TEXT));
		mGeneralNode.setTooltip(GENERAL_NODE_TOOLTIP);

		if (accessibility == true)
			mGeneralNode.setTooltip(GENERAL_NODE_TOOLTIP_ACCESS);
		if (accessibility == false) {
			mGeneralNode.setOnNodeExpand(TREE_NODE_EXPAND_EVENT);
			mGeneralNode.setOnNodeClose(TREE_NODE_CLOSE_EVENT);
		}
		mGeneralNode.setOpen(true);
		mHomeNode.addChildNode(mGeneralNode);

		// general customer settings
		mGeneralCustNode = new XCMTreeNode(GENERAL_CUST_NODE_ID);
		mGeneralCustNode.setComponent(new TextView(GENERAL_CUST_NODE_TEXT));
		mGeneralCustNode.setTooltip(GENERAL_CUST_NODE_TOOLTIP);
		if (accessibility == true)
			mGeneralCustNode.setTooltip(GENERAL_CUST_NODE_TOOLTIP_ACCESS);
		if (accessibility == false) {
			mGeneralCustNode.setOnNodeExpand(TREE_NODE_EXPAND_EVENT);
			mGeneralCustNode.setOnNodeClose(TREE_NODE_CLOSE_EVENT);
		}
		mGeneralNode.addChildNode(mGeneralCustNode);

		mScenariosNode = new XCMTreeNode(SCENARIOS_NODE_ID);
		//mScenariosNode.setText(SCENARIOS_NODE_ID);
		mScenariosNode.setComponent(new TextView(SCENARIOS_NODE_TEXT));
		mScenariosNode.setOnNodeClick(MODE_ON_CLICK_EVENT);
		if (accessibility == false) {
			mScenariosNode.setOnNodeExpand(TREE_NODE_EXPAND_EVENT);
			mScenariosNode.setOnNodeClose(TREE_NODE_CLOSE_EVENT);
		}
		mScenariosNode.setTooltip(SCENARIOS_NODE_TOOLTIP);
		mScenariosNode.setOpen(true);
		mScenariosNode.setShowExpander(false);
		mHomeNode.addChildNode(mScenariosNode);

		mScenariosSAPNode = new XCMTreeNode(SCENARIOS_SAP_NODE_ID);
		mScenariosSAPNode.setComponent(new TextView(SAP_NODE_TEXT));
		mScenariosSAPNode.setTooltip(SAP_NODE_TOOLTIP);
		if (accessibility == true)
			mScenariosSAPNode.setTooltip(SAP_NODE_TOOLTIP_ACCESS);
		if (accessibility == false) {
			mScenariosSAPNode.setOnNodeExpand(TREE_NODE_EXPAND_EVENT);
			mScenariosSAPNode.setOnNodeClose(TREE_NODE_CLOSE_EVENT);
		}
		mScenariosNode.addChildNode(mScenariosSAPNode);

		mScenariosCustNode = new XCMTreeNode(SCENARIOS_CUST_NODE_ID);
		mScenariosCustNode.setComponent(new TextView(CUST_NODE_TEXT));
		mScenariosCustNode.setTooltip(CUST_NODE_TOOLTIP_ACCESS);
		if (accessibility == true)
			mScenariosCustNode.setTooltip(CUST_NODE_TOOLTIP_ACCESS);
		if (accessibility == false) {
			mScenariosCustNode.setOnNodeExpand(TREE_NODE_EXPAND_EVENT);
			mScenariosCustNode.setOnNodeClose(TREE_NODE_CLOSE_EVENT);
		}
		mScenariosNode.addChildNode(mScenariosCustNode);

		mComponentsNode = new XCMTreeNode(COMP_NODE_ID);
		//mComponentsNode.setText(COMP_NODE_ID);
		mComponentsNode.setComponent(new TextView(COMP_NODE_TEXT));
		mComponentsNode.setOnNodeClick(MODE_ON_CLICK_EVENT);
		if (accessibility == false) {
			mComponentsNode.setOnNodeExpand(TREE_NODE_EXPAND_EVENT);
			mComponentsNode.setOnNodeClose(TREE_NODE_CLOSE_EVENT);
		}
		mComponentsNode.setTooltip(COMP_NODE_TOOLTIP);
		mComponentsNode.setOpen(true);
		mHomeNode.addChildNode(mComponentsNode);

		mComponentsSAPNode = new XCMTreeNode(COMP_SAP_NODE_ID);
		//mComponentsSAPNode.setText(SAP_NODE_TEXT);
		mComponentsSAPNode.setComponent(new TextView(SAP_NODE_TEXT));
		mComponentsSAPNode.setTooltip(COMP_SAP_NODE_TOOLTIP);
		if (accessibility == true)
			mComponentsSAPNode.setTooltip(COMP_SAP_NODE_TOOLTIP_ACCESS);
		if (accessibility == false) {
			mComponentsSAPNode.setOnNodeExpand(TREE_NODE_EXPAND_EVENT);
			mComponentsSAPNode.setOnNodeClose(TREE_NODE_CLOSE_EVENT);
		}
		mComponentsNode.addChildNode(mComponentsSAPNode);

		mComponentsCustNode = new XCMTreeNode(COMP_CUST_NODE_ID);
		//mComponentsCustNode.setText(CUST_NODE_TEXT);
		if (accessibility == true)
			mComponentsCustNode.setTooltip(COMP_CUST_NODE_TOOLTIP_ACCESS);

		mComponentsCustNode.setComponent(new TextView(CUST_NODE_TEXT));
		mComponentsCustNode.setTooltip(COMP_CUST_NODE_TOOLTIP);
		if (accessibility == false) {
			mComponentsCustNode.setOnNodeExpand(TREE_NODE_EXPAND_EVENT);
			mComponentsCustNode.setOnNodeClose(TREE_NODE_CLOSE_EVENT);
		}
		mComponentsNode.addChildNode(mComponentsCustNode);

		// add general configuration nodes
		addGeneralNoder();

		// add tree information of components 
		addComponentNodes();

		// add tree information of scenarios
		addScenarioNodes();
	}

	private void addScenarioNodes() {

		Map scenarios = mScenarioData.getScenarioConfigs();
		for (Iterator iter = scenarios.values().iterator(); iter.hasNext();) {
			ScenarioDataManipulator.ScenarioConfig scenarioConfig =
				(ScenarioDataManipulator.ScenarioConfig) iter.next();
			String scenarioName = scenarioConfig.getName();

			TreeNode scenarioNode = null;
			if (scenarioConfig.isScenarioDerived()) {
				scenarioNode =
					new XCMTreeNode(CUST_NODE_ID_EXTENSION + scenarioName);
				mScenariosCustNode.addChildNode(scenarioNode);
			} else {
				if (scenarioConfig.isObsolete())
					continue;
					
					scenarioNode =
						new XCMTreeNode(SAP_NODE_ID_EXTENSION + scenarioName);
					mScenariosSAPNode.addChildNode(scenarioNode);
			}
			scenarioNode.setText(scenarioName);

			scenarioNode.setComponent(
				getScenarioName(
					scenarioName,
					scenarioConfig.isDefaultScenario(),
					scenarioConfig.isActiveScenario(),
					scenarioConfig.isObsolete()));

			String descr;

			if (scenarioConfig.isDefaultScenario())
				descr =
					mScenarioData
						.getScenarioConfig(scenarioName)
						.getShorttext()
						+ " "
						+ GREEN_LIGHT_TOOLTIP;
			else
				descr =
					mScenarioData
						.getScenarioConfig(scenarioName)
						.getShorttext()
						+ " "
						+ EMPTY_LIGHT1_TOOLTIP
						+ scenarioName
						+ EMPTY_LIGHT2_TOOLTIP;
			scenarioNode.setTooltip(descr);
			scenarioNode.setOnNodeClick(SCENARIO_ON_CLICK_EVENT);

		}

	}

	private void addGeneralNoder() {

		Set components = mConfigData.getCompConfigs();

		for (Iterator iter1 = components.iterator(); iter1.hasNext();) {
			String compId = (String) iter1.next();

			if (!XCMUtils.isApplicationScopeCompConfig(compId))
				continue;

			String compIdCust = CUST_GENERAL_NODE_ID_EXTENSION + compId;

			CompDataManipulator.CompConfig compConfig =
				mConfigData.getCompConfig(compId);

			mGeneralCompNodeCust = new XCMTreeNode(compIdCust);
			String tooltip = mMessageResources.getMessage("xcm.tree.general.config", compId);

			if (compConfig.getStatus().equalsIgnoreCase(XCMAdminConstants.OBSOLETE)) {
				GridLayout grid = new GridLayout(1, 2);
				grid.setCellPadding(3);
				Image deprecatedCompImage =
					new Image("mimes/ICON_DEPRECATED.gif", DEPRECATED_COMPONENT_TOOLTIP);
				TextView compNameTW = new TextView(compId);
				compNameTW.setTooltip(tooltip + DEPRECATED_COMPONENT_TOOLTIP);
				grid.addComponent(1, 1, deprecatedCompImage);
				grid.addComponent(1, 2, compNameTW);
				mGeneralCompNodeCust.setComponent(grid);
				mGeneralCompNodeCust.setTooltip(tooltip + DEPRECATED_COMPONENT_TOOLTIP);
			} else {
				mGeneralCompNodeCust.setComponent(new TextView(compId));
				mGeneralCompNodeCust.setTooltip(tooltip);
			}
			mGeneralCompNodeCust.setText(compId);

			
			// add component node
			mGeneralCustNode.addChildNode(mGeneralCompNodeCust);
	
			
			if (accessibility == false) {
				mGeneralCompNodeCust.setOnNodeExpand(TREE_NODE_EXPAND_EVENT);
				mGeneralCompNodeCust.setOnNodeClose(TREE_NODE_CLOSE_EVENT);
			}

			Map paramConfigs = compConfig.getParamConfigs();
			for (Iterator iter2 = paramConfigs.keySet().iterator();
				iter2.hasNext();
				) {

				String paramConfigId = (String) iter2.next();

				CompDataManipulator.ParamConfig paramConfig =
					compConfig.getParamConfig(paramConfigId);

				// check if configuration should not be displayed
				// this is special behavior needed because of backward compatibility
				if (paramConfig
					.getXCMControl()
					.equals(CompDataManipulator.INVISIBLE_XCM_CONTROL)
					&& !paramConfig.isDerivedCompConfig())
					continue;

				// check if config derived; if yes do not display

				if (!paramConfig.isDerivedNode()) {
					// create a new derived configuration with the same name and display it
					String paramConfigName = paramConfig.getId();
					compConfig.createParamConfig(
						paramConfigName,
						paramConfigName);
				}

				TreeNode paramNode = new XCMTreeNode(compId + paramConfigId);
				mGeneralCompNodeCust.addChildNode(paramNode);

				paramNode.setText(paramConfigId);
				paramNode.setComponent(new TextView(paramConfigId));
				paramNode.setTooltip(
					PARAMETER_SET1
						+ paramConfigId
						+ PARAMETER_SET2
						+ compId
						+ "'");
				paramNode.setOnNodeClick(CONFIG_ON_CLICK_EVENT);

			}

		}
	}

	private void addComponentNodes() {

		Set components = mConfigData.getCompConfigs();
		//Set components = mConfigData.getGeneralCompConfigs();
		for (Iterator iter1 = components.iterator(); iter1.hasNext();) {
			String compId = (String) iter1.next();
			if (XCMUtils.isApplicationScopeCompConfig(compId))
				continue;

			String compIdSAP = SAP_NODE_ID_EXTENSION + compId;
			String compIdCust = CUST_NODE_ID_EXTENSION + compId;

			mCompNodeSAP = new XCMTreeNode(compIdSAP);
			mCompNodeCust = new XCMTreeNode(compIdCust);
			String tooltip = CONF_FOR_COMP_TOOLTIP + compId + "'. ";
			CompDataManipulator.CompConfig compConfig =
				mConfigData.getCompConfig(compId);

			if (compConfig.getStatus().equalsIgnoreCase(XCMAdminConstants.OBSOLETE)) {
				if (!compConfig.isDescendendParamConfigAvailable())
					continue;
				GridLayout grid = new GridLayout(1, 2);
				grid.setCellPadding(3);
				Image deprecatedCompImage =
					new Image("mimes/ICON_DEPRECATED.gif", DEPRECATED_COMPONENT_TOOLTIP);
				TextView compNameTW = new TextView(compId);
				compNameTW.setTooltip(tooltip + DEPRECATED_COMPONENT_TOOLTIP);
				grid.addComponent(1, 1, deprecatedCompImage);
				grid.addComponent(1, 2, compNameTW);
				mCompNodeCust.setComponent(grid);
				mCompNodeCust.setTooltip(tooltip + DEPRECATED_COMPONENT_TOOLTIP);
			} else {
				mCompNodeSAP.setComponent(new TextView(compId));
				mCompNodeCust.setComponent(new TextView(compId));
				mCompNodeSAP.setTooltip(tooltip);
				mCompNodeCust.setTooltip(tooltip);
			}
			mCompNodeSAP.setText(compId);
			mCompNodeCust.setText(compId);

			// add component node
			if (!compConfig.getStatus().equalsIgnoreCase(XCMAdminConstants.OBSOLETE)) 
				mComponentsSAPNode.addChildNode(mCompNodeSAP);

			// add component node only if there are param configurations available				
			mComponentsCustNode.addChildNode(mCompNodeCust);

			mCompNodeSAP.setOnNodeClick(COMPONENT_ON_CLICK_EVENT);
			if (accessibility == false) {
				mCompNodeSAP.setOnNodeExpand(TREE_NODE_EXPAND_EVENT);
				mCompNodeSAP.setOnNodeClose(TREE_NODE_CLOSE_EVENT);
			}

			mCompNodeCust.setOnNodeClick(COMPONENT_ON_CLICK_EVENT);
			if (accessibility == false) {
				mCompNodeCust.setOnNodeExpand(TREE_NODE_EXPAND_EVENT);
				mCompNodeCust.setOnNodeClose(TREE_NODE_CLOSE_EVENT);
			}

			Map paramConfigs = compConfig.getParamConfigs();
			boolean isCustomerConfigAvailable = false;
			for (Iterator iter2 = paramConfigs.keySet().iterator();
				iter2.hasNext();
				) {
				String paramConfigId = (String) iter2.next();
				CompDataManipulator.ParamConfig paramConfig =
					compConfig.getParamConfig(paramConfigId);
				TreeNode paramNode = null;
				if (paramConfig.isDerivedCompConfig()) {
					paramNode = new XCMTreeNode(compIdCust + paramConfigId);
				} else {
					paramNode = new XCMTreeNode(compIdSAP + paramConfigId);
				}
				paramNode.setText(paramConfigId);
				String baseParamConfigId = paramConfig.getBaseConfigId();
				// add only deprecated customer parameter configurations
				if ((compConfig.getStatus().equalsIgnoreCase(XCMAdminComponentMetadata.OBSOLETE))||
					XCMUtils.isCompConfigObsolete(compConfig.getCompId(), paramConfigId) ||
					XCMUtils.isCompConfigObsolete(compConfig.getCompId(), baseParamConfigId)) {
					
					// do not add obsolete sap param configs
					if (baseParamConfigId == null || baseParamConfigId.length() == 0)
						continue;
						
					GridLayout grid = new GridLayout(1, 2);
					isCustomerConfigAvailable = true; 
					grid.setCellPadding(3);
					Image deprecatedCompImage =
						new Image("mimes/ICON_DEPRECATED.gif", DEPRECATED_COMPONENT_CONFIG_TOOLTIP);
					TextView compNameTW = new TextView(paramConfigId);
					compNameTW.setTooltip(PARAMETER_SET1 + paramConfigId + PARAMETER_SET2 + compId + "'. " + DEPRECATED_COMPONENT_CONFIG_TOOLTIP);
					grid.addComponent(1, 1, deprecatedCompImage);
					grid.addComponent(1, 2, compNameTW);
					paramNode.setComponent(grid);
					paramNode.setTooltip(PARAMETER_SET1 + paramConfigId + PARAMETER_SET2 + compId + "'" + DEPRECATED_COMPONENT_TOOLTIP);
				} else {
					paramNode.setComponent(new TextView(paramConfigId));
					paramNode.setTooltip(PARAMETER_SET1 + paramConfigId + PARAMETER_SET2 + compId + "'");
				}
				if (paramConfig.isDerivedCompConfig()) {
					mCompNodeCust.addChildNode(paramNode);
				} else {
					mCompNodeSAP.addChildNode(paramNode);
				}
				paramNode.setOnNodeClick(CONFIG_ON_CLICK_EVENT);
				paramNode.setText(paramConfigId);
			}
		}
	}

	/**
	 * Updates the child nodes of the current node
	 */
	public void updateCurrentNode() {

		// check if current node is a component root node

		if (mCurrentActiveNode.getID().startsWith(SAP_NODE_ID_EXTENSION)
			|| mCurrentActiveNode.getID().startsWith(CUST_NODE_ID_EXTENSION)) {
			// delete children of node
			for (Enumeration enum = mCurrentActiveNode.getChildNodes();
				enum.hasMoreElements();
				) {
				mCurrentActiveNode.removeChildNode(
					(TreeNode) enum.nextElement());
			}
			// add children to node from data source			
		}
	}

	/**
	 * Sets the currently active node
	 */
	public void setActiveNode(TreeNode node) {
		if (mCurrentActiveNode != null) {
			Component com = mCurrentActiveNode.getComponent();
			//TextView tw = (TextView)mCurrentActiveNode.getComponent();
			TextView tw = getNameTextView(com);
			tw.setDesign(TextViewDesign.STANDARD);
		}

		TextView tw = getNameTextView(node.getComponent());
		tw.setDesign(TextViewDesign.EMPHASIZED);
		mCurrentActiveNode = node;
		//openParentNode(node);
	}

	/**
	 * Sets the currently active node
	 */
	public TreeNode getActiveNode() {
		return mCurrentActiveNode;
	}

	public void updateCompNodes(String compId, boolean isDeleted) {

		if (isDeleted)
			setActiveNode(mCurrentActiveNode.getParentNode());

		deleteChildNodes(mCurrentActiveNode);

		String currentCompId = mCurrentActiveNode.getID();

		String compIdSAP = SAP_NODE_ID_EXTENSION + compId;
		String compIdCust = CUST_NODE_ID_EXTENSION + compId;

		CompDataManipulator.CompConfig compConfig =
			mConfigData.getCompConfig(compId);
		Map paramConfigs = compConfig.getParamConfigs();
		for (Iterator iter2 = paramConfigs.keySet().iterator();	iter2.hasNext();) {
			String paramConfigId = (String) iter2.next();
			CompDataManipulator.ParamConfig paramConfig =
				compConfig.getParamConfig(paramConfigId);
			TreeNode paramNode = null;

			if (paramConfig.isDerivedCompConfig() && compIdCust.equals(currentCompId)) {
				paramNode = new XCMTreeNode(compIdCust + paramConfigId);
			}
			if (paramConfig.isDerivedCompConfig() && compIdSAP.equals(currentCompId)) {
				paramNode = new XCMTreeNode(compIdSAP + paramConfigId);
			}

			
			String tooltip = CONF_FOR_COMP_TOOLTIP + compId + "'. ";
			String baseParamConfigId = paramConfig.getBaseConfigId();
			if ((compConfig.getStatus().equalsIgnoreCase(XCMAdminComponentMetadata.OBSOLETE))||
				XCMUtils.isCompConfigObsolete(compConfig.getCompId(), paramConfigId) ||
				XCMUtils.isCompConfigObsolete(compConfig.getCompId(), baseParamConfigId)) {
				// do not add obsolete sap param configs
				if (baseParamConfigId == null || baseParamConfigId.length() == 0)
					continue;
				if (paramNode == null)
					continue;
						
				GridLayout grid = new GridLayout(1, 2); 
				grid.setCellPadding(3);
				Image deprecatedCompImage =
					new Image("mimes/ICON_DEPRECATED.gif", DEPRECATED_COMPONENT_CONFIG_TOOLTIP);
				TextView compNameTW = new TextView(paramConfigId);
				compNameTW.setTooltip(PARAMETER_SET1 + paramConfigId + PARAMETER_SET2 + compId + "'. " + DEPRECATED_COMPONENT_CONFIG_TOOLTIP);
				grid.addComponent(1, 1, deprecatedCompImage);
				grid.addComponent(1, 2, compNameTW);
				paramNode.setComponent(grid);
				paramNode.setTooltip(PARAMETER_SET1 + paramConfigId + PARAMETER_SET2 + compId + "'" + DEPRECATED_COMPONENT_TOOLTIP);
				paramNode.setOnNodeClick(CONFIG_ON_CLICK_EVENT);
				paramNode.setText(paramConfigId);
			} else if (paramNode != null) {
				paramNode.setComponent(new TextView(paramConfigId));
				paramNode.setText(paramConfigId);
				paramNode.setTooltip(PARAMETER_SET1 + paramConfigId	+ PARAMETER_SET2 + compId + "'");
				paramNode.setOnNodeClick(CONFIG_ON_CLICK_EVENT);
			}
			if (paramConfig.isDerivedCompConfig() && compIdCust.equals(currentCompId)) {
				mCurrentActiveNode.addChildNode(paramNode);
			}
			if (paramConfig.isDerivedCompConfig() && compIdSAP.equals(currentCompId)) {
				mCurrentActiveNode.addChildNode(paramNode);
			}


		}
	}

	public void updateScenNodes() {

		deleteChildNodes(mScenariosCustNode);

		Map scenarios = mScenarioData.getScenarioConfigs();

		for (Iterator iter = scenarios.values().iterator(); iter.hasNext();) {
			ScenarioDataManipulator.ScenarioConfig scenarioConfig =
				(ScenarioDataManipulator.ScenarioConfig) iter.next();
			String scenarioName = scenarioConfig.getName();

			TreeNode scenarioNode = null;
			if (scenarioConfig.isScenarioDerived()) {
				scenarioNode =
					new XCMTreeNode(CUST_NODE_ID_EXTENSION + scenarioName);
				mScenariosCustNode.addChildNode(scenarioNode);
				scenarioNode.setText(scenarioName);
				scenarioNode.setComponent(
					getScenarioName(
						scenarioName,
						scenarioConfig.isDefaultScenario(),
						scenarioConfig.isActiveScenario(), 
						scenarioConfig.isObsolete()));
				scenarioNode.setTooltip(SCENARIO_TOOLTIP + scenarioName + "'");
				scenarioNode.setOnNodeClick(SCENARIO_ON_CLICK_EVENT);
			}
		}
	}

	public void setOptionsActiveTreeNode() {
		setActiveNode(mOptionsNode);
		openParentNode(mOptionsNode);
	}

	public void setScenariosActiveTreeNode() {
		setActiveNode(mScenariosNode);
		openParentNode(mScenariosNode);
	}

	public void setScenariosCustomerActiveTreeNode() {
		setActiveNode(mScenariosCustNode);
		openParentNode(mScenariosCustNode);
	}

	public void setComponentsActiveTreeNode() {
		setActiveNode(mComponentsNode);
		openParentNode(mComponentsNode);
	}

	/**
	 * Sets the active node for the given node Id
	 */
	public void setComponentActiveNode(
		String compId,
		String paramConfigId,
		boolean isSAP) {
		String treeNodeId = null;

		if (paramConfigId == null)
			paramConfigId = "";

		if (isSAP)
			treeNodeId = SAP_NODE_ID_EXTENSION + compId + paramConfigId;
		else
			treeNodeId = CUST_NODE_ID_EXTENSION + compId + paramConfigId;

		TreeNode node = mHomeNode.getNodeByID(treeNodeId);

		if (node == null)
			return;
		setActiveNode(node);
		openParentNode(node);
	}

	public void setScenarioActiveNode(String scenName, boolean isSAP) {
		String treeNodeId = null;
		if (isSAP)
			treeNodeId = SAP_NODE_ID_EXTENSION + scenName;
		else
			treeNodeId = CUST_NODE_ID_EXTENSION + scenName;

		TreeNode node = mHomeNode.getNodeByID(treeNodeId);
		if (node == null)
			return;
		setActiveNode(node);
	}

	private void openParentNode(TreeNode node) {
		node.setOpen(true);
		TreeNode parentNode = node.getParentNode();
		if (parentNode == null)
			return;
		else
			openParentNode(parentNode);
	}

	public void expandNode(String nodeId, boolean isOpen) {

		TreeNode expandNode = null;

		if (nodeId.equals(SCENARIOS_SAP_NODE_ID))
			expandNode = mScenariosSAPNode;

		if (nodeId.equals(SCENARIOS_CUST_NODE_ID))
			expandNode = mScenariosCustNode;

		if (nodeId.equals(SCENARIOS_NODE_ID))
			expandNode = mScenariosNode;

		if (nodeId.equals(COMP_SAP_NODE_ID))
			expandNode = mComponentsSAPNode;

		if (nodeId.equals(COMP_CUST_NODE_ID))
			expandNode = mComponentsCustNode;

		if (nodeId.equals(COMP_NODE_ID))
			expandNode = mComponentsNode;

		expandNode.setOpen(isOpen);
		if (isOpen)
			openParentNode(expandNode);
	}

	private void deleteChildNodes(TreeNode parentNode) {
		// delete children of currently active node
		Set childNodes = new HashSet();
		for (Enumeration enum = parentNode.getChildNodes();
			enum.hasMoreElements();
			) {
			TreeNode currentNode = (TreeNode) enum.nextElement();
			childNodes.add(currentNode);
		}
		// delete child nodes
		for (Iterator iter = childNodes.iterator(); iter.hasNext();) {
			TreeNode currentNode = (TreeNode) iter.next();
			parentNode.removeChildNode(currentNode);
		}
	}

	public static TextView getNameTextView(Component comp) {
		if (comp instanceof TextView)
			return (TextView) comp;
		else {
			GridLayout gl = (GridLayout) comp;
			GridLayoutCell cell = gl.getCell(1, 2);
			if (cell.getContent() instanceof TextView)
				return (TextView) cell.getContent();
			else {
				cell = gl.getCell(1, 3);
				return (TextView) cell.getContent();
			}
		}
	}

	private GridLayout getScenarioName(String name, 
		boolean isDefault, boolean isActive, boolean isDeprecated) {
		GridLayout gl = null; 
		Image defaultScenarioImage = null;
		Image activeScenarioImage = null;
		Image deprecatedScenarioImage = null;
		TextView scenarioNameTW = new TextView(name);
		if (isDefault) {
			defaultScenarioImage =
				new Image("mimes/icon_led_green.gif", GREEN_LIGHT_TOOLTIP);
			scenarioNameTW.setTooltip(GREEN_LIGHT_TOOLTIP);
		} else {
			scenarioNameTW.setTooltip(
				EMPTY_LIGHT1_TOOLTIP + name + EMPTY_LIGHT2_TOOLTIP);
		}
		if (!isActive) {
			activeScenarioImage =
				new Image("mimes/ICON_LED_RED.gif", RED_LIGHT_TOOLTIP);
			scenarioNameTW.setTooltip(RED_LIGHT_TOOLTIP);
		} 
		
		if (isDeprecated) {
			deprecatedScenarioImage =
				new Image("mimes/ICON_DEPRECATED.gif", DEPRECATED_SCENARIO_TOOLTIP);
			scenarioNameTW.setTooltip(DEPRECATED_SCENARIO_TOOLTIP);
			gl = new GridLayout(1, 3);
			gl.addComponent(1, 1, deprecatedScenarioImage);
			gl.addComponent(1, 2, new Image("mimes/ICON_LED_RED.gif", RED_LIGHT_TOOLTIP));
			gl.addComponent(1, 3, scenarioNameTW);
			return gl;
		}
		
		if ((activeScenarioImage != null) && (defaultScenarioImage != null)) {
			gl = new GridLayout(1, 3);
			gl.addComponent(1, 1, activeScenarioImage);
			gl.addComponent(1, 2, defaultScenarioImage);
			gl.addComponent(1, 3, scenarioNameTW);
			return gl;
		} 
		if ((activeScenarioImage != null) && (defaultScenarioImage == null)) {
			gl = new GridLayout(1, 2);
			gl.addComponent(1, 1, activeScenarioImage);
			gl.addComponent(1, 2, scenarioNameTW);
			return gl;
		}
		if (defaultScenarioImage == null) {
			defaultScenarioImage =
				new Image("mimes/icon_empty.gif", RED_LIGHT_TOOLTIP); 
		}
		gl = new GridLayout(1, 2);
		gl.addComponent(1, 1, defaultScenarioImage);
		gl.addComponent(1, 2, scenarioNameTW);
		return gl;		
	}
}
