/*
 * Created on Sep 9, 2004
 *
 */
package com.sap.isa.core.xcm.admin.model;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.MalformedURLException;
import java.net.URL;

import org.w3c.dom.Document;

import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.xcm.StringInputStream;
import com.sap.isa.core.xcm.xml.XMLUtil;

/**
 * @author I802891
 *
 */
public class XMLAdminUtilFS extends XMLAdminUtil {

	private String _baseHref = null;
	private String _docHref = null;
	
	/**
	 * @throws XCMAdminException
	 */
	public XMLAdminUtilFS() throws XCMAdminException {
		super();
		// TODO Auto-generated constructor stub
	}

	public void setBaseHREF(String href) {
		this._baseHref = href;
	}
	
	public String getBaseHREF() {
		return this._baseHref;
	}
	
	public String getDocHREF() {
		return this._docHref;
	}
	
	public void setDocHREF(String href) {
		if(href == null || href.trim().length() == 0) throw new IllegalArgumentException("HREF = " + href + " is invalid");
		this._docHref = href;
	}
	
	/**
	 * Initializes the extending doc. An unambigous node id is added to every
	 * XML element
	 */
	public Document loadConfigurationDocument() throws XCMAdminException {
 		
		if (mLoadDoc == null) 
			refreshConfiguration();
		return mLoadDoc;
	}
	
	
	public void refreshConfiguration() throws XCMAdminException {
		if(this._docHref == null) throw new XCMAdminException("docHREF is null");
 		
		XMLUtil util = XMLUtil.getInstance();
		try {
			mLoadDoc = util.getDoc(_baseHref, _docHref);
		} catch (Exception ex) {
			XCMAdminException xcmEx = new XCMAdminException("Error loading document [href]='" + _docHref + "'");
			xcmEx.setBaseException(ex);
			throw xcmEx;			
		}
		mLoadDoc = util.addIdsToDoc(0, mLoadDoc);
		if (mLoadDoc == null) 
			throw new XCMAdminException("Error adding element id");
	}

	/**
	 * Saves the current configuration to the file system
	 */
	public void saveConfiguration(String configData) throws XCMAdminException {
		String href = null;
		String path = null;
		try {
			if (log.isDebugEnabled()) {
				log.debug("Save path [baseHref]='" + _baseHref + "' [docHre]='" + _docHref + "'");
			}
			href = XMLUtil.getInstance().getAbsoluteURL(_baseHref, _docHref);
			if (log.isDebugEnabled()) {
				log.debug("Save path [href]='" + href);
				log.debug("path of docHre [href]='" + new URL(_docHref).getPath());
				log.debug("host of docHre [href]='" + new URL(_docHref).getHost());
			}
			path = new URL(_docHref).getPath();
			if (log.isDebugEnabled()) {
				log.debug("Saving component configuration [path]='" + path + "'");
			}
		} catch (MalformedURLException murlex) {
			log.error(LogUtil.APPS_USER_INTERFACE,"system.xcm.admin.exception", new Object[] {}, murlex);
		}
		File configFile = new File(path);
		Writer fw = null;
		StringInputStream sis = null;
		try {
			FileOutputStream fos = new FileOutputStream(configFile);
			// write in UTF-8 enconding on FS
			fw = new OutputStreamWriter(fos,"UTF-8");
			sis = new StringInputStream(configData);

			int c;

			while ((c = sis.read()) != -1)
			   fw.write(c);

			fw.close();
			sis.close();
    						
		} catch (Throwable ex) {
			try {
				if (fw != null)
					fw.close();
				if (sis != null)
					sis.close();		
			} catch (Throwable t) {
				log.debug(t.getMessage());
			}
				
			log.error(LogUtil.APPS_USER_INTERFACE,"system.xcm.admin.exception", new Object[] {}, ex);
			String msg = ex.toString();
			StringBuffer str = new StringBuffer();

			int len = (msg != null) ? msg.length() : 0;
			for (int i = 0; i < len; i++ ) {
			  char ch = msg.charAt(i);
			  switch ( ch ) {
				case '\\': {
				  str.append("/");
				  break; 
				}
				default: {
				  str.append(ch);
				}
			  }
			}
			throw new XCMAdminException(str.toString());
		}
	}	
}
