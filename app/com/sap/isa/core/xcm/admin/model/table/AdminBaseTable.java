package com.sap.isa.core.xcm.admin.model.table;

// isa imports
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Vector;

import com.sap.isa.core.logging.IsaLocation;
import com.sapportals.htmlb.table.TableColumn;
import com.sapportals.htmlb.table.TableViewModel;
import com.sapportals.htmlb.util.IndexedLinkedList;

public abstract class AdminBaseTable implements TableViewModel {
	
	protected static IsaLocation log = null;
	protected ArrayList columns = new ArrayList();
	protected Locale locale = null;
	protected Map selectedRows = new LinkedHashMap();

	public AdminBaseTable() {
		if (log == null)
			log = IsaLocation.getInstance(this.getClass().getName());
		init();
	}

	protected abstract void init();

	public int getColumnCount() {
		return columns.size();
	}

	public String getColumnName(int columnIndex) {
		if (!checkColumnIndex(columnIndex))
			return null;
		return checkColumnIndex(columnIndex)
			? ((TableColumn) columns.get(columnIndex)).getTitle()
			: null;
	}

	public void setColumnName(String newVal, int index) {
		if (!checkColumnIndex(index))
			return;
		((TableColumn) columns.get(index)).setTitle(newVal);
	}

	public void setKeyColumn(int index) {
	}

	public void addKeyColumn(int index) {
	}

	public IndexedLinkedList getKeyColumn() {
		return null;
	}

	public Vector getColumns() {
		return new Vector(columns);
	}

	public TableColumn getColumnAt(int columnIndex) {
		if (checkColumnIndex(columnIndex))
			return ((TableColumn) columns.get(columnIndex));
		return null;
	}

	/**
	 * Not allowed
	 */
	public TableColumn addColumn(String newColumn) {
		TableColumn newCol = new TableColumn(this, newColumn);
		columns.add(newCol);
		return newCol;
	}
	
	public TableColumn addColumn(String newColumn, String tooltip) {
		TableColumn newCol = new TableColumn(this, newColumn);
		newCol.setTooltipForColumnHeader(tooltip);
		columns.add(newCol);
		return newCol;
	}

	public void removeColumn(String name) {
		Iterator it = columns.iterator();
		while (it.hasNext()) {
			TableColumn tableColumn = (TableColumn) it.next();
			if (name.intern() == tableColumn.getTitle().intern())
				columns.remove(tableColumn);
		}
	}

	protected int getColumnIndex(String name) {
		for (int i = 0; i < columns.size(); i++) {
			TableColumn col = (TableColumn) columns.get(i);
			if (col.getTitle().intern() == name.intern())
				return i + 1;
		}
		return 0;
	}

	public int getSingleSelectedRow() {
		if (getNumberOfSelectedRows() > 0) {
			return ((Integer) getSelectedRows().next()).intValue();
		}
		return 0;
	}

	public int getNumberOfSelectedRows() {
		return selectedRows.size();
	}

	public Iterator getSelectedRows() {
		return selectedRows.entrySet().iterator();
	}

	public boolean isSelected(int row) {
		return selectedRows.get(new Integer(row)) != null;
	}

	public void setSelected(int row) {
		Integer rowSelected = new Integer(row);
		selectedRows.put(rowSelected, rowSelected);
	}

	public void removeSelected(int row) {
		selectedRows.remove(new Integer(row));
	}

	protected boolean checkColumnIndex(int columnIndex) {
		return (columnIndex > 0 && columnIndex <= columns.size());
	}
}
