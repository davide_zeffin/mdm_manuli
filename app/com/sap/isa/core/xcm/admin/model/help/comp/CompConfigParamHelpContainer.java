package com.sap.isa.core.xcm.admin.model.help.comp;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

import com.sap.isa.core.xcm.admin.model.CompDataManipulator;
import com.sap.isa.core.xcm.admin.model.help.HelpItem;
import com.sap.isa.core.xcm.admin.model.help.HelpItemContainer;

/**
 * Provides a summary of component configuration
 */

public class CompConfigParamHelpContainer implements HelpItemContainer {
	
	private String mContName;
	private Set mParamNames = new LinkedHashSet();
	private HelpItemContainer mAllConfigParams;

	public CompConfigParamHelpContainer(String contName, HelpItemContainer allConfParams, CompDataManipulator.ParamConfig paramConfig) {
		mAllConfigParams = allConfParams;
		mContName = contName;
		Set params = paramConfig.getParamNames();
		for (Iterator iter = params.iterator(); iter.hasNext();) {
			String paramName = (String)iter.next();
			mParamNames.add(paramName);
		}
	}
	
	public String getContainerTitle() {
		return mContName;
	}
	
	public int getNumItems() {
		return mParamNames.size();
	}
	
	public HelpItem getItem(String name) {
		return mAllConfigParams.getItem(name);
	}

	public Set getItemNames() {
		return Collections.unmodifiableSet(mParamNames);
	}
}
