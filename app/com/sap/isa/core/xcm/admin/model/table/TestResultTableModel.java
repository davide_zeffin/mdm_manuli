package com.sap.isa.core.xcm.admin.model.table;

// java imports 

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.sapportals.htmlb.type.AbstractDataType;
import com.sapportals.htmlb.type.DataString;
import com.sap.isa.core.xcm.admin.model.table.TestResultTableModel;
import java.util.Map;
import com.sap.isa.core.xcm.admin.controller.ComponentConfigController;
import com.sap.isa.core.init.InitializationHandler;
import com.sap.isa.core.xcm.admin.actionform.TestResultForm;



/**
 * 
 */
public class TestResultTableModel extends AdminBaseTable {
	private List mItemParamNames = new LinkedList();
	private List mItemParamValues = new LinkedList(); 
	private static org.apache.struts.util.MessageResources mMessageResources;
	


	public TestResultTableModel(ComponentConfigController compConfig) {
		mMessageResources =	InitializationHandler.getMessageResources();
		Map testParams = compConfig.getCurrentTestParams();
		ComponentDetailTableModel tableModel = compConfig.getCompDetailTableModel();		
		
		for (Iterator iter = testParams.keySet().iterator(); iter.hasNext();) {
			String paramName = (String)iter.next();
			String paramValue = (String)testParams.get(paramName);
			boolean isPassword = tableModel.isPassword(paramName);

			if (paramValue == null)  
				paramValue = mMessageResources.getMessage(TestResultForm.PARAM_ERROR, null);
			else  if (!isPassword) {
															
			} else {
				paramValue = "?";											

			}
			mItemParamNames.add(paramName);
			mItemParamValues.add(paramValue);
		}
						
	}

	/**
	 * @see com.sap.isa.core.xcm.admin.model.table.AdminBaseTable#init()
	 */
	protected void init() {
		if (log.isDebugEnabled())	
			log.debug("Initializing 'TestParamsTableModel' table");
			
  		addColumn("Parameter", "Parameter");
  		addColumn("Value", "Value");
	}

	/**
	 * @see com.sapportals.htmlb.table.TableViewModel#getRowCount()
	 */
	public int getRowCount() {
		return mItemParamNames.size();
	}

	/**
	 * @see com.sapportals.htmlb.table.TableViewModel#getValueAt(int, int)
	 */
	public AbstractDataType getValueAt(int rowIndex, int index) {
   		String retVal;
   		if (index==1){
   			 retVal = mItemParamNames.get(rowIndex-1).toString();   			 
   		}else{
   			retVal = mItemParamValues.get(rowIndex-1).toString();;
   		}
		return new DataString(retVal);
	}

	/**
	 * @see com.sapportals.htmlb.table.TableViewModel#getValueAt(int, String)
	 */
	public AbstractDataType getValueAt(int rowIndex, String name) {
	    return getValueAt(rowIndex, getColumnIndex(name));
	}

	/**
	 * @see com.sapportals.htmlb.table.TableViewModel#setValueAt(AbstractDataType, int, int)
	 */
	public void setValueAt(AbstractDataType data, int rowIndex, int column) {
		// no implementation
	}
}
