package com.sap.isa.core.xcm.admin.model.help.scenario;

import com.sap.isa.core.xcm.admin.model.HelpConstants;
import com.sap.isa.core.xcm.admin.model.ScenarioDataManipulator;
import com.sap.isa.core.xcm.admin.model.help.HelpItem;
import com.sap.isa.core.xcm.admin.model.help.HelpItemContainer;


/**
 * Base class for all times holding help information
 *
 */
public class ScenDetailHelpItem implements HelpItem  {

	ScenarioDataManipulator.ScenarioConfig mConfig;
	private String mContainerName;
	private HelpItemContainer mMandatoryParams;
	
	public ScenDetailHelpItem(String containerName, String scenName, String baseScenName, ScenarioDataManipulator.ScenarioConfig config) {
		mConfig = config;
		mContainerName = containerName;
	}
	
	/**
	 * Initializes mandatory parameter container
	 */
	private void initParamContainer() {
	}
	
	public String getContainerName() {
		return mContainerName;
	}
	
	public String getTypeName() {
		return HelpConstants.TYPE_NAME_SCENARIO;
	}

	public int getType() {
		return HelpConstants.TYPE_SCENARIO;
	}

	
	public String getName() {
		return mConfig.getName();
	}

	public String getShorttext() {
		if (mConfig.getShorttext() == null)
			return "";
		else
			return mConfig.getShorttext();
	}
	
	public String getLongtext() {
		if (mConfig.getLongtext() == null)
			return "";
		else
			return mConfig.getLongtext();
	}
	

	public String getBaseConfigName() {
		return mConfig.getBaseConfigName();
	}

	public String getConfigValue() {
		return "";
	}

	public String getDependendCompName() {
		return "";
	}	

	public HelpItemContainer getChildren() {
		return null;
	}
	public String getScope() {
		return "";	
	}
}
