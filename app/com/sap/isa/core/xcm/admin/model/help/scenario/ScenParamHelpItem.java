package com.sap.isa.core.xcm.admin.model.help.scenario;

import com.sap.isa.core.xcm.XCMUtils;
import com.sap.isa.core.xcm.admin.controller.ControllerUtils;
import com.sap.isa.core.xcm.admin.model.HelpConstants;
import com.sap.isa.core.xcm.admin.model.ScenarioDataManipulator;
import com.sap.isa.core.xcm.admin.model.help.HelpItem;
import com.sap.isa.core.xcm.admin.model.help.HelpItemContainer;

public class ScenParamHelpItem implements HelpItem {

	private ScenarioDataManipulator.ParamConfig mParamConfig;
	private String mShortText;	
	private String mLongText;
	private int mType;
	private String mValue;
	private String mBaseConfig;	
	private String mDependendCompName;

	/**
	* Type should be either XCMAdminParamConstrainMetadata.TYPE_OPTIONAL
	* or XCMAdminParamConstrainMetadata.TYPE_MANDATORY
	*/
	public ScenParamHelpItem(String scenName, ScenarioDataManipulator.ParamConfig paramConfig, int type, String baseConfig) {
		
		mParamConfig = paramConfig;
		mType = type;
		mValue = paramConfig.getValue();
		mBaseConfig = baseConfig;
		
		if (mBaseConfig != null && mBaseConfig.length() > 0 ) {
			mShortText = ControllerUtils.getScenConfigParamShorttext(mBaseConfig, paramConfig.getName());
			mLongText = ControllerUtils.getScenConfigParamLongtext(mBaseConfig, paramConfig.getName());
		} else {
			mShortText = ControllerUtils.getScenConfigParamShorttext(scenName, paramConfig.getName());
			mLongText = ControllerUtils.getScenConfigParamLongtext(scenName, paramConfig.getName());
		}

	}

	/**
	 * The type of the item.
	 * @return int
	 */
	public int getType() {
		return mType;
	}
	
	/**
	 * The type of the item as text. e.g. Scenario
	 * @return String
	 */
	public String getTypeName() {
		if (mType == HelpConstants.TYPE_SCENARIO_PARAM_OPTIONAL)
			return HelpConstants.TYPE_NAME_SCENARIO_PARAM_OPTIONAL;
		else
			return HelpConstants.TYPE_NAME_SCENARIO_PARAM_MANDATORY;
	}

	public String getName() {
		return mParamConfig.getName();
	}

	public String getShorttext() {
		return mShortText;
	}
	
	public String getLongtext() {
		if (mLongText == null)
			return "";
		else
			return mLongText;
	}
	
	public String getBaseConfigName() {
		return "";
	}
	
	public String getConfigValue() {
		return mValue;
	}

	public String getDependendCompName() {
		return mDependendCompName;
	}

	public HelpItemContainer getChildren() {
		return null;	
	}

	public String getScope() {
		return "";
	}
}
