package com.sap.isa.core.xcm.admin.model;

// java imports 

/**
 * This exception is thrown when something goes wrong while ECM administration
 */
public class XCMAdminException extends Exception {

	private Exception mBaseEx;
	private String mMsg = "";

	/** 
	 * Constructor
	 * @param msg Message describing the exception
	 */
	public XCMAdminException(String msg) {
		super(msg);
		mMsg = msg;
	}

	/**
	 * Returns the base exception or <code>null</code> if there
	 * is no root exception
	 * @return the root exception
	 */
	public Exception getBaseException() {
		return mBaseEx;
	}

	/**
	 * Sets the base exception
	 * @param ex the base exception 
	 */
	public void setBaseException(Exception baseEx) {
		mBaseEx = baseEx;
	}

	/**
	 * Returns a string representation of this exception
	 * @return a string representation of this exception
	 */
	public String toString() {
		if (mBaseEx == null)
			return mMsg;
		else
			return mMsg + "\n" + mBaseEx.toString();
	}

}