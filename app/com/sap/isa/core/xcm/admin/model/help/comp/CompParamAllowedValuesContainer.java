package com.sap.isa.core.xcm.admin.model.help.comp;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import com.sap.isa.core.xcm.XCMUtils;
import com.sap.isa.core.xcm.admin.model.CompDataManipulator;
import com.sap.isa.core.xcm.admin.model.HelpConstants;
import com.sap.isa.core.xcm.admin.model.help.HelpItem;
import com.sap.isa.core.xcm.admin.model.help.HelpItemBase;
import com.sap.isa.core.xcm.admin.model.help.HelpItemContainer;


public class CompParamAllowedValuesContainer implements HelpItemContainer {
	
	private int numItems;
	private String mContName;
	private Map mAllowedValues = new LinkedHashMap();
	
	public CompParamAllowedValuesContainer(String contName, String compId, String paramName, CompDataManipulator.ParamConfig paramConfig) {
		mContName = contName;
		Set allowedValues = paramConfig.getAllowedValues(paramName);
		for (Iterator iter = allowedValues.iterator(); iter.hasNext();) {
			String paramValue = (String)iter.next();
			String shortText = 
			XCMUtils.getCompConfigParamValueShorttext(compId, paramName, paramValue);
			String longText = 
				XCMUtils.getCompConfigParamValueLongtext(compId, paramName, paramValue);
			HelpItemBase allowedItemHelp = 
				new HelpItemBase(
						HelpConstants.TYPE_COMPONENT_PARAM_ALLOWED_VALUE,
						paramValue, shortText, longText, paramValue, "", "", "");
			mAllowedValues.put(paramValue, allowedItemHelp);
		}
		
	}
	
	public String getContainerTitle() {
		return mContName;
	}
	
	public int getNumItems() {
		return mAllowedValues.size();
	}
	
	
	public HelpItem getItem(String name) {
		return (HelpItem)mAllowedValues.get(name);
	}

	public Set getItemNames() {
		return Collections.unmodifiableSet(mAllowedValues.keySet());
	}
}
