package com.sap.isa.core.xcm.admin.model.help;

import java.util.Set;

/**
 * Used to store help items
 *
 */
public interface HelpItemContainer {
	
		
	public String getContainerTitle();
	
	public int getNumItems();
	
	public HelpItem getItem(String itemName);
	
	public Set getItemNames();

}
