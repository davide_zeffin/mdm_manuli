package com.sap.isa.core.xcm.admin.model.table;

// java imports 
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.sap.isa.core.xcm.admin.controller.ScenarioConfigController;
import com.sap.isa.core.xcm.admin.model.ScenarioDataManipulator;
import com.sap.isa.core.xcm.init.XCMAdminComponentMetadata;
import com.sap.isa.core.xcm.init.XCMAdminInitHandler;
import com.sap.isa.core.xcm.init.XCMAdminParamConstrainMetadata;
import com.sapportals.htmlb.DefaultListModel;
import com.sapportals.htmlb.IListModel;
import com.sapportals.htmlb.type.AbstractDataType;
import com.sapportals.htmlb.type.DataString;
import com.sap.isa.core.init.InitializationHandler;

/**
 * 
 */
public class ScenarioDetailTableModel extends AdminBaseTable {
	
	private static org.apache.struts.util.MessageResources mMessageResources =
						InitializationHandler.getMessageResources();
	public static final String ON_NAVIGATE_EVENT =
		"OnNavigate.ScenarioDetailTableModel.table.model.xcm.core.isa.sap.com";

	public static final String NAME_PARAM_VALUE = "paramValue";

	public static final int COL_INDEX_SCENARIO_VALUE = 2;
	public static final int COL_INDEX_SCENARIO_NAME  = 1;

	public static final String TABLE_HEADER_C1_TEXT = mMessageResources.getMessage("xcm.scenconf.tab.h1.te");
		public static final String TABLE_HEADER_C1_TOOLTIP = mMessageResources.getMessage("xcm.scenconf.tab.h1.to");
		public static final String TABLE_HEADER_C2_TEXT = mMessageResources.getMessage("xcm.scenconf.tab.h2.te");
		public static final String TABLE_HEADER_C2_TOOLTIP = mMessageResources.getMessage("xcm.scenconf.tab.h2.to");
		public static final String TABLE_HEADER_C3_TOOLTIP = mMessageResources.getMessage("xcm.scenconf.tab.h3.to");
		public static final String TABLE_HEADER_C4_TEXT = mMessageResources.getMessage("xcm.scenconf.tab.h4.te");
		public static final String TABLE_HEADER_C4_TOOLTIP = mMessageResources.getMessage("xcm.scenconf.tab.h4.te");
		public static final String TABLE_HEADER_C5_TEXT = mMessageResources.getMessage("xcm.scenconf.tab.h5.te");
		public static final String TABLE_HEADER_C5_TOOLTIP = mMessageResources.getMessage("xcm.scenconf.tab.h5.to");
		public static final String TABLE_HEADER_C6_TOOLTIP = mMessageResources.getMessage("xcm.scenconf.tab.h6.to");


	private ScenarioConfigController mScenConfigController; 
	private ScenarioDataManipulator.ScenarioConfig mScenDataManipulator;

 
	private ArrayList props = new ArrayList();
	private List mScenarioParams = Collections.EMPTY_LIST; 
	private boolean readOnly;
	 
	private class ScenarioConfigRow {

		private String mName;
		private String mValue;
		private String mDescr;
		private boolean mIsDerived;
		private boolean mDeprecated;
	
		ScenarioConfigRow(String name, String value, String descr, boolean isDerived, boolean isDeprecated) {
			mName = name;
			mValue = value;	
			mDescr = descr;
			mIsDerived = isDerived;
			mDeprecated = isDeprecated;
		}
	
		public String getName() {
			return mName;
		}
		
		public String getValue() {
			return mValue;
		}

		public boolean isDerived() {
			return mIsDerived;
		}

		public boolean isDeprecated() {
			return mDeprecated;
		}


		public String getDescription() {
			return mDescr;
		}

		public void setValue(String value) {
			mValue = value;
		}
	}
	
	public ScenarioDetailTableModel(ScenarioConfigController scenController, ScenarioDataManipulator.ScenarioConfig scenConfig) {
		if (scenConfig == null)
			return;
		mScenConfigController = scenController;
		mScenDataManipulator = scenConfig;
		// invisible first row
		mScenarioParams = new LinkedList();
		mScenarioParams.add(new ScenarioConfigRow("empty", "empty","empty", false, false));
		Map paramValues = scenConfig.getParamValues();
		for (Iterator iter = paramValues.keySet().iterator(); iter.hasNext(); ) {
			String name = (String)iter.next();
	
			String type = scenController.getConfigParamType(name);
			if (type == null) {
				String msg = "Type of parameter [name]='" + name + "' missing. Assigning [type]='optional'. Type should be specfied in XCM admin configuration file using type='mandatory|optional'"; 
				log.warn("system.xcm.warn", new Object[] { msg }, null);
				type = XCMAdminParamConstrainMetadata.TYPE_OPTIONAL;
			}
			
			if (type.equalsIgnoreCase(XCMAdminParamConstrainMetadata.TYPE_OPTIONAL) 
				&& !mScenConfigController.isAdvancedOptionsOn())
				continue;
					
			String value = (String)paramValues.get(name);
			String descr = mScenConfigController.getConfigParamShorttext(name);
			boolean isDerived = scenController.isParamConfigDerived(name);
			boolean isDeprecated = scenController.isScenarioParamObsolete(name);
			mScenarioParams.add(new ScenarioConfigRow(name, value,descr, isDerived, isDeprecated));
			}
		} 

	/**
	 * @see com.sap.isa.core.xcm.admin.model.table.AdminBaseTable#init()
	 */
	protected void init() {
		if (log.isDebugEnabled())	
			log.debug("Initializing 'ComponentDetailTable' table");
					
			addColumn(TABLE_HEADER_C1_TEXT, TABLE_HEADER_C1_TOOLTIP);
			addColumn(TABLE_HEADER_C2_TEXT, TABLE_HEADER_C2_TOOLTIP);
			addColumn("", TABLE_HEADER_C3_TOOLTIP);
			addColumn(TABLE_HEADER_C4_TEXT, TABLE_HEADER_C4_TOOLTIP);
			addColumn(TABLE_HEADER_C5_TEXT, TABLE_HEADER_C5_TOOLTIP);
			addColumn("", TABLE_HEADER_C6_TOOLTIP);
	}

	/**
	 * @see com.sapportals.htmlb.table.TableViewModel#getRowCount()
	 */
	public int getRowCount() {
		return mScenarioParams.size() - 1;
	}

	public boolean isDerived(int rowIndex) {
		if (rowIndex >= mScenarioParams.size())
			return false;
		ScenarioConfigRow row = (ScenarioConfigRow)mScenarioParams.get(rowIndex);
		return row.isDerived();
	}

	public boolean isDeprecated(int rowIndex) {
		if (rowIndex >= mScenarioParams.size())
			return false;
		ScenarioConfigRow row = (ScenarioConfigRow)mScenarioParams.get(rowIndex);
		return row.isDeprecated();
	}


	/**
	 * @see com.sapportals.htmlb.table.TableViewModel#getValueAt(int, int)
	 */
	public AbstractDataType getValueAt(int rowIndex, int index) {
	    String retVal = "";
	    switch (index) {
	    	case 1: { 
	    			retVal = getParamName(rowIndex);
	    			break;
	    	}

	    	case 2: {
		    	retVal = getParamValue(rowIndex);
		    	break;
	    	}
			case 4: {
				retVal = getComponentId(rowIndex);
				break;
			}
	    	case 5: {
		    	retVal = getParamDescription(rowIndex);
		    	break;
	    	}
	    	case 6: {
	    		boolean derived = isDerived(rowIndex);
	    		if (derived)
	    			retVal = "true";
	    		else retVal = "false";
	    	}
	    }
		return new DataString(retVal);
	}

	/**
	 * @see com.sapportals.htmlb.table.TableViewModel#getValueAt(int, String)
	 */
	public AbstractDataType getValueAt(int rowIndex, String name) {
	    return getValueAt(rowIndex, getColumnIndex(name));
	}

	/**
	 * @see com.sapportals.htmlb.table.TableViewModel#setValueAt(AbstractDataType, int, int)
	 */
	public void setValueAt(AbstractDataType data, int rowIndex, int column) {
		if (rowIndex >= mScenarioParams.size())
			return;
		ScenarioConfigRow row = (ScenarioConfigRow)mScenarioParams.get(rowIndex);
		if (data == null)
			return;
		row.setValue(data.getValueAsString());
	}

	private String getComponentId(int rowIndex) {
		String paramName = getParamName(rowIndex);
		XCMAdminParamConstrainMetadata metaData = XCMAdminInitHandler.getXCMAdminScenarioMetadata(paramName);
		if (metaData == null)
			return null;
		String compId = metaData.getComponent();
		return compId;	
	}


	private String getParamName(int rowIndex) {
		if (rowIndex >= mScenarioParams.size())
			return "";
		ScenarioConfigRow row = (ScenarioConfigRow)mScenarioParams.get(rowIndex);
		if (row == null)
			return "";
		return row.getName();
	}

	private String getParamValue(int rowIndex) {
		if (rowIndex >= mScenarioParams.size())
			return "";

		ScenarioConfigRow row = (ScenarioConfigRow)mScenarioParams.get(rowIndex);
		if (row == null)
			return "";
		return row.getValue();
	}
	private String getParamDescription(int rowIndex) {
		if (rowIndex >= mScenarioParams.size())
			return "";

		ScenarioConfigRow row = (ScenarioConfigRow)mScenarioParams.get(rowIndex);
		if (row == null)
			return "";
		if (isDeprecated(rowIndex))
			return XCMAdminComponentMetadata.OBSOLETE.toUpperCase() + " " + row.getDescription();
		return row.getDescription();
	}
	
	public IListModel getAllowedValuesListModel(int row) {
		String paramValue = getParamValue(row);
		String paramName = getParamName(row);
 
 		if (log.isDebugEnabled())
 			log.debug("Requesting allowed values for [config parameter]='" + paramName + "'");
 		
		List allowedValues = mScenConfigController.getCurrentScenarioAllowedValues(paramName);
		
		DefaultListModel listBoxModel = new DefaultListModel();
		
		if (allowedValues.size() > 0) {

			for (Iterator iter = allowedValues.iterator(); iter.hasNext(); ) {
				String allowedValue = (String)iter.next();
				if (listBoxModel.getTextForKey(allowedValue) == null)
					listBoxModel.addItem(allowedValue, allowedValue);
			}
		}
		if (listBoxModel.getTextForKey(paramValue) == null)
			listBoxModel.addItem(paramValue, paramValue);

		listBoxModel.setSelection(paramValue);
		
		listBoxModel.setSingleSelection(true);
		
		return listBoxModel;
	}

	

	/**
	 * @return
	 */
	public boolean isReadOnly() {
		return readOnly;
	}

	/**
	 * @param b
	 */
	public void setReadOnly(boolean b) {
		readOnly = b;
	}

}
