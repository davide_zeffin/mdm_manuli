package com.sap.isa.core.xcm.admin.model.help.comp;

import com.sap.isa.core.xcm.XCMUtils;
import com.sap.isa.core.xcm.admin.model.CompDataManipulator;
import com.sap.isa.core.xcm.admin.model.HelpConstants;
import com.sap.isa.core.xcm.admin.model.help.HelpItem;
import com.sap.isa.core.xcm.admin.model.help.HelpItemContainer;
import com.sap.isa.core.xcm.init.XCMAdminComponentMetadata;
import com.sap.isa.core.xcm.init.XCMAdminParamConstrainMetadata;

/**
 * Encapsulates help detail for a XCM component 
*/
public class CompDetailHelpItem implements HelpItem {
	
	private CompDataManipulator.CompConfig mCompConfig;
	private final String mCompId;


	public CompDetailHelpItem(CompDataManipulator.CompConfig compConfig) {
		mCompConfig = compConfig;
		mCompId = mCompConfig.getCompId();
	}

	public int getType() {
		return HelpConstants.TYPE_COMPONENT;
	}

	public String getTypeName() {
		return HelpConstants.TYPE_NAME_COMPONENT;
	}

	public String getName() {
		return mCompId;
	}

	public String getShorttext() {
		return XCMUtils.getCompShorttext(mCompId);
	}

	public String getLongtext() {
		return XCMUtils.getCompLongtext(mCompId);
	}

	public String getBaseConfigName() {
		return "";
	}

	public String getConfigValue() {
		return "";
	}

	public String getDependendCompName() {
		return "";
	}

	public HelpItemContainer getChildren() {
		return null;
	}
	
	public String getScope() {
		XCMAdminComponentMetadata metadata = XCMUtils.getComponentMetadata(mCompId);
		if (metadata != null) {
			String scope = metadata.getScope();
			if (scope != null && scope.equalsIgnoreCase("application"))
				return "general settings";
			
			if (scope != null && scope.equalsIgnoreCase("scenario"))
				return "depends on XCM application configuration";
		} 
		return "";
	}
}
