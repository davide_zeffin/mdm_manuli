package com.sap.isa.core.xcm.admin.model.help.comp;

import com.sap.isa.core.xcm.admin.model.HelpConstants;
import com.sap.isa.core.xcm.admin.model.help.HelpItem;
import com.sap.isa.core.xcm.admin.model.help.HelpItemContainer;

public class CompConfigHelpItem implements HelpItem {

	private String mShortText;	
	private String mLongText;
	private int mType;
	private String mName;
	private String mScope; 

	public CompConfigHelpItem(String name, String shortText, String longText, String scope) {
		mName = name;
		mShortText = shortText;
		mLongText = longText;
		mScope = scope;
	}
 
	public int getType() {
		return HelpConstants.TYPE_COMPONENT_CONFIG;
	}
	
	public String getTypeName() {
		return HelpConstants.TYPE_NAME_COMPONENT_CONFIG;
	}

	public String getName() {
		return mName;
	}

	public String getShorttext() {
		return mShortText;
	}
	
	public String getLongtext() {
		return mLongText;
	}
	
	public String getBaseConfigName() {
		return "";
	}
	
	public String getConfigValue() {
		return "";
	}

	public String getDependendCompName() {
		return "";
	}

	public HelpItemContainer getChildren() {
		return null;	
	}

	public String getScope() {
		return mScope;
	}
}
