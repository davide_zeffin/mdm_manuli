/**
 * Created on Sep 9, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.core.xcm.admin.model;

import java.util.LinkedHashSet;
import java.util.Set;

import org.w3c.dom.Document;

import com.sap.isa.core.db.DBException;
import com.sap.isa.core.db.DBQueryResult;
import com.sap.isa.core.db.jdbc.JdbcPersistenceManager;
import com.sap.isa.core.db.jdbc.JdbcPersistenceManagerFactory;
import com.sap.isa.core.db.jdbc.SQLQuery;
import com.sap.isa.core.locking.LockException;
import com.sap.isa.core.locking.LockManager;
import com.sap.isa.core.locking.enqueue.EnqueueLockInfo;
import com.sap.isa.core.locking.enqueue.EnqueueLockManager;
import com.sap.isa.core.util.OIDGenerator;
import com.sap.isa.core.xcm.db.ApplicationConfigBean;
import com.sap.isa.core.xcm.db.ConfigDataUnit;
import com.sap.isa.core.xcm.db.ConfigSet;
import com.sap.isa.core.xcm.db.jdbc.AppConfigSetLinkDAO;
import com.sap.isa.core.xcm.db.jdbc.ConfigDataUnitDAO;
import com.sap.isa.core.xcm.db.jdbc.ConfigSetDAO;
import com.sap.isa.core.xcm.xml.XMLUtil;

/**
 * @author I802891
 *
 */
public class XMLAdminUtilDB extends XMLAdminUtil {
	
	private JdbcPersistenceManagerFactory _pmf;
	
	private String _appName;
	private String _configSetName;
	private String _dataUnitName;
	private int _unitType;
	
	private ApplicationConfigBean _appConfig;
	private ConfigSet _configSet;
	private ConfigDataUnit _dataUnit;
	
	private static final String DEFAULT_CONFIG_DATA =
	"<data version=\"4.0\" xmlns:isa=\"com.sap.isa.core.config\" xmlns:xi=\"http://www.w3.org/2001/XInclude\" xmlns:xml=\"http://www.w3.org/XML/1998/namespace\">" +
		"<xi:include href=\"${sap}/configuration/config-data.xml#xpointer(data/*)\"/>" +
	"</data>";
	
	private static final String DEFAULT_SCENARIO_CONFIG_DATA =
	"<scenarios xmlns:isa=\"com.sap.isa.core.config\" xmlns:xi=\"http://www.w3.org/2001/XInclude\" xmlns:xml=\"http://www.w3.org/XML/1998/namespace\">" +
	"  <xi:include href=\"${sap}/configuration/scenario-config.xml#xpointer(scenarios /*)\"/>" +
	"</scenarios>";
	
	/**
	 * @throws XCMAdminException
	 */
	public XMLAdminUtilDB(JdbcPersistenceManagerFactory pmf) throws XCMAdminException {
		super();
		
		this._pmf = pmf;
		

	}
	
	/**
	 * Initializes and Loads the Data from database
	 * An unambigous node id is added to every XML element
	 */
	public Document loadConfigurationDocument() throws XCMAdminException {
		if(mLoadDoc == null)
			refreshConfiguration(); 
		
		return mLoadDoc;
	}
	
	public void refreshConfiguration() throws XCMAdminException {
		if(_appName == null) {
			throw new XCMAdminException("Application name is null");
		}
		
		if(_configSetName == null) {
			throw new XCMAdminException("ConfigSet name is null");
		}
		
		if(_dataUnitName == null) {
			throw new XCMAdminException("ConfigUnit name is null");
		}
		
		_appConfig = new ApplicationConfigBean();
		_appConfig.setApplicationName(_appName);
		
		_configSet = null;
		
		_dataUnit = null;
		
		
		JdbcPersistenceManager pm = null;
		try {

			pm = (JdbcPersistenceManager)_pmf.getDBPersistenceManager();
			
			SQLQuery query = ConfigSetDAO.getConfigSetsByApplicationNameQuery(pm);
			
			DBQueryResult result = query.execute(new Object[]{_appName});
			
			if(result.size() > 0) {
				Object[] objs = result.fetch(result.size());
				
				Set configSets = new LinkedHashSet(objs.length > 0 ? objs.length : 4);
				
				for(int i = 0; i < objs.length; i++) {
					ConfigSet configSet = (ConfigSet)objs[i];
					configSets.add(objs[i]);
				}
				_appConfig.setConfigDataSets(configSets);
				
				result.close();
				
				_configSet = _appConfig.getConfigSetByName(_configSetName);
			}
			
			
						
			// Config Set does not exist in database
			if(_configSet == null) {
				ConfigSetDAO setDAO = new ConfigSetDAO();
				setDAO.setConfigSetId(OIDGenerator.newOID());
				setDAO.setDescription(_configSetName);
				setDAO.setName(_configSetName);				
				
				AppConfigSetLinkDAO linkDAO = new AppConfigSetLinkDAO();
				linkDAO.setKey(OIDGenerator.newOID());
				linkDAO.setApplicationName(_appName);
				linkDAO.setConfigDataSet(setDAO);
				
				_configSet = setDAO;
				
				pm.getTransaction().begin();
				pm.save(setDAO);
				pm.save(linkDAO);
			}
			
			_dataUnit = _configSet.getConfigDataUnitByName(_dataUnitName);
			
			// Config Data Unit does not exist in database
			if(_dataUnit == null) {
				
				ConfigDataUnitDAO unitDAO = new ConfigDataUnitDAO();
				unitDAO.setConfigSetId(((ConfigSetDAO)_configSet).getConfigSetId());
				unitDAO.setUnitName(_dataUnitName);
				unitDAO.setUnitType(_unitType);
				if(_unitType == ConfigDataUnit.TYPE_CONFIG_DATA) {
					unitDAO.setDataAsString(DEFAULT_CONFIG_DATA);
				}
				else {
					unitDAO.setDataAsString(DEFAULT_SCENARIO_CONFIG_DATA);
				} 
				_configSet.addConfigDataUnit(unitDAO);
				if(!pm.getTransaction().isActive()) {
					pm.getTransaction().begin();
				}
				pm.save(unitDAO);
				_dataUnit = unitDAO;				
			}
			
			if(pm.getTransaction().isActive())
				pm.getTransaction().commit();
			// detach the DAOs from PM
			pm.detach(_configSet);
			pm.detach(_dataUnit);
			
		}
		catch(DBException ex) {
			_configSet = null;
			_dataUnit = null;
			XCMAdminException xcmex = new XCMAdminException(ex.getMessage());
			xcmex.setBaseException(ex);
			throw xcmex;
		}
		finally {
			if(pm != null) {
				if(pm.getTransaction().isActive())
					pm.getTransaction().rollback();
				pm.close();
			}
		}
		
		if(_dataUnit.getDataAsString() != null) {
			XMLUtil util = XMLUtil.getInstance();
			mLoadDoc = XMLUtil.getInstance().getDoc(_dataUnit.getDataAsString());
			mLoadDoc = util.addIdsToDoc(0, mLoadDoc);
		}		
	}
	
	/**
	 * Saves the current configuration to the file system
	 */
	public void saveConfiguration(String configData) throws XCMAdminException {
		JdbcPersistenceManager pm = null;
		try {
			pm = (JdbcPersistenceManager)_pmf.getDBPersistenceManager();
			
			// attach the DAOs
			pm.attach(_configSet);
			pm.attach(_dataUnit);
			
			// Start a Tx
			pm.getTransaction().begin();
			
			// sync data to DAOs
			_configSet.setDescription(_configSetName);
			_configSet.setName(_configSetName);
			
			_dataUnit.setUnitName(_dataUnitName);
			_dataUnit.setDataAsString(configData);
			_dataUnit.setLastModifiedDate(new java.util.Date(System.currentTimeMillis()));
			
			// Save DAOs
			pm.save(_configSet);
			pm.save(_dataUnit);
			
			// commit the Tx
			pm.getTransaction().commit();
			
			// detach the DAOs
			pm.detach(_configSet);
			pm.detach(_dataUnit);
		}
		catch(DBException ex) {
			XCMAdminException xcmex = new XCMAdminException(ex.getMessage());
			xcmex.setBaseException(ex);
		}
		finally {
			if(pm != null) {
				if(pm.getTransaction().isActive())
					pm.getTransaction().rollback();
				pm.close();
			} 
		}
	}
	
	
	/**
	 * @return
	 */
	public String getAppName() {
		return _appName;
	}

	/**
	 * @return
	 */
	public String getConfigSetName() {
		return _configSetName;
	}

	/**
	 * @return
	 */
	public String getConfigUnitName() {
		return _dataUnitName;
	}

	/**
	 * @param string
	 */
	public void setAppName(String string) {
		_appName = string;
	}

	/**
	 * @param string
	 */
	public void setConfigSetName(String string) {
		_configSetName = string;
	}

	/**
	 * @param string
	 */
	public void setConfigUnitName(String string) {
		_dataUnitName = string;
	}
	
	public void setConfigUnitType(int type) {
		this._unitType = type;
	}
	
	public int getConfigUnitType() {
		return _unitType;
	}
	
	public void lockConfiguration(LockManager lm) throws LockException {
		EnqueueLockManager elm = (EnqueueLockManager)lm;
		ConfigSetDAO dao = (ConfigSetDAO)_configSet;
		EnqueueLockInfo lock = new EnqueueLockInfo();
		lock.setLifetime(EnqueueLockManager.LIFETIME_SESSION);
		lock.setLockType(EnqueueLockManager.WRITE_LOCK);
		dao.fillLockInfo(lock);
		elm.lock(lock,0);
	}
	
	public void unlockConfiguration(LockManager lm) throws LockException {
		EnqueueLockManager elm = (EnqueueLockManager)lm;
		ConfigSetDAO dao = (ConfigSetDAO)_configSet;
		EnqueueLockInfo lock = new EnqueueLockInfo();
		lock.setLifetime(EnqueueLockManager.LIFETIME_SESSION);
		lock.setLockType(EnqueueLockManager.WRITE_LOCK);
		dao.fillLockInfo(lock);
		elm.unlock(lock,false);		
	}	
}
