package com.sap.isa.core.xcm.admin.model.help.comp;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import com.sap.isa.core.xcm.XCMUtils;
import com.sap.isa.core.xcm.admin.model.CompDataManipulator;
import com.sap.isa.core.xcm.admin.model.help.HelpItem;
import com.sap.isa.core.xcm.admin.model.help.HelpItemContainer;

/**
 * This container contains all parameter for the given component

 */

public class CompParamHelpContainer implements HelpItemContainer {
	
	private String mContName;
	private Map mHelpParamsItems = new LinkedHashMap();

	public CompParamHelpContainer(String contName, String compId, CompDataManipulator.CompConfig compConfig) {
		mContName = contName;
		String compConfigId = compConfig.getCompId();
		Map paramConfigs = compConfig.getParamConfigs();

		for (Iterator iter = paramConfigs.keySet().iterator(); iter.hasNext();) {
			String confName = (String)iter.next();
			CompDataManipulator.ParamConfig paramConfig = 
				compConfig.getParamConfig(confName);
			if (paramConfig.isDerivedCompConfig())
				continue;
			
			Set paramNames = paramConfig.getParamNames();
			for(Iterator iterParam = paramNames.iterator(); iterParam.hasNext();) {
				String paramName = (String)iterParam.next();
				if (mHelpParamsItems.containsKey(paramName))
					continue;
				String paramValue = paramConfig.getValue(paramName);
				String shortText = paramConfig.getShorttext(paramName);
				String longText = paramConfig.getLongtext(paramName);
				CompParamHelpItem paramHelpItem = 
					new CompParamHelpItem(paramName, shortText, longText, paramValue);
				mHelpParamsItems.put(paramName, paramHelpItem);

				// get allowed values for parameter
				Set allowedValues = paramConfig.getAllowedValues(paramName);
				if (allowedValues == null)
					continue;
				
				for (Iterator iterAW = allowedValues.iterator(); iterAW.hasNext();) {
					String aw = (String)iterAW.next();
					String awShorttext = 
					XCMUtils.getCompConfigParamValueShorttext(
							compId, paramName, aw);  
					String awLongtext = 
					XCMUtils.getCompConfigParamValueLongtext(
							compId, paramName, aw);  
					CompParamHelpItem awHelpItem = 
						new CompParamHelpItem(aw, awShorttext, awLongtext, aw);
					paramHelpItem.addAllowedValueHelpItem(awHelpItem);
				}
	
			}
			
		}

	}
	
	public String getContainerTitle() {
		return mContName;
	}
	
	public int getNumItems() {
		return mHelpParamsItems.size();
	}
	
	public HelpItem getItem(String name) {
		return (HelpItem)mHelpParamsItems.get(name);
	}

	public Set getItemNames() {
		return Collections.unmodifiableSet(mHelpParamsItems.keySet());
	}



}
