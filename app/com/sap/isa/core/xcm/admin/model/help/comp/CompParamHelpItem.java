package com.sap.isa.core.xcm.admin.model.help.comp;

import com.sap.isa.core.xcm.admin.model.HelpConstants;
import com.sap.isa.core.xcm.admin.model.help.HelpItem;
import com.sap.isa.core.xcm.admin.model.help.HelpItemContainer;
import com.sap.isa.core.xcm.admin.model.help.HelpItemContainerBase;

public class CompParamHelpItem implements HelpItem {

	private String mShortText;	
	private String mLongText;
	private int mType;
	private String mValue;
	private String mBaseConfig;	
	private String mDependendCompName;
	private String mName;
	private HelpItemContainerBase mAllowedValuesContainer = new HelpItemContainerBase("Allowed Values"); 

	/**
	* Type should be either XCMAdminParamConstrainMetadata.TYPE_OPTIONAL
	* or XCMAdminParamConstrainMetadata.TYPE_MANDATORY
	*/
	public CompParamHelpItem(String name, String shortText, String longText, String value) {
		
		mName = name;
		mShortText = shortText;
		mLongText = longText;
		mValue = value;
	}

	/**
	 * The type of the item.
	 * @return int
	 */
	public int getType() {
		return HelpConstants.TYPE_COMPONENT;
	}
	
	/**
	 * The type of the item as text. e.g. Scenario
	 * @return String
	 */
	public String getTypeName() {
		return HelpConstants.TYPE_NAME_COMPONENT_PARAMETER;
	}

	public String getName() {
		return mName;
	}

	public String getShorttext() {
		return mShortText;
	}
	
	public String getLongtext() {
		return mLongText;
	}
	
	public String getBaseConfigName() {
		return "";
	}
	
	public String getConfigValue() {
		return mValue;
	}

	public String getDependendCompName() {
		return mDependendCompName;
	}
	
	public void addAllowedValueHelpItem(HelpItem allowedValue) {
		mAllowedValuesContainer.addItem(allowedValue);
	}
	
	public HelpItemContainer getChildren() {
		return mAllowedValuesContainer;
	}
	public String getScope() {
		return "";
	}
}
