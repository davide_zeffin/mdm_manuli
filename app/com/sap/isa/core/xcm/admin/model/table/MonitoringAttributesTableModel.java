package com.sap.isa.core.xcm.admin.model.table;

// java imports 

import java.util.LinkedList;
import java.util.List;
import com.sapportals.htmlb.type.AbstractDataType;
import com.sapportals.htmlb.type.DataString;
import com.sap.isa.core.xcm.admin.model.table.MonitoringAttributesTableModel;
import com.sap.isa.core.xcm.ConfigContainer;
import java.util.StringTokenizer;
import java.util.Map;
import com.sap.isa.core.xcm.xml.ParamsStrategy;
import java.util.Iterator;

/**
 * 
 */
public class MonitoringAttributesTableModel extends AdminBaseTable {

	private List mItemNames = new LinkedList();
	private List mItemValues = new LinkedList();

	public MonitoringAttributesTableModel(Map params) {

		//StringTokenizer st = new StringTokenizer(key, "#");
		String value="";
		Iterator params_iter = params.entrySet().iterator();
		
		while (params_iter.hasNext()){
			Map.Entry entry = (Map.Entry) params_iter.next();
			mItemNames.add(entry.getKey().toString());
			mItemValues.add(entry.getValue().toString());
		
		
		}

	}

	/**
	 * @see com.sap.isa.core.xcm.admin.model.table.AdminBaseTable#init()
	 */
	protected void init() {
		if (log.isDebugEnabled())
			log.debug("Initializing 'MonitoringFileTableModel' table");

		addColumn("Name", "Name");
		addColumn("Value", "Value");
	}

	/**
	 * @see com.sapportals.htmlb.table.TableViewModel#getRowCount()
	 */
	public int getRowCount() {
		return mItemNames.size();
	}

	/**
	 * @see com.sapportals.htmlb.table.TableViewModel#getValueAt(int, int)
	 */
	public AbstractDataType getValueAt(int rowIndex, int index) {
		String retVal;
		if (index == 1) {
			retVal = (String) mItemNames.get(rowIndex - 1);
		} else {
			retVal = (String) mItemValues.get(rowIndex - 1);
		}
		return new DataString(retVal);
	}

	/**
	 * @see com.sapportals.htmlb.table.TableViewModel#getValueAt(int, String)
	 */
	public AbstractDataType getValueAt(int rowIndex, String name) {
		return getValueAt(rowIndex, getColumnIndex(name));
	}

	/**
	 * @see com.sapportals.htmlb.table.TableViewModel#setValueAt(AbstractDataType, int, int)
	 */
	public void setValueAt(AbstractDataType data, int rowIndex, int column) {
		// no implementation
	}
}
