package com.sap.isa.core.xcm.admin.model;

public interface HelpConstants {

	public static final int TYPE_SCENARIO                      = 1;
	public static final int TYPE_SCENARIO_PARAM_OPTIONAL       = 2;
	public static final int TYPE_SCENARIO_PARAM_MANDATORY      = 3;
	public static final int TYPE_PARAM_ALLOWED_VALUE           = 4;
	public static final int TYPE_COMPONENT                     = 5;
	public static final int TYPE_COMPONENT_PARAM               = 6;
	public static final int TYPE_COMPONENT_PARAM_ALLOWED_VALUE = 7;
	public static final int TYPE_COMPONENT_CONFIG              = 8;	
	
	public static final String TYPE_NAME_SCENARIO = "Configuration";
	public static final String TYPE_NAME_COMPONENT = "Component";
	public static final String TYPE_NAME_COMPONENT_CONFIG = "Component Configuration";
	public static final String TYPE_NAME_COMPONENT_PARAMETER = "Parameter";
	public static final String TYPE_NAME_PARAMETER_ALLOWED_VALUE = "Parameter Allowed Value";
	public static final String TYPE_NAME_SCENARIO_PARAM_OPTIONAL = "Optional Parameter";
	public static final String TYPE_NAME_SCENARIO_PARAM_MANDATORY = "Mandatory Parameter";
	public static final String TYPE_NAME_PARAM_ALLOWED_VALUE      = "Allowed Value";
	
	
}
