package com.sap.isa.core.xcm.admin.model.help.comp;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import com.sap.isa.core.xcm.XCMUtils;
import com.sap.isa.core.xcm.admin.model.CompDataManipulator;
import com.sap.isa.core.xcm.admin.model.help.HelpItem;
import com.sap.isa.core.xcm.admin.model.help.HelpItemContainer;


public class CompConfigHelpContainer implements HelpItemContainer {
	
	private int numItems;
	private Map mConfigItems = new LinkedHashMap();
	private String mContName;
	
	public CompConfigHelpContainer(String contName, CompDataManipulator.CompConfig compConfig, String compId) {
		mContName = contName;
		Map paramConfigs = compConfig.getParamConfigs();
		numItems = paramConfigs.size();
		for (Iterator iter = paramConfigs.keySet().iterator(); iter.hasNext();) {
			String confName = (String)iter.next();
 			CompDataManipulator.ParamConfig config = 
				compConfig.getParamConfig(confName);
			// check if component has application scope
			
			if (config.isDerivedCompConfig() && !XCMUtils.isApplicationScopeCompConfig(compId))
				continue;
 			String shortText =  XCMUtils.getCompConfigShorttext(compId, confName);		 
			String longText = XCMUtils.getCompConfigLongtext(compId, confName);
			String scope = XCMUtils.getComponentMetadata(compId).getScope();
			CompConfigHelpItem confgHelpItem = new CompConfigHelpItem(confName, shortText, longText, scope);
			mConfigItems.put(confName, confgHelpItem); 
		}
	}
	
	public String getContainerTitle() {
		return mContName;
	}
	
	public int getNumItems() {
		return mConfigItems.size();
	}
	
	
	public HelpItem getItem(String name) {
		return (HelpItem)mConfigItems.get(name);
	}

	public Set getItemNames() {
		return Collections.unmodifiableSet(mConfigItems.keySet());
	}
}
