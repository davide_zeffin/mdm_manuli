package com.sap.isa.core.xcm.admin.model.help.scenario;

import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.xcm.admin.model.CompDataManipulator;
import com.sap.isa.core.xcm.admin.model.HelpConstants;
import com.sap.isa.core.xcm.admin.model.ScenarioDataManipulator;
import com.sap.isa.core.xcm.admin.model.help.HelpItem;
import com.sap.isa.core.xcm.admin.model.help.HelpItemContainer;
import com.sap.isa.core.xcm.init.XCMAdminInitHandler;
import com.sap.isa.core.xcm.init.XCMAdminParamConstrainMetadata;


public class ScenParamHelpContainer implements HelpItemContainer {
	
	protected static IsaLocation log = IsaLocation.
		getInstance(ScenParamHelpContainer.class.getName());
	
	
	private ScenarioDataManipulator.ScenarioConfig mScenConfig;
	private CompDataManipulator mCompMan; 
	private CompDataManipulator.ParamConfig mParamConfig;
	private String mContName;
	private String mType;
	private Set mParamNames = new LinkedHashSet();
	
	/**
	 * Type should be either XCMAdminParamConstrainMetadata.TYPE_OPTIONAL
	 * or XCMAdminParamConstrainMetadata.TYPE_MANDATORY
	 * @param contName
	 * @param type
	 * @param scenConfig
	 */
	public ScenParamHelpContainer(String contName, String type, ScenarioDataManipulator.ScenarioConfig scenConfig, CompDataManipulator mParamConfigMan) {
		mScenConfig = scenConfig;
		mContName = contName;
		mType = type;
		mCompMan = mParamConfigMan; 
		Set allParamNames = mScenConfig.getParmNames();
		for (Iterator iter = allParamNames.iterator(); iter.hasNext();) {
			String paramName = (String)iter.next();


			XCMAdminParamConstrainMetadata scenMetaData = XCMAdminInitHandler.getXCMAdminScenarioMetadata(paramName);
			if (scenMetaData == null) {
				String msg = "There is no meta data for configuration parameter [name]='" + paramName + "'";
				log.warn("system.xcm.warn", new Object[] { msg }, null);
				continue;
			}
				
			// check if type has been overwritten directly in scneario
			// configuration
			String paramType = "";
			ScenarioDataManipulator.ParamConstrain currentConstrain = 
				scenConfig.getParamConstrain(paramName);
			if(currentConstrain != null && currentConstrain.getType() != null && currentConstrain.getType().length() > 0)
				paramType = currentConstrain.getType();
			else
				paramType =  scenMetaData.getType();
			
			
			if (paramType == null) {
				String msg = "no type for [scenario parameter]='" + paramName + "defined. Using default type='optional'"; 
				log.warn("system.xcm.warn", new Object[] { msg }, null);
				paramType = XCMAdminParamConstrainMetadata.TYPE_OPTIONAL;
			}
				
	
			if (paramType.equals(type)) {
					mParamNames.add(paramName);
			}
		}
	}
	
	public String getContainerTitle() {
		return mContName;
	}
	
	public int getNumItems() {
		return mParamNames.size();
	}
	
	
	public HelpItem getItem(String name) {
		if (mType.equals(XCMAdminParamConstrainMetadata.TYPE_OPTIONAL))
			return new ScenParamHelpItem(mScenConfig.getName(), mScenConfig.getParamConfig(name), HelpConstants.TYPE_SCENARIO_PARAM_OPTIONAL, mScenConfig.getBaseConfigName());
		else
			return new ScenParamHelpItem(mScenConfig.getName(), mScenConfig.getParamConfig(name), HelpConstants.TYPE_SCENARIO_PARAM_MANDATORY, mScenConfig.getBaseConfigName());
	}

	public Set getItemNames() {
		return Collections.unmodifiableSet(mParamNames);
	}
}
