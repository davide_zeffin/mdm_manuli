package com.sap.isa.core.xcm.admin.model;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set; 
import java.util.TreeMap;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.sap.isa.core.locking.LockException;
import com.sap.isa.core.locking.LockManager;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.xcm.init.XCMAdminConstants;
import com.sap.isa.core.xcm.init.XCMAdminInitHandler;
import com.sap.isa.core.xcm.xml.XMLConstants;


/**
 * Used to manipulate the scenario definition
 */
public class ScenarioDataManipulator {

	public static final String XML_ELEMENT_CONFIGPARAM     = "configparam";
	public static final String XML_ELEMENT_SCENARIO        = "scenario";
	public static final String XML_ELEMENT_ALLOWEDVALUE    = "allowedvalue";
	public static final String XML_ELEMENT_PARAMCONSTRAINT = "paramconstraint";	
	public static final String XML_ELEMENT_LONGTEXT        = "longtext";
	public static final String XML_ATTR_COMPONENT          = "component";	
	public static final String XML_ATTR_VALUE              = "value";
	public static final String XML_ATTR_NAME               = "name";
	public static final String XML_ATTR_DESCRIPTION        = "description";
	public static final String XML_ATTR_STATUS             = "status";
	public static final String XML_ATTR_SHORTTEXT          = "shorttext";
	public static final String XML_ATTR_ID                 = "id";
	public static final String XML_ATTR_DEFAULT_SCENARIO   = "defaultscenario";
	public static final String TRUE					     = "true";	
	public static final String FALSE					     = "false";
	public static final String XML_ATTR_TYPE               = "type";
	public static final String XML_ATTR_ACTIVE_SCENARIO		= "activescenario";
	
	
	public static final String XML_ELEMENT_ID_VALUE_OVERWRITTEN = "overwritten";
	
	protected static IsaLocation log = IsaLocation.getInstance(ScenarioDataManipulator.class.getName());
	protected static IsaLocation logtest = IsaLocation.getInstance("com.sap.isa.mba");
		
	private static int mScenNameLength;


	private String mDocHref = null;
	private String mBaseHref = null;	
	private Document mDocBeforeInclude;
	private Document mDocAfterInclude;
	private Document mDocAfterExtension;
	private XMLAdminUtil mXmlAdmin;
	private Map mScenarios;
	
	private boolean targetDatastoreDB = false;
	private String appName;	

	/**
	 * Container for scenario configuration
	 */
	public static class ScenarioConfig {
		
		private Map mParamConfigs = new LinkedHashMap();
		private Map mParamValues;
		private Map mParamConstrains = new LinkedHashMap();
		private Element mCurrentElement;
		private String mScenName; 
		private String mScenShorttext;
		private String mLongtext;
		private String mCompId;
		private boolean mObsolete = false;
		
		/**
		 * Constructor
		 * @param the root node of the scenario configuration
		 */
		public ScenarioConfig(String scenName, Element rootElement) {
			mScenName = scenName;
			mCurrentElement = rootElement;
			init();
		}
	
	
		private void init() {
			// get shorttext of configuration
			mScenShorttext =  mCurrentElement.getAttribute(XML_ATTR_DESCRIPTION);
			if (mScenShorttext != null && mScenShorttext.length() == 0) {
				mScenShorttext = mCurrentElement.getAttribute(XML_ATTR_SHORTTEXT);
				mScenShorttext = XCMAdminInitHandler.getText(mScenShorttext);
			}
			
			// get type of configuraiton
			String status = mCurrentElement.getAttribute(XML_ATTR_STATUS);
			if (status != null && status.equals(XCMAdminConstants.OBSOLETE))
				mObsolete = true;
			
			
			// get longtext
			NodeList longtextElements = (NodeList)mCurrentElement.getChildNodes();
			for (int i = 0; i < longtextElements.getLength(); i++) {
				Node currentNode = longtextElements.item(i);
				if (currentNode.getNodeType() != Node.ELEMENT_NODE)
					continue;
				if (currentNode.getNodeName().equals(XML_ELEMENT_LONGTEXT)) {
					mLongtext = currentNode.getFirstChild().getNodeValue();
					mLongtext = XCMAdminInitHandler.getText(mLongtext);
				}
			}
			
			if (log.isDebugEnabled())
				log.debug("Reading scenarion data [name]='" + mScenName + "'");
			
			NodeList params = ((Element)mCurrentElement).getElementsByTagName(XML_ELEMENT_CONFIGPARAM);
			for (int i = 0; i < params.getLength(); i++) {
				Node paramNode = params.item(i);
				if (paramNode.getNodeType() != Node.ELEMENT_NODE)
					continue;
				Element paramElement = (Element)paramNode;
				String name = paramElement.getAttribute(XML_ATTR_NAME);
				ScenarioDataManipulator.ParamConfig paramConfig = new ParamConfig(mCurrentElement, paramElement);
				mParamConfigs.put(name, paramConfig);
			}
			
			// get paramcontraint nodes
			NodeList constrains = ((Element)mCurrentElement).getElementsByTagName(XML_ELEMENT_PARAMCONSTRAINT);
			for (int i = 0; i < constrains.getLength(); i++) {
				Element contraintElement = (Element)constrains.item(i);
				String paramName = ((Element)contraintElement).getAttribute(XML_ATTR_NAME);
				ScenarioDataManipulator.ParamConstrain paramConstrain = 
				   new ScenarioDataManipulator.ParamConstrain(contraintElement);
				mParamConstrains.put(paramName, paramConstrain);
			}
			
		}
	
	
		public boolean isDefaultScenario() {
			String defaultScenarioElement = mCurrentElement.getAttribute(XML_ATTR_DEFAULT_SCENARIO);
			if (defaultScenarioElement == null 
				|| defaultScenarioElement.length() == 0
				|| defaultScenarioElement.equalsIgnoreCase(FALSE))
				return false;
			
			if (defaultScenarioElement.equalsIgnoreCase(TRUE))
				return true;
			
			return false;
		}
		
		public boolean isActiveScenario() {
			// every deprecated scenario is automatically de-activated
			if (isObsolete())
				return true;
				
			String activeScenarioElement = mCurrentElement.getAttribute(XML_ATTR_ACTIVE_SCENARIO);
			if (activeScenarioElement == null 
				|| activeScenarioElement.length() == 0){
				
					if (activeScenarioElement == null 
						|| activeScenarioElement.length() == 0)
							log.debug("");
				return true; // scenario is by default active
				}
				
	
			if (activeScenarioElement.equalsIgnoreCase(TRUE))
				return true;
			else
				return false;
		}
		
		private void setDefaultScenario(boolean defaultScenario) {
			mCurrentElement.setAttribute(XML_ATTR_DEFAULT_SCENARIO, String.valueOf(defaultScenario));
		}
		
		private void setActiveScenario(boolean activeScenario){
			mCurrentElement.setAttribute(XML_ATTR_ACTIVE_SCENARIO, String.valueOf(activeScenario));
		}
		
		public Set getParmNames() {
			return Collections.unmodifiableSet(mParamConfigs.keySet());		
		}
		
		public ScenarioDataManipulator.ParamConfig getParamConfig(String paramName) {
			return (ScenarioDataManipulator.ParamConfig)mParamConfigs.get(paramName);
		}

		public Element getRootNode() {
			return mCurrentElement;
		}

		/**
		 * Returns shorttext of this scenario
		 */
		public String getShorttext() {
			return mScenShorttext;
		}
		
		/**
		 * Returns true if the scenario has been marked as deprecated
		 * @return true if the scenario has been marked as deprecated
		 */
		public boolean isObsolete() {
			return mObsolete;
		}
		
		/** 
		 * Sets the short text
		 * @param shortText
		 */
		public void setShortText(String shortText) {
			mCurrentElement.setAttribute(XML_ATTR_SHORTTEXT, shortText);
			mScenShorttext = shortText;
		}

		/**
		 * Returns longtext of this scenario
		 */
		public String getLongtext() {
			return mLongtext;
		}

		
		/**
		 * Returns <code>true</code>if this scenario is derived from an other
		 * scenario
		 * @return <code>true</code>if this scenario is derived from an other
		 * scenario
		 */
		public boolean isScenarioDerived() {
			
			String extended = mCurrentElement.getAttribute(XMLConstants.EXTENDED_NS);
			String base = mCurrentElement.getAttribute(XMLConstants.ISA_BASE_NS);
			if (extended.length() > 0 || base.length() > 0) {
				return true;
			}
			return false;
		}

		public String getBaseConfigName() {
			String retVal = "";
			String extended = mCurrentElement.getAttribute(XMLConstants.EXTENDED_NS);
			if (extended.length() == 0)
				return retVal;
			
			int first = extended.indexOf("'");
			if (first == -1)
				return retVal;
			int last = extended.lastIndexOf("'");
			if (last > first)
				return extended.substring(first + 1, last);
			else
				return retVal;			
		}

		public String getParameterValue(String paramName) {
			String paramValue = (String)mParamValues.get(paramName);
			if (paramValue == null)
				return ""; 
			else
				return paramValue;			
		}

		/**
		 * Returns a Map containing the parameter name/values
		 */
		public Map getParamValues() {
				
			mParamValues = new LinkedHashMap();
			NodeList params = ((Element)mCurrentElement).getElementsByTagName(XML_ELEMENT_CONFIGPARAM);
			for (int i = 0; i < params.getLength(); i++) {
				Node paramNode = params.item(i);
				if (paramNode.getNodeType() != Node.ELEMENT_NODE)
					continue;
				Element paramElement = (Element)paramNode;
				String name = paramElement.getAttribute(XML_ATTR_NAME);
				String value = paramElement.getAttribute(XML_ATTR_VALUE);
				mParamValues.put(name, value);
			}
			return mParamValues;
		}

		/**
		 * Returns the name of the scenario
		 */
		public String getName() {
			return mCurrentElement.getAttribute(XML_ATTR_NAME);
		}
		
		public boolean hasParamConstrains(String paramName) {
			ParamConstrain paramConstrain = 
				(ParamConstrain)mParamConstrains.get(paramName);
				
			if (paramConstrain.getAllowedValues().size()== 0)
				return false;
			else
				return true;
			
		}
		
		public Set getAvailableParamConstrainNames() {
			return Collections.unmodifiableSet(mParamConstrains.keySet());
		}
		
		public ParamConstrain getParamConstrain(String paramName) {
			return (ParamConstrain)mParamConstrains.get(paramName);
		}
		
	}

	public static class ParamConstrain {
		
		private String mShorttext;
		private String mLongtext;
		private Element mRootElement;
		private String mParamName;
		private String mParamType;
 		private Map mAllowedFixValues = new LinkedHashMap();
		
		public ParamConstrain(Element rootElement) {
			mRootElement = rootElement;
			mParamType = rootElement.getAttribute(XML_ATTR_TYPE);
			mShorttext = rootElement.getAttribute(XML_ATTR_SHORTTEXT);
			NodeList childnodes = mRootElement.getChildNodes();
			for (int i = 0; i < childnodes.getLength(); i++ ) {
				Node currentNode = childnodes.item(i);
				if (currentNode.getNodeType() != Node.ELEMENT_NODE)
					continue;
				Element currentElement = (Element)currentNode;
				if (currentElement.getNodeName().equals(XML_ELEMENT_ALLOWEDVALUE)) {
					String allowedValue = currentElement.getAttribute("value");
					mAllowedFixValues.put(allowedValue, currentElement);
				}
				if (currentElement.getNodeName().equals(XML_ELEMENT_LONGTEXT)) {
					mLongtext = currentElement.getFirstChild().getNodeValue();
					mLongtext = XCMAdminInitHandler.getText(mLongtext);
				}
			}

		}
		
		/**
		 * The name of the parameter the constrain is valid for
		 */
		public String getParamName() {
			return mParamName;
		}
		
		/**
		 * The type of the parameter
		 */
		public String getType() {
			return mParamType;
		}
		
		public Set getAllowedValues() {
			return mAllowedFixValues.keySet();
		}
		
		public String getShorttext() {
			return mShorttext;
		}
		
		public String getLongtext() {
			return mLongtext;
		}
	}

	/**
	 * Container for a scenario parameter
	 */
	public static class ParamConfig {

		private Element mParamRootElement;
		private Element mParentNode;
 
		public ParamConfig(Node parentNode, Node rootNode) {
			mParamRootElement = (Element)rootNode;
			mParentNode = (Element)parentNode;
		}

		public boolean isDerived() {
			String nodeId = mParamRootElement.getAttributeNS(XMLConstants.ISA_NAMESPACE, XMLConstants.NODE_ID);
			if (nodeId == null || nodeId.length() == 0)
				return false;
			else
				return true;
		}
 
		private boolean isScenarioDerived() {
			
			String extended = mParentNode.getAttribute(XMLConstants.EXTENDED_NS);
			String base = mParentNode.getAttribute(XMLConstants.ISA_BASE_NS);
			if (extended.length() > 0 || base.length() > 0) {
				return true;
			}
			return false;
		}

 
		/**
		 * Sets value 
		* @param value parameter value
		 */
		public void setValue(String newValue) {

			if (newValue == null)
				newValue = "";
			
			// if new value equals old value then do nothing 
			String oldValue = mParamRootElement.getAttribute(XML_ATTR_VALUE);

			if (oldValue == null) 
				oldValue = "";
			
			// if same value, do nothing
			if (oldValue.equals(newValue)) 
				return;
			
			// if derived then overwrite (add elementid)
			if (isScenarioDerived()) {
				// save the old  value
				mParamRootElement.setAttribute(XMLConstants.SAVE_PARAM_VALUE, oldValue);
				mParamRootElement.setAttribute(XML_ATTR_VALUE, newValue);
				XMLAdminUtil.addElementId(mParamRootElement);
			} else
				mParamRootElement.setAttribute(XML_ATTR_VALUE, newValue);
		}

		/**
		 * Restores a derived value
		 * @param name name of parameter which value is to be restored
		 */
		public void restoreValue() {
			String currentValue = mParamRootElement.getAttribute(XML_ATTR_VALUE);
			String derivedValue = mParamRootElement.getAttribute(XMLConstants.SAVE_PARAM_VALUE);			
			
			if (currentValue.equals(derivedValue))
				return;
			
			mParamRootElement.setAttribute(XML_ATTR_VALUE, derivedValue);
			XMLAdminUtil.removeElementId(mParamRootElement);
		
		}	


		/**
		 * returns the value of this parameter
		 */
		public String getValue() {
			return mParamRootElement.getAttribute(XML_ATTR_VALUE);
		}

		

		/**
		 * returns the name of this parameter
		 */
		public String getName() {
			return mParamRootElement.getAttribute(XML_ATTR_NAME);
		}
	
	}

	public void init(XMLAdminUtil adminUtil) throws XCMAdminException {
	
		mXmlAdmin = adminUtil;
		
		// get document as stored on filesystem
		// additionally an 'isa:elementid' is added to every node
		setDocBeforeInclude(mXmlAdmin.loadDocument(mBaseHref, mDocHref));
	}

	private void setDocBeforeInclude(Document docBeforeInclude) throws XCMAdminException {

		mDocBeforeInclude = (Document)docBeforeInclude.cloneNode(true);
		// save xinclude information
		mXmlAdmin.saveXInclude(mDocBeforeInclude);
		// perform include 
		mDocAfterInclude = mXmlAdmin.processXInclude(mBaseHref,mDocBeforeInclude);
		// perform extension
		mDocAfterExtension = mXmlAdmin.processExtension(mDocAfterInclude);	

		NodeList scenarios = mDocAfterExtension.getElementsByTagName(XML_ELEMENT_SCENARIO);
		
		mScenarios = Collections.synchronizedMap(new TreeMap());
		
		for (int i = 0; i < scenarios.getLength(); i++) {
			Element scenarioElement = (Element)scenarios.item(i);
			String scenarioName = scenarioElement.getAttribute(XML_ATTR_NAME);
			ScenarioDataManipulator.ScenarioConfig scenario = 
				new ScenarioDataManipulator.ScenarioConfig(scenarioName, scenarioElement);
			//mScenariosSet.add(scenario);
			logtest.debug("Add scenario" + scenarioName); 
			mScenarios.put(scenarioName, scenario);
		}		
	}
	
	/**
	 * Adds a new configuration
	 */
	public void createScenarioConfig(String scenarioName, String baseScenarioName) {
		if (log.isDebugEnabled()) {
			log.debug("Adding new scenario configuration [name]='" + scenarioName + 
			"' based on scenario [base]='" + baseScenarioName + "'");
		}
		// get base configuration
		ScenarioConfig baseScenarioConfig = (ScenarioConfig)mScenarios.get(baseScenarioName);
		Element baseConfigNode = baseScenarioConfig.getRootNode();
		Element newConfigNode = (Element)baseConfigNode.cloneNode(true);
		// set new id
		newConfigNode.setAttribute(XML_ATTR_NAME, scenarioName);
		// add new node
		baseConfigNode.getParentNode().insertBefore(newConfigNode, baseConfigNode);
		// add new scenario configuration
		mScenarios.put(scenarioName, new ScenarioConfig(scenarioName, newConfigNode));
		// add element id in order to prevent this node to be deleted
		newConfigNode.setAttributeNS(XMLConstants.ISA_NAMESPACE, 
							XMLConstants.NODE_ID_NS, XML_ELEMENT_ID_VALUE_OVERWRITTEN);
				
		// add element id node to all parent nodes
		XMLAdminUtil.addElementId(newConfigNode);
 
		
		// eventually add the isa:extends attribute
		String isaExtends = ((Element)newConfigNode).getAttribute(XMLConstants.EXTENDED_NS);
		if (isaExtends.length() == 0) {
			// add isa:extends attribute
			String extendsValue = "../scenario[@name='" + baseScenarioName + "']";
			((Element)newConfigNode).setAttributeNS(
				XMLConstants.ISA_NAMESPACE, XMLConstants.EXTENDED_NS, extendsValue);
			// remove name attribute
//			((Element)newConfigNode).removeAttribute(XML_ATTR_NAME);

		}
	}		

	/**
	 * Deletes an existing parameter configuration
	 */
	public void deleteScenarioConfig(String scenarioName) {
		ScenarioConfig config = (ScenarioConfig)mScenarios.get(scenarioName);
		if (config == null)
			return;

		// sap scenarios should not be deleted
		if (!config.isScenarioDerived())
			return;
		
		Node rootNode = config.getRootNode(); 
		rootNode.getParentNode().removeChild(rootNode);	
		log.debug("scenario name: " + scenarioName);
		mScenarios.remove(scenarioName);
		// check existing parmeter configuration whether they are in the SAP folder
		// component config in the customer file can be deleted
		XMLAdminUtil.removeElementId(rootNode); 
	}

	public boolean isScenarioAvailable(String scenName) {
		if (scenName != null)
			return mScenarios.containsKey(scenName);
		else
			return false;
	}

	/**
	 * Sets the HREF to the extending document
	 * @param href HREF to the extending document
	 */
	public void setDocHREF(String href) {
		mDocHref = href;	
	}

	/**
	 * Gets the HREF to the extending scenario-config document
	 * @param href HREF to the extending document
	 */
	public String getDocHREF() {
		return mDocHref;	
	}


	/**
	 * Sets the base HREF
	 * @param href base HREF 
	 */
	public void setBaseHREF(String href) {
		mBaseHref = href;	
	}
	
	/**
	 * @param flag true => target datastore is database else filesystem
	 */
	public void setTargetDataStoreDB(boolean flag) {
		this.targetDatastoreDB = flag;
	}
	
	public void setApplicationName(String name) {
		this.appName = name;
	}
	
	public String getApplicationName() {
		return appName;
	}
	
	/**
	 * @return true of the target datastore is database
	 */
	public boolean isTargetDatastoreDB() {
		return targetDatastoreDB;
	}
	
	/**
	 * Returns a DOM representation of the extending document
	 * @return a DOM representation of the extending document
	 */
	public Document getDocBeforeInclude() throws XCMAdminException {
		return mDocBeforeInclude;
	}
	
	/**
	 * Returns a DOM representation of the result document
	 * @return a DOM representation of the extending document
	 */
	public Document getDocAfterInclude() throws XCMAdminException {
		return mDocAfterInclude;
	}

	/**
	 * Returns a document with processed extensions
	 */
	public Document getDocAfterExtension() {
		return mDocAfterExtension;	
	}

	/**
	 * Returns a set containing scenario configurations
	 */
	public Map getScenarioConfigs() {
		return mScenarios;
	}
	

	/**
	 * Returns a configuration for the given scenarion name
	 */ 
	public ScenarioDataManipulator.ScenarioConfig getScenarioConfig(String scenName) {
		if (scenName == null)
			return null;
		if (!mScenarios.containsKey(scenName))
			return null;
		ScenarioDataManipulator.ScenarioConfig config = (ScenarioDataManipulator.ScenarioConfig)mScenarios.get(scenName);
		if (config == null)
			return null;
			
		return config;
	}

	/**
	 * Returns a string representation of the XML file after removing 
	 * all nodes without <code>isa:elementid<code>
	 * @return 
	 */
	public String getExtendingDocAsString() {
		return XMLAdminUtil.getExtendingDocAsString(mDocAfterExtension);
	}

	/**
	 * Saves the current configuration to the data store
	 */
	public void saveConfiguration() throws XCMAdminException {
		mXmlAdmin.saveConfiguration(getExtendingDocAsString());
	}
	
	/**
	 * Overwrite the configuration data (e.g. used in upload)
	 * @param data
	 * @throws Exception
	 */
	public void overwriteConfiguration(Document docBeforeInclude) throws XCMAdminException {
		if(docBeforeInclude != null)
			setDocBeforeInclude(docBeforeInclude);
	}

	public void invalidateConfiguration() throws XCMAdminException {
		setDocBeforeInclude(mXmlAdmin.loadConfigurationDocument());
	}

	public void refreshConfiguration() throws XCMAdminException {
		mXmlAdmin.refreshConfiguration();
		setDocBeforeInclude(mXmlAdmin.loadConfigurationDocument());
	}
	
	
	public void lockConfiguration(LockManager lm) throws LockException {
		mXmlAdmin.lockConfiguration(lm);
	}
	
	public void unlockConfiguration(LockManager lm) throws LockException {
		mXmlAdmin.unlockConfiguration(lm);
	}
	
	/**
	 * Sets the config with the given id as default
	 * All others are set to false
	 */
	public void setAsDefaultConfig(String configId) {
		ScenarioConfig config = (ScenarioConfig)mScenarios.get(configId);
		if (config == null)
			return;
		for (Iterator iter = mScenarios.values().iterator(); iter.hasNext(); ) {
			ScenarioConfig currentConfig = (ScenarioConfig)iter.next();
			currentConfig.setDefaultScenario(false);
		}
		config.setDefaultScenario(true);
	}
	
	public void setAsActive(String configId, boolean active) {
		ScenarioConfig config = (ScenarioConfig)mScenarios.get(configId);
		if (config == null)
			return;
		log.debug("in setasactive=" + active);
		config.setActiveScenario(active);
	}
 
	/**
	 * Sets shorttext for a given config
	 * All others are set to false
	 */
	public void setScenarioShortText(String configId, String shortText) {
		ScenarioConfig config = (ScenarioConfig)mScenarios.get(configId);
		if (config == null)
			return;
		for (Iterator iter = mScenarios.values().iterator(); iter.hasNext(); ) {
			ScenarioConfig currentConfig = (ScenarioConfig)iter.next();
			currentConfig.setShortText(shortText);
		}
	}

	
	/**
	 * set this scenario as default if it is the only customer
	 * scenario
	 * @return if this has happened
	 */
	public boolean setSingleCustomerScenarioAsDefault() {
		int numCustScen = 0;
		String nameCustScen = null;		
		for (Iterator iter = mScenarios.values().iterator(); iter.hasNext(); ) {
			ScenarioConfig currentConfig = (ScenarioConfig)iter.next();
			if (currentConfig.isScenarioDerived()) {
				numCustScen++;
				nameCustScen = currentConfig.getName(); 
			}
			if (numCustScen > 1)
				return false;
		}
		setAsDefaultConfig(nameCustScen);
		return true;
		
	}

}
