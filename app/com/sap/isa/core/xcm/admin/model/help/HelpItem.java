package com.sap.isa.core.xcm.admin.model.help;

public interface HelpItem {

	/**
	 * The type of the item.
	 * @return int
	 */
	public int getType();
	
	/**
	 * The type of the item as text. e.g. Scenario
	 * @return String
	 */
	public String getTypeName();

	public String getName();

	public String getShorttext();
	 
	public String getLongtext();
	
	public String getBaseConfigName();
	
	public String getConfigValue();
	
	public String getDependendCompName();
	
	public HelpItemContainer getChildren();
	
	public String getScope();
}
