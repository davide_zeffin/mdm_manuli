package com.sap.isa.core.xcm.admin.model.table;

// java imports 
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import com.sap.isa.core.xcm.admin.controller.ComponentConfigController;
import com.sap.isa.core.xcm.admin.model.CompDataManipulator;
import com.sap.isa.core.xcm.init.XCMAdminComponentParamMetadata;
import com.sap.isa.core.xcm.init.XCMAdminConstants;
import com.sap.isa.core.xcm.init.XCMAdminParamConstrainMetadata;
import com.sapportals.htmlb.table.TableColumn;
import com.sapportals.htmlb.type.AbstractDataType;
import com.sapportals.htmlb.type.DataString;
import com.sap.isa.core.init.InitializationHandler;

/**
 *
 */
public class ComponentDetailTableModel extends AdminBaseTable {

	private static org.apache.struts.util.MessageResources mMessageResources =
						InitializationHandler.getMessageResources();
	public static final String ON_NAVIGATE_EVENT =
		"OnNavigate.ComponentDetailTableModel.toolbar.table.model.xcm.core.isa.sap.com";

	public static final String NAME_PARAM_VALUE_INPUT_FIELD = "paramValueInputFileld";
	public static final String NAME_PARAM_VALUE_DROPDOWN_LISTBOX = "paramValueDrowpdownListbox";
	
	public static final int COL_INDEX_PARAM_VALUE = 2;
	public static final int COL_INDEX_PARAM_NAME  = 1;
	
	public static final String TABLE_HEADER_C1_TEXT = mMessageResources.getMessage("xcm.compconf.tab.h1.te");
	public static final String TABLE_HEADER_C1_TOOLTIP = mMessageResources.getMessage("xcm.compconf.tab.h1.to");
	public static final String TABLE_HEADER_C2_TEXT = mMessageResources.getMessage("xcm.compconf.tab.h2.te");
	public static final String TABLE_HEADER_C2_TOOLTIP = mMessageResources.getMessage("xcm.compconf.tab.h2.to");
	public static final String TABLE_HEADER_C3_TOOLTIP = mMessageResources.getMessage("xcm.compconf.tab.h3.to");
	public static final String TABLE_HEADER_C4_TOOLTIP = mMessageResources.getMessage("xcm.compconf.tab.h4.te");
	public static final String TABLE_HEADER_C5_TEXT = mMessageResources.getMessage("xcm.compconf.tab.h5.te");
	public static final String TABLE_HEADER_C5_TOOLTIP = mMessageResources.getMessage("xcm.compconf.tab.h5.to");
	public static final String TABLE_HEADER_C6_TOOLTIP = mMessageResources.getMessage("xcm.compconf.tab.h6.to");
	

	private ArrayList props = new ArrayList();
	private List mParams = Collections.EMPTY_LIST; 
	private boolean readOnly;
	private ComponentConfigController mCompConfigController;
	
	private class CompConfigRow {

		private String mName;
		private String mValue;
		private String mBaseValue;		
		private String mDescr;
		private boolean mIsDerived;
		private boolean mIsPassword;
		private String mType;
		private Set mAllowedValues;
		private String mStatus;
	
		CompConfigRow(String name, String value, String baseValue, String descr, boolean isDerived, boolean isPassword, String type, Set allowedValues, String status) {
			mName = name;
			mValue = value;	
			mBaseValue = baseValue;
			mDescr = descr;
			mIsDerived = isDerived;
			mIsPassword = isPassword;
			mType = type;	
			mAllowedValues = allowedValues;
			mStatus = status;
		}
	
		public String getName() {
			return mName;
		}
		
		public String getValue() {
			return mValue;
		}

		public String getBaseValue() {
			return mBaseValue;
		}

		public String getDescription() {
			return mDescr;
		}
		public String getType() {
			return mType;
		}
		public Set getAllowedValues() {
			return mAllowedValues;
		}
		
		public boolean isDerived() {
			return mIsDerived;
		}

		public boolean isPassword() {
			return mIsPassword;
		}
	
		public void setValue(String value) {
			mValue = value;
		}
		
		public String getStatus(){
			return mStatus;
		}
	}
	
	/**
	 * @param paramConfig a map containing CompDataManipulator.ParamsConfig
	 */
	
	public ComponentDetailTableModel(CompDataManipulator.ParamConfig paramConfig) { //, ComponentConfigController compConfController) {
		//mCompConfigController = compConfController;
		if (paramConfig == null)
			return;
		mParams = new LinkedList();
		String empty = "empty";
		mParams.add(new CompConfigRow(empty, empty, empty, empty,false, false, empty, null, empty));
		for (Iterator iter = paramConfig.getParamNames().iterator(); iter.hasNext(); ) {
				String name = (String)iter.next();
				String descr = paramConfig.getShorttext(name);
				String value = paramConfig.getValue(name);
				String baseValue = paramConfig.getBaseValue(name);
				boolean isDerived = paramConfig.isDerived(name);
				boolean isPassword = paramConfig.isPassword(name);
				String type = paramConfig.getType(name);
				String status = paramConfig.getStatus(name);
				Set allowedValues = paramConfig.getAllowedValues(name);
				CompConfigRow row = new CompConfigRow(name, value, baseValue, descr, isDerived, isPassword, type, allowedValues, status);
				mParams.add(row);
			}
		}

	/**
	 * @see com.sap.isa.core.xcm.admin.model.table.AdminBaseTable#init()
	 */
	protected void init() {
		if (log.isDebugEnabled())	
			log.debug("Initializing 'ComponentDetailTable' table");
			
  	    TableColumn nameolumn = null;
  	    	addColumn(TABLE_HEADER_C1_TEXT, TABLE_HEADER_C1_TOOLTIP);
			addColumn(TABLE_HEADER_C2_TEXT, TABLE_HEADER_C2_TOOLTIP);
			addColumn("", TABLE_HEADER_C3_TOOLTIP);  		
			addColumn("", TABLE_HEADER_C4_TOOLTIP);	
			addColumn(TABLE_HEADER_C5_TEXT, TABLE_HEADER_C5_TOOLTIP);
			addColumn("", TABLE_HEADER_C6_TOOLTIP);			
  	         	
 	}

	/**
	 * @see com.sapportals.htmlb.table.TableViewModel#getRowCount()
	 */
	public int getRowCount() {
		return mParams.size() - 1;
	}

	/**
	 * @see com.sapportals.htmlb.table.TableViewModel#getValueAt(int, int)
	 */
	public AbstractDataType getValueAt(int rowIndex, int index) {
	    String retVal = "";
	    switch (index) {
	    	case 1: { 
	    			retVal = getParamName(rowIndex);
	    			break;
	    	}

	    	case 2: {
		    	retVal = getParamValue(rowIndex);
		    	break;
	    	}

	    	case 4: {
		    	retVal = getParamBaseValue(rowIndex);
		    	break;
	    	}

	    	case 5: {
		    	retVal = getParamDescription(rowIndex);
		    	break;
	    	}
	    	
	    	case 6: {
	    		
	    		 if(isDerived(rowIndex))
	    		 	retVal = "true";
	    		 else
	    		 	retVal = "false";
	    	}
	    }
		return new DataString(retVal);
	}
	
	public String getParamValueType(int rowIndex) {
		CompConfigRow row = (CompConfigRow)mParams.get(rowIndex);
		return row.getType();
	}

	/**
	 * @see com.sapportals.htmlb.table.TableViewModel#getValueAt(int, String)
	 */
	public AbstractDataType getValueAt(int rowIndex, String name) {
	    return getValueAt(rowIndex, getColumnIndex(name));
	}

	/**
	 * @see com.sapportals.htmlb.table.TableViewModel#setValueAt(AbstractDataType, int, int)
	 */
	public void setValueAt(AbstractDataType data, int rowIndex, int column) {
		if (rowIndex >= mParams.size())
			return;
		CompConfigRow row = (CompConfigRow)mParams.get(rowIndex);
		String value = data.getValueAsString();
		value.trim(); 
		row.setValue(value);
	}

	public boolean isDerived(int rowIndex) {
		if (rowIndex >= mParams.size())
			return false;
		CompConfigRow row = (CompConfigRow)mParams.get(rowIndex);
		return row.isDerived();
	}
	
	public String getStatus(int rowIndex){
		if (rowIndex >= mParams.size())
			return "";
		CompConfigRow row = (CompConfigRow)mParams.get(rowIndex);
		return row.getStatus();
	}

	public String getParamType(int rowIndex) {
		CompConfigRow row = (CompConfigRow)mParams.get(rowIndex);
		return row.getType();
	}
	
	public Set getParamValues(int rowIndex) {
		CompConfigRow row = (CompConfigRow)mParams.get(rowIndex);
		return row.getAllowedValues();
	}

	public boolean isPassword(int rowIndex) {
		if (rowIndex >= mParams.size())
			return false;
		CompConfigRow row = (CompConfigRow)mParams.get(rowIndex);
		return row.isPassword();
	}

	public boolean isPassword(String name) {
		for (int i = 0; i < getRowCount(); i++) {
			String currentName = getValueAt(i, 1).getValueAsString();
			if (currentName.equals(name))
				return isPassword(i);
		}
		return false;
	}

	private String getParamName(int rowIndex) {
		if (rowIndex >= mParams.size())
			return "";
		CompConfigRow row = (CompConfigRow)mParams.get(rowIndex);
		if (row == null)
			return "";
		return row.getName();
	}

	private String getParamValue(int rowIndex) {
		if (rowIndex >= mParams.size())
			return "";

		CompConfigRow row = (CompConfigRow)mParams.get(rowIndex);
		if (row == null)
			return "";
		return row.getValue();
	}
	private String getParamDescription(int rowIndex) {
		if (rowIndex >= mParams.size())
			return "";

		CompConfigRow row = (CompConfigRow)mParams.get(rowIndex);
		if (row == null)
			return "";
		if (row.getStatus()!= null && row.getStatus().equals(XCMAdminConstants.OBSOLETE)) {
			return XCMAdminConstants.OBSOLETE.toUpperCase() + " " + row.getDescription(); 
		}
		return row.getDescription();
	}	
	private String getParamBaseValue(int rowIndex) {
		if (rowIndex >= mParams.size())
			return "";

		CompConfigRow row = (CompConfigRow)mParams.get(rowIndex);
		if (row == null)
			return "";
		return row.getBaseValue();
	}
	
		
	/**
	 * @return
	 */
	public boolean isReadOnly() {
		return readOnly;
	}

	/**
	 * @param b
	 */
	public void setReadOnly(boolean b) {
		readOnly = b;
	}
}
