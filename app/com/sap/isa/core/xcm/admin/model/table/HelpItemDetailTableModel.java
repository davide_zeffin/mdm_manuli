package com.sap.isa.core.xcm.admin.model.table;

// java imports 
import com.sap.isa.core.xcm.admin.model.help.HelpItem;
import com.sapportals.htmlb.table.TableColumn;
import com.sapportals.htmlb.type.AbstractDataType;
import com.sapportals.htmlb.type.DataString;

/**
 * 
 */
public class HelpItemDetailTableModel extends AdminBaseTable {

	private HelpItem mHelpItem;

	public HelpItemDetailTableModel(HelpItem helpItem) {
		mHelpItem = helpItem;
	}

	/**
	 * @see com.sap.isa.core.xcm.admin.model.table.AdminBaseTable#init()
	 */
	protected void init() {
		if (log.isDebugEnabled())	
			log.debug("Initializing 'HelpItemDetailTableModel' table");
			
  	    TableColumn nameolumn = null;
  		addColumn("Scenario", "Scenario");
  		addColumn("Description", "Description");
	}

	/**
	 * @see com.sapportals.htmlb.table.TableViewModel#getRowCount()
	 */
	public int getRowCount() {
		return 1;
	}

	/**
	 * @see com.sapportals.htmlb.table.TableViewModel#getValueAt(int, int)
	 */
	public AbstractDataType getValueAt(int rowIndex, int index) {
	    String retVal = "";
	    switch (index) {
	    	case 1: { 
	    		retVal = mHelpItem.getName();
	    		break;
	    	}
	    	case 2: {
				retVal = mHelpItem.getLongtext();
		    	break;
	    	}
	    }
		return new DataString(retVal);
	}

	/**
	 * @see com.sapportals.htmlb.table.TableViewModel#getValueAt(int, String)
	 */
	public AbstractDataType getValueAt(int rowIndex, String name) {
	    return getValueAt(rowIndex, getColumnIndex(name));
	}

	/**
	 * @see com.sapportals.htmlb.table.TableViewModel#setValueAt(AbstractDataType, int, int)
	 */
	public void setValueAt(AbstractDataType data, int rowIndex, int column) {
		// no implementation
	}

}
