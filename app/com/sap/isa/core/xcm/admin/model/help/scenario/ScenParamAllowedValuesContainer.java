package com.sap.isa.core.xcm.admin.model.help.scenario;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.sap.isa.core.xcm.XCMUtils;
import com.sap.isa.core.xcm.admin.Constants;
import com.sap.isa.core.xcm.admin.controller.ControllerUtils;
import com.sap.isa.core.xcm.admin.model.CompDataManipulator;
import com.sap.isa.core.xcm.admin.model.HelpConstants;
import com.sap.isa.core.xcm.admin.model.ScenarioDataManipulator;
import com.sap.isa.core.xcm.admin.model.help.HelpItem;
import com.sap.isa.core.xcm.admin.model.help.HelpItemBase;
import com.sap.isa.core.xcm.admin.model.help.HelpItemContainer;


public class ScenParamAllowedValuesContainer implements HelpItemContainer {
	
	private ScenarioDataManipulator mScenMan;
	private CompDataManipulator mCompMan;
	private String mScenName;
	private String mParamName;
	
	private String mContName;
	private String mType;
	private Map mAllowedValues = new LinkedHashMap();
	private List mAllowedValueNames = new LinkedList();
	
	
	public ScenParamAllowedValuesContainer(String contName, String scenName, String paramName, ScenarioDataManipulator scenMan, CompDataManipulator compMan) {
		mContName = contName;
		mScenName = scenName;
		mParamName = paramName;
		mScenName = scenName;
		mCompMan = compMan;
		mAllowedValueNames = ControllerUtils.getScenarioParamAllowedValues(scenName, paramName, scenMan, compMan);
		int i = 0;
		for (Iterator iter = mAllowedValueNames.iterator(); iter.hasNext();) {
			i++;
			String name= (String)iter.next();
			if (name.equals(Constants.CREATE_NEW_CONFIG_ID))
				continue;
			String longText = XCMUtils.getScenarioConfigParamValueLongtext(paramName, name);
			String shortText = ControllerUtils.getScenarioConfigParamValueShorttext(paramName, name, compMan);
			HelpItemBase allowedValue = 
				new HelpItemBase(HelpConstants.TYPE_PARAM_ALLOWED_VALUE, name, shortText, longText, "TODO: Base Config", "", "",  "");
			mAllowedValues.put(name, allowedValue);
		}
	}
	
	public String getContainerTitle() {
		return mContName;
	}
	
	public int getNumItems() {
		return mAllowedValues.size();
	}
	
	
	public HelpItem getItem(String name) {
		return (HelpItem)mAllowedValues.get(name);
	}

	public Set getItemNames() {
		return Collections.unmodifiableSet(mAllowedValues.keySet());
	}
}
