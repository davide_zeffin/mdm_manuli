package com.sap.isa.core.xcm.admin.model.table;

// java imports 

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.sapportals.htmlb.type.AbstractDataType;
import com.sapportals.htmlb.type.DataString;
import com.sap.isa.core.xcm.admin.model.table.MonitoringFileTableModel;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.net.URLEncoder;



/**
 * 
 */
public class MonitoringFileTableModel extends AdminBaseTable {
	
	private List mItemNames_href = new LinkedList();
	private List mItemNames_path = new LinkedList();
	
	private Map mItemNames_files = new TreeMap();

	public MonitoringFileTableModel(Map configs, String key) {
					
						for (Iterator iter = configs.keySet().iterator(); iter.hasNext();) {
							String path = (String)iter.next();
							String encodedPath = URLEncoder.encode(path);
							String encodedKey = URLEncoder.encode(key);
							String href = "xcm_config.jsp?href="+ encodedPath + "&key=" + encodedKey;
							String[] path_array = new String[3];
							path_array[0] = href;
							path_array[1] = path;
							String split_file[] = path.split("/");
							path_array[2] = split_file[split_file.length-1];
							String file_key = split_file[split_file.length-1]+path;
							
							
							mItemNames_href.add(href);
							mItemNames_path.add(path);
												
							mItemNames_files.put(file_key, path_array);
						}
						
	}

	/**
	 * @see com.sap.isa.core.xcm.admin.model.table.AdminBaseTable#init()
	 */
	protected void init() {
		if (log.isDebugEnabled())	
			log.debug("Initializing 'MonitoringFileTableModel' table");
			
  		addColumn("Files", "Files");
  		addColumn("Path", "Path");
	}

	/**
	 * @see com.sapportals.htmlb.table.TableViewModel#getRowCount()
	 */
	public int getRowCount() {
		return mItemNames_href.size();
	}

	/**
	 * @see com.sapportals.htmlb.table.TableViewModel#getValueAt(int, int)
	 */
	public AbstractDataType getValueAt(int rowIndex, int index) {
   		String retVal;
		String key = (String) mItemNames_files.keySet().toArray()[rowIndex-1];

		String file[] = (String[]) mItemNames_files.get(key);
   		if (index==1){
   			//retVal = (String) mItemNames_href.get(rowIndex-1);

   			 retVal = file[0];
   			 
   		} else if (index==2){
   			retVal = file[1];
   		}else{
   			retVal = file[2];
   		}

		return new DataString(retVal);
	}

	/**
	 * @see com.sapportals.htmlb.table.TableViewModel#getValueAt(int, String)
	 */
	public AbstractDataType getValueAt(int rowIndex, String name) {
	    return getValueAt(rowIndex, getColumnIndex(name));
	}

	/**
	 * @see com.sapportals.htmlb.table.TableViewModel#setValueAt(AbstractDataType, int, int)
	 */
	public void setValueAt(AbstractDataType data, int rowIndex, int column) {
		// no implementation
	}
}
