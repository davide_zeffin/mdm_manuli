package com.sap.isa.core.xcm.admin.model.table;

// java imports 

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import com.sap.isa.core.xcm.admin.model.help.HelpItem;
import com.sap.isa.core.xcm.admin.model.help.HelpItemContainer;
import com.sapportals.htmlb.table.TableColumn;
import com.sapportals.htmlb.type.AbstractDataType;
import com.sapportals.htmlb.type.DataString;

/**
 * 
 */
public class HelpItemSummaryTableModel extends AdminBaseTable {

	private HelpItemContainer mHelpItemContainer;
	
	private List mItemNames = new LinkedList();

	public HelpItemSummaryTableModel(HelpItemContainer helpItemContainer) {
		mHelpItemContainer = helpItemContainer;
		Set names = mHelpItemContainer.getItemNames();
		for (Iterator iter = names.iterator(); iter.hasNext();) {
			String name = (String)iter.next();
			mItemNames.add(name);
		}
	}

	/**
	 * @see com.sap.isa.core.xcm.admin.model.table.AdminBaseTable#init()
	 */
	protected void init() {
		if (log.isDebugEnabled())	
			log.debug("Initializing 'HelpItemDetailTableModel' table");
			
  	    TableColumn nameolumn = null;
  		addColumn("Name", "Name");
  		addColumn("Description", "Description");
	}

	/**
	 * @see com.sapportals.htmlb.table.TableViewModel#getRowCount()
	 */
	public int getRowCount() {
		return mHelpItemContainer.getNumItems();
	}

	/**
	 * @see com.sapportals.htmlb.table.TableViewModel#getValueAt(int, int)
	 */
	public AbstractDataType getValueAt(int rowIndex, int index) {
   		String retVal = "";
   		String name = (String)mItemNames.get(rowIndex - 1);
	    HelpItem item = mHelpItemContainer.getItem(name);
	    if (item == null)
	    	return new DataString("");
	    switch (index) {
	    	case 1: { 
	    		retVal = item.getName();
	    		break;
	    	}
	    	case 2: {
				retVal = item.getShorttext();
		    	break;
	    	}
			case 3: {
				retVal = item.getLongtext();
				break;
			}

	    }
		return new DataString(retVal);

	}

	/**
	 * @see com.sapportals.htmlb.table.TableViewModel#getValueAt(int, String)
	 */
	public AbstractDataType getValueAt(int rowIndex, String name) {
	    return getValueAt(rowIndex, getColumnIndex(name));
	}

	/**
	 * @see com.sapportals.htmlb.table.TableViewModel#setValueAt(AbstractDataType, int, int)
	 */
	public void setValueAt(AbstractDataType data, int rowIndex, int column) {
		// no implementation
	}
}
