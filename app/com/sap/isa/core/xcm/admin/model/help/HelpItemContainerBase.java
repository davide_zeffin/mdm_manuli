package com.sap.isa.core.xcm.admin.model.help;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class HelpItemContainerBase implements HelpItemContainer {

	private Map mHelpItems = new LinkedHashMap();
	private String mName;
	
	public HelpItemContainerBase(String name) {
		mName = name;
	}
	
	public String getContainerTitle() {
		return mName;
	}

	public int getNumItems() {
		return mHelpItems.size();
	}

	public Set getItemNames() {
		return Collections.unmodifiableSet(mHelpItems.keySet());
	}
	
	public HelpItem getItem(String name) {
		return (HelpItem)mHelpItems.get(name);
	}

	public void addItem(HelpItem item) {
		mHelpItems.put(item.getName(),item);
	}
	
	public String getBaseConfigName() {
		return "TODO: base config name";
	}
	
}
