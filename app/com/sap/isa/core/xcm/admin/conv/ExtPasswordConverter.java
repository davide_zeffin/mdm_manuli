package com.sap.isa.core.xcm.admin.conv;

import com.sap.isa.core.EnvironmentManager;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.security.SecureStore;
import com.sap.isa.core.util.conv.PasswordConverter;

  
/**
 * This class encrypts/decrypts passwords. It is used in XCM Administratrator and have to be registered 
 * in a  component parameter as follows:
 * <pre>
 * <param name="secret" type="text" shorttext="shorttext" 
 *        conversionclass="com.sap.isa.core.xcm.admin.conv.ExtPasswordConverter" 
 *        encodemethod="save" 
 *        decodemethod="retrieve"/>
 * </pre>
 * If the runtime environment is SAP J2EE Engine 6.20 a simple obfuscating algorithm is used. 
 * (@see SAJCoPasswordConverter)
 * If the runimte enviroment is SAP J2EE Engine 6.30 or higher, the Secure Storage is used. 
 */
public class ExtPasswordConverter implements PasswordConverter {

	protected static IsaLocation loc = IsaLocation.getInstance(ExtPasswordConverter.class.getName());	
	private ISAJCoPasswordConverter m620Conv = new ISAJCoPasswordConverter();

	public String save(String compName, String configName, String paramName, String paramValue) {
		if (EnvironmentManager.isSAPJ2EE620()) {
			return m620Conv.encode(paramValue);
		} else {
			try {
				SecureStore secStore = new SecureStore();
				return secStore.save(compName, configName, paramName, paramValue); 
			} catch (Throwable t) {
				String msg = "usage of SAP J2EE Engine Secure Storage failed. Using default encryption";
				loc.error("system.xcm.exception", new Object[] { msg }, t);
				return m620Conv.decode(paramValue);
			}
		}
	}

	public String retrieve(String compName, String configName, String paramName, String paramValue) {
		if (EnvironmentManager.isSAPJ2EE620()) {
			return m620Conv.decode(paramName);
		} else {
			try {
				SecureStore secStore = new SecureStore();
				return secStore.retrieve(compName, configName, paramName, paramValue); 
			} catch (Throwable t) {
				String msg = "usage of SAP J2EE Engine Secure Storage failed. Using default encryption";
				loc.error("system.xcm.exception", new Object[] { msg }, t);
				return m620Conv.decode(paramName);
			}
		}
	
	}
}
