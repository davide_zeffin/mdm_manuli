package com.sap.isa.core.xcm.admin.conv;

import com.sap.isa.core.security.PasswordEncrypt;
import com.sap.isa.core.util.conv.*;
import com.sap.isa.core.util.conv.ParamConverter;


public class ISAJCoPasswordConverter implements ParamConverter, PasswordConverter {

	private static final String key = "secret";

	public String encode(String value) {
		return PasswordEncrypt.encryptStringAsHex(value, key);
	}

	public String decode(String value) {
		try {
			return PasswordEncrypt.decryptStringAsHex(value, key);	
		} catch (IllegalArgumentException iaex) {
			return value;
		}
	}

}
