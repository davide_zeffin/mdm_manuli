package com.sap.isa.core.xcm.init;

/**
 * Constanst used when reading XCM Admin metadata
 */
public interface XCMAdminConstants {

	public static final String OBSOLETE = "obsolete";

}
