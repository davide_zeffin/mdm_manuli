package com.sap.isa.core.xcm.init;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.sap.isa.core.logging.IsaLocation;

/**
 * Encapsulates meta data used to display configuration parameter
 */
public class XCMAdminParamConstrainMetadata {
	
	/**
	 * Parameter type: optional
	 */
	public static final String TYPE_OPTIONAL = "optional";

	/**
	 * Parameter type: mandatory
	 */
	public static final String TYPE_MANDATORY = "mandatory";

	
	private Map mAllowedValuesDerived = new HashMap();
	private Map mAllowedValuesFix = new HashMap();
	private Map mAllowedValuesBase = new HashMap();
	private Map mAllowedValues     = new HashMap();
	private String mName;
	private String mComponent;
	private String mShorttext;
	private String mLongtext;
	private String mType;
	private String mStatus;
 
	protected static IsaLocation log = IsaLocation.
		getInstance(XCMAdminParamConstrainMetadata.class.getName());


	public void addAllowedValue(XCMAdminAllowedValueMetadata aw) {
		String type = aw.getType();
		if (type == null) {
			log.warn("system.xcm.warn", new Object[] { "Reading 'allowed value' failed: [type]=null" }, null);
			return;
		}
		if (type.equals(XCMAdminAllowedValueMetadata.VALUE_TYPE_BASE)) {
			mAllowedValuesBase.put(aw.getValue(), aw);
		}

		if (type.equals(XCMAdminAllowedValueMetadata.VALUE_TYPE_DERIVED))
			mAllowedValuesDerived.put(aw.getValue(), aw);

		if (type.equals(XCMAdminAllowedValueMetadata.VALUE_TYPE_FIX))
			mAllowedValuesFix.put(aw.getValue(), aw);

		mAllowedValues.put(aw.getValue(), aw);
	}

	public Map getAllowedValueMetaData() {
		return mAllowedValues; 
	}

	public Set getFixAllowedValue() {
		 return mAllowedValuesFix.keySet();
	}
	
	public void setName(String name) {
		mName = name;
	}
 
	public String getName() {
		return mName;
	}

	public void setStatus(String status) {
		mStatus = status;
	}
	
	
	
	public void setComponent(String component) {
		mComponent = component;
	}

	public String getComponent() {
		return mComponent;
	}

	public void setType(String type) {
		mType = type;
	}

	public String getType() {
		return mType;
	}


	public void setShorttext(String description) {
		mShorttext = XCMAdminInitHandler.getText(description);
	}

	public void setLongtext(String description) {
		mLongtext = XCMAdminInitHandler.getText(description);
	}


	public String getShorttext() {
		return mShorttext;
	}

	public String getLongtext() {
		return mLongtext;
	}

	public String getStatus () {
		return mStatus;
	}
	
	public String getAllowedValueShorttext(String allowedValueName) {
		if (allowedValueName == null || allowedValueName.length() == 0)
			return "No value selected";
		XCMAdminAllowedValueMetadata allowedValue = 
			(XCMAdminAllowedValueMetadata)mAllowedValues.get(allowedValueName);
		if (allowedValue == null)
			return null;
		else
			return allowedValue.getShorttext();			
			
	}

	public String getAllowedValueLongtext(String allowedValueName) {
		if (allowedValueName == null || allowedValueName.length() == 0)
			return "No value selected";
		XCMAdminAllowedValueMetadata allowedValue = 
			(XCMAdminAllowedValueMetadata)mAllowedValues.get(allowedValueName);
		if (allowedValue == null)
			return null;
		else
			return allowedValue.getLongtext();			
			
	}
	
	public boolean isAllowedValueObsolete(String allowedValueName) {
		if (allowedValueName == null || allowedValueName.length() == 0)
			return false;
		XCMAdminAllowedValueMetadata allowedValue = 
			(XCMAdminAllowedValueMetadata)mAllowedValues.get(allowedValueName);
		if (allowedValue == null)
			return false;
		else
			return allowedValue.isObsolete();			
	}

}
