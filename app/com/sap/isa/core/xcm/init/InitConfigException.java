package com.sap.isa.core.xcm.init;

/**
 * This exception is thrown by the ConfigReader if something goes wrong
 * while reading/processing configuration
 */
public class InitConfigException extends Exception {

	private Exception mBaseEx;
	private String mMsg = "";

	/** 
	 * Constructor
	 * @param msg Message describing the exception
	 */
	public InitConfigException(String msg) {
		super(msg);
		mMsg = msg;
	}

	/**
	 * Returns the base exception or <code>null</code> if there
	 * is no root exception
	 * @return the root exception
	 */
	public Exception getBaseException() {
		return mBaseEx;
	}

	/**
	 * Sets the base exception
	 * @param ex the base exception 
	 */
	public void setBaseException(Exception baseEx) {
		mBaseEx = baseEx;
	}

	/**
	 * Returns a string representation of this exception
	 * @return a string representation of this exception
	 */
	public String toString() {
		if (mBaseEx == null)
			return mMsg;
		else
			return mMsg + "\n" + mBaseEx.toString();
	}

}