package com.sap.isa.core.xcm.init;

// java imports
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.sap.isa.core.xcm.xml.ParamsStrategy;

public class ParamsConfig {

	private Map mValue = new HashMap();
	private Map mDescr = new HashMap();
//	private Map mScope = new HashMap();

	/**
	 * Adds parameter configuration
	 * @param name name of parameter
	 * @param value value of parameter
	 * @param scope scope of parameter
	 * @param descr description of parameter
	 */
	public void addParamConfig(String name, String value, String descr) {
		mValue.put(name, value);
		mDescr.put(name, descr);
	}

	/**
	 * Returns a container containing parameter configuration 
	 * This container is used when processing XML files
	 * @return a container containing parameter configuration 
	 */
	public ParamsStrategy.Params getParamsContainer() {
		ParamsStrategy.ParamsContainer params =
			new ParamsStrategy.ParamsContainer();

		for (Iterator iter = mValue.keySet().iterator(); iter.hasNext();) {
			String paramName = (String) iter.next();
			String paramValue = (String) mValue.get(paramName);
			params.addParam(paramName, paramValue);
		}

		return params;
	}

	public String toString() {
		StringBuffer sb = new StringBuffer();
		for (Iterator iter = mValue.keySet().iterator(); iter.hasNext();) {
			String paramName = (String) iter.next();
			String value = (String) mValue.get(paramName);
			String description = (String) mDescr.get(paramName);
			sb.append(
				"param name = '"
					+ paramName
					+ "'; value = '"
					+ value
					+ "'; description = '"
					+ description
					+ "'\n");
		}

		return sb.toString();
	}
}
